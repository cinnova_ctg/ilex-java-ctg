package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.ScheduleTab;

public class ScheduleTabOrganizer {
	public static void setFormFromScheduleTab(ScheduleTab scheduleTab,JobDashboardBean form)	{
		form.setAppendixId(scheduleTab.getAppendixId());
		form.setJobId(scheduleTab.getJobId());
		GeneralJobInfoOrganizer.setFormFromGeneralInfo(scheduleTab.getGeneralJobInfo(),form);
	}

}
