package com.mind.newjobdb.business;

import com.mind.bean.mpo.PODTO;
import com.mind.bean.msp.SiteInfo;
import com.mind.bean.newjobdb.PartnerInfo;
import com.mind.newjobdb.dao.PoWoInfo;

public class POWOTabOrganizer {
	
	public static void setFormFromPOWOTab(PoWoInfo powoInfo,PODTO poBean)
	{
		POGeneralInfoOrganizer.setFormFromPOGeneralInfo(powoInfo.getPoGeneralInfo(),poBean);
		poBean.setSpecial_condition(powoInfo.getPoSpecialConditions());
		poBean.setWoNOA(powoInfo.getWoNatureOfActivity());
		poBean.setWoSI(powoInfo.getWoSpecialInstructions());
		PORequiredEquipmentOrganizer.setFormFromRequiredEquipment(powoInfo.getRequiredEquipments(),poBean);
	}
	public static PoWoInfo setPOWOTabFromForm(PODTO poBean)
	{
		PoWoInfo powoInfo = new PoWoInfo();
		powoInfo.setPoGeneralInfo(POGeneralInfoOrganizer.setPOGeneralInfoFromForm(poBean));
		powoInfo.setPoSpecialConditions(poBean.getSpecial_condition());
		powoInfo.setWoNatureOfActivity(poBean.getWoNOA());
		powoInfo.setWoSpecialInstructions(poBean.getWoSI());
		powoInfo.setRequiredEquipments(PORequiredEquipmentOrganizer.setRequiredEquipmentFromForm(poBean));
		return powoInfo;
	}
	
	public static void setFormFromPartnerInfo(PartnerInfo partnerInfo, PODTO poBean){
		poBean.setPartnerAddress(partnerInfo.getPartnerAddress());
		poBean.setPartnerCity(partnerInfo.getPartnerCity());
		poBean.setPartnerCountry(partnerInfo.getPartnerCountry());
		poBean.setPartnerLatitude(partnerInfo.getPartnerLatitude());
		poBean.setPartnerLongitude(partnerInfo.getPartnerLongitude());
	}
	
	public static void setFormFromSiteInfo(SiteInfo siteInfo, PODTO poBean){
		poBean.setJobAddress(siteInfo.getSite_address());
		poBean.setJobCity(siteInfo.getSite_city());
		poBean.setJobCountry(siteInfo.getSite_country());
		poBean.setJobLatitude(siteInfo.getSite_latitude());
		poBean.setJobLongitude(siteInfo.getSite_longitude());
		poBean.setJobState(siteInfo.getSite_state());
	}

}
