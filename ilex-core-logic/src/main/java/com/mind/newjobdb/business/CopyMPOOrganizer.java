package com.mind.newjobdb.business;



import com.mind.bean.mpo.PODTO;
import com.mind.bean.newjobdb.GeneralPOSetup;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.MPOList;
import com.mind.bean.po.CopyMpoToPoDTO;
public class CopyMPOOrganizer {
	public static void setFormFromGeneralPOSetup(GeneralPOSetup generalPOSetup,CopyMpoToPoDTO copyMpoToPoBean)
	{
		setFormFromDeliveryDate(generalPOSetup.getDeliverByDate(),copyMpoToPoBean);
		setFormFromIssueDate(generalPOSetup.getIssueDate(),copyMpoToPoBean);
	}
	public static void setGeneralPOSetupFromForm(CopyMpoToPoDTO copyMpoToPoBean)
	{
		GeneralPOSetup generalPOSetup = new GeneralPOSetup ();
		
	}
	public static void setFormFromMPOList(MPOList mpoList,CopyMpoToPoDTO copyMpoToPoBean)
	{
		MasterPurchaseOrderOrganizer.setFormFromMasterPurchaseOrders(mpoList.getMasterPurchaseOrders(),copyMpoToPoBean);
	}
	
	private static void setFormFromDeliveryDate(IlexTimestamp deliveryDate,CopyMpoToPoDTO copyMpoToPoBean)
	{
		copyMpoToPoBean.setDeliveryDate(deliveryDate.getDate());
		copyMpoToPoBean.setDeliveryHour(deliveryDate.getHour());
		copyMpoToPoBean.setDeliveryMinute(deliveryDate.getMinute());
		copyMpoToPoBean.setDeliveryIsAm(IlexTimestamp.getStringAMPM(deliveryDate.isAM()));
	}
	private static void setFormFromIssueDate(IlexTimestamp issueDate,CopyMpoToPoDTO copyMpoToPoBean)
	{
		copyMpoToPoBean.setIssueDate(issueDate.getDate());
		copyMpoToPoBean.setIssueHour(issueDate.getHour());
		copyMpoToPoBean.setIssueMinute(issueDate.getMinute());
		copyMpoToPoBean.setIssueIsAm(IlexTimestamp.getStringAMPM(issueDate.isAM()));
	}
	public static GeneralPOSetup setGeneralPOSetupFromForm(PODTO poBean)
	{
		GeneralPOSetup generalPOSetup = new GeneralPOSetup ();
		generalPOSetup.setDeliverByDate(setDeliveryByDateFromForm(poBean));
		generalPOSetup.setIssueDate(setIssueDateFromForm(poBean));
		return generalPOSetup;
	}
	private static IlexTimestamp setDeliveryByDateFromForm(PODTO poBean)
	{
		IlexTimestamp deliveryByDate = new IlexTimestamp();
		deliveryByDate.setDate(poBean.getDeliveryDate());
		deliveryByDate.setHour(poBean.getDeliveryHour());
		deliveryByDate.setMinute(poBean.getDeliveryMinute());
		if(poBean.getDeliveryIsAm()!=null) {
			deliveryByDate.setAM(IlexTimestamp.getBooleanAMPM(poBean.getDeliveryIsAm()));
		}
		return deliveryByDate;
	}
	private static IlexTimestamp setIssueDateFromForm(PODTO poBean)
	{
		IlexTimestamp issueDate = new IlexTimestamp();
		issueDate.setDate(poBean.getIssueDate());
		issueDate.setHour(poBean.getIssueHour());
		issueDate.setMinute(poBean.getIssueMinute());
		if(poBean.getIssueIsAm()!=null) {
			issueDate.setAM(IlexTimestamp.getBooleanAMPM(poBean.getIssueIsAm()));
		}
		return issueDate;
	}
}
