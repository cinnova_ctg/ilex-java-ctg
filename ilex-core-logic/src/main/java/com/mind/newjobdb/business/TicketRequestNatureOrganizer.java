package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.TicketRequestNature;

public class TicketRequestNatureOrganizer {

	
	public static void setFormFromTicketRequestNature(TicketRequestNature ticketRequestNature,JobDashboardBean form) {
	
		form.setProblemCategory(ticketRequestNature.getProblemCategory());
		form.setProblemDescription(ticketRequestNature.getProblemDescription());
		form.setSpecialInstructions(ticketRequestNature.getSpecialInstructions());
		form.setTicketRules(ticketRequestNature.getTicketRules());
	}
	
	public static TicketRequestNature getTicketRequestNature(JobDashboardBean form) {
		TicketRequestNature ticketRequestNature = new TicketRequestNature();
		ticketRequestNature.setProblemCategory(form.getProblemCategory());
		ticketRequestNature.setProblemDescription(form.getProblemDescription());
		ticketRequestNature.setSpecialInstructions(form.getSpecialInstructions());
		ticketRequestNature.setTicketRules(form.getTicketRules());
		return ticketRequestNature;
	}
	
}
