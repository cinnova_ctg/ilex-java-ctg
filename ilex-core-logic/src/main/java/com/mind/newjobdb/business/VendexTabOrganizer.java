package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.VendexTab;

public class VendexTabOrganizer {
	
	public static void setFormFromVendexTab(VendexTab vendexTab,JobDashboardBean form)	{
		form.setAppendixId(vendexTab.getAppendixId());
		form.setJobId(vendexTab.getJobId());
		
		GeneralJobInfoOrganizer.setFormFromGeneralInfo(vendexTab.getGeneralJobInfo(),form);
		
	}
	public static void setVendexTabFromForm(VendexTab vendexTab,JobDashboardBean form) {
		vendexTab.setGeneralJobInfo(GeneralJobInfoOrganizer.getGeneralJobInformation(form));
	}

}
