package com.mind.newjobdb.business;

import org.apache.log4j.Logger;

import com.mind.bean.newjobdb.CostVariations;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.POType;

public class CostVariationsOrganizer {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CostVariationsOrganizer.class);

	
	public static CostVariations getCostVariation(JobDashboardBean form)
	{
		CostVariations costVariations = new CostVariations();
		costVariations.setPVSType(setPVSType(form));
		POType PVSType =  costVariations.getPVSType();
		costVariations.setMinutemanType(setMinuteManType(form,PVSType));
		//costVariations.getMinutemanType().setComparedToPOType(costVariations.getPVSType());
		costVariations.setSpeedpayType(setSpeedpayType(form,PVSType));
		//costVariations.getSpeedpayType().setComparedToPOType(costVariations.getPVSType());
		return costVariations;
	}
	private static POType setPVSType(JobDashboardBean form)
	{
		POType PVSType = new POType(null);
		PVSType.setContractLaborCost(new Float(form.getPVSTypeCLC()));
		PVSType.setHourlyTravelCost(new Float(form.getPVSTypeTHC()));
		PVSType.setTotalCost(new Float(form.getPVSTypeTC()));
		return PVSType;
	}
	private static POType setMinuteManType(JobDashboardBean form,POType PVSType)
	{
		POType minutemanType = new POType(PVSType);
		minutemanType.setContractLaborCost(new Float(form.getMinutemanTypeCLC()));
		minutemanType.setHourlyTravelCost(new Float(form.getMinutemanTypeTHC()));
		minutemanType.setTotalCost(new Float(form.getMinutemanTypeTC()));
		return minutemanType;
	}
	private static POType setSpeedpayType(JobDashboardBean form,POType PVSType)
	{
		POType speedPayType = new POType(PVSType);
		speedPayType.setContractLaborCost(new Float(form.getSpeedpayTypeCLC()));
		speedPayType.setHourlyTravelCost(new Float(form.getSpeedpayTypeTHC()));
		speedPayType.setTotalCost(new Float(form.getSpeedpayTypeTC()));
		return speedPayType;
	}
	
	public static CostVariations setFormFromCostVariations(CostVariations costVariations,JobDashboardBean form)
	{
		setFormFromPVSType(costVariations.getPVSType(),form);
		setFormFromMinuteManType(costVariations.getMinutemanType(),form);
		setFormFromSpeedPayType(costVariations.getSpeedpayType(),form);
		return costVariations;
	}
	private static void  setFormFromMinuteManType(POType minuteManType,JobDashboardBean form)
	{
		if (logger.isDebugEnabled()) {
			logger.debug("setFormFromMinuteManType(POType, JobDashboardForm) - test of minute man" + minuteManType);
		}
		form.setMinutemanTypeCLC(String.valueOf(minuteManType.getContractLaborCost()));
		form.setMinutemanTypeTHC(String.valueOf(minuteManType.getHourlyTravelCost()));
		form.setMinutemanTypeTC(String.valueOf(minuteManType.getTotalCost()));
		form.setMinutemanTypeCostSavings(String.valueOf(minuteManType.getCostSaving()));
	}
	private static void  setFormFromSpeedPayType(POType speedPayType,JobDashboardBean form)
	{
		form.setSpeedpayTypeCLC(String.valueOf(speedPayType.getContractLaborCost()));
		form.setSpeedpayTypeTHC(String.valueOf(speedPayType.getHourlyTravelCost()));
		form.setSpeedpayTypeTC(String.valueOf(speedPayType.getTotalCost()));
		form.setSpeedpayTypeCostSavings(String.valueOf(speedPayType.getCostSaving()));
	}
	private static void  setFormFromPVSType(POType PVSType,JobDashboardBean form)
	{
		form.setPVSTypeCLC(String.valueOf(PVSType.getContractLaborCost()));
		form.setPVSTypeTHC(String.valueOf(PVSType.getHourlyTravelCost()));
		form.setPVSTypeTC(String.valueOf(PVSType.getTotalCost()));
		form.setPVSTypeCostSavings(String.valueOf(PVSType.getCostSaving()));
	}
	
}
