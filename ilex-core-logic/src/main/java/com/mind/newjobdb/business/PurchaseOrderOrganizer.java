package com.mind.newjobdb.business;

import java.util.ArrayList;

import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.common.POStatusTypes;
import com.mind.newjobdb.dao.PurchaseOrder;

public class PurchaseOrderOrganizer {
	
	public static ArrayList<PurchaseOrder> getListOfPurchaseOrder(JobDashboardBean form)
	{
		ArrayList <PurchaseOrder> listOfPurchaseOrders = new ArrayList <PurchaseOrder> ();
		for(int i=0;i<form.getPoNumber().length;i++)
		{
			PurchaseOrder purchaseOrder = new PurchaseOrder();
			purchaseOrder.setPoNumber(form.getPoNumber()[i]);
			purchaseOrder.setPoId(form.getPoId()[i]);
			purchaseOrder.setCancelledPONumber(form.getCancelledPONumber()[i]);
			purchaseOrder.setStatus(new Integer(form.getStatus()[i]));
			purchaseOrder.setCostingType(form.getCostingType()[i]);
			purchaseOrder.setRevision(form.getRevision()[i]);
			purchaseOrder.setPreviousStatus(new Integer(form.getPreviousStatus()[i]));
			purchaseOrder.setAuthorizedCost(new Float(form.getAuthorizedCost()[i]));
			purchaseOrder.setPartnerContact(form.getPartnerContact()[i]);
			purchaseOrder.setWoNumber(form.getWoNumber()[i]);
			listOfPurchaseOrders.add(purchaseOrder);
		}
		return listOfPurchaseOrders;
	}
	
	public static void  setFormFromPurchaseOrder(ArrayList<PurchaseOrder> listOfPurchaseOrder,JobDashboardBean form)
	{
		 String poNumber[] = new String[listOfPurchaseOrder.size()];
		 String cancelledPONumber[] = new String[listOfPurchaseOrder.size()];
		 String woNumber[] = new String[listOfPurchaseOrder.size()];
		 String status[] = new String[listOfPurchaseOrder.size()];
		 String previousStatus[] = new String[listOfPurchaseOrder.size()];
		 String revision[] = new String[listOfPurchaseOrder.size()];
		 String costingType[] = new String[listOfPurchaseOrder.size()];
		 String authorizedCost[] = new String[listOfPurchaseOrder.size()];
		 String partnerContact[] = new String[listOfPurchaseOrder.size()];
		 String primaryContact[] = new String[listOfPurchaseOrder.size()];
		 String poId[] = new String[listOfPurchaseOrder.size()];
		 String masterPOItemType[] = new String[listOfPurchaseOrder.size()];
		 Object[] availableActions = new Object[listOfPurchaseOrder.size()];
		 String customerDelCount[] = new String[listOfPurchaseOrder.size()];
		 String internalDelCount[] = new String[listOfPurchaseOrder.size()];
		 String totalDelCount[] = new String[listOfPurchaseOrder.size()];
		 String commissionPO[] = new String[listOfPurchaseOrder.size()];
		 
		for(int i=0;i<listOfPurchaseOrder.size();i++)
		{
			PurchaseOrder purchaseOrder = listOfPurchaseOrder.get(i);
			poNumber[i] = purchaseOrder.getPoNumber();
			poId[i] = purchaseOrder.getPoId();
			cancelledPONumber[i] = purchaseOrder.getCancelledPONumber();
			woNumber[i] = purchaseOrder.getWoNumber();
			status[i] = String.valueOf(POStatusTypes.getStatusName(purchaseOrder.getStatus()));
			previousStatus[i] = String.valueOf(purchaseOrder.getPreviousStatus());
			revision[i] = purchaseOrder.getRevision();
			costingType[i] = purchaseOrder.getCostingType();
			authorizedCost[i] = String.valueOf(purchaseOrder.getAuthorizedCost());
			partnerContact[i] = purchaseOrder.getPartnerContact();
			primaryContact[i] = purchaseOrder.getPrimaryContact();
			masterPOItemType[i] = purchaseOrder.getMasterPOItemType();
			availableActions[i] = purchaseOrder.getAvailableActions();
			customerDelCount[i] = purchaseOrder.getCustomerDelCount();
			internalDelCount[i] = purchaseOrder.getInternalDelCount();
			totalDelCount[i] = purchaseOrder.getTotalDelCount();
			commissionPO[i] = purchaseOrder.getCommissionPO();
		}
		form.setPoNumber(poNumber);
		form.setPoId(poId);
		form.setCancelledPONumber(cancelledPONumber);
		form.setWoNumber(woNumber);
		form.setStatus(status);
		form.setPreviousStatus(previousStatus);
		form.setRevision(revision);
		form.setCostingType(costingType);
		form.setAuthorizedCost(authorizedCost);
		form.setPartnerContact(partnerContact);
		form.setPrimaryContact(primaryContact);
		form.setMasterPOItemType(masterPOItemType);
		form.setAvailableActions(availableActions);
		form.setCustomerDelCount(customerDelCount);
		form.setInternalDelCount(internalDelCount);
		form.setTotalDelCount(totalDelCount);
		form.setCommissionPO(commissionPO);
	}
}
