package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.TicketScheduleInfo;

public class TicketScheduleInfoOrganizer {
	
	public static void setFormFromTicketScheduleInfo(TicketScheduleInfo ticketScheduleInfo,JobDashboardBean form) {
		setFormFromRequestedTimestamp(ticketScheduleInfo.getRequestedTimestamp(),form);
		setFormFromPreferredTimestamp(ticketScheduleInfo.getPreferredTimestamp(),form);
		setFormFromWindowFromTimestamp(ticketScheduleInfo.getWindowFromTimestamp(),form);
		setFormFromWindowToTimestamp(ticketScheduleInfo.getWindowToTimestamp(),form);
		setFormFromReceivedTimestamp(ticketScheduleInfo.getReceivedTimestamp(),form);
		
	}
	
	private static void  setFormFromRequestedTimestamp(IlexTimestamp requestedTimestamp,JobDashboardBean form) {
		form.setRequestedDate(requestedTimestamp.getDate());
		form.setRequestedHour(requestedTimestamp.getHour());
		form.setRequestedMinute(requestedTimestamp.getMinute());
		form.setRequestedIsAm(IlexTimestamp.getStringAMPM(requestedTimestamp.isAM()));
	}
	private static void  setFormFromPreferredTimestamp(IlexTimestamp preferredTimestamp,JobDashboardBean form) {
		form.setPreferredDate(preferredTimestamp.getDate());
		form.setPreferredHour(preferredTimestamp.getHour());
		form.setPreferredMinute(preferredTimestamp.getMinute());
		form.setPreferredIsAm(IlexTimestamp.getStringAMPM(preferredTimestamp.isAM()));
	}
	private static void  setFormFromWindowFromTimestamp(IlexTimestamp windowFromTimestamp,JobDashboardBean form) {
		form.setWindowFromDate(windowFromTimestamp.getDate());
		form.setWindowFromHour(windowFromTimestamp.getHour());
		form.setWindowFromMinute(windowFromTimestamp.getMinute());
		form.setWindowFromIsAm(IlexTimestamp.getStringAMPM(windowFromTimestamp.isAM()));
	}
	private static void  setFormFromWindowToTimestamp(IlexTimestamp windowToTimestamp,JobDashboardBean form) {
		form.setWindowToDate(windowToTimestamp.getDate());
		form.setWindowToHour(windowToTimestamp.getHour());
		form.setWindowToMinute(windowToTimestamp.getMinute());
		form.setWindowToIsAm(IlexTimestamp.getStringAMPM(windowToTimestamp.isAM()));
	}
	private static void  setFormFromReceivedTimestamp(IlexTimestamp receivedTimestamp,JobDashboardBean form) {
		form.setReceivedDate(receivedTimestamp.getDate());
		form.setReceivedHour(receivedTimestamp.getHour());
		form.setReceivedMinute(receivedTimestamp.getMinute());
		form.setReceivedIsAm(IlexTimestamp.getStringAMPM(receivedTimestamp.isAM()));
	}
	
	public static TicketScheduleInfo getTicketScheduleInfo(JobDashboardBean form) {
		TicketScheduleInfo ticketScheduleInfo = new TicketScheduleInfo();
		ticketScheduleInfo.setRequestedTimestamp(getRequestedTimeStamp(form));
		ticketScheduleInfo.setPreferredTimestamp(getPrefferedTimeStamp(form));
		ticketScheduleInfo.setWindowFromTimestamp(getWindowFromTimeStamp(form));
		ticketScheduleInfo.setWindowToTimestamp(getwindowToTimeStamp(form));
//		ticketScheduleInfo.setReceivedTimestamp(getReceivedTimeStamp(form));
		return ticketScheduleInfo;
	}
	
	private static IlexTimestamp  getRequestedTimeStamp(JobDashboardBean form) {
		IlexTimestamp requestedTimeStamp = new IlexTimestamp();
		requestedTimeStamp.setDate(form.getRequestedDate());
		requestedTimeStamp.setHour(form.getRequestedHour());
		requestedTimeStamp.setMinute(form.getRequestedMinute());
		requestedTimeStamp.setAM(IlexTimestamp.getBooleanAMPM(form.getRequestedIsAm()));
		return requestedTimeStamp;
	}
	private static IlexTimestamp  getPrefferedTimeStamp(JobDashboardBean form) {
		IlexTimestamp prefferedTimeStamp = new IlexTimestamp();
		prefferedTimeStamp.setDate(form.getPreferredDate());
		prefferedTimeStamp.setHour(form.getPreferredHour());
		prefferedTimeStamp.setMinute(form.getPreferredMinute());
		prefferedTimeStamp.setAM(IlexTimestamp.getBooleanAMPM(form.getPreferredIsAm()));
		return prefferedTimeStamp;
	}
	private static IlexTimestamp  getWindowFromTimeStamp(JobDashboardBean form) {
		IlexTimestamp windowFromTimeStamp = new IlexTimestamp();
		windowFromTimeStamp.setDate(form.getWindowFromDate());
		windowFromTimeStamp.setHour(form.getWindowFromHour());
		windowFromTimeStamp.setMinute(form.getWindowFromMinute());
		windowFromTimeStamp.setAM(IlexTimestamp.getBooleanAMPM(form.getWindowFromIsAm()));
		return windowFromTimeStamp;
	}
	private static IlexTimestamp  getwindowToTimeStamp(JobDashboardBean form) {
		IlexTimestamp windowToTimeStamp = new IlexTimestamp();
		windowToTimeStamp.setDate(form.getWindowToDate());
		windowToTimeStamp.setHour(form.getWindowToHour());
		windowToTimeStamp.setMinute(form.getWindowToMinute());
		windowToTimeStamp.setAM(IlexTimestamp.getBooleanAMPM(form.getWindowToIsAm()));
		return windowToTimeStamp;
	}
	private static IlexTimestamp  getReceivedTimeStamp(JobDashboardBean form) {
		IlexTimestamp receivedTimeStamp = new IlexTimestamp();
		receivedTimeStamp.setDate(form.getReceivedDate());
		receivedTimeStamp.setHour(form.getReceivedHour());
		receivedTimeStamp.setMinute(form.getReceivedMinute());
		receivedTimeStamp.setAM(IlexTimestamp.getBooleanAMPM(form.getReceivedIsAm()));
		return receivedTimeStamp;
	}
	
}
