package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.CustomerReferenceInfo;
import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobActivities;
import com.mind.bean.newjobdb.JobDashboardTab;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.JobPlanningNote;
import com.mind.newjobdb.dao.JobWorkflowChecklist;



/**
 * @purpose This class is used for the representation of 
 * 			the SETUP tab.
 */
public class SetupTab extends JobDashboardTab {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SetupTab() {
		tabId = JobDashboardTabType.SETUP_TAB;
	}
	private GeneralJobInfo generalJobInfo;
	private JobWorkflowChecklist topPendingCheckListItems;
	private JobActivities jobActivities;
	private JobPlanningNote jobPlanningNote;
	private CustomerReferenceInfo customerReference;
	
	public CustomerReferenceInfo getCustomerReference() {
		return customerReference;
	}
	public void setCustomerReference(CustomerReferenceInfo customerReference) {
		this.customerReference = customerReference;
	}
	public GeneralJobInfo getGeneralJobInfo() {
		return generalJobInfo;
	}
	public void setGeneralJobInfo(GeneralJobInfo generalInfo) {
		this.generalJobInfo = generalInfo;
	}
	public JobActivities getJobActivities() {
		return jobActivities;
	}
	public void setJobActivities(JobActivities jobActivities) {
		this.jobActivities = jobActivities;
	}
	public JobPlanningNote getJobPlanningNote() {
		return jobPlanningNote;
	}
	public void setJobPlanningNote(JobPlanningNote jobPlanningNote) {
		this.jobPlanningNote = jobPlanningNote;
	}
	public JobWorkflowChecklist getTopPendingCheckListItems() {
		return topPendingCheckListItems;
	}
	public void setTopPendingCheckListItems(
			JobWorkflowChecklist topPendingCheckListItems) {
		this.topPendingCheckListItems = topPendingCheckListItems;
	}
}