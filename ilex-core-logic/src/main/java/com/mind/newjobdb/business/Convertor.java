package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.DispatchScheduleTab;
import com.mind.bean.newjobdb.JobDashboard;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobDashboardTab;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.JobWorkflowBean;
import com.mind.bean.newjobdb.ScheduleTab;
import com.mind.bean.newjobdb.TicketDepotRequisition;
import com.mind.bean.newjobdb.TicketTab;
import com.mind.bean.newjobdb.VendexTab;
import com.mind.bean.prm.DepotRequisitionBean;
import com.mind.newjobdb.dao.ExecuteTab;
import com.mind.newjobdb.dao.JobWorkflowChecklist;



public class Convertor {
	
	public static JobDashboard convertToJobDashboard(JobDashboardBean form ) {
		JobDashboardTab jobDashboardTab = getJobDashboardTab(new Integer(form.getTabId().trim()).intValue());
		
		if (jobDashboardTab != null) {
			if(jobDashboardTab instanceof SetupTab) {
				SetupTabOrganizer.setSetupTabFromForm((SetupTab)jobDashboardTab,form);
			} else if(jobDashboardTab instanceof TicketTab) {
				TicketTabOrganizer.setTicketTabFromForm((TicketTab)jobDashboardTab, form);
			} else if(jobDashboardTab instanceof BuyTab) {
				BuyTabOrganizer.setBuyTabFromForm((BuyTab)jobDashboardTab,form);
			}
			else if(jobDashboardTab instanceof CompleteTab) {
				CompleteTabOrganizer.setCompleteTabFromForm((CompleteTab)jobDashboardTab,form);
			}
		}
		
		JobDashboard jobDashboard  = new JobDashboard();
		jobDashboard.setJobDashboardTab(jobDashboardTab);
		
		return jobDashboard;
	}
	
	public static void convertToJobDashboardForm(JobDashboard jobDashboard,JobDashboardBean form) {
		JobDashboardTab jobDashboardTab = jobDashboard.getJobDashboardTab();
		setFormFromTab(jobDashboardTab.getTabId(),jobDashboardTab,form);
	}
	
	private static JobDashboardTab getJobDashboardTab(int tabId) {
		JobDashboardTab jobDashboardTab = null;
		
		if( tabId == JobDashboardTabType.SETUP_TAB) {
			jobDashboardTab = new SetupTab();
		} else if (tabId == JobDashboardTabType.TICKET_TAB) {
			jobDashboardTab = new TicketTab();
		} else if (tabId == JobDashboardTabType.BUY_TAB) {
			jobDashboardTab = new BuyTab();
		}
		
		return jobDashboardTab;
	}
	
	private static JobDashboardTab setFormFromTab(int tabId,JobDashboardTab jobDashboardTab,JobDashboardBean form)	{
		if( tabId == JobDashboardTabType.SETUP_TAB) {
			SetupTab setUpTab = (SetupTab)jobDashboardTab;
			SetupTabOrganizer.setFormFromSetupTab(setUpTab,form);
		} else if (tabId == JobDashboardTabType.TICKET_TAB) {
			TicketTab ticketTab = (TicketTab)jobDashboardTab;
			TicketTabOrganizer.setFormFromTicketTab(ticketTab,form);
		} else if (tabId == JobDashboardTabType.BUY_TAB) {
			BuyTab buyTab = (BuyTab)jobDashboardTab;
			BuyTabOrganizer.setFormFromBuyTab(buyTab, form);
		}else if (tabId == JobDashboardTabType.COMPLETE_TAB) {
			CompleteTab buyTab = (CompleteTab)jobDashboardTab;
			CompleteTabOrganizer.setFormFromCompleteTab(buyTab, form);
		}else if (tabId == JobDashboardTabType.EXECUTE_TAB) {
			ExecuteTab executeTab = (ExecuteTab)jobDashboardTab;
			ExecuteTabOrganizer.setFormFromExecuteTab(executeTab, form);
		}else if (tabId == JobDashboardTabType.CLOSE_TAB) {
			CloseTab closeTab = (CloseTab)jobDashboardTab;
			CloseTabOrganizer.setFormFromCloseTab(closeTab, form);
		}else if (tabId == JobDashboardTabType.VENDEX_TAB) {
			VendexTab vendexTab = (VendexTab)jobDashboardTab;
			VendexTabOrganizer.setFormFromVendexTab(vendexTab, form);
		}else if (tabId == JobDashboardTabType.SITE_TAB) {
			SiteTab siteTab = (SiteTab)jobDashboardTab;
			SiteTabOrganizer.setFormFromSiteTab(siteTab, form);
		}else if (tabId == JobDashboardTabType.FINANCIAL_TAB) {
			FinancialTab financialTab = (FinancialTab)jobDashboardTab;
			FinancialTabOrganizer.setFormFromFinancialTab(financialTab, form);
		}else if (tabId == JobDashboardTabType.SCHEDULE_TAB) {
			ScheduleTab scheduleTab = (ScheduleTab)jobDashboardTab;
			ScheduleTabOrganizer.setFormFromScheduleTab(scheduleTab, form);
		}else if (tabId == JobDashboardTabType.DISPATCH_SCHEDULE_TAB) {
			DispatchScheduleTab dispatchScheduleTab = (DispatchScheduleTab)jobDashboardTab;
			DispatchScheduleTabOrganizer.setFormFromDispatchScheduleTab(dispatchScheduleTab, form);
		}
		
		
		return jobDashboardTab;
	}
	
	public TicketDepotRequisition convertToTicketDepotRequisition(DepotRequisitionBean form)
	{
		TicketDepotRequisition ticketDepotRequisition = TicketDepotOrganizer.getTicketDepotRequisition(form);
		return ticketDepotRequisition;
	}
	public static void convertTDRToJobDashboardForm(TicketDepotRequisition ticketDepotRequisition,DepotRequisitionBean form) {
		TicketDepotOrganizer.setFormFromTDPR(ticketDepotRequisition.getProductRequests(),form);
	}
	public static JobWorkflowChecklist convertToJobWorkflowChecklist(JobWorkflowBean form)
	{
		JobWorkflowChecklist jobWorkflowChecklist = JobWorkflowCheckListOrganizer.getJobWorkflowChecklist(form);
		return jobWorkflowChecklist;
	}
	public static void convertJobWorkflowChecklistToJobWorkFlowForm(JobWorkflowChecklist jobWorkflowChecklist,JobWorkflowBean form) {
		JobWorkflowCheckListOrganizer.setFormFromJobWorkflowChecklist(jobWorkflowChecklist,form);
	}
}
