package com.mind.newjobdb.business;

import java.util.ArrayList;


import com.mind.bean.newjobdb.MasterPurchaseOrder;
import com.mind.bean.po.CopyMpoToPoDTO;
import com.mind.common.Util;

public class MasterPurchaseOrderOrganizer {
	
	public static void setFormFromMasterPurchaseOrders(ArrayList<MasterPurchaseOrder> listOfMasterPurchaseOrder,CopyMpoToPoDTO copyMpoToPoBean)
	{
		String mpoId[] = new String[listOfMasterPurchaseOrder.size()];
		String mpoName[] = new String[listOfMasterPurchaseOrder.size()];
		String masterPOItem[] = new String[listOfMasterPurchaseOrder.size()];
		String estimatedTotalCost[] = new String[listOfMasterPurchaseOrder.size()];
		String status[]  = new String[listOfMasterPurchaseOrder.size()];
		String isSelectable[] = new String[listOfMasterPurchaseOrder.size()];
		for(int i=0;i<listOfMasterPurchaseOrder.size();i++)
		{
			 
			 MasterPurchaseOrder masterPurchaseOrder = listOfMasterPurchaseOrder.get(i);
			 mpoId[i] = masterPurchaseOrder.getMpoId();
			 mpoName[i] = masterPurchaseOrder.getMpoName();
			 masterPOItem[i] = masterPurchaseOrder.getMasterPOItem();
			 estimatedTotalCost[i] = String.valueOf(masterPurchaseOrder.getEstimatedTotalCost());
			 status[i] = masterPurchaseOrder.getStatus();
			 isSelectable[i] = Util.getStringval(masterPurchaseOrder.isSelectable());
		}
		copyMpoToPoBean.setMpoId(mpoId);
		copyMpoToPoBean.setMpoName(mpoName);
		copyMpoToPoBean.setMasterPOItem(masterPOItem);
		copyMpoToPoBean.setEstimatedTotalCost(estimatedTotalCost);
		copyMpoToPoBean.setStatus(status);
		copyMpoToPoBean.setIsSelectable(isSelectable);
	}

}
