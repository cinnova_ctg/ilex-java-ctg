package com.mind.newjobdb.business;

import java.text.DecimalFormat;

import com.mind.bean.newjobdb.FinancialAnalysis;
import com.mind.bean.newjobdb.JobDashboardBean;

public class FinancialTabOrganizer {
	
	public static void setFormFromFinancialTab(FinancialTab financialTab,JobDashboardBean form)	{
		form.setAppendixId(financialTab.getAppendixId());
		form.setJobId(financialTab.getJobId());
		GeneralJobInfoOrganizer.setFormFromGeneralInfo(financialTab.getGeneralJobInfo(),form);	
		//PurchaseOrderListOrganizer.setFormFromPurchaseOrderList(financialTab.getPurchaseOrderList(),form);
		
		DecimalFormat dfcur = new DecimalFormat( "$#,###,##0.00" );
		DecimalFormat df = new DecimalFormat( "#,###,##0.00" );
		form.setCostAnalysis(financialTab.getFinancialAnalysis().getCostAnalysis());
		form.setMinutemanLaborTravel( dfcur.format(financialTab.getFinancialAnalysis().getMinutemanLaborTravel())+"" );
		form.setSpeedpayLaborTravel( dfcur.format(financialTab.getFinancialAnalysis().getSpeedpayLaborTravel())+"" );
		form.setMinutemanUsage( financialTab.getFinancialAnalysis().getMinutemanUsage()+"%" );
		form.setSpeedpayUsage( financialTab.getFinancialAnalysis().getSpeedpayUsage()+"%");
		form.setMinutemanCostSavings( dfcur.format(financialTab.getFinancialAnalysis().getMinutemanCostSavings())+"" );
		form.setSpeedpayCostSavings( dfcur.format(financialTab.getFinancialAnalysis().getSpeedpayCostSavings())+"" );
		form.setExtendedPrice( dfcur.format(financialTab.getFinancialAnalysis().getExtendedPrice())+"" );
		form.setJobDiscount( dfcur.format(financialTab.getFinancialAnalysis().getJobDiscount())+"" );
		form.setInvoicePrice( dfcur.format(financialTab.getFinancialAnalysis().getInvoicePrice())+"" );
		form.setJobReserver( dfcur.format(financialTab.getFinancialAnalysis().getJobReserver())+"" );
		form.setProFormaVGPM( df.format(financialTab.getFinancialAnalysis().getProFormaVGPM())+"" );
		form.setActualVGPM( df.format(financialTab.getFinancialAnalysis().getActualVGPM())+"" );
		form.setAppendixActualVGPM( df.format(financialTab.getFinancialAnalysis().getAppendixActualVGPM())+"" );
		form.setTotalEstimatedCost( dfcur.format(financialTab.getFinancialAnalysis().getTotalEstimatedCost())+"" );
		form.setTotalActualCost( dfcur.format(financialTab.getFinancialAnalysis().getTotalActualCost())+"" );
		form.setContractLaborEstimatedCost( dfcur.format(financialTab.getFinancialAnalysis().getContractLaborEstimatedCost())+"" );
		form.setContractLaborActualCost( dfcur.format(financialTab.getFinancialAnalysis().getContractLaborActualCost())+"" );
		form.setContractMaterialEstimatedCost( dfcur.format(financialTab.getFinancialAnalysis().getContractMaterialEstimatedCost())+"" );
		form.setContractMaterialActualCost( dfcur.format(financialTab.getFinancialAnalysis().getContractMaterialActualCost())+"" );
		form.setContractFreightEstimatedCost( dfcur.format(financialTab.getFinancialAnalysis().getContractFreightEstimatedCost())+"" );
		form.setContractFreightActualCost( dfcur.format(financialTab.getFinancialAnalysis().getContractFreightActualCost())+"" );
		form.setContractTravelEstimatedCost( dfcur.format(financialTab.getFinancialAnalysis().getContractTravelEstimatedCost())+"" );
		form.setContractTravelActualCost( dfcur.format(financialTab.getFinancialAnalysis().getContractTravelActualCost())+"" );
		form.setContractTotalEstimatedCost( dfcur.format(financialTab.getFinancialAnalysis().getContractTotalEstimatedCost())+"" );
		form.setContractTotalActualCost( dfcur.format(financialTab.getFinancialAnalysis().getContractTotalActualCost())+"" );
		form.setCorporateLaborEstimatedCost( dfcur.format(financialTab.getFinancialAnalysis().getCorporateLaborEstimatedCost())+"" );
		form.setCorporateLaborActualCost( dfcur.format(financialTab.getFinancialAnalysis().getCorporateLaborActualCost())+"" );
		form.setCorporateMaterialEstimatedCost( dfcur.format(financialTab.getFinancialAnalysis().getCorporateMaterialEstimatedCost())+"" );
		form.setCorporateMaterialActualCost( dfcur.format(financialTab.getFinancialAnalysis().getCorporateMaterialActualCost())+"" );
		form.setCorporateFreightEstimatedCost( dfcur.format(financialTab.getFinancialAnalysis().getCorporateFreightEstimatedCost())+"" );
		form.setCorporateFreightActualCost( dfcur.format(financialTab.getFinancialAnalysis().getCorporateFreightActualCost())+"" );
		form.setCorporateTravelEstimatedCost( dfcur.format(financialTab.getFinancialAnalysis().getCorporateTravelEstimatedCost())+"" );
		form.setCorporateTravelActualCost( dfcur.format(financialTab.getFinancialAnalysis().getCorporateTravelActualCost())+"" );
		form.setCorporateTotalEstimatedCost(dfcur.format(financialTab.getFinancialAnalysis().getCorporateTotalEstimatedCost())+"" );
		form.setCorporateTotalActualCost( dfcur.format(financialTab.getFinancialAnalysis().getCorporateTotalActualCost())+"" );
		
		String[] parameterStr = getChd(financialTab.getFinancialAnalysis());
		form.setChd(parameterStr[0]);
		form.setChl(parameterStr[1]);
		form.setChco(parameterStr[2]);
	}
	
	private static String[] getChd(FinancialAnalysis financialAnalysis){
		String[] parameterStr = new String[3];
		StringBuffer chd = new StringBuffer();
		StringBuffer chl = new StringBuffer();
		StringBuffer chco = new StringBuffer();
		
		if(financialAnalysis.getContractCost() > 0.0){
			chd.append(financialAnalysis.getContractCost());
			chl.append("Contract%20Cost");
			chco.append("F8A186");
		}
		if(financialAnalysis.getVgp() > 0.0){
			addChar(chd,chl,chco);
			chd.append(financialAnalysis.getVgp());
			chl.append("VGP");
			chco.append("7f99b0");
		}
		if(financialAnalysis.getCorporateCost() > 0.0){
			addChar(chd,chl,chco);
			chd.append(financialAnalysis.getCorporateCost());
			chl.append("Corporate%20Cost");
			chco.append("a9a8ad");
		}
		if(financialAnalysis.getDiscountCost() > 0.0){
			addChar(chd,chl,chco);
			chd.append(financialAnalysis.getDiscountCost());
			chl.append("Discount%20Cost");
			chco.append("f5533e");
		}
		
		parameterStr[0] = chd.toString().trim();
		parameterStr[1] = chl.toString().trim();
		parameterStr[2] = chco.toString().trim();
		return parameterStr;
	}
	
	private static void addChar(StringBuffer chd, StringBuffer chl, StringBuffer chco){
		if(chd.toString().trim().length() > 0){
			chd.append(",");
			chl.append("|");
			chco.append(",");
		}
	}

}
