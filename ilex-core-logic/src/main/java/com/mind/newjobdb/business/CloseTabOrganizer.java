package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.JobDashboardBean;

public class CloseTabOrganizer {
	
	public static void setCloseTabFromForm(CloseTab closeTab,JobDashboardBean form) {
		closeTab.setGeneralJobInfo(GeneralJobInfoOrganizer.getGeneralJobInformation(form));
		closeTab.setPurchaseOrderList(PurchaseOrderListOrganizer.getPurcahseOrderListObject(form));
	}
	
	public static void setFormFromCloseTab(CloseTab closeTab,JobDashboardBean form)	{
		form.setAppendixId(closeTab.getAppendixId());
		form.setJobId(closeTab.getJobId());
		GeneralJobInfoOrganizer.setFormFromGeneralInfo(closeTab.getGeneralJobInfo(),form);
		GeneralJobInfoOrganizer.setFormFromInvoiceSummary(closeTab.getJobInvoiceSummary(), form);
		JobWorkflowCheckListOrganizer.setFormFromJobWorkflowChecklist(closeTab.getTopPendingCheckListItems(), form);
		PurchaseOrderListOrganizer.setFormFromPurchaseOrderList(closeTab.getPurchaseOrderList(),form);
	}

}
