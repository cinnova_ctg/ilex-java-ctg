package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobDashboardTab;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.JobInvoiceSummary;
import com.mind.newjobdb.dao.JobWorkflowChecklist;
import com.mind.newjobdb.dao.PurchaseOrderList;



/**
 * @purpose This class is used for the representation of 
 * 			the Close tab.
 * 
 * @ActionsAllowed from Close tab:
 * 		All POs will be displayed on the Close tab, but only 
 * 		POs in the draft status will have applicable actions 
 * 		which can be initiated from the Close tab.
 * 		
 * 		The Close tab is used to manage the POs which are in the draft state.  
 * 		Thus as POs are created based on the copy or build functions, 
 * 		they appear on the Close tab with Actions possible on Draft state.
 */
public class CloseTab extends JobDashboardTab {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appendixId;
	private String jobId;

	/**
	 * The Close tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	private GeneralJobInfo generalJobInfo;
	private JobWorkflowChecklist topPendingCheckListItems;
	private PurchaseOrderList purchaseOrderList;
	private JobInvoiceSummary jobInvoiceSummary;

	public JobInvoiceSummary getJobInvoiceSummary() {
		return jobInvoiceSummary;
	}

	public void setJobInvoiceSummary(JobInvoiceSummary jobInvoiceSummary) {
		this.jobInvoiceSummary = jobInvoiceSummary;
	}

	public PurchaseOrderList getPurchaseOrderList() {
		return purchaseOrderList;
	}

	public void setPurchaseOrderList(PurchaseOrderList purchaseOrderList) {
		this.purchaseOrderList = purchaseOrderList;
	}

	public CloseTab() {
		tabId = JobDashboardTabType.CLOSE_TAB;
	}

	public JobWorkflowChecklist getTopPendingCheckListItems() {
		return topPendingCheckListItems;
	}

	public void setTopPendingCheckListItems(
			JobWorkflowChecklist topPendingCheckListItems) {
		this.topPendingCheckListItems = topPendingCheckListItems;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	/**
	 * The Close tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public GeneralJobInfo getGeneralJobInfo() {
		return generalJobInfo;
	}

	/**
	 * The Close tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public void setGeneralJobInfo(GeneralJobInfo generalJobInfo) {
		this.generalJobInfo = generalJobInfo;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

}
