package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobPlanningNote;

public class JobPlanningNoteOrganizer {
	
	public static JobPlanningNote getJobPlanningNote(JobDashboardBean form) {
		JobPlanningNote planningNote = new JobPlanningNote();
		
		planningNote.setNote(form.getNote());
		
		return planningNote;
	}
	
	public static void setFormFromJobPlanningNote(JobPlanningNote jobPlanningNote,JobDashboardBean form) {
		form.setNote(jobPlanningNote.getNote());
	}

}
