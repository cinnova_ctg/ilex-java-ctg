package com.mind.newjobdb.business;

import java.util.ArrayList;


import com.mind.bean.mpo.PODTO;
import com.mind.bean.newjobdb.POResource;
import com.mind.common.Util;

public class POResourceOrganizer {
	
	
	
	
	public static void setFormFromPOResource(ArrayList <POResource> listOfPOResource,PODTO poBean)
	{
		
		String activityId[] = new String[listOfPOResource.size()];
		String activityName[] = new String[listOfPOResource.size()];
		String resourceName[] = new String[listOfPOResource.size()];
		String type[] = new String[listOfPOResource.size()];
		String revision[] = new String[listOfPOResource.size()];
		String activityQuantity[] = new String[listOfPOResource.size()];
		String resourceQuantity[] = new String[listOfPOResource.size()];
		String totalQuantity[] = new String[listOfPOResource.size()];
		String estimatedCostUnit[] = new String[listOfPOResource.size()];
		String estimatedCostTotal[] = new String[listOfPOResource.size()];
		String asPlannedQuantity[] = new String[listOfPOResource.size()];
		String asPlannedUnitCost[] = new String[listOfPOResource.size()];
		String asplannedSubTotal[] = new String[listOfPOResource.size()];
		String isShowOnWO[] = new String[listOfPOResource.size()];
		String isSelectedInPO[] = new String[listOfPOResource.size()];
		String resourceId[] = new String[listOfPOResource.size()];
		String resourceSubName[] = new String[listOfPOResource.size()];
		String resourceSubNamePre[] = new String[listOfPOResource.size()];
		String resourceSubId[] = new String[listOfPOResource.size()];
		String typeId[] = new String[listOfPOResource.size()];
		String estimatedCosttotal[] = new String[listOfPOResource.size()];
		String inTransit[] = new String[listOfPOResource.size()];
		for(int i=0;i<listOfPOResource.size();i++)
		{
			POResource poResource = listOfPOResource.get(i);
			activityId[i] = poResource.getActivityId();
			activityName[i] = poResource.getActivityName();
			resourceName[i] = poResource.getResourceName();
			type[i] = poResource.getType();
			revision[i] = poResource.getRevision();
			activityQuantity[i] = String.valueOf(poResource.getActivityQuantity());
			resourceQuantity[i] = String.valueOf(poResource.getResourceQuantity());
			totalQuantity[i] = String.valueOf(poResource.getTotalQuantity());
			estimatedCostUnit[i] = String.valueOf(poResource.getEstimatedCostUnit());
			estimatedCostTotal[i] = String.valueOf(poResource.getEstimatedCostTotal());
			asPlannedQuantity[i] = String.valueOf(poResource.getAsPlannedQuantity());
			asPlannedUnitCost[i] = String.valueOf(poResource.getAsPlannedUnitCost());
			asplannedSubTotal[i] = String.valueOf(poResource.getAsplannedSubTotal());
			isShowOnWO[i] = Util.getStringval(poResource.isShowOnWO());
			isSelectedInPO[i] = Util.getStringval(poResource.isSelectedInPO());
			resourceId[i] = poResource.getResourceId();
			resourceSubNamePre[i] = poResource.getResourceSubNamePre();
			resourceSubName[i] = poResource.getResourceSubName();
			resourceSubId[i] = poResource.getResourceSubId();
			typeId[i] = poResource.getTypeId();
			estimatedCosttotal[i] = String.valueOf(poResource.getEstimatedCostTotal());
			inTransit[i] = poResource.getInTransit();
		}
		
		poBean.setMpoActId(activityId);
		poBean.setActivityName(activityName);
		poBean.setMpoActivityResource(resourceId);
		poBean.setResourceName(resourceName);
		poBean.setMpoTypeName(type);
		poBean.setMpoAct(activityQuantity);
		poBean.setMpoRes(resourceQuantity);
		poBean.setMpoUnit(estimatedCostUnit);
		poBean.setMpoActivityPlanQuantity(asPlannedQuantity);
		poBean.setMpoActivityPlanUnitCost(asPlannedUnitCost);
		poBean.setMpoActivityIsWOShow(isShowOnWO);
		poBean.setIsSelectedInPO(isSelectedInPO);
		poBean.setResourceHidden(resourceId);
		poBean.setMpoActResourceSubNamePre(resourceSubNamePre);
		poBean.setMpoActResourceSubName(resourceSubName);
		poBean.setMpoActResourceSubId(resourceSubId);
		poBean.setMpoType(typeId);
		poBean.setEstimatedCostTotal(estimatedCostTotal);
		poBean.setRevision(revision);
		poBean.setCheckInTransit(inTransit);
	}
	public static ArrayList<POResource> setPOResourceFromForm(PODTO poBean)
	{
		ArrayList<POResource> listOfPOResources = new ArrayList<POResource>();
		String plqty,plUnc;
		if(poBean.getResourceHidden()!=null) {
			for(int i=0;i<poBean.getResourceHidden().length;i++)
			{
					//System.out.println("val of resource "+poBean.getResourceHidden()[i]);
					String rs[] = poBean.getResourceHidden()[i].split("~");
				if(rs[1]!=null && !rs[1].trim().equalsIgnoreCase("N")){
					POResource poResource = new POResource();
					//poResource.setActivityId(poBean.getMpoActId()[i]);
					//poResource.setActivityName(poBean.getActivityName()[i]);
					poResource.setActivityQuantity(new Float(poBean.getMpoAct()[i]));
					poResource.setType(poBean.getMpoTypeName()[i]);
					poResource.setResourceQuantity(new Float(poBean.getMpoRes()[i]));
					poResource.setEstimatedCostUnit(new Float(poBean.getMpoUnit()[i]));
					if(poBean.getMpoActivityPlanQuantity()[i]==null || poBean.getMpoActivityPlanQuantity()[i].trim().equalsIgnoreCase(""))
						plqty = "0";
					else
						plqty = poBean.getMpoActivityPlanQuantity()[i];
					if(poBean.getMpoActivityPlanUnitCost()[i]==null || poBean.getMpoActivityPlanUnitCost()[i].trim().equalsIgnoreCase(""))
						plUnc = "0";
					else
						plUnc = poBean.getMpoActivityPlanUnitCost()[i];
					poResource.setAsPlannedQuantity(new Float(plqty));
					poResource.setAsPlannedUnitCost(new Float(plUnc));
					poResource.setShowOnWO(Util.getBooleanval(poBean.getIsShowHidden()[i]));
					//poResource.setSelectedInPO(Util.getBooleanval(poBean.getIsSelectedInPO()[i]));
					poResource.setResourceId(rs[0]);
					poResource.setResourceSubName(poBean.getMpoActResourceSubName()[i]);
					poResource.setResourceSubId(poBean.getMpoActResourceSubId()[i]);
					poResource.setTypeId(poBean.getMpoType()[i]);
					//poResource.setEstimatedCostTotal(new Float(poBean.getEstimatedCostTotal()[i]));
					listOfPOResources.add(poResource);
				}
			}
		}
		return listOfPOResources;
	}
}
