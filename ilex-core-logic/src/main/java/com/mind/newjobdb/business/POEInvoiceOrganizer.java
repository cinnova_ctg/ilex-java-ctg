package com.mind.newjobdb.business;

import com.mind.bean.mpo.PODTO;
import com.mind.bean.newjobdb.EInvoice;

public class POEInvoiceOrganizer {

	public static void setFormFromEInvoiceInfo(EInvoice eInvoice,PODTO poBean){
		
		poBean.setInvoiceId(eInvoice.getEInvoiceSummary().getInvoiceId());
		poBean.setInvoiceNumber(eInvoice.getEInvoiceSummary().getInvoiceNumber());
		poBean.setInvoiceType(eInvoice.getEInvoiceSummary().getInvoiceType());
		poBean.setInvoiceStatus(eInvoice.getEInvoiceSummary().getInvoiceStatus());
		poBean.setInvoiceDate(eInvoice.getEInvoiceSummary().getInvoiceDate());
		poBean.setInvoiceReceivedDate(eInvoice.getEInvoiceSummary().getInvoiceReceivedDate());
		poBean.setInvoicePartnerComment(eInvoice.getEInvoiceSummary().getInvoicePartnerComment());
		poBean.setInvoiceRecivedBy(eInvoice.getEInvoiceSummary().getInvoiceRecivedBy());
		poBean.setInvoiceApprovedDate(eInvoice.getEInvoiceSummary().getInvoiceApprovedDate());
		poBean.setInvoiceApprovedBy(eInvoice.getEInvoiceSummary().getInvoiceApprovedBy());
		poBean.setInvoiceExplanation(eInvoice.getEInvoiceSummary().getInvoiceExplanation());
		poBean.setExplanationToPartner(eInvoice.getEInvoiceSummary().getInvoiceExplanation());
		poBean.setInvoicePaidDate(eInvoice.getEInvoiceSummary().getInvoicePaidDate());
		poBean.setInvoiceCheckNumber(eInvoice.getEInvoiceSummary().getInvoiceCheckNumber());
		poBean.setInvoiceAuthorisedTotal(eInvoice.getEInvoiceSummary().getInvoiceAuthorisedTotal());
		poBean.setInvoiceTotal(eInvoice.getEInvoiceSummary().getInvoiceTotal());
		poBean.setInvoiceApprovedTotal(eInvoice.getEInvoiceSummary().getInvoiceApprovedTotal());
		poBean.setInvoiceLastUpdatedBy(eInvoice.getEInvoiceSummary().getInvoiceLastUpdatedBy());
		poBean.setInvoiceLastUpdate( eInvoice.getEInvoiceSummary().getInvoiceLastUpdate());
		poBean.setInvoiceItems(eInvoice.getEInvoiceItems());
		poBean.setLineTaxAmount(eInvoice.getEInvoiceSummary().getLineTaxAmount());
		poBean.setLineTotal(eInvoice.getEInvoiceSummary().getLineTotal());
	}
}
