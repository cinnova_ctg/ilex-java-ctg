package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.JobDashboardBean;


public class BuyTabOrganizer {
	
		public static void setBuyTabFromForm(BuyTab buyTab,JobDashboardBean form) {
		buyTab.setGeneralJobInfo(GeneralJobInfoOrganizer.getGeneralJobInformation(form));
		buyTab.setJobPlanningNote(JobPlanningNoteOrganizer.getJobPlanningNote(form));
		buyTab.setPurchaseOrderList(PurchaseOrderListOrganizer.getPurcahseOrderListObject(form));
		buyTab.setCostVariations(CostVariationsOrganizer.getCostVariation(form));
	}
	
	public static void setFormFromBuyTab(BuyTab buyTab,JobDashboardBean form)	{
		form.setAppendixId(buyTab.getAppendixId());
		form.setJobId(buyTab.getJobId());
		GeneralJobInfoOrganizer.setFormFromGeneralInfo(buyTab.getGeneralJobInfo(),form);
		JobPlanningNoteOrganizer.setFormFromJobPlanningNote(buyTab.getJobPlanningNote(), form);
		JobWorkflowCheckListOrganizer.setFormFromJobWorkflowChecklist(buyTab.getTopPendingCheckListItems(), form);
		PurchaseOrderListOrganizer.setFormFromPurchaseOrderList(buyTab.getPurchaseOrderList(),form);
		CostVariationsOrganizer.setFormFromCostVariations(buyTab.getCostVariations(),form);
	}

}
