package com.mind.newjobdb.business;

import org.apache.log4j.Logger;

import com.mind.bean.msp.SiteInfo;
import com.mind.bean.newjobdb.CustomerReferenceInfo;
import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobInvoiceSummary;
import com.mind.bean.newjobdb.ScheduleInfo;
import com.mind.bean.newjobdb.TeamInfo;
import com.mind.common.Util;

public class GeneralJobInfoOrganizer {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(GeneralJobInfoOrganizer.class);

	public static GeneralJobInfo getGeneralJobInformation(JobDashboardBean form) {
		GeneralJobInfo generalJobInfo = new GeneralJobInfo();

		generalJobInfo.setJobId(new Long(form.getJobId()).longValue());
		generalJobInfo.setJobStatus(form.getJobStatus());
		generalJobInfo.setScheduleInfo(getScheduleInfo(form));
		generalJobInfo.setSiteInfo(getSiteInfo(form));
		// generalJobInfo.setTeamInfo(setTeamInfo(form));
		generalJobInfo.setCustomerReferenceInfo(getCustomerReferenceInfo(form));
		generalJobInfo.setShowCustomerReference(Util.getBooleanval(form
				.getShowCustomerReference()));

		return generalJobInfo;
	}

	private static ScheduleInfo getScheduleInfo(JobDashboardBean form) {
		ScheduleInfo scheduleInfo = new ScheduleInfo();

		if (logger.isDebugEnabled()) {
			logger.debug("getScheduleInfo(JobDashboardForm) - 111"
					+ form.getScheduleType());
		}
		scheduleInfo.setScheduleType(form.getScheduleType());
		if (logger.isDebugEnabled()) {
			logger.debug("getScheduleInfo(JobDashboardForm) - testingghghgh"
					+ scheduleInfo.getScheduleType());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getScheduleInfo(JobDashboardForm) - 222");
		}
		scheduleInfo.setStartDate(form.getStartDate());
		if (logger.isDebugEnabled()) {
			logger.debug("getScheduleInfo(JobDashboardForm) - 333");
		}
		scheduleInfo.setEndDate(form.getEndDate());
		if (logger.isDebugEnabled()) {
			logger.debug("getScheduleInfo(JobDashboardForm) - 444");
		}

		return scheduleInfo;
	}

	private static SiteInfo getSiteInfo(JobDashboardBean form) {
		SiteInfo siteInfo = new SiteInfo();
		siteInfo.setSite_city(form.getSite_city());
		siteInfo.setSite_country(form.getSite_country());
		siteInfo.setSite_number(form.getSite_number());
		siteInfo.setSite_state(form.getSite_state());
		siteInfo.setUS(form.isUs());

		return siteInfo;
	}

	private static TeamInfo getTeamInfo(JobDashboardBean form) {
		TeamInfo teamInfo = new TeamInfo();

		teamInfo.setOwner(form.getOwner());
		teamInfo.setSchedulers(form.getSchedulers());

		return teamInfo;
	}

	private static CustomerReferenceInfo getCustomerReferenceInfo(
			JobDashboardBean form) {
		CustomerReferenceInfo customerReferenceInfo = new CustomerReferenceInfo();

		customerReferenceInfo.setReference(form.getReference());

		return customerReferenceInfo;
	}

	public static void setFormFromGeneralInfo(GeneralJobInfo generalJobInfo,
			JobDashboardBean form) {
		form.setJobId(String.valueOf(generalJobInfo.getJobId()));
		form.setJobStatus(generalJobInfo.getJobStatus());
		form.setIsAddendumApproved(generalJobInfo.getIsAddendumApproved());
		form.setAddendumStatus(generalJobInfo.getAddendumStatus());

		setFormFromScheduleInfo(generalJobInfo.getScheduleInfo(), form);
		setFormFromTeamInfo(generalJobInfo.getTeamInfo(), form);
		setFormFromSiteInfo(generalJobInfo.getSiteInfo(), form);
		// setSiteInfoForForm(generalJobInfo,form);
		// setTeamInfoForForm(generalJobInfo,form);
		setFormFromCustomerRefInfo(generalJobInfo.getCustomerReferenceInfo(),
				form);
		form.setShowCustomerReference(Util.getStringval(generalJobInfo
				.isShowCustomerReference()));
	}

	private static void setFormFromScheduleInfo(ScheduleInfo scheduleInfo,
			JobDashboardBean form) {
		form.setScheduleid(scheduleInfo.getScheduleId());
		form.setScheduleType(scheduleInfo.getScheduleType());
		form.setStartDate(scheduleInfo.getStartDate());
		form.setEndDate(scheduleInfo.getEndDate());
		form.setCurrentSchDate(scheduleInfo.getStartDate());
		form.setReScheduleCount(scheduleInfo.getReScheduleCount());
		form.setReScheduleClientCount(scheduleInfo.getReScheduleClientCount());
		form.setReScheduleContingentCount(scheduleInfo
				.getReScheduleContingentCount());
		form.setReScheduleInternalCount(scheduleInfo
				.getReScheduleInternalCount());
		form.setTimeOnSite(scheduleInfo.getTimeOnSite());
		form.setTimeToRespond(scheduleInfo.getTimeToRespond());
		form.setCriticalityActual(scheduleInfo.getCriticalityActual());
		form.setCriticalitySuggested(scheduleInfo.getCriticalitySuggested());
	}

	private static void setFormFromSiteInfo(SiteInfo siteInfo,
			JobDashboardBean form) {
		form.setSite_city(siteInfo.getSite_city());
		form.setSite_country(siteInfo.getSite_country());
		form.setSite_number(siteInfo.getSite_number());
		form.setSite_state(siteInfo.getSite_state());
		form.setSiteName(siteInfo.getSite_name());
		form.setSiteAddress(siteInfo.getSite_address());
		form.setSitePrimaryPoc(siteInfo.getSite_primary_poc());
		form.setSiteSecondaryPoc(siteInfo.getSite_secondary_poc());
		form.setSitePrimaryPocEmail(siteInfo.getSite_primary_poc_email());
		form.setSiteSecondaryPocEmail(siteInfo.getSite_secondary_poc_email());
		form.setSitePrimaryPocPhone(siteInfo.getSite_primary_phone());
		form.setSiteSecondaryPocPhone(siteInfo.getSite_secondary_phone());
		form.setSiteZip(siteInfo.getSite_zip());
		form.setUs(siteInfo.isUS());
		form.setSite_priorities(siteInfo.getPriority());
	}

	public static void setFormFromInvoiceSummary(
			JobInvoiceSummary jobInvoiceSummary, JobDashboardBean form) {

		form.setInvNumber(jobInvoiceSummary.getInvNumber());
		form.setInvDate(jobInvoiceSummary.getInvDate());
		form.setInvAmount(jobInvoiceSummary.getInvAmount());
		form.setInvDiscount(jobInvoiceSummary.getInvDiscount());
		form.setInvAge(jobInvoiceSummary.getInvAge());
	}

	private static void setFormFromTeamInfo(TeamInfo teamInfo,
			JobDashboardBean form) {
		form.setOwner(teamInfo.getOwner());
		form.setSchedulers(teamInfo.getSchedulers());
	}

	private static void setFormFromCustomerRefInfo(
			CustomerReferenceInfo customerReferenceInfo, JobDashboardBean form) {
		form.setReference(customerReferenceInfo.getReference());
	}

}
