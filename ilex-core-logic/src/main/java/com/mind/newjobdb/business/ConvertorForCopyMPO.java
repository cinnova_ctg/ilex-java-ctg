package com.mind.newjobdb.business;

import com.mind.bean.po.CopyMpoToPoDTO;
import com.mind.newjobdb.dao.MPOCopier;

public class ConvertorForCopyMPO {

	public static void setFormFromMpoCopier(MPOCopier mpoCopier,CopyMpoToPoDTO copyMpoToPoBean)
	{
		//copyMpoToPoBean.setJobId(mpoCopier.getJobId());
		//copyMpoToPoBean.setAppendixId(mpoCopier.getAppendixId());
		CopyMPOOrganizer.setFormFromGeneralPOSetup(mpoCopier.getGeneralPOSetup(),copyMpoToPoBean);
		CopyMPOOrganizer.setFormFromMPOList(mpoCopier.getMpoList(),copyMpoToPoBean);
	}
	/*public static void setMasterPOFromForm(CopyMpoToPoBean copyMpoToPoBean)
	{
		CopyMPOOrganizer.setGeneralPOSetupFromForm(copyMpoToPoBean);
		CopyMPOOrganizer.setMPOListFromForm(copyMpoToPoBean);
		
	}*/
}
