package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.newjobdb.dao.PurchaseOrderList;

public class PurchaseOrderListOrganizer {

	
	public static PurchaseOrderList getPurcahseOrderListObject(JobDashboardBean form)
	{
		PurchaseOrderList purchaseOrderList = new PurchaseOrderList();
		purchaseOrderList.setPurchaseOrders(PurchaseOrderOrganizer.getListOfPurchaseOrder(form));
		return purchaseOrderList;
	}
	public static void setFormFromPurchaseOrderList(PurchaseOrderList purchaseOrderList,JobDashboardBean form)
	{
		PurchaseOrderOrganizer.setFormFromPurchaseOrder(purchaseOrderList.getPurchaseOrders(),form);
	}
	
}
