package com.mind.newjobdb.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.docm.Document;
import com.mind.bean.newjobdb.DocumentId;
import com.mind.common.Util;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.utils.DocumentUtility;
import com.mind.fw.core.dao.util.DBUtil;

public class AttachDocument {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AttachDocument.class);

	/**
	 * Get Document Object from DocM.
	 * 
	 * @param attachPOWO
	 * @param powoId
	 * @param ds
	 * @return
	 */
	public Document[] getDocument(String[] attachPOWO, String powoId,
			DataSource ds) {
		String type = getType(attachPOWO);
		ArrayList<DocumentId> documetnId = getDocumetnIds(powoId, ds);
		String[] docIds = getDocId(type, documetnId);
		Document[] documents = setDocument(docIds);
		return documents;
	}

	/**
	 * Set Document Object from DocM.
	 * 
	 * @param docIds
	 * @return
	 */
	private Document[] setDocument(String[] docIds) {

		Document[] documents = new Document[docIds.length];
		DocumentUtility documentUtility = new DocumentUtility();
		for (int i = 0; i < docIds.length; i++) {
			try {
				documents[i] = documentUtility.getDocument(docIds[i], "");
			} catch (CouldNotCheckDocumentException ex) {
				logger.error("setDocument(String[])", ex);

				logger.error("setDocument(String[])", ex);
			}
		}
		return documents;
	}

	/**
	 * @param attachPOWO
	 * @return
	 */
	private String getType(String[] attachPOWO) {
		StringBuffer type = new StringBuffer();

		for (int i = 0; i < attachPOWO.length; i++) {
			type.append(attachPOWO[i]);
		}
		return type.toString();
	}

	/**
	 * @param type
	 * @param docIdList
	 * @return
	 */
	private String[] getDocId(String type, ArrayList<DocumentId> docIdList) {

		Iterator<DocumentId> iterator = docIdList.iterator();
		StringBuffer poDocIdSB = new StringBuffer();
		StringBuffer woDocIdSB = new StringBuffer();
		StringBuffer withPOWODocIdSB = new StringBuffer();

		String[] poDocId;
		String[] woDocId;
		String[] withPOWO;
		String[] docIds;

		while (iterator.hasNext()) {
			DocumentId documentId = (DocumentId) iterator.next();

			if (documentId.getWithPO().equals("1")
					&& documentId.getWithWO().equals("0")) {
				poDocIdSB.append(documentId.getDocId() + ",");
			}
			if (documentId.getWithPO().equals("1")
					&& documentId.getWithWO().equals("1")) {
				withPOWODocIdSB.append(documentId.getDocId() + ",");
			}
			if (documentId.getWithPO().equals("0")
					&& documentId.getWithWO().equals("1")) {
				woDocIdSB.append(documentId.getDocId() + ",");
			}
		}
		Util util = new Util();
		poDocIdSB = util.removeLastCommChar(poDocIdSB);
		woDocIdSB = util.removeLastCommChar(woDocIdSB);
		withPOWODocIdSB = util.removeLastCommChar(withPOWODocIdSB);

		poDocId = poDocIdSB.toString().split(",");
		woDocId = woDocIdSB.toString().split(",");
		withPOWO = withPOWODocIdSB.toString().split(",");

		poDocId = checkLengh(poDocId);
		woDocId = checkLengh(woDocId);
		withPOWO = checkLengh(withPOWO);

		docIds = getIds(type, poDocId, woDocId, withPOWO);

		return docIds;
	}

	private String[] checkLengh(String[] strArray) {
		if (strArray.length == 1) {
			if (strArray[0] == null || strArray[0].equals("")) {
				strArray = new String[0];
			}
		}
		return strArray;
	}

	/**
	 * Get Strin[] of docIds.
	 * 
	 * @param type
	 * @param poDocId
	 * @param woDocId
	 * @return
	 */
	private String[] getIds(String type, String[] poDocId, String[] woDocId,
			String[] withPOWO) {

		int arrayLength = 0;

		if (type.equals("PO")) {
			arrayLength = poDocId.length + withPOWO.length;
		} else if (type.equals("POWO")) {
			arrayLength = poDocId.length + woDocId.length + withPOWO.length;
			;
		} else if (type.equals("WO")) {
			arrayLength = woDocId.length + withPOWO.length;
			;
		}

		String[] docIds = new String[arrayLength];

		if (type.equals("PO")) {
			docIds = new String[arrayLength];
			int count1 = 0;
			for (int i = 0; i < poDocId.length; i++) {
				docIds[count1++] = poDocId[i];
			}
			for (int i = 0; i < withPOWO.length; i++) {
				docIds[count1++] = withPOWO[i];
			}

		} else if (type.equals("POWO")) {
			docIds = new String[arrayLength];
			int count2 = 0;
			for (int i = 0; i < poDocId.length; i++) {
				docIds[count2++] = poDocId[i];
			}
			for (int i = 0; i < woDocId.length; i++) {
				docIds[count2++] = woDocId[i];
			}
			for (int i = 0; i < withPOWO.length; i++) {
				docIds[count2++] = withPOWO[i];
			}
		} else if (type.equals("WO")) {
			docIds = new String[arrayLength];
			int count3 = 0;
			for (int i = 0; i < woDocId.length; i++) {
				docIds[count3++] = woDocId[i];
			}
			for (int i = 0; i < withPOWO.length; i++) {
				docIds[count3++] = withPOWO[i];
			}
		}
		return docIds;
	}

	/**
	 * @param powoId
	 * @param ds
	 * @return
	 */
	private ArrayList getDocumetnIds(String powoId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList<DocumentId> docIdList = new ArrayList<DocumentId>();
		// powoId = "20774";
		try {
			conn = ds.getConnection();
			String sql = "select podb_doc_id, doc_with_po, doc_with_wo from podb_document where lm_po_id = ? "; // -
																												// 1
																												// parameter
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, powoId);
			rs = pStmt.executeQuery();

			while (rs.next()) {
				try {
					if (DocumentUtility.isDocumentExists(rs
							.getString("podb_doc_id"))) {
						Document document = DocumentUtility.getDocument(
								rs.getString("podb_doc_id"), "");
						if (document.getFileBytes() != null) {
							DocumentId documentId = new DocumentId();
							documentId.setDocId(rs.getString("podb_doc_id"));
							documentId.setWithPO(rs.getString("doc_with_po"));
							documentId.setWithWO(rs.getString("doc_with_wo"));
							docIdList.add(documentId);
						}
					}
				} catch (Exception e) {
					logger.warn(
							"getDocumetnIds(String, DataSource) - exception ignored",
							e);
				}
			}
		} catch (Exception e) {
			logger.error("getDocumetnIds(String, DataSource)", e);

			logger.error("getDocumetnIds(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return docIdList;
	}

	/**
	 * Method is used to check whether all the doucments attached to this po are
	 * present or not.
	 * 
	 * @param powoId
	 * @param ds
	 * @return String
	 */
	public static String checkAttachedDocuments(String powoId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String documentExists = "exists";
		try {
			conn = ds.getConnection();
			String sql = "select podb_doc_id, doc_with_po, doc_with_wo from podb_document where lm_po_id = ? "; // -
																												// 1
																												// parameter
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, powoId);
			rs = pStmt.executeQuery();

			while (rs.next()) {
				DocumentId documentId = new DocumentId();
				try {
					if (DocumentUtility.isDocumentExists(rs
							.getString("podb_doc_id"))) {
						Document document = DocumentUtility.getDocument(
								rs.getString("podb_doc_id"), "");
						if (document.getFileBytes() == null) {
							documentExists = "notexists";
							break;
						}
					} else {
						documentExists = "notexists";
						break;
					}
				} catch (Exception e) {
					logger.warn(
							"checkAttachedDocuments(String, DataSource) - exception ignored",
							e);
				}
			}
		} catch (Exception e) {
			logger.error("checkAttachedDocuments(String, DataSource)", e);

			logger.error("checkAttachedDocuments(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("checkAttachedDocuments(String, DataSource) - WWWWWWWWWWWWW    "
					+ documentExists + "WWWWWWWWWWWWWWWWW");
		}
		return documentExists;
	}

}
