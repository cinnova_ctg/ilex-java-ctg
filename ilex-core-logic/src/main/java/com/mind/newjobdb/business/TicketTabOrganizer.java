package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.TicketTab;

public class TicketTabOrganizer {

    public static void setFormFromTicketTab(TicketTab ticketTab, JobDashboardBean form) {
        form.setTicket_id(String.valueOf(ticketTab.getTicketId()));
        form.setAppendixId(String.valueOf(ticketTab.getAppendixId()));
        form.setJobId(String.valueOf(ticketTab.getJobId()));
        form.setTicketNumber(String.valueOf(ticketTab.getTicketNumber()));
        form.setUpdateCompleteTicket(ticketTab.getUpdateCompleteTicket());
        form.setWebTicket(ticketTab.getWebTicket());

        form.setRequestorIds(ticketTab.getRequestorIds());
        form.setAdditionalRecipientIds(ticketTab.getAdditionalRecipientIds());

        TicketRequestInfoOrganizer.setFormFromTicketRequestInfo(ticketTab.getRequestInfo(), form);
        TicketRequestNatureOrganizer.setFormFromTicketRequestNature(ticketTab.getRequestNature(), form);
        TicketScheduleInfoOrganizer.setFormFromTicketScheduleInfo(ticketTab.getScheduleInfo(), form);
    }

    public static void setTicketTabFromForm(TicketTab ticketTab, JobDashboardBean form) {
        ticketTab.setTicketId(new Long(form.getTicket_id()).longValue());
        ticketTab.setAppendixId(new Long(form.getAppendixId()).longValue());
        ticketTab.setJobId(new Long(form.getJobId()).longValue());
        ticketTab.setTicketNumber(form.getTicketNumber());
        ticketTab.setWebTicket(form.getWebTicket());

        ticketTab.setRequestorIds(form.getRequestorIds());
        ticketTab.setAdditionalRecipientIds(form.getAdditionalRecipientIds());

        ticketTab.setRequestInfo(TicketRequestInfoOrganizer.getTicketRequestInfo(form));
        ticketTab.setScheduleInfo(TicketScheduleInfoOrganizer.getTicketScheduleInfo(form));
        ticketTab.setRequestNature(TicketRequestNatureOrganizer.getTicketRequestNature(form));
    }
}
