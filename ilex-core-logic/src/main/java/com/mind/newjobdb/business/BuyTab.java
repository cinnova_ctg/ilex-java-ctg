package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.CostVariations;
import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobDashboardTab;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.JobPlanningNote;
import com.mind.newjobdb.dao.JobWorkflowChecklist;
import com.mind.newjobdb.dao.PurchaseOrderList;



/**
 * @purpose This class is used for the representation of 
 * 			the BUY tab.
 * 
 * @ActionsAllowed from buy tab:
 * 		All POs will be displayed on the Buy tab, but only 
 * 		POs in the draft status will have applicable actions 
 * 		which can be initiated from the Buy tab.
 * 		
 * 		The Buy tab is used to manage the POs which are in the draft state.  
 * 		Thus as POs are created based on the copy or build functions, 
 * 		they appear on the Buy tab with Actions possible on Draft state.
 */
public class BuyTab extends JobDashboardTab {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appendixId;
	private String jobId;

	/**
	 * The Buy tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	private GeneralJobInfo generalJobInfo;
	private JobWorkflowChecklist topPendingCheckListItems;
	private PurchaseOrderList purchaseOrderList;
	private JobPlanningNote jobPlanningNote;
	private JobBuyComments jobBuyComments;
	
	/**
	 * 'Cost Variations By PO Type' Section: These variation values are the 
	 * sum of all the POs that are part of this Job (listed in PO list).
	 */
	private CostVariations costVariations =  new CostVariations();

	public BuyTab() {
		tabId = JobDashboardTabType.BUY_TAB;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public CostVariations getCostVariations() {
		return costVariations;
	}

	public void setCostVariations(CostVariations costVariations) {
		this.costVariations = costVariations;
	}

	/**
	 * The Buy tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public GeneralJobInfo getGeneralJobInfo() {
		return generalJobInfo;
	}

	/**
	 * The Buy tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public void setGeneralJobInfo(GeneralJobInfo generalJobInfo) {
		this.generalJobInfo = generalJobInfo;
	}

	public JobBuyComments getJobBuyComments() {
		return jobBuyComments;
	}

	public void setJobBuyComments(JobBuyComments jobBuyComments) {
		this.jobBuyComments = jobBuyComments;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public JobPlanningNote getJobPlanningNote() {
		return jobPlanningNote;
	}

	public void setJobPlanningNote(JobPlanningNote jobPlanningNote) {
		this.jobPlanningNote = jobPlanningNote;
	}

	public PurchaseOrderList getPurchaseOrderList() {
		return purchaseOrderList;
	}

	public void setPurchaseOrderList(PurchaseOrderList purchaseOrderList) {
		this.purchaseOrderList = purchaseOrderList;
	}

	public JobWorkflowChecklist getTopPendingCheckListItems() {
		return topPendingCheckListItems;
	}

	public void setTopPendingCheckListItems(
			JobWorkflowChecklist topPendingCheckListItems) {
		this.topPendingCheckListItems = topPendingCheckListItems;
	}
}
