package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobDashboardTab;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.pm.SiteBean;



/**
 * @purpose This class is used for the representation of 
 * 			the Site tab.
 * 
 * @ActionsAllowed from Site tab:
 * 		All POs will be displayed on the Site tab, but only 
 * 		POs in the draft status will have applicable actions 
 * 		which can be initiated from the Site tab.
 * 		
 * 		The Site tab is used to manage the POs which are in the draft state.  
 * 		Thus as POs are created based on the copy or build functions, 
 * 		they appear on the Site tab with Actions possible on Draft state.
 */
public class SiteTab extends JobDashboardTab {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appendixId;
	private String jobId;

	/**
	 * The Site tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	private GeneralJobInfo generalJobInfo;
	
	private SiteBean siteForm; 
	

	public SiteTab() {
		tabId = JobDashboardTabType.SITE_TAB;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	/**
	 * The Site tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public GeneralJobInfo getGeneralJobInfo() {
		return generalJobInfo;
	}

	/**
	 * The Site tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public void setGeneralJobInfo(GeneralJobInfo generalJobInfo) {
		this.generalJobInfo = generalJobInfo;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public SiteBean getSiteForm() {
		return siteForm;
	}

	public void setSiteForm(SiteBean siteForm) {
		this.siteForm = siteForm;
	}

}
