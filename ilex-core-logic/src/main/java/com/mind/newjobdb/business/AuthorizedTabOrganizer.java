package com.mind.newjobdb.business;




import com.mind.bean.mpo.PODTO;
import com.mind.newjobdb.dao.POAuthorizedCostInfo;

public class AuthorizedTabOrganizer {
	
	public static void setFormFromAuthorizedTab(POAuthorizedCostInfo poAuthorizedCostInfo,PODTO poBean )
	{
		POGeneralInfoOrganizer.setFormFromPOGeneralInfo(poAuthorizedCostInfo.getPoGeneralInfo(),poBean);
		POActivityManagerOrganizer.setFormFromPOActivityManager(poAuthorizedCostInfo.getPoActivityManager(),poBean);
	}
	public static POAuthorizedCostInfo setAuthorizedTabFromForm(PODTO poBean )
	{
		POAuthorizedCostInfo poAuthorizedCostInfo = new POAuthorizedCostInfo();
		poAuthorizedCostInfo.setPoGeneralInfo(POGeneralInfoOrganizer.setPOGeneralInfoFromForm(poBean));
		poAuthorizedCostInfo.setPoActivityManager(POActivityManagerOrganizer.setPOActivityManagerFromForm(poBean));
		return poAuthorizedCostInfo;
	}

}
