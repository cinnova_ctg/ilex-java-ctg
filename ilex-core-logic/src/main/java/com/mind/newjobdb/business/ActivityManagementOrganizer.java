package com.mind.newjobdb.business;

import java.text.DecimalFormat;

import com.mind.bean.newjobdb.JobActivities;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.common.Util;


public class ActivityManagementOrganizer {
	
	public static void setFormFromJobActivities(JobActivities jobActivities,JobDashboardBean form) {
		
		DecimalFormat dfcur	= new DecimalFormat( "######0.00" );
		String[] activity_Id= new String[jobActivities.getActivities().size()];
		String[] activitylibrary_Id=new String[jobActivities.getActivities().size()];	
		String[] activityName = new String[jobActivities.getActivities().size()];
		String[] scope = new String[jobActivities.getActivities().size()];
		String[] uplifts = new String[jobActivities.getActivities().size()];
		String[] quantity = new String[jobActivities.getActivities().size()];
		String[] estimatedUnitcost = new String[jobActivities.getActivities().size()];
		String[] estimatedTotalCost = new String[jobActivities.getActivities().size()];
		String[] extendedUnitcost = new String[jobActivities.getActivities().size()];
		String[] extendedTotalCost = new String[jobActivities.getActivities().size()];
		String[] activity_oos_flag=new String[jobActivities.getActivities().size()];
		String[] activity_lx_ce_type=new String[jobActivities.getActivities().size()];
		String[] activityType = new String[jobActivities.getActivities().size()];
		
		for(int i=0;i<jobActivities.getActivities().size();i++)
		{
			activity_Id[i] = (String.valueOf(jobActivities.getActivities().get(i).getActivityId()));
			activityName[i] = (jobActivities.getActivities().get(i).getActivityName());
			scope[i] = (jobActivities.getActivities().get(i).getScope());
			uplifts[i] = ( String.valueOf(Util.checkNegative(dfcur.format(jobActivities.getActivities().get(i).getUplifts()))) );
			quantity[i] = ( String.valueOf(dfcur.format(jobActivities.getActivities().get(i).getQuantity())) );
			estimatedUnitcost[i] = String.valueOf( dfcur.format(jobActivities.getActivities().get(i).getEstimatedCostInfo().getUnit()) );
			estimatedTotalCost[i] = String.valueOf( dfcur.format(jobActivities.getActivities().get(i).getEstimatedCostInfo().getTotal()) );
			
			extendedUnitcost[i] = String.valueOf( dfcur.format(jobActivities.getActivities().get(i).getExtendedCostInfo().getUnit()) );
			extendedTotalCost[i] = String.valueOf( dfcur.format(jobActivities.getActivities().get(i).getExtendedCostInfo().getTotal()) );
			activitylibrary_Id[i]= jobActivities.getActivities().get(i).getActivityLibraryId();
			activity_oos_flag[i]= jobActivities.getActivities().get(i).getActivity_oos_flag();
			activity_lx_ce_type[i]= jobActivities.getActivities().get(i).getActivity_lx_ce_type();
			activityType[i] = jobActivities.getActivities().get(i).getActivityType();
		}
		form.setActivityType(activityType);
		form.setActivity_Id(activity_Id);
		form.setActivitylibrary_Id(activitylibrary_Id);
		form.setActivityName(activityName);
		form.setScope(scope);
		form.setUplifts(uplifts);
		form.setQuantity(quantity);
		form.setEstimatedUnitcost(estimatedUnitcost);
		form.setEstimatedTotalCost(estimatedTotalCost);
		form.setExtendedUnitcost(extendedUnitcost);
		form.setExtendedTotalCost(extendedTotalCost);
		form.setTotalQuantity( String.valueOf( dfcur.format(jobActivities.getTotalQuantity()) ) );
		form.setTotalEstimatedUnitCost( String.valueOf( dfcur.format(jobActivities.getTotalEstimatedUnitCost())) );
		form.setTotalEstimatedCost( String.valueOf( dfcur.format(jobActivities.getTotalEstimatedCost())) );
		form.setTotalExtendedUnitPrice( String.valueOf( dfcur.format(jobActivities.getTotalExtendedUnitPrice())) );
		form.setTotalExtendedPrice( String.valueOf( dfcur.format(jobActivities.getTotalExtendedPrice())) );
		form.setActivity_oos_flag(activity_oos_flag);
		form.setActivity_lx_ce_type(activity_lx_ce_type);
	}
	

}
