package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.newjobdb.dao.ExecuteTab;

public class ExecuteTabOrganizer {
	
	public static void setExecuteTabFromForm(ExecuteTab executeTab,JobDashboardBean form) {
		executeTab.setGeneralJobInfo(GeneralJobInfoOrganizer.getGeneralJobInformation(form));
		executeTab.setPurchaseOrderList(PurchaseOrderListOrganizer.getPurcahseOrderListObject(form));
	}
	
	public static void setFormFromExecuteTab(ExecuteTab executeTab,JobDashboardBean form)	{
		form.setAppendixId(executeTab.getAppendixId());
		form.setJobId(executeTab.getJobId());
		form.setTicketType(executeTab.getGeneralJobInfo().getRequestType());
		GeneralJobInfoOrganizer.setFormFromGeneralInfo(executeTab.getGeneralJobInfo(),form);
		JobWorkflowCheckListOrganizer.setFormFromJobWorkflowChecklist(executeTab.getTopPendingCheckListItems(), form);
		PurchaseOrderListOrganizer.setFormFromPurchaseOrderList(executeTab.getPurchaseOrderList(),form);
		if(executeTab.getTicketActivitySummary() != null){
			setTicketActivitySummary(executeTab,form);
		}
		
	}
	
	private static void setTicketActivitySummary(ExecuteTab executeTab,JobDashboardBean form){
		form.setTicketActivityId(executeTab.getTicketActivitySummary().getActivityId());
		form.setTotal_extended_price(executeTab.getTicketActivitySummary().getTotal_extended_price());
		form.setActual_cost(executeTab.getTicketActivitySummary().getActual_cost());
		form.setVgpm(executeTab.getTicketActivitySummary().getVgpm());
		form.setUnion_uplift_factor(executeTab.getTicketActivitySummary().getUnion_uplift_factor());
		form.setExpedite24_uplift_factor(executeTab.getTicketActivitySummary().getExpedite24_uplift_factor());
		form.setExpedite48_uplift_factor(executeTab.getTicketActivitySummary().getExpedite48_uplift_factor());
		form.setNon_pps_uplift_factor(executeTab.getTicketActivitySummary().getNon_pps_uplift_factor());
		form.setInternational_uplift_factor(executeTab.getTicketActivitySummary().getInternational_uplift_factor());
		form.setTicketResourceSummary(executeTab.getTicketActivitySummary().getTicketResourceSummary());
	}

}
