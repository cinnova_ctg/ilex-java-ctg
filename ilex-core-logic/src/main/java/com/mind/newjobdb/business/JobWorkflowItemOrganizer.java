package com.mind.newjobdb.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.mind.bean.newjobdb.ChecklistStatusType;
import com.mind.bean.newjobdb.JobWorkflowBean;
import com.mind.bean.newjobdb.JobWorkflowItem;
import com.mind.newjobdb.dao.JobWorkflowChecklist;

public class JobWorkflowItemOrganizer {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JobWorkflowItemOrganizer.class);
	
	public static ArrayList<JobWorkflowItem> getListJobWorkflowItems(JobWorkflowBean form)
	{
		if (logger.isDebugEnabled()) {
			logger.debug("getListJobWorkflowItems(JobWorkflowForm) - size od the tlist" + form.getItemId().length);
		}
		ArrayList <JobWorkflowItem> jobWorkflowItemList = new  ArrayList <JobWorkflowItem>();
		for(int i=0;i<form.getItemId().length;i++)
		{
			JobWorkflowItem jobWorkflowItem = new JobWorkflowItem();
			jobWorkflowItem.setItemId(new Long(form.getItemId()[i]).longValue());
			jobWorkflowItem.setItemStatus(form.getItemStatus()[i]);
			jobWorkflowItem.setItemStatusCode(new Integer(form.getItemStatusCode()[i]).intValue());
			jobWorkflowItem.setDescription(form.getDescription()[i]);
			jobWorkflowItem.setLastItemStatusCode(new Integer(form.getLastItemStatusCode()[i]).intValue());
			jobWorkflowItem.setProjectWorkflowItemId(new Long(form.getProjectWorkflowItemId()[i]).longValue());
			jobWorkflowItem.setReferenceDate(form.getReferenceDate()[i]);
			jobWorkflowItem.setSequence(form.getSequence()[i]);
			jobWorkflowItem.setUpdatedBy(form.getUpdatedBy()[i]);
			jobWorkflowItem.setUpdatedDate(form.getUpdatedDate()[i].trim().equalsIgnoreCase("")?new Date():new Date(form.getUpdatedDate()[i].trim()));
			jobWorkflowItem.setUser(form.getUser()[i]);
			jobWorkflowItem.setCreatedBy(form.getCreatedBy()[i]);
			jobWorkflowItem.setCreatedDate(new Date(form.getCreatedDate()[i]));
			jobWorkflowItem.setJobId(form.getJobId());
			jobWorkflowItemList.add(jobWorkflowItem);
		}
		
		return jobWorkflowItemList;
	}
	
	public static void setFormFromJobWorkflowItem(JobWorkflowChecklist jobWorkflowChecklist,JobWorkflowBean form)
	{
		 String[] itemId = new String[jobWorkflowChecklist.getJobWorkflowItems().size()];
		 String[] description = new String[jobWorkflowChecklist.getJobWorkflowItems().size()];
		 String[] sequence = new String[jobWorkflowChecklist.getJobWorkflowItems().size()];
		 String[] referenceDate = new String[jobWorkflowChecklist.getJobWorkflowItems().size()];
		 String[] user = new String[jobWorkflowChecklist.getJobWorkflowItems().size()];
		 String jobId;
		 String[] createdBy = new String[jobWorkflowChecklist.getJobWorkflowItems().size()];
		 String[] updatedBy = new String[jobWorkflowChecklist.getJobWorkflowItems().size()];
		 String[] createdDate = new String[jobWorkflowChecklist.getJobWorkflowItems().size()];
		 String[] updatedDate = new String[jobWorkflowChecklist.getJobWorkflowItems().size()];
		 String[] itemStatus =new String[jobWorkflowChecklist.getJobWorkflowItems().size()];
		 String[] itemStatusCode = new String[jobWorkflowChecklist.getJobWorkflowItems().size()];
		 String[] projectWorkflowItemId = new String[jobWorkflowChecklist.getJobWorkflowItems().size()];
		 ArrayList<JobWorkflowItem> completeItemList = new ArrayList<JobWorkflowItem>();
		 ArrayList<JobWorkflowItem> pendingItemList = new ArrayList<JobWorkflowItem>();
		 SimpleDateFormat sf = new SimpleDateFormat("MM/dd/yyyy");
		for(int i=0;i<jobWorkflowChecklist.getJobWorkflowItems().size();i++)
		{
			JobWorkflowItem jobWorkflowItem = jobWorkflowChecklist.getJobWorkflowItems().get(i);
			if(jobWorkflowItem.getItemStatusCode()!= ChecklistStatusType.PENDING_STATUS)
			{
				completeItemList.add(jobWorkflowItem);
			}
			else
				pendingItemList.add(jobWorkflowItem);
			itemId[i] = String.valueOf(jobWorkflowItem.getItemId());
			description[i] = jobWorkflowItem.getDescription();
			sequence[i] = jobWorkflowItem.getSequence();
			referenceDate[i] = jobWorkflowItem.getReferenceDate();
			user[i] = jobWorkflowItem.getUser();
			jobId = jobWorkflowItem.getJobId();
			createdBy[i] = jobWorkflowItem.getCreatedBy();
			createdDate[i] = jobWorkflowItem.getCreatedDate() != null ? sf.format(jobWorkflowItem.getCreatedDate()) : null;
			updatedBy[i] = jobWorkflowItem.getUpdatedBy();
			updatedDate[i] = jobWorkflowItem.getUpdatedDate() != null ?sf.format(jobWorkflowItem.getUpdatedDate()) : null;
			itemStatus[i] = jobWorkflowItem.getItemStatus();
			itemStatusCode[i] = String.valueOf(jobWorkflowItem.getItemStatusCode());
			projectWorkflowItemId[i] = String.valueOf(jobWorkflowItem.getProjectWorkflowItemId());
		}
		form.setCompleteItemList(completeItemList);
		form.setCompleteItemListSize(completeItemList.size());
		ArrayList<JobWorkflowItem> pendingList = new ArrayList<JobWorkflowItem>();
		for(int i=0;i<pendingItemList.size();i++)
		{
			if(i==0){
				 ArrayList<JobWorkflowItem> firstPenndingList = new ArrayList<JobWorkflowItem>();
				 firstPenndingList.add(pendingItemList.get(i));
				form.setFirstPenndingList(firstPenndingList);
				form.setFirstPenndingListSize(firstPenndingList.size());
			}
			else{
					pendingList.add(pendingItemList.get(i));
					form.setPendingList(pendingList);
					form.setPendingListSize(pendingList.size());
			}
				
		}
		form.setItemId(itemId);
		form.setDescription(description);
		form.setSequence(sequence);
		form.setReferenceDate(referenceDate);
		form.setUser(user);
		form.setCreatedBy(createdBy);
		form.setCreatedDate(createdDate);
		form.setUpdatedBy(updatedBy);
		form.setUpdatedDate(updatedDate);
		form.setItemStatus(itemStatus);
		form.setItemStatusCode(itemStatusCode);
		form.setProjectWorkflowItemId(projectWorkflowItemId);
	}
}
