package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.FinancialAnalysis;
import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobDashboardTab;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.newjobdb.dao.PurchaseOrderList;



/**
 * @purpose This class is used for the representation of 
 * 			the Financial tab.
 * 
 * @ActionsAllowed from Financial tab:
 * 		All POs will be displayed on the Financial tab, but only 
 * 		POs in the draft status will have applicable actions 
 * 		which can be initiated from the Financial tab.
 * 		
 * 		The Financial tab is used to manage the POs which are in the draft state.  
 * 		Thus as POs are created based on the copy or build functions, 
 * 		they appear on the Financial tab with Actions possible on Draft state.
 */
public class FinancialTab extends JobDashboardTab {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appendixId;
	private String jobId;
	private PurchaseOrderList purchaseOrderList;
	private FinancialAnalysis financialAnalysis;

	/**
	 * The Financial tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	private GeneralJobInfo generalJobInfo;
	

	public FinancialTab() {
		tabId = JobDashboardTabType.FINANCIAL_TAB;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	/**
	 * The Financial tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public GeneralJobInfo getGeneralJobInfo() {
		return generalJobInfo;
	}

	/**
	 * The Financial tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public void setGeneralJobInfo(GeneralJobInfo generalJobInfo) {
		this.generalJobInfo = generalJobInfo;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public PurchaseOrderList getPurchaseOrderList() {
		return purchaseOrderList;
	}

	public void setPurchaseOrderList(PurchaseOrderList purchaseOrderList) {
		this.purchaseOrderList = purchaseOrderList;
	}

	public FinancialAnalysis getFinancialAnalysis() {
		return financialAnalysis;
	}

	public void setFinancialAnalysis(FinancialAnalysis financialAnalysis) {
		this.financialAnalysis = financialAnalysis;
	}

}
