package com.mind.newjobdb.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.newjobdb.PlanningNote;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * @purpose This class is used for the representation of the Project level
 *          planning note.
 */
public class ProjectPlanningNote extends PlanningNote {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ProjectPlanningNote.class);

	public static void copyToJob(DataSource ds, String appendixId,
			String jobId, String userId) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			Timestamp ts = new java.sql.Timestamp(
					new java.util.Date().getTime());
			conn = ds.getConnection();
			String sql = "select * from dbo.prj_lvl_job_notes where appendix_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, appendixId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				sql = "insert into job_lvl_job_notes (job_id,prj_lvl_note_id,notes,created_by,created_date)"
						+ "\n (select ?,note_id,notes,?,? from dbo.prj_lvl_job_notes where appendix_id =?)";
				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, jobId);
				pStmt.setString(2, userId);
				pStmt.setTimestamp(3, ts);
				pStmt.setString(4, appendixId);
				pStmt.executeUpdate();
			}

		} catch (Exception e) {
			logger.error("copyToJob(DataSource, String, String, String)", e);

			logger.error("copyToJob(DataSource, String, String, String)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}
}
