package com.mind.newjobdb.business;

import javax.sql.DataSource;

import com.mind.newjobdb.dao.JobDAO;




/**
 * @purpose This class is used to represent 
 * 			the job's buy related comments
 * 
 * The comments section is a quick method for entering and viewing 
 * job level comments as currently implemented. This will be implemented 
 * as an Ajax action and will not refresh the whole page. 
 */
public class JobBuyComments {
	private String comments;
	
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public static void addComments(String comments, String Job_Id, String loginuserid, DataSource ds ) {
		JobDAO.addJobComments(comments, Job_Id, loginuserid, ds);
	}
}
