package com.mind.newjobdb.business;

import com.mind.bean.mpo.PODTO;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.common.POStatusTypes;
import com.mind.newjobdb.dao.POGeneralInfo;

public class POGeneralInfoOrganizer {

	public static void setFormFromPOGeneralInfo(POGeneralInfo poGeneralInfo,
			PODTO poBean) {
		poBean.setPoNumber(poGeneralInfo.getPoNumber());
		poBean.setPoStatus(POStatusTypes.getStatusName(poGeneralInfo
				.getPoStatus()));
		poBean.setPoStatusId(new Integer(poGeneralInfo.getPoStatus())
				.toString());
		poBean.setPoType(poGeneralInfo.getPoType());
		poBean.setMasterPOItem(poGeneralInfo.getMasterPOItem());
		poBean.setMasterPOItemDesc(poGeneralInfo.getMasterPOItemDesc());
		poBean.setTerms(poGeneralInfo.getTerms());
		poBean.setTermsDesc(poGeneralInfo.getTermsDesc());
		poBean.setRevisionLevel(String.valueOf(poGeneralInfo.getRevisionLevel()));
		poBean.setDeliveryDate(poGeneralInfo.getGeneralPOSetup()
				.getDeliverByDate().getDate());
		poBean.setDeliveryHour(poGeneralInfo.getGeneralPOSetup()
				.getDeliverByDate().getHour());
		poBean.setDeliveryMinute(poGeneralInfo.getGeneralPOSetup()
				.getDeliverByDate().getMinute());
		poBean.setDeliveryIsAm(IlexTimestamp.getStringAMPM(poGeneralInfo
				.getGeneralPOSetup().getDeliverByDate().isAM()));
		poBean.setIssueDate(poGeneralInfo.getGeneralPOSetup().getIssueDate()
				.getDate());
		poBean.setIssueHour(poGeneralInfo.getGeneralPOSetup().getIssueDate()
				.getHour());
		poBean.setIssueMinute(poGeneralInfo.getGeneralPOSetup().getIssueDate()
				.getMinute());
		poBean.setIssueIsAm(IlexTimestamp.getStringAMPM(poGeneralInfo
				.getGeneralPOSetup().getIssueDate().isAM()));
		poBean.setPartner(poGeneralInfo.getPartner());
		poBean.setContact(poGeneralInfo.getContact());
		poBean.setContactPoc(poGeneralInfo.getContactPoc());
		poBean.setAuthorizedPaymentMethod(poGeneralInfo
				.getAuthorizedPaymentMethod());
		poBean.setParentTabType(poGeneralInfo.getParentTabType());

		poBean.setPvsJustification(poGeneralInfo.getPvsJustification());
		poBean.setGeographicConstraint(poGeneralInfo.getGeographicConstraint());
		poBean.setCmboxCoreCompetency(poGeneralInfo.getCmboxCoreCompetency());
	}

	public static POGeneralInfo setPOGeneralInfoFromForm(PODTO poBean) {

		POGeneralInfo poGeneralInfo = new POGeneralInfo();
		poGeneralInfo.setPoNumber(poBean.getPoNumber());
		poGeneralInfo.setPoStatus(0);
		poGeneralInfo.setPoType(poBean.getPoType());
		poGeneralInfo.setMasterPOItem(poBean.getMasterPOItem());
		poGeneralInfo.setTerms(poBean.getTerms());
		poGeneralInfo.setRevisionLevel(poBean.getRevisionLevel());
		poGeneralInfo.setPartner(poBean.getPartner());
		poGeneralInfo.setContact(poBean.getContact());
		poGeneralInfo.setContactPoc(poBean.getContactPoc());
		poGeneralInfo.setAuthorizedPaymentMethod(poBean
				.getAuthorizedPaymentMethod());
		poGeneralInfo.setParentTabType(poBean.getParentTabType());
		poGeneralInfo.setGeneralPOSetup(CopyMPOOrganizer
				.setGeneralPOSetupFromForm(poBean));
		poGeneralInfo.setPvsJustification(poBean.getPvsJustification());
		poGeneralInfo.setGeographicConstraint(poBean.getGeographicConstraint());
		poGeneralInfo.setCmboxCoreCompetency(poBean.getCmboxCoreCompetency());
		return poGeneralInfo;
	}
}
