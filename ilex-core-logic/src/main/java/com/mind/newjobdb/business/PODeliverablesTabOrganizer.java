package com.mind.newjobdb.business;

import com.mind.bean.mpo.PODTO;
import com.mind.newjobdb.dao.PODeliverables;

public class PODeliverablesTabOrganizer {
	
	public static void setFormFromPODeliverables(PODeliverables poDeliverables,PODTO poBean)
	{
		POGeneralInfoOrganizer.setFormFromPOGeneralInfo(poDeliverables.getPoGeneralInfo(), poBean);
		PODeliverableOrganizer.setFormFromPODeliverable(poDeliverables.getPoDeliverable(),poBean);
	}
	public static PODeliverables setPODocumentManagerFromForm(PODTO poBean)
	{
		PODeliverables poDeliverables = new PODeliverables ();
		poDeliverables.setPoGeneralInfo(POGeneralInfoOrganizer.setPOGeneralInfoFromForm(poBean));
		poDeliverables.setPoDeliverable(PODeliverableOrganizer.setPODeliverableFromForm(poBean));
		return poDeliverables;
	}

}
