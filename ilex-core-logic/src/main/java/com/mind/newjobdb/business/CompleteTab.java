package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobDashboardTab;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.newjobdb.dao.JobWorkflowChecklist;
import com.mind.newjobdb.dao.PurchaseOrderList;



/**
 * @purpose This class is used for the representation of 
 * 			the Complete tab.
 * 
 * @ActionsAllowed from Complete tab:
 * 		All POs will be displayed on the Complete tab, but only 
 * 		POs in the draft status will have applicable actions 
 * 		which can be initiated from the Complete tab.
 * 		
 * 		The Complete tab is used to manage the POs which are in the draft state.  
 * 		Thus as POs are created based on the copy or build functions, 
 * 		they appear on the Complete tab with Actions possible on Draft state.
 */
public class CompleteTab extends JobDashboardTab {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appendixId;
	private String jobId;
	private JobWorkflowChecklist topPendingCheckListItems;
	private PurchaseOrderList purchaseOrderList;

	/**
	 * The Complete tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	private GeneralJobInfo generalJobInfo;
	

	public CompleteTab() {
		tabId = JobDashboardTabType.COMPLETE_TAB;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	/**
	 * The Complete tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public GeneralJobInfo getGeneralJobInfo() {
		return generalJobInfo;
	}

	/**
	 * The Complete tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public void setGeneralJobInfo(GeneralJobInfo generalJobInfo) {
		this.generalJobInfo = generalJobInfo;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public JobWorkflowChecklist getTopPendingCheckListItems() {
		return topPendingCheckListItems;
	}

	public void setTopPendingCheckListItems(
			JobWorkflowChecklist topPendingCheckListItems) {
		this.topPendingCheckListItems = topPendingCheckListItems;
	}

	public PurchaseOrderList getPurchaseOrderList() {
		return purchaseOrderList;
	}

	public void setPurchaseOrderList(PurchaseOrderList purchaseOrderList) {
		this.purchaseOrderList = purchaseOrderList;
	}

}
