package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.DispatchScheduleTab;
import com.mind.bean.newjobdb.JobDashboardBean;

public class DispatchScheduleTabOrganizer {
	public static void setFormFromDispatchScheduleTab(DispatchScheduleTab dispatchScheduleTab,JobDashboardBean form)	{
		form.setAppendixId(dispatchScheduleTab.getAppendixId());
		form.setJobId(dispatchScheduleTab.getJobId());
		GeneralJobInfoOrganizer.setFormFromGeneralInfo(dispatchScheduleTab.getGeneralJobInfo(),form);
	}

}
