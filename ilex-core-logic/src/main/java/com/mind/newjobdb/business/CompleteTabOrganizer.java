package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.JobDashboardBean;

public class CompleteTabOrganizer {
	
	public static void setCompleteTabFromForm(CompleteTab completeTab,JobDashboardBean form) {
		completeTab.setGeneralJobInfo(GeneralJobInfoOrganizer.getGeneralJobInformation(form));
		completeTab.setPurchaseOrderList(PurchaseOrderListOrganizer.getPurcahseOrderListObject(form));
	}
	
	public static void setFormFromCompleteTab(CompleteTab completeTab,JobDashboardBean form)	{
		form.setAppendixId(completeTab.getAppendixId());
		form.setJobId(completeTab.getJobId());
		GeneralJobInfoOrganizer.setFormFromGeneralInfo(completeTab.getGeneralJobInfo(),form);
		JobWorkflowCheckListOrganizer.setFormFromJobWorkflowChecklist(completeTab.getTopPendingCheckListItems(), form);
		PurchaseOrderListOrganizer.setFormFromPurchaseOrderList(completeTab.getPurchaseOrderList(),form);
		
	}

}
