package com.mind.newjobdb.business;

import java.util.ArrayList;
import java.util.List;

import com.mind.bean.newjobdb.TicketDepotProduct;
import com.mind.bean.newjobdb.TicketDepotProductRequest;
import com.mind.bean.newjobdb.TicketDepotRequisition;
import com.mind.bean.prm.DepotRequisitionBean;

public class TicketDepotOrganizer {

	
	public static TicketDepotRequisition getTicketDepotRequisition(DepotRequisitionBean form)
	{
		TicketDepotRequisition ticketDepotRequisition = new TicketDepotRequisition();
		ArrayList listOfTDPR = (ArrayList)getListOfTicketDepotProductRequest(form);
		ticketDepotRequisition.setProductRequests(listOfTDPR);	
		return ticketDepotRequisition;
	}
	private static List getListOfTicketDepotProductRequest(DepotRequisitionBean form)
	{
		 List listOfTDPR= new ArrayList() ;
		for(int i=0;i<form.getDe_product_id().length;i++)
		{
			TicketDepotProductRequest ticketDepotProductRequest = new TicketDepotProductRequest();
			ticketDepotProductRequest.setDepotRequestId(new Long(form.getDp_cat_id()).longValue());
			ticketDepotProductRequest.setQuantity(new Float(form.getDepot_quantity()[i]).floatValue());
			TicketDepotProduct ticketDepotProduct = new TicketDepotProduct();
			ticketDepotProduct.setProductCategory(form.getDp_cat_name());
			ticketDepotProduct.setProductId(new Long(form.getDe_product_id()[i]).longValue());
			ticketDepotProduct.setProductName(form.getDp_prod_name());
			ticketDepotProductRequest.setProduct(ticketDepotProduct);
			listOfTDPR.add(ticketDepotProductRequest);
		}
		return listOfTDPR;
	}
	public static void setFormFromTDPR(ArrayList<TicketDepotProductRequest> listOfTDPR,DepotRequisitionBean form)
	{
		String depot_quantity[] = new String[listOfTDPR.size()];
		String de_product_id[] = new String [listOfTDPR.size()];
		//String dp_cat_name[] = new String [listOfTDPR.size()];
		//String dp_prod_name[] = new String [listOfTDPR.size()];
		for(int i=0;i<listOfTDPR.size();i++)
		{
			TicketDepotProductRequest ticketDepotProductRequest = listOfTDPR.get(i);
			depot_quantity[i] =String.valueOf(ticketDepotProductRequest.getQuantity());
			TicketDepotProduct ticketDepotProduct = ticketDepotProductRequest.getProduct();
			de_product_id[i] = String.valueOf(ticketDepotProduct.getProductId());
			form.setDp_cat_name(ticketDepotProduct.getProductCategory());
			form.setDp_prod_name(ticketDepotProduct.getProductName());
			//dp_cat_name[i] = ticketDepotProduct.getProductCategory();
			//dp_prod_name[i] = ticketDepotProduct.getProductName();
		}
		form.setDepot_quantity(depot_quantity);
		
	}
}
