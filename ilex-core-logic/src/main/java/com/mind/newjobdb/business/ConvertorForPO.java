package com.mind.newjobdb.business;

import com.mind.bean.mpo.PODTO;
import com.mind.newjobdb.dao.PurchaseOrder;

public class ConvertorForPO {

	public static void setFormFromPurchaseOrder(PurchaseOrder purchaseOrder,PODTO poBean)
	{
		if(purchaseOrder.getPoAuthorizedCostInfo()!=null){
			AuthorizedTabOrganizer.setFormFromAuthorizedTab(purchaseOrder.getPoAuthorizedCostInfo(),poBean);
		}			
		POWOTabOrganizer.setFormFromPOWOTab(purchaseOrder.getPowoInfo(),poBean);
		
		if(purchaseOrder.getEInvoice() != null){
			POEInvoiceOrganizer.setFormFromEInvoiceInfo(purchaseOrder.getEInvoice(), poBean);
		}
		if(purchaseOrder.getPartnerInfo() != null){
			POWOTabOrganizer.setFormFromPartnerInfo(purchaseOrder.getPartnerInfo(), poBean);
		}
		if(purchaseOrder.getSiteInfo() != null){
			POWOTabOrganizer.setFormFromSiteInfo(purchaseOrder.getSiteInfo(), poBean);
		}
//		DocumentManagementTabOrganizer.setFormFromPODocumentManager(purchaseOrder.getPoDocumentManager(),poBean);
//		PODeliverablesTabOrganizer.setFormFromPODeliverables(purchaseOrder.getPoDeliverables(),poBean);
	}
	public static void setPurchaseOrderFromForm(PODTO poBean)
	{
		PurchaseOrder purchaseOrder = new PurchaseOrder ();
		purchaseOrder.setPoAuthorizedCostInfo(AuthorizedTabOrganizer.setAuthorizedTabFromForm(poBean));
		purchaseOrder.setPowoInfo(POWOTabOrganizer.setPOWOTabFromForm(poBean));
		purchaseOrder.setPoDocumentManager(DocumentManagementTabOrganizer.setPODocumentManagerFromForm(poBean));
		purchaseOrder.setPoDeliverables(PODeliverablesTabOrganizer.setPODocumentManagerFromForm(poBean));
	}
}
