package com.mind.newjobdb.business;


import com.mind.bean.mpo.PODTO;
import com.mind.bean.newjobdb.POActivityManager;
import com.mind.common.Util;

public class POActivityManagerOrganizer {
	
	public static void setFormFromPOActivityManager(POActivityManager poActivityManager,PODTO poBean)
	{
		poBean.setDeltaCost(String.valueOf(poActivityManager.getDeltaCost()));
		poBean.setMpoTotalCost(String.valueOf(poActivityManager.getMpoTotalCost()));
		poBean.setEstimatedCost(String.valueOf(poActivityManager.getEstimatedTotalCost()));
		poBean.setAuthorizedTotalCost(String.valueOf(poActivityManager.getAuthorizedTotalCost()));
		POResourceOrganizer.setFormFromPOResource(poActivityManager.getPoResources(),poBean);
		poBean.setAuthorizedCostInput(String.valueOf(poActivityManager.getAuthorizedCostInput()));
	}
	public static POActivityManager setPOActivityManagerFromForm(PODTO poBean)
	{
		POActivityManager poActivityManager = new POActivityManager();
		poActivityManager.setEstimatedTotalCost(Util.getFloatValue(poBean.getEstimatedCost()));
		poActivityManager.setAuthorizedTotalCost(Util.getFloatValue(poBean.getAuthorizedTotalCost()));
		poActivityManager.setDeltaCost(Util.getFloatValue(poBean.getDeltaCost()));
		poActivityManager.setAuthorizedCostInput(Util.getFloatValue(poBean.getAuthorizedCostInput()));
		float mpoTotalCost = 0;
		mpoTotalCost = Util.getFloatValue(poBean.getMpoTotalCost());
		poActivityManager.setMpoTotalCost(mpoTotalCost);
		poActivityManager.setPoResources(POResourceOrganizer.setPOResourceFromForm(poBean));
		return poActivityManager;
	}
	
}
