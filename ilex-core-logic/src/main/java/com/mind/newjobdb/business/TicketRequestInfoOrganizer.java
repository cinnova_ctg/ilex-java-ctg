package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.TicketRequestInfo;
import com.mind.common.Util;

public class TicketRequestInfoOrganizer {
	
	public static void setFormFromTicketRequestInfo(TicketRequestInfo ticketRequestInfo,JobDashboardBean form) {
		form.setRequestor(ticketRequestInfo.getRequestor());
		form.setRequestorEmail(ticketRequestInfo.getRequestorEmail());
		form.setRequestType(ticketRequestInfo.getRequestType());
		form.setResource(ticketRequestInfo.getResource());
		form.setCriticality(ticketRequestInfo.getCriticality());
		form.setCustomerReference(ticketRequestInfo.getCustomerReference());
		form.setMsp(Util.getStringval(ticketRequestInfo.isMSP()));
		form.setPps(Util.getStringval(ticketRequestInfo.isPPS()));
		form.setStandBy(Util.getStringval(ticketRequestInfo.isStandBy()));
		form.setHelpDesk(Util.getStringval(ticketRequestInfo.isHelpDesk()));
		
		form.setSite(ticketRequestInfo.getSite());
		
	}
	
	public static TicketRequestInfo getTicketRequestInfo(JobDashboardBean form) {
		TicketRequestInfo ticketRequestInfo = new TicketRequestInfo();
		ticketRequestInfo.setCriticality(form.getCriticality());
		ticketRequestInfo.setCustomerReference(form.getCustomerReference());
		ticketRequestInfo.setRequestor(form.getRequestor());
		ticketRequestInfo.setRequestorEmail(form.getRequestorEmail());
		ticketRequestInfo.setRequestType(form.getRequestType());
		ticketRequestInfo.setResource(form.getResource());
		ticketRequestInfo.setSite(form.getSitelistid());
		ticketRequestInfo.setHelpDesk(Util.getBooleanval(form.getHelpDesk()));
		ticketRequestInfo.setMSP(Util.getBooleanval(form.getMsp()));
		ticketRequestInfo.setPPS(Util.getBooleanval(form.getPps()));
		ticketRequestInfo.setStandBy(Util.getBooleanval(form.getStandBy()));
		return ticketRequestInfo;
	}
	
}
