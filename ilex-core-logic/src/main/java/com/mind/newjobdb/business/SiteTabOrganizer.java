package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.pm.SiteBean;

public class SiteTabOrganizer {
	
	public static void setSiteTabFromForm(SiteTab siteTab,JobDashboardBean form) {
		siteTab.setGeneralJobInfo(GeneralJobInfoOrganizer.getGeneralJobInformation(form));
		SiteBean siteForm = new SiteBean();
		siteForm.setSite_Id(form.getSite_id());
		siteForm.setSite_number(form.getSite_number());
		siteForm.setSite_name(form.getSite_name());
		
		siteForm.setSite_address(form.getSite_address());
		siteForm.setSite_city(form.getSite_city());
		siteForm.setState(form.getSite_state());
		siteForm.setCountry(form.getSite_country());
		siteForm.setSite_zipcode(form.getSite_zipcode());
		//Brand ?? form.setSite
		siteForm.setSitegroup(form.getSitegroup());
		siteForm.setSiteendcustomer(form.getSiteendcustomer());
		siteForm.setSite_notes(form.getSite_notes());
		siteForm.setSite_worklocation(form.getSite_worklocation());		
		siteForm.setSite_direction(form.getSite_direction());
		siteForm.setSitetimezone(form.getSitetimezone());
		siteForm.setLocality_uplift(form.getLocality_uplift());
		siteForm.setUnion_site(form.getUnion_site());
		siteForm.setPrimary_Name(form.getPrimary_Name());
		siteForm.setPrimary_email(form.getPrimary_email());
		siteForm.setSite_phone(form.getSite_phone());
		siteForm.setSecondary_Name(form.getSecondary_Name());
		siteForm.setSecondary_email(form.getSecondary_email());
		siteForm.setSecondary_phone(form.getSecondary_phone());	
		
		siteForm.setSite_designator(form.getSite_designator());
		siteForm.setId1(form.getAppendixId());
		siteForm.setId2(form.getJobId());
		siteForm.setSiteregion(form.getSiteregion());
		siteForm.setSitecategory(form.getSitecategory());
		siteForm.setSitelistid(form.getSitelistid());
		siteForm.setSite_designator(form.getSite_designator());		
		
		siteForm.setSitelatdeg(form.getSitelatdeg());
		siteForm.setSitelatmin(form.getSitelatmin());
		siteForm.setSitelatdirection(form.getSitelatdirection());
		siteForm.setSitelondeg(form.getSitelondeg());
		siteForm.setSitelonmin(form.getSitelonmin());
		siteForm.setSitelondirection(form.getSitelondirection());
		siteForm.setSitestatus(form.getSitestatus());	
		
		siteTab.setSiteForm(siteForm);
	}
	
	public static void setFormFromExecuteTab(SiteTab siteTab,JobDashboardBean form)	{
		form.setAppendixId(siteTab.getAppendixId());
		form.setJobId(siteTab.getJobId());
		GeneralJobInfoOrganizer.setFormFromGeneralInfo(siteTab.getGeneralJobInfo(),form);	
	}
	
	public static void setFormFromSiteTab(SiteTab siteTab,JobDashboardBean form) {
		form.setAppendixId(siteTab.getAppendixId());
		form.setJobId(siteTab.getJobId());
		GeneralJobInfoOrganizer.setFormFromGeneralInfo(siteTab.getGeneralJobInfo(),form);
		
		SiteBean siteForm = siteTab.getSiteForm();
		form.setSite_id(siteForm.getSite_Id());
		form.setSite_number(siteForm.getSite_number());
		form.setSite_name(siteForm.getSite_name());
		
		form.setSite_address(siteForm.getSite_address());
		form.setSite_city(siteForm.getSite_city());
		form.setSite_state(siteForm.getState());
		form.setSite_country(siteForm.getCountry());
		form.setSite_zipcode(siteForm.getSite_zipcode());
		//Brand ?? form.setSite
		form.setSitegroup(siteForm.getSitegroup());
		form.setSiteendcustomer(siteForm.getSiteendcustomer());
		form.setSite_notes(siteForm.getSite_notes());
		form.setSite_worklocation(siteForm.getSite_worklocation());		
		form.setSite_direction(siteForm.getSite_direction());
		form.setSitetimezone(siteForm.getSitetimezone());
		form.setLocality_uplift(siteForm.getLocality_uplift());
		form.setUnion_site(siteForm.getUnion_site());
		form.setPrimary_Name(siteForm.getPrimary_Name());
		form.setPrimary_email(siteForm.getPrimary_email());
		form.setSite_phone(siteForm.getSite_phone());
		form.setSecondary_Name(siteForm.getSecondary_Name());
		form.setSecondary_email(siteForm.getSecondary_email());
		form.setSecondary_phone(siteForm.getSecondary_phone());	
		
		form.setSite_designator(siteForm.getSite_designator());
		form.setId1(siteForm.getId1());
		form.setId2(siteForm.getId2());
		form.setSiteregion(siteForm.getSiteregion());
		form.setSitecategory(siteForm.getSitecategory());
		form.setSitelistid(siteForm.getSitelistid());
		form.setSite_designator(siteForm.getSite_designator());	
		
		form.setSitelatdeg(siteForm.getSitelatdeg());
		form.setSitelatmin(siteForm.getSitelatmin());
		form.setSitelatdirection(siteForm.getSitelatdirection());
		form.setSitelondeg(siteForm.getSitelondeg());
		form.setSitelonmin(siteForm.getSitelonmin());
		form.setSitelondirection(siteForm.getSitelondirection());
		form.setSitestatus(siteForm.getSitestatus());
	
	}
	

}
