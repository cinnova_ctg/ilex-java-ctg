package com.mind.newjobdb.business;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.mind.bean.newjobdb.ChecklistStatusType;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobWorkflowBean;
import com.mind.bean.newjobdb.JobWorkflowItem;
import com.mind.newjobdb.dao.JobWorkflowChecklist;

public class JobWorkflowCheckListOrganizer {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JobWorkflowCheckListOrganizer.class);

	public static JobWorkflowChecklist getJobWorkflowChecklist(JobDashboardBean form) {
		JobWorkflowChecklist jobWorkflowChecklist = new JobWorkflowChecklist();
		//jobWorkflowChecklist.setJobWorkflowItems(form.getJobWorkflowChecklist().getJobWorkflowItems());	
		return jobWorkflowChecklist;
	}
	public static JobWorkflowChecklist getJobWorkflowChecklist(JobWorkflowBean form) {
		JobWorkflowChecklist jobWorkflowChecklist = new JobWorkflowChecklist();
		jobWorkflowChecklist.setJobWorkflowItems(JobWorkflowItemOrganizer.getListJobWorkflowItems(form));	
		return jobWorkflowChecklist;
	}

	public static void setFormFromJobWorkflowChecklist(JobWorkflowChecklist jobWorkflowChecklist,JobDashboardBean form) {
		if (logger.isDebugEnabled()) {
			logger.debug("setFormFromJobWorkflowChecklist(JobWorkflowChecklist, JobDashboardForm) - called12345" + jobWorkflowChecklist.getTopPendingItems(2).getJobWorkflowItems());
		}
		ArrayList<JobWorkflowItem> jobWorkflowItems = (jobWorkflowChecklist.getTopPendingItems(2)).getJobWorkflowItems();
		Iterator iter = jobWorkflowItems.iterator();		
		String[] jobWorkflowItemDescription = new String[jobWorkflowItems.size()];
		long[] jobWorkflowItemId = new long[jobWorkflowItems.size()];
		String[] jobWorkflowItemStatus = new String[jobWorkflowItems.size()];		
		String[] jobWorkflowItemSequence = new String[jobWorkflowItems.size()];
		String[] jobWorkflowItemReferenceDate = new String[jobWorkflowItems.size()];
		String[] jobWorkflowItemUser = new String[jobWorkflowItems.size()];
		
		int i = 0;
		while (iter.hasNext()){			
			JobWorkflowItem jobWorkflowItem = (JobWorkflowItem)iter.next();
			jobWorkflowItemDescription[i] = jobWorkflowItem.getDescription();
			jobWorkflowItemId[i] = jobWorkflowItem.getItemId();
			if(jobWorkflowItem.getItemId()>0) {
				jobWorkflowItemStatus[i] = ChecklistStatusType.getStatusName(jobWorkflowItem.getItemStatusCode());
			} else {
				jobWorkflowItemStatus[i]="";
			}
			jobWorkflowItemSequence[i] = jobWorkflowItem.getSequence();
			jobWorkflowItemReferenceDate[i] = jobWorkflowItem.getReferenceDate();
			jobWorkflowItemUser[i] = jobWorkflowItem.getUser();
			i++;	
		}
		
		
		form.setJobWorkflowItemDescription(jobWorkflowItemDescription);
		form.setJobWorkflowItemId(jobWorkflowItemId);
		form.setJobWorkflowItemStatus(jobWorkflowItemStatus);
		form.setJobWorkflowItemSequence(jobWorkflowItemSequence);
		form.setJobWorkflowItemReferenceDate(jobWorkflowItemReferenceDate);
		form.setJobWorkflowItemUser(jobWorkflowItemUser);
		form.setAllItemStatus(jobWorkflowChecklist.getAllItemStatus());
		form.setItemListSize(jobWorkflowChecklist.getItemListSize());
		if (logger.isDebugEnabled()) {
			logger.debug("setFormFromJobWorkflowChecklist(JobWorkflowChecklist, JobDashboardForm) - list size---------" + form.getItemListSize());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("setFormFromJobWorkflowChecklist(JobWorkflowChecklist, JobDashboardForm) - list size---------" + jobWorkflowChecklist.getItemListSize());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("setFormFromJobWorkflowChecklist(JobWorkflowChecklist, JobDashboardForm) - jobWorkflowItems.size()::" + jobWorkflowItems.size());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("setFormFromJobWorkflowChecklist(JobWorkflowChecklist, JobDashboardForm) - form.getJobWorkflowItemDescription()[0]: " + form.getJobWorkflowItemDescription().length);
		}
	}
	public static void setFormFromJobWorkflowChecklist(JobWorkflowChecklist jobWorkflowChecklist,JobWorkflowBean form) {
		
		JobWorkflowItemOrganizer.setFormFromJobWorkflowItem( jobWorkflowChecklist, form);
	}
}
