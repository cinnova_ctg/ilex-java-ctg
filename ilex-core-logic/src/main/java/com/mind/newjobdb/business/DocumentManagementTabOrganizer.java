package com.mind.newjobdb.business;

import com.mind.bean.mpo.PODTO;
import com.mind.newjobdb.dao.PODocumentManager;

public class DocumentManagementTabOrganizer {
	
	public static void setFormFromPODocumentManager(PODocumentManager poDocumentManager,PODTO poBean)
	{
		POGeneralInfoOrganizer.setFormFromPOGeneralInfo(poDocumentManager.getPoGeneralInfo(), poBean);
		PODocumentOrganizer.setFormFromPODocument(poDocumentManager.getPoDocument(),poBean);
	}
	public static PODocumentManager setPODocumentManagerFromForm(PODTO poBean)
	{
		PODocumentManager poDocumentManager = new PODocumentManager ();
		poDocumentManager.setPoGeneralInfo(POGeneralInfoOrganizer.setPOGeneralInfoFromForm(poBean));
		poDocumentManager.setPoDocument(PODocumentOrganizer.setPODocumentFromForm(poBean));
		return poDocumentManager;
	}

}
