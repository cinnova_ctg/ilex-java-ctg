package com.mind.newjobdb.business;

import com.mind.bean.newjobdb.JobDashboardBean;



public class SetupTabOrganizer {
	
	public static void setSetupTabFromForm(SetupTab setupTab,JobDashboardBean form) {
		setupTab.setGeneralJobInfo(GeneralJobInfoOrganizer.getGeneralJobInformation(form));
		setupTab.setJobPlanningNote(JobPlanningNoteOrganizer.getJobPlanningNote(form));
	}
	
	public static void setFormFromSetupTab(SetupTab setUpTab,JobDashboardBean form)	{
		GeneralJobInfoOrganizer.setFormFromGeneralInfo(setUpTab.getGeneralJobInfo(),form);
		JobPlanningNoteOrganizer.setFormFromJobPlanningNote(setUpTab.getJobPlanningNote(), form);
		ActivityManagementOrganizer.setFormFromJobActivities(setUpTab.getJobActivities(),form);
		JobWorkflowCheckListOrganizer.setFormFromJobWorkflowChecklist(setUpTab.getTopPendingCheckListItems(), form);
	}

}
