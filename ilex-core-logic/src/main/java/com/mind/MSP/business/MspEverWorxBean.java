package com.mind.MSP.business;

import java.util.ArrayList;
import java.util.TreeMap;

public class MspEverWorxBean {
	
	private ArrayList<TreeMap> everWorxList= new ArrayList<TreeMap>();

	public ArrayList<TreeMap> getEverWorxList() {
		return everWorxList;
	}

	public void setEverWorxList(ArrayList<TreeMap> everWorxList) {
		this.everWorxList = everWorxList;
	}

	
}
