package com.mind.MSP.business;

import java.util.ArrayList;

import com.mind.bean.msp.MonitoringSummary;

/**
 * The Class MonitoringSummaryBean.
 */
public class MonitoringSummaryBean {

	/** The monitoring summary list. */
	ArrayList<MonitoringSummary> monitoringSummaryList = new ArrayList<MonitoringSummary>();

	/**
	 * Gets the monitoring summary list.
	 * 
	 * @return the monitoring summary list
	 */
	public ArrayList<MonitoringSummary> getMonitoringSummaryList() {
		return monitoringSummaryList;
	}

	/**
	 * Sets the monitoring summary list.
	 * 
	 * @param monitoringSummaryList
	 *            the new monitoring summary list
	 */
	public void setMonitoringSummaryList(
			ArrayList<MonitoringSummary> monitoringSummaryList) {
		this.monitoringSummaryList = monitoringSummaryList;
	}

}
