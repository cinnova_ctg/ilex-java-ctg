package com.mind.MSP.business;

import java.util.HashMap;

import com.mind.bean.msp.ShippingInstructionSiteInfo;

public class ShippingInstructionBean {
	private String appendixId = null;
	private String jobId = null;
	private HashMap<String, String> customFieldUsedMap = null;
	private ShippingInstructionSiteInfo siteInfo = null;
	
	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public HashMap<String, String> getCustomFieldUsedMap() {
		return customFieldUsedMap;
	}

	public void setCustomFieldUsedMap(HashMap<String, String> customFieldUsedMap) {
		this.customFieldUsedMap = customFieldUsedMap;
	}

	public ShippingInstructionSiteInfo getSiteInfo() {
		return siteInfo;
	}

	public void setSiteInfo(ShippingInstructionSiteInfo siteInfo) {
		this.siteInfo = siteInfo;
	}

}
