package com.mind.MSP.business;

import java.util.ArrayList;

import com.mind.bean.msp.MspNetmedxTicket;

/**
 * The Class MspCustomerReportedEventBean.
 * 
 * @author vijay kumar singh
 */
public class MspCustomerReportedEventBean {

	private ArrayList<MspNetmedxTicket> mspNetmedxTicketList;

	public ArrayList<MspNetmedxTicket> getMspNetmedxTicketList() {
		return mspNetmedxTicketList;
	}

	public void setMspNetmedxTicketList(
			ArrayList<MspNetmedxTicket> mspNetmedxTicketList) {
		this.mspNetmedxTicketList = mspNetmedxTicketList;
	}

}
