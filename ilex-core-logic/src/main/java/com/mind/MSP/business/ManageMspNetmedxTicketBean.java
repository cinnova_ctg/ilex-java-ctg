package com.mind.MSP.business;

import java.util.ArrayList;

import com.mind.bean.msp.DeviceInfoBean;
import com.mind.bean.msp.SiteInfo;
import com.mind.bean.msp.TicketInformation;
import com.mind.common.LabelValue;

/**
 * The Class ManageMspNetmedxTicketBean.
 * 
 * @author vijay kumar singh
 */
public class ManageMspNetmedxTicketBean {

	Integer ticketId;
	DeviceInfoBean deviceInfo;
	SiteInfo siteInfo = new SiteInfo();
	TicketInformation ticketInformation = new TicketInformation();
	private ArrayList<LabelValue> problemCategoryList;

	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public DeviceInfoBean getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfoBean deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public SiteInfo getSiteInfo() {
		return siteInfo;
	}

	public void setSiteInfo(SiteInfo siteInfo) {
		this.siteInfo = siteInfo;
	}

	public TicketInformation getTicketInformation() {
		return ticketInformation;
	}

	public void setTicketInformation(TicketInformation ticketInformation) {
		this.ticketInformation = ticketInformation;
	}

	public ArrayList<LabelValue> getProblemCategoryList() {
		return problemCategoryList;
	}

	public void setProblemCategoryList(ArrayList<LabelValue> problemCategoryList) {
		this.problemCategoryList = problemCategoryList;
	}

}
