package com.mind.MSP.business;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.mind.bean.msp.RealTimeState;

public class MspEventBean {

	/** The real time state. */
	private ArrayList<RealTimeState> incidentList = new ArrayList<RealTimeState>();
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
	      return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	    }

	public ArrayList<RealTimeState> getIncidentList() {
		return incidentList;
	}

	/**
	 * Sets the incident list.
	 * 
	 * @param incidentList the new incident list
	 */
	public void setIncidentList(ArrayList<RealTimeState> incidentList) {
		this.incidentList = incidentList;
	}
}
