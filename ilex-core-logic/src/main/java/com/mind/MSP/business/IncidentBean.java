package com.mind.MSP.business;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.mind.bean.msp.RealTimeState;

/**
 * The Class IncidentsHelpDeskBean.
 */
public class IncidentBean {

	/** The real time state. */
	private ArrayList<RealTimeState> incidentList = new ArrayList<RealTimeState>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	/**
	 * Gets the real time state.
	 * 
	 * @return the real time state
	 */
	public ArrayList<RealTimeState> getIncidentList() {
		return incidentList;
	}

	/**
	 * Sets the real time state.
	 * 
	 * @param realTimeState
	 *            the new real time state
	 */
	public void setIncidentList(ArrayList<RealTimeState> realTimeState) {
		this.incidentList = realTimeState;
	}
}
