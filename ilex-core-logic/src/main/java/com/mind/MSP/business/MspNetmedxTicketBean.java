package com.mind.MSP.business;

import java.util.ArrayList;

import com.mind.common.LabelValue;

/**
 * The Class MspNetmedxTicketBean.
 * 
 * @author vijay kumar singh
 */
public class MspNetmedxTicketBean {

	private String customer;
	private ArrayList<LabelValue> customerList = new ArrayList<LabelValue>();
	private String siteSearch;
	private String search;
	private String problemCategory;
	private ArrayList<LabelValue> problemCategoryList = new ArrayList<LabelValue>();
	private String problemDescription;
	private String createTicket;
	private String siteName;
	private ArrayList<LabelValue> siteList = new ArrayList<LabelValue>();

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public ArrayList<LabelValue> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(ArrayList<LabelValue> customerList) {
		this.customerList = customerList;
	}

	public String getSiteSearch() {
		return siteSearch;
	}

	public void setSiteSearch(String siteSearch) {
		this.siteSearch = siteSearch;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getProblemCategory() {
		return problemCategory;
	}

	public void setProblemCategory(String problemCategory) {
		this.problemCategory = problemCategory;
	}

	public ArrayList<LabelValue> getProblemCategoryList() {
		return problemCategoryList;
	}

	public void setProblemCategoryList(ArrayList<LabelValue> problemCategoryList) {
		this.problemCategoryList = problemCategoryList;
	}

	public String getProblemDescription() {
		return problemDescription;
	}

	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}

	public String getCreateTicket() {
		return createTicket;
	}

	public void setCreateTicket(String createTicket) {
		this.createTicket = createTicket;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public ArrayList<LabelValue> getSiteList() {
		return siteList;
	}

	public void setSiteList(ArrayList<LabelValue> siteList) {
		this.siteList = siteList;
	}

}
