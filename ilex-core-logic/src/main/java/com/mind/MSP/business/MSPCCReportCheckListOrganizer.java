package com.mind.MSP.business;

import java.util.ArrayList;

import javax.sql.DataSource;

import com.mind.dao.PRM.CustomerInfoBean;
import com.mind.dao.PRM.CustomerInformationdao;
import com.mind.newjobdb.dao.JobLevelWorkflowDAO;
import com.mind.newjobdb.dao.JobWorkflowChecklist;

public class MSPCCReportCheckListOrganizer {

	public static void setCheckListData(MSPCCReportCheckListBean bean,
			DataSource ds) {
		ArrayList<CustomerInfoBean> customerInfoList =
		// CustomerInformationdao.getCustomerRequiredData("352",
		// bean.getJobId(), ds);

				CustomerInformationdao.getCustomerRequiredData(bean
						.getAppendixId(), bean.getJobId(), ds);

		JobWorkflowChecklist topPendingCheckListItems =
				(JobLevelWorkflowDAO.getJobWorkflowChecklist(Long
						.parseLong(bean.getJobId()), ds)).getTopPendingItems(1,
						true);

		bean.setCustomerInfoList(customerInfoList);
		bean.setChecklist(topPendingCheckListItems);
	}
}
