package com.mind.MSP.business;

import java.util.ArrayList;

import com.mind.bean.msp.DeviceInfoBean;
import com.mind.bean.msp.SiteInfo;
import com.mind.bean.msp.TicketInformation;
import com.mind.common.LabelValue;

public class ManageOutageBean {

	DeviceInfoBean deviceInfoBean = new DeviceInfoBean();
	SiteInfo siteInfo = new SiteInfo();
	TicketInformation ticketInformation = new TicketInformation();
	private ArrayList<LabelValue> problemCategoryList = new ArrayList<LabelValue>();
	private ArrayList<LabelValue> issueOwnerList = new ArrayList<LabelValue>();
	private ArrayList<LabelValue> resolutionList = new ArrayList<LabelValue>();
	private ArrayList<LabelValue> rootCauseList = new ArrayList<LabelValue>();
	private ArrayList<LabelValue> nextActionList = new ArrayList<LabelValue>();

	public ArrayList<LabelValue> getResolutionList() {
		return resolutionList;
	}

	public void setResolutionList(ArrayList<LabelValue> resolutionList) {
		this.resolutionList = resolutionList;
	}

	public ArrayList<LabelValue> getIssueOwnerList() {
		return issueOwnerList;
	}

	public void setIssueOwnerList(ArrayList<LabelValue> issueOwnerList) {
		this.issueOwnerList = issueOwnerList;
	}

	public ArrayList<LabelValue> getRootCauseList() {
		return rootCauseList;
	}

	public void setRootCauseList(ArrayList<LabelValue> rootCauseList) {
		this.rootCauseList = rootCauseList;
	}

	public ArrayList<LabelValue> getProblemCategoryList() {
		return problemCategoryList;
	}

	public void setProblemCategoryList(ArrayList<LabelValue> problemCategoryList) {
		this.problemCategoryList = problemCategoryList;
	}

	public TicketInformation getTicketInformation() {
		return ticketInformation;
	}

	public void setTicketInformation(TicketInformation ticketInformation) {
		this.ticketInformation = ticketInformation;
	}

	public DeviceInfoBean getDeviceInfoBean() {
		return deviceInfoBean;
	}

	public void setDeviceInfoBean(DeviceInfoBean deviceInfoBean) {
		this.deviceInfoBean = deviceInfoBean;
	}

	public SiteInfo getSiteInfo() {
		return siteInfo;
	}

	public void setSiteInfo(SiteInfo siteInfo) {
		this.siteInfo = siteInfo;
	}

	public ArrayList<LabelValue> getNextActionList() {
		return nextActionList;
	}

	public void setNextActionList(ArrayList<LabelValue> nextActionList) {
		this.nextActionList = nextActionList;
	}

}
