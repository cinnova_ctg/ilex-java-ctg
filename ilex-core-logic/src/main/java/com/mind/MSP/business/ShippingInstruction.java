package com.mind.MSP.business;

import java.util.HashMap;

import javax.sql.DataSource;

import com.mind.bean.msp.ShippingInstructionSiteInfo;
import com.mind.dao.PRM.ShippingInstructionDAO;

public class ShippingInstruction {

	public static ShippingInstructionBean setShippingInstructionData(
			ShippingInstructionBean bean, DataSource ds) {
		HashMap<String, String> shippingInstrMap =
				ShippingInstructionDAO.getShippingInstructionData(bean
						.getJobId(), ds);

		ShippingInstructionSiteInfo siteInfo =
				ShippingInstructionDAO.getJobSiteInfo(bean.getJobId(), ds);

		bean.setCustomFieldUsedMap(shippingInstrMap);
		bean.setSiteInfo(siteInfo);
		return bean;
	}

}
