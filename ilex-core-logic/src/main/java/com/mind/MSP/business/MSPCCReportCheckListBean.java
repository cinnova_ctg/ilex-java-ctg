package com.mind.MSP.business;

import java.util.ArrayList;

import com.mind.dao.PRM.CustomerInfoBean;
import com.mind.newjobdb.dao.JobWorkflowChecklist;

public class MSPCCReportCheckListBean {
	private String appendixId = null;
	private String jobId = null;
	private ArrayList<CustomerInfoBean> customerInfoList = null;
	private JobWorkflowChecklist checklist = null; 
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public ArrayList<CustomerInfoBean> getCustomerInfoList() {
		return customerInfoList;
	}
	public void setCustomerInfoList(ArrayList<CustomerInfoBean> customerInfoList) {
		this.customerInfoList = customerInfoList;
	}
	public JobWorkflowChecklist getChecklist() {
		return checklist;
	}
	public void setChecklist(JobWorkflowChecklist checklist) {
		this.checklist = checklist;
	}

}
