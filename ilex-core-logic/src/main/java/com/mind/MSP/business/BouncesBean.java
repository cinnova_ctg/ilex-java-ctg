package com.mind.MSP.business;

import java.util.ArrayList;

import com.mind.bean.msp.Bounces;

/**
 * The Class BouncesBean.
 */
public class BouncesBean {

	/** The bounces list. */
	private static ArrayList<Bounces> bouncesList = new ArrayList<Bounces>();

	/**
	 * Gets the bounces list.
	 * 
	 * @return the bounces list
	 */
	public static ArrayList<Bounces> getBouncesList() {
		return bouncesList;
	}

	/**
	 * Sets the bounces list.
	 * 
	 * @param bouncesList
	 *            the new bounces list
	 */
	public static void setBouncesList(ArrayList<Bounces> bouncesList) {
		BouncesBean.bouncesList = bouncesList;
	}

}
