package com.mind.MSP.business;

import java.util.ArrayList;

import com.mind.bean.msp.RealTimeState;
import com.mind.bean.msp.RealTimeStateInfo;

/**
 * The Class RealTimeStateBean.
 */
public class RealTimeStateBean {

	/** The real time state. */
	private ArrayList<RealTimeState> realTimeState = new ArrayList<RealTimeState>();

	/** The unManaged Initial Assessment. */
	private ArrayList<RealTimeState> unManagedIniAssessment = new ArrayList<RealTimeState>();

	/** The real time state info. */
	private RealTimeStateInfo realTimeStateInfo = new RealTimeStateInfo();

	private RealTimeStateInfo unManagedRealTimeStateInfo = new RealTimeStateInfo();

	/**
	 * Gets the real time state.
	 * 
	 * @return the real time state
	 */
	public ArrayList<RealTimeState> getRealTimeState() {
		return realTimeState;
	}

	/**
	 * Sets the real time state.
	 * 
	 * @param realTimeState
	 *            the new real time state
	 */
	public void setRealTimeState(ArrayList<RealTimeState> realTimeState) {
		this.realTimeState = realTimeState;
	}

	/**
	 * Gets the real time state info.
	 * 
	 * @return the real time state info
	 */
	public RealTimeStateInfo getRealTimeStateInfo() {
		return realTimeStateInfo;
	}

	/**
	 * Sets the real time state info.
	 * 
	 * @param realTimeStateInfo
	 *            the new real time state info
	 */
	public void setRealTimeStateInfo(RealTimeStateInfo realTimeStateInfo) {
		this.realTimeStateInfo = realTimeStateInfo;
	}

	public ArrayList<RealTimeState> getUnManagedIniAssessment() {
		return unManagedIniAssessment;
	}

	public void setUnManagedIniAssessment(
			ArrayList<RealTimeState> unManagedIniAssessment) {
		this.unManagedIniAssessment = unManagedIniAssessment;
	}

	public RealTimeStateInfo getUnManagedRealTimeStateInfo() {
		return unManagedRealTimeStateInfo;
	}

	public void setUnManagedRealTimeStateInfo(
			RealTimeStateInfo unManagedRealTimeStateInfo) {
		this.unManagedRealTimeStateInfo = unManagedRealTimeStateInfo;
	}

}
