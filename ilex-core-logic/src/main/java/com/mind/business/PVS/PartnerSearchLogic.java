package com.mind.business.PVS;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFAnchor;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFComment;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

import com.mind.bean.pvs.Partner_SearchBean;

public class PartnerSearchLogic {

	public static final Logger logger = Logger
			.getLogger(PartnerSearchLogic.class);

	public HSSFWorkbook exportPartnerSearchResultToExcel(
			List<Partner_SearchBean> partnerDetailList,
			Map<Integer, String> coreCompeteancies,
			Map<String, String> coreCompetanciesWithfullName) {

		HSSFWorkbook wb = new HSSFWorkbook();

		HSSFSheet sheet = wb.createSheet("Sheet1");

		createHeaderSection(wb, sheet, coreCompetanciesWithfullName);

		createBodySection(partnerDetailList, wb, sheet, coreCompeteancies);

		return wb;

	}

	@SuppressWarnings("deprecation")
	private void createBodySection(List<Partner_SearchBean> partnerDetailList,
			HSSFWorkbook wb, HSSFSheet sheet,
			Map<Integer, String> coreCompeteancies) {
		HSSFCellStyle coreComptencyCellStyle = wb.createCellStyle();
		coreComptencyCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		HSSFCellStyle centerAlignStyler = wb.createCellStyle();
		centerAlignStyler.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		HSSFCellStyle leftAlignStyler = wb.createCellStyle();
		leftAlignStyler.setAlignment(HSSFCellStyle.ALIGN_LEFT);

		HSSFCellStyle dateStyler = wb.createCellStyle();
		setcellDateStyle(dateStyler);

		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

		int rowCount = 2;
		for (Partner_SearchBean partner : partnerDetailList) {
			HSSFRow dataRow = sheet.createRow(rowCount);

			HSSFCell parnterNameCell = dataRow.createCell((short) 0);
			parnterNameCell.setCellValue(partner.getPartnername());

			HSSFCell partnerPhoneCell = dataRow.createCell((short) 1,
					HSSFCell.CELL_TYPE_STRING);
			partnerPhoneCell.setCellStyle(leftAlignStyler);
			try {
				partnerPhoneCell.setCellValue(Double.valueOf(partner
						.getPartnerPriPhone()));
			} catch (NumberFormatException e) {
				partnerPhoneCell.setCellValue(partner.getPartnerPriPhone());
			}

			HSSFCell partnerEmailCell = dataRow.createCell((short) 2);
			partnerEmailCell.setCellValue(partner.getPartnerPriEmail());

			HSSFCell addressCell = dataRow.createCell((short) 3);
			addressCell.setCellValue(partner.getPartnerAddress1());

			HSSFCell cityCell = dataRow.createCell((short) 4);
			cityCell.setCellValue(partner.getCity());

			HSSFCell stateCell = dataRow.createCell((short) 5);
			stateCell.setCellStyle(centerAlignStyler);
			stateCell.setCellValue(partner.getState());

			HSSFCell countryCell11 = dataRow.createCell((short) 6);
			countryCell11.setCellStyle(centerAlignStyler);
			countryCell11.setCellValue(partner.getCountry());

			HSSFCell zipcodeCell = dataRow.createCell((short) 7);
			zipcodeCell.setCellStyle(centerAlignStyler);

			try {
				zipcodeCell.setCellValue(Double.valueOf(partner.getZipcode()));
			} catch (NumberFormatException e) {
				zipcodeCell.setCellValue(partner.getZipcode());
			}

			String lastUsedDate = "";
			if (StringUtils.isEmpty(partner.getPartnerLastUsed())) {
				if (StringUtils.isEmpty(partner.getUpdateDate())) {
					lastUsedDate = partner.getCreateDate();
				} else {
					lastUsedDate = partner.getUpdateDate();
				}
			} else {
				lastUsedDate = partner.getPartnerLastUsed();
			}

			if ("Minuteman".equalsIgnoreCase(partner.getPartnerType())) {
				HSSFCell countCell = dataRow.createCell((short) 8);
				countCell.setCellStyle(centerAlignStyler);
				countCell.setCellValue(Integer.valueOf(partner.getPoCount()));

				HSSFCell lastUsedCell = dataRow.createCell((short) 9);
				lastUsedCell.setCellStyle(dateStyler);
				if (Integer.valueOf(partner.getPoCount()) != 0) {
					try {
						Date date = formatter.parse(lastUsedDate);
						lastUsedCell.setCellValue(date);
					} catch (ParseException e) {
						lastUsedCell.setCellValue(lastUsedDate);
					}
				}

			}
			if ("Certified Partners - Speedpay Users".equalsIgnoreCase(partner
					.getPartnerType())) {
				HSSFCell countCell = dataRow.createCell((short) 10);
				countCell.setCellStyle(centerAlignStyler);
				countCell.setCellValue(Integer.valueOf(partner.getPoCount()));

				HSSFCell lastUsedCell = dataRow.createCell((short) 11);
				lastUsedCell.setCellStyle(dateStyler);
				if (Integer.valueOf(partner.getPoCount()) != 0) {
					try {
						Date date = formatter.parse(lastUsedDate);
						lastUsedCell.setCellValue(date);
					} catch (ParseException e) {
						lastUsedCell.setCellValue(lastUsedDate);
					}
				}
			}
			if ("Certified".equalsIgnoreCase(partner.getPartnerType())) {
				HSSFCell countCell = dataRow.createCell((short) 12);
				countCell.setCellStyle(centerAlignStyler);
				countCell.setCellValue(Integer.valueOf(partner.getPoCount()));

				HSSFCell lastUsedCell = dataRow.createCell((short) 13);
				lastUsedCell.setCellStyle(dateStyler);
				if (Integer.valueOf(partner.getPoCount()) != 0) {
					try {
						Date date = formatter.parse(lastUsedDate);
						lastUsedCell.setCellValue(date);
					} catch (ParseException e) {
						lastUsedCell.setCellValue(lastUsedDate);
					}
				}
			}

			HSSFCell ratingCell = dataRow.createCell((short) 14);
			if (StringUtils.isNotEmpty(partner.getPartnerAvgRating()))
				ratingCell.setCellValue(Float.valueOf(partner
						.getPartnerAvgRating()));

			ratingCell.setCellStyle(centerAlignStyler);

			HSSFCell contractedCell = dataRow.createCell((short) 15);
			if (StringUtils.isNotEmpty(partner.getContractedCount()))
				contractedCell.setCellValue(Integer.valueOf(partner
						.getContractedCount()));
			contractedCell.setCellStyle(centerAlignStyler);

			HSSFCell compeletedCell = dataRow.createCell((short) 16);
			if (StringUtils.isNotEmpty(partner.getCompeletedCount()))
				compeletedCell.setCellValue(Float.valueOf(partner
						.getCompeletedCount()));
			compeletedCell.setCellStyle(centerAlignStyler);

			HSSFCell quesProfessionalCell = dataRow.createCell((short) 17);
			if (StringUtils.isNotEmpty(partner.getQuesProfessional()))
				quesProfessionalCell.setCellValue(Float.valueOf(partner
						.getQuesProfessional()));
			quesProfessionalCell.setCellStyle(centerAlignStyler);

			HSSFCell quesOnTimeCell = dataRow.createCell((short) 18);
			if (StringUtils.isNotEmpty(partner.getQuesOnTime()))
				quesOnTimeCell.setCellValue(Float.valueOf(partner
						.getQuesOnTime()));
			quesOnTimeCell.setCellStyle(centerAlignStyler);

			HSSFCell quesKnowledgeableCell = dataRow.createCell((short) 19);
			if (StringUtils.isNotEmpty(partner.getQuesKnowledgeable()))
				quesKnowledgeableCell.setCellValue(Float.valueOf(partner
						.getQuesKnowledgeable()));
			quesKnowledgeableCell.setCellStyle(centerAlignStyler);

			HSSFCell quesEquippedCell = dataRow.createCell((short) 20);
			if (StringUtils.isNotEmpty(partner.getQuesEquipped()))
				quesEquippedCell.setCellValue(Float.valueOf(partner
						.getQuesEquipped()));
			quesEquippedCell.setCellStyle(centerAlignStyler);

			HSSFCell partnerAdpoCell = dataRow.createCell((short) 21);
			if (StringUtils.isNotEmpty(partner.getPartnerAdpo()))
				partnerAdpoCell.setCellValue(Integer.valueOf(partner
						.getPartnerAdpo()));
			partnerAdpoCell.setCellStyle(centerAlignStyler);

			HSSFCell lastDateCell = dataRow.createCell((short) 22);
			lastDateCell.setCellStyle(dateStyler);

			try {
				Date date = formatter.parse(lastUsedDate);
				lastDateCell.setCellValue(date);
			} catch (ParseException e) {
				lastDateCell.setCellValue(lastUsedDate);
			}

			HSSFCell distanceCell = dataRow.createCell((short) 23);
			distanceCell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);

			distanceCell.setCellStyle(centerAlignStyler);
			distanceCell.setCellValue(Float.valueOf(partner.getDistance()));

			HSSFCell signUpDate = dataRow.createCell((short) 24);
			signUpDate.setCellStyle(dateStyler);

			try {
				Date date = formatter.parse(partner.getCreateDate());
				signUpDate.setCellValue(date);
			} catch (ParseException e) {
				signUpDate.setCellValue(partner.getCreateDate());
			}

			int count = 25;
			for (Integer partnerCoreCompetencyId : coreCompeteancies.keySet()) {

				HSSFCell cell = dataRow.createCell((short) count);
				if (partner.getCmboxcorecompetencies().equals("")) {

				}
				String[] partnerCoreCompetencies = partner
						.getCmboxcorecompetencies().split(",");

				if (coreCompeteancyFound(partnerCoreCompetencyId,
						partnerCoreCompetencies)) {
					cell.setCellValue("X");

					cell.setCellStyle(coreComptencyCellStyle);
				}

				count++;
			}

			rowCount++;

			// Add code for tool list in excel file for comma seperated

			ArrayList<String> arList = new ArrayList<String>();

			HSSFCell partnerToolCell = dataRow.createCell((short) 50);
			StringBuilder sb = new StringBuilder();
			for (String str : partner.getToollist()) {
				sb.append(str).append(",");

			}

			String strfromArrayList = sb.toString();

			partnerToolCell.setCellStyle(leftAlignStyler);
			partnerToolCell.setCellValue(strfromArrayList);

			System.out.println(strfromArrayList);

		}

		for (short colNum = 0; colNum < 24 + coreCompeteancies.size() + 5; colNum++) {
			sheet.autoSizeColumn(colNum);
		}

		sheet.setColumnWidth((short) 24, (short) 1000);

	}

	private boolean coreCompeteancyFound(Integer partnerCoreCompetencyId,
			String[] partnerCoreCompetencies) {
		boolean found = false;
		for (String partnerCoreCompetency : partnerCoreCompetencies) {
			Integer coreCompetency;
			try {
				coreCompetency = Integer.valueOf(partnerCoreCompetency);
			} catch (NumberFormatException e) {
				coreCompetency = -1;
			}

			if (partnerCoreCompetencyId == coreCompetency) {
				found = true;
				return true;
			}

		}
		return found;

	}

	@SuppressWarnings("deprecation")
	private void createHeaderSection(HSSFWorkbook wb, HSSFSheet sheet,

	Map<String, String> coreCompetanciesWithfullName) {

		HSSFCellStyle styleHeader = wb.createCellStyle();
		setcellHeaderStyle(styleHeader);

		HSSFFont hSSFFont = wb.createFont();
		hSSFFont.setFontName(HSSFFont.FONT_ARIAL);
		hSSFFont.setFontHeightInPoints((short) 10);
		hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		hSSFFont.setColor(HSSFColor.BLACK.index);
		styleHeader.setFont(hSSFFont);

		HSSFRow dataRow0 = sheet.createRow(0);
		HSSFRow dataRow1 = sheet.createRow(1);

		HSSFCell cell00 = dataRow0.createCell((short) 0);
		HSSFCell cell10 = dataRow1.createCell((short) 0);
		cell00.setCellValue("Partner");

		HSSFCell cell01 = dataRow0.createCell((short) 1);
		HSSFCell cell11 = dataRow1.createCell((short) 1);
		cell01.setCellValue("Phone");

		HSSFCell cell02 = dataRow0.createCell((short) 2);
		HSSFCell cell12 = dataRow1.createCell((short) 2);
		cell02.setCellValue("E-mail");

		HSSFCell cell03 = dataRow0.createCell((short) 3);
		HSSFCell cell13 = dataRow1.createCell((short) 3);
		cell03.setCellValue("Street Address");

		HSSFCell cell04 = dataRow0.createCell((short) 4);
		HSSFCell cell14 = dataRow1.createCell((short) 4);
		cell04.setCellValue("City");

		HSSFCell cell05 = dataRow0.createCell((short) 5);
		HSSFCell cell15 = dataRow1.createCell((short) 5);
		cell05.setCellValue("State");

		HSSFCell cell06 = dataRow0.createCell((short) 6);
		HSSFCell cell16 = dataRow1.createCell((short) 6);
		cell06.setCellValue("Country");

		HSSFCell cell07 = dataRow0.createCell((short) 7);
		HSSFCell cell17 = dataRow1.createCell((short) 7);
		cell07.setCellValue("Zip Code");

		HSSFCell cell08 = dataRow0.createCell((short) 8);
		HSSFCell cell09 = dataRow0.createCell((short) 9);
		cell08.setCellValue("MM");

		HSSFCell cell010 = dataRow0.createCell((short) 10);
		HSSFCell cell011 = dataRow0.createCell((short) 11);
		cell010.setCellValue("SP");

		HSSFCell cell012 = dataRow0.createCell((short) 12);
		HSSFCell cell013 = dataRow0.createCell((short) 13);
		cell012.setCellValue("PVS");

		HSSFCell cell014 = dataRow0.createCell((short) 14);
		HSSFCell cell015 = dataRow0.createCell((short) 15);
		HSSFCell cell016 = dataRow0.createCell((short) 16);
		HSSFCell cell017 = dataRow0.createCell((short) 17);
		HSSFCell cell018 = dataRow0.createCell((short) 18);
		HSSFCell cell019 = dataRow0.createCell((short) 19);
		HSSFCell cell020 = dataRow0.createCell((short) 20);
		cell014.setCellValue("Rating");

		HSSFCell cell021 = dataRow0.createCell((short) 21);
		HSSFCell cell121 = dataRow1.createCell((short) 21);

		cell021.setCellValue("ADPO");

		HSSFCell cell022 = dataRow0.createCell((short) 22);
		HSSFCell cell122 = dataRow1.createCell((short) 22);
		cell022.setCellValue("Last Used");

		HSSFCell cell023 = dataRow0.createCell((short) 23);
		HSSFCell cell123 = dataRow1.createCell((short) 23);
		cell023.setCellValue("Distance");

		HSSFCell cell024 = dataRow0.createCell((short) 24);
		HSSFCell cell124 = dataRow1.createCell((short) 24);
		cell024.setCellValue("Sign Up");

		HSSFCell cell18 = dataRow1.createCell((short) 8);
		cell18.setCellValue("Count");

		HSSFCell cell19 = dataRow1.createCell((short) 9);
		cell19.setCellValue("Last Used");

		HSSFCell cell110 = dataRow1.createCell((short) 10);
		cell110.setCellValue("Count");

		HSSFCell cell111 = dataRow1.createCell((short) 11);
		cell111.setCellValue("Last Used");

		HSSFCell cell112 = dataRow1.createCell((short) 12);
		cell112.setCellValue("Count");

		HSSFCell cell113 = dataRow1.createCell((short) 13);
		cell113.setCellValue("Last Used");

		HSSFCell cell114 = dataRow1.createCell((short) 14);
		cell114.setCellValue("Rating");

		HSSFCell cell115 = dataRow1.createCell((short) 15);
		cell115.setCellValue("Contracted");

		HSSFCell cell116 = dataRow1.createCell((short) 16);
		cell116.setCellValue("Completed");

		HSSFCell cell117 = dataRow1.createCell((short) 17);
		HSSFCell cell118 = dataRow1.createCell((short) 18);
		HSSFCell cell119 = dataRow1.createCell((short) 19);
		HSSFCell cell120 = dataRow1.createCell((short) 20);

		cell117.setCellValue("POKE");

		HSSFCell cell025 = dataRow0.createCell((short) 25);
		cell025.setCellValue("Core Competencies");
		for (int i = 26; i < 26 + coreCompetanciesWithfullName.size() - 1; i++) {
			dataRow0.createCell((short) i).setCellStyle(styleHeader);
		}
		Short coreCompletenciesLastCell = (short) (24 + coreCompetanciesWithfullName
				.size() - 1);

		int count = 25;

		HSSFPatriarch patriarch = sheet.createDrawingPatriarch();

		for (String key : coreCompetanciesWithfullName.keySet()) {
			String value = coreCompetanciesWithfullName.get(key);
			HSSFCell cell = dataRow1.createCell((short) count);
			HSSFAnchor anchor = new HSSFClientAnchor(0, 0, 0, 0, (short) count,
					0, (short) (count + 6), 2);
			HSSFComment comment = patriarch.createComment(anchor);
			comment.setString(new HSSFRichTextString(key));
			cell.setCellComment(comment);

			cell.setCellValue(value);
			cell.setCellStyle(styleHeader);
			count++;
			sheet.autoSizeColumn((short) count);
		}

		// cells added for set comments columns
		HSSFCell cell125 = dataRow1.createCell((short) (count));
		HSSFCell cell126 = dataRow1.createCell((short) (count + 1));
		HSSFCell cell127 = dataRow1.createCell((short) (count + 2));
		HSSFCell cell128 = dataRow1.createCell((short) (count + 3));
		HSSFCell cell129 = dataRow1.createCell((short) (count + 4));
		HSSFCell cell130 = dataRow1.createCell((short) (count + 5));

		HSSFCell cell040 = dataRow0.createCell((short) 50);
		HSSFCell cell150 = dataRow1.createCell((short) 50); // add column for
		// tool list
		cell040.setCellValue("Tool List");

		sheet.addMergedRegion(new Region(0, (short) 0, 1, (short) 0));
		sheet.addMergedRegion(new Region(0, (short) 1, 1, (short) 1));
		sheet.addMergedRegion(new Region(0, (short) 2, 1, (short) 2));
		sheet.addMergedRegion(new Region(0, (short) 3, 1, (short) 3));
		sheet.addMergedRegion(new Region(0, (short) 4, 1, (short) 4));
		sheet.addMergedRegion(new Region(0, (short) 5, 1, (short) 5));
		sheet.addMergedRegion(new Region(0, (short) 6, 1, (short) 6));
		sheet.addMergedRegion(new Region(0, (short) 7, 1, (short) 7));
		sheet.addMergedRegion(new Region(0, (short) 8, 0, (short) 9));
		sheet.addMergedRegion(new Region(0, (short) 10, 0, (short) 11));
		sheet.addMergedRegion(new Region(0, (short) 12, 0, (short) 13));
		sheet.addMergedRegion(new Region(0, (short) 14, 0, (short) 20));

		sheet.addMergedRegion(new Region(1, (short) 17, 1, (short) 20));// poke
																		// Cell

		sheet.addMergedRegion(new Region(0, (short) 21, 1, (short) 21));
		sheet.addMergedRegion(new Region(0, (short) 22, 1, (short) 22));
		sheet.addMergedRegion(new Region(0, (short) 23, 1, (short) 23));
		sheet.addMergedRegion(new Region(0, (short) 24, 1, (short) 24));
		// column
		// list
		sheet.addMergedRegion(new Region(0, (short) 25, 0,
				coreCompletenciesLastCell));

		sheet.addMergedRegion(new Region(0, (short) 50, 1, (short) 50));
		// sheet.addMergedRegion(new Region(0, (short) 25, 0, (short) 25));

		/* sheet.addMergedRegion(new Region(0, (short) 31, 1, (short) 31)); */

		/*
		 * sheet.addMergedRegion(new Region(0, (short) 26, 1, (short) 26)); //
		 * tool // kit
		 */
		cell00.setCellStyle(styleHeader);
		cell01.setCellStyle(styleHeader);
		cell02.setCellStyle(styleHeader);
		cell03.setCellStyle(styleHeader);
		cell04.setCellStyle(styleHeader);
		cell05.setCellStyle(styleHeader);
		cell06.setCellStyle(styleHeader);
		cell07.setCellStyle(styleHeader);
		cell08.setCellStyle(styleHeader);
		cell09.setCellStyle(styleHeader);
		cell010.setCellStyle(styleHeader);
		cell011.setCellStyle(styleHeader);
		cell012.setCellStyle(styleHeader);
		cell013.setCellStyle(styleHeader);
		cell014.setCellStyle(styleHeader);
		cell015.setCellStyle(styleHeader);
		cell016.setCellStyle(styleHeader);
		cell017.setCellStyle(styleHeader);
		cell018.setCellStyle(styleHeader);
		cell019.setCellStyle(styleHeader);
		cell020.setCellStyle(styleHeader);
		cell021.setCellStyle(styleHeader);
		cell022.setCellStyle(styleHeader);
		cell023.setCellStyle(styleHeader);
		cell024.setCellStyle(styleHeader);
		cell025.setCellStyle(styleHeader);
		/* cell026.setCellStyle(styleHeader); */// tool kit list
		cell040.setCellStyle(styleHeader);

		cell10.setCellStyle(styleHeader);
		cell11.setCellStyle(styleHeader);
		cell12.setCellStyle(styleHeader);
		cell13.setCellStyle(styleHeader);
		cell14.setCellStyle(styleHeader);
		cell15.setCellStyle(styleHeader);
		cell16.setCellStyle(styleHeader);
		cell17.setCellStyle(styleHeader);
		cell18.setCellStyle(styleHeader);
		cell19.setCellStyle(styleHeader);
		cell110.setCellStyle(styleHeader);
		cell111.setCellStyle(styleHeader);
		cell112.setCellStyle(styleHeader);

		cell113.setCellStyle(styleHeader);
		cell114.setCellStyle(styleHeader);
		cell115.setCellStyle(styleHeader);
		cell116.setCellStyle(styleHeader);
		cell117.setCellStyle(styleHeader);
		cell118.setCellStyle(styleHeader);
		cell119.setCellStyle(styleHeader);
		cell120.setCellStyle(styleHeader);
		cell121.setCellStyle(styleHeader);
		cell122.setCellStyle(styleHeader);
		cell123.setCellStyle(styleHeader);
		cell124.setCellStyle(styleHeader);
		cell150.setCellStyle(styleHeader);

	}

	/**
	 * Purpose : Set the Header Style of the Excel Sheet
	 * 
	 * @param styleHeader
	 */
	public void setcellHeaderStyle(HSSFCellStyle styleHeader) {
		// Set cell header
		styleHeader.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		styleHeader.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		styleHeader.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		styleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleHeader.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		styleHeader.setBottomBorderColor(HSSFColor.WHITE.index);
		styleHeader.setBorderRight(HSSFCellStyle.BORDER_THIN);
		styleHeader.setRightBorderColor(HSSFColor.WHITE.index);
		styleHeader.setHidden(false);

	}

	/**
	 * Sets the cell date style.
	 * 
	 * @param styleDate
	 *            the new cell date style
	 */
	public void setcellDateStyle(HSSFCellStyle styleDate) {
		styleDate.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy"));
		styleDate.setAlignment(HSSFCellStyle.ALIGN_CENTER);

	}

}
