package com.mind.business.IM;

import java.util.Iterator;

import javax.sql.DataSource;

import com.mind.bean.im.InvoiceJobCommissionPOBean;
import com.mind.bean.im.InvoicedJobDetails;
import com.mind.dao.IM.InvoiceJobDao;


public class InvoiceJobDelegate {
	
	
	
	public static void setDefaultPaymentreceivedDate(InvoiceJobCommissionPOBean bean, DataSource ds) {
		bean.setPaymentReceiveDate(InvoiceJobDao.getDbSystemDate(ds));
	}
	
	public static void setClosedInvoiceJobListToBean(InvoiceJobCommissionPOBean bean, DataSource ds) {
		bean.setInvoicedJobs(InvoiceJobDao.getClosedInvoicedJobs(bean.getInvoiceNo(), ds));
		if(bean.getInvoicedJobs().size() > 0) {
			Float sumTotalRevenue = 0.0f;
			Iterator iterator = bean.getInvoicedJobs().iterator();
			while(iterator.hasNext()) {
				InvoicedJobDetails jobDetails = (InvoicedJobDetails)iterator.next();
				sumTotalRevenue+=Float.parseFloat(jobDetails.getRevenue());
			}
			
			bean.setTotalRevenue(sumTotalRevenue+"");
		} else {
			bean.setTotalRevenue("0.00");
		}
	}
	
	public static int invoiceCommissionPOsForJobs(InvoiceJobCommissionPOBean bean, String userId, DataSource ds)	{
		String jobIdString = "";
		for(int i = 0; i<bean.getJobId().length; i++) {
			jobIdString = jobIdString + bean.getJobId(i)+",";
		}
		
		if(jobIdString.length()>0) {
			jobIdString = jobIdString.substring(0, jobIdString.length()-1);
		}
		int retInvoiceCommFlag = InvoiceJobDao.setCommissionPOInvoice(jobIdString, bean.getPaymentReceiveDate(), userId, ds);
		return retInvoiceCommFlag;
	}
	
	
}
