package com.mind.business.PRM;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.CustomerRequireBean;
import com.mind.common.LabelValue;
import com.mind.dao.PRM.CustomerRequiredDAO;

/**
 * The Class CustomerRequiredDataLogic.
 */
public class CustomerRequiredDataLogic {

	/** The Constant logger. */
	public static final Logger logger = Logger
			.getLogger(CustomerRequiredDataLogic.class);

	/**
	 * Insert customer require data.
	 * 
	 * @param customerRequireBean
	 *            the customer require bean
	 * @param userId
	 *            the user id
	 * @param ds
	 *            the ds
	 * @return the string
	 */
	public String insertCustomerRequireData(
			CustomerRequireBean customerRequireBean, String userId,
			DataSource ds) {
		if (customerRequireBean.getFirstVisitQuestion().booleanValue()) {

			try {
				CustomerRequiredDAO.deleteCustomerRequireData(
						customerRequireBean.getJobId(), ds);
				CustomerRequiredDAO.insertFirstVisitSuccessData(
						customerRequireBean, userId, ds);
			} catch (Exception e) {
				return e.getMessage();
			}
		} else {
			try {
				CustomerRequiredDAO.deleteCustomerRequireData(
						customerRequireBean.getJobId(), ds);
				CustomerRequiredDAO.insertAllCustomerRequireData(
						customerRequireBean, userId, ds);
			} catch (Exception e) {
				return e.getMessage();
			}
		}

		return "success";

	}

	/**
	 * Gets the customer require data from db.
	 * 
	 * @param jobId
	 *            the job id
	 * @param ds
	 *            the ds
	 * @return the customer require data
	 */
	public CustomerRequireBean getCustomerRequireData(String jobId,
			DataSource ds)

	{
		CustomerRequireBean customerRequireBean = new CustomerRequireBean();
		customerRequireBean = CustomerRequiredDAO.getCustomerRequiredData(
				jobId, ds);
		Map<String, List<LabelValue>> selectedCategoryValues = new LinkedHashMap<String, List<LabelValue>>();
		if ((customerRequireBean.isDisplayStatus())
				&& (!customerRequireBean.getFirstVisitQuestion().booleanValue())) {
			List<String> categories = customerRequireBean.getCategory();
			List<String> categoryValues = customerRequireBean
					.getCategoryValues();
			List<LabelValue> selectedCategoryList;
			for (int i = 0; i < categories.size(); i++) {
				selectedCategoryList = CustomerRequiredDAO
						.getSelectedCategoryCodes(categories.get(i),
								categoryValues.get(i), ds);
				selectedCategoryValues.put(categories.get(i),
						selectedCategoryList);
			}

			customerRequireBean
					.setSelectedCategoryValues(selectedCategoryValues);
		}
		return customerRequireBean;

	}
}
