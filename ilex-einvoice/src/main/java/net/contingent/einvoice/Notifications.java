package net.contingent.einvoice;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.*;

public class Notifications {

	Session session;
	
	/*************************************************************************/

	/* Title: Notification Constructor
	 * 
	 * Initialize notification
	 * 
	 * Inputs: 
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *  
	 */
	
	Notifications () {
		
		Properties props = System.getProperties();
		Authenticator auth = new SMTPAuthenticator();

		props.put("mail.smtp.host",      "smtp2.donet.com");
		props.put("mail.smtp.auth",      "true");
		props.put("mail.smtp.localhost", "hd.contingent.com");

		// Exchange Server: cnsmail.contingent.local or 192.168.20.25
		//props.put("mail.smtp.host",      "cnsmail.contingent.local");
		//props.put("mail.smtp.auth",      "true");
		//props.put("mail.smtp.localhost", "contingent.net");

		try {
			 session = Session.getDefaultInstance(props, auth);
			 session.setDebug(true);
		} catch (Exception e) {
			System.out.println("Session Error: "+e);
		}
		
	}

	/*************************************************************************/

	/* Title: Send notification
	 * 
	 * 1) Compose message
	 * 
	 * 2) Send Message
	 * 
	 * Inputs: Recipients
	 * 
	 * Returns: Status of send
	 * 
	 * Revision History:
	 *  
	 */

	boolean sendNotification (Map email_details) {
		boolean rtn_code = true;
		
		String from = (String)email_details.get("From");
		if (from == null) {
			from = "ilex@contingent.net";
		}

		String content_type = (String)email_details.get("content_type");
		if (content_type == null) {
			content_type = "text";
		}
		
		String to = (String)email_details.get("To");
		
		String cc = (String)email_details.get("CC");
		if (cc == null) {
			cc = "Not Available";
		}
	
		String subject = (String)email_details.get("Subject");
		String msg = (String)email_details.get("Message");
				
		// Test 
		//to = "cmurray@contingent.net";
		//cc = "Not Available";
		
		try {
			MimeMessage message = new MimeMessage(session);
			
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			
			if (to.equals("kmcqueen@contingent.com")) {
				//message.addRecipient(Message.RecipientType.CC, new InternetAddress("mmeadows@contingent.net"));
				message.addRecipient(Message.RecipientType.CC, new InternetAddress("cphillips@contingent.com"));
			}
			
			if (!cc.equals("Not Available")) {
				message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc));
			}
			
			//message.addRecipient(Message.RecipientType.BCC, new InternetAddress("cmurray@contingent.net"));
			
			message.setSubject(subject);
			
			if (content_type.equals("text")) {
				message.setText(msg);
			} else {
				message.setContent(msg, content_type);
			}

			Transport.send(message);
			
		} catch (Exception e) {
			rtn_code = false;
			System.out.println("sendNotification Error: "+e);
		}

       return rtn_code;
	}
	
	/*************************************************************************/

    private class SMTPAuthenticator extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
           // Donet Connection
           String username = "ilex.cn";
           String password = "Rocket01";
        	
           // Exchange Connection
           //String username = "ilex";
           //String password = "Rocket01";
 
           return new PasswordAuthentication(username, password);
        }
    }

	/*************************************************************************/

	public static void main(String[] args) {

		Notifications x = new Notifications();
		
		Map email_details = new HashMap ();
		email_details.put("To", "cmurray@contingent.net");
		email_details.put("CC", "cmurray@contingent.net");
		//email_details.put("CC", "gsheppard@contingent.com");
		email_details.put("Subject", "Test Message 4");
		email_details.put("Message", "Test Mesage");
		x.sendNotification(email_details);
	}

}
