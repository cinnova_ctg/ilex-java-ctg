package net.contingent.einvoice;

import java.sql.ResultSet;
import java.util.Vector;


public class Invoice {

	String InvoiceStatus;
	Data da;
	String DataSources;
	
	// Invoice and PO data
	String lm_po_VendorID;
	
	String lm_pi_id;
	String lm_pi_status;
	String lm_pi_submit_type;
	
	String lm_pi_invoice_number;
	String lm_pi_invoice_date;
	String lm_pi_date_received;
	String lm_pi_submitter;
	String lm_pi_total;
	String lm_pi_partner_comments;
	String lm_pi_reconcile_comments;
	String lm_pi_approved_total;
	String lm_pi_approved_by;
	String lm_pi_approved_date;
	String lm_pi_gp_paid_date;
	String lm_pi_gp_check_number;

	String lm_po_terms;
	String lm_po_number;
	String lm_po_issue_date;
	String lm_po_authorised_total;
	String lm_po_deliverables;
	String lm_po_js_id;

	String site_number ;
	String scheduled;
	String job_start;
	String job_end;
	String contingent_contact;
	String contingent_contact_phone;
	String email_address;

	String lm_pi_minuteman_status;
	
	Vector InvoiceFields = new Vector();

	
	/*************************************************************************/

	/* Title: Constructor - semi datasource independent
	 * 
	 * 1) Initialize invoice field list
	 * 
	 * 2) Get field data
	 * 
	 * Inputs: 
	 * 
	 * All invoice fields as pulled from the lm_ei_ilex_to_web_invoices_01 SP
	 * 
	 * 
	 * Returns: 
	 *  
	 */
	
	Invoice (String pi_id, Data xda, String src) {
		
		lm_pi_id = pi_id;
		da = xda;
		DataSources = src; 
		
		// Initialize invoice list of fields
		initializeInvoiceFields ();
		
		// Get data
		getInvoiceData (lm_pi_id); 
	}
	

	/*************************************************************************/

	/* Title: Get Invoice data
	 * 
	 * Get all invoice data.
	 * 
	 * Revision History:
	 *   08/05/07 - 01.00 - Initial Release 
	 *  
	 */

	void getInvoiceData (String id) {

		
		String sql;
		
		if (DataSources.equals("I")) {
			sql = "exec lm_ei_ilex_to_web_invoices_02 "+id;
		} else {
			sql = "select * from lm_ei_ilex_to_web_invoices_01 where lm_pi_id = "+id;
		}
		
		String field = "";
		ResultSet rs = da.getResultSet(sql);

		try {
			if (rs.next()) {
				
				InvoiceStatus = "Found";
				
				lm_po_VendorID = rs.getString("lm_po_VendorID");
				lm_pi_status = rs.getString("lm_pi_status");
				lm_pi_partner_comments=  rs.getString("lm_pi_partner_comments");
				lm_pi_reconcile_comments =  rs.getString("lm_pi_reconcile_comments");
				lm_pi_approved_total = rs.getString("lm_pi_approved_total");
				lm_pi_approved_date =  rs.getString("lm_pi_approved_date");
				lm_pi_gp_paid_date =  rs.getString("lm_pi_gp_paid_date");
				lm_pi_invoice_number  =  rs.getString("lm_pi_invoice_number");
											
			} else {
				InvoiceStatus = "Not Found";
			}

			rs.close();
		}
		catch (Exception e) {
			System.out.println(e+" - with - "+sql);
		}

		return;
	}
	
	
	/*************************************************************************/

	/* Title: Update Invoice data
	 * 
	 * 
	 * 
	 * Revision History:
	 *   08/05/07 - 01.00 - Initial Release 
	 *  
	 */

	void updateInvoiceData () {

		
	}

	
	/*************************************************************************/

	/* Title: Insert Invoice data
	 * 
	 * 
	 * 
	 * Revision History:
	 *   08/05/07 - 01.00 - Initial Release 
	 *  
	 */

	void insertInvoiceData () {

		
	}

	/*************************************************************************/

	/* Title: Delete Invoice data
	 * 
	 * 
	 * 
	 * Revision History:
	 *   08/05/07 - 01.00 - Initial Release 
	 *  
	 */

	void deleteInvoiceData () {

		
	}

	/*************************************************************************/

	/* Title: Initialize Invoice Fields
	 * 
	 * Initialize the list of invoice fields - column names - used in queries to
	 * both Ilex and the Web.  The intent is to automate access to/from respective
	 * databases
	 * 
	 * Revision History:
	 *   08/05/07 - 01.00 - Initial Release 
	 *  
	 */

	void initializeInvoiceFields () {
		
		
		InvoiceFields.add(0, "lm_po_VendorID");
		InvoiceFields.add(1, "lm_pi_id");
		InvoiceFields.add(2, "lm_pi_status");
		InvoiceFields.add(3, "lm_pi_submit_type");
		InvoiceFields.add(4, "lm_pi_invoice_number");
		InvoiceFields.add(5, "lm_pi_invoice_date");
		InvoiceFields.add(6, "lm_pi_date_received");
		InvoiceFields.add(7, "lm_pi_submitter");
		InvoiceFields.add(8, "lm_pi_total");
		InvoiceFields.add(9, "lm_pi_partner_comments");
		InvoiceFields.add(10, "lm_pi_reconcile_comments");
		InvoiceFields.add(11, "lm_pi_approved_total");
		InvoiceFields.add(12, "lm_pi_approved_by");
		InvoiceFields.add(13, "lm_pi_approved_date");
		InvoiceFields.add(14, "lm_pi_gp_paid_date");
		InvoiceFields.add(15, "lm_pi_gp_check_number");
		InvoiceFields.add(16, "lm_po_terms");
		InvoiceFields.add(17, "lm_po_number");
		InvoiceFields.add(18, "lm_po_issue_date");
		InvoiceFields.add(19, "lm_po_authorised_total");
		InvoiceFields.add(20, "lm_po_deliverables");
		InvoiceFields.add(21, "lm_po_js_id");
		InvoiceFields.add(22, "site_number");
		InvoiceFields.add(23, "scheduled");
		InvoiceFields.add(24, "job_start");
		InvoiceFields.add(25, "job_end");
		InvoiceFields.add(26, "contingent_contact");
		InvoiceFields.add(27, "contingent_contact_phone");
		InvoiceFields.add(28, "email_address");
		InvoiceFields.add(29, "lm_pi_minuteman_status");
 
		return;
	}
	
	/******************************************************************/

	public static void main(String[] args) {
    
	}

}
