package net.contingent.einvoice;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
 
//import net.contingent.eisync.Logger;

public class Transaction {

	boolean propertyFileStatus = false;
	
	Vector fields = new Vector ();
	Vector values = new Vector ();
	
	Map dtype = new HashMap ();
	Map dataTypeRef = new HashMap();
	Map io = new HashMap ();
	Map validation = new HashMap();
	 
	Properties properties = null;
	
	static DataAccess da = new DataAccess();
	
	DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	static long NET60_TERM1 = (1000*60*60*24)*45;
	static long NET60_TERM2 = (1000*60*60*24)*60;
	static long NET60_TERM3 = (1000*60*60*24)*120;
	
	/*************************************************************************/

	/* Title: 
	 * 
	 * Responsible for database interactions on an action by action basis.  To the
	 * extent possible, database operations are automated based on form action name.
	 * The action name is translated to the information needed to support all database 
	 * interactions. 
	 * 
	 * Inputs: 
	 * 
	 * action - as used by struts action mapping
	 * 
	 * Returns: 
	 *  
	 */

	public Transaction (String action) {
		
		// Initialize 
		initConstants ();
		
		
		
		String pwd = "/webapps/eInvoice/WEB-INF/pfiles/";
		String pfile_test = System.getProperty("catalina.base");
		
        // Get property file
				
		if (initializeProperties (pfile_test+pwd+action+".txt")) {
           // Process property file
		   if (getFields ()) {
			   propertyFileStatus = true;
		   }
		}	
	}

	public Transaction () {
		
		
	}

	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * Read property file
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */
	
	boolean getFields () {
		
		boolean rtn_status = true;
		
		String[] fl = properties.getProperty("fields").split(",");
		
		if (fl.length > 0) {
			for (int i=0; i<fl.length; i++) {
				fields.add(i, (fl[i].trim()));
				// Get map data for this field from property.
				setMap(fl[i]);
			}
		} else {
			rtn_status = false;
		}
		
	    return rtn_status;
	}
	
	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * Set maps for each field based on property file.
	 * 
	 * Three properties are defined for each field:
	 * 
	 * Data Type
	 * Input/Output
	 * Validation
	 * 
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */
	
	void setMap (String field) {
		
		dtype.put(field, properties.getProperty(field+"_TYPE"));
		io.put(field, properties.getProperty(field+"_IO"));
		validation.put(field, properties.getProperty(field+"_VAL"));
	}
	
	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * Read property file
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */
	
	boolean initializeProperties (String pf) {
		
		boolean rtn_status = true;
		
		properties = new Properties();
		 
	    try {
	        properties.load(new FileInputStream(pf));
	    } catch (IOException e) {
	    	rtn_status = false;	    	
	    }

	    return rtn_status;
	}
	
	
	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * Move request data to Transaction class
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */
	
	public void moveRequestData (HttpServletRequest request) {
		
		String fld;
		String val;
		String typ;
		
		for (int i=0; i<fields.size(); i++) {
			fld = (String)fields.get(i);
			val = request.getParameter(fld);
			typ = (String)dtype.get(fld);
			values.add(i, filterRequestData(fld, typ, val));
		}
	}

	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * Remove and $ or < that may be submitted with the data
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */
	
	public void updateInvoiceEntryFields (String[] money_list) {
		
		int index;
		String val;
		
		for (int i=0; i<money_list.length; i++) {
		
			if (values.contains(money_list[i])) {
				index = values.indexOf(money_list[i]);
			    val = (String)values.get(index);
			   
				val = val.replace("$", "");
				val = val.replace(",", "");
			
				values.set(index, val);
			}
		}
		
	}
	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * Test version  of this method
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */
	
	public void moveRequestData (Map test_request) {
		
		String fld;
		String val;
		String typ;
		
		for (int i=0; i<fields.size(); i++) {
			fld = (String)fields.get(i);
			val = (String)test_request.get(fld);
			typ = (String)dtype.get(fld);
			values.add(i, filterRequestData(fld, typ, val));
		}
	}
	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * Perform basic data filtering/cleanup.  Blank data is treated as null
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */
	
	/*
	   dataTypeRef.put("bit", Types.BIT);
	   dataTypeRef.put("boolean", Types.BOOLEAN);
	   dataTypeRef.put("char", Types.CHAR);
	   dataTypeRef.put("date", Types.TIMESTAMP);
	   dataTypeRef.put("decimal", Types.DECIMAL);
	   dataTypeRef.put("double", Types.DOUBLE);
	   dataTypeRef.put("float", Types.FLOAT);
	   dataTypeRef.put("int", Types.INTEGER);
	   dataTypeRef.put("null", Types.NULL);
	   dataTypeRef.put("numeric", Types.NUMERIC);
	   dataTypeRef.put("varchar", Types.VARCHAR);
	 */	

	String filterRequestData (String fld, String typ, String val) {

		if (typ.endsWith("char")) {
			// Filter character data
			if (val != null) {
				val = val.trim();
				if (val.length() == 0) {
					val = null;
				}else {
					val.replaceAll("'", "''");  // Filter out single quotes
				}
			} 

		} else if (typ.equals("date"))  {
			// Filter dates
			if (val != null) {
				val = val.trim();
				if (val.length() == 0) {
					val = null;
				}
			} 

		} else {
			// Filter numeric data
			if (val != null) {
				val = val.trim();
				if (val.length() == 0) {
					val = null;
				}
			} else {
				val = null;
			}
		}

		return val;		
	}
	
	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * Execute the SP call and return the results.
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */

	Map executeSP () {

		Map rtn_values = new HashMap ();

		String sp = "{call "+properties.getProperty("sp")+"}";

		String name = "";
		String type = "";
		String value = "";

		int in = 1;
		int out = 1;

		try {
			CallableStatement stmt = da.databaseConnection.prepareCall(sp);

			/*
			 * Iteratate through each field to determine if it is an input, output or both 
			 * for the SP. The order is based on the fields vector.
			 */

			for (int i=0; i<fields.size(); i++) {

				// Get parameter name, value, and type
				name = (String)fields.get(i);
				type = (String)dtype.get(name);
				
				value = ((String)values.get(i));
                if (value != null) {
                	value = value.trim();
                }
				//Set input parameter
				if (((String)io.get(name)).contains("in")) {
					setInputParameter (in, value, type,  stmt);
				
				}

				if (((String)io.get(name)).contains("out")) {
					setOutputParameter (in, type,  stmt);
				
				}
				in++;
			}

			// Execute the SP
			stmt.execute();

			/*
			 * Repeat the above but this time only for output parameters.  
			 */
			
			out = 1;
			//System.out.println("List of output parameters:");
			for (int i=0; i<fields.size(); i++) {
				name = (String)fields.get(i);
				type = (String)dtype.get(name);
			
				if (((String)io.get(name)).contains("out")) {
					Object x = getOutputParameter (out, stmt);
					rtn_values.put(name, x);
				
				} 
				out++;
			}
			stmt.close();
		}
		
		catch (Exception e) {
			System.out.println(e);
		}

		return rtn_values;		
	}

	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * Execute the SP call and return the results.
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */

	Map executeSP (String pk) {

		Map rtn_values = new HashMap ();

		String sql = "exec "+properties.getProperty("sp")+" "+pk;
		String name = "";
		String type = "";
		
		try {

			ResultSet rs = da.getResultSet(sql);
			
			/*
			 * Iteratate through each field to be returned
			 */
			
			if (rs.next()) {
				for (int i=0; i<fields.size(); i++) {
					name = (String)fields.get(i);
					type = (String)dtype.get(name);

					if (((String)io.get(name)).contains("select")) {
						rtn_values.put(name, getSelectParameter (name, type, rs));
						
					}
				}
			}
			rs.close();
		}

		catch (Exception e) {
			System.out.println(e);
		}

		return rtn_values;		
	}
	
	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * Execute invoice specific search and return strucutre results based
	 * invoice status
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */


	Map invoiceSearch (String wc, String sc) {

		Map rtn_values = new HashMap ();
        String view = "select * from lm_envoice_search_results_v02";
        String temp = "";
 		Vector pending = new Vector();
 		Vector received = new Vector();
 		Vector approved = new Vector();
 		Vector paid = new Vector();
 		long td = 0;
 		
 		Map personnel = getPersonnelList();
		
		try {
			view = view+" "+wc+" "+sc;
			ResultSet rs = da.getResultSet(view);
			System.out.println(view);
			while (rs.next()) {
				String[] row = new String[24];
				
				// Default late pay indicator
				row[23] = "N";
				row[0] = rs.getString(1);
				row[1] = rs.getString(2);
				row[2] = rs.getString(3);
				row[3] = replaceBlanks(rs.getString(4));
				row[4] = replaceBlanks(rs.getString(5));
				row[5] = replaceBlanks(rs.getString(6));;
				row[6] = rs.getString(7);
				row[7] = rs.getString(8);
				row[8] = rs.getString(9);
				row[9] = rs.getString(10);
				row[10] = rs.getString(11);
				row[11] = rs.getString(12);
				row[12] = rs.getString(13);
				row[13] = rs.getString(14); 
				row[14] = rs.getString(15);
				
				// Owner
				temp = rs.getString(16);
				if (rs.wasNull()) {
					temp = "999999";
				}
				
				if (personnel.get(temp) != null) {
					row[15] = (String)personnel.get(temp);
				} else {
					row[15] = "N/A";
				}
				
                // PM
				temp = rs.getString(17);
				if (rs.wasNull()) {
					temp = "999999";
				}
				if (personnel.get(temp) != null) {
					row[16] = (String)personnel.get(temp);
				} else {
					row[16] = "N/A";
				}
				
				row[17] = replaceBlanks(rs.getString(18));  
				row[18] = replaceBlanks(rs.getString(19));  //site
				row[19] = rs.getString(20);  //start
				row[20] = rs.getString(21);  //end
				
				row[21] = rs.getString(22);  //Invoice #
				row[22] = rs.getString(23);  //Invoice date
				
				if (row[0].contains("Pending")) {
					pending.add(row);
				} else if (row[0].contains("Received")) {
					received.add(row);
				} else if (row[0].contains("Approved")) {
					// Check how long this is approved - may be overdue
					
					try {
						Date x1 = df.parse(row[10]);
						Date x2 = new Date();
						td = (x2.getTime()-x1.getTime())/((1000*60*60*24));
						if (td >= 45 && td < 60){
							row[23] = "Y";
						} else if (td >= 60 && td < 120) {
							row[23] = "R";
						} else if (td >= 120) {
							row[23] = "N";
						} 
					} catch (Exception e) {
						row[23] = "N";
					}
					
					approved.add(row);
				} else if (row[0].contains("Paid")) {
					paid.add(row);
				}
			}
			
			if (pending.size()>0) {
				rtn_values.put("pending", pending);
			}
			if (received.size()>0) {
				rtn_values.put("received", received);
			}
			if (approved.size()>0) {
				rtn_values.put("approved", approved);
			}
			if (paid.size()>0) {
				rtn_values.put("paid", paid);
			}

			rs.close();
		}

		catch (Exception e) {
			System.out.println("invoiceSearch "+e);
		}

		return rtn_values;		
	}
	

	/**************************************************************************/

	/**
	 * Description: Get Line Items
	 *  
	 * Revision History
	 *   04/17/07 - 01.00 - Initial Release
	 * 
	 */

	Vector getLineItems (String lm_pi_id) {

		Vector rtn_values = new Vector ();
		String sql= "lm_partner_invoice_line_item_list_01 "+lm_pi_id;
	
		String temp = "";
		int i = 0;

		try {
			ResultSet rs = da.getResultSet(sql);
			while (rs.next()) {
				String[] row = new String[6];
				row[0] = rs.getString(1);
				row[1] = rs.getString(2);
				row[2] = rs.getString(3);
				row[3] = rs.getString(4);
				row[4] = rs.getString(5);
				row[5] = rs.getString(6);
				rtn_values.add(i++, row);
			}
			rs.close();
		}
		catch (Exception e) {
			System.out.println("Transaction.getLineItems "+sql);
			e.printStackTrace();
		}

		return rtn_values;		
	}
	
	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * Execute invoice specific search and return strucutre results based
	 * invoice status
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */


	Map getPersonnelList () {

		Map personnel =  new HashMap ();
		personnel.put("999999", "N/A");
        String sql = "select distinct cast(ma_op_pc_id as varchar(10)),ma_op_pc_last_name from ma_operations_personnel order by ma_op_pc_last_name";
		
		try {
			ResultSet rs = da.getResultSet(sql);			
			while (rs.next()) {
				personnel.put(rs.getString(1), rs.getString(2));
			}
			rs.close();
		}
		catch (Exception e) {
			System.out.println("getPersonnelList "+e);
		}

		return personnel;		
	}
	
	/**************************************************************************/

	/**
	 * Description: Get Email Details
	 *  
	 * Revision History
	 *   04/17/07 - 01.00 - Initial Release
	 * 
	 */

	Map getEmailDetails (String lm_pi_id) {

		Map rtn_values = new HashMap ();
		String cc_check = "";
		String sql= "lm_partner_invoice_get_email_data_01 "+lm_pi_id;
		
		
		try {
			ResultSet rs = da.getResultSet(sql);
			while (rs.next()) {

				rtn_values.put("PO Number", rs.getString("lm_po_number"));
				rtn_values.put("Delivery Date", rs.getString("lm_po_deliver_by_date"));
				rtn_values.put("Authorized Total", rs.getString("lm_po_authorised_total"));
				rtn_values.put("Invoice Total", rs.getString("lm_pi_total"));
				rtn_values.put("Invoice Number", rs.getString("invoice_number"));
				
				rtn_values.put("Customer", rs.getString("lo_ot_name"));
				
				rtn_values.put("Job", rs.getString("lm_js_title"));
				rtn_values.put("Site", rs.getString("lm_si_number"));
				rtn_values.put("Status", rs.getString("job_status"));
				rtn_values.put("Complete Date", rs.getString("job_complete_date"));
				
				rtn_values.put("Partner", rs.getString("lo_om_division_ptn"));
				
				rtn_values.put("First Name", rs.getString("lo_pc_first_name_owner"));
				rtn_values.put("To", rs.getString("lo_pc_email_owner"));
				
				// Per request from Dispatch - map cc to lead
				cc_check = rs.getString("lo_pc_email_pm");
				if (rs.wasNull()) {
					cc_check = "Not Available";
				} else if (cc_check.startsWith("cshroder")) {
					cc_check = "samen@contingent.net";
				}
				rtn_values.put("CC", cc_check);
				
				rtn_values.put("Approved Total", rs.getString("approved_total"));
				if (rs.wasNull()) {
					rtn_values.put("Approved Total", "Not Entered");
				}
			}
			rs.close();
		}
		catch (Exception e) {
			System.out.println("Transaction.getEmailDetails "+sql);
			e.printStackTrace();
		}

		return rtn_values;		
	}

	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * 
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */

	void setInputParameter (int i, String val, String typ,  CallableStatement stm) {

		try {
			if (typ.equals("date")){
				if (val == null){
					stm.setObject(i, val, Types.DATE);
				} else {
					DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
					stm.setObject(i, new Timestamp ((df.parse(val)).getTime()), Types.DATE);
				}
			} else if (val != null){
			 
				stm.setObject(i, val, (Integer)dataTypeRef.get(typ));	
			} else {
				stm.setNull(i, (Integer)dataTypeRef.get(typ));
			}
		} catch (Exception e) {
			System.out.println("SP input exception for "+i+" Error: "+e);
		}
	}


	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * 
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */

	void setOutputParameter (int i, String typ,  CallableStatement stm) {

		try {
			stm.registerOutParameter(i, (Integer)dataTypeRef.get(typ));
		} catch (Exception e) {
			System.out.println("SP output exception for "+i+" Error: "+e);
		}
	}
	
	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * 
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */

	Object getOutputParameter (int i, CallableStatement stm) {

		try {
			return stm.getObject(i);
		} catch (Exception e) {
			System.out.println("SP output exception for "+i+" "+e);
			return null;
		}
	}

	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * 
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */

	Object getSelectParameter (String fld, String typ, ResultSet rs) {

		try {
			if (typ.contains("char")) {
				return rs.getString(fld);
			} else if (typ.equals("date")) {
				return rs.getString(fld);
			} else {
				return rs.getInt(fld);
			} 
		} catch (Exception e) {
			System.out.println("SP output exception for "+fld+" "+e);
			return null;
		}
	}

	/**************************************************************************/

	/**
	 * Description: 
	 * 
	 * 
	 *  
	 * Version
	 * 
	 * 1.00 03/05/07 Initial release
	 * 
	 */

	String replaceBlanks (String string_value) {
	  
		if (string_value != null) {
			string_value = string_value.replaceAll("\\s", "&nbsp;");
		}
      return string_value;
	}
	
	/*************************************************************************/

	/* Title: initConstants
	 * 
	 * Description: Initiaze various constants
	 * 
	 * Inputs: N/A
	 * 
	 * Returns: N/A
	 */

	void initConstants () {

		dataTypeRef.put("bit", Types.BIT);
		dataTypeRef.put("boolean", Types.BOOLEAN);
		dataTypeRef.put("char", Types.CHAR);
		dataTypeRef.put("date", Types.TIMESTAMP);
		dataTypeRef.put("decimal", Types.DECIMAL);
		dataTypeRef.put("double", Types.DOUBLE);
		dataTypeRef.put("float", Types.FLOAT);
		dataTypeRef.put("int", Types.INTEGER);
		dataTypeRef.put("null", Types.NULL);
		dataTypeRef.put("numeric", Types.INTEGER);
		dataTypeRef.put("varchar", Types.VARCHAR);
 
	}

	public static int updateApprovedTotal(String purchaseOrder,
			String approvedTotal) {
				String sql = "UPDATE dbo.lm_po_reconciliation_details SET lm_recon_approved_total="+approvedTotal+" WHERE lm_recon_po_id ="+purchaseOrder;
				int result = da.executeUpdateQuery(sql);
				return result;
	}
	public static String getRole(String userid) {
		String role = "N";
		String sql = "select 'Y' as role from lo_poc_rol LEFT  join lo_role on lo_pr_ro_id = lo_ro_id " +
				"LEFT JOIN lo_poc ON lo_pc_id = lo_pr_pc_id " +
				"LEFT  join lo_classification on lo_pc_role = lo_cl_id " +
				" where lo_pr_pc_id ="+userid+" AND lo_cl_role = 'Accounting' ";
		try{
			ResultSet rs = da.getResultSet(sql);
			if(rs.next()){
				role = rs.getString("role");
			}
		}catch(Exception ee){
		}
		return role;
	}
}
