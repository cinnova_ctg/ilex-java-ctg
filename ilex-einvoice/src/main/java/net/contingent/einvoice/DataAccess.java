package net.contingent.einvoice;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DataAccess {

	public static Connection databaseConnection = null;
	
	private Statement sql;

	public DataAccess () {
		
		if (databaseConnection == null) {
			// Get Classes MS JDBC Driver (2005)
			 try              
			  {
			   Class.forName("net.sourceforge.jtds.jdbc.Driver");
			  }
			  catch (Exception e)
			  {
			   System.out.println("Class.forName Failed with error: "+e);
			  }	
			  
			  // Get connection for ILEX
			  try   
			   {  
			    //String db_url = "jdbc:jtds:sqlserver://192.168.32.8:1433/ilexTestRelease2_1;user=ilex;password=ilex";
				String db_url = "jdbc:jtds:sqlserver://192.168.32.53:1433/ilex;user=ilex;password=0s1adFi!";
//				  String db_url = "jdbc:jtds:sqlserver://192.168.0.150:1433/ilex;user=cinnova-ilex;password=farid12345";
			    //String db_url = "jdbc:sqlserver://192.168.20.52:1433;databaseName=ilex;User=sa;Password=Wawd01t";
				//String db_url = "jdbc:sqlserver://localhost:1433;databaseName=ilexTestRelease1;User=sa;Password=unlenn";
			    databaseConnection = DriverManager.getConnection (db_url); 
			    System.out.println("Connection established for eInvoice");
			   }
			   catch (Exception sqle)
			   {
			     System.out.println("Connection Failed with error: "+sqle);
			   }
	    }
	}
	

	/*************************************************************************/

	/* Title: getResultSet
	 * 
	 * Description: Generic sql execution
	 * 
	 * Inputs: sql
	 * 
	 * Returns: result set
	 */

	public ResultSet getResultSet (String sql_to_execute) {

		ResultSet rs_return = null;

		try {
			sql = databaseConnection.createStatement();
			rs_return = sql.executeQuery(sql_to_execute);
		}
		catch (Exception e) {
			System.out.println(e);
			rs_return = null;
		}

		return rs_return;
	}
	
        
        public void reconnect(){
		if (databaseConnection == null) {
			// Get Classes MS JDBC Driver (2005)
			 try {
			   Class.forName("net.sourceforge.jtds.jdbc.Driver");
			  }
			  catch (Exception e) {
			   System.out.println("Class.forName Failed with error: "+e);
			  }	
			  
			  // Get connection for ILEX
			  try {  
                            String db_url = "jdbc:jtds:sqlserver://192.168.32.53:1433/ilex;user=ilex;password=0s1adFi!";
			    databaseConnection = DriverManager.getConnection (db_url); 
			    System.out.println("Connection established for eInvoice");
			   }
			   catch (Exception sqle) {
			     System.out.println("Connection Failed with error: "+sqle);
			   }
	    }
        }
        
	/*************************************************************************/

	/* Title: Execute non-Result Set Query
	 * 
	 * Description: Generic sql execution
	 * 
	 * Inputs: sql
	 * 
	 * Returns: result set
	 */

	public boolean executeQuery (String sql_to_execute) {

                reconnect();
		boolean executeStatus = false;
		
		try {
			sql = databaseConnection.createStatement();
			executeStatus = sql.execute(sql_to_execute);
		}
		catch (Exception e) {
			System.out.println(e+" "+sql_to_execute);
			return executeStatus;
		}

		return executeStatus;
	}
	
	/* Title: Execute Update Set Query
	 * 
	 * Description: Generic sql execution
	 * 
	 * Inputs: sql
	 * 
	 * Returns: int
	 */

	public int executeUpdateQuery (String sql_to_execute) {

		int executeStatus = 0;
		
		try {
			sql = databaseConnection.createStatement();
			executeStatus = sql.executeUpdate(sql_to_execute);
		}
		catch (Exception e) {
			System.out.println(e+" "+sql_to_execute);
			return executeStatus;
		}

		return executeStatus;
	}
	

	/*************************************************************************/

	/* Title: Get Vales for Menu with ~ delimiter
	 * 
	 * Description: Generic sql execution
	 * 
	 * Inputs: sql
	 * 
	 * Returns: result set
	 */

	public String getValues1 (String sql_to_execute) {

		String values = "";
		String delimiter = "";
        
		try {
			sql = databaseConnection.createStatement();
			ResultSet rs = sql.executeQuery(sql_to_execute);
			while (rs.next()) {
				values += delimiter+rs.getString(1);
				delimiter = "~";
			}
		}
		catch (Exception e) {
			System.out.println(e);
			values = null;
		}

		return values;
	}
	
	public static void main(String[] args) {
		
		DataAccess x = new DataAccess();

	}
}