package net.contingent.einvoice;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
 
public class PurchaseOrder extends DispatchAction{
	
	/*************************************************************************/
	
	/* Summary of Chnages
	 * 
	 * See indivdual methods for details
	 * 
	 * 
	 */
	
	/*************************************************************************/

	Transaction xtrans;
	
	Vector searchParameters = new Vector();
	
	Map  spType  = new HashMap ();
	
	Map reportTitle = new HashMap ();
	
	Vector lineItems = new Vector(20);

	String na_lm_pi_id = "";
	
	String next_action  = "";
	
	String deliverable_status  = "";
	
	/*************************************************************************/

	/* Title: Display Internal Entry Form
	 * 
	 * Get core PO and related data and display entry form.  This may also require 
	 * getting data previously entered by the PC
	 * 
	 * Inputs: HTTP Request
	 * 
	 * Returns: HTTP Request Attributes
	 * 
	 * Revision History:
	 *   04/18/07 - 01.00 - Initial Release
	 */
	
	public ActionForward internal_invoice_entry_display (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response ) throws Exception {

		String action = "internal_invoice_entry_display";

		if (!getCorePOData (request)) {
			action = "FAILURE";
		} 	 
				
		return mapping.findForward(action);
	}
	
	/*************************************************************************/

	/* Title: Update Invoice Entry (Internal Version) for Receive Action
	 * 
	 * Update an invoice entry when received.  This includes line items
	 * 
	 * Inputs: 
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *   04/18/07 - 01.00 - Intial Release
	 */

	public ActionForward internal_invoice_entry ( ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response ) throws Exception {

		String action = "internal_invoice_entry";
		String lm_pi_id = "";
		String lm_po_id = "";
		
		// The insert action may return as Normal meaning it was received or 
		// Approved meaning it was received, but updated to Approved based on 
		// received state.
		
		String rtn_status = insertInvoice (action, request);
		
		if (!rtn_status.equals("FAILED")) {
			
			lm_pi_id = getRequestData(request, "lm_pi_id");
			lm_po_id = getRequestData(request, "lm_po_id");
			
		    // Initialize data strucuture used to retrieve line items
			initLineItems ();
			
			// Test for line items and process if found.
			if (getLineItems(request)) {
				// process line items
				processLineItems(lm_pi_id, request);
			}
			
			//setting up URL to send email			
			String url  = "http://"+request.getServerName()+":"+request.getServerPort()+"/eInvoice/";
			
            // Send email
			if (rtn_status.equals("Normal")) {
				sendEmailApprove (lm_pi_id, lm_po_id, 1,url);
			} else {
				sendEmailApprove (lm_pi_id, lm_po_id, 2,url);
				sendEmailPay (lm_pi_id, lm_po_id);
			}

		} else {
			
			action = "FAILURE";
		}
		
		return mapping.findForward(action);
	}

	
	/*************************************************************************/

	/* Title: Display an Invoice for Approval
	 * 
	 * Get core data.
	 * Get invoice data
	 * Format and send to page
	 * 
	 * Inputs: lm_pi_id for the target invoice (in request)
	 * 
	 * Returns: See function calls
	 * 
	 * Revision History:
	 *  03/20/07 - Initial Release
	 */

	public ActionForward approve_display ( ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response ) throws Exception {

		String action = "approve_display";
		String response_page = "FAILURE";

		if (getCorePOData (request)) {
			
			// Get invoice data
			if (getApproveData (request)) {
				response_page = action;
			}
		}
		
		return mapping.findForward(response_page);
	}

	
	/*************************************************************************/

	/* Title: Process Approve Invoice
	 * 
	 * Process an approved invoice
	 * 
	 * Inputs: 
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *   04/18/07 - 01.00 - Initial Release
	 */

	public ActionForward approve_execute (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response ) throws Exception {

		String action = "approve_execute";
		String lm_po_id = getRequestData(request, "lm_po_id");
		String lm_pi_id = getRequestData(request, "lm_pi_id");

		if (!excuteTransaction(action, request)) {
			action = "FAILURE";
		} else {
			// Successful - now send email.
			sendEmailPay (lm_pi_id, lm_po_id);
		}
			
		return mapping.findForward(action);
	}

	/*************************************************************************/

	/* Title: Display an Invoice for Payment
	 * 
	 * Get core data.
	 * Get invoice data
	 * Format and send to page
	 * 
	 * Inputs: lm_pi_id for the target invoice (in request)
	 * 
	 * Returns: See function calls
	 * 
	 * Revision History:
	 *  03/20/07 - 01.00 - Initial Release
	 */

	public ActionForward pay_display ( ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response ) throws Exception {

		String action = "pay_display";
		String response_page = "FAILURE";

		if (getCorePOData (request)) {
			
			// Get invoice data
			if (getApproveData (request)) {
				response_page = action;
			}
		}
		
		return mapping.findForward(response_page);
	}

	/*************************************************************************/

	/* Title: Process Pay Request
	 * 
	 * Process a pay action
	 * 
	 * Inputs: 
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *   04/18/07 - 01.00 -Initial Release
	 */

	public ActionForward pay_execute ( ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response ) throws Exception {

		String action = "pay_execute";		
		
		if (!excuteTransaction ("invoice_paid", request)) {
			// Perform detail inserts
			action  = "FAILURE";
		}
		
		return mapping.findForward(action);
	}
	
	/*************************************************************************/

	/* Title: PO Search by PO number
	 * 
	 * Search for PO in order to locate a partner invoice information and the next action associated
	 * with this PO/invoice.  Based on the status, display next action
	 * 
	 * Inputs: lm_po_number
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *   04/18/07 - 01.00 - Initial Release
	 */
	
	public ActionForward partner_po_serach (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response ) throws Exception {

		String response_page = "FAILED";

		if (request.getParameter("lm_po_number") != null) {
			
			response_page = performSearch (request);
		}
		
		return mapping.findForward(response_page);
	}	

	/*************************************************************************/

	/* Title: Perform Search and Generate Reports
	 * 
	 * Overview:
	 * 
	 * Get
	 * 
	 * Inputs: 
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *  
	 */
	
	public ActionForward invoice_search (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response ) throws Exception {
        
		String sc = "";
		String wc = "";
		String rt = "";
		Transaction xtrans = new Transaction();
		
		// Determine the type of execution
		String response_page = "invoice_search";
		String search_type = request.getParameter("searchtype");
		String search_year = request.getParameter("yyyy");
		String searchSalesAgentFlag = "0";
		
		if(request.getParameter("SalesAgentFlag")!=null){
			searchSalesAgentFlag = request.getParameter("SalesAgentFlag");
		}
		
		initSearchParameters ();
		
		if (search_type.startsWith("rpt")) {
			// 
			if (search_type.equals("rpt3")) {
				sc = "order by lm_pi_approved_date";
			} else {
				sc = "order by lm_pi_status, lm_po_number";
			}
			
			wc = getReportWhereClause (search_type, search_year, searchSalesAgentFlag);
			rt = "Invoice Report for "+(String)reportTitle.get(search_type)+" ("+search_year+")";
			request.setAttribute("search_results", xtrans.invoiceSearch(wc, sc));
			request.setAttribute("report_title", rt);
			
			
		} else {
			
			Vector requestSP = new Vector();
			requestSP = getSearchParameters (request);
			
			if (requestSP.size()>0) {
				
				wc = buildWhere(request, requestSP, searchSalesAgentFlag);
				sc = buildSort(requestSP);
				rt = "Invoice Search - Results by "+determineReportTitle(requestSP);
				request.setAttribute("search_results", xtrans.invoiceSearch(wc, sc));
				request.setAttribute("report_title", rt);

			} else {
				
			  // Need some search criteria
			  request.setAttribute("error_message", "No search criteria was provided.  Enter the required search criteria and try again.");
			  response_page = "FAILURE";
			}
		}
		
		request.setAttribute("search_type", search_type);
		request.setAttribute("SalesAgentFlag", searchSalesAgentFlag);
		return mapping.findForward(response_page);
	}
	
	/*************************************************************************/

	/* Title: Display Update Form
	 * 
	 * Inputs: HTTP Request
	 * 
	 * Returns: HTTP Request Attributes
	 * 
	 * Revision History:
	 *   07/08/07 - 01.00 - Initial Release
	 */
	
	public ActionForward update_form (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response ) throws Exception {

		String action = "update_form";
		String lm_pi_id = request.getParameter("lm_pi_id");
		
		if (lm_pi_id != null) {
			if (!getUpdateData (request, lm_pi_id)) {
				
				action = "FAILURE";
			} 	
		}
				
		return mapping.findForward(action);
	}

	/*************************************************************************/

	/* Title: Display Update Form
	 * 
	 * Inputs: HTTP Request
	 * 
	 * Returns: HTTP Request Attributes
	 * 
	 * Revision History:
	 *   07/08/07 - 01.00 - Initial Release
	 */
	
	public ActionForward update_form_action1 (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response ) throws Exception {

		String action = "update_list";
		
		String lm_pi_id = request.getParameter("lm_pi_id");
		
		if (lm_pi_id != null) {
			
			Transaction xtrans = new Transaction ("update_form_action1");
			xtrans.moveRequestData (request);
			Map xrtn = xtrans.executeSP();
			
		}
				
		return mapping.findForward(action);
	}
	
	/*************************************************************************/

	/* Title: Invoice Summary
	 * 
	 * Display summary of invoice and related information
	 * 
	 * Inputs: 
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *   05/20/08 - 01.00 - Initial Release
	 */

	public ActionForward invoice_summary (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response ) throws Exception {

		String action = "invoice_summary";
		
		// Reset class variables
		next_action  = "";
		deliverable_status  = "";
		
		String lm_po_id = getRequestData(request, "lm_po_id");

		if (validateRequestInput(lm_po_id)) {
			
			// Get all PO and job realted information
			if (getCorePOData (request)) {
				
				// Get invoice summary
				getInvoiceSummary (request, lm_po_id);	
				
			}
		} else {
			action = "FAILURE";
		}

		return mapping.findForward(action);
	}
	
	/*************************************************************************/

	/* Title: Insert Invoice
	 * 
	 * Perform invoice receive function
	 * 
	 * Inputs: 
	 * 
	 * action - SE
	 * request - SE
	 * 
	 * Returns: 
	 * 
	 * rtn_status as FAILED, Normal, or Approved
	 * 
	 * Revision History:
	 *   04/18/07 - 01.00 - Initial Release
	 */
	
	String insertInvoice (String action, HttpServletRequest request) {
		
		String rtn_status = "FAILED";
		Integer rtn_code;
		Map xrtn;
		// Initialize transaction, move data, perform action
		
		Transaction xtrans = new Transaction (action);
		xtrans.moveRequestData (request);
		// Remove any $ or , that may be submitted with money values
		//xtrans.updateInvoiceEntryFields(money_list);
		xrtn = xtrans.executeSP();
		
		// Test results of database action
		
		rtn_code = (Integer)xrtn.get("return_status");
		if (rtn_code != null) {
			if (rtn_code == 0) {
				rtn_status = (String)xrtn.get("invoice_status");
			} else {
				//Set return error condition
				request.setAttribute("error_message", "Invoice transaction for "+action+" failed with an error code of "+rtn_code+".");
			}
            // Set return error condition
			request.setAttribute("error_message", "Invoice transaction for "+action+" failed with no error code returned.");
		}

		return rtn_status;
	}

	/*************************************************************************/

	/* Title: Generic Transaction for Approve and Pay
	 * 
	 * Execute SP for Approve and Pay actions
	 * 
	 * Inputs: 
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *   04/18/07 - 01.00 - Initial Release
	 */
	
	boolean excuteTransaction (String action, HttpServletRequest request) {
		
		boolean rtn_status = false;
		Integer rtn_code;

		Map xrtn;
		// Initialize transaction, move data, perform action

		Transaction xtrans = new Transaction (action);
		xtrans.moveRequestData (request);
		xrtn = xtrans.executeSP();

		// Test results of database action

		rtn_code = (Integer)xrtn.get("return_status");
		
		if (rtn_code != null) {
			if (rtn_code == 0) {
				rtn_status = true;
			} else {
				//Set return error condition
				request.setAttribute("error_message", "Transaction attempted for "+action+"-1 failed with an error code of "+rtn_code+".");
				System.out.println("PurchaseOrder.excuteTransaction: Transaction attempted for "+action+" failed with an error code of "+rtn_code+".");
			} 
		} else {
			// Set return error condition
			request.setAttribute("error_message", "Transaction attempted for "+action+" failed with no error code returned.");
			System.out.println("PurchaseOrder.excuteTransaction: Transaction attempted for "+action+" failed with no error code (null).");
		}
	return rtn_status;
	}
	
	/*************************************************************************/

	/* Title: Get Core Data
	 * 
	 * Get the core job/Po data as well as the invoice data.  This uses two calls. The 
	 * first call retrieves the job/PO data.  if successful, the invoice data is retrieved
	 * 
	 * Inputs: HTTP Request - see usage below
	 * 
	 * Returns: Request Attribute - see usage below
	 * 
	 * Revision History:
	 *   03/20/07 - Initial Release
	 */
	
	boolean getCorePOData (HttpServletRequest request)  {
		
		boolean rtn_status = true;
		String po_status = "";
		
		Transaction xt = new Transaction ("core_invoice_data");
		
		String lm_po_id = getRequestData(request, "lm_po_id");
		String lm_pi_id = getRequestData(request, "lm_pi_id"); // Some times this will be supplied
		
		HttpSession session = request.getSession();
		String userid = (String)session.getAttribute("userid");
		System.out.println("userid in getCorePOData()="+userid);
		String role = getRole(userid);	
		request.setAttribute("role", role);
		if (lm_po_id != null) {
			
			request.setAttribute("lm_po_id", lm_po_id);
			Map core_invoice_data = xt.executeSP(lm_po_id); 
			
			request.setAttribute("coreData", core_invoice_data);
			deliverable_status = (String)core_invoice_data.get("deliverables");
			lm_pi_id = (String)core_invoice_data.get("lm_pi_id");
			
			// Get invoice data if available
			
			if (lm_pi_id !=  null && !lm_pi_id.equals("")) {
				
				request.setAttribute("lm_pi_id", lm_pi_id);
				
				if (!getInvoiceData(request, lm_pi_id)) {
					request.setAttribute("error_message", "The requested invoice information was not properly formatted and/or identified.  (Error from getInvoiceData returning to getCoreData1)");
					rtn_status = false;	
				}
				
			} else {
				
				po_status = (String)core_invoice_data.get("po_status");
				
				if (po_status !=  null && !po_status.equals("Complete")) {
					
					request.setAttribute("error_message", "The eInvoice is not available until the PO is marked complete.");
					request.setAttribute("next_action", "Pending PO Completion");
					rtn_status = false;

				} else {
					request.setAttribute("error_message", "The requested invoice information was not found.  Verify the PO number is correct and try again.  (Error from getCoreData2)");
					rtn_status = false;
				}
			}
			
		} else {
			request.setAttribute("error_message", "The requested invoice information was not properly formatted and/or identified.  (Error from getCoreData3)");
			rtn_status = false;
		}

		return rtn_status;
	}
	
	/*************************************************************************/

	/* Title: Get Approve Data
	 * 
	 * Get the data need for approving an invoice during the approval process.
	 * 
	 * Use the standard data retrieve functionality to get data and then 
	 * return in attribute based on the action name.
	 * 
	 * Inputs: request - see usage below
	 * 
	 * Returns: request - see usage below
	 * 
	 * Revision History:
	 *  03/20/07 - Initial Release
	 *  
	 */
	
	boolean getApproveData (HttpServletRequest request)  {
		
		boolean rtn_status = true;
		
		Transaction xtrans = new Transaction ("approve_display");
		
		String lm_pi_id = getRequestData(request, "lm_pi_id");
		
		if (lm_pi_id != null) {
			request.setAttribute("lm_pi_id", lm_pi_id);
			request.setAttribute("approve_display", xtrans.executeSP(lm_pi_id));
			request.setAttribute("line_items", xtrans.getLineItems(lm_pi_id));
		} else {
			request.setAttribute("error_message", "The invoice identifier was not supplied for this request (approve_display).");
			rtn_status = false;
		}

		return rtn_status;
	}
	
	
	/*************************************************************************/

	/* Title: Get Approve Data
	 * 
	 * Get the data need for approving an invoice during the approval process.
	 * 
	 * Use the standard data retrieve functionality to get data and then 
	 * return in attribute based on the action name.
	 * 
	 * Inputs: request - see usage below
	 * 
	 * Returns: request - see usage below
	 * 
	 * Revision History:
	 *  03/20/07 - Initial Release
	 *  
	 */
	
	boolean getApproveLineItems (HttpServletRequest request)  {
		
		boolean rtn_status = true;
		
		Transaction xtrans = new Transaction ("get_line_items");
		
		String lm_pi_id = getRequestData(request, "lm_pi_id");
		
		if (lm_pi_id != null) {
			request.setAttribute("lm_pi_id", lm_pi_id);
			request.setAttribute("approve_display", xtrans.executeSP(lm_pi_id));
		} else {
			request.setAttribute("error_message", "The invoice identifier was not supplied for this request.");
			rtn_status = false;
		}

		return rtn_status;
	}
	
	/*************************************************************************/

	/* Title: Display Change Staus Form
	 * 
	 * Get core PO and invoice data and display entry form. 
	 * 
	 *  Two version:
	 *  
	 *  1) Form to enter initial PO number
	 *  
	 *  2) Form to change the status
	 * 
	 * Inputs: HTTP Request with PO number as lm_po_id
	 * 
	 * Returns: HTTP Request Attributes
	 * 
	 * Revision History:
	 *   05/13/08 - 01.00 - Initial Release
	 */
	
	public ActionForward change_status_display (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response ) throws Exception {
        
		String action = "change_status_display";
		String lm_po_id = getRequestData(request, "lm_po_id");

		if (validateRequestInput(lm_po_id)) {

			if (getCorePOData (request)) {
				action = "change_status_display";
				// Based on invoice status - determine allowed to transitions
				request.setAttribute("allowed_change_to_status", getAllowedStatusChange((String)request.getAttribute("lm_pi_status")));
			} else {
				action = "FAILURE";
			}
		} else {
			lm_po_id = null;
			request.removeAttribute("lm_po_id");
		}
		
		return mapping.findForward(action);
	}
	
	/*************************************************************************/

	/* Title: Change Staus Action
	 * 
	 * Process status change 
	 * 
	 *  1) Validate require request parameters
	 *  
	 *  2) Implement status change
	 *  
	 *  3) Send notifications
	 * 
	 * Inputs: HTTP Request 
	 * 
	 * Reuired parameters:
	 * 
	 *                   lm_pi_id
	 *                   new_invoice_status
	 *                   lm_po_id
	 * 
	 * Returns: HTTP Request Attributes
	 * 
	 * Revision History:
	 *   05/13/08 - 01.00 - Initial Release
	 */
	
	public ActionForward change_status_action (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response ) throws Exception {
		
		boolean mail_status = true;
		boolean request_status = true;
		
		String notify_owner = null;
		String notify_partner = null;

		String action = "change_status_action";
		
		String lm_pi_id = getRequestData(request, "lm_pi_id");
		String lm_po_id = getRequestData(request, "lm_po_id");
        String new_invoice_status = getRequestData(request, "new_invoice_status");
        
        request.removeAttribute("success_message");
        request.removeAttribute("error_message");
        
        if (validateRequestInput(lm_pi_id) && validateRequestInput(new_invoice_status) && validateRequestInput(lm_po_id)) {

        	// Perform requested status change
        	if (!new_invoice_status.equals("Error")) {

        		if (excuteTransaction (action, request)) {

        			// Get additional inout options

        			notify_owner = getRequestData(request, "notify_owner");
        			notify_partner = getRequestData(request, "notify_partner");

        			if (notify_owner.equals("Y")) {
        				if (!sendOwnwerNotification(new_invoice_status, lm_pi_id, lm_po_id)) {
        					request.setAttribute("error_message", "At least one email failed.  Double check the status of the eInvoice to verify it was changed as requested.");
        					mail_status = false;
        				}
        			}

        			// Only send partner when a partner action is potentially required.
        			if (notify_partner.equals("Y") && !new_invoice_status.equals("Received")) {
        				//sendPartnerNotification();
        				//mail_status = false;
        			}

        		} else {
        			// Transaction Error
        			request.setAttribute("error_message", "The status change failed.  Please check the PO number of your request.<br>If this problem persists, contact the Ilex administrator.");
        			request_status = false;
        		}	
        	} else {
        		// Request Error
        		request.setAttribute("error_message", "The status change is invalid.  Please check the PO number of your request.");
        		request_status = false;
        	}
        } else {
    		// Request Error
    		request.setAttribute("error_message", "The input parameters are invalid.  Please check the PO number of your request.");
    		request_status = false;
    	}
        
    	// Handle error message here - redisplay complete form
    	if (!request_status || !mail_status)  {

    		// Get the need infor for displaying form again
    		if (getCorePOData (request)) {
    			action = "change_status_display";
    			// Based on invoice status - determine allowed to transitions
    			request.setAttribute("allowed_change_to_status", getAllowedStatusChange((String)request.getAttribute("lm_pi_status")));
    		} else {
    			action = "FAILURE";
    		}
    	} else {
			request.removeAttribute("lm_po_id");
			request.setAttribute("lm_po_id","");
			request.setAttribute("success_message", "The status change request was successful for PO "+lm_po_id);
			lm_po_id = null;
    	}

        return mapping.findForward(action);
	}
	/*************************************************************************/

	/* Title: Perform PO Search
	 * 
	 * Find PO and direct to the appropriate action page based on the PO/invoice
	 * status  
	 * 
	 * Inputs: PO number as input by user
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *   04/10/07 - 01.0 - Intial Release
	 *  
	 */
	
	String performSearch (HttpServletRequest request) {
		
		String rtn_page = "";
		
		/*
		 * Execute search thru SP call.  The SP will return 
		 * in the returned Map
		 * 
		 * lm_pi_id
		 * lm_po_id
		 * lm_pi_status
		 * 
		 */ 
		Integer x = 0;
		
		// Add validation to test value for valid interger value
		
		xtrans = new Transaction ("partner_po_serach");
		xtrans.moveRequestData (request);
		Map xrtn = xtrans.executeSP();
		
		// Examine resutls to determine next action
		String lm_pi_status = (String)xrtn.get("lm_pi_status");
		String lm_po_id  = (String)xrtn.get("lm_po_id");
		String lm_pi_id  = (String)xrtn.get("lm_pi_id");
		
		if (lm_pi_status != null && !lm_pi_status.equals("Not Found")) {
			
			// If PO was found the following data was founds on needs
			// to be put on the request for reference in one of the following
			// if conditions plus the jsp
			
			request.setAttribute("lm_po_id", lm_po_id);
			request.setAttribute("lm_pi_id", lm_pi_id);
			
			// Get core data which will be need for all statuses			
			getCorePOData (request);
			
			// Apply status unique business logic
			if (lm_pi_status.equals("Pending Receipt")) {
				
                // Redirect to receive page
				rtn_page = "pending_receipt_display";

			} else if (lm_pi_status.equals("Received")) {
				
				// Redirect to approve page
				if (getApproveData (request)) {
					rtn_page = "approve_display";
				}
				
			} else if (lm_pi_status.equals("Approved")) {
				
				// Redirect to pay page
				if (getApproveData (request)) {
					rtn_page = "pay_display";
				}
				
			} else if (lm_pi_status.equals("Partial Payment")) {

				// Currently not supported
				
			} else if (lm_pi_status.equals("Paid")) {	
				
				// Need a page here - currently the default page
				rtn_page = "paid_display";	
			}
			
		} else {
			
			rtn_page = "not_found";
		}
		
		return rtn_page;
	}

	/*************************************************************************/

	/* Title: Get Invoice Data
	 * 
	 * Get invoice data from from lm_partner_invoice.  Since this data is used in 
	 * form fields, each value is placed on the request so it can be accessed by the
	 * applicable form tag
	 * 
	 * Inputs: HTTP Request and specific invoice item
	 * 
	 * Returns: HTTP Request Attributes
	 * 
	 * Revision History:
	 *   04/18/2007 - 01.00 - Initial Release
	 */
	
	boolean getInvoiceData (HttpServletRequest request, String id)  {
		
		boolean rtn_status = true;
		xtrans = new Transaction ("invoice_details");
		
		Map invoicedata = xtrans.executeSP(id);
		
		Iterator key = invoicedata.keySet().iterator();
		
		// Make the individual fields available to the form fields
		
	    while (key.hasNext()) {
	        String fld = (String)key.next();
	        String value = (String)invoicedata.get(fld);
	        request.setAttribute(fld, value);
	    }

		request.setAttribute("invoiceData", xtrans.executeSP(id));

		return rtn_status;
	}

	/*************************************************************************/

	/* Title: Get request Data
	 * 
	 * Get request data either as a parameter or attribute.  Check for a
	 * parameter value first, then check for an attribute.
	 * 
	 * Inputs: request
	 * 
	 * Returns: request value
	 * 
	 * Revision History:
	 *  
	 */
	
	String getRequestData (HttpServletRequest request, String key) {
		
		String rtn_page = "";
		
		if (request.getParameter(key) != null) {
			rtn_page = request.getParameter(key);
		} else if (request.getAttribute(key) != null) {
			rtn_page = (String)request.getAttribute(key);
		}
		
		return rtn_page;
	}

	/*************************************************************************/

	/* Title: Get search Parameters
	 * 
	 * Inputs: 
	 * 
	 * Search parameters are known based on the form.  
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *  
	 */
	
	Vector getSearchParameters (HttpServletRequest request)  {
		
		Vector sp = new Vector();
		int j=0;
		
		String parameter = "";
		String value  = "";
		
		for (int i=0; i<searchParameters.size(); i++) {
			
			parameter = (String)searchParameters.get(i);
			value = (request.getParameter(parameter)).trim();
			
			if (value != null && value.length()>0) {
				sp.add(j++, parameter);
			}
		}
		
		return sp;
	}

	/*************************************************************************/

	/* Title: Perform Search
	 * 
	 * Inputs: 
	 *
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *  
	 */
	
	boolean invoiceSearch (HttpServletRequest request, Vector sp, String salesAgent)  {
		
		boolean rtn_status = false;
		
		String wc = "";
		String sc = "";
		String rt = "";
		
		if (sp.size()>0) {
			
			wc = buildWhere(request, sp, salesAgent);
			sc = buildSort(sp);
			rt = determineReportTitle(sp);
						
		}
		
		return rtn_status;
	}

	/*************************************************************************/

	/* Title: Perform Search
	 * 
	 * build   lower(lo_om_division) like '%'+lower('nA')+'%'
	 * 
	 * Inputs: 
	 *
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *  
	 */
	
	String buildWhere (HttpServletRequest request, Vector sp, String salesAgent)  {
		
		String whereClaus = "where ";
		String parameter;
		String value;
		String type;
		
		for (int i=0; i<sp.size(); i++) {
			parameter = (String)sp.get(i);
			type = (String)spType.get(parameter);
			value = (request.getParameter(parameter)).trim();
			
			
			if (type.endsWith("char")) {
				whereClaus += "lower("+parameter+") like '%'+lower('"+value+"')+'%'";
			} else {
				whereClaus += parameter+" = "+value;
			}
			
			if (i < sp.size()-1) {
				whereClaus = whereClaus+" and ";
			}
		}
		
		if(salesAgent.equals("0")){
			whereClaus  = whereClaus + " and cp_pt_external_sales_agent = 0 ";
		}else{
			whereClaus  = whereClaus + " and cp_pt_external_sales_agent = 1 ";
		}
		
		
		
		return whereClaus;
	}
	
	/*************************************************************************/

	/* Title: Build Search
	 * 
	 * Inputs: 
	 *
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *  
	 */
	
	String buildSort (Vector sp)  {
		
		String sortClaus = "order by lm_pi_status, ";
		String parameter;
		
		
		for (int i=0; i<sp.size(); i++) {
			parameter = (String)sp.get(i);
			sortClaus += parameter;
			if (i < sp.size()-1) {
				sortClaus += ", ";
			}
		}
		
		return sortClaus;
	}
	
	/*************************************************************************/

	/* Title: Build Search
	 * 
	 * Inputs: 
	 *
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *  
	 */
	
	String determineReportTitle (Vector sp)  {
		
		String report_title = "order by lm_pi_status, ";
		String parameter;
		
		parameter = (String)sp.get(0);
		if (reportTitle.containsKey(parameter)) {
			report_title = (String)reportTitle.get(parameter);
		} else
		{
			report_title = "XXX";
		}
	
		
		return report_title;
	}

	/*************************************************************************/

	/* Title: Build Search
	 * 
	 * Inputs: 
	 *
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *  
	 */
	
	String getReportWhereClause (String ty, String ry, String salesAgent)  {
		
		String wc = "where lm_pi_status = ";
		
		if (ty.equals("rpt1")) {
			wc = wc+"'Pending Receipt' ";
		} else if (ty.equals("rpt2")) {
			wc = wc+"'Received' ";
		} else if (ty.equals("rpt3")) {
			wc = wc+"'Approved' ";
		}
	    
		if (!ry.equals("All")) {
			if (ty.equals("rpt1")) {
				wc = wc+"and datepart(yy, ref_date) = "+ry+" ";
			} else if (ty.equals("rpt2")) {
				wc = wc+"and datepart(yy, lm_pi_date_received) = "+ry+" ";
			} else if (ty.equals("rpt3")) {
				wc = wc+"and datepart(yy, lm_pi_approved_date) = "+ry+" ";
			}
		}
		
		if(salesAgent.equals("0")){
			wc  = wc + " and cp_pt_external_sales_agent = 0 ";
		}else{
			wc  = wc + " and cp_pt_external_sales_agent = 1 ";
		}
		
		return wc;
	}

	/*************************************************************************/

	/* Title: Initialize list of Search Parameters
	 * 
	 * Inputs: 
	 *
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *  
	 */
	
	void initSearchParameters ()  {
		
		searchParameters.clear();
		searchParameters.add(0, "lo_om_division");
		spType.put("lo_om_division", "varchar");
		searchParameters.add(1, "lm_ap_pc_id");
		spType.put("lm_ap_pc_id", "numeric");
		searchParameters.add(2, "lm_js_created_by");
		spType.put("lm_js_created_by", "numeric");
		searchParameters.add(3, "lo_ot_name");
		spType.put("lo_ot_name", "varchar");
		searchParameters.add(4, "lx_pr_title");
		spType.put("lx_pr_title", "varchar");
		searchParameters.add(5, "lm_js_title");
		spType.put("lm_js_title", "varchar");
		
		reportTitle.put("lo_om_division", "Partner");
		reportTitle.put("lm_ap_pc_id", "Project Manager");
		reportTitle.put("lm_js_created_by", "Owner");
		reportTitle.put("lo_ot_name", "Customer");
		reportTitle.put("lx_pr_title", "Project");
		reportTitle.put("lm_js_title", "Job");
		
		reportTitle.put("rpt1", "Pending Receipt");
		reportTitle.put("rpt2", "Received");
		reportTitle.put("rpt3", "Approved");
		
		return;
	}	
	
	/*************************************************************************/

	/* Title: Initialize Line Item Data Structure
	 * 
	 * This data strucutre is used to mirror potentail line item inputs as request fields.   
	 * Data strucutre lineItems is a class variable.
	 * 
	 * Inputs: None
	 * 
	 * Returns: 
	 * 
	 * Revision History:
	 *   04/18/07 - 01.00 - Initial Release
	 */
	
	 void initLineItems () {
		
		 Integer row = 0;
		 String row_increment = "";
		 int capacity = lineItems.capacity();
		 for (int i=0; i<capacity; i++) {
			 
			 Map lineItem = new HashMap(10);
			 row = i+1;
			 row_increment = row.toString();
			 
			 lineItem.put("txtRow"+row_increment,    "");
			 lineItem.put("txtQty"+row_increment,    "");
			 lineItem.put("txtuPrice"+row_increment, "");
			 lineItem.put("txttPrice"+row_increment, "");
			 lineItem.put("selRow"+row_increment,    "");
			 
			 lineItems.add(i,lineItem);
		 }
	 }
	 
	 /*************************************************************************/

	 /* Title: Get Line Items
	  * 
	  * Get each complete line item from the request.  Determine end point.
	  * 
	  * Assumes line items are entered sequentially
	  * 
	  * Inputs: request
	  *
	  * Returns: true/false indicator for method success
	  * 
	  * Revision History:
	  *   04/18/07 - 01.00 - Initial Release
	  */

	 boolean getLineItems (HttpServletRequest request) {

		 boolean found_line_item = false;
		 int attributes = 0;
		 int lineItemCount = 0;
		 int clearPoint = 0;
		 
		 for (int i=0; i<lineItems.size(); i++) {

			 Map lineItem = (Map)lineItems.get(i);

			 Set keys = lineItem.keySet();
			 Iterator keyIter = keys.iterator();
			 attributes = 0;
			 
			 while (keyIter.hasNext()) {
				 String parameter = (String)keyIter.next();
				 String value = request.getParameter(parameter);
				 
				 if (value != null &&  value.length()>0) {
					 lineItem.put(parameter, value);
					 attributes++;
				 }
			 }
			 
			 if (attributes == 5) {
				 lineItems.set(i, lineItem);
				 lineItemCount++;
			 } else {
				 clearPoint = i-1;
				 break;
			 }
		 }

		 if (clearPoint >= 0) {
			 found_line_item = true;
			 lineItems.setSize(lineItemCount);
		 } else {
			 lineItems.clear();
		 }

		 return found_line_item;
	 }
	 
	 /*************************************************************************/

	 /* Title: Process Each Line Item
	  * 
	  * Inputs: 
	  *
	  * lineItem class varaible
	  * lm_pi_id which ownes the line items
	  * HTTP Request
	  * 
	  * Returns: None
	  * 
	  * Revision History:
	  * 05/15/07 - 01.00 - Initial Release
	  */

	 void processLineItems (String lm_pi_id, HttpServletRequest request) {
		 
		 Integer row = 0;
		 String row_increment = "";
		 String error_message = "Line item insert failed for: ";
		 boolean line_item_failure = false;
		 
		 //Delete 
		 Map delete_line_item = new HashMap();
		 delete_line_item.put("lm_pi_id", lm_pi_id);
		 
		 if (performTransaction ("invoice_line_item_delete", delete_line_item)) {
             
			 for (int i=0; i<lineItems.size(); i++) {

				 Map lineItem = (Map)lineItems.get(i);
				 Map xref = new HashMap();

				 row = i+1;
				 row_increment = row.toString();

				 xref.put("lm_pi_id", lm_pi_id);
				 xref.put("lm_li_item", row_increment);
				 xref.put("lm_li_description", (String)lineItem.get("txtRow"+row_increment));
				 xref.put("lm_li_quantity", (String)lineItem.get("txtQty"+row_increment));
				 xref.put("lm_li_unit_price", (String)lineItem.get("txtuPrice"+row_increment));
				 xref.put("lm_li_total_price", (String)lineItem.get("txttPrice"+row_increment));
				 xref.put("lm_li_category", (String)lineItem.get("selRow"+row_increment));

				 if (!performTransaction ("invoice_line_item", xref)) {
					 line_item_failure = true;
					 error_message += row_increment+" ";
				 }
			 }
		 } else {
			 line_item_failure = true;
			 error_message = "Could not delete line items for pm_pi_id = "+lm_pi_id+". Line item insert failed.";
		 }
			 
		 // Check for error processing any of the line items
		 if (line_item_failure) {
			 request.setAttribute("error_message", error_message);
			 System.out.println("PurchaseOrder.processLineItems error message: "+error_message);
		 }

		 return;
	 }
	 
		/*************************************************************************/

		/* Title: Generic Transaction Management
		 * 
		 * Execute a transaction and examine return code.
		 * 
		 * Inputs: 
		 * 
		 * action - maps to a property that defines the SP and inputs/outputs
		 * Request - Map that contains field/value pairs for the transaction
		 * Returns: 
		 * 
		 * Revision History:
		 * 05/15/07 - 01.00 - Initial Release
		 *  
		 */
		
		boolean performTransaction (String action, Map request) {
			
			boolean rtn_status = false;
			Map xrtn;
			Integer rtn_code;
			
			// Initialize transaction and move data
			Transaction xtrans = new Transaction (action);
			xtrans.moveRequestData (request);
			
			// Perform database actions			
			xrtn = xtrans.executeSP();
			rtn_code = (Integer)xrtn.get("rtn_status");
			
			if (rtn_code != null) {
				if (rtn_code == 0) {
					rtn_status = true;
				} 		
			}

			return rtn_status;
		}
		
		
		/*************************************************************************/

		/* Title: Send Email - Approve
		 * 
		 * Inputs: 
		 * 
		 * Returns: 
		 * 
		 * Revision History:
		 *   04/18/07 - 01.00 - Intial Release
		 */ 

		void sendEmailApprove (String lm_pi_id, String lm_po_id, int type_approve, String url) {
			
			String message = "<div style=\"font-family: Arial, Tahoma, Verdana; font-size: 12px;\">";
			Transaction xt = new Transaction ();
			Map email = xt.getEmailDetails(lm_pi_id);
			
			email.put("Subject", "eInvoice for PO "+(String)email.get("PO Number")+" (Internal-Paper)");
			
			message += (String)email.get("First Name")+",<br><br>";
			
			if (type_approve == 1) {
				message += "The following partner invoice is being forwarded to you for review and approval.<br><br>";
			} else {
				message += "The following partner invoice was automatically approved.  Please double check the information below.<br><br>";
			}
			
			message += "<b>PO Number</b>: "+(String)email.get("PO Number")+"<br>";
			message += "<b>Delivery Date</b>: "+(String)email.get("Delivery Date")+"<br>";
			message += "<b>Job</b>: "+(String)email.get("Job")+"<br>";
			message += "<b>Site</b>: "+(String)email.get("Site")+"<br><br>";
			
			message += "<b>Financial Summary</b><br><br>";
			message += "<b>Authorized</b>: "+(String)email.get("Authorized Total")+"<br>";
			message += "<b>Invoiced</b>: "+(String)email.get("Invoice Total")+"<br>";
			message += "<b>Approved</b>: "+(String)email.get("Approved Total")+"<br><br>";
			
			message += "<b>Partner</b>: "+(String)email.get("Partner")+"<br>";
			message += "<b>Invoice Number</b>: "+(String)email.get("Invoice Number")+"<br><br>";
			
			
			if (type_approve != 1) {
				message += "To approve this eInvoice, <a href=\""+url+"Summary.xo?action=invoice_summary&lm_pi_id="+lm_pi_id+"&lm_po_id="+lm_po_id+"\">Click Here.</a><br><br>";
			} 

			message += "For a summary of this eInvoice, <a href=\""+url+"Summary.xo?action=invoice_summary&lm_po_id="+lm_po_id+"\">Click Here.</a><br><br>";

			message += "Ilex - Automated Message 1</div>";
			
			email.put("Message", message);
			email.put("content_type", "text/html");

		    Notifications msg = new Notifications ();
		    msg.sendNotification (email);
			
			return;
		}

		/*************************************************************************/

		/* Title: Send Email - Payment
		 * 
		 * Inputs: 
		 * 
		 * Returns: 
		 * 
		 * Revision History:
		 *   04/18/07 - 01.00 - Intial Release
		 */

		void sendEmailPay (String lm_pi_id, String lm_po_id) {
			
			String message = "<div style=\"font-family: Arial, Tahoma, Verdana; font-size: 12px;\">";
			Transaction xt = new Transaction ();
			Map email = xt.getEmailDetails(lm_pi_id);
						
			email.put("To", "kmcqueen@contingent.com");
			email.put("CC", "scampbell@contingent.com");
			
			email.put("Subject", "eInvoice for PO "+(String)email.get("PO Number"));
			
			message += "Hello,<br><br>";
			message += "The following partner invoice is being forwarded to you for payment.<br><br>";
			message += "<b>Customer</b>: "+(String)email.get("Customer")+"<br>";
			message += "<b>PO Number</b>: "+(String)email.get("PO Number")+"<br>";
			message += "<b>Delivery Date</b>: "+(String)email.get("Delivery Date")+"<br>";
			message += "<b>Job</b>: "+(String)email.get("Job")+"<br>";
			message += "<b>Site</b>: "+(String)email.get("Site")+"<br><br>";
			
			message += "<b>Financial Summary</b><br><br>";
			message += "<b>Authorized</b>: "+(String)email.get("Authorized Total")+"<br>";
			message += "<b>Invoiced</b>: "+(String)email.get("Invoice Total")+"<br>";
			message += "<b>Approved</b>: "+(String)email.get("Approved Total")+"<br><br>";
			
			message += "<b>Partner</b>: "+(String)email.get("Partner")+"<br>";
			message += "<b>Invoice Number</b>: "+(String)email.get("Invoice Number")+"<br><br>";

			message += "Ilex - Automated Message 2</div>";

			email.put("Message", message);
			email.put("content_type", "text/html");

			try {
				Notifications msg = new Notifications();
				msg.sendNotification(email);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return ;
		}
		
		/*************************************************************************/

		/* Title: Send Email - Notification to Owner
		 * 
		 * 1) Get Owner Information
		 * 
		 * 2) Get Invoice information
		 * 
		 * 3) Build mesage
		 * 
		 * 4) Send Message
		 * 
		 * Inputs: 
		 * 
		 * Returns: 
		 * 
		 * Revision History:
		 *   04/18/07 - 01.00 - Intial Release
		 */

		/*************************************************************************/

		boolean sendOwnwerNotification (String new_status, String lm_pi_id, String lm_po_id) {
			
			boolean return_code;
			
			String message = "<div style=\"font-family: Arial, Tahoma, Verdana; font-size: 12px;\">";
			String subject = "";
		
			if (new_status.equals("Received")) {
				
				subject = "eInvoice for PO "+lm_po_id+" - Status Changed Back to "+new_status;
				
			} else if (new_status.equals("Pending Receipt")) {
				
				subject = "eInvoice for PO "+lm_po_id+" - Status Changed Back to "+new_status;
				
			} else if (new_status.equals("Update Web")) {
				
				subject = "eInvoice for PO "+lm_po_id+" - PCC Updated to Reflect Correct Status";
				
			}
			
			Transaction xt = new Transaction ();
			Map email = xt.getEmailDetails(lm_pi_id);
			
			email.put("From", "accounting@contingent.net");
			
			email.put("Subject", subject);
			
			message += "Hello,<br><br>";
			
			if (new_status.equals("Received") || new_status.equals("Pending Receipt")) {
				
				message += "Per your request or based on a request from the partner, the eInvoice status for PO "+lm_po_id+" was changed back to <b>"+new_status+"</b> and all appropriate fields reset.<br><br>";
				message += "Please review the eInvoice to determine if you have a required action.<br><br>";
			
			} else if (new_status.equals("Update Web")) {
				
				message += "A problem was reported with the PPC status of the eInvoice for PO "+lm_po_id+".&nbsp;&nbsp;It has been updated and should now be displaying correctly.<br><br>";
				message += "Please review the current status and take action, as applicable.<br><br>";
			} 
			
			message += "For a summary of the PO and Invoice <a href=\"http://ilex2.contingent.local/eInvoice/Summary.xo?action=invoice_summary&lm_po_id="+lm_po_id+"\">click here</a>.";
			message += "<br><br>";

			message += "Thanks<br>";
			message += "Accounting</div>";
			
			// Explicitly set the content type for HTML
			email.put("content_type", "text/html");
			email.put("Message", message);

		    Notifications msg = new Notifications ();
		    return_code =msg.sendNotification (email);
			
			return return_code;
		}
		
		/*************************************************************************/

		/* Title: Get Invoice Data for Update Form
		 * 
		 * Inputs: HTTP Request and specific invoice item
		 * 
		 * Returns: HTTP Request Attributes
		 * 
		 * Revision History:
		 *   07/08/2007 - 01.00 - Initial Release
		 */
		
		boolean getUpdateData (HttpServletRequest request, String id)  {
			
			boolean rtn_status = true;
			xtrans = new Transaction ("update_form");
			
			Map invoicedata = xtrans.executeSP(id);
			
			Iterator key = invoicedata.keySet().iterator();
			
			// Make the individual fields availble to the form fields
			
		    while (key.hasNext()) {
		        String fld = (String)key.next();
		        String value = (String)invoicedata.get(fld);
		        request.setAttribute(fld, value);
		    }

			request.setAttribute("invoiceData", xtrans.executeSP(id));

			return rtn_status;
		}
		
		/*************************************************************************/

		/* Title: Validate Request Parameter Input
		 * 
		 * Perform simple validation where some value is expect.  Validation ensures a
		 * value was supplied by the form
		 * 
		 * Inputs: 
		 *
		 * request_parameter - parameter to be validated
		 * 
		 * Returns: true or false
		 * 
		 * Revision History:
		 *  
		 */
		
		boolean validateRequestInput (String request_parameter)  {
			
			boolean validated = true;
		    
			if (request_parameter == null) {
				validated = false;
			} else if ((request_parameter.trim()).length() == 0) {
				validated = false;
			} else if ((request_parameter.trim()).equals("null")) {
				validated = false;
			}
			
			return validated;
		}
		
		/*************************************************************************/

		/* Title: Validate Request Parameter Input
		 * 
		 * Perform simple validation where some value is expect.  Validation ensures a
		 * value was supplied by the form
		 * 
		 * Inputs: 
		 *
		 * request_parameter - parameter to be validated
		 * 
		 * Returns: true or false
		 * 
		 * Revision History:
		 *  
		 */
		
		String getAllowedStatusChange (String from_status)  {
			
			String allowed_status_changes;
		    
			if (from_status.equals("Approved")) {
				allowed_status_changes = "Received|Received~Pending Receipt|Pending Receipt~Update Web|Update Web";
			} else if (from_status.equals("Received")) {
				allowed_status_changes = "Pending Receipt|Pending Receipt~Update Web|Update Web";
			} else {
				allowed_status_changes = "Error|No Transition Allowed";
			}
			
			return allowed_status_changes;
		}

		/*************************************************************************/

		/* Title: Determine Next Invoice Action
		 * 
		 * BAsed on the current status, determine the next eInvoice action
		 * 
		 * Inputs: 
		 *
		 * request_parameter - parameter to be validated
		 * 
		 * Returns: true or false
		 * 
		 * Revision History:
		 *  
		 */
		
		String determineNextAction (String current_status)  {
			
			String next_action = "";
			
			if (deliverable_status.equals("A") || deliverable_status.equals("C") || 
					deliverable_status.equals("F") || deliverable_status.equals("F") || deliverable_status.equals("G")) {
				
				if (current_status.equals("Pending Receipt")) {
					next_action = "Receive";
				} else if (current_status.equals("Received")) {
					next_action = "Approve";
				} else if (current_status.equals("Approved")) {
					next_action = "Pay";
				} else if (current_status.equals("Deleted")) {
					next_action = "PO was Deleted - No Action Allowed";
				}else {
					next_action = "No Further Action Required for this PO.";
				}
				
			} else {
				next_action = "No Action Allowed - Pending Deliverables";
			}
			
		
			return next_action;
		}
	

		
		/*************************************************************************/

		/* Title: Get Approve Data
		 * 
		 * Get all invoice summary data.
		 * 
		 * Inputs: request - see usage below
		 * 
		 * Returns: request - see usage below
		 * 
		 * Revision History:
		 *  05/20/08 - Initial Release
		 *  
		 */
		
		boolean getInvoiceSummary (HttpServletRequest request, String lm_po_id)  {
			
			boolean rtn_status = true;
			
			Transaction xtrans = new Transaction ("invoice_summary");
			Map invoice_summary = xtrans.executeSP(lm_po_id);
			// Set attributes
			request.setAttribute("lm_po_id", lm_po_id);
			request.setAttribute("lm_pi_id", (String)invoice_summary.get("lm_pi_id"));
			request.setAttribute("next_action", determineNextAction ((String)invoice_summary.get("lm_pi_status")));
			request.setAttribute("invoice_summary", invoice_summary);
			
			return rtn_status;
		}
		public ActionForward update_approved_total(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response ) throws Exception {
			String action = "update_approved_total";
			String approvedTotal = (String)request.getParameter("approvedTotal");
			String purchaseOrder = (String)request.getParameter("purchaseOrder");
			String userid = (String)request.getParameter("userid");
			String role = getRole(userid);	
			request.setAttribute("role", role);
			int result = Transaction.updateApprovedTotal(purchaseOrder, approvedTotal);
			String message="";
			if(result == 1)
				message= "Record updated";
			else
				message = "Record update failed";		
			request.setAttribute("message", message);
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(message);
			return null;
		}
		private String getRole(String userid) {
			return Transaction.getRole(userid);
		}
}
