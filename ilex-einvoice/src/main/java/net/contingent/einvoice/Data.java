package net.contingent.einvoice;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class Data {

	Connection databaseConnection = null;
	Statement sql;
	
	/*************************************************************************/

	/* Title: Get Connection
	 * 
	 * Description: Establish a connection to the target database
	 *
	 * 
	 * Revision History:
	 *   08/05/07 - 01.00 - Initial Release 
	 */

	public Data () {

	}
	
	/*************************************************************************/

	/* Title: getResultSet
	 * 
	 * Description: Generic sql execution
	 * 
	 * Inputs: sql
	 * 
	 * Returns: result set
	 * 
	 * Revision History:
	 *   08/05/07 - 01.00 - Initial Release 
	 */

	public ResultSet getResultSet (String sql_to_execute) {

		ResultSet rs_return = null;

		try {
			sql = databaseConnection.createStatement();
			rs_return = sql.executeQuery(sql_to_execute);
		}
		catch (Exception e) {
			System.out.println(e);
			rs_return = null;
		}

		return rs_return;
	}

	/*************************************************************************/

	/* Title: Execute non-Result Set Query
	 * 
	 * Description: Generic sql execution
	 * 
	 * Inputs: sql
	 * 
	 * Revision History:
	 *   08/05/07 - 01.00 - Initial Release 
	 */

	public boolean executeQuery (String sql_to_execute) {

		boolean executeStatus = false;
		
		try {
			sql = databaseConnection.createStatement();
			executeStatus = sql.execute(sql_to_execute);
		}
		catch (Exception e) {
			System.out.println(e+" "+sql_to_execute);
			return executeStatus;
		}

		return executeStatus;
	}

}
