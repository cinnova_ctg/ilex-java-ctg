package net.contingent.einvoice;

import java.sql.DriverManager;

/*************************************************************************/

/* Title: MySQL Access to DONet
 * 
 *  
 */

class DataWeb extends Data {
	
	DataWeb () {

		if (databaseConnection == null) {

			try  
			{
				Class.forName("com.mysql.jdbc.Driver");
			}
			catch (Exception e)
			{
				System.out.println("Class.forName Failed with error: "+e);
			}		  

			try   
			{
				databaseConnection = DriverManager.getConnection("jdbc:mysql://breadstick.donet.com:3306/contingent_ccc", "contingent", "0bs0lete");
				System.out.println("Connection established - MySQL");
			}
			catch (Exception sqle)
			{
				System.out.println("Connection Failed with error: "+sqle);
			}
		}
	}

}

