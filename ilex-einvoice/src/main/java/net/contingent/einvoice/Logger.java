package net.contingent.einvoice;

import java.sql.ResultSet;
import java.sql.Statement;

 public class Logger {

	static DataAccess da;
	
	public Logger (DataAccess da) {
		this.da  = da;
	}
	
	static void summary (int in, int up, int de) {
				
		String sp = "exec ei_summary_log_manage_01 "+in+", "+up+", "+de;
		try {
			Statement stmt = da.databaseConnection.createStatement();
			int rs = stmt.executeUpdate(sp);
			stmt.close();
		}
		catch (Exception e) {
			System.out.println(e);
			System.exit(1);
		}
	}
	
	static void error (String lm_pi_id, String lm_po_id, String location, String message) {
		
		if (location.length() > 64) {
			location = location.substring(0, 64);
		}
		
		if (message.length() > 128) {
			message = message.substring(0, 128);
		}

		String sp = "exec ei_error_log_manage_01 "+lm_pi_id+", "+lm_po_id+", '"+location+"', '"+message+"'";
		try {
			Statement stmt = da.databaseConnection.createStatement();
			int rs = stmt.executeUpdate(sp);
			stmt.close();
		}
		catch (Exception e) {
			System.out.println(e);
			System.exit(1);
		}

	}

}
