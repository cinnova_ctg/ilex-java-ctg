package net.contingent.einvoice;

import java.sql.ResultSet;
import java.util.Vector;

public class InvoiceManager {

	DataIlex ida = new DataIlex();
	DataWeb wda = new DataWeb();

	Vector invoices = new Vector();
	
	int webUpdates = 0;
	int webInserts = 0;
	int ilexNotEligible = 0;
	
	/*************************************************************************/

	InvoiceManager () {

		controller ();
		
		System.out.println("Invoice Count: "+invoices.size()+" Ilex not eligible: "+ilexNotEligible+" Inserts: "+webInserts+" Updates: "+webUpdates);
	}
	
	/*************************************************************************/

	/* Title: Manager Invoice Comparison and Update
	 * 
	 * Description: 
	 * 
	 * 1) Get all lm_pi_id's for invoices
	 * 
	 * 2) Compare Ilex and Web
	 * 
	 * 3) Adjust Web as needed.
	 * 
	 * 4) Determine if any Web invoices should be deleted
	 * 
	 * Revision History:
	 *   08/05/07 - 01.00 - Initial Release 
	 */

	void controller () {
		
		String compare_result;
		
		// 1 - get a list of Ilex invoices
		getIlexInvoices ();
		
		// 2 - Compare Ilex vs Web
		//invoices.setSize(15);  // Limit qty for testing
		
		for (int i=0; i<invoices.size(); i++) {
			
			String id = (String)invoices.get(i);
			
			Invoice ilex = new Invoice (id, ida, "I");
			
			if (ilex.InvoiceStatus.equals("Found")){
				
				Invoice web = new Invoice (id, wda, "W");
				
				if (web.InvoiceStatus.equals("Found")) {
					compare_result = compareInvoices (ilex, web);
				} else {
					compare_result = "Insert";
				}
				
				if (!compare_result.equals("OK")) {
					updateWebInvoice (compare_result, ilex);
				}
				
			} else {
				ilexNotEligible++;
			}
		}
	}
	
	
	/*************************************************************************/

	/* Title: Get Ilex Invoices
	 * 
	 * Description: 
	 * 
	 * 
	 * Revision History:
	 *   08/05/07 - 01.00 - Initial Release 
	 */

	void getIlexInvoices () {

		// Get a list of Ilex invoices

		String sql = "select lm_pi_id from lm_partner_invoice where lm_pi_id > 1000";
		int i = 0;
		
		String field = "";
		ResultSet rs = ida.getResultSet(sql);

		try {
			while (rs.next()) {
				
				invoices.add(i++, rs.getString(1));
			}
            System.out.println(invoices.size());
			rs.close();
		}
		catch (Exception e) {
			System.out.println(e+" - with - "+sql);
		}
		
	}
	
	/*************************************************************************/


	/* Title: Compare two invoices
	 * 
	 * Description: 
	 * 
	 * 
	 * Revision History:
	 *   08/05/07 - 01.00 - Initial Release 
	 */

	String compareInvoices (Invoice i, Invoice w) {

		// Get a list of Ilex invoices

		String comparison = "OK";
		
		if (!i.lm_po_VendorID.equals(w.lm_po_VendorID) || !i.lm_pi_invoice_number.equals(w.lm_pi_invoice_number)) 
		{
			comparison = "Invalid";
			return comparison;
		}

		if (!i.lm_pi_status.equals(w.lm_pi_status)) 
		{
			comparison = "Update";
			return comparison;
		}

		if (!i.lm_pi_partner_comments.equals(w.lm_pi_partner_comments)) 
		{
			comparison = "Update";
			return comparison;
		}

		if (!i.lm_pi_reconcile_comments.equals(w.lm_pi_reconcile_comments)) 
		{
			comparison = "Update";
			return comparison;
		}
		
		if (!i.lm_pi_approved_total.equals(w.lm_pi_approved_total)) 
		{
			comparison = "Update";
			return comparison;
		}
		
		if (!i.lm_pi_approved_date.equals(w.lm_pi_approved_date)) 
		{
			comparison = "Update";
			return comparison;
		}
		
		if (!i.lm_pi_gp_paid_date.equals(w.lm_pi_gp_paid_date)) 
		{
			comparison = "Update";
			return comparison;
		}
		
		return comparison;		
	}
	
	/*************************************************************************/


	/* Title: Update Web Invoice
	 * 
	 * Description: 
	 * 
	 * 
	 * Revision History:
	 *   08/05/07 - 01.00 - Initial Release 
	 */

	void updateWebInvoice (String status, Invoice i) {


		if (status.equals("Update")) {
			webUpdates++;
			//System.out.println("Update: "+i.lm_pi_id);
			
			// Call update method
		} else {
			webInserts++;
			//System.out.println("Insert: "+i.lm_pi_id);
			
			// Call insert method
		}
		
	}

	/*************************************************************************/

	public static void main(String[] args) {
		
		InvoiceManager x = new InvoiceManager();
		
	}

}
