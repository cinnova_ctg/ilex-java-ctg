package net.contingent.einvoice;

import java.sql.DriverManager;

public class DataIlex extends Data {

	public DataIlex () {
		
		if (databaseConnection == null) {
			// Get Classes MS JDBC Driver (2005)
			 try              
			  {
			   Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			  }
			  catch (Exception e)
			  {
			   System.out.println("Class.forName Failed with error: "+e);
			  }	
			  
			  // Get connection for ILEX
			  try   
			   {
			    //String db_url = "jdbc:sqlserver://192.168.20.8:1433;databaseName=ilexTestRelease1;User=ilex;Password=ilex";
			    String db_url = "jdbc:sqlserver://192.168.20.53:1433;databaseName=ilex;User=ilex;Password=0s1adFi!";
				//String db_url = "jdbc:sqlserver://localhost:1433;databaseName=ilexTestRelease1;User=sa;Password=unlenn";
			    databaseConnection = DriverManager.getConnection (db_url); 
			    System.out.println("Connection established for MS SQL");
			   }
			   catch (Exception sqle)
			   {
			     System.out.println("Connection Failed with error: "+sqle);
			   }
	    }
	}
		
}