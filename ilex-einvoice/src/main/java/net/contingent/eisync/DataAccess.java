package net.contingent.eisync;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DataAccess {

	/******************************************************************/

	/* Name: 
	 * 
	 * Description:
	 * 
	 * 
	 * Inputs:
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */

	public static Connection databaseConnection = null;

	//private String sql_stmt; 
	private Statement sql;


	public DataAccess () {

		if (databaseConnection == null) {
//			Get Classes MS JDBC Driver (2005)
			try              
			{
				Class.forName("net.sourceforge.jtds.jdbc.Driver");
			}
			catch (Exception e)
			{
				System.out.println("Class.forName Failed with error: "+e);
			}	

//			Get connection for ILEX
			try   
			{
				//jdbc:jtds:<server_type>://<server>[:<port>][/<database>][;<property>=<value>[;...]]
				String db_url = "jdbc:jtds:sqlserver://192.168.32.53:1433/ilex;user=ilex;password=0s1adFi!";
//				String db_url = "jdbc:jtds:sqlserver://192.168.0.150:1433/ilex;user=cinnova-ilex;password=farid12345";
				//String db_url = "jdbc:jtds:sqlserver://localhost:1433/ilex_local;user=sa;password=unlenn";
				databaseConnection = DriverManager.getConnection (db_url); 
				//System.out.println("Connection established");
			}
			catch (Exception sqle)
			{
				System.out.println("Connection Failed with error: "+sqle);
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
