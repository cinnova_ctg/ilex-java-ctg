package net.contingent.eisync;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;

public class PurchaseOrder {

	/******************************************************************/

	String lm_po_id;
	
	String lm_po_VendorID;
	String lm_pi_status ;
	String lm_pi_submit_type ;
	String lm_po_terms;
	String lm_pi_invoice_number ;
	String lm_pi_invoice_date;
	String lm_pi_date_received;
	String lm_pi_submitter;
	String lm_pi_total;
	String lm_pi_partner_comments ;
	String lm_pi_reconcile_comments;
	String lm_pi_approved_total;
	String lm_pi_approved_by;
	String lm_pi_approved_date;
	String lm_pi_gp_paid_date;
	String lm_pi_gp_check_number;
	String lm_po_number;
	String lm_po_issue_date;
	String lm_po_authorised_total;
	String lm_po_deliverables;
	String lm_po_js_id;
	String lm_pi_last_update;
	String site_number;
	String scheduled;
	String job_start;
	String job_end;
	String contingent_contact;
	String contingent_contact_phone;
	String email_address;
	String lm_pi_minuteman_status;
	String lm_po_pwo_type_id;
	Double authorised_total;
	
	WebDataAccess wda = null;
	DataAccess da = null;

	
	/******************************************************************/

	/* Name: Contructor
	 * 
	 * Description:
	 * 
     * For a given PO, determine if the PO is available on the web and 
     * if so whether it needs to be updated.
     * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   05/12/09 - 01.00 - Initial Release
	 */
	
	public PurchaseOrder(DataAccess da, WebDataAccess wda, String lm_po_id) {
		
		this.da = da;
		this.wda = wda;
		this.lm_po_id = lm_po_id;
		
		if (getInvoiceData ()) {
			
			// Compare to web and insert/update as needed
			checkForUpdate();
		}
		
		return;
	}
	
	/******************************************************************/

	/* Name: Contructor
	 * 
	 * Description:
	 * 

	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   05/12/09 - 01.00 - Initial Release
	 */
	
	boolean checkForUpdate() {
		
		boolean status = true;
		int database_action; // 0 = no action, 1 = insert, 2 = update
		
		String row_change_date = "";
		
		// Attempt to get the purchase order record from mysql
		String sp = "select lm_pi_invoice_row_change_date from lm_ei_ilex_to_web_invoices_01 where lm_pi_status = 'PO In Work' and lm_po_number = "+lm_po_id;
		
		database_action = 0; // Assume no action
		
		try {
			Statement stmt = wda.databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sp);
			
			//  Assume that the record does not exist - i.e. needs to be inserted.  Thus if not found default is insert
			
			if (rs.next()) {
				
				// Check to see if reference date is the same - if not update
				
				row_change_date = rs.getString(1).substring(0, 19);
				
				if (!row_change_date.equals(lm_pi_last_update)) {
					database_action = 2;
				}
			} else {
				database_action = 1;
			}
			
			stmt.close();
		}
		
		catch (Exception e) {
			status = false;
			System.out.println(e);
		}
		
		// Determine which MySQl action: None, Insert, or Update
		
		if (database_action == 1) {
			// Insert
			insertWeb();
		} else if (database_action == 2) {
			// Update
			updateWeb();
		}

		return status;
	}
	
	/******************************************************************/

	/* Name: Get Invoice Data
	 * 
	 * Description:
	 * 
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   05/12/09 - 01.00 - Initial Release
	 */
	
	boolean getInvoiceData () {
		
		boolean status = true;
		
		String sp = "exec lm_ei_ilex_to_web_po_01 "+lm_po_id;
		
		try {
			Statement stmt = da.databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sp);
						
			if (rs.next()) {
				 lm_po_VendorID =  rs.getString("lm_po_VendorID");
				 lm_pi_status  =  rs.getString("lm_pi_status");
				 lm_pi_submit_type  =  rs.getString("lm_pi_submit_type");
				 lm_po_terms =  rs.getString("lm_po_terms");
				 lm_pi_invoice_number  =  rs.getString("lm_pi_invoice_number");
				 lm_pi_invoice_date =  rs.getString("lm_pi_invoice_date");
				 lm_pi_date_received =  rs.getString("lm_pi_date_received");
				 lm_pi_submitter  =  rs.getString("lm_pi_submitter");
				 lm_pi_total =  rs.getString("lm_pi_total");
				 lm_pi_partner_comments  =  rs.getString("lm_pi_partner_comments");
				 lm_pi_reconcile_comments =  rs.getString("lm_pi_reconcile_comments");
				 lm_pi_approved_total =  rs.getString("lm_pi_approved_total");
				 lm_pi_approved_by  =  rs.getString("lm_pi_approved_by");
				 lm_pi_approved_date =  rs.getString("lm_pi_approved_date");
				 lm_pi_gp_paid_date  =  rs.getString("lm_pi_gp_paid_date");
				 lm_pi_gp_check_number   =  rs.getString("lm_pi_gp_check_number");
				 lm_po_number  =  rs.getString("lm_po_number");
				 lm_po_issue_date =  rs.getString("lm_po_issue_date");
				 lm_po_authorised_total  =  rs.getString("lm_po_authorised_total");
				 lm_po_deliverables  =  rs.getString("lm_po_deliverables");
				 lm_po_js_id  =  rs.getString("lm_po_js_id");
				 lm_pi_last_update =  rs.getString("lm_pi_last_update");
				 site_number  =  rs.getString("site_number" );
				 scheduled  =  rs.getString("scheduled");
				 job_start  =  rs.getString("job_start");
				 job_end  =  rs.getString("job_end");
				 contingent_contact  =  rs.getString("contingent_contact");
				 contingent_contact_phone  =  rs.getString("contingent_contact_phone");
				 email_address  =  rs.getString("email_address");
				 lm_pi_minuteman_status =  rs.getString("lm_pi_minuteman_status");
				 lm_po_pwo_type_id = rs.getString("lm_po_pwo_type_id");
				 authorised_total = rs.getDouble("authorised_total");
			} else {
				status = false;
			}
			stmt.close();
		}
		catch (Exception e) {
			status = false;
			System.out.println(e);
		}
		
		return status;
	}
	
	/******************************************************************/

	/* Name: Update Web Data
	 * 
	 * Description:
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	boolean updateWeb () {
		boolean status = true;
		
		// Check for lock - partner is updating
		
		int row_count = 0;
		String sql = "update lm_ei_ilex_to_web_invoices_01 set ";
		sql += "lm_po_VendorID = ?, ";
		sql += "lm_pi_status = ?, ";
		sql += "lm_pi_submit_type = ?, ";
		sql += "lm_po_terms = ?, ";
		sql += "lm_pi_invoice_number = ?, ";
		sql += "lm_pi_invoice_date = ?, ";
		sql += "lm_pi_date_received = ?, ";
		sql += "lm_pi_submitter = ?, ";
		sql += "lm_pi_total = ?, ";
		sql += "lm_pi_partner_comments = ?, ";
		sql += "lm_pi_reconcile_comments = ?, ";
		sql += "lm_pi_approved_total = ?, ";
		sql += "lm_pi_approved_by = ?, ";
		sql += "lm_pi_approved_date = ?, ";
		sql += "lm_pi_gp_paid_date = ?, ";
		sql += "lm_pi_gp_check_number = ?, ";
		sql += "lm_po_number = ?, ";
		sql += "lm_po_issue_date = ?, ";
		sql += "lm_po_authorised_total = ?, ";
		sql += "lm_po_deliverables = ?, ";
		sql += "lm_po_js_id = ?, ";
		sql += "site_number = ?, ";
		sql += "scheduled = ?, ";
		sql += "job_start = ?, ";
		sql += "job_end = ?, ";
		sql += "contingent_contact = ?, ";
		sql += "contingent_contact_phone = ?, ";
		sql += "email_address = ?, ";
		sql += "lm_pi_minuteman_status = ?, ";
		sql += "lm_pi_invoice_row_change_date = ? ";
		sql += "where lm_po_number = '"+lm_po_id+"'";

		try {
			PreparedStatement ps = wda.databaseConnection.prepareStatement(sql);
			ps.setString(1, lm_po_VendorID);
			ps.setString(2, lm_pi_status );
			ps.setString(3, lm_pi_submit_type );
			ps.setString(4, lm_po_terms);
			ps.setString(5, lm_pi_invoice_number );
			ps.setString(6, lm_pi_invoice_date);
			ps.setString(7, lm_pi_date_received);
			ps.setString(8, lm_pi_submitter );
			ps.setString(9, lm_pi_total);
			ps.setString(10, lm_pi_partner_comments );
			ps.setString(11, lm_pi_reconcile_comments);
			ps.setString(12, lm_pi_approved_total);
			ps.setString(13, lm_pi_approved_by );
			ps.setString(14, lm_pi_approved_date);
			ps.setString(15, lm_pi_gp_paid_date );
			ps.setString(16, lm_pi_gp_check_number  );
			ps.setString(17, lm_po_number );
			ps.setString(18, lm_po_issue_date);
			ps.setString(19, lm_po_authorised_total );
			ps.setString(20, lm_po_deliverables );
			ps.setString(21, lm_po_js_id );
			ps.setString(22, site_number );
			ps.setString(23, scheduled );
			ps.setString(24, job_start );
			ps.setString(25, job_end );
			ps.setString(26, contingent_contact );
			ps.setString(27, contingent_contact_phone );
			ps.setString(28, email_address );
			ps.setString(29, lm_pi_minuteman_status);
			ps.setString(30, lm_pi_last_update);

			row_count = ps.executeUpdate();

			if (row_count == 0) {
				// log error
				status = false;
			}
			ps.close();

		}
		catch (Exception e) {
			status = false;
			System.out.println(e);
		}

		return status;
	}
	
	/******************************************************************/

	/* Name: Insert Web Data
	 * 
	 * Description:
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 *   05/27/09 - 02.00 - Update for Ilex 2+
	 */
	
	boolean insertWeb () {
		boolean status = true;
		
		int row_count = 0;
		String sql = "insert into lm_ei_ilex_to_web_invoices_01 ";
        sql += "(lm_pi_id, lm_po_VendorID, lm_pi_status, lm_pi_submit_type, lm_po_terms, lm_pi_invoice_number, lm_pi_invoice_date, lm_pi_date_received, lm_pi_submitter, lm_pi_total, lm_pi_partner_comments, lm_pi_reconcile_comments, lm_pi_approved_total, lm_pi_approved_by, lm_pi_approved_date, lm_pi_gp_paid_date, lm_pi_gp_check_number, lm_po_number , lm_po_issue_date, lm_po_authorised_total, lm_po_deliverables, lm_po_js_id, site_number, scheduled, job_start, job_end, contingent_contact, contingent_contact_phone, email_address, lm_pi_minuteman_status, lm_pi_invoice_row_change_date, esa_flag)";
        sql += " values ";
        sql += "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
 
        // The eInvoice does not exist at this point so use a place holder to go with the Po information
        String lm_pi_id = "99"+lm_po_id;
        
        try {
			PreparedStatement ps = wda.databaseConnection.prepareStatement(sql);
			ps.setString(1, lm_pi_id);
			ps.setString(2, lm_po_VendorID);
			ps.setString(3, lm_pi_status );
			ps.setString(4, lm_pi_submit_type );
			ps.setString(5, lm_po_terms);
			ps.setString(6, lm_pi_invoice_number );
			ps.setString(7, lm_pi_invoice_date);
			ps.setString(8, lm_pi_date_received);
			ps.setString(9, lm_pi_submitter );
			ps.setString(10, lm_pi_total);
			ps.setString(11, lm_pi_partner_comments );
			ps.setString(12, lm_pi_reconcile_comments);
			ps.setString(13, lm_pi_approved_total);
			ps.setString(14, lm_pi_approved_by );
			ps.setString(15, lm_pi_approved_date);
			ps.setString(16, lm_pi_gp_paid_date );
			ps.setString(17, lm_pi_gp_check_number  );
			ps.setString(18, lm_po_number );
			ps.setString(19, lm_po_issue_date);
			ps.setString(20, lm_po_authorised_total );
			ps.setString(21, lm_po_deliverables );
			ps.setString(22, lm_po_js_id );
			ps.setString(23, site_number );
			ps.setString(24, scheduled );
			ps.setString(25, job_start );
			ps.setString(26, job_end );
			ps.setString(27, contingent_contact );
			ps.setString(28, contingent_contact_phone );
			ps.setString(29, email_address );
			ps.setInt(30, Integer.parseInt(lm_pi_minuteman_status));
			ps.setString(31, lm_pi_last_update);
			ps.setInt(32, 0);
			
			row_count = ps.executeUpdate();

			if (row_count == 0) {
                // log error
				status = false;
			}
			ps.close();
			
		}
		catch (Exception e) {
			status = false;
		}
		
		return status;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
