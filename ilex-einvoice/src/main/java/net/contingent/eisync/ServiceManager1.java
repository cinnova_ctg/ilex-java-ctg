package net.contingent.eisync;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ServiceManager1 {

	static DataAccess da;
	static WebDataAccess wda;
	static Logger logger = new Logger(da);
    static String timestamp = "";
	static PortGrabber fieldPortGrabber;

	/******************************************************************/

	public static void main( String[] args ) throws Exception
	{

		// Build date stamp for log records and file name
		DateFormat df1 = new SimpleDateFormat("yyyyMMdd kk:mm.ss");
		timestamp = df1.format(new Date());
		
		DateFormat df2 = new SimpleDateFormat("yyyyMMdd");
		String date_time_stamp = df2.format(new Date());
		
		// Redirect to log file - need to replace with circular
		// Debug System.out.println ("Switching to file output with log/eInvoiceSync_"+date_time_stamp+".log");
		PrintStream	stdout	= null;
		try 
		{
			stdout = new PrintStream (new FileOutputStream("log/eInvoiceSync_"+date_time_stamp+".log", true));
			System.setOut(stdout);
			System.out.println(timestamp+"-Starting eInvoice sync" );
		}
		catch (Exception e)
		{
			System.out.println ("Redirect:  Unable to open output file");
			System.exit (1);
		}

		// Initialize and begin execution with launch method
		
		da = new DataAccess();
		wda = new WebDataAccess ();
		logger = new Logger(da);

		ServiceManager1 launcher = new ServiceManager1();
		launcher.launch( args );
		
		timestamp = df1.format(new Date());
		System.out.println(timestamp+"-Terminating eInvoice sync" );
		
		// Clean up older files in the directory
		long ref_date1 = 0;

		// Get a list of files under the log directory
		File f1 = new File ("log") ;
		File[] strFilesDirs = f1.listFiles ();

		// Set the delete point to -7 days from current time
		Calendar file_delete_point = Calendar.getInstance();
		file_delete_point.add(Calendar.DATE, -7);

		// Iterate through list and delete files older than 7 days
		for ( int i = 0 ; i < strFilesDirs.length ; i ++ ) {
			if (strFilesDirs[i].isFile ( )) {
				// Debug System.out.println("Check "+strFilesDirs[i].getName());
				ref_date1 = strFilesDirs[i].lastModified();
				if (ref_date1 < file_delete_point.getTimeInMillis()) {
					System.out.println("Deleting "+strFilesDirs[i].getName());
					strFilesDirs[i].delete();
				}
			}
		}

		// Close stdout
		stdout.close();

	}

	/******************************************************************/

	public void launch( String[] args ) throws Exception
	{
		/* Turn this off for now
		int port = PortGrabber.DEFAULT_PORT;
		if ( args.length == 0 )
		{
			printPortFlagUsage();
		}
		else
		{
			port = parsePortArgument( args );
		}
		lockOnPort( port );
        */
		
		// Launch application here
		ActionManager x = new ActionManager(da, wda);
		
        /*
		releaseLock();
		*/
	}

	/******************************************************************/

	void releaseLock() throws IOException
	{
		DateFormat df1 = new SimpleDateFormat("yyyyMMddkkmmss");
		timestamp = df1.format(new Date());
		debug( ServiceManager1.timestamp+" Releasing port " + getPortGrabber().getPort() + ". Execution terminating.");
		getPortGrabber().release();
	}

	/******************************************************************/

	protected void lockOnPort( int port )
	{
		getPortGrabber().setPort( port );
		if ( getPortGrabber().bindToPort() )
		{
			debug( ServiceManager1.timestamp+" Application bound to port " + port + ". Execution starting." );
		}
		else
		{
			debug( ServiceManager1.timestamp+" Port " + port + " is already in use, exiting..." );
			exit();
		}
	}

	/******************************************************************/

	protected void exit()
	{
		System.exit( 1 );
	}

	/******************************************************************/

	protected int parsePortArgument( String[] args )
	{
		//Set default port
		int port = PortGrabber.DEFAULT_PORT;

		// Look at each argument to find the port number if supplied
		for ( int i = 0; i < args.length; i++ )
		{
			if ( "-p".equals( args[i] ) )
			{
				String arg = nextArg( args, i );
				if ( arg == null )
				{
					printPortFlagUsage();
				}
				else
				{
					try
					{
						port = Integer.parseInt( arg );
					}
					catch ( NumberFormatException e )
					{
						printPortFlagUsage();
					}
				}
			}
		}
		return port;
	}

	/******************************************************************/

	protected void printUsage()
	{
		//debug( "Usage: ExclusiveLauncher -p <port> -main <com.mycompany.MainClass> <main class arguments>");
	}

	/******************************************************************/

	protected void printPortFlagUsage()
	{
		printUsage();
		//debug( "No port specified, using default port " + PortGrabber.DEFAULT_PORT );
	}

	/******************************************************************/

	protected String nextArg( String[] args, int i )
	{
		int nextIndex = i + 1;
		if ( args.length == nextIndex )
		{
			return null;
		}
		return args[nextIndex];
	}

	/******************************************************************/

	public void debug( String inMessage )
	{
		System.out.println( inMessage );
	}

	/******************************************************************/

	protected PortGrabber getPortGrabber()
	{
		if ( fieldPortGrabber == null )
		{
			fieldPortGrabber = new PortGrabber();
		}
		return fieldPortGrabber;
	}

}
