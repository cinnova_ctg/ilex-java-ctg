package net.contingent.eisync;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

public class ActionManager {

	
	WebDataAccess wda = null;
	DataAccess da = null;
		
	// Common variables
	Date x1;
	Date x2;
	long delta;

	boolean run_flag = false;
	
	/******************************************************************/

	/* Name: Constructor
	 * 
	 * Description:
	 * 
	 * This constructor initializes class variables for database access and
	 * then hands off control to the determineAction method which controls
	 * execution.
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	public ActionManager (DataAccess da, WebDataAccess wda) {
		
		this.da = da;
		this.wda = wda;
		
		GregorianCalendar dt1;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		
		
		// Always update POs In Work
		dt1 = new GregorianCalendar();
		System.out.println(df.format(dt1.getTime())+"	Processing In Work");
		processInWorkPOs ();
		
		dt1 = new GregorianCalendar();
		System.out.println(df.format(dt1.getTime())+"	Processing eInvoices");
		determineAction();
		
		//deleteInWorkPOs();
	}
	
	/******************************************************************/
	
	/* Name: Process In Work POs
	 * 
	 * Description:
	 * 
	 * On each execution, process in work purchase orders
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   05/12/09 - 01.00 - Initial Release
	 */
	
	void processInWorkPOs () {
		
		Vector po_in_work_list = new Vector ();
		String processed_po_list = "";
		String delimiter = "";
		
		GregorianCalendar dt1;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm a");		
		dt1 = new GregorianCalendar();
		String dt_Stamp = df.format(dt1.getTime())+"	";

		// As temp fix just delete then reinsert for In work
		deleteInWorkPOs ();
		
		po_in_work_list = getPOsInWork ();
		
		if (po_in_work_list.size() > 0) {
			for (int i = 0; i < po_in_work_list.size(); i++) {
				PurchaseOrder x = new PurchaseOrder (da, wda, (String)po_in_work_list.get(i));
				//processed_po_list += delimiter+"'"+(String)po_in_work_list.get(i)+"'";
				//delimiter = ",";
			}
		}
		
		System.out.println(dt_Stamp+"Processing In Work Complete - Checked: "+po_in_work_list.size());

		
	}
	
	/******************************************************************/
	
	/* Name: Process In Work POs
	 * 
	 * Description:
	 * 
	 * Get the list of in work POs based on Ilex 2.0 criteria
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   05/12/09 - 01.00 - Initial Release
	 *   03/02/10 - 01.01 - Update query with and isnull(lm_po_commission_flag, 0) = 0
	 */
	
	Vector getPOsInWork () {
		
		Vector po_in_work_list = new Vector ();
		
		String sql = "select lm_po_id from lm_purchase_order left join lm_partner_invoice on lm_po_id = lm_pi_po_id "+
                     "where po_status in (1,2) and lm_po_type is not null and lm_po_pwo_type_id is not null and lm_po_pwo_type_id is not null and lm_pi_status is null " +
                     "and isnull(lm_po_change_date,lm_po_create_date) > dateadd(mm, -2, getdate()) " +
                     "and isnull(lm_po_commission_flag, 0) = 0"; //and lm_po_partner_id = 3917
		//System.out.println(sql);
		try {
			Statement stmt = da.databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				po_in_work_list.add(rs.getString(1));
			}
			stmt.close();
		}
		catch (Exception e) {

		}

		return po_in_work_list;
	}
	
	/******************************************************************/
	
	/* Name: Delete In Work POs 
	 * 
	 * Description:
	 * 
	 * Delete in work POs on web which have been completed or otherwise removed
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   05/12/09 - 01.00 - Initial Release
	 */
	
	boolean deleteInWorkPOs () {
		boolean status = true;
		
		int row_count = 0;
		String sql = "delete from lm_ei_ilex_to_web_invoices_01 where lm_pi_status = 'PO In Work'";
		
		try {
			PreparedStatement ps = wda.databaseConnection.prepareStatement(sql);
			//ps.setString(1, valid_in_work_po_list);
			row_count = ps.executeUpdate();

			ps.close();
		}
		catch (Exception e) {
			status = false;
			System.out.println(e);
		}
		
		return status;
	}
	/******************************************************************/
	
	/* Name: Determine Action
	 * 
	 * Description:
	 * 
	 * Determine action performs two basic functions.  First it determines what
	 * type of executon should be performed.  Since it is being called every 2 
	 * minutes from the the cron tab, it needs to determine if it is going to 
	 * execute a full check or check for recent updates.  This determination is made in 
	 * getActions. getActions makes this determination and gets the appropriate lists 
	 * associated with the two actions.  The lists provide the invoices which must 
	 * be inserted, updated, or deleted.
	 * 
	 * Once the lists are available, getActions initiates the appropriate action 
	 * for invoices on each list.
	 * 
	 * This action directly uses InvoiceList to build the list by insert, update, and delete.
	 * It then call iterates through each list and uses InvoiceData to perfform specific actions
	 * for each list.
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	boolean determineAction () {
		boolean status = true;
		int in = 0;
		int up = 0;
		int de = 0;
		int cc = 0;
		
			// Get possible invoice actions. The InvoiceList instance contains
			// a list of inserts, updates and deletes
			
			InvoiceList x = getActions ();
			
			in = 0;
			up = 0;
			de = 0;
			cc = 0;

			// If actions returned, find and execute
			if (x != null) {
				
				if (x.DeleteList.size() > 0) {
					deleteInvoice (x.DeleteList);
					de = x.DeleteList.size();
				}

				if (x.InsertList.size() > 0) {
					in = insertInvoice (x.InsertList);
				}

				if (x.UpdateList.size() > 0) {
					up = updateInvoice (x.UpdateList);
				}
				
				if (x.CancelList.size() > 0) {
					cc = cancelInvoice (x.CancelList);
				}
				Logger.summary(in, up, de);
			}
			
			/* Run from cron tab with port locker
			try {
				Thread.currentThread().sleep(60*1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			*/

		return status;
	}
	
	/******************************************************************/
	
	/* Name: getActions
	 * 
	 * Description:
	 * 
	 * Determine the type of work to be done namely real-time (updates from last hour) or
	 * the daily check.
	 * 
	 * Get invoice actions
	 * 
	 * Inputs: None.
	 * 
	 * Output:
	 * 
	 * InvoiceList class instance
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */

	InvoiceList getActions () {
		
		InvoiceList x = null;
		
		boolean run_flag = true;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		
		GregorianCalendar dt1;
		GregorianCalendar dt2;
		
			dt1 = new GregorianCalendar();
			int year = dt1.get(GregorianCalendar.YEAR);
			int month = dt1.get(GregorianCalendar.MONTH);
			int day = dt1.get(GregorianCalendar.DATE);
			int hour24 = dt1.get(GregorianCalendar.HOUR_OF_DAY);
			int minute = dt1.get(GregorianCalendar.MINUTE);
			
			//hour24 = 23;
			//minute = 33;
			
			if (hour24 == 23 && (minute >= 30 && minute < 35)) {
				System.out.println(df.format(dt1.getTime())+"	Running Daily Catch Up.");
				x = new InvoiceList(da, wda);
				run_flag = false; 
			} else {
				System.out.println(df.format(dt1.getTime())+"	Running Real-time Hour Sync Up.");
				run_flag = true;
				dt2 = new GregorianCalendar();
				dt2.add(Calendar.HOUR_OF_DAY, -72);
				x = new InvoiceList(da, wda, df.format(dt2.getTime()));
			}
		
		return x;
	}

	/******************************************************************/
	
	/* Name: deleteInvoice
	 * 
	 * Description:
	 * 
	 * Delete invoice from the web
	 * 
	 * Get invoice actions
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	 
	boolean deleteInvoice (Vector invoice_list) {
		
		boolean status = true;
		String lm_pi_id = "";
		   
		for (int i = 0; i<invoice_list.size(); i++) {
			
			// Get the invoice
			 lm_pi_id = (String)invoice_list.get(i);
			 
			 // Implement delete action
			 InvoiceData eI = new InvoiceData (wda, lm_pi_id);
			 
			 if (!eI.deleteWebData()) {
				 Logger.error (lm_pi_id, "0", "ActionManager.deleteInvoice", "Bad return from: new InvoiceData (wda, lm_pi_id);");
				 try {
					 PortGrabber.release();
				 } catch (Exception e) {
					 Logger.error (lm_pi_id, "0", "ActionManager.deleteInvoice", "Could not release port.;");
				 }
			 }
		}
		return status;
	}
	
	/******************************************************************/
	
	/* Name: updateInvoice
	 * 
	 * Description:
	 * 
	 * Update invoice on the web from Ilex data
	 * 
	 * Get invoice actions
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	int updateInvoice (Vector invoice_list) {
		
		String lm_pi_id = "";
		int cnt = 0;
		
		for (int i = 0; i<invoice_list.size(); i++) {
			
			// Get the invoice
			 lm_pi_id = (String)invoice_list.get(i);
			 
			 // Implement update
			 InvoiceData eI = new InvoiceData (da, wda, lm_pi_id);
			 
			 if (!eI.updateWeb()) {
				 Logger.error (lm_pi_id, "0", "ActionManager.updateInvoice", "Bad return from: new InvoiceData (da, wda, lm_pi_id);");
                 System.out.println(ServiceManager1.timestamp+" ActionManager.updateInvoice for "+lm_pi_id);
				 try {
					 PortGrabber.release();
				 } catch (Exception e) {
					 Logger.error (lm_pi_id, "0", "ActionManager.deleteInvoice", "Could not release port.;");
				 }
			 } else {
				 cnt++;
			 }
	
		}
		return cnt;
	}
	
	/******************************************************************/
	
	/* Name: insertInvoice
	 * 
	 * Description:
	 * 
	 * Insert invoice on the seb from Ilex data
	 * 
	 * Get invoice actions
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	int insertInvoice (Vector invoice_list) {
		
		String lm_pi_id = "";
		int cnt = 0;

		for (int i = 0; i<invoice_list.size(); i++) {
			
			// Get the invoice
			 lm_pi_id = (String)invoice_list.get(i);
			 
			 // Implement insert
			 InvoiceData eI = new InvoiceData (da, wda, lm_pi_id);
			 
			 if (!eI.insertWebData()) {
				 Logger.error (lm_pi_id, "0", "ActionManager.insertInvoice", "Bad return from: new InvoiceData (da, wda, lm_pi_id);");

			 }	else {
				 cnt++;
			 }


		}
		return cnt;
	}
	
	/******************************************************************/
	
	/* Name: cancelInvoice
	 * 
	 * Description:
	 * 
	 * cancel invoice on the web from Ilex data
	 * 
	 * Get invoice actions
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	int cancelInvoice (Vector invoice_list) {
		
		String lm_pi_id = "";
		int cnt = 0;
		
		for (int i = 0; i<invoice_list.size(); i++) {
			
			// Get the invoice
			 lm_pi_id = (String)invoice_list.get(i);
			 
			 // Implement update
			 InvoiceData eI = new InvoiceData (wda, lm_pi_id);
			 
			 if (!eI.cancelWeb()) {
				 Logger.error (lm_pi_id, "0", "ActionManager.cancelInvoice", "Bad return from: new InvoiceData (wda, lm_pi_id);");
                 System.out.println(ServiceManager1.timestamp+" ActionManager.updateInvoice for "+lm_pi_id);
				 
			 } else {
				 cnt++;
			 }
	
		}
		return cnt;
	}
	/******************************************************************/
    
	public static void main(String[] args) {
		
		WebDataAccess wda = new WebDataAccess ();
		DataAccess da = new DataAccess();

		ActionManager x = new ActionManager(da, wda);

	}

}
