package net.contingent.eisync;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.Map;
import java.util.Vector;

public class InvoiceData {

	/******************************************************************/

	String lm_pi_id;
	String lm_po_VendorID;
	String lm_pi_status ;
	String lm_pi_submit_type ;
	String lm_po_terms;
	String lm_pi_invoice_number ;
	String lm_pi_invoice_date;
	String lm_pi_date_received;
	String lm_pi_submitter;
	String lm_pi_total;
	String lm_pi_partner_comments ;
	String lm_pi_reconcile_comments;
	String lm_pi_approved_total;
	String lm_pi_approved_by;
	String lm_pi_approved_date;
	String lm_pi_gp_paid_date;
	String lm_pi_gp_check_number;
	String lm_po_number;
	String lm_po_issue_date;
	String lm_po_authorised_total;
	String lm_po_deliverables;
	String lm_po_js_id;
	String lm_pi_last_update;
	String site_number;
	String scheduled;
	String job_start;
	String job_end;
	String contingent_contact;
	String contingent_contact_phone;
	String email_address;
	String lm_pi_minuteman_status;
	String lm_po_pwo_type_id;
	Double authorised_total;
	String po_revenue_basis;
	
	LineItem lineitem = null;
		
	WebDataAccess wda = null;
	DataAccess da = null;

	// Common variables
	Date x1;
	Date x2;
	long delta;
	
	String ErrorMessage = "";

	/******************************************************************/

	/* Name: Contructor
	 * 
	 * Description:
	 * 
	 * Get invoice data and if applicable, get line item data
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	public InvoiceData(DataAccess da, WebDataAccess wda, String lm_pi_id) {
		
		this.da = da;
		this.wda = wda;
		this.lm_pi_id = lm_pi_id;
		
		if (getInvoiceData()) {
			
			// Determine if line itmes are needed
			if (!lm_pi_status.equals("Pending Invoice")) {
				getLineItems();
			} else {
				lineitem = null;
			}
				
		} else {
			this.lm_pi_id = null;
			// log error
		}
		
		return;
	}

	/******************************************************************/

	/* Name: Contructor
	 * 
	 * Description:
	 * 
	 * Initialize for delete and cancel actions
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	public InvoiceData(WebDataAccess wda, String lm_pi_id) {
		
		this.wda = wda;
		this.lm_pi_id = lm_pi_id;
		
		return;
	}
	
	/******************************************************************/

	/* Name: Get Invoice Data
	 * 
	 * Description:
	 * 
	 * Call eInvoice SP variation for returning just a single row
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 *   03/02/10 - 01.01 - Zero out po_revenue_basis. This was added to support ESA commissions.
	 *                      These commissions are handled by a different application.
	 */
	
	boolean getInvoiceData () {
		x1 = new Date();
		
		boolean status = true;
		String sp = "exec lm_ei_ilex_to_web_invoices_03 "+lm_pi_id;
		
		try {
			Statement stmt = da.databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sp);
			
			if (rs.next()) {
				lm_po_VendorID =  rs.getString("lm_po_VendorID");
				lm_pi_status  =  rs.getString("lm_pi_status");
				lm_pi_submit_type  =  rs.getString("lm_pi_submit_type");
				lm_po_terms =  rs.getString("lm_po_terms");
				lm_pi_invoice_number  =  rs.getString("lm_pi_invoice_number");
				lm_pi_invoice_date =  rs.getString("lm_pi_invoice_date");
				lm_pi_date_received =  rs.getString("lm_pi_date_received");
				lm_pi_submitter  =  rs.getString("lm_pi_submitter");
				lm_pi_total =  rs.getString("lm_pi_total");
				lm_pi_partner_comments  =  rs.getString("lm_pi_partner_comments");
				lm_pi_reconcile_comments =  rs.getString("lm_pi_reconcile_comments");
				lm_pi_approved_total =  rs.getString("lm_pi_approved_total");
				lm_pi_approved_by  =  rs.getString("lm_pi_approved_by");
				lm_pi_approved_date =  rs.getString("lm_pi_approved_date");
				lm_pi_gp_paid_date  =  rs.getString("lm_pi_gp_paid_date");
				lm_pi_gp_check_number   =  rs.getString("lm_pi_gp_check_number");
				lm_po_number  =  rs.getString("lm_po_number");
				lm_po_issue_date =  rs.getString("lm_po_issue_date");
				lm_po_authorised_total  =  rs.getString("lm_po_authorised_total");
				lm_po_deliverables  =  rs.getString("lm_po_deliverables");
				lm_po_js_id  =  rs.getString("lm_po_js_id");
				lm_pi_last_update =  rs.getString("lm_pi_last_update");
				site_number  =  rs.getString("site_number" );
				scheduled  =  rs.getString("scheduled");
				job_start  =  rs.getString("job_start");
				job_end  =  rs.getString("job_end");
				contingent_contact  =  rs.getString("contingent_contact");
				contingent_contact_phone  =  rs.getString("contingent_contact_phone");
				email_address  =  rs.getString("email_address");
				lm_pi_minuteman_status =  rs.getString("lm_pi_minuteman_status");
				lm_po_pwo_type_id = rs.getString("lm_po_pwo_type_id");
				authorised_total = rs.getDouble("authorised_total");
				po_revenue_basis = rs.getString("po_revenue_basis");
				po_revenue_basis = "0.0";
				
			} else {
				status = false;
			}
			stmt.close();
		}
		catch (Exception e) {
			status = false;
			System.out.println(e);
			ErrorMessage = e.getMessage();
		}
		
		delta = new Date().getTime()-x1.getTime();
		//System.out.println("Get Ilex Invoice: "+delta+" ms");

		return status;
	}
	
	/******************************************************************/

	/* Name: Update Web Data
	 * 
	 * Description:
	 * 
	 * Perform web update.  Before updating, check to see if the invoice
	 * is locked because it is being updated.  Once main record is updated,
	 * update line items as needed.
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	boolean updateWeb () {
		boolean status = true;
		
		// Check for lock - partner is updating
		
		if (invoiceNotLocked()) {

			int row_count = 0;
			String sql = "update lm_ei_ilex_to_web_invoices_01 set ";
			sql += "lm_po_VendorID = ?, ";
			sql += "lm_pi_status = ?, ";
			sql += "lm_pi_submit_type = ?, ";
			sql += "lm_po_terms = ?, ";
			sql += "lm_pi_invoice_number = ?, ";
			sql += "lm_pi_invoice_date = ?, ";
			sql += "lm_pi_date_received = ?, ";
			sql += "lm_pi_submitter = ?, ";
			sql += "lm_pi_total = ?, ";
			sql += "lm_pi_partner_comments = ?, ";
			sql += "lm_pi_reconcile_comments = ?, ";
			sql += "lm_pi_approved_total = ?, ";
			sql += "lm_pi_approved_by = ?, ";
			sql += "lm_pi_approved_date = ?, ";
			sql += "lm_pi_gp_paid_date = ?, ";
			sql += "lm_pi_gp_check_number = ?, ";
			sql += "lm_po_number = ?, ";
			sql += "lm_po_issue_date = ?, ";
			sql += "lm_po_authorised_total = ?, ";
			sql += "lm_po_deliverables = ?, ";
			sql += "lm_po_js_id = ?, ";
			sql += "site_number = ?, ";
			sql += "scheduled = ?, ";
			sql += "job_start = ?, ";
			sql += "job_end = ?, ";
			sql += "contingent_contact = ?, ";
			sql += "contingent_contact_phone = ?, ";
			sql += "email_address = ?, ";
			sql += "lm_pi_minuteman_status = ?, ";
			sql += "lm_pi_invoice_row_change_date = ?, ";
			sql += "po_revenue_basis = ? ";
			sql += "where lm_pi_id = '"+lm_pi_id+"'";

			try {
				PreparedStatement ps = wda.databaseConnection.prepareStatement(sql);
				ps.setString(1, lm_po_VendorID);
				ps.setString(2, lm_pi_status );
				ps.setString(3, lm_pi_submit_type );
				ps.setString(4, lm_po_terms);
				ps.setString(5, lm_pi_invoice_number );
				ps.setString(6, lm_pi_invoice_date);
				ps.setString(7, lm_pi_date_received);
				ps.setString(8, lm_pi_submitter );
				ps.setString(9, lm_pi_total);
				ps.setString(10, lm_pi_partner_comments );
				ps.setString(11, lm_pi_reconcile_comments);
				ps.setString(12, lm_pi_approved_total);
				ps.setString(13, lm_pi_approved_by );
				ps.setString(14, lm_pi_approved_date);
				ps.setString(15, lm_pi_gp_paid_date );
				ps.setString(16, lm_pi_gp_check_number  );
				ps.setString(17, lm_po_number );
				ps.setString(18, lm_po_issue_date);
				ps.setString(19, lm_po_authorised_total );
				ps.setString(20, lm_po_deliverables );
				ps.setString(21, lm_po_js_id );
				ps.setString(22, site_number );
				ps.setString(23, scheduled );
				ps.setString(24, job_start );
				ps.setString(25, job_end );
				ps.setString(26, contingent_contact );
				ps.setString(27, contingent_contact_phone );
				ps.setString(28, email_address );
				ps.setString(29, lm_pi_minuteman_status);
				ps.setString(30, lm_pi_last_update);
				ps.setString(31, po_revenue_basis);

				row_count = ps.executeUpdate();
				
				if (row_count == 0) {
					// log error
					System.out.println(ServiceManager1.timestamp+" Update returned zero row count for "+lm_pi_id+" and "+lm_po_number);
					Logger.error (lm_pi_id, lm_po_number, "InvoiceData.updateWeb", "Update action failed with zero return count");
					status = false;
				}
				ps.close();

				// Determine if line items need to be updated
				if (!manageLineItemInsert()) {
					// log error
					status = false;
					System.out.println(ServiceManager1.timestamp+" Update returned line item error for "+lm_pi_id+" and "+lm_po_number);
					Logger.error (lm_pi_id, lm_po_number, "InVoiceData.updateWeb", "Update action failed for line items");
				}
			}
			catch (Exception e) {
				status = false;
				ErrorMessage = e.getMessage();
				System.out.println(e);
			}

		}
		return status;
	}
	
	/******************************************************************/

	/* Name: Insert Web Data
	 * 
	 * Description:
	 * 
	 * Perform insert action of main record only.  If applicable which is actually
	 * unlikely, insert line items.
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 *   03/03/10 - 01.01 - Default the esa_flag field to zero (0).  This flag is used by ESA commissions
	 *                      which are handled separately
	 */
	
	boolean insertWebData () {
		
		boolean status = true;
		
		int row_count = 0;
		String sql = "insert into lm_ei_ilex_to_web_invoices_01 ";
        sql += "(lm_pi_id, lm_po_VendorID, lm_pi_status, lm_pi_submit_type, lm_po_terms, lm_pi_invoice_number, lm_pi_invoice_date, lm_pi_date_received, lm_pi_submitter, lm_pi_total, lm_pi_partner_comments, lm_pi_reconcile_comments, lm_pi_approved_total, lm_pi_approved_by, lm_pi_approved_date, lm_pi_gp_paid_date, lm_pi_gp_check_number, lm_po_number , lm_po_issue_date, lm_po_authorised_total, lm_po_deliverables, lm_po_js_id, site_number, scheduled, job_start, job_end, contingent_contact, contingent_contact_phone, email_address, lm_pi_minuteman_status, lm_pi_invoice_row_change_date, po_revenue_basis, esa_flag)";
        sql += " values ";
        sql += "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
 
        try {
			PreparedStatement ps = wda.databaseConnection.prepareStatement(sql);
			ps.setString(1, lm_pi_id);
			ps.setString(2, lm_po_VendorID);
			ps.setString(3, lm_pi_status );
			ps.setString(4, lm_pi_submit_type );
			ps.setString(5, lm_po_terms);
			ps.setString(6, lm_pi_invoice_number );
			ps.setString(7, lm_pi_invoice_date);
			ps.setString(8, lm_pi_date_received);
			ps.setString(9, lm_pi_submitter );
			ps.setString(10, lm_pi_total);
			ps.setString(11, lm_pi_partner_comments );
			ps.setString(12, lm_pi_reconcile_comments);
			ps.setString(13, lm_pi_approved_total);
			ps.setString(14, lm_pi_approved_by );
			ps.setString(15, lm_pi_approved_date);
			ps.setString(16, lm_pi_gp_paid_date );
			ps.setString(17, lm_pi_gp_check_number  );
			ps.setString(18, lm_po_number );
			ps.setString(19, lm_po_issue_date);
			ps.setString(20, lm_po_authorised_total );
			ps.setString(21, lm_po_deliverables );
			ps.setString(22, lm_po_js_id );
			ps.setString(23, site_number );
			ps.setString(24, scheduled );
			ps.setString(25, job_start );
			ps.setString(26, job_end );
			ps.setString(27, contingent_contact );
			ps.setString(28, contingent_contact_phone );
			ps.setString(29, email_address );
			ps.setInt(30, Integer.parseInt(lm_pi_minuteman_status));
			ps.setString(31, lm_pi_last_update);
			ps.setString(32, po_revenue_basis);
			ps.setInt(33, 0);
			
			row_count = ps.executeUpdate();

			if (row_count == 0) {
                // log error
				status = false;
			}
			ps.close();
			
			// Determine if line items need to be updated
			if (!manageLineItemInsert()) {
                // log error
				status = false;
			}

		}
		catch (Exception e) {
			status = false;
			ErrorMessage = e.getMessage();
			System.out.println(e);
		}
		
		return status;
	}
	
	/******************************************************************/

	/* Name: Delete Web Data
	 * 
	 * Description:
	 * 
	 * Delete invoice and line itmes
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	boolean deleteWebData () {
		boolean status = true;
		
		int row_count = 0;
		
		// First delete line items
		
		if (deleteLineItems(lm_pi_id)) {
			
			// Then delete invoices
			String sql = "delete from lm_ei_ilex_to_web_invoices_01 where lm_pi_id = ?";

			try {
				PreparedStatement ps = wda.databaseConnection.prepareStatement(sql);
				ps.setString(1, lm_pi_id);

				row_count = ps.executeUpdate();
				
				ps.close();
			}
			catch (Exception e) {
				status = false;
				ErrorMessage = e.getMessage();
				System.out.println(e);
			}
		}
		
		return status;
	}
	
	/******************************************************************/

	/* Name: Get Line Items
	 * 
	 * Description:
	 * 
	 * Get line items  for non fixed price PO.
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */

	boolean getLineItems () {
		boolean status = true;
		lineitem = null;
		
		// If not equal to Fixed Price - get line items
		try {
			if (lm_po_pwo_type_id != null) {

				if (!lm_po_pwo_type_id.equals("2")) {

					// Get invoice details
					if (lm_po_number != null && !lm_po_number.equals("")) {
						lineitem = new LineItem (da, lm_po_number);
					} else {
						// Log error
					}
				} else {
					// Build default line item for FP
					lineitem = new LineItem (authorised_total);
				}
			}
		} catch (Exception e) {
			System.out.println (e+" "+lm_po_number);
		}

		return status;
	}

	/******************************************************************/

	/* Name: Manage Line Item Insert
	 * 
	 * Description:
	 * 
	 * 1) Delete, then each line item
	 * 
	 * 2) Insert each line item
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	 boolean manageLineItemInsert () {
		 boolean status = true;

		 if (lineitem != null && lineitem.LineItems.size() > 0) {
			 
			 if (deleteLineItems(lm_pi_id)) {

				 if (!insertLineItem()) {
					 // Log error
				 }
			 }
			 
		 }

		 return status;
	 }
	 
	 
	/******************************************************************/

	/* Name: Insert Line Items
	 * 
	 * Description:
	 * 
	 * Insert line items
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	/*
	Populate MySQL table for given purchase order/ invoice
	
	lm_partner_invoice_line_item_01 (
	  `id` int(11) NOT NULL auto_increment,  <-- Ignore 
	  `lm_pi_id` int(11) NOT NULL default '0', <-- From invoice list
	  
	  Build this list in LineItems as a Map per resource order by L, M, F, T
	  
	  `lm_li_description` varchar(255) NOT NULL default '',
	  `lm_li_quantity` int(11) NOT NULL default '0',
	  `lm_li_unit_price` float NOT NULL default '0',
	  `lm_li_total_price` float NOT NULL default '0',
	  `lm_li_category` char(1) NOT NULL default ''
	) 

	 */

	 boolean insertLineItem () {
		 boolean status = true;

		 String sql = "";
		 int row_count = 0;

		 sql = "insert into lm_partner_invoice_line_item_01 ";
		 sql += "(lm_pi_id, lm_li_description, lm_li_quantity, lm_li_unit_price, lm_li_total_price, lm_li_category)";
		 sql += " values ";
		 sql += "(?, ?, ?, ?, ?, ?)";

		 for (int i = 0; i<lineitem.LineItems.size(); i++) {
			 Map li = (Map)lineitem.LineItems.get(i);

			 try {
				 PreparedStatement ps = wda.databaseConnection.prepareStatement(sql);
				 
				 ps.setString(1, lm_pi_id);
				 ps.setString(2, (String)li.get("lm_li_description"));
				 ps.setDouble(3, (Double)li.get("lm_li_quantity"));
				 ps.setDouble(4, (Double)li.get("lm_li_unit_price"));
				 ps.setDouble(5, (Double)li.get("lm_li_total_price"));
				 ps.setString(6, (String)li.get("lm_li_category"));

				 row_count = ps.executeUpdate();

				 if (row_count == 0) {
					 // log error
					 status = false;
				 }
				 ps.close();
			 }
			 catch (Exception e) {
				 status = false;
				 ErrorMessage = e.getMessage();
				 System.out.println(e);
			 }
		 }
		 return status;		
	}
	
	/******************************************************************/

	/* Name: Delete Line Items
	 * 
	 * Description:
	 * 
	 * Delete line items
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */

	
	boolean deleteLineItems (String lm_pi_id) {
		boolean status = true;
		
		int row_count = 0;
		String sql = "delete from lm_partner_invoice_line_item_01 where lm_pi_id = ?";
		
		try {
			PreparedStatement ps = wda.databaseConnection.prepareStatement(sql);
			ps.setString(1, lm_pi_id);
			
			row_count = ps.executeUpdate();

			ps.close();
		}
		catch (Exception e) {
			status = false;
			ErrorMessage = e.getMessage();
			System.out.println(e);
		}
		
		return status;
	}
	
	/******************************************************************/

	/* Name: Check lock status
	 * 
	 * Description:
	 * 
	 * If a partner has initiated the invoicing process for this invoice, then 
	 * don't initate an update action
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	boolean invoiceNotLocked () {
		
		boolean status = true;
		
		String sql = "select count(*) from lm_ei_invoice_lock where lm_pi_id = '"+lm_pi_id+"'";
		
		try {
			Statement stmt = wda.databaseConnection.createStatement();
			//ResultSet rs = stmt.executeQuery(sql);
			/*
			if (rs.next()) {
				//
				int lock_count = rs.getInt(1);
									
				if (lock_count > 0) {
					status = false;
				}
			}
            */
			
			stmt.close();
		}

		catch (Exception e) {
			status = false;
			ErrorMessage = e.getMessage();
			System.out.println(e);
		}

		return status;
	}
	
	
	/******************************************************************/

	/* Name: Cancel Web Data
	 * 
	 * Description:
	 * 
	 * Perform web cancel.  This invoice has been canceled or deleted on Ilex and thus may have
	 * incomplete information.  In this situation just cancel out web.
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	boolean cancelWeb () {
		boolean status = true;
		
		// Check for lock - partner is updating
		
		if (invoiceNotLocked()) {

			int row_count = 0;
			String sql = "update lm_ei_ilex_to_web_invoices_01 set ";
			sql += "lm_pi_status = ? ";
			sql += "where lm_pi_id = '"+lm_pi_id+"'";

			try {
				PreparedStatement ps = wda.databaseConnection.prepareStatement(sql);
				ps.setString(1, "Canceled");

				row_count = ps.executeUpdate();
				
				if (row_count == 0) {
					// log error
					System.out.println(ServiceManager1.timestamp+" Update returned zero row count for "+lm_pi_id+" and "+lm_po_number);
					Logger.error (lm_pi_id, lm_po_number, "InvoiceData.updateWeb", "Update action failed with zero return count");
					status = false;
				}
				ps.close();

			}
			catch (Exception e) {
				status = false;
				ErrorMessage = e.getMessage();
				System.out.println(e);
			}

		}
		return status;
	}
	/******************************************************************/
    /*
	public static void main(String[] args) {
		
		WebDataAccess wda = new WebDataAccess ();
		DataAccess da = new DataAccess();
		
		InvoiceData x = new InvoiceData(da, wda, "13244");
		//InvoiceData x = new InvoiceData(wda, "13244");
		//x.updateWeb ();
        x.insertWebData();
        //x.deleteWebData();
	}
   */
}
