package net.contingent.eisync;

import java.io.IOException;
import java.net.ServerSocket;

public class PortGrabber
{
	public static int DEFAULT_PORT = 41017;
	
	protected static int fieldPort = DEFAULT_PORT;
	
	protected static ServerSocket fieldServerSocket;
	
	public boolean bindToPort()
	{
		if ( getServerSocket() == null )
		{
			try
			{
				setServerSocket( new ServerSocket( getPort() ) );
			}
			catch ( IOException e )
			{
				return false;
			}
		}
		return true;
	}
	
	static public void release() throws IOException
	{
		if ( fieldServerSocket != null )
		{
			getServerSocket().close();
			setServerSocket( null );
		}
	}

	static public ServerSocket getServerSocket()
	{
		return fieldServerSocket;
	}
	
	public int getPort()
	{
		return fieldPort;
	}

	public void setPort( int port )
	{
		fieldPort = port;
	}

	static protected void setServerSocket( ServerSocket serverSocket )
	{
		fieldServerSocket = serverSocket;
	}



}
