package net.contingent.eisync;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class LineItem {

	Vector resources = new Vector();
	Vector Labor = new Vector();
	Vector Material = new Vector();
	Vector Freight = new Vector();
	Vector Travel = new Vector();
	Vector Various = new Vector();

	Vector LineItems = new Vector();
	
	DataAccess da = null;
	
	/*
	 * 
	 * This is called from teh context of an Invoice so the specific 
	 * Invoice or PO does not need to be identified.  The PO number is supplied to 
	 * get the list of resources where needed. 
	
	
	Populate MySQL table for given purchase order/ invoice
	
	lm_partner_invoice_line_item_01 (
	  `id` int(11) NOT NULL auto_increment,  <-- Ignore 
	  `lm_pi_id` int(11) NOT NULL default '0', <-- From invoice list
	  
	Build this list in LineItems as a Map per resource order by L, M, F, T
	  
	  `lm_li_description` varchar(255) NOT NULL default '',
	  `lm_li_quantity` int(11) NOT NULL default '0',
	  `lm_li_unit_price` float NOT NULL default '0',
	  `lm_li_total_price` float NOT NULL default '0',
	  `lm_li_category` char(1) NOT NULL default ''
	) 
	*/
	
	/******************************************************************/
	
	/* Name: Contructor
	 * 
	 * Description:
	 * 
	 *  Build list of resources based for the given PO. 
	 * 
	 * Inputs:
	 * 
	 * lm_po_id - PO number
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	LineItem (DataAccess da, String lm_po_id) {
		
		Labor.clear();
		Material.clear();
		Freight.clear();
		Travel.clear();
		Various.clear();

		LineItems.clear();
		
		this.da = da;
		build_TM_Resources (lm_po_id);
		buildLineItems ();
		
		return;
	}
	
	/******************************************************************/
	
	/* Name: Contructor
	 * 
	 * Description:
	 * 
	 *  Build default line item for FP PO
	 * 
	 * Inputs:
	 * 
	 * lm_po_id
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	LineItem (Double unit_cost) {

		Labor.clear();
		Material.clear();
		Freight.clear();
		Travel.clear();
		Various.clear();

		LineItems.clear();
		
		build_FP_Resource (unit_cost);
		buildLineItem (Various, "V");
		
		return;
	}
	
	
	/******************************************************************/
	
	/* Name: build_TM_Resource
	 * 
	 * Description:
	 * 
	 * Get all of the resources associated with this PO and organize into 
	 * the four primary categories: L, M, T, F
	 * 
	 * Inputs:
	 * 
	 * lm_po_id
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	boolean build_TM_Resources (String lm_po_id) {
		boolean status = true;
		
		String sql = "exec ei_get_invoice_line_items_01 "+lm_po_id;
		
		String lm_rs_type = "";
				
		try {
			
			Statement stmt = da.databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				
				Map resource = new HashMap();
				lm_rs_type = rs.getString(2); // 2 - lm_rs_type
				
				if (!rs.wasNull()) {
					
					resource.put("cns_part_number", rs.getString(1));      // 1 - lm_rs_iv_cns_part_number
					resource.put("type",            lm_rs_type);           // 2 - lm_rs_type
					resource.put("name",            rs.getString(3));      // 3 - iv_rp_name
					resource.put("quantity",        rs.getDouble(4));      // 4 - lm_pwo_authorized_qty
					resource.put("unit_cost",       rs.getDouble(5));      // 5 - lm_pwo_authorized_unit_cost
					resource.put("total_cost",      rs.getDouble(6));      // 6 - lm_pwo_authorized_total_cost
					resource.put("drop_shipped",    rs.getString(7));      // 7 - lm_pwo_drop_shipped
					resource.put("pvs_supplied",    rs.getString(8));      // 8 - lm_pwo_pvs_supplied
					resource.put("credit_card",     rs.getString(9));      // 9 - lm_pwo_credit_card
										
					if (lm_rs_type.equals("L")) {
						Labor.add(resource);
					} else if (lm_rs_type.equals("M")) {
						Material.add(resource);
					} else if (lm_rs_type.equals("T")) {
						Travel.add(resource) ;
					} else if (lm_rs_type.equals("F")) {
						Freight.add(resource);
					} else  {
						status = false;
						break;
					}
				} else {
					status = false;
					break;
				}		
			}
			
		} catch (SQLException e) {
			status = false;
			System.out.println("Error Message: "+e);
		}

		return status;
	}
	
	/******************************************************************/
	
	/* Name: buildTM_Resources
	 * 
	 * Description:
	 * 
	 * Build a simple FP resource
	 * 
	 * Inputs:
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	boolean build_FP_Resource (Double authorized) {
		boolean status = true;
		
		Labor.clear();
		Material.clear();
		Freight.clear();
		Travel.clear();
		Various.clear();
		
		Map resource = new HashMap();
		
		resource.put("cns_part_number", "999999999");      
		resource.put("type",            "V");
		resource.put("name",            "Fixed Priced Services"); 
		resource.put("quantity",        1.0);         // Default value for FP
		resource.put("unit_cost",       authorized);  // From PO data <- total authorized cost
		resource.put("total_cost",      authorized); 
		resource.put("drop_shipped",    "N");
		resource.put("pvs_supplied",    "Y");
		resource.put("credit_card",     "N");
		
		Various.add(resource);

		return status;
	}
	
	/******************************************************************/
	
	/* Name: buildLineItems
	 * 
	 * Description:
	 * 
	 * Iterate through each type of resource and build the list of 
	 * line itms for the PO.  This takes the higher-level resource information
	 * and distills it to what is needed for line itme display.
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	boolean buildLineItems () {
		boolean status = true;
		
		if (Labor.size() > 0) {
			buildLineItem (Labor, "L");
		}
		
		if (Material.size() > 0) {
			buildLineItem (Material, "M");
		}

		if (Freight.size() > 0) {
			buildLineItem (Freight, "F");
		}

		if (Travel.size() > 0) {
			buildLineItem (Travel, "T");
		}

		return status;
	}
	
	/******************************************************************/
	
	/* Name: buildLineItems
	 * 
	 * Description:
	 * 
	 * Distill the resource information into line item information.
	 * 
	 * Inputs:
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	boolean buildLineItem (Vector rl, String type) {
		boolean status = true;
		
		Map li = new HashMap();
		String labor_type;
		String pvs_supplied;
		String drop_shipped;
		
		for (int i=0; i<rl.size(); i++) {
			
			// Get each resource
			Map resource = (Map)rl.get(i);
			
			labor_type = (String)resource.get("cns_part_number");
			
			// Skip resource that pass the following test - these are not allotted to a vendor or
			// technically should not be even though they are on the PO
			
			if (labor_type.startsWith("6")) {
				continue;
			}
			
			if (type.equals("M")) {
				pvs_supplied = (String)resource.get("pvs_supplied");
				drop_shipped = (String)resource.get("drop_shipped");
				
				if (pvs_supplied.equals("N") || drop_shipped.equals("N")) {
					continue;
				}
			}
			
			// Build the line item from the resource information

			li.put("lm_li_description",   resource.get("name"));
			li.put("lm_li_quantity",      resource.get("quantity"));
			li.put("lm_li_unit_price",    resource.get("unit_cost"));
			li.put("lm_li_total_price",   resource.get("total_cost"));
			li.put("lm_li_category",      type);
			
			// Add to LineItem Vector
			LineItems.add(li);
			
		}
		
		return status;
	}
	
}
