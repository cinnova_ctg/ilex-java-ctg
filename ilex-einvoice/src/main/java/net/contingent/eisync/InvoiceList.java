package net.contingent.eisync;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

public class InvoiceList {

	Map IlexInvoices = new HashMap();
	
    Map Web6Month_status;
    Map Web6Month_vendor;
	
	Vector Ilex6MonthList_ID;
	Vector Ilex6MonthList_Status;
	Vector Ilex6MonthList_Vendor;
	
	Vector InsertList = new Vector();
	Vector UpdateList = new Vector();
	Vector DeleteList = new Vector();
	Vector CancelList = new Vector();
	
	WebDataAccess wda = null;
	DataAccess da = null;
	
	// Common variables
	Date x1;
	Date x2;
	long delta;
	
	String ErrorMessage = "";
	
	/******************************************************************/
	
	/* Name: InvoiceList - Short List
	 * 
	 * Description: Constructor
	 * 
     *  1) Get the list of ilex invoices based off of a starting time stamp.  This 
     *     uses a SP which will determine the best date against which to compare the input timestamp
     *     against the invoice last update or create time stamp (looks across the PO and job dates).  
     *     If a date different than the invoice date is used, the invoice should be updated.
     *  
     *  2) Attempt to locate the invoice by lm_pi_id.  Since only valid invoices are selected
     *     all inoices should be located.  If not found, it should be inserted.
     *  
     *  
	 * Inputs:
	 * 
	 * Database connection - MS SQL and MySQL
	 * 
	 * Time stamp - this is the target date/time to select from
	 * 
	 * Output:
	 * 
	 * ActionLists - made available while in scope (used by FlowManager)
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	InvoiceList (DataAccess da, WebDataAccess wda, String timestamp) {
		
		this.da = da;
		this.wda = wda;
		
		// First get the list from Ilex of all invoices for last 7 days
		
		if (getIlexActiveList(timestamp)) {
			
			// Then compare one by to the MySQl record
			if (!checkWebInvoice()) {
				Logger.error ("0", "0", "InvoiceList.InvoiceList", "Error retrieving Web list");
			}
			
		} else {
			Logger.error ("0", "0", "InvoiceList.InvoiceList", "Error retrieving Ilex list");
		}
			
		return;
	}
	
	/******************************************************************/
	
	/* Name: InvoiceList - Long List (Normally Once a Day)
	 * 
	 * Description: Constructor
	 * 
     *  1) Get the last 6 months worth of data from Ilex and compare to Web.  Get the 
     *     status and vendor identifier
     *    
     *     a) If not found, add to insert list
     *     b) If not the same, add to update list
     *  
     *  2) Get the last 6 months of Web data and compare against Ilex.  If Web record
     *     is not found in Ilex, then delete from Web
     *  
	 * Inputs:
	 * 
	 * Database connection - MS SQL and MySQL
	 * 
	 * Output:
	 * 
	 * ActionLists - made available while in scope (used by FlowManager)
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	InvoiceList (DataAccess da, WebDataAccess wda) {
		
		this.da = da;
		this.wda = wda;
		
		// First get the list from Ilex of all invoices for last ... 
		
		if (getIlex6MonthList()) {
			
			//checkWeb6MonthCheck();
			
			if (getWeb6Monthlist()) {
				
				// Compare lists

				compareIlexWeb ();
			}
			
		} else {
           // Log error event
		}
			
		return;
	}
	/******************************************************************/
	
	/* Name: Get the Ilex list
	 * 
	 * Description:
	 * 
	 * Call the SP and get the lm_pi_id and d/t stamp
	 * 
	 * Inputs:
	 * 
	 * timestamp - passed from constructor
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 *   03/02/10 - N/C   - SP ei_get_invoice_list_02 to exclude ESA commissions
	 */
	
	boolean getIlexActiveList(String timestamp) {
		x1 = new Date();
		
		GregorianCalendar dt1;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm a");		
		dt1 = new GregorianCalendar();
		String dt_Stamp = df.format(dt1.getTime())+"	";

		boolean status = true;
		
		IlexInvoices.clear();
		String sp = "exec ei_get_invoice_list_02 '"+timestamp+"'";
		try {
			Statement stmt = da.databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sp);
			
			while (rs.next()) {
				IlexInvoices.put(rs.getString(1), rs.getString(2));
			}
			stmt.close();
		}
		catch (Exception e) {
			status = false;
			System.out.println(e);
			ErrorMessage = e.getMessage();
		}
		
		delta = new Date().getTime()-x1.getTime();
		System.out.println(dt_Stamp+"Get Ilex Real-time Sync List - Ilex eInvoices to check: "+IlexInvoices.size()+" records"+" (Processing time: "+delta/1000+" sec)");
		
		return status;
	}
	
	/******************************************************************/
	
	/* Name: Check Web Invoice
	 * 
	 * Description:
	 * 
	 * Check the Web for each invoice record, d/t stamps should match.
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	boolean checkWebInvoice() {
		x1 = new Date();
		
		GregorianCalendar dt1;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm a");		
		dt1 = new GregorianCalendar();
		String dt_Stamp = df.format(dt1.getTime())+"	";

		boolean status = true;
		
		UpdateList.clear();
		InsertList.clear();
		
		// Iterate thru the list of Ilex invoices and check to see if present on the web.
		
		for (Iterator i = IlexInvoices.keySet().iterator(); i.hasNext();) {
			String lm_pi_id = (String) i.next();
			String date_stamp = (String)IlexInvoices.get(lm_pi_id);
			
			String sql = "select lm_pi_invoice_row_change_date from lm_ei_ilex_to_web_invoices_01 where lm_pi_id = '"+lm_pi_id+"'";
			
			try {
				Statement stmt = wda.databaseConnection.createStatement();
				ResultSet rs = stmt.executeQuery(sql);
				
				if (rs.next()) {
					//
					String w_date_stamp = rs.getString(1).substring(0, 19);
										
					if (!date_stamp.equals(w_date_stamp)) {
						UpdateList.add(lm_pi_id);
						System.out.println("Update Web - Web:"+w_date_stamp+" vs Ilex: "+date_stamp+" for "+lm_pi_id);
					}
						
				} else {
					InsertList.add(lm_pi_id);
					//System.out.println("Missing on Web - Ilex: "+lm_pi_id);
				}

				stmt.close();
			}

			catch (Exception e) {
				status = false;
				ErrorMessage = e.getMessage();
				System.out.println(e);
			}

		}
		
		delta = new Date().getTime()-x1.getTime();

		System.out.println(df.format(dt1.getTime())+"	Real-time Sync - Checked: "+IlexInvoices.size()+" with Updates: "+UpdateList.size()+" and Inserts: "+InsertList.size()+" (Processing time: "+delta/1000+" sec)");
		
		return status;
	}
	
	/******************************************************************/
	
	/* Name: Get Ilex 6 Month List
	 * 
	 * Description:
	 * 
	 * 1) Determine timestamp based on current d/t minus six months
	 * 
	 * 2) Build a parallel vectors for lm_po_id, status and partner
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 *   05/28/09 - 02.00 - Update for Ilex 2+
	 *   03/02/10 - N/C   - SP ei_get_invoice_list_02 to exclude ESA commissions
	 */
	
	boolean getIlex6MonthList() {
		
		x1 = new Date();
		boolean status = true;
		
		// Manage the invoice in parallel vectors
		Ilex6MonthList_ID = new Vector();
		Ilex6MonthList_Status = new Vector();
		Ilex6MonthList_Vendor = new Vector();

		// Determine the 6 month timestamp
		DateFormat df = new SimpleDateFormat("MM/dd/yyy");
		GregorianCalendar dt = new GregorianCalendar();
		dt.add(Calendar.MONTH, -36);
		String timestamp = df.format(dt.getTime());
		
		// Read and save invoice data
		// OK for Ilex 2+
		String sp = "exec ei_get_invoice_list_02 '"+timestamp+"'";
		System.out.println(sp);
		try {
			Statement stmt = da.databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sp);
			
			while (rs.next()) {
				String test1 = rs.getString(1);
				if (test1.equals("26868")) {
					System.out.println("Found");
				}
				Ilex6MonthList_ID.add(rs.getString(1));
				Ilex6MonthList_Status.add(rs.getString(4));
				Ilex6MonthList_Vendor.add(rs.getString(6));
			}
			stmt.close();
		}

		catch (Exception e) {
			status = false;
			ErrorMessage = e.getMessage();
			// Log error
			System.out.println(e);
		}
		delta = new Date().getTime()-x1.getTime();
		System.out.println("Ilex 6 Month  List: "+delta/1000+" sec for "+Ilex6MonthList_ID.size()+" records");
		
		return status;
	}
	
	/******************************************************************/
	
	/* Name: Get Web Invoice Data - Long List
	 * 
	 * Description:
	 * 
	 * Get the last 6 months worth of invoices.  This will not necessarily parallel the Ilex
	 * list but it will be close.  All exception become error conditions which should be corrected
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	boolean getWeb6Monthlist() {
		x1 = new Date();
		boolean status = true;
		
		// Maps are both indexed with th elm_pi_id which will be common to Ilex list
	    Web6Month_status = new HashMap();
	    Web6Month_vendor = new HashMap();

		// Read and store
		String sp = "select lm_pi_id, lm_pi_status, lm_po_VendorID from lm_ei_ilex_to_web_invoices_01 where lm_pi_invoice_row_change_date > SYSDATE() - INTERVAL 36 MONTH order by lm_pi_invoice_row_change_date";
		try {
			Statement stmt = wda.databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sp);
			
			while (rs.next()) {
				Web6Month_status.put(rs.getString(1), rs.getString(2));
				Web6Month_vendor.put(rs.getString(1), rs.getString(3));
			}
			stmt.close();
		}
		catch (Exception e) {
			status = false;
			ErrorMessage = e.getMessage();
			System.out.println(e);
		}
		
		delta = new Date().getTime()-x1.getTime();
		System.out.println("Web 6 Month List: "+delta/1000+" sec for "+Web6Month_status.size()+" records");
		
		return status;
	}

	/******************************************************************/

	/* Name: Compare Ilex and Web Six Month lists
	 * 
	 * Description:
	 * 
	 * 1) Check vendor - vendors should match.  If not mark for delete/insert
	 * 
	 * 2) Check status - if no match - mark for update.
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	void compareIlexWeb() {
		x1 = new Date();

		boolean status = true;
		String lm_pi_id = "";
		String lm_pi_status = "";
		String lm_po_VendorID = "";
		String w_lm_pi_status = "";
		String w_lm_po_VendorID = "";

		DeleteList.clear();
		InsertList.clear();
		UpdateList.clear();
		CancelList.clear();
		
		// Iterate through the Ilex list to determine corresponding web status
		for (int i = 0; i<Ilex6MonthList_ID.size(); i++ ) {

			// ILex
			lm_pi_id = (String)Ilex6MonthList_ID.get(i);
			lm_pi_status = (String)Ilex6MonthList_Status.get(i);
			lm_po_VendorID = (String)Ilex6MonthList_Vendor.get(i);
			// Web
			w_lm_pi_status = (String)Web6Month_status.get(lm_pi_id);
			w_lm_po_VendorID = (String)Web6Month_vendor.get(lm_pi_id);
			
			// The Web vendor and status information must be available to begin check
			
			if (w_lm_po_VendorID != null && w_lm_pi_status != null) {

				if (lm_po_VendorID != null && !lm_po_VendorID.equals(w_lm_po_VendorID)) {
					DeleteList.add(lm_pi_id);
					InsertList.add(lm_pi_id);
					//System.out.println("Vendor ID did not match - Web: "+w_lm_po_VendorID+" Ilex:"+lm_po_VendorID+" ID "+lm_pi_id);

				} else if (!lm_pi_status.equals(w_lm_pi_status)) {
					// Adjust for status name difference between Ilex and the Web - first two condition represent equivalent statuses
					if (lm_pi_status.equals("Pending Receipt") && (w_lm_pi_status.equals("PO In Work") || w_lm_pi_status.equals("Pending Invoice"))) {
						// Ignore this condition
					} else if (lm_pi_status.equals("Received") && w_lm_pi_status.equals("Invoiced")) {
						// Ignore this condition
					} else if (w_lm_pi_status.equals("Canceled")) {
						// Ignore this condition
					}else {
						
						if (lm_pi_status.equals("Deleted") || lm_pi_status.equals("Canceled")) {
							CancelList.add(lm_pi_id);
							System.out.println("Ilex Canceled PO did not match - Web: "+w_lm_pi_status+" Ilex:"+lm_pi_status+" ID "+lm_pi_id);
						} else {
							UpdateList.add(lm_pi_id);
							System.out.println("Status did not match - Web: "+w_lm_pi_status+" Ilex:"+lm_pi_status+" ID "+lm_pi_id);
						}
					}
				}
			} else {
				if (lm_pi_status.equals("Deleted") || lm_pi_status.equals("Canceled")) {
					// Ignore this condition - not on web and does not need to go.
				} else {
					//System.out.println("Invoice missing from Web: Ilex:"+lm_po_VendorID+" ID "+lm_pi_id);
					InsertList.add(lm_pi_id);
				}
			}
		}	

		delta = new Date().getTime()-x1.getTime();
		//System.out.println("Web List for 6 Months vs Ilex: "+delta/1000+" sec for Updates "+UpdateList.size()+" and Inserts: "+InsertList.size()+" and Deletes: "+DeleteList.size());
	}

	/******************************************************************/
	
	/* Name: Compare Ilex Long List to Web Long List - Individual Check
	 * 
	 * Description:
	 * 
	 * 
	 * Inputs: None.
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
	
	boolean checkWeb6MonthCheck() {
		x1 = new Date();
		
		boolean status = true;
		String lm_pi_id = "";
		String lm_pi_status = "";
		String lm_po_VendorID = "";
		String w_lm_pi_status = "";
		String w_lm_po_VendorID = "";

		// Iterate thru the list of Ilex invoices and compare web.
		
		for (int i = 0; i<Ilex6MonthList_ID.size(); i++ ) {
			
			lm_pi_id = (String)Ilex6MonthList_ID.get(i);
			lm_pi_status = (String)Ilex6MonthList_Status.get(i);
			lm_po_VendorID = (String)Ilex6MonthList_Vendor.get(i);
			
			String sql = "select lm_pi_status, lm_po_VendorID from lm_ei_ilex_to_web_invoices_01 where lm_pi_id = '"+lm_pi_id+"'";
			
			try {
				Statement stmt = wda.databaseConnection.createStatement();
				ResultSet rs = stmt.executeQuery(sql);
				
				if (rs.next()) {
					w_lm_pi_status = rs.getString(1);
					w_lm_po_VendorID = rs.getString(2);
					
					if (!lm_po_VendorID.equals(w_lm_po_VendorID)) {
						DeleteList.add(lm_pi_id);
						InsertList.add(lm_pi_id);
					} else if (!lm_pi_status.equals(w_lm_pi_status)) {
						UpdateList.add(lm_pi_id);
					}
				} else {
					InsertList.add(lm_pi_id);
				}
				stmt.close();
			}
			catch (Exception e) {
				status = false;
				ErrorMessage = e.getMessage();
				System.out.println(e);
			}
		}
		
		delta = new Date().getTime()-x1.getTime();
		//System.out.println("Compare 6 Lists: "+delta/1000+" sec for Updates "+UpdateList.size()+" and Inserts: "+InsertList.size()+" and Deletes: "+DeleteList.size());
		
		return status;
	}
	
	/******************************************************************/

	/*
	public static void main(String[] args) {

		WebDataAccess wda = new WebDataAccess ();
		DataAccess da = new DataAccess();

		Date dt = new Date(); // Default to current date/time
		DateFormat df = new SimpleDateFormat("MM/dd/yyy hh:mm a");

		GregorianCalendar dt1 = new GregorianCalendar();
		dt1.setTime(dt);
		dt1.add(Calendar.DAY_OF_MONTH, -1);
		dt = dt1.getTime();
		String date_time_stamp = df.format(dt);
		
		InvoiceList x = new InvoiceList(da, wda, date_time_stamp);
		//InvoiceList x = new InvoiceList(da, wda);
		
		System.out.println(date_time_stamp);
	}
   */

	/******************************************************************/
	
	/* Name: 
	 * 
	 * Description:
	 * 
	 * 
	 * Inputs:
	 * 
	 * Output: None.
	 * 
	 * Exceptions: None.
	 * 
	 * Logging: None.
	 * 
	 * Special Test Requirements: None.
	 * 
	 * Revision History
	 *   02/28/08 - 01.00 - Initial Release
	 */
}
