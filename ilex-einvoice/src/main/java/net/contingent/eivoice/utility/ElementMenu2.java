package net.contingent.eivoice.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/*************************************************************************/

	/* Title: 
	 * 
	 * Description:
	 * 
	 * 
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 */

public class ElementMenu2 extends Element{

	ElementMenu2 (Map init, HttpServletRequest request) {

		super (init, request);
		createElement();

	}

/*************************************************************************/
	
	/* Title: 
	 * 
	 * Description:
	 * 
	 *  
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 *  
	 */

	void createElement () {
	 
     String[] values = data.split("~");
     String selected = "";
     String use_value = "";
     String[] value;
     
     if (currentValue != null) {
     	use_value = currentValue;
     } 

     elementHTML = "\t<option value=\"\"> ---Select--- </option>\n";
     String cr = "\n";
     for (int i=0; i<values.length; i++)
     {
    	if (i == values.length-1) {
    		cr = "";
    	}
    	// Split value on |
    	value = values[i].split("\\|");
    	if (use_value.equals(value[0])) {
    		selected = " SELECTED";
    	}
    	elementHTML = elementHTML.concat("\t<option value=\""+value[0]+"\""+selected+">"+value[1]+"</option>"+cr);
  	    selected = "";
     }
      
     elementHTML = "<select name=\""+name+"\" size=\"1\" "+javascript+" class=\"select\">\n"+elementHTML+"</select>";
     
	}

}

