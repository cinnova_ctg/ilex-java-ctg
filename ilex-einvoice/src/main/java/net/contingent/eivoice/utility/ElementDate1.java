package net.contingent.eivoice.utility;

import java.util.Map;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

/*************************************************************************/

	/* Title: 
	 * 
	 * Description:
	 * 
	 * 
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 */

	public class ElementDate1 extends Element{

		
		ElementDate1 (Map init, HttpServletRequest request) {

			super (init, request);
			createElement();

		}

/*************************************************************************/
	
	/* Title: 
	 * 
	 * Description:
	 * 
	 *  
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 *  
	 */

		void createElement () {

			String image = "";
			String use_value = "";

			if (currentValue != null) {
				use_value = currentValue;
			} else if (data.equals("Today")) {
				Date timeDate = new Date(System.currentTimeMillis());
				DateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
				use_value = fmt.format(timeDate);
			} else {
				use_value = "";
			}

			elementHTML = "<input name=\""+name+"\" type=\"text\" size=\""+size+"\" "+javascript+" class=\""+cssclass+"\" value=\""+use_value+"\">";
		}
	}
