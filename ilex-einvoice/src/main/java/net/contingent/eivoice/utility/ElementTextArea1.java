package net.contingent.eivoice.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/*************************************************************************/

	/* Title: 
	 * 
	 * Description:
	 * 
	 * 
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 */

public class ElementTextArea1 extends Element{

	ElementTextArea1 (Map init, HttpServletRequest request) {

		super (init, request);
		createElement();

	}

/*************************************************************************/
	
	/* Title: 
	 * 
	 * Description:
	 * 
	 *  
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 *  
	 */

	void createElement () {
	 
     String use_value = "";
     
     if (currentValue != null) {
     	use_value = currentValue;
     }  

     elementHTML = "<textarea name=\""+name+"\" rows=\"3\" cols=\"50\" class=\"textarea1\">"+use_value+"</textarea>\n";

	}

}

