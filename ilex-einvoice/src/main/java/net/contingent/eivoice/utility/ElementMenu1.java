package net.contingent.eivoice.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/*************************************************************************/

	/* Title: 
	 * 
	 * Description:
	 * 
	 * 
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 */

public class ElementMenu1 extends Element{

	ElementMenu1 (Map init, HttpServletRequest request) {

		super (init, request);
		createElement();

	}

/*************************************************************************/
	
	/* Title: 
	 * 
	 * Description:
	 * 
	 *  
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 *  
	 */

	void createElement () {
	 
     String[] values = data.split("~");
     String selected = "";
     String use_value = "";
     
     if (currentValue != null) {
     	use_value = currentValue;
     } 

     elementHTML = "\t<option value=\"\"> -- Select Option -- </option>\n";
     String cr = "\n";
     for (int i=0; i<values.length; i++)
     {
    	if (i == values.length-1) {
    		cr = "";
    	}
    	if (use_value.equals(values[i])) {
    		selected = " SELECTED";
    	}
    	elementHTML = elementHTML.concat("\t<option value=\""+values[i]+"\""+selected+">"+values[i]+"</option>"+cr);
  	    selected = "";
     }
      
     elementHTML = "<select name=\""+name+"\" size=\"1\" class=\""+cssclass+"\" "+javascript+">\n"+elementHTML+"</select>";
     
	 
	}

}

