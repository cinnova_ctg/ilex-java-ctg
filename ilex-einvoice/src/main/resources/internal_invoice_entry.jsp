<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element1.tld" prefix="el"%>
<%@ page import = "java.util.Map" %>
<%
Map coreData = (Map)request.getAttribute("coreData");
String po_authotized_amount = (String)coreData.get("lm_po_authorised_total");
if (po_authotized_amount == null) {
	po_authotized_amount = "0.00";
	
}
String lm_pi_id = (String)request.getAttribute("lm_pi_id");
String lm_po_id = (String)request.getAttribute("lm_po_id");
%>
<html>
<head>
<link href="css/content.css" type="text/css" rel="stylesheet"/>
<link href="css/cal.css" type="text/css" rel="stylesheet"/>
<script language="javascript" src="scripts/newrow.js">
</script>
<script language="javascript" src="scripts/common.js">
</script>
<script language="javascript" src="scripts/CalendarPopup.js"></script>
<script language="JavaScript">
	var cal = new CalendarPopup("date_calendar");
	cal.setCssPrefix("TEST");
	var po_total = "<%=po_authotized_amount%>";
</script>
<script language="javascript" type="text/javascript">
	function validate_form(frm){
		try{
			ret = validateNotEmpty(frm.lm_pi_invoice_number, "Please Enter Invoice Number.");
			if(!ret)
				return false;			
			ret = validateNotEmpty(frm.lm_pi_invoice_date, "Please Enter the Invoice Date");
			if(!ret)
				return false;
			ret = validateNotEmpty(frm.lm_pi_date_received, "Please Enter the Received Date");
			if(!ret)
				return false;
			ret = validateNotEmpty(frm.lm_pi_total, "Please Enter the Invoice Total");
			if(!ret)
				return false;
			return CheckInvoiceTotal(frm.lm_pi_total);
		}
		catch(ex){
			alert(ex);
			return false;
		}
	}

	var po_total;
	function CheckInvoiceTotal(txtInv){
		if(po_total != txtInv.value)
			return confirm("The invoice total does not match the PO authorized amount.  Do you want to continue?");
		else 
			return true;
	}
</script>
<style>
.mainBody {
  margin : 0px 0px 0px 10px;
}
</style>
</head>
<body>
<DIV id="date_calendar" style="Z-INDEX: 1000; VISIBILITY: hidden; POSITION: absolute; BACKGROUND-COLOR: white; border: none;"></DIV>
<div class="mainBody">
<form name="form1" action="InternalAction.xo?" onSubmit="return validate_form(form1);" method="post">
  <table width="780" border="0" cellpadding="0" cellspacing="1">
    <tr>
      <td colspan="2"><h1>Internal Invoice Entry Form</h1></td>
    </tr>
    <tr>
      <td height="5" colspan="2"></td>
    </tr>
    <tr>
      <td colspan="4"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="remSpace">
          <tbody>
            <tr>
              <td class="formCaption"><b>PO Details for <%=(String)coreData.get("lm_po_number")%></b></td>
              <td class="formCaption"><b>Job Details</b></td>
            </tr>
            <tr>
              <td valign="top" width="50%"><table border="0" cellpadding="0" cellspacing="1" width="100%">
                  <tbody>
                    <tr>
                      <td class="colDark">Partner:</td>
                      <td class="colLight"><%=(String)coreData.get("lo_om_division_ptn")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Contact Person:</td>
                      <td class="colLight"><%=(String)coreData.get("ptn_name")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Contact Phone:</td>
                      <td class="colLight"><%=(String)coreData.get("ptn_phone")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Contact Email:</td>
                      <td class="colLight"><%=(String)coreData.get("ptn_email")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Delivered:</td>
                      <td class="colLight"><%=(String)coreData.get("lm_po_deliver_by_date")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Type:</td>
                      <td class="colLight"><%=(String)coreData.get("lm_pwo_type_desc")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Terms: </td>
                      <td class="colLight"><%=(String)coreData.get("lm_pwo_terms_master_data")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">PO Notes (Internal)</td>
                      <td class="colLight"><%=(String)coreData.get("lm_pi_internal_comments")%></td>
                    </tr>
                  </tbody>
                </table></td>
              <td valign="top" width="50%"><table border="0" cellpadding="0" cellspacing="1" width="100%">
                  <tbody>
                    <tr>
                      <td class="colDark">Account:</td>
                      <td class="colLight"><%=(String)coreData.get("lo_ot_name")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Project:</td>
                      <td class="colLight"><%=(String)coreData.get("lx_pr_title")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Job:</td>
                      <td class="colLight"><%=(String)coreData.get("lm_js_title")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Site:</td>
                      <td class="colLight"><%=(String)coreData.get("lm_si_number")%></td>
                    </tr>                    
                    <tr>
                      <td class="colDark">Status:</td>
                      <td class="colLight"><%=(String)coreData.get("job_status")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Date:</td>
                      <td class="colLight"><%=(String)coreData.get("job_complete_date")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">PM:</td>
                      <td class="colLight"><%=(String)coreData.get("lo_pc_last_name_pm")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Owner:</td>
                      <td class="colLight"><%=(String)coreData.get("lo_pc_last_name_owner")%></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td colspan="2" class="secSeperator"></td>
    </tr>	
    <tr>
      <td colspan="2" class="formCaption">Line Items&nbsp;
        <input type="button" value="Add" class="addButton" onclick="addRowToTable();"></td>
    </tr>
    <tr>
      <td colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="1" class="remSpace"  id="tblSample">
          <tr>
            <th width="2%">Item</th>
            <th width="20%">Description</th>
            <th width="3%">Quantity</th>
            <th width="4%">Unit Price ($)</th>
            <th width="4%">Total Price ($)</th>
            <th width="3%">Category</th>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="2" class="secSeperator"></td>
    </tr>
    <tr>
      <td colspan="4" class="formCaption">Invoice Amount</td>
    </tr>
    
    <input type="hidden" name="lm_pi_id" value="<%=lm_pi_id%>">
    <input type="hidden" name="lm_po_id" value="<%=lm_po_id%>">
    <input type="hidden" name="lm_pi_submit_type" value="Internal">
    <input type="hidden" name="action" value="internal_invoice_entry">
    
    <tr>
      <td class="colDark">Invoice #: <font color="#ff0000">*</font></td>
      <td class="colLight"><el:element type="text1" request="<%=request%>" name="lm_pi_invoice_number" size="17" cssclass="text"/></td>
    </tr>
    <tr>
      <td class="colDark">Invoice Date: <font color="#ff0000">*</font></td>
      <td class="colLight"><el:element type="date1" request="<%=request%>" name="lm_pi_invoice_date" size="10" cssclass="text"/>
        &nbsp;&nbsp;&nbsp;<a id="anchor18" title="cal.select(document.form1.lm_pi_invoice_date,'anchor18','MM/dd/yyyy'); return false;" onclick="cal.select(document.form1.lm_pi_invoice_date,'anchor18','MM/dd/yyyy'); return false;" href="#" name="anchor18"><img align="absmiddle" border="0" src="images/calenderImage.gif"></a></td>
    </tr>
    <tr>
      <td class="colDark">Received Date: <font color="#ff0000">*</font></td>
      <td class="colLight"><el:element type="date1" request="<%=request%>" name="lm_pi_date_received" size="10" cssclass="text"/>
        &nbsp;&nbsp;&nbsp;<a id="anchor19" title="cal.select(document.form1.lm_pi_date_received,'anchor19','MM/dd/yyyy'); return false;" onclick="cal.select(document.form1.lm_pi_date_received,'anchor19','MM/dd/yyyy'); return false;" href="#" name="anchor19"><img align="absmiddle" border="0" src="images/calenderImage.gif"></a></td>
    </tr>
    <tr>
      <td class="colDark">Labor:</td>
      <td class="colLight"><el:element type="text1" request="<%=request%>" name="lm_pi_labor_amount" size="17" cssclass="text"/></td>
    </tr>
    <tr>
      <td class="colDark">Travel:</td>
      <td class="colLight"><el:element type="text1" request="<%=request%>" name="lm_pi_travel_amount" size="17" cssclass="text"/></td>
    </tr>
    <tr>
      <td class="colDark">Freight:</td>
      <td class="colLight"><el:element type="text1" request="<%=request%>" name="lm_pi_frieght_amount" size="17" cssclass="text"/></td>
    </tr>    
    <tr>
      <td class="colDark">Materials:</td>
      <td class="colLight"><el:element type="text1" request="<%=request%>" name="lm_pi_materials_amount" size="17" cssclass="text"/></td>
    </tr>
    <tr>
      <td class="colDark">Tax:</td>
      <td class="colLight"><el:element type="text1" request="<%=request%>" name="lm_pi_tax_amount" size="17" cssclass="text"/></td>
    </tr>
    <tr>
      <td class="colDark">Total: <font color="#ff0000">*</font></td>
      <td class="colLight"><el:element type="text1" request="<%=request%>" name="lm_pi_total" size="17" cssclass="text"/>
        &nbsp;&nbsp;&nbsp;&nbsp;<b>PO Authorized Amount:</b>
        <script language="javascript">document.write(" $"+po_total);</script></td>
    </tr>
    <tr>
      <td class="colDark" valign="top">Comments:<br>(Partner comments from invoice.) </td>
      <td class="colLight"><textarea name="comments" rows="5" cols="85" class="textarea"></textarea></td>
    </tr>
    <tr>
      <td class="colDark"></td>
      <td class="colLight"><input class="button" type="submit" value="Submit"></td>
    </tr>
    <tr>
      <td colspan="2" class="secSeperator"></td>
    </tr>	
  </table>
</form>
</div>
</body>
</html>
