<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element1.tld" prefix="el"%>
<%@ page import = "java.util.Map" %>
<%
String lm_po_number = (String)request.getParameter("lm_po_number");
String success_message = "";

if (lm_po_number == null) {
	success_message = "An error occurred while searching for the selected PO. Try re-entering the PO<BR>Remember, a PO number is a numeric value only.";
} else  {
	success_message = "PO number "+lm_po_number+" was not found. Verify that the PO exists in Ilex.";
}
	
%>
<html>
<head>
<link href="css/content.css" type="text/css" rel="stylesheet"/>
<style>
.mainBody {
  margin : 0px 0px 0px 10px;
}
</style>
</head>
<body>
<div class="mainBody">
  <table width="780" border="0" cellpadding="0" cellspacing="1">
    <tr>
      <td colspan="4"><h1><i>eInvoice</i> Search Error</h1></td>
    </tr>
    <tr>
      <td height="5" colspan="4"></td>
    </tr>
    <tr>
      <td><%=success_message%></td>
    </tr>
  </table>
  </div>
</body>
</html>
