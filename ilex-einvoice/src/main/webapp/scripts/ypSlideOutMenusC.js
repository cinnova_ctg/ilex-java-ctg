/**************************************************************************
BROWSER
*/
// Browser Detection *********************************
var detect = navigator.userAgent.toLowerCase();
var OS,browser,version,total,thestring;

if (checkIt('konqueror'))
{
	browser = "Konqueror";
	OS = "Linux";
}
else if (checkIt('safari')) browser = "Safari"
else if (checkIt('omniweb')) browser = "OmniWeb"
else if (checkIt('opera')) browser = "Opera"
else if (checkIt('webtv')) browser = "WebTV";
else if (checkIt('icab')) browser = "iCab"
else if (checkIt('msie')) browser = "Internet Explorer"
else if (!checkIt('compatible'))
{
	browser = "Netscape Navigator"
	version = detect.charAt(8);
}
else browser = "An unknown browser";

if (!version) version = detect.charAt(place + thestring.length);

if (!OS)
{
	if (checkIt('linux')) OS = "Linux";
	else if (checkIt('x11')) OS = "Unix";
	else if (checkIt('mac')) OS = "Mac"
	else if (checkIt('win')) OS = "Windows"
	else OS = "an unknown operating system";
}

function checkIt(string)
{
	place = detect.indexOf(string) + 1;
	thestring = string;
	return place;
}
//alert(browser);
//***********************************************
/*
End BROWSER
**************************************************************************/
/*****************************************************
* ypSlideOutMenu
* Thanks to www.youngpup.net
* --youngpup--  1/22/2004
*****************************************************/

ypSlideOutMenu.Registry = []
ypSlideOutMenu.aniLen = 250
ypSlideOutMenu.hideDelay = 100
ypSlideOutMenu.minCPUResolution = 10

ypSlideOutMenu.styleMod = '';

function ypSlideOutMenu(id, dir, left, top, width, height, parentid, degrade, debug)
{	//alert("ypSlideOutMenu");
	width = 190;
	this.ie = document.all ? 1 : 0
	this.ns4 = document.layers ? 1 : 0
	this.dom = document.getElementById ? 1 : 0
	if (this.ie || this.ns4 || this.dom) {
		this.degrade = degrade;
		this.debug = debug;
		this.sliding = false;
		this.id = id
//alert(id);
		this.parentid = parentid
		this.dir = dir
		this.orientation = dir == "left" || dir == "right" ? "h" : "v"
		this.dirType = dir == "right" || dir == "down" ? "-" : "+"
		this.dim = this.orientation == "h" ? width : height
		this.hideTimer = false
		this.aniTimer = false
		this.open = false
		this.over = false
		this.startTime = 0
		this.gRef = "ypSlideOutMenu_"+id
		eval(this.gRef+"=this")
		ypSlideOutMenu.Registry[id] = this
		
		this.initleft = left
		this.inittop = top
		this.initwidth = width
		this.initheight = height
		this.showcount = 0;

// The following code originally invoked "document.write" but it caused
// a crash in IE (but not NS) when there were
// more than 31 menus total.  What crashed is that the styles that were
// declared in the css disappeared with 31 menus, where 32 menus caused
// the d.write call to fail entirely.  When I changed
// it so that the style changes were accumulated into one string
// (then at the end that string was written into the document,
// so that document.write was only called once) this solved the problem.

		ypSlideOutMenu.styleMod += '#' + this.id + 'Container { visibility:hidden; ';
		ypSlideOutMenu.styleMod += 'left:' + left + 'px; ';
		ypSlideOutMenu.styleMod += 'z-index:50; ';
		ypSlideOutMenu.styleMod += 'top:' + top + 'px; ';
		ypSlideOutMenu.styleMod += 'overflow:hidden; }';
		ypSlideOutMenu.styleMod += '#' + this.id + 'Container, #' + this.id + 'Content { position:absolute; ';
		ypSlideOutMenu.styleMod += 'height:' + height + 'px; ';
		ypSlideOutMenu.styleMod += 'width:' + width + 'px; ';
		ypSlideOutMenu.styleMod += 'z-index:50; ';
		ypSlideOutMenu.styleMod += 'clip:rect(0 ' + width + ' ' + height + ' 0); ';
		ypSlideOutMenu.styleMod += '}  ';
		this.load()
	}
}

ypSlideOutMenu.prototype.load = function() {	//alert("ypSlideOutMenu.prototype.load");
	var d = document
	var lyrId1 = this.id + "Container"
	var lyrId2 = this.id + "Content"
	var obj1 = this.dom ? d.getElementById(lyrId1) : this.ie ? d.all[lyrId1] : d.layers[lyrId1]
	if (obj1) var obj2 = this.ns4 ? obj1.layers[lyrId2] : this.ie ? d.all[lyrId2] : d.getElementById(lyrId2)
	if (!obj1 || !obj2) {
		window.setTimeout(this.gRef + ".load()", 1000);
	} else {
		this.container = obj1
		this.menu = obj2
		this.style = this.ns4 ? this.menu : this.menu.style
		// SEE NOTE ABOVE.  The following function call for the 31 menus bug.
		//this.setStyle()
		this.homePos = eval("0" + this.dirType + this.dim)
		this.outPos = 0
		this.accelConst = (this.outPos - this.homePos) / ypSlideOutMenu.aniLen / ypSlideOutMenu.aniLen 
		if (this.ns4) this.menu.captureEvents(Event.MOUSEOVER | Event.MOUSEOUT);
		this.menu.onmouseover = new Function("ypSlideOutMenu.showMenu('" + this.id + "')")
		this.menu.onmouseout = new Function("ypSlideOutMenu.hideMenu('" + this.id + "')")
		this.endSlide()
	}
}

//The following functions showMenu and hideMenu were altered on 4/2/2004 to disable the menu for Safari users
var notSafari;
notSafari = true;
if (navigator.userAgent.toLowerCase().indexOf("safari") > -1) {	//alert("notSafari");
	notSafari = false;
	}

ypSlideOutMenu.showMenu = function(id, e)
{	//alert(id);
	if (notSafari) {
		var reg = ypSlideOutMenu.Registry
		var obj = ypSlideOutMenu.Registry[id]
		//alert(obj.container);
		if (obj.container) {
			obj.over = true
			if (obj.hideTimer) { reg[id].hideTimer = window.clearTimeout(reg[id].hideTimer) }
			obj.showcount++;
			if (!obj.open && !obj.aniTimer) reg[id].startSlide(true)
		}
		if (obj.ns4) obj.menu.routeEvent(Event.MOUSEOVER);
	} //end if notSafari
}

ypSlideOutMenu.hideMenu = function(id, e)
{	//alert("ypSlideOutMenu.hideMenu");
	if (notSafari) {
		var obj = ypSlideOutMenu.Registry[id]
		if (obj.container) {
	//		if (obj.ie) alert('hiding: ' + id);
			if (obj.hideTimer) window.clearTimeout(obj.hideTimer)
			obj.showcount--;
			obj.hideTimer = window.setTimeout("ypSlideOutMenu.hide('" + id + "')", ypSlideOutMenu.hideDelay);
		}
		if (obj.ns4) obj.menu.routeEvent(Event.MOUSEOUT);
	} //end if notSafari
}

ypSlideOutMenu.hide = function(id) {	//alert("ypSlideOutMenu.hide");
	var obj = ypSlideOutMenu.Registry[id]
	var reg = ypSlideOutMenu.Registry
	obj.over = false
	if (obj.hideTimer) window.clearTimeout(obj.hideTimer)
	obj.hideTimer = 0
	var close = true;
	for (menu in reg) {
		// for each child, if either
		//  1. the child is open or
		//  2. the child is closing (but hasn't closed yet)
		// then we don't close this menu.
		var pid = ypSlideOutMenu.Registry[menu].parentid
		if (pid == id) {
			if (ypSlideOutMenu.Registry[menu].open) close = false;
			if (!ypSlideOutMenu.Registry[menu].open && ypSlideOutMenu.Registry[menu].sliding) close = false;
		}
	}
	if (obj.open && !obj.aniTimer && close && !obj.showcount) obj.startSlide(false);
}

ypSlideOutMenu.prototype.startSlide = function(open) {	//alert("ypSlideOutMenu.prototype.startSlide");
	this.open = open
	if (open) this.setVisibility(true)
	this.startTime = (new Date()).getTime() 
	this.sliding = true;
	this.aniTimer = window.setInterval(this.gRef + ".slide()", ypSlideOutMenu.minCPUResolution)
}

ypSlideOutMenu.prototype.slide = function() {	//alert("ypSlideOutMenu.prototype.slide");
	var elapsed = (new Date()).getTime() - this.startTime
	if (elapsed > ypSlideOutMenu.aniLen) this.endSlide()
	else {
		var d = Math.round(Math.pow(ypSlideOutMenu.aniLen-elapsed, 2) * this.accelConst)
		if (this.open && this.dirType == "-") d = -d
		else if (this.open && this.dirType == "+") d = -d
		else if (!this.open && this.dirType == "-") d = -this.dim + d
		else d = this.dim + d
		this.moveTo(d)
	}
}

ypSlideOutMenu.prototype.endSlide = function() {	//alert("ypSlideOutMenu.prototype.endSlide");
	this.aniTimer = window.clearTimeout(this.aniTimer)
	this.moveTo(this.open ? this.outPos : this.homePos)
	if (!this.open) this.setVisibility(false)
	this.sliding = false;
	if (((this.open && !this.over) || (!this.open && this.over)) && (!this.parent || this.parent.open)) {
		this.startSlide(this.over)
	} else {
		var overchild = false;
		var reg = ypSlideOutMenu.Registry
		for (menu in reg) {
			var pid = ypSlideOutMenu.Registry[menu].parentid
			if (pid == this.id) overchild = ypSlideOutMenu.Registry[menu].over ? true : overchild		
		}
		if (!overchild && this.parentid && !ypSlideOutMenu.Registry[this.parentid].over) ypSlideOutMenu.hide(this.parentid);
	}
}

ypSlideOutMenu.prototype.setVisibility = function(bShow) {		//alert("ypSlideOutMenu.prototype.setVisibility");
	var s = this.ns4 ? this.container : this.container.style
	s.visibility = bShow ? "visible" : "hidden"
}

ypSlideOutMenu.prototype.moveTo = function(p) { 	//alert("ypSlideOutMenu.prototype.moveTo");
	this.style[this.orientation == "h" ? "left" : "top"] = p
}

ypSlideOutMenu.prototype.getPos = function(c) {		//alert("ypSlideOutMenu.prototype.getPos");
	return parseInt(this.style[c])
}
/**************************************************************************
Global Navroll
*/

function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

function changeImages() {
	if (document.images && (preloadFlag == true)) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
      //alert("changeImages.arguments[" + i + "]].src = changeImages.arguments[" + (i+1) + "]");
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}
//declare some global variables
var preloadFlag = false;
function preloadImages() {
	if (document.images) {
		nav_aboutqc_over = new Image();
    	nav_aboutqc_over.src = "/common/media/header/nav-aboutqc-over.gif";
		nav_aboutqc_off = new Image();
    	nav_aboutqc_off.src = "/common/media/header/nav-aboutqc.gif";
		
		nav_tech_over = new Image();
   	 	nav_tech_over.src = "/common/media/header/nav-tech-over.gif";
		nav_tech_off = new Image();
    	nav_tech_off.src = "/common/media/header/nav-tech.gif";
		
		nav_prod_over = new Image();
    	nav_prod_over.src = "/common/media/header/nav-prod-over.gif";
		nav_prod_off = new Image();
    	nav_prod_off.src = "/common/media/header/nav-prod.gif";
		
		nav_press_over = new Image();
    	nav_press_over.src = "/common/media/header/nav-press-over.gif";
		nav_press_off = new Image();
    	nav_press_off.src = "/common/media/header/nav-press.gif";
		
		nav_investor_over = new Image();
    	nav_investor_over.src = "/common/media/header/nav-investor-over.gif";
		nav_investor_off = new Image();
    	nav_investor_off.src = "/common/media/header/nav-investor.gif";
		
		nav_careers_over = new Image();
    	nav_careers_over.src = "/common/media/header/nav-careers-over.gif";
		nav_careers_off = new Image();
    	nav_careers_off.src = "/common/media/header/nav-careers.gif";
		
		preloadFlag = true;
		//turn on the image for the section we're in
		turnOnImage();
	}
}

// Function to 'activate' images.
function NavImgOn(imgName) {
        if (document.images && (preloadFlag == true)) {
            document[imgName].src = eval(imgName + "_over.src");
        }
}

function NavImgOff(imgName) {
        if (document.images && (preloadFlag == true)) {
            document[imgName].src = eval(imgName + "_off.src");
        }
}

theBrowserVersion = parseInt(navigator.appVersion);
//get the Path of your current location
thePath = window.location.pathname
if (thePath.indexOf("/about/") != -1){
    directory = "aboutqc";
}
else if (thePath.indexOf("/technology/") != -1){
    directory = "tech";
}
else if (thePath.indexOf("/products/") != -1){
    directory = "prod";
}
else if (thePath.indexOf("/press/") != -1){
    directory = "press";
}
else if (thePath.indexOf("/ir/") != -1){
    directory = "investor";
}
else if (thePath.indexOf("/careers/") != -1){
    directory = "careers";
}
else if (thePath.indexOf("/enterprise/") != -1){
    directory = "aboutqc";
}
else if (thePath.indexOf("/mediaflo/") != -1){
    directory = "tech";
}
else if (thePath.indexOf("/community/") != -1){
    directory = "aboutqc";
}
else {
    directory = "home";
}

//set the down state of the current section the client is visiting    
function turnOnImage(){
    if (theBrowserVersion >= 4){ 
        switch(directory){
        case 'aboutqc':				   
						document.nav_aboutqc.src = "/common/media/header/nav-aboutqc-over.gif";
            nav_aboutqc_over.src = "/common/media/header/nav-aboutqc-over.gif";
            nav_aboutqc_off.src = "/common/media/header/nav-aboutqc-over.gif";
            break;
        case 'tech':
            document.nav_tech.src = "/common/media/header/nav-tech-over.gif";
            nav_tech_over.src = "/common/media/header/nav-tech-over.gif";
            nav_tech_off.src = "/common/media/header/nav-tech-over.gif";
            break;
				case 'prod':
            document.nav_prod.src = "/common/media/header/nav-prod-over.gif";
            nav_prod_over.src = "/common/media/header/nav-prod-over.gif";
            nav_prod_off.src = "/common/media/header/nav-prod-over.gif";
            break; 
        case 'press':
            document.nav_press.src = "/common/media/header/nav-press-over.gif";
            nav_press_over.src = "/common/media/header/nav-press-over.gif";
            nav_press_off.src = "/common/media/header/nav-press-over.gif";
            break; 
        case 'investor':
            document.nav_investor.src = "/common/media/header/nav-investor-over.gif";
            nav_investor_over.src = "/common/media/header/nav-investor-over.gif";
            nav_investor_off.src = "/common/media/header/nav-investor-over.gif";
            break; 
        case 'careers':
            document.nav_careers.src = "/common/media/header/nav-careers-over.gif";
            nav_careers_over.src = "/common/media/header/nav-careers-over.gif";
            nav_careers_off.src = "/common/media/header/nav-careers-over.gif";
            break;        
        }
        
    }
}

/*
End Global Navroll
**************************************************************************/
/**************************************************************************
Sniffer
*/
<!--//hidden

//////////////////////////////////////////////////////
// Browser DOM and DHTML detect
//////////////////////////////////////////////////////
var is_dom = 0;
var is_dhtml = 0;
if (document.getElementById) {
	// browser supports W3C DOM
	is_dom = 1;
}
if (document.getElementById || document.all || document.layers) {
	//browser can handle DHTML
	is_dhtml = 1;
}
///////////////////
// Browser Version
///////////////////
var agt=navigator.userAgent.toLowerCase();

var is_major = parseInt(navigator.appVersion);
var is_minor = parseFloat(navigator.appVersion);
var is_ns  = ((agt.indexOf('mozilla')!=-1) && (agt.indexOf('spoofer')==-1) && (agt.indexOf('compatible') == -1) && (agt.indexOf('opera')==-1) && (agt.indexOf('webtv')==-1));
var is_ie   = (agt.indexOf("msie") != -1);
////////////
// Platform
////////////
var is_win   = ((agt.indexOf("win")!=-1) || (agt.indexOf("16bit")!=-1) );
var is_win95 = ((agt.indexOf("win95")!=-1) || (agt.indexOf("windows 95")!=-1));
var is_win16 = ((agt.indexOf("win16")!=-1) || (agt.indexOf("16bit")!=-1) || (agt.indexOf("windows 3.1")!=-1) || (agt.indexOf("windows 16-bit")!=-1) );
var is_win31 = ((agt.indexOf("windows 3.1")!=-1) || (agt.indexOf("win16")!=-1) || (agt.indexOf("windows 16-bit")!=-1));
var is_win98 = ((agt.indexOf("win98")!=-1) || (agt.indexOf("windows 98")!=-1));
var is_winnt = ((agt.indexOf("winnt")!=-1) || (agt.indexOf("windows nt")!=-1));
var is_win32 = (is_win95 || is_winnt || is_win98 || ((is_major >= 4) && (navigator.platform == "Win32")) || (agt.indexOf("win32")!=-1) || (agt.indexOf("32bit")!=-1));
var is_mac= (agt.indexOf("mac")!=-1);
var is_ie45mac  = (is_mac && is_ie && (agt.indexOf("msie 5.0")==-1) && (agt.indexOf("msie 5.5")==-1) && (agt.indexOf("msie 4.5")!=-1));
var is_safari  = (is_mac && (agt.indexOf("safari")!=-1));
//////////////////////////////////////////
// Flash Detection
//////////////////////////////////////////
var is_flash7 = 0;
var is_flash6_65 = 0;
var is_flash6 = 0;
var is_flash5 = 0;
var is_flash4 = 0;

if (navigator.plugins["Shockwave Flash"]) {
	var plugin_major_version = 0;
	var plugin_minor_version = 0;
	var plugin_description = navigator.plugins["Shockwave Flash"].description.split(" ");
	for (var i = 0; i < plugin_description.length; ++i) {
		if (isNaN(parseInt(plugin_description[i])))
			continue;
			plugin_major_version = plugin_description[i];
	}
	for (var i = 0; i < plugin_description.length; ++i) {
		if (plugin_description[i].slice(0,1).toLowerCase() == "r")
			plugin_minor_version = parseInt(plugin_description[i].slice(1));
	}
}
if (plugin_major_version >= 7) {
	is_flash7 = 1;
}
if (plugin_major_version >= 6) {
	if(plugin_major_version >= 7) {
		is_flash6_65 = 1;
	} else if(plugin_minor_version >= 65) {
		is_flash6_65 = 1;
	}
}
if (plugin_major_version >= 6) {
	is_flash6 = 1;
}
if (plugin_major_version >= 5) {
	is_flash5 = 1;
}
if (plugin_major_version >= 4) {
	is_flash4 = 1;
}

// Check IE on windows for flash using VB Script
if (is_ie && is_win32) {
	document.write('<scr'+'ipt language="VBScript"\>\n');
	document.write('on error resume next\n');
	document.write('is_flash7 = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.7")))\n');
	document.write('on error resume next\n');
	document.write('is_flash6 = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.6")))\n');
	document.write('on error resume next\n');
	document.write('is_flash5 = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.5")))\n');
	document.write('on error resume next\n');
	document.write('is_flash4 = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.4")))\n');
	document.write('<'+'/scr'+'ipt> \n');
}
// -->
/*
End Sniffer
**************************************************************************/