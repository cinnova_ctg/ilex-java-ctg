function addRowToTable()
{
  var tbl = document.getElementById('tblSample');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow); 
  // left cell
  var cellLeft = row.insertCell(0);
  cellLeft.className = 'colLight';
  var textNode = document.createTextNode(iteration);
  cellLeft.appendChild(textNode);
  
  // right cell
  var catCell = row.insertCell(1);
  catCell.className = 'colLight';
  var el = document.createElement('input');
  el.type = 'text';
  el.name = 'txtRow' + iteration;
  el.id = 'txtRow' + iteration;
  el.size = '75';
  el.className = 'text';
  el.value = '';
  catCell.appendChild(el);
  
  // Quantity cell
  var qtySel = row.insertCell(2);
  qtySel.className = 'colLight';
  var sel = document.createElement('input');
  sel.type = 'text';
  sel.name = 'txtQty' + iteration;
  sel.id = 'txtQty' + iteration;
  sel.size = '4';
  sel.className = 'text';
  sel.value = '';
  qtySel.appendChild(sel);

  // Unit Price
  var upriceSel = row.insertCell(3);
  upriceSel.className = 'colLight';
  var upsel = document.createElement('input');
  upsel.type = 'text';
  upsel.name = 'txtuPrice' + iteration;
  upsel.id = 'txtuPrice' + iteration;
  upsel.size = '10';
  upsel.className = 'text';
  upsel.value = ''; 
  upriceSel.appendChild(upsel);

  // Total Price
  var tpriceSel = row.insertCell(4);
  tpriceSel.className = 'colLight';
  var tpsel = document.createElement('input');
  tpsel.type = 'text';
  tpsel.name = 'txttPrice' + iteration;
  tpsel.id = 'txttPrice' + iteration;
  tpsel.size = '10';
  tpsel.className = 'text';
  tpsel.value = '';
  //upsel.onfocus = getValues;
  upsel.onblur = getValues;
  tpriceSel.appendChild(tpsel);

 // category cell
  var catCellSel = row.insertCell(5);
  catCellSel.className = 'colLight';
  var sel4 = document.createElement('select');
  sel4.name = 'selRow' + iteration;
  sel4.className = 'select';
  sel4.options[0] = new Option('Select', 'value0');
  sel4.options[1] = new Option('Labor', 'L');
  sel4.options[2] = new Option('Travel', 'T');
  sel4.options[3] = new Option('Material', 'M');
  sel4.options[4] = new Option('Freight', 'F');
  sel4.options[5] = new Option('Various', 'V');
  catCellSel.appendChild(sel4);
  
  function getValues(){
 
	//var a = parseFloat(sel.value);
	//var b = parseFloat(upsel.value);
	//var c = tpsel.value;
	//c = parseFloat(a * b);
	//tpsel.value = '$' + c;
	 
	if (!IsNumeric(sel.value)) {
          sel.value = "";
        }

	if (!IsNumeric(upsel.value)) {
          upsel.value = "";
        }

	if (sel.value != "" && upsel.value!= "") {
 	      var a = parseFloat(sel.value);
	      var b = parseFloat(upsel.value);
          tpsel.value = parseFloat(a * b);
        } else {
          alert("Item quantity and/or unit cost was entered incorrectly.  Please correct");
        } 
  }
  
}

  function IsNumeric(strString)
   //  check for valid numeric strings	
   {
   var strValidChars = "0123456789.-";
   var strChar;
   var blnResult = true;

   if (strString.length == 0) return false;

   //  test strString consists of valid characters listed above
   for (i = 0; i < strString.length && blnResult == true; i++)
      {
      strChar = strString.charAt(i);
      if (strValidChars.indexOf(strChar) == -1)
         {
         blnResult = false;
         }
      }
   return blnResult;
   }
 
function generateRows(){	
	for (var x = 1; x <= 4; x++)
	   {
		addRowToTable();
	   }
}

function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	}
	else {
		window.onload = function() {
			if (oldonload) {
			    oldonload();
			}
			func();
		}
	}

}

addLoadEvent(generateRows);