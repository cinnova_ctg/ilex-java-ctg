function hideSpan(id, bttn, statusFalse, statusTrue){
bttn.src = (bttn.src.toString().indexOf(statusFalse)>-1)?statusTrue:statusFalse;
var sp = document.getElementById(id);
sp.checked = !sp.checked
}//end of function

<!--
var DED = window.DED || {};
DED.util = {};
DED.port = function() {
var libraries = {
'dom' : YAHOO.util.Dom,
'event' : YAHOO.util.Event,
'animation' : YAHOO.util.Anim,
'motion' : YAHOO.util.Motion,
'color' : YAHOO.util.ColorAnim,
'connection' : YAHOO.util.Connect,
'dragdrop' : YAHOO.util.DD
};
return {
get : function(lib) {
return libraries[lib];
}
};
}();
/*
Extending our Base YAHOO Dom
*/
YAHOO.util.Dom.cleanWhitespace = function(el) {
var el = YAHOO.util.Dom.get(el);
for (var i = 0; i < el.childNodes.length; i++) {
var node = el.childNodes[i];
if (node.nodeType === 3 && !/\S/.test(node.nodeValue)) {
node.parentNode.removeChild(node);
}
}
};

var $E = DED.port.get('event');
var $D = DED.port.get('dom');
var $A = DED.port.get('animation');
var $Color = DED.port.get('color');
var $M = DED.port.get('motion');
/*----------------------------------------------------------------------------------------*/
function toogle_view(){
try{
var bttn_show = document.getElementById('anc_show');
var bttn_close = document.getElementById('anc_close');
var yui_event = DED.port.get('event');
yui_event.addListener(bttn_show, "click", swoop, window.event, true);
yui_event.addListener(bttn_close, "click", swoop, window.event, true);
}
catch(ex){
//			alert(ex);
}
}//end of function
/*----------------------------------------------------------------------------------------*/
function setHeights(el) {
var target = el.nextSibling;
target.setAttribute('animheight', /*target.offsetHeight, */'opacity');
}
/*----------------------------------------------------------------------------------------*/	
function swoop (e){
try{		
var oDiv = document.getElementById("filters");
if(oDiv.style.visibility != "visible"){
oDiv.style.visibility = "visible";
}
$E.preventDefault(e);
var target = document.getElementById('filters');
var anc = document.getElementById('anc_show');
var anim, attr;
if($D.hasClass(anc, 'closed')) {
attr = {opacity: {to : 100}};
document.getElementById('filters').style.zIndex = 1000;
document.getElementById("anc_close").style.display = "inline";
$D.removeClass(anc, 'closed');
}
else{
attr = {opacity: {to : 0}};
document.getElementById('filters').style.zIndex = -1;
document.getElementById("anc_close").style.display = "none";
$D.addClass(anc, 'closed');
}
anim = new $A(target, attr, 0.2, YAHOO.util.Easing.backOut);
var start = function() {
try{
var el = this.getEl();
el.style.overflow = 'hidden';
}
catch(ex){
alert(ex);
}	
};
var finish = function() {
var el = this.getEl();
el.style.overflow = 'hidden';
};
//anim.onStart.subscribe(start);
//anim.onComplete.subscribe(finish);
anim.animate();	
}
catch(ex){
//			alert(ex);
}
}
/*----------------------------------------------------------------------------------------*/	
YAHOO.util.Event.on(window, 'load', toogle_view);
//-->
