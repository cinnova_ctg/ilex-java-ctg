// swap image function goes here


function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


	function trimAll( strValue ) {
	/************************************************
	DESCRIPTION: Removes leading and trailing spaces.
	
	PARAMETERS: Source string from which spaces will
	  be removed;
	
	RETURNS: Source string with whitespaces removed.
	*************************************************/ 
	 var objRegExp = /^(\s*)$/;
	
		//check for all spaces
		if(objRegExp.test(strValue)) {
		   strValue = strValue.replace(objRegExp, '');
		   if( strValue.length == 0)
			  return strValue;
		}
		
	   //check for leading & trailing spaces
	   objRegExp = /^(\s*)([\W\w]*)(\b\s*$)/;
	   if(objRegExp.test(strValue)) {
		   //remove leading and trailing whitespace characters
		   strValue = strValue.replace(objRegExp, '$2');
		}
	  return strValue;
	
	}

	function validateNotEmpty( form_field,strmsg ) {
	/************************************************
	DESCRIPTION: Validates that a string is not all
	  blank (whitespace) characters.
		
	PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	*************************************************/
	   var strTemp = form_field.value;
	   strTemp = trimAll(strTemp);
	   if(strTemp.length > 0){
		 return true;
	   }  
	   else
	   {
			alert(strmsg);		
			form_field.focus();
			return false;
		}	
	}

function validDateMDY(formField, message)
	{
		var result = true;
		if (!validateNotEmpty(formField, message))
			result = false;
		if (result)
		{
			var elems = formField.value.split('/');
			result = (elems.length == 3); // should be three components
			if (result)
			{
				var day = parseInt(elems[1]);
				var month = parseInt(elems[0]);
				var year = parseInt(elems[2]);
				//alert(month + "/" + day + "/" + year);
				result = !isNaN(month) && (month > 0) && (month < 13) && !isNaN(day) && (day > 0) && (day < 32) && !isNaN(year) && (elems[2].length == 4);
			}
			if (!result)
			{
				//alert(month + "/" + day + "/" + year);
				alert(message);
				formField.focus();
			}
		}
		return result;
	}
/*---------------------------------------------------*/
function $(id){return document.getElementById(id);}
/*---------------------------------------------------*/
