<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tld/Element1.tld" prefix="el"%>
<%@ page import = "java.util.Map" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
   
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="css/content.css" type="text/css" rel="stylesheet"/>
<style>
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 9px;
    	cursor : auto;
    	background-color: #E1EAE7;
    }
    .divBody01 {
		 padding : 10px 0px 0px 5px;
    }
    .divBody02 {
		 padding : 10px 0px 0px 20px;
    }
    .section01 {
		font-size : 8pt;
		font-weight : bold;
		padding : 1px 0px 1px 0px;
        vertical-align : top;
    }    
    .section02 {
		font-size : 8pt;
		font-weight : normal;
		padding : 1px 3px 1px 6px;
        text-align : right;
    }
    
    .section03 {
		font-size : 8pt;
		font-weight : normal;
		padding : 3px 0px 1px 10px;

    }    
    .section04 {
		font-size : 8pt;
		font-weight : normal;
		padding : 3px 0px 1px 30px;

    }    
    input.text
    {
	    border: 1px solid #b9cada;
	    font-size: 10px;
	    color: #000000;
    }
    
    select
    {
	    font-size: 10px;
	    border-style : solid;
	    border-width : 1px;
        border-color : #b9cada;
    }    
</style>
<script language="javascript" type="text/javascript">
	function validate_form2(){
	 
      if(document.forms.form2.lo_om_division.value == "" && 
         document.forms.form2.lo_ot_name.value == "" &&
         document.forms.form2.lx_pr_title.value == "" &&
         document.forms.form2.lm_js_title.value == "" &&
         document.forms.form2.lm_js_created_by.value == "" && document.forms.form2.lm_ap_pc_id.value == "")
        {
           alert("To perform an Invoice Search, you must enter at least one search parameter!");
           return false;
        }
	}
	
	function validate_form1(){
	 
      if(document.forms.form1.lm_po_id.value == "")
        {
           alert("Please enter a PO before searching!");
           return false;
        }
	}	
</script>
</head>
<body>
<div class="divBody01">
<table border="0" cellspacing="0" cellpadding="0">

<tr>
 <td class="section01">Find Invoice by Ilex PO</td>
</tr>

<form name="form1" action="Summary.xo" onSubmit="return validate_form1();" method="post" target="wa">
<tr>
 <td class="section02">PO:&nbsp;<el:element type="text1" request="<%=request%>" name="lm_po_id" size="8" cssclass="text"/>&nbsp;<input class="text" type="submit" value="Go"></td>
</tr>
   <input type="hidden" name="action" value="invoice_summary">
</form>

<tr>
 <td height="12"></td>
</tr>

<tr>
 <td class="section01">Invoice Reports</td>
</tr>

<tr>
 <td class="section03"><a href="List.xo?action=invoice_search&searchtype=rpt1&yyyy=All" target="wa">Pending Receipt</a></td>
</tr>	
<%
String status_Pending;
GregorianCalendar cal = new GregorianCalendar();
	//out.print(cal.get(Calendar.YEAR));
	int currentyr=cal.get(Calendar.YEAR);
for (int year=2007; year<=currentyr; year++) {
//	"<a href=\"List.xo?action=invoice_search&searchtype=rpt1&yyyy="+row+"\" target=\"wa\">"+row+"</a>";	
        String row = Integer.toString(year);
        status_Pending = "<a href=\"List.xo?action=invoice_search&searchtype=rpt1&yyyy="+row+"\" target=\"wa\">"+row+"</a>";
        		
    %>


<tr>
 <td class="section04"><%=status_Pending%></td>
</tr>

 <%	
    }
 %>
<!-- <tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt1&yyyy=2008" target="wa">2008</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt1&yyyy=2009" target="wa">2009</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt1&yyyy=2010" target="wa">2010</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt1&yyyy=2011" target="wa">2011</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt1&yyyy=2012" target="wa">2012</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt1&yyyy=2013" target="wa">2013</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt1&yyyy=2014" target="wa">2014</a></td>
</tr> -->
<tr>
 <td class="section03"><a href="List.xo?action=invoice_search&searchtype=rpt2&yyyy=All" target="wa">Received/Pending Approval</a></td>
</tr>

<!--  add dynamic  -->
 
<%
String status_Received;

	//out.print(cal.get(Calendar.YEAR));
	
for (int year=2007; year<=currentyr; year++) {
	//"<a href=\"List.xo?action=invoice_search&searchtype=rpt1&yyyy="+row+"\" target=\"wa\">"+row+"</a>"
        String row = Integer.toString(year);
        status_Received = "<a href=\"List.xo?action=invoice_search&searchtype=rpt2&yyyy="+row+"\" target=\"wa\">"+row+"</a>";
        		
    %> 
  

<tr>
 <td class="section04"><%=status_Received%></td>
</tr>

 <%	
    }
 %>

<!-- <tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt2&yyyy=2007" target="wa">2007</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt2&yyyy=2008" target="wa">2008</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt2&yyyy=2009" target="wa">2009</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt2&yyyy=2010" target="wa">2010</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt2&yyyy=2011" target="wa">2011</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt2&yyyy=2012" target="wa">2012</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt2&yyyy=2013" target="wa">2013</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt2&yyyy=2014" target="wa">2014</a></td>
</tr> -->
<tr>
 <td class="section03"><a href="List.xo?action=invoice_search&searchtype=rpt3&yyyy=All" target="wa">Approved/Pending Payment</a></td>
</tr>
<%
String status_Approved;

	//out.print(cal.get(Calendar.YEAR));
	
for (int year=2007; year<=currentyr; year++) {
	//<a href=\"List.xo?action=invoice_search&searchtype=rpt1&yyyy="+row+"\" target=\"wa\">"+row+"</a>  	
        String row = Integer.toString(year);
        status_Approved = "<a href=\"List.xo?action=invoice_search&searchtype=rpt3&yyyy="+row+"\" target=\"wa\">"+row+"</a>";
        		
    %> 
  

<tr>
 <td class="section04"><%=status_Approved%></td>
</tr>

 <%	
    }
 %>

<!-- <tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt3&yyyy=2007" target="wa">2007</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt3&yyyy=2008" target="wa">2008</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt3&yyyy=2009" target="wa">2009</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt3&yyyy=2010" target="wa">2010</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt3&yyyy=2011" target="wa">2011</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt3&yyyy=2012" target="wa">2012</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt3&yyyy=2013" target="wa">2013</a></td>
</tr>
<tr>
 <td class="section04"><a href="List.xo?action=invoice_search&searchtype=rpt3&yyyy=2014" target="wa">2014</a></td>
</tr>
<tr> -->
 <td height="12"></td>
</tr>

<tr>
 <td class="section01">Invoice Search</td>
</tr>

<form name="form2" action="List.xo?" onSubmit="return validate_form2();" method="get" target="wa">
<tr>
 <td>
  <table>
   <tr>
    <td class="section02">Partner:</td>
    <td class="section01"><el:element type="text1" request="<%=request%>" name="lo_om_division" size="12" cssclass="text"/></td>
   </tr>
  <tr>
    <td class="section02">Account:</td>
    <td class="section01"><el:element type="text1" request="<%=request%>" name="lo_ot_name" size="12" cssclass="text"/></td>
   </tr>   
   <tr>
    <td class="section02">Project:</td>
    <td class="section01"><el:element type="text1" request="<%=request%>" name="lx_pr_title" size="12" cssclass="text"/></td>
   </tr>
   <tr>
    <td class="section02">Job:</td>
    <td class="section01"><el:element type="text1" request="<%=request%>" name="lm_js_title" size="12" cssclass="text"/></td>
   </tr>
   <tr>
    <td class="section02">Owner:</td>
    <td class="section01"><el:element type="menu2" request="<%=request%>" name="lm_js_created_by" source="database" data="select cast(lo_pc_id as varchar(10))+'|'+lo_pc_last_name+', '+ left(lo_pc_first_name,1) from lo_organization_top join lo_organization_main on lo_om_ot_id = lo_ot_id and lo_om_ot_id = 5 join lo_poc on lo_om_id = lo_pc_om_id and (lo_pc_id in (9312,12277,12977) or lo_pc_role in (5,6,10)) where isnull(lo_pc_active_user, 1) = 1 or (isnull(lo_pc_active_user, 0) = 0 and lo_pc_termination_date > dateadd(dd, -160, getdate()))  or isnull(lo_pc_active_user, 3) = 3   order by lo_pc_last_name" cssclass="text"/></td>
   </tr>
   <tr>
    <td class="section02">PM:</td>
    <td class="section01"><el:element type="menu2" request="<%=request%>" name="lm_ap_pc_id" source="database" data="select cast(lo_pc_id as varchar(10))+'|'+lo_pc_last_name+', '+ left(lo_pc_first_name,1) from lo_poc where  lo_pc_om_id = 4 and (isnull(lo_pc_active_user, 1) = 1 or (lo_pc_termination_date > dateadd(dd, -120, getdate()))) and lo_pc_title in ('Sr. Project Manager', 'Project Manager') and lo_pc_id != 30054 order by lo_pc_last_name
   " cssclass="select"/></td>
   </tr>   
  <tr>
    <td>&nbsp;</td>
    <td class="section02"><input class="text" type="submit" value="Go"></td>
   </tr>             
  </table>
 </td>
</tr>
<input type="hidden" name="searchtype" value="search">
<input type="hidden" name="action" value="invoice_search">
</form>
</table>
</div>
</body>
</html>