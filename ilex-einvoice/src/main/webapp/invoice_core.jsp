<%@ page import = "java.util.Map" %>
<script language="JavaScript" src="scripts/jquery-1.3.2.min.js"></script>
<script language="javascript" type="text/javascript">
function saveApprovedTotal(purchaseOrder){
	var approvedTotal = document.getElementById('approvedTotal').value;
	var authorisedTotal = document.getElementById('authorisedTotal').textContent;
	authorisedTotal = authorisedTotal.replace("$","");
	authorisedTotal = authorisedTotal.replace(",","");
	if(parseFloat(approvedTotal) > parseFloat(authorisedTotal) ){
		alert("Approved total cannot be greater than Authorised total");
		return false;
	}
	var url="InternalAction.xo?action=update_approved_total&userid=25173&approvedTotal="+approvedTotal+"&purchaseOrder="+purchaseOrder;
	$.get(url,function(data, status){
		alert(data);
	});
}
</script>
<%
// Specific page level logic
Map is = (Map)request.getAttribute("invoice_summary");
Map coreData = (Map)request.getAttribute("coreData");
String po_status = (String)coreData.get("po_status");
String po_number = (String)coreData.get("lm_po_number");
String role = (String)request.getAttribute("role");
%>

<% 
if (is != null) {
%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td valign="top" width="50%">
<table border="0" cellpadding="0" cellspacing="1" width="100%">
<tr>
  <td colspan="2" class="formCaption"><b>Invoice Details</b></td>
</tr>
<tr>
  <td class="colDark">Received From:</td>
  <td class="colLight"><%=(String)is.get("lm_pi_submit_type")%></td>
</tr>
<tr>
  <td class="colDark">Invoice #:</td>
  <td class="colLight"><%=(String)is.get("lm_pi_invoice_number")%></td>
</tr>
<tr>
  <td class="colDark">Invoice Date:</td>
  <td class="colLight"><%=(String)is.get("lm_pi_invoice_date")%></td>
</tr>
<tr>
  <td class="colDark">Invoice Status:</td>
  <td class="colLight"><%=(String)is.get("lm_pi_status")%></td>
</tr>
<tr>
  <td class="colDark">Received Date:</td>
  <td class="colLight"><%=(String)is.get("lm_pi_date_received")%></td>
</tr>
<tr>
  <td class="colDark">Approval Date:</td>
  <td class="colLight"><%=(String)is.get("lm_pi_approved_date")%></td>
</tr>
<tr>
  <td class="colDark">ADPO:</td>
  <td class="colLight">TBP</td>
</tr>
<tr>
  <td class="colDark">Payment Date:</td>
  <td class="colLight"><%=(String)is.get("lm_pi_gp_paid_date")%></td>
</tr>
<tr>
  <td class="colDark">Check Number:</td>
  <td class="colLight"><%=(String)is.get("lm_pi_gp_check_number")%></td>
</tr>
<tr>
  <td class="colDark">Partner Comments:</td>
  <td class="colLight"><%=(String)is.get("lm_pi_partner_comments")%><br><br></td>
</tr>
<tr>
  <td class="colDark">Explanation to Partner:</td>
  <td class="colLight"><%=(String)is.get("lm_pi_reconcile_comments")%><br><br></td>
</tr>
</table></td>
<td valign="top" width="50%"><table border="0" cellpadding="0" cellspacing="1" width="100%">
<tr>
  <td colspan="2" class="formCaption"><b>Cost Breakdown</b></td>
</tr>

<tr>
  <td class="colDark">Labor:</td>
  <td style="padding-left: 10px"><table border="0" cellpadding="0" cellspacing="0" width="75"><tr><td class="colLight" style="text-align: right;"><%=(String)is.get("lm_pi_labor_amount")%></td></tr></table></td>
</tr>
<tr>
  <td class="colDark">Material:</td>
  <td style="padding-left: 10px"><table border="0" cellpadding="0" cellspacing="0" width="75"><tr><td class="colLight" style="text-align: right;"><%=(String)is.get("lm_pi_materials_amount")%></td></tr></table></td>
</tr>
<tr>
  <td class="colDark">Travel:</td>
  <td style="padding-left: 10px"><table border="0" cellpadding="0" cellspacing="0" width="75"><tr><td class="colLight" style="text-align: right;"><%=(String)is.get("lm_pi_travel_amount")%></td></tr></table></td>
</tr>
<tr>
  <td class="colDark">Frieght:</td>
  <td style="padding-left: 10px"><table border="0" cellpadding="0" cellspacing="0" width="75"><tr><td class="colLight" style="text-align: right;"><%=(String)is.get("lm_pi_frieght_amount")%></td></tr></table></td>
</tr>
<tr>
  <td class="colDark">Tax:</td>
  <td style="padding-left: 10px"><table border="0" cellpadding="0" cellspacing="0" width="75"><tr><td class="colLight" style="text-align: right;"><%=(String)is.get("lm_pi_tax_amount")%></td></tr></table></td>
</tr>
<tr>
  <td class="colDark">Invoice Total:</td>
  <td style="padding-left: 10px"><table border="0" cellpadding="0" cellspacing="0" width="75"><tr><td class="colLight" style="text-align: right; border-top: 1px solid #000000"><%=(String)is.get("lm_pi_total")%></td></tr></table></td>
</tr>
<tr>
  <td class="colDark">Authorized Total:</td>
  <td style="padding-left: 10px"><table border="0" cellpadding="0" cellspacing="0" width="75"><tr><td class="colLight" style="text-align: right;"><label for="authorisedTotal" id="authorisedTotal"><%=(String)is.get("lm_po_authorised_total")%></label></td></tr></table></td>
</tr>
<tr>
  <td class="colDark">Approved Total:</td>
  <%if("Y".equals(role) ){%>
  <td style="padding-left: 10px"><table border="0" cellpadding="0" cellspacing="0" width="75"><tr><td class="colLight" style="text-align: right;"><input type="text" size="8" value='<%=(String)coreData.get("lm_recon_approved_total")%>' id='approvedTotal' name='approvedTotal' /> <a href="javascript:saveApprovedTotal('<%=po_number%>')" >SAVE</a> </td></tr></table></td>
  <%
  }else{
  %>
  <td style="padding-left: 10px"><table border="0" cellpadding="0" cellspacing="0" width="75"><tr><td class="colLight" style="text-align: right;"><label for="authorisedTotal" id="authorisedTotal"><%=(String)coreData.get("lm_recon_approved_total")%></label></td></tr></table></td>
  <%} %> 
</tr>
<tr>
  <td colspan="2" class="formCaption"><b>Statistics</b></td>
</tr>
<tr>
  <td class="colDark">Auto Approved:</td>
  <td class="colLight"><%=(String)is.get("lm_pi_auto_approved")%></td>
</tr>
<tr>
  <td class="colDark">GP Synchronized:</td>
  <td class="colLight"><%=(String)is.get("lm_pi_gp_sync")%></td>
</tr>
<tr>
  <td class="colDark">Last Update:</td>
  <td class="colLight"><%=(String)is.get("lm_pi_last_update")%></td>
</tr>
<tr>
  <td class="colDark">Comcast Vendor ID:</td>
  <td class="colLight"><%=(String)is.get("comcast_vendor_id")%></td>
</tr>
<tr>
  <td class="colDark">Comcast Site Code:</td>
  <td class="colLight"><%=(String)is.get("site_code")%></td>
</tr>
<tr>
  <td class="colDark">GP Vendor ID:</td>
  <td class="colLight"><%=(String)is.get("gp_vendor_id")%></td>
</tr>
</table></td>
</tr>
</table>

<% 
} else if (po_status != null && !po_status.equals("Complete")) {
%>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
   <td class="formCaption">eInvoice data is not available at this time. It will be available once the PO is complete.</td>
</tr>
</table>

<% 
} else {
%>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
   <td class="formCaption">Error: eInvoice data could not be found.</td>
</tr>
</table>

<% 
} 
%>
