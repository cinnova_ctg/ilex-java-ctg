<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element1.tld" prefix="el"%>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.Vector" %>
<%
Vector rows;
Map searchResults = (Map)request.getAttribute("search_results");

String page_title = (String)request.getAttribute("report_title");
String search_type = (String)request.getAttribute("search_type");
String saleAgentFlagValue = (String)request.getAttribute("SalesAgentFlag");

String[] cssstyle = new String[5];
cssstyle[0] = "rowDark";
cssstyle[1] = "rowLight";
cssstyle[2] = "warningA";
cssstyle[3] = "warningB";
cssstyle[4] = "warningC";
int cssIndex = 1;

String update;
String status_action;

%>
<html>
<head>
<link href="css/content.css" type="text/css" rel="stylesheet"/>
<style>
.mainBody {
  margin : 0px 0px 0px 10px;
}

h1{
font-size: 16px;
font-weight: bold;
color: #2e3d80;
padding: 8px 0px 2px 0px;
margin: 0px 0px 0px 0px;
}

.subTitle{
font-size: 11px;
font-weight: bold;
padding: 2px 0px 6px 0px;
}

.tableHeader {
font-weight: bold;
font-size: 10px;
padding: 1px 4px 1px 4px;
text-align: center;
background: #E0E0DC;
}

</style>
<script type="text/javascript">
function populateListByAgents(isShowAgent){
	
	var url = document.URL;
	
	url = url.replace("SalesAgentFlag=0","");
	url = url.replace("SalesAgentFlag=1","");
	if(isShowAgent=="0"){
		url = url +"&SalesAgentFlag=0";
	}else{
		url = url +"&SalesAgentFlag=1";
	}
	<% if(search_type.equals("search")){ %>
		window.open(url,"_self");
	<%}else { %>
		window.open(url,"_self");
	<%  } %>
}
</script>
</head>
<body>
<div class="mainBody">
<table border="0" cellpadding="0" cellspacing="1"  width="90%">

<tr>
<td><h1><%=page_title%></h1></td>
</tr>
<tr><td class="subTitle">
Include Sales Agent:<input type="radio" name="SalesAgentFlag" id="SalesAgentFlag" value="1"  <% if(saleAgentFlagValue.equals("1")){ %>checked="checked" <% } %> title="Yes" onclick="populateListByAgents(this.value);" />Yes
<input type="radio" name="SalesAgentFlag" id="SalesAgentFlag" value="0" <% if(saleAgentFlagValue.equals("0")){ %>checked="checked" <% } %> title="No" onclick="populateListByAgents(this.value);" />No
</td></tr>
    
    <%
    if (searchResults.size() > 0) {
    %>	
   
    <%
    if (searchResults.get("pending") != null) {
    	
    	rows = (Vector)searchResults.get("pending");
    %>	
    <tr>
     <td class="subTitle"><span style="margin-right: 20px;">Pending Receipt</span>
     </td>
    </tr>
    <tr>
    <td>
     <div class="mainBody">
     <table border="0" cellpadding="0" cellspacing="1" width="100%">
  
      <tr>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invoice&nbsp;#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;PO&nbsp;#&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;Issued&nbsp;Date&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Partner&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       
       
       <td class="tableHeader">Authorized</td>
       <td class="tableHeader">Invoiced</td>
       <td class="tableHeader">Approved</td>
       
       <td class="tableHeader">Customer</td>
       <td class="tableHeader">Project</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Job&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">Complete</td>
       <td class="tableHeader">Start</td>
       <td class="tableHeader">End</td>
       
       <td class="tableHeader">&nbsp;&nbsp;PM&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;Owner&nbsp;&nbsp;</td>
     </tr> 
   
    
    <%
    for (int i=0; i<rows.size(); i++) {
    	
        String[] row = (String[])rows.get(i);
    	
        // Build URL for update and delete
        update = "<a href=\"\">Update</a>";
        status_action = "<a href=\"/eInvoice/Summary.xo?action=invoice_summary&lm_pi_id="+row[1]+"&lm_po_id="+row[3]+"\">Details</a>";
        		
    %>
     <tr>
      <td class="<%=cssstyle[i%2]%>">TBD&nbsp;[<%=status_action%>]</td>
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[2]%></td>
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[8]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[7]%></td>
      
      
      <td align="right" class="<%=cssstyle[i%2]%>"><%=row[12]%></td>
      <td align="right" class="<%=cssstyle[i%2]%>"><%=row[13]%></td>
      <td align="right" class="<%=cssstyle[i%2]%>"><%=row[14]%></td>
      
      <td class="<%=cssstyle[i%2]%>"><%=row[4]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[5]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[6]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[18]%></td>
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[17]%></td>
      
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[19]%></td>
      
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[20]%></td>
      
      <td class="<%=cssstyle[i%2]%>"><%=row[16]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[15]%></td>
    </tr> 
    
    <%	
    }
    %>
     </table>
     </div>
    </td>
   </tr>
    <%
    }
    %>
    
   
    <%
    if (searchResults.get("received") != null) {
    	
    	rows = (Vector)searchResults.get("received");
    %>	
    <tr>
     <td class="subTitle"><span style="margin-right: 20px;">Received</span>
     </td>
    </tr>
    <tr>
    <td>
     <div class="mainBody">
     <table border="0" cellpadding="0" cellspacing="1" width="100%">
  
      <tr>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invoice&nbsp;#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;Rec&nbsp;Date&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;PO&nbsp;#&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Partner&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      
       
       <td class="tableHeader">Authorized</td>
       <td class="tableHeader">Invoiced</td>
       <td class="tableHeader">Approved</td>
       
       <td class="tableHeader">Customer</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;Project&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Job&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">Complete</td>
       <td class="tableHeader">Start</td>
       <td class="tableHeader">End</td>
       
       <td class="tableHeader">PM</td>
       <td class="tableHeader">Owner&nbsp;</td>
     </tr> 
   
    
    <%
    // Status  = Received and Actio is Approve
    for (int i=0; i<rows.size(); i++) {
    	
        String[] row = (String[])rows.get(i);
    	
        // Build URL for update and delete
        update = "<a href=\"\">Details</a>";
        status_action = "<a href=\"/eInvoice/Summary.xo?action=invoice_summary&lm_pi_id="+row[1]+"&lm_po_id="+row[3]+"\">Details</a>";
        		
    %>
     <tr>
      <td class="<%=cssstyle[i%2]%>"><%=row[21]%>&nbsp;[<%=status_action%>]</td>
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[9]%></td>
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[2]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[7]%></td>
      
      
      <td align="right" class="<%=cssstyle[i%2]%>"><%=row[12]%></td>
      <td align="right" class="<%=cssstyle[i%2]%>"><%=row[13]%></td>
      <td align="right" class="<%=cssstyle[i%2]%>"><%=row[14]%></td>
      
      <td class="<%=cssstyle[i%2]%>"><%=row[4]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[5]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[6]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[18]%></td>
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[17]%></td>
      
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[19]%></td>
      
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[20]%></td>
      
      <td class="<%=cssstyle[i%2]%>"><%=row[16]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[15]%></td>
    </tr> 
    
    <%	
    }
    %>
     </table>
     </div>
    </td>
   </tr>
    <%
    }
    %>
        

    <%
    if (searchResults.get("approved") != null) {
    	
    	rows = (Vector)searchResults.get("approved");
    	
    %>	
    <tr>
     <td class="subTitle"><span style="margin-right: 20px;">Approved</span>
     </td>
    </tr>
    <tr>
    <td>
     <div class="mainBody">
     <table border="0" cellpadding="0" cellspacing="1" width="100%">
  
      <tr>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invoice&nbsp;#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">Approved&nbsp;Date</td>
       <td class="tableHeader">&nbsp;&nbsp;PO&nbsp;#&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Partner&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       
       
       <td class="tableHeader">Authorized</td>
       <td class="tableHeader">Invoiced</td>
       <td class="tableHeader">Approved</td>
       
       <td class="tableHeader">Customer</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;Project&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Job&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">Complete</td>
       <td class="tableHeader">Start</td>
       <td class="tableHeader">End</td>
       
       <td class="tableHeader">PM</td>
       <td class="tableHeader">Owner&nbsp;</td>
     </tr> 
 
    <%
    // Status  = Received and Actio is Approve
    for (int i=0; i<rows.size(); i++) {
    	
        String[] row = (String[])rows.get(i);
    	if (row[23].equals("Y")) {
    		cssIndex = 2;
    	} else if (row[23].equals("R")) {
    		cssIndex = 4;
    	} else {
    		cssIndex = 0;
    	}
    	//cssIndex = i%2;
    	
        // Build URL for update and delete
        update = "<a href=\"\">Update</a>";
        status_action = "<a href=\"/eInvoice/Summary.xo?action=invoice_summary&lm_pi_id="+row[1]+"&lm_po_id="+row[3]+"\">Details</a>";
        		
    %>
     <tr>
      <td class="<%=cssstyle[cssIndex]%>"><%=row[21]%>&nbsp;[<%=status_action%>]</td>
      <td align="center" class="<%=cssstyle[cssIndex]%>"><%=row[10]%></td>
      <td class="<%=cssstyle[cssIndex]%>"><%=row[2]%></td>
      <td class="<%=cssstyle[cssIndex]%>"><%=row[7]%></td>
      
      <td align="right" class="<%=cssstyle[cssIndex]%>"><%=row[12]%></td>
      <td align="right" class="<%=cssstyle[cssIndex]%>"><%=row[13]%></td>
      <td align="right" class="<%=cssstyle[cssIndex]%>"><%=row[14]%></td>
      
      <td class="<%=cssstyle[cssIndex]%>"><%=row[4]%></td>
      <td class="<%=cssstyle[cssIndex]%>"><%=row[5]%></td>
      <td class="<%=cssstyle[cssIndex]%>"><%=row[6]%></td>
      <td class="<%=cssstyle[cssIndex]%>"><%=row[18]%></td>

      <td align="center" class="<%=cssstyle[cssIndex]%>"><%=row[19]%></td>
      <td align="center" class="<%=cssstyle[cssIndex]%>"><%=row[20]%></td>
      <td align="center" class="<%=cssstyle[cssIndex]%>"><%=row[17]%></td>
      
      <td class="<%=cssstyle[cssIndex]%>"><%=row[16]%></td>
      <td class="<%=cssstyle[cssIndex]%>"><%=row[15]%></td>
    </tr> 
    
    <%	
    }
    %>
     </table>
     </div>
    </td>
   </tr>
    <%
    }
    %>    
    
    
     <%
    if (searchResults.get("paid") != null) {
    	
    	rows = (Vector)searchResults.get("paid");
    %>	
    <tr>
     <td class="subTitle">Paid</td>
    </tr>
    <tr>
    <td>
     <div class="mainBody">
     <table border="0" cellpadding="0" cellspacing="1" width="100%">
  
      <tr>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invoice&nbsp;#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">Paid&nbsp;Date</td>
       <td class="tableHeader">&nbsp;&nbsp;PO&nbsp;#&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Partner&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       
       
       <td class="tableHeader">Authorized</td>
       <td class="tableHeader">Invoiced</td>
       <td class="tableHeader">Approved</td>
       
       <td class="tableHeader">Customer</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;Project&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Job&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td class="tableHeader">Complete</td>
       <td class="tableHeader">Start</td>
       <td class="tableHeader">End</td>
       
       <td class="tableHeader">PM</td>
       <td class="tableHeader">Owner&nbsp;</td>
     </tr> 
   
    
     <%
     // Status  = Received and Actio is Approve
     for (int i=0; i<rows.size(); i++) {
    	
        String[] row = (String[])rows.get(i);
    	
        // Build URL for update and delete
        update = "<a href=\"\">Update</a>";
     %>
    
     <tr>
      <td class="<%=cssstyle[i%2]%>"><%=row[21]%></td>
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[11]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[2]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[7]%></td>
      
      <td align="right" class="<%=cssstyle[i%2]%>"><%=row[12]%></td>
      <td align="right" class="<%=cssstyle[i%2]%>"><%=row[13]%></td>
      <td align="right" class="<%=cssstyle[i%2]%>"><%=row[14]%></td>
      
      <td class="<%=cssstyle[i%2]%>"><%=row[4]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[5]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[6]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[18]%></td>
      
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[19]%></td>
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[20]%></td>
      <td align="center" class="<%=cssstyle[i%2]%>"><%=row[17]%></td>
      
      <td class="<%=cssstyle[i%2]%>"><%=row[16]%></td>
      <td class="<%=cssstyle[i%2]%>"><%=row[15]%></td>
    </tr> 
    
    <%	
    }
    %>
     </table>
     </div>
    </td>
   </tr>
    <%
    }
    %>
    
    
    <%	
    } else {
    %>
     
    <tr>
    <td class="subTitle"><span style="margin-right: 20px;">There are no invoice meeting the entered criteria.</span>
    </td>
    </tr>

    <%	
    }
    %> 
    
   </table>
   </div>
 </td>
</tr>

</table>
</div>
</body>
</html>
