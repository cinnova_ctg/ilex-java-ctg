<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element1.tld" prefix="el"%>
<%@ page import = "java.util.Map" %>
<%
String error_message = (String)request.getAttribute("error_message");
if (error_message == null) {
	error_message = "Unkown Error";
}
%>
<html>
<head>
<link href="css/content.css" type="text/css" rel="stylesheet"/>
<style>
.mainBody {
  margin : 0px 0px 0px 10px;
}
</style>
</head>
<body>
<div class="mainBody">
  <table width="780" border="0" cellpadding="0" cellspacing="1">
    <tr>
      <td colspan="4"><h1><i>EInvoice</i> Error Page</h1></td>
    </tr>
    <tr>
      <td height="5" colspan="4"></td>
    </tr>
    <tr>
      <td>An error occurred while processing your request.  Error Message: <%=error_message%></td>
    </tr>
  </table>
  </div>
</body>
</html>
