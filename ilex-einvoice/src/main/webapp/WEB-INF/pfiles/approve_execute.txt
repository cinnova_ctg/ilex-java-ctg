sp=lm_partner_invoice_approved_01(?, ?, ?, ?, ?)
fields=lm_pi_id,lm_pi_reconcile_comments,lm_pi_approved_total,lm_pi_last_updated_by,return_status

lm_pi_id_TYPE=int
lm_pi_id_IO=in
lm_pi_id_type_VAL=

lm_pi_reconcile_comments_TYPE=varchar
lm_pi_reconcile_comments_IO=in
lm_pi_reconcile_comments_VAL=

lm_pi_approved_total_TYPE=double
lm_pi_approved_total_IO=in
lm_pi_approved_total_VAL=

lm_pi_last_updated_by_TYPE=numeric
lm_pi_last_updated_by_IO=in
lm_pi_last_updated_by_VAL=

return_status_TYPE=int
return_status_IO=out
return_status_VAL=

success_page=invoice_approval