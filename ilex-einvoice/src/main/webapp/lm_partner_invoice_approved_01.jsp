<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element1.tld" prefix="el"%>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.Vector" %>
<% 
Map approve_display = (Map)request.getAttribute("approve_display");
Vector line_items = (Vector)request.getAttribute("line_items");
String lm_pi_id = (String)request.getAttribute("lm_pi_id");
String lm_po_id = (String)request.getAttribute("lm_po_id");
String invoice_total = (String)approve_display.get("lm_pi_total");
if (invoice_total != null) {
	invoice_total = "$"+invoice_total;
}
String authorized_total = (String)approve_display.get("lm_po_authorised_total");
if (authorized_total != null) {
	authorized_total = "$"+authorized_total;
}
String submit = "<input class=\"button\" type=\"submit\" value=\"Submit\">";
Map cd = (Map)request.getAttribute("coreData");
if (((String)cd.get("deliverables")).equals("B")) {
	submit = "<b>Customer Deliverables Required - This PO cannot be approved.</b>";
}
if (((String)cd.get("deliverables")).equals("D")) {
	submit = "<b>Internal Deliverables Required - This PO cannot be approved.</b>";
}
if (((String)cd.get("deliverables")).equals("E")) {
	submit = "<b>Pending Deliverables Required - This PO cannot be approved.</b>";
}
%>
<html>
<head>
<link href="css/content.css" type="text/css" rel="stylesheet"/>
<link href="css/cal.css" type="text/css" rel="stylesheet"/>
<script language="javascript" src="scripts/common.js">
</script>
<script language="javascript" src="scripts/CalendarPopup.js"></script>
<script language="JavaScript">
	var cal = new CalendarPopup("date_calendar");
	cal.setCssPrefix("TEST");
	var g_total = <%=(String)approve_display.get("lm_pi_total")%>;
	var po_total = <%=(String)approve_display.get("lm_po_authorised_total")%>;
/*------------------------------------------------------------------*/
function CheckInvoiceTotal(){
	var changeColor = $('grandtotal');		
	if(g_total == po_total) {
		changeColor.bgColor = '#E9F3FC';
	}
	else if (g_total < po_total){
		changeColor.bgColor = '#FFFFDE';
	}
	else if (g_total > po_total){
		changeColor.bgColor = '#F4C2C2';
	}
	else 
			return true;
}//end of funciton

</script>
<style>
.mainBody {
  margin : 0px 0px 0px 10px;
}
</style>
</head>
<body onLoad="CheckInvoiceTotal();">
<DIV id="date_calendar" style="Z-INDEX: 1000; VISIBILITY: hidden; POSITION: absolute; BACKGROUND-COLOR: white;"></DIV>
<div class="mainBody">
<form name="form1" action="ApproveExecute.xo" onSubmit="return validate_form(this);" method="post">

  <input type="hidden" name="lm_pi_id" value="<%=lm_pi_id%>">
  <input type="hidden" name="lm_po_id" value="<%=lm_po_id%>">
  <input type="hidden" name="action" value="approve_execute">
    
  <table width="780" border="0" cellpadding="0" cellspacing="1" class="remSpace">
    <tr>
      <td colspan="2"><h1>Partner Invoice Approval Form</h1></td>
    </tr>
    <tr>
      <td height="5" colspan="2"></td>
    </tr>
    <tr>
      <td colspan="2"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="remSpace">
          <tbody>
            
          <jsp:include page="/core_po_table_and_action.jsp" flush="true" />
            
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td colspan="2" class="secSeperator"></td>
    </tr>
    <tr>
      <td colspan="2" class="formCaption"><span style="float: left;">Line Items</td>
    </tr>
    
    <tr>
      <td colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="1" class="remSpace">
          <tr>
            <th width="1%">&nbsp;Item&nbsp;</th>
            <th width="34%">&nbsp;Description&nbsp;</th>
            <th width="6%">&nbsp;Quantity&nbsp;</th>
            <th width="6%">&nbsp;Unit&nbsp;Price&nbsp;</th>
            <th width="6%">&nbsp;Total&nbsp;Price&nbsp;</th>
            <th width="6%">Category</th>
          </tr>
          
          <% 
          if (line_items != null && line_items.size() > 0) {
        	  
        	  for (int i=0; i<line_items.size(); i++) {
        		  String[] row = (String[])line_items.get(i);
          %>
          
          <tr>
            <td class="colLight"><%=row[0]%></tD>
            <td class="colLight"><%=row[1]%></td>
            <td align="center" class="colLight"><%=row[2]%></td>
            <td align="right" class="colLight"><%=row[3]%></td>
            <td align="right" class="colLight"><%=row[4]%></td>
            <td class="colLight"><%=row[5]%></td>
          </tr>
          
          <%  
        	  }
          } else {
          %>
          <td align="center" colspan="6" class="colLight">&nbsp;The are no line items defined for this invoice.&nbsp;</tD>
          <%  
          }
          %>
        </table></td>
    </tr>
    <tr>
      <td colspan="2" class="secSeperator"></td>
    </tr>
    <tr>
      <td colspan="2" class="formCaption">Invoice Details</td>
    </tr>
    <tr>
      <td class="colDark">Invoice #:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_invoice_number")%> dated <%=(String)approve_display.get("lm_pi_invoice_date")%></td>
    </tr>
    <tr>
      <td class="colDark">Recieved Date:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_date_received")%></td>
    </tr>
    <tr>
      <td class="colDark">Labor:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_labor_amount")%></td>
    </tr>
    <tr>
      <td class="colDark">Travel:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_travel_amount")%></td>
    </tr>
    <tr>
      <td class="colDark">Materials:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_materials_amount")%></td>
    </tr>
    <tr>
      <td class="colDark">Freight:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_frieght_amount")%></td>
    </tr>
    <tr>
      <td class="colDark">Tax:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_tax_amount")%></td>
    </tr>
    <tr>
      <td class="colDark">Total:</td>
      <td id="grandtotal"><%=invoice_total%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>PO Authorized Amount:&nbsp;&nbsp;</b><%=authorized_total%></td>
    </tr>
    <tr>
      <td rowspan="2" class="colDark" valign="top">Invoice Disposition <font color="#ff0000">*</font></td>
      <td class="colLight"><input type="radio" name="payFull" id="rd_pay_full" value="pay_full" onClick="update_values(this);"/>
        <label for="rd_pay_full">Pay in full</label></td>
    </tr>
    <tr>
      <td class="colLight"><input type="radio" name="payFull" id="rd_adjust" value="adjust" onClick="update_values(this);" />
        <label for="rd_adjust">Adjusted Payment.  Enter amount and explanation below.</label></td>
    </tr>
    <tr>
      <td class="colDark">Payment Amount: <font color="#ff0000">*</font></td>
      <td class="colLight"><el:element type="text1" request="<%=request%>" name="lm_pi_approved_total" size="17" cssclass="text"/></td>
    </tr>
    <tr>
      <td class="colDark">Comments From Partners: </td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_partner_comments")%></td>
    </tr>
    
    <tr>
      <td class="colDark" valign="top">Explanation to Partner: </td>
      <td class="colLight"><textarea name="lm_pi_reconcile_comments" id="comments" rows="5" cols="55" class="textarea"></textarea></td>
    </tr>
    <tr>
      <td class="colDark"></td>
      <td class="colLight"><%=submit %></td>
    </tr>
    <tr>
      <td colspan="2" class="secSeperator"></td>
    </tr>
  </table>
  <script language="javascript" type="text/javascript">
/*---------------------------------------------------*/
function validate_form(frm){
    return;
	try{
	var rd_ad = $("rd_adjust");var rd_full = $("rd_pay_full");
	if(rd_ad.checked){
		var txt_payment = $("payment_amount");
		if(isNaN(txt_payment.value)){
				alert("Invalid Characters found");
				txt_payment.select();
				return false;
		}
		ret = validateNotEmpty(txt_payment, "Please Enter some Amount Here");
		if(!ret)
			return false;
			var t_comment = $("comments");
			ret = validateNotEmpty(t_comment, "Enter some Thing in Text Area");
			if(!ret)
				return false;
		}
		else if(rd_full.checked){
			var txt_payment = $("payment_amount");
			ret = validateNotEmpty(txt_payment, "Please Enter some Amount Here");
			if(!ret){txt_payment.focus(); return false;}
		}
	}
	catch(ex){return false;}
}//end of function
/*---------------------------------------------------*/
function update_values(rObj){
var txt_payment = $("payment_amount");
switch(rObj.value){
		case "pay_full":
			form1.lm_pi_approved_total.value = "<%=(String)approve_display.get("lm_pi_total")%>";
			break;
		case "adjust":
			form1.lm_pi_approved_total.value = "";
			break;
	}
}
</script>

</form>
</DIV>
</body>
</html>