<%@ page import = "java.util.Map" %>
<%
boolean show_data = false;

String deliverables = null;

Map coreData = (Map)request.getAttribute("coreData");

if (coreData != null) {
	
	deliverables = (String)coreData.get("deliverables");

	if (deliverables != null) {
    	if (deliverables.equals("B")) {
    	    deliverables = "<b>Customer Deliverables Required</b>";
    	} else if (deliverables.equals("A")) {
    	    deliverables = "Not Required";
    	} else if (deliverables.equals("D")) {
    	    deliverables = "<b>Internal Deliverables Required</b>";
    	} else if (deliverables.equals("E")) {
    	    deliverables = "<b>Pending Deliverables</b>";
    	} else if (deliverables.equals("F")) {
    	    deliverables = "No Outstanding Deliverables";
    	} else if (deliverables.equals("C")) {
    	    deliverables = "No Outstanding Deliverables(1)";
    	} else {
    	    deliverables = "No Deliverables Required";
    	}	
    	show_data = true;
    } else {
    	deliverables = "Not Defined";
     }
} 

if (show_data) {
%>

  <tr>
    <td valign="top" width="50%"><table border="0" cellpadding="0" cellspacing="1" width="100%">
          <tr>
            <td  colspan="2" class="formCaption"><b>Details for PO <%=(String)coreData.get("lm_po_number")%></b> (<%=(String)coreData.get("po_status")%>)</td>
          </tr>
          <tr>
            <td class="colDark">Partner:</td>
            <td class="colLight"><%=(String)coreData.get("lo_om_division_ptn")%></td>
          </tr>
          <tr>
            <td class="colDark">Contact Person:</td>
            <td class="colLight"><%=(String)coreData.get("ptn_name")%></td>
          </tr>
          <tr>
            <td class="colDark">Contact Phone:</td>
            <td class="colLight"><%=(String)coreData.get("ptn_phone")%></td>
          </tr>
          <tr>
            <td class="colDark">Contact Email:</td>
            <td class="colLight"><%=(String)coreData.get("ptn_email")%></td>
          </tr>
          <tr>
            <td class="colDark">Deliver By Date:</td>
            <td class="colLight"><%=(String)coreData.get("lm_po_deliver_by_date")%></td>
          </tr>
          <tr>
            <td class="colDark">PO Type: </td>
            <td class="colLight"><%=(String)coreData.get("lm_po_type")%></td>
          </tr>                    
          <tr>
            <td class="colDark">Master PO Item:</td>
            <td class="colLight"><%=(String)coreData.get("lm_pwo_type_desc")%></td>
          </tr>
          <tr>
            <td class="colDark">Terms: </td>
            <td class="colLight"><%=(String)coreData.get("lm_pwo_terms_master_data")%></td>
          </tr>
         <tr>
            <td class="colDark">Deliverables:</td>
            <td class="colLight"><%=deliverables%></td>
          </tr>          
          <tr>
            <td class="colDark">PO Notes<br><br></td>
            <td class="colLight"><%=(String)coreData.get("lm_pi_internal_comments")%></td>
          </tr>
      </table></td>
    <td valign="top" width="50%"><table border="0" cellpadding="0" cellspacing="1" width="100%">
          <tr>
            <td colspan="2" class="formCaption"><b>Job Details</b></td>
          </tr>
 
          <tr>
            <td class="colDark">Customer:</td>
            <td class="colLight"><%=(String)coreData.get("lo_ot_name")%></td>
          </tr>
          <tr>
            <td class="colDark">Project:</td>
            <td class="colLight"><%=(String)coreData.get("lx_pr_title")%></td>
          </tr>
          <tr>
            <td class="colDark">Job:</td>
            <td class="colLight"><%=(String)coreData.get("lm_js_title")%></td>
          </tr>
          
          <tr>
            <td class="colDark">Site:</td>
            <td class="colLight"><%=(String)coreData.get("lm_si_number")%></td>
          </tr>                    
          <tr>
            <td class="colDark">Status:</td>
            <td class="colLight"><%=(String)coreData.get("job_status")%></td>
          </tr>
          <tr>
            <td class="colDark">Complete Date:</td>
            <td class="colLight"><%=(String)coreData.get("job_complete_date")%></td>
          </tr>
          <tr>
            <td class="colDark">Sr.PM:</td>
            <td class="colLight"><%=(String)coreData.get("lo_pc_last_name_pm")%></td>
          </tr>
          <tr>
            <td class="colDark">Owner:</td>
            <td class="colLight"><%=(String)coreData.get("lo_pc_last_name_owner")%></td>
          </tr>
           
      </table></td>
  </tr>
<% 
} else { 
%>
  <tr>
    <td class="formCaption">The PO could not be found.  Check below for additional information if the eInvoice exists. Otherwise, verify the PO number is correct.</td>
  </tr>
<% 
}
%>
  
  