<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element1.tld" prefix="el"%>
<%@ page import = "java.util.Map" %>

<html>
<head>
<link href="css/content.css" type="text/css" rel="stylesheet"/>
<link href="css/cal.css" type="text/css" rel="stylesheet"/>

<style>
.mainBody {
  margin : 0px 0px 0px 5px;
}


.colHeader{
font-weight: normal;
font-size: 11px;
padding: 3px 0px 3px 5px;
text-align: center;
color: #545556;
background: #ececec;
}

.colDark{
height: 18px;
width: 170px;
font-size: 11px;
vertical-align : top;
padding: 3px 5px 0px 18px;
color: #333333;
background: #ececec;
}

.colLight{
height: 18px;
width: 600px;
white-space: normal;
font-size: 11px;
vertical-align : top;
padding: 3px 5px 0px 10px;
color: #333333;
background: #f9f9f9;
}

</style>

</head>
<body>
<DIV id="date_calendar" style="Z-INDEX: 1000; VISIBILITY: hidden; POSITION: absolute; BACKGROUND-COLOR: white; border: none;"></DIV>
<div class="mainBody">
<table width="780" border="0" cellpadding="0" cellspacing="1">
<tr>
<td colspan="2"><h1>Partner Invoice Details for PO # <%=(String)request.getAttribute("lm_pi_po_id")%></h1></td>
</tr>
<tr>
<td class="colDark">Invoice Number:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_invoice_number")%></td>
</tr>
<tr>
<td class="colDark">Invoice Status:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_status")%></td>
</tr>
<tr>
<td class="colDark">Invoice Date:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_invoice_date")%></td>
</tr>
<tr>
<td class="colDark">Invoice Date Received:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_date_received")%></td>
</tr>
<tr>
<td colspan="2" class="formCaption"><b>Line Items</b></td>
</tr>
<tr>
<td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="1"  id="tblSample">
    <tr>
    <td class="colHeader" width="2%">Item</td>
    <td class="colHeader" width="20%px" align="center">Description</td>
    <td class="colHeader" width="3%">Quantity</td>
    <td class="colHeader" width="4%">Unit Price ($)</td>
    <td class="colHeader" width="4%">Total Price ($)</td>
    <td class="colHeader" width="3%">Category</td>
    </tr>
 </table></td>
</tr>
<tr>
<td colspan="2" class="formCaption"><b>Cost Summary</b></td>
</tr>
<tr>
<td class="colDark">Labor:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_labor_amount")%></td>
</tr>
<tr>
<td class="colDark">Travel:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_travel_amount")%></td>
</tr>
<tr>
<td class="colDark">Materials:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_frieght_amount")%></td>
</tr>
<tr>
<td class="colDark">Freight:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_materials_amount")%></td>
</tr>
<tr>
<td class="colDark">Tax:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_tax_amount")%></td>
</tr>
<tr>
<td class="colDark">Invoiced Total:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_total")%></td>
</tr>
<tr>
<td class="colDark">Approved Total:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_approved_total")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Authorized Total:&nbsp;&nbsp;<%=(String)request.getAttribute("lm_po_authorised_total")%></td>
</tr>
<tr>
<td class="colDark">Explanation to Partner:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_reconcile_comments")%></td>
</tr>
<tr>
<td colspan="2" class="formCaption"><b>Invoice Disposition</b></td>
</tr>
<tr>
<td class="colDark">Payment Date:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_gp_paid_date")%></td>
</tr>
<tr>
<td class="colDark">Check Number:</td>
<td class="colLight"><%=(String)request.getAttribute("lm_pi_gp_check_number")%></td>
</tr>
</table>
</div>
</body>
</html>