<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element1.tld" prefix="el"%>
<%@ page import = "java.util.Map" %>
<%
// Specific page level logic
String next_action = (String)request.getAttribute("next_action");
String action_url = "";
String form_action = "";
String lm_po_id = "";
String lm_pi_id = "";
boolean display_form = false;
boolean po_deleted = false;
boolean display_form_action = false;

Map coreData = (Map)request.getAttribute("coreData");
String po_status = (String)coreData.get("po_status");


if (next_action != null) {
	
	lm_po_id = (String)request.getAttribute("lm_po_id");
	lm_pi_id = (String)request.getAttribute("lm_pi_id");
	display_form_action = true;
	
	if (next_action.equals("Receive")) {
		display_form = true;
		lm_po_id = (String)request.getAttribute("lm_po_id");
		lm_pi_id = (String)request.getAttribute("lm_pi_id");
		action_url = "InternalDisplay.xo";
		form_action = "internal_invoice_entry_display";
	} else if (next_action.equals("Approve")) {
		display_form = true;
		lm_po_id = (String)request.getAttribute("lm_po_id");
		lm_pi_id = (String)request.getAttribute("lm_pi_id");
		action_url = "ApproveDisplay.xo";
		form_action = "approve_display";
	} else if (next_action.equals("Pay")) {
		display_form = true;
		lm_po_id = (String)request.getAttribute("lm_po_id");
		lm_pi_id = (String)request.getAttribute("lm_pi_id");
		action_url = "PayDisplay.xo";
		form_action = "pay_display";
	} else if (!next_action.equals("Pending PO Completion")) {
		display_form = true;
		display_form_action = false;
	}
} 

if (po_status == null || !po_status.equals("Complete")) {
	display_form = false;
}

String error_message = (String)request.getAttribute("error_message");
boolean display_error_message = false;

if (error_message != null && (error_message.trim()).length() > 0) {
	display_error_message = true;
}

%>
<html>
<head>
<link href="css/content.css" type="text/css" rel="stylesheet"/>
<style>
.mainBody {
  margin : 0px 0px 0px 10px;
}
</style>
</head>
<body>
<div class="mainBody">
<table width="800" border="0" cellpadding="0" cellspacing="0">
<tr><td><h1>eInvoice Summary</h1></td></tr>

<%
if (display_error_message) {
%>
<tr><td class="errorMessage"><%=error_message%></td></tr>
<%
}
%>

<tr>
  <td><table width="100%" border="0" cellpadding="0" cellspacing="0"><jsp:include page="/core_po_table_and_action.jsp" flush="true" /></table></td>
</tr>

<tr><td height="5"></td></tr>

<tr>
  <td><jsp:include page="/invoice_core.jsp" flush="true" /></td>
</tr>

<tr><td height="5"></td></tr>

<%
if (display_form) {
%>

<tr>
  <td class="formCaption"><b>eInvoice Action</b></td>
</tr>

<tr><td height="1"></td></tr>

<%
if (!display_form_action) {
%>
<tr>
  <td><table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr>
  <td class="colDark">Next Action:</td>
  <td class="label1">&nbsp;&nbsp;<%=next_action%></td>
  </tr>   
  </table></td>
</tr>
<% 
} else {
%>
<form name="form1" action="<%=action_url%>" method="post">
<input type="hidden" name="action" value="<%=form_action%>">
<input type="hidden" name="lm_po_id" value="<%=lm_po_id%>">
<input type="hidden" name="lm_pi_id" value="<%=lm_pi_id%>">
<tr>
  <td><table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr>
  <td class="colDark">Next Action:</td>
  <td class="label1">&nbsp;&nbsp;&nbsp;&nbsp;<input class="button1" type="submit" value="&nbsp;&nbsp;&nbsp;&nbsp;<%=next_action%>&nbsp;&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>   
  </table></td>
</tr>
</form>
<% 
} 
%>

<% 
} 
%>

</table>
</div>
</body>
</html>