<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element1.tld" prefix="el"%>
<%@ page import = "java.util.Map" %>
<%
Map coreData = (Map)request.getAttribute("coreData");
String po_authotized_amount = (String)request.getAttribute("lm_po_authorised_total");
if (po_authotized_amount == null) {
	po_authotized_amount = "0.00";
}
%>
<html>
<head>
<link href="css/content.css" type="text/css" rel="stylesheet"/>
<style>
.mainBody {
  margin : 0px 0px 0px 10px;
}
</style>
</head>
<body>
<div class="mainBody">
  <table width="780" border="0" cellpadding="0" cellspacing="1">
    <tr>
      <td colspan="4"><h1>PO Search Results</h1></td>
    </tr>
    <tr>
      <td height="5" colspan="4"></td>
    </tr>
    <tr>
      <td colspan="4"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="remSpace">
          <tbody>
            <!-- Include core PO data here -->
            <tr>
              <td class="formCaption"><b>PO Details for <%=(String)coreData.get("lm_po_number")%></b></td>
              <td class="formCaption"><b>Job Details</b></td>
            </tr>
            <tr>
              <td valign="top" width="50%"><table border="0" cellpadding="0" cellspacing="1" width="100%">
                  <tbody>
                    <tr>
                      <td class="colDark">Partner:</td>
                      <td class="colLight"><%=(String)coreData.get("lo_om_division_ptn")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Contact Person:</td>
                      <td class="colLight"><%=(String)coreData.get("ptn_name")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Contact Phone:</td>
                      <td class="colLight"><%=(String)coreData.get("ptn_phone")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Contact Email:</td>
                      <td class="colLight"><%=(String)coreData.get("ptn_email")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Delivered:</td>
                      <td class="colLight"><%=(String)coreData.get("lm_po_deliver_by_date")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Type:</td>
                      <td class="colLight"><%=(String)coreData.get("lm_pwo_type_desc")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Terms: </td>
                      <td class="colLight"><%=(String)coreData.get("lm_pwo_terms_master_data")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">PO Notes</td>
                      <td class="colLight"><%=(String)coreData.get("lm_pi_reconcile_comments")%></td>
                    </tr>
                  </tbody>
                </table></td>
              <td valign="top" width="50%"><table border="0" cellpadding="0" cellspacing="1" width="100%">
                  <tbody>
                    <tr>
                      <td class="colDark">Account:</td>
                      <td class="colLight"><%=(String)coreData.get("lo_ot_name")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Project:</td>
                      <td class="colLight"><%=(String)coreData.get("lx_pr_title")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Job:</td>
                      <td class="colLight"><%=(String)coreData.get("lm_js_title")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Site:</td>
                      <td class="colLight"><%=(String)coreData.get("lm_si_number")%></td>
                    </tr>                    
                    <tr>
                      <td class="colDark">Status:</td>
                      <td class="colLight"><%=(String)coreData.get("job_status")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Date:</td>
                      <td class="colLight"><%=(String)coreData.get("job_complete_date")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">PM:</td>
                      <td class="colLight"><%=(String)coreData.get("lo_pc_last_name_pm")%></td>
                    </tr>
                    <tr>
                      <td class="colDark">Owner:</td>
                      <td class="colLight"><%=(String)coreData.get("lo_pc_last_name_owner")%></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr>
              <td colspan="2" class="formCaption"><b>Next Action</b>:  Update or Approve</td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </table>
  </div>
</body>
</html>
