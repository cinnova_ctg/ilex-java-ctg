<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element1.tld" prefix="el"%>
<%@ page import = "java.util.Map" %>
<%
// Local or mixed variables
boolean form_only = true;
String error_message = null;
String success_message = null;
boolean display_error_message = false;
boolean display_success_message = false;

// Parameters that are expected
Map approve_display = (Map)request.getAttribute("invoiceData");
String lm_pi_id = (String)request.getAttribute("lm_pi_id");
String lm_po_id = (String)request.getAttribute("lm_po_id");
String allowed_change_to_status = (String)request.getAttribute("allowed_change_to_status");

success_message = (String)request.getAttribute("success_message");
if (success_message != null && (success_message.trim()).length() > 0) {
	display_success_message = true;
}

error_message = (String)request.getAttribute("error_message");
if (error_message != null && (error_message.trim()).length() > 0) {
	display_error_message = true;
}

// If initial display - then just display basic form - trigger is a valid PO number
if (lm_po_id != null && lm_po_id.length()>0) {
	form_only = false;
}

if (!form_only && (allowed_change_to_status == null || allowed_change_to_status.equals(""))) {
	display_error_message = true;
	error_message = "An error occurred while retrieving the requested PO.  Try the request again.<BR>If this problem persists, contact the Ilex administrator.";
	form_only = true;
}

%>
<html>
<head>
<link href="css/content.css" type="text/css" rel="stylesheet"/>
<link href="css/cal.css" type="text/css" rel="stylesheet"/>
<script language="javascript" src="scripts/common.js"></script>

<style>
.mainBody {
  margin : 0px 0px 0px 10px;
}
</style>
</head>
<body onLoad="javascript:document.form1.lm_po_id.value=''">
<div class="mainBody">
<form name="form1" action="Status.xo" onSubmit="return validate_form(form1);" method="post">
<input type="hidden" name="action" value="change_status_display">

<table width="780" border="0" cellpadding="0" cellspacing="0">
<tr><td><h1>Change eInvoice Status</h1></td></tr>
<%
if (display_success_message) {
%>
<tr><td class="successMessage"><%=success_message%></td></tr>
<%
}
%>
<%
if (display_error_message) {
%>
<tr><td class="errorMessage"><%=error_message%></td></tr>
<%
}
%>
<tr><td height="5"></td></tr>

<tr>
   <td>
	<table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td class="label1">Enter PO Number:<font color="#ff0000">*</font></td>
    <td class="label1"><el:element type="text1" request="<%=request%>" name="lm_po_id" size="10" cssclass="text"/></td>
    <td class="label1"><input class="button1" type="submit" value="Submit"></td>
    </tr>
    </table>
   </td>
</tr>
</form>

<tr><td height="5"></td></tr>

<tr>
  <td class="secSeperator"></td>
</tr>

<% 
if (!form_only) {
%>

<tr>
   <td>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <jsp:include page="/core_po_table_and_action.jsp" flush="true" />
    </table>
   </td>
</tr>
												
<tr>
  <td class="secSeperator"></td>
</tr>

<tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="1">

   <tr>
    <td colspan="2" class="formCaption">Invoice Details</td>
   </tr>

   <tr>
    <td class="colDark">Invoice #: </td>
    <td class="colLight"><%=(String)approve_display.get("lm_pi_invoice_number") != null ? (String)approve_display.get("lm_pi_invoice_number") : ""%></td>
   </tr>
   
   <tr>
    <td class="colDark">Invoice Date: </td>
    <td class="colLight"><%=(String)approve_display.get("lm_pi_invoice_date") != null ? (String)approve_display.get("lm_pi_invoice_date") : ""%></td>
   </tr>
   <tr>
    <td class="colDark">Invoice Source: </td>
    <td class="colLight"><%=(String)approve_display.get("lm_pi_submit_type") != null ? (String)approve_display.get("lm_pi_submit_type") : ""%></td>
   </tr>   
   <tr>
    <td class="colDark">Received Date: </td>
    <td class="colLight"><%=(String)approve_display.get("lm_pi_date_received") != null ? (String)approve_display.get("lm_pi_date_received") : ""%></td>
   </tr>
   
   <tr>
      <td class="colDark">Labor:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_labor_amount") != null ? (String)approve_display.get("lm_pi_labor_amount") : ""%></td>
    </tr>
    <tr>
      <td class="colDark">Travel:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_travel_amount") != null ? (String)approve_display.get("lm_pi_travel_amount") : ""%></td>
    </tr>
    <tr>
      <td class="colDark">Materials:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_materials_amount") != null ? (String)approve_display.get("lm_pi_materials_amount") : ""%></td>
    </tr>
    <tr>
      <td class="colDark">Freight:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_frieght_amount") != null ? (String)approve_display.get("lm_pi_frieght_amount") : ""%></td>
    </tr>
    <tr>
      <td class="colDark">Tax:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_tax_amount") != null ? (String)approve_display.get("lm_pi_tax_amount") : ""%></td>
    </tr>
    
   <tr>
    <td class="colDark">Invoiced Amt: </td>
    <td class="colLight"><%=(String)approve_display.get("lm_pi_total") != null ? "$"+(String)approve_display.get("lm_pi_total") : ""%></td>
   </tr>   
   
   <tr>
    <td class="colDark">Authorized Amt:</td>
    <td class="colLight"><%=(String)approve_display.get("lm_po_authorised_total") != null ? "$"+(String)approve_display.get("lm_po_authorised_total") : ""%></td>
   </tr>   
 
  <tr>
    <td class="colDark">Approved Amt:</td>
    <td class="colLight"><%=(String)approve_display.get("lm_pi_approved_total") != null ? "$"+(String)approve_display.get("lm_pi_approved_total") : ""%></td>
  </tr>
  <tr>
    <td class="colDark" valign="top">Explanation to Partner: </td>
    <td class="colLight"><%=(String)approve_display.get("lm_pi_reconcile_comments") != null ? (String)approve_display.get("lm_pi_reconcile_comments") : ""%></td>
  </tr>

  </table>
  </td>
</tr>

<tr>
  <td colspan="2" class="secSeperator"></td>
</tr>
<tr>
  <td>
  <form name="form2" action="Status.xo" onSubmit="return validate_form2(form2);" method="post">
  <input type="hidden" name="action" value="change_status_action">
  <input type="hidden" name="lm_pi_id" value="<%=lm_pi_id%>">
  <input type="hidden" name="lm_po_id" value="<%=lm_po_id%>">
  <table width="100%" border="0" cellpadding="0" cellspacing="1">
  <tr>
    <td colspan="2" class="formCaption">Change Invoice Actions</td>
  </tr>
  
   <tr>
    <td class="colDark">From Invoice Status: </td>
    <td class="colLight"><%=(String)approve_display.get("lm_pi_status")%></td>
   </tr>
 
  <tr>
    <td class="colDark" valign="top">New Invoice Status: </td>
    <td class="colLight"><el:element type="menu2" request="<%=request%>" name="new_invoice_status" source="" data="<%=allowed_change_to_status%>" cssclass="select"/></td>
  </tr>

  <tr>
    <td class="colDark" valign="top">Notify Job Owner: </td>
    <td class="colLight"><input type="radio" name="notify_owner" value="Y"/> Yes</td>
  </tr>
  
  <tr>
    <td class="colDark" valign="top">Notify Partner: </td>
    <td class="colLight"><input type="radio" name="notify_partner" value="Y"/> Yes (Note: Only applies to Pending Receipt and Web Update changes)</td>
  </tr>   
    
  <tr>
    <td class="colDark"></td>
    <td class="colLight"><input class="button" type="submit" value="Submit"></td>
  </tr>
  
  </table>
  </form>
  </td>
</tr>

<%	
}
%>
</table>

</DIV>
</body>

<script language="javascript" type="text/javascript">

	function validate_form(frm){
		try{
			ret = validateNotEmpty(frm.lm_po_id, "Please enter the PO number associated with the einvoice to be changed");
			if(!ret)
				return false;			
		}
		catch(ex){
			alert(ex);
			return false;
		}
	}

	function validate_form2(frm){
		try{
			ret = validateNotEmpty(frm.new_invoice_status, "You must select a new status for this invoice!");
			if(!ret)
				return false;			
		}
		catch(ex){
			alert(ex);
			return false;
		}
	}	
</script>

</html>