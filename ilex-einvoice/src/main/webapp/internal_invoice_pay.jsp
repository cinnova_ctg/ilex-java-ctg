<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element1.tld" prefix="el"%>
<%@ page import = "java.util.Map" %>
<%
Map approve_display = (Map)request.getAttribute("approve_display");
String lm_pi_id = (String)request.getAttribute("lm_pi_id");
%>
<html>
<head>
<link href="css/content.css" type="text/css" rel="stylesheet"/>
<link href="css/cal.css" type="text/css" rel="stylesheet"/>

<script language="javascript" src="scripts/CalendarPopup.js"></script>
<script language="JavaScript">
	var cal = new CalendarPopup("date_calendar");
	cal.setCssPrefix("TEST");
</script>
<script language="javascript" src="scripts/common.js">
</script>
<script language="javascript" type="text/javascript">
    
	function validate_form(frm){
		try{
			ret = validateNotEmpty(frm.lm_pi_gp_check_number, "Please Enter Check Number.");
			if(!ret)
				return false;			
			ret = validateNotEmpty(frm.lm_pi_gp_paid_date, "Please Enter the Invoice Date");
			if(!ret)
				return false;
		}
		catch(ex){
			alert('Error: '+ex);
			return false;
		}
	}
</script>
<style>
.mainBody {
  margin : 0px 0px 0px 10px;
}
</style>
</head>
<body>
<DIV id="date_calendar" style="Z-INDEX: 1000; VISIBILITY: hidden; POSITION: absolute; BACKGROUND-COLOR: white; border: none;"></DIV>
<div class="mainBody">
<form name="form1" action="PayExecute.xo" onSubmit="return validate_form(form1);" method="post">

 <input type="hidden" name="lm_pi_id" value="<%=lm_pi_id%>">
 <input type="hidden" name="action" value="pay_execute">

<table width="780" border="0" cellpadding="0" cellspacing="0">

<tr><td><h1>Partner Payment Form</h1></td></tr>

<tr><td height="5"></td></tr>

<tr>
  <td class="secSeperator"></td>
</tr>
				
<tr>
   <td>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <jsp:include page="/core_po_table_and_action.jsp" flush="true" />
    </table>
   </td>
</tr>
												
<tr>
  <td class="secSeperator"></td>
</tr>

<tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="1">

   <tr>
    <td colspan="2" class="formCaption">Invoice Details</td>
   </tr>

   <tr>
    <td class="colDark">Invoice #: </td>
    <td class="colLight"><%=(String)approve_display.get("lm_pi_invoice_number")%></td>
   </tr>
   <tr>
    <td class="colDark">Invoice Date: </td>
    <td class="colLight"><%=(String)approve_display.get("lm_pi_invoice_date")%></td>
   </tr>
   <tr>
    <td class="colDark">Invoice Source: </td>
    <td class="colLight"><%=(String)approve_display.get("lm_pi_submit_type")%></td>
   </tr>   
   <tr>
    <td class="colDark">Received Date: </td>
    <td class="colLight"><%=(String)approve_display.get("lm_pi_date_received")%></td>
   </tr>
   
   <tr>
      <td class="colDark">Labor:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_labor_amount")%></td>
    </tr>
    <tr>
      <td class="colDark">Travel:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_travel_amount")%></td>
    </tr>
    <tr>
      <td class="colDark">Materials:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_materials_amount")%></td>
    </tr>
    <tr>
      <td class="colDark">Freight:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_frieght_amount")%></td>
    </tr>
    <tr>
      <td class="colDark">Tax:</td>
      <td class="colLight"><%=(String)approve_display.get("lm_pi_tax_amount")%></td>
    </tr>
    
   <tr>
    <td class="colDark">Invoiced Amt: </td>
    <td class="colLight">$<%=(String)approve_display.get("lm_pi_total")%></td>
   </tr>   
   
   <tr>
    <td class="colDark">Authorized Amt:</td>
    <td class="colLight">$<%=(String)approve_display.get("lm_po_authorised_total")%></td>
   </tr>   
  </table>
  </td>
</tr>

<tr>
  <td colspan="2" class="secSeperator"></td>
</tr>
<tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="1">
  <tr>
    <td colspan="2" class="formCaption">Invoice Disposition</td>
  </tr>
  <tr>
    <td class="colDark">Approved Amt:</td>
    <td class="colLight">$<%=(String)approve_display.get("lm_pi_approved_total")%></td>
  </tr>
  <tr>
    <td class="colDark" valign="top">Explanation to Partner: </td>
    <td class="colLight"><%=(String)approve_display.get("lm_pi_reconcile_comments")%></td>
  </tr>
  <tr>
    <td class="colDark">Check Number: <font color="#ff0000">*</font></td>
    <td class="colLight"><el:element type="text1" request="<%=request%>" name="lm_pi_gp_check_number" size="10" cssclass="text"/></td>
  </tr>
  
  <tr>
    <td class="colDark">Date: <font color="#ff0000">*</font></td>
    <td class="colLight"><el:element type="date1" request="<%=request%>" name="lm_pi_gp_paid_date" size="10" data="Today" cssclass="text"/>
        &nbsp;&nbsp;&nbsp;<a id="anchor19" onclick="cal.select(document.form1.lm_pi_gp_paid_date,'anchor19','MM/dd/yyyy'); return false;" href="#" name="anchor19"><img align="absmiddle" border="0" src="images/calenderImage.gif"></a></td>
  </tr>
   
  <tr>
    <td class="colDark"></td>
    <td class="colLight"><input class="button" type="submit" value="Submit"></td>
  </tr>
  </table>
  </td>
</tr>

</table>
</form>
</DIV>
</body>
</html>