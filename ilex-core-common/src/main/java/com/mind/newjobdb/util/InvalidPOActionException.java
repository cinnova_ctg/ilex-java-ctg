package com.mind.newjobdb.util;

public class InvalidPOActionException extends Exception {

	@Override
	public String getMessage() {
		return "InvalidPOActionException: " + super.getMessage();
	}

}
