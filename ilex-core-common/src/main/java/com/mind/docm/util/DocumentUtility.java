package com.mind.docm.util;

import java.util.ArrayList;

import com.mind.bean.docm.Field;

public class DocumentUtility {
	// Use Case
	/**
	 * @name fieldArray
	 * @purpose This method is used an arrayList in FieldArray object.
	 * @steps 1. create a loop for all the object in arrayList. 2. convert each
	 *        arraylist object to field object.
	 * @param fieldArrayList
	 * @returnDescription Field[]
	 * @throws None
	 */
	public static Field[] fieldArray(ArrayList fieldArrayList) {
		Object[] obj = fieldArrayList.toArray();
		Field[] field = new Field[obj.length];
		for (int i = 0; i < obj.length; i++) {
			field[i] = (Field) obj[i];
		}
		return field;
	}

}
