//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : VersionSchema.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.docm.util;

import org.apache.log4j.Logger;


/**
 * The Class VersionSchema.
 */
public class VersionSchema {
	private static final Logger logger = Logger.getLogger(VersionSchema.class);

	/** The CHANG e_ typ e_ onl y_ file. */
	public static String CHANGE_TYPE_ONLY_FILE = "FileChange";

	/** The CHANG e_ typ e_ onl y_ metadata. */
	public static String CHANGE_TYPE_ONLY_METADATA = "MetadataChange";

	/** The CHANG e_ typ e_ fil e_ an d_ metadata. */
	public static String CHANGE_TYPE_FILE_AND_METADATA = "FileAndMetadataChange";

	/** The file version schema. */
	private FileVersionSchema fileVersionSchema;

	/** The meta data version schema. */
	private MetaDataVersionSchema metaDataVersionSchema;

	/** The new version. */
	public String newVersion = "";

	/** The increment. */
	private static float increment = 1.0f;

	/** The mdata_increment. */
	private static float mdata_increment = 0.1f;

	/**
	 * Gets the new version.
	 * 
	 * @param changeType
	 *            the change type
	 * @param currentVersion
	 *            the current version
	 * 
	 * @return the new version
	 */
	public String getNewVersion(String changeType, String currentVersion) {
		if (changeType.equals(CHANGE_TYPE_ONLY_FILE)) {
			newVersion = fileVersionSchema.getNewVersion(currentVersion);
		} else if (changeType.equals(CHANGE_TYPE_ONLY_METADATA)) {
			newVersion = metaDataVersionSchema.getNewVersion(currentVersion);
		} else if (changeType.equals(CHANGE_TYPE_FILE_AND_METADATA)) {
			String fileNewVersion = fileVersionSchema
					.getNewVersion(currentVersion);
			String metaDataNewVersion = metaDataVersionSchema
					.getNewVersion(currentVersion);
			newVersion = Math.max(Float.valueOf(fileNewVersion).floatValue(),
					Float.valueOf(metaDataNewVersion).floatValue()) + "";
		}
		return newVersion;
	}

	// Use Case
	/**
	 * @name getNewFileVersion
	 * @purpose This method is get the file version for a document.
	 * @steps 1. get the current version and make a increment of 1.0
	 * @param currentVersion
	 * @returnDescription Returns the new file version for document.
	 */
	public String getNewFileVersion(String currentVersion) {
		String newVersion = "";
		logger.trace("currentVersion" + currentVersion);
		if (currentVersion == null || currentVersion.equals("")) {
			currentVersion = "0";
		}
		newVersion = Float.toString((((Float.valueOf(currentVersion)
				.floatValue())) + increment));
		return newVersion;
	}

	// Use Case
	/**
	 * @name getNewMdataVersion
	 * @purpose This method is get the document version for a document.
	 * @steps 1. get the current version and make a increment of 0.1
	 * @param currentVersion
	 * @returnDescription Returns the new version for document.
	 */
	public String getNewMdataVersion(String currentVersion) {
		String split = "";
		int minorVer = 0;
		String major = "";
		logger.trace("currentVersion " + currentVersion);
		if (!currentVersion.equals("")) {
			int idx = currentVersion.lastIndexOf(".") + 1;
			split = currentVersion.substring(idx, currentVersion.length());
			logger.trace("split:: " + split);
			minorVer = Integer.valueOf(split).intValue();
			logger.trace("minorVer:: " + minorVer);
			major = currentVersion.substring(0,
					(currentVersion.lastIndexOf(".")));
			logger.trace("major:: " + major);
		}
		int subStr = minorVer + 1;
		String newVersion = major + "." + Integer.toString(subStr);

		logger.trace("newVersion :: " + newVersion);
		return newVersion;
	}

	/**
	 * Gets the file version schema.
	 * 
	 * @return the file version schema
	 */
	public FileVersionSchema getFileVersionSchema() {
		return fileVersionSchema;
	}

	/**
	 * Sets the file version schema.
	 * 
	 * @param fileVersionSchema
	 *            the new file version schema
	 */
	public void setFileVersionSchema(FileVersionSchema fileVersionSchema) {
		this.fileVersionSchema = fileVersionSchema;
	}

	/**
	 * Gets the meta data version schema.
	 * 
	 * @return the meta data version schema
	 */
	public MetaDataVersionSchema getMetaDataVersionSchema() {
		return metaDataVersionSchema;
	}

	/**
	 * Sets the meta data version schema.
	 * 
	 * @param metaDataVersionSchema
	 *            the new meta data version schema
	 */
	public void setMetaDataVersionSchema(
			MetaDataVersionSchema metaDataVersionSchema) {
		this.metaDataVersionSchema = metaDataVersionSchema;
	}

	/**
	 * The main method.
	 * 
	 * @param s
	 *            the arguments
	 */
	public static void main(String s[]) {
		VersionSchema v = new VersionSchema();
		logger.trace(v.getNewMdataVersion("0.1"));
	}
}
