//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : FileVersionSchema.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.docm.util;


/**
 * The Class FileVersionSchema.
 */
public class FileVersionSchema extends VersionSchema {

	/** The increment. */
	private float increment = 1.0f;

	/**
	 * Gets the new version.
	 * 
	 * @param currentVersion
	 *            the current version
	 * 
	 * @return the new version
	 */
	public String getNewVersion(String currentVersion) {
		String newVersion = (Math.round((Float.valueOf(currentVersion)
				.floatValue())) + increment)
				+ "";
		return newVersion;
	}
}
