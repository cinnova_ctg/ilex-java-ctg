//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : MetaDataVersionSchema.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.docm.util;


/**
 * The Class MetaDataVersionSchema.
 */
public class MetaDataVersionSchema extends VersionSchema {

	/**
	 * Gets the new version.
	 * 
	 * @param currentVersion
	 *            the current version
	 * 
	 * @return the new version
	 */
	public String getNewVersion(String currentVersion) {
		int idx = currentVersion.lastIndexOf(".") - 1;
		String split = currentVersion.substring(idx);
		Float subStr = Float.valueOf(split).floatValue() + 0.1f;
		String newVersion = Float.valueOf(currentVersion).floatValue() + subStr
				+ "";
		return newVersion;
	}
}
