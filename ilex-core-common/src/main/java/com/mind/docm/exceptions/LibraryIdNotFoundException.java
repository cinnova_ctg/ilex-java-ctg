//
//
//
//@ Project : DocM - Document Management System - Ilex
//@ File Name : LibraryIdNotFoundException.java
//@ Date : 12-June-2008
//@ Author : Vishal Kapoor
//
//
package com.mind.docm.exceptions;


public class LibraryIdNotFoundException extends Exception{
	/**
	 * @name LibraryIdNotFoundException
	 * @purpose This method is used to show the message when the Library correspponding to the entity not exists.  
	 * @steps	1. User requests to get the libId from the database and it does not exists so this exception is thrown.
	 * @param String
	 * @return  
	 * @returnDescription       
	 */
	public LibraryIdNotFoundException(String expMessage){
		super(expMessage);
	}
}


