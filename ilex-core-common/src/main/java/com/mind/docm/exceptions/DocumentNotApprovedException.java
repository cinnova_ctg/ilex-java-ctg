package com.mind.docm.exceptions;

/**
 * The Class DocumentNotApprovedException.
 */
public class DocumentNotApprovedException extends Exception {

	/** The err. */
	String err = "";

	/**
	 * Instantiates a new document not approved exception.
	 * 
	 * @param errmsg
	 *            the errmsg
	 */
	public DocumentNotApprovedException(String errmsg) {
		err = errmsg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	public String toString() {
		return "Document Exception[" + err + "]";
	}
}
