package com.mind.docm.exceptions;

/**
 * The Class CouldNotPhysicallyDeleteException.
 */
public class CouldNotPhysicallyDeleteException extends Exception {

	/** The err. */
	String err = "";

	/**
	 * Instantiates a new could not physically delete exception.
	 * 
	 * @param errmsg
	 *            the errmsg
	 */
	public CouldNotPhysicallyDeleteException(String errmsg) {
		err = errmsg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	public String toString() {
		return "Document Exception[" + err + "]";
	}
}
