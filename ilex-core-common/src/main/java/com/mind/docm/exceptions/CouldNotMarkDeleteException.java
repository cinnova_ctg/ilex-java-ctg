package com.mind.docm.exceptions;

/**
 * The Class CouldNotMarkDeleteException.
 */
public class CouldNotMarkDeleteException extends Exception {

	/** The err. */
	String err = "";

	/**
	 * Instantiates a new could not mark delete exception.
	 * 
	 * @param errmsg
	 *            the errmsg
	 */
	public CouldNotMarkDeleteException(String errmsg) {
		err = errmsg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	public String toString() {
		return "Document Not Mark As Delete Exception[" + err + "]";
	}
}
