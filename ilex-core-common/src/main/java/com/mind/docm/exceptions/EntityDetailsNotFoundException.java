//
//
//
//@ Project : DocM - Document Management System - Ilex
//@ File Name : EntityDetailsNotFoundException.java
//@ Date : 12-June-2008
//@ Author : Vishal Kapoor
//
//
package com.mind.docm.exceptions;

public class EntityDetailsNotFoundException extends Exception{
	/**
	 * @name EntityDetailsNotFoundException
	 * @purpose This method is used to show the message when entity details are not found.  
	 * @steps	1. User requests to get the entitydetails from the database and it does not exists so this exception is thrown.
	 * @param String
	 * @return  
	 * @returnDescription       
	 */

	public EntityDetailsNotFoundException(String expMessage){
		super(expMessage);
	}
}
