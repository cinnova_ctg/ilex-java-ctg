package com.mind.docm.exceptions;

/**
 * The Class ControlledTypeException.
 */
public class ControlledTypeException extends Exception {

	/** The err. */
	String err = "";

	/**
	 * Instantiates a new controlled type exception.
	 * 
	 * @param errmsg
	 *            the errmsg
	 */
	public ControlledTypeException(String errmsg) {
		err = errmsg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	public String toString() {
		return "Control Type Exception[" + err + "]";
	}
}
