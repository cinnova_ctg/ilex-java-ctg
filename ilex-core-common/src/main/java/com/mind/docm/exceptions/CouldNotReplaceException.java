package com.mind.docm.exceptions;

/**
 * The Class CouldNotReplaceException.
 */
public class CouldNotReplaceException extends Exception {

	/** The err. */
	String err = "";

	/**
	 * Instantiates a new could not replace exception.
	 * 
	 * @param errmsg
	 *            the errmsg
	 */
	public CouldNotReplaceException(String errmsg) {
		err = errmsg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	public String toString() {
		return "Document Exception[" + err + "]";
	}
}
