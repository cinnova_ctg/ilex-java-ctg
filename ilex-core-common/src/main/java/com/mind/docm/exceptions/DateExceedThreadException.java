/*
 * Copyright (C) 2008 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: <File contains the logic for showing message when certain type of exception is thrown.>
 *
 */

package com.mind.docm.exceptions;

import org.apache.log4j.Logger;

/**
 * Class DateExceedThreadException - Class contains the logic for showing
 * message when certain type of exception is thrown. methods -
 * contextInitialized, contextDestroyed
 */
public class DateExceedThreadException extends Exception {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DateExceedThreadException.class);

	private String date = null;

	public DateExceedThreadException(String date) {
		this.date = date;
	}

	/**
	 * Method showMessage - This method shows the message for the perticular
	 * type of exception.
	 * 
	 * @return Return value None
	 */

	public void showMessage() {
		logger.trace("Start :(Class : com.mind.docm.exceptions."
				+ "DateExceedThreadException | Method : showMessage)");
		if (logger.isDebugEnabled()) {
			logger.debug("showMessage() - " + this.date + " is not possible.");
		}

		logger.trace("End :(Class : com.mind.docm.exceptions."
				+ "DateExceedThreadException | Method : showMessage)");
	}

}
