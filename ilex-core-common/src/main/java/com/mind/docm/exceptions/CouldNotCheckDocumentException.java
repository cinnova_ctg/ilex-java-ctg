package com.mind.docm.exceptions;

/**
 * The Class CouldNotCheckDocumentException.
 */
public class CouldNotCheckDocumentException extends Exception {

	/** The err. */
	String err = "";

	/**
	 * Instantiates a new could not check document exception.
	 * 
	 * @param errmsg
	 *            the errmsg
	 */
	public CouldNotCheckDocumentException(String errmsg) {
		err = errmsg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	public String toString() {
		return "Document Exception[" + err + "]";
	}
}
