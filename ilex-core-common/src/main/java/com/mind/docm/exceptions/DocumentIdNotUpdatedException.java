//
//
//
//@ Project : DocM - Document Management System - Ilex
//@ File Name : DocumentIdNotUpdatedException.java
//@ Date : 12-June-2008
//@ Author : Vishal Kapoor
//
//
package com.mind.docm.exceptions;

public class DocumentIdNotUpdatedException extends Exception{
	/**
	 * @name DocumentIdNotUpdatedException
	 * @purpose This method is used to show the message when updating database for the documentId.  
	 * @steps	1. User requests to set the docuemntId for the specific entity and it does run successfully so this exception is thrown.
	 * @param String
	 * @return  
	 * @returnDescription       
	 */
	public DocumentIdNotUpdatedException(String expMessage){
		super(expMessage);
	}
}
