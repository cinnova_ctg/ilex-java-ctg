//
//
//
//@ Project : DocM - Document Management System - Ilex
//@ File Name : DocumentIdNotFoundException.java
//@ Date : 12-June-2008
//@ Author : Vishal Kapoor
//
//
package com.mind.docm.exceptions;


public class DocumentIdNotFoundException extends Exception{
	/**
	 * @name DocumentIdNotFoundException
	 * @purpose This method is used to show the message when the documentId correspponding to the entity not exists.  
	 * @steps	1. User requests to get the documentId from the database and it does not exists so this exception is thrown.
	 * @param String
	 * @return  
	 * @returnDescription       
	 */
	public DocumentIdNotFoundException(String expMessage){
		super(expMessage);
	}
}


