package com.mind.docm.exceptions;

public class EntityFileNameNotFoundException extends Exception{
	/**
	 * @name DocumentIdNotFoundException
	 * @purpose This method is used to show the message when the entityFileName not found.  
	 * @steps	1. User requests to get the entity corresponding fileName,if not found then exception is thrown.
	 * @param String
	 * @return  
	 * @returnDescription       
	 */
	public EntityFileNameNotFoundException(String expMessage){
		super(expMessage);
	}
}
