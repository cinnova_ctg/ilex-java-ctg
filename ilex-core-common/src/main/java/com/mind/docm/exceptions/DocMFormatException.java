package com.mind.docm.exceptions;

/**
 * The Class DocMFormatException.
 */
public class DocMFormatException extends Exception {

	/** The err. */
	String err = "";

	/**
	 * Instantiates a new doc m format exception.
	 * 
	 * @param errmsg
	 *            the errmsg
	 */
	public DocMFormatException(String errmsg) {
		err = errmsg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	public String toString() {
		return "Document Management Exception[" + err + "]";
	}
}
