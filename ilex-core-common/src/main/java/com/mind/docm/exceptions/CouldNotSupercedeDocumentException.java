package com.mind.docm.exceptions;

/**
 * The Class CouldNotSupercedeDocumentException.
 */
public class CouldNotSupercedeDocumentException extends Exception {

	/** The err. */
	String err = "";

	/**
	 * Instantiates a new could not supercede document exception.
	 * 
	 * @param errmsg
	 *            the errmsg
	 */
	public CouldNotSupercedeDocumentException(String errmsg) {
		err = errmsg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	public String toString() {
		return "Document Exception[" + err + "]";
	}
}
