package com.mind.docm.exceptions;

/**
 * The Class DocumentNotExistsException.
 */
public class DocumentNotExistsException extends Exception {

	/** The err. */
	String err = "";

	/**
	 * Instantiates a new document not exists exception.
	 * 
	 * @param errmsg
	 *            the errmsg
	 */
	public DocumentNotExistsException(String errmsg) {
		err = errmsg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	public String toString() {
		return "Document Not Exists Exception[" + err + "]";
	}
}
