package com.mind.docm.exceptions;

import org.apache.log4j.Logger;

public class FrequencyFormatException extends Exception{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(FrequencyFormatException.class);
	
	public void showMessage()
	{
		if (logger.isDebugEnabled()) {
			logger.debug("showMessage() - Frequecy format is not as per requirement.");
		}
	}

}
