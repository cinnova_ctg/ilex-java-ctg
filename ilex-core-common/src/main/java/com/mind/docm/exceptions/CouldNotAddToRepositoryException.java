package com.mind.docm.exceptions;

/**
 * The Class CouldNotAddToRepositoryException.
 */
public class CouldNotAddToRepositoryException extends Exception {

	/** The err. */
	String err;

	/**
	 * Instantiates a new could not add to repository exception.
	 * 
	 * @param error
	 *            the error
	 */
	CouldNotAddToRepositoryException(String error) {
		err = error;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	public String toString() {
		return "Could Not Add To Repository Exception[" + err + "]";
	}

}
