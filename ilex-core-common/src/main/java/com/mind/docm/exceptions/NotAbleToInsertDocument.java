package com.mind.docm.exceptions;

public class NotAbleToInsertDocument extends Exception{
	/**
	 * @name NotAbleToInsertDocument
	 * @purpose This method is used to show the message when the document insertion failed in DocM.  
	 * @steps	1. User requests to change staus of the entity and insert into the DocM.
	 * @param String
	 * @return  
	 * @returnDescription       
	 */
	public NotAbleToInsertDocument(String expMessage){
		super(expMessage);
	}

}
