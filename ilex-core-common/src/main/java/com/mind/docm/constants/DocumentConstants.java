package com.mind.docm.constants;

/**
 * The Class DocumentConstants.
 */
public class DocumentConstants {

	/** The DOCUMEN t_ version. */
	public static String DOCUMENT_VERSION = "dm_document_version";

	/** The DOCUMEN t_ master. */
	public static String DOCUMENT_MASTER = "dm_document_master";

	/** The DOCUMEN t_ file. */
	public static String DOCUMENT_FILE = "dm_document_file";

	/** The METADAT a_ list. */
	public static String METADATA_LIST = "dm_metadata_fields_list";

	/** The LIBRAR y_ metadat a_ setup. */
	public static String LIBRARY_METADATA_SETUP = "dm_library_metadata_setup";

	/** The SUPERSEDED. */
	public static String SUPERSEDED = "Superseded";

	/** The APPROVED. */
	public static String APPROVED = "Approved";

	/** The DOC m_ fil e_ sysdb. */
	public static String DOCM_FILE_SYSDB = "DocMFileSys";

	/** The DOC m_ sysdb. */
	public static String DOCM_SYSDB = "DocMSys";
}
