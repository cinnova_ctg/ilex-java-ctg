package com.mind.msp.common;

import org.apache.log4j.Logger;

/**
 * Constant values for MSP. The Class EscalationLevel.
 */
public class EscalationLevel {
	/*
	 * Escalation Level.
	 */
	/** The Constant INITIAL_ASSESSMENT for Real-time State Change. */
	private static final Logger logger = Logger
			.getLogger(EscalationLevel.class);
	static {
		if (logger.isDebugEnabled()) {
			logger.debug("this come in the escalate new");
		}
	}
	public static final int INITIAL_ASSESSMENT = 0;

	/** The Constant HELP_DESK for Incident � Help Desk. */
	public static final int HELP_DESK = 1;

	/** The Constant MSP_DISPATCH for Event � MSP Dispatch. */
	public static final int MSP_DISPATCH = 2;

	/*
	 * Ticket info. For Ticket Creation from Escalate.
	 */
	/** The Constant PROBLEM_CATEGORY. */
	public static final String PROBLEM_CATEGORY = "Repair Connectivity / Circuit";

	/** The Constant PROBLEM_DESCRIPTION. */
	public static final String PROBLEM_DESCRIPTION = "Troubleshoot site internet connectivity problem.";

	/** The Constant CRITICALITY. */
	public static final String CRITICALITY = "1";

	/** The Constant RESOURCE. */
	public static final String RESOURCE = "Systems Engineer";

	/** The Constant MSP_AGREEMENT. */
	public static final String MSP_AGREEMENT = "MSP Agreement";

	/** The Constant REQUESTOR. */
	public static final String REQUESTOR = "";

	/** The Constant REQUESTOR_EMAIL. */
	public static final String REQUESTOR_EMAIL = "";

	/** The STANDBY. */
	public static boolean STANDBY = false;

	/** The MSP. */
	public static boolean MSP = false;

	/** The HELPDESK. */
	public static boolean HELPDESK = true;

	/** The Constant SPECIAL_INSTRUCTIONS. */
	public static final String SPECIAL_INSTRUCTIONS = "";

	/** The Constant REQUEST_TYPE. */
	public static final String REQUEST_TYPE = "NetMedX - Standard";

	/*
	 * Messsage of MSP
	 */
	/** The Constant NO_ACTION_MESSAGE. */
	public static final String NO_ACTION_MESSAGE = "Record deleted successfully.";

	/** The Constant ESCALATE_MESSAGE. */
	public static final String ESCALATE_MESSAGE = "Ticket created successfully.";

	/*
	 * Others
	 */
	/** The Constant HELP_DESK for Incident � Help Desk. */
	public static final int Site_NAME_LENGTH = 16;

	/*
	 * Query for fill combo-boxes of Manage Outage.
	 */
	public static final String ISSUE_LIST_QUERY = "select * from dbo.wug_issue_owner";
	public static final String RESOLUTION_QUERY = "select * from dbo.wug_resolution";
	public static final String ROOT_CAUSE_QUERY = "select * from dbo.wug_root_cause";

	/* Labels */
	public static final String LABEL_0 = "Initial Assessment";
	public static final String LABEL_1 = "Help Desk";
	public static final String LABEL_2 = "MSP Dispatch";

	/**
	 * Resolved by Help Desk Constant
	 */
	public static final String INVOICE_NUMBER = "Help Desk";

	/**
	 * Escalate to MSP Dispatch Constants.
	 */

	public static final int ESCALATE_TICKET_CRITICALITY = 4;
	public static final int ESCALATE_TICKET_CRITICALITY_CATEGORY = 7;
	public static final String ESCALATE_TICKET_RESOURCELEVEL = "CFT2-C4";

	public static final String ILEX_CUSTOMER_NAME = "select distinct wug_gn_msa_id, wug_gn_ilex_customer_name"
			+ " from dbo.wug_group_name"
			+ " where wug_gn_active = 1 and wug_gn_ilex_customer_name is not null and wug_gn_group_name != 'Craigs Map'"
			+ " order by wug_gn_ilex_customer_name";

	public static final String ILEX_CUSTOMER_NAME_BY_DAY = "select distinct wug_dw_cp_customer, wug_dw_cp_msa_id from wug_dw_fact_customer_performance_by_day where wug_dw_cp_customer is not null order by wug_dw_cp_customer ";

	public static final String ILEX_CUSTOMER_NAME_FOR_CREDITS_RPT = "select distinct wug_gn_msa_id, wug_gn_ilex_customer_name "
			+ "from wug_group_name where wug_gn_ilex_customer_name is not null and wug_gn_msa_id is not null"
			+ " order by wug_gn_ilex_customer_name";
	public static final String ILEX_MSP_ISP_CIRCUIT_TYPE = "select wug_dw_is_table from wug_dw_fact_isp_performance_summary where wug_dw_is_type = 'ISP2' and wug_dw_is_msa_id = 0";
	public static final String ILEX_MSP_ISP_CIRCUIT_IP = "select wug_dw_is_table from wug_dw_fact_isp_performance_summary where wug_dw_is_type = 'ISP1' and wug_dw_is_msa_id = 0";

	public static final String ILEX_CUSTOMER_NAME_FOR_CHONIC_SITES = "select distinct label = wug_dw_ch_customer, "
			+ "val = wug_dw_ch_customer from wug_dw_fact_chronic_sites_by_customer "
			+ "order by wug_dw_ch_customer";
	/**
	 * Escalate Ticket Constatnts from Ticket Dashboard.
	 */

	public static final String CLOSE_ESCALATE_TICKET_INVOICE_NUMBER = "MSP Dispatch � 1";

	/* MSP CC Report Base Query for Section A list */

	public static final String CHECL_LIST_DATA = "Checklist/Data";

	public static final String BASE_QUERY_FORMSPCCREPORT = "select lr_ri_name, lr_ri_reporting_value1,isnull(lr_ri_reporting_value2,'') as lr_ri_reporting_value2, lr_ri_group "
			+ " from lr_msp_activity_based_progress_indicators";

	public static final String Base_Query_Map = "select lr_pc_prj_chk_item, lr_pc_prj_item_id from lr_project_to_checklist_item_map";

	public static final String BASE_QUERY_ORDERBYCLAUSE = "order by lr_Ri_group, lr_ri_sequence";

	public static final String BASE_QUERY_SECTIONB = "select distinct lr_ci_js_title, lr_ci_opr, "
			+ " isnull(convert(char(8),lr_ci_reference_date,1),'') as lr_ci_reference_date, "
			+ " lr_ci_error_status, lr_ci_js_id"
			+ " from lr_job_checklist_status";

	public static final String BASE_QUERY_CHICKFIL_SECTIONB = "select distinct lr_ci_js_id, lr_ci_js_title, lr_jc_name, lr_jc_value = case when rtrim(lr_jc_value) = '' "
			+ "then 'No Data' else rtrim(lr_jc_value) end from lr_job_checklist_status left outer join "
			+ "lr_job_custom_field_used on lr_ci_js_id = lr_jc_ci_js_id ";

	public static final String BASE_CHECK_QUERY = "select * from lr_msp_cc_status_report";

	/**
	 * For Managed And Unmanaged Data On Monitoring Screen 1 for Managed and 0
	 * for Unmanaged Initial Assessment
	 */
	/**
	 * The Constant MANAGED_INIT_ASSESSEMENT(1) for Real-Time Monitoring .
	 */
	public static final int MANAGED_INIT_ASSESSEMENT = 1;
	/**
	 * The Constant UNMANAGED_INIT_ASSESSEMENT(0) for Real-Time Monitoring.
	 */
	public static final int UNMANAGED_INIT_ASSESSEMENT = 0;

}