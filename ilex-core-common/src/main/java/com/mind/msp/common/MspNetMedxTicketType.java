package com.mind.msp.common;

/**
 * The Class MspNetMedxTicketType.
 * 
 * @author vijay kumar singh
 */
public enum MspNetMedxTicketType {
	PROBLEM_DESCRIPTION("Troubleshoot site internet connectivity problem."), PROBLEM_CATEGORY(
			"Repair Connectivity / Circuit"), REQUESTOR(""), REQUEST_TYPE(
			"NetMedX - Standard"), REQUESTOR_EMAIL(""), SPECIAL_INSTRUCTIONS(""), STANDBY(
			"false"), MSP("true"), HELPDESK("true"), CRITICALITY("1-Emergency"), PERIOD_OF_PERFORMANCE(
			"true"), RESOURCE("Systems Engineer"), MSP_AGREEMENT(
			"MSP Agreement"), ESCALATE_MONITORING_SOURCE(1), MSP_NETMEDIX_MONITORING_SOURCE(
			2), POP_VALUE("1"), RESOURCE_CLASS_NAME("Other"), SEPARATOR1("~"), SEPARATOR2(
			" - "), INVOICE_NUMBER("Helpdesk-Manual");

	private String code = null;
	private Integer id = null;

	private MspNetMedxTicketType(String reportName) {
		this.code = reportName;
	}

	private MspNetMedxTicketType(Integer reportName) {
		this.id = reportName;
	}

	public String getCode() {
		return code;
	}

	public Integer getInt() {
		return id;
	}

	public void setCode(String reportName) {
		this.code = reportName;
	}

	public void setCode(Integer reportName) {
		this.id = reportName;
	}

}
