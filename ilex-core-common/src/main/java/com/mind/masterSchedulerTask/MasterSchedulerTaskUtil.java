package com.mind.masterSchedulerTask;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;

import com.mind.bean.masterScheduler.AutoCompleteMasterSchedulerTaskBean;
import com.mind.common.EnvironmentSelector;

public class MasterSchedulerTaskUtil {

	public final static String CONTENT_TYPE_KEY = "Content-type";
	public final static String CONTENT_TYPE_VALUE = "application/json";
	public final static String POST_REQUEST = "POST";
	public final static String PROTOCOL = "TLSv1";
	public final static String PATH = "ms/project/template_task/auto_complete/";
	static Logger logger = Logger.getLogger(MasterSchedulerTaskUtil.class);
	public final static String secureUrl = EnvironmentSelector
			.getBundleString("api.secure.url");

	public static String sendAPIRequestForLogin(String userName, String password) {
		String jsonString = "";
		try {

			URL url = new URL(secureUrl + "ilex/user");
			URLConnection uc = url.openConnection();

			SSLContext ssl = SSLContext.getInstance(PROTOCOL);
			ssl.init(null, new TrustManager[] { new SimpleX509TrustManager() },
					null);
			SSLSocketFactory factory = ssl.getSocketFactory();

			HttpsURLConnection httpcon = (HttpsURLConnection) (uc);

			httpcon.setRequestMethod(POST_REQUEST);
			httpcon.setDoInput(true);
			httpcon.setDoOutput(true);
			httpcon.setSSLSocketFactory(factory);

			httpcon.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			});

			// setting up the parameters for POST request.
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("user_id", userName);
			params.put("access_token", password);

			params.put("x-access-user-id",
					EnvironmentSelector.getBundleString("api.username"));
			params.put("x-access-token",
					EnvironmentSelector.getBundleString("api.password"));

			// params.put("task_type_id", bean.getTaskTypeId());
			// params.put("task_type_key", bean.getTaskTypeKey());
			// params.put("user_id", "4");
			// params.put(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
			// to send the param in POST.
			OutputStream os = httpcon.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
					os, "UTF-8"));
			writer.write(getPostParamString(params));
			writer.flush();
			writer.close();
			os.close();

			httpcon.connect();

			// to get response.
			InputStream is = httpcon.getInputStream();
			Properties properties = new Properties();
			properties.load(is);

			jsonString = properties.toString();

			is.close();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return jsonString;
	}

	public static String sendAPIRequest(AutoCompleteMasterSchedulerTaskBean bean) {
		try {

			URL url = new URL(bean.getUrl() + PATH);
			URLConnection uc = url.openConnection();

			SSLContext ssl = SSLContext.getInstance(PROTOCOL);
			ssl.init(null, new TrustManager[] { new SimpleX509TrustManager() },
					null);
			SSLSocketFactory factory = ssl.getSocketFactory();

			HttpsURLConnection httpcon = (HttpsURLConnection) (uc);

			httpcon.setRequestMethod(POST_REQUEST);
			httpcon.setDoInput(true);
			httpcon.setDoOutput(true);
			httpcon.setSSLSocketFactory(factory);

			httpcon.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			});

			// setting up the parameters for POST request.
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("x-access-user-id", bean.getApiUser());
			params.put("x-access-token", bean.getApiUserPassword());
			params.put("task_type_id", bean.getTaskTypeId());
			params.put("task_type_key", bean.getTaskTypeKey());
			params.put("user_id", bean.getUserId());
			params.put(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
			// to send the param in POST.
			OutputStream os = httpcon.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
					os, "UTF-8"));
			writer.write(getPostParamString(params));
			writer.flush();
			writer.close();
			os.close();

			httpcon.connect();

			// to get response.
			InputStream is = httpcon.getInputStream();
			Properties properties = new Properties();
			properties.load(is);
			// System.out.println(properties.get("{\"status\""));
			is.close();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return "Status";
	}

	private static String getPostParamString(Hashtable<String, String> params) {
		if (params.size() == 0)
			return "";

		StringBuffer buf = new StringBuffer();
		Enumeration<String> keys = params.keys();
		while (keys.hasMoreElements()) {
			buf.append(buf.length() == 0 ? "" : "&");
			String key = keys.nextElement();
			buf.append(key).append("=").append(params.get(key));
		}
		return buf.toString();
	}

}

class SimpleX509TrustManager implements X509TrustManager {
	public void checkClientTrusted(X509Certificate[] cert, String s)
			throws CertificateException {
	}

	public void checkServerTrusted(X509Certificate[] cert, String s)
			throws CertificateException {
	}

	@Override
	public X509Certificate[] getAcceptedIssuers() {
		// TODO Auto-generated method stub
		return null;
	}

}