package com.mind.common;

import java.io.Serializable;

public class CMPocSearchList implements Serializable{
	
	private String lo_pc_id=null;
	private String contact=null;
	private String lo_pc_phone=null;
	private String lo_pc_fax=null;
	private String lo_pc_email1=null;
	private String lo_pc_address1=null;
	private String lo_pc_address2=null;
	private String lo_pc_city=null;
	private String lo_pc_state=null;
	private String lo_pc_zip_code=null;
	private String lo_pc_country=null;
	
	public CMPocSearchList(String lo_pc_id, String contact, String lo_pc_phone, String lo_pc_fax, String lo_pc_email1, String lo_pc_address1, String lo_pc_address2, String lo_pc_city, String lo_pc_state, String lo_pc_zip_code, String lo_pc_country) {
		this.lo_pc_id = lo_pc_id;
		this.contact = contact;
		this.lo_pc_phone = lo_pc_phone;
		this.lo_pc_fax = lo_pc_fax;
		this.lo_pc_email1 = lo_pc_email1;
		this.lo_pc_address1 = lo_pc_address1;
		this.lo_pc_address2 = lo_pc_address2;
		this.lo_pc_city = lo_pc_city;
		this.lo_pc_state = lo_pc_state;
		this.lo_pc_zip_code = lo_pc_zip_code;
		this.lo_pc_country = lo_pc_country;
	}
	public CMPocSearchList() {
		super();

	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getLo_pc_address1() {
		return lo_pc_address1;
	}
	public void setLo_pc_address1(String lo_pc_address1) {
		this.lo_pc_address1 = lo_pc_address1;
	}
	public String getLo_pc_address2() {
		return lo_pc_address2;
	}
	public void setLo_pc_address2(String lo_pc_address2) {
		this.lo_pc_address2 = lo_pc_address2;
	}
	public String getLo_pc_city() {
		return lo_pc_city;
	}
	public void setLo_pc_city(String lo_pc_city) {
		this.lo_pc_city = lo_pc_city;
	}
	public String getLo_pc_country() {
		return lo_pc_country;
	}
	public void setLo_pc_country(String lo_pc_country) {
		this.lo_pc_country = lo_pc_country;
	}
	public String getLo_pc_email1() {
		return lo_pc_email1;
	}
	public void setLo_pc_email1(String lo_pc_email1) {
		this.lo_pc_email1 = lo_pc_email1;
	}
	public String getLo_pc_fax() {
		return lo_pc_fax;
	}
	public void setLo_pc_fax(String lo_pc_fax) {
		this.lo_pc_fax = lo_pc_fax;
	}
	public String getLo_pc_id() {
		return lo_pc_id;
	}
	public void setLo_pc_id(String lo_pc_id) {
		this.lo_pc_id = lo_pc_id;
	}
	public String getLo_pc_phone() {
		return lo_pc_phone;
	}
	public void setLo_pc_phone(String lo_pc_phone) {
		this.lo_pc_phone = lo_pc_phone;
	}
	public String getLo_pc_state() {
		return lo_pc_state;
	}
	public void setLo_pc_state(String lo_pc_state) {
		this.lo_pc_state = lo_pc_state;
	}
	public String getLo_pc_zip_code() {
		return lo_pc_zip_code;
	}
	public void setLo_pc_zip_code(String lo_pc_zip_code) {
		this.lo_pc_zip_code = lo_pc_zip_code;
	}

}
