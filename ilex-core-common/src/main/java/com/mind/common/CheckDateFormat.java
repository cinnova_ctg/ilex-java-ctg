package com.mind.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;


public class CheckDateFormat {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CheckDateFormat.class);

	public static boolean isValidDateStr(String date, String format) {
		boolean check = true;
		 
	    try {
	      SimpleDateFormat sdf = new SimpleDateFormat(format);
//		  String inputDate = sdf.format(date);
//	      sdf.parse(date);
		  Date d = new SimpleDateFormat(format).parse(date);

	    }catch (ParseException e) {
			check = false;
	    }catch (IllegalArgumentException e) {
			logger.error("isValidDateStr(String, String)", e);
			check =  false;
	    } 
	    return check;
	}
	public static boolean checkFormat(String str) {
		boolean check=true;
		String checkDate[] = str.split("/");

		if(CheckDateFormat.isValidDateStr(str,"MM/dd/yyyy HH:mm")) {
			if(checkDate[0].length()>2) {
				check=false;
			} else {
				 if(checkDate[0].length()==1) {
					 check=true;
				 } else {
					 if(Integer.parseInt(checkDate[0].substring(0,1))==0) {
						 check=true;
					 }  else {
						 if(Integer.parseInt(checkDate[0].substring(0,1))==1 && Integer.parseInt(checkDate[0].substring(1,2))<=2) {
							 check=true;
						 }	 
						 else 
							 check=false;
					 }
				 }
			}
			//System.out.println("check--1"+check);
			 if(check) {
				 if(checkDate[1].length()>2) {
					 check=false;
				 } else {
					 if(checkDate[1].length()==1) {
							 check=true;
						 } else {
							 if(Integer.parseInt(checkDate[1].substring(0,1))<=3) 
								 check=true;
							 else 
								 check=false;
							 if(Integer.parseInt(checkDate[1].substring(0,1))==3) { 
								 if(Integer.parseInt(checkDate[1].substring(1,2))<=1)
									 check=true;
								 else 
									 check=false;
							 }
						 }
				 }
			 }
			 //System.out.println("check--2"+check);
			 if(check) {
				if(checkDate[2].indexOf(" ")==2 || checkDate[2].indexOf(" ")==4 ) {
					if(checkDate[2].split(":").length >2) {
						check=false;
					} else {
						//System.out.println(checkDate[2].length()-checkDate[2].indexOf(":"));
						if(checkDate[2].length()-checkDate[2].indexOf(":")>3) { 
							if( checkDate[2].substring(checkDate[2].length()-2,checkDate[2].length()).equals("PM")
								|| checkDate[2].substring(checkDate[2].length()-2,checkDate[2].length()).equals("AM")) {
								check=true;
							} else {
								//System.out.println("wwwwwww---");
								check=false;
							}
						} else {
							check=true;
						}
					}
				} else 
					check = false;
			 }
		} else {
			if(CheckDateFormat.isValidDateStr(str,"MM/dd/yyyy")) {
				if(checkDate[0].length()>2) {
					check=false;
				} else {
					 if(checkDate[0].length()==1) {
						 check=true;
					 } else {
						 if(Integer.parseInt(checkDate[0].substring(0,1))==0) {
							 check=true;
						 }  else {
							 if(Integer.parseInt(checkDate[0].substring(0,1))==1 && Integer.parseInt(checkDate[0].substring(1,2))<=2) {
								 //System.out.println("checkdate first 2 part--"+checkDate[0]);
								 check=true;
							 }	 
							 else 
								 check=false;
						 }
					 }
				}
//				System.out.println("check--1"+check);
				 if(check) {
					 if(checkDate[1].length()>2) {
						 check=false;
					 } else {
						 if(checkDate[1].length()==1) {
								 check=true;
							 } else {
								 if(Integer.parseInt(checkDate[1].substring(0,1))<=3) 
									 check=true;
								 else 
									 check=false;
								 if(Integer.parseInt(checkDate[1].substring(0,1))==3) { 
									 if(Integer.parseInt(checkDate[1].substring(1,2))<=1)
										 check=true;
									 else 
										 check=false;
								 }
							 }
					 }
				 }
				 //System.out.println("check--2"+check);
				 if(check) {
					if(checkDate[2].length()==2 || checkDate[2].length()==4 ) {
						check=true;
					} else {
						check=false;
					}
				 }
			} else {
				check=false;
			}
		} 
		//System.out.println("value ----"+check);
		return check;
	}
	public static void main(String[] args) {
		String str="21/31/2008 55:59 AM";
		boolean check=true;
		if (logger.isDebugEnabled()) {
			logger.debug("main(String[]) - " + str + "  valid ? " + CheckDateFormat.checkFormat(str));
		}
				/*String check2[]=str.split("/");
				//System.out.println(check2.length);
				//System.out.println("1 st value "+check2[0]+"check2[0].substring(1,2)--"+check2[0].substring(0,2));
				if(CheckDateFormat.isValidDateStr(str,"MM/dd/yyyy HH:mm")) {
					 if(check2[0].length()==1) {
						 check=true;
					 } else {
						 if(Integer.parseInt(check2[0].substring(1,2))<=2) {
							 check=true;
						 }	 
						 else 
							 check=false;
					 }
					 if(check) {
					 if(check2[1].length()==1) {
							 check=true;
						 } else {
							 if(Integer.parseInt(check2[1].substring(0,1))<=3) 
								 check=true;
							 else 
								 check=false;
							 if(Integer.parseInt(check2[1].substring(0,1))==3) { 
								 if(Integer.parseInt(check2[1].substring(1,2))<=1)
									 check=true;
								 else 
									 check=false;
							 }
						 }
					 }
					 if(check) {
						if(check2[2].indexOf(" ")==2 || check2[2].indexOf(" ")==4 ) 
							check = true;
						else 
							check = false;
					 }
				} else {
					check=false;
				}
				System.out.println("2 value "+check2[1]+ "check2[1].substring(1,1)" +check2[1].substring(1,1));
				System.out.println("3 value "+check2[2].indexOf(" "));
				System.out.println("value ----"+check);
				*/
			//System.out.println("value--"+Integer.parseInt("12/3/2008".substring(1,3)));

		    // "1990-12/13" throws a ParseException
		    //System.out.println(" 1900-12/13 valid ? " 
		        //+ CheckDateFormat.isValidDateStr("1900-12/13","mm-dd-yy"));
		    // "1990-13-12" throws a IllegalArgumentException
		    //System.out.println(" 1900-13-12 valid ? " 
		        //+ CheckDateFormat.isValidDateStr("1900-13-12","yyyy-MM-dd"));
		    /*
		     * output :
		     *  1900-12-13 valid ? true
		     *  1900-12/13 valid ? false
		     *  1900-13-12 valid ? false
		     */
	}
}



