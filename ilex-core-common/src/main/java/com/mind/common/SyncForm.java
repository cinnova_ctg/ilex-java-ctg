package com.mind.common;

public class SyncForm {
	
	//Table dm_lo_organization_type
	private String orgType = null;
	private String orgTypeName = null;
	
	//Table dm_lo_organization_discipline
	private String orgDispId = null;
	private String orgDispName = null;
	
	//Table dm_lo_role
	private String roleId = null;
	private String roleDesc = null;
	private String roleName = null;
	
	//Table dm_cc_function
	private String functionId = null;
	private String functionGroupName = null;
	private String functionName = null;
	private String functionTypeGroup = null;
	private String functionType = null;
	private String functionIdDocM = null;
	
	//Table dm_lo_poc
	private String loPcId = null;
	private String loPcFirstName = null;
	private String loPcLastName = null;
	private String loPcTitle = null;
	private String loPcCreatedDate = null;
	
	//Table dm_lo_user
	private String loUsPcId = null;
	private String loUsPcName = null;
	private String loUsPcPass = null;
	
	public String getRoleDesc() {
		return roleDesc;
	}
	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getOrgDispId() {
		return orgDispId;
	}
	public void setOrgDispId(String orgDispId) {
		this.orgDispId = orgDispId;
	}
	public String getOrgDispName() {
		return orgDispName;
	}
	public void setOrgDispName(String orgDispName) {
		this.orgDispName = orgDispName;
	}
	public String getOrgType() {
		return orgType;
	}
	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}
	public String getOrgTypeName() {
		return orgTypeName;
	}
	public void setOrgTypeName(String orgTypeName) {
		this.orgTypeName = orgTypeName;
	}
	public String getFunctionGroupName() {
		return functionGroupName;
	}
	public void setFunctionGroupName(String functionGroupName) {
		this.functionGroupName = functionGroupName;
	}
	public String getFunctionId() {
		return functionId;
	}
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getFunctionType() {
		return functionType;
	}
	public void setFunctionType(String functionType) {
		this.functionType = functionType;
	}
	public String getFunctionTypeGroup() {
		return functionTypeGroup;
	}
	public void setFunctionTypeGroup(String functionTypeGroup) {
		this.functionTypeGroup = functionTypeGroup;
	}
	public String getLoPcCreatedDate() {
		return loPcCreatedDate;
	}
	public void setLoPcCreatedDate(String loPcCreatedDate) {
		this.loPcCreatedDate = loPcCreatedDate;
	}
	public String getLoPcFirstName() {
		return loPcFirstName;
	}
	public void setLoPcFirstName(String loPcFirstName) {
		this.loPcFirstName = loPcFirstName;
	}
	public String getLoPcId() {
		return loPcId;
	}
	public void setLoPcId(String loPcId) {
		this.loPcId = loPcId;
	}
	public String getLoPcLastName() {
		return loPcLastName;
	}
	public void setLoPcLastName(String loPcLastName) {
		this.loPcLastName = loPcLastName;
	}
	public String getLoPcTitle() {
		return loPcTitle;
	}
	public void setLoPcTitle(String loPcTitle) {
		this.loPcTitle = loPcTitle;
	}
	public String getFunctionIdDocM() {
		return functionIdDocM;
	}
	public void setFunctionIdDocM(String functionIdDocM) {
		this.functionIdDocM = functionIdDocM;
	}
	public String getLoUsPcId() {
		return loUsPcId;
	}
	public void setLoUsPcId(String loUsPcId) {
		this.loUsPcId = loUsPcId;
	}
	public String getLoUsPcName() {
		return loUsPcName;
	}
	public void setLoUsPcName(String loUsPcName) {
		this.loUsPcName = loUsPcName;
	}
	public String getLoUsPcPass() {
		return loUsPcPass;
	}
	public void setLoUsPcPass(String loUsPcPass) {
		this.loUsPcPass = loUsPcPass;
	}
}
