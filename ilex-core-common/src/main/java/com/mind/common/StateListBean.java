package com.mind.common;

import java.util.ArrayList;

public class StateListBean {
	
	private ArrayList initialStateCategory = null;
	private ArrayList tempList = null;
	private String siteCountryId = null;
	private String siteCountryIdName = null;
	
	
	public ArrayList getInitialStateCategory() {
		return initialStateCategory;
	}
	public void setInitialStateCategory(ArrayList initialStateCategory) {
		this.initialStateCategory = initialStateCategory;
	}
	public String getSiteCountryId() {
		return siteCountryId;
	}
	public void setSiteCountryId(String siteCountryId) {
		this.siteCountryId = siteCountryId;
	}
	public String getSiteCountryIdName() {
		return siteCountryIdName;
	}
	public void setSiteCountryIdName(String siteCountryIdName) {
		this.siteCountryIdName = siteCountryIdName;
	}
	public ArrayList getTempList() {
		return tempList;
	}
	public void setTempList(ArrayList tempList) {
		this.tempList = tempList;
	}
	

}
