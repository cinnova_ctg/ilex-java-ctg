package com.mind.common;

import java.util.ResourceBundle;

import com.mind.fw.util.PropertyFileUtil;

/*
 * CLass is used to return the Resource Bundle object on the basis of Property File.
 */
public class EnvironmentSelector {
	public static String getBundleString(String key) {
		return PropertyFileUtil.getAppProperty(key);
	}

	public static String getResourceString(String key) {
		ResourceBundle rb = ResourceBundle
				.getBundle("com.mind.properties.ApplicationResources");
		return rb.getString(key);
	}

}
