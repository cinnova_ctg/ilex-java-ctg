package com.mind.common;



public class PODeliverableStatusTypes {
	public static final int STATUS_PENDING=0;
	public static final int STATUS_UPLOADED=1;
	public static final int STATUS_ACCEPTED=2;
	public static final int STATUS_REJECTED=3;
	
	public static String getStatusName(int statusCode) {
		if(statusCode==STATUS_PENDING) {
			return "Pending";
		} else if(statusCode==STATUS_UPLOADED) {
			return "Uploaded";
		} else if(statusCode==STATUS_ACCEPTED) {
			return "Accepted";
		} else if(statusCode==STATUS_REJECTED) {
			return "Rejected";
		} else {
			return null;
		}
	}
}
