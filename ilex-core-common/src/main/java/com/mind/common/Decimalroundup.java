package com.mind.common;

import java.text.DecimalFormat;
public class Decimalroundup 
{
	public static String twodecimalplaces( float value , int roundcriteria )
	{
		DecimalFormat dfcurtwo	= new DecimalFormat( "###0.00" );
		DecimalFormat dfcurfour = new DecimalFormat( "###0.0000" );
		DecimalFormat dfcur = new DecimalFormat( "#,###,##0.00" );
		DecimalFormat dfcurthree = new DecimalFormat( "###0.000" );
		
		String temp = null;
		
		if( roundcriteria == 2 )
			temp = dfcurtwo.format( value )+"";
		
		if( roundcriteria == 4 )
			temp = dfcurfour.format( value )+"";
		
		if( roundcriteria == 3 )
			temp = dfcurthree.format( value )+"";
		
		if( roundcriteria == 1 )
			temp = dfcur.format( value )+"";
		
		return temp;
	}
}
