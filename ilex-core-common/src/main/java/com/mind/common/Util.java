package com.mind.common;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.log4j.Logger;

/**
 * The Class Util.
 */
public class Util {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Util.class);

	/**
	 * This method will change a string of single qoutes to double quotes.
	 * 
	 * @author amitm
	 * @param strSingleQuotes
	 * @return
	 */

	public static String changeStringToDoubleQuotes(String strSingleQuotes) {
		String str = "";

		str = strSingleQuotes.replace("\'", "|");

		return str;
	}

	/**
	 * method for getting temporary folder location
	 * 
	 * @return the temp
	 */
	public static String getTemp() {
		return (System.getProperty("ilex.temp.path"));
	}

	/**
	 * Change string to mpercent.
	 * 
	 * @param strSingleQuotes
	 *            the str single quotes
	 * 
	 * @return the string
	 */
	public static String changeStringToMpercent(String strSingleQuotes) {
		String str = "";

		str = strSingleQuotes.replace("&amp;", "&");

		return str;
	}

	/**
	 * Single to double quotes.
	 * 
	 * @param input
	 *            the input
	 * 
	 * @return the string
	 */
	public static String singleToDoubleQuotes(String input) {
		String str = "";
		if (input != null && !input.equals(""))
			str = input.trim().replaceAll("\'", "\'\'");
		return str;

	}

	/**
	 * Change string to comma seperation.
	 * 
	 * @param input
	 *            the input
	 * 
	 * @return the string
	 */
	public static String changeStringToCommaSeperation(String[] input) {
		String output = "";
		if (input != null && input.length > 0) {
			for (int i = 0; i < input.length; i++)
				output = output + singleToDoubleQuotes(input[i]) + ",";
			output = output.substring(0, output.length() - 1);
		}
		return output;
	}

	/**
	 * Method used to filter the special characters with their corresponding
	 * equivalent
	 * 
	 * @param String
	 *            any String
	 * @return String String after filtering the special characters
	 * @see
	 **/
	public static String filter(String input) {
		if (!hasSpecialChars(input)) {
			return (input);
		}
		StringBuffer filtered = new StringBuffer(input.length());
		char c;

		for (int i = 0; i < input.length(); i++) {
			c = input.charAt(i);
			switch (c) {
			case '<':
				filtered.append("&lt;");
				break;
			case '>':
				filtered.append("&gt;");
				break;
			case '"':
				filtered.append("&quot;");
				break;
			case '&':
				filtered.append("&amp;");
				break;
			case '\'':
				filtered.append("\\'");
				break;
			default:
				filtered.append(c);
			}
		}
		return (filtered.toString());
	}

	/**
	 * Method used to filter the special characters with their corresponding
	 * equivalent
	 * 
	 * @param String
	 *            any String
	 * @return String String after filtering the special characters
	 * @see
	 **/
	public static String filterAjaxXML(String input) {
		if (!hasSpecialChars(input)) {
			return (input);
		}
		StringBuffer filtered = new StringBuffer(input.length());
		char c;

		for (int i = 0; i < input.length(); i++) {
			c = input.charAt(i);
			switch (c) {
			case '<':
				filtered.append("&lt;");
				break;
			case '>':
				filtered.append("&gt;");
				break;
			case '"':
				filtered.append("&quot;");
				break;
			case '&':
				filtered.append("&amp;");
				break;
			default:
				filtered.append(c);
			}
		}
		return (filtered.toString());
	}

	/**
	 * Checks for special chars.
	 * 
	 * @param input
	 *            the input
	 * 
	 * @return true, if successful
	 */
	private static boolean hasSpecialChars(String input) {
		boolean flag = false;
		if ((input != null) && (input.length() > 0)) {
			char c;
			for (int i = 0; i < input.length(); i++) {
				c = input.charAt(i);
				switch (c) {
				case '<':
					flag = true;
					break;
				case '>':
					flag = true;
					break;
				case '"':
					flag = true;
					break;
				case '&':
					flag = true;
					break;
				case '\'':
					flag = true;
					break;
				}
			}
		}
		return (flag);
	}

	/**
	 * Replace all \n(Enter) char to <BR/>
	 * from given String.
	 * 
	 * @param str
	 */
	public static String replaceToBr(String str) {
		try {
			str.trim();
			str = str.replaceAll("\n", "<BR/>").replaceAll("\r", "");
			// str = str.replaceAll("<", "&lt;");
			// str = str.replaceAll(">", "&lt;");
		} catch (Exception e) {
		}

		return str;
	}

	/**
	 * Html filter.
	 * 
	 * @param input
	 *            the input
	 * 
	 * @return the string
	 */
	public static String htmlFilter(String input) {
		if (!hasSpecialChars(input)) {
			return (input);
		}
		StringBuffer filtered = new StringBuffer(input.length());
		char c;

		for (int i = 0; i < input.length(); i++) {
			c = input.charAt(i);
			switch (c) {
			case '"':
				filtered.append("&quot;");
				break;
			case '&':
				filtered.append("&amp;");
				break;
			case '\'':
				filtered.append("\\'");
				break;
			default:
				filtered.append(c);
			}
		}
		return (filtered.toString());
	}

	/**
	 * Gets the value from property file.
	 * 
	 * @param key
	 *            the key
	 * 
	 * @return the value from property file
	 */
	public static String getValueFromPropertyFile(String key) {
		String value = "";
		ResourceBundle bundle = ResourceBundle
				.getBundle("com.mind.properties.ApplicationResources");
		if (key != null && !key.equals("")) {
			value = bundle.getString(key);
		}
		return value;
	}

	/**
	 * Description: This method marge state and country whith a ','
	 * 
	 * @param state
	 *            - State Id
	 * @param country
	 *            - Country Id
	 * @return String - stateId,countryId
	 */
	public static String concatString(String stateId, String countryId) {
		return stateId + "," + countryId;
	}

	/**
	 * Gets the key value.
	 * 
	 * @param key
	 *            the key
	 * 
	 * @return the key value
	 */
	public static String getKeyValue(String key) {
		ResourceBundle bundle = ResourceBundle
				.getBundle("com.mind.properties.ApplicationResources");
		String value = "";
		if (key != null && !key.equals("")) {
			value = bundle.getString(key);
		}
		return value;
	}

	/**
	 * Gets the stringval.
	 * 
	 * @param val
	 *            the val
	 * 
	 * @return the stringval
	 */
	public static String getStringval(boolean val) {
		if (val)
			return "Y";
		else if (!val)
			return "N";
		else
			return "0";
	}

	/**
	 * Gets the booleanval.
	 * 
	 * @param val
	 *            the val
	 * 
	 * @return the booleanval
	 */
	public static boolean getBooleanval(String val) {
		if (val != null && val.trim().equalsIgnoreCase("Y"))
			return true;
		else
			return false;
	}

	/**
	 * Gets the hours.
	 * 
	 * @return the hours
	 */
	public static ArrayList<LabelValue> getHours() {
		ArrayList<LabelValue> list = new ArrayList();
		String val = "";
		for (int i = 0; i <= 12; i++) {
			if (i < 10)
				val = "0" + String.valueOf(i);
			else
				val = String.valueOf(i);
			list.add(new LabelValue(val, val));
		}
		return list;
	}

	/**
	 * Gets the minutes.
	 * 
	 * @return the minutes
	 */
	public static ArrayList<LabelValue> getMinutes() {
		ArrayList<LabelValue> list = new ArrayList();
		String val = "";
		for (int i = 0; i <= 59; i++) {
			if (i < 10)
				val = "0" + String.valueOf(i);
			else
				val = String.valueOf(i);
			list.add(new LabelValue(val, val));

		}
		return list;
	}

	/**
	 * Discription: This method remove last char when it is ','.
	 * 
	 * @param str
	 *            - String
	 * @return String - modify string
	 */
	public static String removeLastComm(String str) {

		if (str != null && !str.trim().equals("")) {
			try {
				str = str.trim();
				if (str.lastIndexOf(",") == (str.length() - 1)) {
					str = str.substring(0, str.length() - 1);
				}
			} catch (Exception e) {
				logger.debug("removeLastComm(String) - exception ignored", e);
			}
		}
		return str;
	}

	/**
	 * Gets the float value.
	 * 
	 * @param floatExpectedvalue
	 *            the float expectedvalue
	 * 
	 * @return the float value
	 */
	public static float getFloatValue(String floatExpectedvalue) {
		if (floatExpectedvalue != null && !floatExpectedvalue.trim().equals("")) {
			try {
				return new Float(floatExpectedvalue);
			} catch (NumberFormatException e) {
				logger.error("getFloatValue(String)", e);

				logger.error("getFloatValue(String)", e);
			} catch (NullPointerException e) {
				logger.error("getFloatValue(String)", e);

				logger.error("getFloatValue(String)", e);
			}
		}
		return 0.0f;
	}

	/**
	 * Truncate string.
	 * 
	 * @param str
	 *            the str
	 * @param size
	 *            the size
	 * 
	 * @return the string
	 */
	public static String truncateString(String str, int size) {

		if (str == null)
			return "";
		if (str.length() > size) {
			str = str.substring(0, size);

			str = str + ".. ";

		}

		return str;
	}

	/**
	 * Discription: This method remove last char from StringBuffer when it is
	 * ','.
	 * 
	 * @param sb
	 * @return
	 */
	public StringBuffer removeLastCommChar(StringBuffer sb) {
		if (logger.isDebugEnabled()) {
			logger.debug("removeLastCommChar(StringBuffer) - AAAA= "
					+ sb.lastIndexOf(","));
		}
		if (logger.isDebugEnabled()) {
			logger.debug("removeLastCommChar(StringBuffer) - BBB= "
					+ sb.length());
		}
		if (sb.length() > 0 && sb.lastIndexOf(",") > -1) {
			if (sb.lastIndexOf(",") == sb.length() - 1) {
				sb.delete(sb.length() - 1, sb.length());
			}
		}
		return sb;
	}

	/**
	 * Gets the defaulting value to 0.
	 * 
	 * @param String
	 *            the str
	 * 
	 * @return String the default value
	 */
	public static String getDefaultValue(String str) {
		if (isNullOrBlank(str))
			str = "";
		return str;
	}

	/**
	 * Find out value of given lable from list..
	 * 
	 * @param ArrayList
	 *            <LabelValue> the list
	 * @param String
	 *            the search text
	 * @param lableValue
	 *            the lable or value
	 * 
	 * @return String the list value
	 */
	public static String getListValue(ArrayList<LabelValue> list,
			String searchString, String lableValue) {
		String value = "";
		Iterator iterator = list.iterator();

		if (isNullOrBlank(searchString)) {
			return Util.getDefaultValue(value);
		}

		if (lableValue.equals("label")) {
			while (iterator.hasNext()) {
				LabelValue labelValue = (LabelValue) iterator.next();
				if (labelValue.getLabel().equals(searchString)) {
					value = labelValue.getValue();
					break;
				}
			}
		} else {

			if (searchString.equals("0"))
				return Util.getDefaultValue(value);

			while (iterator.hasNext()) {
				LabelValue labelValue = (LabelValue) iterator.next();
				if (labelValue.getValue().equals(searchString)) {
					value = labelValue.getLabel();
					break;
				}
			}
		}
		return Util.getDefaultValue(value);
	}

	/**
	 * Discription: This method is used to write a file into /PM/Viewxml/temp
	 * folder.
	 * 
	 * @param file
	 *            - File name for store.
	 * @param path
	 *            - Path name where this file will be stored.
	 */
	public static void writeFile(byte[] fileData, String path) throws Exception {
		/**
		 * First convet file to byte[]. Open FileOutputStream and set path for
		 * store. Write byte[] and after that close this FileOutputStream.
		 */
		byte[] buffer = fileData;
		FileOutputStream fw = new FileOutputStream(path);
		fw.write(buffer);
		fw.close();
	}

	/**
	 * Discription: to remove -0.0 value.
	 * 
	 * @param value
	 * @return
	 */
	public static String checkNegative(String value) {
		if (value != null && value.equals("-0.00")) {
			return "0.00";
		}
		return value;
	}

	/**
	 * Return true when stirng is null or blank.
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNullOrBlank(String str) {
		if (str == null || str.trim().equals("")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Compare two String if its match, return true else return false;
	 * 
	 * @param str1
	 *            -- To Compare String
	 * @param str2
	 *            -- With Compare String
	 * @return
	 */
	public static boolean compareTwoString(String str1, String str2) {
		boolean check = false;
		if (!isNullOrBlank(str1) && !isNullOrBlank(str2)) {
			if (str1.equalsIgnoreCase(str2)) {
				check = true;
			}
		}
		return check;
	}

	/**
	 * Discription:- This method used to extarct numeric value from string.
	 * 
	 * @param stringVal
	 * @return
	 */
	public static int fixNumeric(String stringVal) {

		logger.debug("Check for Integer.ParseInt(appendixId) Job Util.fixNumeric(String) 88888 =>"
				+ stringVal);
		int tempIntVal = 0;
		boolean isConverted = false;
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < 4; i++) {
			try {
				tempIntVal = Integer.parseInt(stringVal);
				isConverted = true;
				break;
			} catch (Exception e) {
			}
		}

		if (!isConverted) {
			char charArr[] = stringVal.toCharArray();
			for (int j = 0; j < charArr.length; j++) {
				try {
					int tempInt = Integer.parseInt(String.valueOf(charArr[j]));
					sb.append(charArr[j]);
				} catch (Exception e) {
				}
			}
			for (int i = 0; i < 4; i++) {
				try {
					tempIntVal = Integer.parseInt(sb.toString());
					isConverted = true;
					break;
				} catch (Exception e) {
				}
			}

		}
		logger.debug("Check for Integer.ParseInt(appendixId) Job Util.fixNumeric(String) 9999 =>"
				+ tempIntVal);
		if (!isConverted)
			tempIntVal = 0;

		return tempIntVal;
	}

	/**
	 * Gets the email addresses with comma seperated.
	 * 
	 * @param str
	 *            the str
	 * @return the email addresses
	 */
	public static String validateEmailAddresses(String str) {

		StringBuffer addresses = new StringBuffer();
		StringTokenizer tokenizer = new StringTokenizer(str, ";");
		while (tokenizer.hasMoreTokens()) {
			String tempAddress = tokenizer.nextToken();
			if (isValidEmailAddress(tempAddress)) {
				addresses.append(tempAddress + ",");
			}
		}
		return addresses.toString();
	}// End String getEmailAddresses()

	public static boolean isValidEmailAddress(String aEmailAddress) {
		if (aEmailAddress == null)
			return false;
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(aEmailAddress);
			if (!hasNameAndDomain(aEmailAddress)) {
				result = false;
			}
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

	private static boolean hasNameAndDomain(String aEmailAddress) {
		String[] tokens = aEmailAddress.split("@");
		return tokens.length == 2 && tokens[0].trim().length() > 0
				&& tokens[1].trim().length() > 0;
	}

	/**
	 * Gets the fix no of Characters from given String.
	 * 
	 * @param String
	 *            the str
	 * @param int the char length
	 * 
	 * @return String the fix characters
	 */
	public static String getFixCharacters(String str, int charLength) {
		if (!Util.isNullOrBlank(str) && str.length() > charLength) {
			StringBuffer sb = new StringBuffer();
			char[] characters = str.toCharArray();
			for (int i = 0; i < charLength; i++) {
				sb.append(characters[i]);
			}
			str = sb.toString();
		}
		return str;
	}// End getFixCharacters();

	/**
	 * Gets the node value from given XML.
	 * 
	 * @param String
	 *            the xml
	 * @param String
	 *            the nodes
	 * 
	 * @return String the node value
	 */
	public static String[] getNodeValue(String xml, String[] nodes) {

		if (!Util.isNullOrBlank(xml) && nodes != null) {
			String[] values = new String[nodes.length];
			for (int i = 0; i < nodes.length; i++) {
				values[i] = xml.substring(xml.indexOf("<" + nodes[i] + ">")
						+ nodes[i].length() + 2,
						xml.indexOf("</" + nodes[i] + ">"));
			}
			return values;
		}
		return null;
	}

	/**
	 * Purpose: Gets the splited array.
	 * 
	 * @param str
	 *            the string which is going for split.
	 * @param regex
	 *            the splitting char
	 * 
	 * @return String[] the splited value.
	 */
	public static String[] getSplitArray(String str, String regex) {
		if (str == null || regex == null) {
			return null;
		}
		return str.split(regex);
	}

	/**
	 * The main method. For testing.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
				+ "<installNotesInfo generated=\"ulq\">\n"
				+ "<latestInstallNotesDate>first date</latestInstallNotesDate>"
				+ "<latestInstallNotes>first note</latestInstallNotes>"
				+ "<secondLatestInstallNotesDate>second date</secondLatestInstallNotesDate>"
				+ "<secondLatestInstallNotes>second date</secondLatestInstallNotes>"
				+ "</installNotesInfo>";

		Util util = new Util();
		String nodes[] = { "latestInstallNotesDate", "latestInstallNotes",
				"secondLatestInstallNotesDate", "secondLatestInstallNotes" };
		String[] values = util.getNodeValue(xml, nodes);
		System.out.println(values.length);
		System.out.println(values[0]);
		System.out.println(values[1]);
		System.out.println(values[2]);
		System.out.println(values[3]);
	}

}
