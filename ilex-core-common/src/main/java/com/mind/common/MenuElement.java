package com.mind.common;

import java.io.PrintWriter;
import java.util.Vector;

public class MenuElement {
	
	private StringBuffer out = null;
	
	//		 images
	private String collapsedWidget = "images/ce_menu/oplus.gif";
	
	private String collapsedWidgetStart = "images/ce_menu/oplusStart.gif";
	
	private String collapsedWidgetStartTop = "images/ce_menu/oplusStartTop.gif";
	
	private String collapsedWidgetEnd = "images/ce_menu/oplusEnd.gif";
	
	private String expandedWidget = "images/ce_menu/ominus.gif";
	
	private String expandedWidgetStart = "images/ce_menu/ominusStart.gif";
	
	private String expandedWidgetStartTop = "images/ce_menu/ominusStartTop.gif";
	
	private String expandedWidgetEnd = "images/ce_menu/ominusEnd.gif";
	
	private String nodeWidget = "images/ce_menu/onode.gif";
	
	private String nodeWidgetEnd = "images/ce_menu/onodeEnd.gif";
	
	private String emptySpace = "images/ce_menu/oempty.gif";
	
	private String chainSpace = "images/ce_menu/ochain.gif";
	
	private String title = "";
	
	private String addImageStr = "";
	
	private String supplemental = "";
	
	private String idents = "";
	
	private String links = "";
	
	private Vector elements = new Vector();
	
	public MenuElement parent = null;
	
	public MenuElement(String title, String supplemental, String idents,
			String links, MenuElement parent) {
		this.title = title;
		this.supplemental = supplemental;
		this.idents = idents;
		this.links = links;
		this.parent = parent;
		out = new StringBuffer("");
	}
	
	public MenuElement(String title, String supplemental, String idents,
			String links, MenuElement parent, String addImageStr) {
		this.title = title;
		this.supplemental = supplemental;
		this.idents = idents;
		this.links = links;
		this.parent = parent;
		this.addImageStr = addImageStr;
		out = new StringBuffer("");
	}
	
	public MenuElement add(MenuElement m) {
		if (m != null)
			elements.add(m);
		return m;
	}
	public int getLevel() {
		MenuElement t =this;
		int count=0;
		while (t.parent!=null) { t=t.parent; count++; }
		return count;
	}
	public MenuElement getAncestor(int level) {
		MenuElement t =this;

		while (t.parent!=null) { t=t.parent; if (t.getLevel()==level) break; }
		return t;
	}
	public boolean isLastElement(MenuElement m) {
		if (m.parent==null) 
			return true;
		if  ( ((MenuElement) (m.parent.elements
				.elementAt(m.parent.elements.size() - 1)))
			.hashCode() == m.hashCode() )
			return true;
		return false;
	}
	public String draw(int level, MenuController mc) {
		
		if( mc.currID == 0 )
		{
			out.append("<div style='visibility:hidden;'  id='line" + (mc.currID++) + "'>");
		}
		else
		{
			out.append("<div class='m2' id='line" + (mc.currID++) + "'>");
		}

		for (int lcount = level; lcount >= 1; lcount--) {
			if (isLastElement(getAncestor(level-lcount)))
				out.append("<img src='" + emptySpace + "'>");
			else
				out.append("<img src='" + chainSpace + "'>");
		}
		if (elements!= null && elements.size()>0) {
			out.append("<img id='widget" + (mc.currID - 1) + "' src='");
			
			if (parent != null
					&& parent.elements != null
					&& ((MenuElement) (parent.elements
							.elementAt(parent.elements.size() - 1)))
							.hashCode() == this.hashCode()
							&& mc.blockID != 0) {
				out.append(collapsedWidgetEnd);
			} else {
				if (mc.blockID == 0) {
					out.append(collapsedWidgetStartTop);
				} else {
					out.append(collapsedWidget);
				}
			}
			
			out.append("' width='15' height='15' border='0' onClick='toggle(this, "
					+ mc.blockID + ",\"" + mc.moduleName + "\")'>");
			// Output display information as needed
			String style = "";
			if (mc.currID == mc.hLine) {
				style = "'a.color : white; background-color:#B7B3AA; '";
			}
			out.append(this.supplemental
					+ this.idents
					+ "<span style='"
					+ style
					+ "position:relative; top:-3px; height:11px;' onclick='sethighlight("
					+ mc.currID + ")'>" + title + "</span>");
			
			mc.currState += (this.getLevel()<=0)?"1":"0";
			
			out.append("<span class='' blocknum='" + mc.blockID
					+ "' id='block" + (mc.blockID++) + "' style='display:block;' >");
			
			for (int i = 0; i < elements.size(); i++) {
				out.append((((MenuElement) (elements.get(i))).draw(
						level + 1, mc)));
			}
			out.append("</span></div>");
		} else {
			out.append("<img id='widget" + (mc.currID - 1) + "' src='");
			if (parent != null && parent.elements != null) {
				out.append((((MenuElement) (parent.elements
						.elementAt(parent.elements.size() - 1)))
						.hashCode() == this.hashCode() ? nodeWidgetEnd
								: nodeWidget));
				}
			else out.append(nodeWidgetEnd);
			out.append("' alt='' border='0'>");
			String style = "";
			if (mc.currID == mc.hLine) {
				style = "a.color : white; background-color:#B7B3AA; ";
			}
			out.append(this.supplemental
					+ this.idents + "<span style='" + style
					+ "position:relative; top: -2px; height: 10px'>"
					+ this.title + "</span>");
			out.append("</div>");
		}
		
		return out.toString();
	}

	
	
	public void drawDirect(int level, MenuController mc , PrintWriter pw ) {
		
		if( mc.currID == 0 )
		{
			pw.print("<div style='visibility:hidden;'  id='line" + (mc.currID++) + "'>");
		}
		else
		{
			pw.print("<div class='m2' id='line" + (mc.currID++) + "'>");
		}
		
		for (int lcount = level; lcount >= 1; lcount--) {
			if (isLastElement(getAncestor(level-lcount)))
				pw.print("<img src='" + emptySpace + "'>");
			else
				pw.print("<img src='" + chainSpace + "'>");
		}
		if (elements!= null && elements.size()>0) {
			pw.print("<img id='widget" + (mc.currID - 1) + "' src='");
			
			if (parent != null
					&& parent.elements != null
					&& ((MenuElement) (parent.elements
							.elementAt(parent.elements.size() - 1)))
							.hashCode() == this.hashCode()
							&& mc.blockID != 0) {
				pw.print(collapsedWidgetEnd);
			} else {
				if (mc.blockID == 0) {
					pw.print(collapsedWidgetStartTop);
				} else {
					pw.print(collapsedWidget);
				}
			} 
			
			pw.print("' width='15' height='15' border='0' onClick='toggle(this, "
					+ mc.blockID + ",\"" + mc.moduleName + "\")'>");
			// Output display information as needed
			String style = "";
			if (mc.currID == mc.hLine) {
				style = "'a.color : white; background-color:#B7B3AA; '";
			}
			
			if(!this.addImageStr.equals(""))
				pw.print(this.supplemental
						+ this.idents
						+ "<span style='"
						+ style
						+ "position:relative; top:-3px; height:11px;' onclick='sethighlight("
						+ mc.currID + ")'>" + title + "</span> "+addImageStr);
			else
				pw.print(this.supplemental
						+ this.idents
						+ "<span style='"
						+ style
						+ "position:relative; top:-3px; height:11px;' onclick='sethighlight("
						+ mc.currID + ")'>" + title + "</span>");
			
			mc.currState += (this.getLevel()<=0)?"1":"0";
			
			pw.print("<span class='' blocknum='" + mc.blockID
					+ "' id='block" + (mc.blockID++) + "' style='display:none;'>");
			
			for (int i = 0; i < elements.size(); i++) {
				((MenuElement) (elements.get(i))).drawDirect(
						level + 1, mc , pw);
			}
			
			pw.print("</span></div>");	
		} else {
			pw.print("<img id='widget" + (mc.currID - 1) + "' src='");
			if (parent != null && parent.elements != null) {
				pw.print((((MenuElement) (parent.elements
						.elementAt(parent.elements.size() - 1)))
						.hashCode() == this.hashCode() ? nodeWidgetEnd
								: nodeWidget));
				}
			else pw.print(nodeWidgetEnd);
			pw.print("' alt='' border='0'>");
			String style = "";
			if (mc.currID == mc.hLine) {
				style = "a.color : white; background-color:#B7B3AA; ";
			}
			
			pw.print(this.supplemental
					+ this.idents + "<span style='" + style
					+ "position:relative; top: -2px; height: 10px'>"
					+ this.title + "</span>");
			
			if(!this.addImageStr.equals(""))
				pw.print(this.addImageStr+ "</div>");
			else
				pw.print("</div>");	
		}
		
		return;
	}
	
	
	
	
	
	public String drawCheck(int level, MenuController mc) {
					
				out.append("<BR>"+this.title+"---"+level);
				if (elements !=null) {

				for (int i = 0; i < elements.size(); i++) {
					out.append((((MenuElement) (elements.get(i))).drawCheck(
							level + 1, mc)));
					}
				}
		return out.toString();
		}
}
