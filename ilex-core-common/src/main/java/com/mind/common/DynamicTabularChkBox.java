/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is a bean class used for getting and setting the values of function name with their function types for a function group.      
*
*/

package com.mind.common;

public class DynamicTabularChkBox {
	
	private String c1 = null;
	private String c2 = null;
	private String c2id = null;
	private String c3 = null;
	private String c3id = null;
	private String c4 = null;
	private String c4id = null;
	private String c5 = null;
	private String c5id = null;
	private String c6 = null;
	private String c6id = null;
	private String c7 = null;
	private String c7id = null;
	private String c8 = null;
	private String c8id = null;
	private String c9 = null;
	private String c9id = null;
	private String c10 = null;
	private String c10id = null;
	private String c11 = null;
	private String c11id = null;
	private String c12 = null;
	private String c12id = null;
	
	
	
	public String getC1() {
		return c1;
	}
	public void setC1(String c1) {
		this.c1 = c1;
	}
	public String getC2() {
		return c2;
	}
	public void setC2(String c2) {
		this.c2 = c2;
	}
	public String getC2id() {
		return c2id;
	}
	public void setC2id(String c2id) {
		this.c2id = c2id;
	}
	public String getC3() {
		return c3;
	}
	public void setC3(String c3) {
		this.c3 = c3;
	}
	public String getC3id() {
		return c3id;
	}
	public void setC3id(String c3id) {
		this.c3id = c3id;
	}
	public String getC4() {
		return c4;
	}
	public void setC4(String c4) {
		this.c4 = c4;
	}
	public String getC4id() {
		return c4id;
	}
	public void setC4id(String c4id) {
		this.c4id = c4id;
	}
	public String getC5() {
		return c5;
	}
	public void setC5(String c5) {
		this.c5 = c5;
	}
	public String getC5id() {
		return c5id;
	}
	public void setC5id(String c5id) {
		this.c5id = c5id;
	}
	public String getC6() {
		return c6;
	}
	public void setC6(String c6) {
		this.c6 = c6;
	}
	public String getC6id() {
		return c6id;
	}
	public void setC6id(String c6id) {
		this.c6id = c6id;
	}
	public String getC7() {
		return c7;
	}
	public void setC7(String c7) {
		this.c7 = c7;
	}
	public String getC7id() {
		return c7id;
	}
	public void setC7id(String c7id) {
		this.c7id = c7id;
	}
	public String getC8() {
		return c8;
	}
	public void setC8(String c8) {
		this.c8 = c8;
	}
	public String getC8id() {
		return c8id;
	}
	public void setC8id(String c8id) {
		this.c8id = c8id;
	}
	public String getC10() {
		return c10;
	}
	public void setC10(String c10) {
		this.c10 = c10;
	}
	public String getC10id() {
		return c10id;
	}
	public void setC10id(String c10id) {
		this.c10id = c10id;
	}
	public String getC11() {
		return c11;
	}
	public void setC11(String c11) {
		this.c11 = c11;
	}
	public String getC11id() {
		return c11id;
	}
	public void setC11id(String c11id) {
		this.c11id = c11id;
	}
	public String getC12() {
		return c12;
	}
	public void setC12(String c12) {
		this.c12 = c12;
	}
	public String getC12id() {
		return c12id;
	}
	public void setC12id(String c12id) {
		this.c12id = c12id;
	}
	public String getC9() {
		return c9;
	}
	public void setC9(String c9) {
		this.c9 = c9;
	}
	public String getC9id() {
		return c9id;
	}
	public void setC9id(String c9id) {
		this.c9id = c9id;
	}
}
