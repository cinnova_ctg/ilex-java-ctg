package com.mind.common;

public class StoreWOArrayContents {

	public static final int CERTIFICATION_SHEET = 1;
	public static final int NETWORK_SHEET = 2;
	public static final int HEADER_ROW_DATA = 3;
	public static final int DESC_WR_DATA = 4;
	public static final int HEADER_DATA_1 = 5;
	public static final int HEADER_DATA = 6;
	public static final int ADDRESS_DATA = 7;
	public static final int ADDRESS_DATA_1 = 8;
	public static final int SITE_ADDRESS_DATA = 9;
	public static final int SITE_ADDRESS_DATA_BRAND = 10;
	public static final int SCHE_INFO_DATA = 11;
	public static final int SITE_CONTACT_DATA = 12;
	public static final int SITE_CONTACT_DATA_1 = 13;
	public static final int DOLLAR_GENERAL = 14;
	public static final int REGISTER = 15;
	public static final int CASH_SURVEY_REPORT = 16;
	public static final int INSTALLATION_CHECKLIST = 17;
	public static final int EXPECTATION_VERBIAGE = 18;

	public static String[] getArrayContents(int arrayType) {
		String returnArray[] = null;

		switch (arrayType) {

		case HEADER_ROW_DATA: {
			String[] HeaderRowData = {
					"WORK \n ORDER", // 0
					"PROJECT NAME:", // 1
					"REQUIRED ARRIVAL DATE & TIME", // 2
					"REFERENCE PO#", // 3
					"JOB SITE INFORMATION", // 4
					"CUSTOMER INFORMATION", // 5
					"CERTIFIED PARTNER INFORMATION", // 6
					"BEHAVIORAL GUIDELINES WHILE ON SITE", // 7
					"DESCRIPTION OF WORK REQUESTED", // 8
					"NATURE OF THE ACTIVITY", // 9
					"SPECIAL INSTRUCTIONS (additional detail may be attached herewith)", // 10
					"MATERIAL & OTHER ITEMS USED - PLEASE COMPLETE***", // 11
					"SCHEDULE AND RELEASE INFORMATION - PLEASE COMPLETE***", // 12
					"SITE CONTACT CERTIFICATION & QUESTIONNAIRE", // 13
					"PRIOR TO DEPARTURE PLEASE FAX COMPLETED AND SIGNED WORK ORDERS TO (513) 860-2105", // 14
					"FORWARD ALL QUESTIONS AND \nESCALATIONS TO CONTINGENT \n(800) 506-9609 and press 6 or via \nemail: dispatch@contingent.net", // 15
					"WORK ORDER ISSUE DATE", // 16
					"WORK ORDER:", // 17
					"Troubleshoot ethernet at register 6", // 18
					"PRIOR TO DEPARTURE PLEASE FAX COMPLETED AND SIGNED WORK ORDERS TO ", // 19
					"Failure of Contractor/Certified Partner to arrive at designated job site on or before required arrival date & time, failure to be properly equipped with appropriate tools or written documentation 		   expressly requested or reasonably expected by Contingent or behavior contrary to professional decorum may result in the cancellation of this WO, the associated PO and subsequently any compensation for the Contractor/Certified Partner.", // 20
					"Created By", // 21
					"Project Manager", // 22
					"Facsimile", // 23
					"Company Name", // 24
					"Address", // 25
					"City, State, Zip", // 26
					"Point of Contact", // 27
					"TICKET #:", // 28
					"888-556-0932", // 29
					"WORKMANSHIP AND STANDARDS",// 30
					"DELIVERABLES" // 31
			};
			returnArray = HeaderRowData;
			break;
		}

		case DESC_WR_DATA: {
			String[] DescWRData = {
					"Please call C.J. with Contingent upon arrival on site at 1-800-506-9609 x464.", // 0
					"Tech must have buttsett, please do not call Abercrombie & Fitch for any reason.", // 1
					"Scope of Work:", // 2
					"Assumptions:", // 3
					"Tech must receive release # from C.J. with Contingent before leaving site." // 4
			};

			returnArray = DescWRData;
			break;
		}

		case HEADER_DATA_1: {
			String[] HeaderData1 = { "Activity", "Part Name", "Part Number",
					"Quantity" };
			returnArray = HeaderData1;
			break;
		}

		case HEADER_DATA: {
			String[] HeaderData = { "LINE ITEM", "ITEM DESCRIPTION", "QTY",
					"EST UNIT PRICE", "EST TOTAL PRICE", "PURPOSE" };
			returnArray = HeaderData;
			break;
		}

		case ADDRESS_DATA: {
			String[] AddressData = { "Company Name", "Address",
					"City, State, Zip", "Telephone, Facsimile",
					"Point of Contact" };
			returnArray = AddressData;
			break;
		}

		case ADDRESS_DATA_1: {
			String[] AddressData1 = { "Contingent Network Services, LLC",
					"4400 Port Union Road", "West Chester, OH 45011",
					"(800) 506-9609 (513) 860-2105", "CJ Herron" };
			returnArray = AddressData1;
			break;
		}

		case SITE_ADDRESS_DATA: {
			String[] SiteAddressData = { "Client Name", "Site Name",
					"Work Location", "Site Number", "Address",
					"City, State, Zip" };
			returnArray = SiteAddressData;
			break;
		}

		case SITE_ADDRESS_DATA_BRAND: {
			String[] SiteAddressDataBrand = { "Client Name", "Brand",
					"Work Location", "Site Number", "Address",
					"City, State, Zip" };
			returnArray = SiteAddressDataBrand;
			break;
		}

		case SCHE_INFO_DATA: {
			String[] ScheInfoData = {
					"ARRIVAL DATE___________ARRIVAL TIME_________DEPART DATE_____________DEPART TIME_________CNS RELEASE NUMBE_____________________________",
					"TECHNICIAN/ENGINEER NAME ________________________________________________________MOBILE TELEPHONE NUMBER______________________________",
					"TECHNICIAN/ENGINEER SIGNATURE ___________________________________________________________________DATE_________________________________" };
			returnArray = ScheInfoData;
			break;
		}

		case SITE_CONTACT_DATA: {
			String[] SiteContactData = {
					"THANK YOU FOR TAKING A MOMENT	TO TELL US HOW WE DID",
					"",
					"Job completed satisfactorily?     ",
					"Professional/courteous?             ",
					"Would you like us to follow up    \nregarding this job?                      ",
					"Yes    	No  " };
			returnArray = SiteContactData;
			break;
		}

		case SITE_CONTACT_DATA_1: {
			String[] SiteContactData1 = {
					"Undersigned authorized representative certifies that the above work has been completed and that, in the course of performing such work, no damage has occurred to subject property or any of its contents.",
					"AUTHORIZED SITE CONTACT NAME_________________________________________________________________",
					"AUTHORIZED SITE CONTACT SIGNATURE ___________________________________DATE___________________",
					"ADDITIONAL COMMENTS __________________________________________________________________________" };
			returnArray = SiteContactData1;
			break;
		}

		case DOLLAR_GENERAL: {
			String[] DollarGeneral = {
					" Field Sheet - ",
					"Date: ___________________",
					"Arrival Time: ________________",
					"Checked Store Number on New Server and successfully completed File Transfer (Step1-6)",
					"Server Installation Complete and Scanner successfully Perform price Inquiry (Step 7)",
					"Register 2 installation complete and scanner successfully performs price Inquiry (Step 8)",
					"Register 3 installation complete and scanner successfully performs price Inquiry (Step 8)",
					"Register 4 installation complete and scanner successfully performs price Inquiry (Step 8)",
					"Old Register processing units packed and ready for removal.",
					"Manager Satisfied (please circle):                         Yes           No",
					"Departure Time: _______________________             Amount of Time Spent Onsite: _________________",
					"*** MANAGER SIGNOFF ***",
					"YOUR SIGNATURE HERE INDICATES THAT YOU ARE HAPPY WITH THE INSTALLATION, AND YOUR STORE HAS ALL REGISTERS UPGRADED AND RUNNING WITH NO ISSUES. IF NOT, INDICATE ALL SUCH ISSUES BELOW.",
					"Store Manager Signature: _______________________________________________________________________ ",
					"Store Manager Name Printed: _______________________________________________________________________ ",
					"Issues or Notes: _______________________________________________________________________ ",
					"_______________________________________________________________________________________",
					"Technician Signature: _______________________________________________________________________ ",
					"Technician Name Printed: _______________________________________________________________________ ",
					"Tech / Store Notes: ___________________________________________________________________________________ ",
					"_______________________________________________________________________________________",
					"Fax completed form to 888-556-0932. (Ask manager if you can use the store fax to send.)" };
			returnArray = DollarGeneral;
			break;
		}

		case REGISTER: {
			String[] Register = {
					"Register #1:        Old Serial Number: ___________________________\n                     "
							+ "							      New Serial Number: ___________________________",
					"Register #2:        Old Serial Number: ___________________________\n                     "
							+ "							      New Serial Number: ___________________________",
					"Register #3:        Old Serial Number: ___________________________\n                     "
							+ "							      New Serial Number: ___________________________",
					"Register #4:        Old Serial Number: ___________________________\n                     "
							+ "						    	  New Serial Number: ___________________________" };
			returnArray = Register;
			break;
		}

		case CASH_SURVEY_REPORT: {
			String[] CashSurveyReport = { "Manufacturer", "Model Number",
					"Serial Number", "OLD Pin Pad", "Register Scanner",
					"Phone", "Register", "NEW Pin Pad",
					"Cash Register Printer", "Other\n(Specify)" };
			returnArray = CashSurveyReport;
			break;
		}

		case CERTIFICATION_SHEET: {
			String[] certificationSheet = {
					"Installation Review Document (IRD)", // 0
					"Work with the Dealership Service Manager to complete this form. ", // 1
					"Dealer Name: 	", // 2
					"Dealer Code: ", // 3
					"Date: _____________", // 4
					"Installation Status", // 5
					"Technician Name: _______________________________________  ", // 6
					"Phone # _____________", // 7
					"Start Time:  ______:______  ", // 8
					"End Time:  ______:______      ", // 9
					"# wiTECH VCI PODs: ______      ", // 10
					"# StarMOBILEs: __________", // 11
					"Please check one:", // 12
					"INSTALL COMPLETE, AND ALL TESTS PASS.", // 13
					"INSTALL COMPLETE, BUT SOME TESTS FAILED.", // 14
					"INSTALL COMPLETE, BUT WITH PROBLEM(S).", // 15
					"INSTALL INCOMPLETE OR PARTIALLY COMPLETE.  PLEASE COMPLETE THE SECTION BELOW.", // 16
					"If the installation could not be completed, please identify the cause:", // 17
					"MISSING OR INOPERATIVE WITECH HARDWARE", // 18
					"WAS THIS A MAJOR COMPONENT - ACCESS POINT OR VCI POD? ", // 19
					"SCHEDULING ISSUE ", // 20
					"DEALER INITATED RESCHEDULE", // 21
					"TECHNICIAN NOT ON-SITE FOR SPECIFIED APPOINTMENT TIME", // 22
					"OTHER.  DESCRIBE: _________________________________________________________________________________________________________", // 23
					"If the installation could not be completed, please answer the following questions:", // 24
					"WERE COMPONENT PACKAGES LEFT UNOPENED? ", // 25
					"WAS THE INSTALLATION PROBLEM ESCALATED TO wiTECH SUPPORT (1-888-948-3241?) ", // 26
					"Please provide a detailed explanation of the reason for the incomplete or partial install:\n"
							+ "____________________________________________________________________________________________________\n\n____________________________________________________________________________________________________", // 27
					"wiTECH Access Gateway Mounting Exception", // 28
					"If the wiTECH Access Gateway was mounted in a location other than where the Installation Technician had recommended, please obtain sign-off from the Service Manager.", // 29
					"I acknowledge that I have requested the wiTECH Access Gateway to me mounted in a location other than that recommended by the Installation Technician.", // 30
					"SIGNATURE _____________________________________________", // 31
					"Service Manager Release of Installer", // 32
					"SERVICE MANAGER NAME (PLEASE PRINT) ", // 33
					"Fax to (866) 544 - 0230 ", // 34
					"use black ink.", // 35
					"Handler Name: __________________________________", // 36
					"FOR ANY FAILED TESTS OR INCOMPLETE INSTALL, ESCALATE ISSUE TO THE WITECH HELPDESK (1-888-948-3241).", // 37
					"HELPDESK TICKET# ___________________ AGENT _____________________", // 38
					"FAX ESCALATED IRD TO (248) 922-3896", // 39
			};
			returnArray = certificationSheet;
			break;
		}

		case NETWORK_SHEET: {
			String[] showNetworkConfig = {
					"Static IP address for WiTech Access Gateway",
					"Subnet Mask", "Default Gateway Address", "Primary DNS",
					"Secondary DNS", "Network ID", "Broadcast ID",
					"IP Addresses", "Service Manager", "IT Administrator" };
			returnArray = showNetworkConfig;
			break;
		}

		case INSTALLATION_CHECKLIST: {
			String[] installationChecklist = {
					"Service Manager's wiTECH Installation Checklist", // 0
					"The wiTECH installer obtained my approval to install the Access Gateway in the location he recommended.", // 1
					"All my registered PC's can view all my wiTECH VCI Pods and registered StarMOBILE devices in the wiTECH Diagnostic Application's discovery screen.", // 2
					"The wiTECH installer demonstrated the wiTECH Diagnostic Application when connected to a vehicle using a wiTECH VCI Pod.  This demonstration included an overview of the Home screen, demonstration of DealerCONNECT connectivity, and overview of the integrated help system. ", // 3
					"My installer was courteous and professional and cleaned up the area after completing the wiTECH installation.", // 4
					"I have spoken with the wiTECH installation project coordinator and have discussed any concerns I have about the installation visit.  The wiTECH installation project coordinator has also guided me through this checklist to my satisfaction.", // 5
					"I am satisfied overall with the wiTECH system installation.", // 6

			};
			returnArray = installationChecklist;
			break;
		}

		case EXPECTATION_VERBIAGE: {
			String[] verbiagePoints = {
					"a.  Preparation", // 0
					" - You are expected to thoroughly review all documentation pertaining to all assigned jobs, BEFORE arriving at job site.", // 1
					" - You are expected to bring a printed copy of the Work Order, and job specific Scope/Script documentation. ", // 2
					" - Schedule adequate time to complete the job.", // 3
					"b.  Dress and Grooming", // 4
					" - Be properly groomed", // 5
					" - Khakis or fitted jeans without holes or tears", // 6
					" - Work shoes or boots", // 7
					" - Shirt with a collar (golf or polo style)", // 8
					" - Shirt should be tucked in", // 9
					" - No shirts with logos", // 10
					" - No shorts", // 11
					" - No Flip Flops or Sandals", // 12
					" - No tattered or torn or dirty or ill-fitting clothes or shoes", // 13
					"c.  Be Courteous ", // 14
					" - On Time arrival is required.", // 15
					" - Pleasantries and greetings are expected, but additional conversation should be kept to a minimum.", // 16
					"d.  What not to say to the End Client (in person or on the phone)", // 17
					" - \"I do not know what I am doing\"", // 1
					" - \"This is the first time I have done this\"", // 19
					" - \"I was just assigned this job and haven't had a chance to review the scope\"", // 20
					" - \"I only have limited availability to complete this  assignment\"", // 21
					" - \"The documentation isn't clear\"", // 22
					" - \"The tech onsite before me really messed things up, or didn't do a good job\"", // 23
					"- \"The work area is dirty or is a mess\"", // 24
					"    ***These are all things you should discuss with your Contingent Project Coordinator (outside of the end clients listening range)", // 25
					"e.  Things you should only discuss with your Project Coordinator (never within listening range of the end client)", // 26
					" - Questions about the Scope", // 27
					" - Approval for a Change Order or any other cost/payment related  items (the end client cannot approve these items)", // 28
					" - Anything derogatory about site conditions.", // 29
					" - Anything that is recommended for future improvements.", // 30
					" - Questions about removing equipment. Never remove equipment without approval from Contingent Project Coordinator", // 31
					" - Anything that could be identified as the cause of the current issue, or reason for the current project.", // 32
					"What contingent expects of field resources" // 33
			};
			returnArray = verbiagePoints;
			break;
		}

		}

		return returnArray;
	}
}
