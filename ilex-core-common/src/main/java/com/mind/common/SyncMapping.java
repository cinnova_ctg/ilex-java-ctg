package com.mind.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.mind.fw.core.dao.util.DBUtil;

public class SyncMapping {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SyncMapping.class);

	public static boolean checkRecordExists(String tableName,
			SyncForm syncform, Connection docMConn) {
		// Connection docMConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean result = false;
		String sql = null;
		sql = getPrepareStatementQuery(tableName);
		try {
			pstmt = docMConn.prepareStatement(sql);
			setPrepareStatementParameters(pstmt, tableName, syncform);
			rs = pstmt.executeQuery();
			if (rs.next())
				result = true;
			else
				result = false;
		} catch (Exception e) {
			logger.error("checkRecordExists(String, SyncForm, Connection)", e);

			logger.error("checkRecordExists(String, SyncForm, Connection)", e);
		} finally {
			DBUtil.closePreparedStatement(rs, pstmt);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("checkRecordExists(String, SyncForm, Connection) - Result is ::"
					+ result);
		}
		return result;
	}

	public static void setPrepareStatementParameters(PreparedStatement pstmt,
			String tableName, SyncForm syncform) {
		try {
			if (tableName.trim().equalsIgnoreCase("lo_organization_type")) {
				pstmt.setString(1, syncform.getOrgType());
			} else if (tableName.trim().equalsIgnoreCase(
					"lo_organization_discipline")) {
				pstmt.setString(1, syncform.getOrgDispId());
			} else if (tableName.trim().equalsIgnoreCase("lo_role")) {
				pstmt.setString(1, syncform.getRoleId());
			} else if (tableName.trim().equalsIgnoreCase("cc_function")) {
				pstmt.setString(1, syncform.getFunctionIdDocM());
			} else if (tableName.trim().equalsIgnoreCase("lo_role_privilege")) {
				pstmt.setString(1, syncform.getRoleId());
				pstmt.setString(2, syncform.getFunctionId());
			} else if (tableName.trim().equalsIgnoreCase("lo_poc")) {
				pstmt.setString(1, syncform.getLoPcId());
			} else if (tableName.trim().equalsIgnoreCase("lo_poc_rol")) {
				pstmt.setString(1, syncform.getLoPcId());
				pstmt.setString(2, syncform.getRoleId());
			} else if (tableName.trim().equalsIgnoreCase("lo_user")) {
				pstmt.setString(1, syncform.getLoPcId());
			}
		} catch (Exception e) {
			logger.error(
					"setPrepareStatementParameters(PreparedStatement, String, SyncForm)",
					e);

			logger.error(
					"setPrepareStatementParameters(PreparedStatement, String, SyncForm)",
					e);
		}
	}

	public static String getPrepareStatementQuery(String tableName) {
		String sql = null;
		if (tableName.trim().equalsIgnoreCase("lo_organization_type"))
			sql = "select 1 from dm_lo_organization_type where dm_lo_organization_type_ilexkey = ? ";
		else if (tableName.trim()
				.equalsIgnoreCase("lo_organization_discipline"))
			sql = "select 1 from dm_lo_organization_discipline where dm_lo_od_ilexkey = ?";
		else if (tableName.trim().equalsIgnoreCase("lo_role"))
			sql = "select 1 from dm_lo_role where dm_lo_ro_ilexkey = ?";
		else if (tableName.trim().equalsIgnoreCase("cc_function"))
			sql = "select 1 from dm_cc_function where dm_cc_fu_id = ?";
		else if (tableName.trim().equalsIgnoreCase("lo_role_privilege"))
			sql = "select 1 from dm_lo_role_privilege where dm_lo_rp_ro_id = ? and dm_lo_rp_fu_id = ?";
		else if (tableName.trim().equalsIgnoreCase("lo_poc"))
			sql = "select 1 from dm_lo_poc where dm_lo_pc_ilexkey = ?";
		else if (tableName.trim().equalsIgnoreCase("lo_poc_rol"))
			sql = "select 1 from dm_lo_poc_rol where dm_lo_pr_pc_id = ? and dm_lo_pr_ro_id = ?";
		else if (tableName.trim().equalsIgnoreCase("lo_user"))
			sql = "select 1 from dm_lo_user where dm_lo_us_ilexkey = ?";
		if (logger.isDebugEnabled()) {
			logger.debug("getPrepareStatementQuery(String) - " + sql);
		}
		return sql;
	}

	public static String getDocMKeyCorrespondingToIlexKey(String tableName,
			String originalKey, Connection docMConn) {
		// Connection docMConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String docMKey = null;
		String sql = sqlForDocMKey(tableName);
		try {
			pstmt = docMConn.prepareStatement(sql);
			getPrepareStatmentForDoCMKey(pstmt, originalKey, tableName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				docMKey = rs.getString(1); // get the organization discipline id
											// for the lo role table from
											// discipline table of DocM.
			}
		} catch (Exception e) {
			logger.error(
					"getDocMKeyCorrespondingToIlexKey(String, String, Connection)",
					e);

			logger.error(
					"getDocMKeyCorrespondingToIlexKey(String, String, Connection)",
					e);
		} finally {
			DBUtil.closePreparedStatement(rs, pstmt);
		}
		return docMKey;
	}

	public static String sqlForDocMKey(String tableName) {
		String sql = null;
		if (tableName.trim().equalsIgnoreCase("lo_organization_type"))
			sql = "select dm_lo_organization_type from dm_lo_organization_type where dm_lo_organization_type_ilexkey = ?";
		else if (tableName.trim().equalsIgnoreCase("lo_role"))
			sql = "select dm_lo_od_id from dm_lo_organization_discipline where dm_lo_od_ilexkey = ?";
		else if (tableName.trim().equalsIgnoreCase("lo_role_privilegeR"))
			sql = "select dm_lo_ro_id from dm_lo_role where dm_lo_ro_ilexkey = ?";
		else if (tableName.trim().equalsIgnoreCase("lo_role_privilegeF"))
			sql = "select dm_cc_fu_id from dm_cc_function where dm_cc_fu_ilexkey = ?";
		else if (tableName.trim().equalsIgnoreCase("lo_poc_rolP"))
			sql = "select dm_lo_pc_id  from dm_lo_poc where dm_lo_pc_ilexkey = ?";
		else if (tableName.trim().equalsIgnoreCase("lo_poc_rolR"))
			sql = "select dm_lo_ro_id from dm_lo_role where dm_lo_ro_ilexkey = ?";
		else if (tableName.trim().equalsIgnoreCase("lo_user"))
			sql = "select dm_lo_pc_id  from dm_lo_poc where dm_lo_pc_ilexkey = ?";
		return sql;
	}

	public static void getPrepareStatmentForDoCMKey(PreparedStatement pstmt,
			String key, String tableName) {
		try {
			// if(tableName.trim().equalsIgnoreCase("lo_organization_type"))
			pstmt.setString(1, key);
			// else if(tableName.trim().equalsIgnoreCase("lo_role"))
			// pstmt.setString(1,key);
			// else if (tableName.trim().equalsIgnoreCase("lo_role_privilegeR"))
			// pstmt.setString(1,key);
			// else if (tableName.trim().equalsIgnoreCase("lo_role_privilegeF"))
			// pstmt.setString(1,key);
		} catch (Exception e) {
			logger.error(
					"getPrepareStatmentForDoCMKey(PreparedStatement, String, String)",
					e);

			logger.error(
					"getPrepareStatmentForDoCMKey(PreparedStatement, String, String)",
					e);
		}
	}

}
