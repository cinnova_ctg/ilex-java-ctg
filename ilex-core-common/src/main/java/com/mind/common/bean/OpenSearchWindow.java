package com.mind.common.bean;

public class OpenSearchWindow 
{
	private String resource_id = null;
	private String resource_name = null;
	private String manufacturer_part_number = null;
	private String cns_part_number = null;
	private String unit_cost = null;
	private String sellable_quantity = null;
	private String minimum_quantity = null;
	private String alternate_identifier = null;
	
	
	public String getAlternate_identifier() {
		return alternate_identifier;
	}
	public void setAlternate_identifier( String alternate_identifier ) {
		this.alternate_identifier = alternate_identifier;
	}
	
	
	public String getCns_part_number() {
		return cns_part_number;
	}
	public void setCns_part_number( String cns_part_number ) {
		this.cns_part_number = cns_part_number;
	}
	
	
	public String getManufacturer_part_number() {
		return manufacturer_part_number;
	}
	public void setManufacturer_part_number( String manufacturer_part_number ) {
		this.manufacturer_part_number = manufacturer_part_number;
	}
	
	
	public String getMinimum_quantity() {
		return minimum_quantity;
	}
	public void setMinimum_quantity( String minimum_quantity ) {
		this.minimum_quantity = minimum_quantity;
	}
	
	
	public String getResource_id() {
		return resource_id;
	}
	public void setResource_id( String resource_id ) {
		this.resource_id = resource_id;
	}
	
	
	public String getResource_name() {
		return resource_name;
	}
	public void setResource_name( String resource_name ) {
		this.resource_name = resource_name;
	}
	
	
	public String getSellable_quantity() {
		return sellable_quantity;
	}
	public void setSellable_quantity( String sellable_quantity ) {
		this.sellable_quantity = sellable_quantity;
	}
	
	
	public String getUnit_cost() {
		return unit_cost;
	}
	public void setUnit_cost( String unit_cost ) {
		this.unit_cost = unit_cost;
	}
	
	
	
	
}
