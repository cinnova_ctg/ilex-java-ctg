package com.mind.common.bean;

import java.io.Serializable;

public class Schedule implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String scheduled_need_date = "";
	private String scheduled_effective_date = "";
	private String scheduled_planned_days = "";
	
	
	public String getScheduled_effective_date() {
		return scheduled_effective_date;
	}
	public void setScheduled_effective_date(String scheduled_effective_date) {
		this.scheduled_effective_date = scheduled_effective_date;
	}
	public String getScheduled_need_date() {
		return scheduled_need_date;
	}
	public void setScheduled_need_date(String scheduled_need_date) {
		this.scheduled_need_date = scheduled_need_date;
	}
	public String getScheduled_planned_days() {
		return scheduled_planned_days;
	}
	public void setScheduled_planned_days(String scheduled_planned_days) {
		this.scheduled_planned_days = scheduled_planned_days;
	}
}
