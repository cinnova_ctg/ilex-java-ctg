package com.mind.common.bean;

import java.io.Serializable;

public class Fileinfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String file_id = null;
	private String file_name = null;
	private String file_uploaded_date = null;
	private String file_uploaded_by = null;
	private String file_remarks = null;
	private byte[] file_data = null;
	private String file_ext = null;
	private String file_status = null;

	public String getFile_id() {
		return file_id;
	}

	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getFile_remarks() {
		return file_remarks;
	}

	public void setFile_remarks(String file_remarks) {
		this.file_remarks = file_remarks;
	}

	public String getFile_uploaded_by() {
		return file_uploaded_by;
	}

	public void setFile_uploaded_by(String file_uploaded_by) {
		this.file_uploaded_by = file_uploaded_by;
	}

	public String getFile_uploaded_date() {
		return file_uploaded_date;
	}

	public void setFile_uploaded_date(String file_uploaded_date) {
		this.file_uploaded_date = file_uploaded_date;
	}

	public byte[] getFile_data() {
		return file_data;
	}

	public void setFile_data(byte[] file_data) {
		this.file_data = file_data;
	}

	public String getFile_ext() {
		return file_ext;
	}

	public void setFile_ext(String file_ext) {
		this.file_ext = file_ext;
	}

	public String getFile_status() {
		return file_status;
	}

	public void setFile_status(String file_status) {
		this.file_status = file_status;
	}

}
