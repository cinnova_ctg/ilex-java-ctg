package com.mind.common.bean;

public class Assumption 
{
	private String assumption_Id = null;
	private String id = null;
	private String assumption = null;
	private String type = null;
	
	
	public String getAssumption() {
		return assumption;
	}
	public void setAssumption( String assumption ) {
		this.assumption = assumption;
	}
	
	
	public String getAssumption_Id() {
		return assumption_Id;
	}
	public void setAssumption_Id( String assumption_Id ) {
		this.assumption_Id = assumption_Id;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId( String id ) {
		this.id = id;
	}
	
	
	public String getType() {
		return type;
	}
	public void setType( String type ) {
		this.type = type;
	}
}

