package com.mind.common.bean;

import java.io.Serializable;

public class POCNameEmail implements Serializable

{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name=null;
	private String email=null;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}



