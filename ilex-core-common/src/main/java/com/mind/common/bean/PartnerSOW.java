package com.mind.common.bean;

public class PartnerSOW {
	private String pfSow_Id = null;
	private String pfId = null;
	private String pfSow = null;
	private String pfType = null;
	
	
	public String getPfId() {
		return pfId;
	}
	public void setPfId(String pfId) {
		this.pfId = pfId;
	}
	public String getPfSow() {
		return pfSow;
	}
	public void setPfSow(String pfSow) {
		this.pfSow = pfSow;
	}
	public String getPfSow_Id() {
		return pfSow_Id;
	}
	public void setPfSow_Id(String pfSow_Id) {
		this.pfSow_Id = pfSow_Id;
	}
	public String getPfType() {
		return pfType;
	}
	public void setPfType(String pfType) {
		this.pfType = pfType;
	}
	
}

