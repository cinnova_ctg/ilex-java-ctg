package com.mind.common.bean;

public class Sow 
{
	private String sow_Id = null;
	private String id = null;
	private String sow = null;
	private String type = null;
	
	
	
	
	public String getSow_Id() {
		return sow_Id;
	}
	public void setSow_Id( String sow_Id ) {
		this.sow_Id = sow_Id;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId( String id ) {
		this.id = id;
	}
	
	
	public String getSow() {
		return sow;
	}
	public void setSow( String sow ) {
		this.sow = sow;
	}
	
	
	public String getType() {
		return type;
	}
	public void setType( String type ) {
		this.type = type;
	}
}

