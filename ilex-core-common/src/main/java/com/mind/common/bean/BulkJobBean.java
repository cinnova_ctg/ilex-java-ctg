package com.mind.common.bean;

public class BulkJobBean {
	
	private String activity = null;
	private String type = null;
	private String qty = null;
	private String estimated_total_cost = null;
	private String extended_sub_total = null;
	private String extended_Price = null;
	private String proforma_Margin = null;
	private String activity_id = null;
	private String libraryId = null;
	private String tempjob_id = null;
	private String tempjob_name = null;
	
	private String temp_activity_id=null;
	
	
	
	public String getTemp_activity_id() {
		return temp_activity_id;
	}
	public void setTemp_activity_id(String temp_activity_id) {
		this.temp_activity_id = temp_activity_id;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public String getEstimated_total_cost() {
		return estimated_total_cost;
	}
	public void setEstimated_total_cost(String estimated_total_cost) {
		this.estimated_total_cost = estimated_total_cost;
	}
	public String getExtended_Price() {
		return extended_Price;
	}
	public void setExtended_Price(String extended_Price) {
		this.extended_Price = extended_Price;
	}
	public String getExtended_sub_total() {
		return extended_sub_total;
	}
	public void setExtended_sub_total(String extended_sub_total) {
		this.extended_sub_total = extended_sub_total;
	}
	public String getProforma_Margin() {
		return proforma_Margin;
	}
	public void setProforma_Margin(String proforma_Margin) {
		this.proforma_Margin = proforma_Margin;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getActivity_id() {
		return activity_id;
	}
	public void setActivity_id(String activity_id) {
		this.activity_id = activity_id;
	}
	public String getLibraryId() {
		return libraryId;
	}
	public void setLibraryId(String libraryId) {
		this.libraryId = libraryId;
	}
	public String getTempjob_id() {
		return tempjob_id;
	}
	public void setTempjob_id(String tempjob_id) {
		this.tempjob_id = tempjob_id;
	}
	public String getTempjob_name() {
		return tempjob_name;
	}
	public void setTempjob_name(String tempjob_name) {
		this.tempjob_name = tempjob_name;
	}
	
	
	
	

}
