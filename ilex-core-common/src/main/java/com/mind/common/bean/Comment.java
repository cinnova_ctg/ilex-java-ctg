package com.mind.common.bean;

import java.io.Serializable;

public class Comment implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String comment = null;
	private String commentby = null;
	private String commentdate = null;
	
	public String getComment() {
		return comment;
	}
	public void setComment( String comment ) {
		this.comment = comment;
	}
	public String getCommentby() {
		return commentby;
	}
	public void setCommentby(String commentby) {
		this.commentby = commentby;
	}
	public String getCommentdate() {
		return commentdate;
	}
	public void setCommentdate(String commentdate) {
		this.commentdate = commentdate;
	}
	
	
	
}
