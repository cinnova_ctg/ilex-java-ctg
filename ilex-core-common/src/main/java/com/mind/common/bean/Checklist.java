package com.mind.common.bean;

public class Checklist 
{
	private String checklistid = null;
	private String itemname = null;
	private String checklisttype = null;
	private String checklistidtype = null;
	private String groupname = null;
	private String option = null;
	private String optiontype = null;
	
	public String getChecklistid() {
		return checklistid;
	}

	public void setChecklistid( String checklistid ) {
		this.checklistid = checklistid;
	}

	
	
	/*public String getChecklistname() {
		return checklistname;
	}

	public void setChecklistname( String checklistname ) {
		this.checklistname = checklistname;
	}

	
	public String getItemdesc() {
		return itemdesc;
	}

	public void setItemdesc( String itemdesc ) {
		this.itemdesc = itemdesc;
	}
	*/
	
	public String getItemname() {
		return itemname;
	}

	public void setItemname( String itemname ) {
		this.itemname = itemname;
	}

	public String getChecklisttype() {
		return checklisttype;
	}

	public void setChecklisttype(String checklisttype) {
		this.checklisttype = checklisttype;
	}

	public String getChecklistidtype() {
		return checklistidtype;
	}

	public void setChecklistidtype(String checklistidtype) {
		this.checklistidtype = checklistidtype;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public String getOptiontype() {
		return optiontype;
	}

	public void setOptiontype(String optiontype) {
		this.optiontype = optiontype;
	}
	
	
	
	
}
