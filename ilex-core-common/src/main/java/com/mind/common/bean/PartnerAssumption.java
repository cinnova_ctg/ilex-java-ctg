package com.mind.common.bean;

public class PartnerAssumption {
	private String pfAssumption_Id = null;
	private String pfId = null;
	private String pfAssumption = null;
	private String pfType = null;
	
	
	public String getPfAssumption() {
		return pfAssumption;
	}
	public void setPfAssumption(String pfAssumption) {
		this.pfAssumption = pfAssumption;
	}
	public String getPfAssumption_Id() {
		return pfAssumption_Id;
	}
	public void setPfAssumption_Id(String pfAssumption_Id) {
		this.pfAssumption_Id = pfAssumption_Id;
	}
	public String getPfId() {
		return pfId;
	}
	public void setPfId(String pfId) {
		this.pfId = pfId;
	}
	public String getPfType() {
		return pfType;
	}
	public void setPfType(String pfType) {
		this.pfType = pfType;
	}
	
	
	
}

