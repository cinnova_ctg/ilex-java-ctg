package com.mind.common;

import java.io.Serializable;

public class CMSearchList implements Serializable {

	private String lo_ot_id = null;
	private String lo_ot_type = null;
	private String lo_ot_name = null;
	private String lo_om_id = null;
	private String lo_om_division = null;
	private String lo_ad_city = null;
	private String lo_ad_state = null;
	private String lo_pc_id = null;
	private String contact = null;
	private String incidentType = null;
	private String status = null;

	public CMSearchList(String lo_ot_id, String lo_ot_type, String lo_ot_name,
			String lo_om_id, String lo_om_division, String lo_ad_city,
			String lo_ad_state, String lo_pc_id, String contact,
			String incidentType, String status) {
		super();
		this.lo_ot_id = lo_ot_id;
		this.lo_ot_type = lo_ot_type;
		this.lo_ot_name = lo_ot_name;
		this.lo_om_id = lo_om_id;
		this.lo_om_division = lo_om_division;
		this.lo_ad_city = lo_ad_city;
		this.lo_ad_state = lo_ad_state;
		this.lo_pc_id = lo_pc_id;
		this.contact = contact;
		this.incidentType = incidentType;
		this.status = status;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getLo_ad_city() {
		return lo_ad_city;
	}

	public void setLo_ad_city(String lo_ad_city) {
		this.lo_ad_city = lo_ad_city;
	}

	public String getLo_ad_state() {
		return lo_ad_state;
	}

	public void setLo_ad_state(String lo_ad_state) {
		this.lo_ad_state = lo_ad_state;
	}

	public String getLo_om_division() {
		return lo_om_division;
	}

	public void setLo_om_division(String lo_om_division) {
		this.lo_om_division = lo_om_division;
	}

	public String getLo_om_id() {
		return lo_om_id;
	}

	public void setLo_om_id(String lo_om_id) {
		this.lo_om_id = lo_om_id;
	}

	public String getLo_ot_id() {
		return lo_ot_id;
	}

	public void setLo_ot_id(String lo_ot_id) {
		this.lo_ot_id = lo_ot_id;
	}

	public String getLo_ot_name() {
		return lo_ot_name;
	}

	public void setLo_ot_name(String lo_ot_name) {
		this.lo_ot_name = lo_ot_name;
	}

	public String getLo_ot_type() {
		return lo_ot_type;
	}

	public void setLo_ot_type(String lo_ot_type) {
		this.lo_ot_type = lo_ot_type;
	}

	public String getLo_pc_id() {
		return lo_pc_id;
	}

	public void setLo_pc_id(String lo_pc_id) {
		this.lo_pc_id = lo_pc_id;
	}

	public String getIncidentType() {
		return incidentType;
	}

	public void setIncidentType(String incidentType) {
		this.incidentType = incidentType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
