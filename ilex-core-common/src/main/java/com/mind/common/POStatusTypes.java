package com.mind.common;

import org.apache.log4j.Logger;




/**
 * @purpose This class is used to represent 
 * 			the purchase order's available statuses 
 */
public class POStatusTypes {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(POStatusTypes.class);
	
	/**
	 * Set on initial creation (copy or build). 
	 * This PO is for internal use.
	 */
	public static final int STATUS_DRAFT=0;
	
	/**
	 * The partner review status marks a trigger point where a PO cannot 
	 * be changed. Formally transmitted to a partner for review/acceptance. 
	 * Document copy is saved to DocM.
	 */
	public static final int STATUS_PARTNER_REVIEW=1;
	
	/**
	 * Formally accepted by the partner.
	 * 
	 * Marking a job 'Onsite' will look at the ideal status of 
	 * all POs is 'Accepted' or as a minimum at least 
	 * one PO in an Accepted status.
	 */
	public static final int STATUS_ACCEPTED=2;
	
	/**
	 * Implies the effort required by PO was completed successfully 
	 * and all requirements satisfied.
	 * 
	 * If a job contains a PO without deliverables, 
	 * marking the job complete will mark the PO complete.
	 */
	public static final int STATUS_COMPLETED=3;
	
	/**
	 * Implies the effort not started or not successfully completed 
	 * per PO/WO. No payable associated with PO.
	 */
	public static final int STATUS_CANCELLED=4;
	
	public static String getStatusName(int statusCode) {
		if(statusCode==STATUS_DRAFT) {
			return "Draft";
		} else if(statusCode==STATUS_PARTNER_REVIEW) {
			return "Partner Review";
		} else if(statusCode==STATUS_ACCEPTED) {
			return "Accepted";
		} else if(statusCode==STATUS_COMPLETED) {
			return "Completed";
		} else if(statusCode==STATUS_CANCELLED) {
			return "Cancelled";
		} else {
			return null;
		}
	}
	
	/**
	 * Retrun PO Status Id on the basis of Status Name.
	 * @param statusName
	 * @return
	 */
	public static int getStatusId(String statusName) {
		try{
			statusName.trim();
		} catch(Exception e){
			logger.error("getStatusId(String)", e);

			return -1;
		}
		
		if(statusName.equals("Draft")) {
			return STATUS_DRAFT;
		} else if(statusName.equals("Partner Review")) {
			return STATUS_PARTNER_REVIEW;
		} else if(statusName.equals("Accepted")) {
			return STATUS_ACCEPTED;
		} else if(statusName.equals("Completed")) {
			return STATUS_COMPLETED;
		} else if(statusName.equals("Cancelled")) {
			return STATUS_CANCELLED;
		} else {
			return -1;
		}
	}
}
