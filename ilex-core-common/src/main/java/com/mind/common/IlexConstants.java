package com.mind.common;

public class IlexConstants {
	public static final String ILEX_DS_NAME = "ilex";
	public static final String ILEX_RPT_NAME = "reporting";
	public static final String CONTINGENT_DS_NAME = "contingent";
	public static final String CONTINGENT_RPT_NAME = "reporting";
	public static String SERVER_NAME = "";
	public static String IMG_REAL_PATH = "";
	public static String ILEX_REQ_URL = "";

	public static final String SYMBOL_TILT = "~";
	public static final String CHARACTER_Y = "Y";

	public static final String W9W9_PDF_BLANK_FILENAME = "blankw9.pdf";
	public static final String W9W9_PDF_BLANK_FILE_LOCATION = "/pdf/blankw9.pdf";
	public static final String W9W9_PDF_BLANK_TEXT_BODY = "\n\nPlease be advised that Contingent does not have your W-9 on file."
			+ " This form is required to process any payments due. \n"
			+ "Please complete and send a signed copy to the Certified Partner Development \n"
			+ " group at cpd@contingent.com or fax to 866-494-4281. \n"
			+ "Please note that all payments will be held until this form is received.";

	public static final String W9W9_SPLIT_EXP = "Sincerely,";
	public static final String W9W9_APPEND_SINCERELY = "<br/></br>Sincerely,";
}
