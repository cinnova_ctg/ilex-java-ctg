package com.mind.common;

import java.io.Serializable;

public class LabelValue implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3545793278051103030L;
	private String label = null;
	private String value = null;

	public LabelValue() {

	}

	public LabelValue(String a, String b) {
		label = a;
		value = b;
	}

	public void setLabel(String l) {
		label = l;
	}

	public String getLabel() {
		return label;
	}

	public void setValue(String v) {
		value = v;
	}

	public String getValue() {
		return value;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof LabelValue) {
			if (this.getLabel().equals(((LabelValue) o).getLabel())
					&& this.getValue().equals(((LabelValue) o).getValue()))
				return true;
		}
		return false;
	}
}
