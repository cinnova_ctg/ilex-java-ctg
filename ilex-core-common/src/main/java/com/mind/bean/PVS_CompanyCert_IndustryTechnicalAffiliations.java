package com.mind.bean;

import java.io.Serializable;

public class PVS_CompanyCert_IndustryTechnicalAffiliations implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tacompany = null;
	private String validFromTA = null;
	private String validToTA = null;
	private String tacompanyName = null;

	public PVS_CompanyCert_IndustryTechnicalAffiliations(String tacompany,
			String fromTA, String toTA, String tacompanyName) {
		this.tacompany = tacompany;
		validFromTA = fromTA;
		validToTA = toTA;
		this.tacompanyName = tacompanyName;
	}

	public PVS_CompanyCert_IndustryTechnicalAffiliations(String tacompany,
			String fromTA, String toTA) {
		this.tacompany = tacompany;
		validFromTA = fromTA;
		validToTA = toTA;
	}

	public PVS_CompanyCert_IndustryTechnicalAffiliations() {
		super();
	}

	public String getTacompany() {
		return tacompany;
	}

	public void setTacompany(String tacompany) {
		this.tacompany = tacompany;
	}

	public String getValidFromTA() {
		return validFromTA;
	}

	public void setValidFromTA(String validFromTA) {
		this.validFromTA = validFromTA;
	}

	public String getValidToTA() {
		return validToTA;
	}

	public void setValidToTA(String validToTA) {
		this.validToTA = validToTA;
	}

	public String getTacompanyName() {
		return tacompanyName;
	}

	public void setTacompanyName(String tacompanyName) {
		this.tacompanyName = tacompanyName;
	}

}
