package com.mind.bean;

import java.io.Serializable;



public class JobCompleteBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String function = null;
	private String jobId = null;
	private String appendixId = null;
	private String type = null;
	private String status = null;
	private String jobType = null;
	private String jobStatus = null;
	private String viewjobtype = null;
	private String ownerId = null;
	private String viewType = null;
	private String appendixName = null;
	private String msaName = null;
	private String msaId = null;
	private String jobName = null;

	private String next = null;
	private String scheduleCheck = null;
	private String installNotesCheck = null;
	private String customerReferenceCheck = null;
	private String outStandingDelieverableCheck = null;
	private String revenue = null;
	private String expense = null;
	private String resourceAllocation = null;
	private String siteContactInfo = null;
	private String overAllStatus = null;

	public String getSiteContactInfo() {
		return siteContactInfo;
	}

	public void setSiteContactInfo(String siteContactInfo) {
		this.siteContactInfo = siteContactInfo;
	}

	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getCustomerReferenceCheck() {
		return customerReferenceCheck;
	}

	public void setCustomerReferenceCheck(String customerReferenceCheck) {
		this.customerReferenceCheck = customerReferenceCheck;
	}

	public String getExpense() {
		return expense;
	}

	public void setExpense(String expense) {
		this.expense = expense;
	}

	public String getInstallNotesCheck() {
		return installNotesCheck;
	}

	public void setInstallNotesCheck(String installNotesCheck) {
		this.installNotesCheck = installNotesCheck;
	}

	public String getOutStandingDelieverableCheck() {
		return outStandingDelieverableCheck;
	}

	public void setOutStandingDelieverableCheck(
			String outStandingDelieverableCheck) {
		this.outStandingDelieverableCheck = outStandingDelieverableCheck;
	}

	public String getResourceAllocation() {
		return resourceAllocation;
	}

	public void setResourceAllocation(String resourceAllocation) {
		this.resourceAllocation = resourceAllocation;
	}

	public String getRevenue() {
		return revenue;
	}

	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}

	public String getScheduleCheck() {
		return scheduleCheck;
	}

	public void setScheduleCheck(String scheduleCheck) {
		this.scheduleCheck = scheduleCheck;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewJobType) {
		this.viewjobtype = viewJobType;
	}

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getOverAllStatus() {
		return overAllStatus;
	}

	public void setOverAllStatus(String overAllStatus) {
		this.overAllStatus = overAllStatus;
	}


}
