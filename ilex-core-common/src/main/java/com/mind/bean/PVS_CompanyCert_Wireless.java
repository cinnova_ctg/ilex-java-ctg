package com.mind.bean;

import java.io.Serializable;

public class PVS_CompanyCert_Wireless implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String wlcompany = null;
	private String validFromWC = null;
	private String validToWC = null;
	private String wlcompanyName = null;

	public PVS_CompanyCert_Wireless(String wlcompany, String fromWC,
			String toWC, String wlcompanyName) {
		this.wlcompany = wlcompany;
		validFromWC = fromWC;
		validToWC = toWC;
		this.wlcompanyName = wlcompanyName;
	}

	public PVS_CompanyCert_Wireless(String wlcompany, String fromWC, String toWC) {
		this.wlcompany = wlcompany;
		validFromWC = fromWC;
		validToWC = toWC;
	}

	public PVS_CompanyCert_Wireless() {
		super();
	}

	public String getValidFromWC() {
		return validFromWC;
	}

	public void setValidFromWC(String validFromWC) {
		this.validFromWC = validFromWC;
	}

	public String getValidToWC() {
		return validToWC;
	}

	public void setValidToWC(String validToWC) {
		this.validToWC = validToWC;
	}

	public String getWlcompany() {
		return wlcompany;
	}

	public void setWlcompany(String wlcompany) {
		this.wlcompany = wlcompany;
	}

	public String getWlcompanyName() {
		return wlcompanyName;
	}

	public void setWlcompanyName(String wlcompanyName) {
		this.wlcompanyName = wlcompanyName;
	}

}
