package com.mind.bean;

import java.io.Serializable;

public class RecRest_History implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name = null;

	private String type = null;
	private String date = null;
	private String createdUser = null;
	private String activity = null;

	public RecRest_History() {
		super();
	}

	public RecRest_History(String name, String type, String date,
			String createdUser, String activity) {

		this.name = name;
		type = type;
		date = date;
		createdUser = createdUser;
		activity = activity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

}
