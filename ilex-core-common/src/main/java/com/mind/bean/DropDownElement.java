package com.mind.bean;

import java.io.Serializable;

public class DropDownElement implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String value;
	String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
