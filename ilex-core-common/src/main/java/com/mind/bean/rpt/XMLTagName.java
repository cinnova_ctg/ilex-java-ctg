package com.mind.bean.rpt;

import java.io.Serializable;

public class XMLTagName implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String dealernumber = "";
	String staticgatewayaddress = "";
	String subnetmask = "";
	String defaultgatewayaddress = "";
	String primarydns = "";
	String secondarydns = "";
	String broadcastid = "";
	String networkid = "";
	String serManagerName = "";
	String serManagerPhone = "";
	String itAdminName = "";
	String itAdminPhone = "";
	String iprange = "";
	String oktoconfig = "";
	String pcData = "";
	
	public String getDealernumber() {
		return dealernumber;
	}
	public void setDealernumber(String dealernumber) {
		this.dealernumber = dealernumber;
	}
	public String getDefaultgatewayaddress() {
		return defaultgatewayaddress;
	}
	public void setDefaultgatewayaddress(String defaultgatewayaddress) {
		this.defaultgatewayaddress = defaultgatewayaddress;
	}
	public String getPrimarydns() {
		return primarydns;
	}
	public void setPrimarydns(String primarydns) {
		this.primarydns = primarydns;
	}
	public String getSecondarydns() {
		return secondarydns;
	}
	public void setSecondarydns(String secondarydns) {
		this.secondarydns = secondarydns;
	}
	public String getStaticgatewayaddress() {
		return staticgatewayaddress;
	}
	public void setStaticgatewayaddress(String staticgatewayaddress) {
		this.staticgatewayaddress = staticgatewayaddress;
	}
	public String getSubnetmask() {
		return subnetmask;
	}
	public void setSubnetmask(String subnetmask) {
		this.subnetmask = subnetmask;
	}
	public String getIprange() {
		return iprange;
	}
	public void setIprange(String iprange) {
		this.iprange = iprange;
	}
	public String getBroadcastid() {
		return broadcastid;
	}
	public void setBroadcastid(String broadcastid) {
		this.broadcastid = broadcastid;
	}
	public String getNetworkid() {
		return networkid;
	}
	public void setNetworkid(String networkid) {
		this.networkid = networkid;
	}
	public String getOktoconfig() {
		return oktoconfig;
	}
	public void setOktoconfig(String oktoconfig) {
		this.oktoconfig = oktoconfig;
	}
	public String getPcData() {
		return pcData;
	}
	public void setPcData(String pcData) {
		this.pcData = pcData;
	}
	public String getSerManagerName() {
		return serManagerName;
	}
	public void setSerManagerName(String serManagerName) {
		this.serManagerName = serManagerName;
	}
	public String getSerManagerPhone() {
		return serManagerPhone;
	}
	public void setSerManagerPhone(String serManagerPhone) {
		this.serManagerPhone = serManagerPhone;
	}
	public String getItAdminName() {
		return itAdminName;
	}
	public void setItAdminName(String itAdminName) {
		this.itAdminName = itAdminName;
	}
	public String getItAdminPhone() {
		return itAdminPhone;
	}
	public void setItAdminPhone(String itAdminPhone) {
		this.itAdminPhone = itAdminPhone;
	}
	
}
