package com.mind.bean.rpt;

import java.io.Serializable;

public class SnaponInfo implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String created = null;
	private String updated = null;
	private String notFound = null;
	private String notFoundsiteNo = null;
	private String foundsiteNo = null;
	
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getFoundsiteNo() {
		return foundsiteNo;
	}
	public void setFoundsiteNo(String foundsiteNo) {
		this.foundsiteNo = foundsiteNo;
	}
	public String getNotFoundsiteNo() {
		return notFoundsiteNo;
	}
	public void setNotFoundsiteNo(String notFoundsiteNo) {
		this.notFoundsiteNo = notFoundsiteNo;
	}
	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	public String getNotFound() {
		return notFound;
	}
	public void setNotFound(String notFounded) {
		this.notFound = notFounded;
	}
	
}
