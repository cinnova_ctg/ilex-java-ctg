package com.mind.bean.cm;

import java.io.Serializable;

public class SearchBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
		private String org=null;
		private String type=null;
		private String division=null;
		private String poc=null;
		private String search=null;
		private String searchtype=null;
		private String org_discipline_id=null;
		private String division_id=null;
		private String minutemanId = null;
		private String certifiedId = null;
		
		
		public String getDivision() {
			return division;
		}
		public void setDivision(String division) {
			this.division = division;
		}
		public String getSearch() {
			return search;
		}
		public void setSearch(String search) {
			this.search = search;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getPoc() {
			return poc;
		}
		public void setPoc(String poc) {
			this.poc = poc;
		}
		public String getOrg() {
			return org;
		}
		public void setOrg(String org) {
			this.org = org;
		}
		public String getSearchtype() {
			return searchtype;
		}
		public void setSearchtype(String searchtype) {
			this.searchtype = searchtype;
		}
		public String getOrg_discipline_id() {
			return org_discipline_id;
		}
		public void setOrg_discipline_id(String org_discipline_id) {
			this.org_discipline_id = org_discipline_id;
		}
		public String getDivision_id() {
			return division_id;
		}
		public void setDivision_id(String division_id) {
			this.division_id = division_id;
		}
		public String getCertifiedId() {
			return certifiedId;
		}
		public void setCertifiedId(String certifiedId) {
			this.certifiedId = certifiedId;
		}
		public String getMinutemanId() {
			return minutemanId;
		}
		public void setMinutemanId(String minutemanId) {
			this.minutemanId = minutemanId;
		}

	}

	

