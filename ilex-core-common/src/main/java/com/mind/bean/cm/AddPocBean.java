package com.mind.bean.cm;

import java.io.Serializable;

public class AddPocBean implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private String mobile = null;
    private String state = null;
    private String stateSelect = null;
    private String secondaddressline = null;
    private String lastname = null;
    private String firstaddressline = null;
    private String phone2 = null;
    private String phone1 = null;
    private String country = null;
    private String city = null;
    private String save = null;
    private String firstname = null;
    private String ckboxUseDivison = null;
    private String email2 = null;
    private String email1 = null;
    private String zip = null;
    private String fax = null;
    private String reset = null;
    private String title = null;

    private String org_name = null;
    private String division_name = null;
    private String refresh = null;
    private String function = null;
    private String org_type = null;
    private String org_discipline_id = null;
    private String division_id = null;
    private String user_name = null;
    private String password = null;
    private String role_assign = null;
    private String[] check = null;
    private String lo_pc_id = null;
    private String lo_pc_ad_id = null;

    private String updatemessage = null;
    private String addmessage = null;
    private String deletemessage = null;
    private String authenticate = "";

    private String type = null;
    private String division = null;
    private String poc = null;
    private String search = null;

    private String lat_deg = null;
    private String lat_min = null;
    private String lat_direction = null;
    private String lon_deg = null;
    private String lon_min = null;
    private String lon_direction = null;
    private String region_id = null;
    private String category_id = null;
    private String siteCountryId = null;
    private String siteCountryIdName = null;
    private String countrychange = null;
    private String timeZoneId = null;
    private String timeZoneName = null;
    private String refreshState = "false";
    private String refreshType = null;
    private String latlonFound = null;
    private String pvsContact = null;
    private String classificationId = null;
    private String empStatus = null;

    private String rank;
    private String invoiceApproval;

    public String getEmpStatus() {
	return empStatus;
    }

    public void setEmpStatus(String empStatus) {
	this.empStatus = empStatus;
    }

    public String getCategory_id() {
	return category_id;
    }

    public String getPvsContact() {
	return pvsContact;
    }

    public void setPvsContact(String pvsContact) {
	this.pvsContact = pvsContact;
    }

    public void setCategory_id(String category_id) {
	this.category_id = category_id;
    }

    public String getCountrychange() {
	return countrychange;
    }

    public void setCountrychange(String countrychange) {
	this.countrychange = countrychange;
    }

    public String getLat_deg() {
	return lat_deg;
    }

    public void setLat_deg(String lat_deg) {
	this.lat_deg = lat_deg;
    }

    public String getLat_direction() {
	return lat_direction;
    }

    public void setLat_direction(String lat_direction) {
	this.lat_direction = lat_direction;
    }

    public String getLat_min() {
	return lat_min;
    }

    public void setLat_min(String lat_min) {
	this.lat_min = lat_min;
    }

    public String getLon_deg() {
	return lon_deg;
    }

    public void setLon_deg(String lon_deg) {
	this.lon_deg = lon_deg;
    }

    public String getLon_direction() {
	return lon_direction;
    }

    public void setLon_direction(String lon_direction) {
	this.lon_direction = lon_direction;
    }

    public String getLon_min() {
	return lon_min;
    }

    public void setLon_min(String lon_min) {
	this.lon_min = lon_min;
    }

    public String getRegion_id() {
	return region_id;
    }

    public void setRegion_id(String region_id) {
	this.region_id = region_id;
    }

    public String getSiteCountryId() {
	return siteCountryId;
    }

    public void setSiteCountryId(String siteCountryId) {
	this.siteCountryId = siteCountryId;
    }

    public String getSiteCountryIdName() {
	return siteCountryIdName;
    }

    public void setSiteCountryIdName(String siteCountryIdName) {
	this.siteCountryIdName = siteCountryIdName;
    }

    public String getTimeZoneId() {
	return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
	this.timeZoneId = timeZoneId;
    }

    public String getTimeZoneName() {
	return timeZoneName;
    }

    public void setTimeZoneName(String timeZoneName) {
	this.timeZoneName = timeZoneName;
    }

    /**
     * Get mobile
     * 
     * @return String
     */
    public String getMobile() {
	return mobile;
    }

    /**
     * Set mobile
     * 
     * @param <code>String</code>
     */
    public void setMobile(String m) {
	this.mobile = m;
    }

    /**
     * Get state
     * 
     * @return String[]
     */
    public String getState() {
	return state;
    }

    /**
     * Set state
     * 
     * @param <code>String[]</code>
     */
    public void setState(String s) {
	this.state = s;
    }

    /**
     * Get secondaddressline
     * 
     * @return String
     */
    public String getSecondaddressline() {
	return secondaddressline;
    }

    /**
     * Set secondaddressline
     * 
     * @param <code>String</code>
     */
    public void setSecondaddressline(String s) {
	this.secondaddressline = s;
    }

    /**
     * Get lastname
     * 
     * @return String
     */
    public String getLastname() {
	return lastname;
    }

    /**
     * Set lastname
     * 
     * @param <code>String</code>
     */
    public void setLastname(String l) {
	this.lastname = l;
    }

    /**
     * Get firstaddressline
     * 
     * @return String
     */
    public String getFirstaddressline() {
	return firstaddressline;
    }

    /**
     * Set firstaddressline
     * 
     * @param <code>String</code>
     */
    public void setFirstaddressline(String f) {
	this.firstaddressline = f;
    }

    /**
     * Get phone2
     * 
     * @return String
     */
    public String getPhone2() {
	return phone2;
    }

    /**
     * Set phone2
     * 
     * @param <code>String</code>
     */
    public void setPhone2(String p) {
	this.phone2 = p;
    }

    /**
     * Get phone1
     * 
     * @return String
     */
    public String getPhone1() {
	return phone1;
    }

    /**
     * Set phone1
     * 
     * @param <code>String</code>
     */
    public void setPhone1(String p) {
	this.phone1 = p;
    }

    /**
     * Get country
     * 
     * @return String
     */
    public String getCountry() {
	return country;
    }

    /**
     * Set country
     * 
     * @param <code>String</code>
     */
    public void setCountry(String c) {
	this.country = c;
    }

    /**
     * Get city
     * 
     * @return String
     */
    public String getCity() {
	return city;
    }

    /**
     * Set city
     * 
     * @param <code>String</code>
     */
    public void setCity(String c) {
	this.city = c;
    }

    /**
     * Get save
     * 
     * @return String
     */
    public String getSave() {
	return save;
    }

    /**
     * Set save
     * 
     * @param <code>String</code>
     */
    public void setSave(String s) {
	this.save = s;
    }

    /**
     * Get firstname
     * 
     * @return String
     */
    public String getFirstname() {
	return firstname;
    }

    /**
     * Set firstname
     * 
     * @param <code>String</code>
     */
    public void setFirstname(String f) {
	this.firstname = f;
    }

    /**
     * Get ckboxUseDivison
     * 
     * @return String
     */
    public String getCkboxUseDivison() {
	return ckboxUseDivison;
    }

    /**
     * Set ckboxUseDivison
     * 
     * @param <code>String</code>
     */
    public void setCkboxUseDivison(String c) {
	this.ckboxUseDivison = c;
    }

    /**
     * Get email2
     * 
     * @return String
     */
    public String getEmail2() {
	return email2;
    }

    /**
     * Set email2
     * 
     * @param <code>String</code>
     */
    public void setEmail2(String e) {
	this.email2 = e;
    }

    /**
     * Get email1
     * 
     * @return String
     */
    public String getEmail1() {
	return email1;
    }

    /**
     * Set email1
     * 
     * @param <code>String</code>
     */
    public void setEmail1(String e) {
	this.email1 = e;
    }

    /**
     * Get zip
     * 
     * @return String
     */
    public String getZip() {
	return zip;
    }

    /**
     * Set zip
     * 
     * @param <code>String</code>
     */
    public void setZip(String z) {
	this.zip = z;
    }

    /**
     * Get fax
     * 
     * @return String
     */
    public String getFax() {
	return fax;
    }

    /**
     * Set fax
     * 
     * @param <code>String</code>
     */
    public void setFax(String f) {
	this.fax = f;
    }

    /**
     * Get reset
     * 
     * @return String
     */
    public String getReset() {
	return reset;
    }

    /**
     * Set reset
     * 
     * @param <code>String</code>
     */
    public void setReset(String r) {
	this.reset = r;
    }

    /**
     * Get title
     * 
     * @return String
     */
    public String getTitle() {
	return title;
    }

    /**
     * Set title
     * 
     * @param <code>String</code>
     */
    public void setTitle(String t) {
	this.title = t;
    }

    public String getDivision_name() {
	return division_name;
    }

    public void setDivision_name(String division_name) {
	this.division_name = division_name;
    }

    public String getOrg_name() {
	return org_name;
    }

    public void setOrg_name(String org_name) {
	this.org_name = org_name;
    }

    public String getDivision_id() {
	return division_id;
    }

    public void setDivision_id(String division_id) {
	this.division_id = division_id;
    }

    public String getFunction() {
	return function;
    }

    public void setFunction(String function) {
	this.function = function;
    }

    public String getOrg_discipline_id() {
	return org_discipline_id;
    }

    public void setOrg_discipline_id(String org_discipline_id) {
	this.org_discipline_id = org_discipline_id;
    }

    public String getOrg_type() {
	return org_type;
    }

    public void setOrg_type(String org_type) {
	this.org_type = org_type;
    }

    public String getRefresh() {
	return refresh;
    }

    public void setRefresh(String refresh) {
	this.refresh = refresh;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getRole_assign() {
	return role_assign;
    }

    public void setRole_assign(String role_assign) {
	this.role_assign = role_assign;
    }

    public String getUser_name() {
	return user_name;
    }

    public void setUser_name(String user_name) {
	this.user_name = user_name;
    }

    public String[] getCheck() {
	return check;
    }

    public void setCheck(String[] ix) {
	check = ix;
    }

    public String getLo_pc_id() {
	return lo_pc_id;
    }

    public void setLo_pc_id(String lo_pc_id) {
	this.lo_pc_id = lo_pc_id;
    }

    public String getAddmessage() {
	return addmessage;
    }

    public void setAddmessage(String addmessage) {
	this.addmessage = addmessage;
    }

    public String getDeletemessage() {
	return deletemessage;
    }

    public void setDeletemessage(String deletemessage) {
	this.deletemessage = deletemessage;
    }

    public String getUpdatemessage() {
	return updatemessage;
    }

    public void setUpdatemessage(String updatemessage) {
	this.updatemessage = updatemessage;
    }

    public String getLo_pc_ad_id() {
	return lo_pc_ad_id;
    }

    public void setLo_pc_ad_id(String lo_pc_ad_id) {
	this.lo_pc_ad_id = lo_pc_ad_id;
    }

    public String getAuthenticate() {
	return authenticate;
    }

    public void setAuthenticate(String authenticate) {
	this.authenticate = authenticate;
    }

    public String getDivision() {
	return division;
    }

    public void setDivision(String division) {
	this.division = division;
    }

    public String getSearch() {
	return search;
    }

    public void setSearch(String search) {
	this.search = search;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public String getPoc() {
	return poc;
    }

    public void setPoc(String poc) {
	this.poc = poc;
    }

    public String getRefreshState() {
	return refreshState;
    }

    public void setRefreshState(String refreshState) {
	this.refreshState = refreshState;
    }

    public String getRefreshType() {
	return refreshType;
    }

    public void setRefreshType(String refreshType) {
	this.refreshType = refreshType;
    }

    public String getLatlonFound() {
	return latlonFound;
    }

    public void setLatlonFound(String latlonFound) {
	this.latlonFound = latlonFound;
    }

    public String getClassificationId() {
	return classificationId;
    }

    public void setClassificationId(String classificationId) {
	this.classificationId = classificationId;
    }

    public String getStateSelect() {
	return stateSelect;
    }

    public void setStateSelect(String stateSelect) {
	this.stateSelect = stateSelect;
    }

    public void setInvoiceApproval(String invoiceApproval) {
	this.invoiceApproval = invoiceApproval;
    }

    public String getInvoiceApproval() {
	return invoiceApproval;
    }

    public void setRank(String rank) {
	this.rank = rank;
    }

    public String getRank() {
	return rank;
    }

}
