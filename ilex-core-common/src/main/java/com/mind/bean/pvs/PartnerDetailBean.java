package com.mind.bean.pvs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class PartnerDetailBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String partnerId = null;
	private String partnerName = null;
	private String partnerAddress1 = null;
	private String partnerAddress2 = null;
	private String partnerCity = null;
	private String partnerState = null;
	private String partnerZipCode = null;
	private String partnerPhone = null;
	private String partnerPriName = null;
	private String partnerPriPhone = null;
	private String partnerPriCellPhone = null;
	private String partnerPriEmail = null;
	private String partnerType = null;
	private String partnerCellPhone = null;

	private String badgeName;
	private String status;
	private Date uploadedOn;
	private String uploadedOnString;
	private Date updatedOn;
	private String updatedOnString;
	private String updatedBy;
	private String imgPath;
	private String imgName;

	private String daysRange = null;
	private String timeRange = null;

	private String speedPayWorkNature = null;
	private int speedPayWorkNatureCount = 0;
	private ArrayList<String[]> thumbUpDownList = new ArrayList<String[]>();

	public String getSpeedPayWorkNature() {
		return speedPayWorkNature;
	}

	public void setSpeedPayWorkNature(String speedPayWorkNature) {
		this.speedPayWorkNature = speedPayWorkNature;
	}

	public int getSpeedPayWorkNatureCount() {
		return speedPayWorkNatureCount;
	}

	public void setSpeedPayWorkNatureCount(int speedPayWorkNatureCount) {
		this.speedPayWorkNatureCount = speedPayWorkNatureCount;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getPartnerAddress1() {
		return partnerAddress1;
	}

	public void setPartnerAddress1(String partnerAddress1) {
		this.partnerAddress1 = partnerAddress1;
	}

	public String getPartnerAddress2() {
		return partnerAddress2;
	}

	public void setPartnerAddress2(String partnerAddress2) {
		this.partnerAddress2 = partnerAddress2;
	}

	public String getPartnerCity() {
		return partnerCity;
	}

	public void setPartnerCity(String partnerCity) {
		this.partnerCity = partnerCity;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerPhone() {
		return partnerPhone;
	}

	public void setPartnerPhone(String partnerPhone) {
		this.partnerPhone = partnerPhone;
	}

	public String getPartnerPriCellPhone() {
		return partnerPriCellPhone;
	}

	public void setPartnerPriCellPhone(String partnerPriCellPhone) {
		this.partnerPriCellPhone = partnerPriCellPhone;
	}

	public String getPartnerPriEmail() {
		return partnerPriEmail;
	}

	public void setPartnerPriEmail(String partnerPriEmail) {
		this.partnerPriEmail = partnerPriEmail;
	}

	public String getPartnerPriName() {
		return partnerPriName;
	}

	public void setPartnerPriName(String partnerPriName) {
		this.partnerPriName = partnerPriName;
	}

	public String getPartnerPriPhone() {
		return partnerPriPhone;
	}

	public void setPartnerPriPhone(String partnerPriPhone) {
		this.partnerPriPhone = partnerPriPhone;
	}

	public String getPartnerZipCode() {
		return partnerZipCode;
	}

	public void setPartnerZipCode(String partnerZipCode) {
		this.partnerZipCode = partnerZipCode;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getPartnerState() {
		return partnerState;
	}

	public void setPartnerState(String partnerState) {
		this.partnerState = partnerState;
	}

	public String getPartnerCellPhone() {
		return partnerCellPhone;
	}

	public void setPartnerCellPhone(String partnerCellPhone) {
		this.partnerCellPhone = partnerCellPhone;
	}

	public String getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}

	public String getDaysRange() {
		return daysRange;
	}

	public void setDaysRange(String daysRange) {
		this.daysRange = daysRange;
	}

	public String getBadgeName() {
		return badgeName;
	}

	public void setBadgeName(String badgeName) {
		this.badgeName = badgeName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	public String getUploadedOnString() {
		return uploadedOnString;
	}

	public void setUploadedOnString(String uploadedOnString) {
		this.uploadedOnString = uploadedOnString;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedOnString() {
		return updatedOnString;
	}

	public void setUpdatedOnString(String updatedOnString) {
		this.updatedOnString = updatedOnString;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getImgName() {
		return imgName;
	}

	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	public ArrayList<String[]> getThumbUpDownList() {
		return thumbUpDownList;
	}

	public void setThumbUpDownList(ArrayList<String[]> thumbUpDownList) {
		this.thumbUpDownList = thumbUpDownList;
	}

}
