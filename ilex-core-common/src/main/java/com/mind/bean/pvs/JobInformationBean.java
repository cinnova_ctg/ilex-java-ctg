package com.mind.bean.pvs;

public class JobInformationBean {
	
	private String customername = null;
	private String sitenumber = null;
	private String sitename = null;
	private String siteaddress = null;
	private String sitePrimarypoc = null;
	private String siteSecondarypoc = null;
	private String owner = null;
	private String jobstatus = null;
	private String appendixName = null;
	private String tempPartnerid = null;
	private String jobName = null;
	
	
	private String schedulestartdate = null;
	private String schedulestarttime = null;
	private String schedulecomplete = null;
	
	private String actualstartdate = null;	
	private String actualstartdatetime = null;
	private String actualend = null;
	
	private String estimatedcost = null;
	private String extendedprice = null;
	private String proformavgp = null;
	private String actualcost = null;
	private String actualvgp = null;
	private String poNumber = null;
	
	
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getPartnerZipCode() {
		return partnerZipCode;
	}
	public void setPartnerZipCode(String partnerZipCode) {
		this.partnerZipCode = partnerZipCode;
	}
	public String getPartnerCountry() {
		return partnerCountry;
	}
	public void setPartnerCountry(String partnerCountry) {
		this.partnerCountry = partnerCountry;
	}
	public String getPartnerState() {
		return partnerState;
	}
	public void setPartnerState(String partnerState) {
		this.partnerState = partnerState;
	}
	public String getPartnerStatus() {
		return partnerStatus;
	}
	public void setPartnerStatus(String partnerStatus) {
		this.partnerStatus = partnerStatus;
	}
	public String getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	private String partnerName = null;
	private String partnerCity = null;
	private String partnerZipCode = null;
	private String partnerCountry = null;
	private String partnerState = null;
	private String partnerStatus = null;
	private String partnerType=null;
	private String  partnerPhoneNo=null;
	
	
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getJobstatus() {
		return jobstatus;
	}
	public void setJobstatus(String jobstatus) {
		this.jobstatus = jobstatus;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getSiteaddress() {
		return siteaddress;
	}
	public void setSiteaddress(String siteaddress) {
		this.siteaddress = siteaddress;
	}
	public String getSitename() {
		return sitename;
	}
	public void setSitename(String sitename) {
		this.sitename = sitename;
	}
	public String getSitenumber() {
		return sitenumber;
	}
	public void setSitenumber(String sitenumber) {
		this.sitenumber = sitenumber;
	}
	public String getSitePrimarypoc() {
		return sitePrimarypoc;
	}
	public void setSitePrimarypoc(String sitePrimarypoc) {
		this.sitePrimarypoc = sitePrimarypoc;
	}
	public String getSiteSecondarypoc() {
		return siteSecondarypoc;
	}
	public void setSiteSecondarypoc(String siteSecondarypoc) {
		this.siteSecondarypoc = siteSecondarypoc;
	}
	public String getTempPartnerid() {
		return tempPartnerid;
	}
	public void setTempPartnerid(String tempPartnerid) {
		this.tempPartnerid = tempPartnerid;
	}
	public String getActualcost() {
		return actualcost;
	}
	public void setActualcost(String actualcost) {
		this.actualcost = actualcost;
	}
	public String getActualend() {
		return actualend;
	}
	public void setActualend(String actualend) {
		this.actualend = actualend;
	}
	public String getActualstartdate() {
		return actualstartdate;
	}
	public void setActualstartdate(String actualstartdate) {
		this.actualstartdate = actualstartdate;
	}
	public String getActualstartdatetime() {
		return actualstartdatetime;
	}
	public void setActualstartdatetime(String actualstartdatetime) {
		this.actualstartdatetime = actualstartdatetime;
	}
	public String getActualvgp() {
		return actualvgp;
	}
	public void setActualvgp(String actualvgp) {
		this.actualvgp = actualvgp;
	}
	public String getEstimatedcost() {
		return estimatedcost;
	}
	public void setEstimatedcost(String estimatedcost) {
		this.estimatedcost = estimatedcost;
	}
	public String getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice(String extendedprice) {
		this.extendedprice = extendedprice;
	}
	public String getProformavgp() {
		return proformavgp;
	}
	public void setProformavgp(String proformavgp) {
		this.proformavgp = proformavgp;
	}
	public String getSchedulecomplete() {
		return schedulecomplete;
	}
	public void setSchedulecomplete(String schedulecomplete) {
		this.schedulecomplete = schedulecomplete;
	}
	public String getSchedulestartdate() {
		return schedulestartdate;
	}
	public void setSchedulestartdate(String schedulestartdate) {
		this.schedulestartdate = schedulestartdate;
	}
	public String getSchedulestarttime() {
		return schedulestarttime;
	}
	public void setSchedulestarttime(String schedulestarttime) {
		this.schedulestarttime = schedulestarttime;
	}
	public void setPartnerCity(String partnerCity) {
		this.partnerCity = partnerCity;
	}
	public String getPartnerCity() {
		return partnerCity;
	}
	public void setPartnerPhoneNo(String partnerPhoneNo) {
		this.partnerPhoneNo = partnerPhoneNo;
	}
	public String getPartnerPhoneNo() {
		return partnerPhoneNo;
	}

}
