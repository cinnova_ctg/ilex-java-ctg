package com.mind.bean.pvs;

import java.io.Serializable;

public class McsaDetailBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mcsa_id = null;
	private byte[] mcsa_data = null;
	private String mcsa_version = null;
	private String mcsa_date = null;
	private String mcsa_created_by = null;
	private String mcsa_create_date = null;
	private String mcsa_changed_by = null;
	private String mcsa_change_date = null;

	public String getMcsa_changed_by() {
		return mcsa_changed_by;
	}

	public void setMcsa_changed_by(String mcsa_changed_by) {
		this.mcsa_changed_by = mcsa_changed_by;
	}

	public String getMcsa_create_date() {
		return mcsa_create_date;
	}

	public void setMcsa_create_date(String mcsa_create_date) {
		this.mcsa_create_date = mcsa_create_date;
	}

	public String getMcsa_created_by() {
		return mcsa_created_by;
	}

	public void setMcsa_created_by(String mcsa_created_by) {
		this.mcsa_created_by = mcsa_created_by;
	}

	public byte[] getMcsa_data() {
		return mcsa_data;
	}

	public void setMcsa_data(byte[] mcsa_data) {
		this.mcsa_data = mcsa_data;
	}

	public String getMcsa_date() {
		return mcsa_date;
	}

	public void setMcsa_date(String mcsa_date) {
		this.mcsa_date = mcsa_date;
	}

	public String getMcsa_id() {
		return mcsa_id;
	}

	public void setMcsa_id(String mcsa_id) {
		this.mcsa_id = mcsa_id;
	}

	public String getMcsa_version() {
		return mcsa_version;
	}

	public void setMcsa_version(String mcsa_version) {
		this.mcsa_version = mcsa_version;
	}

	public String getMcsa_change_date() {
		return mcsa_change_date;
	}

	public void setMcsa_change_date(String mcsa_change_date) {
		this.mcsa_change_date = mcsa_change_date;
	}

}
