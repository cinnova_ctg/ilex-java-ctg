package com.mind.bean.pvs;

import java.io.Serializable;
import java.util.Date;

public class PartnerSearchMySQLBean implements Serializable {

	private String pid = null;
	private String badgeName;
	private String status;
	private Date uploadedOn;
	private String uploadedOnString;
	private Date updatedOn;
	private String updatedOnString;
	private String updatedBy;
	private String imgPath;
	private String imgName;

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getImgName() {
		return imgName;
	}

	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getBadgeName() {
		return badgeName;
	}

	public void setBadgeName(String badgeName) {
		this.badgeName = badgeName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	public String getUploadedOnString() {
		return uploadedOnString;
	}

	public void setUploadedOnString(String uploadedOnString) {
		this.uploadedOnString = uploadedOnString;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedOnString() {
		return updatedOnString;
	}

	public void setUpdatedOnString(String updatedOnString) {
		this.updatedOnString = updatedOnString;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
