/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: A bean containing getter/setter methods for Partner statistics  page
 *
 */
package com.mind.bean.pvs;

import java.io.Serializable;

public class Partner_Statistics implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cpartners = null;
	private String qsummary = null;
	private String lsummary = null;

	private String qvalue = null;
	private String lvalue = null;

	public void setQvalue(String qval) {
		qvalue = qval;
	}

	public String getQvalue() {
		return qvalue;
	}

	public void setLvalue(String lval) {
		lvalue = lval;
	}

	public String getLvalue() {
		return lvalue;
	}

	public void setCpartners(String cpart) {
		cpartners = cpart;
	}

	public String getCpartners() {
		return cpartners;
	}

	public void setQsummary(String qsum) {
		qsummary = qsum;
	}

	public String getQsummary() {
		return qsummary;
	}

	public void setLsummary(String lsum) {
		lsummary = lsum;
	}

	public String getLsummary() {
		return lsummary;
	}

}
