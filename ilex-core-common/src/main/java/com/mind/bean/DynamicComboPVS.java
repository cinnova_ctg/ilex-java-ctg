package com.mind.bean;

import java.io.Serializable;
import java.util.Collection;

public class DynamicComboPVS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Collection customerlist = null;
	private Collection technicianlist = null;
	private Collection ampmlist = null;
	private Collection competencylist = null;
	private Collection certificationlist = null;
	private Collection partnertypelist = null;
	private Collection formlist = null;
	private Collection msalist = null;
	private Collection sitelist = null;
	private Collection incidentTypeList = null;

	public Collection getAmpmlist() {
		return ampmlist;
	}

	public void setAmpmlist(Collection ampmlist) {
		this.ampmlist = ampmlist;
	}

	public Collection getCustomerlist() {
		return customerlist;
	}

	public void setCustomerlist(Collection customerlist) {
		this.customerlist = customerlist;
	}

	public Collection getTechnicianlist() {
		return technicianlist;
	}

	public void setTechnicianlist(Collection technicianlist) {
		this.technicianlist = technicianlist;
	}

	public Collection getCompetencylist() {
		return competencylist;
	}

	public void setCompetencylist(Collection competencylist) {
		this.competencylist = competencylist;
	}

	public Collection getCertificationlist() {
		return certificationlist;
	}

	public void setCertificationlist(Collection certificationlist) {
		this.certificationlist = certificationlist;
	}

	public Collection getPartnertypelist() {
		return partnertypelist;
	}

	public void setPartnertypelist(Collection partnertypelist) {
		this.partnertypelist = partnertypelist;
	}

	public Collection getFormlist() {
		return formlist;
	}

	public void setFormlist(Collection formlist) {
		this.formlist = formlist;
	}

	public Collection getMsalist() {
		return msalist;
	}

	public void setMsalist(Collection msalist) {
		this.msalist = msalist;
	}

	public Collection getSitelist() {
		return sitelist;
	}

	public void setSitelist(Collection sitelist) {
		this.sitelist = sitelist;
	}

	public Collection getIncidentTypeList() {
		return incidentTypeList;
	}

	public void setIncidentTypeList(Collection incidentTypeList) {
		this.incidentTypeList = incidentTypeList;
	}

}
