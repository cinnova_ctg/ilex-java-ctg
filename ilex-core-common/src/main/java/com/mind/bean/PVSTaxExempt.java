package com.mind.bean;

import java.io.Serializable;

public class PVSTaxExempt implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String stateR1 = null;
	String taxR1 = null;
	String label = null;

	public PVSTaxExempt(String stater1, String taxr1, String label) {
		stateR1 = stater1;
		taxR1 = taxr1;
		this.label = label;
	}

	public PVSTaxExempt() {
		super();
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getStateR1() {
		return stateR1;
	}

	public void setStateR1(String stateR1) {
		this.stateR1 = stateR1;
	}

	public String getTaxR1() {
		return taxR1;
	}

	public void setTaxR1(String taxR1) {
		this.taxR1 = taxR1;
	}
}
