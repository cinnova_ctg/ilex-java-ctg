package com.mind.bean;

public class PMInworkStateList implements java.io.Serializable {
	private String lo_ot_name=null;
	private String lx_pr_id=null;
	private String lx_pr_title=null;
	private String lx_pr_status=null;

	public PMInworkStateList() {
		super();
	}
	
	public PMInworkStateList(String lo_ot_name, String lx_pr_id, String lx_pr_title, String lx_pr_status) {
		super();
		this.lo_ot_name = lo_ot_name;
		this.lx_pr_id = lx_pr_id;
		this.lx_pr_title = lx_pr_title;
		this.lx_pr_status = lx_pr_status;
	}

	public String getLo_ot_name() {
		return lo_ot_name;
	}

	public void setLo_ot_name(String lo_ot_name) {
		this.lo_ot_name = lo_ot_name;
	}

	public String getLx_pr_id() {
		return lx_pr_id;
	}

	public void setLx_pr_id(String lx_pr_id) {
		this.lx_pr_id = lx_pr_id;
	}

	public String getLx_pr_status() {
		return lx_pr_status;
	}

	public void setLx_pr_status(String lx_pr_status) {
		this.lx_pr_status = lx_pr_status;
	}

	public String getLx_pr_title() {
		return lx_pr_title;
	}

	public void setLx_pr_title(String lx_pr_title) {
		this.lx_pr_title = lx_pr_title;
	}


}
