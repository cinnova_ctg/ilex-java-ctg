package com.mind.bean.im;

import java.io.Serializable;



public class InvoiceJobDetailDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String bulkInvoiceNo = null;
	private String bulkInvoiceDate = null;
	private String bulkInvoiceNoForSearch = null;
	private String radio = null;

	public String getRadio() {
		return radio;
	}

	public void setRadio(String radio) {
		this.radio = radio;
	}

	public String getBulkInvoiceNoForSearch() {
		return bulkInvoiceNoForSearch;
	}

	public void setBulkInvoiceNoForSearch(String bulkInvoiceNoForSearch) {
		this.bulkInvoiceNoForSearch = bulkInvoiceNoForSearch;
	}

	public String getBulkInvoiceDate() {
		return bulkInvoiceDate;
	}

	public void setBulkInvoiceDate(String bulkInvoiceDate) {
		this.bulkInvoiceDate = bulkInvoiceDate;
	}

	public String getBulkInvoiceNo() {
		return bulkInvoiceNo;
	}

	public void setBulkInvoiceNo(String bulkInvoiceNo) {
		this.bulkInvoiceNo = bulkInvoiceNo;
	}

	private String id = null;
	private String titleappendixname = null;
	private String titlemsaname = null;
	private String rdofilter = null;
	private String msaId = null;
	private String msaName = null;
	private String appendixId = null;
	private String appendixName = null;
	private String jobId = null;
	private String jobName = null;

	private String problem_summary = null;

	private String viewjobtype = null;
	private String ownerId = null;
	private String nettype = null;

	private String extendedprice = null;

	private String status = null;
	private String actualstartdate = null;
	private String actualcompletedate = null;
	private String submitteddate = null;

	private String powo = null;

	private String poAuthorizedTotal = null;

	private String type = null;
	private String[] check = null;
	private String function = null;
	private String view = null;
	private String selectmessage = null;

	private String function_type = null;
	private String job_type = null;

	// Start :Added By Amit For Closed date
	private String[] closed_date = null;
	// Added By Amit
	private String[] invoice_no = null;
	private String[] associated_date = null;
	private String[] hid_jobid = null;
	private String[] customerref = null;

	private String out_of_scope_activity_flag = null;

	private String partner_name = null;
	private String customer_name = null;
	private String powo_number = null;
	private String from_date = null;
	private String to_date = null;
	private String search = null;
	private String cancel = null;
	private String flag = null;
	private String flag1 = null;
	private String invoiceno = null;
	private String invoice_Flag = null;
	private String toggle = null;
	private String exportStatus = null;

	private String rsName = null;
	private String rsType = null;
	private String rsquantity = null;
	private String rsPrice = null;

	private String onSite = null;
	private String startDate = null;
	private String startEnd = null;
	private String sequenceNo = null;

	private String siteName = null;
	private String siteNumber = null;
	private String siteAddress = null;
	private String siteCity = null;
	private String siteState = null;
	private String siteCountry = null;
	private String siteZipCode = null;
	private String custref = null;
	private String typeJob = null;
	private String requestorName = null;
	private String problemdesc = null;
	private String requestType = null;
	private String timeOnSite = null;
	private String cnsPartNo = null;
	private String rsTransit = null;
	private String searchType = null;
	private String billingInfo = null;

	public String getBillingInfo() {
		return billingInfo;
	}

	public void setBillingInfo(String billingInfo) {
		this.billingInfo = billingInfo;
	}

	public String getRsTransit() {
		return rsTransit;
	}

	public void setRsTransit(String rsTransit) {
		this.rsTransit = rsTransit;
	}

	public String getCnsPartNo() {
		return cnsPartNo;
	}

	public void setCnsPartNo(String cnsPartNo) {
		this.cnsPartNo = cnsPartNo;
	}

	public String getExportStatus() {
		return exportStatus;
	}

	public void setExportStatus(String exportStatus) {
		this.exportStatus = exportStatus;
	}

	public String getInvoiceno() {
		return invoiceno;
	}

	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}

	public String getCancel() {
		return cancel;
	}

	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getTitleappendixname() {
		return titleappendixname;
	}

	public void setTitleappendixname(String titleappendixname) {
		this.titleappendixname = titleappendixname;
	}

	public String getTitlemsaname() {
		return titlemsaname;
	}

	public void setTitlemsaname(String titlemsaname) {
		this.titlemsaname = titlemsaname;
	}

	public String getExtendedprice() {
		return extendedprice;
	}

	public void setExtendedprice(String extendedprice) {
		this.extendedprice = extendedprice;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String[] getCheck() {
		return check;
	}

	public void setCheck(String[] check) {
		this.check = check;
	}

	public String getCheck(int i) {
		return check[i];
	}

	public String[] getAssociated_date() {
		return associated_date;
	}

	public void setAssociated_date(String[] associated_date) {
		this.associated_date = associated_date;
	}

	public String getAssociated_date(int i) {
		return associated_date[i];
	}

	public String getClosed_date(int i) {
		return closed_date[i];
	}

	public String[] getHid_jobid() {
		return hid_jobid;
	}

	public void setHid_jobid(String[] hid_jobid) {
		this.hid_jobid = hid_jobid;
	}

	public String getHid_jobid(int i) {
		return hid_jobid[i];
	}

	public String[] getInvoice_no() {
		return invoice_no;
	}

	public void setInvoice_no(String[] invoice_no) {
		this.invoice_no = invoice_no;
	}

	public String getInvoice_no(int i) {
		return invoice_no[i];
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}

	public String getSelectmessage() {
		return selectmessage;
	}

	public void setSelectmessage(String selectmessage) {
		this.selectmessage = selectmessage;
	}

	public String getFunction_type() {
		return function_type;
	}

	public void setFunction_type(String function_type) {
		this.function_type = function_type;
	}

	public String getJob_type() {
		return job_type;
	}

	public void setJob_type(String job_type) {
		this.job_type = job_type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	public String getActualcompletedate() {
		return actualcompletedate;
	}

	public void setActualcompletedate(String actualcompletedate) {
		this.actualcompletedate = actualcompletedate;
	}

	public String getActualstartdate() {
		return actualstartdate;
	}

	public void setActualstartdate(String actualstartdate) {
		this.actualstartdate = actualstartdate;
	}

	public String getSubmitteddate() {
		return submitteddate;
	}

	public void setSubmitteddate(String submitteddate) {
		this.submitteddate = submitteddate;
	}

	

	public String getRdofilter() {
		return rdofilter;
	}

	public void setRdofilter(String rdofilter) {
		this.rdofilter = rdofilter;
	}

	public String getPowo() {
		return powo;
	}

	public void setPowo(String powo) {
		this.powo = powo;
	}

	public String getNettype() {
		return nettype;
	}

	public void setNettype(String nettype) {
		this.nettype = nettype;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getPoAuthorizedTotal() {
		return poAuthorizedTotal;
	}

	public void setPoAuthorizedTotal(String poAuthorizedTotal) {
		this.poAuthorizedTotal = poAuthorizedTotal;
	}

	public String[] getCustomerref() {
		return customerref;
	}

	public void setCustomerref(String[] customerref) {
		this.customerref = customerref;
	}

	public String getCustomerref(int i) {
		return customerref[i];
	}

	public String getProblem_summary() {
		return problem_summary;
	}

	public void setProblem_summary(String problem_summary) {
		this.problem_summary = problem_summary;
	}

	public String[] getClosed_date() {
		return closed_date;
	}

	public void setClosed_date(String[] closed_date) {
		this.closed_date = closed_date;
	}

	public String getOut_of_scope_activity_flag() {
		return out_of_scope_activity_flag;
	}

	public void setOut_of_scope_activity_flag(String out_of_scope_activity_flag) {
		this.out_of_scope_activity_flag = out_of_scope_activity_flag;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getFrom_date() {
		return from_date;
	}

	public void setFrom_date(String from_date) {
		this.from_date = from_date;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public String getPowo_number() {
		return powo_number;
	}

	public void setPowo_number(String powo_number) {
		this.powo_number = powo_number;
	}

	public String getTo_date() {
		return to_date;
	}

	public void setTo_date(String to_date) {
		this.to_date = to_date;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getFlag1() {
		return flag1;
	}

	public void setFlag1(String flag1) {
		this.flag1 = flag1;
	}

	public String getInvoice_Flag() {
		return invoice_Flag;
	}

	public void setInvoice_Flag(String invoice_Flag) {
		this.invoice_Flag = invoice_Flag;
	}

	public String getToggle() {
		return toggle;
	}

	public void setToggle(String toggle) {
		this.toggle = toggle;
	}

	public String getRsName() {
		return rsName;
	}

	public void setRsName(String rsName) {
		this.rsName = rsName;
	}

	public String getRsPrice() {
		return rsPrice;
	}

	public void setRsPrice(String rsPrice) {
		this.rsPrice = rsPrice;
	}

	public String getRsquantity() {
		return rsquantity;
	}

	public void setRsquantity(String rsquantity) {
		this.rsquantity = rsquantity;
	}

	public String getRsType() {
		return rsType;
	}

	public void setRsType(String rsType) {
		this.rsType = rsType;
	}

	public String getCustref() {
		return custref;
	}

	public void setCustref(String custref) {
		this.custref = custref;
	}

	public String getProblemdesc() {
		return problemdesc;
	}

	public void setProblemdesc(String problemdesc) {
		this.problemdesc = problemdesc;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteCountry() {
		return siteCountry;
	}

	public void setSiteCountry(String siteCountry) {
		this.siteCountry = siteCountry;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getSiteZipCode() {
		return siteZipCode;
	}

	public void setSiteZipCode(String siteZipCode) {
		this.siteZipCode = siteZipCode;
	}

	public String getTypeJob() {
		return typeJob;
	}

	public void setTypeJob(String typeJob) {
		this.typeJob = typeJob;
	}

	public String getTimeOnSite() {
		return timeOnSite;
	}

	public void setTimeOnSite(String timeOnSite) {
		this.timeOnSite = timeOnSite;
	}

	public String getOnSite() {
		return onSite;
	}

	public void setOnSite(String onSite) {
		this.onSite = onSite;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStartEnd() {
		return startEnd;
	}

	public void setStartEnd(String startEnd) {
		this.startEnd = startEnd;
	}

	public String getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

}
