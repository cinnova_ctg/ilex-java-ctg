package com.mind.bean.im;

public class InvoiceJobDetailBean {

	private String msaId = null;
	private String msaName = null;
	private String appendixId = null;
	private String appendixName = null;
	private String jobId = null;
	private String jobName = null;
	// Start :Added By Amit For Received Date & Time on Accounting Manager
	// screen

	private String request_received_date = null;
	private String request_received_time = null;
	private String ticket_id = null;
	private String siteNo = null;
	private String partner_name = null;

	private String approved_total = null;
	private String paid_status = null;
	private String paid_date = null;
	private String check_number = null;

	private String closed_date = null;
	// End

	private String extendedprice = null;

	private String status = null;
	private String actualstartdate = null;
	private String actualcompletedate = null;
	private String submitteddate = null;

	private String powo = null;
	private String poAuthorizedTotal = null;

	private String job_type = null;
	private String hid_jobid = null;

	private String invoice_no = null;
	private String associated_date = null;

	private String gpImportStatus;
	private String gpImportId;

	public String getInvoicePrice() {
		return invoicePrice;
	}

	public void setInvoicePrice(String invoicePrice) {
		this.invoicePrice = invoicePrice;
	}

	private String createdby = null;
	private String customerref = null;
	private String exportStatus = null;

	private String out_of_scope_activity_flag = null;
	private String jobOffsiteDate = null;
	private String invoicePrice = null;
	private String discount = null;

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getJobOffsiteDate() {
		return jobOffsiteDate;
	}

	public void setJobOffsiteDate(String jobOffsiteDate) {
		this.jobOffsiteDate = jobOffsiteDate;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getActualcompletedate() {
		return actualcompletedate;
	}

	public void setActualcompletedate(String actualcompletedate) {
		this.actualcompletedate = actualcompletedate;
	}

	public String getActualstartdate() {
		return actualstartdate;
	}

	public void setActualstartdate(String actualstartdate) {
		this.actualstartdate = actualstartdate;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getAssociated_date() {
		return associated_date;
	}

	public void setAssociated_date(String associated_date) {
		this.associated_date = associated_date;
	}

	public String getExtendedprice() {
		return extendedprice;
	}

	public void setExtendedprice(String extendedprice) {
		this.extendedprice = extendedprice;
	}

	public String getHid_jobid() {
		return hid_jobid;
	}

	public void setHid_jobid(String hid_jobid) {
		this.hid_jobid = hid_jobid;
	}

	public String getInvoice_no() {
		return invoice_no;
	}

	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}

	public String getJob_type() {
		return job_type;
	}

	public void setJob_type(String job_type) {
		this.job_type = job_type;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	public String getPowo() {
		return powo;
	}

	public void setPowo(String powo) {
		this.powo = powo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubmitteddate() {
		return submitteddate;
	}

	public void setSubmitteddate(String submitteddate) {
		this.submitteddate = submitteddate;
	}

	public String getPoAuthorizedTotal() {
		return poAuthorizedTotal;
	}

	public void setPoAuthorizedTotal(String poAuthorizedTotal) {
		this.poAuthorizedTotal = poAuthorizedTotal;
	}

	public String getCustomerref() {
		return customerref;
	}

	public void setCustomerref(String customerref) {
		this.customerref = customerref;
	}

	public String getRequest_received_date() {
		return request_received_date;
	}

	public void setRequest_received_date(String request_received_date) {
		this.request_received_date = request_received_date;
	}

	public String getRequest_received_time() {
		return request_received_time;
	}

	public void setRequest_received_time(String request_received_time) {
		this.request_received_time = request_received_time;
	}

	public String getTicket_id() {
		return ticket_id;
	}

	public void setTicket_id(String ticket_id) {
		this.ticket_id = ticket_id;
	}

	public String getSiteNo() {
		return siteNo;
	}

	public void setSiteNo(String siteNo) {
		this.siteNo = siteNo;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public String getClosed_date() {
		return closed_date;
	}

	public void setClosed_date(String closed_date) {
		this.closed_date = closed_date;
	}

	public String getOut_of_scope_activity_flag() {
		return out_of_scope_activity_flag;
	}

	public void setOut_of_scope_activity_flag(String out_of_scope_activity_flag) {
		this.out_of_scope_activity_flag = out_of_scope_activity_flag;
	}

	public String getExportStatus() {
		return exportStatus;
	}

	public void setExportStatus(String exportStatus) {
		this.exportStatus = exportStatus;
	}

	public String getApproved_total() {
		return approved_total;
	}

	public void setApproved_total(String approved_total) {
		this.approved_total = approved_total;
	}

	public String getCheck_number() {
		return check_number;
	}

	public void setCheck_number(String check_number) {
		this.check_number = check_number;
	}

	public String getPaid_status() {
		return paid_status;
	}

	public void setPaid_status(String paid_status) {
		this.paid_status = paid_status;
	}

	public String getPaid_date() {
		return paid_date;
	}

	public void setPaid_date(String paid_date) {
		this.paid_date = paid_date;
	}

	public String getGpImportStatus() {
		return gpImportStatus;
	}

	public void setGpImportStatus(String gpImportStatus) {
		this.gpImportStatus = gpImportStatus;
	}

	public String getGpImportId() {
		return gpImportId;
	}

	public void setGpImportId(String gpImportId) {
		this.gpImportId = gpImportId;
	}

}
