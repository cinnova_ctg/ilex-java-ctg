package com.mind.bean.im;

import java.util.ArrayList;


public class InvoiceJobCommissionPOBean {
	private String invoiceNo = null;
	private String paymentReceiveDate = null;
	private String totalRevenue = null;
	private ArrayList<InvoicedJobDetails> invoicedJobs = null;
	private String[] jobId = null;
	
	public String[] getJobId() {
		return jobId;
	}
	public void setJobId(String[] jobId) {
		this.jobId = jobId;
	}
	public String getJobId(int i) {
		return jobId[i];
	}
	
	public ArrayList<InvoicedJobDetails> getInvoicedJobs() {
		return invoicedJobs;
	}
	public void setInvoicedJobs(ArrayList<InvoicedJobDetails> invoicedJobs) {
		this.invoicedJobs = invoicedJobs;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getPaymentReceiveDate() {
		return paymentReceiveDate;
	}
	public void setPaymentReceiveDate(String paymentReceiveDate) {
		this.paymentReceiveDate = paymentReceiveDate;
	}
	public String getTotalRevenue() {
		return totalRevenue;
	}
	public void setTotalRevenue(String totalRevenue) {
		this.totalRevenue = totalRevenue;
	}
	
}
