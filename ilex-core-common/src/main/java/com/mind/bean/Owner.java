package com.mind.bean;

import java.io.Serializable;

public class Owner implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String ownerId = null;
	private String ownerName = null;

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
}
