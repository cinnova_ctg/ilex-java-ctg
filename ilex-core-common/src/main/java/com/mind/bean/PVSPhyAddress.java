package com.mind.bean;

public class PVSPhyAddress implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String phyFieldAddress1 = null;
	private String phyFieldAddress2 = null;
	private String phyFieldState = null;
	private String phyFieldZip = null;
	private String phyFieldPhone = null;
	private String phyFieldFax = null;

	public PVSPhyAddress() {

	}

	public PVSPhyAddress(String address1, String address2, String state,
			String zip, String phone, String fax) {
		super();

		phyFieldAddress1 = address1;
		phyFieldAddress2 = address2;
		phyFieldState = state;
		phyFieldZip = zip;
		phyFieldPhone = phone;
		phyFieldFax = fax;
	}

	public String getPhyFieldAddress1() {
		return phyFieldAddress1;
	}

	public void setPhyFieldAddress1(String phyFieldAddress1) {
		this.phyFieldAddress1 = phyFieldAddress1;
	}

	public String getPhyFieldAddress2() {
		return phyFieldAddress2;
	}

	public void setPhyFieldAddress2(String phyFieldAddress2) {
		this.phyFieldAddress2 = phyFieldAddress2;
	}

	public String getPhyFieldFax() {
		return phyFieldFax;
	}

	public void setPhyFieldFax(String phyFieldFax) {
		this.phyFieldFax = phyFieldFax;
	}

	public String getPhyFieldPhone() {
		return phyFieldPhone;
	}

	public void setPhyFieldPhone(String phyFieldPhone) {
		this.phyFieldPhone = phyFieldPhone;
	}

	public String getPhyFieldState() {
		return phyFieldState;
	}

	public void setPhyFieldState(String phyFieldState) {
		this.phyFieldState = phyFieldState;
	}

	public String getPhyFieldZip() {
		return phyFieldZip;
	}

	public void setPhyFieldZip(String phyFieldZip) {
		this.phyFieldZip = phyFieldZip;
	}

}
