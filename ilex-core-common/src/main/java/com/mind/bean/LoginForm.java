package com.mind.bean;

import java.io.Serializable;

public class LoginForm implements Serializable {

	private String login_user_id = null;
	private String login_user_email = null;
	private String login_user_name = null;

	public String getLogin_user_email() {
		return login_user_email;
	}

	public void setLogin_user_email(String login_user_email) {
		this.login_user_email = login_user_email;
	}

	public String getLogin_user_id() {
		return login_user_id;
	}

	public void setLogin_user_id(String login_user_id) {
		this.login_user_id = login_user_id;
	}

	public String getLogin_user_name() {
		return login_user_name;
	}

	public void setLogin_user_name(String login_user_name) {
		this.login_user_name = login_user_name;
	}

}
