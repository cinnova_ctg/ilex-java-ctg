package com.mind.bean;

import java.io.Serializable;

public class PVS_CompanyCert_StructuredDistribution implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String sdcompany = null;
	private String validFromSD = null;
	private String validToSD = null;
	private String sdcompanyName = null;

	public PVS_CompanyCert_StructuredDistribution(String sdcompany,
			String fromSD, String toSD, String sdcompanyName) {
		this.sdcompany = sdcompany;
		validFromSD = fromSD;
		validToSD = toSD;
		this.sdcompanyName = sdcompanyName;
	}

	public PVS_CompanyCert_StructuredDistribution(String sdcompany,
			String fromSD, String toSD) {
		this.sdcompany = sdcompany;
		validFromSD = fromSD;
		validToSD = toSD;
	}

	public PVS_CompanyCert_StructuredDistribution() {
		super();
	}

	public String getSdcompany() {
		return sdcompany;
	}

	public void setSdcompany(String sdcompany) {
		this.sdcompany = sdcompany;
	}

	public String getValidFromSD() {
		return validFromSD;
	}

	public void setValidFromSD(String validFromSD) {
		this.validFromSD = validFromSD;
	}

	public String getValidToSD() {
		return validToSD;
	}

	public void setValidToSD(String validToSD) {
		this.validToSD = validToSD;
	}

	public String getSdcompanyName() {
		return sdcompanyName;
	}

	public void setSdcompanyName(String sdcompanyName) {
		this.sdcompanyName = sdcompanyName;
	}
}
