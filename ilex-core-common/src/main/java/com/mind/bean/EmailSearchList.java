package com.mind.bean;

import java.io.Serializable;

public class EmailSearchList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String pocname = null;
	private String divison = null;
	private String emailid = null;
	private String pocid = null;

	public String getDivison() {
		return divison;
	}

	public void setDivison(String divison) {
		this.divison = divison;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getPocid() {
		return pocid;
	}

	public void setPocid(String pocid) {
		this.pocid = pocid;
	}

	public String getPocname() {
		return pocname;
	}

	public void setPocname(String pocname) {
		this.pocname = pocname;
	}

}
