package com.mind.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class SearchProblemCategoryBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String keyword = null;
	private String search = null;
	private String problemcategory = null;
	private Collection problemcategorycombo = null;
	private String searchclick = null;
	private ArrayList problemlist = null;
	private String problemcategory_id = null;
	private String problemcategory_desc = null;
	private String search_type = null;
	private String lo_ot_id = null;
	private String poc_email = null;
	
	
	
	public String getProblemcategory_desc() {
		return problemcategory_desc;
	}
	public void setProblemcategory_desc(String problemcategory_desc) {
		this.problemcategory_desc = problemcategory_desc;
	}
	public String getProblemcategory_id() {
		return problemcategory_id;
	}
	public void setProblemcategory_id(String problemcategory_id) {
		this.problemcategory_id = problemcategory_id;
	}
	public String getSearchclick() {
		return searchclick;
	}
	public void setSearchclick(String searchclick) {
		this.searchclick = searchclick;
	}
	public Collection getProblemcategorycombo() {
		return problemcategorycombo;
	}
	public void setProblemcategorycombo(Collection problemcategorycombo) {
		this.problemcategorycombo = problemcategorycombo;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getProblemcategory() {
		return problemcategory;
	}
	public void setProblemcategory(String problemcategory) {
		this.problemcategory = problemcategory;
	}
	public ArrayList getProblemlist() {
		return problemlist;
	}
	public void setProblemlist(ArrayList problemlist) {
		this.problemlist = problemlist;
	}
	public String getSearch_type() {
		return search_type;
	}
	public void setSearch_type(String search_type) {
		this.search_type = search_type;
	}
	public String getLo_ot_id() {
		return lo_ot_id;
	}
	public void setLo_ot_id(String lo_ot_id) {
		this.lo_ot_id = lo_ot_id;
	}
	public String getPoc_email() {
		return poc_email;
	}
	public void setPoc_email(String poc_email) {
		this.poc_email = poc_email;
	}

}
