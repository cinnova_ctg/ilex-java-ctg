package com.mind.bean;

import java.io.Serializable;
import java.util.Collection;

public class DynamicComboEX implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection paragraphidentifier = null;

	public Collection getParagraphidentifier() {
		return paragraphidentifier;
	}

	public void setParagraphidentifier(Collection paragraphidentifier) {
		this.paragraphidentifier = paragraphidentifier;
	}

}
