package com.mind.bean;

public class AddClrCategoryBean 
{
	private String clrservicetime = null;
	private String clrcriticality = null;
	private String criticalityid = null;
	
	
	public String getClrcriticality() {
		return clrcriticality;
	}
	public void setClrcriticality(String clrcriticality) {
		this.clrcriticality = clrcriticality;
	}
	public String getClrservicetime() {
		return clrservicetime;
	}
	public void setClrservicetime(String clrservicetime) {
		this.clrservicetime = clrservicetime;
	}
	public String getCriticalityid() {
		return criticalityid;
	}
	public void setCriticalityid(String criticalityid) {
		this.criticalityid = criticalityid;
	}
	
	
	
}
