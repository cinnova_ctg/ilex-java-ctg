package com.mind.bean.po;

import java.io.Serializable;

public class FreqUsedDeliverableDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String deliverableTitle;
	private String deliverableDescription;
	private String deliverableId;

	public String getDeliverableTitle() {
		return deliverableTitle;
	}

	public void setDeliverableTitle(String deliverableTitle) {
		this.deliverableTitle = deliverableTitle;
	}

	public String getDeliverableDescription() {
		return deliverableDescription;
	}

	public void setDeliverableDescription(String deliverableDescription) {
		this.deliverableDescription = deliverableDescription;
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof FreqUsedDeliverableDTO) {
			if (this.deliverableTitle
					.equals(((FreqUsedDeliverableDTO) object).deliverableTitle)
					&& (this.deliverableDescription
							.equals(((FreqUsedDeliverableDTO) object).deliverableDescription))) {
				return true;

			}
		}

		return false;

	}

	public String getDeliverableId() {
		return deliverableId;
	}

	public void setDeliverableId(String deliverableId) {
		this.deliverableId = deliverableId;
	}
}
