package com.mind.bean.masterScheduler;

public class AutoCompleteMasterSchedulerTaskBean {

	private String apiUser;
	private String apiUserPassword;
	private String userId;
	private String taskTypeId;
	private String taskTypeKey;

	private String url;

	public String getApiUser() {
		return apiUser;
	}

	public void setApiUser(String apiUser) {
		this.apiUser = apiUser;
	}

	public String getApiUserPassword() {
		return apiUserPassword;
	}

	public void setApiUserPassword(String apiUserPassword) {
		this.apiUserPassword = apiUserPassword;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTaskTypeId() {
		return taskTypeId;
	}

	public void setTaskTypeId(String taskTypeId) {
		this.taskTypeId = taskTypeId;
	}

	public String getTaskTypeKey() {
		return taskTypeKey;
	}

	public void setTaskTypeKey(String taskTypeKey) {
		this.taskTypeKey = taskTypeKey;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
