package com.mind.bean.msp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.mind.common.LabelValue;
import com.mind.bean.msp.PCISearchInfo;

/**
 * 
 * @author yogendrasingh
 * 
 */
public class PCIControlSearchBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String orgTopId = "";

	private String selectedSiteID = "";
	private List<LabelValue> customerList;
	private String selectedChangeRequestStatus = "";
	private List<LabelValue> selectedChangeRequestStatusList = new ArrayList<LabelValue>();
	private List<LabelValue> siteIdentifierList = new ArrayList<LabelValue>();

	private List<String> pciControlSearchList;

	private List<PCISearchInfo> pciChangeControlFormList;

	private String fromDate;
	private String toDate;

	public String getOrgTopId() {
		return orgTopId;
	}

	public void setOrgTopId(String orgTopId) {
		this.orgTopId = orgTopId;
	}

	public String getSelectedSiteID() {
		return selectedSiteID;
	}

	public void setSelectedSiteID(String selectedSiteID) {
		this.selectedSiteID = selectedSiteID;
	}

	public List<LabelValue> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<LabelValue> customerList) {
		this.customerList = customerList;
	}

	public String getSelectedChangeRequestStatus() {
		return selectedChangeRequestStatus;
	}

	public void setSelectedChangeRequestStatus(
			String selectedChangeRequestStatus) {
		this.selectedChangeRequestStatus = selectedChangeRequestStatus;
	}

	public List<LabelValue> getSelectedChangeRequestStatusList() {
		return selectedChangeRequestStatusList;
	}

	public void setSelectedChangeRequestStatusList(
			List<LabelValue> selectedChangeRequestStatusList) {
		this.selectedChangeRequestStatusList = selectedChangeRequestStatusList;
	}

	public List<LabelValue> getSiteIdentifierList() {
		return siteIdentifierList;
	}

	public void setSiteIdentifierList(List<LabelValue> siteIdentifierList) {
		this.siteIdentifierList = siteIdentifierList;
	}

	public List<String> getPciControlSearchList() {
		return pciControlSearchList;
	}

	public void setPciControlSearchList(List<String> pciControlSearchList) {
		this.pciControlSearchList = pciControlSearchList;
	}

	public List<PCISearchInfo> getPciChangeControlFormList() {
		return pciChangeControlFormList;
	}

	public void setPciChangeControlFormList(
			List<PCISearchInfo> pciChangeControlFormList) {
		this.pciChangeControlFormList = pciChangeControlFormList;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

}
