package com.mind.bean.msp;

import java.io.Serializable;

/**
 * The Class Bounces.
 */
public class Bounces implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The site. */
	private String site = null;

	/** The date. */
	private String date = null;

	/** The count. */
	private String count = null;

	/**
	 * Gets the count.
	 * 
	 * @return the count
	 */
	public String getCount() {
		return count;
	}

	/**
	 * Sets the count.
	 * 
	 * @param count
	 *            the new count
	 */
	public void setCount(String count) {
		this.count = count;
	}

	/**
	 * Gets the date.
	 * 
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 * 
	 * @param date
	 *            the new date
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Gets the site.
	 * 
	 * @return the site
	 */
	public String getSite() {
		return site;
	}

	/**
	 * Sets the site.
	 * 
	 * @param site
	 *            the new site
	 */
	public void setSite(String site) {
		this.site = site;
	}

}
