package com.mind.bean.msp;


public class ShippingInstructionSiteInfo extends SiteInfo {
	private String msaName = null;

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
}
