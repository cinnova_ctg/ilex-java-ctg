package com.mind.bean.msp;

/**
 * The Class RealTimeStateChanges.
 */
public class RealTimeState {
	/*
	 * Column names which is returned by SP wug_ip_nActiveMonitorStateChangeID
	 * wug_dv_display_name event_day event_time wug_ip_current_status
	 * wug_ip_event_count wug_ip_ticket_reference previous_no_action
	 * rows_not_displayed multiple_days wug_ip_outage_duration
	 */
	/** The wug_ip_nActiveMonitorStateChangeID. */
	private String nActiveMonitorStateChangeID = null;

	// private String jobOwner = null;

	// public String getJobOwner() {
	// return jobOwner;
	// }
	//
	// public void setJobOwner(String jobOwner) {
	// this.jobOwner = jobOwner;
	// }

	private String wugInstance = null;

	public String getWugInstance() {
		return wugInstance;
	}

	public void setWugInstance(String wugInstance) {
		this.wugInstance = wugInstance;
	}

	/** The wug_dv_display_name. */
	private String displayName = null;

	/** The event_day. */
	private String eventDay = null;

	/** The event_time. */
	private String eventTime = null;

	/** The wug_ip_current_status. */
	private String currentStatus = null;

	/** The wug_ip_event_count. */
	private String eventCount = null;

	/** The wug_ip_ticket_reference. */
	private String ticketReference = null;

	/** The previous_no_action. */
	private int previousNoAction = 0;

	/** The rows_not_displayed. */
	private String rowsNotDisplayed = null;

	/** The multiple_days. */
	private String multipleDays = null;

	/** The outage duration. */
	private String outageDuration = null;
	private String lastAction;

	private String nextAction;
	private String ticketId;;

	/**
	 * Gets the current status.
	 * 
	 * @return the current status
	 */
	public String getCurrentStatus() {
		return currentStatus;
	}

	/**
	 * Sets the current status.
	 * 
	 * @param currentStatus
	 *            the new current status
	 */
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	/**
	 * Gets the display name.
	 * 
	 * @return the display name
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Sets the display name.
	 * 
	 * @param displayName
	 *            the new display name
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * Gets the event count.
	 * 
	 * @return the event count
	 */
	public String getEventCount() {
		return eventCount;
	}

	/**
	 * Sets the event count.
	 * 
	 * @param eventCount
	 *            the new event count
	 */
	public void setEventCount(String eventCount) {
		this.eventCount = eventCount;
	}

	/**
	 * Gets the event day.
	 * 
	 * @return the event day
	 */
	public String getEventDay() {
		return eventDay;
	}

	/**
	 * Sets the event day.
	 * 
	 * @param eventDay
	 *            the new event day
	 */
	public void setEventDay(String eventDay) {
		this.eventDay = eventDay;
	}

	/**
	 * Gets the event time.
	 * 
	 * @return the event time
	 */
	public String getEventTime() {
		return eventTime;
	}

	/**
	 * Sets the event time.
	 * 
	 * @param eventTime
	 *            the new event time
	 */
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	/**
	 * Gets the n active monitor state change id.
	 * 
	 * @return the n active monitor state change id
	 */
	public String getNActiveMonitorStateChangeID() {
		return nActiveMonitorStateChangeID;
	}

	/**
	 * Sets the n active monitor state change id.
	 * 
	 * @param activeMonitorStateChangeID
	 *            the new n active monitor state change id
	 */
	public void setNActiveMonitorStateChangeID(String activeMonitorStateChangeID) {
		nActiveMonitorStateChangeID = activeMonitorStateChangeID;
	}

	/**
	 * Gets the ticket reference.
	 * 
	 * @return the ticket reference
	 */
	public String getTicketReference() {
		return ticketReference;
	}

	/**
	 * Sets the ticket reference.
	 * 
	 * @param ticketReference
	 *            the new ticket reference
	 */
	public void setTicketReference(String ticketReference) {
		this.ticketReference = ticketReference;
	}

	/**
	 * Gets the outage duration.
	 * 
	 * @return the outage duration
	 */
	public String getOutageDuration() {
		return outageDuration;
	}

	/**
	 * Sets the outage duration.
	 * 
	 * @param outageDuration
	 *            the new outage duration
	 */
	public void setOutageDuration(String outageDuration) {
		this.outageDuration = outageDuration;
	}

	/**
	 * Gets the multiple days.
	 * 
	 * @return the multiple days
	 */
	public String getMultipleDays() {
		return multipleDays;
	}

	/**
	 * Sets the multiple days.
	 * 
	 * @param multipleDays
	 *            the new multiple days
	 */
	public void setMultipleDays(String multipleDays) {
		this.multipleDays = multipleDays;
	}

	public int getPreviousNoAction() {
		return previousNoAction;
	}

	public void setPreviousNoAction(int previousNoAction) {
		this.previousNoAction = previousNoAction;
	}

	/**
	 * Gets the rows not displayed.
	 * 
	 * @return the rows not displayed
	 */
	public String getRowsNotDisplayed() {
		return rowsNotDisplayed;
	}

	/**
	 * Sets the rows not displayed.
	 * 
	 * @param rowsNotDisplayed
	 *            the new rows not displayed
	 */
	public void setRowsNotDisplayed(String rowsNotDisplayed) {
		this.rowsNotDisplayed = rowsNotDisplayed;
	}

	public String getLastAction() {
		return lastAction;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public void setLastAction(String lastAction) {
		this.lastAction = lastAction;
	}

	public String getNextAction() {
		return nextAction;
	}

	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}

}
