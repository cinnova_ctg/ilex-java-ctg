package com.mind.bean.msp;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;


public class SiteDetailBean implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * Site detail Information
	 */
	private String siteId = "";
	private String siteNumber = "";

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	DeviceInfoBean deviceInfoBean = new DeviceInfoBean();
	SiteInfo siteInfo = new SiteInfo();

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public DeviceInfoBean getDeviceInfoBean() {
		return deviceInfoBean;
	}

	public void setDeviceInfoBean(DeviceInfoBean deviceInfoBean) {
		this.deviceInfoBean = deviceInfoBean;
	}

	public SiteInfo getSiteInfo() {
		return siteInfo;
	}

	public void setSiteInfo(SiteInfo siteInfo) {
		this.siteInfo = siteInfo;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
