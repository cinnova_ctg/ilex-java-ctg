package com.mind.bean.msp;
import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.mind.common.LabelValue;

public class MspSiteSearchBean implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private String customer = null;
	private ArrayList<LabelValue> customerList = new ArrayList<LabelValue>();
	private String siteNumber = null;
	private String address = null;
	private String city = null;
	private String zipcode = null;
	private String accountNumber = null;
	private String sitePhoneNumber=null;
	private String siteInternetLineNumber=null;
	
	public String getSiteInternetLineNumber() {
		return siteInternetLineNumber;
	}
	public void setSiteInternetLineNumber(String siteInternetLineNumber) {
		this.siteInternetLineNumber = siteInternetLineNumber;
	}

	private String state=null;
	private ArrayList<SiteInfo>searchList= new ArrayList<SiteInfo>();

	
	public String getSiteNumber() {
		return siteNumber;
	}
	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getSitePhoneNumber() {
		return sitePhoneNumber;
	}
	public void setSitePhoneNumber(String sitePhoneNumber) {
		this.sitePhoneNumber = sitePhoneNumber;
	}
	public ArrayList<SiteInfo> getSearchList() {
		return searchList;
	}
	public void setSearchList(ArrayList<SiteInfo> searchList) {
		this.searchList = searchList;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	private String search = null;

	public String toString(){
      return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public ArrayList<LabelValue> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(ArrayList<LabelValue> customerList) {
		this.customerList = customerList;
	}
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}



	
	
}
