package com.mind.bean.msp;

import java.io.Serializable;
import java.util.ArrayList;

public class MSPCCReportBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name = null;
	private String value = null;
	private String errorValue = null;
	private String strtDateName = null;
	private String strtDateValue = null;
	private String plannedDateName = null;
	private String plannedDateValue = null;
	private String siteCountName = null;
	private String siteCountValue = null;
	private String lastUpdateDateName = null;
	private String lastUpdateDateValue = null;
	private String totalCompleted = null;
	private String totalErrors = null;
	private String count = null;
	private String[] categoryColumns = null;

	public String[] getCategoryColumns() {
		return categoryColumns;
	}

	public void setCategoryColumns(String[] categoryColumns) {
		this.categoryColumns = categoryColumns;
	}

	public String getCategoryColumns(int i) {
		return categoryColumns[i];
	}

	public String getTotalCompleted() {
		return totalCompleted;
	}

	public void setTotalCompleted(String totalCompleted) {
		this.totalCompleted = totalCompleted;
	}

	public String getTotalErrors() {
		return totalErrors;
	}

	public void setTotalErrors(String totalErrors) {
		this.totalErrors = totalErrors;
	}

	private ArrayList<MSPCCReportBean> sectionAList = null;
	private ArrayList<MSPCCReportBean> sectionCList = null;
	private ArrayList<MSPCCReportBean> sectionDList = null;
	private ArrayList<MSPCCReportBean> sectionEList = null;
	private ArrayList<MSPCCReportBean> sectionFList = null;
	private ArrayList<MSPCCReportBean> sectionBHeaderList = null;

	public ArrayList<MSPCCReportBean> getSectionBHeaderList() {
		return sectionBHeaderList;
	}

	public void setSectionBHeaderList(
			ArrayList<MSPCCReportBean> sectionBHeaderList) {
		this.sectionBHeaderList = sectionBHeaderList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getStrtDateName() {
		return strtDateName;
	}

	public void setStrtDateName(String strtDateName) {
		this.strtDateName = strtDateName;
	}

	public String getStrtDateValue() {
		return strtDateValue;
	}

	public void setStrtDateValue(String strtDateValue) {
		this.strtDateValue = strtDateValue;
	}

	public String getPlannedDateName() {
		return plannedDateName;
	}

	public void setPlannedDateName(String plannedDateName) {
		this.plannedDateName = plannedDateName;
	}

	public String getPlannedDateValue() {
		return plannedDateValue;
	}

	public void setPlannedDateValue(String plannedDateValue) {
		this.plannedDateValue = plannedDateValue;
	}

	public String getSiteCountName() {
		return siteCountName;
	}

	public void setSiteCountName(String siteCountName) {
		this.siteCountName = siteCountName;
	}

	public String getSiteCountValue() {
		return siteCountValue;
	}

	public void setSiteCountValue(String siteCountValue) {
		this.siteCountValue = siteCountValue;
	}

	public String getLastUpdateDateName() {
		return lastUpdateDateName;
	}

	public void setLastUpdateDateName(String lastUpdateDateName) {
		this.lastUpdateDateName = lastUpdateDateName;
	}

	public String getLastUpdateDateValue() {
		return lastUpdateDateValue;
	}

	public void setLastUpdateDateValue(String lastUpdateDateValue) {
		this.lastUpdateDateValue = lastUpdateDateValue;
	}

	public ArrayList<MSPCCReportBean> getSectionAList() {
		return sectionAList;
	}

	public void setSectionAList(ArrayList<MSPCCReportBean> sectionAList) {
		this.sectionAList = sectionAList;
	}

	public ArrayList<MSPCCReportBean> getSectionCList() {
		return sectionCList;
	}

	public void setSectionCList(ArrayList<MSPCCReportBean> sectionCList) {
		this.sectionCList = sectionCList;
	}

	public ArrayList<MSPCCReportBean> getSectionDList() {
		return sectionDList;
	}

	public void setSectionDList(ArrayList<MSPCCReportBean> sectionDList) {
		this.sectionDList = sectionDList;
	}

	public ArrayList<MSPCCReportBean> getSectionEList() {
		return sectionEList;
	}

	public void setSectionEList(ArrayList<MSPCCReportBean> sectionEList) {
		this.sectionEList = sectionEList;
	}

	public ArrayList<MSPCCReportBean> getSectionFList() {
		return sectionFList;
	}

	public void setSectionFList(ArrayList<MSPCCReportBean> sectionFList) {
		this.sectionFList = sectionFList;
	}

	public String getErrorValue() {
		return errorValue;
	}

	public void setErrorValue(String errorValue) {
		this.errorValue = errorValue;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

}
