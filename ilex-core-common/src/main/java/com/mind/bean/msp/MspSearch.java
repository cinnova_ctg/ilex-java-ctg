package com.mind.bean.msp;

import java.io.Serializable;
import java.util.ArrayList;

import com.mind.bean.msp.RealTimeState;

public class MspSearch implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String total = null;
	private ArrayList<RealTimeState> searchList = new ArrayList<RealTimeState>();
	
	public ArrayList<RealTimeState> getSearchList() {
		return searchList;
	}
	public void setSearchList(ArrayList<RealTimeState> searchList) {
		this.searchList = searchList;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}

}
