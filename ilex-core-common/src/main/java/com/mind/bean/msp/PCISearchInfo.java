package com.mind.bean.msp;

/**
 * 
 * @author yogendrasingh
 * 
 */
public class PCISearchInfo {

	private String customer;
	private String status;
	private String site;
	private String changeDiscription;
	private String changeRequestID;

	public String getChangeRequestID() {
		return changeRequestID;
	}

	public void setChangeRequestID(String changeRequestID) {
		this.changeRequestID = changeRequestID;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getChangeDiscription() {
		return changeDiscription;
	}

	public void setChangeDiscription(String changeDiscription) {
		this.changeDiscription = changeDiscription;
	}

}
