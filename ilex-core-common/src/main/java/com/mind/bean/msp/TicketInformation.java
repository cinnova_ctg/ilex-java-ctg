package com.mind.bean.msp;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class TicketInformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String customerName = null;
	private String appendixId = null;
	private String jobId = null;
	private String ticketId = null;
	private String ticketNumber = null;
	private String problemCategory = null;
	private String probleDescription = null;
	private boolean isInstallNotesPresent = false;
	private String latestInstallNotesDate = null;
	private String latestInstallNotes = null;
	private String secondLatestInstallNotesDate = null;
	private String secondLatestInstallNotes = null;
	private String estimatedEffort = null;
	private String issueOwner = null;
	private String correctiveAction = null;
	private String rootCause = null;
	private String[] recentNotes = null;
	private String currentStatus = null;
	private String lastAction = null;
	private String backupCircuitStatus = null;
	private Integer installNotesCount;

	public Integer getInstallNotesCount() {
		return installNotesCount;
	}

	public void setInstallNotesCount(Integer installNotesCount) {
		this.installNotesCount = installNotesCount;
	}

	public String getBackupCircuitStatus() {
		return backupCircuitStatus;
	}

	public void setBackupCircuitStatus(String backupCircuitStatus) {
		this.backupCircuitStatus = backupCircuitStatus;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	public String getCorrectiveAction() {
		return correctiveAction;
	}

	public void setCorrectiveAction(String correctiveAction) {
		this.correctiveAction = correctiveAction;
	}

	public String getEstimatedEffort() {
		return estimatedEffort;
	}

	public void setEstimatedEffort(String estimatedEffort) {
		this.estimatedEffort = estimatedEffort;
	}

	public String getLatestInstallNotes() {
		return latestInstallNotes;
	}

	public void setLatestInstallNotes(String latestInstallNotes) {
		this.latestInstallNotes = latestInstallNotes;
	}

	public String getLatestInstallNotesDate() {
		return latestInstallNotesDate;
	}

	public void setLatestInstallNotesDate(String latestInstallNotesDate) {
		this.latestInstallNotesDate = latestInstallNotesDate;
	}

	public String getSecondLatestInstallNotes() {
		return secondLatestInstallNotes;
	}

	public void setSecondLatestInstallNotes(String secondLatestInstallNotes) {
		this.secondLatestInstallNotes = secondLatestInstallNotes;
	}

	public String getSecondLatestInstallNotesDate() {
		return secondLatestInstallNotesDate;
	}

	public void setSecondLatestInstallNotesDate(
			String secondLatestInstallNotesDate) {
		this.secondLatestInstallNotesDate = secondLatestInstallNotesDate;
	}

	public String getIssueOwner() {
		return issueOwner;
	}

	public void setIssueOwner(String issueOwner) {
		this.issueOwner = issueOwner;
	}

	public String getProbleDescription() {
		return probleDescription;
	}

	public void setProbleDescription(String probleDescription) {
		this.probleDescription = probleDescription;
	}

	public String getProblemCategory() {
		return problemCategory;
	}

	public void setProblemCategory(String problemCategory) {
		this.problemCategory = problemCategory;
	}

	public String[] getRecentNotes() {
		return recentNotes;
	}

	public void setRecentNotes(String[] recentNotes) {
		this.recentNotes = recentNotes;
	}

	public String getRootCause() {
		return rootCause;
	}

	public void setRootCause(String rootCause) {
		this.rootCause = rootCause;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public boolean isInstallNotesPresent() {
		return isInstallNotesPresent;
	}

	public void setInstallNotesPresent(boolean isInstallNotesPresent) {
		this.isInstallNotesPresent = isInstallNotesPresent;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getLastAction() {
		return lastAction;
	}

	public void setLastAction(String lastAction) {
		this.lastAction = lastAction;
	}

}
