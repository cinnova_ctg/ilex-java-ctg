package com.mind.bean.msp;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.mind.bean.msp.MspSearch;
import com.mind.common.LabelValue;

public class MspReportBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<LabelValue> customerList = new ArrayList<LabelValue>();
	private String customerId = null;
	private String fromDate = null;
	private String toDate = null;
	private MspSearch mspSearch = null;
	private String reportType = null; 
	

	public String toString(){
	      return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	    }
	public String getReportType() {
		return reportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	public MspSearch getMspSearch() {
		return mspSearch;
	}
	public void setMspSearch(MspSearch mspSearch) {
		this.mspSearch = mspSearch;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public ArrayList<LabelValue> getCustomerList() {
		return customerList;
	}
	public void setCustomerList(ArrayList<LabelValue> customerList) {
		this.customerList = customerList;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
}
