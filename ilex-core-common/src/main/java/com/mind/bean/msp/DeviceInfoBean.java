package com.mind.bean.msp;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class DeviceInfoBean implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/* Site/Device Information */
	private String circuitType = null;
	private String modemType = null;
	private String internetServiceLineNo = null;
	private String isp = null;
	private String ispSupportNo = null;
	private String circuitIP = null;
	private String siteContactNo = null;
	private String location = null;
	private String jobId = null;
	private String account = null;

	// Added For New SPR #114 Changes
	private String accountNew = null;
	private String VOIP = null;
	private String accessCode = null;
	private String helpDeskNew = null;
	private String fax = null;

	public String getHelpDeskNew() {
		return helpDeskNew;
	}

	public void setHelpDeskNew(String helpDeskNew) {
		this.helpDeskNew = helpDeskNew;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getAccount() {
		return account;
	}

	public String getAccountNew() {
		return accountNew;
	}

	public void setAccountNew(String accountNew) {
		this.accountNew = accountNew;
	}

	public String getVOIP() {
		return VOIP;
	}

	public void setVOIP(String vOIP) {
		VOIP = vOIP;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	public String getCircuitIP() {
		return circuitIP;
	}

	public void setCircuitIP(String circuitIP) {
		this.circuitIP = circuitIP;
	}

	public String getCircuitType() {
		return circuitType;
	}

	public void setCircuitType(String circuitType) {
		this.circuitType = circuitType;
	}

	public String getInternetServiceLineNo() {
		return internetServiceLineNo;
	}

	public void setInternetServiceLineNo(String internetServiceLineNo) {
		this.internetServiceLineNo = internetServiceLineNo;
	}

	public String getIsp() {
		return isp;
	}

	public void setIsp(String isp) {
		this.isp = isp;
	}

	public String getIspSupportNo() {
		return ispSupportNo;
	}

	public void setIspSupportNo(String ispSupportNo) {
		this.ispSupportNo = ispSupportNo;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getModemType() {
		return modemType;
	}

	public void setModemType(String modemType) {
		this.modemType = modemType;
	}

	public String getSiteContactNo() {
		return siteContactNo;
	}

	public void setSiteContactNo(String siteContactNo) {
		this.siteContactNo = siteContactNo;
	}

}
