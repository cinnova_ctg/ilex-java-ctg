package com.mind.bean.msp;

import java.io.Serializable;


public class TicketSearchBean  extends RealTimeState implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String escalationLevel = null;
	private String ticketId = null;
	private String jobId = null;
	
	public String getEscalationLevel() {
		return escalationLevel;
	}
	public void setEscalationLevel(String escalationLevel) {
		this.escalationLevel = escalationLevel;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

}
