package com.mind.bean.msp;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * The Class MonitoringSummary.
 */
public class MonitoringSummary implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The line. */
	private String line = "";

	/** The display symbol. */
	private int displaySymbol = 0;

	/** The active. */
	private int active = 0;

	/** The down. */
	private int down = 0;

	/** The initial assessment. */
	private int initialAssessment = 0;

	/** The help desk. */
	private int helpDesk = 0;

	/** The msp dispatch. */
	private int mspDispatch = 0;

	/** The watch. */
	private int watch = 0;

	/** The bounces. */
	private int bounces = 0;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	/**
	 * Gets the active.
	 * 
	 * @return the active
	 */
	public int getActive() {
		return active;
	}

	/**
	 * Sets the active.
	 * 
	 * @param active
	 *            the new active
	 */
	public void setActive(int active) {
		this.active = active;
	}

	/**
	 * Gets the bounces.
	 * 
	 * @return the bounces
	 */
	public int getBounces() {
		return bounces;
	}

	/**
	 * Sets the bounces.
	 * 
	 * @param bounces
	 *            the new bounces
	 */
	public void setBounces(int bounces) {
		this.bounces = bounces;
	}

	/**
	 * Gets the display symbol.
	 * 
	 * @return the display symbol
	 */
	public int getDisplaySymbol() {
		return displaySymbol;
	}

	/**
	 * Sets the display symbol.
	 * 
	 * @param displaySymbol
	 *            the new display symbol
	 */
	public void setDisplaySymbol(int displaySymbol) {
		this.displaySymbol = displaySymbol;
	}

	/**
	 * Gets the down.
	 * 
	 * @return the down
	 */
	public int getDown() {
		return down;
	}

	/**
	 * Sets the down.
	 * 
	 * @param down
	 *            the new down
	 */
	public void setDown(int down) {
		this.down = down;
	}

	/**
	 * Gets the help desk.
	 * 
	 * @return the help desk
	 */
	public int getHelpDesk() {
		return helpDesk;
	}

	/**
	 * Sets the help desk.
	 * 
	 * @param helpDesk
	 *            the new help desk
	 */
	public void setHelpDesk(int helpDesk) {
		this.helpDesk = helpDesk;
	}

	/**
	 * Gets the initial assessment.
	 * 
	 * @return the initial assessment
	 */
	public int getInitialAssessment() {
		return initialAssessment;
	}

	/**
	 * Sets the initial assessment.
	 * 
	 * @param initialAssessment
	 *            the new initial assessment
	 */
	public void setInitialAssessment(int initialAssessment) {
		this.initialAssessment = initialAssessment;
	}

	/**
	 * Gets the line.
	 * 
	 * @return the line
	 */
	public String getLine() {
		return line;
	}

	/**
	 * Sets the line.
	 * 
	 * @param line
	 *            the new line
	 */
	public void setLine(String line) {
		this.line = line;
	}

	/**
	 * Gets the msp dispatch.
	 * 
	 * @return the msp dispatch
	 */
	public int getMspDispatch() {
		return mspDispatch;
	}

	/**
	 * Sets the msp dispatch.
	 * 
	 * @param mspDispatch
	 *            the new msp dispatch
	 */
	public void setMspDispatch(int mspDispatch) {
		this.mspDispatch = mspDispatch;
	}

	/**
	 * Gets the watch.
	 * 
	 * @return the watch
	 */
	public int getWatch() {
		return watch;
	}

	/**
	 * Sets the watch.
	 * 
	 * @param watch
	 *            the new watch
	 */
	public void setWatch(int watch) {
		this.watch = watch;
	}
}
