package com.mind.bean.msp;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * @purpose This class is used for the representation of the Site Information.
 */
public class SiteInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String site_country = "";
	private String site_city = "";
	private String site_state = "";
	private String site_zip = "";
	private String site_number = "";
	private String site_name = "";
	private String site_address = "";
	private String site_primary_poc = "";
	private String site_secondary_poc = "";
	private String site_primary_poc_email = "";
	private String site_secondary_poc_email = "";
	private String site_primary_phone = "";
	private String site_secondary_phone = "";
	private String site_time_zone = "";
	private String site_latitude = "";
	private String site_longitude = "";
	private String site_customer = "";
	private String priority;

	public String getSite_customer() {
		return site_customer;
	}

	public void setSite_customer(String site_customer) {
		this.site_customer = site_customer;
	}

	private boolean isUS;
	private String site_id = "";

	public String getSite_id() {
		return site_id;
	}

	public void setSite_id(String siteId) {
		site_id = siteId;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	public boolean isUS() {
		return isUS;
	}

	public void setUS(boolean isUS) {
		this.isUS = isUS;
	}

	public String getSite_city() {
		return site_city;
	}

	public void setSite_city(String site_city) {
		this.site_city = site_city;
	}

	public String getSite_country() {
		return site_country;
	}

	public void setSite_country(String site_country) {
		this.site_country = site_country;
	}

	public String getSite_number() {
		return site_number;
	}

	public void setSite_number(String site_number) {
		this.site_number = site_number;
	}

	public String getSite_state() {
		return site_state;
	}

	public void setSite_state(String site_state) {
		this.site_state = site_state;
	}

	public String getSite_primary_poc() {
		return site_primary_poc;
	}

	public void setSite_primary_poc(String primary_poc) {
		this.site_primary_poc = primary_poc;
	}

	public String getSite_secondary_poc() {
		return site_secondary_poc;
	}

	public void setSite_secondary_poc(String secondary_poc) {
		this.site_secondary_poc = secondary_poc;
	}

	public String getSite_address() {
		return site_address;
	}

	public void setSite_address(String site_address) {
		this.site_address = site_address;
	}

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public String getSite_primary_poc_email() {
		return site_primary_poc_email;
	}

	public void setSite_primary_poc_email(String primary_poc_email) {
		this.site_primary_poc_email = primary_poc_email;
	}

	public String getSite_secondary_poc_email() {
		return site_secondary_poc_email;
	}

	public void setSite_secondary_poc_email(String secondary_poc_email) {
		this.site_secondary_poc_email = secondary_poc_email;
	}

	public String getSite_primary_phone() {
		return site_primary_phone;
	}

	public void setSite_primary_phone(String site_primary_phone) {
		this.site_primary_phone = site_primary_phone;
	}

	public String getSite_secondary_phone() {
		return site_secondary_phone;
	}

	public void setSite_secondary_phone(String site_secondary_phone) {
		this.site_secondary_phone = site_secondary_phone;
	}

	public String getSite_zip() {
		return site_zip;
	}

	public void setSite_zip(String site_zip) {
		this.site_zip = site_zip;
	}

	public String getSite_time_zone() {
		return site_time_zone;
	}

	public void setSite_time_zone(String site_time_zone) {
		this.site_time_zone = site_time_zone;
	}

	public String getSite_latitude() {
		return site_latitude;
	}

	public void setSite_latitude(String site_latitude) {
		this.site_latitude = site_latitude;
	}

	public String getSite_longitude() {
		return site_longitude;
	}

	public void setSite_longitude(String site_longitude) {
		this.site_longitude = site_longitude;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}
}
