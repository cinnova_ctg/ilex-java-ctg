package com.mind.bean.msp;

import java.io.Serializable;

public class ChronicSite implements Serializable {

	private static final long serialVersionUID = 1L;
	private String site = null;
	private String upTime = null;
	private String credits = null;
	
	public String getCredits() {
		return credits;
	}
	public void setCredits(String credits) {
		this.credits = credits;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getUpTime() {
		return upTime;
	}
	public void setUpTime(String upTime) {
		this.upTime = upTime;
	}
}
