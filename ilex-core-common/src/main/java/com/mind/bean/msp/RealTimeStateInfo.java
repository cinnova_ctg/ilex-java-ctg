package com.mind.bean.msp;

/**
 * The Class RealTimeStateInfo.
 */
public class RealTimeStateInfo {

	/** The page flag. */
	private int pageFlag = 0;

	/** The run time alert. */
	private int runTimeAlert = 0;

	/** The run time display. */
	private String runTimeDisplay = null;

	/** The event count. */
	private int eventCount = 0;

	/** The site errors. */
	private int siteErrors = 0;

	/**
	 * Gets the run time display.
	 * 
	 * @return the run time display
	 */
	public String getRunTimeDisplay() {
		return runTimeDisplay;
	}

	/**
	 * Sets the run time display.
	 * 
	 * @param timeDisplay
	 *            the new run time display
	 */
	public void setRunTimeDisplay(String timeDisplay) {
		this.runTimeDisplay = timeDisplay;
	}

	/**
	 * Gets the event count.
	 * 
	 * @return the event count
	 */
	public int getEventCount() {
		return eventCount;
	}

	/**
	 * Sets the event count.
	 * 
	 * @param eventCount
	 *            the new event count
	 */
	public void setEventCount(int eventCount) {
		this.eventCount = eventCount;
	}

	/**
	 * Gets the page flag.
	 * 
	 * @return the page flag
	 */
	public int getPageFlag() {
		return pageFlag;
	}

	/**
	 * Sets the page flag.
	 * 
	 * @param pageFlag
	 *            the new page flag
	 */
	public void setPageFlag(int pageFlag) {
		this.pageFlag = pageFlag;
	}

	/**
	 * Gets the run time alert.
	 * 
	 * @return the run time alert
	 */
	public int getRunTimeAlert() {
		return runTimeAlert;
	}

	/**
	 * Sets the run time alert.
	 * 
	 * @param runTimeAlert
	 *            the new run time alert
	 */
	public void setRunTimeAlert(int runTimeAlert) {
		this.runTimeAlert = runTimeAlert;
	}

	/**
	 * Gets the site errors.
	 * 
	 * @return the site errors
	 */
	public int getSiteErrors() {
		return siteErrors;
	}

	/**
	 * Sets the site errors.
	 * 
	 * @param siteErrors
	 *            the new site errors
	 */
	public void setSiteErrors(int siteErrors) {
		this.siteErrors = siteErrors;
	}
}
