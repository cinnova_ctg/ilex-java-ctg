package com.mind.bean.msp;

import java.io.Serializable;
import java.util.ArrayList;

import com.mind.common.LabelValue;

public class SystemPerformanceBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String customer = null;
	private ArrayList<LabelValue> customerList = new ArrayList<LabelValue>();
	private String xCordinateDate = null;
	private String yCordinateSLA = null;
	private String yCordinateBounces = null;
	private String yCordinateCount = null;
	private String slaGraphXML = null;
	private String bouncesGraphXML = null;
	private String countsGraphXML = null;
	private String customerId = null;
	
	private String slaTable = "";
	private String countTable = "";
	private String costTable = "";
	
	public String getSlaTable() {
		return slaTable;
	}
	public void setSlaTable(String slaTable) {
		this.slaTable = slaTable;
	}
	public String getCountTable() {
		return countTable;
	}
	public void setCountTable(String countTable) {
		this.countTable = countTable;
	}
	
	public String getCostTable() {
		return costTable;
	}
	public void setCostTable(String costTable) {
		this.costTable = costTable;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getSlaGraphXML() {
		return slaGraphXML;
	}
	public void setSlaGraphXML(String slaGraphXML) {
		this.slaGraphXML = slaGraphXML;
	}
	public String getBouncesGraphXML() {
		return bouncesGraphXML;
	}
	public void setBouncesGraphXML(String bouncesGraphXML) {
		this.bouncesGraphXML = bouncesGraphXML;
	}
	public String getCountsGraphXML() {
		return countsGraphXML;
	}
	public void setCountsGraphXML(String countsGraphXML) {
		this.countsGraphXML = countsGraphXML;
	}
	public String getxCordinateDate() {
		return xCordinateDate;
	}
	public void setxCordinateDate(String xCordinateDate) {
		this.xCordinateDate = xCordinateDate;
	}
	public String getyCordinateSLA() {
		return yCordinateSLA;
	}
	public void setyCordinateSLA(String yCordinateSLA) {
		this.yCordinateSLA = yCordinateSLA;
	}
	public String getyCordinateBounces() {
		return yCordinateBounces;
	}
	public void setyCordinateBounces(String yCordinateBounces) {
		this.yCordinateBounces = yCordinateBounces;
	}
	public String getyCordinateCount() {
		return yCordinateCount;
	}
	public void setyCordinateCount(String yCordinateCount) {
		this.yCordinateCount = yCordinateCount;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public ArrayList<LabelValue> getCustomerList() {
		return customerList;
	}
	public void setCustomerList(ArrayList<LabelValue> customerList) {
		this.customerList = customerList;
	}
}
