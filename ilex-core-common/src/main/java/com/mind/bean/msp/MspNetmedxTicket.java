package com.mind.bean.msp;

import java.io.Serializable;

/**
 * The Class MspNetmedxTicket.
 * 
 * @author vijay kumar singh
 */
public class MspNetmedxTicket implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String siteName;
	private String createdDate;
	private String ownerName;
	private String ticketNumber;
	private String ticketId;
	private String jobId;
	private String appendixId;
	private String customerAppendixName;

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getCustomerAppendixName() {
		return customerAppendixName;
	}

	public void setCustomerAppendixName(String customerAppendixName) {
		this.customerAppendixName = customerAppendixName;
	}

}
