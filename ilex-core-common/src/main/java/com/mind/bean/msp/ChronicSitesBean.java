package com.mind.bean.msp;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.mind.common.LabelValue;

public class ChronicSitesBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private String customerName = null;
	private ArrayList<ChronicSite> chronicSiteList = null;
	private ArrayList<LabelValue> customerList = null;
	
	public String toString(){
	      return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	    }
	
	public ArrayList<ChronicSite> getChronicSiteList() {
		return chronicSiteList;
	}
	public void setChronicSiteList(ArrayList<ChronicSite> chronicSiteList) {
		this.chronicSiteList = chronicSiteList;
	}
	public ArrayList<LabelValue> getCustomerList() {
		return customerList;
	}
	public void setCustomerList(ArrayList<LabelValue> customerList) {
		this.customerList = customerList;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

}
