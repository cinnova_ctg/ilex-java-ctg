package com.mind.bean;

import java.io.Serializable;

public class PartnerSOWAssumptionBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String actCategory = null;
	private String actId = null;
	private String actName = null;
	private String actType = null; 
	private String actStatus = null;
	private String actQty = null;
	
	private String SOWAssumptionId = null;
	private String SOWAssumptionValue = null;
	private String SOWAssumptionType = null;
	private String fromId = null;
	private String fromType = null;
	private String viewType = null;
	
	private String dataType = null;
	private String context = null;
	private String contextType = null;
	
	private String netType = null;
	private String viewJobType = null;
	private String ownerId = null;
	private String partnerInformation = null;
	private String deleteSOWAssumptionId = null;
	private String addFlag = null;
	
	private String appendixId = null;
	private String msaId = null;
	private String msaName = null;
	private String appendixName = null;
	private String jobName = null;
	private String fromPage = null;
	
	
//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	
	
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
			
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getAddFlag() {
		return addFlag;
	}
	public void setAddFlag(String addFlag) {
		this.addFlag = addFlag;
	}
	public String getPartnerInformation() {
		return partnerInformation;
	}
	public void setPartnerInformation(String partnerInformation) {
		this.partnerInformation = partnerInformation;
	}
	public String getDeleteSOWAssumptionId() {
		return deleteSOWAssumptionId;
	}
	public void setDeleteSOWAssumptionId(String deleteSOWAssumptionId) {
		this.deleteSOWAssumptionId = deleteSOWAssumptionId;
	}
	public String getActCategory() {
		return actCategory;
	}
	public void setActCategory(String actCategory) {
		this.actCategory = actCategory;
	}
	public String getActId() {
		return actId;
	}
	public void setActId(String actId) {
		this.actId = actId;
	}
	public String getActName() {
		return actName;
	}
	public void setActName(String actName) {
		this.actName = actName;
	}
	public String getActStatus() {
		return actStatus;
	}
	public void setActStatus(String actStatus) {
		this.actStatus = actStatus;
	}
	public String getActType() {
		return actType;
	}
	public void setActType(String actType) {
		this.actType = actType;
	}
	public String getFromType() {
		return fromType;
	}
	public void setFromType(String fromType) {
		this.fromType = fromType;
	}
	public String getSOWAssumptionType() {
		return SOWAssumptionType;
	}
	public void setSOWAssumptionType(String assumptionType) {
		SOWAssumptionType = assumptionType;
	}
	public String getFromId() {
		return fromId;
	}
	public void setFromId(String fromId) {
		this.fromId = fromId;
	}
	public String getActQty() {
		return actQty;
	}
	public void setActQty(String actQty) {
		this.actQty = actQty;
	}
	public String getSOWAssumptionValue() {
		return SOWAssumptionValue;
	}
	public void setSOWAssumptionValue(String assumptionValue) {
		SOWAssumptionValue = assumptionValue;
	}
	public String getViewType() {
		return viewType;
	}
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}
	public String getSOWAssumptionId() {
		return SOWAssumptionId;
	}
	public void setSOWAssumptionId(String assumptionId) {
		SOWAssumptionId = assumptionId;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public String getContextType() {
		return contextType;
	}
	public void setContextType(String contextType) {
		this.contextType = contextType;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getViewJobType() {
		return viewJobType;
	}
	public void setViewJobType(String viewJobType) {
		this.viewJobType = viewJobType;
	}
	public String getNetType() {
		return netType;
	}
	public void setNetType(String netType) {
		this.netType = netType;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

}
