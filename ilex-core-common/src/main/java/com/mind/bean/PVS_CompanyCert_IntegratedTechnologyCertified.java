package com.mind.bean;

import java.io.Serializable;

public class PVS_CompanyCert_IntegratedTechnologyCertified implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String itcompany = null;
	private String validFromIT = null;
	private String validToIT = null;
	private String itcompanyName = null;

	public PVS_CompanyCert_IntegratedTechnologyCertified(String itcompany,
			String fromIT, String toIT, String itcompanyName) {
		this.itcompany = itcompany;
		validFromIT = fromIT;
		validToIT = toIT;
		this.itcompanyName = itcompanyName;
	}

	public PVS_CompanyCert_IntegratedTechnologyCertified(String itcompany,
			String fromIT, String toIT) {
		this.itcompany = itcompany;
		validFromIT = fromIT;
		validToIT = toIT;
	}

	public PVS_CompanyCert_IntegratedTechnologyCertified() {
		super();
	}

	public String getItcompany() {
		return itcompany;
	}

	public void setItcompany(String itcompany) {
		this.itcompany = itcompany;
	}

	public String getValidFromIT() {
		return validFromIT;
	}

	public void setValidFromIT(String validFromIT) {
		this.validFromIT = validFromIT;
	}

	public String getValidToIT() {
		return validToIT;
	}

	public void setValidToIT(String validToIT) {
		this.validToIT = validToIT;
	}

	public String getItcompanyName() {
		return itcompanyName;
	}

	public void setItcompanyName(String itcompanyName) {
		this.itcompanyName = itcompanyName;
	}

}
