package com.mind.bean;

import java.io.Serializable;
import java.util.Collection;

public class DynamicComboCM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection firstlevelcatglist = null;
	private Collection toolKitlist = null;
	private Collection secondlevelcatglist = null;
	private Collection thirdlevelcatglist = null;
	private Collection partnerstatus = null;
	private Collection mcsaVersion = null;
	private Collection partnerType = null;
	private Collection unionSite = null;
	private Collection ownerForAAppendix = null;
	private Collection statusList = null;

	private Collection recommendedlist = null;
	private Collection restrictedlist = null;

	private Collection workCenterList = null;

	public Collection getStatusList() {
		return statusList;
	}

	public void setStatusList(Collection statusList) {
		this.statusList = statusList;
	}

	public Collection getMcsaVersion() {
		return mcsaVersion;
	}

	public void setMcsaVersion(Collection mcsaVersion) {
		this.mcsaVersion = mcsaVersion;
	}

	public Collection getSecondlevelcatglist() {
		return secondlevelcatglist;
	}

	public void setSecondlevelcatglist(Collection slcatglist) {
		this.secondlevelcatglist = slcatglist;
	}

	public Collection getThirdlevelcatglist() {
		return thirdlevelcatglist;
	}

	public void setThirdlevelcatglist(Collection thirdlevelcatglist) {
		this.thirdlevelcatglist = thirdlevelcatglist;
	}

	public Collection getFirstlevelcatglist() {
		return firstlevelcatglist;
	}

	public void setFirstlevelcatglist(Collection firstlevelcatglist) {
		this.firstlevelcatglist = firstlevelcatglist;
	}

	public Collection getPartnerstatus() {
		return partnerstatus;
	}

	public void setPartnerstatus(Collection partnerstatus) {
		this.partnerstatus = partnerstatus;
	}

	public Collection getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(Collection partnerType) {
		this.partnerType = partnerType;
	}

	public Collection getUnionSite() {
		return unionSite;
	}

	public void setUnionSite(Collection unionSite) {
		this.unionSite = unionSite;
	}

	public Collection getOwnerForAAppendix() {
		return ownerForAAppendix;
	}

	public void setOwnerForAAppendix(Collection ownerForAAppendix) {
		this.ownerForAAppendix = ownerForAAppendix;
	}

	public Collection getRecommendedlist() {
		return recommendedlist;
	}

	public void setRecommendedlist(Collection recommendedlist) {
		this.recommendedlist = recommendedlist;
	}

	public Collection getRestrictedlist() {
		return restrictedlist;
	}

	public void setRestrictedlist(Collection restrictedlist) {
		this.restrictedlist = restrictedlist;
	}

	public Collection getToolKitlist() {
		return toolKitlist;
	}

	public void setToolKitlist(Collection toolKitlist) {
		this.toolKitlist = toolKitlist;
	}

	public Collection getWorkCenterList() {
		return workCenterList;
	}

	public void setWorkCenterList(Collection workCenterList) {
		this.workCenterList = workCenterList;
	}
}
