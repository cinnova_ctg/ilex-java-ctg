package com.mind.bean.mpo;

import java.util.ArrayList;
import java.util.Collection;

import com.mind.bean.newjobdb.EInvoiceItems;
import com.mind.common.LabelValue;

public class PODTO extends MPODTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appendixId = null;
	private String jobId = null;
	private String poNumber;
	private String poType;
	private String masterPOItem;
	private String masterPOItemDesc;
	private String terms;
	private String termsDesc;

	private ArrayList po;
	private ArrayList masterpOItem;
	private ArrayList poTerms;
	private ArrayList partnerPoc;
	private String contactPoc = "";
	private ArrayList<LabelValue> hourList = null;
	private ArrayList<LabelValue> minuteList = null;

	private String revisionLevel;
	private String deliveryDate;
	private String deliveryHour;
	private String deliveryMinute;
	private String deliveryIsAm;
	private String issueDate;
	private String issueHour;
	private String issueMinute;
	private String issueIsAm;
	private String poId;
	private String partner;
	private String contact;
	private String poStatus;
	private String authorizedPaymentMethod;
	private String parentTabType;
	private String isSelectedInPO[] = null;
	private String activityName[] = null;
	private String estimatedCostTotal[] = null;
	private String authorizedTotalCost;
	private String mpoTotalCost;
	private String deltaCost;
	private String tabId;
	private String resourceName[] = null;
	private String revision[] = null;
	private String woSpecialInstructions;
	private String poFooterCreateDate;
	private String poFooterChangeDate;
	private String poFooterSource;
	private String devTitle;
	private String devType;
	private String devFormat;
	private String devDescription;
	private String partnerPOId;
	private String save;
	private String poDocId;
	private String isEditablePO = null;
	private String poStatusId = null;

	private String devStatus;
	private String devStatusName;
	private String devDate;

	private String uploadDeliverable;

	private String poFooterCreateBy;
	private String poFooterChangeBy;

	/* eInvoice */
	private String invoiceId = null;
	private String invoiceNumber = null;
	private String invoiceType = null;
	private String invoiceStatus = null;
	private String invoiceDate = null;
	private String invoiceReceivedDate = null;
	private String invoicePartnerComment = null;
	private String invoiceRecivedBy = null;
	private String invoiceApprovedDate = null;
	private String invoiceApprovedBy = null;
	private String invoiceExplanation = null;
	private String invoicePaidDate = null;
	private String invoiceCheckNumber = null;
	private String invoiceAuthorisedTotal = null;
	private String invoiceTotal = null;
	private String invoiceApprovedTotal = null;
	private String invoiceLastUpdatedBy = null;
	private String invoiceLastUpdate = null;
	private String lineTaxAmount = null;
	private String lineTotal = null;
	private ArrayList<EInvoiceItems> invoiceItems = new ArrayList<EInvoiceItems>();
	private String invoiceDisposition = null;
	private String explanationToPartner = null;
	private String updateQuantity = null;

	private String partnerType = null;
	private String checkInTransit[] = null;

	// element added for saveDeliverablesdocument
	private String job_Name = null;
	private String wO_ID = null;
	private String partner_Name = null;
	private String msaId = null;
	private String appendix_Name = null;
	private String mSA_Name = null;
	private String userName = null;

	private String partnerAddress = null;
	private String partnerCountry = null;
	private String partnerCity = null;
	private String partnerState = null;
	private String partnerLatitude = null;
	private String partnerLongitude = null;

	private String jobAddress = null;
	private String jobCountry = null;
	private String jobCity = null;
	private String jobState = null;
	private String jobLatitude = null;
	private String jobLongitude = null;

	private String appendixType = null;
	private String authorizedCostInput = null;

	private String isNewPO = null;

	private char mobileUploadAllowed;
	private String uploadSource;
	private int pvsJustification;
	// added for geographic constraint
	private ArrayList geographicConstraintList;
	private int geographicConstraint;
	private String[] cmboxCoreCompetency;

	private boolean devMobile = true;

	public String getUploadSource() {
		return uploadSource;
	}

	public void setUploadSource(String uploadSource) {
		this.uploadSource = uploadSource;
	}

	public char getMobileUploadAllowed() {
		return mobileUploadAllowed;
	}

	public void setMobileUploadAllowed(char mobileUploadAllowed) {
		this.mobileUploadAllowed = mobileUploadAllowed;
	}

	/*
	 * property to check the whether the Override Minuteman Cost CheckBox is
	 * Checked or Unchecked
	 */
	private String overrideMinumtemanCostChecked;
	private String overrideMinumtemanCostCheckedHidden;

	public String getOverrideMinumtemanCostChecked() {
		return overrideMinumtemanCostChecked;
	}

	public void setOverrideMinumtemanCostChecked(
			String overrideMinumtemanCostChecked) {
		this.overrideMinumtemanCostChecked = overrideMinumtemanCostChecked;
	}

	public String getIsNewPO() {
		return isNewPO;
	}

	public void setIsNewPO(String isNewPO) {
		this.isNewPO = isNewPO;
	}

	public String getAppendixType() {
		return appendixType;
	}

	public void setAppendixType(String appendixType) {
		this.appendixType = appendixType;
	}

	public String getAuthorizedCostInput() {
		return authorizedCostInput;
	}

	public void setAuthorizedCostInput(String authorizedCostInput) {
		this.authorizedCostInput = authorizedCostInput;
	}

	public String getUpdateQuantity() {
		return updateQuantity;
	}

	public void setUpdateQuantity(String updateQuantity) {
		this.updateQuantity = updateQuantity;
	}

	public String getExplanationToPartner() {
		return explanationToPartner;
	}

	public void setExplanationToPartner(String explanationToPartner) {
		this.explanationToPartner = explanationToPartner;
	}

	public String getInvoiceDisposition() {
		return invoiceDisposition;
	}

	public void setInvoiceDisposition(String invoiceDisposition) {
		this.invoiceDisposition = invoiceDisposition;
	}

	public String getPoStatusId() {
		return poStatusId;
	}

	public void setPoStatusId(String poStatusId) {
		this.poStatusId = poStatusId;
	}

	public String getIsEditablePO() {
		return isEditablePO;
	}

	public void setIsEditablePO(String isEditablePO) {
		this.isEditablePO = isEditablePO;
	}

	public String getPoDocId() {
		return poDocId;
	}

	public void setPoDocId(String poDocId) {
		this.poDocId = poDocId;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public String getPartnerPOId() {
		return partnerPOId;
	}

	public void setPartnerPOId(String partnerPOId) {
		this.partnerPOId = partnerPOId;
	}

	public String getWoSpecialInstructions() {
		return woSpecialInstructions;
	}

	public void setWoSpecialInstructions(String woSpecialInstructions) {
		this.woSpecialInstructions = woSpecialInstructions;
	}

	public String[] getRevision() {
		return revision;
	}

	public void setRevision(String[] revision) {
		this.revision = revision;
	}

	public String[] getResourceName() {
		return resourceName;
	}

	public void setResourceName(String[] resourceName) {
		this.resourceName = resourceName;
	}

	public String getAuthorizedTotalCost() {
		return authorizedTotalCost;
	}

	public void setAuthorizedTotalCost(String authorizedTotalCost) {
		this.authorizedTotalCost = authorizedTotalCost;
	}

	public String getDeltaCost() {
		return deltaCost;
	}

	public void setDeltaCost(String deltaCost) {
		this.deltaCost = deltaCost;
	}

	public String getMpoTotalCost() {
		return mpoTotalCost;
	}

	public void setMpoTotalCost(String mpoTotalCost) {
		this.mpoTotalCost = mpoTotalCost;
	}

	public String[] getEstimatedCostTotal() {
		return estimatedCostTotal;
	}

	public void setEstimatedCostTotal(String[] estimatedCostTotal) {
		this.estimatedCostTotal = estimatedCostTotal;
	}

	public String[] getActivityName() {
		return activityName;
	}

	public void setActivityName(String[] activityName) {
		this.activityName = activityName;
	}

	public String[] getIsSelectedInPO() {
		return isSelectedInPO;
	}

	public void setIsSelectedInPO(String[] isSelectedInPO) {
		this.isSelectedInPO = isSelectedInPO;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getAuthorizedPaymentMethod() {
		return authorizedPaymentMethod;
	}

	public void setAuthorizedPaymentMethod(String authorizedPaymentMethod) {
		this.authorizedPaymentMethod = authorizedPaymentMethod;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getMasterPOItem() {
		return masterPOItem;
	}

	public void setMasterPOItem(String masterPOItem) {
		this.masterPOItem = masterPOItem;
	}

	public String getParentTabType() {
		return parentTabType;
	}

	public void setParentTabType(String parentTabType) {
		this.parentTabType = parentTabType;
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getPoStatus() {
		return poStatus;
	}

	public void setPoStatus(String poStatus) {
		this.poStatus = poStatus;
	}

	public String getPoType() {
		return poType;
	}

	public void setPoType(String poType) {
		this.poType = poType;
	}

	public String getRevisionLevel() {
		return revisionLevel;
	}

	public void setRevisionLevel(String revisionLevel) {
		this.revisionLevel = revisionLevel;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDeliveryHour() {
		return deliveryHour;
	}

	public void setDeliveryHour(String deliveryHour) {
		this.deliveryHour = deliveryHour;
	}

	public String getDeliveryIsAm() {
		return deliveryIsAm;
	}

	public void setDeliveryIsAm(String deliveryIsAm) {
		this.deliveryIsAm = deliveryIsAm;
	}

	public String getDeliveryMinute() {
		return deliveryMinute;
	}

	public void setDeliveryMinute(String deliveryMinute) {
		this.deliveryMinute = deliveryMinute;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getIssueHour() {
		return issueHour;
	}

	public void setIssueHour(String issueHour) {
		this.issueHour = issueHour;
	}

	public String getIssueIsAm() {
		return issueIsAm;
	}

	public void setIssueIsAm(String issueIsAm) {
		this.issueIsAm = issueIsAm;
	}

	public String getIssueMinute() {
		return issueMinute;
	}

	public void setIssueMinute(String issueMinute) {
		this.issueMinute = issueMinute;
	}

	public String getPoId() {
		return poId;
	}

	public void setPoId(String poId) {
		this.poId = poId;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public Collection getMasterpOItem() {
		return masterpOItem;
	}

	public ArrayList getPo() {
		return po;
	}

	public void setPo(ArrayList po) {
		this.po = po;
	}

	public ArrayList getPoTerms() {
		return poTerms;
	}

	public void setPoTerms(ArrayList poTerms) {
		this.poTerms = poTerms;
	}

	public void setMasterpOItem(ArrayList masterpOItem) {
		this.masterpOItem = masterpOItem;
	}

	public ArrayList getPartnerPoc() {
		return partnerPoc;
	}

	public void setPartnerPoc(ArrayList partnerPoc) {
		this.partnerPoc = partnerPoc;
	}

	public String getPoFooterChangeDate() {
		return poFooterChangeDate;
	}

	public void setPoFooterChangeDate(String poFooterChangeDate) {
		this.poFooterChangeDate = poFooterChangeDate;
	}

	public String getPoFooterCreateDate() {
		return poFooterCreateDate;
	}

	public void setPoFooterCreateDate(String poFooterCreateDate) {
		this.poFooterCreateDate = poFooterCreateDate;
	}

	public String getPoFooterSource() {
		return poFooterSource;
	}

	public void setPoFooterSource(String poFooterSource) {
		this.poFooterSource = poFooterSource;
	}

	public String getDevDescription() {
		return devDescription;
	}

	public void setDevDescription(String devDescription) {
		this.devDescription = devDescription;
	}

	public String getDevFormat() {
		return devFormat;
	}

	public void setDevFormat(String devFormat) {
		this.devFormat = devFormat;
	}

	public String getDevTitle() {
		return devTitle;
	}

	public void setDevTitle(String devTitle) {
		this.devTitle = devTitle;
	}

	public String getDevType() {
		return devType;
	}

	public void setDevType(String devType) {
		this.devType = devType;
	}

	public ArrayList<LabelValue> getHourList() {
		return hourList;
	}

	public void setHourList(ArrayList<LabelValue> hourList) {
		this.hourList = hourList;
	}

	public ArrayList<LabelValue> getMinuteList() {
		return minuteList;
	}

	public void setMinuteList(ArrayList<LabelValue> minuteList) {
		this.minuteList = minuteList;
	}

	public String getContactPoc() {
		return contactPoc;
	}

	public void setContactPoc(String contactPoc) {
		this.contactPoc = contactPoc;
	}

	public String getInvoiceApprovedBy() {
		return invoiceApprovedBy;
	}

	public void setInvoiceApprovedBy(String invoiceApprovedBy) {
		this.invoiceApprovedBy = invoiceApprovedBy;
	}

	public String getInvoiceApprovedDate() {
		return invoiceApprovedDate;
	}

	public void setInvoiceApprovedDate(String invoiceApprovedDate) {
		this.invoiceApprovedDate = invoiceApprovedDate;
	}

	public String getInvoiceApprovedTotal() {
		return invoiceApprovedTotal;
	}

	public void setInvoiceApprovedTotal(String invoiceApprovedTotal) {
		this.invoiceApprovedTotal = invoiceApprovedTotal;
	}

	public String getInvoiceAuthorisedTotal() {
		return invoiceAuthorisedTotal;
	}

	public void setInvoiceAuthorisedTotal(String invoiceAuthorisedTotal) {
		this.invoiceAuthorisedTotal = invoiceAuthorisedTotal;
	}

	public String getInvoiceCheckNumber() {
		return invoiceCheckNumber;
	}

	public void setInvoiceCheckNumber(String invoiceCheckNumber) {
		this.invoiceCheckNumber = invoiceCheckNumber;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceExplanation() {
		return invoiceExplanation;
	}

	public void setInvoiceExplanation(String invoiceExplanation) {
		this.invoiceExplanation = invoiceExplanation;
	}

	public String getInvoiceLastUpdate() {
		return invoiceLastUpdate;
	}

	public void setInvoiceLastUpdate(String invoiceLastUpdate) {
		this.invoiceLastUpdate = invoiceLastUpdate;
	}

	public String getInvoiceLastUpdatedBy() {
		return invoiceLastUpdatedBy;
	}

	public void setInvoiceLastUpdatedBy(String invoiceLastUpdated_by) {
		this.invoiceLastUpdatedBy = invoiceLastUpdated_by;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getInvoicePaidDate() {
		return invoicePaidDate;
	}

	public void setInvoicePaidDate(String invoicePaidDate) {
		this.invoicePaidDate = invoicePaidDate;
	}

	public String getInvoicePartnerComment() {
		return invoicePartnerComment;
	}

	public void setInvoicePartnerComment(String invoicePartnerComment) {
		this.invoicePartnerComment = invoicePartnerComment;
	}

	public String getInvoiceRecivedBy() {
		return invoiceRecivedBy;
	}

	public void setInvoiceRecivedBy(String invoiceRecivedBy) {
		this.invoiceRecivedBy = invoiceRecivedBy;
	}

	public String getInvoiceReceivedDate() {
		return invoiceReceivedDate;
	}

	public void setInvoiceReceivedDate(String invoiceRecivedDate) {
		this.invoiceReceivedDate = invoiceRecivedDate;
	}

	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public String getInvoiceTotal() {
		return invoiceTotal;
	}

	public void setInvoiceTotal(String invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public ArrayList<EInvoiceItems> getInvoiceItems() {
		return invoiceItems;
	}

	public void setInvoiceItems(ArrayList<EInvoiceItems> invoiceItems) {
		this.invoiceItems = invoiceItems;
	}

	public String getLineTaxAmount() {
		return lineTaxAmount;
	}

	public void setLineTaxAmount(String lineTaxAmount) {
		this.lineTaxAmount = lineTaxAmount;
	}

	public String getLineTotal() {
		return lineTotal;
	}

	public void setLineTotal(String lineTotal) {
		this.lineTotal = lineTotal;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getMasterPOItemDesc() {
		return masterPOItemDesc;
	}

	public void setMasterPOItemDesc(String masterPOItemDesc) {
		this.masterPOItemDesc = masterPOItemDesc;
	}

	public String getTermsDesc() {
		return termsDesc;
	}

	public void setTermsDesc(String termsDesc) {
		this.termsDesc = termsDesc;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String[] getCheckInTransit() {
		return checkInTransit;
	}

	public void setCheckInTransit(String[] checkInTransit) {
		this.checkInTransit = checkInTransit;
	}

	public String getDevDate() {
		return devDate;
	}

	public void setDevDate(String devDate) {
		this.devDate = devDate;
	}

	public String getDevStatus() {
		return devStatus;
	}

	public void setDevStatus(String devStatus) {
		this.devStatus = devStatus;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUploadDeliverable() {
		return uploadDeliverable;
	}

	public void setUploadDeliverable(String uploadDeliverable) {
		this.uploadDeliverable = uploadDeliverable;
	}

	public String getAppendix_Name() {
		return appendix_Name;
	}

	public void setAppendix_Name(String appendix_Name) {
		this.appendix_Name = appendix_Name;
	}

	public String getJob_Name() {
		return job_Name;
	}

	public void setJob_Name(String job_Name) {
		this.job_Name = job_Name;
	}

	public String getMSA_Name() {
		return mSA_Name;
	}

	public void setMSA_Name(String name) {
		mSA_Name = name;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getPartner_Name() {
		return partner_Name;
	}

	public void setPartner_Name(String partner_Name) {
		this.partner_Name = partner_Name;
	}

	public String getWO_ID() {
		return wO_ID;
	}

	public void setWO_ID(String wo_id) {
		wO_ID = wo_id;
	}

	public String getPoFooterChangeBy() {
		return poFooterChangeBy;
	}

	public void setPoFooterChangeBy(String poFooterChangeBy) {
		this.poFooterChangeBy = poFooterChangeBy;
	}

	public String getPoFooterCreateBy() {
		return poFooterCreateBy;
	}

	public void setPoFooterCreateBy(String poFooterCreateBy) {
		this.poFooterCreateBy = poFooterCreateBy;
	}

	public String getPartnerAddress() {
		return partnerAddress;
	}

	public void setPartnerAddress(String partnerAddress) {
		this.partnerAddress = partnerAddress;
	}

	public String getPartnerCity() {
		return partnerCity;
	}

	public void setPartnerCity(String partnerCity) {
		this.partnerCity = partnerCity;
	}

	public String getPartnerCountry() {
		return partnerCountry;
	}

	public void setPartnerCountry(String partnerCountry) {
		this.partnerCountry = partnerCountry;
	}

	public String getPartnerLatitude() {
		return partnerLatitude;
	}

	public void setPartnerLatitude(String partnerLatitude) {
		this.partnerLatitude = partnerLatitude;
	}

	public String getPartnerLongitude() {
		return partnerLongitude;
	}

	public void setPartnerLongitude(String partnerLongitude) {
		this.partnerLongitude = partnerLongitude;
	}

	public String getPartnerState() {
		return partnerState;
	}

	public void setPartnerState(String partnerState) {
		this.partnerState = partnerState;
	}

	public String getJobAddress() {
		return jobAddress;
	}

	public void setJobAddress(String jobAddress) {
		this.jobAddress = jobAddress;
	}

	public String getJobCity() {
		return jobCity;
	}

	public void setJobCity(String jobCity) {
		this.jobCity = jobCity;
	}

	public String getJobCountry() {
		return jobCountry;
	}

	public void setJobCountry(String jobCountry) {
		this.jobCountry = jobCountry;
	}

	public String getJobLatitude() {
		return jobLatitude;
	}

	public void setJobLatitude(String jobLatitude) {
		this.jobLatitude = jobLatitude;
	}

	public String getJobLongitude() {
		return jobLongitude;
	}

	public void setJobLongitude(String jobLongitude) {
		this.jobLongitude = jobLongitude;
	}

	public String getJobState() {
		return jobState;
	}

	public void setJobState(String jobState) {
		this.jobState = jobState;
	}

	public String getOverrideMinumtemanCostCheckedHidden() {
		return overrideMinumtemanCostCheckedHidden;
	}

	public void setOverrideMinumtemanCostCheckedHidden(
			String overrideMinumtemanCostCheckedHidden) {
		this.overrideMinumtemanCostCheckedHidden = overrideMinumtemanCostCheckedHidden;
	}

	public int getPvsJustification() {
		return pvsJustification;
	}

	public void setPvsJustification(int pvsJustification) {
		this.pvsJustification = pvsJustification;
	}

	public ArrayList getGeographicConstraintList() {
		return geographicConstraintList;
	}

	public void setGeographicConstraintList(ArrayList geographicConstraintList) {
		this.geographicConstraintList = geographicConstraintList;
	}

	public int getGeographicConstraint() {
		return geographicConstraint;
	}

	public void setGeographicConstraint(int geographicConstraint) {
		this.geographicConstraint = geographicConstraint;
	}

	public String[] getCmboxCoreCompetency() {
		return cmboxCoreCompetency;
	}

	public void setCmboxCoreCompetency(String[] cmboxCoreCompetency) {
		this.cmboxCoreCompetency = cmboxCoreCompetency;
	}
	public boolean isDevMobile() {
		return devMobile;
	}

	public void setDevMobile(boolean devMobile) {
		this.devMobile = devMobile;
	}
	public String getDevStatusName() {
		return devStatusName;
	}

	public void setDevStatusName(String devStatusName) {
		this.devStatusName = devStatusName;
	}

}
