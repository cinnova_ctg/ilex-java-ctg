package com.mind.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.mind.common.LabelValue;

public class Codelist implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection msatypecode = null;
	private Collection appendixtypecode = null;
	private Collection activitytypecode = null;

	private Collection customerdivision = null;

	private Collection customername = null;

	private Collection salestypecode = null;
	private Collection salesPOCcode = null;

	private Collection paytermscode = null;
	private Collection payqualifiercode = null;

	private Collection draftcode = null;
	private Collection businesscode = null;

	private Collection custPOCbusinesscode = null;
	private Collection custPOCcontractcode = null;
	private Collection custPOCbillingcode = null;

	private Collection statuscode = null;

	private ArrayList msalist = null;
	private ArrayList appendixlist = null;
	private ArrayList joblist = null;
	private ArrayList pocList = null;

	private ArrayList managerList = null;

	private Collection sortparameter = null; // would contain sort paramters

	private List<LabelValue> appendixTypeBreakoutList;

	public java.util.Collection getAppendixtypecode() {
		/*
		 * appendixtypecode = new java.util.ArrayList(); appendixtypecode.add(
		 * new LabelValue( "-Select-" , "0" ) ); appendixtypecode.add( new
		 * LabelValue( "I Multi-Site" , "I Multi-Site" ) );
		 * appendixtypecode.add( new LabelValue( "II Single Site" ,
		 * "II Single Site" ) ); appendixtypecode.add( new LabelValue(
		 * "III NetMedX" , "III NetMedX" ) ); appendixtypecode.add( new
		 * LabelValue( "IV Hardware" , "IV Hardware" ) );
		 */

		return appendixtypecode;
	}

	public void setAppendixtypecode(java.util.Collection list) {
		appendixtypecode = list;
	}

	public Collection getCustomerdivision() {
		return customerdivision;
	}

	public void setCustomerdivision(Collection customerdivision) {
		this.customerdivision = customerdivision;
	}

	public java.util.Collection getMsatypecode() {
		return msatypecode;
	}

	public void setMsatypecode(java.util.Collection list) {
		msatypecode = list;
	}

	public java.util.Collection getCustomername() {
		return customername;
	}

	public void setCustomername(java.util.Collection list) {
		customername = list;
	}

	public java.util.Collection getSalestypecode() {
		salestypecode = new java.util.ArrayList();
		salestypecode.add(new LabelValue("Select", "0"));
		salestypecode.add(new LabelValue("Direct", "D"));
		salestypecode.add(new LabelValue("Channel", "C"));

		return salestypecode;
	}

	public void setSalestypecode(java.util.Collection list) {
		salestypecode = list;
	}

	public java.util.Collection getSalesPOCcode() {
		return salesPOCcode;
	}

	public void setSalesPOCcode(java.util.Collection list) {
		salesPOCcode = list;
	}

	public java.util.Collection getPaytermscode() {
		paytermscode = new java.util.ArrayList();
		paytermscode.add(new LabelValue("---Select---", "0"));
		paytermscode.add(new LabelValue("Prepaid", "Prepaid"));
		paytermscode.add(new LabelValue("Net 10", "Net 10"));
		paytermscode.add(new LabelValue("Net 15", "Net 15"));
		paytermscode.add(new LabelValue("Net 20", "Net 20"));
		paytermscode.add(new LabelValue("Net 30", "Net 30"));
		paytermscode.add(new LabelValue("Net 60", "Net 60"));
		paytermscode.add(new LabelValue("Net 75", "Net 75"));

		return paytermscode;
	}

	public void setPaytermscode(java.util.Collection list) {
		paytermscode = list;
	}

	public java.util.Collection getPayqualifiercode() {
		payqualifiercode = new java.util.ArrayList();
		payqualifiercode.add(new LabelValue("---Select---", "0"));
		payqualifiercode.add(new LabelValue("Discount for Early Payment",
				"Discount for Early Payment"));
		payqualifiercode.add(new LabelValue("2% Net 10", "2% Net 10"));
		payqualifiercode.add(new LabelValue("2% Net 15", "2% Net 15"));
		payqualifiercode.add(new LabelValue("None", "None"));

		return payqualifiercode;
	}

	public void setPayqualifiercode(java.util.Collection list) {
		payqualifiercode = list;
	}

	public java.util.Collection getDraftcode() {
		draftcode = new java.util.ArrayList();
		draftcode.add(new LabelValue("---Select---", "0"));
		draftcode.add(new LabelValue("Standard", "S"));
		draftcode.add(new LabelValue("Expedite", "E"));

		return draftcode;
	}

	public void setDraftcode(java.util.Collection list) {
		draftcode = list;
	}

	public java.util.Collection getBusinesscode() {
		businesscode = new java.util.ArrayList();
		businesscode.add(new LabelValue("---Select---", "0"));
		businesscode.add(new LabelValue("Standard", "S"));
		businesscode.add(new LabelValue("Expedite", "E"));

		return businesscode;
	}

	public void setBusinesscode(java.util.Collection list) {
		businesscode = list;
	}

	public java.util.Collection getCustPOCbusinesscode() {
		return custPOCbusinesscode;
	}

	public void setCustPOCbusinesscode(java.util.Collection list) {
		custPOCbusinesscode = list;
	}

	public java.util.Collection getCustPOCcontractcode() {
		return custPOCcontractcode;
	}

	public void setCustPOCcontractcode(java.util.Collection list) {
		custPOCcontractcode = list;
	}

	public Collection getCustPOCbillingcode() {
		return custPOCbillingcode;
	}

	public void setCustPOCbillingcode(Collection custPOCbillingcode) {
		this.custPOCbillingcode = custPOCbillingcode;
	}

	public java.util.Collection getStatuscode() {
		statuscode = new java.util.ArrayList();
		statuscode.add(new LabelValue("---Select---", "Draft"));
		statuscode.add(new LabelValue("Draft", "Draft"));

		return statuscode;
	}

	public void setStatuscode(java.util.Collection list) {
		statuscode = list;
	}

	public ArrayList getMsalist() {
		return msalist;
	}

	public void setMsalist(ArrayList as) {
		msalist = as;
	}

	public ArrayList getAppendixlist() {
		return appendixlist;
	}

	public void setAppendixlist(ArrayList appendixlist) {
		this.appendixlist = appendixlist;
	}

	public ArrayList getJoblist() {
		return joblist;
	}

	public void setJoblist(ArrayList joblist) {
		this.joblist = joblist;
	}

	public java.util.Collection getSortparameter() {
		return sortparameter;
	}

	public void setSortparameter(java.util.Collection list) {
		sortparameter = list;
	}

	public ArrayList getPocList() {
		return pocList;
	}

	public void setPocList(ArrayList pocList) {
		this.pocList = pocList;
	}

	public Collection getActivitytypecode() {
		return activitytypecode;
	}

	public void setActivitytypecode(Collection activitytypecode) {
		this.activitytypecode = activitytypecode;
	}

	public ArrayList getManagerList() {
		return managerList;
	}

	public void setManagerList(ArrayList managerList) {
		this.managerList = managerList;
	}

	/**
	 * @return the appendixTypeBreakoutList
	 */
	public List<LabelValue> getAppendixTypeBreakoutList() {
		return appendixTypeBreakoutList;
	}

	/**
	 * @param appendixTypeBreakoutList
	 *            the appendixTypeBreakoutList to set
	 */
	public void setAppendixTypeBreakoutList(
			List<LabelValue> appendixTypeBreakoutList) {
		this.appendixTypeBreakoutList = appendixTypeBreakoutList;
	}

}
