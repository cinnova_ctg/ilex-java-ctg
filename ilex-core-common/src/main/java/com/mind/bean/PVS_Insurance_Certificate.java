package com.mind.bean;

import java.io.Serializable;

public class PVS_Insurance_Certificate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String carrier = null;
	private String policyNumber = null;
	private String amountGeneral = null;
	private String amountAutomobile = null;
	private String amountUmbrella = null;

	public PVS_Insurance_Certificate() {
		super();
	}

	public PVS_Insurance_Certificate(String carrier, String number,
			String general, String automobile, String umbrella) {

		this.carrier = carrier;
		policyNumber = number;
		amountGeneral = general;
		amountAutomobile = automobile;
		amountUmbrella = umbrella;
	}

	public String getAmountAutomobile() {
		return amountAutomobile;
	}

	public void setAmountAutomobile(String amountAutomobile) {
		this.amountAutomobile = amountAutomobile;
	}

	public String getAmountGeneral() {
		return amountGeneral;
	}

	public void setAmountGeneral(String amountGeneral) {
		this.amountGeneral = amountGeneral;
	}

	public String getAmountUmbrella() {
		return amountUmbrella;
	}

	public void setAmountUmbrella(String amountUmbrella) {
		this.amountUmbrella = amountUmbrella;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

}
