package com.mind.bean;

import java.io.Serializable;

public class PVSPocSearchList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cp_partner_id = null;
	private String lo_ot_id = null;
	private String lo_ot_type = null;
	private String lo_ot_name = null;
	private String lo_om_id = null;
	private String lo_om_division = null;
	private String lo_ad_address1 = null;
	private String lo_ad_address2 = null;
	private String lo_ad_city = null;
	private String lo_ad_state = null;
	private String lo_ad_country = null;
	private String lo_ad_zip_code = null;
	private String contact = null;
	private String cp_pd_incident_type = null;
	private String cp_pd_status = null;
	private String cp_pt_date = null;
	private String addressValidator = null;
	private String regRenewalDate = null;
	private String receivedstatus = null;

	public String getReceivedstatus() {
		return receivedstatus;
	}

	public void setReceivedstatus(String receivedstatus) {
		this.receivedstatus = receivedstatus;
	}

	public String getRegRenewalDate() {
		return regRenewalDate;
	}

	public void setRegRenewalDate(String regRenewalDate) {
		this.regRenewalDate = regRenewalDate;
	}

	public String getCp_partner_id() {
		return cp_partner_id;
	}

	public void setCp_partner_id(String cp_partner_id) {
		this.cp_partner_id = cp_partner_id;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getCp_pd_incident_type() {
		return cp_pd_incident_type;
	}

	public void setCp_pd_incident_type(String cp_pd_incident_type) {
		this.cp_pd_incident_type = cp_pd_incident_type;
	}

	public String getCp_pd_status() {
		return cp_pd_status;
	}

	public void setCp_pd_status(String cp_pd_status) {
		this.cp_pd_status = cp_pd_status;
	}

	public String getLo_ad_address1() {
		return lo_ad_address1;
	}

	public void setLo_ad_address1(String lo_ad_address1) {
		this.lo_ad_address1 = lo_ad_address1;
	}

	public String getLo_ad_address2() {
		return lo_ad_address2;
	}

	public void setLo_ad_address2(String lo_ad_address2) {
		this.lo_ad_address2 = lo_ad_address2;
	}

	public String getLo_ad_city() {
		return lo_ad_city;
	}

	public void setLo_ad_city(String lo_ad_city) {
		this.lo_ad_city = lo_ad_city;
	}

	public String getLo_ad_country() {
		return lo_ad_country;
	}

	public void setLo_ad_country(String lo_ad_country) {
		this.lo_ad_country = lo_ad_country;
	}

	public String getLo_ad_state() {
		return lo_ad_state;
	}

	public void setLo_ad_state(String lo_ad_state) {
		this.lo_ad_state = lo_ad_state;
	}

	public String getLo_ad_zip_code() {
		return lo_ad_zip_code;
	}

	public void setLo_ad_zip_code(String lo_ad_zip_code) {
		this.lo_ad_zip_code = lo_ad_zip_code;
	}

	public String getLo_om_division() {
		return lo_om_division;
	}

	public void setLo_om_division(String lo_om_division) {
		this.lo_om_division = lo_om_division;
	}

	public String getLo_om_id() {
		return lo_om_id;
	}

	public void setLo_om_id(String lo_om_id) {
		this.lo_om_id = lo_om_id;
	}

	public String getLo_ot_id() {
		return lo_ot_id;
	}

	public void setLo_ot_id(String lo_ot_id) {
		this.lo_ot_id = lo_ot_id;
	}

	public String getLo_ot_name() {
		return lo_ot_name;
	}

	public void setLo_ot_name(String lo_ot_name) {
		this.lo_ot_name = lo_ot_name;
	}

	public String getLo_ot_type() {
		return lo_ot_type;
	}

	public void setLo_ot_type(String lo_ot_type) {
		this.lo_ot_type = lo_ot_type;
	}

	public String getCp_pt_date() {
		return cp_pt_date;
	}

	public void setCp_pt_date(String cp_pt_date) {
		this.cp_pt_date = cp_pt_date;
	}

	public String getAddressValidator() {
		return addressValidator;
	}

	public void setAddressValidator(String addressValidator) {
		this.addressValidator = addressValidator;
	}

}
