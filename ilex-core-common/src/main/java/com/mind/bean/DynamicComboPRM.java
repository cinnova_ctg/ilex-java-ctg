/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This is a bean class used for getting and setting the values of collections used in Add Addendum Form.      
 *
 */

package com.mind.bean;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.mind.common.LabelValue;

public class DynamicComboPRM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Collection custpoc = null;
	private Collection addendum = null;
	private Collection cnspoclist = null;
	private Collection custpoclist = null;
	private Collection salespoclist = null;
	private Collection businesspoclist = null;
	private Collection contractpoclist = null;
	private Collection billingpoclist = null;
	private Collection allOwnerName = null;
	private Collection schedulerList = null;

	// by hamid for criticality in report
	private Collection criticalitylist = null;

	// by hamid for Job/ticket status in report
	private Collection jobstatuslist = null;

	// by hamid for requestor in report
	private Collection requestorlist = null;

	// by hamid for criticality description in report
	private Collection critdesclist = null;

	private Collection yesnolist = null;

	private Collection msalist = null;
	private Collection appendixlist = null;
	private Collection notificationMethodList = null;

	private List<LabelValue> cnsPrjManagerList;
	private List<LabelValue> teamLeadList;

	public Collection getCustpoc() {
		return custpoc;
	}

	public void setCustpoc(Collection custpoc) {
		this.custpoc = custpoc;
	}

	public Collection getAddendum() {
		return addendum;
	}

	public void setAddendum(Collection addendum) {
		this.addendum = addendum;
	}

	public Collection getCnspoclist() {
		return cnspoclist;
	}

	public void setCnspoclist(Collection cnspoclist) {
		this.cnspoclist = cnspoclist;
	}

	public Collection getCustpoclist() {
		return custpoclist;
	}

	public void setCustpoclist(Collection custpoclist) {
		this.custpoclist = custpoclist;
	}

	public Collection getBillingpoclist() {
		return billingpoclist;
	}

	public void setBillingpoclist(Collection billingpoclist) {
		this.billingpoclist = billingpoclist;
	}

	public Collection getBusinesspoclist() {
		return businesspoclist;
	}

	public void setBusinesspoclist(Collection businesspoclist) {
		this.businesspoclist = businesspoclist;
	}

	public Collection getContractpoclist() {
		return contractpoclist;
	}

	public void setContractpoclist(Collection contractpoclist) {
		this.contractpoclist = contractpoclist;
	}

	public Collection getSalespoclist() {
		return salespoclist;
	}

	public void setSalespoclist(Collection salespoclist) {
		this.salespoclist = salespoclist;
	}

	public Collection getAllOwnerName() {
		return allOwnerName;
	}

	public void setAllOwnerName(Collection allOwnerName) {
		this.allOwnerName = allOwnerName;
	}

	public Collection getCriticalitylist() {
		return criticalitylist;
	}

	public void setCriticalitylist(Collection criticalitylist) {
		this.criticalitylist = criticalitylist;
	}

	public Collection getJobstatuslist() {
		return jobstatuslist;
	}

	public void setJobstatuslist(Collection jobstatuslist) {
		this.jobstatuslist = jobstatuslist;
	}

	public Collection getCritdesclist() {
		return critdesclist;
	}

	public void setCritdesclist(Collection critdesclist) {
		this.critdesclist = critdesclist;
	}

	public Collection getRequestorlist() {
		return requestorlist;
	}

	public void setRequestorlist(Collection requestorlist) {
		this.requestorlist = requestorlist;
	}

	public Collection getYesnolist() {
		return yesnolist;
	}

	public void setYesnolist(Collection yesnolist) {
		this.yesnolist = yesnolist;
	}

	public Collection getAppendixlist() {
		return appendixlist;
	}

	public void setAppendixlist(Collection appendixlist) {
		this.appendixlist = appendixlist;
	}

	public Collection getMsalist() {
		return msalist;
	}

	public void setMsalist(Collection msalist) {
		this.msalist = msalist;
	}

	public Collection getNotificationMethodList() {
		return notificationMethodList;
	}

	public void setNotificationMethodList(Collection notificationMethodList) {
		this.notificationMethodList = notificationMethodList;
	}

	public Collection getSchedulerList() {
		return schedulerList;
	}

	public void setSchedulerList(Collection schedulerList) {
		this.schedulerList = schedulerList;
	}

	/**
	 * @return the cnsPrjManagerList
	 */
	public List<LabelValue> getCnsPrjManagerList() {
		return cnsPrjManagerList;
	}

	/**
	 * @param cnsPrjManagerList
	 *            the cnsPrjManagerList to set
	 */
	public void setCnsPrjManagerList(List<LabelValue> cnsPrjManagerList) {
		this.cnsPrjManagerList = cnsPrjManagerList;
	}

	/**
	 * @return the teamLeadList
	 */
	public List<LabelValue> getTeamLeadList() {
		return teamLeadList;
	}

	/**
	 * @param teamLeadList
	 *            the teamLeadList to set
	 */
	public void setTeamLeadList(List<LabelValue> teamLeadList) {
		this.teamLeadList = teamLeadList;
	}

}
