//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : ReportFilterCriteria.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.bean.reports;

import java.io.Serializable;

/**
 * Class ReportFilterCriteria - This is base class which helps in getting and
 * setting report criteria. methods - getter, setter.
 **/
public class ReportFilterCriteria implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String reportName = "";
	private String reportType = "";
	private String startDate = "";
	private String endDate = "";
	private String criticality = "";
	private String status = "";
	private String requestor = "";
	private String arrivalOptions = "";
	private String msp = "";
	private String helpDesk = "";

	public String getArrivalOptions() {
		return arrivalOptions;
	}

	public void setArrivalOptions(String arrivalOptions) {
		this.arrivalOptions = arrivalOptions;
	}

	public String getCriticality() {
		return criticality;
	}

	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}

	public String getHelpDesk() {
		return helpDesk;
	}

	public void setHelpDesk(String helpDesk) {
		this.helpDesk = helpDesk;
	}

	public String getMsp() {
		return msp;
	}

	public void setMsp(String msp) {
		this.msp = msp;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	// public void setEndDate(Date endDate) {
	// this.endDate = endDate;
	// }
	// public void setStartDate(Date startDate) {
	// this.startDate = startDate;
	// }
	// public Date getEndDate() {
	// return endDate;
	// }
	// public Date getStartDate() {
	// return startDate;
	// }
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public String getStartDate() {
		return startDate;
	}
}
