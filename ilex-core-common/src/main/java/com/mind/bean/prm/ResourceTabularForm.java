package com.mind.bean.prm;

import java.io.Serializable;

public class ResourceTabularForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String resource_id = null;
	private String resource_name = null;
	private String resource_qty = null;
	private String resource_cnspartnumber = null;
	private String resource_type = null;
	private String resource_sourcename = null;
	private String resource_sourcepartno = null;
	private String resource_estimatedunitcost = null;
	private String resource_estimatedtotalcost = null;
	private String resource_extendedunitprice = null;
	private String resource_extendedtotalprice = null;
	private String resource_margin = null;
	private String resource_lx_ce_type = null;
	private String resource_partner_name = null;
	private String resource_drop_shipped = null;
	
	public String getResource_cnspartnumber() {
		return resource_cnspartnumber;
	}
	public void setResource_cnspartnumber(String resource_cnspartnumber) {
		this.resource_cnspartnumber = resource_cnspartnumber;
	}
	public String getResource_drop_shipped() {
		return resource_drop_shipped;
	}
	public void setResource_drop_shipped(String resource_drop_shipped) {
		this.resource_drop_shipped = resource_drop_shipped;
	}
	public String getResource_estimatedtotalcost() {
		return resource_estimatedtotalcost;
	}
	public void setResource_estimatedtotalcost(String resource_estimatedtotalcost) {
		this.resource_estimatedtotalcost = resource_estimatedtotalcost;
	}
	public String getResource_estimatedunitcost() {
		return resource_estimatedunitcost;
	}
	public void setResource_estimatedunitcost(String resource_estimatedunitcost) {
		this.resource_estimatedunitcost = resource_estimatedunitcost;
	}
	public String getResource_extendedtotalprice() {
		return resource_extendedtotalprice;
	}
	public void setResource_extendedtotalprice(String resource_extendedtotalprice) {
		this.resource_extendedtotalprice = resource_extendedtotalprice;
	}
	public String getResource_extendedunitprice() {
		return resource_extendedunitprice;
	}
	public void setResource_extendedunitprice(String resource_extendedunitprice) {
		this.resource_extendedunitprice = resource_extendedunitprice;
	}
	public String getResource_id() {
		return resource_id;
	}
	public void setResource_id(String resource_id) {
		this.resource_id = resource_id;
	}
	public String getResource_lx_ce_type() {
		return resource_lx_ce_type;
	}
	public void setResource_lx_ce_type(String resource_lx_ce_type) {
		this.resource_lx_ce_type = resource_lx_ce_type;
	}
	public String getResource_margin() {
		return resource_margin;
	}
	public void setResource_margin(String resource_margin) {
		this.resource_margin = resource_margin;
	}
	public String getResource_name() {
		return resource_name;
	}
	public void setResource_name(String resource_name) {
		this.resource_name = resource_name;
	}
	public String getResource_partner_name() {
		return resource_partner_name;
	}
	public void setResource_partner_name(String resource_partner_name) {
		this.resource_partner_name = resource_partner_name;
	}
	public String getResource_qty() {
		return resource_qty;
	}
	public void setResource_qty(String resource_qty) {
		this.resource_qty = resource_qty;
	}
	public String getResource_sourcename() {
		return resource_sourcename;
	}
	public void setResource_sourcename(String resource_sourcename) {
		this.resource_sourcename = resource_sourcename;
	}
	public String getResource_sourcepartno() {
		return resource_sourcepartno;
	}
	public void setResource_sourcepartno(String resource_sourcepartno) {
		this.resource_sourcepartno = resource_sourcepartno;
	}
	public String getResource_type() {
		return resource_type;
	}
	public void setResource_type(String resource_type) {
		this.resource_type = resource_type;
	}
	
	
	
	
}
