package com.mind.bean.prm;

import java.io.Serializable;

public class UpliftValue implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String upliftFactorName = null;
	String upliftFactorValue = null;
	String upliftFactorCost = null;
	
	public String getUpliftFactorCost() {
		return upliftFactorCost;
	}
	public void setUpliftFactorCost(String upliftFactorCost) {
		this.upliftFactorCost = upliftFactorCost;
	}
	public String getUpliftFactorName() {
		return upliftFactorName;
	}
	public void setUpliftFactorName(String upliftFactorName) {
		this.upliftFactorName = upliftFactorName;
	}
	public String getUpliftFactorValue() {
		return upliftFactorValue;
	}
	public void setUpliftFactorValue(String upliftFactorValue) {
		this.upliftFactorValue = upliftFactorValue;
	}
	
}
