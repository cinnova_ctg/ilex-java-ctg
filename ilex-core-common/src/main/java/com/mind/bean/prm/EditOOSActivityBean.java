package com.mind.bean.prm;

import java.io.Serializable;

public class EditOOSActivityBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String aoId = null;
	private String activityId = null;
	private String activityName = null;
	private String dateNotified = null;
	private String notificationMethod = null;
	private String custName = null;
	private String dateApproved = null;
	private String jobId = null;
	private String appendixId = null;
	private String save = null;
	private String back = null;
	private String description = null;
	private String flagOOA = null;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getAoId() {
		return aoId;
	}
	public void setAoId(String aoId) {
		this.aoId = aoId;
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getDateApproved() {
		return dateApproved;
	}
	public void setDateApproved(String dateApproved) {
		this.dateApproved = dateApproved;
	}
	public String getDateNotified() {
		return dateNotified;
	}
	public void setDateNotified(String dateNotified) {
		this.dateNotified = dateNotified;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getNotificationMethod() {
		return notificationMethod;
	}
	public void setNotificationMethod(String notificationMethod) {
		this.notificationMethod = notificationMethod;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getBack() {
		return back;
	}
	public void setBack(String back) {
		this.back = back;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getFlagOOA() {
		return flagOOA;
	}
	public void setFlagOOA(String flagOOA) {
		this.flagOOA = flagOOA;
	}

}

