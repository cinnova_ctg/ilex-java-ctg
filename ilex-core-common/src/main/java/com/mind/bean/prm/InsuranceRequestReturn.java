package com.mind.bean.prm;

public class InsuranceRequestReturn {
	
	private String retName = null;
	private String retEmail = null;
	private String retDirectNumber = null;
	private String retFax = null;
	
	public String getRetDirectNumber() {
		return retDirectNumber;
	}
	public void setRetDirectNumber(String retDirectNumber) {
		this.retDirectNumber = retDirectNumber;
	}
	public String getRetEmail() {
		return retEmail;
	}
	public void setRetEmail(String retEmail) {
		this.retEmail = retEmail;
	}
	public String getRetFax() {
		return retFax;
	}
	public void setRetFax(String retFax) {
		this.retFax = retFax;
	}
	public String getRetName() {
		return retName;
	}
	public void setRetName(String retName) {
		this.retName = retName;
	}

}
