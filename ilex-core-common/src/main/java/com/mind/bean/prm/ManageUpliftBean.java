package com.mind.bean.prm;

import java.io.Serializable;

public class ManageUpliftBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appendixId = null;
	private String appendixName = null;
	private String jobId = null;
	private String jobName = null;
	private String unionUplift = null;
	private String expedite24Uplift = null;
	private String expedite48Uplift = null;
	private String afterHoursUplift = null;
	private String whUplift = null;
	private String save = null;
	private String ref = null;
	private String viewjobtype = null;
	private String ownerId = null;
	
	private String[] activity_id = null;
	private String[] activity_tempid = null;
	private String[] atUnionUplift = null;
	private String[] atExpedite24Uplift = null;
	private String[] atExpedite48Uplift = null;
	private String[] atAfterHoursUplift = null;
	private String[] atWhUplift = null;
	
	public String getAfterHoursUplift() {
		return afterHoursUplift;
	}
	public void setAfterHoursUplift(String afterHoursUplift) {
		this.afterHoursUplift = afterHoursUplift;
	}
	public String getExpedite24Uplift() {
		return expedite24Uplift;
	}
	public void setExpedite24Uplift(String expedite24Uplift) {
		this.expedite24Uplift = expedite24Uplift;
	}
	public String getExpedite48Uplift() {
		return expedite48Uplift;
	}
	public void setExpedite48Uplift(String expedite48Uplift) {
		this.expedite48Uplift = expedite48Uplift;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getUnionUplift() {
		return unionUplift;
	}
	public void setUnionUplift(String unionUplift) {
		this.unionUplift = unionUplift;
	}
	public String getWhUplift() {
		return whUplift;
	}
	public void setWhUplift(String whUplift) {
		this.whUplift = whUplift;
	}
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String[] getAtAfterHoursUplift() {
		return atAfterHoursUplift;
	}
	public String getAtAfterHoursUplift( int i ) {
		return atAfterHoursUplift[i];
	}
	public void setAtAfterHoursUplift(String[] atAfterHoursUplift) {
		this.atAfterHoursUplift = atAfterHoursUplift;
	}
	public String[] getAtExpedite24Uplift() {
		return atExpedite24Uplift;
	}
	public String getAtExpedite24Uplift( int i ) {
		return atExpedite24Uplift[i];
	}
	public void setAtExpedite24Uplift(String[] atExpedite24Uplift) {
		this.atExpedite24Uplift = atExpedite24Uplift;
	}
	public String[] getAtExpedite48Uplift() {
		return atExpedite48Uplift;
	}
	public String getAtExpedite48Uplift( int i ) {
		return atExpedite48Uplift[i];
	}
	public void setAtExpedite48Uplift(String[] atExpedite48Uplift) {
		this.atExpedite48Uplift = atExpedite48Uplift;
	}
	public String[] getAtUnionUplift() {
		return atUnionUplift;
	}
	public String getAtUnionUplift( int i ) {
		return atUnionUplift[i];
	}
	public void setAtUnionUplift(String[] atUnionUplift) {
		this.atUnionUplift = atUnionUplift;
	}
	public String[] getAtWhUplift() {
		return atWhUplift;
	}
	public String getAtWhUplift( int i ) {
		return atWhUplift[i];
	}
	public void setAtWhUplift(String[] atWhUplift) {
		this.atWhUplift = atWhUplift;
	}
	public String[] getActivity_id() {
		return activity_id;
	}
	public String getActivity_id( int i ) {
		return activity_id[i];
	}
	public void setActivity_id(String[] activity_id) {
		this.activity_id = activity_id;
	}
	public String[] getActivity_tempid() {
		return activity_tempid;
	}
	public String getActivity_tempid( int i ) {
		return activity_tempid[i];
	}
	public void setActivity_tempid(String[] activity_tempid) {
		this.activity_tempid = activity_tempid;
	}
}



