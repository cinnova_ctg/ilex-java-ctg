package com.mind.bean.prm;

import java.io.Serializable;
import java.util.Collection;

public class JobDashboardBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String appendix_id = null;
	private String appendix_name = null;
	private String job_name = null;
	private String jobtype = null;
	private String msa_id = null;
	private String custref = null;

	private String request_received_date = null;
	private String request_received_time = null;

	// Start :Added By Amit
	private String stringChangeStatus = null;
	// end : Added By Amit

	private String ownerId = null;
	private String viewjobtype = null;
	private String nettype = null;

	private String job_sitename = "";
	private String job_sitenumber = "";
	private String job_siteaddress = "";
	private String job_sitestate = "";
	private String job_sitezipcode = "";
	private String job_sitepoc = "";
	private String job_sitecity = "";

	private String job_sitephoneno = null;
	private String job_priemailid = null;

	private String job_sitesecpoc = null;
	private String job_sitesecphone = null;
	private String job_sitesecemailid = null;

	private String job_scope = null;
	private String job_status = null;
	private String job_siteno = null;
	private String site_id = null;
	private String jobid = null;
	private String sitescheduleplanned_startdate = null;
	private String sitescheduleplanned_startdatetime = null;

	private String sitescheduleplanned_enddate = null;
	private String sitescheduleplanned_enddatetime = null;

	private String sitescheduleactual_startdate = null;
	private String sitescheduleactual_startdatetime = null;

	private String sitescheduleactual_enddate = null;
	private String sitescheduleactual_enddatetime = null;

	private String job_extendedprice = null;
	private String job_estimatedcost = null;
	private String job_cnslaborcost = null;
	private String job_fieldlaborcost = null;
	private String job_materialcost = null;
	private String job_freightcost = null;
	private String job_travelcost = null;

	private String job_proformamarginvgp = null;
	private String job_actualcost = null;
	private String job_actualvgp_new = null;
	private String job_actualvgp = null;

	private String job_netticketid = null;
	private String job_netrequestor = null;
	private String job_netspecialcondition = null;
	private String job_netproblemdesc = null;
	private String job_netspecialinstruct = null;
	private String job_netcategory = null;

	private String job_netcust_reference = null;

	private String job_netpreferred_arrival_start_date = null;
	private String job_netpreferred_arrival_start_time = null;

	private String job_netpreferred_arrival_window_start_date = null;
	private String job_vpreferred_arrival_window_end_date = null;

	private String job_netpreferred_arrival_window_start_time = null;
	private String job_netpreferred_arrival_window_end_time = null;

	private String job_netauthorizedtotalcost = null;
	private String job_netactualprice = null;
	private String job_netactualvgp = null;
	private String job_invoiceno = null;

	private String job_netinstallation_date = "";
	private String job_netinstallation_time = "";
	private String job_netinstallation_comment = "";
	private Collection installationlist = null;
	private String time_onsite = null;
	private String time_toarrive = null;

	private String custinfodata1 = null;
	private String custinfovalue1 = null;
	private String custinfodata2 = null;
	private String custinfovalue2 = null;
	private String custinfodata3 = null;
	private String custinfovalue3 = null;
	// start

	private String custinfodata4 = null;
	private String custinfovalue4 = null;

	private String custinfodata5 = null;
	private String custinfovalue5 = null;
	private String custinfodata6 = null;
	private String custinfovalue6 = null;
	private String custinfodata7 = null;
	private String custinfovalue7 = null;
	private String custinfodata8 = null;
	private String custinfovalue8 = null;
	private String custinfodata9 = null;
	private String custinfovalue9 = null;
	private String custinfodata10 = null;
	private String custinfovalue10 = null;

	// over

	private String updatedby = null;
	private String updatedtime = null;
	private String updateddate = null;

	private String partnername = null;
	private String partnerid = null;
	private String partnerpocname = null;
	private String partnerpocphone = null;
	private String ponumber = null;
	private String lo_ot_name = null;

	// for back to invoice manager
	private String inv_type = null;
	private String inv_id = null;
	private String inv_rdofilter = null;
	private String invoice_Flag = null;
	private String invoiceno = null;
	private String from_date = null;
	private String to_date = null;
	private String powo_number = null;
	private String partner_name = null;

	private String textboxdummy = null;

	private String jobcreatedby = null;
	private String createtime = null;
	private String createdate = null;

	private String schedule_position = null;
	private int schedule_total = 0;
	private String totaltime_onsite = null;

	private String optactivity = null;

	// private String check_out_scope_flag = null;
	private String viewType = null;

	/* Uplift Factors */
	private String atExpedite24Uplift = null;
	private String atExpedite48Uplift = null;
	private String atAfterHoursUplift = null;
	private String atWhUplift = null;

	private String atUpliftCost = null;
	private String jobScheduler = null;

	private String jobRseCount;

	/* job Test Indicator field. */
	private String jobTestIndicator = null;

	public String getJobTestIndicator() {
		return jobTestIndicator;
	}

	public void setJobTestIndicator(String jobTestIndicator) {
		this.jobTestIndicator = jobTestIndicator;
	}

	public String getPartnerid() {
		return partnerid;
	}

	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}

	public String getPartnername() {
		return partnername;
	}

	public void setPartnername(String partnername) {
		this.partnername = partnername;
	}

	public String getPartnerpocname() {
		return partnerpocname;
	}

	public void setPartnerpocname(String partnerpocname) {
		this.partnerpocname = partnerpocname;
	}

	public String getPartnerpocphone() {
		return partnerpocphone;
	}

	public void setPartnerpocphone(String partnerpocphone) {
		this.partnerpocphone = partnerpocphone;
	}

	public String getPonumber() {
		return ponumber;
	}

	public void setPonumber(String ponumber) {
		this.ponumber = ponumber;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public String getUpdatedtime() {
		return updatedtime;
	}

	public void setUpdatedtime(String updatedtime) {
		this.updatedtime = updatedtime;
	}

	public String getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(String updateddate) {
		this.updateddate = updateddate;
	}

	public String getCustinfodata1() {
		return custinfodata1;
	}

	public void setCustinfodata1(String custinfodata1) {
		this.custinfodata1 = custinfodata1;
	}

	public String getCustinfodata2() {
		return custinfodata2;
	}

	public void setCustinfodata2(String custinfodata2) {
		this.custinfodata2 = custinfodata2;
	}

	public String getCustinfodata3() {
		return custinfodata3;
	}

	public void setCustinfodata3(String custinfodata3) {
		this.custinfodata3 = custinfodata3;
	}

	public String getCustinfovalue1() {
		return custinfovalue1;
	}

	public void setCustinfovalue1(String custinfovalue1) {
		this.custinfovalue1 = custinfovalue1;
	}

	public String getCustinfovalue2() {
		return custinfovalue2;
	}

	public void setCustinfovalue2(String custinfovalue2) {
		this.custinfovalue2 = custinfovalue2;
	}

	public String getCustinfovalue3() {
		return custinfovalue3;
	}

	public void setCustinfovalue3(String custinfovalue3) {
		this.custinfovalue3 = custinfovalue3;
	}

	public String getJob_actualcost() {
		return job_actualcost;
	}

	public void setJob_actualcost(String job_actualcost) {
		this.job_actualcost = job_actualcost;
	}

	public String getJob_actualvgp() {
		return job_actualvgp;
	}

	public void setJob_actualvgp(String job_actualvgp) {
		this.job_actualvgp = job_actualvgp;
	}

	public String getJob_cnslaborcost() {
		return job_cnslaborcost;
	}

	public void setJob_cnslaborcost(String job_cnslaborcost) {
		this.job_cnslaborcost = job_cnslaborcost;
	}

	public String getJob_estimatedcost() {
		return job_estimatedcost;
	}

	public void setJob_estimatedcost(String job_estimatedcost) {
		this.job_estimatedcost = job_estimatedcost;
	}

	public String getJob_extendedprice() {
		return job_extendedprice;
	}

	public void setJob_extendedprice(String job_extendedprice) {
		this.job_extendedprice = job_extendedprice;
	}

	public String getJob_fieldlaborcost() {
		return job_fieldlaborcost;
	}

	public void setJob_fieldlaborcost(String job_fieldlaborcost) {
		this.job_fieldlaborcost = job_fieldlaborcost;
	}

	public String getJob_freightcost() {
		return job_freightcost;
	}

	public void setJob_freightcost(String job_freightcost) {
		this.job_freightcost = job_freightcost;
	}

	public String getJob_materialcost() {
		return job_materialcost;
	}

	public void setJob_materialcost(String job_materialcost) {
		this.job_materialcost = job_materialcost;
	}

	public String getJob_proformamarginvgp() {
		return job_proformamarginvgp;
	}

	public void setJob_proformamarginvgp(String job_proformamarginvgp) {
		this.job_proformamarginvgp = job_proformamarginvgp;
	}

	public String getJob_scope() {
		return job_scope;
	}

	public void setJob_scope(String job_scope) {
		this.job_scope = job_scope;
	}

	public String getJob_siteno() {
		return job_siteno;
	}

	public void setJob_siteno(String job_siteno) {
		this.job_siteno = job_siteno;
	}

	public String getJob_status() {
		return job_status;
	}

	public void setJob_status(String job_status) {
		this.job_status = job_status;
	}

	public String getJob_travelcost() {
		return job_travelcost;
	}

	public void setJob_travelcost(String job_travelcost) {
		this.job_travelcost = job_travelcost;
	}

	public String getJobid() {
		return jobid;
	}

	public void setJobid(String jobid) {
		this.jobid = jobid;
	}

	public String getSite_id() {
		return site_id;
	}

	public void setSite_id(String site_id) {
		this.site_id = site_id;
	}

	public String getSitescheduleactual_enddate() {
		return sitescheduleactual_enddate;
	}

	public void setSitescheduleactual_enddate(String sitescheduleactual_enddate) {
		this.sitescheduleactual_enddate = sitescheduleactual_enddate;
	}

	public String getSitescheduleactual_startdate() {
		return sitescheduleactual_startdate;
	}

	public void setSitescheduleactual_startdate(
			String sitescheduleactual_startdate) {
		this.sitescheduleactual_startdate = sitescheduleactual_startdate;
	}

	public String getSitescheduleplanned_enddate() {
		return sitescheduleplanned_enddate;
	}

	public void setSitescheduleplanned_enddate(
			String sitescheduleplanned_enddate) {
		this.sitescheduleplanned_enddate = sitescheduleplanned_enddate;
	}

	public String getSitescheduleplanned_startdate() {
		return sitescheduleplanned_startdate;
	}

	public void setSitescheduleplanned_startdate(
			String sitescheduleplanned_startdate) {
		this.sitescheduleplanned_startdate = sitescheduleplanned_startdate;
	}

	public String getAppendix_name() {
		return appendix_name;
	}

	public void setAppendix_name(String appendix_name) {
		this.appendix_name = appendix_name;
	}

	public String getJob_name() {
		return job_name;
	}

	public void setJob_name(String job_name) {
		this.job_name = job_name;
	}

	public String getAppendix_id() {
		return appendix_id;
	}

	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}

	public String getJob_siteaddress() {
		return job_siteaddress;
	}

	public void setJob_siteaddress(String job_siteaddress) {
		this.job_siteaddress = job_siteaddress;
	}

	public String getJob_sitename() {
		return job_sitename;
	}

	public void setJob_sitename(String job_sitename) {
		this.job_sitename = job_sitename;
	}

	public String getJob_sitepoc() {
		return job_sitepoc;
	}

	public void setJob_sitepoc(String job_sitepoc) {
		this.job_sitepoc = job_sitepoc;
	}

	public String getJob_sitestate() {
		return job_sitestate;
	}

	public void setJob_sitestate(String job_sitestate) {
		this.job_sitestate = job_sitestate;
	}

	public String getJob_sitezipcode() {
		return job_sitezipcode;
	}

	public void setJob_sitezipcode(String job_sitezipcode) {
		this.job_sitezipcode = job_sitezipcode;
	}

	public String getJob_sitecity() {
		return job_sitecity;
	}

	public void setJob_sitecity(String job_sitecity) {
		this.job_sitecity = job_sitecity;
	}

	public String getSitescheduleactual_enddatetime() {
		return sitescheduleactual_enddatetime;
	}

	public void setSitescheduleactual_enddatetime(
			String sitescheduleactual_enddatetime) {
		this.sitescheduleactual_enddatetime = sitescheduleactual_enddatetime;
	}

	public String getSitescheduleactual_startdatetime() {
		return sitescheduleactual_startdatetime;
	}

	public void setSitescheduleactual_startdatetime(
			String sitescheduleactual_startdatetime) {
		this.sitescheduleactual_startdatetime = sitescheduleactual_startdatetime;
	}

	public String getSitescheduleplanned_enddatetime() {
		return sitescheduleplanned_enddatetime;
	}

	public void setSitescheduleplanned_enddatetime(
			String sitescheduleplanned_enddatetime) {
		this.sitescheduleplanned_enddatetime = sitescheduleplanned_enddatetime;
	}

	public String getSitescheduleplanned_startdatetime() {
		return sitescheduleplanned_startdatetime;
	}

	public void setSitescheduleplanned_startdatetime(
			String sitescheduleplanned_startdatetime) {
		this.sitescheduleplanned_startdatetime = sitescheduleplanned_startdatetime;
	}

	public String getJobtype() {
		return jobtype;
	}

	public void setJobtype(String jobtype) {
		this.jobtype = jobtype;
	}

	public String getInv_id() {
		return inv_id;
	}

	public void setInv_id(String inv_id) {
		this.inv_id = inv_id;
	}

	public String getInv_rdofilter() {
		return inv_rdofilter;
	}

	public void setInv_rdofilter(String inv_rdofilter) {
		this.inv_rdofilter = inv_rdofilter;
	}

	public String getInv_type() {
		return inv_type;
	}

	public void setInv_type(String inv_type) {
		this.inv_type = inv_type;
	}

	public String getTextboxdummy() {
		return textboxdummy;
	}

	public void setTextboxdummy(String textboxdummy) {
		this.textboxdummy = textboxdummy;
	}

	// for netmedx job//
	public String getJob_netcategory() {
		return job_netcategory;
	}

	public void setJob_netcategory(String job_netcategory) {
		this.job_netcategory = job_netcategory;
	}

	public String getJob_netpreferred_arrival_start_date() {
		return job_netpreferred_arrival_start_date;
	}

	public void setJob_netpreferred_arrival_start_date(
			String job_netpreferred_arrival_start_date) {
		this.job_netpreferred_arrival_start_date = job_netpreferred_arrival_start_date;
	}

	public String getJob_netpreferred_arrival_start_time() {
		return job_netpreferred_arrival_start_time;
	}

	public void setJob_netpreferred_arrival_start_time(
			String job_netpreferred_arrival_start_time) {
		this.job_netpreferred_arrival_start_time = job_netpreferred_arrival_start_time;
	}

	public String getJob_netpreferred_arrival_window_end_time() {
		return job_netpreferred_arrival_window_end_time;
	}

	public void setJob_netpreferred_arrival_window_end_time(
			String job_netpreferred_arrival_window_end_time) {
		this.job_netpreferred_arrival_window_end_time = job_netpreferred_arrival_window_end_time;
	}

	public String getJob_netpreferred_arrival_window_start_date() {
		return job_netpreferred_arrival_window_start_date;
	}

	public void setJob_netpreferred_arrival_window_start_date(
			String job_netpreferred_arrival_window_start_date) {
		this.job_netpreferred_arrival_window_start_date = job_netpreferred_arrival_window_start_date;
	}

	public String getJob_netpreferred_arrival_window_start_time() {
		return job_netpreferred_arrival_window_start_time;
	}

	public void setJob_netpreferred_arrival_window_start_time(
			String job_netpreferred_arrival_window_start_time) {
		this.job_netpreferred_arrival_window_start_time = job_netpreferred_arrival_window_start_time;
	}

	public String getJob_netproblemdesc() {
		return job_netproblemdesc;
	}

	public void setJob_netproblemdesc(String job_netproblemdesc) {
		this.job_netproblemdesc = job_netproblemdesc;
	}

	public String getJob_netrequestor() {
		return job_netrequestor;
	}

	public void setJob_netrequestor(String job_netrequestor) {
		this.job_netrequestor = job_netrequestor;
	}

	public String getJob_netspecialcondition() {
		return job_netspecialcondition;
	}

	public void setJob_netspecialcondition(String job_netspecialcondition) {
		this.job_netspecialcondition = job_netspecialcondition;
	}

	public String getJob_netspecialinstruct() {
		return job_netspecialinstruct;
	}

	public void setJob_netspecialinstruct(String job_netspecialinstruct) {
		this.job_netspecialinstruct = job_netspecialinstruct;
	}

	public String getJob_vpreferred_arrival_window_end_date() {
		return job_vpreferred_arrival_window_end_date;
	}

	public void setJob_vpreferred_arrival_window_end_date(
			String job_vpreferred_arrival_window_end_date) {
		this.job_vpreferred_arrival_window_end_date = job_vpreferred_arrival_window_end_date;
	}

	public String getJob_netcust_reference() {
		return job_netcust_reference;
	}

	public void setJob_netcust_reference(String job_netcust_reference) {
		this.job_netcust_reference = job_netcust_reference;
	}

	public String getJob_netinstallation_comment() {
		return job_netinstallation_comment;
	}

	public void setJob_netinstallation_comment(
			String job_netinstallation_comment) {
		this.job_netinstallation_comment = job_netinstallation_comment;
	}

	public String getJob_netinstallation_date() {
		return job_netinstallation_date;
	}

	public void setJob_netinstallation_date(String job_netinstallation_date) {
		this.job_netinstallation_date = job_netinstallation_date;
	}

	public String getJob_netinstallation_time() {
		return job_netinstallation_time;
	}

	public void setJob_netinstallation_time(String job_netinstallation_time) {
		this.job_netinstallation_time = job_netinstallation_time;
	}

	public Collection getInstallationlist() {
		return installationlist;
	}

	public void setInstallationlist(Collection installationlist) {
		this.installationlist = installationlist;
	}

	public String getJob_netactualprice() {
		return job_netactualprice;
	}

	public void setJob_netactualprice(String job_netactualprice) {
		this.job_netactualprice = job_netactualprice;
	}

	public String getJob_netactualvgp() {
		return job_netactualvgp;
	}

	public void setJob_netactualvgp(String job_netactualvgp) {
		this.job_netactualvgp = job_netactualvgp;
	}

	public String getJob_invoiceno() {
		return job_invoiceno;
	}

	public void setJob_invoiceno(String job_invoiceno) {
		this.job_invoiceno = job_invoiceno;
	}

	public String getJob_netauthorizedtotalcost() {
		return job_netauthorizedtotalcost;
	}

	public void setJob_netauthorizedtotalcost(String job_netauthorizedtotalcost) {
		this.job_netauthorizedtotalcost = job_netauthorizedtotalcost;
	}

	public String getJob_priemailid() {
		return job_priemailid;
	}

	public void setJob_priemailid(String job_priemailid) {
		this.job_priemailid = job_priemailid;
	}

	public String getJob_sitephoneno() {
		return job_sitephoneno;
	}

	public void setJob_sitephoneno(String job_sitephoneno) {
		this.job_sitephoneno = job_sitephoneno;
	}

	public String getJob_sitesecemailid() {
		return job_sitesecemailid;
	}

	public void setJob_sitesecemailid(String job_sitesecemailid) {
		this.job_sitesecemailid = job_sitesecemailid;
	}

	public String getJob_sitesecphone() {
		return job_sitesecphone;
	}

	public void setJob_sitesecphone(String job_sitesecphone) {
		this.job_sitesecphone = job_sitesecphone;
	}

	public String getJob_sitesecpoc() {
		return job_sitesecpoc;
	}

	public void setJob_sitesecpoc(String job_sitesecpoc) {
		this.job_sitesecpoc = job_sitesecpoc;
	}

	public String getJob_netticketid() {
		return job_netticketid;
	}

	public void setJob_netticketid(String job_netticketid) {
		this.job_netticketid = job_netticketid;
	}

	public String getTime_onsite() {
		return time_onsite;
	}

	public void setTime_onsite(String time_onsite) {
		this.time_onsite = time_onsite;
	}

	public String getTime_toarrive() {
		return time_toarrive;
	}

	public void setTime_toarrive(String time_toarrive) {
		this.time_toarrive = time_toarrive;
	}

	public String getNettype() {
		return nettype;
	}

	public void setNettype(String nettype) {
		this.nettype = nettype;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getLo_ot_name() {
		return lo_ot_name;
	}

	public void setLo_ot_name(String lo_ot_name) {
		this.lo_ot_name = lo_ot_name;
	}

	public String getJob_sitenumber() {
		return job_sitenumber;
	}

	public void setJob_sitenumber(String job_sitenumber) {
		this.job_sitenumber = job_sitenumber;
	}

	public String getJobcreatedby() {
		return jobcreatedby;
	}

	public void setJobcreatedby(String jobcreatedby) {
		this.jobcreatedby = jobcreatedby;
	}

	public String getCreatedate() {
		return createdate;
	}

	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public String getMsa_id() {
		return msa_id;
	}

	public void setMsa_id(String msa_id) {
		this.msa_id = msa_id;
	}

	public String getRequest_received_date() {
		return request_received_date;
	}

	public void setRequest_received_date(String request_received_date) {
		this.request_received_date = request_received_date;
	}

	public String getRequest_received_time() {
		return request_received_time;
	}

	public void setRequest_received_time(String request_received_time) {
		this.request_received_time = request_received_time;
	}

	public String getOptactivity() {
		return optactivity;
	}

	public void setOptactivity(String optactivity) {
		this.optactivity = optactivity;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getStringChangeStatus() {
		return stringChangeStatus;
	}

	public void setStringChangeStatus(String stringChangeStatus) {
		this.stringChangeStatus = stringChangeStatus;
	}

	public String getCustref() {
		return custref;
	}

	public void setCustref(String custref) {
		this.custref = custref;
	}

	public String getSchedule_position() {
		return schedule_position;
	}

	public void setSchedule_position(String schedule_position) {
		this.schedule_position = schedule_position;
	}

	public int getSchedule_total() {
		return schedule_total;
	}

	public void setSchedule_total(int schedule_total) {
		this.schedule_total = schedule_total;
	}

	public String getTotaltime_onsite() {
		return totaltime_onsite;
	}

	public void setTotaltime_onsite(String totaltime_onsite) {
		this.totaltime_onsite = totaltime_onsite;
	}

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	public String getAtAfterHoursUplift() {
		return atAfterHoursUplift;
	}

	public void setAtAfterHoursUplift(String atAfterHoursUplift) {
		this.atAfterHoursUplift = atAfterHoursUplift;
	}

	public String getAtExpedite24Uplift() {
		return atExpedite24Uplift;
	}

	public void setAtExpedite24Uplift(String atExpedite24Uplift) {
		this.atExpedite24Uplift = atExpedite24Uplift;
	}

	public String getAtExpedite48Uplift() {
		return atExpedite48Uplift;
	}

	public void setAtExpedite48Uplift(String atExpedite48Uplift) {
		this.atExpedite48Uplift = atExpedite48Uplift;
	}

	public String getAtWhUplift() {
		return atWhUplift;
	}

	public void setAtWhUplift(String atWhUplift) {
		this.atWhUplift = atWhUplift;
	}

	public String getAtUpliftCost() {
		return atUpliftCost;
	}

	public void setAtUpliftCost(String atUpliftCost) {
		this.atUpliftCost = atUpliftCost;
	}

	public String getInvoice_Flag() {
		return invoice_Flag;
	}

	public void setInvoice_Flag(String invoice_Flag) {
		this.invoice_Flag = invoice_Flag;
	}

	public String getInvoiceno() {
		return invoiceno;
	}

	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}

	public String getFrom_date() {
		return from_date;
	}

	public void setFrom_date(String from_date) {
		this.from_date = from_date;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public String getPowo_number() {
		return powo_number;
	}

	public void setPowo_number(String powo_number) {
		this.powo_number = powo_number;
	}

	public String getTo_date() {
		return to_date;
	}

	public void setTo_date(String to_date) {
		this.to_date = to_date;
	}

	public String getCustinfodata10() {
		return custinfodata10;
	}

	public void setCustinfodata10(String custinfodata10) {
		this.custinfodata10 = custinfodata10;
	}

	public String getCustinfodata4() {
		return custinfodata4;
	}

	public void setCustinfodata4(String custinfodata4) {
		this.custinfodata4 = custinfodata4;
	}

	public String getCustinfodata5() {
		return custinfodata5;
	}

	public void setCustinfodata5(String custinfodata5) {
		this.custinfodata5 = custinfodata5;
	}

	public String getCustinfodata6() {
		return custinfodata6;
	}

	public void setCustinfodata6(String custinfodata6) {
		this.custinfodata6 = custinfodata6;
	}

	public String getCustinfodata7() {
		return custinfodata7;
	}

	public void setCustinfodata7(String custinfodata7) {
		this.custinfodata7 = custinfodata7;
	}

	public String getCustinfodata8() {
		return custinfodata8;
	}

	public void setCustinfodata8(String custinfodata8) {
		this.custinfodata8 = custinfodata8;
	}

	public String getCustinfodata9() {
		return custinfodata9;
	}

	public void setCustinfodata9(String custinfodata9) {
		this.custinfodata9 = custinfodata9;
	}

	public String getCustinfovalue10() {
		return custinfovalue10;
	}

	public void setCustinfovalue10(String custinfovalue10) {
		this.custinfovalue10 = custinfovalue10;
	}

	public String getCustinfovalue4() {
		return custinfovalue4;
	}

	public void setCustinfovalue4(String custinfovalue4) {
		this.custinfovalue4 = custinfovalue4;
	}

	public String getCustinfovalue5() {
		return custinfovalue5;
	}

	public void setCustinfovalue5(String custinfovalue5) {
		this.custinfovalue5 = custinfovalue5;
	}

	public String getCustinfovalue6() {
		return custinfovalue6;
	}

	public void setCustinfovalue6(String custinfovalue6) {
		this.custinfovalue6 = custinfovalue6;
	}

	public String getCustinfovalue7() {
		return custinfovalue7;
	}

	public void setCustinfovalue7(String custinfovalue7) {
		this.custinfovalue7 = custinfovalue7;
	}

	public String getCustinfovalue8() {
		return custinfovalue8;
	}

	public void setCustinfovalue8(String custinfovalue8) {
		this.custinfovalue8 = custinfovalue8;
	}

	public String getCustinfovalue9() {
		return custinfovalue9;
	}

	public void setCustinfovalue9(String custinfovalue9) {
		this.custinfovalue9 = custinfovalue9;
	}

	public String getJob_actualvgp_new() {
		return job_actualvgp_new;
	}

	public void setJob_actualvgp_new(String job_actualvgp_new) {
		this.job_actualvgp_new = job_actualvgp_new;
	}

	public String getJobScheduler() {
		return jobScheduler;
	}

	public void setJobScheduler(String jobScheduler) {
		this.jobScheduler = jobScheduler;
	}

	public String getJobRseCount() {
		return jobRseCount;
	}

	public void setJobRseCount(String jobRseCount) {
		this.jobRseCount = jobRseCount;
	}

}
