package com.mind.bean.prm;

import java.io.Serializable;

public class TicketDetailBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String depot_quantity = null;
	private String de_product_id = null;
	private String de_product_name = null;
	
	public String getDe_product_id() {
		return de_product_id;
	}
	public void setDe_product_id(String de_product_id) {
		this.de_product_id = de_product_id;
	}
	public String getDe_product_name() {
		return de_product_name;
	}
	public void setDe_product_name(String de_product_name) {
		this.de_product_name = de_product_name;
	}
	public String getDepot_quantity() {
		return depot_quantity;
	}
	public void setDepot_quantity(String depot_quantity) {
		this.depot_quantity = depot_quantity;
	}
	

	
	
}
