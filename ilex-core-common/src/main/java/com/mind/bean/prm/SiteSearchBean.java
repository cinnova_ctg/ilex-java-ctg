package com.mind.bean.prm;

import java.util.Collection;



public class SiteSearchBean extends JobSetUpDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String siteid = null;
	
	private String site_name = null;
	private String site_number = null;
	private String site_state = null;
	private String site_city = null;
	private String site_brand = null;
	private String site_end_customer = null;
	private String siteCountryId = null;
	private Collection tempList = null;  
	private String msa_id = null;

	//Added by Seema-14/11/2006
	private String msa_name = null;
	
	private String[] sitename = null;
	private String[] sitenumber = null;
	private String[] siteaddress = null;
	private String[] sitecity = null;
	private String[] sitestate = null;
	private String[] sitestatedesc = null;
	private String[] sitecountry = null;
	private String search = null;
	private String selectedsite = null;
	private String ref = null;
	private String reset = null;
	private String back = null;
	private String authenticate = "";
	private String[] msaid = null;
	private String norecordmessage = null;
	private String assignmessage = null;
	private String save = null;
	private String jobid = null;
	private String appendixid = null;
	private String[] sitelistid = null;
	private String[] sitelocalityfactor = null;
	private String[] unionsite = null;
	private String[] sitedesignator = null;
	private String[] siteworklocation = null;
	private String[] sitezipcode = null;
	private String[] sitesecphone = null;
	private String[] sitephone = null;
	private String[] sitedirection = null;
	private String[] sitepripoc = null;
	private String[] sitesecpoc = null;
	private String[] sitepriemail = null;
	private String[] sitesecemail = null;
	private String[] sitenotes = null;
	private String[] installerpoc = null;
	private String[] sitepoc = null;
	private String reftype = null;
	
	private String lo_ot_id = null;
	private String add = null;
	private String fromType =null;
	
	private String appendixname = null;
	private String jobName = null;
	
	
	public String getAppendixname() {
		return appendixname;
	}

	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}

	public String getAdd() {
		return add;
	}

	public void setAdd(String add) {
		this.add = add;
	}



	



	public String getAppendixid() {
		return appendixid;
	}



	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}



	public String getAssignmessage() {
		return assignmessage;
	}



	public void setAssignmessage(String assignmessage) {
		this.assignmessage = assignmessage;
	}



	public String getAuthenticate() {
		return authenticate;
	}



	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}



	public String getBack() {
		return back;
	}



	public void setBack(String back) {
		this.back = back;
	}

	
	
	

	public String getLo_ot_id() {
		return lo_ot_id;
	}



	public void setLo_ot_id(String lo_ot_id) {
		this.lo_ot_id = lo_ot_id;
	}



	public String[] getInstallerpoc() {
		return installerpoc;
	}

	public String getInstallerpoc( int i ) 
	{
		return installerpoc[i];
	}

	public void setInstallerpoc(String[] installerpoc) {
		this.installerpoc = installerpoc;
	}



	public String getJobid() {
		return jobid;
	}



	public void setJobid(String jobid) {
		this.jobid = jobid;
	}



	public String[] getMsaid() {
		return msaid;
	}



	public void setMsaid(String[] msaid) {
		this.msaid = msaid;
	}

	public String getMsaid( int i ) 
	{
		return msaid[i];
	}

	public String getNorecordmessage() {
		return norecordmessage;
	}



	public void setNorecordmessage(String norecordmessage) {
		this.norecordmessage = norecordmessage;
	}



	public String getRef() {
		return ref;
	}



	public void setRef(String ref) {
		this.ref = ref;
	}



	public String getReftype() {
		return reftype;
	}



	public void setReftype(String reftype) {
		this.reftype = reftype;
	}



	public String getReset() {
		return reset;
	}



	public void setReset(String reset) {
		this.reset = reset;
	}



	public String getSave() {
		return save;
	}



	public void setSave(String save) {
		this.save = save;
	}


	public String getSearch() {
		return search;
	}



	public void setSearch(String search) {
		this.search = search;
	}



	public String getSelectedsite() {
		return selectedsite;
	}



	public void setSelectedsite(String selectedsite) {
		this.selectedsite = selectedsite;
	}



	public String getSite_name() {
		return site_name;
	}



	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}



	public String getSite_number() {
		return site_number;
	}



	public void setSite_number(String site_number) {
		this.site_number = site_number;
	}



	public String[] getSiteaddress() {
		return siteaddress;
	}
	
	public String getSiteaddress( int i ) 
	{
		return siteaddress[i];
	}


	public void setSiteaddress(String[] siteaddress) {
		this.siteaddress = siteaddress;
	}



	public String[] getSitecity() {
		return sitecity;
	}

	public String getSitecity( int i ) 
	{
		return sitecity[i];
	}

	public void setSitecity(String[] sitecity) {
		this.sitecity = sitecity;
	}



	public String[] getSitecountry() {
		return sitecountry;
	}
	
	public String getSitecountry( int i ) 
	{
		return sitecountry[i];
	}


	public void setSitecountry(String[] sitecountry) {
		this.sitecountry = sitecountry;
	}



	public String[] getSitedesignator() {
		return sitedesignator;
	}

	public String getSitedesignator( int i ) 
	{
		return sitedesignator[i];
	}


	public void setSitedesignator(String[] sitedesignator) {
		this.sitedesignator = sitedesignator;
	}



	public String[] getSitedirection() {
		return sitedirection;
	}

	public String getSitedirection( int i ) 
	{
		return sitedirection[i];
	}


	public void setSitedirection(String[] sitedirection) {
		this.sitedirection = sitedirection;
	}



	public String getSiteid() {
		return siteid;
	}



	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}



	public String[] getSitelistid() {
		return sitelistid;
	}

	public String getSitelistid( int i ) 
	{
		return sitelistid[i];
	}

	public void setSitelistid(String[] sitelistid) {
		this.sitelistid = sitelistid;
	}



	public String[] getSitelocalityfactor() {
		return sitelocalityfactor;
	}

	public String getSitelocalityfactor( int i ) 
	{
		return sitelocalityfactor[i];
	}

	public void setSitelocalityfactor(String[] sitelocalityfactor) {
		this.sitelocalityfactor = sitelocalityfactor;
	}



	public String[] getSitename() {
		return sitename;
	}

	public String getSitename( int i ) 
	{
		return sitename[i];
	}

	public void setSitename(String[] sitename) {
		this.sitename = sitename;
	}



	public String[] getSitenotes() {
		return sitenotes;
	}

	public String getSitenotes( int i ) 
	{
		return sitenotes[i];
	}

	public void setSitenotes(String[] sitenotes) {
		this.sitenotes = sitenotes;
	}



	public String[] getSitenumber() {
		return sitenumber;
	}

	public String getSitenumber( int i ) 
	{
		return sitenumber[i];
	}

	public void setSitenumber(String[] sitenumber) {
		this.sitenumber = sitenumber;
	}



	public String[] getSitephone() {
		return sitephone;
	}

	public String getSitephone( int i ) 
	{
		return sitephone[i];
	}

	public void setSitephone(String[] sitephone) {
		this.sitephone = sitephone;
	}



	public String[] getSitepoc() {
		return sitepoc;
	}

	public String getSitepoc( int i ) 
	{
		return sitepoc[i];
	}

	public void setSitepoc(String[] sitepoc) {
		this.sitepoc = sitepoc;
	}



	public String[] getSitepriemail() {
		return sitepriemail;
	}

	public String getSitepriemail( int i ) 
	{
		return sitepriemail[i];
	}

	public void setSitepriemail(String[] sitepriemail) {
		this.sitepriemail = sitepriemail;
	}



	public String[] getSitepripoc() {
		return sitepripoc;
	}

	public String getSitepripoc( int i ) 
	{
		return sitepripoc[i];
	}

	public void setSitepripoc(String[] sitepripoc) {
		this.sitepripoc = sitepripoc;
	}



	public String[] getSitesecemail() {
		return sitesecemail;
	}

	public String getSitesecemail( int i ) 
	{
		return sitesecemail[i];
	}

	public void setSitesecemail(String[] sitesecemail) {
		this.sitesecemail = sitesecemail;
	}



	public String[] getSitesecphone() {
		return sitesecphone;
	}

	public String getSitesecphone( int i ) 
	{
		return sitesecphone[i];
	}

	public void setSitesecphone(String[] sitesecphone) {
		this.sitesecphone = sitesecphone;
	}



	public String[] getSitesecpoc() {
		return sitesecpoc;
	}

	public String getSitesecpoc( int i ) 
	{
		return sitesecpoc[i];
	}

	public void setSitesecpoc(String[] sitesecpoc) {
		this.sitesecpoc = sitesecpoc;
	}



	public String[] getSitestate() {
		return sitestate;
	}


	public String getSitestate( int i ) 
	{
		return sitestate[i];
	}
	
	public void setSitestate(String[] sitestate) {
		this.sitestate = sitestate;
	}



	public String[] getSiteworklocation() {
		return siteworklocation;
	}

	public String getSiteworklocation( int i ) 
	{
		return siteworklocation[i];
	}

	public void setSiteworklocation(String[] siteworklocation) {
		this.siteworklocation = siteworklocation;
	}



	public String[] getSitezipcode() {
		return sitezipcode;
	}

	public String getSitezipcode( int i ) 
	{
		return sitezipcode[i];
	}

	public void setSitezipcode(String[] sitezipcode) {
		this.sitezipcode = sitezipcode;
	}



	public String[] getUnionsite() {
		return unionsite;
	}

	public String getUnionsite( int i ) 
	{
		return unionsite[i];
	}

	public void setUnionsite(String[] unionsite) {
		this.unionsite = unionsite;
	}



	public String getMsa_id() {
		return msa_id;
	}

	

	public void setMsa_id(String msa_id) {
		this.msa_id = msa_id;
	}



	public String[] getSitestatedesc() {
		return sitestatedesc;
	}

	public String getSitestatedesc( int i ) 
	{
		return sitestatedesc[i];
	}

	public void setSitestatedesc(String[] sitestatedesc) {
		this.sitestatedesc = sitestatedesc;
	}



	public String getSite_city() {
		return site_city;
	}



	public void setSite_city(String site_city) {
		this.site_city = site_city;
	}



	public String getSite_state() {
		return site_state;
	}



	public void setSite_state(String site_state) {
		this.site_state = site_state;
	}


// added by Seema-14/11/2006
	public String getMsa_name() {
		return msa_name;
	}



	public void setMsa_name(String msa_name) {
		this.msa_name = msa_name;
	}



	public String getSite_brand() {
		return site_brand;
	}



	public void setSite_brand(String site_brand) {
		this.site_brand = site_brand;
	}



	public String getSiteCountryId() {
		return siteCountryId;
	}



	public void setSiteCountryId(String siteCountryId) {
		this.siteCountryId = siteCountryId;
	}



	public Collection getTempList() {
		return tempList;
	}



	public void setTempList(Collection tempList) {
		this.tempList = tempList;
	}



	public String getSite_end_customer() {
		return site_end_customer;
	}



	public void setSite_end_customer(String site_end_customer) {
		this.site_end_customer = site_end_customer;
	}

	public String getFromType() {
		return fromType;
	}

	public void setFromType(String fromType) {
		this.fromType = fromType;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	
	

}
