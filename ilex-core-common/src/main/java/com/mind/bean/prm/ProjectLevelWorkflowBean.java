package com.mind.bean.prm;

import java.io.Serializable;
import java.util.Date;

public class ProjectLevelWorkflowBean extends JobSetUp implements Serializable {
	String jobId="";
	String appendixId="";
	private long itemId;
	private String description;
	private String sequence;
	private Date referenceDate;
	private String user;
	private String hmode="";
	String[] seq=null;
	String[] desc=null;
	String[] item_id=null;
	String[] del_item=null;
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	public Date getReferenceDate() {
		return referenceDate;
	}

	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getHmode() {
		return hmode;
	}

	public void setHmode(String hmode) {
		this.hmode = hmode;
	}

	public String[] getDesc() {
		return desc;
	}

	public void setDesc(String[] desc) {
		this.desc = desc;
	}

	public String[] getSeq() {
		return seq;
	}

	public void setSeq(String[] seq) {
		this.seq = seq;
	}

	public String[] getItem_id() {
		return item_id;
	}

	public void setItem_id(String[] item_id) {
		this.item_id = item_id;
	}

	public String[] getDel_item() {
		return del_item;
	}

	public void setDel_item(String[] del_item) {
		this.del_item = del_item;
	}
	
}



