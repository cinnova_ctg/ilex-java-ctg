package com.mind.bean.prm;

import java.io.Serializable;

public class JobSetUp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	
		String jobId= "";
		String jobStatus = "";
		String appendixId = "";
		String tabId = "";
		String ticket_id = "";
		String site_id = "";
		String createdBy = "";
		String hmode = "";
		//Menu
		private String msaId = null;
		private String msaName = null;
		private String appendixName = null;
		private String jobName = null;
		private String appendixType = null;
		private String jobType = null;
		private String activity_Id=null;
		private String activitylibrary_Id=null;	
		public String getJobType() {
			return jobType;
		}

		public void setJobType(String jobType) {
			this.jobType = jobType;
		}

		public String getAppendixName() {
			return appendixName;
		}

		public void setAppendixName(String appendixName) {
			this.appendixName = appendixName;
		}

		public String getJobName() {
			return jobName;
		}

		public void setJobName(String jobName) {
			this.jobName = jobName;
		}

		public String getMsaId() {
			return msaId;
		}

		public void setMsaId(String msaId) {
			this.msaId = msaId;
		}

		public String getMsaName() {
			return msaName;
		}

		public void setMsaName(String msaName) {
			this.msaName = msaName;
		}

		public String getSite_id() {
			return site_id;
		}

		public void setSite_id(String site_id) {
			this.site_id = site_id;
		}

		public String getTicket_id() {
			return ticket_id;
		}

		public void setTicket_id(String ticket_id) {
			this.ticket_id = ticket_id;
		}

		public String getJobStatus() {
			return jobStatus;
		}

		public void setJobStatus(String jobStatus) {
			this.jobStatus = jobStatus;
		}

		public String getTabId() {
			return tabId;
		}

		public void setTabId(String tabId) {
			this.tabId = tabId;
		}

		public String getAppendixId() {
			return appendixId;
		}

		public void setAppendixId(String appendixId) {
			this.appendixId = appendixId;
		}

		public String getAppendixType() {
			return appendixType;
		}

		public void setAppendixType(String appendixType) {
			this.appendixType = appendixType;
		}

		public String getJobId() {
			return jobId;
		}

		public void setJobId(String jobId) {
			this.jobId = jobId;
		}

		public String getCreatedBy() {
			return createdBy;
		}

		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}

		public String getHmode() {
			return hmode;
		}

		public void setHmode(String hmode) {
			this.hmode = hmode;
		}

		public String getActivity_Id() {
			return activity_Id;
		}

		public void setActivity_Id(String activity_Id) {
			this.activity_Id = activity_Id;
		}

		public String getActivitylibrary_Id() {
			return activitylibrary_Id;
		}

		public void setActivitylibrary_Id(String activitylibrary_Id) {
			this.activitylibrary_Id = activitylibrary_Id;
		}

		
	}

	


