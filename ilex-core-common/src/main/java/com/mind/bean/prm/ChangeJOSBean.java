package com.mind.bean.prm;

import java.io.Serializable;
import java.util.ArrayList;

public class ChangeJOSBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String jobOwner=null;
	private String appendixid=null;
	private String msaId=null;
	private String getJobOwnerOtherCheck=null;
	private String ownerId=null;
	private String appendixName=null;
	private String newOwner=null;
	private String newScheduler=null;
	private String siteNumber=null;
	private String CountryId=null;
	private String CountryName=null;
	private ArrayList tempList = null;
	private String stateCmb= null;
	private String msaname=null;
	private String jobEndCustomer = null;
	private String brand=null;
	private String search=null;
	private String[] jobIdCheck=null;
	private String jobSiteNumber=null;
	private String jobStatus=null;
	private String ownerJob=null;
	private String jobScheduler=null;
	private String jobName=null;
	private String planedStartDate=null;
	private String jobSelectStatus=null;
	private String newSearchOwner=null;
	private String city = null;
	private String resultPlannedStart=null;
	private String newSearchScheduler=null;
	private String update=null;
	private String jobId=null;
	private String viewjobtype=null;
	
//	For View Selector: Start 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
//	For View Selector: End

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	public String getNewSearchScheduler() {
		return newSearchScheduler;
	}

	public void setNewSearchScheduler(String newSearchScheduler) {
		this.newSearchScheduler = newSearchScheduler;
	}

	public String getResultPlannedStart() {
		return resultPlannedStart;
	}

	public void setResultPlannedStart(String resultPlannedStart) {
		this.resultPlannedStart = resultPlannedStart;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getNewSearchOwner() {
		return newSearchOwner;
	}

	public void setNewSearchOwner(String newSearchOwner) {
		this.newSearchOwner = newSearchOwner;
	}

	public String getPlanedStartDate() {
		return planedStartDate;
	}

	public void setPlanedStartDate(String planedStartDate) {
		this.planedStartDate = planedStartDate;
	}

	public String[] getJobIdCheck() {
		return jobIdCheck;
	}

	public void setJobIdCheck(String[] jobIdCheck) {
		this.jobIdCheck = jobIdCheck;
	}
	public String getJobIdCheck(int i) {
		return jobIdCheck[i];
	}

	public String getJobScheduler() {
		return jobScheduler;
	}

	public void setJobScheduler(String jobScheduler) {
		this.jobScheduler = jobScheduler;
	}

	public String getJobSiteNumber() {
		return jobSiteNumber;
	}

	public void setJobSiteNumber(String jobSiteNumber) {
		this.jobSiteNumber = jobSiteNumber;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getOwnerJob() {
		return ownerJob;
	}

	public void setOwnerJob(String ownerJob) {
		this.ownerJob = ownerJob;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getJobOwner() {
		return jobOwner;
	}


	public String getAppendixid() {
		return appendixid;
	}

	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	
	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getNewOwner() {
		return newOwner;
	}

	public void setNewOwner(String newOwner) {
		this.newOwner = newOwner;
	}

	public String getNewScheduler() {
		return newScheduler;
	}

	public void setNewScheduler(String newScheduler) {
		this.newScheduler = newScheduler;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getCountryId() {
		return CountryId;
	}

	public void setCountryId(String countryId) {
		CountryId = countryId;
	}

	public String getCountryName() {
		return CountryName;
	}

	public void setCountryName(String countryName) {
		CountryName = countryName;
	}

	public ArrayList getTempList() {
		return tempList;
	}

	public void setTempList(ArrayList tempList) {
		this.tempList = tempList;
	}

	public String getStateCmb() {
		return stateCmb;
	}

	public void setStateCmb(String stateCmb) {
		this.stateCmb = stateCmb;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getJobEndCustomer() {
		return jobEndCustomer;
	}

	public void setJobEndCustomer(String jobEndCustomer) {
		this.jobEndCustomer = jobEndCustomer;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public void setJobOwner(String jobOwner) {
		this.jobOwner = jobOwner;
	}

	public String getJobSelectStatus() {
		return jobSelectStatus;
	}

	public void setJobSelectStatus(String jobSelectStatus) {
		this.jobSelectStatus = jobSelectStatus;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getGetJobOwnerOtherCheck() {
		return getJobOwnerOtherCheck;
	}

	public void setGetJobOwnerOtherCheck(String getJobOwnerOtherCheck) {
		this.getJobOwnerOtherCheck = getJobOwnerOtherCheck;
	}

	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}

	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}
	
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	
	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}

	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}

	
}


