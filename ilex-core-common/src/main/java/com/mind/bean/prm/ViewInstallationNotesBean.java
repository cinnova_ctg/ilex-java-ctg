package com.mind.bean.prm;

import java.io.Serializable;

public class ViewInstallationNotesBean implements Serializable {
	private String installnotes = null;
	private String createdatetime = null;
	private String function = null;
	private String appendixid = null;
	private String jobid = null;
	private String appendixname = null;
	private String jobname = null;
	private String authenticate = "";
	private String createdby = null;
	private String back = null;
	private String from = null;
	private String nettype = null;
	private String viewjobtype = null;
	private String ownerId = null;
	private String typeId = null;

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getAppendixid() {
		return appendixid;
	}

	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}

	public String getAppendixname() {
		return appendixname;
	}

	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}

	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public String getCreatedatetime() {
		return createdatetime;
	}

	public void setCreatedatetime(String createdatetime) {
		this.createdatetime = createdatetime;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getInstallnotes() {
		return installnotes;
	}

	public void setInstallnotes(String installnotes) {
		this.installnotes = installnotes;
	}

	public String getJobid() {
		return jobid;
	}

	public void setJobid(String jobid) {
		this.jobid = jobid;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getBack() {
		return back;
	}

	public void setBack(String back) {
		this.back = back;
	}

	public String getJobname() {
		return jobname;
	}

	public void setJobname(String jobname) {
		this.jobname = jobname;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getNettype() {
		return nettype;
	}

	public void setNettype(String nettype) {
		this.nettype = nettype;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

}
