package com.mind.bean.prm;

import java.io.Serializable;

public class ViewTicketSummaryBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private String tc_id = null;
	private String ticket_number = null;
	private String site_number = null;
	private String site_phone = null;
	private String site_worklocation = null;
	private String po_number = null;
	private String site_address = null;
	private String site_city = null;
	private String site_state = null;
	private String site_zipcode = null;
	private String site_name = null;
	private String email = null;
	private String contact_person = null;
	private String contact_phone = null;
	private String primary_Name = null;
	private String pri_site_phone = null;
	private String secondary_Name = null;
	private String secondary_phone = null;
	private String requested_date = null;
	private String criticality = null;
	private String arrivalwindowstartdate = null;
	private String arrivalwindowenddate = null;
	private String arrivaldate = null;
	private String problem_description = null;
	private String sp_instructions = null;
	private String stand_by = null;
	private String other_email_info = null;
	private String cust_specific_fields = null;
	private String created_date = null;
	
	
	public String getArrivaldate() {
		return arrivaldate;
	}
	public void setArrivaldate(String arrivaldate) {
		this.arrivaldate = arrivaldate;
	}
	public String getArrivalwindowenddate() {
		return arrivalwindowenddate;
	}
	public void setArrivalwindowenddate(String arrivalwindowenddate) {
		this.arrivalwindowenddate = arrivalwindowenddate;
	}
	public String getArrivalwindowstartdate() {
		return arrivalwindowstartdate;
	}
	public void setArrivalwindowstartdate(String arrivalwindowstartdate) {
		this.arrivalwindowstartdate = arrivalwindowstartdate;
	}
	public String getContact_person() {
		return contact_person;
	}
	public void setContact_person(String contact_person) {
		this.contact_person = contact_person;
	}
	public String getContact_phone() {
		return contact_phone;
	}
	public void setContact_phone(String contact_phone) {
		this.contact_phone = contact_phone;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public String getCriticality() {
		return criticality;
	}
	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}
	public String getCust_specific_fields() {
		return cust_specific_fields;
	}
	public void setCust_specific_fields(String cust_specific_fields) {
		this.cust_specific_fields = cust_specific_fields;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOther_email_info() {
		return other_email_info;
	}
	public void setOther_email_info(String other_email_info) {
		this.other_email_info = other_email_info;
	}
	public String getPo_number() {
		return po_number;
	}
	public void setPo_number(String po_number) {
		this.po_number = po_number;
	}
	public String getPri_site_phone() {
		return pri_site_phone;
	}
	public void setPri_site_phone(String pri_site_phone) {
		this.pri_site_phone = pri_site_phone;
	}
	public String getPrimary_Name() {
		return primary_Name;
	}
	public void setPrimary_Name(String primary_Name) {
		this.primary_Name = primary_Name;
	}
	public String getProblem_description() {
		return problem_description;
	}
	public void setProblem_description(String problem_description) {
		this.problem_description = problem_description;
	}
	public String getRequested_date() {
		return requested_date;
	}
	public void setRequested_date(String requested_date) {
		this.requested_date = requested_date;
	}
	public String getSecondary_Name() {
		return secondary_Name;
	}
	public void setSecondary_Name(String secondary_Name) {
		this.secondary_Name = secondary_Name;
	}
	public String getSecondary_phone() {
		return secondary_phone;
	}
	public void setSecondary_phone(String secondary_phone) {
		this.secondary_phone = secondary_phone;
	}
	public String getSite_address() {
		return site_address;
	}
	public void setSite_address(String site_address) {
		this.site_address = site_address;
	}
	public String getSite_city() {
		return site_city;
	}
	public void setSite_city(String site_city) {
		this.site_city = site_city;
	}
	public String getSite_name() {
		return site_name;
	}
	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}
	public String getSite_number() {
		return site_number;
	}
	public void setSite_number(String site_number) {
		this.site_number = site_number;
	}
	public String getSite_phone() {
		return site_phone;
	}
	public void setSite_phone(String site_phone) {
		this.site_phone = site_phone;
	}
	public String getSite_state() {
		return site_state;
	}
	public void setSite_state(String site_state) {
		this.site_state = site_state;
	}
	public String getSite_worklocation() {
		return site_worklocation;
	}
	public void setSite_worklocation(String site_worklocation) {
		this.site_worklocation = site_worklocation;
	}
	public String getSite_zipcode() {
		return site_zipcode;
	}
	public void setSite_zipcode(String site_zipcode) {
		this.site_zipcode = site_zipcode;
	}
	public String getSp_instructions() {
		return sp_instructions;
	}
	public void setSp_instructions(String sp_instructions) {
		this.sp_instructions = sp_instructions;
	}
	public String getStand_by() {
		return stand_by;
	}
	public void setStand_by(String stand_by) {
		this.stand_by = stand_by;
	}
	public String getTc_id() {
		return tc_id;
	}
	public void setTc_id(String tc_id) {
		this.tc_id = tc_id;
	}
	public String getTicket_number() {
		return ticket_number;
	}
	public void setTicket_number(String ticket_number) {
		this.ticket_number = ticket_number;
	}
		
}	


