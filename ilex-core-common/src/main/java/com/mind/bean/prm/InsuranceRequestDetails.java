package com.mind.bean.prm;

import java.util.ArrayList;

import com.mind.bean.docm.LabelValue;

public class InsuranceRequestDetails {
	
	private String scheduleStart = null;
	private String scheduleEnd = null;
	private String siteNumber = null;
	private String address = null;
	private String city = null;
	private String state = null;
	private String zipCode = null;
	private String insuranceRequirements = null;
	
	private ArrayList<LabelValue> stateList = new ArrayList<LabelValue>();
	
	public ArrayList<LabelValue> getStateList() {
		return stateList;
	}
	public void setStateList(ArrayList<LabelValue> stateList) {
		this.stateList = stateList;
	}

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getInsuranceRequirements() {
		return insuranceRequirements;
	}
	public void setInsuranceRequirements(String insuranceRequirements) {
		this.insuranceRequirements = insuranceRequirements;
	}
	public String getScheduleEnd() {
		return scheduleEnd;
	}
	public void setScheduleEnd(String scheduleEnd) {
		this.scheduleEnd = scheduleEnd;
	}
	public String getScheduleStart() {
		return scheduleStart;
	}
	public void setScheduleStart(String scheduleStart) {
		this.scheduleStart = scheduleStart;
	}
	public String getSiteNumber() {
		return siteNumber;
	}
	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

}
