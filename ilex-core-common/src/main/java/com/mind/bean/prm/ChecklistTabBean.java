package com.mind.bean.prm;

import java.io.Serializable;


public class ChecklistTabBean implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String activityid = null;
	private String activityname = null;
	private String activitystatus = null;
	
	private String itemname = null;
	private String groupname = null;
	private String option = null;
	private String optiontype = null;
	private String optionid = null;
	private String optioncheckbox = null;
	//private String optioncheckboxid = null;
	
	//private String[] options = null;
	//private String[] optionsid = null;
	private String selectedoption = null;
	private String optionsperques = null;
	
	//Collection optionobject = null;
	private String label = null;
	private String percent = null;
	
	
	public String getPercent() {
		return percent;
	}
	public void setPercent(String percent) {
		this.percent = percent;
	}
	public String getOption() {
		return option;
	}
	public void setOption(String option) {
		this.option = option;
	}
	public String getOptiontype() {
		return optiontype;
	}
	public void setOptiontype(String optiontype) {
		this.optiontype = optiontype;
	}
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	public String getItemname() {
		return itemname;
	}
	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	public String getActivityid() {
		return activityid;
	}
	public void setActivityid(String activityid) {
		this.activityid = activityid;
	}
	public String getActivityname() {
		return activityname;
	}
	public void setActivityname(String activityname) {
		this.activityname = activityname;
	}
	public String getActivitystatus() {
		return activitystatus;
	}
	public void setActivitystatus(String activitystatus) {
		this.activitystatus = activitystatus;
	}
	public String getOptionid() {
		return optionid;
	}
	public void setOptionid(String optionid) {
		this.optionid = optionid;
	}
	public String getOptioncheckbox() {
		return optioncheckbox;
	}
	public void setOptioncheckbox(String optioncheckbox) {
		this.optioncheckbox = optioncheckbox;
	}
		
	public String getSelectedoption() {
		return selectedoption;
	}
	public void setSelectedoption(String selectedoption) {
		this.selectedoption = selectedoption;
	}
	
	public String getOptionsperques() {
		return optionsperques;
	}
	public void setOptionsperques(String optionsperques) {
		this.optionsperques = optionsperques;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	
}
