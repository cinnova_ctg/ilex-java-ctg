package com.mind.bean.prm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class DepotRequisitionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String jobid = null;
	private String jobname = null;
	private String appendixid = null;
	private String appendixname = null;
	private String msaid = null;
	private String nettype = null;
	private String lo_ot_id = null;
	private String ticket_type = null;
	
	private String ticket_id = null;
	private String ticket_number = null;
	private String requestor_name = null;
	private String requestor_email = null;
	private String company_name = null;
	private boolean refresh = false;
	private boolean refreshprodlist = false;
	private String site_id = null;
	private String site_name = null;
	private String site_number = null;
	private String site_address = null;
	private String site_city = null;
	private String site_state = null;
	private String site_zipcode = null;
	private String site_country = null;
	private String locality_uplift = null;
	private String union_site = null;
	private String site_worklocation = null;
	private String site_direction = null;
	private String primary_Name = null;
	private String secondary_Name = null;
	private String site_phone = null;
	private String secondary_phone = null;
	private String primary_email = null;
	private String secondary_email = null;
	private String site_notes = null;
	private String sitesearch = null;
	private String id1 = null;
	private String id2 = null;
	private String ref1 = null;
	private String ref = null;
	private String viewjobtype = null;
	private String ownerId = null;
	//01/03/2007
	private String sitelatdeg = null;
	private String sitelatmin = null;
	private String sitelatdirection = null;
	private String sitelondeg = null;
	private String sitelonmin = null;
	private String sitelondirection = null;
	private String sitelistid = null;
	
	
	private String requested_date = null;
	private String requested_datehh = null;
	private String requested_datemm = null;
	private String requested_dateop = null;
	
	private String standby = null;
	private String msp = null;
	private String helpdesk = null;
	private String category = null;
	private String category_name = null;
	private String resourcelevel = null;
	private String customer_references = null;
	private String problem_description = null;
	private String special_instruction = null;
	
	private String[] depot_requisition = null;
	
	
	private String addquantity = null;
	private ArrayList depoproductlist = null;
	private String depoproductlistsize = null;
		
	private String criticality = null;
	private String arrivaldate = null;
	private String arrivaldatehh = null;
	private String arrivaldatemm = null;
	private String arrivaldateop = null;
	
	private String arrivalwindowstartdate = null;
	private String arrivalwindowstartdatehh = null;
	private String arrivalwindowstartdatemm = null;
	private String arrivalwindowstartdateop = null;
	
	private String arrivalwindowenddate = null;
	private String arrivalwindowenddatehh = null;
	private String arrivalwindowenddatemm = null;
	private String arrivalwindowenddateop = null;
	private String crit_cat_id = null;
	private String crit_cat_name = null;
	
	private String invoice_Flag = null;
	private String invoiceno=null;
	private String from_date=null;
	private String to_date=null;
	private String powo_number=null;
	private String partner_name=null;
	
//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	
	
	
	
	
	private String dp_cat_id = null;
	private String dp_cat_name = null;
	
	private Collection prodList = null;  
	
	private String dp_prod_id = null;
	private String dp_prod_name = null;
	
	private String save = null;
	
	private String lm_ticket_type = null;
	
	private String authenticate = "";

	private String[] depot_quantity = null;
	private String[] de_product_id = null;
	
	private int dialog_number = 0;
	
	private String rules = null;
	
	
	private String sitestatus = null;
	private String sitecategory  = null;
	private String siteregion = null;
	private String sitegroup = null;
	private String siteendcustomer = null;
	private String sitetimezone = null;
	private Collection tempList = null;  
	private String siteCountryId = null;
	private String siteCountryIdName = null;
	private String msaname = null;
	private String siteLatLon  = null;
	private String refersh = null;
	// added  for chel help desk

	private String helpDeskCheck=null;
	
	
	public String getHelpDeskCheck() {
		return helpDeskCheck;
	}

	public void setHelpDeskCheck(String helpDeskCheck) {
		this.helpDeskCheck = helpDeskCheck;
	}

	public int getDialog_number() {
		return dialog_number;
	}

	public void setDialog_number(int dialog_number) {
		this.dialog_number = dialog_number;
	}

	public String[] getDe_product_id() {
		return de_product_id;
	}

	public String getDe_product_id( int i )	{	
		return de_product_id[i];
	}
	
	public void setDe_product_id(String[] de_product_id) {
		this.de_product_id = de_product_id;
	}

	public String[] getDepot_requisition() {
		return depot_requisition;
	}

	public String getDepot_requisition( int i )	{	
		return depot_requisition[i];
	}
	
	public void setDepot_requisition( String[] depot_requisition ) {
		this.depot_requisition = depot_requisition;
	}
	
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	public String getCriticality() {
		return criticality;
	}
	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}
	public String getCustomer_references() {
		return customer_references;
	}
	public void setCustomer_references(String customer_references) {
		this.customer_references = customer_references;
	}

	
	public String getLocality_uplift() {
		return locality_uplift;
	}
	public void setLocality_uplift(String locality_uplift) {
		this.locality_uplift = locality_uplift;
	}
	public String getMsp() {
		return msp;
	}
	public void setMsp(String msp) {
		this.msp = msp;
	}
	public String getPrimary_email() {
		return primary_email;
	}
	public void setPrimary_email(String primary_email) {
		this.primary_email = primary_email;
	}
	
	public String getProblem_description() {
		return problem_description;
	}
	public void setProblem_description(String problem_description) {
		this.problem_description = problem_description;
	}
	public String getRequested_date() {
		return requested_date;
	}
	public void setRequested_date(String requested_date) {
		this.requested_date = requested_date;
	}
	public String getRequestor_email() {
		return requestor_email;
	}
	public void setRequestor_email(String requestor_email) {
		this.requestor_email = requestor_email;
	}
	public String getRequestor_name() {
		return requestor_name;
	}
	public void setRequestor_name(String requestor_name) {
		this.requestor_name = requestor_name;
	}
	public String getSecondary_email() {
		return secondary_email;
	}
	public void setSecondary_email(String secondary_email) {
		this.secondary_email = secondary_email;
	}
	
	public String getSecondary_phone() {
		return secondary_phone;
	}
	public void setSecondary_phone(String secondary_phone) {
		this.secondary_phone = secondary_phone;
	}
	public String getSite_address() {
		return site_address;
	}
	public void setSite_address(String site_address) {
		this.site_address = site_address;
	}
	public String getSite_city() {
		return site_city;
	}
	public void setSite_city(String site_city) {
		this.site_city = site_city;
	}
	public String getSite_country() {
		return site_country;
	}
	public void setSite_country(String site_country) {
		this.site_country = site_country;
	}
	public String getSite_direction() {
		return site_direction;
	}
	public void setSite_direction(String site_direction) {
		this.site_direction = site_direction;
	}
	public String getSite_name() {
		return site_name;
	}
	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}
	public String getSite_number() {
		return site_number;
	}
	public void setSite_number(String site_number) {
		this.site_number = site_number;
	}
	public String getSite_state() {
		return site_state;
	}
	public void setSite_state(String site_state) {
		this.site_state = site_state;
	}
	public String getSite_worklocation() {
		return site_worklocation;
	}
	public void setSite_worklocation(String site_worklocation) {
		this.site_worklocation = site_worklocation;
	}
	public String getSite_zipcode() {
		return site_zipcode;
	}
	public void setSite_zipcode(String site_zipcode) {
		this.site_zipcode = site_zipcode;
	}
	public String getSpecial_instruction() {
		return special_instruction;
	}
	public void setSpecial_instruction(String special_instruction) {
		this.special_instruction = special_instruction;
	}
	public String getStandby() {
		return standby;
	}
	public void setStandby(String standby) {
		this.standby = standby;
	}
	public String getTicket_number() {
		return ticket_number;
	}
	public void setTicket_number(String ticket_number) {
		this.ticket_number = ticket_number;
	}
	public String getUnion_site() {
		return union_site;
	}
	public void setUnion_site(String union_site) {
		this.union_site = union_site;
	}
	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	public String getMsaid() {
		return msaid;
	}
	public void setMsaid(String msaid) {
		this.msaid = msaid;
	}
	public String getArrivaldate() {
		return arrivaldate;
	}
	public void setArrivaldate(String arrivaldate) {
		this.arrivaldate = arrivaldate;
	}
	public String getArrivaldatehh() {
		return arrivaldatehh;
	}
	public void setArrivaldatehh(String arrivaldatehh) {
		this.arrivaldatehh = arrivaldatehh;
	}
	public String getArrivaldatemm() {
		return arrivaldatemm;
	}
	public void setArrivaldatemm(String arrivaldatemm) {
		this.arrivaldatemm = arrivaldatemm;
	}
	public String getArrivaldateop() {
		return arrivaldateop;
	}
	public void setArrivaldateop(String arrivaldateop) {
		this.arrivaldateop = arrivaldateop;
	}
	public String getArrivalwindowenddate() {
		return arrivalwindowenddate;
	}
	public void setArrivalwindowenddate(String arrivalwindowenddate) {
		this.arrivalwindowenddate = arrivalwindowenddate;
	}
	public String getArrivalwindowenddatehh() {
		return arrivalwindowenddatehh;
	}
	public void setArrivalwindowenddatehh(String arrivalwindowenddatehh) {
		this.arrivalwindowenddatehh = arrivalwindowenddatehh;
	}
	public String getArrivalwindowenddatemm() {
		return arrivalwindowenddatemm;
	}
	public void setArrivalwindowenddatemm(String arrivalwindowenddatemm) {
		this.arrivalwindowenddatemm = arrivalwindowenddatemm;
	}
	public String getArrivalwindowenddateop() {
		return arrivalwindowenddateop;
	}
	public void setArrivalwindowenddateop(String arrivalwindowenddateop) {
		this.arrivalwindowenddateop = arrivalwindowenddateop;
	}
	public String getArrivalwindowstartdate() {
		return arrivalwindowstartdate;
	}
	public void setArrivalwindowstartdate(String arrivalwindowstartdate) {
		this.arrivalwindowstartdate = arrivalwindowstartdate;
	}
	public String getArrivalwindowstartdatehh() {
		return arrivalwindowstartdatehh;
	}
	public void setArrivalwindowstartdatehh(String arrivalwindowstartdatehh) {
		this.arrivalwindowstartdatehh = arrivalwindowstartdatehh;
	}
	public String getArrivalwindowstartdatemm() {
		return arrivalwindowstartdatemm;
	}
	public void setArrivalwindowstartdatemm(String arrivalwindowstartdatemm) {
		this.arrivalwindowstartdatemm = arrivalwindowstartdatemm;
	}
	public String getArrivalwindowstartdateop() {
		return arrivalwindowstartdateop;
	}
	public void setArrivalwindowstartdateop(String arrivalwindowstartdateop) {
		this.arrivalwindowstartdateop = arrivalwindowstartdateop;
	}
	public String getRequested_datehh() {
		return requested_datehh;
	}
	public void setRequested_datehh(String requested_datehh) {
		this.requested_datehh = requested_datehh;
	}
	public String getRequested_datemm() {
		return requested_datemm;
	}
	public void setRequested_datemm(String requested_datemm) {
		this.requested_datemm = requested_datemm;
	}
	public String getRequested_dateop() {
		return requested_dateop;
	}
	public void setRequested_dateop(String requested_dateop) {
		this.requested_dateop = requested_dateop;
	}
	public String getSitesearch() {
		return sitesearch;
	}
	public void setSitesearch(String sitesearch) {
		this.sitesearch = sitesearch;
	}
	public String getSite_phone() {
		return site_phone;
	}
	public void setSite_phone(String site_phone) {
		this.site_phone = site_phone;
	}
	public String getPrimary_Name() {
		return primary_Name;
	}
	public void setPrimary_Name(String primary_Name) {
		this.primary_Name = primary_Name;
	}
	public String getSecondary_Name() {
		return secondary_Name;
	}
	public void setSecondary_Name(String secondary_Name) {
		this.secondary_Name = secondary_Name;
	}
	public String getId1() {
		return id1;
	}
	public void setId1(String id1) {
		this.id1 = id1;
	}
	public String getId2() {
		return id2;
	}
	public void setId2(String id2) {
		this.id2 = id2;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getRef1() {
		return ref1;
	}
	public void setRef1(String ref1) {
		this.ref1 = ref1;
	}
	public String getSite_notes() {
		return site_notes;
	}
	public void setSite_notes(String site_notes) {
		this.site_notes = site_notes;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getSite_id() {
		return site_id;
	}
	public void setSite_id(String site_id) {
		this.site_id = site_id;
	}
	public String getTicket_id() {
		return ticket_id;
	}
	public void setTicket_id(String ticket_id) {
		this.ticket_id = ticket_id;
	}
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getResourcelevel() {
		return resourcelevel;
	}
	public void setResourcelevel(String resourcelevel) {
		this.resourcelevel = resourcelevel;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public boolean getRefresh() {
		return refresh;
	}
	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}
	public String getNettype() {
		return nettype;
	}
	public void setNettype(String nettype) {
		this.nettype = nettype;
	}
	public String getCrit_cat_id() {
		return crit_cat_id;
	}
	public void setCrit_cat_id(String crit_cat_id) {
		this.crit_cat_id = crit_cat_id;
	}
	public String getCrit_cat_name() {
		return crit_cat_name;
	}
	public void setCrit_cat_name(String crit_cat_name) {
		this.crit_cat_name = crit_cat_name;
	}
	public String getDp_cat_id() {
		return dp_cat_id;
	}
	public void setDp_cat_id(String dp_cat_id) {
		this.dp_cat_id = dp_cat_id;
	}
	public String getDp_cat_name() {
		return dp_cat_name;
	}
	public void setDp_cat_name(String dp_cat_name) {
		this.dp_cat_name = dp_cat_name;
	}
	public String getDp_prod_id() {
		return dp_prod_id;
	}
	public void setDp_prod_id(String dp_prod_id) {
		this.dp_prod_id = dp_prod_id;
	}
	public String getDp_prod_name() {
		return dp_prod_name;
	}
	public void setDp_prod_name(String dp_prod_name) {
		this.dp_prod_name = dp_prod_name;
	}
	public Collection getProdList() {
		return prodList;
	}
	public void setProdList(Collection prodList) {
		this.prodList = prodList;
	}
	
	public String getAddquantity() {
		return addquantity;
	}

	public void setAddquantity(String addquantity) {
		this.addquantity = addquantity;
	}

	public String[] getDepot_quantity() {
		return depot_quantity;
	}
	
	public String getDepot_quantity( int i )	{	
		return depot_quantity[i];
	}

	public void setDepot_quantity(String[] depot_quantity) {
		this.depot_quantity = depot_quantity;
	}
	
	public String getLo_ot_id() {
		return lo_ot_id;
	}

	public void setLo_ot_id(String lo_ot_id) {
		this.lo_ot_id = lo_ot_id;
	}

	public ArrayList getDepoproductlist() {
		return depoproductlist;
	}

	public void setDepoproductlist(ArrayList depoproductlist) {
		this.depoproductlist = depoproductlist;
	}

	public String getDepoproductlistsize() {
		return depoproductlistsize;
	}

	public void setDepoproductlistsize(String depoproductlistsize) {
		this.depoproductlistsize = depoproductlistsize;
	}

	public String getTicket_type() {
		return ticket_type;
	}

	public void setTicket_type(String ticket_type) {
		this.ticket_type = ticket_type;
	}

	public boolean getRefreshprodlist() {
		return refreshprodlist;
	}

	public void setRefreshprodlist(boolean refreshprodlist) {
		this.refreshprodlist = refreshprodlist;
	}

	public String getRules() {
		return rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}

	public String getHelpdesk() {
		return helpdesk;
	}

	public void setHelpdesk(String helpdesk) {
		this.helpdesk = helpdesk;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getLm_ticket_type() {
		return lm_ticket_type;
	}

	public void setLm_ticket_type(String lm_ticket_type) {
		this.lm_ticket_type = lm_ticket_type;
	}

	public String getFrom_date() {
		return from_date;
	}

	public void setFrom_date(String from_date) {
		this.from_date = from_date;
	}

	public String getInvoice_Flag() {
		return invoice_Flag;
	}

	public void setInvoice_Flag(String invoice_Flag) {
		this.invoice_Flag = invoice_Flag;
	}

	public String getInvoiceno() {
		return invoiceno;
	}

	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public String getPowo_number() {
		return powo_number;
	}

	public void setPowo_number(String powo_number) {
		this.powo_number = powo_number;
	}

	public String getTo_date() {
		return to_date;
	}

	public void setTo_date(String to_date) {
		this.to_date = to_date;
	}

	public String getSitelatdeg() {
		return sitelatdeg;
	}

	public void setSitelatdeg(String sitelatdeg) {
		this.sitelatdeg = sitelatdeg;
	}

	public String getSitelatdirection() {
		return sitelatdirection;
	}

	public void setSitelatdirection(String sitelatdirection) {
		this.sitelatdirection = sitelatdirection;
	}

	public String getSitelatmin() {
		return sitelatmin;
	}

	public void setSitelatmin(String sitelatmin) {
		this.sitelatmin = sitelatmin;
	}

	public String getSitelondeg() {
		return sitelondeg;
	}

	public void setSitelondeg(String sitelondeg) {
		this.sitelondeg = sitelondeg;
	}

	public String getSitelondirection() {
		return sitelondirection;
	}

	public void setSitelondirection(String sitelondirection) {
		this.sitelondirection = sitelondirection;
	}

	public String getSitelonmin() {
		return sitelonmin;
	}

	public void setSitelonmin(String sitelonmin) {
		this.sitelonmin = sitelonmin;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getRefersh() {
		return refersh;
	}

	public void setRefersh(String refersh) {
		this.refersh = refersh;
	}

	public String getSitecategory() {
		return sitecategory;
	}

	public void setSitecategory(String sitecategory) {
		this.sitecategory = sitecategory;
	}

	public String getSiteCountryId() {
		return siteCountryId;
	}

	public void setSiteCountryId(String siteCountryId) {
		this.siteCountryId = siteCountryId;
	}

	public String getSiteCountryIdName() {
		return siteCountryIdName;
	}

	public void setSiteCountryIdName(String siteCountryIdName) {
		this.siteCountryIdName = siteCountryIdName;
	}

	public String getSiteendcustomer() {
		return siteendcustomer;
	}

	public void setSiteendcustomer(String siteendcustomer) {
		this.siteendcustomer = siteendcustomer;
	}

	public String getSitegroup() {
		return sitegroup;
	}

	public void setSitegroup(String sitegroup) {
		this.sitegroup = sitegroup;
	}

	public String getSiteLatLon() {
		return siteLatLon;
	}

	public void setSiteLatLon(String siteLatLon) {
		this.siteLatLon = siteLatLon;
	}

	public String getSiteregion() {
		return siteregion;
	}

	public void setSiteregion(String siteregion) {
		this.siteregion = siteregion;
	}

	public String getSitestatus() {
		return sitestatus;
	}

	public void setSitestatus(String sitestatus) {
		this.sitestatus = sitestatus;
	}

	public String getSitetimezone() {
		return sitetimezone;
	}

	public void setSitetimezone(String sitetimezone) {
		this.sitetimezone = sitetimezone;
	}

	public Collection getTempList() {
		return tempList;
	}

	public void setTempList(Collection tempList) {
		this.tempList = tempList;
	}

	public String getSitelistid() {
		return sitelistid;
	}

	public void setSitelistid(String sitelistid) {
		this.sitelistid = sitelistid;
	}

	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}

	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}

	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
	
}



