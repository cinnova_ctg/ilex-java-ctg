package com.mind.bean.prm;

public class InsuranceRequestAttention {

	private String attention = null;
	private String attentionEmailTo = null;
	private String attentionDirect = null;
	private String attentionFax = null;
	private String attentionAdditionalCC = null;
	
	public String getAttention() {
		return attention;
	}
	public void setAttention(String attention) {
		this.attention = attention;
	}
	public String getAttentionAdditionalCC() {
		return attentionAdditionalCC;
	}
	public void setAttentionAdditionalCC(String attentionAdditionalCC) {
		this.attentionAdditionalCC = attentionAdditionalCC;
	}
	public String getAttentionDirect() {
		return attentionDirect;
	}
	public void setAttentionDirect(String attentionDirect) {
		this.attentionDirect = attentionDirect;
	}
	public String getAttentionEmailTo() {
		return attentionEmailTo;
	}
	public void setAttentionEmailTo(String attentionEmailTo) {
		this.attentionEmailTo = attentionEmailTo;
	}
	public String getAttentionFax() {
		return attentionFax;
	}
	public void setAttentionFax(String attentionFax) {
		this.attentionFax = attentionFax;
	}
	
	
}
