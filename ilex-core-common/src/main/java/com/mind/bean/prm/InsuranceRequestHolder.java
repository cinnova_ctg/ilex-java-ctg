package com.mind.bean.prm;

import java.util.ArrayList;

import com.mind.bean.docm.LabelValue;

public class InsuranceRequestHolder {
	
	private String holderName = null;
	private String holderAddress = null;
	private String holderCity = null;
	private String holderState = null;
	private String holderZipCode = null;
	private ArrayList<LabelValue> holderStateList = new ArrayList<LabelValue>();
	
	public ArrayList<LabelValue> getHolderStateList() {
		return holderStateList;
	}
	public void setHolderStateList(ArrayList<LabelValue> holderStateList) {
		this.holderStateList = holderStateList;
	}
	public String getHolderAddress() {
		return holderAddress;
	}
	public void setHolderAddress(String holderAddress) {
		this.holderAddress = holderAddress;
	}
	public String getHolderCity() {
		return holderCity;
	}
	public void setHolderCity(String holderCity) {
		this.holderCity = holderCity;
	}
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public String getHolderState() {
		return holderState;
	}
	public void setHolderState(String holderState) {
		this.holderState = holderState;
	}
	public String getHolderZipCode() {
		return holderZipCode;
	}
	public void setHolderZipCode(String holderZipCode) {
		this.holderZipCode = holderZipCode;
	}
	

}
