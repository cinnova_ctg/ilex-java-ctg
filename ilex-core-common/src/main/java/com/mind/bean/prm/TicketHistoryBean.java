package com.mind.bean.prm;

import java.io.Serializable;

public class TicketHistoryBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String ticketid = null;
	private String ticketrequestorname = null;
	private String ticketnum = null;
	private String status = null;
	
	private String ownerId = null;
	private String viewjobtype = null;
	private String nettype = null;
	private String appendixid = null;
	
	private String jobid = null;
	private String jobname = null;
	
	private String from = null;
	private String ticketrequesteddate = null;
	private String ticketcustreference = null;
	private String problemdesc = null;
	private String criticality = null;
	
	private String final_price = null;
	private String completed_date = null;
	
	public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTicketid() {
		return ticketid;
	}
	public void setTicketid(String ticketid) {
		this.ticketid = ticketid;
	}
	public String getTicketrequestorname() {
		return ticketrequestorname;
	}
	public void setTicketrequestorname(String ticketrequestorname) {
		this.ticketrequestorname = ticketrequestorname;
	}
	
	
	public String getCriticality() {
		return criticality;
	}
	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}
	public String getProblemdesc() {
		return problemdesc;
	}
	public void setProblemdesc(String problemdesc) {
		this.problemdesc = problemdesc;
	}
	public String getTicketcustreference() {
		return ticketcustreference;
	}
	public void setTicketcustreference(String ticketcustreference) {
		this.ticketcustreference = ticketcustreference;
	}
	public String getTicketnum() {
		return ticketnum;
	}
	public void setTicketnum(String ticketnum) {
		this.ticketnum = ticketnum;
	}
	public String getTicketrequesteddate() {
		return ticketrequesteddate;
	}
	public void setTicketrequesteddate(String ticketrequesteddate) {
		this.ticketrequesteddate = ticketrequesteddate;
	}
	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	public String getCompleted_date() {
		return completed_date;
	}
	public void setCompleted_date(String completed_date) {
		this.completed_date = completed_date;
	}
	public String getFinal_price() {
		return final_price;
	}
	public void setFinal_price(String final_price) {
		this.final_price = final_price;
	}
	public String getNettype() {
		return nettype;
	}
	public void setNettype(String nettype) {
		this.nettype = nettype;
	}
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	
	
}



