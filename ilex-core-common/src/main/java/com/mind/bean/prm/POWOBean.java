package com.mind.bean.prm;

import java.io.Serializable;
import java.util.ArrayList;

public class POWOBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
		private String jobid = null;
		private String jobname = null;
		private String appendixid = null;
		private String appendixname = null;
		private String appendixType = null;
		private String powoid = null;
		private String partnerid = null;
		private String srccompanyname = null;
		private String powono = null;
		
		private String[] pvs_supplied = null;
		private String[] resource_temp_pvs_supplied = null;
		
		private String viewjobtype = null;
		private String ownerId = null;
		private String nettype = null;
		private String type = null;
		private ArrayList typelist = null;
		private ArrayList partnerpoclist = null;
		private String partnerpoc = null;
			
		private String bulk = null;
		private String authorizedtotal = null;
		private String issuedate = null;
		private String deliverbydate = null;
		private String deliverbydatehh = null;
		private String deliverbydatemm = null;
		private String deliverbydateop = null;
		
		private String terms = null;
		private ArrayList termslist = null;
		
		private String masterpoitem = null;
		private String speccondition = null;
		private String reconid = null;
		private String invoicetotal = null;
		private String approvedtotal = null;
		private String invoicerecddate = null;
		private String invoiceno = null;
		private String invoicecomments = null;
		
		// element for resource details
		private String cnspartnum = null;
		private String resourcename = null;
		private String resourceqty = null;
		private String resourcetype = null;
		private String resourcemfgpart = null;
		private String resourceathounitcost = null;
		private String resourceathototalcost = null;
		private String resourceshowon = null;
		private String resourcedropshift = null;
		
		//added by Seema
		private String resourcepvssupplied = null;
		private String resourcecreditcard = null;
		
		private String deliverable = null;
		private ArrayList deliverableList = null;
		
		// for PO/WO detail
		private String shiptoaddress = null;
		private String speccond = null;
		private String tempspeccond = null;
		
		private String sow = null;
		private String assumption = null;
		
		private String activity = null;
		private String estimatedunitcost = null;
		private String estimatedtotalcost = null;
		
		private ArrayList resourcelist = null;
		
		private ArrayList masterpritemlist = null;
		private String problemdesc = null;
		
		//element for tabular
		private String[] act_name = null;
		
		private String[] resource_id = null;
		private String[] resource_name = null;
		private String[] resource_qty = null;
		private String[] resource_unitcost = null;
		private String[] resource_totalcost = null;
		private String[] resource_authorizedunitcost = null;
		private String[] resource_authorizedtotalcost = null;
		private String[] resource_showonwo = null;
		private String[] resource_tempshowonwo = null;
		
		private String[] resource_dropshift = null;
		private String[] resource_tempdropshift = null;
		
		private String[] resource_tempid = null;
		private String[] resource_act_qty = null;
		
		private String[] resource_subtotalcost = null;
		private String[] resource_authorizedqty = null;
		
		private String partnername = null;
		
		private String next = null;
		private String save = null;
		private String firsttime = null;
		private String specialinstruction = null;
		
		private String resetsow = null;
		private String resetassumption = null;
		private String resetSPCond = null;
		private String resetSPInst = null;
		private String refflag = null;
		private String defaultAssumption = null;
		private String defaultSow = null;
		
//		 for back invoice manager
		private String inv_type = null;
		private String inv_id = null;
		private String inv_rdofilter = null;
		private String invoice_Flag = null;
		private String invoiceNo=null;
		private String from_date=null;
		private String to_date=null;
		private String powo_number=null;
		private String partner_name=null;
		
		private String fixedestimatedcost = null;
		
		private String totalcost = null;
		
		private String[] act_qty = null;
		
		private String aut_qty = null;
		//added by Seema
		private String[] credit_card = null;
		private String[] resource_temp_credit_card = null;
		private String resource_type = null;
		private String fcost=null;
		private String resourceSubCat = null;
		//18/07/2007
		private String contractEstTotal = null;
		private String nonContractEstTotal = null;
		private String nonContractEstLabelTotal = null;
		private boolean  check = true; 
		private boolean changePoPayment = false;
		// added on 9/02/2008
		private String checkDefault="";
		private String partnerIsMinuteman =null;
		private String refbyonchange="false";
		
		private String poHistory = null;
		private String woHistory = null;
		
		public String getPoHistory() {
			return poHistory;
		}

		public void setPoHistory(String poHistory) {
			this.poHistory = poHistory;
		}

		public String getWoHistory() {
			return woHistory;
		}

		public void setWoHistory(String woHistory) {
			this.woHistory = woHistory;
		}

		public String getRefbyonchange() {
			return refbyonchange;
		}

		public void setRefbyonchange(String refbyonchange) {
			this.refbyonchange = refbyonchange;
		}

		public String getResourceSubCat() {
			return resourceSubCat;
		}

		public boolean isCheck() {
			return check;
		}

		public void setCheck(boolean check) {
			this.check = check;
		}

		public void setResourceSubCat(String resourceSubCat) {
			this.resourceSubCat = resourceSubCat;
		}

		public String getFcost() {
			return fcost;
		}

		public void setFcost(String fcost) {
			this.fcost = fcost;
		}

		public String getResource_type() {
			return resource_type;
		}

		public void setResource_type(String resource_type) {
			this.resource_type = resource_type;
		}

		//over
		public String getJobid() {
			return jobid;
		}

		public void setJobid(String jobid) {
			this.jobid = jobid;
		}

		public String getAppendixid() {
			return appendixid;
		}

		public void setAppendixid(String appendixid) {
			this.appendixid = appendixid;
		}

		public String getApprovedtotal() {
			return approvedtotal;
		}

		public void setApprovedtotal(String approvedtotal) {
			this.approvedtotal = approvedtotal;
		}

		public String getAuthorizedtotal() {
			return authorizedtotal;
		}

		public void setAuthorizedtotal(String authorizedtotal) {
			this.authorizedtotal = authorizedtotal;
		}

		public String getBulk() {
			return bulk;
		}

		public void setBulk(String bulk) {
			this.bulk = bulk;
		}

		public String getDeliverbydate() {
			return deliverbydate;
		}

		public void setDeliverbydate(String deliverbydate) {
			this.deliverbydate = deliverbydate;
		}

		public String getInvoiceno() {
			return invoiceno;
		}

		public void setInvoiceno(String invoiceno) {
			this.invoiceno = invoiceno;
		}

		public String getInvoicerecddate() {
			return invoicerecddate;
		}

		public void setInvoicerecddate(String invoicerecddate) {
			this.invoicerecddate = invoicerecddate;
		}

		public String getInvoicetotal() {
			return invoicetotal;
		}

		public void setInvoicetotal(String invoicetotal) {
			this.invoicetotal = invoicetotal;
		}

		public String getIssuedate() {
			return issuedate;
		}

		public void setIssuedate(String issuedate) {
			this.issuedate = issuedate;
		}

		public String getMasterpoitem() {
			return masterpoitem;
		}

		public void setMasterpoitem(String masterpoitem) {
			this.masterpoitem = masterpoitem;
		}

		public String getPowono() {
			return powono;
		}

		public void setPowono(String powono) {
			this.powono = powono;
		}

		public String getReconid() {
			return reconid;
		}

		public void setReconid(String reconid) {
			this.reconid = reconid;
		}

		public String getSpeccondition() {
			return speccondition;
		}

		public void setSpeccondition(String speccondition) {
			this.speccondition = speccondition;
		}

		public String getSrccompanyname() {
			return srccompanyname;
		}

		public void setSrccompanyname(String srccompanyname) {
			this.srccompanyname = srccompanyname;
		}

		public String getTerms() {
			return terms;
		}

		public void setTerms(String terms) {
			this.terms = terms;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getPowoid() {
			return powoid;
		}

		public void setPowoid(String powoid) {
			this.powoid = powoid;
		}

		public String getInvoicecomments() {
			return invoicecomments;
		}

		public void setInvoicecomments(String invoicecomments) {
			this.invoicecomments = invoicecomments;
		}

		public String getAppendixname() {
			return appendixname;
		}

		public void setAppendixname(String appendixname) {
			this.appendixname = appendixname;
		}

		public String getJobname() {
			return jobname;
		}

		public void setJobname(String jobname) {
			this.jobname = jobname;
		}

		public String getCnspartnum() {
			return cnspartnum;
		}

		public void setCnspartnum(String cnspartnum) {
			this.cnspartnum = cnspartnum;
		}

		public String getResourceathototalcost() {
			return resourceathototalcost;
		}

		public void setResourceathototalcost(String resourceathototalcost) {
			this.resourceathototalcost = resourceathototalcost;
		}

		public String getResourceathounitcost() {
			return resourceathounitcost;
		}

		public void setResourceathounitcost(String resourceathounitcost) {
			this.resourceathounitcost = resourceathounitcost;
		}

		public String getResourcemfgpart() {
			return resourcemfgpart;
		}

		public void setResourcemfgpart(String resourcemfgpart) {
			this.resourcemfgpart = resourcemfgpart;
		}

		public String getResourcename() {
			return resourcename;
		}

		public void setResourcename(String resourcename) {
			this.resourcename = resourcename;
		}

		public String getResourceqty() {
			return resourceqty;
		}

		public void setResourceqty(String resourceqty) {
			this.resourceqty = resourceqty;
		}

		public String getResourceshowon() {
			return resourceshowon;
		}

		public void setResourceshowon(String resourceshowon) {
			this.resourceshowon = resourceshowon;
		}

		public String getResourcetype() {
			return resourcetype;
		}

		public void setResourcetype(String resourcetype) {
			this.resourcetype = resourcetype;
		}

		public String getActivity() {
			return activity;
		}

		public void setActivity(String activity) {
			this.activity = activity;
		}

		public String getAssumption() {
			return assumption;
		}

		public void setAssumption(String assumption) {
			this.assumption = assumption;
		}

		public String getEstimatedtotalcost() {
			return estimatedtotalcost;
		}

		public void setEstimatedtotalcost(String estimatedtotalcost) {
			this.estimatedtotalcost = estimatedtotalcost;
		}

		public String getEstimatedunitcost() {
			return estimatedunitcost;
		}

		public void setEstimatedunitcost(String estimatedunitcost) {
			this.estimatedunitcost = estimatedunitcost;
		}

		public String getShiptoaddress() {
			return shiptoaddress;
		}

		public void setShiptoaddress(String shiptoaddress) {
			this.shiptoaddress = shiptoaddress;
		}

		public String getSow() {
			return sow;
		}

		public void setSow(String sow) {
			this.sow = sow;
		}

		public String getSpeccond() {
			return speccond;
		}

		public void setSpeccond(String speccond) {
			this.speccond = speccond;
		}

		public ArrayList getResourcelist() {
			return resourcelist;
		}

		public void setResourcelist(ArrayList resourcelist) {
			this.resourcelist = resourcelist;
		}
		
		
		public ArrayList getMasterpritemlist() 
		{
			return masterpritemlist;
		}

		public void setMasterpritemlist( ArrayList masterpritemlist ) {
			this.masterpritemlist = masterpritemlist;
		}

		public ArrayList getTypelist() {
			
			return typelist;
		}

		public void setTypelist( ArrayList typelist ) {
			this.typelist = typelist;
		}

		
		public ArrayList getTermslist() {
			return termslist;
		}

		public void setTermslist( ArrayList termslist ) {
			this.termslist = termslist;
		}

		public String getPartnerid() {
			return partnerid;
		}

		public void setPartnerid(String partnerid) {
			this.partnerid = partnerid;
		}

		
		
		
		
		
		public String[] getAct_name() {
			return act_name;
		}

		public void setAct_name( String[] act_name ) {
			this.act_name = act_name;
		}
		public String getAct_name( int i ) {
			return act_name[i];
		}

		public String[] getResource_authorizedtotalcost() {
			return resource_authorizedtotalcost;
		}

		public void setResource_authorizedtotalcost(
				String[] resource_authorizedtotalcost) {
			this.resource_authorizedtotalcost = resource_authorizedtotalcost;
		}
		public String getResource_authorizedtotalcost( int i ) {
			return resource_authorizedtotalcost[i];
		}

		
		public String[] getResource_authorizedunitcost() {
			return resource_authorizedunitcost;
		}

		public void setResource_authorizedunitcost(String[] resource_authorizedunitcost) {
			
			//this.resource_authorizedunitcost = new String[resource_authorizedunitcost.length];
			//for(int i=0; i<resource_authorizedunitcost.length; i++) {
				this.resource_authorizedunitcost = resource_authorizedunitcost; 
			//} 
		}
		
		public String getResource_authorizedunitcost( int i ) {
			return resource_authorizedunitcost[i];
		}
		
		
		

		public String[] getResource_name() {
			return resource_name;
		}

		public void setResource_name(String[] resource_name) {
			this.resource_name = resource_name;
		}
		public String getResource_name( int i ) {
			return resource_name[i];
		}

		
		public String[] getResource_qty() {
			return resource_qty;
		}

		public void setResource_qty(String[] resource_qty) {
			this.resource_qty = resource_qty;
		}
		public String getResource_qty( int i ) {
			return resource_qty[i];
		}
		
		

		public String[] getResource_showonwo() {
			return resource_showonwo;
		}

		public void setResource_showonwo(String[] resource_showonwo) {
			this.resource_showonwo = resource_showonwo;
		}
		public String getResource_showonwo( int i ) {
			return resource_showonwo[i];
		}
		
		

		public String[] getResource_totalcost() {
			return resource_totalcost;
		}

		public void setResource_totalcost(String[] resource_totalcost) {
			this.resource_totalcost = resource_totalcost;
		}
		public String getResource_totalcost( int i ) {
			return resource_totalcost[i];
		}
		
		

		public String[] getResource_unitcost() {
			return resource_unitcost;
		}

		public void setResource_unitcost(String[] resource_unitcost) {
			this.resource_unitcost = resource_unitcost;
		}
		public String getResource_unitcost( int i ) {
			return resource_unitcost[i];
		}

		public String[] getResource_id() {
			return resource_id;
		}

		public void setResource_id( String[] resource_id ) {
			this.resource_id = resource_id;
		}
		public String getResource_id( int i ) {
			return resource_id[i];
		}

		public String getFirsttime() {
			return firsttime;
		}

		public void setFirsttime(String firsttime) {
			this.firsttime = firsttime;
		}

		public String getNext() {
			return next;
		}

		public void setNext(String next) {
			this.next = next;
		}

		public String getSave() {
			return save;
		}

		public void setSave( String save) {
			this.save = save;
		}

		
		public String getRefflag() {
			return refflag;
		}

		public void setRefflag( String refflag ) {
			this.refflag = refflag;
		}

		public String getSpecialinstruction() {
			return specialinstruction;
		}

		public void setSpecialinstruction(String specialinstruction) {
			this.specialinstruction = specialinstruction;
		}

		public String[] getResource_tempid() {
			return resource_tempid;
		}

		public void setResource_tempid(String[] resource_tempid) {
			this.resource_tempid = resource_tempid;
		}
		public String getResource_tempid( int i ) {
			return resource_tempid[i];
		}

		public String[] getResource_tempshowonwo() {
			return resource_tempshowonwo;
		}

		public void setResource_tempshowonwo(String[] resource_tempshowonwo) {
			this.resource_tempshowonwo = resource_tempshowonwo;
		}
		public String getResource_tempshowonwo( int i ) {
			return resource_tempshowonwo[i];
		}
		
		
		
		
		public String getPartnername() {
			return partnername;
		}

		public void setPartnername(String partnername) {
			this.partnername = partnername;
		}

		
		public String getResetassumption() {
			return resetassumption;
		}

		public void setResetassumption(String resetassumption) {
			this.resetassumption = resetassumption;
		}

		public String getResetsow() {
			return resetsow;
		}

		public void setResetsow(String resetsow) {
			this.resetsow = resetsow;
		}

		public ArrayList getPartnerpoclist() {
			return partnerpoclist;
		}

		public void setPartnerpoclist(ArrayList partnerpoclist) {
			this.partnerpoclist = partnerpoclist;
		}

		public String getPartnerpoc() {
			return partnerpoc;
		}

		public void setPartnerpoc(String partnerpoc) {
			this.partnerpoc = partnerpoc;
		}

		public String getInv_id() {
			return inv_id;
		}

		public void setInv_id(String inv_id) {
			this.inv_id = inv_id;
		}

		public String getInv_rdofilter() {
			return inv_rdofilter;
		}

		public void setInv_rdofilter(String inv_rdofilter) {
			this.inv_rdofilter = inv_rdofilter;
		}

		public String getInv_type() {
			return inv_type;
		}

		public void setInv_type(String inv_type) {
			this.inv_type = inv_type;
		}

		
		
		public String[] getResource_tempdropshift() {
			return resource_tempdropshift;
		}

		public void setResource_tempdropshift(String[] resource_tempdropshift) {
			this.resource_tempdropshift = resource_tempdropshift;
		}
		
		public String getResource_tempdropshift( int i ) {
			return resource_tempdropshift[i];
		}
		
		

		public String getResourcedropshift() {
			return resourcedropshift;
		}

		public void setResourcedropshift(String resourcedropshift) {
			this.resourcedropshift = resourcedropshift;
		}

		public String[] getResource_dropshift() {
			return resource_dropshift;
		}

		public void setResource_dropshift(String[] resource_dropshift) {
			this.resource_dropshift = resource_dropshift;
		}
		
		public String getResource_dropshift( int i ) {
			return resource_dropshift[i];
		}

		public String getNettype() {
			return nettype;
		}

		public void setNettype(String nettype) {
			this.nettype = nettype;
		}

		public String getViewjobtype() {
			return viewjobtype;
		}

		public void setViewjobtype(String viewjobtype) {
			this.viewjobtype = viewjobtype;
		}

		public String getDeliverbydatehh() {
			return deliverbydatehh;
		}

		public void setDeliverbydatehh(String deliverbydatehh) {
			this.deliverbydatehh = deliverbydatehh;
		}

		public String getDeliverbydatemm() {
			return deliverbydatemm;
		}

		public void setDeliverbydatemm(String deliverbydatemm) {
			this.deliverbydatemm = deliverbydatemm;
		}

		public String getDeliverbydateop() {
			return deliverbydateop;
		}

		public void setDeliverbydateop(String deliverbydateop) {
			this.deliverbydateop = deliverbydateop;
		}

		public String getProblemdesc() {
			return problemdesc;
		}

		public void setProblemdesc(String problemdesc) {
			this.problemdesc = problemdesc;
		}

		public String getTempspeccond() {
			return tempspeccond;
		}

		public void setTempspeccond(String tempspeccond) {
			this.tempspeccond = tempspeccond;
		}

		public String[] getResource_act_qty() {
			return resource_act_qty;
		}

		public void setResource_act_qty(String[] resource_act_qty) {
			this.resource_act_qty = resource_act_qty;
		}
		
		public String  getResource_act_qty( int i ) {
			return  resource_act_qty[i];
		}

		public String[] getPvs_supplied() {
			return pvs_supplied;
		}

		public void setPvs_supplied(String[] pvs_supplied) {
			this.pvs_supplied = pvs_supplied;
		}

		public String getPvs_supplied( int i ) {
			return pvs_supplied[i];
		}

		public String[] getResource_temp_pvs_supplied() {
			return resource_temp_pvs_supplied;
		}

		public void setResource_temp_pvs_supplied(String[] resource_temp_pvs_supplied) {
			this.resource_temp_pvs_supplied = resource_temp_pvs_supplied;
		}
		
		//Added
		public String getResource_temp_pvs_supplied( int i ) {
			return resource_tempshowonwo[i];
		}

		public String getOwnerId() {
			return ownerId;
		}

		public void setOwnerId(String ownerId) {
			this.ownerId = ownerId;
		}

		public String getFixedestimatedcost() {
			return fixedestimatedcost;
		}

		public void setFixedestimatedcost(String fixedestimatedcost) {
			this.fixedestimatedcost = fixedestimatedcost;
		}

		public String[] getAct_qty() {
			return act_qty;
		}

		public void setAct_qty(String[] act_qty) {
			this.act_qty = act_qty;
		}
		
		public String getAct_qty( int i ) {
			return act_qty[i];
		}

		public String[] getResource_subtotalcost() {
			return resource_subtotalcost;
		}

		public void setResource_subtotalcost(String[] resource_subtotalcost) {
			this.resource_subtotalcost = resource_subtotalcost;
		}
		
		public String getResource_subtotalcost( int i ) {
			return resource_subtotalcost[i];
		}

		public String[] getResource_authorizedqty() {
			return resource_authorizedqty;
		}

		public void setResource_authorizedqty(String[] resource_authorizedqty) {
			this.resource_authorizedqty = resource_authorizedqty;
		}
		
		public String getResource_authorizedqty( int i ) {
			return resource_authorizedqty[i];
		}

		public String getTotalcost() {
			return totalcost;
		}

		public void setTotalcost(String totalcost) {
			this.totalcost = totalcost;
		}

		public String getAut_qty() {
			return aut_qty;
		}

		public void setAut_qty(String aut_qty) {
			this.aut_qty = aut_qty;
		}

		//changed by Seema
		public String[] getCredit_card() {
			return credit_card;
		}

		public void setCredit_card(String[] credit_card) {
			this.credit_card = credit_card;
		}
		
			
		public String getCredit_card( int i) {
			return credit_card[i];
		}


		public String[] getResource_temp_credit_card() {
			return resource_temp_credit_card;
		}

		public void setResource_temp_credit_card(String[] resource_temp_credit_card) {
			this.resource_temp_credit_card = resource_temp_credit_card;
		}
		
		
		public String getResource_temp_credit_card( int i) {
			return resource_temp_credit_card[i];
		}

		public String getResourcecreditcard() {
			return resourcecreditcard;
		}

		public void setResourcecreditcard(String resourcecreditcard) {
			this.resourcecreditcard = resourcecreditcard;
		}

		public String getResourcepvssupplied() {
			return resourcepvssupplied;
		}

		public void setResourcepvssupplied(String resourcepvssupplied) {
			this.resourcepvssupplied = resourcepvssupplied;
		}

		public String getFrom_date() {
			return from_date;
		}

		public void setFrom_date(String from_date) {
			this.from_date = from_date;
		}

		public String getInvoice_Flag() {
			return invoice_Flag;
		}

		public void setInvoice_Flag(String invoice_Flag) {
			this.invoice_Flag = invoice_Flag;
		}

		public String getInvoiceNo() {
			return invoiceNo;
		}

		public void setInvoiceNo(String invoiceNo) {
			this.invoiceNo = invoiceNo;
		}

		public String getPartner_name() {
			return partner_name;
		}

		public void setPartner_name(String partner_name) {
			this.partner_name = partner_name;
		}

		public String getPowo_number() {
			return powo_number;
		}

		public void setPowo_number(String powo_number) {
			this.powo_number = powo_number;
		}

		public String getTo_date() {
			return to_date;
		}

		public void setTo_date(String to_date) {
			this.to_date = to_date;
		}

		public String getAppendixType() {
			return appendixType;
		}

		public void setAppendixType(String appendixType) {
			this.appendixType = appendixType;
		}

		public String getResetSPCond() {
			return resetSPCond;
		}

		public void setResetSPCond(String resetSPCond) {
			this.resetSPCond = resetSPCond;
		}

		public String getResetSPInst() {
			return resetSPInst;
		}

		public void setResetSPInst(String resetSPInst) {
			this.resetSPInst = resetSPInst;
		}

		public String getDefaultAssumption() {
			return defaultAssumption;
		}

		public void setDefaultAssumption(String defaultAssumption) {
			this.defaultAssumption = defaultAssumption;
		}

		public String getDefaultSow() {
			return defaultSow;
		}

		public void setDefaultSow(String defaultSow) {
			this.defaultSow = defaultSow;
		}

		public String getDeliverable() {
			return deliverable;
		}

		public void setDeliverable(String deliverable) {
			this.deliverable = deliverable;
		}

		public ArrayList getDeliverableList() {
			return deliverableList;
		}

		public void setDeliverableList(ArrayList deliverableList) {
			this.deliverableList = deliverableList;
		}

		public String getContractEstTotal() {
			return contractEstTotal;
		}

		public void setContractEstTotal(String contractEstTotal) {
			this.contractEstTotal = contractEstTotal;
		}

		public String getNonContractEstTotal() {
			return nonContractEstTotal;
		}

		public void setNonContractEstTotal(String nonContractEstTotal) {
			this.nonContractEstTotal = nonContractEstTotal;
		}

		public String getNonContractEstLabelTotal() {
			return nonContractEstLabelTotal;
		}

		public void setNonContractEstLabelTotal(String nonContractEstLabelTotal) {
			this.nonContractEstLabelTotal = nonContractEstLabelTotal;
		}

		public boolean isChangePoPayment() {
			return changePoPayment;
		}

		public void setChangePoPayment(boolean changePoPayment) {
			this.changePoPayment = changePoPayment;
		}
		public String getCheckDefault() {
			return checkDefault;
		}

		public void setCheckDefault(String checkDefault) {
			this.checkDefault = checkDefault;
		}

		public String getPartnerIsMinuteman() {
			return partnerIsMinuteman;
		}

		public void setPartnerIsMinuteman(String partnerIsMinuteman) {
			this.partnerIsMinuteman = partnerIsMinuteman;
		}
		
		
	}



