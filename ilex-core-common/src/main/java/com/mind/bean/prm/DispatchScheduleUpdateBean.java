package com.mind.bean.prm;

import java.io.Serializable;


public class DispatchScheduleUpdateBean implements Serializable {
	private String scheduleid = null;
	private String typeid = null;
	private String type = null;
	private String nettype = null;
	
	private String[] scheduleId = null;
	
	private String[] startdate = null;
	private String[] startdatehh = null;
	private String[] startdatemm = null;
	private String[] startdateop = null;
	
	private String[] scheduleEnddate = null;
	private String[] scheduleEndhh = null;
	private String[] scheduleEndmm = null;
	private String[] scheduleEndop = null;
	
	private String[] actstartdate = null;
	private String[] actstartdatehh = null;
	private String[] actstartdatemm = null;
	private String[] actstartdateop = null;
	
	private String[] actenddate = null;
	private String[] actenddatehh = null;
	private String[] actenddatemm = null;
	private String[] actenddateop = null;
		
	private String rownum = null;
	private String deletenum = null;
		
	private String deleteStatus = null;
	private String save = null;
	private String reset = null;
	private String function = null;
	private String updatemessage = null;
	private String authenticate = "";
	private String page = null;
	private String pageid = null;
	private String back = null;
	private String appendixname = null;
	private String jobname = null;
	
	private String viewjobtype = null;
	private String ownerId = null;
		
	private String checknetmedxtype = null;
	// Add for Insatallation Notes
	private String[] prevactarival=null;
	private String[] prevdeparture=null;
	private String[] scheduleNumber = null;
	
	public String[] getScheduleNumber() {
		return scheduleNumber;
	}
	public void setScheduleNumber(String[] scheduleNumber) {
		this.scheduleNumber = scheduleNumber;
	}
	
	public String getScheduleNumber(int i) {
		return scheduleNumber[i];
	}
	
	public void setScheduleNumber(String scheduleNumber, int i) {
		this.scheduleNumber[i] = scheduleNumber;
	}
	
	
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	
	public String getReset() {
		return reset;
	}
	public void setReset(String reset) {
		this.reset = reset;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getScheduleid() {
		return scheduleid;
	}
	public void setScheduleid(String scheduleid) {
		this.scheduleid = scheduleid;
	}
	public String getTypeid() {
		return typeid;
	}
	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getUpdatemessage() {
		return updatemessage;
	}
	public void setUpdatemessage(String updatemessage) {
		this.updatemessage = updatemessage;
	}
	
	
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getPageid() {
		return pageid;
	}
	public void setPageid(String pageid) {
		this.pageid = pageid;
	}
	public String getBack() {
		return back;
	}
	public void setBack(String back) {
		this.back = back;
	}
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	public String getNettype() {
		return nettype;
	}
	public void setNettype(String nettype) {
		this.nettype = nettype;
	}
	public String getChecknetmedxtype() {
		return checknetmedxtype;
	}
	public void setChecknetmedxtype(String checknetmedxtype) {
		this.checknetmedxtype = checknetmedxtype;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String[] getStartdate() {
		return startdate;
	}
	
	public String getStartdate(int i) {
		return startdate[i];
	}
	
	public void setStartdate(String[] startdate) {
		this.startdate = startdate;
	}
	
	public void setStartdate(String startdate, int i) {
		this.startdate[i] = startdate;
	}
	
	
	public String[] getStartdatehh() {
		return startdatehh;
	}
	public String getStartdatehh(int i) {
		return startdatehh[i];
	}
	public void setStartdatehh(String[] startdatehh) {
		this.startdatehh = startdatehh;
	}
	public void setStartdatehh(String startdatehh, int i) {
		this.startdatehh[i] = startdatehh;
	}
	public String[] getStartdatemm() {
		return startdatemm;
	}
	public String getStartdatemm(int i) {
		return startdatemm[i];
	}
	public void setStartdatemm(String[] startdatemm) {
		this.startdatemm = startdatemm;
	}
	public void setStartdatemm(String startdatemm, int i) {
		this.startdatemm[i] = startdatemm;
	}
	public String[] getStartdateop() {
		return startdateop;
	}
	public String getStartdateop(int i) {
		return startdateop[i];
	}
	public void setStartdateop(String[] startdateop) {
		this.startdateop = startdateop;
	}
	public void setStartdateop(String startdateop, int i) {
		this.startdateop[i] = startdateop;
	}
	
	
	public String[] getActstartdate() {
		return actstartdate;
	}
	
	public String getActstartdate(int i) {
		//System.out.println("donedone done done done ::"+actstartdate[i]+":::::::::"+i);
		return actstartdate[i];
	}
	
	public void setActstartdate(String[] actstartdate) {
		if(actstartdate!=null)
		this.actstartdate = actstartdate;
		else
		{
			//this.actstartdate = new String[1];
			this.actstartdate = actstartdate;
		
		}
	}
	
	public void setActstartdate(String actstartdate, int i) {
		
		if(actstartdate!=null && this.actstartdate != null)
		{
			this.actstartdate[i] = actstartdate;
		}
		
	}
	
	
	public String[] getActstartdatehh() {
		return actstartdatehh;
	}
	public String getActstartdatehh(int i) {
		return actstartdatehh[i];
	}
	public void setActstartdatehh(String[] actstartdatehh) {
		this.actstartdatehh = actstartdatehh;
	}
	public void setActstartdatehh(String actstartdatehh, int i) {
		this.actstartdatehh[i] = actstartdatehh;
	}
	public String[] getActstartdatemm() {
		return actstartdatemm;
	}
	public String getActstartdatemm(int i) {
		return actstartdatemm[i];
	}
	public void setActstartdatemm(String[] actstartdatemm) {
		this.actstartdatemm = actstartdatemm;
	}
	public void setActstartdatemm(String actstartdatemm, int i) {
		this.actstartdatemm[i] = actstartdatemm;
	}
	public String[] getActstartdateop() {
		return actstartdateop;
	}
	public String getActstartdateop(int i) {
		return actstartdateop[i];
	}
	public void setActstartdateop(String[] actstartdateop) {
		this.actstartdateop = actstartdateop;
	}
	public void setActstartdateop(String actstartdateop, int i) {
		this.actstartdateop[i] = actstartdateop;
	}
	
	
	public String[] getActenddate() {
		return actenddate;
	}
	
	public String getActenddate(int i) {
		return actenddate[i];
	}
	
	public void setActenddate(String[] actenddate) {
		this.actenddate = actenddate;
	}
	
	public void setActenddate(String actenddate, int i) {
		this.actenddate[i] = actenddate;
	}
	
	
	public String[] getActenddatehh() {
		return actenddatehh;
	}
	public String getActenddatehh(int i) {
		return actenddatehh[i];
	}
	public void setActenddatehh(String[] actenddatehh) {
		this.actenddatehh = actenddatehh;
	}
	public void setActenddatehh(String actenddatehh, int i) {
		this.actenddatehh[i] = actenddatehh;
	}
	public String[] getActenddatemm() {
		return actenddatemm;
	}
	public String getActenddatemm(int i) {
		return actenddatemm[i];
	}
	public void setActenddatemm(String[] actenddatemm) {
		this.actenddatemm = actenddatemm;
	}
	public void setActenddatemm(String actenddatemm, int i) {
		this.actenddatemm[i] = actenddatemm;
	}
	public String[] getActenddateop() {
		return actenddateop;
	}
	public String getActenddateop(int i) {
		return actenddateop[i];
	}
	public void setActenddateop(String[] actenddateop) {
		this.actenddateop = actenddateop;
	}
	public void setActenddateop(String actenddateop, int i) {
		this.actenddateop[i] = actenddateop;
	}
	public String getRownum() {
		return rownum;
	}
	public void setRownum(String rownum) {
		this.rownum = rownum;
	}
	public String[] getScheduleId() {
		return scheduleId;
	}
	public String getScheduleId(int i) {
		return scheduleId[i];
	}
	public void setScheduleId(String[] scheduleId) {
		this.scheduleId = scheduleId;
	}
	public String getDeleteStatus() {
		return deleteStatus;
	}
	
	public void setDeleteStatus(String deleteStatus) {
		this.deleteStatus = deleteStatus;
	}
	public String getDeletenum() {
		return deletenum;
	}
	public void setDeletenum(String deletenum) {
		this.deletenum = deletenum;
	}
	public String[] getPrevactarival() {
		return prevactarival;
	}
	public String getPrevactarival(int i) {
		return prevactarival[i];
	}
	public void setPrevactarival(String[] prevactarival) {
		this.prevactarival = prevactarival;
	}
	public void setPrevactarival(String prevactarival, int i) {
		this.prevactarival[i] = prevactarival;
	}
	
	public String[] getPrevdeparture() {
		return prevdeparture;
	}
	public String getPrevdeparture(int i) {
		return prevdeparture[i];
	}
	public void setPrevdeparture(String[] prevdeparture) {
		this.prevdeparture = prevdeparture;
	}
	public void setPrevdeparture(String prevdeparture, int i) {
		this.prevdeparture[i] = prevdeparture;
	}
	public String[] getScheduleEnddate() {
		return scheduleEnddate;
	}
	public void setScheduleEnddate(String[] scheduleEnddate) {
		this.scheduleEnddate = scheduleEnddate;
	}
	public String getScheduleEnddate(int i) {
		return scheduleEnddate[i];
	}
	public String[] getScheduleEndhh() {
		return scheduleEndhh;
	}
	public void setScheduleEndhh(String[] scheduleEndhh) {
		this.scheduleEndhh = scheduleEndhh;
	}
	public String getScheduleEndhh(int i) {
		return scheduleEndhh[i];
	}
	public String[] getScheduleEndmm() {
		return scheduleEndmm;
	}
	public void setScheduleEndmm(String[] scheduleEndmm) {
		this.scheduleEndmm = scheduleEndmm;
	}
	public String getScheduleEndmm(int i) {
		return scheduleEndmm[i];
	}
	public String[] getScheduleEndop() {
		return scheduleEndop;
	}
	public void setScheduleEndop(String[] scheduleEndop) {
		this.scheduleEndop = scheduleEndop;
	}
	public String getScheduleEndop(int i) {
		return scheduleEndop[i];
	}

}



