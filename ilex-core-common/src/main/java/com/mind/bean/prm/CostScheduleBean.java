package com.mind.bean.prm;

import java.io.Serializable;


public class CostScheduleBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String jobname = null;
	private String activityname = null;
	private String appendixname = null;
	private String appendixid = null;
	private String jobid = null;
	private String plannedstartdate = null;
	private String plannedenddate = null;
	private String actualstartdate = null;
	private String actualenddate = null;
	private String status = null;
	private String deviation = null;
	private String authenticate = "";
	private String back = null;
	private String viewjobtype = null;
	private String ownerId = null;
	private String msaId = null;
	
//	For View Selector 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}

	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
			
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	
	
	

	public String getBack() {
		return back;
	}




	public void setBack(String back) {
		this.back = back;
	}




	public String getActivityname() {
		return activityname;
	}




	public void setActivityname(String activityname) {
		this.activityname = activityname;
	}




	public String getJobname() {
		return jobname;
	}




	public void setJobname(String jobname) {
		this.jobname = jobname;
	}




	public String getAppendixname() {
		return appendixname;
	}




	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}




	public String getActualenddate() {
		return actualenddate;
	}




	public void setActualenddate(String actualenddate) {
		this.actualenddate = actualenddate;
	}


	

	public String getJobid() {
		return jobid;
	}




	public void setJobid(String jobid) {
		this.jobid = jobid;
	}




	public String getActualstartdate() {
		return actualstartdate;
	}




	public void setActualstartdate(String actualstartdate) {
		this.actualstartdate = actualstartdate;
	}




	public String getAppendixid() {
		return appendixid;
	}




	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}




	public String getAuthenticate() {
		return authenticate;
	}




	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}




	public String getDeviation() {
		return deviation;
	}




	public void setDeviation(String deviation) {
		this.deviation = deviation;
	}




	public String getPlannedenddate() {
		return plannedenddate;
	}




	public void setPlannedenddate(String plannedenddate) {
		this.plannedenddate = plannedenddate;
	}




	public String getPlannedstartdate() {
		return plannedstartdate;
	}




	public void setPlannedstartdate(String plannedstartdate) {
		this.plannedstartdate = plannedstartdate;
	}




	public String getStatus() {
		return status;
	}




	public void setStatus(String status) {
		this.status = status;
	}









	public String getViewjobtype() {
		return viewjobtype;
	}




	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}




	public String getMsaId() {
		return msaId;
	}




	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}




	public String getOwnerId() {
		return ownerId;
	}




	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	
}



