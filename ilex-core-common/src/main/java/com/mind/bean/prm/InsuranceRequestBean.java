package com.mind.bean.prm;

import java.io.File;

import com.mind.bean.prm.InsuranceRequestAttention;
import com.mind.bean.prm.InsuranceRequestDetails;
import com.mind.bean.prm.InsuranceRequestHolder;
import com.mind.bean.prm.InsuranceRequestReturn;

public class InsuranceRequestBean {
	
	private String jobId = null;
	private String appendixId = null;
	private String msaId = null;
	private String contactPerson = null;
	private String todayDate = null;
	private File file1 = null;
	private String fileName1 = null;
	private String loginUserId = null;
	private String loginUserEmail = null;
	
	private InsuranceRequestAttention insuranceRequestAttention;
	private InsuranceRequestDetails insuranceRequestDetails;
	private InsuranceRequestHolder insuranceRequestHolder;
	private InsuranceRequestReturn insuranceRequestReturn;
	
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public InsuranceRequestAttention getInsuranceRequestAttention() {
		return insuranceRequestAttention;
	}
	public void setInsuranceRequestAttention(
			InsuranceRequestAttention insuranceRequestAttention) {
		this.insuranceRequestAttention = insuranceRequestAttention;
	}
	public InsuranceRequestDetails getInsuranceRequestDetails() {
		return insuranceRequestDetails;
	}
	public void setInsuranceRequestDetails(
			InsuranceRequestDetails insuranceRequestDetails) {
		this.insuranceRequestDetails = insuranceRequestDetails;
	}
	public InsuranceRequestHolder getInsuranceRequestHolder() {
		return insuranceRequestHolder;
	}
	public void setInsuranceRequestHolder(
			InsuranceRequestHolder insuranceRequestHolder) {
		this.insuranceRequestHolder = insuranceRequestHolder;
	}
	public InsuranceRequestReturn getInsuranceRequestReturn() {
		return insuranceRequestReturn;
	}
	public void setInsuranceRequestReturn(
			InsuranceRequestReturn insuranceRequestReturn) {
		this.insuranceRequestReturn = insuranceRequestReturn;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getTodayDate() {
		return todayDate;
	}
	public void setTodayDate(String todayDate) {
		this.todayDate = todayDate;
	}
	public String getFileName1() {
		return fileName1;
	}
	public void setFileName1(String fileName1) {
		this.fileName1 = fileName1;
	}
	public File getFile1() {
		return file1;
	}
	public void setFile1(File file1) {
		this.file1 = file1;
	}
	public String getLoginUserEmail() {
		return loginUserEmail;
	}
	public void setLoginUserEmail(String loginUserEmail) {
		this.loginUserEmail = loginUserEmail;
	}
	public String getLoginUserId() {
		return loginUserId;
	}
	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}
	
}
