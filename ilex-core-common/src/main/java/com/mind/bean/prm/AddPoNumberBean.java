package com.mind.bean.prm;

import java.io.Serializable;

public class AddPoNumberBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
		private String typeid = null;
		private String type = null;
		private String partnerid = null;
		//private String jobname = null;
		//private String jobid = null;
		private String jobname = null;
		private String jobid = null;
		private String partnername = null;
		private String ponumber = null;
		private String wonumber = null;
		private String estimatedcost = null;
		private String invoicedcost = null;
		private String ckboxjob = null;
		private String[] ckboxactivity = null;
		
		private String save = null;
		private String reset = null;
		private String function = null;
		private String updatemessage = null;
		private String message = null;
		private String authenticate = "";
		private String[] ponumberact = null;
		private String[] wonumberact = null;
		private String[] hid_actid = null;
		
		
		
		
		public String getAuthenticate() {
			return authenticate;
		}




		public void setAuthenticate(String authenticate) {
			this.authenticate = authenticate;
		}




		public String[] getCkboxactivity() {
			return ckboxactivity;
		}

		public void setCkboxactivity(String[] ckboxactivity) {
			this.ckboxactivity = ckboxactivity;
		}

		public String getCkboxactivity( int i ) {
			return ckboxactivity[i];
		}


		public String getCkboxjob() {
			return ckboxjob;
		}




		public void setCkboxjob(String ckboxjob) {
			this.ckboxjob = ckboxjob;
		}




		public String getFunction() {
			return function;
		}




		public void setFunction(String function) {
			this.function = function;
		}




		public String getReset() {
			return reset;
		}




		public void setReset(String reset) {
			this.reset = reset;
		}




		public String getSave() {
			return save;
		}




		public void setSave(String save) {
			this.save = save;
		}




		public String getType() {
			return type;
		}




		public void setType(String type) {
			this.type = type;
		}




		public String getTypeid() {
			return typeid;
		}




		public void setTypeid(String typeid) {
			this.typeid = typeid;
		}




		public String getUpdatemessage() {
			return updatemessage;
		}




		public void setUpdatemessage(String updatemessage) {
			this.updatemessage = updatemessage;
		}

		


		public String getEstimatedcost() {
			return estimatedcost;
		}




		public void setEstimatedcost(String estimatedcost) {
			this.estimatedcost = estimatedcost;
		}




		public String getInvoicedcost() {
			return invoicedcost;
		}




		public void setInvoicedcost(String invoicedcost) {
			this.invoicedcost = invoicedcost;
		}




		public String getJobid() {
			return jobid;
		}




		public void setJobid(String jobid) {
			this.jobid = jobid;
		}




		public String getJobname() {
			return jobname;
		}




		public void setJobname(String jobname) {
			this.jobname = jobname;
		}




		public String getPartnerid() {
			return partnerid;
		}




		public void setPartnerid(String partnerid) {
			this.partnerid = partnerid;
		}




		public String getPartnername() {
			return partnername;
		}




		public void setPartnername(String partnername) {
			this.partnername = partnername;
		}




		public String getPonumber() {
			return ponumber;
		}




		public void setPonumber(String ponumber) {
			this.ponumber = ponumber;
		}




		public String getWonumber() {
			return wonumber;
		}




		public void setWonumber(String wonumber) {
			this.wonumber = wonumber;
		}


		

		public String getMessage() {
			return message;
		}




		public void setMessage(String message) {
			this.message = message;
		}


		public String[] getPonumberact() {
			return ponumberact;
		}


		public void setPonumberact(String[] ponumberact) {
			this.ponumberact = ponumberact;
		}
		
		public String getPonumberact( int i ) {
			return ponumberact[i];
		}
		
		




		public String[] getWonumberact() {
			return wonumberact;
		}


		public void setWonumberact(String[] wonumberact) {
			this.wonumberact = wonumberact;
		}
		
		public String getWonumberact( int i ) {
			return wonumberact[i];
		}







		public String[] getHid_actid() {
			return hid_actid;
		}




		public void setHid_actid(String[] hid_actid) {
			this.hid_actid = hid_actid;
		}
		
		public String getHid_actid( int i ) {
			return hid_actid[i];
		}




		
		

	}



