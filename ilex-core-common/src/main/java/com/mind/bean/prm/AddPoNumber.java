package com.mind.bean.prm;

import java.io.Serializable;

public class AddPoNumber implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String partnerid = null;
	//private String jobname = null;
	//private String jobid = null;
	private String activityname = null;
	private String activityid = null;
	private String partnername = null;
	private String ponumberact = null;
	private String wonumberact = null;
	private String estimatedcost = null;
	private String invoicedcost = null;
	private String hid_actid = null;
	
	
	public String getEstimatedcost() {
		return estimatedcost;
	}
	public void setEstimatedcost(String estimatedcost) {
		this.estimatedcost = estimatedcost;
	}
	public String getInvoicedcost() {
		return invoicedcost;
	}
	public void setInvoicedcost(String invoicedcost) {
		this.invoicedcost = invoicedcost;
	}
	public String getPartnerid() {
		return partnerid;
	}
	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}
	public String getPartnername() {
		return partnername;
	}
	public void setPartnername(String partnername) {
		this.partnername = partnername;
	}
	
	public String getPonumberact() {
		return ponumberact;
	}
	public void setPonumberact(String ponumberact) {
		this.ponumberact = ponumberact;
	}
	public String getWonumberact() {
		return wonumberact;
	}
	public void setWonumberact(String wonumberact) {
		this.wonumberact = wonumberact;
	}
	public String getActivityid() {
		return activityid;
	}
	public void setActivityid(String activityid) {
		this.activityid = activityid;
	}
	public String getActivityname() {
		return activityname;
	}
	public void setActivityname(String activityname) {
		this.activityname = activityname;
	}
	public String getHid_actid() {
		return hid_actid;
	}
	public void setHid_actid(String hid_actid) {
		this.hid_actid = hid_actid;
	}
	
	
}
