package com.mind.bean.prm;

import java.io.Serializable;
import java.util.Collection;

import com.mind.common.LabelValue;

public class AddAddendumBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
	 * Copyright (C) 2005 MIND
	 * All rights reserved.
	 * The information contained here in is confidential and
	 * proprietary to MIND and forms the part of MIND
	 * Project	: ILEX
	 * Description	: This is a form bean class used for getting and setting the values of Add Addendum Form.      
	 *
	 */


		private String appendixid = "";
		private String id = "";
		private String type = "";
		private String viewjobtype = null;
		private String ownerId = null;
		private String lx_addendum_id = "";

		private String functionPara = "";

		private String lx_addendum_name = "";

		private String lx_addendum_created_by = "";

		private String lx_addendum_create_date = "";

		private String lx_addendum_status = "O";

		private String parameter = "";

		private String customer_poc_id = "";

		private String customer_poc_phone = "";

		private String customer_poc_email = "";

		private String lx_co_addendum_type = "";
		private Collection lx_co_addendum_list = null;
		
		private String authenticate = "";
		private String save = null;

		private boolean refresh = false;
		
		private String back = null;
		
		private String appendixname = null;
		private String jobid = null;
		private String jobname = null;
		private String ref = null;
		private String selectedjob = null;
		private String addendumid = null;
		private String site = null;
		private String sitestate = null;
		private String sitezipcode = null;
		private String schedulestart = null;
		private String scheduletime = null;
		private String schedulecomplete = null;
		private String jobstatus = null;



		public String getBack() {
			return back;
		}

		public void setBack(String back) {
			this.back = back;
		}

		public String getCustomer_poc_email() {
			return customer_poc_email;
		}

		public void setCustomer_poc_email(String customer_poc_email) {
			this.customer_poc_email = customer_poc_email;
		}

		public String getCustomer_poc_phone() {
			return customer_poc_phone;
		}

		public void setCustomer_poc_phone(String customer_poc_phone) {
			this.customer_poc_phone = customer_poc_phone;
		}

		public String getLx_addendum_id() {
			return lx_addendum_id;
		}

		public void setLx_addendum_id(String lx_addendum_id) {
			this.lx_addendum_id = lx_addendum_id;
		}

		public String getLx_addendum_name() {
			return lx_addendum_name;
		}

		public void setLx_addendum_name(String lx_addendum_name) {
			this.lx_addendum_name = lx_addendum_name;
		}

		public boolean getRefresh() {
			return refresh;
		}

		public void setRefresh(boolean refresh) {
			this.refresh = refresh;
		}

		public String getSave() {
			return save;
		}

		public void setSave(String save) {
			this.save = save;
		}

		public String getLx_addendum_create_date() {
			return lx_addendum_create_date;
		}

		public void setLx_addendum_create_date(String lx_addendum_create_date) {
			this.lx_addendum_create_date = lx_addendum_create_date;
		}

		public String getLx_addendum_status() {
			return lx_addendum_status;
		}

		public void setLx_addendum_status(String lx_addendum_status) {
			this.lx_addendum_status = lx_addendum_status;
		}

		public String getCustomer_poc_id() {
			return customer_poc_id;
		}

		public void setCustomer_poc_id(String customer_poc_id) {
			this.customer_poc_id = customer_poc_id;
		}

		public String getAppendixid() {
			return appendixid;
		}

		public void setAppendixid(String appendixid) {
			this.appendixid = appendixid;
		}

		public String getLx_addendum_created_by() {
			return lx_addendum_created_by;
		}

		public void setLx_addendum_created_by(String lx_addendum_created_by) {
			this.lx_addendum_created_by = lx_addendum_created_by;
		}

		public String getFunctionPara() {
			return functionPara;
		}

		public void setFunctionPara(String functionPara) {
			this.functionPara = functionPara;
		}

		public String getParameter() {
			return parameter;
		}

		public void setParameter(String parameter) {
			this.parameter = parameter;
		}

		
		public Collection getLx_co_addendum_list() 
		{
			lx_co_addendum_list = new java.util.ArrayList();
			lx_co_addendum_list.add( new LabelValue( "Addendum" , "A" ) );
			lx_co_addendum_list.add( new LabelValue( "Change Order" , "C" ) );
			
			
			return lx_co_addendum_list;
		}

		public void setLx_co_addendum_list( Collection lx_co_addendum_list ) {
			this.lx_co_addendum_list = lx_co_addendum_list;
		}

		
		
		public String getLx_co_addendum_type() {
			return lx_co_addendum_type;
		}

		public void setLx_co_addendum_type( String lx_co_addendum_type ) {
			this.lx_co_addendum_type = lx_co_addendum_type;
		}

		
		public String getId() {
			return id;
		}

		public void setId( String id ) {
			this.id = id;
		}

		public String getType() {
			return type;
		}

		public void setType( String type ) {
			this.type = type;
		}

		public String getAuthenticate() {
			return authenticate;
		}

		public void setAuthenticate( String authenticate ) {
			this.authenticate = authenticate;
		}

		public String getJobid() {
			return jobid;
		}

		public void setJobid(String jobid) {
			this.jobid = jobid;
		}

		public String getJobname() {
			return jobname;
		}

		public void setJobname(String jobname) {
			this.jobname = jobname;
		}

		public String getRef() {
			return ref;
		}

		public void setRef(String ref) {
			this.ref = ref;
		}

		public String getSelectedjob() {
			return selectedjob;
		}

		public void setSelectedjob(String selectedjob) {
			this.selectedjob = selectedjob;
		}

		public String getAddendumid() {
			return addendumid;
		}

		public void setAddendumid(String addendumid) {
			this.addendumid = addendumid;
		}

		public String getJobstatus() {
			return jobstatus;
		}

		public void setJobstatus(String jobstatus) {
			this.jobstatus = jobstatus;
		}

		public String getSchedulecomplete() {
			return schedulecomplete;
		}

		public void setSchedulecomplete(String schedulecomplete) {
			this.schedulecomplete = schedulecomplete;
		}

		public String getSchedulestart() {
			return schedulestart;
		}

		public void setSchedulestart(String schedulestart) {
			this.schedulestart = schedulestart;
		}

		public String getScheduletime() {
			return scheduletime;
		}

		public void setScheduletime(String scheduletime) {
			this.scheduletime = scheduletime;
		}

		public String getSite() {
			return site;
		}

		public void setSite(String site) {
			this.site = site;
		}

		public String getSitestate() {
			return sitestate;
		}

		public void setSitestate(String sitestate) {
			this.sitestate = sitestate;
		}

		public String getSitezipcode() {
			return sitezipcode;
		}

		public void setSitezipcode(String sitezipcode) {
			this.sitezipcode = sitezipcode;
		}

		public String getAppendixname() {
			return appendixname;
		}

		public void setAppendixname(String appendixname) {
			this.appendixname = appendixname;
		}

		public String getViewjobtype() {
			return viewjobtype;
		}

		public void setViewjobtype(String viewjobtype) {
			this.viewjobtype = viewjobtype;
		}

		public String getOwnerId() {
			return ownerId;
		}

		public void setOwnerId(String ownerId) {
			this.ownerId = ownerId;
		}

			
		

	}



