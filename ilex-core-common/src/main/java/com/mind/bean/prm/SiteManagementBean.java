package com.mind.bean.prm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class SiteManagementBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// General Values
	private String appendixid = null;
	private String appendixname = null;
	private String ownerId = null;
	private String msaid = null;
	private String msaname = null;
	private String countRow = null;
	private String pageType = null;
	private String viewjobtype = null;
	private String cboxsite;
	// Database Values
	private String siteID = null;
	private String siteName = null;
	private String siteNumber = null;
	private String siteAddress = null;
	private String siteCity = null;
	private String state = null;
	private String stateSelect = null;
	private String siteZipCode = null;
	private String country = null;
	private String siteLocalityFactor = null;
	private String siteUnion = null;
	private String siteDesignator = null;
	private String siteDirections = null;
	private String siteWorkLocation = null;
	private ArrayList singleSiteInfo = null;
	private String sitePrimaryName = null;
	private String sitePrimaryPhone = null;
	private String sitePrimaryEmail = null;
	private String siteSecondaryName = null;
	private String siteSecondayPhone = null;
	private String siteSecondaryEmail = null;
	private String siteSpecialConditions = null;
	private String siteCreatedDate = null;
	private String siteCreatedBy = null;
	private String siteChangedDate = null;
	private String siteChangedBy = null;
	private String siteInstallerPoc = null;
	private String sitePoc = null;
	private String siteNotes = null;
	private String siteLatitude = null;
	private String siteLongitude = null;
	private String count = null;

	// SiteSearch values
	private String siteSearchName = null;
	private String siteSearchNumber = null;
	private String siteSearchCity = null;
	private String siteSearchState = null;
	private String siteSearchZip = null;
	private String siteSearchCountry = null;
	private String siteSearchGroup = null;
	private String siteSearchEndCustomer = null;
	private String siteSearchTimeZone = null;
	private String siteSearchStatus = null;

	// SiteColumn Spefic List
	private String siteNameForColumn = "on";
	private String siteNumberForColumn = "on";
	private String siteAddressForColumn = null;
	private String siteCityForColumn = null;
	private String siteStateForColumn = null;
	private String siteZipForColumn = null;
	private String siteCountryForColumn = null;

	//
	private String siteStatus = null;

	private String go = null;
	// private String search = null;
	private String hiddenstate = null;

	// Paging

	private String lines_per_page = null; // page_size
	private String current_page_no = null; // page_no
	private String total_page_size = null; // total_page_no
	private String org_page_no = null;
	private String org_lines_per_page = null;
	private String row_size = null;

	private String first = null;
	private String last = null;
	private String previous = null;
	private String next = null;

	private String inactive_sites = null;
	private String active_sites = null;
	private String total_sites = null;

	private String site_group = null;
	private String site_end_customer = null;
	private String site_time_zone = null;
	private String siteGroup = null;
	private String siteEndCustomer = null;

	private String siteCategory = null;
	private String siteRegion = null;

	private String siteCountryId = null;
	private String siteCountryIdName = null;
	private Collection tempList = null;

	private String refersh = null;
	private String siteTimeZone = null;
	private String siteLatLon = null;

	private String siteJobId = null;
	private ArrayList completeSiteList = null;
	private String specialEndCustomer = null;
	private String specialGroup = null;
	private String jobid = null;
	private String newadd = null;
	private String fromType = null;
	private String jobName = null;
	private String fromPage = null;

	// For View Selector
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;

	private String latitude = null;
	private String longitude = null;
	private String latLongAccuracy = null;
	private String latLongSource = null;

	private String clientHD_email;
	private String clientHD_phone;
	private String clientHD_Name;

	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}

	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}

	public String getJobSelectedStatus(int i) {
		return jobSelectedStatus[i];
	}

	public String getJobSelectedOwners(int i) {
		return jobSelectedOwners[i];
	}

	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}

	public String getJobid() {
		return jobid;
	}

	public String getNewadd() {
		return newadd;
	}

	public void setNewadd(String newadd) {
		this.newadd = newadd;
	}

	public void setJobid(String jobid) {
		this.jobid = jobid;
	}

	public String getSpecialGroup() {
		return specialGroup;
	}

	public void setSpecialGroup(String specialGroup) {
		this.specialGroup = specialGroup;
	}

	public ArrayList getCompleteSiteList() {
		return completeSiteList;
	}

	public void setCompleteSiteList(ArrayList completeSiteList) {
		this.completeSiteList = completeSiteList;
	}

	public String getSiteJobId() {
		return siteJobId;
	}

	public void setSiteJobId(String siteJobId) {
		this.siteJobId = siteJobId;
	}

	public String getSiteLatLon() {
		return siteLatLon;
	}

	public void setSiteLatLon(String siteLatLon) {
		this.siteLatLon = siteLatLon;
	}

	public String getSiteTimeZone() {
		return siteTimeZone;
	}

	public void setSiteTimeZone(String siteTimeZone) {
		this.siteTimeZone = siteTimeZone;
	}

	public String getRefersh() {
		return refersh;
	}

	public void setRefersh(String refersh) {
		this.refersh = refersh;
	}

	public String getSiteCountryIdName() {
		return siteCountryIdName;
	}

	public void setSiteCountryIdName(String siteCountryIdName) {
		this.siteCountryIdName = siteCountryIdName;
	}

	public String getSiteCountryId() {
		return siteCountryId;
	}

	public void setSiteCountryId(String siteCountryId) {
		this.siteCountryId = siteCountryId;
	}

	public String getSiteEndCustomer() {
		return siteEndCustomer;
	}

	public void setSiteEndCustomer(String siteEndCustomer) {
		this.siteEndCustomer = siteEndCustomer;
	}

	public String getSiteGroup() {
		return siteGroup;
	}

	public void setSiteGroup(String siteGroup) {
		this.siteGroup = siteGroup;
	}

	public String getSite_end_customer() {
		return site_end_customer;
	}

	public void setSite_end_customer(String site_end_customer) {
		this.site_end_customer = site_end_customer;
	}

	public String getSite_group() {
		return site_group;
	}

	public void setSite_group(String site_group) {
		this.site_group = site_group;
	}

	public String getSite_time_zone() {
		return site_time_zone;
	}

	public void setSite_time_zone(String site_time_zone) {
		this.site_time_zone = site_time_zone;
	}

	public String getActive_sites() {
		return active_sites;
	}

	public void setActive_sites(String active_sites) {
		this.active_sites = active_sites;
	}

	public String getInactive_sites() {
		return inactive_sites;
	}

	public void setInactive_sites(String inactive_sites) {
		this.inactive_sites = inactive_sites;
	}

	public String getTotal_sites() {
		return total_sites;
	}

	public void setTotal_sites(String total_sites) {
		this.total_sites = total_sites;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public String getPrevious() {
		return previous;
	}

	public void setPrevious(String previous) {
		this.previous = previous;
	}

	public String getCurrent_page_no() {
		return current_page_no;
	}

	public void setCurrent_page_no(String current_page_no) {
		this.current_page_no = current_page_no;
	}

	public String getLines_per_page() {
		return lines_per_page;
	}

	public void setLines_per_page(String lines_per_page) {
		this.lines_per_page = lines_per_page;
	}

	public String getOrg_page_no() {
		return org_page_no;
	}

	public void setOrg_page_no(String org_page_no) {
		this.org_page_no = org_page_no;
	}

	public String getTotal_page_size() {
		return total_page_size;
	}

	public void setTotal_page_size(String total_page_size) {
		this.total_page_size = total_page_size;
	}

	public String getGo() {
		return go;
	}

	public void setGo(String go) {
		this.go = go;
	}

	public String getSiteAddressForColumn() {
		return siteAddressForColumn;
	}

	public void setSiteAddressForColumn(String siteAddressForColumn) {
		this.siteAddressForColumn = siteAddressForColumn;
	}

	public String getSiteCityForColumn() {
		return siteCityForColumn;
	}

	public void setSiteCityForColumn(String siteCityForColumn) {
		this.siteCityForColumn = siteCityForColumn;
	}

	public String getSiteCountryForColumn() {
		return siteCountryForColumn;
	}

	public void setSiteCountryForColumn(String siteCountryForColumn) {
		this.siteCountryForColumn = siteCountryForColumn;
	}

	public String getSiteNameForColumn() {
		return siteNameForColumn;
	}

	public void setSiteNameForColumn(String siteNameForColumn) {
		this.siteNameForColumn = siteNameForColumn;
	}

	public String getSiteNumberForColumn() {
		return siteNumberForColumn;
	}

	public void setSiteNumberForColumn(String siteNumberForColumn) {
		this.siteNumberForColumn = siteNumberForColumn;
	}

	public String getSiteStateForColumn() {
		return siteStateForColumn;
	}

	public void setSiteStateForColumn(String siteStateForColumn) {
		this.siteStateForColumn = siteStateForColumn;
	}

	public String getSiteZipForColumn() {
		return siteZipForColumn;
	}

	public void setSiteZipForColumn(String siteZipForColumn) {
		this.siteZipForColumn = siteZipForColumn;
	}

	public String getSiteSearchName() {
		return siteSearchName;
	}

	public void setSiteSearchName(String siteSearchName) {
		this.siteSearchName = siteSearchName;
	}

	public String getSiteSearchNumber() {
		return siteSearchNumber;
	}

	public void setSiteSearchNumber(String siteSearchNumber) {
		this.siteSearchNumber = siteSearchNumber;
	}

	public String getSiteLatitude() {
		return siteLatitude;
	}

	public void setSiteLatitude(String siteLatitude) {
		this.siteLatitude = siteLatitude;
	}

	public String getSiteLongitude() {
		return siteLongitude;
	}

	public void setSiteLongitude(String siteLongitude) {
		this.siteLongitude = siteLongitude;
	}

	public String getSiteNotes() {
		return siteNotes;
	}

	public void setSiteNotes(String siteNotes) {
		this.siteNotes = siteNotes;
	}

	public String getSiteChangedBy() {
		return siteChangedBy;
	}

	public void setSiteChangedBy(String siteChangedBy) {
		this.siteChangedBy = siteChangedBy;
	}

	public String getSiteChangedDate() {
		return siteChangedDate;
	}

	public void setSiteChangedDate(String siteChangedDate) {
		this.siteChangedDate = siteChangedDate;
	}

	public String getSiteCreatedBy() {
		return siteCreatedBy;
	}

	public void setSiteCreatedBy(String siteCreatedBy) {
		this.siteCreatedBy = siteCreatedBy;
	}

	public String getSiteCreatedDate() {
		return siteCreatedDate;
	}

	public void setSiteCreatedDate(String siteCreatedDate) {
		this.siteCreatedDate = siteCreatedDate;
	}

	public String getSiteInstallerPoc() {
		return siteInstallerPoc;
	}

	public void setSiteInstallerPoc(String siteInstallerPoc) {
		this.siteInstallerPoc = siteInstallerPoc;
	}

	public String getSitePoc() {
		return sitePoc;
	}

	public void setSitePoc(String sitePoc) {
		this.sitePoc = sitePoc;
	}

	public String getSitePrimaryEmail() {
		return sitePrimaryEmail;
	}

	public void setSitePrimaryEmail(String sitePrimaryEmail) {
		this.sitePrimaryEmail = sitePrimaryEmail;
	}

	public String getSitePrimaryName() {
		return sitePrimaryName;
	}

	public void setSitePrimaryName(String sitePrimaryName) {
		this.sitePrimaryName = sitePrimaryName;
	}

	public String getSitePrimaryPhone() {
		return sitePrimaryPhone;
	}

	public void setSitePrimaryPhone(String sitePrimaryPhone) {
		this.sitePrimaryPhone = sitePrimaryPhone;
	}

	public String getSiteSecondaryEmail() {
		return siteSecondaryEmail;
	}

	public void setSiteSecondaryEmail(String siteSecondaryEmail) {
		this.siteSecondaryEmail = siteSecondaryEmail;
	}

	public String getSiteSecondaryName() {
		return siteSecondaryName;
	}

	public void setSiteSecondaryName(String siteSecondaryName) {
		this.siteSecondaryName = siteSecondaryName;
	}

	public String getSiteSecondayPhone() {
		return siteSecondayPhone;
	}

	public void setSiteSecondayPhone(String siteSecondayPhone) {
		this.siteSecondayPhone = siteSecondayPhone;
	}

	public String getSiteSpecialConditions() {
		return siteSpecialConditions;
	}

	public void setSiteSpecialConditions(String siteSpecialConditions) {
		this.siteSpecialConditions = siteSpecialConditions;
	}

	public String getSiteDirections() {
		return siteDirections;
	}

	public void setSiteDirections(String siteDirections) {
		this.siteDirections = siteDirections;
	}

	public String getSiteWorkLocation() {
		return siteWorkLocation;
	}

	public void setSiteWorkLocation(String siteWorkLocation) {
		this.siteWorkLocation = siteWorkLocation;
	}

	public String getSiteID() {
		return siteID;
	}

	public void setSiteID(String siteID) {
		this.siteID = siteID;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getMsaid() {
		return msaid;
	}

	public void setMsaid(String msaid) {
		this.msaid = msaid;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getAppendixname() {
		return appendixname;
	}

	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}

	public String getAppendixid() {
		return appendixid;
	}

	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}

	public String getSiteDesignator() {
		return siteDesignator;
	}

	public void setSiteDesignator(String siteDesignator) {
		this.siteDesignator = siteDesignator;
	}

	public String getSiteLocalityFactor() {
		return siteLocalityFactor;
	}

	public void setSiteLocalityFactor(String siteLocalityFactor) {
		this.siteLocalityFactor = siteLocalityFactor;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getSiteUnion() {
		return siteUnion;
	}

	public void setSiteUnion(String siteUnion) {
		this.siteUnion = siteUnion;
	}

	public String getCountRow() {
		return countRow;
	}

	public void setCountRow(String countRow) {
		this.countRow = countRow;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String siteCountry) {
		this.country = siteCountry;
	}

	public String getSiteZipCode() {
		return siteZipCode;
	}

	public void setSiteZipCode(String siteZipCode) {
		this.siteZipCode = siteZipCode;
	}

	public ArrayList getSingleSiteInfo() {
		return singleSiteInfo;
	}

	public void setSingleSiteInfo(ArrayList singleSiteInfo) {
		this.singleSiteInfo = singleSiteInfo;
	}

	public String getHiddenstate() {
		return hiddenstate;
	}

	public void setHiddenstate(String hiddenstate) {
		this.hiddenstate = hiddenstate;
	}

	public String getOrg_lines_per_page() {
		return org_lines_per_page;
	}

	public void setOrg_lines_per_page(String org_lines_per_page) {
		this.org_lines_per_page = org_lines_per_page;
	}

	/*
	 * public String getSearch() { return search; }
	 * 
	 * public void setSearch(String search) { this.search = search; }
	 */

	public String getSiteStatus() {
		return siteStatus;
	}

	public void setSiteStatus(String siteStatus) {
		this.siteStatus = siteStatus;
	}

	public String getRow_size() {
		return row_size;
	}

	public void setRow_size(String row_size) {
		this.row_size = row_size;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getSiteSearchCity() {
		return siteSearchCity;
	}

	public void setSiteSearchCity(String siteSearchCity) {
		this.siteSearchCity = siteSearchCity;
	}

	public String getSiteSearchCountry() {
		return siteSearchCountry;
	}

	public void setSiteSearchCountry(String siteSearchCountry) {
		this.siteSearchCountry = siteSearchCountry;
	}

	public String getSiteSearchState() {
		return siteSearchState;
	}

	public void setSiteSearchState(String siteSearchState) {
		this.siteSearchState = siteSearchState;
	}

	public String getSiteSearchZip() {
		return siteSearchZip;
	}

	public void setSiteSearchZip(String siteSearchZip) {
		this.siteSearchZip = siteSearchZip;
	}

	public String getSiteSearchEndCustomer() {
		return siteSearchEndCustomer;
	}

	public void setSiteSearchEndCustomer(String siteSearchEndCustomer) {
		this.siteSearchEndCustomer = siteSearchEndCustomer;
	}

	public String getSiteSearchGroup() {
		return siteSearchGroup;
	}

	public void setSiteSearchGroup(String siteSearchGroup) {
		this.siteSearchGroup = siteSearchGroup;
	}

	public Collection getTempList() {
		return tempList;
	}

	public void setTempList(Collection tempList) {
		this.tempList = tempList;
	}

	public String getSiteCategory() {
		return siteCategory;
	}

	public void setSiteCategory(String siteCategory) {
		this.siteCategory = siteCategory;
	}

	public String getSiteRegion() {
		return siteRegion;
	}

	public void setSiteRegion(String siteRegion) {
		this.siteRegion = siteRegion;
	}

	public String getSiteSearchTimeZone() {
		return siteSearchTimeZone;
	}

	public void setSiteSearchTimeZone(String siteSearchTimeZone) {
		this.siteSearchTimeZone = siteSearchTimeZone;
	}

	public String getPageType() {
		return pageType;
	}

	public void setPageType(String pageType) {
		this.pageType = pageType;
	}

	public String getSpecialEndCustomer() {
		return specialEndCustomer;
	}

	public void setSpecialEndCustomer(String specialEndCustomer) {
		this.specialEndCustomer = specialEndCustomer;
	}

	public String getSiteSearchStatus() {
		return siteSearchStatus;
	}

	public void setSiteSearchStatus(String siteSearchStatus) {
		this.siteSearchStatus = siteSearchStatus;
	}

	public String getFromType() {
		return fromType;
	}

	public void setFromType(String fromType) {
		this.fromType = fromType;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatLongAccuracy() {
		return latLongAccuracy;
	}

	public void setLatLongAccuracy(String latLongAccuracy) {
		this.latLongAccuracy = latLongAccuracy;
	}

	public String getLatLongSource() {
		return latLongSource;
	}

	public void setLatLongSource(String latLongSource) {
		this.latLongSource = latLongSource;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateSelect() {
		return stateSelect;
	}

	public void setStateSelect(String stateSelect) {
		this.stateSelect = stateSelect;
	}

	public String getClientHD_email() {
		return clientHD_email;
	}

	public void setClientHD_email(String clientHD_email) {
		this.clientHD_email = clientHD_email;
	}

	public String getClientHD_phone() {
		return clientHD_phone;
	}

	public void setClientHD_phone(String clientHD_phone) {
		this.clientHD_phone = clientHD_phone;
	}

	public String getClientHD_Name() {
		return clientHD_Name;
	}

	public void setClientHD_Name(String clientHD_Name) {
		this.clientHD_Name = clientHD_Name;
	}

	public String getCboxsite() {
		return cboxsite;
	}

	public void setCboxsite(String string) {
		this.cboxsite = string;
	}

}
