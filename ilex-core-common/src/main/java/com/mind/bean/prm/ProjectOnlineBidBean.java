package com.mind.bean.prm;

import java.io.Serializable;

public class ProjectOnlineBidBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appendixid = null;
	private String activityId = null;
	private String activityName = null;
	private String quantity = null;
	private String sow = null;
	private String assumption = null;
	private String estimatedCost = null;
	private String minutemanCost = null;
	private String speedpayCost = null;
	private String status = null;
	private String enableBid = null;
	private String disableBid = null;
	
//	For View Selector: Start 
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
//	For View Selector: End
	
//	For Menu: Start
	private String ownerId = null;
	private String viewjobtype = null;
	private String msaId = null;
	private String fromPage = null;
//	For Menu: End
	
	
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	/*
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	*/
	public String getAssumption() {
		return assumption;
	}
	public void setAssumption(String assumption) {
		this.assumption = assumption;
	}
	public String getEstimatedCost() {
		return estimatedCost;
	}
	public void setEstimatedCost(String estimatedCost) {
		this.estimatedCost = estimatedCost;
	}
	public String getMinutemanCost() {
		return minutemanCost;
	}
	public void setMinutemanCost(String minutemanCost) {
		this.minutemanCost = minutemanCost;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getSow() {
		return sow;
	}
	public void setSow(String sow) {
		this.sow = sow;
	}
	public String getSpeedpayCost() {
		return speedpayCost;
	}
	public void setSpeedpayCost(String speedpayCost) {
		this.speedpayCost = speedpayCost;
	}
	public String getDisableBid() {
		return disableBid;
	}
	public void setDisableBid(String disableBid) {
		this.disableBid = disableBid;
	}
	public String getEnableBid() {
		return enableBid;
	}
	public void setEnableBid(String enableBid) {
		this.enableBid = enableBid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFromPage() {
		return fromPage;
	}
	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}
	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}
	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}
	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}
	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}
	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}
	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}
	
	public String getJobSelectedOwners( int i ) {
		return jobSelectedOwners[i];
	}
	
	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}
	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}
	public String getJobSelectedStatus( int i ) {
		return jobSelectedStatus[i];
	}
	
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	
}


