package com.mind.bean.prm;

import java.io.Serializable;
import java.util.Collection;

public class ActivityTabularForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String activity_id = null;
	private String activity_name = null;
	private String quantity = null;
	private String type = null;
	private String scope = null;
	private String estimatedunitcost = null;
	private String estimatedtotalcost = null;
	private String extendedunitprice = null;
	private String extendedtotalprice = null;
	private String margin = null;
	private String status = null;
	private String activity_lx_ce_type = null;
	private String atUpliftCost = null;
	private String activity_tempid = null;
	private String activity_oos_flag = null;
	private Collection resourcelist = null;	
	
	public Collection getResourcelist() {
		return resourcelist;
	}
	public void setResourcelist(Collection resourcelist) {
		this.resourcelist = resourcelist;
	}
	public String getActivity_id() {
		return activity_id;
	}
	public void setActivity_id(String activity_id) {
		this.activity_id = activity_id;
	}
	public String getActivity_lx_ce_type() {
		return activity_lx_ce_type;
	}
	public void setActivity_lx_ce_type(String activity_lx_ce_type) {
		this.activity_lx_ce_type = activity_lx_ce_type;
	}
	public String getActivity_name() {
		return activity_name;
	}
	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}
	public String getActivity_oos_flag() {
		return activity_oos_flag;
	}
	public void setActivity_oos_flag(String activity_oos_flag) {
		this.activity_oos_flag = activity_oos_flag;
	}
	public String getActivity_tempid() {
		return activity_tempid;
	}
	public void setActivity_tempid(String activity_tempid) {
		this.activity_tempid = activity_tempid;
	}
	public String getAtUpliftCost() {
		return atUpliftCost;
	}
	public void setAtUpliftCost(String atUpliftCost) {
		this.atUpliftCost = atUpliftCost;
	}
	public String getEstimatedtotalcost() {
		return estimatedtotalcost;
	}
	public void setEstimatedtotalcost(String estimatedtotalcost) {
		this.estimatedtotalcost = estimatedtotalcost;
	}
	public String getEstimatedunitcost() {
		return estimatedunitcost;
	}
	public void setEstimatedunitcost(String estimatedunitcost) {
		this.estimatedunitcost = estimatedunitcost;
	}
	public String getExtendedtotalprice() {
		return extendedtotalprice;
	}
	public void setExtendedtotalprice(String extendedtotalprice) {
		this.extendedtotalprice = extendedtotalprice;
	}
	public String getExtendedunitprice() {
		return extendedunitprice;
	}
	public void setExtendedunitprice(String extendedunitprice) {
		this.extendedunitprice = extendedunitprice;
	}
	public String getMargin() {
		return margin;
	}
	public void setMargin(String margin) {
		this.margin = margin;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
