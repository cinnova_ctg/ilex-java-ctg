package com.mind.bean.prm;

import java.io.Serializable;

public class SiteHistoryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String siteId = null;
	private String siteListSize = null;
	private String customerName = null;
	private String appendixName = null;
	private String jobName = null;
	private String status = null;
	private String actualstartdate = null;
	private String owner = null;
	private String jobid = null;
	private String partnerName = null;
	private String schedulestartdate = null;
	
	private String schedulestarttime = null;
	private String schedulecomplete = null;
	private String actualstartdatetime = null;
	private String actualend = null;
	
	//Start :Folloeing for Ticket 
	private String msp = null;
	private String standby = null;
	private String requestReceivedDate_Ticket = null;
	private String requestReceivedTime_Ticket = null;
	private String scheduledArrivalDate_Ticket = null;
	private String scheduleArrivalTime_Ticket = null;
	private String criticality = null;
	private String hourlyCost = null;
	private String hourlyPrice = null;
	private String hourlyVGP = null;
	private String resource = null;
	//End
	
	private String estimatedcost = null;
	private String extendedprice = null;
	private String proformavgp = null;
	private String actualcost = null;
	private String actualvgp = null;
	
	private String from = null;
	private String appendixid = null;
	private String viewjobtype = null;
	private String ownerid = null;
	
	//Start :Following are for particular site Information
	private String id2 = null; //Job Id which will come from site.jsp 
	private String site_address = null;
	private String site_number = null;
	private String site_name = null;
	private String site_city = null;
	private String site_state = null;
	private String site_zipcode = null;
	private String site_country = null;
	private String locality_uplift  = null;
	private String union_site = null;
	private String site_worklocation = null;
	private String site_direction = null;
	private String primary_Name = null;
	private String site_phone = null;
	private String primary_email = null;
	private String secondary_Name = null;
	private String secondary_phone = null;
	private String secondary_email = null;
	private String site_notes = null;
	
	//End :Following are for particular site Information
	
	private String page = null;
	private String ref1 = null;
	
	private String msaid = null;
	private String msaname = null;
	private String checkRef1 = null;
	
	public String getMsaid() {
		return msaid;
	}
	public void setMsaid(String msaid) {
		this.msaid = msaid;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	public String getActualstartdate() {
		return actualstartdate;
	}
	public void setActualstartdate(String actualstartdate) {
		this.actualstartdate = actualstartdate;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getLocality_uplift() {
		return locality_uplift;
	}
	public void setLocality_uplift(String locality_uplift) {
		this.locality_uplift = locality_uplift;
	}
	public String getPrimary_email() {
		return primary_email;
	}
	public void setPrimary_email(String primary_email) {
		this.primary_email = primary_email;
	}
	public String getPrimary_Name() {
		return primary_Name;
	}
	public void setPrimary_Name(String primary_Name) {
		this.primary_Name = primary_Name;
	}
	public String getSecondary_email() {
		return secondary_email;
	}
	public void setSecondary_email(String secondary_email) {
		this.secondary_email = secondary_email;
	}
	public String getSecondary_Name() {
		return secondary_Name;
	}
	public void setSecondary_Name(String secondary_Name) {
		this.secondary_Name = secondary_Name;
	}
	public String getSecondary_phone() {
		return secondary_phone;
	}
	public void setSecondary_phone(String secondary_phone) {
		this.secondary_phone = secondary_phone;
	}
	public String getSite_address() {
		return site_address;
	}
	public void setSite_address(String site_address) {
		this.site_address = site_address;
	}
	public String getSite_city() {
		return site_city;
	}
	public void setSite_city(String site_city) {
		this.site_city = site_city;
	}
	public String getSite_direction() {
		return site_direction;
	}
	public void setSite_direction(String site_direction) {
		this.site_direction = site_direction;
	}
	public String getSite_name() {
		return site_name;
	}
	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}
	public String getSite_notes() {
		return site_notes;
	}
	public void setSite_notes(String site_notes) {
		this.site_notes = site_notes;
	}
	public String getSite_number() {
		return site_number;
	}
	public void setSite_number(String site_number) {
		this.site_number = site_number;
	}
	public String getSite_phone() {
		return site_phone;
	}
	public void setSite_phone(String site_phone) {
		this.site_phone = site_phone;
	}
	public String getSite_state() {
		return site_state;
	}
	public void setSite_state(String site_state) {
		this.site_state = site_state;
	}
	public String getSite_worklocation() {
		return site_worklocation;
	}
	public void setSite_worklocation(String site_worklocation) {
		this.site_worklocation = site_worklocation;
	}
	public String getSite_zipcode() {
		return site_zipcode;
	}
	public void setSite_zipcode(String site_zipcode) {
		this.site_zipcode = site_zipcode;
	}
	public String getUnion_site() {
		return union_site;
	}
	public void setUnion_site(String union_site) {
		this.union_site = union_site;
	}
	public String getId2() {
		return id2;
	}
	public void setId2(String id2) {
		this.id2 = id2;
	}
	public String getSite_country() {
		return site_country;
	}
	public void setSite_country(String site_country) {
		this.site_country = site_country;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getSchedulestartdate() {
		return schedulestartdate;
	}
	public void setSchedulestartdate(String schedulestartdate) {
		this.schedulestartdate = schedulestartdate;
	}
	public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	public String getActualend() {
		return actualend;
	}
	public void setActualend(String actualend) {
		this.actualend = actualend;
	}
	public String getActualstartdatetime() {
		return actualstartdatetime;
	}
	public void setActualstartdatetime(String actualstartdatetime) {
		this.actualstartdatetime = actualstartdatetime;
	}
	public String getSchedulecomplete() {
		return schedulecomplete;
	}
	public void setSchedulecomplete(String schedulecomplete) {
		this.schedulecomplete = schedulecomplete;
	}
	public String getSchedulestarttime() {
		return schedulestarttime;
	}
	public void setSchedulestarttime(String schedulestarttime) {
		this.schedulestarttime = schedulestarttime;
	}
	public String getActualcost() {
		return actualcost;
	}
	public void setActualcost(String actualcost) {
		this.actualcost = actualcost;
	}
	public String getActualvgp() {
		return actualvgp;
	}
	public void setActualvgp(String actualvgp) {
		this.actualvgp = actualvgp;
	}
	public String getEstimatedcost() {
		return estimatedcost;
	}
	public void setEstimatedcost(String estimatedcost) {
		this.estimatedcost = estimatedcost;
	}
	public String getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice(String extendedprice) {
		this.extendedprice = extendedprice;
	}
	public String getProformavgp() {
		return proformavgp;
	}
	public void setProformavgp(String proformavgp) {
		this.proformavgp = proformavgp;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	public String getViewjobtype() {
		return viewjobtype;
	}
	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}
	public String getCriticality() {
		return criticality;
	}
	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}
	public String getHourlyCost() {
		return hourlyCost;
	}
	public void setHourlyCost(String hourlyCost) {
		this.hourlyCost = hourlyCost;
	}
	public String getHourlyPrice() {
		return hourlyPrice;
	}
	public void setHourlyPrice(String hourlyPrice) {
		this.hourlyPrice = hourlyPrice;
	}
	public String getHourlyVGP() {
		return hourlyVGP;
	}
	public void setHourlyVGP(String hourlyVGP) {
		this.hourlyVGP = hourlyVGP;
	}
	public String getMsp() {
		return msp;
	}
	public void setMsp(String msp) {
		this.msp = msp;
	}
	public String getRequestReceivedDate_Ticket() {
		return requestReceivedDate_Ticket;
	}
	public void setRequestReceivedDate_Ticket(String requestReceivedDate_Ticket) {
		this.requestReceivedDate_Ticket = requestReceivedDate_Ticket;
	}
	public String getRequestReceivedTime_Ticket() {
		return requestReceivedTime_Ticket;
	}
	public void setRequestReceivedTime_Ticket(String requestReceivedTime_Ticket) {
		this.requestReceivedTime_Ticket = requestReceivedTime_Ticket;
	}
	public String getScheduleArrivalTime_Ticket() {
		return scheduleArrivalTime_Ticket;
	}
	public void setScheduleArrivalTime_Ticket(String scheduleArrivalTime_Ticket) {
		this.scheduleArrivalTime_Ticket = scheduleArrivalTime_Ticket;
	}
	public String getScheduledArrivalDate_Ticket() {
		return scheduledArrivalDate_Ticket;
	}
	public void setScheduledArrivalDate_Ticket(String scheduledArrivalDate_Ticket) {
		this.scheduledArrivalDate_Ticket = scheduledArrivalDate_Ticket;
	}
	public String getStandby() {
		return standby;
	}
	public void setStandby(String standby) {
		this.standby = standby;
	}
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}
	public String getSiteListSize() {
		return siteListSize;
	}
	public void setSiteListSize(String siteListSize) {
		this.siteListSize = siteListSize;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getRef1() {
		return ref1;
	}
	public void setRef1(String ref1) {
		this.ref1 = ref1;
	}
	public String getOwnerid() {
		return ownerid;
	}
	public void setOwnerid(String ownerid) {
		this.ownerid = ownerid;
	}
	public String getCheckRef1() {
		return checkRef1;
	}
	public void setCheckRef1(String checkRef1) {
		this.checkRef1 = checkRef1;
	}
	

}



