package com.mind.bean.prm;

import java.io.Serializable;

public class SiteChecklistDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private String checklistitemid = null;
	private String checklistname = null;
	private String itemname = null;
	private String itemdescription = null;
	private String checklisttype = null;
	private String checklistidtype = null;
	
	
	public String getChecklistitemid() {
		return checklistitemid;
	}
	public void setChecklistitemid(String checklistitemid) {
		this.checklistitemid = checklistitemid;
	}
	public String getChecklistname() {
		return checklistname;
	}
	public void setChecklistname(String checklistname) {
		this.checklistname = checklistname;
	}
	public String getItemdescription() {
		return itemdescription;
	}
	public void setItemdescription(String itemdescription) {
		this.itemdescription = itemdescription;
	}
	public String getItemname() {
		return itemname;
	}
	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	public String getChecklisttype() {
		return checklisttype;
	}
	public void setChecklisttype(String checklisttype) {
		this.checklisttype = checklisttype;
	}
	public String getChecklistidtype() {
		return checklistidtype;
	}
	public void setChecklistidtype(String checklistidtype) {
		this.checklistidtype = checklistidtype;
	}
	
	


}
