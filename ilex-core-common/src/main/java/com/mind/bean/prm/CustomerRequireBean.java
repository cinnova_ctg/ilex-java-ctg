package com.mind.bean.prm;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.mind.common.LabelValue;

public class CustomerRequireBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String jobId;
	private String appendixId;
	private String jobStatus;
	private String msaId;
	private List<LabelValue> contingentList;
	private List<LabelValue> clientList;
	private List<LabelValue> lecList;
	private List<LabelValue> otherPartyList;

	private String[] contingentFailVisitArray;
	private String[] clientFailVisitArray;
	private String[] lecFailVisitArray;
	private String[] otherPartyFailVisitArray;

	private Boolean firstVisitQuestion;
	private String[] failedVisitQuestion;
	private Boolean addSiteVisitQuestion;
	private String unplannedAddVisit;
	private List<String> categoryValues;
	private List<String> category;
	private boolean displayStatus;
	private Map<String, List<LabelValue>> selectedCategoryValues;

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public List<LabelValue> getContingentList() {
		return contingentList;
	}

	public void setContingentList(List<LabelValue> contingentList) {
		this.contingentList = contingentList;
	}

	public List<LabelValue> getClientList() {
		return clientList;
	}

	public void setClientList(List<LabelValue> clientList) {
		this.clientList = clientList;
	}

	public List<LabelValue> getLecList() {
		return lecList;
	}

	public void setLecList(List<LabelValue> lecList) {
		this.lecList = lecList;
	}

	public List<LabelValue> getOtherPartyList() {
		return otherPartyList;
	}

	public void setOtherPartyList(List<LabelValue> otherPartyList) {
		this.otherPartyList = otherPartyList;
	}

	public String[] getContingentFailVisitArray() {
		return contingentFailVisitArray;
	}

	public void setContingentFailVisitArray(String[] contingentFailVisitArray) {
		this.contingentFailVisitArray = contingentFailVisitArray;
	}

	public String[] getClientFailVisitArray() {
		return clientFailVisitArray;
	}

	public void setClientFailVisitArray(String[] clientFailVisitArray) {
		this.clientFailVisitArray = clientFailVisitArray;
	}

	public String[] getLecFailVisitArray() {
		return lecFailVisitArray;
	}

	public void setLecFailVisitArray(String[] lecFailVisitArray) {
		this.lecFailVisitArray = lecFailVisitArray;
	}

	public String[] getOtherPartyFailVisitArray() {
		return otherPartyFailVisitArray;
	}

	public void setOtherPartyFailVisitArray(String[] otherPartyFailVisitArray) {
		this.otherPartyFailVisitArray = otherPartyFailVisitArray;
	}

	public String getUnplannedAddVisit() {
		return unplannedAddVisit;
	}

	public void setUnplannedAddVisit(String unplannedAddVisit) {
		this.unplannedAddVisit = unplannedAddVisit;
	}

	public String[] getFailedVisitQuestion() {
		return failedVisitQuestion;
	}

	public void setFailedVisitQuestion(String[] failedVisitQuestion) {
		this.failedVisitQuestion = failedVisitQuestion;
	}

	public List<String> getCategoryValues() {
		return categoryValues;
	}

	public void setCategoryValues(List<String> categoryValues) {
		this.categoryValues = categoryValues;
	}

	public List<String> getCategory() {
		return category;
	}

	public void setCategory(List<String> category) {
		this.category = category;
	}

	public boolean isDisplayStatus() {
		return displayStatus;
	}

	public void setDisplayStatus(boolean displayStatus) {
		this.displayStatus = displayStatus;
	}

	public Map<String, List<LabelValue>> getSelectedCategoryValues() {
		return selectedCategoryValues;
	}

	public void setSelectedCategoryValues(
			Map<String, List<LabelValue>> selectedCategoryValues) {
		this.selectedCategoryValues = selectedCategoryValues;
	}

	public Boolean getFirstVisitQuestion() {
		return firstVisitQuestion;
	}

	public void setFirstVisitQuestion(Boolean firstVisitQuestion) {
		this.firstVisitQuestion = firstVisitQuestion;
	}

	public Boolean getAddSiteVisitQuestion() {
		return addSiteVisitQuestion;
	}

	public void setAddSiteVisitQuestion(Boolean addSiteVisitQuestion) {
		this.addSiteVisitQuestion = addSiteVisitQuestion;
	}

}
