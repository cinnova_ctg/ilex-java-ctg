package com.mind.bean.prm;

public class SiteHistoryBean {

	
	private String siteId = null;
	private String customerName = null;
	private String appendixName = null;
	private String jobName = null;
	private String status = null;
	
	private String owner = null;
	private String jobid = null;
	private String partnerName = null;
	
	
	private String schedulestartdate = null;
	private String schedulestarttime = null;
	private String schedulecomplete = null;
	
	private String actualstartdate = null;	
	private String actualstartdatetime = null;
	private String actualend = null;
	
	private String estimatedcost = null;
	private String extendedprice = null;
	private String proformavgp = null;
	private String actualcost = null;
	private String actualvgp = null;
	
//	Start :Folloeing for Ticket 
	private String msp = null;
	private String standby = null;
	private String requestReceivedDate_Ticket = null;
	private String requestReceivedTime_Ticket = null;
	private String scheduledArrivalDate_Ticket = null;
	private String scheduleArrivalTime_Ticket = null;
	private String criticality = null;
	private String hourlyCost = null;
	private String hourlyPrice = null;
	private String hourlyVGP = null;
	private String resource = null;
	//End
	
	
	
	public String getSchedulestartdate() {
		return schedulestartdate;
	}
	public void setSchedulestartdate(String schedulestartdate) {
		this.schedulestartdate = schedulestartdate;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	
	public String getActualstartdate() {
		return actualstartdate;
	}
	public void setActualstartdate(String actualstartdate) {
		this.actualstartdate = actualstartdate;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	public String getActualend() {
		return actualend;
	}
	public void setActualend(String actualend) {
		this.actualend = actualend;
	}
	public String getActualstartdatetime() {
		return actualstartdatetime;
	}
	public void setActualstartdatetime(String actualstartdatetime) {
		this.actualstartdatetime = actualstartdatetime;
	}
	public String getSchedulecomplete() {
		return schedulecomplete;
	}
	public void setSchedulecomplete(String schedulecomplete) {
		this.schedulecomplete = schedulecomplete;
	}
	public String getSchedulestarttime() {
		return schedulestarttime;
	}
	public void setSchedulestarttime(String schedulestarttime) {
		this.schedulestarttime = schedulestarttime;
	}
	public String getActualcost() {
		return actualcost;
	}
	public void setActualcost(String actualcost) {
		this.actualcost = actualcost;
	}
	public String getActualvgp() {
		return actualvgp;
	}
	public void setActualvgp(String actualvgp) {
		this.actualvgp = actualvgp;
	}
	public String getEstimatedcost() {
		return estimatedcost;
	}
	public void setEstimatedcost(String estimatedcost) {
		this.estimatedcost = estimatedcost;
	}
	public String getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice(String extendedprice) {
		this.extendedprice = extendedprice;
	}
	public String getProformavgp() {
		return proformavgp;
	}
	public void setProformavgp(String proformavgp) {
		this.proformavgp = proformavgp;
	}
	public String getCriticality() {
		return criticality;
	}
	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}
	public String getHourlyCost() {
		return hourlyCost;
	}
	public void setHourlyCost(String hourlyCost) {
		this.hourlyCost = hourlyCost;
	}
	public String getHourlyPrice() {
		return hourlyPrice;
	}
	public void setHourlyPrice(String hourlyPrice) {
		this.hourlyPrice = hourlyPrice;
	}
	public String getHourlyVGP() {
		return hourlyVGP;
	}
	public void setHourlyVGP(String hourlyVGP) {
		this.hourlyVGP = hourlyVGP;
	}
	public String getMsp() {
		return msp;
	}
	public void setMsp(String msp) {
		this.msp = msp;
	}
	public String getRequestReceivedDate_Ticket() {
		return requestReceivedDate_Ticket;
	}
	public void setRequestReceivedDate_Ticket(String requestReceivedDate_Ticket) {
		this.requestReceivedDate_Ticket = requestReceivedDate_Ticket;
	}
	public String getRequestReceivedTime_Ticket() {
		return requestReceivedTime_Ticket;
	}
	public void setRequestReceivedTime_Ticket(String requestReceivedTime_Ticket) {
		this.requestReceivedTime_Ticket = requestReceivedTime_Ticket;
	}
	public String getScheduleArrivalTime_Ticket() {
		return scheduleArrivalTime_Ticket;
	}
	public void setScheduleArrivalTime_Ticket(String scheduleArrivalTime_Ticket) {
		this.scheduleArrivalTime_Ticket = scheduleArrivalTime_Ticket;
	}
	public String getScheduledArrivalDate_Ticket() {
		return scheduledArrivalDate_Ticket;
	}
	public void setScheduledArrivalDate_Ticket(String scheduledArrivalDate_Ticket) {
		this.scheduledArrivalDate_Ticket = scheduledArrivalDate_Ticket;
	}
	public String getStandby() {
		return standby;
	}
	public void setStandby(String standby) {
		this.standby = standby;
	}
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}
}
