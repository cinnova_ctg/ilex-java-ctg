package com.mind.bean.newjobdb;

import java.io.Serializable;

public class EInvoiceItems implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String lineItem = "";
	private String lineType = "";
	private String lineDiscription = "";
	private String lineQuantity = "";
	private String lineUnitCost = "";
	private String lineTotalCost = "";
	
	public String getLineDiscription() {
		return lineDiscription;
	}
	public void setLineDiscription(String lineDiscription) {
		this.lineDiscription = lineDiscription;
	}
	public String getLineItem() {
		return lineItem;
	}
	public void setLineItem(String lineItem) {
		this.lineItem = lineItem;
	}
	
	public String getLineType() {
		return lineType;
	}
	public void setLineType(String lineType) {
		this.lineType = lineType;
	}
	public String getLineQuantity() {
		return lineQuantity;
	}
	public void setLineQuantity(String lineQuantity) {
		this.lineQuantity = lineQuantity;
	}
	public String getLineTotalCost() {
		return lineTotalCost;
	}
	public void setLineTotalCost(String lineTotalCost) {
		this.lineTotalCost = lineTotalCost;
	}
	public String getLineUnitCost() {
		return lineUnitCost;
	}
	public void setLineUnitCost(String lineUnitCost) {
		this.lineUnitCost = lineUnitCost;
	}
	

}
