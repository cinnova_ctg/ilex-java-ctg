package com.mind.bean.newjobdb;

import java.io.Serializable;
import java.util.ArrayList;



/**
 * @purpose This class is used for the representation of 
 * 			the Team Information.
 */
public class TeamInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String owner;
	private ArrayList<String> schedulers;
	
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public ArrayList<String> getSchedulers() {
		return schedulers;
	}
	public void setSchedulers(ArrayList<String> schedulers) {
		this.schedulers = schedulers;
	}
}
