package com.mind.bean.newjobdb;

import java.io.Serializable;



/**
 * @purpose This class is used for the representation of 
 * 			the Planning notes.
 */
public class PlanningNote implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String note;

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
