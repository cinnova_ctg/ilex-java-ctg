package com.mind.bean.newjobdb;

/**
 * @purpose This class is used for the representation of the TICKET tab.
 */
public class TicketTab extends JobDashboardTab {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String ticketNumber;
    private long ticketId;
    private long appendixId;
    private long jobId;
    private String webTicket;
    private String updateCompleteTicket = "0";
    private Integer monitoringSourceCd = null;
    private Long deviceId = null;

    private String requestorIds = null;
    private String additionalRecipientIds = null;

    private TicketRequestInfo requestInfo = new TicketRequestInfo();
    private TicketScheduleInfo scheduleInfo = new TicketScheduleInfo();
    private TicketRequestNature requestNature = new TicketRequestNature();

    public TicketTab() {
        tabId = JobDashboardTabType.TICKET_TAB;
    }

    public void updateTicket() {

    }

    public TicketRequestInfo getRequestInfo() {
        return requestInfo;
    }

    public void setRequestInfo(TicketRequestInfo requestInfo) {
        this.requestInfo = requestInfo;
    }

    public TicketRequestNature getRequestNature() {
        return requestNature;
    }

    public void setRequestNature(TicketRequestNature requestNature) {
        this.requestNature = requestNature;
    }

    public TicketScheduleInfo getScheduleInfo() {
        return scheduleInfo;
    }

    public void setScheduleInfo(TicketScheduleInfo scheduleInfo) {
        this.scheduleInfo = scheduleInfo;
    }

    public long getAppendixId() {
        return appendixId;
    }

    public void setAppendixId(long appendixId) {
        this.appendixId = appendixId;
    }

    public long getTicketId() {
        return ticketId;
    }

    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public String getWebTicket() {
        return webTicket;
    }

    public void setWebTicket(String webTicket) {
        this.webTicket = webTicket;
    }

    public String getUpdateCompleteTicket() {
        return updateCompleteTicket;
    }

    public void setUpdateCompleteTicket(String updateCompleteTicket) {
        this.updateCompleteTicket = updateCompleteTicket;
    }

    public Integer getMonitoringSourceCd() {
        return monitoringSourceCd;
    }

    public void setMonitoringSourceCd(Integer monitoringSourceCd) {
        this.monitoringSourceCd = monitoringSourceCd;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getRequestorIds() {
        return requestorIds;
    }

    public void setRequestorIds(String requestorIds) {
        this.requestorIds = requestorIds;
    }

    public String getAdditionalRecipientIds() {
        return additionalRecipientIds;
    }

    public void setAdditionalRecipientIds(String additionalRecipientIds) {
        this.additionalRecipientIds = additionalRecipientIds;
    }
}
