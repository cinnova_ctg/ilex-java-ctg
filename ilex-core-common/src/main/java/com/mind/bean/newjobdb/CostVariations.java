package com.mind.bean.newjobdb;

import java.io.Serializable;




/**
 * @purpose This class is used to represent 
 * 			the Cost Variations
 */
public class CostVariations implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private POType PVSType = new POType(null);
	private POType minutemanType = new POType(PVSType);
	private POType speedpayType = new POType(PVSType);
	
	public POType getMinutemanType() {
		return minutemanType;
	}
	public void setMinutemanType(POType minutemanType) {
		this.minutemanType = minutemanType;
	}
	public POType getPVSType() {
		return PVSType;
	}
	public void setPVSType(POType type) {
		PVSType = type;
	}
	public POType getSpeedpayType() {
		return speedpayType;
	}
	public void setSpeedpayType(POType speedpayType) {
		this.speedpayType = speedpayType;
	}
	
}
