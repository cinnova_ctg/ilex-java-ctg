package com.mind.bean.newjobdb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class ResourceLevel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String resName = "";
	private ArrayList<ResourceLevel> optionGroup = new ArrayList<ResourceLevel>();
	private Collection resourceList = null;

	public ArrayList<ResourceLevel> getOptionGroup() {
		return optionGroup;
	}

	public void setOptionGroup(ArrayList<ResourceLevel> optionGroup) {
		this.optionGroup = optionGroup;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public Collection getResourceList() {
		return resourceList;
	}

	public void setResourceList(Collection resourceList) {
		this.resourceList = resourceList;
	}

}
