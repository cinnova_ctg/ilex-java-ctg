package com.mind.bean.newjobdb;

import java.io.Serializable;

public class EInvoiceSummary implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String invoiceId = "";
	private String invoiceNumber = "";
	private String invoiceType = "";
	private String invoiceStatus = "";
	private String invoiceDate = "";
	private String invoiceReceivedDate = "";
	private String invoicePartnerComment = "";
	private String invoiceRecivedBy = "";
	private String invoiceApprovedDate = "";
	private String invoiceApprovedBy = "";
	private String invoiceExplanation = "";
	private String invoicePaidDate = "";
	private String invoiceCheckNumber = "";
	private String invoiceAuthorisedTotal = "";
	private String invoiceTotal = "";
	private String invoiceApprovedTotal = "";
	private String invoiceLastUpdatedBy = "";
	private String invoiceLastUpdate = "";
	private String lineTaxAmount = "";
	private String lineTotal = "";
	
	
	public String getLineTaxAmount() {
		return lineTaxAmount;
	}
	public void setLineTaxAmount(String lineTaxAmount) {
		this.lineTaxAmount = lineTaxAmount;
	}
	public String getLineTotal() {
		return lineTotal;
	}
	public void setLineTotal(String lineTotal) {
		this.lineTotal = lineTotal;
	}
	public String getInvoiceApprovedBy() {
		return invoiceApprovedBy;
	}
	public void setInvoiceApprovedBy(String invoiceApprovedBy) {
		this.invoiceApprovedBy = invoiceApprovedBy;
	}
	public String getInvoiceApprovedDate() {
		return invoiceApprovedDate;
	}
	public void setInvoiceApprovedDate(String invoiceApprovedDate) {
		this.invoiceApprovedDate = invoiceApprovedDate;
	}
	
	public String getInvoiceAuthorisedTotal() {
		return invoiceAuthorisedTotal;
	}
	public void setInvoiceAuthorisedTotal(String invoiceAuthorisedTotal) {
		this.invoiceAuthorisedTotal = invoiceAuthorisedTotal;
	}
	public String getInvoiceCheckNumber() {
		return invoiceCheckNumber;
	}
	public void setInvoiceCheckNumber(String invoiceCheckNumber) {
		this.invoiceCheckNumber = invoiceCheckNumber;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getInvoiceExplanation() {
		return invoiceExplanation;
	}
	public void setInvoiceExplanation(String invoiceExplanation) {
		this.invoiceExplanation = invoiceExplanation;
	}
	
	
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getInvoicePaidDate() {
		return invoicePaidDate;
	}
	public void setInvoicePaidDate(String invoicePaidDate) {
		this.invoicePaidDate = invoicePaidDate;
	}
	public String getInvoicePartnerComment() {
		return invoicePartnerComment;
	}
	public void setInvoicePartnerComment(String invoicePartnerComment) {
		this.invoicePartnerComment = invoicePartnerComment;
	}
	public String getInvoiceRecivedBy() {
		return invoiceRecivedBy;
	}
	public void setInvoiceRecivedBy(String invoiceRecivedBy) {
		this.invoiceRecivedBy = invoiceRecivedBy;
	}
	public String getInvoiceReceivedDate() {
		return invoiceReceivedDate;
	}
	public void setInvoiceReceivedDate(String invoiceRecivedDate) {
		this.invoiceReceivedDate = invoiceRecivedDate;
	}
	public String getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	
	
	
	public String getInvoiceLastUpdate() {
		return invoiceLastUpdate;
	}
	public void setInvoiceLastUpdate(String invoiceLastUpdate) {
		this.invoiceLastUpdate = invoiceLastUpdate;
	}
	public String getInvoiceLastUpdatedBy() {
		return invoiceLastUpdatedBy;
	}
	public void setInvoiceLastUpdatedBy(String invoiceLastUpdatedBy) {
		this.invoiceLastUpdatedBy = invoiceLastUpdatedBy;
	}
	
	public String getInvoiceApprovedTotal() {
		return invoiceApprovedTotal;
	}
	public void setInvoiceApprovedTotal(String invoiceApprovedTotal) {
		this.invoiceApprovedTotal = invoiceApprovedTotal;
	}
	public String getInvoiceTotal() {
		return invoiceTotal;
	}
	public void setInvoiceTotal(String invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}
	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	

}
