package com.mind.bean.newjobdb;

import java.io.Serializable;

/**
 @purpose This class is used for the representation of 
 * 			the Job Invoice Summary.
 */
public class JobInvoiceSummary implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String invNumber= null;
	private String invDate= null;
	private String invAmount= null;
	private String invDiscount= null;
	private String invAge= null;
	
	public String getInvAge() {
		return invAge;
	}
	public void setInvAge(String invAge) {
		this.invAge = invAge;
	}
	public String getInvAmount() {
		return invAmount;
	}
	public void setInvAmount(String invAmount) {
		this.invAmount = invAmount;
	}
	public String getInvDate() {
		return invDate;
	}
	public void setInvDate(String invDate) {
		this.invDate = invDate;
	}
	public String getInvDiscount() {
		return invDiscount;
	}
	public void setInvDiscount(String invDiscount) {
		this.invDiscount = invDiscount;
	}
	public String getInvNumber() {
		return invNumber;
	}
	public void setInvNumber(String invNumber) {
		this.invNumber = invNumber;
	}

}
