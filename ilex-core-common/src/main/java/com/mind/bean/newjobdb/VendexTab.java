package com.mind.bean.newjobdb;




/**
 * @purpose This class is used for the representation of 
 * 			the Vendex tab.
 * 
 * @ActionsAllowed from Vendex tab:
 * 		All POs will be displayed on the Vendex tab, but only 
 * 		POs in the draft status will have applicable actions 
 * 		which can be initiated from the Vendex tab.
 * 		
 * 		The Vendex tab is used to manage the POs which are in the draft state.  
 * 		Thus as POs are created based on the copy or build functions, 
 * 		they appear on the Vendex tab with Actions possible on Draft state.
 */
public class VendexTab extends JobDashboardTab {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appendixId;
	private String jobId;

	/**
	 * The Vendex tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	private GeneralJobInfo generalJobInfo;
	

	public VendexTab() {
		tabId = JobDashboardTabType.VENDEX_TAB;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	/**
	 * The Vendex tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public GeneralJobInfo getGeneralJobInfo() {
		return generalJobInfo;
	}

	/**
	 * The Vendex tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public void setGeneralJobInfo(GeneralJobInfo generalJobInfo) {
		this.generalJobInfo = generalJobInfo;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

}
