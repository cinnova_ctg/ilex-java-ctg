package com.mind.bean.newjobdb;

import java.io.Serializable;
import java.util.ArrayList;

public class JobWorkflowBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String[] itemId = null;
	private String[] description = null;
	private String[] sequence = null;
	private String[] referenceDate = null;
	private String[] user = null;
	private String jobId;
	private String[] createdBy = null;
	private String[] updatedBy = null;
	private String[] createdDate = null;
	private String[] updatedDate = null;
	private String[] itemStatus;
	private String[] itemStatusCode;
	private String[] projectWorkflowItemId;
	private ArrayList<JobWorkflowItem> completeItemList = null;
	private ArrayList<JobWorkflowItem> firstPenndingList = null;
	private ArrayList<JobWorkflowItem> pendingList = null;
	private int completeItemListSize ;
	private int firstPenndingListSize ;
	private int pendingListSize ;
	private String [] lastItemStatusCode;
	public String[] getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String[] createdBy) {
		this.createdBy = createdBy;
	}
	
	public String[] getDescription() {
		return description;
	}
	public void setDescription(String[] description) {
		this.description = description;
	}
	
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String[] getReferenceDate() {
		return referenceDate;
	}
	public void setReferenceDate(String[] referenceDate) {
		this.referenceDate = referenceDate;
	}
	public String[] getSequence() {
		return sequence;
	}
	public void setSequence(String[] sequence) {
		this.sequence = sequence;
	}
	public String[] getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String[] updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public String[] getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String[] createdDate) {
		this.createdDate = createdDate;
	}
	public String[] getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String[] updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String[] getUser() {
		return user;
	}
	public void setUser(String[] user) {
		this.user = user;
	}
	public String[] getItemStatus() {
		return itemStatus;
	}
	public void setItemStatus(String[] itemStatus) {
		this.itemStatus = itemStatus;
	}
	
	
	
	public String[] getItemId() {
		return itemId;
	}
	public void setItemId(String[] itemId) {
		this.itemId = itemId;
	}
	public String[] getItemStatusCode() {
		return itemStatusCode;
	}
	public void setItemStatusCode(String[] itemStatusCode) {
		this.itemStatusCode = itemStatusCode;
	}
	public String[] getLastItemStatusCode() {
		return lastItemStatusCode;
	}
	public void setLastItemStatusCode(String[] lastItemStatusCode) {
		this.lastItemStatusCode = lastItemStatusCode;
	}
	public String[] getProjectWorkflowItemId() {
		return projectWorkflowItemId;
	}
	public void setProjectWorkflowItemId(String[] projectWorkflowItemId) {
		this.projectWorkflowItemId = projectWorkflowItemId;
	}
	public ArrayList<JobWorkflowItem> getCompleteItemList() {
		return completeItemList;
	}
	public void setCompleteItemList(ArrayList<JobWorkflowItem> completeItemList) {
		this.completeItemList = completeItemList;
	}
	public ArrayList<JobWorkflowItem> getFirstPenndingList() {
		return firstPenndingList;
	}
	public void setFirstPenndingList(ArrayList<JobWorkflowItem> firstPenndingList) {
		this.firstPenndingList = firstPenndingList;
	}
	public ArrayList<JobWorkflowItem> getPendingList() {
		return pendingList;
	}
	public void setPendingList(ArrayList<JobWorkflowItem> pendingList) {
		this.pendingList = pendingList;
	}
	public int getCompleteItemListSize() {
		return completeItemListSize;
	}
	public void setCompleteItemListSize(int completeItemListSize) {
		this.completeItemListSize = completeItemListSize;
	}
	public int getFirstPenndingListSize() {
		return firstPenndingListSize;
	}
	public void setFirstPenndingListSize(int firstPenndingListSize) {
		this.firstPenndingListSize = firstPenndingListSize;
	}
	public int getPendingListSize() {
		return pendingListSize;
	}
	public void setPendingListSize(int pendingListSize) {
		this.pendingListSize = pendingListSize;
	}
	

}
