package com.mind.bean.newjobdb;

import java.io.Serializable;
import java.util.ArrayList;

public class FinancialAnalysis implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/* Labor Analysis */
	private ArrayList<CostAnalysis> costAnalysis = new ArrayList<CostAnalysis>();
	private float minutemanLaborTravel = 0.0f;
	private float speedpayLaborTravel = 0.0f;
	private float minutemanUsage = 0.0f;
	private float speedpayUsage = 0.0f;
	private float minutemanCostSavings = 0.0f;
	private float speedpayCostSavings = 0.0f;
	
	/* Revenue and VGPM */
	private float extendedPrice = 0.0f;
	private float jobDiscount = 0.0f;
	private float invoicePrice = 0.0f;
	private float jobReserver = 0.0f;
	private float proFormaVGPM = 0.0f;
	private float actualVGPM = 0.0f;
	private float appendixActualVGPM = 0.0f;
	
	/* Cost */
	private float totalEstimatedCost = 0.0f;
	private float totalActualCost = 0.0f;
	private float contractLaborEstimatedCost = 0.0f;
	private float contractLaborActualCost = 0.0f;
	private float contractMaterialEstimatedCost = 0.0f;
	private float contractMaterialActualCost = 0.0f;
	private float contractFreightEstimatedCost = 0.0f;
	private float contractFreightActualCost = 0.0f;
	private float contractTravelEstimatedCost = 0.0f;
	private float contractTravelActualCost = 0.0f;
	private float contractTotalEstimatedCost = 0.0f;
	private float contractTotalActualCost = 0.0f;
	private float corporateLaborEstimatedCost = 0.0f;
	private float corporateLaborActualCost = 0.0f;
	private float corporateMaterialEstimatedCost = 0.0f;
	private float corporateMaterialActualCost = 0.0f;
	private float corporateFreightEstimatedCost = 0.0f;
	private float corporateFreightActualCost = 0.0f;
	private float corporateTravelEstimatedCost = 0.0f;
	private float corporateTravelActualCost = 0.0f;
	private float corporateTotalEstimatedCost = 0.0f;
	private float corporateTotalActualCost = 0.0f;
	
	/* Financial Breakdown */
	private float vgp = 0.0f;
	private float contractCost = 0.0f;
	private float corporateCost = 0.0f;
	private float discountCost = 0.0f;
	
	
	/* Getters and Setters */
	public float getActualVGPM() {
		return actualVGPM;
	}
	public void setActualVGPM(float actualVGPM) {
		this.actualVGPM = actualVGPM;
	}
	public float getAppendixActualVGPM() {
		return appendixActualVGPM;
	}
	public void setAppendixActualVGPM(float appendixActualVGPM) {
		this.appendixActualVGPM = appendixActualVGPM;
	}
	public float getContractCost() {
		return contractCost;
	}
	public void setContractCost(float contractCost) {
		this.contractCost = contractCost;
	}
	public float getContractFreightActualCost() {
		return contractFreightActualCost;
	}
	public void setContractFreightActualCost(float contractFreightActualCost) {
		this.contractFreightActualCost = contractFreightActualCost;
	}
	public float getContractFreightEstimatedCost() {
		return contractFreightEstimatedCost;
	}
	public void setContractFreightEstimatedCost(float contractFreightEstimatedCost) {
		this.contractFreightEstimatedCost = contractFreightEstimatedCost;
	}
	public float getContractLaborActualCost() {
		return contractLaborActualCost;
	}
	public void setContractLaborActualCost(float contractLaborActualCost) {
		this.contractLaborActualCost = contractLaborActualCost;
	}
	public float getContractLaborEstimatedCost() {
		return contractLaborEstimatedCost;
	}
	public void setContractLaborEstimatedCost(float contractLaborEstimatedCost) {
		this.contractLaborEstimatedCost = contractLaborEstimatedCost;
	}
	public float getContractMaterialActualCost() {
		return contractMaterialActualCost;
	}
	public void setContractMaterialActualCost(float contractMaterialActualCost) {
		this.contractMaterialActualCost = contractMaterialActualCost;
	}
	public float getContractMaterialEstimatedCost() {
		return contractMaterialEstimatedCost;
	}
	public void setContractMaterialEstimatedCost(float contractMaterialEstimatedCost) {
		this.contractMaterialEstimatedCost = contractMaterialEstimatedCost;
	}
	public float getContractTotalActualCost() {
		return contractTotalActualCost;
	}
	public void setContractTotalActualCost(float contractTotalActualCost) {
		this.contractTotalActualCost = contractTotalActualCost;
	}
	public float getContractTotalEstimatedCost() {
		return contractTotalEstimatedCost;
	}
	public void setContractTotalEstimatedCost(float contractTotalEstimatedCost) {
		this.contractTotalEstimatedCost = contractTotalEstimatedCost;
	}
	public float getContractTravelActualCost() {
		return contractTravelActualCost;
	}
	public void setContractTravelActualCost(float contractTravelActualCost) {
		this.contractTravelActualCost = contractTravelActualCost;
	}
	public float getContractTravelEstimatedCost() {
		return contractTravelEstimatedCost;
	}
	public void setContractTravelEstimatedCost(float contractTravelEstimatedCost) {
		this.contractTravelEstimatedCost = contractTravelEstimatedCost;
	}
	public float getCorporateCost() {
		return corporateCost;
	}
	public void setCorporateCost(float corporateCost) {
		this.corporateCost = corporateCost;
	}
	public float getCorporateFreightActualCost() {
		return corporateFreightActualCost;
	}
	public void setCorporateFreightActualCost(float corporateFreightActualCost) {
		this.corporateFreightActualCost = corporateFreightActualCost;
	}
	public float getCorporateFreightEstimatedCost() {
		return corporateFreightEstimatedCost;
	}
	public void setCorporateFreightEstimatedCost(float corporateFreightEstimatedCost) {
		this.corporateFreightEstimatedCost = corporateFreightEstimatedCost;
	}
	public float getCorporateLaborActualCost() {
		return corporateLaborActualCost;
	}
	public void setCorporateLaborActualCost(float corporateLaborActualCost) {
		this.corporateLaborActualCost = corporateLaborActualCost;
	}
	public float getCorporateLaborEstimatedCost() {
		return corporateLaborEstimatedCost;
	}
	public void setCorporateLaborEstimatedCost(float corporateLaborEstimatedCost) {
		this.corporateLaborEstimatedCost = corporateLaborEstimatedCost;
	}
	public float getCorporateMaterialActualCost() {
		return corporateMaterialActualCost;
	}
	public void setCorporateMaterialActualCost(float corporateMaterialActualCost) {
		this.corporateMaterialActualCost = corporateMaterialActualCost;
	}
	public float getCorporateMaterialEstimatedCost() {
		return corporateMaterialEstimatedCost;
	}
	public void setCorporateMaterialEstimatedCost(
			float corporateMaterialEstimatedCost) {
		this.corporateMaterialEstimatedCost = corporateMaterialEstimatedCost;
	}
	public float getCorporateTotalActualCost() {
		return corporateTotalActualCost;
	}
	public void setCorporateTotalActualCost(float corporateTotalActualCost) {
		this.corporateTotalActualCost = corporateTotalActualCost;
	}
	public float getCorporateTotalEstimatedCost() {
		return corporateTotalEstimatedCost;
	}
	public void setCorporateTotalEstimatedCost(float corporateTotalEstimatedCost) {
		this.corporateTotalEstimatedCost = corporateTotalEstimatedCost;
	}
	public float getCorporateTravelActualCost() {
		return corporateTravelActualCost;
	}
	public void setCorporateTravelActualCost(float corporateTravelActualCost) {
		this.corporateTravelActualCost = corporateTravelActualCost;
	}
	public float getCorporateTravelEstimatedCost() {
		return corporateTravelEstimatedCost;
	}
	public void setCorporateTravelEstimatedCost(float corporateTravelEstimatedCost) {
		this.corporateTravelEstimatedCost = corporateTravelEstimatedCost;
	}
	public ArrayList<CostAnalysis> getCostAnalysis() {
		return costAnalysis;
	}
	public void setCostAnalysis(ArrayList<CostAnalysis> costAnalysis) {
		this.costAnalysis = costAnalysis;
	}
	public float getDiscountCost() {
		return discountCost;
	}
	public void setDiscountCost(float discountCost) {
		this.discountCost = discountCost;
	}
	public float getExtendedPrice() {
		return extendedPrice;
	}
	public void setExtendedPrice(float extendedPrice) {
		this.extendedPrice = extendedPrice;
	}
	public float getInvoicePrice() {
		return invoicePrice;
	}
	public void setInvoicePrice(float invoicePrice) {
		this.invoicePrice = invoicePrice;
	}
	public float getJobDiscount() {
		return jobDiscount;
	}
	public void setJobDiscount(float jobDiscount) {
		this.jobDiscount = jobDiscount;
	}
	public float getJobReserver() {
		return jobReserver;
	}
	public void setJobReserver(float jobReserver) {
		this.jobReserver = jobReserver;
	}
	public float getMinutemanCostSavings() {
		return minutemanCostSavings;
	}
	public void setMinutemanCostSavings(float minutemanCostSavings) {
		this.minutemanCostSavings = minutemanCostSavings;
	}
	public float getMinutemanLaborTravel() {
		return minutemanLaborTravel;
	}
	public void setMinutemanLaborTravel(float minutemanLaborTravel) {
		this.minutemanLaborTravel = minutemanLaborTravel;
	}
	public float getMinutemanUsage() {
		return minutemanUsage;
	}
	public void setMinutemanUsage(float minutemanUses) {
		this.minutemanUsage = minutemanUses;
	}
	public float getProFormaVGPM() {
		return proFormaVGPM;
	}
	public void setProFormaVGPM(float proFormaVGPM) {
		this.proFormaVGPM = proFormaVGPM;
	}
	public float getSpeedpayCostSavings() {
		return speedpayCostSavings;
	}
	public void setSpeedpayCostSavings(float speedpayCostSavings) {
		this.speedpayCostSavings = speedpayCostSavings;
	}
	public float getSpeedpayLaborTravel() {
		return speedpayLaborTravel;
	}
	public void setSpeedpayLaborTravel(float speedpayLaborTravel) {
		this.speedpayLaborTravel = speedpayLaborTravel;
	}
	public float getSpeedpayUsage() {
		return speedpayUsage;
	}
	public void setSpeedpayUsage(float speedpayUses) {
		this.speedpayUsage = speedpayUses;
	}
	public float getTotalActualCost() {
		return totalActualCost;
	}
	public void setTotalActualCost(float totalActualCost) {
		this.totalActualCost = totalActualCost;
	}
	public float getTotalEstimatedCost() {
		return totalEstimatedCost;
	}
	public void setTotalEstimatedCost(float totalEstimatedCost) {
		this.totalEstimatedCost = totalEstimatedCost;
	}
	public float getVgp() {
		return vgp;
	}
	public void setVgp(float vgp) {
		this.vgp = vgp;
	}
	
}
