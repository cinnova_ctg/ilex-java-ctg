package com.mind.bean.newjobdb;

import java.io.Serializable;

public class PartnerInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String partnerAddress = "";
	private String partnerCountry = "";
	private String partnerCity = "";
	private String partnerState = "";
	private String partnerLatitude = "";
	private String partnerLongitude = "";
	
	public String getPartnerAddress() {
		return partnerAddress;
	}
	public void setPartnerAddress(String partnerAddress) {
		this.partnerAddress = partnerAddress;
	}
	public String getPartnerCity() {
		return partnerCity;
	}
	public void setPartnerCity(String partnerCity) {
		this.partnerCity = partnerCity;
	}
	public String getPartnerCountry() {
		return partnerCountry;
	}
	public void setPartnerCountry(String partnerCountry) {
		this.partnerCountry = partnerCountry;
	}
	public String getPartnerLatitude() {
		return partnerLatitude;
	}
	public void setPartnerLatitude(String partnerLatitude) {
		this.partnerLatitude = partnerLatitude;
	}
	public String getPartnerLongitude() {
		return partnerLongitude;
	}
	public void setPartnerLongitude(String partnerLongitude) {
		this.partnerLongitude = partnerLongitude;
	}
	public String getPartnerState() {
		return partnerState;
	}
	public void setPartnerState(String partnerState) {
		this.partnerState = partnerState;
	}
	

}
