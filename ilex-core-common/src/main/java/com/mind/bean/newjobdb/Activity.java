package com.mind.bean.newjobdb;

import java.io.Serializable;



/**
 * @purpose This class is used to contain the functionality for the 
 * 			activity list in the Activity management 
 */
public class Activity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long activityId;
	private String activityName;
	private String scope;
	private float uplifts;
	private float quantity;
	private CostInfo estimatedCostInfo;
	private CostInfo extendedCostInfo;
	private String activityLibraryId;
	private String activity_oos_flag;
	private String activity_lx_ce_type ;
	private String activityType ; 
	
	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	public String getActivity_oos_flag() {
		return activity_oos_flag;
	}
	public void setActivity_oos_flag(String activity_oos_flag) {
		this.activity_oos_flag = activity_oos_flag;
	}
	public String getActivityLibraryId() {
		return activityLibraryId;
	}
	public void setActivityLibraryId(String activityLibraryId) {
		this.activityLibraryId = activityLibraryId;
	}
	public long getActivityId() {
		return activityId;
	}
	public void setActivityId(long activityId) {
		this.activityId = activityId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public CostInfo getEstimatedCostInfo() {
		return estimatedCostInfo;
	}
	public void setEstimatedCostInfo(CostInfo estimatedCostInfo) {
		this.estimatedCostInfo = estimatedCostInfo;
	}
	public CostInfo getExtendedCostInfo() {
		return extendedCostInfo;
	}
	public void setExtendedCostInfo(CostInfo extendedCostInfo) {
		this.extendedCostInfo = extendedCostInfo;
	}
	public float getQuantity() {
		return quantity;
	}
	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public float getUplifts() {
		return uplifts;
	}
	public void setUplifts(float uplifts) {
		this.uplifts = uplifts;
	}
	public String getActivity_lx_ce_type() {
		return activity_lx_ce_type;
	}
	public void setActivity_lx_ce_type(String activity_lx_ce_type) {
		this.activity_lx_ce_type = activity_lx_ce_type;
	}
	
}
