package com.mind.bean.newjobdb;

import java.io.Serializable;

public class POFooterInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String poFooterCreateDate;
	private String poFooterChangeDate;
	private String poFooterSource;
	private String poFooterCreateBy;
	private String poFooterChangeBy;
	
	
	public String getPoFooterChangeBy() {
		return poFooterChangeBy;
	}
	public void setPoFooterChangeBy(String poFooterChangeBy) {
		this.poFooterChangeBy = poFooterChangeBy;
	}
	public String getPoFooterCreateBy() {
		return poFooterCreateBy;
	}
	public void setPoFooterCreateBy(String poFooterCreateBy) {
		this.poFooterCreateBy = poFooterCreateBy;
	}
	public String getPoFooterChangeDate() {
		return poFooterChangeDate;
	}
	public void setPoFooterChangeDate(String poFooterChangeDate) {
		this.poFooterChangeDate = poFooterChangeDate;
	}
	public String getPoFooterCreateDate() {
		return poFooterCreateDate;
	}
	public void setPoFooterCreateDate(String poFooterCreateDate) {
		this.poFooterCreateDate = poFooterCreateDate;
	}
	public String getPoFooterSource() {
		return poFooterSource;
	}
	public void setPoFooterSource(String poFooterSource) {
		this.poFooterSource = poFooterSource;
	}

}
