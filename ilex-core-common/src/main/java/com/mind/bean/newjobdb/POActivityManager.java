package com.mind.bean.newjobdb;

import java.io.Serializable;
import java.util.ArrayList;




public class POActivityManager implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<POResource> poResources;
	private float mpoTotalCost;
	private float estimatedTotalCost;
	private float authorizedTotalCost;
	private float deltaCost;
	private float authorizedCostInput;
	
	public static void savePoResources(POResource poResource) {
		
	}
	
	public ArrayList<POResource> getAllPossibleResources() {
		return null;
	}

	public float getAuthorizedTotalCost() {
		return authorizedTotalCost;
	}

	public void setAuthorizedTotalCost(float authorizedTotalCost) {
		this.authorizedTotalCost = authorizedTotalCost;
	}

	public float getDeltaCost() {
		return deltaCost;
	}

	public void setDeltaCost(float deltaCost) {
		this.deltaCost = deltaCost;
	}

	public float getEstimatedTotalCost() {
		return estimatedTotalCost;
	}

	public void setEstimatedTotalCost(float estimatedTotalCost) {
		this.estimatedTotalCost = estimatedTotalCost;
	}

	public float getMpoTotalCost() {
		return mpoTotalCost;
	}

	public void setMpoTotalCost(float mpoTotalCost) {
		this.mpoTotalCost = mpoTotalCost;
	}

	public ArrayList<POResource> getPoResources() {
		return poResources;
	}

	public void setPoResources(ArrayList<POResource> poResources) {
		this.poResources = poResources;
	}

	public float getAuthorizedCostInput() {
		return authorizedCostInput;
	}

	public void setAuthorizedCostInput(float authorizedCostInput) {
		this.authorizedCostInput = authorizedCostInput;
	}
}
