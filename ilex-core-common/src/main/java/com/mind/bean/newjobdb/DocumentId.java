package com.mind.bean.newjobdb;

import java.io.Serializable;

public class DocumentId implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String DocId = null;
	private String withPO = null;
	private String withWO = null;
	
	public String getDocId() {
		return DocId;
	}
	public void setDocId(String docId) {
		DocId = docId;
	}
	public String getWithPO() {
		return withPO;
	}
	public void setWithPO(String withPO) {
		this.withPO = withPO;
	}
	public String getWithWO() {
		return withWO;
	}
	public void setWithWO(String withWo) {
		this.withWO = withWo;
	}

}
