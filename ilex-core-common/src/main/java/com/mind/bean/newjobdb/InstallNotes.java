package com.mind.bean.newjobdb;

import java.io.Serializable;

public class InstallNotes implements Serializable {
	private String date = null;
	private String installNote = null;
	private String user = null;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getInstallNote() {
		return installNote;
	}
	public void setInstallNote(String installNote) {
		this.installNote = installNote;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}

}
