package com.mind.bean.newjobdb;

import java.io.Serializable;

/**
 * @purpose This class is used to represent the  
 * 			the TICKET depot product
 */
public class TicketDepotProduct implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long productId;
	private String productCategory;
	private String productName;
	
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}

}
