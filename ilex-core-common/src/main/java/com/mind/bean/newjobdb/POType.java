package com.mind.bean.newjobdb;

import java.io.Serializable;




/**
 * @purpose This class is used to represent a POType 
 */
public class POType implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private float contractLaborCost;
	private float hourlyTravelCost;
	private float totalCost;
	private POType comparedToPOType;
	
	public float getCostSaving() {
		if(comparedToPOType!=null) {
			return comparedToPOType.totalCost - totalCost;
		} else {
			return 0.0f;
		}
	}
	
	
	public POType(POType comparedToPOType)
	{
		this.setComparedToPOType(comparedToPOType);
	}
	public POType getComparedToPOType() {
		return comparedToPOType;
	}

	private void setComparedToPOType(POType comparedToPOType) {
		this.comparedToPOType = comparedToPOType;
	}

	public float getContractLaborCost() {
		return contractLaborCost;
	}

	public void setContractLaborCost(float contractLaborCost) {
		this.contractLaborCost = contractLaborCost;
	}

	public float getHourlyTravelCost() {
		return hourlyTravelCost;
	}

	public void setHourlyTravelCost(float hourlyTravelCost) {
		this.hourlyTravelCost = hourlyTravelCost;
	}

	public float getTotalCost() {
		return contractLaborCost+hourlyTravelCost;
	}

	public void setTotalCost(float totalCost) {
		this.totalCost = totalCost;
	}
}
