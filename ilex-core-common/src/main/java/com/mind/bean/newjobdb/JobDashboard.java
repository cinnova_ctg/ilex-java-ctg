package com.mind.bean.newjobdb;

import java.io.Serializable;


/**
 * @purpose This class is used to contain the functionality for the 
 * 			Job Dashboard.  
 */
public class JobDashboard implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long jobId;
	private JobDashboardTab jobDashboardTab = null;
	public JobDashboardTab getJobDashboardTab() {
		return jobDashboardTab;
	}
	public void setJobDashboardTab(JobDashboardTab jobDashboardTab) {
		this.jobDashboardTab = jobDashboardTab;
	}
	public JobDashboardTab getDefaultTab() {
		return null;
	}
	public long getJobId() {
		return jobId;
	}
	public void setJobId(long jobId) {
		this.jobId = jobId;
	}
}
