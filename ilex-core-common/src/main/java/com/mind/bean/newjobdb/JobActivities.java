package com.mind.bean.newjobdb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;



/**
 * @purpose This class is used for the representation of 
 * 			the Job Activities.
 */
public class JobActivities implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Activity> activities;
	
	public float getTotalQuantity() {
	
		float sum = 0.0f;
		Iterator iterator = activities.iterator();
		while(iterator.hasNext()){
			Activity activity = (Activity)iterator.next();
			sum += activity.getQuantity();
		}
		return sum;
	}
	
	public float getTotalEstimatedUnitCost() {
	
		float sum = 0.0f;
		Iterator iterator = activities.iterator();
		while(iterator.hasNext()){
			Activity activity = (Activity)iterator.next();
			sum += activity.getEstimatedCostInfo().getUnit();
		}
		return sum;
		
	}
	
	public float getTotalEstimatedCost() {
		
		float sum = 0.0f;
		Iterator iterator = activities.iterator();
		while(iterator.hasNext()){
			Activity activity = (Activity)iterator.next();
			sum += activity.getEstimatedCostInfo().getTotal();
		}
		return sum;
		
	}
	
	public float getTotalExtendedUnitPrice() {
		
		float sum = 0.0f;
		Iterator iterator = activities.iterator();
		while(iterator.hasNext()){
			Activity activity = (Activity)iterator.next();
			sum += activity.getExtendedCostInfo().getUnit();
		}
		return sum;
	
	}
	public float getTotalExtendedPrice() {
		
		float sum = 0.0f;
		Iterator iterator = activities.iterator();
		while(iterator.hasNext()){
			Activity activity = (Activity)iterator.next();
			sum += activity.getExtendedCostInfo().getTotal();
		}
		return sum;
	
	}

	public ArrayList<Activity> getActivities() {
		return activities;
	}

	public void setActivities(ArrayList<Activity> activities) {
		this.activities = activities;
	}
}
