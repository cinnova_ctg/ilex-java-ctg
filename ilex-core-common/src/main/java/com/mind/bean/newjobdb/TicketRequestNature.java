package com.mind.bean.newjobdb;

import java.io.Serializable;




/**
 * @purpose This class is used for the representation of 
 * 			the TICKET request nature.
 */
public class TicketRequestNature implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String problemCategory;
	private String problemDescription;
	private String specialInstructions;
	private String ticketRules;
	
	public String getProblemCategory() {
		return problemCategory;
	}

	public void setProblemCategory(String problemCategory) {
		this.problemCategory = problemCategory;
	}

	public String getProblemDescription() {
		return problemDescription;
	}

	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}

	public String getSpecialInstructions() {
		return specialInstructions;
	}

	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public String getTicketRules() {
		return ticketRules;
	}

	public void setTicketRules(String ticketRules) {
		this.ticketRules = ticketRules;
	}

	public void findSupportingDocs() {
	
	}
}
