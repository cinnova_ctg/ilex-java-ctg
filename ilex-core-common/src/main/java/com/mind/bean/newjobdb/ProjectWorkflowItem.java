package com.mind.bean.newjobdb;



/**
 * @purpose This class is used for the representation of 
 * 			the Project Level Workflow Checklist Item.
 */
public class ProjectWorkflowItem extends WorkflowChecklistItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String appendixId;

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
}
