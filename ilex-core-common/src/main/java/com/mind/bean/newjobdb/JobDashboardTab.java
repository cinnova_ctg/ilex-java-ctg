package com.mind.bean.newjobdb;

import java.io.Serializable;



/**
 * @purpose This class is used to be implimented for the 
 * 			presentation for a Tab of the Job Dashboard 
 */
public abstract class JobDashboardTab implements Serializable {
	public int tabId;
	
	public int getTabId() {
		return tabId;
	}
}
