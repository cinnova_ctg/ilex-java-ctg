package com.mind.bean.newjobdb;


/**
 * @purpose This class is used for the representation of 
 * 			the tab types available.
 */
public class JobDashboardTabType  { 
	public final static int FINANCIAL_TAB=0;
	public final static int SETUP_TAB = 1;
	public final static int TICKET_TAB = 2;
	public final static int BUY_TAB = 3;
	public final static int EXECUTE_TAB=4;
	public final static int COMPLETE_TAB=5;
	public final static int CLOSE_TAB=6;
	public final static int SCHEDULE_TAB=7;
	public final static int DISPATCH_SCHEDULE_TAB=8;
	public final static int SITE_TAB=9;
	public final static int VENDEX_TAB=10;
	
}
