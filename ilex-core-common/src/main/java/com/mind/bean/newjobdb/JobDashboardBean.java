package com.mind.bean.newjobdb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.mind.bean.prm.DispatchScheduleUpdateBean;
import com.mind.common.LabelValue;

public class JobDashboardBean extends DispatchScheduleUpdateBean implements
		Serializable {

	private String jobId = "";
	private String jobStatus = "";
	private String appendixId = "";
	private String tabId = "";
	private String ticket_id = "";
	private String ticketType = "";
	private String site_id = "";
	private String createdBy = "";
	private String hmode = "";
	private String scheduleType = "";
	private String startDate = null;
	private String endDate = null;
	private String reference = "";
	private String note = "";
	private String currentOwner = "";
	private String ticketNumber = "";
	private String showCustomerReference = "";
	private String optactivity = null;
	private String[] activity_lx_ce_type = null;
	// Menu
	private String msaId = null;
	private String msaName = null;
	private String appendixName = null;
	private String jobName = null;
	private String appendixType = null;
	private String jobType = null;

	private String[] activityType = null;
	private String[] activity_Id = null;
	private String[] activitylibrary_Id = null;
	private String[] activityName = null;
	private String[] scope = null;
	private String[] uplifts = null;
	private String[] quantity = null;
	private String[] estimatedUnitcost = null;
	private String[] estimatedTotalCost = null;
	private String[] extendedUnitcost = null;
	private String[] extendedTotalCost = null;

	private String totalQuantity = null;
	private String totalEstimatedUnitCost = null;
	private String totalEstimatedCost = null;
	private String totalExtendedUnitPrice = null;
	private String totalExtendedPrice = null;

	private String owner = "";
	private ArrayList schedulers = null;

	private boolean isUs;
	private String site_country = "";
	private String site_city = "";
	private String site_state = "";
	private String site_number = "";
	private String site_priorities;
	private long[] jobWorkflowItemId;
	private String[] jobWorkflowItemStatus;
	private String[] jobWorkflowItemDescription;
	private String[] jobWorkflowItemSequence;
	private String[] jobWorkflowItemReferenceDate;
	private String[] jobWorkflowItemUser;
	private String[] activity_oos_flag;
	private String allItemStatus;
	private String itemListSize;
	private String jobIndicator = null;

	// for Ticket
	private String requestor;
	private String requestorEmail;
	private String isSendEmail;
	private String requestType;
	private String criticality;
	private String pps;
	private String resource;
	private String standBy;
	private String msp;
	private String helpDesk;
	private String customerReference;
	private String site;
	private String problemCategory;
	private String problemDescription;
	private String specialInstructions;
	private String ticketRules;
	private String requestedDate;
	private String requestedHour;
	private String requestedMinute;
	private String requestedIsAm;
	private String preferredDate;
	private String preferredHour;
	private String preferredMinute;
	private String preferredIsAm;
	private String windowFromDate;
	private String windowFromHour;
	private String windowFromMinute;
	private String windowFromIsAm;
	private String windowToDate;
	private String windowToHour;
	private String windowToMinute;
	private String windowToIsAm;
	private String receivedDate;
	private String receivedHour;
	private String receivedMinute;
	private String receivedIsAm;
	private ArrayList<LabelValue> requestorList = null;
	private ArrayList<LabelValue> requestTypeList = null;
	private ArrayList<LabelValue> criticalityList = null;
	private ArrayList<LabelValue> resourceList = null;
	private ArrayList<LabelValue> problemCategoriesList = null;
	private ArrayList<LabelValue> requestedHourList = null;
	private ArrayList<LabelValue> requestedMinuteList = null;
	private ArrayList<LabelValue> preferredHourList = null;
	private ArrayList<LabelValue> preferredMinuteList = null;
	private ArrayList<LabelValue> windowFromHourList = null;
	private ArrayList<LabelValue> windowFromMinuteList = null;
	private ArrayList<LabelValue> windowToHourList = null;
	private ArrayList<LabelValue> windowToMinuteList = null;
	private String poNumber[];
	private String poId[];
	private String cancelledPONumber[];
	private String woNumber[];
	private String status[];
	private String previousStatus[];
	private String revision[];
	private String costingType[];
	private String authorizedCost[];
	private String partnerContact[];
	private String primaryContact[];
	private String masterPOItemType[];
	private String minutemanTypeCLC = "";
	private String minutemanTypeTHC = "";
	private String minutemanTypeTC = "";
	private String minutemanTypeCostSavings = "";
	private String speedpayTypeCLC = "";
	private String speedpayTypeTHC = "";
	private String speedpayTypeTC = "";
	private String speedpayTypeCostSavings = "";
	private String PVSTypeCLC = "";
	private String PVSTypeTHC = "";
	private String PVSTypeTC = "";
	private String PVSTypeCostSavings = "";
	private String actionOnPO = "";
	private Object[] availableActions = null;
	private String isClicked = null;
	private String mapView = null;
	private String selection = null;
	private String onsiteDate;
	private String onsiteHour;
	private String onsiteMinute;
	private String onsiteIsAm;
	private String offsiteDate;
	private String offsiteHour;
	private String offsiteMinute;
	private String offsiteIsAm;
	private String scheduleStartDate;
	private String scheduleStartHour;
	private String scheduleStartMinute;
	private String scheduleStartIsAm;

	private String scheduleEndDate;
	private String scheduleEndHour;
	private String scheduleEndMinute;
	private String scheduleEndIsAm;

	private String latestInstallNotes;
	private String secondLatestInstallNotes;
	private String latestInstallNotesDate;
	private String secondLatestInstallNotesDate;
	private String latestInstallNotesType = "0";
	private String secondLatestInstallNotesType = "0";
	private String installNoteCount;
	private String save;

	private String webTicket;
	private String updateCompleteTicket;

	// Site information
	private String id1 = null;
	private String id2 = null;
	private String site_Id = null;

	private String site_name = null;
	private String site_designator = null;
	private String site_worklocation = null;
	private String site_address = null;

	private String state = null;
	private String country = null;
	private String stateSelect = null;
	private String site_zipcode = null;
	private String site_notes = null;
	private String site_direction = null;
	private String site_phone = null;
	private String locality_uplift = null;
	private String union_site = null;
	private String primary_email = null;
	private String secondary_email = null;
	private String secondary_phone = null;
	private String primary_Name = null;
	private String secondary_Name = null;

	private String appendixname = null;

	private String submitPage = null;
	private String ref = null;
	private String ref1 = null;

	private String addendum_id = null;
	private String back = null;
	private String authenticate = "";
	private String sitesearch = null;

	private String sitelatdeg = null;
	private String sitelatmin = null;
	private String sitelatdirection = null;
	private String sitelondeg = null;
	private String sitelonmin = null;
	private String sitelondirection = null;

	private String sitestatus = null;
	private String sitecategory = null;
	private String siteregion = null;
	private String sitegroup = null;
	private String siteendcustomer = null;
	private String sitetimezone = null;
	private Collection tempList = null;
	private String siteCountryId = null;
	private String siteCountryIdName = null;
	private String siteLatLon = null;
	private String refersh = null;

	private String sitelistid = null;
	private String siteid = null;
	private String fromType = null;

	private String siteName = null;
	private String siteZip = null;
	private String siteAddress = null;
	private String sitePrimaryPoc = null;
	private String siteSecondaryPoc = null;
	private String sitePrimaryPocEmail = null;
	private String siteSecondaryPocEmail = null;
	private String sitePrimaryPocPhone = null;
	private String siteSecondaryPocPhone = null;

	// end site information

	/* Financial Tab: Start */
	/* Labor Analysis */
	private ArrayList<CostAnalysis> costAnalysis = new ArrayList<CostAnalysis>();
	private String minutemanLaborTravel = null;
	private String speedpayLaborTravel = null;
	private String minutemanUsage = null;
	private String speedpayUsage = null;
	private String minutemanCostSavings = null;
	private String speedpayCostSavings = null;

	/* Revenue and VGPM */
	private String extendedPrice = null;
	private String jobDiscount = null;
	private String invoicePrice = null;
	private String jobReserver = null;
	private String proFormaVGPM = null;
	private String actualVGPM = null;
	private String appendixActualVGPM = null;

	/* Cost */
	private String totalActualCost = null;
	private String contractLaborEstimatedCost = null;
	private String contractLaborActualCost = null;
	private String contractMaterialEstimatedCost = null;
	private String contractMaterialActualCost = null;
	private String contractFreightEstimatedCost = null;
	private String contractFreightActualCost = null;
	private String contractTravelEstimatedCost = null;
	private String contractTravelActualCost = null;
	private String contractTotalEstimatedCost = null;
	private String contractTotalActualCost = null;
	private String corporateLaborEstimatedCost = null;
	private String corporateLaborActualCost = null;
	private String corporateMaterialEstimatedCost = null;
	private String corporateMaterialActualCost = null;
	private String corporateFreightEstimatedCost = null;
	private String corporateFreightActualCost = null;
	private String corporateTravelEstimatedCost = null;
	private String corporateTravelActualCost = null;
	private String corporateTotalEstimatedCost = null;
	private String corporateTotalActualCost = null;
	private String leadName = null;

	public String getLeadName() {
		return leadName;
	}

	public void setLeadName(String leadName) {
		this.leadName = leadName;
	}

	public String getLeadEmail() {
		return leadEmail;
	}

	public void setLeadEmail(String leadEmail) {
		this.leadEmail = leadEmail;
	}

	public String getLeadAddress() {
		return leadAddress;
	}

	public void setLeadAddress(String leadAddress) {
		this.leadAddress = leadAddress;
	}

	private String leadEmail = null;
	private String leadAddress = null;

	/* Financial Breakdown */
	private String chd = null;
	private String chl = null;
	private String chco = null;
	/* Financial Tab: End */

	/* Reschedule: Start */
	private String oldSchStart = null;
	private String rseType = null;
	private String rseReason = null;
	private String currentSchDate = null;
	private String reScheduleCount = null;
	// Add for Reschedule Internal Justification
	private String rseInternalJustification;
	// Added separate fields for rsecount fields(client,contingent,internal)
	private String reScheduleClientCount = null;
	private String reScheduleContingentCount = null;
	private String reScheduleInternalCount = null;
	/* Reschedule: End */

	/* Ticket Activity Summary */
	private String ticketActivityId = null;
	private String total_extended_price = null;
	private String actual_cost = null;
	private String vgpm = null;
	private String union_uplift_factor = null;
	private String expedite24_uplift_factor = null;
	private String expedite48_uplift_factor = null;
	private String non_pps_uplift_factor = null;
	private String international_uplift_factor = null;
	private ArrayList<TicketResourceSummary> ticketResourceSummary = new ArrayList<TicketResourceSummary>();

	/* Schedule Summary */
	private String timeOnSite = null;
	private String timeToRespond = null;
	private String criticalityActual = null;
	private String criticalitySuggested = null;

	private String isAddendumApproved = null;
	private String addendumStatus = null;

	private String isPOAssigned = null;
        
        private String requestorIds = null;
        private String additionalRecipientIds = null;

	/* Job Comments */
	private String latestCommentsDate = null;
	private String latestComments = null;
	private String countDispatch = null;

	/* Purchase Order List */
	private String[] customerDelCount;
	private String[] internalDelCount;
	private String[] totalDelCount;
	private String[] commissionPO;

	/* Invoice summary */
	private String invNumber = null;
	private String invDate = null;
	private String invAmount = null;
	private String invDiscount = null;
	private String invAge = null;
	private String leadTitle = null;
	private String pocPhone = null;
	private String pocEmail = null;

	public String getPocPhone() {
		return pocPhone;
	}

	public void setPocPhone(String pocPhone) {
		this.pocPhone = pocPhone;
	}

	public String getPocAddress() {
		return pocAddress;
	}

	public void setPocAddress(String pocAddress) {
		this.pocAddress = pocAddress;
	}

	public String getPocTitle() {
		return pocTitle;
	}

	public void setPocTitle(String pocTitle) {
		this.pocTitle = pocTitle;
	}

	private String pocAddress = null;
	private String pocTitle = null;

	/* Change Order */

	public String getLeadTitle() {
		return leadTitle;
	}

	public void setLeadTitle(String leadTitle) {
		this.leadTitle = leadTitle;
	}

	private String intiateDate = null;
	private String intiateMinute = null;
	private String intiateHour = null;
	private String minuteList = null;
	private String intiateIsAm = null;
	private String customerName = null;
	private String projectName = null;
	private String customerPOC = null;
	private String alternateCustomerPOC = null;
	private String clientEmailId = null;
	private String senderEmailId = null;
	private String senderSignatures = null;
	private String ccEmail = null;
	private String proposedChange = null;
	private String estimatedCost = null;
	private String estimatedScheduleImpact = null;
	private String typeOption = null;
	private String standardNotification = null;
	private String attachment = null;

	public String getInstallNoteCount() {
		return installNoteCount;
	}

	public void setInstallNoteCount(String installNoteCount) {
		this.installNoteCount = installNoteCount;
	}

	public String getLatestInstallNotesType() {
		return latestInstallNotesType;
	}

	public void setLatestInstallNotesType(String latestInstallNotesType) {
		this.latestInstallNotesType = latestInstallNotesType;
	}

	public String getSecondLatestInstallNotesType() {
		return secondLatestInstallNotesType;
	}

	public void setSecondLatestInstallNotesType(
			String secondLatestInstallNotesType) {
		this.secondLatestInstallNotesType = secondLatestInstallNotesType;
	}

	public String getIntiateDate() {
		return intiateDate;
	}

	public void setIntiateDate(String intiateDate) {
		this.intiateDate = intiateDate;
	}

	public String getIntiateMinute() {
		return intiateMinute;
	}

	public void setIntiateMinute(String intiateMinute) {
		this.intiateMinute = intiateMinute;
	}

	public String getIntiateHour() {
		return intiateHour;
	}

	public void setIntiateHour(String intiateHour) {
		this.intiateHour = intiateHour;
	}

	public String getMinuteList() {
		return minuteList;
	}

	public void setMinuteList(String minuteList) {
		this.minuteList = minuteList;
	}

	public String getIntiateIsAm() {
		return intiateIsAm;
	}

	public void setIntiateIsAm(String intiateIsAm) {
		this.intiateIsAm = intiateIsAm;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getCustomerPOC() {
		return customerPOC;
	}

	public void setCustomerPOC(String customerPOC) {
		this.customerPOC = customerPOC;
	}

	public String getAlternateCustomerPOC() {
		return alternateCustomerPOC;
	}

	public void setAlternateCustomerPOC(String alternateCustomerPOC) {
		this.alternateCustomerPOC = alternateCustomerPOC;
	}

	public String getClientEmailId() {
		return clientEmailId;
	}

	public void setClientEmailId(String clientEmailId) {
		this.clientEmailId = clientEmailId;
	}

	public String getSenderEmailId() {
		return senderEmailId;
	}

	public void setSenderEmailId(String senderEmailId) {
		this.senderEmailId = senderEmailId;
	}

	public String getSenderSignatures() {
		return senderSignatures;
	}

	public void setSenderSignatures(String senderSignatures) {
		this.senderSignatures = senderSignatures;
	}

	public String getCcEmail() {
		return ccEmail;
	}

	public void setCcEmail(String ccEmail) {
		this.ccEmail = ccEmail;
	}

	public String getProposedChange() {
		return proposedChange;
	}

	public void setProposedChange(String proposedChange) {
		this.proposedChange = proposedChange;
	}

	public String getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(String estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	public String getEstimatedScheduleImpact() {
		return estimatedScheduleImpact;
	}

	public void setEstimatedScheduleImpact(String estimatedScheduleImpact) {
		this.estimatedScheduleImpact = estimatedScheduleImpact;
	}

	public String getTypeOption() {
		return typeOption;
	}

	public void setTypeOption(String typeOption) {
		this.typeOption = typeOption;
	}

	public String getStandardNotification() {
		return standardNotification;
	}

	public void setStandardNotification(String standardNotification) {
		this.standardNotification = standardNotification;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getCountDispatch() {
		return countDispatch;
	}

	public void setCountDispatch(String countDispatch) {
		this.countDispatch = countDispatch;
	}

	public String getLatestComments() {
		return latestComments;
	}

	public void setLatestComments(String latestComments) {
		this.latestComments = latestComments;
	}

	public String getLatestCommentsDate() {
		return latestCommentsDate;
	}

	public void setLatestCommentsDate(String latestCommentsDate) {
		this.latestCommentsDate = latestCommentsDate;
	}

	public String getRseReason() {
		return rseReason;
	}

	public void setRseReason(String rseReason) {
		this.rseReason = rseReason;
	}

	public String getRseType() {
		return rseType;
	}

	public void setRseType(String rseType) {
		this.rseType = rseType;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public Object[] getAvailableActions() {
		return availableActions;
	}

	public void setAvailableActions(Object[] availableActions) {
		this.availableActions = availableActions;
	}

	public String getActionOnPO() {
		return actionOnPO;
	}

	public void setActionOnPO(String actionOnPO) {
		this.actionOnPO = actionOnPO;
	}

	public String getTotalEstimatedCost() {
		return totalEstimatedCost;
	}

	public void setTotalEstimatedCost(String totalEstimatedCost) {
		this.totalEstimatedCost = totalEstimatedCost;
	}

	public String getTotalEstimatedUnitCost() {
		return totalEstimatedUnitCost;
	}

	public void setTotalEstimatedUnitCost(String totalEstimatedUnitCost) {
		this.totalEstimatedUnitCost = totalEstimatedUnitCost;
	}

	public String getTotalExtendedPrice() {
		return totalExtendedPrice;
	}

	public void setTotalExtendedPrice(String totalExtendedPrice) {
		this.totalExtendedPrice = totalExtendedPrice;
	}

	public String getTotalExtendedUnitPrice() {
		return totalExtendedUnitPrice;
	}

	public void setTotalExtendedUnitPrice(String totalExtendedUnitPrice) {
		this.totalExtendedUnitPrice = totalExtendedUnitPrice;
	}

	public String getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(String totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	public String getSite_id() {
		return site_id;
	}

	public void setSite_id(String site_id) {
		this.site_id = site_id;
	}

	public String getTicket_id() {
		return ticket_id;
	}

	public void setTicket_id(String ticket_id) {
		this.ticket_id = ticket_id;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getAppendixType() {
		return appendixType;
	}

	public void setAppendixType(String appendixType) {
		this.appendixType = appendixType;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getHmode() {
		return hmode;
	}

	public void setHmode(String hmode) {
		this.hmode = hmode;
	}

	public String[] getActivity_Id() {
		return activity_Id;
	}

	public void setActivity_Id(String[] activity_Id) {
		this.activity_Id = activity_Id;
	}

	public String[] getActivitylibrary_Id() {
		return activitylibrary_Id;
	}

	public void setActivitylibrary_Id(String[] activitylibrary_Id) {
		this.activitylibrary_Id = activitylibrary_Id;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public ArrayList getSchedulers() {
		return schedulers;
	}

	public void setSchedulers(ArrayList schedulers) {
		this.schedulers = schedulers;
	}

	public String getSite_city() {
		return site_city;
	}

	public void setSite_city(String site_city) {
		this.site_city = site_city;
	}

	public String getSite_country() {
		return site_country;
	}

	public void setSite_country(String site_country) {
		this.site_country = site_country;
	}

	public String getSite_number() {
		return site_number;
	}

	public void setSite_number(String site_number) {
		this.site_number = site_number;
	}

	public String getSite_state() {
		return site_state;
	}

	public void setSite_state(String site_state) {
		this.site_state = site_state;
	}

	public boolean isUs() {
		return isUs;
	}

	public void setUs(boolean isUs) {
		this.isUs = isUs;
	}

	public String getCurrentOwner() {
		return currentOwner;
	}

	public void setCurrentOwner(String currentOwner) {
		this.currentOwner = currentOwner;
	}

	public String[] getActivityName() {
		return activityName;
	}

	public void setActivityName(String[] activityName) {
		this.activityName = activityName;
	}

	public String[] getQuantity() {
		return quantity;
	}

	public void setQuantity(String[] quantity) {
		this.quantity = quantity;
	}

	public String[] getScope() {
		return scope;
	}

	public void setScope(String[] scope) {
		this.scope = scope;
	}

	public String[] getUplifts() {
		return uplifts;
	}

	public void setUplifts(String[] uplifts) {
		this.uplifts = uplifts;
	}

	public String[] getEstimatedTotalCost() {
		return estimatedTotalCost;
	}

	public void setEstimatedTotalCost(String[] estimatedTotalCost) {
		this.estimatedTotalCost = estimatedTotalCost;
	}

	public String[] getEstimatedUnitcost() {
		return estimatedUnitcost;
	}

	public void setEstimatedUnitcost(String[] estimatedUnitcost) {
		this.estimatedUnitcost = estimatedUnitcost;
	}

	public String[] getExtendedTotalCost() {
		return extendedTotalCost;
	}

	public void setExtendedTotalCost(String[] extendedTotalCost) {
		this.extendedTotalCost = extendedTotalCost;
	}

	public String[] getExtendedUnitcost() {
		return extendedUnitcost;
	}

	public void setExtendedUnitcost(String[] extendedUnitcost) {
		this.extendedUnitcost = extendedUnitcost;
	}

	public String getCriticality() {
		return criticality;
	}

	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public String getProblemCategory() {
		return problemCategory;
	}

	public void setProblemCategory(String problemCategory) {
		this.problemCategory = problemCategory;
	}

	public String getProblemDescription() {
		return problemDescription;
	}

	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}

	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String getRequestorEmail() {
		return requestorEmail;
	}

	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getSpecialInstructions() {
		return specialInstructions;
	}

	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public String getTicketRules() {
		return ticketRules;
	}

	public void setTicketRules(String ticketRules) {
		this.ticketRules = ticketRules;
	}

	public String getPreferredDate() {
		return preferredDate;
	}

	public void setPreferredDate(String preferredDate) {
		this.preferredDate = preferredDate;
	}

	public String getPreferredHour() {
		return preferredHour;
	}

	public void setPreferredHour(String preferredHour) {
		this.preferredHour = preferredHour;
	}

	public String getPreferredMinute() {
		return preferredMinute;
	}

	public void setPreferredMinute(String preferredMinute) {
		this.preferredMinute = preferredMinute;
	}

	public String getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}

	public String getReceivedHour() {
		return receivedHour;
	}

	public void setReceivedHour(String receivedHour) {
		this.receivedHour = receivedHour;
	}

	public String getReceivedMinute() {
		return receivedMinute;
	}

	public void setReceivedMinute(String receivedMinute) {
		this.receivedMinute = receivedMinute;
	}

	public String getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
	}

	public String getRequestedHour() {
		return requestedHour;
	}

	public void setRequestedHour(String requestedHour) {
		this.requestedHour = requestedHour;
	}

	public String getRequestedMinute() {
		return requestedMinute;
	}

	public void setRequestedMinute(String requestedMinute) {
		this.requestedMinute = requestedMinute;
	}

	public String getWindowFromDate() {
		return windowFromDate;
	}

	public void setWindowFromDate(String windowFromDate) {
		this.windowFromDate = windowFromDate;
	}

	public String getWindowFromHour() {
		return windowFromHour;
	}

	public void setWindowFromHour(String windowFromHour) {
		this.windowFromHour = windowFromHour;
	}

	public String getWindowFromMinute() {
		return windowFromMinute;
	}

	public void setWindowFromMinute(String windowFromMinute) {
		this.windowFromMinute = windowFromMinute;
	}

	public String getWindowToDate() {
		return windowToDate;
	}

	public void setWindowToDate(String windowToDate) {
		this.windowToDate = windowToDate;
	}

	public String getWindowToHour() {
		return windowToHour;
	}

	public void setWindowToHour(String windowToHour) {
		this.windowToHour = windowToHour;
	}

	public String getWindowToMinute() {
		return windowToMinute;
	}

	public void setWindowToMinute(String windowToMinute) {
		this.windowToMinute = windowToMinute;
	}

	public String[] getJobWorkflowItemDescription() {
		return jobWorkflowItemDescription;
	}

	public void setJobWorkflowItemDescription(
			String[] jobWorkflowItemDescription) {
		this.jobWorkflowItemDescription = jobWorkflowItemDescription;
	}

	public long[] getJobWorkflowItemId() {
		return jobWorkflowItemId;
	}

	public void setJobWorkflowItemId(long[] jobWorkflowItemId) {
		this.jobWorkflowItemId = jobWorkflowItemId;
	}

	public String[] getJobWorkflowItemReferenceDate() {
		return jobWorkflowItemReferenceDate;
	}

	public void setJobWorkflowItemReferenceDate(
			String[] jobWorkflowItemReferenceDate) {
		this.jobWorkflowItemReferenceDate = jobWorkflowItemReferenceDate;
	}

	public String[] getJobWorkflowItemSequence() {
		return jobWorkflowItemSequence;
	}

	public void setJobWorkflowItemSequence(String[] jobWorkflowItemSequence) {
		this.jobWorkflowItemSequence = jobWorkflowItemSequence;
	}

	public String[] getJobWorkflowItemStatus() {
		return jobWorkflowItemStatus;
	}

	public void setJobWorkflowItemStatus(String[] jobWorkflowItemStatus) {
		this.jobWorkflowItemStatus = jobWorkflowItemStatus;
	}

	public String[] getJobWorkflowItemUser() {
		return jobWorkflowItemUser;
	}

	public void setJobWorkflowItemUser(String[] jobWorkflowItemUser) {
		this.jobWorkflowItemUser = jobWorkflowItemUser;
	}

	public ArrayList<LabelValue> getCriticalityList() {
		return criticalityList;
	}

	public void setCriticalityList(ArrayList<LabelValue> criticalityList) {
		this.criticalityList = criticalityList;
	}

	public ArrayList<LabelValue> getProblemCategoriesList() {
		return problemCategoriesList;
	}

	public void setProblemCategoriesList(
			ArrayList<LabelValue> problemCategoriesList) {
		this.problemCategoriesList = problemCategoriesList;
	}

	public ArrayList<LabelValue> getRequestorList() {
		return requestorList;
	}

	public void setRequestorList(ArrayList<LabelValue> requestorList) {
		this.requestorList = requestorList;
	}

	public ArrayList<LabelValue> getRequestTypeList() {
		return requestTypeList;
	}

	public void setRequestTypeList(ArrayList<LabelValue> requestTypeList) {
		this.requestTypeList = requestTypeList;
	}

	public ArrayList<LabelValue> getResourceList() {
		return resourceList;
	}

	public void setResourceList(ArrayList<LabelValue> resourceList) {
		this.resourceList = resourceList;
	}

	public String getHelpDesk() {
		return helpDesk;
	}

	public void setHelpDesk(String helpDesk) {
		this.helpDesk = helpDesk;
	}

	public String getMsp() {
		return msp;
	}

	public void setMsp(String msp) {
		this.msp = msp;
	}

	public String getPps() {
		return pps;
	}

	public void setPps(String pps) {
		this.pps = pps;
	}

	public String getStandBy() {
		return standBy;
	}

	public void setStandBy(String standBy) {
		this.standBy = standBy;
	}

	public ArrayList<LabelValue> getPreferredHourList() {
		return preferredHourList;
	}

	public void setPreferredHourList(ArrayList<LabelValue> preferredHourList) {
		this.preferredHourList = preferredHourList;
	}

	public ArrayList<LabelValue> getPreferredMinuteList() {
		return preferredMinuteList;
	}

	public void setPreferredMinuteList(ArrayList<LabelValue> preferredMinuteList) {
		this.preferredMinuteList = preferredMinuteList;
	}

	public ArrayList<LabelValue> getRequestedHourList() {
		return requestedHourList;
	}

	public void setRequestedHourList(ArrayList<LabelValue> requestedHourList) {
		this.requestedHourList = requestedHourList;
	}

	public ArrayList<LabelValue> getRequestedMinuteList() {
		return requestedMinuteList;
	}

	public void setRequestedMinuteList(ArrayList<LabelValue> requestedMinuteList) {
		this.requestedMinuteList = requestedMinuteList;
	}

	public ArrayList<LabelValue> getWindowFromHourList() {
		return windowFromHourList;
	}

	public void setWindowFromHourList(ArrayList<LabelValue> windowFromHourList) {
		this.windowFromHourList = windowFromHourList;
	}

	public ArrayList<LabelValue> getWindowFromMinuteList() {
		return windowFromMinuteList;
	}

	public void setWindowFromMinuteList(
			ArrayList<LabelValue> windowFromMinuteList) {
		this.windowFromMinuteList = windowFromMinuteList;
	}

	public ArrayList<LabelValue> getWindowToHourList() {
		return windowToHourList;
	}

	public void setWindowToHourList(ArrayList<LabelValue> windowToHourList) {
		this.windowToHourList = windowToHourList;
	}

	public ArrayList<LabelValue> getWindowToMinuteList() {
		return windowToMinuteList;
	}

	public void setWindowToMinuteList(ArrayList<LabelValue> windowToMinuteList) {
		this.windowToMinuteList = windowToMinuteList;
	}

	public String getPreferredIsAm() {
		return preferredIsAm;
	}

	public void setPreferredIsAm(String preferredIsAm) {
		this.preferredIsAm = preferredIsAm;
	}

	public String getReceivedIsAm() {
		return receivedIsAm;
	}

	public void setReceivedIsAm(String receivedIsAm) {
		this.receivedIsAm = receivedIsAm;
	}

	public String getRequestedIsAm() {
		return requestedIsAm;
	}

	public void setRequestedIsAm(String requestedIsAm) {
		this.requestedIsAm = requestedIsAm;
	}

	public String getWindowFromIsAm() {
		return windowFromIsAm;
	}

	public void setWindowFromIsAm(String windowFromIsAm) {
		this.windowFromIsAm = windowFromIsAm;
	}

	public String getWindowToIsAm() {
		return windowToIsAm;
	}

	public void setWindowToIsAm(String windowToIsAm) {
		this.windowToIsAm = windowToIsAm;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public void setAuthorizedCost(String[] authorizedCost) {
		this.authorizedCost = authorizedCost;
	}

	public void setCancelledPONumber(String[] cancelledPONumber) {
		this.cancelledPONumber = cancelledPONumber;
	}

	public void setCostingType(String[] costingType) {
		this.costingType = costingType;
	}

	public void setPartnerContact(String[] partnerContact) {
		this.partnerContact = partnerContact;
	}

	public void setPoNumber(String[] poNumber) {
		this.poNumber = poNumber;
	}

	public void setPreviousStatus(String[] previousStatus) {
		this.previousStatus = previousStatus;
	}

	public void setRevision(String[] revision) {
		this.revision = revision;
	}

	public void setStatus(String[] status) {
		this.status = status;
	}

	public void setWoNumber(String[] woNumber) {
		this.woNumber = woNumber;
	}

	public String[] getAuthorizedCost() {
		return authorizedCost;
	}

	public String[] getCancelledPONumber() {
		return cancelledPONumber;
	}

	public String[] getCostingType() {
		return costingType;
	}

	public String[] getPartnerContact() {
		return partnerContact;
	}

	public String[] getPoNumber() {
		return poNumber;
	}

	public String[] getPreviousStatus() {
		return previousStatus;
	}

	public String[] getRevision() {
		return revision;
	}

	public String[] getStatus() {
		return status;
	}

	public String[] getWoNumber() {
		return woNumber;
	}

	public String getMinutemanTypeCLC() {
		return minutemanTypeCLC;
	}

	public void setMinutemanTypeCLC(String minutemanTypeCLC) {
		this.minutemanTypeCLC = minutemanTypeCLC;
	}

	public String getMinutemanTypeTC() {
		return minutemanTypeTC;
	}

	public void setMinutemanTypeTC(String minutemanTypeTC) {
		this.minutemanTypeTC = minutemanTypeTC;
	}

	public String getMinutemanTypeTHC() {
		return minutemanTypeTHC;
	}

	public void setMinutemanTypeTHC(String minutemanTypeTHC) {
		this.minutemanTypeTHC = minutemanTypeTHC;
	}

	public String getPVSTypeCLC() {
		return PVSTypeCLC;
	}

	public void setPVSTypeCLC(String typeCLC) {
		PVSTypeCLC = typeCLC;
	}

	public String getPVSTypeTC() {
		return PVSTypeTC;
	}

	public void setPVSTypeTC(String typeTC) {
		PVSTypeTC = typeTC;
	}

	public String getPVSTypeTHC() {
		return PVSTypeTHC;
	}

	public void setPVSTypeTHC(String typeTHC) {
		PVSTypeTHC = typeTHC;
	}

	public String getSpeedpayTypeCLC() {
		return speedpayTypeCLC;
	}

	public void setSpeedpayTypeCLC(String speedpayTypeCLC) {
		this.speedpayTypeCLC = speedpayTypeCLC;
	}

	public String getSpeedpayTypeTC() {
		return speedpayTypeTC;
	}

	public void setSpeedpayTypeTC(String speedpayTypeTC) {
		this.speedpayTypeTC = speedpayTypeTC;
	}

	public String getSpeedpayTypeTHC() {
		return speedpayTypeTHC;
	}

	public void setSpeedpayTypeTHC(String speedpayTypeTHC) {
		this.speedpayTypeTHC = speedpayTypeTHC;
	}

	public String getMinutemanTypeCostSavings() {
		return minutemanTypeCostSavings;
	}

	public void setMinutemanTypeCostSavings(String minutemanTypeCostSavings) {
		this.minutemanTypeCostSavings = minutemanTypeCostSavings;
	}

	public String getPVSTypeCostSavings() {
		return PVSTypeCostSavings;
	}

	public void setPVSTypeCostSavings(String typeCostSavings) {
		PVSTypeCostSavings = typeCostSavings;
	}

	public String getSpeedpayTypeCostSavings() {
		return speedpayTypeCostSavings;
	}

	public void setSpeedpayTypeCostSavings(String speedpayTypeCostSavings) {
		this.speedpayTypeCostSavings = speedpayTypeCostSavings;
	}

	public String getShowCustomerReference() {
		return showCustomerReference;
	}

	public void setShowCustomerReference(String showCustomerReference) {
		this.showCustomerReference = showCustomerReference;
	}

	public String[] getPoId() {
		return poId;
	}

	public void setPoId(String[] poId) {
		this.poId = poId;
	}

	public String getIsClicked() {
		return isClicked;
	}

	public void setIsClicked(String isClicked) {
		this.isClicked = isClicked;
	}

	public String getMapView() {
		return mapView;
	}

	public void setMapView(String mapView) {
		this.mapView = mapView;
	}

	public String getLatestInstallNotes() {
		return latestInstallNotes;
	}

	public void setLatestInstallNotes(String latestInstallNotes) {
		this.latestInstallNotes = latestInstallNotes;
	}

	public String getOffsiteDate() {
		return offsiteDate;
	}

	public void setOffsiteDate(String offsiteDate) {
		this.offsiteDate = offsiteDate;
	}

	public String getOffsiteHour() {
		return offsiteHour;
	}

	public void setOffsiteHour(String offsiteHour) {
		this.offsiteHour = offsiteHour;
	}

	public String getOffsiteIsAm() {
		return offsiteIsAm;
	}

	public void setOffsiteIsAm(String offsiteIsAm) {
		this.offsiteIsAm = offsiteIsAm;
	}

	public String getOffsiteMinute() {
		return offsiteMinute;
	}

	public void setOffsiteMinute(String offsiteMinute) {
		this.offsiteMinute = offsiteMinute;
	}

	public String getOnsiteDate() {
		return onsiteDate;
	}

	public void setOnsiteDate(String onsiteDate) {
		this.onsiteDate = onsiteDate;
	}

	public String getOnsiteHour() {
		return onsiteHour;
	}

	public void setOnsiteHour(String onsiteHour) {
		this.onsiteHour = onsiteHour;
	}

	public String getOnsiteIsAm() {
		return onsiteIsAm;
	}

	public void setOnsiteIsAm(String onsiteIsAm) {
		this.onsiteIsAm = onsiteIsAm;
	}

	public String getOnsiteMinute() {
		return onsiteMinute;
	}

	public void setOnsiteMinute(String onsiteMinute) {
		this.onsiteMinute = onsiteMinute;
	}

	public String getSelection() {
		return selection;
	}

	public void setSelection(String selection) {
		this.selection = selection;
	}

	public String getScheduleStartDate() {
		return scheduleStartDate;
	}

	public void setScheduleStartDate(String scheduleStartDate) {
		this.scheduleStartDate = scheduleStartDate;
	}

	public String getScheduleStartHour() {
		return scheduleStartHour;
	}

	public void setScheduleStartHour(String scheduleStartHour) {
		this.scheduleStartHour = scheduleStartHour;
	}

	public String getScheduleStartIsAm() {
		return scheduleStartIsAm;
	}

	public void setScheduleStartIsAm(String scheduleStartIsAm) {
		this.scheduleStartIsAm = scheduleStartIsAm;
	}

	public String getScheduleStartMinute() {
		return scheduleStartMinute;
	}

	public void setScheduleStartMinute(String scheduleStartMinute) {
		this.scheduleStartMinute = scheduleStartMinute;
	}

	public String getScheduleEndDate() {
		return scheduleEndDate;
	}

	public void setScheduleEndDate(String scheduleEndDate) {
		this.scheduleEndDate = scheduleEndDate;
	}

	public String getScheduleEndHour() {
		return scheduleEndHour;
	}

	public void setScheduleEndHour(String scheduleEndHour) {
		this.scheduleEndHour = scheduleEndHour;
	}

	public String getScheduleEndIsAm() {
		return scheduleEndIsAm;
	}

	public void setScheduleEndIsAm(String scheduleEndIsAm) {
		this.scheduleEndIsAm = scheduleEndIsAm;
	}

	public String getScheduleEndMinute() {
		return scheduleEndMinute;
	}

	public void setScheduleEndMinute(String scheduleEndMinute) {
		this.scheduleEndMinute = scheduleEndMinute;
	}

	public String[] getMasterPOItemType() {
		return masterPOItemType;
	}

	public void setMasterPOItemType(String[] masterPOItemType) {
		this.masterPOItemType = masterPOItemType;
	}

	public String getAddendum_id() {
		return addendum_id;
	}

	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}

	public String getAppendixname() {
		return appendixname;
	}

	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}

	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	public String getBack() {
		return back;
	}

	public void setBack(String back) {
		this.back = back;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getFromType() {
		return fromType;
	}

	public void setFromType(String fromType) {
		this.fromType = fromType;
	}

	public String getId1() {
		return id1;
	}

	public void setId1(String id1) {
		this.id1 = id1;
	}

	public String getId2() {
		return id2;
	}

	public void setId2(String id2) {
		this.id2 = id2;
	}

	public String getLocality_uplift() {
		return locality_uplift;
	}

	public void setLocality_uplift(String locality_uplift) {
		this.locality_uplift = locality_uplift;
	}

	public String getPrimary_email() {
		return primary_email;
	}

	public void setPrimary_email(String primary_email) {
		this.primary_email = primary_email;
	}

	public String getPrimary_Name() {
		return primary_Name;
	}

	public void setPrimary_Name(String primary_Name) {
		this.primary_Name = primary_Name;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getRef1() {
		return ref1;
	}

	public void setRef1(String ref1) {
		this.ref1 = ref1;
	}

	public String getRefersh() {
		return refersh;
	}

	public void setRefersh(String refersh) {
		this.refersh = refersh;
	}

	public String getSecondary_email() {
		return secondary_email;
	}

	public void setSecondary_email(String secondary_email) {
		this.secondary_email = secondary_email;
	}

	public String getSecondary_Name() {
		return secondary_Name;
	}

	public void setSecondary_Name(String secondary_Name) {
		this.secondary_Name = secondary_Name;
	}

	public String getSecondary_phone() {
		return secondary_phone;
	}

	public void setSecondary_phone(String secondary_phone) {
		this.secondary_phone = secondary_phone;
	}

	public String getSite_address() {
		return site_address;
	}

	public void setSite_address(String site_address) {
		this.site_address = site_address;
	}

	public String getSite_designator() {
		return site_designator;
	}

	public void setSite_designator(String site_designator) {
		this.site_designator = site_designator;
	}

	public String getSite_direction() {
		return site_direction;
	}

	public void setSite_direction(String site_direction) {
		this.site_direction = site_direction;
	}

	public String getSite_Id() {
		return site_Id;
	}

	public void setSite_Id(String site_Id) {
		this.site_Id = site_Id;
	}

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public String getSite_notes() {
		return site_notes;
	}

	public void setSite_notes(String site_notes) {
		this.site_notes = site_notes;
	}

	public String getSite_phone() {
		return site_phone;
	}

	public void setSite_phone(String site_phone) {
		this.site_phone = site_phone;
	}

	public String getSite_worklocation() {
		return site_worklocation;
	}

	public void setSite_worklocation(String site_worklocation) {
		this.site_worklocation = site_worklocation;
	}

	public String getSite_zipcode() {
		return site_zipcode;
	}

	public void setSite_zipcode(String site_zipcode) {
		this.site_zipcode = site_zipcode;
	}

	public String getSitecategory() {
		return sitecategory;
	}

	public void setSitecategory(String sitecategory) {
		this.sitecategory = sitecategory;
	}

	public String getSiteCountryId() {
		return siteCountryId;
	}

	public void setSiteCountryId(String siteCountryId) {
		this.siteCountryId = siteCountryId;
	}

	public String getSiteCountryIdName() {
		return siteCountryIdName;
	}

	public void setSiteCountryIdName(String siteCountryIdName) {
		this.siteCountryIdName = siteCountryIdName;
	}

	public String getSiteendcustomer() {
		return siteendcustomer;
	}

	public void setSiteendcustomer(String siteendcustomer) {
		this.siteendcustomer = siteendcustomer;
	}

	public String getSitegroup() {
		return sitegroup;
	}

	public void setSitegroup(String sitegroup) {
		this.sitegroup = sitegroup;
	}

	public String getSiteid() {
		return siteid;
	}

	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}

	public String getSitelatdeg() {
		return sitelatdeg;
	}

	public void setSitelatdeg(String sitelatdeg) {
		this.sitelatdeg = sitelatdeg;
	}

	public String getSitelatdirection() {
		return sitelatdirection;
	}

	public void setSitelatdirection(String sitelatdirection) {
		this.sitelatdirection = sitelatdirection;
	}

	public String getSiteLatLon() {
		return siteLatLon;
	}

	public void setSiteLatLon(String siteLatLon) {
		this.siteLatLon = siteLatLon;
	}

	public String getSitelatmin() {
		return sitelatmin;
	}

	public void setSitelatmin(String sitelatmin) {
		this.sitelatmin = sitelatmin;
	}

	public String getSitelistid() {
		return sitelistid;
	}

	public void setSitelistid(String sitelistid) {
		this.sitelistid = sitelistid;
	}

	public String getSitelondeg() {
		return sitelondeg;
	}

	public void setSitelondeg(String sitelondeg) {
		this.sitelondeg = sitelondeg;
	}

	public String getSitelondirection() {
		return sitelondirection;
	}

	public void setSitelondirection(String sitelondirection) {
		this.sitelondirection = sitelondirection;
	}

	public String getSitelonmin() {
		return sitelonmin;
	}

	public void setSitelonmin(String sitelonmin) {
		this.sitelonmin = sitelonmin;
	}

	public String getSiteregion() {
		return siteregion;
	}

	public void setSiteregion(String siteregion) {
		this.siteregion = siteregion;
	}

	public String getSitesearch() {
		return sitesearch;
	}

	public void setSitesearch(String sitesearch) {
		this.sitesearch = sitesearch;
	}

	public String getSitestatus() {
		return sitestatus;
	}

	public void setSitestatus(String sitestatus) {
		this.sitestatus = sitestatus;
	}

	public String getSitetimezone() {
		return sitetimezone;
	}

	public void setSitetimezone(String sitetimezone) {
		this.sitetimezone = sitetimezone;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateSelect() {
		return stateSelect;
	}

	public void setStateSelect(String stateSelect) {
		this.stateSelect = stateSelect;
	}

	public String getSubmitPage() {
		return submitPage;
	}

	public void setSubmitPage(String submitPage) {
		this.submitPage = submitPage;
	}

	public Collection getTempList() {
		return tempList;
	}

	public void setTempList(Collection tempList) {
		this.tempList = tempList;
	}

	public String getUnion_site() {
		return union_site;
	}

	public void setUnion_site(String union_site) {
		this.union_site = union_site;
	}

	public String getOptactivity() {
		return optactivity;
	}

	public void setOptactivity(String optactivity) {
		this.optactivity = optactivity;
	}

	public String[] getActivity_oos_flag() {
		return activity_oos_flag;
	}

	public void setActivity_oos_flag(String[] activity_oos_flag) {
		this.activity_oos_flag = activity_oos_flag;
	}

	public String getSitePrimaryPoc() {
		return sitePrimaryPoc;
	}

	public void setSitePrimaryPoc(String primaryPoc) {
		this.sitePrimaryPoc = primaryPoc;
	}

	public String getSitePrimaryPocEmail() {
		return sitePrimaryPocEmail;
	}

	public void setSitePrimaryPocEmail(String primaryPocEmail) {
		this.sitePrimaryPocEmail = primaryPocEmail;
	}

	public String getSiteSecondaryPoc() {
		return siteSecondaryPoc;
	}

	public void setSiteSecondaryPoc(String secondaryPoc) {
		this.siteSecondaryPoc = secondaryPoc;
	}

	public String getSiteSecondaryPocEmail() {
		return siteSecondaryPocEmail;
	}

	public void setSiteSecondaryPocEmail(String secondaryPocEmail) {
		this.siteSecondaryPocEmail = secondaryPocEmail;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSitePrimaryPocPhone() {
		return sitePrimaryPocPhone;
	}

	public void setSitePrimaryPocPhone(String sitePrimaryPocPhone) {
		this.sitePrimaryPocPhone = sitePrimaryPocPhone;
	}

	public String getSiteSecondaryPocPhone() {
		return siteSecondaryPocPhone;
	}

	public void setSiteSecondaryPocPhone(String siteSecondaryPocPhone) {
		this.siteSecondaryPocPhone = siteSecondaryPocPhone;
	}

	public String getSiteZip() {
		return siteZip;
	}

	public void setSiteZip(String siteZip) {
		this.siteZip = siteZip;
	}

	public String[] getActivity_lx_ce_type() {
		return activity_lx_ce_type;
	}

	public void setActivity_lx_ce_type(String[] activity_lx_ce_type) {
		this.activity_lx_ce_type = activity_lx_ce_type;
	}

	public String getAllItemStatus() {
		return allItemStatus;
	}

	public void setAllItemStatus(String allItemStatus) {
		this.allItemStatus = allItemStatus;
	}

	public String getItemListSize() {
		return itemListSize;
	}

	public void setItemListSize(String itemListSize) {
		this.itemListSize = itemListSize;
	}

	public String getLatestInstallNotesDate() {
		return latestInstallNotesDate;
	}

	public void setLatestInstallNotesDate(String latestInstallNotesDate) {
		this.latestInstallNotesDate = latestInstallNotesDate;
	}

	public String getSecondLatestInstallNotes() {
		return secondLatestInstallNotes;
	}

	public void setSecondLatestInstallNotes(String secondLatestInstallNotes) {
		this.secondLatestInstallNotes = secondLatestInstallNotes;
	}

	public String getSecondLatestInstallNotesDate() {
		return secondLatestInstallNotesDate;
	}

	public void setSecondLatestInstallNotesDate(
			String secondLatestInstallNotesDate) {
		this.secondLatestInstallNotesDate = secondLatestInstallNotesDate;
	}

	public String getActualVGPM() {
		return actualVGPM;
	}

	public void setActualVGPM(String actualVGPM) {
		this.actualVGPM = actualVGPM;
	}

	public String getAppendixActualVGPM() {
		return appendixActualVGPM;
	}

	public void setAppendixActualVGPM(String appendixActualVGPM) {
		this.appendixActualVGPM = appendixActualVGPM;
	}

	public String getContractFreightActualCost() {
		return contractFreightActualCost;
	}

	public void setContractFreightActualCost(String contractFreightActualCost) {
		this.contractFreightActualCost = contractFreightActualCost;
	}

	public String getContractFreightEstimatedCost() {
		return contractFreightEstimatedCost;
	}

	public void setContractFreightEstimatedCost(
			String contractFreightEstimatedCost) {
		this.contractFreightEstimatedCost = contractFreightEstimatedCost;
	}

	public String getContractLaborActualCost() {
		return contractLaborActualCost;
	}

	public void setContractLaborActualCost(String contractLaborActualCost) {
		this.contractLaborActualCost = contractLaborActualCost;
	}

	public String getContractLaborEstimatedCost() {
		return contractLaborEstimatedCost;
	}

	public void setContractLaborEstimatedCost(String contractLaborEstimatedCost) {
		this.contractLaborEstimatedCost = contractLaborEstimatedCost;
	}

	public String getContractMaterialActualCost() {
		return contractMaterialActualCost;
	}

	public void setContractMaterialActualCost(String contractMaterialActualCost) {
		this.contractMaterialActualCost = contractMaterialActualCost;
	}

	public String getContractMaterialEstimatedCost() {
		return contractMaterialEstimatedCost;
	}

	public void setContractMaterialEstimatedCost(
			String contractMaterialEstimatedCost) {
		this.contractMaterialEstimatedCost = contractMaterialEstimatedCost;
	}

	public String getContractTotalActualCost() {
		return contractTotalActualCost;
	}

	public void setContractTotalActualCost(String contractTotalActualCost) {
		this.contractTotalActualCost = contractTotalActualCost;
	}

	public String getContractTotalEstimatedCost() {
		return contractTotalEstimatedCost;
	}

	public void setContractTotalEstimatedCost(String contractTotalEstimatedCost) {
		this.contractTotalEstimatedCost = contractTotalEstimatedCost;
	}

	public String getContractTravelActualCost() {
		return contractTravelActualCost;
	}

	public void setContractTravelActualCost(String contractTravelActualCost) {
		this.contractTravelActualCost = contractTravelActualCost;
	}

	public String getContractTravelEstimatedCost() {
		return contractTravelEstimatedCost;
	}

	public void setContractTravelEstimatedCost(
			String contractTravelEstimatedCost) {
		this.contractTravelEstimatedCost = contractTravelEstimatedCost;
	}

	public String getCorporateFreightActualCost() {
		return corporateFreightActualCost;
	}

	public void setCorporateFreightActualCost(String corporateFreightActualCost) {
		this.corporateFreightActualCost = corporateFreightActualCost;
	}

	public String getCorporateFreightEstimatedCost() {
		return corporateFreightEstimatedCost;
	}

	public void setCorporateFreightEstimatedCost(
			String corporateFreightEstimatedCost) {
		this.corporateFreightEstimatedCost = corporateFreightEstimatedCost;
	}

	public String getCorporateLaborActualCost() {
		return corporateLaborActualCost;
	}

	public void setCorporateLaborActualCost(String corporateLaborActualCost) {
		this.corporateLaborActualCost = corporateLaborActualCost;
	}

	public String getCorporateLaborEstimatedCost() {
		return corporateLaborEstimatedCost;
	}

	public void setCorporateLaborEstimatedCost(
			String corporateLaborEstimatedCost) {
		this.corporateLaborEstimatedCost = corporateLaborEstimatedCost;
	}

	public String getCorporateMaterialActualCost() {
		return corporateMaterialActualCost;
	}

	public void setCorporateMaterialActualCost(
			String corporateMaterialActualCost) {
		this.corporateMaterialActualCost = corporateMaterialActualCost;
	}

	public String getCorporateMaterialEstimatedCost() {
		return corporateMaterialEstimatedCost;
	}

	public void setCorporateMaterialEstimatedCost(
			String corporateMaterialEstimatedCost) {
		this.corporateMaterialEstimatedCost = corporateMaterialEstimatedCost;
	}

	public String getCorporateTotalActualCost() {
		return corporateTotalActualCost;
	}

	public void setCorporateTotalActualCost(String corporateTotalActualCost) {
		this.corporateTotalActualCost = corporateTotalActualCost;
	}

	public String getCorporateTotalEstimatedCost() {
		return corporateTotalEstimatedCost;
	}

	public void setCorporateTotalEstimatedCost(
			String corporateTotalEstimatedCost) {
		this.corporateTotalEstimatedCost = corporateTotalEstimatedCost;
	}

	public String getCorporateTravelActualCost() {
		return corporateTravelActualCost;
	}

	public void setCorporateTravelActualCost(String corporateTravelActualCost) {
		this.corporateTravelActualCost = corporateTravelActualCost;
	}

	public String getCorporateTravelEstimatedCost() {
		return corporateTravelEstimatedCost;
	}

	public void setCorporateTravelEstimatedCost(
			String corporateTravelEstimatedCost) {
		this.corporateTravelEstimatedCost = corporateTravelEstimatedCost;
	}

	public ArrayList<CostAnalysis> getCostAnalysis() {
		return costAnalysis;
	}

	public void setCostAnalysis(ArrayList<CostAnalysis> costAnalysis) {
		this.costAnalysis = costAnalysis;
	}

	public String getExtendedPrice() {
		return extendedPrice;
	}

	public void setExtendedPrice(String extendedPrice) {
		this.extendedPrice = extendedPrice;
	}

	public String getInvoicePrice() {
		return invoicePrice;
	}

	public void setInvoicePrice(String invoicePrice) {
		this.invoicePrice = invoicePrice;
	}

	public String getJobDiscount() {
		return jobDiscount;
	}

	public void setJobDiscount(String jobDiscount) {
		this.jobDiscount = jobDiscount;
	}

	public String getJobReserver() {
		return jobReserver;
	}

	public void setJobReserver(String jobReserver) {
		this.jobReserver = jobReserver;
	}

	public String getMinutemanCostSavings() {
		return minutemanCostSavings;
	}

	public void setMinutemanCostSavings(String minutemanCostSavings) {
		this.minutemanCostSavings = minutemanCostSavings;
	}

	public String getMinutemanLaborTravel() {
		return minutemanLaborTravel;
	}

	public void setMinutemanLaborTravel(String minutemanLaborTravel) {
		this.minutemanLaborTravel = minutemanLaborTravel;
	}

	public String getMinutemanUsage() {
		return minutemanUsage;
	}

	public void setMinutemanUsage(String minutemanUsage) {
		this.minutemanUsage = minutemanUsage;
	}

	public String getProFormaVGPM() {
		return proFormaVGPM;
	}

	public void setProFormaVGPM(String proFormaVGPM) {
		this.proFormaVGPM = proFormaVGPM;
	}

	public String getSpeedpayCostSavings() {
		return speedpayCostSavings;
	}

	public void setSpeedpayCostSavings(String speedpayCostSavings) {
		this.speedpayCostSavings = speedpayCostSavings;
	}

	public String getSpeedpayLaborTravel() {
		return speedpayLaborTravel;
	}

	public void setSpeedpayLaborTravel(String speedpayLaborTravel) {
		this.speedpayLaborTravel = speedpayLaborTravel;
	}

	public String getSpeedpayUsage() {
		return speedpayUsage;
	}

	public void setSpeedpayUsage(String speedpayUsage) {
		this.speedpayUsage = speedpayUsage;
	}

	public String getTotalActualCost() {
		return totalActualCost;
	}

	public void setTotalActualCost(String totalActualCost) {
		this.totalActualCost = totalActualCost;
	}

	public String getChco() {
		return chco;
	}

	public void setChco(String chco) {
		this.chco = chco;
	}

	public String getChd() {
		return chd;
	}

	public void setChd(String chd) {
		this.chd = chd;
	}

	public String getChl() {
		return chl;
	}

	public void setChl(String chl) {
		this.chl = chl;
	}

	public String getOldSchStart() {
		return oldSchStart;
	}

	public void setOldSchStart(String oldSchStart) {
		this.oldSchStart = oldSchStart;
	}

	public String getCurrentSchDate() {
		return currentSchDate;
	}

	public void setCurrentSchDate(String currentSchDate) {
		this.currentSchDate = currentSchDate;
	}

	public String getReScheduleCount() {
		return reScheduleCount;
	}

	public void setReScheduleCount(String reScheduleCount) {
		this.reScheduleCount = reScheduleCount;
	}

	public String getActual_cost() {
		return actual_cost;
	}

	public void setActual_cost(String actual_cost) {
		this.actual_cost = actual_cost;
	}

	public String getExpedite24_uplift_factor() {
		return expedite24_uplift_factor;
	}

	public void setExpedite24_uplift_factor(String expedite24_uplift_factor) {
		this.expedite24_uplift_factor = expedite24_uplift_factor;
	}

	public String getExpedite48_uplift_factor() {
		return expedite48_uplift_factor;
	}

	public void setExpedite48_uplift_factor(String expedite48_uplift_factor) {
		this.expedite48_uplift_factor = expedite48_uplift_factor;
	}

	public String getInternational_uplift_factor() {
		return international_uplift_factor;
	}

	public void setInternational_uplift_factor(
			String international_uplift_factor) {
		this.international_uplift_factor = international_uplift_factor;
	}

	public String getNon_pps_uplift_factor() {
		return non_pps_uplift_factor;
	}

	public void setNon_pps_uplift_factor(String non_pps_uplift_factor) {
		this.non_pps_uplift_factor = non_pps_uplift_factor;
	}

	public ArrayList<TicketResourceSummary> getTicketResourceSummary() {
		return ticketResourceSummary;
	}

	public void setTicketResourceSummary(
			ArrayList<TicketResourceSummary> ticketResourceSummary) {
		this.ticketResourceSummary = ticketResourceSummary;
	}

	public String getTotal_extended_price() {
		return total_extended_price;
	}

	public void setTotal_extended_price(String total_extended_price) {
		this.total_extended_price = total_extended_price;
	}

	public String getUnion_uplift_factor() {
		return union_uplift_factor;
	}

	public void setUnion_uplift_factor(String union_uplift_factor) {
		this.union_uplift_factor = union_uplift_factor;
	}

	public String getVgpm() {
		return vgpm;
	}

	public void setVgpm(String vgpm) {
		this.vgpm = vgpm;
	}

	public String getCriticalityActual() {
		return criticalityActual;
	}

	public void setCriticalityActual(String criticalityActual) {
		this.criticalityActual = criticalityActual;
	}

	public String getCriticalitySuggested() {
		return criticalitySuggested;
	}

	public void setCriticalitySuggested(String criticalitySuggested) {
		this.criticalitySuggested = criticalitySuggested;
	}

	public String getTimeOnSite() {
		return timeOnSite;
	}

	public void setTimeOnSite(String timeOnSite) {
		this.timeOnSite = timeOnSite;
	}

	public String getTimeToRespond() {
		return timeToRespond;
	}

	public void setTimeToRespond(String timeToRespond) {
		this.timeToRespond = timeToRespond;
	}

	public String getTicketActivityId() {
		return ticketActivityId;
	}

	public void setTicketActivityId(String ticketActivityId) {
		this.ticketActivityId = ticketActivityId;
	}

	public String getIsPOAssigned() {
		return isPOAssigned;
	}

	public void setIsPOAssigned(String isPOAssigned) {
		this.isPOAssigned = isPOAssigned;
	}

	public String getRequestorIds() {
		return requestorIds;
	}

	public void setRequestorIds(String requestorIds) {
		this.requestorIds = requestorIds;
	}

	public String getAdditionalRecipientIds() {
		return additionalRecipientIds;
	}

	public void setAdditionalRecipientIds(String additionalRecipientIds) {
		this.additionalRecipientIds = additionalRecipientIds;
	}

	public String[] getActivityType() {
		return activityType;
	}

	public void setActivityType(String[] activityType) {
		this.activityType = activityType;
	}

	public String getIsAddendumApproved() {
		return isAddendumApproved;
	}

	public void setIsAddendumApproved(String isAddendumApproved) {
		this.isAddendumApproved = isAddendumApproved;
	}

	public String getAddendumStatus() {
		return addendumStatus;
	}

	public void setAddendumStatus(String addendumStatus) {
		this.addendumStatus = addendumStatus;
	}

	public String[] getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String[] primaryContact) {
		this.primaryContact = primaryContact;
	}

	public String getWebTicket() {
		return webTicket;
	}

	public void setWebTicket(String webTicket) {
		this.webTicket = webTicket;
	}

	public String getUpdateCompleteTicket() {
		return updateCompleteTicket;
	}

	public void setUpdateCompleteTicket(String updateCompleteTicket) {
		this.updateCompleteTicket = updateCompleteTicket;
	}

	public String getJobIndicator() {
		return jobIndicator;
	}

	public void setJobIndicator(String jobIndicator) {
		this.jobIndicator = jobIndicator;
	}

	public String[] getCustomerDelCount() {
		return customerDelCount;
	}

	public void setCustomerDelCount(String[] customerDelCount) {
		this.customerDelCount = customerDelCount;
	}

	public String[] getInternalDelCount() {
		return internalDelCount;
	}

	public void setInternalDelCount(String[] internalDelCount) {
		this.internalDelCount = internalDelCount;
	}

	public String[] getTotalDelCount() {
		return totalDelCount;
	}

	public void setTotalDelCount(String[] totalDelCount) {
		this.totalDelCount = totalDelCount;
	}

	public String getInvAmount() {
		return invAmount;
	}

	public void setInvAmount(String inv_amount) {
		this.invAmount = inv_amount;
	}

	public String getInvAge() {
		return invAge;
	}

	public void setInvAge(String invAge) {
		this.invAge = invAge;
	}

	public String getInvDate() {
		return invDate;
	}

	public void setInvDate(String invDate) {
		this.invDate = invDate;
	}

	public String getInvDiscount() {
		return invDiscount;
	}

	public void setInvDiscount(String invDiscount) {
		this.invDiscount = invDiscount;
	}

	public String getInvNumber() {
		return invNumber;
	}

	public void setInvNumber(String invNumber) {
		this.invNumber = invNumber;
	}

	public String[] getCommissionPO() {
		return commissionPO;
	}

	public void setCommissionPO(String[] commissionPO) {
		this.commissionPO = commissionPO;
	}

	public String getRseInternalJustification() {
		return rseInternalJustification;
	}

	public void setRseInternalJustification(String rseInternalJustification) {
		this.rseInternalJustification = rseInternalJustification;
	}

	public String getReScheduleClientCount() {
		return reScheduleClientCount;
	}

	public void setReScheduleClientCount(String reScheduleClientCount) {
		this.reScheduleClientCount = reScheduleClientCount;
	}

	public String getReScheduleContingentCount() {
		return reScheduleContingentCount;
	}

	public void setReScheduleContingentCount(String reScheduleContingentCount) {
		this.reScheduleContingentCount = reScheduleContingentCount;
	}

	public String getReScheduleInternalCount() {
		return reScheduleInternalCount;
	}

	public void setReScheduleInternalCount(String reScheduleInternalCount) {
		this.reScheduleInternalCount = reScheduleInternalCount;
	}

	public String getIsSendEmail() {
		return isSendEmail;
	}

	public void setIsSendEmail(String isSendEmail) {
		this.isSendEmail = isSendEmail;
	}

	public String getPocEmail() {
		return pocEmail;
	}

	public void setPocEmail(String pocEmail) {
		this.pocEmail = pocEmail;
	}

	public String getSite_priorities() {
		return site_priorities;
	}

	public void setSite_priorities(String site_priorities) {
		this.site_priorities = site_priorities;
	}

}
