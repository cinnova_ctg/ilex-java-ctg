package com.mind.bean.newjobdb;

import java.io.Serializable;

public class MasterPurchaseOrder implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mpoId;
	private String mpoName;
	private String masterPOItem;
	private float estimatedTotalCost;
	private String status;
	private boolean isSelectable;
	
	
	public boolean isSelectableForCopyToPO() {
		if(this.status!=null && this.status.trim().equalsIgnoreCase("Approved"))
			return true;
		else
			return false;
	}

	public float getEstimatedTotalCost() {
		return estimatedTotalCost;
	}

	public void setEstimatedTotalCost(float estimatedTotalCost) {
		this.estimatedTotalCost = estimatedTotalCost;
	}

	public String getMasterPOItem() {
		return masterPOItem;
	}

	public void setMasterPOItem(String masterPOItem) {
		this.masterPOItem = masterPOItem;
	}

	public String getMpoId() {
		return mpoId;
	}

	public void setMpoId(String mpoId) {
		this.mpoId = mpoId;
	}

	public String getMpoName() {
		return mpoName;
	}

	public void setMpoName(String mpoName) {
		this.mpoName = mpoName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isSelectable() {
		return isSelectable;
	}

	public void setSelectable(boolean isSelectable) {
		this.isSelectable = isSelectable;
	}
}
