package com.mind.bean.newjobdb;

public class TicketResourceSummary {
	
	private String resource_name = "";
	private String resource_quantity = "";
	private String resource_unit_price = "";
	private String resource_total_price = "";
	
	
	public String getResource_unit_price() {
		return resource_unit_price;
	}
	public void setResource_unit_price(String resource_unit_price) {
		this.resource_unit_price = resource_unit_price;
	}
	public String getResource_name() {
		return resource_name;
	}
	public void setResource_name(String resource_name) {
		this.resource_name = resource_name;
	}
	public String getResource_quantity() {
		return resource_quantity;
	}
	public void setResource_quantity(String resource_quantity) {
		this.resource_quantity = resource_quantity;
	}
	public String getResource_total_price() {
		return resource_total_price;
	}
	public void setResource_total_price(String resource_total_price) {
		this.resource_total_price = resource_total_price;
	}
	
	

}
