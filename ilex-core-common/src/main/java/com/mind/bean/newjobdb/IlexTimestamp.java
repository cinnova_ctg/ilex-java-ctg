package com.mind.bean.newjobdb;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;




/**
 * @purpose This class is used for the representation of 
 * 			the Timestamp object for data.
 */
public class IlexTimestamp implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String date;
	private String hour;
	private String minute;
	private boolean isAM;
	
	public IlexTimestamp(){		
	}
	public IlexTimestamp(String date, String hour, String minute, boolean isAM){
		this.date = date;
		this.hour = hour;
		this.minute = minute;
		this.isAM = isAM;
	}
	public static IlexTimestamp convertToIlexTimestamp(java.sql.Date date) {
		IlexTimestamp ilexTimestamp=new IlexTimestamp(); 
		java.sql.Timestamp ts = new java.sql.Timestamp(date.getTime());
		SimpleDateFormat sf = new SimpleDateFormat("MM/dd/yyyy");
		ilexTimestamp.setDate(ts.getDate()+"");
		ilexTimestamp.setHour(ts.getHours()+"");
		ilexTimestamp.setMinute(ts.getMinutes()+"");
		//ts.get
		//need to be set am
		//ilexTimestamp.setAM(Util.getStringTime());
		return ilexTimestamp;
	}
	
	/**
	 * Rerurn IlexTimestamp object of current system datetime.
	 * @return
	 */
	public static IlexTimestamp getSystemDateTime(){
		SimpleDateFormat sf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss:mss aaa");
		java.util.Date date = new java.util.Date();
		return converToTimestamp(sf.format(date));
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public boolean isAM() {
		return isAM;
	}

	public void setAM(boolean isAM) {
		this.isAM = isAM;
	}

	public String getMinute() {
		return minute;
	}

	public void setMinute(String minute) {
		this.minute = minute;
	}

	/**
	 * return true/false on the basis of String AM/PM.
	 * @param val
	 * @return
	 */
	public static boolean getBooleanAMPM(String val)
	{
		if(val.trim().equalsIgnoreCase("AM"))
			return true;
		else
			return false;
	}
	
	/** return AM/PM on the basis of boolean value.
	 * @param val
	 * @return
	 */
	public static String getStringAMPM(boolean val)
	{
		if(val)
			return "AM";
		else if(!val)
			return "PM";
		else
			return "0";
	}
	/**
	 * Discription:		This method is used to convert value of TicketTimestamp object into a String(mm/dd/yyyy hh:mm AM/PM).
	 * 					when no date defined its return 00:00 AM.
	 * @param timestamp		-- TicketTimestamp object
	 * @return
	 */
	public static String getDateTimeType2( IlexTimestamp timestamp ){
		
		StringBuffer datetime = new StringBuffer();
		if(timestamp.getDate() == null || timestamp.getDate().trim().equals("")){
			datetime.append("00:00 AM");
			return datetime.toString();
		}
		datetime.append(timestamp.getDate());
		datetime.append(" ");
		datetime.append(timestamp.getHour());
		datetime.append(":");
		datetime.append(timestamp.getMinute());
		datetime.append(" ");
		setAM(timestamp);
		if(timestamp.isAM())
			datetime.append("AM");
		else
			datetime.append("PM");
		
		return datetime.toString();		// - mm/dd/yyyy hh:mm AM/PM
	}
	/**
	 * Discription: This method conver mm/dd/yyyy hh:mm:ss:mss AM/PM to Data, hour, minute, and AM/PM and store into 
	 * 				TicketTimestamp.
	 * @param requested_date	-- mm/dd/yyyy hh:mm:ss:mss AM/PM
	 * @return
	 */
	public static IlexTimestamp converToTimestamp(String requested_date) {
		IlexTimestamp timestamp = new IlexTimestamp();
		if(requested_date != null && !requested_date.equals("")) { 
			timestamp.setDate(requested_date.substring(0, 10));
			if(requested_date.length() > 16 ) {
				if(requested_date.substring(11,12).equals(" "))
					timestamp.setHour("0"+requested_date.substring(12,13));
				else
					timestamp.setHour(requested_date.substring(11,13));
				
				timestamp.setMinute(requested_date.substring(14,16));
			} else {
				timestamp.setHour("00");
				timestamp.setMinute("00");
				timestamp.setAM(true);
			}
			
			//if(requested_date.substring(23,(requested_date.length()-1)).equals("AM"))
			if(requested_date.indexOf("AM") > 21)
				timestamp.setAM(true);
			else
				timestamp.setAM(false);
		}else{
			timestamp.setDate("");
			timestamp.setHour("00");
			timestamp.setAM(true);
		}
			
		return timestamp;
	} // - end of converToTimestamp() method
	public static Date convertToDate(IlexTimestamp ilexTimestamp) {
		return null;
	}
	
	/**
	 * Discription:		This method is used to convert value of TicketTimestamp object into a String(mm/dd/yyyy hh:mm AM/PM).
	 * 					when no date defined its return default to 01/01/1900.
	 * @param timestamp		-- TicketTimestamp object
	 * @return
	 */
	public static String getDateTimeType1( IlexTimestamp ilexTimestamp ){
		
		StringBuffer datetime = new StringBuffer();
		boolean check = true;
		if(ilexTimestamp.getDate() == null || ilexTimestamp.getDate().trim().equals("")){
			check = false;
			datetime.append("01/01/1900");
		}
		if(check){
			datetime.append(ilexTimestamp.getDate());
			datetime.append(" ");
			datetime.append(ilexTimestamp.getHour());
			datetime.append(":");
			datetime.append(ilexTimestamp.getMinute());
			datetime.append(" ");
			setAM(ilexTimestamp);
			if(ilexTimestamp.isAM())
				datetime.append("AM");
			else
				datetime.append("PM");
		}
		
		return datetime.toString();		// - mm/dd/yyyy hh:mm AM/PM
	}
	
	/**
	 * This mthod return AM/PM on the basis of IlexTimestamp object. 
	 * @param ilexTimestamp
	 * @return
	 */
	public static String getOP(IlexTimestamp ilexTimestamp){
		if(ilexTimestamp.isAM){
			return "AM";
		} else{
			return "PM";
		}
	}
	
	/**
	 * When Hour is 00 then set AM true.
	 * @param ilexTimestamp
	 */
	private static void setAM(IlexTimestamp ilexTimestamp) {
		if(ilexTimestamp.getHour() == null || ilexTimestamp.getHour().equals("00") || ilexTimestamp.getHour().equals("")){
			ilexTimestamp.setAM(true);
		}
	}
}
