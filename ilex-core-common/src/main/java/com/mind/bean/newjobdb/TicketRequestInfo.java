package com.mind.bean.newjobdb;

import java.util.ArrayList;

/**
 * @purpose This class is used to represent the the TICKET Request Info.
 */
public class TicketRequestInfo {
	private String requestor;
	private String requestorEmail;
	private String requestType;
	private String criticality;
	private boolean isPPS;
	private String resource;
	private boolean isStandBy;
	private boolean isMSP;
	private boolean isHelpDesk;
	private String customerReference;
	private String site;

	public String getCriticality() {
		return criticality;
	}

	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public boolean isHelpDesk() {
		return isHelpDesk;
	}

	public void setHelpDesk(boolean isHelpDesk) {
		this.isHelpDesk = isHelpDesk;
	}

	public boolean isMSP() {
		return isMSP;
	}

	public void setMSP(boolean isMSP) {
		this.isMSP = isMSP;
	}

	public boolean isPPS() {
		return isPPS;
	}

	public void setPPS(boolean isPPS) {
		this.isPPS = isPPS;
	}

	public boolean isStandBy() {
		return isStandBy;
	}

	public void setStandBy(boolean isStandBy) {
		this.isStandBy = isStandBy;
	}

	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String getRequestorEmail() {
		return requestorEmail;
	}

	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public static ArrayList<String> getResources(long ticketId,
			String criticality, boolean isPPS) {
		return null;
	}

	public void findSite() {

	}
}
