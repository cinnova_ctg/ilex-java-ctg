package com.mind.bean.newjobdb;

import java.io.Serializable;


public class GeneralPOSetup implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IlexTimestamp deliverByDate;
	private IlexTimestamp issueDate;
	
	public IlexTimestamp getDefaultDeliverByDate() {
		return null;
	}
	
	public IlexTimestamp getDefaultIssueDate() {
		return null;
	}

	public IlexTimestamp getDeliverByDate() {
		return deliverByDate;
	}
	public void setDeliverByDate(IlexTimestamp deliverByDate) {
		this.deliverByDate = deliverByDate;
	}
	public IlexTimestamp getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(IlexTimestamp issueDate) {
		this.issueDate = issueDate;
	}
}
