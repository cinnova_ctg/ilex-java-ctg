package com.mind.bean.newjobdb;

import java.io.Serializable;
import java.util.ArrayList;


public class TicketActivitySummary implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String activityId = "";
	private String total_extended_price = "";
	private String actual_cost = "";
	private String vgpm = "";
	private String union_uplift_factor = "";
	private String expedite24_uplift_factor = "";
	private String expedite48_uplift_factor = "";
	private String non_pps_uplift_factor = "";
	private String international_uplift_factor = "";
	private ArrayList<TicketResourceSummary> ticketResourceSummary = new ArrayList<TicketResourceSummary>();
	
	public String getActual_cost() {
		return actual_cost;
	}
	public void setActual_cost(String actual_cost) {
		this.actual_cost = actual_cost;
	}
	public String getExpedite24_uplift_factor() {
		return expedite24_uplift_factor;
	}
	public void setExpedite24_uplift_factor(String expedite24_uplift_factor) {
		this.expedite24_uplift_factor = expedite24_uplift_factor;
	}
	public String getExpedite48_uplift_factor() {
		return expedite48_uplift_factor;
	}
	public void setExpedite48_uplift_factor(String expedite48_uplift_factor) {
		this.expedite48_uplift_factor = expedite48_uplift_factor;
	}
	public String getInternational_uplift_factor() {
		return international_uplift_factor;
	}
	public void setInternational_uplift_factor(String international_uplift_factor) {
		this.international_uplift_factor = international_uplift_factor;
	}
	public String getNon_pps_uplift_factor() {
		return non_pps_uplift_factor;
	}
	public void setNon_pps_uplift_factor(String non_pps_uplift_factor) {
		this.non_pps_uplift_factor = non_pps_uplift_factor;
	}
	public ArrayList<TicketResourceSummary> getTicketResourceSummary() {
		return ticketResourceSummary;
	}
	public void setTicketResourceSummary(
			ArrayList<TicketResourceSummary> ticketResourceSummary) {
		this.ticketResourceSummary = ticketResourceSummary;
	}
	public String getTotal_extended_price() {
		return total_extended_price;
	}
	public void setTotal_extended_price(String total_extended_price) {
		this.total_extended_price = total_extended_price;
	}
	public String getUnion_uplift_factor() {
		return union_uplift_factor;
	}
	public void setUnion_uplift_factor(String union_uplift_factor) {
		this.union_uplift_factor = union_uplift_factor;
	}
	public String getVgpm() {
		return vgpm;
	}
	public void setVgpm(String vgpm) {
		this.vgpm = vgpm;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	
	
	
	
	

}
