package com.mind.bean.newjobdb;

import java.io.Serializable;



/**
 * @purpose This class is used for the representation of 
 * 			the Cost Information.
 */
public class CostInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private float unit;
	private float total;
	
	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	public float getUnit() {
		return unit;
	}
	public void setUnit(float unit) {
		this.unit = unit;
	}
}
