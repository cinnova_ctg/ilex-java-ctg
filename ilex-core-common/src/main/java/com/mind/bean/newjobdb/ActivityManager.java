package com.mind.bean.newjobdb;

import java.io.Serializable;



/**
 * @purpose This class is used to contain the functionality for the 
 * 			activity management 
 */
public class ActivityManager implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void openAdder() {
	
	}
	
	public void openManagement() {
	
	}
	
	public void openUplifts() {
	
	}
	
	public void viewResources() {
	
	}
	
	public void openOOSAdder() {
	
	}
}
