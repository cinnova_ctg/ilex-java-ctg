package com.mind.bean.newjobdb;

public class JobChecklistParameters  {
	public static final String ITEM_DESCRIPTION_1 = "jobWorkflowItemDescription_1";
	public static final String ITEM_DESCRIPTION_2 = "jobWorkflowItemDescription_2";
	public static final String ITEM_ID1 = "jobWorkflowItemId_1";
	public static final String ITEM_ID2 = "jobWorkflowItemId_2";
	public static final String ITEM_STATUS1 = "jobWorkflowItemStatus_1";
	public static final String ITEM_STATUS2 = "jobWorkflowItemStatus_2";
	public static final String ITEM_SEQUENCE1 = "jobWorkflowItemSequence_1";
	public static final String ITEM_SEQUENCE2 = "jobWorkflowItemSequence_2";
	
	public static final String ITEM_REFERENCE_DATE_1 = "jobWorkflowItemReferenceDate_1";
	public static final String ITEM_REFERENCE_DATE_2 = "jobWorkflowItemReferenceDate_2";
	public static final String ITEM_USER_1 = "jobWorkflowItemUser_1";
	public static final String ITEM_USER_2 = "jobWorkflowItemUser_2";
}
