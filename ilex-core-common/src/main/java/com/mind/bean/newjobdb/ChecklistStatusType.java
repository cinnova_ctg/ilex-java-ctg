package com.mind.bean.newjobdb;



/**
 * @purpose This class is used to contain the list of 
 * 			Checklist statuses
 */
public class ChecklistStatusType  {
	public static final int PENDING_STATUS = 0;
	public static final int COMPLETED_STATUS = 1;
	public static final int SKIPPED_STATUS = 2;
	public static final int NOT_APPLICABLE_STATUS = 3;
	
	public static  String getStatusName(int statusCode){
		String status = "";
		if(statusCode==NOT_APPLICABLE_STATUS){
			status = "Not Applicable";
		}
		else if(statusCode==PENDING_STATUS){
			status = "Pending";
		}
		else if(statusCode==SKIPPED_STATUS){
			status = "Skipped";
		}
		else if(statusCode==COMPLETED_STATUS){
			status = "Completed";
		}
		else{
			status="";
		}
		
		return status;
	}
}
