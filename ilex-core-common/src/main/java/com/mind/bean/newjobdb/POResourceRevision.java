package com.mind.bean.newjobdb;

import java.io.Serializable;

public class POResourceRevision implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String resourceId;
	private float resourceUnitCost;
	private float resourceQuantity;
	private String resourceShowOnWo;
	public String getResourceId() {
		return resourceId;
	}
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	
	public String getResourceShowOnWo() {
		return resourceShowOnWo;
	}
	public void setResourceShowOnWo(String resourceShowOnWo) {
		this.resourceShowOnWo = resourceShowOnWo;
	}
	public float getResourceQuantity() {
		return resourceQuantity;
	}
	public void setResourceQuantity(float resourceQuantity) {
		this.resourceQuantity = resourceQuantity;
	}
	public float getResourceUnitCost() {
		return resourceUnitCost;
	}
	public void setResourceUnitCost(float resourceUnitCost) {
		this.resourceUnitCost = resourceUnitCost;
	}
	

}
