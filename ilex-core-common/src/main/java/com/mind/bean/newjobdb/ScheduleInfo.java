package com.mind.bean.newjobdb;

import java.io.Serializable;

/**
 * @purpose This class is used for the representation of the Schedule
 *          Information.
 */
public class ScheduleInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String scheduleId;
	private String scheduleType;
	private String startDate;
	private String endDate;
	private String reScheduleCount;
	private String timeOnSite;
	private String timeToRespond;
	private String criticalityActual;
	private String criticalitySuggested;

	// Added separate fields for rsecount fields(client,contingent,internal)
	private String reScheduleClientCount = null;
	private String reScheduleContingentCount = null;
	private String reScheduleInternalCount = null;

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getReScheduleCount() {
		return reScheduleCount;
	}

	public void setReScheduleCount(String reScheduleCount) {
		this.reScheduleCount = reScheduleCount;
	}

	public String getCriticalityActual() {
		return criticalityActual;
	}

	public void setCriticalityActual(String criticalityActual) {
		this.criticalityActual = criticalityActual;
	}

	public String getCriticalitySuggested() {
		return criticalitySuggested;
	}

	public void setCriticalitySuggested(String criticalitySuggested) {
		this.criticalitySuggested = criticalitySuggested;
	}

	public String getTimeOnSite() {
		return timeOnSite;
	}

	public String getReScheduleClientCount() {
		return reScheduleClientCount;
	}

	public void setReScheduleClientCount(String reScheduleClientCount) {
		this.reScheduleClientCount = reScheduleClientCount;
	}

	public String getReScheduleContingentCount() {
		return reScheduleContingentCount;
	}

	public void setReScheduleContingentCount(String reScheduleContingentCount) {
		this.reScheduleContingentCount = reScheduleContingentCount;
	}

	public String getReScheduleInternalCount() {
		return reScheduleInternalCount;
	}

	public void setReScheduleInternalCount(String reScheduleInternalCount) {
		this.reScheduleInternalCount = reScheduleInternalCount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setTimeOnSite(String timeOnSite) {
		this.timeOnSite = timeOnSite;
	}

	public String getTimeToRespond() {
		return timeToRespond;
	}

	public void setTimeToRespond(String timeToRespond) {
		this.timeToRespond = timeToRespond;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

}
