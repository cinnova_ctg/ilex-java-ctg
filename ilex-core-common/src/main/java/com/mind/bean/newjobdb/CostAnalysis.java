package com.mind.bean.newjobdb;

import java.io.Serializable;

public class CostAnalysis implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String laborResource;
	private float job = 0.0f;
	private float project = 0.0f;
	
	public float getJob() {
		return job;
	}
	public void setJob(float job) {
		this.job = job;
	}
	public String getLaborResource() {
		return laborResource;
	}
	public void setLaborResource(String laborResource) {
		this.laborResource = laborResource;
	}
	public float getProject() {
		return project;
	}
	public void setProject(float project) {
		this.project = project;
	}

}
