package com.mind.bean.newjobdb;

import java.io.Serializable;




public class PODocument implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String documentName;
	private String documentStatus;
	private String date;
	private String docId;
	private boolean isIncludedWithPO;
	private boolean isIncludedWithWO;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getDocumentStatus() {
		return documentStatus;
	}
	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}
	public boolean isIncludedWithPO() {
		return isIncludedWithPO;
	}
	public void setIncludedWithPO(boolean isIncludedWithPO) {
		this.isIncludedWithPO = isIncludedWithPO;
	}
	public boolean isIncludedWithWO() {
		return isIncludedWithWO;
	}
	public void setIncludedWithWO(boolean isIncludedWithWO) {
		this.isIncludedWithWO = isIncludedWithWO;
	}
	public String getDocId() {
		return docId;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}
}
