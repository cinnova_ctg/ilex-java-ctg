package com.mind.bean.newjobdb;

import java.io.Serializable;
import java.util.ArrayList;

import com.mind.bean.newjobdb.MasterPurchaseOrder;



public class MPOList implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<MasterPurchaseOrder> masterPurchaseOrders;

	public ArrayList<MasterPurchaseOrder> getMasterPurchaseOrders() {
		return masterPurchaseOrders;
	}

	public void setMasterPurchaseOrders(
			ArrayList<MasterPurchaseOrder> masterPurchaseOrders) {
		this.masterPurchaseOrders = masterPurchaseOrders;
	}
}
