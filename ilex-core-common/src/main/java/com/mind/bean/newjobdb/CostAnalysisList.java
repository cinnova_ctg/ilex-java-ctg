package com.mind.bean.newjobdb;

import java.util.ArrayList;


public class CostAnalysisList {
	
	private ArrayList<CostAnalysis> costAnalysis = new ArrayList<CostAnalysis>();

	public ArrayList<CostAnalysis> getCostAnalysis() {
		return costAnalysis;
	}

	public void setCostAnalysis(ArrayList<CostAnalysis> costAnalysis) {
		this.costAnalysis = costAnalysis;
	}
	

}
