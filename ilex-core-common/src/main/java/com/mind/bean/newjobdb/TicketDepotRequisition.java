package com.mind.bean.newjobdb;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @purpose This class is used to represent the  
 * 			the TICKET depot requests.
 */
public class TicketDepotRequisition implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long ticketId;
	private ArrayList<TicketDepotProductRequest> productRequests;
	
	public ArrayList<TicketDepotProductRequest> getProductRequests() {
		return productRequests;
	}
	public void setProductRequests(
			ArrayList<TicketDepotProductRequest> productRequests) {
		this.productRequests = productRequests;
	}
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
}
