package com.mind.bean.newjobdb;



/**
 * @purpose This class is used for the representation of 
 * 			the Job Level Workflow Checklist Item.
 */
public class JobWorkflowItem extends WorkflowChecklistItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String itemStatus;
	private int itemStatusCode;
	private long projectWorkflowItemId;
	
	private int lastItemStatusCode;
	
	public int getLastItemStatusCode() {
		return lastItemStatusCode;
	}

	public void setLastItemStatusCode(int lastItemStatusCode) {
		this.lastItemStatusCode = lastItemStatusCode;
	}

	public int getItemStatusCode() {
		return itemStatusCode;
	}

	public void setItemStatusCode(int itemStatusCode) {
		this.itemStatusCode = itemStatusCode;
	}

	public boolean isCompletedType() {
		return 
		itemStatusCode==ChecklistStatusType.COMPLETED_STATUS
			|| itemStatusCode==ChecklistStatusType.NOT_APPLICABLE_STATUS
			|| itemStatusCode==ChecklistStatusType.SKIPPED_STATUS
			;
	}
	
	public boolean isPendingToBeCompleted() {
		return true;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public long getProjectWorkflowItemId() {
		return projectWorkflowItemId;
	}

	public void setProjectWorkflowItemId(long projectWorkflowItemId) {
		this.projectWorkflowItemId = projectWorkflowItemId;
	}
}
