package com.mind.bean.newjobdb;

import java.io.Serializable;



public class POResource implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String resourceId;
	private String activityId;
	private String activityName;
	private String resourceName;
	private String type;
	private String revision;
	private float activityQuantity;
	private float resourceQuantity;
	private float totalQuantity;
	private float estimatedCostUnit;
	private float estimatedCostTotal;
	private float asPlannedQuantity;
	private float asPlannedUnitCost;
	private float asplannedSubTotal;
	private boolean isShowOnWO;
	private boolean isSelectedInPO;
	private String resourceSubName;
	private String resourceSubNamePre;
	private String resourceSubId;
	private String typeId;
	private String inTransit;
	
	public String getInTransit() {
		return inTransit;
	}
	public void setInTransit(String inTransit) {
		this.inTransit = inTransit;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public float getActivityQuantity() {
		return activityQuantity;
	}
	public void setActivityQuantity(float activityQuantity) {
		this.activityQuantity = activityQuantity;
	}
	public float getAsPlannedQuantity() {
		return asPlannedQuantity;
	}
	public void setAsPlannedQuantity(float asPlannedQuantity) {
		this.asPlannedQuantity = asPlannedQuantity;
	}
	public float getAsplannedSubTotal() {
		return asplannedSubTotal;
	}
	public void setAsplannedSubTotal(float asplannedSubTotal) {
		this.asplannedSubTotal = asplannedSubTotal;
	}
	public float getAsPlannedUnitCost() {
		return asPlannedUnitCost;
	}
	public void setAsPlannedUnitCost(float asPlannedUnitCost) {
		this.asPlannedUnitCost = asPlannedUnitCost;
	}
	public float getEstimatedCostTotal() {
		return estimatedCostTotal;
	}
	public void setEstimatedCostTotal(float estimatedCostTotal) {
		this.estimatedCostTotal = estimatedCostTotal;
	}
	public float getEstimatedCostUnit() {
		return estimatedCostUnit;
	}
	public void setEstimatedCostUnit(float estimatedCostUnit) {
		this.estimatedCostUnit = estimatedCostUnit;
	}
	public boolean isSelectedInPO() {
		return isSelectedInPO;
	}
	public void setSelectedInPO(boolean isSelectedInPO) {
		this.isSelectedInPO = isSelectedInPO;
	}
	public boolean isShowOnWO() {
		return isShowOnWO;
	}
	public void setShowOnWO(boolean isShowOnWO) {
		this.isShowOnWO = isShowOnWO;
	}
	public String getResourceName() {
		return resourceName;
	}
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	public float getResourceQuantity() {
		return resourceQuantity;
	}
	public void setResourceQuantity(float resourceQuantity) {
		this.resourceQuantity = resourceQuantity;
	}
	public float getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(float totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getResourceId() {
		return resourceId;
	}
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	public String getResourceSubName() {
		return resourceSubName;
	}
	public void setResourceSubName(String resourceSubName) {
		this.resourceSubName = resourceSubName;
	}
	public String getResourceSubId() {
		return resourceSubId;
	}
	public void setResourceSubId(String resourceSubId) {
		this.resourceSubId = resourceSubId;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getRevision() {
		return revision;
	}
	public void setRevision(String revision) {
		this.revision = revision;
	}
	public String getResourceSubNamePre() {
		return resourceSubNamePre;
	}
	public void setResourceSubNamePre(String resourceSubNamePre) {
		this.resourceSubNamePre = resourceSubNamePre;
	}
	
}
