package com.mind.bean.newjobdb;

import java.io.Serializable;

/**
 * @purpose This class is used to represent the  
 * 			the TICKET depot request product-quantities.
 */
public class TicketDepotProductRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long depotRequestId;
	private TicketDepotProduct product;
	private float quantity;
	
	public long getDepotRequestId() {
		return depotRequestId;
	}
	public void setDepotRequestId(long depotRequestId) {
		this.depotRequestId = depotRequestId;
	}
	public float getQuantity() {
		return quantity;
	}
	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}
	public TicketDepotProduct getProduct() {
		return product;
	}
	public void setProduct(TicketDepotProduct product) {
		this.product = product;
	}
	
}
