package com.mind.bean.newjobdb;

public class POActionTypes  {
	
	public static final int ACTION_COPY = 1; 
	
	public static final int ACTION_BUILD = 2;
	
	public static final int ACTION_EDIT = 3;
	
	public static final int ACTION_ASSIGN = 4;
	
	public static final int ACTION_DELETE = 5;
	
	public static final int ACTION_SEND = 6;
	
	public static final int ACTION_CANCEL = 7;
	
	public static final int ACTION_REASSIGN = 8;
	
	public static final int ACTION_REISSUE = 9;
	
	public static final int ACTION_ACCEPT = 10;
	
	public static final int ACTION_COMPLETE = 11;
	
	public static final int ACTION_EMAIL = 12;
	
	public static final int ACTION_UNDO = 13;
	
	public static String getActionNames(int actionCode)
	{
		if(actionCode == ACTION_EDIT)
			return "Edit";
		else if (actionCode == ACTION_ASSIGN)
			return "Assign";
		else if (actionCode == ACTION_SEND)
			return "Send";
		else if (actionCode == ACTION_DELETE)
			return "Delete";
		else if (actionCode == ACTION_CANCEL)
			return "Cancel";
		else if (actionCode == ACTION_ACCEPT)
			return "Accept";
		else if (actionCode == ACTION_COMPLETE)
			return "Complete";
		else if (actionCode == ACTION_EMAIL)
			return "email";
		else if (actionCode == ACTION_UNDO)
			return "Undo";
		else if (actionCode == ACTION_REISSUE)
			return "Reissue";
		else if (actionCode == ACTION_REASSIGN)
			return "Reassign";
		else if (actionCode == ACTION_COPY)
			return "Copy";
		else if (actionCode == ACTION_BUILD)
			return "Build";
		return "";
	}
}
