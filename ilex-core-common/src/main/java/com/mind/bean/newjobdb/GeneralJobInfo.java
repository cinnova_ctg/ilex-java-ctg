package com.mind.bean.newjobdb;

import java.io.Serializable;

import com.mind.bean.msp.SiteInfo;



/**
 * @purpose This class is used for the representation of 
 * 			the general Job Information.
 */
public class GeneralJobInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long jobId;
	private String jobStatus;
	private ScheduleInfo scheduleInfo;
	private TeamInfo teamInfo;
	private SiteInfo siteInfo;
	private CustomerReferenceInfo customerReferenceInfo;
	private boolean isShowCustomerReference;
	private String isAddendumApproved;
	private String addendumStatus;
	private String requestType;

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getIsAddendumApproved() {
		return isAddendumApproved;
	}

	public void setIsAddendumApproved(String isAddendumApproved) {
		this.isAddendumApproved = isAddendumApproved;
	}

	public CustomerReferenceInfo getCustomerReferenceInfo() {
		return customerReferenceInfo;
	}

	public void setCustomerReferenceInfo(CustomerReferenceInfo customerReferenceInfo) {
		this.customerReferenceInfo = customerReferenceInfo;
	}

	public long getJobId() {
		return jobId;
	}

	public void setJobId(long jobId) {
		this.jobId = jobId;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public ScheduleInfo getScheduleInfo() {
		return scheduleInfo;
	}

	public void setScheduleInfo(ScheduleInfo scheduleInfo) {
		this.scheduleInfo = scheduleInfo;
	}

	public SiteInfo getSiteInfo() {
		return siteInfo;
	}

	public void setSiteInfo(SiteInfo siteInfo) {
		this.siteInfo = siteInfo;
	}

	public TeamInfo getTeamInfo() {
		return teamInfo;
	}

	public void setTeamInfo(TeamInfo teamInfo) {
		this.teamInfo = teamInfo;
	}

	private ScheduleInfo getAppropriateScheduleInfo() {
		return null;
	}
	
	public boolean isShowCustomerReferenceInfo() {
		return false;
	}

	public boolean isShowCustomerReference() {
		return isShowCustomerReference;
	}

	public void setShowCustomerReference(boolean isShowCustomerReference) {
		this.isShowCustomerReference = isShowCustomerReference;
	}

	public String getAddendumStatus() {
		return addendumStatus;
	}

	public void setAddendumStatus(String addendumStatus) {
		this.addendumStatus = addendumStatus;
	}

	
}
