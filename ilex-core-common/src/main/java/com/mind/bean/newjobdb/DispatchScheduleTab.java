package com.mind.bean.newjobdb;




/**
 * @purpose This class is used for the representation of 
 * 			the DispatchSchedule tab.
 * 
 * @ActionsAllowed from DispatchSchedule tab:
 * 		All POs will be displayed on the DispatchSchedule tab, but only 
 * 		POs in the draft status will have applicable actions 
 * 		which can be initiated from the DispatchSchedule tab.
 * 		
 * 		The DispatchSchedule tab is used to manage the POs which are in the draft state.  
 * 		Thus as POs are created based on the copy or build functions, 
 * 		they appear on the DispatchSchedule tab with Actions possible on Draft state.
 */
public class DispatchScheduleTab extends JobDashboardTab {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appendixId;
	private String jobId;

	/**
	 * The DispatchSchedule tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	private GeneralJobInfo generalJobInfo;
	

	public DispatchScheduleTab() {
		tabId = JobDashboardTabType.DISPATCH_SCHEDULE_TAB;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	/**
	 * The DispatchSchedule tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public GeneralJobInfo getGeneralJobInfo() {
		return generalJobInfo;
	}

	/**
	 * The DispatchSchedule tab will dont have the customer reference value shown in 
	 * the general job info based on the isShowCustomerReference field.
	 */
	public void setGeneralJobInfo(GeneralJobInfo generalJobInfo) {
		this.generalJobInfo = generalJobInfo;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

}
