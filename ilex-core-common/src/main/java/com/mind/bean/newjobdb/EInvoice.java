package com.mind.bean.newjobdb;

import java.io.Serializable;
import java.util.ArrayList;


public class EInvoice implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private EInvoiceSummary eInvoiceSummary;
	private ArrayList<EInvoiceItems> eInvoiceItems;
	
	public ArrayList<EInvoiceItems> getEInvoiceItems() {
		return eInvoiceItems;
	}
	public void setEInvoiceItems(ArrayList<EInvoiceItems> invoiceItems) {
		eInvoiceItems = invoiceItems;
	}
	public EInvoiceSummary getEInvoiceSummary() {
		return eInvoiceSummary;
	}
	public void setEInvoiceSummary(EInvoiceSummary invoiceSummary) {
		eInvoiceSummary = invoiceSummary;
	}
	
	

}
