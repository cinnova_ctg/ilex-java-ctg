package com.mind.bean;

import java.io.Serializable;

public class ActivityGridControl implements Serializable {

	private static final long serialVersionUID = 1L;
	private String activityModuleFlag = null;
	private String activityAddFlag = null;
	private String activityDeleteFlag = null;
	private String activityQuantityFlag = null;
	private String activityTypeFlag = null;
	private String activityStatusFlag = null;
	private String activityBidFlag = null;

	public String getActivityAddFlag() {
		return activityAddFlag;
	}

	public void setActivityAddFlag(String activityAddFlag) {
		this.activityAddFlag = activityAddFlag;
	}

	public String getActivityBidFlag() {
		return activityBidFlag;
	}

	public void setActivityBidFlag(String activityBidFlag) {
		this.activityBidFlag = activityBidFlag;
	}

	public String getActivityDeleteFlag() {
		return activityDeleteFlag;
	}

	public void setActivityDeleteFlag(String activityDeleteFlag) {
		this.activityDeleteFlag = activityDeleteFlag;
	}

	public String getActivityQuantityFlag() {
		return activityQuantityFlag;
	}

	public void setActivityQuantityFlag(String activityQuantityFlag) {
		this.activityQuantityFlag = activityQuantityFlag;
	}

	public String getActivityStatusFlag() {
		return activityStatusFlag;
	}

	public void setActivityStatusFlag(String activityStatusFlag) {
		this.activityStatusFlag = activityStatusFlag;
	}

	public String getActivityTypeFlag() {
		return activityTypeFlag;
	}

	public void setActivityTypeFlag(String activityTypeFlag) {
		this.activityTypeFlag = activityTypeFlag;
	}

	public String getActivityModuleFlag() {
		return activityModuleFlag;
	}

	public void setActivityModuleFlag(String activityModuleFlag) {
		this.activityModuleFlag = activityModuleFlag;
	}
}
