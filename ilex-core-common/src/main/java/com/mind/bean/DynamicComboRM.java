/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: A formbean containing getter/setter methods for populating all comboboxes in Resource Manager.
 *
 */
package com.mind.bean;

import java.io.Serializable;
import java.util.Collection;

public class DynamicComboRM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection secondlevelcatglist = null;
	private Collection thirdlevelcatglist = null;
	private Collection statuslist = null;

	public Collection getSecondlevelcatglist() {
		return secondlevelcatglist;
	}

	public void setSecondlevelcatglist(Collection slcatglist) {
		this.secondlevelcatglist = slcatglist;
	}

	public Collection getThirdlevelcatglist() {
		return thirdlevelcatglist;
	}

	public void setThirdlevelcatglist(Collection thirdlevelcatglist) {
		this.thirdlevelcatglist = thirdlevelcatglist;
	}

	public Collection getStatuslist() {
		return statuslist;
	}

	public void setStatuslist(Collection statuslist) {
		this.statuslist = statuslist;
	}

}
