package com.mind.bean.MasterSite;

import java.io.Serializable;

public class MasterSiteDeviceAttributeBean implements Serializable {

	private String siteNumber;
	private String circuitType;
	private String modemType;
	private String internetLineNo;
	private String isp;
	private String ispSupport;
	private String circuitIP;
	private String siteContactNo;
	private String location;
	private String account;

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getCircuitType() {
		return circuitType;
	}

	public void setCircuitType(String circuitType) {
		this.circuitType = circuitType;
	}

	public String getModemType() {
		return modemType;
	}

	public void setModemType(String modemType) {
		this.modemType = modemType;
	}

	public String getInternetLineNo() {
		return internetLineNo;
	}

	public void setInternetLineNo(String internetLineNo) {
		this.internetLineNo = internetLineNo;
	}

	public String getIsp() {
		return isp;
	}

	public void setIsp(String isp) {
		this.isp = isp;
	}

	public String getIspSupport() {
		return ispSupport;
	}

	public void setIspSupport(String ispSupport) {
		this.ispSupport = ispSupport;
	}

	public String getCircuitIP() {
		return circuitIP;
	}

	public void setCircuitIP(String circuitIP) {
		this.circuitIP = circuitIP;
	}

	public String getSiteContactNo() {
		return siteContactNo;
	}

	public void setSiteContactNo(String siteContactNo) {
		this.siteContactNo = siteContactNo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

}
