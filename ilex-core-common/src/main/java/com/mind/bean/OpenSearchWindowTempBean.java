package com.mind.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class OpenSearchWindowTempBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String category = null;
	private ArrayList categorycombo = null;
	
	private String subcategory = null;
	private ArrayList subcategorycombo = null;
	
	private String subsubcategory = null;
	private ArrayList subsubcategorycombo = null;
	
	
	private String type = null;
	private String keyword = null;
	private String search = null;
	private String from = null;
	private String fromflag = null;
	
	private String submit = null;
	private String activity_Id = null;
	
	private String searchclick = null;
	
	//private String resource_id = null; 
	private ArrayList resourcelist = null;
	
	//private String clr = null;
	private String offset1 = null;
	private String offset2 = null;
	
	/*for multiple resource selection start */
	private String[] check = null;
	private String ok = null;
	private String MSA_Id = null;
	private String searchmore = null;
	private String ref = null;
	private String resourceListType = null;
	/* for multiple resource selection end */
	
	private String addendum_id = null;
	
	private String checknetmedx = null;
	
	private String clr[] = null; 
	private String resource_id[] = null; 
	private String detail_id[] = null;
	private String jobId = null;
	
	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory( String category ) {
		this.category = category;
	}

	
	public ArrayList getCategorycombo() {
		return categorycombo;
	}

	public void setCategorycombo( ArrayList categorycombo ) {
		this.categorycombo = categorycombo;
	}

	
	public String getKeyword() {
		return keyword;
	}

	public void setKeyword( String keyword ) {
		this.keyword = keyword;
	}

	
	public String getSubcategory() {
		return subcategory;
	}

	public void setSubcategory( String subcategory ) {
		this.subcategory = subcategory;
	}

	public ArrayList getSubcategorycombo() {
		return subcategorycombo;
	}

	
	public void setSubcategorycombo( ArrayList subcategorycombo ) {
		this.subcategorycombo = subcategorycombo;
	}

	public String getSubmit() {
		return submit;
	}

	
	public void setSubmit( String submit ) {
		this.submit = submit;
	}

	public String getSubsubcategory() {
		return subsubcategory;
	}

	
	public void setSubsubcategory( String subsubcategory ) {
		this.subsubcategory = subsubcategory;
	}

	public ArrayList getSubsubcategorycombo() {
		return subsubcategorycombo;
	}

	
	public void setSubsubcategorycombo( ArrayList subsubcategorycombo ) {
		this.subsubcategorycombo = subsubcategorycombo;
	}

	
	public String getType() {
		return type;
	}

	public void setType( String type ) {
		this.type = type;
	}

	
	public String getActivity_Id() {
		return activity_Id;
	}

	public void setActivity_Id( String activity_Id ) {
		this.activity_Id = activity_Id;
	}

	
	public ArrayList getResourcelist() {
		return resourcelist;
	}

	public void setResourcelist( ArrayList resourcelist ) {
		this.resourcelist = resourcelist;
	}

	
	public String getSearch() {
		return search;
	}

	public void setSearch( String search ) {
		this.search = search;
	}

	
	public String getFrom() {
		return from;
	}

	public void setFrom( String from ) {
		this.from = from;
	}

	
		
	public String getSearchclick() {
		return searchclick;
	}

	public void setSearchclick( String searchclick ) {
		this.searchclick = searchclick;
	}

	public String getOffset1() {
		return offset1;
	}

	public void setOffset1(String offset1) {
		this.offset1 = offset1;
	}

	public String getOffset2() {
		return offset2;
	}

	public void setOffset2(String offset2) {
		this.offset2 = offset2;
	}

	public String[] getCheck() {
		return check;
	}

	public void setCheck(String[] check) {
		this.check = check;
	}
	
	public String getCheck( int i ) 
	{	
		return check[i];
	}

	public String getMSA_Id() {
		return MSA_Id;
	}

	public void setMSA_Id(String id) {
		MSA_Id = id;
	}

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}

	public String getSearchmore() {
		return searchmore;
	}

	public void setSearchmore(String searchmore) {
		this.searchmore = searchmore;
	}

	public String getAddendum_id() {
		return addendum_id;
	}

	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}

	public String getChecknetmedx() {
		return checknetmedx;
	}

	public void setChecknetmedx(String checknetmedx) {
		this.checknetmedx = checknetmedx;
	}

	public String[] getClr() {
		return clr;
	}
	
	public String getClr( int i ) 
	{	
		return clr[i];
	}

	public void setClr(String[] clr) {
		this.clr = clr;
	}

	public String[] getResource_id() {
		return resource_id;
	}
	
	public String getResource_id( int i ) 
	{	
		return resource_id[i];
	}

	public void setResource_id(String[] resource_id) {
		this.resource_id = resource_id;
	}

	public String[] getDetail_id() {
		return detail_id;
	}
	
	public String getDetail_id( int i ) 
	{	
		return detail_id[i];
	}

	public void setDetail_id(String[] detail_id) {
		this.detail_id = detail_id;
	}

	public String getResourceListType() {
		return resourceListType;
	}

	public void setResourceListType(String resourceListType) {
		this.resourceListType = resourceListType;
	}

	public String getFromflag() {
		return fromflag;
	}

	public void setFromflag(String fromflag) {
		this.fromflag = fromflag;
	}

}
