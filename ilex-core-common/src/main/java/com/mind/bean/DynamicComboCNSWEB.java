package com.mind.bean;

import java.io.Serializable;
import java.util.Collection;

public class DynamicComboCNSWEB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection statename = null;
	private Collection monthname = null;
	private Collection dayname = null;
	private Collection hourname = null;
	private Collection minutename = null;
	private Collection optionname = null;
	private Collection criticalityname = null;
	private Collection activitycategory = null;
	private Collection problemcategory = null;
	private Collection resourcelevel = null;
	private Collection requestorlist = null;
	private Collection exportStatusList = null;

	public Collection getActivitycategory() {
		return activitycategory;
	}

	public void setActivitycategory(Collection activitycategory) {
		this.activitycategory = activitycategory;
	}

	public Collection getCriticalityname() {
		return criticalityname;
	}

	public void setCriticalityname(Collection criticalityname) {
		this.criticalityname = criticalityname;
	}

	public Collection getHourname() {
		return hourname;
	}

	public void setHourname(Collection hourname) {
		this.hourname = hourname;
	}

	public Collection getMinutename() {
		return minutename;
	}

	public void setMinutename(Collection minutename) {
		this.minutename = minutename;
	}

	public Collection getOptionname() {
		return optionname;
	}

	public void setOptionname(Collection optionname) {
		this.optionname = optionname;
	}

	public Collection getDayname() {
		return dayname;
	}

	public void setDayname(Collection dayname) {
		this.dayname = dayname;
	}

	public Collection getMonthname() {
		return monthname;
	}

	public void setMonthname(Collection monthname) {
		this.monthname = monthname;
	}

	public Collection getStatename() {
		return statename;
	}

	public void setStatename(Collection statename) {
		this.statename = statename;
	}

	public Collection getProblemcategory() {
		return problemcategory;
	}

	public void setProblemcategory(Collection problemcategory) {
		this.problemcategory = problemcategory;
	}

	public Collection getResourcelevel() {
		return resourcelevel;
	}

	public void setResourcelevel(Collection resourcelevel) {
		this.resourcelevel = resourcelevel;
	}

	public Collection getRequestorlist() {
		return requestorlist;
	}

	public void setRequestorlist(Collection requestorlist) {
		this.requestorlist = requestorlist;
	}

	public Collection getExportStatusList() {
		return exportStatusList;
	}

	public void setExportStatusList(Collection exportStatusList) {
		this.exportStatusList = exportStatusList;
	}

}
