package com.mind.bean;

public class Phone_Email implements java.io.Serializable {

	private String afterHoursPhone=null;
	private String afterHoursEmail=null;
	public Phone_Email() {
	}
	public Phone_Email(String phone, String email) {
		super();
	
		afterHoursPhone = phone;
		afterHoursEmail = email;
	}
	public void setAfterHoursPhone(String l)
	{
		afterHoursPhone=l;
	}

	public String getAfterHoursPhone()
	{
		return afterHoursPhone;
	}

	public void setAfterHoursEmail(String v)
	{
		afterHoursEmail=v;
	}

	public String getAfterHoursEmail()
	{
		return afterHoursEmail;
	}


}

