//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : SchemaSetupField.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.bean.docm;

import java.io.Serializable;


/**
 * The Class SchemaSetupField.
 */
public class SchemaSetupField implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The field. */
	private Field field;

	/** The is entry field. */
	private boolean isEntryField;

	/** The is compulsory field. */
	private boolean isCompulsoryField;

	/** The is unique across revision. */
	private boolean isUniqueAcrossRevision;

	/** The is unique across library. */
	private boolean isUniqueAcrossLibrary;

	/** The entry field. */
	private String entryField;

	/** The entry field name. */
	private String entryFieldName;

	/** The entry field id. */
	private String entryFieldId;

	/** The compulsory field. */
	private String compulsoryField;

	/** The unique across revision. */
	private String uniqueAcrossRevision;

	/** The unique across library. */
	private String uniqueAcrossLibrary;

	/** The taxonomy group. */
	private String taxonomyGroup;

	/** The description. */
	private String description;

	/**
	 * Gets the field.
	 * 
	 * @return the field
	 */
	public Field getField() {
		return field;
	}

	/**
	 * Sets the field.
	 * 
	 * @param field
	 *            the new field
	 */
	public void setField(Field field) {
		this.field = field;
	}

	/**
	 * Gets the checks if is compulsory field.
	 * 
	 * @return the checks if is compulsory field
	 */
	public boolean getIsCompulsoryField() {
		return isCompulsoryField;
	}

	/**
	 * Sets the checks if is compulsory field.
	 * 
	 * @param isCompulsoryField
	 *            the new checks if is compulsory field
	 */
	public void setIsCompulsoryField(boolean isCompulsoryField) {
		this.isCompulsoryField = isCompulsoryField;
	}

	/**
	 * Gets the checks if is entry field.
	 * 
	 * @return the checks if is entry field
	 */
	public boolean getIsEntryField() {
		return isEntryField;
	}

	/**
	 * Sets the checks if is entry field.
	 * 
	 * @param isEntryField
	 *            the new checks if is entry field
	 */
	public void setIsEntryField(boolean isEntryField) {
		this.isEntryField = isEntryField;
	}

	/**
	 * Gets the checks if is unique across library.
	 * 
	 * @return the checks if is unique across library
	 */
	public boolean getIsUniqueAcrossLibrary() {
		return isUniqueAcrossLibrary;
	}

	/**
	 * Sets the checks if is unique across library.
	 * 
	 * @param isUniqueAcrossLibrary
	 *            the new checks if is unique across library
	 */
	public void setIsUniqueAcrossLibrary(boolean isUniqueAcrossLibrary) {
		this.isUniqueAcrossLibrary = isUniqueAcrossLibrary;
	}

	/**
	 * Gets the checks if is unique across revision.
	 * 
	 * @return the checks if is unique across revision
	 */
	public boolean getIsUniqueAcrossRevision() {
		return isUniqueAcrossRevision;
	}

	/**
	 * Sets the checks if is unique across revision.
	 * 
	 * @param isUniqueAcrossRevision
	 *            the new checks if is unique across revision
	 */
	public void setIsUniqueAcrossRevision(boolean isUniqueAcrossRevision) {
		this.isUniqueAcrossRevision = isUniqueAcrossRevision;
	}

	/**
	 * Sets the compulsory field.
	 * 
	 * @param isCompulsoryField
	 *            the new compulsory field
	 */
	public void setCompulsoryField(boolean isCompulsoryField) {
		this.isCompulsoryField = isCompulsoryField;
	}

	/**
	 * Sets the entry field.
	 * 
	 * @param isEntryField
	 *            the new entry field
	 */
	public void setEntryField(boolean isEntryField) {
		this.isEntryField = isEntryField;
	}

	/**
	 * Sets the unique across library.
	 * 
	 * @param isUniqueAcrossLibrary
	 *            the new unique across library
	 */
	public void setUniqueAcrossLibrary(boolean isUniqueAcrossLibrary) {
		this.isUniqueAcrossLibrary = isUniqueAcrossLibrary;
	}

	/**
	 * Sets the unique across revision.
	 * 
	 * @param isUniqueAcrossRevision
	 *            the new unique across revision
	 */
	public void setUniqueAcrossRevision(boolean isUniqueAcrossRevision) {
		this.isUniqueAcrossRevision = isUniqueAcrossRevision;
	}

	/**
	 * Gets the compulsory field.
	 * 
	 * @return the compulsory field
	 */
	public String getCompulsoryField() {
		return compulsoryField;
	}

	/**
	 * Sets the compulsory field.
	 * 
	 * @param compulsoryField
	 *            the new compulsory field
	 */
	public void setCompulsoryField(String compulsoryField) {
		this.compulsoryField = compulsoryField;
	}

	/**
	 * Gets the entry field id.
	 * 
	 * @return the entry field id
	 */
	public String getEntryFieldId() {
		return entryFieldId;
	}

	/**
	 * Sets the entry field id.
	 * 
	 * @param entryFieldId
	 *            the new entry field id
	 */
	public void setEntryFieldId(String entryFieldId) {
		this.entryFieldId = entryFieldId;
	}

	/**
	 * Gets the entry field name.
	 * 
	 * @return the entry field name
	 */
	public String getEntryFieldName() {
		return entryFieldName;
	}

	/**
	 * Sets the entry field name.
	 * 
	 * @param entryFieldName
	 *            the new entry field name
	 */
	public void setEntryFieldName(String entryFieldName) {
		this.entryFieldName = entryFieldName;
	}

	/**
	 * Gets the taxonomy group.
	 * 
	 * @return the taxonomy group
	 */
	public String getTaxonomyGroup() {
		return taxonomyGroup;
	}

	/**
	 * Sets the taxonomy group.
	 * 
	 * @param taxonomyGroup
	 *            the new taxonomy group
	 */
	public void setTaxonomyGroup(String taxonomyGroup) {
		this.taxonomyGroup = taxonomyGroup;
	}

	/**
	 * Gets the unique across library.
	 * 
	 * @return the unique across library
	 */
	public String getUniqueAcrossLibrary() {
		return uniqueAcrossLibrary;
	}

	/**
	 * Sets the unique across library.
	 * 
	 * @param uniqueAcrossLibrary
	 *            the new unique across library
	 */
	public void setUniqueAcrossLibrary(String uniqueAcrossLibrary) {
		this.uniqueAcrossLibrary = uniqueAcrossLibrary;
	}

	/**
	 * Gets the unique across revision.
	 * 
	 * @return the unique across revision
	 */
	public String getUniqueAcrossRevision() {
		return uniqueAcrossRevision;
	}

	/**
	 * Sets the unique across revision.
	 * 
	 * @param uniqueAcrossRevision
	 *            the new unique across revision
	 */
	public void setUniqueAcrossRevision(String uniqueAcrossRevision) {
		this.uniqueAcrossRevision = uniqueAcrossRevision;
	}

	/**
	 * Gets the entry field.
	 * 
	 * @return the entry field
	 */
	public String getEntryField() {
		return entryField;
	}

	/**
	 * Sets the entry field.
	 * 
	 * @param entryField
	 *            the new entry field
	 */
	public void setEntryField(String entryField) {
		this.entryField = entryField;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
