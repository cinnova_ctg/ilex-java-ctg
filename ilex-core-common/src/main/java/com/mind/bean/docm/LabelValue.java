package com.mind.bean.docm;

import java.io.Serializable;

/**
 * The Class LabelValue.
 */
public class LabelValue implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3545793278051103030L;

	/** The label. */
	private String label = null;

	/** The value. */
	private String value = null;

	/**
	 * Instantiates a new label value.
	 * 
	 * @param a
	 *            the a
	 * @param b
	 *            the b
	 */
	public LabelValue(String a, String b) {
		label = a;
		value = b;
	}

	/**
	 * Sets the label.
	 * 
	 * @param l
	 *            the new label
	 */
	public void setLabel(String l) {
		label = l;
	}

	/**
	 * Gets the label.
	 * 
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Sets the value.
	 * 
	 * @param v
	 *            the new value
	 */
	public void setValue(String v) {
		value = v;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
}
