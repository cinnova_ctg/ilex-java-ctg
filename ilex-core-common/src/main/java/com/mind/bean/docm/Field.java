//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : Field.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.bean.docm;

import java.io.Serializable;

/**
 * The Class Field.
 */
public class Field implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The name. */
	private String name;

	/** The value. */
	private String value;

	/** The field_name. */
	private String field_name;

	/** The field_type. */
	private String field_type;

	/** The description. */
	private String description;

	/** The default_value. */
	private String default_value;

	/** The tax_group. */
	private String tax_group;

	/** The field_id. */
	private String field_id;

	/**
	 * Gets the default_value.
	 * 
	 * @return the default_value
	 */
	public String getDefault_value() {
		return default_value;
	}

	/**
	 * Sets the default_value.
	 * 
	 * @param default_value
	 *            the new default_value
	 */
	public void setDefault_value(String default_value) {
		this.default_value = default_value;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the field_name.
	 * 
	 * @return the field_name
	 */
	public String getField_name() {
		return field_name;
	}

	/**
	 * Sets the field_name.
	 * 
	 * @param field_name
	 *            the new field_name
	 */
	public void setField_name(String field_name) {
		this.field_name = field_name;
	}

	/**
	 * Gets the field_type.
	 * 
	 * @return the field_type
	 */
	public String getField_type() {
		return field_type;
	}

	/**
	 * Sets the field_type.
	 * 
	 * @param field_type
	 *            the new field_type
	 */
	public void setField_type(String field_type) {
		this.field_type = field_type;
	}

	/**
	 * Gets the tax_group.
	 * 
	 * @return the tax_group
	 */
	public String getTax_group() {
		return tax_group;
	}

	/**
	 * Sets the tax_group.
	 * 
	 * @param tax_group
	 *            the new tax_group
	 */
	public void setTax_group(String tax_group) {
		this.tax_group = tax_group;
	}

	/**
	 * Instantiates a new field.
	 * 
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 */
	public Field(String name, String value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * Instantiates a new field.
	 */
	public Field() {
		super();
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the field_id.
	 * 
	 * @return the field_id
	 */
	public String getField_id() {
		return field_id;
	}

	/**
	 * Sets the field_id.
	 * 
	 * @param field_id
	 *            the new field_id
	 */
	public void setField_id(String field_id) {
		this.field_id = field_id;
	}
}
