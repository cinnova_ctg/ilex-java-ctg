//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : LibraryView.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.bean.docm;

import java.io.Serializable;

import com.mind.bean.docm.FilterCriterion;
import com.mind.bean.docm.SortedColumn;

/**
 * The Class LibraryView.
 */
public class LibraryView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The displayed columns. */
	private Field[] displayedColumns;

	/** The sorted columns. */
	private SortedColumn[] sortedColumns;

	/** The grouped columns. */
	private Field[] groupedColumns;

	/** The filter criteria. */
	private FilterCriterion[] filterCriteria;

	/** The Library view id. */
	private String LibraryViewId = "";

	/** The Library view name. */
	private String LibraryViewName = "";

	/**
	 * Gets the displayed columns.
	 * 
	 * @return the displayed columns
	 */
	public Field[] getDisplayedColumns() {
		return displayedColumns;
	}

	/**
	 * Sets the displayed columns.
	 * 
	 * @param displayedColumns
	 *            the new displayed columns
	 */
	public void setDisplayedColumns(Field[] displayedColumns) {
		this.displayedColumns = displayedColumns;
	}

	/**
	 * Gets the filter criteria.
	 * 
	 * @return the filter criteria
	 */
	public FilterCriterion[] getFilterCriteria() {
		return filterCriteria;
	}

	/**
	 * Sets the filter criteria.
	 * 
	 * @param filterCriteria
	 *            the new filter criteria
	 */
	public void setFilterCriteria(FilterCriterion[] filterCriteria) {
		this.filterCriteria = filterCriteria;
	}

	/**
	 * Gets the grouped columns.
	 * 
	 * @return the grouped columns
	 */
	public Field[] getGroupedColumns() {
		return groupedColumns;
	}

	/**
	 * Sets the grouped columns.
	 * 
	 * @param groupedColumns
	 *            the new grouped columns
	 */
	public void setGroupedColumns(Field[] groupedColumns) {
		this.groupedColumns = groupedColumns;
	}

	/**
	 * Gets the sorted columns.
	 * 
	 * @return the sorted columns
	 */
	public SortedColumn[] getSortedColumns() {
		return sortedColumns;
	}

	/**
	 * Sets the sorted columns.
	 * 
	 * @param sortedColumns
	 *            the new sorted columns
	 */
	public void setSortedColumns(SortedColumn[] sortedColumns) {
		this.sortedColumns = sortedColumns;
	}

	/**
	 * Gets the library view id.
	 * 
	 * @return the library view id
	 */
	public String getLibraryViewId() {
		return LibraryViewId;
	}

	/**
	 * Sets the library view id.
	 * 
	 * @param libraryViewId
	 *            the new library view id
	 */
	public void setLibraryViewId(String libraryViewId) {
		LibraryViewId = libraryViewId;
	}

	/**
	 * Gets the library view name.
	 * 
	 * @return the library view name
	 */
	public String getLibraryViewName() {
		return LibraryViewName;
	}

	/**
	 * Sets the library view name.
	 * 
	 * @param libraryViewName
	 *            the new library view name
	 */
	public void setLibraryViewName(String libraryViewName) {
		LibraryViewName = libraryViewName;
	}
}
