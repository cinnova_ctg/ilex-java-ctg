//
//
//
//@ Project : DocM - Document Management System - Ilex
//@ File Name : IlexEntity.java
//@ Date : 12-June-2008
//@ Author : Tanuj Mittal
//
//

package com.mind.bean.docm;

import java.io.Serializable;


public class IlexEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String entityId = null;
	private String entityName = null;
	private String entityType = null;
	private String entityIdentifier = null;
	private String entityCreatedBy = null;
	private String entityCreatedDate = null;
	private String entityChangeDate = null;
	private String entityChangeBy = null;
	private String entityBaseDocument = null;
	private String entityMSAId = null;
	private String entityAppendixId = null;
	private String entityJobId = null;
	private String entityPoId = null;
	private String entityWoId = null;
	private String entityNetMedXReportId = null;
	private String entityMimeType = null;

	// New properties for Report Entity
	private String reportFltStartDate = null;
	private String reportFltEndDate = null;
	private String reportFltCriticality = null;
	private String reportFltReportStatus = null;
	private String reportFltRequestor = null;
	private String reportFltArrivalOptions = null;
	private String reportFltMsp = null;
	private String reportFltHelpDesk = null;

	private boolean isEntitySupplement;

	private EntitySupplementDocs entitySupplementDocs;

	// names of different fields
	private String msaName = null;
	private String appendixName = null;
	private String jobName = null;
	private String partnerName = null;
	private String woPartnerId = null;
	private String dtlNedxIdName = null;

	public String getEntityIdentifier() {
		return entityIdentifier;
	}

	public void setEntityIdentifier(String entityIdentifier) {
		this.entityIdentifier = entityIdentifier;
	}

	public EntitySupplementDocs getEntitySupplementDocs() {
		return entitySupplementDocs;
	}

	public void setEntitySupplementDocs(
			EntitySupplementDocs entitySupplementDocs) {
		this.entitySupplementDocs = entitySupplementDocs;
	}

	public String getEntityChangeBy() {
		return entityChangeBy;
	}

	public void setEntityChangeBy(String entityChangeBy) {
		this.entityChangeBy = entityChangeBy;
	}

	public String getEntityChangeDate() {
		return entityChangeDate;
	}

	public void setEntityChangeDate(String entityChangeDate) {
		this.entityChangeDate = entityChangeDate;
	}

	public String getEntityCreatedBy() {
		return entityCreatedBy;
	}

	public void setEntityCreatedBy(String entityCreatedBy) {
		this.entityCreatedBy = entityCreatedBy;
	}

	public String getEntityCreatedDate() {
		return entityCreatedDate;
	}

	public void setEntityCreatedDate(String entityCreatedDate) {
		this.entityCreatedDate = entityCreatedDate;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public boolean isEntitySupplement() {
		return isEntitySupplement;
	}

	public void setEntitySupplement(boolean isEntitySupplement) {
		this.isEntitySupplement = isEntitySupplement;
	}

	public String getEntityAppendixId() {
		return entityAppendixId;
	}

	public void setEntityAppendixId(String entityAppendixId) {
		this.entityAppendixId = entityAppendixId;
	}

	public String getEntityJobId() {
		return entityJobId;
	}

	public void setEntityJobId(String entityJobId) {
		this.entityJobId = entityJobId;
	}

	public String getEntityMSAId() {
		return entityMSAId;
	}

	public void setEntityMSAId(String entityMSAId) {
		this.entityMSAId = entityMSAId;
	}

	public String getEntityPoId() {
		return entityPoId;
	}

	public void setEntityPoId(String entityPoId) {
		this.entityPoId = entityPoId;
	}

	public String getEntityWoId() {
		return entityWoId;
	}

	public void setEntityWoId(String entityWoId) {
		this.entityWoId = entityWoId;
	}

	// Use Case
	/**
	 * @name equals
	 * @purpose This method is used to compare two individual strings
	 * @steps 1. IlexEntity.equals(Object) method initialtes this method from
	 *        within.
	 * @param String
	 * @param String
	 * @return boolean
	 * @returnDescription True/False depending on the comparison
	 */
	private boolean equals(String a, String b) {
		boolean equals = false;
		// check for both being null
		if (a == null && b == null)
			return true;
		// check for only one of them to be null
		if ((a == null && b != null) || (a != null && b == null))
			return false;
		// check for equals()
		return a.trim().equals(b.trim());
	}

	// Use Case
	/**
	 * @name equals
	 * @purpose This method is used to compare two IlexEntity Objects
	 * @steps 1. User request to get compare the two IlexEntity Objects.
	 * @param Object
	 * @return boolean
	 * @returnDescription True/False depending on the comparison
	 */
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof IlexEntity))
			return false;
		else {
			IlexEntity ilexEntity = (IlexEntity) obj;
			return equals(this.entityId, ilexEntity.getEntityId())
					&& equals(this.entityType, ilexEntity.getEntityType())
					&& equals(this.entityIdentifier,
							ilexEntity.getEntityIdentifier())
					&& equals(this.entityCreatedBy,
							ilexEntity.getEntityCreatedBy())
					&& equals(this.entityCreatedDate,
							ilexEntity.getEntityCreatedDate())
					&& equals(this.entityChangeBy,
							ilexEntity.getEntityChangeBy())
					&& equals(this.entityBaseDocument,
							ilexEntity.getEntityBaseDocument())
					&& equals(this.entityChangeDate,
							ilexEntity.getEntityChangeDate())
					&& equals(this.entityMSAId, ilexEntity.getEntityMSAId())
					&& equals(this.entityAppendixId,
							ilexEntity.getEntityAppendixId())
					&& equals(this.entityJobId, ilexEntity.getEntityJobId())
					&& equals(this.entityPoId, ilexEntity.getEntityPoId())
					&& equals(this.entityWoId, ilexEntity.getEntityWoId())
					&& equals(this.entityNetMedXReportId,
							ilexEntity.getEntityNetMedXReportId())
					&& equals(this.reportFltStartDate,
							ilexEntity.getReportFltStartDate())
					&& equals(this.reportFltEndDate,
							ilexEntity.getReportFltEndDate())
					&& equals(this.reportFltCriticality,
							ilexEntity.getReportFltCriticality())
					&& equals(this.reportFltReportStatus,
							ilexEntity.getReportFltReportStatus())
					&& equals(this.reportFltRequestor,
							ilexEntity.getReportFltRequestor())
					&& equals(this.reportFltArrivalOptions,
							ilexEntity.getReportFltArrivalOptions())
					&& equals(this.reportFltMsp, ilexEntity.getReportFltMsp())
					&& equals(this.reportFltReportStatus,
							ilexEntity.getReportFltReportStatus())
					&& equals(this.reportFltHelpDesk,
							ilexEntity.getReportFltHelpDesk())
					&& equals(this.msaName, ilexEntity.getMsaName())
					&& equals(this.appendixName, ilexEntity.getAppendixName())
					&& equals(this.jobName, ilexEntity.getJobName())
					&& equals(this.partnerName, ilexEntity.getPartnerName())
					&& equals(this.woPartnerId, ilexEntity.getWoPartnerId())
					&& equals(this.dtlNedxIdName, ilexEntity.getDtlNedxIdName());
		}
	}

	// Use Case
	/**
	 * @name getDefaltLibraryName
	 * @purpose This method is used to get the default library name
	 *          corresponding to the entity.
	 * @steps 1. User request to get the default Library Name.
	 * @param entityType
	 * @return String
	 * @returnDescription The default library name of the Entity.
	 */

	public String toString() {
		return this.getEntityId() + "\t" + this.getEntityType() + "\t"
				+ this.getEntityIdentifier() + "\t" + this.getEntityCreatedBy()
				+ "\t" + this.getEntityCreatedDate() + "\t"
				+ this.getEntityChangeBy() + "\t"
				+ this.getEntityBaseDocument() + "\t"
				+ this.getEntityChangeDate() + "\t" + this.getEntityMSAId()
				+ "\t" + this.getEntityAppendixId() + "\t"
				+ this.getEntityJobId() + "\t" + this.getEntityPoId() + "\t"
				+ this.getEntityWoId() + "\t" + this.getEntityNetMedXReportId()
				+ "\t" + this.getReportFltStartDate() + "\t"
				+ this.getReportFltEndDate() + "\t"
				+ this.getReportFltCriticality() + "\t"
				+ this.getReportFltReportStatus() + "\t"
				+ this.getReportFltRequestor() + "\t"
				+ this.getReportFltArrivalOptions() + "\t"
				+ this.getReportFltMsp() + "\t"
				+ this.getReportFltReportStatus() + "\t"
				+ this.getReportFltHelpDesk() + "\t" + this.getMsaName() + "\t"
				+ this.getAppendixName() + "\t" + this.getJobName() + "\t"
				+ this.getPartnerName() + "\t" + this.getWoPartnerId() + "\t"
				+ this.getDtlNedxIdName() + "\t\r\n";
	}

	public String getDefaltLibraryName(String entityType) {
		return entityType;
	}

	public String getReportFltArrivalOptions() {
		return reportFltArrivalOptions;
	}

	public void setReportFltArrivalOptions(String reportFltArrivalOptions) {
		this.reportFltArrivalOptions = reportFltArrivalOptions;
	}

	public String getReportFltCriticality() {
		return reportFltCriticality;
	}

	public void setReportFltCriticality(String reportFltCriticality) {
		this.reportFltCriticality = reportFltCriticality;
	}

	public String getReportFltEndDate() {
		return reportFltEndDate;
	}

	public void setReportFltEndDate(String reportFltEndDate) {
		this.reportFltEndDate = reportFltEndDate;
	}

	public String getReportFltHelpDesk() {
		return reportFltHelpDesk;
	}

	public void setReportFltHelpDesk(String reportFltHelpDesk) {
		this.reportFltHelpDesk = reportFltHelpDesk;
	}

	public String getReportFltMsp() {
		return reportFltMsp;
	}

	public void setReportFltMsp(String reportFltMsp) {
		this.reportFltMsp = reportFltMsp;
	}

	public String getReportFltReportStatus() {
		return reportFltReportStatus;
	}

	public void setReportFltReportStatus(String reportFltReportStatus) {
		this.reportFltReportStatus = reportFltReportStatus;
	}

	public String getReportFltRequestor() {
		return reportFltRequestor;
	}

	public void setReportFltRequestor(String reportFltRequestor) {
		this.reportFltRequestor = reportFltRequestor;
	}

	public String getReportFltStartDate() {
		return reportFltStartDate;
	}

	public void setReportFltStartDate(String reportFltStartDate) {
		this.reportFltStartDate = reportFltStartDate;
	}

	public String getEntityNetMedXReportId() {
		return entityNetMedXReportId;
	}

	public void setEntityNetMedXReportId(String entityNetMedXReportId) {
		this.entityNetMedXReportId = entityNetMedXReportId;
	}

	public String getEntityBaseDocument() {
		return entityBaseDocument;
	}

	public void setEntityBaseDocument(String entityBaseDocument) {
		this.entityBaseDocument = entityBaseDocument;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getDtlNedxIdName() {
		return dtlNedxIdName;
	}

	public void setDtlNedxIdName(String dtlNedxIdName) {
		this.dtlNedxIdName = dtlNedxIdName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getWoPartnerId() {
		return woPartnerId;
	}

	public void setWoPartnerId(String woPartnerId) {
		this.woPartnerId = woPartnerId;
	}

	public String getEntityMimeType() {
		return entityMimeType;
	}

	public void setEntityMimeType(String entityMimeType) {
		this.entityMimeType = entityMimeType;
	}
}
