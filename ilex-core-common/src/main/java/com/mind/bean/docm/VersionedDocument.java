//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : VersionedDocument.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.bean.docm;



/**
 * The Class VersionedDocument.
 */
public class VersionedDocument extends Document {

	/** The document id. */
	private String documentId;

	/** The document versions. */
	private DocumentVersion[] documentVersions;

	/**
	 * Gets the document id.
	 * 
	 * @return the document id
	 */
	public String getDocumentId() {
		return documentId;
	}

	/**
	 * Sets the document id.
	 * 
	 * @param documentId
	 *            the new document id
	 */
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	/**
	 * Gets the document versions.
	 * 
	 * @return the document versions
	 */
	public DocumentVersion[] getDocumentVersions() {
		return documentVersions;
	}

	/**
	 * Sets the document versions.
	 * 
	 * @param documentVersions
	 *            the new document versions
	 */
	public void setDocumentVersions(DocumentVersion[] documentVersions) {
		this.documentVersions = documentVersions;
	}
}
