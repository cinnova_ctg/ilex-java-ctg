//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : SortElement.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.bean.docm;

import java.io.Serializable;


/**
 * The Class SortedColumn.
 */
public class SortedColumn implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The column. */
	private Field column;

	/** The order type. */
	private String orderType;

	/**
	 * Gets the column.
	 * 
	 * @return the column
	 */
	public Field getColumn() {
		return column;
	}

	/**
	 * Sets the column.
	 * 
	 * @param column
	 *            the new column
	 */
	public void setColumn(Field column) {
		this.column = column;
	}

	/**
	 * Gets the order type.
	 * 
	 * @return the order type
	 */
	public String getOrderType() {
		return orderType;
	}

	/**
	 * Sets the order type.
	 * 
	 * @param orderType
	 *            the new order type
	 */
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
}
