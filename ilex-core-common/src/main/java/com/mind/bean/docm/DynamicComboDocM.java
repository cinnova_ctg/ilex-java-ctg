//
//
//
//@ Project : DocM - Document Management System - Ilex
//@ File Name : EntityDescriptionDao.java
//@ Date : 12-June-2008
//@ Author : Vishal Kapoor
//
//
package com.mind.bean.docm;

import java.io.Serializable;
import java.util.Collection;

/**
 * The Class DynamicComboDocM.
 */
public class DynamicComboDocM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The msalist. */
	private Collection msalist = null;

	/** The reportlist. */
	private Collection reportlist = null;

	/** The monthnamelist. */
	private Collection monthnamelist = null;

	/** The liblist. */
	private Collection liblist = null;

	/** The fieldlist. */
	private Collection fieldlist = null;

	/** The rel opt cmb. */
	private Collection relOptCmb = null;

	/** The logic opt cmb. */
	private Collection logicOptCmb = null;

	/** The toolslist. */
	private Collection toolslist = null;

	/**
	 * Gets the toolslist.
	 * 
	 * @return the toolslist
	 */
	public Collection getToolslist() {
		return toolslist;
	}

	/**
	 * Sets the toolslist.
	 * 
	 * @param toolslist
	 *            the new toolslist
	 */
	public void setToolslist(Collection toolslist) {
		this.toolslist = toolslist;
	}

	/**
	 * Gets the logic opt cmb.
	 * 
	 * @return the logic opt cmb
	 */
	public Collection getLogicOptCmb() {
		return logicOptCmb;
	}

	/**
	 * Sets the logic opt cmb.
	 * 
	 * @param logicOptCmb
	 *            the new logic opt cmb
	 */
	public void setLogicOptCmb(Collection logicOptCmb) {
		this.logicOptCmb = logicOptCmb;
	}

	/**
	 * Gets the fieldlist.
	 * 
	 * @return the fieldlist
	 */
	public Collection getFieldlist() {
		return fieldlist;
	}

	/**
	 * Sets the fieldlist.
	 * 
	 * @param fieldlist
	 *            the new fieldlist
	 */
	public void setFieldlist(Collection fieldlist) {
		this.fieldlist = fieldlist;
	}

	/**
	 * Gets the liblist.
	 * 
	 * @return the liblist
	 */
	public Collection getLiblist() {
		return liblist;
	}

	/**
	 * Sets the liblist.
	 * 
	 * @param liblist
	 *            the new liblist
	 */
	public void setLiblist(Collection liblist) {
		this.liblist = liblist;
	}

	/**
	 * Gets the monthnamelist.
	 * 
	 * @return the monthnamelist
	 */
	public Collection getMonthnamelist() {
		return monthnamelist;
	}

	/**
	 * Sets the monthnamelist.
	 * 
	 * @param monthnamelist
	 *            the new monthnamelist
	 */
	public void setMonthnamelist(Collection monthnamelist) {
		this.monthnamelist = monthnamelist;
	}

	/**
	 * Gets the msalist.
	 * 
	 * @return the msalist
	 */
	public Collection getMsalist() {
		return msalist;
	}

	/**
	 * Sets the msalist.
	 * 
	 * @param msalist
	 *            the new msalist
	 */
	public void setMsalist(Collection msalist) {
		this.msalist = msalist;
	}

	/**
	 * Gets the reportlist.
	 * 
	 * @return the reportlist
	 */
	public Collection getReportlist() {
		return reportlist;
	}

	/**
	 * Sets the reportlist.
	 * 
	 * @param reportlist
	 *            the new reportlist
	 */
	public void setReportlist(Collection reportlist) {
		this.reportlist = reportlist;
	}

	/**
	 * Gets the rel opt cmb.
	 * 
	 * @return the rel opt cmb
	 */
	public Collection getRelOptCmb() {
		return relOptCmb;
	}

	/**
	 * Sets the rel opt cmb.
	 * 
	 * @param relOptCmb
	 *            the new rel opt cmb
	 */
	public void setRelOptCmb(Collection relOptCmb) {
		this.relOptCmb = relOptCmb;
	}
	/** The librarylist. */
	private Collection librarylist = null;

	/**
	 * Gets the librarylist.
	 * 
	 * @return the librarylist
	 */
	public Collection getLibrarylist() {
		return librarylist;
	}

	/**
	 * Sets the librarylist.
	 * 
	 * @param librarylist
	 *            the new librarylist
	 */
	public void setLibrarylist(Collection librarylist) {
		this.librarylist = librarylist;
	}

}
