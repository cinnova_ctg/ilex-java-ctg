//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : Library.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.bean.docm;

import java.io.Serializable;



/**
 * The Class Library.
 */
public class Library implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The library id. */
	private String libraryId;

	/** The library name. */
	private String libraryName;

	/** The library char. */
	private String libraryChar;

	/** The notify mail id. */
	private String notifyMailId;

	/** The can notify. */
	private boolean canNotify;

	/** The views. */
	private LibraryView[] views;

	/** The schema setup fields. */
	private SchemaSetupField[] schemaSetupFields;

	/** The documents. */
	private VersionedDocument[] documents;

	/**
	 * Gets the library id.
	 * 
	 * @return the library id
	 */
	public String getLibraryId() {
		return libraryId;
	}

	/**
	 * Sets the library id.
	 * 
	 * @param libraryId
	 *            the new library id
	 */
	public void setLibraryId(String libraryId) {
		this.libraryId = libraryId;
	}

	/**
	 * Notify by mail.
	 */
	public void notifyByMail() {

	}

	/**
	 * Checks if is can notify.
	 * 
	 * @return true, if is can notify
	 */
	public boolean isCanNotify() {
		return canNotify;
	}

	/**
	 * Sets the can notify.
	 * 
	 * @param canNotify
	 *            the new can notify
	 */
	public void setCanNotify(boolean canNotify) {
		this.canNotify = canNotify;
	}

	/**
	 * Gets the documents.
	 * 
	 * @return the documents
	 */
	public VersionedDocument[] getDocuments() {
		return documents;
	}

	/**
	 * Sets the documents.
	 * 
	 * @param documents
	 *            the new documents
	 */
	public void setDocuments(VersionedDocument[] documents) {
		this.documents = documents;
	}

	/**
	 * Gets the library name.
	 * 
	 * @return the library name
	 */
	public String getLibraryName() {
		return libraryName;
	}

	/**
	 * Sets the library name.
	 * 
	 * @param libraryName
	 *            the new library name
	 */
	public void setLibraryName(String libraryName) {
		this.libraryName = libraryName;
	}

	/**
	 * Gets the notify mail id.
	 * 
	 * @return the notify mail id
	 */
	public String getNotifyMailId() {
		return notifyMailId;
	}

	/**
	 * Sets the notify mail id.
	 * 
	 * @param notifyMailId
	 *            the new notify mail id
	 */
	public void setNotifyMailId(String notifyMailId) {
		this.notifyMailId = notifyMailId;
	}

	/**
	 * Gets the schema setup fields.
	 * 
	 * @return the schema setup fields
	 */
	public SchemaSetupField[] getSchemaSetupFields() {
		return schemaSetupFields;
	}

	/**
	 * Sets the schema setup elements.
	 * 
	 * @param schemaSetupFields
	 *            the new schema setup elements
	 */
	public void setSchemaSetupElements(SchemaSetupField[] schemaSetupFields) {
		this.schemaSetupFields = schemaSetupFields;
	}

	/**
	 * Gets the views.
	 * 
	 * @return the views
	 */
	public LibraryView[] getViews() {
		return views;
	}

	/**
	 * Sets the views.
	 * 
	 * @param views
	 *            the new views
	 */
	public void setViews(LibraryView[] views) {
		this.views = views;
	}

	/**
	 * Gets the library char.
	 * 
	 * @return the library char
	 */
	public String getLibraryChar() {
		return libraryChar;
	}

	/**
	 * Sets the library char.
	 * 
	 * @param libraryChar
	 *            the new library char
	 */
	public void setLibraryChar(String libraryChar) {
		this.libraryChar = libraryChar;
	}
}
