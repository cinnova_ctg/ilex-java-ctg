//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : Document.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.bean.docm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

import org.apache.log4j.Logger;


import com.mind.docm.constants.GlobalConstants;

/**
 * The Class Document.
 */
public class Document implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Document.class);

	/** The file bytes. */
	private byte[] fileBytes;

	/** The file name. */
	private String fileName;

	/** The mime type. */
	private String mimeType;

	/** The meta data. */
	private MetaData metaData;

	/**
	 * Gets the file.
	 * 
	 * @param fileBytes
	 *            the file bytes
	 * 
	 * @return the file
	 */
	public File getFile(byte[] fileBytes) {
		File file = new File(GlobalConstants.filePath + getFileName());
		try {
			FileOutputStream out = new FileOutputStream(file);
			out.write(fileBytes);
		} catch (FileNotFoundException e) {
			logger.error("getFile(byte[])", e);

			logger.error("getFile(byte[])", e);
		} catch (IOException e) {
			logger.error("getFile(byte[])", e);

			logger.error("getFile(byte[])", e);
		}
		return file;
	}

	/**
	 * Gets the meta data.
	 * 
	 * @return the meta data
	 */
	public MetaData getMetaData() {
		return metaData;
	}

	/**
	 * Sets the meta data.
	 * 
	 * @param metaData
	 *            the new meta data
	 */
	public void setMetaData(MetaData metaData) {
		this.metaData = metaData;
	}

	/**
	 * Gets the file bytes.
	 * 
	 * @return the file bytes
	 */
	public byte[] getFileBytes() {
		return fileBytes;
	}

	/**
	 * Sets the file bytes.
	 * 
	 * @param fileBytes
	 *            the new file bytes
	 */
	public void setFileBytes(byte[] fileBytes) {
		this.fileBytes = fileBytes;
	}

	/**
	 * Gets the file name.
	 * 
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 * 
	 * @param fileName
	 *            the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the mime type.
	 * 
	 * @return the mime type
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Sets the mime type.
	 * 
	 * @param mimeType
	 *            the new mime type
	 */
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
}
