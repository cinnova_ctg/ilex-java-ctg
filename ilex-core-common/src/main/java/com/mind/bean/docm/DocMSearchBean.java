package com.mind.bean.docm;

import java.io.Serializable;

public class DocMSearchBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The selected msa. */
	private String selectedMSA;

	/** The add conditiion. */
	private String addConditiion = null;

	/** The search now. */
	private String searchNow = null;

	/** The link to doc. */
	private String linkToDoc = null;

	/** The selected filter columns. */
	private String[] selectedFilterColumns = null;

	/** The filter condition. */
	private String[] filterCondition = null;

	/** The logical condition. */
	private String[] logicalCondition = null;

	/** The filter type. */
	private String[] filterType = null;

	/** The filter id. */
	private String[] filterId = null;

	/** The field type id. */
	private String[] fieldTypeId = null;

	/** The add more. */
	private String addMore = null;

	// For Menu's
	/** The msa id. */
	private String msaId = null;

	/** The msa name. */
	private String msaName = null;

	/** The appendix id. */
	private String appendixId = null;

	/** The appendix name. */
	private String appendixName = null;

	/** The job id. */
	private String jobId = null;

	/** The job name. */
	private String jobName = null;

	/** The link lib name from ilex. */
	private String linkLibNameFromIlex = "";

	/** The appendix status checking. */
	private String appendixStatusChecking = null;

	/** The entity id. */
	private String entityId = null;

	/** The entity type. */
	private String entityType = null;

	/** The source type. */
	private String sourceType = null;

	/** The appendix type. */
	private String appendixType = null;

	/** The ticket_type. */
	private String ticket_type = null;

	/** The ticket_id. */
	private String ticket_id = null;

	/** The showhidesearch. */
	private String showhidesearch = "";

	/** The is show. */
	private String isShow;

	/** The show all columns. */
	private String showAllColumns = null;

	/**
	 * Gets the show all columns.
	 * 
	 * @return the show all columns
	 */
	public String getShowAllColumns() {
		return showAllColumns;
	}

	/**
	 * Sets the show all columns.
	 * 
	 * @param showAllColumns
	 *            the new show all columns
	 */
	public void setShowAllColumns(String showAllColumns) {
		this.showAllColumns = showAllColumns;
	}

	/**
	 * Gets the adds the more.
	 * 
	 * @return the adds the more
	 */
	public String getAddMore() {
		return addMore;
	}

	/**
	 * Sets the adds the more.
	 * 
	 * @param addMore
	 *            the new adds the more
	 */
	public void setAddMore(String addMore) {
		this.addMore = addMore;
	}

	/**
	 * Gets the adds the conditiion.
	 * 
	 * @return the adds the conditiion
	 */
	public String getAddConditiion() {
		return addConditiion;
	}

	/**
	 * Sets the adds the conditiion.
	 * 
	 * @param addConditiion
	 *            the new adds the conditiion
	 */
	public void setAddConditiion(String addConditiion) {
		this.addConditiion = addConditiion;
	}

	/**
	 * Gets the search now.
	 * 
	 * @return the search now
	 */
	public String getSearchNow() {
		return searchNow;
	}

	/**
	 * Sets the search now.
	 * 
	 * @param searchNow
	 *            the new search now
	 */
	public void setSearchNow(String searchNow) {
		this.searchNow = searchNow;
	}

	/**
	 * Gets the selected msa.
	 * 
	 * @return the selected msa
	 */
	public String getSelectedMSA() {
		return selectedMSA;
	}

	/**
	 * Sets the selected msa.
	 * 
	 * @param selectedMSA
	 *            the new selected msa
	 */
	public void setSelectedMSA(String selectedMSA) {
		this.selectedMSA = selectedMSA;
	}

	/**
	 * Gets the filter condition.
	 * 
	 * @return the filter condition
	 */
	public String[] getFilterCondition() {
		return filterCondition;
	}

	/**
	 * Sets the filter condition.
	 * 
	 * @param filterCondition
	 *            the new filter condition
	 */
	public void setFilterCondition(String[] filterCondition) {
		this.filterCondition = filterCondition;
	}

	/**
	 * Gets the filter id.
	 * 
	 * @return the filter id
	 */
	public String[] getFilterId() {
		return filterId;
	}

	/**
	 * Sets the filter id.
	 * 
	 * @param filterId
	 *            the new filter id
	 */
	public void setFilterId(String[] filterId) {
		this.filterId = filterId;
	}

	/**
	 * Gets the filter type.
	 * 
	 * @return the filter type
	 */
	public String[] getFilterType() {
		return filterType;
	}

	/**
	 * Sets the filter type.
	 * 
	 * @param filterType
	 *            the new filter type
	 */
	public void setFilterType(String[] filterType) {
		this.filterType = filterType;
	}

	/**
	 * Gets the logical condition.
	 * 
	 * @return the logical condition
	 */
	public String[] getLogicalCondition() {
		return logicalCondition;
	}

	/**
	 * Sets the logical condition.
	 * 
	 * @param logicalCondition
	 *            the new logical condition
	 */
	public void setLogicalCondition(String[] logicalCondition) {
		this.logicalCondition = logicalCondition;
	}

	/**
	 * Gets the selected filter columns.
	 * 
	 * @return the selected filter columns
	 */
	public String[] getSelectedFilterColumns() {
		return selectedFilterColumns;
	}

	/**
	 * Sets the selected filter columns.
	 * 
	 * @param selectedFilterColumns
	 *            the new selected filter columns
	 */
	public void setSelectedFilterColumns(String[] selectedFilterColumns) {
		this.selectedFilterColumns = selectedFilterColumns;
	}

	/**
	 * Gets the field type id.
	 * 
	 * @return the field type id
	 */
	public String[] getFieldTypeId() {
		return fieldTypeId;
	}

	/**
	 * Sets the field type id.
	 * 
	 * @param fieldTypeId
	 *            the new field type id
	 */
	public void setFieldTypeId(String[] fieldTypeId) {
		this.fieldTypeId = fieldTypeId;
	}

	/**
	 * Gets the link to doc.
	 * 
	 * @return the link to doc
	 */
	public String getLinkToDoc() {
		return linkToDoc;
	}

	/**
	 * Sets the link to doc.
	 * 
	 * @param linkToDoc
	 *            the new link to doc
	 */
	public void setLinkToDoc(String linkToDoc) {
		this.linkToDoc = linkToDoc;
	}

	/**
	 * Gets the appendix id.
	 * 
	 * @return the appendix id
	 */
	public String getAppendixId() {
		return appendixId;
	}

	/**
	 * Sets the appendix id.
	 * 
	 * @param appendixId
	 *            the new appendix id
	 */
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	/**
	 * Gets the appendix name.
	 * 
	 * @return the appendix name
	 */
	public String getAppendixName() {
		return appendixName;
	}

	/**
	 * Sets the appendix name.
	 * 
	 * @param appendixName
	 *            the new appendix name
	 */
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	/**
	 * Gets the job id.
	 * 
	 * @return the job id
	 */
	public String getJobId() {
		return jobId;
	}

	/**
	 * Sets the job id.
	 * 
	 * @param jobId
	 *            the new job id
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	/**
	 * Gets the job name.
	 * 
	 * @return the job name
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * Sets the job name.
	 * 
	 * @param jobName
	 *            the new job name
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * Gets the msa id.
	 * 
	 * @return the msa id
	 */
	public String getMsaId() {
		return msaId;
	}

	/**
	 * Sets the msa id.
	 * 
	 * @param msaId
	 *            the new msa id
	 */
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	/**
	 * Gets the msa name.
	 * 
	 * @return the msa name
	 */
	public String getMsaName() {
		return msaName;
	}

	/**
	 * Sets the msa name.
	 * 
	 * @param msaName
	 *            the new msa name
	 */
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	/**
	 * Gets the link lib name from ilex.
	 * 
	 * @return the link lib name from ilex
	 */
	public String getLinkLibNameFromIlex() {
		return linkLibNameFromIlex;
	}

	/**
	 * Sets the link lib name from ilex.
	 * 
	 * @param linkLibNameFromIlex
	 *            the new link lib name from ilex
	 */
	public void setLinkLibNameFromIlex(String linkLibNameFromIlex) {
		this.linkLibNameFromIlex = linkLibNameFromIlex;
	}

	/**
	 * Gets the appendix status checking.
	 * 
	 * @return the appendix status checking
	 */
	public String getAppendixStatusChecking() {
		return appendixStatusChecking;
	}

	/**
	 * Sets the appendix status checking.
	 * 
	 * @param appendixStatusChecking
	 *            the new appendix status checking
	 */
	public void setAppendixStatusChecking(String appendixStatusChecking) {
		this.appendixStatusChecking = appendixStatusChecking;
	}

	/**
	 * Gets the entity id.
	 * 
	 * @return the entity id
	 */
	public String getEntityId() {
		return entityId;
	}

	/**
	 * Sets the entity id.
	 * 
	 * @param entityId
	 *            the new entity id
	 */
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	/**
	 * Gets the entity type.
	 * 
	 * @return the entity type
	 */
	public String getEntityType() {
		return entityType;
	}

	/**
	 * Sets the entity type.
	 * 
	 * @param entityType
	 *            the new entity type
	 */
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	/**
	 * Gets the source type.
	 * 
	 * @return the source type
	 */
	public String getSourceType() {
		return sourceType;
	}

	/**
	 * Sets the source type.
	 * 
	 * @param sourceType
	 *            the new source type
	 */
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	/**
	 * Gets the appendix type.
	 * 
	 * @return the appendix type
	 */
	public String getAppendixType() {
		return appendixType;
	}

	/**
	 * Sets the appendix type.
	 * 
	 * @param appendixType
	 *            the new appendix type
	 */
	public void setAppendixType(String appendixType) {
		this.appendixType = appendixType;
	}

	/**
	 * Gets the ticket_id.
	 * 
	 * @return the ticket_id
	 */
	public String getTicket_id() {
		return ticket_id;
	}

	/**
	 * Sets the ticket_id.
	 * 
	 * @param ticket_id
	 *            the new ticket_id
	 */
	public void setTicket_id(String ticket_id) {
		this.ticket_id = ticket_id;
	}

	/**
	 * Gets the ticket_type.
	 * 
	 * @return the ticket_type
	 */
	public String getTicket_type() {
		return ticket_type;
	}

	/**
	 * Sets the ticket_type.
	 * 
	 * @param ticket_type
	 *            the new ticket_type
	 */
	public void setTicket_type(String ticket_type) {
		this.ticket_type = ticket_type;
	}

	/**
	 * Gets the showhidesearch.
	 * 
	 * @return the showhidesearch
	 */
	public String getShowhidesearch() {
		return showhidesearch;
	}

	/**
	 * Sets the showhidesearch.
	 * 
	 * @param showhidesearch
	 *            the new showhidesearch
	 */
	public void setShowhidesearch(String showhidesearch) {
		this.showhidesearch = showhidesearch;
	}

	/**
	 * Gets the checks if is show.
	 * 
	 * @return the checks if is show
	 */
	public String getIsShow() {
		return isShow;
	}

	/**
	 * Sets the checks if is show.
	 * 
	 * @param isShow
	 *            the new checks if is show
	 */
	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}

}
