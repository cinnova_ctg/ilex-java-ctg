package com.mind.bean.docm;

import java.io.Serializable;

public class EntitySupplementDocs implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fileId;
	private String entityId;
	private String entityType;
	private String fileName;
	private String fileRemarks;
	private byte[] fileData;
	private String fileCreatedBy;
	private String fileCreatedDate;
	private String mimeType = null;

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getFileCreatedBy() {
		return fileCreatedBy;
	}

	public void setFileCreatedBy(String fileCreatedBy) {
		this.fileCreatedBy = fileCreatedBy;
	}

	public String getFileCreatedDate() {
		return fileCreatedDate;
	}

	public void setFileCreatedDate(String fileCreatedDate) {
		this.fileCreatedDate = fileCreatedDate;
	}

	public byte[] getFileData() {
		return fileData;
	}

	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileRemarks() {
		return fileRemarks;
	}

	public void setFileRemarks(String fileRemarks) {
		this.fileRemarks = fileRemarks;
	}

}
