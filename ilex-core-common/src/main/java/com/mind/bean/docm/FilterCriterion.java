//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : FilterCriterion.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.bean.docm;

import java.io.Serializable;


/**
 * The Class FilterCriterion.
 */
public class FilterCriterion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The field. */
	private Field field;

	/** The condition type. */
	private String conditionType;

	/** The filter value. */
	private String filterValue;

	/** The pre operator. */
	private String preOperator;

	/**
	 * Gets the condition type.
	 * 
	 * @return the condition type
	 */
	public String getConditionType() {
		return conditionType;
	}

	/**
	 * Sets the condition type.
	 * 
	 * @param conditionType
	 *            the new condition type
	 */
	public void setConditionType(String conditionType) {
		this.conditionType = conditionType;
	}

	/**
	 * Gets the field.
	 * 
	 * @return the field
	 */
	public Field getField() {
		return field;
	}

	/**
	 * Sets the field.
	 * 
	 * @param field
	 *            the new field
	 */
	public void setField(Field field) {
		this.field = field;
	}

	/**
	 * Gets the filter value.
	 * 
	 * @return the filter value
	 */
	public String getFilterValue() {
		return filterValue;
	}

	/**
	 * Sets the filter value.
	 * 
	 * @param filterValue
	 *            the new filter value
	 */
	public void setFilterValue(String filterValue) {
		this.filterValue = filterValue;
	}

	/**
	 * Gets the pre operator.
	 * 
	 * @return the pre operator
	 */
	public String getPreOperator() {
		return preOperator;
	}

	/**
	 * Sets the pre operator.
	 * 
	 * @param preOperator
	 *            the new pre operator
	 */
	public void setPreOperator(String preOperator) {
		this.preOperator = preOperator;
	}
}
