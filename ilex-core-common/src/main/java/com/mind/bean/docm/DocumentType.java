package com.mind.bean.docm;

import java.io.Serializable;

/**
 * The Class DocumentType.
 */
public class DocumentType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The Id. */
	private String Id = null;

	/** The job id. */
	private String jobId = null;

	/** The type. */
	private String type = null;

	/** The from type. */
	private String fromType = null;

	/** The buffer. */
	private byte[] buffer = null;

	/** The is xml modified. */
	private String isXmlModified = null;

	/** The doc id. */
	private String docId = null;

	/** The file name. */
	private String fileName = null;

	/**
	 * Gets the from type.
	 * 
	 * @return the from type
	 */
	public String getFromType() {
		return fromType;
	}

	/**
	 * Sets the from type.
	 * 
	 * @param fromType
	 *            the new from type
	 */
	public void setFromType(String fromType) {
		this.fromType = fromType;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return Id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		Id = id;
	}

	/**
	 * Gets the job id.
	 * 
	 * @return the job id
	 */
	public String getJobId() {
		return jobId;
	}

	/**
	 * Sets the job id.
	 * 
	 * @param jobId
	 *            the new job id
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the buffer.
	 * 
	 * @return the buffer
	 */
	public byte[] getBuffer() {
		return buffer;
	}

	/**
	 * Sets the buffer.
	 * 
	 * @param buffer
	 *            the new buffer
	 */
	public void setBuffer(byte[] buffer) {
		this.buffer = buffer;
	}

	/**
	 * Gets the checks if is xml modified.
	 * 
	 * @return the checks if is xml modified
	 */
	public String getIsXmlModified() {
		return isXmlModified;
	}

	/**
	 * Sets the checks if is xml modified.
	 * 
	 * @param isXmlModified
	 *            the new checks if is xml modified
	 */
	public void setIsXmlModified(String isXmlModified) {
		this.isXmlModified = isXmlModified;
	}

	/**
	 * Gets the doc id.
	 * 
	 * @return the doc id
	 */
	public String getDocId() {
		return docId;
	}

	/**
	 * Sets the doc id.
	 * 
	 * @param docId
	 *            the new doc id
	 */
	public void setDocId(String docId) {
		this.docId = docId;
	}

	/**
	 * Gets the file name.
	 * 
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 * 
	 * @param fileName
	 *            the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
