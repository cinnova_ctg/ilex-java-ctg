//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : DocumentVersion.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.bean.docm;

import java.io.Serializable;

import com.mind.docm.util.VersionSchema;

/**
 * The Class DocumentVersion.
 */
public class DocumentVersion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The version. */
	private String version;

	/** The version schema. */
	private VersionSchema versionSchema;

	/** The document. */
	private Document document;

	/**
	 * Gets the document.
	 * 
	 * @return the document
	 */
	public Document getDocument() {
		return document;
	}

	/**
	 * Sets the document.
	 * 
	 * @param document
	 *            the new document
	 */
	public void setDocument(Document document) {
		this.document = document;
	}

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 * 
	 * @param version
	 *            the new version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Gets the version schema.
	 * 
	 * @return the version schema
	 */
	public VersionSchema getVersionSchema() {
		return versionSchema;
	}

	/**
	 * Sets the version schema.
	 * 
	 * @param versionSchema
	 *            the new version schema
	 */
	public void setVersionSchema(VersionSchema versionSchema) {
		this.versionSchema = versionSchema;
	}
}
