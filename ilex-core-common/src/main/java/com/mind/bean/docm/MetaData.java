//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : MetaData.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.bean.docm;

import java.io.Serializable;


/**
 * The Class MetaData.
 */
public class MetaData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** The fields. */
	private Field[] fields;

	/**
	 * Gets the fields.
	 * 
	 * @return the fields
	 */
	public Field[] getFields() {
		return fields;
	}

	/**
	 * Sets the fields.
	 * 
	 * @param fields
	 *            the new fields
	 */
	public void setFields(Field[] fields) {
		this.fields = fields;
	}
}
