package com.mind.bean;

import java.io.Serializable;

public class PVSDepotFacility implements Serializable {

	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	String chkzipECDF = null;
	String zipCodeECDF = null;
	String label = null;

	public PVSDepotFacility(String tools, String label, String zips) {
		chkzipECDF = tools;
		zipCodeECDF = zips;
		this.label = label;
	}

	public PVSDepotFacility() {

	}

	public String getchkzipECDF() {
		return chkzipECDF;
	}

	public void setchkzipECDF(String chkzipECDF) {
		this.chkzipECDF = chkzipECDF;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getzipCodeECDF() {
		return zipCodeECDF;
	}

	public void setzipCodeECDF(String zipCodeECDF) {
		this.zipCodeECDF = zipCodeECDF;
	}

}
