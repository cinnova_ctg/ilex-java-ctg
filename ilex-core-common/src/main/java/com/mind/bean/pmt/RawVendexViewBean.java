package com.mind.bean.pmt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class RawVendexViewBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String endCustomer = null;
	private String zipCode = null;
	private String radius = null;
	private String searchMap = null;
	private String msaId = null;
	private String msaName = null;
	private String isMsa =  null;
	private Collection endCustomeList = null;
	private Collection radiusList = null;
	private ArrayList msaList = null;
	private String[] check = null;
	private String saveEndCustomer = null;
	private String countendCustomer = null;
	
	public String getCountendCustomer() {
		return countendCustomer;
	}
	public void setCountendCustomer(String countendCustomer) {
		this.countendCustomer = countendCustomer;
	}
	public String getSaveEndCustomer() {
		return saveEndCustomer;
	}
	public void setSaveEndCustomer(String saveEndCustomer) {
		this.saveEndCustomer = saveEndCustomer;
	}
	public String[] getCheck() {
		return check;
	}
	public void setCheck(String[] check) {
		this.check = check;
	}
	public String getCheck(int i) {
		return this.check[i] = check[i];
	}
	public Collection getRadiusList() {
		return radiusList;
	}
	public void setRadiusList(Collection radiusList) {
		this.radiusList = radiusList;
	}
	public String getEndCustomer() {
		return endCustomer;
	}
	public void setEndCustomer(String endCustomer) {
		this.endCustomer = endCustomer;
	}
	public String getRadius() {
		return radius;
	}
	public void setRadius(String radius) {
		this.radius = radius;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public Collection getEndCustomeList() {
		return endCustomeList;
	}
	public void setEndCustomeList(Collection endCustomeList) {
		this.endCustomeList = endCustomeList;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getSearchMap() {
		return searchMap;
	}
	public void setSearchMap(String searchMap) {
		this.searchMap = searchMap;
	}
	public String getIsMsa() {
		return isMsa;
	}
	public void setIsMsa(String isMsa) {
		this.isMsa = isMsa;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public ArrayList getMsaList() {
		return msaList;
	}
	public void setMsaList(ArrayList msaList) {
		this.msaList = msaList;
	}


	

}
