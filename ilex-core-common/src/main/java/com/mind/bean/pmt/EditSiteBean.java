package com.mind.bean.pmt;

import java.io.Serializable;

public class EditSiteBean implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String msaid = null;
	private String siteid = null;
	private String unionSitecombo = null;
	private String mastersiteid = null;
	private String siteNumber = null;
	private String siteName = null;
	private String localityFactor = null;
	private String unionSite = null;
	private String designator = null;
	private String worklocation = null;
	private String address = null;
	private String statecombo = null;
	
	private String city = null;
	private String state = null;
	private String zipcode = null;
	private String country = null;
	private String directions = null;
	private String primaryContactPerson = null;
	private String secondaryContactPerson = null;
	
	private String primaryPhoneNumber = null;
	private String secondaryPhoneNumber = null;
	private String primaryEmailId = null;
	private String secondaryEmailId = null;
	private String specialCondition = null;
	private String msaName = null;
	
	
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getDesignator() {
		return designator;
	}
	public void setDesignator(String designator) {
		this.designator = designator;
	}
	public String getDirections() {
		return directions;
	}
	public void setDirections(String directions) {
		this.directions = directions;
	}
	public String getLocalityFactor() {
		return localityFactor;
	}
	public void setLocalityFactor(String localityFactor) {
		this.localityFactor = localityFactor;
	}
	
	public String getMsaid() {
		return msaid;
	}
	public void setMsaid(String msaid) {
		this.msaid = msaid;
	}
	public String getPrimaryContactPerson() {
		return primaryContactPerson;
	}
	public void setPrimaryContactPerson(String primaryContactPerson) {
		this.primaryContactPerson = primaryContactPerson;
	}
	public String getPrimaryEmailId() {
		return primaryEmailId;
	}
	public void setPrimaryEmailId(String primaryEmailId) {
		this.primaryEmailId = primaryEmailId;
	}
	public String getPrimaryPhoneNumber() {
		return primaryPhoneNumber;
	}
	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
		this.primaryPhoneNumber = primaryPhoneNumber;
	}
	public String getSecondaryContactPerson() {
		return secondaryContactPerson;
	}
	public void setSecondaryContactPerson(String secondaryContactPerson) {
		this.secondaryContactPerson = secondaryContactPerson;
	}
	public String getSecondaryEmailId() {
		return secondaryEmailId;
	}
	public void setSecondaryEmailId(String secondaryEmailId) {
		this.secondaryEmailId = secondaryEmailId;
	}
	public String getSecondaryPhoneNumber() {
		return secondaryPhoneNumber;
	}
	public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
		this.secondaryPhoneNumber = secondaryPhoneNumber;
	}
	public String getSiteid() {
		return siteid;
	}
	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}
	public String getSiteNumber() {
		return siteNumber;
	}
	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}
	public String getSpecialCondition() {
		return specialCondition;
	}
	public void setSpecialCondition(String specialCondition) {
		this.specialCondition = specialCondition;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUnionSite() {
		return unionSite;
	}
	public void setUnionSite(String unionSite) {
		this.unionSite = unionSite;
	}
	public String getWorklocation() {
		return worklocation;
	}
	public void setWorklocation(String worklocation) {
		this.worklocation = worklocation;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getMastersiteid() {
		return mastersiteid;
	}
	public void setMastersiteid(String mastersiteid) {
		this.mastersiteid = mastersiteid;
	}
	public String getStatecombo() {
		return statecombo;
	}
	public void setStatecombo(String statecombo) {
		this.statecombo = statecombo;
	}
	public String getUnionSitecombo() {
		return unionSitecombo;
	}
	public void setUnionSitecombo(String unionSitecombo) {
		this.unionSitecombo = unionSitecombo;
	}
	

}
