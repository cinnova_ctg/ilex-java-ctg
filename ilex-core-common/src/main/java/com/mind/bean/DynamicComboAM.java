package com.mind.bean;

import java.io.Serializable;
import java.util.Collection;

public class DynamicComboAM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection classlist = null;
	private Collection resourcelist = null;
	private Collection shortcatlist = null;

	public Collection getClasslist() {
		return classlist;
	}

	public void setClasslist(Collection classlist) {
		this.classlist = classlist;
	}

	public Collection getResourcelist() {
		return resourcelist;
	}

	public void setResourcelist(Collection resourcelist) {
		this.resourcelist = resourcelist;
	}

	public Collection getShortcatlist() {
		return shortcatlist;
	}

	public void setShortcatlist(Collection shortcatlist) {
		this.shortcatlist = shortcatlist;
	}

}
