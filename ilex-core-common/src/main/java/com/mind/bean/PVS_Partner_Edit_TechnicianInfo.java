package com.mind.bean;

public class PVS_Partner_Edit_TechnicianInfo implements java.io.Serializable {
	// seema

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tech_id = null;
	// over
	private String engName = null;
	private String cmboxResourceLevel = null;
	private String LevelName = null;
	private String engZip = null;
	private String[] chkboxEquipped = null;
	private String rdoUnion = null;
	private String cmboxHighCriticality = null;
	private String[] cmboxCertifications = null;
	private String certificationName = null;
	private String highCriticalityName = null;
	private String resourceLevelName = null;

	private String checkBox1 = null;
	private String checkBox2 = null;
	private String checkBox3 = null;
	private String durgScreenCertificationFileId = null;
	private String criminalBackgroundCertificationFileId = null;

	public String getCheckBox1() {
		return checkBox1;
	}

	public void setCheckBox1(String checkBox1) {
		this.checkBox1 = checkBox1;
	}

	public String getCheckBox2() {
		return checkBox2;
	}

	public void setCheckBox2(String checkBox2) {
		this.checkBox2 = checkBox2;


	}

	public String getCheckBox1Date() {
		return checkBox1Date;
	}

	public void setCheckBox1Date(String checkBox1Date) {
		this.checkBox1Date = checkBox1Date;
	}

	public String getCheckBox2Date() {
		return checkBox2Date;
	}

	public void setCheckBox2Date(String checkBox2Date) {
		this.checkBox2Date = checkBox2Date;
	}

	public String getCheckBox3Date() {
		return checkBox3Date;
	}

	public void setCheckBox3Date(String checkBox3Date) {
		this.checkBox3Date = checkBox3Date;
	}

	private String checkBox1Date = null;
	private String checkBox2Date = null;
	private String checkBox3Date = null;

	public PVS_Partner_Edit_TechnicianInfo() {

	}

	public PVS_Partner_Edit_TechnicianInfo(String id, String name,
			String level, String level_name, String zip, String[] equipped,
			String union, String criticality, String[] certifications) {

		tech_id = id;
		engName = name;
		cmboxResourceLevel = level;
		LevelName = level_name;
		engZip = zip;
		chkboxEquipped = equipped;
		rdoUnion = union;
		cmboxHighCriticality = criticality;
		cmboxCertifications = certifications;
		remove_Cert_Spaces(cmboxCertifications);
	}

	public PVS_Partner_Edit_TechnicianInfo(String id, String name,
			String level, String level_name, String zip, String[] equipped,
			String union, String criticality, String[] certifications,
			String certification_name, String high_criticality_name,
			String resource_level_name) {

		tech_id = id;
		engName = name;
		cmboxResourceLevel = level;
		LevelName = level_name;
		engZip = zip;
		chkboxEquipped = equipped;
		rdoUnion = union;
		cmboxHighCriticality = criticality;
		cmboxCertifications = certifications;
		certificationName = certification_name;
		highCriticalityName = high_criticality_name;
		resourceLevelName = resource_level_name;
		remove_Cert_Spaces(cmboxCertifications);
	}

	public PVS_Partner_Edit_TechnicianInfo(String id, String name,
			String level, String level_name, String zip, String[] equipped,
			String union, String criticality, String[] certifications,
			String certification_name, String high_criticality_name,
			String resource_level_name, String checkBox1, String checkBox2,
			String checkBox3, String checkBox1Date, String checkBox2Date,
			String checkBox3Date, String durgScreenCertificationFileId,
			String criminalBackgroundCertificationFileId) {

		tech_id = id;
		engName = name;
		cmboxResourceLevel = level;
		LevelName = level_name;
		engZip = zip;
		chkboxEquipped = equipped;
		rdoUnion = union;
		cmboxHighCriticality = criticality;
		cmboxCertifications = certifications;
		certificationName = certification_name;
		highCriticalityName = high_criticality_name;
		resourceLevelName = resource_level_name;
		this.checkBox1 = checkBox1;
		this.checkBox2 = checkBox2;
		this.setCheckBox3(checkBox3);
		this.checkBox1Date = checkBox1Date;
		this.checkBox2Date = checkBox2Date;
		this.checkBox3Date = checkBox3Date;
		this.durgScreenCertificationFileId = durgScreenCertificationFileId;
		this.criminalBackgroundCertificationFileId = criminalBackgroundCertificationFileId;
		remove_Cert_Spaces(cmboxCertifications);
	}

	private void remove_Cert_Spaces(String[] certifications) {
		for (int i = 0; i < certifications.length; i++) {
			certifications[i] = certifications[i].trim();
		}
	}

	public String[] getChkboxEquipped() {
		return chkboxEquipped;
	}

	public void setChkboxEquipped(String[] chkboxEquipped) {
		this.chkboxEquipped = chkboxEquipped;
	}

	public String getCmboxHighCriticality() {
		return cmboxHighCriticality;
	}

	public void setCmboxHighCriticality(String cmboxHighCriticality) {
		this.cmboxHighCriticality = cmboxHighCriticality;
	}

	public String getCmboxResourceLevel() {
		return cmboxResourceLevel;
	}

	public void setCmboxResourceLevel(String cmboxResourceLevel) {
		this.cmboxResourceLevel = cmboxResourceLevel;
	}

	public String getEngName() {
		return engName;
	}

	public void setEngName(String engName) {
		this.engName = engName;
	}

	public String getEngZip() {
		return engZip;
	}

	public void setEngZip(String engZip) {
		this.engZip = engZip;
	}

	public String getRdoUnion() {
		return rdoUnion;
	}

	public void setRdoUnion(String rdoUnion) {
		this.rdoUnion = rdoUnion;
	}

	public String[] getCmboxCertifications() {
		return cmboxCertifications;
	}

	public void setCmboxCertifications(String[] cmboxCertifications) {
		this.cmboxCertifications = cmboxCertifications;
	}

	public String getTech_id() {
		return tech_id;
	}

	public void setTech_id(String tech_id) {
		this.tech_id = tech_id;
	}

	public String getLevelName() {
		return LevelName;
	}

	public void setLevelName(String levelName) {
		LevelName = levelName;
	}

	public String getCertificationName() {
		return certificationName;
	}

	public void setCertificationName(String certificationName) {
		this.certificationName = certificationName;
	}

	public String getHighCriticalityName() {
		return highCriticalityName;
	}

	public void setHighCriticalityName(String highCriticalityName) {
		this.highCriticalityName = highCriticalityName;
	}

	public String getResourceLevelName() {
		return resourceLevelName;
	}

	public void setResourceLevelName(String resourceLevelName) {
		this.resourceLevelName = resourceLevelName;
	}

	public String getCheckBox3() {
		return checkBox3;
	}
	public void setCheckBox3(String checkBox3) {
		this.checkBox3 = checkBox3;
	}
	public String getDurgScreenCertificationFileId() {
		return durgScreenCertificationFileId;
	}
	public void setDurgScreenCertificationFileId(
			String durgScreenCertificationFileId) {
		this.durgScreenCertificationFileId = durgScreenCertificationFileId;
	}
	public String getCriminalBackgroundCertificationFileId() {
		return criminalBackgroundCertificationFileId;
	}
	public void setCriminalBackgroundCertificationFileId(
			String criminalBackgroundCertificationFileId) {
		this.criminalBackgroundCertificationFileId = criminalBackgroundCertificationFileId;
	}
}
