/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This is a bean class used for getting and setting the values of collections used in Role Privilege Form.      
 *
 */

package com.mind.bean;

import java.io.Serializable;
import java.util.Collection;

public class DynamicComboUM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection functiongroup = null;
	private Collection rolename = null;

	public Collection getFunctiongroup() {
		return functiongroup;
	}

	public void setFunctiongroup(Collection functiongroup) {
		this.functiongroup = functiongroup;
	}

	public Collection getRolename() {
		return rolename;
	}

	public void setRolename(Collection rolename) {
		this.rolename = rolename;
	}

}
