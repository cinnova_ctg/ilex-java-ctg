package com.mind.bean;

import java.io.Serializable;

public class PVSToolList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String chkboxTools = null;
	String toolZips = null;
	String label = null;
	String parent_id = null;
	int width = 0;

	public String getParent_id() {
		return parent_id;
	}

	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}

	public PVSToolList(String tools, String label, String zips,
			String parentid, int width) {
		chkboxTools = tools;
		toolZips = zips;
		this.label = label;
		parent_id = parentid;
		this.width = width;
	}

	// customize constructor
	public PVSToolList(String tools, String label, String zips) {
		chkboxTools = tools;
		toolZips = zips;
		this.label = label;

	}

	public PVSToolList() {

	}

	public String getchkboxTools() {
		return chkboxTools;
	}

	public void setchkboxTools(String chkboxTools) {
		this.chkboxTools = chkboxTools;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String gettoolZips() {
		return toolZips;
	}

	public void settoolZips(String toolZips) {
		this.toolZips = toolZips;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

}
