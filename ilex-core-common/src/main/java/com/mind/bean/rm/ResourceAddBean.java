package com.mind.bean.rm;

import java.io.Serializable;


public class ResourceAddBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String minimumqty = null;
	private String status = null;
	private String reset = null;
	private String sellableqty = null;
	private String laborname = null;
	private String save = null;
	private String unit = null;
	private String basecost = null;
	private String laboridentifier = null;
	private String flag = null;
	private String resourceflag=null;
	private String id=null;
	private String id1=null;
	private String id2=null;
	private String id3=null;
	private String firstcatg=null;
	private String secondcatg=null;
	private String thirdcatg=null;
	private String mfgpartno=null;
	private String weight=null;
	private String addmessage=null;
	private String updatemessage=null;
	private String message=null;
	private String function=null;
	private String updatecombovalue=null;
	private String cnspartno=null;
	private String authenticate="";
	private String criticality=null;
	private String criticalityname=null;
	private String criticalityid=null;
	private String createdby=null;
	private String createdate=null;
	private String changedby=null;
	private String changedate=null;
	
	
	
	
	public String getUpdatecombovalue() {
		return updatecombovalue;
	}

	public void setUpdatecombovalue(String updatecombovalue) {
		this.updatecombovalue = updatecombovalue;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}
	public String getAddmessage() {
		return addmessage;
	}

	public void setAddmessage(String addmessage) {
		this.addmessage = addmessage;
	}

	public String getUpdatemessage() {
		return updatemessage;
	}

	public void setUpdatemessage(String updatemessage) {
		this.updatemessage = updatemessage;
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public String getMfgpartno() {
		return mfgpartno;
	}

	public void setMfgpartno(String mfgpartno) {
		this.mfgpartno = mfgpartno;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getFirstcatg() {
		return firstcatg;
	}

	public void setFirstcatg(String firstcatg) {
		this.firstcatg = firstcatg;
	}

	public String getSecondcatg() {
		return secondcatg;
	}

	public void setSecondcatg(String secondcatg) {
		this.secondcatg = secondcatg;
	}

	public String getThirdcatg() {
		return thirdcatg;
	}

	public void setThirdcatg(String thirdcatg) {
		this.thirdcatg = thirdcatg;
	}
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId1() {
		return id1;
	}

	public void setId1(String id1) {
		this.id1 = id1;
	}

	public String getId2() {
		return id2;
	}

	public void setId2(String id2) {
		this.id2 = id2;
	}

	public String getId3() {
		return id3;
	}

	public void setId3(String id3) {
		this.id3 = id3;
	}

	public String getResourceflag() {
		return resourceflag;
	}

	public void setResourceflag(String resourceflag) {
		this.resourceflag = resourceflag;
	}

	public String getMinimumqty() {
		return minimumqty;
	}

	/**
	 * Set minimumqty
	 * @param <code>String</code>
	 */
	public void setMinimumqty(String m) {
		this.minimumqty = m;
	}

	
	public String getStatus() {
		
		return status;
	}

	
	public void setStatus(String s) {
		this.status = s;
	}

	
	public String getReset() {
		return reset;
	}

	
	public void setReset(String r) {
		this.reset = r;
	}

	
	public String getSellableqty() {
		return sellableqty;
	}

	
	public void setSellableqty(String s) {
		this.sellableqty = s;
	}

	
	public String getLaborname() {
		return laborname;
	}

	
	public void setLaborname(String l) {
		this.laborname = l;
	}

	
	public String getSave() {
		return save;
	}

	
	public void setSave(String s) {
		this.save = s;
	}

	
	public String getUnit() {
		return unit;
	}

	
	public void setUnit(String u) {
		this.unit = u;
	}

	
	public String getBasecost() {
		return basecost;
	}

	
	public void setBasecost(String b) {
		this.basecost = b;
	}

	
	public String getLaboridentifier() {
		return laboridentifier;
	}

	public void setLaboridentifier(String l) {
		this.laboridentifier = l;
	}
	
	public String getCnspartno() {
		return cnspartno;
	}

	public void setCnspartno(String cnspartno) {
		this.cnspartno = cnspartno;
	}
	
	public String getAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}

	
	public String getCriticality() {
		return criticality;
	}

	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}

	public String getCriticalityid() {
		return criticalityid;
	}

	public void setCriticalityid(String criticalityid) {
		this.criticalityid = criticalityid;
	}

	public String getCriticalityname() {
		return criticalityname;
	}

	public void setCriticalityname(String criticalityname) {
		this.criticalityname = criticalityname;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public String getCreatedate() {
		return createdate;
	}
	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}
	public void setChangedby(String changedby)
	{
		this.changedby= changedby;
	}
	public String getChangedby()
	{
		return changedby;
	}
	
	
	
	public String getChangedate() {
		return changedate;
	}
	public void setChangedate(String changedate) {
		this.changedate = changedate;
	}

	

	

	

}
