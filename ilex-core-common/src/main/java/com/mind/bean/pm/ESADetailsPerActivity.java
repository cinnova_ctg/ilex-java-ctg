package com.mind.bean.pm;

public class ESADetailsPerActivity {
	private String idESA = null;
	private String agentName = null; 
	private String esaCommissionType = null;
	private String esaActCommissionRate = null;
	//private String esaActCost = null;
	//private String esaActRevenue = null;
	
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getEsaActCommissionRate() {
		return esaActCommissionRate;
	}
	public void setEsaActCommissionRate(String esaActCommissionRate) {
		this.esaActCommissionRate = esaActCommissionRate;
	}
/*	public String getEsaActCost() {
		return esaActCost;
	}
	public void setEsaActCost(String esaActCost) {
		this.esaActCost = esaActCost;
	}
	public String getEsaActRevenue() {
		return esaActRevenue;
	}
	public void setEsaActRevenue(String esaActRevenue) {
		this.esaActRevenue = esaActRevenue;
	}
	*/
	public String getEsaCommissionType() {
		return esaCommissionType;
	}
	public void setEsaCommissionType(String esaCommissionType) {
		this.esaCommissionType = esaCommissionType;
	}
	public String getIdESA() {
		return idESA;
	}
	public void setIdESA(String idESA) {
		this.idESA = idESA;
	}
}
