package com.mind.bean.pm;

import java.io.Serializable;


public class ESAList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String esaId = null;
	private String esaSeqNo = null;
	private String agent = null;
	private String commission = null;
	private String payementTerms = null;
	private String effectiveDate = null;
	private String status = null;
	private String commissionType = null;

	public String getCommissionType() {
		return commissionType;
	}
	public void setCommissionType(String commissionType) {
		this.commissionType = commissionType;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getEsaId() {
		return esaId;
	}
	public void setEsaId(String esaId) {
		this.esaId = esaId;
	}
	public String getEsaSeqNo() {
		return esaSeqNo;
	}
	public void setEsaSeqNo(String esaSeqNo) {
		this.esaSeqNo = esaSeqNo;
	}
	public String getPayementTerms() {
		return payementTerms;
	}
	public void setPayementTerms(String payementTerms) {
		this.payementTerms = payementTerms;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
