package com.mind.bean.pm;

import java.io.Serializable;

public class FreightEditBean implements Serializable {
	
		
		private String activity_Id = null;
		private String freightid = null;
		private String freightcostlibid = null;
		private String freightname = null;
		private String freighttype = null;
		private String cnspartnumber = null;
		private String quantity = null;
		private String prevquantity = null;
		private String estimatedunitcost = null;
		private String estimatedtotalcost = null;
		private String proformamargin = null;
		private String priceunit = null;
		private String priceextended = null;
		private String sellablequantity = null;
		private String minimumquantity = null;
		private String status = null;
		private String addendum_id = null;
		private String flag = null; // for resource from temporary table or not
		private String flag_freightid = null; //for setting checkbox checked
		private String activity_name = null;
		private String msa_Id = null;
		private String msaname = null;
		private String chkaddendum = null;
		private String save = null;
		private String appendixId = null;
		private String AppendixName = null;
		private String jobName = null;
		private String jobId = null;
		private String msaName = null;
		private String msaId = null;
		private String activityName = null;
		private String fromflag = null;
		private String dashboardid = null;
		private String Ref = null;
		private String createdBy = null;
		private String createdDate = null;
		private String changedBy = null;
		private String changedDate = null;
		
		
		public String getChangedBy() {
			return changedBy;
		}
		public void setChangedBy(String changedBy) {
			this.changedBy = changedBy;
		}
		public String getChangedDate() {
			return changedDate;
		}
		public void setChangedDate(String changedDate) {
			this.changedDate = changedDate;
		}
		public String getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}
		public String getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
		}
		public String getDashboardid() {
			return dashboardid;
		}
		public void setDashboardid(String dashboardid) {
			this.dashboardid = dashboardid;
		}
		public String getFromflag() {
			return fromflag;
		}
		public void setFromflag(String fromflag) {
			this.fromflag = fromflag;
		}
		public String getActivityName() {
			return activityName;
		}
		public void setActivityName(String activityName) {
			this.activityName = activityName;
		}
		public String getAppendixId() {
			return appendixId;
		}
		public void setAppendixId(String appendixId) {
			this.appendixId = appendixId;
		}
		public String getAppendixName() {
			return AppendixName;
		}
		public void setAppendixName(String appendixName) {
			AppendixName = appendixName;
		}
		public String getJobId() {
			return jobId;
		}
		public void setJobId(String jobId) {
			this.jobId = jobId;
		}
		public String getJobName() {
			return jobName;
		}
		public void setJobName(String jobName) {
			this.jobName = jobName;
		}
		public String getMsaId() {
			return msaId;
		}
		public void setMsaId(String msaId) {
			this.msaId = msaId;
		}
		public String getMsaName() {
			return msaName;
		}
		public void setMsaName(String msaName) {
			this.msaName = msaName;
		}
		public String getActivity_Id() {
			return activity_Id;
		}
		public void setActivity_Id(String activity_Id) {
			this.activity_Id = activity_Id;
		}
		public String getActivity_name() {
			return activity_name;
		}
		public void setActivity_name(String activity_name) {
			this.activity_name = activity_name;
		}
		public String getAddendum_id() {
			return addendum_id;
		}
		public void setAddendum_id(String addendum_id) {
			this.addendum_id = addendum_id;
		}
		public String getCnspartnumber() {
			return cnspartnumber;
		}
		public void setCnspartnumber(String cnspartnumber) {
			this.cnspartnumber = cnspartnumber;
		}
		public String getEstimatedtotalcost() {
			return estimatedtotalcost;
		}
		public void setEstimatedtotalcost(String estimatedtotalcost) {
			this.estimatedtotalcost = estimatedtotalcost;
		}
		public String getEstimatedunitcost() {
			return estimatedunitcost;
		}
		public void setEstimatedunitcost(String estimatedunitcost) {
			this.estimatedunitcost = estimatedunitcost;
		}
		public String getFlag() {
			return flag;
		}
		public void setFlag(String flag) {
			this.flag = flag;
		}
		public String getFlag_freightid() {
			return flag_freightid;
		}
		public void setFlag_freightid(String flag_freightid) {
			this.flag_freightid = flag_freightid;
		}
		public String getFreightcostlibid() {
			return freightcostlibid;
		}
		public void setFreightcostlibid(String freightcostlibid) {
			this.freightcostlibid = freightcostlibid;
		}
		public String getFreightid() {
			return freightid;
		}
		public void setFreightid(String freightid) {
			this.freightid = freightid;
		}
		public String getFreightname() {
			return freightname;
		}
		public void setFreightname(String freightname) {
			this.freightname = freightname;
		}
		public String getFreighttype() {
			return freighttype;
		}
		public void setFreighttype(String freighttype) {
			this.freighttype = freighttype;
		}
		public String getMinimumquantity() {
			return minimumquantity;
		}
		public void setMinimumquantity(String minimumquantity) {
			this.minimumquantity = minimumquantity;
		}
		public String getMsa_Id() {
			return msa_Id;
		}
		public void setMsa_Id(String msa_Id) {
			this.msa_Id = msa_Id;
		}
		public String getMsaname() {
			return msaname;
		}
		public void setMsaname(String msaname) {
			this.msaname = msaname;
		}
		public String getPrevquantity() {
			return prevquantity;
		}
		public void setPrevquantity(String prevquantity) {
			this.prevquantity = prevquantity;
		}
		public String getPriceextended() {
			return priceextended;
		}
		public void setPriceextended(String priceextended) {
			this.priceextended = priceextended;
		}
		public String getPriceunit() {
			return priceunit;
		}
		public void setPriceunit(String priceunit) {
			this.priceunit = priceunit;
		}
		public String getProformamargin() {
			return proformamargin;
		}
		public void setProformamargin(String proformamargin) {
			this.proformamargin = proformamargin;
		}
		public String getQuantity() {
			return quantity;
		}
		public void setQuantity(String quantity) {
			this.quantity = quantity;
		}
		public String getSellablequantity() {
			return sellablequantity;
		}
		public void setSellablequantity(String sellablequantity) {
			this.sellablequantity = sellablequantity;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getChkaddendum() {
			return chkaddendum;
		}
		public void setChkaddendum(String chkaddendum) {
			this.chkaddendum = chkaddendum;
		}
		public String getSave() {
			return save;
		}
		public void setSave(String save) {
			this.save = save;
		}
		public String getRef() {
			return Ref;
		}
		public void setRef(String ref) {
			Ref = ref;
		}
		
		

	}



