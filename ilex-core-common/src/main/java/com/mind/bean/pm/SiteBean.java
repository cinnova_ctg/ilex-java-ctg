package com.mind.bean.pm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.mind.bean.docm.LabelValue;

public class SiteBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



		private String id1 = null;
		private String id2 = null;
		private String site_Id = null;
		private String site_number = null;
		private String site_name = null;
		private String site_designator = null;
		private String site_worklocation = null;
		private String site_address = null;
		private String site_city = null;
		private String state = null;
		private String country = null;
		private String stateSelect=null;
		private String site_zipcode = null;
		private String site_notes = null;
		private String site_direction = null;
		private String site_phone = null;
		private String locality_uplift = null;
		private String union_site = null;
		private String primary_email = null;
		private String secondary_email = null;
		private String secondary_phone = null;
		private String primary_Name = null;
		private String secondary_Name = null;
		private ArrayList poc = null;
		private ArrayList unionlist = null;
		private String appendixname=null;
		private String jobName = null;
		
		private String submitPage = null;
		private String ref = null;
		private String ref1 = null;
		
		private String nettype = null;
		private String viewjobtype = null;
		private String ownerId = null;
		private String addendum_id = null;
		private String back = null;
		private String authenticate = "";
		private String sitesearch = null;
		
		//02/28/2007 
		private String sitelatdeg = null;
		private String sitelatmin = null;
		private String sitelatdirection = null;
		private String sitelondeg = null;
		private String sitelonmin = null;
		private String sitelondirection = null;
		
		private String sitestatus = null;
		private String sitecategory  = null;
		private String siteregion = null;
		private String sitegroup = null;
		private String siteendcustomer = null;
		private String sitetimezone = null;
		private Collection tempList = null;  
		private String siteCountryId = null;
		private String siteCountryIdName = null;
		private String msaid = null;
		private String msaname = null;
		private String siteLatLon  = null;
		private String refersh = null;
		
		private String sitelistid = null;
		private String siteid = null;
		private String fromType = null; 
		
		//Field for carry date field of daily Report
		private String date=null;
		

		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public String getSitelistid() {
			return sitelistid;
		}
		public void setSitelistid(String sitelistid) {
			this.sitelistid = sitelistid;
		}
		public String getRefersh() {
			return refersh;
		}
		public void setRefersh(String refersh) {
			this.refersh = refersh;
		}
		public String getMsaid() {
			return msaid;
		}
		public void setMsaid(String msaid) {
			this.msaid = msaid;
		}
		public String getMsaname() {
			return msaname;
		}
		public void setMsaname(String msaname) {
			this.msaname = msaname;
		}
		public String getSiteCountryIdName() {
			return siteCountryIdName;
		}
		public void setSiteCountryIdName(String siteCountryIdName) {
			this.siteCountryIdName = siteCountryIdName;
		}
		public String getSiteCountryId() {
			return siteCountryId;
		}
		public void setSiteCountryId(String siteCountryId) {
			this.siteCountryId = siteCountryId;
		}
		public Collection getTempList() {
			return tempList;
		}
		public void setTempList(Collection tempList) {
			this.tempList = tempList;
		}
		public String getAuthenticate() {
			return authenticate;
		}
		public void setAuthenticate(String authenticate) {
			this.authenticate = authenticate;
		}
		public String getId1() {
			return id1;
		}
		public void setId1( String id1 ) {
			this.id1 = id1;
		}
		
		
		public String getId2() {
			return id2;
		}
		public void setId2(String id2 ) {
			this.id2 = id2;
		}
		
		
		public String getSite_Id() {
			return site_Id;
		}
		public void setSite_Id( String site_Id ) {
			this.site_Id = site_Id;
		}
		
		
		public String getSite_address() {
			return site_address;
		}
		public void setSite_address( String site_address ) {
			this.site_address = site_address;
		}
		
		
		public String getSite_city() {
			return site_city;
		}
		public void setSite_city( String site_city ) {
			this.site_city = site_city;
		}
		
		public String getSite_designator() {
			return site_designator;
		}
		public void setSite_designator( String site_designator ) {
			this.site_designator = site_designator;
		}
		
		
		public String getSite_direction() {
			return site_direction;
		}
		public void setSite_direction( String site_direction ) {
			this.site_direction = site_direction;
		}
		
		
		
		
		
		public String getSite_name() {
			return site_name;
		}
		public void setSite_name( String site_name ) {
			this.site_name = site_name;
		}
		
		
		public String getSite_notes() {
			return site_notes;
		}
		public void setSite_notes( String site_notes ) {
			this.site_notes = site_notes;
		}
		
		
		public String getSite_number() {
			return site_number;
		}
		public void setSite_number( String site_number ) {
			this.site_number = site_number;
		}
		
		
		public String getSite_worklocation() {
			return site_worklocation;
		}
		public void setSite_worklocation( String site_worklocation ) {
			this.site_worklocation = site_worklocation;
		}
		
		
		public String getSite_zipcode() {
			return site_zipcode;
		}
		public void setSite_zipcode( String site_zipcode ) {
			this.site_zipcode = site_zipcode;
		}
		

		public String getSite_phone() {
			return site_phone;
		}
		public void setSite_phone(String site_phone) {
			this.site_phone = site_phone;
		}
		
		
		public String getRef() {
			return ref;
		}
		public void setRef( String ref ) {
			this.ref = ref;
		}
		

		public String getLocality_uplift() {
			return locality_uplift;
		}
		public void setLocality_uplift(String locality_uplift) {
			this.locality_uplift = locality_uplift;
		}
		
		
		public ArrayList getPoc() {
			return poc;
		}
		public void setPoc( ArrayList poc ) {
			this.poc = poc;
		}
		
		
		public ArrayList getUnionlist() 
		{
			unionlist = new java.util.ArrayList();
			unionlist.add( new LabelValue("-Select-" , "0"));
			unionlist.add( new LabelValue( "YES" , "Y") );
			unionlist.add( new LabelValue( "NO" , "N" ) );
			
			return unionlist;
		}
		
		public void setUnionlist( ArrayList list ) 
		{
			unionlist = list;
		}
		

		public String getUnion_site() {
			return union_site;
		}
		public void setUnion_site( String union_site ) {
			this.union_site = union_site;
		}
		
		

		public String getRef1() {
			return ref1;
		}
		public void setRef1( String ref1 ) {
			this.ref1 = ref1;
		}
		
		
		
		public String getAddendum_id() {
			return addendum_id;
		}
		public void setAddendum_id( String addendum_id ) {
			this.addendum_id = addendum_id;
		}
		
		
		
		public String getBack() {
			return back;
		}
		public void setBack(String back) {
			this.back = back;
		}
		public String getPrimary_email() {
			return primary_email;
		}
		public void setPrimary_email(String primary_email) {
			this.primary_email = primary_email;
		}
		public String getSecondary_email() {
			return secondary_email;
		}
		public void setSecondary_email(String secondary_email) {
			this.secondary_email = secondary_email;
		}
		public String getSecondary_phone() {
			return secondary_phone;
		}
		public void setSecondary_phone(String secondary_phone) {
			this.secondary_phone = secondary_phone;
		}
		public String getPrimary_Name() {
			return primary_Name;
		}
		public void setPrimary_Name(String primary_Name) {
			this.primary_Name = primary_Name;
		}
		public String getSecondary_Name() {
			return secondary_Name;
		}
		public void setSecondary_Name(String secondary_Name) {
			this.secondary_Name = secondary_Name;
		}
		public String getSitesearch() {
			return sitesearch;
		}
		public void setSitesearch(String sitesearch) {
			this.sitesearch = sitesearch;
		}
		public String getNettype() {
			return nettype;
		}
		public void setNettype(String nettype) {
			this.nettype = nettype;
		}
		public String getViewjobtype() {
			return viewjobtype;
		}
		public void setViewjobtype(String viewjobtype) {
			this.viewjobtype = viewjobtype;
		}
		public String getAppendixname() {
			return appendixname;
		}
		public void setAppendixname(String appendixname) {
			this.appendixname = appendixname;
		}
		public String getJobName() {
			return jobName;
		}
		public void setJobName(String jobName) {
			this.jobName = jobName;
		}
		public String getOwnerId() {
			return ownerId;
		}
		public void setOwnerId(String ownerId) {
			this.ownerId = ownerId;
		}
		public String getSitelatdeg() {
			return sitelatdeg;
		}
		public void setSitelatdeg(String sitelatdeg) {
			this.sitelatdeg = sitelatdeg;
		}
		public String getSitelatdirection() {
			return sitelatdirection;
		}
		public void setSitelatdirection(String sitelatdirection) {
			this.sitelatdirection = sitelatdirection;
		}
		public String getSitelatmin() {
			return sitelatmin;
		}
		public void setSitelatmin(String sitelatmin) {
			this.sitelatmin = sitelatmin;
		}
		public String getSitelondeg() {
			return sitelondeg;
		}
		public void setSitelondeg(String sitelondeg) {
			this.sitelondeg = sitelondeg;
		}
		public String getSitelondirection() {
			return sitelondirection;
		}
		public void setSitelondirection(String sitelondirection) {
			this.sitelondirection = sitelondirection;
		}
		public String getSitelonmin() {
			return sitelonmin;
		}
		public void setSitelonmin(String sitelonmin) {
			this.sitelonmin = sitelonmin;
		}
		public String getSitecategory() {
			return sitecategory;
		}
		public void setSitecategory(String sitecategory) {
			this.sitecategory = sitecategory;
		}
		
		public String getSitegroup() {
			return sitegroup;
		}
		public void setSitegroup(String sitegroup) {
			this.sitegroup = sitegroup;
		}
		public String getSiteregion() {
			return siteregion;
		}
		public void setSiteregion(String siteregion) {
			this.siteregion = siteregion;
		}
		public String getSitestatus() {
			return sitestatus;
		}
		public void setSitestatus(String sitestatus) {
			this.sitestatus = sitestatus;
		}
		
		public String getSiteLatLon() {
			return siteLatLon;
		}
		public void setSiteLatLon(String siteLatLon) {
			this.siteLatLon = siteLatLon;
		}
		public String getSitetimezone() {
			return sitetimezone;
		}
		public void setSitetimezone(String sitetimezone) {
			this.sitetimezone = sitetimezone;
		}
		public String getSiteendcustomer() {
			return siteendcustomer;
		}
		public void setSiteendcustomer(String siteendcustomer) {
			this.siteendcustomer = siteendcustomer;
		}
		public String getSubmitPage() {
			return submitPage;
		}
		public void setSubmitPage(String submitPage) {
			this.submitPage = submitPage;
		}
		public String getSiteid() {
			return siteid;
		}
		public void setSiteid(String siteid) {
			this.siteid = siteid;
		}
		public String getFromType() {
			return fromType;
		}
		public void setFromType(String fromType) {
			this.fromType = fromType;
		}
		public String getCountry() {
			return country;
		}
		public void setCountry(String country) {
			this.country = country;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public String getStateSelect() {
			return stateSelect;
		}
		public void setStateSelect(String stateSelect) {
			this.stateSelect = stateSelect;
		}
	}



