package com.mind.bean.pm;

import java.io.Serializable;

public class ESAActivityList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String actId = null;
	private String activityName = null;
	private String actQty = null;
	private String resBasis = null;
	private String resTotRevenue = null;
	private String actType = null; 
	
	private String esaId1 = null;
	private String esaType1 = null;
	private String commissionStatus1 = null;
	private String commissionRate1 = null;
	private String esaCost1 = null;
	private String esaRevenue1 = null;
	private String esaActActiveStatus1 = null;
	private String esaTypeLabel1 = null;
	
	private String esaId2 = null;
	private String esaType2 = null;
	private String commissionStatus2 = null;
	private String commissionRate2 = null;
	private String esaCost2 = null;
	private String esaRevenue2 = null;
	private String esaActActiveStatus2 = null;
	private String esaTypeLabel2 = null;
	
	private String esaId3 = null;
	private String esaType3 = null;
	private String commissionStatus3 = null;
	private String commissionRate3 = null;
	private String esaCost3 = null;
	private String esaRevenue3 = null;
	private String esaActActiveStatus3 = null;
	private String esaTypeLabel3 = null;
	public String getActId() {
		return actId;
	}
	public void setActId(String actId) {
		this.actId = actId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getActQty() {
		return actQty;
	}
	public void setActQty(String actQty) {
		this.actQty = actQty;
	}
	public String getActType() {
		return actType;
	}
	public void setActType(String actType) {
		this.actType = actType;
	}
	public String getCommissionRate1() {
		return commissionRate1;
	}
	public void setCommissionRate1(String commissionRate1) {
		this.commissionRate1 = commissionRate1;
	}
	public String getCommissionRate2() {
		return commissionRate2;
	}
	public void setCommissionRate2(String commissionRate2) {
		this.commissionRate2 = commissionRate2;
	}
	public String getCommissionRate3() {
		return commissionRate3;
	}
	public void setCommissionRate3(String commissionRate3) {
		this.commissionRate3 = commissionRate3;
	}
	public String getCommissionStatus1() {
		return commissionStatus1;
	}
	public void setCommissionStatus1(String commissionStatus1) {
		this.commissionStatus1 = commissionStatus1;
	}
	public String getCommissionStatus2() {
		return commissionStatus2;
	}
	public void setCommissionStatus2(String commissionStatus2) {
		this.commissionStatus2 = commissionStatus2;
	}
	public String getCommissionStatus3() {
		return commissionStatus3;
	}
	public void setCommissionStatus3(String commissionStatus3) {
		this.commissionStatus3 = commissionStatus3;
	}
	public String getEsaCost1() {
		return esaCost1;
	}
	public void setEsaCost1(String esaCost1) {
		this.esaCost1 = esaCost1;
	}
	public String getEsaCost2() {
		return esaCost2;
	}
	public void setEsaCost2(String esaCost2) {
		this.esaCost2 = esaCost2;
	}
	public String getEsaCost3() {
		return esaCost3;
	}
	public void setEsaCost3(String esaCost3) {
		this.esaCost3 = esaCost3;
	}
	public String getEsaId1() {
		return esaId1;
	}
	public void setEsaId1(String esaId1) {
		this.esaId1 = esaId1;
	}
	public String getEsaId2() {
		return esaId2;
	}
	public void setEsaId2(String esaId2) {
		this.esaId2 = esaId2;
	}
	public String getEsaId3() {
		return esaId3;
	}
	public void setEsaId3(String esaId3) {
		this.esaId3 = esaId3;
	}
	public String getEsaRevenue1() {
		return esaRevenue1;
	}
	public void setEsaRevenue1(String esaRevenue1) {
		this.esaRevenue1 = esaRevenue1;
	}
	public String getEsaRevenue2() {
		return esaRevenue2;
	}
	public void setEsaRevenue2(String esaRevenue2) {
		this.esaRevenue2 = esaRevenue2;
	}
	public String getEsaRevenue3() {
		return esaRevenue3;
	}
	public void setEsaRevenue3(String esaRevenue3) {
		this.esaRevenue3 = esaRevenue3;
	}
	public String getEsaType1() {
		return esaType1;
	}
	public void setEsaType1(String esaType1) {
		this.esaType1 = esaType1;
	}
	public String getEsaType2() {
		return esaType2;
	}
	public void setEsaType2(String esaType2) {
		this.esaType2 = esaType2;
	}
	public String getEsaType3() {
		return esaType3;
	}
	public void setEsaType3(String esaType3) {
		this.esaType3 = esaType3;
	}
	public String getResBasis() {
		return resBasis;
	}
	public void setResBasis(String resBasis) {
		this.resBasis = resBasis;
	}
	public String getResTotRevenue() {
		return resTotRevenue;
	}
	public void setResTotRevenue(String resTotRevenue) {
		this.resTotRevenue = resTotRevenue;
	}
	public String getEsaActActiveStatus1() {
		return esaActActiveStatus1;
	}
	public void setEsaActActiveStatus1(String esaActActiveStatus1) {
		this.esaActActiveStatus1 = esaActActiveStatus1;
	}
	public String getEsaActActiveStatus2() {
		return esaActActiveStatus2;
	}
	public void setEsaActActiveStatus2(String esaActActiveStatus2) {
		this.esaActActiveStatus2 = esaActActiveStatus2;
	}
	public String getEsaActActiveStatus3() {
		return esaActActiveStatus3;
	}
	public void setEsaActActiveStatus3(String esaActActiveStatus3) {
		this.esaActActiveStatus3 = esaActActiveStatus3;
	}
	public String getEsaTypeLabel1() {
		return esaTypeLabel1;
	}
	public void setEsaTypeLabel1(String esaTypeLabel1) {
		this.esaTypeLabel1 = esaTypeLabel1;
	}
	public String getEsaTypeLabel2() {
		return esaTypeLabel2;
	}
	public void setEsaTypeLabel2(String esaTypeLabel2) {
		this.esaTypeLabel2 = esaTypeLabel2;
	}
	public String getEsaTypeLabel3() {
		return esaTypeLabel3;
	}
	public void setEsaTypeLabel3(String esaTypeLabel3) {
		this.esaTypeLabel3 = esaTypeLabel3;
	}
	
}
