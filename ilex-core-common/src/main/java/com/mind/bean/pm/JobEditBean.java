package com.mind.bean.pm;

import java.io.Serializable;
import java.util.ArrayList;

import com.mind.common.LabelValue;

public class JobEditBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String appendixId = null;
    private String jobId = null;
    private String ownerId = null;
    private String viewjobtype = null;

    private String jobName = null;

    private String requestorIds = null;
    private String additionalRecipientIds = null;

    private String requestorEmail = null;
    private String requestor = null;
    private ArrayList<LabelValue> requestorList = null;
    private String requestType;

    private String ref = null;
    private String custRef = null;
    private String jobType = null;
    private String union = null;
    private String localityUplift = null;
    private String unionUpliftFactor = null;
    private String siteId = null;
    private String siteName = null;
    private String siteNumber = null;
    private String siteAddress = null;
    private String siteCity = null;
    private String siteState = null;
    private String siteZipcode = null;
    private String sitePoc = null;
    private String sitePhone = null;

    private String estimatedCnsFieldLaborCost = null;
    private String estimatedContractFieldLaborCost = null;
    private String estimatedFreightCost = null;
    private String estimatedTravelCost = null;
    private String estimatedMaterialCost = null;
    private String estimatedTotalCost = null;

    private String extendedPrice = null;
    private String proformaMargin = null;
    private String estimatedStartSchedule = null;
    private String estimatedCompleteSchedule = null;

    private String lastComment = null;
    private String lastChangeBy = null;
    private String lastChangeDate = null;
    private String status = null;

    private String lastJobModifiedBy = null;
    private String lastJobModifiedDate = null;
    private String cnsPoc = null;
    private String installationPoc = null;

    private String addendumId = null;

    private String msaId = null;
    private String msaName = null;
    private String appendixName = null;
    private String actionAddUpdate = null;

    private String save = null;
    private String cancel = null;
    private String back = null;

    // For View Selector
    private String jobOwnerOtherCheck = null;
    private String[] jobSelectedStatus = null;
    private String[] jobSelectedOwners = null;
    private String jobMonthWeekCheck = null;

    private String startdatehh = null;
    private String startdatemm = null;
    private String startoptionname = null;
    private String completedatehh = null;
    private String completedatemm = null;
    private String completeoptionname = null;

    public String getJobMonthWeekCheck() {
        return jobMonthWeekCheck;
    }

    public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
        this.jobMonthWeekCheck = jobMonthWeekCheck;
    }

    public String[] getJobSelectedOwners() {
        return jobSelectedOwners;
    }

    public void setJobSelectedOwners(String[] jobSelectedOwners) {
        this.jobSelectedOwners = jobSelectedOwners;
    }

    public String[] getJobSelectedStatus() {
        return jobSelectedStatus;
    }

    public String getJobSelectedStatus(int i) {
        return jobSelectedStatus[i];
    }

    public String getJobSelectedOwners(int i) {
        return jobSelectedOwners[i];
    }

    public void setJobSelectedStatus(String[] jobSelectedStatus) {
        this.jobSelectedStatus = jobSelectedStatus;
    }

    public String getJobOwnerOtherCheck() {
        return jobOwnerOtherCheck;
    }

    public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
        this.jobOwnerOtherCheck = jobOwnerOtherCheck;
    }

    public String getActionAddUpdate() {
        return actionAddUpdate;
    }

    public void setActionAddUpdate(String action) {
        this.actionAddUpdate = action;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String job_Id) {
        this.jobId = job_Id;
    }

    public String getAddendumId() {
        return addendumId;
    }

    public void setAddendumId(String addendum_id) {
        this.addendumId = addendum_id;
    }

    public String getCnsPoc() {
        return cnsPoc;
    }

    public void setCnsPoc(String cns_poc) {
        cnsPoc = cns_poc;
    }

    public String getCustRef() {
        return custRef;
    }

    public void setCustRef(String custref) {
        this.custRef = custref;
    }

    public String getEstimatedCnsFieldLaborCost() {
        return estimatedCnsFieldLaborCost;
    }

    public void setEstimatedCnsFieldLaborCost(String estimated_cnsfieldlaborcost) {
        this.estimatedCnsFieldLaborCost = estimated_cnsfieldlaborcost;
    }

    public String getEstimatedCompleteSchedule() {
        return estimatedCompleteSchedule;
    }

    public void setEstimatedCompleteSchedule(String estimated_complete_schedule) {
        this.estimatedCompleteSchedule = estimated_complete_schedule;
    }

    public String getEstimatedContractFieldLaborCost() {
        return estimatedContractFieldLaborCost;
    }

    public void setEstimatedContractFieldLaborCost(
            String estimated_contractfieldlaborcost) {
        this.estimatedContractFieldLaborCost = estimated_contractfieldlaborcost;
    }

    public String getEstimatedFreightCost() {
        return estimatedFreightCost;
    }

    public void setEstimatedFreightCost(String estimated_freightcost) {
        this.estimatedFreightCost = estimated_freightcost;
    }

    public String getEstimatedMaterialCost() {
        return estimatedMaterialCost;
    }

    public void setEstimatedMaterialCost(String estimated_materialcost) {
        this.estimatedMaterialCost = estimated_materialcost;
    }

    public String getEstimatedStartSchedule() {
        return estimatedStartSchedule;
    }

    public void setEstimatedStartSchedule(String estimated_start_schedule) {
        this.estimatedStartSchedule = estimated_start_schedule;
    }

    public String getEstimatedTotalCost() {
        return estimatedTotalCost;
    }

    public void setEstimatedTotalCost(String estimated_totalcost) {
        this.estimatedTotalCost = estimated_totalcost;
    }

    public String getEstimatedTravelCost() {
        return estimatedTravelCost;
    }

    public void setEstimatedTravelCost(String estimated_travelcost) {
        this.estimatedTravelCost = estimated_travelcost;
    }

    public String getExtendedPrice() {
        return extendedPrice;
    }

    public void setExtendedPrice(String extendedprice) {
        this.extendedPrice = extendedprice;
    }

    public String getInstallationPoc() {
        return installationPoc;
    }

    public void setInstallationPoc(String installation_POC) {
        this.installationPoc = installation_POC;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getLastChangeBy() {
        return lastChangeBy;
    }

    public void setLastChangeBy(String lastchangeby) {
        this.lastChangeBy = lastchangeby;
    }

    public String getLastChangeDate() {
        return lastChangeDate;
    }

    public void setLastChangeDate(String lastchangedate) {
        this.lastChangeDate = lastchangedate;
    }

    public String getLastComment() {
        return lastComment;
    }

    public void setLastComment(String lastcomment) {
        this.lastComment = lastcomment;
    }

    public String getLastJobModifiedBy() {
        return lastJobModifiedBy;
    }

    public void setLastJobModifiedBy(String lastjobmodifiedby) {
        this.lastJobModifiedBy = lastjobmodifiedby;
    }

    public String getLastJobModifiedDate() {
        return lastJobModifiedDate;
    }

    public void setLastJobModifiedDate(String lastjobmodifieddate) {
        this.lastJobModifiedDate = lastjobmodifieddate;
    }

    public String getLocalityUplift() {
        return localityUplift;
    }

    public void setLocalityUplift(String locality_uplift) {
        this.localityUplift = locality_uplift;
    }

    public String getMsaId() {
        return msaId;
    }

    public void setMsaId(String msa_Id) {
        this.msaId = msa_Id;
    }

    public String getProformaMargin() {
        return proformaMargin;
    }

    public void setProformaMargin(String proformamargin) {
        this.proformaMargin = proformamargin;
    }

    public String getSiteAddress() {
        return siteAddress;
    }

    public void setSiteAddress(String site_address) {
        this.siteAddress = site_address;
    }

    public String getSiteCity() {
        return siteCity;
    }

    public void setSiteCity(String site_city) {
        this.siteCity = site_city;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String site_id) {
        this.siteId = site_id;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String site_name) {
        this.siteName = site_name;
    }

    public String getSiteNumber() {
        return siteNumber;
    }

    public void setSiteNumber(String site_number) {
        this.siteNumber = site_number;
    }

    public String getSitePhone() {
        return sitePhone;
    }

    public void setSitePhone(String site_phone) {
        this.sitePhone = site_phone;
    }

    public String getSitePoc() {
        return sitePoc;
    }

    public void setSitePoc(String site_POC) {
        this.sitePoc = site_POC;
    }

    public String getSiteState() {
        return siteState;
    }

    public void setSiteState(String site_state) {
        this.siteState = site_state;
    }

    public String getSiteZipcode() {
        return siteZipcode;
    }

    public void setSiteZipcode(String site_zipcode) {
        this.siteZipcode = site_zipcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUnion() {
        return union;
    }

    public void setUnion(String union) {
        this.union = union;
    }

    public String getUnionUpliftFactor() {
        return unionUpliftFactor;
    }

    public void setUnionUpliftFactor(String union_uplift_factor) {
        this.unionUpliftFactor = union_uplift_factor;
    }

    public String getSave() {
        return save;
    }

    public void setSave(String save) {
        this.save = save;
    }

    public String getAppendixName() {
        return appendixName;
    }

    public void setAppendixName(String appendixName) {
        this.appendixName = appendixName;
    }

    public String getMsaName() {
        return msaName;
    }

    public void setMsaName(String msaname) {
        this.msaName = msaname;
    }

    public String getBack() {
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getRequestorIds() {
        return requestorIds;
    }

    public void setRequestorIds(String requestorIds) {
        this.requestorIds = requestorIds;
    }

    public String getAdditionalRecipientIds() {
        return additionalRecipientIds;
    }

    public void setAdditionalRecipientIds(String additionalRecipientIds) {
        this.additionalRecipientIds = additionalRecipientIds;
    }

    public String getAppendixId() {
        return appendixId;
    }

    public void setAppendixId(String appendixId) {
        this.appendixId = appendixId;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getViewjobtype() {
        return viewjobtype;
    }

    public void setViewjobtype(String viewjobtype) {
        this.viewjobtype = viewjobtype;
    }

    public String getStartdatehh() {
        return startdatehh;
    }

    public void setStartdatehh(String startdatehh) {
        this.startdatehh = startdatehh;
    }

    public String getStartdatemm() {
        return startdatemm;
    }

    public void setStartdatemm(String startdatemm) {
        this.startdatemm = startdatemm;
    }

    public String getStartoptionname() {
        return startoptionname;
    }

    public void setStartoptionname(String startoptionname) {
        this.startoptionname = startoptionname;
    }

    public String getCompletedatehh() {
        return completedatehh;
    }

    public void setCompletedatehh(String completedatehh) {
        this.completedatehh = completedatehh;
    }

    public String getCompletedatemm() {
        return completedatemm;
    }

    public void setCompletedatemm(String completedatemm) {
        this.completedatemm = completedatemm;
    }

    public String getCompleteoptionname() {
        return completeoptionname;
    }

    public void setCompleteoptionname(String completeoptionname) {
        this.completeoptionname = completeoptionname;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getRequestorEmail() {
        return requestorEmail;
    }

    public void setRequestorEmail(String requestorEmail) {
        this.requestorEmail = requestorEmail;
    }

    public String getRequestor() {
        return requestor;
    }

    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }

    public ArrayList<LabelValue> getRequestorList() {
        return requestorList;
    }

    public void setRequestorList(ArrayList<LabelValue> requestorList) {
        this.requestorList = requestorList;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

}
