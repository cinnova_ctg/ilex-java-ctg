package com.mind.bean.pm;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The Class ViewSelectorBean.
 */
public class ViewSelectorBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The appendix id. */
	private String appendixId = null;

	/** The msa id. */
	private String msaId = null;

	/** The appendix selected status. */
	private String[] appendixSelectedStatus = null;

	/** The appendix selected owners. */
	private String[] appendixSelectedOwners = null;

	/** The appendix other check. */
	private String appendixOtherCheck = null;

	/** The prev selected status. */
	private String prevSelectedStatus = null;

	/** The prev selected owner. */
	private String prevSelectedOwner = null;

	/** The go. */
	private String go = null;

	/** The home. */
	private String home = null;

	/** The month week check. */
	private String monthWeekCheck = null;

	/** The check appendix. */
	private String checkAppendix = null;

	/** The msa name. */
	private String msaName = null;

	/** The appendix name. */
	private String appendixName = null;

	/** The type. */
	private String type = null;

	/** The selected owner. */
	private String selectedOwner = null;

	/** The temp owner. */
	private String tempOwner = null;

	/** The selected status. */
	private String selectedStatus = null;

	/** The temp status. */
	private String tempStatus = null;

	/** The status list. */
	private ArrayList statusList = new ArrayList();

	/** The owner list. */
	private ArrayList ownerList = new ArrayList();

	/** The chk other owner. */
	private boolean chkOtherOwner = false;

	/** The url. */
	private String url = null;

	/**
	 * Gets the url.
	 * 
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 * 
	 * @param url
	 *            the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Checks if is chk other owner.
	 * 
	 * @return true, if is chk other owner
	 */
	public boolean isChkOtherOwner() {
		return chkOtherOwner;
	}

	/**
	 * Sets the chk other owner.
	 * 
	 * @param chkOtherOwner
	 *            the new chk other owner
	 */
	public void setChkOtherOwner(boolean chkOtherOwner) {
		this.chkOtherOwner = chkOtherOwner;
	}

	/**
	 * Gets the appendix id.
	 * 
	 * @return the appendix id
	 */
	public String getAppendixId() {
		return appendixId;
	}

	/**
	 * Sets the appendix id.
	 * 
	 * @param appendixId
	 *            the new appendix id
	 */
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	/**
	 * Gets the appendix name.
	 * 
	 * @return the appendix name
	 */
	public String getAppendixName() {
		return appendixName;
	}

	/**
	 * Sets the appendix name.
	 * 
	 * @param appendixName
	 *            the new appendix name
	 */
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	/**
	 * Gets the appendix other check.
	 * 
	 * @return the appendix other check
	 */
	public String getAppendixOtherCheck() {
		return appendixOtherCheck;
	}

	/**
	 * Sets the appendix other check.
	 * 
	 * @param appendixOtherCheck
	 *            the new appendix other check
	 */
	public void setAppendixOtherCheck(String appendixOtherCheck) {
		this.appendixOtherCheck = appendixOtherCheck;
	}

	/**
	 * Gets the appendix selected owners.
	 * 
	 * @return the appendix selected owners
	 */
	public String[] getAppendixSelectedOwners() {
		return appendixSelectedOwners;
	}

	/**
	 * Sets the appendix selected owners.
	 * 
	 * @param appendixSelectedOwners
	 *            the new appendix selected owners
	 */
	public void setAppendixSelectedOwners(String[] appendixSelectedOwners) {
		this.appendixSelectedOwners = appendixSelectedOwners;
	}

	/**
	 * Gets the appendix selected status.
	 * 
	 * @return the appendix selected status
	 */
	public String[] getAppendixSelectedStatus() {
		return appendixSelectedStatus;
	}

	/**
	 * Gets the appendix selected owners.
	 * 
	 * @param i
	 *            the i
	 * 
	 * @return the appendix selected owners
	 */
	public String getAppendixSelectedOwners(int i) {
		return appendixSelectedOwners[i];
	}

	/**
	 * Sets the appendix selected status.
	 * 
	 * @param appendixSelectedStatus
	 *            the new appendix selected status
	 */
	public void setAppendixSelectedStatus(String[] appendixSelectedStatus) {
		this.appendixSelectedStatus = appendixSelectedStatus;
	}

	/**
	 * Gets the appendix selected status.
	 * 
	 * @param i
	 *            the i
	 * 
	 * @return the appendix selected status
	 */
	public String getAppendixSelectedStatus(int i) {
		return appendixSelectedStatus[i];
	}

	/**
	 * Gets the check appendix.
	 * 
	 * @return the check appendix
	 */
	public String getCheckAppendix() {
		return checkAppendix;
	}

	/**
	 * Sets the check appendix.
	 * 
	 * @param checkAppendix
	 *            the new check appendix
	 */
	public void setCheckAppendix(String checkAppendix) {
		this.checkAppendix = checkAppendix;
	}

	/**
	 * Gets the go.
	 * 
	 * @return the go
	 */
	public String getGo() {
		return go;
	}

	/**
	 * Sets the go.
	 * 
	 * @param go
	 *            the new go
	 */
	public void setGo(String go) {
		this.go = go;
	}

	/**
	 * Gets the home.
	 * 
	 * @return the home
	 */
	public String getHome() {
		return home;
	}

	/**
	 * Sets the home.
	 * 
	 * @param home
	 *            the new home
	 */
	public void setHome(String home) {
		this.home = home;
	}

	/**
	 * Gets the month week check.
	 * 
	 * @return the month week check
	 */
	public String getMonthWeekCheck() {
		return monthWeekCheck;
	}

	/**
	 * Sets the month week check.
	 * 
	 * @param monthWeekCheck
	 *            the new month week check
	 */
	public void setMonthWeekCheck(String monthWeekCheck) {
		this.monthWeekCheck = monthWeekCheck;
	}

	/**
	 * Gets the msa id.
	 * 
	 * @return the msa id
	 */
	public String getMsaId() {
		return msaId;
	}

	/**
	 * Sets the msa id.
	 * 
	 * @param msaId
	 *            the new msa id
	 */
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	/**
	 * Gets the msa name.
	 * 
	 * @return the msa name
	 */
	public String getMsaName() {
		return msaName;
	}

	/**
	 * Sets the msa name.
	 * 
	 * @param msaName
	 *            the new msa name
	 */
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	/**
	 * Gets the prev selected owner.
	 * 
	 * @return the prev selected owner
	 */
	public String getPrevSelectedOwner() {
		return prevSelectedOwner;
	}

	/**
	 * Sets the prev selected owner.
	 * 
	 * @param prevSelectedOwner
	 *            the new prev selected owner
	 */
	public void setPrevSelectedOwner(String prevSelectedOwner) {
		this.prevSelectedOwner = prevSelectedOwner;
	}

	/**
	 * Gets the prev selected status.
	 * 
	 * @return the prev selected status
	 */
	public String getPrevSelectedStatus() {
		return prevSelectedStatus;
	}

	/**
	 * Sets the prev selected status.
	 * 
	 * @param prevSelectedStatus
	 *            the new prev selected status
	 */
	public void setPrevSelectedStatus(String prevSelectedStatus) {
		this.prevSelectedStatus = prevSelectedStatus;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the selected owner.
	 * 
	 * @return the selected owner
	 */
	public String getSelectedOwner() {
		return selectedOwner;
	}

	/**
	 * Sets the selected owner.
	 * 
	 * @param selectedOwner
	 *            the new selected owner
	 */
	public void setSelectedOwner(String selectedOwner) {
		this.selectedOwner = selectedOwner;
	}

	/**
	 * Gets the temp owner.
	 * 
	 * @return the temp owner
	 */
	public String getTempOwner() {
		return tempOwner;
	}

	/**
	 * Sets the temp owner.
	 * 
	 * @param tempOwner
	 *            the new temp owner
	 */
	public void setTempOwner(String tempOwner) {
		this.tempOwner = tempOwner;
	}

	/**
	 * Gets the selected status.
	 * 
	 * @return the selected status
	 */
	public String getSelectedStatus() {
		return selectedStatus;
	}

	/**
	 * Sets the selected status.
	 * 
	 * @param selectedStatus
	 *            the new selected status
	 */
	public void setSelectedStatus(String selectedStatus) {
		this.selectedStatus = selectedStatus;
	}

	/**
	 * Gets the temp status.
	 * 
	 * @return the temp status
	 */
	public String getTempStatus() {
		return tempStatus;
	}

	/**
	 * Sets the temp status.
	 * 
	 * @param tempStatus
	 *            the new temp status
	 */
	public void setTempStatus(String tempStatus) {
		this.tempStatus = tempStatus;
	}

	/**
	 * Gets the owner list.
	 * 
	 * @return the owner list
	 */
	public ArrayList getOwnerList() {
		return ownerList;
	}

	/**
	 * Sets the owner list.
	 * 
	 * @param ownerList
	 *            the new owner list
	 */
	public void setOwnerList(ArrayList ownerList) {
		this.ownerList = ownerList;
	}

	/**
	 * Gets the status list.
	 * 
	 * @return the status list
	 */
	public ArrayList getStatusList() {
		return statusList;
	}

	/**
	 * Sets the status list.
	 * 
	 * @param statusList
	 *            the new status list
	 */
	public void setStatusList(ArrayList statusList) {
		this.statusList = statusList;
	}

}
