package com.mind.bean.pm;

import java.io.Serializable;
import java.util.ArrayList;

import com.mind.common.LabelValue;

/**
 * The Class ESAEditBean.
 */
public class ESAEditBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The agent. */
	private String[] agent = null;

	/** The commission. */
	private String[] commission = null;

	/** The payement terms. */
	private String[] payementTerms = null;
	private String[] status = null;

	/** The effective date. */
	private String[] effectiveDate = null;
	private String[] esaId = null;
	private String[] esaSeqNo = null;
	private String[] commissionType = null;

	/** The esa agent list. */
	private ArrayList<LabelValue> esaAgentList = null;

	/** The esa payment list. */
	private ArrayList<LabelValue> esaPaymentList = null;
	private ArrayList<ESAList> esaList = null;

	private String appendixId = null;

	public String[] getAgent() {
		return agent;
	}

	public void setAgent(String[] agent) {
		this.agent = agent;
	}

	public String getAgent(int i) {
		return agent[i];
	}

	public String[] getCommission() {
		return commission;
	}

	public void setCommission(String[] commission) {
		this.commission = commission;
	}

	public String getCommission(int i) {
		return commission[i];
	}

	public String[] getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String[] effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getEffectiveDate(int i) {
		return effectiveDate[i];
	}

	public ArrayList<LabelValue> getEsaAgentList() {
		return esaAgentList;
	}

	public void setEsaAgentList(ArrayList<LabelValue> esaAgentList) {
		this.esaAgentList = esaAgentList;
	}

	public String[] getEsaId() {
		return esaId;
	}

	public void setEsaId(String[] esaId) {
		this.esaId = esaId;
	}

	public String getEsaId(int i) {
		return esaId[i];
	}

	public ArrayList<LabelValue> getEsaPaymentList() {
		return esaPaymentList;
	}

	public void setEsaPaymentList(ArrayList<LabelValue> esaPaymentList) {
		this.esaPaymentList = esaPaymentList;
	}

	public String[] getEsaSeqNo() {
		return esaSeqNo;
	}

	public void setEsaSeqNo(String[] esaSeqNo) {
		this.esaSeqNo = esaSeqNo;
	}

	public String getEsaSeqNo(int i) {
		return esaSeqNo[i];
	}

	public String[] getPayementTerms() {
		return payementTerms;
	}

	public void setPayementTerms(String[] payementTerms) {
		this.payementTerms = payementTerms;
	}

	public String getPayementTerms(int i) {
		return payementTerms[i];
	}

	public String[] getStatus() {
		return status;
	}

	public void setStatus(String[] status) {
		this.status = status;
	}

	public String getStatus(int i) {
		return status[i];
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public ArrayList<ESAList> getEsaList() {
		return esaList;
	}

	public void setEsaList(ArrayList<ESAList> esaList) {
		this.esaList = esaList;
	}

	public String[] getCommissionType() {
		return commissionType;
	}

	public void setCommissionType(String[] commissionType) {
		this.commissionType = commissionType;
	}

	public String getCommissionType(int i) {
		return commissionType[i];
	}
}
