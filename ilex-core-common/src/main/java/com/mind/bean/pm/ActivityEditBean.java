package com.mind.bean.pm;

import java.io.Serializable;

import com.mind.bean.prm.JobSetUp;

public class ActivityEditBean extends JobSetUp implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String activityName = null;
	private String activityTypeDetail = null;
	private String quantity = null;
	private String estimatedMaterialcost = null; 
	private String estimatedCnsfieldhqlaborcost = null;
	private String estimatedFreightcost = null;
	private String estimatedTravelcost = null;
	private String estimatedTotalcost = null;
	private String extendedprice = null;
	private String overheadcost = null;
	private String listprice = null;
	private String estimatedStartSchedule = null;
	private String estimatedCompleteSchedule = null;
	private String proformamargin = null;
	private String estimatedContractfieldlaborcost = null;
	private String save = null;
	private String jobId = null;
	private String jobName = null;
	private String lastactivitymodifiedby = null;
	private String lastactivitymodifieddate = null;
	private String creatBy = null;
	private String creatDate = null;
	private String newStatus = null;
	private String Activity_Id = null;
	private String estimatedCnshqlaborcost = null;
	private String addendum_id = null;
	private String activity_lib_Id = null;
	private String activity_cost_type = null;
	private String msaName = null;
	private String msaId = null;
	private String appendixName = null;
	private String appendixid = null;
	private String activityType = null;
	private String jobType = null;
	private String EstimatedCnsfieldlaborcost = null;
	private String commentdate = null;
	private String commentby = null;
	private String comment = null;
	private String chkaddendum = null;
	private String ref = null;
	private String from = null;
	
	
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getChkaddendum() {
		return chkaddendum;
	}
	public void setChkaddendum(String chkaddendum) {
		this.chkaddendum = chkaddendum;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCommentby() {
		return commentby;
	}
	public void setCommentby(String commentby) {
		this.commentby = commentby;
	}
	public String getCommentdate() {
		return commentdate;
	}
	public void setCommentdate(String commentdate) {
		this.commentdate = commentdate;
	}
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public String getActivity_lib_Id() {
		return activity_lib_Id;
	}
	public void setActivity_lib_Id(String activity_lib_Id) {
		this.activity_lib_Id = activity_lib_Id;
	}
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}
	public String getEstimatedCnshqlaborcost() {
		return estimatedCnshqlaborcost;
	}
	public void setEstimatedCnshqlaborcost(String estimatedCnshqlaborcost) {
		this.estimatedCnshqlaborcost = estimatedCnshqlaborcost;
	}
	public String getActivity_Id() {
		return Activity_Id;
	}
	public void setActivity_Id(String activity_Id) {
		Activity_Id = activity_Id;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
	
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	
	
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getActivityTypeDetail() {
		return activityTypeDetail;
	}
	public void setActivityTypeDetail(String activityTypeDetail) {
		this.activityTypeDetail = activityTypeDetail;
	}
	public String getNewStatus() {
		return newStatus;
	}
	public void setNewStatus(String newStatus) {
		this.newStatus = newStatus;
	}
	public String getActivity_cost_type() {
		return activity_cost_type;
	}
	public void setActivity_cost_type(String activity_cost_type) {
		this.activity_cost_type = activity_cost_type;
	}
	public String getAppendixName() {
		return appendixName;
	}
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}
	public String getMsaName() {
		return msaName;
	}
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getAppendixid() {
		return appendixid;
	}
	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}
	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	
	public String getEstimatedCnsfieldhqlaborcost() {
		return estimatedCnsfieldhqlaborcost;
	}
	public void setEstimatedCnsfieldhqlaborcost(String estimatedCnsfieldhqlaborcost) {
		this.estimatedCnsfieldhqlaborcost = estimatedCnsfieldhqlaborcost;
	}
	public String getEstimatedCompleteSchedule() {
		return estimatedCompleteSchedule;
	}
	public void setEstimatedCompleteSchedule(String estimatedCompleteSchedule) {
		this.estimatedCompleteSchedule = estimatedCompleteSchedule;
	}
	public String getEstimatedContractfieldlaborcost() {
		return estimatedContractfieldlaborcost;
	}
	public void setEstimatedContractfieldlaborcost(
			String estimatedContractfieldlaborcost) {
		this.estimatedContractfieldlaborcost = estimatedContractfieldlaborcost;
	}
	public String getEstimatedFreightcost() {
		return estimatedFreightcost;
	}
	public void setEstimatedFreightcost(String estimatedFreightcost) {
		this.estimatedFreightcost = estimatedFreightcost;
	}
	public String getEstimatedMaterialcost() {
		return estimatedMaterialcost;
	}
	public void setEstimatedMaterialcost(String estimatedMaterialcost) {
		this.estimatedMaterialcost = estimatedMaterialcost;
	}
	public String getEstimatedStartSchedule() {
		return estimatedStartSchedule;
	}
	public void setEstimatedStartSchedule(String estimatedStartSchedule) {
		this.estimatedStartSchedule = estimatedStartSchedule;
	}
	public String getEstimatedTotalcost() {
		return estimatedTotalcost;
	}
	public void setEstimatedTotalcost(String estimatedTotalcost) {
		this.estimatedTotalcost = estimatedTotalcost;
	}
	public String getEstimatedTravelcost() {
		return estimatedTravelcost;
	}
	public void setEstimatedTravelcost(String estimatedTravelcost) {
		this.estimatedTravelcost = estimatedTravelcost;
	}
	public String getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice(String extendedprice) {
		this.extendedprice = extendedprice;
	}
	public String getListprice() {
		return listprice;
	}
	public void setListprice(String listprice) {
		this.listprice = listprice;
	}
	public String getOverheadcost() {
		return overheadcost;
	}
	public void setOverheadcost(String overheadcost) {
		this.overheadcost = overheadcost;
	}
	public String getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin(String proformamargin) {
		this.proformamargin = proformamargin;
	}
	public String getEstimatedCnsfieldlaborcost() {
		return EstimatedCnsfieldlaborcost;
	}
	public void setEstimatedCnsfieldlaborcost(String estimatedCnsfieldlaborcost) {
		EstimatedCnsfieldlaborcost = estimatedCnsfieldlaborcost;
	}
	public String getLastactivitymodifiedby() {
		return lastactivitymodifiedby;
	}
	public void setLastactivitymodifiedby(String lastactivitymodifiedby) {
		this.lastactivitymodifiedby = lastactivitymodifiedby;
	}
	public String getLastactivitymodifieddate() {
		return lastactivitymodifieddate;
	}
	public void setLastactivitymodifieddate(String lastactivitymodifieddate) {
		this.lastactivitymodifieddate = lastactivitymodifieddate;
	}
	public String getCreatBy() {
		return creatBy;
	}
	public void setCreatBy(String creatBy) {
		this.creatBy = creatBy;
	}
	public String getCreatDate() {
		return creatDate;
	}
	public void setCreatDate(String creatDate) {
		this.creatDate = creatDate;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}

}



