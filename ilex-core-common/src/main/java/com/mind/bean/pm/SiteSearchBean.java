package com.mind.bean.pm;

public class SiteSearchBean {

	//private String siteid = null;
	
	
	private String msaid = null;
	private String sitename = null;
	private String sitenumber = null;
	private String siteaddress = null;
	private String sitecity = null;
	private String sitestate = null;
	private String sitestatedesc = null;
	private String sitecountry = null;
	
	private String selectedsite = null;
	
	private String sitelistid = null;
	private String sitelocalityfactor = null;
	private String unionsite = null;
	private String sitedesignator = null;
	private String siteworklocation = null;
	private String sitezipcode = null;
	private String sitesecphone = null;
	private String sitephone = null;
	private String sitedirection = null;
	private String sitepripoc = null;
	private String sitesecpoc = null;
	private String sitepriemail = null;
	private String sitesecemail = null;
	private String sitenotes = null;
	private String installerpoc = null;
	private String sitepoc = null;
	private String sitelatdeg = null;
	private String sitelatmin = null;
	private String sitelatdirection = null;
	private String sitelondeg = null;
	private String sitelonmin = null;
	private String sitelondirection = null;
	private String sitebrand = null;
	private String sitestatus = null;
	private String sitecategory  = null;
	private String siteregion = null;
	private String siteendcustomer = null;
	private String sitetimezone = null;
	
	
	public String getInstallerpoc() {
		return installerpoc;
	}
	public void setInstallerpoc(String installerpoc) {
		this.installerpoc = installerpoc;
	}
	public String getSelectedsite() {
		return selectedsite;
	}
	public void setSelectedsite(String selectedsite) {
		this.selectedsite = selectedsite;
	}
	public String getSiteaddress() {
		return siteaddress;
	}
	public void setSiteaddress(String siteaddress) {
		this.siteaddress = siteaddress;
	}
	public String getSitecity() {
		return sitecity;
	}
	public void setSitecity(String sitecity) {
		this.sitecity = sitecity;
	}
	public String getSitecountry() {
		return sitecountry;
	}
	public void setSitecountry(String sitecountry) {
		this.sitecountry = sitecountry;
	}
	public String getSitedesignator() {
		return sitedesignator;
	}
	public void setSitedesignator(String sitedesignator) {
		this.sitedesignator = sitedesignator;
	}
	public String getSitedirection() {
		return sitedirection;
	}
	public void setSitedirection(String sitedirection) {
		this.sitedirection = sitedirection;
	}
	public String getSitelistid() {
		return sitelistid;
	}
	public void setSitelistid(String sitelistid) {
		this.sitelistid = sitelistid;
	}
	public String getSitelocalityfactor() {
		return sitelocalityfactor;
	}
	public void setSitelocalityfactor(String sitelocalityfactor) {
		this.sitelocalityfactor = sitelocalityfactor;
	}
	public String getSitename() {
		return sitename;
	}
	public void setSitename(String sitename) {
		this.sitename = sitename;
	}
	public String getSitenotes() {
		return sitenotes;
	}
	public void setSitenotes(String sitenotes) {
		this.sitenotes = sitenotes;
	}
	public String getSitenumber() {
		return sitenumber;
	}
	public void setSitenumber(String sitenumber) {
		this.sitenumber = sitenumber;
	}
	public String getSitephone() {
		return sitephone;
	}
	public void setSitephone(String sitephone) {
		this.sitephone = sitephone;
	}
	public String getSitepoc() {
		return sitepoc;
	}
	public void setSitepoc(String sitepoc) {
		this.sitepoc = sitepoc;
	}
	public String getSitepriemail() {
		return sitepriemail;
	}
	public void setSitepriemail(String sitepriemail) {
		this.sitepriemail = sitepriemail;
	}
	public String getSitepripoc() {
		return sitepripoc;
	}
	public void setSitepripoc(String sitepripoc) {
		this.sitepripoc = sitepripoc;
	}
	public String getSitesecemail() {
		return sitesecemail;
	}
	public void setSitesecemail(String sitesecemail) {
		this.sitesecemail = sitesecemail;
	}
	public String getSitesecphone() {
		return sitesecphone;
	}
	public void setSitesecphone(String sitesecphone) {
		this.sitesecphone = sitesecphone;
	}
	public String getSitesecpoc() {
		return sitesecpoc;
	}
	public void setSitesecpoc(String sitesecpoc) {
		this.sitesecpoc = sitesecpoc;
	}
	public String getSitestate() {
		return sitestate;
	}
	public void setSitestate(String sitestate) {
		this.sitestate = sitestate;
	}
	public String getSiteworklocation() {
		return siteworklocation;
	}
	public void setSiteworklocation(String siteworklocation) {
		this.siteworklocation = siteworklocation;
	}
	public String getSitezipcode() {
		return sitezipcode;
	}
	public void setSitezipcode(String sitezipcode) {
		this.sitezipcode = sitezipcode;
	}
	public String getUnionsite() {
		return unionsite;
	}
	public void setUnionsite(String unionsite) {
		this.unionsite = unionsite;
	}
	public String getMsaid() {
		return msaid;
	}
	public void setMsaid(String msaid) {
		this.msaid = msaid;
	}
	public String getSitestatedesc() {
		return sitestatedesc;
	}
	public void setSitestatedesc(String sitestatedesc) {
		this.sitestatedesc = sitestatedesc;
	}
	public String getSitelatdeg() {
		return sitelatdeg;
	}
	public void setSitelatdeg(String sitelatdeg) {
		this.sitelatdeg = sitelatdeg;
	}
	public String getSitelatdirection() {
		return sitelatdirection;
	}
	public void setSitelatdirection(String sitelatdirection) {
		this.sitelatdirection = sitelatdirection;
	}
	public String getSitelatmin() {
		return sitelatmin;
	}
	public void setSitelatmin(String sitelatmin) {
		this.sitelatmin = sitelatmin;
	}
	public String getSitelondeg() {
		return sitelondeg;
	}
	public void setSitelondeg(String sitelondeg) {
		this.sitelondeg = sitelondeg;
	}
	public String getSitelondirection() {
		return sitelondirection;
	}
	public void setSitelondirection(String sitelondirection) {
		this.sitelondirection = sitelondirection;
	}
	public String getSitelonmin() {
		return sitelonmin;
	}
	public void setSitelonmin(String sitelonmin) {
		this.sitelonmin = sitelonmin;
	}
	public String getSitecategory() {
		return sitecategory;
	}
	public void setSitecategory(String sitecategory) {
		this.sitecategory = sitecategory;
	}
	public String getSiteendcustomer() {
		return siteendcustomer;
	}
	public void setSiteendcustomer(String siteendcustomer) {
		this.siteendcustomer = siteendcustomer;
	}
	public String getSiteregion() {
		return siteregion;
	}
	public void setSiteregion(String siteregion) {
		this.siteregion = siteregion;
	}
	public String getSitestatus() {
		return sitestatus;
	}
	public void setSitestatus(String sitestatus) {
		this.sitestatus = sitestatus;
	}
	public String getSitetimezone() {
		return sitetimezone;
	}
	public void setSitetimezone(String sitetimezone) {
		this.sitetimezone = sitetimezone;
	}
	public String getSitebrand() {
		return sitebrand;
	}
	public void setSitebrand(String sitebrand) {
		this.sitebrand = sitebrand;
	}
	
}