/*
 * This class contains the getter and setter methods of activity elements
 * 
 * 
 */

package com.mind.bean.pm;

import java.io.Serializable;

public class Activity implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String job_Id = null;
	private String activity_Id = null;
	private String activity_lib_Id = null;
	private String name = null;
	
	private String activitytype = null;
	private String activitytypecombo = null;
	
	private String jobtype = null;
	
	private String quantity = null;
	private String estimated_materialcost = null;
	
	private String estimated_cnsfieldlaborcost = null;
	private String estimated_cnshqlaborcost = null;
	private String estimated_cnsfieldhqlaborcost = null;
	
	
	
	private String estimated_contractfieldlaborcost = null;
	private String estimated_freightcost = null;
	private String estimated_travelcost = null;
	private String estimated_totalcost = null;
	
	private String extendedprice = null;
	private String overheadcost = null;
	private String listprice = null;
	private String proformamargin = null;
	
	private String estimated_start_schedule = null;
	private String estimated_complete_schedule = null;
	
	private String lastcomment = null;
	private String lastchangeby = null;
	private String lastchangedate = null;
	private String status = null;
	private String lastactivitymodifiedby = null;
	private String lastactivitymodifieddate = null;
	
	private String addendum_id = null;
	
	private String estimated_unitcost = null;
	private String estimated_unitprice = null;
	
	private String temp_jobid = null;
	private String flag = null;
	private String job_name = null;
	private String msa_id = null;
	private String msaname = null;
	
	private String appendix_id = null;
	private String appendixname = null;
	private String bidFlag = null;
	
	private String commissionCost = null;
	private String commissionRevenue = null;
	
	public String getCommissionCost() {
		return commissionCost;
	}
	public void setCommissionCost(String commissionCost) {
		this.commissionCost = commissionCost;
	}
	public String getCommissionRevenue() {
		return commissionRevenue;
	}
	public void setCommissionRevenue(String commissionRevenue) {
		this.commissionRevenue = commissionRevenue;
	}
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String activity_Id ) {
		this.activity_Id = activity_Id;
	}
	
	
	public String getActivity_lib_Id() {
		return activity_lib_Id;
	}
	public void setActivity_lib_Id( String activity_lib_Id ) {
		this.activity_lib_Id = activity_lib_Id;
	}
	
	
	public String getActivitytype() {
		return activitytype;
	}
	public void setActivitytype( String activitytype ) {
		this.activitytype = activitytype;
	}
	
	
	public String getJobtype() {
		return jobtype;
	}
	public void setJobtype( String jobtype ) {
		this.jobtype = jobtype;
	}
	
	
	public String getActivitytypecombo() {
		return activitytypecombo;
	}
	public void setActivitytypecombo( String activitytypecombo ) {
		this.activitytypecombo = activitytypecombo;
	}
	
	
	public String getEstimated_cnsfieldlaborcost() {
		return estimated_cnsfieldlaborcost;
	}
	public void setEstimated_cnsfieldlaborcost( String estimated_cnsfieldlaborcost ) {
		this.estimated_cnsfieldlaborcost = estimated_cnsfieldlaborcost;
	}
	
	
	public String getEstimated_contractfieldlaborcost() {
		return estimated_contractfieldlaborcost;
	}
	public void setEstimated_contractfieldlaborcost( String estimated_contractfieldlaborcost ) {
		this.estimated_contractfieldlaborcost = estimated_contractfieldlaborcost;
	}
	
	
	public String getEstimated_freightcost() {
		return estimated_freightcost;
	}
	public void setEstimated_freightcost( String estimated_freightcost ) {
		this.estimated_freightcost = estimated_freightcost;
	}
	
	
	public String getEstimated_materialcost() {
		return estimated_materialcost;
	}
	public void setEstimated_materialcost( String estimated_materialcost ) {
		this.estimated_materialcost = estimated_materialcost;
	}
	
	
	public String getEstimated_totalcost() {
		return estimated_totalcost;
	}
	public void setEstimated_totalcost( String estimated_totalcost ) {
		this.estimated_totalcost = estimated_totalcost;
	}
	
	
	public String getEstimated_travelcost() {
		return estimated_travelcost;
	}
	public void setEstimated_travelcost( String estimated_travelcost ) {
		this.estimated_travelcost = estimated_travelcost;
	}
	
	
	public String getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice( String extendedprice ) {
		this.extendedprice = extendedprice;
	}
	
	
	public String getJob_Id() {
		return job_Id;
	}
	public void setJob_Id( String job_Id ) {
		this.job_Id = job_Id;
	}
	
	
	public String getLastchangeby() {
		return lastchangeby;
	}
	public void setLastchangeby( String lastchangeby ) {
		this.lastchangeby = lastchangeby;
	}
	
	
	public String getLastchangedate() {
		return lastchangedate;
	}
	public void setLastchangedate( String lastchangedate ) {
		this.lastchangedate = lastchangedate;
	}
	
	
	public String getLastcomment() {
		return lastcomment;
	}
	public void setLastcomment( String lastcomment ) {
		this.lastcomment = lastcomment;
	}
	
	
	public String getListprice() {
		return listprice;
	}
	public void setListprice( String listprice ) {
		this.listprice = listprice;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName( String name ) {
		this.name = name;
	}
	
	
	public String getOverheadcost() {
		return overheadcost;
	}
	public void setOverheadcost( String overheadcost ) {
		this.overheadcost = overheadcost;
	}
	
	
	public String getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin( String proformamargin ) {
		this.proformamargin = proformamargin;
	}
	
	
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity( String quantity ) {
		this.quantity = quantity;
	}
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus( String status ) {
		this.status = status;
	}
	
	
	public String getEstimated_complete_schedule() {
		return estimated_complete_schedule;
	}
	public void setEstimated_complete_schedule( String estimated_complete_schedule ) {
		this.estimated_complete_schedule = estimated_complete_schedule;
	}
	public String getEstimated_start_schedule() {
		return estimated_start_schedule;
	}
	public void setEstimated_start_schedule( String estimated_start_schedule ) {
		this.estimated_start_schedule = estimated_start_schedule;
	}
	public String getLastactivitymodifiedby() {
		return lastactivitymodifiedby;
	}
	public void setLastactivitymodifiedby(String lastactivitymodifiedby) {
		this.lastactivitymodifiedby = lastactivitymodifiedby;
	}
	public String getLastactivitymodifieddate() {
		return lastactivitymodifieddate;
	}
	public void setLastactivitymodifieddate(String lastactivitymodifieddate) {
		this.lastactivitymodifieddate = lastactivitymodifieddate;
	}
	
	
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id( String addendum_id ) {
		this.addendum_id = addendum_id;
	}
	
	
	public String getEstimated_cnshqlaborcost() {
		return estimated_cnshqlaborcost;
	}
	public void setEstimated_cnshqlaborcost( String estimated_cnshqlaborcost ) {
		this.estimated_cnshqlaborcost = estimated_cnshqlaborcost;
	}
	
	
	public String getEstimated_cnsfieldhqlaborcost() {
		return estimated_cnsfieldhqlaborcost;
	}
	public void setEstimated_cnsfieldhqlaborcost(
			String estimated_cnsfieldhqlaborcost) {
		this.estimated_cnsfieldhqlaborcost = estimated_cnsfieldhqlaborcost;
	}
	public String getEstimated_unitcost() {
		return estimated_unitcost;
	}
	public void setEstimated_unitcost(String estimated_unitcost) {
		this.estimated_unitcost = estimated_unitcost;
	}
	public String getEstimated_unitprice() {
		return estimated_unitprice;
	}
	public void setEstimated_unitprice(String estimated_unitprice) {
		this.estimated_unitprice = estimated_unitprice;
	}
	
	
	public String getTemp_jobid() {
		return temp_jobid;
	}
	public void setTemp_jobid(String temp_jobid) {
		this.temp_jobid = temp_jobid;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	public String getJob_name() {
		return job_name;
	}
	public void setJob_name(String job_name) {
		this.job_name = job_name;
	}
	
	
	public String getMsa_id() {
		return msa_id;
	}
	public void setMsa_id(String msa_id) {
		this.msa_id = msa_id;
	}
	
	
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	
	
	public String getAppendix_id() {
		return appendix_id;
	}
	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}
	
	
	public String getAppendixname() {
		return appendixname;
	}
	public void setAppendixname(String appendixname) {
		this.appendixname = appendixname;
	}
	public String getBidFlag() {
		return bidFlag;
	}
	public void setBidFlag(String bidFlag) {
		this.bidFlag = bidFlag;
	}
	
}
