package com.mind.bean.pm;

import java.util.Collection;

public class Appendix {
	private String appendix_Id = null;
	private String msa_Id = null;
	private String msaname = null;

	private String name = null;

	private String appendixtype = null;
	private String appendixtypecombo = null;

	private String cnsPOC = null;
	private String cnsPOCcombo = null;

	private String customerdivision = null;
	private String customerdivisioncombo = null;

	private String siteno = null;
	private String custref = null;
	private String endcust = null;

	private String estimatedcost = null;
	private String extendedprice = null;
	private String proformamargin = null;
	private String reqplasch = null;
	private String effplasch = null;
	private String schdays = null;

	private String draftreviews = null;
	private String draftreviewscombo = null;

	private String businessreviews = null;
	private String businessreviewscombo = null;

	private String custPocbusiness = null;
	private String custPocbusinesscombo = null;

	private String custPoccontract = null;
	private String custPoccontractcombo = null;

	private String custPocbilling = null;
	private String custPocbillingcombo = null;

	private String lastcomment = null;
	private String lastchangeby = null;
	private String lastchangedate = null;

	private String lastappendixmodifiedby = null;
	private String lastappendixmodifieddate = null;

	private String status = null;
	private Collection poclist = null;
	private String file_upload_check = null;

	private String unionupliftfactor = null;
	private String expedite24upliftfactor = null;
	private String expedite48upliftfactor = null;
	private String afterhoursupliftfactor = null;
	private String whupliftfactor = null;
	private String closed_job = null;
	private String inwork_job = null;
	private String complete_job = null;
	private String total_job = null;
	private String earliestStart = null;
	private String earliestEnd = null;
	private String latestStart = null;
	private String latestEnd = null;
	private String pendingWebTicket = null;
	private String toBeSchedule = null;
	private String schedule = null;
	private String overdue = null;
	private String msp = null;
	private String travelAuthorized = null;
	private String checkType = null;
	private String testAppendix = null;
	private String appendixTypeBreakout;

	public String getTestAppendix() {
		return testAppendix;
	}

	public void setTestAppendix(String testAppendix) {
		this.testAppendix = testAppendix;
	}

	public String getTravelAuthorized() {
		return travelAuthorized;
	}

	public void setTravelAuthorized(String travelAuthorized) {
		this.travelAuthorized = travelAuthorized;
	}

	public String getMsp() {
		return msp;
	}

	public void setMsp(String msp) {
		this.msp = msp;
	}

	public String getFile_upload_check() {
		return file_upload_check;
	}

	public void setFile_upload_check(String file_upload_check) {
		this.file_upload_check = file_upload_check;
	}

	public String getAppendix_Id() {
		return appendix_Id;
	}

	public void setAppendix_Id(String appendix_Id) {
		this.appendix_Id = appendix_Id;
	}

	public String getMsa_Id() {
		return msa_Id;
	}

	public void setMsa_Id(String msa_Id) {
		this.msa_Id = msa_Id;
	}

	public String getMsaname() {
		return msaname;
	}

	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	public String getAppendixtype() {
		return appendixtype;
	}

	public void setAppendixtype(String appendixtype) {
		this.appendixtype = appendixtype;
	}

	public String getAppendixtypecombo() {
		return appendixtypecombo;
	}

	public void setAppendixtypecombo(String appendixtypecombo) {
		this.appendixtypecombo = appendixtypecombo;
	}

	public String getBusinessreviews() {
		return businessreviews;
	}

	public void setBusinessreviews(String businessreviews) {
		this.businessreviews = businessreviews;
	}

	public String getBusinessreviewscombo() {
		return businessreviewscombo;
	}

	public void setBusinessreviewscombo(String businessreviewscombo) {
		this.businessreviewscombo = businessreviewscombo;
	}

	public String getCnsPOC() {
		return cnsPOC;
	}

	public void setCnsPOC(String cnsPOC) {
		this.cnsPOC = cnsPOC;
	}

	public String getCnsPOCcombo() {
		return cnsPOCcombo;
	}

	public void setCnsPOCcombo(String cnsPOCcombo) {
		this.cnsPOCcombo = cnsPOCcombo;
	}

	public String getCustomerdivision() {
		return customerdivision;
	}

	public void setCustomerdivision(String customerdivision) {
		this.customerdivision = customerdivision;
	}

	public String getCustomerdivisioncombo() {
		return customerdivisioncombo;
	}

	public void setCustomerdivisioncombo(String customerdivisioncombo) {
		this.customerdivisioncombo = customerdivisioncombo;
	}

	public String getCustPocbusiness() {
		return custPocbusiness;
	}

	public void setCustPocbusiness(String custPocbusiness) {
		this.custPocbusiness = custPocbusiness;
	}

	public String getCustPocbusinesscombo() {
		return custPocbusinesscombo;
	}

	public void setCustPocbusinesscombo(String custPocbusinesscombo) {
		this.custPocbusinesscombo = custPocbusinesscombo;
	}

	public String getCustPoccontract() {
		return custPoccontract;
	}

	public void setCustPoccontract(String custPoccontract) {
		this.custPoccontract = custPoccontract;
	}

	public String getCustPoccontractcombo() {
		return custPoccontractcombo;
	}

	public void setCustPoccontractcombo(String custPoccontractcombo) {
		this.custPoccontractcombo = custPoccontractcombo;
	}

	public String getDraftreviews() {
		return draftreviews;
	}

	public void setDraftreviews(String draftreviews) {
		this.draftreviews = draftreviews;
	}

	public String getDraftreviewscombo() {
		return draftreviewscombo;
	}

	public void setDraftreviewscombo(String draftreviewscombo) {
		this.draftreviewscombo = draftreviewscombo;
	}

	public String getEffplasch() {
		return effplasch;
	}

	public void setEffplasch(String effplasch) {
		this.effplasch = effplasch;
	}

	public String getEstimatedcost() {
		return estimatedcost;
	}

	public void setEstimatedcost(String estimatedcost) {
		this.estimatedcost = estimatedcost;
	}

	public String getExtendedprice() {
		return extendedprice;
	}

	public void setExtendedprice(String extendedprice) {
		this.extendedprice = extendedprice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNewcustPocbilling() {
		return custPocbilling;
	}

	public void setCustPocbilling(String custPocbilling) {
		this.custPocbilling = custPocbilling;
	}

	public String getCustPocbilling() {
		return custPocbilling;
	}

	public String getCustPocbillingcombo() {
		return custPocbillingcombo;
	}

	public void setCustPocbillingcombo(String custPocbillingcombo) {
		this.custPocbillingcombo = custPocbillingcombo;
	}

	public String getProformamargin() {
		return proformamargin;
	}

	public void setProformamargin(String proformamargin) {
		this.proformamargin = proformamargin;
	}

	public String getReqplasch() {
		return reqplasch;
	}

	public void setReqplasch(String reqplasch) {
		this.reqplasch = reqplasch;
	}

	public String getSchdays() {
		return schdays;
	}

	public void setSchdays(String schdays) {
		this.schdays = schdays;
	}

	public String getSiteno() {
		return siteno;
	}

	public void setSiteno(String siteno) {
		this.siteno = siteno;
	}

	public String getLastcomment() {
		return lastcomment;
	}

	public void setLastcomment(String lastcomment) {
		this.lastcomment = lastcomment;
	}

	public String getLastchangeby() {
		return lastchangeby;
	}

	public void setLastchangeby(String lastchangeby) {
		this.lastchangeby = lastchangeby;
	}

	public String getLastchangedate() {
		return lastchangedate;
	}

	public void setLastchangedate(String lastchangedate) {
		this.lastchangedate = lastchangedate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUnionupliftfactor() {
		return unionupliftfactor;
	}

	public void setUnionupliftfactor(String unionupliftfactor) {
		this.unionupliftfactor = unionupliftfactor;
	}

	public String getLastappendixmodifiedby() {
		return lastappendixmodifiedby;
	}

	public void setLastappendixmodifiedby(String lastappendixmodifiedby) {
		this.lastappendixmodifiedby = lastappendixmodifiedby;
	}

	public String getLastappendixmodifieddate() {
		return lastappendixmodifieddate;
	}

	public void setLastappendixmodifieddate(String lastappendixmodifieddate) {
		this.lastappendixmodifieddate = lastappendixmodifieddate;
	}

	public Collection getPoclist() {
		return poclist;
	}

	public void setPoclist(Collection poclist) {
		this.poclist = poclist;
	}

	public String getCustref() {
		return custref;
	}

	public void setCustref(String custref) {
		this.custref = custref;
	}

	public String getAfterhoursupliftfactor() {
		return afterhoursupliftfactor;
	}

	public void setAfterhoursupliftfactor(String afterhoursupliftfactor) {
		this.afterhoursupliftfactor = afterhoursupliftfactor;
	}

	public String getExpedite24upliftfactor() {
		return expedite24upliftfactor;
	}

	public void setExpedite24upliftfactor(String expedite24upliftfactor) {
		this.expedite24upliftfactor = expedite24upliftfactor;
	}

	public String getExpedite48upliftfactor() {
		return expedite48upliftfactor;
	}

	public void setExpedite48upliftfactor(String expedite48upliftfactor) {
		this.expedite48upliftfactor = expedite48upliftfactor;
	}

	public String getWhupliftfactor() {
		return whupliftfactor;
	}

	public void setWhupliftfactor(String whupliftfactor) {
		this.whupliftfactor = whupliftfactor;
	}

	public String getClosed_job() {
		return closed_job;
	}

	public void setClosed_job(String closed_job) {
		this.closed_job = closed_job;
	}

	public String getComplete_job() {
		return complete_job;
	}

	public void setComplete_job(String complete_job) {
		this.complete_job = complete_job;
	}

	public String getInwork_job() {
		return inwork_job;
	}

	public void setInwork_job(String inwork_job) {
		this.inwork_job = inwork_job;
	}

	public String getTotal_job() {
		return total_job;
	}

	public void setTotal_job(String total_job) {
		this.total_job = total_job;
	}

	public String getEarliestEnd() {
		return earliestEnd;
	}

	public void setEarliestEnd(String earliestEnd) {
		this.earliestEnd = earliestEnd;
	}

	public String getEarliestStart() {
		return earliestStart;
	}

	public void setEarliestStart(String earliestStart) {
		this.earliestStart = earliestStart;
	}

	public String getLatestEnd() {
		return latestEnd;
	}

	public void setLatestEnd(String latestEnd) {
		this.latestEnd = latestEnd;
	}

	public String getLatestStart() {
		return latestStart;
	}

	public void setLatestStart(String latestStart) {
		this.latestStart = latestStart;
	}

	public String getPendingWebTicket() {
		return pendingWebTicket;
	}

	public void setPendingWebTicket(String pendingWebTicket) {
		this.pendingWebTicket = pendingWebTicket;
	}

	public String getOverdue() {
		return overdue;
	}

	public void setOverdue(String overdue) {
		this.overdue = overdue;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public String getToBeSchedule() {
		return toBeSchedule;
	}

	public void setToBeSchedule(String toBeSchedule) {
		this.toBeSchedule = toBeSchedule;
	}

	public String getEndcust() {
		return endcust;
	}

	public void setEndcust(String endcust) {
		this.endcust = endcust;
	}

	public String getCheckType() {
		return checkType;
	}

	public void setCheckType(String checkType) {
		this.checkType = checkType;
	}

	/**
	 * @return the appendixTypeBreakout
	 */
	public String getAppendixTypeBreakout() {
		return appendixTypeBreakout;
	}

	/**
	 * @param appendixTypeBreakout
	 *            the appendixTypeBreakout to set
	 */
	public void setAppendixTypeBreakout(String appendixTypeBreakout) {
		this.appendixTypeBreakout = appendixTypeBreakout;
	}

}
