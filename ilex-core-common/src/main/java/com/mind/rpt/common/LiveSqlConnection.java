package com.mind.rpt.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;


/**
 * Discription: This class is use to make connection to //192.168.20.22 database.
 * Method	- getConnection().
 * @author vijaykumarsingh
 *
 */
public class LiveSqlConnection {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(LiveSqlConnection.class);

	/**
	 * Discription: Make connection form live database.
	 * @return Connection
	 */
		public static Connection  getConnection()
			{
				String dbdriver = LiveSqlConnection.getProperty("dbdriver"); // - net.sourceforge.jtds.jdbc.Driver
				String dburl = LiveSqlConnection.getProperty("dburl");		 // - jdbc:jtds:sqlserver://192.168.20.22;DatabaseName=ilex
				String dbuser = LiveSqlConnection.getProperty("dbuser");	 // - sa 
				String dbpswd = LiveSqlConnection.getProperty("dbpswd");	 // -  *******
				
				Connection con=null;	
						try
							{
							Class.forName(dbdriver);
							con = DriverManager.getConnection(dburl,dbuser,dbpswd);
							}
							catch(Exception ee)
							{
			logger.error("getConnection()", ee);

			if (logger.isDebugEnabled()) {
				logger.debug("getConnection() - Error in Connection Making" + ee);
			}
							}
				return con;
		}
	 	/**
	 	 * Use to get dbdriver, dburl, dbuser and dbpswd  from property file for connection.
	 	 * @param key		- property file key.
	 	 * @return String 	- key value 
	 	 */
	 	public static String getProperty(String key){
	         ResourceBundle rb = ResourceBundle.getBundle("com.mind.RPT.properties.ApplicationResourcesRPT");
	         String value = rb.getString(key);
	         return value;
		}
		
}
