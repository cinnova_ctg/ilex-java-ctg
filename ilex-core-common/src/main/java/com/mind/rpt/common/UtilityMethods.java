package com.mind.rpt.common;

import java.io.FileOutputStream;

import org.apache.log4j.Logger;
public class UtilityMethods {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UtilityMethods.class);

	/**
	 * Discription:	This method remove last char when it is ','.
	 * @param str		- String
	 * @return String	- modify string
	 */

	public static String removeLastComm(String str){
		str = str.trim();
		try{
			if(str.lastIndexOf(",") == (str.length() - 1) ){
				str = str.substring( 0, str.length()-1 );
			}
		}catch(Exception e){
			logger.warn("removeLastComm(String) - exception ignored", e);
}
		
		 return str;
	 }
	
	
}
