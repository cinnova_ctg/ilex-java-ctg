package module;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.contingent.ilex.report.DataAccess;

public class OpsSummaryFuture extends OpsSummaryData2 {
	public OpsSummaryFuture(HttpServletRequest request, String period,
			DataAccess da, String selected) {
		int i_selected = -1;
		try {
			Vector<String[]> raw_data = getRawDataView(period, da);

			Map<String, Integer> spm_index = mapSPM(raw_data);

			Map<String, Integer> type_index = mapPOType();

			double[][] matrix = buildMatrix(raw_data, spm_index, type_index);
			if (spm_index.containsKey(selected)) {
				i_selected = ((Integer) spm_index.get(selected)).intValue();
			}
			double[] stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("M")).intValue()],
					i_selected);

			formatAndAddToRequest("Future", stats, request);
		} catch (ReportingException localReportingException) {
		}
	}

	public OpsSummaryFuture() {
	}

	double[][] buildMatrix(Vector<String[]> raw_data,
			Map<String, Integer> spm_index, Map<String, Integer> type_index) {
		double[][] matrix = new double[type_index.size()][spm_index.size() + 1];
		int k = 0;
		int M = 0;
		int T = 1;
		int TOTAL = spm_index.size();
		for (int i = 0; i < raw_data.size(); i++) {
			k = ((Integer) spm_index.get(((String[]) raw_data.get(i))[1]))
					.intValue();

			matrix[M][k] = Double.parseDouble(((String[]) raw_data.get(i))[2]);

			matrix[M][TOTAL] += Double
					.parseDouble(((String[]) raw_data.get(i))[3]);

			matrix[T][k] += Double.parseDouble(((String[]) raw_data.get(i))[3]);
		}
		return matrix;
	}

	Map<String, Integer> mapPOType() {
		Map<String, Integer> type_index = new HashMap(5);

		type_index.put("M", Integer.valueOf(0));
		type_index.put("Total", Integer.valueOf(1));

		return type_index;
	}

	String SQL_GetRawData(String period) {
		String sql =

		"select ac_os_day_index, ac_os_spm, ac_op_percentage, ac_op_total   from datawarehouse.dbo.ac_fact2_ops_summary_po_future  where ac_op_type = 'M' and ac_os_day_index = "
				+

				period;

		return sql;
	}
}
