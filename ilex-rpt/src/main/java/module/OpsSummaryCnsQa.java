package module;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.contingent.ilex.report.DataAccess;

public class OpsSummaryCnsQa extends OpsSummaryData2 {
	public OpsSummaryCnsQa(HttpServletRequest request, String period,
			DataAccess da, String selected) {
		int i_selected = -1;
		try {
			Vector<String[]> raw_data = getRawDataView(period, da);

			Map<String, Integer> spm_index = mapSPM(raw_data);

			Map<String, Integer> type_index = mapPOType();

			double[][] matrix = buildMatrix(raw_data, spm_index, type_index);
			if (spm_index.containsKey(selected)) {
				i_selected = ((Integer) spm_index.get(selected)).intValue();
			}
			double[] stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("P")).intValue()],
					i_selected);
			formatAndAddToRequest("Poke", stats, request);

			stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("O")).intValue()],
					i_selected);
			formatAndAddToRequest("pOke", stats, request);

			stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("K")).intValue()],
					i_selected);
			formatAndAddToRequest("poKe", stats, request);

			stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("E")).intValue()],
					i_selected);
			formatAndAddToRequest("pokE", stats, request);
		} catch (ReportingException localReportingException) {
			localReportingException.printStackTrace();
		}
	}

	double[][] buildMatrix(Vector<String[]> raw_data,
			Map<String, Integer> spm_index, Map<String, Integer> type_index) {
		double[][] matrix = new double[type_index.size()][spm_index.size() + 1];
		int k = 0;
		int P = 0;
		int O = 1;
		int K = 2;
		int E = 3;
		int T = 4;
		int TOTAL = spm_index.size();
		for (int i = 0; i < raw_data.size(); i++) {
			k = ((Integer) spm_index.get(((String[]) raw_data.get(i))[1]))
					.intValue();

			matrix[P][k] = Double.parseDouble(((String[]) raw_data.get(i))[2]);
			matrix[O][k] = Double.parseDouble(((String[]) raw_data.get(i))[3]);
			matrix[K][k] = Double.parseDouble(((String[]) raw_data.get(i))[4]);
			matrix[E][k] = Double.parseDouble(((String[]) raw_data.get(i))[5]);

			matrix[P][TOTAL] += Double
					.parseDouble(((String[]) raw_data.get(i))[6]);
			matrix[O][TOTAL] += Double
					.parseDouble(((String[]) raw_data.get(i))[6]);
			matrix[K][TOTAL] += Double
					.parseDouble(((String[]) raw_data.get(i))[6]);
			matrix[E][TOTAL] += Double
					.parseDouble(((String[]) raw_data.get(i))[6]);

			matrix[T][k] += Double.parseDouble(((String[]) raw_data.get(i))[6]);
		}
		return matrix;
	}

	Map<String, Integer> mapPOType() {
		Map<String, Integer> type_index = new HashMap(5);

		type_index.put("P", Integer.valueOf(0));
		type_index.put("O", Integer.valueOf(1));
		type_index.put("K", Integer.valueOf(2));
		type_index.put("E", Integer.valueOf(3));
		type_index.put("T", Integer.valueOf(4));

		return type_index;
	}

	String SQL_GetRawData(String period) {
		String sql =

		"select ac_os_day_index, ac_os_spm, ac_os_p, ac_os_o, ac_os_k, ac_os_e, ac_os_count   from datawarehouse.dbo.ac_fact2_ops_sumary_cns_qa  where ac_os_day_index = "
				+

				period;

		return sql;
	}
}
