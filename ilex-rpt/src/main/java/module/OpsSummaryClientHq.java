package module;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.contingent.ilex.report.DataAccess;

public class OpsSummaryClientHq extends OpsSummaryData2 {
	public OpsSummaryClientHq(HttpServletRequest request, String period,
			DataAccess da, String selected) {
		int i_selected = -1;
		try {
			Vector<String[]> raw_data = getRawDataView(period, da);

			Map<String, Integer> spm_index = mapSPM(raw_data);

			Map<String, Integer> type_index = mapPOType();

			double[][] matrix = buildMatrix(raw_data, spm_index, type_index);
			if (spm_index.containsKey(selected)) {
				i_selected = ((Integer) spm_index.get(selected)).intValue();
			}
			double[] stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("CS")).intValue()],
					i_selected);
			formatAndAddToRequest("QA-H-CS", stats, request);

			stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("K")).intValue()],
					i_selected);
			formatAndAddToRequest("QA-H-K", stats, request);

			stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("E")).intValue()],
					i_selected);
			formatAndAddToRequest("QA-H-E", stats, request);

			stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("P")).intValue()],
					i_selected);
			formatAndAddToRequest("QA-H-P", stats, request);

			stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("O")).intValue()],
					i_selected);
			formatAndAddToRequest("QA-H-O", stats, request);

			stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("Overall")).intValue()],
					i_selected);
			formatRateAndAddToRequest("QA-H-Overall", stats, request);
		} catch (ReportingException localReportingException) {
		}
	}

	double[][] buildMatrix(Vector<String[]> raw_data,
			Map<String, Integer> spm_index, Map<String, Integer> type_index) {
		double[][] matrix = new double[type_index.size()][spm_index.size() + 1];
		int k = 0;

		int CS = 0;
		int K = 1;
		int E = 2;
		int P = 3;
		int O = 4;
		int OVERALL = 5;
		int TOTAL = spm_index.size();
		for (int i = 0; i < raw_data.size(); i++) {
			k = ((Integer) spm_index.get(((String[]) raw_data.get(i))[1]))
					.intValue();

			matrix[CS][k] = Double.parseDouble(((String[]) raw_data.get(i))[2]);
			matrix[K][k] = Double.parseDouble(((String[]) raw_data.get(i))[3]);
			matrix[E][k] = Double.parseDouble(((String[]) raw_data.get(i))[4]);
			matrix[P][k] = Double.parseDouble(((String[]) raw_data.get(i))[5]);
			matrix[O][k] = Double.parseDouble(((String[]) raw_data.get(i))[6]);
			matrix[OVERALL][k] = Double
					.parseDouble(((String[]) raw_data.get(i))[7]);

			matrix[CS][TOTAL] += Double
					.parseDouble(((String[]) raw_data.get(i))[8]);
			matrix[K][TOTAL] += Double
					.parseDouble(((String[]) raw_data.get(i))[8]);
			matrix[E][TOTAL] += Double
					.parseDouble(((String[]) raw_data.get(i))[8]);
			matrix[P][TOTAL] += Double
					.parseDouble(((String[]) raw_data.get(i))[8]);
			matrix[O][TOTAL] += Double
					.parseDouble(((String[]) raw_data.get(i))[8]);
			matrix[OVERALL][TOTAL] += Double.parseDouble(((String[]) raw_data
					.get(i))[8]);
		}
		return matrix;
	}

	Map<String, Integer> mapPOType() {
		Map<String, Integer> type_index = new HashMap(5);

		type_index.put("CS", Integer.valueOf(0));
		type_index.put("K", Integer.valueOf(1));
		type_index.put("E", Integer.valueOf(2));
		type_index.put("P", Integer.valueOf(3));
		type_index.put("O", Integer.valueOf(4));
		type_index.put("Overall", Integer.valueOf(5));

		return type_index;
	}

	String SQL_GetRawData(String period) {
		String sql =

		"select ac_os_day_index, ac_os_spm,        ac_os_job_completed_satisfactorily, ac_os_k, ac_os_e, ac_os_p, ac_os_o, ac_os_overall_rating, ac_os_count  from datawarehouse.dbo.ac_fact2_ops_sumary_client_hq  where ac_os_day_index = "
				+

				period;

		return sql;
	}
}
