package module;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.contingent.ilex.report.DataAccess;

public class OpsSummaryClientSite extends OpsSummaryData2 {
	public OpsSummaryClientSite(HttpServletRequest request, String period,
			DataAccess da, String selected) {
		int i_selected = -1;
		try {
			Vector<String[]> raw_data = getRawDataView(period, da);

			Map<String, Integer> spm_index = mapSPM(raw_data);

			Map<String, Integer> type_index = mapPOType();

			double[][] matrix = buildMatrix(raw_data, spm_index, type_index);
			if (spm_index.containsKey(selected)) {
				i_selected = ((Integer) spm_index.get(selected)).intValue();
			}
			double[] stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("Overall")).intValue()],
					i_selected);
			formatRateAndAddToRequest("QA-S-Overall", stats, request);

			stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("PKE")).intValue()],
					i_selected);
			formatAndAddToRequest("QA-S-PKE", stats, request);

			stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("O")).intValue()],
					i_selected);
			formatAndAddToRequest("QA-S-O", stats, request);

			stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("Issue")).intValue()],
					i_selected);
			formatRateAndAddToRequest("QA-S-Issue", stats, request);
		} catch (ReportingException localReportingException) {
		}
	}

	double[][] buildMatrix(Vector<String[]> raw_data,
			Map<String, Integer> spm_index, Map<String, Integer> type_index) {
		double[][] matrix = new double[type_index.size()][spm_index.size() + 1];
		int k = 0;

		int OVERALL = 0;
		int PKE = 1;
		int O = 2;
		int ISSUE = 3;
		int TOTAL = spm_index.size();
		for (int i = 0; i < raw_data.size(); i++) {
			k = ((Integer) spm_index.get(((String[]) raw_data.get(i))[1]))
					.intValue();

			matrix[OVERALL][k] = Double
					.parseDouble(((String[]) raw_data.get(i))[2]);
			matrix[PKE][k] = Double
					.parseDouble(((String[]) raw_data.get(i))[3]);
			matrix[O][k] = Double.parseDouble(((String[]) raw_data.get(i))[4]);
			matrix[ISSUE][k] = Double
					.parseDouble(((String[]) raw_data.get(i))[5]);

			matrix[OVERALL][TOTAL] += Double.parseDouble(((String[]) raw_data
					.get(i))[6]);
			matrix[PKE][TOTAL] += Double.parseDouble(((String[]) raw_data
					.get(i))[6]);
			matrix[O][TOTAL] += Double
					.parseDouble(((String[]) raw_data.get(i))[6]);
			matrix[ISSUE][TOTAL] += Double.parseDouble(((String[]) raw_data
					.get(i))[6]);
		}
		return matrix;
	}

	Map<String, Integer> mapPOType() {
		Map<String, Integer> type_index = new HashMap(5);

		type_index.put("Overall", Integer.valueOf(0));
		type_index.put("PKE", Integer.valueOf(1));
		type_index.put("O", Integer.valueOf(2));
		type_index.put("Issue", Integer.valueOf(3));
		type_index.put("Total", Integer.valueOf(4));

		return type_index;
	}

	String SQL_GetRawData(String period) {
		String sql =

		"select ac_os_day_index, ac_os_spm,        ac_os_overall as Overall, ac_os_pke as PKE, ac_os_o as O, ac_os_issue as Issue,        ac_os_count as Count  from datawarehouse.dbo.ac_fact2_ops_sumary_client_site  where ac_os_day_index = "
				+

				period;

		return sql;
	}
}
