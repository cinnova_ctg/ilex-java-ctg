package module;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.contingent.ilex.report.DataAccess;

public class OpsSummaryData2 {
	public OpsSummaryData2(HttpServletRequest request, String period,
			DataAccess da, String selected) {
		int i_selected = -1;
		try {
			Vector<String[]> raw_data = getRawDataView(period, da);

			Map<String, Integer> spm_index = mapSPM(raw_data);

			Map<String, Integer> type_index = mapPOType();

			double[][] matrix = buildMatrix(raw_data, spm_index, type_index);
			if (spm_index.containsKey(selected)) {
				i_selected = ((Integer) spm_index.get(selected)).intValue();
			}
			double[] stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("M")).intValue()],
					i_selected);
			formatAndAddToRequest("MM2", stats, request);

			stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("S")).intValue()],
					i_selected);
			formatAndAddToRequest("SP2", stats, request);

			stats = computeMatrixRowStatistics(
					matrix[((Integer) type_index.get("P")).intValue()],
					i_selected);
			formatAndAddToRequest("PVS2", stats, request);
		} catch (ReportingException localReportingException) {
		}
	}

	public OpsSummaryData2() {
	}

	void formatAndAddToRequest(String attr, double[] stats,
			HttpServletRequest request) {
		String[] format_stats = new String[stats.length];

		NumberFormat currency = new DecimalFormat("#,###");
		NumberFormat decimal_format = new DecimalFormat("#0.0");
		if (stats[0] == 0.0D) {
			format_stats[0] = "0";
		} else {
			format_stats[0] = decimal_format.format(stats[0] * 100.0D);
		}
		format_stats[1] = decimal_format.format(stats[1] * 100.0D);
		format_stats[2] = decimal_format.format(stats[2] * 100.0D);
		format_stats[3] = decimal_format.format(stats[3] * 100.0D);

		format_stats[4] = currency.format(stats[4]);

		request.setAttribute(attr, format_stats);
	}

	void formatRateAndAddToRequest(String attr, double[] stats,
			HttpServletRequest request) {
		String[] format_stats = new String[stats.length];

		NumberFormat currency = new DecimalFormat("#,###");
		NumberFormat decimal_format = new DecimalFormat("#0.0");
		if (stats[0] == 0.0D) {
			format_stats[0] = "";
		} else {
			format_stats[0] = decimal_format.format(stats[0]);
		}
		format_stats[1] = decimal_format.format(stats[1]);
		format_stats[2] = decimal_format.format(stats[2]);
		format_stats[3] = decimal_format.format(stats[3]);

		format_stats[4] = currency.format(stats[4]);

		request.setAttribute(attr, format_stats);
	}

	double[] computeMatrixRowStatistics(double[] data_set, int selected) {
		double[] stats = new double[5];

		double hi = 0.0D;
		double lo = 99999999.0D;
		double sum = 0.0D;
		double avg = 0.0D;
		double selected_value = 0.0D;
		for (int i = 0; i < data_set.length - 1; i++) {
			if (i != selected) {
				sum += data_set[i];
				if (hi < data_set[i]) {
					hi = data_set[i];
				}
				if (lo > data_set[i]) {
					lo = data_set[i];
				}
			} else {
				selected_value = data_set[i];
			}
		}
		if (selected != -1) {
			avg = sum / (data_set.length - 2);
		} else {
			avg = sum / (data_set.length - 1);
		}
		stats[0] = selected_value;
		stats[1] = avg;
		stats[2] = hi;
		stats[3] = lo;
		stats[4] = data_set[(data_set.length - 1)];

		return stats;
	}

	Vector<String[]> getRawDataView(String period, DataAccess da)
			throws ReportingException {
		Vector<String[]> raw_data = da
				.getVectorListForMultipleColumns(SQL_GetRawData(period));
		if ((raw_data == null) || (raw_data.size() < 1)) {
			throw new ReportingException(100,
					"No raw data available for this report");
		}
		return raw_data;
	}

	Map<String, Integer> mapSPM(Vector<String[]> raw_data) {
		Map<String, Integer> spm_index = new HashMap(7);

		int i_spm = 0;
		for (int i = 0; i < raw_data.size(); i++) {
			if (!spm_index.containsKey(((String[]) raw_data.get(i))[1])) {
				spm_index.put(((String[]) raw_data.get(i))[1],
						Integer.valueOf(i_spm));
				i_spm++;
			}
		}
		return spm_index;
	}

	Map<String, Integer> mapPOType() {
		Map<String, Integer> type_index = new HashMap(5);

		type_index.put("M", Integer.valueOf(0));
		type_index.put("S", Integer.valueOf(1));
		type_index.put("P", Integer.valueOf(2));
		type_index.put("T", Integer.valueOf(3));

		return type_index;
	}

	double[][] buildMatrix(Vector<String[]> raw_data,
			Map<String, Integer> spm_index, Map<String, Integer> type_index) {
		double[][] matrix = new double[type_index.size()][spm_index.size() + 1];
		int j = 0;
		int k = 0;
		double value = 0.0D;

		int matrix_rows = matrix.length - 1;
		int matrix_cols = matrix[0].length - 1;
		for (int i = 0; i < raw_data.size(); i++) {
			j = ((Integer) type_index.get(((String[]) raw_data.get(i))[0]))
					.intValue();
			k = ((Integer) spm_index.get(((String[]) raw_data.get(i))[1]))
					.intValue();
			value = Double.parseDouble(((String[]) raw_data.get(i))[2]);
			matrix[j][k] = value;

			matrix[j][matrix_cols] += value;
			matrix[matrix_rows][k] += value;
			matrix[matrix_rows][matrix_cols] += value;
		}
		for (int c = 0; c < matrix_cols; c++) {
			for (int r = 0; r < matrix_rows; r++) {
				matrix[r][c] /= matrix[matrix_rows][c];
			}
		}
		return matrix;
	}

	String SQL_GetRawData(String period) {
		String sql =

		"select ac_op_type, ac_os_spm, ac_op_count   from datawarehouse.dbo.ac_fact2_ops_summary_po  where ac_os_bdm = '%' and ac_os_spm != '%' and ac_os_day_index = "
				+

				period
				+ "       and ac_os_spm not in ('Supinger, Bob', 'Shroder, Charles','Courtney, Terezia','Carlton, Lance')";

		return sql;
	}
}
