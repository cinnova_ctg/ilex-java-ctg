package module;

public class ReportingException extends Exception {
	int errorIdentifier;
	String errorMessage;

	public ReportingException(int ei, String em) {
		setErrorIdentifier(ei);
		setErrorMessage(em);
	}

	int getErrorIdentifier() {
		return this.errorIdentifier;
	}

	void setErrorIdentifier(int ei) {
		this.errorIdentifier = ei;
	}

	String getErrorMessage() {
		return this.errorMessage;
	}

	void setErrorMessage(String em) {
		this.errorMessage = em;
	}
}
