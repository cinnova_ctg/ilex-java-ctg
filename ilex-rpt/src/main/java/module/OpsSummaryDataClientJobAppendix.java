package module;

import java.text.DecimalFormat;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.contingent.ilex.report.DataAccess;



public class OpsSummaryDataClientJobAppendix {
	
	public OpsSummaryDataClientJobAppendix(HttpServletRequest request, String period,
			DataAccess da, String selected){
		
		Vector<String[]> raw_data = null;
		
		String query = "select lo_pc_last_name+', '+lo_pc_first_name AS ac_du_name, "
				+ " count(DISTINCT ac_jd_pd_pr_id) project, count(DISTINCT ac_jd_js_id) job, count(DISTINCT lo_ot_name) customer "
				+ " from datawarehouse.dbo.ac_dimensions_job "
				+ " join datawarehouse.dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id "
				+ " join datawarehouse.dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id "
				+ " join datawarehouse.dbo.ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id "
				+ " join ilex.dbo.lm_appendix_poc on lm_ap_pr_id = ac_jd_pd_pr_id "
				+ " join ilex.dbo.lo_poc on lm_ap_pc_id = lo_pc_id "
				+ " join ilex.dbo.lx_appendix_main on lm_ap_pr_id = lx_pr_id "
				+ " join ilex.dbo.lo_organization_top on lx_pr_mm_id = lo_ot_id "
				+ " where ac_jd_date_closed > dateadd(dd, -"+period+", getdate()) "
				+ " and ac_du_name not in ('Shroder, Charles','Supinger, Bob', 'Epling, Dave', 'Courtney, Terezia', '%') "
				+ " GROUP BY  (lo_pc_last_name+', '+lo_pc_first_name) ";
		
		raw_data = da.getClientJobAppendixList(query);
		computeMatrixRowStatistics(raw_data, selected, request);
		
		
		query = "SELECT lo_pc_last_name+', '+lo_pc_first_name AS ac_du_name,  count(distinct pr_ja_actionee) count "
				+ " from ilex.dbo.pr_job_actions_minute_rollup "
				+ " JOIN datawarehouse.dbo.ac_dimensions_job ON  ac_jd_js_id  = pr_ja_lm_id  "
				+ " join datawarehouse.dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id "
				+ " join datawarehouse.dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id "
				+ " join datawarehouse.dbo.ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id "
				+ " join ilex.dbo.lm_appendix_poc on lm_ap_pr_id = ac_jd_pd_pr_id "
				+ " join ilex.dbo.lo_poc on lm_ap_pc_id = lo_pc_id "
				+ " where ac_jd_date_closed > dateadd(dd, -"+period+", getdate())  "
				+ " and ac_du_name not in ('Shroder, Charles','Supinger, Bob', 'Epling, Dave', 'Courtney, Terezia', '%') "
				+ " and (datepart(hh,pr_ja_date_time_stamp) between 07 and 19) "
				+ " GROUP BY  (lo_pc_last_name+', '+lo_pc_first_name) ";
		
		raw_data = da.getPPSNonPPSData(query);
		computeMatrixRowStatisticsForPPSNonPPS(raw_data, selected, "UserPPS", request);
		
		query = "SELECT lo_pc_last_name+', '+lo_pc_first_name AS ac_du_name,  count(distinct pr_ja_actionee) count "
				+ " from ilex.dbo.pr_job_actions_minute_rollup "
				+ " JOIN datawarehouse.dbo.ac_dimensions_job ON  ac_jd_js_id  = pr_ja_lm_id  "
				+ " join datawarehouse.dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id "
				+ " join datawarehouse.dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id "
				+ " join datawarehouse.dbo.ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id "
				+ " join ilex.dbo.lm_appendix_poc on lm_ap_pr_id = ac_jd_pd_pr_id "
				+ " join ilex.dbo.lo_poc on lm_ap_pc_id = lo_pc_id "
				+ " where ac_jd_date_closed > dateadd(dd, -"+period+", getdate())  "
				+ " and ac_du_name not in ('Shroder, Charles','Supinger, Bob', 'Epling, Dave', 'Courtney, Terezia', '%') "
				+ " and (datepart(hh,pr_ja_date_time_stamp) between 00 and 06 or datepart(hh,pr_ja_date_time_stamp) between 20 and 23) "
				+ " GROUP BY  (lo_pc_last_name+', '+lo_pc_first_name) ";
		
		raw_data = da.getPPSNonPPSData(query);
		computeMatrixRowStatisticsForPPSNonPPS(raw_data, selected, "UserNonPPS", request);
		
		query = "SELECT lo_pc_last_name+', '+lo_pc_first_name AS ac_du_name,  count(pr_ja_action) count "
				+ " from ilex.dbo.pr_job_actions_minute_rollup "
				+ " JOIN datawarehouse.dbo.ac_dimensions_job ON  ac_jd_js_id  = pr_ja_lm_id  "
				+ " join datawarehouse.dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id "
				+ " join datawarehouse.dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id "
				+ " join datawarehouse.dbo.ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id "
				+ " join ilex.dbo.lm_appendix_poc on lm_ap_pr_id = ac_jd_pd_pr_id "
				+ " join ilex.dbo.lo_poc on lm_ap_pc_id = lo_pc_id "
				+ " where ac_jd_date_closed > dateadd(dd, -"+period+", getdate())  "
				+ " and ac_du_name not in ('Shroder, Charles','Supinger, Bob', 'Epling, Dave', 'Courtney, Terezia', '%') "
				+ " and (datepart(hh,pr_ja_date_time_stamp) between 07 and 19) "
				+ " and pr_ja_action != 15 "
				+ " GROUP BY  (lo_pc_last_name+', '+lo_pc_first_name) ";
		
		
		raw_data = da.getPPSNonPPSData(query);
		computeMatrixRowStatisticsForPPSNonPPS(raw_data, selected, "UserActionPPS", request);
		
		
		query = "SELECT lo_pc_last_name+', '+lo_pc_first_name AS ac_du_name,  count(pr_ja_action) count "
				+ " from ilex.dbo.pr_job_actions_minute_rollup "
				+ " JOIN datawarehouse.dbo.ac_dimensions_job ON  ac_jd_js_id  = pr_ja_lm_id  "
				+ " join datawarehouse.dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id "
				+ " join datawarehouse.dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id "
				+ " join datawarehouse.dbo.ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id "
				+ " join ilex.dbo.lm_appendix_poc on lm_ap_pr_id = ac_jd_pd_pr_id "
				+ " join ilex.dbo.lo_poc on lm_ap_pc_id = lo_pc_id "
				+ " where ac_jd_date_closed > dateadd(dd, -"+period+", getdate())  "
				+ " and ac_du_name not in ('Shroder, Charles','Supinger, Bob', 'Epling, Dave', 'Courtney, Terezia', '%') "
				+ " and (datepart(hh,pr_ja_date_time_stamp) between 00 and 06 or datepart(hh,pr_ja_date_time_stamp) between 20 and 23) "
				+ " and pr_ja_action != 15 "
				+ " GROUP BY  (lo_pc_last_name+', '+lo_pc_first_name) ";
		
		
		raw_data = da.getPPSNonPPSData(query);
		computeMatrixRowStatisticsForPPSNonPPS(raw_data, selected, "UserActionNonPPS", request);
		
		

	}
	
	void computeMatrixRowStatisticsForPPSNonPPS(Vector<String[]> data_set, String selected, String attrib, HttpServletRequest request) {
		double[] stats = new double[5];
		

		double hi = 0.0D;
		double lo = 99999999.0D;
		double sum = 0.0D;
		double avg = 0.0D;
		double selected_value = 0.0D;
		
		
		
		for (int i = 0; i < data_set.size(); i++) {
			String[] data = data_set.get(i);
			
			if (!data[0].equals(selected)) {

				sum += Double.parseDouble(data[1]);
								
				if (hi < Double.parseDouble(data[1])) {
					hi = Double.parseDouble(data[1]);
				}
				if (lo > Double.parseDouble(data[1])) {
					lo = Double.parseDouble(data[1]);
				}
				
			} else {
				
				selected_value += Double.parseDouble(data[1]);
			}
		}
		
		DecimalFormat df = new DecimalFormat("#.00");		 
		if (selected.equals("%") && data_set.size() >1) {
			avg = sum / (data_set.size() - 1); 
			
		} else {
			avg = sum / (data_set.size()==0?1:data_set.size());
		}
		
		stats[0] = Double.parseDouble(df.format(selected_value));
		stats[1] = Double.parseDouble(df.format(avg));
		stats[2] = Double.parseDouble(df.format(hi));
		stats[3] = Double.parseDouble(df.format(lo));
		stats[4] = sum;		
		
		request.setAttribute(attrib, stats);
		

		
	}
	
	
	void computeMatrixRowStatistics(Vector<String[]> data_set, String selected, HttpServletRequest request) {
		double[] projectStats = new double[5];
		double[] jobStats = new double[5];
		double[] customerStats = new double[5];

		double hi_proj = 0.0D;
		double lo_proj = 99999999.0D;
		double sum_proj = 0.0D;
		double avg_proj = 0.0D;
		double selected_value_proj = 0.0D;
		
		double hi_job = 0.0D;
		double lo_job = 99999999.0D;
		double sum_job = 0.0D;
		double avg_job = 0.0D;
		double selected_value_job = 0.0D;
		
		double hi_cust = 0.0D;
		double lo_cust = 99999999.0D;
		double sum_cust = 0.0D;
		double avg_cust = 0.0D;
		double selected_value_cust = 0.0D;
		
		
		for (int i = 0; i < data_set.size(); i++) {
			String[] data = data_set.get(i);
			
			if (!data[0].equals(selected)) {

				sum_proj += Double.parseDouble(data[1]);
				sum_job += Double.parseDouble(data[2]);
				sum_cust += Double.parseDouble(data[3]);
				
				if (hi_proj < Double.parseDouble(data[1])) {
					hi_proj = Double.parseDouble(data[1]);
				}
				if (lo_proj > Double.parseDouble(data[1])) {
					lo_proj = Double.parseDouble(data[1]);
				}
				
				if (hi_job < Double.parseDouble(data[2])) {
					hi_job =Double.parseDouble(data[2]);
				}
				if (lo_job > Double.parseDouble(data[2])) {
					lo_job = Double.parseDouble(data[2]);
				}
				
				if (hi_cust < Double.parseDouble(data[3])) {
					hi_cust = Double.parseDouble(data[3]);
				}
				if (lo_cust > Double.parseDouble(data[3])) {
					lo_cust = Double.parseDouble(data[3]);
				}
			} else {
				
				selected_value_proj += Double.parseDouble(data[1]);
				selected_value_job += Double.parseDouble(data[2]);
				selected_value_cust += Double.parseDouble(data[3]);
			}
		}
		
		if (selected.equals("%")) {
			avg_cust = sum_cust / (data_set.size() - 1);
			avg_job = sum_job / (data_set.size() - 1);
			avg_proj = sum_proj / (data_set.size() - 1);
			
		} else {
			avg_cust = sum_cust / (data_set.size()==0?1:data_set.size());
			avg_job = sum_job / (data_set.size()==0?1:data_set.size());
			avg_proj = sum_proj / (data_set.size()==0?1:data_set.size());
		}
		DecimalFormat df = new DecimalFormat("#.00");
		projectStats[0] = Double.parseDouble(df.format(selected_value_proj));
		projectStats[1] = Double.parseDouble(df.format(avg_proj));
		projectStats[2] = Double.parseDouble(df.format(hi_proj));
		projectStats[3] = Double.parseDouble(df.format(lo_proj));
		projectStats[4] = sum_proj;		
		request.setAttribute("projectData", projectStats);
		
		jobStats[0] = Double.parseDouble(df.format(selected_value_job));
		jobStats[1] = Double.parseDouble(df.format(avg_job));
		jobStats[2] = Double.parseDouble(df.format(hi_job));
		jobStats[3] = Double.parseDouble(df.format(lo_job));
		jobStats[4] = sum_job;		
		request.setAttribute("jobData", jobStats);
		
		customerStats[0] = Double.parseDouble(df.format(selected_value_cust));
		customerStats[1] = Double.parseDouble(df.format(avg_cust));
		customerStats[2] = Double.parseDouble(df.format(hi_cust));
		customerStats[3] = Double.parseDouble(df.format(lo_cust));
		customerStats[4] = sum_cust;
		request.setAttribute("customerData", customerStats);
		

		
	}

}
