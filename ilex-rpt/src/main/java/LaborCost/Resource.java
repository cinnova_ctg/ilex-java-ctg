package LaborCost;

import java.sql.ResultSet;

import com.contingent.ilex.report.DataAccess;

public class Resource {
	public static int LM_RS_ID = 1;
	static int LM_RS_AT_ID = 2;
	static int LM_RS_RP_ID = 3;
	static int LM_RS_IV_CNS_PART_NUMBER = 4;
	static int LM_RS_LIB_ID = 5;
	static int LM_RS_STATUS = 6;
	static int LM_RS_QUANTITY = 7;
	static int LM_RS_TYPE = 8;
	static int LM_RS_PRIORITY = 9;
	static int LM_RS_MARGIN = 10;
	static int LM_RS_CE_COST = 11;
	static int LM_RS_CREATED_BY = 12;
	static int LM_RS_CREATE_DATE = 13;
	static int LM_RS_SW_SOW = 14;
	static int LM_RS_OUT_OF_SCOPE = 15;
	static int LM_RS_AS_ASSUMPTIONS = 16;
	static int LM_RS_CHANGED_DATE = 17;
	static int LM_RS_CHANGED_BY = 18;
	static int LM_RS_TRANSIT = 19;
	static int LM_RS_CLR_DETAIL_ID = 20;
	static int LM_RS_TROUBLE_AND_REPAIR_FLAG = 21;
	String[] lm_resource = new String[LM_RS_TROUBLE_AND_REPAIR_FLAG + 1];
	int IV_RP_ID = 1;
	int IV_RP_SC_ID = 2;
	int IV_RP_SC_NUM = 3;
	int IV_RP_STATUS = 4;
	static int IV_RP_NAME = 5;
	int IV_RP_ALT_IDENTIFIER = 6;
	int IV_RP_CNS_PART_NUMBER = 7;
	int IV_RP_MFG_PART_NUMBER = 8;
	int IV_RP_SELL_QUANTITY = 9;
	int IV_RP_MINIMUM_QUANTITY = 10;
	int IV_RP_QUANTITY_FRACTION = 11;
	int IV_RP_SELL_QUANTITY_WEIGHT_IN_LBS = 12;
	int IV_RP_UNITS = 13;
	int IV_COST = 14;
	int IV_RP_STATUS_DATE = 15;
	int IV_RP_CREATED_BY = 16;
	int IV_RP_CREATE_DATE = 17;
	int IV_RP_CHANGED_BY = 18;
	int IV_RP_CHANGE_DATE = 19;
	public String[] iv_resource_pool = new String[this.IV_RP_CHANGE_DATE + 1];
	int LM_PWO_ID = 1;
	public static int LM_PWO_PO_ID = 2;
	int LM_PWO_RS_ID = 3;
	static int LM_PWO_AUTHORISED_UNIT_COST = 4;
	static int LM_PWO_AUTHORISED_TOTAL_COST = 5;
	int LM_PWO_SHOW_ON_WO = 6;
	int LM_PWO_DROP_SHIPPED = 7;
	static int LM_PWO_AUTHORISED_QTY = 8;
	int LM_PWO_PVS_SUPPLIED = 9;
	int LM_PWO_CREDIT_CARD = 10;
	int LM_PWO_RS_REVISION = 11;
	String[] lm_purchase_work_order = new String[this.LM_PWO_RS_REVISION + 1];
	int LM_PO_ID = 1;
	int LM_PO_NUMBER = 2;
	int LM_PO_JS_ID = 3;
	int LM_PO_PARTNER_ID = 4;
	int LM_PO_TYPE = 5;
	int LM_PO_PWO_TYPE_ID = 6;
	int LM_PO_BULK = 7;
	int LM_PO_ISSUE_DATE = 8;
	int LM_PO_DELIVER_BY_DATE = 9;
	int LM_PO_TERMS = 10;
	int LM_PO_SHIP_TO_ADDRESS = 11;
	int LM_PO_SPECIAL_CONDITIONS = 12;
	int LM_PO_SPECIAL_INSTRUCTIONS = 13;
	int LM_PO_AUTHORISED_TOTAL = 14;
	int LM_PO_CREATED_BY = 15;
	int LM_PO_CREATE_DATE = 16;
	int LM_PO_CHANGED_BY = 17;
	int LM_PO_CHANGE_DATE = 18;
	int LM_PO_PARTNER_POC_ID = 19;
	int LM_PO_TC_ID = 20;
	int LM_PO_DELIVERABLES = 21;
	int LM_PO_PAYMENT = 22;
	int LM_PO_ESTIMATED_NONCONTRACT_TOTAL = 23;
	int LM_PO_PAYMENT_DATE = 24;
	int LM_PO_DOC_ID = 25;
	int LM_PO_WO_DOC_ID = 26;
	int LM_PO_FIRST_SENT_BY = 27;
	int LM_PO_FIRST_SENT_DATE = 28;
	int LM_PO_LAST_SENT_BY = 29;
	int LM_PO_LAST_SENT_DATE = 30;
	int REVISION = 31;
	int PO_STATUS = 32;
	int PREVIOUS_STATUS = 33;
	int CANCELLED_PO_NUMBER = 34;
	int MPO_ID = 35;
	int LM_PO_DEFAULT_TICKET = 36;
	int LM_PO_COMPLETE_DATE = 37;
	int LM_PO_BEST_OFFSITE_DATE = 38;
	int LM_PO_COMMISSION_FLAG = 39;
	int LM_PO_JOB_TRAVEL_DISTANCE = 40;
	int LM_PO_JOB_TRAVEL_DURATION = 41;
	int LM_PO_MINUTEMAN_COST_OVERRIDE = 42;
	String[] lm_purchase_order = new String[this.LM_PO_MINUTEMAN_COST_OVERRIDE + 1];
	int LM_PI_ID = 1;
	int LM_PI_PO_ID = 2;
	int LM_PI_STATUS = 3;
	int LM_PI_SUBMIT_TYPE = 4;
	int LM_PI_INVOICE_NUMBER = 5;
	int LM_PI_INVOICE_DATE = 6;
	int LM_PI_DATE_RECEIVED = 7;
	int LM_PI_SUBMITTER = 8;
	int LM_PI_LABOR_AMOUNT = 9;
	int LM_PI_TRAVEL_AMOUNT = 10;
	int LM_PI_FRIEGHT_AMOUNT = 11;
	int LM_PI_MATERIALS_AMOUNT = 12;
	int LM_PI_TAX_AMOUNT = 13;
	int LM_PI_TOTAL = 14;
	int LM_PI_PARTNER_COMMENTS = 15;
	int LM_PI_RECONCILE_COMMENTS = 16;
	int LM_PI_APPROVED_TOTAL = 17;
	int LM_PI_ADJUSTMENT_TO_INVOICE = 18;
	int LM_PI_APPROVED_BY = 19;
	int LM_PI_APPROVED_DATE = 20;
	int LM_PI_GP_PAID_DATE = 21;
	int LM_PI_GP_CHECK_NUMBER = 22;
	int LM_PI_INTERNAL_COMMENTS = 23;
	int LM_PI_LAST_UPDATED_BY = 24;
	int LM_PI_LAST_UPDATE = 25;
	int LM_PI_EINVOICE_TOUCHED = 26;
	int LM_PI_MINUTEMAN_STATUS = 27;
	int LM_PI_AUTO_APPROVED = 28;
	int LM_PI_PREVIOUS_PO_NUMBER = 29;
	int LM_PI_PARTNER_ID = 30;
	int LM_PI_GP_SYNC = 31;
	int LM_PI_PAYMENT_SCHEDULED = 32;
	String[] lm_partner_invoice = new String[this.LM_PI_PAYMENT_SCHEDULED + 1];
	int LX_CE_ID = 1;
	int LX_CE_USED_IN = 2;
	int LX_CE_TYPE = 3;
	int LX_CE_DATE = 4;
	static int LX_CE_QUANTITY = 5;
	int LX_CE_MARGIN = 6;
	int LX_CE_INVOICE_PRICE = 7;
	int LX_CE_LIST_PRICE = 8;
	static int LX_CE_TOTAL_PRICE = 9;
	static int LX_CE_UNIT_PRICE = 10;
	static int LX_CE_TOTAL_COST = 11;
	static int LX_CE_UNIT_COST = 12;
	int LX_CE_OVERHEAD_COST = 13;
	int LX_CE_LOCALITY_UPLIFT = 14;
	int LX_CE_UNION_UPLIFT = 15;
	int LX_CE_NON_PPS_COST = 16;
	int LX_CE_ADDENDUM_COST = 17;
	int LX_CE_ADDENDUM_PRICE = 18;
	int LX_CE_CHANGE_ORDER_COST = 19;
	int LX_CE_CHANGE_ORDER_PRICE = 20;
	int LX_CE_FRIEGHT_COST = 21;
	int LX_CE_TAXES = 22;
	int LX_CE_TAX_RATE = 23;
	int LX_CE_CNS_HQ_LABOR_COST = 24;
	int LX_CE_CNS_FIELD_LABOR_COST = 25;
	int LX_CE_CTR_FIELD_LABOR_COST = 26;
	int LX_CE_MATERIAL_COST = 27;
	int LX_CE_TRAVEL_COST = 28;
	int LX_CE_SUPPLIER_ESTIMATED_COST = 29;
	int LX_CE_SUPPLIER_ESTIMATED_COST_DATE = 30;
	int LX_CE_SUPPLIER_INVOICE_COST = 31;
	int LX_CE_SUPPLIER_INVOICE_DATE = 32;
	int LX_CE_MOVED_TO_STAGING = 33;
	int LX_CE_MOVED_TO_STAGING_DATE = 34;
	int LX_CE_COMPLETE = 35;
	String[] lx_cost_element = new String[this.LX_CE_COMPLETE + 1];
	public boolean ResourceToPOMapped = false;
	static DataAccess da;

	Resource(DataAccess DA, String lm_rs_id) {
		da = DA;

		getResourceData(lm_rs_id);
	}

	void getResourceData(String lm_rs_id) {
		getResourceMain(lm_rs_id);
		getResourcePool(this.lm_resource[LM_RS_RP_ID]);
		getPODetail(lm_rs_id);
		getCostElement(lm_rs_id, this.lm_resource[LM_RS_TYPE]);
	}

	void getResourceMain(String lm_rs_id) {
		String[] columns = { "", "lm_rs_id", "lm_rs_at_id", "lm_rs_rp_id",
				"lm_rs_iv_cns_part_number", "lm_rs_lib_id", "lm_rs_status",
				"lm_rs_quantity", "lm_rs_type", "lm_rs_priority",
				"lm_rs_margin", "lm_rs_ce_cost", "lm_rs_created_by",
				"lm_rs_create_date", "lm_rs_sw_sow", "lm_rs_out_of_scope",
				"lm_rs_as_assumptions", "lm_rs_changed_date",
				"lm_rs_changed_by", "lm_rs_transit", "lm_rs_clr_detail_id",
				"lm_rs_trouble_and_repair_flag" };

		String sql = "select * from lm_resource where lm_rs_id = " + lm_rs_id;

		ResultSet rs = da.getResultSet(sql);
		try {
			if (rs.next()) {
				for (int i = 1; i < columns.length; i++) {
					this.lm_resource[i] = rs.getString(columns[i]);
				}
			}
			rs.close();
		} catch (Exception e) {
			System.out.println("Resource.getResourceMain " + e);
		}
	}

	void getResourcePool(String iv_rp_id) {
		String[] columns = { "", "iv_rp_id", "iv_rp_sc_id", "iv_rp_sc_num",
				"iv_rp_status", "iv_rp_name", "iv_rp_alt_identifier",
				"iv_rp_cns_part_number", "iv_rp_mfg_part_number",
				"iv_rp_sell_quantity", "iv_rp_minimum_quantity",
				"iv_rp_quantity_fraction", "iv_rp_sell_quantity_weight_in_lbs",
				"iv_rp_units", "iv_cost", "iv_rp_status_date",
				"iv_rp_created_by", "iv_rp_create_date", "iv_rp_changed_by",
				"iv_rp_change_date" };

		String sql = "select * from iv_resource_pool where iv_rp_id = "
				+ iv_rp_id;

		ResultSet rs = da.getResultSet(sql);
		try {
			if (rs.next()) {
				for (int i = 1; i < columns.length; i++) {
					this.iv_resource_pool[i] = rs.getString(columns[i]);
				}
			}
			rs.close();
		} catch (Exception e) {
			System.out.println("Resource.getResourcePool " + e);
		}
	}

	void getPODetail(String lm_rs_id) {
		String[] columns = { "", "lm_pwo_id", "lm_pwo_po_id", "lm_pwo_rs_id",
				"lm_pwo_authorised_unit_cost", "lm_pwo_authorised_total_cost",
				"lm_pwo_show_on_wo", "lm_pwo_drop_shipped",
				"lm_pwo_authorised_qty", "lm_pwo_pvs_supplied",
				"lm_pwo_credit_card", "lm_pwo_rs_revision" };

		String[] columns2 = { "", "lm_po_id", "lm_po_number", "lm_po_js_id",
				"lm_po_partner_id", "lm_po_type", "lm_po_pwo_type_id",
				"lm_po_bulk", "lm_po_issue_date", "lm_po_deliver_by_date",
				"lm_po_terms", "lm_po_ship_to_address",
				"lm_po_special_conditions", "lm_po_special_instructions",
				"lm_po_authorised_total", "lm_po_created_by",
				"lm_po_create_date", "lm_po_changed_by", "lm_po_change_date",
				"lm_po_partner_poc_id", "lm_po_tc_id", "lm_po_deliverables",
				"lm_po_payment", "lm_po_estimated_noncontract_total",
				"lm_po_payment_date", "lm_po_doc_id", "lm_po_wo_doc_id",
				"lm_po_first_sent_by", "lm_po_first_sent_date",
				"lm_po_last_sent_by", "lm_po_last_sent_date", "revision",
				"po_status", "previous_status", "cancelled_po_number",
				"mpo_id", "lm_po_default_ticket", "lm_po_complete_date",
				"lm_po_best_offsite_date", "lm_po_commission_flag",
				"lm_po_job_travel_distance", "lm_po_job_travel_duration",
				"lm_po_minuteman_cost_override" };

		String[] columns3 = { "", "lm_pi_id", "lm_pi_po_id", "lm_pi_status",
				"lm_pi_submit_type", "lm_pi_invoice_number",
				"lm_pi_invoice_date", "lm_pi_date_received", "lm_pi_submitter",
				"lm_pi_labor_amount", "lm_pi_travel_amount",
				"lm_pi_frieght_amount", "lm_pi_materials_amount",
				"lm_pi_tax_amount", "lm_pi_total", "lm_pi_partner_comments",
				"lm_pi_reconcile_comments", "lm_pi_approved_total",
				"lm_pi_adjustment_to_invoice", "lm_pi_approved_by",
				"lm_pi_approved_date", "lm_pi_gp_paid_date",
				"lm_pi_gp_check_number", "lm_pi_internal_comments",
				"lm_pi_last_updated_by", "lm_pi_last_update",
				"lm_pi_eInvoice_touched", "lm_pi_minuteman_status",
				"lm_pi_auto_approved", "lm_pi_previous_po_number",
				"lm_pi_partner_id", "lm_pi_gp_sync", "lm_pi_payment_scheduled" };

		String sql = "select * from lm_purchase_work_order    join lm_purchase_order on lm_pwo_po_id = lm_po_id    left join lm_partner_invoice on lm_po_id = lm_pi_po_id  where po_status = 3 and lm_pwo_rs_id = "
				+

				lm_rs_id;

		ResultSet rs = da.getResultSet(sql);
		try {
			if (rs.next()) {
				for (int i = 1; i < columns.length; i++) {
					this.lm_purchase_work_order[i] = rs.getString(columns[i]);
				}
				for (int i = 1; i < columns2.length; i++) {
					this.lm_purchase_order[i] = rs.getString(columns2[i]);
				}
				for (int i = 1; i < columns3.length; i++) {
					this.lm_partner_invoice[i] = rs.getString(columns3[i]);
				}
				this.ResourceToPOMapped = true;
			}
			rs.close();
		} catch (Exception e) {
			System.out.println("Resource.getPODetail " + e);
		}
	}

	void getCostElement(String lm_rs_id, String type) {
		String[] columns = { "", "lx_ce_id", "lx_ce_used_in", "lx_ce_type",
				"lx_ce_date", "lx_ce_quantity", "lx_ce_margin",
				"lx_ce_invoice_price", "lx_ce_list_price", "lx_ce_total_price",
				"lx_ce_unit_price", "lx_ce_total_cost", "lx_ce_unit_cost",
				"lx_ce_overhead_cost", "lx_ce_locality_uplift",
				"lx_ce_union_uplift", "lx_ce_non_pps_cost",
				"lx_ce_addendum_cost", "lx_ce_addendum_price",
				"lx_ce_change_order_cost", "lx_ce_change_order_price",
				"lx_ce_frieght_cost", "lx_ce_taxes", "lx_ce_tax_rate",
				"lx_ce_cns_hq_labor_cost", "lx_ce_cns_field_labor_cost",
				"lx_ce_ctr_field_labor_cost", "lx_ce_material_cost",
				"lx_ce_travel_cost", "lx_ce_supplier_estimated_cost",
				"lx_ce_supplier_estimated_cost_date",
				"lx_ce_supplier_invoice_cost", "lx_ce_supplier_invoice_date",
				"lx_ce_moved_to_staging", "lx_ce_moved_to_staging_date",
				"lx_ce_complete" };

		String sql = "select * from lx_cost_element where lx_ce_used_in = "
				+ lm_rs_id + " and lx_ce_type = '" + type + "'";

		ResultSet rs = da.getResultSet(sql);
		try {
			if (rs.next()) {
				for (int i = 1; i < columns.length; i++) {
					this.lx_cost_element[i] = rs.getString(columns[i]);
				}
			}
			rs.close();
		} catch (Exception e) {
			System.out.println("Resource.getCostElement " + e);
		}
	}
}
