package LaborCost;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.contingent.ilex.report.DataAccess;

public class LaborMetrics2 {
	static final int BILLABLE = 1;
	static final int NONBILLABLE = 2;
	static final int TOTALPRICE = 3;
	static final int AVGLABORPRICE = 4;
	static final int POLABOR = 5;
	static final int POLABORWGT = 6;
	static final int POLABORTOTALCOST = 7;
	static final int POLABORAVGCOST = 8;
	Map<String, Map<String, Double[]>> LaborCostByMonth = new HashMap();
	DataAccess da = new DataAccess();

	public LaborMetrics2() {
		String start_point = null;

		Vector<String> jobs = getListOfJob(start_point);

		buildJobPODetails(jobs);
	}

	Vector<String> getListOfJob(String start_point) {
		Vector<String> jobs = new Vector();
		String job = "";
		if (start_point != null) {
			start_point = "getdate()";
		} else {
			start_point = "dateadd(mm, -2, getdate())";
		}
		String sql = "select  ac_od_jd_js_id, ac_od_po_id, ac_od_type   from datawarehouse.dbo.ac_dimensions_purchase_order join datawarehouse.dbo.ac_dimensions_job on ac_od_jd_js_id = ac_jd_js_id where ac_od_jd_js_id = 9 and ac_od_date_complete between '2/1/2012' and '3/31/2012'";

		sql = "select ac_jd_js_id   from datawarehouse.dbo.ac_dimensions_job  where ac_jd_date_closed > "
				+

				start_point;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				job = rs.getString(1);
				jobs.add(job);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return jobs;
	}

	void buildJobPODetails(Vector<String> jobs) {
		for (int i = 0; i < jobs.size(); i++) {
			Job2 localJob2 = new Job2(this.da, (String) jobs.get(i), 1);
		}
	}

	public static void main(String[] args) {
		SimpleDateFormat sd = new SimpleDateFormat("MM/dd/yyyy");

		String send_email = "No";
		String end_date = "";
		String[] get_date = { "", "" };
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("send_email")) {
				send_email = "Yes";
			}
			if (args[i].startsWith("end_date=")) {
				end_date = "Yes";
				get_date = args[i].split("=");
			}
		}
		try {
			if (end_date.equals("Yes")) {
				GregorianCalendar ed = new GregorianCalendar();
				ed.setTime(sd.parse(get_date[1]));
			} else {
				GregorianCalendar ed = new GregorianCalendar();
				ed.set(ed.get(1), ed.get(2), ed.get(5));
			}
			new LaborMetrics2();
		} catch (Exception e) {
			System.out.println("Error initializing date");
		}
	}
}
