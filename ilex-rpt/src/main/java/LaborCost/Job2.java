package LaborCost;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.contingent.ilex.report.DataAccess;

public class Job2 {
	static int LM_AT_ID = 1;
	static int LM_AT_TITLE = 2;
	static int LM_AT_QUANTITY = 3;
	static int LM_AT_TYPE = 4;
	static int LM_AT_OOS_FLAG = 5;
	static int LX_CE_UNIT_PRICE = 6;
	static int LX_CE_TOTAL_PRICE = 7;
	static int LM_AT_AFTERHOURS_UPLIFT_FACTOR = 8;
	static int COLUMN_CNT = 9;
	public String Job;
	public String JobTitle;
	public String InvoiceDate;
	public String ZipCode;
	public String International;
	public String LocationDetails;
	public String CustomerReference;
	public String SiteNumber;
	public String Criticality;
	public String NonPPS;
	String Project;
	String ProjectTitle;
	String ProjectType;
	public String Customer;
	public String JobOwner;
	public String PO;
	public Double ProFormaCost;
	public Double ActualCost;
	public Double Revenue;
	public Double VGPM;
	public Double PGPM;
	Vector<String[]> lm_activity = new Vector();
	Map<String, Map<String, String>> Activity = new HashMap();
	Vector<String> pf_resource_list = new Vector();
	Vector<Resource> pf_resource = new Vector();
	Map<String, Resource> pf_resource2 = new HashMap();
	Vector<String> PurchaseOrderList = new Vector();
	Map<String, Map<String, String>> PurchaseOrderDetails = new HashMap();
	Map<String, String> ResourceToPurchaseOrders = new HashMap();
	Map<String, Vector<String>> PurchaseOrderToResources = new HashMap();
	Double LaborQtyPF = Double.valueOf(0.0D);
	Double LaborQtySell = Double.valueOf(0.0D);
	Double LaborQtyBuy = Double.valueOf(0.0D);
	Double LaborAmtPF = Double.valueOf(0.0D);
	Double LaborAmtSell = Double.valueOf(0.0D);
	Double LaborAmtBuy = Double.valueOf(0.0D);
	public Map<String, Map<String, String>> POTableMap = new HashMap();
	DataAccess da;
	DecimalFormat sd = new DecimalFormat("#,##0.00");
	DecimalFormat sd2 = new DecimalFormat("#,##0");
	SimpleDateFormat df1 = new SimpleDateFormat("MM/dd/yyyy");

	public Job2(DataAccess DA, String job, int src) {
		this.da = DA;
		this.Job = job;

		getJobDetails();

		getActivities(job);

		processActivityResources();

		getPODetails();

		evaluatePurchaseOrders(src);
	}

	void getJobDetails() {
		String sql = "select * from lm_job   join dbo.ap_appendix_list_01 on lm_js_pr_id = lx_pr_id   join lm_site_detail on lm_js_si_id = lm_si_id   join lx_cost_element on lm_js_id = lx_ce_used_in and lx_ce_type = 'IJ'   join lo_poc on lm_js_created_by = lo_pc_id   left join datawarehouse.dbo.ac_fact_job_rollup on lm_js_id = ac_ru_js_id   left join lm_ticket on lm_js_id = lm_tc_js_id   left join dbo.iv_criticality on lm_tc_crit_cat_id = iv_crit_id where lm_js_id = "
				+

				this.Job;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				this.Job = rs.getString("lm_js_id");
				this.JobTitle = (rs.getString("lx_pr_title") + " / " + rs
						.getString("lm_js_title"));
				this.InvoiceDate = rs.getString("lm_js_offsite_date");
				if (rs.wasNull()) {
					this.InvoiceDate = "Not Avaiable";
				}
				this.ZipCode = rs.getString("lm_si_zip_code");
				this.International = rs.getString("lm_si_country");
				if (!this.International.equals("US")) {
					this.International = ("Yes("
							+ rs.getString("lm_si_country") + ")");
				} else {
					this.International = "No";
				}
				this.CustomerReference = rs
						.getString("lm_js_customer_reference");

				this.SiteNumber = rs.getString("lm_si_number");
				this.LocationDetails = (rs.getString("lm_si_address") + ", "
						+ rs.getString("lm_si_city") + ", " + rs
						.getString("lm_si_state"));

				this.Project = rs.getString("lm_js_pr_id");
				this.ProjectTitle = rs.getString("lx_pr_title");
				this.ProjectType = rs.getString("lx_pr_type");
				this.Customer = rs.getString("lo_ot_name");
				this.JobOwner = rs.getString("lo_pc_last_name");

				this.Criticality = rs.getString("iv_crit_name");
				if (rs.wasNull()) {
					this.Criticality = "N/A";
				}
				this.ProFormaCost = Double.valueOf(rs
						.getDouble("ac_ru_pro_forma"));
				this.ActualCost = Double.valueOf(rs.getDouble("ac_ru_expense"));
				this.Revenue = Double.valueOf(rs.getDouble("ac_ru_revenue"));
				if (this.Revenue.doubleValue() > 0.0D) {
					this.VGPM = Double.valueOf(1.0D
							- this.ActualCost.doubleValue()
							/ this.Revenue.doubleValue());
					this.PGPM = Double.valueOf(1.0D
							- this.ProFormaCost.doubleValue()
							/ this.Revenue.doubleValue());
				} else {
					this.VGPM = Double.valueOf(0.0D);
					this.PGPM = Double.valueOf(0.0D);
				}
			}
			rs.close();
		} catch (Exception e) {
			System.out.println("Job2.getJobDetails Exception " + e);
		}
	}

	Vector<String[]> getActivities(String lm_js_id) {
		String[] columns = { "", "lm_at_id", "lm_at_title", "lm_at_quantity",
				"lm_at_type", "lm_at_oos_flag", "lx_ce_unit_price",
				"lx_ce_total_price", "lm_at_afterhours_uplift_factor" };

		String sql = "select * from lm_activity join lx_cost_element on lm_at_id = lx_ce_used_in and lx_ce_type = 'IA' where lm_at_type != 'V' and lm_at_js_id = "
				+ lm_js_id + " order by lm_at_title";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] activities = new String[COLUMN_CNT];
				Map<String, String> activity = new HashMap();

				String t1 = "";
				for (int i = 1; i < columns.length; i++) {
					if (LM_AT_OOS_FLAG == i) {
						t1 = rs.getString(columns[i]);
						if (rs.wasNull()) {
							t1 = "0";
						}
						if (t1.equals("0")) {
							activities[i] = "";
							activity.put("lm_at_oos_flag", "No");
						} else {
							activities[i] = "Yes";
							activity.put("lm_at_oos_flag", "Yes");
						}
					} else if (LM_AT_TYPE == i) {
						t1 = rs.getString(columns[i]);
						if ((t1.equals("E")) || (t1.equals("P"))) {
							activities[i] = "Non-billable";
							activity.put("lm_at_type", "Non-billable");
						} else {
							activities[i] = "Billable";
							activity.put("lm_at_type", "Billable");
						}
					} else if (LM_AT_AFTERHOURS_UPLIFT_FACTOR == i) {
						t1 = rs.getString(columns[i]);
						if (rs.wasNull()) {
							this.NonPPS = "No";
						} else {
							this.NonPPS = "Yes";
						}
						activity.put("lm_at_afterhours_uplift_factor",
								this.NonPPS);
						activities[i] = this.NonPPS;
					} else {
						activities[i] = rs.getString(columns[i]);
					}
				}
				this.lm_activity.add(activities);

				activity.put("lm_at_quantity", rs.getString("lm_at_quantity"));
				activity.put("lx_ce_total_price",
						rs.getString("lx_ce_total_price"));

				this.Activity.put(rs.getString("lm_at_id"), activity);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println("Job2.getActivities: " + e);
		}
		return this.lm_activity;
	}

	void processActivityResources() {
		Vector<String> po_to_resource_mapping = new Vector();
		try {
			getActivityResourceList();
			for (int i = 0; i < this.pf_resource_list.size(); i++) {
				Resource r = new Resource(this.da,
						(String) this.pf_resource_list.get(i));

				String lm_rs_id = r.lm_resource[Resource.LM_RS_ID];

				this.pf_resource.add(r);
				this.pf_resource2.put(lm_rs_id, r);
				if (r.ResourceToPOMapped) {
					String po = r.lm_purchase_work_order[Resource.LM_PWO_PO_ID];

					this.ResourceToPurchaseOrders.put(lm_rs_id, po);
					if (this.PurchaseOrderToResources.containsKey(po)) {
						po_to_resource_mapping = (Vector) this.PurchaseOrderToResources
								.get(po);
					} else {
						po_to_resource_mapping = new Vector();

						this.PurchaseOrderList.add(po);
					}
					po_to_resource_mapping.add(lm_rs_id);

					this.PurchaseOrderToResources.put(po,
							po_to_resource_mapping);
				} else {
					this.ResourceToPurchaseOrders.put(lm_rs_id, "None");
				}
			}
		} catch (Exception e) {
			System.out.println("Job2.processActivityResources: " + e);
		}
	}

	void getActivityResourceList() {
		String act_id_list = "";
		String del = "";
		for (int i = 0; i < this.lm_activity.size(); i++) {
			act_id_list = act_id_list + del
					+ ((String[]) this.lm_activity.get(i))[LM_AT_ID];
			del = ",";
		}
		String sql = "select lm_rs_id   from lm_resource        join iv_resource_pool on lm_rs_rp_id = iv_rp_id where iv_rp_cns_part_number not like '6%'       and lm_rs_type in ('IT','IL')        and lm_rs_at_id in ("
				+

				act_id_list + ")";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				this.pf_resource_list.add(rs.getString(1));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println("Job2.getActivityResourceList: " + e);
		}
	}

	void getPODetails() {
		String po = "";
		for (int i = 0; i < this.PurchaseOrderList.size(); i++) {
			po = (String) this.PurchaseOrderList.get(i);

			this.PurchaseOrderDetails.put(po, getPODetail(po));
		}
	}

	Map<String, String> getPODetail(String po) {
		Map<String, String> po_details = new HashMap();

		String[] columns = { "", "lm_po_partner_id", "lm_po_type",
				"lm_po_pwo_type_id", "lm_po_authorised_total", "po_status",
				"lm_po_job_travel_distance", "lm_po_job_travel_duration",
				"lm_po_minuteman_cost_override", "lo_om_division" };

		String sql = "select * from  dbo.lm_purchase_order          join dbo.cp_partner_search_list_01 on lm_po_partner_id = cp_partner_id where lm_po_id = "
				+

				po;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				for (int i = 1; i < columns.length; i++) {
					String key = columns[i];
					String value = rs.getString(columns[i]);
					if (rs.wasNull()) {
						value = "";
					}
					po_details.put(key, value);
				}
			}
			rs.close();
		} catch (Exception e) {
			System.out.println("Job2.getPODetail for PO" + po + " " + e);
		}
		return po_details;
	}

	void evaluatePurchaseOrders(int src) {
		String po = "";

		Double q_delta = Double.valueOf(0.0D);
		String po_status = "";
		String lm_po_type = "";
		try {
			for (int i = 0; i < this.PurchaseOrderList.size(); i++) {
				po = (String) this.PurchaseOrderList.get(i);
				Map<String, String> po_details = (Map) this.PurchaseOrderDetails
						.get(po);
				po_status = (String) po_details.get("po_status");
				lm_po_type = (String) po_details.get("lm_po_type");
				if ((po_status.equals("3"))
						&& ((lm_po_type.equals("M"))
								|| (lm_po_type.equals("S")) || (lm_po_type
									.equals("P")))) {
					Vector<String> rl = (Vector) this.PurchaseOrderToResources
							.get(po);

					this.LaborQtyBuy = Double.valueOf(0.0D);
					this.LaborAmtBuy = Double.valueOf(0.0D);

					this.LaborQtySell = Double.valueOf(0.0D);
					this.LaborAmtSell = Double.valueOf(0.0D);

					this.LaborQtyPF = Double.valueOf(this.LaborQtyPF
							.doubleValue() + 0.0D);
					this.LaborAmtPF = Double.valueOf(this.LaborAmtPF
							.doubleValue() + 0.0D);

					computePOBuySellSide(rl);

					q_delta = Double.valueOf(this.LaborQtyBuy.doubleValue()
							- this.LaborQtySell.doubleValue());

					evaluateLaborUsage(po, q_delta, src);
				}
			}
		} catch (Exception e) {
			System.out.println("Job2.getPODetail: " + e);
		}
	}

	void computePOBuySellSide(Vector<String> rl) {
		Double bs = Double.valueOf(0.0D);
		Double bq = Double.valueOf(0.0D);

		Double ss = Double.valueOf(0.0D);
		Double sq = Double.valueOf(0.0D);

		Double ss_pf = Double.valueOf(0.0D);
		Double sq_pf = Double.valueOf(0.0D);

		String type = "";
		for (int i = 0; i < rl.size(); i++) {
			Resource r = (Resource) this.pf_resource2.get(rl.get(i));

			type = r.lm_resource[Resource.LM_RS_TYPE];
			if ((type.equals("IL")) || (type.equals("IT"))) {
				try {
					bs = Double
							.valueOf(Double
									.parseDouble(r.lm_purchase_work_order[Resource.LM_PWO_AUTHORISED_TOTAL_COST]));
					bq = Double
							.valueOf(Double
									.parseDouble(r.lm_purchase_work_order[Resource.LM_PWO_AUTHORISED_QTY]));
				} catch (Exception e) {
					bs = Double.valueOf(0.0D);
				}
				try {
					ss = getSellSideActual(r);
					sq = getSellSideQty(r);
				} catch (Exception e) {
					ss = Double.valueOf(0.0D);
				}
				try {
					sq_pf = getSellSideQtyPF(r);
					ss_pf = ss;
				} catch (Exception e) {
					ss = Double.valueOf(0.0D);
				}
				this.LaborQtyBuy = Double.valueOf(this.LaborQtyBuy
						.doubleValue() + bq.doubleValue());
				this.LaborAmtBuy = Double.valueOf(this.LaborAmtBuy
						.doubleValue() + bs.doubleValue());

				this.LaborAmtSell = Double.valueOf(this.LaborAmtSell
						.doubleValue() + ss.doubleValue());
				this.LaborQtySell = Double.valueOf(this.LaborQtySell
						.doubleValue() + sq.doubleValue());

				this.LaborQtyPF = Double.valueOf(this.LaborQtyPF.doubleValue()
						+ sq_pf.doubleValue());
				this.LaborAmtPF = Double.valueOf(this.LaborAmtPF.doubleValue()
						+ ss_pf.doubleValue());
			}
		}
	}

	Double getSellSideActual(Resource r) {
		Double ss = Double.valueOf(0.0D);
		Double q = Double.valueOf(0.0D);

		String lm_rs_at_id = r.lm_resource[Resource.LM_RS_AT_ID];
		Map<String, String> activity = (Map) this.Activity.get(lm_rs_at_id);

		String type = (String) activity.get("lm_at_type");
		String qty = (String) activity.get("lm_at_quantity");
		String oos = (String) activity.get("lm_at_oos_flag");
		try {
			ss = Double.valueOf(Double
					.parseDouble(r.lx_cost_element[Resource.LX_CE_TOTAL_COST]));
			q = Double.valueOf(Double.parseDouble(qty));
			ss = Double.valueOf(ss.doubleValue() * q.doubleValue());
		} catch (Exception e) {
			System.out.println("Job2.getSellSideActual for " + lm_rs_at_id);
		}
		return ss;
	}

	Double getSellSideQty(Resource r) {
		Double q1 = Double.valueOf(0.0D);
		Double q2 = Double.valueOf(0.0D);

		String lm_rs_at_id = r.lm_resource[Resource.LM_RS_AT_ID];
		Map<String, String> activity = (Map) this.Activity.get(lm_rs_at_id);

		String type = (String) activity.get("lm_at_type");
		String qty = (String) activity.get("lm_at_quantity");
		String oos = (String) activity.get("lm_at_oos_flag");
		try {
			if ((oos.equals("Yes")) && (type.equals("Non-billable"))) {
				q2 = Double.valueOf(0.0D);
			} else {
				q1 = Double
						.valueOf(Double
								.parseDouble(r.lx_cost_element[Resource.LX_CE_QUANTITY]));
				q2 = Double.valueOf(Double.parseDouble(qty));
				q2 = Double.valueOf(q1.doubleValue() * q2.doubleValue());
			}
		} catch (Exception e) {
			System.out.println("Job2.getSellSideActual for " + lm_rs_at_id);
		}
		return q2;
	}

	Double getSellSideQtyPF(Resource r) {
		Double q1 = Double.valueOf(0.0D);
		Double q2 = Double.valueOf(0.0D);

		String lm_rs_at_id = r.lm_resource[Resource.LM_RS_AT_ID];
		Map<String, String> activity = (Map) this.Activity.get(lm_rs_at_id);

		String type = (String) activity.get("lm_at_type");
		String qty = (String) activity.get("lm_at_quantity");
		String oos = (String) activity.get("lm_at_oos_flag");
		try {
			q1 = Double.valueOf(Double
					.parseDouble(r.lx_cost_element[Resource.LX_CE_QUANTITY]));
			q2 = Double.valueOf(Double.parseDouble(qty));
			q2 = Double.valueOf(q1.doubleValue() * q2.doubleValue());
		} catch (Exception e) {
			System.out.println("Job2.getSellSideActual for " + lm_rs_at_id);
		}
		return q2;
	}

	void evaluateLaborUsage(String po, Double delta, int src) {
		Map<String, String> po_details = (Map) this.PurchaseOrderDetails
				.get(po);

		String po_type = (String) po_details.get("lm_po_type");
		Double authorised_total = Double.valueOf(0.0D);
		Double effective_rate = Double.valueOf(0.0D);
		Double effective_rate_all = Double.valueOf(0.0D);
		Double effective_rate_delta = Double.valueOf(0.0D);
		Double effective_rate_delta_all = Double.valueOf(0.0D);

		String oob = "";
		try {
			authorised_total = Double.valueOf(Double
					.parseDouble((String) po_details
							.get("lm_po_authorised_total")));
		} catch (Exception e) {
			authorised_total = Double.valueOf(0.0D);
		}
		try {
			if (po_type.equals("M")) {
				oob = "OK";
				if (this.LaborQtyBuy.doubleValue() > 0.0D) {
					effective_rate = Double.valueOf(this.LaborAmtBuy
							.doubleValue() / this.LaborQtyBuy.doubleValue());
				} else {
					effective_rate = Double.valueOf(18.0D);
				}
				if ((this.LaborQtyBuy.doubleValue() > 0.0D)
						&& (authorised_total.doubleValue() > 0.0D)) {
					effective_rate_all = Double.valueOf(authorised_total
							.doubleValue() / this.LaborQtyBuy.doubleValue());
				} else {
					effective_rate_all = Double.valueOf(18.0D);
				}
				effective_rate_delta = Double.valueOf(Math.abs(effective_rate
						.doubleValue() - 18.0D));
				effective_rate_delta_all = Double.valueOf(Math
						.abs(effective_rate.doubleValue() - 18.0D));
				if (this.ProjectType.equals("3")) {
					if (authorised_total.doubleValue() > 54.0D) {
						if (delta.doubleValue() > 0.01D) {
							oob = "MM Hours";
						}
					} else if ((effective_rate_delta.doubleValue() > 0.01D)
							|| (effective_rate_delta_all.doubleValue() > 36.0D)) {
						oob = "MM Rate";
					}
				} else if (delta.doubleValue() > 0.01D) {
					oob = "MM Hours";
				} else if ((effective_rate_delta.doubleValue() > 0.01D)
						|| (effective_rate_delta_all.doubleValue() > 36.0D)) {
					oob = "MM Rate";
				}
			} else if (po_type.equals("S")) {
				oob = "OK";
				if (this.LaborQtyBuy.doubleValue() > 0.0D) {
					effective_rate = Double.valueOf(this.LaborAmtBuy
							.doubleValue() / this.LaborQtyBuy.doubleValue());
				} else {
					effective_rate = Double.valueOf(24.0D);
				}
				if ((this.LaborQtyBuy.doubleValue() > 0.0D)
						&& (authorised_total.doubleValue() > 0.0D)) {
					effective_rate_all = Double.valueOf(authorised_total
							.doubleValue() / this.LaborQtyBuy.doubleValue());
				} else {
					effective_rate_all = Double.valueOf(24.0D);
				}
				effective_rate_delta = Double.valueOf(Math.abs(effective_rate
						.doubleValue() - 24.0D));
				effective_rate_delta_all = Double.valueOf(Math
						.abs(effective_rate.doubleValue() - 24.0D));
				if (delta.doubleValue() > 0.01D) {
					oob = "SP Hours";
				} else if ((effective_rate_delta.doubleValue() > 0.01D)
						|| (effective_rate_delta_all.doubleValue() > 48.0D)) {
					oob = "SP Rate";
				}
			} else if (po_type.equals("P")) {
				oob = "OK";
				if (this.LaborQtyBuy.doubleValue() > 0.0D) {
					effective_rate = Double.valueOf(this.LaborAmtBuy
							.doubleValue() / this.LaborQtyBuy.doubleValue());
					if (authorised_total.doubleValue() > 0.0D) {
						effective_rate_all = Double
								.valueOf(authorised_total.doubleValue()
										/ this.LaborQtyBuy.doubleValue());
					} else {
						effective_rate_all = effective_rate;
					}
				}
				effective_rate_delta = Double.valueOf(0.0D);
				effective_rate_delta_all = Double.valueOf(Math
						.abs(effective_rate.doubleValue()
								- effective_rate_all.doubleValue()));
				if (delta.doubleValue() > 0.01D) {
					oob = "PVS Hours";
				}
				if (effective_rate_delta_all.doubleValue() > 2.0D * effective_rate
						.doubleValue()) {
					oob = "PVS Rate";
				}
			}
			if (!oob.equals("OK")) {
				if (src == 1) {
					saveException(oob, po, po_type, effective_rate);

					System.out.println("ProFormaCost " + this.ProFormaCost
							+ "\tActualCost " + this.ActualCost + "\tRevenue "
							+ this.Revenue + "\t" + this.LaborQtyPF + "\t"
							+ this.LaborAmtPF + "\t" + this.LaborQtySell + "\t"
							+ this.LaborAmtSell + "\t" + this.LaborQtyBuy
							+ "\t" + this.LaborAmtBuy);
				}
			}
			if (src == 0) {
				buildPoTable(po, oob, authorised_total, effective_rate);
			}
		} catch (Exception e) {
			System.out.println("Job2.evaluateLaborUsage " + e);
		}
	}

	void saveException(String status, String po, String po_type, Double er) {
		execute_ax_labor_buy_manage_02(po, this.Job, this.LaborQtyBuy,
				this.LaborAmtBuy, status, po_type, er);

		execute_ax_labor_pf_and_actual_manage_02(this.Job, this.LaborQtyPF,
				this.LaborAmtPF, this.LaborQtySell, this.LaborAmtSell);
	}

	boolean execute_ax_labor_pf_and_actual_manage_02(String ax_js_id,
			Double labor_qty_pf, Double labor_amt_pf, Double labot_qty_sell,
			Double labor_amt_sell) {
		boolean rc = false;
		int sp_rc = 0;

		String sql = "{? = call datawarehouse.dbo.ax_labor_pf_and_actual_manage_02(?, ?, ?, ?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.registerOutParameter(1, 4);
			stmt.setObject("@ax_js_id", ax_js_id);
			stmt.setObject("@labor_qty_pf", labor_qty_pf);
			stmt.setObject("@labor_amt_pf", labor_amt_pf);
			stmt.setObject("@labor_qty_sell", labot_qty_sell);
			stmt.setObject("@labor_amt_sell", labor_amt_sell);
			stmt.execute();
			sp_rc = stmt.getInt(1);
			stmt.close();
			if (sp_rc > 0) {
				rc = true;
			}
		} catch (Exception e) {
			System.out.println("Job2.execute_ax_labor_pf_and_actual_manage_02 "
					+ e);
		}
		return rc;
	}

	boolean execute_ax_labor_buy_manage_02(String ax_po_id, String ax_po_js_id,
			Double labor_qty_buy, Double labot_amt_buy, String status,
			String po_type, Double er) {
		boolean rc = false;
		int sp_rc = 0;

		String sql = "{? = call datawarehouse.dbo.ax_labor_buy_manage_02(?, ?, ?, ?, ?, ?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.registerOutParameter(1, 4);
			stmt.setObject("@ax_po_id", ax_po_id);
			stmt.setObject("@ax_po_js_id", ax_po_js_id);
			stmt.setObject("@labor_qty_buy", labor_qty_buy);
			stmt.setObject("@labor_amt_buy", labot_amt_buy);
			stmt.setObject("@status", status);
			stmt.setObject("@po_type", po_type);
			stmt.setObject("@effective_rate", er);

			stmt.execute();
			sp_rc = stmt.getInt(1);
			stmt.close();
			if (sp_rc > 0) {
				rc = true;
			}
		} catch (Exception e) {
			System.out.println("Job2.execute_ax_labor_buy_manage_02 " + e);
		}
		return rc;
	}

	void buildPoTable(String po, String status, Double authorised_total,
			Double effective_rate) {
		Map<String, String> po_details = (Map) this.PurchaseOrderDetails
				.get(po);
		if (!status.equals("OK")) {
			status = "<span class=\"txtE11B\">" + status + "</span>";
		}
		po_details.put("status", status);
		po_details.put("authorised_total", this.sd.format(authorised_total));
		po_details.put("effective_rate", this.sd.format(effective_rate));

		po_details.put("table", buildResourceTable(po));

		this.POTableMap.put(po, po_details);
	}

	String buildResourceTable(String po) {
		String table_output = "";
		String[] bgc = { "", " bgcD" };
		Double t1 = Double.valueOf(0.0D);
		Double t2 = Double.valueOf(0.0D);
		Double t3 = Double.valueOf(0.0D);
		Double t4 = Double.valueOf(0.0D);
		Double t5 = Double.valueOf(0.0D);
		Double t6 = Double.valueOf(0.0D);
		Double t7 = Double.valueOf(0.0D);

		Double r_total = Double.valueOf(0.0D);

		Vector<String> rl = (Vector) this.PurchaseOrderToResources.get(po);
		for (int i = 0; i < rl.size(); i++) {
			Resource r = (Resource) this.pf_resource2.get(rl.get(i));
			Map<String, String> activity = (Map) this.Activity
					.get(r.lm_resource[Resource.LM_RS_AT_ID]);

			String r_name = r.iv_resource_pool[Resource.IV_RP_NAME];
			String r_type = r.lm_resource[Resource.LM_RS_TYPE];
			if (r_type.equals("IL")) {
				r_type = "Labor";
			} else {
				r_type = "Travel";
			}
			String oos = (String) activity.get("lm_at_oos_flag");

			String billable = (String) activity.get("lm_at_type");
			if (billable.equals("Billable")) {
				billable = "Yes";
			} else {
				billable = "No";
			}
			String a_qty = (String) activity.get("lm_at_quantity");

			String r_qty = r.lx_cost_element[Resource.LX_CE_QUANTITY];
			String rc_unit = r.lx_cost_element[Resource.LX_CE_UNIT_COST];
			String rc_total = r.lx_cost_element[Resource.LX_CE_TOTAL_COST];

			String po_qty = r.lm_purchase_work_order[Resource.LM_PWO_AUTHORISED_QTY];
			String po_unit = r.lm_purchase_work_order[Resource.LM_PWO_AUTHORISED_UNIT_COST];
			String po_total = r.lm_purchase_work_order[Resource.LM_PWO_AUTHORISED_TOTAL_COST];

			t1 = Double.valueOf(parseColumn(r_qty).doubleValue()
					* parseColumn(a_qty).doubleValue());

			t7 = Double.valueOf(t7.doubleValue() + t1.doubleValue());
			t2 = Double.valueOf(t2.doubleValue()
					+ parseColumn(rc_unit).doubleValue());
			r_total = Double.valueOf(t1.doubleValue()
					* parseColumn(rc_unit).doubleValue());
			t3 = Double.valueOf(t3.doubleValue() + r_total.doubleValue());

			t4 = Double.valueOf(t4.doubleValue()
					+ parseColumn(po_qty).doubleValue());
			t5 = Double.valueOf(t5.doubleValue()
					+ parseColumn(po_unit).doubleValue());
			t6 = Double.valueOf(t6.doubleValue()
					+ parseColumn(po_total).doubleValue());

			table_output = table_output + "<tr class=\"txtC10N p3010 ar"
					+ bgc[(i % 2)] + "\">" + "<td class=\"al\">" + r_name
					+ "&nbsp;&nbsp;</td>" + "<td class=\"ac\">" + r_type
					+ "</td>" + "<td class=\"ac\">" + billable + "</td>"
					+ "<td class=\"ac\">" + oos + "</td>" + "<td>"
					+ this.sd.format(t1) + "</td>" + "<td>"
					+ formatColumn(rc_unit) + "</td>" + "<td>"
					+ this.sd.format(r_total) + "</td>"
					+ "<td><img src=\"lua/sp1.gif\" width=\"10\"></td>"
					+ "<td>" + formatColumn(po_qty) + "</td>" + " <td>"
					+ formatColumn(po_unit) + "</td>" + "<td>"
					+ formatColumn(po_total) + "</td>" + "</tr>\n";
		}
		table_output =

		table_output
				+ "<tr><td colspan=\"11\"><img src=\"lua/sp1.gif\" height=\"3\"></td></tr><tr><td colspan=\"11\" class=\"rule2BM1010\"><img src=\"lua/sp1.gif\"></td></tr><tr class=\"txtB11B p5005 ar\"><td class=\"p5000 al\">Total</td><td class=\"ac\">&nbsp;</td><td class=\"ac\">&nbsp;</td><td class=\"ac\">&nbsp;</td><td>"
				+ this.sd.format(t7) + "</td>" + "<td>$" + this.sd.format(t2)
				+ "</td>" + "<td>$" + this.sd.format(t3) + "</td>"
				+ "<td><img src=\"lua/sp1.gif\" width=\"10\"></td>"
				+ "<td class=\"txtA11B\">" + this.sd.format(t4) + "</td>"
				+ " <td class=\"txtA11B\">$" + this.sd.format(t5) + "</td>"
				+ "<td class=\"txtA11B\">$" + this.sd.format(t6) + "</td>"
				+ "</tr>\n";

		return table_output;
	}

	Double sumColumn(Double sum, String s_value) {
		Double value = Double.valueOf(0.0D);
		try {
			value = Double.valueOf(Double.parseDouble(s_value));
			sum = Double.valueOf(sum.doubleValue() + value.doubleValue());
		} catch (Exception localException) {
		}
		return sum;
	}

	Double parseColumn(String s_value) {
		Double value = Double.valueOf(0.0D);
		try {
			value = Double.valueOf(Double.parseDouble(s_value));
		} catch (Exception localException) {
		}
		return value;
	}

	String formatColumn(String s_value) {
		String f_value = "";

		Double value = parseColumn(s_value);
		try {
			f_value = this.sd.format(value);
		} catch (Exception localException) {
		}
		return f_value;
	}
}
