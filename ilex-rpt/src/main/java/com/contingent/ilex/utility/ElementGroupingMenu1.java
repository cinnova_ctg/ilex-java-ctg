package com.contingent.ilex.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ElementGroupingMenu1 extends Element {
	ElementGroupingMenu1(Map init, HttpServletRequest request) {
		super(init, request);
		createElement();
	}

	void createElement() {
		String[] values = this.data.split("~");

		String selected = "";
		String use_value = "";

		String current_group = "";
		String new_group = null;
		if (this.currentValue != null) {
			use_value = this.currentValue;
		}
		this.elementHTML = "\t<option value=\"\"> -- Select Option -- </option>\n";

		String cr = "\n";
		for (int i = 0; i < values.length; i++) {
			if (i == values.length - 1) {
				cr = "";
			}
			String[] option_values = values[i].split("\\|");
			if (use_value.equals(option_values[2])) {
				selected = " SELECTED";
			}
			new_group = option_values[0];
			if (!new_group.equals(current_group)) {
				current_group = new_group;
				this.elementHTML = this.elementHTML
						.concat("  <optgroup label=\"" + new_group + "\">" + cr);
			}
			this.elementHTML = this.elementHTML.concat("\t<option value=\""
					+ option_values[2] + "\"" + selected + ">"
					+ option_values[1] + "</option>" + cr);
			selected = "";
		}
		this.elementHTML = ("<select name=\"" + this.name
				+ "\" size=\"1\" class=\"menu\" " + this.javascript + ">\n"
				+ this.elementHTML + "</select>");
	}
}
