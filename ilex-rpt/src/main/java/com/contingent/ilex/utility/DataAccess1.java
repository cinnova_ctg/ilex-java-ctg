package com.contingent.ilex.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DataAccess1 {
	public static Connection databaseConnection = null;
	private Statement sql;

	public DataAccess1() {
		if (databaseConnection == null) {
			try {
				Class.forName("net.sourceforge.jtds.jdbc.Driver");
			} catch (Exception e) {
				System.out.println("Class.forName Failed with error: " + e);
			}
			try {
				String db_url = "jdbc:jtds:sqlserver://192.168.32.53:1433/ilex;user=sa;password=Wawd01t";

				databaseConnection = DriverManager.getConnection(db_url);
				System.out.println("Connection established");
			} catch (Exception sqle) {
				System.out.println("Connection Failed with error: " + sqle);
			}
		}
	}

	public ResultSet getResultSet(String sql_to_execute) {
		ResultSet rs_return = null;
		try {
			this.sql = databaseConnection.createStatement();
			rs_return = this.sql.executeQuery("use ilex; " + sql_to_execute);
		} catch (Exception e) {
			System.out.println(e);
			rs_return = null;
		}
		return rs_return;
	}

	public boolean executeQuery(String sql_to_execute) {
		boolean executeStatus = false;
		try {
			this.sql = databaseConnection.createStatement();
			this.sql.execute("use ilex; " + sql_to_execute);
			executeStatus = true;
		} catch (Exception e) {
			System.out.println(e + " " + sql_to_execute);
			return executeStatus;
		}
		return executeStatus;
	}

	public String getValues1(String sql_to_execute) {
		String values = "";
		String delimiter = "";
		try {
			this.sql = databaseConnection.createStatement();
			ResultSet rs = this.sql.executeQuery("use ilex; " + sql_to_execute);
			while (rs.next()) {
				values = values + delimiter + rs.getString(1);
				delimiter = "~";
			}
		} catch (Exception e) {
			System.out.println(e);
			values = null;
		}
		return values;
	}
}
