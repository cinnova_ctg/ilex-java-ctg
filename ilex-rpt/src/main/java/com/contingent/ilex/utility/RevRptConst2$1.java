package com.contingent.ilex.utility;

import java.util.HashMap;

final class RevRptConst2$1 extends HashMap<String, String> {
	private static final long serialVersionUID = -6310981411835783146L;

	RevRptConst2$1() {
		put("Jan", "01");
		put("Feb", "02");
		put("Mar", "03");
		put("Apr", "04");
		put("May", "05");
		put("Jun", "06");
		put("Jul", "07");
		put("Aug", "08");
		put("Sep", "09");
		put("Oct", "10");
		put("Nov", "11");
		put("Dec", "12");
		put("01", "Jan");
		put("02", "Feb");
		put("03", "Mar");
		put("04", "Apr");
		put("05", "May");
		put("06", "Jun");
		put("07", "Jul");
		put("08", "Aug");
		put("09", "Sep");
		put("10", "Oct");
		put("11", "Nov");
		put("12", "Dec");
	}
}
