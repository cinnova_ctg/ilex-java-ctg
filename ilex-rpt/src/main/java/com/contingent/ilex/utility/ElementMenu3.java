package com.contingent.ilex.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ElementMenu3 extends Element {
	ElementMenu3(Map init, HttpServletRequest request) {
		super(init, request);
		createElement();
	}

	void createElement() {
		String[] values = this.data.split("~");
		String selected = "";
		String use_value = "";
		if (this.currentValue != null) {
			use_value = this.currentValue;
		}
		this.elementHTML = "\t<option value=\"\"> -- Select Option -- </option>\n";
		if (use_value.equals("All")) {
			selected = " SELECTED";
		}
		this.elementHTML = this.elementHTML.concat("\t<option value=\"All\""
				+ selected + ">All</option>\n");
		this.elementHTML = this.elementHTML.concat("\t<option value=\"Active\""
				+ selected + ">Active</option>\n");

		selected = "";

		String cr = "\n";
		for (int i = 0; i < values.length; i++) {
			if (i == values.length - 1) {
				cr = "";
			}
			if (use_value.equals(values[i])) {
				selected = " SELECTED";
			}
			this.elementHTML = this.elementHTML.concat("\t<option value=\""
					+ values[i] + "\"" + selected + ">" + values[i]
					+ "</option>" + cr);
			selected = "";
		}
		this.elementHTML = ("<select name=\"" + this.name
				+ "\" size=\"1\" class=\"menu\" " + this.javascript + ">\n"
				+ this.elementHTML + "</select>");
	}
}
