package com.contingent.ilex.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ElementMenu2 extends Element {
	ElementMenu2(Map init, HttpServletRequest request) {
		super(init, request);
		createElement();
	}

	void createElement() {
		String[] values = this.data.split("~");
		String selected = "";
		String use_value = "";
		if (this.currentValue != null) {
			use_value = this.currentValue;
		}
		this.elementHTML = "\t<option value=\"\"> -- Select Option -- </option>\n";
		String cr = "\n";
		for (int i = 0; i < values.length; i++) {
			if (i == values.length - 1) {
				cr = "";
			}
			String[] option_date = values[i].split("\\|");
			if (use_value.equals(option_date[0])) {
				selected = " SELECTED";
			}
			this.elementHTML = this.elementHTML.concat("\t<option value=\""
					+ option_date[0] + "\"" + selected + ">" + option_date[1]
					+ "</option>" + cr);
			selected = "";
		}
		this.elementHTML = ("<select name=\"" + this.name
				+ "\" size=\"1\" class=\"menu\" " + this.javascript + ">\n"
				+ this.elementHTML + "</select>");
	}
}
