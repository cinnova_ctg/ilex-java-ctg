package com.contingent.ilex.utility;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ReportUtils {
	private static DecimalFormat currency = null;

	public static String getMonthFromIntDate(int date) {
		String month = Integer.toString(date).substring(4);
		return (String) RevRptConst.MONTH_MAP.get(month);
	}

	public static String currencyFormatIntValue(int val) {
		if (currency == null) {
			currency = new DecimalFormat();
			currency.setDecimalSeparatorAlwaysShown(false);
		}
		return "$" + currency.format(Integer.valueOf(val));
	}

	public static int getYearFromIntDate(int date) {
		String year = Integer.toString(date).substring(0, 4);
		return Integer.parseInt(year);
	}

	public static ArrayList<Integer> getDatesAsInts(ArrayList<Integer> dates,
			int dstartYM, int dendYM) {
		boolean useDateFlag = false;
		boolean finishedFlag = false;
		ArrayList<Integer> retVals = new ArrayList();
		for (int i = 0; i < dates.size(); i++) {
			if (finishedFlag) {
				break;
			}
			int temp = ((Integer) dates.get(i)).intValue();
			if (temp == dstartYM) {
				useDateFlag = true;
			}
			if (useDateFlag) {
				retVals.add(Integer.valueOf(temp));
			}
			if (temp == dendYM) {
				finishedFlag = true;
			}
		}
		return retVals;
	}

	public static int roundInt(int input, int precision) {
		int output = 0;
		double multDiv = 0.0D;
		double temp = input;

		int numDigits = Integer.toString(input).length();
		int diff = numDigits - precision;
		if (diff >= -1) {
			multDiv = Math.pow(10.0D, diff);
			temp = Math.floor(temp / multDiv) * multDiv;
		}
		output = (int) temp;
		return output;
	}

	public static void main(String[] args) {
		int testCurrency = 1234567;
		String currency = currencyFormatIntValue(testCurrency);
		System.out.println("int " + testCurrency + " converted to " + currency);
		int testRound = 7815;
		System.out.println("testRound (" + testRound + ") rounded to "
				+ roundInt(testRound, 2));
	}

	public static String getTimeStamp() {
		Date startDate = new Date(System.currentTimeMillis());
		SimpleDateFormat sdfNow = new SimpleDateFormat(
				"MM/dd/yyyy,HH:mm:ss.SSS");
		String now = sdfNow.format(startDate);
		return now;
	}
}
