package com.contingent.ilex.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ElementTextArea1 extends Element {
	ElementTextArea1(Map init, HttpServletRequest request) {
		super(init, request);
		createElement();
	}

	void createElement() {
		String use_value = "";
		if (this.currentValue != null) {
			use_value = this.currentValue;
		}
		this.elementHTML = ("<textarea name=\"" + this.name
				+ "\" rows=\"3\" cols=\"50\" class=\"textarea1\">" + use_value + "</textarea>\n");
	}
}
