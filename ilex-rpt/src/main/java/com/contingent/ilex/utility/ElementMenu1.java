package com.contingent.ilex.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ElementMenu1 extends Element {
	ElementMenu1(Map init, HttpServletRequest request) {
		super(init, request);
		createElement();
	}

	void createElement() {
		String[] values = this.data.split("~");
		String selected = "";
		String use_value = "";
		if (this.currentValue != null) {
			use_value = this.currentValue;
		}
		this.elementHTML = "\t<option value=\"\"> -- Select Option -- </option>\n";

		String cr = "\n";
		for (int i = 0; i < values.length; i++) {
			if (i == values.length - 1) {
				cr = "";
			}
			if (use_value.equals(values[i])) {
				selected = " SELECTED";
			}
			this.elementHTML = this.elementHTML.concat("\t<option value=\""
					+ values[i] + "\"" + selected + ">" + values[i]
					+ "</option>" + cr);
			selected = "";
		}
		this.elementHTML = ("<select name=\"" + this.name
				+ "\" size=\"1\" class=\"menu\" " + this.javascript + ">\n"
				+ this.elementHTML + "</select>");
	}
}
