package com.contingent.ilex.utility;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class ElementBuilder extends TagSupport {
	String type;
	String name;
	String source;
	String data;
	String javascript;
	String checkbox;
	Map elementData = new HashMap();
	HttpServletRequest request;

	public int doStartTag() throws JspException {
		Element e = new Element();
		try {
			Element el = Element.ElementFactory(this.elementData, this.request);
			this.pageContext.getOut().print(el.elementHTML);
		} catch (Exception ioException) {
			System.out.println("generateFieldElement Error: " + ioException);
		}
		return 0;
	}

	public void setType(String ty) {
		this.type = ty;
		this.elementData.put("type", ty);
	}

	public void setRequest(HttpServletRequest rq) {
		this.request = rq;
	}

	public void setName(String nm) {
		this.name = nm;
		this.elementData.put("name", nm);
	}

	public void setSource(String sr) {
		this.source = sr;
		this.elementData.put("source", sr);
	}

	public void setData(String da) {
		this.data = da;
		this.elementData.put("data", da);
	}

	public void setJs(String js) {
		this.javascript = js;
		this.elementData.put("javascript", js);
	}

	public void setCvalue(String value) {
		this.checkbox = value;
		this.elementData.put("checkbox", this.checkbox);
	}
}
