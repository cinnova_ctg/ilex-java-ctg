package com.contingent.ilex.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ElementHidden extends Element {
	ElementHidden(Map init, HttpServletRequest request) {
		super(init, request);
		createElement();
	}

	void createElement() {
		String use_value = "";
		if (this.currentValue != null) {
			use_value = this.currentValue;
		}
		this.elementHTML = ("<input type=\"hidden\" name=\"" + this.name
				+ "\"  value=\"" + use_value + "\">");
	}
}
