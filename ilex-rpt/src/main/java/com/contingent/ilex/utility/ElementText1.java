package com.contingent.ilex.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ElementText1 extends Element {
	ElementText1(Map init, HttpServletRequest request) {
		super(init, request);
		createElement();
	}

	void createElement() {
		String use_value = "";
		if (this.currentValue != null) {
			use_value = this.currentValue;
		}
		this.elementHTML = ("<input type=\"text\" name=\"" + this.name
				+ "\" value=\"" + use_value + "\" size=\"10\" class=\"text1\">");
	}
}
