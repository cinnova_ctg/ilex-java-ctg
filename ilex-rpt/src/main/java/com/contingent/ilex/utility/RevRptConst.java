package com.contingent.ilex.utility;

import java.util.HashMap;
import java.util.Map;

public class RevRptConst {
	public static final String attrValueSeparator = "#|#";
	public static final String userName = "contingent";
	public static final String password = "c0nting3nt";
	public static final String DIM_DATE_TABLE = "DimDate";
	public static final String DIM_EMP_TABLE = "DimEmployee";
	public static final String DIM_OPRATING_TABLE = "DimOperationalRating";
	public static final String DIM_OPSRATING_TABLE = "DimOperationsRating";
	public static final String DIM_PROJ_TABLE = "DimProject";
	public static final String DIM_QUALCALL_TABLE = "DimQualityCall";
	public static final String FACT_JOBREVACT_TABLE = "FactJobRevenueActualization";
	public static final int LASTNAMEFIRST = 0;
	public static final int FIRSTNAMEFIRST = 1;
	public static final int SR_PROJ_MGR_IDTYPE = 1;
	public static final int BUS_DEV_MGR_IDTYPE = 2;
	public static final int JOB_OWNER_IDTYPE = 3;
	public static final String dJOBOFFSITE = "JobOffsiteDate";
	public static final String dJOBSUBMITTED = "JobSubmittedDate";
	public static final String dJOBCLOSED = "JobClosedDate";
	public static final String JOBOFFSITE = "Offsite Date";
	public static final String JOBSUBMITTED = "Completed Date";
	public static final String JOBCLOSED = "Closed Date";
	public static final String[] DATETYPES = { "JobOffsiteDate",
			"JobSubmittedDate", "JobClosedDate" };
	public static final String[] DATEVALUES = { "Offsite Date",
			"Completed Date", "Closed Date" };
	public static final String[] MONTHS_TXT = { "Jan", "Feb", "Mar", "Apr",
			"May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	public static final String[] MONTHS_NUM = { "01", "02", "03", "04", "05",
			"06", "07", "08", "09", "10", "11", "12" };
	public static final Map<String, String> MONTH_MAP = new HashMap<String, String>();
	public static final String DEF_DATETYPE = "JobOffsiteDate";
	public static final String DEF_SRPROJMAN = "All Senior Project Mgrs";
	public static final String DEF_BUSDEVMAN = "All Business Development Mgrs";
	public static final String DEF_JOBOWNER = "All Job Owners";
	public static final String DEF_CLIENT = "All Clients";
	public static final String DEF_PROJTYPE = "All Project Types";
	public static final int DEF_MIN_CHT_WIDTH = 170;
	public static final int DEF_MAX_CHT_WIDTH = 1000;
	public static final int DEF_MAX_CHT_HEIGHT = 300;
	public static final int DEF_BAR_WIDTH = 9;
	public static final int DEF_BAR_SEP = 1;
	public static final int DEF_GRP_SEP = 8;
	public static final int DEF_WIDTH_PER_MONTH = 37;
	public static final String DEF_CHARTBG_COLOR = "FFFFCC";
	public static final String DEF_CHARTAREA_COLOR = "E2E2E2";
	public static final float DEF_CHART_MULTIPLIER = 0.05F;
	public static final String DEF_TABLE_BORDER_COLOR = "000000";
	public static final int DEF_TABLE_BORDER_WIDTH = 1;
	public static final String LEGEND_VCOGS = "VCOGS (%)";
	public static final String LEGEND_VGP = "VGP (%)";
	public static final String LEGEND_REVENUE = "REVENUE ($)";
	public static final String COLOR_VCOGS = "990000";
	public static final String COLOR_VGP = "0000CC";
	public static final String COLOR_REVENUE = "009900";
	public static final int DEF_NUM_YAXIS_LABELS = 4;
	public static final int DEF_YAXIS_PRECISION = 2;
}
