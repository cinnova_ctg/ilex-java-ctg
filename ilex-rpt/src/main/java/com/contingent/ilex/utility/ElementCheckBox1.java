package com.contingent.ilex.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ElementCheckBox1 extends Element {
	ElementCheckBox1(Map init, HttpServletRequest request) {
		super(init, request);
		createElement();
	}

	void createElement() {
		String use_value = "";
		if (this.currentValue != null) {
			use_value = this.currentValue;
		}
		this.elementHTML = ("<input type=\"checkbox\" name=\"" + this.name
				+ "\" value=\"" + this.checkbox + "\" checked>");
	}
}
