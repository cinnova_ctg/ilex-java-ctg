package com.contingent.ilex.utility;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ReportUtils2 {
	private static DecimalFormat currency = null;

	public static int getMonthYearAsInt(String date) {
		String[] sDate = date.split("[ ]+");
		return Integer.parseInt(sDate[1]
				+ (String) RevRptConst.MONTH_MAP.get(sDate[0]));
	}

	public static String getMonthFromIntDate(int date) {
		String month = Integer.toString(date).substring(4);
		return (String) RevRptConst.MONTH_MAP.get(month);
	}

	public static String currencyFormatIntValue(int val) {
		if (currency == null) {
			currency = new DecimalFormat();
			currency.setDecimalSeparatorAlwaysShown(false);
		}
		return "$" + currency.format(Integer.valueOf(val));
	}

	public static int getYearFromIntDate(int date) {
		String year = Integer.toString(date).substring(0, 4);
		return Integer.parseInt(year);
	}

	public static ArrayList<Integer> getDatesAsInts(ArrayList<String> dates,
			String start, String end) {
		boolean useDateFlag = false;
		boolean finishedFlag = false;
		ArrayList<Integer> retVals = new ArrayList();
		for (int i = 0; i < dates.size(); i++) {
			if (finishedFlag) {
				break;
			}
			String temp = (String) dates.get(i);
			if (temp.equals(start)) {
				useDateFlag = true;
			}
			if (useDateFlag) {
				retVals.add(Integer.valueOf(getMonthYearAsInt(temp)));
			}
			if (temp.equals(end)) {
				finishedFlag = true;
			}
		}
		return retVals;
	}

	public static int roundInt(int input, int precision) {
		int output = 0;
		double multDiv = 0.0D;
		double temp = input;

		int numDigits = Integer.toString(input).length();
		int diff = numDigits - precision;
		if (diff >= -1) {
			multDiv = Math.pow(10.0D, diff);
			temp = Math.floor(temp / multDiv) * multDiv;
		}
		output = (int) temp;
		return output;
	}

	public static void main(String[] args) {
		String testdate = "Feb 2007";
		int date = getMonthYearAsInt(testdate);
		System.out.println("testdate " + testdate + " converted to " + date);
		int testCurrency = 1234567;
		String currency = currencyFormatIntValue(testCurrency);
		System.out.println("int " + testCurrency + " converted to " + currency);
		int testRound = 7815;
		System.out.println("testRound (" + testRound + ") rounded to "
				+ roundInt(testRound, 2));
	}
}
