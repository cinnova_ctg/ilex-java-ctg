package com.contingent.ilex.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.contingent.ilex.report.DataAccess;

public class Element {
	String type;
	String name;
	String currentValue;
	String source;
	String data;
	String javascript;
	String checkbox;
	String elementHTML;
	HttpServletRequest request;
	DataAccess da = new DataAccess();
	static String html;

	Element() {
	}

	Element(Map tp, HttpServletRequest request) {
		this.type = ((String) tp.get("type"));
		this.name = ((String) tp.get("name"));
		this.source = ((String) tp.get("source"));
		this.data = ((String) tp.get("data"));
		this.javascript = ((String) tp.get("javascript"));
		this.checkbox = ((String) tp.get("checkbox"));
		if (this.source.equals("database")) {
			this.data = this.da.getValues1(this.data);
		}
		this.currentValue = getParameter(this.name, request);
	}

	static Element ElementFactory(Map init, HttpServletRequest request) {
		Element newElement = null;

		String element_type = (String) init.get("type");
		if (element_type.equals("hidden")) {
			newElement = new ElementHidden(init, request);
		} else if (element_type.equals("menu1")) {
			newElement = new ElementMenu1(init, request);
		} else if (element_type.equals("menu2")) {
			newElement = new ElementMenu2(init, request);
		} else if (element_type.equals("menu3")) {
			newElement = new ElementMenu3(init, request);
		} else if (element_type.equals("text1")) {
			newElement = new ElementText1(init, request);
		} else if (element_type.equals("date1")) {
			newElement = new ElementDate1(init, request);
		} else if (element_type.equals("textarea1")) {
			newElement = new ElementTextArea1(init, request);
		} else if (element_type.equals("groupingmenu1")) {
			newElement = new ElementGroupingMenu1(init, request);
		} else if (element_type.equals("checkbox1")) {
			newElement = new ElementCheckBox1(init, request);
		}
		return newElement;
	}

	String getParameter(String parameter, HttpServletRequest request) {
		String parameter_value = null;
		if (request != null) {
			parameter_value = request.getParameter(parameter);
			if (parameter_value == null) {
				parameter_value = (String) request.getAttribute(parameter);
				if (parameter_value != null) {
					parameter_value = parameter_value.trim();
				}
			}
		}
		return parameter_value;
	}
}
