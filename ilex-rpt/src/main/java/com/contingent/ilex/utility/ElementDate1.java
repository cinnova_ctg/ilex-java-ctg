package com.contingent.ilex.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ElementDate1 extends Element {
	ElementDate1(Map init, HttpServletRequest request) {
		super(init, request);
		createElement();
	}

	void createElement() {
		String image = "";
		String use_value = "";
		if (this.currentValue != null) {
			use_value = this.currentValue;
		} else if (use_value.equals("Today")) {
			Date timeDate = new Date(System.currentTimeMillis());
			DateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
			use_value = fmt.format(timeDate);
		}
		image = "&nbsp;<img src=\"/rpt/images/calendar1.jpg\" width=\"14\" height=\"14\" alt=\"\" border=\"0\" onclick = \"return popUpCalendar(document.forms[0]."
				+ this.name
				+ ", document.forms[0]."
				+ this.name
				+ ", 'mm/dd/yyyy')\" onmouseover = \"window.status = 'Date Picker';return true;\" onmouseout = \"window.status = '';return true;\">";

		this.elementHTML = ("<input name=\"" + this.name
				+ "\" class=\"date1\" type=\"text\" size=\"9\" value=\""
				+ use_value + "\" readonly=\"readonly\">" + image);
	}
}
