package com.contingent.ilex.report;

import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

public class NetMedXInvoiceHeader extends PdfPageEventHelper {
	static String header;

	public NetMedXInvoiceHeader(String invoice_number) {
		header = "Invoice: " + invoice_number;
	}

	public void onEndPage(PdfWriter writer, Document document) {
		if (document.getPageNumber() > 1) {
			PdfContentByte cb = writer.getDirectContent();
			BaseFont bf;
			try {
				bf = BaseFont.createFont("Helvetica", "Cp1252", false);
				cb.beginText();
				cb.setFontAndSize(bf, 8.0F);
				cb.setTextMatrix(document.right() - 84.0F, document.top());
				cb.showText(header);
				cb.endText();
			} catch (DocumentException localDocumentException) {
			} catch (IOException localIOException) {
			}
		}
	}

	public static void main(String[] args) {
	}
}
