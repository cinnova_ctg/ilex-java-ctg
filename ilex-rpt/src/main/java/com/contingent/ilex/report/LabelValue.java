package com.contingent.ilex.report;
import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class LabelValue implements Serializable {

	private static final long serialVersionUID = -8195352616125870131L;
	private String label = null;
	private String value = null;

	public LabelValue(String a, String b) {
		label = a;
		value = b;
	}

	public void setLabel(String l) {
		label = l;
	}

	public String getLabel() {
		return label;
	}

	public void setValue(String v) {
		value = v;
	}

	public String getValue() {
		return value;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
