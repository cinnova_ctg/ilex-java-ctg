package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class LaborRates extends DispatchAction {
	Map<String, Vector<String>> RequestIputMap = new HashMap();
	Map<String, String> RequestIputs = new HashMap();
	String ErrorMessag = "";
	int grouping_index;
	int lm_pwo_authorised_qty;
	int lm_pwo_authorised_unit_cost;
	int lm_pwo_authorised_total_cost;
	DataAccess da = new DataAccess();
	NumberFormat currency = NumberFormat.getCurrencyInstance();
	NumberFormat percentage = NumberFormat.getPercentInstance();
	DecimalFormat nf1 = new DecimalFormat("0.000");
	DecimalFormat nf2 = new DecimalFormat("##,###,##0.00");

	public ActionForward lab1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "";

		initializeRequestInputMap();
		if (validateRequestInputs(request, request.getParameter("ua"))) {
			switch (Integer.parseInt((String) this.RequestIputs.get("ua"))) {
			case 1:
				getListOfLaborRates(request);

				forward_key = "1";
				break;
			case 2:
				forward_key = "2";
				break;
			case 3:
				break;
			case 4:
				forward_key = "";
				break;
			case 5:
				forward_key = "Failed";
			}
		} else {
			forward_key = "test";
		}
		return mapping.findForward(forward_key);
	}

	void getListOfLaborRates(HttpServletRequest request) {
		String columns = "1 as type, cp_country_name, iv_rp_name, avg(lm_pwo_authorised_qty) as lm_pwo_authorised_qty, avg(lm_pwo_authorised_unit_cost) as lm_pwo_authorised_unit_cost, avg(lm_pwo_authorised_total_cost) as lm_pwo_authorised_total_cost";

		String sql = buildInternationalQuery(columns);

		request.setAttribute("rows", getList(sql, breakDownColumns(columns)));
	}

	Vector<String[]> getList(String sql, String[] columns) {
		Vector<String[]> rows = new Vector();
		String group = "";

		int start = 1;
		int end = 0;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				if (!group.equals(rs.getString(columns[this.grouping_index]))) {
					if (!group.equals("")) {
						String[] row = new String[columns.length];

						row[0] = "3";
						row[this.lm_pwo_authorised_qty] = "0.00";
						row[this.lm_pwo_authorised_unit_cost] = "0.00";
						row[this.lm_pwo_authorised_total_cost] = "0.00";
						for (int x = start; x < end; x++) {
							row[this.lm_pwo_authorised_qty] = String
									.valueOf(Double
											.parseDouble(row[this.lm_pwo_authorised_qty])
											+ Double.parseDouble(((String[]) rows
													.get(x))[this.lm_pwo_authorised_qty]));
							((String[]) rows.get(x))[this.lm_pwo_authorised_qty] = this.nf2
									.format(Double.parseDouble(((String[]) rows
											.get(x))[this.lm_pwo_authorised_qty]));

							row[this.lm_pwo_authorised_unit_cost] = String
									.valueOf(Double
											.parseDouble(row[this.lm_pwo_authorised_unit_cost])
											+ Double.parseDouble(((String[]) rows
													.get(x))[this.lm_pwo_authorised_unit_cost]));
							((String[]) rows.get(x))[this.lm_pwo_authorised_unit_cost] = this.nf2
									.format(Double.parseDouble(((String[]) rows
											.get(x))[this.lm_pwo_authorised_unit_cost]));

							row[this.lm_pwo_authorised_total_cost] = String
									.valueOf(Double
											.parseDouble(row[this.lm_pwo_authorised_total_cost])
											+ Double.parseDouble(((String[]) rows
													.get(x))[this.lm_pwo_authorised_total_cost]));
							((String[]) rows.get(x))[this.lm_pwo_authorised_total_cost] = this.nf2
									.format(Double.parseDouble(((String[]) rows
											.get(x))[this.lm_pwo_authorised_total_cost]));
						}
						row[this.lm_pwo_authorised_total_cost] = this.nf2
								.format(Double
										.parseDouble(row[this.lm_pwo_authorised_total_cost])
										/ Double.parseDouble(row[this.lm_pwo_authorised_qty]));
						rows.add(row);
						end++;

						row = new String[columns.length];
						row[0] = "4";
						rows.add(row);
						end++;

						row = new String[columns.length];
						row[0] = "2";
						row[1] = rs.getString(columns[this.grouping_index]);
						rows.add(row);
						end++;
						group = row[1];
						start = end;
					} else {
						String[] row = new String[columns.length];
						row[0] = "2";
						row[1] = rs.getString(columns[this.grouping_index]);
						rows.add(row);
						group = row[1];
						end++;
					}
				}
				String[] row = new String[columns.length];
				for (int i = 0; i < columns.length; i++) {
					row[i] = rs.getString(columns[i]);
				}
				rows.add(row);
				end++;
			}
			String[] row = new String[columns.length];
			row[0] = "3";
			row[this.lm_pwo_authorised_qty] = "0.00";
			row[this.lm_pwo_authorised_unit_cost] = "0.00";
			row[this.lm_pwo_authorised_total_cost] = "0.00";
			for (int x = start; x < end; x++) {
				row[this.lm_pwo_authorised_qty] = String
						.valueOf(Double
								.parseDouble(row[this.lm_pwo_authorised_qty])
								+ Double.parseDouble(((String[]) rows.get(x))[this.lm_pwo_authorised_qty]));
				((String[]) rows.get(x))[this.lm_pwo_authorised_qty] = this.nf2
						.format(Double.parseDouble(((String[]) rows.get(x))[this.lm_pwo_authorised_qty]));

				row[this.lm_pwo_authorised_unit_cost] = String
						.valueOf(Double
								.parseDouble(row[this.lm_pwo_authorised_unit_cost])
								+ Double.parseDouble(((String[]) rows.get(x))[this.lm_pwo_authorised_unit_cost]));
				((String[]) rows.get(x))[this.lm_pwo_authorised_unit_cost] = this.nf2
						.format(Double.parseDouble(((String[]) rows.get(x))[this.lm_pwo_authorised_unit_cost]));

				row[this.lm_pwo_authorised_total_cost] = String
						.valueOf(Double
								.parseDouble(row[this.lm_pwo_authorised_total_cost])
								+ Double.parseDouble(((String[]) rows.get(x))[this.lm_pwo_authorised_total_cost]));
				((String[]) rows.get(x))[this.lm_pwo_authorised_total_cost] = this.nf2
						.format(Double.parseDouble(((String[]) rows.get(x))[this.lm_pwo_authorised_total_cost]));
			}
			row[this.lm_pwo_authorised_total_cost] = this.nf2.format(Double
					.parseDouble(row[this.lm_pwo_authorised_total_cost])
					/ Double.parseDouble(row[this.lm_pwo_authorised_qty]));
			rows.add(row);

			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return rows;
	}

	String buildInternationalQuery(String columns) {
		String sql = "with country (job_id, cp_country_name) as (select distinct lm_js_id, cp_country_name    from lm_job        join lm_site_detail on lm_js_si_id = lm_si_id        join cp_state on lm_si_country = cp_country_id where rtrim(ltrim(isnull(lm_si_country, ''))) not in ('US', 'CA', 'USA', '27103', 'test', 'sdfds', '60638', '')) select "
				+

				columns
				+ "  from lm_purchase_order "
				+ "         join dbo.lm_purchase_work_order on lm_po_id = lm_pwo_po_id "
				+ "         join lm_resource on lm_pwo_rs_id = lm_rs_id "
				+ "         join iv_resource_pool on lm_rs_rp_id = iv_rp_id  "
				+ "         join country on lm_po_js_id = job_id "
				+ " where iv_rp_cns_part_number like '7%' and iv_rp_name not like 'Agent%'  "
				+ "       and isnull(lm_pwo_authorised_total_cost, 0.0) > 0.0 and lm_po_js_id in  "
				+ "(select lm_js_id "
				+ "  from lm_job join lm_site_detail on lm_js_si_id = lm_si_id "
				+ "where rtrim(ltrim(isnull(lm_si_country, ''))) not in ('US', 'CA', 'USA', '27103', 'test', 'sdfds', '60638', '')) "
				+ "group by cp_country_name, iv_rp_name "
				+ "order by cp_country_name, iv_rp_name ";

		return sql;
	}

	boolean validateRequestInputs(HttpServletRequest request, String ua) {
		boolean rc = false;

		Vector<String> inputs = (Vector) this.RequestIputMap.get(ua);

		this.ErrorMessag = "";
		for (int i = 0; i < inputs.size(); i++) {
			String parameter = (String) inputs.get(i);
			String value = validateInput(request.getParameter(parameter));
			this.RequestIputs.put(parameter, value);
			request.setAttribute(parameter, value);
		}
		if (inputs.size() > 0) {
			rc = true;
		}
		return rc;
	}

	String validateInput(String value) {
		if (value == null) {
			value = "";
		}
		return value.trim();
	}

	void initializeRequestInputMap() {
		Vector<String> inputs = new Vector();
		inputs.add("ua");
		this.RequestIputMap.put("1", inputs);
	}

	String[] breakDownColumns(String select) {
		String[] columns = select.split(", ");
		for (int i = 0; i < columns.length; i++) {
			if (columns[i].contains(" as ")) {
				String[] as_col = columns[i].split(" as ");
				columns[i] = as_col[1].trim();
			}
			if (columns[i].equals("cp_country_name")) {
				this.grouping_index = i;
			} else if (columns[i].equals("lm_pwo_authorised_qty")) {
				this.lm_pwo_authorised_qty = i;
			} else if (columns[i].equals("lm_pwo_authorised_unit_cost")) {
				this.lm_pwo_authorised_unit_cost = i;
			} else if (columns[i].equals("lm_pwo_authorised_total_cost")) {
				this.lm_pwo_authorised_total_cost = i;
			}
		}
		return columns;
	}
}
