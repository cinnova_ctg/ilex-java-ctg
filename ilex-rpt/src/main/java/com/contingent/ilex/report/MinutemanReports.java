package com.contingent.ilex.report;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class MinutemanReports extends DispatchAction {
	DataAccess da = new DataAccess();

	public ActionForward mm_summary(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "mm_summary";

		getChartData(request);

		ap_minuteman_pay_history_01(request, "M");

		getMMLatLong(request);

		return mapping.findForward(forward_key);
	}

	public ActionForward sp_summary(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "sp_summary";

		getChartData(request);

		getMMLatLong(request);

		ap_minuteman_pay_history_01(request, "S");

		return mapping.findForward(forward_key);
	}

	public ActionForward sp_current(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "sp_current";
		String display_pay_button = "No";
		if (ap_speedpay_pay_period_02(request).equals("Current")) {
			display_pay_button = determinePayStatus(request
					.getParameter("end_date"));
		}
		request.setAttribute("display_pay_button", display_pay_button);

		return mapping.findForward(forward_key);
	}

	void ap_minuteman_summary_01(HttpServletRequest request) {
		ResultSet rs = this.da.getResultSet("exec ap_minuteman_summary_01");
		try {
			if (rs.next()) {
				request.setAttribute("recruit_last_7",
						rs.getString("recruit_last_7"));
				request.setAttribute("recruit_all", rs.getString("recruit_all"));
				request.setAttribute("used_last_7", rs.getString("used_last_7"));
				request.setAttribute("used_all", rs.getString("used_all"));
				request.setAttribute("cost_last_7", rs.getString("cost_last_7"));
				request.setAttribute("cost_all", rs.getString("cost_all"));
			}
			rs.close();
		} catch (SQLException localSQLException) {
		}
	}

	void ap_minuteman_pay_history_01(HttpServletRequest request, String type) {
		Vector rows = new Vector();
		int i = 0;

		ResultSet rs = this.da
				.getResultSet("exec ap_pay_period_mm_sp_summary_01 '" + type
						+ "'");
		try {
			while (rs.next()) {
				String[] row = new String[7];
				row[0] = rs.getString(1);
				row[1] = rs.getString(2);
				row[2] = rs.getString(3);
				row[3] = rs.getString(4);
				row[4] = rs.getString(5);
				row[5] = rs.getString(6);
				row[6] = rs.getString(7);
				rows.add(i++, row);
			}
			rs.close();
			request.setAttribute("pay_days", rows);
		} catch (SQLException e) {
			System.out.println("Error: " + e);
		}
	}

	public ActionForward mm_pay_day(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "mm_pay_day";

		ap_minuteman_pay_period_01(request);

		return mapping.findForward(forward_key);
	}

	void ap_minuteman_pay_period_01(HttpServletRequest request) {
		Vector rows = new Vector();
		int i = 0;
		String start_date = request.getParameter("start_date");
		String end_date = request.getParameter("end_date");

		String sql = "exec ap_minuteman_pay_period_01 '" + start_date + "', '"
				+ end_date + "'";
		System.out.println(sql);
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[14];
				row[0] = rs.getString(1);
				row[1] = rs.getString(2);
				row[2] = rs.getString(3);
				row[3] = rs.getString(4);
				row[4] = rs.getString(5);
				row[5] = rs.getString(6);
				row[6] = rs.getString(7);
				row[7] = rs.getString(8);
				row[8] = rs.getString(9);
				row[9] = rs.getString(10);
				row[10] = rs.getString(11);
				row[11] = rs.getString(12);
				row[12] = rs.getString(13);
				row[13] = rs.getString(14);

				rows.add(i++, row);
			}
			rs.close();
			request.setAttribute("pay_period", rows);
			request.setAttribute("start_date", start_date);
			request.setAttribute("end_date", end_date);
		} catch (SQLException e) {
			System.out.println(e + " " + "exec ap_minuteman_pay_period_01 '"
					+ start_date + "' '" + end_date + "'");
		}
	}

	public ActionForward mm_pay_day2(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "mm_pay_day2";
		String display_pay_button = "No";
		if (ap_minuteman_pay_period_02(request).equals("Current")) {
			display_pay_button = determinePayStatus(request
					.getParameter("end_date"));
		}
		request.setAttribute("display_pay_button", display_pay_button);

		return mapping.findForward(forward_key);
	}

	String ap_minuteman_pay_period_02(HttpServletRequest request) {
		Vector rows = new Vector();
		int i = 0;
		Map summary = new HashMap();

		String start_date = request.getParameter("start_date");
		String end_date = request.getParameter("end_date");
		String window_type = request.getParameter("window_type");

		int pass = 0;
		String current_division = "";
		String previous_division = "";

		String current_status = "";
		String previous_status = "";

		Double approved = Double.valueOf(0.0D);
		Double labor = Double.valueOf(0.0D);
		Double hardware = Double.valueOf(0.0D);
		Double travel = Double.valueOf(0.0D);

		String sql = "exec ap_minuteman_select_payment_window_01 '"
				+ start_date + "', '" + end_date + "', '" + window_type + "'";
		System.out.println(sql);
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[21];

				current_status = rs.getString(5);

				current_division = rs.getString(3);
				if (!previous_division.equals(current_division)) {
					if (pass > 0) {
						String[] row_summary = new String[4];

						row_summary[0] = NumberFormat.getCurrencyInstance()
								.format(approved);
						row_summary[1] = NumberFormat.getCurrencyInstance()
								.format(labor);
						row_summary[2] = NumberFormat.getCurrencyInstance()
								.format(travel);
						row_summary[3] = NumberFormat.getCurrencyInstance()
								.format(hardware);
						summary.put(previous_status + previous_division,
								row_summary);
					}
					pass++;

					approved = Double.valueOf(0.0D);
					labor = Double.valueOf(0.0D);
					hardware = Double.valueOf(0.0D);
					travel = Double.valueOf(0.0D);
					previous_division = current_division;
				}
				approved = Double.valueOf(approved.doubleValue()
						+ rs.getDouble(14));
				labor = Double.valueOf(labor.doubleValue() + rs.getDouble(17));
				hardware = Double.valueOf(hardware.doubleValue()
						+ rs.getDouble(15));
				travel = Double
						.valueOf(travel.doubleValue() + rs.getDouble(16));

				row[0] = rs.getString(1);
				row[1] = rs.getString(2);
				row[2] = rs.getString(3);
				row[3] = rs.getString(4);
				row[4] = rs.getString(5);
				row[5] = rs.getString(6);
				row[6] = rs.getString(7);
				row[7] = rs.getString(8);
				row[8] = rs.getString(9);
				row[9] = rs.getString(10);
				row[10] = rs.getString(11);
				row[11] = rs.getString(12);
				row[12] = rs.getString(13);
				row[13] = rs.getString(14);
				row[14] = rs.getString(15);
				row[15] = rs.getString(16);
				row[16] = rs.getString(17);
				row[17] = rs.getString(18);

				previous_status = current_status;

				rows.add(i++, row);
			}
			rs.close();

			String[] row_summary = new String[4];
			row_summary[0] = NumberFormat.getCurrencyInstance()
					.format(approved);
			row_summary[1] = NumberFormat.getCurrencyInstance().format(labor);
			row_summary[2] = NumberFormat.getCurrencyInstance().format(travel);
			row_summary[3] = NumberFormat.getCurrencyInstance()
					.format(hardware);
			summary.put(previous_status + previous_division, row_summary);

			request.setAttribute("pay_period", rows);
			request.setAttribute("start_date", start_date);
			request.setAttribute("end_date", end_date);
			request.setAttribute("window_type", window_type);
			request.setAttribute("division_summary", summary);
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
		return window_type;
	}

	String determinePayStatus(String end_date) {
		String pay_status = "No";

		SimpleDateFormat x = new SimpleDateFormat("M/d/y");
		Calendar today = new GregorianCalendar();
		Calendar allow_pay = new GregorianCalendar();

		end_date = "8/20/07";
		try {
			allow_pay.setTimeInMillis(x.parse(end_date).getTime());
		} catch (Exception e) {
			System.out
					.println("Error: Invalid end date passed to determine whether to pay current pay period.");
		}
		if (today.after(allow_pay)) {
			pay_status = "Yes";
		}
		return pay_status;
	}

	public ActionForward mm_current(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "mm_current";
		String display_pay_button = "No";
		if (ap_minuteman_pay_period_02(request).equals("Current")) {
			display_pay_button = determinePayStatus(request
					.getParameter("end_date"));
		}
		request.setAttribute("display_pay_button", display_pay_button);

		return mapping.findForward(forward_key);
	}

	public ActionForward sp_pay_day2(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "sp_pay_day2";

		ap_speedpay_pay_period_02(request);

		return mapping.findForward(forward_key);
	}

	String ap_speedpay_pay_period_02(HttpServletRequest request) {
		Vector rows = new Vector();
		int i = 0;
		Map summary = new HashMap();

		String start_date = request.getParameter("start_date");
		String end_date = request.getParameter("end_date");
		String window_type = request.getParameter("window_type");

		int pass = 0;
		String current_division = "";
		String previous_division = "";

		String current_status = "";
		String previous_status = "";

		Double approved = Double.valueOf(0.0D);
		Double labor = Double.valueOf(0.0D);
		Double hardware = Double.valueOf(0.0D);
		Double travel = Double.valueOf(0.0D);

		String sql = "exec ap_speedpay_select_payment_window_01 '" + start_date
				+ "', '" + end_date + "', '" + window_type + "'";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[21];

				current_status = rs.getString(5);

				current_division = rs.getString(3);
				if (!previous_division.equals(current_division)) {
					if (pass > 0) {
						String[] row_summary = new String[4];

						row_summary[0] = NumberFormat.getCurrencyInstance()
								.format(approved);
						row_summary[1] = NumberFormat.getCurrencyInstance()
								.format(labor);
						row_summary[2] = NumberFormat.getCurrencyInstance()
								.format(travel);
						row_summary[3] = NumberFormat.getCurrencyInstance()
								.format(hardware);
						summary.put(previous_status + previous_division,
								row_summary);
					}
					pass++;

					approved = Double.valueOf(0.0D);
					labor = Double.valueOf(0.0D);
					hardware = Double.valueOf(0.0D);
					travel = Double.valueOf(0.0D);
					previous_division = current_division;
				}
				approved = Double.valueOf(approved.doubleValue()
						+ rs.getDouble(14));
				labor = Double.valueOf(labor.doubleValue() + rs.getDouble(17));
				hardware = Double.valueOf(hardware.doubleValue()
						+ rs.getDouble(15));
				travel = Double
						.valueOf(travel.doubleValue() + rs.getDouble(16));

				row[0] = rs.getString(1);
				row[1] = rs.getString(2);
				row[2] = rs.getString(3);
				row[3] = rs.getString(4);
				row[4] = rs.getString(5);
				row[5] = rs.getString(6);
				row[6] = rs.getString(7);
				row[7] = rs.getString(8);
				row[8] = rs.getString(9);
				row[9] = rs.getString(10);
				row[10] = rs.getString(11);
				row[11] = rs.getString(12);
				row[12] = rs.getString(13);
				row[13] = rs.getString(14);
				row[14] = rs.getString(15);
				row[15] = rs.getString(16);
				row[16] = rs.getString(17);
				row[17] = rs.getString(18);

				previous_status = current_status;

				rows.add(i++, row);
			}
			rs.close();

			String[] row_summary = new String[4];
			row_summary[0] = NumberFormat.getCurrencyInstance()
					.format(approved);
			row_summary[1] = NumberFormat.getCurrencyInstance().format(labor);
			row_summary[2] = NumberFormat.getCurrencyInstance().format(travel);
			row_summary[3] = NumberFormat.getCurrencyInstance()
					.format(hardware);
			summary.put(previous_status + previous_division, row_summary);

			request.setAttribute("pay_period", rows);
			request.setAttribute("start_date", start_date);
			request.setAttribute("end_date", end_date);
			request.setAttribute("window_type", window_type);
			request.setAttribute("division_summary", summary);
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
		return window_type;
	}

	public ActionForward mm_pay_record_payment(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "mm_pay_record_payment";

		ap_minuteman_pay_period_02(request);

		return mapping.findForward(forward_key);
	}

	public ActionForward mm_pay_record_payment_action(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "mm_pay_record_payment_action";

		process_payment_request(request);

		ap_minuteman_pay_period_02(request);

		return mapping.findForward(forward_key);
	}

	void process_payment_request(HttpServletRequest request) {
		String start_date = request.getParameter("start_date");
		String end_date = request.getParameter("end_date");

		String process_po_status = "";
		String error_message = "None.";

		String pay_period = "Failed";
		String[] po_list = request.getParameterValues("lm_po_id");
		if ((po_list != null) && (po_list.length > 0)) {
			pay_period = getPayPeriod("ma_pay_periods_minuteman");
			if (!pay_period.equals("Failed")) {
				process_po_status = processEachPO(po_list, pay_period);
				if (process_po_status.equals("Sucessful")) {
					if (!closePeriod(pay_period)) {
						error_message = "The pay period could not be closed.";
					}
				} else {
					error_message = "All POs were not processed";
				}
			} else {
				error_message = "The pay period was not retrieved";
			}
		} else {
			error_message = "List of POs is missing";
		}
		request.setAttribute("error_message", error_message);
	}

	String getPayPeriod(String table) {
		String sql = "select convert(char(8), min(convert(datetime, ma_mp_period_id)),1) from "
				+ table + " where ma_mp_payment_status = 'Pending'";
		String pay_period = "Failed";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				pay_period = rs.getString(1);
			}
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
		return pay_period;
	}

	String processEachPO(String[] po_list, String pay_period) {
		int initial_count = po_list.length;
		int processed_count = 0;

		String rtn_code = "Failed";

		String sql = "{call ap_minuteman_speedpay_payment_record_action_01 (?,?,?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			for (int i = 0; i < po_list.length; i++) {
				stmt.setObject(1, po_list[i], 12);
				stmt.setObject(2, pay_period, 12);
				stmt.registerOutParameter(3, 12);

				stmt.execute();

				rtn_code = (String) stmt.getObject(3);
				System.out.println("Iteration: " + i + " " + rtn_code);
				if (rtn_code.equals("Failed")) {
					break;
				}
				processed_count++;
			}
			stmt.close();
		} catch (Exception e) {
			rtn_code = "Failed";
			System.out.println(e);
		}
		if (po_list.length == processed_count) {
			rtn_code = "Successful";
		}
		return pay_period;
	}

	boolean closePeriod(String period) {
		boolean rtn_code = false;

		String sql = "{call ap_minuteman_speedpay_payment_record_action_01 (?,?,?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);

			stmt.setObject(1, period, 12);
			stmt.setObject(2, "M", 12);
			stmt.registerOutParameter(3, 12);

			stmt.execute();
			if (((String) stmt.getObject(3)).equals("Successful")) {
				rtn_code = true;
			}
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return rtn_code;
	}

	void getChartData(HttpServletRequest request) {
		Map charts = new HashMap();
		Map parameters = null;

		String sql = "select dw_chart_name, rtrim(dw_chd) as dw_chd, rtrim(dw_chxl1) as dw_chxl1, rtrim(dw_chxl2) as dw_chxl2,rtrim(dw_chxl3) as dw_chxl3 from dw_charts";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				parameters = new HashMap(6);

				parameters.put("dw_chd", rs.getString("dw_chd"));
				parameters.put("dw_chxl1", rs.getString("dw_chxl1"));
				parameters.put("dw_chxl2", rs.getString("dw_chxl2"));
				parameters.put("dw_chxl3", rs.getString("dw_chxl3"));
				charts.put(rs.getString("dw_chart_name"), parameters);
			}
			rs.close();

			request.setAttribute("charts", charts);
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	void getMMLatLong(HttpServletRequest request) {
		Vector latitude = new Vector();
		Vector longitude = new Vector();
		Vector partner = new Vector();
		Vector type = new Vector();

		partner.clear();
		type.clear();
		latitude.clear();
		longitude.clear();

		String sql = "select 'Name', cp_pt_partner_type, lo_ad_latitude, lo_ad_longitude from cp_partner join lo_organization_main on cp_partner_om_id = lo_om_id join lo_address on lo_om_ad_id1 = lo_ad_id ";
		sql = sql
				+ "where (cp_pt_partner_type = 'M' or cp_pt_speedpay_user is not null) and lo_ad_latlon_source in ('Google','Zip Code') order by cp_pt_partner_type";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				partner.add(rs.getString(1));
				type.add(rs.getString(2));
				latitude.add(rs.getString(3));
				longitude.add(rs.getString(4));
			}
			rs.close();

			request.setAttribute("latitude", latitude);
			request.setAttribute("longitude", longitude);
			request.setAttribute("partner", partner);
			request.setAttribute("type", type);
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}
}
