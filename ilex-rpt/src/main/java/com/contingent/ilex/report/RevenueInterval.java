package com.contingent.ilex.report;

import java.util.GregorianCalendar;

public class RevenueInterval {
	Integer NumberOfDays;
	Integer Month;
	Integer Year;
	String allocationDate;
	Double RevenueAllocationRatio;
	Double RevenueAllocation;
	GregorianCalendar date;

	RevenueInterval(GregorianCalendar interval_point, int days) {
		this.NumberOfDays = Integer.valueOf(days);
		this.Month = Integer.valueOf(interval_point.get(2) + 1);
		this.Year = Integer.valueOf(interval_point.get(1));
		this.allocationDate = (this.Month.toString() + "/1/" + this.Year
				.toString());
		this.date = interval_point;
	}
}
