package com.contingent.ilex.report;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class Commission2 extends DispatchAction {
	Map<String, String> JobTypeMap = new HashMap();
	DataAccess da = new DataAccess();

	public ActionForward com_new_data(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String[] link_parameters = buildLinkQS(request);

		getReportData(request, buildWhereClause(request), link_parameters);

		String forward_key = "com_new_data";

		return mapping.findForward(forward_key);
	}

	public ActionForward com_new_data_manager(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String[] link_parameters = buildLinkQS(request);

		getReportData_manager(request, buildWhereClause_manager(request),
				link_parameters);

		String forward_key = "com_new_data_manager";

		return mapping.findForward(forward_key);
	}

	String buildWhereClause(HttpServletRequest request) {
		String where = "where ac_jd_date_closed between ";

		String qtr = determineQuarter(request);
		request.setAttribute("quarter", qtr);
		String w1 = lookupQuarterBoundaries(qtr);

		where = where + w1;

		String w2 = buildJobTypes(request);
		where = where + w2;

		setSubTitles(request);

		return where;
	}

	String buildWhereClause_manager(HttpServletRequest request) {
		String where = "where ac_jd_date_closed between ";
		String manager = "";

		String qtr = determineQuarter(request);
		request.setAttribute("quarter", qtr);
		String w1 = lookupQuarterBoundaries(qtr);

		where = where + w1;

		String type = request.getParameter("type");
		request.setAttribute("type", type);
		if ("bdm".equals(type)) {
			manager = "ac_jd_bdm=" + request.getParameter("ac_jd_bdm");
			request.setAttribute("ac_jd_bdm", request.getParameter("ac_jd_bdm"));
		} else {
			manager = "ac_jd_spm=" + request.getParameter("ac_jd_spm");
			request.setAttribute("ac_jd_spm", request.getParameter("ac_jd_spm"));
		}
		where = where + " and " + manager + " ";

		String w2 = buildJobTypes(request);
		where = where + w2;

		setSubTitles(request);

		return where;
	}

	String determineQuarter(HttpServletRequest request) {
		String quarters = "";

		String input_quarter = request.getParameter("quarter") != null ? request
				.getParameter("quarter") : "";
		if (input_quarter.equals("")) {
			GregorianCalendar qtr = new GregorianCalendar();
      
			int month = qtr.get(2);
			int year = qtr.get(1);
		
			
			int quarter = -1;
			if (month > 8) {
				quarter = 4;
			} else if (month > 5) {
				quarter = 3;
			} else if (month > 2) {
				quarter = 2;
			
			} else {
				
				
				
				
				quarter = 1;
				
				
				
			}
			input_quarter = "Qtr " + String.valueOf(quarter) + " "
					+ String.valueOf(year);
		}
		return input_quarter;
	}

	String lookupQuarterBoundaries(String input_quarter) {
		String quarters = "";
		String[] quarter_date = input_quarter.split(" ");
		String year = quarter_date[2].substring(2);
		if(quarter_date[1].equals("1")){
			quarters = "'01/01/"+year+"' and '03/31/"+year+"'";
		}else if(quarter_date[1].equals("2")){
			quarters = "'04/01/"+year+"' and '06/30/"+year+"'";
		}else if(quarter_date[1].equals("3")){
			quarters = "'07/01/"+year+"' and '09/30/"+year+"'";
		}else if(quarter_date[1].equals("4")){
			quarters = "'10/01/"+year+"' and '12/31/"+year+"'";
		}
		
	
		return quarters;
	}

	String buildJobTypes(HttpServletRequest request) {
		String job_type_list = "";

		String[] job_types = request.getParameterValues("job_types");
		if (request.getParameter("job_type_manager") != null) {
			job_types = request.getParameter("job_type_manager").split(",");
		}
		mapJobTypesSelections();
		if ((job_types != null) && (job_types.length > 0)) {
			for (int i = 0; i < job_types.length - 1; i++) {
				job_type_list = job_type_list
						+ (String) this.JobTypeMap.get(job_types[i]) + " or ";
			}
			job_type_list = job_type_list
					+ (String) this.JobTypeMap
							.get(job_types[(job_types.length - 1)]);
			job_type_list = " and (" + job_type_list + ") ";
		} else {
			job_type_list = buildJobTypesDefaults();
		}
		this.JobTypeMap.clear();

		return job_type_list;
	}

	String buildJobTypesDefaults() {
		String defaults = "";

		defaults = " and ((ac_cl_major2 = 'Standard' and ac_cl_minor2 = 'Billable') or (ac_cl_major2 = 'NetMedX' and ac_cl_minor2 = 'Billable') or (ac_cl_major2 = 'MSP' and (ac_cl_minor2 = 'Billable' or ac_cl_minor_classification = 'EverWorX Operations'))) ";

		return defaults;
	}

	String mapJobTypesSelections() {
		String defaults = "";

		this.JobTypeMap.put("Standard Billable",
				" (ac_cl_major2 = 'Standard' and ac_cl_minor2 = 'Billable') ");
		this.JobTypeMap
				.put("Standard Non-Billable",
						" (ac_cl_major2 = 'Standard' and ac_cl_minor2 = 'Non-Billable') ");
		this.JobTypeMap.put("NetMedX Billable",
				" (ac_cl_major2 = 'NetMedX' and ac_cl_minor2 = 'Billable') ");
		this.JobTypeMap
				.put("NetMedX Non-Billable",
						" (ac_cl_major2 = 'NetMedX' and ac_cl_minor2 = 'Non-Billable') ");
		this.JobTypeMap
				.put("MSP Billable",
						" (ac_cl_major2 = 'MSP' and (ac_cl_minor2 = 'Billable' or ac_cl_minor_classification = 'EverWorX Operations')) ");
		this.JobTypeMap.put("MSP Non-Billable",
				" (ac_cl_major2 = 'MSP' and ac_cl_minor2 = 'Non-Billable') ");

		return defaults;
	}

	void buildDataVectors(String type, String sql, HttpServletRequest request,
			String[] link_parameters) {
		String[] columns = { "occ", "ac_du_name", "pf_exp", "actual_exp",
				"ur1_exp", "ur2_exp", "exp", "jd_exp", "rev", "pf_vgpm",
				"vgpm", "delta", "occ", "NMX_PMPC" };

		String manager = "ac_jd_" + type;
		String individual = "";

		Vector<String[]> rows = new Vector();

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[columns.length];

				individual = rs.getString(1);
				for (int i = 0; i < columns.length; i++) {
					row[i] = rs.getString(columns[i]);
					if (columns[i].equals("ac_du_name")) {
						row[i] = ("<a href='Troll.xo?ac=com_new_data_manager&"
								+ link_parameters[3] + "&type=" + type + "&"
								+ manager + "=" + individual + "&detail=yes'>"
								+ row[i] + "</a>");
					}
				}
				rows.add(row);
			}
			rs.close();

			sumFormatRows(type, columns, rows, request);
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
	}

	void getReportData(HttpServletRequest request, String where,
			String[] link_parameters) {
		String sql1 = "SELECT ac_jd_bdm, ac_du_name, sum(pf_exp) pf_exp, sum(actual_exp) actual_exp, "
				+ " sum(ur1_exp) ur1_exp, sum(ur2_exp) ur2_exp, sum(jd_exp) jd_exp, sum(exp) exp,  "
				+ " sum(rev) rev,  "
				+ " case when sum(rev) > 0.0 then  (1-sum(pf_exp)/sum(rev)) "
				+ " else 0.0 end as pf_vgpm,   "
				+ " case when sum(rev) > 0.0 then  (1-(sum(exp)+sum(NetMedX10Time))/sum(rev)) "
				+ " else 0.0 end as vgpm, "
				+ " case when sum(rev) > 0.0 "
				+ " then  (1-(sum(exp)+sum(NetMedX10Time))/sum(rev))-(1-sum(pf_exp)/sum(rev))else 0.0 "
				+ " end as delta, "
				+ " sum(occ) occ, sum(NetMedX10Time) NMX_PMPC "
				+ " FROM ( select ac_jd_bdm, ac_du_name, sum(ac_ru_pro_forma) as pf_exp,  sum(isnull(ac_fn_po_expense, 0.0)) as actual_exp,  sum(isnull(ac_ur_expense, 0.0)) as ur1_exp, sum(isnull(ac_uo_expense, 0.0)) as ur2_exp, sum(isnull(ac_de_discounted_amount, 0.0)) as jd_exp,  sum(ac_ru_expense) as exp, sum(ac_ru_revenue) as rev, count(*) as occ ,ac_cl_major2, CASE WHEN ac_cl_major2 = 'NetMedX' then count(ac_cl_major2) * 10 ELSE 0  END NetMedX10Time  from datawarehouse.dbo.ac_dimensions_project join datawarehouse.dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id join datawarehouse.dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id join datawarehouse.dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id join datawarehouse.dbo.ac_dimension_user on ac_jd_bdm = ac_du_lo_pc_id left join datawarehouse.dbo.ac_fact_unallocated_expense  on ac_jd_js_id = ac_ur_jd_js_id left join datawarehouse.dbo.ac_fact_unallocated_overhead_expense on ac_jd_js_id = ac_uo_jd_js_id left join datawarehouse.dbo.ac_fact_job_discount_expense on ac_jd_js_id = ac_de_jd_js_id left join datawarehouse.dbo.ac_fact_job_expense on ac_jd_js_id = ac_fn_js_id  "
				+

				where
				+ " "
				+ "group by ac_jd_bdm, ac_du_name, ac_cl_major2 )aaaa "
				+ " group by ac_jd_bdm, ac_du_name " + " order by ac_du_name";

		String sql2 = "select ac_jd_spm, ac_du_name, sum(ac_ru_pro_forma) as pf_exp,  sum(isnull(ac_fn_po_expense, 0.0)) as actual_exp, sum(isnull(ac_ur_expense, 0.0)) as ur1_exp, sum(isnull(ac_uo_expense, 0.0)) as ur2_exp, sum(isnull(ac_de_discounted_amount, 0.0)) as jd_exp,  sum(ac_ru_expense) as exp, sum(ac_ru_revenue) as rev, case when sum(ac_ru_revenue) > 0.0 then  (1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) else 0.0 end as pf_vgpm,  case when sum(ac_ru_revenue) > 0.0 then  (1-sum(ac_ru_expense)/sum(ac_ru_revenue)) else 0.0 end as vgpm,  case when sum(ac_ru_revenue) > 0.0 then  (1-sum(ac_ru_expense)/sum(ac_ru_revenue))-(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue))else 0.0 end as delta, count(*) as occ, 0 NMX_PMPC from datawarehouse.dbo.ac_dimensions_project join datawarehouse.dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id join datawarehouse.dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id join datawarehouse.dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id join datawarehouse.dbo.ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id left join datawarehouse.dbo.ac_fact_unallocated_expense  on ac_jd_js_id = ac_ur_jd_js_id left join datawarehouse.dbo.ac_fact_unallocated_overhead_expense on ac_jd_js_id = ac_uo_jd_js_id left join datawarehouse.dbo.ac_fact_job_discount_expense on ac_jd_js_id = ac_de_jd_js_id left join datawarehouse.dbo.ac_fact_job_expense on ac_jd_js_id = ac_fn_js_id  "
				+

				where
				+ " "
				+ "group by ac_jd_spm, ac_du_name order by ac_du_name";

		buildDataVectors("bdm", sql1, request, link_parameters);

		buildDataVectors("spm", sql2, request, link_parameters);
	}

	void getReportData_manager(HttpServletRequest request, String where,
			String[] link_parameters) {
		String type = request.getParameter("type");

//		String sql = "select lo_pc_last_name+', '+lo_pc_first_name as ac_du_name, sum(ac_ru_pro_forma) as pf_exp,  sum(isnull(ac_fn_po_expense, 0.0)) as actual_exp, " +
//				" sum(isnull(ac_ur_expense, 0.0)) as ur1_exp, sum(isnull(ac_uo_expense, 0.0)) as ur2_exp, " +
//				" sum(isnull(ac_de_discounted_amount, 0.0)) as jd_exp,  sum(ac_ru_expense) as exp, sum(ac_ru_revenue) as rev, " +
//				" case when sum(ac_ru_revenue) > 0.0 then  (1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) else 0.0 end as pf_vgpm, " +
//				" case when sum(ac_ru_revenue) > 0.0 then  (1-sum(ac_ru_expense)/sum(ac_ru_revenue)) " +
//				" else 0.0 end as vgpm,  case when sum(ac_ru_revenue) > 0.0 " +
//				" then  (1-sum(ac_ru_expense)/sum(ac_ru_revenue))-(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue))else 0.0 end as delta, count(*) as occ," +
//				" lo_ot_name, lx_pr_title from ilex.dbo.ap_appendix_list_01" +
//				" join datawarehouse.dbo.ac_dimensions_job on lx_pr_id = ac_jd_pd_pr_id " +
//				" join ilex.dbo.lm_appendix_poc lap on lm_ap_pr_id = ac_jd_pd_pr_id " +
//				" join ilex.dbo.lo_poc on manager = lo_pc_id "+	
//				" join datawarehouse.dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id " +
//				" join datawarehouse.dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id     " +
//				//" join datawarehouse.dbo.ac_dimension_user on manager = ac_du_lo_pc_id " +
//				" left join datawarehouse.dbo.ac_fact_unallocated_expense  on ac_jd_js_id = ac_ur_jd_js_id " +
//				" left join datawarehouse.dbo.ac_fact_unallocated_overhead_expense on ac_jd_js_id = ac_uo_jd_js_id" +
//				" left join datawarehouse.dbo.ac_fact_job_discount_expense on ac_jd_js_id = ac_de_jd_js_id " +
//				" left join datawarehouse.dbo.ac_fact_job_expense on ac_jd_js_id = ac_fn_js_id  "
		String sql = "select ac_du_name, sum(ac_ru_pro_forma) as pf_exp,  sum(isnull(ac_fn_po_expense, 0.0)) as actual_exp, " +
				" sum(isnull(ac_ur_expense, 0.0)) as ur1_exp, sum(isnull(ac_uo_expense, 0.0)) as ur2_exp, " +
				" sum(isnull(ac_de_discounted_amount, 0.0)) as jd_exp,  sum(ac_ru_expense) as exp, sum(ac_ru_revenue) as rev, " +
				" case when sum(ac_ru_revenue) > 0.0 then  (1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) else 0.0 end as pf_vgpm, " +
				" case when sum(ac_ru_revenue) > 0.0 then  (1-sum(ac_ru_expense)/sum(ac_ru_revenue)) " +
				" else 0.0 end as vgpm,  case when sum(ac_ru_revenue) > 0.0 " +
				" then  (1-sum(ac_ru_expense)/sum(ac_ru_revenue))-(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue))else 0.0 end as delta, count(*) as occ," +
				" lo_ot_name, lx_pr_title from ilex.dbo.ap_appendix_list_01" +
				" join datawarehouse.dbo.ac_dimensions_job on lx_pr_id = ac_jd_pd_pr_id " +
				" join datawarehouse.dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id " +
				" join datawarehouse.dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id     " +
				" join datawarehouse.dbo.ac_dimension_user on manager = ac_du_lo_pc_id " +
				" left join datawarehouse.dbo.ac_fact_unallocated_expense  on ac_jd_js_id = ac_ur_jd_js_id " +
				" left join datawarehouse.dbo.ac_fact_unallocated_overhead_expense on ac_jd_js_id = ac_uo_jd_js_id" +
				" left join datawarehouse.dbo.ac_fact_job_discount_expense on ac_jd_js_id = ac_de_jd_js_id " +
				" left join datawarehouse.dbo.ac_fact_job_expense on ac_jd_js_id = ac_fn_js_id  "
				+

				where
				+ " "
				+ "group by lo_ot_name, lx_pr_title, ac_du_name order by lo_ot_name, lx_pr_title, ac_du_name";
		if ("bdm".equals(type)) {
			sql = sql.replace("manager", "ac_jd_bdm");
		} else {
			sql = sql.replace("manager", "ac_jd_spm");
		}
		buildDataVectors_manager("bdm", sql, request, link_parameters);
	}

	void buildDataVectors_manager(String type, String sql,
			HttpServletRequest request, String[] link_parameters) {
		String[] columns = { "occ", "ac_du_name", "pf_exp", "actual_exp",
				"ur1_exp", "ur2_exp", "exp", "jd_exp", "rev", "pf_vgpm",
				"vgpm", "delta", "occ", "lo_ot_name", "lx_pr_title" };

		Vector<String[]> rows = new Vector();

		System.out.println(type + "\n" + sql);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[columns.length];
				for (int i = 0; i < columns.length; i++) {
					row[i] = rs.getString(columns[i]);
				}
				rows.add(row);
			}
			rs.close();

			sumFormatRows_manager(type, columns, rows, request);
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
	}

	void sumFormatRows_manager(String type, String[] columns,
			Vector<String[]> rows, HttpServletRequest request) {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		NumberFormat np = NumberFormat.getPercentInstance();
		NumberFormat nd = new DecimalFormat("0.00");

		int occ = 0;
		int ac_du_name = 1;
		int pf_exp = 2;
		int actual_exp = 3;
		int ur1_exp = 4;
		int ur2_exp = 5;
		int exp = 6;
		int jd_exp = 7;
		int rev = 8;
		int pf_vgpm = 9;
		int vgpm = 10;
		int delta = 11;

		double[] totals = new double[15];
		for (int j = 0; j < totals.length; j++) {
			totals[j] = 0.0D;
		}
		for (int i = 0; i < rows.size(); i++) {
			String[] r = (String[]) rows.get(i);
			if (i == 0) {
				request.setAttribute("individual", r[1]);
			}
			try {
				r[ac_du_name] = (r[13] + " / " + r[14]);
				totals[pf_exp] += Double.parseDouble(r[pf_exp]);
				r[pf_exp] = nf.format(Double.parseDouble(r[pf_exp]));

				totals[actual_exp] += Double.parseDouble(r[actual_exp]);
				r[actual_exp] = nf.format(Double.parseDouble(r[actual_exp]));

				totals[ur1_exp] += Double.parseDouble(r[ur1_exp]);
				r[ur1_exp] = nf.format(Double.parseDouble(r[ur1_exp]));

				totals[ur2_exp] += Double.parseDouble(r[ur2_exp]);
				r[ur2_exp] = nf.format(Double.parseDouble(r[ur2_exp]));

				totals[exp] += Double.parseDouble(r[exp]);
				r[exp] = nf.format(Double.parseDouble(r[exp]));

				totals[jd_exp] += Double.parseDouble(r[jd_exp]);
				r[jd_exp] = nf.format(Double.parseDouble(r[jd_exp]));

				totals[rev] += Double.parseDouble(r[rev]);
				r[rev] = nf.format(Double.parseDouble(r[rev]));

				r[pf_vgpm] = nd.format(Double.parseDouble(r[pf_vgpm]));
				r[vgpm] = nd.format(Double.parseDouble(r[vgpm]));

				rows.set(i, r);
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		String[] r = new String[15];
		r[ac_du_name] = "<b>Totals</b>";
		r[pf_exp] = nf.format(totals[pf_exp]);
		r[actual_exp] = nf.format(totals[actual_exp]);
		r[ur1_exp] = nf.format(totals[ur1_exp]);
		r[ur2_exp] = nf.format(totals[ur2_exp]);
		r[exp] = nf.format(totals[exp]);
		r[jd_exp] = nf.format(totals[jd_exp]);
		r[rev] = nf.format(totals[rev]);
		if (totals[rev] > 0.0D) {
			r[pf_vgpm] = nd.format(1.0D - totals[pf_exp] / totals[rev]);
			r[vgpm] = nd.format(1.0D - totals[exp] / totals[rev]);
		} else {
			r[pf_vgpm] = "";
			r[vgpm] = "";
		}
		rows.add(r);

		request.setAttribute(type, rows);
	}

	void sumFormatRows(String type, String[] columns, Vector<String[]> rows,
			HttpServletRequest request) {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		NumberFormat np = NumberFormat.getPercentInstance();
		NumberFormat nd = new DecimalFormat("0.00");

		int occ = 0;
		int ac_du_name = 1;
		int pf_exp = 2;
		int actual_exp = 3;
		int ur1_exp = 4;
		int ur2_exp = 5;
		int exp = 6;
		int jd_exp = 7;
		int rev = 8;
		int pf_vgpm = 9;
		int vgpm = 10;
		int delta = 11;
		int nmx_pmpc = 13 ;

		double[] totals = new double[14];
		for (int j = 0; j < totals.length; j++) {
			totals[j] = 0.0D;
		}
		for (int i = 0; i < rows.size(); i++) {
			String[] r = (String[]) rows.get(i);
			try {
				r[ac_du_name] = r[1];
				totals[pf_exp] += Double.parseDouble(r[pf_exp]);
				r[pf_exp] = nf.format(Double.parseDouble(r[pf_exp]));

				totals[actual_exp] += Double.parseDouble(r[actual_exp]);
				r[actual_exp] = nf.format(Double.parseDouble(r[actual_exp]));

				totals[ur1_exp] += Double.parseDouble(r[ur1_exp]);
				r[ur1_exp] = nf.format(Double.parseDouble(r[ur1_exp]));

				totals[ur2_exp] += Double.parseDouble(r[ur2_exp]);
				r[ur2_exp] = nf.format(Double.parseDouble(r[ur2_exp]));

				totals[exp] += Double.parseDouble(r[exp]);
				r[exp] = nf.format(Double.parseDouble(r[exp]));

				totals[jd_exp] += Double.parseDouble(r[jd_exp]);
				r[jd_exp] = nf.format(Double.parseDouble(r[jd_exp]));

				totals[rev] += Double.parseDouble(r[rev]);
				r[rev] = nf.format(Double.parseDouble(r[rev]));
				
				totals[nmx_pmpc] += Double.parseDouble(r[nmx_pmpc]);
				r[nmx_pmpc] = nf.format(Double.parseDouble(r[nmx_pmpc]));

				r[pf_vgpm] = nd.format(Double.parseDouble(r[pf_vgpm]));
				r[vgpm] = nd.format(Double.parseDouble(r[vgpm]));

				rows.set(i, r);
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		String[] r = new String[14];
		r[ac_du_name] = "<b>Totals</b>";
		r[pf_exp] = nf.format(totals[pf_exp]);
		r[actual_exp] = nf.format(totals[actual_exp]);
		r[ur1_exp] = nf.format(totals[ur1_exp]);
		r[ur2_exp] = nf.format(totals[ur2_exp]);
		r[exp] = nf.format(totals[exp]);
		r[jd_exp] = nf.format(totals[jd_exp]);
		r[rev] = nf.format(totals[rev]);
		if (totals[rev] > 0.0D) {
			r[pf_vgpm] = nd.format(1.0D - totals[pf_exp] / totals[rev]);
			r[vgpm] = nd.format(1.0D - totals[exp] / totals[rev]);
		} else {
			r[pf_vgpm] = ""; 
			r[vgpm] = "";
		}
		
		r[nmx_pmpc] = nf.format(totals[nmx_pmpc]);
		
		
		rows.add(r);

		request.setAttribute(type, rows);
	}

	String[] buildLinkQS(HttpServletRequest request) {
		String[] query_string = new String[4];
		String delimiter = "";

		query_string[0] = determineQuarter(request);

		String[] job_types = request.getParameterValues("job_types");

		query_string[1] = "";
		if (request.getParameter("job_type_manager") != null) {
			job_types = request.getParameter("job_type_manager").split(",");
		}
		if (job_types != null) {
			for (int i = 0; i < job_types.length; i++) {
				int tmp70_69 = 1;
				String[] tmp70_68 = query_string;
				tmp70_68[tmp70_69] = (tmp70_68[tmp70_69] + delimiter + job_types[i]);
				delimiter = ",";
			}
		} else {
			query_string[1] = "Standard Billable,NetMedX Billable,MSP Billable";
		}
		try {
			query_string[0] = URLEncoder.encode(query_string[0], "UTF8");
			query_string[1] = URLEncoder.encode(query_string[1], "UTF8");
			query_string[2] = ("link=default&quarter=" + query_string[0]
					+ "&job_types=" + query_string[1]);
			query_string[3] = ("quarter=" + query_string[0]
					+ "&job_type_manager=" + query_string[1]);
		} catch (UnsupportedEncodingException localUnsupportedEncodingException) {
		}
		String return_link = "link=default&quarter=" + query_string[0]
				+ "&job_type_manager=" + query_string[1];
		request.setAttribute("return_link", return_link);

		return query_string;
	}

	void setSubTitles(HttpServletRequest request) {
		String delimiter = "";
		String subtitle = "";

		String[] job_types = request.getParameterValues("job_types");
		if (request.getParameter("job_type_manager") != null) {
			job_types = request.getParameter("job_type_manager").split(",");
		}
		if (job_types != null) {
			for (int i = 0; i < job_types.length; i++) {
				subtitle = subtitle + delimiter + job_types[i];
				delimiter = ", ";
			}
		} else {
			subtitle = "Standard Billable, NetMedX Billable, MSP Billable";
		}
		request.setAttribute("subtitle", subtitle);
	}
}
