package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class MapStatus extends DispatchAction {
	DataAccess da = new DataAccess();

	public ActionForward map_status(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "map_status";

		String type = request.getParameter("type");
		if (type == null) {
			type = "0";
		}
		request.setAttribute("type", type);

		request.setAttribute("points", getSites(type));

		return mapping.findForward(forward_key);
	}

	Vector getSites(String type) {
		String sql = "exec wug_map_status_demo_01 " + type;

		Vector<String[]> points = new Vector();

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] pt = new String[2];
				pt[0] = rs.getString(1);
				pt[1] = (rs.getString(2) + ", " + rs.getString(3));
				points.add(pt);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return points;
	}
}
