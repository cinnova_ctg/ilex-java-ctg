package com.contingent.ilex.report;

import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class AccountingAllocation extends DispatchAction {
	public ActionForward mal(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "mal";

		buildAllocationReport(request);

		return mapping.findForward(forward_key);
	}

	void buildAllocationReport(HttpServletRequest request) {
		Vector<String> parameters = initializeReportParameters();
	}

	String[] buildMonthList() {
		String[] months = new String[18];

		return months;
	}

	Vector<String> initializeReportParameters() {
		Vector<String> parameters = new Vector();

		return parameters;
	}

	String getParameterValue() {
		String p = "";

		return p;
	}
}
