package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class CNSReports extends DispatchAction {
	DataAccess da = new DataAccess();
	SimpleDateFormat MMM_YY = new SimpleDateFormat("MMM y");

	public ActionForward weekly_personnel(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "weekly_personnel";
		int count = 0;
		String dept = "";
		int totals = 0;
		Map<String, Vector<String>> organization = getOrganizationStructure();
		Vector<String> dept_details = new Vector();
		for (Map.Entry<String, Vector<String>> dd : organization.entrySet()) {
			dept_details.add((String) dd.getKey());
		}
		System.out.println(dept_details);
		Map<String, String> datasets = new HashMap();
		for (int i = 0; i < dept_details.size(); i++) {
			dept_details.get(i);
			datasets.put((String) dept_details.get(i), "<dataset seriesName='"
					+ (String) dept_details.get(i) + "'>");
		}
		String[] category_dates = getCategoryDates();
		String categories = "<categories>";
		for (int i = 0; i < category_dates.length; i++) {
			categories = categories + " <category Label='" + category_dates[i]
					+ "'/>";
		}
		categories = categories + "</categories>";
		String company_dataseries = "<dataset seriesName='Contingent'>";

		Map<String, Map<String, Map<String, String>>> reportdata = getMonthDepartmentRankCount();
		for (int i = 0; i < category_dates.length; i++) {
			if (reportdata.containsKey(category_dates[i])) {
				Map<String, Map<String, String>> by_dept = (Map) reportdata
						.get(category_dates[i]);
				totals = 0;
				for (int j = 0; j < dept_details.size(); j++) {
					count = 0;
					dept = (String) dept_details.get(j);
					if (by_dept.containsKey(dept)) {
						Map<String, String> dept_ranks_cont = (Map) by_dept
								.get(dept);
						for (Map.Entry<String, String> d : dept_ranks_cont
								.entrySet()) {
							count += Integer.parseInt((String) d.getValue());
						}
					}
					String temp1 = (String) datasets.get(dept);
					temp1 = temp1
							+ " <set value='"
							+ count
							+ "' link='CNSRPT.xo?ac=weekly_personnel_details&in_department="
							+ dept + "' />";
					datasets.put(dept, temp1);
					totals += count;
				}
				company_dataseries = company_dataseries + "<set value='"
						+ totals + "' />";
			}
		}
		for (int i = 0; i < dept_details.size(); i++) {
			dept = (String) dept_details.get(i);
			datasets.put(dept, (String) datasets.get(dept) + "</dataset>");
		}
		for (int i = 0; i < dept_details.size(); i++) {
			dept = (String) dept_details.get(i);
			categories = categories + (String) datasets.get(dept);
		}
		categories = categories + company_dataseries + "</dataset>";
		request.setAttribute("chart_data", categories);
		return mapping.findForward(forward_key);
	}

	public ActionForward weekly_personnel_xls(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "weekly_personnel_xls";
		String sql = "exec ilex.dbo.ac_direct_personnel_xls";
		String first_name = "";
		String last_name = "";
		String title = "";
		String rank = "";
		String role = "";
		String table_rows = "";
		int i = 0;
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				first_name = rs.getString(1);
				last_name = rs.getString(2);
				title = rs.getString(3);
				rank = rs.getString(4);
				role = rs.getString(5);
				table_rows = table_rows + "<tr><td>" + first_name + "</td><td>"
						+ last_name + "</td><td>" + title + "</td><td>" + rank
						+ "</td><td>" + role + "</td></tr>";
			}
			rs.close();
		} catch (Exception localException) {
		}
		request.setAttribute("xls_data", table_rows);
		return mapping.findForward(forward_key);
	}

	public ActionForward weekly_personnel_details(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "weekly_personnel_details";
		String count = "";
		String rank = "";
		String in_department = request.getParameter("in_department");
		String chart = "<chart caption='"
				+ in_department
				+ " Head Count by Rank' xAxisName='Week' yAxisName='Amount' labelDisplay='Rotate'>";
		Map<String, Vector<String>> organization = getOrganizationStructure();
		Map<String, String> datasets = new HashMap();

		Vector<String> dept_ranks = (Vector) organization.get(in_department);
		for (int i = 0; i < dept_ranks.size(); i++) {
			dept_ranks.get(i);
			datasets.put((String) dept_ranks.get(i), "<dataset seriesName='"
					+ (String) dept_ranks.get(i) + "'>");
		}
		String[] category_dates = getCategoryDates();
		String categories = "<categories>";
		for (int i = 0; i < category_dates.length; i++) {
			categories = categories + " <category Label='" + category_dates[i]
					+ "'/>";
		}
		categories = categories + "</categories>";
		Map<String, Map<String, Map<String, String>>> reportdata = getMonthDepartmentRankCount();
		for (int i = 0; i < category_dates.length; i++) {
			if (reportdata.containsKey(category_dates[i])) {
				Map<String, Map<String, String>> by_dept = (Map) reportdata
						.get(category_dates[i]);
				Map<String, String> by_rank = (Map) by_dept.get(in_department);
				for (int j = 0; j < dept_ranks.size(); j++) {
					if (by_rank.containsKey(dept_ranks.get(j))) {
						rank = (String) dept_ranks.get(j);
						count = (String) by_rank.get(dept_ranks.get(j));
					} else {
						rank = (String) dept_ranks.get(j);
						count = "0";
					}
					String temp1 = (String) datasets.get(rank);
					temp1 = temp1 + " <set value='" + count + "' />";
					datasets.put(rank, temp1);
				}
			}
		}
		for (int i = 0; i < dept_ranks.size(); i++) {
			rank = (String) dept_ranks.get(i);
			datasets.put(rank, (String) datasets.get(rank) + "</dataset>");
		}
		for (int i = 0; i < dept_ranks.size(); i++) {
			rank = (String) dept_ranks.get(i);
			categories = categories + (String) datasets.get(rank);
		}
		request.setAttribute("chart_data", chart + categories);
		return mapping.findForward(forward_key);
	}

	String[] getCategoryDates() {
		String[] cat = new String[52];
		int i = 0;
		String sql = "exec ilex.dbo.ac_direct_personnel_52trailing_weeks";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				cat[(i++)] = rs.getString(1);
			}
			rs.close();
		} catch (Exception localException) {
		}
		return cat;
	}

	Map<String, Map<String, Map<String, String>>> getMonthDepartmentRankCount() {
		Map<String, Map<String, Map<String, String>>> by_month = new HashMap();
		Map<String, Map<String, String>> by_department = new HashMap();
		Map<String, String> by_rank = new HashMap();
		String previous = "";
		String current = "";
		String previous_dept = "";
		String current_dept = "";
		String sql = "exec ilex.dbo.ac_direct_personnel_details";
		try {
			ResultSet rs = this.da.getResultSet(sql);
			if (rs.next()) {
				current = rs.getString("window_start");
				previous = rs.getString("window_start");
				previous_dept = rs.getString("department");
				current_dept = rs.getString("department");
				by_rank.put(rs.getString("rank"), rs.getString("count"));
			}
			while (rs.next()) {
				current = rs.getString("window_start");
				current_dept = rs.getString("department");
				if (!current_dept.equals(previous_dept)) {
					by_department.put(previous_dept, by_rank);
					by_rank = new HashMap();
					previous_dept = current_dept;
				}
				if (!current.equals(previous)) {
					by_department.put(previous_dept, by_rank);
					by_month.put(previous, by_department);
					by_department = new HashMap();
					by_rank = new HashMap();
					previous = current;
					previous_dept = current_dept;
				}
				by_rank.put(rs.getString("rank"), rs.getString("count"));
			}
			rs.close();
		} catch (Exception localException) {
		}
		return by_month;
	}

	Map<String, Vector<String>> getOrganizationStructure() {
		Map<String, Vector<String>> organization = new HashMap();
		Vector<String> ranks = new Vector();
		String previous = "";
		String current = "";
		String sql = "SELECT lo_cl_role, lo_ra_rank_value FROM dbo.lo_classification JOIN dbo.lo_rank ON lo_cl_id = lo_ra_cl_id ORDER BY lo_cl_role, lo_ra_rank_value";
		try {
			ResultSet rs = this.da.getResultSet(sql);
			if (rs.next()) {
				current = rs.getString("lo_cl_role");
				previous = rs.getString("lo_cl_role");
				ranks.add(rs.getString("lo_ra_rank_value"));
			}
			while (rs.next()) {
				current = rs.getString("lo_cl_role");
				if (!current.equals(previous)) {
					organization.put(previous, ranks);
					ranks = new Vector();
					previous = current;
				}
				ranks.add(rs.getString("lo_ra_rank_value"));
			}
			organization.put(current, ranks);
			rs.close();
		} catch (Exception localException) {
		}
		return organization;
	}
}
