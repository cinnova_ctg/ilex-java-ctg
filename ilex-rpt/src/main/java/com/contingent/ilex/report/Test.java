package com.contingent.ilex.report;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class Test extends DispatchAction {
	public ActionForward topLevelReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "test";

		SimpleDateFormat df = new SimpleDateFormat("M/d/y m:s.S");
		String hit_time = df.format(new Date());
		request.setAttribute("hit_time", hit_time);
		System.out.println("Submitted-not XX");

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String userId = (String) session.getAttribute("userid");

		System.out.println("User ID: " + userId);

		return mapping.findForward(forward_key);
	}
}
