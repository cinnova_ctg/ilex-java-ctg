package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class QuarterlyCommission extends DispatchAction {
	String quarter;
	String page_type;
	String manager;
	String manager_type;
	DataAccess da = new DataAccess();
	NumberFormat currency = NumberFormat.getCurrencyInstance();
	NumberFormat percentage = NumberFormat.getPercentInstance();
	DecimalFormat nf1 = new DecimalFormat("0.000");
	DecimalFormat nf2 = new DecimalFormat("##,###,##0.00");

	public ActionForward qrtcom1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "summary1";

		this.quarter = (request.getParameter("quarter") != null ? request
				.getParameter("quarter").trim() : "");
		if (this.quarter.trim().equals("")) {
			this.quarter = "<font color=\"red\"> - Quarter Not Selected</font>";
		} else {
			validateRequestParameters(request);
			if (this.page_type.equals("summary1")) {
				buildSummaryCommissionReport1(request);
				forward_key = "summary1";
			} else if (this.page_type.equals("summary2")) {
				buildSummaryCommissionReport2(request);
				forward_key = "summary2";
			} else if (this.page_type.equals("detail1")) {
				buildDetailedCommissionReport1(request);
				forward_key = "detail1";
			}
		}
		request.setAttribute("quarter", this.quarter);
		request.setAttribute("page_type", this.page_type);
		request.setAttribute("manager", this.manager);
		request.setAttribute("manager_type", this.manager_type);

		return mapping.findForward(forward_key);
	}

	void validateRequestParameters(HttpServletRequest request) {
		this.page_type = (request.getParameter("page_type") != null ? request
				.getParameter("page_type").trim() : "");
		this.manager = (request.getParameter("manager") != null ? request
				.getParameter("manager").trim() : "");
		this.manager_type = (request.getParameter("manager_type") != null ? request
				.getParameter("manager_type").trim() : "");
		if (this.page_type.equals("")) {
			this.page_type = "summary1";
			this.manager = "";
			this.manager_type = "";
		} else if (this.page_type.equals("summary1")) {
			this.manager = "";
			this.manager_type = "";
		} else if (this.page_type.equals("summary2")) {
			this.manager = "";
			this.manager_type = "";
		} else if ((this.manager.equals("")) || (this.manager_type.equals(""))) {
			this.page_type = "summary1";
		} else {
			this.page_type = "detail1";
		}
	}

	boolean buildSummaryCommissionReport2(HttpServletRequest request) {
		boolean rc = false;

		String columns = "cm_qt_classification, cm_qt_type, cm_qt_name, sum(cm_qt_pf_cost) as cm_qt_pf_cost, sum(cm_qt_authorized_cost) as cm_qt_authorized_cost, sum(cm_qt_approved_cost) as cm_qt_approved_cost, sum(cm_qt_actual_cost) as cm_qt_actual_cost, sum(cm_qt_unallocated_ctr) as cm_qt_unallocated_ctr, sum(cm_qt_unallocated_pmcl) as cm_qt_unallocated_pmcl, sum((cm_qt_actual_cost+cm_qt_unallocated_ctr+cm_qt_unallocated_pmcl)) as corrected_cost, sum(cm_qt_msp_redundant_cost) as cm_qt_msp_redundant_cost, sum(cm_qt_revenue) as cm_qt_revenue, case when sum(cm_qt_revenue) > 0.0 then (1-sum(cm_qt_pf_cost)/sum(cm_qt_revenue)) else 0.0 end as pf_vgpm, case when sum(cm_qt_revenue) > 0.0 then (1-sum((cm_qt_actual_cost+cm_qt_unallocated_ctr+cm_qt_unallocated_pmcl))/sum(cm_qt_revenue)) else 0.0 end as vgpm";

		String sql = "select "
				+ columns
				+ " from datawarehouse.dbo.com_quarterly_manager_commission_summary "
				+ " where cm_qt_quarter = '" + this.quarter + "' " +

				" group by cm_qt_classification, cm_qt_type, cm_qt_name "
				+ " order by cm_qt_classification, cm_qt_type, cm_qt_name";

		Vector<String[]> rows = getRowData(breakDownColumns(columns), sql);

		breakoutData(request, rows);

		return rc;
	}

	void breakoutData(HttpServletRequest request, Vector<String[]> rows) {
		Vector<String[]> breakout = new Vector();
		Map<String, Vector<String[]>> classifications = new HashMap();

		String classification = ((String[]) rows.get(0))[0];
		for (int i = 0; i < rows.size(); i++) {
			if (!((String[]) rows.get(i))[0].equals(classification)) {
				classifications.put(classification, breakout);
				breakout = new Vector();
				classification = ((String[]) rows.get(i))[0];
			}
			breakout.add((String[]) rows.get(i));
		}
		if (breakout.size() > 0) {
			classifications.put(classification, breakout);
		}
		for (Map.Entry<String, Vector<String[]>> entry : classifications
				.entrySet()) {
			breakAndSumByRole(request, (String) entry.getKey(),
					(Vector) entry.getValue());
		}
	}

	void breakAndSumByRole(HttpServletRequest request, String classification,
			Vector<String[]> rows) {
		Vector<String[]> breakout = new Vector();

		String role = ((String[]) rows.get(0))[1];
		for (int i = 0; i < rows.size(); i++) {
			if (!((String[]) rows.get(i))[1].equals(role)) {
				request.setAttribute(classification + ":" + role,
						sumManagers2(breakout, classification));
				breakout = new Vector();
				role = ((String[]) rows.get(i))[1];
			}
			breakout.add((String[]) rows.get(i));
		}
		if (breakout.size() > 0) {
			request.setAttribute(classification + ":" + role,
					sumManagers2(breakout, classification));
		}
	}

	Vector<String[]> sumManagers2(Vector<String[]> rows, String type) {
		double[] sum = { 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D,
				0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D };
		String[] formatted_sum = new String[15];
		for (int i = 0; i < rows.size(); i++) {
			String[] r = (String[]) rows.get(i);
			if ((type.equals("EverWorX-Deployment"))
					|| (type.equals("EverWorX-Help Desk"))) {
				try {
					r[12] = this.nf2.format(Double.parseDouble(r[11])
							- Double.parseDouble(r[9]));
				} catch (IllegalArgumentException e) {
					r[12] = "**";
				}
			}
			for (int j = 3; j < 12; j++) {
				sum[j] += Double.parseDouble(r[j]);
				r[j] = this.nf2.format(Double.parseDouble(r[j]));
			}
		}
		for (int k = 3; k < 12; k++) {
			formatted_sum[k] = this.currency.format(sum[k]);
		}
		if ((!type.equals("EverWorX-Deployment"))
				&& (!type.equals("EverWorX-Help Desk"))) {
			sum[12] = (1.0D - sum[3] / sum[11]);
			sum[13] = (1.0D - sum[9] / sum[11]);

			formatted_sum[12] = this.nf1.format(sum[12]);
			formatted_sum[13] = this.nf1.format(sum[13]);
		} else {
			sum[12] = (sum[11] - sum[9]);
			formatted_sum[12] = this.nf2.format(sum[12]);
		}
		rows.add(formatted_sum);

		return rows;
	}

	boolean buildDetailedCommissionReport1(HttpServletRequest request) {
		boolean rc = false;

		String columns = "cm_qt_type, cm_qt_name, cm_qt_customer, cm_qt_project, cm_qt_classification, cm_qt_pf_cost as cm_qt_pf_cost, cm_qt_authorized_cost as cm_qt_authorized_cost, cm_qt_approved_cost as cm_qt_approved_cost, cm_qt_actual_cost as cm_qt_actual_cost, cm_qt_unallocated_ctr as cm_qt_unallocated_ctr, cm_qt_unallocated_pmcl as cm_qt_unallocated_pmcl, (cm_qt_actual_cost+cm_qt_unallocated_ctr+cm_qt_unallocated_pmcl) as corrected_cost, cm_qt_revenue as cm_qt_revenue, case when cm_qt_revenue > 0.0 then (1-cm_qt_pf_cost/cm_qt_revenue) else 0.0 end as pf_vgpm, case when cm_qt_revenue > 0.0 then (1-(cm_qt_actual_cost+cm_qt_unallocated_ctr+cm_qt_unallocated_pmcl)/cm_qt_revenue) else 0.0 end as vgpm";

		String sql = "select "
				+ columns
				+ "  from datawarehouse.dbo.com_quarterly_manager_commission_summary "
				+ " where cm_qt_quarter = '"
				+ this.quarter
				+ "' "
				+ "       and cm_qt_classification in ('Project', 'NetMedX', 'EverWorX-Deployment') "
				+ " order by cm_qt_type, cm_qt_name, cm_qt_customer, cm_qt_project, cm_qt_classification";

		Vector<String[]> rows = getRowData(breakDownColumns(columns), sql);

		request.setAttribute("rows", rows);

		return rc;
	}

	boolean buildSummaryCommissionReport1(HttpServletRequest request) {
		boolean rc = false;

		String columns = "cm_qt_type, cm_qt_name, sum(cm_qt_pf_cost) as cm_qt_pf_cost, sum(cm_qt_authorized_cost) as cm_qt_authorized_cost, sum(cm_qt_approved_cost) as cm_qt_approved_cost, sum(cm_qt_actual_cost) as cm_qt_actual_cost, sum(cm_qt_unallocated_ctr) as cm_qt_unallocated_ctr, sum(cm_qt_unallocated_pmcl) as cm_qt_unallocated_pmcl, sum((cm_qt_actual_cost+cm_qt_unallocated_ctr+cm_qt_unallocated_pmcl)) as corrected_cost, sum(cm_qt_revenue) as cm_qt_revenue, case when sum(cm_qt_revenue) > 0.0 then (1-sum(cm_qt_pf_cost)/sum(cm_qt_revenue)) else 0.0 end as pf_vgpm, case when sum(cm_qt_revenue) > 0.0 then (1-sum((cm_qt_actual_cost+cm_qt_unallocated_ctr+cm_qt_unallocated_pmcl))/sum(cm_qt_revenue)) else 0.0 end as vgpm";

		String sql = "select "
				+ columns
				+ " from datawarehouse.dbo.com_quarterly_manager_commission_summary "
				+ " where cm_qt_quarter = '"
				+ this.quarter
				+ "' "
				+ "       and cm_qt_classification in ('Project', 'NetMedX', 'EverWorX-Operations') "
				+ " group by cm_qt_type, cm_qt_name "
				+ " order by cm_qt_type, cm_qt_name";

		Vector<String[]> rows = getRowData(breakDownColumns(columns), sql);

		breakoutManagers(request, rows);

		return rc;
	}

	void breakoutManagers(HttpServletRequest request, Vector<String[]> rows) {
		Vector<String[]> bdm = new Vector();
		Vector<String[]> spm = new Vector();
		for (int i = 0; i < rows.size(); i++) {
			String[] r = (String[]) rows.get(i);
			if (r[0].equals("BDM")) {
				bdm.add(r);
			} else {
				spm.add(r);
			}
		}
		request.setAttribute("bdm", bdm);
		request.setAttribute("spm", spm);

		sumManagers(request, bdm, "bdm_total");
		sumManagers(request, spm, "spm_total");
	}

	void sumManagers(HttpServletRequest request, Vector<String[]> rows,
			String type) {
		double[] sum = { 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D,
				0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D };
		String[] formatted_sum = new String[12];
		for (int i = 0; i < rows.size(); i++) {
			String[] r = (String[]) rows.get(i);
			for (int j = 2; j < 10; j++) {
				sum[j] += Double.parseDouble(r[j]);
				r[j] = this.nf2.format(Double.parseDouble(r[j]));
			}
		}
		sum[10] = (1.0D - sum[2] / sum[9]);
		sum[11] = (1.0D - sum[8] / sum[9]);
		for (int k = 2; k < 10; k++) {
			formatted_sum[k] = this.currency.format(sum[k]);
		}
		formatted_sum[10] = this.nf1.format(sum[10]);
		formatted_sum[11] = this.nf1.format(sum[11]);

		request.setAttribute(type, formatted_sum);
	}

	Vector<String[]> getRowData(String[] columns, String sql) {
		Vector<String[]> result_set = new Vector();

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[columns.length];
				for (int i = 0; i < columns.length; i++) {
					row[i] = rs.getString(columns[i]);
				}
				result_set.add(row);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			result_set.clear();
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return result_set;
	}

	String[] breakDownColumns(String select) {
		String[] columns = select.split(", ");
		for (int i = 0; i < columns.length; i++) {
			if (columns[i].contains(" as ")) {
				String[] as_col = columns[i].split(" as ");
				columns[i] = as_col[1].trim();
			}
		}
		return columns;
	}
}
