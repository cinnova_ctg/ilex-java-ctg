package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class TopLevel extends DispatchAction {
	DataAccess da = new DataAccess();
	String project_type;
	String month;
	String year;

	public ActionForward topLevelReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = executeTopLevelSummary(request);

		return mapping.findForward(forward_key);
	}

	String executeTopLevelSummary(HttpServletRequest request) {
		String forward_key = "XXX";

		getReportParameters(request);
		getReportData(formatSQL(), request);
		getChartData(request);
		return forward_key;
	}

	void getReportParameters(HttpServletRequest request) {
		this.project_type = formatRequestParameter(request
				.getParameter("project_type"));
		request.setAttribute("project_type", this.project_type);

		this.month = formatRequestParameter(request.getParameter("month"));
		request.setAttribute("month", this.month);

		this.year = formatRequestParameter(request.getParameter("year"));
		request.setAttribute("year", this.year);
	}

	String formatRequestParameter(String parameter) {
		if (parameter == null) {
			parameter = "";
		}
		return parameter;
	}

	String formatSQL() {
		String where1 = null;
		String where2 = null;
		int where_clause_count = 0;

		String sql_command = "SELECT ap_tl_status, sum(ap_tl_estimated_cost) estimated, sum(ap_tl_actual_cost) actual,  sum(ap_tl_price) price, sum(ap_tl_count) as count FROM  ap_top_level_summary ";
		if (!this.project_type.equals("all")) {
			if (this.project_type.equals("deployments")) {
				where1 = "ap_tl_pr_type != 3";
			} else if (this.project_type.equals("netmedx")) {
				where1 = "ap_tl_pr_type =3 ";
			}
			where_clause_count++;
		}
		if ((!this.month.equals("")) && (!this.year.equals(""))) {
			where2 = "ap_tl_month_year = '" + this.month + "/1/" + this.year
					+ "'";
			where_clause_count++;
		}
		if (where_clause_count > 0) {
			sql_command = sql_command + "where ";
			if (where1 != null) {
				sql_command = sql_command + where1 + " ";
				if (where2 != null) {
					sql_command = sql_command + "and " + where2 + " ";
				}
			} else {
				sql_command = sql_command + where2 + " ";
			}
		}
		sql_command = sql_command + "group by ap_tl_status";
		System.out.println(sql_command);
		return sql_command;
	}

	void getReportData(String sql, HttpServletRequest request) {
		Map summary = new HashMap();
		Map statusTrack = initializeTracking();
		Double temp_vgp = Double.valueOf(0.0D);

		Double estimated = Double.valueOf(0.0D);
		Double actual = Double.valueOf(0.0D);
		Double price = Double.valueOf(0.0D);
		Integer count = Integer.valueOf(0);

		Double rec_estimated = Double.valueOf(0.0D);
		Double rec_actual = Double.valueOf(0.0D);
		Double rec_price = Double.valueOf(0.0D);
		Integer rec_count = Integer.valueOf(0);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				Vector<Comparable> x = new Vector(4);

				String status = rs.getString("ap_tl_status");

				x.add(0, formatCurrencyValues(Double.valueOf(rs
						.getDouble("estimated"))));
				x.add(1, formatCurrencyValues(Double.valueOf(rs
						.getDouble("actual"))));
				x.add(2, formatCurrencyValues(Double.valueOf(rs
						.getDouble("price"))));
				if (rs.getDouble("price") > 0.0D) {
					temp_vgp = Double.valueOf(1.0D - rs.getDouble("actual")
							/ rs.getDouble("price"));
				} else {
					temp_vgp = Double.valueOf(0.0D);
				}
				x.add(3, formatNumericValues(temp_vgp));
				x.add(4, Integer.valueOf(rs.getInt("count")));
				if (!status.equals("Reconciled")) {
					estimated = Double.valueOf(estimated.doubleValue()
							+ rs.getDouble("estimated"));
					actual = Double.valueOf(actual.doubleValue()
							+ rs.getDouble("actual"));
					price = Double.valueOf(price.doubleValue()
							+ rs.getDouble("price"));
					count = Integer.valueOf(count.intValue()
							+ rs.getInt("count"));
				} else {
					rec_estimated = Double.valueOf(rs.getDouble("estimated"));
					rec_actual = Double.valueOf(rs.getDouble("actual"));
					rec_price = Double.valueOf(rs.getDouble("price"));
					rec_count = Integer.valueOf(rec_count.intValue()
							+ rs.getInt("count"));
				}
				statusTrack.put(status, Integer.valueOf(1));
				summary.put(status, x);
			}
			rs.close();

			summary = addMissingStatus(statusTrack, summary);

			Vector x = new Vector(5);
			x.add(0, formatCurrencyValues(estimated));
			x.add(1, formatCurrencyValues(actual));
			x.add(2, formatCurrencyValues(price));
			if (price.doubleValue() > 0.0D) {
				temp_vgp = Double.valueOf(1.0D - actual.doubleValue()
						/ price.doubleValue());
			} else {
				temp_vgp = Double.valueOf(0.0D);
			}
			x.add(3, formatNumericValues(temp_vgp));
			x.add(4, count);
			summary.put("Overall", x);

			Vector z = new Vector(5);
			z.add(0,
					formatPercentageValues(Double.valueOf(rec_estimated
							.doubleValue() / estimated.doubleValue())));
			z.add(1,
					formatPercentageValues(Double.valueOf(rec_actual
							.doubleValue() / actual.doubleValue())));
			z.add(2,
					formatPercentageValues(Double.valueOf(rec_price
							.doubleValue() / price.doubleValue())));
			z.add(3, "TBD");
			Integer c = count;
			Integer rc = rec_count;
			Double rc_avg = Double.valueOf(rc.doubleValue() / c.doubleValue());
			z.add(4, formatPercentageValues(rc_avg));
			summary.put("Reconciled_Percentage", z);

			request.setAttribute("Summary", summary);
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	void getChartData(HttpServletRequest request) {
		Map charts = new HashMap();
		Map parameters = null;

		String sql = "select dw_chart_name, rtrim(dw_chd) as dw_chd, rtrim(dw_chxl1) as dw_chxl1, rtrim(dw_chxl2) as dw_chxl2,rtrim(dw_chxl3) as dw_chxl3 from dw_charts";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				parameters = new HashMap();

				parameters.put("dw_chd", rs.getString("dw_chd"));
				parameters.put("dw_chxl1", rs.getString("dw_chxl1"));
				parameters.put("dw_chxl2", rs.getString("dw_chxl2"));
				parameters.put("dw_chxl3", rs.getString("dw_chxl3"));
				charts.put(rs.getString("dw_chart_name"), parameters);
			}
			rs.close();

			request.setAttribute("charts", charts);
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	Map initializeTracking() {
		Map statuses = new HashMap();

		statuses.put("In Work - TBS", Integer.valueOf(0));
		statuses.put("In Work - Overdue", Integer.valueOf(0));
		statuses.put("In Work - Scheduled", Integer.valueOf(0));
		statuses.put("In Work - Onsite", Integer.valueOf(0));
		statuses.put("In Work - Offsite", Integer.valueOf(0));
		statuses.put("Complete", Integer.valueOf(0));
		statuses.put("Closed", Integer.valueOf(0));
		statuses.put("Overall", Integer.valueOf(0));
		statuses.put("Reconciled", Integer.valueOf(0));
		statuses.put("Reconciled_Percentage", Integer.valueOf(0));

		return statuses;
	}

	Map addMissingStatus(Map found, Map summary) {
		Set keys = found.keySet();
		Iterator keyIter = keys.iterator();
		while (keyIter.hasNext()) {
			Object status = keyIter.next();
			Integer used = (Integer) found.get(status);
			if (used.intValue() == 0) {
				Vector blank = new Vector();
				blank.add(0, "");
				blank.add(1, "");
				blank.add(2, "");
				blank.add(3, "");
				blank.add(4, "");
				summary.put(status, blank);
			}
		}
		return summary;
	}

	String formatCurrencyValues(Double value) {
		NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
		String formattedValue = nf.format(value);

		return formattedValue;
	}

	String formatPercentageValues(Double value) {
		NumberFormat nf = NumberFormat.getPercentInstance(Locale.US);
		String formattedValue = nf.format(value);

		return formattedValue;
	}

	String formatNumericValues(Double value) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		String formattedValue = nf.format(value);

		return formattedValue;
	}

	public static void main(String[] args) {
	}
}
