package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class VGP extends DispatchAction {
	DataAccess da = new DataAccess();
	String PageLevel = "1";
	Vector sortOrder = new Vector();
	Map sortDirection = new HashMap();

	public ActionForward vgp1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "vgp";

		managePageRequest(request);

		return mapping.findForward(forward_key);
	}

	void managePageRequest(HttpServletRequest request) {
		this.sortOrder.clear();
		this.sortDirection.clear();

		Map inputs = getInputParameters(request);

		String sql = buildQuery(request, inputs);

		vgp_customer(request, sql);
	}

	Map getInputParameters(HttpServletRequest request) {
		Vector requestParameters = initializeInputParameters();

		Map inputs = new HashMap();
		Map sort_input = new HashMap();
		for (int i = 0; i < requestParameters.size(); i++) {
			String parameter = (String) requestParameters.get(i);
			String value = request.getParameter(parameter);
			if (parameter.equals("sort_input")) {
				if ((value != null) && (value.trim().length() > 0)) {
					getSortInput(value.trim());
				}
			} else if ((parameter.equals("sort")) && (value != null)) {
				this.sortOrder.add(0, value);
				if (this.sortOrder.size() > 4) {
					this.sortOrder.setSize(4);
				}
				this.sortDirection.put(value, "asc");
			} else if ((value != null) && (value.trim().length() > 0)) {
				request.setAttribute(parameter, value);
				if (value.contains("'")) {
					value = value.replace("'", "''");
				}
				inputs.put(parameter, value);
			}
			System.out.println("P/V: " + parameter + " / " + value);
		}
		return inputs;
	}

	void getSortInput(String is) {
		String[] sort_list = is.split(",");
		for (int i = sort_list.length - 1; i >= 0; i--) {
			this.sortOrder.add(0, sort_list[i]);
		}
	}

	String buildQuery(HttpServletRequest request, Map inputs) {
		boolean customer = inputs.get("customer") != null;
		boolean project = (inputs.get("lx_pr_id") != null)
				&& (!((String) inputs.get("lx_pr_id")).equals(""));
		boolean from_date = (inputs.get("from_date") != null)
				&& (!((String) inputs.get("from_date")).equals(""));
		boolean to_date = (inputs.get("to_date") != null)
				&& (!((String) inputs.get("to_date")).equals(""));
		boolean lx_pr_cns_poc = (inputs.get("lx_pr_cns_poc") != null)
				&& (((String) inputs.get("lx_pr_cns_poc")).length() > 0);
		boolean low_vgp = (inputs.get("low_vgp") != null)
				&& (((String) inputs.get("low_vgp")).length() > 0);

		String sql = "";
		String common_columns1 = "";
		String common_columns2 = ", sum(ap_jl_price) as revenue, sum(ap_jl_price)-sum(ap_jl_actual_cost) as vgp, 1-sum(ap_jl_actual_cost)/sum(ap_jl_price) as vgpm, count(*) as jobs, sum(ap_jl_actual_cost) as cost ";

		String from = "from ap_job_level_summary join lx_appendix_main on ap_jl_pr_id = lx_pr_id join lo_poc on lx_pr_cns_poc = lo_pc_id ";
		String where = "where ap_jl_status = 'Closed' ";

		from = "from ap_job_level_summary_vw01 ";
		where = "where ap_jl_price > -9999999.0 ";

		String having = "having sum(ap_jl_price) > 0.0 ";
		String order_by = "";
		if ((customer) && (!project)) {
			common_columns1 = "ap_jl_ot_name, ap_jl_pr_title, ap_jl_pr_id, lo_pc_last_name, ap_jl_supervisor";
			where = where + " and ap_jl_ot_name = '"
					+ (String) inputs.get("customer") + "' ";
			this.PageLevel = "2";
		} else if (project) {
			common_columns1 = "ap_jl_ot_name, ap_jl_pr_title, ap_jl_pr_id, lo_pc_last_name, ap_jl_supervisor";
			where = where + " and ap_jl_pr_id = "
					+ (String) inputs.get("lx_pr_id") + " ";
			this.PageLevel = "3";
		} else {
			common_columns1 = "ap_jl_ot_name, ap_jl_pr_title, ap_jl_supervisor, lo_pc_last_name ";
			this.PageLevel = "1";
		}
		if (lx_pr_cns_poc) {
			where = where + " and lx_pr_cns_poc = "
					+ inputs.get("lx_pr_cns_poc");
		}
		if ((from_date) && (to_date)) {
			where = where + "and (ap_jl_month_year >= '"
					+ (String) inputs.get("from_date")
					+ "' and ap_jl_month_year <= '"
					+ (String) inputs.get("to_date") + "') ";
		} else if (from_date) {
			where = where + "and (ap_jl_month_year >= '"
					+ (String) inputs.get("from_date") + "') ";
		} else if (to_date) {
			where = where + "and (ap_jl_month_year <= '"
					+ (String) inputs.get("to_date") + "') ";
		}
		if (low_vgp) {
			having = having
					+ "and 1-sum(ap_jl_actual_cost)/sum(ap_jl_price) < "
					+ (String) inputs.get("low_vgp") + " ";
		}
		if (this.sortOrder.size() > 0) {
			order_by = getOrderByList();
			request.setAttribute("sort_input", order_by);
		} else {
			order_by = common_columns1;
			request.removeAttribute("sort_input");
		}
		sql = "select " + common_columns1 + common_columns2 + from + where
				+ "group by " + common_columns1 + " " + having + "order by "
				+ order_by;
		System.out.println(sql);
		return sql;
	}

	Vector initializeInputParameters() {
		Vector inputParameters = new Vector();

		inputParameters.clear();
		inputParameters.add(0, "sort_input");
		inputParameters.add(1, "customer");
		inputParameters.add(2, "lx_pr_id");
		inputParameters.add(3, "from_date");
		inputParameters.add(4, "to_date");
		inputParameters.add(5, "lx_pr_cns_poc");
		inputParameters.add(6, "sort");
		inputParameters.add(7, "low_vgp");

		return inputParameters;
	}

	String getOrderByList() {
		String order = "";
		String next_sort_parameter = "";
		String delimiter = "";
		Map current_sort_list = new HashMap();
		for (int i = 0; i < this.sortOrder.size(); i++) {
			next_sort_parameter = (String) this.sortOrder.get(i);
			if (!current_sort_list.containsKey(next_sort_parameter)) {
				current_sort_list.put(next_sort_parameter, "");
				order = order + delimiter + next_sort_parameter;
				delimiter = ",";
			}
		}
		return order;
	}

	void vgp_customer(HttpServletRequest request, String sql) {
		Vector rows = new Vector();

		Double vgp = Double.valueOf(0.0D);
		Double vgpm = Double.valueOf(0.0D);
		Double revenue = Double.valueOf(0.0D);
		Double cost = Double.valueOf(0.0D);
		String[] bottom_line = new String[3];
		int i = 0;
		String title = "VGP: All Customers";
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		NumberFormat formatter2 = new DecimalFormat("00.00");

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[8];
				if (this.PageLevel.equals("1")) {
					row[0] = rs.getString(1);
				} else if (this.PageLevel.equals("2")) {
					row[0] = rs.getString(2);
					title = "VGP: " + rs.getString(1) + ",<br>All Projects";
				} else if (this.PageLevel.equals("3")) {
					row[0] = rs.getString(2);
					title = "VGP: " + rs.getString(1) + ",<br>"
							+ rs.getString(2);
				}
				row[1] = formatter.format(rs.getDouble("revenue"));
				revenue = Double.valueOf(revenue.doubleValue()
						+ rs.getDouble("revenue"));
				row[2] = formatter2.format(rs.getDouble("vgpm"));
				vgpm = Double
						.valueOf(vgpm.doubleValue() + rs.getDouble("vgpm"));
				row[3] = formatter.format(rs.getDouble("vgp"));
				vgp = Double.valueOf(vgp.doubleValue() + rs.getDouble("vgp"));
				row[4] = rs.getString("jobs");
				row[5] = rs.getString("lo_pc_last_name");
				row[6] = rs.getString("ap_jl_pr_title");
				row[7] = rs.getString("ap_jl_supervisor");
				cost = Double
						.valueOf(cost.doubleValue() + rs.getDouble("cost"));
				rows.add(i++, row);
			}
			request.setAttribute("vgp", rows);
			request.setAttribute("page_title", title);
			request.setAttribute("page_level", this.PageLevel);
			if (i > 0) {
				bottom_line[0] = formatter.format(revenue);
				bottom_line[1] = formatter2.format(1.0D - cost.doubleValue()
						/ revenue.doubleValue());
				bottom_line[2] = formatter.format(vgp);
				request.setAttribute("bottom_line", bottom_line);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
	}
}
