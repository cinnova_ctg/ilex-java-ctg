package com.contingent.ilex.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;



public class MinutemanPayment extends DispatchAction {
	Vector inputParameterList = new Vector();
	Map inputParameterValue = new HashMap();
	DataAccess da = new DataAccess();
	Vector partner_lists = new Vector();
	Vector partner_list1 = new Vector();
	Vector partner_list2 = new Vector();
	Vector partner_list3 = new Vector();
	Vector partner_list4 = new Vector();
	String status_total1 = "";
	String PayDate = null;
	String AuthorizedUser = "199A";

	public ActionForward review_list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "FAILURE";

		this.partner_list1.clear();
		this.partner_list2.clear();
		this.partner_list3.clear();
		this.partner_list4.clear();
		this.partner_lists.clear();
		
		
		
		
		initializeParameters("review_list");

		getInputParameters(request);
		if (requestOK("review_list")) {
			if (getPO_List(buildSPCall("review_list"))) {
				request.setAttribute("partner_list1", this.partner_list1);
				this.partner_lists.add(0, Integer.valueOf(0));
				request.setAttribute("partner_list2", this.partner_list2);
				this.partner_lists.add(1, Integer.valueOf(1));
				request.setAttribute("partner_list3", this.partner_list3);
				this.partner_lists.add(2, Integer.valueOf(2));
				request.setAttribute("partner_list4", this.partner_list4);
				this.partner_lists.add(3, Integer.valueOf(3));
				request.setAttribute("partner_lists", this.partner_lists);

				request.setAttribute("status_total1", this.status_total1);

				setInputParameters(request);
				forward_key = "REVIEW";
				
				
				
				String generateExport = request.getParameter("genExport");
				String tableId = request.getParameter("tableId");

				if (generateExport != null && generateExport.equals("Export")) {
					byte[] buffer  = null;
					if(tableId.equals("0")){
						buffer = export(this.partner_list1, request.getParameter("type_of_po"));
					}else if(tableId.equals("1")){
						buffer = export(this.partner_list2, request.getParameter("type_of_po"));
					}else if(tableId.equals("2")){
						buffer = export(this.partner_list3, request.getParameter("type_of_po"));
					}else if(tableId.equals("3")){
						buffer = export(this.partner_list4, request.getParameter("type_of_po"));
					}
					
					response.setContentType("application/x-msdownload");
					response.setHeader("Content-Disposition",
							"attachment;filename=poExport.csv;size="
									+ buffer.length + "");
					ServletOutputStream outStream = response.getOutputStream();
					outStream.write(buffer);
					outStream.close();
				}if (generateExport != null && generateExport.equals("ExportPOEntry")) {
					byte[] buffer  = null;
					if(tableId.equals("0")){
						buffer = exportPOENTRY(this.partner_list1, request.getParameter("type_of_po"));
					}else if(tableId.equals("1")){
						buffer = exportPOENTRY(this.partner_list2, request.getParameter("type_of_po"));
					}else if(tableId.equals("2")){
						buffer = exportPOENTRY(this.partner_list3, request.getParameter("type_of_po"));
					}else if(tableId.equals("3")){
						buffer = exportPOENTRY(this.partner_list4, request.getParameter("type_of_po"));
					}
					
					response.setContentType("application/x-msdownload");
					response.setHeader("Content-Disposition",
							"attachment;filename=POEntryExport.csv;size="
									+ buffer.length + "");
					ServletOutputStream outStream = response.getOutputStream();
					outStream.write(buffer);
					outStream.close();
				}if (generateExport != null && generateExport.equals("ExportReceipt")) {
					byte[] buffer  = null;
					if(tableId.equals("0")){
						buffer = exportReceipt(this.partner_list1, request.getParameter("type_of_po"));
					}else if(tableId.equals("1")){
						buffer = exportReceipt(this.partner_list2, request.getParameter("type_of_po"));
					}else if(tableId.equals("2")){
						buffer = exportReceipt(this.partner_list3, request.getParameter("type_of_po"));
					}else if(tableId.equals("3")){
						buffer = exportReceipt(this.partner_list4, request.getParameter("type_of_po"));
					}
					
					response.setContentType("application/x-msdownload");
					response.setHeader("Content-Disposition",
							"attachment;filename=ExportReceipt.csv;size="
									+ buffer.length + "");
					ServletOutputStream outStream = response.getOutputStream();
					outStream.write(buffer);
					outStream.close();
				}
				
				

				
			}
		}
		return mapping.findForward(forward_key);
	}

	public ActionForward approve_list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "FAILURE";

		initializeParameters("approve_list");
		
		String po=request.getParameter("po");
		String[] gpvendorId=request.getParameterValues("gpvendorId");
		
		
		if(po !=null && gpvendorId!=null){
			int code=0;
			String po_id=po;
			String[] gp_Id=removeNullValues(gpvendorId,true);
			if (gp_Id.length > 0) {
				code=markAsgpVendorUpdate(po_id,gp_Id[0]);
			}
		}
		
		getInputParameters(request);
		if (requestOK("approve_list")) {
			if (getPO_List(buildSPCall("approve_list"))) {
				request.setAttribute("partner_list1", this.partner_list1);
				this.partner_lists.add(0, Integer.valueOf(0));

				request.setAttribute("status_total1", this.status_total1);

				setInputParameters(request);
				forward_key = "APPROVE";
				
				String generateExport = request.getParameter("genExport");

				if (generateExport != null && generateExport.equals("Export")) {
					byte[] buffer = export(this.partner_list1, request.getParameter("type_of_po"));
					response.setContentType("application/x-msdownload");
					response.setHeader("Content-Disposition",
							"attachment;filename=poExport.csv;size="
									+ buffer.length + "");
					ServletOutputStream outStream = response.getOutputStream();
					outStream.write(buffer);
					outStream.close();
				}else if (generateExport != null && generateExport.equals("ExportPOEntry")) {
					byte[] buffer = exportPOENTRY(this.partner_list1, request.getParameter("type_of_po"));
					response.setContentType("application/x-msdownload");
					response.setHeader("Content-Disposition",
							"attachment;filename=ExportPOEntry.csv;size="
									+ buffer.length + "");
					ServletOutputStream outStream = response.getOutputStream();
					outStream.write(buffer);
					outStream.close();
				}else if (generateExport != null && generateExport.equals("ExportReceipt")) {
					byte[] buffer = exportReceipt(this.partner_list1, request.getParameter("type_of_po"));
					response.setContentType("application/x-msdownload");
					response.setHeader("Content-Disposition",
							"attachment;filename=ExportReceipt.csv;size="
									+ buffer.length + "");
					ServletOutputStream outStream = response.getOutputStream();
					outStream.write(buffer);
					outStream.close();
				}
			}
		}
		return mapping.findForward(forward_key);
	}

	public ActionForward payment(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "FAILURE";

		String pay_status = null;
		String status = null;
		int po_count_to_pay = 0;
		int po_count_paid = 0;

		initializeParameters("payment");
		getInputParameters(request);

		String[] pay_list = request.getParameterValues("lm_po_id");

		String pt = (String) this.inputParameterValue.get("type_of_po");
		if (validateUser()) {
			status = checkPayPeriod();
			if (status.equals("OK")) {
				if ((pay_list != null) && (pay_list.length > 0)) {
					po_count_to_pay = pay_list.length;
					for (int i = 0; i < pay_list.length; i++) {
						status = markAsPaid(pay_list[i], this.PayDate, pt);
						if (status.equals("OK")) {
							po_count_paid++;
							System.out.println(i + " " + pay_list[i]);
						} else {
							pay_status = "Process failed at PO# " + pay_list[i]
									+ " with error message " + status;
							System.out.println(pay_status);
							break;
						}
					}
				} else {
					pay_status = "No were POs selected for payment.";
				}
			} else {
				pay_status = "An invalid pay period was selected.  Message was: "
						+ status;
			}
		} else {
			pay_status = "An invalid user attempted to intiate this pay period.";
		}
		if ((po_count_to_pay == po_count_paid) && (pay_status == null)) {
			pay_status = closePeriod(this.PayDate, pt);
			if (pay_status.equals("OK")) {
				if (getPO_List(buildSPCall("payment"))) {
					request.setAttribute("partner_list1", this.partner_list1);
					this.partner_lists.add(0, Integer.valueOf(0));
					setInputParameters(request);
					request.removeAttribute("pay_status");
					pay_status = null;
					forward_key = "PAID";
				}
			}
		}
		if (pay_status != null) {
			request.setAttribute("pay_status", pay_status);
			forward_key = "FAILURE";
		}
		System.out.println("Status: " + pay_status);

		return mapping.findForward(forward_key);
	}

	public ActionForward history(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "FAILURE";

		initializeParameters("history");
		getInputParameters(request);
		if (requestOK("history")) {
			this.PayDate = ((String) this.inputParameterValue.get("pay_date"));
			if (getPO_List(buildSPCall("payment"))) {
				request.setAttribute("partner_list1", this.partner_list1);
				this.partner_lists.add(0, Integer.valueOf(0));

				request.setAttribute("status_total1", this.status_total1);

				setInputParameters(request);

				forward_key = "PAID";

				String generateExport = request.getParameter("genExport");

				if (generateExport != null && generateExport.equals("Export")) {
					byte[] buffer = export(this.partner_list1, request.getParameter("type_of_po"));
					response.setContentType("application/x-msdownload");
					response.setHeader("Content-Disposition",
							"attachment;filename=poExport.csv;size="
									+ buffer.length + "");
					ServletOutputStream outStream = response.getOutputStream();
					outStream.write(buffer);
					outStream.close();
				}else	if (generateExport != null && generateExport.equals("ExportPOEntry")) {
					byte[] buffer = exportPOENTRY(this.partner_list1, request.getParameter("type_of_po"));
					response.setContentType("application/x-msdownload");
					response.setHeader("Content-Disposition",
							"attachment;filename=ExportPOEntry.csv;size="
									+ buffer.length + "");
					ServletOutputStream outStream = response.getOutputStream();
					outStream.write(buffer);
					outStream.close();
				}else if (generateExport != null && generateExport.equals("ExportReceipt")) {
					byte[] buffer = exportReceipt(this.partner_list1, request.getParameter("type_of_po"));
					response.setContentType("application/x-msdownload");
					response.setHeader("Content-Disposition",
							"attachment;filename=ExportReceipt.csv;size="
									+ buffer.length + "");
					ServletOutputStream outStream = response.getOutputStream();
					outStream.write(buffer);
					outStream.close();
				}
			}
		}
		return mapping.findForward(forward_key);
	}

	public byte[] export(Vector plist, String partnerType) throws Exception {

		Vector v = plist;

		String currentDate =  new SimpleDateFormat("MMddyy").format(new Date(System.currentTimeMillis()));
		
				
		
		File f = new File(System.getProperty( "catalina.base" )+"\\temp\\"+System.currentTimeMillis()+"data.csv");
		if (!f.exists()) {
			f.createNewFile();
		}
		FileWriter writer = new FileWriter(f);

		writer.append("Batch ID");
		writer.append(",");
		writer.append("Doc Type");
		writer.append(",");
		writer.append("Vendor");
		writer.append(",");
		writer.append("Doc Date");
		writer.append(",");
		writer.append("Voucher Number");
		writer.append(",");
		writer.append("Doc Number");
		writer.append(",");
		writer.append("Purchases Amt");
		writer.append(",");
		writer.append("Account");
		writer.append(",");
		writer.append("Dist type");
		writer.append(",");
		writer.append("Debit");
		writer.append(",");
		writer.append("Credit");
		writer.append('\n');
		writer.append('\n');

		for (int i = 0; i < v.size(); i++) {
			Map s = (HashMap) v.get(i);
			// System.out.println(s);

			try {

				Vector poListV = (Vector) s.get("po_list");
				

				for (int j = 0; j < poListV.size(); j++) {
					String[] arr = (String[]) poListV.get(j);

					double laborAmnt = Double.parseDouble((arr[8].substring(1,
							arr[8].length()).replace(",", "")));
					double trvlAmnt = Double.parseDouble((arr[9].substring(1,
							arr[9].length()).replace(",", "")));
					double laborAndTrvlAmnt = laborAmnt + trvlAmnt;
					double totalPOAmnt = Double.parseDouble((arr[5].substring(
							1, arr[5].length()).replace(",", "")));
					double total = laborAndTrvlAmnt + totalPOAmnt;
					String docDate = arr[2];
					if(!arr[2].matches(".*\\d.*")){
						/*
						 * condition checks if arr[2] does not contain digits it
						 * means its not a date ... so we replace it with space
						 * ... as we have to show date there in doc type col.
						 */
						
						if("onsite".equalsIgnoreCase(docDate)){
							String ss = arr[20].substring(0,10);
							docDate = ss;
						}
						
						
					} 
					//writer.append("SP030714");
					writer.append(partnerType.equals("M")?"MM"+currentDate:"SP"+currentDate);
					writer.append(",");
					writer.append("1");
					writer.append(",");
					writer.append("\"" + (arr[19]==null?"":arr[19]).toString() + "\"");//
					writer.append(",");
//					writer.append("\"" + arr[2] + "\"");
					writer.append("\"" + docDate + "\"");
					writer.append(",");
					writer.append("");
					writer.append(",");
					writer.append("\"" + arr[1] + "\"");
					writer.append(",");
					writer.append("$" + total + "");
					writer.append(",");
					//writer.append(partnerType.equals("M")?"MM-4501-25":"SP-4501-26");
					writer.append(partnerType.equals("M")?"4501-25":"501-26");
					writer.append(",");
					writer.append("6");
					writer.append(",");
					writer.append(laborAndTrvlAmnt + "");
					writer.append(",");
					writer.append("");
					writer.append('\n');

					//writer.append("SP030714");
					writer.append(partnerType.equals("M")?"MM"+currentDate:"SP"+currentDate);
					writer.append(",");
					writer.append("1");
					writer.append(",");
					writer.append("\"" + (arr[19]==null?"":arr[19]).toString() + "\"");//
					writer.append(",");
//					writer.append("\"" + arr[2] + "\"");
					writer.append("\"" + docDate + "\"");
					writer.append(",");
					writer.append("");
					writer.append(",");
					writer.append("\"" + arr[1] + "\"");
					writer.append(",");
					writer.append("$" + total + "");
					writer.append(",");
					writer.append("4520-02");
					writer.append(",");
					writer.append("6");
					writer.append(",");
					writer.append("$" + totalPOAmnt + "");
					writer.append(",");
					writer.append("");
					writer.append('\n');

					//writer.append("SP030714");
					writer.append(partnerType.equals("M")?"MM"+currentDate:"SP"+currentDate);
					writer.append(",");
					writer.append("1");
					writer.append(",");
					writer.append("\"" + (arr[19]==null?"":arr[19]).toString() + "\"");//
					writer.append(",");
//					writer.append("\"" + arr[2] + "\"");
					writer.append("\"" + docDate + "\"");
					writer.append(",");
					writer.append("");
					writer.append(",");
					writer.append("\"" + arr[1] + "\"");
					writer.append(",");
 					writer.append("$" + total + "");
					writer.append(",");
					writer.append("2100-00");
					writer.append(",");
					writer.append("2");
					writer.append(",");
					writer.append("");
					writer.append(",");
					writer.append("$" + total + "");
					writer.append('\n');
					writer.append('\n');
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		writer.flush();
		writer.close();

		String csvfilepath = f.getAbsolutePath();

		String filedata = null;
		File file = new File(csvfilepath);

		// FileReader filereader = new FileReader(file);

		FileInputStream fileinputstream = new FileInputStream(file);

		byte[] buffer = new byte[fileinputstream.available()];

		if (fileinputstream.read(buffer) != -1)
			;

		return buffer;

	}
// get Export File in case PO Entry
	public byte[] exportPOENTRY(Vector plist, String partnerType) throws Exception {

		Vector v = plist;
		String itemlabel="";
		String currentDate =  new SimpleDateFormat("MMddyy").format(new Date(System.currentTimeMillis()));
		boolean flag=false;
		double laborAndTrvlAmnt ;		
		
		File f = new File(System.getProperty( "catalina.base" )+"\\temp\\"+System.currentTimeMillis()+"data.csv");
		if (!f.exists()) {
			f.createNewFile();
		}
		FileWriter writer = new FileWriter(f);

		writer.append("Type");
		writer.append(",");
		writer.append("PO Number");
		writer.append(",");
		writer.append("Vendor ID");
		writer.append(",");
		writer.append("Date");
		writer.append(",");
		writer.append("Item");
		writer.append(",");
		writer.append("Quantity");
		writer.append(",");
		writer.append("Site ID");
		writer.append(",");
		writer.append("Cost");
		writer.append('\n');
		writer.append('\n');

		for (int i = 0; i < v.size(); i++) {
			Map s = (HashMap) v.get(i);
			// System.out.println(s);

			try {

				Vector poListV = (Vector) s.get("po_list");
				

				for (int j = 0; j < poListV.size(); j++) {
					String[] arr = (String[]) poListV.get(j);

					
					
				//	
					
					double laborAmnt = Double.parseDouble((arr[8].substring(1,
							arr[8].length()).replace(",", "")));
					double trvlAmnt = Double.parseDouble((arr[9].substring(1,
							arr[9].length()).replace(",", "")));
					 laborAndTrvlAmnt = laborAmnt + trvlAmnt;	
					double totalHardwareCost = Double.parseDouble((arr[10].substring(1,
							arr[10].length()).replace(",", "")));
					double totalPOAmnt = Double.parseDouble((arr[5].substring(
							1, arr[5].length()).replace(",", "")));
					String docDate = arr[2];
					if(!arr[2].matches(".*\\d.*")){
						/*
						 * condition checks if arr[2] does not contain digits it
						 * means its not a date ... so we replace it with space
						 * ... as we have to show date there in doc type col.
						 */
						
						if("onsite".equalsIgnoreCase(docDate)){
							String ss = arr[20].substring(0,10);
							docDate = ss;
						}
						
						
					} 
					
					
					if(arr[13].equals("HR")){
					itemlabel="Labor";
					if(laborAndTrvlAmnt!=0){
					
					writer.append("1");
					writer.append(",");
					writer.append("\"" + arr[1] + "\"");
					writer.append(",");
					writer.append("\"" + (arr[19]==null?"":arr[19]).toString() + "\"");//
					writer.append(",");
//					writer.append("\"" + arr[2] + "\"");
					writer.append("\"" + docDate + "\"");
					writer.append(",");
					writer.append(partnerType.equals("M")?"MM"+itemlabel:"SP"+itemlabel);
					writer.append(",");
					writer.append("1");
					writer.append(",");
					writer.append("MAIN");
					writer.append(",");
					writer.append(laborAndTrvlAmnt + "");
					writer.append(",");
					writer.append('\n');
				}
					
					if(totalHardwareCost!=0){
					itemlabel="Materials";
					writer.append("1");
					writer.append(",");
					writer.append("\"" + arr[1] + "\"");
					writer.append(",");
					writer.append("\"" + (arr[19]==null?"":arr[19]).toString() + "\"");//
					writer.append(",");
//					writer.append("\"" + arr[2] + "\"");
					writer.append("\"" + docDate + "\"");
					writer.append(",");
					writer.append(partnerType.equals("M")?"MM"+itemlabel:"SP"+itemlabel);
					writer.append(",");
					writer.append("1");
					writer.append(",");
					writer.append("MAIN");
					writer.append(",");
					writer.append(totalHardwareCost + "");
					writer.append(",");
					writer.append('\n');
					
				}	
				
			}else{
				
				
				if(arr[13].equals("FP")){
					
					itemlabel="Labor";
		
					writer.append("1");
					writer.append(",");
					writer.append("\"" + arr[1] + "\"");
					writer.append(",");
					writer.append("\"" + (arr[19]==null?"":arr[19]).toString() + "\"");//
					writer.append(",");
//					writer.append("\"" + arr[2] + "\"");
					writer.append("\"" + docDate + "\"");
					writer.append(",");
					writer.append(partnerType.equals("M")?"MM"+itemlabel:"SP"+itemlabel);
					writer.append(",");
					writer.append("1");
					writer.append(",");
					writer.append("MAIN");
					writer.append(",");
					writer.append(totalPOAmnt + "");
					writer.append(",");
					writer.append('\n');
			
				
			    }	
				
			}
					
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		writer.flush();
		writer.close();

		String csvfilepath = f.getAbsolutePath();

		String filedata = null;
		File file = new File(csvfilepath);

		// FileReader filereader = new FileReader(file);

		FileInputStream fileinputstream = new FileInputStream(file);

		byte[] buffer = new byte[fileinputstream.available()];

		if (fileinputstream.read(buffer) != -1)
			;

		return buffer;

	}	
// Get Receipt details
	public byte[] exportReceipt(Vector plist, String partnerType) throws Exception {

		Vector v = plist;
		String itemlabel="";
		String currentDate =  new SimpleDateFormat("MMddyy").format(new Date(System.currentTimeMillis()));
		boolean flag=false;
		double laborAndTrvlAmnt ;		
		
		File f = new File(System.getProperty( "catalina.base" )+"\\temp\\"+System.currentTimeMillis()+"data.csv");
		if (!f.exists()) {
			f.createNewFile();
		}
		FileWriter writer = new FileWriter(f);

		writer.append("Type");
		writer.append(",");
	//	writer.append("PO Number");
	//	writer.append(",");
		writer.append("Vendor ID");
		writer.append(",");
		writer.append("Date");
		writer.append(",");
		writer.append("Document Number");
		writer.append(",");
		writer.append("PO Number");
		writer.append(",");
		writer.append("Item");
		writer.append(",");
		writer.append("Quantity");
		writer.append(",");
		writer.append("Site ID");
		/*writer.append(",");
		writer.append("Cost");*/
		writer.append('\n');
		writer.append('\n');

		for (int i = 0; i < v.size(); i++) {
			Map s = (HashMap) v.get(i);
			// System.out.println(s);

			try {

				Vector poListV = (Vector) s.get("po_list");
				

				for (int j = 0; j < poListV.size(); j++) {
					String[] arr = (String[]) poListV.get(j);

					
					
				//	
					
					double laborAmnt = Double.parseDouble((arr[8].substring(1,
							arr[8].length()).replace(",", "")));
					double trvlAmnt = Double.parseDouble((arr[9].substring(1,
							arr[9].length()).replace(",", "")));
					 laborAndTrvlAmnt = laborAmnt + trvlAmnt;	
					double totalHardwareCost = Double.parseDouble((arr[10].substring(1,
							arr[10].length()).replace(",", "")));
					double totalPOAmnt = Double.parseDouble((arr[5].substring(
							1, arr[5].length()).replace(",", "")));
					String docDate = arr[2];
					if(!arr[2].matches(".*\\d.*")){
						/*
						 * condition checks if arr[2] does not contain digits it
						 * means its not a date ... so we replace it with space
						 * ... as we have to show date there in doc type col.
						 */
						
						if("onsite".equalsIgnoreCase(docDate)){
							String ss = arr[20].substring(0,10);
							docDate = ss;
						}
						
						
					} 
					
					
					if(arr[13].equals("HR")){
					itemlabel="Labor";
					writer.append("3");
					writer.append(",");
			//		writer.append("\"" + arr[1] + "\"");
			//		writer.append(",");
					writer.append("\"" + (arr[19]==null?"":arr[19]).toString() + "\"");//
					writer.append(",");
//					writer.append("\"" + arr[2] + "\"");
					writer.append("\"" + docDate + "\"");
					writer.append(",");
					writer.append("\"" + arr[1] + "\"");
					writer.append(",");
					writer.append("\"" + arr[1] + "\"");
					writer.append(",");
					writer.append(partnerType.equals("M")?"MM"+itemlabel:"SP"+itemlabel);
					writer.append(",");
					writer.append("1");
					writer.append(",");
					writer.append("MAIN");
					/*writer.append(",");
					writer.append("$" + laborAndTrvlAmnt + "");
					writer.append(",");*/
					writer.append('\n');
					
					
					//writer.append("SP030714");
					itemlabel="Materials";
					writer.append("3");
					writer.append(",");
			//		writer.append("\"" + arr[1] + "\"");
			//		writer.append(",");
					writer.append("\"" + (arr[19]==null?"":arr[19]).toString() + "\"");//
					writer.append(",");
//					writer.append("\"" + arr[2] + "\"");
					writer.append("\"" + docDate + "\"");
					writer.append(",");
					writer.append("\"" + arr[1] + "\"");
					writer.append(",");
					writer.append("\"" + arr[1] + "\"");
					writer.append(",");
					writer.append(partnerType.equals("M")?"MM"+itemlabel:"SP"+itemlabel);
					writer.append(",");
					writer.append("1");
					writer.append(",");
					writer.append("MAIN");
					/*writer.append(",");
					writer.append("$" + laborAndTrvlAmnt + "");
					writer.append(",");*/
					writer.append('\n');
			}else{
				
				
				if(arr[13].equals("FP")){
					itemlabel="Labor";
					writer.append("3");
					writer.append(",");
			//		writer.append("\"" + arr[1] + "\"");
			//		writer.append(",");
					writer.append("\"" + (arr[19]==null?"":arr[19]).toString() + "\"");//
					writer.append(",");
//					writer.append("\"" + arr[2] + "\"");
					writer.append("\"" + docDate + "\"");
					writer.append(",");
					writer.append("\"" + arr[1] + "\"");
					writer.append(",");
					writer.append("\"" + arr[1] + "\"");
					writer.append(",");
					writer.append(partnerType.equals("M")?"MM"+itemlabel:"SP"+itemlabel);
					writer.append(",");
					writer.append("1");
					writer.append(",");
					writer.append("MAIN");
					/*writer.append(",");
					writer.append("$" + laborAndTrvlAmnt + "");
					writer.append(",");*/
					writer.append('\n');
				
			     }	
				
			}
					
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		writer.flush();
		writer.close();

		String csvfilepath = f.getAbsolutePath();

		String filedata = null;
		File file = new File(csvfilepath);

		// FileReader filereader = new FileReader(file);

		FileInputStream fileinputstream = new FileInputStream(file);

		byte[] buffer = new byte[fileinputstream.available()];

		if (fileinputstream.read(buffer) != -1)
			;

		return buffer;

	}	
	
	
	
	boolean getPO_List(String sql) {
		boolean status = true;

		String current_status = "";
		String previous_status = "";

		String current_pid = "";
		String previous_pid = "Initial";

		Double effective = Double.valueOf(0.0D);
		Double labor = Double.valueOf(0.0D);
		Double hardware = Double.valueOf(0.0D);
		Double travel = Double.valueOf(0.0D);
		Double status_total = Double.valueOf(0.0D);

		String partner_name = "";

		Vector po_list = new Vector();
		Map partner_details = new HashMap();

		this.partner_lists.clear();
		this.partner_list1.clear();
		this.partner_list2.clear();
		this.partner_list3.clear();
		this.partner_list4.clear();

		System.out.println(sql);
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				current_status = rs.getString(5);
				current_pid = rs.getString(22);
				if ((!current_status.equals(previous_status))
						|| (!current_pid.equals(previous_pid))) {
					if (!previous_pid.equals("Initial")) {
						partner_details.put("partner_id", previous_pid);
						partner_details.put("partner_name", partner_name);

						partner_details.put("effective", NumberFormat
								.getCurrencyInstance().format(effective));
						partner_details.put("labor", NumberFormat
								.getCurrencyInstance().format(labor));
						partner_details.put("hardware", NumberFormat
								.getCurrencyInstance().format(hardware));
						partner_details.put("travel", NumberFormat
								.getCurrencyInstance().format(travel));
						partner_details.put("po_list", po_list);

						addToPartnerList(previous_status, partner_details);
					}
					if ((previous_status.equals("1"))
							&& (current_status.equals("2"))) {
						this.status_total1 = NumberFormat.getCurrencyInstance()
								.format(status_total);
						System.out.println("Total Payable "
								+ this.status_total1);
						status_total = Double.valueOf(0.0D);
					}
					partner_details = new HashMap();
					po_list = new Vector();

					effective = Double.valueOf(0.0D);
					labor = Double.valueOf(0.0D);
					hardware = Double.valueOf(0.0D);
					travel = Double.valueOf(0.0D);

					previous_pid = current_pid;
					previous_status = current_status;

					partner_name = buildPartnerName(rs.getString(3),
							rs.getString(18), rs.getString(19));
				}
				effective = Double.valueOf(effective.doubleValue()
						+ rs.getDouble(21));
				labor = Double.valueOf(labor.doubleValue() + rs.getDouble(16));
				hardware = Double.valueOf(hardware.doubleValue()
						+ rs.getDouble(14));
				travel = Double
						.valueOf(travel.doubleValue() + rs.getDouble(15));
				status_total = Double.valueOf(status_total.doubleValue()
						+ rs.getDouble(21));

				po_list.add(getPOFields(rs));
			}
			if (!previous_pid.equals("Initial")) {
				partner_details.put("partner_id", previous_pid);
				partner_details.put("partner_name", partner_name);
				partner_details.put("effective", NumberFormat
						.getCurrencyInstance().format(effective));
				partner_details.put("labor", NumberFormat.getCurrencyInstance()
						.format(labor));
				partner_details.put("hardware", NumberFormat
						.getCurrencyInstance().format(hardware));
				partner_details.put("travel", NumberFormat
						.getCurrencyInstance().format(travel));
				partner_details.put("po_list", po_list);

				addToPartnerList(previous_status, partner_details);
			}
			if (previous_status.equals("1")) {
				this.status_total1 = NumberFormat.getCurrencyInstance().format(
						status_total);
				System.out.println("Total Payable " + this.status_total1);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e);
			status = false;
		}
		return status;
	}

	void addToPartnerList(String status, Map partner_details) {
		if (status.equals("1")) {
			this.partner_list1.add(partner_details);
		} else if (status.equals("2")) {
			this.partner_list2.add(partner_details);
		} else if (status.equals("3")) {
			this.partner_list3.add(partner_details);
		} else if (status.equals("4")) {
			this.partner_list4.add(partner_details);
		}
	}

	String buildPartnerName(String name, String cp_pd_w9w9, String cp_pd_status) {
		String partner_status = "";
		String delimiter = "";
		if ((cp_pd_status == null) || (!cp_pd_status.equals("A"))) {
			partner_status = partner_status + "Restricted";
			delimiter = ", ";
		}
		if ((cp_pd_w9w9 == null) || (!cp_pd_w9w9.equals("Y"))) {
			partner_status = partner_status + delimiter + "W-9 Missing";
		}
		if (partner_status.length() > 0) {
			partner_status = "<br>(" + partner_status + ")";
		}
		name = name + partner_status;

		return name;
	}

	String buildSPCall(String call_type) {
		String sql = "";

		String sd = (String) this.inputParameterValue.get("start_date");
		String ed = (String) this.inputParameterValue.get("end_date");
		String pt = (String) this.inputParameterValue.get("type_of_po");
		if (call_type.equals("review_list")) {
			if (sd.equals("")) {
				sd = "01/01/1900";
				sql = "exec ap_mm_sp_view_01 '" + sd + "', '" + ed + "', '"
						+ pt + "', 'All'";
			} else {
				sql = "exec ap_mm_sp_view_03 '" + sd + "', '" + ed + "', '"
						+ pt + "', 'All'";
			}
		} else if (call_type.equals("approve_list")) {
			sql = "exec ap_mm_sp_view_01 '" + sd + "', '" + ed + "', '" + pt
					+ "', 'Payable'";
		} else if (call_type.equals("payment")) {
			sql = "exec ap_mm_sp_view_witn_vendorId '" + this.PayDate + "', '" + pt + "'";
		} 
		
		
		return sql;
	}

	String buildPO_Type(String type, String deliverables) {
		String po_suffix = "";

		po_suffix = type;

		return po_suffix;
	}

	String[] getPOFields(ResultSet rs) {
		String[] po = new String[21];
		String[] po_type = { "HR", "FP", "M", "SI" };

		String cp_pd_w9w9 = "";
		String cp_pd_status = "";
		String partner_status = "";
		String delimiter = "";
		try {
			po[0] = rs.getString(3);
			po[1] = rs.getString(2);
			po[2] = rs.getString(10);
			po[3] = rs.getString(23);
			po[4] = rs.getString(9);
			po[5] = NumberFormat.getCurrencyInstance().format(rs.getDouble(12));
			po[6] = NumberFormat.getCurrencyInstance().format(rs.getDouble(13));
			po[7] = NumberFormat.getCurrencyInstance().format(rs.getDouble(21));
			po[8] = NumberFormat.getCurrencyInstance().format(rs.getDouble(16));
			po[9] = NumberFormat.getCurrencyInstance().format(rs.getDouble(15));
			po[10] = NumberFormat.getCurrencyInstance()
					.format(rs.getDouble(14));
			po[11] = rs.getString(4);
			po[12] = rs.getString(8);
			int po_type_index = rs.getInt(20);
			if (rs.wasNull()) {
				po[13] = buildPO_Type("", rs.getString(7));
			} else {
				po[13] = buildPO_Type(po_type[(po_type_index - 1)],
						rs.getString(7));
			}
			po[14] = rs.getString(1);
			po[15] = rs.getString(6);
			po[16] = rs.getString(7);
			po[17] = rs.getString(24);
			po[18] = rs.getString(25);
			po[19] = rs.getString("gpvendor_id")==null?"":rs.getString("gpvendor_id");
			po[20] = rs.getString("lm_po_complete_date")==null?"":rs.getString("lm_po_complete_date");
		} catch (Exception e) {
			System.out.println("getPOFields error: " + e);
		}
		return po;
	}

	boolean requestOK(String action) {
		boolean status = false;

		status = true;

		return status;
	}

	void getInputParameters(HttpServletRequest request) {
		String parameter = "";
		String parameter_value = "";
		for (int i = 0; i < this.inputParameterList.size(); i++) {
			parameter = (String) this.inputParameterList.get(i);
			parameter_value = cleanParameter(request.getParameter(parameter));
			this.inputParameterValue.put(parameter, parameter_value);
		}
		String v1 = (String) request.getSession().getAttribute("userid");
		String v2 = (String) request.getSession().getAttribute("useremail");
		String v3 = (String) request.getSession().getAttribute("username");

		System.out.println("Userid " + v1);
	}

	void initializeParameters(String function) {
		this.inputParameterList.clear();
		if (function.equals("review_list")) {
			this.inputParameterList.add(0, "start_date");
			this.inputParameterList.add(1, "end_date");
			this.inputParameterList.add(2, "type_of_po");
			this.inputParameterList.add(3, "list_flag");
		} else if (function.equals("approve_list")) {
			this.inputParameterList.add(0, "start_date");
			this.inputParameterList.add(1, "end_date");
			this.inputParameterList.add(2, "type_of_po");
			this.inputParameterList.add(3, "list_flag");
		} else if (function.equals("payment")) {
			this.inputParameterList.add(0, "start_date");
			this.inputParameterList.add(1, "end_date");
			this.inputParameterList.add(2, "type_of_po");
			this.inputParameterList.add(3, "list_flag");
			this.inputParameterList.add(4, "user");
		} else if (function.equals("history")) {
			this.inputParameterList.add(0, "pay_date");
			this.inputParameterList.add(1, "end_date");
			this.inputParameterList.add(2, "type_of_po");
		}
	}

	String cleanParameter(String value) {
		if (value == null) {
			value = "";
		} else {
			value = value.trim();
		}
		return value;
	}

	void setInputParameters(HttpServletRequest request) {
		String parameter = "";
		String parameter_value = "";
		for (int i = 0; i < this.inputParameterList.size(); i++) {
			parameter = (String) this.inputParameterList.get(i);
			parameter_value = cleanParameter((String) this.inputParameterValue
					.get(parameter));

			request.setAttribute(parameter, parameter_value);
		}
	}

	boolean validateUser() {
		boolean rtn_code = false;
		if (this.AuthorizedUser.equals((String) this.inputParameterValue
				.get("user"))) {
			rtn_code = true;
		}
		return rtn_code;
	}

	String checkPayPeriod() {
		String rtn_code = "checkPayPeriod Failed";

		String ed = (String) this.inputParameterValue.get("end_date");
		String pt = (String) this.inputParameterValue.get("type_of_po");

		String status = null;
		System.out.println(ed + " " + pt);
		DateFormat df = DateFormat.getDateInstance(3);
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall("{call ap_po_check_pay_period_01(?,?,?,?)}");

			stmt.setObject(1, new Timestamp(df.parse(ed).getTime()), 91);

			stmt.setObject(2, pt, 12);

			stmt.registerOutParameter(3, 12);

			stmt.registerOutParameter(4, 12);

			stmt.execute();

			status = stmt.getString(3);
			if (status.equals("OK")) {
				this.PayDate = stmt.getString(4);
				rtn_code = "OK";
			} else {
				rtn_code = status;
			}
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return rtn_code;
	}

	String markAsPaid(String lm_po_id, String pd, String pt) {
		String rtn_code = "Undefined Error (markAsPaid)";

		DateFormat df = DateFormat.getDateInstance(3);
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall("{call ap_po_mark_paid_01(?,?,?,?)}");

			stmt.setObject(1, lm_po_id, 2);

			stmt.setObject(2, pd, 12);

			stmt.setObject(3, pt, 12);

			stmt.registerOutParameter(4, 12);

			stmt.execute();

			rtn_code = stmt.getString(4);

			stmt.close();
		} catch (Exception e) {
			rtn_code = "Database access error(markAsPaid)";
			System.out.println(e);
		}
		return rtn_code;
	}

	String closePeriod(String pd, String pt) {
		String rtn_code = "Undefined Error (closePeriod)";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall("{call ap_po_close_pay_period_01(?,?,?)}");

			stmt.setObject(1, pd, 12);

			stmt.setObject(2, pt, 12);

			stmt.registerOutParameter(3, 12);

			stmt.execute();

			rtn_code = stmt.getString(3);

			stmt.close();
			if (!rtn_code.equals("OK")) {
				rtn_code = "Could not close the pay period.  Message is: "
						+ rtn_code;
			}
		} catch (Exception e) {
			rtn_code = "Database access error(closePeriod)";
			System.out.println(e);
		}
		return rtn_code;
	}

	public static void main(String[] args) {
	}
	

int  markAsgpVendorUpdate(String lm_po_id, String gpId) {
		int  rtn_code = 0;


		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall("{call  sp_update_gpvendor(?,?)}");

			stmt.setObject(1, lm_po_id, 2);

			stmt.setObject(2, gpId, 12);

			

			

			rtn_code = stmt.executeUpdate();

			

			stmt.close();
		} catch (Exception e) {
			rtn_code = -1;
			System.out.println(e);
		}
		return rtn_code;
	}
private static String[] removeNullValues(String input[],
		boolean preserveInput) {

	String[] str;
	if (preserveInput) {
		str = new String[input.length];
		System.arraycopy(input, 0, str, 0, input.length);
	} else {
		str = input;
	}

	// Filter values null, empty or with blank spaces
	int p = 0, i = 0;
	for (; i < str.length; i++, p++) {
		str[p] = str[i];
		if (str[i] == null || str[i].isEmpty()
				|| (str[i].startsWith(" ") && str[i].trim().isEmpty()))
			p--;
	}

	// Resize the array
	String[] tmp = new String[p];
	System.arraycopy(str, 0, tmp, 0, p);
	str = null;

	return tmp;
}

}
