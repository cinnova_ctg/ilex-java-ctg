package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class RevenueReports extends DispatchAction {
	DataAccess da = new DataAccess();
	String project_type;
	String month;
	String year;

	public ActionForward revenueSummary(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = executeRevenueSummary(request);

		return mapping.findForward(forward_key);
	}

	String executeRevenueSummary(HttpServletRequest request) {
		String forward_key = "revenueSummary";

		getReportParameters(request);
		getReportData(formatSQL(), request);

		return forward_key;
	}

	void getReportParameters(HttpServletRequest request) {
		this.project_type = formatRequestParameter(request
				.getParameter("project_type"));
		request.setAttribute("project_type", this.project_type);

		this.month = formatRequestParameter(request.getParameter("month"));

		this.year = formatRequestParameter(request.getParameter("year"));
		if ((this.month.equals("")) || (this.year.equals(""))) {
			currentDate();
		}
		request.setAttribute("month", this.month);
		request.setAttribute("year", this.year);
	}

	String formatRequestParameter(String parameter) {
		if (parameter == null) {
			parameter = "";
		}
		return parameter;
	}

	String formatSQL() {
		String where1 = null;
		String where2 = null;
		int where_clause_count = 0;

		String sql_command = "SELECT ar_pl_task_type as type, ar_pl_real_time_status as real_time_status, sum(ar_pl_estimated_cost) estimated, sum(ar_pl_actual_cost) actual,  sum(ar_pl_revenue) revenue, sum(ar_pl_task_count) as count FROM  ar_project_level_revenue where ar_pl_month_year = '"
				+

				this.month + "/1/" + this.year + "' ";
		if (!this.project_type.equals("all")) {
			if (this.project_type.equals("deployments")) {
				sql_command = sql_command + "and ar_pl_project_type != 3";
			} else if (this.project_type.equals("netmedx")) {
				sql_command = sql_command + "and ar_pl_project_type = 3";
			}
		}
		sql_command = sql_command
				+ " group by ar_pl_task_type, ar_pl_real_time_status";
		System.out.println(sql_command);
		return sql_command;
	}

	void getReportData(String sql, HttpServletRequest request) {
		Map summary = new HashMap();
		Map statusTrack = initializeTracking();
		Double temp_vgp = Double.valueOf(0.0D);

		Double estimated = Double.valueOf(0.0D);
		Double actual = Double.valueOf(0.0D);
		Double revenue = Double.valueOf(0.0D);
		Integer count = Integer.valueOf(0);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				Vector<Comparable> x = new Vector(5);

				String status = rs.getString("real_time_status");

				x.add(0, formatCurrencyValues(Double.valueOf(rs
						.getDouble("estimated"))));
				x.add(1, formatCurrencyValues(Double.valueOf(rs
						.getDouble("actual"))));
				x.add(2, formatCurrencyValues(Double.valueOf(rs
						.getDouble("revenue"))));
				if (rs.getDouble("revenue") > 0.0D) {
					temp_vgp = Double.valueOf(1.0D - rs.getDouble("actual")
							/ rs.getDouble("revenue"));
				} else {
					temp_vgp = Double.valueOf(0.0D);
				}
				x.add(3, formatNumericValues(temp_vgp));
				x.add(4, Integer.valueOf(rs.getInt("count")));

				estimated = Double.valueOf(estimated.doubleValue()
						+ rs.getDouble("estimated"));
				actual = Double.valueOf(actual.doubleValue()
						+ rs.getDouble("actual"));
				revenue = Double.valueOf(revenue.doubleValue()
						+ rs.getDouble("revenue"));
				count = Integer.valueOf(count.intValue() + rs.getInt("count"));

				statusTrack.put(status, Integer.valueOf(1));
				summary.put(status, x);
			}
			rs.close();

			summary = addMissingStatus(statusTrack, summary);

			Vector x = new Vector(5);
			x.add(0, formatCurrencyValues(estimated));
			x.add(1, formatCurrencyValues(actual));
			x.add(2, formatCurrencyValues(revenue));
			if (revenue.doubleValue() > 0.0D) {
				temp_vgp = Double.valueOf(1.0D - actual.doubleValue()
						/ revenue.doubleValue());
			} else {
				temp_vgp = Double.valueOf(0.0D);
			}
			x.add(3, formatNumericValues(temp_vgp));
			x.add(4, count);
			summary.put("Overall", x);

			request.setAttribute("Summary", summary);
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	Map initializeTracking() {
		Map statuses = new HashMap();

		statuses.put("In Work - TBS", Integer.valueOf(0));
		statuses.put("In Work - Overdue", Integer.valueOf(0));
		statuses.put("In Work - Scheduled", Integer.valueOf(0));
		statuses.put("In Work - Onsite", Integer.valueOf(0));
		statuses.put("In Work - Offsite", Integer.valueOf(0));
		statuses.put("Complete", Integer.valueOf(0));
		statuses.put("Closed", Integer.valueOf(0));
		statuses.put("Overall", Integer.valueOf(0));
		statuses.put("Reconciled", Integer.valueOf(0));

		return statuses;
	}

	Map addMissingStatus(Map found, Map summary) {
		Set keys = found.keySet();
		Iterator keyIter = keys.iterator();
		while (keyIter.hasNext()) {
			Object status = keyIter.next();
			Integer used = (Integer) found.get(status);
			if (used.intValue() == 0) {
				Vector blank = new Vector();
				blank.add(0, "");
				blank.add(1, "");
				blank.add(2, "");
				blank.add(3, "");
				blank.add(4, "");
				summary.put(status, blank);
			}
		}
		return summary;
	}

	String formatCurrencyValues(Double value) {
		NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
		String formattedValue = nf.format(value);

		return formattedValue;
	}

	String formatNumericValues(Double value) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		String formattedValue = nf.format(value);

		return formattedValue;
	}

	void currentDate() {
		Date today = new Date();

		this.month = new SimpleDateFormat("M").format(today);
		this.year = new SimpleDateFormat("yyyy").format(today);
	}
}
