package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class Commission extends DispatchAction {
	Vector Parameters = new Vector();
	Map Submitted = new HashMap();
	Map ParameterValue = new HashMap();
	Map Validation = new HashMap();
	Map Mandatory = new HashMap();
	Vector bdm = new Vector();
	Vector spm = new Vector();
	Map offsite = new HashMap();
	Map complete = new HashMap();
	Map closed = new HashMap();
	Vector Field = new Vector();
	Vector FieldType = new Vector();
	Vector DynamicColumnTitles = new Vector();
	HttpServletRequest Request;
	DataAccess da = new DataAccess();

	public ActionForward commission(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		this.Request = request;
		this.Field.clear();
		this.FieldType.clear();
		this.DynamicColumnTitles.clear();
		this.Parameters.clear();
		this.Submitted.clear();

		initialzeInputParameters();
		getInputParameters();
		String forward_key;
		if (ManageRequest1()) {
			forward_key = "commission";
		} else {
			forward_key = "failed";
		}
		// String forward_key = "commission";
		return mapping.findForward(forward_key);
	}

	boolean ManageRequest1() {
		boolean rtn_status = false;
		String qtitle = "";
		Map Quarters = new HashMap();

		qtitle = (String) this.ParameterValue.get("quarter");
		if (this.Submitted.get("default_view") != null) {
			Quarters.put("20073", "Third Quarter 2007");
			Quarters.put("20074", "Fourth Quarter 2007");
			Quarters.put("20081", "First Quarter 2008");
			Quarters.put("20082", "Second Quarter 2008");
			Quarters.put("20083", "Third Quarter 2008");
			Quarters.put("20084", "Fourth Quarter 2008");
			Quarters.put("20091", "First Quarter 2009");
			Quarters.put("20092", "Second Quarter 2009");
			Quarters.put("20093", "Third Quarter 2009");
			Quarters.put("20094", "Fourth Quarter 2009");
			Quarters.put("20101", "First Quarter 2010");
			Quarters.put("20102", "Second Quarter 2010");
			Quarters.put("20103", "Third Quarter 2010");
			Quarters.put("20104", "Fourth Quarter 2010");

			buildResultList();

			this.Request.setAttribute("title", "Quarterly Summary - "
					+ (String) Quarters.get(qtitle));
			this.Request.setAttribute("report", "1");
		} else if (this.Submitted.get("ttm_profile") != null) {
			getResultSet(buildTTMSQL());

			this.Request.setAttribute("report", "2");
			this.Request.setAttribute("title",
					"Financial Summary - 12 Trailing Months");
		} else {
			getResultSet(buildSQL());

			this.Request.setAttribute("report", "3");
		}
		return rtn_status;
	}

	void initialzeInputParameters() {
		this.Parameters.add("project");
		this.Parameters.add("bdm");
		this.Parameters.add("project_manager");
		this.Parameters.add("job_owner");
		this.Parameters.add("from");
		this.Parameters.add("to");
		this.Parameters.add("quarter");
		this.Parameters.add("sort_order");
		this.Parameters.add("default_view");
		this.Parameters.add("ttm_profile");
	}

	void getInputParameters() {
		for (int i = 0; i < this.Parameters.size(); i++) {
			String parameter = (String) this.Parameters.get(i);
			String value = this.Request.getParameter(parameter);
			if ((value != null) && (value.trim().length() > 0)) {
				this.ParameterValue.put(parameter, value.trim());
				this.Submitted.put(parameter, "True");
			}
		}
	}

	String buildDefaultSQL(String date_reference, String user_group) {
		String select = "";
		String from = "";
		String where = "";
		String group = "";
		String order = "";
		String SQL = "";
		String wc = "";
		String wc_term_date = "";
		if (this.Submitted.get("quarter") != null) {
			wc = " datepart(qq, " + date_reference + ")+datepart(year, "
					+ date_reference + ")*10 = "
					+ (String) this.ParameterValue.get("quarter") + " ";
			wc_term_date = (String) this.ParameterValue.get("quarter") + " ";
		} else {
			wc = " datepart(qq, "
					+ date_reference
					+ ")+datepart(year, "
					+ date_reference
					+ ")*10 = datepart(qq, dateadd(qq, -1, getdate()))+datepart(year, dateadd(qq, -1, getdate()))*10 ";
			wc_term_date = " datepart(qq, dateadd(qq, -1, getdate()))+datepart(year, dateadd(qq, -1, getdate()))*10 ";
		}
		select = "select lo_pc_first_name+' '+lo_pc_last_name as name, count(*) as jobs, sum(dw_cs_revenue) as dw_cs_revenue, sum(dw_cs_best_cost_estimate) as dw_cs_best_cost_estimate";
		from = " from dw_revenue_commission_support join lo_poc on "
				+ user_group + " = lo_pc_id";
		where = " where "
				+ wc
				+ " "
				+ " and (isnull(lo_pc_active_user, 0) = 1 or (isnull(lo_pc_active_user, 0) = 0 and  "
				+ date_reference
				+ " < dateadd(mm, 1, lo_pc_termination_date)))"
				+ " and lo_pc_first_name != 'Chip' ";
		group = " group by lo_pc_first_name+' '+lo_pc_last_name";
		order = " order by lo_pc_first_name+' '+lo_pc_last_name";

		SQL = select + from + where + group + order;

		this.Field.add("name");
		this.FieldType.add("String");

		this.Field.add("jobs");
		this.FieldType.add("String");

		this.Field.add("dw_cs_revenue");
		this.FieldType.add("String");

		this.Field.add("dw_cs_best_cost_estimate");
		this.FieldType.add("String");

		return SQL;
	}

	String buildTTMSQL() {
		String select = "select convert (datetime, convert (char(4), datepart(yyyy, dw_cs_lm_js_offsite_date))+'-'+convert (varchar(2), datepart(mm, dw_cs_lm_js_offsite_date))+'-1') as month, sum(dw_cs_best_cost_estimate) as dw_cs_best_cost_estimate, sum(dw_cs_revenue) as dw_cs_revenue";
		String from = " from dw_revenue_commission_support";
		String where = " where dw_cs_lm_js_offsite_date > dateadd(mm, -11, getdate())";
		String group = " group by convert (datetime, convert (char(4), datepart(yyyy, dw_cs_lm_js_offsite_date))+'-'+convert (varchar(2), datepart(mm, dw_cs_lm_js_offsite_date))+'-1')";
		String order = " order by convert (datetime, convert (char(4), datepart(yyyy, dw_cs_lm_js_offsite_date))+'-'+convert (varchar(2), datepart(mm, dw_cs_lm_js_offsite_date))+'-1')";
		String SQL = select + from + where + group + order;

		this.Field.add("month");
		this.FieldType.add("String");

		this.Field.add("dw_cs_best_cost_estimate");
		this.FieldType.add("String");

		this.Field.add("dw_cs_revenue");
		this.FieldType.add("String");

		System.out.println("TTL: " + SQL);

		return SQL;
	}

	String buildSQL() {
		String select = "select ";
		String from = "";
		String where = "";
		String group = "";
		String order = "";
		String SQL = "";

		String s1 = buildGroupSortClause();

		select = "select "
				+ s1
				+ ", count(*) as jobs, sum(dw_cs_revenue) as dw_cs_revenue, sum(dw_cs_best_cost_estimate) as dw_cs_best_cost_estimate";
		from = " from dw_revenue_commission_support";
		where = buildWhereClause();
		group = " group by " + s1;
		order = " order by " + s1;

		SQL = select + from + where + group + order;

		return SQL;
	}

	String buildWhereClause() {
		String wc = "";
		String condition = "";
		if (this.Submitted.get("quarter") != null) {
			wc = "datepart(quarter, dw_cs_lm_js_offsite_date)+datepart(year, dw_cs_lm_js_offsite_date)*10 = "
					+ this.ParameterValue.get("quarter");
		} else if ((this.Submitted.get("from") != null)
				&& (this.Submitted.get("to") != null)) {
			wc = "dw_cs_lm_js_offsite_date between '"
					+ this.ParameterValue.get("from") + "' and '"
					+ this.ParameterValue.get("to") + "'";
		}
		if (this.Submitted.get("bdm") != null) {
			condition = "dw_cs_bdm = " + this.ParameterValue.get("bdm");
			wc = wc + (wc.length() > 0 ? " and " + condition : condition);
		}
		if (this.Submitted.get("project_manager") != null) {
			condition = "dw_cs_spm = "
					+ this.ParameterValue.get("project_manager");
			wc = wc + (wc.length() > 0 ? " and " + condition : condition);
		}
		if (this.Submitted.get("job_owner") != null) {
			condition = "dw_cs_job_owner = "
					+ this.ParameterValue.get("job_owner");
			wc = wc + (wc.length() > 0 ? " and " + condition : condition);
		}
		if (this.Submitted.get("project") != null) {
			condition = "dw_cs_lm_js_pr_id = "
					+ this.ParameterValue.get("project");
			wc = wc + (wc.length() > 0 ? " and " + condition : condition);
		}
		if (wc.length() > 0) {
			wc = " where " + wc;
		}
		return wc;
	}

	String buildGroupSortClause() {
		String list = "";
		String condition = "";
		if (this.Submitted.get("project") != null) {
			list = "dw_cs_lm_js_pr_id";
			this.Field.add("dw_cs_lm_js_pr_id");
			this.FieldType.add("String");
			this.DynamicColumnTitles.add("Appendix");
		}
		if (this.Submitted.get("bdm") != null) {
			condition = "dw_cs_bdm";
			list = list + (list.length() > 0 ? ", " + condition : condition);
			this.Field.add("dw_cs_bdm");
			this.FieldType.add("String");
			this.DynamicColumnTitles.add("Business Development Maanager");
		}
		if (this.Submitted.get("project_manager") != null) {
			condition = "dw_cs_spm";
			list = list + (list.length() > 0 ? ", " + condition : condition);
			this.Field.add("dw_cs_spm");
			this.FieldType.add("String");
			this.DynamicColumnTitles.add("Project Manager");
		}
		if (this.Submitted.get("job_owner") != null) {
			condition = "dw_cs_job_owner";
			list = list + (list.length() > 0 ? ", " + condition : condition);
			this.Field.add("dw_cs_job_owner");
			this.FieldType.add("String");
			this.DynamicColumnTitles.add("Job Owner");
		}
		if (list.length() == 0) {
			list = "dw_cs_lm_js_pr_id";
			this.Field.add("dw_cs_lm_js_pr_id");
			this.FieldType.add("String");
			this.DynamicColumnTitles.add("Appendix");
		}
		this.Field.add("jobs");
		this.FieldType.add("String");

		this.Field.add("dw_cs_revenue");
		this.FieldType.add("String");

		this.Field.add("dw_cs_best_cost_estimate");
		this.FieldType.add("String");

		return list;
	}

	boolean getResultSet(String sql) {
		boolean rtn_status = false;

		Vector rows = new Vector();

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				Vector row = new Vector();
				for (int i = 0; i < this.Field.size(); i++) {
					String field = (String) this.Field.get(i);
					String field_type = (String) this.FieldType.get(i);
					row.add(getFieldValue(field, field_type, rs));
				}
				rows.add(row);
			}
			rs.close();

			this.Request.setAttribute("rows", rows);
			if (this.Submitted.get("ttm_profile") != null) {
				buildChartParameters(rows);
			} else if (this.Submitted.get("default_view") != null) {
				buildViewData(rows);
			} else {
				buildGeneralView(rows);
			}
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
		return rtn_status;
	}

	Object getFieldValue(String p, String v, ResultSet rs) {
		try {
			if (v.equals("Money")) {
				return Double.valueOf(rs.getDouble(p));
			}
			if (v.equals("Int")) {
				return Integer.valueOf(rs.getInt(p));
			}
			return rs.getString(p);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return "Error";
	}

	void buildChartParameters(Vector rows) {
		Double[] cost = new Double[12];
		Double[] revenue = new Double[12];
		Double[] vgp = new Double[12];
		Double[] p_cost = new Double[12];
		Double[] p_vgp = new Double[12];
		Double[] vgpm = new Double[12];

		Vector table = new Vector();

		String cost_series = "";
		String revenue_series = "";
		String month_series = "";
		String vgp_series = "";

		String series_del1 = "";
		String series_del2 = "";

		NumberFormat nf = NumberFormat.getCurrencyInstance();
		NumberFormat np = NumberFormat.getPercentInstance();
		NumberFormat nd = new DecimalFormat("0.00");

		String[] month = new String[12];
		String[] month_name = new String[12];
		String start_year = "";
		String end_year = "";

		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat df2 = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat df3 = new SimpleDateFormat("MMM");
		DateFormat df4 = new SimpleDateFormat("yyyy");

		Date temp = null;
		for (int i = 0; i < rows.size(); i++) {
			Vector row = (Vector) rows.get(i);
			try {
				temp = df1.parse(((String) row.get(0)).substring(0, 10));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			month[i] = df2.format(temp);
			month_name[i] = df3.format(temp);
			if (i == 0) {
				start_year = df4.format(temp);
			}
			if (i == rows.size() - 1) {
				end_year = df4.format(temp);
			}
			cost[i] = Double.valueOf((String) row.get(1));
			revenue[i] = Double.valueOf((String) row.get(2));
			vgp[i] = Double.valueOf(revenue[i].doubleValue()
					- cost[i].doubleValue());
			vgpm[i] = Double.valueOf(revenue[i].doubleValue() > 0.0D ? 1.0D
					- cost[i].doubleValue() / revenue[i].doubleValue() : 0.0D);
			p_vgp[i] = Double.valueOf(revenue[i].doubleValue() > 0.0D ? vgp[i]
					.doubleValue() / revenue[i].doubleValue() : 0.0D);
			p_cost[i] = Double
					.valueOf(revenue[i].doubleValue() > 0.0D ? cost[i]
							.doubleValue() / revenue[i].doubleValue() : 0.0D);

			String[] tr = new String[7];
			tr[0] = month[i];
			tr[1] = nf.format(revenue[i]);
			tr[2] = nf.format(cost[i]);
			tr[3] = np.format(p_cost[i]);
			tr[4] = nf.format(vgp[i]);
			tr[5] = np.format(p_vgp[i]);
			tr[6] = nd.format(vgpm[i]);
			table.add(tr);
		}
		Double max = revenue[0];
		Double min = cost[0];
		Double middle = Double.valueOf((max.doubleValue() - min.doubleValue())
				/ 2.0D + min.doubleValue());
		for (int i = 1; i < cost.length; i++) {
			if (cost[i].doubleValue() < min.doubleValue()) {
				min = cost[i];
			}
			if (revenue[i].doubleValue() > max.doubleValue()) {
				max = revenue[i];
			}
		}
		for (int i = 1; i < vgp.length; i++) {
			if (vgp[i].doubleValue() < min.doubleValue()) {
				min = vgp[i];
			}
		}
		Double norm_value_denominator = Double.valueOf(max.doubleValue()
				- min.doubleValue());
		for (int i = 0; i < cost.length; i++) {
			Double norm_value = Double
					.valueOf(Math.rint((cost[i].doubleValue() - min
							.doubleValue())
							/ norm_value_denominator.doubleValue() * 100.0D));
			if (norm_value.doubleValue() == 0.0D) {
				norm_value = Double.valueOf(1.0D);
			}
			cost_series = cost_series + series_del1 + norm_value.toString();

			norm_value = Double
					.valueOf(Math.rint((revenue[i].doubleValue() - min
							.doubleValue())
							/ norm_value_denominator.doubleValue() * 100.0D));
			if (norm_value.doubleValue() == 0.0D) {
				norm_value = Double.valueOf(1.0D);
			}
			revenue_series = revenue_series + series_del1
					+ norm_value.toString();

			norm_value = Double.valueOf(Math.rint((vgp[i].doubleValue() - min
					.doubleValue())
					/ norm_value_denominator.doubleValue()
					* 100.0D));
			if (norm_value.doubleValue() == 0.0D) {
				norm_value = Double.valueOf(1.0D);
			}
			vgp_series = vgp_series + series_del1 + norm_value.toString();

			month_series = month_series + series_del2 + month_name[i];

			series_del1 = ",";
			series_del2 = "|";
		}
		String chd = revenue_series + "|" + cost_series + "|" + vgp_series;

		String chxl_0 = month_series;
		min = Double.valueOf(Math.rint(min.doubleValue() / 1000.0D));
		max = Double.valueOf(Math.rint(max.doubleValue() / 1000.0D));
		middle = Double.valueOf(Math.rint(middle.doubleValue() / 1000.0D));

		String chxl_1 = nf.format(min) + "K|" + nf.format(middle) + "K|"
				+ nf.format(max) + "K";
		String chxl_2 = start_year + "|" + end_year;

		String img = "<img src=\"http://chart.apis.google.com/chart?cht=bvg&amp;chs=625x250&amp;chd=t:"
				+

				chd
				+ "&amp;"
				+ "chxt=x,y,x&amp;"
				+ "chxl=0:|"
				+ chxl_0
				+ "|1:|"
				+ chxl_1
				+ "|2:|2008|||||||||||2009&amp;"
				+ "chbh=10,1,6&amp;"
				+ "chco=00cc00,ff0000,0000ff&amp;"
				+ "chdl=Revenue|Cost|VGP&amp;"
				+ "chtt=Revenue,+Cost,+and+VGP|Based+on+Offsite Date&amp;"
				+ "alt=\"Offsite Revenue for Completed/Closed\"/>";

		this.Request.setAttribute("chart", img);
		this.Request.setAttribute("ttm_table", table);
	}

	void buildViewData(Vector rows) {
		Vector table = new Vector();

		NumberFormat nf = NumberFormat.getCurrencyInstance();
		NumberFormat np = NumberFormat.getPercentInstance();
		NumberFormat nd = new DecimalFormat("0.00");
		for (int i = 0; i < rows.size(); i++) {
			Vector row = (Vector) rows.get(i);

			Double cost = Double.valueOf((String) row.get(3));
			Double revenue = Double.valueOf((String) row.get(2));
			Double vgp = Double.valueOf(revenue.doubleValue()
					- cost.doubleValue());
			Double vgpm = Double.valueOf(revenue.doubleValue() > 0.0D ? 1.0D
					- cost.doubleValue() / revenue.doubleValue() : 0.0D);
			Double p_vgp = Double.valueOf(revenue.doubleValue() > 0.0D ? vgp
					.doubleValue() / revenue.doubleValue() : 0.0D);
			Double p_cost = Double.valueOf(revenue.doubleValue() > 0.0D ? cost
					.doubleValue() / revenue.doubleValue() : 0.0D);

			String[] tr = new String[7];
			tr[0] = ((String) row.get(0));
			tr[1] = nf.format(revenue);
			tr[2] = nf.format(cost);
			tr[3] = np.format(p_cost);
			tr[4] = nf.format(vgp);
			tr[5] = np.format(p_vgp);
			tr[6] = nd.format(vgpm);
			table.add(tr);
		}
		this.Request.setAttribute("default_view", table);
	}

	void buildGeneralView(Vector rows) {
		int dyn_count = 0;
		Vector d_tr = new Vector();
		if (this.Field.size() > 0) {
			dyn_count = this.Field.size();
		}
		Vector table = new Vector();

		NumberFormat nf = NumberFormat.getCurrencyInstance();
		NumberFormat np = NumberFormat.getPercentInstance();
		NumberFormat nd = new DecimalFormat("0.00");
		for (int i = 0; i < rows.size(); i++) {
			Vector row = (Vector) rows.get(i);
			d_tr = new Vector();
			if (dyn_count > 0) {
				for (int d = 0; d < this.Field.size(); d++) {
					d_tr.add(row.get(d));
				}
			}
			Double cost = Double
					.valueOf(row.get(dyn_count - 1) != null ? Double.valueOf(
							(String) row.get(dyn_count - 1)).doubleValue()
							: 0.0D);
			Double revenue = Double
					.valueOf(row.get(dyn_count - 2) != null ? Double.valueOf(
							(String) row.get(dyn_count - 2)).doubleValue()
							: 0.0D);

			Double vgp = Double.valueOf(revenue.doubleValue()
					- cost.doubleValue());
			Double vgpm = Double.valueOf(revenue.doubleValue() > 0.0D ? 1.0D
					- cost.doubleValue() / revenue.doubleValue() : 0.0D);
			Double p_vgp = Double.valueOf(revenue.doubleValue() > 0.0D ? vgp
					.doubleValue() / revenue.doubleValue() : 0.0D);
			Double p_cost = Double.valueOf(revenue.doubleValue() > 0.0D ? cost
					.doubleValue() / revenue.doubleValue() : 0.0D);

			String[] tr = new String[6];
			tr[0] = nf.format(revenue);
			tr[1] = nf.format(cost);
			tr[2] = np.format(p_cost);
			tr[3] = nf.format(vgp);
			tr[4] = np.format(p_vgp);
			tr[5] = nd.format(vgpm);

			table.add(tr);
		}
		this.Request.setAttribute("general", table);
		if (dyn_count > 0) {
			this.Request.setAttribute("general-d_tr", d_tr);
			this.Request.setAttribute("DynamicColumnTitles",
					this.DynamicColumnTitles);
		}
	}

	void buildResultList() {
		this.offsite.clear();
		this.complete.clear();
		this.closed.clear();
		this.bdm.clear();
		this.spm.clear();

		Map ResultMap = new HashMap();

		getResultSetDV(
				buildDefaultSQL("dw_cs_lm_js_offsite_date", "dw_cs_bdm"),
				this.offsite, this.bdm);
		getResultSetDV(
				buildDefaultSQL("dw_cs_lm_js_submitted_date", "dw_cs_bdm"),
				this.complete, this.bdm);
		getResultSetDV(buildDefaultSQL("dw_cs_lm_js_closed_date", "dw_cs_bdm"),
				this.closed, this.bdm);

		System.out.println(buildDefaultSQL("dw_cs_lm_js_submitted_date",
				"dw_cs_bdm"));
		System.out.println(buildDefaultSQL("dw_cs_lm_js_closed_date",
				"dw_cs_bdm"));

		ResultMap.put("BDM", buildTableResults(this.bdm));

		getResultSetDV(
				buildDefaultSQL("dw_cs_lm_js_offsite_date", "dw_cs_spm"),
				this.offsite, this.spm);
		getResultSetDV(
				buildDefaultSQL("dw_cs_lm_js_submitted_date", "dw_cs_spm"),
				this.complete, this.spm);
		getResultSetDV(buildDefaultSQL("dw_cs_lm_js_closed_date", "dw_cs_spm"),
				this.closed, this.spm);

		System.out.println(buildDefaultSQL("dw_cs_lm_js_offsite_date",
				"dw_cs_spm"));
		System.out.println(buildDefaultSQL("dw_cs_lm_js_submitted_date",
				"dw_cs_spm"));
		System.out.println(buildDefaultSQL("dw_cs_lm_js_closed_date",
				"dw_cs_spm"));

		ResultMap.put("SPM", buildTableResults(this.spm));

		this.Request.setAttribute("ResultMap", ResultMap);
		this.Request.setAttribute("bdm", this.bdm);
		this.Request.setAttribute("spm", this.spm);
	}

	boolean getResultSetDV(String sql, Map collector, Vector names) {
		boolean rtn_status = false;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				Double[] row = new Double[2];

				String name = rs.getString("name");
				row[0] = Double.valueOf(rs
						.getDouble("dw_cs_best_cost_estimate"));
				row[1] = Double.valueOf(rs.getDouble("dw_cs_revenue"));
				if (!names.contains(name)) {
					names.add(name);
				}
				collector.put(name, row);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
		return rtn_status;
	}

	Vector buildTableResults(Vector names) {
		String name = "";
		Double[] financials = new Double[2];

		Vector table = new Vector();
		Double[] t_financials = { Double.valueOf(0.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D), Double.valueOf(0.0D) };
		Double rev = Double.valueOf(0.0D);
		Double cost = Double.valueOf(0.0D);

		NumberFormat nf = NumberFormat.getCurrencyInstance();
		NumberFormat np = NumberFormat.getPercentInstance();
		NumberFormat nd = new DecimalFormat("0.00");
		for (int i = 0; i < names.size(); i++) {
			String[] tr = new String[19];

			name = (String) names.get(i);
			tr[0] = name;
			if (this.offsite.get(name) != null) {
				financials = (Double[]) this.offsite.get(name);
				Double vgp = Double.valueOf(financials[1].doubleValue()
						- financials[0].doubleValue());
				Double vgpm = Double
						.valueOf(financials[1].doubleValue() > 0.0D ? 1.0D
								- financials[0].doubleValue()
								/ financials[1].doubleValue() : 0.0D);
				Double p_vgp = Double
						.valueOf(financials[1].doubleValue() > 0.0D ? vgp
								.doubleValue() / financials[1].doubleValue()
								: 0.0D);
				Double p_cost = Double
						.valueOf(financials[1].doubleValue() > 0.0D ? financials[0]
								.doubleValue() / financials[1].doubleValue()
								: 0.0D);
				int tmp276_275 = 0;
				Double[] tmp276_273 = t_financials;
				tmp276_273[tmp276_275] = Double.valueOf(tmp276_273[tmp276_275]
						.doubleValue() + financials[0].doubleValue());
				int tmp295_294 = 1;
				Double[] tmp295_292 = t_financials;
				tmp295_292[tmp295_294] = Double.valueOf(tmp295_292[tmp295_294]
						.doubleValue() + financials[1].doubleValue());

				tr[1] = nf.format(financials[0]);
				tr[2] = nf.format(financials[1]);
				tr[3] = np.format(p_cost);
				tr[4] = nf.format(vgp);
				tr[5] = np.format(p_vgp);
				tr[6] = nd.format(vgpm);
			} else {
				tr[1] = "&nbsp";
				tr[2] = "&nbsp";
				tr[3] = "&nbsp";
				tr[4] = "&nbsp";
				tr[5] = "&nbsp";
				tr[6] = "&nbsp";
			}
			if (this.complete.get(name) != null) {
				financials = (Double[]) this.complete.get(name);
				Double vgp = Double.valueOf(financials[1].doubleValue()
						- financials[0].doubleValue());
				Double vgpm = Double
						.valueOf(financials[1].doubleValue() > 0.0D ? 1.0D
								- financials[0].doubleValue()
								/ financials[1].doubleValue() : 0.0D);
				Double p_vgp = Double
						.valueOf(financials[1].doubleValue() > 0.0D ? vgp
								.doubleValue() / financials[1].doubleValue()
								: 0.0D);
				Double p_cost = Double
						.valueOf(financials[1].doubleValue() > 0.0D ? financials[0]
								.doubleValue() / financials[1].doubleValue()
								: 0.0D);
				int tmp574_573 = 2;
				Double[] tmp574_571 = t_financials;
				tmp574_571[tmp574_573] = Double.valueOf(tmp574_571[tmp574_573]
						.doubleValue() + financials[0].doubleValue());
				int tmp593_592 = 3;
				Double[] tmp593_590 = t_financials;
				tmp593_590[tmp593_592] = Double.valueOf(tmp593_590[tmp593_592]
						.doubleValue() + financials[1].doubleValue());

				tr[7] = nf.format(financials[0]);
				tr[8] = nf.format(financials[1]);
				tr[9] = np.format(p_cost);
				tr[10] = nf.format(vgp);
				tr[11] = np.format(p_vgp);
				tr[12] = nd.format(vgpm);
			} else {
				tr[7] = "&nbsp";
				tr[8] = "&nbsp";
				tr[9] = "&nbsp";
				tr[10] = "&nbsp";
				tr[11] = "&nbsp";
				tr[12] = "&nbsp";
			}
			if (this.closed.get(name) != null) {
				financials = (Double[]) this.closed.get(name);
				Double vgp = Double.valueOf(financials[1].doubleValue()
						- financials[0].doubleValue());
				Double vgpm = Double
						.valueOf(financials[1].doubleValue() > 0.0D ? 1.0D
								- financials[0].doubleValue()
								/ financials[1].doubleValue() : 0.0D);
				Double p_vgp = Double
						.valueOf(financials[1].doubleValue() > 0.0D ? vgp
								.doubleValue() / financials[1].doubleValue()
								: 0.0D);
				Double p_cost = Double
						.valueOf(financials[1].doubleValue() > 0.0D ? financials[0]
								.doubleValue() / financials[1].doubleValue()
								: 0.0D);
				int tmp882_881 = 4;
				Double[] tmp882_879 = t_financials;
				tmp882_879[tmp882_881] = Double.valueOf(tmp882_879[tmp882_881]
						.doubleValue() + financials[0].doubleValue());
				int tmp901_900 = 5;
				Double[] tmp901_898 = t_financials;
				tmp901_898[tmp901_900] = Double.valueOf(tmp901_898[tmp901_900]
						.doubleValue() + financials[1].doubleValue());

				tr[13] = nf.format(financials[0]);
				tr[14] = nf.format(financials[1]);
				tr[15] = np.format(p_cost);
				tr[16] = nf.format(vgp);
				tr[17] = np.format(p_vgp);
				tr[18] = nd.format(vgpm);
			} else {
				tr[13] = "&nbsp";
				tr[14] = "&nbsp";
				tr[15] = "&nbsp";
				tr[16] = "&nbsp";
				tr[17] = "&nbsp";
				tr[18] = "&nbsp";
			}
			table.add(tr);
		}
		String[] tr = new String[19];
		tr[0] = "Totals";

		rev = t_financials[1];
		cost = t_financials[0];
		Double vgp = Double.valueOf(rev.doubleValue()
				- financials[0].doubleValue());
		Double vgpm = Double.valueOf(rev.doubleValue() > 0.0D ? 1.0D
				- cost.doubleValue() / rev.doubleValue() : 0.0D);
		Double p_vgp = Double.valueOf(rev.doubleValue() > 0.0D ? vgp
				.doubleValue() / rev.doubleValue() : 0.0D);
		Double p_cost = Double.valueOf(rev.doubleValue() > 0.0D ? cost
				.doubleValue() / rev.doubleValue() : 0.0D);
		tr[1] = nf.format(cost);
		tr[2] = nf.format(rev);
		tr[3] = np.format(p_cost);
		tr[4] = nf.format(vgp);
		tr[5] = np.format(p_vgp);
		tr[6] = nd.format(vgpm);

		rev = t_financials[3];
		cost = t_financials[2];
		vgp = Double.valueOf(rev.doubleValue() - financials[0].doubleValue());
		vgpm = Double.valueOf(rev.doubleValue() > 0.0D ? 1.0D
				- cost.doubleValue() / rev.doubleValue() : 0.0D);
		p_vgp = Double.valueOf(rev.doubleValue() > 0.0D ? vgp.doubleValue()
				/ rev.doubleValue() : 0.0D);
		p_cost = Double.valueOf(rev.doubleValue() > 0.0D ? cost.doubleValue()
				/ rev.doubleValue() : 0.0D);
		tr[7] = nf.format(cost);
		tr[8] = nf.format(rev);
		tr[9] = np.format(p_cost);
		tr[10] = nf.format(vgp);
		tr[11] = np.format(p_vgp);
		tr[12] = nd.format(vgpm);

		rev = t_financials[5];
		cost = t_financials[4];
		vgp = Double.valueOf(rev.doubleValue() - financials[0].doubleValue());
		vgpm = Double.valueOf(rev.doubleValue() > 0.0D ? 1.0D
				- cost.doubleValue() / rev.doubleValue() : 0.0D);
		p_vgp = Double.valueOf(rev.doubleValue() > 0.0D ? vgp.doubleValue()
				/ rev.doubleValue() : 0.0D);
		p_cost = Double.valueOf(rev.doubleValue() > 0.0D ? cost.doubleValue()
				/ rev.doubleValue() : 0.0D);
		tr[13] = nf.format(cost);
		tr[14] = nf.format(rev);
		tr[15] = np.format(p_cost);
		tr[16] = nf.format(vgp);
		tr[17] = np.format(p_vgp);
		tr[18] = nd.format(vgpm);

		table.add(tr);

		return table;
	}

	public static void main(String[] args) {
	}
}
