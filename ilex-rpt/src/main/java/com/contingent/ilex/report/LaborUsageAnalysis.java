package com.contingent.ilex.report;

import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import LaborCost.Job2;

public class LaborUsageAnalysis extends DispatchAction {

	static final int ac_du_name = 0;
	static final int pf_qty = 1;
	static final int pf_rev = 2;
	static final int ac_qty = 3;
	static final int ac_rev = 4;
	static final int po_qty = 5;
	static final int po_exp = 6;
	static final int eff_rate = 7;
	static final int cnt = 8;
	static final int pc_id = 9;
	static final int e_vgpm = 10;
	static final int col_jo = 11;
    static DataSource ilexDS = null;
    static DataSource ilexMainDS = null;
	DecimalFormat sd = new DecimalFormat("#,##0.00");
	DecimalFormat sd2 = new DecimalFormat("#,##0");
	SimpleDateFormat df1 = new SimpleDateFormat("MM/dd/yyyy");
	static DataAccess da = new DataAccess();

	public ActionForward lua(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "";
		initialize(request);

		String rpt = getRequestParameter(request, "rpt");
		if (rpt.equals("1")) {
			forward_key = buildTopLevelSummary(request);
		} else if ((rpt.equals("2")) || (rpt.equals("21"))) {
			forward_key = buildAppendixLevelSummary(request);
		} else if (rpt.equals("3")) {
			manageJobResourceDetails(request);

			forward_key = "lua33";
		} else if (!rpt.equals("4")) {
			if (rpt.equals("5")) {
				forward_key = buildSPMSummary(request);
			} else if (rpt.equals("6")) {
				forward_key = buildJobOwnerSummary(request);
			} else if (rpt.equals("7")) {
				forward_key = buildTrendCharts(request);
			} else if (rpt.equals("51")) {
				forward_key = buildTrendChartsData_dg1(request);
			} else if (rpt.equals("52")) {
				forward_key = buildTrendChartsData_dg2(request);
			} else if (rpt.equals("53")) {
				forward_key = buildPieChartsData_dg53(request, 2);
			} else if (rpt.equals("54")) {
				forward_key = buildPieChartsData_dg53(request, 1);
			} else if (rpt.equals("11")) {
				buildTopLevelSummary(request);
				forward_key = "lua11";
			} else if (rpt.equals("12")) {
				forward_key = "lua12";
			} else if (rpt.equals("70")) {
				forward_key = buildDetails(request);
			} else if (rpt.equals("71")) {
				forward_key = buildDetails(request);
			}else if (rpt.equals("701")) {
				forward_key = buildTrendCharts_html(request);
			}
		}
		return mapping.findForward(forward_key);
	}

	String buildTopLevelSummary(HttpServletRequest request) {
		String key = "lua1";

		getExceptionList(request);

		return key;
	}

	void getExceptionList(HttpServletRequest request) {
		String table_data = "";
		String style = "";
		String elr = "0.00";
		Double s2 = Double.valueOf(0.0D);
		Double s3 = Double.valueOf(0.0D);
		Double s4 = Double.valueOf(0.0D);

		String sql = getClientSQL(request);
		try {
			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				elr = effectiveLaborRate(rs.getString("ac_qty"),
						rs.getString("po_exp"));

				style = style.equals("0") ? "1" : "0";

				table_data = table_data
						+ "<tr><td class=\"cDataLeft"
						+ style
						+ "\"><a href=\"LaborUsageAnalysis.xo?ac=lua&rpt=2&client="
						+ rs.getString("lp_mm_id") + "\">"
						+ rs.getString("lo_ot_name") + "</a></td>"
						+ "<td class=\"cDataRight" + style + "\">"
						+ rs.getString("po_exp") + "</td>"
						+ "<td class=\"cDataRight" + style + "\">"
						+ rs.getString("ac_qty") + "</td>"
						+ "<td class=\"cDataRight" + style + "\">" + elr
						+ "</td>" + "<td class=\"cDataRight" + style + "\">"
						+ rs.getString("cnt") + "</td>" + "</tr>";

				s2 = sumColumn(s2, rs.getString("po_exp"));
				s3 = sumColumn(s3, rs.getString("ac_qty"));
				s4 = sumColumn(s4, rs.getString("cnt"));
			}
			rs.close();
			if (table_data.equals("")) {
				table_data = "<tr><td colspan=\"6\" class=\"cDataLeft0\"> No data available.</td></tr>";
			} else {
				if (s3.doubleValue() > 0.0D) {
					elr = this.sd.format(s2.doubleValue() / s3.doubleValue());
				}
				style = style.equals("0") ? "1" : "0";
				table_data = table_data + "<tr><td class=\"cDataLeft" + style
						+ "\"><b>Total</b></td>" + "<td class=\"cDataRight"
						+ style + "\"><b>" + this.sd.format(s2) + "</b></td>"
						+ "<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s3) + "</b></td>"
						+ "<td class=\"cDataRight" + style + "\"><b>" + elr
						+ "</b></td>" + "<td class=\"cDataRight" + style
						+ "\"><b>" + this.sd2.format(s4) + "</b></td>"
						+ "</tr>";
			}
			request.setAttribute("table_data", table_data);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	String buildAppendixLevelSummary(HttpServletRequest request) {
		String key = "lua2";

		getExceptionListAppenidx(request);

		return key;
	}

	void getExceptionListAppenidx(HttpServletRequest request) {
		String table_data = "";
		String style = "";
		String elr = "0.00";
		Double s2 = Double.valueOf(0.0D);
		Double s3 = Double.valueOf(0.0D);
		Double s4 = Double.valueOf(0.0D);
		Double s5 = Double.valueOf(0.0D);
		Double s6 = Double.valueOf(0.0D);
		Double s7 = Double.valueOf(0.0D);
		Double s9 = Double.valueOf(0.0D);
		String display_client = "";
		String subtitle = "";

		String sql = getClientAppendixSQL(request);
		try {
			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				elr = effectiveLaborRate(rs.getString("ac_qty"),
						rs.getString("po_exp"));

				style = style.equals("0") ? "1" : "0";
				display_client = rs.getString("lo_ot_name");
				subtitle = URLEncoder.encode(rs.getString("lo_ot_name") + ", "
						+ rs.getString("lx_pr_title"));

				table_data = table_data
						+ "<tr><td class=\"cDataLeft"
						+ style
						+ "\">"
						+ rs.getString("lo_ot_name")
						+ "</a></td>"
						+ "<td class=\"cDataLeft"
						+ style
						+ "\"><a href=\"LaborUsageAnalysis.xo?ac=lua&rpt=71&project="
						+ rs.getString("lx_pr_id") + "&subtitle=" + subtitle
						+ "\">" + rs.getString("lx_pr_title") + "</a></td>" +

						"<td class=\"cDataRight" + style + "\">"
						+ rs.getString("pf_qty") + "</td>"
						+ "<td class=\"cDataRight" + style + "\">"
						+ rs.getString("pf_rev") + "</td>" +

						"<td class=\"cDataRight" + style + "\">"
						+ rs.getString("ac_qty") + "</td>"
						+ "<td class=\"cDataRight" + style + "\">"
						+ rs.getString("ac_rev") + "</td>" +

						"<td class=\"cDataRight" + style + "\">"
						+ rs.getString("po_qty") + "</td>"
						+ "<td class=\"cDataRight" + style + "\">"
						+ rs.getString("po_exp") + "</td>" +

						"<td class=\"cDataRight" + style + "\">" + elr
						+ "</td>" + "<td class=\"cDataRight" + style + "\">"
						+ rs.getString("cnt") + "</td>" + "</tr>";

				s2 = sumColumn(s2, rs.getString("pf_qty"));
				s3 = sumColumn(s3, rs.getString("pf_rev"));
				s4 = sumColumn(s4, rs.getString("ac_qty"));
				s5 = sumColumn(s5, rs.getString("ac_rev"));
				s6 = sumColumn(s6, rs.getString("po_qty"));
				s7 = sumColumn(s7, rs.getString("po_exp"));
				s9 = sumColumn(s9, rs.getString("cnt"));
			}
			rs.close();
			if (table_data.equals("")) {
				table_data = "<tr><td colspan=\"6\" class=\"cDataLeft0\"> No data available.</td></tr>";
			} else {
				if (s3.doubleValue() > 0.0D) {
					elr = this.sd.format(s7.doubleValue() / s4.doubleValue());
				}
				style = style.equals("0") ? "1" : "0";
				table_data = table_data
						+ "<tr><td colspan=\"2\" class=\"cDataLeft" + style
						+ "\"><b>Total</b></td>" + "<td class=\"cDataRight"
						+ style + "\"><b>" + this.sd.format(s2) + "</b></td>"
						+ "<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s3) + "</b></td>" +

						"<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s4) + "</b></td>"
						+ "<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s5) + "</b></td>" +

						"<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s6) + "</b></td>"
						+ "<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s7) + "</b></td>" +

						"<td class=\"cDataRight" + style + "\"><b>" + elr
						+ "</b></td>" + "<td class=\"cDataRight" + style
						+ "\"><b>" + this.sd2.format(s9) + "</b></td>"
						+ "</tr>";
			}
			request.setAttribute("table_data", table_data);
			request.setAttribute("display_client", display_client);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	void manageJobResourceDetails(HttpServletRequest request) {
		String[] job = new String[3];

		String lm_js_id = getRequestParameter(request, "job");
		if (!lm_js_id.equals("")) {
			job[0] = lm_js_id;
			getDetails(request, job);
		}
		System.out.println(lm_js_id);
	}

	void getDetails(HttpServletRequest request, String[] job) {
		Job2 j = new Job2(this.da, job[0], 0);

		request.setAttribute("JobTitle", j.JobTitle);
		request.setAttribute("Customer", j.Customer);
		request.setAttribute("SiteNumber", j.SiteNumber + ", "
				+ j.LocationDetails);

		request.setAttribute("InvoiceDate", j.InvoiceDate.substring(0, 16));
		request.setAttribute("JobOwner", j.JobOwner);
		request.setAttribute("Criticality", j.Criticality);
		request.setAttribute("NonPPS", j.NonPPS);

		request.setAttribute("PO", j.PO);

		request.setAttribute("ProFormaCost", this.sd.format(j.ProFormaCost));
		request.setAttribute("ActualCost", this.sd.format(j.ActualCost));
		request.setAttribute("Revenue", this.sd.format(j.Revenue));
		request.setAttribute("VGPM", this.sd.format(j.VGPM));
		request.setAttribute("PGPM", this.sd.format(j.PGPM));

		request.setAttribute("POTableMap", j.POTableMap);
	}

	String buildJobOwnerDrillDown(HttpServletRequest request) {
		String key = "lua4";

		String po_type = getRequestParameter(request, "po_type");
		if (po_type.equals("")) {
			po_type = "M";
		}
		getExceptionListJobOwner(request, po_type);

		request.setAttribute("po_type", po_type);

		return key;
	}

	void getExceptionListJobOwner(HttpServletRequest request, String po_type) {
		Vector<String[]> rows = new Vector();
		String[] row = new String[10];

		String sql = "{call datawarehouse.dbo.ax_labor_usage_job_owner_01(?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.setObject("@po_type", po_type);

			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				row = new String[10];
				for (int i = 1; i < row.length; i++) {
					row[i] = rs.getString(i);
				}
				rows.add(row);
			}
			stmt.close();

			request.setAttribute("rows", rows);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	String buildSPMSummary(HttpServletRequest request) {
		String key = "lua5";

		getExceptionListSpm(request);

		return key;
	}

	void getExceptionListSpm(HttpServletRequest request) {
		String table_data = "";
		String style = "";
		String elr = "0.00";
		Double s2 = Double.valueOf(0.0D);
		Double s3 = Double.valueOf(0.0D);
		Double s4 = Double.valueOf(0.0D);
		Double s5 = Double.valueOf(0.0D);
		Double s6 = Double.valueOf(0.0D);
		Double s7 = Double.valueOf(0.0D);
		Double s9 = Double.valueOf(0.0D);
		String display_spm = "";

		String sql = getSpmSQL(request);
		try {
			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				elr = effectiveLaborRate(rs.getString("ac_qty"),
						rs.getString("po_exp"));

				style = style.equals("0") ? "1" : "0";

				table_data = table_data
						+ "<tr><td class=\"cDataLeft"
						+ style
						+ "\"><a href=\"LaborUsageAnalysis.xo?ac=lua&rpt=21&spm="
						+ rs.getString("ac_du_lo_pc_id") + "\">"
						+ rs.getString("ac_du_name") + "</a></td>" +

						"<td class=\"cDataRight" + style + "\">"
						+ rs.getString("pf_qty") + "</td>"
						+ "<td class=\"cDataRight" + style + "\">"
						+ rs.getString("pf_rev") + "</td>" +

						"<td class=\"cDataRight" + style + "\">"
						+ rs.getString("ac_qty") + "</td>"
						+ "<td class=\"cDataRight" + style + "\">"
						+ rs.getString("ac_rev") + "</td>" +

						"<td class=\"cDataRight" + style + "\">"
						+ rs.getString("po_qty") + "</td>"
						+ "<td class=\"cDataRight" + style + "\">"
						+ rs.getString("po_exp") + "</td>" +

						"<td class=\"cDataRight" + style + "\">" + elr
						+ "</td>" + "<td class=\"cDataRight" + style + "\">"
						+ rs.getString("cnt") + "</td>" + "</tr>";

				s2 = sumColumn(s2, rs.getString("pf_qty"));
				s3 = sumColumn(s3, rs.getString("pf_rev"));
				s4 = sumColumn(s4, rs.getString("ac_qty"));
				s5 = sumColumn(s5, rs.getString("ac_rev"));
				s6 = sumColumn(s6, rs.getString("po_qty"));
				s7 = sumColumn(s7, rs.getString("po_exp"));
				s9 = sumColumn(s9, rs.getString("cnt"));

				display_spm = rs.getString("ac_du_name");
			}
			rs.close();
			if (table_data.equals("")) {
				table_data = "<tr><td colspan=\"6\" class=\"cDataLeft0\"> No data available.</td></tr>";
			} else {
				if (s3.doubleValue() > 0.0D) {
					elr = this.sd.format(s7.doubleValue() / s4.doubleValue());
				}
				style = style.equals("0") ? "1" : "0";
				table_data = table_data + "<tr><td class=\"cDataLeft" + style
						+ "\"><b>Total</b></td>" + "<td class=\"cDataRight"
						+ style + "\"><b>" + this.sd.format(s2) + "</b></td>"
						+ "<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s3) + "</b></td>" +

						"<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s4) + "</b></td>"
						+ "<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s5) + "</b></td>" +

						"<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s6) + "</b></td>"
						+ "<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s7) + "</b></td>" +

						"<td class=\"cDataRight" + style + "\"><b>" + elr
						+ "</b></td>" + "<td class=\"cDataRight" + style
						+ "\"><b>" + this.sd2.format(s9) + "</b></td>"
						+ "</tr>";
			}
			request.setAttribute("table_data", table_data);
			request.setAttribute("display_client", display_spm);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	String buildJobOwnerSummary(HttpServletRequest request) {
		String key = "lua6";

		getExceptionListJODetail(request);

		return key;
	}

	void getExceptionListJODetail(HttpServletRequest request) {
		String table_data = "";
		String style = "";
		String display_spm = "";
		Double tot = Double.valueOf(0.0D);
		Double rev = Double.valueOf(0.0D);
		Double exp = Double.valueOf(0.0D);
		Double per = Double.valueOf(0.0D);
		Double t1 = Double.valueOf(0.0D);
		Double t2 = Double.valueOf(0.0D);
		String vgpm_all = "";
		String vgpm_correction = "";

		Vector<String[]> result_set = getExceptionListJODetailData1(request);

		Map<String, Map<String, String>> result_set2 = getExceptionListJODetailData2(request);
		Map<String, String> row2 = new HashMap();
		if (result_set.size() > 0) {
			for (int i = 0; i < result_set.size() - 1; i++) {
				String[] row = (String[]) result_set.get(i);
				row2 = (Map) result_set2.get(row[9]);
				if (row2 == null) {
					vgpm_correction = "0.00";
				} else {
					vgpm_correction = (String) row2.get("vgpm");
				}
				try {
					t1 = Double.valueOf(Double.parseDouble(row[8]));
					t2 = Double.valueOf(Double.parseDouble((String) row2
							.get("cnt")));
				} catch (Exception e) {
					System.out.println();
				}
				per = Double.valueOf(t2.doubleValue() > 0.0D ? t1.doubleValue()
						/ t2.doubleValue() * 100.0D : 0.0D);

				style = style.equals("0") ? "1" : "0";

				table_data = table_data
						+ "<tr><td class=\"cDataLeft"
						+ style
						+ "\"><a href=\"LaborUsageAnalysis.xo?ac=lua&rpt=70&jo="
						+ row[9] + "\">" + row[0] + "</a></td>" +

						"<td class=\"cDataRight" + style + "\">" + row[1]
						+ "</td>" + "<td class=\"cDataRight" + style + "\">"
						+ row[2] + "</td>" +

						"<td class=\"cDataRight" + style + "\">" + row[3]
						+ "</td>" + "<td class=\"cDataRight" + style + "\">"
						+ row[4] + "</td>" +

						"<td class=\"cDataRight" + style + "\">" + row[5]
						+ "</td>" + "<td class=\"cDataRight" + style + "\">"
						+ row[6] + "</td>" +

						"<td class=\"cDataRight" + style + "\">" + row[7]
						+ "</td>" + "<td class=\"cDataRight" + style + "\">"
						+ row[8] + "</td>" +

						"<td class=\"cDataRight" + style + "\">"
						+ this.sd2.format(per) + "%</td>"
						+ "<td class=\"cDataRight" + style + "\">" + row[10]
						+ "</td>" + "<td class=\"cDataRight" + style + "\">"
						+ vgpm_correction + "</td>" + "</tr>";
				if (row2 != null) {
					tot = sumColumn(tot, (String) row2.get("cnt"));
					rev = sumColumn(rev, (String) row2.get("rev"));
					exp = sumColumn(exp, (String) row2.get("exp"));
				}
			}
			if (rev.doubleValue() > 0.0D) {
				vgpm_all = this.sd.format(1.0D - exp.doubleValue()
						/ rev.doubleValue());
			} else {
				vgpm_all = "N/A";
			}
			String[] row = (String[]) result_set.lastElement();
			row2 = (Map) result_set2.get("total");

			t1 = Double.valueOf(Double.parseDouble(row[8]));
			per = Double.valueOf(t2.doubleValue() > 0.0D ? t1.doubleValue()
					/ tot.doubleValue() * 100.0D : 0.0D);

			style = style.equals("0") ? "1" : "0";
			table_data = table_data + "<tr><td class=\"cDataLeft" + style
					+ "\"><b>Total</b></td>" + "<td class=\"cDataRight" + style
					+ "\"><b>" + row[1] + "</b></td>"
					+ "<td class=\"cDataRight" + style + "\"><b>" + row[2]
					+ "</b></td>" +

					"<td class=\"cDataRight" + style + "\"><b>" + row[3]
					+ "</b></td>" + "<td class=\"cDataRight" + style + "\"><b>"
					+ row[4] + "</b></td>" +

					"<td class=\"cDataRight" + style + "\"><b>" + row[5]
					+ "</b></td>" + "<td class=\"cDataRight" + style + "\"><b>"
					+ row[6] + "</b></td>" +

					"<td class=\"cDataRight" + style + "\"><b>" + row[7]
					+ "</b></td>" + "<td class=\"cDataRight" + style + "\"><b>"
					+ this.sd2.format(t1) + "</b></td>"
					+ "<td class=\"cDataRight" + style + "\"><b>"
					+ this.sd2.format(per) + "%</b></td>"
					+ "<td class=\"cDataRight" + style + "\"><b>" + row[10]
					+ "</b></td>" + "<td class=\"cDataRight" + style + "\"><b>"
					+ vgpm_all + "</b></td>" +

					"</tr>";
		} else {
			table_data = "<tr><td colspan=\"6\" class=\"cDataLeft0\"> No data available.</td></tr>";
		}
		request.setAttribute("table_data", table_data);
	}

	Vector<String[]> getExceptionListJODetailData1(HttpServletRequest request) {
		Vector<String[]> result_set = new Vector();

		Double s2 = Double.valueOf(0.0D);
		Double s3 = Double.valueOf(0.0D);
		Double s4 = Double.valueOf(0.0D);
		Double s5 = Double.valueOf(0.0D);
		Double s6 = Double.valueOf(0.0D);
		Double s7 = Double.valueOf(0.0D);
		Double s9 = Double.valueOf(0.0D);
		Double rev = Double.valueOf(0.0D);
		Double exp = Double.valueOf(0.0D);
		String elr = "";

		String sql = getJoSQL(request);
		try {
			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				String[] row = new String[11];

				row[0] = rs.getString("ac_du_name");
				row[1] = rs.getString("pf_qty");
				row[2] = rs.getString("pf_rev");
				row[3] = rs.getString("ac_qty");
				row[4] = rs.getString("ac_rev");
				row[5] = rs.getString("po_qty");
				row[6] = rs.getString("po_exp");
				row[7] = effectiveLaborRate(rs.getString("ac_qty"),
						rs.getString("po_exp"));
				row[8] = rs.getString("cnt");
				row[9] = rs.getString("ac_du_lo_pc_id");
				row[10] = rs.getString("vgpm");

				s2 = sumColumn(s2, rs.getString("pf_qty"));
				s3 = sumColumn(s3, rs.getString("pf_rev"));
				s4 = sumColumn(s4, rs.getString("ac_qty"));
				s5 = sumColumn(s5, rs.getString("ac_rev"));
				s6 = sumColumn(s6, rs.getString("po_qty"));
				s7 = sumColumn(s7, rs.getString("po_exp"));
				s9 = sumColumn(s9, rs.getString("cnt"));
				rev = sumColumn(rev, rs.getString("rev"));
				exp = sumColumn(exp, rs.getString("exp"));

				result_set.add(row);
			}
			rs.close();
			if (result_set.size() > 0) {
				if (s3.doubleValue() > 0.0D) {
					elr = this.sd.format(s7.doubleValue() / s4.doubleValue());
				} else {
					elr = "Not Defined";
				}
				String[] row = new String[11];

				row[0] = "Total";
				row[1] = this.sd.format(s2);
				row[2] = this.sd.format(s3);
				row[3] = this.sd.format(s4);
				row[4] = this.sd.format(s5);
				row[5] = this.sd.format(s6);
				row[6] = this.sd.format(s7);
				row[7] = elr;
				row[8] = Double.toString(s9.doubleValue());
				row[9] = "";
				if (rev.doubleValue() > 0.0D) {
					row[10] = this.sd.format(1.0D - exp.doubleValue()
							/ rev.doubleValue());
				} else {
					row[10] = "N/A";
				}
				result_set.add(row);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return result_set;
	}

	Map<String, Map<String, String>> getExceptionListJODetailData2(
			HttpServletRequest request) {
		Map<String, Map<String, String>> result_set = new HashMap();

		Double cnt = Double.valueOf(0.0D);
		Double rev = Double.valueOf(0.0D);
		Double exp = Double.valueOf(0.0D);

		String sql = getJoSQLAll(request);
		try {
			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				Map<String, String> row = new HashMap();

				row.put("exp", rs.getString("exp"));
				row.put("rev", rs.getString("rev"));
				row.put("vgpm", rs.getString("vgpm"));
				row.put("cnt", rs.getString("cnt"));

				exp = sumColumn(exp, rs.getString("exp"));
				rev = sumColumn(rev, rs.getString("rev"));
				cnt = sumColumn(cnt, rs.getString("cnt"));

				result_set.put(rs.getString("pc_id"), row);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return result_set;
	}

	String buildDetails(HttpServletRequest request) {
		String key = "lua70";

		getExceptionListDetails(request);

		return key;
	}

	void getExceptionListDetails(HttpServletRequest request) {
		String table_data = "";
		String style = "";
		String elr = "0.00";
		Double s2 = Double.valueOf(0.0D);
		Double s3 = Double.valueOf(0.0D);
		Double s4 = Double.valueOf(0.0D);
		Double s5 = Double.valueOf(0.0D);
		Double s6 = Double.valueOf(0.0D);
		Double s7 = Double.valueOf(0.0D);
		String display_subtitle = "";

		String sql = getDetailsSQL(request);
		try {
			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				elr = effectiveLaborRate(rs.getString("ac_qty"),
						rs.getString("po_exp"));

				style = style.equals("0") ? "1" : "0";

				table_data = table_data
						+ "<tr><td class=\"cDataLeft"
						+ style
						+ "\">"
						+ rs.getString("lm_js_title")
						+ "</td>"
						+ "<td class=\"cDataLeft"
						+ style
						+ "\">"
						+ rs.getString("ac_jd_date_closed")
						+ "</td>"
						+

						"<td class=\"cDataRight"
						+ style
						+ "\">"
						+ rs.getString("pf_qty")
						+ "</td>"
						+ "<td class=\"cDataRight"
						+ style
						+ "\">"
						+ rs.getString("pf_rev")
						+ "</td>"
						+

						"<td class=\"cDataRight"
						+ style
						+ "\">"
						+ rs.getString("ac_qty")
						+ "</td>"
						+ "<td class=\"cDataRight"
						+ style
						+ "\">"
						+ rs.getString("ac_rev")
						+ "</td>"
						+

						"<td class=\"cDataRight"
						+ style
						+ "\">"
						+ rs.getString("po_qty")
						+ "</td>"
						+ "<td class=\"cDataRight"
						+ style
						+ "\">"
						+ rs.getString("po_exp")
						+ "</td>"
						+

						"<td class=\"cDataRight"
						+ style
						+ "\">"
						+ elr
						+ "&nbsp;&nbsp;<img src=\"lua/info.gif\" class=\"job\" jobid=\""
						+ rs.getString("ax_js_id") + "\"></td>" +

						"</tr>";

				s2 = sumColumn(s2, rs.getString("pf_qty"));
				s3 = sumColumn(s3, rs.getString("pf_rev"));
				s4 = sumColumn(s4, rs.getString("ac_qty"));
				s5 = sumColumn(s5, rs.getString("ac_rev"));
				s6 = sumColumn(s6, rs.getString("po_qty"));
				s7 = sumColumn(s7, rs.getString("po_exp"));

				display_subtitle = rs.getString("ac_du_name");
			}
			rs.close();
			if (table_data.equals("")) {
				table_data = "<tr><td colspan=\"6\" class=\"cDataLeft0\"> No data available.</td></tr>";
			} else {
				if (s3.doubleValue() > 0.0D) {
					elr = this.sd.format(s7.doubleValue() / s4.doubleValue());
				}
				style = style.equals("0") ? "1" : "0";
				table_data = table_data + "<tr><td class=\"cDataLeft" + style
						+ "\"><b>Total</b></td>" + "<td class=\"cDataLeft"
						+ style + "\">&nbsp;</td>" +

						"<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s2) + "</b></td>"
						+ "<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s3) + "</b></td>" +

						"<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s4) + "</b></td>"
						+ "<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s5) + "</b></td>" +

						"<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s6) + "</b></td>"
						+ "<td class=\"cDataRight" + style + "\"><b>"
						+ this.sd.format(s7) + "</b></td>" +

						"<td class=\"cDataRight" + style + "\"><b>" + elr
						+ "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"
						+

						"</tr>";
			}
			request.setAttribute("table_data", table_data);
			request.setAttribute("display_subtitle", display_subtitle);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	String buildTrendCharts(HttpServletRequest request) {
		String key = "lua7";

		String po_type = getRequestParameter(request, "po_type");
		if (po_type.equals("")) {
			po_type = "M";
		}
		request.setAttribute("po_type", po_type);
		request.setAttribute("rpt", "7");

		return key;
	}
	
	
	// build trend chart for 701
	String buildTrendCharts_html(HttpServletRequest request) {
		String key = "lua701";

		String po_type = getRequestParameter(request, "po_type");
		if (po_type.equals("")) {
			po_type = "M";
		}
		request.setAttribute("po_type", po_type);
		request.setAttribute("rpt", "7");
		key=buildTrendChartsData_dg1_html(request);
		return key;
	}
	
	
	
	
	
	
	
	
	String buildTrendChartsData_dg1(HttpServletRequest request) {
		String key = "dg1";
		String sql = "{call datawarehouse.dbo.ax_lua_exception_chart1_01(?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.registerOutParameter(1, 12);
			stmt.registerOutParameter(2, 12);
			
			stmt.execute();
			request.setAttribute("chart_data", stmt.getString(1)+stmt.getString(2));
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return key;
	}
	
// function added by html 	
	String buildTrendChartsData_dg1_html(HttpServletRequest request) {
		String key = "lua701";
			//	"dg1";
		ArrayList<LabelValue> listData=new ArrayList<LabelValue>();
		ArrayList<LabelValue> listDataspeed=new ArrayList<LabelValue>();
		ArrayList<LabelValue> listDataminute=new ArrayList<LabelValue>();
		ArrayList<LabelValue> listDatapvs=new ArrayList<LabelValue>();
		ArrayList<String[]> array = new ArrayList<String[]>();
	    
	    
	  //  Object[] array = new Object[list.size()];
	
		String sql="";
		
		
		
		try {
		//	listData=populateTrendChart(sql);
			sql=getQuery("M");
			listDataminute=populateTrendChart(sql);
			String sqls=getQuery("S");
			listDataspeed=populateTrendChart(sqls);
			String sqlp=getQuery("P");
			listDatapvs=populateTrendChart(sqlp);
			for(int i=0;i< listDataspeed.size();i++){
				String[] para = new String[4];
				para[0]=listDataspeed.get(i).getLabel();
				para[1]=listDataminute.get(i).getValue();
				para[2]=listDataspeed.get(i).getValue();
				para[3]=listDatapvs.get(i).getValue();
				
				 array.add(para);
			}
					
			request.setAttribute("listDataminute", listDataminute);
			request.setAttribute("listDataspeed", listDataspeed);
			request.setAttribute("listDatapvs", listDatapvs);
			request.setAttribute("array", array);
			
			
			
		} catch (Exception e) {  
			System.out.println(e);
		}
		key = buildTrendChartsData_dg2_html(request);
		key = buildPieChartsData_dg53_html_01(request,2);
		key = buildPieChartsData_dg53_html_01(request,1);
	
		return key;
	}
	
	
String buildTrendChartsData_dg2(HttpServletRequest request) {
		String key = "dg2";
		String sql = "{call datawarehouse.dbo.ax_lua_exception_chart52_01(?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.registerOutParameter(1, 12);
			stmt.execute();
		//	loadXml(stmt.getString(1));
			request.setAttribute("chart_data", stmt.getString(1));
			
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return key;
	}

// get second chart trend  added on 2-4-2015
	String buildTrendChartsData_dg2_html(HttpServletRequest request) {
	String key = "lua701";
	ResultSet rs = null;
	String sql = "{call datawarehouse.dbo.ax_lua_exception_chart52_01_html()}";
	try {
		CallableStatement stmt = DataAccess.databaseConnection
				.prepareCall(sql);
	//	stmt.registerOutParameter(1, 12);
		rs = stmt.executeQuery();
		//String array=(String) stmt.getString(1);
		ArrayList<LabelValueValue> projectCategories = new ArrayList<LabelValueValue>();
		int projectCategoriesCount = 0;
		ArrayList<String[]> arraygraph2 = new ArrayList<String[]>();
		while (rs.next()) {
			
			LabelValueValue projectCategory = new LabelValueValue(
					rs.getString("@label"),
					rs.getString("@value"),rs.getString("@value1"));
			projectCategories.add(projectCategoriesCount, projectCategory);
			projectCategoriesCount++;
			
			
		}
		for(int i=0;i< projectCategories.size();i++){
			String[] para = new String[4];
			para[0]=projectCategories.get(i).getLabel();
			para[1]=projectCategories.get(i).getValue();
			para[2]=projectCategories.get(i).getValue01();
			
			
			arraygraph2.add(para);
		}
		
		request.setAttribute("arraygraph2", arraygraph2);
		request.setAttribute("projectCategories", projectCategories);
		//request.setAttribute("chart_data", stmt.getString(1));
		
		stmt.close();
	} catch (Exception e) {
		System.out.println(e);
	}
	return key;
}


	String buildPieChartsData_dg53_html_01(HttpServletRequest request, int index) {
		String key = "lua701";
		ResultSet rs = null;
		String sql = "{call datawarehouse.dbo.ax_labor_exceptions_categorized_01_html(?,?)}";
		ArrayList<String[]> arraygraph3 = new ArrayList<String[]>();
		ArrayList<String[]> arraygraphadd2 = new ArrayList<String[]>();
		ArrayList<String[]> arraygraphadd3 = new ArrayList<String[]>();
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.registerOutParameter(1, 12);
			stmt.registerOutParameter(2, 12);
			rs = stmt.executeQuery();
			ArrayList<LabelValue> projectCategories01 = new ArrayList<LabelValue>();
			
			int projectCategoriesCount = 0;
			while (rs.next()) {
				
				LabelValue projectCategory = new LabelValue(
						rs.getString("@label"),
						rs.getString("@value"));
				projectCategories01.add(projectCategoriesCount, projectCategory);
				projectCategoriesCount++;
			
				
			}
			for(int i=0;i< projectCategories01.size();i++){
				String[] para = new String[4];
				para[0]=projectCategories01.get(i).getLabel();
				para[1]=projectCategories01.get(i).getValue();
				
				
				
				arraygraph3.add(para);
			}
			
			/*request.setAttribute("projectCategories01", projectCategories01);*/
		//	request.setAttribute("chart_data", stmt.getString(index));
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		arraygraphadd2=buildPieChartsData_dg53_html_02(request,index);
		
		
		arraygraph3.addAll(arraygraphadd2);
		arraygraphadd3=buildPieChartsData_dg53_html_03(request,index);
		//arraygraph3.addAll(arraygraphadd3);
		request.setAttribute("arraygraphadd3", arraygraphadd3);
		request.setAttribute("arraygraph3", arraygraph3);
		return key;
	}

	// second function 
	ArrayList<String[]> buildPieChartsData_dg53_html_02(HttpServletRequest request, int index) {
		String key = "lua701";
		ResultSet rs = null;
		String sql = "{call datawarehouse.dbo.ax_labor_exceptions_categorized_02_html(?,?)}";
		ArrayList<String[]> arraygraph4 = new ArrayList<String[]>();
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.registerOutParameter(1, 12);
			stmt.registerOutParameter(2, 12);
			rs = stmt.executeQuery();
			ArrayList<LabelValue> projectCategories02 = new ArrayList<LabelValue>();
			
			int projectCategoriesCount = 0;
			while (rs.next()) {
				
				LabelValue projectCategory = new LabelValue(
						rs.getString("@label"),
						rs.getString("@value"));
				projectCategories02.add(projectCategoriesCount, projectCategory);
				projectCategoriesCount++;
				
				
			}
			
			for(int i=0;i< projectCategories02.size();i++){
				String[] para = new String[4];
				para[0]=projectCategories02.get(i).getLabel();
				para[1]=projectCategories02.get(i).getValue();
				
				
				
				arraygraph4.add(para);
			}
			
			request.setAttribute("projectCategories02", projectCategories02);
		//	request.setAttribute("chart_data", stmt.getString(index));
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return arraygraph4;
	}

//get third
	ArrayList<String[]> buildPieChartsData_dg53_html_03(HttpServletRequest request, int index) {
		String key = "lua701";
		ResultSet rs = null;
		ArrayList<String[]> arraygraph4 = new ArrayList<String[]>();
		String sql = "{call datawarehouse.dbo.ax_labor_exceptions_categorized_03_html(?,?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.registerOutParameter(1, 12);
			stmt.registerOutParameter(2, 12);
			rs = stmt.executeQuery();
			ArrayList<LabelValue> projectCategories03 = new ArrayList<LabelValue>();
			int projectCategoriesCount = 0;
			while (rs.next()) {
				
				LabelValue projectCategory = new LabelValue(
						rs.getString("@label"),
						rs.getString("@value"));
				projectCategories03.add(projectCategoriesCount, projectCategory);
				projectCategoriesCount++;
				
				
			}
			for(int i=0;i< projectCategories03.size();i++){
				String[] para = new String[4];
				para[0]=projectCategories03.get(i).getLabel();
				para[1]=projectCategories03.get(i).getValue();
				
				
				
				arraygraph4.add(para);
			}
			
			
			
			request.setAttribute("projectCategories03", projectCategories03);
			request.setAttribute("chart_data", stmt.getString(index));
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return arraygraph4;
	}


	String buildPieChartsData_dg53(HttpServletRequest request, int index) {
		String key = "dg53";
		String sql = "{call datawarehouse.dbo.ax_labor_exceptions_categorized_01(?,?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.registerOutParameter(1, 12);
			stmt.registerOutParameter(2, 12);
			stmt.execute();
		//	populate(stmt.getString(index));
			request.setAttribute("chart_data", stmt.getString(index));
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return key;
	}

	HttpSession initialize(HttpServletRequest request) {
		HttpSession session = request.getSession(true);

		String[] p = initializeParameters();
		String[] v = new String[p.length];
		String rpt = getCurrentValue(request, "rpt");
		String frm = getCurrentValue(request, "frm");
		if (frm.equals("1")) {
			setSessionValue(session, "jo", getCurrentValue(request, "jo"));
			setSessionValue(session, "spm", getCurrentValue(request, "spm"));
			setSessionValue(session, "project",
					getCurrentValue(request, "project"));
			setSessionValue(session, "from_date",
					getCurrentValue(request, "from_date"));
			setSessionValue(session, "to_date",
					getCurrentValue(request, "to_date"));
			setSessionValue(session, "status",
					getCurrentValue(request, "status"));
			setSessionValue(session, "exception",
					getCurrentValue(request, "exception"));
		}
		for (int i = 0; i < p.length; i++) {
			v[i] = getCurrentValue(request, p[i]);
			if (v[i].equals("")) {
				v[i] = getSessionValue(session, p[i], rpt);
			}
			if (v[i].equals("")) {
				v[i] = setDefaultValue(p[i]);
			}
			session.setAttribute(p[i], v[i]);
			request.setAttribute(p[i], v[i]);
		}
		return session;
	}

	String getCurrentValue(HttpServletRequest request, String parameter) {
		String p_value = request.getParameter(parameter) != null ? request
				.getParameter(parameter).trim() : "";

		return p_value;
	}

	void setSessionValue(HttpSession session, String parameter, String value) {
		session.setAttribute(parameter, value);
	}

	String getSessionValue(HttpSession session, String parameter, String rpt) {
		String p_value = "";
		if ((!rpt.equals("5"))
				|| ((!parameter.equals("spm")) && (!parameter.equals("jo")))) {
			if ((!rpt.equals("6")) || (!parameter.equals("jo"))) {
				p_value = session.getAttribute(parameter) != null ? ((String) session
						.getAttribute(parameter)).trim() : "";
			}
		}
		return p_value;
	}

	String getSessionValue(HttpSession session, String parameter) {
		String p_value = session.getAttribute(parameter) != null ? ((String) session
				.getAttribute(parameter)).trim() : "";

		return p_value;
	}

	String setDefaultValue(String parameter) {
		String p_value = "";
		if (parameter.equals("jo")) {
			p_value = "";
		} else if (parameter.equals("spm")) {
			p_value = "";
		} else if (parameter.equals("po_type")) {
			p_value = "A";
		} else if (parameter.equals("project")) {
			p_value = "";
		} else if (parameter.equals("client")) {
			p_value = "";
		} else if (parameter.equals("from_date")) {
			p_value = initializeFromDate();
		} else if (parameter.equals("to_date")) {
			p_value = initializeToDate();
		} else if (parameter.equals("status")) {
			p_value = "All";
		} else if (parameter.equals("exception")) {
			p_value = "%";
		} else if (parameter.equals("jo_list")) {
			p_value = initializeMenus1(parameter);
		} else if (parameter.equals("spm_list")) {
			p_value = initializeMenus1(parameter);
		} else if (parameter.equals("cp_list")) {
			p_value = initializeMenus1(parameter);
		} else if (parameter.equals("rpt")) {
			p_value = "1";
		}
		return p_value;
	}

	String initializeFromDate() {
		String sfrom_date = "";

		GregorianCalendar from_date = new GregorianCalendar();
		from_date.set(from_date.get(1), from_date.get(2), from_date.get(5));

		from_date.add(2, -1);

		sfrom_date = this.df1.format(from_date.getTime());

		return sfrom_date;
	}

	String initializeToDate() {
		String sto_date = "";

		GregorianCalendar to_date = new GregorianCalendar();
		to_date.set(to_date.get(1), to_date.get(2), to_date.get(5));

		sto_date = this.df1.format(to_date.getTime());

		return sto_date;
	}

	String initializeMenus1(String parameter) {
		String menu = "";
		if (parameter.equals("jo_list")) {
			menu = formatMenu1(this.da.getResultSet(getJOSQL()));
		} else if (parameter.equals("spm_list")) {
			menu = formatMenu1(this.da.getResultSet(getSPMSQL()));
		} else if (parameter.equals("cp_list")) {
			menu = formatMenuGrouping(this.da.getResultSet(getCPSQL()));
		}
		return menu;
	}

	String formatMenu1(ResultSet rs) {
		String menu = "";
		String delimiter = "";
		try {
			while (rs.next()) {
				menu = menu + delimiter + rs.getString(1) + "|"
						+ rs.getString(2);
				delimiter = "~";
			}
		} catch (Exception e) {
			System.out.println("initializeMenus1: " + e);
		}
		return menu;
	}

	String formatMenuGrouping(ResultSet rs) {
		String menu = "";
		String delimiter = "";
		try {
			while (rs.next()) {
				menu = menu + delimiter + rs.getString(1) + "|"
						+ rs.getString(2) + "|" + rs.getString(3);
				delimiter = "~";
			}
		} catch (Exception e) {
			System.out.println("initializeMenus1: " + e);
		}
		return menu;
	}

	String getJOSQL() {
		String sql = "";
//select distinct ac_du_lo_pc_id
		sql = " select distinct ac_jd_spm as ac_du_lo_pc_id, ac_du_name   from datawarehouse.dbo.ax_labor_pf_and_actual2        join datawarehouse.dbo.ac_dimensions_job on ax_js_id = ac_jd_js_id        join datawarehouse.dbo.ac_dimension_user on ac_jd_job_owner = ac_du_lo_pc_id  where ac_jd_date_closed > dateadd(mm, -12, getdate()) order by ac_du_name";

		return sql;
	}

	String getSPMSQL() {
		String sql = "";
//ac_du_lo_pc_id, ac_du_name
		sql = " select distinct  ac_jd_spm as ac_du_lo_pc_id, ac_du_name   from datawarehouse.dbo.ax_labor_pf_and_actual2        join datawarehouse.dbo.ac_dimensions_job on ax_js_id = ac_jd_js_id        join datawarehouse.dbo.ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id  where ac_jd_date_closed > dateadd(mm, -12, getdate())       and ac_jd_spm not in (6228, 7878) order by ac_du_name";

		return sql;
	}

	String getCPSQL() {
		String sql = "";

		sql = " select distinct substring(lo_ot_name, 1, 30),  substring(lx_pr_title, 1, 36), lx_pr_id  from datawarehouse.dbo.ax_labor_pf_and_actual2  join datawarehouse.dbo.ac_dimensions_job on ax_js_id = ac_jd_js_id  join ilex.dbo.ap_appendix_list_01 on ac_jd_pd_pr_id = lx_pr_id  where ac_jd_date_closed > dateadd(mm, -12, getdate())  order by substring(lo_ot_name, 1, 30),  substring(lx_pr_title, 1, 36), lx_pr_id";

		return sql;
	}

	String getClientSQL(HttpServletRequest request) {
		String sql = "";

		String from_date = getRequestParameter(request, "from_date");
		String to_date = getRequestParameter(request, "to_date");
		String po_type = getRequestParameter(request, "po_type");
		String spm = getRequestParameter(request, "spm");
		String jo = getRequestParameter(request, "jo");
		String exception = getRequestParameter(request, "exception");

		sql = "select substring(ac_pd_lo_ot_name, 1, 36) as lo_ot_name,         convert(decimal(8,2), sum(labor_qty_pf)) as pf_qty,        convert(decimal(8,2), sum(labor_amt_pf)) as pf_rev,        convert(decimal(8,2), sum(labor_qty_sell)) as ac_qty,        convert(decimal(8,2), sum(labor_amt_sell)) as ac_rev,        convert(decimal(8,2), sum(labor_qty_buy)) as po_qty,        convert(decimal(8,2), sum(labor_amt_buy)) as po_exp,        count(*) as cnt,        ac_pd_lp_mm_id as lp_mm_id from datawarehouse.dbo.ax_labor_pf_and_actual2      join datawarehouse.dbo.ax_labor_buy2 on ax_js_id = ax_po_js_id      join datawarehouse.dbo.ac_dimensions_job on ax_js_id = ac_jd_js_id      join datawarehouse.dbo.ac_dimensions_project on ac_jd_pd_pr_id = ac_pd_pr_id where labor_qty_sell > 0.0      and ac_jd_date_closed between '"
				+

				from_date + "' and '" + to_date + "' ";
		if ((po_type.equals("M")) || (po_type.equals("S"))
				|| (po_type.equals("P"))) {
			sql = sql + "     and po_type = '" + po_type + "' ";
		}
		if (!spm.equals("")) {
			sql = sql + "     and ac_jd_spm = " + spm + " ";
		}
		if (!jo.equals("")) {
			sql = sql + "     and ac_jd_job_owner = " + jo + " ";
		}
		if ((!exception.equals("All")) && (!exception.equals(""))) {
			sql = sql + "     and status like '" + exception + "' ";
		}
		sql = sql
				+ "group by  substring(ac_pd_lo_ot_name, 1, 36), ac_pd_lp_mm_id order by  substring(ac_pd_lo_ot_name, 1, 36) ";

		return sql;
	}

	String getClientAppendixSQL(HttpServletRequest request) {
		String sql = "";
		String detail_criteria = "";

		String client = getRequestParameter(request, "client");
		String from_date = getRequestParameter(request, "from_date");
		String to_date = getRequestParameter(request, "to_date");
		String po_type = getRequestParameter(request, "po_type");
		String spm = getRequestParameter(request, "spm");
		String jo = getRequestParameter(request, "jo");
		String exception = getRequestParameter(request, "exception");

		String rpt = getRequestParameter(request, "rpt");
		if (rpt.equals("21")) {
			if (!spm.equals("")) {
				detail_criteria = "and ac_jd_spm = " + spm;
			}
		} else if (!client.equals("")) {
			detail_criteria = " and ac_pd_lp_mm_id = " + client;
		}
		sql =

		"select substring(ac_pd_lo_ot_name, 1, 36) as lo_ot_name,         lx_pr_title,        convert(decimal(8,2), sum(labor_qty_pf)) as pf_qty,        convert(decimal(8,2), sum(labor_amt_pf)) as pf_rev,        convert(decimal(8,2), sum(labor_qty_sell)) as ac_qty,        convert(decimal(8,2), sum(labor_amt_sell)) as ac_rev,        convert(decimal(8,2), sum(labor_qty_buy)) as po_qty,        convert(decimal(8,2), sum(labor_amt_buy)) as po_exp,        count(*) as cnt,        ac_jd_pd_pr_id as lx_pr_id from datawarehouse.dbo.ax_labor_pf_and_actual2      join datawarehouse.dbo.ax_labor_buy2 on ax_js_id = ax_po_js_id      join datawarehouse.dbo.ac_dimensions_job on ax_js_id = ac_jd_js_id      join datawarehouse.dbo.ac_dimensions_project on ac_jd_pd_pr_id = ac_pd_pr_id      join ilex.dbo.ap_appendix_list_01 on ac_pd_pr_id = lx_pr_id where labor_qty_sell > 0.0      "
				+ detail_criteria
				+ " "
				+ "     and ac_jd_date_closed between '"
				+ from_date
				+ "' and '" + to_date + "' ";
		if ((po_type.equals("M")) || (po_type.equals("S"))
				|| (po_type.equals("P"))) {
			sql = sql + "     and po_type = '" + po_type + "' ";
		}
		if (!spm.equals("")) {
			sql = sql + "     and ac_jd_spm = " + spm + " ";
		}
		if (!jo.equals("")) {
			sql = sql + "     and ac_jd_job_owner = " + jo + " ";
		}
		if ((!exception.equals("All")) && (!exception.equals(""))) {
			sql = sql + "     and status like '" + exception + "' ";
		}
		sql = sql
				+ "group by  substring(ac_pd_lo_ot_name, 1, 36), lx_pr_title, ac_jd_pd_pr_id order by  substring(ac_pd_lo_ot_name, 1, 36), lx_pr_title ";

		System.out.println(sql);

		return sql;
	}

	String getSpmSQL(HttpServletRequest request) {
		String sql = "";

		String client = getRequestParameter(request, "client");

		String from_date = getRequestParameter(request, "from_date");
		String to_date = getRequestParameter(request, "to_date");
		String po_type = getRequestParameter(request, "po_type");
		String spm = getRequestParameter(request, "spm");
		String jo = getRequestParameter(request, "jo");
		String exception = getRequestParameter(request, "exception");

		sql = "select substring(ac_du_name, 1, charindex(',', ac_du_name)-1) as ac_du_name,        convert(decimal(8,2), sum(labor_qty_pf)) as pf_qty,        convert(decimal(8,2), sum(labor_amt_pf)) as pf_rev,        convert(decimal(8,2), sum(labor_qty_sell)) as ac_qty,        convert(decimal(8,2), sum(labor_amt_sell)) as ac_rev,        convert(decimal(8,2), sum(labor_qty_buy)) as po_qty,        convert(decimal(8,2), sum(labor_amt_buy)) as po_exp,        count(*) as cnt,        ac_du_lo_pc_id as ac_du_lo_pc_id from datawarehouse.dbo.ax_labor_pf_and_actual2      join datawarehouse.dbo.ax_labor_buy2 on ax_js_id = ax_po_js_id      join datawarehouse.dbo.ac_dimensions_job on ax_js_id = ac_jd_js_id      join datawarehouse.dbo.ac_dimensions_project on ac_jd_pd_pr_id = ac_pd_pr_id      join datawarehouse.dbo.ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id      join ilex.dbo.ap_appendix_list_01 on ac_pd_pr_id = lx_pr_id where labor_qty_sell > 0.0 and ac_jd_spm != 6228      and ac_jd_date_closed between '"
				+

				from_date + "' and '" + to_date + "' ";
		if ((po_type.equals("M")) || (po_type.equals("S"))
				|| (po_type.equals("P"))) {
			sql = sql + "     and po_type = '" + po_type + "' ";
		}
		if (!jo.equals("")) {
			sql = sql + "     and ac_jd_job_owner = " + jo + " ";
		}
		if (!spm.equals("")) {
			sql = sql + "     and ac_jd_spm = " + spm + " ";
		}
		if ((!exception.equals("All")) && (!exception.equals(""))) {
			sql = sql + "     and status like '" + exception + "' ";
		}
		sql = sql
				+ "group by  substring(ac_du_name, 1, charindex(',', ac_du_name)-1), ac_du_lo_pc_id order by  substring(ac_du_name, 1, charindex(',', ac_du_name)-1) ";

		System.out.println(sql);

		return sql;
	}

	String getJoSQL(HttpServletRequest request) {
		String sql = "";

		String client = getRequestParameter(request, "client");

		String from_date = getRequestParameter(request, "from_date");
		String to_date = getRequestParameter(request, "to_date");
		String po_type = getRequestParameter(request, "po_type");
		String spm = getRequestParameter(request, "spm");
		String jo = getRequestParameter(request, "jo");
		String exception = getRequestParameter(request, "exception");

		sql = "select substring(ac_du_name, 1, charindex(',', ac_du_name)-1) as ac_du_name,        convert(decimal(8,2), sum(labor_qty_pf)) as pf_qty,        convert(decimal(8,2), sum(labor_amt_pf)) as pf_rev,        convert(decimal(8,2), sum(labor_qty_sell)) as ac_qty,        convert(decimal(8,2), sum(labor_amt_sell)) as ac_rev,        convert(decimal(8,2), sum(labor_qty_buy)) as po_qty,        convert(decimal(8,2), sum(labor_amt_buy)) as po_exp,        count(*) as cnt,        case when sum(revenue) > 0.0 then convert(decimal(8,2), 1-(sum(actual_cost)/sum(revenue))) else 0.00 end as vgpm,        sum(revenue) as rev,        sum(actual_cost) as exp,        ac_du_lo_pc_id as ac_du_lo_pc_id from datawarehouse.dbo.ax_labor_pf_and_actual2      join datawarehouse.dbo.ax_labor_buy2 on ax_js_id = ax_po_js_id      join datawarehouse.dbo.ac_dimensions_job on ax_js_id = ac_jd_js_id      join datawarehouse.dbo.ac_dimensions_project on ac_jd_pd_pr_id = ac_pd_pr_id      join datawarehouse.dbo.ac_dimension_user on ac_jd_job_owner = ac_du_lo_pc_id      join ilex.dbo.ap_appendix_list_01 on ac_pd_pr_id = lx_pr_id where labor_qty_sell > 0.0 and ac_jd_spm != 6228      and ac_jd_date_closed between '"
				+

				from_date + "' and '" + to_date + "' ";
		if ((po_type.equals("M")) || (po_type.equals("S"))
				|| (po_type.equals("P"))) {
			sql = sql + "     and po_type = '" + po_type + "' ";
		}
		if (!jo.equals("")) {
			sql = sql + "     and ac_jd_job_owner = " + jo + " ";
		}
		if (!spm.equals("")) {
			sql = sql + "     and ac_jd_spm = " + spm + " ";
		}
		if ((!exception.equals("All")) && (!exception.equals(""))) {
			sql = sql + "     and status like '" + exception + "' ";
		}
		sql = sql
				+ "group by  substring(ac_du_name, 1, charindex(',', ac_du_name)-1), ac_du_lo_pc_id order by  substring(ac_du_name, 1, charindex(',', ac_du_name)-1) ";

		return sql;
	}

	String getJoSQLAll(HttpServletRequest request) {
		String sql = "";

		String from_date = getRequestParameter(request, "from_date");
		String to_date = getRequestParameter(request, "to_date");
		String po_type = getRequestParameter(request, "po_type");
		String spm = getRequestParameter(request, "spm");
		String jo = getRequestParameter(request, "jo");

		sql = "select ac_jd_job_owner as pc_id,        sum(ac_ru_expense) as exp,        sum(ac_ru_revenue) as rev,        case when sum(ac_ru_revenue) > 0.0 then convert(decimal(8,2), 1-(sum(ac_ru_expense)/sum(ac_ru_revenue))) else 0.0 end as vgpm,       count(*) as cnt  from datawarehouse.dbo.ac_fact_job_rollup        join datawarehouse.dbo.ac_dimensions_job on ac_ru_js_id = ac_jd_js_id where ac_jd_date_closed between '"
				+

				from_date + "' and '" + to_date + "' ";
		if ((po_type.equals("M")) || (po_type.equals("S"))
				|| (po_type.equals("P"))) {
			sql = sql + "     and po_type = '" + po_type + "' ";
		}
		if (!jo.equals("")) {
			sql = sql + "     and ac_jd_job_owner = " + jo + " ";
		}
		if (!spm.equals("")) {
			sql = sql + "     and ac_jd_spm = " + spm + " ";
		}
		sql = sql + "group by  ac_jd_job_owner ";

		System.out.println(sql);

		return sql;
	}

	String getDetailsSQL(HttpServletRequest request) {
		String sql = "";
		String detail_criteria = "";

		String client = getRequestParameter(request, "client");
		String from_date = getRequestParameter(request, "from_date");
		String to_date = getRequestParameter(request, "to_date");
		String po_type = getRequestParameter(request, "po_type");
		String spm = getRequestParameter(request, "spm");
		String jo = getRequestParameter(request, "jo");
		String exception = getRequestParameter(request, "exception");
		String rpt = getRequestParameter(request, "rpt");
		String project = getRequestParameter(request, "project");
		if (rpt.equals("70")) {
			detail_criteria = "and ac_jd_job_owner = " + jo;
		} else {
			detail_criteria = " and ac_jd_pd_pr_id = " + project;
		}
		sql =

		"select substring(ac_du_name, 1, charindex(',', ac_du_name)-1) as ac_du_name,        lm_js_title,       convert(varchar(10),  ac_jd_date_closed, 101) as ac_jd_date_closed,        convert(decimal(8,2), (labor_qty_pf)) as pf_qty,        convert(decimal(8,2), (labor_amt_pf)) as pf_rev,        convert(decimal(8,2), (labor_qty_sell)) as ac_qty,        convert(decimal(8,2), (labor_amt_sell)) as ac_rev,        convert(decimal(8,2), (labor_qty_buy)) as po_qty,        convert(decimal(8,2), (labor_amt_buy)) as po_exp,        ax_js_id from datawarehouse.dbo.ax_labor_pf_and_actual2      join datawarehouse.dbo.ax_labor_buy2 on ax_js_id = ax_po_js_id      join datawarehouse.dbo.ac_dimensions_job on ax_js_id = ac_jd_js_id      join ilex.dbo.lm_job on ax_js_id = lm_js_id      join datawarehouse.dbo.ac_dimension_user on ac_jd_job_owner = ac_du_lo_pc_id where labor_qty_sell > 0.0 and ac_jd_spm != 6228      and ac_jd_date_closed between '"
				+ from_date
				+ "' and '"
				+ to_date
				+ "' "
				+ "\t  "
				+ detail_criteria + " ";
		if ((po_type.equals("M")) || (po_type.equals("S"))
				|| (po_type.equals("P"))) {
			sql = sql + "     and po_type = '" + po_type + "' ";
		}
		if (!spm.equals("")) {
			sql = sql + "     and ac_jd_spm = " + spm + " ";
		}
		if ((!exception.equals("All")) && (!exception.equals(""))) {
			sql = sql + "     and status like '" + exception + "' ";
		}
		sql = sql + "order by  ac_jd_date_closed ";

		System.out.println(sql);

		return sql;
	}

	String[] initializeParameters() {
		String[] parameters = { "spm", "jo", "project", "client", "from_date",
				"to_date", "po_type", "status", "exception", "jo_list",
				"spm_list", "cp_list", "rpt" };

		return parameters;
	}

	String getRequestParameter(HttpServletRequest request, String parameter) {
		String value = request.getParameter(parameter);
		if (value != null) {
			value = value.trim();
		} else {
			value = getSessionValue(request.getSession(), parameter);
		}
		return value;
	}

	String effectiveLaborRate(String qty, String amt) {
		Double elr = Double.valueOf(0.0D);
		Double q = Double.valueOf(0.0D);
		Double a = Double.valueOf(0.0D);
		try {
			q = Double.valueOf(Double.parseDouble(qty));
			a = Double.valueOf(Double.parseDouble(amt));
			if (q.doubleValue() > 0.0D) {
				elr = Double.valueOf(a.doubleValue() / q.doubleValue());
			} else {
				elr = Double.valueOf(0.0D);
			}
			elr = Double.valueOf(a.doubleValue() / q.doubleValue());
		} catch (Exception e) {
			elr = Double.valueOf(a.doubleValue() / q.doubleValue());
		}
		return this.sd.format(elr);
	}

	Double sumColumn(Double sum, String s_value) {
		Double value = Double.valueOf(0.0D);
		try {
			value = Double.valueOf(Double.parseDouble(s_value));
			sum = Double.valueOf(sum.doubleValue() + value.doubleValue());
		} catch (Exception localException) {
		}
		return sum;
	}
	
	
//get trend graph 
	public static ArrayList<LabelValue> populateTrendChart(String query)
			 {
		

		DataAccess da = new DataAccess();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<LabelValue> projectCategories = new ArrayList<LabelValue>();
		int projectCategoriesCount = 0;
		
		try {
			
			
		
			rs = da.getResultSet(query);
			while (rs.next()) {
			
				LabelValue projectCategory = new LabelValue(
						rs.getString("label"),
						rs.getString("value"));
				projectCategories.add(projectCategoriesCount, projectCategory);
				projectCategoriesCount++;
			}

		} catch (SQLException sqlE) {
			System.out.print(sqlE);
		} 
		return projectCategories;
	}



private String getQuery(String sqltype){
String sql="";
if(sqltype.equals("M")){	
	
  sql ="use datawarehouse; select  substring(convert(varchar(12), sc_dd_month_mm01yyyy, 105), 4, 9) label , convert (decimal(8,2), count(*)) value"
		+  " from dbo.ax_labor_buy2 "
		+  " join dbo.ac_dimensions_purchase_order on ax_po_id = ac_od_po_id"
		+  " join dbo.ac_dimension_date on ac_od_date_complete = sc_dd_mmddyyyy"
		+ "  where ac_od_type = 'M'  "
		+  " group by sc_dd_month_mm01yyyy  "
		+  " order by sc_dd_month_mm01yyyy ";	
}	
else if(sqltype.equals("S"))
			{

	 sql =" use datawarehouse; select  substring(convert(varchar(12), sc_dd_month_mm01yyyy, 105), 4, 9) label , convert (decimal(8,2), count(*)) value"
			+  " from dbo.ax_labor_buy2 "
			+  " join dbo.ac_dimensions_purchase_order on ax_po_id = ac_od_po_id"
			+  " join dbo.ac_dimension_date on ac_od_date_complete = sc_dd_mmddyyyy"
			+ "  where ac_od_type = 'S'  "
			+  " group by sc_dd_month_mm01yyyy  "
			+  " order by sc_dd_month_mm01yyyy ";	
	
			}
	else{
	
	sql ="use datawarehouse; select  substring(convert(varchar(12), sc_dd_month_mm01yyyy, 105), 4, 9) label , convert (decimal(8,2), count(*)) value"
			+  " from dbo.ax_labor_buy2 "
			+  " join dbo.ac_dimensions_purchase_order on ax_po_id = ac_od_po_id"
			+  " join dbo.ac_dimension_date on ac_od_date_complete = sc_dd_mmddyyyy"
			+ "  where ac_od_type = 'P'  "
			+  " group by sc_dd_month_mm01yyyy  "
			+  " order by sc_dd_month_mm01yyyy ";	
	
	}
return sql;

}


}
