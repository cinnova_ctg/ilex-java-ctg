package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class ManagementReports extends DispatchAction {
	Page page;
	DataAccess da = new DataAccess();
	Vector rows;
	String report_type;
	String job_status;
	String account;

	public ActionForward managementReports(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "Default Value";

		this.rows = new Vector();

		unpackSessionAttribute(request);

		request.setAttribute("rows", this.rows);

		forward_key = generateReportData(this.report_type, this.job_status,
				request);

		return mapping.findForward(forward_key);
	}

	String generateReportData(String rt, String js, HttpServletRequest rq) {
		String fk = "completeRpt";
		String where = "";

		String sql = "SELECT ap_jl_pr_title, ap_jl_ot_name, ap_jl_js_title, convert(varchar(8),ap_jl_start,101) as ap_jl_start, convert(varchar(8),ap_jl_end,101) as ap_jl_end, ap_jl_supervisor, ap_jl_employee, ap_jl_actual_cost, ap_jl_price, cast (ap_jl_vgp as decimal(5,2)) as ap_jl_vgp FROM   ap_job_level_summary ";
		if (js.equals("complete")) {
			where = "where  ap_jl_status = 'Complete' ";
			rq.setAttribute("title", "Complete");
			if (rt.equals("all")) {
				where = where
						+ " and (ap_jl_customer_reference = 'Y' or ap_jl_site = 'Y' or ap_jl_notes = 'Y' or ap_jl_cost_issue = 'Y' or ap_jl_inconsistent_job  = 'Y' or (ap_jl_vgp < 0.2 or ap_jl_vgp > 0.8))";
				rq.setAttribute("subtitle", "All Jobs");
			} else if (rt.equals("notes")) {
				where = where + " and (ap_jl_notes = 'Y') ";
				rq.setAttribute("subtitle", "Missing Notes");
			} else if (rt.equals("customer")) {
				where = where + " and (ap_jl_customer_reference = 'Y') ";
				rq.setAttribute("subtitle", "Missing Customer Reference");
			} else if (rt.equals("site")) {
				where = where + " and (ap_jl_site = 'Y') ";
				rq.setAttribute("subtitle", "Missing Site Information");
			} else if (rt.equals("price")) {
				where = where + " and (ap_jl_cost_issue = 'Y') ";
				rq.setAttribute("subtitle", "Cost/Price Inconsistency");
			} else if (rt.equals("vgp")) {
				where = where + " and (ap_jl_vgp < 0.2 or ap_jl_vgp > 0.8) ";
				rq.setAttribute("subtitle", "VGP Out of Limits");
			}
		} else if (js.equals("inwork-onsite")) {
			where = "where  ap_jl_status = 'In Work - Onsite' ";
			rq.setAttribute("title", "In Work - Onsite");
		} else if (js.equals("inwork-offsite")) {
			where = "where  ap_jl_status = 'In Work - Offsite' ";
			rq.setAttribute("title", "In Work - Offsite");
		}
		if (!this.account.equals("")) {
			if (where.equals("")) {
				where = "where ap_jl_ot_name = '" + this.account + "' ";
			} else {
				where = where + " and ap_jl_ot_name = '" + this.account + "' ";
			}
		}
		sql = sql + where;
		sql = sql + "order  by ap_jl_ot_name, ap_jl_pr_title";

		getData(sql, rq);

		return fk;
	}

	void getData(String sql, HttpServletRequest rq) {
		int count = 0;
		Double actual = Double.valueOf(0.0D);
		Double price = Double.valueOf(0.0D);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[10];

				row[0] = rs.getString("ap_jl_pr_title");
				row[1] = rs.getString("ap_jl_ot_name");
				row[2] = rs.getString("ap_jl_js_title");
				row[3] = rs.getString("ap_jl_supervisor");
				row[4] = rs.getString("ap_jl_employee");
				row[5] = formatCurrencyValues(Double.valueOf(rs
						.getDouble("ap_jl_actual_cost")));
				row[6] = formatCurrencyValues(Double.valueOf(rs
						.getDouble("ap_jl_price")));
				row[7] = rs.getString("ap_jl_vgp");
				row[8] = rs.getString("ap_jl_start");
				if (rs.wasNull()) {
					row[8] = "";
				}
				row[9] = rs.getString("ap_jl_end");
				if (rs.wasNull()) {
					row[9] = "";
				}
				actual = Double.valueOf(actual.doubleValue()
						+ rs.getDouble("ap_jl_actual_cost"));
				price = Double.valueOf(price.doubleValue()
						+ rs.getDouble("ap_jl_price"));

				this.rows.add(count, row);
				count++;
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
		rq.setAttribute("cost", formatCurrencyValues(actual));
		rq.setAttribute("price", formatCurrencyValues(price));
	}

	boolean unpackSessionAttribute(HttpServletRequest rq) {
		boolean ur = false;

		this.page = ((Page) rq.getSession().getAttribute("Page"));
		if (this.page != null) {
			ur = true;
		}
		this.report_type = rq.getParameter("report_type");
		if (this.report_type == null) {
			this.report_type = "all";
		}
		this.job_status = rq.getParameter("job_status");
		if (this.job_status == null) {
			this.job_status = "complete";
		}
		this.account = rq.getParameter("account");
		if (this.account == null) {
			this.account = "";
		}
		rq.setAttribute("job_status", this.job_status);
		rq.setAttribute("report_type", this.report_type);
		rq.setAttribute("account", this.account);

		return ur;
	}

	String formatCurrencyValues(Double value) {
		NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
		String formattedValue = nf.format(value);

		return formattedValue;
	}
}
