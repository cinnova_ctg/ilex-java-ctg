package com.contingent.ilex.report;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class AgentSupportFramework extends DispatchAction {
	Map<String, Map> ParameterMappings = new HashMap(5, 1.0F);
	Map<String, String> InputParameters = new HashMap(11, 1.0F);
	String SF;
	int iSF;
	String PO_SelectedList = "";
	String ErrorMessage;
	String[] TxErrors = { "", "SP Input", "lm_purchase_order update",
			"lm_partner_invoice update", "commission_paid_reference action",
			"execution problem" };
	Vector<String> PO_ErrorList = new Vector();
	DataAccess da = new DataAccess();
	DecimalFormat nf2 = new DecimalFormat("0.00");

	public ActionForward acm(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "error";
		if (validateMinimumRequestData(request)) {
			switch (this.iSF) {
			case 1:
				listPayableCommissions(request);
				forward_key = "1";

				break;
			case 2:
				if (getSelectedPOs(request)) {
					listPayableCommissions(request);
					this.InputParameters.put("po_list", this.PO_SelectedList);
				} else {
					listPayableCommissions(request);
					this.ErrorMessage = "The were no POs selected to pay.  Please review your request and resubmit";
					this.InputParameters.put("sf", "1");
				}
				forward_key = "1";

				break;
			case 3:
				if (executePayment()) {
					if (buildEmailRequestForm(request)) {
						forward_key = "4";
						this.InputParameters.put("sf", "3");
					} else {
						this.ErrorMessage = ("An error occurred trying to create the agent email.<br>" + this.ErrorMessage);
					}
				} else {
					this.ErrorMessage = ("An error occurred on the payment action.<br>" + this.ErrorMessage);
				}
				break;
			case 4:
				if (sendConfirmation()) {
					forward_key = "5";
				} else {
					this.ErrorMessage = ("An error occurred when sending the email.<br>" + this.ErrorMessage);
				}
				break;
			default:
				this.ErrorMessage = ("The request was submitted with an unsupported sf function = " + this.SF);
			}
		}
		if (!this.ErrorMessage.equals("")) {
			request.setAttribute("error", this.ErrorMessage);
		}
		for (Map.Entry<String, String> c : this.InputParameters.entrySet()) {
			request.setAttribute((String) c.getKey(), c.getValue());
		}
		return mapping.findForward(forward_key);
	}

	boolean validateMinimumRequestData(HttpServletRequest request) {
		boolean rc = false;

		this.ErrorMessage = "";
		this.PO_SelectedList = "";
		this.InputParameters.clear();

		String sf = request.getParameter("sf");
		if (sf != null) {
			sf = sf.trim();
			if (!sf.equals("")) {
				try {
					this.iSF = Integer.parseInt(sf);
					this.SF = sf;
					if (initializeParameterMaps(this.iSF, request)) {
						rc = true;
					}
				} catch (NumberFormatException e) {
					this.ErrorMessage = ("Improper request was submit with iSF = " + sf);
				}
			}
		}
		if ((!rc) && (this.ErrorMessage.equals(""))) {
			this.ErrorMessage = ("Request was submitted with missing or improper iSF = " + sf);
		}
		return rc;
	}

	boolean initializeParameterMaps(int sf, HttpServletRequest request) {
		boolean rc = true;
		String missing_p = "";
		this.InputParameters.clear();
		try {
			switch (sf) {
			case 1:
				this.InputParameters.put("timeframe",
						getParameter(request.getParameter("timeframe")));
				this.InputParameters.put("agent",
						getParameter(request.getParameter("agent")));
				this.InputParameters.put("com_type",
						getParameter(request.getParameter("com_type")));
				this.InputParameters.put("sf", "1");
				this.InputParameters.put("check_number", "");
				for (Map.Entry<String, String> c : this.InputParameters
						.entrySet()) {
					if (c.getValue() == null) {
						missing_p = missing_p + " " + (String) c.getKey();
					}
				}
				if (!missing_p.equals("")) {
					this.ErrorMessage = ("Request is missing required parameter(s). Missing values for " + missing_p);
					rc = false;
				}
				break;
			case 2:
				this.InputParameters.put("timeframe",
						getParameter(request.getParameter("timeframe")));
				this.InputParameters.put("agent",
						getParameter(request.getParameter("agent")));
				this.InputParameters.put("com_type",
						getParameter(request.getParameter("com_type")));
				this.InputParameters.put("check_number",
						getParameter(request.getParameter("check_number")));
				this.InputParameters.put("sf", "2");
				for (Map.Entry<String, String> c : this.InputParameters
						.entrySet()) {
					if (c.getValue() == null) {
						missing_p = missing_p + " " + (String) c.getKey();
					} else if ((((String) c.getKey()).equals("check_number"))
							&& (((String) c.getValue()).equals(""))) {
						missing_p = missing_p + " " + (String) c.getKey();
					}
				}
				if (!missing_p.equals("")) {
					this.ErrorMessage = ("Request is missing required parameter(s). Missing values for " + missing_p);
					rc = false;
				}
				break;
			case 3:
				this.InputParameters.put("timeframe",
						getParameter(request.getParameter("timeframe")));
				this.InputParameters.put("agent",
						getParameter(request.getParameter("agent")));
				this.InputParameters.put("com_type",
						getParameter(request.getParameter("com_type")));
				this.InputParameters.put("check_number",
						getParameter(request.getParameter("check_number")));
				this.InputParameters.put("po_list",
						getParameter(request.getParameter("po_list")));
				for (Map.Entry<String, String> c : this.InputParameters
						.entrySet()) {
					if (c.getValue() == null) {
						missing_p = missing_p + " " + (String) c.getKey();
					} else if ((((String) c.getKey()).equals("check_number"))
							&& (((String) c.getValue()).equals(""))) {
						missing_p = missing_p + " " + (String) c.getKey();
					} else if ((((String) c.getKey()).equals("po_list"))
							&& (((String) c.getValue()).equals(""))) {
						missing_p = missing_p + " " + (String) c.getKey();
					}
				}
				if (!missing_p.equals("")) {
					this.ErrorMessage = ("Request is missing required parameter(s). Missing values for " + missing_p);
					rc = false;
				}
				break;
			case 4:
				this.InputParameters.put("timeframe",
						getParameter(request.getParameter("timeframe")));
				this.InputParameters.put("agent",
						getParameter(request.getParameter("agent")));
				this.InputParameters.put("partner_name",
						getParameter(request.getParameter("partner_name")));
				this.InputParameters.put("com_type",
						getParameter(request.getParameter("com_type")));
				this.InputParameters.put("email",
						getParameter(request.getParameter("email")));
				this.InputParameters.put("email_address",
						getParameter(request.getParameter("email_address")));
				for (Map.Entry<String, String> c : this.InputParameters
						.entrySet()) {
					if (c.getValue() == null) {
						missing_p = missing_p + " " + (String) c.getKey();
					}
				}
				if (!missing_p.equals("")) {
					this.ErrorMessage = ("Request is missing required parameter(s). Missing values for " + missing_p);
					rc = false;
				}
				break;
			}
		} catch (RuntimeException re) {
			this.ErrorMessage = re.getMessage();
			rc = false;
		}
		return rc;
	}

	boolean listPayableCommissions(HttpServletRequest request) {
		boolean rc = true;

		getApprovedCommissionList(request, "Approved");

		getAgentsName();

		return rc;
	}

	void getApprovedCommissionList(HttpServletRequest request, String status) {
		String columns = "lo_ot_name, lx_pr_title, lm_js_title, convert(varchar(10),lm_po_complete_date,101) as lm_po_complete_date, " +
				"lm_pi_status, lm_pi_approved_total, convert(varchar(10),lm_pi_approved_date,101) as lm_pi_approved_date, " +
				"convert(varchar(10),lm_pi_payment_scheduled,101) as lm_pi_payment_scheduled, lm_po_id, lm_po_partner_id, " +
				"lm_js_invoice_no, convert(varchar(10),lm_pi_gp_paid_date,101) as lm_pi_gp_paid_date, convert(varchar(10),lm_pi_invoice_date,101) as lm_pi_invoice_date";

		String sql = buildApprovedCommisionQuery(columns, status);

		request.setAttribute("rows",
				getCommissionList(sql, breakDownColumns(columns)));
	}

	String buildApprovedCommisionQuery(String columns, String status) {
		String where1 = " po_status = 3 and lm_po_commission_flag = 1 and lm_pi_status = '"
				+ status
				+ "'"
				+ " and lm_pwo_terms_master_data = 'Quarterly 90 Days Arrears/Paid Invoices'";

		String where2 = " and lm_po_partner_id = "
				+ (String) this.InputParameters.get("agent") + " and"
				+ " convert(varchar(10),lm_pi_payment_scheduled,101) = '"
				+ (String) this.InputParameters.get("timeframe") + "'";
		if (!this.PO_SelectedList.equals("")) {
			where2 = where2 + "and lm_po_id in (" + this.PO_SelectedList + ")";
		}
		String sql = "select "
				+ columns
				+ " from lm_purchase_order"
				+ " join lm_partner_invoice on lm_po_id = lm_pi_po_id"
				+ " join lm_pwo_terms_master on lm_pwo_terms_master_id = lm_po_terms"
				+ " join lm_job on lm_po_js_id = lm_js_id"
				+ " join dbo.ap_appendix_list_01 on lm_js_pr_id = lx_pr_id"
				+ " where" + where1 + where2
				+ " order by lo_ot_name, lx_pr_title";

		return sql;
	}

	Vector<String[]> getCommissionList(String sql, String[] columns) {
		Vector<String[]> rows = new Vector();

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[columns.length];
				for (int i = 0; i < columns.length; i++) {
					if (columns[i].equals("lm_pi_approved_total")) {
						row[i] = this.nf2.format(rs.getDouble(columns[i]));
					} else {
						row[i] = rs.getString(columns[i]);
					}
				}
				rows.add(row);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return rows;
	}

	void getAgentsName() {
		String sql = "select partner_name, cp_pd_primary_first_name, cp_pd_primary_last_name, cp_pd_primary_contact_email  from dbo.cp_partner_standard_partner_application_01 where cp_pd_partner_id = "
				+

				(String) this.InputParameters.get("agent");

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				this.InputParameters.put("partner_name",
						rs.getString("partner_name"));
				this.InputParameters.put("first_name",
						rs.getString("cp_pd_primary_first_name"));
				this.InputParameters.put("last_name",
						rs.getString("cp_pd_primary_last_name"));
				this.InputParameters.put("email_address",
						rs.getString("cp_pd_primary_contact_email"));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	boolean getSelectedPOs(HttpServletRequest request) {
		boolean rc = false;

		String[] selected_POs = request
				.getParameterValues("checked_commissions");

		String del = "";
		if (selected_POs != null) {
			if (selected_POs.length > 0) {
				rc = true;
				for (int i = 0; i < selected_POs.length; i++) {
					this.PO_SelectedList = (this.PO_SelectedList + del + selected_POs[i]);

					del = ",";
				}
			}
		}
		return rc;
	}

	boolean executePayment() {
		boolean rc = false;

		String po_list = (String) this.InputParameters.get("po_list");
		String check_number = (String) this.InputParameters.get("check_number");
		if (paidCommissions(po_list, check_number)) {
			rc = true;
		}
		return rc;
	}

	boolean paidCommissions(String po_list, String check_number) {
		boolean rc = false;

		Vector<String> po_processed_successfully = new Vector();
		this.PO_ErrorList.clear();

		String[] po = po_list.split(",");
		if (po.length > 0) {
			for (int i = 0; i < po.length; i++) {
				if (!payTransaction(po[i], check_number)) {
					break;
				}
				po_processed_successfully.add(po[i]);
			}
			if (po_processed_successfully.size() != po.length) {
				determinedMissed_POs(po, po_processed_successfully);
			} else {
				rc = true;
			}
		}
		return rc;
	}

	boolean payTransaction(String po, String check_number) {
		boolean rc = false;

		int sp_rc = 0;

		String sp = "{? = call lm_esa_commission_execute_payment_01 (?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sp);
			stmt.registerOutParameter(1, 4);
			stmt.setObject("@lm_po_id", po);
			stmt.setObject("@check_number", check_number);

			stmt.execute();

			sp_rc = stmt.getInt(1);
			if (sp_rc == 0) {
				rc = true;
			} else {
				sp_rc = 5;
			}
		} catch (Exception e) {
			System.out.println(e);
			sp_rc = 5;
		} finally {
			if (!rc) {
				this.ErrorMessage = ("Error in pay transaction with " + this.TxErrors[sp_rc]);
			}
		}
		return rc;
	}

	void determinedMissed_POs(String[] po_list, Vector<String> processed_po) {
		this.PO_ErrorList.clear();
		for (int i = 0; i < po_list.length; i++) {
			if (!processed_po.contains(po_list[i])) {
				this.PO_ErrorList.add(po_list[i]);
			}
		}
	}

	boolean buildEmailRequestForm(HttpServletRequest request) {
		boolean rc = false;

		String check_number = (String) this.InputParameters.get("check_number");
		String timeframe = (String) this.InputParameters.get("timeframe");
		this.PO_SelectedList = ((String) this.InputParameters.get("po_list"));

		getApprovedCommissionList(request, "Paid");
		Vector commissions = (Vector) request.getAttribute("rows");

		getAgentsName();

		request.setAttribute("email",
				buildHTMLBody(commissions, check_number, timeframe));

		return true;
	}

	String buildHTMLBody(Vector c, String cn, String tf) {
		double email_total = 0.0D;

		String email_body = "Agent Commission Payment Summmary<br><br>";

		email_body = email_body
				+ "Per your agent agreement, this email summarizes commission payments for the timeframe listed below.<br><br>";

		email_body = email_body + "Timeframe: " + tf + "<br><br>";

		email_body = email_body + "Check Number: " + cn + "<br><br>";

		email_body = email_body
				+ "<table border='1' cellspacing='0' cellpadding='5'>";

		email_body = email_body
				+ "<tr><td align='center'>Customer (Appendix)</td><td align='center'>Job/Site</td><td align='center'>Reference</td><td align='center'>Commission</td></tr>";
		for (int i = 0; i < c.size(); i++) {
			String[] r = (String[]) c.get(i);
			email_total += Double.parseDouble(r[5]);
			email_body = email_body + "<tr><td align='left'>" +

			r[0] + "&nbsp;(" + r[1] + ")</td>" + "<td align='left'>" + r[2]
					+ "</td>" + "<td align='center'>" + r[8] + "</td>"
					+ "<td align='right'>" + r[5] + "</td>" + "</tr>";
		}
		email_body =

		email_body
				+ "<tr><td colspan='3' align='right'><strong>Total</strong></td><td align='right'><strong>$"
				+ this.nf2.format(email_total) + "</strong></td>" + "</tr>";

		email_body = email_body + "</table>";

		email_body = email_body
				+ "<br>Please see your <a href='https://www.contingent.com/partner_login'>AgentCare Resource Center</a> " +
				"for additional details.<br><br>Don't know or forgot your login, contact Contingent Sales for help.";

		return email_body;
	}

	boolean sendConfirmation() {
		boolean rc = false;

		String email_to_address = (String) this.InputParameters
				.get("email_address");
		String email_body = (String) this.InputParameters.get("email");

		email_to_address = "cmurray@contingent.net";

		Map<String, String> email = new HashMap();

		email.put("from", "accounting@contingent.net");
		email.put("to", email_to_address);
		email.put("cc", "jstine@contingent.net,dallgeier@contingent.net");
		email.put("subject", "Agent Payment Summary");
		email.put("message", email_body);
		email.put("content_type", "text/html");

		EmailNotification send_msg = new EmailNotification();
		rc = send_msg.sendNotification(email);

		return rc;
	}

	String getParameter(String p) {
		if (p != null) {
			p = p.trim();
		}
		return p;
	}

	String[] breakDownColumns(String select) {
		String[] columns = select.split(", ");
		for (int i = 0; i < columns.length; i++) {
			if (columns[i].contains(" as ")) {
				String[] as_col = columns[i].split(" as ");
				columns[i] = as_col[1].trim();
			}
		}
		return columns;
	}

	public static void main(String[] args) {
	}
}
