package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.util.Vector;

public class Appendix {
	String lx_pr_id;
	String lp_mm_id;
	String lx_pr_status;
	String lx_pr_title;
	String lx_pr_type;
	String lo_ot_name;
	String sales_contact;
	String project_contact;
	CostElement financialData;
	Vector jobs = new Vector();
	Integer jobCount = Integer.valueOf(0);
	DataAccess da = new DataAccess();

	public Appendix(String app_id) {
		this.lx_pr_id = app_id;
		if (getAppendixInformation()) {
			collectJobs();
		}
		System.out.println("Appendix: " + this.lo_ot_name + "/"
				+ this.lx_pr_title + " Jobs: " + this.jobCount);
	}

	boolean getAppendixInformation() {
		boolean appendix_status = false;

		String sql = "exec ac_lm_appendix_information_01 " + this.lx_pr_id;
		ResultSet rs = this.da.getResultSet(sql);
		try {
			rs.next();

			this.lx_pr_id = rs.getString("lx_pr_id");
			this.lp_mm_id = rs.getString("lp_mm_id");
			this.lx_pr_status = rs.getString("lx_pr_status");
			this.lx_pr_title = rs.getString("lx_pr_title");
			this.lx_pr_type = rs.getString("lx_pr_type");
			this.lo_ot_name = rs.getString("lo_ot_name");
			this.project_contact = rs.getString("project_contact");
			this.sales_contact = rs.getString("sales_contact");

			this.financialData = new CostElement(this.lx_pr_id, "('P')");

			rs.close();
			appendix_status = true;
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
		return appendix_status;
	}

	private void collectJobs() {
		int i = 0;

		String sql = "select lm_js_id from lm_job where lm_js_pr_id = "
				+ this.lx_pr_id
				+ " and lm_js_status = 'A' and "
				+ "lm_js_prj_status in ('I', 'F', 'O', 'H')  and "
				+ "lm_js_title not like 'Addendum%' and lm_js_type not in ('Default', 'Addendum') and "
				+ "lower(lm_js_title) not like '%test%'";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				Job job = new Job(rs.getString("lm_js_id"));
				if ((job.lm_js_prj_status.equals("O"))
						|| (job.lm_js_prj_status.equals("F"))) {
					System.out.println(job.lm_js_id + "\t"
							+ job.financialData.lx_ce_total_price + "\t"
							+ job.lm_js_prj_status);
				}
				this.jobs.add(i, job);
				i++;
			}
			rs.close();
			this.jobCount = Integer.valueOf(i);
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	public static void main(String[] args) {
		Appendix x = new Appendix("115");
	}
}
