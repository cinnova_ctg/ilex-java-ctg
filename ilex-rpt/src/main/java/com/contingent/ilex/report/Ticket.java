package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class Ticket {
	String Ignore = "OK";
	String lm_js_id;
	String PurchaseOrderTypeSummary;
	String ResourceName;
	Vector PurchaseOrderType = new Vector();
	Vector ResourceType = new Vector();
	String Labor = "No";
	String Travel = "No";
	Vector BuyQty = new Vector();
	Vector BuyUnitCost = new Vector();
	Vector SellQty = new Vector();
	Vector SellUnitCost = new Vector();
	Double TotalPrice;
	Double TotalTravelPrice;
	Double SumSellTravelQty;
	Double TotalLaborPrice;
	Double SumSellLaborQty;
	Double TotalCost;
	Double TotalTravelCost;
	Double SumAuthTravelQty;
	Double TotalLaborCost;
	Double SumAuthLaborQty;
	Integer TotalTimeOnsite;
	Integer CountOfDispatches;
	Double CU_LaborBuyQty;
	Double CU_TravelBuyQty;
	Double CU_LaborSellQty;
	Double CU_TravelSellQty;
	Double CU_LaborBuyAvgUnit;
	Double CU_TravelBuyAvgUnit;
	Double CU_LaborSellAvgUnit;
	Double CU_TravelSellAvgUnit;
	Double CU_LaborBuyTotal;
	Double CU_TravelBuyTotal;
	Double CU_LaborSellTotal;
	Double CU_TravelSellTotal;
	Double CU_TotalCost;
	Double CU_TotalPrice;
	Double CU_EstimatedPrice;

	Ticket(String lm_js_id, DataAccess da) {
		this.lm_js_id = lm_js_id;

		getTicketData(da);

		this.PurchaseOrderTypeSummary = testPO_Type();

		testResourcesFound();
		if (!this.PurchaseOrderTypeSummary.equals("Mix")) {
			if (this.CountOfDispatches.intValue() == 1) {
				if (this.TotalTimeOnsite.intValue() > 0) {
					ticketData();
				} else {
					this.Ignore = "Bad TOS";
				}
			} else {
				this.Ignore = "Multiple Dispatches";
			}
		} else {
			this.Ignore = "Multiple PO Types";
		}
	}

	void ticketData() {
		this.SumAuthLaborQty = computeTotalQty("L", this.BuyQty);
		this.SumAuthTravelQty = computeTotalQty("T", this.BuyQty);

		this.SumSellLaborQty = computeTotalQty("L", this.SellQty);
		this.SumSellTravelQty = computeTotalQty("T", this.SellQty);

		this.TotalLaborPrice = computeTotalByResource("L", this.SellUnitCost,
				this.SellQty);
		this.TotalTravelPrice = computeTotalByResource("T", this.SellUnitCost,
				this.SellQty);
		this.TotalPrice = Double.valueOf(this.TotalTravelPrice.doubleValue()
				+ this.TotalLaborPrice.doubleValue());

		this.TotalTravelCost = computeTotalByResource("T", this.BuyUnitCost,
				this.BuyQty);
		this.TotalLaborCost = computeTotalByResource("L", this.BuyUnitCost,
				this.BuyQty);
		this.TotalCost = Double.valueOf(this.TotalTravelCost.doubleValue()
				+ this.TotalLaborCost.doubleValue());

		this.CU_LaborBuyQty = mapLaborBuyQty(this.TotalTimeOnsite.intValue());
		this.CU_TravelBuyQty = mapTravelBuyQty(this.SumAuthTravelQty);
		if (this.PurchaseOrderTypeSummary.equals("M")) {
			this.CU_LaborBuyQty = correctMinutemanLaborBuy(this.CU_LaborBuyQty);
			this.CU_TravelBuyQty = correctMinutemanTravelBuy(this.CU_TravelBuyQty);
		} else if (this.PurchaseOrderTypeSummary.equals("S")) {
			this.CU_LaborBuyQty = correctSpeedpayLaborBuy(this.CU_LaborBuyQty);
		}
		this.CU_LaborSellQty = mapLaborSellQty(this.TotalTimeOnsite.intValue());
		this.CU_TravelSellQty = mapTravelSellQty(this.TotalTimeOnsite
				.intValue());

		this.CU_LaborBuyAvgUnit = computeAvgUnit("L", this.BuyUnitCost,
				this.BuyQty);
		this.CU_TravelBuyAvgUnit = computeAvgUnit("T", this.BuyUnitCost,
				this.BuyQty);

		this.CU_LaborSellAvgUnit = computeAvgUnit("L", this.SellUnitCost,
				this.SellQty);
		this.CU_TravelSellAvgUnit = computeAvgUnit("T", this.SellUnitCost,
				this.SellQty);

		this.CU_LaborSellTotal = Double.valueOf(this.CU_LaborSellAvgUnit
				.doubleValue() * this.CU_LaborSellQty.doubleValue());
		this.CU_TravelSellTotal = Double.valueOf(this.CU_TravelSellAvgUnit
				.doubleValue() * this.CU_TravelSellQty.doubleValue());

		this.CU_LaborBuyTotal = Double.valueOf(this.CU_LaborBuyAvgUnit
				.doubleValue() * this.CU_LaborBuyQty.doubleValue());
		this.CU_TravelBuyTotal = Double.valueOf(this.CU_TravelBuyAvgUnit
				.doubleValue() * this.CU_TravelBuyQty.doubleValue());

		this.CU_TotalCost = Double.valueOf(this.CU_LaborBuyTotal.doubleValue()
				+ this.CU_TravelBuyTotal.doubleValue());
		this.CU_TotalPrice = Double.valueOf(this.CU_LaborSellTotal
				.doubleValue() + this.CU_TravelSellTotal.doubleValue());

		this.CU_EstimatedPrice = copperPrice(this.TotalTimeOnsite.intValue(),
				Double.valueOf(98.0D));
	}

	Double computeAvgUnit(String type, Vector unit, Vector qty) {
		Double q = Double.valueOf(0.0D);
		Double u = Double.valueOf(0.0D);
		Double ui = Double.valueOf(0.0D);
		String rt = "";

		Double weighted_unit_cost = Double.valueOf(0.0D);
		for (int i = 0; i < unit.size(); i++) {
			rt = (String) this.ResourceType.get(i);
			if (type.equals(rt)) {
				q = Double.valueOf(q.doubleValue()
						+ ((Double) qty.get(i)).doubleValue());
				u = Double.valueOf(u.doubleValue()
						+ ((Double) unit.get(i)).doubleValue());
				ui = Double.valueOf(ui.doubleValue() + 1.0D);
				weighted_unit_cost = Double.valueOf(weighted_unit_cost
						.doubleValue()
						+ ((Double) unit.get(i)).doubleValue()
						* ((Double) qty.get(i)).doubleValue());
			}
		}
		if (q.doubleValue() > 0.0D) {
			weighted_unit_cost = Double.valueOf(weighted_unit_cost
					.doubleValue() / q.doubleValue());
		} else {
			weighted_unit_cost = Double.valueOf(u.doubleValue()
					/ ui.doubleValue());
		}
		if ((type.equals("L")) && (this.Labor.equals("No"))) {
			weighted_unit_cost = Double.valueOf(0.0D);
		}
		if ((type.equals("T")) && (this.Travel.equals("No"))) {
			weighted_unit_cost = Double.valueOf(0.0D);
		}
		return weighted_unit_cost;
	}

	Double computeTotalByResource(String type, Vector unit, Vector qty) {
		Double total = Double.valueOf(0.0D);
		String rt = "";
		for (int i = 0; i < unit.size(); i++) {
			rt = (String) this.ResourceType.get(i);
			if (type.equals(rt)) {
				total = Double.valueOf(total.doubleValue()
						+ ((Double) qty.get(i)).doubleValue()
						* ((Double) unit.get(i)).doubleValue());
			}
		}
		return total;
	}

	Double computeTotalQty(String type, Vector qty) {
		Double q = Double.valueOf(0.0D);
		String rt = "";
		for (int i = 0; i < qty.size(); i++) {
			rt = (String) this.ResourceType.get(i);
			if (type.equals(rt)) {
				q = Double.valueOf(q.doubleValue()
						+ ((Double) qty.get(i)).doubleValue());
			}
		}
		return q;
	}

	void getTicketData(DataAccess da) {
		String sql = "exec su_multiple_dispatch_analysis_01_temp "
				+ this.lm_js_id;

		int i = 0;

		ResultSet rs = da.getResultSet(sql);
		try {
			while (rs.next()) {
				this.PurchaseOrderType.add(rs.getString(2));

				this.ResourceType.add(rs.getString(3));

				this.BuyQty.add(Double.valueOf(rs.getDouble(4)));
				this.BuyUnitCost.add(Double.valueOf(rs.getDouble(5)));

				this.SellQty.add(Double.valueOf(rs.getDouble(6)));
				this.SellUnitCost.add(Double.valueOf(rs.getDouble(7)));
				if (i == 0) {
					this.ResourceName = rs.getString(1);
					this.TotalTimeOnsite = Integer.valueOf(rs.getInt(8));
					this.CountOfDispatches = Integer.valueOf(rs.getInt(9));
					i++;
				}
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	Double mapLaborBuyQty(int tos) {
		Double qty = Double.valueOf(0.0D);
		if (tos > 15) {
			int t1 = tos / 60;
			qty = Double.valueOf(t1 * 1.0D);
			int t2 = tos % 60;
			if (t2 > 45) {
				qty = Double.valueOf(qty.doubleValue() + 1.0D);
			} else if (t2 > 30) {
				qty = Double.valueOf(qty.doubleValue() + 0.75D);
			} else if (t2 > 15) {
				qty = Double.valueOf(qty.doubleValue() + 0.5D);
			} else if (t2 > 0) {
				qty = Double.valueOf(qty.doubleValue() + 0.25D);
			}
		} else {
			qty = Double.valueOf(0.25D);
		}
		return qty;
	}

	Double mapTravelBuyQty(Double travel) {
		if (travel.doubleValue() > 1.0D) {
			travel = Double.valueOf(1.0D);
		}
		return travel;
	}

	Double mapLaborSellQty(int tos) {
		Double qty = Double.valueOf(0.0D);
		if (tos > 120) {
			int t1 = tos / 60;
			qty = Double.valueOf(t1 * 1.0D);
			int t2 = tos % 60;
			if (t2 > 30) {
				qty = Double.valueOf(qty.doubleValue() + 1.0D);
			} else if (t2 > 0) {
				qty = Double.valueOf(qty.doubleValue() + 0.5D);
			}
		} else {
			qty = Double.valueOf(2.0D);
		}
		return qty;
	}

	Double mapTravelSellQty(int travel) {
		Double qty = Double.valueOf(0.0D);

		qty = Double.valueOf(1.0D);

		return qty;
	}

	Double correctMinutemanLaborBuy(Double mm_qty) {
		if (mm_qty.doubleValue() < 2.0D) {
			mm_qty = Double.valueOf(2.0D);
		}
		return mm_qty;
	}

	Double correctSpeedpayLaborBuy(Double sp_qty) {
		if (sp_qty.doubleValue() < 2.0D) {
			sp_qty = Double.valueOf(2.0D);
		}
		return sp_qty;
	}

	Double correctMinutemanTravelBuy(Double mm_qty) {
		if (mm_qty.doubleValue() != 1.0D) {
			mm_qty = Double.valueOf(1.0D);
		}
		return mm_qty;
	}

	String testPO_Type() {
		String po_type = "";
		Map po_test = new HashMap();
		for (int i = 0; i < this.PurchaseOrderType.size(); i++) {
			po_type = (String) this.PurchaseOrderType.get(i);
			po_test.put(po_type, "");
		}
		if (po_test.size() > 1) {
			po_type = "Mix";
		}
		return po_type;
	}

	void testResourcesFound() {
		String type = "";
		for (int i = 0; i < this.ResourceType.size(); i++) {
			type = (String) this.ResourceType.get(i);
			if (type.equals("L")) {
				this.Labor = "Yes";
			} else if (type.equals("T")) {
				this.Travel = "Yes";
			}
		}
	}

	Double copperPrice(int tos, Double rate) {
		int min = 0;

		Double cu_price = Double.valueOf(0.0D);
		int t1;
		if (tos > 120) {
			min = tos - 120;
			t1 = 2 + min / 60;
			int t2 = min % 60;
			if (t2 > 0) {
				t1++;
			}
		} else {
			t1 = 2;
		}
		cu_price = Double.valueOf(t1 * rate.doubleValue());

		return cu_price;
	}

	Double copperMargin(Double cost, Double rate) {
		Double m = Double.valueOf(1.0D - cost.doubleValue()
				/ rate.doubleValue());

		return m;
	}
}
