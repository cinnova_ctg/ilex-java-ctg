package com.contingent.ilex.report;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.contingent.ilex.utility.ReportUtils;

public class RevenueGraphAction extends Action {
	private static final long serialVersionUID = 1L;
	private HttpSession session = null;
	LinkedHashMap<Integer, RevenueChartData> monthData = null;
	private String dtype = "";
	private int dstartYM = 0;
	private int dendYM = 0;
	private String spman = "";
	private String bdman = "";
	private String jowner = "";
	private String client = "";
	private String ptype = "";

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		StringBuffer sb = new StringBuffer();
		getRequestParams(request);
		ArrayList<Integer> allDates = (ArrayList) this.session
				.getAttribute("iDatesArray");

		String chartUrl = developChartData(allDates);
		String chartTable = buildChartTable();
		sb.append("<table width=100%><tr><td colspan=3 height='5px' style='border-top:solid 2px black;'><br></td></tr>");
		sb.append("<tr><td align='right'>" + chartUrl
				+ "</td width=1%><td><br></td><td align = 'left'>" + chartTable
				+ "</td></tr></table>");

		response.setContentType("text/xml;charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(sb.toString());
		return null;
	}

	private void getRequestParams(HttpServletRequest req) {
		this.dtype = req.getParameter("datetype");
		this.dstartYM = Integer.parseInt(req.getParameter("startdateYM"));
		this.dendYM = Integer.parseInt(req.getParameter("enddateYM"));
		this.spman = req.getParameter("spmgr");
		this.bdman = req.getParameter("bdmgr");
		this.jowner = req.getParameter("jowner");
		this.client = req.getParameter("clients");
		this.ptype = req.getParameter("ptype");
		this.session = req.getSession();
	}

	private String developChartData(ArrayList<Integer> aDates) {
		String msg = "";
		BigDecimal maxVal = new BigDecimal("0.00");
		BigDecimal chartScalingFactor = new BigDecimal("1.05");
		StringBuffer rev = new StringBuffer();
		StringBuffer vgp = new StringBuffer();
		StringBuffer vcogs = new StringBuffer();
		StringBuffer month = new StringBuffer();
		ArrayList<String> year = new ArrayList();
		StringBuffer url = new StringBuffer();

		boolean firstFlag = true;

		this.monthData = new LinkedHashMap();
		ArrayList<Integer> intDates = ReportUtils.getDatesAsInts(aDates,
				this.dstartYM, this.dendYM);
		this.monthData = DWSqlBean.getChartData(intDates, this.dstartYM,
				this.dendYM, this.dtype, this.spman, this.bdman, this.jowner,
				this.client, this.ptype);
		if (this.monthData.isEmpty()) {
			System.out
					.println("monthData was empty...returning an empty string");
			msg = "";
			return msg;
		}
		Collection<RevenueChartData> c = this.monthData.values();
		Iterator<RevenueChartData> itr = c.iterator();
		while (itr.hasNext()) {
			RevenueChartData temp = (RevenueChartData) itr.next();
			if (temp.getRevenue().compareTo(maxVal) == 1) {
				maxVal = temp.getRevenue();
			}
			if (firstFlag) {
				rev.append(temp.getRevenue().toString());
				vgp.append(temp.getVgp().toString());
				vcogs.append(temp.getVcogs().toString());
				month.append(temp.getMonth());
				year.add(Integer.toString(temp.getYear()));
				firstFlag = false;
			} else {
				rev.append("," + temp.getRevenue().toString());
				vgp.append("," + temp.getVgp().toString());
				vcogs.append("," + temp.getVcogs().toString());
				month.append("|" + temp.getMonth());
				year.add(Integer.toString(temp.getYear()));
			}
		}
		int dataHgt = maxVal.multiply(chartScalingFactor,
				new MathContext(0, RoundingMode.HALF_UP)).intValue();
		url.append("<img src='http://chart.apis.google.com/chart?chs="
				+ getChartDimensions(this.monthData.size()) + "&amp;");

		url.append("chtt=Revenue,+VGP+and+VCOGS&amp;");

		url.append("chts=3333CC,19&amp;chf=bg,s,FFFFCC|c,s,E2E2E2&amp;");

		url.append("chd=t:" + vcogs.toString() + "|");
		url.append(vgp.toString() + "|");
		url.append(rev.toString() + "&amp;");

		url.append("chxt=x,y,x&amp;chxl=0:|" + month.toString() + "|1:|"
				+ getYaxisLabels(dataHgt) + "|2:|" + getYearLabels(year, false)
				+ "&amp;");

		url.append("chxr=1,0," + dataHgt + "&amp;");

		url.append("chxp=1," + getYaxisPositions(dataHgt) + "&amp;");

		url.append("chdl=VCOGS (%)|VGP (%)|REVENUE ($)&amp;");

		url.append("chco=990000,0000CC,009900&amp;");

		url.append("chbh=9,1,8");

		url.append("&amp;cht=bvg&amp;chds=0," + dataHgt + "&amp;chg=0,"
				+ getYaxisGridWidth(dataHgt));
		url.append("'alt='Revenue Report Chart' />");

		return url.toString();
	}

	private String buildChartTable() {
		StringBuffer sb = new StringBuffer();
		String prevYear = "";
		RevenueChartData temp = null;
		float vcogsPercent = 0.0F;
		float vgpPercent = 0.0F;

		DecimalFormat df = new DecimalFormat("##.#");
		sb.append("<table class='charttable' border='1' cellpadding='3' cellspacing='3' ");
		sb.append("rules='none' frame='box' bordercolor='#000000'>");
		sb.append("<tr><th class='thyearcell'>Year</th><th class='thmonthcell'>Month</th>");
		sb.append("<th class='thcell'>VCOGS</th><th class='thcell'>VGP</th><th class='thcell'>Revenue</th></tr>");
		Collection<RevenueChartData> c = this.monthData.values();
		Iterator<RevenueChartData> itr = c.iterator();
		while (itr.hasNext()) {
			temp = (RevenueChartData) itr.next();

			sb.append("<tr>");

			String thisYear = Integer.toString(temp.getYear());
			if (!thisYear.equals(prevYear)) {
				sb.append("<td class='yearcell'>" + thisYear + "</td>");
			} else {
				sb.append("<td></td>");
			}
			prevYear = thisYear;

			sb.append("<td class='monthcell'>" + temp.getMonth() + "</td>");
			int rev = temp.getRevenue().setScale(0, 4).intValue();
			if (rev != 0) {
				vcogsPercent = temp.getVcogs().setScale(0, 4).intValue() / rev
						* 100.0F;
				vgpPercent = temp.getVgp().setScale(0, 4).intValue() / rev
						* 100.0F;
			}
			sb.append("<td class='vcogscell' align='right'>"
					+ df.format(vcogsPercent) + "%</td>");
			sb.append("<td class='vgpcell' align='right'>"
					+ df.format(vgpPercent) + "%</td>");
			sb.append("<td class='revcell' align='right'>"
					+ ReportUtils.currencyFormatIntValue(rev) + "</td>");

			sb.append("</tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	private String getChartDimensions(int size) {
		int width = 170 + 37 * size;
		if (width > 1000) {
			width = 1000;
		}
		return width + "x" + 300;
	}

	private String getYearLabels(ArrayList<String> yearList, boolean showAll) {
		String lastYear = "";
		boolean EQUAL_FLAG = false;
		String thisYear = "";
		String prevYear = "";
		StringBuffer sb = new StringBuffer();
		thisYear = (String) yearList.get(0);
		lastYear = (String) yearList.get(yearList.size() - 1);
		sb.append(thisYear);
		if (thisYear.equals(lastYear)) {
			EQUAL_FLAG = true;
		}
		for (int i = 1; i < yearList.size(); i++) {
			prevYear = thisYear;
			thisYear = (String) yearList.get(i);
			if (showAll) {
				sb.append("|" + thisYear);
			} else if (((EQUAL_FLAG) && (i == yearList.size() - 1))
					|| (!thisYear.equals(prevYear))) {
				sb.append("|" + thisYear);
			} else {
				sb.append("|");
			}
		}
		return sb.toString();
	}

	private String getYaxisLabels(int chartHeight) {
		StringBuffer sb = new StringBuffer();
		int topVal = ReportUtils.roundInt(chartHeight, 2);
		topVal = (int) (topVal - topVal * 0.05F);
		int increment = topVal / 4;
		sb.append("$0|" + ReportUtils.currencyFormatIntValue(increment));
		for (int i = 2; i <= 4; i++) {
			sb.append("|" + ReportUtils.currencyFormatIntValue(increment * i));
		}
		return sb.toString();
	}

	private String getYaxisPositions(int chartHeight) {
		StringBuffer sb = new StringBuffer();
		int topVal = ReportUtils.roundInt(chartHeight, 2);
		topVal = (int) (topVal - topVal * 0.05F);
		int increment = topVal / 4;
		sb.append("0," + increment);
		for (int i = 2; i <= 4; i++) {
			sb.append("," + increment * i);
		}
		return sb.toString();
	}

	private String getYaxisGridWidth(int chartHeight) {
		int topVal = ReportUtils.roundInt(chartHeight, 2);
		topVal = (int) (topVal - topVal * 0.05F);
		int increment = topVal / 4;
		float incrPercent = increment / chartHeight * 100.0F;
		DecimalFormat df = new DecimalFormat("##.#");
		String retVal = df.format(incrPercent);

		return retVal;
	}
}
