package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class ScheduledJobs extends DispatchAction {
	DataAccess da = new DataAccess();
	String WhereClause = "";

	public ActionForward dg_scheduled(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "dg_scheduled";

		managePageRequest1(request);

		return mapping.findForward(forward_key);
	}

	String managePageRequest1(HttpServletRequest request) {
		String x = "";

		Map inputs = getInputParameters(request);

		String wc = buildWhereClause(inputs);
		if (inputs.get("lx_pr_id") != null) {
			getData(request, inputs, wc);
		}
		return x;
	}

	void getData(HttpServletRequest request, Map inputs, String wc) {
		String sql = "select substring (lm_js_title, 10,len(lm_js_title)), convert(varchar(20),lm_js_planned_start_date, 1), right(convert(varchar(24), lm_js_planned_start_date), 7)+' - '+right(convert(varchar(24), dateadd(mi, 30, lm_js_planned_start_date)),7),        lo_pc_last_name, lm_si_city, lm_si_state, case when lo_om_division is not null then lo_om_division else 'Not Assigned' end,        lm_js_prj_status        from lm_job              join lm_site_detail on lm_js_si_id=lm_si_id            join lo_poc on lm_js_created_by = lo_pc_id             left join lm_purchase_order on lm_js_id=lm_po_js_id             left join cp_partner on lm_po_partner_id = cp_partner_id             left join lo_organization_main on cp_partner_om_id = lo_om_id  where "
				+

				wc + " order by lm_js_planned_start_date";

		Vector rows = new Vector();
		String[] row = new String[8];
		System.out.println(" " + sql);
		int i = 0;
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				row = new String[8];

				row[0] = rs.getString(1);
				row[1] = rs.getString(2);
				row[2] = rs.getString(3);
				row[3] = rs.getString(4);
				row[4] = rs.getString(5);
				row[5] = rs.getString(6);
				row[6] = rs.getString(7);
				row[7] = rs.getString(8);

				rows.add(i++, row);
			}
			rs.close();

			request.setAttribute("rows", rows);
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
	}

	String buildWhereClause(Map inputs) {
		String wc = "";
		String delimiter = "";

		Vector expressions = new Vector();
		int i = 0;

		boolean lx_pr_id = inputs.get("lx_pr_id") != null;
		boolean from_date = (inputs.get("from_date") != null)
				&& (!((String) inputs.get("from_date")).equals(""));
		boolean to_date = (inputs.get("to_date") != null)
				&& (!((String) inputs.get("to_date")).equals(""));
		boolean all_jobs = (inputs.get("all_jobs") != null)
				&& (!((String) inputs.get("all_jobs")).equals(""));
		System.out.println("all_jobs " + all_jobs);
		if (lx_pr_id) {
			expressions.add(i++,
					"lm_js_pr_id = " + (String) inputs.get("lx_pr_id"));
		}
		if (!all_jobs) {
			if (from_date) {
				expressions.add(i++, "lm_js_planned_start_date >= '"
						+ (String) inputs.get("from_date") + "'");
			}
			if (to_date) {
				expressions.add(i++,
						"lm_js_planned_start_date < dateadd(dd,1,'"
								+ (String) inputs.get("to_date") + "')");
			}
		}
		wc = " ";
		delimiter = "";
		for (int j = 0; j < expressions.size(); j++) {
			wc = wc + delimiter + expressions.get(j);
			delimiter = " and ";
		}
		return wc;
	}

	Map getInputParameters(HttpServletRequest request) {
		Vector requestParameters = initializeInputParameters();

		Map inputs = new HashMap();
		for (int i = 0; i < requestParameters.size(); i++) {
			String parameter = (String) requestParameters.get(i);
			String value = request.getParameter(parameter);
			if ((value != null) && (value.trim().length() > 0)) {
				inputs.put(parameter, value);
				request.setAttribute(parameter, value);
			}
		}
		request.setAttribute("requestParameters", requestParameters);

		return inputs;
	}

	Vector initializeInputParameters() {
		Vector inputParameters = new Vector();

		inputParameters.clear();
		inputParameters.add(0, "lx_pr_id");
		inputParameters.add(1, "from_date");
		inputParameters.add(2, "to_date");
		inputParameters.add(3, "all_jobs");
		return inputParameters;
	}
}
