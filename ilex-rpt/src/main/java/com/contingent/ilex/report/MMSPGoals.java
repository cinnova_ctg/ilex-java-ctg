package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class MMSPGoals extends DispatchAction {
	Map<String, Map<String, Double[]>> GoalsByMonth = new HashMap();
	String[] color = { "FF9999", "9999FF", "99FF99" };
	String[] months = { "Aug", "Sep", "Oct", "Nov", "Dec", "Jan", "Feb", "Mar",
			"Apr", "May", "Jun", "Jul" };
	Vector<String> HartMissedMonths = new Vector();
	DataAccess da = new DataAccess();
	NumberFormat decimal_percentage = new DecimalFormat("#00");
	NumberFormat percentage = NumberFormat.getPercentInstance();
	NumberFormat comma_delimited = new DecimalFormat("#,###");
	SimpleDateFormat mmm_yyyy = new SimpleDateFormat("MMM-yyyy");
	SimpleDateFormat mmm = new SimpleDateFormat("MMM");
	SimpleDateFormat yyyy_MM_dd = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat yyyy_MM = new SimpleDateFormat("yyyy-MM");
	SimpleDateFormat MMM_YYYY = new SimpleDateFormat("MMM yyyy");

	public ActionForward mmsp_goals1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		buildGoals();

		String[] month = getDate(request);

		buildHeatChartSPM(request, month);

		monthlyProgression2(request);

		getSPM_StackedCharts(request);

		Double[] monthly_mean = computeGoalAverages(month[0]);

		Double[] jo_goals = determineGoals();

		buildHeatChart(request, jo_goals, monthly_mean);

		String forward_key = "mmsp_goals1";

		return mapping.findForward(forward_key);
	}

	void buildHeatChart(HttpServletRequest request, Double[] goals,
			Double[] monthly_goals) {
		Vector<String[]> formats = new Vector();
		Vector<String[]> rows = new Vector();
		try {
			String sql = "select jo, (1.0*m)/(m+s+p)*1.0, (1.0*s)/(m+s+p)*1.0, ((m+s)*1.0)/(m+s+p), (m+s+p) from (select jo, type        from datawarehouse.dbo.ac_fact_po_usage_summary where day>dateadd(dd, -30, getdate()) and jo in (select (lo_pc_last_name+', '+lo_pc_first_name) from ilex.dbo.lo_poc where lo_pc_active_user = 1 and lo_pc_om_id = 4)) s         PIVOT (COUNT(type) FOR type IN ([M],[S],[P])) p  where (m+s+p) > 5 order by jo";

			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				String[] row = new String[5];
				String[] format = new String[3];

				row[4] = rs.getString(1);
				if (row[4].indexOf(",") > 9) {
					row[4] = row[4].substring(0, row[4].indexOf(","));
				} else {
					row[4] = row[4].substring(0, row[4].indexOf(",") + 3);
				}
				format[0] = this.color[testGoal2(
						Double.valueOf(rs.getDouble(2)), monthly_goals[0],
						goals[3])];
				row[0] = this.decimal_percentage
						.format(rs.getDouble(2) * 100.0D);

				format[1] = this.color[testGoal2(
						Double.valueOf(rs.getDouble(3)), monthly_goals[1],
						goals[4])];
				row[1] = this.decimal_percentage
						.format(rs.getDouble(3) * 100.0D);

				format[2] = this.color[testGoal2(
						Double.valueOf(rs.getDouble(4)), monthly_goals[2],
						goals[4])];
				row[2] = this.decimal_percentage
						.format(rs.getDouble(4) * 100.0D);

				row[3] = this.comma_delimited.format(rs.getDouble(5));

				formats.add(format);
				rows.add(row);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		Map<String, String[]> jo = getVGP_JO();
		NumberFormat dp = new DecimalFormat("#.00");
		String dataset = "";
		String tool_tip = "";
		for (int i = 0; i < rows.size(); i++) {
			String[] row = (String[]) rows.get(i);
			String[] format = (String[]) formats.get(i);
			String[] vgp = (String[]) jo.get(row[4]);

			tool_tip = row[4] + "{br}  Minuteman: " + row[0] + "%"
					+ "{br}  Speedpay: " + row[1] + "%" + "{br}  Combined: "
					+ row[2] + "%" + "{br}  Count: " + row[3];
			try {
				if (vgp != null) {
					vgp[0] = this.comma_delimited.format(Double
							.parseDouble(vgp[0]));
					vgp[1] = this.comma_delimited.format(Double
							.parseDouble(vgp[1]));

					vgp[2] = dp.format(Double.parseDouble(vgp[2]));
					vgp[3] = dp.format(Double.parseDouble(vgp[3]));

					tool_tip = tool_tip + "{br}VGP:{br}  Expense: " + vgp[0]
							+ "{br}  Revenue " + vgp[1] + "{br}  VGPM: "
							+ vgp[2] + "{br}  Delta: " + vgp[3];
				}
			} catch (Exception e) {
				System.out.println("{br}VGP:{br}  Expense: " + vgp[0]
						+ "{br}  Revenue " + vgp[1] + "{br}  VGPM: " + vgp[2]
						+ "{br}  Delta: " + vgp[3]);
			}
			dataset = dataset + " <set columnId='" + row[4]
					+ "' rowId='Minuteman' color='" + format[0]
					+ "' displayValue='" + row[0] + "' toolText='" + tool_tip
					+ "' /> \\\n";
			dataset = dataset + " <set columnId='" + row[4]
					+ "' rowId='Speedpay' color='" + format[1]
					+ "'  displayValue='" + row[1] + "' toolText='" + tool_tip
					+ "' /> \\\n";
			dataset = dataset + " <set columnId='" + row[4]
					+ "' rowId='Combined'  color='" + format[2]
					+ "' displayValue='" + row[2] + "' toolText='" + tool_tip
					+ "' /> \\\n";
		}
		String xml = buildHeatChartElementsJO(request, dataset);

		request.setAttribute("heat1", xml);
	}

	String buildHeatChartElementsJO(HttpServletRequest request, String dataset) {
		String xml = "\"<chart xAxislabelDisplay='ROTATE' rotatexAxisLabels ='1' slantxAxisLabels ='1' mapByCategory='1' showBorder ='0'  baseFontColor='ffffff'  bgColor='000000' bgAlpha='100' showBorder='0' borderColor='000000'  canvasBorderThickness='0' canvasBorderColor='FFFFFF' plotBorderColor='FFFFFF'  showToolTip='1' toolTipBgColor='FAF8CC' toolTipBorderColor='ababab' showColumnIdInToolTip='0' showRowIdInToolTip='0' showToolTipShadow='1'> \\\n<dataset> \\\n"
				+

				dataset
				+ "</dataset> \\\n"
				+ "<styles>"
				+ "<definition><style name='myToolTipFont' type='font' font='Verdana' size='9' color='000000'/></definition>"
				+ "<application><apply toObject='ToolTip' styles='myToolTipFont' /></application>"
				+ "</styles>" + "</chart>\"";

		request.setAttribute("heat_xml", xml);

		String ms_line_chart = "<div id='heat_chart_jo' align='center'>Waiting...</div>\n<script type='text/javascript'>\nvar heat_data_jo1 = ~data_url~\nvar myChart = new FusionCharts('vgp/HeatMap.swf', 'chtid1', '1480', '270', '0', '0');\nmyChart.setDataXML(heat_data_jo1);\nmyChart.setTransparent(true);\nmyChart.render('heat_chart_jo');\n</script>";

		ms_line_chart = ms_line_chart.replace("~data_url~", xml);

		return ms_line_chart;
	}

	Map<String, String[]> getVGP_JO() {
		Map<String, String[]> vgp = new HashMap();
		String name = "";

		String sql = "select lo_pc_last_name+', '+left(lo_pc_last_name,1), sum(ac_ru_pro_forma), sum(ac_ru_expense), sum(ac_ru_revenue),        case when sum(ac_ru_revenue) > 0.0 then 1-(sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) else 0.00 end,        case when sum(ac_ru_revenue) > 0.0 then 1-(sum(ac_ru_expense)/sum(ac_ru_revenue)) else 0.00 end,        case when sum(ac_ru_revenue) > 0.0 then 1-(sum(ac_ru_expense)/sum(ac_ru_revenue))-(1-(sum(ac_ru_pro_forma)/sum(ac_ru_revenue))) else 0.00 end        from datawarehouse.dbo.ac_dimensions_job        join datawarehouse.dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id        join ilex.dbo.lo_poc on ac_jd_job_owner = lo_pc_id and lo_pc_active_user = 1 and lo_pc_om_id = 4        join datawarehouse.dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id        where ac_jd_date_closed > dateadd(dd, -30, getdate()) and ac_cl_minor2 = 'Billable'        group by lo_pc_last_name+', '+left(lo_pc_last_name,1)";
		try {
			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				String[] jo = new String[5];
				name = rs.getString(1);
				jo[0] = rs.getString(3);
				jo[1] = rs.getString(4);
				jo[2] = rs.getString(6);
				jo[3] = rs.getString(7);
				jo[4] = rs.getString(2);
				vgp.put(name, jo);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		return vgp;
	}

	Map<String, String[]> getVGP_SPM() {
		Map<String, String[]> vgp = new HashMap();
		String name = "";

		String sql = "select lo_pc_last_name, sum(ac_ru_pro_forma), sum(ac_ru_expense), sum(ac_ru_revenue),        case when sum(ac_ru_revenue) > 0.0 then 1-(sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) else 0.00 end,        case when sum(ac_ru_revenue) > 0.0 then 1-(sum(ac_ru_expense)/sum(ac_ru_revenue)) else 0.00 end,        case when sum(ac_ru_revenue) > 0.0 then 1-(sum(ac_ru_expense)/sum(ac_ru_revenue))-(1-(sum(ac_ru_pro_forma)/sum(ac_ru_revenue))) else 0.00 end        from datawarehouse.dbo.ac_dimensions_job        join datawarehouse.dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id        join ilex.dbo.lo_poc on ac_jd_spm = lo_pc_id and lo_pc_active_user = 1 and lo_pc_om_id = 4        join datawarehouse.dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id        where ac_jd_date_closed > dateadd(dd, -30, getdate()) and ac_cl_minor2 = 'Billable'        group by lo_pc_last_name";
		try {
			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				String[] jo = new String[5];
				name = rs.getString(1);
				jo[0] = rs.getString(3);
				jo[1] = rs.getString(4);
				jo[2] = rs.getString(6);
				jo[3] = rs.getString(7);
				jo[4] = rs.getString(2);
				vgp.put(name, jo);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		return vgp;
	}

	void buildHeatChartSPM(HttpServletRequest request, String[] month) {
		buildGoals();

		Double[] goals = new Double[3];
		String spm_last_name = "";
		String month_key = month[0].substring(0, 7);

		Vector<String[]> formats = new Vector();
		Vector<String[]> rows = new Vector();

		Map<String, Double[]> spms = new HashMap();
		try {
			String sql = "select spm, (1.0*m)/(m+s+p)*1.0, (1.0*s)/(m+s+p)*1.0, ((m+s)*1.0)/(m+s+p), (m+s+p)        from (select spm, type from datawarehouse.dbo.ac_fact_po_usage_summary               where spm != 'Shroder, Charles' and spm not like 'Supinger%' and month = '"
					+

					month[0]
					+ "') s "
					+ " PIVOT (COUNT(type) FOR type IN ([M],[S],[P])) p  "
					+ " order by spm";

			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				String[] row = new String[5];
				String[] format = new String[3];

				spm_last_name = rs.getString(1);
				spm_last_name = spm_last_name.substring(0,
						spm_last_name.indexOf(","));

				spms = (Map) this.GoalsByMonth.get(month_key);
				goals = (Double[]) spms.get(spm_last_name);

				format[0] = this.color[testGoal(
						Double.valueOf(rs.getDouble(2)), goals[0])];
				row[0] = (this.decimal_percentage
						.format(rs.getDouble(2) * 100.0D) + " of " + this.decimal_percentage
						.format(goals[0].doubleValue() * 100.0D));

				format[1] = this.color[testGoal(
						Double.valueOf(rs.getDouble(3)), goals[1])];
				row[1] = (this.decimal_percentage
						.format(rs.getDouble(3) * 100.0D) + " of " + this.decimal_percentage
						.format(goals[1].doubleValue() * 100.0D));

				format[2] = this.color[testGoal(
						Double.valueOf(rs.getDouble(4)), goals[2])];
				row[2] = (this.decimal_percentage
						.format(rs.getDouble(4) * 100.0D) + " of " + this.decimal_percentage
						.format(goals[2].doubleValue() * 100.0D));

				row[3] = spm_last_name;
				row[4] = this.comma_delimited.format(rs.getDouble(5));

				formats.add(format);
				rows.add(row);
			}
			rs.close();
			request.setAttribute("rows", rows);
			request.setAttribute("formats", formats);
		} catch (SQLException e) {
			System.out.println(e);
		}
		Map<String, String[]> spm = getVGP_SPM();
		NumberFormat dp = new DecimalFormat("#.00");

		String dataset = "";
		String tool_tip = "";
		for (int i = 0; i < rows.size(); i++) {
			String[] row = (String[]) rows.get(i);
			String[] format = (String[]) formats.get(i);
			String[] vgp = (String[]) spm.get(row[3]);
			
			if(vgp!=null){
				
			

			tool_tip = row[3] + "{br}  Minuteman: " + row[0] + "%"
					+ "{br}  Speedpay: " + row[1] + "%" + "{br}  Combined: "
					+ row[2] + "%" + "{br}  Count: " + row[4];
			if (vgp != null) {
				vgp[0] = this.comma_delimited
						.format(Double.parseDouble(vgp[0]));
				vgp[1] = this.comma_delimited
						.format(Double.parseDouble(vgp[1]));

				vgp[2] = dp.format(Double.parseDouble(vgp[2]));
				vgp[3] = dp.format(Double.parseDouble(vgp[3]));

				tool_tip = tool_tip + "{br}VGP:{br}  Expense: " + vgp[0]
						+ "{br}  Revenue " + vgp[1] + "{br}  VGPM: " + vgp[2]
						+ "{br}  Delta: " + vgp[3];
			}
			dataset = dataset + " <set columnId='" + row[3]
					+ "' rowId='Minuteman' color='" + format[0]
					+ "' displayValue='" + row[0] + "' toolText='" + tool_tip
					+ "' /> \\\n";
			dataset = dataset + " <set columnId='" + row[3]
					+ "' rowId='Speedpay'  color='" + format[1]
					+ "' displayValue='" + row[1] + "' toolText='" + tool_tip
					+ "' /> \\\n";
			dataset = dataset + " <set columnId='" + row[3]
					+ "' rowId='Combined'  color='" + format[2]
					+ "' displayValue='" + row[2] + "' toolText='" + tool_tip
					+ "' /> \\\n";
			}
		}
		String xml_spm = buildHeatChartElementsSPM(request, dataset);

		request.setAttribute("heat2", xml_spm);
		try {
			request.setAttribute("month",
					this.MMM_YYYY.format(this.yyyy_MM.parse(month[0])));
		} catch (ParseException localParseException) {
		}
	}

	String buildHeatChartElementsSPM(HttpServletRequest request, String dataset) {
		String xml = "\"<chart xAxislabelDisplay='ROTATE' rotatexAxisLabels ='1' slantxAxisLabels ='1' mapByCategory='1' showBorder ='0'  bgColor='000000' bgAlpha='100' showBorder='0' borderColor='000000'  canvasBorderThickness='0' canvasBorderColor='FFFFFF' plotBorderColor='FFFFFF'  baseFontColor='ffffff' showToolTip='1' toolTipBgColor='FAF8CC' toolTipBorderColor='ababab' showColumnIdInToolTip='0' showRowIdInToolTip='0' showToolTipShadow='1'> \\\n<dataset> \\\n"
				+

				dataset
				+ "</dataset> \\\n"
				+ "<styles>"
				+ "<definition><style name='myToolTipFont' type='font' font='Verdana' size='9' color='000000'/></definition>"
				+ "<application><apply toObject='ToolTip' styles='myToolTipFont' /></application>"
				+ "</styles>" + "</chart>\"";

		request.setAttribute("heat_xml", xml);

		String ms_line_chart = "<div id='heat_chart_spm' align='center'>Waiting...</div>\n<script type='text/javascript'>\nvar heat_data_spm1 = ~data_url~\nvar myChart = new FusionCharts('vgp/HeatMap.swf', 'chtid2', '360', '360', '0', '0');\nmyChart.setDataXML(heat_data_spm1);\nmyChart.setTransparent(true);\nmyChart.render('heat_chart_spm');\n</script>";

		ms_line_chart = ms_line_chart.replace("~data_url~", xml);

		return ms_line_chart;
	}

	public ActionForward mmsp_goals(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		buildGoals();

		String[] month = getDate(request);

		getSPMs(request, month);

		getSPM_StackedCharts(request);

		Double[] monthly_mean = computeGoalAverages(month[0]);

		Double[] jo_goals = determineGoals();

		getJobOwners(request, jo_goals, monthly_mean);

		request.setAttribute("monthly_mean", monthly_mean);
		request.setAttribute("jo_goals", jo_goals);

		String forward_key = "mmsp_goals";

		return mapping.findForward(forward_key);
	}

	Vector<String[]> getSPMs(HttpServletRequest request, String[] month) {
		String spm_last_name = "";
		String month_key = month[0].substring(0, 7);
		Double[] goals = new Double[3];

		Vector<int[]> formats = new Vector();
		Vector<String[]> rows = new Vector();

		Map<String, Double[]> spms = new HashMap();
		try {
			String sql = "select spm, (1.0*m)/(m+s+p)*1.0, (1.0*s)/(m+s+p)*1.0, ((m+s)*1.0)/(m+s+p), (m+s+p)        from (select spm, type from datawarehouse.dbo.ac_fact_po_usage_summary               where spm != 'Shroder, Charles' and spm not like 'Supinger%' and month = '"
					+

					month[0]
					+ "') s "
					+ " PIVOT (COUNT(type) FOR type IN ([M],[S],[P])) p  "
					+ " order by spm";

			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				String[] row = new String[4];
				int[] format = new int[3];

				spm_last_name = rs.getString(1);
				spm_last_name = spm_last_name.substring(0,
						spm_last_name.indexOf(","));
				if (spm_last_name.equals("Courtney")) {
					spm_last_name = "Hart";
				}
				spms = (Map) this.GoalsByMonth.get(month_key);
				goals = (Double[]) spms.get(spm_last_name);

				format[0] = testGoal(Double.valueOf(rs.getDouble(2)), goals[0]);
				row[0] = (this.percentage.format(rs.getDouble(2)) + " of " + this.percentage
						.format(goals[0]));

				format[1] = testGoal(Double.valueOf(rs.getDouble(3)), goals[1]);
				row[1] = (this.percentage.format(rs.getDouble(3)) + " of " + this.percentage
						.format(goals[1]));

				format[2] = testGoal(Double.valueOf(rs.getDouble(4)), goals[2]);
				row[2] = (this.percentage.format(rs.getDouble(4)) + " of " + this.percentage
						.format(goals[2]));

				row[3] = spm_last_name;

				formats.add(format);
				rows.add(row);
			}
			rs.close();
			request.setAttribute("rows", rows);
			request.setAttribute("formats", formats);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return rows;
	}

	int testGoal(Double value, Double target) {
		int result = 1;
		if (value.doubleValue() < target.doubleValue() - 0.01D) {
			result = 0;
		} else if (value.doubleValue() > target.doubleValue() + 0.01D) {
			result = 2;
		}
		return result;
	}

	String[] getDate(HttpServletRequest request) {
		String[] month = new String[3];
		Date d = new Date();
		GregorianCalendar cd = new GregorianCalendar();
		if (request.getParameter("month") != null) {
			cd = new GregorianCalendar();
			try {
				cd.setTime(this.yyyy_MM_dd.parse(request.getParameter("month")));
				month[0] = (this.yyyy_MM.format(cd.getTime()) + "-01");
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else {
			cd.add(5, -5);
			cd.get(2);
			month[0] = (this.yyyy_MM.format(cd.getTime()) + "-01");
		}
		GregorianCalendar cd1 = cd;
		cd1.add(2, -1);
		month[1] = (this.yyyy_MM.format(cd.getTime()) + "-01");
		GregorianCalendar cd2 = cd;
		cd2.add(2, 2);
		month[2] = (this.yyyy_MM.format(cd.getTime()) + "-01");

		return month;
	}

	void buildGoals() {
		this.GoalsByMonth.clear();

		Map<String, Double[]> spm = new HashMap();

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.46D), Double.valueOf(0.25D)));
		spm.put("Carlton",
				newGoals(Double.valueOf(0.3D), Double.valueOf(0.32D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.31D), Double.valueOf(0.43D)));
		spm.put("Hart", newGoals(Double.valueOf(0.42D), Double.valueOf(0.28D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.42D), Double.valueOf(0.28D)));
		this.GoalsByMonth.put("2011-09", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.48D), Double.valueOf(0.27D)));
		spm.put("Carlton",
				newGoals(Double.valueOf(0.33D), Double.valueOf(0.34D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.32D), Double.valueOf(0.44D)));
		spm.put("Hart", newGoals(Double.valueOf(0.45D), Double.valueOf(0.29D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.45D), Double.valueOf(0.29D)));
		this.GoalsByMonth.put("2011-10", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.49D), Double.valueOf(0.29D)));
		spm.put("Carlton",
				newGoals(Double.valueOf(0.36D), Double.valueOf(0.38D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.33D), Double.valueOf(0.45D)));
		spm.put("Hart", newGoals(Double.valueOf(0.48D), Double.valueOf(0.3D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.48D), Double.valueOf(0.3D)));
		this.GoalsByMonth.put("2011-11", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.5D), Double.valueOf(0.3D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.4D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.34D), Double.valueOf(0.46D)));
		spm.put("Hart", newGoals(Double.valueOf(0.5D), Double.valueOf(0.3D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.5D), Double.valueOf(0.3D)));
		this.GoalsByMonth.put("2011-12", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.55D), Double.valueOf(0.3D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.4D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.39D), Double.valueOf(0.46D)));
		spm.put("Hart", newGoals(Double.valueOf(0.55D), Double.valueOf(0.3D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.55D), Double.valueOf(0.3D)));
		this.GoalsByMonth.put("2012-01", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.55D), Double.valueOf(0.3D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.4D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.39D), Double.valueOf(0.46D)));
		spm.put("Hart", newGoals(Double.valueOf(0.55D), Double.valueOf(0.3D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.55D), Double.valueOf(0.3D)));
		this.GoalsByMonth.put("2012-02", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		this.GoalsByMonth.put("2012-03", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		this.GoalsByMonth.put("2012-04", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		this.GoalsByMonth.put("2012-05", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		this.GoalsByMonth.put("2012-06", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		this.GoalsByMonth.put("2012-07", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		this.GoalsByMonth.put("2012-08", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		this.GoalsByMonth.put("2012-09", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		this.GoalsByMonth.put("2012-10", spm);

		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		this.GoalsByMonth.put("2012-11", spm);

		spm = new HashMap();
		spm.put("Ackels", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		this.GoalsByMonth.put("2012-12", spm);
		
		spm = new HashMap();
		spm.put("Zea", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Shumaker", newGoals(Double.valueOf(0.49D), Double.valueOf(0.29D)));
		this.GoalsByMonth.put("2015-01", spm);

		spm = new HashMap();
		spm.put("Ackels", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Shumaker", newGoals(Double.valueOf(0.49D), Double.valueOf(0.29D)));
		this.GoalsByMonth.put("2015-02", spm);
		
		spm = new HashMap();
		spm.put("Ackels", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Shumaker", newGoals(Double.valueOf(0.49D), Double.valueOf(0.29D)));
		this.GoalsByMonth.put("2015-03", spm);
		
		spm = new HashMap();
		spm.put("Ackels", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Shumaker", newGoals(Double.valueOf(0.49D), Double.valueOf(0.29D)));
		this.GoalsByMonth.put("2015-04", spm);
		
		spm = new HashMap();
		spm.put("Ackels", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Shumaker", newGoals(Double.valueOf(0.49D), Double.valueOf(0.29D)));
		this.GoalsByMonth.put("2015-05", spm);
		
		spm = new HashMap();
		spm.put("Ackels", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Shumaker", newGoals(Double.valueOf(0.49D), Double.valueOf(0.29D)));
		this.GoalsByMonth.put("2015-06", spm);
		
		spm = new HashMap();
		spm.put("Ackels", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Shumaker", newGoals(Double.valueOf(0.49D), Double.valueOf(0.29D)));
		this.GoalsByMonth.put("2015-07", spm);
		
		spm = new HashMap();
		spm.put("Ackels", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Carlton", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Kuhr", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Hart", newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Courtney",
				newGoals(Double.valueOf(0.6D), Double.valueOf(0.4D)));
		spm.put("Shumaker", newGoals(Double.valueOf(0.49D), Double.valueOf(0.29D)));
		this.GoalsByMonth.put("2015-08", spm);
	}

	Double[] newGoals(Double m, Double s) {
		Double[] msc = new Double[3];

		msc[0] = m;
		msc[1] = s;
		msc[2] = Double.valueOf(m.doubleValue() + s.doubleValue());

		return msc;
	}

	Double[] computeGoalAverages(String month) {
		Double mm = Double.valueOf(0.0D);
		Double sp = Double.valueOf(0.0D);
		Double composite = Double.valueOf(0.0D);
		Double i = Double.valueOf(0.0D);

		month = month.substring(0, 7);

		Double[] msc_avg = new Double[3];

		Map<String, Double[]> current_month = (Map) this.GoalsByMonth
				.get(month);
		for (Map.Entry<String, Double[]> spms : current_month.entrySet()) {
			Double[] usages = (Double[]) spms.getValue();

			mm = Double.valueOf(mm.doubleValue() + usages[0].doubleValue());
			sp = Double.valueOf(sp.doubleValue() + usages[1].doubleValue());
			composite = Double.valueOf(composite.doubleValue()
					+ usages[2].doubleValue());
			i = Double.valueOf(i.doubleValue() + 1.0D);
		}
		msc_avg[0] = Double.valueOf(mm.doubleValue() / i.doubleValue());
		msc_avg[1] = Double.valueOf(sp.doubleValue() / i.doubleValue());
		msc_avg[2] = Double.valueOf(composite.doubleValue() / i.doubleValue());

		return msc_avg;
	}

	void getSPM_StackedCharts(HttpServletRequest request) {
		String[] mm = new String[12];
		String[] sp = new String[12];
		String[] mth = new String[12];
		for (int i = 0; i < 12; i++) {
			mm[i] = "";
			sp[i] = "";
			int tmp43_41 = i;
			String[] tmp43_39 = mth;
			tmp43_39[tmp43_41] = tmp43_39[tmp43_41];
		}
		String xml_static1 = " \"<chart numbersuffix='%25' yaxisminvalue='0' yaxismaxvalue='100'  showValues='0' numberPrefix=''  bgColor='000000' bgAlpha='100' showBorder='0' borderColor='000000'  canvasBorderThickness ='1' canvasBorderColor='CBCBCB' canvasBgColor='CBCBCB'  showLegend='1' legendBgColor='000000' legendBorderColor='000000' legendShadow='0' plotGradientColor=' ' baseFontColor='ffffff' showToolTip='1' toolTipBgColor='FAF8CC' toolTipBorderColor='ababab' showColumnIdInToolTip='0' showRowIdInToolTip='0' showToolTipShadow='1'> \\\n";

		String xml_static2 = "<trendLines><line startValue='98' color='B99C7A' displayValue='2012 Goal' valueOnRight='1' thickness='3' /></trendLines><styles><definition><style name='myToolTipFont' type='font' font='Verdana' size='9' color='000000'/><style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' /></definition><application><apply toObject='Canvas' styles='CanvasAnim' /><apply toObject='ToolTip' styles='myToolTipFont' /></application></styles></chart>\";";

		String s1 = "<dataset seriesName='MM' showValue='0' color='00cc99'>";
		String s2 = "<dataset seriesName='SP' showValue='0' color='0099ff'>";
		String cat = "<categories>";
		try {
			String sql = "select convert(char(3), month, 7), (1.0*m)/(m+s+p)*1.0, (1.0*s)/(m+s+p)*1.0, ((m+s)*1.0)/(m+s+p), (m+s+p)        from (select month, type from datawarehouse.dbo.ac_fact_po_usage_summary where day > dateadd(mm, -12, getdate())) s  PIVOT (COUNT(type) FOR type IN ([M],[S],[P])) p  where (m+s+p) > 2  order by month";

			ResultSet rs = this.da.getResultSet(sql);
			int i = 0;
			while (rs.next()) {
				mm[i] = this.decimal_percentage
						.format(rs.getDouble(2) * 100.0D);
				sp[i] = this.decimal_percentage
						.format(rs.getDouble(3) * 100.0D);
				mth[i] = rs.getString(1);
				i++;
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		for (int i = 0; i < this.months.length; i++) {
			s1 = s1 + "<set value='" + mm[i] + "' />";
			s2 = s2 + "<set value='" + sp[i] + "' />";
			cat = cat + "<category label='" + mth[i] + "' />";
		}
		s1 = s1 + "</dataset> \\\n";
		s2 = s2 + "</dataset> \\\n";
		cat = cat + "</categories> \\\n";

		String ms_line_chart = "<div id='progression_chart' align='center'>Waiting...</div>\n<script type='text/javascript'>\nvar progression_chart = ~data_url~\nvar myChart = new FusionCharts('vgp/StackedColumn2D.swf', 'progression_chart', '450', '340', '0', '0');\nmyChart.setDataXML(progression_chart);\nmyChart.setTransparent(true);\nmyChart.render('progression_chart');\n</script>";

		String xml = xml_static1 + cat + s1 + s2 + xml_static2;
		ms_line_chart = ms_line_chart.replace("~data_url~", xml);

		request.setAttribute("chart1", ms_line_chart);
	}

	Vector<String[]> getJobOwners(HttpServletRequest request, Double[] goals,
			Double[] monthly_goals) {
		Vector<int[]> formats = new Vector();
		Vector<String[]> rows = new Vector();
		try {
			String sql = "select jo, (1.0*m)/(m+s+p)*1.0, (1.0*s)/(m+s+p)*1.0, ((m+s)*1.0)/(m+s+p), (m+s+p) from (select jo, type        from datawarehouse.dbo.ac_fact_po_usage_summary where day>dateadd(dd, -30, getdate()) and jo in (select (lo_pc_last_name+', '+lo_pc_first_name) from ilex.dbo.lo_poc where lo_pc_active_user = 1 and lo_pc_om_id = 4)) s         PIVOT (COUNT(type) FOR type IN ([M],[S],[P])) p  where (m+s+p) > 2 order by jo";

			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				String[] row = new String[5];
				int[] format = new int[3];

				row[4] = rs.getString(1);

				format[0] = testGoal2(Double.valueOf(rs.getDouble(2)),
						monthly_goals[0], goals[3]);
				row[0] = this.percentage.format(rs.getDouble(2));

				format[1] = testGoal2(Double.valueOf(rs.getDouble(3)),
						monthly_goals[1], goals[4]);
				row[1] = this.percentage.format(rs.getDouble(3));

				format[2] = testGoal2(Double.valueOf(rs.getDouble(4)),
						monthly_goals[2], goals[4]);
				row[2] = this.percentage.format(rs.getDouble(4));

				row[3] = this.comma_delimited.format(rs.getDouble(5));

				formats.add(format);
				rows.add(row);
			}
			rs.close();

			request.setAttribute("jo_rows", rows);
			request.setAttribute("jo_formats", formats);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return rows;
	}

	int testGoal2(Double value, Double avg, Double std) {
		int result = 1;
		if (value.doubleValue() < avg.doubleValue()) {
			result = 0;
		} else if (value.doubleValue() > avg.doubleValue() + std.doubleValue()) {
			result = 2;
		}
		return result;
	}

	Double[] determineGoals() {
		Double[] goals = new Double[6];
		try {
			String sql = "with samples (job_owner, mm, sp, combined, sample)  as  (  select jo, (1.0*m)/(m+s+p)*1.0, (1.0*s)/(m+s+p)*1.0, ((m+s)*1.0)/(m+s+p), (m+s+p) from (select jo, type from datawarehouse.dbo.ac_fact_po_usage_summary where day>dateadd(dd, -30, getdate())) s   \t PIVOT (COUNT(type) FOR type IN ([M],[S],[P])) p    where (m+s+p) > 2  )  select avg(mm), avg(sp), avg(combined), stdev (mm), stdev(sp), stdev (combined)   from samples";

			ResultSet rs = this.da.getResultSet(sql);
			if (rs.next()) {
				goals[0] = Double.valueOf(rs.getDouble(1));
				goals[1] = Double.valueOf(rs.getDouble(2));
				goals[2] = Double.valueOf(rs.getDouble(3));
				goals[3] = Double.valueOf(rs.getDouble(4));
				goals[4] = Double.valueOf(rs.getDouble(5));
				goals[5] = Double.valueOf(rs.getDouble(6));
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		return goals;
	}

	void monthlyProgression2(HttpServletRequest request) {
		SimpleDateFormat mmm_yyyy = new SimpleDateFormat("MMM-yyyy");
		SimpleDateFormat mmm = new SimpleDateFormat("MMM");
		SimpleDateFormat yyyy_MM_dd = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat yyyy_MM = new SimpleDateFormat("yyyy-MM");
		SimpleDateFormat MMM_YYYY = new SimpleDateFormat("MMM yyyy");

		NumberFormat decimal = new DecimalFormat("#.000");

		Map<String, Map<String, String>> progression = new HashMap();

		Vector<String> month_order = new Vector();
		Map<String, Vector<String>> spm_dataset = new HashMap();

		String last_month = "";
		String month_key = "";
		String spm_key = "";
		String mm_sp = "";

		Double cnt_ms = Double.valueOf(0.0D);
		Double cnt_all = Double.valueOf(0.0D);

		this.HartMissedMonths.clear();
		this.HartMissedMonths.add("2011-07-01");

		String sql = "select month, spm, ((m+s)*1.0)/(m+s+p), m+s, (m+s+p)        from (select month, spm, type from datawarehouse.dbo.ac_fact_po_usage_summary                   where spm != 'Shroder, Charles' and spm not like 'Supinger%' and month >= '8/1/2011') s PIVOT (COUNT(type) FOR type IN ([M],[S],[P])) p  order by month, spm";
		try {
			ResultSet rs = this.da.getResultSet(sql);
			if (rs.next()) {
				last_month = rs.getString(1).substring(0, 10);

				Map<String, String> spms = new HashMap();
				month_key = rs.getString(1).substring(0, 10);
				spm_key = rs.getString(2).substring(0,
						rs.getString(2).indexOf(","));
				mm_sp = decimal.format(rs.getDouble(3) * 100.0D);
				spms.put(spm_key, mm_sp);

				cnt_ms = Double.valueOf(cnt_ms.doubleValue() + rs.getDouble(4));
				cnt_all = Double.valueOf(cnt_all.doubleValue()
						+ rs.getDouble(5));
				while (rs.next()) {
					if (!last_month.equals(rs.getString(1).substring(0, 10))) {
						if (this.HartMissedMonths.contains(last_month)) {
							spms.put("Hart", "0.0");
						}
						spms.put(
								"All",
								decimal.format(cnt_ms.doubleValue()
										/ cnt_all.doubleValue() * 100.0D));
						progression.put(last_month, spms);
						month_order.add(last_month);
						spms = new HashMap();
						last_month = rs.getString(1).substring(0, 10);
						cnt_ms = Double.valueOf(0.0D);
						cnt_all = Double.valueOf(0.0D);
					}
					month_key = rs.getString(1).substring(0, 10);
					spm_key = rs.getString(2).substring(0,
							rs.getString(2).indexOf(","));
					mm_sp = decimal.format(rs.getDouble(3) * 100.0D);
					spms.put(spm_key, mm_sp);

					cnt_ms = Double.valueOf(cnt_ms.doubleValue()
							+ rs.getDouble(4));
					cnt_all = Double.valueOf(cnt_all.doubleValue()
							+ rs.getDouble(5));
				}
				spms.put(
						"All",
						decimal.format(cnt_ms.doubleValue()
								/ cnt_all.doubleValue() * 100.0D));
				progression.put(last_month, spms);
				month_order.add(last_month);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		String category = "<categories>";
		for (int i = 0; i < month_order.size(); i++) {
			try {
				category = category
						+ "<category label='"
						+ MMM_YYYY.format(yyyy_MM_dd.parse((String) month_order
								.get(i))) + "' />";
			} catch (Exception localException) {
			}
			Map<String, String> spms = (Map) progression
					.get(month_order.get(i));
			for (Map.Entry<String, String> spm : spms.entrySet()) {
				if (spm_dataset.containsKey(spm.getKey())) {
					((Vector) spm_dataset.get(spm.getKey())).add((String) spm
							.getValue());
				} else {
					Vector<String> ds = new Vector();
					ds.add((String) spm.getValue());
					spm_dataset.put((String) spm.getKey(), ds);
				}
			}
		}
		category = "<categories>";
		for (int i = 0; i < this.months.length; i++) {
			category = category + "<category label='" + this.months[i] + "' />";
		}
		category = category + "</categories>";

		buildMonthlyProgressionChart(request, category, spm_dataset);
	}

	String buildMonthlyProgressionChart(HttpServletRequest request,
			String categories, Map<String, Vector<String>> spm_dataset) {
		String xml = "";
		String data_set = "";

		String[] spm_order = { "Carlton", "Kuhr", "Shumaker", "Ackels",  "Hart", "All" };
		String[] spm_color = { "00cc66", "ff9966", "cc6699", "DD77ff", "ff0000", "F0FC00" };
		for (int j = 0; j < spm_order.length; j++) {
			data_set = "<dataset seriesName='" + spm_order[j] + "' color='"
					+ spm_color[j] + "' >";
			Vector<String> ds = (Vector) spm_dataset.get(spm_order[j]);
			for (int i = 0; i < ds.size(); i++) {
				data_set = data_set
						+ "<set value='"
						+ (String) ds.get(i)
						+ "' toolText='"
						+ spm_order[j]
						+ ": "
						+ this.percentage.format(Double.parseDouble((String) ds
								.get(i)) / 100.0D) + "' />";
			}
			data_set = data_set + "</dataset>";
			xml = xml + data_set;
		}
		data_set = "<dataset seriesName='Target' color='B99C7A' >";

		data_set = data_set + "<set value='85' toolText='85%' />";
		data_set = data_set + "<set value='85' toolText='85%' />";
		data_set = data_set
				+ "<set value='98' toolText='Increase MM/SP to 98% in 2012' />";
		data_set = data_set
				+ "<set value='98' toolText='Increase MM/SP to 98% in 2012' />";
		data_set = data_set
				+ "<set value='98' toolText='Increase MM/SP to 98% in 2012' />";
		data_set = data_set
				+ "<set value='98' toolText='Increase MM/SP to 98% in 2012' />";
		data_set = data_set
				+ "<set value='98' toolText='Increase MM/SP to 98% in 2012' />";
		data_set = data_set
				+ "<set value='98' toolText='Increase MM/SP to 98% in 2012' />";
		data_set = data_set
				+ "<set value='98' toolText='Increase MM/SP to 98% in 2012' />";
		data_set = data_set
				+ "<set value='98' toolText='Increase MM/SP to 98% in 2012' />";
		data_set = data_set
				+ "<set value='98' toolText='Increase MM/SP to 98% in 2012' />";
		data_set = data_set
				+ "<set value='98' toolText='Increase MM/SP to 98% in 2012' />";

		data_set = data_set + "</dataset>";
		xml = xml + data_set;

		String xml_static1 = " \"<chart numbersuffix='%25' yaxisminvalue='0' yaxismaxvalue='100'  showValues='0' numberPrefix=''  bgColor='000000' bgAlpha='100' showBorder='0' borderColor='000000'  canvasBorderThickness ='1' canvasBorderColor='CBCBCB' canvasBgColor='CBCBCB'  showLegend='1' legendBgColor='000000' legendBorderColor='000000' legendShadow='0' plotGradientColor=' ' baseFontColor='ffffff' showToolTip='1' toolTipBgColor='FAF8CC' toolTipBorderColor='ababab' showColumnIdInToolTip='0' showRowIdInToolTip='0' showToolTipShadow='1'> \\\n";

		xml_static1 = xml_static1 + categories;

		xml_static1 = xml_static1 + xml;

		xml_static1 = xml_static1
				+ "<trendLines><line startValue='98' color='B99C7A' displayValue='2012 Goal' valueOnRight='1' thickness='2' /></trendLines><styles><definition><style name='myToolTipFont' type='font' font='Verdana' size='9' color='000000'/></definition><application><apply toObject='ToolTip' styles='myToolTipFont' /></application></styles></chart>\";";

		String ms_line_chart = "<div id='progression_chart_id' align='center'>Waiting...</div>\n<script type='text/javascript'>\nvar progression_chart1 = ~data_url~\nvar myChart = new FusionCharts('vgp/MSColumn2D.swf', 'prgcht1', '700', '340', '0', '0');\nmyChart.setDataXML(progression_chart1);\nmyChart.setTransparent(true);\nmyChart.render('progression_chart_id');\n</script>";

		ms_line_chart = ms_line_chart.replace("~data_url~", xml_static1);

		request.setAttribute("prog_chart1", ms_line_chart);

		return xml;
	}
}
