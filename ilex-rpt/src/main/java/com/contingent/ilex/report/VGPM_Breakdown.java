package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class VGPM_Breakdown extends DispatchAction {
	DataAccess da = new DataAccess();
	Vector inputParameterList = new Vector();
	Map inputParameterValue = new HashMap();
	boolean project = false;
	boolean pm = false;
	boolean owner = false;
	boolean team = false;

	public ActionForward vgpm_all(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "vgpm_all";
		String user = "";
		String sql = "";

		getInputParameters(request);

		sql = buildQuery(request, this.inputParameterValue);
		if (this.pm) {
			user = (String) this.inputParameterValue.get("pm");
		} else {
			user = (String) this.inputParameterValue.get("owner");
		}
		if (user.equals("All")) {
			user = "";
		}
		executeQuery(request, sql, user,
				(String) this.inputParameterValue.get("from_date"),
				(String) this.inputParameterValue.get("to_date"));

		setTitle(request);

		setInputParameters(request);

		return mapping.findForward(forward_key);
	}

	public ActionForward vgpm_all1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "vgpm_all1";
		String user = "";
		String sql = "";

		formatTable(request, executeQuery1(buildQuery1()));

		return mapping.findForward(forward_key);
	}

	String buildQuery(HttpServletRequest request, Map inputs) {
		String sql = "";
		String dynamic_columns = "";
		String common_columns = ", sum(ap_jl_estimated_cost)as estimated, sum(ap_jl_authorized) as authorized, sum(ap_jl_approved) as approved, sum(ap_jl_actual_cost) as actual, sum(ap_jl_price) as revenue, 1-sum(ap_jl_actual_cost)/sum(ap_jl_price) as vgpm, count(ap_jl_js_id) as qty, 1-sum(ap_jl_estimated_cost)/sum(ap_jl_price) as pro_vgpm, (1-sum(ap_jl_actual_cost)/sum(ap_jl_price))-(1-sum(ap_jl_estimated_cost)/sum(ap_jl_price)) as delta  ";
		String from = "from ap_job_level_summary ";
		String where = "where (ap_jl_supervisor is not null and ap_jl_employee is not null ) and ap_jl_status = 'Closed' ";
		String having = " having sum(ap_jl_price) > 0.0 ";
		String order_by = "";
		String group_columns = "";

		String pm_name = (String) inputs.get("pm");
		String ow_name = (String) inputs.get("owner");

		boolean from_date = (inputs.get("from_date") != null)
				&& (!((String) inputs.get("from_date")).equals(""));
		boolean to_date = (inputs.get("to_date") != null)
				&& (!((String) inputs.get("to_date")).equals(""));
		if (ow_name.equals("Active")) {
			from = from
					+ "join lo_poc on ap_jl_pc_id = lo_pc_id and lo_pc_active_user=1 ";
		}
		if (this.pm) {
			if (this.team) {
				where = where + "and ap_jl_supervisor = '"
						+ (String) inputs.get("pm") + "' ";
				dynamic_columns = "ap_jl_supervisor as pm, ap_jl_employee as owner";
				group_columns = "ap_jl_supervisor, ap_jl_employee";
				order_by = "ap_jl_supervisor, ap_jl_employee";
			} else if (this.project) {
				where = where + "and ap_jl_supervisor = '"
						+ (String) inputs.get("pm") + "' ";
				dynamic_columns = "ap_jl_pr_id, ap_jl_ot_name+'-'+ap_jl_pr_title as project, ap_jl_supervisor as pm";
				group_columns = "ap_jl_pr_id, ap_jl_ot_name+'-'+ap_jl_pr_title, ap_jl_supervisor";
				order_by = "ap_jl_pr_id, ap_jl_ot_name+'-'+ap_jl_pr_title, ap_jl_supervisor";
			} else {
				if ((!pm_name.equals("All")) && (!pm_name.equals("Active"))) {
					where = where + "and ap_jl_supervisor = '"
							+ (String) inputs.get("pm") + "' ";
				}
				dynamic_columns = "ap_jl_supervisor as pm";
				group_columns = "ap_jl_supervisor";
				order_by = "ap_jl_supervisor";
			}
		} else if (this.project) {
			where = where + "and ap_jl_employee = '"
					+ (String) inputs.get("owner") + "' ";
			dynamic_columns = "ap_jl_pr_id, ap_jl_ot_name+'-'+ap_jl_pr_title as project, ap_jl_employee as owner";
			group_columns = "ap_jl_pr_id, ap_jl_ot_name+'-'+ap_jl_pr_title, ap_jl_employee";
			order_by = "ap_jl_pr_id, ap_jl_ot_name+'-'+ap_jl_pr_title, ap_jl_employee";
		} else {
			if ((!ow_name.equals("All")) && (!ow_name.equals("Active"))) {
				where = where + "and ap_jl_employee = '"
						+ (String) inputs.get("owner") + "' ";
			}
			dynamic_columns = "ap_jl_employee as owner";
			group_columns = "ap_jl_employee";
			order_by = "ap_jl_employee";
		}
		if ((from_date) && (to_date)) {
			where = where + "and (ap_jl_month_year >= '"
					+ (String) inputs.get("from_date")
					+ "' and ap_jl_month_year < dateadd(dd, 1, '"
					+ (String) inputs.get("to_date") + "')) ";
		} else if (from_date) {
			where = where + "and (ap_jl_month_year >= '"
					+ (String) inputs.get("from_date") + "') ";
		} else if (to_date) {
			where = where + "and (ap_jl_month_year < dateadd(dd, 1, '"
					+ (String) inputs.get("to_date") + "')) ";
		}
		sql = "select " + dynamic_columns + common_columns + from + where
				+ "group by " + group_columns + " " + having + "order by "
				+ order_by;

		System.out.println(sql);

		return sql;
	}

	String buildQuery1() {
		String sql = "select PocReference, lo_pc_last_name+', '+lo_pc_first_name as name,        EstimatedCost,AuthorizedCost,ApprovedCost,VariableGrossProfit,Revenue, Margin, ProFormaMargin, MarginDelta,        convert(varchar(24), ReferenceDate, 100) as ReferenceDate, TotalJobs, RevenuePerJob   from datawarehouse.dbo.PocFinancialSummaryByRole join ilex.dbo.lo_poc on PocReference = lo_pc_id  where PocReferenceType = 'Job Owner' and TotalJobs > 10 and  PocReference not in (4,7, 49, 8792, 9817, 9460, 12276, 12289) order by lo_pc_last_name";

		return sql;
	}

	void executeQuery(HttpServletRequest request, String sql, String user,
			String from_date, String to_date) {
		Vector rows = new Vector();
		int i = 0;
		String url = "";
		String base_url = "VGPM.xo?form_action=vgpm_all";
		String from_to_query = "&from_date=" + from_date + "&to_date="
				+ to_date;

		String team_url = "";
		String project_url = "";

		NumberFormat formatter1 = NumberFormat.getCurrencyInstance();
		NumberFormat formatter2 = new DecimalFormat("0.00");
		double delta = 0.0D;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[14];
				if (this.project) {
					row[0] = rs.getString(2);
				}
				if (this.pm) {
					row[2] = rs.getString("pm");
					team_url = "&nbsp;&nbsp;<a href =\"" + base_url + "&pm="
							+ row[2] + "&team=1" + from_to_query
							+ "\">Team</a>&nbsp;&nbsp;";
					project_url = "&nbsp;&nbsp;<a href =\"" + base_url + "&pm="
							+ row[2] + "&project=1" + from_to_query
							+ "\">Projects</a>&nbsp;&nbsp;";

					row[2] = (row[2] + team_url + "|" + project_url);
					if (this.team) {
						row[1] = rs.getString("pm");
						row[2] = rs.getString("owner");
					} else if (this.project) {
						row[2] = rs.getString("pm");
					}
				} else {
					row[2] = rs.getString("owner");
					if (!this.project) {
						url = "VGPM.xo?form_action=vgpm_all&project=1&from_date="
								+ from_date
								+ "&to_date="
								+ to_date
								+ "&owner="
								+ row[2];
						row[2] = (row[2]
								+ "&nbsp;&nbsp;<span style=\"text-align : right\"><a href =\""
								+ url + "\">Projects</a></span>");
					}
				}
				row[3] = formatter1.format(rs.getDouble("estimated"));
				row[4] = formatter1.format(rs.getDouble("authorized"));
				row[5] = formatter1.format(rs.getDouble("approved"));
				row[6] = formatter1.format(rs.getDouble("actual"));
				row[7] = formatter1.format(rs.getDouble("revenue"));
				row[8] = formatter2.format(rs.getDouble("vgpm"));
				row[9] = rs.getString("qty");
				row[10] = formatter2.format(rs.getDouble("pro_vgpm"));

				delta = rs.getDouble("delta");
				row[11] = formatter2.format(delta);
				if (delta > 0.004D) {
					row[12] = "#e5f9de";
				} else if (delta < -0.004D) {
					row[12] = "#ffccc9";
				} else {
					row[12] = "#e0edf5";
					row[11] = "0.00";
				}
				rows.add(i++, row);
			}
			request.setAttribute("rows", rows);

			rs.close();
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
	}

	Vector executeQuery1(String sql) {
		Vector rows = new Vector();
		int i = 0;

		double delta = 0.0D;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[14];

				row[0] = rs.getString("PocReference");
				row[1] = rs.getString("name");

				row[2] = setMoneyFormat(Double.valueOf(rs
						.getDouble("EstimatedCost")));
				row[3] = setMoneyFormat(Double.valueOf(rs
						.getDouble("AuthorizedCost")));
				row[4] = setMoneyFormat(Double.valueOf(rs
						.getDouble("ApprovedCost")));
				row[5] = setMoneyFormat(Double.valueOf(rs
						.getDouble("VariableGrossProfit")));
				row[6] = setMoneyFormat(Double.valueOf(rs.getDouble("Revenue")));
				row[7] = setDecimalFormat(Double.valueOf(rs
						.getDouble("ProFormaMargin")));
				row[8] = setDecimalFormat(Double
						.valueOf(rs.getDouble("Margin")));
				delta = rs.getDouble("MarginDelta");
				row[9] = setDecimalFormat(Double.valueOf(delta));
				row[10] = rs.getString("ReferenceDate");
				row[10] = row[10].replace(" ", "&nbsp;");
				row[11] = rs.getString("TotalJobs");
				row[12] = setMoneyFormat(Double.valueOf(rs
						.getDouble("RevenuePerJob")));
				if (delta > 0.004D) {
					row[13] = "#e5f9de";
				} else if (delta < -0.004D) {
					row[13] = "#ffccc9";
				} else {
					row[13] = "#e0edf5";
					row[9] = "0.00";
				}
				rows.add(i++, row);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
		return rows;
	}

	void formatTable(HttpServletRequest request, Vector rows) {
		String vgpm_summary = "";
		for (int i = 0; i < rows.size(); i++) {
			String[] row = (String[]) rows.get(i);

			vgpm_summary = vgpm_summary + "<tr>\n";
			vgpm_summary = vgpm_summary
					+ " <td class=\"ccaa\" style=\"background-color:" + row[13]
					+ "\">" + row[1] + "</td>";
			vgpm_summary = vgpm_summary
					+ " <td class=\"ccac\" style=\"background-color:" + row[13]
					+ "\">" + row[2] + "</td>";
			vgpm_summary = vgpm_summary
					+ " <td class=\"ccac\" style=\"background-color:" + row[13]
					+ "\">" + row[3] + "</td>";
			vgpm_summary = vgpm_summary
					+ " <td class=\"ccac\" style=\"background-color:" + row[13]
					+ "\">" + row[4] + "</td>";
			vgpm_summary = vgpm_summary
					+ " <td class=\"ccac\" style=\"background-color:" + row[13]
					+ "\">" + row[6] + "</td>";
			vgpm_summary = vgpm_summary
					+ " <td class=\"ccab\" style=\"background-color:" + row[13]
					+ "\">" + row[7] + "</td>";
			vgpm_summary = vgpm_summary
					+ " <td class=\"ccab\" style=\"background-color:" + row[13]
					+ "\">" + row[8] + "</td>";
			vgpm_summary = vgpm_summary
					+ " <td class=\"ccab\" style=\"background-color:" + row[13]
					+ "\">" + row[9] + "</td>";
			vgpm_summary = vgpm_summary
					+ " <td class=\"ccac\" style=\"background-color:" + row[13]
					+ "\">" + row[11] + "</td>";
			vgpm_summary = vgpm_summary
					+ " <td class=\"ccac\" style=\"background-color:" + row[13]
					+ "\">" + row[12] + "</td>";
			vgpm_summary = vgpm_summary
					+ " <td class=\"ccaa\" style=\"background-color:" + row[13]
					+ "\">" + row[10] + "</td>";
			vgpm_summary = vgpm_summary + "</tr>\n";
		}
		request.setAttribute("vgpm_summary", vgpm_summary);
	}

	void setTitle(HttpServletRequest request) {
		String title = "";
		String column_header = "";
		if (this.pm) {
			title = "Project Manager Summary";
			column_header = "PM";
		} else {
			title = "Job Owner Summary";
			column_header = "Job Owner";
		}
		request.setAttribute("title", title);
		request.setAttribute("column_header", column_header);
	}

	void getInputParameters(HttpServletRequest request) {
		initializeParameters();

		String parameter = "";
		String parameter_value = "";
		for (int i = 0; i < this.inputParameterList.size(); i++) {
			parameter = (String) this.inputParameterList.get(i);
			parameter_value = cleanParameter(request.getParameter(parameter));

			this.inputParameterValue.put(parameter, parameter_value);
		}
		this.project = ((this.inputParameterValue.get("project") != null) && (!((String) this.inputParameterValue
				.get("project")).equals("")));
		this.team = ((this.inputParameterValue.get("team") != null) && (!((String) this.inputParameterValue
				.get("team")).equals("")));
		this.pm = ((this.inputParameterValue.get("pm") != null) && (!((String) this.inputParameterValue
				.get("pm")).equals("")));
		this.owner = ((this.inputParameterValue.get("owner") != null) && (!((String) this.inputParameterValue
				.get("owner")).equals("")));
	}

	void initializeParameters() {
		this.inputParameterList.add(0, "pm");
		this.inputParameterList.add(1, "owner");
		this.inputParameterList.add(2, "from_date");
		this.inputParameterList.add(3, "to_date");
		this.inputParameterList.add(4, "team");
		this.inputParameterList.add(5, "project");
	}

	String cleanParameter(String value) {
		if (value == null) {
			value = "";
		} else {
			value = value.trim();
		}
		return value;
	}

	void setInputParameters(HttpServletRequest request) {
		String parameter = "";
		String parameter_value = "";
		for (int i = 0; i < this.inputParameterList.size(); i++) {
			parameter = (String) this.inputParameterList.get(i);
			parameter_value = cleanParameter((String) this.inputParameterValue
					.get(parameter));

			request.setAttribute(parameter, parameter_value);
		}
	}

	String setMoneyFormat(Double value) {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		String sValue = "";
		if (value == null) {
			sValue = "No Data";
		} else {
			sValue = nf.format(value);
		}
		return sValue;
	}

	String setDecimalFormat(Double value) {
		NumberFormat nf = new DecimalFormat("0.00");
		String sValue = "";
		if (value == null) {
			sValue = "No Data";
		} else {
			sValue = nf.format(value);
		}
		return sValue;
	}

	String buildNavigation() {
		String navigation = "";

		return navigation;
	}

	String buildMonthYearSelector() {
		String sValue = "";

		return sValue;
	}
}
