package com.contingent.ilex.report;

import java.sql.ResultSet;

public class CostElement {
	String lx_ce_date;
	Double lx_ce_margin;
	Double lx_ce_total_price;
	Double lx_ce_unit_price;
	Double lx_ce_total_cost;
	Double lx_ce_unit_cost;

	CostElement(String used_in, String criteria) {
		String sql = "SELECT convert(varchar(12), lx_ce_date, 101) as lx_ce_date, lx_ce_margin,        lx_ce_invoice_price as lx_ce_total_price, lx_ce_unit_price, lx_ce_total_cost, lx_ce_unit_cost  FROM  lx_cost_element";

		sql = sql + " where lx_ce_used_in = " + used_in + " and lx_ce_type in "
				+ criteria;

		DataAccess da = new DataAccess();
		ResultSet rs = da.getResultSet(sql);
		try {
			rs.next();
			this.lx_ce_date = rs.getString("lx_ce_date");
			this.lx_ce_margin = Double.valueOf(rs.getDouble("lx_ce_margin"));
			this.lx_ce_total_price = Double.valueOf(rs
					.getDouble("lx_ce_total_price"));
			this.lx_ce_unit_price = Double.valueOf(rs
					.getDouble("lx_ce_unit_price"));
			this.lx_ce_total_cost = Double.valueOf(rs
					.getDouble("lx_ce_total_cost"));
			this.lx_ce_unit_cost = Double.valueOf(rs
					.getDouble("lx_ce_unit_cost"));
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	public static void main(String[] args) {
		CostElement x = new CostElement("834", "('P')");
		System.out.println("Price: " + x.lx_ce_total_price + " Cost: "
				+ x.lx_ce_total_cost);
	}
}
