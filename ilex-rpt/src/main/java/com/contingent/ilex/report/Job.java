package com.contingent.ilex.report;

import java.sql.ResultSet;

public class Job {
	String lm_js_id;
	String lm_js_pr_id;
	String lm_js_si_id;
	String lm_js_title;
	String lm_js_status;
	String lm_js_prj_status;
	String lm_js_changed_by;
	String lm_js_created_by;
	String lm_js_change_date;
	String lm_js_type;
	String lm_js_invoice_no;
	String lm_js_customer_reference;
	String first_visit_success;
	String lx_pr_type;
	String lm_si_name;
	String lm_si_number;
	String zip_code;
	String lm_tc_msp;
	String lm_tc_help_desk;
	Double authorized;
	Double approved;
	Double invoice_total;
	Double actual_cost;
	Double vgp;
	String reconciled;
	String invoiced_vs_approved;
	String lm_js_create_date;
	String lm_js_planned_start_date;
	String lm_js_planned_end_date;
	String start_date;
	String end_date;
	String lm_js_submitted_date;
	String lm_js_closed_date;
	String lm_js_associated_date;
	String latestPOIssueDate;
	String latestInvoiceDate;
	String overall_status;
	String overall_status_date;
	String allocation_month;
	String project_manager;
	String employee;
	String notes_missing;
	String customer_ref_missing;
	String site_missing;
	String inconsistent_job;
	CostElement financialData;
	String taskType;
	String revenueMonth;
	DataAccess da = new DataAccess();

	public Job(String job_id) {
		this.lm_js_id = job_id;
		if (jobInformation()) {
			getCostElement();
			costApprovedCheck();
		}
	}

	void getCostElement() {
		this.financialData = new CostElement(this.lm_js_id, "('J', 'IJ')");
		if (this.financialData.lx_ce_total_price.doubleValue() != 0.0D) {
			this.vgp = Double.valueOf(1.0D - this.actual_cost.doubleValue()
					/ this.financialData.lx_ce_total_price.doubleValue());
		} else {
			this.vgp = Double.valueOf(0.0D);
		}
		if (this.vgp.doubleValue() < -100.0D) {
			this.vgp = Double.valueOf(-99.900000000000006D);
		}
	}

	void costApprovedCheck() {
		this.invoiced_vs_approved = "N";
		if (this.overall_status.equals("Complete")) {
			if (this.authorized.doubleValue() > this.financialData.lx_ce_total_price
					.doubleValue()) {
				this.invoiced_vs_approved = "Y";
			}
			if (this.invoice_total.doubleValue() > this.approved.doubleValue()) {
				this.invoiced_vs_approved = "Y";
			}
			if ((this.vgp.doubleValue() < 0.2D)
					|| (this.vgp.doubleValue() > 0.8D)) {
				this.invoiced_vs_approved = "Y";
			}
		}
	}

	public boolean jobInformation() {
		String sql = "exec ac_lm_job_information_01 " + this.lm_js_id;

		boolean job_status = false;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			rs.next();
			this.lm_js_pr_id = rs.getString("lm_js_pr_id");
			this.lm_js_si_id = rs.getString("lm_js_si_id");
			this.lm_js_title = rs.getString("lm_js_title");
			this.lm_js_status = rs.getString("lm_js_status");
			this.lm_js_prj_status = rs.getString("lm_js_prj_status");
			this.lm_js_changed_by = rs.getString("lm_js_changed_by");
			this.lm_js_created_by = rs.getString("lm_js_created_by");
			this.lm_js_change_date = rs.getString("lm_js_change_date");
			this.lm_js_type = rs.getString("lm_js_type");
			this.lm_js_invoice_no = rs.getString("lm_js_invoice_no");
			this.lm_js_customer_reference = rs
					.getString("lm_js_customer_reference");
			this.first_visit_success = rs.getString("first_visit_success");

			this.lx_pr_type = rs.getString("lx_pr_type");
			this.lm_si_name = rs.getString("lm_si_name");
			this.lm_si_number = rs.getString("lm_si_number");

			this.zip_code = rs.getString("lm_si_zip_code");
			this.lm_tc_help_desk = rs.getString("lm_tc_help_desk");
			this.lm_tc_msp = rs.getString("lm_tc_msp");

			this.authorized = Double.valueOf(rs.getDouble("authorized"));
			this.approved = Double.valueOf(rs.getDouble("approved"));
			this.invoice_total = Double.valueOf(rs.getDouble("invoice_total"));
			this.actual_cost = Double.valueOf(rs.getDouble("actual_cost"));
			this.reconciled = rs.getString("reconciled");
			this.invoiced_vs_approved = rs.getString("invoiced_vs_approved");

			this.lm_js_create_date = rs.getString("lm_js_create_date");
			this.lm_js_planned_start_date = rs
					.getString("lm_js_planned_start_date");
			this.lm_js_planned_end_date = rs
					.getString("lm_js_planned_end_date");
			this.start_date = rs.getString("start_date");
			this.end_date = rs.getString("end_date");
			this.lm_js_submitted_date = rs.getString("lm_js_submitted_date");
			this.lm_js_closed_date = rs.getString("lm_js_closed_date");
			this.lm_js_associated_date = rs.getString("lm_js_associated_date");
			this.latestPOIssueDate = rs.getString("latestPOIssueDate");
			this.latestInvoiceDate = rs.getString("latestInvoiceDate");

			this.overall_status = rs.getString("overall_status");
			this.overall_status_date = rs.getString("overall_status_date");
			this.allocation_month = rs.getString("allocation_month");

			this.project_manager = rs.getString("project_manager");
			this.employee = rs.getString("employee");

			this.notes_missing = rs.getString("notes_missing");
			this.customer_ref_missing = rs.getString("customer_ref_missing");
			this.site_missing = rs.getString("site_missing");
			this.inconsistent_job = rs.getString("inconsistent_job");

			rs.close();
			job_status = true;
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
		return job_status;
	}

	public static void main(String[] args) {
		Job x = new Job("6667");
	}
}
