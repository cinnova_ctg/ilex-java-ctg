package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class Inventory extends DispatchAction {
	DataAccess da = new DataAccess();

	public ActionForward inventory(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = vendorSummary(request);

		return mapping.findForward(forward_key);
	}

	String vendorSummary(HttpServletRequest request) {
		String forward_key = "searchResults";

		getReportParameters(request);
		getReportData(formatSQL(), request);

		return forward_key;
	}

	void getReportParameters(HttpServletRequest request) {
	}

	String formatRequestParameter(String parameter) {
		if (parameter == null) {
			parameter = "";
		}
		return parameter;
	}

	String formatSQL() {
		String sql_command = "select top 30 lo_om_division, sum(lm_po_authorised_total) from lm_purchase_order join cp_partner_search_list_01 on lm_po_partner_id = cp_partner_id where lm_po_authorised_total is not null and lm_po_authorised_total > 0.0 group by lo_om_division order by sum(lm_po_authorised_total) desc";

		return sql_command;
	}

	void getReportData(String sql, HttpServletRequest request) {
		Vector rows = new Vector();
		int i = 0;
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[2];
				row[0] = rs.getString(1);
				row[1] = formatCurrencyValues(Double.valueOf(rs.getDouble(2)));
				rows.add(i++, row);
			}
			rs.close();
			request.setAttribute("rows", rows);
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	String formatCurrencyValues(Double value) {
		NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
		String formattedValue = nf.format(value);

		return formattedValue;
	}

	String formatPercentageValues(Double value) {
		NumberFormat nf = NumberFormat.getPercentInstance(Locale.US);
		String formattedValue = nf.format(value);

		return formattedValue;
	}

	String formatNumericValues(Double value) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		String formattedValue = nf.format(value);

		return formattedValue;
	}
}
