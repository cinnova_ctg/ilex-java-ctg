package com.contingent.ilex.report;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DWSqlBean2 {
	private static DataSource ds = null;
	private static Connection conn = null;

	public DWSqlBean2() {
		try {
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");

			ds = (DataSource) envCtx.lookup("jdbc/ContingentDataWH");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static Connection getConnection() throws SQLException {
		try {
			if (conn == null) {
				conn = ds.getConnection();
			}
			return conn;
		} catch (SQLException s) {
			System.out
					.println("SQLException thrown in DWSqlBean.getConnection()");
			s.printStackTrace();
			throw s;
		}
	}

	public void debugConnection() {
		Connection conn = null;

		Properties props = System.getProperties();

		StringBuffer path = new StringBuffer("\t"
				+ props.getProperty("java.library.path"));
		int idx = 0;
		while (idx < path.length()) {
			idx = path.toString().indexOf(";", idx);
			if (idx == -1) {
				break;
			}
			path.replace(idx++, idx, "\n\t");
			idx++;
		}
		StringBuffer classpath = new StringBuffer("\t"
				+ props.getProperty("java.class.path"));
		int idx2 = 0;
		while (idx2 < classpath.length()) {
			idx2 = classpath.toString().indexOf(";", idx2);
			if (idx2 == -1) {
				break;
			}
			classpath.replace(idx2++, idx2, "\n\t");
			idx2++;
		}
		System.out.println("java runtime Version: \t"
				+ props.getProperty("java.runtime.version") + "\n");
		System.out.println("\n=============");
		System.out.println("java vm Version: \t"
				+ props.getProperty("java.vm.version") + "\n");
		System.out.println("\n=============");
		System.out.println("java vm name: \t"
				+ props.getProperty("java.vm.name") + "\n");
		System.out.println("\n=============");
		System.out.println("java library path: \n" + path + "\n");
		System.out.println("\n=============");
		System.out.println("java classpath: " + classpath + "\n");
		try {
			conn = getConnection();

			DatabaseMetaData meta = conn.getMetaData();
			System.out.println("\n=============\nDatabase Product Name is ... "
					+ meta.getDatabaseProductName());
			System.out.println("\nDatabase Product Version is "
					+ meta.getDatabaseProductVersion());
			System.out.println("\n=============\nJDBC Driver Name is ........ "
					+ meta.getDriverName());
			System.out.println("\nJDBC Driver Version is ..... "
					+ meta.getDriverVersion());
			System.out.println("\nJDBC URL " + meta.getURL());
			System.out.println("\n=============");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Integer> getBusDevMgrIDs() {
		ArrayList<Integer> ids = new ArrayList();
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();

			String selectSQL = "SELECT DISTINCT BusinessDevelopmentMgrID FROM FactJobRevenueActualization";
			stmt = conn.prepareStatement(selectSQL);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				ids.add(new Integer(rs.getInt("BusinessDevelopmentMgrID")));
			}
		} catch (SQLException s) {
			System.out
					.println("SQLException thrown in DWSqlBean.getBusDevMgrIDs()");
			s.printStackTrace();
		} catch (Exception e) {
			System.out
					.println("Exception thrown in DWSqlBean.getBusDevMgrIDs()");
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				System.out
						.println("SQLException thrown in DWSqlBean.getBusDevMgrIDs() while closing stmt");
				s.printStackTrace();
			}
		}
		return ids;
	}

	public ArrayList<Integer> getSnrProjMgrIDs() {
		ArrayList<Integer> ids = new ArrayList();
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();

			String selectSQL = "SELECT DISTINCT SeniorProjectMgrID FROM FactJobRevenueActualization";
			stmt = conn.prepareStatement(selectSQL);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				ids.add(new Integer(rs.getInt("SeniorProjectMgrID")));
			}
		} catch (SQLException s) {
			System.out
					.println("SQLException thrown in DWSqlBean.getSnrProjMgrIDs()");
			s.printStackTrace();
		} catch (Exception e) {
			System.out
					.println("Exception thrown in DWSqlBean.getSnrProjMgrIDs()");
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				System.out
						.println("SQLException thrown in DWSqlBean.getSnrProjMgrIDs() while closing stmt");
				s.printStackTrace();
			}
		}
		return ids;
	}

	public ArrayList<Integer> getJobOwnerIDs() {
		ArrayList<Integer> ids = new ArrayList();
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();

			String selectSQL = "SELECT DISTINCT JobOwnerID FROM FactJobRevenueActualization";
			stmt = conn.prepareStatement(selectSQL);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				ids.add(new Integer(rs.getInt("JobOwnerID")));
			}
		} catch (SQLException s) {
			System.out
					.println("SQLException thrown in DWSqlBean.getJobOwnerIDs()");
			s.printStackTrace();
		} catch (Exception e) {
			System.out
					.println("Exception thrown in DWSqlBean.getJobOwnerIDs()");
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				System.out
						.println("SQLException thrown in DWSqlBean.getJobOwnerIDs() while closing stmt");
				s.printStackTrace();
			}
		}
		return ids;
	}

	public String getFullNameFromId(int id, int style) {
		String name = "";
		String last = "";
		String first = "";
		int cntr = 0;
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();

			String selectSQL = "SELECT EmployeeFirstName,EmployeeLastName FROM DimEmployee WHERE EmployeeID=?";
			stmt = conn.prepareStatement(selectSQL);
			stmt.setString(1, Integer.toString(id));
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				first = rs.getString("EmployeeFirstName");
				last = rs.getString("EmployeeLastName");
				cntr++;
			}
			if (cntr < 1) {
			}
			// for (;;)
			// {
			// return null;
			// ResultSet rs;
			// String selectSQL;
			// switch (style)
			// {
			// case 0:
			// name = last + ", " + first;
			// break;
			// case 1:
			// name = first + " " + last;
			// break;
			// default:
			// System.out.println("Error! Invalid style " + style +
			// " value passed into getFullNameFromID()");
			// }
			// }
		} catch (SQLException s) {
			System.out
					.println("SQLException thrown in DWSqlBean.getFullNameFromID()");
			s.printStackTrace();
		} catch (Exception e) {
			System.out
					.println("Exception thrown in DWSqlBean.getFullNameFromID()");
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				System.out
						.println("SQLException thrown in DWSqlBean.getFullNameFromID() while closing stmt");
				s.printStackTrace();
			}
		}
		return name;
	}

	public static int getIdFromFullName(String name, int style) {
		int id = -1;
		StringTokenizer st = null;
		String first = "";
		String last = "";
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			switch (style) {
			case 0:
				st = new StringTokenizer(name, ", ");
				while (st.hasMoreTokens()) {
					last = st.nextToken();
					first = st.nextToken();
				}
				break;
			case 1:
				st = new StringTokenizer(name, " ");
				while (st.hasMoreTokens()) {
					first = st.nextToken();
					last = st.nextToken();
				}
				break;
			default:
				System.out.println("Error! Invalid style " + style
						+ " value passed into getIdFromFullName()");
				int i = id;
				return i;
			}
			String selectSQL = "SELECT DISTINCT EmployeeID FROM DimEmployee WHERE EmployeeFirstName=? AND EmployeeLastName=?";
			stmt = conn.prepareStatement(selectSQL);
			stmt.setString(1, first);
			stmt.setString(2, last);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				id = rs.getInt("EmployeeID");
			}
		} catch (SQLException s) {
			System.out
					.println("SQLException thrown in DWSqlBean.getIdFromFullName()");
			s.printStackTrace();
		} catch (Exception e) {
			System.out
					.println("Exception thrown in DWSqlBean.getIdFromFullName()");
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				System.out
						.println("SQLException thrown in DWSqlBean.getIdFromFullName() while closing stmt");
				s.printStackTrace();
			}
		}
		return id;
	}

	public ArrayList<String> getFullNamesForIdList(int idtype, int style,
			String selectAll) {
		ArrayList<String> names = new ArrayList();
		ArrayList<Integer> ids = null;
		switch (idtype) {
		case 1:
			ids = getSnrProjMgrIDs();
			break;
		case 2:
			ids = getBusDevMgrIDs();
			break;
		case 3:
			ids = getJobOwnerIDs();
			break;
		default:
			System.out.println("Unknown idtype '" + idtype
					+ "' passed to getFullNamesForIdList()");
		}
		for (Iterator<Integer> it = ids.iterator(); it.hasNext();) {
			int id = ((Integer) it.next()).intValue();
			String name = getFullNameFromId(id, style);
			if ((name != null) && (!name.equals(""))) {
				names.add(name);
			}
		}
		Collections.sort(names);
		if ((selectAll != null) && (!selectAll.equals(""))) {
			names.add(0, selectAll);
		}
		return names;
	}

	public ArrayList<String> getProjectTypes(String selectAll) {
		ArrayList<String> types = new ArrayList();
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();

			String selectSQL = "SELECT DISTINCT ProjectType FROM DimProject ORDER BY ProjectType";
			stmt = conn.prepareStatement(selectSQL);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				types.add(rs.getString("ProjectType"));
			}
			if ((selectAll != null) && (!selectAll.equals(""))) {
				types.add(0, selectAll);
			}
		} catch (SQLException s) {
			System.out
					.println("SQLException thrown in DWSqlBean.getProjectTypes()");
			s.printStackTrace();
		} catch (Exception e) {
			System.out
					.println("Exception thrown in DWSqlBean.getProjectTypes()");
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				System.out
						.println("SQLException thrown in DWSqlBean.getProjectTypes() while closing stmt");
				s.printStackTrace();
			}
		}
		return types;
	}

	public ArrayList<String> getClients(String selectAll) {
		ArrayList<String> clients = new ArrayList();
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();

			String selectSQL = "SELECT DISTINCT ClientName FROM DimProject ORDER BY ClientName";
			stmt = conn.prepareStatement(selectSQL);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				clients.add(rs.getString("ClientName"));
			}
			if ((selectAll != null) && (!selectAll.equals(""))) {
				clients.add(0, selectAll);
			}
		} catch (SQLException s) {
			System.out.println("SQLException thrown in DWSqlBean.getClients()");
			s.printStackTrace();
		} catch (Exception e) {
			System.out.println("Exception thrown in DWSqlBean.getClients()");
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				System.out
						.println("SQLException thrown in DWSqlBean.getClients() while closing stmt");
				s.printStackTrace();
			}
		}
		return clients;
	}

	public ArrayList<String> getMonthYearDates(String selectAll) {
		throw new Error(
				"Unresolved compilation problem: \n\tRevRptConst.MONTHS cannot be resolved\n");
	}

	public static RevenueChartData getChartData(int date, String dtype,
			String spman, String bdman, String jowner, String client,
			String ptype) {
		throw new Error(
				"Unresolved compilation problems: \n\tRevRptConst.dALL cannot be resolved\n\tRevRptConst.dPROFORMA cannot be resolved\n\tRevRptConst.dPROFORMADATE cannot be resolved\n");
	}
}
