package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class DispatchAnalysis extends DispatchAction {
	DataAccess da = new DataAccess();
	String PageLevel = "1";

	public ActionForward dd_real_time(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "dd_real_time";

		Vector cca_list = initializeCCA();

		getTodaysMetrics(request, cca_list);

		getAvgMetrics(request);

		return mapping.findForward(forward_key);
	}

	void getTodaysMetrics(HttpServletRequest request, Vector cca_list) {
		Map x = new HashMap();
		Map y = null;
		Map z = new HashMap();

		String current_cca = "";
		String previous_cca = "";

		String current_id = "";
		String previous_id = "";

		String sql = "select ap_ds_owner_fname, ap_ds_pc_id, ap_ds_key, ap_ds_subkey, ap_ds_value from ap_dispatch_summaries where ap_ds_key = 'Today' order by ap_ds_owner_fname, ap_ds_key, ap_ds_subkey";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				current_cca = rs.getString(1);
				current_id = rs.getString(2);
				if (!current_cca.equals(previous_cca)) {
					if (y != null) {
						z.put(previous_cca, previous_id);
						x.put(previous_cca, y);
					}
					y = new HashMap();
					previous_cca = current_cca;
					previous_id = current_id;
				}
				y.put(rs.getString("ap_ds_subkey"), rs.getString("ap_ds_value"));
			}
			x.put(previous_cca, y);
			z.put(previous_cca, previous_id);

			request.setAttribute("today", x);
			request.setAttribute("lo_pc_id", z);

			request.setAttribute("cca_list", cca_list);

			Map na = (Map) x.get("Not Assigned");
			if (na != null) {
				request.setAttribute("not_assigned", na.get("To be Scheduled"));
			} else {
				request.setAttribute("not_assigned", "0");
			}
			rs.close();
		} catch (Exception e) {
			System.out.println("DispatchAnalysis.getTodaysMetrics: " + e
					+ " - with - " + sql);
		}
	}

	void getAvgMetrics(HttpServletRequest request) {
		Map x = new HashMap();
		Map y = null;

		String current_cca = "";
		String previous_cca = "";

		String sql = "select ap_ds_owner_fname, ap_ds_key, ap_ds_subkey, ap_ds_value from ap_dispatch_summaries where ap_ds_key = 'Average' and ap_ds_owner_fname != 'Not Assigned' order by ap_ds_owner_fname, ap_ds_key, ap_ds_subkey";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				current_cca = rs.getString("ap_ds_owner_fname");
				if (!current_cca.equals(previous_cca)) {
					if (y != null) {
						x.put(previous_cca, y);
					}
					y = new HashMap();
					previous_cca = current_cca;
				}
				y.put(rs.getString("ap_ds_subkey"), rs.getString("ap_ds_value"));
			}
			x.put(previous_cca, y);
			request.setAttribute("avg", x);

			rs.close();
		} catch (Exception e) {
			System.out.println("DispatchAnalysis.getAvgMetrics: " + e
					+ " - with - " + sql);
		}
	}

	public ActionForward dd_vgpm(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "dd_vgpm";

		Vector cca_list = initializeCCA();

		getAvgVGP(request, cca_list);

		return mapping.findForward(forward_key);
	}

	void getAvgVGP(HttpServletRequest request, Vector cca_list) {
		Map x = new HashMap();
		Map y = null;

		String current_cca = "";
		String previous_cca = "";

		String sql = "select ap_ds_owner_fname, ap_ds_key, ap_ds_subkey, ap_ds_value from ap_dispatch_summaries where ap_ds_key = 'Average VGP' and ap_ds_owner_fname != 'Not Assigned' order by ap_ds_owner_fname, ap_ds_key, ap_ds_subkey";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				current_cca = rs.getString("ap_ds_owner_fname");
				if (!current_cca.equals(previous_cca)) {
					if (y != null) {
						x.put(previous_cca, y);
					}
					y = new HashMap();
					previous_cca = current_cca;
				}
				y.put(rs.getString("ap_ds_subkey"), rs.getString("ap_ds_value"));
			}
			x.put(previous_cca, y);
			request.setAttribute("vgpm", x);
			request.setAttribute("cca_list", cca_list);
			rs.close();
		} catch (Exception e) {
			System.out.println("DispatchAnalysis.getAvgVGP: " + e
					+ " - with - " + sql);
		}
	}

	public ActionForward dd_vcogs(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "dd_vcogs";

		Vector cca_list = initializeCCA();

		getVCOGS(request, cca_list);

		return mapping.findForward(forward_key);
	}

	void getVCOGS(HttpServletRequest request, Vector cca_list) {
		Map x = new HashMap();
		Map y = null;

		String current_cca = "";
		String previous_cca = "";

		String sql = "select ap_ds_owner_fname, ap_ds_key, ap_ds_subkey, ap_ds_value from ap_dispatch_summaries where ap_ds_key = '700100067' and ap_ds_owner_fname != 'Not Assigned' order by ap_ds_owner_fname, ap_ds_key, ap_ds_subkey";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				current_cca = rs.getString("ap_ds_owner_fname");
				if (!current_cca.equals(previous_cca)) {
					if (y != null) {
						x.put(previous_cca, y);
					}
					y = new HashMap();
					previous_cca = current_cca;
				}
				y.put(rs.getString("ap_ds_subkey"), rs.getString("ap_ds_value"));
			}
			x.put(previous_cca, y);
			request.setAttribute("vcogs", x);
			request.setAttribute("cca_list", cca_list);
			rs.close();
		} catch (Exception e) {
			System.out.println("DispatchAnalysis.getVCOGS: " + e + " - with - "
					+ sql);
		}
	}

	Vector initializeCCA() {
		Vector cca_list = new Vector();
		cca_list.add(0, "Sacha");

		cca_list.add(1, "Diana");
		cca_list.add(2, "Jared");

		cca_list.add(3, "Brian");

		cca_list.add(4, "Alex");
		cca_list.add(5, "Derek");
		cca_list.add(6, "Daneka");
		cca_list.add(7, "Danika");
		cca_list.add(8, "Tracy");

		return cca_list;
	}
}
