package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class MSReports extends DispatchAction {
	DataAccess da = new DataAccess();
	SimpleDateFormat MMM_YY = new SimpleDateFormat("MMM y");

	public ActionForward monthly_by_user(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "monthly_by_user";

		String categories = "<categories>";
		String personnel = "<dataset seriesName='Personnel'>";
		String incidents = "<dataset seriesName='Incidents (Hundreds)'>";
		String installs = "<dataset seriesName='Wug Adds (Hundreds)'>";
		String composite = "<dataset seriesName='Composite'>";

		Vector<String> months = buildTimeFrame();
		categories = "<categories>";
		for (int i = 0; i < months.size(); i++) {
			categories = categories + " <category Label='"
					+ (String) months.get(i) + "'/>";
		}
		categories = categories + "</categories>";
		Map<String, String[]> report_date = getReportData_Workload();
		for (int i = 0; i < months.size(); i++) {
			if (report_date.containsKey(months.get(i))) {
				String[] row = (String[]) report_date.get(months.get(i));
				personnel = personnel + " <set value='" + row[0] + "' />";
				incidents = incidents + " <set value='" + row[1] + "' />";
				installs = installs + " <set value='" + row[2] + "' />";
				composite = composite + " <set value='" + row[3] + "' />";
			} else {
				personnel = personnel + " <set value='' />";
				incidents = incidents + " <set value='' />";
				installs = installs + " <set value='' />";
				composite = composite + " <set value='' />";
			}
		}
		personnel = personnel + "</dataset>";
		incidents = incidents + "</dataset>";
		installs = installs + "</dataset>";
		composite = composite + "</dataset>";
		request.setAttribute("chart_data", categories + personnel + incidents
				+ installs + composite);
		System.out.println(categories + personnel + incidents + installs
				+ composite);
		return mapping.findForward(forward_key);
	}

	public ActionForward weekly_ave_outage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "weekly_ave_outage";

		String categories = "<categories>";
		String personnel = "<dataset seriesName='Personnel'>";
		String outavedowntime = "<dataset seriesName='Outage Average Downtime (Minutes/100)'>";
		Vector<String[]> report_date = getReportData_Outage();
		for (int i = 0; i < report_date.size(); i++) {
			String[] row = (String[]) report_date.get(i);
			categories = categories + " <category Label='" + row[0] + "'/>";
			personnel = personnel + " <set value='" + row[3] + "' />";
			outavedowntime = outavedowntime + " <set value='" + row[2] + "' />";
		}
		categories = categories + "</categories>";
		personnel = personnel + "</dataset>";
		outavedowntime = outavedowntime + "</dataset>";
		request.setAttribute("chart_data", categories + personnel
				+ outavedowntime);
		System.out.println(categories + personnel + outavedowntime);
		return mapping.findForward(forward_key);
	}

	public ActionForward weekly_outage_percent(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "weekly_outage_percent";
		String categories = "<categories>";
		String personnel = "<dataset seriesName='Personnel'>";
		String outpercent = "<dataset seriesName='Outage Percentage'>";
		Vector<String[]> report_date = getReportData_Outage();
		for (int i = 0; i < report_date.size(); i++) {
			String[] row = (String[]) report_date.get(i);
			categories = categories + " <category Label='" + row[0] + "'/>";
			personnel = personnel + " <set value='" + row[3] + "' />";
			outpercent = outpercent + " <set value='" + row[1] + "' />";
		}
		categories = categories + "</categories>";
		personnel = personnel + "</dataset>";
		outpercent = outpercent + "</dataset>";
		request.setAttribute("chart_data", categories + personnel + outpercent);
		System.out.println(categories + personnel + outpercent);
		return mapping.findForward(forward_key);
	}

	Map<String, String[]> getReportData_Workload() {
		Map<String, String[]> rows = new HashMap();
		String sql = "exec datawarehouse.dbo.ac_report_help_desk_workload_analysis";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[4];

				row[0] = rs.getString("head_cnt");
				row[1] = rs.getString("incident_cnt");
				row[2] = rs.getString("install_cnt");
				row[3] = rs.getString("composite");

				rows.put(getDisplayDate(rs.getString("yyyymm")), row);
			}
			rs.close();
		} catch (Exception e) {
			System.out.print(e);
		}
		return rows;
	}

	private String Round(String string, int i) {
		return null;
	}

	Vector<String[]> getReportData_Outage() {
		Vector<String[]> rows = new Vector();
		String sql = "exec datawarehouse.dbo.ac_report_help_desk_outages_01";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[4];
				row[0] = rs.getString("week_date");
				row[1] = rs.getString("avg_outage_cnt");
				row[2] = rs.getString("avg_outage_duration_min");
				row[3] = rs.getString("head_cnt");
				rows.add(row);
			}
			rs.close();
		} catch (Exception e) {
			System.out.print(e);
		}
		return rows;
	}

	String getDisplayDate(String YYYYMM) {
		int yyyy = Integer.valueOf(YYYYMM.substring(0, 4)).intValue();
		int mm = Integer.valueOf(YYYYMM.substring(4, 6)).intValue() - 1;
		GregorianCalendar d1 = new GregorianCalendar(yyyy, mm, 1);
		YYYYMM = this.MMM_YY.format(d1.getTime());
		return YYYYMM;
	}

	Vector<String> buildTimeFrame() {
		Calendar tday = new GregorianCalendar();
		tday.add(2, -11);
		Vector<String> Months = new Vector();
		for (int i = 0; i < 12; i++) {
			Months.add(this.MMM_YY.format(tday.getTime()));
			tday.add(2, 1);
		}
		return Months;
	}
}
