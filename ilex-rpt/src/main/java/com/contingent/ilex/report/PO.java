package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class PO {
	String lm_po_id;
	String lm_js_id;
	String POTypeID;
	String POIssueDate;
	Double ActualCost;
	Double EstimatedCost = Double.valueOf(0.0D);
	String lo_pc_id = "";
	public Vector Resources = new Vector();
	Map ResourceUnitCost = new HashMap();
	Map ResourceQty = new HashMap();
	Map ResourceTotalCost = new HashMap();
	DataAccess da;

	PO(String po_id, String po_type, String js_id, String pc_id, DataAccess d_a) {
		this.lm_po_id = po_id;
		this.POTypeID = po_type;
		this.lm_js_id = js_id;
		this.lo_pc_id = pc_id;
		this.da = d_a;
	}

	void processPO() {
		if (this.POTypeID.equals("1")) {
			getPODetailsHS();
		} else if (this.POTypeID.equals("2")) {
			if (getPODetailsFP()) {
				computeEstimatedFPCost();
				assignAuthorizedEstimate();
			}
		}
	}

	boolean getPODetailsFP() {
		boolean rtn_code = true;

		String sql = "exec ap_get_FP_purchase_order_details_01 "
				+ this.lm_po_id;
		int i = 0;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				if (i == 0) {
					this.POIssueDate = rs.getString("lm_po_issue_date");
					this.ActualCost = Double.valueOf(rs
							.getDouble("lm_po_authorised_total"));
					i++;
				}
				addResourceData(rs.getString("lm_rs_iv_cns_part_number"),
						Double.valueOf(rs.getDouble("lm_rs_ce_cost")),
						Double.valueOf(rs.getDouble("lm_rs_quantity")));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
		return rtn_code;
	}

	boolean getPODetailsHS() {
		boolean rtn_code = true;

		String sql = "exec ap_get_HS_purchase_order_details_01 "
				+ this.lm_po_id;
		int i = 0;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				if (i == 0) {
					this.POIssueDate = rs.getString("lm_po_issue_date");
					this.ActualCost = Double.valueOf(rs
							.getDouble("lm_po_authorised_total"));
					i++;
				}
				addResourceDataHS(rs.getString("lm_rs_iv_cns_part_number"),
						Double.valueOf(rs
								.getDouble("lm_pwo_authorised_unit_cost")),
						Double.valueOf(rs.getDouble("lm_rs_quantity")));
			}
			rs.close();
			for (int j = 0; j < this.Resources.size(); j++) {
				String r = (String) this.Resources.get(j);
				Double tc = (Double) this.ResourceTotalCost.get(r);
				Double qt = (Double) this.ResourceQty.get(r);
				if ((tc.doubleValue() > 0.0D) && (qt.doubleValue() > 0.0D)) {
					this.ResourceUnitCost
							.put(r,
									Double.valueOf(tc.doubleValue()
											/ qt.doubleValue()));
				}
			}
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
			rtn_code = false;
		}
		return rtn_code;
	}

	public void addResourceData(String rs, Double cost, Double qty) {
		if (this.Resources.contains(rs)) {
			Double t_qty = Double.valueOf(((Double) this.ResourceQty.get(rs))
					.doubleValue() + qty.doubleValue());
			this.ResourceQty.put(rs, t_qty);
		} else {
			this.Resources.add(rs);
			this.ResourceUnitCost.put(rs, cost);
			this.ResourceQty.put(rs, qty);
		}
	}

	public void addResourceDataHS(String rs, Double cost, Double qty) {
		Double t_tc = Double.valueOf(cost.doubleValue() * qty.doubleValue());
		if (this.Resources.contains(rs)) {
			Double t_qty = Double.valueOf(((Double) this.ResourceQty.get(rs))
					.doubleValue() + qty.doubleValue());
			this.ResourceQty.put(rs, t_qty);

			t_tc = Double.valueOf(((Double) this.ResourceTotalCost.get(rs))
					.doubleValue() + t_tc.doubleValue());
			this.ResourceTotalCost.put(rs, t_tc);
		} else {
			this.Resources.add(rs);
			this.ResourceTotalCost.put(rs, t_tc);
			this.ResourceQty.put(rs, qty);
		}
	}

	public void computeEstimatedFPCost() {
		String key = null;
		for (int i = 0; i < this.Resources.size(); i++) {
			key = (String) this.Resources.get(i);
			double res_unit_cost = ((Double) this.ResourceUnitCost.get(key))
					.doubleValue();
			double res_qty = ((Double) this.ResourceQty.get(key)).doubleValue();
			this.EstimatedCost = Double.valueOf(this.EstimatedCost
					.doubleValue() + res_qty * res_unit_cost);
		}
	}

	public void assignAuthorizedEstimate() {
		String resource = null;
		for (int i = 0; i < this.Resources.size(); i++) {
			resource = (String) this.Resources.get(i);

			double res_unit_cost = ((Double) this.ResourceUnitCost
					.get(resource)).doubleValue();
			double res_qty = ((Double) this.ResourceQty.get(resource))
					.doubleValue();
			double res_total_cost = res_unit_cost * res_qty;
			if ((res_qty > 0.0D) && (res_unit_cost != 0.0D)) {
				double res_weight = res_total_cost
						/ this.EstimatedCost.doubleValue();
				double res_auth_total_cost = res_weight
						* this.ActualCost.doubleValue();
				double res_auth_unit_cost = res_auth_total_cost / res_qty;
				this.ResourceUnitCost.put(resource,
						Double.valueOf(res_auth_unit_cost));
			} else {
				this.ResourceUnitCost.put(resource, Double.valueOf(0.0D));
			}
		}
	}

	public void saveResourceDetails(Vector resource_list) {
		String resource = null;
		String sql = "";

		int j = 0;
		for (int i = 0; i < this.Resources.size(); i++) {
			resource = (String) this.Resources.get(i);
			if (resource_list.contains(resource)) {
				sql = "exec ap_populate_resource_analysis_tables ";

				Double unit_cost = (Double) this.ResourceUnitCost.get(resource);
				Double quantity = (Double) this.ResourceQty.get(resource);
				if ((unit_cost != null)
						&& (!unit_cost.equals(Double.valueOf(0.0D)))
						&& (!unit_cost.isNaN())) {
					sql = sql + this.lm_po_id + "," + "'" + this.POTypeID
							+ "'," + "'" + this.POIssueDate + "'," + "'"
							+ resource + "'," + quantity + "," + unit_cost
							+ "," + this.lm_js_id;

					this.da.executeQuery(sql);
				}
			}
		}
	}
}
