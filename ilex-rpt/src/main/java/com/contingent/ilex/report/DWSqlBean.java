package com.contingent.ilex.report;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.contingent.ilex.utility.ReportUtils;
import com.contingent.ilex.utility.RevRptConst;

public class DWSqlBean {
	private static DataSource ds = null;
	private static Connection conn = null;
	private ArrayList<String> _years = new ArrayList();

	public DWSqlBean() {
		try {
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");

			ds = (DataSource) envCtx.lookup("jdbc/ContingentDataWH");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static Connection getConnection() throws SQLException {
		try {
			if (conn == null) {
				conn = ds.getConnection();
			}
			return conn;
		} catch (SQLException s) {
			System.out
					.println("SQLException thrown in DWSqlBean.getConnection()");
			s.printStackTrace();
			throw s;
		}
	}

	public void debugConnection() {
		Connection conn = null;

		Properties props = System.getProperties();

		StringBuffer path = new StringBuffer("\t"
				+ props.getProperty("java.library.path"));
		int idx = 0;
		while (idx < path.length()) {
			idx = path.toString().indexOf(";", idx);
			if (idx == -1) {
				break;
			}
			path.replace(idx++, idx, "\n\t");
			idx++;
		}
		StringBuffer classpath = new StringBuffer("\t"
				+ props.getProperty("java.class.path"));
		int idx2 = 0;
		while (idx2 < classpath.length()) {
			idx2 = classpath.toString().indexOf(";", idx2);
			if (idx2 == -1) {
				break;
			}
			classpath.replace(idx2++, idx2, "\n\t");
			idx2++;
		}
		System.out.println("java runtime Version: \t"
				+ props.getProperty("java.runtime.version") + "\n");
		System.out.println("\n=============");
		System.out.println("java vm Version: \t"
				+ props.getProperty("java.vm.version") + "\n");
		System.out.println("\n=============");
		System.out.println("java vm name: \t"
				+ props.getProperty("java.vm.name") + "\n");
		System.out.println("\n=============");
		System.out.println("java library path: \n" + path + "\n");
		System.out.println("\n=============");
		System.out.println("java classpath: " + classpath + "\n");
		try {
			conn = getConnection();

			DatabaseMetaData meta = conn.getMetaData();
			System.out.println("\n=============\nDatabase Product Name is ... "
					+ meta.getDatabaseProductName());
			System.out.println("\nDatabase Product Version is "
					+ meta.getDatabaseProductVersion());
			System.out.println("\n=============\nJDBC Driver Name is ........ "
					+ meta.getDriverName());
			System.out.println("\nJDBC Driver Version is ..... "
					+ meta.getDriverVersion());
			System.out.println("\nJDBC URL " + meta.getURL());
			System.out.println("\n=============");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static int getIdFromFullName(String name, int style) {
		int id = -1;
		StringTokenizer st = null;
		String first = "";
		String last = "";
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			switch (style) {
			case 0:
				st = new StringTokenizer(name, ", ");
				while (st.hasMoreTokens()) {
					last = st.nextToken();
					first = st.nextToken();
				}
				break;
			case 1:
				st = new StringTokenizer(name, " ");
				while (st.hasMoreTokens()) {
					first = st.nextToken();
					last = st.nextToken();
				}
				break;
			default:
				System.out.println("Error! Invalid style " + style
						+ " value passed into getIdFromFullName()");
				int i = id;
				return i;
			}
			String selectSQL = "SELECT DISTINCT EmployeeID FROM DimEmployee WHERE EmployeeFirstName=? AND EmployeeLastName=?";
			stmt = conn.prepareStatement(selectSQL);
			stmt.setString(1, first);
			stmt.setString(2, last);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				id = rs.getInt("EmployeeID");
			}
		} catch (SQLException s) {
			System.out
					.println("SQLException thrown in DWSqlBean.getIdFromFullName()");
			s.printStackTrace();
		} catch (Exception e) {
			System.out
					.println("Exception thrown in DWSqlBean.getIdFromFullName()");
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				System.out
						.println("SQLException thrown in DWSqlBean.getIdFromFullName() while closing stmt");
				s.printStackTrace();
			}
		}
		return id;
	}

	public ArrayList<String> getFullNamesForIdList(int idtype, int style,
			String selectAll) {
		ArrayList<String> names = new ArrayList();
		PreparedStatement stmt = null;
		Connection conn = null;
		StringBuffer sb = new StringBuffer();
		String idType = "";
		String first = "";
		String last = "";
		int idNum = 0;
		try {
			conn = getConnection();
			switch (idtype) {
			case 1:
				idType = "SeniorProjectMgrID";
				break;
			case 2:
				idType = "BusinessDevelopmentMgrID";
				break;
			case 3:
				idType = "JobOwnerID";
				break;
			default:
				System.out.println("Unknown idtype '" + idtype
						+ "' passed to getFullNamesForIdList()");
				label125: return null;
			}
			sb.append("SELECT DISTINCT fj." + idType
					+ ", de.EmployeeLastName, de.EmployeeFirstName FROM "
					+ "FactJobRevenueActualization" + " fj INNER JOIN "
					+ "DimEmployee" + " de ON fj." + idType
					+ " = de.EmployeeID ORDER BY de.EmployeeLastName");

			stmt = conn.prepareStatement(sb.toString());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				idNum = rs.getInt(1);
				last = rs.getString(2);
				first = rs.getString(3);
				if ((last.equals(null)) || (last.equals(""))) {
					System.out.println("Error!  ID " + idNum
							+ " matched a null last name");
				}
				switch (style) {
				case 0:
					names.add(last + ", " + first);
					break;
				case 1:
					names.add(first + " " + last);
					break;
				default:
					System.out.println("Error! Invalid style " + style
							+ " value passed into getFullNameFromID()");
					break;
				}
			}
			if ((selectAll != null) && (!selectAll.equals(""))) {
				names.add(0, selectAll);
			}
		} catch (SQLException s) {
			System.out
					.println("SQLException thrown in DWSqlBean.getIdFromFullName()");
			s.printStackTrace();
		} catch (Exception e) {
			System.out
					.println("Exception thrown in DWSqlBean.getIdFromFullName()");
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				System.out
						.println("SQLException thrown in DWSqlBean.getIdFromFullName() while closing stmt");
				s.printStackTrace();
			}
		}
		return names;
	}

	public ArrayList<String> getProjectTypes(String selectAll) {
		ArrayList<String> types = new ArrayList();
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();

			String selectSQL = "SELECT DISTINCT ProjectType FROM DimProject ORDER BY ProjectType";
			stmt = conn.prepareStatement(selectSQL);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				types.add(rs.getString("ProjectType"));
			}
			if ((selectAll != null) && (!selectAll.equals(""))) {
				types.add(0, selectAll);
			}
		} catch (SQLException s) {
			System.out
					.println("SQLException thrown in DWSqlBean.getProjectTypes()");
			s.printStackTrace();
		} catch (Exception e) {
			System.out
					.println("Exception thrown in DWSqlBean.getProjectTypes()");
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				System.out
						.println("SQLException thrown in DWSqlBean.getProjectTypes() while closing stmt");
				s.printStackTrace();
			}
		}
		return types;
	}

	public ArrayList<String> getClients(String selectAll) {
		ArrayList<String> clients = new ArrayList();
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();

			String selectSQL = "SELECT DISTINCT ClientName FROM DimProject ORDER BY ClientName";
			stmt = conn.prepareStatement(selectSQL);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				clients.add(rs.getString("ClientName"));
			}
			if ((selectAll != null) && (!selectAll.equals(""))) {
				clients.add(0, selectAll);
			}
		} catch (SQLException s) {
			System.out.println("SQLException thrown in DWSqlBean.getClients()");
			s.printStackTrace();
		} catch (Exception e) {
			System.out.println("Exception thrown in DWSqlBean.getClients()");
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				System.out
						.println("SQLException thrown in DWSqlBean.getClients() while closing stmt");
				s.printStackTrace();
			}
		}
		return clients;
	}

	private void getYearsFromDB() {
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();

			String selectSQL = "SELECT DISTINCT YearNumber FROM DimDate ORDER BY YearNumber";
			stmt = conn.prepareStatement(selectSQL);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				this._years.add(Integer.toString(rs.getInt("YearNumber")));
			}
		} catch (SQLException s) {
			System.out
					.println("SQLException thrown in DWSqlBean.getYearsFromDB()");
			s.printStackTrace();
		} catch (Exception e) {
			System.out
					.println("Exception thrown in DWSqlBean.getYearsFromDB()");
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				System.out
						.println("SQLException thrown in DWSqlBean.getMonthYearDates() while closing stmt");
				s.printStackTrace();
			}
		}
	}

	public ArrayList<String> getReadableMonthYearDates(String selectAll) {
		ArrayList<String> dates = new ArrayList();
		try {
			if (this._years.isEmpty()) {
				getYearsFromDB();
			}
			Date nowDate = new Date(System.currentTimeMillis());
			SimpleDateFormat sdfNow = new SimpleDateFormat("MMM yyyy");
			String nowMonthYear = sdfNow.format(nowDate);
			Iterator<String> mon;
			for (Iterator<String> yr = this._years.iterator(); yr.hasNext(); mon
					.hasNext()) {
				String year = (String) yr.next();
				mon = Arrays.asList(RevRptConst.MONTHS_TXT).iterator(); // continue;
				String current = (String) mon.next() + " " + year;
				dates.add(current);
				if (current.equals(nowMonthYear)) {
					break;
				}
			}
			if ((selectAll != null) && (!selectAll.equals(""))) {
				dates.add(0, selectAll);
			}
		} catch (Exception e) {
			System.out
					.println("Exception thrown in DWSqlBean.getMonthYearDates()");
			e.printStackTrace();
		}
		return dates;
	}

	public ArrayList<Integer> getIntegerYearMonthDates() {
		ArrayList<Integer> dates = new ArrayList();
		try {
			if (this._years.isEmpty()) {
				getYearsFromDB();
			}
			Date nowDate = new Date(System.currentTimeMillis());
			SimpleDateFormat sdfNow = new SimpleDateFormat("yyyyMM");
			String nowYearMonth = sdfNow.format(nowDate);
			Iterator<String> mon;
			for (Iterator<String> yr = this._years.iterator(); yr.hasNext(); mon
					.hasNext()) {
				int year = Integer.parseInt((String) yr.next());
				mon = Arrays.asList(RevRptConst.MONTHS_NUM).iterator(); // continue;
				String current = year + (String) mon.next();
				dates.add(Integer.valueOf(Integer.parseInt(current)));
				if (current == nowYearMonth) {
					break;
				}
			}
		} catch (Exception e) {
			System.out
					.println("Exception thrown in DWSqlBean.getIntegerYearMonthDates()");
			e.printStackTrace();
		}
		return dates;
	}

	public static LinkedHashMap<Integer, RevenueChartData> getChartData(
			ArrayList<Integer> iDates, int sDate, int eDate, String dtype,
			String spman, String bdman, String jowner, String client,
			String ptype) {
		Statement stmt = null;
		Connection conn = null;
		LinkedHashMap<Integer, RevenueChartData> monthData = new LinkedHashMap();
		StringBuffer sb = new StringBuffer();
		try {
			conn = getConnection();

			int monthStart = sDate * 100 + 1;
			int monthEnd = eDate * 100 + 31;

			sb.append("select fj."
					+ dtype
					+ "/100, SUM(fj.JobRevenue), SUM(COALESCE(fj.JobApprovedCost,fj.JobAuthorizedCost)), SUM(fj.JobVariableGrossProfit) FROM "
					+ "FactJobRevenueActualization" + " fj ");
			if ((!client.equals("All Clients"))
					|| (!ptype.equals("All Project Types"))) {
				sb.append("JOIN DimProject p ON fj.ProjectKey = p.ProjectKey WHERE ");
				if (!client.equals("All Clients")) {
					sb.append("ISNULL(p.ClientName, 'ALL') = CASE WHEN '"
							+ client + "'  = '" + "All Clients"
							+ "' THEN ISNULL(p.ClientName, 'ALL') ELSE '"
							+ client + "' END AND ");
				}
				if (!ptype.equals("All Project Types")) {
					sb.append("ISNULL(p.ProjectType, 'ALL') = CASE WHEN '"
							+ ptype + "' = '" + "All Project Types"
							+ "' THEN ISNULL(p.ProjectType, 'ALL') ELSE '"
							+ ptype + "' END AND ");
				}
			} else {
				sb.append("WHERE ");
			}
			if (dtype.equals("JobOffsiteDate")) {
				sb.append("fj.JobOffsiteDate BETWEEN " + monthStart + " AND "
						+ monthEnd);
			} else if (dtype.equals("JobSubmittedDate")) {
				sb.append("fj.JobSubmittedDate BETWEEN " + monthStart + " AND "
						+ monthEnd);
			} else if (dtype.equals("JobClosedDate")) {
				sb.append("(fj.JobClosedDate BETWEEN " + monthStart + " AND "
						+ monthEnd + ")");
			}
			if (!spman.equals("All Senior Project Mgrs")) {
				int spman_id = getIdFromFullName(spman, 0);
				sb.append(" AND fj.SeniorProjectMgrID = CASE WHEN " + spman_id
						+ " = 0 THEN fj.SeniorProjectMgrID ELSE " + spman_id
						+ " END");
			}
			if (!bdman.equals("All Business Development Mgrs")) {
				int bdman_id = getIdFromFullName(bdman, 0);
				sb.append(" AND fj.BusinessDevelopmentMgrID = CASE WHEN "
						+ bdman_id
						+ " = 0 THEN fj.BusinessDevelopmentMgrID ELSE "
						+ bdman_id + " END");
			}
			if (!jowner.equals("All Job Owners")) {
				int jowner_id = getIdFromFullName(jowner, 0);
				sb.append(" AND ISNULL(fj.JobOwnerID,0) = CASE WHEN "
						+ jowner_id + " = 0 THEN ISNULL(fj.JobOwnerID,0) ELSE "
						+ jowner_id + " END ");
			}
			sb.append(" GROUP BY fj." + dtype + "/100 ORDER BY fj." + dtype
					+ "/100");

			String x = sb.toString();
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(x);
			while (rs.next()) {
				RevenueChartData rcd = new RevenueChartData();
				int date = rs.getInt(1);

				rcd.setMonth(ReportUtils.getMonthFromIntDate(date));
				rcd.setYear(ReportUtils.getYearFromIntDate(date));
				BigDecimal jRevenue = rs.getBigDecimal(2);

				rcd.setRevenue(jRevenue.setScale(0, 4));
				BigDecimal jVCOGS = rs.getBigDecimal(3);

				rcd.setVcogs(jVCOGS.setScale(0, 4));
				BigDecimal jVGP = rs.getBigDecimal(4);

				rcd.setVgp(jVGP.setScale(0, 4));

				monthData.put(Integer.valueOf(date), rcd);
			}
		} catch (SQLException s) {
			System.out
					.println("SQLException thrown in DWSqlBean.getChartData()");
			s.printStackTrace();
		} catch (Exception e) {
			System.out.println("Exception thrown in DWSqlBean.getChartData()");
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				System.out
						.println("SQLException thrown in DWSqlBean.getChartData() while closing stmt");
				s.printStackTrace();
			}
		}
		return monthData;
	}
}
