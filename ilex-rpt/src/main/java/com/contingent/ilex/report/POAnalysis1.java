package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class POAnalysis1 extends DispatchAction {
	DataAccess da = new DataAccess();
	NumberFormat percentage = new DecimalFormat("0.00");
	NumberFormat comma_delimited = new DecimalFormat("#,###");

	public ActionForward report1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "report1";

		String where_clause = "";
		Vector<String> columns = new Vector();
		Vector<String[]> rows = new Vector();
		String title = "";
		String spm = "";
		String jo = "";
		String link = "";

		String sreport = request.getParameter("report");
		if ((sreport == null) || (sreport.equals(""))) {
			sreport = "0";
		}
		int report = Integer.parseInt(sreport);
		switch (report) {
		case 0:
			columns.add("spm");
			where_clause = "spm != 'Shroder, Charles'";
			rows = getUsageStatistics(columns, where_clause);
			title = "Sr. PM - Purchasing Overview for TTM";
			link = "";

			break;
		case 1:
			columns.add("month");
			spm = request.getParameter("spm");
			where_clause = "spm = '" + spm + "'";
			rows = getUsageStatistics(columns, where_clause);
			title = spm.substring(0, spm.indexOf(","))
					+ " - Purchasing for TTM";
			link = "POA1.xo?ac=report1&report=0";
			break;
		case 2:
			columns.add("customer");
			columns.add("project");
			spm = request.getParameter("spm");
			where_clause = "spm = '" + spm + "'";
			rows = getUsageStatistics(columns, where_clause);
			title = spm.substring(0, spm.indexOf(","))
					+ " - Purchasing by Project";
			link = "POA1.xo?ac=report1&report=0";
			break;
		case 3:
			columns.add("jo");
			spm = request.getParameter("spm");
			where_clause = "spm = '" + spm + "'";
			rows = getUsageStatistics(columns, where_clause);
			title = spm.substring(0, spm.indexOf(","))
					+ " - Purchasing by Job Owner";
			link = "POA1.xo?ac=report1&report=0";
			break;
		case 4:
			columns.add("jo");
			where_clause = "spm != 'Shroder, Charles'";
			rows = getUsageStatistics(columns, where_clause);
			title = "Job Owner - Purchasing Overview for TTM";
			link = "";

			break;
		case 5:
			columns.add("customer");
			columns.add("project");
			jo = request.getParameter("jo");
			where_clause = "jo = '" + jo + "'";
			rows = getUsageStatistics(columns, where_clause);
			title = jo.substring(0, jo.indexOf(","))
					+ " - Purchasing by Project";
			link = "POA1.xo?ac=report1&report=4";

			break;
		case 6:
			columns.add("month");
			jo = request.getParameter("jo");
			where_clause = "jo = '" + jo + "'";
			rows = getUsageStatistics(columns, where_clause);
			title = jo.substring(0, jo.indexOf(",")) + " - Purchasing for TTM";
			link = "POA1.xo?ac=report1&report=4";
		}
		request.setAttribute("rows", rows);
		request.setAttribute("report", sreport);
		request.setAttribute("title", title);
		request.setAttribute("link", link);

		return mapping.findForward(forward_key);
	}

	Vector<String[]> getUsageStatistics(Vector<String> cols, String wc) {
		Vector<String[]> rows = new Vector();

		int m = cols.size();
		int s = m + 1;
		int p = s + 1;
		int total = p + 1;
		int msp = 0;
		String column_list = "";

		column_list = (String) cols.get(0);
		for (int i = 1; i < cols.size(); i++) {
			column_list = column_list + "," + (String) cols.get(i);
		}
		try {
			String sql = "select * from (select "
					+ column_list
					+ ", type from datawarehouse.dbo.ac_fact_po_usage_summary where "
					+ wc + ") s "
					+ " PIVOT (COUNT(type) FOR type IN ([M],[S],[P])) p "
					+ " order by " + column_list;

			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				String[] row = new String[cols.size() + 4];
				for (int i = 0; i < cols.size(); i++) {
					row[i] = rs.getString((String) cols.get(i));
				}
				row[m] = rs.getString("M");
				row[s] = rs.getString("S");
				row[p] = rs.getString("P");
				msp = Integer.parseInt(row[m]) + Integer.parseInt(row[s])
						+ Integer.parseInt(row[p]);
				if (msp > 5) {
					row[total] = String.valueOf(msp);

					row[m] = this.percentage.format(Double.parseDouble(row[m])
							/ Double.parseDouble(row[total]));
					row[s] = this.percentage.format(Double.parseDouble(row[s])
							/ Double.parseDouble(row[total]));
					row[p] = this.percentage.format(Double.parseDouble(row[p])
							/ Double.parseDouble(row[total]));
					row[total] = this.comma_delimited.format(Integer
							.parseInt(row[total]));

					rows.add(row);
				}
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		return rows;
	}
}
