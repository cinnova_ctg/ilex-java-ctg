package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.util.Vector;

public class FinancialChartAnalysis2 {
	Double Hi;
	Double Low;
	Double Average;
	Double Total;
	Vector<String> SPM_Ordered_List = new Vector();
	Vector<Double> Revenue = new Vector();
	Vector<Double> Expense = new Vector();
	Vector<Double> PFExpense = new Vector();
	Double[] RevenueArray;
	Double[] ExpenseArray;
	Double[] PFExpenseArray;
	String RevenueSelected = "";
	String RevenueBenchmark = "";
	String RevenueHi = "";
	String RevenueLow = "";
	String RevenueAverage = "";
	String VGPMSelected = "";
	String VGPMBenchmark = "";
	String VGPMHi = "";
	String VGPMLow = "";
	String VGPMAverage = "";
	String PFVGPMSelected = "";
	String PFVGPMBenchmark = "";
	String PFVGPMHi = "";
	String PFVGPMLow = "";
	String PFVGPMAverage = "";

	public FinancialChartAnalysis2(Double[] values) {
		this.Hi = Double.valueOf(0.0D);
		this.Low = Double.valueOf(0.0D);
		this.Average = Double.valueOf(0.0D);
		this.Total = Double.valueOf(0.0D);

		computeValues(values);
	}

	public FinancialChartAnalysis2(Double[] rev, Double[] exp) {
		this.Hi = Double.valueOf(0.0D);
		this.Low = Double.valueOf(0.0D);
		this.Average = Double.valueOf(0.0D);
		this.Total = Double.valueOf(0.0D);

		Double[] vgpm = new Double[rev.length];
		for (int i = 0; i < rev.length; i++) {
			if (rev[i].doubleValue() > 0.0D) {
				vgpm[i] = Double.valueOf(1.0D - exp[i].doubleValue()
						/ rev[i].doubleValue());
			} else {
				vgpm[i] = Double.valueOf(0.0D);
			}
		}
		computeValues(vgpm);
	}

	public FinancialChartAnalysis2(ResultSet rs, String selected) {
		this.Hi = Double.valueOf(0.0D);
		this.Low = Double.valueOf(0.0D);
		this.Average = Double.valueOf(0.0D);
		this.Total = Double.valueOf(0.0D);

		this.Revenue.clear();
		this.Expense.clear();
		this.PFExpense.clear();

		int pm_cnt = 0;
		int pm_selected = -1;

		buildRawData(rs);

		this.Total = computeTotal(this.Revenue);
		if (!selected.equals("%")) {
			pm_cnt = this.SPM_Ordered_List.size() - 1;
			pm_selected = findSelectedPM(selected);
		} else {
			pm_cnt = this.SPM_Ordered_List.size();
		}
		this.RevenueArray = new Double[pm_cnt];
		this.ExpenseArray = new Double[pm_cnt];
		this.PFExpenseArray = new Double[pm_cnt];
		for (int i = 0; i < this.SPM_Ordered_List.size(); i++) {
			if (pm_selected != i) {
				this.RevenueArray[i] = ((Double) this.Revenue.get(i));
				this.ExpenseArray[i] = ((Double) this.Expense.get(i));
				this.PFExpenseArray[i] = ((Double) this.PFExpense.get(i));
			}
		}
		this.RevenueArray = sortValues(this.RevenueArray);

		this.Hi = this.RevenueArray[0];
		this.Low = this.RevenueArray[(this.RevenueArray.length - 1)];
		this.Average = computeAverage(this.RevenueArray);

		this.ExpenseArray = sortValues(this.ExpenseArray);
		this.PFExpenseArray = sortValues(this.PFExpenseArray);
	}

	void computeValues(Double[] values) {
		values = sortValues(values);

		this.Hi = values[(values.length - 1)];
		this.Low = values[0];
		this.Average = computeAverage(values);
	}

	Double[] sortValues(Double[] v) {
		for (int i = 0; i < v.length - 1; i++) {
			for (int j = i + 1; j < v.length; j++) {
				if (v[i].doubleValue() > v[j].doubleValue()) {
					Double t1 = v[i];
					v[i] = v[j];
					v[j] = t1;
				}
			}
		}
		return v;
	}

	Double computeAverage(Double[] v) {
		Double cnt = Double.valueOf(1.0D * v.length);
		for (int i = 0; i < v.length; i++) {
			this.Total = Double.valueOf(this.Total.doubleValue()
					+ v[i].doubleValue());
		}
		return Double.valueOf(this.Total.doubleValue() / cnt.doubleValue());
	}

	Double computeTotal(Vector<Double> r) {
		Double total = Double.valueOf(0.0D);
		for (int i = 0; i < r.size(); i++) {
			total = Double.valueOf(total.doubleValue()
					+ ((Double) r.get(i)).doubleValue());
		}
		return total;
	}

	int findSelectedPM(String sp) {
		int found_sp = -1;
		for (int i = 0; i < this.SPM_Ordered_List.size(); i++) {
			if (((String) this.SPM_Ordered_List.get(i)).equals(sp)) {
				found_sp = i;
				break;
			}
		}
		return found_sp;
	}

	void buildRawData(ResultSet rs) {
		try {
			while (rs.next()) {
				this.SPM_Ordered_List.add(rs.getString("ac_os_spm"));
				this.Revenue.add(Double.valueOf(rs.getDouble("revenue")));
				this.Expense.add(Double.valueOf(rs.getDouble("Actual")));
				this.PFExpense.add(Double.valueOf(rs.getDouble("pf")));
			}
			rs.close();
		} catch (Exception localException) {
		}
	}

	public static void main(String[] args) {
		Double[] v1 = { Double.valueOf(1.0D), Double.valueOf(2.0D),
				Double.valueOf(3.0D), Double.valueOf(4.0D),
				Double.valueOf(6.0D), Double.valueOf(8.1D) };
		Double[] v2 = { Double.valueOf(1.0D), Double.valueOf(1.5D),
				Double.valueOf(1.5D), Double.valueOf(4.0D),
				Double.valueOf(3.0D), Double.valueOf(6.1D) };

		FinancialChartAnalysis2 fca = new FinancialChartAnalysis2(v1, v2);

		System.out.println(fca.Hi + " " + fca.Low + " " + fca.Average);
	}
}
