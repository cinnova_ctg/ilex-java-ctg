package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Vector;

public class AnalysisTasks {
	Vector Appendices = new Vector();
	Integer appendixCount = Integer.valueOf(0);
	DataAccess da = new DataAccess();
	String sql;
	SimpleDateFormat x = new SimpleDateFormat("M/d/y");
	int monthPosition = 0;
	Vector interval = new Vector(12);
	Integer numberOfDays = Integer.valueOf(0);

	public AnalysisTasks() {
		int i = 0;

		String sql = "select lx_pr_id from ap_appendix_id_list_01 ";

		sql = sql + " where lx_pr_status in ('I', 'F', 'O')";
		sql = sql + " order by lo_ot_name";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String id = rs.getString("lx_pr_id");
				Appendix x = new Appendix(id);
				this.Appendices.add(i, x);
				i++;
			}
			this.appendixCount = Integer.valueOf(i);
			System.out.println("Appendices Consider: " + this.appendixCount);
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	void updateSummaries() {
		try {
			this.sql = "exec ap_sumary_update 'clear'";
			this.da.executeQuery(this.sql);
			for (int i = 0; i < this.Appendices.size(); i++) {
				Appendix appendix = (Appendix) this.Appendices.get(i);
				String ap_jl_pr_id = appendix.lx_pr_id;
				String ap_jl_pr_title = appendix.lx_pr_title.replaceAll("'",
						"''");
				String ap_jl_ot_name = appendix.lo_ot_name;
				for (int j = 0; j < appendix.jobs.size(); j++) {
					Job job = (Job) appendix.jobs.get(j);

					this.sql =

					("execute ap_job_level_insert_01 "
							+ convertNumeric(job.lm_js_id)
							+ ", "
							+ convertString(job.allocation_month)
							+ ", "
							+ convertString(job.overall_status)
							+ ", "
							+ convertNumeric(ap_jl_pr_id)
							+ ", "
							+ convertString(ap_jl_pr_title)
							+ ", "
							+ convertString(ap_jl_ot_name)
							+ ", "
							+ convertString(job.lm_js_title)
							+ ", "
							+ convertString(job.start_date)
							+ ", "
							+ convertString(job.end_date)
							+ ", "
							+ convertDouble(job.financialData.lx_ce_total_cost)
							+ ", "
							+ convertDouble(job.authorized)
							+ ", "
							+ convertDouble(job.invoice_total)
							+ ", "
							+ convertDouble(job.approved)
							+ ", "
							+ convertDouble(job.actual_cost)
							+ ", "
							+ convertDouble(job.financialData.lx_ce_total_price)
							+ ", " + convertDouble(job.vgp) + ", "
							+ convertString(job.project_manager) + ", "
							+ convertString(job.employee) + ", "
							+ convertString(job.customer_ref_missing) + ", "
							+ convertString(job.reconciled) + ", "
							+ convertString(job.site_missing) + ", "
							+ convertString(job.notes_missing) + ", "
							+ convertString(job.invoiced_vs_approved) + ", "
							+ convertString(job.first_visit_success) + ", "
							+ convertString(job.inconsistent_job) + ", "
							+ convertString(job.zip_code) + ", "
							+ convertString(job.lx_pr_type) + ", "
							+ convertString(job.lm_tc_msp) + ", " + convertString(job.lm_tc_help_desk));
					this.da.executeQuery(this.sql);
				}
			}
			this.sql = "exec ap_sumary_update 'update'";
			this.da.executeQuery(this.sql);
		} catch (Exception e) {
			System.out.println(e + " " + this.sql);
		}
	}

	void updateRevenue() {
		try {
			this.sql = "exec ap_revenue_summary_update 'clear'";
			this.da.executeQuery(this.sql);
			for (int i = 0; i < this.Appendices.size(); i++) {
				computeJobRevenue((Appendix) this.Appendices.get(i));
			}
			this.sql = "exec ap_revenue_summary_update 'update'";
			this.da.executeQuery(this.sql);
		} catch (Exception e) {
			System.out.println(e + " " + this.sql);
		}
	}

	void computeJobRevenue(Appendix appendix) {
		String[] intervalPoints = new String[3];
		for (int j = 0; j < appendix.jobs.size(); j++) {
			Job job = (Job) appendix.jobs.get(j);

			this.interval.clear();
			this.numberOfDays = Integer.valueOf(0);
			this.monthPosition = 0;
			intervalPoints = getStartEndPoints(job);
			if (!intervalPoints[2].equals("No Revenue")) {
				job.taskType = intervalPoints[2];
				buildRevenueIntervals(intervalPoints[0], intervalPoints[1], job);

				createRevenueJobRecord(appendix, job);
			}
		}
	}

	String[] getStartEndPoints(Job job) {
		String[] ip = new String[3];
		if (job.overall_status.equals("Complete")) {
			if (job.end_date == null) {
				ip[1] = job.lm_js_submitted_date;
			} else {
				ip[1] = job.end_date;
			}
			if (job.start_date == null) {
				ip[0] = ip[1];
			} else {
				ip[0] = job.start_date;
			}
			ip[2] = "Closed";
		} else if (job.overall_status.equals("Closed")) {
			if (job.end_date == null) {
				ip[1] = job.lm_js_closed_date;
			} else {
				ip[1] = job.end_date;
			}
			if (job.start_date == null) {
				ip[0] = ip[1];
			} else {
				ip[0] = job.start_date;
			}
			ip[2] = "Closed";
		} else if (job.overall_status.equals("In Work - Offsite")) {
			if (job.start_date == null) {
				ip[0] = job.lm_js_create_date;
			} else {
				ip[0] = job.start_date;
			}
			ip[1] = job.end_date;
			ip[2] = "Closed";
		} else if (job.overall_status.equals("In Work - Onsite")) {
			ip[0] = job.start_date;
			if (job.lm_js_planned_end_date == null) {
				ip[1] = job.start_date;
			} else {
				ip[1] = job.lm_js_planned_end_date;
			}
			ip[2] = "Onsite";
		} else if ((job.overall_status.equals("In Work - Scheduled"))
				|| (job.overall_status.equals("In Work - Overdue"))) {
			ip[0] = job.lm_js_planned_start_date;
			if (job.lm_js_planned_end_date == null) {
				ip[1] = job.lm_js_planned_start_date;
			} else {
				ip[1] = job.lm_js_planned_end_date;
			}
			ip[2] = "Scheduled";
		} else {
			ip[0] = null;
			ip[1] = null;
			ip[2] = "No Revenue";
		}
		return ip;
	}

	void buildRevenueIntervals(String start_point, String end_point, Job job) {
		try {
			GregorianCalendar startOfInterval = new GregorianCalendar();
			startOfInterval
					.setTimeInMillis(this.x.parse(start_point).getTime());

			GregorianCalendar endOfInterval = new GregorianCalendar();
			endOfInterval.setTimeInMillis(this.x.parse(end_point).getTime());
			if (endOfInterval.after(startOfInterval)) {
				computeInterval(startOfInterval, endOfInterval);
			} else if (endOfInterval.equals(startOfInterval)) {
				this.interval.add(0, new RevenueInterval(startOfInterval, 1));
				this.numberOfDays = Integer.valueOf(this.numberOfDays
						.intValue() + 1);
			} else if ((job.lm_js_status.equals("O"))
					|| (job.lm_js_status.equals("O"))) {
				System.out.println("Corected start and end points s/e: "
						+ start_point + " " + end_point + " " + job.lm_js_id);
				this.interval.add(0, new RevenueInterval(endOfInterval, 1));
				this.numberOfDays = Integer.valueOf(this.numberOfDays
						.intValue() + 1);
			} else {
				System.out.println("Illegal start and end points s/e: "
						+ start_point + " " + end_point + " " + job.lm_js_id);
			}
		} catch (Exception e) {
			System.out.println("Compute Interval " + e);
		}
	}

	void computeInterval(GregorianCalendar s1, GregorianCalendar e1) {
		if ((s1.get(2) == e1.get(2)) && (s1.get(1) == e1.get(1))) {
			int x = s1.get(5);
			int y = e1.get(5);
			int delta = y - x + 1;
			this.numberOfDays = Integer.valueOf(this.numberOfDays.intValue()
					+ delta);
			this.interval.add(this.monthPosition++, new RevenueInterval(s1,
					delta));
		} else {
			int x = s1.get(5);
			int y = s1.getActualMaximum(5);
			int delta = y - x + 1;
			this.numberOfDays = Integer.valueOf(this.numberOfDays.intValue()
					+ delta);
			this.interval.add(this.monthPosition++, new RevenueInterval(s1,
					delta));
			s1.set(s1.get(1), s1.get(2) + 1, 1);

			computeInterval(s1, e1);
		}
	}

	void createRevenueJobRecord(Appendix appendix, Job job) {
		try {
			for (int j = 0; j < this.interval.size(); j++) {
				RevenueInterval ri = (RevenueInterval) this.interval.get(j);
				Double allocation = Double.valueOf(ri.NumberOfDays
						.doubleValue() / this.numberOfDays.doubleValue());
				Double revenue = Double.valueOf(allocation.doubleValue()
						* job.financialData.lx_ce_total_price.doubleValue());
				Double e_cost = Double.valueOf(allocation.doubleValue()
						* job.financialData.lx_ce_total_cost.doubleValue());
				Double a_cost = Double.valueOf(allocation.doubleValue()
						* job.actual_cost.doubleValue());

				this.sql =

				("exec ap_job_level_revenue_insert_01 "
						+ convertString(job.lm_js_id)
						+ ", "
						+ convertString(ri.allocationDate)
						+ ", "
						+ convertString(appendix.lo_ot_name)
						+ ", "
						+ convertString(appendix.lx_pr_id)
						+ ", "
						+ convertString(appendix.lx_pr_title.replaceAll("'",
								"''")) + ", "
						+ convertNumeric(appendix.lx_pr_type) + ", "
						+ convertString(appendix.project_contact) + ", "
						+ convertString(appendix.sales_contact) + ", "
						+ convertString(job.taskType) + ", "
						+ convertDouble(allocation) + ", "
						+ convertDouble(revenue) + ", " + convertDouble(e_cost)
						+ ", " + convertDouble(a_cost) + ", " + convertString(job.overall_status));

				this.da.executeQuery(this.sql);
			}
		} catch (Exception e) {
			System.out.println(e + " " + this.sql);
		}
	}

	String convertString(String s) {
		if ((s == null) || (s.length() < 1)) {
			s = "null";
		} else {
			s = "'" + s.replaceAll("'", "''") + "'";
		}
		return s;
	}

	Double convertDouble(Double d) {
		if (d == null) {
			d = Double.valueOf(0.0D);
		}
		return d;
	}

	String convertNumeric(String n) {
		if (n == null) {
			n = "null";
		}
		return n;
	}

	public static void main(String[] args) {
		AnalysisTasks x = new AnalysisTasks();
		x.updateSummaries();
		x.updateRevenue();
	}
}
