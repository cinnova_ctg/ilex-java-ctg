package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class Charts extends DispatchAction {
	DataAccess da = new DataAccess();

	public ActionForward charts(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "charts";

		getChartData(request);

		return mapping.findForward(forward_key);
	}

	void getChartData(HttpServletRequest request) {
		Map charts = new HashMap();
		Map parameters = null;

		String sql = "select dw_chart_name, rtrim(dw_chd) as dw_chd, rtrim(dw_chxl1) as dw_chxl1, rtrim(dw_chxl2) as dw_chxl2,rtrim(dw_chxl3) as dw_chxl3 from dw_charts";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				parameters = new HashMap(6);

				parameters.put("dw_chd", rs.getString("dw_chd"));
				parameters.put("dw_chxl1", rs.getString("dw_chxl1"));
				parameters.put("dw_chxl2", rs.getString("dw_chxl2"));
				parameters.put("dw_chxl3", rs.getString("dw_chxl3"));
				charts.put(rs.getString("dw_chart_name"), parameters);
			}
			rs.close();

			request.setAttribute("charts", charts);
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}
}
