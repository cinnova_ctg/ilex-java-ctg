package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class ConsolidatedInvoices extends DispatchAction {
	DataAccess da = new DataAccess();
	String WhereClause = "";
	Vector MasterActivityList = new Vector();
	Vector Jobs = new Vector();

	public ActionForward invoice_list1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "invoice_list1";

		request.setAttribute("lx_pr_id", "819");

		managePageRequest1(request);

		return mapping.findForward(forward_key);
	}

	public ActionForward invoice_list2(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "invoice_list2";

		request.setAttribute("lx_pr_id", "819");

		managePageRequest2(request);

		return mapping.findForward(forward_key);
	}

	String managePageRequest2(HttpServletRequest request) {
		String x = "";

		Map inputs = getInputParameters(request);

		this.Jobs.clear();

		this.MasterActivityList.clear();
		this.MasterActivityList.add(0, "IBM POS Upgrade 1x-nP");
		this.MasterActivityList.add(1, "fxibmupg");
		this.MasterActivityList.add(2, "Revisit");
		this.MasterActivityList.add(3, "OOS-All");
		if ((inputs.get("lx_pr_id") != null)
				&& (inputs.get("from_date") != null)
				&& (inputs.get("to_date") != null)) {
			Vector rows = getData2(request, inputs);
			if ((rows != null) && (rows.size() > 0)) {
				getActivities(rows);
				request.setAttribute("Jobs", this.Jobs);
				request.setAttribute("ma", this.MasterActivityList);
			}
		}
		return x;
	}

	Vector getData2(HttpServletRequest request, Map inputs) {
		String sql = "exec ap_consolidated_invoice_list_01A "
				+ (String) inputs.get("lx_pr_id") + ", '"
				+ (String) inputs.get("from_date") + "', '"
				+ (String) inputs.get("to_date") + "'";
		System.out.println(sql);

		Vector rows = new Vector();
		String[] row = new String[8];

		String customer = "";

		int i = 0;
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				row = new String[12];

				customer = rs.getString(2);

				row[0] = rs.getString(1);
				row[1] = rs.getString(3);
				row[2] = rs.getString(4);
				row[3] = rs.getString(5);
				row[4] = rs.getString(6);
				row[5] = rs.getString(7);
				row[6] = rs.getString(8);
				row[7] = rs.getString(9);
				row[8] = rs.getString(10);
				row[9] = rs.getString(11);

				rows.add(i++, row);
			}
			rs.close();

			request.setAttribute("customer", customer);
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
		return rows;
	}

	void getActivities(Vector rows) {
		String sql = "";

		String lm_js_id = "";
		for (int i = 0; i < rows.size(); i++) {
			Map job = new HashMap();

			String[] row = (String[]) rows.get(i);
			job.put("row", row);

			lm_js_id = row[0];

			Map activities = new HashMap();

			activities.clear();
			activities.put("IBM POS Upgrade 1x-nP", Double.valueOf(0.0D));
			activities.put("fxibmupg", Double.valueOf(0.0D));
			activities.put("Revisit", Double.valueOf(0.0D));
			activities.put("OOS-All", Double.valueOf(0.0D));

			sql = "select ltrim(rtrim(lm_at_title)), lx_ce_total_price from lm_activity join lx_cost_element on lm_at_id = lx_ce_used_in and lx_ce_type like '%A' where  lm_at_title != 'FIXED OVERHEAD' and lm_at_js_id = "
					+ lm_js_id;

			ResultSet rs = this.da.getResultSet(sql);
			try {
				while (rs.next()) {
					String activity = rs.getString(1);

					Double price = Double.valueOf(rs.getDouble(2));
					if (activities.containsKey(activity)) {
						activities.put(activity, Double
								.valueOf(((Double) activities.get(activity))
										.doubleValue() + price.doubleValue()));
					} else {
						activities.put("OOS-All", Double
								.valueOf(((Double) activities.get("OOS-All"))
										.doubleValue() + price.doubleValue()));
					}
				}
				job.put("activities", activities);
				rs.close();
			} catch (SQLException e) {
				System.out.println(e + " " + sql);
			}
			String installation_notes = "";
			String delimiter = "";
			sql = "select convert(varchar(24), lm_in_cdate, 0), lm_in_notes from lm_install_notes where lm_in_js_id  = "
					+ lm_js_id + " order by lm_in_cdate desc";

			rs = this.da.getResultSet(sql);
			try {
				while (rs.next()) {
					installation_notes = installation_notes + delimiter
							+ rs.getString(1) + " - " + rs.getString(2);
					delimiter = "; ";
				}
				job.put("installation_notes", installation_notes);
				rs.close();
			} catch (SQLException e) {
				System.out.println(e + " " + sql);
			}
			this.Jobs.add(i, job);
		}
	}

	String managePageRequest1(HttpServletRequest request) {
		String x = "";

		Map inputs = getInputParameters(request);
		if ((inputs.get("lx_pr_id") != null)
				&& (inputs.get("from_date") != null)
				&& (inputs.get("to_date") != null)) {
			getData(request, inputs);
		}
		return x;
	}

	void getData(HttpServletRequest request, Map inputs) {
		String sql = "exec ap_consolidated_invoice_list_01 "
				+ (String) inputs.get("lx_pr_id") + ", '"
				+ (String) inputs.get("from_date") + "', '"
				+ (String) inputs.get("to_date") + "'";
		System.out.println(sql);

		Vector rows = new Vector();
		String[] row = new String[8];

		String customer = "";

		int i = 0;
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				row = new String[8];
				customer = rs.getString(1);

				row[0] = rs.getString(2);
				row[1] = rs.getString(3);
				row[2] = rs.getString(4);
				row[3] = rs.getString(5);
				row[4] = rs.getString(6);
				row[5] = rs.getString(7);
				row[6] = rs.getString(8);

				rows.add(i++, row);
			}
			rs.close();

			request.setAttribute("rows", rows);

			request.setAttribute("customer", customer);
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
	}

	String buildWhereClause(Map inputs) {
		String wc = "";
		String delimiter = "";

		Vector expressions = new Vector();
		int i = 0;

		boolean lx_pr_id = inputs.get("lx_pr_id") != null;
		boolean from_date = (inputs.get("from_date") != null)
				&& (!((String) inputs.get("from_date")).equals(""));
		boolean to_date = (inputs.get("to_date") != null)
				&& (!((String) inputs.get("to_date")).equals(""));
		if (lx_pr_id) {
			expressions.add(i++,
					"lm_js_pr_id = " + (String) inputs.get("lx_pr_id"));
		}
		if (from_date) {
			expressions.add(
					i++,
					"lm_js_submitted_date >= '"
							+ (String) inputs.get("from_date") + "'");
		}
		if (to_date) {
			expressions.add(i++, "lm_js_submitted_date < dateadd(dd,1,'"
					+ (String) inputs.get("to_date") + "')");
		}
		wc = " ";
		delimiter = " and ";
		for (int j = 0; j < expressions.size(); j++) {
			wc = wc + delimiter + expressions.get(j);
		}
		return wc;
	}

	Map getInputParameters(HttpServletRequest request) {
		Vector requestParameters = initializeInputParameters();

		Map inputs = new HashMap();
		for (int i = 0; i < requestParameters.size(); i++) {
			String parameter = (String) requestParameters.get(i);
			String value = request.getParameter(parameter);
			if ((value != null) && (value.trim().length() > 0)) {
				inputs.put(parameter, value);
				request.setAttribute(parameter, value);
			}
		}
		request.setAttribute("requestParameters", requestParameters);

		return inputs;
	}

	Vector initializeInputParameters() {
		Vector inputParameters = new Vector();

		inputParameters.clear();
		inputParameters.add(0, "lx_pr_id");
		inputParameters.add(1, "from_date");
		inputParameters.add(2, "to_date");

		return inputParameters;
	}
}
