package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class Workloads extends DispatchAction {
	Vector<String[]> InputParameters = new Vector();
	int Days = 7;
	int Points = 11;
	String[][] Matrix = new String[this.Days][this.Points];
	int ac_as_date = 0;
	int ac_as_job_classification = 1;
	int ac_as_customersint = 2;
	int ac_as_appendices = 3;
	int ac_as_jobs = 4;
	int ac_as_users_ppsint = 5;
	int ac_as_users_non_ppsint = 6;
	int ac_as_actions_job_pps = 7;
	int ac_as_actions_job_non_pps = 8;
	int ac_as_actions_po_pps = 9;
	int ac_as_actions_po_non_pps = 10;
	Map<String, String> JobTypeMap = new HashMap();
	DataAccess da = new DataAccess();
	static NumberFormat currency;
	static NumberFormat percentage = NumberFormat.getPercentInstance();
	static NumberFormat vgpm_format = new DecimalFormat("0.00");

	public ActionForward workload1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "error";

		mapJobTypesSelections();
		if (getReportData(request, buildSQL(request))) {
			forward_key = "workload1";
		}
		return mapping.findForward(forward_key);
	}

	String buildSQL(HttpServletRequest request) {
		String sql = "select sc_dd_mmddyyyy,ac_as_job_classification, ac_as_customers, ac_as_appendices, ac_as_jobs, ac_as_users_pps, ac_as_users_non_pps, ac_as_actions_job_pps, ac_as_actions_job_non_pps, ac_as_actions_po_pps, ac_as_actions_po_non_pps   from  datawarehouse.dbo.ac_dimension_date       left join datawarehouse.dbo.ac_fact2_action_based_summaries_by_day       on sc_dd_mmddyyyy = ac_as_date and -filter-  where sc_dd_mmddyyyy between dateadd (dd, -8, getdate()) and dateadd (dd, -1, getdate()) ";

		String job_classification = request.getParameter("job_type");
		if ((job_classification == null) || (job_classification.equals(""))) {
			job_classification = "All";
		}
		sql = sql.replace("-filter-",
				(CharSequence) this.JobTypeMap.get(job_classification));

		System.out.println(sql);
		return sql;
	}

	boolean getReportData(HttpServletRequest request, String sql) {
		boolean rc = false;
		int i = 0;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				for (int j = 0; j < this.Points; j++) {
					this.Matrix[i][j] = rs.getString(j + 1);
					if (rs.wasNull()) {
						this.Matrix[i][j] = "";
					}
					if (j == 0) {
						this.Matrix[i][j] = this.Matrix[i][j].substring(8, 10);
					}
				}
				i++;
			}
			rs.close();
			request.setAttribute("matrix", this.Matrix);
			rc = true;
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
		return rc;
	}

	String mapJobTypesSelections() {
		String defaults = "";

		this.JobTypeMap.put("All", " (ac_as_job_classification = 'All') ");
		this.JobTypeMap.put("Standard",
				" (ac_as_job_classification = 'Standard') ");
		this.JobTypeMap.put("NetMedX",
				" (ac_as_job_classification = 'NetMedX') ");
		this.JobTypeMap.put("MSP", " (ac_as_job_classification = 'MSP') ");
		this.JobTypeMap.put("Help Desk",
				" (ac_as_job_classification = 'Help Desk') ");

		this.Matrix = new String[this.Days][this.Points];
		for (int i = 0; i < this.Days; i++) {
			for (int j = 0; j < this.Points; j++) {
				this.Matrix[i][j] = "";
			}
		}
		return defaults;
	}
}
