package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class MSPMap extends DispatchAction {
	DataAccess da = new DataAccess();

	public MSPMap() {
		getSites("1");

		getSiteStats();
	}

	public ActionForward msp_map(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "msp_map";

		String type = request.getParameter("type");
		if (type == null) {
			type = "1";
		}
		request.setAttribute("type", type);

		request.setAttribute("points", getSites(type));

		request.setAttribute("stats", getSiteStats());

		return mapping.findForward(forward_key);
	}

	Vector getSites(String type) {
		String sql = "exec wug_map_demo_01 " + type;

		Vector<String[]> points = new Vector();

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] pt = new String[6];
				pt[0] = rs.getString(1);
				pt[1] = rs.getString(2);
				pt[2] = rs.getString(3);
				pt[3] = rs.getString(4);
				pt[4] = rs.getString(5);
				pt[5] = rs.getString(6);
				points.add(pt);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return points;
	}

	String[] getSiteStats() {
		String sql = "select wug_ms_cnt_active, wug_ms_cnt_down, wug_ms_cnt_level_0, wug_ms_cnt_level_1, wug_ms_cnt_level_2, wug_ms_cnt_bounces   from dbo.wug_monitoring_summary where wug_ms_dv_type = 1";

		String[] site_stats = new String[6];

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				site_stats[0] = rs.getString(1);
				site_stats[1] = rs.getString(2);
				site_stats[2] = rs.getString(3);
				site_stats[3] = rs.getString(4);
				site_stats[4] = rs.getString(5);
				site_stats[5] = rs.getString(6);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return site_stats;
	}

	public static void main(String[] args) {
		MSPMap x = new MSPMap();
	}
}
