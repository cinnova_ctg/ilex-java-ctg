package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class AppendixSummary extends DispatchAction {
	DataAccess da = new DataAccess();
	Map Status = new HashMap();

	public ActionForward appendixList(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		init();

		String forward_key = appendixList1(request);

		return mapping.findForward(forward_key);
	}

	public ActionForward appendixEUSR(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		init();

		String forward_key = appendixEUSR1(request);

		return mapping.findForward(forward_key);
	}

	String appendixList1(HttpServletRequest request) {
		String forward_key = "searchResults";

		getRequestParameters(request);
		getReportData(formatSQL(), request);

		return forward_key;
	}

	void getRequestParameters(HttpServletRequest request) {
	}

	String formatRequestParameter(String parameter) {
		if (parameter == null) {
			parameter = "";
		}
		return parameter;
	}

	String formatSQL() {
		String sql_command = "select lo_ot_name, lx_pr_title, lo_pc_last_name, lx_pr_status, lx_pr_type, convert(varchar(10),lx_pd_created_date,101) from lx_appendix_main join lp_msa_main on lx_pr_mm_id = lp_mm_id join lo_organization_top  on lp_mm_ot_id=lo_ot_id join lx_appendix_detail on lx_pd_pr_id = lx_pr_id  left join lo_poc on lx_pr_cns_poc = lo_pc_id where lx_pr_status not in ('Z', 'C') order by lo_ot_name";

		return sql_command;
	}

	void getReportData(String sql, HttpServletRequest request) {
		Vector rows = new Vector();
		int i = 0;
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[6];
				row[0] = rs.getString(1);
				row[1] = rs.getString(2);
				row[2] = rs.getString(3);

				row[3] = rs.getString(4);
				if (!rs.wasNull()) {
					row[3] = ((String) this.Status.get(row[3]));
				}
				row[4] = rs.getString(5);
				row[5] = rs.getString(6);

				rows.add(i++, row);
			}
			rs.close();
			request.setAttribute("rows", rows);
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	String appendixEUSR1(HttpServletRequest request) {
		String forward_key = "EUSR";

		getRequestParameters(request);
		getReportData1(formatSQL1(), request);

		return forward_key;
	}

	String formatSQL1() {
		String sql_command = "select lm_js_title, lm_si_number, lm_si_address, lm_si_city, lm_si_state, lm_si_zip_code, lm_js_prj_status,   convert(varchar(12),lm_js_closed_date,101) as lm_js_closed_date, convert(varchar(12),lm_js_submitted_date,101) as lm_js_submitted_date,  convert(varchar(12),lm_js_planned_start_date,101) as lm_js_planned_start_date, convert(varchar(12),lm_js_planned_end_date,101) as lm_js_planned_end_date, convert(varchar(12),lm_js_submitted_date,101) as lm_js_submitted_date, convert(varchar(12), lx_se_start, 101)+' '+convert(varchar(12), lx_se_start, 108) as lx_se_start, convert(varchar(12), lx_se_end, 101)+' '+convert(varchar(12), lx_se_end, 108) as lx_se_end, lm_js_id from lm_job   join lm_site_detail on lm_js_si_id=lm_si_id  join lx_schedule_element on lm_js_se_schedule=lx_se_id where lm_js_pr_id=517";

		return sql_command;
	}

	void getReportData1(String sql, HttpServletRequest request) {
		Vector rows = new Vector();
		Vector updatedRows = new Vector();

		Map parameter = new HashMap();
		Map paraOrder = new HashMap();
		Vector paraList = new Vector();

		Integer N = new Integer(0);
		int n = 0;
		int i = 0;
		int x = 0;
		Integer M = new Integer(0);
		int m = 0;

		String sql1 = "select lm_ap_cust_info_id, lm_ap_cust_info_parameter   from lm_appendix_customer_info  where lm_ap_cust_info_pr_id = 517  order by lm_ap_cust_info_id";

		ResultSet rs = this.da.getResultSet(sql1);
		try {
			while (rs.next()) {
				parameter.put(rs.getString(1), rs.getString(2));
				paraOrder.put(rs.getString(1), N);
				paraList.add(n, rs.getString(1));
				n++;
				N = Integer.valueOf(n);
			}
			rs.close();
			request.setAttribute("parameter", parameter);
			request.setAttribute("paraList", paraList);
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql1);
		}
		rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[17];
				row[0] = rs.getString("lm_js_title");
				row[1] = rs.getString("lm_si_number");
				row[2] = rs.getString("lm_si_address");
				row[3] = rs.getString("lm_si_city");
				row[4] = rs.getString("lm_si_state");
				row[5] = rs.getString("lm_si_zip_code");
				row[6] = formatStatus(rs.getString("lm_js_prj_status"));

				row[7] = formatDateValues(rs.getString("lm_js_submitted_date"));
				row[8] = formatDateValues(rs.getString("lm_js_closed_date"));

				row[9] = formatDateValues(rs
						.getString("lm_js_planned_start_date"));
				row[10] = formatDateValues(rs
						.getString("lm_js_planned_end_date"));

				row[11] = formatDateValues(rs.getString("lx_se_start"));
				row[12] = formatDateValues(rs.getString("lx_se_end"));

				row[13] = formatDateValues(rs.getString("lm_js_id"));
				row[14] = "";
				row[15] = "";
				row[16] = "";
				rows.add(i++, row);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
		for (int j = 0; j < rows.size(); j++) {
			String[] updatedRow = (String[]) rows.get(j);

			String sql2 = "select lm_ci_ap_cust_info_id, lm_ci_value   from lm_customer_info  where lm_ci_ap_cust_info_id in (49, 50, 51) and lm_ci_js_id = "
					+

					updatedRow[13] + " " + " order by lm_ci_ap_cust_info_id";
			rs = this.da.getResultSet(sql2);
			try {
				while (rs.next()) {
					String lm_ci_value = rs.getString(2);
					String lm_ci_ap_cust_info_id = rs.getString(1);
					if (paraOrder == null) {
						System.out.println("paraOrder Bad");
						if (paraOrder.get(lm_ci_ap_cust_info_id) == null) {
							System.out.println(j + " - "
									+ lm_ci_ap_cust_info_id + " = "
									+ lm_ci_value + " " + sql2);
						}
					} else if (paraOrder.get(lm_ci_ap_cust_info_id) != null) {
						M = (Integer) paraOrder.get(lm_ci_ap_cust_info_id);
						m = M.intValue();
						updatedRow[(14 + m)] = lm_ci_value;
						System.out.println(j + " " + m + " with "
								+ lm_ci_ap_cust_info_id + " = " + lm_ci_value);
					} else {
						System.out.println("Bad id");
					}
				}
				rs.close();

				updatedRows.add(j, updatedRow);
			} catch (Exception e) {
				System.out.println(e + " - with - " + sql2);
			}
		}
		request.setAttribute("rows", updatedRows);
	}

	String formatCurrencyValues(Double value) {
		NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
		String formattedValue = nf.format(value);

		return formattedValue;
	}

	String formatDateValues(String date_Value) {
		if (date_Value == null) {
			date_Value = "";
		} else if (date_Value.startsWith("01/01/1900")) {
			date_Value = "";
		}
		return date_Value;
	}

	String formatStatus(String status) {
		status = (String) this.Status.get(status);

		return status;
	}

	String formatPercentageValues(Double value) {
		NumberFormat nf = NumberFormat.getPercentInstance(Locale.US);
		String formattedValue = nf.format(value);

		return formattedValue;
	}

	String formatNumericValues(Double value) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		String formattedValue = nf.format(value);

		return formattedValue;
	}

	void init() {
		this.Status.put("A", "Approved");
		this.Status.put("D", "Draft");
		this.Status.put("F", "Complete");
		this.Status.put("H", "Hold");
		this.Status.put("I", "In Work");
		this.Status.put("O", "Closed");
		this.Status.put("T", "Transmitted");
		this.Status.put("S", "Signed");
	}
}
