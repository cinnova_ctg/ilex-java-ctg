package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class CopperAnalysis {
	Vector TicketList = new Vector();
	Vector Tickets = new Vector();
	DataAccess da = new DataAccess();

	public CopperAnalysis() {
		getSampleSet();
		for (int i = 0; i < this.TicketList.size(); i++) {
			this.Tickets.add(new Ticket((String) this.TicketList.get(i),
					this.da));
		}
		for (int i = 0; i < this.TicketList.size(); i++) {
			Ticket t = (Ticket) this.Tickets.get(i);
			if ((t.Ignore.equals("OK"))
					&& (t.PurchaseOrderTypeSummary.equals("M"))) {
				System.out.println(t.lm_js_id
						+ "\t"
						+ t.PurchaseOrderTypeSummary
						+ "\t"
						+ t.TotalTimeOnsite
						+ "\t"
						+

						t.CU_LaborBuyAvgUnit
						+ "\t"
						+ t.SumAuthLaborQty
						+ "\t"
						+ t.TotalLaborCost
						+ "\t"
						+ t.CU_TravelBuyAvgUnit
						+ "\t"
						+ t.SumAuthTravelQty
						+ "\t"
						+ t.TotalTravelCost
						+ "\t"
						+ t.TotalCost
						+ "\t\t"
						+

						t.CU_LaborSellAvgUnit
						+ "\t"
						+ t.SumSellLaborQty
						+ "\t"
						+ t.TotalLaborPrice
						+ "\t"
						+ t.CU_TravelSellAvgUnit
						+ "\t"
						+ t.SumSellTravelQty
						+ "\t"
						+ t.TotalTravelPrice
						+ "\t"
						+ t.TotalPrice
						+ "\t\t"
						+

						t.CU_LaborBuyAvgUnit
						+ "\t"
						+ t.CU_LaborBuyQty
						+ "\t"
						+ t.CU_LaborBuyTotal
						+ "\t"
						+ t.CU_TravelBuyAvgUnit
						+ "\t"
						+ t.CU_TravelBuyQty
						+ "\t"
						+ t.CU_TravelBuyTotal
						+ "\t"
						+ t.CU_TotalCost
						+ "\t\t"
						+

						t.CU_LaborSellAvgUnit
						+ "\t"
						+ t.CU_LaborSellQty
						+ "\t"
						+ t.CU_LaborSellTotal
						+ "\t"
						+ t.CU_TravelSellAvgUnit
						+ "\t"
						+ t.CU_TravelSellQty
						+ "\t"
						+ t.CU_TravelSellTotal
						+ "\t"
						+ t.CU_TotalPrice
						+ "\t\t"
						+

						t.copperPrice(t.TotalTimeOnsite.intValue(),
								Double.valueOf(98.0D))
						+ "\t"
						+ t.copperMargin(t.CU_TotalCost, t.copperPrice(
								t.TotalTimeOnsite.intValue(),
								Double.valueOf(98.0D)))
						+ "\t"
						+ t.copperPrice(t.TotalTimeOnsite.intValue(),
								Double.valueOf(88.0D))
						+ "\t"
						+ t.copperMargin(t.CU_TotalCost, t.copperPrice(
								t.TotalTimeOnsite.intValue(),
								Double.valueOf(88.0D))));
			}
		}
	}

	void getSampleSet() {
		String sql = "select distinct lm_js_id   from lm_job   join lm_purchase_order on lm_js_id = lm_po_js_id   join lx_appendix_main on lm_js_pr_id = lx_pr_id and lx_pr_type = 3   join lm_ticket on lm_js_id = lm_tc_js_id and lm_tc_msp = 'N'   join lm_activity on lm_js_id = lm_at_js_id   join lm_resource on lm_at_id = lm_rs_at_id and lm_rs_type = 'IL' and lm_rs_iv_cns_part_number = '700100067'   join iv_resource_pool on lm_rs_rp_id = iv_rp_id   join lm_purchase_work_order on lm_rs_id = lm_pwo_rs_id   join lx_cost_element  on lm_rs_id = lx_ce_used_in and lx_ce_type = 'IL'  where lm_js_type not in ('Default', 'Addendum') and lm_js_create_date > '12/1/08' and lm_js_prj_status='O'   ";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				this.TicketList.add(rs.getString(1));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	public static void main(String[] args) {
		CopperAnalysis x = new CopperAnalysis();
	}
}
