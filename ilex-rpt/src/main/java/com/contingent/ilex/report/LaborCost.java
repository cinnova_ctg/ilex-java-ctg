package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.util.Vector;

public class LaborCost {
	DataAccess da = new DataAccess();
	Vector PurchaseOrders = new Vector();
	Double res_auth_sum = Double.valueOf(0.0D);
	Double res_qty_sum = Double.valueOf(0.0D);
	Double res_auth_avg = Double.valueOf(0.0D);

	LaborCost(String delta) {
		getPOData(delta);
	}

	void getPOData(String delta) {
		int i = 0;
		int j = 0;

		String last_PO = "";
		String current_PO = "";
		PurchaseOrder po = null;
		boolean one_trip = false;

		String sql = "exec su_fixed_price_labor_cost_01 " + delta;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				current_PO = rs.getString("lm_po_id");
				if (!last_PO.equals(current_PO)) {
					if (one_trip) {
						po.computeEstimatedCost();
						po.assignAuthorizedEstimate();
						this.PurchaseOrders.add(i++, po);
					}
					po = new PurchaseOrder(Double.valueOf(rs
							.getDouble("lm_po_authorised_total")), current_PO);
					last_PO = current_PO;
					one_trip = true;
				}
				po.addResourceData(rs.getString("lm_rs_iv_cns_part_number"),
						Double.valueOf(rs.getDouble("lm_rs_ce_cost")),
						Double.valueOf(rs.getDouble("lm_rs_quantity")));

				j++;
			}
			rs.close();

			po.computeEstimatedCost();
			po.assignAuthorizedEstimate();
			this.PurchaseOrders.add(i, po);
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	void getAverageLabor(String resource) {
		PurchaseOrder po = null;

		Double res_auth_unit_cost = Double.valueOf(0.0D);
		Double res_qty = Double.valueOf(0.0D);

		int j = 0;

		this.res_auth_sum = Double.valueOf(0.0D);
		this.res_qty_sum = Double.valueOf(0.0D);
		this.res_auth_avg = Double.valueOf(0.0D);
		for (int i = 0; i < this.PurchaseOrders.size(); i++) {
			po = (PurchaseOrder) this.PurchaseOrders.get(i);
			if (po.resources.contains(resource)) {
				res_auth_unit_cost = (Double) po.resourceAuthorizedCost
						.get(resource);
				res_qty = (Double) po.resourceQty.get(resource);
				if ((!res_auth_unit_cost.isNaN()) && (!res_qty.isNaN())) {
					this.res_auth_sum = Double.valueOf(this.res_auth_sum
							.doubleValue()
							+ res_auth_unit_cost.doubleValue()
							* res_qty.doubleValue());
					this.res_qty_sum = Double.valueOf(this.res_qty_sum
							.doubleValue() + res_qty.doubleValue());

					j++;
				}
			}
		}
		this.res_auth_avg = Double.valueOf(this.res_auth_sum.doubleValue()
				/ this.res_qty_sum.doubleValue());
	}

	public static void main(String[] args) {
		System.out.println("");
		System.out.println("Window: Last 1 Days");
		LaborCost x = new LaborCost("1");
		x.getAverageLabor("700000004");
		System.out.println("CFT2-C5 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");
		x.getAverageLabor("700100067");
		System.out.println("CFT2-C4 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");
		x.getAverageLabor("700100037");
		System.out.println("CFT2-C3 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");

		System.out.println("");
		System.out.println("Window: Last 7 Days");
		x = new LaborCost("7");
		x.getAverageLabor("700000004");
		System.out.println("CFT2-C5 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");
		x.getAverageLabor("700100067");
		System.out.println("CFT2-C4 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");
		x.getAverageLabor("700100037");
		System.out.println("CFT2-C3 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");

		System.out.println("");
		System.out.println("Window: Last 30 Days");
		x = new LaborCost("30");
		x.getAverageLabor("700000004");
		System.out.println("CFT2-C5 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");
		x.getAverageLabor("700100067");
		System.out.println("CFT2-C4 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");
		x.getAverageLabor("700100037");
		System.out.println("CFT2-C3 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");

		System.out.println("");
		System.out.println("Window: Last 90 Days");
		x = new LaborCost("90");
		x.getAverageLabor("700000004");
		System.out.println("CFT2-C5 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");
		x.getAverageLabor("700100067");
		System.out.println("CFT2-C4 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");
		x.getAverageLabor("700100037");
		System.out.println("CFT2-C3 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");
		System.out.println("");
		System.out.println("Window: Last 365 Days");

		x = new LaborCost("365");
		x.getAverageLabor("700000004");
		System.out.println("CFT2-C5 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");
		x.getAverageLabor("700100067");
		System.out.println("CFT2-C4 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");
		x.getAverageLabor("700100037");
		System.out.println("CFT2-C3 " + x.res_auth_avg + " over "
				+ x.res_qty_sum + " Hours");
	}
}
