package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class VGPVerification extends DispatchAction {
	DataAccess da = new DataAccess();

	public ActionForward vgp_verification1(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "vgp_verification1";

		managePageRequest(request);

		return mapping.findForward(forward_key);
	}

	void managePageRequest(HttpServletRequest request) {
		String project = checkProject(request);
		if (project.equals("")) {
			buildReport1(request);
		}
		buildRadio(request);

		buildDropDowns(request, project);
	}

	void buildRadio(HttpServletRequest request) {
		String view = request.getParameter("view") != null ? request
				.getParameter("view") : "both";

		String radio = "<input type=\"radio\" name=\"view\" value=\"unallocated\""
				+ (view.equals("unallocated") ? " checked" : "")
				+ ">Unallocated&nbsp;&nbsp;";
		radio = radio
				+ "<input type=\"radio\" name=\"view\" value=\"internal\""
				+ (view.equals("internal") ? " checked" : "")
				+ ">Internal PO&nbsp;&nbsp;";
		radio = radio + "<input type=\"radio\" name=\"view\" value=\"both\""
				+ (view.equals("both") ? " checked" : "") + ">Both";

		request.setAttribute("radio", radio);
	}

	void buildDropDowns(HttpServletRequest request, String project) {
		request.setAttribute(
				"customer",
				buildDropDown(
						"customer",
						request.getParameter("customer") != null ? request
								.getParameter("customer") : ""));
		request.setAttribute(
				"bdm",
				buildDropDown(
						"bdm",
						request.getParameter("bdm") != null ? request
								.getParameter("bdm") : ""));
		request.setAttribute(
				"spm",
				buildDropDown(
						"spm",
						request.getParameter("spm") != null ? request
								.getParameter("spm") : ""));
		if (!project.equals("")) {
			request.setAttribute(
					"owner",
					buildDropDown(
							"owner",
							request.getParameter("owner") != null ? request
									.getParameter("owner") : ""));
		}
	}

	String buildDropDown(String dd, String selection) {
		String sql = "";
		String dd_list = "";
		String dd_item = "";
		String checked = "";
		if (dd.equals("customer")) {
			sql = "select customer from vgp_verification_customer order by customer";
			dd_list = "  <option value=\"\">-- Select Customer --</option>\n";
		} else if (dd.equals("bdm")) {
			sql = "select bdm from vgp_verification_bdm order by bdm";
			dd_list = "  <option value=\"\">-- Select BDM --</option>\n";
		} else if (dd.equals("spm")) {
			sql = "select spm from vgp_verification_spm order by spm";
			dd_list = "  <option value=\"\">-- Select SPM --</option>\n";
		} else if (dd.equals("owner")) {
			sql = "select owner from vgp_verification_owner order by owner";
			dd_list = "  <option value=\"\">-- Select Job Owner --</option>\n";
		}
		System.out.println(sql);
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				dd_item = rs.getString(1);
				if (dd_item.equals(selection)) {
					checked = " selected";
				} else {
					checked = "";
				}
				dd_list = dd_list + "  <option value=\"" + dd_item + "\""
						+ checked + ">" + dd_item + "</option>\n";
			}
			dd_list = "<select name=" + dd + " class=\"menu\">\n" + dd_list
					+ "</select>\n";

			rs.close();
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
		return dd_list;
	}

	String checkProject(HttpServletRequest request) {
		String project = request.getParameter("project") != null ? request
				.getParameter("project") : "";
		if (!project.equals("")) {
			String customer = request.getParameter("customer") != null ? request
					.getParameter("customer") : "";
			String hcustomer = request.getParameter("hcustomer") != null ? request
					.getParameter("hcustomer") : "";
			String bdm = request.getParameter("bdm") != null ? request
					.getParameter("bdm") : "";
			String hbdm = request.getParameter("hbdm") != null ? request
					.getParameter("hbdm") : "";
			String spm = request.getParameter("spm") != null ? request
					.getParameter("spm") : "";
			String hspm = request.getParameter("hspm") != null ? request
					.getParameter("hspm") : "";
			if ((!customer.equals(hcustomer)) || (!bdm.equals(hbdm))
					|| (!spm.equals(hspm))) {
				project = "";
			}
		}
		return project;
	}

	void buildReport1(HttpServletRequest request) {
		String where_date_range = buildDateClause(request);
		String where_other = buildGeneralWhere(request);
		String where_cost = buildCostClause(request);
		String data = "";
		String project = "";

		int columns = 10;
		if (!where_other.equals("")) {
			where_date_range = where_date_range + " and " + where_other;
		}
		if (!where_cost.equals("")) {
			where_date_range = where_date_range + " and " + where_cost;
		}
		String sql = "select customer, project, lx_pr_id, convert(varchar(14),sum(unallocated_resource_sum),1), convert(varchar(14),sum(isnull(internal_po_delta, 0.0)),1), convert(decimal(5,3),1-(sum(estimated_cost)/sum(revenue))), convert(decimal(5,2),1-(sum(vgp_cost)/sum(revenue))), convert(decimal(5,2),1-(sum(vgp_cost+isnull(internal_po_delta,0.0)+unallocated_resource_sum)/sum(revenue))), convert(varchar(14),sum(revenue),1), convert(varchar(14), sum(revenue-(vgp_cost+isnull(internal_po_delta,0.0)+unallocated_resource_sum)), 1) from ap_vgp_verification "
				+

				where_date_range
				+ " "
				+ "group by customer, project, lx_pr_id "
				+ "having sum(revenue) > 0.0 "
				+ "order by  customer, project, lx_pr_id";

		System.out.println(sql);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				data = data + "<tr>";
				for (int i = 1; i <= columns; i++) {
					switch (i) {
					case 1:
						data = data + "<td><div class=\"cDataLeft0\">"
								+ rs.getString(i) + "</div></td>";
						break;
					case 2:
						data = data + "<td><div class=\"cDataLeft0\">"
								+ rs.getString(i) + "</div></td>";
						break;
					case 4:
					case 5:
					case 9:
					case 10:
						data = data + "<td><div class=\"cDataRight0\">$"
								+ rs.getString(i) + "</div></td>";
						break;
					case 6:
					case 7:
					case 8:
						data = data + "<td><div class=\"cDataRight0\">"
								+ rs.getString(i) + "</div></td>";
					}
				}
				data = data + "</tr>\n";
			}
			rs.close();
			if (data.equals("")) {
				data = data
						+ "<tr><td><div class=\"\">Sorry.  The criteria you selected did not match any data.</div></td></tr>\n";
			} else {
				data =

				"<tr><td><div class=\"columnHeader1\">Customer</div></td><td><div class=\"columnHeader1\">Project</div></td><td><div class=\"columnHeader1\">Unallocated</div></td><td><div class=\"columnHeader1\">Internal PO</div></td><td><div class=\"columnHeader1\">Pro Forma</div></td><td><div class=\"columnHeader1\">VGPM</div></td><td><div class=\"columnHeader1\">Corrected VGPM</div></td><td><div class=\"columnHeader1\">Revenue</div></td><td><div class=\"columnHeader1\">Corrected VGP</div></td></tr>\n"
						+ data;

				buildSummary(request, where_date_range);
			}
			request.setAttribute("data", data);
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
	}

	void buildSummary(HttpServletRequest request, String where) {
		String sql = "select count(*), convert(varchar(14), sum(vgp_cost), 1), convert(varchar(14), sum(unallocated_resource_sum),1), convert(varchar(14), sum(isnull(internal_po_delta, 0.0)),1), convert(varchar(14), sum(vgp_cost+isnull(internal_po_delta,0.0)+unallocated_resource_sum), 1) from ap_vgp_verification "
				+

				where;
		String summary = "";

		System.out.println(sql);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				summary =

				"<tr><td><div class=\"columnHeader3\">" + rs.getString(1)
						+ "</div></td>" + "<td><div class=\"columnHeader3\">$"
						+ rs.getString(2) + "</div></td>"
						+ "<td><div class=\"columnHeader3\">$"
						+ rs.getString(3) + "</div></td>"
						+ "<td><div class=\"columnHeader3\">$"
						+ rs.getString(4) + "</div></td>"
						+ "<td><div class=\"columnHeader3\">$"
						+ rs.getString(5) + "</div></td>" + "</tr>\n";
			}
			request.setAttribute("summary", summary);
			rs.close();
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
	}

	String buildDateClause(HttpServletRequest request) {
		String from_date = request.getParameter("from_date") != null ? request
				.getParameter("from_date") : "";
		String to_date = request.getParameter("to_date") != null ? request
				.getParameter("to_date") : "";
		if (from_date.equals("")) {
			from_date = "04/01/2009";
			request.setAttribute("from_date", from_date);
		}
		if (to_date.equals("")) {
			to_date = "6/30/2009";
			request.setAttribute("to_date", to_date);
		}
		String where = "where (closed_date between '" + from_date + "' and '"
				+ to_date + " 23:59')";

		return where;
	}

	String buildGeneralWhere(HttpServletRequest request) {
		String delimiter = "";
		String where = "";

		String customer = request.getParameter("customer") != null ? request
				.getParameter("customer") : "";
		if (!customer.equals("")) {
			where = "customer = '" + customer + "'";
			delimiter = " and ";
		}
		String bdm = request.getParameter("bdm") != null ? request
				.getParameter("bdm") : "";
		if (!bdm.equals("")) {
			where = where + delimiter + "bdm = '" + bdm + "'";
			delimiter = " and ";
		}
		String spm = request.getParameter("spm") != null ? request
				.getParameter("spm") : "";
		if (!spm.equals("")) {
			where = where + delimiter + "spm = '" + spm + "'";
		}
		return where;
	}

	String buildCostClause(HttpServletRequest request) {
		String view = request.getParameter("view") != null ? request
				.getParameter("view") : "";
		String where = "";
		if (view.equals("unallocated")) {
			where = "unallocated_resource_sum > 0.0";
		} else if (view.equals("internal")) {
			where = "isnull(internal_po_delta, 0.0) != 0.0";
		}
		return where;
	}

	public static void main(String[] args) {
	}
}
