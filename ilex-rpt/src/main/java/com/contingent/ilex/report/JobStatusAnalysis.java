package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class JobStatusAnalysis extends DispatchAction {
	DataAccess da = new DataAccess();
	String PageLevel = "1";
	Vector sortOrder = new Vector();
	Map sortDirection = new HashMap();
	Vector columnHeaders = new Vector();
	Vector pageTitle = new Vector();
	String[] Select = new String[20];
	String[] Count = new String[20];
	Vector ViewColumnList = new Vector();
	String[] IncludeFileNames = new String[18];
	String[] xmlURL = new String[18];
	String WhereClause = "";
	String[] Subtitle = new String[20];

	public ActionForward jsa_summary(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "jsa_summary";

		managePageRequest1(request);

		return mapping.findForward(forward_key);
	}

	String managePageRequest1(HttpServletRequest request) {
		String x = "";

		initializeSelect();

		Map inputs = getInputParameters(request);

		int view = 0;

		this.WhereClause = "";

		this.WhereClause = buildWhereClause(inputs);

		jsaSummaryDataAll(request, this.WhereClause);
		if (inputs.get("view") != null) {
			view = Integer.parseInt((String) inputs.get("view"));

			request.setAttribute("Subtitle", this.Subtitle[view]);
			request.setAttribute("view", Integer.valueOf(view));
		}
		return x;
	}

	void jsaSummaryDataAll(HttpServletRequest request, String wc) {
		for (int i = 0; i < this.Select.length; i++) {
			String sql = "select count(*) from ap_job_audit_analysis where "
					+ this.Select[i] + wc;

			ResultSet rs = this.da.getResultSet(sql);
			try {
				if (rs.next()) {
					this.Count[i] = rs.getString(1);
				}
				rs.close();
			} catch (SQLException e) {
				System.out.println(e + " " + sql);
			}
		}
		request.setAttribute("count", this.Count);
	}

	public ActionForward audit_view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "audit_view";

		managePageRequest2(request);

		return mapping.findForward(forward_key);
	}

	String managePageRequest2(HttpServletRequest request) {
		String x = "";
		int view = 0;

		initializeSelect();
		initializeColumnList();
		initializeXMLRequestParameters();

		Map inputs = getInputParameters(request);

		view = Integer.parseInt((String) inputs.get("view"));

		this.WhereClause = "";

		this.WhereClause = buildWhereClause(inputs);

		getXMLOutput(request, this.WhereClause, view);

		request.setAttribute("", this.IncludeFileNames[view]);
		request.setAttribute("", this.xmlURL[view]);

		return x;
	}

	void getXMLOutput(HttpServletRequest request, String wc, int view) {
		Vector columns = buildColumnVector(view, 0);
		Vector elements = buildColumnVector(view, 1);

		String select_columns = buildColumnList(columns);
		String column = "";
		String element = "";

		String xml_output = "";
		int z = 0;
		String sql = "select top 21 "
				+ select_columns
				+ " from ap_job_audit_analysis join lm_job on ap_aa_js_id = lm_js_id where "
				+ this.Select[view] + wc;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				xml_output = xml_output + "<job>\n";
				xml_output = xml_output + "<jobattributes>\n";
				for (int i = 0; i < columns.size(); i++) {
					column = rs.getString(i + 1);
					if (rs.wasNull()) {
						column = "";
					}
					element = (String) elements.get(i);
					element = element.replaceAll("&", "&amp;");
					xml_output = xml_output + "<" + element + ">" + column
							+ "</" + element + ">\n";
				}
				xml_output = xml_output + "</jobattributes>\n";
				xml_output = xml_output + "</job>\n";
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
		request.setAttribute("xml_output", xml_output);
	}

	Vector buildColumnVector(int view, int option) {
		Vector column_lists = new Vector();
		String[] temp = new String[2];

		Vector cl = (Vector) this.ViewColumnList.get(view);
		for (int i = 0; i < cl.size(); i++) {
			temp = ((String) cl.get(i)).split(" as ");
			column_lists.add(i, temp[option]);
		}
		return column_lists;
	}

	String buildColumnList(Vector cl) {
		String select_columns = "";
		String delimiter = "";
		for (int i = 0; i < cl.size(); i++) {
			select_columns = select_columns + delimiter + (String) cl.get(i);
			delimiter = ", ";
		}
		return select_columns;
	}

	public ActionForward inline_audit_view(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "audit_view";

		managePageRequest3(request);

		return mapping.findForward(forward_key);
	}

	String managePageRequest3(HttpServletRequest request) {
		String x = "";

		initializeSelect();

		Map inputs = getInputParameters(request);

		int view = Integer.parseInt((String) inputs.get("view"));

		this.WhereClause = "";

		this.WhereClause = buildWhereClause(inputs);

		getFlatData(request, this.WhereClause, view);

		request.setAttribute("Subtitle", this.Subtitle[view]);
		request.setAttribute("view", Integer.valueOf(view));

		return x;
	}

	void getFlatData(HttpServletRequest request, String wc, int view) {
		String sql = "select left(ap_aa_customer,20), left(ap_aa_project,22) as project, left(lm_js_title, 28), convert(numeric(6,2),  ap_aa_status_delta/(24*60)), convert (char(8),ap_aa_js_date,1), left(ap_aa_owner,16), ap_aa_pm_name,  case when ap_aa_install_notes=1 then 'I' else ''end+case when ap_aa_customer_reference=1 then 'C' else ''end+case when ap_aa_deliverables=1 then 'D' else ''end  from ap_job_audit_analysis join lm_job on ap_aa_js_id=lm_js_id where ";

		sql = sql + this.Select[view] + wc
				+ " order by ap_aa_customer, ap_aa_js_date";

		Vector rows = new Vector();
		String[] row = new String[8];

		int i = 0;
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				row = new String[8];

				row[0] = rs.getString(1);
				row[1] = rs.getString(2);
				row[2] = rs.getString(3);
				row[3] = rs.getString(4);
				row[4] = rs.getString(5);
				row[5] = rs.getString(6);
				row[6] = rs.getString(7);
				row[7] = rs.getString(8);

				rows.add(i++, row);
			}
			rs.close();

			request.setAttribute("rows", rows);
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
	}

	String buildWhereClause(Map inputs) {
		String wc = "";
		String delimiter = "";

		Vector expressions = new Vector();
		int i = 0;

		boolean lx_pr_id = inputs.get("lx_pr_id") != null;
		boolean pm = inputs.get("pm") != null;
		boolean bdm = inputs.get("bdm") != null;
		boolean from_date = (inputs.get("from_date") != null)
				&& (!((String) inputs.get("from_date")).equals(""));
		boolean to_date = (inputs.get("to_date") != null)
				&& (!((String) inputs.get("to_date")).equals(""));
		boolean view = inputs.get("view") != null;
		boolean jo = inputs.get("jo") != null;
		boolean sl = inputs.get("sl") != null;
		if (lx_pr_id) {
			expressions.add(i++,
					"ap_aa_pr_id = " + (String) inputs.get("lx_pr_id"));
		}
		if (pm) {
			expressions.add(i++, "ap_aa_pm_id = " + (String) inputs.get("pm"));
		}
		if (bdm) {
			expressions
					.add(i++, "ap_aa_bdm_id = " + (String) inputs.get("bdm"));
		}
		if (from_date) {
			expressions.add(i++,
					"ap_aa_js_date >= '" + (String) inputs.get("from_date")
							+ "'");
		}
		if (to_date) {
			expressions.add(i++, "ap_aa_js_date > dateadd(dd,1,'"
					+ (String) inputs.get("to_date") + "')");
		}
		if (jo) {
			expressions.add(i++,
					"ap_aa_owner_id = " + (String) inputs.get("jo"));
		}
		sl = true;
		if (sl) {
			expressions.add(i++, "ap_aa_js_date > dateadd(mm,-12,getdate())");
		}
		wc = " ";
		delimiter = " and ";
		for (int j = 0; j < expressions.size(); j++) {
			wc = wc + delimiter + expressions.get(j);
		}
		return wc;
	}

	Map getInputParameters(HttpServletRequest request) {
		Vector requestParameters = initializeInputParameters();

		Map inputs = new HashMap();
		for (int i = 0; i < requestParameters.size(); i++) {
			String parameter = (String) requestParameters.get(i);
			String value = request.getParameter(parameter);
			if ((value != null) && (value.trim().length() > 0)) {
				inputs.put(parameter, value);
				request.setAttribute(parameter, value);
			}
		}
		request.setAttribute("requestParameters", requestParameters);

		return inputs;
	}

	Vector initializeInputParameters() {
		Vector inputParameters = new Vector();

		inputParameters.clear();
		inputParameters.add(0, "lx_pr_id");
		inputParameters.add(1, "bdm");
		inputParameters.add(2, "pm");
		inputParameters.add(3, "from_date");
		inputParameters.add(4, "to_date");
		inputParameters.add(5, "view");
		inputParameters.add(6, "jo");

		return inputParameters;
	}

	void initializeReport() {
		String[] c1 = { "Created", "Scheduled" };
		this.columnHeaders.add(0, c1);
		String[] c2 = { "Created", "Scheduled" };
		this.columnHeaders.add(1, c2);
		String[] c3 = { "On-site", "Scheduled End" };
		this.columnHeaders.add(3, c3);
		String[] c4 = { "On-site", "Off-Site" };
		this.columnHeaders.add(4, c4);
		String[] c5 = { "Off-Site", "Complete" };
		this.columnHeaders.add(5, c5);

		this.pageTitle.add(0, "To Be Scheduled");
		this.pageTitle.add(1, "Scheduled");
		this.pageTitle.add(2, "In work");
		this.pageTitle.add(3, "Complete Lag (Off-Site But Not Complete)");
		this.pageTitle.add(4, "Close Lag (Complete But Not Closed)");
	}

	void initializeSelect() {
		this.Select[0] = "ap_aa_status = 'To Be Scheduled'";
		this.Select[1] = "ap_aa_status = 'To Be Scheduled Over-1'";

		this.Select[2] = "ap_aa_status = 'Scheduled'";
		this.Select[3] = "ap_aa_status = 'Scheduled Over-1'";

		this.Select[4] = "ap_aa_status = 'Onsite'";
		this.Select[5] = "ap_aa_status = 'Onsite Over-1'";
		this.Select[6] = "ap_aa_status = 'Offsite'";
		this.Select[7] = "ap_aa_status = 'Offsite Over-2'";
		this.Select[8] = "ap_aa_status = 'Offsite Over/Del-7'";

		this.Select[9] = "ap_aa_status = 'Complete'";
		this.Select[10] = "ap_aa_status like 'Complete%' and (ap_aa_deliverables = 1 or ap_aa_install_notes = 1 or ap_aa_customer_reference = 1 or ap_aa_cost = 0.0)";
		this.Select[11] = "ap_aa_status = 'Complete Over-5'";
		this.Select[12] = "ap_aa_status like 'Complete%' and  ap_aa_reconciled = 0";

		this.Select[13] = "ap_aa_status = 'Closed-NR'";

		this.Select[14] = "ap_aa_status like 'To Be Scheduled%'";
		this.Select[15] = "ap_aa_status like 'Scheduled%'";
		this.Select[16] = "(ap_aa_status like 'Onsite%' or ap_aa_status like 'Offsite%' or ap_aa_status='Confirmed')";
		this.Select[17] = "ap_aa_status like 'Complete%'";

		this.Select[18] = "ap_aa_status != 'Closed-NR'";

		this.Select[19] = "ap_aa_status = 'Confirmed'";

		this.Subtitle[0] = "To Be Scheduled OK";
		this.Subtitle[1] = "To Be Scheduled Over-1";
		this.Subtitle[2] = "Scheduled OK";
		this.Subtitle[3] = "Scheduled Over-1";
		this.Subtitle[4] = "Onsite OK";
		this.Subtitle[5] = "Onsite Over-1";
		this.Subtitle[6] = "Offsite OK";
		this.Subtitle[7] = "Offsite Over-2";
		this.Subtitle[8] = "Offsite Del>7";
		this.Subtitle[9] = "Complete OK (I=Installation Notes, C=Customer Reference, and D=Deliverables)";
		this.Subtitle[10] = "Complete Problems (I=Installation Notes, C=Customer Reference, and D=Deliverables)";
		this.Subtitle[11] = "Complete Over-5 (I=Installation Notes, C=Customer Reference, and D=Deliverables)";
		this.Subtitle[12] = "Complete NR";
		this.Subtitle[13] = "Closed";

		this.Subtitle[19] = "Confirmed";
	}

	void initializeColumnList() {
		Vector columns = new Vector();
		columns.add(0, "ap_aa_customer as customer");
		columns.add(1, "ap_aa_project as project");
		columns.add(2, "lm_js_title as job_title");
		columns.add(3,
				"convert(numeric(6,2), ap_aa_status_delta/(24*60)) as delta");
		columns.add(4, "convert (char(8),ap_aa_js_date,1) as date");
		columns.add(5, "ap_aa_owner as owner");
		columns.add(6, "ap_aa_pm_name as pm");
		this.ViewColumnList.add(0, columns);

		columns = new Vector();
		columns.add(0, "ap_aa_customer as customer");
		columns.add(1, "ap_aa_project as project");
		columns.add(2, "lm_js_title as job_title");
		columns.add(3,
				"convert(numeric(6,2), ap_aa_status_delta/(24*60)) as delta");
		columns.add(4, "convert (char(8),ap_aa_js_date,1) as date");
		columns.add(5, "ap_aa_owner as owner");
		columns.add(6, "ap_aa_pm_name as pm");
		this.ViewColumnList.add(1, columns);

		columns = new Vector();
		columns.add(0, "ap_aa_customer as customer");
		columns.add(1, "ap_aa_project as project");
		columns.add(2, "lm_js_title as job_title");
		columns.add(3,
				"convert(numeric(6,2), ap_aa_status_delta/(24*60)) as delta");
		columns.add(4, "convert (char(8),ap_aa_js_date,1) as date");
		columns.add(5, "ap_aa_owner as owner");
		columns.add(6, "ap_aa_pm_name as pm");
		this.ViewColumnList.add(2, columns);

		columns = new Vector();
		columns.add(0, "ap_aa_customer as customer");
		columns.add(1, "ap_aa_project as project");
		columns.add(2, "lm_js_title as job_title");
		columns.add(3,
				"convert(numeric(6,2), ap_aa_status_delta/(24*60)) as delta");
		columns.add(4, "convert (char(8),ap_aa_js_date,1) as date");
		columns.add(5, "ap_aa_owner as owner");
		columns.add(6, "ap_aa_pm_name as pm");
		this.ViewColumnList.add(3, columns);

		columns = new Vector();
		columns.add(0, "ap_aa_customer as customer");
		columns.add(1, "ap_aa_project as project");
		columns.add(2, "lm_js_title as job_title");
		columns.add(3,
				"convert(numeric(6,2), ap_aa_status_delta/(24*60)) as delta");
		columns.add(4, "convert (char(8),ap_aa_js_date,1) as date");
		columns.add(5, "ap_aa_owner as owner");
		columns.add(6, "ap_aa_pm_name as pm");
		this.ViewColumnList.add(4, columns);
	}

	void initializeXMLRequestParameters() {
		this.IncludeFileNames[0] = "tbs.txt";
		this.xmlURL[0] = "JSA.xo?form_action=audit_view&view=0";
	}
}
