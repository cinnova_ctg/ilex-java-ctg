package com.contingent.ilex.report;

import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailNotification {

	Session session;

	EmailNotification() {

		Properties props = System.getProperties();
		Authenticator auth = new SMTPAuthenticator();

		props.put("mail.smtp.host", "smtp2.donet.com");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.localhost", "contingent.net");

		try {
			session = Session.getDefaultInstance(props, auth);
			session.setDebug(false);
		} catch (Exception e) {
			System.out.println("Session Error: " + e);
		}

	}

	boolean sendNotification(Map<String, String> email_details) {
		boolean rtn_code = true;

		String from = email_details.get("from");
		String to = email_details.get("to");
		String cc = email_details.get("cc");
		String subject = email_details.get("subject");
		String msg = email_details.get("message");
		String content_type = email_details.get("content_type");

		// In general - one to address but multiple cc
		String[] cc_list = cc.split(",");

		try {
			MimeMessage message = new MimeMessage(session);

			// Set message content
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			for (int i = 0; i < cc_list.length; i++) {
				message.addRecipient(Message.RecipientType.CC,
						new InternetAddress(cc_list[i]));
			}
			// message.addRecipient(Message.RecipientType.BCC, new
			// InternetAddress("cmurray@contingent.net"));
			message.setSubject(subject);
			message.setContent(msg, content_type);

			Transport.send(message);

		} catch (Exception e) {
			rtn_code = false;
			System.out.println("sendNotification Error: " + e);
		}

		return rtn_code;
	}

	private class SMTPAuthenticator extends javax.mail.Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {

			// Donet Connection
			String username = "ilex.cn";
			String password = "Rocket01";

			// Exchange Connection
			// String username = "ilex";
			// String password = "Rocket01";

			return new PasswordAuthentication(username, password);
		}
	}

}
