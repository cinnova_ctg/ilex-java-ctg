package com.contingent.ilex.report;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class AccountingExpense extends DispatchAction {
	Vector<String[]> InputParameters = new Vector();
	String[] ReportData = { "mm01yyyy", "cnt" };
	Map<String, String> JobTypeMap = new HashMap();
	Vector<String[]> ReportRows = new Vector();
	DataAccess da = new DataAccess();
	static NumberFormat currency = NumberFormat.getCurrencyInstance();
	static NumberFormat percentage = NumberFormat.getPercentInstance();
	static NumberFormat number = new DecimalFormat("###,###,###");
	static NumberFormat vgpm_format = new DecimalFormat("0.00");
	SimpleDateFormat standard_date1 = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat standard_date3 = new SimpleDateFormat("MM/dd/yyyy");
	SimpleDateFormat standard_date2 = new SimpleDateFormat("MM/yyyy");
	SimpleDateFormat standard_mm = new SimpleDateFormat("MM");
	SimpleDateFormat standard_yyyy = new SimpleDateFormat("yyyy");
	static String[] LastDayOfMonth = { "31", "28", "31", "30", "31", "30",
			"31", "31", "30", "31", "30", "31" };

	public ActionForward ttm(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "error";

		String[] grouping = { "convert(varchar(8), ac_jd_date_closed, 121)+'01'" };

		String[] columns = { "month", "ac_ru_pro_forma", "ac_jb_labor",
				"ac_jb_materials", "ac_jb_travel", "ac_jb_freight",
				"ac_jb_rentals", "other_expense",

				"ac_ru_expense", "ac_ru_revenue", "pf_vgpm", "act_vgpm", "cnt",
				"ac_gp_amount" };

		String from_date = "'3/1/2010'";
		String to_date = "'6/30/2011'";
		if (getReportData(request, buildSQL_TTM(from_date, to_date, request),
				columns)) {
			forward_key = "no_inputs";
		}
		setSubTitles(request);

		forward_key = "ttm";

		return mapping.findForward(forward_key);
	}

	public ActionForward month(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "error";

		String[] columns = { "customer", "ac_ru_pro_forma", "ac_jb_labor",
				"ac_jb_materials", "ac_jb_travel", "ac_jb_freight",
				"ac_jb_rentals", "other_expense", "ac_ru_expense",
				"ac_ru_revenue", "pf_vgpm", "act_vgpm", "cnt" };

		String[] from_to = getFromToDates(request);

		request.setAttribute("title", "Expense Breakout by Account for "
				+ from_to[2]);
		request.setAttribute("month", request.getParameter("month"));
		request.setAttribute("year", request.getParameter("year"));
		if (getReportDataMonth(from_to[0], request,
				buildSQL_MONTH(from_to[0], from_to[1], request), columns)) {
			forward_key = "month";
		}
		setSubTitles(request);

		return mapping.findForward(forward_key);
	}

	String[] getFromToDates(HttpServletRequest request) {
		String[] fr = new String[3];
		Calendar calendar = Calendar.getInstance();

		String mm = request.getParameter("month");
		String yyyy = request.getParameter("year");

		int year = Integer.parseInt(yyyy);
		int month = Integer.parseInt(mm) - 1;
		calendar.set(year, month, 1);
		int last_day = calendar.getActualMaximum(5);

		fr[0] = ("'" + mm + "/01/" + yyyy + "'");
		fr[1] = ("'" + mm + "/" + String.valueOf(last_day) + "/" + yyyy + "'");
		fr[2] = (mm + "/" + yyyy);

		return fr;
	}

	public ActionForward account(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "error";

		String[] columns = { "customer", "po_number", "ac_pe_pro_forma_cost",
				"ac_pb_labor", "ac_pb_materials", "ac_pb_travel",
				"ac_pb_freight", "ac_pb_rentals", "ac_pe_best_cost" };

		String[] from_to = getFromToDates(request);
		String account = request.getParameter("account");
		request.setAttribute("title", "Expense Breakout for " + account
				+ " during " + from_to[2]);
		request.setAttribute("month", request.getParameter("month"));
		request.setAttribute("year", request.getParameter("year"));
		request.setAttribute("account", request.getParameter("account"));
		if (getReportDataPO(request,
				buildSQL_PO(from_to[0], from_to[1], request), columns)) {
			forward_key = "account";
		}
		setSubTitles(request);

		return mapping.findForward(forward_key);
	}

	String buildSQL_TTM(String from_date, String to_date,
			HttpServletRequest request) {
		String sql = "";

		String where = "where ac_jd_date_closed between ";

		String w1 = from_date + " and " + to_date;
		where = where + w1;

		String w2 = buildJobTypes(request);
		where = where + w2;

		sql = "use datawarehouse;select convert(varchar(8), ac_jd_date_closed, 121)+'01' as month,  sum(isnull(ac_ru_pro_forma,0.0)) as ac_ru_pro_forma, sum(isnull(ac_jb_labor,0.0)) as ac_jb_labor,  sum(isnull(ac_jb_materials,0.0)) as ac_jb_materials,  sum(isnull(ac_jb_travel,0.0)) as ac_jb_travel,  sum(isnull(ac_jb_freight,0.0)) as ac_jb_freight,  sum(isnull(ac_jb_rentals,0.0))as ac_jb_rentals, sum(isnull(ac_ur_expense,0.0))+sum(isnull(ac_uo_expense,0.0)) as other_expense, sum(isnull(ac_ru_expense,0.0)) as ac_ru_expense, sum(isnull(ac_ru_revenue,0.0)) as ac_ru_revenue, case when sum(isnull(ac_ru_revenue,0.0)) > 0.0 then 1-(sum(isnull(ac_ru_pro_forma,0.0))/sum(isnull(ac_ru_revenue,0.0))) else 0.0 end as pf_vgpm, case when sum(isnull(ac_ru_revenue,0.0)) > 0.0 then 1-(sum(isnull(ac_ru_expense,0.0))/sum(isnull(ac_ru_revenue,0.0))) else 0.0 end as act_vgpm, count(*) as cnt, sum(isnull(ac_gp_amount,0.0)) as ac_gp_amount from dbo.ac_dimensions_job join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id left join dbo.ac_fact_job_expense_breakout on ac_ru_js_id = ac_jb_jd_js_id join dbo.ac_dimension_job_classification on  ac_jd_js_id = ac_cl_jd_js_id left join dbo.ac_fact_unallocated_expense on ac_jd_js_id = ac_ur_jd_js_id left join dbo.ac_fact_unallocated_overhead_expense on ac_jd_js_id =  ac_uo_jd_js_id left join dbo.ac_fact_gp_expense_export2 on ac_jd_js_id =  ac_gp_lm_js_id "
				+

				where
				+ " "
				+ "group by convert(varchar(8), ac_jd_date_closed, 121)+'01' "
				+ "order by convert(varchar(8), ac_jd_date_closed, 121)+'01' desc";

		System.out.println(sql);

		return sql;
	}

	String buildSQL_MONTH(String from_date, String to_date,
			HttpServletRequest request) {
		String sql = "";

		String where = "where ac_jd_date_closed between ";

		String w1 = from_date + " and " + to_date;
		where = where + w1;

		String w2 = buildJobTypes(request);
		where = where + w2;

		sql = "use datawarehouse; select ac_pd_lo_ot_name as customer, sum(isnull(ac_ru_pro_forma,0.0)) as ac_ru_pro_forma, sum(isnull(ac_jb_labor,0.0)) as ac_jb_labor,  sum(isnull(ac_jb_materials,0.0)) as ac_jb_materials,  sum(isnull(ac_jb_travel,0.0)) as ac_jb_travel,  sum(isnull(ac_jb_freight,0.0)) as ac_jb_freight,  sum(isnull(ac_jb_rentals,0.0))as ac_jb_rentals, sum(isnull(ac_ur_expense,0.0))+sum(isnull(ac_uo_expense,0.0)) as other_expense, sum(isnull(ac_ru_expense,0.0)) as ac_ru_expense, sum(isnull(ac_ru_revenue,0.0)) as ac_ru_revenue, case when sum(isnull(ac_ru_revenue,0.0)) > 0.0 then 1-(sum(isnull(ac_ru_pro_forma,0.0))/sum(isnull(ac_ru_revenue,0.0))) else 0.0 end as pf_vgpm, case when sum(isnull(ac_ru_revenue,0.0)) > 0.0 then 1-(sum(isnull(ac_ru_expense,0.0))/sum(isnull(ac_ru_revenue,0.0))) else 0.0 end as act_vgpm, count(*) as cnt from   dbo.ac_dimensions_project        join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id       join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id left        join dbo.ac_dimension_job_classification on  ac_jd_js_id = ac_cl_jd_js_id        join dbo.ac_fact_job_expense_breakout on ac_ru_js_id = ac_jb_jd_js_id left join dbo.ac_fact_unallocated_expense on ac_jd_js_id = ac_ur_jd_js_id left join dbo.ac_fact_unallocated_overhead_expense on ac_jd_js_id =  ac_uo_jd_js_id "
				+

				where
				+ " "
				+ "group by ac_pd_lo_ot_name "
				+ "order by ac_pd_lo_ot_name";

		return sql;
	}

	String buildSQL_PO(String from_date, String to_date,
			HttpServletRequest request) {
		String sql = "";

		String where = "where ac_jd_date_closed between ";

		String w1 = from_date + " and " + to_date;
		where = where + w1;

		String w2 = buildJobTypes(request);
		where = where + w2;

		where = where + " and ac_pd_lo_ot_name = '"
				+ request.getParameter("account") + "'";

		sql = "use datawarehouse;select ac_pd_lo_ot_name as customer,  ac_pb_od_po_id as po_number, isnull(ac_pb_labor ,0.0) as ac_pb_labor,  isnull(ac_pb_materials ,0.0) as ac_pb_materials, isnull(ac_pb_travel ,0.0) as ac_pb_travel,  isnull(ac_pb_freight ,0.0) as ac_pb_freight,  isnull(ac_pb_rentals ,0.0) as  ac_pb_rentals, isnull(ac_pe_pro_forma_cost,0.0) as ac_pe_pro_forma_cost, isnull(ac_pe_best_cost ,0.0) as ac_pe_best_cost from dbo.ac_dimensions_project  join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id  join dbo.ac_dimension_job_classification on  ac_jd_js_id = ac_cl_jd_js_id join dbo.ac_dimensions_purchase_order on ac_jd_js_id = ac_od_jd_js_id  join dbo.ac_fact_purchase_order_expense_breakout on ac_od_po_id = ac_pb_od_po_id  join dbo.ac_fact_purchase_order_expense on ac_pb_od_po_id = ac_pe_od_po_id   "
				+

				where + " " + "order by ac_pb_od_po_id ";

		return sql;
	}

	String buildWhereClause() {
		String where_clause = "";

		return where_clause;
	}

	String getRequestParameters() {
		String where_clause = "";

		return where_clause;
	}

	boolean getReportData(HttpServletRequest request, String sql,
			String[] columns) {
		boolean rc = false;

		String mm = "";
		String yyyy = "";

		Vector<String[]> rows = new Vector();

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[columns.length];
				for (int i = 0; i < columns.length; i++) {
					row[i] = rs.getString(columns[i]);
					switch (i) {
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 12:
					case 13:
						row[i] = number.format(Double.parseDouble(row[i]));
						break;
					case 10:
					case 11:
						row[i] = vgpm_format.format(Double.parseDouble(row[i]));
						break;
					case 0:
						try {
							mm = this.standard_mm.format(this.standard_date1
									.parse(row[i]));
							yyyy = this.standard_yyyy
									.format(this.standard_date1.parse(row[i]));

							getJobTypes(request);
							row[i] = this.standard_date2
									.format(this.standard_date1.parse(row[i]));
							row[i] = ("&nbsp;<a href=\"ACCTEXP.xo?ac=month&month="
									+ mm
									+ "&year="
									+ yyyy
									+ "&job_types_link="
									+ getJobTypes(request) + "\">" + row[i] + "</a>");
						} catch (Exception localException) {
						}
					}
				}
				rows.add(row);
			}
			rs.close();
			request.setAttribute("rows", rows);
			rc = true;
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
			request.setAttribute("error", e.getMessage());
		}
		return rc;
	}

	boolean getReportDataMonth(String from_date, HttpServletRequest request,
			String sql, String[] columns) {
		boolean rc = false;

		String mm = "";
		String yyyy = "";
		String account = "";

		Vector<String[]> rows = new Vector();

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[columns.length];
				for (int i = 0; i < columns.length; i++) {
					row[i] = rs.getString(columns[i]);
					switch (i) {
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 12:
						row[i] = number.format(Double.parseDouble(row[i]));
						break;
					case 10:
					case 11:
						row[i] = vgpm_format.format(Double.parseDouble(row[i]));
						break;
					case 0:
						try {
							account = URLEncoder.encode(row[i], "UTF-8");
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}
						try {
							mm = request.getParameter("month");
							yyyy = request.getParameter("year");
							row[i] = ("&nbsp;<a href=\"ACCTEXP.xo?ac=account&account="
									+ account
									+ "&month="
									+ mm
									+ "&year="
									+ yyyy
									+ "&job_types_link="
									+ getJobTypes(request) + "\">" + row[i] + "</a>");
						} catch (Exception localException) {
						}
					}
				}
				rows.add(row);
			}
			rs.close();
			request.setAttribute("rows", rows);
			rc = true;
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
			request.setAttribute("error", e.getMessage());
		}
		return rc;
	}

	boolean getReportDataPO(HttpServletRequest request, String sql,
			String[] columns) {
		boolean rc = false;

		Vector<String[]> rows = new Vector();

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[columns.length];
				for (int i = 0; i < columns.length; i++) {
					row[i] = rs.getString(columns[i]);
					switch (i) {
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 12:
						row[i] = number.format(Double.parseDouble(row[i]));
					}
				}
				rows.add(row);
			}
			rs.close();
			request.setAttribute("rows", rows);
			rc = true;
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
			request.setAttribute("error", e.getMessage());
		}
		return rc;
	}

	String buildJobTypes(HttpServletRequest request) {
		String job_type_list = "";

		String job_types_link = request.getParameter("job_types_link");
		String[] job_types;
		// String[] job_types;
		if (job_types_link != null) {
			job_types = job_types_link.split(",");
		} else {
			job_types = request.getParameterValues("job_types");
		}
		mapJobTypesSelections();
		if ((job_types != null) && (job_types.length > 0)
				&& (job_types[0].length() > 0)) {
			for (int i = 0; i < job_types.length - 1; i++) {
				job_type_list = job_type_list
						+ (String) this.JobTypeMap.get(job_types[i]) + " or ";
			}
			job_type_list = job_type_list
					+ (String) this.JobTypeMap
							.get(job_types[(job_types.length - 1)]);
			job_type_list = " and (" + job_type_list + ") ";
		} else {
			job_type_list = buildJobTypesDefaults();
		}
		this.JobTypeMap.clear();

		return job_type_list;
	}

	String buildJobTypesDefaults() {
		String defaults = "";

		defaults = " and ((ac_cl_major2 = 'Standard' and ac_cl_minor2 = 'Billable') or (ac_cl_major2 = 'NetMedX' and ac_cl_minor2 = 'Billable') or (ac_cl_major2 = 'MSP' and ac_cl_minor2 = 'Billable')) ";

		return defaults;
	}

	String mapJobTypesSelections() {
		String defaults = "";

		this.JobTypeMap.put("Standard Billable",
				" (ac_cl_major2 = 'Standard' and ac_cl_minor2 = 'Billable') ");
		this.JobTypeMap
				.put("Standard Non-Billable",
						" (ac_cl_major2 = 'Standard' and ac_cl_minor2 = 'Non-Billable') ");
		this.JobTypeMap.put("NetMedX Billable",
				" (ac_cl_major2 = 'NetMedX' and ac_cl_minor2 = 'Billable') ");
		this.JobTypeMap
				.put("NetMedX Non-Billable",
						" (ac_cl_major2 = 'NetMedX' and ac_cl_minor2 = 'Non-Billable') ");
		this.JobTypeMap.put("MSP Billable",
				" (ac_cl_major2 = 'MSP' and ac_cl_minor2 = 'Billable') ");
		this.JobTypeMap.put("MSP Non-Billable",
				" (ac_cl_major2 = 'MSP' and ac_cl_minor2 = 'Non-Billable') ");

		return defaults;
	}

	void setSubTitles(HttpServletRequest request) {
		String job_type_list = "";
		String[] job_types = request.getParameterValues("job_types");
		if ((job_types != null) && (job_types.length > 0)) {
			for (int i = 0; i < job_types.length - 1; i++) {
				job_type_list = job_type_list + job_types[i] + ", ";
			}
			job_type_list = job_type_list + job_types[(job_types.length - 1)];
		} else {
			job_type_list = "Standard Billable, MSP Billable, NetMedX Billable";
		}
		request.setAttribute("subtitle", job_type_list);
	}

	String getJobTypes(HttpServletRequest request) {
		String job_type_list = "";
		String[] job_types = request.getParameterValues("job_types");
		if ((job_types != null) && (job_types.length > 0)) {
			for (int i = 0; i < job_types.length - 1; i++) {
				job_type_list = job_type_list + job_types[i] + ",";
			}
			job_type_list = job_type_list + job_types[(job_types.length - 1)];
		} else {
			job_type_list = "Standard Billable,MSP Billable,NetMedX Billable";
		}
		try {
			job_type_list = URLEncoder.encode(job_type_list, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return job_type_list;
	}

	public ActionForward revenue_breakout(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "error";

		Calendar calendar1 = Calendar.getInstance();

		String invoice_date = request.getParameter("invoice");
		calendar1.setTimeInMillis(this.standard_date3.parse(invoice_date)
				.getTime());
		int lastDate = calendar1.getActualMaximum(5);
		int firstDate = calendar1.getActualMinimum(5);

		calendar1.set(5, firstDate);
		String invoice1 = this.standard_date3.format(Long.valueOf(calendar1
				.getTimeInMillis()));
		calendar1.set(5, lastDate);
		String invoice2 = this.standard_date3.format(Long.valueOf(calendar1
				.getTimeInMillis()));

		String offsite_date = request.getParameter("offsite");
		calendar1.setTimeInMillis(this.standard_date1.parse(offsite_date)
				.getTime());
		lastDate = calendar1.getActualMaximum(5);
		firstDate = calendar1.getActualMinimum(5);

		calendar1.set(5, firstDate);
		String offsite1 = this.standard_date3.format(Long.valueOf(calendar1
				.getTimeInMillis()));
		calendar1.set(5, lastDate);
		String offsite2 = this.standard_date3.format(Long.valueOf(calendar1
				.getTimeInMillis()));

		request.setAttribute("title", "Revenue Breakout for Invoice Window in "
				+ invoice1 + " and Offsite Window " + offsite1);
		request.setAttribute("subtitle", "");
		if (getRevenueBreakout(request, invoice1, invoice2, offsite1, offsite2)) {
			forward_key = "revenue_breakout";
		}
		return mapping.findForward(forward_key);
	}

	boolean getRevenueBreakout(HttpServletRequest request, String i1,
			String i2, String o1, String o2) {
		boolean rc = true;

		String sql1 = "select lo_ot_name, lx_pr_title, (ac_rd_weight*ac_ru_pro_forma), (ac_rd_weight*ac_ru_expense), (ac_rd_weight*ac_ru_revenue), ac_rd_onsite, ac_rd_offsite, ac_rd_weight, lm_js_invoice_no, lm_js_id   from datawarehouse.dbo.ac_fact_job_rollup        join datawarehouse.dbo.ac_dimension_job_revenue_dates on ac_ru_js_id = ac_rd_js_id        join datawarehouse.dbo.ac_dimension_job_classification on ac_ru_js_id = ac_cl_jd_js_id        join datawarehouse.dbo.ac_dimensions_job on ac_ru_js_id = ac_jd_js_id        join datawarehouse.dbo.ac_dimension_date on convert (char(10), ac_rd_offsite, 121) = convert (char(10), sc_dd_mmddyyyy, 121)       join ilex.dbo.ap_appendix_list_01 on ac_jd_pd_pr_id = lx_pr_id        join ilex.dbo.lm_job on ac_ru_js_id = lm_js_id  where  ac_jd_date_closed between '"
				+

				i1
				+ "' and '"
				+ i2
				+ "' and ac_rd_offsite between '"
				+ o1
				+ "' and '"
				+ o2
				+ "' "
				+ " and ac_cl_major2 in ('Standard', 'NetMedX', 'MSP') and ac_cl_minor2 = 'Billable'"
				+ " order by ac_jd_js_id ";

		System.out.println(sql1);

		Vector<String[]> jobs = new Vector();

		ResultSet rs = this.da.getResultSet(sql1);
		try {
			while (rs.next()) {
				String[] job = new String[10];
				job[0] = rs.getString(1);
				job[1] = rs.getString(2);
				job[2] = currency.format(rs.getDouble(3));
				job[3] = currency.format(rs.getDouble(4));
				job[4] = currency.format(rs.getDouble(5));
				job[5] = rs.getString(6).substring(5, 16);
				job[6] = rs.getString(7).substring(5, 16);
				job[7] = percentage.format(rs.getDouble(8));
				job[8] = rs.getString(9);
				job[9] = rs.getString(10);
				jobs.add(job);
			}
			rs.close();
			request.setAttribute("jobs", jobs);
		} catch (SQLException e) {
			rc = false;
			System.out.println(e + " " + sql1);
			request.setAttribute("error", e.getMessage());
		}
		return rc;
	}

	public ActionForward revenue_allocation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "error";

		Calendar calendar = Calendar.getInstance();
		int lastDate = calendar.getActualMaximum(5);
		int firstDate = calendar.getActualMinimum(5);

		String month1 = request.getParameter("month");
		String month2 = "";
		if (month1 == null) {
			calendar.set(5, firstDate);
			month1 = this.standard_date3.format(Long.valueOf(calendar
					.getTimeInMillis()));
			calendar.set(5, lastDate);
			month2 = this.standard_date3.format(Long.valueOf(calendar
					.getTimeInMillis()));
		} else {
			month1 = request.getParameter("month");
			calendar.setTimeInMillis(this.standard_date3.parse(month1)
					.getTime());
			lastDate = calendar.getActualMaximum(5);
			calendar.set(5, lastDate);
			month2 = this.standard_date3.format(Long.valueOf(calendar
					.getTimeInMillis()));
		}
		System.out.println(month1 + " " + month2);

		request.setAttribute("month", month1);

		request.setAttribute("title", "Revenue Allocation for " + month1);
		request.setAttribute("subtitle", "");
		if (getRevenueAllocation(request, month1, month2)) {
			forward_key = "revenue_allocation";
		}
		return mapping.findForward(forward_key);
	}

	boolean getRevenueAllocation(HttpServletRequest request,
			String display_month1, String display_month2) {
		boolean rc = true;

		String sql1 = "select convert(char(10), sc_dd_month_mm01yyyy, 121), sum(ac_rd_weight*ac_ru_pro_forma), sum(ac_rd_weight*ac_ru_expense), sum(ac_rd_weight*ac_ru_revenue), count(*)   from datawarehouse.dbo.ac_fact_job_rollup        join datawarehouse.dbo.ac_dimension_job_revenue_dates on ac_ru_js_id = ac_rd_js_id        join datawarehouse.dbo.ac_dimension_job_classification on ac_ru_js_id = ac_cl_jd_js_id        join datawarehouse.dbo.ac_dimensions_job on ac_ru_js_id = ac_jd_js_id        join datawarehouse.dbo.ac_dimension_date on convert (char(10), ac_rd_offsite, 121) = convert (char(10), sc_dd_mmddyyyy, 121) where  ac_jd_date_closed between '"
				+

				display_month1
				+ "' and '"
				+ display_month2
				+ "' "
				+ " and ac_cl_major2 in ('Standard', 'NetMedX', 'MSP') and ac_cl_minor2 = 'Billable'"
				+ " group by convert(char(10), sc_dd_month_mm01yyyy, 121) "
				+ " order by convert(char(10), sc_dd_month_mm01yyyy, 121) ";
		System.out.println(sql1);

		String month = "";
		Vector months = new Vector();
		Map<String, String[]> months1 = new HashMap();
		Map<String, String[]> months2 = new HashMap();
		Map<String, String[]> months3 = new HashMap();
		String[] allocation_default = { "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;",
				"&nbsp;" };

		ResultSet rs = this.da.getResultSet(sql1);
		try {
			while (rs.next()) {
				month = rs.getString(1);
				months.add(month);
				String[] allocation = new String[5];
				allocation[0] = rs.getString(1);
				allocation[1] = currency.format(rs.getDouble(2));
				allocation[2] = currency.format(rs.getDouble(3));
				allocation[3] = currency.format(rs.getDouble(4));
				allocation[4] = number.format(rs.getInt(5));
				months1.put(month, allocation);
				months2.put(month, allocation_default);
				months3.put(month, allocation_default);
			}
			rs.close();
			request.setAttribute("m1", months1);
			request.setAttribute("months", months);
		} catch (SQLException e) {
			System.out.println(e + " " + sql1);
			request.setAttribute("error", e.getMessage());
		}
		sql1 =

		"select convert(char(10), sc_dd_month_mm01yyyy, 121), sum(ac_ru_pro_forma), sum(ac_ru_expense), sum(ac_ru_revenue), count(*)   from datawarehouse.dbo.ac_fact_job_rollup        join datawarehouse.dbo.ac_dimensions_job on ac_ru_js_id = ac_jd_js_id        join datawarehouse.dbo.ac_dimension_job_classification on ac_ru_js_id = ac_cl_jd_js_id        join datawarehouse.dbo.ac_dimension_date on convert (char(10), ac_jd_date_closed, 121) = convert (char(10), sc_dd_mmddyyyy, 121)  where ac_jd_date_closed between '"
				+ display_month1
				+ "' and '"
				+ display_month2
				+ "' "
				+ "       and ac_cl_major2 in ('Standard', 'NetMedX', 'MSP') and ac_cl_minor2 = 'Billable'"
				+ " group by convert(char(10), sc_dd_month_mm01yyyy, 121) "
				+ " order by convert(char(10), sc_dd_month_mm01yyyy, 121) ";
		System.out.println(sql1);

		rs = this.da.getResultSet(sql1);
		try {
			while (rs.next()) {
				month = rs.getString(1);
				String[] allocation = new String[5];
				allocation[0] = rs.getString(1);
				allocation[1] = currency.format(rs.getDouble(2));
				allocation[2] = currency.format(rs.getDouble(3));
				allocation[3] = currency.format(rs.getDouble(4));
				allocation[4] = number.format(rs.getInt(5));
				months2.put(month, allocation);
			}
			rs.close();
			request.setAttribute("m2", months2);
		} catch (SQLException e) {
			System.out.println(e + " " + sql1);
			request.setAttribute("error", e.getMessage());
		}
		sql1 =

		"select convert(char(10), sc_dd_month_mm01yyyy, 121), sum(ac_rd_weight*ac_ru_pro_forma), sum(ac_rd_weight*ac_ru_expense), sum(ac_rd_weight*ac_ru_revenue), count(*)   from datawarehouse.dbo.ac_fact_job_rollup        join datawarehouse.dbo.ac_dimension_job_revenue_dates on ac_ru_js_id = ac_rd_js_id        join datawarehouse.dbo.ac_dimension_job_classification on ac_ru_js_id = ac_cl_jd_js_id        join datawarehouse.dbo.ac_dimensions_job on ac_ru_js_id = ac_jd_js_id        join datawarehouse.dbo.ac_dimension_date on convert (char(10), ac_rd_offsite, 121) = convert (char(10), sc_dd_mmddyyyy, 121) where  ac_rd_offsite between '"
				+ display_month1
				+ "' and '"
				+ display_month2
				+ "' "
				+ " and ac_cl_major2 in ('Standard', 'NetMedX', 'MSP') and ac_cl_minor2 = 'Billable'"
				+ " group by convert(char(10), sc_dd_month_mm01yyyy, 121) "
				+ " order by convert(char(10), sc_dd_month_mm01yyyy, 121) ";
		System.out.println(sql1);

		rs = this.da.getResultSet(sql1);
		try {
			while (rs.next()) {
				month = rs.getString(1);
				String[] allocation = new String[5];
				allocation[0] = rs.getString(1);
				allocation[1] = currency.format(rs.getDouble(2));
				allocation[2] = currency.format(rs.getDouble(3));
				allocation[3] = currency.format(rs.getDouble(4));
				allocation[4] = number.format(rs.getInt(5));
				months3.put(month, allocation);
			}
			rs.close();
			request.setAttribute("m3", months3);
		} catch (SQLException e) {
			System.out.println(e + " " + sql1);
			request.setAttribute("error", e.getMessage());
		}
		return rc;
	}
}
