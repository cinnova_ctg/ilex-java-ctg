package com.contingent.ilex.report;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.contingent.ilex.utility.ReportUtils;

public class RevenueGraphServlet extends HttpServlet implements Servlet {
	private static final long serialVersionUID = 1L;
	private HttpSession session = null;
	LinkedHashMap<Integer, RevenueChartData> monthData = null;
	private String dtype = "";
	private String dstart = "";
	private String dend = "";
	private String spman = "";
	private String bdman = "";
	private String jowner = "";
	private String client = "";
	private String ptype = "";

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		StringBuffer sb = new StringBuffer();
		getRequestParams(req);
		ArrayList<String> allDates = (ArrayList) this.session
				.getAttribute("datesArray");

		String chartUrl = devChartData(allDates);
		String chartTable = buildChartTable();
		sb.append("<table width=100%><tr><td colspan=3 height='5px' style='border-top:solid 2px black;'><br></td></tr>");
		sb.append("<tr><td align='right'>" + chartUrl
				+ "</td width=1%><td><br></td><td align = 'left'>" + chartTable
				+ "</td></tr></table>");

		resp.setContentType("text/xml;charset=UTF-8");
		resp.setHeader("Cache-Control", "no-cache");
		resp.getWriter().write(sb.toString());
	}

	private void getRequestParams(HttpServletRequest req) {
		this.dtype = req.getParameter("dtype");
		this.dstart = req.getParameter("dstart");
		this.dend = req.getParameter("dend");
		this.spman = req.getParameter("spm");
		this.bdman = req.getParameter("bdm");
		this.jowner = req.getParameter("jo");
		this.client = req.getParameter("cli");
		this.ptype = req.getParameter("pt");
		this.session = req.getSession();
	}

	private String devChartData(ArrayList<String> aDates) {
		throw new Error(
				"Unresolved compilation problem: \n\tThe method getDatesAsInts(ArrayList<Integer>, int, int) in the type ReportUtils is not applicable for the arguments (ArrayList<String>, String, String)\n");
	}

	private String buildChartTable() {
		StringBuffer sb = new StringBuffer();
		String prevYear = "";
		RevenueChartData temp = null;
		float vcogsPercent = 0.0F;
		float vgpPercent = 0.0F;

		DecimalFormat df = new DecimalFormat("##.#");
		sb.append("<table class='charttable' border='1' cellpadding='3' cellspacing='3' ");
		sb.append("rules='none' frame='box' bordercolor='#000000'>");
		sb.append("<tr><th class='thyearcell'>Year</th><th class='thmonthcell'>Month</th>");
		sb.append("<th class='thcell'>VCOGS</th><th class='thcell'>VGP</th><th class='thcell'>Revenue</th></tr>");
		Collection<RevenueChartData> c = this.monthData.values();
		Iterator<RevenueChartData> itr = c.iterator();
		while (itr.hasNext()) {
			temp = (RevenueChartData) itr.next();

			sb.append("<tr>");

			String thisYear = Integer.toString(temp.getYear());
			if (!thisYear.equals(prevYear)) {
				sb.append("<td class='yearcell'>" + thisYear + "</td>");
			} else {
				sb.append("<td></td>");
			}
			prevYear = thisYear;

			sb.append("<td class='monthcell'>" + temp.getMonth() + "</td>");
			int rev = temp.getRevenue().setScale(0, 4).intValue();
			if (rev != 0) {
				vcogsPercent = temp.getVcogs().setScale(0, 4).intValue() / rev
						* 100.0F;
				vgpPercent = temp.getVgp().setScale(0, 4).intValue() / rev
						* 100.0F;
			}
			sb.append("<td class='vcogscell' align='right'>"
					+ df.format(vcogsPercent) + "%</td>");
			sb.append("<td class='vgpcell' align='right'>"
					+ df.format(vgpPercent) + "%</td>");
			sb.append("<td class='revcell' align='right'>"
					+ ReportUtils.currencyFormatIntValue(rev) + "</td>");

			sb.append("</tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	private String getChartDimensions(int size) {
		int width = 170 + 37 * size;
		if (width > 1000) {
			width = 1000;
		}
		return width + "x" + 300;
	}

	private String getYearLabels(ArrayList<String> yearList, boolean showAll) {
		String lastYear = "";
		boolean EQUAL_FLAG = false;
		String thisYear = "";
		String prevYear = "";
		StringBuffer sb = new StringBuffer();
		thisYear = (String) yearList.get(0);
		lastYear = (String) yearList.get(yearList.size() - 1);
		sb.append(thisYear);
		if (thisYear.equals(lastYear)) {
			EQUAL_FLAG = true;
		}
		for (int i = 1; i < yearList.size(); i++) {
			prevYear = thisYear;
			thisYear = (String) yearList.get(i);
			if (showAll) {
				sb.append("|" + thisYear);
			} else if (((EQUAL_FLAG) && (i == yearList.size() - 1))
					|| (!thisYear.equals(prevYear))) {
				sb.append("|" + thisYear);
			} else {
				sb.append("|");
			}
		}
		return sb.toString();
	}

	private String getYaxisLabels(int chartHeight) {
		StringBuffer sb = new StringBuffer();
		int topVal = ReportUtils.roundInt(chartHeight, 2);
		topVal = (int) (topVal - topVal * 0.05F);
		int increment = topVal / 4;
		sb.append("$0|" + ReportUtils.currencyFormatIntValue(increment));
		for (int i = 2; i <= 4; i++) {
			sb.append("|" + ReportUtils.currencyFormatIntValue(increment * i));
		}
		return sb.toString();
	}

	private String getYaxisPositions(int chartHeight) {
		StringBuffer sb = new StringBuffer();
		int topVal = ReportUtils.roundInt(chartHeight, 2);
		topVal = (int) (topVal - topVal * 0.05F);
		int increment = topVal / 4;
		sb.append("0," + increment);
		for (int i = 2; i <= 4; i++) {
			sb.append("," + increment * i);
		}
		return sb.toString();
	}

	private String getYaxisGridWidth(int chartHeight) {
		int topVal = ReportUtils.roundInt(chartHeight, 2);
		topVal = (int) (topVal - topVal * 0.05F);
		int increment = topVal / 4;
		float incrPercent = increment / chartHeight * 100.0F;
		DecimalFormat df = new DecimalFormat("##.#");
		String retVal = df.format(incrPercent);

		return retVal;
	}
}
