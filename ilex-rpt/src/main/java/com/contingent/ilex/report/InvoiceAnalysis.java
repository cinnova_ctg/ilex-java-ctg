package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class InvoiceAnalysis extends DispatchAction {
	DataAccess da = new DataAccess();
	Vector Internal0 = new Vector(6);
	Vector Internal1 = new Vector(6);
	Vector WEB0 = new Vector(6);
	Vector WEB1 = new Vector(6);
	double manualFactor = 0.0D;
	double paperFactor = 0.0D;
	double webFactor = 0.0D;
	Map defaultFactors = new HashMap();

	public ActionForward invoice_analysis(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "invoice_analysis";
		setDefaultFactors();

		getFactors(request);

		getinvoiceSummary(request, "Invoice Analysis");

		getMinutemanSummary(request);

		getStatusSummary(request);

		return mapping.findForward(forward_key);
	}

	void getinvoiceSummary(HttpServletRequest request, String group) {
		Map invoice_analysis = new HashMap();
		Vector x = new Vector();
		Vector y = new Vector();
		double ef = 0.0D;
		NumberFormat formatter2 = new DecimalFormat("0.0");

		String key = "";
		String next_key = "";
		String value = "";
		int i = 0;
		String sql = "select ap_in_group, ap_in_key, convert(varchar(12), ap_in_subkey, 101), ap_in_value from ap_invoice_summaries where ap_in_key is not null and ap_in_group = '"
				+ group + "' order by ap_in_group, ap_in_key, ap_in_subkey asc";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				key = rs.getString("ap_in_key");
				if (!key.equals(next_key)) {
					if (i > 0) {
						invoice_analysis.put(next_key, x);
						invoice_analysis.put(next_key + "_ef", y);
					} else {
						i++;
					}
					next_key = key;
					x = new Vector();
					y = new Vector();
				}
				if (key.startsWith("Internal")) {
					ef = this.manualFactor - this.paperFactor;
					if (key.endsWith("1")) {
						ef /= 0.5D;
					}
				} else if (key.startsWith("WEB")) {
					ef = this.manualFactor - this.webFactor;
					if (key.endsWith("1")) {
						ef /= 0.5D;
					}
				}
				value = rs.getString("ap_in_value");

				x.add(0, value);
				ef = ef * Double.parseDouble(value) / 60.0D;
				y.add(0, formatter2.format(ef));
			}
			if (x.size() > 0) {
				invoice_analysis.put(key, x);
				invoice_analysis.put(key + "_ef", y);
			}
			request.setAttribute("invoice_analysis", invoice_analysis);

			rs.close();
		} catch (Exception e) {
			System.out.println("InvoiceAnalysis.getinvoiceSummary: " + e
					+ " - with - " + sql);
		}
	}

	void getMinutemanSummary(HttpServletRequest request) {
		Map invoice_analysis = new HashMap();
		Vector x = new Vector();
		Vector y = new Vector();
		double ef = 0.0D;
		NumberFormat formatter2 = new DecimalFormat("0.0");

		String key = "";
		String next_key = "";
		String value = "";
		int i = 0;
		String sql = "select ap_in_group, ap_in_key, convert(varchar(12), ap_in_subkey, 101), ap_in_value from ap_invoice_summaries where ap_in_key is not null and ap_in_group = 'Minute Analysis' order by ap_in_group, ap_in_key, ap_in_subkey asc";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				key = rs.getString("ap_in_key");
				if (!key.equals(next_key)) {
					if (i > 0) {
						invoice_analysis.put(next_key, x);
						invoice_analysis.put(next_key + "_ef", y);
					} else {
						i++;
					}
					next_key = key;
					x = new Vector();
					y = new Vector();
				}
				ef = this.manualFactor;
				value = rs.getString("ap_in_value");

				x.add(0, value);
				ef = ef * Double.parseDouble(value) / 60.0D;
				y.add(0, formatter2.format(ef));
			}
			if (x.size() > 0) {
				invoice_analysis.put(key, x);
				invoice_analysis.put(key + "_ef", y);
			}
			request.setAttribute("mm_analysis", invoice_analysis);

			rs.close();
		} catch (Exception e) {
			System.out.println("InvoiceAnalysis.getMinutemanSummary: " + e
					+ " - with - " + sql);
		}
	}

	void getStatusSummary(HttpServletRequest request) {
		Map invoice_status = new HashMap();

		String key = "";
		String value = "";
		String sql = "select ap_in_key, sum(convert(int,ap_in_value)) from ap_invoice_summaries where ap_in_group ='Status Summary' group by ap_in_key order by ap_in_key";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				key = rs.getString(1);
				value = rs.getString(2);
				invoice_status.put(key, value);
			}
			request.setAttribute("invoice_status", invoice_status);

			rs.close();
		} catch (Exception e) {
			System.out.println("InvoiceAnalysis.getStatusSummary: " + e
					+ " - with - " + sql);
		}
	}

	void getFactors(HttpServletRequest request) {
		this.manualFactor = getFactor(request, "manual_invoice");
		this.paperFactor = getFactor(request, "paper_invoice");
		this.webFactor = getFactor(request, "web_invoice");
	}

	double getFactor(HttpServletRequest request, String factor) {
		String in_factor = request.getParameter(factor);
		request.setAttribute(factor, in_factor);
		double f;
		try {
			f = Double.parseDouble(in_factor);
		} catch (Exception e) {
			// double f;
			in_factor = (String) this.defaultFactors.get(factor);
			request.setAttribute(factor, in_factor);
			f = Double.parseDouble(in_factor);
		}
		return f;
	}

	void setDefaultFactors() {
		this.defaultFactors.put("manual_invoice", "12");
		this.defaultFactors.put("paper_invoice", "5");
		this.defaultFactors.put("web_invoice", "2.5");
	}
}
