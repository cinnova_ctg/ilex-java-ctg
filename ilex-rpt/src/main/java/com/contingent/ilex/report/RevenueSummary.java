package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class RevenueSummary extends DispatchAction {
	DataAccess da = new DataAccess();
	String year = "2008";
	Map reportRevenue = new HashMap();

	public ActionForward revenueSummaryReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = executeTopLevelSummary(request);

		return mapping.findForward(forward_key);
	}

	String executeTopLevelSummary(HttpServletRequest request) {
		String forward_key = "revenue_summary";
		this.reportRevenue.put("2008", "20000000");
		this.reportRevenue.put("2007", "17500000");
		this.reportRevenue.put("2006", "4000000");

		getReportParameters(request);
		getReportData(formatSQL(), request);

		getVCOGS(request);

		getTimeliness(request);

		getPPCompany(request);

		getChartData(request);

		return forward_key;
	}

	void getReportParameters(HttpServletRequest request) {
		this.year = request.getParameter("year");
		if ((this.year == null) || (this.year.equals(""))) {
			this.year = "2008";
		}
	}

	String formatSQL() {
		String rev = (String) this.reportRevenue.get(this.year);
		String sql_command = "exec ap_company_objective_data_01 '" + this.year
				+ "', " + rev;

		return sql_command;
	}

	void getReportData(String sql, HttpServletRequest request) {
		Map summary = new HashMap();
		Double[] percentage = new Double[5];
		String[] displayDollar = new String[5];

		ResultSet rs = this.da.getResultSet(sql);
		try {
			rs.next();

			percentage[0] = Double.valueOf(rs.getDouble("inwork_percent"));
			percentage[1] = Double.valueOf(rs.getDouble("complete_percent"));
			percentage[2] = Double.valueOf(rs.getDouble("closed_percent"));
			percentage[3] = Double.valueOf(rs.getDouble("delta_percent"));
			percentage[4] = Double.valueOf(rs.getDouble("delta_percentc"));

			displayDollar[0] = rs.getString("inwork_display");
			displayDollar[1] = rs.getString("complete_display");
			displayDollar[2] = rs.getString("closed_display");
			displayDollar[3] = rs.getString("delta_display");
			displayDollar[4] = rs.getString("delta_displayc");

			String reportDate = rs.getString("run_date");

			rs.close();

			summary.put("percentage", percentage);
			summary.put("displayDollar", displayDollar);
			summary.put("reportDate", reportDate);

			request.setAttribute("Summary", summary);
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	void getVCOGS(HttpServletRequest request) {
		Map x = new HashMap();
		String[] y = (String[]) null;

		String sql = "select ap_rc_resource, cast (ap_rc_delta as varchar(3)) as delta, '$'+convert(varchar(8),ap_rc_average_cost,128) as average_cost, cast(convert(int,ap_rc_total_quantity) as varchar(12)) as total_quantity from ap_resource_cost_short_summary order by ap_rc_resource, ap_rc_delta";
		ResultSet rs = this.da.getResultSet(sql);

		String current_resource = "";
		String previous_resource = "";
		int i = 0;
		try {
			while (rs.next()) {
				current_resource = rs.getString("ap_rc_resource");
				if (!current_resource.equals(previous_resource)) {
					if (y != null) {
						x.put(previous_resource, y);
					}
					y = new String[4];
					i = 0;
					previous_resource = current_resource;
				}
				y[(i++)] = rs.getString("average_cost");
			}
			x.put(current_resource, y);
			request.setAttribute("resource_cost", x);
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	void getTimeliness(HttpServletRequest request) {
		Map x = new HashMap();
		String[] y = (String[]) null;

		String sql = "select ap_jt_interval, cast (ap_jt_delta as varchar(3)) as delta, cast (ap_jt_average_days as varchar(3)) as days from ap_job_timeliness where ap_jt_project_type = 'B' order by ap_jt_interval, ap_jt_delta ";
		ResultSet rs = this.da.getResultSet(sql);

		String current_interval = "";
		String previous_interval = "";
		int i = 0;
		try {
			while (rs.next()) {
				current_interval = rs.getString("ap_jt_interval");
				if (!current_interval.equals(previous_interval)) {
					if (y != null) {
						x.put(previous_interval, y);
					}
					y = new String[4];
					i = 0;
					previous_interval = current_interval;
				}
				y[(i++)] = rs.getString("days");
			}
			x.put(current_interval, y);
			request.setAttribute("timeliness", x);
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	void getPPCompany(HttpServletRequest request) {
		Map x = new HashMap();
		String[] y = (String[]) null;

		String sql = "select ap_pp_category, cast (ap_pp_delta as varchar(3)) as delta, ap_pp_value as count from ap_profitability_productivity order by ap_pp_category, ap_pp_delta";
		ResultSet rs = this.da.getResultSet(sql);

		String current_interval = "";
		String previous_interval = "";
		int i = 0;
		try {
			while (rs.next()) {
				current_interval = rs.getString("ap_pp_category");
				if (!current_interval.equals(previous_interval)) {
					if (y != null) {
						x.put(previous_interval, y);
					}
					y = new String[4];
					i = 0;
					previous_interval = current_interval;
				}
				y[(i++)] = rs.getString("count");
			}
			x.put(current_interval, y);
			request.setAttribute("profit_productivity", x);
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	void getChartData(HttpServletRequest request) {
		Map charts = new HashMap();
		Map parameters = null;

		String sql = "select dw_chart_name, rtrim(dw_chd) as dw_chd, rtrim(dw_chxl1) as dw_chxl1, rtrim(dw_chxl2) as dw_chxl2,rtrim(dw_chxl3) as dw_chxl3 from dw_charts";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				parameters = new HashMap(6);

				parameters.put("dw_chd", rs.getString("dw_chd"));
				parameters.put("dw_chxl1", rs.getString("dw_chxl1"));
				parameters.put("dw_chxl2", rs.getString("dw_chxl2"));
				parameters.put("dw_chxl3", rs.getString("dw_chxl3"));
				charts.put(rs.getString("dw_chart_name"), parameters);
			}
			rs.close();

			request.setAttribute("charts", charts);
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}
}
