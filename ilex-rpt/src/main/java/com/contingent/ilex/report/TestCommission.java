package com.contingent.ilex.report;

public class TestCommission {
	public TestCommission() {
		Commission xt = new Commission();
		xt.initialzeInputParameters();

		setParameters(xt);

		xt.ManageRequest1();
	}

	void setParameters(Commission xt) {
		setParameter(xt, "bdm", "4");

		setParameter(xt, "default_view", "yes");
	}

	void setParameter(Commission xt, String p, String v) {
		xt.ParameterValue.put(p, v);
		xt.Submitted.put(p, v);
	}

	public static void main(String[] args) {
		TestCommission x = new TestCommission();
	}
}
