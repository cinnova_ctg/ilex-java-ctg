package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class InvoiceStatus_20Dec2013 extends DispatchAction {
	Map<String, Vector<String>> RequestIputMap = new HashMap();
	Map<String, String> RequestIputs = new HashMap();
	Vector<String[]> AgentsOrderList = new Vector(7);
	Map<String, String> AgentMapping = new HashMap(7, 1.0F);
	String ErrorMessag = "";
	static int lm_po_partner_id;
	static int lm_pi_payment_scheduled;
	static int lm_pi_approved_total;
	static int partner_name;
	static int lp_es_commission_rate;
	static int lp_eac_res_tot_revenue;
	static int lp_eac_comm_cost;
	DataAccess da = new DataAccess();
	NumberFormat currency = NumberFormat.getCurrencyInstance();
	NumberFormat percentage = NumberFormat.getPercentInstance();
	DecimalFormat nf1 = new DecimalFormat("0.000");
	DecimalFormat nf2 = new DecimalFormat("##,###,##0.00");

	public ActionForward agent_list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "";

		initializeRequestInputMap();
		if (validateRequestInputs(request, request.getParameter("ua"))) {
			switch (Integer.parseInt((String) this.RequestIputs.get("ua"))) {
			case 4:
				buildAgentCommissionList(request);

				forward_key = "4";
				break;
			case 5:
				break;
			default:
				forward_key = "test";
			}
		}
		return mapping.findForward(forward_key);
	}

	void buildAgentCommissionList(HttpServletRequest request) {
		String columns = "1 as type, lo_ot_name, lx_pr_title, partner_name+' - '+case when lp_es_payment_terms = 10 then 'MRC' else 'Quarterly NRC' end as partner_name, lp_es_commission_rate, lp_es_type, sum(lp_eac_res_tot_revenue) as lp_eac_res_tot_revenue, sum(lp_eac_comm_cost) as lp_eac_comm_cost";

		String sql = buildSQL4(columns);

		request.setAttribute("rows",
				getAgentCommissionList(sql, breakDownColumns(columns)));
	}

	String buildSQL4(String columns) {
		String sql =

		"select "
				+ columns
				+ "  from dbo.lp_esa_setup "
				+ "       join dbo.ap_appendix_list_01 on lp_es_pr_id = lx_pr_id "
				+ "       join dbo.lp_esa_activity_commission on lp_es_id = lp_eac_es_id and lp_eac_js_type in ('D','A') "
				+ "       join dbo.cp_partner_standard_partner_application_01 on lp_es_cp_id = cp_pd_partner_id "
				+

				" group by lo_ot_name, lx_pr_title, partner_name, lp_es_payment_terms, lp_es_commission_rate, lp_es_type "
				+ " order by partner_name, lo_ot_name, lx_pr_title ";

		return sql;
	}

	Vector<String[]> getAgentCommissionList(String sql, String[] columns) {
		Vector<String[]> rows = new Vector();
		String group = "";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				if (!group.equals(rs.getString(columns[partner_name]))) {
					if (!group.equals("")) {
						String[] row = new String[columns.length];
						row[0] = "4";
						rows.add(row);

						row = new String[columns.length];
						row[0] = "2";
						row[1] = rs.getString(columns[partner_name]);
						rows.add(row);
						group = row[1];
					} else {
						String[] row = new String[columns.length];
						row[0] = "2";
						row[1] = rs.getString(columns[partner_name]);
						rows.add(row);
						group = row[1];
					}
				}
				String[] row = new String[columns.length];
				for (int i = 0; i < columns.length; i++) {
					if (i == lp_es_commission_rate) {
						row[i] = this.percentage.format(rs
								.getDouble(columns[i]));
					} else if ((i == lp_eac_res_tot_revenue)
							|| (i == lp_eac_comm_cost)) {
						row[i] = this.nf2.format(rs.getDouble(columns[i]));
					} else {
						row[i] = rs.getString(columns[i]);
					}
				}
				rows.add(row);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return rows;
	}

	public ActionForward inv1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "";
		boolean inputs_ok = false;

		initializeRequestInputMap();
		inputs_ok = validateRequestInputs(request, request.getParameter("ua"));
		switch (Integer.parseInt((String) this.RequestIputs.get("ua"))) {
		case 1:
			forward_key = "";
			break;
		case 2:
			request.setAttribute("agent_drop_down", buildAgentList());

			forward_key = "2";
			break;
		case 3:
			request.setAttribute("agent_drop_down", buildAgentList());
			if (inputs_ok) {
				getApprovedCommissionList(request);

				request.setAttribute("agent", this.AgentMapping
						.get(this.RequestIputs.get("partner_id")));
				request.setAttribute("error", "");
				forward_key = "2";
			} else {
				forward_key = "2";
				request.setAttribute("error", this.ErrorMessag);
			}
			break;
		case 4:
			forward_key = "";
			break;
		case 5:
			forward_key = "";
			break;
		default:
			forward_key = "test";
		}
		return mapping.findForward(forward_key);
	}

	void getApprovedCommissionList(HttpServletRequest request) {
		String columns = "1 as type, lo_ot_name, lx_pr_title, lm_js_title, convert(varchar(10),lm_po_complete_date,101) as lm_po_complete_date, lm_pi_status, lm_pi_approved_total, convert(varchar(10),lm_pi_approved_date,101) as lm_pi_approved_date, convert(varchar(10),lm_pi_payment_scheduled,101) as lm_pi_payment_scheduled, lm_po_id, lm_po_partner_id, lm_js_invoice_no";

		String sql = buildApprovedCommisionQuery(columns);

		request.setAttribute("rows",
				getCommissionList(sql, breakDownColumns(columns)));
	}

	Vector<String[]> getCommissionList(String sql, String[] columns) {
		Vector<String[]> rows = new Vector();
		String group = "";

		int start = 1;
		int end = 0;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				if (!group.equals(rs
						.getString(columns[lm_pi_payment_scheduled]))) {
					if (!group.equals("")) {
						String[] row = new String[columns.length];
						row[0] = "3";
						row[lm_pi_approved_total] = "0.00";
						for (int x = start; x < end; x++) {
							row[lm_pi_approved_total] = String.valueOf(Double
									.parseDouble(row[lm_pi_approved_total])
									+ Double.parseDouble(((String[]) rows
											.get(x))[lm_pi_approved_total]));
							((String[]) rows.get(x))[lm_pi_approved_total] = this.nf2
									.format(Double.parseDouble(((String[]) rows
											.get(x))[lm_pi_approved_total]));
						}
						row[lm_pi_approved_total] = this.currency.format(Double
								.parseDouble(row[lm_pi_approved_total]));
						rows.add(row);
						end++;

						row = new String[columns.length];
						row[0] = "4";
						rows.add(row);
						end++;

						row = new String[columns.length];
						row[0] = "2";
						row[1] = rs.getString(columns[lm_pi_payment_scheduled]);
						rows.add(row);
						end++;
						group = row[1];
						start = end;
					} else {
						String[] row = new String[columns.length];
						row[0] = "2";
						row[1] = rs.getString(columns[lm_pi_payment_scheduled]);
						rows.add(row);
						group = row[1];
						end++;
					}
				}
				String[] row = new String[columns.length];
				for (int i = 0; i < columns.length; i++) {
					row[i] = rs.getString(columns[i]);
				}
				row[lm_po_partner_id] = ((String) this.AgentMapping
						.get(row[lm_po_partner_id]));
				rows.add(row);
				end++;
			}
			String[] row = new String[columns.length];
			row[0] = "3";
			row[lm_pi_approved_total] = "0.00";
			for (int x = start; x < end; x++) {
				row[lm_pi_approved_total] = String
						.valueOf(Double.parseDouble(row[lm_pi_approved_total])
								+ Double.parseDouble(((String[]) rows.get(x))[lm_pi_approved_total]));
				((String[]) rows.get(x))[lm_pi_approved_total] = this.nf2
						.format(Double.parseDouble(((String[]) rows.get(x))[lm_pi_approved_total]));
			}
			row[lm_pi_approved_total] = this.currency.format(Double
					.parseDouble(row[lm_pi_approved_total]));
			rows.add(row);

			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return rows;
	}

	String buildApprovedCommisionQuery(String columns) {
		String where1 = " po_status = 3 and lm_po_commission_flag = 1 and lm_pi_status = 'Approved' and lm_pwo_terms_master_data = 'Quarterly 90 Days Arrears/Paid Invoices'";
		String where2;
		// String where2;
		if (!((String) this.RequestIputs.get("partner_id")).equals("")) {
			where2 = " and lm_po_partner_id = "
					+ (String) this.RequestIputs.get("partner_id");
		} else {
			where2 = " and lm_po_id = "
					+ (String) this.RequestIputs.get("po_id");
		}
		String sql = "select "
				+ columns
				+ " from lm_purchase_order"
				+ " join lm_partner_invoice on lm_po_id = lm_pi_po_id"
				+ " join lm_pwo_terms_master on lm_pwo_terms_master_id = lm_po_terms"
				+ " join lm_job on lm_po_js_id = lm_js_id"
				+ " join dbo.ap_appendix_list_01 on lm_js_pr_id = lx_pr_id"
				+ " where" + where1 + where2
				+ " order by lm_pi_payment_scheduled, lm_po_partner_id";

		return sql;
	}

	String buildAgentList() {
		String dd = "<option value=\"\"> -- Select&nbsp;Partner -- </option>";
		String selected = "";
		String columns = "cp_pd_partner_id, partner_name ";
		String sql = "select distinct "
				+ columns
				+ " from lm_purchase_order"
				+ "   join dbo.cp_partner_standard_partner_application_01 on lm_po_partner_id = cp_pd_partner_id"
				+ " where po_status = 3 and lm_po_commission_flag = 1"
				+ " order by partner_name";

		getAgentLists(sql);

		String partner_id = (String) this.RequestIputs.get("partner_id");
		for (int i = 0; i < this.AgentsOrderList.size(); i++) {
			selected = (partner_id != null)
					&& (partner_id.equals(((String[]) this.AgentsOrderList
							.get(i))[0])) ? " selected" : "";
			dd = dd + "<option value=\""
					+ ((String[]) this.AgentsOrderList.get(i))[0] + "\""
					+ selected + ">"
					+ ((String[]) this.AgentsOrderList.get(i))[1] + "</option>";
		}
		return dd;
	}

	boolean validateRequestInputs(HttpServletRequest request, String ua) {
		boolean rc = false;

		Vector<String> inputs = (Vector) this.RequestIputMap.get(ua);

		this.ErrorMessag = "";
		for (int i = 0; i < inputs.size(); i++) {
			String parameter = (String) inputs.get(i);
			String value = validateInput(request.getParameter(parameter));
			this.RequestIputs.put(parameter, value);
			request.setAttribute(parameter, value);
		}
		switch (Integer.parseInt(ua)) {
		case 3:
			if (((String) this.RequestIputs.get("partner_id")).equals("")) {
				this.ErrorMessag = "Select an agent.";
			} else {
				rc = true;
			}
			break;
		}
		rc = true;

		return rc;
	}

	String validateInput(String value) {
		if (value == null) {
			value = "";
		}
		return value.trim();
	}

	void initializeRequestInputMap() {
		Vector<String> inputs = new Vector();
		inputs.add("ua");
		this.RequestIputMap.put("1", inputs);

		inputs = new Vector();
		inputs.add("ua");
		inputs.add("ut");
		this.RequestIputMap.put("2", inputs);

		inputs = new Vector();
		inputs.add("ua");
		inputs.add("ut");
		inputs.add("partner_id");
		inputs.add("po_id");
		inputs.add("start");
		inputs.add("end");
		inputs.add("display_pay");
		this.RequestIputMap.put("3", inputs);

		inputs = new Vector();
		inputs.add("ua");
		inputs.add("ut");
		inputs.add("partner_id");
		inputs.add("po_id");
		inputs.add("start");
		inputs.add("end");
		inputs.add("display_pay");
		this.RequestIputMap.put("4", inputs);

		inputs = new Vector();
		inputs.add("ua");
		this.RequestIputMap.put("4", inputs);
	}

	String[] breakDownColumns(String select) {
		String[] columns = select.split(", ");
		for (int i = 0; i < columns.length; i++) {
			if (columns[i].contains(" as ")) {
				String[] as_col = columns[i].split(" as ");
				columns[i] = as_col[1].trim();
			}
			if (columns[i].equals("lm_po_partner_id")) {
				lm_po_partner_id = i;
			} else if (columns[i].equals("lm_pi_payment_scheduled")) {
				lm_pi_payment_scheduled = i;
			} else if (columns[i].equals("lm_pi_approved_total")) {
				lm_pi_approved_total = i;
			} else if (columns[i].equals("partner_name")) {
				partner_name = i;
			} else if (columns[i].equals("lp_es_commission_rate")) {
				lp_es_commission_rate = i;
			} else if (columns[i].equals("lp_eac_res_tot_revenue")) {
				lp_eac_res_tot_revenue = i;
			} else if (columns[i].equals("lp_eac_comm_cost")) {
				lp_eac_comm_cost = i;
			}
		}
		return columns;
	}

	void getAgentLists(String sql) {
		this.AgentsOrderList.clear();
		this.AgentMapping.clear();

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] agent = new String[2];
				agent[0] = rs.getString(1);
				agent[1] = rs.getString(2);
				this.AgentsOrderList.add(agent);
				this.AgentMapping.put(rs.getString(1), rs.getString(2));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			this.AgentsOrderList.clear();
			this.AgentMapping.clear();
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}
}
