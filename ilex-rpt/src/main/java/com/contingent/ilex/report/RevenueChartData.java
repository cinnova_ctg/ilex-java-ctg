package com.contingent.ilex.report;

import java.math.BigDecimal;

public class RevenueChartData {
	private String month = "";
	private int year = 0;
	private BigDecimal revenue = new BigDecimal("0.0");
	private BigDecimal vcogs = new BigDecimal("0.0");
	private BigDecimal vgp = new BigDecimal("0.0");

	public String getMonth() {
		return this.month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public BigDecimal getRevenue() {
		return this.revenue;
	}

	public void setRevenue(BigDecimal revenue) {
		this.revenue = revenue;
	}

	public BigDecimal getVcogs() {
		return this.vcogs;
	}

	public void setVcogs(BigDecimal vcogs) {
		this.vcogs = vcogs;
	}

	public BigDecimal getVgp() {
		return this.vgp;
	}

	public void setVgp(BigDecimal vgp) {
		this.vgp = vgp;
	}

	public String toString() {
		return "Year='" + Integer.toString(getYear()) + "', Month='"
				+ getMonth() + "', VCOGS='" + getVcogs().toString()
				+ "', VGP='" + getVgp().toString() + "', Revenue='"
				+ getRevenue().toString();
	}
}
