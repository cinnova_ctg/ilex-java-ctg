package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class CustomerRating extends DispatchAction {
	Vector Parameters = new Vector();
	Map Submitted = new HashMap();
	Map ParameterValue = new HashMap();
	Vector Customer = new Vector();
	Vector BDM = new Vector();
	Vector EstimatedCost = new Vector();
	Vector ActualCost = new Vector();
	Vector Revenue = new Vector();
	Vector rows;
	NumberFormat cf = NumberFormat.getCurrencyInstance();
	NumberFormat pf = NumberFormat.getPercentInstance();
	NumberFormat df = new DecimalFormat("0.00");
	DataAccess da = new DataAccess();
	HttpServletRequest Request;

	public ActionForward revenue_customer(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		this.Request = request;

		initialzeInputParameters(request);

		manageAction();

		String forward_key = "revenue_customer";
		return mapping.findForward(forward_key);
	}

	void manageAction() {
		this.Revenue.add(Double.valueOf(10.1D));
		this.Revenue.add(Double.valueOf(30.100000000000001D));
		this.Revenue.add(Double.valueOf(20.100000000000001D));
		this.Revenue.add(Double.valueOf(80.099999999999994D));
		this.Revenue.add(Double.valueOf(10.1D));
		this.Revenue.add(Double.valueOf(10.1D));
		this.Revenue.add(Double.valueOf(40.100000000000001D));
		this.Revenue.add(Double.valueOf(10.1D));
		this.Revenue.add(Double.valueOf(10.1D));
		this.Revenue.add(Double.valueOf(60.100000000000001D));

		String sql = buildSQL();

		String[] fields = { "lo_ot_name", "dw_cs_revenue" };
		int[] field_type = { 1, 2 };

		this.rows = getResultSet(sql, fields, field_type);

		formatMainResultSet(this.rows);

		buildPieChartData();
	}

	void buildPieChartData() {
		String[] label = new String[this.rows.size()];
		Double[] value = new Double[this.rows.size()];
		Double total1 = Double.valueOf(0.0D);
		Double total2 = Double.valueOf(0.0D);

		int items_to_show = 6;
		for (int x = 0; x < this.rows.size(); x++) {
			Object[] row = (Object[]) this.rows.get(x);
			label[x] = ((String) row[0]);
			value[x] = ((Double) row[1]);
			total1 = Double.valueOf(total1.doubleValue()
					+ value[x].doubleValue());
		}
		Double min_value = value[(value.length - 1)];
		Double max_value = value[0];
		Double range = Double.valueOf(max_value.doubleValue()
				- min_value.doubleValue());
		if (value.length < 6) {
			items_to_show = value.length;
		}
		for (int x = items_to_show; x < value.length; x++) {
			total2 = Double.valueOf(total2.doubleValue()
					+ value[x].doubleValue());
		}
		String t = "";
		String del1 = "";
		String chtd = "";
		String del2 = "";
		String chl = "";
		String p = "";
		for (int i = 0; i < items_to_show; i++) {
			Double d1 = Double.valueOf(value[i].doubleValue()
					/ total1.doubleValue() * 100.0D);
			t = d1.toString();
			t = t.substring(0, t.indexOf("."));
			p = " (" + t + "%)";

			chtd = chtd + del1 + t;

			int and_test = label[i].indexOf("&");
			if (and_test > 0) {
				t = label[i].substring(1, and_test) + " and "
						+ label[i].substring(and_test + 1);
				t = label[i].replaceAll("\\Q&\\E", " and ");
			} else {
				t = label[i];
			}
			chl = chl + del2 + t + p;

			del1 = ",";
			del2 = "|";
		}
		Double d1 = Double.valueOf(total2.doubleValue() / total1.doubleValue()
				* 100.0D);
		t = d1.toString();
		t = t.substring(0, t.indexOf("."));
		p = " (" + t + "%)";
		chtd = chtd + del1 + t;
		chl = chl + del2 + "Others" + p;

		String img = "";

		img = "<img src=\"http://chart.apis.google.com/chart?cht=p3&amp;chs=700x200&amp;chd=t:"
				+ chtd
				+ "&amp;"
				+ "chl="
				+ chl
				+ "\" "
				+ "alt=\"Customer Revenue Breakdown\"/>";
	}

	void buildChartData() {
		Double total = Double.valueOf(0.0D);
		Double temp = Double.valueOf(0.0D);
		int itemp = 0;
		String chtd = "";
		String del = "";

		int[] sorted_order = new int[this.Revenue.size()];
		Double[] sorted_values = new Double[this.Revenue.size()];
		for (int i = 0; i < this.Revenue.size(); i++) {
			total = Double.valueOf(total.doubleValue()
					+ ((Double) this.Revenue.get(i)).doubleValue());
			sorted_values[i] = ((Double) this.Revenue.get(i));
			sorted_order[i] = i;
		}
		for (int i = 0; i < this.Revenue.size() - 1; i++) {
			for (int j = i + 1; j < this.Revenue.size(); j++) {
				if (sorted_values[i].doubleValue() < sorted_values[j]
						.doubleValue()) {
					temp = sorted_values[i];
					itemp = sorted_order[i];
					sorted_values[i] = sorted_values[j];
					sorted_order[i] = sorted_order[j];
					sorted_values[j] = temp;
					sorted_order[j] = itemp;
				}
			}
		}
		Double min_value = sorted_values[(sorted_values.length - 1)];
		Double max_value = sorted_values[1];
		Double range = Double.valueOf(max_value.doubleValue()
				- min_value.doubleValue());

		Double bcht_range = Double.valueOf((max_value.doubleValue() - min_value
				.doubleValue()) * 1.1D);
		Double bcht_min_value = Double.valueOf(min_value.doubleValue() * 0.9D);
		if (sorted_values.length > 6) {
			itemp = 6;
		}
		for (int i = itemp; i < sorted_values.length; i++) {
			temp = Double.valueOf(temp.doubleValue()
					+ sorted_values[i].doubleValue());
		}
		String t = "";
		for (int i = 0; i < itemp; i++) {
			t = sorted_values[i].toString();
			t = t.substring(0, t.indexOf("."));
			chtd = chtd + del + t;
			del = ",";
		}
	}

	void initialzeInputParameters(HttpServletRequest request) {
		this.Parameters.clear();

		this.Parameters.add("customer");
		this.Parameters.add("bdm");
		this.Parameters.add("bdm_view");
		this.Parameters.add("job_owner");
		this.Parameters.add("from");
		this.Parameters.add("to");
		this.Parameters.add("quarter");
		this.Parameters.add("sort_order");

		getInputParameters(request);
	}

	void getInputParameters(HttpServletRequest request) {
		this.Submitted.clear();
		this.ParameterValue.clear();
		for (int i = 0; i < this.Parameters.size(); i++) {
			String parameter = (String) this.Parameters.get(i);
			String value = request.getParameter(parameter);
			if ((value != null) && (value.trim().length() > 0)) {
				this.ParameterValue.put(parameter, value.trim());
				this.Submitted.put(parameter, "True");
			}
		}
	}

	String buildSQL() {
		String sql = "";

		sql = "select lo_ot_name, sum(dw_cs_revenue) as dw_cs_revenue ";
		sql = sql
				+ "from dbo.dw_revenue_commission_support join lo_organization_top on dw_cs_lo_ot_id =  lo_ot_id ";
		sql = sql
				+ "where datepart (yyyy, dw_cs_lm_js_offsite_date)*10+datepart (qq, dw_cs_lm_js_offsite_date) = 20082 ";
		sql = sql + "group by lo_ot_name ";
		sql = sql + "having sum(dw_cs_revenue) > 1000.0 ";
		sql = sql + "order by sum(dw_cs_revenue) desc ";

		return sql;
	}

	String buildSQL_projects() {
		String sql = "";

		sql = "select lo_ot_name, sum(dw_cs_revenue) as dw_cs_revenue ";
		sql = sql
				+ "from dbo.dw_revenue_commission_support join lo_organization_top on dw_cs_lo_ot_id =  lo_ot_id ";
		sql = sql
				+ "where datepart (yyyy, dw_cs_lm_js_offsite_date)*10+datepart (qq, dw_cs_lm_js_offsite_date) = 20082 ";
		sql = sql + "group by lo_ot_name ";
		sql = sql + "having sum(dw_cs_revenue) > 1000.0 ";
		sql = sql + "order by sum(dw_cs_revenue) desc ";

		return sql;
	}

	String buildSQL_dispatch() {
		String sql = "";

		sql = "select lo_ot_name, sum(dw_cs_revenue) as dw_cs_revenue ";
		sql = sql
				+ "from dbo.dw_revenue_commission_support join lo_organization_top on dw_cs_lo_ot_id =  lo_ot_id ";
		sql = sql
				+ "where datepart (yyyy, dw_cs_lm_js_offsite_date)*10+datepart (qq, dw_cs_lm_js_offsite_date) = 20082 ";
		sql = sql + "group by lo_ot_name ";
		sql = sql + "having sum(dw_cs_revenue) > 1000.0 ";
		sql = sql + "order by sum(dw_cs_revenue) desc ";

		return sql;
	}

	Vector getResultSet(String sql, String[] fields, int[] field_type) {
		Vector rows = new Vector();

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				Object[] row = new Object[2];
				for (int i = 0; i < fields.length; i++) {
					row[i] = getFieldValue(fields[i], field_type[i], rs);
				}
				rows.add(row);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		return rows;
	}

	Object getFieldValue(String p, int i, ResultSet rs) {
		try {
			switch (i) {
			case 2:
				return Double.valueOf(rs.getDouble(p));
			case 3:
				return Integer.valueOf(rs.getInt(p));
			}
			return rs.getString(p);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return "Error";
	}

	void formatMainResultSet(Vector rows) {
		String[][] display_rows = new String[rows.size()][2];
		for (int i = 0; i < rows.size(); i++) {
			Object[] row = (Object[]) rows.get(i);
			display_rows[i][0] = ((String) row[0]);
			display_rows[i][1] = this.cf.format((Double) row[1]);
		}
		this.Request.setAttribute("rows", display_rows);
	}
}
