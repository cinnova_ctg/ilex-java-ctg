package com.contingent.ilex.report;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEvent;
import com.lowagie.text.pdf.PdfWriter;

public class NetMedXInvoice extends DispatchAction {
	float[] DispatchTable = { 4.25F, 0.75F, 1.0F, 1.0F };
	float[] HeaderTable = { 1.5F, 1.0F };
	float[] AppendixTable = { 1.0F };
	Font InvoiceColumnHeader = new Font(1, 8.0F, 1);
	Font InvoiceDetailBold = new Font(1, 9.0F, 1);
	Font InvoiceDetailNormal = new Font(1, 9.0F, 0);
	Font PageHeaderBold = new Font(1, 9.0F, 1);
	Font PageHeaderNormal = new Font(1, 9.0F, 0);
	Font PageHeaderBoldLarge = new Font(1, 12.0F, 1);
	Font InvoiceFooterNormal = new Font(1, 8.0F, 0);
	Font PageHeaderBoldShip = new Font(1, 10.0F, 1);
	Map InvoiceOptions = new HashMap();
	ByteArrayOutputStream baos = new ByteArrayOutputStream();

	public ActionForward inv1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		getReportOptions(request);

		int qty = 1;

		Map invoice_data = setDefault(qty);
		boolean appendix = false;

		this.PageHeaderBold.setColor(0, 36, 93);
		this.PageHeaderNormal.setColor(0, 36, 93);
		this.PageHeaderBoldLarge.setColor(0, 36, 93);

		Document invoice = initializeDocument((String) invoice_data
				.get("invoice_number"));

		buildPage1HeaderA(invoice, invoice_data);

		PdfPTable table = new PdfPTable(this.DispatchTable);
		table.setWidthPercentage(100.0F);
		buildPage1HeaderB(invoice, table);
		table.setHeaderRows(2);

		table.getDefaultCell().setBorder(0);

		table.getDefaultCell().enableBorderSide(4);
		table.getDefaultCell().enableBorderSide(8);
		if (((Vector) invoice_data.get("installation_photos")).size() > 0) {
			appendix = true;
		}
		if (((String) this.InvoiceOptions.get("count")).equals("2")) {
			buildInvoiceDetails(invoice, table, invoice_data, appendix);
			buildInvoiceDetails(invoice, table, invoice_data, appendix);
			buildInvoiceDetails(invoice, table, invoice_data, appendix);
			buildInvoiceDetails(invoice, table, invoice_data, appendix);
		} else {
			buildInvoiceDetails(invoice, table, invoice_data, appendix);
		}
		buildTableSummaryRows(table, (Vector) invoice_data.get("summary_items"));
		try {
			invoice.add(table);
		} catch (DocumentException localDocumentException) {
		}
		if (this.InvoiceOptions.get("photo") != null) {
			invoice.newPage();

			PdfPTable table_appendix = new PdfPTable(this.AppendixTable);
			table_appendix.setWidthPercentage(100.0F);

			buildInstallationPictures(table_appendix,
					(Vector) invoice_data.get("installation_photos"));
			try {
				invoice.add(table_appendix);
			} catch (DocumentException localDocumentException1) {
			}
		}
		invoice.close();

		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition",
				" attachment; filename=\"test.pdf\"");
		response.setContentLength(this.baos.size());
		ServletOutputStream out = response.getOutputStream();
		this.baos.writeTo(out);
		out.flush();

		return null;
	}

	Document initializeDocument(String invoice_number) {
		Document inv = new Document(PageSize.LETTER);
		inv.setMargins(36.0F, 36.0F, 18.0F, 36.0F);
		try {
			PdfWriter pdfwriter = PdfWriter.getInstance(inv, this.baos);
			PdfPageEvent page_event = new NetMedXInvoiceHeader(invoice_number);
			pdfwriter.setPageEvent(page_event);

			inv.open();
		} catch (Exception localException) {
		}
		return inv;
	}

	void buildPage1HeaderA(Document invoice, Map invoice_data) {
		int header_line_height = 8;
		int ship_line_height = 6;
		int line_count_used = 0;

		Chunk new_line = new Chunk(Chunk.NEWLINE);
		String temp = "";

		PdfPTable table = new PdfPTable(this.HeaderTable);
		table.setWidthPercentage(100.0F);
		table.getDefaultCell().setBorder(0);
		try {
			Map header_corporate_information = (Map) invoice_data
					.get("header_corporate_information");

			PdfPCell cell = new PdfPCell();
			cell.setBorder(0);

			String logo = setPWD()
					+ (String) header_corporate_information.get("logo");
			Image img1 = Image.getInstance(logo);
			img1.scaleAbsolute(110.0F, 69.0F);
			img1.setDpi(96, 96);
			cell.addElement(img1);

			Paragraph p = new Paragraph();
			p.setLeading(9.0F);

			temp = (String) header_corporate_information.get("name");
			Chunk c1 = new Chunk(temp, this.PageHeaderBold);
			p.add(c1);
			p.add(new_line);
			line_count_used++;

			Vector header_corporate_address = (Vector) header_corporate_information
					.get("address");
			for (int i = 0; i < header_corporate_address.size(); i++) {
				temp = (String) header_corporate_address.get(i);
				c1 = new Chunk(temp, this.PageHeaderNormal);
				p.add(c1);
				p.add(new_line);
				line_count_used++;
			}
			temp = (String) header_corporate_information.get("phone1");
			String p2 = (String) header_corporate_information.get("phone2");
			String f = (String) header_corporate_information.get("fax");
			if (p2 != null) {
				temp = temp + " " + p2;
			}
			if (f != null) {
				temp = temp + " FAX " + f;
			}
			c1 = new Chunk(temp, this.PageHeaderNormal);
			p.add(c1);
			p.add(new_line);
			line_count_used++;

			temp = (String) header_corporate_information.get("web_site");
			if (temp != null) {
				c1 = new Chunk(temp, this.PageHeaderNormal);
				p.add(c1);
				p.add(new_line);
				line_count_used++;
			}
			for (int i = line_count_used; i < 8; i++) {
				p.add(new_line);
			}
			cell.addElement(p);
			table.addCell(cell);

			Map header_invoice = (Map) invoice_data.get("header_invoice");
			p = new Paragraph();
			p.setLeading(12.0F);
			cell = new PdfPCell();
			cell.setBorder(0);

			p.add(new_line);
			p.add(new_line);
			p.add(new Chunk("INVOICE  ", this.PageHeaderBoldLarge));
			temp = (String) header_invoice.get("invoice_number");
			p.add(new Chunk(temp, this.InvoiceDetailBold));
			p.add(new_line);

			p.add(new Chunk("Date:  ", this.PageHeaderBold));
			p.add(new Chunk((String) header_invoice.get("date"),
					this.InvoiceDetailNormal));
			p.add(new_line);

			p.add(new Chunk("Payment Terms:  ", this.PageHeaderBold));
			p.add(new Chunk((String) header_invoice.get("terms"),
					this.InvoiceDetailNormal));
			p.add(new_line);

			p.add(new Chunk("Purchase Order Number:  ", this.PageHeaderBold));
			p.add(new Chunk((String) header_invoice.get("po_number"),
					this.InvoiceDetailNormal));
			p.add(new_line);

			cell.addElement(p);
			table.addCell(cell);

			table.addCell(new Paragraph("BILL TO:", this.PageHeaderBoldShip));
			table.addCell(new Paragraph("SHIP TO:", this.PageHeaderBoldShip));

			Vector header_bill_to = (Vector) invoice_data.get("header_bill_to");
			p = new Paragraph();
			p.setLeading(11.0F);
			cell = new PdfPCell();
			cell.setBorder(0);
			for (int i = 0; i < header_bill_to.size(); i++) {
				p.add(new Chunk((String) header_bill_to.get(i),
						this.InvoiceDetailNormal));
				p.add(new_line);
			}
			cell.addElement(p);
			table.addCell(cell);

			Vector header_ship_to = (Vector) invoice_data.get("header_ship_to");
			p = new Paragraph();
			p.setLeading(11.0F);
			cell = new PdfPCell();
			cell.setBorder(0);
			if (((String) this.InvoiceOptions.get("count")).equals("2")) {
				p.add(new Chunk("MULTI-SITE INVOICE", this.InvoiceDetailNormal));
				p.add(new_line);
				p.add(new Chunk("SEE DISPATCH DETAILS",
						this.InvoiceDetailNormal));
			} else {
				for (int i = 0; i < header_ship_to.size(); i++) {
					p.add(new Chunk((String) header_ship_to.get(i),
							this.InvoiceDetailNormal));
					p.add(new_line);
				}
			}
			cell.addElement(p);
			table.addCell(cell);

			invoice.add(table);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	void buildPage1HeaderB(Document invoice, PdfPTable table) {
		PdfPCell cell = new PdfPCell();
		cell.setFixedHeight(18.0F);
		cell.setBorder(0);
		cell.addElement(new Paragraph(""));
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);

		table.getDefaultCell().setBackgroundColor(Color.LIGHT_GRAY);
		table.getDefaultCell().setHorizontalAlignment(1);
		table.getDefaultCell().setPaddingBottom(4.0F);
		table.addCell(new Paragraph("Description", this.InvoiceColumnHeader));
		table.addCell(new Paragraph("Quantity", this.InvoiceColumnHeader));
		table.addCell(new Paragraph("Unit Price", this.InvoiceColumnHeader));
		table.addCell(new Paragraph("Extended Price", this.InvoiceColumnHeader));
		table.getDefaultCell().setBackgroundColor(Color.WHITE);
		table.getDefaultCell().setHorizontalAlignment(0);
		table.getDefaultCell().setPaddingBottom(2.0F);
	}

	void buildInvoiceDetails(Document invoice, PdfPTable table,
			Map invoice_data, boolean installation_photos) {
		table.getDefaultCell().enableBorderSide(1);

		Vector invoice_details = (Vector) invoice_data.get("invoice_details");
		for (int i = 0; i < invoice_details.size(); i++) {
			Map dispatch = (Map) invoice_details.get(i);

			table.getDefaultCell().enableBorderSide(1);
			buildTableRow1(table, (String) dispatch.get("dispatch_type"),
					this.InvoiceDetailBold);
			table.getDefaultCell().disableBorderSide(1);
			if (this.InvoiceOptions.get("ticket_number") != null) {
				buildTableRow1(table,
						"Ticket Number: " + (String) dispatch.get("ticket"),
						this.InvoiceDetailNormal);
			}
			buildTableRow1(table,
					"Request Date: " + (String) dispatch.get("request_date"),
					this.InvoiceDetailNormal);

			buildTableRow1(table, "Site: " + (String) dispatch.get("site"),
					this.InvoiceDetailNormal);
			if (this.InvoiceOptions.get("ticket_requestor") != null) {
				buildTableRow1(table,
						"Requestor: " + (String) dispatch.get("requestor"),
						this.InvoiceDetailNormal);
			}
			if (this.InvoiceOptions.get("ticket_category") != null) {
				buildTableRow1(table,
						"Category: " + (String) dispatch.get("category"),
						this.InvoiceDetailNormal);
			}
			boolean msp = false;
			String msp_input = (String) this.InvoiceOptions.get("MSP");
			if ((msp_input != null) && (msp_input.equals("MSP"))) {
				msp = true;
			}
			buildTableRow1(table, "Labor", this.InvoiceDetailBold);
			buildLaborMaterial(table, (Vector) dispatch.get("labor"), msp);

			buildTableRow1(table, "Materials", this.InvoiceDetailBold);
			buildLaborMaterial(table, (Vector) dispatch.get("materials"), false);

			boolean summary = true;
			if ((this.InvoiceOptions.get("contact") == null)
					&& (this.InvoiceOptions.get("on_off_site") == null)
					&& (this.InvoiceOptions.get("tos") == null)
					&& (this.InvoiceOptions.get("notes") == null)
					&& (this.InvoiceOptions.get("photo") == null)) {
				summary = false;
			}
			if (summary) {
				buildTableRow1(table, "Summary", this.InvoiceDetailBold);
				if (this.InvoiceOptions.get("contact") != null) {
					buildSummaryPM(table, dispatch);
				}
				if (this.InvoiceOptions.get("on_off_site") != null) {
					buildTableRow1(table, "Onsite/Offsite: "
							+ (String) dispatch.get("onsite_offsite"),
							this.InvoiceDetailNormal);
				}
				if (this.InvoiceOptions.get("tos") != null) {
					buildTableRow1(table, "Time on Site (Hours): "
							+ (String) dispatch.get("tos"),
							this.InvoiceDetailNormal);
				}
				if (this.InvoiceOptions.get("notes") != null) {
					buildTableRow1(table, "Notes: ", this.InvoiceDetailNormal);
					buildSummaryInstallNotes(table,
							(String) dispatch.get("notes"));
				}
				if ((installation_photos)
						&& (this.InvoiceOptions.get("photo") != null)) {
					buildTableRow1(table,
							"See appendix for installation photographs.",
							this.InvoiceDetailNormal);
				}
			}
			if (((String) this.InvoiceOptions.get("count")).equals("2")) {
				buildTableRow3(table, "Job Discount",
						(String) invoice_data.get("job_discount"),
						this.InvoiceDetailBold);
				buildTableRow3(table, "Job Total",
						(String) invoice_data.get("job_total"),
						this.InvoiceDetailBold);
			}
		}
		buildTableBottomCell(table);
	}

	void buildLaborMaterial(PdfPTable table, Vector line_items, boolean msp) {
		if (msp) {
			String[] li = new String[4];
			li[0] = "System Engineer (Minutes)";
			li[1] = "12";
			li[2] = "$1.25";
			li[3] = "$15.00";

			line_items.clear();
			line_items.add(li);
		}
		for (int i = 0; i < line_items.size(); i++) {
			buildTableRow2(table, (String[]) line_items.get(i),
					this.InvoiceDetailNormal);
		}
	}

	void buildSummaryPM(PdfPTable table, Map dispatch) {
		Paragraph p = new Paragraph();
		p.setLeading(10.0F);
		p.setIndentationLeft(16.0F);

		buildTableRow1(table, "Project Manager: ", this.InvoiceDetailNormal);

		p.add(new Chunk((String) dispatch.get("pm_name"),
				this.InvoiceDetailNormal));
		p.add(new Chunk(Chunk.NEWLINE));
		p.add(new Chunk((String) dispatch.get("pm_email"),
				this.InvoiceDetailNormal));
		p.add(new Chunk(Chunk.NEWLINE));
		p.add(new Chunk((String) dispatch.get("pm_phone"),
				this.InvoiceDetailNormal));

		table.addCell(buildTableCell(p));
		table.addCell(new Paragraph("", this.InvoiceDetailNormal));
		table.addCell(new Paragraph("", this.InvoiceDetailNormal));
		table.addCell(new Paragraph("", this.InvoiceDetailNormal));
	}

	void buildSummaryInstallNotes(PdfPTable table, String notes) {
		Paragraph p = new Paragraph(notes, this.InvoiceDetailNormal);
		p.setLeading(10.0F);

		table.addCell(buildTableCell(p));
		table.addCell("");
		table.addCell("");
		table.addCell("");
	}

	void buildTableRow1(PdfPTable table, String content, Font font) {
		table.addCell(new Paragraph(content, font));
		table.addCell("");
		table.addCell("");
		table.addCell("");
	}

	void buildTableRow2(PdfPTable table, String[] content, Font font) {
		table.addCell(new Paragraph(content[0], font));
		table.getDefaultCell().setHorizontalAlignment(2);
		table.addCell(new Paragraph(content[1], font));
		table.addCell(new Paragraph(content[2], font));
		table.addCell(new Paragraph(content[3], font));
		table.getDefaultCell().setHorizontalAlignment(0);
	}

	void buildTableRow3(PdfPTable table, String content1, String content2,
			Font font) {
		table.addCell(new Paragraph(content1, font));
		table.addCell("");
		table.addCell("");
		table.getDefaultCell().setHorizontalAlignment(2);
		table.addCell(new Paragraph(content2, font));
		table.getDefaultCell().setHorizontalAlignment(0);
	}

	PdfPCell buildTableCell(Paragraph paragraph) {
		PdfPCell cell = new PdfPCell();
		paragraph.setIndentationLeft(16.0F);

		cell.setBorder(0);
		cell.enableBorderSide(4);
		cell.enableBorderSide(8);
		cell.addElement(paragraph);

		return cell;
	}

	PdfPCell buildTableBottomCell(PdfPTable table) {
		PdfPCell cell = new PdfPCell();
		Paragraph p = new Paragraph("");

		cell.setBorder(0);
		cell.enableBorderSide(4);
		cell.enableBorderSide(8);
		cell.enableBorderSide(2);
		cell.addElement(p);

		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);

		cell.setBorder(0);

		return cell;
	}

	void buildTableSummaryRows(PdfPTable table, Vector summary_items) {
		Paragraph space = new Paragraph("");
		Paragraph p = new Paragraph("");

		table.getDefaultCell().setHorizontalAlignment(2);
		for (int i = 0; i < summary_items.size(); i++) {
			String[] summary_item = (String[]) summary_items.get(i);

			PdfPCell cell = new PdfPCell();
			cell.setBorder(0);
			cell.setColspan(2);
			cell.addElement(space);
			table.addCell(cell);

			cell = new PdfPCell();

			cell.enableBorderSide(4);
			cell.enableBorderSide(8);
			cell.setHorizontalAlignment(2);
			p = new Paragraph(summary_item[0], this.InvoiceDetailNormal);
			p.setAlignment(2);
			cell.addElement(p);
			table.addCell(p);

			cell = new PdfPCell();
			cell.setBorder(0);
			cell.enableBorderSide(4);
			cell.enableBorderSide(8);
			p = new Paragraph(summary_item[1], this.InvoiceDetailNormal);
			p.setAlignment(2);
			cell.addElement(p);
			table.addCell(p);
		}
		PdfPCell cell = new PdfPCell();
		cell.setBorder(0);
		cell.setColspan(2);
		cell.addElement(space);
		table.addCell(cell);

		cell = new PdfPCell();
		cell.setBorder(0);
		cell.enableBorderSide(4);
		cell.enableBorderSide(8);
		cell.enableBorderSide(2);
		cell.addElement(space);
		table.addCell(cell);
		table.addCell(cell);

		table.getDefaultCell().setHorizontalAlignment(0);
	}

	void buildInstallationPictures(PdfPTable table, Vector installation_photos) {
		PdfPCell cell = new PdfPCell();
		Paragraph p = new Paragraph();
		Paragraph space = new Paragraph("");

		table.getDefaultCell().setBorder(0);

		table.addCell(new Paragraph("Installation Photographs",
				this.InvoiceDetailBold));
		table.addCell(space);

		System.out.println(installation_photos.size());
		for (int i = 0; i < installation_photos.size(); i++) {
			String[] installation_photo = (String[]) installation_photos.get(i);

			table.addCell(new Paragraph(installation_photo[0],
					this.InvoiceDetailNormal));

			table.addCell(new Paragraph(installation_photo[1],
					this.InvoiceDetailNormal));
			table.addCell(space);
			try {
				Image img1 = Image
						.getInstance(setPWD() + installation_photo[2]);
				img1.scaleAbsoluteWidth(400.0F);
				img1.setDpi(72, 72);
				cell = new PdfPCell();
				cell.setBorder(0);
				cell.addElement(img1);
				table.addCell(cell);
			} catch (BadElementException localBadElementException) {
			} catch (MalformedURLException localMalformedURLException) {
			} catch (IOException localIOException) {
			}
		}
	}

	Map setDefault(int qty) {
		Map dv = new HashMap();

		Map header_corporate_information = new HashMap();
		Vector header_corporate_address = new Vector();
		Map header_invoice = new HashMap();
		Vector header_bill_to = new Vector();
		Vector header_ship_to = new Vector();
		if (qty > 1) {
			header_corporate_information.put("multiple invoices", "YES");
			header_ship_to.add("CONSOLIDATED INVOICE");
			header_ship_to.add("SEE EACH DISPATCH");
		} else {
			header_corporate_information.put("multiple invoices", "NO");
			header_ship_to.add("Company Name");
			header_ship_to.add("Address1");
			header_ship_to.add("Address2");
			header_ship_to.add("City ST, 85091");
		}
		dv.put("invoice_number", "500000001");
		header_corporate_address.add("10008 INTERNATIONAL BLVD.");
		header_corporate_address.add("CINCINNATI, OH 45246");

		header_corporate_information.put("logo", "contingent-network.gif");

		header_corporate_information.put("address", header_corporate_address);
		header_corporate_information.put("name",
				"CONTINGENT NETWORK SERVICES, LLC");

		header_corporate_information.put("web_site", "www.contingent.com");
		header_corporate_information.put("phone1", "(800) 506-9609");
		header_corporate_information.put("phone2", "(513) 860-2573");
		header_corporate_information.put("fax", "(513) 860-2105");

		header_bill_to.add("Company Name");
		header_bill_to.add("Address1");
		header_bill_to.add("City ST, 99012");

		header_invoice.put("invoice_number", "500000001");
		header_invoice.put("date", "01/03/2009");
		header_invoice.put("terms", "NET 30 Days");
		header_invoice.put("po_number", "X1234871-01");

		dv.put("header_corporate_information", header_corporate_information);
		dv.put("header_invoice", header_invoice);
		dv.put("header_bill_to", header_bill_to);
		dv.put("header_ship_to", header_ship_to);

		Vector invoice_details = new Vector();

		Map dispatch = new HashMap();

		dispatch.put("dispatch_type", "Standard NetMedX Dispatch");
		dispatch.put("ticket", "00001803");
		dispatch.put("request_date", "04/16/09");
		dispatch.put("site", "CSS-OMX-0422, Yuba City, CA, US");
		dispatch.put("requestor", "Bob Jones");
		dispatch.put("category", "Repair Cabling");

		Vector labor = new Vector();

		String[] labor_row = new String[4];
		labor_row[0] = "Field Technician, Level 3, C3 (Dispatch Minimum)";
		labor_row[1] = "1";
		labor_row[2] = "$178.00";
		labor_row[3] = "$178.00";
		labor.add(labor_row);

		labor_row = new String[4];
		labor_row[0] = "Travel  (Hours)";
		labor_row[1] = "1";
		labor_row[2] = "$89.00";
		labor_row[3] = "$89.00";
		labor.add(labor_row);

		labor_row = new String[4];
		labor_row[0] = "Extended Onsite Time (Hours)";
		labor_row[1] = "1";
		labor_row[2] = "$89.00";
		labor_row[3] = "$89.00";
		labor.add(labor_row);

		dispatch.put("labor", labor);

		Vector materials = new Vector();

		String[] materials_row = new String[4];
		materials_row[0] = "Cat 5E, 1000 Ft, Blue";
		materials_row[1] = "2";
		materials_row[2] = "$60.00";
		materials_row[3] = "$120.00";
		materials.add(materials_row);

		materials_row = new String[4];
		materials_row[0] = "Cat 5E, 1000 Ft, Yellow";
		materials_row[1] = "1";
		materials_row[2] = "$60.00";
		materials_row[3] = "$60.00";
		materials.add(materials_row);

		dispatch.put("materials", materials);

		dispatch.put("pm_name", "Michael Phelps");
		dispatch.put("pm_email", "mphelps@contingent.net");
		dispatch.put("pm_phone", "(513) 555-1212, X433");
		dispatch.put("onsite_offsite", "MM/DD/YY HH:MM AM - MM/DD/YY HH:MM AM");
		dispatch.put("tos", "3.5");
		dispatch.put(
				"notes",
				"The site visit was completed successfully.  The new router was installed and configured.  Connectivity was confirmed with the help desk.  MOD verified site is operational.");

		invoice_details.add(dispatch);

		dv.put("invoice_details", invoice_details);

		Vector summary_items = new Vector();

		String[] summary_item = new String[2];
		summary_item[0] = "Subtotal: ";
		summary_item[1] = "$536.00";
		summary_items.add(summary_item);

		summary_item = new String[2];
		summary_item[0] = "Tax: ";
		summary_item[1] = "$10.29";
		summary_items.add(summary_item);

		summary_item = new String[2];
		summary_item[0] = "Freight: ";
		summary_item[1] = "$30.50";
		summary_items.add(summary_item);

		summary_item = new String[2];
		summary_item[0] = "Total: ";
		summary_item[1] = "$576.79";
		summary_items.add(summary_item);

		dv.put("summary_items", summary_items);

		Vector installation_photos = new Vector();
		String[] photo = new String[3];
		photo[0] = "Ticket/Site: 00001803/CSS-OMX-0422, Yuba City, CA, US";
		photo[1] = "Final Installation Configuration";
		photo[2] = "sample.jpg";
		installation_photos.add(photo);

		dv.put("installation_photos", installation_photos);

		dv.put("job_discount", "$0.00");
		dv.put("job_total", "$576.79");

		return dv;
	}

	void getReportOptions(HttpServletRequest request) {
		this.InvoiceOptions.put("site", getParameter(request, "site"));
		this.InvoiceOptions.put("ticket_number",
				getParameter(request, "ticket_number"));
		this.InvoiceOptions.put("ticket_requestor",
				getParameter(request, "ticket_requestor"));
		this.InvoiceOptions.put("ticket_category",
				getParameter(request, "ticket_category"));
		this.InvoiceOptions.put("labor", getParameter(request, "labor"));
		this.InvoiceOptions.put("material", getParameter(request, "material"));
		this.InvoiceOptions.put("contact", getParameter(request, "contact"));
		this.InvoiceOptions.put("on_off_site",
				getParameter(request, "on_off_site"));
		this.InvoiceOptions.put("tos", getParameter(request, "tos"));
		this.InvoiceOptions.put("notes", getParameter(request, "notes"));
		this.InvoiceOptions.put("photo", getParameter(request, "photo"));
		this.InvoiceOptions.put("count", getParameter(request, "count"));
		this.InvoiceOptions.put("MSP", getParameter(request, "MSP"));
	}

	String getParameter(HttpServletRequest request, String p) {
		String parameter = null;

		parameter = request.getParameter(p);
		if ((p.equals("photo")) && (parameter.length() == 0)) {
			parameter = null;
		}
		return parameter;
	}

	String setPWD() {
		String pwd = "";
		String pfile_test = System.getProperty("user.dir");
		if (pfile_test.equals("C:\\Eclipse\\eclipse")) {
			pwd = "C:\\Eclipse\\eclipse\\pdf\\";
		} else {
			pwd = "/opt/ilex/jakarta-tomcat-5.5.9/webapps/rpt/pdf/";
		}
		return pwd;
	}

	public static void main(String[] args) {
	}
}
