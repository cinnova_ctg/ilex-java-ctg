package com.contingent.ilex.report;

import java.sql.CallableStatement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class Workload extends DispatchAction {
	static DataAccess da = new DataAccess();

	public ActionForward displayWorkloadPage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "displayWorkloadPage1";

		GregorianCalendar ts = new GregorianCalendar();
		request.setAttribute("uniqueness", Integer.toString(ts.get(13)));

		return mapping.findForward(forward_key);
	}

	public ActionForward setDataXML(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "setDataXML1";
		System.out.println("Get Data Formating XML");

		return mapping.findForward(forward_key);
	}

	public ActionForward getWorkloadDataStream(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "getWorkloadDataStream";

		GregorianCalendar ts = new GregorianCalendar();

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		String timeLabel = sdf.format(cal.getTime());
		String[] l_and_v = getLabelAndValue();

		String dataParameters = "&label=" + l_and_v[1] + "&value=" + l_and_v[0];

		System.out.println(dataParameters);

		request.setAttribute("data", dataParameters);

		return mapping.findForward(forward_key);
	}

	String[] getLabelAndValue() {
		String sp = "{call pr_sample_datastream_01 (?, ?)}";
		String[] label_and_value = new String[2];
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sp);
			stmt.registerOutParameter("@data_value", 12);
			stmt.registerOutParameter("@label", 12);
			stmt.execute();
			label_and_value[0] = stmt.getString("@data_value");
			label_and_value[1] = stmt.getString("@label");
		} catch (Exception e) {
			System.out.println(e);
		}
		return label_and_value;
	}

	public ActionForward getWorkloadStatistics(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "getWorkloadStatistics";

		GregorianCalendar ts = new GregorianCalendar();

		System.out.println("Executing getWorkloadStatistics");

		return mapping.findForward(forward_key);
	}
}
