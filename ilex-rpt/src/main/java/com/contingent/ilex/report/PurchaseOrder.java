package com.contingent.ilex.report;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class PurchaseOrder {
	String lm_po_id = "";
	String po_type = "";
	double authorizedCost = 0.0D;
	double estimatedCost = 0.0D;
	public Vector resources = new Vector();
	Map resourceUnitCost = new HashMap();
	Map resourceQty = new HashMap();
	Map resourceAuthorizedCost = new HashMap();

	PurchaseOrder(Double at, String id) {
		this.authorizedCost = at.doubleValue();
		this.lm_po_id = id;
	}

	boolean getPODetailsFP() {
		boolean rtn_code = true;

		String sql = "exec ap_get_purchase_order_details " + this.lm_po_id;

		return rtn_code;
	}

	boolean getPODetailsHS() {
		boolean rtn_code = true;

		String sql = "exec ap_get_FP_purchase_order_details_01 "
				+ this.lm_po_id;

		return rtn_code;
	}

	public void computeEstimatedCost() {
		String key = null;
		for (int i = 0; i < this.resources.size(); i++) {
			key = (String) this.resources.get(i);
			double res_unit_cost = ((Double) this.resourceUnitCost.get(key))
					.doubleValue();
			double res_qty = ((Double) this.resourceQty.get(key)).doubleValue();
			this.estimatedCost += res_qty * res_unit_cost;
		}
	}

	public void addResourceData(String rs, Double cost, Double qty) {
		if (this.resources.contains(rs)) {
			Double t_qty = Double.valueOf(((Double) this.resourceQty.get(rs))
					.doubleValue() + qty.doubleValue());
			this.resourceQty.put(rs, t_qty);
		} else {
			this.resources.add(rs);
			this.resourceUnitCost.put(rs, cost);
			this.resourceQty.put(rs, qty);
		}
	}

	public void assignAuthorizedEstimate() {
		String resource = null;
		for (int i = 0; i < this.resources.size(); i++) {
			resource = (String) this.resources.get(i);

			double res_unit_cost = ((Double) this.resourceUnitCost
					.get(resource)).doubleValue();
			double res_qty = ((Double) this.resourceQty.get(resource))
					.doubleValue();
			double res_total_cost = res_unit_cost * res_qty;

			double res_weight = res_total_cost / this.estimatedCost;

			double res_auth_total_cost = res_weight * this.authorizedCost;
			double res_auth_unit_cost = res_auth_total_cost / res_qty;

			this.resourceAuthorizedCost.put(resource,
					Double.valueOf(res_auth_unit_cost));
		}
	}
}
