package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.util.Vector;

public class ResourceCostAnalysis {
	DataAccess da = new DataAccess();
	Vector PurchaseOrders = new Vector();

	ResourceCostAnalysis() {
		getPOData();
	}

	void getPOData() {
		String sql = "exec ap_resource_cost_analysis_01";
		int i = 0;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				this.PurchaseOrders.add(
						i++,
						new PO(rs.getString("lm_po_id"), rs
								.getString("lm_po_pwo_type_id"), rs
								.getString("lm_po_js_id"), rs
								.getString("lm_js_created_by"), this.da));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
		System.out.println("Purchase Orders Considered: " + i);
	}

	void clearResources() {
		String sql = "truncate table ap_labor_resource_details";
		this.da.executeQuery(sql);
	}

	void buildNewSummary() {
		String sql = "exec ap_resource_cost_short_summary_populate_01";
		this.da.executeQuery(sql);
	}

	void buildTimelinessSummary() {
		String sql = "exec ap_job_timeliness_summary_01";
		this.da.executeQuery(sql);
	}

	public static void main(String[] args) {
		Vector Resources = new Vector();

		Resources.add(0, "700000004");
		Resources.add(1, "700100067");
		Resources.add(2, "700100037");

		ResourceCostAnalysis x = new ResourceCostAnalysis();

		x.clearResources();

		System.out.println("Process Selected Resources");
		for (int i = 0; i < x.PurchaseOrders.size(); i++) {
			PO po = (PO) x.PurchaseOrders.get(i);
			po.processPO();
			po.saveResourceDetails(Resources);
		}
		System.out.println("Updating Resource Cost Analysis Summary Tables");
		x.buildNewSummary();

		System.out.println("Updating Timeliness Summary Tables");

		x.buildTimelinessSummary();
	}
}
