package com.contingent.ilex.report;
import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class LabelValueValue implements Serializable {

	private static final long serialVersionUID = -8195352616125870131L;
	private String label = null;
	private String value = null;
	private String value01 = null;

	public LabelValueValue(String a, String b,String c) {
		label = a;
		value = b;
		value01=c;
	}

	public void setLabel(String l) {
		label = l;
	}

	public String getLabel() {
		return label;
	}

	public void setValue(String v) {
		value = v;
	}

	public String getValue() {
		return value;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	public String getValue01() {
		return value01;
	}

	public void setValue01(String value01) {
		this.value01 = value01;
	}
}
