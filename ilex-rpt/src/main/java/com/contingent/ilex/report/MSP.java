package com.contingent.ilex.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class MSP extends DispatchAction {
	DataAccess da = new DataAccess();

	public ActionForward current_outages(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "outage1";

		request.setAttribute("outage_table",
				formatOutageReport(getCurrentOutages()));

		return mapping.findForward(forward_key);
	}

	Vector getCurrentOutages() {
		Vector rows = new Vector();

		String sql = "select lo_ot_name, lx_pr_title, isnull(lm_mp_si_number, 'Not found in Ilex'), 'sDisplayName', isnull(lm_mp_status, 'Pending'),        convert(varchar(24), lm_mp_down_point, 101)+' '+convert(varchar(24), lm_mp_down_point, 114), a.lm_mp_id   from lm_msp_outage a          join ap_appendix_list_01 on lm_mp_pr_id = lx_pr_id where lm_mp_down_point > dateadd (hh, -24, getdate()) and lm_mp_si_number is not null  order by lo_ot_name, isnull(lm_mp_si_number, 'Not found in Ilex'), lm_mp_down_point desc";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[7];

				row[0] = rs.getString(1);
				row[1] = rs.getString(2);
				row[2] = rs.getString(3);
				row[3] = rs.getString(4);
				row[4] = rs.getString(5);
				row[5] = rs.getString(6);
				row[6] = rs.getString(7);
				rows.add(row);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException e1) {
				System.out.println(e1);
			}
		}
		return rows;
	}

	String formatOutageReport(Vector rows) {
		String st1 = "ccaa";
		String st2 = "ccab";
		String style = "";
		String[] bgc = { "#E0E0D8", "#ffffff" };
		String last_customer = "";

		String outage_table = "";
		if (rows.size() > 0) {
			for (int i = 0; i < rows.size(); i++) {
				String[] row = (String[]) rows.get(i);
				outage_table = outage_table + "\t<tr>";

				style = "style=\"background-color: " + bgc[(i % 2)] + ";\"";
				if (!last_customer.equals(row[0])) {
					last_customer = row[0];
					outage_table = outage_table + "<td class=\"" + st1 + "\""
							+ style + "\">" + row[0] + "</td>";
				} else {
					outage_table = outage_table
							+ "<td class=\""
							+ st1
							+ "\" style=\"border-bottom: 1px solid #FFFFFF\">&nbsp;</td>";
				}
				outage_table = outage_table + "<td class=\"" + st2 + "\""
						+ style + ">" + row[1] + "</td>";
				outage_table = outage_table + "<td class=\"" + st1 + "\""
						+ style + ">" + row[2] + "</td>";
				outage_table = outage_table + "<td class=\"" + st1 + "\""
						+ style + ">" + row[3] + "</td>";
				outage_table = outage_table + "<td class=\"" + st2 + "\""
						+ style + ">" + row[4] + "</td>";
				outage_table = outage_table + "<td class=\"" + st2 + "\""
						+ style + ">" + row[5] + "</td>";
				outage_table = outage_table
						+ "<td class=\""
						+ st2
						+ "\""
						+ style
						+ ">&nbsp;&nbsp;"
						+ "<a href=\"/rpt/MSP.xo?action=mark_starting_point&lm_mp_id="
						+ row[6]
						+ "\">Start</a>  | "
						+ "<a href=\"/rpt/MSP.xo?action=add_to_starting_point&lm_mp_id="
						+ row[6] + "\">Add</a> | "
						+ "<a href=\"#\">Close</a>&nbsp;&nbsp;&nbsp;</td>";

				outage_table = outage_table + "</tr>\n";
			}
		} else {
			outage_table = outage_table
					+ "<td class=\""
					+ st1
					+ "\">There are no outage to report in the last 24 hours.</td></tr>\n";
		}
		return outage_table;
	}

	public ActionForward mark_starting_point(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "mark_start1";

		String lm_mp_id = request.getParameter("lm_mp_id");

		setStatusToDown(lm_mp_id);

		return mapping.findForward(forward_key);
	}

	Vector setStatusToDown(String lm_mp_id) {
		Vector rows = new Vector();

		String sql = "update lm_msp_outage set lm_mp_status = 'Down' where lm_mp_id = "
				+ lm_mp_id;

		this.da.executeQuery(sql);

		return rows;
	}

	public ActionForward add_to_starting_point(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "add1";

		String lm_mp_id = request.getParameter("lm_mp_id");

		addToPreviousOutage(lm_mp_id);

		return mapping.findForward(forward_key);
	}

	Vector addToPreviousOutage(String lm_mp_id) {
		Vector rows = new Vector();

		String sql = "exec lm_msp_add_to_outage " + lm_mp_id;

		this.da.executeQuery(sql);

		return rows;
	}

	public ActionForward rpt_matrix_summary(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Vector min_max = new Vector();
		Vector outage = new Vector();

		String[] timeframe = new String[12];
		String[] display_labels = new String[12];
		String[] labels = new String[3];

		String forward_key = "rpt_matrix_summary1";

		buildTimeFrame("Month", timeframe, labels, display_labels);

		getMatrixData(outage, min_max);

		request.setAttribute("msp_outages",
				buildMatrix(outage, timeframe, display_labels, labels));

		return mapping.findForward(forward_key);
	}

	String buildMatrix(Vector outages, String[] timeframe,
			String[] display_labels, String[] labels) {
		String current_customer = "initial_loop";
		Map customer = new HashMap();
		Vector table = new Vector();

		String msp_outages = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n<tr><td class=\"ch1b\" style=\"border-bottom: 1px solid #BFBEC4\">Customer</td>";
		for (int i = 0; i < timeframe.length; i++) {
			msp_outages = msp_outages
					+ "<td class=\"ch1b\" style=\"border-bottom: 1px solid #BFBEC4\">"
					+ display_labels[i] + "</td>";
		}
		msp_outages = msp_outages + "</tr>\n";
		for (int i = 0; i < outages.size(); i++) {
			String[] row = (String[]) outages.get(i);
			if (!row[0].equals(current_customer)) {
				if (!current_customer.equals("initial_loop")) {
					table.add(customer);
				}
				current_customer = row[0];
				customer = new HashMap();
				customer.put("current", current_customer);
			}
			customer.put(row[1], row[2]);
		}
		table.add(customer);

		String v = "";
		for (int i = 0; i < table.size(); i++) {
			Map t = (Map) table.get(i);
			msp_outages = msp_outages + "<tr><td class=\"ccac\">"
					+ (String) t.get("current") + "</td>";
			for (int j = 0; j < timeframe.length; j++) {
				v = (String) t.get(timeframe[j]);
				if (v == null) {
					v = "&nbsp;";
				}
				msp_outages = msp_outages + "<td class=\"ccac\">" + v + "</td>";
			}
			String chart = build8WeekTrend((String) t.get("current"));

			msp_outages = msp_outages + "<td class=\"ccbb\">" + chart + "</td>";
			msp_outages = msp_outages + "</tr>\n";
		}
		msp_outages = msp_outages + "</table>\n";
		return msp_outages;
	}

	void getMatrixData(Vector outages, Vector min_max) {
		int max_min_test = 0;
		int max = 0;
		int min = 999999999;

		String sql = "select lo_ot_name, msp_so_yyyymm, sum(msp_so_site_outage_count_per_day)   from datawarehouse.dbo.msp_site_outages_by_appendix_and_day        join ilex.dbo.ap_appendix_id_list_01 on msp_so_pr_id = lx_pr_id  group by lo_ot_name, msp_so_yyyymm  order by lo_ot_name asc, msp_so_yyyymm asc ";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] row = new String[3];

				row[0] = rs.getString(1);
				row[1] = rs.getString(2);
				row[2] = rs.getString(3);

				max_min_test = rs.getInt(3);

				max = rs.getInt(3);
				if (max_min_test > max) {
					max = max_min_test;
				} else if (max_min_test < min) {
					min = max_min_test;
				}
				outages.add(row);
			}
			min_max.add(Integer.valueOf(min));
			min_max.add(Integer.valueOf(max));

			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException e1) {
				System.out.println(e1);
			}
		}
	}

	void buildTimeFrame(String type, String[] order_index_list,
			String[] labels, String[] display_labels) {
		String sql = "";
		int i;
		if (type.equals("Week")) {
			i = 7;
			sql = "select distinct top 8 yyyywwKey, yyyywwSymbol   from datawarehouse.dbo.DimDateMonthWeek  where yyyywwKey <= datepart(yy, getdate())*100+datepart(wk, getdate())  order by yyyywwKey desc";
		} else {
			i = 11;
			sql = "select distinct top 12 yyyymmKey, yyyymmSymbol   from datawarehouse.dbo.DimDateMonthWeek  where yyyymmKey <= datepart(yy, getdate())*100+datepart(mm, getdate())  order by yyyymmKey desc";
		}
		int imax = i;

		String delimiter = "";
		String order_symb_list1 = "";
		String year_label = "";
		String starting_point = "";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				order_index_list[i] = rs.getString(1);
				display_labels[i] = rs.getString(2);
				order_symb_list1 = rs.getString(2) + delimiter
						+ order_symb_list1;
				delimiter = "|";
				i--;
			}
			if (i == -1) {
				year_label = order_index_list[0].substring(0, 4) + "|"
						+ order_index_list[imax].substring(0, 4);
				starting_point = String.valueOf(Integer
						.parseInt(order_index_list[0]) - 1);
			}
			rs.close();

			labels[0] = order_symb_list1;
			labels[1] = year_label;
			labels[2] = starting_point;
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException e1) {
				System.out.println(e1);
			}
		}
	}

	String build8WeekTrend(String current) {
		NumberFormat nf1 = NumberFormat.getIntegerInstance();

		String[] timeframe = new String[8];
		String[] display_labels = new String[8];
		String[] labels = new String[3];
		Map chart_values = new HashMap();
		String yaxis = "";
		String delimiter = "";
		String ylabel = "";

		buildTimeFrame("Week", timeframe, labels, display_labels);
		int max = getChartData(current, chart_values);

		yaxis = "";
		for (int i = 0; i < timeframe.length; i++) {
			Integer y = (Integer) chart_values.get(timeframe[i]);
			if (y == null) {
				y = Integer.valueOf(-1);
			}
			yaxis = yaxis + delimiter + y.toString();
			delimiter = ",";
		}
		int incr = max / 6;
		delimiter = "";
		for (int k = 1; k < 4; k++) {
			ylabel = ylabel + delimiter + nf1.format(incr * k);
			delimiter = "|";
		}
		ylabel = "0" + delimiter + ylabel + delimiter + nf1.format(max);

		current = current.replaceAll(" ", "+");
		current = current.replaceAll("&", "and");
		current = current.replaceAll("'s", "");

		String chart = "<a href=\"#\" onclick=\"changeSrc('http://chart.apis.google.com/chart?cht=lc&amp;chs=420x200&amp;chd=t:"
				+ yaxis
				+ "&amp;"
				+ "chxt=x,x,y&amp;chxl="
				+ "0:|"
				+ labels[0]
				+ "|"
				+ "1:|"
				+ labels[1]
				+ "|"
				+ "2:|"
				+ ylabel
				+ "&amp;"
				+ "chco=cad384,7f99b0,5a5a5a,f5533e&amp;chdl=All|DSL|Cable|Other"
				+ "&amp;chtt="
				+ current
				+ "+-+8+Week+Summary&amp;chts=5a5a5a,14')\";>8 Week Trend</a>";
		System.out.println(chart);

		return chart;
	}

	int getChartData(String current, Map outages) {
		int max = 0;
		int min = 0;

		int value = 0;
		String tf = "";
		Double temp = Double.valueOf(0.0D);

		Vector v = new Vector();
		Vector t = new Vector();

		current = current.replace("'", "''");

		String sql = "select msp_so_yyyywk, sum(msp_so_site_outage_count_per_day)    from datawarehouse.dbo.msp_site_outages_by_appendix_and_day         join ilex.dbo.ap_appendix_id_list_01 on msp_so_pr_id = lx_pr_id  and lo_ot_name = '"
				+

				current
				+ "' "
				+ " group by msp_so_yyyywk "
				+ " order by msp_so_yyyywk asc ";

		System.out.println(sql);
		outages.clear();
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				value = rs.getInt(2);
				tf = rs.getString(1);
				if (value > max) {
					max = value;
				}
				v.add(Integer.valueOf(value));
				t.add(tf);
			}
			rs.close();
			for (int i = 0; i < v.size(); i++) {
				value = ((Integer) v.get(i)).intValue();
				temp = Double.valueOf(value * 1.0D);
				temp = Double.valueOf(temp.doubleValue() / max * 100.0D);
				value = temp.intValue();
				outages.put((String) t.get(i), Integer.valueOf(value));
			}
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException e1) {
				System.out.println(e1);
			}
		}
		return max;
	}

	public static void main(String[] args) {
	}
}
