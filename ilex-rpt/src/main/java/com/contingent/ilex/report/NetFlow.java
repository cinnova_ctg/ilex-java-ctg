package com.contingent.ilex.report;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class NetFlow extends DispatchAction {
	DataAccess da = new DataAccess();

	public ActionForward usage3(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "usage3";

		String int1 = request.getParameter("int");
		if ((int1 == null) || (int1.equals(""))) {
			int1 = "26";
		}
		request.setAttribute("Ch1", getFrequncyData(request, "26"));
		request.setAttribute("Ch2", getFrequncyData(request, "3"));
		getTopSendRec(request, int1);

		return mapping.findForward(forward_key);
	}

	String getFrequncyData(HttpServletRequest request, String interface1) {
		String xmldata = "";

		String sp = "{call datawarehouse.dbo.ax_frequency_distribution_02 (?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sp);
			stmt.setInt("@interface", Integer.parseInt(interface1));
			stmt.registerOutParameter("@chartdata", 12);

			stmt.execute();

			xmldata = stmt.getString("@chartdata");

			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return xmldata;
	}

	void getTopSendRec(HttpServletRequest request, String interface1) {
		String sp = "{call datawarehouse.dbo.ax_sender_top_5_by_interface_01 (?, ?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sp);
			stmt.setInt("@interfacedID", Integer.parseInt(interface1));
			stmt.registerOutParameter("@inchart", 12);
			stmt.registerOutParameter("@outchart", 12);

			stmt.execute();

			request.setAttribute("inchart", stmt.getString("@inchart"));
			request.setAttribute("outchart", stmt.getString("@outchart"));

			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public ActionForward usage1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "usage1";

		String name = getRequestParameter(request, "name");
		String identifier = getRequestParameter(request, "identifier");
		String i1 = getRequestParameter(request, "i1");
		String i2 = getRequestParameter(request, "i2");

		getdevices(request);
		if ((!identifier.equals("")) && (!i1.equals(""))) {
			getHistory(request, identifier, i1);
		}
		request.setAttribute("name", name);
		request.setAttribute("identifier", identifier);
		request.setAttribute("i1", i1);
		request.setAttribute("i2", i2);

		return mapping.findForward(forward_key);
	}

	public ActionForward usage2(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "usage2";

		String name = getRequestParameter(request, "name");
		String identifier = getRequestParameter(request, "identifier");
		String i1 = getRequestParameter(request, "i1");
		String i2 = getRequestParameter(request, "i2");
		String from_date = getDateRequestParameter(request, "from_date");

		getWANdevices(request);
		if ((!identifier.equals("")) && (!i1.equals(""))) {
			getChartData(request, identifier, i1, from_date);
			getTableDate(request, identifier, i1, from_date);
		}
		request.setAttribute("name", name);
		request.setAttribute("identifier", identifier);
		request.setAttribute("i1", i1);
		request.setAttribute("i2", i2);
		request.setAttribute("from_date", from_date);

		return mapping.findForward(forward_key);
	}

	void getHistory(HttpServletRequest request, String identifier, String i1) {
		String sp = "{call nf_retrieve_usage_all_interfaces_byte_history_01 (?, ?, ?, ?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sp);
			stmt.setInt("@wug_dv_id", Integer.parseInt(identifier));
			stmt.setInt("@interface", Integer.parseInt(i1));
			stmt.registerOutParameter("@input_series", 12);
			stmt.registerOutParameter("@output_series", 12);
			stmt.registerOutParameter("@categories", 12);

			stmt.execute();

			request.setAttribute("input_series",
					stmt.getString("@input_series"));
			request.setAttribute("output_series",
					stmt.getString("@output_series"));
			request.setAttribute("categories", stmt.getString("@categories"));

			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	void getChartData(HttpServletRequest request, String identifier, String i1,
			String from_date) {
		String sp = "{call datawarehouse.dbo.ax_nf_wan_charts_01 (?, ?, ?, ?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sp);
			stmt.setString("@dt_stamp", from_date);
			stmt.setInt("@interfaceID", Integer.parseInt(i1));
			stmt.registerOutParameter("@input", 12);
			stmt.registerOutParameter("@output", 12);
			stmt.registerOutParameter("@combined", 12);

			stmt.execute();

			request.setAttribute("input", stmt.getString("@input"));
			request.setAttribute("output", stmt.getString("@output"));
			request.setAttribute("combined", stmt.getString("@combined"));

			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	void getTableDate(HttpServletRequest request, String identifier, String i1,
			String from_date) {
		DecimalFormat nf = new DecimalFormat("#,###");

		String title = "<tr><td>&nbsp;&nbsp;Hour&nbsp;&nbsp;</td><td align=\"right\">&nbsp;&nbsp;Total&nbsp;Bytes&nbsp;&nbsp;</td><td align=\"right\">&nbsp;&nbsp;Total&nbsp;Bits&nbsp;&nbsp;</td></tr>\n";
		String table_input = title;
		String table_output = title;

		String t1 = "";
		int input_sum = 0;
		int output_sum = 0;

		String sp = "{call datawarehouse.dbo.ax_nf_wan_table_01 (?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sp);
			stmt.setString("@dt_stamp", from_date);
			stmt.setInt("@interfaceID", Integer.parseInt(i1));

			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				if (rs.getString(1).equals("Input")) {
					t1 = rs.getString(2);
					t1 = t1.substring(11, 16);
					input_sum += rs.getInt(3);
					table_input = table_input + "<tr><td>&nbsp;" + t1
							+ "&nbsp;</td><td align=\"right\">&nbsp;&nbsp;"
							+ nf.format(rs.getInt(3))
							+ "</td><td align=\"right\">&nbsp;&nbsp;"
							+ nf.format(rs.getInt(3) * 8 / 3600)
							+ "</td></tr>\n";
				} else {
					t1 = rs.getString(2);
					t1 = t1.substring(11, 16);
					output_sum += rs.getInt(3);
					table_output = table_output + "<tr><td>&nbsp;" + t1
							+ "&nbsp;</td><td align=\"right\">&nbsp;&nbsp;"
							+ nf.format(rs.getInt(3))
							+ "</td><td align=\"right\">&nbsp;&nbsp;"
							+ nf.format(rs.getInt(3) * 8 / 3600)
							+ "</td></tr>\n";
				}
			}
			stmt.close();

			table_input = table_input
					+ "<tr><td>&nbsp;Total&nbsp;</td><td align=\"right\">&nbsp;&nbsp;"
					+ nf.format(input_sum)
					+ "</td><td align=\"right\">&nbsp;&nbsp;"
					+ nf.format(input_sum * 8 / 86400) + "</td></tr>\n";
			table_output = table_output
					+ "<tr><td>&nbsp;Total&nbsp;</td><td align=\"right\">&nbsp;&nbsp;"
					+ nf.format(output_sum)
					+ "</td><td align=\"right\">&nbsp;&nbsp;"
					+ nf.format(output_sum * 8 / 86400) + "</td></tr>\n";

			request.setAttribute("table_input", table_input);
			request.setAttribute("table_output", table_output);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public ActionForward getUsageDataStream(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "getUsageDataStream";

		String[] l_and_v = getUsageDataStream(request);

		String dataParameters = "&label=" + l_and_v[1] + "&value=" + l_and_v[0];

		System.out.println(dataParameters);

		request.setAttribute("data", dataParameters);

		return mapping.findForward(forward_key);
	}

	String[] getUsageDataStream(HttpServletRequest request) {
		String sp = "{call nf_retrieve_usage_all_interfaces_byte_02 (?, ?, ?, ?, ?)}";
		String[] label_and_value = new String[2];

		String identifier = getRequestParameter(request, "identifier");
		String i1 = getRequestParameter(request, "i1");
		String i2 = getRequestParameter(request, "i2");
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sp);
			stmt.setInt("@wug_dv_id", Integer.parseInt(identifier));
			stmt.setInt("@interface", Integer.parseInt(i1));
			stmt.setString("@direction", i2);
			stmt.registerOutParameter("@values", 12);
			stmt.registerOutParameter("@label", 12);
			stmt.execute();
			label_and_value[0] = stmt.getString("@values");
			label_and_value[1] = stmt.getString("@label");
		} catch (Exception e) {
			System.out.println(e);
		}
		return label_and_value;
	}

	public ActionForward devices(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "devices";

		return mapping.findForward(forward_key);
	}

	void getdevices(HttpServletRequest request) {
		String c_device = "";
		String p_device = "";
		String sql = getSQLDeviceList();

		Map<String, Vector<Map<String, String>>> device_details = new HashMap();
		Vector<String> device_list = new Vector();
		try {
			ResultSet rs = this.da.getResultSet(sql);
			if (rs.next()) {
				Vector<Map<String, String>> interface_list = new Vector();
				Map<String, String> interface_details = new HashMap();

				c_device = rs.getString("nf_sr_sdisplayname");
				p_device = c_device;
				interface_details.put("nf_si_ninterfaceid",
						rs.getString("nf_si_ninterfaceid"));
				interface_details.put("nf_sr_dv_id",
						rs.getString("nf_sr_dv_id"));
				interface_details.put("nf_si_nsnmpindex",
						rs.getString("nf_si_nsnmpindex"));
				interface_list.add(interface_details);
				while (rs.next()) {
					c_device = rs.getString("nf_sr_sdisplayname");
					if (!c_device.equals(p_device)) {
						device_list.add(p_device);
						device_details.put(p_device, interface_list);

						p_device = c_device;
						interface_list = new Vector();
					}
					interface_details = new HashMap();
					interface_details.put("nf_si_ninterfaceid",
							rs.getString("nf_si_ninterfaceid"));
					interface_details.put("nf_sr_dv_id",
							rs.getString("nf_sr_dv_id"));
					interface_details.put("nf_si_nsnmpindex",
							rs.getString("nf_si_nsnmpindex"));
					interface_list.add(interface_details);
				}
				if (interface_list.size() > 0) {
					device_list.add(c_device);
					device_details.put(c_device, interface_list);
				}
			}
			rs.close();

			request.setAttribute("device_list", device_list);
			request.setAttribute("device_details", device_details);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	void getWANdevices(HttpServletRequest request) {
		String c_device = "";
		String p_device = "";
		String sql = getSQLWANDeviceList();

		Map<String, Vector<Map<String, String>>> device_details = new HashMap();
		Vector<String> device_list = new Vector();
		try {
			ResultSet rs = this.da.getResultSet(sql);
			if (rs.next()) {
				Vector<Map<String, String>> interface_list = new Vector();
				Map<String, String> interface_details = new HashMap();

				c_device = rs.getString("nf_sr_sdisplayname");
				p_device = c_device;
				interface_details.put("nf_si_ninterfaceid",
						rs.getString("nf_si_ninterfaceid"));
				interface_details.put("nf_sr_dv_id",
						rs.getString("nf_sr_dv_id"));
				interface_details.put("nf_si_nsnmpindex",
						rs.getString("nf_si_nsnmpindex"));
				interface_list.add(interface_details);
				while (rs.next()) {
					c_device = rs.getString("nf_sr_sdisplayname");
					if (!c_device.equals(p_device)) {
						device_list.add(p_device);
						device_details.put(p_device, interface_list);

						p_device = c_device;
						interface_list = new Vector();
					}
					interface_details = new HashMap();
					interface_details.put("nf_si_ninterfaceid",
							rs.getString("nf_si_ninterfaceid"));
					interface_details.put("nf_sr_dv_id",
							rs.getString("nf_sr_dv_id"));
					interface_details.put("nf_si_nsnmpindex",
							rs.getString("nf_si_nsnmpindex"));
					interface_list.add(interface_details);
				}
				if (interface_list.size() > 0) {
					device_list.add(c_device);
					device_details.put(c_device, interface_list);
				}
			}
			rs.close();

			request.setAttribute("device_list", device_list);
			request.setAttribute("device_details", device_details);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	String getSQLDeviceList() {
		String sql = "select nf_sr_sdisplayname, nf_sr_dv_id, nf_sm_nsourceid, nf_sm_source_ip_address, nf_si_ninterfaceid, nf_si_nsnmpindex   from dbo.nf_device        join dbo.nf_source on nf_sr_dv_id = nf_sm_sr_dv_id        join dbo.nf_source_interface_map on nf_sm_nsourceid = nf_si_sm_nsourceid order by nf_sr_sdisplayname, nf_si_nsnmpindex";

		return sql;
	}

	String getSQLWANDeviceList() {
		String sql = "select nf_sr_sdisplayname, nf_sr_dv_id, nf_sm_nsourceid, nf_sm_source_ip_address, nf_si_ninterfaceid, nf_si_nsnmpindex   from dbo.nf_device        join dbo.nf_source on nf_sr_dv_id = nf_sm_sr_dv_id        join dbo.nf_source_interface_map on nf_sm_nsourceid = nf_si_sm_nsourceid  where nf_si_nsnmpindex = 5order by nf_sr_sdisplayname, nf_si_nsnmpindex";

		return sql;
	}

	String getRequestParameter(HttpServletRequest request, String parameter) {
		String value = request.getParameter(parameter);
		if (value == null) {
			value = "";
		} else {
			value = value.trim();
		}
		return value;
	}

	String getDateRequestParameter(HttpServletRequest request, String parameter) {
		SimpleDateFormat sd1 = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sd2 = new SimpleDateFormat("yyyy-MM-dd");
		GregorianCalendar check_date = new GregorianCalendar();

		String value = request.getParameter(parameter);
		if (value == null) {
			value = "";
		} else {
			value = value.trim();
		}
		if (value.equals("")) {
			check_date.set(check_date.get(1), check_date.get(2),
					check_date.get(5));
			check_date.add(5, -1);
		} else {
			try {
				check_date.setTime(sd1.parse(value));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return sd2.format(check_date.getTime());
	}
}
