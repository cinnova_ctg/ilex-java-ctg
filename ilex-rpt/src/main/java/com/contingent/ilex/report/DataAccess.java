package com.contingent.ilex.report;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class DataAccess {
	public static Connection databaseConnection = null;
	private Statement sql;

	public DataAccess() {
		if (databaseConnection == null) {
			try {
				Class.forName("net.sourceforge.jtds.jdbc.Driver");
			} catch (Exception e) {
				System.out.println("Class.forName Failed with error: " + e);
			}
			try {
				String db_url = "jdbc:jtds:sqlserver://192.168.32.53:1433/ilex;user=ilex;password=0s1adFi!";
				// String db_url =
				// "jdbc:jtds:sqlserver://192.168.0.150:1433/ilex_production;user=cinnova-ilex;password=cinnova12345";
				// System.out.println(db_url);

				databaseConnection = DriverManager.getConnection(db_url);
				System.out.println("Connection established");
			} catch (Exception sqle) {
				System.out.println("Connection Failed with error: " + sqle);
			}
		}
	}

	public ResultSet getResultSet(String sql_to_execute) {
		ResultSet rs_return = null;
		try {
			this.sql = databaseConnection.createStatement();
			rs_return = this.sql.executeQuery("use ilex; " + sql_to_execute);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
			rs_return = null;
		}
		return rs_return;
	}

        public void reconnect(){
            if (databaseConnection == null) {
                try {
                    Class.forName("net.sourceforge.jtds.jdbc.Driver");
                } catch (Exception e) {
                    System.out.println("Class.forName Failed with error: " + e);
                }
                try {
                    String db_url = "jdbc:jtds:sqlserver://192.168.32.53:1433/ilex;user=ilex;password=0s1adFi!";

                    databaseConnection = DriverManager.getConnection(db_url);
                    System.out.println("Connection established");
                } catch (Exception sqle) {
                    System.out.println("Connection Failed with error: " + sqle);
                }
            }
        }
        
	public boolean executeQuery(String sql_to_execute) {
                reconnect();
		boolean executeStatus = false;
		try {
			this.sql = databaseConnection.createStatement();
			this.sql.execute("use ilex; " + sql_to_execute);
			executeStatus = true;
		} catch (Exception e) {
			System.out.println(e + " " + sql_to_execute);
			return executeStatus;
		}
		return executeStatus;
	}

	public String getValues1(String sql_to_execute) {
		String values = "";
		String delimiter = "";
		try {
			this.sql = databaseConnection.createStatement();
			ResultSet rs = this.sql.executeQuery("use ilex; " + sql_to_execute);
			while (rs.next()) {
				values = values + delimiter + rs.getString(1);
				delimiter = "~";
			}
		} catch (Exception e) {
			System.out.println(e);
			values = null;
		}
		return values;
	}

	public Map<String, String> getSingleRowMap(String sql_to_execute) {
		int column_count = 0;
		Map<String, String> row_data = new HashMap();
		try {
			Statement stmt = databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sql_to_execute);
			ResultSetMetaData rsmd = rs.getMetaData();

			column_count = rsmd.getColumnCount();
			if (rs.next()) {
				for (int i = 1; i <= column_count; i++) {
					String cn = rsmd.getColumnName(i);
					String cv = rs.getString(cn);
					if (rs.wasNull()) {
						cv = "";
					}
					row_data.put(cn, cv);
				}
			}
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return row_data;
	}
	
	public Vector<String[]> getClientJobAppendixList(
			String sql_to_execute) {
		
		Vector<String[]> rows = new Vector();

		int column_count = 0;
		try {
			Statement stmt = databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sql_to_execute);
			ResultSetMetaData rsmd = rs.getMetaData();

			column_count = rsmd.getColumnCount();
			String[] row = null;
			while (rs.next()) {
				 row = new String[column_count];
				
				row[0] = rs.getString("ac_du_name");
				row[1] = rs.getString("project");
				row[2] = rs.getString("job");
				row[3] = rs.getString("customer");
				
				rows.add(row);
			}
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return rows;
	}
	
	public Vector<String[]> getPPSNonPPSData(
			String sql_to_execute) {
		
		Vector<String[]> rows = new Vector();

		int column_count = 0;
		try {
			Statement stmt = databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sql_to_execute);
			ResultSetMetaData rsmd = rs.getMetaData();

			column_count = rsmd.getColumnCount();
			String[] row = null;
			while (rs.next()) {
				 row = new String[column_count];
				
				row[0] = rs.getString("ac_du_name");
				row[1] = rs.getString("count");				
				rows.add(row);
			}
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return rows;
	}
	
	

	public Vector<String[]> getVectorListForMultipleColumns(
			String sql_to_execute) {
		Vector<String[]> rows = new Vector();

		int column_count = 0;
		try {
			Statement stmt = databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sql_to_execute);
			ResultSetMetaData rsmd = rs.getMetaData();

			column_count = rsmd.getColumnCount();
			while (rs.next()) {
				String[] row = new String[column_count];
				for (int i = 0; i < column_count; i++) {
					String cn = rsmd.getColumnName(i + 1);
					String cv = rs.getString(cn);
					if (rs.wasNull()) {
						cv = "";
					}
					row[i] = cv;
				}
				rows.add(row);
			}
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return rows;
	}

	public Map<String, String> getMapMultipleRowsWithTwoColumns(
			String sql_to_execute) {
		Map<String, String> row_data = new HashMap();
		try {
			Statement stmt = databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sql_to_execute);
			while (rs.next()) {
				String key = rs.getString(1);
				if (rs.wasNull()) {
					key = "";
				}
				String value = rs.getString(2);
				if (rs.wasNull()) {
					value = "";
				}
				row_data.put(key, value);
			}
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return row_data;
	}

	public Map<String, Map<String, String>> getMapHiearchyForMultipleRows(
			String sql_to_execute, String key) {
		Map<String, Map<String, String>> rows = new HashMap();
		Map<String, String> row = new HashMap();
		int column_count = 0;
		try {
			Statement stmt = databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(sql_to_execute);
			ResultSetMetaData rsmd = rs.getMetaData();

			column_count = rsmd.getColumnCount();
			while (rs.next()) {
				row = new HashMap();
				for (int i = 0; i < column_count; i++) {
					String cn = rsmd.getColumnName(i + 1);
					String cv = rs.getString(cn);
					if (rs.wasNull()) {
						cv = "";
					}
					row.put(cn, cv);
				}
				rows.put(rs.getString(key), row);
			}
			stmt.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return rows;
	}
}
