package com.contingent.ilex.report;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import module.OpsSummaryClientHq;
import module.OpsSummaryClientSite;
import module.OpsSummaryCnsQa;
import module.OpsSummaryData2;
import module.OpsSummaryDataClientJobAppendix;
import module.OpsSummaryFuture;
import module.OpsSummaryLUA;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.JSONArray;
import org.json.JSONObject;


public class TopLevel2 extends DispatchAction {
	int BDM = 0;
	int SPM = 1;
	int PERIOD = 2;
	int HI = 0;
	int LOW = 1;
	int BENCHMARK = 2;
	int SELECTED = 3;
	int TOTAL = 4;
	String[] DEFAULT_PARAMETERS = { "%", "%", "" };
	String SQL = "use datawarehouse; select sum(ac_os_pf_expense), sum(ac_os_expense), " +
			"convert(int, sum(ac_os_revenue))/1000 as revenue, sum(ac_os_count), convert(decimal(3,2), " +
			"case when (sum(ac_os_revenue) > 0.0) then 1-(sum(ac_os_pf_expense)/sum(ac_os_revenue)) else 0.0 end) as pf_vgpm, " +
			"convert(decimal(3,2), case when (sum(ac_os_revenue) > 0.0) then 1-(sum(ac_os_expense)/sum(ac_os_revenue)) else 0.0 end) as vgpm " +
			"from dbo.ac_fact2_ops_summary_main ";
	String WHERE = "where ac_os_day_index = 'period' and ac_os_bdm = '-bdm-' and ac_os_spm = '-spm-'";
	String SQL_INVOICE_DELAY = "select ac_os_bdm, ac_os_spm, ac_id_complete_delay, ac_id_closed_delay, ac_id_real_invoice_delay  " +
			"from datawarehouse.dbo.ac_fact2_ops_summary_invoice_delays ";
	String SQL_OOB = "select ac_ob_oob_condition, ac_ob_count from datawarehouse.dbo.ac_fact2_ops_summary_oob ";
	String WHERE_OOB = "where ac_os_bdm = '-bdm-' and ac_os_spm = '-spm-'";
	String SQL_PO_TYPES = "select ac_op_type, ac_op_percentage from datawarehouse.dbo.ac_fact2_ops_summary_po ";
	DataAccess da = new DataAccess();
	NumberFormat currency = new DecimalFormat("#,###");
	NumberFormat percentage = NumberFormat.getPercentInstance();
	NumberFormat decimal_format = new DecimalFormat("#,###.0");
	NumberFormat vgpm = new DecimalFormat("0.00");

	public ActionForward db_summary(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "db_summary";

		String[] parameters = getParameters(request);

		getOpsSummaryMain4(request, parameters);

		getOpsSummaryMainTop10Revenue(request, parameters);
		getOpsSummaryMainTop10Count(request, parameters);

		getOpsSummaryInvoiceDelays(request, parameters);
		getOpsSummaryInvoiceDelaysAvg(request, parameters[this.PERIOD]);

		getOpsSummaryPOType(request, parameters);
		getOpsSummaryPOTypeAvg(request, parameters[this.PERIOD]);

		buildProductivityValues(request, parameters);

		oob2(request, parameters[this.SPM]);

		getClientJobAppendixDetails(request, parameters);

		pivotUserActions(request, parameters);

		purchasing(request, parameters);

		quality(request, parameters);

		qualityByClientHqAndSite(request, parameters);

		purchasingFuture(request, parameters);

		revenue_by_fte(request, parameters);

		network_help_desk(request, parameters);

		catalystIssues(request);

		healthCallCheck(request, parameters);

		return mapping.findForward(forward_key);
	}

	// getting response from webservice
	private void catalystIssues(HttpServletRequest request) {

		String data = null;
		String myURL = "https://ccc.contingent.com/index.php?cmp=phone_ws&type=catalyst_summary";
		data = getDataFromWeb(myURL, 9999, "\n");

		String[] dat = null;
		List<String[]> cataActionsList = new ArrayList<String[]>();

		if (data == null || data.equals("")) {
			data = "{outstanding:0,new:0,defects:0,tasks:0,change:0,resolved_last_7_days:0,verification:0,clarification:0}";
		}
		dat = data.split("\\|}");

		String[] keyVal1 = new String[2];
		String[] keyVal = dat[0].split(",");
		String[] resArr = new String[8];
		for (int j = 0; j < keyVal.length; j++) {

			keyVal1 = keyVal[j].split(":");

			resArr[j] = keyVal1[1] == null ? "0" : keyVal1[1];

		}

		request.setAttribute("outstanding", resArr[0]);
		request.setAttribute("newstat", resArr[1]);
		request.setAttribute("defects", resArr[2]);
		request.setAttribute("task", resArr[3]);
		request.setAttribute("change", resArr[4]);

		request.setAttribute("resolvedday", resArr[5]);
		request.setAttribute("verification", resArr[6]);
		if (resArr[7].contains("}")) {

			resArr[7] = resArr[7].replace('}', ' ');

		}
		request.setAttribute("clarification", resArr[7]);

	}

	private void getClientJobAppendixDetails(HttpServletRequest request,
			String[] p) {
		new OpsSummaryDataClientJobAppendix(request, p[this.PERIOD], this.da,
				p[this.SPM]);
	}

	String[] getParameters(HttpServletRequest request) {
		String[] p = new String[3];

		String bdm = request.getParameter("bdm");
		if ((bdm == null) || (bdm.equals(""))) {
			bdm = "%";
		}
		String spm = request.getParameter("spm");
		if ((spm == null) || (spm.equals(""))) {
			spm = "%";
		}
		String period = request.getParameter("period");
		if ((period == null) || (period.equals(""))) {
			period = "30";
		}
		p[this.BDM] = bdm;
		p[this.SPM] = spm;
		p[this.PERIOD] = period;

		request.setAttribute("bdm", bdm);
		request.setAttribute("spm", spm);
		request.setAttribute("period", period);

		return p;
	}

	private String getData(String url, int timeout) {
		try {
			URL u = new URL(url);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.setConnectTimeout(timeout);
			c.setReadTimeout(timeout);
			c.connect();
			int status = c.getResponseCode();
			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(
						c.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();
				return sb.toString();
			}
		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private List<String[]> calculteVal(List<String[]> list, String name,
			String currentCount) {
		if (list.size() > 0) {
			boolean flag = false;
			for (int j = 0; j < list.size(); j++) {
				String[] valArr = list.get(j);
				if (name.equals(valArr[0])) {

					int val1 = Integer.parseInt(currentCount);
					int val2 = Integer.parseInt(valArr[1]);
					list.remove(j);
					list.add(new String[] { name, String.valueOf(val1 + val2) });
					flag = true;
					break;
				}

			}
			if (!flag) {
				/*
				 * if record not found then add as new record.
				 */
				list.add(new String[] { name, currentCount });
			}
		} else {
			list.add(new String[] { name, currentCount });
		}
		return list;
	}

	private void pivotUserActions(HttpServletRequest request, String[] p) {

		String data = null;
		String days = p[this.PERIOD];
		String selectedSPM = p[this.SPM];

		try {
			String myURL = "https://ccc.contingent.com/index.php?cmp=phone_ws&type=pivot_user_actions&period="
					+ days;
			data = this.getData(myURL, 9999);
			try {
				List<String[]> ppsActionsList = new ArrayList<String[]>();
				List<String[]> ppsNonPPSJobsList = new ArrayList<String[]>();
				List<String[]> ppsNonPPSCustomerList = new ArrayList<String[]>();
				List<String[]> ppsNonPPSProjectList = new ArrayList<String[]>();

				List<String[]> nonPPSActionsList = new ArrayList<String[]>();

				if (!data.contains("connection failed")) {
					JSONObject st = new JSONObject(data);
					JSONArray jsonMainArr = st.getJSONArray("PPS");

					for (int i = 0; i < jsonMainArr.length(); i++) { // **line
																		// 2**
						JSONObject childJSONObject = jsonMainArr
								.getJSONObject(i);
						String name = childJSONObject.getString("Name");

						ppsActionsList.add(new String[] { name,
								childJSONObject.getString("Count") });
						ppsNonPPSCustomerList.add(new String[] { name,
								childJSONObject.getString("Customers") });
						ppsNonPPSJobsList.add(new String[] { name,
								childJSONObject.getString("Jobs") });
						ppsNonPPSProjectList.add(new String[] { name,
								childJSONObject.getString("Appendices") });

					}

					jsonMainArr = st.getJSONArray("NON PPS");

					for (int i = 0; i < jsonMainArr.length(); i++) { // **line
																		// 2**
						JSONObject childJSONObject = jsonMainArr
								.getJSONObject(i);
						String name = childJSONObject.getString("Name");
						nonPPSActionsList.add(new String[] { name,
								childJSONObject.getString("Count") });

						ppsNonPPSCustomerList = calculteVal(
								ppsNonPPSCustomerList, name,
								childJSONObject.getString("Customers"));
						ppsNonPPSJobsList = calculteVal(ppsNonPPSJobsList,
								name, childJSONObject.getString("Jobs"));
						ppsNonPPSProjectList = calculteVal(
								ppsNonPPSProjectList, name,
								childJSONObject.getString("Appendices"));

					}

				}

				request.setAttribute(
						"ppsActionsList",
						computeMatrixRowStatisticsForHealthCall(ppsActionsList,
								selectedSPM));
				request.setAttribute(
						"nonPPSActionsList",
						computeMatrixRowStatisticsForHealthCall(
								nonPPSActionsList, selectedSPM));
				request.setAttribute(
						"ppsNonPPSCustomerList",
						computeMatrixRowStatisticsForHealthCall(
								ppsNonPPSCustomerList, selectedSPM));
				request.setAttribute(
						"ppsNonPPSJobsList",
						computeMatrixRowStatisticsForHealthCall(
								ppsNonPPSJobsList, selectedSPM));
				request.setAttribute(
						"ppsNonPPSProjectList",
						computeMatrixRowStatisticsForHealthCall(
								ppsNonPPSProjectList, selectedSPM));

			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private void healthCallCheck(HttpServletRequest request, String[] p) {

		String data = null;

		String listOfAllSPM = "";

		String sql = "select spm from datawarehouse.dbo.ac_dimension_ops_summary_managers " +
				"where spm is not null and spm not in ('Shroder, Charles','Supinger, Bob', 'Courtney, Terezia', 'Carlton, Lance')order by spm";

		ResultSet rs = this.da.getResultSet(sql);

		try {
			while (rs.next()) {
				String[] name = rs.getString("spm").split(", ");
				listOfAllSPM += name[1] + " " + name[0] + ", ";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		String selectedSPM = p[this.SPM];
		String days = p[this.PERIOD];

		try {
			String myURL = "https://ccc.contingent.com/index.php?cmp=phone_ws&type=HealthCheckCalls&days="
					+ days;
			data = this.getData(myURL, 9999);

			try {
				double[] arr1 = new double[5];
				double[] arr2 = new double[5];
				List<String[]> hcBeanList = new ArrayList<String[]>();
				if (!data.contains("connection failed")) {
					JSONObject st = new JSONObject(data);
					JSONArray jsonMainArr = st.getJSONArray("Late");

					for (int i = 0; i < jsonMainArr.length(); i++) { // **line
																		// 2**
						JSONObject childJSONObject = jsonMainArr
								.getJSONObject(i);
						String name = childJSONObject.getString("Name");
						if (listOfAllSPM.contains(name)) {
							hcBeanList.add(new String[] { name,
									childJSONObject.getString("Calls") });
						}
					}

					arr1 = computeMatrixRowStatisticsForHealthCall(hcBeanList,
							selectedSPM);

					jsonMainArr = st.getJSONArray("Future");
					hcBeanList = new ArrayList<String[]>();
					for (int i = 0; i < jsonMainArr.length(); i++) { // **line
																		// 2**
						JSONObject childJSONObject = jsonMainArr
								.getJSONObject(i);
						String name = childJSONObject.getString("Name");
						if (listOfAllSPM.contains(name)) {
							hcBeanList.add(new String[] { name,
									childJSONObject.getString("Calls") });
						}
					}
					arr2 = computeMatrixRowStatisticsForHealthCall(hcBeanList,
							selectedSPM);
				}

				request.setAttribute("healthCareLateList", arr1);
				request.setAttribute("healthCareFutureList", arr2);

			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	double[] computeMatrixRowStatisticsForPivotUsers(List<String[]> data_set,
			String selectedSPM) {
		double[] stats = new double[5];

		if (selectedSPM.contains(", ")) {
			String[] ar = selectedSPM.split(", ");
			selectedSPM = ar[1] + " " + ar[0];
		}

		double hi = 0.0D;
		double lo = 0D;
		double sum = 0.0D;
		double avg = 0.0D;
		double selected_value = 0.0D;
		if (data_set.size() > 0) {
			String[] data = data_set.get(0);
			lo = Double.parseDouble(data[1]);
		}

		for (int i = 0; i < data_set.size(); i++) {
			String[] data = data_set.get(i);

			if (!data[0].equals(selectedSPM)) {
				sum += Integer.parseInt(data[1]);
				if (hi < Integer.parseInt(data[1])) {
					hi = Integer.parseInt(data[1]);
				}
				if (lo > Integer.parseInt(data[1])) {
					lo = Integer.parseInt(data[1]);
				}
			} else {
				selected_value = Integer.parseInt(data[1]);
			}
		}
		if (selectedSPM.equals("%") && data_set.size() > 0) {
			avg = sum / (data_set.size());
		} else if (data_set.size() > 0) {
			avg = sum / (data_set.size() - 1);
		}

		vgpm = new DecimalFormat("0.00");
		stats[0] = Double.parseDouble(vgpm.format(selected_value));
		stats[1] = Double.parseDouble(vgpm.format(avg));
		stats[2] = Double.parseDouble(vgpm.format(hi));
		stats[3] = Double.parseDouble(vgpm.format(lo));
		stats[4] = Double.parseDouble(vgpm.format(sum));
		return stats;
	}

	double[] computeMatrixRowStatisticsForHealthCall(List<String[]> data_set,
			String selectedSPM) {
		double[] stats = new double[5];

		if (selectedSPM.contains(", ")) {
			String[] ar = selectedSPM.split(", ");
			selectedSPM = ar[1] + " " + ar[0];
		}

		double hi = 0.0D;
		double lo = 0D;
		double sum = 0.0D;
		double avg = 0.0D;
		double selected_value = 0.0D;
		if (data_set.size() > 0) {
			String[] data = data_set.get(0);
			lo = Double.parseDouble(data[1]);
		}

		for (int i = 0; i < data_set.size(); i++) {
			String[] data = data_set.get(i);

			if (!data[0].equals(selectedSPM)) {
				sum += Integer.parseInt(data[1]);
				if (hi < Integer.parseInt(data[1])) {
					hi = Integer.parseInt(data[1]);
				}
				if (lo > Integer.parseInt(data[1])) {
					lo = Integer.parseInt(data[1]);
				}
			} else {
				selected_value = Integer.parseInt(data[1]);
			}
		}
		if (selectedSPM.equals("%") && data_set.size() > 0) {
			avg = sum / (data_set.size());
		} else if (data_set.size() > 0) {
			avg = sum / (data_set.size() - 1);
		}

		stats[0] = Double.parseDouble(vgpm.format(selected_value));
		stats[1] = Double.parseDouble(vgpm.format(avg));
		stats[2] = Double.parseDouble(vgpm.format(hi));
		stats[3] = Double.parseDouble(vgpm.format(lo));
		stats[4] = Double.parseDouble(vgpm.format(sum));

		return stats;
	}

	void getOpsSummaryMain2(HttpServletRequest request, String[] p) {
		Map<String, Double> revenue = new HashMap();
		Map<String, Double> pf_cost = new HashMap();
		Map<String, Double> cost = new HashMap();
		Map<String, String> spm_map = new HashMap();
		Vector<String> spms = new Vector();

		Double[] legacy_vgp = { Double.valueOf(-1.0D),
				Double.valueOf(9999999.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D), Double.valueOf(0.0D) };
		Double[] legacy_pf_vgp = { Double.valueOf(-1.0D),
				Double.valueOf(9999999.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D), Double.valueOf(0.0D) };
		Double[] legacy_revenue = { Double.valueOf(-1.0D),
				Double.valueOf(9999999.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D), Double.valueOf(0.0D) };

		String sql = "select ac_os_spm, 'Legacy',        sum(ac_os_revenue) as revenue,        sum(ac_os_pf_expense) pf,  " +
				"sum(ac_os_expense) actual   from datawarehouse.dbo.ac_fact2_ops_summary_main  " +
				"where ac_os_day_index = 'period' and ac_os_bdm = '%' and ac_os_spm != '%'        " +
				"and ac_os_spm not in ('Shroder, Charles','Supinger, Bob', 'Epling, Dave')  group by ac_os_spm";

		sql = updateSQL(sql, p[this.PERIOD]);
		System.out.println("getOpsSummaryMain2-XX\n" + sql);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				spm_map.put(rs.getString("ac_os_spm"),
						rs.getString("ac_os_spm"));
				revenue.put(rs.getString("ac_os_spm"),
						Double.valueOf(rs.getDouble("revenue")));
				pf_cost.put(rs.getString("ac_os_spm"),
						Double.valueOf(rs.getDouble("pf")));
				cost.put(rs.getString("ac_os_spm"),
						Double.valueOf(rs.getDouble("Actual")));
			}
			for (Map.Entry<String, String> spm : spm_map.entrySet()) {
				spms.add((String) spm.getKey());
			}
			rs.close();

			legacy_revenue = computeTotal(spms, p[this.SPM], revenue);
			if (p[this.SPM].equals("%")) {
				request.setAttribute("revenue_selected", "");
			} else {
				request.setAttribute("revenue_selected",
						this.currency.format(legacy_revenue[this.SELECTED]
								.doubleValue() / 1000.0D));
			}
			request.setAttribute("revenue_hi", this.currency
					.format(legacy_revenue[this.HI].doubleValue() / 1000.0D));
			request.setAttribute("revenue_low", this.currency
					.format(legacy_revenue[this.LOW].doubleValue() / 1000.0D));
			request.setAttribute("revenue_benchmark",
					this.currency.format(legacy_revenue[this.BENCHMARK]
							.doubleValue() / 1000.0D));
			request.setAttribute("revenue_total", this.currency
					.format(legacy_revenue[this.TOTAL].doubleValue() / 1000.0D));

			legacy_pf_vgp = computeAverageVGP(spms, p[this.SPM], revenue,
					pf_cost);
			if (p[this.SPM].equals("%")) {
				request.setAttribute("pf_vgpm_selected", "");
			} else {
				request.setAttribute("pf_vgpm_selected",
						this.vgpm.format(legacy_pf_vgp[this.SELECTED]));
			}
			request.setAttribute("pf_vgpm_hi",
					this.vgpm.format(legacy_pf_vgp[this.HI]));
			request.setAttribute("pf_vgpm_low",
					this.vgpm.format(legacy_pf_vgp[this.LOW]));
			request.setAttribute("pf_vgpm_benchmark",
					this.vgpm.format(legacy_pf_vgp[this.BENCHMARK]));
			request.setAttribute("pf_vgpm_total",
					this.vgpm.format(legacy_pf_vgp[this.TOTAL]));

			legacy_vgp = computeAverageVGP(spms, p[this.SPM], revenue, cost);
			if (p[this.SPM].equals("%")) {
				request.setAttribute("vgpm_selected", "");
			} else {
				request.setAttribute("vgpm_selected",
						this.vgpm.format(legacy_vgp[this.SELECTED]));
			}
			request.setAttribute("vgpm_hi",
					this.vgpm.format(legacy_vgp[this.HI]));
			request.setAttribute("vgpm_low",
					this.vgpm.format(legacy_vgp[this.LOW]));
			request.setAttribute("vgpm_benchmark",
					this.vgpm.format(legacy_vgp[this.BENCHMARK]));
			request.setAttribute("vgpm_total",
					this.vgpm.format(legacy_vgp[this.TOTAL]));

			getOpsSummaryMain2Temp(request);
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException1) {
			}
		}
		getOpsSummaryMain2Managed(request, p, legacy_revenue, revenue, pf_cost,
				cost, legacy_vgp);
	}

	void getOpsSummaryMain3(HttpServletRequest request, String[] p) {
		Map<String, Double> revenue = new HashMap();
		Map<String, Double> pf_cost = new HashMap();
		Map<String, Double> cost = new HashMap();
		Map<String, String> spm_map = new HashMap();
		Vector<String> spms = new Vector();

		Double[] legacy_vgp = { Double.valueOf(-1.0D),
				Double.valueOf(9999999.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D), Double.valueOf(0.0D) };
		Double[] legacy_pf_vgp = { Double.valueOf(-1.0D),
				Double.valueOf(9999999.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D), Double.valueOf(0.0D) };
		Double[] legacy_revenue = { Double.valueOf(-1.0D),
				Double.valueOf(9999999.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D), Double.valueOf(0.0D) };

		Vector<Double> rev = new Vector();
		Vector<Double> pf_exp = new Vector();
		Vector<Double> exp = new Vector();

		String sql = "select ac_os_spm, 'Legacy',        sum(ac_os_revenue) as revenue,        sum(ac_os_pf_expense) pf,        " +
				"sum(ac_os_expense) actual   from datawarehouse.dbo.ac_fact2_ops_summary_main  where ac_os_day_index = 'period' " +
				"and ac_os_bdm = '%' and ac_os_spm != '%'        and ac_os_spm not in ('Shroder, Charles','Supinger, Bob', 'Epling, Dave')  group by ac_os_spm";

		sql = updateSQL(sql, p[this.PERIOD]);
		System.out.println("getOpsSummaryMain2-XX\n" + sql);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				spm_map.put(rs.getString("ac_os_spm"),
						rs.getString("ac_os_spm"));
				revenue.put(rs.getString("ac_os_spm"),
						Double.valueOf(rs.getDouble("revenue")));
				pf_cost.put(rs.getString("ac_os_spm"),
						Double.valueOf(rs.getDouble("pf")));
				cost.put(rs.getString("ac_os_spm"),
						Double.valueOf(rs.getDouble("Actual")));

				rev.add(Double.valueOf(rs.getDouble("revenue")));
				exp.add(Double.valueOf(rs.getDouble("Actual")));
				pf_exp.add(Double.valueOf(rs.getDouble("pf")));
			}
			FinancialChartAnalysis2 leg_rev = new FinancialChartAnalysis2(
					(Double[]) rev.toArray());
			for (Map.Entry<String, String> spm : spm_map.entrySet()) {
				spms.add((String) spm.getKey());
			}
			rs.close();

			legacy_revenue = computeTotal(spms, p[this.SPM], revenue);
			if (p[this.SPM].equals("%")) {
				request.setAttribute("revenue_selected", "");
			} else {
				request.setAttribute("revenue_selected",
						this.currency.format(legacy_revenue[this.SELECTED]
								.doubleValue() / 1000.0D));
			}
			request.setAttribute("revenue_hi", this.currency
					.format(legacy_revenue[this.HI].doubleValue() / 1000.0D));
			request.setAttribute("revenue_low", this.currency
					.format(legacy_revenue[this.LOW].doubleValue() / 1000.0D));
			request.setAttribute("revenue_benchmark",
					this.currency.format(legacy_revenue[this.BENCHMARK]
							.doubleValue() / 1000.0D));
			request.setAttribute("revenue_total", this.currency
					.format(legacy_revenue[this.TOTAL].doubleValue() / 1000.0D));

			legacy_pf_vgp = computeAverageVGP(spms, p[this.SPM], revenue,
					pf_cost);
			if (p[this.SPM].equals("%")) {
				request.setAttribute("pf_vgpm_selected", "");
			} else {
				request.setAttribute("pf_vgpm_selected",
						this.vgpm.format(legacy_pf_vgp[this.SELECTED]));
			}
			request.setAttribute("pf_vgpm_hi",
					this.vgpm.format(legacy_pf_vgp[this.HI]));
			request.setAttribute("pf_vgpm_low",
					this.vgpm.format(legacy_pf_vgp[this.LOW]));
			request.setAttribute("pf_vgpm_benchmark",
					this.vgpm.format(legacy_pf_vgp[this.BENCHMARK]));
			request.setAttribute("pf_vgpm_total",
					this.vgpm.format(legacy_pf_vgp[this.TOTAL]));

			legacy_vgp = computeAverageVGP(spms, p[this.SPM], revenue, cost);
			if (p[this.SPM].equals("%")) {
				request.setAttribute("vgpm_selected", "");
			} else {
				request.setAttribute("vgpm_selected",
						this.vgpm.format(legacy_vgp[this.SELECTED]));
			}
			request.setAttribute("vgpm_hi",
					this.vgpm.format(legacy_vgp[this.HI]));
			request.setAttribute("vgpm_low",
					this.vgpm.format(legacy_vgp[this.LOW]));
			request.setAttribute("vgpm_benchmark",
					this.vgpm.format(legacy_vgp[this.BENCHMARK]));
			request.setAttribute("vgpm_total",
					this.vgpm.format(legacy_vgp[this.TOTAL]));

			getOpsSummaryMain2Temp(request);
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		getOpsSummaryMain2Managed(request, p, legacy_revenue, revenue, pf_cost,
				cost, legacy_vgp);
	}

	void getOpsSummaryMain4(HttpServletRequest request, String[] p) {
		String sp_spm = p[this.SPM].equals("%") ? "" : p[this.SPM];
		String sql = "exec datawarehouse.dbo.ac_ops_summary_main_02 "
				+ p[this.PERIOD] + ", '" + sp_spm + "'";
		System.out.println(sql);
		try {
			Map<String, Map<String, String>> rows = this.da
					.getMapHiearchyForMultipleRows(sql, "rpt_type");
			request.setAttribute("rows", rows);
		} catch (Exception e) {
			System.out.println(e + " " + sql);
		}
	}

	void getLastUpdated(HttpServletRequest request) {
		String sql = "SELECT TOP 1 cns_task_last_run FROM ilex.dbo.cns_tasks WHERE cns_task_name = 'Operations Summary Report'";
		System.out.println(sql);
		String last_update = "N/A";
		try {
			ResultSet rs = this.da.getResultSet(sql);
			while (rs.next()) {
				last_update = rs.getString("cns_task_last_run");
			}
		} catch (Exception e) {
			System.out.println(e + " " + sql);
		}
		request.setAttribute("last_update", last_update);
	}

	void getOpsSummaryMain2Managed(HttpServletRequest request, String[] p,
			Double[] dv_legacy, Map<String, Double> revenue_legacy,
			Map<String, Double> pfc_legacy, Map<String, Double> ac_legacy,
			Double[] legacy_vgp) {

		String sql = "select ac_os_day_index,        "
				+ "sum(ac_os_revenue) as revenue,        "
				+ "sum(ac_os_pf_expense) pf,        "
				+ "sum(ac_os_expense) actual   "
				+ "from datawarehouse.dbo.ac_fact2_ops_summary_main_managed  "
				+ "where ac_os_day_index = 'period' " + "and ac_os_bdm = '%' "
				+ "and ac_os_spm != '%'  " + "group by ac_os_day_index";

		sql = updateSQL(sql, p[this.PERIOD]);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				Double revenue = Double.valueOf(rs.getDouble("revenue"));
				Double pf_cost = Double.valueOf(rs.getDouble("pf"));
				Double cost = Double.valueOf(rs.getDouble("actual"));
				Double vgp;
				if (revenue.doubleValue() > 0.0D) {
					vgp = Double.valueOf(1.0D - cost.doubleValue()
							/ revenue.doubleValue());
				} else {
					vgp = Double.valueOf(0.0D);
				}
				request.setAttribute("mrevenue_selected", "");
				request.setAttribute("mrevenue_hi", "");
				request.setAttribute("mrevenue_low", "");
				request.setAttribute("mrevenue_benchmark", "");
				request.setAttribute("mrevenue_total",
						this.currency.format(revenue.doubleValue() / 1000.0D));
				int

				tmp205_202 = this.TOTAL;
				Double[] tmp205_200 = dv_legacy;
				tmp205_200[tmp205_202] = Double.valueOf(tmp205_200[tmp205_202]
						.doubleValue() + revenue.doubleValue());

				request.setAttribute("trevenue_selected", "");
				request.setAttribute("trevenue_hi", "");
				request.setAttribute("trevenue_low", "");
				request.setAttribute("trevenue_benchmark", "");
				request.setAttribute("trevenue_total", this.currency
						.format(dv_legacy[this.TOTAL].doubleValue() / 1000.0D));

				request.setAttribute("mvgpm_selected", "");
				request.setAttribute("mvgpm_hi", "");
				request.setAttribute("mvgpm_low", "");
				request.setAttribute("mvgpm_benchmark", "");
				request.setAttribute("mvgpm_total", this.vgpm.format(vgp));

				Double total_ac_costs = Double.valueOf(0.0D);
				for (Map.Entry<String, Double> costs : ac_legacy.entrySet()) {
					total_ac_costs = Double.valueOf(total_ac_costs
							.doubleValue()
							+ ((Double) costs.getValue()).doubleValue());
				}
				total_ac_costs = Double.valueOf(total_ac_costs.doubleValue()
						+ cost.doubleValue());

				System.out.println("Cost " + total_ac_costs + " "
						+ dv_legacy[this.TOTAL]);
				if (dv_legacy[this.TOTAL].doubleValue() > 0.0D) {
					vgp = Double.valueOf(1.0D - total_ac_costs.doubleValue()
							/ dv_legacy[this.TOTAL].doubleValue());
				} else {
					vgp = Double.valueOf(0.0D);
				}
				request.setAttribute("Amvgpm_selected", "");
				request.setAttribute("Amvgpm_hi", "");
				request.setAttribute("Amvgpm_low", "");
				request.setAttribute("Amvgpm_benchmark", "");
				request.setAttribute("Amvgpm_total", this.vgpm.format(vgp));
			} else {
				if (p[this.SPM].equals("%")) {
					request.setAttribute("mrevenue_selected", "");
				} else {
					request.setAttribute("mrevenue_selected", "");
				}
				request.setAttribute("mrevenue_hi", "");
				request.setAttribute("mrevenue_low", "");
				request.setAttribute("mrevenue_benchmark", "");
				request.setAttribute("mrevenue_total", "");
				if (p[this.SPM].equals("%")) {
					request.setAttribute("trevenue_selected", "");
				} else {
					request.setAttribute("trevenue_selected",
							this.currency.format(dv_legacy[this.SELECTED]
									.doubleValue() / 1000.0D));
				}
				request.setAttribute("trevenue_hi", this.currency
						.format(dv_legacy[this.HI].doubleValue() / 1000.0D));
				request.setAttribute("trevenue_low", this.currency
						.format(dv_legacy[this.LOW].doubleValue() / 1000.0D));
				request.setAttribute("trevenue_benchmark",
						this.currency.format(dv_legacy[this.BENCHMARK]
								.doubleValue() / 1000.0D));
				request.setAttribute("trevenue_total", this.currency
						.format(dv_legacy[this.TOTAL].doubleValue() / 1000.0D));
				if (p[this.SPM].equals("%")) {
					request.setAttribute("mvgpm_selected", "");
				} else {
					request.setAttribute("mvgpm_selected", "");
				}
				request.setAttribute("mvgpm_hi", "");
				request.setAttribute("mvgpm_low", "");
				request.setAttribute("mvgpm_benchmark", "");
				request.setAttribute("mvgpm_total", "");
				if (p[this.SPM].equals("%")) {
					request.setAttribute("Amvgpm_selected", "");
				} else {
					request.setAttribute("Amvgpm_selected",
							this.vgpm.format(legacy_vgp[this.SELECTED]));
				}
				request.setAttribute("Amvgpm_hi",
						this.vgpm.format(legacy_vgp[this.HI]));
				request.setAttribute("Amvgpm_low",
						this.vgpm.format(legacy_vgp[this.LOW]));
				request.setAttribute("Amvgpm_benchmark",
						this.vgpm.format(legacy_vgp[this.BENCHMARK]));
				request.setAttribute("Amvgpm_total",
						this.vgpm.format(legacy_vgp[this.TOTAL]));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	void getOpsSummaryMain2Temp(HttpServletRequest request) {
		request.setAttribute("all_pf_vgpm_selected", "");
		request.setAttribute("all_pf_vgpm_hi", "");
		request.setAttribute("all_pf_vgpm_low", "");
		request.setAttribute("all_pf_vgpm_benchmark", "");
		request.setAttribute("all_pf_vgpm_total", "");

		request.setAttribute("all_vgpm_selected", "");
		request.setAttribute("all_vgpm_hi", "");
		request.setAttribute("all_vgpm_low", "");
		request.setAttribute("all_vgpm_benchmark", "");
		request.setAttribute("all_vgpm_total", "");

		request.setAttribute("msp_pf_vgpm_selected", "");
		request.setAttribute("msp_pf_vgpm_hi", "");
		request.setAttribute("msp_pf_vgpm_low", "");
		request.setAttribute("msp_pf_vgpm_benchmark", "");
		request.setAttribute("msp_pf_vgpm_total", "");

		request.setAttribute("msp_vgpm_selected", "");
		request.setAttribute("msp_vgpm_hi", "");
		request.setAttribute("msp_vgpm_low", "");
		request.setAttribute("msp_vgpm_benchmark", "");
		request.setAttribute("msp_vgpm_total", "");
	}

	Double[] computeAverageVGP(Vector<String> list, String exclude,
			Map<String, Double> revenue, Map<String, Double> cost) {
		Double[] display_values = { Double.valueOf(-1.0D),
				Double.valueOf(9999999.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D), Double.valueOf(0.0D) };

		Double x = Double.valueOf(0.0D);
		Double c = Double.valueOf(0.0D);
		Double r = Double.valueOf(0.0D);
		Double c_sum = Double.valueOf(0.0D);
		Double r_sum = Double.valueOf(0.0D);

		Double c_sum_total = Double.valueOf(0.0D);
		Double r_sum_total = Double.valueOf(0.0D);
		for (int i = 0; i < list.size(); i++) {
			c_sum_total = Double.valueOf(c_sum_total.doubleValue()
					+ ((Double) cost.get(list.get(i))).doubleValue());
			r_sum_total = Double.valueOf(r_sum_total.doubleValue()
					+ ((Double) revenue.get(list.get(i))).doubleValue());

			c = (Double) cost.get(list.get(i));
			r = (Double) revenue.get(list.get(i));
			if (!((String) list.get(i)).equals(exclude)) {
				c_sum = Double.valueOf(c_sum.doubleValue() + c.doubleValue());
				r_sum = Double.valueOf(r_sum.doubleValue() + r.doubleValue());
			}
			x = Double.valueOf(0.0D);
			if (r.doubleValue() > 0.0D) {
				x = Double.valueOf(1.0D - c.doubleValue() / r.doubleValue());
			}
			if (x.doubleValue() > display_values[this.HI].doubleValue()) {
				display_values[this.HI] = x;
			}
			if (x.doubleValue() < display_values[this.LOW].doubleValue()) {
				display_values[this.LOW] = x;
			}
		}
		if (r_sum.doubleValue() > 0.0D) {
			int tmp353_350 = this.BENCHMARK;
			Double[] tmp353_347 = display_values;
			tmp353_347[tmp353_350] = Double.valueOf(tmp353_347[tmp353_350]
					.doubleValue()
					+ (1.0D - c_sum.doubleValue() / r_sum.doubleValue()));
		} else {
			display_values[this.BENCHMARK] = Double.valueOf(0.0D);
		}
		if (r_sum_total.doubleValue() > 0.0D) {
			int tmp406_403 = this.TOTAL;
			Double[] tmp406_400 = display_values;
			tmp406_400[tmp406_403] = Double.valueOf(tmp406_400[tmp406_403]
					.doubleValue()
					+ (1.0D - c_sum_total.doubleValue()
							/ r_sum_total.doubleValue()));
		} else {
			display_values[this.TOTAL] = Double.valueOf(0.0D);
		}
		if (!exclude.equals("%")) {
			c = (Double) cost.get(exclude);
			r = (Double) revenue.get(exclude);
			if (r.doubleValue() > 0.0D) {
				display_values[this.SELECTED] = Double.valueOf(1.0D
						- c.doubleValue() / r.doubleValue());
			}
		} else {
			display_values[this.SELECTED] = Double.valueOf(-1.0D);
		}
		return display_values;
	}

	Double[] computeAverage(Vector<String> list, String exclude,
			Map<String, Double> value1) {
		Double[] display_values = { Double.valueOf(-1.0D),
				Double.valueOf(9999999.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D), Double.valueOf(0.0D) };

		Double cnt = Double.valueOf(0.0D);

		Double x = Double.valueOf(0.0D);
		for (int i = 0; i < list.size(); i++) {
			x = (Double) value1.get(list.get(i));
			int

			tmp86_83 = this.TOTAL;
			Double[] tmp86_80 = display_values;
			tmp86_80[tmp86_83] = Double.valueOf(tmp86_80[tmp86_83]
					.doubleValue()
					+ ((Double) value1.get(list.get(i))).doubleValue());
			if (!((String) list.get(i)).equals(exclude)) {
				int tmp136_133 = this.BENCHMARK;
				Double[] tmp136_130 = display_values;
				tmp136_130[tmp136_133] = Double.valueOf(tmp136_130[tmp136_133]
						.doubleValue() + x.doubleValue());
				cnt = Double.valueOf(cnt.doubleValue() + 1.0D);
			}
		}
		if (x.doubleValue() > display_values[this.HI].doubleValue()) {
			display_values[this.HI] = x;
		}
		if (x.doubleValue() < display_values[this.LOW].doubleValue()) {
			display_values[this.LOW] = x;
		}
		display_values[this.BENCHMARK] = Double
				.valueOf(display_values[this.BENCHMARK].doubleValue()
						/ cnt.doubleValue());

		display_values[this.TOTAL] = Double.valueOf(display_values[this.TOTAL]
				.doubleValue() / list.size());
		if (!exclude.equals("%")) {
			display_values[this.SELECTED] = ((Double) value1.get(exclude));
		} else {
			display_values[this.SELECTED] = Double.valueOf(-1.0D);
		}
		return display_values;
	}

	Double[] computeTotal(Vector<String> list, String exclude,
			Map<String, Double> value1) {
		Double[] display_values = { Double.valueOf(-1.0D),
				Double.valueOf(9999999.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D), Double.valueOf(0.0D) };

		Double cnt = Double.valueOf(0.0D);

		Double x = Double.valueOf(0.0D);
		for (int i = 0; i < list.size(); i++) {
			x = (Double) value1.get(list.get(i));
			int

			tmp86_83 = this.TOTAL;
			Double[] tmp86_80 = display_values;
			tmp86_80[tmp86_83] = Double.valueOf(tmp86_80[tmp86_83]
					.doubleValue() + x.doubleValue());
			if (!((String) list.get(i)).equals(exclude)) {
				int tmp123_120 = this.BENCHMARK;
				Double[] tmp123_117 = display_values;
				tmp123_117[tmp123_120] = Double.valueOf(tmp123_117[tmp123_120]
						.doubleValue() + x.doubleValue());

				cnt = Double.valueOf(cnt.doubleValue() + 1.0D);
			}
			if (x.doubleValue() > display_values[this.HI].doubleValue()) {
				display_values[this.HI] = x;
			}
			if (x.doubleValue() < display_values[this.LOW].doubleValue()) {
				display_values[this.LOW] = x;
			}
		}
		if (!exclude.equals("%")) {
			display_values[this.SELECTED] = ((Double) value1.get(exclude));
		} else {
			display_values[this.SELECTED] = Double.valueOf(-1.0D);
		}
		display_values[this.BENCHMARK] = Double
				.valueOf(display_values[this.BENCHMARK].doubleValue()
						/ cnt.doubleValue());

		return display_values;
	}

	void getOpsSummaryMain(HttpServletRequest request, String[] p) {
		String sql = this.SQL + updateSQL(p);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				request.setAttribute("vgpm", getColumnValue(rs, "vgpm"));
				request.setAttribute("revenue",
						getColumnValueCurrency(rs, "revenue"));
				request.setAttribute("pf_vgpm", getColumnValue(rs, "pf_vgpm"));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	String getColumnValue(ResultSet rs, String column) {
		String column_value = "";
		try {
			column_value = rs.getString(column);
			if (rs.wasNull()) {
				column_value = "N/A";
			}
		} catch (SQLException e) {
			column_value = "E1";
		}
		return column_value;
	}

	String getColumnValueCurrency(ResultSet rs, String column) {
		String column_value = "";
		try {
			column_value = "$" + this.currency.format(rs.getInt(column));
			if (rs.wasNull()) {
				column_value = "N/A";
			}
		} catch (SQLException e) {
			column_value = "E1";
		}
		return column_value;
	}

	void getOpsSummaryMainAvg(HttpServletRequest request, String period) {
		String[] p = { "%", "%", "" };
		p[this.PERIOD] = period;

		String sql = this.SQL + updateSQL(p);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				request.setAttribute("avg_vgpm", rs.getString("vgpm"));
				request.setAttribute("avg_revenue", this.currency
						.format(Integer.parseInt(rs.getString("revenue"))));
				request.setAttribute("avg_pf_vgpm", rs.getString("pf_vgpm"));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	void getOpsSummaryMainTop10Revenue(HttpServletRequest request, String[] p) {
		String sql = "select top 15 left(ac_os_customer,25), convert(int, sum(ac_os_revenue)/1000)  from datawarehouse.dbo.ac_fact2_ops_summary_main ";

		sql = sql + updateSQL(p);

		sql = sql
				+ " group by ac_os_customer  order by sum(ac_os_revenue) desc";

		String t = "";

		String row = "<td class=\"TBCNT004\" width=\"160\" style=\"text-indent:20px;\">-name-</td><td class=\"TBCNT004R\" width=\"75\">$-value-</td></tr>";
		String trow = "";
		String rows = "";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				t = rs.getString(1).replace(" ", "&nbsp;");
				t = t.replace("-", "&ndash;");
				trow = row.replace("-name-", t);
				trow = trow
						.replace("-value-", this.currency.format(Integer
								.parseInt(rs.getString(2))));
				rows = rows + trow;
			}
			rs.close();

			request.setAttribute("revenue10", rows);
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	void getOpsSummaryMainTop10Count(HttpServletRequest request, String[] p) {
		String sql = "select top 15 left(ac_os_customer,25), convert(int, sum(ac_os_count))  from datawarehouse.dbo.ac_fact2_ops_summary_main ";

		sql = sql + updateSQL(p);

		sql = sql + " group by ac_os_customer  order by sum(ac_os_count) desc";

		String t = "";

		String row = "<td class=\"TBCNT004\" width=\"160\" style=\"text-indent:20px;\">-name-</td><td class=\"TBCNT004R\" width=\"75\">-value-</td></tr>";
		String trow = "";
		String rows = "";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				t = rs.getString(1).replace(" ", "&nbsp;");
				t = t.replace("-", "&ndash;");
				trow = row.replace("-name-", t);
				trow = trow
						.replace("-value-", this.currency.format(Integer
								.parseInt(rs.getString(2))));
				rows = rows + trow;
			}
			rs.close();

			request.setAttribute("count10", rows);
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	void getOpsSummaryInvoiceDelays(HttpServletRequest request, String[] p) {
		String sql = this.SQL_INVOICE_DELAY + updateSQL(p);

		System.out.println(sql);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				request.setAttribute("complete_delay",
						getColumnValue(rs, "ac_id_complete_delay"));
				request.setAttribute("closed_delay",
						getColumnValue(rs, "ac_id_closed_delay"));
				request.setAttribute("invoice_delay",
						getColumnValue(rs, "ac_id_real_invoice_delay"));
			} else {
				request.setAttribute("complete_delay", "N/A");
				request.setAttribute("closed_delay", "N/A");
				request.setAttribute("invoice_delay", "N/A");
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	void getOpsSummaryInvoiceDelaysAvg(HttpServletRequest request, String period) {
		String[] p = { "%", "%", "" };
		p[this.PERIOD] = period;

		String sql = this.SQL_INVOICE_DELAY + updateSQL(p);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				request.setAttribute("avg_complete_delay",
						getColumnValue(rs, "ac_id_complete_delay"));
				request.setAttribute("avg_closed_delay",
						getColumnValue(rs, "ac_id_closed_delay"));
				request.setAttribute("avg_invoice_delay",
						getColumnValue(rs, "ac_id_real_invoice_delay"));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	void getOpsSummaryPOType(HttpServletRequest request, String[] p) {
		String sql = this.SQL_PO_TYPES + updateSQL(p);

		ResultSet rs = this.da.getResultSet(sql);

		Map<String, String> po_types = new HashMap();

		po_types.put("M", "0.0%");
		po_types.put("S", "0.0%");
		po_types.put("P", "0.0%");
		try {
			while (rs.next()) {
				po_types.put(rs.getString("ac_op_type"), this.percentage
						.format(rs.getDouble("ac_op_percentage")));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		request.setAttribute("minuteman", po_types.get("M"));
		request.setAttribute("speedpay", po_types.get("S"));
		request.setAttribute("standard", po_types.get("P"));
	}

	void getOpsSummaryPOTypeAvg(HttpServletRequest request, String period) {
		String[] p = { "%", "%", "" };
		p[this.PERIOD] = period;

		String sql = this.SQL_PO_TYPES + updateSQL(p);

		ResultSet rs = this.da.getResultSet(sql);

		Map<String, String> po_types = new HashMap();

		po_types.put("M", "0.0%");
		po_types.put("S", "0.0%");
		po_types.put("P", "0.0%");
		try {
			while (rs.next()) {
				po_types.put(rs.getString("ac_op_type"), this.percentage
						.format(rs.getDouble("ac_op_percentage")));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		request.setAttribute("avg_minuteman", po_types.get("M"));
		request.setAttribute("avg_speedpay", po_types.get("S"));
		request.setAttribute("avg_standard", po_types.get("P"));
	}

	void buildProductivityValues(HttpServletRequest request, String[] p) {
		Map<String, Double> number_employees = new HashMap();
		Map<String, Double> revenue_employees = new HashMap();
		Map<String, Double> jobs_employees = new HashMap();
		Map<String, Double> revenue_job = new HashMap();

		String spm = "";

		String sql = "select ac_os_spm, isnull(ac_pm_head_count,0) as ac_pm_head_count, "
				+ " isnull(ac_pm_job_count/ac_pm_head_count,0) as ac_pm_job_count,"
				+ " isnull(ac_pm_productivity/1000.0,0) as ac_pm_productivity,"
				+ " isnull(ac_pm_revenue/ac_pm_job_count, 0) as rev_per_job "
				+ " from datawarehouse.dbo.ac_fact2_ops_summary_productivity_main "
				+ " where ac_os_spm not in ('Shroder, Charles','Supinger, Bob', 'Courtney, Terezia') "
				+ " and ac_os_day_index = " +

				p[this.PERIOD];
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				spm = rs.getString("ac_os_spm");
				number_employees.put(spm,
						Double.valueOf(rs.getDouble("ac_pm_head_count")));
				revenue_employees.put(spm,
						Double.valueOf(rs.getDouble("ac_pm_productivity")));
				jobs_employees.put(spm,
						Double.valueOf(rs.getDouble("ac_pm_job_count")));
				revenue_job.put(spm,
						Double.valueOf(rs.getDouble("rev_per_job")));
			}
			rs.close();

			request.setAttribute("number_employees",
					analyzeProductivity(number_employees, p[this.SPM], "Total"));
			request.setAttribute(
					"revenue_employees",
					analyzeProductivity(revenue_employees, p[this.SPM],
							"Overall"));
			request.setAttribute("jobs_employees",
					analyzeProductivity(jobs_employees, p[this.SPM], "Overall"));
			request.setAttribute("revenue_job",
					analyzeProductivity(revenue_job, p[this.SPM], "Overall"));
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
				System.out.println(localSQLException.getMessage());
			}
		}
	}

	void purchasing(HttpServletRequest request, String[] p) {
		new OpsSummaryData2(request, p[this.PERIOD], this.da, p[this.SPM]);

		new OpsSummaryLUA(request, p[this.PERIOD], this.da, p[this.SPM]);
	}

	void purchasingFuture(HttpServletRequest request, String[] p) {
		new OpsSummaryFuture(request, p[this.PERIOD], this.da, p[this.SPM]);
	}

	void revenue_by_fte(HttpServletRequest request, String[] p) {
		NumberFormat decimal_format1 = new DecimalFormat("#,###");
		NumberFormat decimal_format2 = new DecimalFormat("#,###.00");

		String sql = "select sum(convert(float, a.ac_os_days_worked)/convert(float, b.ac_os_days_worked)) as fte,        " +
				"(ac_os_revenue/1000.0)/sum(convert(float, a.ac_os_days_worked)/convert(float, b.ac_os_days_worked)) as rev_by_fte  " +
				"from datawarehouse.dbo.ac_fact2_ops_summary_productivity_employees_days_worked a       " +
				"join datawarehouse.dbo.ac_fact2_ops_summary_productivity_workdays b on a.ac_os_day_index = b.ac_os_day_index       " +
				"join datawarehouse.dbo.ac_fact2_ops_summary_revenue c on a.ac_os_day_index = c.ac_os_day_index where a.ac_os_day_index = "
				+

				p[this.PERIOD] + " group by ac_os_revenue";

		Map<String, String> fte = this.da.getSingleRowMap(sql);

		double employee_count = Double.parseDouble((String) fte.get("fte"));
		double employee_revenue = Double.parseDouble((String) fte
				.get("rev_by_fte"));

		request.setAttribute("fte_employee_count",
				decimal_format1.format(employee_count));
		request.setAttribute("fte_employee_revenue",
				decimal_format2.format(employee_revenue));
	}

	void network_help_desk(HttpServletRequest request, String[] p) {
		String sql = "select name, actual  from [datawarehouse].[dbo].[ac_dimension_ops_summary_network]  where period = "
				+

				p[this.PERIOD];

		Map<String, String> network_help_desk = this.da
				.getMapMultipleRowsWithTwoColumns(sql);

		request.setAttribute("network_help_desk", network_help_desk);
	}

	void quality(HttpServletRequest request, String[] p) {
		new OpsSummaryCnsQa(request, p[this.PERIOD], this.da, p[this.SPM]);
	}

	void qualityByClientHqAndSite(HttpServletRequest request, String[] p) {
		new OpsSummaryClientSite(request, p[this.PERIOD], this.da, p[this.SPM]);

		new OpsSummaryClientHq(request, p[this.PERIOD], this.da, p[this.SPM]);
	}

	Map<String, String> analyzeProductivity(Map<String, Double> values,
			String selected, String type) {
		Map<String, String> display_mapping = new HashMap();

		int size = 0;

		Double high = Double.valueOf(0.0D);
		Double low = Double.valueOf(0.0D);
		Double total = Double.valueOf(0.0D);
		Double benchmark = Double.valueOf(0.0D);

		Double next_value = Double.valueOf(0.0D);

		total = (Double) values.get("%");
		values.remove("%");
		size = values.size();

		high = Double.valueOf(0.0D);
		low = total;
		for (Map.Entry<String, Double> value : values.entrySet()) {
			next_value = (Double) value.getValue();
			if (!selected.equals(value.getKey())) {
				benchmark = Double.valueOf(benchmark.doubleValue()
						+ next_value.doubleValue());
			}
			if (next_value.doubleValue() > high.doubleValue()) {
				high = next_value;
			}
			if (next_value.doubleValue() < low.doubleValue()) {
				low = next_value;
			}
		}
		if (!selected.equals("%") && values.size() > 0) {
			benchmark = Double.valueOf(benchmark.doubleValue() / (size - 1));
			try {
				display_mapping.put("selected",
						this.decimal_format.format(values.get(selected)));
			} catch (Exception e) {
				System.out.println(e.getMessage());
				display_mapping.put("selected", "0");
			}
		} else {
			benchmark = Double.valueOf(benchmark.doubleValue() / size);
			display_mapping.put("selected", "0");
		}
		display_mapping.put("high", this.decimal_format.format(high));
		display_mapping.put("low", this.decimal_format.format(low));
		display_mapping.put("total", this.decimal_format.format(total));
		if (selected.equals("%")) {
			if (type.equals("Total")) {
				display_mapping.put("benchmark",
						this.decimal_format.format(benchmark));
			} else {
				display_mapping.put("benchmark",
						this.decimal_format.format(total));
			}
		} else {
			display_mapping.put("benchmark",
					this.decimal_format.format(benchmark));
		}
		return display_mapping;
	}

	Map getOpsSummaryOOB(HttpServletRequest request, String[] p) {
		Map<String, String> oob = initializeOOB();

		String sql = this.SQL_OOB + updateSQL(this.WHERE_OOB, p);

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				oob.put(rs.getString(1), rs.getString(2));
			}
			rs.close();
			oob = totalOOBConditions(oob);
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return oob;
	}

	Map<String, String> totalOOBConditions(Map<String, String> oob) {
		int s1 = 0;

		s1 = Integer.parseInt((String) oob.get("To Be Scheduled"))
				+ Integer.parseInt((String) oob.get("To Be Scheduled Over-1"));
		oob.put("To Be Scheduled Total", String.valueOf(s1));

		s1 = Integer.parseInt((String) oob.get("Scheduled"))
				+ Integer.parseInt((String) oob.get("Scheduled Over-1"));
		oob.put("Scheduled Total", String.valueOf(s1));

		s1 = Integer.parseInt((String) oob.get("Confirmed"))
				+ Integer.parseInt((String) oob.get("Onsite"))
				+ Integer.parseInt((String) oob.get("Onsite Over-1"))
				+ Integer.parseInt((String) oob.get("Offsite"))
				+ Integer.parseInt((String) oob.get("Offsite Over-2"))
				+ Integer.parseInt((String) oob.get("Offsite Over/Del-7"));
		oob.put("In Work", String.valueOf(s1));

		s1 = Integer.parseInt((String) oob.get("Complete"))
				+ Integer.parseInt((String) oob.get("Complete Over-5"));
		oob.put("Complete Total", String.valueOf(s1));
		for (Map.Entry<String, String> c : oob.entrySet()) {
			if (((String) c.getValue()).length() > 3) {
				s1 = Integer.parseInt((String) c.getValue());
				c.setValue(this.currency.format(s1));
			}
		}
		return oob;
	}

	Map<String, String> initializeOOB() {
		Map<String, String> oob = new HashMap();

		ResultSet rs = this.da
				.getResultSet("select * from datawarehouse.dbo.ac_dimension_ops_summary_oob_status");
		try {
			while (rs.next()) {
				oob.put(rs.getString(1), "0");
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return oob;
	}

	String updateSQL(String[] p) {
		String sql = this.WHERE;

		sql = sql.replace("-bdm-", p[this.BDM]);
		sql = sql.replace("-spm-", p[this.SPM]);
		sql = sql.replace("period", p[this.PERIOD]);

		return sql;
	}

	String updateSQL(String where, String[] p) {
		String sql = where;

		sql = sql.replace("-bdm-", p[this.BDM]);
		sql = sql.replace("-spm-", p[this.SPM]);

		return sql;
	}

	String updateSQL(String sql, String p) {
		sql = sql.replace("period", p);

		return sql;
	}

	void oob2(HttpServletRequest request, String spm) {
		Vector<String> oob_condition_list = initializeOOB2();

		Vector<String> oob_spm_list = initializeSPM2();

		Map<String, Map<String, Integer>> oob_list = initializeMapForOOB(
				oob_condition_list, oob_spm_list);

		oob_list = getActualOOBs(oob_list);

		computeStatusForOOB(request, oob_list, spm, oob_condition_list,
				oob_spm_list);
	}

	Vector<String> initializeOOB2() {
		Vector<String> oob = new Vector();

		ResultSet rs = this.da
				.getResultSet("select * from datawarehouse.dbo.ac_dimension_ops_summary_oob_status");
		try {
			while (rs.next()) {
				oob.add(rs.getString(1));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return oob;
	}

	Vector<String> initializeSPM2() {
		Vector<String> oob = new Vector();

		ResultSet rs = this.da
				.getResultSet("select distinct ac_os_spm from datawarehouse.dbo.ac_fact2_ops_summary_oob where ac_os_bdm = '%' and ac_os_spm != '%' " +
						"and ac_os_spm not in ('Shroder, Charles','Supinger, Bob', 'Epling, Dave','Courtney, Terezia')");
		try {
			while (rs.next()) {
				oob.add(rs.getString(1));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return oob;
	}

	Map<String, Map<String, Integer>> initializeMapForOOB(
			Vector<String> condition_list, Vector<String> spm_list) {
		Map<String, Map<String, Integer>> oob = new HashMap();

		Integer initial_condition = Integer.valueOf(0);
		for (int i = 0; i < condition_list.size(); i++) {
			Map<String, Integer> counts = new HashMap();
			for (int j = 0; j < spm_list.size(); j++) {
				counts.put((String) spm_list.get(j), initial_condition);
			}
			oob.put((String) condition_list.get(i), counts);
		}
		return oob;
	}

	Map<String, Map<String, Integer>> getActualOOBs(
			Map<String, Map<String, Integer>> oob) {
		ResultSet rs = this.da
				.getResultSet("select ac_ob_oob_condition, ac_os_spm, ac_ob_count from datawarehouse.dbo.ac_fact2_ops_summary_oob where ac_os_bdm = '%' " +
						"and ac_os_spm != '%' and ac_os_spm not in ('Shroder, Charles','Supinger, Bob', 'Epling, Dave', 'Courtney, Terezia')");
		try {
			while (rs.next()) {
				Map<String, Integer> temp_count = (Map) oob.get(rs
						.getString("ac_ob_oob_condition"));
				temp_count.put(rs.getString("ac_os_spm"),
						Integer.valueOf(rs.getInt("ac_ob_count")));
				oob.put(rs.getString("ac_ob_oob_condition"), temp_count);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return oob;
	}

	void computeStatusForOOB(HttpServletRequest request,
			Map<String, Map<String, Integer>> oob_list, String spm,
			Vector<String> condition_list, Vector<String> spm_list) {
		String condition = "";
		String target_spm = "";

		Map<String, String> Benchmark = new HashMap();
		Map<String, String> Benchmark2 = new HashMap();
		Map<String, String> Hi = new HashMap();
		Map<String, String> Low = new HashMap();
		Map<String, String> Selected = new HashMap();
		Map<String, String> Total = new HashMap();

		Integer value = Integer.valueOf(0);
		Integer sum1 = Integer.valueOf(0);
		Integer hi = Integer.valueOf(-1);
		Integer low = Integer.valueOf(999999999);
		Integer benchmark = Integer.valueOf(0);
		for (int i = 0; i < condition_list.size(); i++) {
			condition = (String) condition_list.get(i);

			value = Integer.valueOf(0);
			sum1 = Integer.valueOf(0);
			hi = Integer.valueOf(-1);
			low = Integer.valueOf(999999999);
			benchmark = Integer.valueOf(0);
			for (int j = 0; j < spm_list.size(); j++) {
				target_spm = (String) spm_list.get(j);

				value = (Integer) ((Map) oob_list.get(condition))
						.get(target_spm);

				sum1 = Integer.valueOf(sum1.intValue() + value.intValue());
				if (!target_spm.equals(spm)) {
					benchmark = Integer.valueOf(benchmark.intValue()
							+ value.intValue());
				}
				if (value.intValue() > hi.intValue()) {
					hi = value;
				}
				if (value.intValue() < low.intValue()) {
					low = value;
				}
				if (target_spm.equals(spm)) {
					Selected.put(condition, String.valueOf(value));
				} 
			}
			double d1;
			if (spm.equals("%")) {
				d1 = 1.0D * sum1.intValue() / spm_list.size();
				benchmark = sum1;
				Selected.put(condition, "0");
			} else {
				d1 = 1.0D * benchmark.intValue() / (spm_list.size() - 1);
			}
			Benchmark.put(condition, this.decimal_format.format(d1));
			Benchmark2.put(condition, String.valueOf(benchmark));
			Hi.put(condition, String.valueOf(hi));
			Low.put(condition, String.valueOf(low));
			Total.put(condition, String.valueOf(sum1));
		}
		double d1 = Integer
				.parseInt((String) Benchmark2.get("To Be Scheduled"))
				+ Integer.parseInt((String) Benchmark2
						.get("To Be Scheduled Over-1"));
		d1 = spm.equals("%") ? d1 / spm_list.size() : d1
				/ (spm_list.size() - 1);
		Benchmark.put("To Be Scheduled Total", this.decimal_format.format(d1));

		d1 = Integer.parseInt((String) Benchmark2.get("Scheduled"))
				+ Integer.parseInt((String) Benchmark2.get("Scheduled Over-1"));
		d1 = spm.equals("%") ? d1 / spm_list.size() : d1
				/ (spm_list.size() - 1);
		Benchmark.put("Scheduled Total", this.decimal_format.format(d1));

		d1 = Integer.parseInt((String) Benchmark2.get("Confirmed"))
				+ Integer.parseInt((String) Benchmark2.get("Onsite"))
				+ Integer.parseInt((String) Benchmark2.get("Onsite Over-1"))
				+ Integer.parseInt((String) Benchmark2.get("Offsite"))
				+ Integer.parseInt((String) Benchmark2.get("Offsite Over-2"))
				+ Integer.parseInt((String) Benchmark2
						.get("Offsite Over/Del-7"));
		d1 = spm.equals("%") ? d1 / spm_list.size() : d1
				/ (spm_list.size() - 1);
		Benchmark.put("In Work", this.decimal_format.format(d1));

		d1 = Integer.parseInt((String) Benchmark2.get("Complete"))
				+ Integer.parseInt((String) Benchmark2.get("Complete Over-5"));
		d1 = spm.equals("%") ? d1 / spm_list.size() : d1
				/ (spm_list.size() - 1);
		Benchmark.put("Complete Total", this.decimal_format.format(d1));

		int s1 = Integer.parseInt((String) Total.get("To Be Scheduled"))
				+ Integer
						.parseInt((String) Total.get("To Be Scheduled Over-1"));
		Total.put("To Be Scheduled Total", this.currency.format(s1));

		s1 = Integer.parseInt((String) Total.get("Scheduled"))
				+ Integer.parseInt((String) Total.get("Scheduled Over-1"));
		Total.put("Scheduled Total", this.currency.format(s1));

		s1 = Integer.parseInt((String) Total.get("Confirmed"))
				+ Integer.parseInt((String) Total.get("Onsite"))
				+ Integer.parseInt((String) Total.get("Onsite Over-1"))
				+ Integer.parseInt((String) Total.get("Offsite"))
				+ Integer.parseInt((String) Total.get("Offsite Over-2"))
				+ Integer.parseInt((String) Total.get("Offsite Over/Del-7"));
		Total.put("In Work", this.currency.format(s1));

		s1 = Integer.parseInt((String) Total.get("Complete"))
				+ Integer.parseInt((String) Total.get("Complete Over-5"));
		Total.put("Complete Total", this.currency.format(s1));
		if (spm.equals("%")) {
			Selected.put("To Be Scheduled Total", "0");
			Selected.put("Scheduled Total", "0");
			Selected.put("In Work", "0");
			Selected.put("Complete Total", "0");
		} else {
			s1 = Integer
					.parseInt((String) (Selected.get("To Be Scheduled") == null ? "0"
							: Selected.get("To Be Scheduled")))
					+ Integer.parseInt((String) (Selected
							.get("To Be Scheduled Over-1") == null ? "0"
							: Selected.get("To Be Scheduled Over-1")));
			Selected.put("To Be Scheduled Total", this.currency.format(s1));

			s1 = Integer
					.parseInt((String) (Selected.get("Scheduled") == null ? "0"
							: Selected.get("Scheduled")))
					+ Integer.parseInt((String) (Selected
							.get("Scheduled Over-1") == null ? "0" : Selected
							.get("Scheduled Over-1")));
			Selected.put("Scheduled Total", this.currency.format(s1));

			s1 = Integer
					.parseInt((String) (Selected.get("Confirmed") == null ? "0"
							: Selected.get("Confirmed")))
					+ Integer
							.parseInt((String) (Selected.get("Onsite") == null ? "0"
									: Selected.get("Onsite")))
					+ Integer
							.parseInt((String) (Selected.get("Onsite Over-1") == null ? "0"
									: Selected.get("Onsite Over-1")))
					+ Integer
							.parseInt((String) (Selected.get("Offsite") == null ? "0"
									: Selected.get("Offsite")))
					+ Integer
							.parseInt((String) (Selected.get("Offsite Over-2") == null ? "0"
									: Selected.get("Offsite Over-2")))
					+ Integer.parseInt((String) (Selected
							.get("Offsite Over/Del-7") == null ? "0" : Selected
							.get("Offsite Over/Del-7")));
			Selected.put("In Work", this.currency.format(s1));

			s1 = Integer
					.parseInt((String) (Selected.get("Complete") == null ? "0"
							: Selected.get("Complete")))
					+ Integer.parseInt((String) (Selected
							.get("Complete Over-5") == null ? "0" : Selected
							.get("Complete Over-5")));
			Selected.put("Complete Total", this.currency.format(s1));
		}
		request.setAttribute("Selected", Selected);
		request.setAttribute("Benchmark", Benchmark);
		request.setAttribute("Hi", Hi);
		request.setAttribute("Low", Low);
		request.setAttribute("Total", Total);
	}

	public static String getDataFromWeb(String url, int timeout,
			String lineBreak) {
		try {
			URL u = new URL(url);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.setConnectTimeout(timeout);
			c.setReadTimeout(timeout);
			c.connect();
			int status = c.getResponseCode();
			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(
						c.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + lineBreak);
				}
				br.close();
				return sb.toString();
			}
		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
