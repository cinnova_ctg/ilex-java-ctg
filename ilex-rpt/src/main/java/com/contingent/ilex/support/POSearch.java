package com.contingent.ilex.support;

import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.contingent.ilex.report.DataAccess;

public class POSearch extends DispatchAction {
	String[] POs;
	DataAccess da = new DataAccess();
	Vector rows = new Vector();

	public ActionForward poSearch(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "searchResults";

		String po_list = request.getParameter("po_list");
		if (po_list != null) {
			this.rows.clear();
			if (unpackPOs(po_list)) {
				getPOs();
				if (this.rows.size() > 0) {
					request.setAttribute("rows", this.rows);
				}
			}
		}
		return mapping.findForward(forward_key);
	}

	boolean unpackPOs(String po_list) {
		boolean unpack_ok = true;

		this.POs = po_list.split(",");
		if (this.POs.length > 0) {
			for (int i = 0; i < this.POs.length; i++) {
				this.POs[i] = this.POs[i].trim();
				System.out.println(this.POs[i]);
			}
		} else {
			unpack_ok = false;
			System.out.println("No POs");
		}
		return unpack_ok;
	}

	void getPOs() {
		int count = 0;
		String sql = "";
		String sp = "exec ap_lm_purchase_order_search_01 ";
		try {
			for (int i = 0; i < this.POs.length; i++) {
				String[] row = new String[12];
				this.POs[i] = this.POs[i].trim();
				if (this.POs[i].length() > 0) {
					row[1] = this.POs[i];
					sql = sp + this.POs[i];
					ResultSet rs = this.da.getResultSet(sql);
					if (rs.next()) {
						row[0] = "G";
						row[2] = rs.getString("lo_ot_name");
						row[3] = rs.getString("lx_pr_title");
						row[4] = rs.getString("lm_js_title");
						row[5] = rs.getString("project_manager");
						row[6] = rs.getString("owner");
						row[7] = rs.getString("lo_om_division");
						row[8] = formatCurrencyValues(Double.valueOf(rs
								.getDouble("lm_po_authorised_total")));
						row[9] = formatCurrencyValues(Double.valueOf(rs
								.getDouble("lm_recon_approved_total")));
						row[10] = rs.getString("lm_js_id");
						row[11] = rs.getString("lx_pr_id");

						this.rows.add(count, row);
						count++;
					} else {
						row[0] = "B";
						row[1] = "PO number not found.";
					}
					System.out.println(this.POs[i]);
					rs.close();
				} else {
					row[0] = "B";
					row[1] = "Bad PO Number.";
				}
			}
		} catch (Exception e) {
			System.out.println(e + " - with - " + sql);
		}
	}

	String formatCurrencyValues(Double value) {
		NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
		String formattedValue = nf.format(value);

		return formattedValue;
	}
}
