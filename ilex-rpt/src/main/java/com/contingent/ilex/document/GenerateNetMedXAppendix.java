package com.contingent.ilex.document;

import com.contingent.ilex.report.DataAccess;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletResponse;

public class GenerateNetMedXAppendix {
	Font Standard = new Font(Font.FontFamily.HELVETICA, 8.0F, 0,
			BaseColor.BLACK);
	Font StandardBold = new Font(Font.FontFamily.HELVETICA, 8.0F, 1,
			BaseColor.BLACK);
	Font StandardUnderline = new Font(Font.FontFamily.HELVETICA, 8.0F, 4,
			BaseColor.BLACK);
	Font Title = new Font(Font.FontFamily.HELVETICA, 11.0F, 1, BaseColor.BLACK);
	Font TitleSuperScript = new Font(Font.FontFamily.HELVETICA, 5.0F, 0,
			BaseColor.BLACK);
	Font TableStandard = new Font(Font.FontFamily.HELVETICA, 7.0F, 0,
			BaseColor.BLACK);
	Font TableStandardBold = new Font(Font.FontFamily.HELVETICA, 7.0F, 1,
			BaseColor.BLACK);
	Font TableStandardHeaderBold = new Font(Font.FontFamily.HELVETICA, 8.0F, 1,
			BaseColor.BLACK);
	static Font HeaderFooterStandard = new Font(Font.FontFamily.HELVETICA,
			6.0F, 0, BaseColor.BLACK);
	Font SuperScript = new Font(Font.FontFamily.HELVETICA, 6.0F, 0,
			BaseColor.BLACK);
	Font SpecialCharacters = new Font(Font.FontFamily.ZAPFDINGBATS, 4.0F, 1,
			BaseColor.BLACK);
	static Paragraph TitleText = new Paragraph();
	static Image HeaderLogo;
	static Phrase FooterText1 = new Phrase();
	static Phrase FooterText2 = new Phrase();
	static Phrase FooterText3 = new Phrase();
	Float ParagraphLeading = Float.valueOf(9.0F);
	Float ParagraphSpacing = Float.valueOf(5.0F);
	PdfWriter writer;
	Document document;
	String NewPDF = "results/pdf02.pdf";
	public static final float[][] COLUMNS = { { 25.0F, 36.0F, 301.0F, 700.0F },
			{ 315.0F, 36.0F, 592.0F, 700.0F } };
	public static final float[][] COLUMNS2 = {
			{ 25.0F, 36.0F, 301.0F, 714.0F }, { 315.0F, 36.0F, 592.0F, 714.0F } };
	int InitialPage = 0;
	float FullTableWidth = 550.0F;
	float PartialTableWidth = 275.0F;
	DataAccess da;
	Vector<String> document_strucutre = new Vector();
	Vector<Object> document_contents = new Vector();
	Vector<int[]> structure = new Vector();
	Vector<int[]> structure_db = new Vector();
	ByteArrayOutputStream baos = new ByteArrayOutputStream();

	GenerateNetMedXAppendix(DataAccess da, HttpServletResponse response) {
		this.da = da;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		readAndStrucutreDocument();
		if (initializeDocument()) {
			this.document.open();

			buildContent3();

			this.document.close();
		}
	}

	void readAndStrucutreDocument() {
		String type = "";
		String page_flow = "1";
		String page_flow_previous = "1";

		int[] tracker = new int[3];

		tracker[0] = 1;
		tracker[1] = 0;

		String sql = "select * from dbo.cx_netmedx_proposal_structure";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				type = rs.getString("cx_st_type");
				page_flow = rs.getString("cx_st_page_flow");
				if ((!page_flow.equals(page_flow_previous))
						|| (type.equals("BR"))) {
					tracker[2] = (this.document_strucutre.size() - 1);
					this.structure.add(tracker);
					tracker = new int[3];
					tracker[0] = Integer.parseInt(page_flow);
					tracker[1] = this.document_strucutre.size();
					page_flow_previous = page_flow;
				}
				if (type.equals("SN")) {
					this.document_strucutre.add("SN");
					this.document_contents.add(new SectionNote(rs
							.getString("cx_st_number"), rs
							.getString("cx_st_content")));
				} else if (type.equals("ST")) {
					this.document_strucutre.add("ST");
					this.document_contents.add(new SectionHeader(rs
							.getString("cx_st_number"), rs
							.getString("cx_st_title"), rs
							.getString("cx_st_content")));
				} else if (type.equals("SH")) {
					this.document_strucutre.add("SH");
					this.document_contents.add(new SectionSubHeader(rs
							.getString("cx_st_number"), rs
							.getString("cx_st_title"), rs
							.getString("cx_st_content")));
				} else if (type.equals("SL")) {
					this.document_strucutre.add("SL");
					this.document_contents.add(new SectionListItem(rs
							.getString("cx_st_content")));
				} else if (type.equals("SP")) {
					this.document_strucutre.add("SP");
					this.document_contents.add(new SectionParagraph(rs
							.getString("cx_st_content")));
				} else if (type.equals("PH")) {
					this.document_strucutre.add("PH");
					this.document_contents.add(new ParagraphHeader(rs
							.getString("cx_st_number")));
				} else {
					this.document_strucutre.add(type);
					this.document_contents.add(rs.getString("cx_st_title"));
				}
			}
		} catch (Exception localException) {
		}
	}

	boolean initializeDocument() {
		this.document = new Document(PageSize.LETTER, 25.0F, 36.0F, 76.0F,
				36.0F);

		boolean document_status = true;
		try {
			this.writer = PdfWriter.getInstance(this.document, this.baos);

			GenerateNetMedXAppendix$HeaderFooter event = new GenerateNetMedXAppendix$HeaderFooter();
			this.writer.setPageEvent(event);
		} catch (Exception e) {
			document_status = false;
		}
		return document_status;
	}

	boolean buildContent3() {
		boolean content_status = true;

		buildHeader();

		buildTitleNetMedX("INSERT CLIENT NAME");

		buildColumns2();

		return content_status;
	}

	void buildHeader() {
		try {
			HeaderLogo = Image.getInstance("results/cns6.jpg");
			HeaderLogo.scaleAbsolute(90.0F, 51.0F);
			HeaderLogo.setAbsolutePosition(264.0F, 732.0F);

			FooterText1.add(new Chunk("Confidential & Proprietary",
					HeaderFooterStandard));
			new Chunk();
			FooterText1.add(Chunk.NEWLINE);
			FooterText1.add(new Chunk("Copyright 2009", HeaderFooterStandard));
			FooterText1.setLeading(0.0F);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void buildTitleNetMedX(String customer_name) {
		Phrase title_line = new Phrase();
		new Chunk();
		title_line.add(Chunk.NEWLINE);
		title_line.add(new Chunk("    ", this.Title));
		title_line.add(new Chunk(customer_name, this.Title));
		title_line.add(new Chunk(
				" - NetMedX APPENDIX TO MASTER SERVICES AGREEMENT (NetMedX",
				this.Title));
		title_line
				.add(new Chunk("TM", this.TitleSuperScript).setTextRise(6.0F));
		title_line.add(new Chunk(")", this.Title));
		title_line.setLeading(6.0F);

		TitleText.add(title_line);
		try {
			this.document.add(title_line);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	void buildColumns2() {
		for (int i = 0; i < this.structure.size(); i++) {
			int[] tracker = (int[]) this.structure.get(i);
			if (tracker[0] == 1) {
				buildColumnFormatSection(tracker[1], tracker[2]);
			} else if (tracker[0] == 2) {
				buildPageFormatSection(tracker[1], tracker[2]);
			}
		}
	}

	void buildColumnFormatSection(int start_pt, int end_pt) {
		String tb_name = "";

		ColumnText ct = new ColumnText(this.writer.getDirectContent());
		if (start_pt == 0) {
			ct.addElement(addParagraph());
		} else {
			this.document.newPage();
		}
		for (int i = start_pt; i < end_pt + 1; i++) {
			String key = (String) this.document_strucutre.get(i);
			if (key.equals("ST")) {
				SectionHeader st = (SectionHeader) this.document_contents
						.get(i);
				ct.addElement(addParagraphSectionHeader(st.Number, st.Title,
						st.Content));
			} else if (key.equals("SH")) {
				SectionSubHeader sh = (SectionSubHeader) this.document_contents
						.get(i);
				ct.addElement(addParagraphSectionSubHeader(sh.Number, sh.Title,
						sh.Content));
			} else if (key.equals("SP")) {
				SectionParagraph sp = (SectionParagraph) this.document_contents
						.get(i);
				ct.addElement(addParagraphSectionParagraph(sp.Content));
			} else if (key.equals("SN")) {
				SectionNote sn = (SectionNote) this.document_contents.get(i);
				ct.addElement(addParagraphSectionNote(sn.Note, sn.Content));
			} else if (key.equals("SL")) {
				SectionListItem sl = (SectionListItem) this.document_contents
						.get(i);
				ct.addElement(addParagraphSectionListItem(sl.Content));
			} else if (key.equals("TB")) {
				tb_name = (String) this.document_contents.get(i);
				if (tb_name.equals("Criticality")) {
					ct.addElement(buildTableCriticality());
				} else if (tb_name.equals("Response Time Goals")) {
					ct.addElement(buildTableResponseTime());
				} else if (tb_name.equals("Additional Response Time")) {
					ct.addElement(buildTableDistance());
				} else if (tb_name.equals("CallManagement")) {
					ct.addElement(buildTableCallManagement());
				} else if (!tb_name.equals("")) {
					tb_name.equals("");
				}
			} else if (key.equals("DC")) {
				ct.addElement(addDiscountLine());
			} else if (key.equals("CB1")) {
				ct.addElement(addClientInitialBlock1());
			} else if (key.equals("CB2")) {
				ct.addElement(addClientInitialBlock2());
			} else if (key.equals("PH")) {
				ParagraphHeader ph = (ParagraphHeader) this.document_contents
						.get(i);
				ct.addElement(addParagraphHeader(ph.Header));
			} else if (key.equals("BR")) {
				this.document.newPage();
			}
		}
		try {
			int first_page = 0;
			int column = 0;
			int status = 0;
			while (ColumnText.hasMoreText(status)) {
				if (this.InitialPage == 0) {
					ct.setSimpleColumn(COLUMNS[column][0], COLUMNS[column][1],
							COLUMNS[column][2], COLUMNS[column][3]);
					ct.setYLine(COLUMNS[column][3]);
				} else {
					ct.setSimpleColumn(COLUMNS2[column][0],
							COLUMNS2[column][1], COLUMNS2[column][2],
							COLUMNS2[column][3]);
					ct.setYLine(COLUMNS2[column][3]);
				}
				status = ct.go();
				column = Math.abs(column - 1);
				first_page++;
				if (column == 0) {
					this.document.newPage();
					this.InitialPage += 1;
				}
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	void buildPageFormatSection(int start_pt, int end_pt) {
		String tb_name = "";
		try {
			if (start_pt > 0) {
				this.document.newPage();
			}
			for (int i = start_pt; i < end_pt + 1; i++) {
				String key = (String) this.document_strucutre.get(i);
				if (key.equals("ST")) {
					SectionHeader st = (SectionHeader) this.document_contents
							.get(i);
					this.document.add(addParagraphSectionHeader(st.Number,
							st.Title, st.Content));
				} else if (key.equals("SH")) {
					SectionSubHeader sh = (SectionSubHeader) this.document_contents
							.get(i);
					this.document.add(addParagraphSectionSubHeader(sh.Number,
							sh.Title, sh.Content));
				} else if (key.equals("SP")) {
					SectionParagraph sp = (SectionParagraph) this.document_contents
							.get(i);
					this.document.add(addParagraphSectionParagraph(sp.Content));
				} else if (key.equals("SN")) {
					SectionNote sn = (SectionNote) this.document_contents
							.get(i);
					this.document.add(addParagraphSectionNote(sn.Note,
							sn.Content));
				} else if (key.equals("SL")) {
					SectionListItem sl = (SectionListItem) this.document_contents
							.get(i);
					this.document.add(addParagraphSectionListItem(sl.Content));
				} else if (key.equals("TB")) {
					tb_name = (String) this.document_contents.get(i);
					if (tb_name
							.equals("Field Services – NetMedX Standard (Time & Material)")) {
						this.document.add(buildTableFieldServices1());
					} else if (tb_name
							.equals("Logistics Services – NetMedX Standard")) {
						this.document.add(buildTableLogisticsServices1());
					} else if (tb_name
							.equals("Field Services – NetMedX Silver")) {
						this.document.add(buildTableNetMedXSilver());
					} else if (tb_name
							.equals("Field Services – NetMedX Copper (Flat Fee Dispatch)")) {
						this.document.add(buildTableNetMedXCopper());
					} else if (tb_name.equals("Call Management - Monthly")) {
						this.document.add(buildTableCallManagementMonthly());
					} else if (tb_name.equals("Network Monitoring")) {
						this.document.add(buildTableNetworkMonitoring());
					} else {
						tb_name.equals("");
					}
				} else if (key.equals("DC")) {
					this.document.add(addDiscountLine());
				} else if (key.equals("CB1")) {
					this.document.add(addClientInitialBlock1());
				} else if (key.equals("CB2")) {
					this.document.add(addClientInitialBlock2());
				} else if (key.equals("PH")) {
					ParagraphHeader ph = (ParagraphHeader) this.document_contents
							.get(i);
					this.document.add(addParagraphHeader(ph.Header));
				} else if (key.equals("BR")) {
					this.document.newPage();
				}
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	void buildParagraph(String p) {
		Paragraph paragraph = new Paragraph();
		paragraph.setLeading(this.ParagraphLeading.floatValue());
		paragraph.setSpacingBefore(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setSpacingAfter(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.add(p);
		try {
			this.document.add(paragraph);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	Paragraph addParagraph() {
		String para = "This Appendix provides the tasks, timeframes and associated pricing for performance of NetmedX services for the Client throughout the Client's network(s). NetmedX services shall begin upon execution of the Agreement in order to facilitate a direct relationship between CNS and Client for operations support of the Client's installed network(s). CNS shall perform select NetmedX services that may include one or more of the following: 1.0 Field Service; 2.0 Call/Problem Management; and 3.0 Network Management/Monitoring for the Client. Support will be provided for the network infrastructure including elements listed below in the scope of work. Parts will be provided locally or shipped upon request. Response and repair times will be \"best effort\".";
		Paragraph paragraph = new Paragraph(new Chunk(para, this.Standard));
		paragraph.setLeading(this.ParagraphLeading.floatValue());
		paragraph.setSpacingBefore(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setSpacingAfter(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setAlignment(3);
		return paragraph;
	}

	Paragraph addParagraphSectionHeader(String level, String header,
			String contnet) {
		Paragraph paragraph = new Paragraph();
		paragraph.add(new Chunk(level, this.StandardBold));
		paragraph.add(new Chunk("    ", this.Standard));
		paragraph.add(new Chunk(header, this.StandardBold));
		new Chunk();
		paragraph.add(Chunk.NEWLINE);
		paragraph.add(new Chunk(contnet, this.Standard));
		paragraph.setLeading(this.ParagraphLeading.floatValue());
		paragraph.setSpacingBefore(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setSpacingAfter(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setAlignment(3);
		return paragraph;
	}

	Paragraph addParagraphSectionSubHeader(String level, String header,
			String contnet) {
		Paragraph paragraph = new Paragraph();

		paragraph.add(new Chunk(level, this.Standard));
		paragraph.add(new Chunk("    ", this.Standard));
		paragraph.add(new Chunk(header, this.Standard)
				.setUnderline(0.5F, -1.3F));

		new Chunk();
		paragraph.add(Chunk.NEWLINE);
		paragraph.add(new Chunk(contnet, this.Standard));
		paragraph.setLeading(this.ParagraphLeading.floatValue());
		paragraph.setSpacingBefore(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setSpacingAfter(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setAlignment(3);

		return paragraph;
	}

	Paragraph addParagraphHeader(String header) {
		Paragraph paragraph = new Paragraph();

		paragraph.add(new Chunk(header, this.Standard)
				.setUnderline(0.5F, -1.3F));

		paragraph.setLeading(this.ParagraphLeading.floatValue());
		paragraph.setSpacingBefore(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setSpacingAfter(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setAlignment(3);

		return paragraph;
	}

	Paragraph addClientInitialBlock1() {
		Paragraph paragraph = new Paragraph();
		paragraph.add(new Chunk("Client Initials  _____", this.Standard));
		new Chunk();
		paragraph.add(Chunk.NEWLINE);
		paragraph.add(new Chunk("Date ___________  ", this.Standard));
		paragraph.setLeading(15.0F);
		paragraph.setSpacingBefore(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setSpacingAfter(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setAlignment(2);

		return paragraph;
	}

	Paragraph addClientInitialBlock2() {
		Paragraph paragraph = new Paragraph();
		paragraph.add(new Chunk("Extended Discount ___%", this.Standard));
		new Chunk();
		paragraph.add(Chunk.NEWLINE);
		new Chunk();
		paragraph.add(Chunk.NEWLINE);
		paragraph.add(new Chunk("Client Initials  _____", this.Standard));
		new Chunk();
		paragraph.add(Chunk.NEWLINE);
		paragraph.add(new Chunk("Date ___________  ", this.Standard));
		paragraph.setLeading(15.0F);
		paragraph.setSpacingBefore(this.ParagraphSpacing.floatValue() / 1.0F);
		paragraph.setSpacingAfter(this.ParagraphSpacing.floatValue() / 1.0F);
		paragraph.setAlignment(2);

		return paragraph;
	}

	Paragraph addDiscountLine() {
		Paragraph paragraph = new Paragraph();
		paragraph.add(new Chunk("NetMedX Standard Extended Discount ___%",
				this.Standard));
		paragraph.setLeading(this.ParagraphLeading.floatValue());
		paragraph.setSpacingBefore(this.ParagraphSpacing.floatValue() / 1.0F);
		paragraph.setSpacingAfter(this.ParagraphSpacing.floatValue() / 1.0F);
		paragraph.setAlignment(2);

		return paragraph;
	}

	Paragraph addParagraphSectionListItem(String contnet) {
		Paragraph paragraph = new Paragraph();
		paragraph.add(new Chunk('n', this.SpecialCharacters).setTextRise(2.0F));
		paragraph.add(new Chunk("    ", this.Standard));
		paragraph.add(new Chunk(contnet, this.Standard));
		paragraph.setLeading(this.ParagraphLeading.floatValue());
		paragraph.setSpacingBefore(this.ParagraphSpacing.floatValue() / 5.0F);
		paragraph.setSpacingAfter(this.ParagraphSpacing.floatValue() / 5.0F);
		paragraph.setAlignment(3);
		return paragraph;
	}

	Paragraph addParagraphSectionParagraph(String contnet) {
		Paragraph paragraph = new Paragraph();
		paragraph.add(new Chunk(contnet, this.Standard));
		paragraph.setLeading(this.ParagraphLeading.floatValue());
		paragraph.setSpacingBefore(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setSpacingAfter(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setAlignment(3);
		return paragraph;
	}

	Paragraph addParagraphSectionNote(String note, String content) {
		Paragraph paragraph = new Paragraph();
		paragraph.add(new Chunk(note, this.SuperScript).setTextRise(2.0F));
		paragraph.add(new Chunk(content, this.TableStandard));
		paragraph.setLeading(this.ParagraphLeading.floatValue());
		paragraph.setSpacingBefore(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setSpacingAfter(this.ParagraphSpacing.floatValue() / 2.0F);
		paragraph.setAlignment(3);

		return paragraph;
	}

	PdfPTable buildTableDistance() {
		PdfPTable table = new PdfPTable(2);

		table.addCell(addTableCellBold("Distance between the Field Technician and the Site"));
		table.addCell(addTableCellBold("Additional Response Time Increment"));
		table.addCell(addTableCellNormal("0 – 50 miles"));
		table.addCell(addTableCellNormal("0"));
		table.addCell(addTableCellNormal("51 – 100 miles"));
		table.addCell(addTableCellNormal("+ 1 hour"));
		table.addCell(addTableCellNormal("101 – 150 miles"));
		table.addCell(addTableCellNormal("+ 2 hours"));
		table.addCell(addTableCellNormal("151 – 200 miles"));
		table.addCell(addTableCellNormal("+ 3 hours"));
		table.addCell(addTableCellNormal("> 200 miles"));
		table.addCell(addTableCellNormal("+ 4 hours"));
		try {
			table.setTotalWidth(this.PartialTableWidth);
			table.setLockedWidth(true);
			table.setWidths(new float[] { 140.0F, 140.0F });
			table.setSpacingBefore(this.ParagraphSpacing.floatValue());
			table.setSpacingAfter(this.ParagraphSpacing.floatValue());
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return table;
	}

	PdfPTable buildTableCallManagement() {
		PdfPTable table = new PdfPTable(3);

		table.addCell(addTableCellBold("CNS Resource"));

		Phrase phrase = new Phrase();
		phrase.add(new Chunk("Price Per Minute", this.TableStandardBold));
		new Chunk();
		phrase.add(Chunk.NEWLINE);
		phrase.add(new Chunk("PPS", this.TableStandard));
		table.addCell(new PdfPCell(phrase));

		phrase = new Phrase();
		phrase.add(new Chunk("Price Per Minute", this.TableStandardBold));
		new Chunk();
		phrase.add(Chunk.NEWLINE);
		phrase.add(new Chunk("Outside PPS", this.TableStandard));
		table.addCell(new PdfPCell(new PdfPCell(phrase)));

		table.addCell(addTableCellNormal("CCA"));
		table.addCell(addTableCellBold("$1.50/minute"));
		table.addCell(addTableCellBold("$2.48/minute"));

		table.addCell(addTableCellNormal("TSS"));
		table.addCell(addTableCellBold("$1.50/minute"));
		table.addCell(addTableCellBold("$2.48/minute"));
		try {
			table.setTotalWidth(this.PartialTableWidth);
			table.setLockedWidth(true);
			table.setWidths(new float[] { 90.0F, 90.0F, 90.0F });
			table.setSpacingBefore(this.ParagraphSpacing.floatValue());
			table.setSpacingAfter(this.ParagraphSpacing.floatValue());
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return table;
	}

	PdfPTable buildTableCriticality() {
		PdfPTable table = new PdfPTable(2);

		table.addCell(addTableCellBold("Criticality"));
		table.addCell(addTableCellBold("Condition"));

		table.addCell(addTableCellNormal("1 – Emergency"));
		table.addCell(addTableCellNormal("A problem that is determined by Client to require immediate response."));
		table.addCell(addTableCellNormal("2 – Major"));
		table.addCell(addTableCellNormal("A problem that prevents the End-Users ability to access the network.  The network, in whole or in part, is \"down\" and is considered inoperable.  Client or Client's Customers are unable to communicate on the network by either primary or backup means."));
		table.addCell(addTableCellNormal("3 – Minor"));
		table.addCell(addTableCellNormal("A minor problem that slightly impacts the End-Users ability to access the network.  The problem may be intermittent in nature.  End-Users have not been hampered by an apparent failure, but rather an inconvenience without a need for fast resolution."));
		table.addCell(addTableCellNormal("4 - Routine"));
		table.addCell(addTableCellNormal("A minor problem that does not impact the End-Users ability to access the network.  The problem may be intermittent in nature. This criticality would also apply to questions and/or manners of general consultation."));
		table.addCell(addTableCellNormal("5 – Scheduled T&M"));
		table.addCell(addTableCellNormal("A minor problem or scheduled maintenance that does not impact the End-Users ability to access the network.  The problem may be intermittent in nature. This criticality would also apply to questions and/or manners of general consultation."));
		try {
			table.setTotalWidth(this.PartialTableWidth);
			table.setLockedWidth(true);
			table.setWidths(new float[] { 80.0F, 190.0F });
			table.setSpacingBefore(this.ParagraphSpacing.floatValue());
			table.setSpacingAfter(this.ParagraphSpacing.floatValue());
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return table;
	}

	PdfPTable buildTableResponseTime() {
		PdfPTable table = new PdfPTable(5);
		Phrase phrase = new Phrase();
		PdfPCell cell = new PdfPCell();

		table.addCell(new Phrase(""));

		phrase.add(new Chunk("Non-PPS (after hours) Arrival -",
				this.TableStandardBold));
		new Chunk();
		phrase.add(Chunk.NEWLINE);
		phrase.add(new Chunk("On Site Response Goal", this.TableStandardBold));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setHorizontalAlignment(1);
		cell.setVerticalAlignment(5);
		cell.setColspan(2);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("PPS Arrival -", this.TableStandardBold));
		new Chunk();
		phrase.add(Chunk.NEWLINE);
		phrase.add(new Chunk("On Site Response Goal", this.TableStandardBold));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setHorizontalAlignment(1);
		cell.setVerticalAlignment(5);
		cell.setColspan(2);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Activity", this.TableStandardBold));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Call Received", this.TableStandardBold));
		new Chunk();
		phrase.add(Chunk.NEWLINE);
		phrase.add(new Chunk("8:00A.M.- 3:00P.M. M-F", this.TableStandard));
		new Chunk();
		phrase.add(Chunk.NEWLINE);
		phrase.add(new Chunk("Site Time", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Call Received", this.TableStandardBold));
		new Chunk();
		phrase.add(Chunk.NEWLINE);
		phrase.add(new Chunk("3:00 P.M.-8:00A.M.", this.TableStandard));
		new Chunk();
		phrase.add(Chunk.NEWLINE);
		phrase.add(new Chunk("Site Time", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Call Received", this.TableStandardBold));
		new Chunk();
		phrase.add(Chunk.NEWLINE);
		phrase.add(new Chunk("8:00A.M.- 3:00P.M. M-F", this.TableStandard));
		new Chunk();
		phrase.add(Chunk.NEWLINE);
		phrase.add(new Chunk("Site Time", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Call Received", this.TableStandardBold));
		new Chunk();
		phrase.add(Chunk.NEWLINE);
		phrase.add(new Chunk("3:00P.M- 8:00A.M.", this.TableStandard));
		new Chunk();
		phrase.add(Chunk.NEWLINE);
		phrase.add(new Chunk("Site Time", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Criticality 1 Emergency", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Does Not Apply", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("2 Hours", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("2 Business Hours", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("2 Hours after start of next calendar day",
				this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Criticality 2 Major", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("4 Hours", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("4 Hours", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("4 Business Hours", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("4 Hours after start of next calendar day",
				this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Criticality 3 Minor", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("8 Hours", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("8 Hours", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("8 Business Hours", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("8 Hours after start of next calendar day",
				this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Criticality 4 Routine", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Next Day", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Next Day", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Next Business Day", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("2", this.TableStandard));
		phrase.add(new Chunk("nd ", this.SuperScript).setTextRise(1.0F));
		phrase.add(new Chunk("Business Day", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Criticality 5 Scheduled", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Does Not Apply", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("Does Not Apply", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("2", this.TableStandard));
		phrase.add(new Chunk("nd ", this.SuperScript).setTextRise(1.0F));
		phrase.add(new Chunk("Business Day", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk("2", this.TableStandard));
		phrase.add(new Chunk("rd ", this.SuperScript).setTextRise(1.0F));
		phrase.add(new Chunk("Business Day", this.TableStandard));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);
		try {
			table.setTotalWidth(this.PartialTableWidth);
			table.setLockedWidth(true);
			table.setWidths(new float[] { 54.0F, 54.0F, 54.0F, 54.0F, 54.0F });
			table.setSpacingBefore(this.ParagraphSpacing.floatValue());
			table.setSpacingAfter(this.ParagraphSpacing.floatValue());
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return table;
	}

	Phrase addTableCellNormal(String cell) {
		Phrase phrase = new Phrase();
		phrase.add(new Chunk(cell, this.Standard));
		phrase.setLeading(this.ParagraphLeading.floatValue());

		return phrase;
	}

	Phrase addTableCellBold(String cell) {
		Phrase phrase = new Phrase();
		phrase.add(new Chunk(cell, this.StandardBold));
		phrase.setLeading(this.ParagraphLeading.floatValue());

		return phrase;
	}

	PdfPTable buildTableFieldServices1() {
		String[][] nrs = getNetMedXRates("");

		PdfPTable table = new PdfPTable(6);

		table.setWidthPercentage(100.0F);
		table.getDefaultCell().setFixedHeight(70.0F);

		Phrase phrase = new Phrase(
				"Field Services – NetMedX Standard (Time & Material)",
				this.TableStandardHeaderBold);
		PdfPCell cell = new PdfPCell(phrase);
		cell.setColspan(6);
		cell.setHorizontalAlignment(1);
		cell.setGrayFill(0.9F);
		cell.setPadding(5.0F);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk(" ", this.TableStandardBold));
		cell.addElement(phrase);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk(" ", this.TableStandardBold));
		cell.addElement(phrase);
		table.addCell(cell);

		phrase = new Phrase("Hourly Labor Rates", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setColspan(4);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk(" ", this.TableStandardBold));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk(" ", this.TableStandardBold));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		phrase.add(new Chunk("Technician", this.TableStandardBold));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell = new PdfPCell(phrase);
		cell.setColspan(2);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Engineer", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setColspan(2);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Activity", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Description", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Level 2", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Level 3", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Level 2", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Level 3", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);
		for (int i = 0; i < 5; i++) {
			phrase = new Phrase();
			phrase.add(new Chunk(nrs[i][0], this.TableStandardBold));
			new Chunk();
			phrase.add(Chunk.NEWLINE);
			phrase.add(new Chunk(nrs[i][1], this.TableStandard));
			cell = new PdfPCell(phrase);
			cell.setPadding(5.0F);
			table.addCell(cell);

			phrase = new Phrase();
			phrase.add(new Chunk(nrs[i][2], this.TableStandardBold));
			new Chunk();
			phrase.add(Chunk.NEWLINE);
			phrase.add(new Chunk(nrs[i][3], this.TableStandard));
			cell = new PdfPCell(phrase);
			cell.setPadding(5.0F);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(nrs[i][4], this.TableStandard));
			cell.setPadding(5.0F);
			cell.setVerticalAlignment(5);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);

			phrase = new Phrase(nrs[i][5], this.TableStandard);
			cell = new PdfPCell(phrase);
			cell.setPadding(5.0F);
			cell.setVerticalAlignment(5);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);

			phrase = new Phrase(nrs[i][6], this.TableStandard);
			cell = new PdfPCell(phrase);
			cell.setPadding(5.0F);
			cell.setVerticalAlignment(5);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);

			phrase = new Phrase(nrs[i][7], this.TableStandard);
			cell = new PdfPCell(phrase);
			cell.setPadding(5.0F);
			cell.setVerticalAlignment(5);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);
		}
		try {
			table.setTotalWidth(this.FullTableWidth);
			table.setLockedWidth(true);
			table.setWidths(new float[] { 150.0F, 300.0F, 50.0F, 50.0F, 50.0F,
					50.0F });
			table.setSpacingBefore(this.ParagraphSpacing.floatValue());
			table.setSpacingAfter(0.0F);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return table;
	}

	String[][] getNetMedXRates(String lx_pr_id) {
		String[][] rates = new String[5][8];

		rates[0][0] = "Criticality 1 Dispatch - Emergency";
		rates[0][1] = "(2 Hours Onsite)";
		rates[0][2] = "5x8x2";
		rates[0][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[0][4] = "$204.00";
		rates[0][5] = "$209.00";
		rates[0][6] = "$215.00";
		rates[0][7] = "$245.00";

		rates[1][0] = "Criticality 2 Dispatch - Major";
		rates[1][1] = "(4 Hours Onsite)";
		rates[1][2] = "5x8x2";
		rates[1][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[1][4] = "$158.00";
		rates[1][5] = "$166.00";
		rates[1][6] = "$166.00";
		rates[1][7] = "$200.00";

		rates[2][0] = "Criticality 3 Dispatch - Minor";
		rates[2][1] = "(Same Day Onsite)";
		rates[2][2] = "5x8x8";
		rates[2][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[2][4] = "$110.00";
		rates[2][5] = "$122.00";
		rates[2][6] = "$124.00";
		rates[2][7] = "$179.00";

		rates[3][0] = "Criticality 4 Dispatch - Routine";
		rates[3][1] = "(Next Day Onsite)";
		rates[3][2] = "Next Business Day";
		rates[3][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[3][4] = "$79.00";
		rates[3][5] = "$90.00";
		rates[3][6] = "$93.00";
		rates[3][7] = "$145.00";

		rates[4][0] = "Criticality 5 Dispatch – Scheduled";
		rates[4][1] = "(2nd Day+ Onsite)";
		rates[4][2] = "2nd  Business Day";
		rates[4][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[4][4] = "$71.00";
		rates[4][5] = "$78.00";
		rates[4][6] = "$86.00";
		rates[4][7] = "$133.00";

		return rates;
	}

	PdfPTable buildTableLogisticsServices1() {
		String[][] rs = getLogisticsRates("");

		PdfPTable logistics = new PdfPTable(4);

		logistics.setWidthPercentage(100.0F);
		logistics.getDefaultCell().setFixedHeight(70.0F);

		PdfPCell cell = new PdfPCell(new Phrase(
				"Logistics Services - NetMedX Standard",
				this.TableStandardHeaderBold));
		cell.setColspan(6);
		cell.setHorizontalAlignment(1);
		cell.setGrayFill(0.9F);
		cell.setPadding(5.0F);
		logistics.addCell(cell);

		Phrase phrase = new Phrase("Activity", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		logistics.addCell(cell);

		phrase = new Phrase("Description", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		logistics.addCell(cell);

		phrase = new Phrase("Activity Rates", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		logistics.addCell(cell);

		phrase = new Phrase("Additional Information", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		logistics.addCell(cell);
		for (int i = 0; i < 12; i++) {
			phrase = new Phrase();
			phrase.add(new Chunk(rs[i][0], this.TableStandardBold));
			new Chunk();
			phrase.add(Chunk.NEWLINE);
			phrase.add(new Chunk(rs[i][1], this.TableStandard));
			cell = new PdfPCell(phrase);
			cell.setPadding(5.0F);
			logistics.addCell(cell);

			phrase = new Phrase();
			phrase.add(new Chunk(rs[i][2], this.TableStandardBold));
			new Chunk();
			phrase.add(Chunk.NEWLINE);
			phrase.add(new Chunk(rs[i][3], this.TableStandard));
			cell = new PdfPCell(phrase);
			cell.setPadding(5.0F);
			logistics.addCell(cell);

			phrase = new Phrase();
			phrase.add(new Chunk(rs[i][4], this.TableStandardBold));
			cell = new PdfPCell(phrase);
			cell.setPadding(5.0F);
			logistics.addCell(cell);

			String[] bullets = rs[i][5].split("~");

			phrase = new Phrase();
			for (int j = 0; j < bullets.length; j++) {
				phrase.add(new Chunk('n', this.SpecialCharacters)
						.setTextRise(1.0F));
				phrase.add(new Chunk(bullets[j], this.TableStandard));
				if (j < bullets.length - 1) {
					new Chunk();
					phrase.add(Chunk.NEWLINE);
				}
			}
			cell = new PdfPCell(phrase);
			cell.setPadding(5.0F);
			logistics.addCell(cell);
		}
		try {
			logistics.setTotalWidth(this.FullTableWidth);
			logistics.setLockedWidth(true);
			logistics.setWidths(new float[] { 150.0F, 225.0F, 75.0F, 200.0F });
			logistics.setSpacingBefore(0.0F);
			logistics.setSpacingAfter(this.ParagraphSpacing.floatValue());
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return logistics;
	}

	String[][] getLogisticsRates(String lx_pr_id) {
		String[][] rs = new String[12][6];

		rs[0][0] = "Parts Depot Storage";
		rs[0][1] = "Nationwide Coverage Available";
		rs[0][2] = "64 Cubic Feet";
		rs[0][3] = "(1 pallet)";
		rs[0][4] = "$60 per month";
		rs[0][5] = "  Monthly price is per location.~  Add 15% for climate control.~   Delivery not included.";

		rs[1][0] = "Parts Depot Storage";
		rs[1][1] = "Nationwide Coverage Available";
		rs[1][2] = "32 Cubic Feet";
		rs[1][3] = "(1/2 pallet)";
		rs[1][4] = "$45 per month";
		rs[1][5] = "  Monthly price is per location.~  Add 15% for climate control.~   Delivery not included.";

		rs[2][0] = "Spares Deployment Processing";
		rs[2][1] = "(Next Business Day)";
		rs[2][2] = "Spare Parts Deployment Processing";
		rs[2][3] = "";
		rs[2][4] = "$25 per incident";
		rs[2][5] = "  Materials Charged Separately~  Shipping not included";

		rs[3][0] = "RMA Processing";
		rs[3][1] = "(5 business days from receipt of faulty part)";
		rs[3][2] = "Warranty Parts Return Processing";
		rs[3][3] = "";
		rs[3][4] = "$25 per incident";
		rs[3][5] = "  Materials Charged Separately~  Shipping not included";

		rs[4][0] = "Bench Repair (Hardware/Software)";
		rs[4][1] = "(best effort only)";
		rs[4][2] = "Component Repair";
		rs[4][3] = "";
		rs[4][4] = "$85 per hour";
		rs[4][5] = "  Materials Charged Separately~   Repair services are best effort only~  Shipping not included";

		rs[5][0] = "Parts Courier Services Criticality 1 - Emergency";
		rs[5][1] = "(2 Hours Onsite)";
		rs[5][2] = "5x8x2";
		rs[5][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rs[5][4] = "$275 per delivery";
		rs[5][5] = "  Price includes parcels weighing less than (25) lbs~  Add 15% for delivery outside PPS";

		rs[6][0] = "Parts Courier Services Criticality 2 - Major";
		rs[6][1] = "(4 Hours Onsite)";
		rs[6][2] = "5x8x4";
		rs[6][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rs[6][4] = "$185 per delivery";
		rs[6][5] = "  Price includes parcels weighing less than (25) lbs~  Add 15% for delivery outside PPS";

		rs[7][0] = "Parts Courier Services Criticality 3 - Minor";
		rs[7][1] = "(8 Hours Onsite)";
		rs[7][2] = "5x8x8";
		rs[7][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rs[7][4] = "$150 per delivery";
		rs[7][5] = "  Price includes parcels weighing less than (25) lbs~  Add 15% for delivery outside PPS";

		rs[8][0] = "Parts Courier Services Criticality 4 - Routine";
		rs[8][1] = "(Next Business Day Onsite)";
		rs[8][2] = "Next Business Day";
		rs[8][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rs[8][4] = "$50 per delivery";
		rs[8][5] = "  Price includes parcels weighing less than (25) lbs~  Add 15% for delivery outside PPS";

		rs[9][0] = "Parts Courier Services Criticality 5 - Scheduled";
		rs[9][1] = "(2nd Business Day Onsite)";
		rs[9][2] = "2nd Business Day";
		rs[9][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rs[9][4] = "$25 per delivery";
		rs[9][5] = "  Price includes parcels weighing less than (25) lbs~  Add 15% for delivery outside PPS";

		rs[10][0] = "Asset Tracking ";
		rs[10][1] = "(7x24 Online Access)";
		rs[10][2] = "Customer Care Center Online";
		rs[10][3] = "www.contingent.net/ccc";
		rs[10][4] = "$199 per month ";
		rs[10][5] = "  Price includes up to 1000 units tracked, 3 user accounts~   Add $175/month for each 1000 unit block and $25/month for each additional user account";

		rs[11][0] = "Certified Equipment Disposal";
		rs[11][1] = "(48 Hours Notice to Pickup)";
		rs[11][2] = "2nd Business Day";
		rs[11][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rs[11][4] = "$0.83 per pound";
		rs[11][5] = "  Price includes freight, disposal, certificate by serial number reporting, EPA and DoD/DHS compliance when applicable, Federal and State Law compliance~   Minimum 50lbs per visit";

		return rs;
	}

	PdfPTable buildTableNetMedXSilver() {
		String[][] nrs = getNetMedXSilverRates("");
		float cellpadding = 3.0F;
		PdfPTable table = new PdfPTable(7);

		table.setWidthPercentage(100.0F);
		table.getDefaultCell().setFixedHeight(70.0F);

		Phrase phrase = new Phrase(
				"Field Services – NetMedX Silver (Prepaid Dispatch \"Blocks\")",
				this.TableStandardHeaderBold);
		PdfPCell cell = new PdfPCell(phrase);
		cell.setColspan(7);
		cell.setHorizontalAlignment(1);
		cell.setGrayFill(0.9F);
		cell.setPadding(cellpadding);
		table.addCell(cell);

		cell = new PdfPCell();
		cell.addElement(new Phrase(new Chunk(" ", this.TableStandardBold)));
		table.addCell(cell);

		phrase = new Phrase();
		phrase.add(new Chunk(
				"PLEASE NOTE: THESE ARE PREPAID DISPATCH RATES (PER VISIT)",
				this.TableStandard));
		cell = new PdfPCell(phrase);
		cell.setColspan(2);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase(new Chunk("Per Dispatch Visit Discounted Rates",
				this.TableStandardBold));
		phrase.add(new Chunk(" 1", this.TableStandard).setTextRise(2.0F));
		cell = new PdfPCell(phrase);
		cell.setColspan(4);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk(" ", this.TableStandardBold));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		cell.setColspan(2);
		phrase.add(new Chunk(" ", this.TableStandardBold));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		phrase.add(new Chunk("Technician", this.TableStandardBold));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell = new PdfPCell(phrase);
		cell.setColspan(2);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Engineer", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setColspan(2);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Activity", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Description", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Penalty for Late Arrival", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Level 2", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Level 3", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Level 2", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Level 3", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);
		for (int i = 0; i < 5; i++) {
			phrase = new Phrase();
			phrase.add(new Chunk(nrs[i][0], this.TableStandardBold));
			new Chunk();
			phrase.add(Chunk.NEWLINE);
			phrase.add(new Chunk(nrs[i][1], this.TableStandard));
			cell = new PdfPCell(phrase);
			cell.setPadding(cellpadding);
			table.addCell(cell);

			phrase = new Phrase();
			phrase.add(new Chunk(nrs[i][2], this.TableStandardBold));
			new Chunk();
			phrase.add(Chunk.NEWLINE);
			phrase.add(new Chunk(
					"Flat (2) hour onsite maximum plus (1) hour travel",
					this.TableStandard));
			new Chunk();
			phrase.add(Chunk.NEWLINE);
			phrase.add(new Chunk(nrs[i][3], this.TableStandard));
			cell = new PdfPCell(phrase);
			cell.setPadding(5.0F);
			table.addCell(cell);

			phrase = new Phrase();
			phrase.add(new Chunk("15%", this.TableStandardBold));
			cell = new PdfPCell(phrase);
			cell.setPadding(cellpadding);
			cell.setHorizontalAlignment(1);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(nrs[i][4], this.TableStandard));
			cell.setPadding(cellpadding);
			cell.setVerticalAlignment(5);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);

			phrase = new Phrase(nrs[i][5], this.TableStandard);
			cell = new PdfPCell(phrase);
			cell.setPadding(cellpadding);
			cell.setVerticalAlignment(5);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);

			phrase = new Phrase(nrs[i][6], this.TableStandard);
			cell = new PdfPCell(phrase);
			cell.setPadding(cellpadding);
			cell.setVerticalAlignment(5);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);

			phrase = new Phrase(nrs[i][7], this.TableStandard);
			cell = new PdfPCell(phrase);
			cell.setPadding(cellpadding);
			cell.setVerticalAlignment(5);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);
		}
		try {
			table.setTotalWidth(this.FullTableWidth);
			table.setLockedWidth(true);
			table.setWidths(new float[] { 150.0F, 200.0F, 100.0F, 50.0F, 50.0F,
					50.0F, 50.0F });
			table.setSpacingBefore(this.ParagraphSpacing.floatValue());
			table.setSpacingAfter(this.ParagraphSpacing.floatValue());
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return table;
	}

	String[][] getNetMedXSilverRates(String lx_pr_id) {
		String[][] rates = new String[5][8];

		rates[0][0] = "Criticality 1 Dispatch - Emergency";
		rates[0][1] = "(2 Hours Onsite)";
		rates[0][2] = "5x8x2";
		rates[0][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[0][4] = "$204.00";
		rates[0][5] = "$209.00";
		rates[0][6] = "$215.00";
		rates[0][7] = "$245.00";

		rates[1][0] = "Criticality 2 Dispatch - Major";
		rates[1][1] = "(4 Hours Onsite)";
		rates[1][2] = "5x8x2";
		rates[1][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[1][4] = "$158.00";
		rates[1][5] = "$166.00";
		rates[1][6] = "$166.00";
		rates[1][7] = "$200.00";

		rates[2][0] = "Criticality 3 Dispatch - Minor";
		rates[2][1] = "(Same Day Onsite)";
		rates[2][2] = "5x8x8";
		rates[2][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[2][4] = "$110.00";
		rates[2][5] = "$122.00";
		rates[2][6] = "$124.00";
		rates[2][7] = "$179.00";

		rates[3][0] = "Criticality 4 Dispatch - Routine";
		rates[3][1] = "(Next Day Onsite)";
		rates[3][2] = "Next Business Day";
		rates[3][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[3][4] = "$79.00";
		rates[3][5] = "$90.00";
		rates[3][6] = "$93.00";
		rates[3][7] = "$145.00";

		rates[4][0] = "Criticality 5 Dispatch – Scheduled";
		rates[4][1] = "(2nd Day+ Onsite)";
		rates[4][2] = "2nd  Business Day";
		rates[4][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[4][4] = "$71.00";
		rates[4][5] = "$78.00";
		rates[4][6] = "$86.00";
		rates[4][7] = "$133.00";

		return rates;
	}

	PdfPTable buildTableNetMedXCopper() {
		String[][] nrs = getNetMedXCopperRates("");
		float cellpadding = 3.0F;
		PdfPTable table = new PdfPTable(7);

		table.setWidthPercentage(100.0F);
		table.getDefaultCell().setFixedHeight(70.0F);

		Phrase phrase = new Phrase(
				"Field Services – NetMedX Copper (Flat Fee Dispatch)",
				this.TableStandardHeaderBold);
		PdfPCell cell = new PdfPCell(phrase);
		cell.setColspan(7);
		cell.setHorizontalAlignment(1);
		cell.setGrayFill(0.9F);
		cell.setPadding(cellpadding);
		table.addCell(cell);

		cell = new PdfPCell();
		cell.addElement(new Phrase(new Chunk(" ", this.TableStandardBold)));
		table.addCell(cell);

		phrase = new Phrase();
		phrase.add(new Chunk(
				"PLEASE NOTE: THESE ARE DISPATCH RATES (PER VISIT)",
				this.TableStandard));
		cell = new PdfPCell(phrase);
		cell.setColspan(2);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase(new Chunk(
				"Per Dispatch Visit Rates (1 Hr Max Onsite)",
				this.TableStandardBold));
		phrase.add(new Chunk(" 1", this.TableStandard).setTextRise(2.0F));
		cell = new PdfPCell(phrase);
		cell.setColspan(4);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		phrase.add(new Chunk(" ", this.TableStandardBold));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell();
		cell.setColspan(2);
		phrase.add(new Chunk(" ", this.TableStandardBold));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell.addElement(phrase);
		cell.setVerticalAlignment(4);
		table.addCell(cell);

		phrase = new Phrase();
		phrase.add(new Chunk("Technician", this.TableStandardBold));
		phrase.setLeading(this.ParagraphLeading.floatValue());
		cell = new PdfPCell(phrase);
		cell.setColspan(2);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Engineer", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setColspan(2);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Activity", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Description", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Hourly Rate Above 1 hr Onsite",
				this.TableStandardBold);
		phrase.add(new Chunk(" 1", this.TableStandard).setTextRise(2.0F));
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Level 2", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Level 3", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Level 2", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Level 3", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(cellpadding);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);
		for (int i = 0; i < 5; i++) {
			phrase = new Phrase();
			phrase.add(new Chunk(nrs[i][0], this.TableStandardBold));
			new Chunk();
			phrase.add(Chunk.NEWLINE);
			phrase.add(new Chunk(nrs[i][1], this.TableStandard));
			cell = new PdfPCell(phrase);
			cell.setPadding(cellpadding);
			table.addCell(cell);

			phrase = new Phrase();
			phrase.add(new Chunk(nrs[i][2], this.TableStandardBold));
			new Chunk();
			phrase.add(Chunk.NEWLINE);
			phrase.add(new Chunk("Flat (1) hour onsite maximum",
					this.TableStandard));
			new Chunk();
			phrase.add(Chunk.NEWLINE);
			phrase.add(new Chunk(nrs[i][3], this.TableStandard));
			cell = new PdfPCell(phrase);
			cell.setPadding(cellpadding);
			table.addCell(cell);

			phrase = new Phrase();
			phrase.add(new Chunk("Billable at NetMedX Standard Hourly Rates",
					this.TableStandardBold));
			cell = new PdfPCell(phrase);
			cell.setPadding(cellpadding);
			cell.setHorizontalAlignment(1);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(nrs[i][4], this.TableStandard));
			cell.setPadding(cellpadding);
			cell.setVerticalAlignment(5);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);

			phrase = new Phrase(nrs[i][5], this.TableStandard);
			cell = new PdfPCell(phrase);
			cell.setPadding(cellpadding);
			cell.setVerticalAlignment(5);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);

			phrase = new Phrase(nrs[i][6], this.TableStandard);
			cell = new PdfPCell(phrase);
			cell.setPadding(cellpadding);
			cell.setVerticalAlignment(5);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);

			phrase = new Phrase(nrs[i][7], this.TableStandard);
			cell = new PdfPCell(phrase);
			cell.setPadding(cellpadding);
			cell.setVerticalAlignment(5);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);
		}
		try {
			table.setTotalWidth(this.FullTableWidth);
			table.setLockedWidth(true);
			table.setWidths(new float[] { 150.0F, 190.0F, 110.0F, 50.0F, 50.0F,
					50.0F, 50.0F });
			table.setSpacingBefore(this.ParagraphSpacing.floatValue());
			table.setSpacingAfter(this.ParagraphSpacing.floatValue());
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return table;
	}

	String[][] getNetMedXCopperRates(String lx_pr_id) {
		String[][] rates = new String[5][8];

		rates[0][0] = "Criticality 1 Dispatch - Emergency";
		rates[0][1] = "(2 Hours Onsite)";
		rates[0][2] = "5x8x2";
		rates[0][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[0][4] = "$204.00";
		rates[0][5] = "$209.00";
		rates[0][6] = "$215.00";
		rates[0][7] = "$245.00";

		rates[1][0] = "Criticality 2 Dispatch - Major";
		rates[1][1] = "(4 Hours Onsite)";
		rates[1][2] = "5x8x2";
		rates[1][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[1][4] = "$158.00";
		rates[1][5] = "$166.00";
		rates[1][6] = "$166.00";
		rates[1][7] = "$200.00";

		rates[2][0] = "Criticality 3 Dispatch - Minor";
		rates[2][1] = "(Same Day Onsite)";
		rates[2][2] = "5x8x8";
		rates[2][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[2][4] = "$110.00";
		rates[2][5] = "$122.00";
		rates[2][6] = "$124.00";
		rates[2][7] = "$179.00";

		rates[3][0] = "Criticality 4 Dispatch - Routine";
		rates[3][1] = "(Next Day Onsite)";
		rates[3][2] = "Next Business Day";
		rates[3][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[3][4] = "$79.00";
		rates[3][5] = "$90.00";
		rates[3][6] = "$93.00";
		rates[3][7] = "$145.00";

		rates[4][0] = "Criticality 5 Dispatch – Scheduled";
		rates[4][1] = "(2nd Day+ Onsite)";
		rates[4][2] = "2nd  Business Day";
		rates[4][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[4][4] = "$71.00";
		rates[4][5] = "$78.00";
		rates[4][6] = "$86.00";
		rates[4][7] = "$133.00";

		return rates;
	}

	PdfPTable buildTableNetworkMonitoring() {
		String[][] nrs = getNetworkMonitoringRates("");

		PdfPTable table = new PdfPTable(6);

		table.setWidthPercentage(100.0F);
		table.getDefaultCell().setFixedHeight(70.0F);

		Phrase phrase = new Phrase(
				"Network Monitoring – Monthly Activity Based*",
				this.TableStandardHeaderBold);
		PdfPCell cell = new PdfPCell(phrase);
		cell.setColspan(6);
		cell.setHorizontalAlignment(1);
		cell.setGrayFill(0.9F);
		cell.setPadding(5.0F);
		table.addCell(cell);

		phrase = new Phrase("Type", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Activity", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Description", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Availability", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Monthly Fee Per Unit", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Additional Information", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);
		for (int i = 0; i < 5; i++) {
			cell = new PdfPCell(new Phrase(new Chunk(nrs[i][0],
					this.TableStandard)));
			cell.setPadding(5.0F);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(new Chunk(nrs[i][1],
					this.TableStandard)));
			cell.setPadding(5.0F);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(new Chunk(nrs[i][2],
					this.TableStandard)));
			cell.setPadding(5.0F);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(new Chunk(nrs[i][3],
					this.TableStandard)));
			cell.setPadding(5.0F);
			cell.setHorizontalAlignment(1);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(new Chunk(nrs[i][4],
					this.TableStandardBold)));
			cell.setPadding(5.0F);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);

			phrase = new Phrase();
			phrase.add(new Chunk('n', this.SpecialCharacters).setTextRise(1.0F));
			phrase.add(new Chunk(nrs[i][5], this.TableStandard));
			cell = new PdfPCell(phrase);
			cell.setPadding(5.0F);
			table.addCell(cell);
		}
		try {
			table.setTotalWidth(this.FullTableWidth);
			table.setLockedWidth(true);
			table.setWidths(new float[] { 60.0F, 90.0F, 190.0F, 60.0F, 75.0F,
					190.0F });
			table.setSpacingBefore(this.ParagraphSpacing.floatValue());
			table.setSpacingAfter(0.0F);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return table;
	}

	String[][] getNetworkMonitoringRates(String lx_pr_id) {
		String[][] rates = new String[5][6];

		rates[0][0] = "Monitoring";
		rates[0][1] = "Pro-Active ICMP Polling (Circuit)";
		rates[0][2] = "Proactive monitoring of DSL, Cable, 3G, 4G, FIOS, FW or T-1/E-1";
		rates[0][3] = "7x24";
		rates[0][4] = "$9.00";
		rates[0][5] = "  Includes ICMP monitoring of telco CPE when technically possible; Static IP address may be required";

		rates[1][0] = "Monitoring";
		rates[1][1] = "Pro-Active SNMP Polling (Circuit)";
		rates[1][2] = "Proactive monitoring of DSL, Cable, 3G, 4G, FIOS, FW or T-1/E-1; VPN tunnel";
		rates[1][3] = "7x24";
		rates[1][4] = "$11.67";
		rates[1][5] = "  Includes SNMP monitoring of telco CPE when technically possible; Static IP address may be required";

		rates[2][0] = "Monitoring";
		rates[2][1] = "Pro-Active HTTPS Polling (Circuit)";
		rates[2][2] = "Proactive monitoring of DSL, Cable, 3G, 4G, FIOS, FW or T-1/E-1";
		rates[2][3] = "7x24";
		rates[2][4] = "$11.67";
		rates[2][5] = "  Includes HTTPS monitoring of telco CPE when technically possible; Static IP address may be required";

		rates[3][0] = "Monitoring";
		rates[3][1] = "Pro-Active ICMP Polling (Device)";
		rates[3][2] = "Proactive monitoring of VPN Router, Standalone AP, UPS, Analog Modem Backup, 3G Device;";
		rates[3][3] = "7x24";
		rates[3][4] = "$9.00";
		rates[3][5] = "  Includes ICMP monitoring of listed devices when technically possible; Static IP address may be required";

		rates[4][0] = "Monitoring";
		rates[4][1] = "Pro-Active SNMP Polling (Device)";
		rates[4][2] = "Proactive monitoring of VPN Router, Standalone AP, UPS, Analog Modem Backup, 3G Device;";
		rates[4][3] = "7x24";
		rates[4][4] = "$11.67";
		rates[4][5] = "  Includes SNMP monitoring of listed devices when technically possible; Static IP address may be required";

		return rates;
	}

	PdfPTable buildTableCallManagementMonthly() {
		String[][] nrs = getCallManagementMonthlyRates("");

		PdfPTable table = new PdfPTable(6);

		table.setWidthPercentage(100.0F);
		table.getDefaultCell().setFixedHeight(70.0F);

		Phrase phrase = new Phrase(
				"Call Management/Problem Management - Monthly Activity Based*",
				this.TableStandardHeaderBold);
		PdfPCell cell = new PdfPCell(phrase);
		cell.setColspan(6);
		cell.setHorizontalAlignment(1);
		cell.setGrayFill(0.9F);
		cell.setPadding(5.0F);
		table.addCell(cell);

		phrase = new Phrase("Type", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Activity", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Description", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Availability", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Monthly Fee Per Unit", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);

		phrase = new Phrase("Additional Information", this.TableStandardBold);
		cell = new PdfPCell(phrase);
		cell.setPadding(5.0F);
		cell.setHorizontalAlignment(1);
		table.addCell(cell);
		for (int i = 0; i < 8; i++) {
			cell = new PdfPCell(new Phrase(new Chunk(nrs[i][0],
					this.TableStandard)));
			cell.setPadding(5.0F);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(new Chunk(nrs[i][1],
					this.TableStandard)));
			cell.setPadding(5.0F);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(new Chunk(nrs[i][2],
					this.TableStandard)));
			cell.setPadding(5.0F);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(new Chunk(
					"8am-9pm Local Site Time, 7 days", this.TableStandard)));
			cell.setPadding(5.0F);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(new Chunk(nrs[i][4],
					this.TableStandardBold)));
			cell.setPadding(5.0F);
			cell.setHorizontalAlignment(2);
			table.addCell(cell);

			phrase = new Phrase();
			phrase.add(new Chunk('n', this.SpecialCharacters).setTextRise(1.0F));
			phrase.add(new Chunk(nrs[i][5], this.TableStandard));
			cell = new PdfPCell(phrase);
			cell.setPadding(5.0F);
			table.addCell(cell);
		}
		try {
			table.setTotalWidth(this.FullTableWidth);
			table.setLockedWidth(true);
			table.setWidths(new float[] { 60.0F, 75.0F, 190.0F, 80.0F, 70.0F,
					190.0F });
			table.setSpacingBefore(this.ParagraphSpacing.floatValue());
			table.setSpacingAfter(0.0F);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return table;
	}

	String[][] getCallManagementMonthlyRates(String lx_pr_id) {
		String[][] rates = new String[8][6];

		rates[0][0] = "Level 2 Help Desk";
		rates[0][1] = "Base Level Reactive Call & Problem Management (Circuit)";
		rates[0][2] = "Reactive management of DSL, Cable, 3G Primary, or T1 Circuit; No pro-active monitoring.  Includes Incident Management of circuit problem. NOC receives notification by phone, email or web portal.";
		rates[0][3] = "";
		rates[0][4] = "$7.61";
		rates[0][5] = "  30 minute maximum active troubleshooting and then  escalated; Applies to primary or back-up circuit; NOC receives notification by phone, email or web portal.";

		rates[1][0] = "Level 2 Help Desk";
		rates[1][1] = "Base Level Pro-Active Call & Problem Management (Circuit)";
		rates[1][2] = "Proactive management of DSL, Cable, 3G Primary, or T1 Circuit; No pro-active monitoring.  Includes Incident Management of circuit problem. ";
		rates[1][3] = "";
		rates[1][4] = "$7.61";
		rates[1][5] = "  30 minute maximum active troubleshooting and then escalated; Applies to primary or back-up circuit; NOC receives notification by Contingent monitoring tool set. 8am-11pm Eastern 7 days";

		rates[2][0] = "Level 3 Help Desk";
		rates[2][1] = "Advanced Level Reactive Call & Problem Management (Circuit)";
		rates[2][2] = "Reactive management of DSL, Cable, 3G Primary, or T1 Circuit; No pro-active monitoring.  Includes Incident Management of circuit problem. ";
		rates[2][3] = "";
		rates[2][4] = "$4.57";
		rates[2][5] = "  Advanced engineering support limited by degree of access to the subject device. Reimage configuration and/or firmware upgrade included; 60 minute maximum active troubleshooting and then out of scope applies 8am 11pm Eastern 7 days";

		rates[3][0] = "Level 3 Help Desk";
		rates[3][1] = "Advanced Level Pro-active Call & Problem Management (Circuit)";
		rates[3][2] = "Pro-active management of DSL, Cable, 3G Primary, or T1 Circuit; No pro-active monitoring.  Includes Incident Management of circuit problem. ";
		rates[3][3] = "";
		rates[3][4] = "$4.57";
		rates[3][5] = "  Advanced engineering support limited by degree of access to the subject device. Reimage configuration and/or firmware upgrade included; 60 minute maximum active troubleshooting and then out of scope applies";

		rates[4][0] = "Level 2 Help Desk";
		rates[4][1] = "Base Level Reactive Call & Problem Management (Device)";
		rates[4][2] = "Reactive management of: Telco CPE, VPN Router, Standalone AP, UPS, Analog Modem Backup, 3G Device; ";
		rates[4][3] = "";
		rates[4][4] = "$3.80";
		rates[4][5] = "  30 minute maximum active troubleshooting and then escalated";

		rates[5][0] = "Level 2 Help Desk";
		rates[5][1] = "Base Level Pro-Active Call & Problem Management (Device)";
		rates[5][2] = "Pro-active management of: Telco CPE, VPN Router, Standalone AP, UPS, Analog Modem Backup, 3G Device; ";
		rates[5][3] = "";
		rates[5][4] = "$3.80";
		rates[5][5] = "  30 minute maximum active troubleshooting and then escalated";

		rates[6][0] = "Level 3 Help Desk";
		rates[6][1] = "Advanced Level Reactive Call & Problem Management (Device)";
		rates[6][2] = "Reactive management of: Telco CPE, VPN Router, Standalone AP, UPS, Analog Modem Backup, 3G Device; ";
		rates[6][3] = "";
		rates[6][4] = "$2.28";
		rates[6][5] = "  Advanced engineering support limited by degree of access to the subject device. Reimage configuration and/or firmware upgrade included; 60 minute maximum active troubleshooting and then out of scope applies";

		rates[7][0] = "Level 3 Help Desk";
		rates[7][1] = "Advanced Level Pro-active Call & Problem Management (Device)";
		rates[7][2] = "Pro-active management of: Telco CPE, VPN Router, Standalone AP, UPS, Analog Modem Backup, 3G Device; ";
		rates[7][3] = "";
		rates[7][4] = "$2.28";
		rates[7][5] = "  Advanced engineering support limited by degree of access to the subject device. Reimage configuration and/or firmware upgrade included; 60 minute maximum active troubleshooting and then out of scope applies";

		return rates;
	}

	public static void main(String[] args) {
	}
}
