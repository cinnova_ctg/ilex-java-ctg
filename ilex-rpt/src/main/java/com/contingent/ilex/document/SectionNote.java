package com.contingent.ilex.document;

public class SectionNote {
	String Note = "";
	String Content = "";

	public SectionNote(String note, String content) {
		this.Note = note;
		this.Content = content;
	}
}
