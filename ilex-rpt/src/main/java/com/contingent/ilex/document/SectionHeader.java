package com.contingent.ilex.document;

public class SectionHeader {
	String Number = "";
	String Title = "";
	String Content = "";

	public SectionHeader(String number, String title, String content) {
		this.Number = number;
		this.Title = title;
		this.Content = content;
	}
}
