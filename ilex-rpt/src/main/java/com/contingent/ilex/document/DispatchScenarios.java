package com.contingent.ilex.document;

import com.contingent.ilex.report.DataAccess;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class DispatchScenarios {
	Vector<String> CriticalityListByNumber = new Vector();
	Vector<String> CriticalityListByName = new Vector();
	Vector<String> Skills = new Vector();
	Map<String, Double> Price = new HashMap();
	Map<String, Double> ProFormaCost = new HashMap();
	Map<String, Double> Rules = new HashMap();
	Map<String, Double> PartnerRules = new HashMap();
	Map<String, Double> UpLifts = new HashMap();
	String Appendix = "0";
	String RequestType;
	Double MinutemanRate = Double.valueOf(18.0D);
	Double SpeedpayRate = Double.valueOf(24.0D);
	NumberFormat hours = new DecimalFormat("##");
	NumberFormat increment = new DecimalFormat("#0.00000000");
	NumberFormat discount = new DecimalFormat("0.00");
	NumberFormat vgpm = new DecimalFormat("0.00");
	NumberFormat dollar = new DecimalFormat("#00.00");
	String[] RuleColumns = { "lm_dr_labor_minimum", "lm_dr_labor_increment",
			"lm_dr_increment_discount", "lm_dr_travel_minimum",
			"lm_dr_travel_maximum", "lm_dr_travel_increment" };
	String[] RuleNames = { "Labor Minimum", "Labor Increment",
			"Increment Discount", "Travel Minimum", "Travel Maximum",
			"Travel Increment" };
	DataAccess da;

	public DispatchScenarios(DataAccess da, String lx_pr_id, String request_type) {
		this.Appendix = lx_pr_id;
		this.RequestType = request_type;
		this.da = da;

		buildDataStructures();
	}

	Vector<String[]> computeProfitability() {
		Double billable = Double.valueOf(((Double) this.Rules
				.get("Labor Minimum")).doubleValue()
				+ ((Double) this.Rules.get("Travel Minimum")).doubleValue());

		String[] row = new String[17];
		int[] index = { 0, 4, 8, 12 };

		Vector<String[]> profitability = new Vector();
		if ((this.ProFormaCost.size() > 0) && (this.Price.size() > 0)) {
			for (int i = 0; i < this.CriticalityListByName.size(); i++) {
				row = new String[17];
				row[0] = ((String) this.CriticalityListByName.get(i));
				for (int j = 0; j < this.Skills.size(); j++) {
					String instance = (String) this.Skills.get(j) + "~"
							+ (String) this.CriticalityListByNumber.get(i);

					Double mm = Double.valueOf(billable.doubleValue()
							* this.MinutemanRate.doubleValue());
					Double sp = Double.valueOf(billable.doubleValue()
							* this.SpeedpayRate.doubleValue());
					Double pvs = Double.valueOf(billable.doubleValue()
							* ((Double) this.ProFormaCost.get(instance))
									.doubleValue());
					Double revenue = Double
							.valueOf(billable.doubleValue()
									* ((Double) this.Price.get(instance))
											.doubleValue());

					row[(index[j] + 1)] = this.vgpm.format(1.0D
							- mm.doubleValue() / revenue.doubleValue());
					row[(index[j] + 2)] = this.vgpm.format(1.0D
							- sp.doubleValue() / revenue.doubleValue());
					row[(index[j] + 3)] = this.vgpm.format(1.0D
							- pvs.doubleValue() / revenue.doubleValue());
					row[(index[j] + 4)] = this.dollar.format(revenue);
				}
				profitability.add(row);
			}
		}
		return profitability;
	}

	boolean buildDataStructures() {
		boolean rc = true;
		if (!buildCriticalityDataStructures()) {
			rc = false;
		}
		if (!buildSkillsDataStructures()) {
			rc = false;
		}
		if (!buildPriceDataStructures()) {
			rc = false;
		}
		if (!buildProFormaCostDataStructures()) {
			rc = false;
		}
		if (!buildRuleDataStructures()) {
			rc = false;
		}
		if (!buildRuleDataStructures("99")) {
			rc = false;
		}
		this.UpLifts.put("Non-PPS", Double.valueOf(1.5D));
		this.UpLifts.put("Locality", Double.valueOf(1.5D));
		this.UpLifts.put("Union", Double.valueOf(1.5D));

		return rc;
	}

	boolean buildCriticalityDataStructures() {
		boolean rc = true;

		String sql = "select cx_cr_id, replace (cx_cr_criticality, 'Dispatch ', '') as cx_cr_criticality from cx_criticality order by cx_cr_id";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				this.CriticalityListByNumber.add(rs.getString("cx_cr_id"));
				this.CriticalityListByName.add(rs
						.getString("cx_cr_criticality"));
			}
			rs.close();
		} catch (Exception e) {
			rc = false;
		}
		return rc;
	}

	boolean buildSkillsDataStructures() {
		boolean rc = true;

		String sql = "select cx_sk_id from cx_skill order by cx_sk_full_name desc, cx_sk_id";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				this.Skills.add(rs.getString("cx_sk_id"));
			}
			rs.close();
		} catch (Exception e) {
			rc = false;
		}
		return rc;
	}

	boolean buildPriceDataStructures() {
		boolean rc = true;

		String sql = "select cx_sk_id, cx_cr_id, cx_lr_rate from cx_labor_rate where cx_lr_pr_id = "
				+ this.Appendix
				+ " and cx_request_type = '"
				+ this.RequestType
				+ "'";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				this.Price.put(
						rs.getString("cx_sk_id") + "~"
								+ rs.getString("cx_cr_id"),
						Double.valueOf(rs.getDouble("cx_lr_rate")));
			}
			rs.close();
		} catch (Exception e) {
			rc = false;
		}
		return rc;
	}

	boolean buildProFormaCostDataStructures() {
		boolean rc = true;

		String sql = "select cx_sk_id, cx_cr_id, iv_cost from dbo.cx_labor_resource_pool_xref join dbo.iv_resource_pool on cx_xr_rp_id = iv_rp_id";
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				this.ProFormaCost.put(
						rs.getString("cx_sk_id") + "~"
								+ rs.getString("cx_cr_id"),
						Double.valueOf(rs.getDouble("iv_cost")));
			}
			rs.close();
		} catch (Exception e) {
			rc = false;
		}
		return rc;
	}

	boolean buildRuleDataStructures() {
		boolean rc = true;

		String sql = "select * from dbo.lm_dispatch_rules where lm_dr_request_type = '"
				+ this.RequestType + "' and lm_dr_pr_id = " + this.Appendix;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				for (int i = 0; i < this.RuleColumns.length; i++) {
					this.Rules.put(this.RuleNames[i],
							Double.valueOf(rs.getDouble(this.RuleColumns[i])));
				}
			}
			rs.close();
		} catch (Exception e) {
			rc = false;
		}
		return rc;
	}

	boolean buildRuleDataStructures(String partner) {
		boolean rc = true;

		String sql = "select * from dbo.lm_dispatch_rules where lm_dr_request_type = '"
				+ partner + "' and lm_dr_pr_id = " + this.Appendix;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				for (int i = 0; i < this.RuleColumns.length; i++) {
					this.PartnerRules.put(this.RuleNames[i],
							Double.valueOf(rs.getDouble(this.RuleColumns[i])));
				}
			}
			rs.close();
		} catch (Exception e) {
			rc = false;
		}
		return rc;
	}

	public static void main(String[] args) {
		DispatchScenarios x = new DispatchScenarios(new DataAccess(), "0", "1");

		x.computeProfitability();
	}
}