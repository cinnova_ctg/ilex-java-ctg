package com.contingent.ilex.document;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

class GenerateNetMedXAppendix$HeaderFooter extends PdfPageEventHelper {
	public void onEndPage(PdfWriter writer, Document document) {
		Font HeaderFooterStandard = new Font(Font.FontFamily.HELVETICA, 6.0F,
				0, BaseColor.BLACK);
		Font SpecialCharacters = new Font(Font.FontFamily.ZAPFDINGBATS, 4.0F,
				1, BaseColor.BLACK);

		Float ParagraphLeading = Float.valueOf(9.0F);
		Float ParagraphSpacing = Float.valueOf(5.0F);

		String ftr = "";

		writer.getPageNumber();

		PdfContentByte header = writer.getDirectContentUnder();
		try {
			header.addImage(GenerateNetMedXAppendix.HeaderLogo);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		Paragraph paragraph = new Paragraph();
		paragraph.add(new Chunk("Confidential & Propietary",
				HeaderFooterStandard));
		ColumnText.showTextAligned(writer.getDirectContent(), 0, paragraph,
				26.0F, 24.0F, 0.0F);

		paragraph = new Paragraph();

		paragraph.add(new Chunk(
				"Copyright 2002 Contingent Network Services, LLC",
				HeaderFooterStandard));
		ColumnText.showTextAligned(writer.getDirectContent(), 0, paragraph,
				26.0F, 17.0F, 0.0F);

		ftr = String.format("Page %d of 10",
				new Object[] { Integer.valueOf(writer.getPageNumber()) });
		GenerateNetMedXAppendix.FooterText2 = new Phrase(new Chunk(ftr,
				HeaderFooterStandard));
		ColumnText.showTextAligned(writer.getDirectContent(), 1,
				GenerateNetMedXAppendix.FooterText2, 308.0F, 17.0F, 0.0F);

		GenerateNetMedXAppendix.FooterText2 = new Phrase(new Chunk(
				"05/18/2011", HeaderFooterStandard));
		ColumnText.showTextAligned(writer.getDirectContent(), 1,
				GenerateNetMedXAppendix.FooterText2, 575.0F, 17.0F, 0.0F);
	}
}
