  package com.contingent.ilex.document;
  
  public class SectionSubHeader
  {
    String Number = "";
    String Title = "";
    String Content = "";
    
    public SectionSubHeader(String number, String title, String content)
    {
      this.Number = number;
      this.Title = title;
      this.Content = content;
    }
  }
