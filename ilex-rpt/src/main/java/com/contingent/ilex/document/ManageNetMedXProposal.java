package com.contingent.ilex.document;

import com.contingent.ilex.report.DataAccess;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class ManageNetMedXProposal extends DispatchAction {
	String[] CellStyles = { "TBCEl001", "TBCEl002", "TBCEl003",

	"TBCEl004", "TBCEl005" };
	int TBCEl001 = 0;
	int TBCEl002 = 1;
	int TBCEl003 = 2;
	int TBCEl004 = 3;
	int TBCEl005 = 4;
	DataAccess da = new DataAccess();

	public ActionForward netmedx_proposal(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String section = request.getParameter("section");
		if (section != null) {
			request.setAttribute("section", section);
			getSectionData(request, section);
		}
		String forward_key = "netmedx_proposal";
		return mapping.findForward(forward_key);
	}

	void getSectionData(HttpServletRequest request, String section) {
		String sql = "select cx_st_id, cx_st_type, cx_st_number, cx_st_title, cx_st_content from cx_netmedx_proposal_structure where cx_st_section = '"
				+ section + "'";

		Vector<String[]> structure = new Vector();
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] element = new String[5];

				element[0] = rs.getString("cx_st_id");
				element[1] = rs.getString("cx_st_type");
				element[2] = rs.getString("cx_st_number");
				element[3] = rs.getString("cx_st_title");
				element[4] = rs.getString("cx_st_content");
				structure.add(element);
			}
			rs.close();

			request.setAttribute("structure", structure);
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
			request.setAttribute("error", e.getMessage());
		}
	}

	public ActionForward netmedx_proposal_update(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "error";

		buildCriticality();

		String cx_st_id = request.getParameter("cx_st_id");
		String cx_st_title = request.getParameter("cx_st_title");
		String cx_st_content = request.getParameter("cx_st_content");
		if (execute_cx_netmedx_proposal_structure_manage_01A(cx_st_id,
				cx_st_title, cx_st_content)) {
			String section = request.getParameter("section");
			if (section != null) {
				request.setAttribute("section", section);
				getSectionData(request, section);
				forward_key = "netmedx_proposal";
			}
		}
		return mapping.findForward(forward_key);
	}

	boolean execute_cx_netmedx_proposal_structure_manage_01A(String cx_st_id,
			String cx_st_title, String cx_st_content) {
		boolean rc = false;
		int sp_rc = 0;

		String sql = "{? = call cx_netmedx_proposal_structure_manage_01A(?, ?, ?, ?, ?, ?, ?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.registerOutParameter(1, 4);
			stmt.setObject("@cx_st_id", cx_st_id);
			stmt.setObject("@cx_st_section", null);
			stmt.setObject("@cx_st_type", null);
			stmt.setObject("@cx_st_sequence", null);
			stmt.setObject("@cx_st_number", null);
			stmt.setObject("@cx_st_title", cx_st_title);
			stmt.setObject("@cx_st_content", cx_st_content);
			stmt.setObject("@cx_st_page_flow", null);
			stmt.execute();
			sp_rc = stmt.getInt(1);
			stmt.close();
			if (sp_rc > 0) {
				rc = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return rc;
	}

	public ActionForward netmedx_tables(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "netmedx_table";

		String table_number = request.getParameter("tab");
		if (table_number.equals("1")) {
			request.setAttribute("table_name", "Call Criticality");
			request.setAttribute("table", buildCriticality());
		} else if (table_number.equals("2")) {
			request.setAttribute("table_name", "Call Response Time");
			request.setAttribute("table", buildResponseTime());
		} else if (table_number.equals("3")) {
			request.setAttribute("table_name",
					"Call Response Time - Additional Increments");
			request.setAttribute("table", buildResponseTimeIncrements());
		} else if (table_number.equals("4")) {
			request.setAttribute("table_name",
					"Field Services - NetMedX Standard");
			request.setAttribute("table", buildNetMedXStandards());
		} else if (table_number.equals("5")) {
			request.setAttribute("table_name",
					"Field Services - NetMedX Silver");
			request.setAttribute("table", buildNetMedXSilver());
		} else if (table_number.equals("6")) {
			request.setAttribute("table_name",
					"Field Services - NetMedX Copper");
			request.setAttribute("table", buildNetMedXCopper());
		} else if (table_number.equals("7")) {
			request.setAttribute("table_name",
					"Logistics Services - NetMedX Standard");
			request.setAttribute("table", buildLogisticsServices());
		} else if (table_number.equals("8")) {
			request.setAttribute("table_name",
					"Call Management/Problem Management - Monthly Activity Based");
			request.setAttribute("table", buildCallManagement());
		} else if (table_number.equals("9")) {
			request.setAttribute("table_name",
					"Network Monitoring - Monthly Activity Based");
			request.setAttribute("table", buildNetworkMonitoring());
		} else if (table_number.equals("10")) {
			request.setAttribute("table_name",
					"Call Management Resources/Rates");
			request.setAttribute("table", buildCallManagementRates());
		} else if (table_number.equals("11")) {
			request.setAttribute("table_name", "Field Services - NetMedX Black");
			request.setAttribute("table", buildNetMedXStandards());
		}
		return mapping.findForward(forward_key);
	}

	String buildCriticality() {
		String table_html = "";
		String row_html = "";

		table_html = "<table width=\"650\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" class=\"TBTOP001\">\n<tr><td class=\"TBHDR001\">Criticality</td><td class=\"TBHDR001\">Condition</td></tr>\n";

		Vector<Vector<String[]>> table = buildTableCriticalityData();
		for (int i = 0; i < table.size(); i++) {
			Vector<String[]> row = (Vector) table.get(i);
			row_html = row_html + "<tr>";
			for (int j = 0; j < row.size(); j++) {
				String[] cell = (String[]) row.get(j);
				row_html = row_html + "<td class=\""
						+ this.CellStyles[java.lang.Integer.parseInt(cell[1])]
						+ "\">" + cell[2] + "</td>";
			}
			row_html = row_html + "</tr>\n";
		}
		table_html = table_html + row_html + "</table>\n";

		return table_html;
	}

	Vector buildTableCriticalityData() {
		Vector<Vector<String[]>> table = new Vector();
		Vector<String[]> row = new Vector();
		String[] cell = new String[3];

		cell[0] = "1";
		cell[1] = "0";
		cell[2] = "1 - Emergency";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "A problem that is determined by Client to require immediate response.";
		row.add(cell);
		table.add(row);

		row = new Vector();
		cell = new String[3];
		cell[0] = "1";
		cell[1] = "0";
		cell[2] = "2 - Major";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "A problem that prevents the End-Users ability to access the network.  The network, in whole or in part, is \"down\" and is considered inoperable.  Client or Client's Customers are unable to communicate on the network by either primary or backup means.";
		row.add(cell);
		table.add(row);

		row = new Vector();
		cell = new String[3];
		cell[0] = "1";
		cell[1] = "0";
		cell[2] = "3 - Minor";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "A minor problem that slightly impacts the End-Users ability to access the network.  The problem may be intermittent in nature.  End-Users have not been hampered by an apparent failure, but rather an inconvenience without a need for fast resolution.";
		row.add(cell);
		table.add(row);

		row = new Vector();
		cell = new String[3];
		cell[0] = "1";
		cell[1] = "0";
		cell[2] = "4 - Routine";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "A minor problem that does not impact the End-Users ability to access the network.  The problem may be intermittent in nature. This criticality would also apply to questions and/or manners of general consultation.";
		row.add(cell);
		table.add(row);

		row = new Vector();
		cell = new String[3];
		cell[0] = "1";
		cell[1] = "0";
		cell[2] = "5 - Scheduled T&M";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "A minor problem or scheduled maintenance that does not impact the End-Users ability to access the network.  The problem may be intermittent in nature. This criticality would also apply to questions and/or manners of general consultation.";
		row.add(cell);
		table.add(row);

		return table;
	}

	String buildResponseTime() {
		String table_html = "";
		String row_html = "";

		table_html = "<table width=\"700\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" class=\"TBTOP001\">\n<tr><td class=\"TBHDR001\">&nbsp;</td><td class=\"TBHDR001\" colspan=\"2\">Non-PPS (after hours) Arrival -<br>On Site Response Goal</td><td class=\"TBHDR001\" colspan=\"2\">PPS Arrival -<br>On Site Response Goal</td></tr>\n<tr><td class=\"TBCEl002\" width=\"75\"><strong>Activity</strong></td><td class=\"TBCEl002\" width=\"150\"><strong>Call Received</strong><br>8:00A.M.- 3:00P.M. M-F<br>Site Time</td><td class=\"TBCEl002\" width=\"150\"><strong>Call Received</strong><br>3:00 P.M.-8:00A.M.<br>Site Time</td><td class=\"TBCEl002\" width=\"150\"><strong>Call Received</strong><br>8:00A.M.- 3:00P.M. M-F<br>Site Time</td><td class=\"TBCEl002\" width=\"150\"><strong>Call Received</strong><br>3:00P.M- 8:00A.M.<br>Site Time</td></tr>\n";

		Vector<Vector<String[]>> table = buildTableResponseTimeData();
		for (int i = 0; i < table.size(); i++) {
			Vector<String[]> row = (Vector) table.get(i);
			row_html = row_html + "<tr>";
			for (int j = 0; j < row.size(); j++) {
				String[] cell = (String[]) row.get(j);
				row_html = row_html + "<td class=\""
						+ this.CellStyles[java.lang.Integer.parseInt(cell[1])]
						+ "\">" + cell[2] + "</td>";
			}
			row_html = row_html + "</tr>\n";
		}
		table_html = table_html + row_html + "</table>\n";

		return table_html;
	}

	Vector buildTableResponseTimeData() {
		Vector<Vector<String[]>> table = new Vector();
		Vector<String[]> row = new Vector();
		String[] cell = new String[3];

		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "Criticality 1 Emergency";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "Does Not Apply.";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "2 Hours";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "2 Business Hours.";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "2 Hours after start of next calendar day";
		row.add(cell);
		table.add(row);

		row = new Vector();
		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "Criticality 2 Major";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "4 Hours";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "4 Hours";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "4 Business Hours.";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "4 Hours after start of next calendar day";
		row.add(cell);
		table.add(row);

		row = new Vector();
		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "Criticality 3 Minor";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "8 Hours";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "8 Hours";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "8 Business Hours.";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "8 Hours after start of next calendar day";
		row.add(cell);
		table.add(row);

		row = new Vector();
		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "Criticality 4 Routine";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "Next Day";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "Next Day";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "Next Business Day";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "2nd Business Day";
		row.add(cell);
		table.add(row);

		row = new Vector();
		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "Criticality 5 Scheduled";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "Does Not Apply";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "Does Not Apply";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "2nd Business Day";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "2nd Business Day";
		row.add(cell);
		table.add(row);

		return table;
	}

	String buildResponseTimeIncrements() {
		String table_html = "";
		String row_html = "";

		table_html = "<table width=\"450\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" class=\"TBTOP001\">\n<tr><td class=\"TBHDR001\">Distance between the Field Technician and the Site</td><td class=\"TBHDR001\">Additional Response Time Increment</td></tr>\n";

		Vector<Vector<String[]>> table = buildTableResponseTimeIncrements();
		for (int i = 0; i < table.size(); i++) {
			Vector<String[]> row = (Vector) table.get(i);
			row_html = row_html + "<tr>";
			for (int j = 0; j < row.size(); j++) {
				String[] cell = (String[]) row.get(j);
				row_html = row_html + "<td class=\""
						+ this.CellStyles[java.lang.Integer.parseInt(cell[1])]
						+ "\">" + cell[2] + "</td>";
			}
			row_html = row_html + "</tr>\n";
		}
		table_html = table_html + row_html + "</table>\n";

		return table_html;
	}

	Vector buildTableResponseTimeIncrements() {
		Vector<Vector<String[]>> table = new Vector();
		Vector<String[]> row = new Vector();
		String[] cell = new String[3];

		cell[0] = "1";
		cell[1] = "0";
		cell[2] = "0 - 50 miles";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "0";
		row.add(cell);
		table.add(row);

		row = new Vector();
		cell = new String[3];
		cell[0] = "1";
		cell[1] = "0";
		cell[2] = "51 - 100 miles";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "+ 1 hour";
		row.add(cell);
		table.add(row);

		row = new Vector();
		cell = new String[3];
		cell[0] = "1";
		cell[1] = "0";
		cell[2] = "101 - 150 miles";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "+ 2 hours";
		row.add(cell);
		table.add(row);

		row = new Vector();
		cell = new String[3];
		cell[0] = "1";
		cell[1] = "0";
		cell[2] = "151 - 200 miles";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "+ 3 hours";
		row.add(cell);
		table.add(row);

		row = new Vector();
		cell = new String[3];
		cell[0] = "1";
		cell[1] = "0";
		cell[2] = "&gt; 200 miles";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "+ 4 hours";
		row.add(cell);
		table.add(row);

		return table;
	}

	String buildNetMedXStandards() {
		String table_html = "";
		String row_html = "";

		table_html = "<table width=\"800\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" class=\"TBTOP001\">\n<tr><td class=\"TBHDR002\" colspan=\"6\">Field Services - NetMedX Standard (Time & Material)</td></tr>\n<tr><td class=\"TBHDR002\">&nbsp;</td><td class=\"TBHDR002\">&nbsp;</td><td class=\"TBHDR002\" colspan=\"4\">Hourly Labor Rates</td></tr>\n<tr><td class=\"TBHDR002\">&nbsp;</td><td class=\"TBHDR002\">&nbsp;</td><td class=\"TBHDR002\" colspan=\"2\">Technician</td><td class=\"TBHDR002\" colspan=\"2\">Engineer</td></tr>\n<tr><td class=\"TBCEl003\" width=\"200\">Activity</td><td class=\"TBCEl003\" width=\"300\">Description</td><td class=\"TBCEl003\" width=\"75\">Level 2</td><td class=\"TBCEl003\" width=\"75\">Level 3</td><td class=\"TBCEl003\" width=\"75\">Level 2</td><td class=\"TBCEl003\" width=\"75\">Level 3</td></tr>\n";

		Vector<Vector<String[]>> table = getNetMedXRates();
		for (int i = 0; i < table.size(); i++) {
			Vector<String[]> row = (Vector) table.get(i);
			row_html = row_html + "<tr>";
			for (int j = 0; j < row.size(); j++) {
				String[] cell = (String[]) row.get(j);
				row_html = row_html + "<td class=\""
						+ this.CellStyles[java.lang.Integer.parseInt(cell[1])]
						+ "\">" + cell[2] + "</td>";
			}
			row_html = row_html + "</tr>\n";
		}
		table_html = table_html + row_html + "</table>\n";

		return table_html;
	}

	Vector<Vector<String[]>> getNetMedXRates() {
		Vector<Vector<String[]>> table = new Vector();
		Vector<String[]> row = new Vector();
		String[] cell = new String[3];

		String[][] rates = new String[5][8];

		rates[0][0] = "Criticality 1 Dispatch - Emergency";
		rates[0][1] = "(2 Hours Onsite)";
		rates[0][2] = "5x8x2";
		rates[0][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[0][4] = "$204.00";
		rates[0][5] = "$209.00";
		rates[0][6] = "$215.00";
		rates[0][7] = "$245.00";

		rates[1][0] = "Criticality 2 Dispatch - Major";
		rates[1][1] = "(4 Hours Onsite)";
		rates[1][2] = "5x8x2";
		rates[1][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[1][4] = "$158.00";
		rates[1][5] = "$166.00";
		rates[1][6] = "$166.00";
		rates[1][7] = "$200.00";

		rates[2][0] = "Criticality 3 Dispatch - Minor";
		rates[2][1] = "(Same Day Onsite)";
		rates[2][2] = "5x8x8";
		rates[2][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[2][4] = "$110.00";
		rates[2][5] = "$122.00";
		rates[2][6] = "$124.00";
		rates[2][7] = "$179.00";

		rates[3][0] = "Criticality 4 Dispatch - Routine";
		rates[3][1] = "(Next Day Onsite)";
		rates[3][2] = "Next Business Day";
		rates[3][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[3][4] = "$79.00";
		rates[3][5] = "$90.00";
		rates[3][6] = "$93.00";
		rates[3][7] = "$145.00";

		rates[4][0] = "Criticality 5 Dispatch - Scheduled";
		rates[4][1] = "(2nd Day+ Onsite)";
		rates[4][2] = "2nd  Business Day";
		rates[4][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[4][4] = "$71.00";
		rates[4][5] = "$78.00";
		rates[4][6] = "$86.00";
		rates[4][7] = "$133.00";
		for (int i = 0; i < 5; i++) {
			row = new Vector();
			for (int j = 0; j < 8; j++) {
				cell = new String[3];
				cell[0] = "1";
				cell[1] = "1";
				switch (j) {
				case 0:
					cell[2] = ("<strong>" + rates[i][0] + "</strong><br>" + rates[i][1]);
					row.add(cell);
					break;
				case 2:
					cell[2] = ("<strong>" + rates[i][2] + "</strong><br>" + rates[i][3]);
					row.add(cell);
					break;
				case 1:
					break;
				case 3:
					break;
				default:
					cell[2] = "&nbsp;";
					row.add(cell);
				}
			}
			table.add(row);
		}
		return table;
	}

	String buildNetMedXSilver() {
		String table_html = "";
		String row_html = "";

		table_html = "<table width=\"800\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" class=\"TBTOP001\">\n<tr><td class=\"TBHDR002\" colspan=\"7\">Field Services - NetMedX Silver (Prepaid Dispatch \"Blocks\")</td></tr>\n<tr><td class=\"TBHDR002\">&nbsp;</td><td class=\"TBCEl003\" colspan=\"2\">PLEASE NOTE: THESE ARE PREPAID DISPATCH RATES (PER VISIT)</td><td class=\"TBHDR002\" colspan=\"4\">Per Dispatch Visit Discounted Rates</td></tr>\n<tr><td class=\"TBHDR002\">&nbsp;</td><td class=\"TBHDR002\" colspan=\"2\">&nbsp;</td><td class=\"TBHDR002\" colspan=\"2\">Technician</td><td class=\"TBHDR002\" colspan=\"2\">Engineer</td></tr>\n<tr><td class=\"TBHDR002\" width=\"235\">Activity</td><td class=\"TBHDR002\" width=\"305\">Description</td><td class=\"TBHDR002\" width=\"165\">Penalty for Late Arrival</td><td class=\"TBHDR002\" width=\"45\">Level 2</td><td class=\"TBHDR002\" width=\"45\">Level 3</td><td class=\"TBHDR002\" width=\"45\">Level 2</td><td class=\"TBHDR002\" width=\"45\">Level 3</td></tr>\n";

		Vector<Vector<String[]>> table = getNetMedXRatesSilver();
		for (int i = 0; i < table.size(); i++) {
			Vector<String[]> row = (Vector) table.get(i);
			row_html = row_html + "<tr>";
			for (int j = 0; j < row.size(); j++) {
				String[] cell = (String[]) row.get(j);
				row_html = row_html + "<td class=\""
						+ this.CellStyles[java.lang.Integer.parseInt(cell[1])]
						+ "\">" + cell[2] + "</td>";
			}
			row_html = row_html + "</tr>\n";
		}
		table_html = table_html + row_html + "</table>\n";

		return table_html;
	}

	Vector<Vector<String[]>> getNetMedXRatesSilver() {
		Vector<Vector<String[]>> table = new Vector();
		Vector<String[]> row = new Vector();
		String[] cell = new String[3];

		String[][] rates = new String[5][8];

		rates[0][0] = "Criticality 1 Dispatch - Emergency";
		rates[0][1] = "(2 Hours Onsite)";
		rates[0][2] = "5x8x2";
		rates[0][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[0][4] = "$204.00";
		rates[0][5] = "$209.00";
		rates[0][6] = "$215.00";
		rates[0][7] = "$245.00";

		rates[1][0] = "Criticality 2 Dispatch - Major";
		rates[1][1] = "(4 Hours Onsite)";
		rates[1][2] = "5x8x2";
		rates[1][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[1][4] = "$158.00";
		rates[1][5] = "$166.00";
		rates[1][6] = "$166.00";
		rates[1][7] = "$200.00";

		rates[2][0] = "Criticality 3 Dispatch - Minor";
		rates[2][1] = "(Same Day Onsite)";
		rates[2][2] = "5x8x8";
		rates[2][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[2][4] = "$110.00";
		rates[2][5] = "$122.00";
		rates[2][6] = "$124.00";
		rates[2][7] = "$179.00";

		rates[3][0] = "Criticality 4 Dispatch - Routine";
		rates[3][1] = "(Next Day Onsite)";
		rates[3][2] = "Next Business Day";
		rates[3][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[3][4] = "$79.00";
		rates[3][5] = "$90.00";
		rates[3][6] = "$93.00";
		rates[3][7] = "$145.00";

		rates[4][0] = "Criticality 5 Dispatch - Scheduled";
		rates[4][1] = "(2nd Day+ Onsite)";
		rates[4][2] = "2nd  Business Day";
		rates[4][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[4][4] = "$71.00";
		rates[4][5] = "$78.00";
		rates[4][6] = "$86.00";
		rates[4][7] = "$133.00";
		for (int i = 0; i < 5; i++) {
			row = new Vector();
			for (int j = 0; j < 8; j++) {
				cell = new String[3];
				cell[0] = "1";
				switch (j) {
				case 4:
					cell[1] = "2";
					break;
				default:
					cell[1] = "1";
				}
				switch (j) {
				case 0:
					cell[2] = ("<strong>" + rates[i][0] + "</strong><br>" + rates[i][1]);
					row.add(cell);
					break;
				case 2:
					cell[2] = ("<strong>"
							+ rates[i][2]
							+ "</strong><br>Flat (2) hour onsite maximum plus (1) hour travel<br>" + rates[i][3]);
					row.add(cell);
					break;
				case 4:
					cell[2] = "<strong>15%</strong>";
					row.add(cell);
					cell = new String[3];
					cell[0] = "1";
					cell[1] = "1";
					cell[2] = "&nbsp;";
					row.add(cell);
					break;
				case 1:
					break;
				case 3:
					break;
				default:
					cell[2] = "&nbsp;";
					row.add(cell);
				}
			}
			table.add(row);
		}
		return table;
	}

	String buildNetMedXCopper() {
		String table_html = "";
		String row_html = "";

		table_html = "<table width=\"800\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" class=\"TBTOP001\">\n<tr><td class=\"TBHDR002\" colspan=\"7\">Field Services - NetMedX Copper (Flat Fee Dispatch)</td></tr>\n<tr><td class=\"TBHDR002\">&nbsp;</td><td class=\"TBCEl003\" colspan=\"2\">PLEASE NOTE: THESE ARE DISPATCH RATES (PER VISIT)</td><td class=\"TBHDR002\" colspan=\"4\">Per Dispatch Visit Rates (1 Hr Max Onsite)</td></tr>\n<tr><td class=\"TBHDR002\">&nbsp;</td><td class=\"TBHDR002\" colspan=\"2\">&nbsp;</td><td class=\"TBHDR002\" colspan=\"2\">Technician</td><td class=\"TBHDR002\" colspan=\"2\">Engineer</td></tr>\n<tr><td class=\"TBHDR002\" width=\"250\">Activity</td><td class=\"TBHDR002\" width=\"300\">Description</td><td class=\"TBHDR002\" width=\"200\">Hourly Rate Above 1 hr Onsite</td><td class=\"TBHDR002\" width=\"50\">Level 2</td><td class=\"TBHDR002\" width=\"50\">Level 3</td><td class=\"TBHDR002\" width=\"50\">Level 2</td><td class=\"TBHDR002\" width=\"50\">Level 3</td></tr>\n";

		Vector<Vector<String[]>> table = getNetMedXRatesCopper();
		for (int i = 0; i < table.size(); i++) {
			Vector<String[]> row = (Vector) table.get(i);
			row_html = row_html + "<tr>";
			for (int j = 0; j < row.size(); j++) {
				String[] cell = (String[]) row.get(j);
				row_html = row_html + "<td class=\""
						+ this.CellStyles[java.lang.Integer.parseInt(cell[1])]
						+ "\">" + cell[2] + "</td>";
			}
			row_html = row_html + "</tr>\n";
		}
		table_html = table_html + row_html + "</table>\n";

		return table_html;
	}

	Vector<Vector<String[]>> getNetMedXRatesCopper() {
		Vector<Vector<String[]>> table = new Vector();
		Vector<String[]> row = new Vector();
		String[] cell = new String[3];

		String[][] rates = new String[5][8];

		rates[0][0] = "Criticality 1 Dispatch - Emergency";
		rates[0][1] = "(2 Hours Onsite)";
		rates[0][2] = "5x8x2";
		rates[0][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[0][4] = "$204.00";
		rates[0][5] = "$209.00";
		rates[0][6] = "$215.00";
		rates[0][7] = "$245.00";

		rates[1][0] = "Criticality 2 Dispatch - Major";
		rates[1][1] = "(4 Hours Onsite)";
		rates[1][2] = "5x8x2";
		rates[1][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[1][4] = "$158.00";
		rates[1][5] = "$166.00";
		rates[1][6] = "$166.00";
		rates[1][7] = "$200.00";

		rates[2][0] = "Criticality 3 Dispatch - Minor";
		rates[2][1] = "(Same Day Onsite)";
		rates[2][2] = "5x8x8";
		rates[2][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[2][4] = "$110.00";
		rates[2][5] = "$122.00";
		rates[2][6] = "$124.00";
		rates[2][7] = "$179.00";

		rates[3][0] = "Criticality 4 Dispatch - Routine";
		rates[3][1] = "(Next Day Onsite)";
		rates[3][2] = "Next Business Day";
		rates[3][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[3][4] = "$79.00";
		rates[3][5] = "$90.00";
		rates[3][6] = "$93.00";
		rates[3][7] = "$145.00";

		rates[4][0] = "Criticality 5 Dispatch - Scheduled";
		rates[4][1] = "(2nd Day+ Onsite)";
		rates[4][2] = "2nd  Business Day";
		rates[4][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rates[4][4] = "$71.00";
		rates[4][5] = "$78.00";
		rates[4][6] = "$86.00";
		rates[4][7] = "$133.00";
		for (int i = 0; i < 5; i++) {
			row = new Vector();
			for (int j = 0; j < 8; j++) {
				cell = new String[3];
				cell[0] = "1";
				switch (j) {
				case 4:
					cell[1] = "2";
					break;
				default:
					cell[1] = "1";
				}
				switch (j) {
				case 0:
					cell[2] = ("<strong>" + rates[i][0] + "</strong><br>" + rates[i][1]);
					row.add(cell);
					break;
				case 2:
					cell[2] = ("<strong>" + rates[i][2]
							+ "</strong><br>Flat (1) hour onsite maximum<br>" + rates[i][3]);
					row.add(cell);
					break;
				case 4:
					cell[2] = "<strong>Billable at NetMedX Standard Hourly Rates</strong>";
					row.add(cell);
					cell = new String[3];
					cell[0] = "1";
					cell[1] = "1";
					cell[2] = "&nbsp;";
					row.add(cell);
					break;
				case 1:
					break;
				case 3:
					break;
				default:
					cell[2] = "&nbsp;";
					row.add(cell);
				}
			}
			table.add(row);
		}
		return table;
	}

	String buildLogisticsServices() {
		String table_html = "";
		String row_html = "";

		table_html = "<table width=\"800\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" class=\"TBTOP001\">\n<tr><td class=\"TBHDR002\" colspan=\"4\">Logistics Services - NetMedX Standard</td></tr>\n<tr><td class=\"TBCEl003\" width=\"250\">Activity</td><td class=\"TBCEl003\" width=\"250\">Description</td><td class=\"TBCEl003\" width=\"100\">Activity Rates</td><td class=\"TBCEl003\" width=\"250\">Additional Information</td></tr>\n";

		Vector<Vector<String[]>> table = getLogisticsRates();
		for (int i = 0; i < table.size(); i++) {
			Vector<String[]> row = (Vector) table.get(i);
			row_html = row_html + "<tr>";
			for (int j = 0; j < row.size(); j++) {
				String[] cell = (String[]) row.get(j);
				row_html = row_html + "<td class=\""
						+ this.CellStyles[java.lang.Integer.parseInt(cell[1])]
						+ "\">" + cell[2] + "</td>";
			}
			row_html = row_html + "</tr>\n";
		}
		table_html = table_html + row_html + "</table>\n";

		return table_html;
	}

	Vector<Vector<String[]>> getLogisticsRates() {
		Vector<Vector<String[]>> table = new Vector();
		Vector<String[]> row = new Vector();
		String[] cell = new String[3];

		String[][] rs = new String[12][6];

		rs[0][0] = "Parts Depot Storage";
		rs[0][1] = "Nationwide Coverage Available";
		rs[0][2] = "64 Cubic Feet";
		rs[0][3] = "(1 pallet)";
		rs[0][4] = "$60 per month";
		rs[0][5] = "  Monthly price is per location.~  Add 15% for climate control.~   Delivery not included.";

		rs[1][0] = "Parts Depot Storage";
		rs[1][1] = "Nationwide Coverage Available";
		rs[1][2] = "32 Cubic Feet";
		rs[1][3] = "(1/2 pallet)";
		rs[1][4] = "$45 per month";
		rs[1][5] = "  Monthly price is per location.~  Add 15% for climate control.~   Delivery not included.";

		rs[2][0] = "Spares Deployment Processing";
		rs[2][1] = "(Next Business Day)";
		rs[2][2] = "Spare Parts Deployment Processing";
		rs[2][3] = "";
		rs[2][4] = "$25 per incident";
		rs[2][5] = "  Materials Charged Separately~  Shipping not included";

		rs[3][0] = "RMA Processing";
		rs[3][1] = "(5 business days from receipt of faulty part)";
		rs[3][2] = "Warranty Parts Return Processing";
		rs[3][3] = "";
		rs[3][4] = "$25 per incident";
		rs[3][5] = "  Materials Charged Separately~  Shipping not included";

		rs[4][0] = "Bench Repair (Hardware/Software)";
		rs[4][1] = "(best effort only)";
		rs[4][2] = "Component Repair";
		rs[4][3] = "";
		rs[4][4] = "$85 per hour";
		rs[4][5] = "  Materials Charged Separately~   Repair services are best effort only~  Shipping not included";

		rs[5][0] = "Parts Courier Services Criticality 1 - Emergency";
		rs[5][1] = "(2 Hours Onsite)";
		rs[5][2] = "5x8x2";
		rs[5][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rs[5][4] = "$275 per delivery";
		rs[5][5] = "  Price includes parcels weighing less than (25) lbs~  Add 15% for delivery outside PPS";

		rs[6][0] = "Parts Courier Services Criticality 2 - Major";
		rs[6][1] = "(4 Hours Onsite)";
		rs[6][2] = "5x8x4";
		rs[6][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rs[6][4] = "$185 per delivery";
		rs[6][5] = "  Price includes parcels weighing less than (25) lbs~  Add 15% for delivery outside PPS";

		rs[7][0] = "Parts Courier Services Criticality 3 - Minor";
		rs[7][1] = "(8 Hours Onsite)";
		rs[7][2] = "5x8x8";
		rs[7][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rs[7][4] = "$150 per delivery";
		rs[7][5] = "  Price includes parcels weighing less than (25) lbs~  Add 15% for delivery outside PPS";

		rs[8][0] = "Parts Courier Services Criticality 4 - Routine";
		rs[8][1] = "(Next Business Day Onsite)";
		rs[8][2] = "Next Business Day";
		rs[8][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rs[8][4] = "$50 per delivery";
		rs[8][5] = "  Price includes parcels weighing less than (25) lbs~  Add 15% for delivery outside PPS";

		rs[9][0] = "Parts Courier Services Criticality 5 - Scheduled";
		rs[9][1] = "(2nd Business Day Onsite)";
		rs[9][2] = "2nd Business Day";
		rs[9][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rs[9][4] = "$25 per delivery";
		rs[9][5] = "  Price includes parcels weighing less than (25) lbs~  Add 15% for delivery outside PPS";

		rs[10][0] = "Asset Tracking ";
		rs[10][1] = "(7x24 Online Access)";
		rs[10][2] = "Customer Care Center Online";
		rs[10][3] = "www.contingent.net/ccc";
		rs[10][4] = "$199 per month ";
		rs[10][5] = "  Price includes up to 1000 units tracked, 3 user accounts~   Add $175/month for each 1000 unit block and $25/month for each additional user account";

		rs[11][0] = "Certified Equipment Disposal";
		rs[11][1] = "(48 Hours Notice to Pickup)";
		rs[11][2] = "2nd Business Day";
		rs[11][3] = "PPS (M-F 8:00A.M. - 5:00P.M. local site time)";
		rs[11][4] = "$0.83 per pound";
		rs[11][5] = "  Price includes freight, disposal, certificate by serial number reporting, EPA and DoD/DHS compliance when applicable, Federal and State Law compliance~   Minimum 50lbs per visit";
		for (int i = 0; i < 12; i++) {
			row = new Vector();
			for (int j = 0; j < 6; j++) {
				cell = new String[3];

				cell[0] = "1";
				cell[1] = "1";
				switch (j) {
				case 0:
					cell[2] = ("<strong>" + rs[i][0] + "</strong><br>" + rs[i][1]);
					row.add(cell);
					break;
				case 2:
					cell[2] = ("<strong>" + rs[i][2] + "</strong><br>" + rs[i][3]);
					row.add(cell);
					break;
				case 4:
					cell[2] = ("<strong>" + rs[i][4] + "</strong>");
					row.add(cell);
					break;
				case 5:
					String rs5 = "";
					String[] bullets = rs[i][5].split("~");
					for (int k = 0; k < bullets.length; k++) {
						rs5 = rs5 + "&bull;&nbsp;&nbsp;" + bullets[k].trim()
								+ "<BR>";
					}
					cell[2] = rs5;
					row.add(cell);
					break;
				}
			}
			table.add(row);
		}
		return table;
	}

	String buildCallManagement() {
		String table_html = "";
		String row_html = "";

		table_html = "<table width=\"810\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" class=\"TBTOP001\">\n<tr><td class=\"TBHDR002\" colspan=\"6\">Call Management/Problem Management - Monthly Activity Based*</td></tr>\n<tr><td class=\"TBHDR002\" width=\"75\">Type</td><td class=\"TBHDR002\" width=\"100\">Activity</td><td class=\"TBHDR002\" width=\"275\">Description</td><td class=\"TBHDR002\" width=\"125\">Availability</td><td class=\"TBHDR002\" width=\"80\">Monthly Fee Per Unit</td><td class=\"TBHDR002\" width=\"290\">Additional Information</td></tr>\n";

		Vector<Vector<String[]>> table = getCallManagementMonthlyRates();
		for (int i = 0; i < table.size(); i++) {
			Vector<String[]> row = (Vector) table.get(i);
			row_html = row_html + "<tr>";
			for (int j = 0; j < row.size(); j++) {
				String[] cell = (String[]) row.get(j);
				row_html = row_html + "<td class=\""
						+ this.CellStyles[java.lang.Integer.parseInt(cell[1])]
						+ "\">" + cell[2] + "</td>";
			}
			row_html = row_html + "</tr>\n";
		}
		table_html = table_html + row_html + "</table>\n";

		return table_html;
	}

	Vector<Vector<String[]>> getCallManagementMonthlyRates() {
		Vector<Vector<String[]>> table = new Vector();
		Vector<String[]> row = new Vector();
		String[] cell = new String[3];

		String[][] rates = new String[8][6];

		rates[0][0] = "Level 2 Help Desk";
		rates[0][1] = "Base Level Reactive Call & Problem Management (Circuit)";
		rates[0][2] = "Reactive management of DSL, Cable, 3G Primary, or T1 Circuit; No pro-active monitoring.  Includes Incident Management of circuit problem. NOC receives notification by phone, email or web portal.";
		rates[0][3] = "8am-9pm Local Site Time, 7 days";
		rates[0][4] = "$7.61";
		rates[0][5] = "  30 minute maximum active troubleshooting and then  escalated; Applies to primary or back-up circuit; NOC receives notification by phone, email or web portal.";

		rates[1][0] = "Level 2 Help Desk";
		rates[1][1] = "Base Level Pro-Active Call & Problem Management (Circuit)";
		rates[1][2] = "Proactive management of DSL, Cable, 3G Primary, or T1 Circuit; No pro-active monitoring.  Includes Incident Management of circuit problem. ";
		rates[1][3] = "8am-9pm Local Site Time, 7 days";
		rates[1][4] = "$7.61";
		rates[1][5] = "  30 minute maximum active troubleshooting and then escalated; Applies to primary or back-up circuit; NOC receives notification by Contingent monitoring tool set. 8am-11pm Eastern 7 days";

		rates[2][0] = "Level 3 Help Desk";
		rates[2][1] = "Advanced Level Reactive Call & Problem Management (Circuit)";
		rates[2][2] = "Reactive management of DSL, Cable, 3G Primary, or T1 Circuit; No pro-active monitoring.  Includes Incident Management of circuit problem. ";
		rates[2][3] = "8am-9pm Local Site Time, 7 days";
		rates[2][4] = "$4.57";
		rates[2][5] = "  Advanced engineering support limited by degree of access to the subject device. Reimage configuration and/or firmware upgrade included; 60 minute maximum active troubleshooting and then out of scope applies 8am 11pm Eastern 7 days";

		rates[3][0] = "Level 3 Help Desk";
		rates[3][1] = "Advanced Level Pro-active Call & Problem Management (Circuit)";
		rates[3][2] = "Pro-active management of DSL, Cable, 3G Primary, or T1 Circuit; No pro-active monitoring.  Includes Incident Management of circuit problem. ";
		rates[3][3] = "8am-9pm Local Site Time, 7 days";
		rates[3][4] = "$4.57";
		rates[3][5] = "  Advanced engineering support limited by degree of access to the subject device. Reimage configuration and/or firmware upgrade included; 60 minute maximum active troubleshooting and then out of scope applies";

		rates[4][0] = "Level 2 Help Desk";
		rates[4][1] = "Base Level Reactive Call & Problem Management (Device)";
		rates[4][2] = "Reactive management of: Telco CPE, VPN Router, Standalone AP, UPS, Analog Modem Backup, 3G Device; ";
		rates[4][3] = "8am-9pm Local Site Time, 7 days";
		rates[4][4] = "$3.80";
		rates[4][5] = "  30 minute maximum active troubleshooting and then escalated";

		rates[5][0] = "Level 2 Help Desk";
		rates[5][1] = "Base Level Pro-Active Call & Problem Management (Device)";
		rates[5][2] = "Pro-active management of: Telco CPE, VPN Router, Standalone AP, UPS, Analog Modem Backup, 3G Device; ";
		rates[5][3] = "8am-9pm Local Site Time, 7 days";
		rates[5][4] = "$3.80";
		rates[5][5] = "  30 minute maximum active troubleshooting and then escalated";

		rates[6][0] = "Level 3 Help Desk";
		rates[6][1] = "Advanced Level Reactive Call & Problem Management (Device)";
		rates[6][2] = "Reactive management of: Telco CPE, VPN Router, Standalone AP, UPS, Analog Modem Backup, 3G Device; ";
		rates[6][3] = "8am-9pm Local Site Time, 7 days";
		rates[6][4] = "$2.28";
		rates[6][5] = "  Advanced engineering support limited by degree of access to the subject device. Reimage configuration and/or firmware upgrade included; 60 minute maximum active troubleshooting and then out of scope applies";

		rates[7][0] = "Level 3 Help Desk";
		rates[7][1] = "Advanced Level Pro-active Call & Problem Management (Device)";
		rates[7][2] = "Pro-active management of: Telco CPE, VPN Router, Standalone AP, UPS, Analog Modem Backup, 3G Device; ";
		rates[7][3] = "8am-9pm Local Site Time, 7 days";
		rates[7][4] = "$2.28";
		rates[7][5] = "  Advanced engineering support limited by degree of access to the subject device. Reimage configuration and/or firmware upgrade included; 60 minute maximum active troubleshooting and then out of scope applies";
		for (int i = 0; i < 8; i++) {
			row = new Vector();
			for (int j = 0; j < 6; j++) {
				cell = new String[3];
				cell[0] = "1";
				switch (j) {
				case 4:
					cell[1] = "4";
					break;
				default:
					cell[1] = "1";
				}
				cell[2] = rates[i][j];
				row.add(cell);
			}
			table.add(row);
		}
		return table;
	}

	String buildNetworkMonitoring() {
		String table_html = "";
		String row_html = "";

		table_html = "<table width=\"800\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" class=\"TBTOP001\">\n<tr><td class=\"TBHDR002\" colspan=\"6\">Network Monitoring - Monthly Activity Based*</td></tr>\n<tr><td class=\"TBHDR002\" width=\"75\">Type</td><td class=\"TBHDR002\" width=\"120\">Activity</td><td class=\"TBHDR002\" width=\"250\">Description</td><td class=\"TBHDR002\" width=\"75\">Availability</td><td class=\"TBHDR002\" width=\"110\">Monthly Fee Per Unit</td><td class=\"TBHDR002\" width=\"280\">Additional Information</td></tr>\n";

		Vector<Vector<String[]>> table = getNetworkMonitoringRates();
		for (int i = 0; i < table.size(); i++) {
			Vector<String[]> row = (Vector) table.get(i);
			row_html = row_html + "<tr>";
			for (int j = 0; j < row.size(); j++) {
				String[] cell = (String[]) row.get(j);
				row_html = row_html + "<td class=\""
						+ this.CellStyles[java.lang.Integer.parseInt(cell[1])]
						+ "\">" + cell[2] + "</td>";
			}
			row_html = row_html + "</tr>\n";
		}
		table_html = table_html + row_html + "</table>\n";

		return table_html;
	}

	Vector<Vector<String[]>> getNetworkMonitoringRates() {
		Vector<Vector<String[]>> table = new Vector();

		String[][] rates = new String[5][6];

		rates[0][0] = "Monitoring";
		rates[0][1] = "Pro-Active ICMP Polling (Circuit)";
		rates[0][2] = "Proactive monitoring of DSL, Cable, 3G, 4G, FIOS, FW or T-1/E-1";
		rates[0][3] = "7x24";
		rates[0][4] = "$9.00";
		rates[0][5] = "  Includes ICMP monitoring of telco CPE when technically possible; Static IP address may be required";

		rates[1][0] = "Monitoring";
		rates[1][1] = "Pro-Active SNMP Polling (Circuit)";
		rates[1][2] = "Proactive monitoring of DSL, Cable, 3G, 4G, FIOS, FW or T-1/E-1; VPN tunnel";
		rates[1][3] = "7x24";
		rates[1][4] = "$11.67";
		rates[1][5] = "  Includes SNMP monitoring of telco CPE when technically possible; Static IP address may be required";

		rates[2][0] = "Monitoring";
		rates[2][1] = "Pro-Active HTTPS Polling (Circuit)";
		rates[2][2] = "Proactive monitoring of DSL, Cable, 3G, 4G, FIOS, FW or T-1/E-1";
		rates[2][3] = "7x24";
		rates[2][4] = "$11.67";
		rates[2][5] = "  Includes HTTPS monitoring of telco CPE when technically possible; Static IP address may be required";

		rates[3][0] = "Monitoring";
		rates[3][1] = "Pro-Active ICMP Polling (Device)";
		rates[3][2] = "Proactive monitoring of VPN Router, Standalone AP, UPS, Analog Modem Backup, 3G Device;";
		rates[3][3] = "7x24";
		rates[3][4] = "$9.00";
		rates[3][5] = "  Includes ICMP monitoring of listed devices when technically possible; Static IP address may be required";

		rates[4][0] = "Monitoring";
		rates[4][1] = "Pro-Active SNMP Polling (Device)";
		rates[4][2] = "Proactive monitoring of VPN Router, Standalone AP, UPS, Analog Modem Backup, 3G Device;";
		rates[4][3] = "7x24";
		rates[4][4] = "$11.67";
		rates[4][5] = "  Includes SNMP monitoring of listed devices when technically possible; Static IP address may be required";
		for (int i = 0; i < 5; i++) {
			Vector<String[]> row = new Vector();
			for (int j = 0; j < 6; j++) {
				String[] cell = new String[3];
				cell[0] = "1";
				switch (j) {
				case 3:
					cell[1] = "2";
					break;
				case 4:
					cell[1] = "4";
					break;
				default:
					cell[1] = "1";
				}
				cell[2] = rates[i][j];
				row.add(cell);
			}
			table.add(row);
		}
		return table;
	}

	String buildCallManagementRates() {
		String table_html = "";
		String row_html = "";

		table_html = "<table width=\"450\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" class=\"TBTOP001\">\n<tr><td class=\"TBCEl002\"><strong>CNS Resource</strong></td><td class=\"TBCEl002\"><strong>Price Per Minute</strong><br>PPS</td><td class=\"TBCEl002\"><strong>Price Per Minute</strong><br>Outside of PPS</td></tr>\n";

		Vector<Vector<String[]>> table = buildTableCallManagementRates();
		for (int i = 0; i < table.size(); i++) {
			Vector<String[]> row = (Vector) table.get(i);
			row_html = row_html + "<tr>";
			for (int j = 0; j < row.size(); j++) {
				String[] cell = (String[]) row.get(j);
				row_html = row_html + "<td class=\""
						+ this.CellStyles[java.lang.Integer.parseInt(cell[1])]
						+ "\">" + cell[2] + "</td>";
			}
			row_html = row_html + "</tr>\n";
		}
		table_html = table_html + row_html + "</table>\n";

		return table_html;
	}

	Vector buildTableCallManagementRates() {
		Vector<Vector<String[]>> table = new Vector();
		Vector<String[]> row = new Vector();
		String[] cell = new String[3];

		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "CCA";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "3";
		cell[2] = "$1.50/minute";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "3";
		cell[2] = "$2.48/minute";
		row.add(cell);
		table.add(row);

		row = new Vector();
		cell = new String[3];
		cell[0] = "1";
		cell[1] = "1";
		cell[2] = "TSS";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "3";
		cell[2] = "$1.50/minute";
		row.add(cell);

		cell = new String[3];
		cell[0] = "1";
		cell[1] = "3";
		cell[2] = "$2.48/minute";
		row.add(cell);
		table.add(row);

		return table;
	}

	public ActionForward netmedx_rules(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String action = request.getParameter("action");
		if (action != null) {
			if (action.equals("display")) {
				getRules(request);
			} else if (action.equals("update")) {
				updateRules(request);
				getRules(request);
			}
			request.setAttribute("section", action);
			getSectionData(request, action);
		}
		String forward_key = "netmedx_rules";
		return mapping.findForward(forward_key);
	}

	boolean getRules(HttpServletRequest request) {
		boolean rc = false;

		String lx_pr_id = request.getParameter("lx_pr_id");
		if (lx_pr_id != null) {
			request.setAttribute("lx_pr_id", lx_pr_id);
			selectRules(request, lx_pr_id);
		}
		return rc;
	}

	boolean selectRules(HttpServletRequest request, String lx_pr_id) {
		boolean rc = false;

		NumberFormat hours = new DecimalFormat("##");
		NumberFormat increment = new DecimalFormat("#0.00000000");
		NumberFormat discount = new DecimalFormat("0.00");

		Map<String, String> default_rules = new HashMap();

		boolean set_defaults = true;

		String[] fields = { "lm_dr_labor_minimum", "lm_dr_labor_increment",
				"lm_dr_increment_discount", "lm_dr_travel_minimum",
				"lm_dr_travel_maximum", "lm_dr_travel_increment" };

		int m = 0;
		default_rules.put(fields[(m++)], "2");
		default_rules.put(fields[(m++)], ".01666666667");
		default_rules.put(fields[(m++)], "0.0");
		default_rules.put(fields[(m++)], "1");
		default_rules.put(fields[(m++)], "1");
		default_rules.put(fields[(m++)], ".01666666667");

		String sql = "select * from lm_dispatch_rules where lm_dr_pr_id = "
				+ lx_pr_id;

		String request_type = "";
		String t1 = "";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				set_defaults = false;

				request_type = rs.getString("lm_dr_request_type");
				for (int j = 0; j < fields.length; j++) {
					t1 = rs.getString(fields[j]);
					if (rs.wasNull()) {
						t1 = (String) default_rules.get(fields[j]);
					}
					switch (j) {
					case 0:
						t1 = hours.format(Double.parseDouble(t1));
						break;
					case 1:
						t1 = increment.format(Double.parseDouble(t1));
						break;
					case 2:
						t1 = discount.format(Double.parseDouble(t1));
						break;
					case 3:
						t1 = hours.format(Double.parseDouble(t1));
						break;
					case 4:
						t1 = hours.format(Double.parseDouble(t1));
						break;
					case 5:
						t1 = increment.format(Double.parseDouble(t1));
					}
					request.setAttribute(fields[j] + "~" + request_type, t1);
				}
			}
		} catch (Exception localException) {
		}
		request.setAttribute("lm_dn_additional_notes", selectRules(lx_pr_id));
		if (set_defaults) {
			setDefaults(request, fields, default_rules);
		}
		return rc;
	}

	String selectRules(String lx_pr_id) {
		String sql = "select lm_dn_additional_notes from lm_dispatch_rules_notes where lm_dn_pr_id = "
				+ lx_pr_id;

		String t1 = "";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				t1 = rs.getString("lm_dn_additional_notes");
				if (rs.wasNull()) {
					t1 = "";
				}
			}
		} catch (Exception localException) {
		}
		return t1;
	}

	void setDefaults(HttpServletRequest request, String[] fields,
			Map<String, String> default_rules) {
		String[] request_id = { "1", "4", "5", "3", "2", "99" };
		for (int i = 0; i < request_id.length; i++) {
			for (int j = 0; j < fields.length; j++) {
				request.setAttribute(fields[j] + "~" + request_id[i],
						default_rules.get(fields[j]));
			}
		}
	}

	boolean updateRules(HttpServletRequest request) {
		boolean rc = false;

		String[] request_id = { "1", "4", "5", "3", "2", "99" };
		String p = "";
		boolean ok_to_process = true;

		String[] fields = { "lm_dr_labor_minimum", "lm_dr_labor_increment",
				"lm_dr_increment_discount", "lm_dr_travel_minimum",
				"lm_dr_travel_maximum", "lm_dr_travel_increment" };

		String[] v = new String[fields.length];
		for (int i = 0; i < request_id.length; i++) {
			for (int j = 0; j < fields.length; j++) {
				p = fields[j] + "~" + request_id[i];
				v[j] = request.getParameter(p);
				switch (j) {
				case 0:
					break;
				case 1:
					break;
				case 2:
					break;
				case 3:
					break;
				}
			}
			if (ok_to_process) {
				String lx_pr_id = request.getParameter("lx_pr_id");
				String lm_dn_additional_notes = request.getParameter(
						"lm_dn_additional_notes").trim();
				if (lm_dn_additional_notes == null) {
					lm_dn_additional_notes = "";
				}
				ok_to_process = execute_lm_dispatch_rules_manage_01A(
						request_id[i], lx_pr_id, v[0], v[1], v[2], v[3], v[4],
						v[5], lm_dn_additional_notes);
			}
		}
		return rc;
	}

	boolean execute_lm_dispatch_rules_manage_01A(String lm_dr_request_type,
			String lm_dr_pr_id, String lm_dr_labor_minimum,
			String lm_dr_labor_increment, String lm_dr_increment_discount,
			String lm_dr_travel_minimum, String lm_dr_travel_maximum,
			String lm_dr_travel_increment, String lm_dn_additional_notes) {
		boolean rc = false;
		int sp_rc = 0;

		String sql = "{? = call lm_dispatch_rules_manage_01A(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.registerOutParameter(1, 4);
			stmt.setObject("@lm_dr_request_type", lm_dr_request_type);
			stmt.setObject("@lm_dr_pr_id", lm_dr_pr_id);
			stmt.setObject("@lm_dr_labor_minimum", lm_dr_labor_minimum);
			stmt.setObject("@lm_dr_labor_increment", lm_dr_labor_increment);
			stmt.setObject("@lm_dr_increment_discount",
					lm_dr_increment_discount);
			stmt.setObject("@lm_dr_travel_minimum", lm_dr_travel_minimum);
			stmt.setObject("@lm_dr_travel_maximum", lm_dr_travel_maximum);
			stmt.setObject("@lm_dr_travel_increment", lm_dr_travel_increment);
			stmt.setObject("@lm_dn_additional_notes", lm_dn_additional_notes);
			stmt.execute();
			sp_rc = stmt.getInt(1);
			stmt.close();
			if (sp_rc > 0) {
				rc = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return rc;
	}

	public ActionForward netmedx_rates(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String action = request.getParameter("action");
		if (action != null) {
			if (action.equals("display")) {
				getRates(request);
			} else if (action.equals("update")) {
				updateRates(request);
				getRates(request);
			} else if (action.equals("discount")) {
				discountRates(request);
				getRates(request);
			} else if (action.equals("reset")) {
				resetRates(request);
				getRates(request);
			}
			request.setAttribute("action", action);
		}
		String forward_key = "netmedx_rates";
		return mapping.findForward(forward_key);
	}

	boolean getRates(HttpServletRequest request) {
		boolean rc = false;

		String lx_pr_id = request.getParameter("lx_pr_id");
		if (lx_pr_id == null) {
			lx_pr_id = "0";
		}
		String request_type = request.getParameter("request_type");
		if ((lx_pr_id != null) && (request_type != null)) {
			request.setAttribute("lx_pr_id", lx_pr_id);
			request.setAttribute("request_type", request_type);
			selectRates(request, lx_pr_id, request_type);

			selectDiscount(request, lx_pr_id, request_type);
		}
		return rc;
	}

	boolean selectRates(HttpServletRequest request, String lx_pr_id,
			String request_type) {
		boolean rc = false;

		boolean set_defaults = true;
		String t2 = "";

		NumberFormat dollar = new DecimalFormat("#.00");

		String[][] criticality = getCriticalities(lx_pr_id);
		request.setAttribute("criticality", criticality);

		Vector<String[]> skills = getSkills(lx_pr_id);
		Map<String, String[]> rates = new HashMap();

		String sql = "select * from cx_labor_rate where cx_lr_pr_id = "
				+ lx_pr_id + " and cx_request_type = '" + request_type + "'";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				t2 = rs.getString("cx_sk_id") + "~" + rs.getString("cx_cr_id");
				request.setAttribute(t2,
						dollar.format(rs.getDouble("cx_lr_rate")));

				set_defaults = false;
			}
		} catch (Exception localException) {
		}
		if (set_defaults) {
			setDefaultRates(request);
		}
		DispatchScenarios x = new DispatchScenarios(new DataAccess(), lx_pr_id,
				request_type);
		request.setAttribute("profitability", x.computeProfitability());

		return rc;
	}

	void selectDiscount(HttpServletRequest request, String lx_pr_id,
			String request_type) {
		String[][] c = new String[5][4];
		int i = 0;

		String sql = "select isnull(-1*lm_dr_discount, 0.0) as lm_dr_discount, isnull(lm_dr_premium,0.0) as lm_dr_premium  from dbo.lm_dispatch_rules  where lm_dr_request_type = "
				+

				request_type + " and lm_dr_pr_id = " + lx_pr_id;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				request.setAttribute("discount", rs.getString("lm_dr_discount"));
				request.setAttribute("premium", rs.getString("lm_dr_premium"));
			}
			rs.close();
		} catch (Exception localException) {
		}
	}

	void setDefaultRates(HttpServletRequest request) {
		String[] criticality = { "1", "2", "3", "4", "5" };
		String[] skills = { "CFT2", "CFT3", "CFE2", "CFE3" };
		Map<String, String> default_rates = new HashMap();

		default_rates.put("CFT2~1", "204.00");
		default_rates.put("CFT3~1", "209.00");
		default_rates.put("CFE2~1", "215.00");
		default_rates.put("CFE3~1", "245.00");

		default_rates.put("CFT2~2", "158.00");
		default_rates.put("CFT3~2", "166.00");
		default_rates.put("CFE2~2", "166.00");
		default_rates.put("CFE3~2", "200.00");

		default_rates.put("CFT2~3", "110.00");
		default_rates.put("CFT3~3", "122.00");
		default_rates.put("CFE2~3", "124.00");
		default_rates.put("CFE3~3", "179.00");

		default_rates.put("CFT2~4", "79.00");
		default_rates.put("CFT3~4", "90.00");
		default_rates.put("CFE2~4", "93.00");
		default_rates.put("CFE3~4", "145.00");

		default_rates.put("CFT2~5", "71.00");
		default_rates.put("CFT3~5", "78.00");
		default_rates.put("CFE2~5", "86.00");
		default_rates.put("CFE3~5", "133.00");

		String t2 = "";
		for (int i = 0; i < criticality.length; i++) {
			for (int j = 0; j < skills.length; j++) {
				t2 = skills[j] + "~" + criticality[i];
				request.setAttribute(t2, default_rates.get(t2));
			}
		}
	}

	String[][] getCriticalities(String lx_pr_id) {
		String[][] c = new String[5][4];
		int i = 0;
		String sql = "select * from cx_criticality order by cx_cr_id";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				c[i][0] = rs.getString("cx_cr_id");
				c[i][1] = rs.getString("cx_cr_criticality");
				c[i][2] = rs.getString("cx_cr_activity");
				c[i][3] = rs.getString("cx_cr_description");
				i++;
			}
		} catch (Exception localException) {
		}
		return c;
	}

	Vector<String[]> getSkills(String lx_pr_id) {
		Vector<String[]> skill = new Vector();
		int i = 0;
		String sql = "select * from cx_skill order by cx_sk_full_name desc, cx_sk_id";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String[] s = new String[2];
				s[0] = rs.getString("cx_sk_id");
				s[1] = rs.getString("cx_sk_full_name");
			}
		} catch (Exception localException) {
		}
		return skill;
	}

	boolean updateRates(HttpServletRequest request) {
		boolean rc = false;

		String[] request_id = { "1", "4", "5", "3", "2", "99" };
		boolean ok_to_process = true;

		String[] criticality = { "1", "2", "3", "4", "5" };
		String[] skills = { "CFT2", "CFT3", "CFE2", "CFE3" };

		String rate_identifier = "";
		String rate = "";

		String lx_pr_id = request.getParameter("lx_pr_id");
		String request_type = request.getParameter("request_type");
		if ((lx_pr_id != null) && (request_type != null)) {
			request.setAttribute("lx_pr_id", lx_pr_id);
			request.setAttribute("request_type", request_type);
			for (int i = 0; i < criticality.length; i++) {
				for (int j = 0; j < skills.length; j++) {
					rate_identifier = skills[j] + "~" + criticality[i];
					rate = request.getParameter(rate_identifier);

					execute_cx_labor_rate_manage_01A(skills[j], criticality[i],
							rate, lx_pr_id, request_type);
				}
			}
		}
		return rc;
	}

	boolean execute_cx_labor_rate_manage_01A(String cx_sk_id, String cx_cr_id,
			String cx_lr_rate, String cx_lr_pr_id, String cx_request_type) {
		boolean rc = false;
		int sp_rc = 0;

		String sql = "{? = call cx_labor_rate_manage_01A(?, ?, ?, ?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.registerOutParameter(1, 4);
			stmt.setObject("@cx_sk_id", cx_sk_id);
			stmt.setObject("@cx_cr_id", cx_cr_id);
			stmt.setObject("@cx_lr_rate", cx_lr_rate);
			stmt.setObject("@cx_lr_pr_id", cx_lr_pr_id);
			stmt.setObject("@cx_request_type", cx_request_type);
			stmt.execute();
			sp_rc = stmt.getInt(1);
			stmt.close();
			if (sp_rc > 0) {
				rc = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return rc;
	}

	boolean discountRates(HttpServletRequest request) {
		boolean rc = false;
		boolean apply_discount = false;
		boolean apply_premium = false;

		String discount = request.getParameter("discount");
		String premium = request.getParameter("premium");
		if ((discount != null) && (discount.trim().length() > 0)) {
			discount = "-" + discount.trim();
		} else {
			discount = "0.0";
		}
		if ((premium != null) && (premium.trim().length() > 0)) {
			premium = premium.trim();
		} else {
			premium = "0.0";
		}
		String lx_pr_id = request.getParameter("lx_pr_id");
		String request_type = request.getParameter("request_type");

		execute_lm_dispatch_manage_and_apply_discount_01A(request_type,
				lx_pr_id, discount, premium);

		return rc;
	}

	boolean execute_lm_dispatch_manage_and_apply_discount_01A(
			String lm_dr_request_type, String lm_dr_pr_id,
			String lm_dr_discount, String lm_dr_premium) {
		boolean rc = false;
		int sp_rc = 0;

		String sql = "{? = call lm_dispatch_manage_and_apply_discount_01A(?, ?, ?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.registerOutParameter(1, 4);
			stmt.setObject("@lm_dr_request_type", lm_dr_request_type);
			stmt.setObject("@lm_dr_pr_id", lm_dr_pr_id);
			stmt.setObject("@lm_dr_discount", lm_dr_discount);
			stmt.setObject("@lm_dr_premium", lm_dr_premium);
			stmt.execute();
			sp_rc = stmt.getInt(1);
			stmt.close();
			if (sp_rc > 0) {
				rc = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return rc;
	}

	boolean resetRates(HttpServletRequest request) {
		boolean rc = false;

		String lx_pr_id = request.getParameter("lx_pr_id");
		String request_type = request.getParameter("request_type");

		execute_cx_labor_reset_01A(request_type, lx_pr_id);

		return rc;
	}

	boolean execute_cx_labor_reset_01A(String cx_request_type,
			String cx_lr_pr_id) {
		boolean rc = false;
		int sp_rc = 0;

		String sql = "{? = call cx_labor_reset_01A(?, ?)}";
		try {
			CallableStatement stmt = DataAccess.databaseConnection
					.prepareCall(sql);
			stmt.registerOutParameter(1, 4);
			stmt.setObject("@cx_lr_pr_id", cx_lr_pr_id);
			stmt.setObject("@cx_request_type", cx_request_type);
			stmt.execute();
			sp_rc = stmt.getInt(1);
			stmt.close();
			if (sp_rc > 0) {
				rc = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return rc;
	}

	public ActionForward netmedx_appendix(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		GenerateNetMedXAppendix ap = new GenerateNetMedXAppendix(this.da,
				response);

		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition",
				" attachment; filename=\"test.pdf\"");
		response.setContentLength(ap.baos.size());
		try {
			ServletOutputStream out = response.getOutputStream();
			ap.baos.writeTo(out);
			out.flush();
		} catch (Exception localException) {
		}
		return null;
	}
}
