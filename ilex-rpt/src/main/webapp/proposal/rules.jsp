<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div>
<form action="NTX.xo?ac=netmedx_rules&action=update" method="post">
<input type="hidden" name="lx_pr_id" value="<%=request.getAttribute("lx_pr_id")%>">
<table border="1" cellspacing="0" cellpadding="0">

<tr> <td rowspan='2' width="180">Rules</td> <td colspan='5' width="440">Customer</td> <td rowspan='2' width="70">Partner</td></tr>
<tr><td class=''>Standard</td><td class=''>Copper</td><td class=''>Black</td><td class=''>Silver Dispatch</td><td class=''>Silver Hourly</td></tr>
<tr><td class='' colspan='7'>Labor</td></tr>
<tr>
  <td class=''>Onsite Minimum (Hours)</td>
  <td class=''><input type="text" name="lm_dr_labor_minimum~1" value="<%=request.getAttribute("lm_dr_labor_minimum~1")%>" size="4"></td>
  <td class=''><input type="text" name="lm_dr_labor_minimum~4" value="<%=request.getAttribute("lm_dr_labor_minimum~4")%>" size="4"></td>
  <td class=''><input type="text" name="lm_dr_labor_minimum~5" value="<%=request.getAttribute("lm_dr_labor_minimum~5")%>" size="4"></td>
  <td class=''><input type="text" name="lm_dr_labor_minimum~3" value="<%=request.getAttribute("lm_dr_labor_minimum~3")%>" size="4"></td>
  <td class=''><input type="text" name="lm_dr_labor_minimum~2" value="<%=request.getAttribute("lm_dr_labor_minimum~2")%>" size="4"></td>
<td class=''><input type="text" name="lm_dr_labor_minimum~99" value="<%=request.getAttribute("lm_dr_labor_minimum~99")%>" size="4"></td></tr>
<tr>
  <td class=''>Increment (Hours)</td>
  <td class=''><input type="text" name="lm_dr_labor_increment~1" value="<%=request.getAttribute("lm_dr_labor_increment~1")%>" size="10"></td>
  <td class=''><input type="text" name="lm_dr_labor_increment~4" value="<%=request.getAttribute("lm_dr_labor_increment~4")%>" size="10"></td>
  <td class=''><input type="text" name="lm_dr_labor_increment~5" value="<%=request.getAttribute("lm_dr_labor_increment~5")%>" size="10"></td>
  <td class=''><input type="text" name="lm_dr_labor_increment~3" value="<%=request.getAttribute("lm_dr_labor_increment~3")%>" size="10"></td>
  <td class=''><input type="text" name="lm_dr_labor_increment~2" value="<%=request.getAttribute("lm_dr_labor_increment~2")%>" size="10"></td>
<td class=''><input type="text" name="lm_dr_labor_increment~99" value="<%=request.getAttribute("lm_dr_labor_increment~99")%>" size="10"></td></tr>
<tr>
  <td class=''>Increment Discount (%)</td>
  <td class=''><input type="text" name="lm_dr_increment_discount~1" value="<%=request.getAttribute("lm_dr_increment_discount~1")%>" size="10"></td>
  <td class=''><input type="text" name="lm_dr_increment_discount~4" value="<%=request.getAttribute("lm_dr_increment_discount~4")%>" size="10"></td>
  <td class=''><input type="text" name="lm_dr_increment_discount~5" value="<%=request.getAttribute("lm_dr_increment_discount~5")%>" size="10"></td>
  <td class=''><input type="text" name="lm_dr_increment_discount~3" value="<%=request.getAttribute("lm_dr_increment_discount~3")%>" size="10"></td>
  <td class=''><input type="text" name="lm_dr_increment_discount~2" value="<%=request.getAttribute("lm_dr_increment_discount~2")%>" size="10"></td>
<td class=''><input type="text" name="lm_dr_increment_discount~99" value="<%=request.getAttribute("lm_dr_increment_discount~99")%>" size="10"></td></tr>
<tr><td class='' colspan='7'>Travel</td></tr>
<tr>
  <td class=''>Minimum (Hours)</td>
  <td class=''><input type="text" name="lm_dr_travel_minimum~1" value="<%=request.getAttribute("lm_dr_travel_minimum~1")%>" size="4"></td>
  <td class=''><input type="text" name="lm_dr_travel_minimum~4" value="<%=request.getAttribute("lm_dr_travel_minimum~4")%>" size="4"></td>
  <td class=''><input type="text" name="lm_dr_travel_minimum~5" value="<%=request.getAttribute("lm_dr_travel_minimum~5")%>" size="4"></td>
  <td class=''><input type="text" name="lm_dr_travel_minimum~3" value="<%=request.getAttribute("lm_dr_travel_minimum~3")%>" size="4"></td>
  <td class=''><input type="text" name="lm_dr_travel_minimum~2" value="<%=request.getAttribute("lm_dr_travel_minimum~2")%>" size="4"></td>
<td class=''><input type="text" name="lm_dr_travel_minimum~99" value="<%=request.getAttribute("lm_dr_travel_minimum~99")%>" size="4"></td></tr>
<tr>
  <td class=''>Maximum (Hours)</td>
  <td class=''><input type="text" name="lm_dr_travel_maximum~1" value="<%=request.getAttribute("lm_dr_travel_maximum~1")%>" size="4"></td>
  <td class=''><input type="text" name="lm_dr_travel_maximum~4" value="<%=request.getAttribute("lm_dr_travel_maximum~4")%>" size="4"></td>
  <td class=''><input type="text" name="lm_dr_travel_maximum~5" value="<%=request.getAttribute("lm_dr_travel_maximum~5")%>" size="4"></td>
  <td class=''><input type="text" name="lm_dr_travel_maximum~3" value="<%=request.getAttribute("lm_dr_travel_maximum~3")%>" size="4"></td>
  <td class=''><input type="text" name="lm_dr_travel_maximum~2" value="<%=request.getAttribute("lm_dr_travel_maximum~2")%>" size="4"></td>
<td class=''><input type="text" name="lm_dr_travel_maximum~99" value="<%=request.getAttribute("lm_dr_travel_maximum~99")%>" size="4"></td></tr>
<tr>
  <td class=''>Travel Increment (Hours)</td>
  <td class=''><input type="text" name="lm_dr_travel_increment~1" value="<%=request.getAttribute("lm_dr_travel_increment~1")%>" size="10"></td>
  <td class=''><input type="text" name="lm_dr_travel_increment~4" value="<%=request.getAttribute("lm_dr_travel_increment~4")%>" size="10"></td>
  <td class=''><input type="text" name="lm_dr_travel_increment~5" value="<%=request.getAttribute("lm_dr_travel_increment~5")%>" size="10"></td>
  <td class=''><input type="text" name="lm_dr_travel_increment~3" value="<%=request.getAttribute("lm_dr_travel_increment~3")%>" size="10"></td>
  <td class=''><input type="text" name="lm_dr_travel_increment~2" value="<%=request.getAttribute("lm_dr_travel_increment~2")%>" size="10"></td>
<td class=''><input type="text" name="lm_dr_travel_increment~99" value="<%=request.getAttribute("lm_dr_travel_increment~99")%>" size="10"></td></tr>

<tr><td class=''>Additional Rules</td><td class='' colspan='6'><textarea name='lm_dn_additional_notes' rows='10' cols='64'><%=request.getAttribute("lm_dn_additional_notes")%></textarea></td></tr>

<tr><td class='' colspan='7' align="right"><input type="submit" value="Update Rates"></td></tr>
</table>
</form>
</div>
</body>
</html>