<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style> 
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	margin: 0px 0px 0px 0px;
	padding: 0px 0px 0px 0px;
	color: #000000;
}

.PGTIT001
{
  font-size: 12px;
  font-weight: bold;
  padding: 5px 5px 5px 10px;
}

.PGBDY001 {
	margin: 10px 0px 0px 10px;
}

.PGDIV001 {
    margin: 10px 0px 0px 10px;
}

.TBTOP001 {
   border:1px solid #B2B2B2;
   background-color : #B2B2B2;
 }
 
.TBHDR001, .TBHDR002 {
  font-size: 11px;
  font-weight: bold;
  text-align: left;
  padding: 6px 6px 6px 6px;
  background-color : #ffffff;
}
.TBHDR002 {
   text-align: center;
 }
.TBCEl001, .TBCEl002, .TBCEl003, .TBCEl004, .TBCEl005 {
  padding: 4px 4px 4px 4px;
  font-size: 11px;
  font-weight: normal;
  vertical-align:text-top;
  background-color : #ffffff;
}
.TBCEl001 {
  white-space:nowrap;
}
.TBCEl002 {
   padding: 4px 4px 4px 4px;
} 
.TBCEl003 {
   text-align: center;
}
.TBCEl004 {
   font-weight: bold;
}
.TBCEl005 {
   text-align: right;
   padding: 4px 8px 4px 4px;
}
</style>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>

</head>
<body>
<div class="PGTIT001">Table: <%=request.getAttribute("table_name")%></div>
<div class="PGDIV001">
<%=request.getAttribute("table")%>
</div>
</body>
</html>