<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import = "java.util.Vector" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	margin: 0px 0px 0px 0px;
	padding: 0px 0px 0px 0px;
	color: #000000;
}

.PGBDY001 {
	margin: 10px 0px 0px 10px;
}

.PGDIV001 {
    margin: 10px 0px 0px 10px;
}

.PGTIT001
{
    font-size: 12px;
	font-weight: bold;
	padding: 5px 5px 5px 10px;
}

.PGCNT001 {
	font-weight: bold;
	padding: 5px 5px 5px 10px;
}

.PGCNT002 {
	padding: 5px 5px 5px 10px;
}

.PGCNT003 {
	text-decoration: underline;
	padding: 5px 5px 5px 10px;
}

.PGCNT004 {
	text-decoration: underline;
	padding: 5px 5px 5px 10px;
}
.PUWIN001 {
  margin : 5px;
  border:3px solid #B2B2B2;
  width : 564px;
  position: absolute;
  display: none;
  left: 0px;
  top: 0px;
  
  z-index:9999;
}
.PUTTL001 {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 16px; font-weight: bold; color: #2E3D80;
  background-color : #FFFFC4;
  border-top : 1px solid #B2B2B2;
  border-bottom : 1px solid #B2B2B2;
  padding : 4px;
}

.PUCNT001 {
  margin : 5px;
  background-color : #ffffff;
  width : 100%;
  height : 100%;
}

<!-- Table Formats -->


<!-- Cell Formats -->

.TBHDR001 {
  padding: 3px 3px 3px 3px;
  font-size: 11px;
  font-weight: bold;
  text-align: center;
}
.TBCEl001, .TBCEl002, .TBCEl003 {
  padding: 2px 2px 2px 2px;
  font-size: 11px;
  font-weight: normal;
}
.TBCEl001 {
  white-space:nowrap;
}
.TBCEl002 {
} 
.TBCEl003 {
}

</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript"></script>
<script src="http://jquery-ui.googlecode.com/svn/tags/latest/external/jquery.bgiframe-2.1.2.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/i18n/jquery-ui-i18n.min.js" type="text/javascript"></script>

<script type="text/javascript">

$(document).ready(function() {
	
	$('.SH').click(function(evt) {
		popup = $(this).attr('element');
		popup = '[id='+popup+"]";
		popupForm(popup, evt)
	})
	
	$('.ST').click(function(evt) {
		popup = $(this).attr('element');
		popup = '[id='+popup+"]";
		popupForm(popup)
	})
	
	$('.SL').click(function(evt) {
		popup = $(this).attr('element');
		popup = '[id='+popup+"]";
		popupForm(popup)
	})
	
	$('.SP').click(function(evt) {
		popup = $(this).attr('element');
		popup = '[id='+popup+"]";
		popupForm(popup)
	})
	
	$('.close-form').click(function() {
		popup = $(this).attr('win');
		popup = '[id='+popup+"]";
		$(popup).fadeOut(); 
		$('#mask').fadeTo("slow",1.0); 
	})
})

function popupForm (popup, evt) {
	
	$('.PUWIN001').fadeOut(); 
	
	$(popup).css('top',  evt.pageY-50);
	$(popup).css('left', evt.pageX+25);
	
	//Get the screen height and width
	var maskHeight = $(document).height()-5;
	var maskWidth = $(window).width()-2;

	//Set heigth and width to mask to fill up the whole screen
	$('#mask').css({'width':maskWidth,'height':maskHeight});
		
	//transition effect		
	$('#mask').fadeTo("slow",0.25);

	$(popup).fadeIn(); 
	$(popup).draggable();
	
}
</script>

</head>
<body>
<div id="mask">

<div class="PGTIT001">Section <%=request.getAttribute("section")%></div>


<!-- Content containment structure -->
<table width="572" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		<%
			String[] el;
            int rows = 0;
            String title = "";
            
			Vector str = (Vector) request.getAttribute("structure");

			if (str != null && str.size() > 0) {

				for (int i = 0; i < str.size(); i++) {
					el = (String[]) str.get(i);
					
                    rows = (el[4].length()/64)+2;
					// Build section based on the elment type 
					if (el[1].equals("ST")) {
		                title = "Section";
		%>
        <!-- Section -->
		<div class="PGDIV001" id="F-<%=el[0]%>">
		<table border="0" cellpadding="0" cellspacing="0">
			<!-- Top Level Section -->
			<tr>
				<td width="30"  class="PGCNT001"><%=el[2]%></td>
				<td width="520" class="PGCNT001"><%=el[3]%></td>
				<td width="22"><img src="proposal/edit4.png" class="<%=el[1]%>" element="<%=el[0]%>"></td>
			</tr>
			<tr>
				<td width="550" colspan="2" class="PGCNT002"><%=el[4]%></td>
				<td width="22">&nbsp;</td>
			</tr>
		</table>
		</div>

		<%
			} // E-O-if (el[1].equals("ST")) {

					if (el[1].equals("SH")) {
						title = "Section";
		%>
        <!-- SubSection SH -->
		
		<div class="PGDIV001" id="F-<%=el[0]%>">
		<table border="0" cellpadding="0" cellspacing="0">
			<!-- Top Level Section -->
			<tr>
				<td width="30"  class="PGCNT002"><%=el[2]%></td>
				<td width="520" class="PGCNT003"><%=el[3]%></td>
				<td width="22"><img src="proposal/edit4.png" class="<%=el[1]%>" element="<%=el[0]%>"></td>
			</tr>
			<tr>
				<td width="550" colspan="2" class="PGCNT002"><%=el[4]%></td>
				<td width="22" valign="bottom">&nbsp;</td>
			</tr>
		</table>
		</div>
		
        <!-- Top Level Section -->
        
		<div class="PUWIN001" id="<%=el[0]%>" style="width: 560px; background-color: #ffffff;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="PUTTL001" id="chart-title">Update <%=title%></td>
				<td class="PUTTL001" align="right"><img src="proposal/delete.gif" class="close-form" win="<%=el[0]%>"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
				<form action="NTX.xo" method="post">
				<input type="hidden" name="cx_st_id" value="<%=el[0]%>">
				<input type="hidden" name="section" value="<%=request.getAttribute("section")%>">
				<input type="hidden" name="ac" value="netmedx_proposal_update">
				<table border="0" cellpadding="4" cellspacing="0">
					<tr>
						<td width="30" class="PGCNT002"><input type="text" name="cx_st_title" size="4" value="<%=el[2]%>" DISABLED></td>
						<td width="520" class="PGCNT003"><input type="text" name="cx_st_title" size="75" value="<%=el[3]%>"></td>
					</tr>
					<tr>
						<td width="550" colspan="2" class="PGCNT002"><textarea name="cx_st_content" rows="<%=rows%>" cols="65"><%=el[4]%></textarea></td>
					</tr>
					<tr>
						<td width="550" colspan="2" class="PGCNT002" align="right"><input type="submit" value="Update"></td>
					</tr>
				</table>
				</form>
				</td>
			</tr>
		</table>
		</div>
		
		<%
			} // E-O-if (el[1].equals("SH")) {

					if (el[1].equals("SL")) {
		%>

	   <!-- List Item -->
		
		<div class="PGDIV001" id="<%=el[0]%>">
		<table border="0" cellpadding="0" cellspacing="0">
			<!-- Top Level Section -->
			<tr>
				<td width="550" colspan="2" class="PGCNT002">&diams;  <%=el[4]%></td>
				<td width="22" valign="top"><img src="proposal/edit4.png" class="<%=el[1]%>" element="<%=el[0]%>"></td>
			</tr>
		</table>
		</div>
		
		<%
					} // E-O-if (el[1].equals("SH")) {

							if (el[1].equals("SP")) {
				%>

        <!-- List Item -->
        
		<div class="PGDIV001" id="<%=el[0]%>">
		<table border="0" cellpadding="0" cellspacing="0">
			<!-- Top Level Section -->
			<tr>
				<td width="550" colspan="2" class="PGCNT002"><%=el[4]%></td>
				<td width="22" valign="top"><img src="proposal/edit4.png" class="<%=el[1]%>" element="<%=el[0]%>"></td>
			</tr>
		</table>
		</div>

		<%
			} // E-O-if (el[1].equals("SH")) {
		%> <%
 	} // E-O-for (int i=0; i<str.size(); i++) {
 	} // E-O-if (str != null and str.size() > 0) {
 %>
		</td>
	</tr>
</table>
</div>
</body>
</html>