<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import = "java.util.Vector" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%
String[][] criticality = (String[][])request.getAttribute("criticality");
int rt = Integer.parseInt((String)request.getAttribute("request_type"))-1;
String[] rt_names = {"Standard","Silver Dispatch","Silver Hourly","Copper","Black"};
%>
<title>Insert title here</title>
</head>
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px; 
    font-weight: normal; 
	margin: 0px 0px 0px 0px;
	padding: 0px 0px 0px 0px;
	color: #000000;
}
td {
   vertical-align:text-top;
}
.SCHDR01 {
  font-size: 16px; font-weight: bold; color: #2E3D80;
  padding: 10px 10px 5px 5px;
}

/* Table header formatting */
.TBHDR001, .TBHDR001A, .TBHDR001B {
font-weight : bold;
vertical-align: middle;
text-align: center;
padding: 3px 6px 3px 6px;
background-color: #d7e1eb;
}
.TBHDR001A {
background-color: #f9f9f9;
}
.TBHDR001B {
background-color: #ececec;
}
/* Cell Formatting */
.CX01 {
padding:3px;
border-bottom-style: solid;
border-color: #ececec;
border-bottom-width: 1px;
}
.CX02, .CX02A, .CX02B,.CX02R, .CX02AR, .CX02BR {
text-align:right;
vertical-align:bottom;
padding:4px 4px 3px 8px;
background-color: #f9f9f9;
}
.CX02B {
background-color: #ececec;
}
.CX02R, .CX02AR, .CX02BR {
background-color: #ffb2b2;
}
/* Form element formatting */
.FX02 {
text-align:right;
}

.FMBUT0001 {
padding: 3px;
font-size : 12px;
font-style : normal;
font-weight : normal;
color : #000000;
letter-spacing : normal;
word-spacing : normal;
border : 1px #333333 solid;
background : #d7e1eb;
}

</style>
<body>
<div style="padding:10px 0px 10px 10px">
<div class="SCHDR01" style="padding:10px 0px 10px 0px"><%=rt_names[rt]%> Rates</div>
<form action="NTX.xo?ac=netmedx_rates&action=discount" method="post">
<input type="hidden" name="lx_pr_id" value="<%=request.getAttribute("lx_pr_id")%>">
<input type="hidden" name="request_type" value="<%=request.getAttribute("request_type")%>">
<table border="0" cellspacing="0" cellpadding="0" width="260">
<tr>
  <td colspan='2' ><div style="padding:5px 0px 5px 0px">Discount:&nbsp;&nbsp;<input type="text" name="discount" value="<%=request.getAttribute("discount")%>" size="3"></div></td>
</tr>
<tr>
  <td><div style="padding:5px 0px 10px 0px">Premium:&nbsp;&nbsp;<input type="text" name="premium" value="<%=request.getAttribute("premium")%>" size="3"></div></td>
  <td align="right"><span><input class="FMBUT0001" type="submit" value="Apply Discount/Premium"></span></td>
</tr>
</table>
</form>

<table border="0" cellspacing="0" cellpadding="0"><tr><td>
<form action="NTX.xo?ac=netmedx_rates&action=update" method="post">
<input type="hidden" name="lx_pr_id" value="<%=request.getAttribute("lx_pr_id")%>">
<input type="hidden" name="request_type" value="<%=request.getAttribute("request_type")%>">

<table border="0" cellspacing="0" cellpadding="0">

<tr>
  <td class="TBHDR001" rowspan='2' width="240">Criticality</td>
  <td class="TBHDR001" rowspan='2' width="300">Description</td>
  <td class="TBHDR001" colspan='4' width="240">Skills and Rates</td>
</tr>

<tr>
  <td class="TBHDR001A">CFT2</td> 
  <td class="TBHDR001B">CFT3</td>
  <td class="TBHDR001A">CFE2</td> 
  <td class="TBHDR001B">CFE3</td>
</tr>


<tr>
  <td class="CX01"><strong><%=criticality[0][1]%></strong><br><%=criticality[0][2]%></td>
  <td class="CX01"><%=criticality[0][3]%></td>
  <td class="CX02A"><input type="text" name="CFT2~1" value="<%=request.getAttribute("CFT2~1")%>" size="4" class="FX02"></td>
  <td class="CX02B"><input type="text" name="CFT3~1" value="<%=request.getAttribute("CFT3~1")%>" size="4" class="FX02"></td>
  <td class="CX02A"><input type="text" name="CFE2~1" value="<%=request.getAttribute("CFE2~1")%>" size="4" class="FX02"></td>
  <td class="CX02B"><input type="text" name="CFE3~1" value="<%=request.getAttribute("CFE3~1")%>" size="4" class="FX02"></td>
</tr>
<tr>
  <td class="CX01"><strong><%=criticality[1][1]%></strong><br><%=criticality[1][2]%></td>
  <td class="CX01"><%=criticality[1][3]%></td>
  <td class="CX02A"><input type="text" name="CFT2~2" value="<%=request.getAttribute("CFT2~2")%>" size="4" class="FX02"></td>
  <td class="CX02B"><input type="text" name="CFT3~2" value="<%=request.getAttribute("CFT3~2")%>" size="4" class="FX02"></td>
  <td class="CX02A"><input type="text" name="CFE2~2" value="<%=request.getAttribute("CFE2~2")%>" size="4" class="FX02"></td>
  <td class="CX02B"><input type="text" name="CFE3~2" value="<%=request.getAttribute("CFE3~2")%>" size="4" class="FX02"></td>
</tr>
<tr>
  <td class="CX01"><strong><%=criticality[2][1]%></strong><br><%=criticality[2][2]%></td>
  <td class="CX01"><%=criticality[2][3]%></td>
  <td class="CX02A"><input type="text" name="CFT2~3" value="<%=request.getAttribute("CFT2~3")%>" size="4" class="FX02"></td>
  <td class="CX02B"><input type="text" name="CFT3~3" value="<%=request.getAttribute("CFT3~3")%>" size="4" class="FX02"></td>
  <td class="CX02A"><input type="text" name="CFE2~3" value="<%=request.getAttribute("CFE2~3")%>" size="4" class="FX02"></td>
  <td class="CX02B"><input type="text" name="CFE3~3" value="<%=request.getAttribute("CFE3~3")%>" size="4" class="FX02"></td>
</tr>
<tr>
  <td class="CX01"><strong><%=criticality[3][1]%></strong><br><%=criticality[3][2]%></td>
  <td class="CX01"><%=criticality[3][3]%></td>
  <td class="CX02A"><input type="text" name="CFT2~4" value="<%=request.getAttribute("CFT2~4")%>" size="4" class="FX02"></td>
  <td class="CX02B"><input type="text" name="CFT3~4" value="<%=request.getAttribute("CFT3~4")%>" size="4" class="FX02"></td>
  <td class="CX02A"><input type="text" name="CFE2~4" value="<%=request.getAttribute("CFE2~4")%>" size="4" class="FX02"></td>
  <td class="CX02B"><input type="text" name="CFE3~4" value="<%=request.getAttribute("CFE3~4")%>" size="4" class="FX02"></td>
</tr>
<tr>
  <td class="CX01"><strong><%=criticality[4][1]%></strong><br><%=criticality[4][2]%></td>
  <td class="CX01"><%=criticality[4][3]%></td>
  <td class="CX02A"><input type="text" name="CFT2~5" value="<%=request.getAttribute("CFT2~5")%>" size="4" class="FX02"></td>
  <td class="CX02B"><input type="text" name="CFT3~5" value="<%=request.getAttribute("CFT3~5")%>" size="4" class="FX02"></td>
  <td class="CX02A"><input type="text" name="CFE2~5" value="<%=request.getAttribute("CFE2~5")%>" size="4" class="FX02"></td>
  <td class="CX02B"><input type="text" name="CFE3~5" value="<%=request.getAttribute("CFE3~5")%>" size="4" class="FX02"></td>
</tr>

<tr><td colspan='8'>&nbsp;</td></tr>

<tr><td colspan='8' align="right"><input class="FMBUT0001" style="width:140px" type="submit" value="Update Rates"></td></tr>
</table>
</form>
</td></tr>
<tr><td>
<div style="padding:20px 0px 0px 0px">
<form action="NTX.xo?ac=netmedx_rates&action=reset" method="post">
<input type="hidden" name="lx_pr_id" value="<%=request.getAttribute("lx_pr_id")%>">
<input type="hidden" name="request_type" value="<%=request.getAttribute("request_type")%>">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr><td align="right"><input class="FMBUT0001" style="width:140px" type="submit" value="Reset Rates to Default"></td></tr>
</table>
</form></div></td></tr>
</table>
</div>

<div style="padding:0px 0px 10px 10px">
<div class="SCHDR01" style="padding:20px 0px 10px 0px">Profitability by Criticality/Skill and Purchase Order Type</div>
<table border="0" cellspacing="1" cellpadding="5">
<tr>
  <td rowspan='3' width="145" class="TBHDR001">Criticality</td>
  <td colspan='16' class="TBHDR001">Revenue and VGP by Skill by PO Type</td>
</tr>
<tr>
  <td colspan='4' class="TBHDR001A">CFT2</td>
  <td colspan='4' class="TBHDR001B">CFT3</td>
  <td colspan='4' class="TBHDR001A">CFE2</td>
  <td colspan='4' class="TBHDR001B">CFE3</td>
</tr>
<tr>
  <td class="TBHDR001A">MM</td>
  <td class="TBHDR001A">SP</td>
  <td class="TBHDR001A">PVS</td>
  <td class="TBHDR001A">Revenue</td>
  <td class="TBHDR001B">MM</td>
  <td class="TBHDR001B">SP</td>
  <td class="TBHDR001B">PVS</td>
  <td class="TBHDR001B">Revenue</td>
  <td class="TBHDR001A">MM</td>
  <td class="TBHDR001A">SP</td>
  <td class="TBHDR001A">PVS</td>
  <td class="TBHDR001A">Revenue</td>
  <td class="TBHDR001B">MM</td>
  <td class="TBHDR001B">SP</td>
  <td class="TBHDR001B">PVS</td>
  <td class="TBHDR001B">Revenue</td>
</tr>
<%
Vector rows = (Vector)request.getAttribute("profitability");
String[] row;
String style = "";
if (rows != null && rows.size() > 0) {
	for (int i=0; i<rows.size(); i++) {
		row = (String[]) rows.get(i);
%>
 <tr>
<%
		for (int j=0; j< row.length; j++) {
			if ((j>0 && j<5) || (j>8 && j<13)) {
				style = "CX02A";
			} else if ((j>4 && j<9) || (j>12)) {
				style = "CX02B";
			} else {
				style = "CX01";
			}
			
			if (j>0 && Double.parseDouble(row[j]) < 0.55) {
				style += "R";
			}
%>
    <td class="<%=style%>"><%=row[j]%></td>
<%
		}
%>
 </tr>
<%
	}
}
%>
</table>
<div style="padding:5px 0px 10px 00px">* Based on request type dispatch minimums.</div>
</div>

<div style="padding:10px 0px 10px 10px">
<div style="padding:10px 0px 10px 00px">Profitability by General Dispatch Scenario</div>
<form action="" id="profitability" >
<input type="hidden" name="lx_pr_id" value="<%=request.getAttribute("lx_pr_id")%>">

<table border="1" cellspacing="0" cellpadding="5">
<tr>
  <td class=''>Criticality:&nbsp;&nbsp;<select><option>-- Select Criticality --</option></select></td>
  <td class=''>Resource:&nbsp;&nbsp;<select><option>-- Select Resource --</option></select></td>
</tr>

<tr>
  <td class='' colspan="2">
  <div>Uplifts: <input type="checkbox"/>Non-PPS <input type="checkbox"/>Locality <input type="checkbox"/>Union</div>
  </td>
</tr>

<tr>
  <td class=''>
  <div>Expected Dispatches/Month: <input type="text"/></div>
  </td>
  <td class=''>
  <div>Site Status: Not Uploaded or N Uploaded</div>
  </td>
</tr>

<tr>
  <td class='' colspan="3" id="analysis">
  
  <table border="1" cellspacing="0" cellpadding="5">
   <tr>
     <td class=''>Purchase Type</td>
     <td class=''>Ticket Minimum</td>
     <td class=''>Pro Forma Cost (Hr)</td>
     <td class=''>Applied Rate (Hr)</td>
     <td class=''>Estimated Monthly Cost</td>
     <td class=''>Estimated Monthly Revenue</td>
     <td class=''>Pro Forma VGP</td>
   </tr>
  </table>
  
  </td>
</tr>

</table>
</form>
</div>



<div style="padding:20px 0px 0px 10px">
<div style="padding:10px 0px 20px 0px">Profitability by Specific Dispatch Scenario</div>
<div style="padding:5px 0px 5px 0px">Time on Site</div>
 <table border="1" cellspacing="0" cellpadding="5">
   <tr>
     <td class=''>Onsite</td>
     <td class=''>Offsite</td>
   </tr>
   <tr>
     <td class=''>
     <table border="1" cellspacing="0" cellpadding="0">
      <tr>
       <td class=''>AM</td>
       <td class=''>12</td>
       <td class=''>01</td>
       <td class=''>02</td>
       <td class=''>03</td>
       <td class=''>04</td>
       <td class=''>05</td>
       <td class=''>06</td>
       <td class=''>07</td>
       <td class=''>08</td>
       <td class=''>09</td>
       <td class=''>10</td>
       <td class=''>11</td>
      </tr>
      <tr>
       <td class=''>PM</td>
       <td class=''>12</td>
       <td class=''>01</td>
       <td class=''>02</td>
       <td class=''>03</td>
       <td class=''>04</td>
       <td class=''>05</td>
       <td class=''>06</td>
       <td class=''>07</td>
       <td class=''>08</td>
       <td class=''>09</td>
       <td class=''>10</td>
       <td class=''>11</td>
      </tr>
     </table>
     </td>
     <td class=''>
     <table border="1" cellspacing="0" cellpadding="0">
      <tr>
       <td class=''>AM</td>
       <td class=''>12</td>
       <td class=''>01</td>
       <td class=''>02</td>
       <td class=''>03</td>
       <td class=''>04</td>
       <td class=''>05</td>
       <td class=''>06</td>
       <td class=''>07</td>
       <td class=''>08</td>
       <td class=''>09</td>
       <td class=''>10</td>
       <td class=''>11</td>
      </tr>
      <tr>
       <td class=''>PM</td>
       <td class=''>12</td>
       <td class=''>01</td>
       <td class=''>02</td>
       <td class=''>03</td>
       <td class=''>04</td>
       <td class=''>05</td>
       <td class=''>06</td>
       <td class=''>07</td>
       <td class=''>08</td>
       <td class=''>09</td>
       <td class=''>10</td>
       <td class=''>11</td>
      </tr>
     </table>     
     </td>
   </tr>
   <tr>
     <td class=''>Minutes</td>
     <td class=''>Minutes</td>
   </tr>
   
  </table>
<div style="padding:15px 0px 5px 0px">Dispatch Parameters</div>
<table border="1" cellspacing="0" cellpadding="5">
	<tr>
		<td class=''>Criticality:&nbsp;&nbsp;<select>
			<option>-- Select Criticality --</option>
		</select></td>
		<td class=''>Resource:&nbsp;&nbsp;<select>
			<option>-- Select Resource --</option>
		</select></td>
	</tr>

	<tr>
	    <td class=''>Travel: <input type="text" name="CFE3~5" value="1" size="4"></td>
		<td class=''>
		<div>Uplifts: <input type="checkbox" />Non-PPS <input type="checkbox" />Locality <input type="checkbox" />Union</div>
		</td>
	</tr>
</table>

<div style="padding:15px 0px 5px 0px">Estimated Ticket Profitability</div>
<div style="padding:0px 0px 0px 20px">
<div style="padding:5px 0px 5px 0px">Total Time Onsite</div>
<div style="padding:5px 0px 5px 0px">Billable Hours</div>
<div style="padding:5px 0px 10px 0px">Estimated Revenue</div>

<table border="1" cellspacing="0" cellpadding="5">
	<tr>
		<td class=''>Purchase Type</td>
		<td class=''>Authorized Labor</td>
		<td class=''>Authorized Travel</td>
		<td class=''>Estimated Cost</td>
		<td class=''>Pro Forma VGP</td>
	</tr>
	<tr>
		<td class=''>Minuteman</td>
		<td class=''>0.0</td>
		<td class=''>0.0</td>
		<td class=''>0.0</td>
		<td class=''>0.0</td>
	</tr>
	<tr>
		<td class=''>Speedpay</td>
		<td class=''>0.0</td>
		<td class=''>0.0</td>
		<td class=''>0.0</td>
		<td class=''>0.0</td>
	</tr>
	<tr>
		<td class=''>Standard Partner</td>
		<td class=''>0.0</td>
		<td class=''>0.0</td>
		<td class=''>0.0</td>
		<td class=''>0.0</td>
	</tr>
</table>
</div>
</div>
</body>
</html>