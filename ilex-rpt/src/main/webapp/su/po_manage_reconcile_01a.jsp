n<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="../js/popcalendar.js" %>
<%
String po = request.getParameter("po");
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PO Search Function</title>
<style>
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .rptBody02 {
		 padding : 10px 0px 0px 20px;
    }
    
    .title {
		font-size : 12pt;
		font-weight : bold;
		text-align : left;
		vertical-align : bottom;
		padding : 0px 0px 6px 0px;
    } 
    .subTitle {
		font-size : 11pt;
		font-weight : bold;
		text-align : left;
		vertical-align : bottom;
		padding : 0px 0px 6px 0px;
    }

    .label1, .label2, .label3 {
        font-size : 9pt;
		padding : 0px 4px 6px 0px;
		vertical-align :5 top;
    }
    .label1 {
		font-weight : bold;
    }
    .label2 {
		font-weight : normal;
    }
    .label3 {
		font-weight : normal;
		padding : 0px 10px 6px 0px;
    }
    .text1, .date1, .textarea1 {
		font-size : 9pt;
		font-weight : normal;
		padding : 0px 0px 0px 2px;
    }    
    
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		text-align : center;
        height: 22px;
    }  
</style>
</head>

<body>
<div class="rptBody01">
<table width="800" border="0" cellspacing="0" cellpadding="0">

<tr>
 <td class="title">Partner Invoice - Process New Invoice</td>
</tr>
 
<tr>
<td>
<div class="rptBody02">
<table border="0" cellspacing="0" cellpadding="0">
<form action="192.168.20.105/rpt/su/po_manage_reconcile_01a.jsp?po=X" method=get">
 <tr>
   <td class="label2">PO Number:</td>
   <td class="label2"><el:element type="text1" request="<%=request%>" name="account" source="" data ="" js=""/></td>
   <td class="label2">Invoice Number:</td>
   <td class="label2"><el:element type="text1" request="<%=request%>" name="account" source="" data ="" js=""/></td>
   <td class="label2">Invoice Date:</td>
   <td class="label2"><el:element type="date1" request="<%=request%>" name="date_field" source="" data ="" js=""/></td>
  </tr>
 
 <tr>
   <td class="label1" colspan="6">Amount</td>
 </tr>
 
 <tr>
   <td>&nbsp;</td>
   <td colspan="5">
   <table border="0" cellspacing="0" cellpadding="0">
   <tr>
     <td class="label2">Labor:</td>
     <td class="label3"><el:element type="text1" request="<%=request%>" name="account" source="" data ="" js=""/></td>
   </tr>
    
   <tr>
     <td class="label2">Travel:</td>
     <td class="label3"><el:element type="text1" request="<%=request%>" name="date_field" source="" data ="" js=""/></td>
   </tr>
   
   <tr>
     <td class="label2">Materials:</td>
     <td class="label3"><el:element type="text1" request="<%=request%>" name="account" source="" data ="" js=""/></td>
   </tr>
 
   <tr>
     <td class="label2">Tax:</td>
     <td class="label3"><el:element type="text1" request="<%=request%>" name="date_field" source="" data ="" js=""/></td>
   </tr>
   
   <tr>
     <td class="label1" colspan="1">Total:</td>
     <td class="label2" colspan="1"><el:element type="text1" request="<%=request%>" name="account" source="" data ="" js=""/></td>
   </tr>
   </table>
   </td>
 </tr>
 
 <tr>
   <td class="label2" style="vertical-align : top;" colspan="1">Comments:</td>
   <td class="label2" colspan="5"><el:element type="textarea1" request="<%=request%>" name="account" source="" data ="" js=""/></td>
 </tr>

<tr>
   <td class="label2" colspan="6" align="right"><input type="submit" value="Submit Invoice" class="button"></td> 
</tr>

</form>
</table>
</div>
</td>
</tr>
<%if (po != null) {%>
<tr>
 <td class="subTitle">PO Details:</td>
</tr>

<tr>
<td>
<div class="rptBody02">
<table width="100%" border="0" cellspacing="0" cellpadding="0">

 <tr>
   <td class="label2">Partner:</td>
   <td class="label2">Delivered</td>
   <td class="label2">Authorized:</td>
   <td class="label2">Type:</td>
   <td class="label2">Terms:</td>
 </tr>
 
 <tr>
  <td class="label2" colspan="1">PO Notes:</td>
  <td class="label2" colspan="4">The notes ....</td>
 </tr>
 
 <tr>
  <td class="label2" colspan="2">Primary Partner Contact:</td>
  <td class="label2">Phone:</td>
  <td class="label2" colspan="2">Email:</td>
 </tr>
 
</table>
</div>
</td>
</tr>

<tr>
 <td class="subTitle">Job Details:</td>
</tr>

<tr>
<td>
<div class="rptBody02">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr>
   <td class="label2">Account:</td>
   <td class="label2">Project:</td>
   <td class="label2">PM:</td>
   <td class="label2">Owner:</td>
 </tr>
 <tr><td class="label2" colspan="4">Job:</td></tr>
 <tr>
  <td class="label2">Status:</td>
  <td class="label2" colspan="3">Date:</td>
 </tr>
</table>
<div class="rptBody02">
</td>
</tr>

<!--

<tr>
 <td class="subTitle">Recommended Action:</td>
</tr>
 
<tr>
<td>
<div class="rptBody02">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr>
   <td class="label2">Initate Payment - check to be mailed: <el:element type="date1" request="<%=request%>" name="account" source="" data ="" js=""/>&nbsp;<input type="submit" value="Go" class="button"></td>
 </tr>
</table>
<div class="rptBody02">
</td>
</tr>
 -->
 
<%}%>
</table>

</div> 
</body>
</html>