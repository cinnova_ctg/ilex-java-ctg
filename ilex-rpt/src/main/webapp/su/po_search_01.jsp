<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%

Vector rows = (Vector)request.getAttribute("rows");
int count = 0;
if (rows != null) {
	count = rows.size();
}

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PO Search Function</title>
<style>
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 12px;
    	cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .rptBody02 {
		 padding : 10px 0px 0px 20px;
    }
    .titleLine {
		font-size : 12pt;
		font-weight : bold;
		text-align : left;
		vertical-align : bottom;
		padding : 0px 0px 6px 0px;
    } 
    .sectionTitleLine {
		font-size : 10pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 0px 0px 6px 0px;
    }
    .sectionTitleLineNav {
		font-size : 8pt;
		font-weight : normal;
		text-align : right;
		vertical-align : middle;
		padding : 0px 0px 0px 0px;
    }
    .label1, .label2 {
        font-size : 9pt;
		font-weight : bold;
		vertical-align : top;
		padding : 10px 10px 6px 0px;
    }
    .form1 {
        font-size : 10pt;
		font-weight : normal;
		padding : 0px 0px 0px 0px;
    }
    .elementContainer {
    padding : 0px 2px 0px 2px;
    }
    .columnHeader1 {
		font-size : 9pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 3px 10px 3px 10px;
		background-color: #E0E0DC;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 8pt;
		font-weight : normal;
		text-align : left;
		vertical-align : middle;
		padding : 1px 4px 1px 4px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #e5f9de;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ffffff;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }        
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
		text-align : center;
		width: 26px;
        height: 24px;
    }  
</style>
</head>
<body>
<div class="rptBody01">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr><td class="titleLine">PO Search</td></tr>
 </table>
 </td>
</tr>
 
<tr>
<td>
 <form method="post" action="/rpt/POSch.xo">
 <table border="0" cellspacing="0" cellpadding="0">
 <tr>
 <td class="label1">Enter Purchase/Work Order Number: </td>
 <td class="elementContainer"><input type="text" size="40" name="po_list" class="form1">&nbsp;<input type="submit" value="Go" class=button></td>
 </tr>
 </table>
 <input type="hidden" name="form_action" value="poSearch">
</form>
</td>
</tr>

<tr>
<td>
<br/>
 <table border="0" cellspacing="1" cellpadding="0">
 <tr>
     <td class="columnHeader1">PO #</td>
     <td class="columnHeader1">Account</td>
     <td class="columnHeader1">Project</td>
     <td class="columnHeader1">Job</td>
     <td class="columnHeader1">Project Manager</td>
     <td class="columnHeader1">Owner</td>
     <td class="columnHeader1">CI</td>
     <td class="columnHeader1">Authorized Cost</td>
     <td class="columnHeader1">Approved Cost</td> 
 </tr>
 
     <% 
     if (rows != null) {
       for (int i=0; i<rows.size(); i++) {
    	   String[] r = (String[])rows.get(i);
    	   if (r[0].equals("G")){
    		   // build link based off of current dashboard design
    		   //String href = "<a href=\"http://ilex.contingent.local/Ilex/JobDashboardAction.do?function=view&jobid="+r[10]+"&appendixid="+r[11]+"\">"+r[4]+"</a>";
    		   String href = "<a href=\"http://ilex.contingent.local/Ilex/JobDashboardAction.do?appendix_Id="+r[11]+"&Job_Id="+r[10]+"\">"+r[4]+"</a>";
     %>
     <tr>
     <td class="cDataLeft<%=i%2%>"><%=r[1]%></td>
     <td class="cDataLeft<%=i%2%>"><%=r[2]%></td>
     <td class="cDataLeft<%=i%2%>"><%=r[3]%></td>
     <td class="cDataCenter<%=i%2%>"><%=href%></td>
     <td class="cDataCenter<%=i%2%>"><%=r[5]%></td>
     <td class="cDataRight<%=i%2%>"><%=r[6]%></td>
     <td class="cDataRight<%=i%2%>"><%=r[7]%></td>
     <td class="cDataRight<%=i%2%>"><%=r[8]%></td>
     <td class="cDataRight<%=i%2%>"><%=r[9]%></td>
     </tr>
     <%
    	   }
        }
     }
     
     %>     
     
 </table>
</td>
</tr>
</table>

</div> 
</body>
</html>