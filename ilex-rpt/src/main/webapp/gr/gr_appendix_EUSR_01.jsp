<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%

Vector rows = (Vector)request.getAttribute("rows");
int count = 0;
if (rows != null) {
	count = rows.size();
}

Vector paraList = (Vector)request.getAttribute("paraList");
Map parameter = (Map)request.getAttribute("parameter");

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PO Search Function</title>
<style>
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 12px;
    	cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .rptBody02 {
		 padding : 10px 0px 0px 20px;
    }
    .titleLine {
		font-size : 12pt;
		font-weight : bold;
		text-align : left;
		vertical-align : bottom;
		padding : 0px 0px 6px 0px;
    } 
    .sectionTitleLine {
		font-size : 10pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 0px 0px 6px 0px;
    }
    .sectionTitleLineNav {
		font-size : 8pt;
		font-weight : normal;
		text-align : right;
		vertical-align : middle;
		padding : 0px 0px 0px 0px;
    }
    .label1, .label2 {
        font-size : 9pt;
		font-weight : bold;
		vertical-align : top;
		padding : 10px 10px 6px 0px;
    }
    .form1 {
        font-size : 10pt;
		font-weight : normal;
		padding : 0px 0px 0px 0px;
    }
    .elementContainer {
    padding : 0px 2px 0px 2px;
    }
    .columnHeader1 {
		font-size : 9pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 3px 10px 3px 10px;
		background-color: #E0E0DC;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 8pt;
		font-weight : normal;
		text-align : left;
		vertical-align : middle;
		padding : 1px 4px 1px 4px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #e5f9de;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ffffff;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }        
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
		text-align : center;
		width: 26px;
        height: 24px;
    }  
</style>
</head>
<body>
<div class="rptBody01">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr><td class="titleLine">Abercrombie & Fitch -> Appendix: Appendix M2 - EUSR Project</td></tr>
 </table>
 </td>
</tr>
 
<tr>
<td>
<br/>
 <table border="0" cellspacing="1" cellpadding="0">
 <tr>
     <td>Job </td>
     <td>Site </td>
     <td>Address </td>
     <td>City </td>
     <td>State </td>
     <td>Zip Code </td>
     <td>Status </td>
     <td>Planned Start </td>
     <td>Planned End </td>
     <td>Actual Start </td>
     <td>Actual End </td>
      <% 
     if (paraList != null) {
       for (int i=0; i<paraList.size(); i++) {
     %>
     <td><%=(String)parameter.get((String)paraList.get(i))%></td>
     <%
        }
     }
     %>  
     
 </tr>
 
     <% 
     if (rows != null) {
       for (int i=0; i<rows.size(); i++) {
    	 String[] r = (String[])rows.get(i);
     %>
     <tr>
     <td><%=r[0]%> </td>
     <td><%=r[1]%> </td>
     <td><%=r[2]%> </td>
     <td><%=r[3]%> </td>
     <td><%=r[4]%> </td>
     <td><%=r[5]%> </td>
     <td><%=r[6]%> </td>
     <td><%=r[7]%> </td>
     <td><%=r[10]%> </td>
     <td><%=r[11]%> </td>
     <td><%=r[12]%> </td>
      <% 
        if (paraList != null) {
         for (int j=0; j<paraList.size(); j++) {
     %>
     <td><%=r[14+j]%></td>
     <%
           }
         }
     %>  
     </tr>
     <%
        }
     }
     %>     
     
 </table>
</td>
</tr>
</table>

</div> 
</body>
</html>