<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.contingent.ilex.utility.RevRptConst"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script language="javascript" type="text/javascript" src="Scripts/firebug/firebug.js"></script>		

		<%-- BEAN DECLARATION --%>
		<jsp:useBean id="dwsql" class="com.contingent.ilex.utility.DWSqlBean" scope="application" />
		<jsp:useBean id="rc" class="com.contingent.ilex.utility.RevRptConst" scope="application" />

		<%-- IMPORT DECLARATION --%>
		<%@ page language="java"
			import="java.util.ArrayList"
		%>

		<%-- JavaScript --%>
		<script>
		var request = false;
		function createRequest() {
			try {
				request = new XMLHttpRequest();
			} catch (failed) {
				request = false;
			}
			if (!request) {
				alert("Error initializing XMLHttpRequest!");
			}
		}
		function initPage() {
			createRequest();
			getNewGraph();
			//winHeight = document.body.clientHeight/1;
			//winWidth = document.body.clientWidth/1;
			//var eText = document.getElementById("elementText");
			//eText.cols = Math.floor(winWidth/widthScale);
		}
		function getNewGraph() {
			document.getElementById('bottom').innerHTML = "";
			var element;
			element = document.getElementById("datetype");
			var dtype = element.options[element.selectedIndex].text;
			element = document.getElementById("startdate");
			var dstart = element.options[element.selectedIndex].text;
			element = document.getElementById("enddate");
			var dend = element.options[element.selectedIndex].text;
			element = document.getElementById("spmgr");
			var spm = element.options[element.selectedIndex].text;
			element = document.getElementById("bdmgr");
			var bdm = element.options[element.selectedIndex].text;
			element = document.getElementById("jowner");
			var jo = element.options[element.selectedIndex].text;
			element = document.getElementById("clients");
			var cli = element.options[element.selectedIndex].text;
			element = document.getElementById("ptype");
			var pt = element.options[element.selectedIndex].text;
			
			//alert("dtype="+dtype+",dstart="+dstart+",dend="+dend+",spm="+spm+",bdm="+bdm+",jo="+jo+",cli="+cli+",pt="+pt);
			url = "/rpt1/RevenueGraphServlet.xo?dtype="+dtype+"&dstart="+dstart+"&dend="+dend+"&spm="+spm+"&bdm="+bdm+"&jo="+jo+"&cli="+cli+"&pt="+pt;
			request.open("get", url, true);
			request.onreadystatechange = updatePage;
			request.send(null);
		}
		function updatePage() {
			var response;
			var txtAllowed = "false";
			if (request.readyState == 4) {
   				if (request.status == 200) {
   					//alert(request.getAllResponseHeaders());
   					response = request.responseText;
   					document.getElementById('bottom').innerHTML = response;
				} else if (request.status == 404) {
					alert("Requested URL does not exist");
				} else if (request.status == 403) {
					alert("Access denied");
				} else {
						alert("request status is "+request.status);
				}
			}
		}
		</script>

		<style type="text/css">
			<!--
			body {font-family:Arial; font-size:90%; background-color:#FFF7E5}
			.thyearcell {border-bottom:3px solid black}
			.thmonthcell {border-bottom:3px solid black; border-right:1px solid black; border-left:1px solid black}
			.thcell {border-bottom:3px solid black; border-right:1px solid black}
			.charttable {background-color:#<%=RevRptConst.DEF_CHARTBG_COLOR%>}
			.yearcell {border-top:1px solid black}
			.monthcell {border-top:1px solid black; border-right:1px solid black; border-left:1px solid black}
			.vcogscell {color:#<%=RevRptConst.COLOR_VCOGS%>; font-weight:bold; border-top:1px solid black; border-right:1px solid black}
			.vgpcell {color:#<%=RevRptConst.COLOR_VGP%>; font-weight:bold; border-top:1px solid black; border-right:1px solid black}
			.revcell {color:#<%=RevRptConst.COLOR_REVENUE%>; font-weight:bold; border-top:1px solid black; border-right:1px solid black}
			.rButton {background-color:#009900; align:left;}
			-->
		</style>

		<title>Financial Summary - Revenue</title>
	</head>
	<body>
		<%
			//Get the data for the drop down lists
				ArrayList<String> dates = dwsql.getMonthYearDates(null);
				session.setAttribute("datesArray", dates); //to re-use elsewhere
				ArrayList<String> spmgr = dwsql.getFullNamesForIdList(RevRptConst.SR_PROJ_MGR_IDTYPE, RevRptConst.LASTNAMEFIRST, RevRptConst.DEF_SRPROJMAN);
				ArrayList<String> bdmgr = dwsql.getFullNamesForIdList(RevRptConst.BUS_DEV_MGR_IDTYPE, RevRptConst.LASTNAMEFIRST, RevRptConst.DEF_BUSDEVMAN);
				ArrayList<String> jowner = dwsql.getFullNamesForIdList(RevRptConst.JOB_OWNER_IDTYPE, RevRptConst.LASTNAMEFIRST, RevRptConst.DEF_JOBOWNER);
				ArrayList<String> clients = dwsql.getClients(RevRptConst.DEF_CLIENT);
				ArrayList<String> projtypes = dwsql.getProjectTypes(RevRptConst.DEF_PROJTYPE);
				// default values
				int lastcol_width = 15;
		%>
		<table  border="0" cellpadding="1" cellspacing="2">
			<tr>
				<td width=15% align='right'>Business Dev't. Mgr:&nbsp;;</td>
				<td width=23% align='left'>
					<select id='bdmgr' size='1'>
							<% String busdevmgr = "";
							for (int i=0; i<bdmgr.size(); i++) {
								busdevmgr = (String)bdmgr.get(i); %>
								<option>
									<%=busdevmgr%>
								</option>
						<%	} %>
					</select>
				</td>
				<td width=9% align='left'>Client:&nbsp;</td>
				<td width=25% align='left'>
					<select id='clients' size='1'>
							<% String clt = "";
							for (int i=0; i<clients.size(); i++) {
								clt = (String)clients.get(i); %>
								<option>
									<%=clt%>
								</option>
						<%	} %>
					</select>
				</td>
				<td>&nbsp;&nbsp;Start (Month/Year):&nbsp;
					<select id='startdate' size='1'>
							<% String start = "";
							for (int i=0; i<dates.size(); i++) {
								start = (String)dates.get(i); %>
								<option <%= i == (dates.size()-12) ? "SELECTED" : "" %>>
									<%=start%>
								</option>
						<%	} %>
					</select>
				</td>
			</tr>
			<tr>
				<td width=15% align='right'>Senior Project Mgr:&nbsp;</td>
				<td width=23% align='left'> 
					<select id='spmgr' size='1'>
							<% String srprojmgr = "";
							for (int i=0; i<spmgr.size(); i++) {
								srprojmgr = (String)spmgr.get(i); %>
								<option>
									<%=srprojmgr%>
								</option>
						<%	} %>
					</select>
				</td>
				<td width=9% align='left'>Project Type:&nbsp;</td>
				<td width=25% align='left'>
					<select id='ptype' size='1'>
							<% String pt = "";
							for (int i=0; i<projtypes.size(); i++) {
								pt = (String)projtypes.get(i); %>
								<option>
									<%=pt%>
								</option>
						<%	} %>
					</select>
				</td>
				<td>&nbsp;&nbsp;End (Month/Year):&nbsp;
					<select id='enddate' size='1'>
							<% String end = "";
							for (int i=0; i<dates.size(); i++) {
								end = (String)dates.get(i); %>
								<option <%= i == dates.size()-1 ? "SELECTED" : "" %>>
									<%=end%>
								</option>
						<%	} %>
					</select>
				</td>
			</tr>
			<tr>
				<td width=12% align='right'>Job Owner:&nbsp;</td>
				<td colspan=3 align='left'>
					<select id='jowner' size='1'>
							<% String jobowner = "";
							for (int i=0; i<jowner.size(); i++) {
								jobowner = (String)jowner.get(i); %>
								<option>
									<%=jobowner%>
								</option>
						<%	} %>
					</select>
				</td>
				<td colspan=3>&nbsp;&nbsp;Revenue Basis:&nbsp;
					<select id='datetype' size='1'>
							<%
								String datetype = "";
							    String datename = "";
												for (int i=0; i<RevRptConst.DATETYPES1.length; i++) {
													datetype = RevRptConst.DATETYPES1[i];
													datename = RevRptConst.DATENAMES[i]; 
							%>
								<option values="<%=datetype%>"> <%=datename%></option>
						<%	} %>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan='5' align="right">
					<INPUT type='button' name='button1' value='Report!' style="background-color:#99FF99"  onClick='getNewGraph()'>
				</td>
			</tr>
		</table>
		<div id='bottom'></div>
		<script>initPage()</script>
	</body >
</html>