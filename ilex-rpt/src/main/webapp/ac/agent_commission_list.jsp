<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<%@ include file="js.txt"%>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr><td class="titleLine">Agent and Commission List</td></tr>
<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
    <tr>
     <td rowspan="2" class="columnHeader2">Customer</td>
     <td rowspan="2" class="columnHeader2">Project</td>
     <td rowspan="2" class="columnHeader2">Percentage</td>
     <td width="80" rowspan="2"  class="columnHeader2">Type</td>
     <td colspan="2"  class="columnHeader2">Estimated</td>
   </tr>
   <tr>
     <td class="columnHeader2">&nbsp;&nbsp;Revenue&nbsp;&nbsp;</td>
     <td class="columnHeader2">&nbsp;&nbsp;Commission&nbsp;&nbsp;</td>
   </tr>
   
   <%

   	Vector rows = (Vector) request.getAttribute("rows");
   	if (rows != null && rows.size() > 0) {
   		for (int i = 0; i < rows.size(); i++) {
   			String[] r = (String[]) rows.get(i);

   			switch (Integer.parseInt(r[0])) {

   			case 1:
   %>
			   <tr>
				<td class="cDataLeft<%=i%2%>"><%=r[1]%></td>
				<td class="cDataLeft<%=i%2%>"><%=r[2]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[4]%></td>
				<td class="cDataCenter<%=i%2%>"><%=r[5]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[6]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[7]%></td>
			   </tr>
			   <%
			   			break;

			   			case 2:
			   %>
			   <tr>
				<td colspan="6" class="cTotalLeft1"><%=r[1]%></td>
			   </tr>
			   <%
			   			break;

			   			case 3:
			   %>
			   <tr>
				<td colspan="5" class="cTotalRight1">Total</td>
				<td class="cTotalRight1"><%=r[6]%></td>
			   </tr>
			   <%
			   			break;

			   			case 4:
			   %>
			   <tr>
				<td colspan="6" class="break1"></td>
			   </tr>
			   <%
			   			break;
			   			}
			   		}
			   	}
			   %>

</table>
</td></tr>
</table>
</div>
</body>
</html>