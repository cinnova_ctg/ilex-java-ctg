<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%
String[][] labels = new String[3][3];
labels[1][0] = ""; // Checkbox status
labels[1][1] = "Confirm Commissions";
labels[1][2] = "Commission Payment Summary - Select Commissions";

labels[2][0] = "disabled";
labels[2][1] = "Pay Commissions";
labels[2][2] = "Commission Payment Summary - Confirm and Pay";

int sf = Integer.parseInt((String)request.getAttribute("sf"));

String reselect_button = "";

if (sf == 2) {
	reselect_button = 
	"&nbsp;&nbsp;<form action=\"/rpt/ManageAgentCommissions.xo\" method=\"post\">"+
	"<input type=\"submit\" value=\"Re-select/Correct Commissions\" class=\"btn5\"/>"+
	"<input type=\"hidden\" name=\"ac\" value=\"acm\">"+
	"<input type=\"hidden\" name=\"sf\" value=\"1\">"+
	"<input type=\"hidden\" name=\"agent\" value=\""+request.getAttribute("agent")+"\">"+
    "<input type=\"hidden\" name=\"timeframe\" value=\""+request.getAttribute("timeframe")+"\">"+
	"<input type=\"hidden\" name=\"com_type\" value=\""+request.getAttribute("com_type")+"\">"+
	"</form>";
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Agent Commission Payment</title>
<script src="/rpt/ac/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

var CommissionTotal = 0.0;

/*************************************************/

// On Ready Function

$(function(){
   ComputeCommissionTotal();     
  }
);

/*************************************************/

// Compute/re-compute the commision based on checkboxes

function ComputeCommissionTotal (job) {

var selector1 = 'span[id^=cost]';
var selector2 = 'span#total';

CommissionTotal = 0.0;

// Get line subtotal elements and add up
var authcost = $(selector1).get();
$.each(authcost, SumTextElements);

// Update total
span1 = $(selector2).get();
span1[0].innerHTML = CommissionTotal.toFixed(2)

}

/*************************************************/

function SumTextElements () {

// For each line, check the checkbox and add if checked
var sel3 = '#'+this.id+'cb:checkbox:checked'

if ($(sel3).val() != null) {
  CommissionTotal = CommissionTotal+parseFloat(this.innerHTML)
}
}
</script>
<%@ include file="styles.txt"%>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<form action="/rpt/ManageAgentCommissions.xo" method="post">
<%
if (sf == 1) {
%>
<input type="hidden" name="ac" value="acm">
<input type="hidden" name="sf" value="2">
<input type="hidden" name="agent" value="<%=request.getAttribute("agent")%>">
<input type="hidden" name="timeframe" value="<%=request.getAttribute("timeframe")%>">
<input type="hidden" name="com_type" value="<%=request.getAttribute("com_type")%>">
<% 
} else if (sf == 2) {
%>
<input type="hidden" name="ac" value="acm">
<input type="hidden" name="sf" value="3">
<input type="hidden" name="agent" value="<%=request.getAttribute("agent")%>">
<input type="hidden" name="timeframe" value="<%=request.getAttribute("timeframe")%>">
<input type="hidden" name="com_type" value="<%=request.getAttribute("com_type")%>">
<input type="hidden" name="po_list" value="<%=request.getAttribute("po_list")%>">
<input type="hidden" name="check_number" value="<%=request.getAttribute("check_number")%>">
<%	
}
%>
<tr><td class="titleLine"><%=labels[sf][2]%></td></tr>
<%if (request.getAttribute("error") != null && !request.getAttribute("error").equals("")) {%><tr><td class="errorMessage"><%=request.getAttribute("error")%></td></tr><%}%>

<tr><td Class= "sectionTitleLine2">Agent:&nbsp;&nbsp;<%=request.getAttribute("partner_name")%></td></tr>
<tr><td Class= "sectionTitleLine2">Scheduled Pay Date:&nbsp;&nbsp;<%=request.getAttribute("timeframe")%></td></tr>
<tr><td Class= "sectionTitleLine2">Total:&nbsp;&nbsp;$<span id="total"><%=request.getAttribute("total_payment")%></span></td></tr>
<tr><td Class= "sectionTitleLine2">Check #:&nbsp;&nbsp;<input class="textInput1" type="text" name="check_number" value="<%=request.getAttribute("check_number")%>" size="10" <%=labels[sf][0]%>></td></tr>

<tr><td align="right">&nbsp;</td></tr>

<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
    <tr>
     <td rowspan="2" class="columnHeader2">Customer/Project</td>
     <td rowspan="2" class="columnHeader2">Job</td>
      <td rowspan="2" class="columnHeader2">CNS Invoice</td>
     <td rowspan="2" class="columnHeader2">&nbsp;&nbsp;Invoiced&nbsp;&nbsp;</td>
     <td colspan="3" class="columnHeader2">Ilex update</td>
   </tr>
   <tr>
     <td width="70" class="columnHeader2">Status</td>
     <td width="80" class="columnHeader2">Date*</td>
     <td width="80" class="columnHeader2">Amount</td>
   </tr>
   <%
   Vector rows = (Vector) request.getAttribute("rows");
  	if (rows != null && rows.size() > 0) {
  		for (int i = 0; i < rows.size(); i++) {
  			
	   /*
	   1 lo_ot_name
	   2 lx_pr_title
	   3 lm_js_title
	   4 lm_po_complete_date
	   5 lm_pi_status
	   6 lm_pi_approved_total
	   7 lm_pi_approved_date
	   8 lm_pi_payment_scheduled
	   9 lm_po_id
	   10 lm_po_partner_id
	   11 lm_js_invoice_no
	   12 eInvoice paid date
	   */
	   
	   String[] r = (String[]) rows.get(i);
   %>
   		<tr>
		 <td class="cDataLeft<%=i%2%>"><input type="checkbox" name="checked_commissions" value="<%=r[8]%>" id="cost<%=i%>cb" onclick="ComputeCommissionTotal()" checked <%=labels[sf][0]%>>&nbsp;&nbsp;<%=r[0]%>/<%=r[1]%></td>
		 <td class="cDataLeft<%=i%2%>"><%=r[2]%></td>
		 <td class="cDataLeft<%=i%2%>"><%=r[10]%></td>
		 
		 <td class="cDataRight<%=i%2%>"><%=r[12]%></td>
		 <td class="cDataCenter<%=i%2%>"><%=r[4]%></td>
		 <td class="cDataRight<%=i%2%>"><%=r[6]%></td>
		 <td class="cDataRight<%=i%2%>"><span id="cost<%=i%>"><%=r[5]%></span></td>
		</tr>
   <%
    } // for loop
   %>
   <tr>
     <td colspan="7" class="submitLine"><input type="submit" value="<%=labels[sf][1]%>" class="btn5"/></td>
   </tr>   
   
   <%
    } // if condition on Vector
   %>   
</table>
</td></tr>
</form>
<tr><td><%=reselect_button%></td></tr>   
</table>
</div>
</body>
</html>
