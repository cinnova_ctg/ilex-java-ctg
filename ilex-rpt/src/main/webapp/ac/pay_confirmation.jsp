<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Agent Commission Payment</title>
<%@ include file="styles.txt"%>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr><td class="titleLine">Commission Payment Summary - Payment Confirmation and Send Notice</td></tr>
<%if (request.getAttribute("error") != null && !request.getAttribute("error").equals("")) {%><tr><td class="errorMessage"><%=request.getAttribute("error")%></td></tr><%}%>
<tr><td Class= "sectionTitleLine2">Agent:&nbsp;&nbsp;<%=request.getAttribute("partner_name")%></td></tr>
<tr><td Class= "sectionTitleLine2">Scheduled Pay Date:&nbsp;&nbsp;<%=request.getAttribute("timeframe")%></td></tr>
<tr><td Class= "sectionTitleLine2">Email Summary:</td></tr>
<tr><td align="right">&nbsp;</td></tr>
<tr><td Class= "sectionTitleLine2">Email To:&nbsp;&nbsp;<%=request.getAttribute("first_name")%>&nbsp;<%=request.getAttribute("last_name")%></td></tr>
<tr><td Class= "sectionTitleLine2">Email Address:&nbsp;&nbsp;<%=request.getAttribute("email_address")%></td></tr>
<tr><td align="right">&nbsp;</td></tr>
<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
<tr><td><%=request.getAttribute("email")%></td></tr>
</table>
</td></tr>
<tr><td align="right">&nbsp;</td></tr>
<tr><td>
<form action="/rpt/ManageAgentCommissions.xo" method="post">
<input type="hidden" name="ac" value="acm">
<input type="hidden" name="sf" value="4">
<input type="hidden" name="agent" value="<%=request.getAttribute("agent")%>">
<input type="hidden" name="partner_name" value="<%=request.getAttribute("partner_name")%>">
<input type="hidden" name="timeframe" value="<%=request.getAttribute("timeframe")%>">
<input type="hidden" name="com_type" value="<%=request.getAttribute("com_type")%>">
<input type="hidden" name="email" value="<%=request.getAttribute("email")%>">
<input type="hidden" name="email_address" value="<%=request.getAttribute("email_address")%>">
<input type="submit" value="Send Email to Agent" class="btn5"/>
</form>
</td></tr>
</table>
</div>
</body>
</html>
