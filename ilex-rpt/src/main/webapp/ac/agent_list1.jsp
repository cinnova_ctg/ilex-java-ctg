<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Agent Commission Payment</title>
<%
Vector rows = (Vector) request.getAttribute("rows");

boolean initial_page;
String agent = "";
String title = "";

if (rows == null) {
   initial_page = true;
   title = "Commission Summary - Select an Agent";	
} else {
   initial_page = false;
   agent = (String)request.getAttribute("agent");
   title = "Commission Summary for "+agent;
}
%>
<%@ include file="styles.txt"%>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr><td class="titleLine">NRC Quarterly Commissions</td></tr>
<%
if (request.getAttribute("error") != null && !request.getAttribute("error").equals("")) {
%>
<tr><td class="errorMessage"><%=request.getAttribute("error")%></td></tr>
<%
}
%>
<tr><td><%@ include file="form2.jsp"%></td></tr>
<tr><td Class= "sectionTitleLine"><%=title%></td></tr>
<%
if (!initial_page) {
%>
<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
    <tr>
     <td rowspan="2" class="columnHeader2">Customer/Project</td>
     <td rowspan="2" class="columnHeader2">Job</td>
     <td rowspan="2" class="columnHeader2">CNS Invoice</td>
     <td rowspan="2" class="columnHeader2">&nbsp;&nbsp;Invoiced&nbsp;&nbsp;</td>
     <td colspan="3" class="columnHeader2">Ilex update</td>
   </tr>
   <tr>
     <td width="70" class="columnHeader2">Status</td>
     <td width="80" class="columnHeader2">Date*</td>
     <td width="80" class="columnHeader2">Amount</td>
   </tr>
   
   <%
   	 /* Add one for type
   	 0 Type
   	 1 lo_ot_name
   	 2 lx_pr_title
   	 3 lm_js_title
   	 4 lm_po_complete_date
   	 5 lm_pi_status
   	 6 lm_pi_approved_total
   	 7 lm_pi_approved_date
   	 8 lm_pi_payment_scheduled
   	 9 lm_po_id
   	 10 lm_po_partner_id   
   	 */

   	if (rows != null && rows.size() > 0) {
   		for (int i = 0; i < rows.size(); i++) {
   			String[] r = (String[]) rows.get(i);

   			switch (Integer.parseInt(r[0])) {

   			case 1:
   %>
			   <tr>
				<td class="cDataLeft<%=i%2%>"><%=r[1]%>/<%=r[2]%></td>
				<td class="cDataLeft<%=i%2%>"><%=r[3]%></td>
				<td class="cDataLeft<%=i%2%>"><%=r[11]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[12]%></td>
				<td class="cDataCenter<%=i%2%>"><%=r[5]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[7]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[6]%></td>
			   </tr>
			   <%
			   			break;

			   			case 2:
			   %>
			   <form action="/rpt/ManageAgentCommissions.xo?ac=acm&sf=1&com_type=1" method="post">
               <input type="hidden" name="agent" value="<%=r[10]%>">
               <input type="hidden" name="timeframe" value="<%=r[8]%>">
			   <tr>
				<td colspan="7" class="cTotalLeft1">Payment Quarter <%=r[1]%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="submit" type="submit" value="Pay" class="btn5a"/></td>
			   </tr>
			   </form>
			   <%
			   			break;

			   			case 3:
			   %>
			   <tr>
				<td colspan="6" class="cTotalRight1">Total</td>
				<td class="cTotalRight1"><%=r[6]%></td>
			   </tr>
			   <%
			   			break;

			   			case 4:
			   %>
			   <tr>
				<td colspan="6" class="break1"></td>
			   </tr>
			   <%
			   			break;
			   			}
			   		}
			   	}
			   %>

</table>
</td></tr>
<%
}
%>
</table>
</div>
</body>
</html>