<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Agent Commission Payment</title>
<%@ include file="styles.txt"%>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr><td class="titleLine">Agent Payment Complete</td></tr>
<%
if (request.getAttribute("error") != null && !request.getAttribute("error").equals("")) {
%>
<tr><td class="errorMessage"><%=request.getAttribute("error")%></td></tr>
<%
}
%>
<tr><td Class= "sectionTitleLine2">Agent:&nbsp;&nbsp;<%=request.getAttribute("partner_name")%></td></tr>
<tr><td Class= "sectionTitleLine2">Scheduled Pay Date:&nbsp;&nbsp;<%=request.getAttribute("timeframe")%></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>This payment transaction was successfully completed.</td></tr>
</table>
</div>
</body>
</html>