<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Agent Commission Payment</title>
<%@ include file="styles.txt"%>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr><td class="titleLine">Page or Process Error.</td></tr>
<tr><td>The following error occurred while processing the last request.  Send a screen shot of this to the Ilex administrator.</td></tr>
<tr><td class="errorMessage"><%=request.getAttribute("error")%></td></tr>
</table>
</div>
</body>
</html>