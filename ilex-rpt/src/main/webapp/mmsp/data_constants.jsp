<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.HashMap" %>
<%
// Parallel set of arrays to support grouping conditions
String[] partner_list_name = {"partner_list1", "partner_list2", "partner_list3", "partner_list4"};

String[] section_titles = {"Ready for Payment",
		                   "Inconsistent Financial Information",
		                   "Pending Additional Information from Partner",
		                   "Job in Non-payable Status"};

String[] section_classes = {"sectionHeader1",
                            "sectionHeader2",
                            "sectionHeader3",
                            "sectionHeader4"};

String[] bgc_list = {"background-color: #e0edf5;",
                     "background-color: #e5f9de;",
                     "background-color: #fff1f0;",
                     "background-color: #ffccc9;"};

//Expand PO Type
Map po_type = new HashMap();
po_type.put("M","Minuteman");
po_type.put("S","Speedpay");

//Expand list_type for page title
Map report_type = new HashMap();
report_type.put("All",     "All POs in the Date Range");
report_type.put("Payable", "Only Payable Valid POs");
%>