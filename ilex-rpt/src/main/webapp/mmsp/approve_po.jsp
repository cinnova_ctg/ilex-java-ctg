<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.HashMap" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="data_constants.jsp" %> 

<%
// Get all inputs need for this page that were passed back on the request 
// as attributes

String type_of_po = (String)request.getAttribute("type_of_po");
String bgc = "";
// Verify that a list of POs was provided
int n = 0;
String list_status = "Error";
Vector partner_list = (Vector)request.getAttribute("partner_list1");

if (partner_list != null && partner_list.size() > 0){ 
	list_status = "OK";
}
 
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Reporting: <%=(String)po_type.get(type_of_po)%> PO Approval Form</title>
<style>
<%@ include file="style1.css"%>
</style>

<script type="text/javascript">


	function exportHistoryData(){
		//alert(document.URL+" ---  "+"/rpt/MMSP.xo?form_action=historyExport&pay_date=02/06/14&end_date=02/01/14&type_of_po=M");
		window.open(document.URL+"?genExport=Export&form_action=approve_list&type_of_po=<%=(String)request.getAttribute("type_of_po")%>&end_date=<%=(String)request.getAttribute("end_date")%>&list_flag=<%=(String)request.getAttribute("list_flag")%>", "_self");
	}
	function exportPOEntryData(){
		//alert(document.URL+" ---  "+"/rpt/MMSP.xo?form_action=historyExport&pay_date=02/06/14&end_date=02/01/14&type_of_po=M");
	window.open(document.URL+"?genExport=ExportPOEntry&form_action=approve_list&type_of_po=<%=(String)request.getAttribute("type_of_po")%>&end_date=<%=(String)request.getAttribute("end_date")%>&list_flag=<%=(String)request.getAttribute("list_flag")%>", "_self");	
	//	window.open(document.URL+"&genExport=ExportPOEntry&tableId="+tableId, "_self");
	}
	function exportReceiptData(){
		//alert(document.URL+" ---  "+"/rpt/MMSP.xo?form_action=historyExport&pay_date=02/06/14&end_date=02/01/14&type_of_po=M");
	window.open(document.URL+"?genExport=ExportReceipt&form_action=approve_list&type_of_po=<%=(String)request.getAttribute("type_of_po")%>&end_date=<%=(String)request.getAttribute("end_date")%>&list_flag=<%=(String)request.getAttribute("list_flag")%>", "_self");	
	//	window.open(document.URL+"&genExport=ExportReceipt&tableId="+tableId, "_self");
	}
	
	 function checkform(index,innerindex) 
	{
		
	if(document.getElementById("gpvendorId"+index+innerindex).value=="")
			 
	{
		
	    alert("Please Enter GP Vendor ID");
	    return false;
	}
	
		return true; 
	
}
	
</script>
<script type='text/javascript'>

function check(index,poid,indexinner)
{

	
	this.document.getElementById("gp"+index+indexinner).style.display = "";
	document.getElementById("blank"+index+indexinner).style.display = "none";
	document.getElementById("po").value = poid;
	
    return false;       
}



</script>

</head>

<body>
<div class="rptBody01">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr><td class="titleLine"><%=(String)po_type.get(type_of_po)%> PO Approval Form</td></tr>
 <tr><td class="sectionTitleLine">All Payable POs through <%=request.getAttribute("end_date")%> (End of Current Pay Period)</td></tr>
 </table>
 </td>
</tr>

<% 
  // Only one list is available - list of ready for payment
  if (list_status.equals("OK")) {
%>

<form action="MMSP.xo" method="post">
<tr>
<td>
<br/>
 <table border="0" cellspacing="1" cellpadding="2">
	<tr> 
	<td colspan="4" class="<%=section_classes[n]%>"><%=section_titles[n]%></td>
	<td colspan="10"  class="<%=section_classes[n]%>"><input type="button" value="Export" name="export" id="export" title="Export" onclick="exportHistoryData()" />&nbsp;<input type="button" value="PO Entry" name="POEntry" id="POEntry" onclick="exportPOEntryData();" title="PO Entry" />&nbsp;<input type="button" value="Receipt" name="Receipt" id="Receipt" onclick="exportReceiptData();" title="Receipt" /></td>
	
	</tr>
	
	<tr>
   	<td rowspan="2" class="columnHeader1">Name</td>
   	<td rowspan="2" class="columnHeader1">CH</td>
	<td rowspan="2" class="columnHeader1">PO&nbsp;#</td>
	<td colspan="3" rowspan="2" class="columnHeader1">GPVendorID&nbsp;</td>
	<td rowspan="2" class="columnHeader1">Offsite</td>
	<td rowspan="2" class="columnHeader1">PO Status</td>
	<td rowspan="2" class="columnHeader1">Job Status</td>
	<td rowspan="2" class="columnHeader1">Owner</td>	
	<td colspan="3" class="columnHeader1">Cost</td>
	<td colspan="3" class="columnHeader1">Cost Breakdown</td>
	<td rowspan="2" class="columnHeader1">Job</td>
	</tr>

	<tr>
    <td class="columnHeader1">&nbspAuthorized&nbsp</td>
	<td class="columnHeader1">&nbspApproved&nbsp</td>
	<td class="columnHeader1">&nbspAmount to Pay&nbsp</td>
	<td class="columnHeader1">&nbsp&nbspLabor&nbsp&nbsp&nbsp</td>
	<td class="columnHeader1">&nbsp&nbspTravel&nbsp&nbsp</td>
	<td class="columnHeader1">&nbspHardware&nbsp</td>
	</tr>
	
    <%
    for (int i=0; i< partner_list.size(); i++) {
	   
	   Map partner_details = (Map)partner_list.get(i);
       
	   String partner_name = (String)partner_details.get("partner_name");
	   String effective = (String)partner_details.get("effective");
	   String labor = (String)partner_details.get("labor");
	   String hardware = (String)partner_details.get("hardware");
	   String travel = (String)partner_details.get("travel");

	   Vector po_list = (Vector) partner_details.get("po_list");
	   
	   if (i%2 > 0) {
		   bgc = bgc_list[n];
	   } else {
		   bgc = "background-color: #ffffff;";
	   }
    
	   for (int j=0; j<po_list.size(); j++) {
		   String[] po = (String[])po_list.get(j);
		   String link = "http://ilex.contingent.local/Ilex/POWODetailAction.do?viewjobtype=A&ownerId=%&powoid="+po[1]+"&jobid="+po[14];
    	   String  gpVendorId=po[19];
    %>
	<tr>
	<% 
	//if(po[19].contains(",")){ 
	
		//gpVendorId=po[19].replace(",", "");
	//}
	

	
	%>
	<%if (!partner_name.equals("")) { %>
	<td rowspan="<%=po_list.size()+1%>" nowrap class="cDataLeft1" style="<%=bgc%>"><b><%=partner_name%></b></td>
	
	
	<%}%>
	
	<td class="cDataCenter1" style="<%=bgc%>"><INPUT TYPE=CHECKBOX NAME="lm_po_id" value="<%=po[1]%>" checked></td>
	<td id="poId<%=i%>" class="cDataLeft1"  style="<%=bgc%>"><a href="<%=link%>"><%=po[1]%></a><%=po[13]%></td>
	<%if (!partner_name.equals("")) { %>
	
	<% if(gpVendorId.equals("") || gpVendorId==null){
	
		bgc = "background-color: red;";	%>
		<%-- <a href="" onclick="updateVendorId(<%=po[1]%>);" >[Update]</a> --%>
		
		<td colspan="3"    rowspan="<%=po_list.size()+1%>"   id="gp<%=i%><%=j%>" style="display:none;" ><INPUT TYPE=text size="8" maxlength="15" NAME="gpvendorId" id="gpvendorId<%=i%><%=j%>" ><input type="submit"  id="accept<%=i%>" style="button" value="Update" onclick="return checkform(<%=i%>,<%=j%>);"></td>
	    <td  colspan="3"    rowspan="<%=po_list.size()+1%>"   id="blank<%=i%><%=j%>" class="cDataCenter1" style="<%=bgc%>;display:;" onclick="check(<%=i%>,<%=po[1]%>,<%=j%>);" >
	    <Span  Style="cursor: pointer;color: blue;width:100px;"  ><u>Add</u></span>
	  
	    </td>
	   
	     <div style="visibility: hidden"> 
		 <input type="text"	style="display:none;" id="po" >
		</div> 
	<% 	}else {%>
	
		<td colspan="3"  rowspan="<%=po_list.size()+1%>"  class="cDataCenter1" style="<%=bgc%>"><%=gpVendorId%></td>
	
	<% }%>
	
	<%}%>
     <% if (i%2 > 0) {
		   bgc = bgc_list[n];
	   } else {
		   bgc = "background-color: #ffffff;";
	   } %>
	<td class="cDataCenter1" style="<%=bgc%>"><%=po[2]%></td>
	<td class="cDataCenter1" style="<%=bgc%>"><%=po[17]%></td>
	<td class="cDataLeft1" style="<%=bgc%>"><%=po[3]%></td>        
	<td class="cDataLeft1" style="<%=bgc%>"><%=po[4]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[5]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[6]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[7]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[8]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[9]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[10]%></td>
	<td class="cDataLeft1" style="<%=bgc%>"><%=po[11]%></td>
    </tr>
    <%
        partner_name = "";
	   }
	%>
	<tr>
    <td colspan="8" class="cDataRight0" style="<%=bgc%>"><b>Subtotal</b></td>
    <td class="cDataRight0" style="<%=bgc%>"><b><%=effective%></b></td>
    <td class="cDataRight0" style="<%=bgc%>"><b><%=labor%></b></td>
    <td class="cDataRight0" style="<%=bgc%>"><b><%=travel%></b></td>
    <td class="cDataRight0" style="<%=bgc%>"><b><%=hardware%></b></td>
    <td class="cDataLeft0" style="<%=bgc%>">&nbsp;</td>
    </tr>  
    
     
	<%
	  }
    %>
    
      	<tr>
	    <td colspan="9" class="cDataRight0" style="<%=bgc%>"><b>Payable Total</b></td>
	    <td class="cDataRight0" style="<%=bgc%>"><b><%=(String)request.getAttribute("status_total1")%></b></td>
	    <td class="cDataRight0" style="<%=bgc%>"><b></b></td>
	    <td class="cDataRight0" style="<%=bgc%>"><b></b></td>
	    <td class="cDataRight0" style="<%=bgc%>"><b></b></td>
	    </tr>   
  
 </table>
</td>
</tr>
<tr>
<td class="label1">
User:&nbsp;&nbsp;<input type="text" name="user" size="3"><br>
<input type="submit" style="button" value="Pay All Checked POs">&nbsp;&nbsp;<input type="Reset" style="button">&nbsp;&nbsp;<button style="button" type="submit">Cancel</button>
<input type="hidden" name="form_action" value="payment">
<input type="hidden" name="start_date"  value="<%=(String)request.getAttribute("start_date")%>">
<input type="hidden" name="end_date"    value="<%=(String)request.getAttribute("end_date")%>">
<input type="hidden" name="type_of_po"  value="<%=(String)request.getAttribute("type_of_po")%>">
</form>
</td>
</tr>
<%

} else {
%>    
<tr>
  <td class="sectionTitleLine"><b>Error</b></td>
</tr>   
<%
}
%>
</table>
</div> 
</body>
</html>