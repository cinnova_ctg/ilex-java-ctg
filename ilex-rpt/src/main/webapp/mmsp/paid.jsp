<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.HashMap" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="data_constants.jsp" %> 

<%
// Get all inputs need for this page that were passed back on the request 
// as attributes

String type_of_po = (String)request.getAttribute("type_of_po");
String status_total1 = "";
String bgc = "";
// Verify that a list of POs was provided
int n = 0;
String list_status = "Error";
Vector partner_list = (Vector)request.getAttribute("partner_list1");

if (partner_list != null && partner_list.size() > 0){ 
	list_status = "OK";
}
 
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Reporting: <%=(String)po_type.get(type_of_po)%> PO Payment Summary</title>
<style>
<%@ include file="style1.css"%>
</style>
<script type="text/javascript">


	function exportHistoryData(){
		//alert(document.URL+" ---  "+"/rpt/MMSP.xo?form_action=historyExport&pay_date=02/06/14&end_date=02/01/14&type_of_po=M");
		window.open(document.URL+"&genExport=Export", "_self");
	}
	function exportPOEntryData(){
		//alert(document.URL+" ---  "+"/rpt/MMSP.xo?form_action=historyExport&pay_date=02/06/14&end_date=02/01/14&type_of_po=M");
	window.open(document.URL+"&genExport=ExportPOEntry", "_self");	
		//window.open(document.URL+"&genExport=ExportPOEntry&tableId="+tableId, "_self");
	}
	function exportReceiptData(){
		//alert(document.URL+" ---  "+"/rpt/MMSP.xo?form_action=historyExport&pay_date=02/06/14&end_date=02/01/14&type_of_po=M");
	window.open(document.URL+"&genExport=ExportReceipt", "_self");	
		//window.open(document.URL+"&genExport=ExportReceipt&tableId="+tableId, "_self");
	}
</script>
</head>

<body>
<div class="rptBody01">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr><td class="titleLine"><%=(String)po_type.get(type_of_po)%> PO Payment Summary</td></tr>
 <tr><td class="sectionTitleLine">Pay Date <%=request.getAttribute("pay_date")%></td></tr>
 </table>
 </td>
</tr>

<% 
  // Only one list is available - list of ready for payment
  if (list_status.equals("OK")) {
%>

<tr>
<td>
<br/>
 <table border="0" cellspacing="1" cellpadding="2">
	<tr> 
	<td colspan="4" class="<%=section_classes[n]%>"><%=section_titles[n]%></td>
	<td colspan="10"  class="<%=section_classes[n]%>"><input type="button" value="Export" name="export" id="export" title="Export" onclick="exportHistoryData()" />&nbsp;<input type="button" value="POEntry" name="POEntry" id="POEntry" onclick="exportPOEntryData();" title="PO Entry" />&nbsp;<input type="button" value="Receipt" name="Receipt" id="Receipt" onclick="exportReceiptData();" title="Receipt" /></td>
	
	</tr>
	
	<tr>
   	<td rowspan="2" class="columnHeader1">Name</td>
	<td colspan="3" class="columnHeader1">Purchase&nbsp;Order</td>
	<td rowspan="2" class="columnHeader1">Payable</td>
	<td rowspan="2" class="columnHeader1">Job</td>
	<td rowspan="2" class="columnHeader1">Offsite</td>
	<td rowspan="2" class="columnHeader1">Status</td>
	<td rowspan="2" class="columnHeader1">Owner</td>	
	<td colspan="2" class="columnHeader1">Cost</td>
	<td colspan="3" class="columnHeader1">Cost Breakdown</td>

	</tr>

	<tr>
	<td class="columnHeader1">&nbsp;Number&nbsp;</td>
	<td class="columnHeader1">&nbsp;Type&nbsp;</td>
	<td class="columnHeader1">&nbsp;Status&nbsp;</td>
    <td class="columnHeader1">&nbsp;Authorized&nbsp;</td>
	<td class="columnHeader1">&nbsp;Amount to Pay&nbsp;</td>
	<td class="columnHeader1">&nbsp;&nbsp;Labor&nbsp;&nbsp;&nbsp;</td>
	<td class="columnHeader1">&nbsp;&nbsp;Travel&nbsp;&nbsp;</td>
	<td class="columnHeader1">&nbsp;Hardware&nbsp;</td>
	</tr>
	
    <%
    for (int i=0; i< partner_list.size(); i++) {
	   
	   Map partner_details = (Map)partner_list.get(i);
	   String partner_name = (String)partner_details.get("partner_name");
	   String effective = (String)partner_details.get("effective");
	   String labor = (String)partner_details.get("labor");
	   String hardware = (String)partner_details.get("hardware");
	   String travel = (String)partner_details.get("travel");

	   Vector po_list = (Vector) partner_details.get("po_list");
	   
	   if (i%2 > 0) {
		   bgc = bgc_list[n];
	   } else {
		   bgc = "background-color: #ffffff;";
	   }
    
	   for (int j=0; j<po_list.size(); j++) {
		   String[] po = (String[])po_list.get(j);
		   String link = "http://ilex.contingent.local/Ilex/POWODetailAction.do?viewjobtype=A&ownerId=%&powoid="+po[1]+"&jobid="+po[14];
    %>
	<tr>
	<%if (!partner_name.equals("")) { %>
	<td rowspan="<%=po_list.size()+1%>" nowrap class="cDataLeft1" style="<%=bgc%>"><b><%=partner_name%></b></td>
	<%}%>
	<td class="cDataCenter1" style="<%=bgc%>"><a href="<%=link%>"><%=po[1]%></a></td>
	<td class="cDataCenter1" style="<%=bgc%>"><%=po[13]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[17]%></td>
	<td class="cDataCenter1" style="<%=bgc%>"><%=po[16]%></td>
	<td class="cDataLeft1" style="<%=bgc%>"><%=po[12]%> | <%=po[11]%></td> 
	<td class="cDataCenter1" style="<%=bgc%>"><%=po[2]%></td>  
	<td class="cDataLeft1" style="<%=bgc%>"><%=po[3]%></td>  
	<td class="cDataLeft1" style="<%=bgc%>"><%=po[4]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[5]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[7]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[8]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[9]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[10]%></td>
    </tr>
    <%
        partner_name = "";
	   } // for (int j=0; j<po_list.size(); j++)
	%>
	<tr>
    <td colspan="9" class="cDataRight0" style="<%=bgc%>"><b>Subtotal</b></td>
    <td class="cDataRight0" style="<%=bgc%>"><b><%=effective%></b></td>
    <td class="cDataRight0" style="<%=bgc%>"><b><%=labor%></b></td>
    <td class="cDataRight0" style="<%=bgc%>"><b><%=travel%></b></td>
    <td class="cDataRight0" style="<%=bgc%>"><b><%=hardware%></b></td>
    </tr>      
	<%
	  }
    
	 if (n == 0) {
		    status_total1 = (String)request.getAttribute("status_total1");
		    %>
		    <tr>
		    <td colspan="10" class="cDataRight0" style="<%=bgc%>"><b>Payable Total</b></td>
		    <td class="cDataRight0" style="<%=bgc%>"><b><%=status_total1%></b></td>
		    <td class="cDataRight0" style="<%=bgc%>"><b></b></td>
		    <td class="cDataRight0" style="<%=bgc%>"><b></b></td>
		    <td class="cDataRight0" style="<%=bgc%>"><b></b></td>
		    </tr>   
		    <%
		 }
    
    %>
 </table>
</td>
</tr>
<%
} else {
%>    
<tr>
  <td class="sectionTitleLine"><b>Error</b></td>
</tr>   
<%
}
%>
</table>
</div> 
</body>
</html>