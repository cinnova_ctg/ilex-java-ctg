<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<HTML>
<HEAD>
<TITLE>Productivity</TITLE>
<title>Quarterly Commission Summary</title>
<%@ include file="styles.txt"%>
<script type="text/javascript" src="netflow/FusionCharts.js"></script>
<%
Map<String, Vector<Map<String,String>>> device_details = (Map<String, Vector<Map<String,String>>>)request.getAttribute("device_details");
Vector<String> device_list = (Vector<String>)request.getAttribute("device_list");

String name = (String)request.getAttribute("name");
String identifier = (String)request.getAttribute("identifier");
String i1 = (String)request.getAttribute("i1");
String i2 = (String)request.getAttribute("i2");

String seriesname;
if (i2.equals("I")) {
	seriesname = "Input";
} else {
	seriesname = "Output";
}
// & = %26 
// = = %3d
String url = "ac%3dgetUsageDataStream%26identifier%3d"+identifier+"%26i1%3d"+i1+"%26i2%3d"+i2;
%>
</HEAD>
<BODY>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="cDataRight0">
<table border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">Device</td><td align="center">Interface</td></tr>
<%

Vector<Map<String,String>> interface_list;
Map<String,String> interface_details;
String site;

if (device_list.size() > 0) {
	for (int i=0; i<device_list.size(); i++) {
		site = device_list.get(i);
%>

<tr>
 <td valign="top" style="padding:4px 10px 1px 10px;"><%=device_list.get(i)%></td>
 <td>
 <table border="0" cellspacing="0" cellpadding="0">
 <%
 
 interface_list = device_details.get(device_list.get(i));
 
 for (int j=0; j<interface_list.size(); j++) {
	 interface_details = interface_list.get(j);
	 site = java.net.URLEncoder.encode(device_list.get(i)+", SNMP Index = "+interface_details.get("nf_si_nsnmpindex")+", Interface "+interface_details.get("nf_si_ninterfaceid"));
	 %>
	 <tr><td style="padding:4px 10px 1px 10px;">
	 <a href="NetFlow.xo?ac=usage1&name=<%=site%>&identifier=<%=interface_details.get("nf_sr_dv_id")%>&i1=<%=interface_details.get("nf_si_ninterfaceid")%>&i2=I">SNMP Index-<%=interface_details.get("nf_si_nsnmpindex")%></a></td></tr>
	 <%
 }

 %>
 </table>
 </td>
</tr>
<%
	}
}
%>
</table>
</td>
<td valign="top">		
<div id="chart1div" style="padding:10px;">Select device/interface to display.</div>
</td>
</tr>
</table>	
</BODY>
<%
if (!name.equals("")) {
%>
<script type="text/javascript">
   var ChartData = "";
   ChartData = "<chart caption='<%=name%>' showPlotBorder='0' showBorder='0' bgAlpha='0,0'  "+
               "dataStreamURL='/rpt/NetFlow.xo%3f<%=url%>' refreshInterval='60' "+
               "decimals='0' yAxisMinValue='0' showRealTimeValue='1' realTimeValuePadding='60'  interactiveLegend='0' "+
               "palette='2' realTimeValuePadding='20' labelDisplay='Rotate' slantLabels='1' "+
               "showRealTimeValue='0' legendBorderThickness='0' legendBorderAlpha='0' >"+
               "<%=request.getAttribute("categories")%>"+
               "<%=request.getAttribute("input_series")%>"+
               "<%=request.getAttribute("output_series")%>"+
               "</chart>";
   var chart1 = new FusionCharts("netflow/RealTimeLine.swf", "ChId1", "600", "400", "0", "0");                
   chart1.setDataXML(ChartData);
   chart1.setTransparent(true);
   chart1.render("chart1div");
</script>
<%
}
%>
</HTML>