<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.HashMap" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="data_constants.jsp" %> 

<%
// Get all inputs need for this page that were passed back on the request 
// as attributes

String type_of_po = (String)request.getAttribute("type_of_po");


%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=(String)po_type.get(type_of_po)%></title>
<style>
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 12px;
    	cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .titleLine {
		font-size : 12pt;
		font-weight : bold;
		text-align : left;
		vertical-align : bottom;
		padding : 0px 0px 6px 0px;
    } 
    .sectionTitleLine {
		font-size : 10pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 0px 0px 6px 0px;
    }
    .sectionTitleLineNav {
		font-size : 8pt;
		font-weight : normal;
		text-align : right;
		vertical-align : middle;
		padding : 0px 0px 0px 0px;
    }
    .sectionHeader1, .sectionHeader2, .sectionHeader3, .sectionHeader4 {
    	font-size : 12pt;
		font-weight : bold;
		text-align : left;
		vertical-align : middle;
		padding : 5px 0px 5px 8px;
		background-color: #e0edf5;
    }
        .sectionHeader2 {
 		background-color: #e5f9de;
    }
        .sectionHeader3 {
		background-color: #fff1f0;
    }
        .sectionHeader4 {
		background-color:  #ffccc9;
    }
    .label1, .label2 {
        font-size : 9pt;
		font-weight : bold;
		vertical-align : top;
		padding : 10px 10px 6px 0px;
    }
    .form1 {
        font-size : 10pt;
		font-weight : normal;
		padding : 0px 0px 0px 0px;
    }
    .elementContainer {
    padding : 0px 2px 0px 2px;
    }
    .columnHeader1 {
		font-size : 9pt;
		font-weight : bold;
		text-align : center;
		vertical-align : bottom;
		padding : 3px 3px 3px 3px;
		background-color: #E0E0DC;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 8pt;
		font-weight : normal;
		text-align : left;
		vertical-align : top;
		padding : 1px 4px 1px 4px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #fffced;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ffffff;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }        
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
		text-align : center;
		width: 26px;
        height: 24px;
    }  
</style>
</head>
<body>
<div class="rptBody01">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr><td class="titleLine"> <%=(String)po_type.get(type_of_po)%> Pay Period: <%=request.getAttribute("start_date")%> to <%=request.getAttribute("end_date")%></td></tr>
 </table>
 </td>
</tr>
 
<tr>
<td>
<br/>
 <table border="0" cellspacing="1" cellpadding="2">
 	
	<% 
	// Get the list of groupings to be displayed
	Vector partner_lists = (Vector)request.getAttribute("partner_lists");
	int n = 0;
	
	// Iterate through each
	for (int m=0; m<partner_lists.size(); m++) {
		
	n = (Integer)partner_lists.get(m); // list reference
	
	Vector partner_list = (Vector)request.getAttribute(partner_list_name[n]);
	
	if (partner_list.size() > 0) {
	
	if (m>0) {
	%>
	<tr> 
	<td colspan="12">&nbsp;</td>
	</tr>
	<%}%>
	<tr> 
	<td colspan="12" class="<%=section_classes[n]%>"><%=section_titles[n]%></td>
	</tr>
	
	<tr>
   	<td rowspan="2" class="columnHeader1">Name</td>
	<td rowspan="2" class="columnHeader1">PO&nbsp;#</td>
	<td rowspan="2" class="columnHeader1">Offsite</td>
	<td rowspan="2" class="columnHeader1">Status</td>
	<td rowspan="2" class="columnHeader1">Owner</td>	
	<td colspan="3" class="columnHeader1">Cost</td>
	<td colspan="3" class="columnHeader1">Cost Breakdown</td>
	<td rowspan="2" class="columnHeader1">Job</td>
	</tr>

	<tr>
    <td class="columnHeader1">&nbspAuthorized&nbsp</td>
	<td class="columnHeader1">&nbspApproved&nbsp</td>
	<td class="columnHeader1">&nbspAmount to Pay&nbsp</td>
	<td class="columnHeader1">&nbsp&nbspLabor&nbsp&nbsp&nbsp</td>
	<td class="columnHeader1">&nbsp&nbspTravel&nbsp&nbsp</td>
	<td class="columnHeader1">&nbspHardware&nbsp</td>
	</tr>
	
    <%
    for (int i=0; i< partner_list.size(); i++) {
	   
	   Map partner_details = (Map)partner_list.get(i);
       String bgc = "";
	   String partner_name = (String)partner_details.get("partner_name");
	   String effective = (String)partner_details.get("effective");
	   String labor = (String)partner_details.get("labor");
	   String hardware = (String)partner_details.get("hardware");
	   String travel = (String)partner_details.get("travel");

	   Vector po_list = (Vector) partner_details.get("po_list");
	   
	   if (i%2 > 0) {
		   bgc = bgc_list[n];
	   } else {
		   bgc = "background-color: #ffffff;";
	   }
    
	   for (int j=0; j<po_list.size(); j++) {
		   String[] po = (String[])po_list.get(j);
		   String link = "http://ilex.contingent.local/Ilex/POWODetailAction.do?viewjobtype=A&ownerId=%&powoid="+po[1]+"&jobid="+po[14];
    %>
	<tr>
	<%if (!partner_name.equals("")) { %>
	<td rowspan="<%=po_list.size()+1%>" nowrap class="cDataLeft1" style="<%=bgc%>"><b><%=partner_name%></b></td>
	<%}%>
	<td class="cDataLeft1" style="<%=bgc%>"><a href="<%=link%>"><%=po[1]%></a><%=po[13]%></td>
	<td class="cDataCenter1" style="<%=bgc%>"><%=po[2]%></td>
	<td class="cDataLeft1" style="<%=bgc%>"><%=po[3]%></td>        
	<td class="cDataLeft1" style="<%=bgc%>"><%=po[4]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[5]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[6]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[7]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[8]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[9]%></td>
	<td class="cDataRight1" style="<%=bgc%>"><%=po[10]%></td>
	<td class="cDataLeft1" style="<%=bgc%>"><%=po[11]%></td>
    </tr>
    <%
        partner_name = "";
	   }
	%>
		
	<tr>
    <td colspan="6" class="cDataRight0" style="<%=bgc%>"><b>Total</b></td>
    <td class="cDataRight0" style="<%=bgc%>"><b><%=effective%></b></td>
    <td class="cDataRight0" style="<%=bgc%>"><b><%=labor%></b></td>
    <td class="cDataRight0" style="<%=bgc%>"><b><%=travel%></b></td>
    <td class="cDataRight0" style="<%=bgc%>"><b><%=hardware%></b></td>
    <td class="cDataLeft0" style="<%=bgc%>">&nbsp;</td>
    </tr>   
	<%
	  }
    %>
    <%
	}
	
	}
    %>
 </table>
</td>
</tr>

<tr>
<td>
<form action="">
<button>sdsdsd</button>
<input type="hidden" name="" value="">
<input type="hidden" name="" value="">
<input type="hidden" name="" value="">
<input type="hidden" name="" value="">
<input type="hidden" name="" value="">
</form>
</td>
</tr>

</table>
</div> 
</body>
</html>