<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style>
body {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	margin: 0px 0px 0px 0px;
	padding: 0px 0px 0px 0px;
	color: #000000;
}

.top {
	margin: 0px 0px 0px 0px;
	padding: 10px 0px 0px 10px;
	border: 0px solid #a9a8ad;
}

.title {
	font-size: 12px; font-weight: bold; color: #CCBE97; text-align: left; vertical-align: bottom; padding: 2px 0px 6px 0px;
}

.ch1a, .ch1b, .ch1c {
	font-size: 11px; font-weight: bold; color: #A8A9AF; text-align: left; vertical-align: bottom; padding: 5px 20px 5px 20px; background-color: #E0E0DC;
}
.ch1b {
	text-align: center;
}
.ch1c {
	text-align: right;
}

/* Basic Cell Format */

.ccaa, .ccab, .ccac {
	font-size: 10px; font-weight: normal; color: #000000; text-align: left; padding: 1px 4px 1px 4px; border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4
}
.ccab {
	text-align: center;
}

.ccac {
	text-align: right;
}

</style>

<script>
<!--


//enter refresh time in "minutes:seconds" Minutes should range from 0 to inifinity. Seconds should range from 0 to 59
var limit="0:59"

if (document.images){
var parselimit=limit.split(":")
parselimit=parselimit[0]*60+parselimit[1]*1
}
function beginrefresh(){
if (!document.images)
return
if (parselimit==1)
window.location.reload()
else{ 
parselimit-=1
curmin=Math.floor(parselimit/60)
cursec=parselimit%60
if (curmin!=0)
curtime=curmin+" minutes and "+cursec+" seconds left until page refresh!"
else
curtime=cursec+" seconds left until page refresh!"
window.status=curtime
setTimeout("beginrefresh()",1000)
}
}

window.onload=beginrefresh
//-->
</script>
<body>
<div class="top">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
  <td><div class="title" style="padding: 6px 0px 6px 0px; border-bottom: 0px solid #a9a8ad">Current Outages</div></td>
<tr>
<tr>
  <td><div style="padding: 6px 0px 6px 0px; border-bottom: 1px solid #a9a8ad"></div></td>
<tr>
<tr>
  <td>
  <table border="0" cellpadding="1" cellspacing="0">
  <tr>
    <td class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Customer</td>
    <td class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Appendix</td>
    <td class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Ilex Site Number</td>
    <td class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">WUG Site Number</td>
    <td class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Status</td>
    <td class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Down Point</td>
    <td class="ch1b" style="border-bottom: 1px solid #BFBEC4">Action</td>
  </tr>
  <%=request.getAttribute("outage_table")%>
  </table>
  </td>
<tr>
</table>
</div>
</body>
</html>
