<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ page import = "java.util.Vector" %>

<%
String[] stats = (String[]) request.getAttribute("stats");

String type = (String) request.getAttribute("type");
String select1 = "";
String select2 = "";
String select3 = "";
String meta = "";

//String url = "localhost";
String url = "ilex.contingent.local";

if (type.equals("3")) {
	select3 = " checked";
	meta = "<meta http-equiv=\"refresh\" content=\"180;url=http://"+url+"/rpt/MSPMap.xo?action=msp_map&#38;type=1\">";
} else if (type.equals("2")) {
	select2 = " checked";
	meta = "<meta http-equiv=\"refresh\" content=\"120;url=http://"+url+"/rpt/MSPMap.xo?action=msp_map&#38;type=3\">";
} else  {
	select1 = " checked";
	meta = "<meta http-equiv=\"refresh\" content=\"120;url=http://"+url+"/rpt/MSPMap.xo?action=msp_map&#38;type=2\">";
} 
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Monitoring Summary</title>

<!-- production key ABQIAAAAk17xvRffek5XvGJR1KDgbxSwRy4s2eep2DvUSxAFcnO47vT-IhSMuP2GfucbtY2TSiiIcVngO9uULQ -->
<!-- local key ABQIAAAAk17xvRffek5XvGJR1KDgbxT2yXp_ZAY8_ufC3CFXhHIE1NvwkxQnAVYWFAbtuuMPaTYX5TsmaVhu0w  -->
  
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAk17xvRffek5XvGJR1KDgbxSwRy4s2eep2DvUSxAFcnO47vT-IhSMuP2GfucbtY2TSiiIcVngO9uULQ" type="text/javascript"></script>
<link href="/rpt/msp/style.css" type="text/css" rel="stylesheet" />
<link href="/rpt/msp/content.css" rel="stylesheet" type="text/css" />
<link href="/rpt/msp/summary.css" rel="stylesheet" type="text/css" />

<style>
body {
    font-family : Arial, Helvetica, sans-serif;
    font-size : 12px;
    cursor : auto;
    }
.rptBody01 {
	padding : 10px 0px 0px 10px;
	align: center;
    }
.Ntextoleftalignnowrappaleyellowborder2 {
	white-space:nowrap;  
	font-size: 10px; 
	font-family: 'Verdana', Arial, Helvetica, 'sans-serif';
	font-weight:normal;
	padding: 2px 1px ( left, right );
	color: #000000; 
	background: #FAF8CC;
	border-color : #FFFFFF #BDBFC1 #849DC0 #849DC0;
	border-style : solid;
	border-top-width : 0px;
	border-right-width : 0px;
	border-bottom-width : 0px;
	border-left-width :0px;
	padding-left:0	
	}
	.BlueTab{ 
	background-color:#dbe4f1; 
	border-top : 1px solid #BDBFC1; 
	padding: 8px 4px 8px 4px;
    }	    
    .FormTab{ 
	background-color:#dbe4f1; 
	border : 1px solid #BDBFC1; 
	margin-left : 82px;
	padding: 1px 1px 1px 1px;
    }
	.Legend{
    font-size: 10px; 
	font-family: 'Verdana', Arial, Helvetica, 'sans-serif';
	font-weight:normal;
	color: gray;
	padding: 4px 0px 0px 8px;
    }	    
    
</style>
<script type="text/javascript">
   
function initialize() {
   if (GBrowserIsCompatible()) 
     {        
       var map = new GMap2(document.getElementById("map1"));
       
       map.addControl(new GLargeMapControl());
       map.addControl(new GScaleControl());
       map.addControl(new GMapTypeControl());
       
       map.setCenter(new GLatLng(38.81362, -94.554209), 4);
             
       var mapIcon1 = new GIcon(G_DEFAULT_ICON);
       mapIcon1.image = "http://ilex.contingent.local/rpt/msp/images/green.png"; 
       mapIcon1.iconSize = new GSize(18, 18);
       mapIcon1.shadow = "http://ilex.contingent.local/rpt/mm/shadow.png";
       mapIcon1.shadowSize = new GSize(29, 22);
       markerOptions1 = { icon:mapIcon1 };
       
       var mapIcon2 = new GIcon(G_DEFAULT_ICON);
       mapIcon2.image = "http://ilex.contingent.local/rpt/msp/images/yellow.png"; 
       mapIcon2.iconSize = new GSize(18, 18);
       mapIcon2.shadow = "http://ilex.contingent.local/rpt/mm/shadow.png";
       mapIcon2.shadowSize = new GSize(29, 22);
       markerOptions2 = { icon:mapIcon2 };  

       var mapIcon3 = new GIcon(G_DEFAULT_ICON);
       mapIcon3.image =  "http://ilex.contingent.local/rpt/msp/images/red.png"; 
       mapIcon3.iconSize = new GSize(18, 18);
       mapIcon3.shadow = "http://ilex.contingent.local/rpt/mm/shadow.png";
       mapIcon3.shadowSize = new GSize(29, 22);
       markerOptions3 = { icon:mapIcon3 };  
     
       <%
         Vector points = (Vector)request.getAttribute("points");
       
         for (int i=0; i<points.size(); i++) {
     	   String[] pt = (String[])points.get(i);
       %>
         map.addOverlay(new GMarker(new GLatLng(<%=pt[3]%>,<%=pt[4]%>), markerOptions<%=pt[5]%>));
       <%
       }
	   %>
     } 
}    
</script>
</head>
<body onload="initialize()">
<form action="/rpt/MSPMap.xo" method="post">
<input type="hidden" name="action" value="msp_map">
<table align="center" border="0" cellpadding="0" cellspacing="0"
	width="100%">
	<tr>
		<td align="center">

		<table border="0" cellpadding="0" cellspacing="0">
		
			<tr><td>
			<table border="0" cellpadding="0" cellspacing="0"><tr>
			<td style="background-color:#b5c8d9;"><img src="http://ilex.contingent.local/rpt/msp/images/activities.jpg" align="left">
			<td style="background-color:#b5c8d9;"><img src="http://ilex.contingent.local/rpt/msp/images/proposals.jpg" align="left">
			<td style="background-color:#b5c8d9;"><img src="http://ilex.contingent.local/rpt/msp/images/projects.jpg" align="left">
			<td style="background-color:#b5c8d9;"><img src="http://ilex.contingent.local/rpt/msp/images/netmedx.gif" align="left">
			<td style="background-color:#b5c8d9;"><img src="http://ilex.contingent.local/rpt/msp/images/accounting.jpg" align="left">
			<td style="background-color:#b5c8d9;"><img src="http://ilex.contingent.local/rpt/msp/images/pvs.jpg" align="left">
			<td style="background-color:#b5c8d9;"><img src="http://ilex.contingent.local/rpt/msp/images/contacts.jpg" align="left">
			<td style="background-color:#b5c8d9;"><img src="http://ilex.contingent.local/rpt/msp/images/masters.jpg" align="left">
			<td style="background-color:#b5c8d9;"><img src="http://ilex.contingent.local/rpt/msp/images/users.jpg" align="left">
			<td style="background-color:#b5c8d9;"><img src="http://ilex.contingent.local/rpt/msp/images/resources.jpg" align="left">
			<td style="background-color:#b5c8d9;"><img src="http://ilex.contingent.local/rpt/msp/images/Documents.jpg" align="left">
			<td width="100%" style="background-color:#b5c8d9;"><img src="http://ilex.contingent.local/rpt/msp/images/logo1.jpg" align="right"></td>
			</tr></table>
			</td></tr>
		
			<tr>
				<td class="Ntextoleftalignnowrappaleyellowborder2"  align="left" style="padding-bottom:4px; padding-top:4px">
				<table border="0" cellpadding="0" cellspacing="5" class="Ntextoleftalignnowrappaleyellowborder2">
					<tr>
						<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>&nbsp;</td>
								<td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Active&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
								<td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Down&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
								<td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;Initial Assessment&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
								<td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Help&nbsp;Desk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
								<td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;MSP Dispatch&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
								<td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Watch&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
								<td class="texthdNoBorder"><b>&nbsp;&nbsp;&nbsp;&nbsp;Bounces&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
								<td rowspan="2">
								 <div class = "FormTab" >
								 <table cellpadding="0" cellspacing="0" border="0">
								 <tr><td>&nbsp;All:</td><td><input type="radio" name="type" value="1" style="height:14px" onclick="this.form.submit()"<%=select1%>></td></tr>
								 <tr><td>&nbsp;Bouncing:</td><td><input type="radio" name="type" value="2" style="height:14px" onclick="this.form.submit()"<%=select2%>></td></tr>
								 <tr><td>&nbsp;Down:</td><td><input type="radio" name="type" value="3" style="height:14px" onclick="this.form.submit()"<%=select3%>></td></tr>
								 </table>
								 </div>								
								</td>
								
							</tr>
							<tr>
								<td class="dbvaluesmallFont">&nbsp;&nbsp;&nbsp;<b>Site</b>:&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td class="Ntextoleftalignnowrap" align="center"><%=stats[0]%></td>
								<td class="Ntextoleftalignnowrap" align="center"><%=stats[1]%></td>
								<td class="Ntextoleftalignnowrap" align="center"><%=stats[2]%></td>
								<td class="Ntextoleftalignnowrap" align="center"><%=stats[3]%></td>
								<td class="Ntextoleftalignnowrap" align="center"><%=stats[4]%></td>
								<td class="Ntextoleftalignnowrap" align="center">24</td>
								<td class="Ntextoleftalignnowrap" align="center"><%=stats[5]%></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>

			<tr>
				<td valign="top" align="left">
				<div class="BlueTab"><div id="map1" style="width: 950px; height: 560px;"></div>
				<table cellpadding="0" cellspacing="0" border="0" class="Legend">
				<tr>
				<td><img src="http://ilex.contingent.local/rpt/msp/images/green_legend.png" style="border : 1px solid #BDBFC1"></td><td>Up&nbsp;&nbsp;</td>
				<td><img src="http://ilex.contingent.local/rpt/msp/images/yellow_legend.png" style="border : 1px solid #BDBFC1"></td><td>Bounced > 5 Within Last 24 Hours&nbsp;&nbsp;</td>
				<td><img src="http://ilex.contingent.local/rpt/msp/images/red_legend.png" style="border : 1px solid #BDBFC1"></td><td>Down, Initial Assessment, Help Desk, or MSP Dispatch</td></tr>
				</table>
				</div>
				</td>
				</div>
				</td>
			</tr>
			
			
		</table>

		</td>
	</tr>
</table>
</form>
</body>
</html>


















