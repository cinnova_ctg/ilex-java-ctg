<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style>
body {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	color: #000000;
}
.top,.select1,.oob1,.main1,.kp1,.kp2 {
	border: 2px solid #ffffff;
	border: 2px solid #FEFFF5;
	border: 12px solid #ffffff;
	margin: 0px 0px 0px 0px;
	background-color: #FEFFF5;
	background-color: #ffffff;
}
.outage {
	width: 412px;
	margin-right: 10px;
	margin-bottom: 10px;
	padding: 4px 4px 4px 4px;
	border: 1px solid #a9a8ad;
}
.sitedetails {
	width: 412px;
	margin-right: 10px;
	margin-bottom: 10px;
	padding: 4px 4px 4px 4px;
	border: 1px solid #a9a8ad;
}
.servicedetails {
	width: 412px;
	margin-right: 10px;
	margin-bottom: 10px;
	padding: 4px 4px 4px 4px;
	border: 1px solid #a9a8ad;
}
.stats {
	width: 512px;
	margin-bottom: 10px;
	padding: 4px 4px 4px 4px;
	border: 1px solid #a9a8ad;
}
.pictures {
	margin: 50px;
	padding: 8px 8px 8px 8px;
	border: 1px solid #a9a8ad;
}
.title1 {
	font-size: 14px;
	font-weight: bold;
	color: #CCBE97;
	text-align: left;
	padding: 0px 0px 10px 0px;
}
.label1 {
	font-size: 12px;
	font-weight: bold;
	color: #000000;
	text-align: left;
	padding: 4px 0px 0px 0px;
}
.columnheader1 {
	font-size: 12px;
	font-weight: bold;
	color: #000000;
	text-align: center;
	padding: 4px 4px 4px 4px;
}
.text1 {
	font-size: 12px;
	font-weight: normal;
	color: #000000;
	text-align: right;
	padding: 4px 2px 4px 4px;
}
.chart1	 {
	font-size: 12px;
	font-weight: normal;
	color: #000000;
	text-align: left;
	padding: 25px 10px 10px 25px;
}

.ch1a, .ch1b, .ch1c {
	font-size: 11px; font-weight: bold; color: #A8A9AF; text-align: left; vertical-align: bottom; padding: 5px 15px 5px 15px; background-color: #ffffff;
}
.ch1b {
	text-align: center;
}
.ch1c {
	text-align: right;
}

/* Basic Cell Format */

.ccaa, .ccab, .ccac {
	font-size: 10px; font-weight: normal; color: #000000; text-align: left; padding: 2px 6px 2px 4px; border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4
}
.ccab {
	text-align: center;
}

.ccac {
	text-align: right
}
.ccba, .ccbb, .ccbc {
	font-size: 10px; font-weight: normal; color: #000000; text-align: left; padding: 2px 6px 2px 4px; border-right: 0px solid #BFBEC4; border-bottom: 0px solid #BFBEC4
}
.ccbb {
	text-align: center;
}

.ccbc {
	text-align: right
}
</style>
<body>
<div class="top">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
  <td><div class="title1" style="padding: 6px 0px 6px 0px; border-bottom: 0px solid #a9a8ad">Outage Summary by Customer</div></td>
<tr>
<tr>
<td>
<%=request.getAttribute("msp_outages")%>
</td>
</tr>
<!-- Image -->
<tr><td><div class="chart1"><img id="1" src="msp/blank.gif"/></div></td></tr>
</table>
</div>
</body>
<script type="text/javascript">
function changeSrc(image_src)
{
document.getElementById("1").src=image_src;
}
</script>
</html>