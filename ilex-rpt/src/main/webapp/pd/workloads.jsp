<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
// Get report data
String[][] m = (String[][])request.getAttribute("matrix");
int rows = 7;
int columns = 11;
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Workload</title>

<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<%@ include file="js.txt"%>

</head>
<body>
<div class="rptBody01"><table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table width="650" border="0" cellspacing="0" cellpadding="2">
<form name="form1" action="WK.xo?ac=workload1" method="post">
<input type="hidden" name="fn", value="qrtcom1">
<input type="hidden" name="page_type", value="summary2">
  <tr>
  <td rowspan="3"class="titleLine">Workload/User Actions</td>
  <td align="right" class="cLinkRight1"></td>
  </tr>
  <tr>
  <td align="right">Job Type:&nbsp;&nbsp;<el:element type="menu1" request="<%=request%>" name="job_type" source="" data="All~Standard~NetMedX~MSP~Help Desk" js=""/></td>
  </tr>
  <tr>
  <td align="right">
  <input type="submit" name="default_view" value="Go" class=date1></td>
  </tr>
  </form>
 </table>
</td></tr><tr>
<td class="sectionTitleLine">Workload</td>
</tr>
<tr>
<td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">

    <tr>
     <td width="80" rowspan="2" class="columnHeader2">&nbsp;</td>
     <td colspan="7" class="columnHeader2">Trailing 7 Days</td>
    </tr>
   
   <tr>
     <td width="60" class="columnHeader2"><%=m[0][0]%></td>
     <td width="60" class="columnHeader2"><%=m[1][0]%></td>
     <td width="60" class="columnHeader2"><%=m[2][0]%></td>
     <td width="60" class="columnHeader2"><%=m[3][0]%></td>
     <td width="60" class="columnHeader2"><%=m[4][0]%></td>
     <td width="60" class="columnHeader2"><%=m[5][0]%></td>
     <td width="60" class="columnHeader2"><%=m[6][0]%></td>
   </tr>
   
   <tr>
	<td class="cDataLeft0">Customers</td>
	<td class="cDataRight0"><%=m[0][2]%></td>
	<td class="cDataRight0"><%=m[1][2]%></td>
	<td class="cDataRight0"><%=m[2][2]%></td>
	<td class="cDataRight0"><%=m[3][2]%></td>
	<td class="cDataRight0"><%=m[4][2]%></td>
	<td class="cDataRight0"><%=m[5][2]%></td>
	<td class="cDataRight0"><%=m[6][2]%></td>
  </tr>
  
   <tr>
	<td class="cDataLeft1">Appendices</td>
	<td class="cDataRight1"><%=m[0][3]%></td>
	<td class="cDataRight1"><%=m[1][3]%></td>
	<td class="cDataRight1"><%=m[2][3]%></td>
	<td class="cDataRight1"><%=m[3][3]%></td>
	<td class="cDataRight1"><%=m[4][3]%></td>
	<td class="cDataRight1"><%=m[5][3]%></td>
	<td class="cDataRight1"><%=m[6][3]%></td>
  </tr>
  
   <tr>
	<td class="cDataLeft0">Jobs</td>
	<td class="cDataRight0"><%=m[0][4]%></td>
	<td class="cDataRight0"><%=m[1][4]%></td>
	<td class="cDataRight0"><%=m[2][4]%></td>
	<td class="cDataRight0"><%=m[3][4]%></td>
	<td class="cDataRight0"><%=m[4][4]%></td>
	<td class="cDataRight0"><%=m[5][4]%></td>
	<td class="cDataRight0"><%=m[6][4]%></td>
  </tr>
  
  
</table></td>
</tr>
<tr>
<td class="sectionTitleLine">Users</td>
</tr><tr>
<td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">

    <tr>
     <td width="80" rowspan="2" class="columnHeader2">Timeframe</td>
     <td colspan="7" class="columnHeader2">Trailing 7 Days</td>
    </tr>
   
   <tr>
     <td width="60" class="columnHeader2"><%=m[0][0]%></td>
     <td width="60" class="columnHeader2"><%=m[1][0]%></td>
     <td width="60" class="columnHeader2"><%=m[2][0]%></td>
     <td width="60" class="columnHeader2"><%=m[3][0]%></td>
     <td width="60" class="columnHeader2"><%=m[4][0]%></td>
     <td width="60" class="columnHeader2"><%=m[5][0]%></td>
     <td width="60" class="columnHeader2"><%=m[6][0]%></td>
   </tr>
    
   <tr>
	<td class="cDataLeft0">PPS</td>
	<td class="cDataRight0"><%=m[0][5]%></td>
	<td class="cDataRight0"><%=m[1][5]%></td>
	<td class="cDataRight0"><%=m[2][5]%></td>
	<td class="cDataRight0"><%=m[3][5]%></td>
	<td class="cDataRight0"><%=m[4][5]%></td>
	<td class="cDataRight0"><%=m[5][5]%></td>
	<td class="cDataRight0"><%=m[6][5]%></td>
  </tr>
  
   <tr>
	<td class="cDataLeft1">Non-PPS</td>
	<td class="cDataRight1"><%=m[0][6]%></td>
	<td class="cDataRight1"><%=m[1][6]%></td>
	<td class="cDataRight1"><%=m[2][6]%></td>
	<td class="cDataRight1"><%=m[3][6]%></td>
	<td class="cDataRight1"><%=m[4][6]%></td>
	<td class="cDataRight1"><%=m[5][6]%></td>
	<td class="cDataRight1"><%=m[6][6]%></td>
  </tr>
  
</table></td>
</tr>
<tr>
<td class="sectionTitleLine">Actions Taken</td>
</tr><tr>
<td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">

    <tr>
     <td width="80" rowspan="2" class="columnHeader2">Category</td>
     <td colspan="7" class="columnHeader2">Trailing 7 Days</td>
    </tr>
   
    <tr>
     <td width="60" class="columnHeader2"><%=m[0][0]%></td>
     <td width="60" class="columnHeader2"><%=m[1][0]%></td>
     <td width="60" class="columnHeader2"><%=m[2][0]%></td>
     <td width="60" class="columnHeader2"><%=m[3][0]%></td>
     <td width="60" class="columnHeader2"><%=m[4][0]%></td>
     <td width="60" class="columnHeader2"><%=m[5][0]%></td>
     <td width="60" class="columnHeader2"><%=m[6][0]%></td>
   </tr>
    
   <tr>
     <td colspan="8" class="columnHeader1">PPS</td>
    </tr>
   
  <tr>
	<td class="cDataLeft0">Job&nbsp;Management</td>
	<td class="cDataRight0"><%=m[0][7]%></td>
	<td class="cDataRight0"><%=m[1][7]%></td>
	<td class="cDataRight0"><%=m[2][7]%></td>
	<td class="cDataRight0"><%=m[3][7]%></td>
	<td class="cDataRight0"><%=m[4][7]%></td>
	<td class="cDataRight0"><%=m[5][7]%></td>
	<td class="cDataRight0"><%=m[6][7]%></td>
  </tr>   
   <tr>
	<td class="cDataLeft1">PO&nbsp;Management</td>
	<td class="cDataRight1"><%=m[0][8]%></td>
	<td class="cDataRight1"><%=m[1][8]%></td>
	<td class="cDataRight1"><%=m[2][8]%></td>
	<td class="cDataRight1"><%=m[3][8]%></td>
	<td class="cDataRight1"><%=m[4][8]%></td>
	<td class="cDataRight1"><%=m[5][8]%></td>
	<td class="cDataRight1"><%=m[6][8]%></td>
  </tr>  
  <tr>
     <td colspan="8" class="columnHeader1">non-PPS</td>
    </tr>

  <tr>
   <td class="cDataLeft0">Job&nbsp;Management</td>
	<td class="cDataRight0"><%=m[0][9]%></td>
	<td class="cDataRight0"><%=m[1][9]%></td>
	<td class="cDataRight0"><%=m[2][9]%></td>
	<td class="cDataRight0"><%=m[3][9]%></td>
	<td class="cDataRight0"><%=m[4][9]%></td>
	<td class="cDataRight0"><%=m[5][9]%></td>
	<td class="cDataRight0"><%=m[6][9]%></td>
  </tr>   
   <tr>
	<td class="cDataLeft1">PO&nbsp;Management</td>
	<td class="cDataRight1"><%=m[0][10]%></td>
	<td class="cDataRight1"><%=m[1][10]%></td>
	<td class="cDataRight1"><%=m[2][10]%></td>
	<td class="cDataRight1"><%=m[3][10]%></td>
	<td class="cDataRight1"><%=m[4][10]%></td>
	<td class="cDataRight1"><%=m[5][10]%></td>
	<td class="cDataRight1"><%=m[6][10]%></td>
  </tr>
  
 
  
</table></td>
</tr>
<tr><td>
  <table  width="650" border="0" cellspacing="1" cellpadding="0">
  <tr><td>&nbsp;</td></tr>
  <tr><td><b>Overview</b></td></tr>
  <tr><td>Counts are determined on a day basis.  For instance, a customer count of 50 means there were user actions, job or PO, for 50 distinct customers.</td></tr>
  <tr><td>The report shows the trailing 7 days of counts.</td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr><td><b>Workload</b></td></tr>
  <tr><td>Customers - Count of unique customers managed.</td></tr>
  <tr><td>Appendices - Count of unique appendices managed.</td></tr>
  <tr><td>Jobs - Count of jobs managed. Users and actions are counted for these jobs.</td></tr>
  
  <tr><td>&nbsp;</td></tr>
  <tr><td><b>Users</b></td></tr>
  <tr><td>PPS - Count of unique users taking actions in the the PPS timeframe listed below.</td></tr>
  <tr><td>non-PPS - Count of unique users taking actions in the the non-PPS timeframe listed below.</td></tr>
  
  <tr><td>&nbsp;</td></tr>
  <tr><td><b>Actions</b></td></tr>
  <tr><td>Job Management - Following the creation of a job, these are the actions required to execute and close a job.  These are broken out by timeframe.</td></tr>
  <tr><td>PO Management - Following the creation of a PO, these are the actions required to execute and complete a PO.  These are broken out by timeframe.</td></tr>
  
  <tr><td>&nbsp;</td></tr>
  <tr><td><b>PPS vs non-PPS</b></td></tr>
  <tr><td>PPS - For this report, PPS is defined to be all user actions between 7:00AM and 7:59PM</td></tr>
  <tr><td>non-PPS - For this report, PPS is defined to be all user actions between 12:00AM to 6:59AM and 8:00PM and 11:59PM.  Note these periods apply to same day, not cross across days.</td></tr>
  
  </table>
</td></tr></table></div>
</body>
</html>