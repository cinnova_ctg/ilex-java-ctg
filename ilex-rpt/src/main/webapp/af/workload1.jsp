<HTML>
<HEAD>
<TITLE>Productivity</TITLE>
<title>Quarterly Commission Summary</title>
<%@ include file="styles.txt"%>
<script type="text/javascript" src="af/FusionCharts.js"></script>
</HEAD>
<BODY>
<CENTER>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
    <tr>
     <td width="70" class="columnHeader2" rowspan="2">Period</td>
     <td width="80" class="columnHeader2" rowspan="2">Customer</td>
     <td width="80" class="columnHeader2" rowspan="2">Appendix</td>
     <td width="70" rowspan="2" class="columnHeader2">Jobs</td>
     <td colspan="2" class="columnHeader2">PM/PC</td>
     <td colspan="2" class="columnHeader2">Actions</td>
     <td rowspan="2" class="columnHeader2">Total Actions</td>
   </tr>
   
   <tr>
     <td width="80" class="columnHeader2">PPS</td>
     <td width="80" class="columnHeader2">Non-PPS</td>
     <td width="80" class="columnHeader2">PPS</td>
     <td width="80" class="columnHeader2">Non-PPS</td>
   </tr>
   
   			<tr>
				<td class="cDataCenter0">24 Hours</td>
				<td class="cDataRight0">63</td>
				<td class="cDataRight0">90</td>
				<td class="cDataRight0">380</td>
				<td class="cDataRight0">41</td>
				<td class="cDataRight0">4</td>
				<td class="cDataRight0">1585</td>
				<td class="cDataRight0">51</td>
				<td class="cDataRight0">1636</td>
			</tr>
    		<tr>
				<td class="cDataCenter1">Week</td>
				<td class="cDataRight1">80</td>
				<td class="cDataRight1">129</td>
				<td class="cDataRight1">1118</td>
				<td class="cDataRight1">54</td>
				<td class="cDataRight1">13</td>
				<td class="cDataRight1">6943</td>
				<td class="cDataRight1">518</td>
				<td class="cDataRight1">7461</td>
			</tr>
  			<tr>
				<td class="cDataCenter0">Months</td>
				<td class="cDataRight0">0</td>
				<td class="cDataRight0">0</td>
				<td class="cDataRight0">0</td>
				<td class="cDataRight0">0</td>
				<td class="cDataRight0">0</td>
				<td class="cDataRight0">0</td>
				<td class="cDataRight0">0</td>
				<td class="cDataRight0">0</td>
			</tr>
   
</table>
<div id="chart1div">
  This text is replaced by the Flash movie.
</div>
<script type="text/javascript">
   var ChartData = "";
   ChartData = "<chart caption='User Actions' showPlotBorder='0' "+
               "showBorder='0' bgAlpha='0,0'  "+
               "dataStreamURL='/rpt/Workload.xo%3fac%3dgetWorkloadDataStream' refreshInterval='300' "+
               "decimals='0' yAxisMinValue='0' showRealTimeValue='1' realTimeValuePadding='50'  interactiveLegend='1' "+
               "palette='2' realTimeValuePadding='20' labelDisplay='Rotate' slantLabels='1' "+
               "showRealTimeValue='0' legendBorderThickness='0' legendBorderAlpha='0' >"+
               "<categories></categories>"+
               "<dataset showValues='0' seriesName='Job Status'></dataset>"+
               "<dataset showValues='0' seriesName='Core Job'></dataset>"+
               "<dataset showValues='0' seriesName='Scheduling'></dataset>"+
                "<dataset showValues='0' seriesName='Purchase Order'></dataset>"+
               "</chart>";
   var chart1 = new FusionCharts("af/RealTimeLine.swf", "ChId1", "600", "400", "0", "0");
   //chart1.setDataURL('/rpt/Workload.xo?ac=setDataXML&t=<%=request.getAttribute("uniqueness")%>');                  
   chart1.setDataXML(ChartData);
   chart1.setTransparent(true);
   chart1.render("chart1div");
</script>
</CENTER>
</BODY>
</HTML>