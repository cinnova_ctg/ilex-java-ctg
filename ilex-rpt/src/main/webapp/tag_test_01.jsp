<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.net.URLEncoder" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%

String sql = "select distinct ap_as_customer from ap_account_summary order by ap_as_customer";

Vector rows = (Vector)request.getAttribute("rows");
int job_count = 0;
if (rows != null) {
	job_count = rows.size();
}
String report_type = (String)request.getAttribute("report_type");
String accountName = (String)request.getAttribute("account");
String job_status = (String)request.getAttribute("job_status");
if (job_status == null) {
	job_status = "";
}
String e_accountName = "";

if (!accountName.equals("")) {
	e_accountName = URLEncoder.encode(accountName);
	accountName = " - Account: "+accountName;
}
else {
	accountName = "";
}

String href = "<a href=\"/rpt/MngRpt.xo?form_action=managementReports&account="+e_accountName+"&report_type=XX\">YY</a>";

String href_all = href.replaceFirst("XX", "all");
       href_all = href_all.replaceFirst("YY", "All");
  
String href_notes = href.replaceFirst("XX", "notes");
       href_notes = href_notes.replaceFirst("YY", "Notes");     
       
       String href_site = href.replaceFirst("XX", "site");
       href_site = href_site.replaceFirst("YY", "Site Information");     
       
       String href_price = href.replaceFirst("XX", "price");
       href_price = href_price.replaceFirst("YY", "Price");  
       
       String href_customer = href.replaceFirst("XX", "customer");
       href_customer = href_customer.replaceFirst("YY", "Customer Reference");
       
       String href_vgp = href.replaceFirst("XX", "vgp");
       href_vgp = href_vgp.replaceFirst("YY", "VGP Out of Limits"); 
       
       String href_flagged = href.replaceFirst("XX", "flagged");
       href_flagged = href_flagged.replaceFirst("YY", "Flagged Bad");

//All | Notes | Site Information | Price | Customer Reference | VGP Out of Limits or Flagged Bad 

String title = "Complete";
String subtitle = "All Jobs";

if (request.getAttribute("title") != null) {
	title = (String)request.getAttribute("title");
}
if (request.getAttribute("subtitle") != null) {
	subtitle = (String)request.getAttribute("subtitle");
}

String cost = (String)request.getAttribute("cost");
String price = (String)request.getAttribute("price");

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 12px;
    	cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .rptBody02 {
		 padding : 10px 0px 0px 20px;
    }
    .titleLine {
		font-size : 12pt;
		font-weight : bold;
		text-align : left;
		vertical-align : bottom;
		padding : 0px 0px 6px 0px;
    } 
    .sectionTitleLine {
		font-size : 10pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 0px 0px 6px 0px;
    }
    .sectionTitleLineNav {
		font-size : 8pt;
		font-weight : normal;
		text-align : right;
		vertical-align : middle;
		padding : 0px 0px 0px 0px;
    }
    .label1, .label2 {
        font-size : 10pt;
		font-weight : normal;
		text-align : left;
		vertical-align : top;
		padding : 0px 10px 8px 0px;
    }
    .columnHeader1 {
		font-size : 9pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 3px 10px 3px 10px;
		background-color: #E0E0DC;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 8pt;
		font-weight : normal;
		text-align : left;
		vertical-align : middle;
		padding : 1px 4px 1px 4px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #e5f9de;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ffffff;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }        
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 1px 1px 1px 1px;
		text-align : center;
		vertical-align : middle;
		width: 25px;
        height: 24px;
    }  
</style>
<script type="text/javascript" language="JavaScript">
</script>
</head>
<body>
<div class="rptBody01">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
 <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
     <tr>
     <td class="titleLine">Monday Morning: <%=title %></td>
     <form method="post" action="/rpt/MngRpt.xo?form_action=managementReports">
     <td align="right"><el:element type="menu1" request="<%=request%>" name="account" source="database" data ="<%=sql%>" js="onChange=this.form.submit()"/>&nbsp;&nbsp;</td>
     <input type="hidden" name="form_action" value="managementReports">
     <input type="hidden" name="report_type" value="<%=report_type %>">
     <input type="hidden" name="job_status" value="<%=job_status %>">
     </form>
     </tr>
     </table>
 </td>
</tr>

<tr>
 <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
     <tr>
     <td class="sectionTitleLine"><%=subtitle %> <%=accountName %></td>
     <%
     if (title.equals("Complete"))
     {
     %>
     <td class="sectionTitleLineNav">Missing: <%=href_all%> | <%=href_notes%> | <%=href_site%> | <%=href_price%> | <%=href_customer%> | <%=href_vgp%> or Flagged Bad </td>
     <%
     }
     %>
     </tr>
     </table>
 </td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0">
     <tr>
     <td class="label1"><strong>Totals</strong></td>
     <td class="label1">Jobs:  <%=job_count %></td>
     <td class="label1">Actual Cost:  <%=cost %></td>
     <td class="label1">Price: <%=price %></td>
     </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
 <td>
 <table width="100%" border="0" cellspacing="1" cellpadding="0">
     <tr>
     <td class="columnHeader1">Account</td>
     <td class="columnHeader1">Project</td>
     <td class="columnHeader1">Job</td>
     <td class="columnHeader1">Start</td>
     <td class="columnHeader1">End</td>
     <td class="columnHeader1">Project Manager</td>
     <td class="columnHeader1">Owner</td>
     <td class="columnHeader1">Actual Cost</td>
     <td class="columnHeader1">Price</td>
     <td class="columnHeader1">VGP</td>
     </tr>
     
     <% 
     for (int i=0; i<rows.size(); i++) {
    	 String[] r = (String[])rows.get(i);
     %>
     <tr>
     <td class="cDataLeft<%=i%2%>"><%=r[1]%></td>
     <td class="cDataLeft<%=i%2%>"><%=r[0]%></td>
     <td class="cDataLeft<%=i%2%>"><%=r[2]%></td>
     <td class="cDataCenter<%=i%2%>"><%=r[8]%></td>
     <td class="cDataCenter<%=i%2%>"><%=r[9]%></td>
     <td class="cDataCenter<%=i%2%>"><%=r[3]%></td>
     <td class="cDataCenter<%=i%2%>"><%=r[4]%></td>
     <td class="cDataRight<%=i%2%>"><%=r[5]%></td>
     <td class="cDataRight<%=i%2%>"><%=r[6]%></td>
     <td class="cDataRight<%=i%2%>"><%=r[7]%></td>
     </tr>
     <%
     }
     %>     
     
 </table>
 </td>
</tr>

</table>
</div> 
</body>
</html>