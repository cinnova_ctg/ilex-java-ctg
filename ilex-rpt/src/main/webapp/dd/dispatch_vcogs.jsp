<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.Vector" %>
<%
Vector cca_list = (Vector)request.getAttribute("cca_list");
String cca = "";
Map vcogs = (Map)request.getAttribute("vcogs");

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="refresh" content="900">
<title>Dispatch Summary by Ticket Owner</title>
<style>
    body {
    		font-family : Arial, Helvetica, sans-serif;
    		font-size : 12px;
    		cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .pageTitle {
		font-size : 15pt;
		font-weight : bold;
		text-align : center;
		padding : 12px 0px 12px 0px;
    } 
    .titleLine {
		font-size : 11pt;
		font-weight : bold;
		text-align : left;
		padding : 0px 0px 0px 0px;
        vertical-align : top;
    } 
    .sectionTitleLine {
		font-size : 10pt;
		font-weight : bold;
		text-align : right;
		padding : 0px 0px 0px 0px;
        vertical-align : bottom;
    }    
    .labelsLeft, .labelsCenter, .labelsRight {
		font-size : 9pt;
		font-weight : bold;
		padding : 5px 0px 0px 0px;
        vertical-align : top;
    }    

    .labelsCenter {
		text-align : center;
    }    
    .forecast, .highprobability, .recognized, .delta  {
		font-size : 8pt;
		font-weight : normal;
		text-align : center;
		padding : 2px 1px 2px 1px;
		border-style: solid;
        border-width: 1px
    }
    .forecast {
		background-color : #ffccc9;
    }  
    .highprobability {
		background-color : #e5f9de;
    }  
    .recognized {
		background-color : #e0edf5;		 
    }     
    .delta {
		background-color : #ffffff;
		text-align : right;
    }
    .dataLineLeft, .dataLineCenter, .dataLineRight,{
		font-size : 9pt;
		font-weight : normal;
		padding : 2px 0px 6px 0px;
    }
    .dataLineCenter {
		text-align : center;
    }
    .dataLineLeft {
		text-align : left;
    }
    .dataLineRight {
        padding : 2px 30px 6px 0px;
		text-align : right;
    }
</style>
</head>
<body>
<div class="rptBody01">
<div class="pageTitle" style="width: 670px">Dispatch Summary</div>

<table width="670" border="0" cellspacing="0" cellpadding="0">

<tr>
<td>
 <div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px; width: 670px">
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr>
 <td colspan="5" class="titleLine">Dispatch Price Trend Report: CFT2-P4</td>
 </tr>
 
 <tr>
 <td class="labelsCenter">&nbsp;</td>
 <td class="labelsCenter">Current Day's Average</td>
 <td class="labelsCenter">Current Week's Average</td>
 <td class="labelsCenter">Current Month's Average</td>
 <td class="labelsCenter">YTD Average</td>
 </tr>
 	 
<%
for (int i=0; i<cca_list.size(); i++) {
	cca = (String)cca_list.get(i);
	Map values = (Map)vcogs.get(cca);
	
	if (values != null) {
		
		String d = ((String)values.get("Current Day") != null ? (String)values.get("Current Day")  : "N/A" );
		String w = ((String)values.get("Current Week") != null ? (String)values.get("Current Week")  : "N/A" );
		String m = ((String)values.get("Current Month") != null ? (String)values.get("Current Month")  : "N/A" );
		String y = ((String)values.get("Current Year") != null ? (String)values.get("Current Year")  : "N/A" );
		
%>
 <tr>
 <td class="dataLineLeft">&nbsp;&nbsp;<%=cca %></td>
 <td class="dataLineCenter"><%=d%></td>
 <td class="dataLineCenter"><%=w%></td>
 <td class="dataLineCenter"><%=m%></td>
 <td class="dataLineCenter"><%=y%></td>
 </tr>
 
<% 
	}
}
%>
 </table>
 </div>
</td>
</tr>

</table>
</div>
</body>
</html>