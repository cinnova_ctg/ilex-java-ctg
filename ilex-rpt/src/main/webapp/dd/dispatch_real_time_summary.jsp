<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.Vector" %>
<%
Vector cca_list = (Vector)request.getAttribute("cca_list");
Map lo_pc_id = (Map)request.getAttribute("lo_pc_id");

String cca = "";
Map today = (Map)request.getAttribute("today");
Map avg = (Map)request.getAttribute("avg");

String not_assigned = (String)request.getAttribute("not_assigned");
if (not_assigned == null) {
	not_assigned = "0";
}

if (!not_assigned.equals("0")) {
	not_assigned = "<a href=\"http://ilex.contingent.local/Ilex/NewWebTicketAction.do\">"+not_assigned+"</a>";
}
	

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="refresh" content="240">
<title>Dispatch Summary by Ticket Owner</title>
<style>
    body {
    		font-family : Arial, Helvetica, sans-serif;
    		font-size : 12px;
    		cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .pageTitle {
		font-size : 15pt;
		font-weight : bold;
		text-align : center;
		padding : 12px 0px 12px 0px;
    } 
    .titleLine {
		font-size : 11pt;
		font-weight : bold;
		text-align : left;
		padding : 0px 0px 0px 0px;
        vertical-align : top;
    } 
    .sectionTitleLine {
		font-size : 10pt;
		font-weight : bold;
		text-align : right;
		padding : 0px 0px 0px 0px;
        vertical-align : bottom;
    }    
    .labelsLeft, .labelsCenter, .labelsRight {
		font-size : 9pt;
		font-weight : bold;
		padding : 5px 2px 2px 2px;
        vertical-align : bottom;
    }    

    .labelsCenter {
		text-align : center;
    }    
    .forecast, .highprobability, .recognized, .delta  {
		font-size : 8pt;
		font-weight : normal;
		text-align : center;
		padding : 2px 1px 2px 1px;
		border-style: solid;
        border-width: 1px
    }
    .forecast {
		background-color : #ffccc9;
    }  
    .highprobability {
		background-color : #e5f9de;
    }  
    .recognized {
		background-color : #e0edf5;		 
    }     
    .delta {
		background-color : #ffffff;
		text-align : right;
    }
    .dataLineLeft, .dataLineCenter, .dataLineRight,{
		font-size : 9pt;
		font-weight : normal;
		padding : 2px 0px 6px 0px;
    }
    .dataLineCenter {
		text-align : center;
		font-size : 12pt;
    }
    .dataLineLeft {
		text-align : left;
    }
    .dataLineRight {
        padding : 2px 30px 6px 0px;
		text-align : right;
    }
</style>
</head>
<body>
<div class="rptBody01">
<div class="pageTitle" style="width: 670px">Dispatch Summary</div>

<table width="670" border="0" cellspacing="0" cellpadding="0">

<tr> 
<td>
 <div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px; width: 670px">
 <table width="100%" border="0" cellspacing="1" cellpadding="0">
 <tr>
 <td colspan="10" class="titleLine">Dispatch Activity for Today and Trends Report:</td>
 </tr>
 <tr>
 <td class="labelsCenter">&nbsp;</td>
 <td colspan=9 class="labelsLeft">Unassigned:&nbsp;&nbsp;<%=not_assigned%></td>
 </tr>
 <tr>
 <td class="labelsCenter">&nbsp;</td>
 <td class="labelsCenter" bgcolor="#e5f9de">To Be<br>Scheduled</td>
 <td class="labelsCenter" bgcolor="#e5f9de">Scheduled</td>
 <td class="labelsCenter" bgcolor="#e5f9de">Onsite</td>
 <td class="labelsCenter" bgcolor="#e5f9de">Offsite</td>
 <td class="labelsCenter" bgcolor="#e5f9de">Completed</td>
 <td class="labelsCenter" bgcolor="#ffccc9">Not Planned<br>for Today</td>
 <td class="labelsCenter" bgcolor="#e0edf5">Week Total</td>
 <td class="labelsCenter" bgcolor="#e0edf5">Month Total</td>
 <td class="labelsCenter" bgcolor="#e0edf5">YTD Total</td>
 </tr>
 	 
<%
for (int i=0; i<cca_list.size(); i++) {
	cca = (String)cca_list.get(i);
	String pc_id = (String)lo_pc_id.get(cca);
	Map values = (Map)today.get(cca);
	
	if (values != null) {
		
		String tbs = ((String)values.get("To be Scheduled") != null ? (String)values.get("To be Scheduled")  : "0" );
		String s = ((String)values.get("Scheduled") != null ? (String)values.get("Scheduled")  : "0" );
		String on = ((String)values.get("Onsite") != null ? (String)values.get("Onsite")  : "0" );
		String off = ((String)values.get("Offsite") != null ? (String)values.get("Offsite")  : "0" );
		String o = ((String)values.get("Other") != null ? (String)values.get("Other")  : "0" );
		String c = ((String)values.get("Completed") != null ? (String)values.get("Completed")  : "0" );
		String w = ((String)values.get("Total Week") != null ? (String)values.get("Total Week")  : "0" );
		String m = ((String)values.get("Total Month") != null ? (String)values.get("Total Month")  : "0" );
		String y = ((String)values.get("Total YTD") != null ? (String)values.get("Total YTD")  : "0" );
		
		String href = "http://ilex.contingent.local/Ilex/AppendixHeader.do?function=view&fromjobofaOwner=showmyjob&changeFilter=Y&viewjobtype=I&nettype=dispatch&appendixid=-1&ownerId="+pc_id;
%>
 <tr>
 <td class="dataLineLeft">&nbsp;&nbsp;<a href="<%=href%>"><%=cca%></a></td>
 <td class="dataLineCenter" bgcolor="#e5f9de"><%=tbs%></td>
 <td class="dataLineCenter" bgcolor="#e5f9de"><%=s%></td>
 <td class="dataLineCenter" bgcolor="#e5f9de"><%=on%></td>
 <td class="dataLineCenter" bgcolor="#e5f9de"><%=off%></td>
 <td class="dataLineCenter" bgcolor="#e5f9de"><%=c%></td>
 <td class="dataLineCenter" bgcolor="#ffccc9"><%=o%></td>
 <td class="dataLineCenter" bgcolor="#e0edf5"><%=w%></td>
 <td class="dataLineCenter" bgcolor="#e0edf5"><%=m%></td>
 <td class="dataLineCenter" bgcolor="#e0edf5"><%=y%></td>
 </tr>
 
<% 
	}
}
%>
 </table>
 </div>
</td>
</tr>

<tr><td><br></td></tr>

<tr>
<td>
 <div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px; width: 670px">
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr>
 <td colspan="5" class="titleLine">Dispatch Activity Average Report:</td>
 </tr>
 
 <tr>
 <td class="labelsCenter">&nbsp;</td>
 <td class="labelsCenter">Daily To Schedule</td>
 <td class="labelsCenter">Daily Scheduled</td>
 <td class="labelsCenter">Daily Complete</td>
 <td class="labelsCenter">Weekly</td>
 <td class="labelsCenter">Monthly</td>
 <td class="labelsCenter">Yearly</td>
 </tr>
 	 
<%
for (int i=0; i<cca_list.size(); i++) {
	cca = (String)cca_list.get(i);
	Map values = (Map)avg.get(cca);
	
	if (values != null) {
		
		String tbs = ((String)values.get("To be Scheduled") != null ? (String)values.get("To be Scheduled")  : "N/A" );
		String s = ((String)values.get("Scheduled") != null ? (String)values.get("Scheduled")  : "N/A" );
		String tbc = ((String)values.get("To be Completed") != null ? (String)values.get("To be Completed")  : "N/A" );
		String w = ((String)values.get("Week") != null ? (String)values.get("Week")  : "N/A" );
		String m = ((String)values.get("Month") != null ? (String)values.get("Month")  : "N/A" );
		String y = ((String)values.get("Last 365 Days") != null ? (String)values.get("Last 365 Days")  : "N/A" );
		
%>
 <tr>
 <td class="dataLineLeft"><%=cca%></td>
 <td class="dataLineCenter"><%=tbs%></td>
 <td class="dataLineCenter"><%=s%></td>
 <td class="dataLineCenter"><%=tbc%></td>
 <td class="dataLineCenter"><%=w%></td>
 <td class="dataLineCenter"><%=m%></td>
 <td class="dataLineCenter"><%=y%></td>
 </tr>
<% 
	}
}
%>
 </table>
 </div>
</td>
</tr>
</table>
</div>
</body>
</html>