<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<HTML>
<HEAD>
<TITLE>NetFlow</TITLE>
<title>NetFlow Verification Charts</title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<script type="text/javascript" src="netflow/FusionCharts.js"></script>
<%
Map<String, Vector<Map<String,String>>> device_details = (Map<String, Vector<Map<String,String>>>)request.getAttribute("device_details");
Vector<String> device_list = (Vector<String>)request.getAttribute("device_list");

String name = (String)request.getAttribute("name");
String identifier = (String)request.getAttribute("identifier");
String i1 = (String)request.getAttribute("i1");
String i2 = (String)request.getAttribute("i2");

String seriesname;
if (i2.equals("I")) {
	seriesname = "Input";
} else {
	seriesname = "Output";
}
// & = %26 
// = = %3d
String url = "ac%3dgetUsageDataStream%26identifier%3d"+identifier+"%26i1%3d"+i1+"%26i2%3d"+i2;
%>
</HEAD>
<BODY>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="cDataRight0" valign="top">
  <table border="0" cellspacing="0" cellpadding="0" valign="top">
  <tr><td align="center"><b>Sites</b></td></tr>
<%
Vector<Map<String,String>> interface_list;
Map<String,String> interface_details;
String site;

if (device_list.size() > 0) {
	for (int i=0; i<device_list.size(); i++) {
		site = device_list.get(i);
%>
  <tr><td valign="top" style="padding:4px 10px 1px 10px;">
<%
       interface_list = device_details.get(device_list.get(i));
 
       for (int j=0; j<interface_list.size(); j++) {
	       interface_details = interface_list.get(j);
	       site = java.net.URLEncoder.encode(device_list.get(i));
	 %>
	 <a href="NetFlow.xo?ac=usage2&name=<%=site%>&identifier=<%=interface_details.get("nf_sr_dv_id")%>&i1=<%=interface_details.get("nf_si_ninterfaceid")%>&i2=I"><%=device_list.get(i)%></a><br>
	 <%
 }

 %>
  </td></tr>
<%
	}
}
%>
</table>
</td>
<td valign="top">
<div style="padding : 0px 0px 0px 20px">
<%@ include file="controls.jsp"%>
<div id="chart1div" style="padding:10px 0px 0px 0px;">Select device/interface to display.</div>
<div id="chart2div" style="padding:10px 0px 0px 0px;"></div>
<div id="chart3div" style="padding:10px 0px 0px 0px;"></div>
</div>
</td>
<td valign="top">
<div style="padding : 0px 0px 0px 40px">
<%
if (!name.equals("")) {
%>
<b>Input</b>
<table border="1" cellspacing="0" cellpadding="3">
<%=request.getAttribute("table_input")%>
</table>
<br><b>Output</b>
<table border="1" cellspacing="0" cellpadding="3">
<%=request.getAttribute("table_output")%>
</table>
</div>
<%
}
%>
</td>
</tr>
</table>	
</BODY>
<%
if (!name.equals("")) {
%>
<script type="text/javascript">
   var ChartData1 = "";
   ChartData1 = "<chart caption='Utilization - Incoming' showPlotBorder='1' plotBorderColor='a0a0a0' xAxisName='Hours' yAxisName='Bytes' "+
                "decimals='0' yAxisMinValue='0' "+
                "palette='2' labelDisplay='Rotate' slantLabels='1' "+
                "showLegend='0' >"+
             
                "<%=request.getAttribute("input")%>"+
              
                "</chart>";
   var chart1 = new FusionCharts("netflow/MSArea.swf", "ChId1", "600", "400", "0", "0");                
   chart1.setDataXML(ChartData1);
   chart1.setTransparent(true);
   chart1.render("chart1div");
   
   var ChartData2 = "";
   ChartData2 = "<chart caption='Utilization - Outgoing' showPlotBorder='1' plotBorderColor='a0a0a0' xAxisName='Hours' yAxisName='Bytes' "+
                "decimals='0' yAxisMinValue='0' showRealTimeValue='1' realTimeValuePadding='60'  interactiveLegend='0' "+
                "palette='2' realTimeValuePadding='20' labelDisplay='Rotate' slantLabels='1' "+
                "showLegend='0'>"+
               
               "<%=request.getAttribute("output")%>"+
               
               "</chart>";
   var chart2 = new FusionCharts("netflow/MSArea.swf", "ChId2", "600", "400", "0", "0");                
   chart2.setDataXML(ChartData2);
   chart2.setTransparent(true);
   chart2.render("chart2div");
   
   var ChartData3 = "";
   ChartData3 = "<chart caption='Utilization - Incoming/Outgoing' showPlotBorder='1' plotBorderColor='a0a0a0' xAxisName='Hours' yAxisName='Bytes' "+
                "decimals='0' yAxisMinValue='0' "+
                "palette='2' labelDisplay='Rotate' slantLabels='1' "+
                "legendBorderThickness='0' legendBorderAlpha='0' >"+
                
                "<%=request.getAttribute("combined")%>"+
                
                "</chart>";
   var chart3 = new FusionCharts("netflow/MSArea.swf", "ChId3", "600", "400", "0", "0");                
   chart3.setDataXML(ChartData3);
   chart3.setTransparent(true);
   chart3.render("chart3div");
</script>
<%
}
%>
</HTML>