<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<HTML>
<HEAD>
<TITLE>NetFlow</TITLE>
<title>NetFlow Verification Charts</title>
<%@ include file="styles.txt"%>
<script type="text/javascript" src="netflow/FusionCharts.js"></script>
<%
// & = %26 
// = = %3d
//String url = "ac%3dgetUsageDataStream%26identifier%3d"+identifier+"%26i1%3d"+i1+"%26i2%3d"+i2;
%>

</HEAD>
<BODY>
<table border="0" cellspacing="5" cellpadding="2">

<tr>
<td class="cDataLeft1" valign="top">CFA-001918</td>
<td class="cDataLeft1" valign="top">CFA-00695</td>
</tr>

<tr>
<td class="cDataRight0" valign="top"><div id="chart1div" style="padding:10px 0px 0px 0px;">Loading...</div></td>
<td class="cDataRight0" valign="top"><div id="chart2div" style="padding:10px 0px 0px 0px;">Loading...</div></td>
</tr>

<tr>
<td class="cDataLeft1" valign="top">Top 5 Senders</td>
<td class="cDataLeft1" valign="top">Top 5 Receivers</td>
</tr>

<tr>
<td class="cDataCenter0" valign="top"><div id="chart3div" style="padding:10px 0px 0px 0px;">Loading...</div></td>
<td class="cDataCenter0" valign="top"><div id="chart4div" style="padding:10px 0px 0px 0px;">Loading...</div></td>
</tr>

</table>

</BODY>

<script type="text/javascript">
   var ChartData1 = "";
   ChartData1 = "<chart caption='Bandwidth Distribution - Inbound' showPlotBorder='1' plotBorderColor='a0a0a0' "+
                " xAxisName='Bandwidth Interval (K)' yAxisName='Frequency' labelStep='2' "+
                "decimals='0' yAxisMinValue='0' "+
                "palette='2' labelDisplay='Rotate' slantLabels='1' "+
                "showLegend='0' "+
                "showValues='0' drawAnchors='0' plotBorderThickness='1' >"+
             
                "<%=request.getAttribute("Ch1")%>"+
              
                "</chart>";
   var chart1 = new FusionCharts("netflow/SplineArea.swf", "ChId1", "800", "500", "0", "0");                
   chart1.setDataXML(ChartData1);
   chart1.setTransparent(true);
   chart1.render("chart1div");
   
   var ChartData2 = "";
   ChartData2 = 
   
   "<chart caption='Bandwidth Distribution - Inbound' showPlotBorder='1' plotBorderColor='a0a0a0' "+
   " xAxisName='Bandwidth Interval (K)' yAxisName='Frequency' labelStep='2' "+
   "decimals='0' yAxisMinValue='0' "+
   "palette='2' labelDisplay='Rotate' slantLabels='1' "+
   "showLegend='0' showValues='0' drawAnchors='0' lineThickness='1' >"+

   "<%=request.getAttribute("Ch2")%>"+
 
   "</chart>";
   
   var chart2 = new FusionCharts("netflow/Spline.swf", "ChId2", "800", "500", "0", "0");                
   chart2.setDataXML(ChartData2);
   chart2.setTransparent(true);
   chart2.render("chart2div");
   
   
   var ChartData3 = "";
   ChartData3 = "<%=request.getAttribute("inchart")%>";
   var chart3 = new FusionCharts("netflow/Doughnut2D.swf", "ChId3", "500", "300", "0", "0");                
   chart3.setDataXML(ChartData3);
   chart3.setTransparent(true);
   chart3.render("chart3div");
   
   var ChartData4 = "";
   ChartData4 = "<%=request.getAttribute("outchart")%>";
   var chart4 = new FusionCharts("netflow/Doughnut2D.swf", "ChId4", "500", "300", "0", "0");                
   chart4.setDataXML(ChartData4);
   chart4.setTransparent(true);
   chart4.render("chart4div");
 
   
</script>

</HTML>