<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ page import = "java.util.Map" %>
<%
Map report = (Map)request.getAttribute("Summary");
Double[] percentage = (Double[])report.get("percentage");
String[] displayDollar = (String[])report.get("displayDollar");
String reportDate = (String)report.get("reportDate"); 
Double ZERO = new Double(0.0);
String[] classes = {"forecast", "highprobability", "recognized", "delta", "delta"};
String rev = "20.0M";

Map resource_cost = (Map)request.getAttribute("resource_cost");
Map timeliness = (Map)request.getAttribute("timeliness");
Map profit_productivity = (Map)request.getAttribute("profit_productivity");

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Top Level Summary</title>

<script type="text/javascript">
var simpleEncoding = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
var PO = new Array(12, 2, 201);
var PO_MAX = 201;
var EI = new Array(39, 92);
var EI_MAX = 92;

function simpleEncode(values,maxValue) {

var chartData = ['s:'];
  for (var i = 0; i < values.length; i++) {
    var currentValue = values[i];
    if (!isNaN(currentValue) && currentValue >= 0) {
    chartData.push(simpleEncoding.charAt(Math.round((simpleEncoding.length-1) * currentValue / maxValue)));
    }
      else {
      chartData.push('_');
      }
  }
return chartData.join('');


}
</script>
<style>
    body {
    		font-family : Arial, Helvetica, sans-serif;
    		font-size : 12px;
    		cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .pageTitle {
		font-size : 15pt;
		font-weight : bold;
		text-align : center;
		padding : 12px 0px 12px 0px;
    } 
    .titleLine {
		font-size : 11pt;
		font-weight : bold;
		text-align : left;
		padding : 0px 0px 0px 0px;
        vertical-align : bottom;
    } 
    .sectionTitleLine, .sectionTitleLineCenter {
		font-size : 10pt;
		font-weight : bold;
		text-align : right;
		padding : 0px 0px 0px 0px;
        vertical-align : bottom;
    }    
    .sectionTitleLineCenter {
		text-align : center;
    }
    .labelsLeft, .labelsCenter, .labelsRight {
		font-size : 9pt;
		font-weight : bold;
		padding : 5px 0px 0px 0px;
        vertical-align : top;
    }    

    .labelsCenter {
		text-align : center;
    }    
    .forecast, .highprobability, .recognized, .delta  {
		font-size : 8pt;
		font-weight : normal;
		text-align : center;
		padding : 2px 1px 2px 1px;
		border-style: solid;
        border-width: 1px
    }
    .forecast {
		background-color : #ffccc9;
    }  
    .highprobability {
		background-color : #e5f9de;
    }  
    .recognized {
		background-color : #e0edf5;		 
    }     
    .delta {
		background-color : #ffffff;
		text-align : right;
    }
    .dataLineLeft, .dataLineCenter {
		font-size : 9pt;
		font-weight : normal;
		padding : 2px 0px 6px 0px;
    }
    .dataLineCenter {
		text-align : center;
    }
    .dataLineLeft {
		text-align : left;
    }
</style>
</head>
<body>

<div class="rptBody01">
<div class="pageTitle" style="width: 895px">Home Dashboard</div>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px; width: 895px">
<table width="895" border="0" cellspacing="0" cellpadding="0">

<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td  class="titleLine">Revenue: Annual Progression</td>
   <td  align="right" class="sectionTitleLine">Target: <%=rev%></td>
  </tr>
 </table>
</td>
</tr>

<tr>
 <td width="840" class="labelsLeft">Forecast, High Probability, and Recognized </td>
</tr>

<tr>
 <td>
  <table width="100%" border="0" cellspacing="1" cellpadding="0">
    <tr>
    <%
    for (int i = 0; i < 4; i++) {

    	if (percentage[i].compareTo(ZERO) == 0 || displayDollar[i] != null) {
    		    if (displayDollar[i] == null) {
    		    	displayDollar[i] = "";
    		    }
    			out.println("<td width=\"" + percentage[i] + "%\" class=\""+classes[i] + "\">" + displayDollar[i] + "</td>");
    	}
    }
    %>
    </tr>
  </table>
 </td>
</tr>

<tr>
 <td width="895" class="labelsLeft">Closed</td>
</tr>

<tr>
 <td>
  <table width="100%" border="0" cellspacing="1" cellpadding="0">
    <tr>
    <%
     if (percentage[2].compareTo(ZERO) == 0 || displayDollar[2] != null) {
    	out.println("<td width=\"" + percentage[2] + "%\" class=\""+ classes[2] + "\">" + displayDollar[2] + "</td>");
     }
    out.println("<td width=\"" + percentage[4] + "%\" class=\""+ classes[4] + "\">" + displayDollar[4] + "</td>");
    %>
    </tr>
  </table>
 </td>
</tr>
</table>
</div>
<br>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px; width: 895px">
<table width="895" border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td class="sectionTitleLineCenter">Job Revenue by Month</td>
   <td class="sectionTitleLineCenter">eInvoice Usage - Internal vs Web</td>
  </tr>
  
  <tr>
  <%
 
  Map charts = (Map)request.getAttribute("charts");
  /*
  String dw_chxl1 = "Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|Jan|Feb|Mar";
  String dw_chxl2 = "2007|2008";
  String dw_chxl3 = "$2,714.30|$1,613,446.60";
  String dw_chd = "51.8,63.5,59.9,70.1,79.7,69.6,100.0,69.9,51.6,74.1,58.7,0.0";
  */
  
  Map parameters = (Map)charts.get("c1-JB-sum-ttm");
  String dw_chxl1 = (String)parameters.get("dw_chxl1");
  String dw_chxl2 = (String)parameters.get("dw_chxl2");;
  String dw_chxl3 = (String)parameters.get("dw_chxl3");;
  String dw_chd = (String)parameters.get("dw_chd");
  
  %>
   <td align="center">
   <img src="http://chart.apis.google.com/chart?cht=bvg&amp;chs=370x200&amp;chd=t:<%=dw_chd%>&amp;chxt=x,y,x&amp;chxl=0:|<%=dw_chxl1%>|1:|<%=dw_chxl3%>|2:|<%=dw_chxl2%>&amp;chbh=15,5&amp;chco=cc0000&amp;alt="Revenue by Month" />
   </td>
    <%  
  parameters = (Map)charts.get("c1-EW-count-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  
  %>
   <td align="center">
<img src="http://chart.apis.google.com/chart?
cht=bvg&amp;chs=500x200&amp;
chd=t:<%=dw_chd%>&amp;
chxt=x,y,x&amp;
chxl=
0:|<%=dw_chxl1%>|
1:|<%=dw_chxl3%>|
2:|<%=dw_chxl2%>
&amp;chbh=10,3,10
&amp;chco=cc0000,0000ff
&amp;chdl=Web|Internal
&amp;chco=cc0000,0000ff
&amp;alt="Web vs Internal Invoices" />
   </td>
  </tr>
 </table>
</div>
<br>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px; width: 895px">
<table width="895" border="0" cellspacing="0" cellpadding="0">
<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td colspan="5" class="titleLine">Invoicing Delays: Current Lag (days)</td>
  </tr>
  <tr>
   <td class="labelsLeft">&nbsp;</td>
   <td class="labelsCenter">This Week</td>
   <td class="labelsCenter">Last 90 Days</td>
   <td class="labelsCenter">Last 365 Days</td>
   <td class="labelsCenter">&nbsp;</td>
   <td class="labelsCenter">Target</td>
  </tr>
  <%
  String[] y = (String[])timeliness.get("onsite_offsite");
  %>
  <tr>
   <td class="dataLineLeft"><span>Operations (onsite to offsite)</span></td>
   <td class="dataLineCenter"><%=y[0]%></td>
   <td class="dataLineCenter"><%=y[1]%></td>
   <td class="dataLineCenter"><%=y[2]%></td>
   <td class="dataLineCenter">&nbsp;</td>
   <td class="dataLineCenter">Same Day</td>  
  </tr>
  
  <!--  
  <tr>
   <td class="dataLineLeft">Operations (onsite to offsite)</td>
   <td class="dataLineCenter">0</td>
   <td class="dataLineCenter">0</td>c
   <td class="dataLineCenter">0</td>
   <td class="dataLineCenter">&nbsp;</td>
   <td class="dataLineCenter">Same Day</td>
  </tr> 
  -->
  
  <%
  y = (String[])timeliness.get("offsite_complete");
  %>
  <tr>
   <td class="dataLineLeft"><span>Operations (offsite to complete)</span></td>
   <td class="dataLineCenter"><%=y[0]%></td>
   <td class="dataLineCenter"><%=y[1]%></td>
   <td class="dataLineCenter"><%=y[2]%></td>
   <td class="dataLineCenter">&nbsp;</td>
   <td class="dataLineCenter">2.5</td>  
  </tr>
  
  <!--
  <tr>
   <td class="dataLineLeft">Operations (offsite to complete)</td>
   <td class="dataLineCenter">5</td>
   <td class="dataLineCenter">6</td>
   <td class="dataLineCenter">9</td>
   <td class="dataLineCenter">&nbsp;</td>
   <td class="dataLineCenter">2.5</td>
  </tr>
  -->
  
  <%
  y = (String[])timeliness.get("complete_closed");
  %>
  <tr>
   <td class="dataLineLeft"><span>Accounting (complete to closed)</span></td>
   <td class="dataLineCenter"><%=y[0]%></td>
   <td class="dataLineCenter"><%=y[1]%></td>
   <td class="dataLineCenter"><%=y[2]%></td>
   <td class="dataLineCenter">&nbsp;</td>
   <td class="dataLineCenter">2.5</td>  
  </tr>
  
  <!--
  <tr>
   <td class="dataLineLeft">Accounting (complete to closed)</td>
   <td class="dataLineCenter">18</td>
   <td class="dataLineCenter">14</td>
   <td class="dataLineCenter">13</td>
   <td class="dataLineCenter">&nbsp;</td>
   <td class="dataLineCenter">2.5</td>
  </tr>
  -->
  
 </table>
</td>
</tr>
</table>
</div>
<br>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px; width: 895px">
<table width="895" border="0" cellspacing="0" cellpadding="0">
<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td colspan="6" class="titleLine">Purchasing: Labor VCOGS Buying Power</td>
  </tr>
  <tr>
   <td  class="labelsLeft">&nbsp;</td>
   <td  class="labelsCenter">This Week</td>
   <td  class="labelsCenter">Last 30 Days</td>
   <td  class="labelsCenter">Last 90 Days</td>
   <td  class="labelsCenter">Last 365 Days</td>
   <td  class="labelsCenter">Target</td>  
  </tr>
  <%
  y = (String[])resource_cost.get("700000004");
  //String[] x = {"0.0","0.0","0.0","0.0","0.0"};
  %>
  <tr>
   <td class="dataLineLeft"><span>CFT2-C5 (PPS)</span></td>
   <td class="dataLineCenter"><%=y[0]%></td>
   <td class="dataLineCenter"><%=y[1]%></td>
   <td class="dataLineCenter"><%=y[2]%></td>
   <td class="dataLineCenter"><%=y[3]%></td>
   <td class="dataLineCenter">$45.00</td>  
  </tr>
  <!-- 
  <tr>
   <td class="dataLineLeft"><span>CFT2-C5 (PPS) - OLD</span></td>
   <td class="dataLineCenter">$50.69</td>
   <td class="dataLineCenter">$53.71</td>
   <td class="dataLineCenter">$53.04</td>
   <td class="dataLineCenter">$50.87</td>
   <td class="dataLineCenter">$45.00</td>  
  </tr>
  -->
  <%
  y = (String[])resource_cost.get("700100067");
  %>
  <tr>
   <td class="dataLineLeft"><span>CFT2-C4 (PPS)</span></td>
   <td class="dataLineCenter"><%=y[0]%></td>
   <td class="dataLineCenter"><%=y[1]%></td>
   <td class="dataLineCenter"><%=y[2]%></td>
   <td class="dataLineCenter"><%=y[3]%></td>
   <td class="dataLineCenter">$45.00</td>  
  </tr>
  <!-- 
  <tr>
   <td class="dataLineLeft"><span>CFT2-C4 (PPS) - OLD</span></td>
   <td class="dataLineCenter">$57.66</td>
   <td class="dataLineCenter">$60.78</td>
   <td class="dataLineCenter">$64.34</td>
   <td class="dataLineCenter">$66.08</td>
   <td class="dataLineCenter">$45.00</td>  
  </tr>
   -->
  <%
  y = (String[])resource_cost.get("700100037");
  %>
  
  <tr>
   <td class="dataLineLeft"><span>CFT2-C3 (PPS)</span></td>
   <td class="dataLineCenter"><%=y[0]%></td>
   <td class="dataLineCenter"><%=y[1]%></td>
   <td class="dataLineCenter"><%=y[2]%></td>
   <td class="dataLineCenter"><%=y[3]%></td>
   <td class="dataLineCenter">$45.00</td>  
  </tr>
  
 </table>
</td></tr></table>
</div>
<br>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px; width: 895px; background-color: #e5f9de">
<table width="895" border="0" cellspacing="0" cellpadding="0">
<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td colspan="6" class="titleLine">Profitability and Productivity: Company</td>
  </tr>
  <tr>
   <td  class="labelsLeft">&nbsp;</td>
   <td  class="labelsCenter">Last Month</td>
   <td  class="labelsCenter">-3 Months</td>
   <td  class="labelsCenter">-6 Months</td>
   <td  class="labelsCenter">-12 Months</td>
   <td  class="labelsCenter">Target</td>  
  </tr>

  <%
  y = (String[])profit_productivity.get("VGPM");
  %>
  <tr>
   <td class="dataLineLeft"><span>VGPM</span></td>
   <td class="dataLineCenter"><%=y[0]%></td>
   <td class="dataLineCenter"><%=y[1]%></td>
   <td class="dataLineCenter"><%=y[2]%></td>
   <td class="dataLineCenter"><%=y[3]%></td>
   <td class="dataLineCenter">0.55</td>  
  </tr>
  
  <%
  y = (String[])profit_productivity.get("# Employees");
  %>
  <tr>
   <td class="dataLineLeft"><span># Employees</span></td>
   <td class="dataLineCenter"><%=y[0]%></td>
   <td class="dataLineCenter"><%=y[1]%></td>
   <td class="dataLineCenter"><%=y[2]%></td>
   <td class="dataLineCenter"><%=y[3]%></td>
   <td class="dataLineCenter">&nbsp;</td>  
  </tr>
  
  <%
  y = (String[])profit_productivity.get("Rev/Employee");
  if (!y[3].equals("N/A")) {
	  y[3] = "$"+y[3];
  }
  %>
  
  <tr>
   <td class="dataLineLeft"><span>Rev/Employee (000's)</span></td>
   <td class="dataLineCenter">$<%=y[0]%></td>
   <td class="dataLineCenter">$<%=y[1]%></td>
   <td class="dataLineCenter">$<%=y[2]%></td>
   <td class="dataLineCenter"><%=y[3]%></td>
   <td class="dataLineCenter">$33.0</td>  
  </tr>
  

  <%
  y = (String[])profit_productivity.get("# Employees (Ops)");
  %>
  <tr>
   <td class="dataLineLeft"><span># Employees (Ops)</span></td>
   <td class="dataLineCenter"><%=y[0]%></td>
   <td class="dataLineCenter"><%=y[1]%></td>
   <td class="dataLineCenter"><%=y[2]%></td>
   <td class="dataLineCenter"><%=y[3]%></td>
   <td class="dataLineCenter">&nbsp;</td>  
  </tr>

  <%
  y = (String[])profit_productivity.get("Rev/Employee (Ops)");
  if (!y[3].equals("N/A")) {
	  y[3] = "$"+y[3];
  }
  %>
  
  <tr>
   <td class="dataLineLeft"><span>Rev/Employee (Ops)</span></td>
   <td class="dataLineCenter">$<%=y[0]%></td>
   <td class="dataLineCenter">$<%=y[1]%></td>
   <td class="dataLineCenter">$<%=y[2]%></td>
   <td class="dataLineCenter"><%=y[3]%></td>
   <td class="dataLineCenter">$60.0</td>  
  </tr>
 
 </table>
</td></tr></table>
</div>

</div>
</body>
</html>