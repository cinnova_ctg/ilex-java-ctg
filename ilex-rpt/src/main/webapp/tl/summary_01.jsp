<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<%

 String sql = "select distinct ap_as_customer from ap_account_summary order by ap_as_customer";

 Map rows = (Map)request.getAttribute("Summary");

 Vector tbs = (Vector)rows.get("In Work - TBS");
 Vector overdue = (Vector)rows.get("In Work - Overdue");
 Vector scheduled = (Vector)rows.get("In Work - Scheduled");
 Vector onsite = (Vector)rows.get("In Work - Onsite");
 Vector offsite = (Vector)rows.get("In Work - Offsite");
 Vector complete = (Vector)rows.get("Complete");
 Vector closed = (Vector)rows.get("Closed");
 Vector overall = (Vector)rows.get("Overall");
 Vector reconciled = (Vector)rows.get("Reconciled");
 Vector recPercentage = (Vector)rows.get("Reconciled_Percentage");
 
 String project_type = (String)request.getAttribute("project_type");
 //String month = (String)request.getAttribute("month");
 //String year = (String)request.getAttribute("year");
 
 String projectTypeName = "All Projects";
 String link1Title = "Deployments";
 String link2Title = "NetMedX";
 String link1Value = "deployments";
 String link2Value = "netmedx";
 
 if (project_type.equals("all")) {
	projectTypeName = "All Projects";
	link1Title = "Deployments";
	link2Title = "NetMedX";
	link1Value = "deployments";
	link2Value = "netmedx";
 } else if (project_type.equals("deployments")) {
	projectTypeName = "Deployments";
	link1Title = "All Projects";
	link2Title = "NetMedX";
	link1Value = "all";
	link2Value = "netmedx";
 } else {
	projectTypeName = "NetMedX";
	link1Title = "Deployments";
	link2Title = "All Projects";
	link1Value = "deployments";
	link2Value = "all";
 }

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Top Level Summary</title>
<style>

    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 12px;
    	cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .rptBody02 {
		 padding : 10px 0px 0px 20px;
    }
    .rptBody02 {
		 padding : 10px 0px 0px 20px;
    }
    .titleLine {
		font-size : 12pt;
		font-weight : bold;
		text-align : left;
		padding : 0px 0px 12px 0px;
    } 
    .sectionTitleLine {
		font-size : 11pt;
		font-weight : bold;
		text-align : left;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
    .labelsLeft, .labelsCenter {
		font-size : 10pt;
		font-weight : bold;
		vertical-align : bottom;
		padding : 6px 12px 3px 12px;
    }    

    .labelsCenter {
		text-align : center;
		padding : 6px 12px 3px 12px;
    }    
    .labelsLeft {
		text-align : right;
		padding : 3px 8px 2px 0px;
    }    
       
    .tbs, .overdue, .onSite, .offsite, .complete, .closed, .overall, .reconciled {
		font-size : 10pt;
		font-weight : normal;
		text-align : left;
		padding : 3px 4px 2px 8px;
		text-align : right;
    }
    .tbs {
		background-color : #fff1f0; 
    }  
    .overdue {
		background-color : #ffccc9; 
    }  
    .onSite, .offsite {
		background-color : #fffced; 
    }  
    .complete {
		background-color : #e0edf5; 
    }  
    .closed {
		background-color : #f9fff3; 
    }  
    .overall {
		background-color : #ffffff;
		font-weight : bold; 
    }  
    .reconciled {
		background-color : #e5f9de; 
    }
  .tagLeft, .tagCenter, .tagRight {
		font-size : 8pt;
		font-weight : normal;
		padding : 3px 3px 3px 3px;
		vertical-align : middle;
    }    
    .tagLeft {
		text-align : left;
    }    
    .tagCenter {
		text-align : center;
    }    
    .tagRight {
		text-align : right;
    }  
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
    
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 1px 1px 1px 1px;
		text-align : center;
		vertical-align : middle;
		width: 25px;
        height: 24px;
    }  
</style>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
 <td class="titleLine">Top Level Summary</td>
</tr>

<tr>
 <td>
 <table width=100% border="0" cellspacing="1" cellpadding="0">
 <tr>
     <td class="sectionTitleLine"><%=projectTypeName%>&nbsp;<span class="tagLeft">(<a href="/rpt/TopLevel.xo?form_action=topLevelReport&project_type=<%=link1Value%>" target="wa"><%=link1Title%></a>&nbsp;|&nbsp;<a href="/rpt/TopLevel.xo?form_action=topLevelReport&project_type=<%=link2Value%>" target="wa"><%=link2Title%></a>)</span></td>
     <form method="post" action="/rpt/TopLevel.xo?form_action=topLevelReport">
     <td align="right"><el:element type="menu1" request="<%=request%>" name="account" source="database" data ="<%=sql%>" js=""/></td><!-- onChange=this.form.submit() -->
     <input type="hidden" name="form_action" value="topLevelReport">
     <input type="hidden" name="project_type" value="<%=project_type%>">
  </tr>
</table>
 </td>
</tr>

<tr>
 <td>
 <div class="rptBody02">
 <table border="0" cellspacing="1" cellpadding="0">
 <tr>
    <td class="labelsCenter">Status</td>
    <td class="labelsCenter">Count</td>
    <td class="labelsCenter">Estimated Cost</td>
    <td class="labelsCenter">Actual Cost</td>
    <td class="labelsCenter">Price</td>
    <td class="labelsCenter">VGP</td>
 </tr>
 
 <tr>
    <td class="labelsLeft">Unscheduled</td>
    <td class="tbs"><%=tbs.get(4)%></td>
    <td class="tbs"><%=tbs.get(0)%></td>
    <td class="tbs">&nbsp;</td>
    <td class="tbs"><%=tbs.get(2)%></td>
    <td class="tbs">&nbsp;</td>
 </tr>
 
 <tr>
    <td class="labelsLeft">Scheduled</td>
    <td class="onSite"><%=scheduled.get(4)%></td>
    <td class="onSite"><%=scheduled.get(0)%></td>
    <td class="onSite"><%=scheduled.get(1)%></td>
    <td class="onSite"><%=scheduled.get(2)%></td>
    <td class="onSite"><%=scheduled.get(3)%></td>
 </tr>
 <tr>
    <td class="labelsLeft">Scheduled-Overdue</td>
    <td class="overdue"><%=overdue.get(4)%></td>
    <td class="overdue"><%=overdue.get(0)%></td>
    <td class="overdue"><%=overdue.get(1)%></td>
    <td class="overdue"><%=overdue.get(2)%></td>
    <td class="overdue"><%=overdue.get(3)%></td>
 </tr>
 
 <tr>
    <td class="labelsLeft">On Site</td>
    <td class="onSite"><%=onsite.get(4)%></td>
    <td class="onSite"><%=onsite.get(0)%></td>
    <td class="onSite"><%=onsite.get(1)%></td>
    <td class="onSite"><%=onsite.get(2)%></td>
    <td class="onSite"><%=onsite.get(3)%></td>
 </tr>

 <tr>
    <td class="labelsLeft">Off Site</td>
    <td class="offsite"><%=offsite.get(4)%></td>
    <td class="offsite"><%=offsite.get(0)%></td>
    <td class="offsite"><%=offsite.get(1)%></td>
    <td class="offsite"><%=offsite.get(2)%></td>
    <td class="offsite"><%=offsite.get(3)%></td>
 </tr>

 <tr>
    <td class="labelsLeft">Complete</td>
    <td class="complete"><%=complete.get(4)%></td>
    <td class="complete"><%=complete.get(0)%></td>
    <td class="complete"><%=complete.get(1)%></td>
    <td class="complete"><%=complete.get(2)%></td>
    <td class="complete"><%=complete.get(3)%></td>
 </tr>

 <tr>
    <td class="labelsLeft">Closed</td>
    <td class="closed"><%=closed.get(4)%></td>
    <td class="closed"><%=closed.get(0)%></td>
    <td class="closed"><%=closed.get(1)%></td>
    <td class="closed"><%=closed.get(2)%></td>
    <td class="closed"><%=closed.get(3)%></td>
 </tr>

<tr>
    <td height="2" bgcolor="#ffffff" colspan="1"></td>
    <td height="2" bgcolor="#000000" colspan="5"></td>
</tr>

 <tr>
    <td class="labelsLeft">Overall</td>
    <td class="overall"><%=overall.get(4)%></td>
    <td class="overall"><%=overall.get(0)%></td>
    <td class="overall"><%=overall.get(1)%></td>
    <td class="overall"><%=overall.get(2)%></td>
    <td class="overall"><%=overall.get(3)%></td>
 </tr>

<tr>
    <td height="6" bgcolor="#ffffff" colspan="6"></td>
</tr>
 
 <tr>
    <td class="labelsLeft">Reconciled</td>
    <td class="reconciled"><%=reconciled.get(4)%></td>
    <td class="reconciled"><%=reconciled.get(0)%></td>
    <td class="reconciled"><%=reconciled.get(1)%></td>
    <td class="reconciled"><%=reconciled.get(2)%></td>
    <td class="reconciled"> </td>
 </tr> 
 
  <tr>
    <td class="labelsLeft">% Reconciled</td>
    <td class="overall"><%=recPercentage.get(4)%></td>
    <td class="overall"><%=recPercentage.get(0)%></td>
    <td class="overall"><%=recPercentage.get(1)%></td>
    <td class="overall"><%=recPercentage.get(2)%></td>
    <td class="overall">&nbsp;</td>
 </tr>
  
 </table>
 </div>
 </td>
</tr>
</table>
</div>  
</body>
</html>