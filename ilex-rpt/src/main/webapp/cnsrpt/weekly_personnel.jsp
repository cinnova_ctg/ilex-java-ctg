<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
	<head>
		<script src="cnsrpt/FusionCharts/FusionCharts.js"></script>
		<script type="text/javascript">
			chart_data = "<chart caption='Personnel Head Count by Department' xAxisName='Week' yAxisName='Amount' labelDisplay='Rotate' yAxisMinValue='0' yAxisMaxValue='50' minimiseWrappingInLegend='1'>"+
             "<%=request.getAttribute("chart_data") %>"+
             "</chart>";
		</script>
    </head>
	<body style="margin: 15px;">
		<div id="chartdiv" align="center" style="margin: 10px;">The chart will appear within this DIV. This text will be replaced by the chart.</div>
		<script type="text/javascript">
			var myChart = new FusionCharts("cnsrpt/FusionCharts/MSLine4.swf", "myChartId", "1200", "800", "0", "0");
	        myChart.setDataXML(chart_data);
	      	myChart.render("chartdiv");
	    </script>
	    <div style="text-align: center;">
	    	<div style="background: #E5E5E5; border: 1px solid #B4B3B3; font-family: Verdana, Geneva, sans-serif; font-size: 12px; padding: 10px; width: 575px;">
	    		<div style="border: 0px; border-right: 1px solid #B4B3B3; float: left; height: 20px;"><a href="?ac=weekly_personnel_xls">Excel Spreadsheet</a></div>
	    		<div style="border: 0px; height: 20px;"><a href="?ac=weekly_personnel_details&in_department=Accounting">Accounting</a> <a href="?ac=weekly_personnel_details&in_department=CPD">CPD</a> <a href="?ac=weekly_personnel_details&in_department=Engineering">Engineering</a> <a href="?ac=weekly_personnel_details&in_department=Field%20Services">Field Services</a> <a href="?ac=weekly_personnel_details&in_department=Help%20Desk">Help Desk</a> <a href="?ac=weekly_personnel_details&in_department=Logistics">Logistics</a> <a href="?ac=weekly_personnel_details&in_department=Management">Management</a>  <a href="?ac=weekly_personnel_details&in_department=MIS">MIS</a> <a href="?ac=weekly_personnel_details&in_department=MSP%20Billing">MSP Billing</a> <a href="?ac=weekly_personnel_details&in_department=Projects">Projects</a> <a href="?ac=weekly_personnel_details&in_department=Quality%20Assurance">Quality Assurance</a> <a href="?ac=weekly_personnel_details&in_department=Sales">Sales</a> <a href="?ac=weekly_personnel_details&in_department=Software">Software</a></div>
	    	</div>
		</div>
	</body>
</html>