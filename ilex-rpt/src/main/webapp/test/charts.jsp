<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.Vector" %>
<%
Map charts = (Map)request.getAttribute("charts");
Map parameters = null;
String dw_chxl1 = null;
String dw_chxl2 = null;
String dw_chxl3 = null;
String dw_chd = null;
String title = "";
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Chart Examples</title>
  <link rel = STYLESHEET href= 'admin.css' Type='text/css'>
<style>
    body {
    		font-family : Arial, Helvetica, sans-serif;
    		font-size : 12px;
    		cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .pageTitle {
		font-size : 15pt;
		font-weight : bold;
		text-align : center;
		padding : 12px 0px 12px 0px;
    } 
    .titleLine {
		font-size : 11pt;
		font-weight : bold;
		text-align : left;
		padding : 0px 0px 0px 0px;
        vertical-align : top;
    } 
    .sectionTitleLine {
		font-size : 10pt;
		font-weight : bold;
		text-align : right;
		padding : 0px 0px 0px 0px;
        vertical-align : bottom;
    }    
    .labelsLeft, .labelsCenter, .labelsRight {
		font-size : 9pt;
		font-weight : bold;
		padding : 5px 10px 5px 10px;
        vertical-align : top;
    }    

    .labelsCenter {
		text-align : center;
    }    
    .forecast, .highprobability, .recognized, .delta  {
		font-size : 8pt;
		font-weight : normal;
		text-align : center;
		padding : 2px 1px 2px 1px;
		border-style: solid;
        border-width: 1px
    }
    .forecast {
		background-color : #ffccc9;
    }  
    .highprobability {
		background-color : #e5f9de;
    }  
    .recognized {
		background-color : #e0edf5;		 
    }     
    .delta {
		background-color : #ffffff;
		text-align : right;
    }
    .dataLineLeft, .dataLineCenter, .dataLineRight,{
		font-size : 9pt;
		font-weight : normal;
		padding : 2px 0px 6px 0px;
    }
    .dataLineCenter {
		text-align : center;
    }
    .dataLineLeft {
		text-align : left;
    }
    .dataLineRight {
        padding : 2px 30px 6px 0px;
		text-align : right;
    }
.labelsCenter1 {		text-align : center;
}
</style>
</head>
<body>
<div class="rptBody01">

<table width="670" border="0" cellspacing="0" cellpadding="0">

<tr><td colspan="3"><div class="pageTitle" style="width: 840px">Minuteman Summary<div></td></tr>

<%
  parameters = (Map)charts.get("c1-MM-count-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  title = "Minuteman Usage by Month";
%>
  
<tr>

<td>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
<span class="labelsCenter"><%=title%></span>
<img src="http://chart.apis.google.com/chart?cht=bvs&amp;chs=400x200&amp;chd=t:<%=dw_chd%>&amp;chxt=x,y,x&amp;chxl=0:|<%=dw_chxl1%>|1:|<%=dw_chxl3%>|2:|<%=dw_chxl2%>&amp;alt="<%=title%>" />
</div>
</td>
 
<td><div style="padding : 0px 0px 0px 0px; width: 20px"><div></td>

<%  
  parameters = (Map)charts.get("c1-MM-sum-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  title = "Minuteman Cost/Month";
%>
<td>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
<span class="labelsCenter"><%=title%></span>
<img src="http://chart.apis.google.com/chart?cht=bvs&amp;chs=400x200&amp;chd=t:<%=dw_chd%>&amp;chxt=x,y,x&amp;chxl=0:|<%=dw_chxl1%>|1:|<%=dw_chxl3%>|2:|<%=dw_chxl2%>&amp;alt="<%=title%>" />
</div>
</td>

</tr>

<tr><td colspan="3"><div class="pageTitle" style="width: 840px">Speedpay Summary</div></td></tr>

<%
  parameters = (Map)charts.get("c1-SS-count-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  title = "Speedpay Usage by Month";
%>
  
<tr>

<td>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
<span class="labelsCenter"><%=title%></span>
<img src="http://chart.apis.google.com/chart?cht=bvs&amp;chs=400x200&amp;chd=t:<%=dw_chd%>&amp;chxt=x,y,x&amp;chxl=0:|<%=dw_chxl1%>|1:|<%=dw_chxl3%>|2:|<%=dw_chxl2%>&amp;alt="<%=title%>" />
</div>
</td>
 
<td><div style="padding : 0px 0px 0px 0px; width: 20px"><div></td>

<%  
  parameters = (Map)charts.get("c1-SS-sum-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  title = "Speedpay Cost/Month";
%>
<td>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
<span class="labelsCenter"><%=title%></span>
<img src="http://chart.apis.google.com/chart?cht=bvs&amp;chs=400x200&amp;chd=t:<%=dw_chd%>&amp;chxt=x,y,x&amp;chxl=0:|<%=dw_chxl1%>|1:|<%=dw_chxl3%>|2:|<%=dw_chxl2%>&amp;alt="<%=title%>" />
</div>
</td>

</tr>


<tr><td colspan="3"><div class="pageTitle" style="width: 840px">Job Summary</div></td></tr>

<%
  parameters = (Map)charts.get("c1-JB-count-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  title = "Jobs by Month";
%>
  
<tr>

<td>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
<span class="labelsCenter"><%=title%></span>
<img src="http://chart.apis.google.com/chart?cht=bvg&amp;chs=400x200&amp;chd=t:<%=dw_chd%>&amp;chxt=x,y,x&amp;chxl=0:|<%=dw_chxl1%>|1:|<%=dw_chxl3%>|2:|<%=dw_chxl2%>&amp;chbh=15,5&amp;chco=cc0000&amp;alt="Revenue by Month" />
</div>
</td>
 
<td><div style="padding : 0px 0px 0px 0px; width: 20px"><div></td>

<%  
  parameters = (Map)charts.get("c1-JB-sum-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  title = "Revenue by Month";
%>
<td>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
<span class="labelsCenter"><%=title%></span>
<img src="http://chart.apis.google.com/chart?cht=bvg&amp;chs=400x200&amp;chd=t:<%=dw_chd%>&amp;chxt=x,y,x&amp;chxl=0:|<%=dw_chxl1%>|1:|<%=dw_chxl3%>|2:|<%=dw_chxl2%>&amp;chbh=15,5&amp;chco=cc0000&amp;alt="Revenue by Month" />
</div>
</td>

</tr>

<tr><td colspan="3"><div class="pageTitle" style="width: 840px">Internal eInvoice Summary</div></td></tr>

<%
  parameters = (Map)charts.get("c1-EI-count-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  title = "eInvoice by Month";
%>
<tr>

<td>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
<span class="labelsCenter"><%=title%></span>
<img src="http://chart.apis.google.com/chart?cht=bvg&amp;chs=400x200&amp;chd=t:<%=dw_chd%>&amp;chxt=x,y,x&amp;chxl=0:|<%=dw_chxl1%>|1:|<%=dw_chxl3%>|2:|<%=dw_chxl2%>&amp;chbh=15,5&amp;chco=blue&amp;alt="Revenue by Month" />
</div>
</td>
 
<td><div style="padding : 0px 0px 0px 0px; width: 20px"><div></td>

<%  
  parameters = (Map)charts.get("c1-EI-sum-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  title = "eInvoice $ by Month";
%>
<td>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
<span class="labelsCenter"><%=title%></span>
<img src="http://chart.apis.google.com/chart?cht=bvg&amp;chs=400x200&amp;chd=t:<%=dw_chd%>&amp;chxt=x,y,x&amp;chxl=0:|<%=dw_chxl1%>|1:|<%=dw_chxl3%>|2:|<%=dw_chxl2%>&amp;chbh=15,5&amp;chco=blue&amp;alt="Revenue by Month" />
</div>
</td>

</tr>


<tr><td colspan="3"><div class="pageTitle" style="width: 840px">Web vs Internal eInvoice Summary</div></td></tr>

<%
  parameters = (Map)charts.get("c1-EW-count-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  title = "Invoices by Month";
%>
<tr>

<td>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
<span class="labelsCenter"><%=title%></span>
<img src="http://chart.apis.google.com/chart?cht=bvg&amp;chs=430x200&amp;chd=t:<%=dw_chd%>&amp;chxt=x,y,x&amp;chxl=0:|<%=dw_chxl1%>|1:|<%=dw_chxl3%>|2:|<%=dw_chxl2%>
&amp;chbh=10,3,10
&amp;chco=cc0000,0000ff
&amp;chdl=Web|Internal
&amp;chco=cc0000,0000ff&amp;alt="<%=title%>" />
</div>
</td>
 
<td><div style="padding : 0px 0px 0px 0px; width: 20px"><div></td>

<%  
  parameters = (Map)charts.get("c1-EW-sum-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  title = "Cost by Month";
%>
<td>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
<span class="labelsCenter"><%=title%></span>
<img src="http://chart.apis.google.com/chart?cht=bvg&amp;chs=460x200&amp;chd=t:<%=dw_chd%>&amp;chxt=x,y,x&amp;chxl=0:|<%=dw_chxl1%>|1:|<%=dw_chxl3%>|2:|<%=dw_chxl2%>&amp;
&amp;chbh=10,3,10
&amp;chco=cc0000,0000ff
&amp;chdl=Web|Internal&amp;alt="<%=title%>" />
</div>
</td>

</tr>

<tr><td colspan="3"><div class="pageTitle" style="width: 840px">Web eInvoice Summary</div></td></tr>

<%
  parameters = (Map)charts.get("c1-WB-count-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  title = "Invoices by Month";
%>
<tr>

<td>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
<span class="labelsCenter"><%=title%></span>
<img src="http://chart.apis.google.com/chart?cht=bvg&amp;chs=400x200&amp;chd=t:<%=dw_chd%>&amp;chxt=x,y,x&amp;chxl=0:|<%=dw_chxl1%>|1:|<%=dw_chxl3%>|2:|<%=dw_chxl2%>&amp;chbh=15,5&amp;chco=blue&amp;alt="Revenue by Month" />
</div>
</td>
 
<td><div style="padding : 0px 0px 0px 0px; width: 20px"><div></td>

<%  
  parameters = (Map)charts.get("c1-WB-sum-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  title = "Cost by Month";
%>
<td>
<div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
<span class="labelsCenter"><%=title%></span>
<img src="http://chart.apis.google.com/chart?cht=bvg&amp;chs=400x200&amp;chd=t:<%=dw_chd%>&amp;chxt=x,y,x&amp;chxl=0:|<%=dw_chxl1%>|1:|<%=dw_chxl3%>|2:|<%=dw_chxl2%>&amp;chbh=15,5&amp;chco=blue&amp;alt="Revenue by Month" />
</div>
</td>

</tr>
</table>
</div>
</body>
</html>