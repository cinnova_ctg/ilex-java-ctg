<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
String title = (String)request.getAttribute("title");
String subtitle = (String)request.getAttribute("subtitle");
String query_month_list = "select convert(varchar(10), sc_dd_month_mm01yyyy, 101) from datawarehouse.dbo.ac_dimension_date where sc_dd_month_mm01yyyy between dateadd(mm, -24,getdate()) and getdate() and datepart(dd, sc_dd_mmddyyyy) = 1 order by sc_dd_month_mm01yyyy";
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Revenue Allocation</title>
<%@ include file="styles.txt"%>
</head>
<body>
<div class="rptBody01">

<table border="0" cellspacing="0" cellpadding="0">

<tr><td>
<form name="month_selected" action="/rpt/ACCTEXP.xo" method="post">
<input type="hidden" name="ac" value="revenue_allocation"> 
<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
  <td class="titleLine"><%=title%><br><span class="cDataLeft0"><%=subtitle%></span></td>
  </tr>
</table></form>
</td></tr>

<tr><td>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
       
   <tr>
     <td class="columnHeader2">Customer</td>
     <td class="columnHeader2">Project</td>
   
     <td class="columnHeader2">Pro Forma</td>
     <td class="columnHeader2">Actual</td>
     <td class="columnHeader2">Revenue</td>
     <td class="columnHeader2">Onsite</td> 
         
     <td class="columnHeader2">Offsite</td>
     <td class="columnHeader2">Weight</td>
     <td class="columnHeader2">Invoice No.</td>
     <td class="columnHeader2">Unique ID</td>
   </tr>

<%
Vector jobs = (Vector)request.getAttribute("jobs");
String[] job;

for (int i = 0; i < jobs.size() ; i++) {
	
	job = (String[])jobs.get(i);
%>
			<tr>
				<td class="cDataLeft<%=i%2%>">&nbsp;&nbsp;<%=job[0]%></td>
				<td class="cDataLeft<%=i%2%>">&nbsp;&nbsp;<%=job[1]%></td>
				<td class="cDataRight<%=i%2%>">&nbsp;&nbsp;<%=job[2]%></td>
				<td class="cDataRight<%=i%2%>">&nbsp;&nbsp;<%=job[3]%></td>

				<td class="cDataRight<%=i%2%>">&nbsp;&nbsp;<%=job[4]%></td>
				<td class="cDataRight<%=i%2%>">&nbsp;&nbsp;<%=job[5]%></td>
				<td class="cDataRight<%=i%2%>">&nbsp;&nbsp;<%=job[6]%></td>
				<td class="cDataRight<%=i%2%>">&nbsp;&nbsp;<%=job[7]%></td>
				<td class="cDataCenter<%=i%2%>">&nbsp;&nbsp;<%=job[8]%></td>
				<td class="cDataCenter<%=i%2%>">&nbsp;&nbsp;<%=job[9]%></td>
			    			
			</tr>
<% } %>
</table>
</td></tr>
<!-- Spacer -->
<tr><td>&nbsp;</td></tr>
<tr><td><b>Notes</b>:<br>
This is a summary of all jobs with the invoice month shown above but falling in a different month based on its offsite date.<br>
</td></tr>
</table>
</div> 
</body>
</html>