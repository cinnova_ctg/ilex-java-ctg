<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
String title = (String)request.getAttribute("title");
String subtitle = (String)request.getAttribute("subtitle");
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quarterly Commission Summary 2</title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<%@ include file="js.txt"%>
</head>
<body>
<div class="rptBody01">

<table border="0" cellspacing="0" cellpadding="0">

<tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<form name="form1" action="ACCTEXP.xo" method="post">
<input type="hidden" name="ac", value="account">
<input type="hidden" name="month", value="<%=request.getAttribute("month") %>">
<input type="hidden" name="year", value="<%=request.getAttribute("year") %>">
<input type="hidden" name="account", value="<%=request.getAttribute("account") %>">
  <tr>
  <td rowspan="2"class="titleLine"><%=title%><br><span class="cDataLeft0"><%=subtitle%></span></td>
  <td align="right" valign="top">
  <table border="0" cellspacing="0" cellpadding="0">
  <tr><td align="right" valign="top">Job Type(s):&nbsp;&nbsp;&nbsp;</td>
  <td><select multiple name="job_types" size="3">
  <option value="Standard Billable">Standard Billable</option>
  <option value="NetMedX Billable">NetMedX Billable</option>
  <option value="MSP Billable">MSP Billable</option>
  <option value="Standard Non-Billable">Standard Non-Billable</option>
  <option value="NetMedX Non-Billable">NetMedX Non-Billable</option>
  <option value="MSP Non-Billable">MSP Non-Billable</option>
  </select></td></tr></table>
  </td>
  </tr>
 
  <tr>
  <td align="right">
  <input type="submit" name="default_view" value="Submit" class=date1></td>
  </tr>
  </form>
 </table>
</td></tr>
<!-- BDMs Main -->
<tr><td>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">

    <tr>
     <td width="80" rowspan="2" class="columnHeader2">Purchase Order #</td>
     <td colspan="7" class="columnHeader2">Cost</td>
    </tr>
   
   <tr>
     <td width="60" class="columnHeader2">Pro&nbsp;Forma</td>
     <td width="60" class="columnHeader2">Labor</td>
     <td width="60" class="columnHeader2">Materials</td>
     <td width="60" class="columnHeader2">Travel</td>
     <td width="60" class="columnHeader2">Freight</td>
     <td width="60" class="columnHeader2">Rentals</td>
     <td width="60" class="columnHeader2">Total</td>
   </tr>

<%
Vector rows = (Vector)request.getAttribute("rows");

for (int i=0; i< rows.size(); i++) {
	String[] r = (String[])rows.get(i); %>

			<tr>
			    
				<td class="cDataCenter<%=i%2%>"><%=r[1]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[2]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[3]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[4]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[5]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[6]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[7]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[8]%></td>
			</tr>
<% } %>
</table>
</td></tr>
<!-- Spacer -->


<tr><td>&nbsp;</td></tr>

<!-- 
<tr><td>
  <table border="0" cellspacing="1" cellpadding="0">
   <tr><td><b>Cost</b></td></tr>
  <tr><td>Actual Cost - Authorized or approved purchase orders cost.</td></tr>
  <tr><td>Unallocated CTR - Unallocated contractor resources. These are resources not allocated to a purchase order.</td></tr>
  <tr><td>Unallocated PMCL - Unallocated internal Contingent resources. These are PM/PC/Logistic resources not allocated to a purchase order.</td></tr>
  <tr><td>Corrected = Actual Cost + Unallocated CTR + Unallocated PMCL</td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr><td><b>Revenue</b></td></tr>
  <tr><td>Inv Correction - The difference between an Ilex invoice price and customer payment as recorded in GP.</td></tr>
  <tr><td>Total = Ilex Invoice Price + GP Invoice Correction</td></tr>
  </table>
</td></tr>
-->
 
</table>
</div> 
</body>
</html>