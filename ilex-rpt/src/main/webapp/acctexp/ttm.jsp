<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
String subtitle = (String)request.getAttribute("subtitle");
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quarterly Commission Summary 2</title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<%@ include file="js.txt"%>
</head>
<body>
<div class="rptBody01">

<table border="0" cellspacing="0" cellpadding="0">

<tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<form name="form1" action="ACCTEXP.xo" method="post">
<input type="hidden" name="ac", value="ttm">
  <tr>
  <td rowspan="2"class="titleLine">Expense Breakout TTM<br><span class="cDataLeft0"><%=subtitle%></span></td>
  <td align="right" valign="top">
  <table border="0" cellspacing="0" cellpadding="0">
  <tr><td align="right" valign="top">Job Type(s):&nbsp;&nbsp;&nbsp;</td>
  <td><select multiple name="job_types" size="3">
  <option value="Standard Billable">Standard Billable</option>
  <option value="NetMedX Billable">NetMedX Billable</option>
  <option value="MSP Billable">MSP Billable</option>
  <option value="Standard Non-Billable">Standard Non-Billable</option>
  <option value="NetMedX Non-Billable">NetMedX Non-Billable</option>
  <option value="MSP Non-Billable">MSP Non-Billable</option>
  </select></td></tr></table>
  </td>
  </tr>
  <tr>
  <td align="right">
  <input type="submit" name="default_view" value="Submit" class=date1></td>
  </tr>
  </form>
 </table>
</td></tr>
<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">

    <tr>
     <td width="70" rowspan="2" class="columnHeader2">Month</td>
     <td colspan="9" class="columnHeader2">Cost</td>
     <td colspan="1" rowspan="2" class="columnHeader2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Revenue&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
     <td colspan="2" class="columnHeader2">VGPM</td>
     <td width="80" rowspan="2" class="columnHeader2">Count</td>
   </tr>
   
   <tr>
     <td width="70" class="columnHeader2">Pro&nbsp;Forma</td>
     <td width="70" class="columnHeader2">Labor</td>
     <td width="80" class="columnHeader2">Materials</td>
     <td width="80" class="columnHeader2">Travel</td>
     <td width="70" class="columnHeader2">Freight</td>
     <td width="80" class="columnHeader2">Rentals</td>
     <td width="80" class="columnHeader2">FCOGS & Unallocated</td>
     <td width="80" class="columnHeader2">GP PO Total</td>
     <td width="80" class="columnHeader2">Total</td>
     <td width="80" class="columnHeader2">Pro Forma</td>
     <td width="80" class="columnHeader2">Actual</td>
   </tr>

<%
Vector rows = (Vector)request.getAttribute("rows");

for (int i=0; i< rows.size(); i++) {
	String[] r = (String[])rows.get(i);
%>

			<tr>
			    <td class="cDataLeft<%=i%2%>"><%=r[0]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[1]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[2]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[3]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[4]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[5]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[6]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[7]%></td>
				
				<td class="cDataRight<%=i%2%>"><%=r[13]%></td>
				
				<td class="cDataRight<%=i%2%>"><%=r[8]%></td>
				
				<td class="cDataRight<%=i%2%>"><%=r[9]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[10]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[11]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[12]%></td>
			</tr>
   
<%
}

%>
</table>
</td></tr>
<!-- Spacer -->
</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><b>Cost</b></td></tr>
<tr><td>
  <table border="0" cellspacing="1" cellpadding="1">
   <tr><td>Pro&nbsp;Forma - Rollup of job level pro forma cost.</td></tr>
   <tr><td>Labor through Freight - Self-explanatory but excludes rentals (see below).</td></tr>
   <tr><td>Rentals - Material resources filtered on the use of "lift" in resource name.</td></tr>
   <tr><td>FCOGS & Unallocated - Unallocated PM/PC and contractor resources.</td></tr>
   <tr><td>GP PO Total - An export from GP by PO Ilex assigned PO number realigned with the originating job.</td></tr>
   <tr><td>Total - Total purchase order expense which includes agent commission not otherwise captured in previous columns.</td></tr>
  </table>
</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><b>Revenue</b></td></tr>
<tr><td>
  <table border="0" cellspacing="0" cellpadding="2">
   <tr><td>Self-explanatory - Ilex Invoice Price + GP Invoice Correction</td></tr>
  </table>
</td></tr>
 
</table>
</div> 
</body>
</html>