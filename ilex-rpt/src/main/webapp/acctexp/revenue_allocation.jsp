<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	String title = (String) request.getAttribute("title");
	String subtitle = (String) request.getAttribute("subtitle");
	String query_month_list = "select convert(varchar(10), sc_dd_month_mm01yyyy, 101) from datawarehouse.dbo.ac_dimension_date where sc_dd_month_mm01yyyy between dateadd(mm, -24,getdate()) and getdate() and datepart(dd, sc_dd_mmddyyyy) = 1 order by sc_dd_month_mm01yyyy";
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Revenue Allocation</title>
<%@ include file="styles.txt"%>
</head>
<body>
<div class="rptBody01">

<table border="0" cellspacing="0" cellpadding="0">

<tr><td>
<form name="month_selected" action="/rpt/ACCTEXP.xo" method="post">
<input type="hidden" name="ac" value="revenue_allocation"> 
<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
  <td class="titleLine"><%=title%><br><span class="cDataLeft0"><%=subtitle%></span></td>
  <td valign="top" align="right">Month: <el:element type="menu1" request="<%=request%>" name="month" source="database" data ="<%=query_month_list%>" js="onChange='document.month_selected.submit();'"/></td>
  </tr>
</table></form>
</td></tr>

<tr><td>
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
				<table border="0" cellspacing="1" cellpadding="0"
					class="outlineYellow3">

					<tr>
						<td rowspan="2" class="columnHeader2">Month</td>
						<td colspan="4" class="columnHeader2">Offsite Date</td>
						<td colspan="4" class="columnHeader2">Invoice Date</td>
						<td colspan="4" class="columnHeader2">Invoiced Date - Grouped
						by Offsite Date</td>
					</tr>

					<tr>
						<td width="60" class="columnHeader2">Pro Forma</td>
						<td width="60" class="columnHeader2">Actual</td>
						<td width="60" class="columnHeader2">Revenue</td>
						<td width="60" class="columnHeader2">Jobs</td>

						<td width="60" class="columnHeader2">Pro Forma</td>
						<td width="60" class="columnHeader2">Actual</td>
						<td width="60" class="columnHeader2">Revenue</td>
						<td width="60" class="columnHeader2">Jobs</td>

						<td width="60" class="columnHeader2">Pro Forma</td>
						<td width="60" class="columnHeader2">Actual</td>
						<td width="60" class="columnHeader2">Revenue</td>
						<td width="60" class="columnHeader2">Dispatches</td>
					</tr>

					<%
						Map m1 = (Map) request.getAttribute("m1");
						Map m2 = (Map) request.getAttribute("m2");
						Map m3 = (Map) request.getAttribute("m3");
						String invoice_date = (String) request.getAttribute("month");
						Vector months = (Vector) request.getAttribute("months");
						String month = "";
						String[] m;
						String[] n;
						String[] p;

						for (int i = 0; i < months.size(); i++) {

							month = (String) months.get(i);

							m = (String[]) m1.get(month);
							n = (String[]) m2.get(month);
							p = (String[]) m3.get(month);
					%>

					<tr>
						<td class="cDataCenter<%=i % 2%>"><%=month%></td>

						<td class="cDataCenter<%=i % 2%>">&nbsp;&nbsp;<%=p[1]%></td>
						<td class="cDataRight<%=i % 2%>">&nbsp;&nbsp;<%=p[2]%></td>
						<td class="cDataRight<%=i % 2%>">&nbsp;&nbsp;<%=p[3]%></td>
						<td class="cDataRight<%=i % 2%>">&nbsp;&nbsp;<%=p[4]%></td>

						<td class="cDataCenter<%=i % 2%>">&nbsp;&nbsp;<%=n[1]%></td>
						<td class="cDataRight<%=i % 2%>">&nbsp;&nbsp;<%=n[2]%></td>
						<td class="cDataRight<%=i % 2%>">&nbsp;&nbsp;<%=n[3]%></td>
						<td class="cDataRight<%=i % 2%>">&nbsp;&nbsp;<%=n[4]%></td>

						<td class="cDataRight<%=i % 2%>">&nbsp;&nbsp;<%=m[1]%></td>
						<td class="cDataRight<%=i % 2%>">&nbsp;&nbsp;<%=m[2]%></td>
						<td class="cDataRight<%=i % 2%>">&nbsp;&nbsp;<%=m[3]%></td>
						<td class="cDataRight<%=i % 2%>">&nbsp;&nbsp;<a
							href="ACCTEXP.xo?ac=revenue_breakout&invoice=<%=invoice_date%>&offsite=<%=m[0]%>"><%=m[4]%></a></td>

					</tr>
					<%
						}
					%>
				</table>
				</td>
			</tr>
			<!-- Spacer -->
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><b>Notes</b>:<br>

				<table border="0" cellspacing="1" cellpadding="10" width="1000">

					<tr>
						<td colspan="2">This report groups revenue by offsite date
						and invoice date. Each grouping defines a distinct set of jobs
						which are summed to show expense/revenue totals.
						
						Further, the invoice date set of jobs is shown in two views: by the given 
						invoice date and by each jobs respective offsite date(s). 
                       
                        </td>

					</tr>

					<tr>
						<td>Offsite Date</td>
						<td>The expense and revenue for all jobs falling in the given
						time frame based on offsite date.</td>
					</tr>

					<tr>
						<td>Invoice Date</td>
						<td>The expense and revenue for all jobs falling in the given
						time frame based on invoice date. This value should correspond to
						the monthly values used in most reporting section pages</td>
					</tr>

					<tr>
						<td>Invoiced Date - Grouped by Offsite Date</td>
						<td>This is the set of jobs grouped together by invoice date,
						but then reported by offsite date. This may cause the jobs to spread
						across multiple months. Note, the expense and revenue values
						reported here are equal to those reported under the single line
						Invoce Date columns</td>
					</tr>
					
					<tr>
						<td>&nbsp;</td>
						<td>When viewed by offsite date, a job's
						revenue may be broken since a single job may have more than one
						dispatch and consequently more than one offsite date. Thus, when
						grouping by offsite date, each offsite occurrence will have a
						distinct revenue amount. The revenue amount is determined by 
						weighting the duration of a given dispatch versus the total time
						on site across all dispatches.</td>
					</tr>
					<tr>
						<td>Jobs</td>
						<td>This report includes all closed billable jobs (i.e. Standard, NetMedX, MSP)</td>
					</tr>

				</table>
				</td>
			</tr>
		</table>
		</div>
</body>
</html>