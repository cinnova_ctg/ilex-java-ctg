<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%@ include file="styles.txt"%>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr><td class="titleLine">International Labor Rates - Summary by Country</td></tr>
<%
if (request.getAttribute("error") != null && !request.getAttribute("error").equals("")) {
%>
<tr><td class="errorMessage"><%=request.getAttribute("error")%></td></tr>
<%
}
%>
<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
    <tr>
     <td rowspan="2" class="columnHeader2">Labor Category</td>
     <td colspan="3" class="columnHeader2">Averages by Labor Category</td>
   </tr>
   <tr>
     <td width="70" class="columnHeader2">Quantity</td>
     <td width="80" class="columnHeader2">Unit Cost</td>
     <td width="80" class="columnHeader2">Total Cost</td>
   </tr>
   
   <%

   	Vector rows = (Vector) request.getAttribute("rows");
   	if (rows != null && rows.size() > 0) {
   		for (int i = 0; i < rows.size(); i++) {
   			String[] r = (String[]) rows.get(i);

   			switch (Integer.parseInt(r[0])) {

   			case 1:
   %>
			   <tr>
				<td class="cDataLeft<%=i%2%>"><%=r[2]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[3]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[4]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[5]%></td>
			   </tr>
			   <%
			   			break;

			   			case 2:
			   %>
			   <tr>
				<td colspan="4" class="cTotalLeft1"><%=r[1]%></td>
			   </tr>
			   <%
			   			break;

			   			case 3:
			   %>
			   <tr>
				<td colspan="3" class="cTotalRight1">Overall Hourly Rate Average</td>
				<td class="cTotalRight1"><%=r[5]%></td>
			   </tr>
			   <%
			   			break;

			   			case 4:
			   %>
			   <tr>
				<td colspan="4" class="break1"></td>
			   </tr>
			   <%
			   			break;
			   			}
			   		}
			   	}
			   %>

</table>
</td></tr>
</table>
</div>
</body>
</html>