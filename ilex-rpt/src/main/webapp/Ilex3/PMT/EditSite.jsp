<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>


<html>
<head>
	<title><bean:message bundle = "AM" key = "am.tabular.title"/></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "javaScript" src = "javascript/JLibrary.js"></script>
</head>

<%
int sitesSize = 0;
String flag = null;

if( request.getAttribute( "sitesSize" ) != null )
{
	 sitesSize =( int ) Integer.parseInt( request.getAttribute( "sitesSize" ).toString()); 
}


	String displaycombo = null;
	String backgroundclass = "texto";
	String labelClass = null;
	String valstr =null;
	
	String readonlyclass = null;
	String readonlynumberclass = null;
	String backnew = "";
	int i = 0;
	String comboclass = null;
	String schdayscheck = null;
	boolean csschooser = true;
	

%>




<body>
<!-- Start : Added By Amit -->
<table border="0" cellspacing="1" cellpadding="1" > 

			<logic:present name = "updateflag" scope = "request">
		  		<tr><td>&nbsp;</td>
			    	<td colspan = "5" class = "message" height = "30">
				  		<logic:equal name = "updateflag" value = "0">
						<bean:message bundle = "pm" key = "pmt.allsites.sitesupdatedsuccessfully"/>
						</logic:equal>
					</td>
				</tr>
		  	</logic:present>
		  	
		  	<logic:equal name = "EditSiteForm" property = "flag" value = "false">
				 <logic:equal name="sitesSize" value="0">
					 <tr><td>&nbsp;</td>
						 <td colspan = "5" class = "message" height = "30">
						 			<bean:message bundle = "pm" key = "pmt.allsites.nositeavailableforthesearchcriteria"/>
						 </td>
					 </tr>
				 </logic:equal>
			 </logic:equal>


  
<td  width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" > 
	<td colspan="7"class="labeleboldwhite" height="30" ><bean:message bundle="pm" key="pmt.allsites.searchforeditsite"/></td>
</tr>
<tr>	

<html:form action = "/EditSiteAction">

<!-- Start :Search Combo For MSA, State, Site Name -->
	<td  class="labelobold" height="20" ><bean:message bundle="pm" key="pmt.allsites.msasearch"/></td>
	<td  class="labelo" height="20" >
	<html:select name="EditSiteForm" property="msasearchcombo" size="1" styleClass="comboo">
	 <html:optionsCollection name = "msalist"  property = "msalist"  label = "label"  value = "value" />
	</html:select></td>

	<td  class="labelobold" height="20" ><bean:message bundle="pm" key="pmt.allsites.statesearch"/></td>
	<td  class="labelo" height="20" >
		<html:select name="EditSiteForm" property="statesearchcombo" size="1" styleClass="comboo">
		 <html:optionsCollection name = "dcStateCM"  property = "firstlevelcatglist"  label = "label"  value = "value" />
		</html:select>
	</td>
	
	<td class="labelobold" height="20" >
		<bean:message bundle="pm" key="pmt.allsites.sitecitysearch"/>
	</td>
	<td class="labelo" height="20">
		<html:text name="EditSiteForm" property="sitecitysearch" styleClass="textbox" size="15"></html:text>
	</td>

	

<!-- Start :Search Combo For MSA, State, Site Name -->

</tr>
<tr>
	<td class="labelebold" height="20" >
		<bean:message bundle="pm" key="pmt.allsites.sitenamesearch"/>
	</td>
	<td class="labele" height="20">
		<html:text name="EditSiteForm" property="sitenamesearch" styleClass="textbox" size="15"></html:text>
	</td>


	<td class="labelebold" height="20" >
		<bean:message bundle="pm" key="pmt.allsites.sitenumbersearch"/>
	</td>
	<td class="labele" height="20">
		<html:text name="EditSiteForm" property="sitenumbersearch" styleClass="textbox" size="15"></html:text>
	</td>



<td class="labele" height="20" colspan="2">
		<html:submit property="search" styleClass="button" onclick="return searchforsite();">
			<bean:message bundle="pm" key="pmt.allsites.search" />
		</html:submit>
</tr>
</table>
<!-- End : Added By Amit -->


<html:hidden  property = "flag" />
<html:hidden  property = "search" />

<logic:equal name = "EditSiteForm" property = "flag" value = "false">
<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
 <tr>
 
  	<td>
  		<table border = "0" cellspacing = "1" cellpadding = "1">

  			
		 
		 <logic:notEqual name="sitesSize" value="0">

		<tr>
			<td>&nbsp; </td>
    		<td class = "labeleboldwhite" colspan = "13" height = "30"><bean:message bundle = "pm" key = "pmt.allsites.editsites"/>: <bean:write name = "EditSiteForm" property = "msa_name"/>	
    		</td>
		</tr>
  
  		<tr> 
    		<td >&nbsp; </td>
    		<logic:present name = "EditSiteForm" property = "msasearchcombo" scope = "request">
    			<logic:equal name = "EditSiteForm" property = "msasearchcombo" value = "%">
		    		<td class = "tryb" >
		    			<bean:message bundle = "pm" key = "pmt.allsites.msaName"/>
		    		</td>
	    		</logic:equal>
    		</logic:present>
    		<td class = "tryb" >
    			<bean:message bundle = "pm" key = "pmt.allsites.siteName"/>
    		</td>
   
			<td class = "tryb" >
				<bean:message bundle = "pm" key = "pmt.allsites.number"/>
			</td>
			<td class = "tryb" >
				<bean:message bundle = "pm" key = "pmt.allsites.localityFactor"/>
			</td>
			<td class = "tryb" >
				<bean:message bundle = "pm" key = "pmt.allsites.unionsite"/>
			</td>
			
			<td class = "tryb" >
				<bean:message bundle = "pm" key = "pmt.allsites.worklocation"/>
			</td>
		    <td class = "tryb" >
		    	<bean:message bundle = "pm" key = "pmt.allsites.address"/>
		    </td>
		    <td class = "tryb" >
		    	<bean:message bundle = "pm" key = "pmt.allsites.city"/>
		    </td>
		    <td class = "tryb">
		    	<bean:message bundle = "pm" key = "pmt.allsites.state"/>
		    </td>
		    <td class = "tryb" >
		    	<bean:message bundle = "pm" key = "pmt.allsites.zipCode"/>
		    </td>
 
	
    		<td class = "tryb" > 
      				<bean:message bundle = "pm" key = "pmt.allsites.country"/>
    		</td>
    
    		<td class = "tryb" > 
      				<bean:message bundle = "pm" key = "pmt.allsites.primarycontactperson"/>
    		</td>
    
		    <td class = "tryb" > 
		      	<bean:message bundle = "pm" key = "pmt.allsites.primaryphonenumber"/>
		    </td>
    
		    <td class = "tryb" > 
		      	<bean:message bundle = "pm" key = "pmt.allsites.primaryemailid"/>
		    </td>

			<td class = "tryb" > 
      				<bean:message bundle = "pm" key = "pmt.allsites.directions"/>
    		</td>
		    
		    <td class = "tryb"  > 
				<bean:message bundle = "pm" key = "pmt.allsites.designator"/>
			</td>
    
		    <td class = "tryb" > 
		      	<bean:message bundle = "pm" key = "pmt.allsites.secondarycontactperson"/>
		    </td>
		    
		    <td class = "tryb" > 
      				<bean:message bundle = "pm" key = "pmt.allsites.secondaryphonenumber"/>
    		</td>
    		
    		<td class = "tryb" > 
      				<bean:message bundle = "pm" key = "pmt.allsites.secondaryemailid"/>
    		</td>
    		
    		<td class = "tryb" > 
      				<bean:message bundle = "pm" key = "pmt.allsites.specialcondition"/>
    		</td>
    		
	</tr>
  


	
 	<logic:iterate id = "slist" name = "siteList">
	
		<%	
			if ( csschooser == true ) 
			{
				backgroundclass = "texto";
				//labelClass ="labelo";
				labelClass= "textoleftalignnowrap";

				//comboclass = "combooddhidden";
				comboclass = "combooddhiddenrightalign";
				csschooser = false;
				readonlyclass = "readonlytextodd";
				readonlynumberclass = "readonlytextnumberodd";
			}
	
			else
			{
				csschooser = true;	
				//comboclass = "comboevenhidden";
				comboclass = "comboevenhiddenrightalign";
				//labelClass ="labele";
				labelClass ="texteleftalignnowrap";
				
				backgroundclass = "texte";
				readonlyclass = "readonlytexteven";
				readonlynumberclass = "readonlytextnumbereven";
			}

		if(sitesSize==1)
		{
			schdayscheck = "javascript: document.forms[0].siteid.checked = true;";
			displaycombo = "javascript: displaycombo(this,'')";
			valstr = "javascript: document.forms[0].siteid.checked = true;updatetext( this ,'' );"; 
		}
		else if(sitesSize>1){
			schdayscheck = "javascript: document.forms[0].siteid['"+i+"'].checked = true;";
			displaycombo = "javascript: displaycombo(this,'"+i+"')";
			valstr = "javascript: document.forms[0].siteid['"+i+"'].checked = true;updatetext( this ,'"+i+"' );"; 
		}
		%>
		<bean:define id = "val" name = "slist" property = "siteid" type = "java.lang.String"/>
		<bean:define id="dcStateCMUnionsite" name="dcStateCMUnionsite" scope="request" />
		<bean:define id="dcStateCM" name="dcStateCM"  scope="request"/>
		
		<bean:define id="msalist" name="msalist" scope="request" />

		
		
		<tr>	
		
		<td class = "labeleboldwhite"> 
		    	<html:multibox property = "siteid" >
		    		<bean:write name = "slist" property = "siteid"/>	
		    	</html:multibox>
		   	</td>
		   	
		   	<logic:present name = "EditSiteForm" property = "msasearchcombo" scope = "request">
    			<logic:equal name = "EditSiteForm" property = "msasearchcombo" value = "%">
					<td class = "<%= labelClass %>">  
						<bean:write name = "slist" property = "msaName"/>		
					</td>
				</logic:equal>
			</logic:present>
					
			<html:hidden name = "slist" property = "mastersiteid" />
			
			<td class = "<%= backgroundclass %>">  
				<html:text styleId = "<%= "siteName"+i%>" name = "slist"   property = "siteName"  styleClass = "textbox" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
			</td>

			<td class = "<%= backgroundclass %>">  
				<html:text styleId = "<%= "siteNumber"+i%>" name = "slist"   property = "siteNumber" size="29" styleClass = "textbox" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
			</td>
			
			<td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "localityFactor"+i%>" name = "slist" size = "6"  property = "localityFactor" styleClass = "textboxnumber" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextFieldright(this);" onmouseout = "unHighlightTextFieldright( this );"/>
		   </td>
		   
		   <td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "unionSite"+i%>" name = "slist" size = "6"  property = "unionSite"  styleClass = "textbox" onchange = "<%= schdayscheck %>" readonly = "true"   onfocus = "<%= displaycombo %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
				<html:select styleId = "<%= "unionSitecombo"+i%>" name="slist" property="unionSitecombo" size="1" styleClass = "<%= comboclass %>" onblur = "hidecombo( this );"  onchange="<%= valstr %>">
				 <html:optionsCollection name = "dcStateCMUnionsite"  property = "unionSite"  label = "label"  value = "value" />
				</html:select>
		   </td>
		   
		   <td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "worklocation"+i%>" name = "slist"   property = "worklocation" size="29" styleClass = "textbox" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
		   </td>
		   
		   <td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "address"+i%>" name = "slist"   property = "address" size="29" styleClass = "textbox" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
		   </td> 
		   
		   <td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "city"+i%>" name = "slist" size = "14"  property = "city" styleClass = "textbox" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
		   </td> 
		   
		   <td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "state"+i%>" name = "slist"  property = "state" styleClass = "textbox"  size="5" onchange = "<%= schdayscheck %>" readonly = "true"   onfocus = "<%= displaycombo %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
				<html:select  styleId = "<%= "statecombo"+i%>" name = "slist" property = "statecombo" styleClass = "<%= comboclass %>" onblur = "hidecombo( this );"  onchange="<%= valstr %>">
						<html:optionsCollection name = "dcStateCM" property = "firstlevelcatglist" value = "value" label = "label"/> 
				</html:select>
		   </td>
		   
		   <td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "zipcode"+i%>" name = "slist" size = "6" maxlength = "6" property = "zipcode" styleClass = "textbox" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
		   </td>
		   
		    <td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "country"+i%>" name = "slist"  property = "country" styleClass = "textbox"  size="12" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
		   </td>
		   
		   <td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "primaryContactPerson"+i%>" name = "slist" size = "20"  property = "primaryContactPerson" styleClass = "textbox" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
		   </td>
		   
		    <td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "primaryPhoneNumber"+i%>" name = "slist"   property = "primaryPhoneNumber" styleClass = "textboxnumber" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextFieldright(this);" onmouseout = "unHighlightTextFieldright( this );"/>
		   </td> 
		   
		    <td class = "<%= backgroundclass %>">  
				<html:text styleId = "<%= "primaryEmailId"+i%>" name = "slist"   property = "primaryEmailId"  styleClass = "textbox" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
			</td>
			
		   <td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "directions"+i%>" name = "slist"  size = "40"  property = "directions" styleClass = "textbox" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
		   </td>
			
		   <td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "designator"+i%>" name = "slist"  size = "6"  property = "designator" styleClass = "textbox" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
		   </td>
			
		   <td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "secondaryContactPerson"+i%>" name = "slist" size = "6"  property = "secondaryContactPerson" styleClass = "textbox" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
		   </td> 
		   
		   <td class = "<%= backgroundclass %>">
				<html:text styleId = "<%= "secondaryPhoneNumber"+i%>" name = "slist"   property = "secondaryPhoneNumber" styleClass = "textboxnumber" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextFieldright(this);" onmouseout = "unHighlightTextFieldright( this );"/>
		   </td>
			
		   <td class = "<%= backgroundclass %>">
				<html:text styleId = "secondaryEmailId" name = "slist"  property = "secondaryEmailId" styleClass = "textbox" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
		   </td>
		   
		   <td class = "<%= backgroundclass %>">
				<html:text styleId = "specialCondition" name = "slist"   property = "specialCondition" styleClass = "textbox" onchange = "<%= schdayscheck %>" onmouseover = "highlightTextField(this);" onmouseout = "unHighlightTextField( this );"/>
		   </td>

	  </tr>
	  <% i++;  %>
	  </logic:iterate>
		
		<tr>	
			<td >&nbsp; </td>
		  	<logic:present name = "EditSiteForm" property = "msasearchcombo" scope = "request">
    			<logic:equal name = "EditSiteForm" property = "msasearchcombo" value = "%">
				  	<td colspan = "19" class = "buttonrow">
				 </logic:equal>
			
    			<logic:notEqual name = "EditSiteForm" property = "msasearchcombo" value = "%">
				  	<td colspan = "18" class = "buttonrow">
				 </logic:notEqual>
			</logic:present>
				  		<html:submit property = "submit" styleClass = "button" onclick = "return validate();"/>	
		  				<html:button property = "sort" styleClass = "button" onclick = "javascript:return sortwindow();"><bean:message bundle = "AM" key = "am.tabular.sort"/></html:button>
					</td>
				
		
			<td class = "buttonrow" align="right">
				<html:reset property = "cancel" styleClass = "button"><bean:message bundle = "pm" key = "pmt.allsites.cancel"/></html:reset>
			</td>
	</tr>
	</logic:notEqual>
		
	</table>
	</td>

</tr>
</table>

</logic:equal>
  
</html:form>

<script>
function validate()
{
	var submitflag = 'false';
	if( <%= sitesSize %> != 0 )
	{
		if( <%= sitesSize %> == 1 )
		{
			if( !( document.forms[0].siteid.checked )  )
			{
				alert( "<bean:message bundle = "pm" key = "pmt.allsites.selectasitetoedit"/>" );
				return false;
			}
			else
			{
			
				//Check for locality Factor
		  		if( document.forms[0].localityFactor.value != "" )
				{
					if( !isFloat( document.forms[0].localityFactor.value ) ) 
					{ 
						alert(  "<bean:message bundle = "pm" key = "pmt.allsites.localityfactorshouldbenumeris"/>" );
						return false;
					}
				}
			
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].siteid.length; i++ )
		  	{
		  		if( document.forms[0].siteid[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		
			  		//Check for locality Factor
			  		if( document.forms[0].localityFactor[i].value != "" )
					{
						if( !isFloat( document.forms[0].localityFactor[i].value ) ) 
						{ 
							
							alert(  "<bean:message bundle = "pm" key = "pmt.allsites.localityfactorshouldbenumeris"/>" );
						
							return false;
						}
					}
					//Check For Name ,site No.,country,state
					if( document.forms[0].siteName[i].value == "" )
					{
							
							alert( "<bean:message bundle = "pm" key = "pmt.allsites.sitenamecannotbeblank"/>" );
				 			document.forms[0].siteName[i].focus();
							return false;
					}
					if( document.forms[0].siteNumber[i].value == "" )
					{
							

							alert( "<bean:message bundle = "pm" key = "pmt.allsites.sitenumbercannotbeblank"/>" );
							document.forms[0].siteNumber[i].focus();
							return false;
					}
					if( document.forms[0].country[i].value == "" )
					{
							

							alert( "<bean:message bundle = "pm" key = "pmt.allsites.countrycannotbeblank"/>" );
							document.forms[0].country[i].focus();
							return false;
					}
					
					if( document.forms[0].statecombo[i].value == "0" )
					{

							alert( "<bean:message bundle = "pm" key = "pmt.allsites.statecannoybeblank"/>" );
				 			document.forms[0].state[i].focus();
							return false;
					}
				}
		  	}
		  	if( submitflag == 'false'  )
		  	{
				alert( "<bean:message bundle = "pm" key = "pmt.allsites.selectasitetoedit"/>" );
				return false;
		  	}
		}
	}
	else
	{
			alert( "<bean:message bundle = "pm" key = "pmt.allsites.selectasitetoedit"/>" );
			return false;
	}
	if(submitflag =='true')
	{
		
		return true;
	}
	else
	{
		if( !isFloat( document.forms[0].localityFactor.value ) )
		{
			alert( "<bean:message bundle = "pm" key = "pmt.allsites.localityfactorshouldbenumeris"/>" );
			return false;
		}
		
	}
	
}

function sortwindow()
{
		
	str = 'SortAction.do?Type=Masters&msaid=<bean:write name = "EditSiteForm" property = "msasearchcombo" />&sitename=<bean:write name = "EditSiteForm" property = "sitenamesearch" />&sitenumber=<bean:write name = "EditSiteForm" property = "sitenumbersearch" />&sitecity=<bean:write name = "EditSiteForm" property = "sitecitysearch" />&flag=<bean:write name = "EditSiteForm" property = "flag" />&stateid=<bean:write name = "EditSiteForm" property = "statesearchcombo" />';

	p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' ); 
	return true;
}

function searchforsite()
{
	
	document.forms[0].flag.value ='false';
	return true;	
}


</script>
</body>
</html>