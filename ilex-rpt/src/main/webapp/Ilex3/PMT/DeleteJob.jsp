<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<html:html>
<head>
	<title>Job Deletion</title>
	<%@ include file="/Header.inc" %>
	<LINK href="styles/style.css" rel="stylesheet" type="text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css"> 
	<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language="JavaScript" src="javascript/ilexGUI.js"></script>
</head>
<%
	String backgroundclass = null;
	String backgroundhyperlinkclass = null;
	String backgroundclasstoleftalign = null;
	boolean csschooser = true;
	int jobSize = 0;
	if(request.getAttribute("Size")!= null && !request.getAttribute("Size").equals(""))
		jobSize = ( int ) Integer.parseInt( request.getAttribute( "Size" ).toString() ); 
	
%>
<body>
	<html:form action ="/DeleteJobAction">
	<table border="0" cellspacing="1" cellpadding="1" width=100%>
		<tr>
		  <td  width="1" height="0"></td>
		  <td>	
			<table border="0" cellspacing="1" cellpadding="1" > 
				<tr><td class = "labeleboldwhite"><bean:message bundle = "pm" key = "pmt.deletejobpagetitle"/></td></tr>
				<tr height="2"><td></td></tr>
				<tr><td class = "dbvaluesmall"><bean:message bundle = "pm" key = "pmt.deletejobpagecomments"/></td></tr>
				<tr height="2"><td></td></tr>
				<logic:present name="deleteSqlFlag" scope="request">
					<logic:equal name = "deleteSqlFlag" value="0">
						<tr><td class="message" height="30"><bean:message bundle = "pm" key = "pmt.delete.success"/></td></tr>
					</logic:equal>
				</logic:present>
				<logic:present name="deleteSqlFlag" scope="request">
					<logic:equal name = "deleteSqlFlag" value="1">
						<tr><td class="message" height="30"><bean:message bundle = "pm" key = "pmt.delete.sql.error"/></td></tr>
					</logic:equal>
				</logic:present>
				<logic:present name="deleteMySqlFlag" scope="request">
					<logic:equal name = "deleteMySqlFlag" value="1">
						<tr><td class="message" height="30"><bean:message bundle = "pm" key = "pmt.delete.mysql.error"/></td></tr>
					</logic:equal>
				</logic:present>
				<tr><td class = "labeleboldwhite"><bean:message bundle = "pm" key = "pmt.searchjobtodelete"/></td></tr>
				<tr>
					<td class="labeleboldwhite"><bean:message bundle = "pm" key = "pmt.jobtitle"/>
											<html:text name ="DeleteJobForm" property = "jobKeyword"></html:text>&nbsp;&nbsp; 
												<html:submit styleClass="button_c" property = "jobSearch" onclick = "return validate();"><bean:message bundle="pm" key="pmt.alljobs.search" /></html:submit>
					</td>
				</tr>
				<tr><td class = "labeleboldwhite">&nbsp;</td></tr>
			</table>
		 </td>	
		</tr>
	</table>
	
	<logic:present name = "codeslist" property = "joblist" > 
	<table border="0" cellspacing="1" cellpadding="1" width="100%">
		<tr><td width="1" height="0"></td>
			<td class = "labeleboldwhite"><bean:message bundle = "pm" key = "pmt.alljobs.search.result"/></td>
		</tr>
		<tr>
		  <td width="1" height="0"></td>
		  <td>
		  	  <table border="0" cellspacing="1" cellpadding="1"> 
		  	  	<tr>
		  			<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.alljobs.customer"/></td>
  					<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.alljobs.appendix"/></td>
  					<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.alljobs.job"/></td>
  					<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.alljobs.status"/></td>
  					<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.alljobs.owner"/></td>
  					<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.alljobs.schedule"/></td>
  					<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.alljobs.pos"/></td>
  					<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.alljobs.deletecheck"/></td>
		  		</tr>
		  		<%if(jobSize == 0){ %>
		  			<tr><td class="dbvaluesmall" colspan="8">
		  					<bean:message bundle = "pm" key = "pmt.alljobs.zero.mesage"/>
		  				</td></tr>
		  		<%}else if(jobSize != 0 && jobSize > 50){%>
		  			<tr><td class="dbvaluesmall" colspan="8">
		  					<bean:message bundle = "pm" key = "pmt.alljobs.exceeds.fifty"/>
		  				</td></tr>		  		
		  		<%}else if(jobSize != 0 && jobSize < 51){%>
				 	<logic:iterate id = "jbs" name = "codeslist" property = "joblist">
						<bean:define id = "status" name = "jbs" property = "jobOrgStatus" type = "java.lang.String"/>
						<bean:define id = "POCheck" name = "jbs" property = "jobPowoCheck" type = "java.lang.String"/>
							<%	
								if ( csschooser == true ){
									backgroundclass = "Ntexto";
									backgroundclasstoleftalign ="Ntextoleftalignnowrap";
									backgroundhyperlinkclass = "Nhyperodd";
									csschooser = false;
								}else{
									csschooser = true;	
									backgroundclass = "Ntexte";
									backgroundclasstoleftalign = "Ntexteleftalignnowrap";
									backgroundhyperlinkclass = "Nhypereven";
								}
							%>
							<tr>
								<td class = "<%= backgroundclass %>"><bean:write name = "jbs" property = "msaName" /></td>
								<td class = "<%= backgroundclass %>"><bean:write name = "jbs" property = "appendixName" /></td>
								<td class = "<%= backgroundclass %>"><bean:write name = "jbs" property = "jobName" /></td>
								<td class = "<%= backgroundclass %>"><bean:write name = "jbs" property = "jobStatus" /></td>
								<td class = "<%= backgroundclass %>"><bean:write name = "jbs" property = "jobOwner" /></td>
								<td class = "<%= backgroundclass %>" align="center"><bean:write name = "jbs" property = "jobScheduleCheck" /></td>
								<td class = "<%= backgroundclass %>" align="center"><bean:write name = "jbs" property = "jobPowoCheck" /></td>
								<%if ( status.equals("I") && POCheck.equals("No") ){ %>
										<td class = "<%= backgroundhyperlinkclass %>" align="center"><a href = "MasterDeleteAction.do?type=PRMJob&jobId=<bean:write name = "jbs" property = "jobId"/>" onclick= "return confirm('<bean:message bundle = "pm" key = "pmt.alljobs.confirm.msg"/>','Job Deletion');"><bean:message bundle = "pm" key = "pmt.alljobs.yes"/></a></td>
								<%}else{%>
									<td class = "<%= backgroundclass %>" align="center"><bean:message bundle = "pm" key = "pmt.alljobs.no"/></td>
								<%}%>
							</tr>
			  		</logic:iterate>
				<%}%>			  		
		  	  </table>  	
		 </td>
		</tr>
	</table>  
	</logic:present>
	</html:form>			 
</body>
<script>
function validate() {
	trimFields();
	if(isBlank(document.forms[0].jobKeyword.value)) {
		warningEmpty(document.forms[0].jobKeyword,"","<bean:message bundle = "pm" key = "pmt.alljobs.empty.error"/>");
		return false;
	}else{
		return true;
	}
}

</script>

</html:html>