
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<html:html>
<head>
<%@ include  file="/Header.inc" %>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>

<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">

<script language="javascript" src="../javascript/ilexGUI.js"></script>
<title>Hierarchy Navigation</title>

<link rel="stylesheet" href="../styles/style.css" type="text/css">
</head>

<body   text="#000000" marginwidth="0" marginheight="0" background="../images/sidebg.jpg" onLoad="MM_preloadImages('../images/t_msa.gif','../images/t_appendix.gif')">

<table border="0" cellspacing="1" cellpadding="0" >
   <tr>
    <td height="30">&nbsp;</td>
    <td class="toplink" height="20" ><b><bean:message bundle = "pm" key = "pmt.pmt"/></b></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
    <td class="link" height="20" ><img src='../images/t_msa.gif' width=15 height=15 border=0></img><a href="../EditXmlAction.do?Type=MSA&EditType=PMT" target="ilexmain"><bean:message bundle = "pm" key = "pmt.msa"/></a></td>
   </tr>
  
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img><a href="../EditXmlAction.do?Type=NetMedX&EditType=PMT" target="ilexmain"><bean:message bundle = "pm" key = "pmt.netmedx"/></a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img><a href="../EditXmlAction.do?Type=Appendix&EditType=PMT" target="ilexmain"><bean:message bundle = "pm" key = "pmt.appendix"/></a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img><a href="../EditXmlAction.do?Type=Hardware&EditType=PMT" target="ilexmain"><bean:message bundle = "pm" key = "pmt.hardware"/></a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img><a href="../EditMasterAction.do?type=emailbody" target="ilexmain"><bean:message bundle = "pm" key = "pmt.email"/></a></td>
   </tr>
   
    <tr> 
    <td height="20">&nbsp;</td>
<!-- <td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img><a href="../EditMasterAction.do?type=poterm" target="ilexmain"><bean:message bundle = "pm" key = "pmt.poterms"/></a></td> -->
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img><a href="../EditMasterAction.do?type=poterm" target="ilexmain">Standard PO Terms and Conditions</a></td>
   </tr>
   
    <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img><a href="../EditMasterAction.do?type=speedterm" target="ilexmain">SPEEDPAY PO Terms and Conditions</a></td>
   </tr>
   
    <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img><a href="../EditMasterAction.do?type=minutemanterm" target="ilexmain">Minuteman PO Terms and Conditions</a></td>
   </tr>
   
    <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img><a href="../EditMasterAction.do?type=pospinst" target="ilexmain"><bean:message bundle = "pm" key = "pmt.pospinst"/></a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img>
	<a href="../MasterchecklisttoAppendixcopyAction.do" target="ilexmain"><bean:message bundle = "pm" key = "pmt.processchecklist"/></a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img><a href="../DeleteJobAction.do?flag=true" target="ilexmain"><bean:message bundle = "pm" key = "pmt.deletejob"/></a></td>
   </tr>
  <!--  End :Added By Amit,For Showing all sites for all MSA -->
 </table>

</body>
</html:html>