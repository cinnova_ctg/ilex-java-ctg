<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%

%>
<html:html>
<head>
	<title><bean:message bundle = "pm" key = "editemail.title"/></title>
	<%@ include file = "/Header.inc" %>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css" />
</head>
<body>
<html:form action = "/EditMasterAction">
<html:hidden property="type" />
<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>
  			<td  width="2" height="0"></td>
  			  <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width = "900"> 
		    		<logic:present name = "retval" scope = "request">
				    	<logic:notEqual name = "retval" value = "-9001">
				    		<tr>
				    			<td  colspan = "2" class = "message" height = "30">
				    				 <logic:equal name="EditMasterForm" property="type" value="emailbody">
				    					<bean:message bundle = "pm" key = "editemail.updatemessage"/>
				    				 </logic:equal>	
				    				 
				    				 <logic:equal name="EditMasterForm" property="type" value="pospinst">
				    					<bean:message bundle = "pm" key = "pospinst.updatemessage"/>
				    				 </logic:equal>
				    				 
				    				 <logic:equal name="EditMasterForm" property="type" value="poterm">
				    					<bean:message bundle = "pm" key = "poterm.updatemessage"/>
				    				 </logic:equal>	
				    				 
				    				 <logic:equal name="EditMasterForm" property="type" value="speedterm">
				    					<bean:message bundle = "pm" key = "speedterm.updatemessage"/>
				    				 </logic:equal>	
				    				 
				    				 <logic:equal name="EditMasterForm" property="type" value="minutemanterm">
				    					<bean:message bundle = "pm" key = "minutemanterm.updatemessage"/>
				    				 </logic:equal>	
				    				 
				    				 
				    			</td>
				    		</tr>
						</logic:notEqual>
			    	</logic:present>
		    		
		    		<tr>    
						<td colspan = "2" class = "labeleboldwhite" height = "30" >
							<logic:equal name="EditMasterForm" property="type" value="emailbody">
								<bean:message bundle = "pm" key = "editemail.edit"/>
							</logic:equal>
							<logic:equal name="EditMasterForm" property="type" value="pospinst">
	    						<bean:message bundle = "pm" key = "pospinst.edit"/>
	    					</logic:equal>		
							<logic:equal name="EditMasterForm" property="type" value="poterm">	
								<bean:message bundle = "pm" key = "poterm.edit"/>
							</logic:equal>
							
							<logic:equal name="EditMasterForm" property="type" value="speedterm">	
								<bean:message bundle = "pm" key = "speedterm.edit"/>
							</logic:equal>
							<logic:equal name="EditMasterForm" property="type" value="minutemanterm">	
								<bean:message bundle = "pm" key = "minutemanterm.edit"/>
							</logic:equal>
							
							
							
							
							
							
						</td>	
					</tr> 
					
					<tr>
  						<td colspan = "2"><img src = "images/spacer.gif" width = "1" height = "1"></td>
  					</tr>
  					
 					 <tr>
			 		  	<td class = "labeloboldtop">
			 		  		<logic:equal name="EditMasterForm" property="type" value="emailbody">
								<bean:message bundle = "pm" key = "editemail.message"/>
							</logic:equal>	
							<logic:equal name="EditMasterForm" property="type" value="poterm">	
								<bean:message bundle = "pm" key = "poterm.message"/>
							</logic:equal>
							<logic:equal name="EditMasterForm" property="type" value="pospinst">	
								<bean:message bundle = "pm" key = "pospinst.message"/>
							</logic:equal>
							<logic:equal name="EditMasterForm" property="type" value="speedterm">	
								<bean:message bundle = "pm" key = "speedterm.message"/>
							</logic:equal>
							<logic:equal name="EditMasterForm" property="type" value="minutemanterm">	
								<bean:message bundle = "pm" key = "minutemanterm.message"/>
							</logic:equal>
							
							
							
			 		  	</td> 
			 		  	<td class = "labelo">
			 		  		<html:textarea property = "messageContent" rows = "40" cols = "128" styleClass = "textbox"/>
			 		  	</td>
  		 		    </tr>
  		 		    
  		 		    <tr>
			 		  	<td colspan = "3" class = "buttonrow">
			 		  		<html:submit property = "save" styleClass = "button" onclick = "return validate();"><bean:message bundle = "pm" key = "common.addcommentpage.add"/></html:submit>
			 		  		<html:reset property = "cancel" styleClass = "button"><bean:message bundle = "pm" key = "msa.editxml.cancel"/></html:reset>
			 		  		<html:button property="back" styleClass="button" onclick = "history.go(-1);"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
			 		  </tr>
  					
 				</table>
			</td>
		</tr>
</table>
</html:form>


<script>
function validate()
{
	if( document.forms[0].messageContent.value == "" )
	{
		alert( "<bean:message bundle = "pm" key = "editemail.entermessage"/>" );
		return false;
	}
	
	return true;
}
</script>
</body>
</html:html>