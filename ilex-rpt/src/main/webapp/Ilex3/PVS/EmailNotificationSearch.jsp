<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<% int contact_size =( int ) Integer.parseInt( request.getAttribute( "contactlistsize" ).toString() ); %>

<%
	String backgroundclass = null;
	boolean csschooser = true;
	int i = 0;
	
	String setvalue = "";
%>


<html:html>
<body>
	<HEAD>
		<%@ include  file = "/Header.inc" %>
		<META http-equiv = "Content-Style-Type" content = "text/css">
		<LINK href = "styles/style.css" rel = "stylesheet" type = "text/css">
		<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
		<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
		<%@ include  file = "/Menu.inc" %>
	
	</head>
	<html:form action = "/EmailNotificationSearch" >
	
  					<table border = "0" cellspacing = "1" cellpadding = "1" > 
	  					<tr>
		  					<td class = "labeleboldwhite">
		  						<bean:message bundle = "CM" key = "cm.search.searchcontacts"/>
		  					</td>
	  					</tr>
  						<tr class = "labelobold">
	  						<td><bean:message bundle = "PVS" key = "pvs.emailnotification.pocname"/></td>
							<td  colspan = "1" >
								<bean:message bundle = "PVS" key = "pvs.emailnotification.divison"/>
							</td>
						</tr>
		
						<tr>
							<td>
								<html:text property = "pocname" size = "20" styleClass = "textbox" />
							</td>
							
							<td>
								<html:text property = "divison" size = "20" styleClass = "textbox" />
							</td>
							
							<td>
								<html:submit property = "search" styleClass = "button">
									<bean:message bundle = "PVS" key = "pvs.emailnotification.search"/>
								</html:submit>
							</td>
						</tr>
					</table>

					
					
					<% if( contact_size > 0 ) { %>
					<table>
						<TR>
							<TD class = labeleboldwhite>&nbsp;</TD>
							<TD class = tryb><bean:message bundle = "PVS" key = "pvs.emailnotification.pocname"/></TD>
							<TD class = tryb><bean:message bundle = "PVS" key = "pvs.emailnotification.divison"/></TD>
							<TD class = tryb><bean:message bundle = "PVS" key = "pvs.emailnotification.emailid"/></TD>
							
								
  				   		</TR>
						
						<logic:iterate id = "list" name = "EmailNotificationSearchForm" property = "contactlist">
						<%	
							if ( csschooser == true ) 
							{
								backgroundclass = "readonlytexteven";
								csschooser = false;
							}
					
							else
							{
								backgroundclass = "readonlytextodd";
								csschooser = true;		
							}
							
						
							if( contact_size == 1 ) 
							{ 
								setvalue = "javascript:assignaddress( '' );";
							}
							else
							{
								setvalue = "javascript:assignaddress( '"+i+"' );";
							}
						%>
							<bean:define id = "val" name = "list" property = "emailid" type = "java.lang.String"/>
							
							<tr>
								<td height="20">
									<html:checkbox property="poccheckbox"  />
									
								</td>
								
								<td class = "<%= backgroundclass %>">
									<bean:write name = "list" property = "pocname" />
									<html:hidden name = "list" property = "pocname" />
								</td>
								
								<td class = "<%= backgroundclass %>">
									<bean:write name = "list" property = "divison" />
									<html:hidden name = "list" property = "divison" />
								</td>
								
								<td class = "<%= backgroundclass %>">
									<bean:write name = "list" property = "emailid" />
									<html:hidden name = "list" property = "emailid" />
								</td>
								
								
							</tr>
					
					 <% i++;  %>
					</logic:iterate>
					</table>
					<%} %>
					
				
					<table>
						<tr>
							<td></td>
							<td colspan = "2">
								<html:button property = "save" styleClass = "button" onclick = "javascript:setemailvalue();"><bean:message bundle = "PVS" key = "pvs.emailnotification.ok"/></html:button>
								<html:button property = "close" styleClass = "button" onclick = "javascript:window.close();"><bean:message bundle = "PVS" key = "pvs.emailnotification.close"/></html:button>
							</td>
						</tr>
					
					</table>
		
				
	</html:form>
<body>
<script>



function setemailvalue()
{
	var emailstring = '';
	
	if( <%= contact_size %> != 0 )
	{
		if( <%= contact_size %> == 1 )
		{
			if( ( document.forms[0].poccheckbox.checked ) )
			{
				emailstring=document.forms[0].emailid.value;
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].poccheckbox.length; i++ )
		  	{
		  		if( document.forms[0].poccheckbox[i].checked ) 
		  		{
		  			emailstring=emailstring+document.forms[0].emailid[i].value+',';
		  		}
		  	}
		  	emailstring=emailstring.substring(0,emailstring.lastIndexOf(","));
		  	
		  	//emailstring=emailstring.substring(0,emailstring.length()-1);
		}
		//alert(emailstring);
	}
	
	
	window.opener.document.forms[0].emaillist.value = emailstring;
	
	 
	window.close();
	
}

</script>



</html:html>
