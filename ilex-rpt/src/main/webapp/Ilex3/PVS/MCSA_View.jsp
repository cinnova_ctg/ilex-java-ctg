<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For MCSA version of the partner.
*
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"
	type="text/css">

<script language="javascript" src="javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>	
<title></title>


<%@ include  file="/NMenu.inc" %>





</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('../images/sales1b.gif','../images/services1b.gif','../images/about1b.gif','../images/cust-serv1b.gif','../images/offers1b.gif')" >



<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td  valign="top" width = "100%"> 
      <table  border="0" cellspacing="0" cellpadding="0" height="18"  width = "100%">
        <tr> 
         <td id="pop1" width="120" class="Ntoprow1" align="center"><a href="#"  onmouseout="MM_swapImgRestore();popDown('elMenu1',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.managepartner"/></center></a></td> 
         <td  width="120" align=left class="Ntoprow1" align="center"><a href="Partner_Search.do?ref=search&type=v"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.partnersearch"/></center></a></td>
         <td width="120"  class="Ntoprow1" align="center"><a  href="MCSA_View.do"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.partneredit.managemcsa"/></center></a></td>		 
		 <td   class="Ntoprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<SCRIPT language=JavaScript1.2>

if (isMenu) {
//start
arMenu1 = new Array(
120,
findPosX( 'pop1' ),findPosY( 'pop1' ),
"","",
"","",
"","",
"Minuteman","#",1,
"Standard Partner","#",1,
"New Partners","PVSSearch.do?orgTopName=all&action=newadded&enablesort=true",0,
"Recent Updates","PVSSearch.do?orgTopName=all&action=resentupdated&enablesort=true",0,
"Annual Review Due","PVSSearch.do?orgTopName=all&action=annualreport&enablesort=true",0
)
arMenu1_1=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Minuteman Partners",0,
"Update","PVSSearch.do?orgTopName=Minuteman Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Minuteman Partners&action=delete",0
<%}%>
)
arMenu1_2=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Certified Partners",0,
"Update","PVSSearch.do?orgTopName=Certified Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Certified Partners&action=delete",0
<%}%>
)

document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}
//end

if (!isMenu) {
 arMenu1=new Array(
120,"","",
"","",
"","",
"","",
"Edit","Partner_Edit.jsp",0,
"Delete","#",0
)

arMenu2=new Array(
120,"","",
"","",
"","",
"","",
"View","ViewIncidentReport.jsp",0,
"Post","AddIncident.jsp",0
)
  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}

</script>


<table>
<html:form action="MCSA_View" enctype = "multipart/form-data">

<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<logic:notPresent name="mcsa_upload" scope="request">
<tr>
  <td  width="2" height="0"></td>
  <td><table border="0" cellspacing="2" cellpadding="2" width="500">
		<logic:present name="savedsuccessfully" scope="request">  
		 <tr>
	  	 	<td colspan="2" class="message" height="30" ><bean:message bundle="PVS" key="pvs.mcsaSavedSuccessfully"/></td>
	     </tr>		
		</logic:present>
	  <tr>
	  	<td colspan="2" class="labeleboldwhite" height="30" ><bean:message bundle="PVS" key="pvs.managemcsa"/></td>
	  </tr>
	   

	  
 	  <tr>
		<td   class="labeloboldtop"  height="20" width="100"><bean:message bundle="PVS" key="pvs.mcsa"/></td>
	 	<td><p>
	 		<html:textarea styleClass="textbox" property="mcsa" cols="90%" rows="15"></html:textarea>
	 	</p></td>
 	</tr>

 	<tr>
		<td  class="labelebold"  height="20"><bean:message bundle="PVS" key="pvs.version"/></td>
		<td   class="labele"><bean:write name="MCSA_ViewForm" property="version"/></td>
	</tr>
	
	<tr>
		<td   class="labelobold"  height="20"><bean:message bundle="PVS" key="pvs.date"/></td>
		<td  class="labelo"><html:text  styleClass="textbox" size="10" property="date" readonly = "true"/>
        <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].date, document.forms[0].date, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
	</tr>

 	<tr> 
	    <td colspan="2" class="buttonrow"> 
	      <html:submit property="save" styleClass="button"><bean:message bundle="PVS" key="pvs.save"/></html:submit>
	      <html:reset property="reset" styleClass="button"><bean:message bundle="PVS" key="pvs.reset"/></html:reset>
	    </td>
  	</tr>
  	
 	<tr>
 		<td colspan="4" ><img src="images/spacer.gif" width="1" height="1"></td>
 	</tr>
 
    <tr>    
		<td   class="labellobold"  height="20" width="200"><bean:message bundle="PVS" key="pvs.version"/></td>
		<td   class="labellobold"  height="20" width="500"><bean:message bundle="PVS" key="pvs.date"/></td>
 	</tr>
 	<%int i=0; String label=""; %>
	<logic:present name="mcsa_details" scope="request">
	<logic:iterate id="mcsa" name="mcsa_details">
	<% if (i%2==0)label="labele";
	   if (i%2==1)label="labelo"; %>
	   <tr>
	   	<td class=<%=label%> height="20"><bean:write name="mcsa" property="mcsa_version"/></td>
	   	
	   	<td class=<%=label%> ><a href='MCSA_View.do?mcsa_id=<bean:write name="mcsa" property="mcsa_id"/>'><bean:write name="mcsa" property="mcsa_date"/></a></td>
	   </tr>
	   <%i++; %>
	</logic:iterate>
	</logic:present>
	
	<tr  align="right"> 
    	<td colspan="2" height="37"><br></td>
  	</tr>
  	</logic:notPresent>
  	
  	<logic:present name="mcsa_upload"  scope="request">
  	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="2" cellpadding="2" width="500">
 	<tr>
		<td   class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.noMCSApresent"/></td></tr>
	<tr>
	    <td class = "labele"><bean:message bundle="PVS" key="pvs.mcsa"/> 
	      <html:file property="browse" styleClass="button"/>
	    </td>
  	</tr>
	<tr>
	    <td colspan="2" class="buttonrow">
	      	      <html:submit property="upload" styleClass="button" onclick="return validateUpload();"><bean:message bundle="PVS" key="pvs.upload"/> </html:submit>
	    </td>
  	</tr>
  	
 	</logic:present>
  	
   <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
    </jsp:include>
 </table>
 </td>
 </tr>
</table>
</html:form>
</table>
</BODY>
<script>
function validateUpload(){
	if (document.forms[0].browse.value==""){
    	alert("<bean:message bundle="PVS" key="pvs.pleaseselectfile"/>");
	    return false;
    }// End of if

    return true;
}
</script>
</html:html>
