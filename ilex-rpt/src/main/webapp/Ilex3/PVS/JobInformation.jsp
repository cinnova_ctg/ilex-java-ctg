<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>


<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.tabular.title"/></title>
	<%@ include file="/Header.inc" %>
	<%@ include  file="/SendEmail.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/summary.css" type="text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "javaScript" src = "javascript/JLibrary.js"></script>
</head>
<%@ include  file = "/NMenu.inc" %>
<%

int counter = -1;

if( session.getAttribute( "countersort" ) != null )
{
	counter = Integer.parseInt( ( String )session.getAttribute( "countersort" ) );
	session.setAttribute( "countersort" , ""+counter );
}
else
{
	session.setAttribute("countersort", "-1");
}


int sitesJobSize = 0;

if( request.getAttribute( "sitesJobSize" ) != null )
{
	sitesJobSize = ( int ) Integer.parseInt( request.getAttribute( "sitesJobSize" ).toString() ); 

}
else
{
	sitesJobSize = 0;
}
	String displaycombo = null;
	String backgroundclass = "texto";
	String labelClass = null;
	String valstr =null;
	
	String readonlyclass = null;
	String readonlynumberclass = null;
	String backnew = "";
	int i = 0;
	String comboclass = null;

	boolean csschooser = true;
	
	
%>
<html:form action = "/JobInformationperPartner">

<html:hidden property = "partnerid" />
<html:hidden property = "from" />

<body onLoad = "MM_preloadImages('images/Expand.gif','images/Collapse.gif');">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top"> 
      <table  border="0" cellspacing="0" cellpadding="0" width = "100%">
        <tr> 
	        <td id="pop1" width="100" class="Ntoprow1" align="center"><a href="#"  onmouseout="MM_swapImgRestore();popDown('elMenu2',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.manageprofile"/></center></a></td>
	        <td id="pop2" width="100" class="Ntoprow1" align="center"><a href="#"  onmouseout="MM_swapImgRestore();popDown('elMenu1',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 124px"><center><bean:message bundle="PVS" key="pvs.managepartner"/></center></a></td>          	        
	        <td  width="100"  class="Ntoprow1" align="center"><a href="Partner_Search.do?ref=search&type=v"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.partnersearch"/></center></a></td>
			<td width="100"  class="Ntoprow1" align="center"><a  href="MCSA_View.do"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.partneredit.managemcsa"/></center></a></td>	        
	        <td class="Ntoprow1">&nbsp;</td>
        </tr>
      </table>
    </td>
   </tr>
</table>
  
<SCRIPT language=JavaScript1.2>

if (isMenu) {
//start
arMenu1 = new Array(
120,
findPosX( 'pop2' ),findPosY( 'pop2' ),
"","",
"","",
"","",
"Minuteman","#",1,
"Standard Partner","#",1,
"New Partners","PVSSearch.do?orgTopName=all&action=newadded&enablesort=true",0,
"Recent Updates","PVSSearch.do?orgTopName=all&action=resentupdated&enablesort=true",0,
"Annual Review Due","PVSSearch.do?orgTopName=all&action=annualreport&enablesort=true",0
)
arMenu1_1=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Minuteman Partners",0,
"Update","PVSSearch.do?orgTopName=Minuteman Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Minuteman Partners&action=delete",0
<%}%>
)
arMenu1_2=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Certified Partners",0,
"Update","PVSSearch.do?orgTopName=Certified Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Certified Partners&action=delete",0
<%}%>
)
//end

 arMenu2=new Array(
120,
findPosX('pop1'),findPosY('pop1'),
"","",
"","",
"","",
"Update","Partner_Edit.do?pid=<bean:write name = "JobInformationForm" property = "partnerid" />&function=Update",0,
"Incident Reports","#",1,
"Job History","javascript:jobHistory();",0

<c:if test="${sessionScope.RdmRds eq 'Y'}"> 
	<c:if test="${requestScope.topPartnerName ne 'Minuteman Partners'}"> 	
		,"Convert To Minuteman","Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="JobInformationForm" property="partnerid"/>&convert=M",0
	</c:if>
	<c:if test="${requestScope.topPartnerName ne 'Certified Partners'}"> 	
		,"Convert To Standard","Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="JobInformationForm" property="partnerid"/>&convert=S",0
	</c:if>
</c:if>

<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>		
,"Delete","javascript:del();",0
<%}%>
<c:if test="${sessionScope.RdmRds eq 'N'}"> 
	,"Request Restriction","javascript:gettingAjaxDataForEmail('<bean:write name="JobInformationForm" property="partnerid"/>');",0
</c:if>
<c:if test="${sessionScope.RdmRds eq 'Y'}"> 
	<c:if test="${sessionScope.partnerRegStatus eq 'R'}"> 
		,"Send Username/Password","javascript:reSendUnamePwd('<bean:write name="JobInformationForm" property="partnerid"/>');",0
	</c:if>
	<c:if test="${sessionScope.partnerRegStatus eq 'N'}">
		,"Re-send Registration Email","javascript:reSendRegEmail('<bean:write name="JobInformationForm" property="partnerid"/>');",0
	</c:if>
</c:if>
)
arMenu2_2=new Array(
"View","AddIncident.do?pid=<bean:write name = "JobInformationForm" property = "partnerid" />&function=view&page=PVS",0,
"Post","AddIncident.do?pid=<bean:write name = "JobInformationForm" property = "partnerid" />&function=add&page=PVS",0
)

  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}
</script>
</table>


<table > <!-- Table 6 start -->
<tr height="20"><td>&nbsp;</td></tr>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0" align = "center"><!-- Table 4 Start -->
  			
  			<%if( sitesJobSize == 0 ) {%>
  			<tr>
  			<td>&nbsp; </td>
				<td class = "message"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.noJobAssociatedForThePartner"/></td>
			</tr>	
				<%}else{} %>
  			
  			<tr>
	 			<td>&nbsp; </td>
	 			
	 			<td colspan = "10" class = "labeleboldwhite" height = "30" ><bean:message bundle="PVS" key="pvs.chronologicaljob.jobInformationForThePartner"/></td>
 			</tr>	
</table> 			
		
<table >
	<tr>
		<td width=2 > &nbsp;</td>
		<td> 

			<table border = "0" cellpadding=0 cellspacing=0>
				<tr>
					<td valign = "top">
					 <a href="javascript:toggleMenuSection('1');"><img id="img_1" src="images/Expand.gif" border="0" alt="+"  /></a>
				</td>
				<td valign = "top" height = "100%" >
						<table  border = "0" height = "100%" class="dbsummtable" width = "700" cellpadding=0 cellspacing=0><!-- Table 3 Start -->
						    
							<tr>
								<td  colspan="8" valign = "top" height = "20"  align="center" class = "trybtopclose" id="classchange1">	
									<bean:message bundle="PVS" key="pvs.chronologicaljob.partnerInformation"/>
								</td>
							</tr>
							<tr id = "div_1" style="display: none"> <!-- newtr -->
								<td valign = "top" height = "100%" width=380>
									<table  height = "100%" border = "0" cellspacing="1" cellpadding="1"><!-- Table2 Start -->
										<tr >
											<td  valign = "top">
												<table width = "500" cellspacing = "2" cellpadding = "2" border=0><!-- Table1 Start -->

													<tr>
	    		    										<td  valign = "top"  class = "dblabel"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.partnerName"/></td>
		    	    									    <td  valign = "top"  class = "dbvalue"><bean:write name="JobInformationForm" property="partnerName"/>&nbsp;&nbsp;</td>
    													    <td  valign = "top"  class = "dblabel"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.partnerCity"/></td>
		    											    <td  valign = "top"  class = "dbvalue"><bean:write name="JobInformationForm" property="partnerCity"/></td>
													</tr>
													
													<tr>
														    <td  valign = "top"  class = "dblabel"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.partnerZipCode"/></td>
														    <td colspan = 9 class = "dbvalue"><bean:write name="JobInformationForm" property="partnerZipCode"/></td>
								  					</tr>
								  					<tr>
													
														    <td class = "dblabel"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.partnerCountry"/></td>
														    <td class = "dbvalue"><bean:write name="JobInformationForm" property="partnerCountry"/></td>
														    <td class = "dblabel"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.partnerState"/></td>
														    <td class = "dbvalue"><bean:write name="JobInformationForm" property="partnerState"/></td>
													</tr>	    
												    <tr>		    
														    <td class = "dblabel"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.partnerStatus"/></td>
														    <td class = "dbvalue"><bean:write name="JobInformationForm" property="partnerStatus"/></td>
														    <td class = "dblabel"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.partnerPhoneNumber"/></td>
														    <td class = "dbvalue"><bean:write name="JobInformationForm" property="partnerPhoneNo"/></td>
													    
								  					</tr>
								  					
								  					
  					
												</table><!-- Table1 End -->
											</td>
										</tr>
									</table><!-- Table2 End -->
								</td>
							</tr><!-- newtr -->
    		            </table><!-- Table 3 End -->
    			</td>
    </tr><!-- new Tr1 -->
    <tr></tr>
</table><!-- Table 4 End -->  


<table width=1152>	<!-- Table 7 start -->
	


	<tr>
	
		<td width=1150> 
	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1"> <!-- Table 5 Start -->
  		<tr> 
	    		<td rowspan = "2">&nbsp;</td>
	    		
				<td class = "tryb" rowspan = "2"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.customerName"/></td>    		
	    		<td class = "tryb" rowspan = "2"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.appendixName"/></td>
	    		<td class = "tryb" rowspan = "2"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.jobName"/></td>
	    		<td class = "tryb" rowspan = "2"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.purchaseOrder"/></td>
	    		<td class = "tryb" colspan = "3"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.schedule"/></td>
	       		<td class = "tryb" rowspan = "2"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.owner"/></td>
				<td class = "tryb" colspan = "2"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.siteInformation"/></td>
	    		<td class = "tryb" colspan = "5"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.costpricesummary"/></td>
				<td class = "tryb" rowspan = "2"><bean:message bundle = "PVS" key = "pvs.chronologicaljob.jobstatus"/></td>

		</tr>
		<tr> 
				<td  class = "tryb"><div><bean:message bundle="PVS" key="pvs.chronologicaljob.schedulestart"/></div></td>
				<td  class = "tryb"><div><bean:message bundle="PVS" key="pvs.chronologicaljob.actualstart"/></div></td>
				<td  class = "tryb"><div><bean:message bundle="PVS" key="pvs.chronologicaljob.actualend"/></div></td>

				<td  class = "tryb"><div><bean:message bundle="PVS" key="pvs.chronologicaljob.sitenumber"/></div></td>
				<td  class = "tryb"><div><bean:message bundle="PVS" key="pvs.chronologicaljob.sitename"/></div></td>
				
				<td  class = "tryb"><div><bean:message bundle="PVS" key="pvs.chronologicaljob.estimatedcost"/></div></td>
				<td  class = "tryb"><div><bean:message bundle="PVS" key="pvs.chronologicaljob.extendedprice"/></div></td>
				<td  class = "tryb"><div><bean:message bundle="PVS" key="pvs.chronologicaljob.proformavgp"/></div></td>
				<td  class = "tryb"><div><bean:message bundle="PVS" key="pvs.chronologicaljob.actualcost"/></div></td>
				<td  class = "tryb"><div><bean:message bundle="PVS" key="pvs.chronologicaljob.actualvjp"/></div></td>
				
        </tr>
  

	<logic:present name = "siteJobList"> 	
 	<logic:iterate id = "slist" name = "siteJobList">
	
		<%	
			if ( csschooser == true ) 
			{
				backgroundclass = "texto";

				labelClass= "textoleftalignnowrap";


				comboclass = "combooddhiddenrightalign";
				csschooser = false;
				readonlyclass = "readonlytextodd";
				readonlynumberclass = "readonlytextnumberodd";
			}
	
			else
			{
				csschooser = true;	

				comboclass = "comboevenhiddenrightalign";

				labelClass ="texteleftalignnowrap";
				
				backgroundclass = "texte";
				readonlyclass = "readonlytexteven";
				readonlynumberclass = "readonlytextnumbereven";
			}


		displaycombo = "javascript: displaycombo(this,'"+i+"')";
		valstr = "javascript: document.forms[0].siteid['"+i+"'].checked = true;updatetext( this ,'"+i+"' );"; 
		%>
		<bean:define id = "powo" name = "slist" property = "poNumber" />
		
		
		<tr>	
					<td >&nbsp; </td>
					
					<td class = "<%= labelClass %>"><bean:write name = "slist" property = "customername"/></td>
				    <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "appendixName"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "jobName"/></td>
	                <td class = "<%= backgroundclass %>"><%=powo %></td>
	           
    				<td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "schedulestartdate"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "actualstartdate"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "actualend"/></td>
	            
	             	<td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "owner"/></td>
					<td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "sitenumber"/></td>
					<td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "sitename"/></td>
	               
				    <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "estimatedcost"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "extendedprice"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "proformavgp"/></td>
	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "actualcost"/></td>
   	                <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "actualvgp"/></td>
   	                
                   <td class = "<%= backgroundclass %>"><bean:write name = "slist" property = "jobstatus"/></td>
	                
               
    	</tr>
	  <% i++;  %>
	  </logic:iterate>
  	  </logic:present>
 
		
		<tr>
					<td>&nbsp; </td>
					
				  	<td colspan=21 class = "buttonrow">
				  	<html:button property = "sort" styleClass = "button" onclick = "javascript:return sortwindow();"><bean:message bundle = "AM" key = "am.tabular.sort"/></html:button>
			<logic:present name="JobInformationForm" property="from" scope = "request">				  	
				  	<logic:equal name="JobInformationForm" property="from" value="Partner_Edit">
				  		<html:button property="back" styleClass="button" onclick = "return BacktoPartnerEdit();"><bean:message bundle="PVS" key="pvs.chronologicaljob.back"/></html:button></td> 
				  	</logic:equal>
				  	<logic:equal name="JobInformationForm" property="from" value="Partner_Search">
				  		<html:button property="back" styleClass="button" onclick = "javascript:return BacktoPartnerSearch();"><bean:message bundle="PVS" key="pvs.chronologicaljob.back"/></html:button></td> 
				  	</logic:equal>
		     </logic:present>
				   
    	</tr>
		</table><!-- Table 5 End -->
	</td>
	</tr>
	</table>	<!-- Table 7 End -->
 	</td>
 	</tr>
 </table> <!-- Table 6 end -->
  
</html:form>

<script>
function toggleMenuSection(unique) {
	
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	
	}
	
	action = "toggleType = toggleDiv('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	
	if (document.getElementById('div_' + unique).offsetHeight > 0) {
		thisImage.src = "images/Collapse.gif";
		
	}
	else {
		thisImage.src = "images/Expand.gif";
		
	}
	
	if(divid=="div_1")
	{
		if(document.getElementById ( "classchange1" ).className == 'trybtop')
		{
			document.getElementById ( "classchange1" ).className = 'trybtopclose';
			
		}
		else
		{
			document.getElementById ( "classchange1" ).className = 'trybtop';
			
		}
	}
}

function toggleDiv(divName) {
	
	 var tableRow = document.getElementsByTagName('tr');
	 
	 
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
	
		if (tableRow[k].getAttributeNode('id').value==divName) {
		 
			if (tableRow[k]) {
				if (tableRow[k].style.display == "none") {
					tableRow[k].style.display = "block";
				}
				else {
					tableRow[k].style.display = "none";
				}
			}
			else {
				errorString = "Error: Could not locate div with id: " + divName;
				alert(errorString);
			}
		}
	
	}
}

function toggleMenuSectionView(unique) {
	
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	
	}
	action = "toggleType = toggleDivView('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	
	if (document.getElementById('div_' + unique).offsetHeight > 0) {
		thisImage.src = "images/Collapse.gif";
		
	}
	else {
		thisImage.src = "images/Expand.gif";
		
	}
	
	
}

function toggleDivView(divName) {
	
	 var tableRow = document.getElementsByTagName('tr');
	if (tableRow.length==0) { return; }
	for (var k = 0; k < tableRow.length; k++) {
	
		if (tableRow[k].getAttributeNode('id').value==divName) {
		
			if (tableRow[k]) {
				if (tableRow[k].style.display == "none") {
					tableRow[k].style.display = "block";
				}
				
			}
			else {
				errorString = "Error: Could not locate div with id: " + divName;
				alert(errorString);
			}
		}
	
	}
}

function BacktoPartnerSearch()
{

	
	history.go('<%= counter %>');
  	/*document.forms[0].action = "Partner_Search.do?ref=search&type=v";
	document.forms[0].submit();
	return true;*/
}

function BacktoPartnerEdit()
{
	
	/*history.go(-1);*/
	document.forms[0].action = "Partner_Edit.do?pid=<bean:write name = "JobInformationForm" property = "partnerid" />&function=Update&formPVSSearch=fromPVSSearch";
	document.forms[0].submit();
	return true;
	
}

function sortwindow()
{
	
	
	str = "SortAction.do?Type=jobInformationPerPartner&partnerid=<bean:write name = "JobInformationForm" property = "partnerid" />&from=<bean:write name = "JobInformationForm" property = "from" />";

	p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' ); 
	return true;
}
function del() 
{
		var convel = confirm( "Are you sure you want to delete the Partner?" );
		if( convel )
		{	
			document.forms[0].action = "Partner_Edit.do?pid=<bean:write name = "JobInformationForm" property = "partnerid" />&func=delete";
			document.forms[0].submit();
			return true;	
		}
} 
function jobHistory() 
{		
	document.forms[0].action = "JobInformationperPartner.do?pid=<bean:write name = "JobInformationForm" property = "partnerid" />&from=Partner_Edit";
	document.forms[0].submit();
	return true;	
} 

</script>
</body>
</html:html>