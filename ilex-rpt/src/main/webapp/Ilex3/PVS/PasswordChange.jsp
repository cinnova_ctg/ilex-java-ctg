<!-- 
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 -->

<! DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<script language="javascript" src="javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<title></title>

<%@ include  file="/Menu.inc" %>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif');" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td><img src="images/spacer.gif" width="1" height="1"></td>
  </tr>
</table>
<html:errors/>
<table border="0" cellspacing="1" cellpadding="1" width=100% >

<logic:present name="invalidOldPassword" scope="request">
<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="PVS" key="pvs.passwordchange.oldpassword.invalid"/>
</td></tr></logic:present>

<logic:present name="changesuccessful" scope="request">
<tr><td  colspan="2" class="message" height="30" ><bean:message bundle="PVS" key="pvs.passwordchange.successful"/>
</td></tr></logic:present>

</table>
<!--  >table width="100%" border="0" cellspacing="0" cellpadding="0"-->
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
  <tr>  <td  width="6" height="0"></td>
  <td colspan="9"class="labeleboldwhite" height="30" ><bean:message bundle="PVS" key="pvs.changepassword"/></td>
  </tr> 
<!--  >/table-->

<html:form action="/passwordchange" >
<html:hidden property="pid" />

<tr>
  <td  width="1" height="0"></td>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<tr>
  <td  width="1" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" width="600"> 
<!-- >tr> 
    <td colspan="9" class="labellobold" height="30"><bean:message bundle="PVS" key="pvs.changepassword"/></td>
  </tr -->

<tr>
	<td   class="labelobold"  colspan="4"><bean:message bundle="PVS" key="pvs.oldpassword"/></td>
	<td   class="labelo"  colspan="5"><html:password  styleClass="textbox" size="20" property="oldPassword" value=""/></td>
</tr>  
<tr>
	<td   class="labelebold"  colspan="4"><bean:message bundle="PVS" key="pvs.newpassword"/></td>
	<td   class="labele"  colspan="5"><html:password  styleClass="textbox" size="20" property="newPassword" value=""/></td>
</tr>
<tr>
	<td  class="labelobold"  colspan="4"><bean:message bundle="PVS" key="pvs.confirmpassword"/></td>
	<td   class="labelo"  colspan="5"><html:password  styleClass="textbox" size="20" property="confirmPassword" value=""/></td>
</tr>
<tr> 
    <td colspan="9" class="buttonrow"> 
      <html:submit property="save" styleClass="button" onclick="return validatePassword();">Save</html:submit>
      <html:reset property="reset" styleClass="button">Reset</html:reset>
    </td>
</tr>
<jsp:include page = '/Footer.jsp'>
      <jsp:param name = 'colspan' value = '28'/>
      <jsp:param name = 'helpid' value = 'pvsPartner_Edit'/>
</jsp:include>
</table>
 </td>
 </tr> 
</table> 
</html:form>
</body>
</html:html>
<script>
function validatePassword(){
	if(! chkBlank(document.forms[0].oldPassword,"<bean:message bundle="PVS" key="pvs.oldpassword"/>"))return false;
	if(! CheckQuotes(document.forms[0].oldPassword,"<bean:message bundle="PVS" key="pvs.oldpassword"/>"))return false;
	
	if(! chkBlank(document.forms[0].newPassword,"<bean:message bundle="PVS" key="pvs.newpassword"/>"))return false;
	if(! CheckQuotes(document.forms[0].newPassword,"<bean:message bundle="PVS" key="pvs.newpassword"/>"))return false;
	
	if((document.forms[0].newPassword.value==document.forms[0].oldPassword.value)){
		alert("<bean:message bundle="PVS" key="pvs.passwordchange.newpassword.equal"/>");
		document.forms[0].newPassword.focus();
		return false;
	}// END OF IF	
	

	if(! chkBlank(document.forms[0].confirmPassword,"<bean:message bundle="PVS" key="pvs.confirmpassword"/>"))return false;
	if(! CheckQuotes(document.forms[0].confirmPassword,"<bean:message bundle="PVS" key="pvs.confirmpassword"/>"))return false;
		
	if(!(document.forms[0].newPassword.value==document.forms[0].confirmPassword.value)){
		alert("<bean:message bundle="PVS" key="pvs.passwordchange.confirmpassword.notequal"/>");
		document.forms[0].confirmPassword.focus();
		return false;
	}// END OF IF	
}
function chkBlank(obj,label){
	if(isBlank(obj.value))	{	
		alert("Please Enter "+label);	
		obj.focus();
		return false;
	}
	return true;
}

function CheckQuotes (obj,label){
	if(invalidChar(obj.value,label))
	{		
		obj.focus();
		return false;
	}
	return true;
}
</script>
