<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>



<html>
<head>

<title><bean:message bundle="PVS" key="pvs.partner.detail" /></title>

 
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<TABLE WIDTH=171 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR>
      <TD><IMG SRC="images/box_01.gif" WIDTH=7 HEIGHT=15 ALT=""></TD>
      <TD background="images/box_02.gif"><IMG SRC="images/box_02.gif" WIDTH=157 HEIGHT=15 ALT=""></TD>
      <TD><a onClick="document.all.ajaxdiv.innerHTML='';" href="#"><IMG SRC="images/box_03.gif" ALT="" WIDTH=16 HEIGHT=15 border="0"></a></TD>
    </TR>
    <TR>
      <TD background="images/box_04.gif"><IMG SRC="images/box_04.gif" WIDTH=7 HEIGHT=109 ALT=""></TD>
      <TD valign="top" bgcolor="#E6E6E6"><table width="100%" border="0" cellspacing="0" cellpadding="1">
			
			<tr> 
				<td class="inside" colspan="3"><bean:write
					name="PartnerDetailForm" property="partnerName" /></td>
			</tr>
			<tr>
				<td class="inside"><bean:write name="PartnerDetailForm"
					property="partnerAddress1" />
					<logic:notEqual name="PartnerDetailForm" property="partnerAddress2" value="">
					,&nbsp;<bean:write name="PartnerDetailForm"
					property="partnerAddress2" />
				</td>
					</logic:notEqual>
				<td></td>
			</tr>
			<tr>
				<td class="inside" colspan="3"><bean:write
					name="PartnerDetailForm" property="partnerCity" />,&nbsp; <bean:write
					name="PartnerDetailForm" property="partnerState" />&nbsp; <bean:write
					name="PartnerDetailForm" property="partnerZipCode" />&nbsp;</td>
			</tr>
			<tr><td class = "inside" colspan = "3"><bean:write name = "PartnerDetailForm" property = "partnerPhone"/></td></tr>
			
			<tr>
				<td class="inside" colspan="3">&nbsp;</td>
			</tr>
			<tr>
				<td class="inside" colspan="3">Primary Contact</td>
			</tr>
			<tr><td style ="padding-left:12px">
			<table border ="0" cellpadding="0" cellspacing="0">
				<tr><td class="inside" colspan="3"><bean:write name="PartnerDetailForm" property="partnerPriName" /></td></tr>
				<tr>
					<td class="inside" colspan="3"><bean:write
						name="PartnerDetailForm" property="partnerPriPhone" /></td>
				</tr>
				<tr>
					<td class="inside" colspan="3"><bean:write
						name="PartnerDetailForm" property="partnerPriCellPhone" /></td>
				</tr> 
				<tr>
					<td class="inside"  colspan="3"><bean:write
						name="PartnerDetailForm" property="partnerPriEmail" /></td>
				</tr>
			</table>
			</td></tr>
		</table>
		</TD> 
		<TD background="images/box_06.gif"><IMG SRC="images/box_06.gif" WIDTH=16 HEIGHT=109 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/box_07.gif" WIDTH=7 HEIGHT=10 ALT=""></TD>
      <TD background="images/box_08.gif"><IMG SRC="images/box_08.gif" WIDTH=157 HEIGHT=10 ALT=""></TD>
      <TD><IMG SRC="images/box_09.gif" WIDTH=16 HEIGHT=10 ALT=""></TD> </TR>
</TABLE>

</body>
</html>
