<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <LINK href="styles/style.css" rel="stylesheet"	type="text/css">
    <title><bean:message bundle="PVS" key="pvs.partner.location.gmap"/></title>
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAXva3r0UhFOUN7QMaRRd3XBSwRy4s2eep2DvUSxAFcnO47vT-IhQ8aJawzOUYasAmUSRGFQcI85bmMA"
      type="text/javascript"></script>
    <script type="text/javascript">
    //<![CDATA[
    var gicons = [];
    
    gicons["Cred"] = new GIcon(G_DEFAULT_ICON, "images/RED_P.gif");
    gicons["Mred"] = new GIcon(G_DEFAULT_ICON, "images/RED_M.gif");
    gicons["Sred"] = new GIcon(G_DEFAULT_ICON, "images/RED_S.gif");
    
    gicons["Cgreen"] = new GIcon(G_DEFAULT_ICON, "images/GREEN_P.gif");
    gicons["Mgreen"] = new GIcon(G_DEFAULT_ICON, "images/GREEN_M.gif");
    gicons["Sgreen"] = new GIcon(G_DEFAULT_ICON, "images/GREEN_S.gif");
    
    gicons["Cyellow"] = new GIcon(G_DEFAULT_ICON, "images/yellow_P.gif");
    gicons["Myellow"] = new GIcon(G_DEFAULT_ICON, "images/yellow_M.gif");
    gicons["Syellow"] = new GIcon(G_DEFAULT_ICON, "images/yellow_S.gif");
    
    gicons["Corange"] = new GIcon(G_DEFAULT_ICON, "images/ORANGE_P.gif");
    gicons["Morange"] = new GIcon(G_DEFAULT_ICON, "images/ORANGE_M.gif");
    gicons["Sorange"] = new GIcon(G_DEFAULT_ICON, "images/ORANGE_S.gif");
    
    gicons["Black"] = new GIcon(G_DEFAULT_ICON, "images/black_circle.gif");
    
    function load() {
      if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map"));
        	map.addControl(new GLargeMapControl());
	    	map.addControl(new GMapTypeControl());
        //map.setCenter(new GLatLng(33.802017, -84.463458), 13);
        //map.setZoom(3);
        var xmlDoc = GXml.parse(<%=request.getAttribute("partnerXML").toString()%>);  
        var partners = xmlDoc.documentElement.getElementsByTagName("partner");
        var xmlDoc1 = GXml.parse(<%=request.getAttribute("siteXML").toString()%>);
        var site = xmlDoc1.documentElement.getElementsByTagName("site");
        
        //alert(zoom);
        //map.setCenter(new GLatLng(36.81362, -96.554209), 13);
        //map.setZoom(9);
        var xmlDoc2 = GXml.parse(<%=request.getAttribute("checkXML").toString()%>);
        var check = xmlDoc2.documentElement.getElementsByTagName("check");
        var zoom=0;
        //alert(site.length);
        //alert(check[0].getAttribute("Radius"))
        
        if(check[0].getAttribute("Radius") != '' ) {
        	if(check[0].getAttribute("Radius") == 25)
        	 zoom=8;
        	else if(check[0].getAttribute("Radius") == 10 )
        	 zoom=9;
        	else if(check[0].getAttribute("Radius") == 50) {
        	 zoom=7;
        	 }
        	else if(check[0].getAttribute("Radius") == 100)
        	 zoom=6;
        	else if(check[0].getAttribute("Radius") == 200)
        	 zoom=5;
        	else if(check[0].getAttribute("Radius") == 500)
        	 zoom=4;
        }
        if(site.length > 0 && zoom != 0) {
        	//alert('site');
        	map.setCenter(new GLatLng(site[0].getAttribute("lat"),site[0].getAttribute("lon") ), 13);
        	 map.setZoom(zoom);
        }
        else if(check[0].getAttribute("checkLat")!= 'null' && check[0].getAttribute("checkLong") != 'null' && zoom != 0 ) {
        	//alert('MAPPArtner');
        	map.setCenter(new GLatLng(partners[0].getAttribute("lat"),partners[0].getAttribute("lon")), 13);
        	map.setZoom(zoom);
        } else {
        	//alert('Other');
        	map.setCenter(new GLatLng(36.81362, -96.554209), 13);
        	map.setZoom(3);
        }
         
        
        
        for (var i = 0; i < site.length; i++) {
	            var lat = parseFloat(site[i].getAttribute("lat"));
	            var lon = parseFloat(site[i].getAttribute("lon"));
				var siteNumber = site[i].getAttribute("siteNumber");
				var siteAddress = site[i].getAttribute("siteAddress");
				var siteCity = site[i].getAttribute("siteCity");
				var siteState = site[i].getAttribute("siteState");
				var siteZipCode = site[i].getAttribute("siteZipCode");
				var icontype = site[i].getAttribute("icontype");
				
	            var point = new GLatLng(lat,lon);
	            //map.setCenter(new GLatLng(lat,lon), 13);
	            var displayString =  siteNumber + "<br>" + siteAddress +  "<br>" 
	            					+ siteCity + " " + siteState + " " + siteZipCode;  
	            var marker = createMarker(point,displayString,"Black");
	            map.addOverlay(marker);
          }
		for (var i = 0; i < partners.length; i++) {
				var lat = parseFloat(partners[i].getAttribute("lat"));
	            var lon = parseFloat(partners[i].getAttribute("lon"));
				var partnerName = partners[i].getAttribute("partnerName");
				var partnerType = partners[i].getAttribute("partnerType");
				var partnerAddress1 = partners[i].getAttribute("partnerAddress1");
				var partnerAddress2 = partners[i].getAttribute("partnerAddress2");
				var partnerCity = partners[i].getAttribute("partnerCity");
				var partnerState = partners[i].getAttribute("partnerState");
				var partnerZipCode = partners[i].getAttribute("partnerZipCode");
				var partnerPhone = partners[i].getAttribute("partnerPhone");
				var partnerPriName = partners[i].getAttribute("partnerPriName");
				var partnerPriPhone = partners[i].getAttribute("partnerPriPhone");
				var partnerPriCell = partners[i].getAttribute("partnerPriCell");
				var partnerPriEmail = partners[i].getAttribute("partnerPriEmail");
				var partnerDistance = partners[i].getAttribute("partnerDistance");
				var icontype = partners[i].getAttribute("icontype");
					
	            var point = new GLatLng(lat,lon);
	            var displayString = partnerName + "<br>" 
	            					+"Partner Type:" + " " + partnerType + "<br>"
	            					+ partnerAddress1 + "" + partnerAddress2 + "<br>" 
	            					+ partnerCity + ", " + partnerState + " " + partnerZipCode + "<br>" 
	            					+ partnerPhone + "<br>" 
	            					+ "<br>" +"Primary Contact" + "<br>"
	            					+ "&nbsp;&nbsp;&nbsp;"+ partnerPriName + "<br>"
	            					+ "&nbsp;&nbsp;&nbsp;"+ partnerPriPhone+ "<br>"
            						+ "&nbsp;&nbsp;&nbsp;"+ partnerPriCell + "<br>" 
            						+ "&nbsp;&nbsp;&nbsp;"+ partnerPriEmail + "<br>" 
            						+ "<br>" + "Distance: "  + partnerDistance;
	            var marker = createMarker(point,displayString,icontype);
	            map.addOverlay(marker);
          }
          
        }
    }
     function createMarker(point,displayString,icontype) {
		        var marker = new GMarker(point, gicons[icontype]);
		        GEvent.addListener(marker, "click", function() {
          		marker.openInfoWindowHtml(displayString);
        		});
		        return marker;
		      }
    //]]>
    </script>
  </head>
  <body onload="load()" onunload="GUnload()">
	<table width = "800px" cellpadding="0" cellspacing="0">
	<tr><td>
		<tr><td style="padding-top: 4px; width: 800px; height: 4px"></td></tr>
		<tr>
		<td align = "right">
		<input type="button" style="width:100;font-family : verdana; padding-left:2;font-size : 10px; font-style : normal; font-weight : normal; color : #000000; letter-spacing : normal; word-spacing : normal; border : 1px #333333 solid; background : #D0D3D5;" name="search123" value="Search Results" onclick="javascript:history.go(-1);" ></td>
		</tr>
		<tr><td style="padding-top: 4px; padding-top: 4px; width: 800px; height: 4px"></td></tr>
	</table>
	<table>
	<tr>
	<td  style= "padding-left: 6px;">		 	   	
  	 <div id="map" style="width: 800px; height: 700px"></div> 
  	 </td>
  	 </tr>
  	 </table>
  </body>
</html>