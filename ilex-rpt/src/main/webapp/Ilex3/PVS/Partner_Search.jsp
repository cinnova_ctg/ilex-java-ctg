<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For performing search operations on partners.
*
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id="dcpvs" name="dcpvs" scope="request" />
<bean:define id="dcPartnerType" name="dcPartnerType" scope="request" />
<bean:define id="dcRadiusRange" name="dcRadiusRange" scope="request" />
<bean:define id="dcCoreCompetancy" name="dcCoreCompetancy" scope="request" />
<bean:define id="dcTechCertifications" name="dcTechCertifications" scope="request" />
<html:html>
<HEAD>


<%@ include  file="/Header.inc" %>


<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/summary.css" rel="stylesheet"	type="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<title></title>
<script language="javascript" src="javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script language="JavaScript" src="javascript/prototype.js"></script>
<%@ include  file="/NMenu.inc" %>

</head>
<%
//response.setHeader("Cache-Control", "no-cache,must-revalidate,no-store");
String heading="";
String fromtype="";

String oldPartnerType = "";

if(request.getParameter("type").equalsIgnoreCase("v")) { 
	heading="pvs.certifiedpartnersummaryview";
} 
else {
	heading="pvs.assignpartner";
} 


if(request.getAttribute("fromtype") != null) {
	fromtype = ""+request.getAttribute("fromtype");
}

String backgroundclass = null;
String backgroundhyperlinkclass = null;
String backgroundclasstoleftalign=null;
boolean csschooser = true;

%>

<body id="pbody" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >

<%if(request.getParameter("type").equalsIgnoreCase("v"))
{%>  
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr >
    <td   valign="top" width = "100%"> 
      <table border="0" cellspacing="0" cellpadding="0" height="18"  width = "100%">
        <tr > 
			 <td id="pop1" width="120" class="Ntoprow1" align="center"><a href="#"  onmouseout="MM_swapImgRestore();popDown('elMenu1',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.managepartner"/></center></a></td>	
	         <td   width="120" class="Ntoprow1" align="center"><a href="Partner_Search.do?ref=search&type=v"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.partnersearch"/></center></a></td>			 
			 <td   width="120" class="Ntoprow1" align="center"><a href="MCSA_View.do"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.managemcsa"/></center></a></td>
			 <td   class="Ntoprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>          
        </tr>
      </table>
    </td>
  </tr>
</table>
<SCRIPT language=JavaScript1.2>



if (isMenu) {
//start
arMenu1 = new Array(
120,
findPosX( 'pop1' ),findPosY( 'pop1' ),
"","",
"","",
"","",
"Minuteman","#",1,
"Standard Partner","#",1,
"New Partners","PVSSearch.do?orgTopName=all&action=newadded&enablesort=true",0,
"Recent Updates","PVSSearch.do?orgTopName=all&action=resentupdated&enablesort=true",0,
"Annual Review Due","PVSSearch.do?orgTopName=all&action=annualreport&enablesort=true",0
)
arMenu1_1=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Minuteman Partners",0,
"Update","PVSSearch.do?orgTopName=Minuteman Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Minuteman Partners&action=delete",0
<%}%>
)
arMenu1_2=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Certified Partners",0,
"Update","PVSSearch.do?orgTopName=Certified Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Certified Partners&action=delete",0
<%}%>
)
}
document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
//end
</SCRIPT>
<%} %>

<%if(request.getParameter("type").equals("J") ||request.getParameter("type").equals("AJ")||request.getParameter("type").equals("CJ")||request.getParameter("type").equals("IJ")||request.getParameter("type").equals("A")|| request.getParameter("type").equals("AA")|| request.getParameter("type").equals("CA")|| request.getParameter("type").equals("IA"))
{%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td  class="toprow"> 
      <table width="800" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="left"> 
        <td  width="200" class="toprow1">
        <%if(request.getParameter("type").equals("J") ||request.getParameter("type").equals("AJ")||request.getParameter("type").equals("CJ")||request.getParameter("type").equals("IJ")) 
        {%>
         <a href="JobHeader.do?function=view&jobid=<%=request.getParameter("typeid") %>"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="PVS" key="pvs.backtojobdashboard"/>
        <%} %>
         <%if(request.getParameter("type").equals("A") ||request.getParameter("type").equals("AA")||request.getParameter("type").equals("CA")||request.getParameter("type").equals("IA")) 
        {%>
         <a href="ActivityHeader.do?function=view&activityid=<%=request.getParameter("typeid") %>"   onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="PVS" key="pvs.backtoactivitydashboard"/>
        <%} %>
        
       
        </a></td>
        <td   width=600 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          </tr>
		</tr>
      </table>
    </td>
  </tr>
</table>
<%} %>



<table>
<html:form action="Partner_Search">
<html:hidden property="ref"/>
<html:hidden property = "fromprm" />
<html:hidden property="type"/>
<html:hidden property="typeid"/>
<html:hidden property="assigncheck"/>
<html:hidden property="formtype"/>
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />
<html:hidden property = "powoId" />
<html:hidden property = "search" />
<html:hidden property = "selectedpartner" />
<html:hidden property = "currentPartner" />
<html:hidden property = "hiddensave" />


<logic:present name = "partnerDeleted" scope = "request">
	<script>
		parent.ilexleft.location.reload();
	</script>
</logic:present>

<table  border="0" cellspacing="0" cellpadding="0" width = "960">
<tr>
  <td  width="2" height="0"></td>
  <td><table border="0" cellspacing="0" cellpadding="0"> 
  <tr>
    <td colspan="7"class="labeleboldwhite" height="30" ><bean:message bundle="PVS" key="<%=heading %>"/>
    <% if(fromtype.equals("powo"))
    {%>
    &nbsp;<bean:message bundle="PVS" key="pvs.forJob"/>:&nbsp;<bean:write name = "Partner_SearchForm" property = "jobName"/></td>
    <%} %>
  </tr> 
  
  <tr>
  	  <td>		
  	  			<table class="labeleboldrightborder">
					  	<tr>
						  	<td class="labelebold" height="24"><bean:message bundle="PVS" key="pvs.name"/>
						  	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		
		  	  				<html:text  styleClass="textbox" size="20" name="Partner_SearchForm" property="name" maxlength="50"/>&nbsp;&nbsp;</td>
					  	</tr>
					  	<tr>	  		
					  		<td class="labelebold" height="24"><bean:message bundle="PVS" key="pvs.keyword"/>
					  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
					  		<html:text styleClass="textbox" size="20" name="Partner_SearchForm" property="searchTerm"/></td>
					  	</tr>
					  	<tr>
							<td class="labelebold" height="24"><bean:message bundle="PVS" key="pvs.partnertype"/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<html:select  name = "Partner_SearchForm" property = "partnerTypeName" size="1" styleClass="comboe">
											<html:optionsCollection name = "dcPartnerType"  property = "firstlevelcatglist"  label = "label"  value = "value" />
										</html:select>
							</td>
						</tr>
						<tr>
							<td class="labelebold" height="24"><bean:message bundle="PVS" key="pvs.mcsaVersion1"/>
							<html:select property="mcsaVersion" size="1" styleClass="comboe">
								<html:optionsCollection name = "mcsaVersionCombo"  property = "mcsaVersion"  label = "label"  value = "value" /></html:select>
							</td>
						</tr>
				   </table>
			  </td>
			  <td>
				  		<table class="labeloboldrightbottomborder">
					  		<tr>
					  			<td class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.siteZipCode"/>:
					  			&nbsp;&nbsp;<html:text styleClass="UpperCasetextbox" size="8" name="Partner_SearchForm" property="zipcode"/>
					  			</td>
					  			<td class="labelo" height="20"><bean:message bundle="PVS" key="pvs.radius"/>:
					  				&nbsp;&nbsp;<html:select property="radius" size="1" styleClass="comboo">
								<html:optionsCollection name = "dcRadiusRange"  property = "firstlevelcatglist"  label = "label"  value = "value" /></html:select>
								</td>	
					  	 	</tr>
					  		 <tr>
					  			<td class="labelo" height="20" colspan="2">
					  			<html:checkbox property="mainOffice" value = "Y"></html:checkbox>&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.mainOffice"/>
					  			&nbsp;&nbsp;<html:checkbox property="fieldOffice" value = "Y"></html:checkbox>&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.fieldOffices"/>
					  			<html:checkbox property="techniciansZip" value = "Y"></html:checkbox>&nbsp;&nbsp;<bean:message bundle="PVS" key="pvs.technicians"/>
					  			</td>
				  	 		</tr>	
					  	 	<tr>	  			
					  			<td class="labeloline" colspan="2" ></td>
					  	 	</tr>
					  	</table> 
					  	<table class="labeloboldnorightborder" width = "100%">
						  	<tr>
						  		<td><bean:message bundle="PVS" key="pvs.search.quality.rating"/></td>
						  		<td><html:radio property = "partnerRating" value = "N"><bean:message bundle="PVS" key="pvs.search.quality.none"/></html:radio>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
						  			<html:radio property = "partnerRating" value = "4"><bean:message bundle="PVS" key="pvs.search.quality.four"/></html:radio>
						  		</td>
						  	</tr>
						  	<tr>
						  		<td>&nbsp;</td>
						  		<td><html:radio property = "partnerRating" value = "3"><bean:message bundle="PVS" key="pvs.search.quality.three"/></html:radio>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				             		<html:radio property = "partnerRating" value = "A"><bean:message bundle="PVS" key="pvs.search.quality.all"/></html:radio>
						  		</td>
						  	</tr>
					  	</table>
			  </td>
			  <td>
				  		<table class="labeleboldwithoutrightborder ">
				  			<tr><td></td></tr>
				  			<tr>
		  						<td class="labelebold" height="28"><bean:message bundle="PVS" key="pvs.partneredit.technicianinfo"/>
									&nbsp;&nbsp;<html:select  name = "Partner_SearchForm" property = "technicians" size="2" styleClass="comboe" multiple = "true">
													<html:optionsCollection name = "dcTechCertifications"  property = "firstlevelcatglist"  label = "label"  value = "value" />
												</html:select>
		  						</td>
		  					</tr>
							<tr>
		  						<td class="labelebold" height="28"><bean:message bundle="PVS" key="pvs.certifications"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  						<logic:present name="initialStateCategory">
									<html:select name ="Partner_SearchForm" property = "certifications"  styleClass="comboe">
										<logic:iterate id="initialStateCategory" name="initialStateCategory" >
											<bean:define id="catname" name="initialStateCategory" property="certificationType" />		   
											<%  if(!catname.equals("notShow")) { %>
											<optgroup class=select label="<%=catname%>">
											<% } %>
												<html:optionsCollection name = "initialStateCategory" property="tempList" value = "value" label = "label" />
											<%  if(!catname.equals("notShow")) { %>	
											</optgroup> 
											<% } %>
										</logic:iterate>
									</html:select>
								</logic:present>
								</td>
		  					</tr>			  		
		  					<tr>
		  						<td class="labelebold" height="28"><bean:message bundle="PVS" key="pvs.corecompetency"/>
									&nbsp;&nbsp;<html:select  name = "Partner_SearchForm" property = "coreCompetency" size="2" styleClass="comboe" multiple = "true">
													<html:optionsCollection name = "dcCoreCompetancy"  property = "firstlevelcatglist"  label = "label"  value = "value" />
												</html:select>
																
		  						</td>
		  					</tr>
		  				</table>
			  </td>
  </tr>
  
  <tr>
  	<td  class="buttonrow" height="20" colspan="1">
		<html:button property="search" styleClass="button" onclick="return validate();">
			<bean:message bundle="PVS" key="pvs.search" />
		</html:button>
		<html:reset property="reset" styleClass="button">
			<bean:message bundle="PVS" key="pvs.reset" />
		</html:reset>
		<%if(fromtype.equals("powo")) {%>
		<html:button property="back" styleClass="button" onclick = "return Backaction();">
			<bean:message bundle="PVS" key="pvs.back" />
		</html:button>
		<%} %>
	</td>
	<td class="labeleboldexeclude" colspan="2" align = "right"> 
		<bean:message bundle="PVS" key="pvs.exclude"/>:
		<html:checkbox name="Partner_SearchForm" property="cbox1" value = "Y"/>&nbsp;<a href="#" target = "ilexmain" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/2.gif',1)"><img name="Image1" src="images/2.gif" border="0" alt="<bean:message bundle="PVS" key="pvs.partnerswithincidents"/>"></a>
		<html:checkbox name="Partner_SearchForm" property="cbox2" value  = "Y"/>&nbsp;<a href="#"  target = "ilexmain" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/1.gif',1)"><img name="Image2" src="images/1.gif" border="0" alt="<bean:message bundle="PVS" key="pvs.partnerswhosemcsaterminated"/>"></a>
	</td>
	<TD class="labele" height="20" colspan="2"></TD>
  </tr> 
  </table>
 </td>
 </tr>
</table>
<table border="0" cellspacing="1" cellpadding="2">
<tr><td>
</td></tr>
</table>

 
  <!-- V stands for control from PVS Manager  -->
  	<table cellspacing="0" cellpadding="0" border="0"><tr><td style="padding-left: 6px">
		<table  border="0" cellspacing="1" cellpadding="0">
			<logic:present name="partnersearchlist" scope="request">
				<tr>
					<td class = "colLight" align ="right" colspan = "14"><html:button property="gMap" styleClass="button" onclick ="showGMap();"><bean:message bundle="PVS" key="pvs.search.mapview"/></html:button></td>
				</tr>
			</logic:present>
			<logic:notPresent name="partnersearchlist" scope="request">
				<tr><td>&nbsp;</td></tr>
			</logic:notPresent>
			<logic:present name="partnersearchlist" scope="request">
			<tr >
				     <td class = "Ntrybgray" rowspan="2">Partner</td>
				     <td class="Ntrybgray"  rowspan = "2"><bean:message bundle="PVS" key="pvs.city"/></td>
		    		 <td class="Ntrybgray"  rowspan = "2"><bean:message bundle="PVS" key="pvs.state"/></td>
					 <td class = "Ntrybgray" colspan = "7"><bean:message bundle="PVS" key="pvs.search.rating"/></td>
					 <td class = "Ntrybgray" rowspan = "2"><bean:message bundle="PVS" key="pvs.search.adpo"/></td>			
					 <td class = "Ntrybgray" rowspan = "2"><bean:message bundle="PVS" key="pvs.search.diversity.indicator"/></td>
 					 <td class = "Ntrybgray" rowspan = "2"><bean:message bundle="PVS" key="pvs.search.last.used"/></td>
					 <td class = "Ntrybgray" rowspan = "2"><bean:message bundle="PVS" key="pvs.search.distance"/></td>					
			</tr>
			<tr>
					<td class="Ntrybgray"><bean:message bundle="PVS" key="pvs.search.rating"/></td>
					<td class="Ntrybgray"><bean:message bundle="PVS" key="pvs.search.contracted"/></td>
					<td class="Ntrybgray"><bean:message bundle="PVS" key="pvs.search.completed"/></td>
					<td class="Ntrybgray" colspan="4"><bean:message bundle="PVS" key="pvs.search.poke"/></td>
			</tr>
			
			 <%int i=1;%>	
			<logic:iterate id="pslist" name="partnersearchlist">
				<bean:define id = "PartType" name = "pslist" property = "partnerType" type = "java.lang.String"/>
				<bean:define id="pID" name="pslist" property="pid" />
				<bean:define id="P" name="pslist" property="pid" />
				<bean:define id="color" name="pslist" property="icontype" type = "java.lang.String" />
						<%	
							if ( csschooser == true ){
								backgroundclass = "Ntexto";
								backgroundclasstoleftalign ="Ntextoleftalignnowrap";
								backgroundhyperlinkclass = "Nhyperodd";
								csschooser = false;
							}else{
								csschooser = true;	
								backgroundclass = "Ntexte";
								backgroundclasstoleftalign = "Ntexteleftalignnowrap";
								backgroundhyperlinkclass = "Nhypereven";
							}
						
						if(color.endsWith("red")){
							backgroundclass = "Ntextpred";
						}else if(color.endsWith("green")){
							backgroundclass = "Ntextpgreen";
						}else if(color.endsWith("yellow")){
							backgroundclass = "Ntextpyellow";
						}else if(color.endsWith("orange")){
							backgroundclass = "Ntextporange";
						}
						%>
					<% if(!oldPartnerType.equals(PartType))
						  {
							oldPartnerType = PartType; i = 1;
							if(PartType.equals("Certified Partners - Speedpay Users")) {%>
								<tr>
								 	<td class="labeleboldgray" colspan="14" align="left" height="18">Speedpay</td></tr>
							<%} else { %>
							<tr><td class="labeleboldgray" colspan="14" align="left" height="18"><bean:write name="pslist" property="partnerType"/></td></tr>
							<%}
						}%>
						<tr>
								<td class = "<%= backgroundclass %>" nowrap="nowrap" height="10">
									<table cellpadding="0" cellspacing="0" border="0" width="100%" ><tr>
											<td colspan="1"  nowrap style="font-size: 10px; text-align: left;">
											 <a href="Partner_Edit.do?pid=<bean:write name="pslist" property="pid"/>&function=add&fromtype=<%= fromtype%>&jobid=<%=request.getParameter("typeid") %>&showViewPage=y"><bean:write name="pslist" property="partnername"/></a>
											 <logic:equal name="pslist" property="incTypeFlag" value="Y" ><img src="images/2.gif" border="0" ></logic:equal>
											 <logic:equal name="pslist" property="barredflag" value="Y" ><img src="images/1.gif" border="0" ></logic:equal>
											</td>
											
											<%if(fromtype.equals("powo")) {%>
											<td   style="font-size: 10px; text-align: right;" nowrap>[<a href = "javascript:void(0)" onClick = "if (window.event || document.layers) showWindow(event,<%= ( String ) pID %>); else showWindow('',<%= ( String ) pID %>);"><bean:message bundle="PVS" key="pvs.search.details"/></a>][<a href = "#" onclick="return assignpartner(<%= ( String ) pID %>);"><bean:message bundle="PVS" key="pvs.search.assign"/></a>]</td>
											<%}else{%>
											 <td  style="font-size: 10px; text-align: right;" nowrap>[<a href="javascript:void(0)"  onClick = "if (window.event || document.layers) showWindow(event,<%= ( String ) pID %>); else showWindow('',<%= ( String ) pID %>);"><bean:message bundle="PVS" key="pvs.search.details"/></a>]</td>
											<%}%>
									</tr></table>
								</td>
								<td  class="<%=backgroundclass %>" ><bean:write name="pslist" property="city"/></td>
		 						<td  class="<%=backgroundclass %>" ><bean:write name="pslist" property="state"/></td>
								<td class = "<%= backgroundclass %>" align="right"><bean:write name="pslist" property="partnerAvgRating"/></td>
								<td class = "<%= backgroundclass %>" align="right"><bean:write name="pslist" property="contractedCount"/></td>	
								<td class = "<%= backgroundclass %>" align="right"><bean:write name="pslist" property="compeletedCount"/></td>
								<td class = "<%= backgroundclass %>"><bean:write name="pslist" property="quesProfessional"/></td>
								<td class = "<%= backgroundclass %>"><bean:write name="pslist" property="quesOnTime"/></td>	
								<td class = "<%= backgroundclass %>"><bean:write name="pslist" property="quesKnowledgeable"/></td>		
								<td class = "<%= backgroundclass %>"><bean:write name="pslist" property="quesEquipped"/></td>	
								<td class = "<%= backgroundclass %>" align="right"><bean:write name="pslist" property="partnerAdpo"/></td>
								<td class = "<%= backgroundclass %>" align="right"><bean:write name="pslist" property="diversityIndicator"/></td>
								<td class = "<%= backgroundclass %>" align ="center"><bean:write name="pslist" property="partnerLastUsed"/></td>
								<td class = "<%= backgroundclass %>" align="right"><bean:write name="pslist" property="distance"/></td>
								
						</tr>
						</logic:iterate>
						<tr>
						<td  class="buttonrow" height="20" colspan="14">
							<html:button property="search" styleClass="button" onclick="return sortwindow();">Sort</html:button>
						</td>
						<!-- <TD class="labele" height="20" colspan="2"></TD> -->
						</tr>
						<tr>
						<td>
							<div id="ajaxdiv"  style=" position:absolute;"></div>
						</td>
						</tr> 
						</logic:present>		
		</table>
	</td></tr></table>

		<table  border="0" cellspacing="1" cellpadding="1"  >
<tr>
  <td  width="2" height="0"></td>
  <td><table border="0"  cellspacing="1" cellpadding="1" > 


<%
if(request.getParameter("type").equalsIgnoreCase("v")  || fromtype.equals("powo"))
{
%>   
<logic:present name = "Partner_SearchForm" property="assignmessage">
	<logic:equal name="Partner_SearchForm" property="assignmessage" value="-9001">
	<table>
		<tr><td  width="100%" class="message" height="30" ><bean:message bundle="PVS" key="pvs.errorassigningpartner"/></td></tr>
	</table>	
	</logic:equal>
	<logic:equal name="Partner_SearchForm" property="assignmessage" value="-9002">
	<table>
		<tr><td  width="100%" class="message" height="30" ><bean:message bundle="PVS" key="pvs.alreadyassigned"/></td></tr>
	</table>	
	</logic:equal>
	<logic:equal name="Partner_SearchForm" property="assignmessage" value="0">
	<table>
		<tr><td  width="100%" class="message" height="30" ><bean:message bundle="PVS" key="pvs.successfullyassigned"/></td></tr>
	</table>	
	</logic:equal>
	<logic:equal name="Partner_SearchForm" property="assignmessage" value="-9100">
	<table>
		<tr><td  width="100%" class="message" height="30" ><bean:message bundle="PVS" key="pvs.selectforassignment"/></td></tr>
	</table>	
	</logic:equal>
</logic:present>	
 <%} %>  
 	<logic:present name = "Partner_SearchForm" property="norecordmessage">
	<logic:equal name="Partner_SearchForm" property="norecordmessage" value="9999">
	<table>
		<tr><td  width="100%" class="message" height="30" ><bean:message bundle="PVS" key="pvs.norecord"/></td></tr>
	</table>	
	</logic:equal>
	</logic:present>
   
 </tr>
  </table>
 </td>
 </tr>
</table> 
</html:form>



</table>
</BODY>
<script>
	 function getResponse(partnerURL){	 
		var url = partnerURL;
		var pars = '';		

		var myAjax = new Ajax.Updater(
					{success: 'ajaxdiv'}, 
					url, 
					{
						method: 'get', 
						parameters: pars, 						
						onSuccess: disableStatus,
						onFailure: disableStatus
					});		
	}
	function disableStatus(res){	
	document.all.ajaxdiv.innerText=res.response.Text;

	}	
</script>
<script>
function showWindow(e,pId) {	
	var div=document.all.ajaxdiv;
    var x=y=0;

    if (e != '') {
        x = e.clientX;
        y = e.clientY+document.all.pbody.scrollTop;
    }
	str = 'PartnerDetail.do?partnerId='+pId;
	//alert(x+":"+y);
	getResponse(str);

	div.style.left=x;
	div.style.top=y;
    //myWindow=window.open(str,'windowName','width=100,height=120,screenX=' + x + ',screenY=' + y + ',left=' + x + ',top=' + y);
    //myWindow.focus();
}
function showGMap() {
		 document.forms[0].action = "PartnerGMap.do";
		 document.forms[0].submit();
		 return true;
}
function chkInteger(obj,label){
	if(!isInteger(obj.value))	{	
		alert("<bean:message bundle="PVS" key="pvs.search.numeric.check"/>"+label);	
		obj.value ="";
		obj.focus();
		return false;
	}
	return true;
}

function validate()
{
	trimFields();
//	var zipCodeFieldChecked = chkInteger(document.forms[0].zipcode,"<bean:message bundle="PVS" key="pvs.zipcode"/>");
	var zipCodeFieldChecked = true;

	//	if (zipCodeFieldChecked && document.forms[0].zipcode.value!="")
		if (document.forms[0].zipcode.value!="")
		{
			if(document.forms[0].mainOffice.checked || document.forms[0].fieldOffice.checked || document.forms[0].techniciansZip.checked){
					zipCodeFieldChecked = true;
				}
			else
				{
					alert("<bean:message bundle="PVS" key="pvs.search.zip.option.message"/>");
					zipCodeFieldChecked = false;
				}
		}
	if(zipCodeFieldChecked == true){
			searchpartner();
	}		
}
function searchpartner()
{
	document.forms[0].ref.value = "search";
	document.forms[0].search.value = "search";
	document.forms[0].submit();
	return true;	
	
}

function assignpartner(pid)
{
	document.forms[0].selectedpartner.value = pid;
	document.forms[0].currentPartner.value = pid;
	document.forms[0].ref.value = "assign";
	document.forms[0].hiddensave.value="save";
	document.forms[0].submit();
	return true;
	
}


function Backaction()
{
	<%
	if(fromtype.equals("powo"))
	 {
	%> 
	          document.forms[0].action = "POWOAction.do?jobid=<%=request.getParameter("typeid")%>";
			  document.forms[0].submit();
			  return true;
	         
  <% } %>
}

function sortwindow()
{
	str = './SortAction.do?type=partnerSearch&ref=Search&name=<bean:write name="Partner_SearchForm" property="name"/>&keyword=<bean:write name="Partner_SearchForm" property="searchTerm"/>&partnersearchType=<bean:write name="Partner_SearchForm" property="partnerTypeName"/>&mvsaVers=<bean:write name="Partner_SearchForm" property="mcsaVersion"/>&zip=<bean:write name="Partner_SearchForm" property="zipcode"/>&radius=<bean:write name="Partner_SearchForm" property="radius"/>&mainoffice=<bean:write name="Partner_SearchForm" property="mainOffice"/>&fieldOffice=<bean:write name="Partner_SearchForm" property="fieldOffice"/>&techhi=<bean:write name="Partner_SearchForm" property="techniciansZip"/>&rating=<bean:write name="Partner_SearchForm" property="partnerRating"/>&technician=<bean:write name="Partner_SearchForm" property="technicians"/>&certification=<bean:write name="Partner_SearchForm" property="certifications"/>&corecomp=<bean:write name="Partner_SearchForm" property="coreCompetency"/>&chekBox1=<bean:write name="Partner_SearchForm" property="cbox1"/>&chekBox2=<bean:write name="Partner_SearchForm" property="cbox2"/>';
	//alert(str);
	//window.open(str,'','width=400,height=350,left=0,top=0,screenX=0,screenY=100,resizable=1,status=yes');
	p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' ); 
	return true;
}


</script>
</html:html>
