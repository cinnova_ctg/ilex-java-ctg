<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For adding incident details against a partner.
*
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<bean:define id = "dcpvs" name = "dynamiccombo" scope = "request"/>
<html:html>
<HEAD>


<%@ include  file = "/Header.inc" %>
<%@ include  file="/SendEmail.inc" %>
<META name = "GENERATOR" content = "IBM WebSphere Studio">

<META http-equiv = "Content-Style-Type" content = "text/css">
<LINK href = "styles/style.css" rel = "stylesheet"	type = "text/css">
<script language = "javascript" src = "javascript/JLibrary.js"></script>
<script language = "javascript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<title></title>


<%@ include  file = "/NMenu.inc" %>

</head>

<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "MM_preloadImages( 'images/sales1b.gif' , 'images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >
<logic:notEqual name = "AddIncidentForm" property = "page" value = "checklist">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
  <tr>
    <td valign="top" width = "100%"> 
      <table  border = "0" cellspacing = "0" cellpadding = "0" height = "18"  width = "100%">
        <tr >         
         <td id="pop2" width="120" class="Ntoprow1" align="center"><a href="#"  onmouseout="MM_swapImgRestore();popDown('elMenu2',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.manageprofile"/></center></a></td>         
         <td id="pop1" width="120" class="Ntoprow1" align="center"><a href="#"  onmouseout="MM_swapImgRestore();popDown('elMenu1',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.managepartner"/></center></a></td> 
         <td    width = "120" class = "Ntoprow1"><a href = "Partner_Search.do?ref=search&type=v"  onmouseout = "MM_swapImgRestore();"    onmouseover = "MM_swapImage( 'Image25', '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><bean:message bundle = "PVS" key = "pvs.partnersearch"/></a></td>
		 <td width = "120" class = "Ntoprow1" align="center"><a href = "MCSA_View.do"  onmouseout = "MM_swapImgRestore();"    onmouseover = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><bean:message bundle = "PVS" key = "pvs.managemcsa"/></a></td>
		 <td  class = "Ntoprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<SCRIPT language = JavaScript1.2>



if ( isMenu ) {
 //start
arMenu1 = new Array(
120,
findPosX( 'pop1' ),findPosY( 'pop1' ),
"","",
"","",
"","",
"Minuteman","#",1,
"Standard Partner","#",1,
"New Partners","PVSSearch.do?orgTopName=all&action=newadded&enablesort=true",0,
"Recent Updates","PVSSearch.do?orgTopName=all&action=resentupdated&enablesort=true",0,
"Annual Review Due","PVSSearch.do?orgTopName=all&action=annualreport&enablesort=true",0
)
arMenu1_1=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Minuteman Partners",0,
"Update","PVSSearch.do?orgTopName=Minuteman Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Minuteman Partners&action=delete",0
<%}%>
)
arMenu1_2=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Certified Partners",0,
"Update","PVSSearch.do?orgTopName=Certified Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Certified Partners&action=delete",0
<%}%>
)
  arMenu2=new Array(
120,
findPosX('pop2'),findPosY('pop2'),
"","",
"","",
"","",
"Update","Partner_Edit.do?pid=<bean:write name="AddIncidentForm" property="pid"/>&function=Update",0,
"Incident Reports","#",1,
"Job History","javascript:jobHistory();",0
<c:if test="${sessionScope.RdmRds eq 'Y'}"> 
	<c:if test="${requestScope.topPartnerName ne 'Minuteman Partners'}"> 	
		,"Convert To Minuteman","Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="AddIncidentForm" property="pid"/>&convert=M",0
	</c:if>
	<c:if test="${requestScope.topPartnerName ne 'Certified Partners'}"> 	
		,"Convert To Standard","Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="AddIncidentForm" property="pid"/>&convert=S",0
	</c:if>
</c:if>
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","javascript:del();",0
<%}%>
<c:if test="${sessionScope.RdmRds eq 'N'}"> 
	,"Request Restriction","javascript:gettingAjaxDataForEmail('<bean:write name="AddIncidentForm" property="pid"/>');",0
</c:if>
<c:if test="${sessionScope.RdmRds eq 'Y'}"> 
	<c:if test="${sessionScope.partnerRegStatus eq 'R'}"> 
		,"Send Username/Password","javascript:reSendUnamePwd('<bean:write name="AddIncidentForm" property="pid"/>');",0
	</c:if>
	<c:if test="${sessionScope.partnerRegStatus eq 'N'}">
		,"Re-send Registration Email","javascript:reSendRegEmail('<bean:write name="AddIncidentForm" property="pid"/>');",0
	</c:if>
</c:if>
)
arMenu2_2=new Array(
"View","AddIncident.do?pid=<bean:write name="AddIncidentForm" property="pid"/>&function=view&page=PVS",0,
"Post","AddIncident.do?pid=<bean:write name="AddIncidentForm" property="pid"/>&function=add&page=PVS",0
)
document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");

//end
}

</script>
</logic:notEqual>
<logic:present name = "AddIncidentForm" property = "addmessage">
	<logic:equal name = "AddIncidentForm" property = "addmessage" value = "-9001">
	<table>
		<tr><td  width = "100%" class = "message" height = "30" ><bean:message bundle = "PVS" key = "pvs.additionfailed"/></td></tr>
	</table>	
	</logic:equal>
	
	<logic:equal name = "AddIncidentForm" property = "addmessage" value = "-1">
	<table>
		<tr><td  width = "100%" class = "message" height = "30" ><bean:message bundle = "PVS" key = "pvs.additionfailed"/></td></tr>
	</table>	
	</logic:equal>
</logic:present>

<table>

<html:form action = "AddIncident" target = "ilexmain">


<html:hidden property = "pid" />
<html:hidden property = "function" />
<html:hidden property = "partnername" />
<html:hidden property = "page"/>

<html:hidden property = "jobid" />
<html:hidden property = "viewjobtype" />
<html:hidden property = "ownerId" />


 <table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
<tr>
  <td width = "2" height = "0"></td>
  <td><table border = "0" cellspacing = "1" cellpadding = "1" >
  <tr>  
    <td  colspan="2" class = "labeleboldwhite" height = "30"  ><b><bean:message bundle = "PVS" key = "pvs.manageincidentreport"/><bean:write name = "AddIncidentForm" property = "partnername"/></b></td>
  </tr> 
     
	
  <tr> 
    <td colspan="2"class="labellobold" height="20"><bean:message bundle="PVS" key="pvs.incidentreport"/></td> 
   
  </tr>
  
  <tr>
	<td  class = "labelebold" height = "20"><bean:message bundle = "PVS" key = "pvs.increport.incidenttype"/></td>
	<td   class = "labele"><html:radio property = "incidenttype" value = "Good">Good</html:radio>&nbsp;&nbsp;&nbsp;
					     <html:radio property = "incidenttype" value = "Bad">Bad</html:radio></td>
 </tr>
 
<tr>
	<td   class = "labelobold" height = "20"><bean:message bundle = "PVS" key = "pvs.date"/></td>
	<td   class = "labelo"><html:text  styleClass = "textbox" size = "10" property = "date"  readonly = "true"/>
    <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].date , document.forms[0].date , 'mm/dd/yyyy' )" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
	
</tr>

<tr>
	<td  class = "labelebold" height = "20"><bean:message bundle = "PVS" key = "pvs.time"/></td>
	<td   class = "labele">
		<html:text  styleClass = "textbox" size = "2" property = "hours"  maxlength = "2"/>:<html:text  styleClass = "textbox" size = "2" property = "minutes"  maxlength = "2"/>&nbsp;&nbsp;
		<html:select name = "AddIncidentForm" property = "am"  styleClass = "comboe" >
			<html:optionsCollection name = "dcpvs"  property = "ampmlist"  label = "label"  value = "value" />
		</html:select>
	</td>
	
</tr>

<tr>
	<td   class = "labelobold" height = "20"><bean:message bundle = "PVS" key = "pvs.endcustomer"/></td>
	<td  class = "labelo">
	<html:select name = "AddIncidentForm" property = "endcustomer" size="1" styleClass = "comboo" >
	<html:optionsCollection name = "dcpvs"  property = "customerlist"  label = "label"  value = "value" /></html:select></td>	
</tr>

<tr>
	<td   class = "labelebold" height = "20"><bean:message bundle = "PVS" key = "pvs.technicianname"/></td>
	<td   class = "labele">
	<html:select name = "AddIncidentForm" property = "technicianname" size = "1" styleClass = "comboe" >
	<html:optionsCollection name = "dcpvs"  property = "technicianlist"  label = "label"  value = "value" /></html:select></td>
</tr>

 <tr>
	<td  class = "labelobold" height = "20"><bean:message bundle = "PVS" key = "pvs.increport.partnerstatus"/></td>
	<td   class = "labelo"><bean:write name = "AddIncidentForm" property = "partnerstatus" /></td>
</tr> 

<tr>
	<td  class = "labelebold" height = "20"><bean:message bundle = "PVS" key = "pvs.increport.partnerresponse"/></td>
	<td   class = "labele"><html:textarea  styleClass = "textbox" property = "partnerresponse" cols = "25" rows = "3" ></html:textarea></td>
</tr> 

  
  <tr>
   
	<td   class = "labellobold" height = "20" colspan = 2><bean:message bundle = "PVS" key = "pvs.incident"/></td>
 	
  </tr>
  
<% int j = 0;
String bgcolor1 = "";%>
<logic:present  name = "ilist" scope = "request">
 
 <logic:iterate id = "il" name = "ilist"> 
 
 <% j++; %>
 <% if( ( j%2 ) == 0 )
		{
			bgcolor1 = "labelo"; 
		
		} 
		else
		{
			bgcolor1 = "labele";
		}%>
 
 
 <tr>
	<td  class = "<%= bgcolor1 %>" height = "20"><html:multibox property = "ckboxincident"><bean:write name = "il" property = "ckboxincidentvalue"/></html:multibox></td>
	<td  class = "<%= bgcolor1 %>"><bean:write name = "il" property = "ckboxincidentlist"/></td>
 </tr>
 
 </logic:iterate>
</logic:present>

<%if( bgcolor1.equals( "labelo" ) ) 
{
	bgcolor1 = "labele";
}
else
{
	bgcolor1 = "labelo";
	
}%>

<tr>
	<td   class = "<%= bgcolor1 %>"  height = "20"><html:checkbox property = "ckboxother" ></html:checkbox></td>
	<td  class = "<%= bgcolor1 %>"><html:textarea  styleClass = "textbox" property = "notes" cols = "25" rows = "3" ></html:textarea></td>
</tr>


 <tr>
    
	<td   class = "labellobold" colspan = 2 height = "20" ><bean:message bundle = "PVS" key = "pvs.recommendedaction"/></td>
 	
  </tr>
<logic:present  name = "alist" scope = "request">
 <% int i = 0;
 String bgcolor = "";%>
 <logic:iterate id="al" name="alist"> 
 
 <% if( ( i%2 ) == 0 )
		{
			bgcolor = "labele"; 
		
		} 
		else
		{
			bgcolor = "labelo";
		}%>
 
 <tr>
	<td  class = "<%= bgcolor %>" height = "20"><html:multibox  property = "ckboxaction"><bean:write name = "al" property = "ckboxactionvalue"/></html:multibox></td>
	<td  class = "<%= bgcolor %>"><bean:write name = "al" property = "ckboxactionlist"/></td>
 </tr>
 <% i++; %>
 </logic:iterate>
</logic:present>
 <tr> 
    <td colspan = "2" class = "buttonrow"> 
      <html:submit property = "save" styleClass = "button" onclick = "return validate();"><bean:message bundle = "PVS" key = "pvs.save" /></html:submit>
      <html:reset property = "reset" styleClass = "button"><bean:message bundle = "PVS" key = "pvs.reset" /></html:reset>
    </td>
  </tr>
<tr align = "right"> 
    <td colspan = "2" height = "37"></td>
  </tr>
   <jsp:include page = '/Footer.jsp'>
  	 <jsp:param name = 'colspan' value = '28'/>
     <jsp:param name = 'helpid' value = 'roleprivilege'/>
   </jsp:include>
   
</table>
 </td>
 </tr> 
</table>
</html:form>
</table>
<script>
function validate()
{
	
	
	trimFields();
	
	if( isBlank(document.forms[0].hours.value ) )
	{
		errormsg = "<bean:message bundle = "PVS" key = "pvs.emptymsg" />";
		
		warningEmpty( document.forms[0].hours , "<bean:message bundle = "PVS" key = "pvs.hours"/>" , errormsg );
		
		return false;
	}
	if( isBlank(document.forms[0].minutes.value ) )
	{
		errormsg = "<bean:message bundle = "PVS" key = "pvs.emptymsg"/>";
		
		warningEmpty( document.forms[0].minutes , "<bean:message bundle = "PVS" key = "pvs.minutes"/>" , errormsg );
		
		return false;
	}
	if( !ValidateNum( document.forms[0].hours , false , 0 , 12 ) )
	{
		document.forms[0].hours.focus();
		return false;
	}
	if( !ValidateNum( document.forms[0].minutes , false , 0 , 59 ) )
	{
		document.forms[0].minutes.focus();
		return false;
	}
	
	/*  Start: Added By Atul 16/01/2007 */
/*
	if( document.forms[0].endcustomer.value == "0" ) 
	{
		errormsg = "<bean:message bundle = "PVS" key = "pvs.selectavalue"/> <bean:message bundle = "PVS" key = "pvs.endcustomer"/>";
		alert( errormsg );
		document.forms[0].endcustomer.focus();
		return false;
	}
	if( document.forms[0].technicianname.value == "0" ) 
	{
		errormsg = "<bean:message bundle = "PVS" key = "pvs.selectavalue"/> <bean:message bundle = "PVS" key = "pvs.technicianname"/>";
		alert( errormsg );
		document.forms[0].technicianname.focus();
		return false;
		
	}
*/
	/*  End: Added By Atul 16/01/2007 */
	
	if( document.forms[0].page.value == 'checklist' )
	{
		window.close();
	}
	return true;
	
	//return CheckQuotes ();
	
}

</script>

<script>
function trimFields()
{

	var field = document.forms[0];

	for( var i = 0; i < field.length; i++ )
	{
		if( field[i].type == 'text' )
		{
			var temp1 = field[i].value;

			var temp2 = "";
			var flag = 0;
			if( temp1.indexOf(" " , temp1.length - 1 ) >= 0 ) {
				for( var j = temp1.length; j > 0; j-- ){
				if( ( temp1.substring( j - 1 , j ) == " " )&&( flag == 0 ) ){
					temp2 = temp1.substring( 0 , j - 1 );
				} else { flag = 1; break;}
			}//for
		} else { temp2 = temp1; }

		temp1 = temp2;
		temp2 = "";
		flag = 0;
		if( temp1.indexOf( " " ) == 0 ) {
			for( var j = 0; j < temp1.length; j++ ){
			if( ( temp1.substring( j , j + 1 ) ==" ")&&( flag == 0 ) ){
			temp2 = temp1.substring( j+1 , temp1.length );
			} else {flag = 1; break;}
			}//for
		} else { temp2 = temp1; }

		field[i].value = temp2;
		}
	}
return true;
}

</script>
<!-- FUNCTION TRIM END -->

<script>



function CheckQuotes ()
{
	
	var field = document.forms[0];
	for( i = 0; i < field.length; i++ )
	{
		if( field[i].type == 'text' )
		{
			
			if( ( field[i].value.indexOf( "'" ) >= 0 ) || ( field[i].value.indexOf( "\"" ) >= 0 ) || ( field[i].value.indexOf( "&" ) >= 0 ) )
			{

				errormsg = "<bean:message bundle="RM" key = "rm.warnmsgchar"/>";
				alert( errormsg );
				field[i].focus();
				return false;
			}
		}
	}
return true;
}

function jobHistory() 
{		
	document.forms[0].action = "JobInformationperPartner.do?pid=<bean:write name="AddIncidentForm" property="pid"/>&from=Partner_Edit";
	document.forms[0].submit();
	return true;	
} 
function del() 
{
		var convel = confirm( "Are you sure you want to delete the Partner?" );
		if( convel )
		{	
			document.forms[0].action = "Partner_Edit.do?fromId=<bean:write name = "AddIncidentForm" property = "pid"/>&func=delete";
			document.forms[0].submit();
			return true;	
		}
} 
	
</script>

</BODY>
</html:html>
