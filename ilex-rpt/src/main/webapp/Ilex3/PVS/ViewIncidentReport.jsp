<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For displaying incident details against a partner.
*
-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

	
<bean:define id = "incform" name = "incidentform" scope = "request"/>



<html:html>
<HEAD>


<%@ include  file="/Header.inc" %>
<%@ include  file="/SendEmail.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<script language="javascript" src="javascript/ilexGUI.js"></script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<title></title>


<%@ include  file="/NMenu.inc" %>




</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >

<%if(request.getParameter("page").equalsIgnoreCase("PRM"))
{%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table  border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="left"> 
          <td  id="pop1" width="130" class="toprow1"><a href="javascript:history.go(-1);"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="PRM" key="prm.back"/></a></td>
		   <td  id="pop2" width=670 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>	
	
<%}
else
{ %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" width = "100%"> 
      <table  border="0" cellspacing="0" cellpadding="0" height="18"  width = "100%">
        <tr >           
          <td id="pop2" width="120" class="Ntoprow1" align="center"><a href="#"  onmouseout="MM_swapImgRestore();popDown('elMenu2',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.manageprofile"/></center></a></td>
          <td id="pop1" width="120" class="Ntoprow1" align="center"><a href="#"  onmouseout="MM_swapImgRestore();popDown('elMenu1',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.managepartner"/></center></a></td>                    
          <td  width="120" class="Ntoprow1" align="center"><a href="Partner_Search.do?ref=search&type=v"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.partnersearch"/></center></a></td>
		  <td  width="120" class="Ntoprow1" align="center"><a href="MCSA_View.do"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="menufont" style="width: 120px"><center><bean:message bundle="PVS" key="pvs.managemcsa"/></center></a></td>
          <td  class="Ntoprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<%} %>

<SCRIPT language=JavaScript1.2>



if (isMenu) {
arMenu1 = new Array(
120,
findPosX( 'pop1' ),findPosY( 'pop1' ),
"","",
"","",
"","",
"Minuteman","#",1,
"Standard Partner","#",1,
"New Partners","PVSSearch.do?orgTopName=all&action=newadded&enablesort=true",0,
"Recent Updates","PVSSearch.do?orgTopName=all&action=resentupdated&enablesort=true",0,
"Annual Review Due","PVSSearch.do?orgTopName=all&action=annualreport&enablesort=true",0
)
arMenu1_1=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Minuteman Partners",0,
"Update","PVSSearch.do?orgTopName=Minuteman Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Minuteman Partners&action=delete",0
<%}%>
)
arMenu1_2=new Array(
"Add","Partner_Edit.do?function=Add&pid=0&orgTopName=Certified Partners",0,
"Update","PVSSearch.do?orgTopName=Certified Partners&action=update",0
<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","PVSSearch.do?orgTopName=Certified Partners&action=delete",0
<%}%>
)

arMenu2=new Array(
120,
findPosX('pop2'),findPosY('pop2'),
"","",
"","",
"","",
"Update","Partner_Edit.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=Update",0,
"Incident Reports","#",1,
"Job History","javascript:jobHistory();",0

<c:if test="${sessionScope.RdmRds eq 'Y'}"> 
	<c:if test="${requestScope.topPartnerName ne 'Minuteman Partners'}"> 	
		,"Convert To Minuteman","Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="Partner_EditForm" property="pid"/>&convert=M",0
	</c:if>
	<c:if test="${requestScope.topPartnerName ne 'Certified Partners'}"> 	
		,"Convert To Standard","Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<bean:write name="Partner_EditForm" property="pid"/>&convert=S",0
	</c:if>
</c:if>

<%if(session.getAttribute("RDM")!=null && session.getAttribute("RDM").equals("Y")){%>
,"Delete","javascript:delPartner();",0
<%}%>
<c:if test="${sessionScope.RdmRds eq 'N'}"> 
	,"Request Restriction","javascript:gettingAjaxDataForEmail('<bean:write name="AddIncidentForm" property="pid"/>');",0
</c:if>
<c:if test="${sessionScope.RdmRds eq 'Y'}"> 
	<c:if test="${sessionScope.partnerRegStatus eq 'R'}"> 
		,"Send Username/Password","javascript:reSendUnamePwd('<bean:write name="AddIncidentForm" property="pid"/>');",0
	</c:if>
	<c:if test="${sessionScope.partnerRegStatus eq 'N'}">
		,"Re-send Registration Email","javascript:reSendRegEmail('<bean:write name="AddIncidentForm" property="pid"/>');",0
	</c:if>
</c:if>
//"Change Password","passwordchange.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=add&page=PVS",0
)
arMenu2_2=new Array(
//"View","AddIncident.do?pid=<bean:write name="Partner_EditForm" property="pid"/>&function=view&page=PVS",0,
"<bean:message bundle="PVS" key="pvs.delete"/>","javascript:del();",0,
"Post","AddIncident.do?pid=<bean:write name="AddIncidentForm" property="pid"/>&function=add&page=PVS",0
)
  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}

</script>


<table>
	
<logic:present name = "incform" property="addmessage">
	<logic:greaterEqual name="incform" property="addmessage" value="0">
		<tr><td   class="message" height="30" ><bean:message bundle="PVS" key="pvs.addedsuccessfully"/></td>
		</tr>	
	</logic:greaterEqual>
</logic:present>

<logic:present name = "incform" property="deletemessage">
	<logic:equal name="incform" property="deletemessage" value="0">
		<tr><td  class="message" height="30" ><bean:message bundle="PVS" key="pvs.deletedsuccessfully"/></td>
		</tr>
	</logic:equal>
	<logic:equal name="incform" property="deletemessage" value="-9001">
		<tr><td   class="message" height="30" ><bean:message bundle="PVS" key="pvs.deletionfailed"/></td>
		</tr>
	</logic:equal>	
</logic:present>
	
</table>
<%String pid1=request.getParameter("pid"); %>
<table>
<html:form action = "AddIncident">
<%if(request.getParameter("function").equalsIgnoreCase("add")) 
{%>


<script>
function del() 
{
		
		if(<%= request.getAttribute("inidvalue")!=null %>)
		{
			var msg="<bean:message bundle="RM" key="rm.deletemsg"/>"
			var convel = confirm( msg );
			if( convel )
			{
				
				document.forms[0].action = "AddIncident.do?did=<%=request.getAttribute("inidvalue")%>&pid=<%=pid1%>&action=1&function=delete&page=<%=request.getParameter("page")%>";
				
				document.forms[0].submit();
								
				return true;	
			}
			
		}
		else
		{
			
			alert( "<bean:message bundle="RM" key="rm.selectdeletion"/>" );
			
		}
}

</script>
 

<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<tr>
  <td  width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" > 
  
  

  <tr> 
    <td colspan="2" class="labeleboldwhite" height="30" ><b><bean:message bundle="PVS" key="pvs.viewincidentreport"/><bean:write name = "incform" property = "partnername"/></b></td>
  </tr> 
    

<tr>
	<td  colspan="2" class="labellobold" height="20" ><bean:message bundle="PVS" key="pvs.incidentreporthash"/>1</td>
 	
</tr>
<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.increport.incidenttype"/></td>
	<td  class="labele" height="20"><bean:write name = "incform" property = "incidenttype"/></td>
	
</tr>

<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.date"/></td>
	<td  class="labelo" height="20"><bean:write name = "incform" property = "date"/></td>
	
</tr>
<tr>
	<td class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.time"/></td>
	<td class="labele" height="20" ><bean:write name = "incform" property = "time"/></td>
	
</tr>
<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.endcustomer"/></td>
	<td  class="labelo" height="20"><bean:write name = "incform" property = "endcustomer"/></td>
	
</tr>

<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.technicianname"/></td>
	<td  class="labele" height="20"><bean:write name = "incform" property = "technicianname"/></td>
</tr>

<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.increport.partnerstatus"/></td>
	<td   class="labelo" height="20"><bean:write name="incform" property="partnerstatus" /></td>
</tr>

<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.increport.partnerresponse"/></td>
	<td   class="labele" height="20"><bean:write name="incform" property="partnerresponse" /></td>
</tr>  

<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.incident"/></td>
	<td  class="labelo" height="20"><bean:write name = "incform" property = "incidentlist"/></td>
</tr>

<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.recommendedaction"/></td>
	<td  class="labele" height="20"><bean:write name = "incform" property = "actionlist"/></td>
</tr>

<logic:notEqual name = "incform" property = "jobname" value = "" >
	<tr>
		<td  class = "labelobold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.msaname"/>
		</td>
		<td  class = "labelo" height = "20">
			<bean:write name = "incform" property = "msaname"/>
		</td>
	</tr>
	
	<tr>
		<td  class = "labelebold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.appendixname"/>
		</td>
		<td  class = "labele" height = "20">
			<bean:write name = "incform" property = "appendixname"/>
		</td>
	</tr>
	
	<tr>
		<td  class = "labelobold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.jobname"/>
		</td>
		<td  class = "labelo" height = "20">
			<bean:write name = "incform" property = "jobname"/>
		</td>
	</tr>
</logic:notEqual>

<tr>
	<td  class = "labelebold" height = "20">
		<bean:message bundle = "PVS" key = "pvs.notes"/>
	</td>
	<td  class = "labele" height = "20">
		<bean:write name = "incform" property = "notes"/>
	</td>
</tr>

<%}else
{%>


<logic:present  name="allreportslist" scope="request">
<% int maxreports =0;
	if( request.getAttribute( "totalreports") != null )
	{
		maxreports = ( int ) Integer.parseInt( request.getAttribute( "totalreports" ).toString()); 
	}%>


<script>

function del() 
{		
		document.forms[0].temp.value='';
		
		if( <%= maxreports %> != 0 )
		{
			if( <%= maxreports %> == 1 )
			{
				if(document.forms[0].document.forms[0].chkdelete.checked )
				{
					document.forms[0].temp.value = document.forms[0].chkdelete.value;
					var msg="<bean:message bundle="PVS" key="pvs.deletemsg"/>"
					var convel = confirm( msg );
					if( convel )
					{
						document.forms[0].action = "AddIncident.do?did="+document.forms[0].temp.value+"&pid=<%=pid1%>&action=2&function=delete&page=<%=request.getParameter("page")%>";
						document.forms[0].submit();			
						return true;	
					}
					
				}
				else
				{
					alert( "<bean:message bundle="PVS" key="pvs.selectdeletion"/>" );
					return false;
				}
			}
			else
			{
				
			
				for(  var j = 0; j<document.forms[0].chkdelete.length; j++)
				{
						
					if(document.forms[0].document.forms[0].chkdelete[j].checked)
					{	
						document.forms[0].temp.value += document.forms[0].chkdelete[j].value+',';
					}
						
				}
				
				document.forms[0].temp.value = document.forms[0].temp.value.substring(0,document.forms[0].temp.value.length-1);
				
				if(document.forms[0].temp.value == "")
				{
					alert( "<bean:message bundle="PVS" key="pvs.selectdeletion"/>" );
				}
				else
				{
					var msg="<bean:message bundle="PVS" key="pvs.deletemsg"/>"
						var convel = confirm( msg );
						if( convel )
						{
							
							document.forms[0].action = "AddIncident.do?did="+document.forms[0].temp.value+"&pid=<%=pid1%>&action=2&function=delete&page=<%=request.getParameter("page")%>";
							
							document.forms[0].submit();
											
							return true;	
						}
				}
			}
		}
		
}

</script>
</logic:present>

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
<tr>
  <td  width = "2" height = "0"></td>
  <td><table border = "0" cellspacing = "1" cellpadding = "1" > 
  <logic:present  name = "allreportslist" scope = "request">
  
  <html:hidden property = "temp"/>
<% int i=0;%>
<logic:iterate id = "arl" name = "allreportslist">

  <tr> 
    <td class = "labeleboldwhite" height = "30" colspan = "2">
    	<html:multibox name = "AddIncidentForm" property = "chkdelete">
    		<bean:write name = "arl" property = "inid"/>
    	</html:multibox>&nbsp;
   		<bean:message bundle = "PVS" key = "pvs.viewincidentreport"/>
   		<bean:write name = "arl" property = "partnername"/>
   	</td>
  </tr> 
     

<tr>
	<td  colspan="2" class="labellobold" height="20" ><bean:message bundle = "PVS" key = "pvs.incidentreporthash"/><%=i+1 %></td>
</tr>

<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.increport.incidenttype"/></td>
	<td  class="labele" height="20"><bean:write name = "arl" property = "incidenttype"/></td>
	
</tr>
<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.date"/></td>
	<td  class="labelo" height="20"><bean:write name = "arl" property = "date"/></td>
	
</tr>

<tr>
	<td class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.time"/></td>
	<td class="labele" height="20" ><bean:write name = "arl" property = "time"/></td>
</tr>

<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.endcustomer"/></td>
	<td  class="labelo" height="20"><bean:write name = "arl" property = "endcustomer"/></td>
</tr>

<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.technicianname"/></td>
	<td  class="labele" height="20"><bean:write name = "arl" property = "technicianname"/></td>
</tr>

<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.increport.partnerstatus"/></td>
	<td   class="labelo" height="20"><bean:write name="arl" property="partnerstatus" /></td>
</tr> 

<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.increport.partnerresponse"/></td>
	<td   class="labele" height="20"><bean:write name="arl" property="partnerresponse" /></td>
</tr>

<tr>
	<td  class="labelobold" height="20"><bean:message bundle="PVS" key="pvs.incident"/></td>
	<td  class="labelo" height="20"><bean:write name = "arl" property = "incidentlist"/></td>
</tr>

<tr>
	<td  class="labelebold" height="20"><bean:message bundle="PVS" key="pvs.recommendedaction"/></td>
	<td  class="labele" height="20"><bean:write name = "arl" property = "actionlist"/></td>
</tr>

<logic:notEqual name = "arl" property = "jobname" value = "" >
	<tr>
		<td  class = "labelobold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.msaname"/>
		</td>
		<td  class = "labelo" height = "20">
			<bean:write name = "arl" property = "msaname"/>
		</td>
	</tr>
	
	<tr>
		<td  class = "labelebold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.appendixname"/>
		</td>
		<td  class = "labele" height = "20">
			<bean:write name = "arl" property = "appendixname"/>
		</td>
	</tr>
	
	<tr>
		<td  class = "labelobold" height = "20">
			<bean:message bundle = "PVS" key = "pvs.jobname"/>
		</td>
		<td  class = "labelo" height = "20">
			<bean:write name = "arl" property = "jobname"/>
		</td>
	</tr>
</logic:notEqual>

<tr>
	<td  class = "labelebold" height = "20">
		<bean:message bundle = "PVS" key = "pvs.notes"/>
	</td>
	<td  class = "labele" height = "20">
		<bean:write name = "arl" property = "notes"/>
	</td>
</tr>


<tr>
	<td colspan="2" height="20"></td>
</tr>

<%i++; %>
</logic:iterate>


 
</logic:present>
<%} %>

<% if( request.getAttribute( "totalreports") != null )
{
	int maxreports = ( int ) Integer.parseInt( request.getAttribute( "totalreports" ).toString()); 
	if(maxreports==0)
		{%>
		
		<tr><td  class="message" height="30" colspan="2"><bean:message bundle="PVS" key="pvs.noreportspresent"/></td></tr>
		<tr><td colspan="2" height="100" ></td></tr>
		
		<%} 
}%>	
	
<jsp:include page = '/Footer.jsp'>
  	<jsp:param name = 'colspan' value = '28'/>
    <jsp:param name = 'helpid' value = 'roleprivilege'/>
</jsp:include>
 </table>
 </td>
 </tr> 
</table>

<logic:present name = "refreshtree" scope = "request">
	<script>
			parent.ilexleft.location.reload();
	</script>
</logic:present>


</html:form>
</table>
</BODY>
<logic:present name="totalreports" scope="request">
  		 <logic:equal name="totalreports" value="0"> 
  		 	<script>
  		 		function del(){
  		 		history.go(0);
  		 		} 
  		 	</script>
  		 </logic:equal>
</logic:present>  

<script>
function jobHistory() 
{		
	document.forms[0].action = "JobInformationperPartner.do?pid=<bean:write name="AddIncidentForm" property="pid"/>&from=Partner_Edit";
	document.forms[0].submit();
	return true;	
} 
function delPartner() 
{
		var convel = confirm( "Are you sure you want to delete the Partner?" );
		if( convel )
		{	
			document.forms[0].action = "Partner_Edit.do?pid=<bean:write name = "AddIncidentForm" property = "pid"/>&func=delete";
			document.forms[0].submit();
			return true;	
		}
} 
</script>
</html:html>
