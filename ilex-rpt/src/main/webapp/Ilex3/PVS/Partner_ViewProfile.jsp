
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"
	type="text/css">
<title></title>


<%@ include  file="Menu.inc" %>

<SCRIPT language=JavaScript1.2>

if (isMenu) {
arMenu1=new Array(
120,
7,18,
"","",
"","",
"","",
"Edit","Partner_Edit.jsp",0,
"Delete","#",0
)

arMenu2=new Array(
120,
189,18,
"","",
"","",
"","",
"View","Addincident.do?pid=<%=request.getParameter("pid")%>&function=view",0,
"Post","Addincident.do?pid=<%=request.getParameter("pid")%>&function=add",0
)

arMenu3=new Array(
120,
340,18,
"","",
"","",
"","",
"","#",0

)

arMenu4=new Array(
100,
460,18,
"","",
"","",
"","",
"","#",0
)


arMenu5=new Array(
100,
580,18,
"","",
"","",
"","",
"","#",0
)


  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}

</script>
<script language="JavaScript">

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

</script>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td ><img src="images/spacer.gif" width="1" height="1"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="800" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center"> 
          <td  width="150" class="toprow1"><html:link href="#"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" styleClass="drop">Manage Partner Profile</html:link></td>
          <td  width="170" class="toprow1"><html:link href="#"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" styleClass="drop">Manage Incident Reports</html:link></td>
         <td  width="100" class="toprow1"><html:link href="Partner_Search.jsp"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" styleClass="drop">Locate Partner</html:link></td>
		 <td  width="140" class="toprow1"><html:link href="Change_Notification"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" styleClass="drop"> Manage Notification</html:link></td>
		 <td width="100" class="toprow1"><html:link forward="MCSA_View"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" styleClass="drop">Manage MCSA</html:link></td>
		  <td  width=140 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          </tr>
      </table>
    </td>
  </tr>
</table>

<br>

<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
<tr>
  <td  width="2" height="0"></td>
  <td><table width="90%"border="0" cellspacing="2" cellpadding="2" >
  <tr><td colspan="9"class="label4" height="30" ><b><bean:message bundle="PVS" key="pvs.profileview"/></b><br>Current Rating:1&nbsp&nbsp;Incidents Reported:0</td>
  </tr>
     <tr><td colspan="9" class="BlackLine"  height="5"></td></tr>
<tr>
	<td  class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.username"/></td>
	<td  class="label2" height="20" colspan="5"> yourit</td>
</tr>
<tr>
	<td  class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.date"/></td>
	<td  class="label1" height="20" colspan="5">6/30/2005</td>
</tr>
<tr>
	<td  class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.partnercandidate"/></td>
	<td  class="label2" height="20" colspan="5">Your I.T. </td>
</tr>
<tr>
	<td  class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.taxid"/></td>
	<td  class="label1" height="20" colspan="5">20-2609210 </td>
</tr>
<tr>
	<td  class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.type"/></td>
	<td  class="label2" height="20" colspan="5">Corp  </td>
</tr>
<tr>
	<td  class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.dateofincorporation"/></td>
	<td  class="label1" height="20" colspan="5">04/15/2005 l</td>
</tr>
<tr>
	<td   class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.corecompetency"/></td>
	<td   class="label2" height="20" colspan="5">Network/PC/Server</td>
</tr>
<tr>
	<td   class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.address1"/></td>
	<td   class="label1" height="20" colspan="5">P.O. Box 687  </td>
</tr>
<tr>
	<td   class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.address2"/></td>
	<td   class="label2" height="20" colspan="5"></td>
</tr>
<tr>
	<td   class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.city"/></td>
	<td   class="label1" height="20" colspan="5"> Grand Blanc</td>
</tr>
<tr>
	<td   class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.state"/></td>
	<td   class="label2" height="20" colspan="5"> MI</td>
</tr>
<tr>
	<td   class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.zip"/></td>
	<td   class="label1" height="20" colspan="5">48480</td>
</tr>
<tr>
	<td   class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.country"/></td>
	<td   class="label2" height="20" colspan="5">US</td>
</tr>
<tr>
	<td   class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.mainphone"/></td>
	<td   class="label1" height="20" colspan="5">(810) 584-0055   </td>
</tr>
<tr>
	<td   class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.mainfax"/></td>
	<td   class="label2" height="20" colspan="5">(501) 646-7785 </td>
</tr>
<tr>
	<td   class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.companyurl"/></td>
	<td   class="label1" height="20" colspan="5">http://www.yourit.ws</td>
</tr>
<tr>
	<td   class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.primarycontactlastname"/></td>
	<td   class="label2" height="20" colspan="5">Astbury   </td>
</tr>
<tr>
	<td   class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.primarycontactfirstname"/></td>
	<td   class="label1" height="20" colspan="5">Kurt  </td>
</tr>
<tr>
	<td   class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.primarycontactemail"/></td>
	<td   class="label2" height="20" colspan="5">sales@yourit.ws    </td>
</tr>
<tr>
	<td   class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.primarycontactphone"/></td>
	<td   class="label1" height="20" colspan="5">(810) 584-0055 </td>
</tr>
<tr>
	<td   class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.primarycontactext"/></td>
	<td   class="label2" height="20" colspan="5"></td>
</tr>
<tr>
	<td   class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.primarycontactmobile"/></td>
	<td   class="label1" height="20" colspan="5">(810) 908-2965 </td>
</tr>
<tr>
	<td   class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.secondarycontactlastname"/></td>
	<td   class="label2" height="20" colspan="5">Stevens  </td>
</tr>
<tr>
	<td   class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.secondarycontactfirstname"/></td>
	<td   class="label1" height="20" colspan="5">Antony  </td>
</tr>
<tr>
	<td   class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.secondarycontactemail"/></td>
	<td   class="label2" height="20" colspan="5"> sales@yourit.ws</td>
</tr>
<tr>
	<td   class="label1Bold" height="20" colspan="4"> <bean:message bundle="PVS" key="pvs.contactphone"/></td>
	<td   class="label1" height="20" colspan="5">(810) 584-0055  </td>
</tr>
<tr>
	<td   class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.secondarycontactext"/></td>
	<td   class="label2" height="20" colspan="5"></td>
</tr>
<tr>
	<td   class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.secondarycontactmobile"/></td>
	<td   class="label1" height="20" colspan="5">(810) 869-4475  </td>
</tr>
<tr>
	<td   class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.registrationcode"/></td>
	<td   class="label2" height="20" colspan="5">Your I.T. </td>
</tr>
<tr>
	<td   class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.vendorid"/></td>
	<td   class="label1" height="20" colspan="5">YourIT698 </td>
</tr> 
  <tr> 
    <td colspan="9" class="label4Bold" height="30"><bean:message bundle="PVS" key="pvs.physicalfieldoffice"/></td>
  </tr> 
  <tr>
	<td class="tryB" height="20" colspan="2"><bean:message bundle="PVS" key="pvs.address1"/></td>
 	<td class="tryB" height="20" colspan="2"><bean:message bundle="PVS" key="pvs.address2"/></td>
	<td class="tryB" height="20" colspan="1"><bean:message bundle="PVS" key="pvs.state"/></td>
	<td class="tryB" height="20" colspan="1"><bean:message bundle="PVS" key="pvs.zipcode"/></td>
	<td class="tryB" height="20" colspan="2"><bean:message bundle="PVS" key="pvs.phone"/></td>
	<td class="tryB" height="20" colspan="1"><bean:message bundle="PVS" key="pvs.fax"/></td>
  </tr>

<tr>	
    <td  class="label2"  colspan="2">&nbsp;</td>
	<td  class="label2"  colspan="2">&nbsp;</td>
	<td  class="label2"  colspan="1">&nbsp;</td>
	<td  class="label2"  colspan="1">&nbsp;</td>
	<td  class="label2"  colspan="2">&nbsp;</td>
	<td  class="label2"  colspan="1">&nbsp;</td>
</tr>
 <tr>
 <td colspan="9" class="label4Bold" height="30" ><bean:message bundle="PVS" key="pvs.technician/engineers"/></td>
  </tr>
  
<tr>
	<td  class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.tech/engineername"/></td>
	<td  class="label2" height="20" colspan="5"> Kurt Astbury</td>
</tr>
<tr>
	<td  class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.resourcelevel"/></td>
	<td  class="label1" height="20" colspan="5">CFE2	</td>
</tr>
<tr>
	<td  class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.location"/></td>
	<td  class="label2" height="20" colspan="5">48473	</td>
</tr>
<tr>
	<td  class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.equippedwith"/></td>
	<td  class="label1" height="20" colspan="5"> Cell  </td>
</tr>
<tr>
	<td  class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.union/nonunion"/></td>
	<td  class="label2" height="20" colspan="5">Non-Union   </td>
</tr>
<tr>
	<td  class="label1Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.highestcriticalityavailable"/></td>
	<td  class="label1" height="20" colspan="5">2hr on-site PPS</td>
</tr>
<tr>
	<td  class="label2Bold" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.certificationsandskill"/></td>
	<td  class="label2" height="20" colspan="5"></td>
</tr>

  <tr> 
    <td colspan="9"class="label4Bold" height="30" ><bean:message bundle="PVS" key="pvs.toollist"/></td>
  </tr> 
  
<tr>
	<td  class="tryB" height="20" colspan="4">&nbsp;</td>
 	<td  class="tryB" height="20" colspan="5"><bean:message bundle="PVS" key="pvs.toolname"/></td>
</tr>

<tr>
	<td  class="label2" height="20" colspan="4"></td>
	<td  class="label2" height="20" colspan="5"></td>
</tr>
<tr>
	<td  class="label1Bold" height="20" colspan="4"></td>
	<td  class="label1" height="20" colspan="5"></td>
</tr>
<tr>
	<td  class="label2" height="20" colspan="4"></td>
	<td  class="label2" height="20" colspan="5"></td>
</tr>
<tr>
	<td  class="label1Bold" height="20" colspan="4"></td>
	<td  class="label1" height="20" colspan="5"></td>
</tr>

  <tr> 
    <td colspan="9"class="label4Bold" height="30"><bean:message bundle="PVS" key="pvs.environmentallycontrolled"/></td>
  </tr> 
  
<tr>
	<td  class="tryB" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.serviceareazipcode"/></td>
 	<td  class="tryB" height="20" colspan="5">Size</td>
</tr>

<tr>
	<td  class="label2" height="20" colspan="4"></td>
	<td  class="label2" height="20" colspan="5"> </td>
</tr>
<tr>
	<td  class="label1Bold" height="20" colspan="4"></td>
	<td  class="label1" height="20" colspan="5"></td>
</tr>
<tr>
	<td  class="label2" height="20" colspan="4"></td>
	<td  class="label2" height="20" colspan="5"></td>
</tr>
<tr>
	<td  class="label1" height="20" colspan="4"></td>
	<td  class="label1" height="20" colspan="5"></td>
</tr>

   <tr> 
    <td colspan="9"class="label4Bold" height="30" ><bean:message bundle="PVS" key="pvs.additionalinsured"/></td>
  </tr> 
  
<tr>
	<td  class="tryB" rowspan="2" height="20" colspan="2"><bean:message bundle="PVS" key="pvs.carrier"/></td>
 	<td  class="tryB" rowspan="2" height="20"><bean:message bundle="PVS" key="pvs.policynumber"/></td>
	<td  class="tryB" colspan="6"  height="20"><bean:message bundle="PVS" key="pvs.amountsfor"/></td>
</tr>
<tr> 
    <td class="tryB" colspan="2"> 
      <div align="center"><bean:message bundle="PVS" key="pvs.general"/></div>
    </td>
	<td class="tryB" colspan="2"> 
      <div align="center"><bean:message bundle="PVS" key="pvs.automobile"/></div>
    </td>
    <td class="tryB" colspan="2"> 
      <div align="center"><bean:message bundle="PVS" key="pvs.umbrella"/></div>
    </td>
</tr>
 <tr>
	<td  class="label2" height="20" colspan="2"></td>
	<td  class="label2" height="20" colspan="1"> </td>
	<td  class="label2" height="20" colspan="2"></td>
	<td  class="label2" height="20" colspan="2"> </td>
	<td  class="label2" height="20" colspan="2"></td>
	
</tr>
<tr>
	<td  class="label1" height="20" colspan="2"></td>
	<td  class="label1" height="20" colspan="1"></td>
    <td  class="label1" height="20" colspan="2"></td>
	<td  class="label1" height="20" colspan="2"> </td>
	<td  class="label1" height="20" colspan="2"></td>
</tr>
<tr>
	<td  class="label2" height="20" colspan="2"></td>
	<td  class="label2" height="20" colspan="1"></td>
	<td  class="label2" height="20" colspan="2"></td>
	<td  class="label2" height="20" colspan="2"> </td>
	<td  class="label2" height="20" colspan="2"></td>
</tr>
<tr>
	<td  class="label1" height="20" colspan="2"></td>
	<td  class="label1" height="20" colspan="1"></td>
    <td  class="label1" height="20" colspan="2"></td>
	<td  class="label1" height="20" colspan="2"> </td>
	<td  class="label1" height="20" colspan="2"></td>
</tr>
 
  <tr> 
    <td colspan="9"class="label4Bold" height="30" ><bean:message bundle="PVS" key="pvs.taxexempt"/></td>
  </tr> 
  
<tr>
	<td   class="tryB" height="20" colspan="1" width="50">&nbsp;</td>
 	<td   class="tryB" height="20" colspan="2" width="180"><bean:message bundle="PVS" key="pvs.taxid"/></td>
	<td   class="tryB" height="20" colspan="1" width="50">&nbsp;</td>
	<td   class="tryB" height="20" colspan="2" width="180"><bean:message bundle="PVS" key="pvs.taxid"/></td>
 	<td   class="tryB" height="20" colspan="1" width="50">&nbsp;</td>
	<td   class="tryB" height="20" colspan="2" width="180"><bean:message bundle="PVS" key="pvs.taxid"/></td>
</tr>

<tr>
	<td   class="label2" height="20" colspan="1">AK</td>
	<td   class="label2" height="20" colspan="2"> </td>
	<td   class="label2" height="20" colspan="1">KY</td>
	<td   class="label2" height="20" colspan="2"></td>
	<td   class="label2" height="20" colspan="1">NY</td>
	<td   class="label2" height="20" colspan="2"></td>
</tr>
<tr>
	<td  class="label1" height="20" colspan="1"></td>
	<td  class="label1" height="20" colspan="2"></td>
	<td  class="label1" height="20" colspan="1"></td>
    <td  class="label1" height="20" colspan="2"></td>
	<td  class="label1" height="20" colspan="1"></td>
	<td  class="label1" height="20" colspan="2"></td>
</tr>

  <tr> 
    <td colspan="9" class="label4Bold" height="30"><bean:message bundle="PVS" key="pvs.companylevelwireless"/></td>
  </tr> 
  
<tr>
	<td  class="tryB" height="20" colspan="1">&nbsp; </td>
 	<td  class="tryB" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.validfrom"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
	<td  class="tryB" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.through"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
</tr>

<tr>
	<td  class="label2Bold" height="20" colspan="1">Cisco</td>
	<td  class="label2" height="20" colspan="4">12/22/2004 </td>
	<td  class="label2" height="20" colspan="4">12/22/2005</td>
</tr>
<tr>
	<td  class="label1Bold" height="20" colspan="1">Symbol</td>
	<td  class="label1" height="20" colspan="4"></td>
	<td  class="label1" height="20" colspan="4"></td>
</tr>


  <tr> 
    <td colspan="9" class="label4Bold" height="30" ><bean:message bundle="PVS" key="pvs.companylevelstructured"/></td>
  </tr> 
  
<tr>
	<td  class="tryB" height="20"colspan="1">&nbsp; </td>
 	<td  class="tryB" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.validfrom"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
	<td  class="tryB" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.through"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
</tr>

<tr>
	<td  class="label2Bold" height="20" colspan="1">Ortronics </td>
	<td  class="label2" height="20" colspan="4">12/22/2004 </td>
	<td  class="label2" height="20" colspan="4">12/22/2005</td>
</tr>
<tr>
	<td  class="label1Bold" height="20" colspan="1">Panduit </td>
	<td  class="label1" height="20" colspan="4"></td>
	<td  class="label1" height="20" colspan="4"></td>
</tr>


  <tr> 
    <td colspan="9" class="label4Bold" height="30"><bean:message bundle="PVS" key="pvs.companylevelintegrated"/></td>
  </tr> 
  
<tr>
	<td   class="tryB" height="20" colspan="1">&nbsp; </td>
 	<td   class="tryB" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.validfrom"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
	<td   class="tryB" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.through"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
</tr>

<tr>
	<td   class="label2Bold" height="20"colspan="1">Ortronics </td>
	<td   class="label2" height="20"colspan="4">12/22/2004 </td>
	<td   class="label2" height="20"colspan="4">12/22/2005</td>
</tr>
<tr>
	<td  class="label1Bold" height="20"colspan="1">Lucent </td>
	<td   class="label1" height="20"colspan="4"></td>
	<td   class="label1" height="20"colspan="4"></td>
</tr>

<tr>
    <td colspan="9" class="label4Bold" height="30" ><bean:message bundle="PVS" key="pvs.companylevelindustry"/></td>
  </tr> 
  
<tr>
	<td  class="tryB" height="20" colspan="1">&nbsp; </td>
 	<td  class="tryB" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.validfrom"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
	<td  class="tryB" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.through"/><br><bean:message bundle="PVS" key="pvs.mm/dd/yyyy"/></td>
</tr>

<tr>
	<td  class="label2Bold" height="20" colspan="1">BICSI </td>
	<td  class="label2" height="20" colspan="4">12/22/2004 </td>
	<td  class="label2" height="20" colspan="4">12/22/2005</td>
</tr>
<tr>
	<td  class="label1Bold" height="20" colspan="1">PMI </td>
	<td  class="label1" height="20" colspan="4"></td>
	<td  class="label1" height="20" colspan="4"></td>
</tr>

  
<tr>
	<td  class="tryB" height="20" colspan="1">&nbsp; </td>
 	<td  class="tryB" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.afterhoursphone"/></td>
	<td  class="tryB" height="20" colspan="4"><bean:message bundle="PVS" key="pvs.afterhouremail"/></td>
</tr>

<tr>
	<td  class="label2" height="20"colspan="1">1.</td>
	<td  class="label2" height="20"colspan="4"> 810) 584-0055 </td>
	<td  class="label2" height="20"colspan="4">sales@yourit.ws </td>
</tr>
<tr>
	<td  class="label1" height="20"colspan="1">2.</td>
	<td  class="label1" height="20"colspan="4"> (810) 908-2965</td>
	<td  class="label1" height="20"colspan="4">kurtastbury@yahoo.com </td>
</tr>
<tr>
	<td  class="label2" height="20"colspan="1">3.</td>
	<td  class="label2" height="20"colspan="4"> </td>
	<td  class="label2" height="20"colspan="4"></td>
</tr>
<tr>
	<td  class="label1" height="20"colspan="1">4.</td>
	<td  class="label1" height="20"colspan="4"></td>
	<td  class="label1" height="20"colspan="4"></td>
</tr>

 <tr> 
    <td colspan="9" class="label4Bold" height="30" ><bean:message bundle="PVS" key="pvs.quantity"/></td>
  </tr>
  
<tr>
	<td  class="label2Bold" height="20" colspan="4">Quantity</td>
	<td   class="label2" height="20" colspan="5">5</td>
</tr>

  <tr> 
    <td colspan="9"class="label4Bold" height="30" ><bean:message bundle="PVS" key="pvs.adminsideitems"/></td>
  </tr>
   <tr> 
    <td colspan="9" class="label4Bold" height="30" ><bean:message bundle="PVS" key="pvs.mcsainformation"/></td>
  </tr>
  


<tr>
	<td   class="label2Bold" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.companyname"/></td>
	<td   class="label2" height="20"colspan="5">Your I.T.</td>
</tr>
<tr>
	<td  class="label1Bold" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.authorizedrepresentstive"/></td>
	<td   class="label1" height="20"colspan="5">Kurt Astbury</td>
</tr>
<tr>
	<td   class="label2Bold" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.title"/></td>
	<td   class="label2" height="20"colspan="5">Vice President</td>
</tr>
<tr>
	<td   class="label1Bold" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.ssn"/></td>
	<td   class="label1" height="20"colspan="5">3833 </td>
</tr>
<tr>
	<td  class="label2Bold" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.birthdate"/></td>
	<td   class="label2" height="20"colspan="5">1982-06-27 </td>
</tr>
<tr>
	<td   class="label1Bold" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.mothersname"/></td>
	<td   class="label1" height="20"colspan="5"></td>
</tr>
<tr>
	<td   class="label2Bold" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.mcsaversion"/>MCSA Version:</td>
	<td   class="label2" height="20"colspan="5">4.0</td>
</tr>
<tr>
	<td   class="label1Bold" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.mcsa"/><bean:message bundle="PVS" key="pvs.partnerlevel"/></td>
	<td   class="label1" height="20"colspan="5"></td>
</tr>
<tr>
	<td   class="label2Bold" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.certifiedpartner"/></td>
	<td   class="label2" height="20"colspan="5"></td>
</tr>
<tr>
	<td   class="label1Bold" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.projectcodename"/></td>
	<td   class="label1" height="20"colspan="5"></td>
</tr>
<tr>
	<td   class="label2Bold" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.trucklogos"/></td>
	<td   class="label2" height="20"colspan="5"></td>
</tr>
<tr>
	<td   class="label1Bold" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.quantity"/></td>
	<td   class="label1" height="20"colspan="5"></td>
</tr>
<tr>
	<td   class="label2Bold" height="20"colspan="4"><bean:message bundle="PVS" key="pvs.badges"/></td>
	<td   class="label2" height="20"colspan="5"></td>
</tr>

  <tr> 
    <td colspan="9"class="label4Bold" height="30" ><bean:message bundle="PVS" key="pvs.uploadedfiles"/></td>
  </tr> 
  
<tr>
	<td   class="tryB" height="20" colspan="2"><bean:message bundle="PVS" key="pvs.dateposted"/></td>
 	<td   class="tryB" height="20" colspan="2"><bean:message bundle="PVS" key="pvs.displayname"/></td>
	<td   class="tryB" height="20" colspan="1"><bean:message bundle="PVS" key="pvs.ext."/></td>
	<td   class="tryB" height="20" colspan="2"><bean:message bundle="PVS" key="pvs.originalname"/></td>
	<td   class="tryB" height="20" colspan="2"><bean:message bundle="PVS" key="pvs.download"/></td>
  </tr>
  <tr>
	<td  class="label2" height="20" colspan="2"></td>
	<td  class="label2" height="20" colspan="2"></td>
	<td  class="label2" height="20" colspan="1"></td>
	<td  class="label2" height="20" colspan="2"></td>
	<td  class="label2" height="20" colspan="2"></td>
  </tr>
 <tr  align="right"> 
    <td><br></td>
 </tr>
 <tr align="right"> 
    <td colspan="30" height="37"><br>
      <a href="#" class="pagelink">Logout </a>|<a href="#" class="pagelink"> Help</a> 
    </td>
  </tr>
</table>
 </td>
 </tr> 
 </table>

</BODY>
</html:html>
