<!-- ajsdl;kfjadsf
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For displaying  Appendix detail.
*
-->


<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<bean:define id = "status" name = "AppendixDetailForm" property = "status" />
<bean:define id = "latestaddendumid" name = "latestaddendumid" scope = "request" />

<%
	String MSA_Id = "";
	String Appendix_Id ="";
	int addRow = -1;
	boolean addSpace = false;
	int rowHeight = 65;	
	int ownerListSize = 0;
	String appendixType = "";
	int j=2;
	String checkowner=null;

	if(request.getAttribute("ownerListSize") != null)
		ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());
	
	rowHeight += ownerListSize*18;

	if( request.getParameter( "MSA_Id" ) != null )
		MSA_Id = request.getParameter( "MSA_Id" );

	else if( request.getAttribute( "MSA_Id" ) != null)
		MSA_Id = (String) request.getAttribute( "MSA_Id" );

	if( request.getParameter( "Appendix_Id" ) != null )
		Appendix_Id = request.getParameter( "Appendix_Id" );

	else if( request.getAttribute( "Appendix_Id" ) != null)
		Appendix_Id = ( String ) request.getAttribute( "Appendix_Id" );

	if( request.getAttribute( "appendixType" ) != null)
		appendixType = (String) request.getAttribute( "appendixType" );
%>
<html:html>
<head>
	<title><bean:message bundle = "pm" key = "appendix.detail.title"/></title>
	<%@ include file = "/Header.inc" %>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>    
  	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
</head>

<%@ include file = "/NMenu.inc" %>


<script>

var str = '';

function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function addcomment()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewcomments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function editappendix()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function view( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}

function viewaddendumpdf( v )
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewAction.do?jobId="+v+"&View=pdf&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
}


function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "appendix.detail.deleteappendixprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>";
			document.forms[0].submit();
			return true;	
		}
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=Appendix&Appendix_Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
	 
}


function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromAppendix&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewappendixdocument&Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Appendix_Id=<%=Appendix_Id%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}
function setendcustomer()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddEndCust.do?typeid=<%=Appendix_Id%>&type=Appendix&viewjobtype=ION&Type=Appendix";
	document.forms[0].submit();
	return true;	
}

function addcontact()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "AddContact.do?function=add&typeid=<%=Appendix_Id%>&type=P&viewjobtype=ION&ownerId=<%=session.getAttribute( "userid" )%>&Type=Appendix";
	document.forms[0].submit();
	return true;	
}


</script>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif');leftAdjLayers();OnLoad();">
<html:form action = "/AppendixDetailAction">
<html:hidden property="appendixOtherCheck"/> 
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>
<html:hidden property="msa_id"/>

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
 <tr>
    <td valign="top" width = "100%">
      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
        <tr>
          <!-- Sub Navigation goes into this td -->
          
						<% if( status.equals( "Signed" ) || status.equals( "Transmitted" ) )
						{
							if( status.equals( "Signed" ) )
							{ %>
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noeditappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
							<%}
							else
							{%>
								<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
							<%}
						}
						else
						{%>	
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center" ><a href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.menu.edit"/></center></a></td>
						<%} %>	
						
							<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
							<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>	
          	<!-- End of Sub Navigation -->
        </tr>
         <tr>
          <!-- BreadCrumb goes into this td -->
          <td background="images/content_head_04.jpg" height="21" colspan="23" width ="100%">
          				<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "AppendixDetailForm" property = "msa_id"/>"><bean:write name = "AppendixDetailForm" property = "msaname"/></a></div>
          </td>
        </tr>
   		  <tr>    
						<td colspan = "5" height = "30" width = "800"><h2>
						 	<bean:message bundle = "pm" key = "appendix.detail.new.label"/>
							<bean:write name = "AppendixDetailForm" property = "name"/>
						</h2></td>
					</tr>
      </table></td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
    <td valign="top" width="155">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td width="155" height="39"  class="headerrow" conspan="2"><table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td width="150"><table width="100%" cellpadding="0" cellspacing="3" border="0">
                    <tr>
                      <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="21" background="images/content_head_04.jpg" width="140">
          	<table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td nowrap="nowrap" colspan="2">
	                <div id="featured1">
	                			<div id="featured">
										<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
								</div>								
	                </div>		
				<span>
				<div id="filterAppendix" class="divstyle">
        			<table width="250" cellpadding="0" class="divtable">
                    <tr>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
							    		<logic:present name ="statuslist" scope="request">
									    	<logic:iterate id = "list" name = "statuslist"> 
										 		<html:multibox property = "appendixSelectedStatus"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											</logic:iterate>
										</logic:present>
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="AppendixDetailForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="AppendixDetailForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="AppendixDetailForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
	    
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					    		<logic:present name ="ownerlist" scope="request">
							    	<logic:iterate id = "olist" name = "ownerlist"> 
										<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
										<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
							    		<%
										checkowner = "javascript: checking('"+ownerId+"');"; 
										if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
											addRow++;
											addSpace = true;
										}	
											
							    		if(addRow == 0) { %> 
								    		<br/>
											&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
											<br/>								
										<% } %>
							    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 							    	
							    	
								 		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
									  <bean:write name ="olist" property = "ownerName"/>
									  <br/> 
									</logic:iterate>
								</logic:present>
						    </td>
                          </tr>
                         
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="34" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" width ="2" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="8" /></td>
                              </tr>
                              
                            </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
</table>




<SCRIPT language = JavaScript1.2>
if (isMenu) {
arMenu1 = new Array()
arMenu2 = new Array(
130,
findPosX( 'pop2' ),findPosY( 'pop2' ),
"","",
"","",
"","",
<%if( ( String )request.getAttribute( "list" ) != "" ) {%>
"<bean:message bundle = "pm" key = "appendix.detail.menu.changestatus.new"/>","#",1,
<%} else{%>
"<bean:message bundle = "pm" key = "appendix.detail.menu.changestatus.new"/>","#",0,
<%}%>
"<bean:message bundle = "pm" key = "appendix.detail.viewappendix"/>","#",1,
"<bean:message bundle = "pm" key = "appendix.detail.menu.sendappendix"/>","javascript:sendemail();",0,

<%if( status.equals( "Signed" ) || status.equals( "Inwork" ))
{
	if( status.equals( "Signed" ) )
	{ %>
		"<bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/>","javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.formaldocdate.noprivilegestatus"/>')",0,
		"<bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/>","javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.endcustomer.noprivilegestatus"/>')",0,
	<%}
	else {%>
		"<bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/>","javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.formaldocdate.noprivilegeinworkstatus"/>')",0,
		"<bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/>","javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.endcustomer.noprivilegeinworkstatus"/>')",0,
	<%}
}
else
{%>	
"<bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/>","javascript:setformaldocdate();",0,
"<bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/>","javascript:setendcustomer();",0,
<%}%>
"<bean:message bundle = "pm" key = "appendix.detail.menu.projectmanager"/>","javascript:addcontact();",0,
"<bean:message bundle = "pm" key = "appendix.detail.comments"/>","#",1,
<% if( status.equals( "Signed" ) || status.equals( "Transmitted" ) )
{
	if( status.equals( "Signed" ) )
	{ %>
		"<bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/>","javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noeditappendix"/>')",0,
	<%}
	else
	{%>
		"<bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/>","javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')",0,
	<%}
}
else
{%>	
"<bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/>","javascript:editappendix();",0,
<%} %>
"<bean:message bundle = "pm" key = "appendix.detail.menu.documents"/>","",1,

"<bean:message bundle = "pm" key = "appendix.detail.menu.deleteappendix"/>","javascript:del();",0
)

<% if( ( String )request.getAttribute( "list" ) != "" ) {%>
arMenu2_1=new Array(
<%= ( String ) request.getAttribute( "list" ) %>
)
<%}%>

arMenu2_2=new Array(
"Details","AppendixDetailAction.do?MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>",0,
"<bean:message bundle = "pm" key = "appendix.detail.menu.viewpdf"/>","javascript:view('pdf');",0,
"<bean:message bundle = "pm" key = "appendix.detail.menu.viewrtf"/>","javascript:view('rtf');",0,
<% if( request.getAttribute( "addendumlist" ) != "" ){%>
"<bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/>","javascript:view('html');",0,
<% if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) { %>
"History","",1,
"<bean:message bundle = "pm" key = "appendix.detail.menu.viewexcel"/>","javascript:view('excel');",0
<%}else {%>
"History","",1
<%}
}else{%>
<% if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) { %>
"<bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/>","javascript:view('html');",0,
"<bean:message bundle = "pm" key = "appendix.detail.menu.viewexcel"/>","javascript:view('excel');",0
<%}else {%>
"<bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/>","javascript:view('html');",0
<%}
}%>
)

<% if( ( String )request.getAttribute( "addendumlist" ) != "" ) {%>
arMenu2_2_5=new Array(
<%= ( String ) request.getAttribute( "addendumlist" ) %>
)
<%} %>

	arMenu2_7 = new Array(
	"<bean:message bundle = "pm" key = "appendix.detail.menu.viewall"/>","javascript:viewcomments();",0,
	"<bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/>","javascript:addcomment();",0
	)
	arMenu2_9 = new Array(
	"<bean:message bundle = "pm" key = "appendix.detail.menu.view"/>","javascript:viewdocuments();",0,
	"<bean:message bundle = "pm" key = "appendix.detail.menu.upload"/>","javascript:upload();",0
	)

document.write("<SCRIPT LANGUAGE = 'JavaScript1.2' SRC = 'javascript/hierMenus.js'><\/SCRIPT>");
}
</script>
	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>
  			 <td width="2" height="0"></td>
  			 <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width = "800"> 
							
							<logic:present name = "clickShow" scope = "request">
						  		<logic:notEqual name = "clickShow" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
							
							<logic:present name = "addcommentflag" scope = "request">
							<tr> 
							<td colspan = "4" class = "message" height = "30">
						    	<logic:equal name = "addcommentflag" value = "0">
									<bean:message bundle = "pm" key = "appendix.detail.addcomment.success"/>	
					    		</logic:equal>
		    		
					    		<logic:notEqual name = "addcommentflag" value = "0">
									<bean:message bundle = "pm" key = "appendix.detail.addcomment.failure"/>
					    		</logic:notEqual>
					    		</td>
					    		</tr>
		    				</logic:present>
    				
		    				<logic:present name = "changestatusflag" scope = "request">
		    				<tr> 
							<td colspan = "4" class = "messagewithwrap" height = "30">
						    	<logic:equal name = "changestatusflag" value = "0">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.success"/>	
					    		</logic:equal>
		    		
					    		<logic:equal name = "changestatusflag" value = "-9001">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.failure1"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "-9002">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.failure2"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "-9003">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.failure3"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "-9004">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.failure4"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "-9005">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.failure5"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "-9006">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.failure6"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "-9008">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.failure8"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "-9012">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.mysql.failure1"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "-9013">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.mysql.failure1"/>	
					    		</logic:equal>
					    		
					    		<logic:equal name = "changestatusflag" value = "9600">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.mysql.failure1"/>	
					    		</logic:equal>
					    		<logic:equal name = "changestatusflag" value = "96001">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.mysql.failure10"/>	
					    		</logic:equal>
					    		<logic:equal name = "changestatusflag" value = "96002">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.mysql.failure11"/>	
					    		</logic:equal>
					    		<logic:equal name = "changestatusflag" value = "9614">
									<bean:message bundle = "pm" key = "appendix.detail.changestatus.mysql.failure12"/>	
					    		</logic:equal>
					    		
					    		</td>
					    		</tr>
		    				</logic:present>
		    				
		    				<logic:present name = "uploadflag" scope = "request">
			    				<tr> 
									<td colspan = "4" class = "message" height = "30">
								    	<logic:equal name = "uploadflag" value = "0">
											<bean:message bundle = "pm" key = "appendix.detail.upload.success"/>
							    		</logic:equal>
		    		
							    		<logic:notEqual name = "uploadflag" value = "0">
											<bean:message bundle = "pm" key = "appendix.detail.upload.failure"/>
							    		</logic:notEqual>
							    	</td>
						    	</tr>
		    				</logic:present>
		    				
		    				<logic:present name = "emailflag" scope = "request">
			    				<tr> 
									<td colspan = "4" class = "message" height = "30">
								    	<logic:equal name = "emailflag" value = "0">
											<logic:equal name = "emailchangestatusflag" value = "0">
												<bean:message bundle = "pm" key = "appendix.detail.mailsent"/>	
											</logic:equal>
											
											<logic:equal name = "emailchangestatusflag" value = "-9001">
												<bean:message bundle = "pm" key = "appendix.detail.changestatus.failure1"/>	
											</logic:equal>
							    		</logic:equal>
		    		
							    		<logic:notEqual name = "emailflag" value = "0">
											Mail Sent Failure Status cannot be changed to transmitted
							    		</logic:notEqual>
							    	</td>
						    	</tr>
		    				</logic:present>
		    				<logic:present name = "editAppendixFlag" scope = "request">	
	  <tr> 
		<td colspan = "4" class = "message" height = "30">
	     	<logic:equal name = "editAppendixFlag" value = "0">
	     		<logic:equal name = "editAction" value = "A">
			 		<script>
						parent.ilexleft.location.reload();
					</script>
			 		Appendix added successfully
			 	</logic:equal>
			
			 	<logic:notEqual name = "editAction" value = "A">
			     	Appendix updated successfully
			 	</logic:notEqual>
		 	</logic:equal>
		
		 	<logic:notEqual name = "editAppendixFlag" value = "0">
			 	<logic:equal name = "editAction" value = "A">
			 		Appendix add failure
			 	</logic:equal>
			
			 	<logic:notEqual name = "editAction" value = "A">
			     	Appendix update failure
			 	</logic:notEqual>
			</logic:notEqual>
	    </td>
	 </tr>
	</logic:present> 
    
	<logic:present name = "changestatusflag" scope = "request">
	<logic:notEqual name = "changestatusflag" value = "-9008">
	    					<script>
								parent.ilexleft.location.reload();
							</script>
	</logic:notEqual>
    </logic:present>
	<logic:present name = "deleteflag" scope = "request">
						<tr> 
								<td colspan = "4" class = "message" height = "30">
		    						<logic:equal name = "deleteflag" value = "0">
		    							<bean:message bundle = "pm" key = "appendix.tabular.delete.success"/>
				    					<script>
											parent.ilexleft.location.reload();
										</script
		    						</logic:equal>
		    						
			   						<logic:equal name = "deleteflag" value = "-9001">
		    							<bean:message bundle = "pm" key = "appendix.tabular.delete.failure1"/>
		    						</logic:equal>
		    						
		    						<logic:equal name = "deleteflag" value = "-9002">
		    							<bean:message bundle = "pm" key = "appendix.tabular.delete.failure2"/>
		    						</logic:equal>
		    					</td>
    					</tr>
    </logic:present>
    			
	<logic:present name = "addflag" scope = "request">
		    			<tr> 
									<td colspan = "4" class = "message" height = "30">
				    				<logic:equal name = "addflag" value = "0">
		    							<bean:message bundle = "pm" key = "appendix.tabular.add.success"/>
				    					<script>
											parent.ilexleft.location.reload();
										</script>
		    						</logic:equal>
		    			
		    						<logic:equal name = "addflag" value = "-9001">
		    							<bean:message bundle = "pm" key = "appendix.tabular.add.failure"/>
		    						</logic:equal>
		    						
		    						<!-- Add message for adding same addpendix failure  -->
		    						<logic:equal name = "addflag" value = "-90001">
		    							<bean:message bundle = "pm" key = "appendix.tabular.addsameappendix.failure"/>
		    						</logic:equal>
		    				
		    						<logic:equal name = "addflag" value = "-9002">
		    							<bean:message bundle = "pm" key = "appendix.tabular.add.failure"/>
		    						</logic:equal>
		    						
		    						<logic:equal name = "addflag" value = "-9020">
		    							<bean:message bundle = "pm" key = "appendix.tabular.addnetmedx.failure"/>
		    						</logic:equal>
		    						
		    						</td>
    						</tr>
    </logic:present>
    			
	<logic:present name = "updateflag" scope = "request">
		    			<tr> 
									<td colspan = "4" class = "message" height = "30">
					    			<logic:equal name = "updateflag" value = "0">
										<bean:message bundle = "pm" key = "appendix.tabular.update.success"/>
				    				</logic:equal>
				    		
				    				<logic:notEqual name = "updateflag" value = "0">
										<bean:message bundle = "pm" key = "appendix.tabular.update.failure"/>
				    				</logic:notEqual>
				    				
				    				<logic:equal name = "updateflag" value = "-9021">
		    							<bean:message bundle = "pm" key = "appendix.tabular.updatenetmedx.failure"/>
		    						</logic:equal>
		    						
				    				</td>
		    				</tr>
	</logic:present>
  					<!-- general:start -->
  					<table width ="440">
  					<tr> 
    					<td colspan = "2" class = "formCaption"><bean:message bundle = "pm" key = "appendix.detail.general"/></td>
    				</tr>
    				<tr>
    					<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.type"/></td>
    					<td class = "colLight" ><bean:write name = "AppendixDetailForm" property = "appendixtype"/></td>
    				</tr>
    				<tr>
    					<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.msp"/></td>
    					<td class = "colLight" ><bean:write name = "AppendixDetailForm" property = "msp"/></td>
    				 
    				</tr> 
    				
    				<tr>
    					<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.customerdivision"/></td>
					    <td class = "colLight"><bean:write name = "AppendixDetailForm" property = "customerdivision"/></td>
    				</tr>
    				<tr>
    					<td class = "colDark" ><bean:message bundle = "pm" key = "appendix.detail.numbersites"/></td>
    					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "siteno"/></td>
    				</tr>
    				<tr> 
    					<td class = "colDark" ><bean:message bundle = "pm" key = "appendix.detail.status"/></td>
    					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "status"/></td>
					</tr>
					
					<tr> 
    					<td class = "colDark" ><bean:message bundle = "pm" key = "appendix.detail.unionupliftfactor"/></td>
    					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "unionupliftfactor"/></td>
					</tr>
					
					<tr> 
    					<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.lastmodified"/></td>
    					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "lastappendixmodifiedby" /> <bean:write name = "AppendixDetailForm" property = "lastappendixmodifieddate" /> </td>
					</tr>
    				<!-- general:End -->
    				
    				<!-- ScheduleInformation:start -->
    				<tr>
						<td colspan = "2"class = "formCaption"><bean:message bundle = "pm" key = "appendix.detail.ScheduleInformation"/></td>
					</tr> 
					<tr> 
						<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.PlannedScheduleRequired"/></td>
    					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "reqplasch"/></td>
  					</tr>
  					<tr> 
					    
						<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.PlannedScheduleEffective"/></td>
					    <td class = "colLight"><bean:write name = "AppendixDetailForm" property = "effplasch"/></td>
  					</tr>
  					<tr> 
						<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.PlannedScheduleDays"/></td>
    					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "schdays"/></td>
					</tr>
					<!-- ScheduleInformation:End -->
					
  					<!-- reviews:start -->
  					<tr> 
    					<td colspan = "2" class = "formCaption" height = "30"><bean:message bundle = "pm" key = "appendix.detail.reviews"/></td>
    				</tr>
    				<tr>
    					<td class = "colDark" height = "20"><bean:message bundle = "pm" key = "appendix.detail.draftreview"/></td>
    					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "draftreviews"/></td>
    				</tr>
    				<!--  
    				<tr>
    					<td class = "colDark" height = "20"><bean:message bundle = "pm" key = "appendix.detail.businessreview"/></td>
    					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "businessreviews"/></td>
    				</tr>
    				-->
    				<!-- reviews:End -->
    				
    				<!-- costprice:start -->
    				<tr>
						<td colspan = "2" class = "formCaption" height = "30"><bean:message bundle = "pm" key = "appendix.detail.costprice"/></td>
  					</tr>
  					<tr> 
						<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.estimatedcost"/></td>
    					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "estimatedcost"/></td>
  					</tr>
  					<tr> 
						<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.extendedprice"/></td>
    					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "extendedprice"/></td>
					</tr>
					<tr> 
						<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.proformamargin"/></td>
    					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "proformamargin"/></td>
					</tr>
  					<!-- costprice:End -->
  					
  					<!-- contact:start -->
  					<tr> 
						<td colspan = "2" class = "formCaption" height = "30" ><bean:message bundle = "pm" key = "appendix.detail.contacts"/></td>
					</tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.cnsPOC"/></td>
						<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "cnsPOC"/></td>
					<tr>
					<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.customerPOCbusiness"/></td>
					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "custPocbusiness"/></td>
					</tr>
					<tr>
					<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.customerPOCcontract"/></td>
					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "custPoccontract"/></td>
					</tr>
					<tr>
					<td class = "colDark"><bean:message bundle = "pm" key = "appendix.detail.customerPOCbilling"/></td>
					<td class = "colLight"><bean:write name = "AppendixDetailForm" property = "custPocbilling"/></td>
					</tr>
					<!-- contact:End -->
					   
					<!-- comment:Start -->
					<tr> 
						<td colspan = "2" class = "formCaption" height = "30"><bean:message bundle = "pm" key = "appendix.detail.Comments"/></b></td>
					</tr> 
					
					
					 <logic:notEqual name = "AppendixDetailForm" property = "lastcomment" value = "">
					<tr> 
					    <td class = "colDark">
					    	<bean:write name = "AppendixDetailForm" property = "lastchangedate" /></td>
					    <td class = "colLight">
					    	<bean:write name = "AppendixDetailForm" property = "lastcomment"/>
					    </td>
				    </tr>
				   </logic:notEqual>
				    <logic:equal name = "AppendixDetailForm" property = "lastcomment" value = "">
					<tr> 
					    <td class = "colLight" colspan="2"><font class="Nmessage">&nbsp;&nbsp;No Comments.</font>
					    </td>
				    </tr>
				   </logic:equal>
					<!-- comment:End -->
					
				    <!-- Uploads Files:start -->
				    <logic:equal name = "AppendixDetailForm" property = "file_upload_check" value = "U">
					<tr>
						<td class = "formCaption" colspan="2">
							<bean:message bundle = "pm" key = "common.fileupload.history"/>
						</td>
					</tr>
				<logic:iterate id = "ms" name = "uploadeddocs" scope = "request">
					<tr> 
						<td class = "colDark"><bean:message bundle = "pm" key = "common.fileupload.uploaddate"/></td>
						<td class = "colLight">
									<bean:write name = "ms" property = "file_uploaded_date"/>		
								</td>
					</tr>
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "common.fileupload.filename"/></td>
						<td class = "colLight"> 
									<a href = "ViewuploadedocumentAction.do?ref=downloaddocument&file_id=<bean:write name = "ms" property = "file_id" />"><bean:write name = "ms" property = "file_name" /></a>
								</td>
					</tr>
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "common.fileupload.uploadremarks"/></td>
						<td class = "colLight">
									<bean:write name = "ms" property = "file_remarks"  />
								</td>	
					</tr>
					<tr>
						<td class = "colDark"><bean:message bundle = "pm" key = "common.fileupload.uploadby"/></td>
						
								<td class = "colLight">
									<bean:write name = "ms" property = "file_uploaded_by"/>		
								</td>
					</tr>				  
						
	   			</logic:iterate>	
		  		</logic:equal>
				    <!-- Uploads Files:End -->
				    
			</table>
					<jsp:include page = '/Footer.jsp'>
		      			<jsp:param name = 'colspan' value = '37'/>
		      			<jsp:param name = 'helpid' value = 'appendixdetailpage'/>
		 			</jsp:include>
		 			<!--  </table> -->
			</td>
		</tr>
	</table>	
</html:form>
</body>

<script>
function initialState()
{
	document.forms[0].go.value="";
	document.forms[0].action="AppendixDetailAction.do?MSA_Id=<bean:write name ='AppendixDetailForm' property ='msa_id'/>&Appendix_Id=<bean:write name ='AppendixDetailForm' property ='appendix_Id'/>&firstCall=true&home=home&clickShow=true";
	document.forms[0].submit();
	return true;
}



function month_Appendix()
{
document.forms[0].go.value="true";
document.forms[0].action="AppendixDetailAction.do?MSA_Id=<bean:write name ='AppendixDetailForm' property ='msa_id'/>&Appendix_Id=<bean:write name ='AppendixDetailForm' property ='appendix_Id'/>&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_Appendix()
{
//alert('in month_Appendix()');
document.forms[0].go.value="true";
document.forms[0].action="AppendixDetailAction.do?MSA_Id=<bean:write name ='AppendixDetailForm' property ='msa_id'/>&Appendix_Id=<bean:write name ='AppendixDetailForm' property ='appendix_Id'/>&week=0&clickShow=true";
document.forms[0].submit();
return true;
}

function ShowDiv()
{
leftAdjLayers();
document.getElementById("filterAppendix").style.visibility="visible";
return false;
}

function hideDiv()
{
//alert('in month_Appendix()');
document.getElementById("filterAppendix").style.visibility="hidden";

return false;
}
function checking(checkboxvalue)
{
if(document.forms[0].appendixOtherCheck.value == 'otherOwnerSelected')
	{
		if(checkboxvalue == '%'){
			for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
			{
				document.forms[0].appendixSelectedOwners[j].checked=false;
			}
		}
		else
		{
			for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
			{
				if(document.forms[0].appendixSelectedOwners[j].checked) {
				 	document.forms[0].appendixSelectedOwners[1].checked = false;
				 	break;
				}
			}		
		}		
	}
	else
	{

	for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
	{
			if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
			{
				document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
				document.forms[0].action="AppendixDetailAction.do?MSA_Id=<bean:write name ='AppendixDetailForm' property ='msa_id'/>&Appendix_Id=<bean:write name ='AppendixDetailForm' property ='appendix_Id'/>&opendiv=0";
 				document.forms[0].submit();

			}
	}
	}
}

function OnLoad()
{
<%if(request.getAttribute("opendiv") != null){%>
document.getElementById("filterAppendix").style.visibility="visible";
<%}%>
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 260;
		document.getElementById("filterAppendix").style.left=leftAdj;
		
}

function show()
{
	document.forms[0].go.value="true";
	document.forms[0].action="AppendixDetailAction.do?MSA_Id=<bean:write name ='AppendixDetailForm' property ='msa_id'/>&Appendix_Id=<bean:write name ='AppendixDetailForm' property ='appendix_Id'/>&clickShow=true";
	document.forms[0].submit();
	return true;
}

</script>
</html:html>


             