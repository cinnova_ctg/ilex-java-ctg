<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For creating and  updating  MSAs.
*
-->
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<bean:define id = "codes" name = "codeslist" scope = "request"/>
<% 
	int addRow = -1;
	boolean addSpace = false;
	int rowHeight = 65;
	int j=2;
	String checkowner = null;
	String viewSelectorMessage ="";
	if(request.getAttribute("viewSelectorMessage") != null)
		viewSelectorMessage = request.getAttribute("viewSelectorMessage").toString();

	int msa_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString() ); 
	int ownerListSize = 0;
	if(request.getAttribute("ownerListSize") != null)
		ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());
	
	
	int appendix_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString()); 
	String MSA_Id = "";
	if( request.getAttribute( "MSA_Id" )!= null ) {
	MSA_Id = ( String ) request.getAttribute( "MSA_Id" );
	request.setAttribute( "MSA_Id" , MSA_Id );

	rowHeight += ownerListSize*18;
} %><head>



	<title><bean:message bundle = "pm" key = "appendix.detail.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css"> 
	<script language = "JavaScript" src = "javascript/functions.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>  
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	
<script>





function initialState()
{
	document.forms[0].go.value="";
	document.forms[0].action="AppendixUpdateAction.do?firstCall=true&home=home&clickShow=true";
	document.forms[0].submit();
	return true;
}	
</script>	
</head>
<html:html>

<%
	String backgroundclass = null;
	String backgroundhyperlinkclass = null;
	String backgroundclasstoleftalign=null;
	boolean csschooser = true;
	
	int i = 0;
%>

<body class="body" onLoad="leftAdjLayers();OnLoad();">
<html:form action = "/AppendixUpdateAction">
<html:hidden property="appendixOtherCheck"/> 
<html:hidden property="prevSelectedStatus"/> 
<html:hidden property="prevSelectedOwner"/>
<html:hidden property="go"/>
<html:hidden property="MSA_Id"/>

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

 <tr>
    <td valign="top" width = "100%">
      <table cellpadding="0" cellspacing="0" border="0" width = "100%">
        <tr>
          <!-- Sub Navigation goes into this td -->
          	<td  id="pop1" width="110" class="headerrow" height="19" width = "100%">&nbsp;</td>
          <!-- End of Sub Navigation -->
        </tr>
         <tr>
          <!-- BreadCrumb goes into this td -->
          <td background="images/content_head_04.jpg" height="21" colspan="23" width ="100%">
          				<div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
          </td>
        </tr>
   		  <tr>
          	 <td colspan="23"><h2><a class="head" href="MSADetailAction.do?MSA_Id=<bean:write name="AppendixListForm" property="MSA_Id"/>&Prj_id=<bean:write name="AppendixListForm" property="MSA_Id"/>"><bean:write name = "msaname" scope = "request"/></a>:&nbsp;<bean:message bundle = "pm" key ="appendix.tabular.new.label"/>
 				</h2></td>  
	     </tr>
      </table></td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
    <td valign="top" width="155">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td width="155" height="39"  class="headerrow" conspan="2"><table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td width="150"><table width="100%" cellpadding="0" cellspacing="3" border="0">
                    <tr>
                      <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="21" background="images/content_head_04.jpg" width="140">
          	<table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td nowrap="nowrap" colspan="2">
	                <div id="featured1">
	                			<div id="featured">
										<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
								</div>								
	                </div>		
				<span>
				<div id="filterAppendix" class="divstyle">
        			<table width="250" cellpadding="0" class="divtable">
                    <tr>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
							    		<logic:present name ="statuslist" scope="request">
									    	<logic:iterate id = "list" name = "statuslist"> 
										 		<html:multibox property = "appendixSelectedStatus"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											</logic:iterate>
										</logic:present>
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="AppendixListForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="AppendixListForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="AppendixListForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
	    
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					    		<logic:present name ="ownerlist" scope="request">
							    	<logic:iterate id = "olist" name = "ownerlist"> 
										<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
										<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
							    		<% 
										checkowner = "javascript: checking('"+ownerId+"');";
										if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
											addRow++;
											addSpace = true;
										}	
											
							    		if(addRow == 0) { %> 
								    		<br/>
											&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
											<br/>								
										<% } %>
							    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 							    	
							    	
								 		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
									  <bean:write name ="olist" property = "ownerName"/>
									  <br/> 
									</logic:iterate>
								</logic:present>
						    </td>
                          </tr>
                         
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="34" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" width ="2" /></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br>
                                  </a></td>
                              </tr>
                              <tr>
                                <td colspan="3"><img src="images/spacer.gif" height="8" /></td>
                              </tr>
                              
                            </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
 <table  width = "90%" border = "0" cellspacing = "0" cellpadding = "0">
 		<logic:present name = "clickShow" scope = "request">
	  		<logic:notEqual name = "clickShow" value = "">
				<script>
					parent.ilexleft.location.reload();
				</script>	
			</logic:notEqual>
		</logic:present>
 <tr>
  <td colspan="3">
  <table border = "0" cellspacing = "1" cellpadding = "1" >
	 <logic:present name = "editAppendixFlag" scope = "request">	
	   <tr><td>&nbsp;</td>
	  	 <td colspan = "2" class = "message" height = "30">
	     	<logic:equal name = "editAppendixFlag" value = "0">
	     		<logic:equal name = "editAction" value = "A">
			 		<script>
						parent.ilexleft.location.reload();
					</script>
			 		Appendix added successfully
			 	</logic:equal>
			
			 	<logic:notEqual name = "editAction" value = "A">
			     	Appendix updated successfully
			 	</logic:notEqual>
		 	</logic:equal>
		
		 	<logic:notEqual name = "editAppendixFlag" value = "0">
			 	<logic:equal name = "editAction" value = "A">
			 		Appendix add failure
			 	</logic:equal>
			
			 	<logic:notEqual name = "editAction" value = "A">
			     	Appendix update failure
			 	</logic:notEqual>
			</logic:notEqual>
	    </td>
	 </tr>
	</logic:present> 
    
	<logic:present name = "changestatusflag" scope = "request">
	    					<script>
								parent.ilexleft.location.reload();
							</script
    </logic:present>
	<logic:present name = "deleteflag" scope = "request">
						<tr> 
			    				<td>&nbsp;</td>
			    				<td colspan = "5" class = "message" height = "30">
		    						<logic:equal name = "deleteflag" value = "0">
		    							<bean:message bundle = "pm" key = "appendix.tabular.delete.success"/>
				    					<script>
											parent.ilexleft.location.reload();
										</script
		    						</logic:equal>
		    						
			   						<logic:equal name = "deleteflag" value = "-9001">
		    							<bean:message bundle = "pm" key = "appendix.tabular.delete.failure1"/>
		    						</logic:equal>
		    						
		    						<logic:equal name = "deleteflag" value = "-9002">
		    							<bean:message bundle = "pm" key = "appendix.tabular.delete.failure2"/>
		    						</logic:equal>
		    					</td>
    					</tr>
    </logic:present>
    			
	<logic:present name = "addflag" scope = "request">
		    			<tr> 
			    				<td>&nbsp;</td>
			    				<td colspan = "5" class = "message" height = "30">
				    				<logic:equal name = "addflag" value = "0">
		    							<bean:message bundle = "pm" key = "appendix.tabular.add.success"/>
				    					<script>
											parent.ilexleft.location.reload();
										</script>
		    						</logic:equal>
		    			
		    						<logic:equal name = "addflag" value = "-9001">
		    							<bean:message bundle = "pm" key = "appendix.tabular.add.failure"/>
		    						</logic:equal>
		    						
		    						<!-- Add message for adding same addpendix failure  -->
		    						<logic:equal name = "addflag" value = "-90001">
		    							<bean:message bundle = "pm" key = "appendix.tabular.addsameappendix.failure"/>
		    						</logic:equal>
		    				
		    						<logic:equal name = "addflag" value = "-9002">
		    							<bean:message bundle = "pm" key = "appendix.tabular.add.failure"/>
		    						</logic:equal>
		    						
		    						<logic:equal name = "addflag" value = "-9020">
		    							<bean:message bundle = "pm" key = "appendix.tabular.addnetmedx.failure"/>
		    						</logic:equal>
		    						
		    						</td>
    						</tr>
    </logic:present>
    			
	<logic:present name = "updateflag" scope = "request">
		    			<tr> 
			    				<td>&nbsp;</td>
			    				<td colspan = "5" class = "message" height = "30">
					    			<logic:equal name = "updateflag" value = "0">
										<bean:message bundle = "pm" key = "appendix.tabular.update.success"/>
				    				</logic:equal>
				    		
				    				<logic:notEqual name = "updateflag" value = "0">
										<bean:message bundle = "pm" key = "appendix.tabular.update.failure"/>
				    				</logic:notEqual>
				    				
				    				<logic:equal name = "updateflag" value = "-9021">
		    							<bean:message bundle = "pm" key = "appendix.tabular.updatenetmedx.failure"/>
		    						</logic:equal>
		    						
				    				</td>
		    				</tr>
	</logic:present>
	
  				<tr> 
	    			<td rowspan = "2">&nbsp; </td>
	    			<td class = "Ntryb" rowspan = "2" colspan ="2">
	    			 <table width="100%"border="0">
					    <tr>
					    	<td class = "Ntryb" align="center"  width="100%"><bean:message bundle = "pm" key = "appendix.tabular.name"/></td>
					    	<td class = "Ntryb" align="left">
					    		<table width="100%"border="0"><tr><td  align="right"><html:button styleClass = "Nbutton" property ="Add" onclick ="return addAppendix();">Add</html:button></td></tr></table>
					    	</td>
					    </tr>
					 </table>
	    			</td>
	    			<td class = "Ntryb" colspan = "3"><bean:message bundle = "pm" key = "appendix.tabular.costprice"/></td>
	    			<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "appendix.tabular.type"/></td>
	    			<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "appendix.tabular.customerdivision"/></td>
	    			<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "appendix.tabular.bdm"/></td>
	    			<td class = "Ntryb" colspan = "2"><bean:message bundle = "pm" key = "appendix.tabular.plannedimplementation"/></td>
	    			<td class = "Ntryb" rowspan = "2"><bean:message bundle = "pm" key = "appendix.tabular.status"/></td>
				</tr>
				<tr>
					<td class = "Ntryb" title = "<bean:message bundle = "pm" key = "appendix.tabular.requiredtooltip"/>"><div align = "center"><bean:message bundle = "pm" key = "appendix.tabular.estimatedcost"/></div></td> 
					<td class = "Ntryb" title = "<bean:message bundle = "pm" key = "appendix.tabular.requiredtooltip"/>"><div align = "center"><bean:message bundle = "pm" key = "appendix.tabular.extendedprice"/></div></td>
					<td class = "Ntryb" title = "<bean:message bundle = "pm" key = "appendix.tabular.requiredtooltip"/>"><div align = "center"><bean:message bundle = "pm" key = "appendix.tabular.proforma"/></div></td>				
    				<td class = "Ntryb" title = "<bean:message bundle = "pm" key = "appendix.tabular.requiredtooltip"/>"><div align = "center"><bean:message bundle = "pm" key = "appendix.tabular.delivery"/></div></td>
    				<td class = "Ntryb" title = "<bean:message bundle = "pm" key = "appendix.tabular.effectivetooltip"/>"><div align = "center"><bean:message bundle = "pm" key = "appendix.tabular.effective"/></div></td>
    				
				</tr>
 <% if( appendix_size > 0 ){  %>  
 <logic:present name = "codes" property = "appendixlist" > 
 	<logic:iterate id = "ms" name = "codes" property = "appendixlist">
 	<bean:define id = "msaStatus" name = "ms" property = "status" type = "java.lang.String"/>
		<%	
			if ( csschooser == true ) 
			{
				backgroundclass =" Ntexto";
				backgroundhyperlinkclass = "Nhyperodd";
				backgroundclasstoleftalign ="Ntextoleftalignnowrap";
				csschooser = false;
			}
	
			else
			{
				csschooser = true;	
				backgroundclass = "Ntexte";
				backgroundclasstoleftalign = "Ntexteleftalignnowrap";
				backgroundhyperlinkclass = "Nhypereven";
			}
		%>
		<tr>		
			<td>&nbsp;</td>
			<td class = "<%= backgroundhyperlinkclass %>"><bean:write name = "ms" property = "name" /></td>
			<td class =" <%= backgroundhyperlinkclass %>">
			<bean:define id ="appendixStatus" name="ms" property ="status"/>
			<% if(appendixStatus.equals("Inwork")){%>
				[Detail&nbsp;|&nbsp;Edit]</td>
			<%}else{%>
			<% if( msaStatus.equals( "Signed" ) || msaStatus.equals( "Transmitted" )){%>
				[<a href = "AppendixDetailAction.do?Appendix_Id=<bean:write name = "ms" property = "appendix_Id" />&MSA_Id=<bean:write name = "AppendixListForm" property = "MSA_Id" />">Detail</a>&nbsp;|&nbsp;Edit]</td>
			<% } else { %>
				[<a href ="AppendixDetailAction.do?Appendix_Id=<bean:write name = "ms" property = "appendix_Id" />&MSA_Id=<bean:write name = "AppendixListForm" property = "MSA_Id" />"/>Detail</a>&nbsp;|&nbsp;<a href ="AppendixEditAction.do?appendixId=<bean:write name = "ms" property = "appendix_Id" />&msaId=<bean:write name = "AppendixListForm" property = "MSA_Id" />"/>Edit</a>]</td>
			<%}}%>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "estimatedcost"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "extendedprice"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "proformamargin"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "appendixtype"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "customerdivision"/></td>
			<td class = "<%= backgroundclasstoleftalign %>"><bean:write name = "ms" property = "cnsPOC"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "reqplasch"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "effplasch"/></td>
			<td class = "<%= backgroundclass %>"><bean:write name = "ms" property = "status"/></td>
  		</tr>
	   <% i++;  %>
	  </logic:iterate>
  </logic:present>
  
	<tr> 
    	<td ><img src = "images/spacer.gif" width = "1" height = "1"></td>
		<td colspan = "23" class = "Nbuttonrow">
			<html:button property = "sort" styleClass = "Nbutton" onclick = "return sortwindow();"><bean:message bundle = "pm" key = "appendix.tabular.sort"/></html:button>
		</td>
	</tr>

<% } else { %>
				<tr>
						<td width = "2">&nbsp;</td>
		    			<td  colspan = "8" class = "message" height = "30">
		    			<%if(viewSelectorMessage.equals("true")){ %>
		    				<bean:message bundle = "pm" key = "appendix.not.present.viewselector"/>
		    			<%}else{%>
		    				<bean:message bundle = "pm" key = "appendix.not.present"/>
		    			<%}%>	
		    			</td>
		    	</tr>
		
<% } %>
 </table>
</td>
</tr>

<jsp:include page = '/Footer.jsp'>
		      <jsp:param name = 'colspan' value = '37'/>
		      <jsp:param name = 'helpid' value = 'appendixtabularpage'/>
</jsp:include>	

</table>
</html:form>
</body>


<script>

function month_Appendix()
{
//alert('in week_Appendix()');
document.forms[0].go.value="true";
document.forms[0].action="AppendixUpdateAction.do?month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_Appendix()
{
//alert('in month_Appendix()');
document.forms[0].go.value="true";
document.forms[0].action="AppendixUpdateAction.do?week=0&clickShow=true";
document.forms[0].submit();
return true;
}

function ShowDiv()
{
leftAdjLayers();
document.getElementById("filterAppendix").style.visibility="visible";
return false;
}

function hideDiv()
{
//alert('in month_Appendix()');
document.getElementById("filterAppendix").style.visibility="hidden";

return false;
}
function checking(checkboxvalue)
{
	if(document.forms[0].appendixOtherCheck.value == 'otherOwnerSelected')
	{
		if(checkboxvalue == '%'){
			for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
			{
				document.forms[0].appendixSelectedOwners[j].checked=false;
			}
		}
		else
		{
			for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
			{
				if(document.forms[0].appendixSelectedOwners[j].checked) {
				 	document.forms[0].appendixSelectedOwners[1].checked = false;
				 	break;
				}
			}		
		}		
	}
	else
	{
		for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
		{
			if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
			{
				document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
				document.forms[0].action="AppendixUpdateAction.do?opendiv=0";
 				document.forms[0].submit();
			}
		}
	}
}

function OnLoad()
{
<%if(request.getAttribute("opendiv") != null){%>
document.getElementById("filterAppendix").style.visibility="visible";
<%}%>
}

function leftAdjLayers(){
		var layers, leftAdj=0;
		leftAdj =document.body.clientWidth;
		leftAdj = leftAdj - 280;
		document.getElementById("filterAppendix").style.left=leftAdj;
		
}

function show()
{
	document.forms[0].go.value="true";
	document.forms[0].action="AppendixUpdateAction.do?clickShow=true";
	document.forms[0].submit();
	return true;
}



var str = '';
function sortwindow()
{
	str = 'SortAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>';
	p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' ); 
}
function addAppendix()
{
document.forms[0].go.value="";
document.forms[0].action = "AppendixEditAction.do?msaId=<%= MSA_Id%>&appendixId=0";
document.forms[0].submit();
return true;
}
</script>
</html:html>
  
