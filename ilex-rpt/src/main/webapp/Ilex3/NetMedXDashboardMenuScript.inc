<SCRIPT language=JavaScript1.2>
	if (isMenu) {
	<%if(!nettype.equals("dispatch")){ %>
	arMenu1=new Array(
	140,
	findPosX('pop1'),findPosY('pop1'),
	"","",
	"","",
	"","",
	"Add Ticket","TicketDetailAction.do?appendixid=<%=appendixid%>&ticket_type=newticket&viewjobtype=<%=viewjobtype%>&ownerId=<%=viewjobtype%>&fromPage=NetMedX",0,
	"Contract Documents","#",1,
	"Custom Fields","AppendixCustomerInformation.do?function=add&appendixid=<%=appendixid%>&viewjobtype=<%=viewjobtype%>&ownerId=<%=viewjobtype%>&fromPage=NetMedX",0,
	"Customer Reference","AddCustRef.do?typeid=<%=appendixid%>&appendixid=<%=appendixid%>&type=Appendix&viewjobtype=<%=viewjobtype%>&ownerId=<%=viewjobtype%>&fromPage=NetMedX",0,
	"Documents","#",1,
	"View Job Sites","SiteManagement.do?function=view&appendixid=<%=appendixid%>&viewjobtype=<%=viewjobtype%>&ownerId=<%=viewjobtype%>&initialCheck=true&pageType=jobsites&fromPage=NetMedX",0,
	"<bean:message bundle = "AM" key = "am.summary.Partnerinfo"/>","#",1
	)
	
	arMenu1_2=new Array(
	"NetMedX", "#",1
	)
	
	arMenu1_2_1 = new Array(
	"Send", "javascript:sendemail();", 0,
	"View", "#", 1
	)
	
	arMenu1_2_1_2 = new Array(
	"PDF", "javascript:view('pdf');", 0 
	)
	
	arMenu1_5=new Array(
	"<bean:message bundle = "PRM" key = "prm.viewalldocuments"/>","javascript:viewdocuments();",0,
	"<bean:message bundle = "PRM" key = "prm.uploaddocuments"/>","javascript:upload();",0
	) 
	
	arMenu1_7 = new Array(
	"<bean:message bundle = "AM" key="am.Partnerinfo.sow"/>","PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=<%=nettype%>&viewjobtype=<%=viewjobtype%>&ownerId=<%= loginUserId%>&fromPage=NetMedX",0,
	"<bean:message bundle = "AM" key="am.Partnerinfo.assumptions"/>","PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=<%=nettype%>&viewjobtype=<%=viewjobtype%>&ownerId=<%= loginUserId%>&fromPage=NetMedX",0,
	"<bean:message bundle = "AM" key="am.Partnerinfo.Inst_cond"/>","PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&viewjobtype=<%=viewjobtype%>&ownerId=<%=viewjobtype%>&fromPage=NetMedX",0
	)
	arMenu2=new Array(
	120,
	findPosX('pop2'),findPosY('pop2'),
	"","",
	"","",
	"","",
	"<bean:message bundle = "PRM" key = "prm.netmedx.redlinereport"/>","javascript:openreportschedulewindow('redline')",0,
	"<bean:message bundle = "PRM" key = "prm.netmedx.detailedsummary"/>","javascript:openreportschedulewindow('detailsummary')",0,
	"<bean:message bundle = "PRM" key = "prm.netmedx.csvsummary"/>","javascript:openreportschedulewindow('CSVSummary')",0
	)
	<%}else{%>
	arMenu1=new Array(
	140,
	findPosX('pop2'),findPosY('pop2'),
	"","",
	"","",
	"","",
	"<bean:message bundle = "PRM" key = "prm.netmedx.redlinereport"/>","javascript:openreportschedulewindowdispatch('redline')",0,
	"<bean:message bundle = "PRM" key = "prm.netmedx.detailedsummary"/>","javascript:openreportschedulewindowdispatch('detailsummary')",0,
	"<bean:message bundle = "PRM" key = "prm.netmedx.csvsummary"/>","javascript:openreportschedulewindowdispatch('CSVSummary')",0,
	"Account Summary Report","javascript:summarywindow('accountsummary')",0,
	"Owner Summary Report","javascript:ownersummarywindow()",0
	)
	<%}%>
	  document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
	}
</SCRIPT>