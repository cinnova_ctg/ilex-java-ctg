<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Ilex Data Warehouse - Navigation</title>
<style>

    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 9px;
    	cursor : auto;
    	background-color: #E1EAE7;
    }
    .divBody01 {
		 padding : 10px 0px 0px 5px;
    }
    .divBody02 {
		 padding : 10px 0px 0px 20px;
    }
    .section01 {
		font-size : 8pt;
		font-weight : bold;
		padding : 3px 0px 3px 0px;
        vertical-align : top;
    }    
    .section02 {
		font-size : 8pt;
		font-weight : normal;
		padding : 3px 0px 3px 10px;
        vertical-align : top;
    }
     .section02A {
		font-size : 8pt;
		font-weight : normal;
		padding : 3px 0px 3px 20px;
        vertical-align : top;
    }   
</style>
</head>
<body>
<div class="divBody01">
<table border="0" cellspacing="0" cellpadding="0">

<tr>
 <td class="section02A"><a href="/rpt/Revenue.xo?form_action=revenueSummaryReport&year=2008" target="wa">Home Dashboard</a></td>
</tr>

<tr>
 <td class="section01">Revenue</td>
</tr>

<tr>
 <td class="section02"><a href="/rpt/RevSum.xo?form_action=revenueSummary&project_type=all" target="wa">Monthly by Status</a></td>
</tr>

<tr>
 <td class="section02"><a href="/rpt/RevSum.xo?form_action=revenueSummary&project_type=all" target="wa">Monthly by Account</a></td>
</tr>

<tr>
 <td class="section02"><a href="/rpt/Revenue.xo?form_action=revenueSummaryReport&year=2007" target="wa">Annual Progression</a></td>
</tr>

<tr>
 <td class="section01">Management</td>
</tr>

<tr>
 <td class="section02"><a href="/rpt//LaborRates.xo?fn=lab1&ua=1" target="wa">International Labor Rates</a></td>
</tr>

<tr>
 <td class="section02"><a href="/rpt/TopLevel.xo?form_action=topLevelReport&project_type=all" target="wa">Real-Time Snapshot</a></td>
</tr>

<tr>
 <td class="section02"><a href="/rpt/MM.xo?form_action=mm_summary" target="wa">Minuteman</a></td>
</tr>

<tr>
 <td class="section02"><a href="/rpt/MM.xo?form_action=sp_summary" target="wa">Speedpay</a></td>
</tr>

<tr>
 <td class="section02">VGP Reports</td>
</tr>
<tr>
 <td class="section02A"><a href="/rpt/QtrCom.xo?fn=qrtcom1&quarter=2/2010&page_type=summary1" target="wa">Quarterly Commission Summary</a></td>
</tr>
<tr>
 <td class="section02A"><a href="/rpt/VGP.xo?form_action=vgp1&sort=ap_jl_ot_name" target="wa">VGP by Account/BDM</a></td>
</tr>

<tr>
 <td class="section02A"><a href="/rpt/XVGP.xo?action=vgp_verification1" target="wa">Corrected VGP</a></td>
</tr>

<tr>
 <td class="section02A"><a href="/rpt/VGPM.xo?form_action=vgpm_all&owner=All" target="wa">VGP by PM/Owner</a></td>
</tr>

<tr>
 <td class="section02A"><a href="/rpt/VGPM.xo?form_action=vgpm_all1" target="wa">VGP by PM/Owner (New)</a></td>
</tr>
<!--
<tr>
 <td class="section02A"><a href="/rpt/COM.xo?form_action=commission&default_view=YES&quarter=20092" target="wa">Quarterly SummaryX</a></td>
</tr>
<tr>
 <td class="section02A"><a href="/rpt/VGP.xo?form_action=vgp1&sort=ap_jl_ot_name" target="wa">Low VGP</a></td>
</tr>
-->
<tr>
 <td class="section02"><a href="/rpt/JSA.xo?form_action=jsa_summary" target="wa">Out-of-Bounds</a></td>
</tr>

<tr>
 <td class="section02"><a href="http://dw.contingent.local/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fFinancialSummary_Revenue" target="wa">Revenue (Offsite Based)</a></td>
</tr>

<!--
<tr>
 <td class="section02"><a href="/rpt/INV.xo?form_action=invoice_list1" target="wa">Consolidated Invoices</a></td>
</tr>

<tr>
 <td class="section02"><a href="/rpt/DG.xo?form_action=dg_scheduled" target="wa">Dollar General - Scheduled</a></td>
</tr>

<tr>
 <td class="section02"><a href="/rpt/INV.xo?form_action=invoice_list2" target="wa">Dollar General - Invoice</a></td>
</tr>
-->
<tr>
 <td class="section01">Accounting</td>
</tr>
<tr>
 <td class="section02">eInvoices</td>
</tr>
<tr>
 <td class="section02A"><a href="/rpt/Inv.xo?fn=inv1&ua=1" target="wa">eInvoice Payment Status</a></td>
</tr>
<tr>
 <td class="section02">Agent Commissions</td>
</tr>
<tr>
 <td class="section02A"><a href="/rpt/Inv.xo?fn=agent_list&ua=4" target="wa">Commission List</a></td>
</tr>
<tr>
 <td class="section02A"><a href="/rpt/Inv.xo?fn=inv1&ua=2" target="wa">NRC Review/Payment</a></td>
</tr>
<!-- Old
<tr>
 <td class="section02"><a href="http://dw.contingent.local/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fProductivity_Headcount" target="wa">Employees Assigned</a></td>
</tr>
<tr>
 <td class="section02"><a href="http://dw.contingent.local/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fProductivity_Revenue" target="wa">Revenue by Employees </a></td>
</tr>
<tr>
 <td class="section02"><a href="http://dw.contingent.local/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fProductivity_Visits" target="wa">Site Visits by Employees </a></td>
</tr>
<tr>
 <td class="section01">Quality</td>
</tr>
-->
<tr>
 <td class="section02"> </td>
</tr>
<tr>
 <td class="section01">Out of Bounds Reports</td>
</tr>
<tr>
 <td class="section02"><a href="/rpt/JSA.xo?form_action=jsa_summary" target="wa">Job Status</a></td>
</tr>
<!--
<tr>
 <td class="section01">Invoicing Delays</td>
</tr>

<tr>
 <td class="section02"><a href="/rpt/MngRpt.xo?form_action=managementReports&report_type=all&job_status=complete" target="wa">Completed (Accting Delay)</a></td>
</tr>

<tr>
 <td class="section02"><a href="/rpt/MngRpt.xo?form_action=managementReports&report_type=all&job_status=inwork-offsite" target="wa">Offsite (Operations Delay)</a></td>
</tr>

<tr>
 <td class="section02"><a href="/rpt/MngRpt.xo?form_action=managementReports&report_type=all&job_status=inwork-onsite" target="wa">Onsite</a></td>
</tr>
-->
<tr>
 <td class="section01">Support Functions</td>
</tr>

<tr>
 <td class="section02">&nbsp;&nbsp;<a href="/rpt/POSch.xo?form_action=poSearch" target="wa">P O Search</a></td>
</tr>

<!--  
<tr>
 <td class="section02">&nbsp;&nbsp;<a href="/rpt/VendorInvoice.xo?form_action=vendorManage" target="wa">Process Vendor Invoice</a></td>
</tr>
-->
<tr>
 <td class="section02">&nbsp;&nbsp;<a href="/rpt/Vendor.xo?form_action=costSummaryTop30" target="wa">Top Vendors</a></td>
</tr>


<tr>
 <td class="section01">Project Reports</td>
</tr>

<tr>
 <td class="section02">&nbsp;&nbsp;<a href="/rpt/App.xo?form_action=appendixList" target="wa">List of Appendices</a></td>
</tr>
<!--
<tr>
 <td class="section02">&nbsp;&nbsp;<a href="#" target="wa">A&F Bi-Weekly</a></td>
</tr>
-->
<tr>
 <td class="section01">Dispatch Reports</td>
</tr>

<tr>
 <td class="section02">&nbsp;&nbsp;<a href="/rpt/DD.xo?form_action=dd_real_time" target="wa">Dispatches by CCA</a></td>
</tr>

<tr>
 <td class="section02">&nbsp;&nbsp;<a href="/rpt/DD.xo?form_action=dd_vgpm" target="wa">VGPM by CCA</a></td>
</tr>

<tr>
 <td class="section02">&nbsp;&nbsp;<a href="/rpt/DD.xo?form_action=dd_vcogs" target="wa">Pricing by CCA</a></td>
</tr>

<tr>
 <td class="section01">e<i>I</i>nvoice</td>
</tr>
<tr>
 <td class="section02"><a href="/eInvoice" target="_top">Home Page</a></td>
</tr>
<tr>
 <td class="section01">Prototype Pages</td>
</tr>
<tr>
 <td class="section02">&nbsp;&nbsp;<a href="/rpt/DB1.xo?action=dash_board1&view=All" target="wa">Top Level Dashboard</a></td>
</tr>
<!--  
<tr>
 <td class="section02">WUG</td>
</tr>
<tr>
 <td class="section02">&nbsp;&nbsp;<a href="/rpt/MSP.xo?action=rpt_matrix_summary" target="wa">Outage Summary</a></td>
</tr>
<tr>
 <td class="section02">&nbsp;&nbsp;<a href="/rpt/MSP.xo?action=current_outages" target="wa">Real-time Outages</a></td>
</tr>
<tr>
 <td class="section02">&nbsp;&nbsp;<a href="/rpt/msp/msp_site_status.html" target="wa">Site Status</a></td>
</tr>
-->
<tr>
 <td class="section02">Invoive</td>
</tr>
<tr>
 <td class="section02">&nbsp;&nbsp;<a href="/rpt/pdf/invoice_configuration.html" target="wa">NetMedX Like</a></td>
</tr>
<tr>
 <td class="section02">Productivity</td>
</tr>
<tr>
 <td class="section02">&nbsp;&nbsp;<a href="/rpt/Workload.xo?ac=displayWorkloadPage" target="wa">Realtime Actions</a></td>
</tr>
</table>
</div>
</body>
</html>