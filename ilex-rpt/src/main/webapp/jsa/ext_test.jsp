<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Array Grid Example</title>

    <link rel="stylesheet" type="text/css" href="ext-1.0.1/resources/css/ext-all.css" />

    <!-- GC --> 
    <link rel="stylesheet" type="text/css" href="ext-1.0.1/resources/css/xtheme-aero.css" />
    <!-- LIBS -->
    <script type="text/javascript" src="ext-1.0.1/adapter/yui/yui-utilities.js"></script>     
    <script type="text/javascript" src="ext-1.0.1/adapter/yui/ext-yui-adapter.js"></script>     
    <!-- ENDLIBS -->
    <script type="text/javascript" src="ext-1.0.1/ext-all.js"></script>

    <script type="text/javascript" src="xml-grid.js"></script>

    <link rel="stylesheet" type="text/css" href="grid-examples.css" />

    <!-- Common Styles for the examples -->
    <link rel="stylesheet" type="text/css" href="ext-1.0.1/examples.css" />
</head>
<body>
<script type="text/javascript" src="ext-1.0.1/examples.js"></script>
<link rel="stylesheet" type="text/css" href="ext-1.0.1/lib.css" />
<div id="lib-bar" class="x-layout-panel-hd"><div id="lib-bar-inner"> <span>Theme:</span>&#160;&#160;<select id="exttheme"><option value="default">Ext Blue</option><option value="gray">Gray Theme</option><option value="aero">Aero Glass Theme</option><option value="vista">Vista Dark Theme</option></select> | <span>Library:</span>&#160;&#160;<select id="extlib"><option value="ext">Ext Stand-alone</option><option value="yahoo">Yahoo! UI Utilities</option><option value="jquery">jQuery</option><option value="prototype">Prototype+Scriptaculous</option></select></div></div>
<h1>Array Grid Example</h1>
<p>This example shows how to create a grid from Array data. For more details on this example, see <a href="http://www.jackslocum.com/yui/2006/08/30/a-grid-component-for-yahoo-ui-extensions-v1/">the blog post</a>.</p>
<p>Note that the js is not minified so it is readable. See <a href="array-grid.js">array-grid.js</a>.</p>

<!-- a place holder for the grid. requires the unique id to be passed in the javascript function, and width and height ! -->
<div id="example-grid" class="x-grid-mso" style="border: 1px solid #c3daf9; overflow: hidden; width:520px;"></div>
</div>
</body>
</html>
