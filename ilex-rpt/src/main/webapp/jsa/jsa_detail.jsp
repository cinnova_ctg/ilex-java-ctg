<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.net.URLEncoder" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
int row_count = 0;

Vector rows = (Vector)request.getAttribute("rows");
if (rows != null) {
	row_count = rows.size();
}

String from_date = (request.getAttribute("from_date") != null ? (String)request.getAttribute("from_date") : "");
String to_date = (request.getAttribute("to_date") != null ? (String)request.getAttribute("to_date") : "");
String lx_pr_type = (request.getAttribute("lx_pr_type") != null ? (String)request.getAttribute("lx_pr_type") : "");


%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Job Status Analysis - Details</title>
<style>
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 12px;
    	cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .rptBody02 {
		 padding : 10px 0px 0px 20px;
    }
    .titleLine {
		font-size : 11pt;
		font-weight : bold;
		text-align : left;
		vertical-align : Middle;
		padding : 0px 0px 6px 0px;
    } 
    .sectionTitleLine {
		font-size : 10pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 0px 0px 6px 0px;
    }
    .sectionTitleLineNav {
		font-size : 8pt;
		font-weight : normal;
		text-align : right;
		vertical-align : middle;
		padding : 0px 0px 0px 0px;
    }
    .label1, .label2 {
        font-size : 9pt;
		font-weight : bold;
		vertical-align : top;
		padding : 10px 10px 6px 0px;
    }
    .form1 {
        font-size : 10pt;
		font-weight : normal;
		padding : 0px 0px 0px 0px;
    }
    .elementContainer {
    padding : 0px 2px 0px 2px;
    }
    .columnHeader1 {
		font-size : 9pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 3px 10px 3px 10px;
		background-color: #ddf0ff;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 10pt;
		font-weight : normal;
		text-align : left;
		vertical-align : middle;
		padding : 1px 4px 1px 4px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #fffced;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ffffff;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }        
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
		text-align : center;
		width: 26px;
        height: 24px;
    }
    .date1 {
        font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
    }
    .bottomLine, .bottomLineCenter {
    	font-size : 8pt;
		font-weight : bold;
		text-align : right;
		vertical-align : middle;
		padding : 2px 2px 2px 0px;
		background-color: #e0edf5;
    }
    .bottomLineCenter {
		text-align : center;
    }
</style>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<form name="form1" action="JSA.xo" method="post">
<input type="hidden" name="form_action", value="jsa_summary">

<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr>
 <td rowspan="2" class="titleLine">Job Status Analysis - Details for </td>
 <td align="right">Project Type:&nbsp;&nbsp;&nbsp;<el:element type="menu1" request="<%=request%>" name="lx_pr_type" source="static" js="" data="All~Deployment~Dispatch"/></td>
 </tr>
 <tr>
 <td align="right">&nbsp;&nbsp;<el:element type="date1" request="<%=request%>" name="from_date" source="" js="" data=""/>&nbsp;&nbsp;to&nbsp;&nbsp;<el:element type="date1" request="<%=request%>" name="to_date" source="" js="" data=""/>&nbsp;&nbsp;<input type="submit" value="Go" class=date1></td>
 </tr>
 </table>
 </td>
</tr>
</form>
<tr>
<td>
 <table border="0" cellspacing="1" cellpadding="0">
 <tr>
     <td class="columnHeader1">Account</td>
     <td class="columnHeader1">Project</td>
     <td class="columnHeader1">Job/Ticket</td>
     <td class="columnHeader1">Status</td>
     <td class="columnHeader1">Out of Limit</td>
     <td class="columnHeader1">Date</td>
     <td class="columnHeader1">Created</td>
     <td class="columnHeader1">Scheduled Start</td>
 </tr>
 
     <% 
     if (row_count > 0) {
       for (int i=0; i<rows.size(); i++) {
    	 String[] row = (String[])rows.get(i);   	 
     %>
 
 <tr>
     <td class="cDataLeft0"><%=row[0]%></td>
     <td class="cDataLeft0"><%=row[1]%></td>
     <td class="cDataLeft0"><%=row[2]%></td>
     <td class="cDataCenter0"><%=row[3]%></td>
     <td class="cDataCenter0"><%=row[4]%></td>
     <td class="cDataCenter0"><%=row[5]%></td>
     <td class="cDataCenter0"><%=row[6]%></td>
     <td class="cDataCenter0"><%=row[7]%></td>
 </tr> 
     <%    	 
     }
     
     } else {
     
     %>  
         <tr><td colspan="8">&nbsp;</td></tr>
         <tr><td colspan="8" class="cDataLeft1">No jobs meet the criteria you selected.</td></tr>
     <%
        }
     %>
 
 </table>
</td>
</tr>

</table>

</div> 
</body>
</html>