Ext.onReady(function(){

    // create the Data Store
    var ds = new Ext.data.Store({
        // load using HTTP
        proxy: new Ext.data.HttpProxy({url: 'http://localhost/rpt/jsa/jsa_xml_output.jsp'}),

        // the return will be XML, so lets set up a reader
        reader: new Ext.data.XmlReader({
               // records will have an "Item" tag
               record: 'job'
           }, [
                'account', 'project', 'title', 'date'
           ])
    });

    var cm = new Ext.grid.ColumnModel([
	    {header: "Customer",   width: 124,  dataIndex: 'account'},
		{header: "Project",    width: 135, dataIndex: 'project'},
		{header: "Job/Ticket", width: 125, dataIndex: 'title'},
		{header: "Duration",   width: 100, dataIndex: 'title'},
		{header: "Date",       width: 82, dataIndex: 'date'},
		{header: "Date",       width: 70, dataIndex: 'date'},
		{header: "Date",       width: 70, dataIndex: 'date'},
		{header: "Date",       width: 70, dataIndex: 'date'}
	]);

    cm.defaultSortable = true;

    // create the grid
    var grid = new Ext.grid.Grid('grid', {
        ds: ds,
        cm: cm
    });
    grid.render();

    ds.load();
});
