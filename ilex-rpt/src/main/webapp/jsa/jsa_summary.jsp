<%@ page language="java" contentType="text/html; charset=iso-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.net.URLEncoder" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Job Audit Analysis - Summary</title>

    <%
	String ext_file = "tbs.txt";
	String ext_url = "/rpt/JSA.xo?form_action=audit_view&view=1";
	String hidden = "";
	
	String view = (String) request.getAttribute("view");
	
	if (view == null) {
		view = "";
	} else {
		 hidden = "<input type=\"hidden\" name=\"view\", value=\""+ view+ "\">\n";
	}

    if (!view.equals("")) {
    	 // get ext_file and ext_url
    	
    %>
    <link rel="stylesheet" type="text/css" href="/rpt/js/ext-1.0.1/resources/css/ext-all.css" />

    <!-- GC --> 
    <link rel="stylesheet" type="text/css" href="/rpt/js/ext-1.0.1/resources/css/xtheme-aero.css" />
    
    <!-- LIBS -->
    <script type="text/javascript" src="/rpt/js/ext-1.0.1/adapter/yui/yui-utilities.js"></script>   
    <script type="text/javascript" src="/rpt/js/ext-1.0.1/adapter/yui/ext-yui-adapter.js"></script>   
    <!-- ENDLIBS -->
    
    <script type="text/javascript" src="/rpt/js/ext-1.0.1/ext-all.js"></script>
     
    <script type="text/javascript">
    Ext.onReady(function(){

    // create the Data Store
    var ds = new Ext.data.Store({
        // load using HTTP
        proxy: new Ext.data.HttpProxy({url: '/rpt/JSA.xo?form_action=audit_view&view=1'}),
        
        <jsp:include page="<%=ext_file%>" flush="true" />

    cm.defaultSortable = true;

    // create the grid
    var grid = new Ext.grid.Grid('grid', {
        ds: ds,
        cm: cm
    });
    grid.render();

    ds.load();
    });
    
    </script>

    <link rel="stylesheet" type="text/css" href="/rpt/js/ext-1.0.1/examples/grid/grid-examples.css" />
    <%
    }
    %>
    
<script language="JavaScript">
	var tempcal = '';
	var	fixedX = -1			// x position (-1 if to appear below control)
	var	fixedY = -1			// y position (-1 if to appear below control)
	var startAt = 1			// 0 - sunday ; 1 - monday
	var showWeekNumber = 1	// 0 - don't show; 1 - show
	var showToday = 1		// 0 - don't show; 1 - show
	var imgDir = "\\rpt\\images\\"	
	var gotoString = "Go To Current Month"
	var todayString = "Today is"
	var weekString = "WkNo"
	var scrollLeftMessage = "Click to scroll to previous month. Hold mouse button to scroll automatically."
	var scrollRightMessage = "Click to scroll to next month. Hold mouse button to scroll automatically."
	var selectMonthMessage = "Click to select a month."
	var selectYearMessage = "Click to select a year."
	var selectDateMessage = "Select [date] as date." // do not replace [date], it will be replaced by date.

	var	crossobj, crossMonthObj, crossYearObj, monthSelected, yearSelected, dateSelected, omonthSelected, oyearSelected, odateSelected, monthConstructed, yearConstructed, intervalID1, intervalID2, timeoutID1, timeoutID2, ctlToPlaceValue, ctlNow, dateFormat, nStartingYear

	var	bPageLoaded=false
	var	ie=document.all
	var	dom=document.getElementById

	var	ns4=document.layers
	var	today =	new	Date()
	var	dateNow	 = today.getDate()
	var	monthNow = today.getMonth()
	var	yearNow	 = today.getYear()
	var	imgsrc = new Array("drop1.gif","drop2.gif","left1.gif","left2.gif","right1.gif","right2.gif")
	var	img	= new Array()

	var bShow = false;

    /* hides <select> and <applet> objects (for IE only) */
    function hideElement( elmID, overDiv )
    {
      if( ie )
      {
        for( i = 0; i < document.all.tags( elmID ).length; i++ )
        {
          obj = document.all.tags( elmID )[i];
          if( !obj || !obj.offsetParent )
          {
            continue;
          }
      
          // Find the element's offsetTop and offsetLeft relative to the BODY tag.
          objLeft   = obj.offsetLeft;
          objTop    = obj.offsetTop;
          objParent = obj.offsetParent;
          
          while( objParent.tagName.toUpperCase() != "BODY" )
          {
            objLeft  += objParent.offsetLeft;
            objTop   += objParent.offsetTop;
            objParent = objParent.offsetParent;
          }
      
          objHeight = obj.offsetHeight;
          objWidth = obj.offsetWidth;
      
          if(( overDiv.offsetLeft + overDiv.offsetWidth ) <= objLeft );
          else if(( overDiv.offsetTop + overDiv.offsetHeight ) <= objTop );
          else if( overDiv.offsetTop >= ( objTop + objHeight ));
          else if( overDiv.offsetLeft >= ( objLeft + objWidth ));
          else
          {
            obj.style.visibility = "hidden";
          }
        }
      }
    }
     
    /*
    * unhides <select> and <applet> objects (for IE only)
    */
    function showElement( elmID )
    {
      if( ie )
      {
        for( i = 0; i < document.all.tags( elmID ).length; i++ )
        {
          obj = document.all.tags( elmID )[i];
          
          if( !obj || !obj.offsetParent )
          {
            continue;
          }
        
          obj.style.visibility = "";
        }
      }
    }

	function HolidayRec (d, m, y, desc)
	{
		this.d = d
		this.m = m
		this.y = y
		this.desc = desc
	}

	var HolidaysCounter = 0
	var Holidays = new Array()

	function addHoliday (d, m, y, desc)
	{
		Holidays[HolidaysCounter++] = new HolidayRec ( d, m, y, desc )
	}

	if (dom)
	{
		for	(i=0;i<imgsrc.length;i++)
		{
			img[i] = new Image
			img[i].src = imgDir + imgsrc[i]
		}
	// set the table width 
		var datePointer = '4';
		document.write ("<div onclick='bShow=true' id='calendar'	style='z-index:+999;position:absolute;visibility:hidden;'>   <table	width="+((showWeekNumber==1)?180:220)+" style='font-family:arial;font-size:11px;border-width:1;border-style:solid;border-color:#a0a0a0;font-family:arial; font-size:11px}' bgcolor='#ffffff'><tr bgcolor=teal><td><table width='"+((showWeekNumber==1)?200:218)+"'><tr><td style='padding:2px;font-family:arial; font-size:11px;'><font color='#ffffff'><B><span id='caption'></span></B></font></td><td align=right><a href='javascript:clearCalendar();'>	<IMG SRC='"+imgDir+"clear.jpg' WIDTH='15' HEIGHT='13' BORDER='0' ALT='Clear the Date'></a></td><td align=right><a href='javascript:hideCalendar()'><IMG SRC='"+imgDir+"close.gif' WIDTH='15' HEIGHT='13' BORDER='0' ALT='Close the Calendar'>					</a></td></tr></table></td></tr><tr><td style='padding:5px' bgcolor=#ffffff><span id='content'></span></td></tr>")
			
		if (showToday==1)
		{
			document.write ("<tr bgcolor=lightgrey><td style='padding:1px' align=center><span id='lblToday'></span></td></tr>")
		}
			
		document.write ("</table></div><div id='selectMonth' style='z-index:+999;position:absolute;visibility:hidden;'></div><div id='selectYear' style='z-index:+999;position:absolute;visibility:hidden;'></div>");
	}

	var	monthName =	new	Array("January","February","March","April","May","June","July","August","September","October","November","December")
	var	monthName2 = new Array("JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC")
	if (startAt==0)
	{
		dayName = new Array	("Sun","Mon","Tue","Wed","Thu","Fri","Sat")
	}
	else
	{
		dayName = new Array	("Mon","Tue","Wed","Thu","Fri","Sat","Sun")
	}
	var	styleAnchor="text-decoration:none;color:black;"
	var	styleLightBorder="border-style:solid;border-width:1px;border-color:#a0a0a0;"

	function swapImage(srcImg, destImg){
		if (ie)	{ document.getElementById(srcImg).setAttribute("src",imgDir + destImg) }
	}

	
	function init()	{
		if (!ns4)
		{
			if (!ie) { yearNow += 1900	}

			crossobj=(dom)?document.getElementById("calendar").style : ie? document.all.calendar : document.calendar
			hideCalendar()

			crossMonthObj=(dom)?document.getElementById("selectMonth").style : ie? document.all.selectMonth	: document.selectMonth

			crossYearObj=(dom)?document.getElementById("selectYear").style : ie? document.all.selectYear : document.selectYear

			monthConstructed=false;
			yearConstructed=false;

			if (showToday==1)
			{
				document.getElementById("lblToday").innerHTML =	todayString + " <a onmousemove='window.status=\""+gotoString+"\"' onmouseout='window.status=\"\"' title='"+gotoString+"' style='"+styleAnchor+"' href='javascript:monthSelected=monthNow;yearSelected=yearNow;constructCalendar();'>"+dayName[(today.getDay()-startAt==-1)?6:(today.getDay()-startAt)]+", " + dateNow + " " + monthName[monthNow].substring(0,3)	+ "	" +	yearNow	+ "</a>"
			}

			sHTML1="<span id='spanLeft'	style='border-style:solid;border-width:1;border-color:#3366FF;cursor:pointer' onmouseover='swapImage(\"changeLeft\",\"left2.gif\");this.style.borderColor=\"#88AAFF\";window.status=\""+scrollLeftMessage+"\"' onclick='javascript:decMonth()' onmouseout='clearInterval(intervalID1);swapImage(\"changeLeft\",\"left1.gif\");this.style.borderColor=\"#3366FF\";window.status=\"\"' onmousedown='clearTimeout(timeoutID1);timeoutID1=setTimeout(\"StartDecMonth()\",500)'	onmouseup='clearTimeout(timeoutID1);clearInterval(intervalID1)'>&nbsp<IMG id='changeLeft' SRC='"+imgDir+"left1.gif' width=10 height=11 BORDER=0>&nbsp</span>&nbsp;"
			sHTML1+="<span id='spanRight' style='border-style:solid;border-width:1;border-color:#3366FF;cursor:pointer'	onmouseover='swapImage(\"changeRight\",\"right2.gif\");this.style.borderColor=\"#88AAFF\";window.status=\""+scrollRightMessage+"\"' onmouseout='clearInterval(intervalID1);swapImage(\"changeRight\",\"right1.gif\");this.style.borderColor=\"#3366FF\";window.status=\"\"' onclick='incMonth()' onmousedown='clearTimeout(timeoutID1);timeoutID1=setTimeout(\"StartIncMonth()\",500)'	onmouseup='clearTimeout(timeoutID1);clearInterval(intervalID1)'>&nbsp<IMG id='changeRight' SRC='"+imgDir+"right1.gif'	width=10 height=11 BORDER=0>&nbsp</span>&nbsp"
			sHTML1+="<span id='spanMonth' style='border-style:solid;border-width:1;border-color:#3366FF;cursor:pointer'	onmouseover='swapImage(\"changeMonth\",\"drop2.gif\");this.style.borderColor=\"#88AAFF\";window.status=\""+selectMonthMessage+"\"' onmouseout='swapImage(\"changeMonth\",\"drop1.gif\");this.style.borderColor=\"#3366FF\";window.status=\"\"' onclick='popUpMonth()'></span>&nbsp;"
			sHTML1+="<span id='spanYear' style='border-style:solid;border-width:1;border-color:#3366FF;cursor:pointer' onmouseover='swapImage(\"changeYear\",\"drop2.gif\");this.style.borderColor=\"#88AAFF\";window.status=\""+selectYearMessage+"\"'	onmouseout='swapImage(\"changeYear\",\"drop1.gif\");this.style.borderColor=\"#3366FF\";window.status=\"\"'	onclick='popUpYear()'></span>&nbsp;"
			
			document.getElementById("caption").innerHTML  =	sHTML1

			bPageLoaded=true
		}
	}

	function hideCalendar()	{
		crossobj.visibility="hidden"
		if (crossMonthObj != null){crossMonthObj.visibility="hidden"}
		if (crossYearObj !=	null){crossYearObj.visibility="hidden"}

	    showElement( 'SELECT' );
		showElement( 'APPLET' );
	}

	function padZero(num) {
		return (num	< 10)? '0' + num : num ;
	}

	function constructDate(d,m,y)
	{
		sTmp = dateFormat
		sTmp = sTmp.replace	("dd","<e>")
		sTmp = sTmp.replace	("d","<d>")
		sTmp = sTmp.replace	("<e>",padZero(d))
		sTmp = sTmp.replace	("<d>",d)
		sTmp = sTmp.replace	("mmmm","<p>")
		sTmp = sTmp.replace	("mmm","<o>")
		sTmp = sTmp.replace	("mm","<n>")
		sTmp = sTmp.replace	("m","<m>")
		sTmp = sTmp.replace	("<m>",m+1)
		sTmp = sTmp.replace	("<n>",padZero(m+1))
		sTmp = sTmp.replace	("<o>",monthName[m])
		sTmp = sTmp.replace	("<p>",monthName2[m])
		sTmp = sTmp.replace	("yyyy",y)
		return sTmp.replace ("yy",padZero(y%100))
	}

	function closeCalendar() {
		var	sTmp

		hideCalendar();
		ctlToPlaceValue.value =	constructDate(dateSelected,monthSelected,yearSelected)
		ctlToPlaceValue.focus()
	}


	function clearCalendar() {
		var	sTmp

		hideCalendar();
		ctlToPlaceValue.value =	'';
		ctlToPlaceValue.focus();
	}
	/*** Month Pulldown	***/

	function StartDecMonth()
	{
		intervalID1=setInterval("decMonth()",80)
	}

	function StartIncMonth()
	{
		intervalID1=setInterval("incMonth()",80)
	}

	function incMonth () {
		monthSelected++
		if (monthSelected>11) {
			monthSelected=0
			yearSelected++
		}
		constructCalendar()
	}

	function decMonth () {
		monthSelected--
		if (monthSelected<0) {
			monthSelected=11
			yearSelected--
		}
		constructCalendar()
	}

	function constructMonth() {
		popDownYear()
		if (!monthConstructed) {
			sHTML =	""
			for	(i=0; i<12;	i++) {
				sName =	monthName[i];
				if (i==monthSelected){
					sName =	"<B>" +	sName +	"</B>"
				}
				sHTML += "<tr><td id='m" + i + "' onmouseover='this.style.backgroundColor=\"#FFCC99\"' onmouseout='this.style.backgroundColor=\"\"' style='cursor:pointer' onclick='monthConstructed=false;monthSelected=" + i + ";constructCalendar();popDownMonth();event.cancelBubble=true'>&nbsp;" + sName + "&nbsp;</td></tr>"
			}

			document.getElementById("selectMonth").innerHTML = "<table width=70	style='font-family:arial; font-size:11px; border-width:1; border-style:solid; border-color:#a0a0a0;' bgcolor='#FFFFDD' cellspacing=0 onmouseover='clearTimeout(timeoutID1)'	onmouseout='clearTimeout(timeoutID1);timeoutID1=setTimeout(\"popDownMonth()\",100);event.cancelBubble=true'>" +	sHTML +	"</table>"

			monthConstructed=true
		}
	}

	function popUpMonth() {
		constructMonth()
		crossMonthObj.visibility = (dom||ie)? "visible"	: "show"
		crossMonthObj.left = parseInt(crossobj.left) + 50
		crossMonthObj.top =	parseInt(crossobj.top) + 26

		hideElement( 'SELECT', document.getElementById("selectMonth") );
		hideElement( 'APPLET', document.getElementById("selectMonth") );			
	}

	function popDownMonth()	{
		crossMonthObj.visibility= "hidden"
	}

	/*** Year Pulldown ***/

	function incYear() {
		for	(i=0; i<7; i++){
			newYear	= (i+nStartingYear)+1
			if (newYear==yearSelected)
			{ txtYear =	"&nbsp;<B>"	+ newYear +	"</B>&nbsp;" }
			else
			{ txtYear =	"&nbsp;" + newYear + "&nbsp;" }
			document.getElementById("y"+i).innerHTML = txtYear
		}
		nStartingYear ++;
		bShow=true
	}

	function decYear() {
		for	(i=0; i<7; i++){
			newYear	= (i+nStartingYear)-1
			if (newYear==yearSelected)
			{ txtYear =	"&nbsp;<B>"	+ newYear +	"</B>&nbsp;" }
			else
			{ txtYear =	"&nbsp;" + newYear + "&nbsp;" }
			document.getElementById("y"+i).innerHTML = txtYear
		}
		nStartingYear --;
		bShow=true
	}

	function selectYear(nYear) {
		yearSelected=parseInt(nYear+nStartingYear);
		yearConstructed=false;
		constructCalendar();
		popDownYear();
	}

	function constructYear() {
		popDownMonth()
		sHTML =	""
		if (!yearConstructed) {

			sHTML =	"<tr><td align='center'	onmouseover='this.style.backgroundColor=\"#FFCC99\"' onmouseout='clearInterval(intervalID1);this.style.backgroundColor=\"\"' style='cursor:pointer'	onmousedown='clearInterval(intervalID1);intervalID1=setInterval(\"decYear()\",30)' onmouseup='clearInterval(intervalID1)'>-</td></tr>"

			var j =	0
			nStartingYear =	yearSelected-3
			for	(i=(yearSelected-3); i<=(yearSelected+3); i++) {
				sName =	i;
				if (i==yearSelected){
					sName =	"<B>" +	sName +	"</B>"
				}

				sHTML += "<tr><td id='y" + j + "' onmouseover='this.style.backgroundColor=\"#FFCC99\"' onmouseout='this.style.backgroundColor=\"\"' style='cursor:pointer' onclick='selectYear("+j+");event.cancelBubble=true'>&nbsp;" + sName + "&nbsp;</td></tr>"
				j ++;
			}

			sHTML += "<tr><td align='center' onmouseover='this.style.backgroundColor=\"#FFCC99\"' onmouseout='clearInterval(intervalID2);this.style.backgroundColor=\"\"' style='cursor:pointer' onmousedown='clearInterval(intervalID2);intervalID2=setInterval(\"incYear()\",30)'	onmouseup='clearInterval(intervalID2)'>+</td></tr>"

			document.getElementById("selectYear").innerHTML	= "<table width=44 style='font-family:arial; font-size:11px; border-width:1; border-style:solid; border-color:#a0a0a0;'	bgcolor='#FFFFDD' onmouseover='clearTimeout(timeoutID2)' onmouseout='clearTimeout(timeoutID2);timeoutID2=setTimeout(\"popDownYear()\",100)' cellspacing=0>"	+ sHTML	+ "</table>"

			yearConstructed	= true
		}
	}

	function popDownYear() {
		clearInterval(intervalID1)
		clearTimeout(timeoutID1)
		clearInterval(intervalID2)
		clearTimeout(timeoutID2)
		crossYearObj.visibility= "hidden"
	}

	function popUpYear() {
		var	leftOffset

		constructYear()
		crossYearObj.visibility	= (dom||ie)? "visible" : "show"
		leftOffset = parseInt(crossobj.left) + document.getElementById("spanYear").offsetLeft
		if (ie)
		{
			leftOffset += 6
		}
		crossYearObj.left =	leftOffset
		crossYearObj.top = parseInt(crossobj.top) +	26
	}

	/*** calendar ***/
   function WeekNbr(n) {
      // Algorithm used:
      // From Klaus Tondering's Calendar document (The Authority/Guru)
      // hhtp://www.tondering.dk/claus/calendar.html
      // a = (14-month) / 12
      // y = year + 4800 - a
      // m = month + 12a - 3
      // J = day + (153m + 2) / 5 + 365y + y / 4 - y / 100 + y / 400 - 32045
      // d4 = (J + 31741 - (J mod 7)) mod 146097 mod 36524 mod 1461
      // L = d4 / 1460
      // d1 = ((d4 - L) mod 365) + L
      // WeekNumber = d1 / 7 + 1
 
      year = n.getFullYear();
      month = n.getMonth() + 1;
      if (startAt == 0) {
         day = n.getDate() + 1;
      }
      else {
         day = n.getDate();
      }
 
      a = Math.floor((14-month) / 12);
      y = year + 4800 - a;
      m = month + 12 * a - 3;
      b = Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400);
      J = day + Math.floor((153 * m + 2) / 5) + 365 * y + b - 32045;
      d4 = (((J + 31741 - (J % 7)) % 146097) % 36524) % 1461;
      L = Math.floor(d4 / 1460);
      d1 = ((d4 - L) % 365) + L;
      week = Math.floor(d1/7) + 1;
 
      return week;
   }

	function constructCalendar () {
		var aNumDays = Array (31,0,31,30,31,30,31,31,30,31,30,31)

		var dateMessage
		var	startDate =	new	Date (yearSelected,monthSelected,1)
		var endDate

		if (monthSelected==1)
		{
			endDate	= new Date (yearSelected,monthSelected+1,1);
			endDate	= new Date (endDate	- (24*60*60*1000));
			numDaysInMonth = endDate.getDate()
		}
		else
		{
			numDaysInMonth = aNumDays[monthSelected];
		}

		datePointer	= 0
		dayPointer = startDate.getDay() - startAt
		
		if (dayPointer<0)
		{
			dayPointer = 6
		}

		sHTML =	"<table	 border=0 align=center style='font-family:verdana;font-size:10px;'><tr>"

	//	if (showWeekNumber==1)
	//	{
	//		sHTML += "<td width=27><b>" + weekString + "</b></td><td width=1 rowspan=7 bgcolor='#d0d0d0' style='padding:0px'><img src='"+imgDir+"divider.gif' width=1></td>"
	//	}

		for	(i=0; i<7; i++)	{
			sHTML += "<td width='22' align='right'><B>"+ dayName[i]+"</B></td>"
		}
		sHTML +="</tr><tr>"
		
	/*	if (showWeekNumber==1)
		{
			sHTML += "<td align=right>" + WeekNbr(startDate) + "&nbsp;</td>"
		}
*/

		for	( var i=1; i<=dayPointer;i++ )
		{
			sHTML += "<td>&nbsp;</td>"
		}
	
		for	( datePointer=1; datePointer<=numDaysInMonth; datePointer++ )
		{
			dayPointer++;
			sHTML += "<td align=right>"
			sStyle=styleAnchor
			if ((datePointer==odateSelected) &&	(monthSelected==omonthSelected)	&& (yearSelected==oyearSelected))
			{ sStyle+=styleLightBorder }

			sHint = ""
			for (k=0;k<HolidaysCounter;k++)
			{
				if ((parseInt(Holidays[k].d)==datePointer)&&(parseInt(Holidays[k].m)==(monthSelected+1)))
				{
					if ((parseInt(Holidays[k].y)==0)||((parseInt(Holidays[k].y)==yearSelected)&&(parseInt(Holidays[k].y)!=0)))
					{
						sStyle+="background-color:#FFDDDD;"
						sHint+=sHint==""?Holidays[k].desc:"\n"+Holidays[k].desc
					}
				}
			}

			var regexp= /\"/g
			sHint=sHint.replace(regexp,"&quot;")

			dateMessage = "onmousemove='window.status=\""+selectDateMessage.replace("[date]",constructDate(datePointer,monthSelected,yearSelected))+"\"' onmouseout='window.status=\"\"' "

			if ((datePointer==dateNow)&&(monthSelected==monthNow)&&(yearSelected==yearNow))
			{ sHTML += "<b><a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' href='javascript:dateSelected="+datePointer+";closeCalendar();'><font color=#ff0000>&nbsp;" + datePointer + "</font>&nbsp;</a></b>"}
			else if	(dayPointer % 7 == (startAt * -1)+1)
			{ sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' href='javascript:dateSelected="+datePointer + ";closeCalendar();'>&nbsp;<font color=#909090>" + datePointer + "</font>&nbsp;</a>" }
			else
			{ sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' href='javascript:dateSelected="+datePointer + ";closeCalendar();'>&nbsp;" + datePointer + "&nbsp;</a>" }

			sHTML += ""
			if ((dayPointer+startAt) % 7 == startAt) { 
				sHTML += "</tr><tr>" 
				//if ((showWeekNumber==1)&&(datePointer<numDaysInMonth))
				//{
				//	sHTML += "<td align=right>" + (WeekNbr(new Date(yearSelected,monthSelected,datePointer+1))) + "&nbsp;</td>"
				//}
			}
		}

		document.getElementById("content").innerHTML   = sHTML
		document.getElementById("spanMonth").innerHTML = "&nbsp;" +	monthName[monthSelected] + "&nbsp;<IMG id='changeMonth' SRC='"+imgDir+"drop1.gif' WIDTH='12' HEIGHT='10' BORDER=0>"
		document.getElementById("spanYear").innerHTML =	"&nbsp;" + yearSelected	+ "&nbsp;<IMG id='changeYear' SRC='"+imgDir+"drop1.gif' WIDTH='12' HEIGHT='10' BORDER=0>"
	}

	function popUpCalendar(ctl,	ctl2, format) {
		var	leftpos=0
		var	toppos=0
		tempcal = ctl2;
		if (bPageLoaded)
		{
			if ( crossobj.visibility ==	"hidden" ) {
				ctlToPlaceValue	= ctl2
				dateFormat=format;

				formatChar = " "
				aFormat	= dateFormat.split(formatChar)
				if (aFormat.length<3)
				{
					formatChar = "/"
					aFormat	= dateFormat.split(formatChar)
					if (aFormat.length<3)
					{
						formatChar = "."
						aFormat	= dateFormat.split(formatChar)
						if (aFormat.length<3)
						{
							formatChar = "-"
							aFormat	= dateFormat.split(formatChar)
							if (aFormat.length<3)
							{
								// invalid date	format
								formatChar=""
							}
						}
					}
				}

				tokensChanged =	0
				if ( formatChar	!= "" )
				{
					// use user's date
					aData =	ctl2.value.split(formatChar)

					for	(i=0;i<3;i++)
					{
						if ((aFormat[i]=="d") || (aFormat[i]=="dd"))
						{
							dateSelected = parseInt(aData[i], 10)
							tokensChanged ++
						}
						else if	((aFormat[i]=="m") || (aFormat[i]=="mm"))
						{
							monthSelected =	parseInt(aData[i], 10) - 1
							tokensChanged ++
						}
						else if	(aFormat[i]=="yyyy")
						{
							yearSelected = parseInt(aData[i], 10)
							tokensChanged ++
						}
						else if	(aFormat[i]=="mmm")
						{
							for	(j=0; j<12;	j++)
							{
								if (aData[i]==monthName[j])
								{
									monthSelected=j
									tokensChanged ++
								}
							}
						}
						else if	(aFormat[i]=="mmmm")
						{
							for	(j=0; j<12;	j++)
							{
								if (aData[i]==monthName2[j])
								{
									monthSelected=j
									tokensChanged ++
								}
							}
						}
					}
				}

				if ((tokensChanged!=3)||isNaN(dateSelected)||isNaN(monthSelected)||isNaN(yearSelected))
				{
					dateSelected = dateNow
					monthSelected =	monthNow
					yearSelected = yearNow
				}

				odateSelected=dateSelected
				omonthSelected=monthSelected
				oyearSelected=yearSelected

				aTag = ctl
				do {
					aTag = aTag.offsetParent;
					leftpos	+= aTag.offsetLeft;
					toppos += aTag.offsetTop;
				} while(aTag.tagName!="BODY");

				crossobj.left =	fixedX==-1 ? ctl.offsetLeft	+ leftpos :	fixedX
				crossobj.top = fixedY==-1 ?	ctl.offsetTop +	toppos + ctl.offsetHeight +	2 :	fixedY
				constructCalendar (1, monthSelected, yearSelected);
				crossobj.visibility=(dom||ie)? "visible" : "show"

				hideElement( 'SELECT', document.getElementById("calendar") );
				hideElement( 'APPLET', document.getElementById("calendar") );			

				bShow = true;
			}
			else
			{
				hideCalendar()
				if (ctlNow!=ctl) {popUpCalendar(ctl, ctl2, format)}
		}
	
			ctlNow = ctl

		}
	}

	document.onkeypress = function hidecal1 () { 
		if (event.keyCode==27) 
		{

			hideCalendar()
		}
	}
	document.onclick = function hidecal2 () { 		
		if (!bShow)
		{
			hideCalendar()
		}
		bShow = false
	}

	if(ie)
	{
		init()
	}
	else
	{
		window.onload=init
	}
</script>

<script type="text/javascript" language="JavaScript">

var cX = 0; var cY = 0; var rX = 0; var rY = 0;
function UpdateCursorPosition(e){ cX = e.pageX; cY = e.pageY;}
function UpdateCursorPositionDocAll(e){ cX = event.clientX; cY = event.clientY;}
if(document.all) { document.onmousemove = UpdateCursorPositionDocAll; }
else { document.onmousemove = UpdateCursorPosition; }
function AssignPosition(d) {
if(self.pageYOffset) {
	rX = self.pageXOffset;
	rY = self.pageYOffset;
	}
else if(document.documentElement && document.documentElement.scrollTop) {
	rX = document.documentElement.scrollLeft;
	rY = document.documentElement.scrollTop;
	}
else if(document.body) {
	rX = document.body.scrollLeft;
	rY = document.body.scrollTop;
	}
if(document.all) {
	cX += rX; 
	cY += rY;
	}
d.style.left = (cX+10) + "px";
d.style.top = (cY+10) + "px";
}
function HideContent(d) {
if(d.length < 1) { return; }
document.getElementById(d).style.display = "none";
}
function ShowContent(d) {
if(d.length < 1) { return; }
var dd = document.getElementById(d);
AssignPosition(dd);
dd.style.display = "block";
}
function ReverseContentDisplay(d) {
if(d.length < 1) { return; }
var dd = document.getElementById(d);
AssignPosition(dd);
if(dd.style.display == "none") { dd.style.display = "block"; }
else { dd.style.display = "none"; }
}
</script>
<style>
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 12px;
    	cursor : auto;
    }
    
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    
    .toBeScheduled1, .scheduled1, .inwork1, .complete1, .closed1 {
      	font-size : 9pt;
		font-weight : bold;
		padding : 2px 6px 0px 6px;
		text-align : center;
    
    }
    .toBeScheduled1 {
		background-color : #fff1f0; 
    }
    .scheduled1 {
		background-color : #fffced; 
    }
    .inwork1 {
		background-color : #e0edf5; 
    }
    .complete1 {
		background-color : #ffccc9; 
    }
    .closed1 {
		background-color : #e5f9de; 
    }
    
    .toBeScheduled2, .scheduled2, .inwork2, .complete2, .closed2 {
      	font-size : 8pt;
		font-weight : Normal;
		padding : 1px 3px 1px 3px;
		text-align : center;
    
    }
    .toBeScheduled2 {
		background-color : #fff1f0; 
    }
    .scheduled2 {
		background-color : #fffced; 
    }
    .inwork2 {
		background-color : #e0edf5; 
    }
    .complete2 {
		background-color : #ffccc9; 
    }
    .closed2 {
		background-color : #e5f9de; 
    }
    
    
    .menu {
		font-size : 8pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 2px 0px 0px 1px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
		text-align : center;
		width: 26px;
        height: 24px;
    }
    .date1 {
        font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
    }


    .titleLine {
		font-size : 11pt;
		font-weight : bold;
		text-align : left;
		vertical-align : Middle;
		padding : 0px 0px 0px 0px;
    } 
    
    .subTitleLine {
		font-size : 10pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 0px 0px 0px 0px;
    }
 
    .detailTitleLine {
		font-size : 9pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 0px 0px 5px 0px;
    }
 
     .formLabels {
        font-size : 8pt;
		font-weight : normal;
		padding : 1px 0px 1px 0px;
    }
 
 
    .label1, .label2 {
        font-size : 8pt;
		font-weight : normal;
		text-align : right;
		vertical-align : top;
		padding : 1px 0px 1px 0px;
    }
    .formLabels {
        font-size : 9pt;
		font-weight : normal;
		padding : 0px 0px 0px 0px;
    }
    .elementContainer {
    padding : 0px 2px 0px 2px;
    }
    .columnHeader1 {
		font-size : 9pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 3px 10px 3px 10px;
		background-color: #E0E0DC;
    }    
    .columnHeader2 {
		font-size : 9pt;
		font-weight : normal;
		text-align : center;
		vertical-align : middle;
		padding : 3px 10px 3px 10px;
		background-color: #E0E0DC;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 9pt;
		font-weight : normal;
		text-align : left;
		vertical-align : middle;
		padding : 1px 4px 1px 4px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #fffced;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ffffff;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }        
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
		text-align : center;
		width: 26px;
        height: 24px;
    }
    .date1 {
        font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
    }
    .bottomLine, .bottomLineCenter {
    	font-size : 8pt;
		font-weight : bold;
		text-align : right;
		vertical-align : middle;
		padding : 2px 2px 2px 0px;
		background-color: #e0edf5;
    }
    .bottomLineCenter {
		text-align : center;
    }
.rptBody01_iframe {
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
}

.ledgendTitle {
        font-size : 9pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 3px 10px 3px 5px;
}

.inboundsTitle {
        font-size : 9pt;
		font-weight : bold;
		color: Green;
		text-align : left;
		vertical-align : top;
		padding : 3px 10px 3px 10px;
}

.outboundsTitle {
        font-size : 9pt;
		font-weight : bold;
		color: Red;
		text-align : left;
		vertical-align : top;
		padding : 3px 10px 3px 10px;
	
}

.inbounds {
        font-weight : Normal;
		padding : 1px 3px 1px 3px;
		text-align : center;
		background-color : #e5f9de;
	    a:link      { color:black; text-decoration:none; }
        a:visited   { color:black; text-decoration:none; }
        a:hover     { color:black; text-decoration:none;}
        a:active    { color:black; text-decoration:none; }		
}

.outbounds {
        font-weight : Normal;
		padding : 1px 3px 1px 3px;
		text-align : center;
		background-color : #ffccc9;
	    a:link      { color:black; text-decoration:none; }
        a:visited   { color:black; text-decoration:none; }
        a:hover     { color:black; text-decoration:none;}
        a:active    { color:black; text-decoration:none; }		
}

.outboundshelp {
        font-size : 9pt;
		font-weight : bold;
		color: Red;
		text-align : left;
		vertical-align : top;
		padding : 3px 10px 3px 10px;	    
		a:link      { color:black; text-decoration:none; }
        a:visited   { color:black; text-decoration:none; }
        a:hover     { color:black; text-decoration:none;}
        a:active    { color:black; text-decoration:none; }		
}
</style>
</head>
<%
	String[] count = (String[]) request.getAttribute("count");

	//Set data list for forms
	String grouping_menu = "select distinct ap_tl_ot_name+'|'+ap_tl_pr_title+'|'+convert(varchar(10),ap_tl_pr_id) from ap_top_level_summary order by ap_tl_ot_name+'|'+ap_tl_pr_title+'|'+convert(varchar(10),ap_tl_pr_id)";
	String bdm_list = "select distinct convert(varchar(12),lo_pc_id)+'|'+lo_pc_first_name+' '+lo_pc_last_name, lo_pc_last_name from lo_poc join lo_poc_rol on lo_pc_id = lo_pr_pc_id and lo_pr_ro_id in (1,2,3,4) where lo_pc_id in (select distinct lx_pr_cns_poc from lx_appendix_main where lx_pr_cns_poc not in (7,32)) order by lo_pc_last_name";
	String pm_list = "select distinct convert(varchar(12),lo_pc_id)+'|'+lo_pc_first_name+' '+lo_pc_last_name, lo_pc_last_name from lo_poc join lo_poc_rol on lo_pc_id = lo_pr_pc_id and lo_pr_ro_id in (5,7,8,12,16,19,20) where lo_pc_id in (select distinct lm_ap_pc_id from lm_appendix_poc where lm_ap_pc_id not in (7,32)) order by lo_pc_last_name";
	String jo_list = "select  distinct convert(varchar(12),lo_pc_id)+'|'+lo_pc_first_name+' '+lo_pc_last_name, lo_pc_last_name from dbo.ap_job_audit_analysis join lo_poc on ap_aa_owner_id = lo_pc_id where (lo_pc_active_user = 1 or lo_pc_termination_date > dateadd(dd, -45, getdate())) and lo_pc_role in (3, 5, 6, 10) order by lo_pc_last_name";

	// Get page type display parameters
	String view_title = (String) request.getAttribute("view_title");

	// Build link and hidden values based on input parameters
	String view_link = "";
	
	// Build view links to include current filter values
    Vector requestParameters = (Vector)request.getAttribute("requestParameters");
    String delimiter = "&";
    String parameter = "";
    String value = "";
    if (requestParameters != null) {
    	for (int x=0; x<requestParameters.size(); x++) {
    		parameter = (String)requestParameters.get(x);
    		if (!parameter.equals("view")) {
        		value = (request.getAttribute(parameter) != null ? (String)request.getAttribute(parameter) : "");
        		if (!value.equals("")) {
        		  view_link += delimiter+parameter+"="+value;
        		}	
    		}
    	}
    }
    
    
	String lx_pr_id = (String)request.getAttribute("lx_pr_id");
	if (lx_pr_id == null) {
		lx_pr_id = "";
	} 

	String bdm = (String) request.getAttribute("bdm");
	if (bdm == null) {
		bdm = "";
	} 

	String pm = (String) request.getAttribute("pm");
	if (pm == null) {
		pm = "";
	}

	String jo = (String) request.getAttribute("jo");
	if (jo == null) {
		jo = "";
	}

	String from_date = (String) request.getAttribute("from_date");
	if (from_date == null) {
		from_date = "";
	} 

	String to_date = (String) request.getAttribute("to_date");
	if (to_date == null) {
		to_date = "";
	} 
	
	String Subtitle = (String) request.getAttribute("Subtitle");
	if (Subtitle == null) {
		Subtitle = "";
	} 
	
%>

<body>
<script type="text/javascript">

function setSubtitle(title) {
  var subtitle01 = document.getElementById("subtitle");
  subtitle01.innerHTML = title;
  return;
}

</script>
<div class="rptBody01">
<table border="0" cellpadding="0" cellspacing="0">
<form name="form1" action="JSA.xo" method="post">
<input type="hidden" name="form_action", value="jsa_summary">

<tr>
 <td>
 <table width="859" border="0" cellspacing="0" cellpadding="0">
 
   <tr>
   <td rowspan="4" class="titleLine">Job Audit</td> 
   
   <td align="right" class="label1">Project:&nbsp;&nbsp;<el:element type="groupingmenu1" request="<%=request%>" name="lx_pr_id" source="database" data ="<%=grouping_menu%>" js=""/></td>
   </tr>
   
   <tr>
   <td align="right" class="label1">Business Development Manager:&nbsp;&nbsp;<el:element type="menu2" request="<%=request%>" name="bdm" source="database" data ="<%=bdm_list%>" js=""/></td>
   </tr>
   
   <tr>
   <td align="right" class="label1">Project Manager:&nbsp;&nbsp;<el:element type="menu2" request="<%=request%>" name="pm" source="database" data ="<%=pm_list%>" js=""/></td>
   </tr>

   <tr>
   <td align="right" class="label1">Job Owner:&nbsp;&nbsp;<el:element type="menu2" request="<%=request%>" name="jo" source="database" data ="<%=jo_list%>" js=""/></td>
   </tr>
   
   <tr>
   <td class="subTitleLine">Active Jobs: <%=count[18]%></td> 
   <td align="right" class="label1">From:&nbsp;&nbsp;<el:element type="date1" request="<%=request%>" name="from_date" source="" js="" data=""/>&nbsp;&nbsp;&nbsp;To:&nbsp;&nbsp;&nbsp;<el:element type="date1" request="<%=request%>" name="to_date" source="" js="" data=""/>&nbsp;&nbsp;<input type="submit" value="Go" class=date1></td>
   </tr>
 </table> </td>
</tr>
</form>

<tr>
  <td>&nbsp;</td>
</tr>

<tr>
  <td><table border="0" cellpadding="0" cellspacing="1">
  
     <tr>
      <td colspan="10" class="columnHeader1">Operations</td>
      <td colspan="3" class="columnHeader1">Accounting</td>
    </tr>
  
    <tr>
      <td colspan="2" class="columnHeader1">To Be Scheduled</td>
      <td colspan="2" class="columnHeader1">Scheduled</td>
      <td colspan="6" class="columnHeader1">In Work</td>
      <td colspan="3" class="columnHeader1">Complete</td>
 
    </tr>
    <tr>
      <td colspan="2" class="columnHeader1"><%=count[14]%></td>
      <td colspan="2" class="columnHeader1"><%=count[15]%></td>
      <td colspan="6" class=columnHeader1><%=count[16]%></td>
      <td colspan="3" class="columnHeader1"><%=count[17]%></td>
    </tr>
    
    <tr>
      <td width="40" class="inbounds">OK</td>
      <td width="45" class="outbounds"><a onmousemove="ShowContent('title1'); return true;" onmouseover="ShowContent('title1'); return true;" onmouseout="HideContent('title1'); return true;" 
      class="outbounds" style="cursor:pointer">Over-1</a><div id="title1" style="display:none; position:absolute; border-style: solid; border-width: thin; background-color: #ffccc9; padding: 5px;">The user has not yet recorded the &quot;Schedule Date&quot;.</div></td>
      <td width="40" class="inbounds">OK</td>
      <td width="45" class="outbounds"><a onmousemove="ShowContent('title2'); return true;" onmouseover="ShowContent('title2'); return true;" onmouseout="HideContent('title2'); return true;" 
      class="outbounds" style="cursor:pointer">Over-1</a><div id="title2" style="display:none; position:absolute; border-style: solid; border-width: thin; background-color: #ffccc9; padding: 5px; text-align : left;">Either the user failed to record the &quot;Onsite Date and Time&quot; when the field personnel arrived<br>or the visit was delayed and the user has yet to adjust the &quot;Schedule Date&quot; to reflect the change.</div></td>
      
      <td width="45" class="outbounds"><a onmousemove="ShowContent('title2a'); return true;" onmouseover="ShowContent('title2a'); return true;" onmouseout="HideContent('title2a'); return true;" 
      class="outbounds" style="cursor:pointer">Confirmed</a><div id="title2a" style="display:none; position:absolute; border-style: solid; border-width: thin; background-color: #ffccc9; padding: 5px; text-align : left;">PO over 1</div></td>
 
      <td width="40" class="inbounds">Onsite</td>
      <td width="85" class="outbounds"><a onmousemove="ShowContent('title3'); return true;" onmouseover="ShowContent('title3'); return true;" onmouseout="HideContent('title3'); return true;" 
      class="outbounds" style="cursor:pointer">Onsite Over-1</a><div id="title3" style="display:none; position:absolute; border-style: solid; border-width: thin; background-color: #ffccc9; padding: 5px; text-align : left;">Either the field personnel have worked more than 24 hours straight at the site or the user<BR>failed to record the &quot;Offsite&quot; Date and Time when the field personnel actually left the site.</div></td>
      <td width="40" class="inbounds">Offsite</td>
      <td width="80" class="outbounds"><a onmousemove="ShowContent('title4'); return true;" onmouseover="ShowContent('title4'); return true;" onmouseout="HideContent('title4'); return true;" 
      class="outbounds" style="cursor:pointer">Offsite Over-2</a><div id="title4" style="display:none; position:absolute; border-style: solid; border-width: thin; background-color: #ffccc9; padding: 5px; text-align : left;">The user has failed to change the job or ticket status to &quot;Complete&quot;. The reasons may be because the<br>user forgot or is waiting for deliverables or other information required to change status.<br>However, there are no justifiable reasons for a job or ticket to fall into this category.</div></td>
      <td width="80" class="outbounds"><a onmousemove="ShowContent('title5'); return true;" onmouseover="ShowContent('title5'); return true;" onmouseout="HideContent('title5'); return true;" 
      class="outbounds" style="cursor:pointer">Offsite Del>7</a><div id="title5" style="display:none; position:absolute; border-style: solid; border-width: thin; background-color: #ffccc9; padding: 5px; text-align : left;">The user has failed to change the job or ticket status to &quot;Complete&quot; because the user is waiting for deliverables required to change status.<br>However, there are no justifiable reasons for a job or ticket to fall into this category.</div></td>
      <td width="40" class="inbounds">OK</td>
      <td width="80" class="outbounds"><a onmousemove="ShowContent('title6'); return true;" onmouseover="ShowContent('title6'); return true;" onmouseout="HideContent('title6'); return true;" 
      class="outbounds" style="cursor:pointer">Over-5</a><div id="title6" style="display:none; position:absolute; border-style: solid; border-width: thin; background-color: #ffccc9; padding: 5px; text-align : left;">Accounting has not invoiced and there for changed the status to &quot;Closed&quot;.<BR> This happens when the accounting team is behind on invoicing or because there is something<BR> wrong the job or ticket record that is preventing them from invoicing for the work.</div></td>
      <td width="40" class="outbounds"><a onmousemove="ShowContent('title7'); return true;" onmouseover="ShowContent('title7'); return true;" onmouseout="HideContent('title7'); return true;" 
      class="outbounds" style="cursor:pointer">Problems</a><div id="title7" style="display:none; position:absolute; border-style: solid; border-width: thin; background-color: #ffccc9; padding: 5px; text-align : left;">Operations user has not included this information that is required to<BR> invoice for the work. This happens because the user forgot to record the<BR> data and/or the data was not available at the time the user changed the<BR> status to &quot;Complete&quot;. However, there are no justifiable reasons for a job<BR> or ticket to fall into this category. Note that a job or ticket can<BR> occupy more than one &quot;Complete&quot; status simultaneously.</div></td>
    </tr>

    <tr>
      <td width="40" class="inbounds"><a href="JSA.xo?form_action=inline_audit_view&view=0<%=view_link%>" target="i1" onclick="setSubtitle('To Be Scheduled OK');document.getElementById('id1').style.display = 'inline';"><%=count[0]%></a></td>
      <td width="40" class="outbounds"><a href="JSA.xo?form_action=inline_audit_view&view=1<%=view_link%>" target="i1" onclick="setSubtitle('To Be Scheduled Over-1');document.getElementById('id1').style.display = 'inline'" alt="The user has not yet \nrecorded the Schedule Date"><%=count[1]%></a></td>
      <td width="40" class="inbounds"><a href="JSA.xo?form_action=inline_audit_view&view=2<%=view_link%>" target="i1" onclick="setSubtitle('Scheduled OK');document.getElementById('id1').style.display = 'inline'"><%=count[2]%></a></td>
      <td width="40" class="outbounds"><a href="JSA.xo?form_action=inline_audit_view&view=3<%=view_link%>" target="i1" onclick="setSubtitle('Scheduled Over-1');document.getElementById('id1').style.display = 'inline'"><%=count[3]%></a></td>
      <td width="40" class="outbounds"><a href="JSA.xo?form_action=inline_audit_view&view=19<%=view_link%>" target="i1" onclick="setSubtitle('Confirmed');document.getElementById('id1').style.display = 'inline'"><%=count[19]%></a></td>
      <td width="40" class="inbounds"><a href="JSA.xo?form_action=inline_audit_view&view=4<%=view_link%>" target="i1" onclick="setSubtitle('Onsite OK');document.getElementById('id1').style.display = 'inline'"><%=count[4]%></a></td>
      <td width="75" class="outbounds"><a href="JSA.xo?form_action=inline_audit_view&view=5<%=view_link%>" target="i1" onclick="setSubtitle('Onsite Over-1');document.getElementById('id1').style.display = 'inline'"><%=count[5]%></a></td>
      <td width="40" class="inbounds"><a href="JSA.xo?form_action=inline_audit_view&view=6<%=view_link%>" target="i1" onclick="setSubtitle('Offsite OK');document.getElementById('id1').style.display = 'inline'"><%=count[6]%></a></td>
      <td width="75" class="outbounds"><a href="JSA.xo?form_action=inline_audit_view&view=7<%=view_link%>" target="i1" onclick="setSubtitle('Offsite Over-2');document.getElementById('id1').style.display = 'inline'"><%=count[7]%></a></td>
      <td width="75" class="outbounds"><a href="JSA.xo?form_action=inline_audit_view&view=8<%=view_link%>" target="i1" onclick="setSubtitle('Offsite Del>7');document.getElementById('id1').style.display = 'inline'"><%=count[8]%></a></td>      
      <td width="40" class="inbounds"><a href="JSA.xo?form_action=inline_audit_view&view=9<%=view_link%>" target="i1"   onclick="setSubtitle('Complete OK (I=Installation Notes, C=Customer Reference, and D=Deliverables)');    document.getElementById('id1').style.display = 'inline'"><%=count[9]%></a></td>
      <td width="75" class="outbounds"><a href="JSA.xo?form_action=inline_audit_view&view=11<%=view_link%>" target="i1" onclick="setSubtitle('Complete Over-5 (I=Installation Notes, C=Customer Reference, and D=Deliverables)');document.getElementById('id1').style.display = 'inline'"><%=count[11]%></a></td>
      <td width="75" class="outbounds"><a href="JSA.xo?form_action=inline_audit_view&view=10<%=view_link%>" target="i1" onclick="setSubtitle('Complete Over-5 (I=Installation Notes, C=Customer Reference, and D=Deliverables)');document.getElementById('id1').style.display = 'inline'"><%=count[10]%></a></td>
    </tr>    
  </table></td>
</tr>

<tr>
 <td>&nbsp;</td>
</tr>

<tr>
 <td><div id="subtitle" class="detailTitleLine"></div></td>
</tr>

<tr>
 <td><table width="1015" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="220" class="columnHeader2">Customer</td>
    <td width="220" class="columnHeader2">Project</td>
    <td width="234" class="columnHeader2">Job/Ticket</td>
    <td width="50"  class="columnHeader2">Days</td>
    <td width="59"  class=columnHeader2>Date</td>
    <td width="110" class="columnHeader2">Owner</td>
    <td width="110" class="columnHeader2">PM</td>
  </tr>
</table></td>
</tr>

<tr>
  <td><div id="id1" style="display:none"><iframe class="rptBody01_iframe" name="i1" src="#"  width="1015" height="450" scrolling="auto" frameborder="0"></iframe></div></td>
</tr>

<tr>
 <td><table width="815" border="0" cellspacing="0" cellpadding="0">
 
  <tr>
    <td colspan="2" class="">&nbsp;</td>
  </tr>
 
  <tr>
    <td colspan="2" class="ledgendTitle"><hr height="1" width="100%"></td>
  </tr>
 
  <tr>
    <td colspan="2" class="ledgendTitle">Overall Legend</td>
  </tr>
  
  <tr>
    <td colspan="2"><span class="outboundsTitle">Red Status</span>&nbsp;&nbsp;Indicates a job or ticket is experiencing billing lag and is "out of bounds"</td>
  </tr>
  
  <tr>
    <td colspan="2"><span class="inboundsTitle">Green Status</span>&nbsp;&nbsp;Indicates a job or ticket is following the prescribed process and is "in bounds"</td>
  </tr>
  
  <tr>
    <td colspan="2" class="">&nbsp;</td>
  </tr>
  
  <tr>
    <td colspan="2" class="ledgendTitle">Operations Legend</td>
  </tr>

  <tr>
    <td width=180" class="inboundsTitle">To&nbsp;Be&nbsp;Scheduled&nbsp;-&nbsp;OK:</td>
    <td>Jobs or tickets that have been created in Ilex that are awaiting a "Schedule Date" to be added to the appropriate field</td>
  </tr>
  
  <tr>
    <td class="outboundshelp"><a onmousemove="ShowContent('title1'); return true;" onmouseover="ShowContent('title1'); return true;" onmouseout="HideContent('title1'); return true;" style="cursor:pointer">To&nbsp;Be&nbsp;Scheduled&nbsp;-&nbsp;Over&nbsp;1</a>:</td>
    <td>Jobs or tickets that have been created in Ilex for more than one day without having a "Schedule Date" added to the appropriate field.</td>
  </tr>
  
  <tr>
    <td class="inboundsTitle">Scheduled&nbsp;-&nbsp;OK:</td>
    <td>Jobs or tickets that have been created in Ilex and have a "Scheduled Date" in the appropriate field that is sometime in the future.</td>
  </tr>

  <tr>
    <td class="outboundshelp"><a onmousemove="ShowContent('title2'); return true;" onmouseover="ShowContent('title2'); return true;" onmouseout="HideContent('title2'); return true;" style="cursor:pointer">Scheduled&nbsp;-&nbsp;Over&nbsp;1</a>:</td><td>Jobs or tickets whose recorded "Schedule Date" has past at least one day ago without having an "Onsite Time and Date" added to the appropriate field.</td>
  </tr>
  
  <tr>
    <td class="outboundshelp"><a onmousemove="ShowContent('title2a'); return true;" onmouseover="ShowContent('title2a'); return true;" onmouseout="HideContent('title2a'); return true;" style="cursor:pointer">Confirmed</a>:</td><td>A scheduled over-1 job or tickets with an assigned PO but not yet marked onsite.</td>
  </tr>
  
  <tr>
    <td class="inboundsTitle">In&nbsp;Work&nbsp;-&nbsp;Onsite:</td><td>Field personnel have arrived and are currently onsite.</td>
  </tr>

  <tr>
    <td class="outboundshelp"><a onmousemove="ShowContent('title3'); return true;" onmouseover="ShowContent('title3'); return true;" onmouseout="HideContent('title3'); return true;" style="cursor:pointer">In&nbsp;Work&nbsp;-&nbsp;Onsite&nbsp;Over&nbsp;1</a>:</td><td>Field personnel have arrived and are currently onsite at least one day after the recorded "Onsite Date and Time".</td>
  </tr>

  <tr>
    <td class="inboundsTitle">In&nbsp;Work&nbsp;-&nbsp;Offsite:</td><td>Field personnel have left the site.</td>
  </tr>
  
  <tr>
    <td class="outboundshelp"><a onmousemove="ShowContent('title4'); return true;" onmouseover="ShowContent('title4'); return true;" onmouseout="HideContent('title4'); return true;" style="cursor:pointer">In&nbsp;Work&nbsp;-&nbsp;Offsite&nbsp;Over&nbsp;2</a>:</td><td>At least two days have elapsed since the "offsite Date and Time" were recorded.</td>
  </tr>

  <tr>
    <td class="outboundshelp"><a onmousemove="ShowContent('title5'); return true;" onmouseover="ShowContent('title5'); return true;" onmouseout="HideContent('title5'); return true;" style="cursor:pointer">In Work&nbsp;-&nbsp;Offsite&nbsp;Del>7</a>:</td><td>At least seven days have elapsed since the "offsite Date and Time" were recorded. </td>
  </tr>
  
  <tr>
    <td colspan="2" class="">&nbsp;</td>
  </tr>  
  
  <tr>
    <tr>
    <td colspan="2" class="ledgendTitle">Accounting Legend</td>
  </tr>
  
  <tr>
    <td class="inboundsTitle">Complete&nbsp;-&nbsp;OK:</td><td>Jobs or Tickets that have a "Complete" status and have been in the completed state for less than five days.</td>
  </tr>
  
  <tr>
    <td class="outboundshelp"><a onmousemove="ShowContent('title6'); return true;" onmouseover="ShowContent('title6'); return true;" onmouseout="HideContent('title6'); return true;" style="cursor:pointer">Complete&nbsp;-&nbsp;Over&nbsp;5</a>:</td><td>Jobs or tickets that have a "Complete" status and have been in the completed state for more than five days.</td>
  </tr>

  <tr>
    <td class="outboundshelp"><a onmousemove="ShowContent('title7'); return true;" onmouseover="ShowContent('title7'); return true;" onmouseout="HideContent('title7'); return true;" style="cursor:pointer">Complete&nbsp;-&nbsp;Problems</a>:</td>
    <td>Jobs or tickets that have a "Complete" status and that are missing installation notes, customer reference number or deliverables.</td>
  </tr>
    
</table></td>
</tr>


</table>
</div> 
</body>
</html>