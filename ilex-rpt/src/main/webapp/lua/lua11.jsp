<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.text.DecimalFormat" %>
<%@ include file="common_form_data.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=base_page_title%></title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<script language="JavaScript" src="lua/FusionCharts.js"></script>
</head>
<body class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<!-- Header -->
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr>
 <td rowspan="2"><div class="titleLine"><%=display_title%></div><div class="sectionTitleLine"><%=display_sub_title%></div></td>
 <td align="right"><%@ include file="controls.jsp"%></td>
 </tr>
 <tr>
 <td align="right"><%@ include file="navigation.jsp"%></td>
 </tr>
</table>
</td>
</tr>
<!-- Report Section Customize inside of the div tag -->
<tr>
<td>
<div style="padding:10px 0px 0px 0px;">
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
    <tr>
     <td class="columnHeader2">Customer</td>
     <td class="columnHeader2">Authorized<br>PO Cost</td>
     <td class="columnHeader2">Billable<br>Labor Qty</td>
     <td class="columnHeader2">Effective<br>Rate</td>
     <td class="columnHeader2">Count</td>
   </tr>
   <!-- Dynamic content -->
   <%=request.getAttribute("table_data")%>
</table>
</div>
</td>
</tr>
</table>
</body>
</html>