<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.text.DecimalFormat" %>
<%@ include file="common_form_data.jsp"%>
<style>
/*************************
Style Definitions

Font sizes: 10, 11, 12

Font weight: Normal and Bold

Color Scheme
  A = #8198CC Light Blue 
  B = #696969 Light Gray
  C = #000000 Black
  D = #F6F6F6 Gray
  E = #C00000 Red
  
Padding and Margin: As defined by class (p and m)

Borders:  As defined by class (b)

Alignment: By classes ac, ar, and al
*/

/* General */

td {
vertical-align: text-bottom;
}

.bgcD {
    background-color:#EAEAEA;
}

/* Text */

.txtA12N, .txtA11N, .txtB12N, .txtB11N, .txtC11N, .txtC10N {
   font-family : Arial, Helvetica, sans-serif;
   font-weight : normal;
}
.txtA12B, .txtA11B, .txtB12B, .txtB11B, .txtE11B {
   font-family : Arial, Helvetica, sans-serif;
   font-weight : bold;
}
/* Font Size */
.txtA12N, txtA12B, .txtB12N, txtB12B {
   font-size : 12px;
}
.txtA11N, .txtA11B, .txtB11N, .txtB11B, .txtC11N, .txtE11B {
   font-size : 11px;
}
.txtC10N {
   font-size : 10px;
}
/* Color */
.txtA12N, .txtA11N, .txtA12B, .txtA11B {
   color: #8198CC;
}
.txtB12N, .txtB11N, .txtB12B, .txtB11B {
   color: #606060;
}
.txtC10N, .txtC11N {
   color: #000000;
}

.txtE11B {
   color: #C00000;
}

/* Padding */

.p00001200 {
    padding : 0px 0px 12px 0px;
}
.p00121212 {
    padding : 0px 12px 12px 12px;
}
.p14000000 {
    padding : 14px 0px 0px 0px;
}
.p00111111 {
    padding : 0px 11px 11px 11px;
}
.p00001000 {
    padding : 0px 0px 10px 0px;
}
.p04120100 {
    padding : 4px 12px 1px 0px;
}
.p4818 {
    padding : 4px 8px 1px 8px;
}
.p4414 {
    padding : 4px 4px 1px 4px;
}
.p0050 {
    padding : 0px 0px 5px 0px;
}
.p5000 {
    padding : 5px 0px 0px 0px;
}
.p5005 {
    padding : 5px 0px 0px 5px;
}
.p5050
{
    padding : 5px 0px 5px 0px;
}
.p0055 {
    padding : 0px 0px 5px 5px;
}
.p0555 {
    padding : 0px 5px 5px 5px;
}
.p0040 {
    padding : 0px 0px 4px 0px;
}
.p3010 {
    padding : 3px 0px 1px 0px;
}
.p0010 {
    padding : 0px 0px 1px 0px;
}

/* Alignment */

.ar {
    text-align : right;
}
.ac {
    text-align : center;
}
.al {
    text-align : left;
}

/* Special */

.rule1BM1010 {
   margin : 1px 0px 1px 0px; 
   border-top : 1px solid #898989; 
}
.rule1AM1010 {
   margin : 1px 0px 1px 0px; 
   border-top : 1px solid #8198CC; 
}
.rule2BM1010 {
   margin : 1px 0px 1px 0px; 
   border-top : 1px solid #898989; 
   border-bottom : 1px solid #898989;
}
</style>
<div>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="2" class="txtB11B p0050">Job Information</td></tr>
<tr><td colspan="2">
  <table border="0" cellspacing="0" cellpadding="0">
  <tr>
        <td width="25%" class="txtB11N p0050">Client:</td>
        <td width="75%" class="txtC11N p0055"><%=request.getAttribute("Customer")%></td>
    </tr>
    <tr>
        <td width="25%" class="txtB11N p0050">Appendix/Job Title:</td>
        <td width="75%" class="txtC11N p0055"><%=request.getAttribute("JobTitle")%></td>
    </tr>
    <tr>
        <td class="txtB11N p0050">Offsite Date:</td>
        <td class="txtC11N p0055"><%=request.getAttribute("InvoiceDate")%></td>
    </tr>
    <tr>
        <td class="txtB11N p0050">Site Location:</td>
        <td class="txtC11N p0055"><%=request.getAttribute("SiteNumber")%></td>
    </tr>
    <%
    String criticality = (request.getAttribute("Criticality") != null) ? (String)request.getAttribute("Criticality") : "N/A" ;
    if (!criticality.equals("N/A")) {
    %>
     <tr>
        <td class="txtB11N p0050">Criticality:</td>
        <td class="txtC11N p0055"><%=criticality%></td>
     </tr>
    <% 
    }
    %>
    <%
    String non_pps = (request.getAttribute("NonPPS") != null) ? (String)request.getAttribute("NonPPS") : "No" ;
    if (!non_pps.equals("No")) {
    %>
     <tr>
        <td class="txtB11N p0050">Non-PPS:</td>
        <td class="txtC11N p0055">Yes</td>
     </tr>
    <% 
    }
    %>
    <tr>
    <td colspan="2">
    <table border="0" cellspacing="1" cellpadding="0">
    <tr>
      <td class="txtB11N p0050">PF Cost:</td><td class="txtC11N p0555">$<%=request.getAttribute("ProFormaCost")%></td>
      <td class="txtB11N p0050">Actual Cost:</td><td class="txtC11N p0555">$<%=request.getAttribute("ActualCost")%></td>
      <td class="txtB11N p0050">Revenue:</td><td class="txtC11N p0555">$<%=request.getAttribute("Revenue")%></td>
      <td class="txtB11N p0050">VGPM:</td><td class="txtC11N p0555"><%=request.getAttribute("VGPM")%></td>
      <td class="txtB11N p0050">PF VGPM:</td><td class="txtC11N p0555"><%=request.getAttribute("PGPM")%></td>
    </tr>
    </table>
    </td>
    </tr>
 </table>
</td></tr>

<tr><td colspan="2" class="txtB11B p5050">Labor Resource Details by PO</td></tr>

<tr><td colspan="2">

<%
String extra_space = "";

String po = "";

Map<String, Map<String, String>> POTableMap = (Map<String, Map<String, String>>)request.getAttribute("POTableMap");

for (Map.Entry<String, Map<String, String>>  po_map : POTableMap.entrySet()) { 
	
	po = po_map.getKey();
	Map<String, String> po_details = po_map.getValue();
%>

<table border="0" cellspacing="0" cellpadding="0" class="p14000000">
<tr>
  <td class="txtA11B p0050"><b>PO:</b></td>
  <td class="txtC11N p0555"><%=po %></td>
  
  <td class="txtA11B p0050"><b>Type:</b></td>
  <td class="txtC11N p0555"><%=po_details.get("lm_po_type") %></td>
  
  <td class="txtA11B p0050"><b>Authorized Total:</b></td>
  <td class="txtC11N p0555"><%=po_details.get("authorised_total") %></td>
  
  <td class="txtA11B p0050"><b>Exception:</b></td>
  <td class="txtC11N p0555"><%=po_details.get("status") %></td>
  
  <td class="txtA11B p0050"><b>Effective Rate:</b></td>
  <td class="txtC11N p0555"><%=po_details.get("effective_rate") %></td>
  
</tr>
</table> 

<table border="0" cellspacing="0" cellpadding="0">

<tr>
  <td colspan="7" class="txtB11B p3010 ac">Job</td>
  <td>&nbsp;</td>
  <td colspan="3" class="txtA11B p3010 ac">Purchase Order</td>
</tr>

<tr>
  <td colspan="7" class="rule1BM1010"><img src="lua/sp1.gif"></td>
  <td colspan="1"><img src="lua/sp1.gif"></td>
  <td colspan="3" class="rule1AM1010"><img src="lua/sp1.gif"></td>
</tr>

<tr class="txtB11N p3010 ar">
  <td class="p04120100 al">Resource Name</td>
  <td class="p4818 ac">Type</td>
  <td class="p4414 ac">Billable</td>
  <td class="p4414 ac">OOS</td>
  <td width="35">Qty</td>
  <td width="55">Unit<br>Cost</td>
  <td width="55">Total<br>Cost</td>
  <td><img src="lua/sp1.gif" width="10"></td>
  <td width="35" class="txtA11N">Qty</td>
  <td width="55" class="txtA11N">Unit<br>Cost</td>
  <td width="55" class="txtA11N">Total<br>Cost</td>
</tr>

<%=po_details.get("table") %>

<%
extra_space = "XXXX&nbsp;<br>";
}
%>

</td></tr>
</table>
</div>