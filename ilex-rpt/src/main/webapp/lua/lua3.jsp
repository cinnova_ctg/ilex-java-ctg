<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.text.DecimalFormat" %>
<%@ include file="styles.txt"%>
<%             
DecimalFormat sd = new DecimalFormat("#,##0.00");

String[] styles0 = {"PCSTDEVEN0001 PCSTD0003 TBTXT0001","PCSTDODD0001 PCSTD0003 TBTXT0001"};
String[] styles1 = {"PCSTDEVEN0001 PCSTD0003 TBTXT0002","PCSTDODD0001 PCSTD0003 TBTXT0002"};
String[] styles2 = {"PCSTDEVEN0001 PCSTD0003 TBMON0001","PCSTDODD0001 PCSTD0003 TBMON0001"};

%>
<style>
<!--
.LB1, .LB2{
font-size: 11px;
font-weight: bold;
white-space:nowrap;
font-family:Verdana, Arial, Helvetica, sans-serif; 
padding: 2px 10px 2px 2px;
color:#000000;
vertical-align: bottom;
}
.LB2 {
TEXT-ALIGN: Center;
padding: 10px 10px 2px 2px;
}
-->
</style>
<table border="0" cellspacing="0" cellpadding="0">

<tr><td colspan="2">
  <table border="0" cellspacing="0" cellpadding="0" style="padding: 2px;">
  
    <tr><td class="LB1">Job Title:</td><td><%=request.getAttribute("JobTitle")%></td></tr>
    <tr><td class="LB1">Offsite Date:</td><td><%=request.getAttribute("InvoiceDate")%></td></tr>
    <tr><td class="LB1">Site Location:</td><td><%=request.getAttribute("SiteNumber")%></td></tr>
  
    <tr>
    <td colspan="2">
    <table border="0" cellspacing="1" cellpadding="0">
    <tr>
      <td class="LB1">PF Cost:</td><td><%=request.getAttribute("ProFormaCost")%>&nbsp;&nbsp;&nbsp;</td>
      <td class="LB1">Actual Cost:</td><td><%=request.getAttribute("ActualCost")%>&nbsp;&nbsp;&nbsp;</td>
      <td class="LB1">Revenue:</td><td><%=request.getAttribute("Revenue")%>&nbsp;&nbsp;&nbsp;</td>
      <td class="LB1">VGPM:</td><td><%=request.getAttribute("VGPM")%>&nbsp;&nbsp;&nbsp;</td>
      <td class="LB1">PF VGPM:</td><td><%=request.getAttribute("PGPM")%></td>
    </tr>
    </table>
    </td>
    </tr>
    
 </table>
</td></tr>

<tr><td class="LB2" colspan="2">Resource Details</td></tr>
 
<tr><td colspan="2">
<table border="0" cellspacing="1" cellpadding="0" class="TBWID0001">
<thead>
<tr>
  <th colspan="7">Job</td>
  <th colspan="3">PO</td>
<tr>
  <th>Name</td>
  <th>Type</td>
  <th>Non-B</td>
  <th>OOS</td>
  <th>Qty</td>
  <th>Unit</td>
  <th>Total</td>
  <th>Qty</td>
  <th>Unit</td>
  <th>Total</td>
</tr>
</thead>
<%
Vector<Map <String, String>> aligned_resources = (Vector<Map <String, String>>)request.getAttribute("ResourceTable");

Double LX_CE_QUANTITY = 0.0;
Double LX_CE_TOTAL_COST = 0.0;
Double LM_PWO_AUTHORISED_QTY =0.0;
Double LM_PWO_AUTHORISED_TOTAL_COST = 0.0;
int ii = 0;

for (int i=0; i<aligned_resources.size(); i++) {
	
	Map <String, String> ar = aligned_resources.get(i);
	
	/*
	rm.put("LM_RS_TYPE", r.lm_resource[Resource.LM_RS_TYPE]);
	rm.put("IV_RP_NAME", r.iv_resource_pool[Resource.IV_RP_NAME]);
	rm.put("LX_CE_QUANTITY", r.lx_cost_element[Resource.LX_CE_QUANTITY]);
	rm.put("LX_CE_UNIT_COST", r.lx_cost_element[Resource.LX_CE_UNIT_COST]);
	rm.put("LX_CE_TOTAL_COST", r.lx_cost_element[Resource.LX_CE_TOTAL_COST]);
	rm.put("LX_CE_TOTAL_COST", r.lx_cost_element[Resource.LX_CE_TOTAL_COST]);
	
    // Buy side
	rm.put("LM_PWO_AUTHORISED_UNIT_COST", r.lm_purchase_work_order[Resource.LM_PWO_AUTHORISED_UNIT_COST]);
	rm.put("LM_PWO_AUTHORISED_TOTAL_COST", r.lm_purchase_work_order[Resource.LM_PWO_AUTHORISED_TOTAL_COST]);
	rm.put("LM_PWO_AUTHORISED_QTY", r.lm_purchase_work_order[Resource.LM_PWO_AUTHORISED_QTY]);
	
	// From job level - activity details
	rm.put("OOS", activity.get("lm_at_oos_flag"));
	rm.put("Non-B", activity.get("lm_at_oos_flag"));
	rm.put("lm_at_quantity", activity.get("lm_at_quantity"));
	*/
	
	
	
%>

<tr>
  <td class="<%=styles0[i%2]%>"><%=ar.get("IV_RP_NAME")%></td>
  <td class="<%=styles1[i%2]%>"><%=ar.get("LM_RS_TYPE")%></td>
  <td class="<%=styles1[i%2]%>"><%=ar.get("Non-B")%></td>
  <td class="<%=styles1[i%2]%>"><%=ar.get("OOS")%></td>
  <td class="<%=styles2[i%2]%>"><%=ar.get("LX_CE_QUANTITY")%></td>
  <td class="<%=styles2[i%2]%>"><%=ar.get("LX_CE_UNIT_COST")%></td>
  <td class="<%=styles2[i%2]%>"><%=ar.get("LX_CE_TOTAL_COST")%></td>
  <td class="<%=styles2[i%2]%>"><%=ar.get("LM_PWO_AUTHORISED_QTY")%></td>
  <td class="<%=styles2[i%2]%>"><%=ar.get("LM_PWO_AUTHORISED_UNIT_COST")%></td>
  <td class="<%=styles2[i%2]%>"><%=Double.parseDouble(ar.get("LM_PWO_AUTHORISED_TOTAL_COST"))%></td>
</tr>

<% 

LX_CE_QUANTITY += Double.parseDouble(ar.get("LX_CE_QUANTITY"));
LX_CE_TOTAL_COST += Double.parseDouble(ar.get("LX_CE_TOTAL_COST"));
LM_PWO_AUTHORISED_QTY += Double.parseDouble(ar.get("LM_PWO_AUTHORISED_QTY"));
LM_PWO_AUTHORISED_TOTAL_COST += Double.parseDouble(ar.get("LM_PWO_AUTHORISED_TOTAL_COST"));

ii = i;
} %>

<tr>
  <td class="<%=styles0[ii%2]%>"><b>Total</b></td>
  <td class="<%=styles1[ii%2]%>">&nbsp;</td>
  <td class="<%=styles1[ii%2]%>">&nbsp;</td>
  <td class="<%=styles1[ii%2]%>">&nbsp;</td>
  <td class="<%=styles2[ii%2]%>"><b><%=sd.format(LX_CE_QUANTITY)%></b></td>
  <td class="<%=styles2[ii%2]%>">&nbsp;</td>
  <td class="<%=styles2[ii%2]%>"><b><%=sd.format(LX_CE_TOTAL_COST)%></b></td>
  <td class="<%=styles2[ii%2]%>"><b><%=sd.format(LM_PWO_AUTHORISED_QTY)%></b></td>
  <td class="<%=styles2[ii%2]%>">&nbsp;</td>
  <td class="<%=styles2[ii%2]%>"><b><%=sd.format(LM_PWO_AUTHORISED_TOTAL_COST)%></b></td>
</tr>

</table>
</td></tr>

</table>