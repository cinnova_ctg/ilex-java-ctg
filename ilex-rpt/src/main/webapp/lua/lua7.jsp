<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.text.DecimalFormat" %>
<%@ include file="common_form_data.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="refresh" content="86400">
<title><%=base_page_title%></title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<script language="JavaScript" src="lua/FusionCharts.js"></script>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table width="1100" border="0" cellspacing="0" cellpadding="0">
<!-- Title and Links -->
 <tr><td>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="titleLine">Labor Usage Analysis - Exception Trend</td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td align="right"><%@ include file="navigation.jsp"%></a></td>
  </tr>
  </table>
</td></tr>
<!-- Charts -->
 <tr><td>
  <table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="sectionTitleLine">Non-compliant Purchase Orders</td>
    <td class="sectionTitleLine">Current MM/SP Percentages versus Non-Compliant MM/SP</td>
  </tr>
  <tr>
    <td valign="top">
    <div id="chart1" align="center">The chart will appear within this DIV. This text will be replaced by the chart.</div>
    <script type="text/javascript">
        var chart1 = new FusionCharts("lua/MSLine.swf", "chart1ID", "500", "450", "0", "0");
        chart1.setDataURL("LaborUsageAnalysis%2Exo%3Fac%3Dlua%26rpt%3D51");
        chart1.render("chart1");
    </script>
    </td>
    <td valign="top">
    <div id="chart2" align="center">The chart will appear within this DIV. This text will be replaced by the chart.</div>
    <script type="text/javascript">
        var chart2 = new FusionCharts("lua/MSLine.swf", "chart2ID", "500", "450", "0", "0");
        chart2.setDataURL("LaborUsageAnalysis%2Exo%3Fac%3Dlua%26rpt%3D52");
        chart2.render("chart2");
    </script>
    </td>
  </tr>
  <tr>
    <td class="cDataLeft0" style="vertical-align : top;">This chart displays non-compliant purchase orders.  Non-compliance includes POs with Non-Billable OOS labor (not billed to customer 
    but pay to partner) and PO which exceed the estimated/expected labor rate.
    </td>
    <td class="cDataLeft0" style="vertical-align : top;">
    This chart illustrates the current MM/SP percentage and the MM/SP percentage with labor exceptions removed(chart from right). 
    Note, the exceptions have only been collected for the last six months which equates to a 09-2011 starting point.
    </td>
  </tr>
  <tr>
    <td class="sectionTitleLine">Breakdown for ALL Purchase Orders (Trailing 6 Months)</td>
    <td class="sectionTitleLine">Breakdown by Exception (Trailing 6 Months)</td>
  </tr>
 <tr>
    <td valign="top">
    <div id="chart3" align="center">The chart will appear within this DIV. This text will be replaced by the chart.</div>
    <script type="text/javascript">
        var chart3 = new FusionCharts("lua/Pie2D.swf", "chart3ID", "470", "300", "0", "0");
        chart3.setDataURL("LaborUsageAnalysis%2Exo%3Fac%3Dlua%26rpt%3D54");
        chart3.render("chart3");
    </script>    
    </td>
    <td valign="top">
    <div id="chart4" align="center">The chart will appear within this DIV. This text will be replaced by the chart.</div>
    <script type="text/javascript">
        var chart4 = new FusionCharts("lua/Pie2D.swf", "chart4ID", "470", "300", "0", "0");
        chart4.setDataURL("LaborUsageAnalysis%2Exo%3Fac%3Dlua%26rpt%3D53");
        chart4.render("chart4");
    </script>
    </td>
  </tr> 
    <tr>
    <td class="cDataLeft0" style="vertical-align : top;">
    <div  style="padding: 15px 0px 10px 0px;">This chart provides a view of compliant vs non-compliant purchase orders.</div>
    </td>
    <td class="cDataLeft0" style="vertical-align : top;"><div  style="padding: 15px 0px 10px 0px;">
    This chart displays the breakdown of non-compliant categories by purchase order type.  There are currently two types of non-compliance: 
    1) Hours = Non-Billable OOS labor (not billed to customer but pay to partner) and 2) Rate = A high effective rate was paid to the partner.</div>
    </td>
  </tr> 
  </table>

 </td></tr>
</table>

</td></tr>
</table>
</div>
</body>
</html>