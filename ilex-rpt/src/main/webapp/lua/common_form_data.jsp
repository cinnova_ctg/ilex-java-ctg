<%
String base_page_title = "Labor Usage Analysis";
String base_url = "LaborUsageAnalysis.xo?ac=lua";
String display_title = "";
String display_sub_title = "";
// Form data

String rpt = (request.getAttribute("rpt") != null) ? (String)request.getAttribute("rpt") : "" ;
int irpt;
String po_type = (request.getAttribute("po_type") != null) ? (String)request.getAttribute("po_type") : "" ;
String project = (request.getAttribute("project") != null) ? (String)request.getAttribute("project") : "" ;
String from_date = (request.getAttribute("from_date") != null) ? (String)request.getAttribute("from_date") : "" ;
String to_date = (request.getAttribute("to_date") != null) ? (String)request.getAttribute("to_date") : "" ;
String jo = (request.getAttribute("jo") != null) ? (String)request.getAttribute("jo") : "" ;
String client = (request.getAttribute("client") != null) ? (String)request.getAttribute("client") : "" ;
String spm = (request.getAttribute("spm") != null) ? (String)request.getAttribute("spm") : "" ;
String exception = (request.getAttribute("exception") != null) ? (String)request.getAttribute("exception") : "" ;
String jo_list = (request.getAttribute("jo_list") != null) ? (String)request.getAttribute("jo_list") : "" ;
String spm_list = (request.getAttribute("spm_list") != null) ? (String)request.getAttribute("spm_list") : "" ;
String cp_list = (request.getAttribute("cp_list") != null) ? (String)request.getAttribute("cp_list") : "" ;

// Navigation data

String core_link = "";
String dyn_link = "";

String home_page_link = "<a href=\"\">Trends</a>";

// Report unique data


if (rpt.equals("")) {
	irpt = 0;
} else {
	irpt = Integer.parseInt(rpt);
}

switch (irpt){

case 0:
	display_title = "Labor Usage Analysis - Exceptions";
	break;
	
case 11:
	display_title = "Labor Usage Analysis - Exceptions";
	display_sub_title = "By Clients" ;
	break;
	
case 2:
	display_title = "Labor Usage Analysis - Exceptions";
	display_sub_title = "By Client and Appendix" ;
	break;
	
case 21:
	display_title = "Labor Usage Analysis - Exceptions";
	display_sub_title = "By Sr. PM, Client and Appendix" ;
	break;

	
case 5:
	display_title = "Labor Usage Analysis - Exceptions";
	display_sub_title = "By Sr. PM" ;
	break;
	
case 6:
	display_title = "Labor Usage Analysis - Exceptions";
	display_sub_title = "By Job Owner" ;
	break;

case 70:
	display_title = "Labor Usage Analysis - Exceptions";
	display_sub_title = (request.getAttribute("display_subtitle") != null) ? ": "+(String)request.getAttribute("display_subtitle") : "" ;
	display_sub_title = "Detail By Job Owner";
	break;
	
case 71:
	display_title = "Labor Usage Analysis - Exceptions";
	display_sub_title = "Detail by Client and Appendix";
	break;


}
%>