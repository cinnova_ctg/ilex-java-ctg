<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.text.DecimalFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%

DecimalFormat sd = new DecimalFormat("#,##0.00");
String base_url = "LaborUsageAnalysis.xo?ac=lua";
String alt_link = "";

String rpt = (String)request.getAttribute("rpt");
String po_type = (String)request.getAttribute("po_type");
String po_type_display = "";
String flip_type = "";
String flip_name = "";

if (po_type.equals("M")) {
	po_type_display = "Minuteman";
	flip_type = base_url+"&rpt="+rpt+"&po_type=S";
	flip_name = "Speedpay";
} else if (po_type.equals("S")) {
	po_type_display = "Speedpay";
	flip_type = base_url+"&rpt="+rpt+"&po_type=M";
	flip_name = "Minuteman";
}

String link = base_url+"&po_type="+po_type+"&rpt=";
String top_level = "<a href=\""+link+"1\">Company</a>";
String spm = "<a href=\""+link+"5\">Sr.PM</a>";
String jo = "<a href=\""+link+"4\">Job Owner</a>";

%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Labor Usage Analysis</title>
<%@ include file="styles.txt"%>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
 
<table border="0" cellspacing="0" cellpadding="0">
  <tr><td>

   <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="titleLine"><%=po_type_display%> Job Owner Labor Usage Analysis - Exceptions</td>
    <td align="right"><%=top_level%> | <%=spm%> | <%=jo%> | <a href="<%=flip_type%>"><%=flip_name%></a></td>
  </tr>
  </table>

  </td></tr>

 
  <tr><td valign="top">
  <table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
  
    <tr>
     <td class="columnHeader2">Job Owner</td>
     <td class="columnHeader2">PF Labor Qty</td>
     <td class="columnHeader2">PF Revenue</td>
     <td class="columnHeader2">Actual Labor Qty</td>
     <td class="columnHeader2">Revenue Based Revenue</td>
     <td class="columnHeader2">PO Auth Qty</td>
     <td class="columnHeader2">PO Auth Cost</td>
     <td class="columnHeader2">Effective Rate</td>
     <td class="columnHeader2">Count</td>
   </tr>
   
   <%
	/*
	 1 Job owner,
     2 labor_qty_pf, 
     3 labor_amt_pf, 
     4 labot_qty_sell,
     5 labor_amt_sell,
     6 labor_qty_buy, 
     7 labot_amt_buy, 
     8 count
	 */
	
   	Vector rows = (Vector)request.getAttribute("rows");
   	if (rows != null && rows.size() > 0) {
   		for (int i = 0; i < rows.size(); i++) {
   			String[] r = (String[]) rows.get(i);
   			Double effective_rate = Double.parseDouble(r[7])/Double.parseDouble(r[4]);
   			String url = base_url+"&rpt=6&po_type="+po_type+"&jo="+r[9];
   	%>
   	<tr>
     <td class="cDataLeft<%=i%2%>"><a href="<%=url%>"><%=r[1]%></a></td>
     <td class="cDataRight<%=i%2%>"><%=r[2]%></td>
     <td class="cDataRight<%=i%2%>"><%=r[3]%></td>
     <td class="cDataRight<%=i%2%>"><%=r[4]%></td>
     <td class="cDataRight<%=i%2%>"><%=r[5]%></td>
     <td class="cDataRight<%=i%2%>"><%=r[6]%></td>
     <td class="cDataRight<%=i%2%>"><%=r[7]%></td>
     <td class="cDataRight<%=i%2%>"><%=sd.format(effective_rate)%></td>
     <td class="cDataRight<%=i%2%>"><%=r[8]%></td>
   </tr>
   
   	<% 
   		}
   	}
   	%>
   	
   </table>
  </td>
  </tr>
</table>
<br><br>
Click in Info button for job details (just top for demo)<br><br>
Notes:<br>
Reports looks at labor and travel resources.<br>
Data does not include the inflated labor based on non-billable materials/freight<br>
Data does not weight a fixed price across all resources proportionally.  To be added after detail pages are available.<br>
Data is restricted to jobs with one PO.  There are a limited number of multi-PO minuteman/speedpay jobs.
</td>
</tr>
</table>
</div> 

</body>
</html>