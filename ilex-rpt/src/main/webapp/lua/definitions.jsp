<tr>
<td>
  <table width="600" border="0" cellspacing="0" cellpadding="0">
  <tr>
  <td>
  <p><b>PF Qty and Cost</b>.  These columns reflect the planned labor quantities and cost.  This does not include OOS.</p>
  <p><b>Revenue Based Cost and Labor Qty</b>.  The term revenue based captures the resources which are being billed.
  These columns use this definition to capture the qty and cost associated with these resources.</p>
  <p><b>Authorized Labor Qty and Authorized Cost</b>.  The PO based cost and qty for labor.</p>
  </td>
  </tr>
  </table>
</td>
</tr>