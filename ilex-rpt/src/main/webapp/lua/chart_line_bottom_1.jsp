 <styles>
      <definition>
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />
         <style name='sCaption1'     type='font' size='12' color='000000' bold='1' />
         <style name='sSubCaption1'  type='font' size='11' color='808080' bold='0' />
         <style name='sXAxisName1'   type='font' size='11' color='000000' bold='1' />
         <style name='sYAxisName1'   type='font' size='11' color='000000' bold='1' />
         <style name='sDataLabels1'  type='font' size='10' color='808080' bold='0' />
         <style name='sYAxisValues1' type='font' size='10' color='808080' bold='0' />
         <style name='sDataValues1'  type='font' size='10' color='808080' bold='0' />
         <style name='sToolTips1'    type='font' size='10' color='000000' bold='0' />
         <style name='sLegend1'      type='font' size='10' color='000000' bold='0' />
      </definition>
      <application>
         <apply toObject='Border'      styles='sBorder1' />
         <apply toObject='Canvas'      styles='CanvasAnim' />
         <apply toObject='Caption'     styles='sCaption1' />
         <apply toObject='SubCaption'  styles='sSubCaption1' />
         <apply toObject='XAxisName'   styles='sXAxisName1' />
         <apply toObject='YAxisName'   styles='sYAxisName1' />
         <apply toObject='DataLabels'  styles='sDataLabels1' />
         <apply toObject='YAxisValues' styles='sYAxisValues1' />
         <apply toObject='ToolTip'     styles='sToolTips1' />
         <apply toObject='Legend'      styles='sLegend1' />
      </application>   
 </styles>

</chart> 