<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.text.DecimalFormat" %>
<%@ include file="common_form_data.jsp"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<link href="<%=request.getContextPath() %>/css/tableSorter.css" rel="stylesheet" type="text/css" />
<%@ page  import="java.text.NumberFormat,java.util.Locale" %>
<%@ page  import="java.text.NumberFormat,java.util.Locale,java.util.Calendar" %>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%> 
<%-- <%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %> --%>
<%-- <%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %> --%>
<%-- <%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> --%>
<%@ page import = "java.util.Map" %>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="refresh" content="86400">
<title><%=base_page_title%></title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<script language="JavaScript" src="lua/FusionCharts.js"></script>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table width="1100" border="0" cellspacing="0" cellpadding="0">
<!-- Title and Links -->
 <tr><td>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="titleLine">Labor Usage Analysis - Exception Trend</td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td align="right"><%@ include file="navigation.jsp"%></a></td>
  </tr>
  </table>
</td></tr>
<!-- Charts -->
 <tr><td>
  <table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="sectionTitleLine">Non-compliant Purchase Orders</td>
    <td class="sectionTitleLine">Current MM/SP Percentages versus Non-Compliant MM/SP</td>
  </tr>
  <tr>
    <td valign="top">
    <div id="chart1" align="center">The chart will appear within this DIV. This text will be replaced by the chart.</div>
    <!-- <script type="text/javascript">
        var chart1 = new FusionCharts("lua/MSLine.swf", "chart1ID", "500", "450", "0", "0");
        chart1.setDataURL("LaborUsageAnalysis%2Exo%3Fac%3Dlua%26rpt%3D51");
        chart1.render("chart1");
    </script> -->
    
     <%--  --%>
    
    <!--  New Graph  -->
    <%
   		ArrayList<String[]> data = (ArrayList<String[]>)request.getAttribute("array");
   		ArrayList<String[]> data2 = (ArrayList<String[]>)request.getAttribute("arraygraph2");
   		ArrayList<String[]> data3 = (ArrayList<String[]>)request.getAttribute("arraygraph3");
   		ArrayList<String[]> data4 = (ArrayList<String[]>)request.getAttribute("arraygraphadd3");
   		/* ArrayList<String[]> data2 = (ArrayList<String[]>)request.getAttribute("projectCategories");
   		ArrayList<String[]> data3 = (ArrayList<String[]>)request.getAttribute("projectCategories01");
   		ArrayList<String[]> data4 = (ArrayList<String[]>)request.getAttribute("projectCategories02");
   		ArrayList<String[]> data5 = (ArrayList<String[]>)request.getAttribute("projectCategories03");
   		ArrayList<String[]> data6 = (ArrayList<String[]>)request.getAttribute("listDataminute");
   		ArrayList<String[]> data7 = (ArrayList<String[]>)request.getAttribute("listDataspeed") */;
   		/* ArrayList<String[]> data8 = (ArrayList<String[]>)request.getAttribute("listDatapvs"); */
   		
   		
   		
                 
		
   	 
   	 
   		
    %>
     <script>
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChartbyday);
      function drawChartbyday() {
        var data = google.visualization.arrayToDataTable([
          ['Months', 'Minutes', 'Speedpay','Pvs']
          <% for(int i=0;i<data.size();i++){
        	  String[] st = data.get(i);
        		// st[i] = data.listIterator().toString();
				%>
				,['<%= st[0]%>', <%= st[1]%>, <%= st[2]%>,<%=st[3]%> ]
				<%
				 
				 
			 }%>
         
        ]);

        var options = {
          title: '',
          hAxis:{"title":"Months"},
          vAxis:{"title":"Counts"},
          height: 500
          ,width: 500
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart1'));
        chart.draw(data, options);
      }
          
    </script>
    
    
    
    
    
    
    
    <!--   -->
    
    </td>
    <td valign="top">
    <div id="chart2" align="center">The chart will appear within this DIV. This text will be replaced by the chart.</div>
   <!--  <script type="text/javascript">
        var chart2 = new FusionCharts("lua/MSLine.swf", "chart2ID", "500", "450", "0", "0");
        chart2.setDataURL("LaborUsageAnalysis%2Exo%3Fac%3Dlua%26rpt%3D52");
        chart2.render("chart2");
    </script> -->
     <script>
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChartby);
      function drawChartby() {
        var data = google.visualization.arrayToDataTable([
          ['Months', 'ALL MM/SP', 'Corrected MM/SP']
          <% for(int i=0;i<data2.size();i++){
        	  String[] st = data2.get(i);
        		// st[i] = data.listIterator().toString();
				%>
				,['<%= st[0]%>', <%= st[1]%>, <%= st[2]%> ]
				<%
				 
				 
			 }%>
         
        ]);

        var options = {
          title: '',
          hAxis:{"title":"Months"},
          vAxis:{"title":"Percentage"},
          height: 500
          ,width: 500
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart2'));
        chart.draw(data, options);
      }
          
    </script>
    
    
    
    
    </td>
  </tr>
  <tr>
    <td class="cDataLeft0" style="vertical-align : top;">This chart displays non-compliant purchase orders.  Non-compliance includes POs with Non-Billable OOS labor (not billed to customer 
    but pay to partner) and PO which exceed the estimated/expected labor rate.
    </td>
    <td class="cDataLeft0" style="vertical-align : top;">
    This chart illustrates the current MM/SP percentage and the MM/SP percentage with labor exceptions removed(chart from right). 
    Note, the exceptions have only been collected for the last six months which equates to a 09-2011 starting point.
    </td>
  </tr>
  <tr>
    <td class="sectionTitleLine">Breakdown for ALL Purchase Orders (Trailing 6 Months)</td>
    <td class="sectionTitleLine">Breakdown by Exception (Trailing 6 Months)</td>
  </tr>
 <tr>
    <td valign="top">
    <div id="chart3" align="center">The chart will appear within this DIV. This text will be replaced by the chart.</div>
    <!-- <script type="text/javascript">
        var chart3 = new FusionCharts("lua/Pie2D.swf", "chart3ID", "470", "300", "0", "0");
        chart3.setDataURL("LaborUsageAnalysis%2Exo%3Fac%3Dlua%26rpt%3D54");
        chart3.render("chart3");
    </script>  -->   
     <script>
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Months', 'ALL MM/SP']
          <% for(int i=0;i<data3.size();i++){
        	  String[] st = data3.get(i);
        		// st[i] = data.listIterator().toString();
				%>
				,['<%= st[0]%>', <%= st[1]%> ]
				<%
				 
				 
			 }%>
         
        ]);

        var options = {
          title: '',
          is3D: true,
          height: 500
          ,width: 500
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart3'));
        chart.draw(data, options);
      }
          
    </script>
    
    
    
    </td>
    <td valign="top">
    <div id="chart4" align="center">The chart will appear within this DIV. This text will be replaced by the chart.</div>
    <!-- <script type="text/javascript">
        var chart4 = new FusionCharts("lua/Pie2D.swf", "chart4ID", "470", "300", "0", "0");
        chart4.setDataURL("LaborUsageAnalysis%2Exo%3Fac%3Dlua%26rpt%3D53");
        chart4.render("chart4");
        data4
    </script> -->
    
    <script>
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChartPie);
      function drawChartPie() {
        var data = google.visualization.arrayToDataTable([
          ['Months', 'ALL MM/SP']
          <% for(int i=0;i<data4.size();i++){
        	  String[] st = data4.get(i);
        		// st[i] = data.listIterator().toString();
				%>
				,['<%= st[0]%>', <%= st[1]%> ]
				<%
				 
				 
			 }%>
         
        ]);

        var options = {
          title: '',
          is3D: true,
          height: 500
          ,width: 500
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart4'));
        chart.draw(data, options);
      }
          
    </script>
    
    </td>
  </tr> 
    <tr>
    <td class="cDataLeft0" style="vertical-align : top;">
    <div  style="padding: 15px 0px 10px 0px;">This chart provides a view of compliant vs non-compliant purchase orders.</div>
    </td>
    <td class="cDataLeft0" style="vertical-align : top;"><div  style="padding: 15px 0px 10px 0px;">
    This chart displays the breakdown of non-compliant categories by purchase order type.  There are currently two types of non-compliance: 
    1) Hours = Non-Billable OOS labor (not billed to customer but pay to partner) and 2) Rate = A high effective rate was paid to the partner.</div>
    </td>
  </tr> 
  </table>

 </td></tr>
</table>

</td></tr>
</table>
</div>
</body>
</html>