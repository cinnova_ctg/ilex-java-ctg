<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.text.DecimalFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%

DecimalFormat sd = new DecimalFormat("#,##0.00");
String base_url = "LaborUsageAnalysis.xo?ac=lua";

String rpt = (String)request.getAttribute("rpt");
String po_type = (String)request.getAttribute("po_type");
String po_type_display = "";
String flip_type = "";
String flip_name = "";

if (po_type.equals("M")) {
	po_type_display = "Minuteman";
	flip_type = base_url+"&rpt="+rpt+"&po_type=S";
	flip_name = "Speedpay";
} else if (po_type.equals("S")) {
	po_type_display = "Speedpay";
	flip_type = base_url+"&rpt="+rpt+"&po_type=M";
	flip_name = "Minuteman";
}

String link = base_url+"&po_type="+po_type+"&rpt=";
String top_level = "<a href=\""+link+"1\">Company</a>";
String spm = "<a href=\""+link+"5\">Sr.PM</a>";
String jo = "<a href=\""+link+"4\">Job Owner</a>";

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Labor Usage Analysis</title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<script language="JavaScript" src="lua/FusionCharts.js"></script>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
 
<table border="0" cellspacing="0" cellpadding="0">
  <tr><td>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="titleLine"><%=po_type_display%> Labor Usage Analysis - Exceptions</td>
    <td align="right"><%=top_level%> | <%=spm%> | <%=jo%> | <a href="<%=flip_type%>"><%=flip_name%></a></td>
  </tr>
  </table>
  
  </td></tr>
 
  <tr><td valign="top">
  <table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
  
    <tr>
     <td class="columnHeader2">Sr. PM</td>
     <td class="columnHeader2">Customer</td>
     <td class="columnHeader2">Project</td>
     <td class="columnHeader2">Auth PO Cost</td>
     <td class="columnHeader2">Billable Labor Qty</td>
     <td class="columnHeader2">Effect Rate</td>
     <td class="columnHeader2">Count</td>
   </tr>
   
   <%
   
	/*
	 1 ac_pd_lo_ot_name, 
	 2 lx_pr_title, 
	 3 substring(ac_du_name, 1, charindex(',', ac_du_name)-1),
     4 sum(labor_qty_pf), 
     5 sum(labor_amt_pf), 
     6 sum(labot_qty_sell), 
     7 sum(labor_amt_sell), 
     8 sum(labor_qty_buy), 
     9 sum(labot_amt_buy), 
    10 convert(decimal(5,2),(labot_amt_buy*1.0)/(labot_qty_sell*1.0)), 
    11 count(*)
	 */
	 
   	Vector rows = (Vector) request.getAttribute("rows");
   	if (rows != null && rows.size() > 0) {
   		for (int i = 0; i < rows.size(); i++) {
   			String[] r = (String[]) rows.get(i);
   			
   			Double effective_rate = Double.parseDouble(r[7])/Double.parseDouble(r[8]);
   			String url = base_url+"&rpt=2&spm_display="+r[3]+"&po_type="+po_type+"&lx_pr_id="+r[12];
   	%>

   	<tr>
     <td class="cDataLeft<%=i%2%>"><%=r[3]%></td>
     <td class="cDataLeft<%=i%2%>"><%=r[1]%></td>
     <td class="cDataLeft<%=i%2%>"><a href="<%=url%>"><%=r[2]%></a></td>
     <td class="cDataRight<%=i%2%>"><%=r[7]%></td>
     <td class="cDataRight<%=i%2%>"><%=r[8]%></td>
     <td class="cDataRight<%=i%2%>"><%=sd.format(effective_rate)%></td>
     <td class="cDataRight<%=i%2%>"><%=r[11]%></td>
   </tr>
   
   	<% 
   		}
   	}
   	%>
   	
   </table>
  </td>
  <td valign="top">
    <div id="chartdiv" align="center">The chart will appear within this DIV. This text will be replaced by the chart.</div>
    <script type="text/javascript">
        var myChart = new FusionCharts("lua/MSLine.swf", "myChartId", "800", "500", "0", "0");
        myChart.setDataURL("lua/data2.xml");
        myChart.render("chartdiv");
    </script>
  </td>
  </tr>
</table>
<br><br>
Auth PO Cost - Authorized PO cost<br>
Billable Labor Qty - Total billable labor quantity for job.<br><br>
Notes:<br>
Reports looks at labor and travel resources.<br>
Data does not include the inflated labor based on non-billable materials/freight<br>
Data does not weight a fixed price across all resources proportionally.  To be added after detail pages are available.<br>
Data is restricted to jobs with one PO.  There are a limited number of multi-PO minuteman/speedpay jobs.
</td>
</tr>
</table>
</div> 
</body>
</html>