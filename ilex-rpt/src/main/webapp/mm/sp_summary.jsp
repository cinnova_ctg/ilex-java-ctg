<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.Vector" %>
<%
Map report = (Map) request.getAttribute("mm_summary");

String href = "";

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Minuteman Summary</title>

<!-- production key ABQIAAAAk17xvRffek5XvGJR1KDgbxSwRy4s2eep2DvUSxAFcnO47vT-IhSMuP2GfucbtY2TSiiIcVngO9uULQ -->
<!-- local key ABQIAAAAk17xvRffek5XvGJR1KDgbxT2yXp_ZAY8_ufC3CFXhHIE1NvwkxQnAVYWFAbtuuMPaTYX5TsmaVhu0w  -->
  
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAk17xvRffek5XvGJR1KDgbxSwRy4s2eep2DvUSxAFcnO47vT-IhSMuP2GfucbtY2TSiiIcVngO9uULQ"
  type="text/javascript"></script>
<link rel = STYLESHEET href= 'admin.css' Type='text/css'>
<style>
    body {
    		font-family : Arial, Helvetica, sans-serif;
    		font-size : 12px;
    		cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .pageTitle {
		font-size : 15pt;
		font-weight : bold;
		text-align : center;
		padding : 12px 0px 12px 0px;
    } 
    .titleLine {
		font-size : 11pt;
		font-weight : bold;
		text-align : left;
		padding : 0px 0px 0px 0px;
        vertical-align : top;
    } 
    .sectionTitleLine {
		font-size : 10pt;
		font-weight : bold;
		text-align : right;
		padding : 0px 0px 0px 0px;
        vertical-align : bottom;
    }    
    .labelsLeft, .labelsCenter, .labelsRight {
		font-size : 9pt;
		font-weight : bold;
		padding : 5px 10px 5px 10px;
        vertical-align : top;
    }    

    .labelsCenter {
		text-align : center;
    }    
    .forecast, .highprobability, .recognized, .delta  {
		font-size : 8pt;
		font-weight : normal;
		text-align : center;
		padding : 2px 1px 2px 1px;
		border-style: solid;
        border-width: 1px
    }
    .forecast {
		background-color : #ffccc9;
    }  
    .highprobability {
		background-color : #e5f9de;
    }  
    .recognized {
		background-color : #e0edf5;		 
    }     
    .delta {
		background-color : #ffffff;
		text-align : right;
    }
    .dataLineLeft, .dataLineCenter, .dataLineRight,{
		font-size : 9pt;
		font-weight : normal;
		padding : 2px 0px 6px 0px;
    }
    .dataLineCenter {
		text-align : center;
    }
    .dataLineLeft {
		text-align : left;
    }
    .dataLineRight {
        padding : 2px 30px 6px 0px;
		text-align : right;
    }
.labelsCenter1 {		text-align : center;
}
</style>
<script type="text/javascript">
   
function initialize() {
   if (GBrowserIsCompatible()) 
     {        
       var map = new GMap2(document.getElementById("map1"));
       map.addControl(new GLargeMapControl());
       map.addControl(new GScaleControl());
       map.addControl(new GMapTypeControl());
       
       map.setCenter(new GLatLng(36.81362, -96.554209), 4);
       
       var redIcon = new GIcon(G_DEFAULT_ICON);
       
       
       var mapIcon = new GIcon(G_DEFAULT_ICON);
       mapIcon.image = "http://ilex.contingent.local/rpt/mm/sp_icon.png"; 
       mapIcon.iconSize = new GSize(18, 18);
       mapIcon.shadow = "http://ilex.contingent.local/rpt/mm/shadow.png";
       mapIcon.shadowSize = new GSize(29, 22);
       markerOptions = { icon:mapIcon };        <%
       
       Vector latitude = (Vector)request.getAttribute("latitude");
       Vector longitude = (Vector)request.getAttribute("longitude");
       Vector type = (Vector)request.getAttribute("type");
       String partner_type = "";
       for (int i=0; i<longitude.size(); i++) {
    	   partner_type = (String)type.get(i);
    	   if (partner_type.equals("S")) {
       %>
	   map.addOverlay(new GMarker(new GLatLng(<%=(String)latitude.get(i)%>,<%=(String)longitude.get(i)%>), markerOptions));
	   <%
    	   }
       }
	   %>
     } 
}    
</script>
</head>
<body onload="initialize()">
<div class="rptBody01">
<div class="pageTitle" style="width: 840px">Speedpay Summary</div>

<table width="670" border="0" cellspacing="0" cellpadding="0">

  <%
  Map charts = (Map)request.getAttribute("charts");
   
  Map parameters = (Map)charts.get("c1-SS-count-ttm");
  String dw_chxl1 = (String)parameters.get("dw_chxl1");
  String dw_chxl2 = (String)parameters.get("dw_chxl2");;
  String dw_chxl3 = (String)parameters.get("dw_chxl3");;
  String dw_chd = (String)parameters.get("dw_chd");
  %>
 
<!--  Obsolete
<tr>
<td>
 <div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
 <span class="labelsCenter">Speedpay Usage by Month</span>
 <img src="http://chart.apis.google.com/chart?
cht=bvs&amp;chs=400x200&amp;
chd=t:<%=dw_chd%>&amp;
chxt=x,y,x&amp;
chxl=
0:|<%=dw_chxl1%>|
1:|<%=dw_chxl3%>|
2:|<%=dw_chxl2%>
&amp;alt="Speedpay Usage/Month" />
 </div>
</td>
 
<td><div style="padding : 0px 0px 0px 0px; width: 20px"><div></td>
  <%  
  parameters = (Map)charts.get("c1-SS-sum-ttm");
  dw_chxl1 = (String)parameters.get("dw_chxl1");
  dw_chxl2 = (String)parameters.get("dw_chxl2");;
  dw_chxl3 = (String)parameters.get("dw_chxl3");;
  dw_chd = (String)parameters.get("dw_chd");
  %>
<td>
 <div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px;">
 <span class="labelsCenter">Speedpay Cost by Month</span>
<img src="http://chart.apis.google.com/chart?
cht=bvs&amp;chs=400x200&amp;
chd=t:<%=dw_chd%>&amp;
chxt=x,y,x&amp;
chxl=
0:|<%=dw_chxl1%>|
1:|<%=dw_chxl3%>|
2:|<%=dw_chxl2%>
&amp;alt="Speedpay Cost/Month" />
 </div>
</td>
</tr>
-->

<tr><td colspan="3"><br></td></tr>

<tr>
<td colspan="3">
 <div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px; width: 840px">
 <table width="100%" border="0" cellspacing="1" cellpadding="0">
 <tr>
 <td colspan="7" class="titleLine">Pay Period Summary</td>
 </tr>
 
 <tr>
   <td bgcolor="E0E0DC" class="labelsCenter">Pay Date</td>
   <td bgcolor="E0E0DC" class="labelsCenter">Pay Status</td>
   <td bgcolor="E0E0DC" class="labelsCenter"> Start</td>
   <td bgcolor="E0E0DC" class="labelsCenter">End</td>
   <td bgcolor="E0E0DC" class="labelsCenter">Purchase Orders</td>
   <td bgcolor="E0E0DC" class="labelsCenter">Total Period Cost</td>
 </tr>
			<%
			Vector rows = (Vector) request.getAttribute("pay_days");

			if (rows.size() > 0) {

			for (int i = 0; i < rows.size()-2; i++) {
			
			String[] row = (String[]) rows.get(i);

			if (row[3].equals("Paid")) {
							
				href = "/rpt/MMSP.xo?form_action=history&pay_date="+row[0]+"&end_date="+row[2]+"&type_of_po=S";
			%>
			<tr>
				<td bgcolor="#e0edf5" class="dataLineCenter"><%=row[0]%></td>
				<td bgcolor="#e0edf5" class="dataLineCenter"><a href="<%=href%>"><%=row[3]%></a></td>
				<td bgcolor="#e0edf5" class="dataLineCenter"><%=row[1]%></td>
				<td bgcolor="#e0edf5" class="dataLineCenter"><%=row[2]%></td>
				<td bgcolor="#e0edf5" class="dataLineCenter"><%=row[4]%></td>
				<td bgcolor="#e0edf5" class="dataLineRight"><%=row[5]%></td>
			</tr>
			<%
			} else if (row[3].equals("Current")) {
				
				href = "/rpt/MMSP.xo?form_action=review_list&end_date="+row[2]+"&type_of_po=S";
			%>
			<tr>
				<td bgcolor="#e5f9de" class="dataLineCenter"><%=row[0]%></td>
				<td bgcolor="#e5f9de" class="dataLineCenter"><a href="<%=href%>"><%=row[3]%></a></td>
				<td bgcolor="#e5f9de" class="dataLineCenter"><%=row[1]%></td>
				<td bgcolor="#e5f9de" class="dataLineCenter"><%=row[2]%></td>
				<td bgcolor="#e5f9de" class="dataLineCenter"><%=row[4]%></td>
				<td bgcolor="#e5f9de" class="dataLineRight"><%=row[5]%></td>
			</tr>
			<%
			} else {
				href = "/rpt/MMSP.xo?form_action=review_list&start_date="+row[1]+"&end_date="+row[2]+"&type_of_po=S";
			%>
			<tr>
				<td bgcolor="#ffccc9" class="dataLineCenter"><%=row[0]%></td>
				<td bgcolor="#ffccc9" class="dataLineCenter"><a href="<%=href%>"><%=row[3]%></a></td>
				<td bgcolor="#ffccc9" class="dataLineCenter"><%=row[1]%></td>
				<td bgcolor="#ffccc9" class="dataLineCenter"><%=row[2]%></td>
				<td bgcolor="#ffccc9" class="dataLineCenter"><%=row[4]%></td>
				<td bgcolor="#ffccc9" class="dataLineRight"><%=row[5]%></td>
			</tr>
			<%
			}
 	}
 	} else {
 %>
 
 <td colspan="7" class="labelsCenter">No summary is currently available.</td>
 
 <%
  }
  %>
 
 </table>
 </div>
</td>
</tr>


<!--  Obsolete

<tr><td colspan="3"><br></td></tr>


<tr>
<td colspan="3">
 <div style="border : 1px solid #323e7a; padding : 5px 5px 5px 5px; width: 840px">
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr>
  <td class="titleLine">Speedpay Partner Locations</td>
 </tr>
 <tr>
 <td class="titleLine">
 <table border="0" cellpadding="0" cellspacing="1" width="100%">
 <tr>
  <td width="100%" valign="top" align="left" style="padding: 0px; border: 1px solid;"><div id="map1" style="width: auto; height: 450px"></div></td>
 </tr>
 </table>
 </td>
 </tr>
 </table>
 </div>
</td>
</tr>
-->

</table>
</div>
</body>
</html>