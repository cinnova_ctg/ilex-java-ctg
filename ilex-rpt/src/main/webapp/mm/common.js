//START{ javascript cookie functions
function createCookie(name,value,days)
{
	if (days)
	{
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name)
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++)
	{
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name)
{
	createCookie(name,"",-1);
}
//}END
// JavaScript Document
function MM_preloadImages() { //v3.0
var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

// ------------------------------------------------------------------------

function writeSource(div) {
	if (!document.getElementById) { return; }
	var o = document.getElementById(div);
	if (typeof(o) == "undefined" || o==null) { return; }
	var s = o.innerHTML;
	if (s==null || s.length==0) { 
		return;
		}
	else {
		var i;
		for(i=0;s.charAt(i)==" "||s.charAt(i)=="\n"||s.charAt(i)=="\r"||s.charAt(i)=="\t";i++) {}
		s = s.substring(i);
		for (i = s.length; i>0; i--) {
			if (s.charAt(i)=="<") {
				s = s.substring(0,i) + "&lt;" + s.substring(i+1) ;
				}
			}
		for (i = s.length; i>0; i--) {
			if (s.charAt(i)==">") {
				s = s.substring(0,i) + "&gt;" + s.substring(i+1) ;
				}
			}
		for (i = s.length; i>0; i--) {
			if (s.charAt(i)=="\t") {
				s = s.substring(0,i) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + s.substring(i+1) ;
				}
			}
		for (i = s.length; i>0; i--) {
			if (s.charAt(i)=="\n") {
				s = s.substring(0,i) + "<BR>" + s.substring(i+1) ;
				}
			}
		s = s + "<BR>";
		}
	document.write('<A STYLE="font-family:arial; font-size:x-small; text-decoration:none;" HREF="#" onClick="var d=document.getElementById(\'jssource'+div+'\').style; if(d.display==\'block\'){d.display=\'none\';this.innerText=\'+ Show Source\';}else{d.display=\'block\';this.innerText=\'- Hide Source\';} return false;">+ Show Source</A><BR>');
	document.write('<SPAN ID="jssource'+div+'" STYLE="display:none;background-color:#EEEEEE"><TT>'+s+'</TT></SPAN>');
	}
	
// ------------------------------------------------------------------------

// JavaScript Document
function OpenWin(width,height,url)
{
	//alert('Hello');
	ss = window.open(url,"subWnd","scrollbars,resizable,toolbar=no,Top=150,Left=150,Height="+height+",Width="+width);
	ss.focus();
	return false;
}
function OpenSubWin(windowName,width,height,url)
{
	//alert(url);
	ss = window.open(url,windowName,"scrollbars,resizable,toolbar=no,Top=150,Left=150,Height="+height+",Width="+width);
	ss.focus();
	return false;
}
function goBack( url )
{
	//alert("yes");
	top.location = url;
}
function del()
{
	//return confirm ("Are you sure you want to delete?");
	return confirm ("Are you sure?");
}
function closeRefreshWindow(location)
{	
	window.opener.location=location;	
	window.close();
}

// function for search box
function check_value(obj, text, valCheck)
{
	//alert(valCheck);
	var objValue = obj.value;
	if(valCheck != objValue)
	{
		if(objValue.length == 0)
			obj.value = text;
	}
	else
		obj.value = text;
}

//---------------------- FUNCTION USED FOR TABS -----------------------------
// returns the object of the document
function getObject(n, d) { //v4.01
	var p,i,x;
	if(!d) d=document; 
	if((p=n.indexOf("?"))>0&&parent.frames.length) 
	{
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
	}
	if(!(x=d[n])&&d.all) x=d.all[n]; 
	for (i=0;!x&&i<d.forms.length;i++) 
		x=d.forms[i][n];
  	for(i=0;!x&&d.layers&&i<d.layers.length;i++) 
	
	x=getObject(n,d.layers[i].document);
	if(!x && d.getElementById) x=d.getElementById(n); 
	//alert(x);
	return x;
}
// ---------------------------------------------------------------------------
function selectMarginTab(divName)
{
	// selecting tab name container's value
	var stName = getObject('selectedMarginTab');
	var selectedTabName = stName.value;
	
	// getting selected tabs object
	var selectedMarginTab = getObject(selectedTabName);
	// getting object to select
	var marginTab = getObject(divName);
	
	// getting link object
	var selectedLink = getObject(selectedTabName+'_link');
	var mlink = getObject(divName+'_link');
	
	// changing properties
	selectedMarginTab.style.display = 'none';
	marginTab.style.display = 'block';
	// changing link properties
	
	selectedLink.className = 'mTabNormal';
	mlink.className = 'mTabSelected';
	
	// changing the value of the tab name container
	stName.value = divName;
}
// ---------------------------------------------------------------------------
// TOOLTIP JS
function DivSetVisible1(state, divName)
{
	//var DivRef = document.getElementById('testdiv1');
	var DivRef = document.getElementById(divName);
	var IfrRef = document.getElementById('DivHolder');
	//alert(divName);
	if(state)
	{
		DivRef.style.display = "block";
		IfrRef.style.width = DivRef.offsetWidth;
		IfrRef.style.height = DivRef.offsetHeight;
		IfrRef.style.top = DivRef.style.top;
		IfrRef.style.left = DivRef.style.left;
		IfrRef.style.zIndex = DivRef.style.zIndex - 1;
		IfrRef.style.display = "block";
		//alert(DivRef.style.zIndex - 1);
	}
	else
	{
		DivRef.style.display = "none";
		IfrRef.style.display = "none";
	}
}
function DivSetVisible(state, divName)
{
	//var DivRef = document.getElementById('testdiv1');
	var DivRef = document.getElementById(divName);
	var IfrRef = document.getElementById('DivHolder');
	//alert(divName);
	if(state)
	{
		DivRef.style.display = "block";
		IfrRef.style.width = DivRef.offsetWidth;
		IfrRef.style.height = DivRef.offsetHeight;
		IfrRef.style.top = DivRef.style.top;
		IfrRef.style.left = DivRef.style.left;
		IfrRef.style.zIndex = DivRef.style.zIndex - 1;
		IfrRef.style.display = "block";
		//alert(DivRef.style.zIndex - 1);
	}
	else
	{
		DivRef.style.display = "none";
		IfrRef.style.display = "none";
	}
}
function getMouseCoords(e)
{
	var posx = 0;
	var posy = 0;
	if (!e) var e = window.event;
	if (e.pageX || e.pageY)
	{
		posx = e.pageX;
		posy = e.pageY;
	}
	else if (e.clientX || e.clientY)
	{
		posx = e.clientX + document.body.scrollLeft;
		posy = e.clientY + document.body.scrollTop;
	}
	// posx and posy contain the mouse position relative to the document
	// Do something with this information
	//alert(posx+" / "+posy);
	var rtnArr = new Array();
	rtnArr[0] = posx;
	rtnArr[1] = posy;
	return rtnArr;
}

function showObject( objID, message, width )
{
	//alert(message);
	var coordsArr = getMouseCoords();
	//alert(coordsArr[0]+" / "+coordsArr[1]);
	var showObj = document.getElementById(objID);
	showObj.style.visibility = "visible";
	showObj.style.top = coordsArr[1]+5;
	showObj.style.left = coordsArr[0]+10;
	showObj.style.width = width;
	//message = (typeof message != "undefined") ? message : "";
	
	DivSetVisible1(1, objID);
	//showObj.innerHTML = coordsArr[0]+" / "+coordsArr[1]+"<br>"+message;
	showObj.innerHTML = message;
	return false;
}

function hideObject( objID )
{
	var hideObj = document.getElementById(objID);
	hideObj.style.visibility = "hidden";
	DivSetVisible1(0, objID);
	return false;
}

// END TOOLTIP JS
// ------------------------------------------------------------------------

	//FORM VALIDATION
	/*******************************************************************************
	FILE: RegExpValidate.js
	
	AUTHOR: Karen Gayda
	
	DATE: 06/11/2000
	
	EMAIL: KGayda@yahoo.com
	
	DESCRIPTION: This file contains a library of validation functions
	  using javascript regular expressions.  Library also contains functions that re-
	  format fields for display or for storage.
	  
	
	  VALIDATION FUNCTIONS:
	
	  validateCurrency - checks for valid currency format
	  validateTime - checks for valid 12 hour time
	  validateState -  checks for valid state abbreviation  
	  validateSSN - checks format of social security number
	  validateEmail - checks format of email address
	  validateUSPhone - checks format of US phone number
	  validateNumeric - checks for valid numeric value
	  validateInteger - checks for valid integer value
	  validateNotEmpty - checks for blank form field
	  validateUSZip - checks for valid US zip code
	  validateUSDate - checks for valid date in US format
	  validateValue - checks a string against supplied pattern
	  
	  FORMAT FUNCTIONS:
	  
	  rightTrim - removes trailing spaces from a string
	  leftTrim - removes leading spaces from a string
	  trimAll - removes leading and trailing spaces from a string
	  removeCurrency - removes currency formatting characters (), $ 
	  addCurrency - inserts currency formatting characters
	  removeCommas - removes comma separators from a number
	  addCommas - adds comma separators to a number
	  removeCharacters - removes characters from a string that match passed pattern
	*******************************************************************************/
	
	
	function validateCurrency( strValue)  {
	/************************************************
	DESCRIPTION: Validates that a string contains a 
	  valid currency format. 
	  
	 PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	*************************************************/
	  var objRegExp = /(^\$\d{1,3}(,\d{3})*\.\d{2}$)|(^\(\$\d{1,3}(,\d{3})*\.\d{2}\)$)/;
	
	  return objRegExp.test( strValue );
	}
	
	function validateTime ( strValue ) {
	/************************************************
	DESCRIPTION: Validates that a string contains a 
	  valid 12 hour time format. Seconds are optional.
	  
	 PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	
	REMARKS: Returns True for time formats such as:
	  HH:MM or HH:MM:SS or HH:MM:SS.mmm (where the
	  .mmm is milliseconds as used in SQL Server 
	  datetime datatype.  Also, the .mmm portion will 
	  accept 1 to 3 digits after the period)
	*************************************************/
	  var objRegExp = /^([1-9]|1[0-2]):[0-5]\d(:[0-5]\d(\.\d{1,3})?)?$/;
	
	  return objRegExp.test( strValue );
	
	}
	
	function validateState (strValue ) {
	/************************************************
	DESCRIPTION: Validates that a string contains a 
	  valid state abbreviation. 
	  
	 PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	*************************************************/
	
	var objRegExp = /^(AK|AL|AR|AZ|CA|CO|CT|DC|DE|FL|GA|HI|IA|ID|IL|IN|KS|KY|LA|MA|MD|ME|MI|MN|MO|MS|MT|NB|NC|ND|NH|NJ|NM|NV|NY|OH|OK|OR|PA|RI|SC|SD|TN|TX|UT|VA|VT|WA|WI|WV|WY)$/i; 
	
	  return objRegExp.test(strValue);
	}
	
	function validateSSN( strValue ) {
	/************************************************
	DESCRIPTION: Validates that a string contains a 
	  valid social security number. 
	  
	 PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	*************************************************/
	var objRegExp  = /^\d{3}\-\d{2}\-\d{4}$/;
	 
	  //check for valid SSN
	  return objRegExp.test(strValue);
	
	}
	
	function validNumSSN(formField,message)
	{
		var result = true;
		if (!validateNotEmpty(formField,message))
			result = false;
		if (result)
		{
			var num = parseInt(formField.value);
			if (isNaN(num) || formField.value == 0)
			{
				alert(message);
				formField.focus();
				result = false;
			}
		}
		if (result)
		{
			if(formField.value.length < 4)
			{
				alert(message);
				formField.focus();
				result = false;
			}
		}
		return result;
	}
	
	
	function validateEmail(formfield,strMsg) {
	/************************************************
	DESCRIPTION: Validates that a string contains a 
	  valid email pattern. 
	  
	 PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	   
	REMARKS: Accounts for email with country appended
	  does not validate that email contains valid URL
	  type (.com, .gov, etc.) and optionally,
	  a valid country suffix.  Since email has many
	  forms this expression only tests for near valid
	  address.  Some additional validation may be
	  required.
	*************************************************/
	var objRegExp  = /^[a-z0-9]([a-z0-9_\-\.]*)@([a-z0-9_\-\.]*)(\.[a-z]{2,3}(\.[a-z]{2}){0,2})$/i;
	  //check for valid email
		if(objRegExp.test(formfield.value))
			return true;
		else	
		{
			alert(strMsg);
			formfield.focus();
			return false;		
		}
	}	
	
	function validateUSPhone( strValue ) {
	/************************************************
	DESCRIPTION: Validates that a string contains valid
	  US phone pattern. 
	  Ex. (999) 999-9999 or (999)999-9999
	  
	PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	*************************************************/
	  var objRegExp  = /^\([1-9]\d{2}\)\s?\d{3}\-\d{4}$/;
	 
	  //check for valid us phone with or without space between 
	  //area code
	  return objRegExp.test(strValue); 
	}
	
	function  validateNumeric( strValue ) {
	/******************************************************************************
	DESCRIPTION: Validates that a string contains only valid numbers.
	
	PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	******************************************************************************/
	  var objRegExp  =  /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/; 	
	  //check for numeric characters 
	  return objRegExp.test(strValue);
	}
	
	function validateInteger( form_field,strmsg  ) {
	/************************************************
	DESCRIPTION: Validates that a string contains only 
		valid integer number.
		
	PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	******************************************************************************/
	  var objRegExp  = /(^-?\d\d*$)/;
	 
	  //check for integer characters
	  if(!objRegExp.test(form_field.value))
	  {
		alert(strmsg);		
		form_field.focus();
		return false;
	  }
	  else
		return true;
	}
	
	function validateNotEmpty( form_field,strmsg ) {
	/************************************************
	DESCRIPTION: Validates that a string is not all
	  blank (whitespace) characters.
		
	PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	*************************************************/
	   var strTemp = form_field.value;
	   strTemp = trimAll(strTemp);
	   if(strTemp.length > 0){
		 return true;
	   }  
	   else
	   {
			alert(strmsg);		
			form_field.focus();
			return false;
		}	
	}
	
	function validateUSZip( strValue ) {
	/************************************************
	DESCRIPTION: Validates that a string a United
	  States zip code in 5 digit format or zip+4
	  format. 99999 or 99999-9999
		
	PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	
	*************************************************/
	var objRegExp  = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
	 
	  //check for valid US Zipcode
	  return objRegExp.test(strValue);
	}
	
	function validateUSDate( strValue ) {
	/************************************************
	DESCRIPTION: Validates that a string contains only 
		valid dates with 2 digit month, 2 digit day, 
		4 digit year. Date separator can be ., -, or /.
		Uses combination of regular expressions and 
		string parsing to validate date.
		Ex. mm/dd/yyyy or mm-dd-yyyy or mm.dd.yyyy
		
	PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	   
	REMARKS:
	   Avoids some of the limitations of the Date.parse()
	   method such as the date separator character.
	*************************************************/
	  var objRegExp = /^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{4}$/
	 
	  //check to see if in correct format
	  if(!objRegExp.test(strValue))
		return false; //doesn't match pattern, bad date
	  else{
		var arrayDate = strValue.split(RegExp.$1); //split date into month, day, year
		var intDay = parseInt(arrayDate[1],10); 
		var intYear = parseInt(arrayDate[2],10);
		var intMonth = parseInt(arrayDate[0],10);
		
		//check for valid month
		if(intMonth > 12 || intMonth < 1) {
			return false;
		}
		
		//create a lookup for months not equal to Feb.
		var arrayLookup = { '01' : 31,'03' : 31, '04' : 30,'05' : 31,'06' : 30,'07' : 31,
							'08' : 31,'09' : 30,'10' : 31,'11' : 30,'12' : 31}
	  
		//check if month value and day value agree
		if(arrayLookup[arrayDate[0]] != null) {
		  if(intDay <= arrayLookup[arrayDate[0]] && intDay != 0)
			return true; //found in lookup table, good date
		}
			
		//check for February
		var booLeapYear = (intYear % 4 == 0 && (intYear % 100 != 0 || intYear % 400 == 0));
		if( ((booLeapYear && intDay <= 29) || (!booLeapYear && intDay <=28)) && intDay !=0)
		  return true; //Feb. had valid number of days
	  }
	  return false; //any other values, bad date
	}
	
	function validateValue( strValue, strMatchPattern ) {
	/************************************************
	DESCRIPTION: Validates that a string a matches
	  a valid regular expression value.
		
	PARAMETERS:
	   strValue - String to be tested for validity
	   strMatchPattern - String containing a valid
		  regular expression match pattern.
		  
	RETURNS:
	   True if valid, otherwise false.
	*************************************************/
	var objRegExp = new RegExp( strMatchPattern);
	 
	 //check if string matches pattern
	 return objRegExp.test(strValue);
	}
	
	
	function rightTrim( strValue ) {
	/************************************************
	DESCRIPTION: Trims trailing whitespace chars.
		
	PARAMETERS:
	   strValue - String to be trimmed.  
		  
	RETURNS:
	   Source string with right whitespaces removed.
	*************************************************/
	var objRegExp = /^([\w\W]*)(\b\s*)$/;
	 
		  if(objRegExp.test(strValue)) {
		   //remove trailing a whitespace characters
		   strValue = strValue.replace(objRegExp, '$1');
		}
	  return strValue;
	}
	
	function leftTrim( strValue ) {
	/************************************************
	DESCRIPTION: Trims leading whitespace chars.
		
	PARAMETERS:
	   strValue - String to be trimmed
	   
	RETURNS:
	   Source string with left whitespaces removed.
	*************************************************/
	var objRegExp = /^(\s*)(\b[\w\W]*)$/;
	 
		  if(objRegExp.test(strValue)) {
		   //remove leading a whitespace characters
		   strValue = strValue.replace(objRegExp, '$2');
		}
	  return strValue;
	}
	
	function trimAll( strValue ) {
	/************************************************
	DESCRIPTION: Removes leading and trailing spaces.
	
	PARAMETERS: Source string from which spaces will
	  be removed;
	
	RETURNS: Source string with whitespaces removed.
	*************************************************/ 
	 var objRegExp = /^(\s*)$/;
	
		//check for all spaces
		if(objRegExp.test(strValue)) {
		   strValue = strValue.replace(objRegExp, '');
		   if( strValue.length == 0)
			  return strValue;
		}
		
	   //check for leading & trailing spaces
	   objRegExp = /^(\s*)([\W\w]*)(\b\s*$)/;
	   if(objRegExp.test(strValue)) {
		   //remove leading and trailing whitespace characters
		   strValue = strValue.replace(objRegExp, '$2');
		}
	  return strValue;
	
	}
	
	function removeCurrency( strValue ) {
	/************************************************
	DESCRIPTION: Removes currency formatting from 
	  source string.
	  
	PARAMETERS: 
	  strValue - Source string from which currency formatting
		 will be removed;
	
	RETURNS: Source string with commas removed.
	*************************************************/
	  var objRegExp = /\(/;
	  var strMinus = '';
	 
	  //check if negative
	  if(objRegExp.test(strValue)){
		strMinus = '-';
	  }
	  
	  objRegExp = /\)|\(|[,]/g;
	  strValue = strValue.replace(objRegExp,'');
	  if(strValue.indexOf('$') >= 0){
		strValue = strValue.substring(1, strValue.length);
	  }
	  return strMinus + strValue;
	}
	
	function addCurrency( strValue ) {
	/************************************************
	DESCRIPTION: Formats a number as currency.
	
	PARAMETERS: 
	  strValue - Source string to be formatted
	
	REMARKS: Assumes number passed is a valid 
	  numeric value in the rounded to 2 decimal 
	  places.  If not, returns original value.
	*************************************************/
	  var objRegExp = /-?[0-9]+\.[0-9]{2}$/;
	   
		if( objRegExp.test(strValue)) {
		  objRegExp.compile('^-');
		  strValue = addCommas(strValue);
		  if (objRegExp.test(strValue)){
			strValue = '($' + strValue.replace(objRegExp,'') + ')';
		  }
		  else {
			strValue = '$' + strValue;
		  }
		  return  strValue;
		}
		else
		  return strValue;
	}
	
	function removeCommas( strValue ) {
	/************************************************
	DESCRIPTION: Removes commas from source string.
	
	PARAMETERS: 
	  strValue - Source string from which commas will 
		be removed;
	
	RETURNS: Source string with commas removed.
	*************************************************/
	  var objRegExp = /,/g; //search for commas globally
	 
	  //replace all matches with empty strings
	  return strValue.replace(objRegExp,'');
	}
	
	function addCommas( strValue ) {
	/************************************************
	DESCRIPTION: Inserts commas into numeric string.
	
	PARAMETERS: 
	  strValue - source string containing commas.
	  
	RETURNS: String modified with comma grouping if
	  source was all numeric, otherwise source is 
	  returned.
	  
	REMARKS: Used with integers or numbers with
	  2 or less decimal places.
	*************************************************/
	  var objRegExp  = new RegExp('(-?[0-9]+)([0-9]{3})'); 
	
		//check for match to search criteria
		while(objRegExp.test(strValue)) {
		   //replace original string with first group match, 
		   //a comma, then second group match
		   strValue = strValue.replace(objRegExp, '$1,$2');
		}
	  return strValue;
	}
	
	function removeCharacters( strValue, strMatchPattern ) {
	/************************************************
	DESCRIPTION: Removes characters from a source string
	  based upon matches of the supplied pattern.
	
	PARAMETERS: 
	  strValue - source string containing number.
	  
	RETURNS: String modified with characters
	  matching search pattern removed
	  
	USAGE:  strNoSpaces = removeCharacters( ' sfdf  dfd', 
									'\s*')
	*************************************************/
	 var objRegExp =  new RegExp( strMatchPattern, 'gi' );
	 
	 //replace passed pattern matches with blanks
	  return strValue.replace(objRegExp,'');
	}
	
	function validateRadioNotEmpty(form_field,strmsg)
	{
		//alert(form_field);
		var rtnBool = false;
		var count=form_field.length;
		for(var i=0; i<count; i++)
		{
			if(form_field[i].checked)
				rtnBool = true;
		}
		if(!rtnBool)
			alert(strmsg);
		return rtnBool;
	}
	function __validateNotEmpty( form_field,strmsg ) {
	/************************************************
	DESCRIPTION: Validates that a string is not all
	  blank (whitespace) characters.
		
	PARAMETERS:
	   strValue - String to be tested for validity
	   
	RETURNS:
	   True if valid, otherwise false.
	*************************************************/
	   var strTemp = form_field.value;
	   strTemp = trimAll(strTemp);
	   if(strTemp.length > 0){
		 return true;
	   }  
	   else
	   {
			alert(strmsg);		
			form_field.focus();
			return false;
		}	
	}
	/******************************************************/
	function validate_al_num(form_field)
	{
		//var al_num=/[a-zA-Z0-9]/;
		//alert(form_field.value);
		//var al_num=/\w/;
		var objRegExp  = /(^\d{5}$)|(^\d{5}-\d{4}$)/; 
		//check for valid US Zipcode 	
		
		alert(objRegExp.test(form_field.value));
		
		return al_num.test(form_field.value); 
	}
	/******************************************************/
	function validate_pass_confpass(pass, confpass, msg)
	{
		if(pass.value != confpass.value)
		{
			alert(msg);
			pass.focus();
			return false;
		}
		
		return true;
	}
	/******************************************************/
	function validDateMDY(formField, message)
	{
		var result = true;
		if (!validateNotEmpty(formField, message))
			result = false;
		if (result)
		{
			var elems = formField.value.split('/');
			result = (elems.length == 3); // should be three components
			if (result)
			{
				var day = parseInt(elems[1]);
				var month = parseInt(elems[0]);
				var year = parseInt(elems[2]);
				//alert(month + "/" + day + "/" + year);
				result = !isNaN(month) && (month > 0) && (month < 13) && !isNaN(day) && (day > 0) && (day < 32) && !isNaN(year) && (elems[2].length == 4);
			}
			if (!result)
			{
				//alert(month + "/" + day + "/" + year);
				alert(message);
				formField.focus();
			}
		}
		return result;
	}
	
// ------------------------------------------------------------------------
function ylib_getObj(id,d)
{
	var i,x;  if(!d) d=document; 
	if(!(x=d[id])&&d.all) x=d.all[id]; 
	for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][id];
	for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=ylib_getObj(id,d.layers[i].document);
	if(!x && document.getElementById) x=document.getElementById(id); 
	return x;
}
function createCookie(name,value,days)
{
	if (days)
	{
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}
/* ------------------------------------------------------------------------
	this functionn inserts A row in the given table Id.
---------------------------------------------------------------------------*/
function createRow(tbl){
	if(!tbl)
		return false;
	var row = tbl.insertRow(tbl.rows.length);
	return row;
}//end of function
/*#################################################################################################################
	This function clears the table.It deleted all Rows of the given table
	ARGUMENTS::
		tbl:: reference of the table
#################################################################################################################*/
	function truncateTable(tblId){
		if(!tblId)
			return false;
		if(tblId.rows && tblId.rows.length){
			this.rows2Delete = tblId.rows.length;
			for(this.delRow=0; this.delRow<this.rows2Delete; this.delRow++)
				tblId.deleteRow(0);
		}//end if
	}//end of fucntion
/*#################################################################################################################
	This function creates column in the a Row...
	ARGUMENST:
		rw :: reference of an row Object
#################################################################################################################*/
function createCol(rw, colNum){
	if(!rw)
		return false;
	var col = rw.insertCell(colNum);
	return col;
}//end of fucntion
/*#################################################################################################################
	This function creates a text Box..
	ARGUMENST:
		name:: name of the text Box.
		val :: value of the text Box.
#################################################################################################################*/
function createTextBox(name, value){
	var hd = document.createElement("input");
	hd.type = "text";
	hd.className = "text";
	hd.name = name;
	hd.id = name;
	hd.value = value;
	hd.size = 4;
	hd.maxLength = 4;
	return hd;
}//end of function
/*###############################################################################################################
###################################################################################################################*/
function maxLengthOfSubject(maxChar, obj){
	if(!obj)
		return false;
	if(obj.value.length >= maxChar)
		obj.value = obj.value.toString().substr(0, (maxChar-1));
	return false;
}//edn of function
/*--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------*/
	function send_request(url, div_id, function_to_call){
		try{
			http_obj = getXHTTPObject();
			http_obj.open("GET", url, true);
			http_obj.onreadystatechange = function(){
				if(http_obj.readyState == 4){
					text = http_obj.responseText;
					d = document.getElementById(div_id);
					if(d){
						d.innerHTML = "";
						d.innerHTML = text;
					}
					eval(function_to_call);
				}		
			}
			http_obj.send(null);
		}
		catch(ex){
			alert(ex);
		}
	}//end of function
/*----------------------------------------------------------------------------------------------------*/
function getXHTTPObject(){
	var xmlhttp;
	if(!xmlhttp){
		try {
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(e){
			try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(E){
				xmlhttp = false;
			}
		}
		if(!xmlhttp && typeof XMLHttpRequest!='undefined'){
			xmlhttp = new XMLHttpRequest();
		}
	}
	return xmlhttp;
}
/*----------------------------------------------------------------------------------------------------*/
