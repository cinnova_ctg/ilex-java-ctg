<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%

Vector rows = (Vector)request.getAttribute("pay_period");
int count = 0;
if (rows != null) {
	count = rows.size();
}

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PO Search Function</title>
<style>
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 12px;
    	cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .rptBody02 {
		 padding : 10px 0px 0px 20px;
    }
    .titleLine {
		font-size : 12pt;
		font-weight : bold;
		text-align : left;
		vertical-align : bottom;
		padding : 0px 0px 6px 0px;
    } 
    .sectionTitleLine {
		font-size : 10pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 0px 0px 6px 0px;
    }
    .sectionTitleLineNav {
		font-size : 8pt;
		font-weight : normal;
		text-align : right;
		vertical-align : middle;
		padding : 0px 0px 0px 0px;
    }
    .label1, .label2 {
        font-size : 9pt;
		font-weight : bold;
		vertical-align : top;
		padding : 10px 10px 6px 0px;
    }
    .form1 {
        font-size : 10pt;
		font-weight : normal;
		padding : 0px 0px 0px 0px;
    }
    .elementContainer {
    padding : 0px 2px 0px 2px;
    }
    .columnHeader1 {
		font-size : 9pt;
		font-weight : bold;
		text-align : center;
		vertical-align : bottom;
		padding : 3px 5px 3px 5px;
		background-color: #E0E0DC;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 8pt;
		font-weight : normal;
		text-align : left;
		vertical-align : top;
		padding : 1px 4px 1px 4px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #e5f9de;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ffffff;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }        
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
		text-align : center;
		width: 26px;
        height: 24px;
    }  
</style>
</head>
<body>
<div class="rptBody01">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr><td class="titleLine">Minuteman Pay Period: <%=(String)request.getAttribute("start_date")%> - <%=(String)request.getAttribute("end_date")%></td></tr>
 </table>
 </td>
</tr>
 
<tr>
<td>
<br/>
 <table border="0" cellspacing="1" cellpadding="0">
 <tr>
     <td class="columnHeader1">Minuteman</td>
     <td class="columnHeader1">PO #</td>
     <td class="columnHeader1">Delivery Date</td>
     <td class="columnHeader1">Resource</td>
     <td class="columnHeader1">Authorized<br>Unit Cost</td>
     <td class="columnHeader1">Authorized<br>Quantity</td>
     <td class="columnHeader1">Authorized&nbsp;Total<br>Resource Cost</td>
     <td class="columnHeader1">Auhorized<br>Total Cost</td>
     <td class="columnHeader1">Approved<br>Total Cost</td>
     <td class="columnHeader1">Customer</td>
     <td class="columnHeader1">Project</td>
     <td class="columnHeader1">Job</td>
     <td class="columnHeader1">Status</td>
     <td class="columnHeader1">Closed Date</td>
 </tr>
 
     <% 
     String class_bgc = "cDataLeft0";
     String current_po = "";
     String previous_po = "";
     int i = 0;
     if (rows != null) {
       for (int j=0; j<rows.size(); j++) {
    	 String[] r = (String[])rows.get(j);
    	 current_po = r[1];
    	 
    	 if (!current_po.equals(previous_po)) {
    		 current_po = r[1];
    		 previous_po = current_po;
    		 i++;
    	       %>
    	       <tr>
    	       <td class="cDataLeft<%=i%2%>"><%=r[0]%></td>
    	       <td class="cDataLeft<%=i%2%>"><%=r[1]%></td>
    	       <td class="cDataLeft<%=i%2%>"><%=r[2]%></td>
    	       <td class="cDataRight<%=i%2%>"><%=r[3]%></td>
    	       <td class="cDataRight<%=i%2%>"><%=r[6]%></td>
    	       <td class="cDataCenter<%=i%2%>"><%=r[7]%></td>
    	       <td class="cDataRight<%=i%2%>"><%=r[8]%></td>
    	       <td class="cDataRight<%=i%2%>"><%=r[4]%></td>
    	       <td class="cDataRight<%=i%2%>"><%=r[5]%></td>
    	       <td class="cDataLeft<%=i%2%>"><%=r[9]%></td>
    	       <td class="cDataLeft<%=i%2%>"><%=r[10]%></td>
    	       <td class="cDataLeft<%=i%2%>"><%=r[11]%></td>
    	       <td class="cDataLeft<%=i%2%>"><%=r[12]%></td>
    	       <td class="cDataLeft<%=i%2%>"><%=r[13]%></td>
    	       </tr>
    	       <%	 
    	 } else {
    	       %>
    	       <tr>
    	       <td class="cDataLeft<%=i%2%>">&nbsp;</td>
    	       <td class="cDataLeft<%=i%2%>">&nbsp;</td>
    	       <td class="cDataLeft<%=i%2%>">&nbsp;</td>
    	       <td class="cDataRight<%=i%2%>"><%=r[3]%></td>
    	       <td class="cDataRight<%=i%2%>"><%=r[6]%></td>
    	       <td class="cDataCenter<%=i%2%>"><%=r[7]%></td>
    	       <td class="cDataRight<%=i%2%>"><%=r[8]%></td>
    	       <td class="cDataLeft<%=i%2%>">&nbsp;</td>
    	       <td class="cDataLeft<%=i%2%>">&nbsp;</td>
    	       <td class="cDataLeft<%=i%2%>">&nbsp;</td>
    	       <td class="cDataLeft<%=i%2%>">&nbsp;</td>
    	       <td class="cDataLeft<%=i%2%>">&nbsp;</td>
    	       <td class="cDataLeft<%=i%2%>">&nbsp;</td>
    	       <td class="cDataLeft<%=i%2%>">&nbsp;</td>
    	       </tr>
    	       <%
    	 }
        }
     }
     %>     
 </table>
</td>
</tr>
</table>
</div> 
</body>
</html>