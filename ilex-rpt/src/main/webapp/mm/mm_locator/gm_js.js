/*-----------------------------------------------------------------------------------------------*/
	function get_image(path, shadow_path){
		this.icon = new GIcon();
		this.icon.image = path;
		this.icon.shadow = shadow_path;
		this.icon.iconSize = new GSize(20, 34);
		this.icon.shadowSize = new GSize(37, 34);
		this.icon.iconAnchor = new GPoint(9, 34);
		this.icon.infoWindowAnchor = new GPoint(9, 2);
		this.icon.infoShadowAnchor = new GPoint(18, 25);
		return this.icon;
	}//end of function
/*-----------------------------------------------------------------------------------------------
	This function will return
-----------------------------------------------------------------------------------------------*/
	function get_small_image(path, shadow_path){
		this.icon = new GIcon();
		this.icon.image = path;
		//this.icon.shadow = "http://www.contingent.net/gm_pages/gm_images/shadow50.png";
		this.icon.shadow = shadow_path;
		this.icon.iconSize = new GSize(18, 18);
		this.icon.shadowSize = new GSize(30, 25);
		this.icon.iconAnchor = new GPoint(9, 20);
		this.icon.infoWindowAnchor = new GPoint(9, 2);
		this.icon.infoShadowAnchor = new GPoint(18, 25);
		return this.icon;
	}//end of function
/*-----------------------------------------------------------------------------------------------*/
	function createMarker(point, ico, pObj) {
        var marker = new GMarker(point, ico);
        GEvent.addListener(marker, "click", function() {
			marker.openInfoWindowHtml(pObj);
        });
		//clusterer.AddMarker(marker,pObj);
		//map.addMarker(marker);
        return marker;
     }
/*-----------------------------------------------------------------------------------------------*/
	function createMarkerWithClick(point, ico, pObj) {
        var marker = new GMarker(point, ico);
        GEvent.addListener(marker, "click", function() {
			get_data(pObj);
        });
		map.addOverlay(marker);
        return marker;
     }
/*-----------------------------------------------------------------------------------------------*/
function OpenNormal(url){
	ss = window.open(url,"NoralWindow","scrollbars=yes,resizable=yes,toolbar=yes,menubar=yes,location=yes,Top=150,Left=150");
}//end of function
/*----------------------------------------------------------------------------------------------------*/
function MouseWheelZoomHandler(e){
	if(e == null){if(event!=null)	e = event; else if(window.event!=null)e = window.event;}
	if(e!=null){var data=0;if(e.wheelData != null){data=e.wheelData;if(window.opera)data=-data;}else if(e.detail!=null)data=-e.detail;
	if(data>0)mouseWheelZoomMap.setZoom(mouseWheelZoomMap.getZoom()+1);else if(data<0)mouseWheelZoomMap.setZoom(mouseWheelZoomMap.getZoom()-1);
	}
}//end of funciton
/*----------------------------------------------------------------------------------------------------*/
var mouseWheelZoomMap = null;
function MouseWheelZoom(map){
	if(map == mouseWheelZoomMap)
		return;
	if(mouseWheelZoomMap != null)
		MouseWheelZoomOff();
	mouseWheelZoomMap = map;
	var container = mouseWheelZoomMap.getContainer();
	if(container.addEventListener)
		container.addEventListener('DOMMouseScroll',MouseWheelZoomHandler,false);
	else
		container.onmousewheel = window.onmousewheel = document.onmousewheel = MouseWheelZoomHandler;
}//end of fucntion
/*----------------------------------------------------------------------------------------------------*/
function MouseWheelZoomOff(){
	if(mouseWheelZoomMap != null){
		var container=mouseWheelZoomMap.getContainer();
		if(container.removeEventListener)
			container.removeEventListener('DOMMouseScroll',MouseWheelZoomHandler,false);
		else
			container.onmousewheel=window.onmousewheel=document.onmousewheel=null;mouseWheelZoomMap=null;
	}
}//end of funciton
