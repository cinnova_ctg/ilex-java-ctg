<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.util.Map" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%
	Vector rows = (Vector) request.getAttribute("pay_period");
	int count = 0;
	if (rows != null) {
		count = rows.size();
	}

	Map summary = (Map)request.getAttribute("division_summary");
	Properties system = System.getProperties();
		
	String display_pay_button = (String)request.getAttribute("display_pay_button");
	
	if (display_pay_button == null) {
		display_pay_button = "No";
	}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PO Search Function</title>
<style>
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 12px;
    	cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .rptBody02 {
		 padding : 10px 0px 0px 20px;
    }
    .titleLine {
		font-size : 12pt;
		font-weight : bold;
		text-align : left;
		vertical-align : bottom;
		padding : 0px 0px 6px 0px;
    } 
    .sectionTitleLine {
		font-size : 10pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 0px 0px 6px 0px;
    }
    .sectionTitleLineNav {
		font-size : 8pt;
		font-weight : normal;
		text-align : right;
		vertical-align : middle;
		padding : 0px 0px 0px 0px;
    }
    .sectionHeader1, .sectionHeader2, .sectionHeader3, .sectionHeader10 {
    	font-size : 8pt;
		font-weight : bold;
		text-align : left;
		vertical-align : middle;
		padding : 2px 0px 2px 5px;
		background-color: #e0edf5;
    }
        .sectionHeader2 {
 		background-color: #e5f9de;
    }
        .sectionHeader3 {
		background-color: #fff1f0;
    }
        .sectionHeader10 {
		background-color:  #ffccc9;
    }
    .label1, .label2 {
        font-size : 9pt;
		font-weight : bold;
		vertical-align : top;
		padding : 10px 10px 6px 0px;
    }
    .form1 {
        font-size : 10pt;
		font-weight : normal;
		padding : 0px 0px 0px 0px;
    }
    .elementContainer {
    padding : 0px 2px 0px 2px;
    }
    .columnHeader1 {
		font-size : 9pt;
		font-weight : bold;
		text-align : center;
		vertical-align : bottom;
		padding : 3px 3px 3px 3px;
		background-color: #E0E0DC;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 8pt;
		font-weight : normal;
		text-align : left;
		vertical-align : top;
		padding : 1px 4px 1px 4px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #fffced;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ffffff;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }        
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
		text-align : center;
		width: 26px;
        height: 24px;
    }  
</style>
</head>
<body>
<div class="rptBody01">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr><td class="titleLine"><%=(String) request.getAttribute("window_type")%> Minuteman Pay Period: <%=(String) request.getAttribute("start_date")%> - <%=(String) request.getAttribute("end_date")%>
 <%
 if (display_pay_button.equals("Yes")) {
 // Include form to launch pay window
 %>
  <a href="/rpt/MM.xo?form_action=pay_display">Pay this Period</a>
 <%
 }
 %>
 </td></tr>
 </table>
 </td>
</tr>
 
<tr>
<td>
<br/>
 <table border="0" cellspacing="1" cellpadding="0">
   	<tr>
   	<td rowspan="2" class="columnHeader1">Minuteman</td>
	<td rowspan="2" class="columnHeader1">PO&nbsp;#</td>
	<td rowspan="2" class="columnHeader1">Offsite</td>
	<td rowspan="2" class="columnHeader1">Owner</td>	
	<td colspan="5" class="columnHeader1">Cost</td>
	<td colspan="3" class="columnHeader1">Job Information</td>
	</tr>

	<tr>
    <td class="columnHeader1">Authotized</td>
	<td class="columnHeader1">&nbspApproved&nbsp</td>
	<td class="columnHeader1">&nbsp&nbspLabor&nbsp&nbsp&nbsp</td>
	<td class="columnHeader1">&nbsp&nbspTravel&nbsp&nbsp</td>
	<td class="columnHeader1">&nbspHardware&nbsp</td>
	<td class="columnHeader1">Project</td>
	<td class="columnHeader1">Title</td>
	<td class="columnHeader1">Status</td>
	</tr>
   
     <%
        	String current_po = "";
        	String previous_po = "";
        	String display_division = "";
        	String current_division = "";
        	String previous_division = "";
        	String[] row_summary;
        	int pass = 0;

        	String current_status = "";
        	String previous_status = "";

        	String mailto = "";
        	String msg_body = "";

        	String section_class = "";
        	String section_title = "";

        	int i = 0;
        	if (rows != null) {
        		
        		for (int j = 0; j < rows.size(); j++) {

        			String[] r = (String[]) rows.get(j);

        			// Check for partner change - if changed create summary line
        			current_division = r[2];
        			current_status = r[4];
        			
        			if (!current_division.equals(previous_division)) {
        		       row_summary = (String[]) summary.get(previous_status+previous_division);
        		       previous_division = current_division;
        		       display_division = current_division;
        		       if (pass > 0) {
        %>
    <tr>
    <td class="cDataLeft0">&nbsp;</td>
    <td class="cDataLeft0">&nbsp;</td>
    <td class="cDataLeft0">&nbsp;</td>
    <td class="cDataRight0">&nbsp;</td>  
    <td class="cDataRight0"><b>Totals:</b></td>
    <td class="cDataRight0"><b><%=row_summary[0]%></b></td>
    <td class="cDataRight0"><b><%=row_summary[1]%></b></td>
    <td class="cDataRight0"><b><%=row_summary[2]%></b></td>
    <td class="cDataRight0"><b><%=row_summary[3]%></b></td>
    <td class="cDataLeft0">&nbsp;</td>
    <td class="cDataLeft0">&nbsp;</td>
    <td class="cDataLeft0">&nbsp;</td>
    </tr>
<%
		                  }
		               pass++;

			          } // 

			        current_status = r[4];
			        if (!current_status.equals(previous_status)) {

		                previous_status = current_status;

						if (current_status.equals("1")) {
							section_class = "sectionHeader1";
							section_title = "Ready for Payment";
						} else if (current_status.equals("2")) {
							section_class = "sectionHeader2";
							section_title = "Cost Information Missing or Inconsistent";
						} else if (current_status.equals("3")) {
							section_class = "sectionHeader3";
							section_title = "Deliverables Outstanding";
						} else {
							section_class = "sectionHeader10";
							section_title = "Job or PO Inconsitency";
						}
%>
	<tr> 
	<td colspan="12" class="<%=section_class%>"><%=section_title%></td>
	</tr>
<%
					} // if (!current_status.equals(previous_status))

					
				if (r[4].equals("1")) {
					mailto = r[9];
				} else {
					msg_body = "Please review the Minuteman information below for a job which is due for immediate payment but is missing information.%0A%0A";
					msg_body += "   Project: " + r[8] + "%0A";
					msg_body += "   Job: " + r[3] + "%0A";
					msg_body += "   PO: " + r[1] + "%0A%0A";
					msg_body += "For additional information, visit the reporting page for the current Minuteman pay period.";

					mailto = "<a href=\"mailto:"
					+ r[18]
					+ "?subject=Minuteman Payment Review - Immediate Action Required&body="
					+ msg_body + "\">" + r[9] + "</a>";
				}
%>
	<tr>
	<td nowrap class="cDataLeft1"><%=display_division%></td>
 	<td class="cDataLeft1"><a href="http://ilex.contingent.local/Ilex/POWODetailAction.do?powoid=<%=r[1]%>&jobid=<%=r[0]%>&viewjobtype=A&ownerId=%"><%=r[1]%></a></td>
	<td class="cDataCenter1"><span class="cDataCenter1"><%=r[10]%></span></td>
	<td class="cDataLeft1"><span class="cDataLeft1"><%=mailto%></span></td>        
	<td class="cDataCenter1"><%=r[12]%></td>
	<td class="cDataRight1"><%=r[13]%></td>
	<td class="cDataRight1"><%=r[16]%></td>
	<td class="cDataRight1"><%=r[15]%></td>
	<td class="cDataRight1"><%=r[14]%></td>
	<td class="cDataLeft1"><%=r[8]%></td>
	<td class="cDataLeft1"><%=r[3]%></td>
	<td class="cDataLeft1"><%=r[5]%></td>
</tr>
<%
                 display_division = "";
                 previous_status = current_status;
    	     	}
        	  }

    	    row_summary = (String[]) summary.get(previous_status+previous_division);
    	    previous_division = current_division;
%>
    <tr>
    <td class="cDataLeft0">&nbsp;</td>
    <td class="cDataLeft0">&nbsp;</td>
    <td class="cDataLeft0">&nbsp;</td>
    <td class="cDataRight0">&nbsp;</td>  
    <td class="cDataRight0"><b>Total</b></td>
    <td class="cDataRight0"><b><%=row_summary[0]%></b></td>
    <td class="cDataRight0"><b><%=row_summary[1]%></b></td>
    <td class="cDataRight0"><b><%=row_summary[2]%></b></td>
    <td class="cDataRight0"><b><%=row_summary[3]%></b></td>
    <td class="cDataLeft0">&nbsp;</td>
    <td class="cDataLeft0">&nbsp;</td>
    <td class="cDataLeft0">&nbsp;</td>
    </tr>
    
 </table>
</td>
</tr>
</table>
</div> 
</body>
</html>