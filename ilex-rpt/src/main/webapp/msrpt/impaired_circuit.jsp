<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
	<head>
		<script src="FusionCharts/FusionCharts.js"></script>
    </head>
	<body style="margin: 15px;">
		<div id="chartdiv" align="center" style="margin: 10px;">The chart will appear within this DIV. This text will be replaced by the chart.</div>
		<script type="text/javascript">
			var myChart = new FusionCharts("FusionCharts/MSLine4.swf", "myChartId", "1200", "800", "0", "0");
	        myChart.setDataXML("<chart caption='Daily Impaired Circuit Count' xAxisName='Date' yAxisName='Amount'>" +
					"   <categories>" +
					"	<category Label='12/5/2011' /> " +
					"	<category Label='12/6/2011' /> " +
					"	<category Label='12/7/2011' /> " +
					"	<category Label='12/8/2011' /> " +
					"	<category Label='12/9/2011' /> " +
					"	<category Label='12/10/2011' /> " +
					"	<category Label='12/11/2011' /> " +
					"	<category Label='12/12/2011' /> " +
					"	<category Label='12/13/2011' /> " +
					"	<category Label='12/14/2011' /> " +
					"	<category Label='12/15/2011' /> " +
					"	<category Label='12/16/2011' /> " +
					"	<category Label='12/17/2011' /> " +
					"	<category Label='12/18/2011' /> " +
					"	<category Label='12/19/2011' /> " +
					"	<category Label='12/20/2011' /> " +
					"	<category Label='12/21/2011' /> " +
					"	<category Label='12/22/2011' /> " +
					"	<category Label='12/23/2011' /> " +
					"	<category Label='12/24/2011' /> " +
					"	<category Label='12/25/2011' /> " +
					"	<category Label='12/26/2011' /> " +
					"	<category Label='12/27/2011' /> " +
					"	<category Label='12/28/2011' /> " +
					"	<category Label='12/29/2011' /> " +
					"	<category Label='12/30/2011' /> " +
					"	<category Label='12/31/2011' /> " +
					"	<category Label='1/1/2012' /> " +
					"	<category Label='1/2/2012' /> " +
					"	<category Label='1/3/2012' /> " +
					"   </categories>" +
					"   <dataset seriesName='&lt; 1'>" +
					"       <set  value='48' />" +
					"       <set  value='122' />" +
					"       <set  value='78' />" +
					"       <set  value='75' />" +
					"       <set  value='60' />" +
					"       <set  value='50' />" +
					"       <set  value='36' />" +
					"       <set  value='38' />" +
					"       <set  value='100' />" +
					"       <set  value='92' />" +
					"       <set  value='97' />" +
					"       <set  value='89' />" +
					"       <set  value='83' />" +
					"       <set  value='1028' />" +
					"       <set  value='53' />" +
					"       <set  value='83' />" +
					"       <set  value='113' />" +
					"       <set  value='115' />" +
					"       <set  value='169' />" +
					"       <set  value='122' />" +
					"       <set  value='40' />" +
					"       <set  value='49' />" +
					"       <set  value='113' />" +
					"       <set  value='108' />" +
					"       <set  value='136' />" +
					"       <set  value='118' />" +
					"       <set  value='108' />" +
					"       <set  value='90' />" +
					"       <set  value='120' />" +
					"       <set  value='1053' />" +
					"   </dataset>" +
					"   <dataset seriesName='&lt; 2'> " +
					"       <set  value='39' />" +
					"       <set  value='124' />" +
					"       <set  value='82' />" +
					"       <set  value='79' />" +
					"       <set  value='53' />" +
					"       <set  value='42' />" +
					"       <set  value='34' />" +
					"       <set  value='46' />" +
					"       <set  value='95' />" +
					"       <set  value='101' />" +
					"       <set  value='82' />" +
					"       <set  value='94' />" +
					"       <set  value='99' />" +
					"       <set  value='756' />" +
					"       <set  value='37' />" +
					"       <set  value='89' />" +
					"       <set  value='82' />" +
					"       <set  value='125' />" +
					"       <set  value='115' />" +
					"       <set  value='86' />" +
					"       <set  value='45' />" +
					"       <set  value='42' />" +
					"       <set  value='61' />" +
					"       <set  value='100' />" +
					"       <set  value='110' />" +
					"       <set  value='89' />" +
					"       <set  value='92' />" +
					"       <set  value='72' />" +
					"       <set  value='124' />" +
					"       <set  value='992' />" +
					"   </dataset>" +
					"   <dataset seriesName='&lt; 3'> " +
					"       <set  value='42' />" +
					"       <set  value='88' />" +
					"       <set  value='79' />" +
					"       <set  value='83' />" +
					"       <set  value='56' />" +
					"       <set  value='44' />" +
					"       <set  value='42' />" +
					"       <set  value='43' />" +
					"       <set  value='96' />" +
					"       <set  value='88' />" +
					"       <set  value='93' />" +
					"       <set  value='76' />" +
					"       <set  value='74' />" +
					"       <set  value='553' />" +
					"       <set  value='30' />" +
					"       <set  value='64' />" +
					"       <set  value='85' />" +
					"       <set  value='104' />" +
					"       <set  value='75' />" +
					"       <set  value='57' />" +
					"       <set  value='38' />" +
					"       <set  value='44' />" +
					"       <set  value='1' />" +
					"       <set  value='128' />" +
					"       <set  value='81' />" +
					"       <set  value='71' />" +
					"       <set  value='78' />" +
					"       <set  value='75' />" +
					"       <set  value='125' />" +
					"       <set  value='909' />" +
					"   </dataset>" +
					"   <dataset seriesName='&lt; 4'> " +
					"       <set  value='41' />" +
					"       <set  value='85' />" +
					"       <set  value='80' />" +
					"       <set  value='73' />" +
					"       <set  value='65' />" +
					"       <set  value='48' />" +
					"       <set  value='34' />" +
					"       <set  value='53' />" +
					"       <set  value='94' />" +
					"       <set  value='72' />" +
					"       <set  value='79' />" +
					"       <set  value='68' />" +
					"       <set  value='74' />" +
					"       <set  value='459' />" +
					"       <set  value='30' />" +
					"       <set  value='56' />" +
					"       <set  value='65' />" +
					"       <set  value='52' />" +
					"       <set  value='71' />" +
					"       <set  value='52' />" +
					"       <set  value='38' />" +
					"       <set  value='40' />" +
					"       <set  value='43' />" +
					"       <set  value='61' />" +
					"       <set  value='85' />" +
					"       <set  value='62' />" +
					"       <set  value='71' />" +
					"       <set  value='88' />" +
					"       <set  value='172' />" +
					"       <set  value='807' />" +
					"   </dataset>" +
					"</chart>");
	        myChart.render("chartdiv");
		</script>
	</body>
</html>