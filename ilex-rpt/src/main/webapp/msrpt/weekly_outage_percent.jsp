<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
	<head>
		<script src="msrpt/FusionCharts/FusionCharts.js"></script>
		<script type="text/javascript">
			chart_data = "<chart caption='Personnel and Outage Percentage by Week' xAxisName='Week' yAxisName='Amount' labelDisplay='Rotate'>"+
             "<%=request.getAttribute("chart_data") %>"+
             "</chart>";
		</script>
    </head>
	<body style="margin: 15px;">
		<div id="chartdiv" align="center" style="margin: 10px;">The chart will appear within this DIV. This text will be replaced by the chart.</div>
		<script type="text/javascript">
			var myChart = new FusionCharts("msrpt/FusionCharts/MSLine4.swf", "myChartId", "1200", "800", "0", "0");
			myChart.setDataXML(chart_data);
			myChart.render("chartdiv");
		</script>
	</body>
</html>