<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="refresh" content="300">
<title>Minuteman/Speedpay Targets-2011</title>
<style> 
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 12px;
    	cursor : auto;
    	background-color: #000000;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .rptBody02 {
		 padding : 10px 0px 0px 20px;
    }
    .chart {
		vertical-align : Middle;
		align : Center;
		padding : 5px 5px 5px 30px;
    } 
    .titleLine {
		font-size : 12pt;
		font-weight : bold;
		text-align : left;
		vertical-align : Middle;
		padding : 0px 0px 6px 0px;
		color: #ffffff;
    } 
    .sectionTitleLine {
		font-size : 11pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 12px 0px 6px 0px;
		color: #2E3D80;
    }
    .sectionTitleLineNav {
		font-size : 8pt;
		font-weight : normal;
		text-align : right;
		vertical-align : middle;
		padding : 0px 0px 0px 0px;
    }
    .label1, .label2 {
        font-size : 9pt;
		font-weight : bold;
		vertical-align : top;
		padding : 10px 10px 6px 0px;
    }
    .form1 {
        font-size : 10pt;
		font-weight : normal;
		padding : 0px 0px 0px 0px;
    }
    .elementContainer {
    padding : 0px 2px 0px 2px;
    }
    .columnHeader1 {
		font-size : 10pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 3px 5px 3px 5px;
		background-color: #E0E0DC; 
    }    
    .columnHeader2 {
		font-size : 9pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 16px 16px 16px 16px;
		background-color: #E0E0DC; 
    }    
    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 9pt;
		font-weight : normal;
		text-align : left;
		vertical-align : middle;
		padding : 1px 5px 1px 5px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #ffffff;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ECECEC;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }        
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
		text-align : center;
		width: 26px;
        height: 24px;
    }
    .date1 {
        font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
    }
    .bottomLine, .bottomLineCenter {
    	font-size : 8pt;
		font-weight : bold;
		text-align : right;
		vertical-align : middle;
		padding : 2px 2px 2px 0px;
		background-color: #e0edf5;
    }
    .bottomLineCenter {
		text-align : center;
    }
    .outlineYellow3 {
	padding: 0px;
	margin: 4px 4px 4px 0px;
	border: 1px solid #FAF8CC;
	background: #ffffff;
   }
    .outlineClear3 {
	padding: 0px;
	margin: 4px 4px 4px 0px;
	border: 2px solid #ffffff;
	background: #ffffff;
   }
   .outlineGray {
	padding: 4px;
	margin: 4px 4px 4px 0px;
	border: 1px solid #E0E0DC;
	background: #ffffff;
   }
   .outlineTopLevel {
	padding: 4px;
	border: 2px solid #E0E0DC;
	background: #ffffff;
   }
   .cTotalRight1 {
	   	font-size : 9pt;
		font-weight : Bold;
		text-align : right;
		vertical-align : middle;
		padding : 3px 4px 3px 4px;
		background: #E0E0DC;
    }    
   .cLinkRight1 {
	   	font-size : 9pt;
		font-weight : normal;
		text-align : right;
		vertical-align : middle;
		padding : 0px 4px 6px 0px;
    }    
 
    .cred0, .cblue0, .cgreen0 {
		font-size : 9pt;
		font-weight : normal;
		text-align : center;
		vertical-align : middle;
		padding : 10px 5px 10px 5px;
    }    
    .cred0 {
		background-color : #ff9999;
    }   
    .cgreen0 {
		background-color : #99ff99;
    }   
    .cblue0 {
		background-color : #9999ff;
    }
 
</style>
<script language="JavaScript" src="vgp/FusionCharts.js"></script>
</head>
<body>
<div style="padding:10px;">
<table border="0" cellspacing="0" cellpadding="0" align="center">
<tr><td>
<table  border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" Align="center">
<div style="padding:20px;">
<div class="titleLine">Job Owners</div>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#000000"><%=request.getAttribute("heat1")%></td>
</tr>
</table>
</div>
</td>
</tr>
<tr>
<!-- Left -->
<td valign="top">
<div style="padding:10px;">
<div class="titleLine">Monthly Team Goals (<%=request.getAttribute("month") %>)</div>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="4" bgcolor="#000000"><%=request.getAttribute("heat2")%></td>
</tr>
</table>
</div>
</td>
<!-- Center -->
<td valign="top">
<div style="padding:10px;">
<div class="titleLine">Teams</div>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><%=request.getAttribute("prog_chart1")%></td>
</tr>
</table>
</div>
</td>
<!-- Right -->
<td valign="top" bgcolor='#000000'>
<div style="padding:10px;">
<div class="titleLine">Company</div>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><%=request.getAttribute("chart1")%></td>
</tr>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td colspan="3">
<div style="padding:20px; color:#ffffff; font-size : 9px;'">
Job Owner Color Codes:&nbsp;&nbsp;<span style="color: #ff9999;">RED</span> < Average of Monthly Team Goals,&nbsp;&nbsp;<span style="color: #9999ff;">BLUE</span> > Average of Monthly Team Goals,&nbsp;&nbsp;and <span style="color: #99ff99;">GREEN</span> > One Standard Deviation Above Average of Monthly Team Goals
</div>
</td>
</tr>
</table>
</div>
</body>
</html>