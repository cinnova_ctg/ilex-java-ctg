<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.text.DateFormat" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.text.ParseException" %>
<%@ page import = "java.util.Date" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style> 
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 12px;
    	cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .rptBody02 {
		 padding : 10px 0px 0px 20px;
    }
    .chart {
		vertical-align : Middle;
		align : Center;
		padding : 5px 5px 5px 30px;
    } 
    .titleLine {
		font-size : 12pt;
		font-weight : bold;
		text-align : left;
		vertical-align : Middle;
		padding : 0px 0px 6px 0px;
    } 
    .sectionTitleLine {
		font-size : 11pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 12px 0px 6px 0px;
		color: #2E3D80;
    }
    .sectionTitleLineNav {
		font-size : 8pt;
		font-weight : normal;
		text-align : right;
		vertical-align : middle;
		padding : 0px 0px 0px 0px;
    }
    .label1, .label2 {
        font-size : 9pt;
		font-weight : bold;
		vertical-align : top;
		padding : 10px 10px 6px 0px;
    }
    .form1 {
        font-size : 10pt;
		font-weight : normal;
		padding : 0px 0px 0px 0px;
    }
    .elementContainer {
    padding : 0px 2px 0px 2px;
    }
    .columnHeader1 {
		font-size : 10pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 3px 5px 3px 5px;
		background-color: #E0E0DC; 
    }    
    .columnHeader2 {
		font-size : 9pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 3px 12px 3px 12px;
		background-color: #DBE4F1; 
    }    
    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 9pt;
		font-weight : normal;
		text-align : left;
		vertical-align : middle;
		padding : 1px 5px 1px 5px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #ffffff;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ECECEC;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }        
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
		text-align : center;
		width: 26px;
        height: 24px;
    }
    .date1 {
        font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
    }
    .bottomLine, .bottomLineCenter {
    	font-size : 8pt;
		font-weight : bold;
		text-align : right;
		vertical-align : middle;
		padding : 2px 2px 2px 0px;
		background-color: #e0edf5;
    }
    .bottomLineCenter {
		text-align : center;
    }
    .outlineYellow3 {
	padding: 0px;
	margin: 4px 4px 4px 0px;
	border: 1px solid #FAF8CC;
	background: #ffffff;
   }
    .outlineClear3 {
	padding: 0px;
	margin: 4px 4px 4px 0px;
	border: 2px solid #ffffff;
	background: #ffffff;
   }
   .cTotalRight1 {
	   	font-size : 9pt;
		font-weight : Bold;
		text-align : right;
		vertical-align : middle;
		padding : 3px 4px 3px 4px;
		background: #E0E0DC;
    }    
   .cLinkRight1 {
	   	font-size : 9pt;
		font-weight : normal;
		text-align : right;
		vertical-align : middle;
		padding : 0px 4px 6px 0px;
    }    
 
</style>

<script language="JavaScript" src="vgp/FusionCharts.js"></script>
</head>
<body>
<%
String spm = "";
String jo = "";
SimpleDateFormat dformat = new SimpleDateFormat("MMM-yyyy");
SimpleDateFormat pformat = new SimpleDateFormat("yyyy-M-dd");

String sreport = request.getParameter("report");
if (sreport == null || sreport.equals("")) {
	sreport = "0";
}

String link = (String)request.getAttribute("link");
if (link == null) {
	link = "";
}

int report = Integer.parseInt(sreport);

Vector rows = (Vector)request.getAttribute("rows");
String[] row;

if (rows != null && rows.size() > 0) {

%>
<div style="padding:20px;">
<div class="titleLine"><%=request.getAttribute("title") %></div>
<div>
<%
switch (report) {

case 0: 
	
	 double x;
	 double y;

	String xml = "";
	String xml_static1 =  " \"<chart caption='Purchasing Program Trends' numbersuffix='%25' yaxisminvalue='0' yaxismaxvalue='100' xAxisName='Sr. PM' yAxisName='%' showValues='1' numberPrefix='' borderColor='#ffffff'> \\\n";
	String xml_static2 = 
		"<styles>"+
		"<definition>"+
		"<style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />"+
		"</definition>"+
		"<application>"+
		"<apply toObject='Canvas' styles='CanvasAnim' />"+
		"</application>"+
		"</styles>"+
		"</chart>\";";

	String s1 = "<dataset seriesName='MM & SP' color='0000FF' showValue='1'>";
	String s2 = "<dataset seriesName='PVS'  color='FF0000' showValue='1'>"; 
	String cat = "<categories>";
	
	String ms_line_chart = 
		"<div id='ttm_chart' align='center'>Waiting...</div>\n"+
	    "<script type='text/javascript'>\n"+
	        "var purchase_chart1 = ~data_url~"+
	        "\nvar myChart = new FusionCharts('vgp/StackedColumn2D.swf', 'ttm_chart', '500', '300', '0', '0');\n"+
	        "myChart.setDataXML(purchase_chart1);\n"+
	        "myChart.setTransparent(true);\n"+
	        "myChart.render('ttm_chart');\n"+
	    "</script>";
%>
  
<!-- Report 1 -->
<table border="0" cellspacing="0" cellpadding="0">
<tr><td valign="top" >
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
<tr>
  <td class="columnHeader2">Sr. PM</td>
  <td class="columnHeader2">MM</td>
  <td class="columnHeader2">SP</td>
  <td class="columnHeader2">PVS</td>
  <td class="columnHeader2">Total</td>
  <td class="columnHeader2">Drill Downs</td>
</tr>

<%
for (int i=0; i<rows.size(); i++) {
	row = (String[]) rows.get(i);
	spm = row[0].replaceAll(" ", "+");
	
	cat += "<category label='"+row[0].substring(0,row[0].indexOf(","))+"' />";
	x = (Double.parseDouble(row[1])+Double.parseDouble(row[2]))*100;
	s1 += "<set value='"+String.valueOf(x)+"' />";
	x = Double.parseDouble(row[3])*100;
	s2 += "<set value='"+String.valueOf(x)+"' />";

%>
<tr>
  <td class="cDataLeft<%=i%2%>"><%=row[0]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[1]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[2]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[3]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[4]%></td>
  <td class="cDataCenter<%=i%2%>"><a href="POA1.xo?ac=report1&report=3&spm=<%=spm%>">Job Owner</a> | <a href="POA1.xo?ac=report1&report=2&spm=<%=spm%>">Project</a> | <a href="POA1.xo?ac=report1&report=1&spm=<%=spm%>">TTM</a></td>
</tr>
<%		
} // Report 1 for loop

cat += "</categories> \\\n";
s1 += "</dataset> \\\n";
s2 += "</dataset> \\\n";

xml = xml_static1+cat+s1+s2+xml_static2;
ms_line_chart = ms_line_chart.replace("~data_url~", xml);

%>
</table>
</td>
<td valign="top" style="padding-left:20px;"><%=ms_line_chart %>
</td>
</tr>
</table>

<% break; // case 0

case 1: 
	xml = "";
	xml_static1 =  " \"<chart caption='Purchasing Program Trends' numbersuffix='%25' yaxisminvalue='0' yaxismaxvalue='100' xAxisName='Month' yAxisName='%' showValues='0' numberPrefix='' borderColor='#ffffff'> \\\n";
	xml_static2 = 
		"<styles>"+
		"<definition>"+
		"<style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />"+
		"</definition>"+
		"<application>"+
		"<apply toObject='Canvas' styles='CanvasAnim' />"+
		"</application>"+
		"</styles>"+
		"</chart>\";";

	s1 = "<dataset seriesName='MM'>";
	s2 = "<dataset seriesName='SP'>";
	String s3 = "<dataset seriesName='PVS' color='FF0000'>";
	String s4 = "<dataset seriesName='MM & SP' color='0000ff' showValue='1' alpha='100' anchorAlpha='0' lineThickness='3'>";
	cat = "<categories>";
	
	ms_line_chart = 
		"<div id='ttm_chart' align='center'>Waiting...</div>\n"+
	    "<script type='text/javascript'>\n"+
	        "var purchase_chart1 = ~data_url~"+
	        "\nvar myChart = new FusionCharts('vgp/MSLine.swf', 'ttm_chart', '500', '300', '0', '0');\n"+
	        "myChart.setDataXML(purchase_chart1);\n"+
	        "myChart.setTransparent(true);\n"+
	        "myChart.render('ttm_chart');\n"+
	    "</script>";
%>

<!-- Report TTM -->

<table border="0" cellspacing="0" cellpadding="0">
<tr><td valign="top" >
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
<tr>
  <td class="columnHeader2">Month</td>
  <td class="columnHeader2">MM</td>
  <td class="columnHeader2">SP</td>
  <td class="columnHeader2">PVS</td>
  <td class="columnHeader2">Total</td>
</tr>

<%
for (int i=0; i<rows.size(); i++) {
	row = (String[]) rows.get(i);
	row[0] = dformat.format(pformat.parse(row[0].replace("\\%","\\.")));
	
	cat += "<category label='"+row[0].substring(0,3)+"' />";
	x = Double.parseDouble(row[1])*100;
	y = x;
	s1 += "<set value='"+String.valueOf(x)+"' />";
	x = Double.parseDouble(row[2])*100;
	y += x;
	s2 += "<set value='"+String.valueOf(x)+"' />";
	x = Double.parseDouble(row[3])*100;
	s3 += "<set value='"+String.valueOf(x)+"' />";
	s4 += "<set value='"+String.valueOf(y)+"' />";
%>
<tr>
  <td class="cDataLeft<%=i%2%>"><%=row[0]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[1]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[2]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[3]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[4]%></td>
</tr>
<%		
} // Report 1 for loop

cat += "</categories> \\\n";
s1 += "</dataset> \\\n";
s2 += "</dataset> \\\n";
s3 += "</dataset> \\\n";
s4 += "</dataset> \\\n";

xml = xml_static1+cat+s1+s2+s3+s4+xml_static2;
ms_line_chart = ms_line_chart.replace("~data_url~", xml);
%>
</table>
</td>
<td valign="top" style="padding-left:20px;"><%=ms_line_chart %>
</td>
</tr>
</table>

<% break; // case 1

case 2: %>

<!-- Report 2 -->

<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
<tr>
  <td class="columnHeader2">Customer/Appendix</td>
  <td class="columnHeader2">MM</td>
  <td class="columnHeader2">SP</td>
  <td class="columnHeader2">PVS</td>
  <td class="columnHeader2">Total</td>
</tr>
<%
for (int i=0; i<rows.size(); i++) {
	row = (String[]) rows.get(i);
%>
<tr>
  <td class="cDataLeft<%=i%2%>"><%=row[0]%>/<%=row[1]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[2]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[3]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[4]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[5]%></td>
</tr>
<%		
} // Report 1 for loop
%>
</table>

<% break; // case 2

case 3: %>

<!-- Report 3 -->

<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
<tr>
  <td class="columnHeader2">Job Owner</td>
  <td class="columnHeader2">MM</td>
  <td class="columnHeader2">SP</td>
  <td class="columnHeader2">PVS</td>
  <td class="columnHeader2">Total</td>
</tr>

<%
for (int i=0; i<rows.size(); i++) {
	row = (String[]) rows.get(i);
%>
<tr>
  <td class="cDataLeft<%=i%2%>"><%=row[0]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[1]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[2]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[3]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[4]%></td>
</tr>
<%		
} // Report 1 for loop
%>
</table>

<% break; // case 3


case 4: %>

<!-- Report 1 -->

<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
<tr>
  <td class="columnHeader2">Job Owner</td>
  <td class="columnHeader2">MM</td>
  <td class="columnHeader2">SP</td>
  <td class="columnHeader2">PVS</td>
  <td class="columnHeader2">Total</td>
  <td class="columnHeader2">Drill Downs</td>
</tr>

<%
for (int i=0; i<rows.size(); i++) {
	row = (String[]) rows.get(i);
	jo = row[0].replaceAll(" ", "+");
%>
<tr>
  <td class="cDataLeft<%=i%2%>"><%=row[0]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[1]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[2]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[3]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[4]%></td>
  <td class="cDataCenter<%=i%2%>"><a href="POA1.xo?ac=report1&report=5&jo=<%=jo%>">Project</a> | <a href="POA1.xo?ac=report1&report=6&jo=<%=jo%>">TTM</a></td>
</tr>
<%		
} // Report 1 for loop
%>
</table>

<% break; // case 4

case 5: %>

<!-- Report 1 -->
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
<tr>
  <td class="columnHeader2">Customer/Appendix</td>
  <td class="columnHeader2">MM</td>
  <td class="columnHeader2">SP</td>
  <td class="columnHeader2">PVS</td>
  <td class="columnHeader2">Total</td>
</tr>

<%
for (int i=0; i<rows.size(); i++) {
	row = (String[]) rows.get(i);
%>
<tr>
  <td class="cDataLeft<%=i%2%>"><%=row[0]%>/<%=row[1]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[2]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[3]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[4]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[5]%></td>
</tr>
<%		
} // Report 1 for loop
%>
</table>

<% break; // case 5

case 6:
	xml = "";
	xml_static1 =  " \"<chart caption='Purchasing Program Trends' numbersuffix='%25' yaxisminvalue='0' yaxismaxvalue='100' xAxisName='Month' yAxisName='Type %' showValues='0' numberPrefix='' borderColor='#ffffff'> \\\n";
	xml_static2 = 
		"<styles>"+
		"<definition>"+
		"<style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />"+
		"</definition>"+
		"<application>"+
		"<apply toObject='Canvas' styles='CanvasAnim' />"+
		"</application>"+
		"</styles>"+
		"</chart>\";";

	s1 = "<dataset seriesName='MM'>";
	s2 = "<dataset seriesName='SP'>";
	s3 = "<dataset seriesName='PVS' color='FF0000'>";
	s4 =  "<dataset seriesName='MM & SP' color='0000FF' showValue='1' alpha='100' anchorAlpha='0' lineThickness='3'>";

	cat = "<categories>";
	
	ms_line_chart = 
		"<div id='ttm_chart' align='center'>Waiting...</div>\n"+
	    "<script type='text/javascript'>\n"+
	        "var purchase_chart1 = ~data_url~"+
	        "\nvar myChart = new FusionCharts('vgp/MSLine.swf', 'ttm_chart', '500', '300', '0', '0');\n"+
	        "myChart.setDataXML(purchase_chart1);\n"+
	        "myChart.setTransparent(true);\n"+
	        "myChart.render('ttm_chart');\n"+
	    "</script>";
%>

<!-- Report TTM -->

<table border="0" cellspacing="0" cellpadding="0">
<tr><td valign="top" >
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">
<tr>
  <td class="columnHeader2">Month</td>
  <td class="columnHeader2">MM</td>
  <td class="columnHeader2">SP</td>
  <td class="columnHeader2">PVS</td>
  <td class="columnHeader2">Total</td>
</tr>

<%
for (int i=0; i<rows.size(); i++) {
	row = (String[]) rows.get(i);
	row[0] = dformat.format(pformat.parse(row[0].replace("\\%","\\.")));
	
	cat += "<category label='"+row[0].substring(0,3)+"' />";
	x = Double.parseDouble(row[1])*100;
	y = x;
	s1 += "<set value='"+String.valueOf(x)+"' />";
	x = Double.parseDouble(row[2])*100;
	y += x;
	s2 += "<set value='"+String.valueOf(x)+"' />";
	x = Double.parseDouble(row[3])*100;
	s3 += "<set value='"+String.valueOf(x)+"' />";
	s4 += "<set value='"+String.valueOf(y)+"' />";

%>
<tr>
  <td class="cDataLeft<%=i%2%>"><%=row[0]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[1]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[2]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[3]%></td>
  <td class="cDataRight<%=i%2%>"><%=row[4]%></td>
</tr>
<%		
} // Report 1 for loop

cat += "</categories> \\\n";
s1 += "</dataset> \\\n";
s2 += "</dataset> \\\n";
s3 += "</dataset> \\\n";
s4 += "</dataset> \\\n";

xml = xml_static1+cat+s1+s2+s3+s4+xml_static2;
ms_line_chart = ms_line_chart.replace("~data_url~", xml);
%>
</table>
</td>
<td valign="top" style="padding-left:20px;"><%=ms_line_chart %>
</td>
</tr>
</table>

<% break; // case 6

}//switch()

if (!link.equals("")) {
%>
<a href="<%=link%>">Top</a>
<%	
}
%>
<div>
</div>
<%
} // if (rows != null && rows.size() > 0) {
%>

</body>
</html>