<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style>
body {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	margin: 0px 0px 0px 0px;
	padding: 0px 0px 0px 0px;
	color: #000000;
}

.top {
	margin: 0px 0px 0px 0px;
	padding: 0px 0px 0px 0px;
	width: 1040px;
	border: 0px solid #a9a8ad;
}

.toprow {
	width: 100%;
	border: 0px solid #a9a8ad;
}

.left {
	width: 390px;
	height: 100%;
	border: 0px solid #a9a8ad;
}

.main {
	width: 420px;
	height: 100%;
	border: 5px solid #a9a8ad;
}

.right {
	width: 400px;
	height: 100%;
	border: 0px solid #a9a8ad;
}

.top, .left, .main, .right {
   border: 10px solid #a9a8ad;
   border: 12px solid #ffffff;
}

.title {
	font-size: 16px; font-weight: bold; color: #CCBE97; text-align: left; vertical-align: bottom; padding: 2px 0px 6px 0px;
}
.sectiontitle {
	font-size: 12px; font-weight: bold; color: #CCBE97; text-align: left; vertical-align: bottom; padding: 6px 4px 8px 0px;
}
.subsectiontitle {
	font-size: 11px; font-weight: bold; color: #A8A9AF; text-align: left; vertical-align: bottom; padding: 6px 4px 8px 6px;
}
.ch1a, .ch1b, .ch1c {
	font-size: 11px; font-weight: bold; color: #A8A9AF; text-align: left; vertical-align: bottom; padding: 2px 2px 2px 10px;
	border-bottom: 0px solid #BFBEC4;
}
.ch1b {
	text-align: center;
}
.ch1c {
	text-align: right;
}

.rh1a, .rh1b, .rh1c {
	font-size: 11px; font-weight: normal; color: #A8A9AF; text-align: left; vertical-align: bottom; padding: 2px 2px 2px 0px;
	border-bottom: 0px solid #BFBEC4;
}
.rh1b {
	text-align: center;
}
.rh1c {
	text-align: right;
}

/* Basic Cell Format */

.ccaa,.ccab,.ccac,.ccpa,.ccbb,.ccbc, .ccca,.cccb,.cccc {
	font-size: 10px; font-weight: normal; color: #A8A9AF; text-align: left; padding: 1px 4px 1px 4px;
}
.ccab {
	text-align: center;
}
.ccac {
	text-align: right;
}
/* with Bottom border */
.ccba {
	text-align: left;   border-top: 1px solid #BFBEC4;
}
.ccbb {
	text-align: center; border-top: 1px solid #BFBEC4;
}
.ccbc {
	text-align: right;  border-top: 1px solid #BFBEC4;
}

/* with Top border */
.ccca {
	text-align: left;   border-bottom: 1px solid #BFBEC4;
}
.cccb {
	text-align: center; border-bottom: 1px solid #BFBEC4;
}
.cccc {
	text-align: right;  border-bottom: 1px solid #BFBEC4;
}

/* with Top border */
.chart1 {
     padding: 4px 0px 4px 0px;
}
</style>
<script>
function setDropDownLists () {

  document.getElementById("bdm").value = "0";
  document.getElementById("spm").value = "0";
  if (document.getElementById("jobowner") != null) {
     document.getElementById("jobowner").value = "0";
  }
  
  document.getElementById("selector").submit();
    
  return;
}

function setViewSelection (dlist, type) {

var spm = document.getElementById("spm");
var bdm = document.getElementById("bdm");

  if (type == 'bdm') {
    
    if (spm != null) {
      document.getElementById("spm").value = "0";
      if (document.getElementById("jobowner") != null) {
       document.getElementById("jobowner").value = "0";
      }
    }
    
  } else if (type == 'spm') {
  
   if (bdm != null) {
     document.getElementById("bdm").value = "0";
   }
    
  }
  
  var selector = document.getElementById("selector");
  selector.view[0].value = "0";
  selector.view[1].value = "0";
  selector.view[2].value = "0";
  
  document.getElementById("selector").submit();
  
  return;
}
</script>
<body>
<form id="selector" name="selector" action="DB1.xo" method="post">
<input type="hidden" name="action" value="dash_board1">
<div class="top">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
  <td colspan="3"><div class="title" style="padding: 6px 0px 6px 0px; border-bottom: 1px solid #a9a8ad">Top-level Dashboard</div></td>
<tr>
<td valign="top"><div class="left">

				<table border="0" cellpadding="0" cellspacing="0">
					<tr> 
						<td class="sectiontitle" style="padding: 0px 0px 6px 0px">Current View</td>
					</tr>
					<tr>
						<td>
						<table border="0" cellpadding="0" cellspacing="0">
					    <tr class="ccaa">
					      <td class="subsectiontitle">View&nbsp;Type:</td>
						  <td style="padding: 0px 4px 0px 12px;"><input type="radio" id="view" name ="view"  value="All" class="rbutton" onclick="setDropDownLists()" <%=(((String)(request.getAttribute("view"))).equals("All")) ? " checked" : " unchecked"%>>All</td>
						  <td style="padding: 0px 4px 0px 12px;"><input type="radio" id="view" name ="view" value="Project" class="rbutton" onclick="setDropDownLists()"<%=(((String)(request.getAttribute("view"))).equals("Project")) ? " checked" : " unchecked"%>>Projects</td>
						  <td style="padding: 0px 4px 0px 12px;"><input type="radio" id="view" name ="view" value="Dispatch" class="rbutton" onclick="setDropDownLists()"<%=(((String)(request.getAttribute("view"))).equals("Dispatch")) ? " checked" : " unchecked"%>>Dispatch</td>
						  <td style="padding: 0px 4px 0px 12px; visibility:hidden"><input type="radio" id="view" name ="view" value="MSP" class="rbutton" onclick="setDropDownLists()" unchecked>MSP</td>
					    </tr>
				        </table>
						</td>
					</tr>
			    <tr>
				<td>
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class="subsectiontitle">SPM:</td>
						<td style="padding: 0px 4px 0px 10px;">
						<SELECT class="ccaa" id="spm" name ="spm" onchange="setViewSelection(this, 'spm')">
						    <option value="0">--- Select SPM ---</option>
						    <%=(String)request.getAttribute("SPM_Dropdown")%>
					    </SELECT>
						</td>

						<td class="subsectiontitle">BDM:</td>

						<td style="padding: 0px 4px 0px 10px;">
						<SELECT class="ccaa" ID="bdm" name ="bdm" onchange="setViewSelection(this, 'bdm')">
							<option value="0">--- Select BDM ---</option>
							<%=(String)request.getAttribute("BDM_Dropdown")%>
						</SELECT>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<%
			if (request.getAttribute("spmXX") != null || request.getAttribute("jobownerXX") != null) {
			%>
			<tr>
				<td>
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class="ccaa" style="padding: 0px 4px 0px 50px;">Employees:</td>
						<td style="padding: 0px 4px 0px 5px;">
						<SELECT class="ccaa" NAME="jobowner" name ="jobowner" onchange="setViewSelection(this, 'jobowner')">
						    <option value="0">--- Select Job Owner ---</option> 
						    <%=(String)request.getAttribute("JobOwner_Dropdown")%>
						</SELECT>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<% } %>
		</table>

		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="sectiontitle" colspan="2" style="padding: 16px 0px 10px 0px">Minuteman & Speedpay Usage</td>
			</tr>
			<tr>
				<td class="chart1" valign="top"><%=(request.getAttribute("MMSPChart") != null) ? (String) request
							.getAttribute("MMSPChart")
							: "<tr><td colspan=\"3\" class=\"ccaa\">Data is not available</td></tr>"%></td>
			</tr>
			<tr>
				<td class="subsectiontitle" colspan="2" style="padding: 16px 0px 10px 0px">Breakdown for Minuteman & Speedpay Use</td>
			</tr>
			
			<tr>
				<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class="ch1b">Month</td>
						<td class="ch1b">MM</td>
						<td class="ch1b">MM%</td>
						<td class="ch1b">SP</td>
						<td class="ch1b">SP%</td>
						<td class="ch1b">Std</td>
						<td class="ch1b">Std%</td>
						<td class="ch1b">Total</td>
					</tr>
					<%=(request.getAttribute("MMSPTable") != null) ? (String) request
							.getAttribute("MMSPTable")
							: "<tr><td colspan=\"12\" class=\"ccbb\">Data is not available</td></tr>"%>
				</table>
				</td>
			</tr>
		</table>
		
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="sectiontitle" colspan="2" style="padding: 20px 0px 10px 0px">Quality Ratings - Partners</td>
			</tr>
			<tr>
				<td class="chart1" valign="top"><%=(request.getAttribute("PartnerRatingChart") != null) ? (String) request
							.getAttribute("PartnerRatingChart")
							: "<tr><td colspan=\"3\" class=\"ccaa\" style=\"padding: 4px 0px 0px 15px\">Data is not available</td></tr>"%></td>
			</tr>
			<tr>
				<td class="subsectiontitle" colspan="2" style="padding: 16px 0px 10px 0px">Breakdown for Partner Rating</td>
			</tr>
			
			<tr>
				<td>
				<table width="75%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class="ch1b">Month</td>
						<td class="ch1b">Average Ratig</td>
						<td class="ch1b">Jobs Rated</td>
					</tr>
					<%=(request.getAttribute("PartnerRatingTable") != null) ? (String) request
							.getAttribute("PartnerRatingTable")
							: "<tr><td colspan=\"12\" class=\"ccaa\" style=\"padding: 8px 0px 0px 15px\">Data is not available</td></tr>"%>	
				</table>
				</td>
			</tr>
		</table>
		   
 
		</div>
</td>
<td valign="top">
<div class="main">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="sectiontitle" colspan="2"
					style="padding: 0px 0px 10px 0px">In Work Revenue</td>
			</tr>
			<tr>
				<td valign="top">
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class="ch1b">Status</td>
						<td class="ch1b">Count</td>
						<td class="ch1b">Revenue</td>
					</tr>
					<%=(request.getAttribute("WIPTable") != null) ? (String) request
							.getAttribute("WIPTable")
							: "<tr><td colspan=\"3\" class=\"ccbb\">Data is not available</td></tr>"%>				
				</table>
				</td>
				<td class="chart1" valign="top"><%=(request.getAttribute("WIPChart") != null) ? (String) request
							.getAttribute("WIPChart")
							: "<tr><td colspan=\"3\" class=\"ccbb\">Data is not available</td></tr>"%></td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0">
			
			
			<tr>
				<td colspan="6" class="sectiontitle" style="padding: 16px 0px 10px 0px">Weekly Closed VGPM Summary</td>
			</tr>
			<tr>
				<td colspan="6" class="rptchart1"><%=(request.getAttribute("WVGPMChart") != null) ? (String) request
							.getAttribute("WVGPMChart")
							: "<div class=\"ccbb\">Data is not available</div>"%></td>
			</tr>
			<tr>
				<td colspan="6" class="sectiontitle" style="padding: 16px 0px 10px 0px">Weekly Closed Revenue Summary</td>
			</tr>
			<tr>
				<td colspan="6" class="rptchart1"><%=(request.getAttribute("WRevenueChart") != null) ? (String) request
							.getAttribute("WRevenueChart")
							: "<div class=\"ccbb\">Data is not available</div>"%></td>
			</tr>
			
			<tr>
				<td colspan="6" class="sectiontitle" style="padding: 16px 0px 10px 0px">Weekly Closed Revenue Details</td>
			</tr>
			<tr style="border-top: 1px solid #BFBEC4">
				<td>&nbsp;</td>
				<td class="ch1b">Revenue</td>
				<td class="ch1b">Margin</td>
				<td class="ch1b">Pro&nbsp;Forma</td>
				<td class="ch1b">Delta</td>
				<td class="ch1b">Jobs</td>
			</tr>
			<%=(request.getAttribute("WVGPMTable") != null) ? (String) request
							.getAttribute("WVGPMTable")
							: "<tr><td colspan=\"6\" class=\"ccbb\">Data is not available</td></tr>"%>
            <tr>
				<td colspan="6" class="sectiontitle" style="padding: 16px 0px 10px 0px">Closed VGPM Summary</td>
			</tr>
			<tr>
				<td colspan="6" class="rptchart1"><%=(request.getAttribute("VGPMChart") != null) ? (String) request
							.getAttribute("VGPMChart")
							: "<div class=\"ccbb\">Data is not available</div>"%></td>
			</tr>			
			<tr>
				<td colspan="6" class="sectiontitle" style="padding: 16px 0px 10px 0px">Closed Revenue Summary</td>
			</tr>
			<tr>
				<td colspan="6" class="rptchart1"><%=(request.getAttribute("RevenueChart") != null) ? (String) request
							.getAttribute("RevenueChart")
							: "<div class=\"ccbb\">Data is not available</div>"%></td>
			</tr>		
			
			
			<tr>
				<td colspan="6" class="sectiontitle" style="padding: 16px 0px 10px 0px">Closed Revenue Details</td>
			</tr>
			<tr style="border-top: 1px solid #BFBEC4">
				<td>&nbsp;</td>
				<td class="ch1b">Revenue</td>
				<td class="ch1b">Margin</td>
				<td class="ch1b">Pro&nbsp;Forma</td>
				<td class="ch1b">Delta</td>
				<td class="ch1b">Jobs</td>
			</tr>
			<%=(request.getAttribute("VGPMTable") != null) ? (String) request
							.getAttribute("VGPMTable")
							: "<tr><td colspan=\"6\" class=\"ccbb\">Data is not available</td></tr>"%>	
		</table>
		</div>
</td>

<td height="100%" valign="top">
<div class="right">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="sectiontitle" style="padding: 0px 0px 10px 0px">Invoicing Delays</td>
			</tr>
			<tr>
				<td class="chart1"><%=(request.getAttribute("InvoiceDelaysChart") != null) ? (String) request
							.getAttribute("InvoiceDelaysChart")
							: "<tr><td colspan=\"3\" class=\"ccbb\">Data is not available</td></tr>"%></td>
			</tr>
			<tr>
				<td class="subsectiontitle">Complete and Closed Summary</td>
			</tr>
			<tr>
				<td>
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class="ch1b">Month</td>
						<td class="ch1b">&nbsp;Offsite-Complete<BR>(Operations)&nbsp;</td>
						<td class="ch1b">&nbsp;&nbsp;Complete-Closed<BR>(Accounting)&nbsp;&nbsp;</td>
					</tr>
					<%=(request.getAttribute("InvoiceDelaysTable") != null) ? (String) request
							.getAttribute("InvoiceDelaysTable")
							: "<tr><td colspan=\"3\" class=\"ccbb\">Data is not available</td></tr>"%>
				</table>
				
				</td>
			</tr>

		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			
			<tr>
				<td class="sectiontitle" style="padding: 16px 0px 10px 0px">Productivity</td>
			</tr>
			<tr>
				<td class="subsectiontitle"  style="padding-top:0px">Current Employee Head Counts</td>
			</tr>
			
			<tr>
				<td>
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class="rh1a" style="padding-left:20px">Full Time (All FTEs):</td>
						<td class="ccac"><%=(String) request.getAttribute("employee_count_All")%></td>
						<td class="rh1a" style="padding-left:20px">Operations (Ops):</td>
						<td class="ccac"><%=(String) request.getAttribute("employee_count_Ops")%></td>
					<tr>
						<td class="rh1a" style="padding-left:20px">Sales:</td>
						<td class="ccac"><%=(String) request.getAttribute("employee_count_Sales")%></td>
						<td class="rh1a" style="padding-left:20px">Administration (Admin):</td>
						<td class="ccac"><%=(String) request.getAttribute("employee_count_Admin")%></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>		
				<td class="chart1" style="padding-top:16px"><%=(request.getAttribute("ProductivityChart") != null) ? (String) request
							.getAttribute("ProductivityChart")
							: "<div class=\"ccbb\">Data is not available</div>"%></td>
			</tr>
			
            <!-- New SPM Table if applicable -->
            
			<%=(request.getAttribute("SPM_ProductivityTable") != null) ? (String) request
							.getAttribute("SPM_ProductivityTable")
							: ""%>

			<tr>
				<td class="subsectiontitle" style="padding-top:16px">Employee Productivity Summary</td>
			</tr>

			<tr>
				<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class="ch1b">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td colspan="2" class="ch1b">Ops</td>
						<td colspan="2" class="ch1b">Sales</td>
						<td colspan="2" class="ch1b">Admin</td>
						<td colspan="2" class="ch1b">All FTE</td>
					</tr>
					<tr>
						<td class="ch1b">Month</td>
						<td class="ch1b">Cnt</td>
						<td class="ch1b">Prod</td>
						<td class="ch1b">Cnt</td>
						<td class="ch1b">Prod</td>
						<td class="ch1b">Cnt</td>
						<td class="ch1b">Prod</td>
						<td class="ch1b">Cnt</td>
						<td class="ch1b">Prod</td>
					</tr>
				    <%=(request.getAttribute("ProductivityTable") != null) ? (String) request
							.getAttribute("ProductivityTable")
							: "<tr><td colspan=\"12\" class=\"ccbb\">Data is not available</td></tr>"%>	
				</table>
				</td>
			</tr>
		</table>
</div>
</td>
</tr>
<tr>
<td colspan="3">&nbsp;<br>&nbsp;<br></td>
</tr>
<tr>
<td colspan="3"><div class="ch1a" style="padding: 6px 0px 6px 0px; border-top: 1px solid #a9a8ad">Notes</div></td>
</tr>
<tr>
<td colspan="3"><div class="ccaa" style="padding: 6px 0px 12px 0px;">
Data is updated once a day.
</div></td>
</tr>
</table>
</div>
</form>
</body>
</html>
