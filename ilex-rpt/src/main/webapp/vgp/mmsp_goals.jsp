<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.text.NumberFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style> 
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 12px;
    	cursor : auto;
    }
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    .rptBody02 {
		 padding : 10px 0px 0px 20px;
    }
    .chart {
		vertical-align : Middle;
		align : Center;
		padding : 5px 5px 5px 30px;
    } 
    .titleLine {
		font-size : 12pt;
		font-weight : bold;
		text-align : left;
		vertical-align : Middle;
		padding : 0px 0px 6px 0px;
    } 
    .sectionTitleLine {
		font-size : 11pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 12px 0px 6px 0px;
		color: #2E3D80;
    }
    .sectionTitleLineNav {
		font-size : 8pt;
		font-weight : normal;
		text-align : right;
		vertical-align : middle;
		padding : 0px 0px 0px 0px;
    }
    .label1, .label2 {
        font-size : 9pt;
		font-weight : bold;
		vertical-align : top;
		padding : 10px 10px 6px 0px;
    }
    .form1 {
        font-size : 10pt;
		font-weight : normal;
		padding : 0px 0px 0px 0px;
    }
    .elementContainer {
    padding : 0px 2px 0px 2px;
    }
    .columnHeader1 {
		font-size : 10pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 3px 5px 3px 5px;
		background-color: #E0E0DC; 
    }    
    .columnHeader2 {
		font-size : 9pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 16px 16px 16px 16px;
		background-color: #E0E0DC; 
    }    
    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 9pt;
		font-weight : normal;
		text-align : left;
		vertical-align : middle;
		padding : 1px 5px 1px 5px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #ffffff;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ECECEC;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }        
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
		text-align : center;
		width: 26px;
        height: 24px;
    }
    .date1 {
        font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
    }
    .bottomLine, .bottomLineCenter {
    	font-size : 8pt;
		font-weight : bold;
		text-align : right;
		vertical-align : middle;
		padding : 2px 2px 2px 0px;
		background-color: #e0edf5;
    }
    .bottomLineCenter {
		text-align : center;
    }
    .outlineYellow3 {
	padding: 0px;
	margin: 4px 4px 4px 0px;
	border: 1px solid #FAF8CC;
	background: #ffffff;
   }
    .outlineClear3 {
	padding: 0px;
	margin: 4px 4px 4px 0px;
	border: 2px solid #ffffff;
	background: #ffffff;
   }
   .outlineGray {
	padding: 4px;
	margin: 4px 4px 4px 0px;
	border: 1px solid #E0E0DC;
	background: #ffffff;
   }
   .outlineTopLevel {
	padding: 4px;
	border: 2px solid #E0E0DC;
	background: #ffffff;
   }
   .cTotalRight1 {
	   	font-size : 9pt;
		font-weight : Bold;
		text-align : right;
		vertical-align : middle;
		padding : 3px 4px 3px 4px;
		background: #E0E0DC;
    }    
   .cLinkRight1 {
	   	font-size : 9pt;
		font-weight : normal;
		text-align : right;
		vertical-align : middle;
		padding : 0px 4px 6px 0px;
    }    
 
    .cred0, .cblue0, .cgreen0 {
		font-size : 9pt;
		font-weight : normal;
		text-align : center;
		vertical-align : middle;
		padding : 10px 5px 10px 5px;
    }    
    .cred0 {
		background-color : #ff9999;
    }   
    .cgreen0 {
		background-color : #99ff99;
    }   
    .cblue0 {
		background-color : #9999ff;
    }
 
</style>

<script language="JavaScript" src="vgp/FusionCharts.js"></script>
</head>
<body>
<div style="padding:20px;">
<table border="0" cellspacing="0" cellpadding="8" class="outlineTopLevel">
<tr>
<!-- Left -->
<td valign="top">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- Left-Top -->
<td valign="top">
<div style="padding:10px;">
<div class="titleLine">Trailing 30 Days</div>
<table border="0" cellspacing="1" cellpadding="0" class="outlineGray" align="center">
<tr>
<td class="columnHeader2" style="padding : 16px 6px 16px 6px;">Job Owner</td>
<td class="columnHeader2" style="padding : 16px 6px 16px 6px;">Minuteman</td>
<td class="columnHeader2" style="padding : 16px 6px 16px 6px;">Speedpay</td>
<td class="columnHeader2" style="padding : 16px 6px 16px 6px;">Combined</td>
<td class="columnHeader2" style="padding : 16px 6px 16px 6px;">Total POs</td>
</tr>
<%
Vector formats = (Vector) request.getAttribute("jo_formats");
Vector rows = (Vector) request.getAttribute("jo_rows");
String[] fc ={"cred0", "cblue0", "cgreen0"};
for (int i=0; i<rows.size(); i++) {
	
String[] row = (String[]) rows.get(i);
int[] format = (int[]) formats.get(i);
%>
<tr>
<td class="cDataLeft0" style="padding: 3px; border-bottom: 1px solid #E0E0DC"><%=row[4]%></td>
<td class="<%=fc[format[0]]%>" style="padding: 3px; border-bottom: 1px solid #E0E0DC"><%=row[0]%></td>
<td class="<%=fc[format[1]]%>" style="padding: 3px; border-bottom: 1px solid #E0E0DC"><%=row[1]%></td>
<td class="<%=fc[format[2]]%>" style="padding: 3px; border-bottom: 1px solid #E0E0DC"><%=row[2]%></td>
<td class="cDataRight0" style="padding: 3px; border-bottom: 1px solid #E0E0DC"><%=row[3]%></td>
</tr>
<%
}
%>
</table>
<%
Double[] g1 = (Double[]) request.getAttribute("monthly_mean");
Double[] g2 = (Double[]) request.getAttribute("jo_goals");
NumberFormat percentage = NumberFormat.getPercentInstance();;
%>
<table border="0" cellspacing="2" cellpadding="4" >
<tr>
<td colspan="4">Legend:</td>
</tr>
<tr>
<td>&nbsp;</td>
<td align="center">Less Than</td><td align="center">Between</td><td align="center">Greater Than</td>
</tr>

</table>
</div>
</td>
</tr>
</table>
</td>
<!-- Center -->
<td valign="top">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- Center-Top -->
<td valign="top">
<div style="padding:10px;">
<div class="titleLine">Monthly Team Goals - Jun 2011</div>
<table border="0" cellspacing="0" cellpadding="0" class="outlineGray" align="center">
<tr>
<td class=cDataLeft0>&nbsp;</td>
<td class="columnHeader2">Minuteman</td>
<td class="columnHeader2">Speedpay</td>
<td class="columnHeader2">Combined</td>
</tr>
<%
formats = (Vector) request.getAttribute("formats");
rows = (Vector) request.getAttribute("rows");

for (int i=0; i<rows.size(); i++) {
	
String[] row = (String[]) rows.get(i);
int[] format = (int[]) formats.get(i);
%>
<tr>
<td class="cDataLeft0" style="border-bottom: 1px solid #E0E0DC"><%=row[3]%></td>
<td class="<%=fc[format[0]]%>" style="border-bottom: 1px solid #E0E0DC"><%=row[0]%></td>
<td class="<%=fc[format[1]]%>" style="border-bottom: 1px solid #E0E0DC"><%=row[1]%></td>
<td class="<%=fc[format[2]]%>" style="border-bottom: 1px solid #E0E0DC"><%=row[2]%></td>
</tr>
<%
}
%>
<tr>
<td colspan="4" class=cDataLeft0><br>Blue: +/- 1%</td>
</tr>
</table>
</div>
</td>
</tr>

<tr>
<!-- Center-Top -->
<td valign="top">
<div style="padding:10px;">
<div class="titleLine">Monthly Team Progression</div>
</div>
</td>
</tr>



<tr>
<!-- Center-Bottom -->
<td valign="top">
<div style="padding:10px;">
<div class="titleLine">2011 Company Progression</div>
<%=request.getAttribute("chart1")%>
</div>
</td>
</tr>
</table>
</td>

<!-- Right -->
<td valign="top">
<table border="0" cellspacing="0" cellpadding="0">
<tr>

<!-- Right-Top -->
<td valign="top">
<div style="padding:10px;">
<div class="titleLine">Trailing 30 Days VGP</div>
<table border="0" cellspacing="1" cellpadding="0" class="outlineGray" align="center">
<tr>
<td class="columnHeader2" style="padding : 16px 6px 16px 6px;">Job Owner</td>
<td class="columnHeader2" style="padding : 16px 6px 16px 6px;">Pro Forma</td>
<td class="columnHeader2" style="padding : 16px 6px 16px 6px;">Actual</td>
<td class="columnHeader2" style="padding : 16px 6px 16px 6px;">Delta</td>
</tr>

</table>
</div>
</td>
</tr>

</table>
</td>
</tr>
</table>
</div>
</body>
</html>
