<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	String q1 = "select distinct case when ap_jl_supervisor is not null then ap_jl_supervisor else 'Not Assigned' end as ap_jl_supervisor from ap_job_level_summary order by ap_jl_supervisor";

	String report = (request.getAttribute("report") != null) ? (String) request
			.getAttribute("report")
			: "4";
	String title = (request.getAttribute("title") != null) ? (String) request
			.getAttribute("title")
			: "None Provided";

	String bgc = "FFFFFF";
	String bgc1 = "FFFFFF";
	String bgc2 = "FFFFFF";
	String bgc3 = "FFFFFF";
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Commission Breakdown</title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<%@ include file="js.txt"%>
</head>
<body>
<div class="rptBody01">
<form name="form1" action="COM.xo" method="post">
<table border="0" cellspacing="0" cellpadding="0">
 <tr>
 <td>
 <!-- Form Elements -->
 <table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
  <td rowspan="2"class="titleLine"><%=title%></td>
  <td align="right">Quarter&nbsp;&nbsp;<el:element type="menu2" request="<%=request%>" name="quarter" source="" data="20074|Qtr 4 2007~20081|Qtr 1 2008~20082|Qtr 2 2008~20083|Qtr 3 2008~20084|Qtr 4 2008~20091|Qtr 1 2009~20092|Qtr 2 2009~20093|Qtr 3 2009~20094|Qtr 4 2009~20101|Qtr 1 2010" js=""/></td>
  </tr>
  
  <tr>
  <td align="right">
  <input type="submit" name="ttm_profile" value="TTM Profile" class=date1>&nbsp;
  <input type="submit"  name="default_view" value="Go Qtr" class=date1></td>
  </tr>
 
 </table>
 </td>
</tr>
<input type="hidden" name="form_action", value="commission">
</form>

<tr>
<td>
<!-- Default View -->
<%
if (report.equals("1")) {
%>
<br>
<table border="0" cellspacing="0" cellpadding="0">
  <tr><td>
  <table border="0" cellspacing="1" cellpadding="0">
    <tr>
     <td width="70" rowspan="2" class="columnHeader2" style="background-color: #edf0d4">BDM</td>
     <td colspan="3" class="columnHeader2" style="background-color: #edf0d4">Offsite</td>
     <td colspan="3" class="columnHeader2" style="background-color: #D8E1BD">Complete</td>
     <td colspan="3" class="columnHeader2" style="background-color: #A6BE85">Closed</td>
   </tr>
   <tr>
     <td width="70" class="columnHeader2" style="background-color: #edf0d4">Cost</td>
     <td width="80" class="columnHeader2" style="background-color: #edf0d4">Revenue</td>
     <td width="80" class="columnHeader2" style="background-color: #edf0d4">VGPM</td>
     <td width="70" class="columnHeader2" style="background-color: #D8E1BD">Cost</td>
     <td width="80" class="columnHeader2" style="background-color: #D8E1BD">Revenue</td>
     <td width="80" class="columnHeader2" style="background-color: #D8E1BD">VGPM</td>
     <td width="70" class="columnHeader2" style="background-color: #A6BE85">Cost</td>
     <td width="80" class="columnHeader2" style="background-color: #A6BE85">Revenue</td>
     <td width="80" class="columnHeader2" style="background-color: #A6BE85">VGPM</td>
   </tr>
<%
   if (request.getAttribute("ResultMap") != null) {
	   
	   Map ResultMap = (Map) request.getAttribute("ResultMap");
	   if (ResultMap != null) {
		   // BDM
           Vector table = (Vector) ResultMap.get("BDM");
		   if (table.size() > 0) {
		      for (int i = 0; i < table.size()-1; i++) {
				  bgc = (i % 2 != 0) ? "#edf0d4" : "#ffffff";
				  bgc1 = (i % 2 != 0) ? "#edf0d4" : "#ffffff";
				  bgc2 = (i % 2 != 0) ? "#D8E1BD" : "#ffffff";
				  bgc3 = (i % 2 != 0) ? "#A6BE85" : "#ffffff";
				  String[] tr = (String[]) table.get(i);
%>
					<tr>
						<td class="cDataLeft1" style="background-color: <%=bgc%>"><%=tr[0]%></td>
						<td class="cDataRight1" style="background-color: <%=bgc1%>"><%=tr[1]%></td>
						<td class="cDataRight1" style="background-color: <%=bgc1%>"><%=tr[2]%></td>
						<td class="cDataCenter1" style="background-color: <%=bgc1%>"><%=tr[6]%></td>
						<td class="cDataRight1" style="background-color: <%=bgc2%>"><%=tr[7]%></td>
						<td class="cDataRight1" style="background-color: <%=bgc2%>"><%=tr[8]%></td>
						<td class="cDataCenter1" style="background-color: <%=bgc2%>"><%=tr[12]%></td>
						<td class="cDataRight1" style="background-color: <%=bgc3%>"><%=tr[13]%></td>
						<td class="cDataRight1" style="background-color: <%=bgc3%>"><%=tr[14]%></td>
						<td class="cDataCenter1" style="background-color: <%=bgc3%>"><%=tr[18]%></td>
					</tr>
<%
			  }
		      String[] tr = (String[]) table.get(table.size()-1);
%>
					<tr>
						<td class="cDataLeft1" style="background-color: #ffffff; font-weight : bold"><%=tr[0]%></td>
						<td class="cDataRight1" style="background-color: #ffffff; font-weight : bold"><%=tr[1]%></td>
						<td class="cDataRight1" style="background-color: #ffffff; font-weight : bold"><%=tr[2]%></td>
						<td class="cDataCenter1" style="background-color: #ffffff; font-weight : bold"><%=tr[6]%></td>
						<td class="cDataRight1" style="background-color: #ffffff; font-weight : bold"><%=tr[7]%></td>
						<td class="cDataRight1" style="background-color: #ffffff; font-weight : bold"><%=tr[8]%></td>
						<td class="cDataCenter1" style="background-color: #ffffff; font-weight : bold"><%=tr[12]%></td>
						<td class="cDataRight1" style="background-color: #ffffff; font-weight : bold"><%=tr[13]%></td>
						<td class="cDataRight1" style="background-color: #ffffff; font-weight : bold"><%=tr[14]%></td>
						<td class="cDataCenter1" style="background-color: #ffffff; font-weight : bold"><%=tr[18]%></td>
					</tr>
<%
		  }
%>
					<tr>
						<td colspan="10" style="background-color: #E0E0DC">&nbsp;</td>
					</tr>
   <tr>
     <td width="70" rowspan="2" class="columnHeader2" style="background-color: #edf0d4">PM</td>
     <td colspan="3" class="columnHeader2" style="background-color: #edf0d4">Offsite</td>
     <td colspan="3" class="columnHeader2" style="background-color: #D8E1BD">Complete</td>
     <td colspan="3" class="columnHeader2" style="background-color: #A6BE85">Closed</td>
   </tr>
   <tr>
     <td width="70" class="columnHeader2" style="background-color: #edf0d4">Cost</td>
     <td width="80" class="columnHeader2" style="background-color: #edf0d4">Revenue</td>
     <td width="80" class="columnHeader2" style="background-color: #edf0d4">VGPM</td>
     <td width="70" class="columnHeader2" style="background-color: #D8E1BD">Cost</td>
     <td width="80" class="columnHeader2" style="background-color: #D8E1BD">Revenue</td>
     <td width="80" class="columnHeader2" style="background-color: #D8E1BD">VGPM</td>
     <td width="70" class="columnHeader2" style="background-color: #A6BE85">Cost</td>
     <td width="80" class="columnHeader2" style="background-color: #A6BE85">Revenue</td>
     <td width="80" class="columnHeader2" style="background-color: #A6BE85">VGPM</td>
   </tr>
<%
           table = (Vector) ResultMap.get("SPM");
		   if (table.size() > 0) {
		      for (int i = 0; i < table.size()-1; i++) {
				  bgc = (i % 2 != 0) ? "#edf0d4" : "#ffffff";
				  bgc1 = (i % 2 != 0) ? "#edf0d4" : "#ffffff";
				  bgc2 = (i % 2 != 0) ? "#D8E1BD" : "#ffffff";
				  bgc3 = (i % 2 != 0) ? "#A6BE85" : "#ffffff";
				  String[] tr = (String[]) table.get(i);
%>
					<tr>
						<td class="cDataLeft1" style="background-color: <%=bgc%>"><%=tr[0]%></td>
						<td class="cDataRight1" style="background-color: <%=bgc1%>"><%=tr[1]%></td>
						<td class="cDataRight1" style="background-color: <%=bgc1%>"><%=tr[2]%></td>
						<td class="cDataCenter1" style="background-color: <%=bgc1%>"><%=tr[6]%></td>
						<td class="cDataRight1" style="background-color: <%=bgc2%>"><%=tr[7]%></td>
						<td class="cDataRight1" style="background-color: <%=bgc2%>"><%=tr[8]%></td>
						<td class="cDataCenter1" style="background-color: <%=bgc2%>"><%=tr[12]%></td>
						<td class="cDataRight1" style="background-color: <%=bgc3%>"><%=tr[13]%></td>
						<td class="cDataRight1" style="background-color: <%=bgc3%>"><%=tr[14]%></td>
						<td class="cDataCenter1" style="background-color: <%=bgc3%>"><%=tr[18]%></td>
					</tr>
<%
			  }
		      String[] tr = (String[]) table.get(table.size()-1);
%>
					<tr>
						<td class="cDataLeft1" style="background-color: #ffffff; font-weight : bold"><%=tr[0]%></td>
						<td class="cDataRight1" style="background-color: #ffffff; font-weight : bold"><%=tr[1]%></td>
						<td class="cDataRight1" style="background-color: #ffffff; font-weight : bold"><%=tr[2]%></td>
						<td class="cDataCenter1" style="background-color: #ffffff; font-weight : bold"><%=tr[6]%></td>
						<td class="cDataRight1" style="background-color: #ffffff; font-weight : bold"><%=tr[7]%></td>
						<td class="cDataRight1" style="background-color: #ffffff; font-weight : bold"><%=tr[8]%></td>
						<td class="cDataCenter1" style="background-color: #ffffff; font-weight : bold"><%=tr[12]%></td>
						<td class="cDataRight1" style="background-color: #ffffff; font-weight : bold"><%=tr[13]%></td>
						<td class="cDataRight1" style="background-color: #ffffff; font-weight : bold"><%=tr[14]%></td>
						<td class="cDataCenter1" style="background-color: #ffffff; font-weight : bold"><%=tr[18]%></td>
					</tr>
<%		      
		      
		  }
	   }
   }
%>

  </table>
  </td>
  <td></td>
  </tr>
</table>
   
<!-- TTM Profile -->
<%
} else if (report.equals("2")) {
%>
<br><br><br>
<table border="0" cellspacing="0" cellpadding="0">
  <tr><td>
  <table border="0" cellspacing="1" cellpadding="0">
   <tr>
     <td width="70" class="columnHeader2">Month</td>
     <td width="80" class="columnHeader2">Revenue</td>
     <td width="80" class="columnHeader2">Cost</td>
     <td width="60"  class="columnHeader2">Percent Cost</td>
     <td width="80" class="columnHeader2">VGP</td>
     <td width="60"  class="columnHeader2">Percent VGP</td>
     <td width="40"  class="columnHeader2">VGPM</td>
   </tr>
   
   <%
      			if (request.getAttribute("ttm_table") != null) {
      			Vector table = (Vector) request.getAttribute("ttm_table");
      			if (table.size() > 0) {
      		for (int i = 0; i < table.size(); i++) {
      			bgc = (i % 2 != 0) ? "#D8E1BD" : "#ffffff";
      			String[] tr = (String[]) table.get(i);
      %>
    		<tr>
    		<td class="cDataLeft1" style="background-color: <%=bgc%>"><%=tr[0]%></td>
    		<td class="cDataRight1" style="background-color: <%=bgc%>"><%=tr[1]%></td>
    		<td class="cDataRight1" style="background-color: <%=bgc%>"><%=tr[2]%></td>
    		<td class="cDataCenter1" style="background-color: <%=bgc%>"><%=tr[3]%></td>
    		<td class="cDataRight1" style="background-color: <%=bgc%>"><%=tr[4]%></td>
    		<td class="cDataCenter1" style="background-color: <%=bgc%>"><%=tr[5]%></td>
    		<td class="cDataCenter1" style="background-color: <%=bgc%>"><%=tr[6]%></td>
    		</tr>
    		<%
    					}
    					}
    				}
    		%>
  </table>
  </td>
  <td class="chart"><%=(request.getAttribute("chart") != null) ? (String) request
								.getAttribute("chart")
								: "Chart Error"%></td>
  </tr>
</table>


<!-- Various Views -->
<%
} else if (report.equals("3")) {
%>
<br><br><br>
<table border="0" cellspacing="0" cellpadding="0">
  <tr><td>
  <table border="0" cellspacing="1" cellpadding="0">
   <tr>
     <td width="70" class="columnHeader2">BDM</td>
     <td width="80" class="columnHeader2">Revenue</td>
     <td width="80" class="columnHeader2">Cost</td>
     <td width="60"  class="columnHeader2">Percent Cost</td>
     <td width="80" class="columnHeader2">VGP</td>
     <td width="60"  class="columnHeader2">Percent VGP</td>
     <td width="40"  class="columnHeader2">VGPM</td>
   </tr>
    <%
    			if (request.getAttribute("general") != null) {
    			Vector table = (Vector) request.getAttribute("general");
    			if (table.size() > 0) {
    		for (int i = 0; i < table.size(); i++) {
    			bgc = (i % 2 != 0) ? "#e0edf5" : "#ffffff";
    			String[] tr = (String[]) table.get(i);
    %>
    		<tr>
    		<td class="cDataLeft1" style="background-color: <%=bgc%>">XXX</td>
    		<td class="cDataRight1" style="background-color: <%=bgc%>"><%=tr[0]%></td>
    		<td class="cDataRight1" style="background-color: <%=bgc%>"><%=tr[1]%></td>
    		<td class="cDataCenter1" style="background-color: <%=bgc%>"><%=tr[2]%></td>
    		<td class="cDataRight1" style="background-color: <%=bgc%>"><%=tr[3]%></td>
    		<td class="cDataCenter1" style="background-color: <%=bgc%>"><%=tr[4]%></td>
    		<td class="cDataCenter1" style="background-color: <%=bgc%>"><%=tr[5]%></td>
    		</tr>
    		<%
    					}
    					}
    				}
    		%>
   </table>
  </td>
  <td></td>
  </tr>
</table>
<%
} else {
%>
Error
<%
}
%>

</td>
</tr>
</table>
</div> 
</body>
</html>