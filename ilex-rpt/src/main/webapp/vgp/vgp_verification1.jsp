<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cost Corrects - Unallocted Resources and Internal POs</title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<%@ include file="js.txt"%>

<style>
    .columnHeader3 {
		font-size : 8pt;
		font-weight : normal;
		text-align : right;
		padding : 3px 10px 3px 10px;
		background-color: #E0E0DC; 
    }  
</style>

<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr>

<!-- Title Block -->
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top"><div class="titleLine">Unallocated Resources and Internal PO with Wrong Authorized Cost</div> 
</td>
</tr>
<tr>

<!-- Summary Table -->
<td>
<table border="0" cellspacing="0" cellpadding="1">
<tr>
<td colspan="5"><div class="columnHeader2" style="text-align : left;">Summary:</div></td>
</tr>
<tr>
<td><div class="columnHeader2">Count</div></td>
<td><div class="columnHeader2">Cost</div></td>
<td><div class="columnHeader2">Unallocated Cost</div></td>
<td><div class="columnHeader2">Internal PO Cost</div></td>
<td><div class="columnHeader2">Corrected Cost</div></td>
</tr>
<%
if (request.getAttribute("summary") != null) {
%>
<%=(String)request.getAttribute("summary")%>
<%	
} else {
%>
<tr><td colspan="5"><div>&nbsp;</div></td></tr>
<%	
}
%>
</table>
</td>
</tr>
</table>
</td>

<!-- Chart Block -->
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

<!-- Form Block -->

<td align="right">
<form name="form1" action="XVGP.xo" method="post">
<input type="hidden" name="action" value="vgp_verification1">
<table border="0" cellspacing="0" cellpadding="0">

<tr align="right">
<td>Customer:&nbsp;&nbsp;<%=(String)request.getAttribute("customer")%></td>
</tr>
<tr align="right">
<td>BDM:&nbsp;&nbsp;<%=(String)request.getAttribute("bdm")%></td>
</tr>
<tr align="right">
<td>SPM:&nbsp;&nbsp;<%=(String)request.getAttribute("spm")%></td>
</tr>
<%
if (request.getAttribute("owner") != null) {
%>
<tr align="right">
<td>Job Owner:&nbsp;&nbsp;<%=(String)request.getAttribute("owner")%></td>
</tr>
<%
}
%>
<tr align="right">
<td>Closed Date:&nbsp;&nbsp;<el:element type="date1" request="<%=request%>" name="from_date" source="" js="" data=""/>&nbsp;&nbsp;to&nbsp;&nbsp;<el:element type="date1" request="<%=request%>" name="to_date" source="" js="" data=""/></td>
</tr>
<tr align="right">
<td>View:&nbsp;&nbsp;<%=(String)request.getAttribute("radio")%>&nbsp;&nbsp;<input type="submit" value="Go" class="button"/>
</td>
</tr>
</table></form>
</td>
</tr>


<!-- Data -->
<tr>
<td colspan="3">
<table border="0" cellspacing="0" cellpadding="1">
<%=request.getAttribute("data") != null ? (String)request.getAttribute("data") : "&nbsp;&nbsp;&nbsp;<b>No records satisfy the search criteria.</b>" %>
</table>
</td>
</tr>
</table>
</div> 
</body>
</html>