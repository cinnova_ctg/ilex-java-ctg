<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.contingent.ilex.utility.RevRptConst"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

		<%-- BEAN DECLARATION --%>
		<jsp:useBean id="dwsql" class="com.contingent.ilex.report.DWSqlBean" scope="application" />

		<%-- IMPORT DECLARATION --%>
		<%@ page language="java"
			import="java.util.ArrayList"
		%>

		<%-- JavaScript --%>
		<script>
		var request = false;
		function createRequest() {
			try {
				request = new XMLHttpRequest();
			} catch (failed) {
				request = false;
			}
			if (!request) {
				alert("Error initializing XMLHttpRequest!");
			}
		}
		function initPage() {
			createRequest();
			getNewGraph();
			//winHeight = document.body.clientHeight/1;
			//winWidth = document.body.clientWidth/1;
			//var eText = document.getElementById("elementText");
			//eText.cols = Math.floor(winWidth/widthScale);
		}
		function getNewGraph() {
			document.getElementById('bottom').innerHTML = "";
			var element;
			element = document.getElementById("datetype");
			var dtype = element.options[element.selectedIndex].value;
			element = document.getElementById("startmonthyr");
			var dstart = element.options[element.selectedIndex].value;
			element = document.getElementById("endmonthyr");
			var dend = element.options[element.selectedIndex].value;
			element = document.getElementById("spmgr");
			var spm = element.options[element.selectedIndex].text;
			element = document.getElementById("bdmgr");
			var bdm = element.options[element.selectedIndex].text;
			element = document.getElementById("jowner");
			var jo = element.options[element.selectedIndex].text;
			element = document.getElementById("clients");
			var cli = element.options[element.selectedIndex].text;
			element = document.getElementById("ptype");
			var pt = element.options[element.selectedIndex].text;
			
			//alert("dtype="+dtype+",dstart="+dstart+",dend="+dend+",spm="+spm+",bdm="+bdm+",jo="+jo+",cli="+cli+",pt="+pt);
			url = "<%=request.getContextPath()%>/getRevenueData.xo?datetype="+dtype+"&startdateYM="+dstart+"&enddateYM="+dend+"&spmgr="+spm+"&bdmgr="+bdm+"&jowner="+jo+"&clients="+cli+"&ptype="+pt;
			//alert("url="+url);
			request.open("GET", url, true);
			request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			request.onreadystatechange = updatePage;
			request.send(null);
		}
		function updatePage() {
			var response;
			var txtAllowed = "false";
			if (request.readyState == 4) {
   				if (request.status == 200) {
   					//alert(request.getAllResponseHeaders());
   					response = request.responseText;
   					document.getElementById('bottom').innerHTML = response;
				} else if (request.status == 404) {
					alert("Requested URL does not exist");
				} else if (request.status == 403) {
					alert("Access denied");
				} else {
						alert("request status is "+request.status);
				}
			}
		}
		</script>

		<style type="text/css">
			<!--
			body {font-family:Arial; font-size:90%; background-color:#FFF7E5}
			.thyearcell {border-bottom:3px solid black}
			.thmonthcell {border-bottom:3px solid black; border-right:1px solid black; border-left:1px solid black}
			.thcell {border-bottom:3px solid black; border-right:1px solid black}
			.charttable {background-color:#<%=RevRptConst.DEF_CHARTBG_COLOR%>}
			.yearcell {border-top:1px solid black}
			.monthcell {border-top:1px solid black; border-right:1px solid black; border-left:1px solid black}
			.vcogscell {color:#<%=RevRptConst.COLOR_VCOGS%>; font-weight:bold; border-top:1px solid black; border-right:1px solid black}
			.vgpcell {color:#<%=RevRptConst.COLOR_VGP%>; font-weight:bold; border-top:1px solid black; border-right:1px solid black}
			.revcell {color:#<%=RevRptConst.COLOR_REVENUE%>; font-weight:bold; border-top:1px solid black; border-right:1px solid black}
			.rButton {background-color:#009900; align:left;}
			-->
		</style>

		<title>Financial Summary - Revenue</title>
	</head>
	<body>
		<form action="getRevenueData" id="revenueForm">
			<%
				//Get the data for the drop down lists
					ArrayList rDates = dwsql.getReadableMonthYearDates(null);
					ArrayList iDates = dwsql.getIntegerYearMonthDates();
					session.setAttribute("iDatesArray", iDates); //to re-use elsewhere
					ArrayList spmgr = dwsql.getFullNamesForIdList(RevRptConst.SR_PROJ_MGR_IDTYPE, RevRptConst.LASTNAMEFIRST, RevRptConst.DEF_SRPROJMAN);
					ArrayList bdmgr = dwsql.getFullNamesForIdList(RevRptConst.BUS_DEV_MGR_IDTYPE, RevRptConst.LASTNAMEFIRST, RevRptConst.DEF_BUSDEVMAN);
					ArrayList jowner = dwsql.getFullNamesForIdList(RevRptConst.JOB_OWNER_IDTYPE, RevRptConst.LASTNAMEFIRST, RevRptConst.DEF_JOBOWNER);
					ArrayList clients = dwsql.getClients(RevRptConst.DEF_CLIENT);
					ArrayList projtypes = dwsql.getProjectTypes(RevRptConst.DEF_PROJTYPE);
					// default values
					int lastcol_width = 15;
			%>
			<table width=100% border="0" cellpadding="1" cellspacing="2">
				<tr>
					<td width=15% align='right'>Business Dev't. Mgr:&nbsp;;</td>
					<td width=23% align='left'>
						<select id='bdmgr' name='bdmgr' size='1'>
								<% String busdevmgr = "";
								for (int i=0; i<bdmgr.size(); i++) {
									busdevmgr = (String)bdmgr.get(i); %>
									<option>
										<%=busdevmgr%>
									</option>
							<%	} %>
						</select>
					</td>
					<td width=9% align='left'>Client:&nbsp;</td>
					<td width=25% align='left'>
						<select id='clients' name='clients' size='1'>
								<% String clt = "";
								for (int i=0; i<clients.size(); i++) {
									clt = (String)clients.get(i); %>
									<option>
										<%=clt%>
									</option>
							<%	} %>
						</select>
					</td>
					<td>&nbsp;&nbsp;Start (Month/Year):&nbsp;
						<select id='startmonthyr' name='startmonthyr' size='1'>
								<% String sStart = "";
								   int iStart = 0;
								for (int i=0; i<rDates.size(); i++) {
									sStart = (String)rDates.get(i);
									Integer t1 = (Integer)iDates.get(i);
									iStart = t1.intValue(); %>
									<option  value="<%=iStart%>" <%= i == (rDates.size()-12) ? "SELECTED" : "" %>>
										<%=sStart%>
									</option>
							<%	} %>
						</select>
					</td>
				</tr>
				<tr>
					<td width=15% align='right'>Senior Project Mgr:&nbsp;</td>
					<td width=23% align='left'> 
						<select id='spmgr' name='spmgr' size='1'>
								<% String srprojmgr = "";
								for (int i=0; i<spmgr.size(); i++) {
									srprojmgr = (String)spmgr.get(i); %>
									<option>
										<%=srprojmgr%>
									</option>
							<%	} %>
						</select>
					</td>
					<td width=9% align='left'>Project Type:&nbsp;</td>
					<td width=25% align='left'>
						<select id='ptype' name='ptype' size='1'>
								<% String pt = "";
								for (int i=0; i<projtypes.size(); i++) {
									pt = (String)projtypes.get(i); %>
									<option>
										<%=pt%>
									</option>
							<%	} %>
						</select>
					</td>
					<td>&nbsp;&nbsp;End (Month/Year):&nbsp;
						<select id='endmonthyr' name='endmonthyr' size='1'>
								<% String sEnd = "";
								   int iEnd = 0;
								for (int i=0; i<rDates.size(); i++) {
									sEnd = (String)rDates.get(i); 				
									Integer t2 = (Integer)iDates.get(i);
									iEnd = t2.intValue(); %>
									<option value="<%=iEnd%>" <%= i == rDates.size()-1 ? "SELECTED" : "" %>>
										<%=sEnd%>
									</option>
							<%	} %>
						</select>
					</td>
				</tr>
				<tr>
					<td width=12% align='right'>Job Owner:&nbsp;</td>
					<td colspan=3 align='left'>
						<select id='jowner' name='jowner' size='1'>
								<% String jobowner = "";
								for (int i=0; i<jowner.size(); i++) {
									jobowner = (String)jowner.get(i); %>
									<option>
										<%=jobowner%>
									</option>
							<%	} %>
						</select>
					</td>
					<td colspan=3>&nbsp;&nbsp;Revenue Basis:&nbsp;
						<select id='datetype' name='datetype' size='1'>
								<%
									String datetype = "";
									String dateval = "";
													for (int i=0; i<RevRptConst.DATETYPES.length; i++) {
														datetype = RevRptConst.DATETYPES[i];
														dateval = RevRptConst.DATEVALUES[i];
								%>
*********									<option value="<%=datetype%>"> <%=dateval%> </option>
							<%	} %>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan='5' align="right">
						<INPUT type='button' name='button1' value='Report!' style="background-color:#99FF99"  onClick='getNewGraph()'>
					</td>
				</tr>
			</table>
			<div id='bottom'></div>
			<script>initPage()</script>
		</form >
	</body>
</html>