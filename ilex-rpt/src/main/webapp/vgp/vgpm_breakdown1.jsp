<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>VGPM Breakdown</title>
<%@ include file="styles1.txt"%>
</head>

<body>
<div class="top">
<table border="0" cellspacing="0" cellpadding="0">

<!-- Form Elements -->
<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  <td rowspan="2" class="titleLine"><div class="title" style="padding: 6px 0px 6px 0px;">Job Owner Summary</div></td>
  </tr>
 </table>
 </td>
</tr>
<!-- Form Elements -->

<tr>
<td>
 <table border="0" cellspacing="1" cellpadding="0">
 <!-- Page Content Starts -->
  <tr>
     <td width="175" class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">&nbsp;Job&nbsp;Owner&nbsp;</td>
     <td width="100" class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Estimated&nbsp;Cost</td>
     <td width="100" class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Authorized&nbsp;Cost</td>
     <td width="100" class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Approved&nbsp;Cost</td>
     <td width="100" class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Revenue</td>
     <td width="70" class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Pro&nbsp;Forma</td>
     <td width="60"  class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">VGPM</td>
     <td width="60"  class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Delta</td>
     <td width="50"  class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Jobs</td>
     <td width="50"  class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Revenue/Job</td>
     <td width="50"  class="ch1b" style="border-right: 1px solid #BFBEC4; border-bottom: 1px solid #BFBEC4">Updated</td>
  </tr>
 
  <%=(request.getAttribute("vgpm_summary") != null) ? (String) request.getAttribute("vgpm_summary"):"<tr><td colspan=\"11\" class=\"ccaa\"><br>There is currently no data available for this report.</td></tr>"%>
  
 </table>
</td>
</tr>
</table>
</div> 
</body>
</html>