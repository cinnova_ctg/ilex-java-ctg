<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
String q1 = "select distinct case when ap_jl_supervisor is not null then ap_jl_supervisor else 'Not Assigned' end as ap_jl_supervisor from ap_job_level_summary order by ap_jl_supervisor";
String q2 = "select distinct case when ap_jl_employee is not null then ap_jl_employee else 'Not Assigned' end as ap_jl_employee from ap_job_level_summary order by ap_jl_employee";

String title = (String)request.getAttribute("title");
String column_header = (String)request.getAttribute("column_header");

String project = (String)request.getAttribute("project");
if (project.equals("")) {
	project = null;
}
String team = (String)request.getAttribute("team");
if (team.equals("")) {
	team = null;
}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>VGPM Breakdown</title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<%@ include file="js.txt"%>
</head>

<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">

<!-- Form Elements -->
<form name="form1" action="VGPM.xo" method="post">
<input type="hidden" name="form_action", value="vgpm_all">
<tr>
 <td>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  <td rowspan="2" class="titleLine"><%=title%></td>
  <td align="right">PM:&nbsp;&nbsp;<el:element type="menu3" request="<%=request%>" name="pm"    source="database" data="<%=q1%>" js="onfocus='resetMenuSelection(document.form1.owner,document.form1.pm)'"/>&nbsp;&nbsp;&nbsp;<b>or</b>&nbsp;&nbsp;&nbsp;
                 Owner:&nbsp;&nbsp;<el:element type="menu3" request="<%=request%>" name="owner" source="database" data="<%=q2%>" js="onfocus='resetMenuSelection(document.form1.pm,document.form1.owner)'"/></td>
  </tr>
  <tr>
  <td align="right">&nbsp;&nbsp;<el:element type="date1" request="<%=request%>" name="from_date" source="" js="" data=""/>&nbsp;&nbsp;to&nbsp;&nbsp;<el:element type="date1" request="<%=request%>" name="to_date" source="" js="" data=""/>&nbsp;&nbsp;<input type="submit" value="Go" class=date1></td>
  </tr>
 </table>
 </td>
</tr>
</form>
<!-- Form Elements -->

<tr>
<td>
 <table border="0" cellspacing="1" cellpadding="0">
 <!-- Page Content Starts -->
  <tr>
     <%
     if (project != null) {
     %>
     <td width="125" class="columnHeader1">Project</td>
     <%
      }
     %>
     
     <%
     if (team != null) {
    	 column_header = "Job Owner";
     %>
     <td width="125" class="columnHeader1">PM</td>
     <%
      }
     %>
     <td width="125" class="columnHeader1"><%=column_header%></td>
     <td width="100" class="columnHeader1">Estimated Cost</td>
     <td width="100" class="columnHeader1">Authorized Cost</td>
     <td width="100" class="columnHeader1">Approved Cost</td>
     <td width="100" class="columnHeader1">Actual Cost</td>
     <td width="100" class="columnHeader1">Revenue</td>
     <td width="70" class="columnHeader1">Pro Forma VGPM</td>
     <td width="60"  class="columnHeader1">VGPM</td>
     <td width="60"  class="columnHeader1">Delta</td>
     <td width="50"  class="columnHeader1">Jobs/ Tickets</td>
  </tr>
     <%
    
     Vector rows = (Vector)request.getAttribute("rows");
     if (rows.size() > 0) {
    	 for (int i=0; i<rows.size(); i++) {
    	   String[] r = (String[])rows.get(i);
     %>
   <tr>
     <% 
     if (project != null) {
     %>
     <td class="cDataLeft1" style="background-color: <%=r[12]%>"><%=r[0]%></td>
     <%
      }
     %>
     <%
     if (team != null) {
     %>
     <td width="125" class="cDataLeft1" style="background-color: <%=r[12]%>"><%=r[1]%></td>
     <%
      }
     %>
     <td class="cDataLeft1" style="background-color: <%=r[12]%>"><%=r[2]%></td>
     <td class="cDataRight1" style="background-color: <%=r[12]%>"><%=r[3]%></td>
     <td class="cDataRight1" style="background-color: <%=r[12]%>"><%=r[4]%></td>
     <td class="cDataRight1" style="background-color: <%=r[12]%>"><%=r[5]%></td>
     <td class="cDataRight1" style="background-color: <%=r[12]%>"><%=r[6]%></td>
     <td class="cDataRight1" style="background-color: <%=r[12]%>"><%=r[7]%></td>
     <td class="cDataCenter1" style="background-color: <%=r[12]%>"><%=r[10]%></td>
     <td class="cDataCenter1" style="background-color: <%=r[12]%>"><%=r[8]%></td>
     <td class="cDataCenter1" style="background-color: <%=r[12]%>"><%=r[11]%></td>
     <td class="cDataRight1"  style="background-color: <%=r[12]%>"><%=r[9]%></td>
  </tr>
     <%
      } // for (rows ...
      } // if rows ...
     %>
  <!-- Page Content Ends -->
 </table>
</td>
</tr>
</table>
</div> 
</body>
</html>