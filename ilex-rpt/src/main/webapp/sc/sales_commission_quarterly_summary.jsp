<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
String quarter = (String)request.getAttribute("quarter");
String link1 = "";
if (!quarter.contains("Quarter Not Selected")) {
	link1 = "<a href=\"QtrCom.xo?fn=qrtcom1&quarter="+quarter+"&page_type=summary2\">Project Type Breakdown</a>";
} 
String title = "Quarterly Commission Summary for "+quarter;
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quarterly Commission Summary</title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<%@ include file="js.txt"%>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<form name="form1" action="QtrCom.xo" method="post">
<input type="hidden" name="fn", value="qrtcom1">
  <tr>
  <td rowspan="3"class="titleLine"><%=title%></td>
  <td align="right" class="cLinkRight1"><%=link1%></td>
  </tr>
  <tr>
  <td align="right">Quarter&nbsp;&nbsp;<el:element type="menu2" request="<%=request%>" name="quarter" source="" data="3/2009|Qtr 3 2009~4/2009|Qtr 4 2009~1/2010|Qtr 1 2010~2/2010|Qtr 2 2010" js=""/></td>
  </tr>
  <tr>
  <td align="right">
  <input type="submit" name="default_view" value="Submit" class=date1></td>
  </tr>
  </form>
 </table>
</td></tr>
<!-- BDMs Main -->
<tr><td>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">

    <tr>
     <td width="70" rowspan="2" class="columnHeader2">BDMs</td>
     <td colspan="7" class="columnHeader2">Cost</td>
     <td colspan="1" rowspan="2" class="columnHeader2">&nbsp;&nbsp;&nbsp;Revenue&nbsp;&nbsp;&nbsp;</td>
     <td colspan="2" class="columnHeader2">VGPM</td>
   </tr>
   
   <tr>
     <td width="70" class="columnHeader2">Pro Forma</td>
     <td width="80" class="columnHeader2">Authorized</td>
     <td width="80" class="columnHeader2">Approved</td>
     <td width="70" class="columnHeader2">Actual</td>
     <td width="80" class="columnHeader2">Unallocated&nbsp;CTR</td>
     <td width="80" class="columnHeader2">Unallocated&nbsp;PMCL</td>
     <td width="70" class="columnHeader2">Corrected</td>
     <td width="80" class="columnHeader2">Pro Forma</td>
     <td width="80" class="columnHeader2">Actual</td>
   </tr>

<% 
 Vector rows = (Vector)request.getAttribute("bdm");
 String[] t = (String[])request.getAttribute("bdm_total");
 
 if (rows != null && rows.size() > 0) {

	 for (int i=0; i<rows.size(); i++) {
		 String[] r = (String[])rows.get(i);
%>
			<tr>
				<td class="cDataLeft<%=i%2%>"><%=r[1]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[2]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[3]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[4]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[5]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[6]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[7]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[8]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[9]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[10]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[11]%></td>
			</tr>
<%
	 } // for loop
%>
 <tr>
    <td class="cTotalRight1"><strong>Totals</strong>:&nbsp;&nbsp;&nbsp;</td>
    <td class="cTotalRight1"><%=t[2]%></td>
    <td class="cTotalRight1"><%=t[3]%></td>
    <td class="cTotalRight1"><%=t[4]%></td>
    <td class="cTotalRight1"><%=t[5]%></td>
    <td class="cTotalRight1"><%=t[6]%></td>
    <td class="cTotalRight1"><%=t[7]%></td>
    <td class="cTotalRight1"><%=t[8]%></td>
    <td class="cTotalRight1"><%=t[9]%></td>
    <td class="cTotalRight1"><%=t[10]%></td>
    <td class="cTotalRight1"><%=t[11]%></td>
  </tr>

<%
 } else {
  // No data provided
%>

<%
 }
%>
</table>
</td></tr>
<!-- Spacer -->
<tr><td>&nbsp;</td></tr>
<!-- SPMs Main -->
<tr><td>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">

    <tr>
     <td width="70" rowspan="2" class="columnHeader2">SPMs</td>
     <td colspan="7" class="columnHeader2">Cost</td>
     <td colspan="1" rowspan="2" class="columnHeader2">&nbsp;&nbsp;&nbsp;Revenue&nbsp;&nbsp;&nbsp;</td>
     <td colspan="2" class="columnHeader2">VGPM</td>
   </tr>
   
   <tr>
     <td width="70" class="columnHeader2">Pro Forma</td>
     <td width="80" class="columnHeader2">Authorized</td>
     <td width="80" class="columnHeader2">Approved</td>
     <td width="70" class="columnHeader2">Actual</td>
     <td width="80" class="columnHeader2">Unallocated&nbsp;CTR</td>
     <td width="80" class="columnHeader2">Unallocated&nbsp;PMCL</td>
     <td width="70" class="columnHeader2">Corrected</td>
     <td width="80" class="columnHeader2">Pro Forma</td>
     <td width="80" class="columnHeader2">Actual</td>
   </tr>

<% 
 rows = (Vector)request.getAttribute("spm");
 t = (String[])request.getAttribute("spm_total");
 
 if (rows != null && rows.size() > 0) {

	 for (int i=0; i<rows.size(); i++) {
		 String[] r = (String[])rows.get(i);
%>
			<tr>
				<td class="cDataLeft<%=i%2%>"><%=r[1]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[2]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[3]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[4]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[5]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[6]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[7]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[8]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[9]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[10]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[11]%></td>
			</tr>
<%
	 } // for loop
%>
  <tr>
    <td class="cTotalRight1"><strong>Totals</strong>:&nbsp;&nbsp;&nbsp;</td>
    <td class="cTotalRight1"><%=t[2]%></td>
    <td class="cTotalRight1"><%=t[3]%></td>
    <td class="cTotalRight1"><%=t[4]%></td>
    <td class="cTotalRight1"><%=t[5]%></td>
    <td class="cTotalRight1"><%=t[6]%></td>
    <td class="cTotalRight1"><%=t[7]%></td>
    <td class="cTotalRight1"><%=t[8]%></td>
    <td class="cTotalRight1"><%=t[9]%></td>
    <td class="cTotalRight1"><%=t[10]%></td>
    <td class="cTotalRight1"><%=t[11]%></td>
  </tr>

<%
 } else {
  // No data provided
%>

<%
 }
%>
</table>
</td></tr>

</table>
</td></tr>
</table>
</div> 
</body>
</html>