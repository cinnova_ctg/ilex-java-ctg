<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
String quarter = (String)request.getAttribute("quarter");
String link1 = "";
if (!quarter.contains("Quarter Not Selected")) {
	link1 = "<a href=\"QtrCom.xo?fn=qrtcom1&quarter="+quarter+"&page_type=summary1\">Summary</a>";
} 
String title = "Quarterly Commission Summary for "+quarter;
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quarterly Commission Summary</title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<%@ include file="js.txt"%>
</head>
<body>
<div class="rptBody01">
<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<form name="form1" action="QtrCom.xo" method="post">
<input type="hidden" name="fn", value="qrtcom1">
<input type="hidden" name="page_type", value="summary2">
  <tr>
  <td rowspan="3"class="titleLine"><%=title%></td>
  <td align="right" class="cLinkRight1"><%=link1%></td>
  </tr>
  <tr>
  <td align="right">Quarter&nbsp;&nbsp;<el:element type="menu2" request="<%=request%>" name="quarter" source="" data="3/2009|Qtr 3 2009~4/2009|Qtr 4 2009~1/2010|Qtr 1 2010~2/2010|Qtr 2 2010" js=""/></td>
  </tr>
  <tr>
  <td align="right">
  <input type="submit" name="default_view" value="Submit" class=date1></td>
  </tr>
  </form>
 </table>
</td></tr>

<!-- Projects -->
<tr><td class="sectionTitleLine">Projects</td></tr>

<jsp:include page="summary_section.jsp">
  <jsp:param name="classification" value="Project:BDM"/>
</jsp:include>

<tr><td>&nbsp;</td></tr>

<jsp:include page="summary_section.jsp">
  <jsp:param name="classification" value="Project:SPM"/>
</jsp:include>

<!-- NetMedX -->
<tr><td class="sectionTitleLine">NetMedX</td></tr>

<jsp:include page="summary_section.jsp">
  <jsp:param name="classification" value="NetMedX:BDM"/>
</jsp:include>

<tr><td>&nbsp;</td></tr>

<jsp:include page="summary_section.jsp">
  <jsp:param name="classification" value="NetMedX:SPM"/>
</jsp:include>

<!-- EverWorX-Operations -->
<tr><td class="sectionTitleLine">EverWorX-Operations</td></tr>

<jsp:include page="summary_section.jsp">
  <jsp:param name="classification" value="EverWorX-Operations:BDM"/>
</jsp:include>

<tr><td>&nbsp;</td></tr>

<jsp:include page="summary_section.jsp">
  <jsp:param name="classification" value="EverWorX-Operations:SPM"/>
</jsp:include>

<!-- EverWorX-Deployment -->
<tr><td class="sectionTitleLine">EverWorX-Deployment*</td></tr>

<jsp:include page="summary_section_everworx_cost.jsp">
  <jsp:param name="classification" value="EverWorX-Deployment:BDM"/>
</jsp:include>

<tr><td>&nbsp;</td></tr>

<jsp:include page="summary_section_everworx_cost.jsp">
  <jsp:param name="classification" value="EverWorX-Deployment:SPM"/>
</jsp:include>

<!-- EverWorX-Help Desk -->
<tr><td class="sectionTitleLine">EverWorX-Help Desk*</td></tr>

<jsp:include page="summary_section_everworx_cost.jsp">
  <jsp:param name="classification" value="EverWorX-Help Desk:BDM"/>
</jsp:include>

<tr><td>&nbsp;</td></tr>

<jsp:include page="summary_section_everworx_cost.jsp">
  <jsp:param name="classification" value="EverWorX-Help Desk:SPM"/>
</jsp:include>


</table>
</td></tr>
</table>
</div> 
</body>
</html>