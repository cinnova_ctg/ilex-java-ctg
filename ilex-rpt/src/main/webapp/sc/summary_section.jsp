<%@ page import = "java.util.Vector" %>
<tr><td>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">

    <tr>
     <td width="70" rowspan="2" class="columnHeader2">BDMs</td>
     <td colspan="7" class="columnHeader2">Cost</td>
     <td colspan="1" rowspan="2" class="columnHeader2">&nbsp;&nbsp;&nbsp;Revenue&nbsp;&nbsp;&nbsp;</td>
     <td colspan="2" class="columnHeader2">VGPM</td>
   </tr>
   
   <tr>
     <td width="70" class="columnHeader2">Pro Forma</td>
     <td width="80" class="columnHeader2">Authorized</td>
     <td width="80" class="columnHeader2">Approved</td>
     <td width="70" class="columnHeader2">Actual</td>
     <td width="80" class="columnHeader2">Unallocated&nbsp;CTR</td>
     <td width="80" class="columnHeader2">Unallocated&nbsp;PMCL</td>
     <td width="70" class="columnHeader2">Corrected</td>
     <td width="80" class="columnHeader2">Pro Forma</td>
     <td width="80" class="columnHeader2">Actual</td>
   </tr>
 <%
 String classification = (String)request.getParameter("classification");
 Vector r = (Vector) request.getAttribute(classification);
 if (r.size()>0) {
	 
	 for (int i=0; i<r.size()-1; i++) {
		 String[] t = (String[])r.get(i);
 %>
			<tr>
			    <td class="cDataLeft<%=i%2%>"><%=t[2]%></td>
				<td class="cDataRight<%=i%2%>"><%=t[3]%></td>
				<td class="cDataRight<%=i%2%>"><%=t[4]%></td>
				<td class="cDataRight<%=i%2%>"><%=t[5]%></td>
				<td class="cDataRight<%=i%2%>"><%=t[6]%></td>
				<td class="cDataRight<%=i%2%>"><%=t[7]%></td>
				<td class="cDataRight<%=i%2%>"><%=t[8]%></td>
				<td class="cDataRight<%=i%2%>"><%=t[9]%></td>
				<td class="cDataRight<%=i%2%>"><%=t[11]%></td>
				<td class="cDataRight<%=i%2%>"><%=t[12]%></td>
				<td class="cDataRight<%=i%2%>"><%=t[13]%></td>
			</tr>
<%
	 }
 }
 
 // Get last row
 String[] t = (String[])r.get(r.size()-1);
%>

 <tr>
    <td class="cTotalRight1"><strong>Totals</strong>:&nbsp;&nbsp;&nbsp;</td>
    <td class="cTotalRight1"><%=t[3]%></td>
    <td class="cTotalRight1"><%=t[4]%></td>
    <td class="cTotalRight1"><%=t[5]%></td>
    <td class="cTotalRight1"><%=t[6]%></td>
    <td class="cTotalRight1"><%=t[7]%></td>
    <td class="cTotalRight1"><%=t[8]%></td>
    <td class="cTotalRight1"><%=t[9]%></td>
    <td class="cTotalRight1"><%=t[11]%></td>
    <td class="cTotalRight1"><%=t[12]%></td>
	<td class="cTotalRight1"><%=t[13]%></td>
    
  </tr>
</table>
</td></tr>
