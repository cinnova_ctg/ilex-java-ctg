<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
String quarter = (String)request.getAttribute("quarter");
String title = "Quarterly Commission Summary for "+quarter;
String subtitle = (String)request.getAttribute("subtitle");

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quarterly Commission Summary 2</title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<%@ include file="js.txt"%>
</head>
<body>
<div class="rptBody01">

<table border="0" cellspacing="0" cellpadding="0">

<tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<form name="form1" action="Troll.xo" method="post">
<input type="hidden" name="ac", value="com_new_data">
  <tr>
  <td rowspan="3"class="titleLine"><%=title%><br><span class="cDataLeft0"><%=subtitle%></span></td>
  <td align="right" valign="top">
  <table border="0" cellspacing="0" cellpadding="0">
  <tr><td align="right" valign="top">Job Type(s):&nbsp;&nbsp;&nbsp;</td>
  <td><select multiple name="job_types" size="3">
  <option value="Standard Billable">Standard Billable</option>
  <option value="NetMedX Billable">NetMedX Billable</option>
  <option value="MSP Billable">MSP Billable</option>
  <option value="Standard Non-Billable">Standard Non-Billable</option>
  <option value="NetMedX Non-Billable">NetMedX Non-Billable</option>
  <option value="MSP Non-Billable">MSP Non-Billable</option>
  </select></td></tr></table>
  </td>
  </tr>
  <tr>
  
  </tr> 
  
  <%
String status;
GregorianCalendar cal = new GregorianCalendar();
	//out.print(cal.get(Calendar.YEAR));
	int currentyr=cal.get(Calendar.YEAR);
	 String qtr="";
	 
for (int year = cal.get(Calendar.YEAR)-5; year<=currentyr; year++) {
	
        String row = Integer.toString(year);
        qtr+= "Qtr 1 "+row+"~Qtr 2 "+row+"~Qtr 3 "+row+"~Qtr 4 "+row+"~";
}     
     		
    %>
  
<%-- <%  
 if (qtr.charAt(qtr.length()-1)=='~')
    {
	 qtr = qtr.replace(qtr.substring(qtr.length()-1), "");
        
    }
%>  --%>   
 <td align="right">Quarter:&nbsp;&nbsp;&nbsp;<el:element type="menu1" request="<%=request%>" name="quarter" source=""  data="<%=qtr%>"   js=""  /></td>
<!-- data="Qtr 1 2010~Qtr 2 2010~Qtr 3 2010~Qtr 4 2010~Qtr 1 2011~Qtr 2 2011~Qtr 3 2011~Qtr 4 2011~Qtr 1 2012~Qtr 2 2012~Qtr 3 2012~Qtr 4 2012~Qtr 1 2013~Qtr 2 2013~Qtr 3 2013~Qtr 4 2013~Qtr 1 2014~Qtr 2 2014~Qtr 3 2014~Qtr 4 2014"  -->

  <tr>
  <td align="right" colspan="2">
  <input type="submit" name="default_view" value="Submit" class=date1></td>
  </tr>
  </form>
 </table>
</td></tr>
<!-- BDMs Main -->
<tr><td>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">

    <tr>
     <td width="110" rowspan="2" class="columnHeader2">BDMs</td>
     <td colspan="6" class="columnHeader2">Cost</td>
     <td colspan="2" class="columnHeader2">&nbsp;&nbsp;&nbsp;Revenue&nbsp;&nbsp;&nbsp;</td>
     <td colspan="2" class="columnHeader2">VGPM</td>
   </tr>
   
   <tr>
     <td width="70" class="columnHeader2">Pro Forma</td>
     <td width="70" class="columnHeader2">Actual</td>
     <td width="80" class="columnHeader2">Unallocated&nbsp;CTR</td>
     <td width="80" class="columnHeader2">Unallocated&nbsp;PMCL</td>
     <td width="80" class="columnHeader2">Unallocated&nbsp;NMX PMPC</td>
     <td width="70" class="columnHeader2">Corrected</td>
     <td width="80" class="columnHeader2">Inv&nbsp;Correction</td>
     <td width="80" class="columnHeader2">Total</td>
     <td width="80" class="columnHeader2">Pro Forma</td>
     <td width="80" class="columnHeader2">Actual</td>
   </tr>

<%
Vector rows = (Vector)request.getAttribute("bdm");

for (int i=0; i< rows.size(); i++) {
	String[] r = (String[])rows.get(i);
%>

			<tr>
				<td class="cDataLeft<%=i%2%>"><%=r[1]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[2]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[3]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[4]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[5]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[13]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[6]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[7]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[8]%></td>
				<td class="cDataCenter<%=i%2%>"><%=r[9]%></td>
				<td class="cDataCenter<%=i%2%>"><%=r[10]%></td>
			</tr>
   
<%
}

%>

</table>
</td></tr>
<!-- Spacer -->
<tr><td>&nbsp;</td></tr>
<!-- SPMs Main -->
<tr><td>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">

    <tr>
     <td width="110" rowspan="2" class="columnHeader2">SPMs</td>
     <td colspan="5" class="columnHeader2">Cost</td>
     <td colspan="2" class="columnHeader2">&nbsp;&nbsp;&nbsp;Revenue&nbsp;&nbsp;&nbsp;</td>
     <td colspan="2" class="columnHeader2">VGPM</td>
   </tr>
   
   <tr>
     <td width="70" class="columnHeader2">Pro Forma</td>
     <td width="70" class="columnHeader2">Actual</td>
     <td width="80" class="columnHeader2">Unallocated&nbsp;CTR</td>
     <td width="80" class="columnHeader2">Unallocated&nbsp;PMCL</td>
     <td width="70" class="columnHeader2">Corrected</td>
     <td width="80" class="columnHeader2">Inv&nbsp;Correction</td>
     <td width="80" class="columnHeader2">Total</td>
     <td width="80" class="columnHeader2">Pro Forma</td>
     <td width="80" class="columnHeader2">Actual</td>
   </tr>

<%
rows = (Vector)request.getAttribute("spm");

for (int i=0; i< rows.size(); i++) {
	String[] r = (String[])rows.get(i);
%>

			<tr>
				<td class="cDataLeft<%=i%2%>"><%=r[1]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[2]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[3]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[4]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[5]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[6]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[7]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[8]%></td>
				<td class="cDataCenter<%=i%2%>"><%=r[9]%></td>
				<td class="cDataCenter<%=i%2%>"><%=r[10]%></td>
			</tr>
   
<%
}

%>
</table>
</td></tr>

</table>
</td></tr>

<tr><td>&nbsp;</td></tr>

<tr><td>

  <table border="0" cellspacing="1" cellpadding="0">
   <tr><td><b>Cost</b></td></tr>
  <tr><td>Actual Cost - Authorized or approved purchase orders cost.</td></tr>
  <tr><td>Unallocated CTR - Unallocated contractor resources. These are resources not allocated to a purchase order.</td></tr>
  <tr><td>Unallocated PMCL - Unallocated internal Contingent resources. These are PM/PC/Logistic resources not allocated to a purchase order.</td></tr>
  <tr><td>Corrected = Actual Cost + Unallocated CTR + Unallocated PMCL</td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr><td><b>Revenue</b></td></tr>
  <tr><td>Inv Correction - The difference between an Ilex invoice price and customer payment as recorded in GP.</td></tr>
  <tr><td>Total = Ilex Invoice Price + GP Invoice Correction</td></tr>
  </table>
  
</td></tr>

</table>
</div> 
</body>
</html>