<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.util.Map" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
String quarter = (String)request.getAttribute("quarter");
String title = "Quarterly Commission Summary for "+quarter;
String subtitle = (String)request.getAttribute("subtitle");
String individual = (String)request.getAttribute("individual");

String type = (String)request.getAttribute("type");
String manager = "";
if ("bdm".equals(type)) {
	manager = "ac_jd_bdm";
} else {
	manager = "ac_jd_spm";
}
String manager_value = (String)request.getAttribute(manager);
String return_link = (String)request.getAttribute("return_link");
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quarterly Commission Summary 2</title>
<%@ include file="styles.txt"%>
<%@ include file="calendar.txt"%> 
<%@ include file="js.txt"%>
</head>
<body>
<div class="rptBody01">

<table border="0" cellspacing="0" cellpadding="0">

<tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<form name="form1" action="Troll.xo" method="post">
<input type="hidden" name="ac", value="com_new_data_manager">
<input type="hidden" name="type", value="<%=type%>">
<input type="hidden" name="<%=manager%>", value="<%=manager_value%>">
  <tr>
  <td rowspan="3"class="titleLine"><%=title%> for <span class="sectionTitleLine"><%=individual%></span><br><span class="cDataLeft0"><%=subtitle%></span><br>
     <span class="cDataLeft0"><a href='Troll.xo?ac=com_new_data&<%=return_link%>'>Back to All Commissions</a></span></td>
  <td align="right" valign="top">
  <table border="0" cellspacing="0" cellpadding="0">
  <tr><td align="right" valign="top">Job Type(s):&nbsp;&nbsp;&nbsp;</td>
  <td><select multiple name="job_types" size="3">
  <option value="Standard Billable">Standard Billable</option>
  <option value="NetMedX Billable">NetMedX Billable</option>
  <option value="MSP Billable">MSP Billable</option>
  <option value="Standard Non-Billable">Standard Non-Billable</option>
  <option value="NetMedX Non-Billable">NetMedX Non-Billable</option>
  <option value="MSP Non-Billable">MSP Non-Billable</option>
  </select></td></tr></table>
  </td>
  </tr>
  
    <%
String status;
GregorianCalendar cal = new GregorianCalendar();
	//out.print(cal.get(Calendar.YEAR));
	int currentyr=cal.get(Calendar.YEAR);
	 String qtr="";
	 
for (int year = cal.get(Calendar.YEAR)-5; year<=currentyr; year++) {
	
        String row = Integer.toString(year);
        qtr+= "Qtr 1 "+row+"~Qtr 2 "+row+"~Qtr 3 "+row+"~Qtr 4 "+row+"~";
}     
     		
    %>
  
  <tr>
  <td align="right">Quarter:&nbsp;&nbsp;&nbsp;<el:element type="menu1" request="<%=request%>" name="quarter" source="" data="<%=qtr%>"  js=""/></td>
  </tr>
  <tr>
  <td align="right">
  <input type="submit" name="default_view" value="Submit" class=date1></td>
  </tr>
  </form>
 </table>
</td></tr>
<!-- BDMs Main -->
<tr><td>
<table border="0" cellspacing="0" cellpadding="0">
<tr><td>
<table border="0" cellspacing="1" cellpadding="0" class="outlineYellow3">

    <tr>
     <td rowspan="2" class="columnHeader2">Customer/Appendix</td>
     <td colspan="5" class="columnHeader2">Cost</td>
     <td colspan="2" class="columnHeader2">&nbsp;&nbsp;&nbsp;Revenue&nbsp;&nbsp;&nbsp;</td>
     <td colspan="2" class="columnHeader2">VGPM</td>
   </tr>
   
   <tr>
     <td width="70" class="columnHeader2">Pro Forma</td>
     <td width="70" class="columnHeader2">Actual</td>
     <td width="80" class="columnHeader2">Unallocated&nbsp;CTR</td>
     <td width="80" class="columnHeader2">Unallocated&nbsp;PMCL</td>
     <td width="70" class="columnHeader2">Corrected</td>
     <td width="80" class="columnHeader2">Inv&nbsp;Correction</td>
     <td width="80" class="columnHeader2">Total</td>
     <td width="80" class="columnHeader2">Pro Forma</td>
     <td width="80" class="columnHeader2">Actual</td>
   </tr>

<%
Vector rows = (Vector)request.getAttribute("bdm");

for (int i=0; i< rows.size(); i++) {
	String[] r = (String[])rows.get(i);
%>

			<tr>
				<td class="cDataLeft<%=i%2%>"><%=r[1]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[2]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[3]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[4]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[5]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[6]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[7]%></td>
				<td class="cDataRight<%=i%2%>"><%=r[8]%></td>
				<td class="cDataCenter<%=i%2%>"><%=r[9]%></td>
				<td class="cDataCenter<%=i%2%>"><%=r[10]%></td>
			</tr>
<%
}
%>
</table>
</td></tr>
<!-- Spacer -->

<tr><td>&nbsp;</td></tr>

<tr><td>

  <table border="0" cellspacing="1" cellpadding="0">
   <tr><td><b>Cost</b></td></tr>
  <tr><td>Actual Cost - Authorized or approved purchase orders cost.</td></tr>
  <tr><td>Unallocated CTR - Unallocated contractor resources. These are resources not allocated to a purchase order.</td></tr>
  <tr><td>Unallocated PMCL - Unallocated internal Contingent resources. These are PM/PC/Logistic resources not allocated to a purchase order.</td></tr>
  <tr><td>Corrected = Actual Cost + Unallocated CTR + Unallocated PMCL</td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr><td><b>Revenue</b></td></tr>
  <tr><td>Inv Correction - The difference between an Ilex invoice price and customer payment as recorded in GP.</td></tr>
  <tr><td>Total = Ilex Invoice Price + GP Invoice Correction</td></tr>
  </table>
  
</td></tr>

</table>
</div> 
</body>
</html>