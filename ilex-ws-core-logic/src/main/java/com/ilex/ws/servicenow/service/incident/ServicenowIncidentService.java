package com.ilex.ws.servicenow.service.incident;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.ilex.ws.servicenow.service.incident.GetRecordsResponse.GetRecordsResult;

@WebService(targetNamespace = "")
public interface ServicenowIncidentService {

	@WebMethod(action = "http://www.service-now.com/incident/getRecords")
	public void getRecords(
			@WebParam(name = "number", targetNamespace = "") java.lang.String number,
			@WebParam(mode = WebParam.Mode.OUT, name = "getRecordsResult", targetNamespace = "") javax.xml.ws.Holder<java.util.List<GetRecordsResult>> getRecordsResult);

}
