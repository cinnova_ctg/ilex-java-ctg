package com.ilex.ws.servicenow.service.incident;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getRecordsResult" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="active" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="activity_due" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="approval" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="approval_set" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="assigned_to" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="assignment_group" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="business_duration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="business_stc" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="calendar_duration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="calendar_stc" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="caller_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="category" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="caused_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="child_incidents" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="close_code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="close_notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="closed_at" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="closed_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="cmdb_ci" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="comments_and_work_notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="contact_type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="correlation_display" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="correlation_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="delivery_plan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="delivery_task" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="due_date" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="escalation" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="expected_start" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="follow_up" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="group_list" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="impact" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="incident_state" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="knowledge" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="location" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="made_sla" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="notify" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="opened_at" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="opened_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="order" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="parent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="parent_incident" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="problem_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="reassignment_count" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="reopen_count" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="resolved_at" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="resolved_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="rfc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="severity" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="sla_due" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="subcategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="sys_class_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="sys_created_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="sys_created_on" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="sys_domain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="sys_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="sys_mod_count" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="sys_updated_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="sys_updated_on" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="time_worked" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="u_short_description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="u_task_assignment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="u_test_task" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="upon_approval" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="upon_reject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="urgency" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="user_input" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="watch_list" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="work_end" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="work_notes_list" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="work_start" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getRecordsResult" })
@XmlRootElement(name = "getRecordsResponse")
public class GetRecordsResponse {

	protected List<GetRecordsResponse.GetRecordsResult> getRecordsResult;

	/**
	 * Gets the value of the getRecordsResult property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the getRecordsResult property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getGetRecordsResult().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link GetRecordsResponse.GetRecordsResult }
	 * 
	 * 
	 */
	public List<GetRecordsResponse.GetRecordsResult> getGetRecordsResult() {
		if (getRecordsResult == null) {
			getRecordsResult = new ArrayList<GetRecordsResponse.GetRecordsResult>();
		}
		return this.getRecordsResult;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="active" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
	 *         &lt;element name="activity_due" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="approval" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="approval_set" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="assigned_to" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="assignment_group" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="business_duration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="business_stc" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="calendar_duration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="calendar_stc" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="caller_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="category" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="caused_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="child_incidents" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="close_code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="close_notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="closed_at" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="closed_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="cmdb_ci" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="comments_and_work_notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="contact_type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="correlation_display" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="correlation_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="delivery_plan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="delivery_task" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="due_date" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="escalation" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="expected_start" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="follow_up" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="group_list" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="impact" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="incident_state" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="knowledge" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
	 *         &lt;element name="location" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="made_sla" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
	 *         &lt;element name="notify" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="opened_at" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="opened_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="order" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="parent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="parent_incident" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="problem_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="reassignment_count" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="reopen_count" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="resolved_at" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="resolved_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="rfc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="severity" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="sla_due" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="subcategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="sys_class_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="sys_created_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="sys_created_on" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="sys_domain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="sys_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="sys_mod_count" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="sys_updated_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="sys_updated_on" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="time_worked" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="u_short_description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="u_task_assignment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="u_test_task" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="upon_approval" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="upon_reject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="urgency" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
	 *         &lt;element name="user_input" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="watch_list" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="work_end" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="work_notes_list" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="work_start" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "active", "activityDue", "approval",
			"approvalSet", "assignedTo", "assignmentGroup", "businessDuration",
			"businessStc", "calendarDuration", "calendarStc", "callerId",
			"category", "causedBy", "childIncidents", "closeCode",
			"closeNotes", "closedAt", "closedBy", "cmdbCi",
			"commentsAndWorkNotes", "company", "contactType",
			"correlationDisplay", "correlationId", "deliveryPlan",
			"deliveryTask", "description", "dueDate", "escalation",
			"expectedStart", "followUp", "groupList", "impact",
			"incidentState", "knowledge", "location", "madeSla", "notify",
			"number", "openedAt", "openedBy", "order", "parent",
			"parentIncident", "priority", "problemId", "reassignmentCount",
			"reopenCount", "resolvedAt", "resolvedBy", "rfc", "severity",
			"shortDescription", "slaDue", "state", "subcategory",
			"sysClassName", "sysCreatedBy", "sysCreatedOn", "sysDomain",
			"sysId", "sysModCount", "sysUpdatedBy", "sysUpdatedOn",
			"timeWorked", "uShortDescription", "uTaskAssignment", "uTestTask",
			"uponApproval", "uponReject", "urgency", "userInput", "watchList",
			"workEnd", "workNotesList", "workStart" })
	public static class GetRecordsResult {

		protected Boolean active;
		@XmlElement(name = "activity_due")
		protected String activityDue;
		protected String approval;
		@XmlElement(name = "approval_set")
		protected String approvalSet;
		@XmlElement(name = "assigned_to")
		protected String assignedTo;
		@XmlElement(name = "assignment_group")
		protected String assignmentGroup;
		@XmlElement(name = "business_duration")
		protected String businessDuration;
		@XmlElement(name = "business_stc")
		protected BigInteger businessStc;
		@XmlElement(name = "calendar_duration")
		protected String calendarDuration;
		@XmlElement(name = "calendar_stc")
		protected BigInteger calendarStc;
		@XmlElement(name = "caller_id")
		protected String callerId;
		protected String category;
		@XmlElement(name = "caused_by")
		protected String causedBy;
		@XmlElement(name = "child_incidents")
		protected BigInteger childIncidents;
		@XmlElement(name = "close_code")
		protected String closeCode;
		@XmlElement(name = "close_notes")
		protected String closeNotes;
		@XmlElement(name = "closed_at")
		protected String closedAt;
		@XmlElement(name = "closed_by")
		protected String closedBy;
		@XmlElement(name = "cmdb_ci")
		protected String cmdbCi;
		@XmlElement(name = "comments_and_work_notes")
		protected String commentsAndWorkNotes;
		protected String company;
		@XmlElement(name = "contact_type")
		protected String contactType;
		@XmlElement(name = "correlation_display")
		protected String correlationDisplay;
		@XmlElement(name = "correlation_id")
		protected String correlationId;
		@XmlElement(name = "delivery_plan")
		protected String deliveryPlan;
		@XmlElement(name = "delivery_task")
		protected String deliveryTask;
		protected String description;
		@XmlElement(name = "due_date")
		protected String dueDate;
		protected BigInteger escalation;
		@XmlElement(name = "expected_start")
		protected String expectedStart;
		@XmlElement(name = "follow_up")
		protected String followUp;
		@XmlElement(name = "group_list")
		protected String groupList;
		protected BigInteger impact;
		@XmlElement(name = "incident_state")
		protected BigInteger incidentState;
		protected Boolean knowledge;
		protected String location;
		@XmlElement(name = "made_sla")
		protected Boolean madeSla;
		protected BigInteger notify;
		protected String number;
		@XmlElement(name = "opened_at")
		protected String openedAt;
		@XmlElement(name = "opened_by")
		protected String openedBy;
		protected BigInteger order;
		protected String parent;
		@XmlElement(name = "parent_incident")
		protected String parentIncident;
		protected BigInteger priority;
		@XmlElement(name = "problem_id")
		protected String problemId;
		@XmlElement(name = "reassignment_count")
		protected BigInteger reassignmentCount;
		@XmlElement(name = "reopen_count")
		protected BigInteger reopenCount;
		@XmlElement(name = "resolved_at")
		protected String resolvedAt;
		@XmlElement(name = "resolved_by")
		protected String resolvedBy;
		protected String rfc;
		protected BigInteger severity;
		@XmlElement(name = "short_description")
		protected String shortDescription;
		@XmlElement(name = "sla_due")
		protected String slaDue;
		protected BigInteger state;
		protected String subcategory;
		@XmlElement(name = "sys_class_name")
		protected String sysClassName;
		@XmlElement(name = "sys_created_by")
		protected String sysCreatedBy;
		@XmlElement(name = "sys_created_on")
		protected String sysCreatedOn;
		@XmlElement(name = "sys_domain")
		protected String sysDomain;
		@XmlElement(name = "sys_id")
		protected String sysId;
		@XmlElement(name = "sys_mod_count")
		protected BigInteger sysModCount;
		@XmlElement(name = "sys_updated_by")
		protected String sysUpdatedBy;
		@XmlElement(name = "sys_updated_on")
		protected String sysUpdatedOn;
		@XmlElement(name = "time_worked")
		protected String timeWorked;
		@XmlElement(name = "u_short_description")
		protected String uShortDescription;
		@XmlElement(name = "u_task_assignment")
		protected String uTaskAssignment;
		@XmlElement(name = "u_test_task")
		protected String uTestTask;
		@XmlElement(name = "upon_approval")
		protected String uponApproval;
		@XmlElement(name = "upon_reject")
		protected String uponReject;
		protected BigInteger urgency;
		@XmlElement(name = "user_input")
		protected String userInput;
		@XmlElement(name = "watch_list")
		protected String watchList;
		@XmlElement(name = "work_end")
		protected String workEnd;
		@XmlElement(name = "work_notes_list")
		protected String workNotesList;
		@XmlElement(name = "work_start")
		protected String workStart;

		/**
		 * Gets the value of the active property.
		 * 
		 * @return possible object is {@link Boolean }
		 * 
		 */
		public Boolean isActive() {
			return active;
		}

		/**
		 * Sets the value of the active property.
		 * 
		 * @param value
		 *            allowed object is {@link Boolean }
		 * 
		 */
		public void setActive(Boolean value) {
			this.active = value;
		}

		/**
		 * Gets the value of the activityDue property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getActivityDue() {
			return activityDue;
		}

		/**
		 * Sets the value of the activityDue property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setActivityDue(String value) {
			this.activityDue = value;
		}

		/**
		 * Gets the value of the approval property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getApproval() {
			return approval;
		}

		/**
		 * Sets the value of the approval property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setApproval(String value) {
			this.approval = value;
		}

		/**
		 * Gets the value of the approvalSet property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getApprovalSet() {
			return approvalSet;
		}

		/**
		 * Sets the value of the approvalSet property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setApprovalSet(String value) {
			this.approvalSet = value;
		}

		/**
		 * Gets the value of the assignedTo property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getAssignedTo() {
			return assignedTo;
		}

		/**
		 * Sets the value of the assignedTo property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setAssignedTo(String value) {
			this.assignedTo = value;
		}

		/**
		 * Gets the value of the assignmentGroup property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getAssignmentGroup() {
			return assignmentGroup;
		}

		/**
		 * Sets the value of the assignmentGroup property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setAssignmentGroup(String value) {
			this.assignmentGroup = value;
		}

		/**
		 * Gets the value of the businessDuration property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getBusinessDuration() {
			return businessDuration;
		}

		/**
		 * Sets the value of the businessDuration property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setBusinessDuration(String value) {
			this.businessDuration = value;
		}

		/**
		 * Gets the value of the businessStc property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getBusinessStc() {
			return businessStc;
		}

		/**
		 * Sets the value of the businessStc property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setBusinessStc(BigInteger value) {
			this.businessStc = value;
		}

		/**
		 * Gets the value of the calendarDuration property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCalendarDuration() {
			return calendarDuration;
		}

		/**
		 * Sets the value of the calendarDuration property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCalendarDuration(String value) {
			this.calendarDuration = value;
		}

		/**
		 * Gets the value of the calendarStc property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getCalendarStc() {
			return calendarStc;
		}

		/**
		 * Sets the value of the calendarStc property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setCalendarStc(BigInteger value) {
			this.calendarStc = value;
		}

		/**
		 * Gets the value of the callerId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCallerId() {
			return callerId;
		}

		/**
		 * Sets the value of the callerId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCallerId(String value) {
			this.callerId = value;
		}

		/**
		 * Gets the value of the category property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCategory() {
			return category;
		}

		/**
		 * Sets the value of the category property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCategory(String value) {
			this.category = value;
		}

		/**
		 * Gets the value of the causedBy property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCausedBy() {
			return causedBy;
		}

		/**
		 * Sets the value of the causedBy property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCausedBy(String value) {
			this.causedBy = value;
		}

		/**
		 * Gets the value of the childIncidents property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getChildIncidents() {
			return childIncidents;
		}

		/**
		 * Sets the value of the childIncidents property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setChildIncidents(BigInteger value) {
			this.childIncidents = value;
		}

		/**
		 * Gets the value of the closeCode property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCloseCode() {
			return closeCode;
		}

		/**
		 * Sets the value of the closeCode property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCloseCode(String value) {
			this.closeCode = value;
		}

		/**
		 * Gets the value of the closeNotes property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCloseNotes() {
			return closeNotes;
		}

		/**
		 * Sets the value of the closeNotes property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCloseNotes(String value) {
			this.closeNotes = value;
		}

		/**
		 * Gets the value of the closedAt property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getClosedAt() {
			return closedAt;
		}

		/**
		 * Sets the value of the closedAt property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setClosedAt(String value) {
			this.closedAt = value;
		}

		/**
		 * Gets the value of the closedBy property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getClosedBy() {
			return closedBy;
		}

		/**
		 * Sets the value of the closedBy property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setClosedBy(String value) {
			this.closedBy = value;
		}

		/**
		 * Gets the value of the cmdbCi property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCmdbCi() {
			return cmdbCi;
		}

		/**
		 * Sets the value of the cmdbCi property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCmdbCi(String value) {
			this.cmdbCi = value;
		}

		/**
		 * Gets the value of the commentsAndWorkNotes property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCommentsAndWorkNotes() {
			return commentsAndWorkNotes;
		}

		/**
		 * Sets the value of the commentsAndWorkNotes property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCommentsAndWorkNotes(String value) {
			this.commentsAndWorkNotes = value;
		}

		/**
		 * Gets the value of the company property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCompany() {
			return company;
		}

		/**
		 * Sets the value of the company property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCompany(String value) {
			this.company = value;
		}

		/**
		 * Gets the value of the contactType property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getContactType() {
			return contactType;
		}

		/**
		 * Sets the value of the contactType property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setContactType(String value) {
			this.contactType = value;
		}

		/**
		 * Gets the value of the correlationDisplay property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCorrelationDisplay() {
			return correlationDisplay;
		}

		/**
		 * Sets the value of the correlationDisplay property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCorrelationDisplay(String value) {
			this.correlationDisplay = value;
		}

		/**
		 * Gets the value of the correlationId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCorrelationId() {
			return correlationId;
		}

		/**
		 * Sets the value of the correlationId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCorrelationId(String value) {
			this.correlationId = value;
		}

		/**
		 * Gets the value of the deliveryPlan property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getDeliveryPlan() {
			return deliveryPlan;
		}

		/**
		 * Sets the value of the deliveryPlan property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setDeliveryPlan(String value) {
			this.deliveryPlan = value;
		}

		/**
		 * Gets the value of the deliveryTask property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getDeliveryTask() {
			return deliveryTask;
		}

		/**
		 * Sets the value of the deliveryTask property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setDeliveryTask(String value) {
			this.deliveryTask = value;
		}

		/**
		 * Gets the value of the description property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getDescription() {
			return description;
		}

		/**
		 * Sets the value of the description property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setDescription(String value) {
			this.description = value;
		}

		/**
		 * Gets the value of the dueDate property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getDueDate() {
			return dueDate;
		}

		/**
		 * Sets the value of the dueDate property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setDueDate(String value) {
			this.dueDate = value;
		}

		/**
		 * Gets the value of the escalation property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getEscalation() {
			return escalation;
		}

		/**
		 * Sets the value of the escalation property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setEscalation(BigInteger value) {
			this.escalation = value;
		}

		/**
		 * Gets the value of the expectedStart property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getExpectedStart() {
			return expectedStart;
		}

		/**
		 * Sets the value of the expectedStart property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setExpectedStart(String value) {
			this.expectedStart = value;
		}

		/**
		 * Gets the value of the followUp property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getFollowUp() {
			return followUp;
		}

		/**
		 * Sets the value of the followUp property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setFollowUp(String value) {
			this.followUp = value;
		}

		/**
		 * Gets the value of the groupList property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getGroupList() {
			return groupList;
		}

		/**
		 * Sets the value of the groupList property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setGroupList(String value) {
			this.groupList = value;
		}

		/**
		 * Gets the value of the impact property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getImpact() {
			return impact;
		}

		/**
		 * Sets the value of the impact property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setImpact(BigInteger value) {
			this.impact = value;
		}

		/**
		 * Gets the value of the incidentState property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getIncidentState() {
			return incidentState;
		}

		/**
		 * Sets the value of the incidentState property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setIncidentState(BigInteger value) {
			this.incidentState = value;
		}

		/**
		 * Gets the value of the knowledge property.
		 * 
		 * @return possible object is {@link Boolean }
		 * 
		 */
		public Boolean isKnowledge() {
			return knowledge;
		}

		/**
		 * Sets the value of the knowledge property.
		 * 
		 * @param value
		 *            allowed object is {@link Boolean }
		 * 
		 */
		public void setKnowledge(Boolean value) {
			this.knowledge = value;
		}

		/**
		 * Gets the value of the location property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getLocation() {
			return location;
		}

		/**
		 * Sets the value of the location property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setLocation(String value) {
			this.location = value;
		}

		/**
		 * Gets the value of the madeSla property.
		 * 
		 * @return possible object is {@link Boolean }
		 * 
		 */
		public Boolean isMadeSla() {
			return madeSla;
		}

		/**
		 * Sets the value of the madeSla property.
		 * 
		 * @param value
		 *            allowed object is {@link Boolean }
		 * 
		 */
		public void setMadeSla(Boolean value) {
			this.madeSla = value;
		}

		/**
		 * Gets the value of the notify property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getNotify() {
			return notify;
		}

		/**
		 * Sets the value of the notify property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setNotify(BigInteger value) {
			this.notify = value;
		}

		/**
		 * Gets the value of the number property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getNumber() {
			return number;
		}

		/**
		 * Sets the value of the number property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setNumber(String value) {
			this.number = value;
		}

		/**
		 * Gets the value of the openedAt property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getOpenedAt() {
			return openedAt;
		}

		/**
		 * Sets the value of the openedAt property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setOpenedAt(String value) {
			this.openedAt = value;
		}

		/**
		 * Gets the value of the openedBy property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getOpenedBy() {
			return openedBy;
		}

		/**
		 * Sets the value of the openedBy property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setOpenedBy(String value) {
			this.openedBy = value;
		}

		/**
		 * Gets the value of the order property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getOrder() {
			return order;
		}

		/**
		 * Sets the value of the order property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setOrder(BigInteger value) {
			this.order = value;
		}

		/**
		 * Gets the value of the parent property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getParent() {
			return parent;
		}

		/**
		 * Sets the value of the parent property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setParent(String value) {
			this.parent = value;
		}

		/**
		 * Gets the value of the parentIncident property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getParentIncident() {
			return parentIncident;
		}

		/**
		 * Sets the value of the parentIncident property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setParentIncident(String value) {
			this.parentIncident = value;
		}

		/**
		 * Gets the value of the priority property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getPriority() {
			return priority;
		}

		/**
		 * Sets the value of the priority property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setPriority(BigInteger value) {
			this.priority = value;
		}

		/**
		 * Gets the value of the problemId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getProblemId() {
			return problemId;
		}

		/**
		 * Sets the value of the problemId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setProblemId(String value) {
			this.problemId = value;
		}

		/**
		 * Gets the value of the reassignmentCount property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getReassignmentCount() {
			return reassignmentCount;
		}

		/**
		 * Sets the value of the reassignmentCount property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setReassignmentCount(BigInteger value) {
			this.reassignmentCount = value;
		}

		/**
		 * Gets the value of the reopenCount property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getReopenCount() {
			return reopenCount;
		}

		/**
		 * Sets the value of the reopenCount property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setReopenCount(BigInteger value) {
			this.reopenCount = value;
		}

		/**
		 * Gets the value of the resolvedAt property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getResolvedAt() {
			return resolvedAt;
		}

		/**
		 * Sets the value of the resolvedAt property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setResolvedAt(String value) {
			this.resolvedAt = value;
		}

		/**
		 * Gets the value of the resolvedBy property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getResolvedBy() {
			return resolvedBy;
		}

		/**
		 * Sets the value of the resolvedBy property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setResolvedBy(String value) {
			this.resolvedBy = value;
		}

		/**
		 * Gets the value of the rfc property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getRfc() {
			return rfc;
		}

		/**
		 * Sets the value of the rfc property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setRfc(String value) {
			this.rfc = value;
		}

		/**
		 * Gets the value of the severity property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getSeverity() {
			return severity;
		}

		/**
		 * Sets the value of the severity property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setSeverity(BigInteger value) {
			this.severity = value;
		}

		/**
		 * Gets the value of the shortDescription property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getShortDescription() {
			return shortDescription;
		}

		/**
		 * Sets the value of the shortDescription property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setShortDescription(String value) {
			this.shortDescription = value;
		}

		/**
		 * Gets the value of the slaDue property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSlaDue() {
			return slaDue;
		}

		/**
		 * Sets the value of the slaDue property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSlaDue(String value) {
			this.slaDue = value;
		}

		/**
		 * Gets the value of the state property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getState() {
			return state;
		}

		/**
		 * Sets the value of the state property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setState(BigInteger value) {
			this.state = value;
		}

		/**
		 * Gets the value of the subcategory property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSubcategory() {
			return subcategory;
		}

		/**
		 * Sets the value of the subcategory property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSubcategory(String value) {
			this.subcategory = value;
		}

		/**
		 * Gets the value of the sysClassName property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSysClassName() {
			return sysClassName;
		}

		/**
		 * Sets the value of the sysClassName property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSysClassName(String value) {
			this.sysClassName = value;
		}

		/**
		 * Gets the value of the sysCreatedBy property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSysCreatedBy() {
			return sysCreatedBy;
		}

		/**
		 * Sets the value of the sysCreatedBy property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSysCreatedBy(String value) {
			this.sysCreatedBy = value;
		}

		/**
		 * Gets the value of the sysCreatedOn property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSysCreatedOn() {
			return sysCreatedOn;
		}

		/**
		 * Sets the value of the sysCreatedOn property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSysCreatedOn(String value) {
			this.sysCreatedOn = value;
		}

		/**
		 * Gets the value of the sysDomain property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSysDomain() {
			return sysDomain;
		}

		/**
		 * Sets the value of the sysDomain property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSysDomain(String value) {
			this.sysDomain = value;
		}

		/**
		 * Gets the value of the sysId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSysId() {
			return sysId;
		}

		/**
		 * Sets the value of the sysId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSysId(String value) {
			this.sysId = value;
		}

		/**
		 * Gets the value of the sysModCount property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getSysModCount() {
			return sysModCount;
		}

		/**
		 * Sets the value of the sysModCount property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setSysModCount(BigInteger value) {
			this.sysModCount = value;
		}

		/**
		 * Gets the value of the sysUpdatedBy property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSysUpdatedBy() {
			return sysUpdatedBy;
		}

		/**
		 * Sets the value of the sysUpdatedBy property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSysUpdatedBy(String value) {
			this.sysUpdatedBy = value;
		}

		/**
		 * Gets the value of the sysUpdatedOn property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSysUpdatedOn() {
			return sysUpdatedOn;
		}

		/**
		 * Sets the value of the sysUpdatedOn property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSysUpdatedOn(String value) {
			this.sysUpdatedOn = value;
		}

		/**
		 * Gets the value of the timeWorked property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getTimeWorked() {
			return timeWorked;
		}

		/**
		 * Sets the value of the timeWorked property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setTimeWorked(String value) {
			this.timeWorked = value;
		}

		/**
		 * Gets the value of the uShortDescription property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getUShortDescription() {
			return uShortDescription;
		}

		/**
		 * Sets the value of the uShortDescription property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setUShortDescription(String value) {
			this.uShortDescription = value;
		}

		/**
		 * Gets the value of the uTaskAssignment property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getUTaskAssignment() {
			return uTaskAssignment;
		}

		/**
		 * Sets the value of the uTaskAssignment property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setUTaskAssignment(String value) {
			this.uTaskAssignment = value;
		}

		/**
		 * Gets the value of the uTestTask property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getUTestTask() {
			return uTestTask;
		}

		/**
		 * Sets the value of the uTestTask property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setUTestTask(String value) {
			this.uTestTask = value;
		}

		/**
		 * Gets the value of the uponApproval property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getUponApproval() {
			return uponApproval;
		}

		/**
		 * Sets the value of the uponApproval property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setUponApproval(String value) {
			this.uponApproval = value;
		}

		/**
		 * Gets the value of the uponReject property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getUponReject() {
			return uponReject;
		}

		/**
		 * Sets the value of the uponReject property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setUponReject(String value) {
			this.uponReject = value;
		}

		/**
		 * Gets the value of the urgency property.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public BigInteger getUrgency() {
			return urgency;
		}

		/**
		 * Sets the value of the urgency property.
		 * 
		 * @param value
		 *            allowed object is {@link BigInteger }
		 * 
		 */
		public void setUrgency(BigInteger value) {
			this.urgency = value;
		}

		/**
		 * Gets the value of the userInput property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getUserInput() {
			return userInput;
		}

		/**
		 * Sets the value of the userInput property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setUserInput(String value) {
			this.userInput = value;
		}

		/**
		 * Gets the value of the watchList property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getWatchList() {
			return watchList;
		}

		/**
		 * Sets the value of the watchList property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setWatchList(String value) {
			this.watchList = value;
		}

		/**
		 * Gets the value of the workEnd property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getWorkEnd() {
			return workEnd;
		}

		/**
		 * Sets the value of the workEnd property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setWorkEnd(String value) {
			this.workEnd = value;
		}

		/**
		 * Gets the value of the workNotesList property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getWorkNotesList() {
			return workNotesList;
		}

		/**
		 * Sets the value of the workNotesList property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setWorkNotesList(String value) {
			this.workNotesList = value;
		}

		/**
		 * Gets the value of the workStart property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getWorkStart() {
			return workStart;
		}

		/**
		 * Sets the value of the workStart property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setWorkStart(String value) {
			this.workStart = value;
		}

	}

}
