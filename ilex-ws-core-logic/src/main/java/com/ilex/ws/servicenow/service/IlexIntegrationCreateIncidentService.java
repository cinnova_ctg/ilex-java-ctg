package com.ilex.ws.servicenow.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * The Interface IlexIntegrationCreateIncidentService. Used to create Proactive
 * Incident i.e from Ilex To Service Now
 */
@WebService(targetNamespace = "http://www.service-now.com/u_integration_create_incident")
public interface IlexIntegrationCreateIncidentService {

	@WebMethod(action = "http://www.service-now.com/u_integration_create_incident/insert")
	public void insert(
			@WebParam(name = "u_category", targetNamespace = "") java.lang.String uCategory,
			@WebParam(name = "u_configuration_item", targetNamespace = "") java.lang.String uConfigurationItem,
			@WebParam(name = "u_description", targetNamespace = "") java.lang.String uDescription,
			@WebParam(name = "u_impact", targetNamespace = "") java.lang.String uImpact,
			@WebParam(name = "u_short_description", targetNamespace = "") java.lang.String uShortDescription,
			@WebParam(name = "u_site_number", targetNamespace = "") java.lang.String uSiteNumber,
			@WebParam(name = "u_state", targetNamespace = "") java.lang.String uState,
			@WebParam(name = "u_symptom", targetNamespace = "") java.lang.String uSymptom,
			@WebParam(name = "u_ticket_number", targetNamespace = "") java.lang.String uTicketNumber,
			@WebParam(name = "u_type", targetNamespace = "") java.lang.String uType,
			@WebParam(name = "u_urgency", targetNamespace = "") java.lang.String uUrgency,
			@WebParam(name = "u_vendor_key", targetNamespace = "") java.lang.String uVendor_key,
			@WebParam(name = "u_work_notes", targetNamespace = "") java.lang.String uWorkNotes,
			@WebParam(mode = WebParam.Mode.OUT, name = "sys_id", targetNamespace = "http://www.service-now.com/u_integration_create_incident") javax.xml.ws.Holder<java.lang.String> sysId,
			@WebParam(mode = WebParam.Mode.OUT, name = "table", targetNamespace = "http://www.service-now.com/u_integration_create_incident") javax.xml.ws.Holder<java.lang.String> table,
			@WebParam(mode = WebParam.Mode.OUT, name = "display_name", targetNamespace = "http://www.service-now.com/u_integration_create_incident") javax.xml.ws.Holder<java.lang.String> displayName,
			@WebParam(mode = WebParam.Mode.OUT, name = "display_value", targetNamespace = "http://www.service-now.com/u_integration_create_incident") javax.xml.ws.Holder<java.lang.String> displayValue,
			@WebParam(mode = WebParam.Mode.OUT, name = "status", targetNamespace = "http://www.service-now.com/u_integration_create_incident") javax.xml.ws.Holder<java.lang.String> status,
			@WebParam(mode = WebParam.Mode.OUT, name = "status_message", targetNamespace = "http://www.service-now.com/u_integration_create_incident") javax.xml.ws.Holder<java.lang.String> statusMessage,
			@WebParam(mode = WebParam.Mode.OUT, name = "error_message", targetNamespace = "http://www.service-now.com/u_integration_create_incident") javax.xml.ws.Holder<java.lang.String> errorMessage,
			@WebParam(mode = WebParam.Mode.OUT, name = "error_number", targetNamespace = "http://www.service-now.com/u_integration_create_incident") javax.xml.ws.Holder<java.lang.String> error_number,
			@WebParam(mode = WebParam.Mode.OUT, name = "error_description", targetNamespace = "http://www.service-now.com/u_integration_create_incident") javax.xml.ws.Holder<java.lang.String> error_description,
			@WebParam(mode = WebParam.Mode.OUT, name = "vTaskNumber", targetNamespace = "http://www.service-now.com/u_integration_create_incident") javax.xml.ws.Holder<java.lang.String> vTaskNumber,
			@WebParam(mode = WebParam.Mode.OUT, name = "ticketNumber", targetNamespace = "http://www.service-now.com/u_integration_create_incident") javax.xml.ws.Holder<java.lang.String> ticketNumber);

}
