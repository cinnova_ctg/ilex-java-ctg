package com.ilex.ws.servicenow.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(targetNamespace = "http://www.service-now.com/u_ilex_integration")
public interface IlexIntegrationService {

	@WebMethod(action = "http://www.service-now.com/u_ilex_integration/insert")
	public void insert(
			@WebParam(name = "active", targetNamespace = "") java.lang.String active,
			@WebParam(name = "category", targetNamespace = "") java.lang.String category,
			@WebParam(name = "caused_by", targetNamespace = "") java.lang.String causedBy,
			@WebParam(name = "incident_state", targetNamespace = "") java.lang.String incidentState,
			@WebParam(name = "short_description", targetNamespace = "") java.lang.String shortDescription,
			@WebParam(name = "u_ticket_id", targetNamespace = "") java.lang.String uTicketId,
			@WebParam(mode = WebParam.Mode.OUT, name = "sys_id", targetNamespace = "http://www.service-now.com/u_ilex_integration") javax.xml.ws.Holder<java.lang.String> sysId,
			@WebParam(mode = WebParam.Mode.OUT, name = "table", targetNamespace = "http://www.service-now.com/u_ilex_integration") javax.xml.ws.Holder<java.lang.String> table,
			@WebParam(mode = WebParam.Mode.OUT, name = "display_name", targetNamespace = "http://www.service-now.com/u_ilex_integration") javax.xml.ws.Holder<java.lang.String> displayName,
			@WebParam(mode = WebParam.Mode.OUT, name = "display_value", targetNamespace = "http://www.service-now.com/u_ilex_integration") javax.xml.ws.Holder<java.lang.String> displayValue,
			@WebParam(mode = WebParam.Mode.OUT, name = "status", targetNamespace = "http://www.service-now.com/u_ilex_integration") javax.xml.ws.Holder<java.lang.String> status,
			@WebParam(mode = WebParam.Mode.OUT, name = "status_message", targetNamespace = "http://www.service-now.com/u_ilex_integration") javax.xml.ws.Holder<java.lang.String> statusMessage,
			@WebParam(mode = WebParam.Mode.OUT, name = "error_message", targetNamespace = "http://www.service-now.com/u_ilex_integration") javax.xml.ws.Holder<java.lang.String> errorMessage);

}
