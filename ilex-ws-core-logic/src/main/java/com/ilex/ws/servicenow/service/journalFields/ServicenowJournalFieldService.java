package com.ilex.ws.servicenow.service.journalFields;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(targetNamespace = "")
public interface ServicenowJournalFieldService {

	@WebMethod(action = "http://www.service-now.com/sys_journal_field/getRecords")
	public void getRecords(
			@WebParam(name = "element", targetNamespace = "") java.lang.String element,
			@WebParam(name = "element_id", targetNamespace = "") java.lang.String elementId,
			@WebParam(mode = WebParam.Mode.OUT, name = "getRecordsResult", targetNamespace = "") javax.xml.ws.Holder<java.util.List<com.ilex.ws.servicenow.service.journalFields.GetRecordsResponse.GetRecordsResult>> getRecordsResponse);

}
