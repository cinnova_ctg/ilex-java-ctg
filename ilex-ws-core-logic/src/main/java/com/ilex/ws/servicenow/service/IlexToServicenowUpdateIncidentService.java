package com.ilex.ws.servicenow.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * The Interface IlexToServicenowUpdateIncidentService. Used for Ilex To Service
 * Now Incident Update Interaction.
 */
@WebService(targetNamespace = "http://www.service-now.com/u_integration_update_vtask")
public interface IlexToServicenowUpdateIncidentService {
	@WebMethod(action = "http://www.service-now.com/u_integration_update_vtask/insert")
	public void insert(
			@WebParam(name = "u_state", targetNamespace = "") java.lang.String uState,
			@WebParam(name = "u_ticket_number", targetNamespace = "") java.lang.String uTicketNumber,
			@WebParam(name = "u_vendor_key", targetNamespace = "") java.lang.String uVendorKey,
			@WebParam(name = "u_vendor_ticket_number", targetNamespace = "") java.lang.String uVendorTicketNumber,
			@WebParam(name = "u_work_notes", targetNamespace = "") java.lang.String uWorkNotes,
			@WebParam(mode = WebParam.Mode.OUT, name = "sys_id", targetNamespace = "http://www.service-now.com/u_integration_update_vtask") javax.xml.ws.Holder<java.lang.String> sysId,
			@WebParam(mode = WebParam.Mode.OUT, name = "table", targetNamespace = "http://www.service-now.com/u_integration_update_vtask") javax.xml.ws.Holder<java.lang.String> table,
			@WebParam(mode = WebParam.Mode.OUT, name = "display_name", targetNamespace = "http://www.service-now.com/u_integration_update_vtask") javax.xml.ws.Holder<java.lang.String> displayName,
			@WebParam(mode = WebParam.Mode.OUT, name = "display_value", targetNamespace = "http://www.service-now.com/u_integration_update_vtask") javax.xml.ws.Holder<java.lang.String> displayValue,
			@WebParam(mode = WebParam.Mode.OUT, name = "status", targetNamespace = "http://www.service-now.com/u_integration_update_vtask") javax.xml.ws.Holder<java.lang.String> status,
			@WebParam(mode = WebParam.Mode.OUT, name = "status_message", targetNamespace = "http://www.service-now.com/u_integration_update_vtask") javax.xml.ws.Holder<java.lang.String> statusMessage,
			@WebParam(mode = WebParam.Mode.OUT, name = "error_message", targetNamespace = "http://www.service-now.com/u_integration_update_vtask") javax.xml.ws.Holder<java.lang.String> errorMessage,
			@WebParam(mode = WebParam.Mode.OUT, name = "error_number", targetNamespace = "http://www.service-now.com/u_integration_update_vtask") javax.xml.ws.Holder<java.lang.String> error_number,
			@WebParam(mode = WebParam.Mode.OUT, name = "error_description", targetNamespace = "http://www.service-now.com/u_integration_update_vtask") javax.xml.ws.Holder<java.lang.String> error_description,
			@WebParam(mode = WebParam.Mode.OUT, name = "vTaskNumber", targetNamespace = "http://www.service-now.com/u_integration_update_vtask") javax.xml.ws.Holder<java.lang.String> vTaskNumber,
			@WebParam(mode = WebParam.Mode.OUT, name = "ticketNumber", targetNamespace = "http://www.service-now.com/u_integration_update_vtask") javax.xml.ws.Holder<java.lang.String> ticketNumber);

}
