package com.ilex.ws.servicenow.service.journalFields;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getRecordsResult" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="element" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="element_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="sys_created_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="sys_created_on" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="sys_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getRecordsResult" })
@XmlRootElement(name = "getRecordsResponse")
public class GetRecordsResponse {
	protected List<GetRecordsResponse.GetRecordsResult> getRecordsResult;

	/**
	 * Gets the value of the getRecordsResult property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the getRecordsResult property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getGetRecordsResult().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link GetRecordsResponse.GetRecordsResult }
	 * 
	 * 
	 */
	public List<GetRecordsResponse.GetRecordsResult> getGetRecordsResult() {
		if (getRecordsResult == null) {
			getRecordsResult = new ArrayList<GetRecordsResponse.GetRecordsResult>();
		}
		return this.getRecordsResult;
	}

	/**
	 * Generates a String representation of the contents of this type. This is
	 * an extension method, produced by the 'ts' xjc plugin
	 * 
	 */

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="element" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="element_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="sys_created_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="sys_created_on" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="sys_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "element", "elementId", "name",
			"sysCreatedBy", "sysCreatedOn", "sysId", "value" })
	public static class GetRecordsResult {
		protected String element;
		@XmlElement(name = "element_id")
		protected String elementId;
		protected String name;
		@XmlElement(name = "sys_created_by")
		protected String sysCreatedBy;
		@XmlElement(name = "sys_created_on")
		protected String sysCreatedOn;
		@XmlElement(name = "sys_id")
		protected String sysId;
		protected String value;

		/**
		 * Gets the value of the element property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getElement() {
			return element;
		}

		/**
		 * Sets the value of the element property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setElement(String value) {
			this.element = value;
		}

		/**
		 * Gets the value of the elementId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getElementId() {
			return elementId;
		}

		/**
		 * Sets the value of the elementId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setElementId(String value) {
			this.elementId = value;
		}

		/**
		 * Gets the value of the name property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getName() {
			return name;
		}

		/**
		 * Sets the value of the name property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setName(String value) {
			this.name = value;
		}

		/**
		 * Gets the value of the sysCreatedBy property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSysCreatedBy() {
			return sysCreatedBy;
		}

		/**
		 * Sets the value of the sysCreatedBy property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSysCreatedBy(String value) {
			this.sysCreatedBy = value;
		}

		/**
		 * Gets the value of the sysCreatedOn property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSysCreatedOn() {
			return sysCreatedOn;
		}

		/**
		 * Sets the value of the sysCreatedOn property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSysCreatedOn(String value) {
			this.sysCreatedOn = value;
		}

		/**
		 * Gets the value of the sysId property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getSysId() {
			return sysId;
		}

		/**
		 * Sets the value of the sysId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setSysId(String value) {
			this.sysId = value;
		}

		/**
		 * Gets the value of the value property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getValue() {
			return value;
		}

		/**
		 * Sets the value of the value property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setValue(String value) {
			this.value = value;
		}

	}

}
