package com.ilex.ws.core.logic.impl;

import org.springframework.stereotype.Component;

import com.ilex.ws.core.logic.DeliveryMethodLogic;

@Component
public class DeliveryMethodLogicImpl implements DeliveryMethodLogic {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.DeliveryMethodLogic#getDeliveryMethod(int)
	 */
	@Override
	public String getDeliveryMethod(int deliveryMethod) {
		String deliveryMethodName = null;
		switch (deliveryMethod) {
		case 0:
			deliveryMethodName = "To Be Determined";
			break;
		case 1:
			deliveryMethodName = "Email";
			break;
		case 2:
			deliveryMethodName = "Fax";
			break;
		case 3:
			deliveryMethodName = "Mail";
			break;

		}
		return deliveryMethodName;
	}

}
