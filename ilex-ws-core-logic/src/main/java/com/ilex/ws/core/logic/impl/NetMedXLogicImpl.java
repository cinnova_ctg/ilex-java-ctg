package com.ilex.ws.core.logic.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.fileupload.FileItem;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.MacWorxDetailVO;
import com.ilex.ws.core.bean.NetMedXConsolidatorRespVO;
import com.ilex.ws.core.bean.NetMedXIntegrationResponseVO;
import com.ilex.ws.core.bean.NetMedXIntegrationVO;
import com.ilex.ws.core.dao.MacWorxDAO;
import com.ilex.ws.core.dao.NetMedXDAO;
import com.ilex.ws.core.dto.NetMedXConsolidatorRequestDTO;
import com.ilex.ws.core.dto.NetMedXConsolidatorRespDTO;
import com.ilex.ws.core.dto.NetMedXIntegrationDTO;
import com.ilex.ws.core.dto.NetMedXIntegrationResponseDTO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.logic.NetMedXLogic;
import com.ilex.ws.core.util.MessageKeyConstant;
import com.mind.bean.docm.Document;
import com.mind.bean.docm.Field;
import com.mind.bean.docm.MetaData;

// TODO: Auto-generated Javadoc
/**
 * The Class NetMedXLogicImpl.
 */
@Component
public class NetMedXLogicImpl implements NetMedXLogic {

	/** The mac worx dao. */
	@Resource
	MacWorxDAO macWorxDAO;

	/** The net med xdao. */
	@Resource
	NetMedXDAO netMedXDAO;

	/** The image max size. */
	@Value("#{ilexProperties['mobile.upload.image.max.size']}")
	private String imageMaxSize;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.NetMedXLogic#netMedXIntergration(com.ilex.ws.core
	 * .dto.NetMedXIntegrationDTO, org.apache.commons.fileupload.FileItem)
	 */
	@Override
	public NetMedXIntegrationResponseDTO netMedXIntergration(
			NetMedXIntegrationDTO netMedXIntegrationDTO, FileItem fileItem) {

		NetMedXIntegrationVO netMedXIntegrationVO = new NetMedXIntegrationVO();
		BeanUtils.copyProperties(netMedXIntegrationDTO, netMedXIntegrationVO);

		NetMedXIntegrationResponseVO netMedXIntegrationResponseVO = new NetMedXIntegrationResponseVO();

		netMedXIntegrationResponseVO = netMedXDAO
				.createJobForNetMedX(netMedXIntegrationVO);

		NetMedXIntegrationResponseDTO netMedXIntegrationResponseDTO = new NetMedXIntegrationResponseDTO();
		BeanUtils.copyProperties(netMedXIntegrationResponseVO,
				netMedXIntegrationResponseDTO);

		if (netMedXIntegrationResponseVO.getApiAccess() != 0) {
			if (fileItem != null && fileItem.getSize() > 0) {
				if (fileItem.getSize() > Long.valueOf(imageMaxSize)) {
					throw new IlexBusinessException(
							MessageKeyConstant.IMAGE_SIZE_NOT_VALID);
				}
				Document doc = new Document();
				doc.setFileBytes(fileItem.get());
				doc.setFileName(fileItem.getName());
				MetaData metaData = getmetaData(fileItem,
						netMedXIntegrationResponseVO);
				doc.setMetaData(metaData);

				int docId = macWorxDAO.uploadFile(doc);
				if (docId == 0) {
					netMedXIntegrationResponseDTO.setAttachmentStatus(0);
				} else {

					netMedXDAO.insertDocEntryToPoDocumentNetMedX(docId,
							netMedXIntegrationDTO.getContact_person(),
							netMedXIntegrationResponseDTO.getJobId());
					netMedXIntegrationResponseDTO.setAttachmentStatus(1);

				}
			}
		}
		return netMedXIntegrationResponseDTO;
	}

	@Override
	public String netmedxUserAuth(String userName, String pass, int prId) {
		NetMedXIntegrationResponseVO netmedxintegrationresponsevo = netMedXDAO
				.netmedxUserAuth(userName, pass, prId);

		return (netmedxintegrationresponsevo.getApiAccess() != 1) ? "InvalidAccess"
				: "GrantAccess";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.NetMedXLogic#getNetMedXJobIntergration(com.ilex
	 * .ws.core.dto.NetMedXConsolidatorRequestDTO)
	 */
	@Override
	public List<NetMedXConsolidatorRespDTO> getNetMedXJobIntergration(
			NetMedXConsolidatorRequestDTO netMedXConsolidatorRequestDTO) {

		List<NetMedXConsolidatorRespDTO> netMedXConsolidatorRespDTOsList = new ArrayList<NetMedXConsolidatorRespDTO>();
		// NetMedXIntegrationResponseVO netMedXIntegrationResponseVO = new
		// NetMedXIntegrationResponseVO();
		//
		// netMedXIntegrationResponseVO = netMedXDAO.netmedxUserAuth(
		// netMedXConsolidatorRequestDTO.getUserName(),
		// netMedXConsolidatorRequestDTO.getUserPass(),
		// Integer.parseInt(netMedXConsolidatorRequestDTO.getProjectId()));
		// if (netMedXIntegrationResponseVO.getApiAccess() != 0) {

		String status_filter = "";
		String archive_filter = "";
		String site_filter = "";

		String orderPart = getOrderByPart(
				netMedXConsolidatorRequestDTO.getSortBy(),
				netMedXConsolidatorRequestDTO.getSortOrder());

		if (!netMedXConsolidatorRequestDTO.getStatus().equals("")) {
			String il_ji_status = "('"
					+ netMedXConsolidatorRequestDTO.getStatus()
					+ "', '"
					+ ((netMedXConsolidatorRequestDTO.getStatus() == "Scheduled") ? "Scheduled - Overdue"
							: ((netMedXConsolidatorRequestDTO.getStatus() == "Invoiced") ? "Closed"
									: "-1")) + "')";
			status_filter = " AND il_ji_status IN " + il_ji_status;
		}

		if (!netMedXConsolidatorRequestDTO.getArchivedPeriod().equals("")) {
			archive_filter = " AND il_ji_date_scheduled > DATE_SUB(NOW(), INTERVAL "
					+ netMedXConsolidatorRequestDTO.getArchivedPeriod()
					+ " MONTH) ";
		}
		if (!netMedXConsolidatorRequestDTO.getScheduledDate().equals("")) {
			archive_filter += " AND il_ji_date_scheduled LIKE '%"
					+ netMedXConsolidatorRequestDTO.getScheduledDate() + "%' ";
		}
		site_filter += (!netMedXConsolidatorRequestDTO.getSiteNumber().equals(
				"")) ? " AND il_job_integration.il_ji_lm_si_number LIKE '%"
				+ netMedXConsolidatorRequestDTO.getSiteNumber() + "%' " : "";
		site_filter += (!netMedXConsolidatorRequestDTO.getSiteNumber().equals(
				"")) ? " AND il_job_integration.il_ji_lm_si_number LIKE '%"
				+ netMedXConsolidatorRequestDTO.getSiteNumber() + "%' " : "";

		String whereClause = archive_filter + status_filter + site_filter;
		String limit = " LIMIT "
				+ netMedXConsolidatorRequestDTO.getStartLimit() + ", "
				+ netMedXConsolidatorRequestDTO.getEndLimit();

		List<NetMedXConsolidatorRespVO> netMedXConsolidatorRespVOsList = netMedXDAO
				.getNetMedXJobIntergration(
						netMedXConsolidatorRequestDTO.getProjectId(),
						whereClause, orderPart, limit);

		if (netMedXConsolidatorRespVOsList.size() > 0) {
			NetMedXConsolidatorRespDTO consolidatorRespDTO = null;
			for (int i = 0; i < netMedXConsolidatorRespVOsList.size(); i++) {
				NetMedXConsolidatorRespVO consolidatorRespVO = netMedXConsolidatorRespVOsList
						.get(i);
				consolidatorRespDTO = new NetMedXConsolidatorRespDTO();
				BeanUtils.copyProperties(consolidatorRespVO,
						consolidatorRespDTO);
				netMedXConsolidatorRespDTOsList.add(consolidatorRespDTO);
			}
		}
		// }
		return netMedXConsolidatorRespDTOsList;

	}

	/**
	 * Gets the meta data.
	 * 
	 * @param file
	 *            the file
	 * @param netMedXIntegrationResponseVO
	 *            the net med x integration response vo
	 * @return the meta data
	 */
	public MetaData getmetaData(FileItem file,
			NetMedXIntegrationResponseVO netMedXIntegrationResponseVO) {
		MacWorxDetailVO macWorxDetailVO = macWorxDAO.getMetaDataForMacWorx(
				String.valueOf(netMedXIntegrationResponseVO.getJobId()),
				String.valueOf(netMedXIntegrationResponseVO.getPoId()));
		String msaName = macWorxDetailVO.getMsaName();
		String appendixName = macWorxDetailVO.getMsaId();
		String jobName = macWorxDetailVO.getJobName();
		String msaId = macWorxDetailVO.getMsaId();
		String appendixId = macWorxDetailVO.getAppendixId();
		String jobid = String.valueOf(netMedXIntegrationResponseVO.getJobId());
		String poId = String.valueOf(netMedXIntegrationResponseVO.getPoId());
		String partnerName = macWorxDetailVO.getPartnerName();
		String jobOwnerName = macWorxDAO
				.getJobOwnerName(netMedXIntegrationResponseVO.getJobId());

		Map<String, String> map = new HashMap<String, String>();
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = formatter.format(currentDate.getTime());
		Calendar archiveDay = Calendar.getInstance();
		archiveDay.add(Calendar.YEAR, 1);

		String archiveDate = formatter.format(archiveDay.getTime());
		if (file.getName().lastIndexOf(".") != -1) {
			map.put(MACWorXFieldEnum.TITLE.getKey(),
					file.getName()
							.substring(0, file.getName().lastIndexOf(".")));
		}
		map.put(MACWorXFieldEnum.THROUGH_PO.getKey(), "PO");
		map.put(MACWorXFieldEnum.DOCUMENT_DATE.getKey(), dateNow);
		map.put(MACWorXFieldEnum.TYPE.getKey(), "Engineering Support");
		map.put(MACWorXFieldEnum.STATUS.getKey(), "Approved");
		map.put(MACWorXFieldEnum.AUTHOR.getKey(), "");
		map.put(MACWorXFieldEnum.CATEGORY.getKey(), "Contract");
		map.put(MACWorXFieldEnum.SOURCE.getKey(), "Ilex");
		map.put(MACWorXFieldEnum.ADD_DATE.getKey(), dateNow);
		map.put(MACWorXFieldEnum.ADD_USER.getKey(), jobOwnerName);
		map.put(MACWorXFieldEnum.ADD_METHOD.getKey(), "Explicit");
		map.put(MACWorXFieldEnum.CHANGE_CONTROL_TYPE.getKey(), "0");
		map.put(MACWorXFieldEnum.PREVOIUS_DOCUMENT_REFERENCE.getKey(), "");
		map.put(MACWorXFieldEnum.VIEWABLE_ON_WEB.getKey(), "1");
		map.put(MACWorXFieldEnum.ARCHIVE_DATE.getKey(), archiveDate.toString());
		map.put(MACWorXFieldEnum.BASE_DOCUMENT.getKey(), "");
		map.put(MACWorXFieldEnum.APPLICATION.getKey(), "FileType");
		map.put(MACWorXFieldEnum.Size.getKey(), String.valueOf(file.getSize()));
		if (file.getName().lastIndexOf(".") != -1) {
			map.put(MACWorXFieldEnum.MIME.getKey(),
					file.getName().substring(
							file.getName().lastIndexOf(".") + 1));
		}

		map.put(MACWorXFieldEnum.MSA_ID.getKey(), msaId);
		map.put(MACWorXFieldEnum.APPENDIX_ID.getKey(), appendixId);
		map.put(MACWorXFieldEnum.JOB_ID.getKey(), jobid);
		map.put(MACWorXFieldEnum.PO_ID.getKey(), poId);
		map.put(MACWorXFieldEnum.WO_ID.getKey(), poId);
		map.put(MACWorXFieldEnum.CHANGE_CONTROL_ID.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_START_DATE.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_END_DATE.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_CRITICALITY.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_STATUS.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_REQUESTOR.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_ARR_OPTIONS.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_MSP.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_HELP_DESK.getKey(), "");
		map.put(MACWorXFieldEnum.LINK_TO_OTHER_DOC_ID.getKey(), "");
		map.put(MACWorXFieldEnum.MSA_NAME.getKey(), msaName);
		map.put(MACWorXFieldEnum.APPENDIX_NAME.getKey(), appendixName);
		map.put(MACWorXFieldEnum.JOB_NAME.getKey(), jobName);
		map.put(MACWorXFieldEnum.PARTNER_NAME.getKey(), partnerName);
		map.put(MACWorXFieldEnum.WO_PARTNER_ID.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_NEDX_ID_NAME.getKey(), "");
		map.put(MACWorXFieldEnum.COMMENT.getKey(), "");
		MetaData metaData = new MetaData();
		Field[] fieldList = getFieldList(map);
		metaData.setFields(fieldList);
		return metaData;
	}

	/**
	 * Gets the field list.
	 * 
	 * @param map
	 *            the map
	 * @return the field list
	 */
	public Field[] getFieldList(Map<String, String> map) {
		Field[] fieldList = new Field[map.size()];
		int i = 0;
		for (Map.Entry<String, String> entry : map.entrySet()) {

			Field field = new Field();
			field.setName(entry.getKey());
			field.setValue(entry.getValue());
			fieldList[i] = field;
			i++;
		}
		return fieldList;

	}

	/**
	 * Gets the order by part.
	 * 
	 * @param sortBy
	 *            the sort by
	 * @param sortOrder
	 *            the sort order
	 * @return the order by part
	 */
	private String getOrderByPart(String sortBy, String sortOrder) {
		String field = "";

		switch (sortBy) {
		case "st":
			field = "il_ji_lm_si_number";
			break;
		case "ct":
			field = "il_ji_lm_si_city";
			break;
		case "sta":
			field = "il_ji_lm_si_state";
			break;
		case "sts":
			field = "il_ji_status";
			break;
		case "inv":
			field = "il_ji_lm_js_invoice_no";
			break;
		case "invp":
			field = "CAST(REPLACE(REPLACE(il_ji_lx_ce_invoice_price, '+', ''), ',', '') AS DECIMAL)";
			break;
		case "sch":
			field = "il_ji_date_scheduled";
			break;
		case "on":
			field = "il_ji_date_onsite";
			break;
		case "off":
			field = "il_ji_date_offsite";
			break;
		case "cmp":
			field = "il_ji_date_complete";
			break;
		case "invd":
			field = "il_ji_date_invoiced";
			break;
		default:
			field = "il_ji_date_scheduled";
			break;
		}

		return " ORDER BY " + field + " " + sortOrder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.NetMedXLogic#getJobCount()
	 */
	@Override
	public String getJobCount(
			NetMedXConsolidatorRequestDTO netMedXConsolidatorRequestDTO) {

		String status_filter = "";
		String archive_filter = "";
		String site_filter = "";

		String orderPart = getOrderByPart(
				netMedXConsolidatorRequestDTO.getSortBy(),
				netMedXConsolidatorRequestDTO.getSortOrder());

		if (!netMedXConsolidatorRequestDTO.getStatus().equals("")) {
			String il_ji_status = "('"
					+ netMedXConsolidatorRequestDTO.getStatus()
					+ "', '"
					+ ((netMedXConsolidatorRequestDTO.getStatus() == "Scheduled") ? "Scheduled - Overdue"
							: ((netMedXConsolidatorRequestDTO.getStatus() == "Invoiced") ? "Closed"
									: "-1")) + "')";
			status_filter = " AND il_ji_status IN " + il_ji_status;
		}

		if (!netMedXConsolidatorRequestDTO.getArchivedPeriod().equals("")) {
			archive_filter = " AND il_ji_date_scheduled > DATE_SUB(NOW(), INTERVAL "
					+ netMedXConsolidatorRequestDTO.getArchivedPeriod()
					+ " MONTH) ";
		}
		if (!netMedXConsolidatorRequestDTO.getScheduledDate().equals("")) {
			archive_filter += " AND il_ji_date_scheduled LIKE '%"
					+ netMedXConsolidatorRequestDTO.getScheduledDate() + "%' ";
		}
		site_filter += (!netMedXConsolidatorRequestDTO.getSiteNumber().equals(
				"")) ? " AND il_job_integration.il_ji_lm_si_number LIKE '%"
				+ netMedXConsolidatorRequestDTO.getSiteNumber() + "%' " : "";
		site_filter += (!netMedXConsolidatorRequestDTO.getSiteNumber().equals(
				"")) ? " AND il_job_integration.il_ji_lm_si_number LIKE '%"
				+ netMedXConsolidatorRequestDTO.getSiteNumber() + "%' " : "";

		String whereClause = archive_filter + status_filter + site_filter;

		return netMedXDAO.getJobCountDAO(
				netMedXConsolidatorRequestDTO.getProjectId(), whereClause,
				orderPart);
	}

}
