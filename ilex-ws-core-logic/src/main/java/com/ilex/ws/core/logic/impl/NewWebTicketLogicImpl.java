package com.ilex.ws.core.logic.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.NewDispatchVO;
import com.ilex.ws.core.bean.TicketScheduleInfo;
import com.ilex.ws.core.dao.NewWebTicketDAO;
import com.ilex.ws.core.dto.LabelValueDTO;
import com.ilex.ws.core.dto.NewDispatchDTO;
import com.ilex.ws.core.dto.NewWebTicketDTO;
import com.ilex.ws.core.dto.SiteDetailDTO;
import com.ilex.ws.core.dto.ViewTicketSummaryDTO;
import com.ilex.ws.core.logic.NewWebTicketLogic;
import com.ilex.ws.core.logic.SiteSearchLogic;
import com.ilex.ws.core.logic.util.NewDispatchOrganizer;
import com.ilex.ws.core.logic.util.SiteDetailOrganizer;
import com.ilex.ws.core.util.IlexDateUtils;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;
import com.mind.bean.newjobdb.ResourceLevel;
import com.mind.bean.pm.SiteBean;
import com.mind.bean.prm.NewWebTicketBean;
import com.mind.bean.prm.ViewTicketSummaryBean;
import com.mind.common.LabelValue;

@Component
public class NewWebTicketLogicImpl implements NewWebTicketLogic {

	@Resource
	NewWebTicketDAO newWebTicketDAO;

	@Resource
	SiteSearchLogic siteSearchLogic;

	@Override
	public List<NewWebTicketDTO> getWebTicketList() {
		List<NewWebTicketBean> newWebTickets = newWebTicketDAO
				.getWebTicketList();

		NullAwareBeanUtilsBean nullAwareBeanUtilsBean = new NullAwareBeanUtilsBean();
		List<NewWebTicketDTO> newWebTicketDTOs = nullAwareBeanUtilsBean
				.copyList(NewWebTicketDTO.class, newWebTickets);
		return newWebTicketDTOs;
	}

	@Override
	public ViewTicketSummaryDTO getWebTicketSummary(String ticketNumber) {
		ViewTicketSummaryBean viewTicketSummaryBean = newWebTicketDAO
				.getWebTicketSummary(ticketNumber);
		ViewTicketSummaryDTO viewTicketSummaryDTO = new ViewTicketSummaryDTO();
		BeanUtils.copyProperties(viewTicketSummaryBean, viewTicketSummaryDTO);
		return viewTicketSummaryDTO;

	}

	@Override
	public List<LabelValueDTO> getRequestors(long msaId) {
		List<LabelValueDTO> requestorList = new ArrayList<LabelValueDTO>();

		List<LabelValue> requestors = newWebTicketDAO.getRequestors(msaId);
		for (LabelValue labelValue : requestors) {
			labelValue.setValue(getRequesterName(labelValue.getValue()));
		}
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		requestorList = awareBeanUtilsBean.copyList(LabelValueDTO.class,
				requestors);
		return requestorList;
	}

	private static String getRequesterName(String str) {
		if (str.indexOf("~") > -1)
			return str.substring(0, str.indexOf("~"));
		else
			return str;
	}

	@Override
	public NewDispatchDTO getWebTicketInfo(Integer msaId, Integer appendixId,
			Integer jobId, Integer ticketId) {

		NewDispatchDTO newDispatchDTO = new NewDispatchDTO();

		List<LabelValue> requestors = newWebTicketDAO.getRequestors(msaId
				.longValue());
		List<LabelValue> criticalityList = newWebTicketDAO
				.getCriticalities(msaId.longValue());
		List<LabelValue> requestTypes = newWebTicketDAO.getRequestTypes();
		List<LabelValue> problemCategories = newWebTicketDAO
				.getProblemCategories();

		NewDispatchVO newDispatchVO = newWebTicketDAO.getTicketInfo(ticketId
				.longValue());
		BeanUtils.copyProperties(newDispatchVO, newDispatchDTO);

		SiteDetailDTO siteDetails = siteSearchLogic.getSiteDetails(String
				.valueOf(jobId));
		newDispatchDTO.setSiteDetails(siteDetails);

		newDispatchDTO.setTicketRules(newWebTicketDAO.getRules(Long
				.valueOf(appendixId)));

		newDispatchDTO.setRequestor(newDispatchDTO.getRequestor() + "~"
				+ newDispatchDTO.getRequestorEmail());

		NewDispatchOrganizer.setTicketScheduleInfo(newDispatchVO,
				newDispatchDTO);
		ViewTicketSummaryDTO viewTicketSummaryDTO = getWebTicketSummary(newDispatchDTO
				.getTicketNumber());

		String customerSpecificFields = viewTicketSummaryDTO
				.getCust_specific_fields();
		if (StringUtils.isNotBlank(customerSpecificFields)) {
			customerSpecificFields = customerSpecificFields.replaceAll(
					"(\r\n|\n)", "<br />");
		}
		String requestSummary = viewTicketSummaryDTO.getOther_email_info();
		if (StringUtils.isNotBlank(requestSummary)) {
			requestSummary = requestSummary.replaceAll("(\r\n|\n)", "<br />");
		}
		newDispatchDTO.setCustomerSpecificDetails(customerSpecificFields);
		newDispatchDTO.setRequestSummary(requestSummary);
		newDispatchDTO.setArrivalDate(viewTicketSummaryDTO.getArrivaldate());

		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		newDispatchDTO.setRequestors(awareBeanUtilsBean.copyList(
				LabelValueDTO.class, requestors));
		newDispatchDTO.setCriticalities(awareBeanUtilsBean.copyList(
				LabelValueDTO.class, criticalityList));
		newDispatchDTO.setRequestTypes(awareBeanUtilsBean.copyList(
				LabelValueDTO.class, requestTypes));
		newDispatchDTO.setProblemCategories(awareBeanUtilsBean.copyList(
				LabelValueDTO.class, problemCategories));

		newDispatchDTO.setRequestedHourList(awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getHours()));
		newDispatchDTO.setRequestedMinuteList(awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getMinutes()));
		newDispatchDTO.setPreferredHourList(awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getHours()));
		newDispatchDTO.setPreferredMinuteList(awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getMinutes()));
		newDispatchDTO.setWindowFromHourList(awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getHours()));
		newDispatchDTO.setWindowFromMinuteList(awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getMinutes()));
		newDispatchDTO.setWindowToHourList(awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getHours()));
		newDispatchDTO.setWindowToMinuteList(awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getMinutes()));

		return newDispatchDTO;

	}

	@Override
	public String saveWebTicketInfo(NewDispatchDTO newDispatchDTO,
			Long loginUserId) {
		NewDispatchVO newDispatchVO = new NewDispatchVO();
		BeanUtils.copyProperties(newDispatchDTO, newDispatchVO);

		newDispatchVO.setSiteListId(newDispatchDTO.getSiteDetails()
				.getSiteListId());
		TicketScheduleInfo scheduleInfo = NewDispatchOrganizer
				.getTicketScheduleInfo(newDispatchDTO);
		newDispatchVO.setTicketScheduleInfo(scheduleInfo);

		SiteBean siteDetails = SiteDetailOrganizer
				.copySiteDetails(newDispatchDTO.getSiteDetails());

		try {
			newWebTicketDAO.updateTicket(newDispatchVO, loginUserId);

			siteSearchLogic
					.updateSite(siteDetails, String.valueOf(loginUserId));
			newWebTicketDAO.updateTicketJobOwner(newDispatchVO, loginUserId);
		} catch (Exception ex) {
			return ex.getMessage();
		}
		return "success";
	}

	@Override
	public List<ResourceLevel> getResourceList(Long appendixId,
			String criticalityId) {
		Map<String, Object> map = new HashMap<String, Object>();

		return newWebTicketDAO.getResources(map, appendixId, criticalityId,
				true);
	}

	public void assignNewSite(String siteListId, String siteId, String jobId,
			String userId) {

	}

	@Override
	public List<LabelValueDTO> getStates() {
		List<LabelValue> states = newWebTicketDAO.getStates();
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		return awareBeanUtilsBean.copyList(LabelValueDTO.class, states);
	}
}
