package com.ilex.ws.core.logic.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.InvBTMClientVO;
import com.ilex.ws.core.bean.InvBTMContactVO;
import com.ilex.ws.core.dao.InvBTMClientDAO;
import com.ilex.ws.core.dao.InvBTMContactDAO;
import com.ilex.ws.core.dto.MasterBTMDTO;
import com.ilex.ws.core.dto.MsaClientDTO;
import com.ilex.ws.core.dto.OrgDivisionDTO;
import com.ilex.ws.core.dto.PointOfContactDTO;
import com.ilex.ws.core.logic.InvBTMLogic;

@Component
public class InvBTMLogicImpl implements InvBTMLogic {
	@Resource
	InvBTMContactDAO invBTMContactDAO;
	@Resource
	InvBTMClientDAO invBTMClientDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvBTMLogic#insertBTMRecord(com.ilex.ws.core.dto
	 * .MasterBTMDTO)
	 */
	@Override
	public void insertBTMRecord(MasterBTMDTO masterBTMDTO) {

		InvBTMClientVO invBTMClientVO = new InvBTMClientVO();
		InvBTMContactVO invBTMContactVO = new InvBTMContactVO();
		setInvBTMClientVO(invBTMClientVO, masterBTMDTO);
		setInvBTMContactVO(invBTMContactVO, masterBTMDTO);
		invBTMContactVO.setMbtId(invBTMClientDAO
				.insertBTMClientDetails(invBTMClientVO));
		if (!invBTMClientVO.isContactExist()) {
			invBTMContactDAO.insertBTMContactDetails(invBTMContactVO);
		}
		;

	}

	/**
	 * Sets the invBTMClientVO.
	 * 
	 * @param invBTMClientVO
	 *            the invBtmClientVO
	 * @param masterBTMDTO
	 *            the master btmdto to represent an active client
	 *            invBTMClientVO.setStatus(true); If There is no billing contact
	 *            defined invBTMClientVO.setContact(true); IF Delivery method is
	 *            to be determined,invBTMClientVO.setDelivery(true);
	 */
	private static void setInvBTMClientVO(InvBTMClientVO invBTMClientVO,
			MasterBTMDTO masterBTMDTO) {
		OrgDivisionDTO orgDivisionDTO = new OrgDivisionDTO();
		MsaClientDTO msaClientDTO = new MsaClientDTO();
		PointOfContactDTO pointOfContactDTO = new PointOfContactDTO();
		pointOfContactDTO = masterBTMDTO.getPoc();
		msaClientDTO = masterBTMDTO.getClient();
		orgDivisionDTO = masterBTMDTO.getDivision();
		invBTMClientVO.setClientName(msaClientDTO.getClientName());
		invBTMClientVO.setAddress1(orgDivisionDTO.getAddress1());
		invBTMClientVO.setAddress2(orgDivisionDTO.getAddress2());
		invBTMClientVO.setCity(orgDivisionDTO.getCity());
		invBTMClientVO.setState(orgDivisionDTO.getState());
		invBTMClientVO.setZipCode(orgDivisionDTO.getZipCode());
		invBTMClientVO.setCountry(orgDivisionDTO.getCountry());
		invBTMClientVO.setClientId(Integer.valueOf(orgDivisionDTO
				.getDivisionId()));
		invBTMClientVO.setMmId(msaClientDTO.getMmId());
		invBTMClientVO.setStatus(true);
		invBTMClientVO.setIntegrated(false);
		if (pointOfContactDTO.getPocId().equals("0")) {
			invBTMClientVO.setContactExist(true);
		}
		if (masterBTMDTO.getDeliveryMethod() == 0) {
			invBTMClientVO.setDeliveryExist(true);
		}
		invBTMClientVO.setDeliveryMethod(masterBTMDTO.getDeliveryMethod());

	}

	/**
	 * Sets the invBtmContactVO.
	 * 
	 * @param invBTMContactVO
	 *            the invbtmcontactvo
	 * @param masterBTMDTO
	 *            the master btmdto to represent active client
	 *            :invBTMContactVO.setStatus(true);
	 */
	private static void setInvBTMContactVO(InvBTMContactVO invBTMContactVO,
			MasterBTMDTO masterBTMDTO) {
		PointOfContactDTO pointOfContactDTO = new PointOfContactDTO();
		pointOfContactDTO = masterBTMDTO.getPoc();
		invBTMContactVO.setCell(pointOfContactDTO.getCellNo());
		invBTMContactVO.setEmail(pointOfContactDTO.getEmailID());
		invBTMContactVO.setFax(pointOfContactDTO.getFaxNo());
		invBTMContactVO.setFirstName(pointOfContactDTO.getFirstName());
		invBTMContactVO.setLastName(pointOfContactDTO.getLastName());
		invBTMContactVO.setPhone(pointOfContactDTO.getPhoneNo());
		invBTMContactVO.setPocId(Integer.valueOf(pointOfContactDTO.getPocId()));
		invBTMContactVO.setStatus(true);

	}

}
