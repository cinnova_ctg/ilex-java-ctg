package com.ilex.ws.core.logic.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.InvCountryListVO;
import com.ilex.ws.core.dao.InvMasterCountryDAO;
import com.ilex.ws.core.dto.InvCountryListDTO;
import com.ilex.ws.core.logic.InvMasterCountryLogic;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;

@Component
public class InvMasterCountryLogicImpl implements InvMasterCountryLogic {
	@Resource
	InvMasterCountryDAO invMasterCountryDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.InvMasterCountryLogic#getCountryList()
	 */
	@Override
	public List<InvCountryListDTO> getCountryList() {
		List<InvCountryListVO> invCountryList = invMasterCountryDAO
				.getCountryList();
		NullAwareBeanUtilsBean nullAwareBeanUtilsBean = new NullAwareBeanUtilsBean();
		List<InvCountryListDTO> invCountryListDTO = nullAwareBeanUtilsBean
				.copyList(InvCountryListDTO.class, invCountryList);
		return invCountryListDTO;

	}
}
