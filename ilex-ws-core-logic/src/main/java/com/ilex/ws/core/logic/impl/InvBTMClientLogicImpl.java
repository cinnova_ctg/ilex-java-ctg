package com.ilex.ws.core.logic.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.InvBTMClientVO;
import com.ilex.ws.core.dao.InvBTMClientDAO;
import com.ilex.ws.core.dao.util.Page;
import com.ilex.ws.core.dto.ClientListDTO;
import com.ilex.ws.core.dto.InvBTMClientDTO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.logic.InvBTMClientLogic;
import com.ilex.ws.core.util.MessageKeyConstant;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;

@Component
public class InvBTMClientLogicImpl implements InvBTMClientLogic {
	@Resource
	InvBTMClientDAO invBTMClientDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.InvBTMClientLogic#getClientList()
	 */
	@Override
	public List<InvBTMClientDTO> getClientList() {

		List<InvBTMClientVO> invBTMClientVOList = invBTMClientDAO
				.getClientList();
		NullAwareBeanUtilsBean nullAwareBeanUtilsBean = new NullAwareBeanUtilsBean();
		List<InvBTMClientDTO> invBTMClientDTOList = nullAwareBeanUtilsBean
				.copyList(InvBTMClientDTO.class, invBTMClientVOList);
		return invBTMClientDTOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvBTMClientLogic#getClientDetails(java.lang.String
	 * )
	 */
	@Override
	public InvBTMClientDTO getClientDetails(String clientId) {
		InvBTMClientVO invBTMClientVO = invBTMClientDAO
				.getClientDetails(clientId);
		InvBTMClientDTO invBTMClientDTO = new InvBTMClientDTO();

		BeanUtils.copyProperties(invBTMClientVO, invBTMClientDTO);

		return invBTMClientDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvBTMClientLogic#updateBTMClientDetails(com.ilex
	 * .ws.core.dto.InvBTMClientDTO)
	 */
	@Override
	public void updateBTMClientDetails(InvBTMClientDTO argbtmClientDTO) {
		InvBTMClientVO invBTMClientVO = new InvBTMClientVO();
		BeanUtils.copyProperties(argbtmClientDTO, invBTMClientVO);
		if (invBTMClientVO.getDeliveryMethod() == 0) {
			invBTMClientVO.setDeliveryExist(true);
		} else {
			invBTMClientVO.setDeliveryExist(false);
		}
		invBTMClientDAO.updateBTMClientDetails(invBTMClientVO);

	}

	@Value("#{ilexProperties['clientlist.page.size']}")
	private int pageSize;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvBTMClientLogic#getClientListByName(java.lang
	 * .String, int)
	 */
	@Override
	public ClientListDTO getClientListByName(String searchKey, int pageIndex) {
		ClientListDTO clientListDTO = new ClientListDTO();
		List<InvBTMClientDTO> invBTMClientDTOList = new ArrayList<InvBTMClientDTO>();
		Page<InvBTMClientVO> invBTMClientVOs = invBTMClientDAO.getClientList(
				searchKey, pageIndex, pageSize);
		List<InvBTMClientVO> invBTMClientVOList = invBTMClientVOs
				.getPageItems();

		if (invBTMClientVOList == null || invBTMClientVOList.size() == 0) {
			throw new IlexBusinessException(MessageKeyConstant.RECORD_NOT_FOUND);
		}
		NullAwareBeanUtilsBean utilsBean = new NullAwareBeanUtilsBean();
		invBTMClientDTOList = utilsBean.copyList(InvBTMClientDTO.class,
				invBTMClientVOList);
		clientListDTO.setInvBTMClientDTOList(invBTMClientDTOList);
		clientListDTO.setPageCount(invBTMClientVOs.getPagesAvailable());
		clientListDTO.setPageIndex(invBTMClientVOs.getPageNumber());

		return clientListDTO;

	}

}
