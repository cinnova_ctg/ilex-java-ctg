package com.ilex.ws.core.logic.impl;

public enum MACWorXFieldEnum {

	TITLE("Title"), DOCUMENT_DATE("DocumentDate"), TYPE("Type"), STATUS(
			"Status"), AUTHOR("Author"), CATEGORY("Category"), SOURCE("Source"), ADD_DATE(
			"AddDate"), ADD_USER("AddUser"), ADD_METHOD("AddMethod"), CHANGE_CONTROL_TYPE(
			"ChangeControlType"), PREVOIUS_DOCUMENT_REFERENCE(
			"PreviousDocumentReference"), VIEWABLE_ON_WEB("ViewableOnWeb"), ARCHIVE_DATE(
			"ArchiveDate"), ARCHIVE_FILE_NAME("ArchiveFileName"), BASE_DOCUMENT(
			"BaseDocument"), APPLICATION("Application"), Size("Size"), MIME(
			"MIME"), MSA_ID("MSA_ID"), APPENDIX_ID("Appendix_ID"), JOB_ID(
			"Job_ID"), ACTIVITY_ID("Activity_ID"), PO_ID("PO_ID"), INVOICE_ID(
			"Invoice_ID"), CHANGE_CONTROL_ID("ChangeControl_ID"), WO_ID("WO_ID"), DTL_SMRY_NMX_ID(
			"Dtl_Smry_NMx_Id"), DTL_SMRY_NMX_FLT_START_DATE(
			"Dtl_Smry_NMx_Flt_StartDate"), DTL_SMRY_NMX_FLT_END_DATE(
			"Dtl_Smry_NMx_Flt_EndDate"), DTL_SMRY_NMX_FLT_CRITICALITY(
			"Dtl_Smry_NMx_Flt_Criticality"), DTL_SMRY_NMX_FLT_STATUS(
			"Dtl_Smry_NMx_Flt_Status"), DTL_SMRY_NMX_FLT_REQUESTOR(
			"Dtl_Smry_NMx_Flt_Requestor"), DTL_SMRY_NMX_FLT_ARR_OPTIONS(
			"Dtl_Smry_NMx_Flt_Arr_Options"), DTL_SMRY_NMX_FLT_MSP(
			"Dtl_Smry_NMx_Flt_Msp"), DTL_SMRY_NMX_FLT_HELP_DESK(
			"Dtl_Smry_NMx_Flt_HelpDesk"), LINK_TO_OTHER_DOC_ID(
			"LinkToOtherDocId"), MSA_NAME("MSA_Name"), APPENDIX_NAME(
			"Appendix_Name"), JOB_NAME("Job_Name"), PARTNER_NAME("Partner_Name"), WO_PARTNER_ID(
			"WO_Partner_ID"), DTL_NEDX_ID_NAME("Dtl_Nedx_ID_Name"), COMMENT(
			"comment"), THROUGH_PO("throughMPO");

	MACWorXFieldEnum(String key) {
		this.key = key;
	}

	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
