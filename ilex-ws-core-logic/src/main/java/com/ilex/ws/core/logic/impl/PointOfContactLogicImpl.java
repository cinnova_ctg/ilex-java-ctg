package com.ilex.ws.core.logic.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.OrgDivisionVO;
import com.ilex.ws.core.bean.PointOfContactVO;
import com.ilex.ws.core.dao.PointOfContactDAO;
import com.ilex.ws.core.dto.OrgDivisionDTO;
import com.ilex.ws.core.dto.PointOfContactDTO;
import com.ilex.ws.core.logic.PointOfContactLogic;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;

@Component
public class PointOfContactLogicImpl implements PointOfContactLogic {

	@Resource
	PointOfContactDAO pointOfContactDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.PointOfContactLogic#getContactList(com.ilex.ws
	 * .core.dto.OrgDivisionDTO)
	 */
	@Override
	public List<PointOfContactDTO> getContactList(OrgDivisionDTO argDivision) {

		List<PointOfContactDTO> pocDTOList = new ArrayList<PointOfContactDTO>();
		OrgDivisionVO orgDivisionVO = new OrgDivisionVO();
		BeanUtils.copyProperties(argDivision, orgDivisionVO);
		List<PointOfContactVO> pocVOList = pointOfContactDAO
				.getContactList(orgDivisionVO);
		NullAwareBeanUtilsBean utilsBean = new NullAwareBeanUtilsBean();
		pocDTOList = utilsBean.copyList(PointOfContactDTO.class, pocVOList);
		return pocDTOList;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.PointOfContactLogic#getContactDetail(com.ilex.
	 * ws.core.dto.PointOfContactDTO)
	 */
	@Override
	public PointOfContactDTO getContactDetail(
			PointOfContactDTO argPointOfContactDTO) {
		PointOfContactDTO pointOfContactDTO = new PointOfContactDTO();
		PointOfContactVO pointOfContactVO = new PointOfContactVO();
		BeanUtils.copyProperties(argPointOfContactDTO, pointOfContactVO);
		pointOfContactVO = pointOfContactDAO.getContactDetail(pointOfContactVO);

		BeanUtils.copyProperties(pointOfContactVO, pointOfContactDTO);
		return pointOfContactDTO;

}
}
