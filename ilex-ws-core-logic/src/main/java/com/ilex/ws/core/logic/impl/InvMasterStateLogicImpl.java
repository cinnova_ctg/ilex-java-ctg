package com.ilex.ws.core.logic.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.InvStateListVO;
import com.ilex.ws.core.dao.InvMasterStateDAO;
import com.ilex.ws.core.dto.InvStateListDTO;
import com.ilex.ws.core.logic.InvMasterStateLogic;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;

@Component
public class InvMasterStateLogicImpl implements InvMasterStateLogic {
	@Resource
	InvMasterStateDAO invMasterStateDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.InvMasterStateLogic#getStateList()
	 */
	@Override
	public List<InvStateListDTO> getStateList() {
		List<InvStateListVO> invStateList = invMasterStateDAO.getStateList();
		NullAwareBeanUtilsBean nullAwareBeanUtilsBean = new NullAwareBeanUtilsBean();
		List<InvStateListDTO> invStateListDTO = nullAwareBeanUtilsBean
				.copyList(InvStateListDTO.class, invStateList);
		return invStateListDTO;

	}

}
