package com.ilex.ws.core.logic.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.InvoiceJobDetailVO;
import com.ilex.ws.core.bean.JobInformationVO;
import com.ilex.ws.core.dao.DashportJobActionDAO;
import com.ilex.ws.core.dao.JobScheduleDAO;
import com.ilex.ws.core.dto.InvoiceJobDetailDTO;
import com.ilex.ws.core.dto.JobInformationDTO;
import com.ilex.ws.core.dto.LabelValue;
import com.ilex.ws.core.dto.PartnerDetailDTO;
import com.ilex.ws.core.dto.SchedulerDTO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.logic.DashportJobActionLogic;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.prm.ViewInstallationNotesBean;
import com.mind.bean.pvs.PartnerDetailBean;
import com.mind.bean.pvs.Partner_SearchBean;
import com.mind.dao.PRM.CustomerInfoBean;

/**
 * The Class DashportJobActionLogicImpl.
 */
@Component
public class DashportJobActionLogicImpl implements DashportJobActionLogic {

	/** The Constant logger. */
	private static final Logger logger = Logger
			.getLogger(DashportJobActionLogicImpl.class);

	/** The dashport job action dao. */
	@Resource
	DashportJobActionDAO dashportJobActionDAO;

	/** The job schedule dao. */
	@Resource
	JobScheduleDAO jobScheduleDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#getScheduledDetail(java
	 * .lang.String)
	 */
	@Override
	public List<InvoiceJobDetailDTO> getScheduledDetail(String jobId) {
		List<InvoiceJobDetailDTO> invoiceJobDetailDTOList = new ArrayList<InvoiceJobDetailDTO>();
		List<InvoiceJobDetailVO> invoiceJobDetailVOList = dashportJobActionDAO
				.getScheduledDetail(jobId);
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		invoiceJobDetailDTOList = awareBeanUtilsBean.copyList(
				InvoiceJobDetailDTO.class, invoiceJobDetailVOList);
		return invoiceJobDetailDTOList;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#getInvoiceActResourceList
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	public List<InvoiceJobDetailDTO> getInvoiceActResourceList(String type,
			String jobId) {
		List<InvoiceJobDetailDTO> invoiceJobDetailDTOList = new ArrayList<InvoiceJobDetailDTO>();
		List<InvoiceJobDetailVO> invoiceJobDetailVOList = dashportJobActionDAO
				.getInvoiceActResourceList(type, jobId);
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		invoiceJobDetailDTOList = awareBeanUtilsBean.copyList(
				InvoiceJobDetailDTO.class, invoiceJobDetailVOList);
		return invoiceJobDetailDTOList;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#getJobInfo(com.ilex.ws.
	 * core.dto.InvoiceJobDetailDTO, java.lang.String)
	 */
	@Override
	public InvoiceJobDetailDTO getJobInfo(
			InvoiceJobDetailDTO invoiceJobDetailDTO, String jobId) {
		InvoiceJobDetailVO invoiceJobDetailVO = new InvoiceJobDetailVO();
		if (StringUtils.isNotEmpty(jobId)) {
			dashportJobActionDAO.setJobTicketInfo(invoiceJobDetailVO, jobId);
			BeanUtils.copyProperties(invoiceJobDetailVO, invoiceJobDetailDTO);
		}
		return invoiceJobDetailDTO;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#addInstalltionNotes(java
	 * .lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String addInstalltionNotes(String jobId,
			String installationNotesArg, String userId) {

		try {
			String instalationTypeId = null;
			String installationNotes = null;
			if (StringUtils.isNotEmpty(installationNotesArg))
				installationNotes = installationNotesArg.trim();
			dashportJobActionDAO.addinstallnotes(jobId, installationNotes,
					userId, "", "0", "0", "", instalationTypeId);
		} catch (Exception ex) {
			return ex.getMessage();
		}
		return "success";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#setScheduleDates(java.lang
	 * .String)
	 */
	@Override
	public SchedulerDTO setScheduleDates(String jobId) {

		JobDashboardBean jobDashboardBean = jobScheduleDAO
				.setScheduleDates(jobId);
		SchedulerDTO schedulerDTO = new SchedulerDTO();
		schedulerDTO.setActenddate(jobDashboardBean.getOffsiteDate());
		schedulerDTO.setActenddatehh(jobDashboardBean.getOffsiteHour());
		schedulerDTO.setActenddatemm(jobDashboardBean.getOffsiteMinute());
		if (jobDashboardBean.getOffsiteIsAm() != null
				&& jobDashboardBean.getOffsiteIsAm().equals("1")) {
			schedulerDTO.setActenddateop("AM");
		} else if (jobDashboardBean.getOffsiteIsAm() != null
				&& jobDashboardBean.getOffsiteIsAm().equals("0")) {
			schedulerDTO.setActenddateop("PM");
		}

		schedulerDTO.setActstartdate(jobDashboardBean.getOnsiteDate());
		schedulerDTO.setActstartdatehh(jobDashboardBean.getOnsiteHour());
		schedulerDTO.setActstartdatemm(jobDashboardBean.getOnsiteMinute());
		if (jobDashboardBean.getOnsiteIsAm() != null
				&& jobDashboardBean.getOnsiteIsAm().equals("1")) {
			schedulerDTO.setActstartdateop("AM");
		} else if (jobDashboardBean.getOnsiteIsAm() != null
				&& jobDashboardBean.getOnsiteIsAm().equals("0")) {
			schedulerDTO.setActstartdateop("PM");
		}

		schedulerDTO.setScheduleEnddate(jobDashboardBean.getScheduleEndDate());
		schedulerDTO.setScheduleEndhh(jobDashboardBean.getScheduleEndHour());
		schedulerDTO.setScheduleEndmm(jobDashboardBean.getScheduleEndMinute());
		if (jobDashboardBean.getScheduleEndIsAm() != null
				&& jobDashboardBean.getScheduleEndIsAm().equals("1")) {
			schedulerDTO.setScheduleEndop("AM");
		} else if (jobDashboardBean.getScheduleEndIsAm() != null
				&& jobDashboardBean.getScheduleEndIsAm().equals("0")) {
			schedulerDTO.setScheduleEndop("PM");
		}

		schedulerDTO.setStartdate(jobDashboardBean.getScheduleStartDate());
		schedulerDTO.setStartdatehh(jobDashboardBean.getScheduleStartHour());
		schedulerDTO.setStartdatemm(jobDashboardBean.getScheduleStartMinute());
		if (jobDashboardBean.getScheduleStartIsAm() != null
				&& jobDashboardBean.getScheduleStartIsAm().equals("1")) {
			schedulerDTO.setStartdateop("AM");
		} else if (jobDashboardBean.getScheduleStartIsAm() != null
				&& jobDashboardBean.getScheduleStartIsAm().equals("0")) {
			schedulerDTO.setStartdateop("PM");
		}

		schedulerDTO.setOldstartdate(jobDashboardBean.getScheduleStartDate());
		schedulerDTO.setOldstartdatehh(jobDashboardBean.getScheduleStartHour());
		schedulerDTO.setOldstartdatemm(jobDashboardBean
				.getScheduleStartMinute());
		if (jobDashboardBean.getScheduleStartIsAm() != null
				&& jobDashboardBean.getScheduleStartIsAm().equals("1")) {
			schedulerDTO.setOldstartdateop("AM");
		} else if (jobDashboardBean.getScheduleStartIsAm() != null
				&& jobDashboardBean.getScheduleStartIsAm().equals("0")) {
			schedulerDTO.setOldstartdateop("PM");
		}

		return schedulerDTO;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#saveSchedulerInfo(com.ilex
	 * .ws.core.dto.SchedulerDTO, java.lang.String)
	 */
	@Override
	public String saveSchedulerInfo(SchedulerDTO schedulerDTO, String userId) {
		try {

			IlexTimestamp scheduleStartTimestamp = new IlexTimestamp(
					schedulerDTO.getStartdate(), schedulerDTO.getStartdatehh(),
					schedulerDTO.getStartdatemm(),
					IlexTimestamp.getBooleanAMPM(schedulerDTO.getStartdateop()));
			IlexTimestamp scheduleEndTimestamp = new IlexTimestamp(
					schedulerDTO.getScheduleEnddate(),
					schedulerDTO.getScheduleEndhh(),
					schedulerDTO.getScheduleEndmm(),
					IlexTimestamp.getBooleanAMPM(schedulerDTO
							.getScheduleEndop()));
			IlexTimestamp actualStartTimestamp = new IlexTimestamp(
					schedulerDTO.getActstartdate(),
					schedulerDTO.getActstartdatehh(),
					schedulerDTO.getActstartdatemm(),
					IlexTimestamp.getBooleanAMPM(schedulerDTO
							.getActstartdateop()));
			IlexTimestamp actualEndTimestamp = new IlexTimestamp(
					schedulerDTO.getActenddate(),
					schedulerDTO.getActenddatehh(),
					schedulerDTO.getActenddatemm(),
					IlexTimestamp.getBooleanAMPM(schedulerDTO.getActenddateop()));

			String scheduleStartDate = IlexTimestamp
					.getDateTimeType1(scheduleStartTimestamp);
			String scheduleEndDate = IlexTimestamp
					.getDateTimeType1(scheduleEndTimestamp);
			String actualStartDate = IlexTimestamp
					.getDateTimeType1(actualStartTimestamp);
			String actualEndDate = IlexTimestamp
					.getDateTimeType1(actualEndTimestamp);

			int retValArray[] = new int[2];
			retValArray = jobScheduleDAO.addScheduleForPRM(
					schedulerDTO.getJobId(), actualStartDate, actualEndDate,
					"J", userId, scheduleStartDate, scheduleEndDate, 0);
			String newScheduleId = String.valueOf(retValArray[1]);

			/* Add Install notes: Start */
			if (!actualStartDate.equals("")
					&& !actualStartDate.equals("01/01/1900")) {
				String defaultInstallationNotesForOnsite = "Tech onsite for Site Visit 1";

				dashportJobActionDAO.addinstallnotes(schedulerDTO.getJobId(),
						defaultInstallationNotesForOnsite, userId,
						actualStartDate, schedulerDTO.getScheduleId(),
						newScheduleId, "S", "0");

			}
			if (!actualEndDate.equals("")
					&& !actualEndDate.equals("01/01/1900")) {
				String defaultInstallationNotesForOffsite = "Tech offsite for Site Visit 1";
				dashportJobActionDAO.addinstallnotes(schedulerDTO.getJobId(),
						defaultInstallationNotesForOffsite, userId,
						actualStartDate, schedulerDTO.getScheduleId(),
						newScheduleId, "S", "0");

			}

		} catch (Exception ex) {
			return ex.getMessage();
		}
		/* Add Install notes: End */

		return "success";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#updateJobCategoryPriority
	 * (com.ilex.ws.core.dto.JobInformationDTO, java.lang.String)
	 */
	@Override
	public String updateJobCategoryPriority(
			JobInformationDTO jobInformationDTO, String userId) {
		try {
			JobInformationVO jobInformationVO = new JobInformationVO();
			BeanUtils.copyProperties(jobInformationDTO, jobInformationVO);
			dashportJobActionDAO.updateJobCategoryPriority(jobInformationVO,
					userId);
		} catch (Exception ex) {
			return ex.getMessage();
		}

		return "success";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#getJobCategoryPriority(
	 * java.lang.String)
	 */
	@Override
	public JobInformationDTO getJobCategoryPriority(String jobId) {
		JobInformationVO jobInformationVO = dashportJobActionDAO
				.getJobCategoryPriority(jobId);
		JobInformationDTO jobInformationDTO = new JobInformationDTO();
		BeanUtils.copyProperties(jobInformationVO, jobInformationDTO);
		jobInformationDTO.setJobCategorySelect(jobInformationDTO
				.getJobCategory());
		jobInformationDTO.setJobPrioritySelect(jobInformationDTO
				.getJobPriority());
		List<String> appendixCategories = dashportJobActionDAO
				.getAppendixCategories(jobId);
		List<String> appendixPriorities = dashportJobActionDAO
				.getAppendixPriorities(jobId);
		jobInformationDTO.setJobCategories(appendixCategories);
		jobInformationDTO.setJobPriorities(appendixPriorities);
		return jobInformationDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#saveReScheduledInfo(com
	 * .ilex.ws.core.dto.SchedulerDTO, java.lang.String)
	 */
	@Override
	public String saveReScheduledInfo(SchedulerDTO schedulerDTO, String userId) {
		try {

			String saveSchedulerStatus = saveSchedulerInfo(schedulerDTO, userId);
			if (!saveSchedulerStatus.equals("success")) {
				throw new IlexBusinessException(saveSchedulerStatus);
			}

			if (schedulerDTO.getRseType() != null
					&& !schedulerDTO.getRseType().equals("Administrative")) {

				IlexTimestamp scheduleStartTimestamp = new IlexTimestamp(
						schedulerDTO.getStartdate(),
						schedulerDTO.getStartdatehh(),
						schedulerDTO.getStartdatemm(),
						IlexTimestamp.getBooleanAMPM(schedulerDTO
								.getStartdateop()));

				String scheduleStartDate = IlexTimestamp
						.getDateTimeType1(scheduleStartTimestamp);

				IlexTimestamp oldscheduleStartTimestamp = new IlexTimestamp(
						schedulerDTO.getOldstartdate(),
						schedulerDTO.getOldstartdatehh(),
						schedulerDTO.getOldstartdatemm(),
						IlexTimestamp.getBooleanAMPM(schedulerDTO
								.getOldstartdateop()));

				String oldscheduleStartDate = IlexTimestamp
						.getDateTimeType1(oldscheduleStartTimestamp);
				jobScheduleDAO.setReschedule(schedulerDTO.getJobId(),
						schedulerDTO.getRseType(), oldscheduleStartDate,
						scheduleStartDate, schedulerDTO.getRseReason(), userId);
			}
		} catch (Exception ex) {
			return ex.getMessage();

		}
		return "success";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#isJobValidForChange(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public String isJobValidForChange(String jobId, String cubeLastRefreshTime) {

		String jobChangedTime = dashportJobActionDAO.getJobChangedTime(jobId);
		DateFormat formatter;
		Date jobChangedDateTime = null;
		Date cubeRefreshDateTime = null;
		formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		try {
			jobChangedDateTime = formatter.parse(jobChangedTime);
			cubeRefreshDateTime = formatter.parse(cubeLastRefreshTime);

		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}

		Calendar jobChangedCalender = Calendar.getInstance();
		Calendar cubeRefeshCalender = Calendar.getInstance();
		jobChangedCalender.setTime(jobChangedDateTime);
		cubeRefeshCalender.setTime(cubeRefreshDateTime);

		if (jobChangedCalender.after(cubeRefeshCalender)) {
			return "invalid";
		}

		return "valid";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#getJOList(java.lang.String)
	 */
	@Override
	public List<LabelValue> getJOList(String appendixId) {
		List<LabelValue> jobOwnerList = dashportJobActionDAO
				.getJOList(appendixId);
		return jobOwnerList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.DashportJobActionLogic#updateJobOwner(int,
	 * int)
	 */
	@Override
	public String updateJobOwner(int jobId, int newJobOwnerId) {
		try {
			dashportJobActionDAO.updateJobOwner(jobId, newJobOwnerId);
		} catch (Exception ex) {
			return ex.getMessage();
		}

		return "success";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#gerCustomerFieldData(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public List<CustomerInfoBean> gerCustomerFieldData(String jobId,
			String appendixId) {
		List<CustomerInfoBean> customerFieldData = new ArrayList<CustomerInfoBean>();
		customerFieldData = dashportJobActionDAO.getCustomerFieldData(jobId,
				appendixId);
		return customerFieldData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#checkForSnapon(java.lang
	 * .String)
	 */
	@Override
	public boolean checkForSnapon(String appendixId) {
		boolean snaponCheck = dashportJobActionDAO.checkForSnapon(appendixId);
		return snaponCheck;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#updateCustomerFiledInfo
	 * (java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String updateCustomerFiledInfo(String jobId,
			String appendixCustomerIds, String customerValues,
			String loginuserid) {
		int status = -1;
		try {
			status = dashportJobActionDAO.updateCustomerFiledInfo(jobId,
					appendixCustomerIds, customerValues, loginuserid);
		} catch (Exception ex) {
			return ex.getMessage();
		}
		String retunStatus = null;
		if (status == 0)
			retunStatus = "success";
		if (status == -9001)
			retunStatus = "Error occured during deleting";
		if (status == -9002)
			retunStatus = "Error occured during updating";
		return retunStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#getInstallationNotesList
	 * (java.lang.String)
	 */
	@Override
	public List<ViewInstallationNotesBean> getInstallationNotesList(String jobId) {
		List<ViewInstallationNotesBean> installtionNotesList = dashportJobActionDAO
				.getInstallationNotesList(jobId);
		return installtionNotesList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#getpartnersSearchList(java
	 * .lang.String)
	 */
	@Override
	public List<Partner_SearchBean> getpartnersSearchList(String zipcode) {
		if (zipcode.equalsIgnoreCase("null")) {
			zipcode = "";
		}
		String sortqueryclause = ",qc_ps_average_rating desc,distance asc";
		Partner_SearchBean partnerSearchBean = new Partner_SearchBean();
		partnerSearchBean.setName("");
		partnerSearchBean.setSearchTerm("");
		String[] checkedPartnerType = { "M", "SS" };
		partnerSearchBean.setCheckedPartnerName(checkedPartnerType);
		partnerSearchBean.setMcsaVersion("0");
		partnerSearchBean.setRadius("25");
		partnerSearchBean.setMainOffice("Y");
		partnerSearchBean.setFieldOffice(null);
		partnerSearchBean.setTechniciansZip(null);
		partnerSearchBean.setTechnicians(null);
		partnerSearchBean.setCertifications("0");
		partnerSearchBean.setCoreCompetency(null);
		partnerSearchBean.setCbox1(null);
		partnerSearchBean.setCbox2("Y");
		partnerSearchBean.setPartnerRating("N");
		partnerSearchBean.setSecurityCertiftn(null);
		partnerSearchBean.setAssign("");
		partnerSearchBean.setSelectedCountryName("0");
		partnerSearchBean.setSelectedCityName("0");
		List<Partner_SearchBean> partnerSearchList = dashportJobActionDAO
				.getpartnersSearchList(partnerSearchBean, sortqueryclause,
						zipcode);
		for (Partner_SearchBean partnerDetail : partnerSearchList) {

			if (partnerDetail.isLeadMinuteman() == true) {
				partnerDetail.setPartnerHighLight("leadMinuteManHighLight");
			} else {

				if (partnerDetail.getPartnerType()
						.equalsIgnoreCase("Minuteman")) {

					if (partnerDetail.getIcontype().equalsIgnoreCase("Mred")) {
						partnerDetail.setPartnerHighLight("redHighLight");
					}

					else if (partnerDetail.getIcontype().equalsIgnoreCase(
							"Mgreen")) {
						partnerDetail.setPartnerHighLight("greenHighLight");
					}

					else if (partnerDetail.getIcontype().equalsIgnoreCase(
							"Myellow")) {
						partnerDetail.setPartnerHighLight("yellowHighLight");
					} else if (partnerDetail.getIcontype().equalsIgnoreCase(
							"Morange")) {
						partnerDetail.setPartnerHighLight("orangeHighLight");
					}
				}

				else if (partnerDetail.getPartnerType().equalsIgnoreCase(
						"Certified Partners - Speedpay Users")) {
					if (partnerDetail.getIcontype().equalsIgnoreCase("Sred")) {
						partnerDetail.setPartnerHighLight("redHighLight");
					}

					else if (partnerDetail.getIcontype().equalsIgnoreCase(
							"Sgreen")) {
						partnerDetail.setPartnerHighLight("greenHighLight");
					}

					else if (partnerDetail.getIcontype().equalsIgnoreCase(
							"Syellow")) {
						partnerDetail.setPartnerHighLight("yellowHighLight");
					} else if (partnerDetail.getIcontype().equalsIgnoreCase(
							"Sorange")) {
						partnerDetail.setPartnerHighLight("orangeHighLight");
					}
				}
			}
			if (StringUtils.isBlank(partnerDetail.getPartnerLastUsed())) {
				if (StringUtils.isNotBlank(partnerDetail.getUpdateDate())) {
					partnerDetail
							.setLastUsedDate(partnerDetail.getUpdateDate());
					partnerDetail
							.setLastUsedDateHighLight("lastUsedDateHighLight");
				} else {
					partnerDetail
							.setLastUsedDateHighLight("lastUsedDateHighLight");
					partnerDetail
							.setLastUsedDate(partnerDetail.getCreateDate());
				}
			}

			else {

				partnerDetail.setLastUsedDate(partnerDetail
						.getPartnerLastUsed());
			}

		}

		return partnerSearchList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.DashportJobActionLogic#getPartnerDetails(com.ilex
	 * .ws.core.dto.PartnerDetailDTO)
	 */
	@Override
	public String getPartnerDetails(PartnerDetailDTO partnerDetailDTO,
			String url) {
		PartnerDetailBean partnerDetailBean = new PartnerDetailBean();
		BeanUtils.copyProperties(partnerDetailDTO, partnerDetailBean);
		String checkPartner = dashportJobActionDAO.getPartnerDetails(
				partnerDetailBean, url);
		BeanUtils.copyProperties(partnerDetailBean, partnerDetailDTO);
		return checkPartner;
	}
}