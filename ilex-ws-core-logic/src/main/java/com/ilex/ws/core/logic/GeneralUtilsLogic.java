package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.CountryDTO;
import com.ilex.ws.core.dto.StateDTO;

public interface GeneralUtilsLogic {
	List<CountryDTO> getCountrylist();

	List<StateDTO> getStateList();

}
