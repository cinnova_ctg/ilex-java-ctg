package com.ilex.ws.core.logic.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.MsaClientVO;
import com.ilex.ws.core.dao.MsaClientDAO;
import com.ilex.ws.core.dto.MsaClientDTO;
import com.ilex.ws.core.logic.MsaClientLogic;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;

@Component
public class MsaClientLogicImpl implements MsaClientLogic {

	
	@Resource
	MsaClientDAO msaClientDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.MsaClientLogic#getClientList()
	 */
	@Override
	public List<MsaClientDTO> getClientList() {

		List<MsaClientDTO> msaClientDTOList = new ArrayList<MsaClientDTO>();
		List<MsaClientVO> msaClientVOList = msaClientDAO.getClientList();
		NullAwareBeanUtilsBean utilsBean = new NullAwareBeanUtilsBean();
		msaClientDTOList = utilsBean.copyList(MsaClientDTO.class,
				msaClientVOList);
		return msaClientDTOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.MsaClientLogic#getClientDetail(com.ilex.ws.core
	 * .dto.MsaClientDTO)
	 */
	@Override
	public MsaClientDTO getClientDetail(MsaClientDTO argClientDTO) {
		MsaClientDTO masterClientDTO = new MsaClientDTO();
		MsaClientVO msaClientVO = new MsaClientVO();
		BeanUtils.copyProperties(argClientDTO, msaClientVO);
		msaClientVO = msaClientDAO.getClientDetail(msaClientVO);
		BeanUtils.copyProperties(msaClientVO, masterClientDTO);
		return masterClientDTO;
	}
}
