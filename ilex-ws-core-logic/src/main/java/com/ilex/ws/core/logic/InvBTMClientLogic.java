package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.ClientListDTO;
import com.ilex.ws.core.dto.InvBTMClientDTO;

public interface InvBTMClientLogic {

	/**
	 * Gets the client list.
	 * 
	 * @return the client list
	 */
	public List<InvBTMClientDTO> getClientList();

	/**
	 * Gets the client details.
	 * 
	 * @param clientId
	 *            the client id
	 * @return the client details
	 */
	InvBTMClientDTO getClientDetails(String clientId);

	/**
	 * Update btm client details.
	 * 
	 * @param btmClientDTO
	 *            the btm client dto
	 */
	void updateBTMClientDetails(InvBTMClientDTO btmClientDTO);

	/**
	 * Gets the client list by name.
	 * 
	 * @param searchKey
	 *            the search key
	 * @param pageIndex
	 *            the page index
	 * @return the client list by name
	 */
	ClientListDTO getClientListByName(String searchKey, int pageIndex);

}
