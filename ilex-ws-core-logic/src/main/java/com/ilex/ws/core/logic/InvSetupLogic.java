package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.AppendixDetailDTO;
import com.ilex.ws.core.dto.AppendixInvSetupDTO;
import com.ilex.ws.core.dto.InvAdditionalInfoDTO;
import com.ilex.ws.core.dto.InvBillToAddressDTO;
import com.ilex.ws.core.dto.InvMasterLineItemDetailsDTO;
import com.ilex.ws.core.dto.InvSearchListDTO;
import com.ilex.ws.core.dto.InvSetupDTO;

/**
 * The Interface InvSetupLogic.
 */
public interface InvSetupLogic {

	/**
	 * Gets the initial details.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @return the initial details
	 */
	InvSetupDTO getInitialDetails(AppendixInvSetupDTO appendixInvSetupDTO);

	/**
	 * Gets the client list for appendix inv setup.
	 * 
	 * @param searchKey
	 *            the search key
	 * @param pageIndex
	 *            the page index
	 * @return the client list for appendix inv setup
	 */
	InvSearchListDTO getClientListForAppendixInvSetup(String searchKey,
			int pageIndex);

	/**
	 * Gets the line item details.
	 * 
	 * @param appendixDetailDTO
	 *            the appendix detail dto
	 * @return the line item details
	 */
	List<InvMasterLineItemDetailsDTO> getLineItemDetails(
			AppendixDetailDTO appendixDetailDTO);

	/**
	 * Save appendix inv detail.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @return
	 */
	int saveAppendixInvDetail(AppendixInvSetupDTO appendixInvSetupDTO);

	/**
	 * Gets the bill to address details.
	 * 
	 * @param invBillToAddressId
	 *            the inv bill to address id
	 * @return the bill to address details
	 */
	InvBillToAddressDTO getBillToAddressDetails(int invBillToAddressId);

	/**
	 * Update client profile.
	 * 
	 * @param invBillToAddressDTO
	 *            the inv bill to address dto
	 */
	void updateClientProfile(InvBillToAddressDTO invBillToAddressDTO);

	/**
	 * Gets the inv setup details.
	 * 
	 * @param invSetupId
	 *            the inv setup id
	 * @return the inv setup details
	 */
	InvSetupDTO getInvSetupDetails(int invSetupId);

	/**
	 * Delete inv setup details.
	 * 
	 * @param invBillToAddressId
	 *            the inv bill to address id
	 */
	void deleteInvSetupDetails(int invBillToAddressId);

	/**
	 * Gets the additional info.
	 * 
	 * @param invSetupDTO
	 *            the inv setup dto
	 * @return the additional info
	 */
	List<InvAdditionalInfoDTO> getAdditionalInfo(InvSetupDTO invSetupDTO);

	/**
	 * Update invoice profile.
	 * 
	 * @param invSetupDTO
	 *            the inv setup dto
	 */
	void updateInvoiceProfile(InvSetupDTO invSetupDTO);

	/**
	 * Update line item details.
	 * 
	 * @param invSetupDTO
	 *            the inv setup dto
	 */
	void updateLineItemDetails(InvSetupDTO invSetupDTO);

	/**
	 * Update summary sheet details.
	 * 
	 * @param invSetupDTO
	 *            the inv setup dto
	 */
	void updateSummarySheetDetails(InvSetupDTO invSetupDTO);

	/**
	 * Update client req info.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 */
	void updateClientReqInfo(AppendixInvSetupDTO appendixInvSetupDTO);

	/**
	 * Gets the cust info parameter.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @return the cust info parameter
	 */
	List<String> getCustInfoParameter(int appendixId);

	InvSetupDTO getInvSetupDetails(InvBillToAddressDTO invBillToAddressDTO);

}
