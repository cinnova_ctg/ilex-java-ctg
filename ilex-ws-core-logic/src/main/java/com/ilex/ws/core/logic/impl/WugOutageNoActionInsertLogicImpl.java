package com.ilex.ws.core.logic.impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.dao.WugOutageNoActionInsertDAO;
import com.ilex.ws.core.logic.WugErrorLogInsertLogic;
import com.ilex.ws.core.logic.WugOutageCloseActionsLogic;
import com.ilex.ws.core.logic.WugOutageNoActionInsertLogic;

@Component
public class WugOutageNoActionInsertLogicImpl implements
		WugOutageNoActionInsertLogic {
	public static final Logger logger = Logger
			.getLogger(WugOutageNoActionInsertLogicImpl.class);
	@Resource
	WugOutageNoActionInsertDAO wugOutageNoActionInsertDAO;
	@Resource
	WugOutageCloseActionsLogic wugOutageCloseActionsLogic;
	@Resource
	WugErrorLogInsertLogic wugErrorLogInsertLogic;

	@Override
	public String wugOutageNoActionInsert(String nActiveMonitorStateChangeID,
			String userId, String wugInstance) {
		String wugNaDvId;

		// First, get the WUG ID associated with this active log ID
		wugNaDvId = wugOutageNoActionInsertDAO.getWUGId(
				nActiveMonitorStateChangeID, wugInstance);
		try {
			wugOutageNoActionInsertDAO
					.insertUpdateWugOutageNoAction(nActiveMonitorStateChangeID,
							wugInstance, wugNaDvId, userId);
		} catch (Exception e) {
			logger.error(e.getMessage());
			String errorData = "Insert/Update into wug_outage_no_action failed for "
					+ nActiveMonitorStateChangeID;
			wugErrorLogInsertLogic.insertErrorLog("Error: No Action",
					errorData, "6");
			return "0";
		}
		// -- Close outage with defaults

		String errorCode = wugOutageCloseActionsLogic.wugOutageCloseAction(
				nActiveMonitorStateChangeID, "2", "0", "0", "0", wugInstance,
				"0");
		if (!errorCode.equalsIgnoreCase("1")) {
			String errorData = "wug_outage_close_actions_01 failed for "
					+ nActiveMonitorStateChangeID;
			wugErrorLogInsertLogic.insertErrorLog("Error: No Action",
					errorData, "6");
			return "0";
		}
		// -- Update monitoring summary to reflect this addition
		if (wugNaDvId == null || !wugNaDvId.equalsIgnoreCase("0")) {
			wugOutageNoActionInsertDAO.upadateMonitoringSummary();
		}

		return "1";
	}

	@Override
	public void wugOutageNoActionInsert(String nActiveMonitorStateChangeID,
			String userId) {
		String wugInstance = "1";
		wugOutageNoActionInsert(nActiveMonitorStateChangeID, userId,
				wugInstance);

	}

}
