package com.ilex.ws.core.logic.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.InvBTMClientVO;
import com.ilex.ws.core.bean.InvBTMContactVO;
import com.ilex.ws.core.dao.InvBTMClientDAO;
import com.ilex.ws.core.dao.InvBTMContactDAO;
import com.ilex.ws.core.dto.InvBTMClientDTO;
import com.ilex.ws.core.dto.InvBTMContactDTO;
import com.ilex.ws.core.dto.MasterBTMDTO;
import com.ilex.ws.core.logic.InvBTMContactLogic;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;

@Component
public class InvBTMContactLogicImpl implements InvBTMContactLogic {
    @Resource
    InvBTMContactDAO invBTMContactDAO;
    @Resource
    InvBTMClientDAO invBTMClientDAO;

    private static final Logger logger = Logger
	    .getLogger(InvBTMContactLogicImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ilex.ws.core.logic.InvBTMContactLogic#getBTMContactDetails(com.ilex
     * .ws.core.dto.InvBTMClientDTO)
     */
    @Override
    public List<InvBTMContactDTO> getBTMContactDetails(
	    InvBTMClientDTO invBTMClientDTO) {
	InvBTMClientVO argInvBTMClientVO = new InvBTMClientVO();
	try {
	    BeanUtils.copyProperties(argInvBTMClientVO, invBTMClientDTO);
	} catch (IllegalAccessException e) {
	    logger.error(e);
	} catch (InvocationTargetException e) {
	    logger.error(e);
	}
	List<InvBTMContactVO> invBTMContactVOList = invBTMContactDAO
		.getBTMContactDetails(argInvBTMClientVO);
	NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
	List<InvBTMContactDTO> invBTMContactDTOList = awareBeanUtilsBean
		.copyList(InvBTMContactDTO.class, invBTMContactVOList);
	return invBTMContactDTOList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ilex.ws.core.logic.InvBTMContactLogic#updateBTMContactDetails(com
     * .ilex.ws.core.dto.BTMContactListDTO)
     */
    @Override
    public void updateBTMContactDetails(MasterBTMDTO masterBTMDTO) {
	int mbtClientId = masterBTMDTO.getBtmClientDTO().getMbtClientId();
	boolean activeContact = false;
	if (masterBTMDTO.getInvBTMContactList() != null
		&& masterBTMDTO.getInvBTMContactList().size() > 0) {
	    for (InvBTMContactDTO invBTMContactDTO : masterBTMDTO
		    .getInvBTMContactList()) {
		if (invBTMContactDTO.isStatus()) {
		    activeContact = true;
		}
		InvBTMContactVO invBTMContactVO = new InvBTMContactVO();
		invBTMContactVO.setMbtId(mbtClientId);
		invBTMContactVO.setStatus(invBTMContactDTO.isStatus());
		invBTMContactVO.setFirstName(invBTMContactDTO.getFirstName());
		invBTMContactVO.setPhone(invBTMContactDTO.getPhone());
		invBTMContactVO.setEmail(invBTMContactDTO.getEmail());
		invBTMContactVO.setFax(invBTMContactDTO.getFax());
		invBTMContactVO.setLastName(invBTMContactDTO.getLastName());
		invBTMContactVO.setCell(invBTMContactDTO.getCell());
		if (invBTMContactDTO.getInvContactId() == null) {
		    boolean contactStatus = validateContact(invBTMContactDTO);
		    if (contactStatus == true) {
			invBTMContactDAO
				.insertBTMContactDetails(invBTMContactVO);
		    }

		} else {
		    invBTMContactVO.setInvContactId(invBTMContactDTO
			    .getInvContactId());
		    invBTMContactDAO.updateBTMContactDetails(invBTMContactVO);
		}
	    }

	    invBTMClientDAO.updateBTMClientContactStatus(mbtClientId,
		    activeContact);

	}
    }

    /**
     * Validate contact.
     * 
     * @param invBTMContactDTO
     *            the inv btm contact dto
     * @return true, if successful
     */
    private boolean validateContact(InvBTMContactDTO invBTMContactDTO) {
	if ((invBTMContactDTO.getFirstName() == null || StringUtils
		.isEmpty(invBTMContactDTO.getFirstName()))
		&& (invBTMContactDTO.getLastName() == null || StringUtils
			.isEmpty(invBTMContactDTO.getLastName()))
		&& (invBTMContactDTO.getCell() == null || StringUtils
			.isEmpty(invBTMContactDTO.getCell()))
		&& (invBTMContactDTO.getEmail() == null || StringUtils
			.isEmpty(invBTMContactDTO.getEmail()))
		&& (invBTMContactDTO.getFax() == null || StringUtils
			.isEmpty(invBTMContactDTO.getFax()))
		&& (invBTMContactDTO.getPhone() == null || StringUtils
			.isEmpty(invBTMContactDTO.getPhone()))) {
	    return false;
	}
	return true;

    }
}
