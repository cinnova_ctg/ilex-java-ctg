package com.ilex.ws.core.logic;

/**
 * The Interface WugErrorLogInsertLogic. Add Error Occurrence Description: Adds
 * error occurrence to include a code and supporting data. Also each caller is
 * responsible for establishing the delete point. No additional error record is
 * recorded beyond this point.
 */
public interface WugErrorLogInsertLogic {

	void insertErrorLog(String code, String data, String deleteDeltaMonths);

}
