package com.ilex.ws.core.logic;

import java.util.List;

import org.apache.commons.fileupload.FileItem;

import com.ilex.ws.core.dto.NetMedXConsolidatorRequestDTO;
import com.ilex.ws.core.dto.NetMedXConsolidatorRespDTO;
import com.ilex.ws.core.dto.NetMedXIntegrationDTO;
import com.ilex.ws.core.dto.NetMedXIntegrationResponseDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface NetMedXLogic.
 */
public interface NetMedXLogic {

	/**
	 * Net med x intergration.
	 * 
	 * @param netMedXIntegrationDTO
	 *            the net med x integration dto
	 * @param fileItem
	 *            the file item
	 * @return the net med x integration response dto
	 */
	NetMedXIntegrationResponseDTO netMedXIntergration(
			NetMedXIntegrationDTO netMedXIntegrationDTO, FileItem fileItem);

	public String netmedxUserAuth(String userName, String pass, int prId);

	/**
	 * Gets the net med x job intergration.
	 * 
	 * @param netMedXIntegrationDTO
	 *            the net med x integration dto
	 * @return the net med x job intergration
	 */
	public List<NetMedXConsolidatorRespDTO> getNetMedXJobIntergration(
			NetMedXConsolidatorRequestDTO netMedXIntegrationDTO);

	/**
	 * Gets the job count.
	 * 
	 * @param projectId
	 *            the project id
	 * @return the job count
	 */
	public String getJobCount(
			NetMedXConsolidatorRequestDTO netMedXConsolidatorRequestDTO);

}
