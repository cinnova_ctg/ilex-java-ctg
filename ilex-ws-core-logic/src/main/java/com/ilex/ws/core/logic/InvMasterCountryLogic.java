package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.InvCountryListDTO;

public interface InvMasterCountryLogic {

	/**
	 * Gets the country list.
	 * 
	 * @return the country list
	 */
	List<InvCountryListDTO> getCountryList();

}
