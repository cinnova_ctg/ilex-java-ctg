package com.ilex.ws.core.logic.impl;

import com.ilex.ws.core.bean.HDTicketVO;
import com.ilex.ws.core.bean.IncidentVO;
import com.ilex.ws.core.bean.TicketStateVO;
import com.ilex.ws.core.dao.HDMainDAO;
import com.ilex.ws.core.dao.ServiceNowDAO;
import com.ilex.ws.core.dto.HDTicketDTO;
import com.ilex.ws.core.dto.IncidentDTO;
import com.ilex.ws.core.dto.IncidentResponseDTO;
import com.ilex.ws.core.dto.TicketStateDTO;
import com.ilex.ws.core.logic.ServiceNowLogic;
import com.ilex.ws.core.util.DateUtils;
import com.ilex.ws.core.util.HdCubeDisplayStatus;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;
import com.ilex.ws.servicenow.dto.ProactiveIncidentResponseDTO;
import com.ilex.ws.servicenow.dto.ProactiveIncidentTxrResponseDTO;
import com.ilex.ws.servicenow.dto.UpdateIncidentResponseDTO;
import com.ilex.ws.servicenow.dto.UpdateIncidentTxrResponseDTO;
import com.ilex.ws.servicenow.service.IlexIntegrationCreateIncidentService;
import com.ilex.ws.servicenow.service.IlexToServicenowUpdateIncidentService;
import com.ilex.ws.servicenow.txr.IlexToServicenowUpdateIncidentTxrService;
import com.ilex.ws.servicenow.txr.IlexToSnCreateIncidentTxrService;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Email;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.xml.ws.Holder;
import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * The Class ServiceNowLogicImpl.
 */
@Component
public class ServiceNowLogicImpl implements ServiceNowLogic {

    /**
     * The service now dao.
     */
    @Resource
    ServiceNowDAO serviceNowDAO;

    /**
     * The hd main dao.
     */
    @Resource
    HDMainDAO hdMainDAO;

    /**
     * The ilex integration create incident service.
     */
    @Resource
    IlexIntegrationCreateIncidentService ilexIntegrationCreateIncidentService;

    /**
     * The ilex to servicenow update incident service.
     */
    @Resource
    IlexToServicenowUpdateIncidentService ilexToServicenowUpdateIncidentService;

    /**
     * The interval between ilex to servicenow request attempt.
     */
    @Value("#{ilexProperties['servicenow.request.attempt.interval']}")
    private int attempInterval;

    /**
     * Total attempt between ilex to servicenow request
     */
    @Value("#{ilexProperties['servicenow.request.attempt.count']}")
    private int attemptCount;

    @Value("#{ilexProperties['servicenow.email.from']}")
    private String servicenowFrom;
    @Value("#{ilexProperties['servicenow.email.to']}")
    private String servicenowTo;
    @Value("#{ilexProperties['servicenow.email.userName']}")
    private String emailUserName;
    @Value("#{ilexProperties['servicenow.email.userPassword']}")
    private String emailUserPassword;
    @Value("#{ilexProperties['servicenow.email.smtpServerName']}")
    private String emailSmtpServerName;
    @Value("#{ilexProperties['servicenow.email.smtpServerPort']}")
    private String emailSmtpServerPort;

    /**
     * The Constant logger.
     */
    public static final Logger logger = Logger
            .getLogger(ServiceNowLogicImpl.class);

    @Resource
    IlexToSnCreateIncidentTxrService createIncidentTxrService;

    /**
     * The ilex to servicenow update incident service.
     */
    @Resource
    IlexToServicenowUpdateIncidentTxrService updateIncidentTxrService;

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.logic.ServiceNowLogic#createProactiveIncident(com.ilex
     * .ws.core.dto.IncidentDTO, int)
     */
    @Override
    public Map<String, String> createProactiveIncident(IncidentDTO proactiveIncidentDTO, int userId) {
        //IncidentVO proactiveIncidentVO = new IncidentVO();
        //BeanUtils.copyProperties(proactiveIncidentDTO, proactiveIncidentVO);
        // insert ticket to ilex
        Map<String, Long> createResponse = serviceNowDAO.insertProactiveIncidentToIlex(proactiveIncidentDTO, userId);
        Long createIdentifier = createResponse.get("createIdentifier");
        Map<String, String> response = new HashMap<>();
        response.put("createIdentifier", String.valueOf(createIdentifier));
        Holder<String> sysId = new Holder<>();
        Holder<String> table = new Holder<>();
        Holder<String> displayName = new Holder<>();
        Holder<String> displayValue = new Holder<>();
        Holder<String> status = new Holder<>();
        Holder<String> statusMessage = new Holder<>();
        Holder<String> errorMessage = new Holder<>();
        Holder<String> errorNumber = new Holder<>();
        Holder<String> errorDescription = new Holder<>();
        Holder<String> vTaskNumber = new Holder<>();
        Holder<String> ticketNumber = new Holder<>();

        Map<String, String> output = new HashMap<>();

        // insert ticket to Service Now
        // check whether create incident service is for CFA or TXR
        if (proactiveIncidentDTO.getMsaId().equalsIgnoreCase("740")) {
            // For TXR
            for (int counter = 1; counter <= attemptCount; counter++) {
                try {
                    createIncidentTxrService.insert(
                            proactiveIncidentDTO.getCategory(),
                            proactiveIncidentDTO.getConfigurationItem(),
                            proactiveIncidentDTO.getDescription(),
                            proactiveIncidentDTO.getImpact(),
                            proactiveIncidentDTO.getShortDescription(),
                            proactiveIncidentDTO.getSiteNumber(),
                            proactiveIncidentDTO.getState(),
                            proactiveIncidentDTO.getSymptom(),
                            proactiveIncidentDTO.getTicketNumber(),
                            proactiveIncidentDTO.getType(),
                            proactiveIncidentDTO.getUrgency(),
                            proactiveIncidentDTO.getVendorKey(),
                            proactiveIncidentDTO.getWorkNotes(), sysId, table,
                            displayName, displayValue, status, statusMessage,
                            errorMessage, errorNumber, errorDescription,
                            vTaskNumber, ticketNumber);
                    break;

                } catch (Exception ex) {
                    if (counter == attemptCount) {
                        logger.info("Service Now TXR failed attempt number="
                                + counter);
                        logger.error(ex);
                        ProactiveIncidentTxrResponseDTO responseDTO = new ProactiveIncidentTxrResponseDTO();
                        responseDTO.setErrorDescription(ex.getMessage());
                        responseDTO.setSysId(null);
                        serviceNowDAO.updateProactiveUnSuccessTxrResponse(
                                responseDTO, userId, createIdentifier);
                        response.put("message", ex.getMessage());
                        return response;
                    } else {
                        logger.info("Service Now TXR failed attempt number="
                                + counter);
                        logger.error(ex);
                        try {
                            Thread.sleep(attempInterval);
                        } catch (InterruptedException e) {
                            logger.error(ex);
                        }
                    }
                }
            }
            ProactiveIncidentTxrResponseDTO responseDTO = new ProactiveIncidentTxrResponseDTO();
            responseDTO.setStatus(status.value);
            responseDTO.setErrorDescription(errorDescription.value);
            responseDTO.setDisplayValue(displayValue.value);
            responseDTO.setSysId(sysId.value);
            responseDTO.setVTaskNumber(vTaskNumber.value);
            if (responseDTO.getStatus().equalsIgnoreCase("error")) {
                serviceNowDAO.updateProactiveUnSuccessTxrResponse(responseDTO,
                        userId, createIdentifier);
                logger.info(responseDTO.getErrorDescription());
                response.put("message", responseDTO.getErrorDescription());
                return response;
            } else if (responseDTO.getStatus().equalsIgnoreCase("inserted")) {
                serviceNowDAO.updateProactiveSuccessfulTxrResponse(responseDTO,
                        userId, createIdentifier);
            }
        } else if (proactiveIncidentDTO.getMsaId().equalsIgnoreCase("462")) {
            // For CFA
            for (int counter = 1; counter <= attemptCount; counter++) {
                serviceNowDAO.logIntegration(serviceNowDAO.OUTBOUND, serviceNowDAO.INFORMATION_LEVEL, "SOAP send attempt " + counter + ".", "MSA: " + proactiveIncidentDTO.getMsaId());
                try {
                    Map<String, String> params = new HashMap<>();
                    params.put("u_category", proactiveIncidentDTO.getCategory());
                    params.put("u_configuration_item", proactiveIncidentDTO.getConfigurationItem());
                    params.put("u_description", proactiveIncidentDTO.getDescription());
                    params.put("u_short_description", proactiveIncidentDTO.getShortDescription());
                    params.put("u_impact", proactiveIncidentDTO.getImpact());
                    params.put("u_site_number", proactiveIncidentDTO.getSiteDisplayNumber());
                    params.put("u_state", proactiveIncidentDTO.getState());
                    params.put("u_symptom", proactiveIncidentDTO.getSymptom());
                    params.put("u_ticket_number", proactiveIncidentDTO.getTicketNumber());
                    params.put("u_type", proactiveIncidentDTO.getType());
                    params.put("u_urgency", proactiveIncidentDTO.getUrgency());
                    params.put("u_vendor_key", proactiveIncidentDTO.getVendorKey());
                    params.put("u_work_notes", proactiveIncidentDTO.getWorkNotes());

                    output = sendOutboundSoap("cfa_service_now_outbound_create", params, userId);
                    serviceNowDAO.logIntegration(serviceNowDAO.OUTBOUND, serviceNowDAO.INFORMATION_LEVEL, "SOAP response recieved.", output.toString());
                    break;

                    /*ilexIntegrationCreateIncidentService.insert(
                     proactiveIncidentDTO.getCategory(),
                     proactiveIncidentDTO.getConfigurationItem(),
                     proactiveIncidentDTO.getDescription(),
                     proactiveIncidentDTO.getImpact(),
                     proactiveIncidentDTO.getShortDescription(),
                     proactiveIncidentDTO.getSiteNumber(),
                     proactiveIncidentDTO.getState(),
                     proactiveIncidentDTO.getSymptom(),
                     proactiveIncidentDTO.getTicketNumber(),
                     proactiveIncidentDTO.getType(),
                     proactiveIncidentDTO.getUrgency(),
                     proactiveIncidentDTO.getVendorKey(),
                     proactiveIncidentDTO.getWorkNotes(), sysId, table,
                     displayName, displayValue, status, statusMessage,
                     errorMessage, errorNumber, errorDescription,
                     vTaskNumber, ticketNumber);
                     break;*/
                } catch (Exception ex) {
                    if (counter == attemptCount) {
                        logger.info("Service Now CFA failed attempt number=" + counter);
                        logger.error(ex);
                        ProactiveIncidentResponseDTO responseDTO = new ProactiveIncidentResponseDTO();
                        responseDTO.setErrorDescription(ex.getMessage());
                        responseDTO.setSysId(null);
                        serviceNowDAO.updateProactiveUnSuccessResponse(
                                responseDTO, userId, createIdentifier);
                        response.put("message", ex.getMessage());
                        return response;
                    } else {
                        logger.info("Service Now CFA failed attempt number=" + counter);
                        logger.error(ex);
                        try {
                            Thread.sleep(attempInterval);
                        } catch (InterruptedException e) {
                            logger.error(ex);
                        }
                    }
                }
            }
            ProactiveIncidentResponseDTO responseDTO = new ProactiveIncidentResponseDTO();
            responseDTO.setStatus((output.containsKey("status") ? output.get("status") : ""));
            responseDTO.setErrorDescription((output.containsKey("error_description") ? output.get("error_description") : ""));
            responseDTO.setTable((output.containsKey("table") ? output.get("table") : ""));
            responseDTO.setDisplayName((output.containsKey("display_name") ? output.get("display_name") : ""));
            responseDTO.setDisplayValue((output.containsKey("display_value") ? output.get("display_value") : ""));
            responseDTO.setSysId((output.containsKey("sys_id") ? output.get("sys_id") : ""));
            responseDTO.setTicketNumber((output.containsKey("ticketNumber") ? output.get("ticketNumber") : ""));
            if (responseDTO.getStatus().equalsIgnoreCase("error")) {
                serviceNowDAO.updateProactiveUnSuccessResponse(responseDTO, userId, createIdentifier);
                logger.info(responseDTO.getErrorDescription());
                response.put("message", responseDTO.getErrorDescription());
                return response;
            } else if (responseDTO.getStatus().equalsIgnoreCase("inserted")) {
                serviceNowDAO.updateProactiveSuccessfulResponse(responseDTO, userId, createIdentifier);
            }
        }
        response.put("message", "success");
        return response;
    }

    @Override
    public Map<String, String> sendOutboundSoap(String soapName, Map<String, String> params, int userId) throws Exception {
        String url = "http://192.168.32.103/development/integration/soap/outbound.php";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "name=" + soapName;

        Iterator it = params.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            urlParameters += "&" + pairs.getKey() + "=" + pairs.getValue();
            it.remove(); // avoids a ConcurrentModificationException
        }

        serviceNowDAO.logIntegration(serviceNowDAO.OUTBOUND, serviceNowDAO.INFORMATION_LEVEL, "SOAP message assembled.", "URL Param: " + urlParameters);

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        Map<String, String> output = new HashMap<>();

        //JSON
        JSONObject json = (JSONObject) JSONSerializer.toJSON(response.toString());
        JSONObject innerResponse = json.getJSONObject("response");
        output.put("sys_id", innerResponse.getString("sys_id"));
        output.put("status", innerResponse.getString("status"));
        output.put("table", innerResponse.getString("table"));
        output.put("display_name", innerResponse.getString("display_name"));

        Object display_values_obj = innerResponse.get("display_value");
        output.put("display_value", display_values_obj.toString());

        if (display_values_obj instanceof JSONArray) {
            JSONArray display_values = (JSONArray) display_values_obj;
            if (display_values.size() > 1) {
                output.put("ticketNumber", display_values.get(1).toString());
            } else {
                output.put("ticketNumber", display_values.get(0).toString());
            }
        } else {
            output.put("ticketNumber", display_values_obj.toString());
        }
        serviceNowDAO.logIntegration(serviceNowDAO.OUTBOUND, serviceNowDAO.INFORMATION_LEVEL, "Customer Ticket " + output.get("ticketNumber") + " created.", "Response: " + output);

        return output;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.logic.ServiceNowLogic#getIncidentsList(java.lang.String,
     * java.lang.String)
     */
    @Override
    public List<IncidentDTO> getIncidentsList(String integrationType,
            String msaId) {
        List<IncidentDTO> incidentDTOList = new ArrayList<>();
        List<IncidentVO> incidentVOList = serviceNowDAO.getIncidentsList(
                integrationType, msaId);
        NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
        incidentDTOList = awareBeanUtilsBean.copyList(IncidentDTO.class,
                incidentVOList);
        for (IncidentDTO incidentDTO : incidentDTOList) {
            incidentDTO.setCreatedDateDisplay(DateUtils.convertDateToString(
                    incidentDTO.getCreatedDate(), "MM/dd HH:mm"));
        }
        return incidentDTOList;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ilex.ws.core.logic.ServiceNowLogic#getTicketStates()
     */
    @Override
    public List<TicketStateDTO> getTicketStates() {
        List<TicketStateVO> ticketStateVOList = serviceNowDAO.getTicketStates();
        List<TicketStateDTO> ticketStateDTOList = new ArrayList<>();

        NullAwareBeanUtilsBean utilsBean = new NullAwareBeanUtilsBean();
        ticketStateDTOList = utilsBean.copyList(TicketStateDTO.class,
                ticketStateVOList);

        return ticketStateDTOList;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ilex.ws.core.logic.ServiceNowLogic#getCreateIncidentErrors()
     */
    @Override
    public List<IncidentDTO> getCreateIncidentErrors() {
        List<IncidentDTO> createIncidentErrorDTOList = new ArrayList<>();
        List<IncidentVO> createIncidentErrorVOList = serviceNowDAO
                .getCreateIncidentErrorLogs();
        NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
        createIncidentErrorDTOList = awareBeanUtilsBean.copyList(
                IncidentDTO.class, createIncidentErrorVOList);
        for (IncidentDTO incidentDTO : createIncidentErrorDTOList) {
            incidentDTO.setCreatedDateDisplay(DateUtils.convertDateToString(
                    incidentDTO.getCreatedDate(), "MM/dd HH:mm"));
        }
        return createIncidentErrorDTOList;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.logic.ServiceNowLogic#createReactiveIncident(com.ilex
     * .ws.core.dto.IncidentDTO)
     */
    @Override
    public IncidentResponseDTO createReactiveIncident(
            IncidentDTO reactiveIncidentDTO) {
        IncidentVO reactiveIncidentVO = new IncidentVO();
        BeanUtils.copyProperties(reactiveIncidentDTO, reactiveIncidentVO);
        // Set Default values -start
        reactiveIncidentVO.setIntegrationType("Reactive");
        if (StringUtils.isNotBlank(reactiveIncidentVO.getConfigurationItem())) {
            if (reactiveIncidentVO.getConfigurationItem().length() >= 100) {
                reactiveIncidentVO.setConfigurationItem(reactiveIncidentVO
                        .getConfigurationItem().substring(0, 100));
            }
        }
        // Set Default values -end
        IncidentResponseDTO incidentResponseDTO = new IncidentResponseDTO();
        String ilexTicketNumber = hdMainDAO.avaliableIlexTicketNumber();
        String siteId = null;
        if (reactiveIncidentDTO.getMsaId().equalsIgnoreCase("740")) {
            // For TXR
            siteId = serviceNowDAO.validateSiteNumberForTXR(reactiveIncidentVO);
        } else if (reactiveIncidentDTO.getMsaId().equalsIgnoreCase("462")) {
            // For CFA
            siteId = serviceNowDAO.validateSiteNumberForCFA(reactiveIncidentVO);
        }
        if (StringUtils.isBlank(siteId)) {
            reactiveIncidentVO.setIntegrationStatus("Error");
            reactiveIncidentVO.setIntegrationStatusMsg("Site "
                    + reactiveIncidentDTO.getSiteNumber()
                    + " is not defined in Contingent site list");
            serviceNowDAO.insertReactiveIncidentToIlex(reactiveIncidentVO,
                    ilexTicketNumber);
            incidentResponseDTO.setSys_id(reactiveIncidentDTO.getVendorKey());
            incidentResponseDTO.setTable("");
            incidentResponseDTO.setDisplay_name("");
            incidentResponseDTO.setDisplay_value("");
            incidentResponseDTO.setStatus("error");
            incidentResponseDTO.setTicketNumber(reactiveIncidentDTO
                    .getTicketNumber());
            incidentResponseDTO.setError_description("");
            incidentResponseDTO.setError_number("2001");
            incidentResponseDTO.setError_message("Site "
                    + reactiveIncidentDTO.getSiteNumber()
                    + " is not defined in Contingent site list");

            return incidentResponseDTO;
        }
        try {
            reactiveIncidentVO.setLpSiteListId(siteId);
            reactiveIncidentVO.setIntegrationStatus("Successful");
            Long incidentCreateIdentifier = serviceNowDAO
                    .insertReactiveIncidentToIlex(reactiveIncidentVO,
                            ilexTicketNumber);
            IncidentVO incidentDetailVO = serviceNowDAO
                    .getIncidentDetail(incidentCreateIdentifier);
            createTicketInIlex(incidentDetailVO, ilexTicketNumber);
            incidentResponseDTO = new IncidentResponseDTO();
            incidentResponseDTO
                    .setSys_id(incidentDetailVO.getServiceNowSysId());
            incidentResponseDTO.setTable("");
            incidentResponseDTO.setDisplay_name("");
            incidentResponseDTO.setDisplay_value("");
            incidentResponseDTO.setStatus("inserted");
            incidentResponseDTO.setTicketNumber(incidentDetailVO.getTicketNumber());
            incidentResponseDTO.setvTaskNumber(incidentDetailVO.getvTaskNumber());
            return incidentResponseDTO;
        } catch (Exception e) {
            logger.error(
                    "Error occurred while creating reactive incident to Ilex for u_ticket_number"
                    + reactiveIncidentDTO.getTicketNumber(), e);
            incidentResponseDTO = new IncidentResponseDTO();
            incidentResponseDTO.setSys_id(reactiveIncidentDTO.getVendorKey());
            incidentResponseDTO.setTable("");
            incidentResponseDTO.setDisplay_name("");
            incidentResponseDTO.setDisplay_value("");
            incidentResponseDTO.setStatus("error");
            incidentResponseDTO.setTicketNumber(reactiveIncidentDTO
                    .getTicketNumber());
            incidentResponseDTO.setError_description("");
            incidentResponseDTO.setError_number("2002");
            incidentResponseDTO.setError_message("Reactive request failed");
            return incidentResponseDTO;
        }
    }

    private void createTicketInIlex(IncidentVO incidentVO,
            String ilexTicketNumber) {
        // Insert in to lm_ticket
        String appendixId = incidentVO.getAppendixId();
        if (checkIntegrationType(appendixId)) {
            Long ticketIdFromLmTicket = serviceNowDAO.insertTicket(
                    ilexTicketNumber, incidentVO.getDescription(),
                    incidentVO.getAppendixId());

            // Insert in to lm_help_desk_ticket_details
            serviceNowDAO.insertHDTicket(incidentVO, ticketIdFromLmTicket);

            // INsert into lm_help_desk_work_notes
            StringBuilder workNotes = new StringBuilder();
            workNotes.append("Ticket created base on ServiceNow request for ");
            workNotes.append(incidentVO.getvTaskNumber());
            workNotes.append("\n");
            workNotes.append("Work Note: ");
            if (StringUtils.isBlank(incidentVO.getWorkNotes())) {
                workNotes.append("None provided");
            } else {
                workNotes.append(incidentVO.getWorkNotes());
            }

            serviceNowDAO.insertWorkNotesFromSnToIlex(
                    ticketIdFromLmTicket.toString(), workNotes.toString(), "0",
                    null);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ilex.ws.core.logic.ServiceNowLogic#getUpdateIncidentErrors()
     */
    @Override
    public List<IncidentDTO> getUpdateIncidentErrors() {
        List<IncidentDTO> updateIncidentErrorDTOList = new ArrayList<>();
        List<IncidentVO> updateIncidentErrorVOList = serviceNowDAO
                .getUpdateIncidentErrorLogs();
        NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
        updateIncidentErrorDTOList = awareBeanUtilsBean.copyList(
                IncidentDTO.class, updateIncidentErrorVOList);
        for (IncidentDTO incidentDTO : updateIncidentErrorDTOList) {
            incidentDTO.setCreatedDateDisplay(DateUtils.convertDateToString(
                    incidentDTO.getCreatedDate(), "MM/dd HH:mm"));
        }
        return updateIncidentErrorDTOList;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.logic.ServiceNowLogic#getIncidentDetail(java.lang.Long)
     */
    @Override
    public IncidentDTO getIncidentDetail(Long createIdentifier) {
        IncidentVO incidentVO = new IncidentVO();
        incidentVO = serviceNowDAO.getIncidentDetail(createIdentifier);
        IncidentDTO incidentDTO = new IncidentDTO();
        BeanUtils.copyProperties(incidentVO, incidentDTO);
        incidentDTO.setCreatedDateDisplay(DateUtils.convertDateToString(
                incidentDTO.getCreatedDate(), "MM/dd HH:mm"));
        return incidentDTO;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.logic.ServiceNowLogic#updateIncidentFromIlextoSN(com
     * .ilex.ws.core.dto.IncidentDTO, int)
     */
    @Override
    public Map<String, String> updateIncidentFromIlextoSN(
            IncidentDTO incidentDTO, int userId) {
        IncidentVO incidentVO = new IncidentVO();
        BeanUtils.copyProperties(incidentDTO, incidentVO);
        // Update Incident to Ilex
        Map<String, Long> createResponse = serviceNowDAO
                .insertIncidentUpdatetRecordToIlex(incidentVO,
                        String.valueOf(userId));
        Long updateIdentifier = createResponse.get("updateIdentifier");
        Map<String, String> response = new HashMap<>();
        response.put("updateIdentifier", String.valueOf(updateIdentifier));
        Holder<String> sysId = new Holder<>();
        Holder<String> table = new Holder<>();
        Holder<String> displayName = new Holder<>();
        Holder<String> displayValue = new Holder<>();
        Holder<String> status = new Holder<>();
        Holder<String> statusMessage = new Holder<>();
        Holder<String> errorMessage = new Holder<>();
        Holder<String> errorNumber = new Holder<>();
        Holder<String> errorDescription = new Holder<>();
        Holder<String> vTaskNumber = new Holder<>();
        Holder<String> ticketNumber = new Holder<>();

        Map<String, String> output = new HashMap<>();

        // Update Incident from Ilex to ServiceNow
        // check whether create incident service is for CFA or TXR
        if (incidentDTO.getMsaId().equalsIgnoreCase("740")) {
            // For TXR
            for (int counter = 1; counter <= attemptCount; counter++) {
                try {
                    updateIncidentTxrService.insert(incidentDTO.getState(),
                            incidentDTO.getTicketNumber(),
                            incidentDTO.getVendorKey(),
                            incidentDTO.getvTaskNumber(),
                            incidentDTO.getWorkNotes(), sysId, table,
                            displayName, displayValue, status, statusMessage,
                            errorMessage, errorNumber, errorDescription,
                            vTaskNumber, ticketNumber);
                    break;

                } catch (Exception ex) {
                    if (counter == attemptCount) {
                        logger.info("Ilex to Service now TXR update failed attempt number="
                                + counter);
                        logger.error(ex);
                        UpdateIncidentTxrResponseDTO responseDTO = new UpdateIncidentTxrResponseDTO();
                        responseDTO.setErrorDescription(ex.getMessage());
                        serviceNowDAO.setIncidentUpdateUnsuccessTxrResponse(
                                responseDTO, userId, updateIdentifier);
                        response.put("message", ex.getMessage());
                        return response;
                    } else {
                        logger.info("Ilex to Service now TXR update failed attempt number="
                                + counter);
                        logger.error(ex);
                        try {
                            Thread.sleep(attempInterval);
                        } catch (InterruptedException e) {
                            logger.error(ex);
                        }
                    }
                }
            }
            UpdateIncidentTxrResponseDTO responseDTO = new UpdateIncidentTxrResponseDTO();
            responseDTO.setStatus(status.value);
            responseDTO.setErrorDescription(errorDescription.value);
            responseDTO.setDisplayValue(displayValue.value);
            responseDTO.setSysId(sysId.value);
            responseDTO.setVTaskNumber(vTaskNumber.value);
            if (responseDTO.getStatus().equalsIgnoreCase("error")) {
                serviceNowDAO.setIncidentUpdateUnsuccessTxrResponse(
                        responseDTO, userId, updateIdentifier);
                logger.info(responseDTO.getErrorDescription());
                response.put("message", responseDTO.getErrorDescription());
                return response;
            } else if (responseDTO.getStatus().equalsIgnoreCase("updated")) {
                serviceNowDAO.setIncidentUpdateSuccessTxrResponse(responseDTO,
                        userId, updateIdentifier);
            }
        } else if (incidentDTO.getMsaId().equalsIgnoreCase("462")) {
            // For CFA
            for (int counter = 1; counter <= attemptCount; counter++) {
                try {
                    Map<String, String> params = new HashMap<>();
                    params.put("u_state", incidentDTO.getState());
                    params.put("u_ticket_number", incidentDTO.getTicketNumber());
                    params.put("u_vendor_key", incidentDTO.getVendorKey());
                    params.put("u_work_notes", incidentDTO.getWorkNotes());
                    params.put("u_vendor_ticket_number", incidentDTO.getvTaskNumber());

                    output = sendOutboundSoap("cfa_service_now_outbound_update", params, userId);
                    break;
                    /*ilexToServicenowUpdateIncidentService.insert(
                     incidentDTO.getState(),
                     incidentDTO.getTicketNumber(),
                     incidentDTO.getVendorKey(),
                     incidentDTO.getTicketNumber(),
                     incidentDTO.getWorkNotes(), sysId, table,
                     displayName, displayValue, status, statusMessage,
                     errorMessage, errorNumber, errorDescription,
                     vTaskNumber, ticketNumber);
                     break;*/

                } catch (Exception ex) {
                    if (counter == attemptCount) {
                        logger.info("Ilex to Service now CFA update failed attempt number="
                                + counter);
                        logger.error(ex);
                        UpdateIncidentResponseDTO responseDTO = new UpdateIncidentResponseDTO();
                        responseDTO.setErrorDescription(ex.getMessage());
                        serviceNowDAO.setIncidentUpdateUnsuccessResponse(
                                responseDTO, userId, updateIdentifier);
                        response.put("message", ex.getMessage());
                        return response;
                    } else {
                        logger.info("Ilex to Service now CFA update failed attempt number="
                                + counter);
                        logger.error(ex);
                        try {
                            Thread.sleep(attempInterval);
                        } catch (InterruptedException e) {
                            logger.error(ex);
                        }
                    }
                }
            }
            UpdateIncidentResponseDTO responseDTO = new UpdateIncidentResponseDTO();
            responseDTO.setStatus((output.containsKey("status") ? output.get("status") : ""));
            responseDTO.setErrorDescription((output.containsKey("error_description") ? output.get("error_description") : ""));
            responseDTO.setTable((output.containsKey("table") ? output.get("table") : ""));
            responseDTO.setDisplayName((output.containsKey("display_name") ? output.get("display_name") : ""));
            responseDTO.setDisplayValue((output.containsKey("display_value") ? output.get("display_value") : ""));
            responseDTO.setSysId((output.containsKey("sys_id") ? output.get("sys_id") : ""));
            responseDTO.setTicketNumber((output.containsKey("ticketNumber") ? output.get("ticketNumber") : ""));
            if (responseDTO.getStatus().equalsIgnoreCase("error")) {
                serviceNowDAO.setIncidentUpdateUnsuccessResponse(responseDTO, userId, updateIdentifier);
                logger.info(responseDTO.getErrorDescription());
                response.put("message", responseDTO.getErrorDescription());
                return response;
            } else if (responseDTO.getStatus().equalsIgnoreCase("updated")) {
                serviceNowDAO.setIncidentUpdateSuccessResponse(responseDTO, userId, updateIdentifier);
            }
        }
        response.put("message", "success");
        return response;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.logic.ServiceNowLogic#getIncidentUpdates(java.lang.Long)
     */
    @Override
    public List<IncidentDTO> getIncidentUpdates(Long createIdentifier) {
        List<IncidentVO> incidentUpdatesVO = serviceNowDAO
                .getIncidentUpdates(createIdentifier);
        List<IncidentDTO> incidentUpdatesDTO = new ArrayList<>();
        NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
        incidentUpdatesDTO = awareBeanUtilsBean.copyList(IncidentDTO.class,
                incidentUpdatesVO);
        for (IncidentDTO incidentDTO : incidentUpdatesDTO) {
            incidentDTO.setCreatedDateDisplay(DateUtils.convertDateToString(
                    incidentDTO.getCreatedDate(), "MM/dd HH:mm"));
        }
        return incidentUpdatesDTO;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.logic.ServiceNowLogic#updateIncidentFromServicenowToIlex
     * (com.ilex.ws.core.dto.IncidentDTO)
     */
    @Override
    public IncidentResponseDTO updateIncidentFromServicenowToIlex(
            IncidentDTO incidentDTOarg) {
        Long createIdentifier = serviceNowDAO.getIncidentCreateIdentifier(incidentDTOarg.getTicketNumber(), incidentDTOarg.getTicketNumber());
        IncidentResponseDTO incidentResponseDTO = new IncidentResponseDTO();
        if (createIdentifier == null) {
            incidentResponseDTO.setSys_id(incidentDTOarg.getServiceNowSysId());
            incidentResponseDTO.setTable("");
            incidentResponseDTO.setDisplay_name("");
            incidentResponseDTO.setDisplay_value("");
            incidentResponseDTO.setStatus("error");
            incidentResponseDTO.setTicketNumber(incidentDTOarg
                    .getTicketNumber());
            incidentResponseDTO.setError_description("");
            incidentResponseDTO.setError_number("2010");
            incidentResponseDTO.setError_message("Site "
                    + incidentDTOarg.getTicketNumber() + " and "
                    + incidentDTOarg.getTicketNumber() + " not found.");

            return incidentResponseDTO;
        }
        if (serviceNowDAO.checkStateMarkedResolved(createIdentifier)) {
            incidentResponseDTO.setSys_id(incidentDTOarg.getServiceNowSysId());
            incidentResponseDTO.setTable("");
            incidentResponseDTO.setDisplay_name("");
            incidentResponseDTO.setDisplay_value("");
            incidentResponseDTO.setStatus("error");
            incidentResponseDTO.setTicketNumber(incidentDTOarg
                    .getTicketNumber());
            incidentResponseDTO.setError_description("");
            incidentResponseDTO.setError_number("2011");
            incidentResponseDTO.setError_message("Site "
                    + incidentDTOarg.getTicketNumber() + " and "
                    + incidentDTOarg.getTicketNumber()
                    + " marked Resolved. No further updates allowed.");

            return incidentResponseDTO;
        }

        try {
            IncidentVO incidentUpdateVO = new IncidentVO();
            BeanUtils.copyProperties(incidentDTOarg, incidentUpdateVO);
            // set values for update record
            incidentUpdateVO.setCreateIdentifier(String
                    .valueOf(createIdentifier));
            incidentUpdateVO.setTriggerSource("ServiceNow");
            incidentUpdateVO.setIntegrationStatus("Successful");
            Map<String, Long> createResponse = serviceNowDAO
                    .insertIncidentUpdatetRecordToIlex(incidentUpdateVO, null);
            Long updateIdentifier = createResponse.get("updateIdentifier");
            updateMainIlexIncident(createIdentifier, updateIdentifier);
            IncidentVO incidentDetail = serviceNowDAO
                    .getIncidentDetail(createIdentifier);
            incidentResponseDTO = new IncidentResponseDTO();
            incidentResponseDTO.setSys_id(incidentDTOarg.getServiceNowSysId());
            incidentResponseDTO.setTable("");
            incidentResponseDTO.setDisplay_name("");
            incidentResponseDTO.setDisplay_value("");
            incidentResponseDTO.setStatus("updated");
            incidentResponseDTO.setTicketNumber(incidentDetail
                    .getTicketNumber());
            incidentResponseDTO.setvTaskNumber(incidentDetail.getTicketNumber());
            return incidentResponseDTO;
        } catch (Exception e) {
            logger.error(
                    "Error occured while Incident update from Service Now To Ilex for VTaskNumber="
                    + incidentDTOarg.getTicketNumber()
                    + "and Ticket Number="
                    + incidentDTOarg.getTicketNumber(), e);
            incidentResponseDTO.setSys_id(incidentDTOarg.getServiceNowSysId());
            incidentResponseDTO.setTable("");
            incidentResponseDTO.setDisplay_name("");
            incidentResponseDTO.setDisplay_value("");
            incidentResponseDTO.setStatus("error");
            incidentResponseDTO.setTicketNumber(incidentDTOarg
                    .getTicketNumber());
            incidentResponseDTO.setError_description("");
            incidentResponseDTO.setError_number("2012");
            incidentResponseDTO.setError_message("Request failed for "
                    + incidentDTOarg.getTicketNumber() + " and "
                    + incidentDTOarg.getTicketNumber());
            return incidentResponseDTO;
        }
    }

    @Override
    public boolean checkIntegrationType(String appendixId) {
        boolean activeIntegration = serviceNowDAO.checkIntegrationType(appendixId);
        return activeIntegration;
    }

    @Override
    public IncidentDTO mapTicketDataToIntegTableForCreateIncident(String ticketId) {
        IncidentVO incidentVO = serviceNowDAO.getHdTicketDetailForCreateIncident(ticketId);
        String workNote = serviceNowDAO.getLatestWorkNote(ticketId);
        String siteNumber = serviceNowDAO.getSiteNumber(incidentVO.getSiteNumber());
        if (incidentVO.getMsaId().equalsIgnoreCase("740")) {
            // For TXR
            if (StringUtils.isNotBlank(siteNumber)) {
                siteNumber = siteNumber.replaceAll("\\D+", "");
                siteNumber = siteNumber.replaceAll("^0*", "");
            }
        } else if (incidentVO.getMsaId().equalsIgnoreCase("462")) {
            // New system handle mapping. No need for it here.
            IncidentDTO incidentDTO = new IncidentDTO();
            BeanUtils.copyProperties(incidentVO, incidentDTO);
            return incidentDTO;
            // For CFA
            /*if (StringUtils.isNotBlank(siteNumber)) {
             siteNumber = siteNumber.replaceAll("\\D+", "");
             if (StringUtils.isNotBlank(siteNumber)) {
             int siteNum = Integer.parseInt(siteNumber);
             siteNumber = String.format("%05d", siteNum);
             }
             }*/
        }
        incidentVO.setWorkNotes(workNote);
        incidentVO.setSiteNumber(siteNumber);
        // set default values
        incidentVO.setCategory("Restaurant/Hardware/Connectivity");
        incidentVO.setSymptom("");
        incidentVO.setState("Work in Progress");
        incidentVO.setType("Connectivity");
        incidentVO.setVendorKey("ILEX");
        incidentVO.setIntegrationType("Proactive");
        incidentVO.setIntegrationStatus("Pending");
        IncidentDTO incidentDTO = new IncidentDTO();
        BeanUtils.copyProperties(incidentVO, incidentDTO);
        return incidentDTO;
    }

    @Override
    public void sendEmail(EmailBean emailBeanArg) {
        EmailBean emailBean = new EmailBean();
        emailBean.setContent(emailBeanArg.getContent());
        emailBean.setSubject(emailBeanArg.getSubject());

        emailBean.setContentType("text/html");

        if (StringUtils.isNotBlank(emailUserName)) {
            emailBean.setUsername(emailUserName);
        }
        if (StringUtils.isNotBlank(emailUserPassword)) {
            emailBean.setUserpassword(emailUserPassword);
        }
        emailBean.setSmtpservername(emailSmtpServerName);
        if (StringUtils.isNotBlank(emailSmtpServerPort)) {
            emailBean.setSmtpserverport(emailSmtpServerPort);
        }

        if (StringUtils.isBlank(emailBean.getTo())
                && StringUtils.isNotBlank(servicenowTo)) {
            emailBean.setTo(servicenowTo);
        }
        emailBean.setFrom(servicenowFrom);
        //emailBean, "text/html");
    }

    @Override
    public IncidentDTO mapTicketDataToIntegTableForUpdateIncident(
            String ticketId) {
        IncidentVO incidentVO = serviceNowDAO
                .getHdTicketDetailForUpdateIncident(ticketId);
        String workNote = serviceNowDAO.getLatestWorkNote(ticketId);
        incidentVO.setWorkNotes(workNote);
        // set default values
        incidentVO.setTriggerSource("Ilex");
        incidentVO.setIntegrationStatus("Pending");
        incidentVO.setVendorKey("Ilex");
        IncidentDTO incidentDTO = new IncidentDTO();
        BeanUtils.copyProperties(incidentVO, incidentDTO);
        return incidentDTO;
    }

    @Override
    public HDTicketDTO getSnMessage(Long ticketId) {
        HDTicketVO hdTicketVO = serviceNowDAO.getSnMessage(ticketId);
        HDTicketDTO hdTicketDTO = new HDTicketDTO();
        BeanUtils.copyProperties(hdTicketVO, hdTicketDTO);
        if (hdTicketVO.getExtSystemUpdateDate() != null) {
            hdTicketDTO.setExtSystemUpdate(DateUtils.convertDateToString(
                    hdTicketVO.getExtSystemUpdateDate(), "MM/dd HH:mm"));
        }
        return hdTicketDTO;
    }

    @Override
    public void clearIntegratedTicketStatus(Long ticketId) {
        serviceNowDAO.clearIntegratedTicketStatus(ticketId);

    }

    private void updateMainIlexIncident(Long createIdentifier,
            Long updateIdentifier) {
        IncidentVO incidentDetailVO = serviceNowDAO
                .getIncidentDetail(createIdentifier);
        String appendixId = incidentDetailVO.getAppendixId();
        if (checkIntegrationType(appendixId)) {
            String ticketNumber = incidentDetailVO.getTicketNumber();
            HDTicketVO ticketDetail = serviceNowDAO
                    .getTicketDetailsFromTicketNumber(ticketNumber);
            IncidentVO updateDetailVO = serviceNowDAO
                    .getIncidentUpdateDetail(String.valueOf(updateIdentifier));
            String ticketId = ticketDetail.getTicketId();
            String previousTicketState = ticketDetail.getState();
            String newTicketState = updateDetailVO.getState();
            String newTicketStateId = serviceNowDAO
                    .getTicketStateId(newTicketState);
            String stateChangeMessage = "";
            if ((StringUtils.isBlank(newTicketState))
                    || (newTicketState.equalsIgnoreCase(previousTicketState))) {
                stateChangeMessage = "State change does not apply";
            } else if (StringUtils.isNotBlank(newTicketState)
                    && StringUtils.isBlank(newTicketStateId)) {
                stateChangeMessage = "Illegal state value provided";
            } else {
                serviceNowDAO.updateTicketState(newTicketStateId, ticketId);
                stateChangeMessage = "State changed from "
                        + previousTicketState + " to " + newTicketState;
            }
            // insert work notes
            StringBuilder workNote = new StringBuilder("");
            workNote.append("ServiceNow update for " + incidentDetailVO.getTicketNumber() + "\n");
            workNote.append("State Change: " + stateChangeMessage + "\n");
            if (StringUtils.isNotBlank(updateDetailVO.getWorkNotes())) {
                workNote.append("Work Note: " + updateDetailVO.getWorkNotes());
            } else {
                workNote.append("Work Note: None provided");
            }
            serviceNowDAO.insertWorkNotesFromSnToIlex(ticketId,
                    workNote.toString(), "1", null);
            // Set lm_help_desk_ticket_details Update Flag
            serviceNowDAO.setHdUpdateFlag(ticketId);
            // Update cube status
            boolean isNextActionOverdue = hdMainDAO
                    .getNextActionOverdue(ticketId);
            String cubesToUpdate = null;

            if (isNextActionOverdue) {
                cubesToUpdate = HdCubeDisplayStatus.WIP_OD.getCubeName();
            } else {
                cubesToUpdate = HdCubeDisplayStatus.WIP.getCubeName();
            }
            hdMainDAO.updateCubeDisplayStatus(cubesToUpdate);

        }

    }
}
