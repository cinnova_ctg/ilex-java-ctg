package com.ilex.ws.core.logic.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.MacWorxDetailVO;
import com.ilex.ws.core.bean.MacWorxIntergrationResponseVO;
import com.ilex.ws.core.bean.MacWorxIntergrationVO;
import com.ilex.ws.core.dao.MacWorxDAO;
import com.ilex.ws.core.dto.MacWorxIntergrationDTO;
import com.ilex.ws.core.dto.MacWorxIntergrationResponseDTO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.logic.MacWorxLogic;
import com.ilex.ws.core.util.MessageKeyConstant;
import com.mind.bean.docm.Document;
import com.mind.bean.docm.Field;
import com.mind.bean.docm.MetaData;

@Component
public class MacWorxLogicImpl implements MacWorxLogic {
	@Resource
	MacWorxDAO macWorxDAO;
	@Value("#{ilexProperties['mobile.upload.image.max.size']}")
	private String imageMaxSize;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.MACWorXLogic#macWorxIntergration(com.ilex.ws.core
	 * .dto.MacWorxIntergrationDTO, org.apache.commons.fileupload.FileItem)
	 */
	@Override
	public MacWorxIntergrationResponseDTO macWorxIntergration(
			MacWorxIntergrationDTO macWorxIntergrationDTO, FileItem fileItem) {

		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxNonPps())) {
			macWorxIntergrationDTO.setMacxNonPps("No");
		}

		if (StringUtils.isEmpty(macWorxIntergrationDTO
				.getMacSpecialInstructions())) {
			macWorxIntergrationDTO.setMacSpecialInstructions("No");
		}
		validateRequest(macWorxIntergrationDTO);

		String address = macWorxIntergrationDTO.getMacxAddress1();
		String city = macWorxIntergrationDTO.getMacxCity();
		String state = macWorxIntergrationDTO.getMacxState();
		String zipcode = macWorxIntergrationDTO.getMacxZipCode();
		String country = "US";
		String[] latLong = macWorxDAO.getLatitudeLongitude(address, city,
				state, country, zipcode);
		MacWorxIntergrationVO macWorxIntergrationVO = new MacWorxIntergrationVO();
		BeanUtils.copyProperties(macWorxIntergrationDTO, macWorxIntergrationVO);
		macWorxIntergrationVO.setLatitude(latLong[3]);
		macWorxIntergrationVO.setLatLongAccuracy(latLong[0]);
		macWorxIntergrationVO.setLatLongSource(latLong[1]);
		macWorxIntergrationVO.setLongitude(latLong[2]);
		MacWorxIntergrationResponseVO macWorxIntergrationResponseVO = new MacWorxIntergrationResponseVO();
		macWorxIntergrationResponseVO = macWorxDAO
				.createJobForMegaPath(macWorxIntergrationVO);

		MacWorxIntergrationResponseDTO macWorxIntergrationResponseDTO = new MacWorxIntergrationResponseDTO();
		BeanUtils.copyProperties(macWorxIntergrationResponseVO,
				macWorxIntergrationResponseDTO);
		if (macWorxIntergrationResponseVO.getStatus() != 1) {

			return macWorxIntergrationResponseDTO;
		}
		if (fileItem != null && fileItem.getSize() > 0) {
			if (fileItem.getSize() > Long.valueOf(imageMaxSize)) {
				throw new IlexBusinessException(
						MessageKeyConstant.IMAGE_SIZE_NOT_VALID);
			}
			Document doc = new Document();
			doc.setFileBytes(fileItem.get());
			doc.setFileName(fileItem.getName());
			MetaData metaData = getmetaData(fileItem,
					macWorxIntergrationResponseVO);
			doc.setMetaData(metaData);

			int docId = macWorxDAO.uploadFile(doc);
			if (docId == 0) {
				macWorxIntergrationResponseDTO.setStatus(2);
			} else {
				macWorxDAO.insertDocEntryToPoDocument(
						macWorxIntergrationResponseVO.getPoId(), docId,
						macWorxIntergrationDTO.getMacxRequestor());
				macWorxIntergrationResponseDTO.setStatus(1);
			}
		}

		return macWorxIntergrationResponseDTO;
	}

	public MetaData getmetaData(FileItem file,
			MacWorxIntergrationResponseVO macWorxIntergrationResponseVO) {
		MacWorxDetailVO macWorxDetailVO = macWorxDAO.getMetaDataForMacWorx(
				String.valueOf(macWorxIntergrationResponseVO.getJobId()),
				String.valueOf(macWorxIntergrationResponseVO.getPoId()));
		String msaName = macWorxDetailVO.getMsaName();
		String appendixName = macWorxDetailVO.getMsaId();
		String jobName = macWorxDetailVO.getJobName();
		String msaId = macWorxDetailVO.getMsaId();
		String appendixId = macWorxDetailVO.getAppendixId();
		String jobid = String.valueOf(macWorxIntergrationResponseVO.getJobId());
		String poId = String.valueOf(macWorxIntergrationResponseVO.getPoId());
		String partnerName = macWorxDetailVO.getPartnerName();
		String jobOwnerName = macWorxDAO
				.getJobOwnerName(macWorxIntergrationResponseVO.getJobId());

		Map<String, String> map = new HashMap<String, String>();
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = formatter.format(currentDate.getTime());
		Calendar archiveDay = Calendar.getInstance();
		archiveDay.add(Calendar.YEAR, 1);

		String archiveDate = formatter.format(archiveDay.getTime());
		if (file.getName().lastIndexOf(".") != -1) {
			map.put(MACWorXFieldEnum.TITLE.getKey(),
					file.getName()
							.substring(0, file.getName().lastIndexOf(".")));
		}
		map.put(MACWorXFieldEnum.THROUGH_PO.getKey(), "PO");
		map.put(MACWorXFieldEnum.DOCUMENT_DATE.getKey(), dateNow);
		map.put(MACWorXFieldEnum.TYPE.getKey(), "Engineering Support");
		map.put(MACWorXFieldEnum.STATUS.getKey(), "Approved");
		map.put(MACWorXFieldEnum.AUTHOR.getKey(), "");
		map.put(MACWorXFieldEnum.CATEGORY.getKey(), "Contract");
		map.put(MACWorXFieldEnum.SOURCE.getKey(), "Ilex");
		map.put(MACWorXFieldEnum.ADD_DATE.getKey(), dateNow);
		map.put(MACWorXFieldEnum.ADD_USER.getKey(), jobOwnerName);
		map.put(MACWorXFieldEnum.ADD_METHOD.getKey(), "Explicit");
		map.put(MACWorXFieldEnum.CHANGE_CONTROL_TYPE.getKey(), "0");
		map.put(MACWorXFieldEnum.PREVOIUS_DOCUMENT_REFERENCE.getKey(), "");
		map.put(MACWorXFieldEnum.VIEWABLE_ON_WEB.getKey(), "1");
		map.put(MACWorXFieldEnum.ARCHIVE_DATE.getKey(), archiveDate.toString());
		map.put(MACWorXFieldEnum.BASE_DOCUMENT.getKey(), "");
		map.put(MACWorXFieldEnum.APPLICATION.getKey(), "FileType");
		map.put(MACWorXFieldEnum.Size.getKey(), String.valueOf(file.getSize()));
		if (file.getName().lastIndexOf(".") != -1) {
			map.put(MACWorXFieldEnum.MIME.getKey(),
					file.getName().substring(
							file.getName().lastIndexOf(".") + 1));
		}

		map.put(MACWorXFieldEnum.MSA_ID.getKey(), msaId);
		map.put(MACWorXFieldEnum.APPENDIX_ID.getKey(), appendixId);
		map.put(MACWorXFieldEnum.JOB_ID.getKey(), jobid);
		map.put(MACWorXFieldEnum.PO_ID.getKey(), poId);
		map.put(MACWorXFieldEnum.WO_ID.getKey(), poId);
		map.put(MACWorXFieldEnum.CHANGE_CONTROL_ID.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_START_DATE.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_END_DATE.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_CRITICALITY.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_STATUS.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_REQUESTOR.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_ARR_OPTIONS.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_MSP.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_SMRY_NMX_FLT_HELP_DESK.getKey(), "");
		map.put(MACWorXFieldEnum.LINK_TO_OTHER_DOC_ID.getKey(), "");
		map.put(MACWorXFieldEnum.MSA_NAME.getKey(), msaName);
		map.put(MACWorXFieldEnum.APPENDIX_NAME.getKey(), appendixName);
		map.put(MACWorXFieldEnum.JOB_NAME.getKey(), jobName);
		map.put(MACWorXFieldEnum.PARTNER_NAME.getKey(), partnerName);
		map.put(MACWorXFieldEnum.WO_PARTNER_ID.getKey(), "");
		map.put(MACWorXFieldEnum.DTL_NEDX_ID_NAME.getKey(), "");
		map.put(MACWorXFieldEnum.COMMENT.getKey(), "");
		MetaData metaData = new MetaData();
		Field[] fieldList = getFieldList(map);
		metaData.setFields(fieldList);
		return metaData;
	}

	public Field[] getFieldList(Map<String, String> map) {
		Field[] fieldList = new Field[map.size()];
		int i = 0;
		for (Map.Entry<String, String> entry : map.entrySet()) {

			Field field = new Field();
			field.setName(entry.getKey());
			field.setValue(entry.getValue());
			fieldList[i] = field;
			i++;
		}
		return fieldList;

	}

	/**
	 * Validate request.
	 * 
	 * @param macWorxIntergrationDTO
	 *            the mac worx intergration dto
	 */
	private void validateRequest(MacWorxIntergrationDTO macWorxIntergrationDTO) {
		if (macWorxIntergrationDTO.getMacxProjectId() == null) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_PROJECT_ID_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxRequestor())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_REQUESTOR_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxSiteNumber())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_SITE_NUMBER_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxSiteLocation())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_SITE_LOCATION_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxAddress1())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_ADDRESS1_NULL);
		}

		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxCity())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_CITY_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxState())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_STATE_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxZipCode())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_ZIPCODE_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxCriticality())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_CRITICALITY_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxScheduleDate())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_SCHEDULE_DATE_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxScheduleTime())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_SCHEDULE_TIME_NULL);
		}
		if ((!macWorxIntergrationDTO.getMacSpecialInstructions()
				.equalsIgnoreCase("Yes"))
				&& (!macWorxIntergrationDTO.getMacSpecialInstructions()
						.equalsIgnoreCase("No"))) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORK_SPEC_INSTR);

		}
		if ((!macWorxIntergrationDTO.getMacxNonPps().equalsIgnoreCase("Yes"))
				&& (!macWorxIntergrationDTO.getMacxNonPps().equalsIgnoreCase(
						"No"))) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORK_SPEC_INSTR);

		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxPocName1())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_POC_NAME_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxPocPhone1())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_POC_PHONE_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxActivityList())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_ACTIVITY_LIST_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxActivityQty())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_ACTIVITY_QTY_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxInstallNotes())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_INSTALLATION_NOTES_NULL);
		}
		if (StringUtils.isEmpty(macWorxIntergrationDTO.getMacxPoNumber())) {
			throw new IlexBusinessException(
					MessageKeyConstant.MACWORX_PO_NUMBER_NULL);
		}

	}

	@Override
	public int macWorxUserAuth(String userName, String password, int prId) {
		return macWorxDAO.macWorxUserAuth(userName, password, prId);
	}

}
