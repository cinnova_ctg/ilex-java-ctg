package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.LabelValueDTO;
import com.ilex.ws.core.dto.SiteDetailDTO;
import com.ilex.ws.core.dto.SiteSearchDTO;
import com.ilex.ws.core.dto.SiteSearchDetailDTO;
import com.mind.bean.pm.SiteBean;

public interface SiteSearchLogic {
	String getEndCustomer(String appendixId);

	int getMsaId(String appendixId);

	List<LabelValueDTO> getSiteEndCustomerList(String msaId);

	List<LabelValueDTO> getGroupNameListSiteSearch(String msaId);

	int updateSite(SiteBean siteform, String loginuserid);

	SiteSearchDTO getSiteInfo(SiteSearchDTO siteSearchDTO);

	List<SiteSearchDetailDTO> getSearchResult(SiteSearchDTO siteInfo);

	List<LabelValueDTO> getSiteStateList(String countryId, String msaId);

	SiteDetailDTO getSiteDetails(String jobId);

	void assignSite(String siteListId, String siteId, String jobId,
			String userId);

}
