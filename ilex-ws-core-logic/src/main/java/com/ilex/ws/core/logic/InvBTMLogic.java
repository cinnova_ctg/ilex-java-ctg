package com.ilex.ws.core.logic;

import com.ilex.ws.core.dto.MasterBTMDTO;

/**
 * The Interface InvBTMLogic.
 */
public interface InvBTMLogic {

	/**
	 * Insert btm record into DB.
	 * 
	 * @param masterBTMDTO
	 *            the master btmdto
	 */
	void insertBTMRecord(MasterBTMDTO masterBTMDTO);


}
