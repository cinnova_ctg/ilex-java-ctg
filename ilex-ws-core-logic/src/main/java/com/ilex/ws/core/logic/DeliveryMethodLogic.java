package com.ilex.ws.core.logic;

/**
 * The Interface DeliveryMethodLogic.
 */
public interface DeliveryMethodLogic {

	/**
	 * Gets the delivery method.
	 * 
	 * @param deliveryMethod
	 *            the delivery method
	 * @return the delivery method
	 */
	public String getDeliveryMethod(int deliveryMethod);

}
