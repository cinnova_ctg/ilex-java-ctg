package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.InvoiceJobDetailDTO;
import com.ilex.ws.core.dto.JobInformationDTO;
import com.ilex.ws.core.dto.LabelValue;
import com.ilex.ws.core.dto.PartnerDetailDTO;
import com.ilex.ws.core.dto.SchedulerDTO;
import com.mind.bean.prm.ViewInstallationNotesBean;
import com.mind.bean.pvs.Partner_SearchBean;
import com.mind.dao.PRM.CustomerInfoBean;

/**
 * The Interface DashportJobActionLogic.
 */
public interface DashportJobActionLogic {

	/**
	 * Gets the job info.
	 * 
	 * @param invoiceJobDetailDTO
	 *            the invoice job detail dto
	 * @param jobId
	 *            the job id
	 * @return the job info
	 */
	InvoiceJobDetailDTO getJobInfo(InvoiceJobDetailDTO invoiceJobDetailDTO,
			String jobId);

	/**
	 * Gets the scheduled detail.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the scheduled detail
	 */
	List<InvoiceJobDetailDTO> getScheduledDetail(String jobId);

	/**
	 * Gets the invoice activity resource list.
	 * 
	 * @param type
	 *            the type
	 * @param jobId
	 *            the job id
	 * @return the invoice act resource list
	 */
	List<InvoiceJobDetailDTO> getInvoiceActResourceList(String type,
			String jobId);

	/**
	 * Adds the installtion notes.
	 * 
	 * @param jobId
	 *            the job id
	 * @param installationNotes
	 *            the installation notes
	 * @param userId
	 *            the user id
	 * @return the string
	 */
	String addInstalltionNotes(String jobId, String installationNotes,
			String userId);

	/**
	 * Sets the schedule dates.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the scheduler dto
	 */
	SchedulerDTO setScheduleDates(String jobId);

	/**
	 * Save scheduler info.
	 * 
	 * @param schedulerDTO
	 *            the scheduler dto
	 * @param userId
	 *            the user id
	 * @return the string
	 */
	String saveSchedulerInfo(SchedulerDTO schedulerDTO, String userId);

	/**
	 * Update job category priority.
	 * 
	 * @param jobInformationDTO
	 *            the job information dto
	 * @param usedId
	 *            the used id
	 * @return the string
	 */
	String updateJobCategoryPriority(JobInformationDTO jobInformationDTO,
			String usedId);

	/**
	 * Gets the job category priority.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the job category priority
	 */
	public JobInformationDTO getJobCategoryPriority(String jobId);

	/**
	 * Save re scheduled info.
	 * 
	 * @param schedulerDTO
	 *            the scheduler dto
	 * @param userId
	 *            the user id
	 * @return the string
	 */
	String saveReScheduledInfo(SchedulerDTO schedulerDTO, String userId);

	/**
	 * Gets the jO list.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @return the jO list
	 */
	List<LabelValue> getJOList(String appendixId);

	/**
	 * Update job owner.
	 * 
	 * @param jobId
	 *            the job id
	 * @param newJobOwnerId
	 *            the new job owner id
	 * @return the string
	 */
	String updateJobOwner(int jobId, int newJobOwnerId);

	/**
	 * Checks if is job valid for change.
	 * 
	 * @param jobId
	 *            the job id
	 * @param cubeLastRefreshTime
	 *            the cube last refresh time
	 * @return the string
	 */
	String isJobValidForChange(String jobId, String cubeLastRefreshTime);

	/**
	 * Ger customer field data.
	 * 
	 * @param jobId
	 *            the job id
	 * @param appendixId
	 *            the appendix id
	 * @return the array list
	 */
	public List<CustomerInfoBean> gerCustomerFieldData(String jobId,
			String appendixId);

	/**
	 * Check for snapon.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @return true, if successful
	 */
	boolean checkForSnapon(String appendixId);

	/**
	 * Update customerfield info.
	 * 
	 * @param jobId
	 *            the job id
	 * @param appendixCustomerIds
	 *            the appendix customer ids
	 * @param custmereValues
	 *            the custmere values
	 * @param loginuserid
	 *            the loginuserid
	 * @return the string
	 */
	String updateCustomerFiledInfo(String jobId, String appendixCustomerIds,
			String custmereValues, String loginuserid);

	/**
	 * Gets the installation notes list.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the installation notes list
	 */
	List<ViewInstallationNotesBean> getInstallationNotesList(String jobId);

	/**
	 * Gets the partners search list.
	 * 
	 * @param zipcode
	 *            the zipcode
	 * @return the partners search list
	 */
	List<Partner_SearchBean> getpartnersSearchList(String zipcode);

	/**
	 * Gets the partner details.
	 * 
	 * @param partnerDetailDTO
	 *            the partner detail dto
	 * @return the partner details
	 */
	String getPartnerDetails(PartnerDetailDTO partnerDetailDTO, String url);
}
