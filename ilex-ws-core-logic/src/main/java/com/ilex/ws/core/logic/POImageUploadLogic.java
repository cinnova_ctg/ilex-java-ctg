package com.ilex.ws.core.logic;

import org.apache.commons.fileupload.FileItem;

import com.ilex.ws.core.dto.POImageUploadDTO;
import com.ilex.ws.core.dto.POImageUploadRequirementDTO;

/**
 * The Interface POImageUploadLogic.
 */
public interface POImageUploadLogic {

	/**
	 * Gets the pO image upload requirement.
	 * 
	 * @param poId
	 *            the po id
	 * @param authenticationString
	 *            the authentication string
	 * @return the pO image upload requirement
	 */
	public POImageUploadRequirementDTO getPOImageUploadRequirement(
			final String poId, final String authenticationString);

	/**
	 * Upload deliverable image for The po.
	 * 
	 * @param poImageUploadDTO
	 *            the po image upload dto
	 * @param imageStream
	 *            the image stream
	 * @return true, if successful
	 */
	public boolean poImageUpload(final POImageUploadDTO poImageUploadDTO,
			FileItem fileItem);

}
