package com.ilex.ws.core.logic.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.ws.Holder;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.IlexTicketVO;
import com.ilex.ws.core.bean.ServiceNowTicketDetailVO;
import com.ilex.ws.core.bean.ServiceNowTicketVO;
import com.ilex.ws.core.dao.DemoServiceNowIntegrationDAO;
import com.ilex.ws.core.dto.IlexTicketCreateInputDTO;
import com.ilex.ws.core.dto.IlexTicketDTO;
import com.ilex.ws.core.dto.ServiceNowTicketDTO;
import com.ilex.ws.core.logic.DemoServiceNowIntegrationLogic;
import com.ilex.ws.core.util.HelpDeskTicketType;
import com.ilex.ws.servicenow.dto.ResponseDTO;
import com.ilex.ws.servicenow.service.IlexIntegrationService;
import com.ilex.ws.servicenow.service.incident.ServicenowIncidentService;
import com.ilex.ws.servicenow.service.journalFields.GetRecordsResponse.GetRecordsResult;
import com.ilex.ws.servicenow.service.journalFields.ServicenowJournalFieldService;

@Component
public class DemoServiceNowIntegrationLogicImpl implements
		DemoServiceNowIntegrationLogic {

	@Resource
	DemoServiceNowIntegrationDAO serviceNowIntegrationDAO;

	@Resource
	IlexIntegrationService incidentClient;

	@Resource
	ServicenowJournalFieldService servicenowJournalFieldServiceClient;

	@Resource
	ServicenowIncidentService servicenowIncidentServiceClient;

	@Override
	public String insertTicket(ServiceNowTicketDTO serviceNowTicketDTO) {

		ServiceNowTicketVO serviceNowTicketVO = new ServiceNowTicketVO();

		BeanUtils.copyProperties(serviceNowTicketDTO, serviceNowTicketVO);
		try {
			int ticketId = serviceNowIntegrationDAO
					.insertTicketToIlex(serviceNowTicketVO);

			Holder<String> sysid = new Holder<>();
			Holder<String> table = new Holder<>();
			Holder<String> display_name = new Holder<>();
			Holder<String> display_value = new Holder<>();
			Holder<String> status = new Holder<>();
			Holder<String> statusMessage = new Holder<>();
			Holder<String> errorMessage = new Holder<>();

			try {
				incidentClient.insert(serviceNowTicketDTO.getActive(),
						serviceNowTicketDTO.getCategory(),
						serviceNowTicketDTO.getCausedBy(),
						serviceNowTicketDTO.getIncidentState(),
						serviceNowTicketDTO.getShortDescription(),
						String.valueOf(ticketId), sysid, table, display_name,
						display_value, status, statusMessage, errorMessage);

			} catch (Exception ex) {
				serviceNowIntegrationDAO.updateTicketStatus(false, ticketId);
				return "Service Now Ticket Creation Failed";
			}

			ResponseDTO responseDTO = new ResponseDTO();

			responseDTO.setDisplayValue(display_value.value);
			responseDTO.setSysId(sysid.value);

			serviceNowIntegrationDAO.insertServiceNowResponse(ticketId,
					responseDTO);
			serviceNowIntegrationDAO.updateTicketStatus(true, ticketId);

		} catch (Exception ex) {
			return ex.getMessage();

		}
		return "Ticket SuccessFully Created";

	}

	@Override
	public List<IlexTicketDTO> getIncidentTicketDetails() {

		List<IlexTicketVO> serviceNowTickets = serviceNowIntegrationDAO
				.getTicketDetails(HelpDeskTicketType.ILEX_TICKET);
		List<IlexTicketDTO> tickets = new ArrayList<>();

		for (IlexTicketVO serviceNowTicket : serviceNowTickets) {
			IlexTicketDTO ilexTicket = new IlexTicketDTO();

			Holder<List<GetRecordsResult>> holderRs = new Holder<>();

			servicenowJournalFieldServiceClient.getRecords("comments",
					serviceNowTicket.getServiceNowSysId(), holderRs);

			List<GetRecordsResult> rs = holderRs.value;
			StringBuilder comments = new StringBuilder();
			if (rs != null) {
				for (GetRecordsResult responseResult : rs) {

					comments = comments.append(responseResult.getValue())
							.append("<br/>");

				}
				comments = new StringBuilder(comments.toString().replaceAll(
						"(\r\n|\n\r|\r|\n)", "<br />"));
				ilexTicket.setComments(comments.substring(0,
						comments.length() - 5));
			} else {
				ilexTicket.setComments("");
			}

			servicenowJournalFieldServiceClient.getRecords("work_notes",
					serviceNowTicket.getServiceNowSysId(), holderRs);

			rs = holderRs.value;
			StringBuilder workNotes = new StringBuilder();
			if (rs != null) {
				for (GetRecordsResult responseResult : rs) {

					workNotes = workNotes.append(responseResult.getValue())
							.append("<br/>");

				}

				workNotes = new StringBuilder(workNotes.toString().replaceAll(
						"(\r\n|\n\r|\r|\n)", "<br />"));

				ilexTicket.setWorkNotes(workNotes.substring(0,
						workNotes.length() - 5));
			} else {
				ilexTicket.setWorkNotes("");

			}

			String ticketNumber = serviceNowTicket.getServiceNowTicketId();
			Holder<List<com.ilex.ws.servicenow.service.incident.GetRecordsResponse.GetRecordsResult>> recordsResult = new Holder<>();
			servicenowIncidentServiceClient.getRecords(ticketNumber,
					recordsResult);
			ilexTicket.setServiceNowTicketId(serviceNowTicket
					.getServiceNowTicketId());

			List<com.ilex.ws.servicenow.service.incident.GetRecordsResponse.GetRecordsResult> incidentResponseList = recordsResult.value;

			if (incidentResponseList != null) {
				com.ilex.ws.servicenow.service.incident.GetRecordsResponse.GetRecordsResult response = incidentResponseList
						.get(0);
				ilexTicket.setCategory(response.getCategory());
				ilexTicket.setShortDescription(response.getShortDescription());
			}

			tickets.add(ilexTicket);
		}

		return tickets;
	}

	@Override
	public String createServiceNowTicket(
			IlexTicketCreateInputDTO ilexTicketCreateInputDTO) {

		ServiceNowTicketVO serviceNowTicketVO = new ServiceNowTicketVO();
		serviceNowTicketVO.setActive(ilexTicketCreateInputDTO.getActive());
		serviceNowTicketVO.setCategory(ilexTicketCreateInputDTO.getCategory());
		serviceNowTicketVO.setIncidentState(ilexTicketCreateInputDTO
				.getIncidentState());
		serviceNowTicketVO.setShortDescription(ilexTicketCreateInputDTO
				.getShortDescription());
		serviceNowTicketVO.setServiceNowTicket(true);
		int ticketId = serviceNowIntegrationDAO
				.insertTicketToIlex(serviceNowTicketVO);

		ResponseDTO responseDTO = new ResponseDTO();
		responseDTO.setSysId(ilexTicketCreateInputDTO.getSysId());
		responseDTO.setDisplayValue(ilexTicketCreateInputDTO.getTicketNumber());
		serviceNowIntegrationDAO
				.insertServiceNowResponse(ticketId, responseDTO);
		serviceNowIntegrationDAO.updateTicketStatus(true, ticketId);

		return String.valueOf(ticketId);
	}

	@Override
	public List<IlexTicketDTO> getServiceNowTicketDetails() {

		List<IlexTicketVO> serviceNowTickets = serviceNowIntegrationDAO
				.getTicketDetails(HelpDeskTicketType.SERVICE_NOW_TICKET);
		List<IlexTicketDTO> tickets = new ArrayList<>();

		for (IlexTicketVO serviceNowTicket : serviceNowTickets) {
			IlexTicketDTO ticket = new IlexTicketDTO();
			tickets.add(ticket);
			String ticketNumber = serviceNowTicket.getServiceNowTicketId();
			ticket.setServiceNowTicketId(ticketNumber);
			ticket.setShortDescription(serviceNowTicket.getShortDescription());
			ticket.setCategory(serviceNowTicket.getCategory());

			ServiceNowTicketDetailVO commentDetails = serviceNowIntegrationDAO
					.getServiceNowComments(ticketNumber);

			StringBuilder workNotes = new StringBuilder(commentDetails
					.getWorkNotes().replaceAll("(\r\n|\n\r|\r|\n)", "<br />"));
			StringBuilder comments = new StringBuilder(commentDetails
					.getComments().replaceAll("(\r\n|\n\r|\r|\n)", "<br />"));
			ticket.setComments(comments.toString());
			ticket.setWorkNotes(workNotes.toString());

		}

		return tickets;

	}

	@Override
	public void addComment(String comments, String workNotes,
			String ticketNumber) {
		serviceNowIntegrationDAO.addComment(comments, workNotes, ticketNumber);

	}
}
