package com.ilex.ws.core.logic.impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.WugOutInProcessVO;
import com.ilex.ws.core.dao.WugOutageCloseActionsDAO;
import com.ilex.ws.core.logic.WugErrorLogInsertLogic;
import com.ilex.ws.core.logic.WugOutageCloseActionsLogic;

@Component
public class WugOutageCloseActionsLogicImpl implements
		WugOutageCloseActionsLogic {
	public static final Logger logger = Logger
			.getLogger(WugOutageCloseActionsLogicImpl.class);
	@Resource
	WugOutageCloseActionsDAO wugOutageCloseActionsDAO;
	@Resource
	WugErrorLogInsertLogic wugErrorLogInsertLogic;

	@Override
	public String wugOutageCloseAction(String nActiveMonitorStateChangeID,
			String wugIpIssueOwner, String wugIpRootCause,
			String wugIpCorrectiveAction, String wugIpEffortMinutes,
			String wugInstance, String wugBackupStatus) {

		WugOutInProcessVO wugOutInProcessVO = wugOutageCloseActionsDAO
				.getDataFromInProcess(nActiveMonitorStateChangeID, wugInstance);
		wugOutInProcessVO
				.setnActiveMonitorStateChangeID(nActiveMonitorStateChangeID);
		wugOutInProcessVO.setWugInstance(wugInstance);
		wugOutInProcessVO.setWugBackupStatus(wugBackupStatus);
		wugOutInProcessVO.setCorrectiveAction(wugIpCorrectiveAction);
		wugOutInProcessVO.setEffortMinutes(wugIpEffortMinutes);
		wugOutInProcessVO.setIssueOwner(wugIpIssueOwner);
		wugOutInProcessVO.setRootCouse(wugIpRootCause);

		int eventTest = wugOutageCloseActionsDAO.getEventTestCount(
				nActiveMonitorStateChangeID, wugInstance);
		if (eventTest == 0) {
			try {
				wugOutageCloseActionsDAO
						.insertWugOutClosedRecord(wugOutInProcessVO);
			} catch (Exception e) {
				logger.error(e);
				String errorData = "Insert into wug_outages_incidents_closed for "
						+ nActiveMonitorStateChangeID;
				wugErrorLogInsertLogic.insertErrorLog("Error: No Action",
						errorData, "6");
				return "0";

			}
			try {
				wugOutageCloseActionsDAO.insertWugOutClosedHistoryRecord(
						nActiveMonitorStateChangeID, wugInstance);
			} catch (Exception e) {
				String errorData = "Insert into wug_outages_incidents_closed_history for "
						+ nActiveMonitorStateChangeID;
				wugErrorLogInsertLogic.insertErrorLog("Error: No Action",
						errorData, "6");
				return "0";

			}
		}// end of insert/update attempt

		/* Delete in process entries which were just moved to closed */
		try {
			wugOutageCloseActionsDAO.deleteWugOutagesInProcessHistory(
					nActiveMonitorStateChangeID, wugInstance);
		} catch (Exception e) {
			String errorData = "Delete from wug_outages_incidents_in_process_history failed for "
					+ nActiveMonitorStateChangeID;
			wugErrorLogInsertLogic.insertErrorLog("Error: No Action",
					errorData, "6");
			return "0";

		}
		try {
			wugOutageCloseActionsDAO.deleteWugOutagesInProcess(
					nActiveMonitorStateChangeID, wugInstance);
		} catch (Exception e) {
			String errorData = "Delete from wug_outages_incidents_in_process_history failed for "
					+ nActiveMonitorStateChangeID;
			wugErrorLogInsertLogic.insertErrorLog("Error: No Action",
					errorData, "6");
			return "0";

		}

		return "1";
	}

	@Override
	public String wugOutageCloseAction(String nActiveMonitorStateChangeID) {
		String wugIpIssueOwner = "2";
		String wugIpRootCause = "0";
		String wugIpCorrectiveAction = "0";
		String wugIpEffortMinutes = "0";
		String wugInstance = "1";
		String wugBackupStatus = "0";
		return wugOutageCloseAction(nActiveMonitorStateChangeID,
				wugIpIssueOwner, wugIpRootCause, wugIpCorrectiveAction,
				wugIpEffortMinutes, wugInstance, wugBackupStatus);
	}

}
