package com.ilex.ws.core.logic;

/**
 * The Interface WugOutageNoActionInsertLogic.Add WUG reported outage to the no
 * action table and close outage.
 * 
 * Update the monitor summary to reflect new update.
 */
public interface WugOutageNoActionInsertLogic {
	String wugOutageNoActionInsert(String nActiveMonitorStateChangeID,
			String userId, String wug_instance);

	void wugOutageNoActionInsert(String nActiveMonitorStateChangeID,
			String userId);
}
