package com.ilex.ws.core.logic;

import com.ilex.ws.core.dto.TicketDispatchDTO;

/**
 * The Interface TicketDispatchLogic.
 */
public interface TicketDispatchLogic {

	/**
	 * Gets the ticket dispatch info.
	 * 
	 * @param userId
	 *            the user id
	 * @param appendixId
	 *            the appendix id
	 * @return the ticket dispatch info
	 */
	TicketDispatchDTO getTicketDispatchInfo(Long userId, Long appendixId);

}
