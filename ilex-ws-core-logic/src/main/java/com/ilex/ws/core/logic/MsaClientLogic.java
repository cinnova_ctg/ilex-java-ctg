package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.MsaClientDTO;

public interface MsaClientLogic {

	/**
	 * Gets the client list.
	 * 
	 * @return the client list
	 */
	public List<MsaClientDTO> getClientList();

	/**
	 * Gets the client detail.
	 * 
	 * @param msaClientDTO
	 *            the msaclientdto
	 * @return the client detail
	 */
	public MsaClientDTO getClientDetail(MsaClientDTO msaClientDTO);

}
