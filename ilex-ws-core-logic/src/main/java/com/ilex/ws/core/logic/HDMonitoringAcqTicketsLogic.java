package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.HdAquiredTicketDTO;

public interface HDMonitoringAcqTicketsLogic {

    List<HdAquiredTicketDTO> getAcquiredTickets(String hostName);

    void releaseLock(Long ticketId);

}
