package com.ilex.ws.core.logic.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.PointOfContactVO;
import com.ilex.ws.core.dao.PointOfContactDAO;
import com.ilex.ws.core.dao.TicketDispatchDAO;
import com.ilex.ws.core.dto.TicketDispatchDTO;
import com.ilex.ws.core.logic.TicketDispatchLogic;

/**
 * The Class TicketDispatchLogicImpl.
 */
@Component
public class TicketDispatchLogicImpl implements TicketDispatchLogic {

	/** The point of contact dao. */
	@Resource
	PointOfContactDAO pointOfContactDAO;

	/** The ticket dispatch dao. */
	@Resource
	TicketDispatchDAO ticketDispatchDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.TicketDispatchLogic#getTicketDispatchInfo(java
	 * .lang.Long, java.lang.Long)
	 */
	@Override
	public TicketDispatchDTO getTicketDispatchInfo(Long userId, Long appendixId) {
		TicketDispatchDTO ticketDispatchDTO = new TicketDispatchDTO();

		PointOfContactVO pointOfContactVO = new PointOfContactVO();
		pointOfContactVO.setPocId(String.valueOf(userId));

		PointOfContactVO pointOfContact = pointOfContactDAO
				.getContactDetail(pointOfContactVO);

		ticketDispatchDTO.setUserId(userId);
		ticketDispatchDTO.setUserName(pointOfContact.getFirstName() + " "
				+ pointOfContact.getLastName());
		ticketDispatchDTO.setEmail(pointOfContact.getEmailID());
		ticketDispatchDTO.setCompanyName(ticketDispatchDAO
				.getCustomerName(appendixId));
		ticketDispatchDTO.setAppendixId(appendixId);

		return ticketDispatchDTO;

	}

}
