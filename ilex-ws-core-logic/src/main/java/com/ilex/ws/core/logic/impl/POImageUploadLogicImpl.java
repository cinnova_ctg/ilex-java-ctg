package com.ilex.ws.core.logic.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.fileupload.FileItem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.POImageDetailVO;
import com.ilex.ws.core.bean.POImageUploadVO;
import com.ilex.ws.core.dao.POImageUploadDAO;
import com.ilex.ws.core.dto.POImageDetailDTO;
import com.ilex.ws.core.dto.POImageUploadDTO;
import com.ilex.ws.core.dto.POImageUploadRequirementDTO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.logic.POImageUploadLogic;
import com.ilex.ws.core.util.MessageKeyConstant;
import com.mind.common.PODeliverableStatusTypes;

/**
 * The Class POImageUploadLogicImpl.
 */
@Component
public class POImageUploadLogicImpl implements POImageUploadLogic {

	/** The po image upload dao. */
	@Resource
	POImageUploadDAO poImageUploadDAO;

	@Value("#{ilexProperties['mobile.min.po.number']}")
	private String minPONumber;

	public POImageUploadRequirementDTO getPOImageUploadRequirement(String poId,
			String authenticationString) {
		String partnerId;
		POImageUploadRequirementDTO poImageUploadRequirementDTO = new POImageUploadRequirementDTO();
		List<POImageDetailDTO> imageDetailDTOList = new ArrayList<POImageDetailDTO>();
		validatePOId(poId);
		if (!validateAuthString(authenticationString)) {
			throw new IlexBusinessException(MessageKeyConstant.AUTH_NOT_VALID);
		}

		partnerId = poImageUploadDAO.getPartnerId(authenticationString);

		List<POImageDetailVO> imageDetailList = poImageUploadDAO
				.getPOImageDetail(poId, partnerId);
		if (imageDetailList.size() > 0) {
			int count = 0;
			for (POImageDetailVO s : imageDetailList) {
				if (count == 0) {
					poImageUploadRequirementDTO.setCustomer(s.getCustomer());
					poImageUploadRequirementDTO.setCity(s.getCity());
					poImageUploadRequirementDTO
							.setCreateDate(s.getCreateDate());
					count = 1;
				}
			}
		}
		copyImageDetailsFromVoToDTO(imageDetailList, imageDetailDTOList);

		poImageUploadRequirementDTO.setAuthentication(authenticationString);
		poImageUploadRequirementDTO.setImages(imageDetailDTOList);
		poImageUploadRequirementDTO.setLm_po_Id(poId);
		poImageUploadRequirementDTO.setStatus(MessageKeyConstant.STATUS_PASS);
		return poImageUploadRequirementDTO;
	}

	public boolean poImageUpload(POImageUploadDTO poImageUploadDTO,
			FileItem fileItem) {
		POImageUploadVO pOImageUploadVO = new POImageUploadVO();
		boolean isUploaded = false;
		validatePOId(poImageUploadDTO.getLm_po_id());

		if (!validateAuthString(poImageUploadDTO.getAuthentication())) {
			throw new IlexBusinessException(MessageKeyConstant.AUTH_NOT_VALID);
		}
		if (poImageUploadDTO.getImagenumber() == null)
			throw new IlexBusinessException(
					MessageKeyConstant.IMAGE_NUMBER_NOT_VALID);
		if (poImageUploadDTO.getName() == null)
			throw new IlexBusinessException(
					MessageKeyConstant.IMAGE_NAME_NOT_VALID);
		copyPOImageUploadDTOToPOImageUploadVO(poImageUploadDTO, pOImageUploadVO);
		// Set PO Image extension to .jpg for all attachments
		pOImageUploadVO.setFile(fileItem.get());
		int mid = fileItem.getName().lastIndexOf(".");
		String fileName = fileItem.getName();
		if (mid != -1) {
			fileName = fileName.substring(0, mid);
		}

		fileName = fileName + ".jpg";
		pOImageUploadVO.setFileName(fileName);
		int deliverableStatus = -1;

		deliverableStatus = poImageUploadDAO
				.getPODeliverableStatus(poImageUploadDTO.getImagenumber());

		// if deliverable status is accepted
		if (MessageKeyConstant.DELIVERABLE_ACCEPTED
				.equals(PODeliverableStatusTypes
						.getStatusName(deliverableStatus))) {
			throw new IlexBusinessException(
					MessageKeyConstant.DELIVERABLE_ALREADY_ACCEPTED);
		}

		isUploaded = poImageUploadDAO.poImageUpload(pOImageUploadVO);

		return isUploaded;
	}

	/**
	 * Validate po id.
	 * 
	 * @param poId
	 *            the po id
	 * @return true, if successful
	 */
	private void validatePOId(String poId) {
		boolean isValidPO = false;
		try {
			if (Integer.parseInt(poId.trim()) >= Integer.parseInt(minPONumber
					.trim())) {
				isValidPO = true;
			}
		} catch (RuntimeException runtimeException) {
			isValidPO = false;
		}

		if (!isValidPO) {
			throw new IlexBusinessException(MessageKeyConstant.PO_ID_NOT_VALID,
					new String[] { minPONumber });

		}
	}

	/**
	 * Validate authString which should contain only alphanumeric characters
	 * 
	 * @param authString
	 *            the authString
	 * @return true, if contains only alphanumeric characters
	 */
	private static boolean validateAuthString(String authString) {

		Pattern ALPHANUMERIC = Pattern.compile("[A-Za-z0-9]+");
		if (authString == null)
			return false;
		else {
			Matcher m = ALPHANUMERIC.matcher(authString);
			return m.matches();
		}

	}

	/**
	 * copy properties for list of POImageDetailVO as source to list of
	 * POImageDetailDTO as Destination
	 * 
	 * @param List
	 *            <POImageDetailVO> poImageDetailVO Source
	 * @param List
	 *            <POImageDetailDTO> poImageDetailDTO Destination
	 * 
	 */

	private static void copyImageDetailsFromVoToDTO(
			List<POImageDetailVO> poImageDetailVO,
			List<POImageDetailDTO> poImageDetailDTO) {
		Iterator<POImageDetailVO> itr = poImageDetailVO.iterator();
		while (itr.hasNext()) {
			POImageDetailDTO imageDetailDTO = new POImageDetailDTO();
			POImageDetailVO imageDetailVO = itr.next();
			imageDetailDTO.setImagename(imageDetailVO.getImageName());
			imageDetailDTO.setImagenumber(imageDetailVO.getImageNumber());
			imageDetailDTO.setDescription(imageDetailVO.getDescription());
			imageDetailDTO.setCustomer(imageDetailVO.getCustomer());
			imageDetailDTO.setCreateDate(imageDetailVO.getCreateDate());
			imageDetailDTO.setCity(imageDetailVO.getCity());
			poImageDetailDTO.add(imageDetailDTO);
		}

	}
	private static void copyImageDetailsFromVoToDTOFOR(
			List<POImageDetailVO> poImageDetailVO,
			List<POImageDetailDTO> poImageDetailDTO) {
		Iterator<POImageDetailVO> itr = poImageDetailVO.iterator();
		while (itr.hasNext()) {
			POImageDetailDTO imageDetailDTO = new POImageDetailDTO();
			POImageDetailVO imageDetailVO = itr.next();
			imageDetailDTO.setImagename(imageDetailVO.getImageName());
			imageDetailDTO.setImagenumber(imageDetailVO.getImageNumber());
			imageDetailDTO.setDescription(imageDetailVO.getDescription());
			imageDetailDTO.setCustomer(imageDetailVO.getCustomer());
			imageDetailDTO.setCreateDate(imageDetailVO.getCreateDate());
			imageDetailDTO.setCity(imageDetailVO.getCity());
			poImageDetailDTO.add(imageDetailDTO);
		}
	}

	/**
	 * copy properties of poImageUploadDTO (Source) to poImageUploadVO
	 * (Destination)
	 * 
	 * @param POImageUploadDTO
	 * @param POImageUploadVO
	 * 
	 */

	private static void copyPOImageUploadDTOToPOImageUploadVO(
			POImageUploadDTO poImageUploadDTO, POImageUploadVO poImageUploadVO) {
		poImageUploadVO.setAuthentication(poImageUploadDTO.getAuthentication());
		poImageUploadVO.setImageName(poImageUploadDTO.getName());
		poImageUploadVO.setImageNumber(poImageUploadDTO.getImagenumber());
		poImageUploadVO.setPoId(poImageUploadDTO.getLm_po_id());
		poImageUploadVO.setDescription(poImageUploadDTO.getDescription());
	}

}
