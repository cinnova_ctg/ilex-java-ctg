package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.IlexTicketCreateInputDTO;
import com.ilex.ws.core.dto.IlexTicketDTO;
import com.ilex.ws.core.dto.ServiceNowTicketDTO;

/**
 * The Interface DemoServiceNowIntegrationLogic.
 */
public interface DemoServiceNowIntegrationLogic {

	/**
	 * Insert ticket From Ilex System to database and create a ticket in service
	 * Now Save service Now response
	 * 
	 * @param serviceNowTicketDTO
	 *            the service now ticket dto
	 * @return the string
	 */
	String insertTicket(ServiceNowTicketDTO serviceNowTicketDTO);

	/**
	 * Gets the incident ticket details of top 10 Tickets from Service Now.
	 * 
	 * @return the incident ticket details
	 */
	List<IlexTicketDTO> getIncidentTicketDetails();

	String createServiceNowTicket(
			IlexTicketCreateInputDTO ilexTicketCreateInputDTO);

	List<IlexTicketDTO> getServiceNowTicketDetails();

	void addComment(String comments, String workNotes, String ticketNumber);

}
