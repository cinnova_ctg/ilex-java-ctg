package com.ilex.ws.core.logic;

import org.apache.commons.fileupload.FileItem;

import com.ilex.ws.core.dto.MacWorxIntergrationDTO;
import com.ilex.ws.core.dto.MacWorxIntergrationResponseDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface MacWorxLogic.
 */
public interface MacWorxLogic {

	/**
	 * Mac worx intergration.
	 * 
	 * @param macWorxIntergrationDTO
	 *            the mac worx intergration dto
	 * @param fileItem
	 *            the file item
	 * @return the mac worx intergration response dto
	 */
	MacWorxIntergrationResponseDTO macWorxIntergration(
			MacWorxIntergrationDTO macWorxIntergrationDTO, FileItem fileItem);

	/**
	 * Mac worx user auth.
	 * 
	 * @param userName
	 *            the user name
	 * @param password
	 *            the password
	 * @param prId
	 *            the pr id
	 * @return the int
	 */
	int macWorxUserAuth(String userName, String password, int prId);

}
