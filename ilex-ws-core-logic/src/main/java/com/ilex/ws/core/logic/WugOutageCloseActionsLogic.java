package com.ilex.ws.core.logic;

/**
 * Close an outage.
 * 
 * An outage, as recorded in the in process main and history tables, is closed
 * based on one of three user actions. First a user may use the No Action link.
 * Second, a Help Desk ticket tied to an outage can be closed. Finally, an MSP
 * Dispatch tied to an outage can be closed.
 * 
 * In all cases, the same basic action is required, a closed and closed history
 * entry must be created for the corresponding in process data. The closed data
 * is a mirror of in process data with a few additions. These include closure
 * details as entered by the user plus the start and stop points of the outage.
 * 
 * Once the closed entries are successfully created, the in process data is
 * deleted
 * 
 * ASSUMED TO BE CALLED FROM THE CONTEXT OF A TRANSACTION. RETURN CODE SHOULD BE
 * USED TO ROLLBACK AS NEEDED.
 */
public interface WugOutageCloseActionsLogic {

	String wugOutageCloseAction(String nActiveMonitorStateChangeID,
			String wugIpIssueOwner, String wugIpRootCause,
			String wugIpCorrectiveAction, String wugIpEffortMinutes,
			String wugInstance, String wugBackupStatus);

	String wugOutageCloseAction(String nActiveMonitorStateChangeID);
}
