package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.InvBTMClientDTO;
import com.ilex.ws.core.dto.InvBTMContactDTO;
import com.ilex.ws.core.dto.MasterBTMDTO;

public interface InvBTMContactLogic {

	/**
	 * Gets the bTM contact details.
	 * 
	 * @param invBTMClientDTO
	 *            the inv btm client dto
	 * @return the bTM contact details
	 */
	List<InvBTMContactDTO> getBTMContactDetails(InvBTMClientDTO invBTMClientDTO);

	/**
	 * Update btm contact details.
	 * 
	 * @param masterBTMDTO
	 *            the master btmdto
	 */
	void updateBTMContactDetails(MasterBTMDTO masterBTMDTO);

}
