package com.ilex.ws.core.logic.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.HDHistoryVO;
import com.ilex.ws.core.bean.HDSessionVO;
import com.ilex.ws.core.bean.HDTicketVO;
import com.ilex.ws.core.bean.HDWIPTableVO;
import com.ilex.ws.core.bean.TicketDropResponseVO;
import com.ilex.ws.core.dao.HDActiveTicketDAO;
import com.ilex.ws.core.dao.HDMainDAO;
import com.ilex.ws.core.dto.HDSessionDTO;
import com.ilex.ws.core.dto.HDTicketDTO;
import com.ilex.ws.core.dto.HDWIPTableDTO;
import com.ilex.ws.core.exception.IlexSystemException;
import com.ilex.ws.core.logic.HDActiveTicketLogic;
import com.ilex.ws.core.util.HDTicketPersistUtils;
import com.ilex.ws.core.util.HdPersistedTickets;

// TODO: Auto-generated Javadoc
/**
 * The Class HDActiveTicketLogicImpl.
 */
@Component
public class HDActiveTicketLogicImpl implements HDActiveTicketLogic {

	/** The hd active ticket dao. */
	@Resource
	HDActiveTicketDAO hdActiveTicketDAO;

	/** The hd main dao. */
	@Resource
	HDMainDAO hdMainDAO;

	/** The Constant logger. */
	private static final Logger logger = Logger
			.getLogger(HDActiveTicketLogicImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDActiveTicketLogic#isTicketAvailableForDrop(java
	 * .lang.Long, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String isTicketAvailableForDrop(Long ticketId, String userId,
			String sessionId, String domainName) {

		String hdSessionTrackerId = hdActiveTicketDAO.getHDSessionTrackerId(
				userId, sessionId, domainName);

		TicketDropResponseVO ticketDropResponseVO = hdActiveTicketDAO
				.isTicketAvailableForDrop(ticketId, hdSessionTrackerId, userId);
		if (ticketDropResponseVO.isDropStatus()) {
			return "success";
		} else {
			if (!StringUtils.isBlank(ticketDropResponseVO.getAssignedUser())) {
				return "This ticket is currently locked by"
						+ ticketDropResponseVO.getAssignedUser();
			} else {
				return "An error occurred while attempting to drop this ticket.  Please try again.";
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDActiveTicketLogic#removeTicketLock(com.ilex.
	 * ws.core.dto.HDSessionDTO)
	 */
	@Override
	public void removeTicketLock(HDSessionDTO hdSessionDTO) {

		HDSessionVO hdSessionVO = new HDSessionVO();
		BeanUtils.copyProperties(hdSessionDTO, hdSessionVO);

		hdActiveTicketDAO.removeTicketLock(hdSessionVO);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDActiveTicketLogic#saveDroppedTicketState(java
	 * .lang.Long, java.lang.String, java.lang.String)
	 */
	@Override
	public void saveDroppedTicketState(Long ticketId, String divId,
			String sessionId) {
		Map<String, String> wipTickets = new HashMap<>();
		if (HDTicketPersistUtils.get(sessionId) == null) {
			HdPersistedTickets hdPersistedTickets = new HdPersistedTickets();

			hdPersistedTickets.setActiveTicketId("");
			wipTickets.put(ticketId.toString(), divId);
			hdPersistedTickets.setWipTickets(wipTickets);
			HDTicketPersistUtils.persistUserTickets(sessionId,
					hdPersistedTickets);

		} else {
			HDTicketPersistUtils.get(sessionId).getWipTickets()
					.put(ticketId.toString(), divId);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDActiveTicketLogic#saveActiveTicket(java.lang
	 * .Long, java.lang.String)
	 */
	@Override
	public void saveActiveTicket(Long ticketId, String sessionId) {
		if (HDTicketPersistUtils.get(sessionId) != null) {
			HDTicketPersistUtils.get(sessionId).setActiveTicketId(
					ticketId.toString());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDActiveTicketLogic#closeActiveTicket(java.lang
	 * .Long, java.lang.String)
	 */
	@Override
	public Boolean closeActiveTicket(Long ticketId, String userId) {
		Boolean status = hdActiveTicketDAO.closeActiveTicket(ticketId, userId);
		HDHistoryVO hdHistoryVO = new HDHistoryVO();
		hdHistoryVO.setEvent("20");
		hdHistoryVO.setFromData("Active Ticket Lock");
		hdHistoryVO.setFromReference(null);
		hdHistoryVO.setInitiator(userId);
		hdHistoryVO.setTicketId(Long.valueOf(ticketId));
		hdHistoryVO.setToData("Release Ticket Lock");
		hdHistoryVO.setToReference(null);
		hdMainDAO.manageChangeHistory(hdHistoryVO);
		return status;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDActiveTicketLogic#getTicketDetails(java.lang
	 * .String)
	 */
	@Override
	public HDWIPTableDTO getTicketDetails(String ticketId) {
		List<HDWIPTableVO> tickets = hdActiveTicketDAO
				.getHDWIPTicketDetails(ticketId);
		HDWIPTableDTO ticketDetails = new HDWIPTableDTO();
		HDWIPTableVO hdwipTableVO = new HDWIPTableVO();

		if (!tickets.isEmpty()) {
			hdwipTableVO = tickets.get(0);
		}
		BeanUtils.copyProperties(hdwipTableVO, ticketDetails);

		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd HH:mm");
		ticketDetails.setNextActionDueDate(dateFormat.format(hdwipTableVO
				.getNextActionDueDateDB()));

		ticketDetails.setDueDate(hdwipTableVO.getNextActionDueDateDB());

		ticketDetails.setOpenedDate(dateFormat.format(hdwipTableVO
				.getOpenedDbDate()));

		Calendar calMin5 = Calendar.getInstance();
		calMin5.add(Calendar.MINUTE, -5);

		Calendar dueDateCal = Calendar.getInstance();
		Date dueDate = null;

		dueDate = ticketDetails.getDueDate();
		dueDateCal.setTime(dueDate);

		if (dueDateCal.after(calMin5)) {
			ticketDetails
					.setTicketStatus("<span class='PCSTD0010'>Pending</span>");
			ticketDetails.setOverdueStatus("Pending");
		} else {

			ticketDetails
					.setTicketStatus("<span class='PCSTD0011' style='color:red;'>Overdue</span>");
			ticketDetails.setOverdueStatus("Overdue");

		}
		return ticketDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDActiveTicketLogic#updateNextAction(java.lang
	 * .String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public HDWIPTableDTO updateNextAction(String ticketId,
			String nextActionTime, String nextActionUnit, String nextAction,
			String userId) {

		Date dueDate = new Date();
		Calendar dueDateCal = Calendar.getInstance();
		if (nextActionUnit.equalsIgnoreCase("Hrs")) {
			Double nextTimeHours = Double.valueOf(nextActionTime);
			Double nextTimeMinutes = Math.ceil((nextTimeHours - Math
					.floor(nextTimeHours)) * 60);
			dueDateCal.add(Calendar.HOUR, nextTimeHours.intValue());
			dueDateCal.add(Calendar.MINUTE, nextTimeMinutes.intValue());
			dueDate.setTime(dueDateCal.getTimeInMillis());
		} else if (nextActionUnit.equalsIgnoreCase("Mins")) {
			Double nextTimeMinutes = Math.ceil(Double.valueOf(nextActionTime));
			dueDateCal.add(Calendar.MINUTE, nextTimeMinutes.intValue());
			dueDate.setTime(dueDateCal.getTimeInMillis());
		} else {
			try {
				SimpleDateFormat fullYearDateFormat = new SimpleDateFormat(
						"MM-dd-yyyy HH:mm");
				dueDate = fullYearDateFormat.parse(nextActionTime);
			} catch (ParseException e) {
				logger.error(e.getMessage(), e);
				throw new IlexSystemException("");
			}
		}

		hdActiveTicketDAO.updateNextAction(ticketId, nextAction, dueDate,
				userId);

		return getTicketDetails(ticketId);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDActiveTicketLogic#saveEfforts(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void saveEfforts(String ticketId, String effortValue, String userId) {
		Boolean isBillable = false;
		Boolean ppsFlag = false;

		isBillable = hdMainDAO.getBillableStatus(Long.valueOf(ticketId));

		if (isBillable) {
			Date effectiveDateForPpsCalculation = hdMainDAO
					.getEffectiveDateforPPS(Long.valueOf(ticketId));
			if (effectiveDateForPpsCalculation != null) {

				Calendar calendar = Calendar.getInstance();
				calendar.setTime(effectiveDateForPpsCalculation);

				int dateHours = calendar.get(Calendar.HOUR_OF_DAY);

				// Logic as given in Stored procedure
				if ((dateHours > 0 && dateHours < 8)
						|| (dateHours > 17 && dateHours < 24)) {
					ppsFlag = true;
				}
			}
		}

		hdMainDAO.saveEfforts(ticketId, effortValue, ppsFlag, userId,
				isBillable);

		Double totalEffortsFrom = hdMainDAO.getTotalEffort(Long
				.valueOf(ticketId));

		Double totalEffortsTo = 0.0;
		if (StringUtils.isNotBlank(effortValue)) {
			totalEffortsTo = totalEffortsFrom + Double.valueOf(effortValue);
		}

		String totalEffortsFromText = "From: ";
		if (totalEffortsFrom.toString().length() > 10) {
			totalEffortsFromText += totalEffortsFrom.toString()
					.substring(0, 10);
		} else {
			totalEffortsFromText += totalEffortsFrom.toString();
		}

		String totalEffortsToText = "To: ";
		if (totalEffortsToText.toString().length() > 10) {
			totalEffortsToText += totalEffortsTo.toString().substring(0, 10);
		} else {
			totalEffortsToText += totalEffortsTo.toString();
		}

		HDHistoryVO hdHistoryVO = new HDHistoryVO();
		hdHistoryVO.setEvent("3");
		hdHistoryVO.setFromData(totalEffortsFromText);
		hdHistoryVO.setFromReference(null);
		hdHistoryVO.setInitiator(userId);
		hdHistoryVO.setTicketId(Long.valueOf(ticketId));
		hdHistoryVO.setToData(totalEffortsToText);
		hdHistoryVO.setToReference(null);

		hdMainDAO.manageChangeHistory(hdHistoryVO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDActiveTicketLogic#saveEffortsForHD(java.lang
	 * .String, java.lang.String, java.lang.String)
	 */
	@Override
	public void saveEffortsForHD(String ticketId, String effortStartDate,
			String userId) {
		Boolean isBillable = false;
		Boolean ppsFlag = false;

		isBillable = hdMainDAO.getBillableStatus(Long.valueOf(ticketId));

		if (isBillable) {
			Date effectiveDateForPpsCalculation = hdMainDAO
					.getEffectiveDateforPPS(Long.valueOf(ticketId));
			if (effectiveDateForPpsCalculation != null) {

				Calendar calendar = Calendar.getInstance();
				calendar.setTime(effectiveDateForPpsCalculation);

				int dateHours = calendar.get(Calendar.HOUR_OF_DAY);

				// Logic as given in Stored procedure
				if ((dateHours > 0 && dateHours < 8)
						|| (dateHours > 17 && dateHours < 24)) {
					ppsFlag = true;
				}
			}
		}

		hdMainDAO.saveEffortsforHD(ticketId, effortStartDate, ppsFlag, userId,
				isBillable);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDActiveTicketLogic#updateEfforts(java.lang.String
	 * , java.lang.String, java.lang.String)
	 */
	@Override
	public void updateEfforts(String ticketId, String effortDate, String userId) {
		Boolean isBillable = false;
		Boolean ppsFlag = false;

		isBillable = hdMainDAO.getBillableStatus(Long.valueOf(ticketId));

		if (isBillable) {
			Date effectiveDateForPpsCalculation = hdMainDAO
					.getEffectiveDateforPPS(Long.valueOf(ticketId));
			if (effectiveDateForPpsCalculation != null) {

				Calendar calendar = Calendar.getInstance();
				calendar.setTime(effectiveDateForPpsCalculation);

				int dateHours = calendar.get(Calendar.HOUR_OF_DAY);

				// Logic as given in Stored procedure
				if ((dateHours > 0 && dateHours < 8)
						|| (dateHours > 17 && dateHours < 24)) {
					ppsFlag = true;
				}
			}
		}

		String effortValue = hdMainDAO.updateEfforts(ticketId, effortDate,
				ppsFlag, userId, isBillable);

		Double totalEffortsFrom = hdMainDAO.getTotalEffort(Long
				.valueOf(ticketId));

		Double totalEffortsTo = 0.0;
		if (StringUtils.isNotBlank(effortValue)) {
			totalEffortsTo = totalEffortsFrom + Double.valueOf(effortValue);
		}

		String totalEffortsFromText = "From: ";
		if (totalEffortsFrom.toString().length() > 10) {
			totalEffortsFromText += totalEffortsFrom.toString()
					.substring(0, 10);
		} else {
			totalEffortsFromText += totalEffortsFrom.toString();
		}

		String totalEffortsToText = "To: ";
		if (totalEffortsToText.toString().length() > 10) {
			totalEffortsToText += totalEffortsTo.toString().substring(0, 10);
		} else {
			totalEffortsToText += totalEffortsTo.toString();
		}

		HDHistoryVO hdHistoryVO = new HDHistoryVO();
		hdHistoryVO.setEvent("3");
		hdHistoryVO.setFromData(totalEffortsFromText);
		hdHistoryVO.setFromReference(null);
		hdHistoryVO.setInitiator(userId);
		hdHistoryVO.setTicketId(Long.valueOf(ticketId));
		hdHistoryVO.setToData(totalEffortsToText);
		hdHistoryVO.setToReference(null);

		hdMainDAO.manageChangeHistory(hdHistoryVO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDActiveTicketLogic#updateState(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void updateState(String ticketId, String state, String userId) {
		hdMainDAO.manageTicketDetailStatus(ticketId, state, userId);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDActiveTicketLogic#updateBackupStatus(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public void updateBackupStatus(String ticketId, String cktStatus,
			String userId) {
		HDTicketVO hdTicketVO = new HDTicketVO();
		hdTicketVO.setTicketId(ticketId);
		hdTicketVO.setBackUpCktStatus(cktStatus);
		hdMainDAO.editBackupDetails(hdTicketVO, userId);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDActiveTicketLogic#getTicketDetailForDashboard
	 * (java.lang.String)
	 */
	@Override
	public HDTicketDTO getTicketDetailForDashboard(String ticketId) {
		HDTicketVO hdTicketVO = hdActiveTicketDAO
				.getTicketDetailForDashboard(ticketId);
		HDTicketDTO hdTicketDTO = new HDTicketDTO();
		BeanUtils.copyProperties(hdTicketVO, hdTicketDTO);
		return hdTicketDTO;
	}
}
