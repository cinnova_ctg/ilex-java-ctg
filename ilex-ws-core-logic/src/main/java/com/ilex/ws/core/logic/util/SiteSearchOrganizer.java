package com.ilex.ws.core.logic.util;

import com.ilex.ws.core.dto.SiteSearchDetailDTO;
import com.mind.bean.pm.SiteSearchBean;

/**
 * The Class SiteSearchOrganizer.
 */
public class SiteSearchOrganizer {

	/**
	 * Copy site details.
	 * 
	 * @param siteSearchBean
	 *            the site search bean(Source)
	 * @return the site search detail dto
	 */
	public static SiteSearchDetailDTO copySiteDetails(
			SiteSearchBean siteSearchBean) {
		SiteSearchDetailDTO siteDetailDTO = new SiteSearchDetailDTO();
		siteDetailDTO.setInstallerPoc(siteSearchBean.getInstallerpoc());
		siteDetailDTO.setMsaId(siteSearchBean.getMsaid());
		siteDetailDTO.setSiteAddress(siteSearchBean.getSiteaddress());
		siteDetailDTO.setSiteBrand(siteSearchBean.getSitebrand());
		siteDetailDTO.setSiteCategory(siteSearchBean.getSitecategory());
		siteDetailDTO.setSiteCity(siteSearchBean.getSitecity());
		siteDetailDTO.setSiteCountry(siteSearchBean.getSitecountry());
		siteDetailDTO.setSiteDesignator(siteSearchBean.getSitedesignator());
		siteDetailDTO.setSiteDirection(siteSearchBean.getSitedesignator());
		siteDetailDTO.setSiteEndCustomer(siteSearchBean.getSiteendcustomer());
		siteDetailDTO.setSiteLatDeg(siteSearchBean.getSitelatdeg());
		siteDetailDTO.setSiteLatDirection(siteSearchBean.getSitelatdirection());
		siteDetailDTO.setSiteLatMin(siteSearchBean.getSitelatmin());
		siteDetailDTO.setSiteListId(siteSearchBean.getSitelistid());
		siteDetailDTO.setSiteLocalityFactor(siteSearchBean
				.getSitelocalityfactor());
		siteDetailDTO.setSiteLonDeg(siteSearchBean.getSitelondeg());
		siteDetailDTO.setSiteLonDirection(siteSearchBean.getSitelondirection());
		siteDetailDTO.setSiteLonMin(siteSearchBean.getSitelonmin());
		siteDetailDTO.setSiteName(siteSearchBean.getSitename());
		siteDetailDTO.setSiteNotes(siteSearchBean.getSitenotes());
		siteDetailDTO.setSiteNumber(siteSearchBean.getSitenumber());
		siteDetailDTO.setSitePhone(siteSearchBean.getSitephone());
		siteDetailDTO.setSitePoc(siteSearchBean.getSitepoc());
		siteDetailDTO.setSitePriEmail(siteSearchBean.getSitepriemail());
		siteDetailDTO.setSitePriPoc(siteSearchBean.getSitepripoc());
		siteDetailDTO.setSiteRegion(siteSearchBean.getSiteregion());
		siteDetailDTO.setSiteSecEmail(siteSearchBean.getSitesecemail());
		siteDetailDTO.setSiteSecPhone(siteSearchBean.getSitesecphone());
		siteDetailDTO.setSiteSecPoc(siteSearchBean.getSitesecpoc());
		siteDetailDTO.setSiteState(siteSearchBean.getSitestate());
		siteDetailDTO.setSiteStateDesc(siteSearchBean.getSitestatedesc());
		siteDetailDTO.setSiteStatus(siteSearchBean.getSitestatus());
		siteDetailDTO.setSiteTimeZone(siteSearchBean.getSitetimezone());
		siteDetailDTO.setSiteWorkLocation(siteSearchBean.getSiteworklocation());
		siteDetailDTO.setSiteZipCode(siteSearchBean.getSitezipcode());
		return siteDetailDTO;
	}

}
