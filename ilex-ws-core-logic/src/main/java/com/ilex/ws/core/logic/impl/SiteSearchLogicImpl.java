package com.ilex.ws.core.logic.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.dao.SiteSearchDAO;
import com.ilex.ws.core.dto.LabelValueDTO;
import com.ilex.ws.core.dto.SiteDetailDTO;
import com.ilex.ws.core.dto.SiteSearchDTO;
import com.ilex.ws.core.dto.SiteSearchDetailDTO;
import com.ilex.ws.core.logic.SiteSearchLogic;
import com.ilex.ws.core.logic.util.SiteDetailOrganizer;
import com.ilex.ws.core.logic.util.SiteSearchOrganizer;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;
import com.mind.bean.pm.SiteBean;
import com.mind.bean.pm.SiteSearchBean;
import com.mind.bean.prm.JobSetUpDTO;
import com.mind.bean.prm.SiteManagementBean;
import com.mind.common.LabelValue;

@Component
public class SiteSearchLogicImpl implements SiteSearchLogic {

	@Resource
	SiteSearchDAO siteSearchDAO;

	private List<SiteSearchBean> getSearchsitedetails(int msaId,
			String siteName, String siteNo, String siteCity, String siteState,
			String siteBrand, String siteEndCustomer) {
		List<SiteSearchBean> siteDetails = siteSearchDAO.getSearchSiteDetails(
				msaId, siteName, siteNo, "", siteCity, siteState, siteBrand,
				siteEndCustomer, 0);
		return siteDetails;
	}

	public List<SiteManagementBean> getCountryList(String msaId) {
		List<SiteManagementBean> countryList = siteSearchDAO
				.getCountryList(msaId);
		return countryList;
	}

	@Override
	public List<LabelValueDTO> getSiteStateList(String countryId, String msaId) {
		List<LabelValue> siteStateList = siteSearchDAO.getSiteStateList(
				countryId, msaId);
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		List<LabelValueDTO> siteStates = new ArrayList<LabelValueDTO>();
		siteStates = awareBeanUtilsBean.copyList(LabelValueDTO.class,
				siteStateList);
		return siteStates;
	}

	@Override
	public String getEndCustomer(String appendixId) {
		return siteSearchDAO.getEndCustomer(appendixId);
	}

	@Override
	public int getMsaId(String appendixId) {
		return siteSearchDAO.getMsaId(appendixId);
	}

	@Override
	public List<LabelValueDTO> getSiteEndCustomerList(String msaId) {
		List<LabelValue> customerList = siteSearchDAO
				.getSiteEndCustomerList(msaId);
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		List<LabelValueDTO> customers = new ArrayList<LabelValueDTO>();
		customers = awareBeanUtilsBean.copyList(LabelValueDTO.class,
				customerList);
		return customers;
	}

	@Override
	public List<LabelValueDTO> getGroupNameListSiteSearch(String msaId) {
		List<LabelValue> groupNameList = siteSearchDAO
				.getGroupNameListSiteSearch(msaId);
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		List<LabelValueDTO> groupNames = new ArrayList<LabelValueDTO>();
		groupNames = awareBeanUtilsBean.copyList(LabelValueDTO.class,
				groupNameList);
		return groupNames;
	}

	@Override
	public int updateSite(SiteBean siteform, String loginuserid) {
		return siteSearchDAO.updateSite(siteform, loginuserid);
	}

	@Override
	public SiteSearchDTO getSiteInfo(SiteSearchDTO siteSearchDTOArgs) {
		SiteSearchDTO siteSearchDTO = new SiteSearchDTO();

		BeanUtils.copyProperties(siteSearchDTOArgs, siteSearchDTO);
		JobSetUpDTO jobInfo = siteSearchDAO.getJobInformation(siteSearchDTO
				.getJobId());
		siteSearchDTO.setAppendixId(jobInfo.getAppendixId());
		siteSearchDTO.setAppendixTitle(jobInfo.getAppendixName());
		siteSearchDTO.setMsaId(jobInfo.getMsaId());
		siteSearchDTO.setMsaName(jobInfo.getMsaName());
		String msaId = jobInfo.getMsaId();
		List<LabelValueDTO> siteBrands = getGroupNameListSiteSearch(msaId);
		siteSearchDTO.setSiteBrands(siteBrands);
		List<LabelValueDTO> siteEndCustomers = getSiteEndCustomerList(msaId);
		siteSearchDTO.setSiteEndCustomers(siteEndCustomers);

		Map<String, List<LabelValueDTO>> statesMap = new LinkedHashMap<String, List<LabelValueDTO>>();

		List<SiteManagementBean> countryList = getCountryList(siteSearchDTO
				.getMsaId());

		for (SiteManagementBean country : countryList) {

			String countryId = country.getSiteCountryId();
			if (StringUtils.isNotBlank(countryId)) {
				List<LabelValueDTO> states = getSiteStateList(countryId, msaId);
				statesMap.put(countryId, states);
			}
		}

		siteSearchDTO.setStatesMap(statesMap);

		return siteSearchDTO;
	}

	@Override
	public List<SiteSearchDetailDTO> getSearchResult(SiteSearchDTO siteInfo) {
		List<SiteSearchBean> siteDetailVO = getSearchsitedetails(
				Integer.valueOf(siteInfo.getMsaId()), siteInfo.getSiteName(),
				siteInfo.getSiteNumber(), siteInfo.getSiteCity(),
				siteInfo.getSiteState(), siteInfo.getSiteBrand(),
				siteInfo.getSiteEndCustomer());

		List<SiteSearchDetailDTO> siteDetails = new ArrayList<SiteSearchDetailDTO>(
				siteDetailVO.size());

		for (SiteSearchBean siteDetail : siteDetailVO) {

			siteDetails.add(SiteSearchOrganizer.copySiteDetails(siteDetail));

		}

		return siteDetails;

	}

	@Override
	public SiteDetailDTO getSiteDetails(String jobId) {
		SiteBean siteDetailVO = siteSearchDAO.getSiteDetails(jobId);
		SiteDetailDTO siteDetails = SiteDetailOrganizer
				.copySiteDetails(siteDetailVO);
		return siteDetails;

	}

	@Override
	public void assignSite(String siteListId, String siteId, String jobId,
			String userId) {
		siteSearchDAO.addNewSite(Integer.valueOf(siteListId),
				Integer.valueOf(siteId), Integer.valueOf(jobId), userId);
	}

}
