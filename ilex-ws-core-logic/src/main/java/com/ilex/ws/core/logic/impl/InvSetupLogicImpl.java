package com.ilex.ws.core.logic.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.InvAdditionalInfoVO;
import com.ilex.ws.core.bean.InvBTMClientVO;
import com.ilex.ws.core.bean.InvBillToAddressVO;
import com.ilex.ws.core.bean.InvMasterLineItemDetailsVO;
import com.ilex.ws.core.bean.InvSearchVO;
import com.ilex.ws.core.bean.InvSetupVO;
import com.ilex.ws.core.dao.AppendixCustomerInfo;
import com.ilex.ws.core.dao.InvAdditionalInfoDAO;
import com.ilex.ws.core.dao.InvBTMClientDAO;
import com.ilex.ws.core.dao.InvBillToAddressDAO;
import com.ilex.ws.core.dao.InvMasterLineItemDetailsDAO;
import com.ilex.ws.core.dao.InvSetupDAO;
import com.ilex.ws.core.dao.util.Page;
import com.ilex.ws.core.dto.AppendixDetailDTO;
import com.ilex.ws.core.dto.AppendixInvSetupDTO;
import com.ilex.ws.core.dto.InvAdditionalInfoDTO;
import com.ilex.ws.core.dto.InvBillToAddressDTO;
import com.ilex.ws.core.dto.InvMasterLineItemDetailsDTO;
import com.ilex.ws.core.dto.InvSearchDTO;
import com.ilex.ws.core.dto.InvSearchListDTO;
import com.ilex.ws.core.dto.InvSetupDTO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.logic.AppendixLogic;
import com.ilex.ws.core.logic.InvSetupLogic;
import com.ilex.ws.core.util.MessageKeyConstant;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;

/**
 * The Class InvSetupLogicImpl.
 */
@Component
public class InvSetupLogicImpl implements InvSetupLogic {

	/** The appendix logic. */
	@Resource
	AppendixLogic appendixLogic;

	/** The inv btm client dao. */
	@Resource
	InvBTMClientDAO invBTMClientDAO;

	/** The inv master line item details dao. */
	@Resource
	InvMasterLineItemDetailsDAO invMasterLineItemDetailsDAO;

	/** The inv additional info dao. */
	@Resource
	InvAdditionalInfoDAO invAdditionalInfoDAO;

	/** The inv bill to address dao. */
	@Resource
	InvBillToAddressDAO invBillToAddressDAO;

	/** The inv setup dao. */
	@Resource
	InvSetupDAO invSetupDAO;

	/** The appendix customer info. */
	@Resource
	AppendixCustomerInfo appendixCustomerInfo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvSetupLogic#getInitialDetails(com.ilex.ws.core
	 * .dto.AppendixInvSetupDTO)
	 */
	@Override
	public InvSetupDTO getInitialDetails(AppendixInvSetupDTO appendixInvSetupDTO) {
		AppendixDetailDTO appendixDetailDTO = appendixLogic
				.getAppendixDetail(appendixInvSetupDTO.getAppendix()
						.getAppendixId());

		InvSetupDTO invSetupDetails = new InvSetupDTO();
		invSetupDetails.setAppendixId(appendixInvSetupDTO.getAppendix()
				.getAppendixId());
		if (Integer.valueOf(appendixDetailDTO.getAppendixType()) == 3) {
			invSetupDetails.setLineItemFormat("Detailed Breakout");

		} else {
			invSetupDetails.setLineItemFormat("Activity Summary");
		}
		invSetupDetails.setPageLayout("Standard-1");
		invSetupDetails
				.setContingentAddress("Contingent Network Services, LLC");
		invSetupDetails.setInvoiceSetupType("Individual");
		invSetupDetails.setInvoiceSetupGroupingType("Separate");
		invSetupDetails.setOutstandingBalanceExist(true);
		invSetupDetails.setCollectedTax(0.00f);
		invSetupDetails.setCustomerReferenceSource("Job");
		invSetupDetails.setCustomerReferenceName("Purchase Order");
		invSetupDetails.setCustomerReferenceValue(null);
		invSetupDetails.setCustomerReferencePoc(null);
		invSetupDetails.setSummarySheet("Not Applicable");
		invSetupDetails.setApprovalLevel("Job Owner");
		invSetupDetails.setTimeFrame("Offsite");
		return invSetupDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvSetupLogic#getLineItemDetails(com.ilex.ws.core
	 * .dto.AppendixDetailDTO)
	 */
	@Override
	public List<InvMasterLineItemDetailsDTO> getLineItemDetails(
			AppendixDetailDTO appendixDetailDTO) {
		List<InvMasterLineItemDetailsDTO> invMasterLineItemDetailsDTOList = new ArrayList<InvMasterLineItemDetailsDTO>();
		String projectType = null;
		if (Integer.valueOf(appendixDetailDTO.getAppendixType()) == 3) {
			projectType = "NetMedX";
		} else {
			projectType = "Projects";
		}
		List<InvMasterLineItemDetailsVO> invMasterLineItemDetailsVOList = invMasterLineItemDetailsDAO
				.getLineItemDetails(projectType);
		NullAwareBeanUtilsBean nullAwareBeanUtilsBean = new NullAwareBeanUtilsBean();
		invMasterLineItemDetailsDTOList = nullAwareBeanUtilsBean.copyList(
				InvMasterLineItemDetailsDTO.class,
				invMasterLineItemDetailsVOList);

		return invMasterLineItemDetailsDTOList;

	}

	/** The page size. */
	@Value("#{ilexProperties['appendix.inv.clientlist.page.size']}")
	private int pageSize;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvSetupLogic#getClientListForAppendixInvSetup
	 * (java.lang.String, int)
	 */
	@Override
	public InvSearchListDTO getClientListForAppendixInvSetup(String searchKey,
			int pageIndex) {
		InvSearchListDTO invSearchListDTO = new InvSearchListDTO();
		List<InvSearchDTO> invSearchDTOList = new ArrayList<InvSearchDTO>();
		Page<InvSearchVO> invSearchVOs = invBTMClientDAO
				.getClientListForAppendixInvSetup(searchKey, pageIndex,
						pageSize);
		List<InvSearchVO> invSearchVOList = invSearchVOs.getPageItems();

		if (invSearchVOList == null || invSearchVOList.size() == 0) {
			throw new IlexBusinessException(MessageKeyConstant.RECORD_NOT_FOUND);
		}
		NullAwareBeanUtilsBean utilsBean = new NullAwareBeanUtilsBean();
		invSearchDTOList = utilsBean.copyList(InvSearchDTO.class,
				invSearchVOList);
		invSearchListDTO.setInvSearchDTOList(invSearchDTOList);
		invSearchListDTO.setPageCount(invSearchVOs.getPagesAvailable());
		invSearchListDTO.setPageIndex(invSearchVOs.getPageNumber());

		return invSearchListDTO;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvSetupLogic#saveAppendixInvDetail(com.ilex.ws
	 * .core.dto.AppendixInvSetupDTO)
	 */
	@Override
	public int saveAppendixInvDetail(AppendixInvSetupDTO appendixInvSetupDTO) {

		InvBillToAddressVO invBillToAddressVO = new InvBillToAddressVO();
		copyBillToAdressDetail(invBillToAddressVO, appendixInvSetupDTO);
		int billToAddressId = invBillToAddressDAO
				.insertBillToAddressDetails(invBillToAddressVO);
		InvSetupVO invSetupVO = new InvSetupVO();
		invSetupVO.setBillToAddressId(billToAddressId);
		copyInvoiceSetupDetails(invSetupVO, appendixInvSetupDTO);
		invSetupDAO.insertInvSetupDetails(invSetupVO);

		return billToAddressId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.InvSetupLogic#getBillToAddressDetails(int)
	 */
	@Override
	public InvBillToAddressDTO getBillToAddressDetails(
			final int invBillToAddressId) {

		InvBillToAddressDTO invBillToAddressDTO = new InvBillToAddressDTO();
		InvBillToAddressVO invBillToAddressVO = invBillToAddressDAO
				.getBillToAddressDetails(invBillToAddressId);
		BeanUtils.copyProperties(invBillToAddressVO, invBillToAddressDTO);
		return invBillToAddressDTO;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvSetupLogic#updateClientProfile(com.ilex.ws.
	 * core.dto.InvBillToAddressDTO)
	 */
	@Override
	public void updateClientProfile(
			final InvBillToAddressDTO invBillToAddressDTO) {

		InvBillToAddressVO invBillToAddressVO = new InvBillToAddressVO();
		BeanUtils.copyProperties(invBillToAddressDTO, invBillToAddressVO);
		if (invBillToAddressDTO.getDeliveryMethod() == 0) {
			invBillToAddressVO.setDeliveryExist(true);
		} else {
			invBillToAddressVO.setDeliveryExist(false);
		}
		invBillToAddressDAO.updateBillToAddressDetails(invBillToAddressVO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvSetupLogic#updateInvoiceProfile(com.ilex.ws
	 * .core.dto.InvSetupDTO)
	 */
	@Override
	public void updateInvoiceProfile(final InvSetupDTO invSetupDTO) {
		InvSetupVO invSetupVO = new InvSetupVO();
		BeanUtils.copyProperties(invSetupDTO, invSetupVO);
		invSetupDAO.updateInvSetupProfile(invSetupVO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvSetupLogic#updateLineItemDetails(com.ilex.ws
	 * .core.dto.InvSetupDTO)
	 */
	@Override
	public void updateLineItemDetails(final InvSetupDTO invSetupDTO) {
		InvSetupVO invSetupVO = new InvSetupVO();
		BeanUtils.copyProperties(invSetupDTO, invSetupVO);
		invSetupDAO.updateLineItemDetails(invSetupVO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvSetupLogic#updateSummarySheetDetails(com.ilex
	 * .ws.core.dto.InvSetupDTO)
	 */
	@Override
	public void updateSummarySheetDetails(final InvSetupDTO invSetupDTO) {
		InvSetupVO invSetupVO = new InvSetupVO();
		BeanUtils.copyProperties(invSetupDTO, invSetupVO);
		invSetupDAO.updateSummarySheetDetails(invSetupVO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvSetupLogic#updateClientReqInfo(com.ilex.ws.
	 * core.dto.AppendixInvSetupDTO)
	 */
	@Override
	public void updateClientReqInfo(
			final AppendixInvSetupDTO appendixInvSetupDTO) {
		InvSetupVO invSetupVO = new InvSetupVO();
		BeanUtils.copyProperties(appendixInvSetupDTO.getInvSetupDetails(),
				invSetupVO);
		List<InvAdditionalInfoDTO> newAdditionInFoDTOList = appendixInvSetupDTO
				.getAdditionalInformationList();
		for (int i = 0; i < newAdditionInFoDTOList.size(); i++) {
			if (newAdditionInFoDTOList.get(i).getName() == null
					|| StringUtils.isEmpty(newAdditionInFoDTOList.get(i)
							.getName())) {
				newAdditionInFoDTOList.remove(i);
			}
		}

		List<InvAdditionalInfoVO> newAdditionInFoVOList = new ArrayList<InvAdditionalInfoVO>();
		NullAwareBeanUtilsBean nullAwareBeanUtilsBean = new NullAwareBeanUtilsBean();
		newAdditionInFoVOList = nullAwareBeanUtilsBean.copyList(
				InvAdditionalInfoVO.class, newAdditionInFoDTOList);
		invAdditionalInfoDAO.deleteAdditionalInfo(invSetupVO);
		if (newAdditionInFoVOList != null) {
			for (int i = 0; i < newAdditionInFoVOList.size(); i++) {
				newAdditionInFoVOList.get(i).setInvSetupId(
						invSetupVO.getInvoiceSetupId());
				invAdditionalInfoDAO.insertAdditionalInfo(newAdditionInFoVOList
						.get(i));
			}
		}

		invSetupDAO.updateClientInfoDetails(invSetupVO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.InvSetupLogic#getCustInfoParameter(int)
	 */
	@Override
	public List<String> getCustInfoParameter(int appendixId) {
		return appendixCustomerInfo.getCustInfoParameter(appendixId);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.InvSetupLogic#getInvSetupDetails(int)
	 */
	@Override
	public InvSetupDTO getInvSetupDetails(final int invSetupId) {
		InvSetupDTO invSetupDTO = new InvSetupDTO();

		InvSetupVO invSetupVO = invSetupDAO.getInvSetupDetails(invSetupId);
		BeanUtils.copyProperties(invSetupVO, invSetupDTO);
		return invSetupDTO;
	}

	@Override
	public InvSetupDTO getInvSetupDetails(
			final InvBillToAddressDTO invBillToAddressDTO) {
		InvSetupDTO invSetupDTO = new InvSetupDTO();
		InvBillToAddressVO invBillToAddressVO = new InvBillToAddressVO();
		BeanUtils.copyProperties(invBillToAddressDTO, invBillToAddressVO);
		InvSetupVO invSetupVO = invSetupDAO
				.getInvSetupDetails(invBillToAddressVO);
		BeanUtils.copyProperties(invSetupVO, invSetupDTO);
		return invSetupDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.InvSetupLogic#getAdditionalInfo(com.ilex.ws.core
	 * .dto.InvSetupDTO)
	 */
	@Override
	public List<InvAdditionalInfoDTO> getAdditionalInfo(InvSetupDTO invSetupDTO) {
		List<InvAdditionalInfoDTO> invAdditionalInfoDTOList = new ArrayList<InvAdditionalInfoDTO>();
		InvSetupVO invSetupVO = new InvSetupVO();
		BeanUtils.copyProperties(invSetupDTO, invSetupVO);
		List<InvAdditionalInfoVO> invAdditionalInfoVOList = invAdditionalInfoDAO
				.getAdditionalInfo(invSetupVO);
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		invAdditionalInfoDTOList = awareBeanUtilsBean.copyList(
				InvAdditionalInfoDTO.class, invAdditionalInfoVOList);
		return invAdditionalInfoDTOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.InvSetupLogic#deleteInvSetupDetails(int)
	 */
	@Override
	public void deleteInvSetupDetails(final int invBillToAddressId) {
		invSetupDAO.deleteInvSetupDetails(invBillToAddressId);

	}

	/**
	 * Copy invoice setup details.
	 * 
	 * @param invSetupVO
	 *            the inv setup vo
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 */
	private void copyInvoiceSetupDetails(InvSetupVO invSetupVO,
			AppendixInvSetupDTO appendixInvSetupDTO) {
		invSetupVO.setAppendixId(appendixInvSetupDTO.getAppendix()
				.getAppendixId());
		InvBTMClientVO invBTMClientVO = invBTMClientDAO.getClientDetails(String
				.valueOf(appendixInvSetupDTO.getClient().getMbtClientId()));

		invSetupVO.setMmId(invBTMClientVO.getMmId());
		invSetupVO.setPageLayout(appendixInvSetupDTO.getInvSetupDetails()
				.getPageLayout());
		invSetupVO.setContingentAddress(appendixInvSetupDTO
				.getInvSetupDetails().getContingentAddress());
		invSetupVO.setInvoiceSetupType(appendixInvSetupDTO.getInvSetupDetails()
				.getInvoiceSetupType());
		invSetupVO.setInvoiceSetupGroupingType(appendixInvSetupDTO
				.getInvSetupDetails().getInvoiceSetupGroupingType());
		invSetupVO.setOutstandingBalanceExist(appendixInvSetupDTO
				.getInvSetupDetails().isOutstandingBalanceExist());
		invSetupVO.setCustomerReferenceSource(appendixInvSetupDTO
				.getInvSetupDetails().getCustomerReferenceSource());
		invSetupVO.setCustomerReferenceName(appendixInvSetupDTO
				.getInvSetupDetails().getCustomerReferenceName());
		invSetupVO.setCustomerReferenceValue(appendixInvSetupDTO
				.getInvSetupDetails().getCustomerReferenceValue());
		invSetupVO.setCustomerReferencePoc(appendixInvSetupDTO
				.getInvSetupDetails().getCustomerReferencePoc());
		invSetupVO.setTimeFrame(appendixInvSetupDTO.getInvSetupDetails()
				.getTimeFrame());
		invSetupVO.setLineItemFormat(appendixInvSetupDTO.getInvSetupDetails()
				.getLineItemFormat());
		invSetupVO.setSummarySheet(appendixInvSetupDTO.getInvSetupDetails()
				.getSummarySheet());
		invSetupVO.setApprovalLevel(appendixInvSetupDTO.getInvSetupDetails()
				.getApprovalLevel());
		invSetupVO.setCollectedTax(appendixInvSetupDTO.getInvSetupDetails()
				.getCollectedTax());
		invSetupVO.setBillToReferenceExist(appendixInvSetupDTO
				.getInvSetupDetails().isBillToReferenceExist());
		invSetupVO.setCustomerReferenceExist(appendixInvSetupDTO
				.getInvSetupDetails().isCustomerReferenceExist());

	}

	/**
	 * Copy bill to adress detail.
	 * 
	 * @param invBillToAddressVO
	 *            the inv bill to address vo
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 */
	private void copyBillToAdressDetail(InvBillToAddressVO invBillToAddressVO,
			AppendixInvSetupDTO appendixInvSetupDTO) {

		invBillToAddressVO.setClientName(appendixInvSetupDTO.getClient()
				.getClientName());

		invBillToAddressVO.setAddress1(appendixInvSetupDTO.getClient()
				.getAddress1());
		invBillToAddressVO.setAddress2(appendixInvSetupDTO.getClient()
				.getAddress2());
		invBillToAddressVO.setCity(appendixInvSetupDTO.getClient().getCity());
		invBillToAddressVO.setState(appendixInvSetupDTO.getClient().getState());
		invBillToAddressVO.setZipCode(appendixInvSetupDTO.getClient()
				.getZipCode());
		invBillToAddressVO.setCountry(appendixInvSetupDTO.getClient()
				.getCountry());
		invBillToAddressVO.setDeliveryMethod(appendixInvSetupDTO.getClient()
				.getDeliveryMethod());
		invBillToAddressVO.setMbtId(appendixInvSetupDTO.getClient()
				.getMbtClientId());
		invBillToAddressVO.setContactExist(appendixInvSetupDTO.getClient()
				.isContactExist());
		if (appendixInvSetupDTO.getSelectedClient() != null) {
			invBillToAddressVO.setInvContactId(appendixInvSetupDTO
					.getContactDetailsList()
					.get(appendixInvSetupDTO.getSelectedClient())
					.getInvContactId());
			invBillToAddressVO.setFirstName(appendixInvSetupDTO
					.getContactDetailsList()
					.get(appendixInvSetupDTO.getSelectedClient())
					.getFirstName());
			invBillToAddressVO
					.setLastName(appendixInvSetupDTO.getContactDetailsList()
							.get(appendixInvSetupDTO.getSelectedClient())
							.getLastName());
			invBillToAddressVO.setPhone(appendixInvSetupDTO
					.getContactDetailsList()
					.get(appendixInvSetupDTO.getSelectedClient()).getPhone());
			invBillToAddressVO.setEmail(appendixInvSetupDTO
					.getContactDetailsList()
					.get(appendixInvSetupDTO.getSelectedClient()).getEmail());
			invBillToAddressVO.setCell(appendixInvSetupDTO
					.getContactDetailsList()
					.get(appendixInvSetupDTO.getSelectedClient()).getCell());
			invBillToAddressVO.setFax(appendixInvSetupDTO
					.getContactDetailsList()
					.get(appendixInvSetupDTO.getSelectedClient()).getFax());

		} else {
			invBillToAddressVO.setInvContactId(null);
		}
		if (appendixInvSetupDTO.getClient().getDeliveryMethod() == 0) {
			invBillToAddressVO.setDeliveryExist(true);
		} else {
			invBillToAddressVO.setDeliveryExist(false);
		}

	}
}
