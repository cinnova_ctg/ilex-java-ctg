package com.ilex.ws.core.logic.impl;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.WugOutInProcessVO;
import com.ilex.ws.core.dao.HDTicketCancelDAO;
import com.ilex.ws.core.dto.HdCancelAllDTO;
import com.ilex.ws.core.logic.HDTicketCancelLogic;

@Component
public class HDTicketCancelLogicImpl implements HDTicketCancelLogic {

	@Resource
	HDTicketCancelDAO hdTicketCancelDAO;

	@Override
	public void cancel(String ticketId, String userId,
			HdCancelAllDTO hdCancelAllDTO) {
		hdTicketCancelDAO.insertInWugNoAction(ticketId, userId);
		WugOutInProcessVO wugOutInProcessVO = hdTicketCancelDAO
				.getDataFromInProcess(ticketId);

		wugOutInProcessVO.setIssueOwner(hdCancelAllDTO.getPointOfFailureId());
		wugOutInProcessVO.setRootCouse(hdCancelAllDTO.getRootCauseId());
		wugOutInProcessVO.setCorrectiveAction(hdCancelAllDTO.getResolutionId());

		int eventTestCount = hdTicketCancelDAO.getEventTestCount(
				wugOutInProcessVO.getnActiveMonitorStateChangeID(),
				wugOutInProcessVO.getWugInstance());

		if (eventTestCount == 0) {
			if (StringUtils.isBlank(wugOutInProcessVO.getWugBackupStatus())) {
				wugOutInProcessVO.setWugBackupStatus("99");
			}
			if (StringUtils.isBlank(wugOutInProcessVO.getEffortMinutes())) {
				wugOutInProcessVO.setEffortMinutes("0");
			}
			if (StringUtils.isBlank(wugOutInProcessVO.getEscalationLevel())) {
				wugOutInProcessVO.setEscalationLevel("0");
			}
			if (StringUtils.isBlank(wugOutInProcessVO.getTicketReference())) {
				wugOutInProcessVO.setTicketReference(null);
			}
			hdTicketCancelDAO.insertWugOutClosedRecord(wugOutInProcessVO);

			hdTicketCancelDAO.insertWugOutClosedHistoryRecord(
					wugOutInProcessVO.getnActiveMonitorStateChangeID(),
					wugOutInProcessVO.getWugInstance());
		}

		hdTicketCancelDAO.deleteWugOutagesInProcessHistory(
				wugOutInProcessVO.getnActiveMonitorStateChangeID(),
				wugOutInProcessVO.getWugInstance());

		hdTicketCancelDAO.deleteWugOutagesInProcess(
				wugOutInProcessVO.getnActiveMonitorStateChangeID(),
				wugOutInProcessVO.getWugInstance());

		hdTicketCancelDAO.deleteFromHDTables(ticketId);
	}

}
