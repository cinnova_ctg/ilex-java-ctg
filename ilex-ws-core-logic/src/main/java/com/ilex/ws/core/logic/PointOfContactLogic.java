package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.OrgDivisionDTO;
import com.ilex.ws.core.dto.PointOfContactDTO;

/**
 * The Interface PointOfContactLogic.
 */
public interface PointOfContactLogic {

	/**
	 * Gets the contact list.
	 * 
	 * @param divisionDTO
	 *            the division dto
	 * @return the contact list
	 */
	public List<PointOfContactDTO> getContactList(OrgDivisionDTO divisionDTO);

	/**
	 * Gets the contact detail.
	 * 
	 * @param pointOfContactDTO
	 *            the point of contact dto
	 * @return the contact detail
	 */
	public PointOfContactDTO getContactDetail(
			PointOfContactDTO pointOfContactDTO);

}
