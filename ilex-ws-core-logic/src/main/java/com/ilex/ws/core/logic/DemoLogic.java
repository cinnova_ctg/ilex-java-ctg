package com.ilex.ws.core.logic;

import com.ilex.ws.core.bean.UserDTO;

public interface DemoLogic {
	public UserDTO getUserInfo(String userId);
}
