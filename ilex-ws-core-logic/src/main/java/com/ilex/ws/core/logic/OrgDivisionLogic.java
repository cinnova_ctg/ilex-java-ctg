package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.MsaClientDTO;
import com.ilex.ws.core.dto.OrgDivisionDTO;

/**
 * The Interface OrgDivisionLogic.
 */
public interface OrgDivisionLogic {

	/**
	 * Gets the divisions.
	 * 
	 * @param client
	 *            the client
	 * @return the divisions list
	 */
	public List<OrgDivisionDTO> getDivisions(MsaClientDTO client);

	/**
	 * Gets the division detail.
	 * 
	 * @param division
	 *            the division
	 * @return the division detail
	 */
	public OrgDivisionDTO getDivisionDetail(OrgDivisionDTO division);

}
