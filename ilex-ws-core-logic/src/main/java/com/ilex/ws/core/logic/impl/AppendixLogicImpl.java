package com.ilex.ws.core.logic.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.AppendixDetailVo;
import com.ilex.ws.core.bean.InvBTMClientVO;
import com.ilex.ws.core.dao.AppendixDAO;
import com.ilex.ws.core.dto.AppendixDetailDTO;
import com.ilex.ws.core.dto.InvBTMClientDTO;
import com.ilex.ws.core.logic.AppendixLogic;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;

@Component
public class AppendixLogicImpl implements AppendixLogic {
	private static final Logger logger = Logger
			.getLogger(AppendixLogicImpl.class);
	@Resource
	AppendixDAO appendixDAO;

	@Override
	public List<AppendixDetailDTO> getAppendixList(
			InvBTMClientDTO invBTMClientDTO) {
		InvBTMClientVO invBTMClientVO = new InvBTMClientVO();
		try {
			BeanUtils.copyProperties(invBTMClientVO, invBTMClientDTO);
		} catch (IllegalAccessException e) {
			logger.error(e);
		} catch (InvocationTargetException e) {
			logger.error(e);
		}

		List<AppendixDetailVo> appendixDetailVoList = appendixDAO
				.getAppendixList(invBTMClientVO);
		NullAwareBeanUtilsBean nullAwareBeanUtilsBean = new NullAwareBeanUtilsBean();
		List<AppendixDetailDTO> appendixDetailDTOList = nullAwareBeanUtilsBean
				.copyList(AppendixDetailDTO.class, appendixDetailVoList);
		return appendixDetailDTOList;
	}

	@Override
	public AppendixDetailDTO getAppendixDetail(int appendixId) {
		AppendixDetailDTO appendixDetailDTO = new AppendixDetailDTO();
		AppendixDetailVo appendixDetailVo = appendixDAO
				.getAppendixDetail(appendixId);
		try {
			BeanUtils.copyProperties(appendixDetailDTO, appendixDetailVo);
		} catch (IllegalAccessException e) {
			logger.error(e);
		} catch (InvocationTargetException e) {
			logger.error(e);
		}
		return appendixDetailDTO;

	}
}
