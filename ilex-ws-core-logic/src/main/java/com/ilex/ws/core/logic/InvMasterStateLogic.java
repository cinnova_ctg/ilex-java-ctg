package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.InvStateListDTO;

public interface InvMasterStateLogic {

	/**
	 * Gets the state list.
	 * 
	 * @return the state list
	 */
	List<InvStateListDTO> getStateList();

}
