package com.ilex.ws.core.logic.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.CircuitStatusVO;
import com.ilex.ws.core.bean.EventCorrelatorVO;
import com.ilex.ws.core.bean.HDAuditTrailVO;
import com.ilex.ws.core.bean.HDBillableVO;
import com.ilex.ws.core.bean.HDCategoryVO;
import com.ilex.ws.core.bean.HDConfigurationItemVO;
import com.ilex.ws.core.bean.HDCubeStatusVO;
import com.ilex.ws.core.bean.HDCustomerDetailVO;
import com.ilex.ws.core.bean.HDEffortVO;
import com.ilex.ws.core.bean.HDMonitoringTimeVO;
import com.ilex.ws.core.bean.HDNextActionVO;
import com.ilex.ws.core.bean.HDPendingAckTableVO;
import com.ilex.ws.core.bean.HDTicketVO;
import com.ilex.ws.core.bean.HDWIPTableVO;
import com.ilex.ws.core.bean.HDWorkNoteVO;
import com.ilex.ws.core.bean.IncidentStateVO;
import com.ilex.ws.core.bean.IncidentVO;
import com.ilex.ws.core.bean.MsaClientVO;
import com.ilex.ws.core.bean.OutageDetailVO;
import com.ilex.ws.core.bean.PointOfContactVO;
import com.ilex.ws.core.bean.ProvisioningDetailVO;
import com.ilex.ws.core.bean.ResolutionsVO;
import com.ilex.ws.core.bean.SiteDetailVO;
import com.ilex.ws.core.bean.StateTailVO;
import com.ilex.ws.core.bean.TicketStateVO;
import com.ilex.ws.core.bean.WugDetailsVO;
import com.ilex.ws.core.bean.WugOutInProcessVO;
import com.ilex.ws.core.dao.HDActiveTicketDAO;
import com.ilex.ws.core.dao.HDJobStructureDAO;
import com.ilex.ws.core.dao.HDMainDAO;
import com.ilex.ws.core.dao.LoginDAO;
import com.ilex.ws.core.dao.MsaClientDAO;
import com.ilex.ws.core.dao.PointOfContactDAO;
import com.ilex.ws.core.dao.ServiceNowDAO;
import com.ilex.ws.core.dto.CircuitStatusDTO;
import com.ilex.ws.core.dto.EventCorrelatorDTO;
import com.ilex.ws.core.dto.HDAuditTrailDTO;
import com.ilex.ws.core.dto.HDCategoryDTO;
import com.ilex.ws.core.dto.HDConfigurationItemDTO;
import com.ilex.ws.core.dto.HDCubeStatusDTO;
import com.ilex.ws.core.dto.HDCustomerDetailDTO;
import com.ilex.ws.core.dto.HDEffortDTO;
import com.ilex.ws.core.dto.HDInstallNoteDTO;
import com.ilex.ws.core.dto.HDInstallNoteVO;
import com.ilex.ws.core.dto.HDMonitoringTimeDTO;
import com.ilex.ws.core.dto.HDNextActionDTO;
import com.ilex.ws.core.dto.HDPendingAckTableDTO;
import com.ilex.ws.core.dto.HDTableHeaderDTO;
import com.ilex.ws.core.dto.HDTableParentDTO;
import com.ilex.ws.core.dto.HDTicketDTO;
import com.ilex.ws.core.dto.HDWIPTableDTO;
import com.ilex.ws.core.dto.HDWorkNoteDTO;
import com.ilex.ws.core.dto.HdBillableDTO;
import com.ilex.ws.core.dto.HdCancelAllDTO;
import com.ilex.ws.core.dto.IncidentDTO;
import com.ilex.ws.core.dto.IncidentStateDTO;
import com.ilex.ws.core.dto.MasterSiteAssetsDeployedDTO;
import com.ilex.ws.core.dto.OutageDetailDTO;
import com.ilex.ws.core.dto.ProvisioningDetailDTO;
import com.ilex.ws.core.dto.ResolutionsDTO;
import com.ilex.ws.core.dto.SiteDetailDTO;
import com.ilex.ws.core.dto.StateTailDTO;
import com.ilex.ws.core.dto.TicketStateDTO;
import com.ilex.ws.core.dto.WugDetailsDTO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.exception.IlexSystemException;
import com.ilex.ws.core.logic.HDActiveTicketLogic;
import com.ilex.ws.core.logic.HDMainLogic;
import com.ilex.ws.core.logic.HDTicketCancelLogic;
import com.ilex.ws.core.logic.ServiceNowLogic;
import com.ilex.ws.core.logic.WugOutageCloseActionsLogic;
import com.ilex.ws.core.logic.WugOutageNoActionInsertLogic;
import com.ilex.ws.core.util.ActionTicketDropDiv;
import com.ilex.ws.core.util.BackUpStatus;
import com.ilex.ws.core.util.DateUtils;
import com.ilex.ws.core.util.HDTables;
import com.ilex.ws.core.util.HDTicketPersistUtils;
import com.ilex.ws.core.util.HdCubeDisplayStatus;
import com.ilex.ws.core.util.HdPersistedTickets;
import com.ilex.ws.core.util.MessageKeyConstant;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;
import com.ilex.ws.core.util.Util;
import com.mind.bean.LoginForm;
import com.mind.bean.msp.RealTimeState;
import com.mind.bean.msp.RealTimeStateInfo;
import com.mind.common.bean.EmailBean;

/**
 * The Class HDMainLogicImpl.
 */
@Component
public class HDMainLogicImpl implements HDMainLogic {

	/**
	 * The Constant logger.
	 */
	public static final Logger logger = Logger.getLogger(HDMainLogicImpl.class);

	/**
	 * The hd main dao.
	 */
	@Value("#{ilexProperties['servicenow.email.userName']}")
	private String emailUserName;

	/**
	 * The email user password.
	 */
	@Value("#{ilexProperties['servicenow.email.userPassword']}")
	private String emailUserPassword;

	/**
	 * The email smtp server name.
	 */
	@Value("#{ilexProperties['servicenow.email.smtpServerName']}")
	private String emailSmtpServerName;

	/**
	 * The email smtp server port.
	 */
	@Value("#{ilexProperties['servicenow.email.smtpServerPort']}")
	private String emailSmtpServerPort;

	/**
	 * The hd main dao.
	 */
	@Resource
	HDMainDAO hdMainDAO;

	/**
	 * The login dao.
	 */
	@Resource
	LoginDAO loginDAO;

	/**
	 * The point of contact dao.
	 */
	@Resource
	PointOfContactDAO pointOfContactDAO;

	/**
	 * The hd active ticket logic.
	 */
	@Resource
	HDActiveTicketLogic hdActiveTicketLogic;

	/**
	 * The msa client dao.
	 */
	@Resource
	MsaClientDAO msaClientDAO;

	/**
	 * The hd job structure dao.
	 */
	@Resource
	HDJobStructureDAO hdJobStructureDAO;

	/**
	 * The wug outage no action insert logic.
	 */
	@Resource
	WugOutageNoActionInsertLogic wugOutageNoActionInsertLogic;

	/**
	 * The wug outage close actions logic.
	 */
	@Resource
	WugOutageCloseActionsLogic wugOutageCloseActionsLogic;

	/**
	 * The hd ticket cancel logic.
	 */
	@Resource
	HDTicketCancelLogic hdTicketCancelLogic;

	/**
	 * The service now logic.
	 */
	@Resource
	ServiceNowLogic serviceNowLogic;

	/**
	 * The service now dao.
	 */
	@Resource
	ServiceNowDAO serviceNowDAO;

	/**
	 * The hd active ticket dao.
	 */
	@Resource
	HDActiveTicketDAO hdActiveTicketDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getTablesHeader()
	 */
	@Override
	public HDTableParentDTO getTablesHeader() {

		HDTableParentDTO hdTableParentDTO = new HDTableParentDTO();

		hdTableParentDTO.setLeftDivWidth(930);
		hdTableParentDTO.setRightDivWidth(410);
		HDTableHeaderDTO hdWIPTableDTO = new HDTableHeaderDTO();
		hdWIPTableDTO.setPriorityWidth(20);
		hdWIPTableDTO.setOpenedDateWidth(65);
		hdWIPTableDTO.setSiteNumberWidth(130);
		hdWIPTableDTO.setStateWidth(45);
		hdWIPTableDTO.setNextActionWidth(130);
		hdWIPTableDTO.setNextActionDueDateWidth(75);
		hdWIPTableDTO.setCcaWidth(70);
		hdWIPTableDTO.setShortDescWidth(120);
		hdWIPTableDTO.setCustomerWidth(90);
		hdWIPTableDTO.setTableWidth(930);
		hdTableParentDTO.setHdWIPTableDTO(hdWIPTableDTO);

		HDTableHeaderDTO hdPendingAckDTO = new HDTableHeaderDTO();
		hdPendingAckDTO.setOpenedDateWidth(70);
		hdPendingAckDTO.setSiteNumberWidth(110);
		hdPendingAckDTO.setStateWidth(85);
		hdPendingAckDTO.setSourceWidth(85);
		hdPendingAckDTO.setCustomerWidth(90);
		hdPendingAckDTO.setTableWidth(410);

		hdTableParentDTO.setHdPendingAckDTO(hdPendingAckDTO);

		return hdTableParentDTO;

	}

	private boolean isdayPassed(List<HDWIPTableVO> wipTableData, String usrId) {

		boolean retValue = false;
		for (int i = 0; i < wipTableData.size(); i++) {
			HDWIPTableVO hdwipTableVO = wipTableData.get(i);
			if (!hdwipTableVO.getDateDiff().equals("")
					&& Integer.parseInt(hdwipTableVO.getDateDiff()) >= 1440
					&& StringUtils.isNotBlank(hdwipTableVO.getPrimaryState())
					&& !hdwipTableVO.getPrimaryState().toLowerCase()
							.contains("down")) {
				/*
				 * If site status not down then close the ticket.
				 */
				HDTicketDTO hdTktDTO = new HDTicketDTO();
				hdTktDTO.setTicketId(hdwipTableVO.getTicketId());
				hdTktDTO.setRootCauseId("");
				hdTktDTO.setResolutionId("");
				hdTktDTO.setLmCompleteFirstCall(true);
				hdTktDTO.setTicketStatus("Closed");
				hdTktDTO.setTicketSource(hdwipTableVO.getTicketSource());
				hdTktDTO.setWorkNotes("Auto Closed");
				updateTicketToResolved(hdTktDTO, usrId);

				retValue = true;
			} else if (!hdwipTableVO.getDateDiff().equals("")
					&& Integer.parseInt(hdwipTableVO.getDateDiff()) >= 1440
					&& StringUtils.isNotBlank(hdwipTableVO.getPrimaryState())
					&& hdwipTableVO.getPrimaryState().toLowerCase()
							.contains("down")) {
				/*
				 * if site status is down then reopen the ticket and set over
				 * due true so that it get displayed in over due bucket.
				 */
				hdMainDAO.updateTicketToclosed(hdwipTableVO.getTicketId(),
						"12", true);

				retValue = true;

			}
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getHDWIPTableData()
	 */
	@Override
	public List<HDWIPTableDTO> getHDResolvedTableData(String siteNum,
			String userId) {

		List<HDWIPTableVO> hdWIPTableDataVO = hdMainDAO
				.getHdResolvedTableData(siteNum);

		// Moving code to repeating task, so as not to be reliant on a user
		// having the help desk open to close resolved tickets.
		/*
		 * if (hdWIPTableDataVO.size() > 0 && isdayPassed(hdWIPTableDataVO,
		 * userId)) { hdWIPTableDataVO =
		 * hdMainDAO.getHdResolvedTableData(siteNum); }
		 */
		List<HDWIPTableDTO> hdWIPTableData = new ArrayList<>();

		for (HDWIPTableVO tableVO : hdWIPTableDataVO) {

			if (tableVO.getUnderReview().equals("0")) {
				tableVO.setLastTd("<span class='greenIcon'>&nbsp;&nbsp;&nbsp;&nbsp;</span>");
			} else {
				tableVO.setLastTd("<span class='redIcon'>&nbsp;&nbsp;&nbsp;&nbsp;</span>");
			}

			HDWIPTableDTO hdWIPTableDTO = new HDWIPTableDTO();

			hdWIPTableDTO.setClientType(tableVO.getClientType());
			hdWIPTableDTO.setMmId(tableVO.getMmId());
			hdWIPTableDTO.setMmIdForDisplay(tableVO.getMmId());

			try {
				org.apache.commons.beanutils.BeanUtils.copyProperties(
						hdWIPTableDTO, tableVO);
			} catch (IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}

			hdWIPTableDTO.setEventColor(tableVO.getEventColor());

			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd HH:mm");
			hdWIPTableDTO.setNextActionDueDate(dateFormat.format(tableVO
					.getNextActionDueDateDB()));

			hdWIPTableDTO.setDueDate(tableVO.getNextActionDueDateDB());

			hdWIPTableDTO.setOpenedDate(dateFormat.format(tableVO
					.getOpenedDbDate()));
			hdWIPTableDTO.setUpdatedDate(dateFormat.format(tableVO
					.getUpdatedDbDate()));

			if (StringUtils.isNotBlank(hdWIPTableDTO.getCca_tss())) {
				if (hdWIPTableDTO.getCca_tss().length() >= 10) {
					hdWIPTableDTO.setCca_tssForDisplay(hdWIPTableDTO
							.getCca_tss().substring(0, 10));
				} else {
					hdWIPTableDTO.setCca_tssForDisplay(hdWIPTableDTO
							.getCca_tss());
				}
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getNextAction())) {
				if (hdWIPTableDTO.getNextAction().length() >= 16) {
					if (hdWIPTableDTO.isExtSystemStatus()) {
						hdWIPTableDTO
								.setNextActionForDisplay(hdWIPTableDTO
										.getNextAction().substring(0, 16)
										+ "<span class='snSymbol' style='float:right;' >&nbsp;&nbsp;&nbsp;</span>");
					} else {
						hdWIPTableDTO.setNextActionForDisplay(hdWIPTableDTO
								.getNextAction().substring(0, 16));
					}

				} else {
					if (hdWIPTableDTO.isExtSystemStatus()) {
						hdWIPTableDTO
								.setNextActionForDisplay(hdWIPTableDTO
										.getNextAction()
										+ "<span class='snSymbol' style='float:right;' >&nbsp;&nbsp;&nbsp;</span>");
					} else {
						hdWIPTableDTO.setNextActionForDisplay(hdWIPTableDTO
								.getNextAction());
					}

				}
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getCustomer())) {
				if (hdWIPTableDTO.getCustomer().length() >= 13) {
					hdWIPTableDTO.setCustomerForDisplay(hdWIPTableDTO
							.getCustomer().substring(0, 13));
				} else {
					hdWIPTableDTO.setCustomerForDisplay(hdWIPTableDTO
							.getCustomer());
				}
			}

			String img = "<img style=\"float:left; margin-right:3px;\" src=\"/Ilex-WS/resources/images/details.png\" class=\"siteDetails\"/>";
			String imgtag = "<img style=\"border:0;  cursor:default;\" src=\"/Ilex-WS/resources/images/priorities.jpg\"/>";
			String prop = hdWIPTableDTO.getSite_prop();
			if (prop.equals("1")) {

				hdWIPTableDTO.setSite_prop("Y");

			} else {

				hdWIPTableDTO.setSite_prop("N");
			}
			String link = "window.open('/Ilex/masterSiteAction.do?siteName="
					+ hdWIPTableDTO.getSiteNumber().replace("#", "-,-,-")
					+ "&mmId=" + hdWIPTableDTO.getMmId()
					+ "', '_blank', 'width=1100,height=700,scrollbars=yes')";

			String siteLinkStart = "<a style='float:left; width:100px;word-break: keep-all;' href=\"javascript:void(0);\" onclick=\""
					+ link + "\" >";
			String siteLinkEnd = "</a>";

			siteLinkStart = img + " " + siteLinkStart;

			if (StringUtils.isNotBlank(hdWIPTableDTO.getSiteNumber())) {
				if (hdWIPTableDTO.getSiteNumber().length() >= 16) {
					hdWIPTableDTO.setSiteNumberDisplay(hdWIPTableDTO
							.getSiteNumber().substring(0, 16));
				} else {
					hdWIPTableDTO.setSiteNumberDisplay(hdWIPTableDTO
							.getSiteNumber());
				}
			}

			String siteNo = hdWIPTableDTO.getSiteNumberDisplay();

			if ("2".equals(hdWIPTableDTO.getTicketSource())) {

				if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
						&& hdWIPTableDTO.getPrimaryState().toLowerCase()
								.contains("up")) {
					hdWIPTableDTO
							.setSiteNumberDisplay(siteLinkStart
									+ siteNo
									+ siteLinkEnd
									+ "<span class='greenRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");
				} else if (StringUtils.isNotBlank(hdWIPTableDTO
						.getPrimaryState())
						&& hdWIPTableDTO.getPrimaryState().toLowerCase()
								.contains("down")) {

					if (prop.equals("1")) {

						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='redRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>"
										+ imgtag);
					} else {

						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='redRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");

					}

				} else {

					if (prop.equals("1")) {

						hdWIPTableDTO.setSiteNumberDisplay(siteLinkStart
								+ siteNo + siteLinkEnd + imgtag);
					} else {

						hdWIPTableDTO.setSiteNumberDisplay(siteLinkStart
								+ siteNo + siteLinkEnd);

					}
				}

			} else {
				if (prop.equals("1")) {
					hdWIPTableDTO.setSiteNumberDisplay(siteLinkStart + siteNo
							+ siteLinkEnd + imgtag);
				} else {

					hdWIPTableDTO.setSiteNumberDisplay(siteLinkStart + siteNo
							+ siteLinkEnd);

				}
			}

			if (hdWIPTableDTO.isNextActionOverDue()) {
				hdWIPTableDTO.setOverdueStatus("overdue");
			} else {

				hdWIPTableDTO.setOverdueStatus("pending");
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
					&& hdWIPTableDTO.getPrimaryState().toLowerCase()
							.contains("up")) {
				hdWIPTableDTO.setSiteStatus("up");
			} else if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
					&& hdWIPTableDTO.getPrimaryState().toLowerCase()
							.contains("down")) {
				hdWIPTableDTO.setSiteStatus("down");
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getShortDescription())) {
				if (hdWIPTableDTO.getShortDescription().length() >= 16) {
					hdWIPTableDTO.setShortDescriptionDisplay(hdWIPTableDTO
							.getShortDescription().substring(0, 16));
				} else {
					hdWIPTableDTO.setShortDescriptionDisplay(hdWIPTableDTO
							.getShortDescription());
				}
			}
			hdWIPTableData.add(hdWIPTableDTO);

		}

		return hdWIPTableData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getHDWIPTableData()
	 */
	@Override
	public List<HDWIPTableDTO> getHDWIPTableData(String siteNum) {

		List<HDWIPTableVO> hdWIPTableDataVO = hdMainDAO.getHdWipTableData(
				HDTables.HD_WIP_ID.getCubeId(), siteNum);

		List<HDWIPTableDTO> hdWIPTableData = new ArrayList<>();

		for (HDWIPTableVO tableVO : hdWIPTableDataVO) {

			if (tableVO.getUnderReview().equals("0")) {
				tableVO.setLastTd("<span class='greenIcon'>&nbsp;&nbsp;&nbsp;&nbsp;</span>");
			} else {
				tableVO.setLastTd("<span class='redIcon'>&nbsp;&nbsp;&nbsp;&nbsp;</span>");
			}

			HDWIPTableDTO hdWIPTableDTO = new HDWIPTableDTO();
			try {
				org.apache.commons.beanutils.BeanUtils.copyProperties(
						hdWIPTableDTO, tableVO);
			} catch (IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}

			hdWIPTableDTO.setEventColor(tableVO.getEventColor());

			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd HH:mm");
			hdWIPTableDTO.setNextActionDueDate(dateFormat.format(tableVO
					.getNextActionDueDateDB()));

			hdWIPTableDTO.setDueDate(tableVO.getNextActionDueDateDB());

			hdWIPTableDTO.setOpenedDate(dateFormat.format(tableVO
					.getOpenedDbDate()));

			if (StringUtils.isNotBlank(hdWIPTableDTO.getCca_tss())) {
				if (hdWIPTableDTO.getCca_tss().length() >= 10) {
					hdWIPTableDTO.setCca_tssForDisplay(hdWIPTableDTO
							.getCca_tss().substring(0, 10));
				} else {
					hdWIPTableDTO.setCca_tssForDisplay(hdWIPTableDTO
							.getCca_tss());
				}
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getNextAction())) {
				if (hdWIPTableDTO.getNextAction().length() >= 16) {
					if (hdWIPTableDTO.isExtSystemStatus()) {
						hdWIPTableDTO
								.setNextActionForDisplay(hdWIPTableDTO
										.getNextAction().substring(0, 16)
										+ "<span class='snSymbol' style='float:right;' >&nbsp;&nbsp;&nbsp;</span>");
					} else {
						hdWIPTableDTO.setNextActionForDisplay(hdWIPTableDTO
								.getNextAction().substring(0, 16));
					}

				} else {
					if (hdWIPTableDTO.isExtSystemStatus()) {
						hdWIPTableDTO
								.setNextActionForDisplay(hdWIPTableDTO
										.getNextAction()
										+ "<span class='snSymbol' style='float:right;' >&nbsp;&nbsp;&nbsp;</span>");
					} else {
						hdWIPTableDTO.setNextActionForDisplay(hdWIPTableDTO
								.getNextAction());
					}

				}
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getCustomer())) {
				if (hdWIPTableDTO.getCustomer().length() >= 13) {
					hdWIPTableDTO.setCustomerForDisplay(hdWIPTableDTO
							.getCustomer().substring(0, 13));
				} else {
					hdWIPTableDTO.setCustomerForDisplay(hdWIPTableDTO
							.getCustomer());
				}
			}

			String img = "<img style=\"float:left; margin-right:3px;\" src=\"/Ilex-WS/resources/images/details.png\" class=\"siteDetails\"/>";
			String imgtag = "<img style=\"border:0;  cursor:default; \" src=\"/Ilex-WS/resources/images/priorities.jpg\"/>";
			String link = "window.open('/Ilex/masterSiteAction.do?siteName="
					+ hdWIPTableDTO.getSiteNumber().replace("#", "-,-,-")
					+ "&mmId=" + hdWIPTableDTO.getMmId()
					+ "', '_blank', 'width=1100,height=700,scrollbars=yes')";

			String siteLinkStart = "<a style='float:left; width:100px;word-break: keep-all;' href=\"javascript:void(0);\" onclick=\""
					+ link + "\" >";
			String siteLinkEnd = "</a>";

			siteLinkStart = img + " " + siteLinkStart;

			if (StringUtils.isNotBlank(hdWIPTableDTO.getSiteNumber())) {
				if (hdWIPTableDTO.getSiteNumber().length() >= 16) {
					hdWIPTableDTO.setSiteNumberDisplay(hdWIPTableDTO
							.getSiteNumber().substring(0, 16));
				} else {
					hdWIPTableDTO.setSiteNumberDisplay(hdWIPTableDTO
							.getSiteNumber());
				}
			}
			String prop = hdWIPTableDTO.getSite_prop();
			if (prop.equals("1")) {

				hdWIPTableDTO.setSite_prop("Y");

			} else {

				hdWIPTableDTO.setSite_prop("N");
			}
			String siteNo = hdWIPTableDTO.getSiteNumberDisplay();

			if ("2".equals(hdWIPTableDTO.getTicketSource())) {

				if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
						&& hdWIPTableDTO.getPrimaryState().toLowerCase()
								.contains("up")) {
					if (prop.equals("1")) {
						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='greenRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>"
										+ imgtag);

					} else {

						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='greenRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");

					}
				} else if (StringUtils.isNotBlank(hdWIPTableDTO
						.getPrimaryState())
						&& hdWIPTableDTO.getPrimaryState().toLowerCase()
								.contains("down")) {

					if (prop.equals("1")) {

						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='redRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>"
										+ imgtag);
					} else {

						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='redRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");

					}
				} else {

					if (prop.equals("1")) {
						hdWIPTableDTO.setSiteNumberDisplay(siteLinkStart
								+ siteNo + siteLinkEnd + imgtag);
					} else {

						hdWIPTableDTO.setSiteNumberDisplay(siteLinkStart
								+ siteNo + siteLinkEnd);

					}

				}

			} else {

				if (prop.equals("1")) {
					hdWIPTableDTO.setSiteNumberDisplay(siteLinkStart + siteNo
							+ siteLinkEnd + imgtag);
				} else {

					hdWIPTableDTO.setSiteNumberDisplay(siteLinkStart + siteNo
							+ siteLinkEnd);

				}

			}

			if (hdWIPTableDTO.isNextActionOverDue()) {
				hdWIPTableDTO.setOverdueStatus("overdue");
			} else {

				hdWIPTableDTO.setOverdueStatus("pending");
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
					&& hdWIPTableDTO.getPrimaryState().toLowerCase()
							.contains("up")) {
				hdWIPTableDTO.setSiteStatus("up");
			} else if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
					&& hdWIPTableDTO.getPrimaryState().toLowerCase()
							.contains("down")) {
				hdWIPTableDTO.setSiteStatus("down");
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getShortDescription())) {
				if (hdWIPTableDTO.getShortDescription().length() >= 16) {
					hdWIPTableDTO.setShortDescriptionDisplay(hdWIPTableDTO
							.getShortDescription().substring(0, 16));
				} else {
					hdWIPTableDTO.setShortDescriptionDisplay(hdWIPTableDTO
							.getShortDescription());
				}
			}
			hdWIPTableData.add(hdWIPTableDTO);

		}

		return hdWIPTableData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#createTicketPriority(java.lang.Long,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int createTicketPriority(Long appendixId, Integer impact,
			Integer urgency) {
		// TODO Auto-generated method stub
		return hdMainDAO.getTicketPriority(appendixId, impact, urgency);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getBackupCircuitStatus(java.lang.String
	 * )
	 */
	@Override
	public String getBackupCircuitStatus(String siteNum) {
		return hdMainDAO.getBackupCircuitStatus(siteNum);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getHDPendingAckTableData()
	 */
	@Override
	public String getHDPendingAckTableData(String siteNum) {
		List<HDPendingAckTableDTO> jsonRowData = new ArrayList<>();
		List<HDPendingAckTableVO> hdPendingAckTableVOList = hdMainDAO
				.getPendingAcKCustData(siteNum);
		HDPendingAckTableDTO hdPendingAckTableDTO = new HDPendingAckTableDTO();

		for (HDPendingAckTableVO hdPendingAckTableVO : hdPendingAckTableVOList) {
			hdPendingAckTableDTO = new HDPendingAckTableDTO();
			BeanUtils.copyProperties(hdPendingAckTableVO, hdPendingAckTableDTO);
			if (!StringUtils.isBlank(hdPendingAckTableVO.getOpenedDateDB()
					.toString())) {
				hdPendingAckTableDTO.setOpenedDate(DateUtils
						.convertDateToString(
								hdPendingAckTableVO.getOpenedDateDB(),
								"MM/dd HH:mm"));
			}
			if (hdPendingAckTableDTO.getSource().equals("ServiceNow")) {
				hdPendingAckTableDTO
						.setSource("<a href='#' style='text-decoration: underline;' onclick='ackTicketStep1(\"hdPendingAckCust\",this);'>ServiceNow</a>");
			}

			if (StringUtils.isNotBlank(hdPendingAckTableDTO.getCustomer())) {
				if (hdPendingAckTableDTO.getCustomer().length() >= 13) {
					String customer = hdPendingAckTableDTO.getCustomer()
							.substring(0, 13);
					hdPendingAckTableDTO.setCustomer(customer);
				}
			}
			String prop = hdPendingAckTableDTO.getSite_prop();
			if (prop.equals("1")) {

				hdPendingAckTableDTO.setSite_prop("Y");

			} else {

				hdPendingAckTableDTO.setSite_prop("N");

			}
			String imgtag = "<img style=\"border:0; cursor:default; \" src=\"/Ilex-WS/resources/images/priorities.jpg\"/>";
			if (StringUtils.isNotBlank(hdPendingAckTableDTO.getSiteNumber())) {
				if (hdPendingAckTableDTO.getSiteNumber().length() >= 16) {

					if (prop.equals("1")) {
						hdPendingAckTableDTO
								.setSiteNumberDisplay(hdPendingAckTableDTO
										.getSiteNumber().substring(0, 16)
										+ imgtag);
					} else {

						hdPendingAckTableDTO
								.setSiteNumberDisplay(hdPendingAckTableDTO
										.getSiteNumber().substring(0, 16));
					}
				} else {

					if (prop.equals("1")) {
						hdPendingAckTableDTO
								.setSiteNumberDisplay(hdPendingAckTableDTO
										.getSiteNumber() + imgtag);
					} else {

						hdPendingAckTableDTO
								.setSiteNumberDisplay(hdPendingAckTableDTO
										.getSiteNumber());

					}
				}
			}
			jsonRowData.add(hdPendingAckTableDTO);
		}

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("aaData", jsonRowData);
		String data = jsonObject.toString();
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getHDPendingAckSecurityTableData()
	 */
	@Override
	public String getHDPendingAckSecurityTableData(String siteNum) {
		List<HDPendingAckTableDTO> jsonRowData = new ArrayList<>();
		List<HDPendingAckTableVO> hdPendingAckTableVOList = hdMainDAO
				.getPendingAcKSecurityData(siteNum);
		HDPendingAckTableDTO hdPendingAckTableDTO = new HDPendingAckTableDTO();

		for (HDPendingAckTableVO hdPendingAckTableVO : hdPendingAckTableVOList) {
			hdPendingAckTableDTO = new HDPendingAckTableDTO();
			BeanUtils.copyProperties(hdPendingAckTableVO, hdPendingAckTableDTO);
			if (!StringUtils.isBlank(hdPendingAckTableVO.getOpenedDateDB()
					.toString())) {
				hdPendingAckTableDTO.setOpenedDate(DateUtils
						.convertDateToString(
								hdPendingAckTableVO.getOpenedDateDB(),
								"MM/dd HH:mm"));
			}

			if (hdPendingAckTableDTO.getSource().equals("Security Event")) {
				hdPendingAckTableDTO
						.setSource("<a href='#' style='text-decoration: underline;' onclick='ackTicketStep1(\"hdPendingAckSecurity\",this);'>Security Event</a>");
			}
			String imgtag = "<img style=\"border:0; cursor:default;\" src=\"/Ilex-WS/resources/images/priorities.jpg\"/>";
			String prop = hdPendingAckTableDTO.getSite_prop();
			if (prop.equals("1")) {

				hdPendingAckTableDTO.setSite_prop("Y");

			} else {

				hdPendingAckTableDTO.setSite_prop("N");

			}
			if (StringUtils.isNotBlank(hdPendingAckTableDTO.getCustomer())) {
				if (hdPendingAckTableDTO.getCustomer().length() >= 13) {
					String customer = hdPendingAckTableDTO.getCustomer()
							.substring(0, 13);
					hdPendingAckTableDTO.setCustomer(customer);
				}
			}

			if (StringUtils.isNotBlank(hdPendingAckTableDTO.getSiteNumber())) {
				if (hdPendingAckTableDTO.getSiteNumber().length() >= 16) {
					if (prop.equals("1")) {
						hdPendingAckTableDTO
								.setSiteNumberDisplay(hdPendingAckTableDTO
										.getSiteNumber().substring(0, 16)
										+ imgtag);
					} else {

						hdPendingAckTableDTO
								.setSiteNumberDisplay(hdPendingAckTableDTO
										.getSiteNumber().substring(0, 16));

					}
				} else {
					if (prop.equals("1")) {
						hdPendingAckTableDTO
								.setSiteNumberDisplay(hdPendingAckTableDTO
										.getSiteNumber() + imgtag);
					} else {
						hdPendingAckTableDTO
								.setSiteNumberDisplay(hdPendingAckTableDTO
										.getSiteNumber());

					}

				}
			}

			jsonRowData.add(hdPendingAckTableDTO);
		}

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("aaData", jsonRowData);
		String data = jsonObject.toString();
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getIndividualTableHeader(java.lang
	 * .String)
	 */
	@Override
	public HDTableParentDTO getIndividualTableHeader(String tableId) {
		HDTableParentDTO hdTableParentDTO = new HDTableParentDTO();
		HDTables tableType = HDTables.getTableFromTableId(tableId);
		HDTableHeaderDTO tableDTO = new HDTableHeaderDTO();
		switch (tableType) {
		case HD_PENDING_ACNOWLEDGEMENT_CUST:
		case HD_MONITOR_DATE_TIME:
		case HD_PENDING_ACNOWLEDGEMENT_ALERT:
		case HD_PENDING_ACNOWLEDGEMENT_MONITOR:
			HDTableHeaderDTO hdPendingAckDTO = new HDTableHeaderDTO();
			hdPendingAckDTO.setOpenedDateWidth(70);
			hdPendingAckDTO.setSiteNumberWidth(110);
			hdPendingAckDTO.setStateWidth(85);
			hdPendingAckDTO.setSourceWidth(85);
			hdPendingAckDTO.setCustomerWidth(90);
			hdPendingAckDTO.setTableWidth(420);
			hdTableParentDTO.setHdPendingAckDTO(hdPendingAckDTO);
			break;
		case HD_WIP:
			tableDTO.setPriorityWidth(50);
			tableDTO.setOpenedDateWidth(65);
			tableDTO.setSiteNumberWidth(130);
			tableDTO.setStateWidth(55);
			tableDTO.setNextActionWidth(130);
			tableDTO.setNextActionDueDateWidth(75);
			tableDTO.setCcaWidth(70);
			tableDTO.setShortDescWidth(120);
			tableDTO.setCustomerWidth(90);
			tableDTO.setTableWidth(955);
			hdTableParentDTO.setHdWIPTableDTO(tableDTO);
			break;
		case HD_WIP_OVERDUE:
			tableDTO.setPriorityWidth(50);
			tableDTO.setOpenedDateWidth(65);
			// tableDTO.setWidth(65);/* temporary change */
			tableDTO.setSiteNumberWidth(130);
			tableDTO.setStateWidth(55);
			tableDTO.setNextActionWidth(130);
			tableDTO.setNextActionDueDateWidth(75);
			tableDTO.setCcaWidth(70);
			tableDTO.setShortDescWidth(120);
			tableDTO.setCustomerWidth(90);
			tableDTO.setTableWidth(955);
			hdTableParentDTO.setHdWIPTableDTO(tableDTO);
			break;
		case HD_PENDING_ACNOWLEDGEMENT_MONITOR_TIER2:
			HDTableHeaderDTO hdPendingAckTier2DTO = new HDTableHeaderDTO();
			hdPendingAckTier2DTO.setOpenedDateWidth(70);
			hdPendingAckTier2DTO.setSiteNumberWidth(110);
			hdPendingAckTier2DTO.setStateWidth(85);
			hdPendingAckTier2DTO.setSourceWidth(85);
			hdPendingAckTier2DTO.setCustomerWidth(90);
			hdPendingAckTier2DTO.setTableWidth(420);
			hdTableParentDTO.setHdPendingAckDTO(hdPendingAckTier2DTO);
			break;

		default:
			break;
		}
		return hdTableParentDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getExcelData(java.lang.String)
	 */
	@Override
	public String getExcelData(String tableId, String userId) {
		HDTables tableType = HDTables.getTableFromTableId(tableId);
		JSONObject jsonObject = new JSONObject();
		List<HDWIPTableDTO> jsonRowData;
		String tickets = null;

		switch (tableType) {
		case HD_PENDING_ACNOWLEDGEMENT_CUST:
			tickets = getHDPendingAckTableData("");
			break;
		case HD_PENDING_ACNOWLEDGEMENT_ALERT:
			tickets = getHDPendingAckAlertTableData("");
			break;
		case HD_PENDING_ACNOWLEDGEMENT_MONITOR:
			tickets = getHDPendingAckMonitorTableData("", "not in");
			break;
		case HD_WIP:
			jsonRowData = getHDWIPTableData("");
			jsonObject.put("aaData", jsonRowData);
			tickets = jsonObject.toString();
			break;
		case HD_WIP_OVERDUE:
			jsonRowData = getHDWIPOverDueTableData("");
			jsonObject.put("aaData", jsonRowData);
			tickets = jsonObject.toString();
			break;
		case HD_MONITOR_DATE_TIME:
			tickets = getHDMonitoringDateTableData("%", userId);
			break;

		default:
			break;
		}
		return tickets;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getHDWIPOverDueTableData()
	 */
	@Override
	public List<HDWIPTableDTO> getHDWIPOverDueTableData(String siteNum) {

		List<HDWIPTableVO> hdWIPTableDataVO = hdMainDAO.getHdWipTableData(
				HDTables.HD_WIP_OVERDUE_ID.getCubeId(), siteNum);

		List<HDWIPTableDTO> hdWIPTableData = new ArrayList<>();

		for (HDWIPTableVO tableVO : hdWIPTableDataVO) {

			if (tableVO.getUnderReview().equals("0")) {
				tableVO.setLastTd("<span class='greenIcon'>&nbsp;&nbsp;&nbsp;&nbsp;</span>");
			} else {
				tableVO.setLastTd("<span class='redIcon'>&nbsp;&nbsp;&nbsp;&nbsp;</span>");
			}

			HDWIPTableDTO hdWIPTableDTO = new HDWIPTableDTO();

			hdWIPTableDTO.setClientType(tableVO.getClientType());
			hdWIPTableDTO.setMmId(tableVO.getMmId());
			hdWIPTableDTO.setMmIdForDisplay(tableVO.getMmId());
			try {
				org.apache.commons.beanutils.BeanUtils.copyProperties(
						hdWIPTableDTO, tableVO);
			} catch (IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}

			hdWIPTableDTO.setEventColor(tableVO.getEventColor());

			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd HH:mm");
			hdWIPTableDTO.setNextActionDueDate(dateFormat.format(tableVO
					.getNextActionDueDateDB()));

			hdWIPTableDTO.setDueDate(tableVO.getNextActionDueDateDB());

			hdWIPTableDTO.setOpenedDate(dateFormat.format(tableVO
					.getOpenedDbDate()));

			if (StringUtils.isNotBlank(hdWIPTableDTO.getCca_tss())) {
				if (hdWIPTableDTO.getCca_tss().length() >= 10) {
					hdWIPTableDTO.setCca_tssForDisplay(hdWIPTableDTO
							.getCca_tss().substring(0, 10));
				} else {
					hdWIPTableDTO.setCca_tssForDisplay(hdWIPTableDTO
							.getCca_tss());
				}
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getNextAction())) {
				if (hdWIPTableDTO.getNextAction().length() >= 16) {
					if (hdWIPTableDTO.isExtSystemStatus()) {
						hdWIPTableDTO
								.setNextActionForDisplay(hdWIPTableDTO
										.getNextAction().substring(0, 16)
										+ "<span class='snSymbol' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");
					} else {
						hdWIPTableDTO.setNextActionForDisplay(hdWIPTableDTO
								.getNextAction().substring(0, 16));
					}

				} else {
					if (hdWIPTableDTO.isExtSystemStatus()) {
						hdWIPTableDTO
								.setNextActionForDisplay(hdWIPTableDTO
										.getNextAction()
										+ "<span class='snSymbol' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");
					} else {
						hdWIPTableDTO.setNextActionForDisplay(hdWIPTableDTO
								.getNextAction());
					}

				}
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getCustomer())) {
				if (hdWIPTableDTO.getCustomer().length() >= 13) {
					hdWIPTableDTO.setCustomerForDisplay(hdWIPTableDTO
							.getCustomer().substring(0, 13));
				} else {
					hdWIPTableDTO.setCustomerForDisplay(hdWIPTableDTO
							.getCustomer());
				}
			}

			String img = "<img style=\"float:left; margin-right:3px;\" src=\"/Ilex-WS/resources/images/details.png\" class=\"siteDetails\"/>";

			String link = "window.open('/Ilex/masterSiteAction.do?siteName="
					+ hdWIPTableDTO.getSiteNumber().replace("#", "-,-,-")
					+ "&mmId=" + hdWIPTableDTO.getMmId()
					+ "', '_blank', 'width=1100,height=700,scrollbars=yes')";

			String siteLinkStart = "<a style='float:left; width:100px;word-break: keep-all;' href=\"javascript:void(0);\" onclick=\""
					+ link + "\" >";
			String siteLinkEnd = "</a>";

			siteLinkStart = img + " " + siteLinkStart;
			String prop = hdWIPTableDTO.getSite_prop();
			if (prop.equals("1")) {

				hdWIPTableDTO.setSite_prop("Y");

			} else {
				hdWIPTableDTO.setSite_prop("N");
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getSiteNumber())) {
				if (hdWIPTableDTO.getSiteNumber().length() >= 16) {
					hdWIPTableDTO.setSiteNumberDisplay(hdWIPTableDTO
							.getSiteNumber().substring(0, 16));

				} else {
					hdWIPTableDTO.setSiteNumberDisplay(hdWIPTableDTO
							.getSiteNumber());
				}
			}

			String siteNo = hdWIPTableDTO.getSiteNumberDisplay();
			String imgtag = "<img style=\"border:0; cursor:default; \" src=\"/Ilex-WS/resources/images/priorities.jpg\"/>";
			if ("2".equals(hdWIPTableDTO.getTicketSource())) {

				if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
						&& hdWIPTableDTO.getPrimaryState().toLowerCase()
								.contains("up")) {
					if (prop.equals("1")) {
						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='greenRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>"
										+ imgtag);
					} else {
						if (prop.equals("0")) {
							hdWIPTableDTO
									.setSiteNumberDisplay(siteLinkStart
											+ siteNo
											+ siteLinkEnd
											+ "<span class='greenRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");

						}
					}
				} else if (StringUtils.isNotBlank(hdWIPTableDTO
						.getPrimaryState())
						&& hdWIPTableDTO.getPrimaryState().toLowerCase()
								.contains("down")) {
					if (prop.equals("1")) {

						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='redRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>"
										+ imgtag);
					} else {

						if (prop.equals("0")) {

							hdWIPTableDTO
									.setSiteNumberDisplay(siteLinkStart
											+ siteNo
											+ siteLinkEnd
											+ "<span class='redRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");

						}

					}

				} else {

					if (prop.equals("1")) {

						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='redRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>"
										+ imgtag);
					} else {

						if (prop.equals("0")) {

							hdWIPTableDTO
									.setSiteNumberDisplay(siteLinkStart
											+ siteNo
											+ siteLinkEnd
											+ "<span class='redRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");

						}

					}
				}

			} else {
				if (prop.equals("1")) {

					hdWIPTableDTO
							.setSiteNumberDisplay(siteLinkStart
									+ siteNo
									+ siteLinkEnd
									+ "<span class='redRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>"
									+ imgtag);
				} else {

					if (prop.equals("0")) {

						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='redRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");

					}

				}
			}
			if (hdWIPTableDTO.isNextActionOverDue()) {
				hdWIPTableDTO.setOverdueStatus("overdue");
			} else {

				hdWIPTableDTO.setOverdueStatus("pending");
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
					&& hdWIPTableDTO.getPrimaryState().toLowerCase()
							.contains("up")) {
				hdWIPTableDTO.setSiteStatus("up");
			} else if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
					&& hdWIPTableDTO.getPrimaryState().toLowerCase()
							.contains("down")) {
				hdWIPTableDTO.setSiteStatus("down");
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getShortDescription())) {
				if (hdWIPTableDTO.getShortDescription().length() >= 16) {
					hdWIPTableDTO.setShortDescriptionDisplay(hdWIPTableDTO
							.getShortDescription().substring(0, 16));
				} else {
					hdWIPTableDTO.setShortDescriptionDisplay(hdWIPTableDTO
							.getShortDescription());
				}
			}
			hdWIPTableData.add(hdWIPTableDTO);
		}

		return hdWIPTableData;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getHDWIPNextActionTableData()
	 */
	@Override
	public List<HDWIPTableDTO> getHDWIPNextActionTableData(String siteNum) {

		List<HDWIPTableVO> hdWIPTableDataVO = hdMainDAO
				.getHdWipNextActionTableData(siteNum);

		List<HDWIPTableDTO> hdWIPTableData = new ArrayList<>();

		for (HDWIPTableVO tableVO : hdWIPTableDataVO) {

			if (tableVO.getUnderReview().equals("0")) {
				tableVO.setLastTd("<span class='greenIcon'>&nbsp;&nbsp;&nbsp;&nbsp;</span>");
			} else {
				tableVO.setLastTd("<span class='redIcon'>&nbsp;&nbsp;&nbsp;&nbsp;</span>");
			}

			HDWIPTableDTO hdWIPTableDTO = new HDWIPTableDTO();

			hdWIPTableDTO.setClientType(tableVO.getClientType());

			hdWIPTableDTO.setMmId(tableVO.getMmId());
			hdWIPTableDTO.setMmIdForDisplay(tableVO.getMmId());

			hdWIPTableDTO.setEventColor(tableVO.getEventColor());

			try {
				org.apache.commons.beanutils.BeanUtils.copyProperties(
						hdWIPTableDTO, tableVO);
			} catch (IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd HH:mm");
			hdWIPTableDTO.setNextActionDueDate(dateFormat.format(tableVO
					.getNextActionDueDateDB()));

			hdWIPTableDTO.setDueDate(tableVO.getNextActionDueDateDB());

			hdWIPTableDTO.setOpenedDate(dateFormat.format(tableVO
					.getOpenedDbDate()));

			if (StringUtils.isNotBlank(hdWIPTableDTO.getCca_tss())) {
				if (hdWIPTableDTO.getCca_tss().length() >= 10) {
					hdWIPTableDTO.setCca_tssForDisplay(hdWIPTableDTO
							.getCca_tss().substring(0, 10));
				} else {
					hdWIPTableDTO.setCca_tssForDisplay(hdWIPTableDTO
							.getCca_tss());
				}
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getNextAction())) {
				if (hdWIPTableDTO.getNextAction().length() >= 16) {
					if (hdWIPTableDTO.isExtSystemStatus()) {
						hdWIPTableDTO
								.setNextActionForDisplay(hdWIPTableDTO
										.getNextAction().substring(0, 16)
										+ "<span class='snSymbol' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");
					} else {
						hdWIPTableDTO.setNextActionForDisplay(hdWIPTableDTO
								.getNextAction().substring(0, 16));
					}

				} else {
					if (hdWIPTableDTO.isExtSystemStatus()) {
						hdWIPTableDTO
								.setNextActionForDisplay(hdWIPTableDTO
										.getNextAction()
										+ "<span class='snSymbol' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");
					} else {
						hdWIPTableDTO.setNextActionForDisplay(hdWIPTableDTO
								.getNextAction());
					}

				}
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getCustomer())) {
				if (hdWIPTableDTO.getCustomer().length() >= 13) {
					hdWIPTableDTO.setCustomerForDisplay(hdWIPTableDTO
							.getCustomer().substring(0, 13));
				} else {
					hdWIPTableDTO.setCustomerForDisplay(hdWIPTableDTO
							.getCustomer());
				}
			}

			String img = "<img style=\"float:left; margin-right:3px;\" src=\"/Ilex-WS/resources/images/details.png\" class=\"siteDetails\"/>";

			String link = "window.open('/Ilex/masterSiteAction.do?siteName="
					+ hdWIPTableDTO.getSiteNumber().replace("#", "-,-,-")
					+ "&mmId=" + hdWIPTableDTO.getMmId()
					+ "', '_blank', 'width=1100,height=700,scrollbars=yes')";

			String siteLinkStart = "<a style='float:left; width:100px;word-break: keep-all;' href=\"javascript:void(0);\" onclick=\""
					+ link + "\" >";
			String siteLinkEnd = "</a>";

			siteLinkStart = img + " " + siteLinkStart;
			String imgtag = "<img style=\"border:0;  cursor:default;\" src=\"/Ilex-WS/resources/images/priorities.jpg\"/>";
			String prop = hdWIPTableDTO.getSite_prop();
			if (prop.equals("1")) {

				hdWIPTableDTO.setSite_prop("Y");

			} else {
				hdWIPTableDTO.setSite_prop("N");
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getSiteNumber())) {
				if (hdWIPTableDTO.getSiteNumber().length() >= 16) {
					hdWIPTableDTO.setSiteNumberDisplay(hdWIPTableDTO
							.getSiteNumber().substring(0, 16));
				} else {
					hdWIPTableDTO.setSiteNumberDisplay(hdWIPTableDTO
							.getSiteNumber());
				}
			}

			String siteNo = hdWIPTableDTO.getSiteNumberDisplay();

			if ("2".equals(hdWIPTableDTO.getTicketSource())) {

				if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
						&& hdWIPTableDTO.getPrimaryState().toLowerCase()
								.contains("up")) {
					if (prop.equals("1")) {
						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='greenRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>"
										+ imgtag);

					} else {

						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='greenRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");

					}
				} else if (StringUtils.isNotBlank(hdWIPTableDTO
						.getPrimaryState())
						&& hdWIPTableDTO.getPrimaryState().toLowerCase()
								.contains("down")) {
					if (prop.equals("1")) {

						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='redRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>"
										+ imgtag);

					} else {

						hdWIPTableDTO
								.setSiteNumberDisplay(siteLinkStart
										+ siteNo
										+ siteLinkEnd
										+ "<span class='redRoundIcon primaryStateIcon' style='float:right;'>&nbsp;&nbsp;&nbsp;</span>");

					}
				} else {
					if (prop.equals("1")) {

						hdWIPTableDTO.setSiteNumberDisplay(siteLinkStart
								+ siteNo + siteLinkEnd + imgtag);

					} else {

						hdWIPTableDTO.setSiteNumberDisplay(siteLinkStart
								+ siteNo + siteLinkEnd);

					}
				}

			} else {

				if (prop.equals("1")) {
					hdWIPTableDTO.setSiteNumberDisplay(siteLinkStart + siteNo
							+ siteLinkEnd + imgtag);
				} else {

					hdWIPTableDTO.setSiteNumberDisplay(siteLinkStart + siteNo
							+ siteLinkEnd);
				}

			}

			if (hdWIPTableDTO.isNextActionOverDue()) {
				hdWIPTableDTO.setOverdueStatus("overdue");
			} else {

				hdWIPTableDTO.setOverdueStatus("pending");
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
					&& hdWIPTableDTO.getPrimaryState().toLowerCase()
							.contains("up")) {
				hdWIPTableDTO.setSiteStatus("up");
			} else if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
					&& hdWIPTableDTO.getPrimaryState().toLowerCase()
							.contains("down")) {
				hdWIPTableDTO.setSiteStatus("down");
			}
			if (StringUtils.isNotBlank(hdWIPTableDTO.getShortDescription())) {
				if (hdWIPTableDTO.getShortDescription().length() >= 16) {
					hdWIPTableDTO.setShortDescriptionDisplay(hdWIPTableDTO
							.getShortDescription().substring(0, 16));
				} else {
					hdWIPTableDTO.setShortDescriptionDisplay(hdWIPTableDTO
							.getShortDescription());
				}
			}
			hdWIPTableData.add(hdWIPTableDTO);
		}

		return hdWIPTableData;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getHDCustomers()
	 */
	@Override
	public Map<String, List<HDCustomerDetailDTO>> getHDCustomers() {

		List<HDCustomerDetailVO> hdCustomerVOS = hdMainDAO.getHDCustomers();

		List<HDCustomerDetailDTO> hdcustomers = new ArrayList<>();

		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		hdcustomers = awareBeanUtilsBean.copyList(HDCustomerDetailDTO.class,
				hdCustomerVOS);

		Map<String, List<HDCustomerDetailDTO>> hdCustomerMap = new LinkedHashMap<>();
		List<HDCustomerDetailDTO> hdCustomerList = new ArrayList<>();

		for (HDCustomerDetailDTO hdCustomer : hdcustomers) {
			String firstCharCustName = String.valueOf(hdCustomer
					.getCustomerName().toUpperCase().charAt(0));

			if (hdCustomerMap.get(firstCharCustName) == null) {
				hdCustomerList = new ArrayList<>();
				hdCustomerList.add(hdCustomer);
				hdCustomerMap.put(firstCharCustName, hdCustomerList);
			} else {
				hdCustomerList.add(hdCustomer);
			}
		}

		return hdCustomerMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getHDTicketDetail(java.lang.String)
	 */
	@Override
	public HDTicketDTO getHDTicketDetail(String hdCustomer) {
		String emailAddress = "";
		HDTicketDTO hdTicketDTO = new HDTicketDTO();

		String[] customerDetails = hdCustomer.split("~");
		Long msaId = Long.valueOf(customerDetails[0]);
		Long appendixId = Long.valueOf(customerDetails[1]);
		String customerName = customerDetails[2];
		String appendixName = customerDetails[3];
		String apppendixBreakOut = customerDetails[4];
		try {
			emailAddress = customerDetails[5];
		} catch (Exception ex) {
			emailAddress = "";
		}

		hdTicketDTO.setMsaId(msaId);
		hdTicketDTO.setAppendixId(appendixId);
		hdTicketDTO.setAppendixName(appendixName);
		hdTicketDTO.setCustomerName(customerName);
		hdTicketDTO.setEmailAddress(emailAddress);
		if (apppendixBreakOut.equalsIgnoreCase("NetMedX,MSP Help Desk")) {
			hdTicketDTO.setTicketType("MSP Help Desk");
			hdTicketDTO.setBillingStatus(false);

		} else if (apppendixBreakOut.equalsIgnoreCase("NetMedX,MSP Help Desk")) {
			hdTicketDTO.setTicketType("MSP Help Desk");
			hdTicketDTO.setBillingStatus(true);
		} else if (apppendixBreakOut
				.equalsIgnoreCase("Call/Problem Management Services")) {
			hdTicketDTO.setTicketType("Call/Problem Management");
			hdTicketDTO.setBillingStatus(true);
		}
		hdTicketDTO.setTicketSource("Manual");

		return hdTicketDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getSites(java.lang.Long)
	 */
	@Override
	public List<SiteDetailDTO> getSites(Long msaId) {
		List<SiteDetailVO> siteVOs = hdMainDAO.getSites(msaId);
		List<SiteDetailDTO> sites = new ArrayList<>();

		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		sites = awareBeanUtilsBean.copyList(SiteDetailDTO.class, siteVOs);

		return sites;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getIncidentState()
	 */
	@Override
	public List<IncidentStateDTO> getIncidentState(boolean all, String actual) {
		List<IncidentStateVO> labelValues = hdMainDAO.getIncidentStates(all,
				actual);
		List<IncidentStateDTO> incidentStateDTOs = new ArrayList<IncidentStateDTO>();
		IncidentStateDTO dto = null;

		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		incidentStateDTOs = awareBeanUtilsBean.copyList(IncidentStateDTO.class,
				labelValues);

		return incidentStateDTOs;
	}

	// get result from dao
	public List<IncidentStateDTO> getEmailAddress(Long all) {
		List<IncidentStateVO> labelValues = hdMainDAO.getEmailAddress(all,
				"Help Desk");
		List<IncidentStateDTO> incidentStateDTOs = new ArrayList<IncidentStateDTO>();
		IncidentStateDTO dto = null;
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		incidentStateDTOs = awareBeanUtilsBean.copyList(IncidentStateDTO.class,
				labelValues);

		return incidentStateDTOs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getResolutions()
	 */
	@Override
	public List<ResolutionsDTO> getResolutions() {
		List<ResolutionsVO> labelValues = hdMainDAO.getResolutions();
		List<ResolutionsDTO> resolutionsDTOs = new ArrayList<ResolutionsDTO>();
		ResolutionsDTO dto = null;

		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		resolutionsDTOs = awareBeanUtilsBean.copyList(ResolutionsDTO.class,
				labelValues);

		return resolutionsDTOs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getEffortStatus(java.lang.String)
	 */
	@Override
	public String getEffortStatus(String ticketId) {

		return hdMainDAO.getEffortStatus(ticketId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getConfigurationItems()
	 */
	@Override
	public List<HDConfigurationItemDTO> getConfigurationItems() {
		List<HDConfigurationItemVO> configurationItemVOs = hdMainDAO
				.getConfigurationItems();
		List<HDConfigurationItemDTO> configurationItems = new ArrayList<>();

		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		configurationItems = awareBeanUtilsBean.copyList(
				HDConfigurationItemDTO.class, configurationItemVOs);
		return configurationItems;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getCategories()
	 */
	@Override
	public List<HDCategoryDTO> getCategories() {
		List<HDCategoryVO> categoryVOs = hdMainDAO.getCategories();
		List<HDCategoryDTO> categories = new ArrayList<>();

		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		categories = awareBeanUtilsBean.copyList(HDCategoryDTO.class,
				categoryVOs);
		return categories;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#sendEmail(com.ilex.ws.core.bean.HDTicketVO
	 * , java.lang.String)
	 */
	@Override
	public void sendEmail(HDTicketVO hdTicketVO, String userId,
			String createOrUpdate) {

		String siteNum = hdMainDAO.getSiteNumber(hdTicketVO.getSiteId());
		String Header = "";
		String div = "";
		String url = "";

		div = "<div style=\"background-color:#ffffff,color:#2a6ebb, width=100%, padding-left: 25px;\">"
				+ url + "   </div>";

		String subject = "(" + hdTicketVO.getJobId() + "-" + userId + ")-"
				+ hdTicketVO.getTicketNumber() + "-"
				+ hdTicketVO.getShortDescription();

		EmailBean emailBean = new EmailBean();
		emailBean.setContentType("text/html");

		if (StringUtils.isNotBlank(emailUserName)) {
			emailBean.setUsername(emailUserName);
		}
		if (StringUtils.isNotBlank(emailUserPassword)) {
			emailBean.setUserpassword(emailUserPassword);
		}
		emailBean.setSmtpservername(emailSmtpServerName);
		if (StringUtils.isNotBlank(emailSmtpServerPort)) {
			emailBean.setSmtpserverport(emailSmtpServerPort);
		}

		LoginForm userDetails = loginDAO.getUserInfoByUserId(userId);

		emailBean.setTo(hdTicketVO.getEmailAddress() + ", "
				+ userDetails.getLogin_user_email());
		emailBean.setFrom("cma@cinnova.com");
		emailBean.setSubject(subject);

		String body = "";

		if (createOrUpdate.equals("create")) {

			SiteDetailDTO siteDetailDTO = getSiteDetails(hdTicketVO.getSiteId());

			body += "<p><b>Site Number: </b> " + siteDetailDTO.getSiteNumber()
					+ " </p>";

			if (siteDetailDTO.getSiteAddress() != null
					&& !siteDetailDTO.getSiteAddress().equals("")) {
				body += "<p><b>Street Address: </b> "
						+ siteDetailDTO.getSiteAddress() + " </p>";
			}

			if (siteDetailDTO.getSiteAddress() != null
					&& !siteDetailDTO.getSiteAddress().equals("")) {
				body += "<p><b>City, State, Zip: </b> "
						+ siteDetailDTO.getSiteCity() + ", "
						+ siteDetailDTO.getState() + ", "
						+ siteDetailDTO.getSiteZipcode() + " </p>";
			}

			body += "<p><b>Incident State: </b>New </p>";

			body += "\n\r<p><b>Priority </b>"
					+ getPriorityLabel(hdTicketVO.getPriority()) + " </p>";

			List<HDCategoryVO> catList = hdMainDAO.getCategories();
			for (int i = 0; i < catList.size(); i++) {
				HDCategoryVO vo = catList.get(i);
				if (vo.getCategoryId().equals(hdTicketVO.getCategory())) {
					body += "\n\r<p><b>Category: </b>" + vo.getCategory()
							+ " </p>";
					break;
				}
			}

			List<HDConfigurationItemVO> configItemList = hdMainDAO
					.getConfigurationItems();
			for (int i = 0; i < configItemList.size(); i++) {
				HDConfigurationItemVO vo = configItemList.get(i);
				if (vo.getConfigurationItemId().equals(
						hdTicketVO.getConfigurationItem())) {
					body += "\n\r<p><b>Configuration Item: </b>"
							+ vo.getConfigurationItem() + " </p>";
					break;
				}
			}

			body += "\n\r<p><b>Problem Description: </b>"
					+ hdTicketVO.getShortDescription() + " </p>";

		} else {

			if (!hdTicketVO.getLastNextAction().equals(
					hdTicketVO.getNextAction())) {
				List<HDNextActionDTO> lst = getNextActions();
				for (int i = 0; i < lst.size(); i++) {
					HDNextActionDTO dto = lst.get(i);
					if (dto.getNextActionId()
							.equals(hdTicketVO.getNextAction())) {
						body += "\n\r<p><b>Next Action: </b>"
								+ dto.getNextAction() + " </p>";
						break;
					}
				}

			}
			if (!hdTicketVO.getLastNextActionDueDate().equals(
					hdTicketVO.getNextActionDueDate().toString())) {
				body += "\n\r<p><b>Next Action Due: </b>"
						+ hdTicketVO.getNextActionDueDate() + " </p>";
			}
		}

		if (hdTicketVO.getLastTicketStatus() != null
				&& !hdTicketVO.getLastTicketStatus().equals(
						hdTicketVO.getTicketStatus())) {

			List<IncidentStateVO> labelValues = hdMainDAO.getIncidentStates(
					true, null);
			for (int i = 0; i < labelValues.size(); i++) {
				IncidentStateVO vo = labelValues.get(i);
				if (vo.getValue().equals(hdTicketVO.getTicketStatus())) {
					body += "<p><b>Incident State: </b> " + vo.getLabel()
							+ " </p>";
					break;
				}
			}

		}

		body += "\n\r<p><b>Short Description: </b>"
				+ hdTicketVO.getShortDescription() + " </p>";

		body += "\n\r<p><b>Work Notes: </b>" + hdTicketVO.getWorkNotes()
				+ " </p>";

		String sign = "\n\r <p> Sincerely <br />"
				+ userDetails.getLogin_user_name()
				+ "<br />Contingent Network Services<br />4400 Port Union Road<br />West Chester, OH 45011<br />(800) 506-9609<br />(513)860-2105(facsimile)<br />medius.contingent.com<br /><br />How are we doing?Please let us know! medius.contingent.com/satisfaction"
				+ "</p>";

		String divf = "<div style=\"background-color:#ffffff,color:#2a6ebb,  width=100%, padding-left: 25px;\">"
				+ url + "   </div>";
		emailBean.setContent(body + "" + sign + "" + divf);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#createTicket(com.ilex.ws.core.dto.
	 * HDTicketDTO, java.lang.String)
	 */
	@Override
	public void createTicket(HDTicketDTO hdTicketDTO, String userId) {

		HDTicketVO hdTicketVO = new HDTicketVO();

		hdTicketDTO.setPriority(getPriorityValue(hdTicketDTO.getPriority()));

		BeanUtils.copyProperties(hdTicketDTO, hdTicketVO);

		hdTicketVO.setSiteId(hdTicketDTO.getSiteDetailDTO().getSiteId());

		String nextActionUnit = hdTicketDTO.getCreateTicketNextActionUnit();
		if (StringUtils.isNotBlank(nextActionUnit)) {
			Calendar cal = Calendar.getInstance();

			if (nextActionUnit.equals("Mins")) {
				Double nextTimeMinutes = Math.ceil(Double.valueOf(hdTicketDTO
						.getCreateTicketNextActionTime()));
				cal.add(Calendar.MINUTE, nextTimeMinutes.intValue());
			} else if (nextActionUnit.equals("Hrs")) {
				Double nextTimeHours = Double.valueOf(hdTicketDTO
						.getCreateTicketNextActionTime());
				Double nextTimeMinutes = Math.ceil((nextTimeHours - Math
						.floor(nextTimeHours)) * 60);
				cal.add(Calendar.HOUR, nextTimeHours.intValue());
				cal.add(Calendar.MINUTE, nextTimeMinutes.intValue());
			}
			hdTicketVO.setNextActionDueDate(cal.getTime());

		} else {
			String nextActionTime = hdTicketDTO.getCreateTicketNextActionDate();
			hdTicketVO.setNextActionDueDate(DateUtils
					.convertStringToDate(nextActionTime));

		}

		Long siteDetailId = null;

		// Default to PPS For Manual Ticket Creation
		hdTicketVO.setCriticalityId(1);

		if (!StringUtils.isBlank(hdTicketDTO.getBackUpCktStatus())) {
			if ("Down".equals(hdTicketDTO.getBackUpCktStatus())) {
				hdTicketVO.setBackUpCktStatus("0");
			} else if ("Up".equals(hdTicketDTO.getBackUpCktStatus())) {
				hdTicketVO.setBackUpCktStatus("1");
			}

		}

		Long ticketId = hdMainDAO.insertTicketToIlex(hdTicketVO);

		hdMainDAO.insertHDTicketDetails(ticketId, hdTicketVO, userId);

		hdTicketVO.setTicketId(ticketId.toString());

		// save efforts and work notes
		hdActiveTicketLogic.saveEffortsForHD(hdTicketVO.getTicketId(),
				hdTicketVO.getEfforts(), userId);

		hdMainDAO.saveWorkNotes(hdTicketVO.getTicketId(),
				hdTicketVO.getWorkNotes(), userId);

		saveAuditTrail(hdTicketVO, userId);

		hdTicketVO.setSiteId(hdTicketDTO.getSiteDetailDTO().getSiteId());

		hdJobStructureDAO.createJobStructure(hdTicketVO, Long.valueOf(userId),
				true);

		siteDetailId = Long.parseLong(hdTicketVO.getSiteId());

		hdMainDAO.updateSite(siteDetailId, hdTicketDTO.getContactName(),
				hdTicketDTO.getPhoneNumber(), hdTicketDTO.getEmailAddress());

		sendMailOnCreateUpdateTicket(hdTicketVO, userId, "create");

		// service now integration code
		createIncidentIlexToSn(hdTicketVO, userId);
	}

	/**
	 * Send mail on create update ticket.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 */
	private void sendMailOnCreateUpdateTicket(HDTicketVO hdTicketVO,
			String userId, String createOrUpdate) {

		if (!hdTicketVO.getTicketStatus().equals("Closed")) {

			if (hdTicketVO.getTicketStatus().equals("RES")) {
				hdTicketVO
						.setCallerEmail(hdTicketVO.getCallerEmail()
								+ ", ProjectCoordinator@contingent.net,  AccountingDept@contingent.net ");
			} else if (hdTicketVO.getTicketStatus().equals("DSP-R")) {
				hdTicketVO
						.setCallerEmail(hdTicketVO.getCallerEmail()
								+ ", ProjectCoordinator@contingent.net,  MSPDispatch@contingent.net ");
			} else if (hdTicketVO.getAssignmentGroupId().equals("3")) {
				hdTicketVO.setCallerEmail(hdTicketVO.getCallerEmail()
						+ ", NOCEngineers@contingent.com");
			} else if (hdTicketVO.getAssignmentGroupId().equals("4")) {
				hdTicketVO.setCallerEmail(hdTicketVO.getCallerEmail()
						+ ", Dispatch@contingent.com");
			} else if (hdTicketVO.getAssignmentGroupId().equals("5")) {
				hdTicketVO.setCallerEmail(hdTicketVO.getCallerEmail()
						+ ", PMO@contingent.com");
			}

			sendEmail(hdTicketVO, userId, createOrUpdate);
		}
	}

	/**
	 * Save audit trail.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 */
	private void saveAuditTrail(HDTicketVO hdTicketVO, String userId) {
		HDAuditTrailVO hdAuditTrailVO = new HDAuditTrailVO();

		hdAuditTrailVO.setBackUpCctState(hdTicketVO.getBackUpCktStatus());
		hdAuditTrailVO.setIncidentState(hdTicketVO.getTicketStatus());
		hdAuditTrailVO.setNextAction(hdTicketVO.getNextAction());
		hdAuditTrailVO.setPointOfFailure(hdTicketVO.getPointOfFailureId());
		hdAuditTrailVO.setResolution(hdTicketVO.getResolutionId());
		hdAuditTrailVO.setRootCause(hdTicketVO.getRootCauseId());
		hdAuditTrailVO.setShortDesc(hdTicketVO.getShortDescription());
		hdAuditTrailVO.setTicketId(hdTicketVO.getTicketNumber());
		hdAuditTrailVO.setUpdatedBy(userId);

		hdMainDAO.saveAuditTrail(hdAuditTrailVO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#updateTicket(com.ilex.ws.core.dto.
	 * HDTicketDTO, java.lang.String)
	 */
	@Override
	public void updateTicket(HDTicketDTO hdTicketDTO, String userId) {
		HDTicketVO hdTicketVO = new HDTicketVO();

		hdTicketDTO.setPriority(getPriorityValue(hdTicketDTO.getPriority()));

		BeanUtils.copyProperties(hdTicketDTO, hdTicketVO);

		hdTicketVO.setSiteId((hdTicketDTO.getSiteDetailDTO().getSiteId()
				.isEmpty()) ? hdTicketDTO.getSiteId() : hdTicketDTO
				.getSiteDetailDTO().getSiteId());

		String nextActionUnit = hdTicketDTO.getCreateTicketNextActionUnit();
		String nextActionTime = hdTicketDTO.getCreateTicketNextActionDate();

		if ((StringUtils.isBlank(nextActionUnit) || StringUtils
				.isEmpty(nextActionUnit))
				&& (StringUtils.isBlank(nextActionTime) || StringUtils
						.isEmpty(nextActionTime))) {
			nextActionUnit = "0";
		}

		if (StringUtils.isNotBlank(nextActionUnit)) {
			Calendar cal = Calendar.getInstance();

			if (nextActionUnit.equals("Mins")) {
				Double nextTimeMinutes = Math.ceil(Double.valueOf(hdTicketDTO
						.getCreateTicketNextActionTime()));
				cal.add(Calendar.MINUTE, nextTimeMinutes.intValue());
			} else if (nextActionUnit.equals("Hrs")) {
				Double nextTimeHours = Double.valueOf(hdTicketDTO
						.getCreateTicketNextActionTime());
				Double nextTimeMinutes = Math.ceil((nextTimeHours - Math
						.floor(nextTimeHours)) * 60);
				cal.add(Calendar.HOUR, nextTimeHours.intValue());
				cal.add(Calendar.MINUTE, nextTimeMinutes.intValue());
			}
			hdTicketVO.setNextActionDueDate(cal.getTime());

		} else {
			nextActionTime = hdTicketDTO.getCreateTicketNextActionDate();

			hdTicketVO.setNextActionDueDate(DateUtils
					.convertStringToDate(nextActionTime));

		}

		Long siteDetailId = null;

		// Default to PPS For Manual Ticket Creation
		hdTicketVO.setCriticalityId(1);

		// need to check here what id is coming from dto and vo
		// Long ticketId = hdMainDAO.insertTicketToIlex(hdTicketVO);
		Long ticketId = Long.parseLong(hdTicketDTO.getTicketId());

		if (!StringUtils.isBlank(hdTicketDTO.getBackUpCktStatus())) {
			if ("Down".equals(hdTicketDTO.getBackUpCktStatus())) {
				hdTicketVO.setBackUpCktStatus("0");
			} else if ("Up".equals(hdTicketDTO.getBackUpCktStatus())) {
				hdTicketVO.setBackUpCktStatus("1");
			}

		}

		hdMainDAO.updateHDTicketDetails(ticketId, hdTicketVO, userId);
		hdTicketVO.setTicketId(ticketId.toString());

		hdMainDAO.saveWorkNotes(hdTicketVO.getTicketId(),
				hdTicketVO.getWorkNotes(), userId);

		saveAuditTrail(hdTicketVO, userId);

		hdTicketVO.setSiteId(hdTicketDTO.getSiteId());

		String snErrorWhenClosingTicket = updateIncidentFromIlexToSn(
				hdTicketVO.getTicketId(), userId);

		if (hdTicketDTO.getTicketStatus().equals("Closed")
				&& !snErrorWhenClosingTicket.equals("")) {
			hdTicketDTO.setSnErrorWhileClosingTicket(snErrorWhenClosingTicket);
		}

		hdTicketVO.setJobId(hdMainDAO.getJobIdByTicketNumber(hdTicketVO
				.getTicketNumber()));
		sendMailOnCreateUpdateTicket(hdTicketVO, userId, "update");

		hdMainDAO.updateSite(Long.parseLong(hdTicketVO.getSiteId()),
				hdTicketDTO.getContactName(), hdTicketDTO.getPhoneNumber(),
				hdTicketDTO.getEmailAddress());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getAckTicketDetails(java.lang.String)
	 */
	@Override
	public HDTicketDTO getAckTicketDetails(String ticketId) {
		HDTicketDTO hdTicketDTO = new HDTicketDTO();
		List<HDTicketVO> hdTicketVOList = new ArrayList<>();
		HDTicketVO hdTicketVO = new HDTicketVO();
		SiteDetailDTO siteDetailDTO = new SiteDetailDTO();

		hdTicketVOList = hdMainDAO.getAckCustTicketDetails(ticketId);

		if (!hdTicketVOList.isEmpty()) {

			BeanUtils.copyProperties(hdTicketVOList.get(0), hdTicketDTO);
			hdTicketVO = hdTicketVOList.get(0);
		}

		if (StringUtils.isNotBlank(hdTicketVO.getState())
				&& hdTicketVO.getState().toLowerCase().contains("up")) {
			hdTicketDTO.setPrimaryStatus("up");
		} else {
			hdTicketDTO.setPrimaryStatus("down");
		}

		if (StringUtils.isBlank(hdTicketDTO.getSiteId())) {
			siteDetailDTO.setSiteNumber(hdTicketVO.getSiteNumberTemp());
			siteDetailDTO.setSiteAddress(hdTicketVO.getSiteAddressTemp());
			siteDetailDTO.setSiteCity(hdTicketVO.getSiteCityTemp());
			siteDetailDTO.setState(hdTicketVO.getSiteStateTemp());
			siteDetailDTO.setCountry(hdTicketVO.getSiteCountryTemp());
			siteDetailDTO.setSiteZipcode(hdTicketVO.getSiteZipcodeTemp());
		} else {
			siteDetailDTO.setSiteNumber(hdTicketVO.getSiteNumber());
			siteDetailDTO.setSiteAddress(hdTicketVO.getSiteAddress());
			siteDetailDTO.setSiteCity(hdTicketVO.getSiteCity());
			siteDetailDTO.setState(hdTicketVO.getSiteState());
			siteDetailDTO.setCountry(hdTicketVO.getSiteCountry());
			siteDetailDTO.setSiteZipcode(hdTicketVO.getSiteZipcode());
		}

		if (!StringUtils.isBlank(siteDetailDTO.getCountry())) {
			if (siteDetailDTO.getCountry().equals("US")) {
				siteDetailDTO.setCountry("");
			}
		}

		if (hdTicketVO.getPreferredArrivalTimeDate() != null) {
			hdTicketDTO.setPreferredArrivalTime(DateUtils.convertDateToString(
					hdTicketVO.getPreferredArrivalTimeDate(),
					"MM-dd-yyyy HH:mm"));
		}

		if (hdTicketVO.getPreferredArrivalWindowFromDate() != null) {
			hdTicketDTO.setPreferredArrivalWindowFrom(DateUtils
					.convertDateToString(
							hdTicketVO.getPreferredArrivalWindowFromDate(),
							"MM-dd-yyyy HH:mm"));
		}

		if (hdTicketVO.getPreferredArrivalWindowToDate() != null) {
			hdTicketDTO.setPreferredArrivalWindowTo(DateUtils
					.convertDateToString(
							hdTicketVO.getPreferredArrivalWindowToDate(),
							"MM-dd-yyyy HH:mm"));
		}

		if (hdTicketVO.getNextActionDueDate() != null) {
			hdTicketDTO.setCreateTicketNextActionDate(DateUtils
					.convertDateToString(hdTicketVO.getNextActionDueDate(),
							"MM-dd-yyyy HH:mm"));
		}

		String ticketType = hdTicketDTO.getTicketType();
		if (ticketType.equalsIgnoreCase("NetMedX,MSP Help Desk")
				|| ticketType.equalsIgnoreCase("NetMedX,MSP Help Desk")) {
			hdTicketDTO.setTicketType("MSP Help Desk");
		} else if (ticketType
				.equalsIgnoreCase("Call/Problem Management Services")) {
			hdTicketDTO.setTicketType("Call/Problem Management");
		}

		hdTicketDTO.setSiteDetailDTO(siteDetailDTO);
		hdTicketDTO.setWorkNotes("");// (CTG-12701) Remove automated note in HD
		// comments section
		return hdTicketDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getSiteDetails(java.lang.String)
	 */
	@Override
	public SiteDetailDTO getSiteDetails(String siteId) {
		SiteDetailDTO siteDetailDTO = new SiteDetailDTO();
		SiteDetailVO siteDetailVO = hdMainDAO.getSiteDetails(siteId);
		BeanUtils.copyProperties(siteDetailVO, siteDetailDTO);
		return siteDetailDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getHDPendingAckAlertTableData()
	 */
	@Override
	public String getHDPendingAckAlertTableData(String siteNum) {
		List<HDPendingAckTableDTO> jsonRowData = new ArrayList<>();
		List<HDPendingAckTableVO> hdPendingAckTableVOList = hdMainDAO
				.getPendingAcKAlertData(siteNum);
		HDPendingAckTableDTO hdPendingAckTableDTO = new HDPendingAckTableDTO();

		for (HDPendingAckTableVO hdPendingAckTableVO : hdPendingAckTableVOList) {
			hdPendingAckTableDTO = new HDPendingAckTableDTO();
			BeanUtils.copyProperties(hdPendingAckTableVO, hdPendingAckTableDTO);
			if (!StringUtils.isBlank(hdPendingAckTableVO.getOpenedDateDB()
					.toString())) {
				hdPendingAckTableDTO.setOpenedDate(DateUtils
						.convertDateToString(
								hdPendingAckTableVO.getOpenedDateDB(),
								"MM/dd HH:mm"));
			}
			String state = hdPendingAckTableVO.getState();
			if (StringUtils.isNotBlank(state)) {
				if (state.length() >= 11) {
					state = state.substring(0, 11);
				}
			}

			hdPendingAckTableDTO.setState(state);
			String prop = hdPendingAckTableDTO.getSite_prop();
			if (prop.equals("1")) {

				hdPendingAckTableDTO.setSite_prop("Y");

			} else {

				hdPendingAckTableDTO.setSite_prop("N");

			}
			String imgtag = "<img style=\"border:0;cursor:default; \" src=\"/Ilex-WS/resources/images/priorities.jpg\"/>";
			if (StringUtils.isNotBlank(hdPendingAckTableDTO.getCustomer())) {
				if (hdPendingAckTableDTO.getCustomer().length() >= 13) {
					String customer = hdPendingAckTableDTO.getCustomer()
							.substring(0, 13);
					hdPendingAckTableDTO.setCustomer(customer);
				}
			}
			if (StringUtils.isNotBlank(hdPendingAckTableDTO.getSiteNumber())) {
				if (hdPendingAckTableDTO.getSiteNumber().length() >= 16) {
					if (prop.equals("1")) {

						hdPendingAckTableDTO
								.setSiteNumberDisplay(hdPendingAckTableDTO
										.getSiteNumber().substring(0, 16)
										+ imgtag);
					} else {

						hdPendingAckTableDTO
								.setSiteNumberDisplay(hdPendingAckTableDTO
										.getSiteNumber().substring(0, 16));
					}
				} else {
					if (prop.equals("1")) {

						hdPendingAckTableDTO
								.setSiteNumberDisplay(hdPendingAckTableDTO
										.getSiteNumber() + imgtag);
					} else {
						hdPendingAckTableDTO
								.setSiteNumberDisplay(hdPendingAckTableDTO
										.getSiteNumber());

					}
				}
			}
			jsonRowData.add(hdPendingAckTableDTO);
		}

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("aaData", jsonRowData);
		String data = jsonObject.toString();
		return data;
	}

	@Override
	public int getPendingAckTier2count(String userId) {
		List<HDPendingAckTableVO> hdPendingAckTableVOList = hdMainDAO
				.getPendingAcKMonitorData("", " IN ");
		if (hdPendingAckTableVOList == null
				|| hdPendingAckTableVOList.size() == 0) {
			return 0;
		}

		for (int i = 0; i < hdPendingAckTableVOList.size(); i++) {
			HDPendingAckTableVO pendingAckTableVO = hdPendingAckTableVOList
					.get(i);
			int diff = Integer.parseInt(pendingAckTableVO.getDateDiff());

			// if (diff >= 1440 && pendingAckTableVO.getState().equals("Up")) {
			// /*
			// * 1440 means 24 hours. if state is Up and 24 hours passed then
			// * cancel the ticket.
			// */
			// saveNoAction(pendingAckTableVO.getTicketId(), userId);
			// hdPendingAckTableVOList.remove(i);
			//
			// }
		}
		return hdPendingAckTableVOList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getHDPendingAckMonitorTableData()
	 */
	@Override
	public String getHDMonitoringDateTableData(String siteNum, String userId) {
		List<HDPendingAckTableDTO> jsonRowData = new ArrayList<>();
		List<HDPendingAckTableVO> hdPendingAckTableVOList = hdMainDAO
				.getMonitoringDateData(siteNum);
		HDPendingAckTableDTO hdPendingAckTableDTO = new HDPendingAckTableDTO();

		for (HDPendingAckTableVO hdPendingAckTableVO : hdPendingAckTableVOList) {
			hdPendingAckTableDTO = new HDPendingAckTableDTO();
			BeanUtils.copyProperties(hdPendingAckTableVO, hdPendingAckTableDTO);
			if (!StringUtils.isBlank(hdPendingAckTableVO.getOpenedDateDB()
					.toString())) {
				hdPendingAckTableDTO.setOpenedDate(DateUtils
						.convertDateToString(
								hdPendingAckTableVO.getOpenedDateDB(),
								"MM/dd HH:mm"));
			}
			String state = hdPendingAckTableDTO.getSiteState();

			hdPendingAckTableDTO.setState(state);

			hdPendingAckTableDTO.setNextActionLabel(hdPendingAckTableVO
					.getNextActionLabel());

			String prop = hdPendingAckTableDTO.getSite_prop();

			if (prop.equals("1")) {

				hdPendingAckTableDTO.setSite_prop("Y");

			} else {

				hdPendingAckTableDTO.setSite_prop("N");
			}
			hdPendingAckTableDTO
					.setCustomer("<a href='#' style='text-decoration: underline;' onclick='openTicketSummaryWithUser("
							+ hdPendingAckTableDTO.getTicketId()
							+ ", "
							+ userId
							+ ");'>"
							+ hdPendingAckTableDTO.getIlexNumber() + "</a>");

			String imgtag = "<img style=\"border:0; cursor:default;\" src=\"/Ilex-WS/resources/images/priorities.jpg\"/>";
			if (prop.equals("1")) {
				hdPendingAckTableDTO.setSiteNumberDisplay(hdPendingAckTableDTO
						.getSiteNumber() + imgtag);
			} else {

				hdPendingAckTableDTO.setSiteNumberDisplay(hdPendingAckTableDTO
						.getSiteNumber());
			}

			hdPendingAckTableDTO.setDateDiffWithoutIcon(hdPendingAckTableVO
					.getDateDiff());
			String img = "<img style=\"float:left; margin-right:3px;\" src=\"/Ilex-WS/resources/images/details.png\" class=\"siteDetailzzz\"/>";
			hdPendingAckTableDTO.setDateDiff(hdPendingAckTableVO.getDateDiff()
					+ img);

			jsonRowData.add(hdPendingAckTableDTO);
		}

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("aaData", jsonRowData);
		String data = jsonObject.toString();
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getHDPendingAckMonitorTableData()
	 */
	@Override
	public String getHDPendingAckMonitorTableData(String siteNum,
			String inClause) {
		List<HDPendingAckTableDTO> jsonRowData = new ArrayList<>();
		List<HDPendingAckTableVO> hdPendingAckTableVOList = hdMainDAO
				.getPendingAcKMonitorData(siteNum, inClause);
		HDPendingAckTableDTO hdPendingAckTableDTO = new HDPendingAckTableDTO();

		for (HDPendingAckTableVO hdPendingAckTableVO : hdPendingAckTableVOList) {
			hdPendingAckTableDTO = new HDPendingAckTableDTO();
			BeanUtils.copyProperties(hdPendingAckTableVO, hdPendingAckTableDTO);
			if (!StringUtils.isBlank(hdPendingAckTableVO.getOpenedDateDB()
					.toString())) {
				hdPendingAckTableDTO.setOpenedDate(DateUtils
						.convertDateToString(
								hdPendingAckTableVO.getOpenedDateDB(),
								"MM/dd HH:mm"));
			}
			String state = hdPendingAckTableDTO.getState();

			if (inClause.equals("in")) {
				hdPendingAckTableDTO
						.setState("<a href='#' style='text-decoration: underline;' onclick='ackTicketStep1(\"hdPendingAckMonitorTier2\", this);'>"
								+ state + "</a>");
			} else {
				hdPendingAckTableDTO
						.setState("<a href='#' style='text-decoration: underline;' onclick='ackTicketStep1(\"hdPendingAckMonitor\", this);'>"
								+ state + "</a>");
			}

			if (StringUtils.isNotBlank(hdPendingAckTableDTO.getCustomer())) {
				if (hdPendingAckTableDTO.getCustomer().length() >= 13) {
					String customer = hdPendingAckTableDTO.getCustomer()
							.substring(0, 13);
					hdPendingAckTableDTO.setCustomer(customer);
				}
			}
			String imgtag = "<img style=\"border:0; cursor:default; \" src=\"/Ilex-WS/resources/images/priorities.jpg\"/>";
			String prop = hdPendingAckTableVO.getSite_prop();
			if (prop.equals("1")) {

				hdPendingAckTableVO.setSite_prop("Y");

			} else {
				hdPendingAckTableVO.setSite_prop("N");
			}
			String displayName = "N/A";
			if (StringUtils.isNotBlank(hdPendingAckTableDTO
					.getSiteDisplayName())) {
				displayName = hdPendingAckTableDTO.getSiteDisplayName();
			} else if (StringUtils.isNotBlank(hdPendingAckTableDTO
					.getSiteNumber())) {
				displayName = hdPendingAckTableDTO.getSiteNumber();
			}

			if (displayName.length() >= 16) {
				if (prop.equals("1")) {
					hdPendingAckTableDTO.setSiteNumberDisplay(displayName
							.substring(0, 16) + imgtag);
				} else {

					hdPendingAckTableDTO.setSiteNumberDisplay(displayName
							.substring(0, 16));

				}
			} else {
				if (prop.equals("1")) {
					hdPendingAckTableDTO.setSiteNumberDisplay(displayName
							+ imgtag);
				} else {
					hdPendingAckTableDTO.setSiteNumberDisplay(displayName);

				}
			}

			jsonRowData.add(hdPendingAckTableDTO);
		}

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("aaData", jsonRowData);
		String data = jsonObject.toString();
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getNextActions()
	 */
	@Override
	public List<HDNextActionDTO> getNextActions() {
		List<HDNextActionDTO> nextActions = new ArrayList<>();
		List<HDNextActionVO> nextActionVOs = hdMainDAO.getNextActions();
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		nextActions = awareBeanUtilsBean.copyList(HDNextActionDTO.class,
				nextActionVOs);

		return nextActions;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getAuditTrail()
	 */
	@Override
	public List<HDAuditTrailDTO> getAuditTrail(String ticketId) {
		List<HDAuditTrailDTO> auditTrailDTOs = new ArrayList<>();
		List<HDAuditTrailVO> auditTrailVOs = hdMainDAO.getAuditTrail(ticketId);
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		auditTrailDTOs = awareBeanUtilsBean.copyList(HDAuditTrailDTO.class,
				auditTrailVOs);

		return auditTrailDTOs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#ackTicket(com.ilex.ws.core.dto.HDTicketDTO
	 * )
	 */
	@Override
	public void ackTicket(HDTicketDTO hdTicketDTO, String userId) {

		HDTicketVO hdTicketVO = new HDTicketVO();
		BeanUtils.copyProperties(hdTicketDTO, hdTicketVO);

		if (StringUtils.isNotBlank(hdTicketDTO.getCreateTicketNextActionUnit())) {
			hdActiveTicketLogic.updateNextAction(hdTicketDTO.getTicketId(),
					hdTicketDTO.getCreateTicketNextActionTime(),
					hdTicketDTO.getCreateTicketNextActionUnit(),
					hdTicketDTO.getNextAction(), userId);
		} else {
			hdTicketDTO.setCreateTicketNextActionUnit("Date");
			hdActiveTicketLogic.updateNextAction(hdTicketDTO.getTicketId(),
					hdTicketDTO.getCreateTicketNextActionDate(),
					hdTicketDTO.getCreateTicketNextActionUnit(),
					hdTicketDTO.getNextAction(), userId);
		}

		hdActiveTicketLogic.saveEfforts(hdTicketDTO.getTicketId(),
				hdTicketDTO.getEfforts(), userId);

		hdTicketVO.setLmCompleteFirstCall(false);
		hdMainDAO.ackTicket(hdTicketVO, userId);

		hdJobStructureDAO.createJobStructure(hdTicketVO, Long.valueOf(userId),
				false);

		hdMainDAO.saveWorkNotes(hdTicketDTO.getTicketId(),
				hdTicketDTO.getWorkNotes(), userId);
		// refresh cubes
		String[] cubesToUpdate = {
				HdCubeDisplayStatus.PA_MONITOR.getCubeName(),
				HdCubeDisplayStatus.PA_ALERT.getCubeName() };
		String cubesList = Util.changeStringToCommaSeperation(cubesToUpdate);
		hdMainDAO.updateCubeDisplayStatus(cubesList);
		// service now code
		if (hdTicketDTO.getAckCubeType().equalsIgnoreCase("hdPendingAckAlert")
				|| hdTicketDTO.getAckCubeType().equalsIgnoreCase(
						"hdPendingAckMonitor")) {
			createIncidentIlexToSn(hdTicketVO, userId);

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getActiveTickets(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<HDWIPTableDTO> getActiveTickets(String userId, String sessionId) {
		List<HDWIPTableVO> hdWIPTableDataVOList = hdMainDAO.getActiveTickets(
				userId, sessionId);
		List<HDWIPTableDTO> hdwipTableDTOList = new ArrayList<>();

		for (HDWIPTableVO hdWIPTableDataVO : hdWIPTableDataVOList) {
			HDWIPTableDTO hdWIPTableDTO = new HDWIPTableDTO();
			hdWIPTableDTO.setSiteNumber(hdWIPTableDataVO.getSiteNumber());
			hdWIPTableDTO.setCustomer(hdWIPTableDataVO.getCustomer());
			hdWIPTableDTO.setTicketId(hdWIPTableDataVO.getTicketId());
			hdWIPTableDTO.setPriority(hdWIPTableDataVO.getPriority());
			hdWIPTableDTO.setCircuitStatus(hdWIPTableDataVO.getCircuitStatus());
			hdWIPTableDTO.setPrimaryState(hdWIPTableDataVO.getPrimaryState());

			if (hdWIPTableDataVO.isNextActionOverDue()) {
				hdWIPTableDTO.setOverdueStatus("overdue");
			} else {

				hdWIPTableDTO.setOverdueStatus("pending");
			}

			if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
					&& hdWIPTableDTO.getPrimaryState().toLowerCase()
							.contains("up")) {
				hdWIPTableDTO.setSiteStatus("up");
			} else if (StringUtils.isNotBlank(hdWIPTableDTO.getPrimaryState())
					&& hdWIPTableDTO.getPrimaryState().toLowerCase()
							.contains("down")) {
				hdWIPTableDTO.setSiteStatus("down");
			}
			hdwipTableDTOList.add(hdWIPTableDTO);
		}

		Map<String, String> wipTickets = new HashMap<>();
		int countDiv = 1;
		if (HDTicketPersistUtils.get(sessionId) == null) {
			HdPersistedTickets hdPersistedTickets = new HdPersistedTickets();

			hdPersistedTickets.setActiveTicketId("");
			for (HDWIPTableDTO hdWipTableDTO : hdwipTableDTOList) {
				switch (countDiv) {
				case 1:
					wipTickets.put(hdWipTableDTO.getTicketId(),
							ActionTicketDropDiv.WIP_1.getDivId());
					hdWipTableDTO
							.setDivId(ActionTicketDropDiv.WIP_1.getDivId());
					countDiv++;
					break;
				case 2:
					wipTickets.put(hdWipTableDTO.getTicketId(),
							ActionTicketDropDiv.WIP_2.getDivId());
					hdWipTableDTO
							.setDivId(ActionTicketDropDiv.WIP_2.getDivId());
					countDiv++;
					break;
				case 3:
					wipTickets.put(hdWipTableDTO.getTicketId(),
							ActionTicketDropDiv.WIP_3.getDivId());
					hdWipTableDTO
							.setDivId(ActionTicketDropDiv.WIP_3.getDivId());
					countDiv++;
					break;
				case 4:
					wipTickets.put(hdWipTableDTO.getTicketId(),
							ActionTicketDropDiv.WIP_4.getDivId());
					hdWipTableDTO
							.setDivId(ActionTicketDropDiv.WIP_4.getDivId());
					countDiv++;
					break;
				case 5:
					wipTickets.put(hdWipTableDTO.getTicketId(),
							ActionTicketDropDiv.WIP_5.getDivId());
					hdWipTableDTO
							.setDivId(ActionTicketDropDiv.WIP_5.getDivId());
					countDiv++;
					break;
				case 6:
					wipTickets.put(hdWipTableDTO.getTicketId(),
							ActionTicketDropDiv.WIP_6.getDivId());
					hdWipTableDTO
							.setDivId(ActionTicketDropDiv.WIP_6.getDivId());
					countDiv++;
					break;
				case 7:
					wipTickets.put(hdWipTableDTO.getTicketId(),
							ActionTicketDropDiv.WIP_7.getDivId());
					hdWipTableDTO
							.setDivId(ActionTicketDropDiv.WIP_7.getDivId());
					countDiv++;
					break;
				case 8:
					wipTickets.put(hdWipTableDTO.getTicketId(),
							ActionTicketDropDiv.WIP_8.getDivId());
					hdWipTableDTO
							.setDivId(ActionTicketDropDiv.WIP_8.getDivId());
					countDiv++;
					break;
				default:
					break;
				}

			}
			hdPersistedTickets.setWipTickets(wipTickets);
			HDTicketPersistUtils.persistUserTickets(sessionId,
					hdPersistedTickets);
		} else {
			wipTickets = HDTicketPersistUtils.get(sessionId).getWipTickets();
			for (HDWIPTableDTO hdWipTableDTO : hdwipTableDTOList) {
				if (hdWipTableDTO.getTicketId()
						.equals(HDTicketPersistUtils.get(sessionId)
								.getActiveTicketId())) {
					hdWipTableDTO.setIsActiveDiv(Boolean.TRUE);
				}
				if (wipTickets.containsKey(hdWipTableDTO.getTicketId())) {
					hdWipTableDTO.setDivId(wipTickets.get(hdWipTableDTO
							.getTicketId()));
				}
			}
		}
		return hdwipTableDTOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getCubeStatus()
	 */
	@Override
	public HDCubeStatusDTO getCubeStatus() {
		HDCubeStatusVO hdCubeStatusVO = new HDCubeStatusVO();
		HDCubeStatusDTO hdCubeStatusDTO = new HDCubeStatusDTO();
		hdCubeStatusVO = hdMainDAO.getCubeStatus();

		BeanUtils.copyProperties(hdCubeStatusVO, hdCubeStatusDTO);
		return hdCubeStatusDTO;
	}

	@Override
	public boolean getNextActionDueStatus() {
		return hdMainDAO.getnextactionduestatus();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getTicketStates()
	 */
	@Override
	public List<TicketStateDTO> getTicketStates() {
		List<TicketStateVO> ticketStateVOList = hdMainDAO.getTicketStates();
		List<TicketStateDTO> ticketStateDTOList = new ArrayList<>();

		NullAwareBeanUtilsBean utilsBean = new NullAwareBeanUtilsBean();
		ticketStateDTOList = utilsBean.copyList(TicketStateDTO.class,
				ticketStateVOList);

		return ticketStateDTOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getCircuitStatus()
	 */
	@Override
	public List<CircuitStatusDTO> getCircuitStatus() {
		List<CircuitStatusVO> circuitStatusVOList = hdMainDAO
				.getCircuitStatus();
		List<CircuitStatusDTO> circuitStatusDTOList = new ArrayList<>();

		NullAwareBeanUtilsBean utilsBean = new NullAwareBeanUtilsBean();
		circuitStatusDTOList = utilsBean.copyList(CircuitStatusDTO.class,
				circuitStatusVOList);

		return circuitStatusDTOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getTicketSummary(java.lang.Long)
	 */
	@Override
	public HDTicketDTO getTicketSummary(Long ticketId) {
		HDTicketVO hdTicketVO = hdMainDAO.getTicketSummary(ticketId);
		hdTicketVO.setPriority(getPriorityLabel(hdTicketVO.getPriority()));
		HDTicketDTO hdTicketDTO = new HDTicketDTO();
		if (hdTicketVO != null) {
			BeanUtils.copyProperties(hdTicketVO, hdTicketDTO);

			hdTicketDTO.setCreateTicketNextActionDate(DateUtils
					.convertDateToString(hdTicketVO.getNextActionDueDate(),
							"MM/dd/yyyy HH:mm"));

			SiteDetailDTO siteDetailDTO = new SiteDetailDTO();

			siteDetailDTO.setSiteNumber(hdTicketVO.getSiteNumber());
			siteDetailDTO.setSiteAddress(hdTicketVO.getSiteAddress());
			siteDetailDTO.setSiteCity(hdTicketVO.getSiteCity());
			siteDetailDTO.setState(hdTicketVO.getSiteState());
			siteDetailDTO.setWorkNotes(hdTicketVO.getSiteWorkNotes());
			if (!StringUtils.isBlank(hdTicketVO.getSiteCountry())) {
				if (!hdTicketVO.getSiteCountry().equalsIgnoreCase("US")) {
					siteDetailDTO.setCountry(hdTicketVO.getSiteCountry());
				}
			}
			siteDetailDTO.setSiteZipcode(hdTicketVO.getSiteZipcode());
			hdTicketDTO.setSiteDetailDTO(siteDetailDTO);

			if (!StringUtils.isBlank(hdTicketVO.getBackUpCktStatus())) {
				if ("0".equals(hdTicketVO.getBackUpCktStatus())) {
					hdTicketDTO.setBackUpCktStatus("Down");
				} else if ("1".equals(hdTicketVO.getBackUpCktStatus())) {
					hdTicketDTO.setBackUpCktStatus("Up");
				} else {
					hdTicketDTO.setBackUpCktStatus("Not Applicable");
				}
				if (StringUtils.isBlank(hdTicketVO.getDevice())) {
					hdTicketDTO.setDevice("Unknown");
				}
			} else {
				hdTicketDTO.setBackUpCktStatus("Not Applicable");
				hdTicketDTO.setDevice("Not Applicable");
			}

			if (!StringUtils.isBlank(hdTicketVO.getImpact())) {
				if ("1".equals(hdTicketVO.getImpact())) {
					hdTicketDTO.setImpact("1-High");
				} else if ("2".equals(hdTicketVO.getImpact())) {
					hdTicketDTO.setImpact("2-Medium");
				} else if ("3".equals(hdTicketVO.getImpact())) {
					hdTicketDTO.setImpact("3-Low");
				}
			}

			if (!StringUtils.isBlank(hdTicketVO.getUrgency())) {
				if ("1".equals(hdTicketVO.getUrgency())) {
					hdTicketDTO.setUrgency("1-High");
				} else if ("2".equals(hdTicketVO.getUrgency())) {
					hdTicketDTO.setUrgency("2-Medium");
				} else if ("3".equals(hdTicketVO.getUrgency())) {
					hdTicketDTO.setUrgency("3-Low");
				}
			}

			List<HDWorkNoteVO> hdWorkNoteVOList = hdMainDAO
					.getWorkNotes(ticketId);
			List<HDWorkNoteDTO> hdWorkNoteDTOList = new ArrayList<>();
			HDWorkNoteDTO hdWorkNoteDTO = null;
			for (HDWorkNoteVO hdWorkNoteVO : hdWorkNoteVOList) {
				hdWorkNoteDTO = new HDWorkNoteDTO();
				BeanUtils.copyProperties(hdWorkNoteVO, hdWorkNoteDTO);
				if (!StringUtils.isBlank(hdWorkNoteVO.getWorkNoteDate()
						.toString())) {
					hdWorkNoteDTO.setWorkNoteDateStr(DateUtils
							.convertDateToString(
									hdWorkNoteVO.getWorkNoteDate(),
									"MM/dd HH:mm"));
				}

				hdWorkNoteDTOList.add(hdWorkNoteDTO);
			}
			hdTicketDTO.setHdWorkNoteDTOList(hdWorkNoteDTOList);

			// Add Install Notes from table
			List<HDInstallNoteVO> hdInstallNoteVOList = hdMainDAO
					.getInstallNotes(ticketId);
			List<HDInstallNoteDTO> hdInstallNoteDTOList = new ArrayList<>();
			HDInstallNoteDTO hdInstallNoteDTO = null;
			for (HDInstallNoteVO hdInstallNoteVO : hdInstallNoteVOList) {
				hdInstallNoteDTO = new HDInstallNoteDTO();
				BeanUtils.copyProperties(hdInstallNoteVO, hdInstallNoteDTO);
				if (!StringUtils.isBlank(hdInstallNoteVO.getInstallNoteDate()
						.toString())) {
					hdInstallNoteDTO.setInstallNoteDateStr(DateUtils
							.convertDateToString(
									hdInstallNoteVO.getInstallNoteDate(),
									"MM/dd HH:mm"));
				}

				hdInstallNoteDTOList.add(hdInstallNoteDTO);
			}
			hdTicketDTO.setHdInstallNoteDTOList(hdInstallNoteDTOList);

			// ////End of Install Notes
			WugDetailsDTO wugDetailsDTO = new WugDetailsDTO();
			WugDetailsVO wugDetailsVO = hdMainDAO.getWUGInformation(hdTicketVO);
			BeanUtils.copyProperties(wugDetailsVO, wugDetailsDTO);
			hdTicketDTO.setWugDetailsDTO(wugDetailsDTO);

			List<HDEffortDTO> hdEffortDTOList = getEffortsDetail(ticketId);
			HDBillableVO hdBillableVO = hdMainDAO.getBillableInfo(ticketId);
			HdBillableDTO hdBillableDTO = new HdBillableDTO();
			BeanUtils.copyProperties(hdBillableVO, hdBillableDTO);
			hdTicketDTO.setHdEffortDTOList(hdEffortDTOList);
			hdTicketDTO.setHdBillableDTO(hdBillableDTO);

			ProvisioningDetailDTO provisioningDetailDTO = new ProvisioningDetailDTO();
			provisioningDetailDTO = getProvisioningDetail(hdTicketDTO
					.getSiteId());
			hdTicketDTO.setProvisioningDetailDTO(provisioningDetailDTO);

			// org.apache.commons.beanutils.BeanUtils.
			hdTicketDTO.setDeviceAttribList(getSiteAssetList(hdTicketDTO
					.getSiteId()));

		}
		return hdTicketDTO;
	}

	private List<MasterSiteAssetsDeployedDTO> getSiteAssetList(String siteId) {
		String data = null;
		List<MasterSiteAssetsDeployedDTO> lst = new ArrayList<MasterSiteAssetsDeployedDTO>();
		try {
			String myURL = "https://ccc.contingent.com/index.php?cmp=phone_ws&type=site_assets&site_id="
					+ siteId;
			data = getDataFromWeb(myURL, 9999, "\n");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		String[] dat = null;
		if (data != null && (!data.equals(""))
				&& !data.contains("mysql connection failed")) {
			dat = data.split("~");
		}
		if (dat != null) {
			MasterSiteAssetsDeployedDTO dto = null;
			for (int i = 0; i < dat.length; i++) {
				String[] ar = dat[i].split("\\|");
				dto = new MasterSiteAssetsDeployedDTO();
				dto.setClValue(ar[1]);
				dto.setDate(ar[0]);
				dto.setQuantity(ar[3]);
				dto.setSrNumber(ar[2]);
				lst.add(dto);
			}
		}
		return lst;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getEffortsDetail(java.lang.Long)
	 */
	@Override
	public List<HDEffortDTO> getEffortsDetail(Long ticketId) {
		NullAwareBeanUtilsBean utilsBean = new NullAwareBeanUtilsBean();
		List<HDEffortVO> hdEffortVOList = hdMainDAO.getEffortsDetail(ticketId);
		List<HDEffortDTO> hdEffortDTOList = new ArrayList<>();
		if (!hdEffortVOList.isEmpty()) {
			hdEffortDTOList = utilsBean.copyList(HDEffortDTO.class,
					hdEffortVOList);
		}
		return hdEffortDTOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getTicketDetailsForEdit(java.lang.
	 * String)
	 */
	@Override
	public HDTicketDTO getTicketDetailsForEdit(String ticketId) {
		HDTicketVO hdTicketVO = hdMainDAO.getTicketDetailsForEdit(ticketId);
		HDTicketDTO hdTicketDTO = new HDTicketDTO();

		BeanUtils.copyProperties(hdTicketVO, hdTicketDTO);
		return hdTicketDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#editTicketDetails(com.ilex.ws.core
	 * .dto.HDTicketDTO)
	 */
	@Override
	public void editTicketDetails(HDTicketDTO hdTicketDTO) {
		HDTicketVO hdTicketVO = new HDTicketVO();

		BeanUtils.copyProperties(hdTicketDTO, hdTicketVO);

		hdMainDAO.editTicketDetails(hdTicketVO);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getPriorityDetailsForEdit(java.lang
	 * .String)
	 */
	@Override
	public HDTicketDTO getPriorityDetailsForEdit(String ticketId) {
		HDTicketVO hdTicketVO = hdMainDAO.getPriorityDetailsForEdit(ticketId);
		HDTicketDTO hdTicketDTO = new HDTicketDTO();

		BeanUtils.copyProperties(hdTicketVO, hdTicketDTO);
		return hdTicketDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#editPrioritySection(com.ilex.ws.core
	 * .dto.HDTicketDTO, java.lang.String)
	 */
	@Override
	public void editPrioritySection(HDTicketDTO hdTicketDTO, String userId) {
		HDTicketVO hdTicketVO = new HDTicketVO();

		BeanUtils.copyProperties(hdTicketDTO, hdTicketVO);

		hdMainDAO.editPrioritySection(hdTicketVO, userId);

		saveWorkNotes(hdTicketDTO.getTicketId(), hdTicketDTO.getWorkNotes(),
				userId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getBackupDetailsForEdit(java.lang.
	 * String)
	 */
	@Override
	public HDTicketDTO getBackupDetailsForEdit(String ticketId) {
		HDTicketVO hdTicketVO = hdMainDAO.getBackupDetailsForEdit(ticketId);
		HDTicketDTO hdTicketDTO = new HDTicketDTO();

		BeanUtils.copyProperties(hdTicketVO, hdTicketDTO);
		return hdTicketDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#editBackupDetails(com.ilex.ws.core
	 * .dto.HDTicketDTO, java.lang.String)
	 */
	@Override
	public void editBackupDetails(HDTicketDTO hdTicketDTO, String userId) {
		HDTicketVO hdTicketVO = new HDTicketVO();

		BeanUtils.copyProperties(hdTicketDTO, hdTicketVO);

		hdMainDAO.editBackupDetails(hdTicketVO, userId);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getResolutionDetailsForEdit(java.lang
	 * .String)
	 */
	@Override
	public HDTicketDTO getResolutionDetailsForEdit(String ticketId) {
		HDTicketVO hdTicketVO = hdMainDAO.getResolutionDetailsForEdit(ticketId);
		HDTicketDTO hdTicketDTO = new HDTicketDTO();

		BeanUtils.copyProperties(hdTicketVO, hdTicketDTO);
		return hdTicketDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getPointOfFailureList()
	 */
	@Override
	public Map<String, String> getPointOfFailureList() {
		Map<String, String> pointOfFailureList = hdMainDAO
				.getPointOfFailureList();
		return pointOfFailureList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getEventCorrelator()
	 */
	@Override
	public List<EventCorrelatorDTO> getEventCorrelator(String type) {
		List<EventCorrelatorVO> vos = hdMainDAO.getEventCorrelator(type);

		List<EventCorrelatorDTO> dtos = new ArrayList<EventCorrelatorDTO>();

		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		dtos = awareBeanUtilsBean.copyList(EventCorrelatorDTO.class, vos);
		return dtos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getRootCauseList()
	 */
	@Override
	public Map<String, String> getRootCauseList() {
		Map<String, String> rootCauseList = hdMainDAO.getRootCauseList();
		return rootCauseList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getResolutionList()
	 */
	@Override
	public Map<String, String> getResolutionList() {
		Map<String, String> resolutionList = hdMainDAO.getResolutionList();
		return resolutionList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#editResolutionDetails(com.ilex.ws.
	 * core.dto.HDTicketDTO, java.lang.String)
	 */
	@Override
	public void editResolutionDetails(HDTicketDTO hdTicketDTO, String userId) {
		HDTicketVO hdTicketVO = new HDTicketVO();

		BeanUtils.copyProperties(hdTicketDTO, hdTicketVO);

		hdMainDAO.editResolutionDetails(hdTicketVO, userId);
		saveWorkNotes(hdTicketDTO.getTicketId(), hdTicketDTO.getWorkNotes(),
				userId);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getStateTail(java.lang.String)
	 */
	@Override
	public StateTailDTO getStateTail(String ticketId) {
		StateTailVO stateTailVO = hdMainDAO.getStateTail(ticketId);
		StateTailDTO stateTailDTO = new StateTailDTO();
		stateTailDTO.setDownMinutes(stateTailVO.getDownMinutes());
		List<WugDetailsDTO> wugDetailsDTOs = new ArrayList<>();
		WugDetailsDTO wugDetailsDTO;

		for (WugDetailsVO wugDetailsVO : stateTailVO.getWugDetailsVOList()) {

			wugDetailsDTO = new WugDetailsDTO();
			if (!StringUtils.isBlank(wugDetailsVO.getWugDateDB().toString())) {
				wugDetailsDTO.setWugDate(DateUtils.convertDateToString(
						wugDetailsVO.getWugDateDB(), "MM-dd HH:mm:ss"));
			}

			if (StringUtils.isNotBlank(wugDetailsVO.getBackUpStatus())
					&& "1".equals(wugDetailsVO.getBackUpStatus())) {
				wugDetailsDTO.setBackUpStatus("Up");
			} else if (StringUtils.isNotBlank(wugDetailsVO.getBackUpStatus())
					&& "0".equals(wugDetailsVO.getBackUpStatus())) {
				wugDetailsDTO.setBackUpStatus("Down");
			} else if (StringUtils.isNotBlank(wugDetailsVO.getBackUpStatus())
					&& "99".equals(wugDetailsVO.getBackUpStatus())) {
				wugDetailsDTO.setBackUpStatus("Not Applicable");
			} else {
				wugDetailsDTO.setBackUpStatus("Unknown");
			}

			wugDetailsDTO.setWugState(wugDetailsVO.getWugState());

			wugDetailsDTOs.add(wugDetailsDTO);

		}

		stateTailDTO.setWugDetailsDTOList(wugDetailsDTOs);
		return stateTailDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getResolveTicketSummary(java.lang.
	 * Long, java.lang.String)
	 */
	@Override
	public HDTicketDTO getResolveTicketSummary(Long ticketId, String userId) {
		HDTicketVO hdTicketVO = hdMainDAO.getTicketSummary(ticketId);
		HDTicketDTO hdTicketDTO = new HDTicketDTO();
		if (hdTicketVO != null) {
			BeanUtils.copyProperties(hdTicketVO, hdTicketDTO);

			if (!StringUtils.isBlank(hdTicketVO.getBackUpCktStatus())) {
				if (BackUpStatus.DOWN.getBackUpStatus().equals(
						hdTicketVO.getBackUpCktStatus())) {
					hdTicketDTO.setBackUpCktStatus("Down");
				} else if (BackUpStatus.UP.getBackUpStatus().equals(
						hdTicketVO.getBackUpCktStatus())) {
					hdTicketDTO.setBackUpCktStatus("Up");
				} else {
					hdTicketDTO.setBackUpCktStatus("Not Applicable");
				}
				if (StringUtils.isBlank(hdTicketVO.getDevice())) {
					hdTicketDTO.setDevice("Unknown");
				}
			} else {
				hdTicketDTO.setBackUpCktStatus("Not Applicable");
				hdTicketDTO.setDevice("Not Applicable");
			}
		}
		List<HDEffortDTO> hdEffortDTOList = getEffortsDetail(ticketId);
		HDBillableVO hdBillableVO = hdMainDAO.getBillableInfo(ticketId);
		HdBillableDTO hdBillableDTO = new HdBillableDTO();
		BeanUtils.copyProperties(hdBillableVO, hdBillableDTO);
		hdTicketDTO.setHdEffortDTOList(hdEffortDTOList);
		hdTicketDTO.setHdBillableDTO(hdBillableDTO);
		boolean errorStatus = false;
		String errorMsg = null;
		if (StringUtils.isNotBlank(hdTicketDTO.getPrimaryState())
				&& hdTicketDTO.getPrimaryState().toLowerCase().contains("down")) {
			errorStatus = true;
			errorMsg = MessageKeyConstant.TICKET__RESOLVED_DEVICE_DOWN;
			hdTicketDTO.setError(errorStatus);
			hdTicketDTO.setErrorMessage(errorMsg);
		} else if (hdTicketDTO.getTicketStatus().equalsIgnoreCase(
				"Dispatch Requested")
				|| hdTicketDTO.getTicketStatus().equalsIgnoreCase(
						"Dispatch Scheduled")
				|| hdTicketDTO.getTicketStatus().equalsIgnoreCase(
						"Dispatch Onsite")) {
			errorStatus = true;
			errorMsg = MessageKeyConstant.TICKET__RESOLVED_OPEN_DISPATCH;
			hdTicketDTO.setError(errorStatus);
			hdTicketDTO.setErrorMessage(errorMsg);
		}
		PointOfContactVO pointOfContactVO = new PointOfContactVO();
		pointOfContactVO.setPocId(userId);
		PointOfContactVO argPointOfContact = pointOfContactDAO
				.getContactDetail(pointOfContactVO);
		SimpleDateFormat simpledateFormat = new SimpleDateFormat(
				"MM/dd/yyyy HH:mm");
		Calendar calendar = Calendar.getInstance();
		String currentDate = simpledateFormat.format(calendar.getTime());
		StringBuilder closingWorkNote = new StringBuilder("");
		closingWorkNote.append("Ticket resolved at " + currentDate + " by \n");
		closingWorkNote.append("" + argPointOfContact.getLastName() + " \n");
		hdTicketDTO.setClosingWorkNote(closingWorkNote.toString());

		return hdTicketDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#updateTicketToResolved(com.ilex.ws
	 * .core.dto.HDTicketDTO, java.lang.String)
	 */
	@Override
	public void updateTicketToResolved(HDTicketDTO hdTicketDTO, String userId) {
		HDTicketVO hdTicketVO = new HDTicketVO();
		BeanUtils.copyProperties(hdTicketDTO, hdTicketVO);
		hdMainDAO.updateTicketToResolved(hdTicketVO, userId);
		saveWorkNotes(hdTicketDTO.getTicketId(), hdTicketDTO.getWorkNotes(),
				userId);
		// Close In Process Outage Records
		WugOutInProcessVO wugOutInProcessVO = hdMainDAO
				.getWUGOutInProcessData(hdTicketVO.getTicketId());
		wugOutageCloseActionsLogic.wugOutageCloseAction(
				wugOutInProcessVO.getnActiveMonitorStateChangeID(),
				hdTicketVO.getPointOfFailureId(), hdTicketVO.getRootCauseId(),
				hdTicketVO.getResolutionId(), "0",
				wugOutInProcessVO.getWugInstance(),
				wugOutInProcessVO.getWugBackupStatus());
		hdMainDAO.jobUpdateForTicketToResolved(hdTicketVO, userId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#addNewSite(com.ilex.ws.core.dto.
	 * SiteDetailDTO, java.lang.String)
	 */
	@Override
	public Long addNewSite(SiteDetailDTO siteDetailDTO, String userId) {
		SiteDetailVO siteDetailVO = new SiteDetailVO();
		BeanUtils.copyProperties(siteDetailDTO, siteDetailVO);
		siteDetailVO.setLocalityUplift("1.0");
		siteDetailVO.setUnionSite("N");
		MsaClientVO msaClientVO = msaClientDAO
				.getClientDetailById(siteDetailDTO.getMsaId());
		siteDetailVO.setSiteName(msaClientVO.getClientName());
		siteDetailVO.setSiteEndCustomer(msaClientVO.getClientName());
		boolean siteExist = hdMainDAO.checkSiteExist(siteDetailVO);
		if (siteExist) {
			throw new IlexBusinessException(
					MessageKeyConstant.SITE_ALREADY_EXIST);
		}
		Long siteId = hdMainDAO.addNewSite(siteDetailVO, userId);
		return siteId;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#cancelTicket(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void cancelTicket(String ticketId, String workNotes, String userId) {
		hdMainDAO.cancelTicket(ticketId, userId);
		saveWorkNotes(ticketId, workNotes, userId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#saveNoAction(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void saveNoAction(String ticketId, String userId) {
		/*
		 * hdMainDAO.checkTicketStatusBeforeNoAction(ticketId); Map<String,
		 * Object> wugInfoMap = hdMainDAO.wugInfoMap(ticketId); String
		 * nActiveMonitorStateChangeID = wugInfoMap.get(
		 * "wug_ip_nActiveMonitorStateChangeID").toString(); String wug_instance
		 * = wugInfoMap.get("wug_ip_instance").toString();
		 * wugOutageNoActionInsertLogic.wugOutageNoActionInsert(
		 * nActiveMonitorStateChangeID, userId, wug_instance);
		 * hdMainDAO.saveNoAction(ticketId); String[] cubesToUpdate = {
		 * HdCubeDisplayStatus.PA_MONITOR.getCubeName(),
		 * HdCubeDisplayStatus.PA_ALERT.getCubeName() }; String cubesList =
		 * Util.changeStringToCommaSeperation(cubesToUpdate);
		 * hdMainDAO.updateCubeDisplayStatus(cubesList);
		 */

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getNoActionsDaysPage(java.lang.String)
	 */
	@Override
	public Map<String, String> getNoActionsDaysPage(String ticketId) {
		Map<String, String> noActionDataMap = hdMainDAO
				.getNoActionsDaysData(ticketId);
		Date date = DateUtils.convertStringToDate(noActionDataMap
				.get("LastOccurrence"));
		noActionDataMap.put("LastOccurrence",
				DateUtils.convertDateToString(date, "MM-dd HH:mm"));
		return noActionDataMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#updateSite(com.ilex.ws.core.dto.
	 * SiteDetailDTO, java.lang.String)
	 */
	@Override
	public void updateSite(SiteDetailDTO siteDetailDTO, String userId) {
		SiteDetailVO siteDetailVO = new SiteDetailVO();
		BeanUtils.copyProperties(siteDetailDTO, siteDetailVO);
		hdMainDAO.updateSite(siteDetailVO, userId);
	}

	/**
	 * Gets the provisioning detail.
	 * 
	 * @param siteId
	 *            the site id
	 * @return the provisioning detail
	 */
	ProvisioningDetailDTO getProvisioningDetail(String siteId) {
		ProvisioningDetailDTO provisioningDetailDTO = new ProvisioningDetailDTO();
		ProvisioningDetailVO provisioningDetailVO = hdMainDAO
				.ProvisioningDetail(siteId);

		BeanUtils.copyProperties(provisioningDetailVO, provisioningDetailDTO);
		if (StringUtils.isBlank(provisioningDetailDTO.getCircuit())) {
			provisioningDetailDTO.setCircuit("Not Available");
		}
		if (StringUtils.isBlank(provisioningDetailDTO.getGateway())) {
			provisioningDetailDTO.setGateway("Not Available");
		}
		if (StringUtils.isBlank(provisioningDetailDTO.getIpPassword())) {
			provisioningDetailDTO.setIpPassword("Not Available");
		}
		if (StringUtils.isBlank(provisioningDetailDTO.getIpType())) {
			provisioningDetailDTO.setIpType("Not Available");
		}
		if (StringUtils.isBlank(provisioningDetailDTO.getIpUsername())) {
			provisioningDetailDTO.setIpUsername("Not Available");
		}
		if (StringUtils.isBlank(provisioningDetailDTO.getPrimaryDNS())) {
			provisioningDetailDTO.setPrimaryDNS("Not Available");
		}
		if (StringUtils.isBlank(provisioningDetailDTO.getSecondayDNS())) {
			provisioningDetailDTO.setSecondayDNS("Not Available");
		}
		if (StringUtils.isBlank(provisioningDetailDTO.getSubnetMask())) {
			provisioningDetailDTO.setSubnetMask("Not Available");
		}
		if (StringUtils.isBlank(provisioningDetailDTO.getUseableIp())) {
			provisioningDetailDTO.setUseableIp("Not Available");
		}
		return provisioningDetailDTO;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#noActionAll(java.lang.String)
	 */
	@Override
	public void noActionAll(String userId) {
		List<RealTimeState> realTimeStateListUpStatus = hdMainDAO
				.getRealTimeStateList();
		for (RealTimeState realTimeState : realTimeStateListUpStatus) {
			String ticketId = realTimeState.getTicketId();
			hdMainDAO.checkTicketStatusBeforeNoAction(ticketId);
			String nActiveMonitorStateChangeID = realTimeState
					.getNActiveMonitorStateChangeID();
			String wugInstance = realTimeState.getWugInstance();
			wugOutageNoActionInsertLogic.wugOutageNoActionInsert(
					nActiveMonitorStateChangeID, userId, wugInstance);
			// hdMainDAO.saveNoAction(ticketId);
		}
		String[] cubesToUpdate = { HdCubeDisplayStatus.PA_MONITOR.getCubeName() };
		String cubesList = Util.changeStringToCommaSeperation(cubesToUpdate);
		hdMainDAO.updateCubeDisplayStatus(cubesList);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getLastUpdateEventDetail()
	 */
	@Override
	public RealTimeStateInfo getLastUpdateEventDetail() {
		RealTimeStateInfo realTimeStateInfo = hdMainDAO
				.getLastUpdateEventDetail();
		return realTimeStateInfo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getWorkNotesList(java.lang.Long)
	 */
	@Override
	public List<HDWorkNoteDTO> getWorkNotesList(Long ticketId) {
		List<HDWorkNoteVO> hdWorkNoteVOList = hdMainDAO.getWorkNotes(ticketId);
		List<HDWorkNoteDTO> hdWorkNoteDTOList = new ArrayList<>();
		HDWorkNoteDTO hdWorkNoteDTO = null;
		for (HDWorkNoteVO hdWorkNoteVO : hdWorkNoteVOList) {
			hdWorkNoteDTO = new HDWorkNoteDTO();
			BeanUtils.copyProperties(hdWorkNoteVO, hdWorkNoteDTO);
			if (!StringUtils.isBlank(hdWorkNoteVO.getWorkNoteDate().toString())) {
				hdWorkNoteDTO.setWorkNoteDateStr(DateUtils.convertDateToString(
						hdWorkNoteVO.getWorkNoteDate(), "MM/dd HH:mm"));
			}

			hdWorkNoteDTOList.add(hdWorkNoteDTO);
		}
		return hdWorkNoteDTOList;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getWorkNotesList(java.lang.Long)
	 */
	@Override
	public List<HDMonitoringTimeDTO> getDateTimeListForMonitoringLogic(
			Long ticketId) {
		List<HDMonitoringTimeVO> hdWorkNoteVOList = hdMainDAO
				.getDateTimeListForMonitoringDAO(ticketId);
		List<HDMonitoringTimeDTO> hdWorkNoteDTOList = new ArrayList<>();
		HDMonitoringTimeDTO hdWorkNoteDTO = null;
		for (HDMonitoringTimeVO hdWorkNoteVO : hdWorkNoteVOList) {
			hdWorkNoteDTO = new HDMonitoringTimeDTO();
			BeanUtils.copyProperties(hdWorkNoteVO, hdWorkNoteDTO);

			hdWorkNoteDTOList.add(hdWorkNoteDTO);
		}
		return hdWorkNoteDTOList;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#getCustomerListForCancelAll()
	 */
	@Override
	public List<HDCustomerDetailDTO> getCustomerListForCancelAll() {
		List<HDCustomerDetailVO> customerListVO = hdMainDAO
				.getCustomerListForCancelAll();
		List<HDCustomerDetailDTO> customerListDTO = new ArrayList<>();
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		customerListDTO = awareBeanUtilsBean.copyList(
				HDCustomerDetailDTO.class, customerListVO);
		for (HDCustomerDetailDTO customerDetailDTO : customerListDTO) {
			customerDetailDTO.setChecked(true);
		}
		return customerListDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getAllOutageEvents(com.ilex.ws.core
	 * .dto.HdCancelAllDTO)
	 */
	@Override
	public List<OutageDetailDTO> getAllOutageEvents(
			HdCancelAllDTO hdCancelAllDTO) {
		List<String> customerDetailList = new ArrayList<>();
		Date startDate = null;
		Date endDate = null;
		String startTimeFrame = null;
		String endTimeFrame = null;
		for (HDCustomerDetailDTO hdCustomerDetailDTO : hdCancelAllDTO
				.getCustomerList()) {
			if (hdCustomerDetailDTO.isChecked()) {
				customerDetailList.add(String.valueOf(hdCustomerDetailDTO
						.getMsaId()));
			}
		}
		String[] customerArray = customerDetailList.toArray(new String[] {});
		String customerList = Util.changeStringToCommaSeperation(customerArray);
		if (StringUtils.isNotBlank(hdCancelAllDTO.getTimeFrameStart())) {
			startDate = DateUtils.convertStringToDate(hdCancelAllDTO
					.getTimeFrameStart());
			startTimeFrame = DateUtils.convertDateToString(startDate,
					"yyyy-MM-dd HH:mm:ss");
			endDate = DateUtils.convertStringToDate(hdCancelAllDTO
					.getTimeFrameEnd());
			endTimeFrame = DateUtils.convertDateToString(endDate,
					"yyyy-MM-dd HH:mm:ss");
		}
		int duration = hdCancelAllDTO.getDuration() * 60;
		List<OutageDetailVO> outageListVO = hdMainDAO.getAllOutageEvents(
				customerList, startTimeFrame, endTimeFrame, duration);
		List<OutageDetailDTO> outageListDTO = new ArrayList<>();
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		outageListDTO = awareBeanUtilsBean.copyList(OutageDetailDTO.class,
				outageListVO);
		for (OutageDetailDTO outageDetailDTO : outageListDTO) {
			outageDetailDTO.setChecked(true);
			if (StringUtils.isNotBlank(outageDetailDTO.getWugIpName())) {
				if (outageDetailDTO.getWugIpName().length() >= 18) {
					outageDetailDTO.setWugIpName(outageDetailDTO.getWugIpName()
							.substring(0, 18));
				}
			}
		}
		return outageListDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#getFalseOutageEvents(com.ilex.ws.core
	 * .dto.HdCancelAllDTO)
	 */
	@Override
	public List<OutageDetailDTO> getFalseOutageEvents(
			HdCancelAllDTO hdCancelAllDTO) {
		List<String> customerDetailList = new ArrayList<>();
		Date startDate = null;
		Date endDate = null;
		String startTimeFrame = null;
		String endTimeFrame = null;
		for (HDCustomerDetailDTO hdCustomerDetailDTO : hdCancelAllDTO
				.getCustomerList()) {
			if (hdCustomerDetailDTO.isChecked()) {
				customerDetailList.add(String.valueOf(hdCustomerDetailDTO
						.getMsaId()));
			}
		}
		String[] customerArray = customerDetailList.toArray(new String[] {});
		String customerList = Util.changeStringToCommaSeperation(customerArray);
		if (StringUtils.isNotBlank(hdCancelAllDTO.getTimeFrameStart())) {
			startDate = DateUtils.convertStringToDate(hdCancelAllDTO
					.getTimeFrameStart());
			startTimeFrame = DateUtils.convertDateToString(startDate,
					"yyyy-MM-dd HH:mm:ss");
			endDate = DateUtils.convertStringToDate(hdCancelAllDTO
					.getTimeFrameEnd());
			endTimeFrame = DateUtils.convertDateToString(endDate,
					"yyyy-MM-dd HH:mm:ss");
		}
		int duration = hdCancelAllDTO.getDuration() * 60;
		List<OutageDetailVO> outageListVO = hdMainDAO.getFalseOutageEvents(
				customerList, startTimeFrame, endTimeFrame, duration);
		List<OutageDetailDTO> outageListDTO = new ArrayList<>();
		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		outageListDTO = awareBeanUtilsBean.copyList(OutageDetailDTO.class,
				outageListVO);
		for (OutageDetailDTO outageDetailDTO : outageListDTO) {
			outageDetailDTO.setChecked(true);
			if (StringUtils.isNotBlank(outageDetailDTO.getWugIpName())) {
				if (outageDetailDTO.getWugIpName().length() >= 18) {
					outageDetailDTO.setWugIpName(outageDetailDTO.getWugIpName()
							.substring(0, 18));
				}
			}
		}
		return outageListDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#cancelAll(java.lang.String,
	 * com.ilex.ws.core.dto.HdCancelAllDTO)
	 */
	@Override
	public void cancelAll(String userId, HdCancelAllDTO hdCancelAllDTO) {
		List<String> customerDetailList = new ArrayList<>();
		Date startDate = null;
		Date endDate = null;
		String startTimeFrame = null;
		String endTimeFrame = null;
		for (HDCustomerDetailDTO hdCustomerDetailDTO : hdCancelAllDTO
				.getCustomerList()) {
			if (hdCustomerDetailDTO.isChecked()) {
				customerDetailList.add(String.valueOf(hdCustomerDetailDTO
						.getMsaId()));
			}
		}
		String[] customerArray = customerDetailList.toArray(new String[] {});
		String customerList = Util.changeStringToCommaSeperation(customerArray);
		if (StringUtils.isNotBlank(hdCancelAllDTO.getTimeFrameStart())) {
			startDate = DateUtils.convertStringToDate(hdCancelAllDTO
					.getTimeFrameStart());
			startTimeFrame = DateUtils.convertDateToString(startDate,
					"yyyy-MM-dd HH:mm:ss");
			endDate = DateUtils.convertStringToDate(hdCancelAllDTO
					.getTimeFrameEnd());
			endTimeFrame = DateUtils.convertDateToString(endDate,
					"yyyy-MM-dd HH:mm:ss");
		}
		int duration = hdCancelAllDTO.getDuration() * 60;
		for (OutageDetailDTO outageDetailDTO : hdCancelAllDTO.getOutageList()) {
			String ticketId = outageDetailDTO.getTicketId();

			if (outageDetailDTO.isChecked()) {
				boolean outageExist = hdMainDAO.validateOutage(customerList,
						startTimeFrame, endTimeFrame, duration, ticketId);
				if (outageExist) {
					try {
						hdTicketCancelLogic.cancel(ticketId, userId,
								hdCancelAllDTO);
					} catch (Exception e) {
						logger.info("Error Occured while executing no action all for ticketId="
								+ ticketId);
						logger.error(e);
						throw new IlexSystemException(
								MessageKeyConstant.TICKET_CANCEL_ACTION_ERROR);
					}
				}
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#avaliableIlexTicketNumber()
	 */
	@Override
	public String avaliableIlexTicketNumber() {
		String ilexTicketNumber = hdMainDAO.avaliableIlexTicketNumber();
		return ilexTicketNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#createIncidentIlexToSn(com.ilex.ws
	 * .core.bean.HDTicketVO, java.lang.String)
	 */
	@Override
	public void createIncidentIlexToSn(HDTicketVO hdTicketVO, String userId) {
		String appendixId = String.valueOf(hdTicketVO.getAppendixId());
		String ticketId = hdTicketVO.getTicketId();
		if (serviceNowLogic.checkIntegrationType(appendixId)) {
			IncidentDTO proactiveIncidentDTO = serviceNowLogic
					.mapTicketDataToIntegTableForCreateIncident(ticketId);
			serviceNowDAO.logIntegration(serviceNowDAO.OUTBOUND,
					serviceNowDAO.INFORMATION_LEVEL,
					"Attempting to send proactive ticket!", "Ticket Id: "
							+ ticketId + ", User Id: " + userId);
			Map<String, String> createResponse = serviceNowLogic
					.createProactiveIncident(proactiveIncidentDTO,
							Integer.parseInt(userId));
			String createIdentifier = createResponse.get("createIdentifier");
			IncidentVO incidentDetail = serviceNowDAO.getIncidentDetail(Long
					.valueOf(createIdentifier));
			if (incidentDetail.getIntegrationStatus().equalsIgnoreCase(
					"Successful")) {
				String workNotes = "ServiceNow incident created. Reference: "
						+ incidentDetail.getTicketNumber();
				saveWorkNotes(ticketId, workNotes, userId);
				hdMainDAO.saveWorkNotes(ticketId, workNotes, userId);
			} else if (incidentDetail.getIntegrationStatus().equalsIgnoreCase(
					"Error")) {
				String workNotes = "ServiceNow integration failed.Contact customer by secondary method.";
				hdMainDAO.saveWorkNotes(ticketId, workNotes, userId);
				// send email
				EmailBean emailBean = new EmailBean();
				StringBuilder emailContentBuilder = new StringBuilder();
				emailContentBuilder
						.append("The subject ServiceNow integration failed.");
				emailContentBuilder.append("\n");
				emailContentBuilder.append("Error message: "
						+ incidentDetail.getIntegrationStatusMsg());
				emailContentBuilder.append("\n");
				emailContentBuilder
						.append("See the integration test section for more details.");
				String emailContent = emailContentBuilder.toString()
						.replaceAll("\n", "<br/>");
				emailContent = "<div style=\"font-family:Verdana,Arial, Helvetica, sans-serif;font-size:14px;\">"
						+ emailContent + "   </div>";
				String subject = "ServiceNow Integration Failure for "
						+ hdTicketVO.getCustomerName() + ", "
						+ incidentDetail.getTicketNumber() + ", "
						+ incidentDetail.getSiteNumber();
				emailBean.setContent(emailContent);
				emailBean.setSubject(subject);
				serviceNowDAO.setHdUpdateFlag(ticketId);

				// Update cube status
				boolean isNextActionOverdue = hdMainDAO
						.getNextActionOverdue(ticketId);
				String cubesToUpdate = null;

				if (isNextActionOverdue) {
					cubesToUpdate = HdCubeDisplayStatus.WIP_OD.getCubeName();
				} else {
					cubesToUpdate = HdCubeDisplayStatus.WIP.getCubeName();
				}
				hdMainDAO.updateCubeDisplayStatus(cubesToUpdate);

			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#updateIncidentFromIlexToSn(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public String updateIncidentFromIlexToSn(String ticketId, String userId) {
		IncidentDTO updateIncidentDTO = serviceNowLogic
				.mapTicketDataToIntegTableForUpdateIncident(ticketId);
		String appendixId = updateIncidentDTO.getAppendixId();
		String snErrorMessageWhenClosing = "";
		if (serviceNowLogic.checkIntegrationType(appendixId)
				&& StringUtils.isNotBlank(updateIncidentDTO
						.getCreateIdentifier())) {
			Map<String, String> updateResponse = serviceNowLogic
					.updateIncidentFromIlextoSN(updateIncidentDTO,
							Integer.parseInt(userId));
			String updateIdentifier = updateResponse.get("updateIdentifier");
			IncidentVO incidentUpdateDetail = serviceNowDAO
					.getIncidentUpdateDetail(updateIdentifier);
			if (incidentUpdateDetail.getIntegrationStatus().equalsIgnoreCase(
					"Successful")) {

			} else if (incidentUpdateDetail.getIntegrationStatus()
					.equalsIgnoreCase("Error")) {
				String workNotes = "ServiceNow update failed.Contact customer by secondary method.";
				hdMainDAO.saveWorkNotes(ticketId, workNotes, userId);
				// send email
				EmailBean emailBean = new EmailBean();
				StringBuilder emailContentBuilder = new StringBuilder();
				emailContentBuilder
						.append("The subject ServiceNow integration update failed.");
				emailContentBuilder.append("\n");
				emailContentBuilder.append("Error message: "
						+ incidentUpdateDetail.getIntegrationStatusMsg());
				emailContentBuilder.append("\n");
				emailContentBuilder
						.append("See the integration test section for more details.");
				String emailContent = emailContentBuilder.toString()
						.replaceAll("\n", "<br/>");
				emailContent = "<div style=\"font-family:Verdana,Arial, Helvetica, sans-serif;font-size:14px;\">"
						+ emailContent + "   </div>";
				String subject = "ServiceNow Integration Update Failure for "
						+ updateIncidentDTO.getCustomerName() + ", "
						+ updateIncidentDTO.getTicketNumber() + ", "
						+ updateIncidentDTO.getSiteNumber();
				emailBean.setContent(emailContent);
				emailBean.setSubject(subject);
				serviceNowDAO.setHdUpdateFlag(ticketId);
				snErrorMessageWhenClosing = emailContentBuilder.toString();

				// Update cube status
				boolean isNextActionOverdue = hdMainDAO
						.getNextActionOverdue(ticketId);
				String cubesToUpdate = null;

				if (isNextActionOverdue) {
					cubesToUpdate = HdCubeDisplayStatus.WIP_OD.getCubeName();
				} else {
					cubesToUpdate = HdCubeDisplayStatus.WIP.getCubeName();
				}
				hdMainDAO.updateCubeDisplayStatus(cubesToUpdate);
			}
		}

		return snErrorMessageWhenClosing;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#saveWorkNotes(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void saveWorkNotes(String ticketId, String workNotes, String userId) {
		hdMainDAO.saveWorkNotes(ticketId, workNotes, userId);
		updateIncidentFromIlexToSn(ticketId, userId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.logic.HDMainLogic#checkForStatus(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void checkForStatus(String ackCubeType, String ticketId,
			String userId, String sessionId, String domainName) {
		synchronized (this) {

			if (hdMainDAO.isTicketUnderReviewByOther(ticketId) == true) {
				throw new IlexBusinessException(
						MessageKeyConstant.TICKET_UNDER_REVIEW);
			}
			if (hdMainDAO.isTicketCancelled(ackCubeType, ticketId) == true) {
				throw new IlexBusinessException(
						MessageKeyConstant.TICKET_ALREADY_CANCELLED);
			}
			if (hdMainDAO.isTicketAcknowledged(ticketId) == true) {
				throw new IlexBusinessException(
						MessageKeyConstant.TICKET_ALREADY_ACKNOWLEDGED);
			}
			String hdSessionTrackerId = hdActiveTicketDAO
					.getHDSessionTrackerId(userId, sessionId, domainName);
			hdMainDAO
					.setTicketUnderReview(ticketId, userId, hdSessionTrackerId);
		}
	}

	@Override
	public String checkForHDTicketLockStatus(String ticketId, String userId,
			String sessionId, String domainName) {
		synchronized (this) {

			if (hdMainDAO.isTicketUnderReviewByOther(ticketId) == true) {

				return "This ticket is currently under review by "
						+ hdMainDAO.lockbyuser(ticketId);
			}
			String hdSessionTrackerId = hdActiveTicketDAO
					.getHDSessionTrackerId(userId, sessionId, domainName);
			hdMainDAO
					.setTicketUnderReview(ticketId, userId, hdSessionTrackerId);
			return "success";
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.HDMainLogic#clearReviewStatus(java.lang.String)
	 */
	@Override
	public void clearReviewStatus(String ticketId) {
		hdMainDAO.clearReviewStatus(ticketId);

	}

	/**
	 * Gets the priority value.
	 * 
	 * @param priorityLabel
	 *            the priority label
	 * @return the priority value
	 */
	private String getPriorityValue(String priorityLabel) {
		String priorityVal = "3";
		if (priorityLabel.equals("Critical")) {
			return "1";
		} else if (priorityLabel.equals("High")) {
			return "2";
		} else if (priorityLabel.equals("Medium")) {
			return "3";
		} else if (priorityLabel.equals("Low")) {
			return "4";
		} else if (priorityLabel.equals("Planned")) {
			return "5";
		}

		return priorityVal;

	}

	/**
	 * Gets the priority label.
	 * 
	 * @param priorityValue
	 *            the priority value
	 * @return the priority label
	 */
	private String getPriorityLabel(String priorityValue) {
		String priorityLabel = "Medium";
		if (priorityValue.equals("1")) {
			return "Critical";
		} else if (priorityValue.equals("2")) {
			return "High";
		} else if (priorityValue.equals("3")) {
			return "Medium";
		} else if (priorityValue.equals("4")) {
			return "Low";
		} else if (priorityValue.equals("5")) {
			return "Low";
		}

		return priorityLabel;

	}

	public static String getDataFromWeb(String url, int timeout,
			String lineBreak) {
		try {
			URL u = new URL(url);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.setConnectTimeout(timeout);
			c.setReadTimeout(timeout);
			c.connect();
			int status = c.getResponseCode();
			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(
						c.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + lineBreak);
				}
				br.close();
				return sb.toString();
			}
		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public List<HDWorkNoteDTO> getInstallNotesList(Long ticketId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getClientEmailAddress(Long appendixId) {
		String email = hdMainDAO.getClientEmailAddress(appendixId);
		return email;
	}

	@Override
	public void saveEventCorrelator(EventCorrelatorDTO dto) {

		if (dto.getCustomerInfo() != null
				&& dto.getCustomerInfo().contains("~")) {
			String[] info = dto.getCustomerInfo().split("~");

			dto.setMmId(info[0]);
			dto.setPrId(info[1]);
		} else if ("0".equals(dto.getCustomerInfo())) {
			/* Adding 0 to represent all customers. */
			dto.setMmId("0");
			dto.setPrId("0");
		}
		EventCorrelatorVO vo = new EventCorrelatorVO();
		BeanUtils.copyProperties(dto, vo);
		hdMainDAO.saveEventCorrelator(vo);

	}

	@Override
	public void deleteEventCorrelator(String id) {
		hdMainDAO.deleteEventCorrelator(id);
	}

}
