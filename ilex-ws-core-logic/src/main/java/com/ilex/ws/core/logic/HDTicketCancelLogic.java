package com.ilex.ws.core.logic;

import com.ilex.ws.core.dto.HdCancelAllDTO;

public interface HDTicketCancelLogic {

	public void cancel(String ticketId, String userId,
			HdCancelAllDTO hdCancelAllDTO);
}
