package com.ilex.ws.core.logic.util;

import com.ilex.ws.core.dto.SiteDetailDTO;
import com.mind.bean.pm.SiteBean;

/**
 * The Class SiteDetailOrganizer.
 */
public class SiteDetailOrganizer {

	/**
	 * Copy site details.
	 * 
	 * @param siteBean
	 *            the site bean(Source)
	 * @return the site detail dto
	 */
	public static SiteDetailDTO copySiteDetails(SiteBean siteBean) {

		SiteDetailDTO siteDetailDTO = new SiteDetailDTO();
		siteDetailDTO.setSiteId(siteBean.getSite_Id());
		siteDetailDTO.setSiteName(siteBean.getSite_name());
		siteDetailDTO.setLocalityUplift(siteBean.getLocality_uplift());
		siteDetailDTO.setUnionSite(siteBean.getUnion_site());
		siteDetailDTO.setSiteDesignator(siteBean.getSite_designator());
		siteDetailDTO.setSiteWorkLocation(siteBean.getSite_worklocation());
		siteDetailDTO.setSiteAddress(siteBean.getSite_address());
		siteDetailDTO.setSiteCity(siteBean.getSite_city());
		siteDetailDTO.setState(siteBean.getState());
		siteDetailDTO.setSiteZipcode(siteBean.getSite_zipcode());
		siteDetailDTO.setCountry(siteBean.getCountry());
		siteDetailDTO.setSiteDirection(siteBean.getSite_direction());
		siteDetailDTO.setPrimaryName(siteBean.getPrimary_Name());
		siteDetailDTO.setSecondaryName(siteBean.getSecondary_Name());
		siteDetailDTO.setSiteNotes(siteBean.getSite_notes());
		siteDetailDTO.setSiteNumber(siteBean.getSite_number());
		siteDetailDTO.setSitePhone(siteBean.getSite_phone());
		siteDetailDTO.setSecondaryPhone(siteBean.getSecondary_phone());
		siteDetailDTO.setPrimaryEmail(siteBean.getPrimary_email());
		siteDetailDTO.setSecondaryEmail(siteBean.getSecondary_email());
		siteDetailDTO.setSiteLatDeg(siteBean.getSitelatdeg());
		siteDetailDTO.setSiteLatMin(siteBean.getSitelatmin());
		siteDetailDTO.setSiteLatDirection(siteBean.getSitelatdirection());
		siteDetailDTO.setSiteLonDeg(siteBean.getSitelondeg());
		siteDetailDTO.setSiteLonMin(siteBean.getSitelonmin());
		siteDetailDTO.setSiteLonDirection(siteBean.getSitelondirection());
		siteDetailDTO.setSiteStatus(siteBean.getSitestatus());
		siteDetailDTO.setSiteGroup(siteBean.getSitegroup());
		siteDetailDTO.setSiteEndCustomer(siteBean.getSiteendcustomer());
		siteDetailDTO.setSiteTimeZone(siteBean.getSitetimezone());
		siteDetailDTO.setSiteRegion(siteBean.getSiteregion());
		siteDetailDTO.setSiteCategory(siteBean.getSitecategory());
		siteDetailDTO.setSiteListId(siteBean.getSitelistid());
		siteDetailDTO.setJobId(siteBean.getId2());
		return siteDetailDTO;
	}

	/**
	 * Copy site details.
	 * 
	 * @param siteDetailDTO
	 *            the site detail dto(Source)
	 * @return the site bean
	 */
	public static SiteBean copySiteDetails(SiteDetailDTO siteDetailDTO) {

		SiteBean siteBean = new SiteBean();
		siteBean.setSite_Id(siteDetailDTO.getSiteId());
		siteBean.setId2(siteDetailDTO.getJobId());
		siteBean.setSite_name(siteDetailDTO.getSiteName());
		siteBean.setLocality_uplift(siteDetailDTO.getLocalityUplift());
		siteBean.setUnion_site(siteDetailDTO.getUnionSite());
		siteBean.setSite_designator(siteDetailDTO.getSiteDesignator());
		siteBean.setSite_worklocation(siteDetailDTO.getSiteWorkLocation());
		siteBean.setSite_address(siteDetailDTO.getSiteAddress());
		siteBean.setSite_city(siteDetailDTO.getSiteCity());
		siteBean.setState(siteDetailDTO.getState());
		siteBean.setSite_zipcode(siteDetailDTO.getSiteZipcode());
		siteBean.setCountry(siteDetailDTO.getCountry());
		siteBean.setSite_direction(siteDetailDTO.getSiteDirection());

		siteBean.setSite_notes(siteDetailDTO.getSiteNotes());
		siteBean.setSite_number(siteDetailDTO.getSiteNumber());

		siteBean.setSite_phone(siteDetailDTO.getSitePhone());

		siteBean.setSecondary_phone(siteDetailDTO.getSecondaryPhone());
		siteBean.setPrimary_Name(siteDetailDTO.getPrimaryName());
		siteBean.setSecondary_Name(siteDetailDTO.getSecondaryName());
		siteBean.setPrimary_email(siteDetailDTO.getPrimaryEmail());
		siteBean.setSecondary_email(siteDetailDTO.getSecondaryEmail());
		siteBean.setSitelatdeg(siteDetailDTO.getSiteLatDeg());
		siteBean.setSitelatmin(siteDetailDTO.getSiteLatMin());
		siteBean.setSitelatdirection(siteDetailDTO.getSiteLatDirection());
		siteBean.setSitelondeg(siteDetailDTO.getSiteLonDeg());
		siteBean.setSitelonmin(siteDetailDTO.getSiteLonMin());
		siteBean.setSitelondirection(siteDetailDTO.getSiteLonDirection());
		siteBean.setSitestatus(siteDetailDTO.getSiteStatus());
		siteBean.setSitegroup(siteDetailDTO.getSiteGroup());
		siteBean.setSiteendcustomer(siteDetailDTO.getSiteEndCustomer());
		siteBean.setSitetimezone(siteDetailDTO.getSiteTimeZone());
		siteBean.setSiteregion(siteDetailDTO.getSiteRegion());
		siteBean.setSitecategory(siteDetailDTO.getSiteCategory());
		siteBean.setSitelistid(siteDetailDTO.getSiteListId());

		return siteBean;

	}
}
