package com.ilex.ws.core.logic.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.CountryVO;
import com.ilex.ws.core.bean.StateVO;
import com.ilex.ws.core.dao.GeneralUtilsDAO;
import com.ilex.ws.core.dto.CountryDTO;
import com.ilex.ws.core.dto.StateDTO;
import com.ilex.ws.core.logic.GeneralUtilsLogic;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;

@Component
public class GeneralUtilsLogicImpl implements GeneralUtilsLogic {
	@Resource
	GeneralUtilsDAO generalUtilsDAO;

	@Override
	public List<CountryDTO> getCountrylist() {
		List<CountryDTO> countryDTOs = new ArrayList<>();
		List<CountryVO> countryVOs = generalUtilsDAO.getCountrylist();
		NullAwareBeanUtilsBean nullAwareBeanUtilsBean = new NullAwareBeanUtilsBean();
		countryDTOs = nullAwareBeanUtilsBean.copyList(CountryDTO.class,
				countryVOs);
		return countryDTOs;
	}

	@Override
	public List<StateDTO> getStateList() {
		List<StateDTO> stateDTOs = new ArrayList<>();
		List<StateVO> stateVOs = generalUtilsDAO.getStateList();
		NullAwareBeanUtilsBean nullAwareBeanUtilsBean = new NullAwareBeanUtilsBean();
		stateDTOs = nullAwareBeanUtilsBean.copyList(StateDTO.class, stateVOs);
		return stateDTOs;
	}

}
