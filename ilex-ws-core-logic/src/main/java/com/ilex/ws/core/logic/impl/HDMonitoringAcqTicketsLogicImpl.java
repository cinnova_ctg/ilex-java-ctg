package com.ilex.ws.core.logic.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.HdAquiredTicketVO;
import com.ilex.ws.core.dao.HDMonitoringAcqTicketsDAO;
import com.ilex.ws.core.dto.HdAquiredTicketDTO;
import com.ilex.ws.core.logic.HDMonitoringAcqTicketsLogic;
import com.ilex.ws.core.util.HDTicketPersistUtils;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;

@Component
public class HDMonitoringAcqTicketsLogicImpl implements
        HDMonitoringAcqTicketsLogic {

    @Resource
    HDMonitoringAcqTicketsDAO hdMonitoringAcqTicketsDAO;

    @Override
    public List<HdAquiredTicketDTO> getAcquiredTickets(String hostName) {

        List<HdAquiredTicketDTO> tickets = new ArrayList<>();
        List<HdAquiredTicketVO> ticketVOs = hdMonitoringAcqTicketsDAO
                .getAcquiredTickets();

        NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
        tickets = awareBeanUtilsBean.copyList(HdAquiredTicketDTO.class,
                ticketVOs);
        for (HdAquiredTicketDTO ticket : tickets) {
            if (hostName.equalsIgnoreCase(ticket.getHostName())) {
                if (HDTicketPersistUtils.isSessionTicketsActive(ticket
                        .getSessionId())) {
                    ticket.setStatus("Active");
                } else {
                    ticket.setStatus("In-Active");
                }

            } else {
                ticket.setStatus("N/A");
            }

        }
        return tickets;
    }

    @Override
    public void releaseLock(Long ticketId) {
        hdMonitoringAcqTicketsDAO.releaseLock(ticketId);

    }

}
