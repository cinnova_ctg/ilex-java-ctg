package com.ilex.ws.core.logic.util;

import com.ilex.ws.core.bean.NewDispatchVO;
import com.ilex.ws.core.bean.TicketScheduleInfo;
import com.ilex.ws.core.dto.IlexTimestamp;
import com.ilex.ws.core.dto.NewDispatchDTO;

public class NewDispatchOrganizer {
	public static void setTicketScheduleInfo(NewDispatchVO newDispatchVO,
			NewDispatchDTO newDispatchDTO) {
		setFormFromRequestedTimestamp(newDispatchVO.getTicketScheduleInfo()
				.getRequestedTimestamp(), newDispatchDTO);
		setFormFromPreferredTimestamp(newDispatchVO.getTicketScheduleInfo()
				.getPreferredTimestamp(), newDispatchDTO);
		setFormFromWindowFromTimestamp(newDispatchVO.getTicketScheduleInfo()
				.getWindowFromTimestamp(), newDispatchDTO);
		setFormFromWindowToTimestamp(newDispatchVO.getTicketScheduleInfo()
				.getWindowToTimestamp(), newDispatchDTO);
		setFormFromReceivedTimestamp(newDispatchVO.getTicketScheduleInfo()
				.getReceivedTimestamp(), newDispatchDTO);

	}

	public static void setFormFromReceivedTimestamp(
			IlexTimestamp receivedTimestamp, NewDispatchDTO newDispatchDTO) {
		newDispatchDTO.setReceivedDate(receivedTimestamp.getDate());
		newDispatchDTO.setReceivedHour(receivedTimestamp.getHour());
		newDispatchDTO.setReceivedMinute(receivedTimestamp.getMinute());
		newDispatchDTO.setReceivedIsAm(IlexTimestamp
				.getStringAMPM(receivedTimestamp.isAM()));
	}

	public static void setFormFromWindowToTimestamp(
			IlexTimestamp windowToTimestamp, NewDispatchDTO newDispatchDTO) {
		newDispatchDTO.setWindowToDate(windowToTimestamp.getDate());
		newDispatchDTO.setWindowToHour(windowToTimestamp.getHour());
		newDispatchDTO.setWindowToMinute(windowToTimestamp.getMinute());
		newDispatchDTO.setWindowToIsAm(IlexTimestamp
				.getStringAMPM(windowToTimestamp.isAM()));

	}

	public static void setFormFromWindowFromTimestamp(
			IlexTimestamp windowFromTimestamp, NewDispatchDTO newDispatchDTO) {
		newDispatchDTO.setWindowFromDate(windowFromTimestamp.getDate());
		newDispatchDTO.setWindowFromHour(windowFromTimestamp.getHour());
		newDispatchDTO.setWindowFromMinute(windowFromTimestamp.getMinute());
		newDispatchDTO.setWindowFromIsAm(IlexTimestamp
				.getStringAMPM(windowFromTimestamp.isAM()));

	}

	public static void setFormFromPreferredTimestamp(
			IlexTimestamp preferredTimestamp, NewDispatchDTO newDispatchDTO) {
		newDispatchDTO.setPreferredDate(preferredTimestamp.getDate());
		newDispatchDTO.setPreferredHour(preferredTimestamp.getHour());
		newDispatchDTO.setPreferredMinute(preferredTimestamp.getMinute());
		newDispatchDTO.setPreferredIsAm(IlexTimestamp
				.getStringAMPM(preferredTimestamp.isAM()));

	}

	public static void setFormFromRequestedTimestamp(
			IlexTimestamp requestedTimestamp, NewDispatchDTO newDispatchDTO) {
		newDispatchDTO.setRequestedDate(requestedTimestamp.getDate());
		newDispatchDTO.setRequestedHour(requestedTimestamp.getHour());
		newDispatchDTO.setRequestedMinute(requestedTimestamp.getMinute());
		newDispatchDTO.setRequestedIsAm(IlexTimestamp
				.getStringAMPM(requestedTimestamp.isAM()));

	}

	public static TicketScheduleInfo getTicketScheduleInfo(NewDispatchDTO form) {
		TicketScheduleInfo ticketScheduleInfo = new TicketScheduleInfo();
		ticketScheduleInfo.setRequestedTimestamp(getRequestedTimeStamp(form));
		ticketScheduleInfo.setPreferredTimestamp(getPrefferedTimeStamp(form));
		ticketScheduleInfo.setWindowFromTimestamp(getWindowFromTimeStamp(form));
		ticketScheduleInfo.setWindowToTimestamp(getwindowToTimeStamp(form));
		// ticketScheduleInfo.setReceivedTimestamp(getReceivedTimeStamp(form));
		return ticketScheduleInfo;
	}

	public static IlexTimestamp getRequestedTimeStamp(NewDispatchDTO form) {
		IlexTimestamp requestedTimeStamp = new IlexTimestamp();
		requestedTimeStamp.setDate(form.getRequestedDate());
		requestedTimeStamp.setHour(form.getRequestedHour());
		requestedTimeStamp.setMinute(form.getRequestedMinute());
		requestedTimeStamp.setAM(IlexTimestamp.getBooleanAMPM(form
				.getRequestedIsAm()));
		return requestedTimeStamp;
	}

	public static IlexTimestamp getPrefferedTimeStamp(NewDispatchDTO form) {
		IlexTimestamp prefferedTimeStamp = new IlexTimestamp();
		prefferedTimeStamp.setDate(form.getPreferredDate());
		prefferedTimeStamp.setHour(form.getPreferredHour());
		prefferedTimeStamp.setMinute(form.getPreferredMinute());
		prefferedTimeStamp.setAM(IlexTimestamp.getBooleanAMPM(form
				.getPreferredIsAm()));
		return prefferedTimeStamp;
	}

	public static IlexTimestamp getWindowFromTimeStamp(NewDispatchDTO form) {
		IlexTimestamp windowFromTimeStamp = new IlexTimestamp();
		windowFromTimeStamp.setDate(form.getWindowFromDate());
		windowFromTimeStamp.setHour(form.getWindowFromHour());
		windowFromTimeStamp.setMinute(form.getWindowFromMinute());
		windowFromTimeStamp.setAM(IlexTimestamp.getBooleanAMPM(form
				.getWindowFromIsAm()));
		return windowFromTimeStamp;
	}

	public static IlexTimestamp getwindowToTimeStamp(NewDispatchDTO form) {
		IlexTimestamp windowToTimeStamp = new IlexTimestamp();
		windowToTimeStamp.setDate(form.getWindowToDate());
		windowToTimeStamp.setHour(form.getWindowToHour());
		windowToTimeStamp.setMinute(form.getWindowToMinute());
		windowToTimeStamp.setAM(IlexTimestamp.getBooleanAMPM(form
				.getWindowToIsAm()));
		return windowToTimeStamp;
	}

	public static IlexTimestamp getReceivedTimeStamp(NewDispatchDTO form) {
		IlexTimestamp receivedTimeStamp = new IlexTimestamp();
		receivedTimeStamp.setDate(form.getReceivedDate());
		receivedTimeStamp.setHour(form.getReceivedHour());
		receivedTimeStamp.setMinute(form.getReceivedMinute());
		receivedTimeStamp.setAM(IlexTimestamp.getBooleanAMPM(form
				.getReceivedIsAm()));
		return receivedTimeStamp;
	}

}
