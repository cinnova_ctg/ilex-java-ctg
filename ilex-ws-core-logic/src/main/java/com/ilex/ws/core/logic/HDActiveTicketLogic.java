package com.ilex.ws.core.logic;

import com.ilex.ws.core.dto.HDSessionDTO;
import com.ilex.ws.core.dto.HDTicketDTO;
import com.ilex.ws.core.dto.HDWIPTableDTO;

public interface HDActiveTicketLogic {

	/**
	 * Checks if is ticket available for drop.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param userId
	 *            the user id
	 * @param sessionId
	 *            the session id
	 * @param domainName
	 *            the domain name
	 * @return the string
	 */
	String isTicketAvailableForDrop(Long ticketId, String userId,
			String sessionId, String domainName);

	/**
	 * Removes the ticket lock.
	 * 
	 * @param hdSessionDTO
	 *            the hd session dto
	 */
	void removeTicketLock(HDSessionDTO hdSessionDTO);

	/**
	 * Save dropped ticket state.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param divId
	 *            the div id
	 * @param sessionId
	 *            the session id
	 */
	void saveDroppedTicketState(Long ticketId, String divId, String sessionId);

	/**
	 * Save active ticket.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param sessionId
	 *            the session id
	 */
	void saveActiveTicket(Long ticketId, String sessionId);

	/**
	 * Close active ticket.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param userId
	 *            the user id
	 * @return the boolean
	 */
	Boolean closeActiveTicket(Long ticketId, String userId);

	/**
	 * Gets the ticket details.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the ticket details
	 */
	HDWIPTableDTO getTicketDetails(String ticketId);

	/**
	 * Update next action.
	 * 
	 * @param ilexNumber
	 *            the ilex number
	 * @param nextActionTime
	 *            the next action time
	 * @param nextActionUnit
	 *            the next action unit
	 * @param nextAction
	 *            the next action
	 * @param userId
	 *            the user id
	 * @return the hDWIP table dto
	 */
	HDWIPTableDTO updateNextAction(String ilexNumber, String nextActionTime,
			String nextActionUnit, String nextAction, String userId);

	/**
	 * Save efforts.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param effortValue
	 *            the effort value
	 * @param userId
	 *            the user id
	 */
	void saveEfforts(String ticketId, String effortValue, String userId);

	/**
	 * Save efforts for hd.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param effortStartDate
	 *            the effort start date
	 * @param userId
	 *            the user id
	 */
	public void saveEffortsForHD(String ticketId, String effortStartDate,
			String userId);

	/**
	 * Update state.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param state
	 *            the state
	 * @param userId
	 *            the user id
	 */
	void updateState(String ticketId, String state, String userId);

	void updateBackupStatus(String ticketId, String cktStatus, String userId);

	HDTicketDTO getTicketDetailForDashboard(String ticketId);

	void updateEfforts(String ticketId, String efforts, String userId);

}
