package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.AppendixDetailDTO;
import com.ilex.ws.core.dto.InvBTMClientDTO;

public interface AppendixLogic {
	public List<AppendixDetailDTO> getAppendixList(
			InvBTMClientDTO invBTMClientDTO);

	AppendixDetailDTO getAppendixDetail(int appendixId);

}
