package com.ilex.ws.core.logic.impl;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.dao.WugErrorLogInsertDAO;
import com.ilex.ws.core.logic.WugErrorLogInsertLogic;

@Component
public class WugErrorLogInsertLogicImpl implements WugErrorLogInsertLogic {

	@Resource
	WugErrorLogInsertDAO wugErrorLogInsertDAO;

	@Override
	public void insertErrorLog(String code, String data,
			String deleteDeltaMonths) {
		if (StringUtils.isBlank(deleteDeltaMonths)) {
			deleteDeltaMonths = "6";
		}
		if (StringUtils.isNotBlank(code) && StringUtils.isNotBlank(data)) {
			wugErrorLogInsertDAO.insertErrorLog(code, data, deleteDeltaMonths);
		} else {
			if (StringUtils.isBlank(code)) {
				code = "Code is Null";
			}
			if (StringUtils.isBlank(data)) {
				data = "Data is Null";
			}
			data = code + " | " + data;
			code = "Bad Error Code";
			wugErrorLogInsertDAO.insertErrorLog(code, data, deleteDeltaMonths);
		}

	}

}
