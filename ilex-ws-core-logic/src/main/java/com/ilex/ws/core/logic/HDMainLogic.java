package com.ilex.ws.core.logic;

import java.util.List;
import java.util.Map;

import com.ilex.ws.core.bean.HDTicketVO;
import com.ilex.ws.core.dto.CircuitStatusDTO;
import com.ilex.ws.core.dto.EventCorrelatorDTO;
import com.ilex.ws.core.dto.HDAuditTrailDTO;
import com.ilex.ws.core.dto.HDCategoryDTO;
import com.ilex.ws.core.dto.HDConfigurationItemDTO;
import com.ilex.ws.core.dto.HDCubeStatusDTO;
import com.ilex.ws.core.dto.HDCustomerDetailDTO;
import com.ilex.ws.core.dto.HDEffortDTO;
import com.ilex.ws.core.dto.HDMonitoringTimeDTO;
import com.ilex.ws.core.dto.HDNextActionDTO;
import com.ilex.ws.core.dto.HDTableParentDTO;
import com.ilex.ws.core.dto.HDTicketDTO;
import com.ilex.ws.core.dto.HDWIPTableDTO;
import com.ilex.ws.core.dto.HDWorkNoteDTO;
import com.ilex.ws.core.dto.HdCancelAllDTO;
import com.ilex.ws.core.dto.IncidentStateDTO;
import com.ilex.ws.core.dto.OutageDetailDTO;
import com.ilex.ws.core.dto.ResolutionsDTO;
import com.ilex.ws.core.dto.SiteDetailDTO;
import com.ilex.ws.core.dto.StateTailDTO;
import com.ilex.ws.core.dto.TicketStateDTO;
import com.mind.bean.msp.RealTimeStateInfo;

// TODO: Auto-generated Javadoc
/**
 * The Interface HDMainLogic.
 */
public interface HDMainLogic {

	/**
	 * Gets the tables header.
	 * 
	 * @return the tables header
	 */
	HDTableParentDTO getTablesHeader();

	/**
	 * Creates the ticket priority.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @param impact
	 *            the impact
	 * @param urgency
	 *            the urgency
	 * @return the int
	 */
	int createTicketPriority(Long appendixId, Integer impact, Integer urgency);

	/**
	 * Gets the hDWIP table data.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the hDWIP table data
	 */
	List<HDWIPTableDTO> getHDWIPTableData(String siteNum);

	/**
	 * Gets the hD Resolved table data.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the hD Resolved table data
	 */
	List<HDWIPTableDTO> getHDResolvedTableData(String siteNum, String userId);

	/**
	 * Gets the hD pending ack table data.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the hD pending ack table data
	 */
	String getHDPendingAckTableData(String siteNum);

	/**
	 * Gets the HD pending ack security table data.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the HD pending ack table data
	 */
	String getHDPendingAckSecurityTableData(String siteNum);

	/**
	 * Gets the backup circuit status.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the backup circuit status
	 */
	String getBackupCircuitStatus(String siteNum);

	/**
	 * Gets the individual table header.
	 * 
	 * @param tableId
	 *            the table id
	 * @return the individual table header
	 */
	HDTableParentDTO getIndividualTableHeader(String tableId);

	/**
	 * To fetch excel data.
	 * 
	 * @param tableId
	 *            the table id
	 * @return the excel data
	 */
	String getExcelData(String tableId, String userId);

	/**
	 * Gets data for the WIP overdue cube.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the hDWIP over due table data
	 */
	List<HDWIPTableDTO> getHDWIPOverDueTableData(String siteNum);

	/**
	 * Gets data for the WIP nextAction cube.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the hDWIP nextAction table data
	 */
	List<HDWIPTableDTO> getHDWIPNextActionTableData(String siteNum);

	/**
	 * Gets the list of hD customers.
	 * 
	 * @return the hD customers
	 */
	Map<String, List<HDCustomerDetailDTO>> getHDCustomers();

	/**
	 * Gets the hD ticket detail.
	 * 
	 * @param hdCustomer
	 *            the hd customer
	 * @return the hD ticket detail
	 */
	HDTicketDTO getHDTicketDetail(String hdCustomer);

	/**
	 * Gets the sites.
	 * 
	 * @param msaId
	 *            the msa id
	 * @return the sites
	 */
	List<SiteDetailDTO> getSites(Long msaId);

	/**
	 * Gets the configuration items.
	 * 
	 * @return the configuration items
	 */
	List<HDConfigurationItemDTO> getConfigurationItems();

	/**
	 * Gets the categories.
	 * 
	 * @return the categories
	 */
	List<HDCategoryDTO> getCategories();

	/**
	 * Send email.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 */
	public void sendEmail(HDTicketVO hdTicketVO, String userId,
			String createOrUpdate);

	/**
	 * Creates the new ticket in HD.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param userId
	 *            the user id
	 */
	void createTicket(HDTicketDTO hdTicketDTO, String userId);

	/**
	 * Update ticket.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param userId
	 *            the user id
	 */
	void updateTicket(HDTicketDTO hdTicketDTO, String userId);

	/**
	 * Gets the site details.
	 * 
	 * @param siteId
	 *            the site id
	 * @return the site details
	 */
	SiteDetailDTO getSiteDetails(String siteId);

	/**
	 * Gets data for hD pending ack alert cube.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the hD pending ack alert table data
	 */
	String getHDPendingAckAlertTableData(String siteNum);

	/**
	 * Gets data for hD pending ack monitor cube.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the hD pending ack monitor table data
	 */
	String getHDPendingAckMonitorTableData(String siteNum, String inClause);

	/**
	 * Gets the effort status.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the effort status
	 */
	String getEffortStatus(String ticketId);

	/**
	 * Gets the next actions.
	 * 
	 * @return the next actions
	 */
	List<HDNextActionDTO> getNextActions();

	/**
	 * Gets the ticket details for tickets displayed under pending ack cubes.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the ack ticket details
	 */
	HDTicketDTO getAckTicketDetails(String ticketId);

	/**
	 * Acknowledges a ticket displayed under pending ack cubes.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param userId
	 *            the user id
	 */
	void ackTicket(HDTicketDTO hdTicketDTO, String userId);

	/**
	 * Gets a list of active tickets.
	 * 
	 * @param userId
	 *            the user id
	 * @param sessionId
	 *            the session id
	 * @return the active tickets
	 */
	List<HDWIPTableDTO> getActiveTickets(String userId, String sessionId);

	/**
	 * Gets the cube status for refresh logic.
	 * 
	 * @return the cube status
	 */
	HDCubeStatusDTO getCubeStatus();

	/**
	 * Gets the ticket states.
	 * 
	 * @return the ticket states
	 */
	List<TicketStateDTO> getTicketStates();

	/**
	 * Gets the circuit status.
	 * 
	 * @return the circuit status
	 */
	List<CircuitStatusDTO> getCircuitStatus();

	/**
	 * Gets the ticket summary.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the ticket summary
	 */
	HDTicketDTO getTicketSummary(Long ticketId);

	/**
	 * Gets the audit trail.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the audit trail
	 */
	List<HDAuditTrailDTO> getAuditTrail(String ticketId);

	/**
	 * Gets the ticket details for edit.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the ticket details for edit
	 */
	HDTicketDTO getTicketDetailsForEdit(String ticketId);

	/**
	 * Edits the ticket details.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 */
	void editTicketDetails(HDTicketDTO hdTicketDTO);

	/**
	 * Gets the priority details for edit.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the priority details for edit
	 */
	HDTicketDTO getPriorityDetailsForEdit(String ticketId);

	/**
	 * Edits the priority section.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param userId
	 *            the user id
	 */
	void editPrioritySection(HDTicketDTO hdTicketDTO, String userId);

	/**
	 * Gets the backup details for edit.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the backup details for edit
	 */
	HDTicketDTO getBackupDetailsForEdit(String ticketId);

	/**
	 * Edits the backup details.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param userId
	 *            the user id
	 */
	void editBackupDetails(HDTicketDTO hdTicketDTO, String userId);

	/**
	 * Gets the resolution details for edit.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the resolution details for edit
	 */
	HDTicketDTO getResolutionDetailsForEdit(String ticketId);

	/**
	 * Gets the point of failure list.
	 * 
	 * @return the point of failure list
	 */
	Map<String, String> getPointOfFailureList();

	/**
	 * Gets the root cause list.
	 * 
	 * @return the root cause list
	 */
	Map<String, String> getRootCauseList();

	/**
	 * Gets the resolution list.
	 * 
	 * @return the resolution list
	 */
	Map<String, String> getResolutionList();

	/**
	 * Edits the resolution details.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param userId
	 *            the user id
	 */
	void editResolutionDetails(HDTicketDTO hdTicketDTO, String userId);

	/**
	 * Gets the state tail.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the state tail
	 */
	StateTailDTO getStateTail(String ticketId);

	/**
	 * Gets the resolve ticket summary.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param userId
	 *            the user id
	 * @return the resolve ticket summary
	 */
	HDTicketDTO getResolveTicketSummary(Long ticketId, String userId);

	/**
	 * Update ticket to resolved.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param userId
	 *            the user id
	 */
	void updateTicketToResolved(HDTicketDTO hdTicketDTO, String userId);

	/**
	 * Adds the new site.
	 * 
	 * @param siteDetailDTO
	 *            the site detail dto
	 * @param userId
	 *            the user id
	 * @return the long
	 */
	Long addNewSite(SiteDetailDTO siteDetailDTO, String userId);

	/**
	 * Cancel ticket.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param workNotes
	 *            the work notes
	 * @param userId
	 *            the user id
	 */
	void cancelTicket(String ticketId, String workNotes, String userId);

	/**
	 * Save no action.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param userId
	 *            the user id
	 */
	void saveNoAction(String ticketId, String userId);

	/**
	 * Gets the no actions days page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the no actions days page
	 */
	Map<String, String> getNoActionsDaysPage(String ticketId);

	/**
	 * Update site.
	 * 
	 * @param siteDetailDTO
	 *            the site detail dto
	 * @param userId
	 *            the user id
	 */
	void updateSite(SiteDetailDTO siteDetailDTO, String userId);

	/**
	 * No action all.
	 * 
	 * @param userId
	 *            the user id
	 */
	void noActionAll(String userId);

	/**
	 * Gets the last update event detail.
	 * 
	 * @return the last update event detail
	 */
	RealTimeStateInfo getLastUpdateEventDetail();

	/**
	 * Gets the work notes list.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the work notes list
	 */
	List<HDWorkNoteDTO> getWorkNotesList(Long ticketId);

	List<HDWorkNoteDTO> getInstallNotesList(Long ticketId);

	/**
	 * Gets the customer list for cancel all.
	 * 
	 * @return the customer list for cancel all
	 */
	List<HDCustomerDetailDTO> getCustomerListForCancelAll();

	/**
	 * Gets the all outage events.
	 * 
	 * @param hdCancelAllDTO
	 *            the hd cancel all dto
	 * @return the all outage events
	 */
	List<OutageDetailDTO> getAllOutageEvents(HdCancelAllDTO hdCancelAllDTO);

	/**
	 * Gets the false outage events.
	 * 
	 * @param hdCancelAllDTO
	 *            the hd cancel all dto
	 * @return the false outage events
	 */
	List<OutageDetailDTO> getFalseOutageEvents(HdCancelAllDTO hdCancelAllDTO);

	/**
	 * Cancel all.
	 * 
	 * @param userId
	 *            the user id
	 * @param hdCancelAllDTO
	 *            the hd cancel all dto
	 */
	void cancelAll(String userId, HdCancelAllDTO hdCancelAllDTO);

	/**
	 * Avaliable ilex ticket number.
	 * 
	 * @return the string
	 */
	String avaliableIlexTicketNumber();

	/**
	 * Creates the incident ilex to sn.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 */
	void createIncidentIlexToSn(HDTicketVO hdTicketVO, String userId);

	/**
	 * Update incident from ilex to sn.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param userId
	 *            the user id
	 */
	String updateIncidentFromIlexToSn(String ticketId, String userId);

	/**
	 * Save work notes.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param workNotes
	 *            the work notes
	 * @param userId
	 *            the user id
	 */
	void saveWorkNotes(String ticketId, String workNotes, String userId);

	/**
	 * Check for status.
	 * 
	 * @param ackCubeType
	 *            the ack cube type
	 * @param ticketId
	 *            the ticket id
	 * @param userId
	 *            the user id
	 * @param sessionId
	 *            the session id
	 * @param domainName
	 *            the domain name
	 */
	void checkForStatus(String ackCubeType, String ticketId, String userId,
			String sessionId, String domainName);

	/**
	 * Clear review status.
	 * 
	 * @param ticketId
	 *            the ticket id
	 */
	void clearReviewStatus(String ticketId);

	/**
	 * Gets the incident state.
	 * 
	 * @param all
	 *            Pull all incident states or just those displayed on the form
	 * @param actual
	 *            Also include this specific state. If null ignored
	 * 
	 * @return the incident state
	 */
	public List<IncidentStateDTO> getIncidentState(boolean all, String actual);

	/**
	 * Gets the incident resolutions.
	 * 
	 * @return the incident resolutions
	 */
	public List<ResolutionsDTO> getResolutions();

	/**
	 * Gets the efforts detail.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the efforts detail
	 */
	List<HDEffortDTO> getEffortsDetail(Long ticketId);

	boolean getNextActionDueStatus();

	String checkForHDTicketLockStatus(String ticketId, String userId,
			String sessionId, String domainName);

	int getPendingAckTier2count(String userId);

	public String getHDMonitoringDateTableData(String siteNum, String userId);

	public List<HDMonitoringTimeDTO> getDateTimeListForMonitoringLogic(
			Long ticketId);

	public List<IncidentStateDTO> getEmailAddress(Long all);

	String getClientEmailAddress(Long appendixId);

	void saveEventCorrelator(EventCorrelatorDTO dto);

	void deleteEventCorrelator(String id);

	public List<EventCorrelatorDTO> getEventCorrelator(String type);

}
