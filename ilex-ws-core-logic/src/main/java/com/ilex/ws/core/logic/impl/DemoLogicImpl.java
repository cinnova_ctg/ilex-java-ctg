package com.ilex.ws.core.logic.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.UserDTO;
import com.ilex.ws.core.dao.DemoDAO;
import com.ilex.ws.core.logic.DemoLogic;

@Component
public class DemoLogicImpl implements DemoLogic {

	@Resource
	DemoDAO demoDAO;

	public UserDTO getUserInfo(String userId) {

		return demoDAO.getUserInfo(userId);
	}
}
