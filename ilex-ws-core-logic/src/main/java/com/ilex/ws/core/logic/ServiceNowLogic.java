package com.ilex.ws.core.logic;

import java.util.List;
import java.util.Map;

import com.ilex.ws.core.dto.HDTicketDTO;
import com.ilex.ws.core.dto.IncidentDTO;
import com.ilex.ws.core.dto.IncidentResponseDTO;
import com.ilex.ws.core.dto.TicketStateDTO;
import com.mind.common.bean.EmailBean;

/**
 * The Interface ServiceNowLogic.
 */
public interface ServiceNowLogic {

    /**
     * Gets the incidents list.
     *
     * @param integrationType the integration type
     * @param msaId the msa id
     * @return the incidents list
     */
    List<IncidentDTO> getIncidentsList(String integrationType, String msaId);

    /**
     * Gets the ticket states.
     *
     * @return the ticket states
     */
    List<TicketStateDTO> getTicketStates();

    /**
     * Gets the creates the incident errors.
     *
     * @return the creates the incident errors
     */
    List<IncidentDTO> getCreateIncidentErrors();

    /**
     * Creates the reactive incident.
     *
     * @param reactiveIncidentDTO the reactive incident dto
     * @return the incident response dto
     */
    IncidentResponseDTO createReactiveIncident(IncidentDTO reactiveIncidentDTO);

    /**
     * Gets the update incident errors.
     *
     * @return the update incident errors
     */
    List<IncidentDTO> getUpdateIncidentErrors();

    /**
     * Gets the incident detail.
     *
     * @param createIdentifier the create identifier
     * @return the incident detail
     */
    IncidentDTO getIncidentDetail(Long createIdentifier);

    /**
     * Update incident from ilex to service now.
     *
     * @param incidentDTO the incident dto
     * @param userId the user id
     * @return Map<String, String>
     */
    Map<String, String> updateIncidentFromIlextoSN(IncidentDTO incidentDTO, int userId);

    /**
     * Gets the incident updates.
     *
     * @param createIdentifier the create identifier
     * @return the incident updates
     */
    List<IncidentDTO> getIncidentUpdates(Long createIdentifier);

    /**
     * Update incident from servicenow to ilex.
     *
     * @param incidentDTO the incident dto
     * @return the incident response dto
     */
    IncidentResponseDTO updateIncidentFromServicenowToIlex(
            IncidentDTO incidentDTO);

    Map<String, String> createProactiveIncident(IncidentDTO proactiveIncidentDTO, int userId);

    Map<String, String> sendOutboundSoap(String soapName, Map<String, String> params, int userId) throws Exception;

    boolean checkIntegrationType(String appendixId);

    IncidentDTO mapTicketDataToIntegTableForCreateIncident(String ticketId);

    void sendEmail(EmailBean emailBeanArg);

    IncidentDTO mapTicketDataToIntegTableForUpdateIncident(String ticketId);

    HDTicketDTO getSnMessage(Long ticketId);

    void clearIntegratedTicketStatus(Long ticketId);

}
