package com.ilex.ws.core.logic.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.bean.MsaClientVO;
import com.ilex.ws.core.bean.OrgDivisionVO;
import com.ilex.ws.core.dao.OrgDivisionDAO;
import com.ilex.ws.core.dto.MsaClientDTO;
import com.ilex.ws.core.dto.OrgDivisionDTO;
import com.ilex.ws.core.logic.OrgDivisionLogic;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;

@Component
public class OrgDivisionLogicImpl implements OrgDivisionLogic {

	@Resource
	OrgDivisionDAO orgDivisionDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.OrgDivisionLogic#getDivisions(com.ilex.ws.core
	 * .dto.MsaClientDTO)
	 */
	@Override
	public List<OrgDivisionDTO> getDivisions(MsaClientDTO argclient) {
		List<OrgDivisionDTO> divisionDTOList = new ArrayList<OrgDivisionDTO>();
		MsaClientVO msaClientVO = new MsaClientVO();
		BeanUtils.copyProperties(argclient, msaClientVO);
		List<OrgDivisionVO> divisionVOList = orgDivisionDAO
				.getDivisions(msaClientVO);
		NullAwareBeanUtilsBean utilsBean = new NullAwareBeanUtilsBean();
		divisionDTOList = utilsBean.copyList(OrgDivisionDTO.class,
				divisionVOList);
		return divisionDTOList;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.logic.OrgDivisionLogic#getDivisionDetail(com.ilex.ws
	 * .core.dto.OrgDivisionDTO)
	 */
	@Override
	public OrgDivisionDTO getDivisionDetail(OrgDivisionDTO argdivision) {
		OrgDivisionDTO orgDivisionDTO = new OrgDivisionDTO();
		OrgDivisionVO orgDivisionVO = new OrgDivisionVO();
		BeanUtils.copyProperties(argdivision, orgDivisionVO);
		orgDivisionVO = orgDivisionDAO.getDivisionDetail(orgDivisionVO);
		BeanUtils.copyProperties(orgDivisionVO, orgDivisionDTO);
		return orgDivisionDTO;
}
}
