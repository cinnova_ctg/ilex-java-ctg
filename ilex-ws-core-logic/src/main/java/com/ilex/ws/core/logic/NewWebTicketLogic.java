package com.ilex.ws.core.logic;

import java.util.List;

import com.ilex.ws.core.dto.LabelValueDTO;
import com.ilex.ws.core.dto.NewDispatchDTO;
import com.ilex.ws.core.dto.NewWebTicketDTO;
import com.ilex.ws.core.dto.ViewTicketSummaryDTO;
import com.mind.bean.newjobdb.ResourceLevel;

public interface NewWebTicketLogic {

	List<NewWebTicketDTO> getWebTicketList();

	ViewTicketSummaryDTO getWebTicketSummary(String ticketNumber);

	NewDispatchDTO getWebTicketInfo(Integer msaId, Integer appendixId,
			Integer jobId, Integer ticketId);

	String saveWebTicketInfo(NewDispatchDTO newDispatchDTO, Long loginUserId);

	List<ResourceLevel> getResourceList(Long appendixId, String criticalityId);

	List<LabelValueDTO> getRequestors(long msaId);

	List<LabelValueDTO> getStates();

}
