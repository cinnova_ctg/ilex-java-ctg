package com.mind.MPO.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.mpo.MPODTO;
import com.mind.common.IlexConstants;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.docm.client.ClientOperator;
import com.mind.docm.dao.ViewManupulationDao;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.reports.reportDao.ReportsDao;

public class MPODao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MPODao.class);

	public void getActivityList(MPODTO bean, Map<String, Object> map) {
		logger.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getColumns)");
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			String sql = "select (convert(varchar,temp1.lm_js_id )+'~'+ convert(varchar,temp1.lm_at_id) +'~'+ convert(varchar,temp1.lm_rs_id)) as id,"
					+ " \n temp1.lm_js_id,temp1.lm_js_title,temp1.lm_at_id,temp1.lm_at_title,temp1.lm_at_quantity,"
					+ " \n temp1.lm_rs_id,temp1.iv_rp_name,temp1.lm_rs_quantity,temp1.iv_ct_type,temp1.iv_ct_type_desc, unit_cost = isnull( temp1.lx_ce_unit_cost , 0.00 ),"
					+ " \n mm.mp_mn_id,mp_mn_name,mp_mn_status,"
					+ " \n mp_mn_master_po_item,mp_mn_estimated_cost,"
					+ " \n mp_appendix_id ,temp1.iv_ct_id,temp1.iv_ct_name,temp1.iv_mf_id,temp1.iv_mf_name from mp_main mm"
					+ " \n inner join (select * from lm_job lj inner join (select * from lm_resource lr"
					+ " \n inner join lm_activity la on"
					+ " \n lr.lm_rs_at_id = la.lm_at_id"
					+ " \n left outer join lx_resource_hierarchy_01 on lx_resource_hierarchy_01.iv_rp_id = lm_rs_rp_id"
					+ " \n left join lx_cost_element  on lx_ce_used_in = lm_rs_id"
					+ " \n and lx_ce_type in ('L', 'M', 'F', 'T', 'IL', 'IM', 'IF', 'IT', 'CL', 'CM', 'CF', 'CT', 'AL', 'AM', 'AF', 'AT')) temp"
					+ " \n on temp.lm_at_js_id = lj.lm_js_id)temp1 on"
					+ " \n mm.mp_appendix_id = temp1.lm_js_pr_id"
					+ " \n where temp1.lm_js_type in  ('Default','Addendum')"
					+ " \n and mm.mp_appendix_id = ? and mp_mn_id = ? order by temp1.lm_js_id,temp1.lm_rs_id";

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, bean.getMp_appendix_id());
			pStmt.setString(2, bean.getMpoId());
			rs = pStmt.executeQuery();
			ArrayList listToBeReturned = resultSetAsList(rs);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityList(MPODTO, HttpServletRequest) - size of list,,,,,,,,,"
						+ listToBeReturned.size());
			}
			map.put("activitylist", listToBeReturned);
			map.put("listSize", listToBeReturned.size());
			fetchDataForActivity(bean);
		} catch (SQLException e) {
			logger.error("getActivityList(MPODTO, HttpServletRequest)", e);

			logger.error("getActivityList(MPODTO, HttpServletRequest)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}

	}

	public void getMPOList(int appendix_id, Map<String, Object> map) {
		logger.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getColumns)");
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select mp_mn_id,mp_mn_name,mp_mn_status,mp_mn_type,mp_mn_master_po_item,mp_mn_estimated_cost=isnull(mp_mn_estimated_cost,0.00),mp_mn_planed_cost,"
							+ " \n mp_mn_approval_required,mp_appendix_id from mp_main where mp_appendix_id="
							+ appendix_id);
			map.put("mpoList", resultSetAsList(rs));
		} catch (SQLException e) {
			logger.error("getMPOList(int, HttpServletRequest)", e);

			logger.error("getMPOList(int, HttpServletRequest)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

	}

	public static ArrayList resultSetAsList(ResultSet rs) {
		ArrayList inner = null;
		ArrayList outer = new ArrayList();
		try {

			while (rs.next()) {
				inner = new ArrayList();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					inner.add(rs.getString(rs.getMetaData().getColumnName(i)));
				}
				outer.add(inner);
			}
		} catch (SQLException e) {
			logger.error("resultSetAsList(ResultSet)", e);

			logger.error("resultSetAsList(ResultSet)", e);
		}
		return outer;
	}

	public boolean MPOCreation(Map<String, Object> sessionMap,
			Map<String, Object> map, MPODTO bean) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		PreparedStatement pStmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		Connection conn = ReportsDao.getStaticConnection();
		if (logger.isDebugEnabled()) {
		}
		String loginuserid = sessionMap.get("userid").toString();
		String loginUserName = (String) sessionMap.get("username");
		// SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		try {

			/*
			 * String sql =
			 * "insert into mp_main (mp_mn_name,mp_mn_master_po_item,mp_appendix_id,mp_mn_created_by,mp_mn_created_date,mp_mn_status,mp_mn_updated_by,mp_mn_updated_date)"
			 * + " \n values (?,?,?,?,?,?,?,?)";
			 * System.out.println("mponame"+bean
			 * .getMpoName()+"mpotype"+bean.getMpoItemType()); pStmt =
			 * conn.prepareStatement(sql); pStmt.setString(1,bean.getMpoName());
			 * pStmt.setString(2,bean.getMpoItemType());
			 * pStmt.setString(3,bean.getMp_appendix_id());
			 * pStmt.setString(4,loginUserName); pStmt.setString(5,"getdate()");
			 * pStmt.setString(6,"Draft"); pStmt.setString(7,loginUserName);
			 * pStmt.setString(8,"getdate()"); pStmt.executeUpdate();
			 */
			String sql = "";
			sql = "select * from mp_main where upper(mp_mn_name)='"
					+ bean.getMpoName().toUpperCase()
					+ "' and mp_appendix_id = " + bean.getMp_appendix_id();
			if (logger.isDebugEnabled()) {
				logger.debug("MPOCreation(HttpServletRequest, MPODTO) - " + sql);
			}
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				map.put("mpoExist", "mpoExist");
				return false;
			} else {
				sql = "insert into mp_main (mp_mn_name,mp_mn_master_po_item,mp_appendix_id,mp_mn_created_by,mp_mn_created_date,mp_mn_status,mp_mn_estimated_cost,mp_mn_updated_by,mp_mn_updated_date)"
						+ " \n values ('"
						+ bean.getMpoName()
						+ "','"
						+ bean.getMpoItemType()
						+ "','"
						+ bean.getMp_appendix_id()
						+ "','"
						+ loginUserName
						+ "',getdate(),'Draft','0','"
						+ loginUserName
						+ "','"
						+ ts + "')";
				if (logger.isDebugEnabled()) {
					logger.debug("MPOCreation(HttpServletRequest, MPODTO) - "
							+ sql);
				}
				stmt = conn.createStatement();
				int result = stmt.executeUpdate(sql);
			}

		} catch (Exception e) {
			logger.error("MPOCreation(HttpServletRequest, MPODTO)", e);

			logger.error("MPOCreation(HttpServletRequest, MPODTO)", e);
		} finally {
			DBUtil.close(stmt);
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return true;
	}

	public void MPODetailSave(Map<String, Object> sessionMap,
			Map<String, Object> map, Map<String, Object> attrmap,
			Map<String, String> paramMap, MPODTO bean, String tableName,
			String tabId) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		PreparedStatement pStmt = null;
		Statement stmt = null;
		String sql = "";
		ResultSet rs1 = null;
		Connection conn = ReportsDao.getStaticConnection();
		if (logger.isDebugEnabled()) {
			logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - connection"
					+ conn);
		}
		String loginuserid = (String) sessionMap.get("userid");
		if (logger.isDebugEnabled()) {
			logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - login userId"
					+ loginuserid);
		}
		String loginUserName = (String) sessionMap.get("username");
		// SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		if (logger.isDebugEnabled()) {
			logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - tabId---------------------------"
					+ tabId);
		}
		try {
			if (tabId != null && !tabId.trim().equalsIgnoreCase("")
					&& tabId.trim().equalsIgnoreCase("1")) {
				if (logger.isDebugEnabled()) {
					logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - in this");
				}
				sql = "select * from mp_main where upper(mp_mn_name)=? and mp_mn_id ! = ? and mp_appendix_id = ?";
				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, bean.getMpoName().toUpperCase());
				pStmt.setString(2, bean.getMpoId());
				pStmt.setString(3, bean.getMp_appendix_id());
				rs1 = pStmt.executeQuery();
				if (rs1.next()) {
					map.put("mpoExist", "mpoExist");
				} else {
					sql = "update mp_main "
							+ " \n set mp_mn_master_po_item = ?,"
							+ " \n mp_mn_name = ?,"
							+ " \n mp_mn_estimated_cost = ?,"
							+ " \n mp_mn_updated_by = ?,"
							+ " \n mp_mn_updated_date = ?"
							+ " \n where mp_mn_id = ?";
					pStmt = conn.prepareStatement(sql);
					pStmt.setString(1, bean.getMpoItemType());
					pStmt.setString(2, bean.getMpoName());
					pStmt.setString(3, bean.getEstimatedCost());
					pStmt.setString(4, loginUserName);
					pStmt.setTimestamp(5, ts);
					pStmt.setString(6, bean.getMpoId());
					pStmt.executeUpdate();

					/*
					 * sql =
					 * "delete from dbo.lp_mpo_activity_and_resource where mp_mn_id = ?"
					 * ; pStmt = conn.prepareStatement(sql);
					 * pStmt.setString(1,bean.getMpoId());
					 * pStmt.executeUpdate();
					 */
					if (bean.getResourceHidden() != null) {
						pStmt = conn.prepareStatement(sql);
						if (logger.isDebugEnabled()) {
							logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - resource length"
									+ bean.getResourceHidden().length);
						}
						for (int i = 0; i < bean.getResourceHidden().length; i++) {
							if (logger.isDebugEnabled()) {
								logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - resource hidden"
										+ bean.getResourceHidden()[i]);
							}
							if (logger.isDebugEnabled()) {
								logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - plan quant"
										+ bean.getMpoActivityPlanQuantity()[i]);
							}
							if (logger.isDebugEnabled()) {
								logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - plan unit"
										+ bean.getMpoActivityPlanUnitCost()[i]);
							}
							String rs[] = bean.getResourceHidden()[i]
									.split("~");
							if (logger.isDebugEnabled()) {
								logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - rs[0]---"
										+ rs[0] + "and rs[1]---" + rs[1]);
							}
							if (rs[1].trim().equalsIgnoreCase("N")) {
								if (logger.isDebugEnabled()) {
									logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - in delete");
								}
								sql = "delete from  dbo.lp_mpo_activity_and_resource where mp_mn_id = ? and  mp_ar_at_id = ? "
										+ " \n and mp_ar_as_id = ?";
								pStmt = conn.prepareStatement(sql);
								pStmt.setString(1, bean.getMpoId());
								pStmt.setString(2, bean.getMpoActId()[i]);
								pStmt.setString(3, rs[0]);
								pStmt.executeUpdate();

							} else {
								sql = "update dbo.lp_mpo_activity_and_resource"
										+ " \n set mp_ar_planed_unit_cost = ?,"
										+ " \n mp_ar_planed_qty = ?,"
										+ " \n mp_ar_show_on_work_order = ?,"
										+ " \n mp_ar_updated_by = ?,"
										+ " \n mp_ar_updated_date = ?,"
										+ " \n mp_ar_resource_type = ?,"
										+ " \n mp_ar_resource_classification = ?,"
										+ " \n mp_ar_at_estimated_qty = ?,"
										+ " \n mp_ar_rs_estimated_qty = ?,"
										+ " \n mp_ar_estimated_unit_cost = ?"
										+ " where mp_mn_id = ? and mp_ar_at_id = ? and mp_ar_as_id = ?";
								pStmt = conn.prepareStatement(sql);
								if (bean.getMpoActivityPlanUnitCost()[i] == null
										|| bean.getMpoActivityPlanUnitCost()[i]
												.equalsIgnoreCase(""))
									pStmt.setString(1, "0");
								else
									pStmt.setString(
											1,
											bean.getMpoActivityPlanUnitCost()[i]);
								if (bean.getMpoActivityPlanQuantity()[i] == null
										|| bean.getMpoActivityPlanQuantity()[i]
												.equalsIgnoreCase(""))
									pStmt.setString(2, "0");
								else
									pStmt.setString(
											2,
											bean.getMpoActivityPlanQuantity()[i]);
								pStmt.setString(3, bean.getIsShowHidden()[i]);
								pStmt.setString(4, loginUserName);
								pStmt.setTimestamp(5, ts);
								// pStmt.setString(5,sf.format(new
								// java.util.Date()));
								pStmt.setString(6, bean.getMpoType()[i]);
								pStmt.setString(7, bean.getMpoTypeName()[i]);
								pStmt.setString(8, bean.getMpoAct()[i]);
								pStmt.setString(9, bean.getMpoRes()[i]);
								pStmt.setString(10, bean.getMpoUnit()[i]);
								pStmt.setString(11, bean.getMpoId());
								pStmt.setString(12, bean.getMpoActId()[i]);
								pStmt.setString(13, rs[0]);
								int result = pStmt.executeUpdate();
								if (result <= 0) {
									sql = "insert into dbo.lp_mpo_activity_and_resource "
											+ " \n (mp_mn_id,mp_ar_at_id,mp_ar_as_id,mp_ar_resource_type,"
											+ " \n mp_ar_resource_classification,mp_ar_at_estimated_qty,mp_ar_rs_estimated_qty,"
											+ " \n mp_ar_estimated_unit_cost,mp_ar_planed_unit_cost,mp_ar_planed_qty,mp_ar_show_on_work_order"
											+ " \n ,mp_ar_created_by,mp_ar_created_date,mp_ar_updated_by,mp_ar_updated_date) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
									pStmt = conn.prepareStatement(sql);
									pStmt.setString(1, bean.getMpoId());
									pStmt.setString(2, bean.getMpoActId()[i]);
									pStmt.setString(3, rs[0]);
									pStmt.setString(4, bean.getMpoType()[i]);
									pStmt.setString(5, bean.getMpoTypeName()[i]);
									pStmt.setString(6, bean.getMpoAct()[i]);
									pStmt.setString(7, bean.getMpoRes()[i]);
									pStmt.setString(8, bean.getMpoUnit()[i]);
									if (bean.getMpoActivityPlanUnitCost()[i] == null
											|| bean.getMpoActivityPlanUnitCost()[i]
													.equalsIgnoreCase(""))
										pStmt.setString(9, "0");
									else
										pStmt.setString(
												9,
												bean.getMpoActivityPlanUnitCost()[i]);
									if (bean.getMpoActivityPlanQuantity()[i] == null
											|| bean.getMpoActivityPlanQuantity()[i]
													.equalsIgnoreCase(""))
										pStmt.setString(10, "0");
									else
										pStmt.setString(
												10,
												bean.getMpoActivityPlanQuantity()[i]);

									if (logger.isDebugEnabled()) {
										logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - hidden val"
												+ bean.getIsShowHidden()[i]);
									}
									pStmt.setString(11,
											bean.getIsShowHidden()[i]);
									pStmt.setString(12, loginUserName);
									pStmt.setTimestamp(13, ts);
									pStmt.setString(14, loginUserName);
									pStmt.setTimestamp(15, ts);

									pStmt.executeUpdate();
								}
							}
							// if(bean.getResourceHidden()[i].trim().equalsIgnoreCase("N"))
						}
					}
				}
			}
			if (tabId != null && !tabId.trim().equalsIgnoreCase("")
					&& tabId.trim().equalsIgnoreCase("5")) {
				sql = "update dbo.mp_purchase_order_deliverables "
						+ " \n set mp_mn_dl_title=?,"
						+ " \n mp_mn_dl_type = ?," + " \n mp_mn_dl_format = ?,"
						+ " \n mp_mn_dl_desc = ?,"
						+ " \n mp_mn_dl_updated_by = ?,"
						+ " \n mp_mn_dl_updated_date = ?,"
						+ " \n mp_mn_dl_mobile_upload_allowed = ? "
						+ "\n where mp_mn_id = ? and mp_mn_dl_id = ?";
				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, bean.getDevTitle());
				pStmt.setString(2, bean.getDevType());
				pStmt.setString(3, bean.getDevFormat());
				pStmt.setString(4, bean.getDevDescription());
				pStmt.setString(5, loginUserName);
				pStmt.setTimestamp(6, ts);
				pStmt.setString(7, bean.getMobileAllowed());
				if (bean.getDevId().trim().equalsIgnoreCase("")) {
					pStmt.setString(8, "-1");
					pStmt.setString(9, "-1");
				} else {
					pStmt.setString(8, bean.getMpoId());
					pStmt.setString(9, bean.getDevId());
				}
				/*
				 * sql = "update dbo.mp_purchase_order_deliverables " +
				 * " \n set mp_mn_dl_title='"+bean.getDevTitle()+"',"+
				 * " \n mp_mn_dl_type ='"+bean.getDevType()+"'," +
				 * " \n mp_mn_dl_format ='"+bean.getDevFormat()+"'," +
				 * " \n mp_mn_dl_desc = '"+bean.getDevDescription()+"'," +
				 * " \n mp_mn_dl_updated_by = '"+loginUserName+"'," +
				 * " \n mp_mn_dl_updated_date = '"+sf.format(new
				 * java.util.Date())+"'" +
				 * "\n where mp_mn_id = "+bean.getMpoId()
				 * +" and mp_mn_dl_id = "+bean.getDevId();
				 */
				if (logger.isDebugEnabled()) {
					logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - "
							+ sql);
				}
				// int result = stmt.executeUpdate(sql);
				int result = pStmt.executeUpdate();
				if (logger.isDebugEnabled()) {
					logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - result"
							+ result);
				}
				if (result <= 0) {
					sql = "insert into dbo.mp_purchase_order_deliverables (mp_mn_id,mp_mn_dl_title,mp_mn_dl_type,"
							+ "mp_mn_dl_format,mp_mn_dl_desc,mp_mn_dl_created_by,mp_mn_dl_created_date,mp_mn_dl_updated_by,mp_mn_dl_updated_date,mp_mn_dl_mobile_upload_allowed) values"
							+ "\n (?,?,?,?,?,?,?,?,?,?)";
					if (logger.isDebugEnabled()) {
						logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - mponame"
								+ bean.getMpoName()
								+ "mpotype"
								+ bean.getMpoItemType());
					}
					if (logger.isDebugEnabled()) {
						logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - mpoid----"
								+ bean.getMpoId()
								+ "devtitl---"
								+ bean.getDevTitle());
					}
					if (logger.isDebugEnabled()) {
						logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - devtype----"
								+ bean.getDevType()
								+ "devformat---"
								+ bean.getDevFormat()
								+ "getDevDescription---"
								+ bean.getDevDescription());
					}
					pStmt = conn.prepareStatement(sql);
					pStmt.setString(1, bean.getMpoId());
					pStmt.setString(2, bean.getDevTitle());
					pStmt.setString(3, bean.getDevType());
					pStmt.setString(4, bean.getDevFormat());
					pStmt.setString(5, bean.getDevDescription());
					pStmt.setString(6, loginUserName);
					pStmt.setTimestamp(7, ts);
					pStmt.setString(8, loginUserName);
					pStmt.setTimestamp(9, ts);
					pStmt.setString(10, bean.getMobileAllowed());

					pStmt.executeUpdate();
				}

			}
			if (tabId != null && !tabId.trim().equalsIgnoreCase("")
					&& tabId.trim().equalsIgnoreCase("3")) {
				// sql= "delete from dbo.mp_work_order_detail where mp_mn_id=?";
				sql = "update dbo.mp_work_order_detail"
						+ " \n set mp_wo_nt_of_act = ?," + " \n mp_wo_si = ?,"
						+ " \n mp_mn_wo_updated_by = ?,"
						+ " \n mp_mn_wo_updated_date = ?"
						+ " where mp_mn_id = ? ";
				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, bean.getWoNOA());
				pStmt.setString(2, bean.getWoSI());
				pStmt.setString(3, loginUserName);
				pStmt.setTimestamp(4, ts);
				pStmt.setString(5, bean.getMpoId());
				int result = pStmt.executeUpdate();
				if (result <= 0) {
					sql = "insert into dbo.mp_work_order_detail (mp_mn_id,mp_wo_nt_of_act,mp_wo_si,mp_mn_wo_created_by,mp_mn_wo_created_date,mp_mn_wo_updated_by,mp_mn_wo_updated_date)"
							+ " \n values(?,?,?,?,?,?,?)";
					pStmt = conn.prepareStatement(sql);
					pStmt.setInt(1, new Integer(bean.getMpoId()).intValue());
					pStmt.setString(2, bean.getWoNOA());
					pStmt.setString(3, bean.getWoSI());
					pStmt.setString(4, loginUserName);
					pStmt.setTimestamp(5, ts);
					pStmt.setString(6, loginUserName);
					pStmt.setTimestamp(7, ts);

					pStmt.executeUpdate();
				}
				/*
				 * sql = "delete from dbo.mp_work_order_tools where mp_mn_id=?";
				 * pStmt = conn.prepareStatement(sql); pStmt.setInt(1,new
				 * Integer(bean.getMpoId()).intValue()); pStmt.executeUpdate();
				 */

				if (bean.getWorkOrderCheck() != null) {
					if (logger.isDebugEnabled()) {
						logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - length"
								+ bean.getWorkOrderCheck().length);
					}
					for (int i = 0; i < bean.getWorkOrderCheck().length; i++) {
						String[] wo = bean.getWorkOrderCheck()[i].trim().split(
								"~");
						if (logger.isDebugEnabled()) {
							logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - wo[0]----"
									+ wo[0] + "wo[1]----" + wo[1]);
						}
						if (wo[1].trim().equalsIgnoreCase("N")) {
							if (logger.isDebugEnabled()) {
								logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - in delete");
							}
							sql = "delete from dbo.mp_work_order_tools where mp_mn_id = ? and mp_tools_id = ?";
							pStmt = conn.prepareStatement(sql);
							pStmt.setString(1, bean.getMpoId());
							pStmt.setString(2, wo[0]);
							pStmt.executeUpdate();
						} else {
							sql = "update dbo.mp_work_order_tools"
									+ " \n set mp_wo_tools_updated_by = ?,"
									+ " \n mp_wo_tools_updated_date = ?"
									+ " \n where mp_mn_id = ? and mp_tools_id = ?";
							pStmt = conn.prepareStatement(sql);
							pStmt.setString(1, loginUserName);
							pStmt.setTimestamp(2, ts);
							pStmt.setString(3, bean.getMpoId());
							pStmt.setString(4, wo[0]);
							int woResult = pStmt.executeUpdate();
							if (logger.isDebugEnabled()) {
								logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - update value"
										+ woResult);
							}
							if (woResult <= 0) {
								sql = "insert into dbo.mp_work_order_tools(mp_mn_id,mp_tools_id,mp_wo_tools_created_by,mp_wo_tools_created_date,mp_wo_tools_updated_by,mp_wo_tools_updated_date)"
										+ " \n values (?,?,?,?,?,?)";
								pStmt = conn.prepareStatement(sql);
								pStmt.setString(1, bean.getMpoId());
								pStmt.setString(2, wo[0]);
								pStmt.setString(3, loginUserName);
								pStmt.setTimestamp(4, ts);
								pStmt.setString(5, loginUserName);
								pStmt.setTimestamp(6, ts);
								int insert = pStmt.executeUpdate();
								if (logger.isDebugEnabled()) {
									logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - inserted value"
											+ insert);
								}
							}
						}
					}
				}
				/*
				 * if(bean.getDevMode()!=null){
				 * System.out.println("devmode1"+bean.getDevMode());
				 * System.out.println("in not null");
				 * generalfetchData(request,bean,"5");
				 * System.out.println("devmode"+bean.getDevMode()); } else{
				 * System.out.println("in null");
				 * getGeneralMPODetailListForDelivrables(request,bean,"5"); }
				 */

				// getDataForWorkOrder(request,bean,tabId);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - mpoID"
						+ bean.getMpoId());
			}
			if (tabId != null && !tabId.trim().equalsIgnoreCase("")
					&& tabId.trim().equalsIgnoreCase("2")) {
				sql = "update mp_purchase_order_detail"
						+ " \n set condition = ? ,"
						+ " \n mp_pr_mn_updated_by = ?,"
						+ " \n mp_pr_mn_updated_date = ?"
						+ " \n where mp_mn_id = ?";
				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, bean.getSpecial_condition());
				pStmt.setString(2, loginUserName);
				pStmt.setTimestamp(3, ts);
				pStmt.setInt(4, new Integer(bean.getMpoId()).intValue());
				int result = pStmt.executeUpdate();
				if (logger.isDebugEnabled()) {
					logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - result"
							+ result);
				}
				if (result <= 0) {
					sql = "insert into mp_purchase_order_detail (mp_mn_id,condition,mp_pr_mn_created_by,mp_pr_mn_created_date,mp_pr_mn_updated_by,mp_pr_mn_updated_date)"
							+ " \n values (?,?,?,?,?,?)";
					if (logger.isDebugEnabled()) {
						logger.debug("MPODetailSave(HttpServletRequest, MPODTO, String, String) - mponame"
								+ bean.getMpoName()
								+ "mpotype"
								+ bean.getSpecial_condition()
								+ "APPENDIX"
								+ bean.getMp_appendix_id());
					}
					pStmt = conn.prepareStatement(sql);
					pStmt.setString(1, bean.getMpoId());
					pStmt.setString(2, bean.getSpecial_condition());
					pStmt.setString(3, loginUserName);
					pStmt.setTimestamp(4, ts);
					pStmt.setString(5, loginUserName);
					pStmt.setTimestamp(6, ts);

					pStmt.executeUpdate();
				}
			}
			if (tabId != null && !tabId.trim().equalsIgnoreCase("")
					&& tabId.trim().equalsIgnoreCase("4")) {
				insertDocForMPO(sessionMap, bean);
			}
			fetchDataForMPO(paramMap, map, attrmap, bean, tabId);
		} catch (Exception e) {
			logger.error(
					"MPODetailSave(HttpServletRequest, MPODTO, String, String)",
					e);

			logger.error(
					"MPODetailSave(HttpServletRequest, MPODTO, String, String)",
					e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	public void generalfetchData(MPODTO bean, String tabId) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String sql = "";
		if (logger.isDebugEnabled()) {
			logger.debug("generalfetchData(HttpServletRequest, MPODTO, String) - connection"
					+ conn);
		}
		try {
			if (!tabId.trim().equalsIgnoreCase("") && tabId != null
					&& tabId.trim().equalsIgnoreCase("1")) {
				sql = "select mp_mn_id, mp_mn_name, mp_mn_master_po_item  from mp_main mm where mm.mp_mn_id = ? and mm.mp_appendix_id = ?";
				pStmt = conn.prepareStatement(sql);
				if (logger.isDebugEnabled()) {
					logger.debug("generalfetchData(HttpServletRequest, MPODTO, String) - id"
							+ bean.getMpoId()
							+ "--- appen---"
							+ bean.getMp_appendix_id());
				}
				pStmt.setInt(1, new Integer(bean.getMpoId()).intValue());
				pStmt.setInt(2,
						new Integer(bean.getMp_appendix_id()).intValue());
				rs = pStmt.executeQuery();
				while (rs.next()) {
					bean.setMpoId(rs.getString("mp_mn_id"));
					bean.setMpoName(rs.getString("mp_mn_name"));
					bean.setMpoItemType(rs.getString("mp_mn_master_po_item"));
				}
			}
			if (!tabId.trim().equalsIgnoreCase("") && tabId != null
					&& tabId.trim().equalsIgnoreCase("5")) {
				sql = "select mp_mn_dl_id, mp_mn_id, mp_mn_dl_title, mp_mn_dl_type, mp_mn_dl_format, mp_mn_dl_desc,mp_mn_dl_mobile_upload_allowed  from mp_purchase_order_deliverables mpod where mpod.mp_mn_id = ? and mpod.mp_mn_dl_id = ?";
				pStmt = conn.prepareStatement(sql);
				if (logger.isDebugEnabled()) {
					logger.debug("generalfetchData(HttpServletRequest, MPODTO, String) - id"
							+ bean.getMpoId()
							+ "--- appen---"
							+ bean.getMp_appendix_id());
				}
				pStmt.setInt(1, new Integer(bean.getMpoId()).intValue());
				pStmt.setInt(2, new Integer(bean.getDevId()).intValue());
				rs = pStmt.executeQuery();
				if (rs.next()) {
					bean.setMpoId(rs.getString("mp_mn_id"));
					bean.setDevId(rs.getString("mp_mn_dl_id"));
					bean.setDevTitle(rs.getString("mp_mn_dl_title"));
					bean.setDevType(rs.getString("mp_mn_dl_type"));
					bean.setDevFormat(rs.getString("mp_mn_dl_format"));
					bean.setDevDescription(rs.getString("mp_mn_dl_desc"));
					bean.setMobileAllowed(rs
							.getString("mp_mn_dl_mobile_upload_allowed"));
				} else {
					// bean.setMpoId("");
					bean.setDevId("");
					bean.setDevTitle("");
					// default type to 'Internal'and mobile upload to 'y'
					bean.setDevType("Internal");
					bean.setDevFormat("");
					bean.setDevDescription("");
					bean.setMobileAllowed("y");
				}
			}
		} catch (Exception e) {
			logger.error(
					"generalfetchData(HttpServletRequest, MPODTO, String)", e);

			logger.error(
					"generalfetchData(HttpServletRequest, MPODTO, String)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

	public void setMPODetails(MPODTO bean) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			sql = "select mp_mn_id, mp_mn_name, mp_mn_master_po_item,mp_mn_status  from mp_main mm where mm.mp_mn_id = ? and mm.mp_appendix_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, new Integer(bean.getMpoId()).intValue());
			pStmt.setInt(2, new Integer(bean.getMp_appendix_id()).intValue());
			rs = pStmt.executeQuery();
			while (rs.next()) {
				bean.setMpoId(rs.getString("mp_mn_id"));
				bean.setMpoName(rs.getString("mp_mn_name"));
				bean.setMpoItemType(rs.getString("mp_mn_master_po_item"));
				bean.setMpoStatus(rs.getString("mp_mn_status"));
			}
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("setMPODetails(HttpServletRequest, MPODTO) - connection"
						+ conn);
			}
			logger.error("setMPODetails(HttpServletRequest, MPODTO)", e);

			logger.error("setMPODetails(HttpServletRequest, MPODTO)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

	public void getDataForPurchaseOrder(MPODTO bean) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ReportsDao.getStaticConnection();
			String sql = "select  mp_mn_id, condition from dbo.mp_purchase_order_detail mpod where mpod.mp_mn_id = ? ";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, new Integer(bean.getMpoId()).intValue());
			rs = pStmt.executeQuery();
			if (rs.next()) {
				bean.setMpoId(rs.getString("mp_mn_id"));
				bean.setSpecial_condition(rs.getString("condition"));
			}
		} catch (Exception e) {
			logger.error("getDataForPurchaseOrder(MPODTO)", e);

			logger.error("getDataForPurchaseOrder(MPODTO)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}

	}

	public void getGeneralMPODetailListForDelivrables(Map<String, Object> map,
			MPODTO bean, String tabId) {
		logger.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getColumns)");
		if (logger.isDebugEnabled()) {
			logger.debug("getGeneralMPODetailListForDelivrables(HttpServletRequest, MPODTO, String) - in getGeneralMPODetailListForDelivrables");
		}
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery(" select mp_mn_id,mp_mn_dl_id,mp_mn_dl_title,mp_mn_dl_type,mp_mn_dl_format,mp_mn_dl_desc,mp_mn_dl_mobile_upload_allowed from dbo.mp_purchase_order_deliverables where mp_mn_id="
							+ bean.getMpoId());
			map.put("devList", resultSetAsList(rs));

		} catch (SQLException e) {
			logger.error(
					"getGeneralMPODetailListForDelivrables(HttpServletRequest, MPODTO, String)",
					e);

			logger.error(
					"getGeneralMPODetailListForDelivrables(HttpServletRequest, MPODTO, String)",
					e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
	}

	public void getDataForWorkOrder(MPODTO bean, String tabId) {
		logger.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getColumns)");
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String woRE[] = null;

		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			getREList(conn, bean);
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery(" select * from dbo.mp_work_order_detail where mp_mn_id="
							+ bean.getMpoId());
			if (logger.isDebugEnabled()) {
				logger.debug("getDataForWorkOrder(HttpServletRequest, MPODTO, String) - rs---"
						+ rs);
			}

			if (rs.next()) {
				if (logger.isDebugEnabled()) {
					logger.debug("getDataForWorkOrder(HttpServletRequest, MPODTO, String) - in not null");
				}
				bean.setWoNOA(rs.getString("mp_wo_nt_of_act"));
				bean.setWoSI(rs.getString("mp_wo_si"));
			} else {
				bean.setWoNOA("");
				bean.setWoSI("");
			}

			String sql = "select mp_tools_id from dbo.mp_work_order_tools where mp_mn_id="
					+ bean.getMpoId();
			woRE = ViewManupulationDao.getDataForOtherFields(conn, sql);
			bean.setWoRE(woRE);
		} catch (SQLException e) {
			logger.error(
					"getDataForWorkOrder(HttpServletRequest, MPODTO, String)",
					e);

			logger.error(
					"getDataForWorkOrder(HttpServletRequest, MPODTO, String)",
					e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
	}

	public static void getREList(Connection conn, MPODTO bean) {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from dbo.cp_tools order by cp_tool_name";
			rs = stmt.executeQuery(sql);
			// request.setAttribute("woREList",resultSetAsList(rs));
			// System.out.println("wolist1111"+request.getAttribute("woREList"));
			bean.setWoToolsList(resultSetAsList(rs));
			if (logger.isDebugEnabled()) {
				logger.debug("getREList(HttpServletRequest, Connection, MPODTO) - wolist"
						+ bean.getWoToolsList());
			}
		} catch (SQLException e) {
			logger.error("getREList(HttpServletRequest, Connection, MPODTO)", e);

			logger.error("getREList(HttpServletRequest, Connection, MPODTO)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
	}

	public void searchForLibrary(String libId, MPODTO bean) {
		String query = "select dm_md_fl_nm from dbo.dm_metadata_fields_list ";
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		Connection conn = ReportsDao.getStaticConnection();
		ArrayList colNameList = new ArrayList();
		try {
			pStmt = conn.prepareStatement(query);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				colNameList.add(rs.getString("dm_md_fl_nm"));
			}
		} catch (SQLException e) {
			logger.error("searchForLibrary(String, MPODTO)", e);

			logger.error("searchForLibrary(String, MPODTO)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		searchDocLibrary(colNameList, bean);
	}

	public void searchDocLibrary(ArrayList al, MPODTO bean) {
		String query = "select * from dm_document_version where ";
		for (int i = 0; i < al.size(); i++) {
			query += (i == 0 ? "" : " and ") + (String) al.get(i) + "like '%"
					+ bean.getDocmKeyword() + "%'";
		}
		if (logger.isDebugEnabled()) {
			logger.debug("searchDocLibrary(ArrayList, MPODTO) - " + query);
		}
	}

	public void fetchDataForMPO(Map<String, String> paramMap,
			Map<String, Object> map, Map<String, Object> attrMap, MPODTO bean,
			String tabId) {
		String actionTaken = paramMap.get("actionTaken");
		if (tabId == null || tabId.trim().equalsIgnoreCase("")) {
			tabId = "0";
			getMPOList(Integer.parseInt(bean.getMp_appendix_id()), map);
			// request.setAttribute("loadForFirstTime","loadForFirstTime");

		}
		if (tabId != null && !tabId.trim().equalsIgnoreCase("")
				&& tabId.trim().equalsIgnoreCase("1")) {
			if (actionTaken == null) {
				setMPODetails(bean);
				getActivityList(bean, map);
				generalfetchData(bean, tabId);
				map.put("continue", "continue");
			} else if (actionTaken.trim().equalsIgnoreCase("AddNewMPO")) {
				map.put("add", "add");
				bean.setMpoName("");
				bean.setMpoItemType("fp");
				// request.setAttribute("loadForFirstTime","loadForFirstTime");
				map.put("isClicked", "");
			}
		}
		if (tabId != null && !tabId.trim().equalsIgnoreCase("")
				&& tabId.trim().equals("0")) {
			getMPOList(Integer.parseInt(bean.getMp_appendix_id()), map);
		}

		if (tabId != null && !tabId.trim().equalsIgnoreCase("")
				&& tabId.trim().equalsIgnoreCase("4")) {
			setMPODetails(bean);
			ArrayList al = featchDocListForMPO(new Integer(bean.getMpoId())
					.intValue());
			if (logger.isDebugEnabled()) {
				logger.debug("fetchDataForMPO(HttpServletRequest, MPODTO, String) - al"
						+ al);
			}
			map.put("docDetailPageList", al);
		}
		if (tabId != null && !tabId.trim().equalsIgnoreCase("")
				&& tabId.trim().equalsIgnoreCase("5")) {
			setMPODetails(bean);
			if (attrMap.get("clicked") != null)
				generalfetchData(bean, tabId);
			else
				getGeneralMPODetailListForDelivrables(map, bean, tabId);
		}
		if (tabId != null && !tabId.trim().equalsIgnoreCase("")
				&& tabId.trim().equalsIgnoreCase("2")) {
			setMPODetails(bean);
			getDataForPurchaseOrder(bean);
		}
		if (tabId != null && !tabId.trim().equalsIgnoreCase("")
				&& tabId.trim().equalsIgnoreCase("3")) {
			setMPODetails(bean);
			getDataForWorkOrder(bean, tabId);
		}
		map.put("tabId", tabId);
		if (logger.isDebugEnabled()) {
			logger.debug("fetchDataForMPO(HttpServletRequest, MPODTO, String) - test");
		}
	}

	public String getMaxMPOId(int appendix_id) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String mpoId = "";
		try {
			conn = ReportsDao.getStaticConnection();
			if (logger.isDebugEnabled()) {
				logger.debug("getMaxMPOId(int) - appendix-------------"
						+ appendix_id);
			}
			String sql = "select max(mp_mn_id) as test from mp_main where mp_appendix_id ="
					+ appendix_id;
			if (logger.isDebugEnabled()) {
				logger.debug("getMaxMPOId(int) - " + sql);
			}
			stmt = conn.createStatement();
			// pStmt = conn.prepareStatement(sql);
			// pStmt.setInt(1,appendix_id);
			// rs = pStmt.executeQuery();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				mpoId = rs.getString("test");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("getMaxMPOId(int) - MPOID" + mpoId);
			}
		} catch (Exception e) {
			logger.error("getMaxMPOId(int)", e);

			logger.error("getMaxMPOId(int)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(stmt);
			DBUtil.close(conn);
		}
		return mpoId;
	}

	public void fetchDataForActivity(MPODTO bean) {
		Connection conn = null;
		String sql = "";
		try {
			conn = ReportsDao.getStaticConnection();
			if (logger.isDebugEnabled()) {
				logger.debug("fetchDataForActivity(HttpServletRequest, MPODTO) - mpoId"
						+ bean.getMpoId());
			}
			sql = "select mp_ar_as_id from dbo.lp_mpo_activity_and_resource where mp_mn_id ="
					+ new Integer(bean.getMpoId()).intValue()
					+ " order by mp_ar_as_id ";
			String[] resourceCheck = ViewManupulationDao.getDataForOtherFields(
					conn, sql);
			bean.setMpoActivityResource(resourceCheck);
			// System.out.println("in resourceCheck");
			sql = "select mp_ar_show_on_work_order from dbo.lp_mpo_activity_and_resource where mp_mn_id ="
					+ new Integer(bean.getMpoId()).intValue()
					+ " order by mp_ar_as_id ";
			String[] isShow = ViewManupulationDao.getDataForOtherFields(conn,
					sql);
			bean.setMpoActivityIsWOShow(isShow);
			if (logger.isDebugEnabled()) {
				logger.debug("fetchDataForActivity(HttpServletRequest, MPODTO) - in isShow");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("fetchDataForActivity(HttpServletRequest, MPODTO) - mpotype"
						+ bean.getMpoItemType());
			}
			// commented for making page behaviour similar for all mpoTypes
			// if(bean.getMpoItemType()!=null &&
			// !bean.getMpoItemType().equalsIgnoreCase("fp")){
			sql = "select mp_ar_planed_unit_cost from dbo.lp_mpo_activity_and_resource where mp_mn_id ="
					+ new Integer(bean.getMpoId()).intValue()
					+ " order by mp_ar_as_id ";
			String[] plUnitCost = ViewManupulationDao.getDataForOtherFields(
					conn, sql);
			if (plUnitCost != null)
				bean.setMpoActivityPlanUnitCost(plUnitCost);
			if (logger.isDebugEnabled()) {
				logger.debug("fetchDataForActivity(HttpServletRequest, MPODTO) - in plUnitCost");
			}
			sql = "select mp_ar_planed_qty from dbo.lp_mpo_activity_and_resource where mp_mn_id ="
					+ new Integer(bean.getMpoId()).intValue()
					+ " order by mp_ar_as_id ";
			String[] plQty = ViewManupulationDao.getDataForOtherFields(conn,
					sql);
			if (plQty != null)
				bean.setMpoActivityPlanQuantity(plQty);
			if (logger.isDebugEnabled()) {
				logger.debug("fetchDataForActivity(HttpServletRequest, MPODTO) - in plQty");
			}
			// }
		} catch (Exception e) {
			logger.error("fetchDataForActivity(HttpServletRequest, MPODTO)", e);

			logger.error("fetchDataForActivity(HttpServletRequest, MPODTO)", e);
		} finally {
			DBUtil.close(conn);
		}
	}

	public boolean isPM(String userId, String appendixId) {
		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		if (logger.isDebugEnabled()) {
			logger.debug("isPM(String, String) - UserId :" + userId);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("isPM(String, String) - AppendixId :" + appendixId);
		}

		try {

			// String sql = "	select * from lo_poc where lo_pc_id in " +
			// " \n (select lo_pr_pc_id  from lo_poc_rol where lo_pr_ro_id in "
			// +
			// " \n (select lo_ro_id from lo_role where lo_ro_role_desc='Project Manager')) "
			// +
			// " \n and lo_pc_id = ?";

			String sql = "select lm_ap_pc_id from lm_appendix_poc where lm_ap_pr_id = ?";
			if (logger.isDebugEnabled()) {
				logger.debug("isPM(String, String) - Sql is :" + sql);
			}
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, appendixId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				if (logger.isDebugEnabled()) {
					logger.debug("isPM(String, String) - Sr .Project Manager Id :"
							+ rs.getString(1));
				}
				if (userId.equals(rs.getString(1).trim())) {
					return true;
				}
			}
		} catch (Exception e) {
			logger.error("isPM(String, String)", e);

			logger.error("isPM(String, String)", e);

		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return false;
	}

	public void deleteDev(Map<String, String> paramMap,
			Map<String, Object> map, Map<String, Object> attrMap, MPODTO bean) {
		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;
		try {

			String sql = "delete from dbo.mp_purchase_order_deliverables where mp_mn_id = ? and mp_mn_dl_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, bean.getMpoId());
			pStmt.setString(2, bean.getDevId());
			pStmt.executeUpdate();
			fetchDataForMPO(paramMap, map, attrMap, bean, paramMap.get("type")
					.toString());
		} catch (Exception e) {
			logger.error("deleteDev(HttpServletRequest, MPODTO)", e);

			logger.error("deleteDev(HttpServletRequest, MPODTO)", e);

		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	public String approveMPO(Map<String, Object> sessionMap,
			Map<String, String> paramMap, Map<String, Object> map,
			Map<String, Object> attrMap, MPODTO bean) {
		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String emailAddress = "";
		String loginUserName = (String) sessionMap.get("username");
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		try {
			String status = paramMap.get("status").toString();
			String sql = "update dbo.mp_main" + " \n set mp_mn_status = ?,"
					+ " \n mp_mn_updated_by = ?,"
					+ " \n mp_mn_updated_date = ?"
					+ " \n where mp_mn_id = ? and mp_appendix_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, status);
			pStmt.setString(2, loginUserName);
			pStmt.setTimestamp(3, ts);
			pStmt.setString(4, bean.getMpoId());
			pStmt.setString(5, bean.getMp_appendix_id());
			int result = pStmt.executeUpdate();
			if (result > 0) {
				sql = "select lo_pc_email1 from lo_poc where lo_pc_id in "
						+ " \n (select lm_ap_pc_id from lm_appendix_poc where lm_ap_pr_id = ?)";
				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, bean.getMp_appendix_id());
				rs = pStmt.executeQuery();
				while (rs.next()) {
					emailAddress = rs.getString("lo_pc_email1");
				}
			}
			fetchDataForMPO(paramMap, map, attrMap, bean, paramMap.get("type")
					.toString());
		} catch (Exception e) {
			logger.error("approveMPO(HttpServletRequest, MPODTO)", e);

			logger.error("approveMPO(HttpServletRequest, MPODTO)", e);

		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		return emailAddress;
	}

	public String getAppendixName(String appendixId) {
		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String appendixName = "";
		try {

			String sql = "select lx_pr_title from dbo.lx_appendix_main where lx_pr_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, appendixId);
			rs = pStmt.executeQuery();
			if (rs.next())
				appendixName = rs.getString("lx_pr_title");
		} catch (Exception e) {
			logger.error("getAppendixName(String)", e);

			logger.error("getAppendixName(String)", e);

		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		return appendixName;
	}

	public String getMsaName(String appendixId) {
		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String msaName = "";
		try {

			String sql = "select lot.lo_ot_name from lp_msa_main lmn join lo_organization_top lot on "
					+ " \n lp_mm_ot_id = lo_ot_id "
					+ " \n join lx_appendix_main lam on "
					+ " \n lmn.lp_mm_id = lam.lx_pr_mm_id "
					+ " \n and lam.lx_pr_id = ? "
					+ " \n order by lam.lx_pr_id,lmn.lp_mm_id";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, appendixId);
			rs = pStmt.executeQuery();
			if (rs.next())
				msaName = rs.getString("lo_ot_name");
		} catch (Exception e) {
			logger.error("getMsaName(String)", e);

			logger.error("getMsaName(String)", e);

		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		return msaName;
	}

	public String getMPOName(MPODTO bean) {
		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String mpoName = "";
		try {

			String sql = "select mp_mn_name from mp_main where mp_mn_id = ? and mp_appendix_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, bean.getMpoId());
			pStmt.setString(2, bean.getMp_appendix_id());
			rs = pStmt.executeQuery();
			if (rs.next())
				mpoName = rs.getString("mp_mn_name");
		} catch (Exception e) {
			logger.error("getMPOName(MPODTO)", e);

			logger.error("getMPOName(MPODTO)", e);

		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		return mpoName;
	}

	public String getFromAdd(String userId) {
		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String fromAdd = "";
		try {

			String sql = "select lo_pc_email1 from lo_poc where lo_pc_id in "
					+ " \n (select lo_us_pc_id  from lo_user where lo_us_pc_id = ?)";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userId);
			rs = pStmt.executeQuery();
			if (rs.next())
				fromAdd = rs.getString("lo_pc_email1");
		} catch (Exception e) {
			logger.error("getFromAdd(String)", e);

			logger.error("getFromAdd(String)", e);

		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		return fromAdd;
	}

	public ArrayList featchDocListForMPO(int mpoId) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		PreparedStatement pStmt = null;
		Connection conn = ReportsDao.getStaticConnection();
		ArrayList outerList = new ArrayList();
		ResultSet rs = null;
		String query = "select mp_doc_id,doc_with_po,doc_with_wo "
				+ "from mp_document where mp_mn_id =?";
		try {
			pStmt = conn.prepareStatement(query);
			pStmt.setInt(1, mpoId);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				ArrayList innerList = ClientOperator.getDocListForMpo(rs
						.getString(1));
				innerList.add(rs.getString(2));
				innerList.add(rs.getString(3));
				innerList.add(rs.getString(1));
				outerList.add(innerList);
			}
		} catch (Exception e) {
			logger.error("featchDocListForMPO(int)", e);

			logger.error("featchDocListForMPO(int)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);

		}
		return outerList;
	}

	public void insertDocForMPO(Map<String, Object> sessionMap, MPODTO bean) {
		String sql = null;
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		String loginUserName = (String) sessionMap.get("username");
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		PreparedStatement pStmt = null;
		Connection conn = ReportsDao.getStaticConnection();
		String[] docIdList = bean.getListOfDoc_Id();
		String[] include_po = bean.getInclude_with_po();
		String[] document_id = bean.getDocument_id();
		String[] include_wo = bean.getInclude_with_wo();
		if (logger.isDebugEnabled()) {
			logger.debug("insertDocForMPO(HttpServletRequest, MPODTO) - length"
					+ docIdList.length);
		}
		try {
			for (int i = 0; i < docIdList.length; i++) {
				if (logger.isDebugEnabled()) {
					logger.debug("insertDocForMPO(HttpServletRequest, MPODTO) - inside insert"
							+ document_id[i]
							+ ":"
							+ include_po[i]
							+ ":"
							+ include_wo[i] + "::" + docIdList[i]);
				}
				String doc_id = getDocIdFromMPO(bean, document_id[i]);
				if (doc_id.equals("")) {
					sql = "insert into mp_document (mp_mn_id,mp_doc_id,doc_with_po,doc_with_wo,mp_doc_created_by,"
							+ "mp_doc_created_date,mp_doc_updated_by,mp_doc_updated_date)"
							+ " \n values (?,?,?,?,?,?,?,?)";
					try {
						if (docIdList[i].equalsIgnoreCase("Y")) {
							pStmt = conn.prepareStatement(sql);
							pStmt.setInt(1,
									new Integer(bean.getMpoId()).intValue());
							pStmt.setInt(2, Integer.parseInt(document_id[i]));
							pStmt.setString(3, include_po[i]);
							pStmt.setString(4, include_wo[i]);
							pStmt.setString(5, loginUserName);
							pStmt.setTimestamp(6, ts);
							pStmt.setString(7, loginUserName);
							pStmt.setTimestamp(8, ts);
							pStmt.executeUpdate();
						}
					} catch (Exception e) {
						logger.error(
								"insertDocForMPO(HttpServletRequest, MPODTO)",
								e);

						logger.error(
								"insertDocForMPO(HttpServletRequest, MPODTO)",
								e);
					}
				} else if (!doc_id.equals("")) {
					sql = "update mp_document" + " \n set mp_doc_id = ? ,"
							+ " \n doc_with_po = ?," + " \n doc_with_wo = ?,"
							+ " \n mp_doc_updated_by = ?,"
							+ " \n mp_doc_updated_date = ?"
							+ " \n where mp_mn_id = ? and mp_doc_id = ?";
					if (docIdList[i].equalsIgnoreCase("Y")) {
						try {
							pStmt = conn.prepareStatement(sql);
							pStmt.setInt(1, Integer.parseInt(document_id[i]));
							pStmt.setString(2, include_po[i]);
							pStmt.setString(3, include_wo[i]);
							pStmt.setString(4, loginUserName);
							pStmt.setTimestamp(5, ts);
							pStmt.setInt(6,
									new Integer(bean.getMpoId()).intValue());
							pStmt.setInt(7, Integer.parseInt(document_id[i]));
							pStmt.executeUpdate();
						} catch (NumberFormatException e) {
							logger.error(
									"insertDocForMPO(HttpServletRequest, MPODTO)",
									e);

							logger.error(
									"insertDocForMPO(HttpServletRequest, MPODTO)",
									e);
						} catch (SQLException e) {
							logger.error(
									"insertDocForMPO(HttpServletRequest, MPODTO)",
									e);

							logger.error(
									"insertDocForMPO(HttpServletRequest, MPODTO)",
									e);
						}
					}

				}
				if (logger.isDebugEnabled()) {
					logger.debug("insertDocForMPO(HttpServletRequest, MPODTO) - "
							+ sql);
				}
			}
		} catch (Exception e) {
			logger.error("insertDocForMPO(HttpServletRequest, MPODTO)", e);

			logger.error("insertDocForMPO(HttpServletRequest, MPODTO)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	public int updateDocFormMPO(MPODTO bean, Map<String, Object> sessionMap) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		String loginUserName = (String) sessionMap.get("username");
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		PreparedStatement pStmt = null;
		int result = 0;
		Connection conn = ReportsDao.getStaticConnection();
		if (logger.isDebugEnabled()) {
			logger.debug("updateDocFormMPO(MPODTO, HttpServletRequest) - doc_id"
					+ bean.getDoc_id());
		}
		String query = "update mp_document set " + " \n doc_with_po = ?,"
				+ " \n doc_with_wo = ?," + " \n mp_doc_updated_by = ?,"
				+ " \n mp_doc_updated_date = ?"
				+ " \n where mp_mn_id = ? and mp_doc_id=?";
		if (logger.isDebugEnabled()) {
			logger.debug("updateDocFormMPO(MPODTO, HttpServletRequest) - po"
					+ bean.getIn_with_po());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("updateDocFormMPO(MPODTO, HttpServletRequest) - wo"
					+ bean.getIn_with_wo());
		}
		try {
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, bean.getIn_with_po());
			pStmt.setString(2, bean.getIn_with_wo());
			pStmt.setString(3, loginUserName);
			pStmt.setTimestamp(4, ts);
			pStmt.setString(5, bean.getMpoId());
			pStmt.setString(6, bean.getDoc_id());
			result = pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error("updateDocFormMPO(MPODTO, HttpServletRequest)", e);

			logger.error("updateDocFormMPO(MPODTO, HttpServletRequest)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		return result;
	}

	public int deleteDocFormMPO(MPODTO bean) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		PreparedStatement pStmt = null;
		int result = 0;
		Connection conn = ReportsDao.getStaticConnection();
		String query = "delete from dbo.mp_document where mp_mn_id=? and mp_doc_id=?";
		try {
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, bean.getMpoId());
			pStmt.setString(2, bean.getDoc_id());
			result = pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error("deleteDocFormMPO(MPODTO)", e);

			logger.error("deleteDocFormMPO(MPODTO)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		return result;
	}

	public String getDocIdFromMPO(MPODTO bean, String docId) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		PreparedStatement pStmt = null;
		String doc_id = "";
		ResultSet result = null;
		Connection conn = ReportsDao.getStaticConnection();
		String query = "select isnull(mp_doc_id,'') as mp_doc_id from dbo.mp_document where mp_mn_id=? and mp_doc_id=?";
		try {
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, bean.getMpoId());
			pStmt.setString(2, docId);
			result = pStmt.executeQuery();
			while (result.next()) {
				doc_id = (result.getString("mp_doc_id"));
			}
		} catch (Exception e) {
			logger.error("getDocIdFromMPO(MPODTO, String)", e);

			logger.error("getDocIdFromMPO(MPODTO, String)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		return doc_id;
	}

	public void deleteMPO(Map<String, String> paramMap,
			Map<String, Object> map, Map<String, Object> attrMap, MPODTO bean,
			String tabId) {
		Connection conn = ReportsDao.getStaticConnection();
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			String sql1 = "delete from dbo.mp_purchase_order_deliverables where mp_mn_id ="
					+ bean.getMpoId();
			stmt.addBatch(sql1);
			String sql2 = "delete from dbo.mp_purchase_order_detail where mp_mn_id ="
					+ bean.getMpoId();
			stmt.addBatch(sql2);
			String sql3 = "delete from dbo.mp_work_order_detail where mp_mn_id ="
					+ bean.getMpoId();
			stmt.addBatch(sql3);
			String sql4 = "delete from dbo.mp_work_order_tools where mp_mn_id ="
					+ bean.getMpoId();
			stmt.addBatch(sql4);
			String sql5 = "delete from dbo.lp_mpo_activity_and_resource where mp_mn_id ="
					+ bean.getMpoId();
			stmt.addBatch(sql5);
			String sql6 = "delete from dbo.mp_document where mp_mn_id ="
					+ bean.getMpoId();
			stmt.addBatch(sql6);
			String sql7 = "delete from dbo.mp_main where mp_mn_id ="
					+ bean.getMpoId();
			stmt.addBatch(sql7);
			stmt.executeBatch();
			fetchDataForMPO(paramMap, map, attrMap, bean, tabId);
		} catch (Exception e) {
			logger.error("deleteMPO(HttpServletRequest, MPODTO, String)", e);

			logger.error("deleteMPO(HttpServletRequest, MPODTO, String)", e);

		} finally {
			DBUtil.close(stmt);
			DBUtil.close(conn);
		}
	}

	public String getdefaultSOWAssumption(String Id, String fromType,
			String viewType, String pfType, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String sowAssumption = null;
		if (logger.isDebugEnabled()) {
			logger.debug("getdefaultSOWAssumption(String, String, String, String, DataSource) - Id :"
					+ Id);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getdefaultSOWAssumption(String, String, String, String, DataSource) - fromType :"
					+ fromType);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getdefaultSOWAssumption(String, String, String, String, DataSource) - viewType :"
					+ viewType);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getdefaultSOWAssumption(String, String, String, String, DataSource) - pfType :"
					+ pfType);
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call pf_get_partner_default_value_mpo(?, ?, ?, ?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, Id);
			cstmt.setString(3, fromType);
			cstmt.setString(4, viewType);
			cstmt.setString(5, pfType);
			rs = cstmt.executeQuery();

			if (rs.next()) {
				sowAssumption = rs.getString(1);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("getdefaultSOWAssumption(String, String, String, String, DataSource) - sowAssumption11: "
						+ sowAssumption);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("getdefaultSOWAssumption(String, String, String, String, DataSource) - sowAssumption22: "
						+ sowAssumption.replaceAll("`~", "\n\n"));
			}

		} catch (Exception e) {
			logger.error(
					"getdefaultSOWAssumption(String, String, String, String, DataSource)",
					e);

			logger.error(
					"getdefaultSOWAssumption(String, String, String, String, DataSource)",
					e);
		} finally {
			try {
				DBUtil.close(rs, cstmt); // Close
											// resultset,callablestament
											// objects
				DBUtil.close(conn); // Close database conneciton
									// object
			} catch (Exception e) {
				logger.error(
						"getdefaultSOWAssumption(String, String, String, String, DataSource)",
						e);

				logger.error(
						"getdefaultSOWAssumption(String, String, String, String, DataSource)",
						e);
				logger.error(e.getMessage());
			}
		}
		// System.out.println("SOw Assumptions are : " + sowAssumption);
		return sowAssumption.replaceAll("`~", "\n\n");
	}

	public String getResouceIdByMPO(String mpoId, DataSource ds) {
		PreparedStatement pStmt = null;
		String doc_id = "";
		ResultSet result = null;
		Connection conn = null;
		String resource_list = "";
		String query = " select mp_ar_as_id"
				+ " from lp_mpo_activity_and_resource "
				+ " inner join mp_main on lp_mpo_activity_and_resource.mp_mn_id = mp_main.mp_mn_id "
				+ " where mp_main.mp_mn_id = ?";
		try {
			conn = ds.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, mpoId);
			result = pStmt.executeQuery();
			while (result.next()) {
				resource_list = resource_list + result.getString(1) + ",";
			}
			if (!resource_list.equals(""))
				resource_list = resource_list.substring(0,
						resource_list.length() - 1);
		} catch (Exception e) {
			logger.error("getResouceIdByMPO(String, DataSource)", e);

			logger.error("getResouceIdByMPO(String, DataSource)", e);
		} finally {
			try {
				DBUtil.close(pStmt);
				DBUtil.close(conn); // Close database conneciton
									// object
			} catch (Exception e) {
				logger.error("getResouceIdByMPO(String, DataSource)", e);

				logger.error(e.getMessage());
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getResouceIdByMPO(String, DataSource) - Resource List is :: "
					+ resource_list);
		}
		return resource_list;
	}

	public void resetToDraft(Map<String, String> paramMap,
			Map<String, Object> sessionMap, MPODTO bean) {
		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;
		try {
			String loginUserName = (String) sessionMap.get("username");
			Timestamp ts = new java.sql.Timestamp(
					new java.util.Date().getTime());
			String sql = "update mp_main" + " \n set mp_mn_status = ? ,"
					+ " \n mp_mn_updated_by = ? ,"
					+ " \n mp_mn_updated_date = ? "
					+ " \n where mp_mn_id = ? and mp_appendix_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, paramMap.get("status").toString());
			pStmt.setString(2, loginUserName);
			pStmt.setTimestamp(3, ts);
			pStmt.setString(4, bean.getMpoId());
			pStmt.setString(5, bean.getMp_appendix_id());
			pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error("resetToDraft(HttpServletRequest, MPODTO)", e);

			logger.error("resetToDraft(HttpServletRequest, MPODTO)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	public void setMenuForAppendix(Map<String, Object> map, MPODTO bean,
			String loginuserid, DataSource ds) {
		// For Appendix/NetMedx Dashboard Menu's

		bean.setMsaId(IMdao.getMSAId(bean.getMp_appendix_id(), ds));
		bean.setAppendixName(Jobdao.getAppendixname(ds,
				bean.getMp_appendix_id()));
		bean.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(ds,
				bean.getMsaId()));

		boolean isPm = isPM(loginuserid, bean.getMp_appendix_id());
		if (isPm == true) {
			map.put("PM", "Y");
			bean.setIsPM("Y");
		}

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(bean.getMp_appendix_id(), ds);
		map.put("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				bean.getMp_appendix_id(), ds);
		bean.setAppendixType(Appendixdao.getAppendixPrType(
				bean.getMp_appendix_id(), ds));
		String appendixType = Appendixdao.getAppendixPrType(
				bean.getMp_appendix_id(), ds);
		map.put("appendixcurrentstatus", appendixcurrentstatus);
		map.put("msaId", bean.getMsaId());
		map.put("appendixid", bean.getMp_appendix_id());
		map.put("appendixType", appendixType);

		if (logger.isDebugEnabled()) {
			logger.debug("setMenuForAppendix(HttpServletRequest, MPODTO, String, DataSource) - Appendix Id :"
					+ bean.getMp_appendix_id());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("setMenuForAppendix(HttpServletRequest, MPODTO, String, DataSource) - MSA Id :"
					+ bean.getMsaId());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("setMenuForAppendix(HttpServletRequest, MPODTO, String, DataSource) - Appendix Type is : "
					+ bean.getAppendixType());
		}

		// End
	}
}