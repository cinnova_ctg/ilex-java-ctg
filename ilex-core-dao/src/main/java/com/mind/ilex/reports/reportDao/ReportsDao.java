
/*
* Copyright (C) 2008 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: <This is a Dao file for the reports which has methods for the MSAList and for generating unique key for the document.>
*
*/



package com.mind.ilex.reports.reportDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.mind.common.IlexConstants;
import com.mind.docm.dao.MSA;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;



/**
* Class ReportsDao - This is a Dao class for the reports which has 
* 						methods for the MSAList and for generating unique 
* 						key for the document.
* methods - getMSAlist, getUniqueKey, getStaticConnection
**/
public class ReportsDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ReportsDao.class);
	
	
	/**
	* getMSAlist - This method takes out the MSAList.  
	* @return	Returns ArrayList object that contains the MSAList.
	**/
	public ArrayList getMSAlist()
	{
		logger.trace("Start :(Class : com.mind.ilex.reports.ReportsDao | " +
						"Method : getMSAlist)");
		ArrayList list = new ArrayList();
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = ""; 
		try
		{
			conn = getStaticConnection();
			stmt = conn.createStatement();
			// fetches data for MSA
			logger.trace("Here scheduler fetches the data for MSA .");
			sql = "select lam.lx_pr_id,lmn.lp_mm_id,lot.lo_ot_name from lp_msa_main lmn join lo_organization_top lot on"+
					" \n lp_mm_ot_id = lo_ot_id "+
					" \n join lx_appendix_main lam on"+
					" \n lmn.lp_mm_id = lam.lx_pr_mm_id"+
					" \n and lx_pr_type = 3"+
					" \n order by lam.lx_pr_id,lmn.lp_mm_id";
			rs = stmt.executeQuery( sql );
			logger.trace("Setting up the MSA Object.");
			while( rs.next() )
			{
				MSA msaObj = new MSA();
				msaObj.setEntityId(rs.getString("lp_mm_id"));
				msaObj.setEntityName(rs.getString("lo_ot_name"));
				msaObj.getAppendix().setEntityId(rs.getString("lx_pr_id"));
				list.add(msaObj);
			}
		}
		catch( Exception e )
		{
			logger.error("getMSAlist()", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMSAlist() - Exception caught in rerieving MSA list" + e);
			}
		}
		finally
		{
			DBUtil.closeStatement(rs,stmt);
			
		}
		logger.trace("End :(Class : com.mind.ilex.reports.ReportsDao | " +
						"Method : getMSAlist).");
		return list;
	}

	/**
	* getUniqueKey - This method Generates the UniqueKey. 
	* @param	Connection- Connection to the dataBase.
	* @param	MSA - MSA Object. 
	* @param	report_id - reportId for which the key is generated.
	* @return	Returns UniqueKey .
	**/
	public String getUniqueKey(Connection con,MSA msa,String report_id)
	{
		logger.trace("Start :(Class : com.mind.ilex.reports.ReportsDao |" +
						" Method : getUniqueKey)");
		String uniqueKey = "";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			// makes the unique key for the document using fields used below in query.
			logger.trace("Here getting unique key for the generated document .");
			/*String sql = "Select (convert(varchar(10),rsl.report_id) +'_'+ convert(varchar(10),?)+'_'+"+       
						"convert(varchar(10),rsl.start_date,112)+'_'+convert(varchar(10),rsl.end_date,112)) as uniqueKey"+
						" \n from report_schedule_log rsl where rsl.report_id = ?";*/
	               		
			String sql = "Select (convert(varchar(10),?)+'_'+"+       
			"convert(varchar(10),rsl.start_date,112)+'_'+convert(varchar(10),rsl.end_date,112)) as uniqueKey"+
			" \n from report_schedule_log rsl where rsl.report_id = ?";
			if (logger.isDebugEnabled()) {
				logger.debug("getUniqueKey(Connection, MSA, String) - sqlString " + sql);
			}
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, new Integer(msa.getEntityId()).intValue());
			stmt.setString(2,report_id);
			rs = stmt.executeQuery();
			while(rs.next())
			{
				uniqueKey = rs.getString("uniqueKey");
			}
		}
		catch(Exception e)
		{
			logger.error("getUniqueKey(Connection, MSA, String)", e);

			logger.error(e.getMessage());
		}
		finally{
			DBUtil.closeStatement(rs,stmt);
		}
		logger.trace("End :(Class : com.mind.ilex.reports.ReportsDao | " +
						"Method : getUniqueKey) .");
		return uniqueKey;
	}
	/**
	* getStaticConnection - Static method helps in generating the static 
	* 						dataBase connection. 
	* @return	Returns Static connection.
	**/
	public static Connection getStaticConnection() {
		try {
			return DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME).getConnection();
		} catch (SQLException e) {
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	  }
}
