//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : Report.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.ilex.reports.reportDao;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.mind.bean.docm.Field;
import com.mind.bean.docm.IlexEntity;
import com.mind.docm.util.DocumentUtility;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;

public class Report extends com.mind.bean.docm.IlexEntity {
	private static final Logger logger = Logger.getLogger(Report.class);

	/**
	 * @name getIlexToDocMMapping
	 * @purpose This method is used as a convertor which converts the IlexEntity
	 *          Object into the MetaData Property of the Document Object.
	 * @steps 1. EntityManager initialtes the call to the method for conversion
	 *        process. 2. In turns corresponds the columns available in the DocM
	 *        System for the Documents with the IlexEntity properties.
	 * @param IlexEntity
	 * @param length
	 * @return Field[]
	 * @returnDescription The array of Field[] property of the MetaData Object.
	 */
	public static Field[] getIlexToDocMMapping(IlexEntity ilexEntity,
			long length, String fileName) {
		logger.trace("(Class : com.mind.ilex.docmanager.Report | Method : getIlexToDocMMapping)");
		logger.trace("This method is used as a convertor which converts the IlexEntity Object into the MetaData Property of the Document Object(MSA).");
		ArrayList fieldsList = new ArrayList();
		Field[] fields = null;
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.title"), ilexEntity
				.getEntityName()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.documentdate"),
				ilexEntity.getEntityCreatedDate()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.type"), ilexEntity
				.getEntityType()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.status"), "Approved"));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.author"), ilexEntity
				.getEntityCreatedBy()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.category"),
				"Project-Status"));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.remarks"),
				"Changed by Ilex"));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.source"), "Ilex"));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.adddate"), ilexEntity
				.getEntityChangeDate()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.adduser"), ilexEntity
				.getEntityCreatedBy()));// ilexEntity.getEntityChangeBy()
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.addmethod"), "Implicit"));
		fieldsList
				.add(new Field(
						DatabaseUtilityDao
								.getKeyValue("ilex.docm.entitymanager.changecontroltype"),
						"0"));
		fieldsList
				.add(new Field(
						DatabaseUtilityDao
								.getKeyValue("ilex.docm.entitymanager.updatedocumentdate"),
						""));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.updatedocumentuser"),
				null));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.updatemethod"),
				"Implicit"));
		fieldsList
				.add(new Field(
						DatabaseUtilityDao
								.getKeyValue("ilex.docm.entitymanager.previousdocumentreference"),
						null));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.deletedate"), null));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.deleteuser"), null));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.viewonweb"), "1"));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.archivedate"), ""));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.archivefilename"), null));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.basedocument"),
				ilexEntity.getEntityBaseDocument()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.application"), null));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.size"), "" + length));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.mime"), "PDF"));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.msaid"), ilexEntity
				.getEntityMSAId()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.appendixid"), ilexEntity
				.getEntityAppendixId()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.jobid"), ilexEntity
				.getEntityJobId()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.activityid"), null));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.poid"), ilexEntity
				.getEntityPoId()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.woid"), ilexEntity
				.getEntityWoId()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.invoiceid"), null));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.changecontrolid"), null));

		// following properties are added for NetMedX Detailed Summary Report.
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitynetmedx.id"), ilexEntity
				.getEntityNetMedXReportId()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("netmedx.report.flt.startdate"), ilexEntity
				.getReportFltStartDate()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("netmedx.report.flt.enddate"), ilexEntity
				.getReportFltEndDate()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("netmedx.report.flt.criticality"), ilexEntity
				.getReportFltCriticality()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("netmedx.report.flt.status"), ilexEntity
				.getReportFltReportStatus()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("netmedx.report.flt.requestor"), ilexEntity
				.getReportFltRequestor()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("netmedx.report.flt.arrival.options"), ilexEntity
				.getReportFltArrivalOptions()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("netmedx.report.flt.msp"), ilexEntity
				.getReportFltMsp()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("netmedx.report.flt.helpdesk"), ilexEntity
				.getReportFltHelpDesk()));

		// following properties are added for different fields.
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.msaname"), ilexEntity
				.getMsaName()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.appendixname"),
				ilexEntity.getAppendixName()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.jobname"), ilexEntity
				.getJobName()));
		fieldsList.add(new Field(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entitymanager.partnername"), ilexEntity
				.getPartnerName()));
		// fieldsList.add(new
		// Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.wopartnerid"),ilexEntity.getWoPartnerId()));
		// fieldsList.add(new
		// Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.dtlnedxidname"),ilexEntity.getDtlNedxIdName()));

		fields = DocumentUtility.fieldArray(fieldsList);
		return fields;
	}

	public static Field[] getIlexToDocMMapping(IlexEntity ilexEntity,
			long length) {
		return getIlexToDocMMapping(ilexEntity, length, null);
	}
}
