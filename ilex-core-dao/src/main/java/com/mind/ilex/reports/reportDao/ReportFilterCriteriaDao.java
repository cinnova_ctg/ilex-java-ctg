/*
 * Copyright (C) 2008 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: <This class contains the whole logic of running and halting of the report scheduler.>
 *
 */

package com.mind.ilex.reports.reportDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.reports.ReportFilterCriteria;
import com.mind.docm.dao.MSA;
import com.mind.docm.dao.MSAList;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * Class ReportFilterCriteriaDao - This class contains the whole logic of
 * running and halting of the report scheduler. methods -
 * getReportFilterCriteria, setFilterCriteria, checkDataExist, getRFCWithPast,
 * getDays, compareDate, getPreviousDate
 */

public class ReportFilterCriteriaDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ReportFilterCriteriaDao.class);

	/**
	 * getReportFilterCriteria - This method checks whether the data for the
	 * past month exist or not.
	 * 
	 * @param reportType
	 *            - ReportType which specifies the type of report for which the
	 *            rpeport scheduler runs.
	 * @param con
	 *            - Connection object.
	 * @param pastTrack
	 *            - which specifies for how many past months scheduler runs if
	 *            its zero then scheduler by passed or not runs.
	 * @param startDateParam
	 *            - startDateParam which specifies the start date param for
	 *            report .
	 * @param endDateParam
	 *            - endDateParam which specifies the start date param for report
	 *            .
	 * @return Returns report filter criteria object.
	 **/
	public ReportFilterCriteria getReportFilterCriteria(String reportType,
			Connection con, String pastTrack, Date startDateParam,
			Date endDateParam) {
		logger.trace("Start :(Class : com.mind.ilex.reports."
				+ "ReportFilterCriteriaDao | Method : getReportFilterCriteria)");
		ReportFilterCriteria reportFilterCriteria = null;
		Statement stmt = null;
		Statement innerStmt = null;
		ResultSet rs = null;
		ResultSet innerRs = null;
		String sql = "";
		String innerSql = "";
		boolean isExists = false;
		Date startDate = new Date();
		Date finalLastDate = null;
		String reportName;
		String criticality;
		String status;
		String requestor;
		String arrivalOptions;
		String msp;
		String helpDesk;
		try {
			logger.trace("Here we check whats the data "
					+ "with the max startdate w.r.t this report type .");
			// this query checks whether the data exist in those tables or not.
			sql = "SELECT MAX(RSL.START_DATE) AS START_DATE,RSL.REPORT_ID FROM REPORT_SCHEDULE_LOG RSL "
					+ " \n JOIN REPORT_SCHEDULE_MAIN RSM ON"
					+ " \n RSL.REPORT_ID = RSM.REPORT_ID"
					+ " \n WHERE RSM.REPORT_TYPE='"
					+ reportType
					+ "'"
					+ " \n GROUP BY RSL.REPORT_ID";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			// if exist than below query exist other wise the belo to below one.
			while (rs.next()) {
				innerSql = "SELECT RSP.REPORT_NAME,RSP.CRITICALITY,RSP.STATUS,RSP.REQUESTOR,RSP.ARRIVAL_OPTIONS,"
						+ " \n RSP.MSP,RSP.HELP_DESK ,RSG.START_DATE,RSG.END_DATE"
						+ " \n FROM REPORT_SCHEDULE_PARAMS RSP JOIN REPORT_SCHEDULE_LOG RSG ON "
						+ " \n RSP.REPORT_TYPE = (SELECT REPORT_TYPE FROM REPORT_SCHEDULE_MAIN "
						+ " \n WHERE REPORT_ID='"
						+ rs.getString("REPORT_ID")
						+ "')"
						+ " \n WHERE RSG.REPORT_ID ='"
						+ rs.getString("REPORT_ID") + "'";
				isExists = true;
			}
			if (isExists == false) {
				isExists = false;
				innerSql = "SELECT * FROM REPORT_SCHEDULE_PARAMS WHERE REPORT_TYPE='"
						+ reportType + "'";
			}
			innerStmt = con.createStatement();
			innerRs = innerStmt.executeQuery(innerSql);
			while (innerRs.next()) {
				if (isExists == false) {
					startDate = startDateParam;
					finalLastDate = endDateParam;
				} else {

					startDate = startDateParam;
					finalLastDate = endDateParam;
				}
				reportName = innerRs.getString("REPORT_NAME");
				criticality = innerRs.getString("CRITICALITY");
				status = innerRs.getString("STATUS");
				arrivalOptions = innerRs.getString("ARRIVAL_OPTIONS");
				requestor = innerRs.getString("REQUESTOR");
				msp = innerRs.getString("MSP");
				helpDesk = innerRs.getString("HELP_DESK");
				/*
				 * this checks whether the data with the same dates exist or not
				 * and also checks that the end date must not be greater than
				 * todays date.
				 */
				logger.trace("Here we check finishLastDate must not be "
						+ "greater then todays date.");
				logger.trace("Here we also check data for these dates "
						+ "must not be in database.");
				if ((this.compareDate(finalLastDate, new Date()))) {

					return null;
				}
				// if above is not true then object for reportfilter criteria is
				// set down other wise null would be returned.
				reportFilterCriteria = this.setFilterCriteria(reportName,
						reportType, startDate, finalLastDate, criticality,
						status, arrivalOptions, requestor, msp, helpDesk);
			}
		}

		catch (Exception e) {
			logger.error(
					"getReportFilterCriteria(String, Connection, String, Date, Date)",
					e);

			logger.error(e.getMessage());
		} finally {
			DBUtil.close(rs, stmt);

			DBUtil.close(innerRs, innerStmt);

			logger.trace("End :(Class : com.mind.ilex.reports.ReportFilterCriteriaDao |"
					+ " Method : getReportFilterCriteria)");
		}
		return reportFilterCriteria;
	}

	/**
	 * setFilterCriteria - This method sets the report filter criteria for the
	 * report on the values passed.
	 * 
	 * @param reportName
	 *            - ReportName which specifies the type of report for which the
	 *            report scheduler runs.
	 * @param reportType
	 *            - ReportType which specifies the type of report for which the
	 *            report scheduler runs.
	 * @param startDate
	 *            - startDate which specifies the start date param for report .
	 * @param endDate
	 *            - endDate which specifies the type of report for which the
	 *            report scheduler runs.
	 * @param criticality
	 *            - criticality which specifies criticality factor for the
	 *            report.
	 * @param status
	 *            - status criticality which specifies status factor for the
	 *            report.
	 * @param arrivalOptions
	 *            - arrivalOptions which specifies arrivalOptions factor for the
	 *            report.
	 * @param requestor
	 *            - requestor which specifies requestor factor for the report.
	 * @param msp
	 *            - msp which specifies msp factor for the report.
	 * @param helpDesk
	 *            - helpDesk which specifies helpDesk factor for the report.
	 * @return Returns report filter criteria object.
	 */
	public ReportFilterCriteria setFilterCriteria(String reportName,
			String reportType, Date startDate, Date endDate,
			String criticality, String status, String arrivalOptions,
			String requestor, String msp, String helpDesk) {
		logger.trace("Start :(Class : com.mind.ilex.reports.ReportFilterCriteriaDao |"
				+ " Method : setFilterCriteria)");
		logger.trace("Setting up the report filter criteria object.");
		SimpleDateFormat sf = new SimpleDateFormat("MM/dd/yyyy");
		// helps in setting the data in reportfiltercriteria object.
		ReportFilterCriteria reportFilterCriteria = new ReportFilterCriteria();
		reportFilterCriteria.setReportName(reportName);
		reportFilterCriteria.setReportType(reportType);
		reportFilterCriteria.setStartDate(sf.format(startDate));
		reportFilterCriteria.setEndDate(sf.format(endDate));
		reportFilterCriteria.setCriticality(criticality);
		reportFilterCriteria.setStatus(status);
		reportFilterCriteria.setRequestor(requestor);
		reportFilterCriteria.setArrivalOptions(arrivalOptions);
		reportFilterCriteria.setMsp(msp);
		reportFilterCriteria.setHelpDesk(helpDesk);
		logger.trace("End :(Class : com.mind.ilex.reports.ReportFilterCriteriaDao | Method : setFilterCriteria)");
		return reportFilterCriteria;

	}

	/**
	 * checkDataExist - This method checks whether the data for the date passed
	 * to the method exist in the database or not, so on the basis of that it
	 * returns true or false.
	 * 
	 * @param reportType
	 *            - ReportType which specifies the type of report for which the
	 *            rpeport scheduler runs.
	 * @param startDate
	 *            - For which the data is to be checked.
	 * @param endDate
	 *            - For which the data is to be checked.
	 * @return Returns true or false.
	 */

	public boolean checkDataExist(String reportType, Date startDate,
			Date endDate, Connection con) {
		// This checks whether the data with the same dates exist or not if yes
		// then return true other wise false.
		logger.trace("Start :(Class : com.mind.ilex.reports.ReportFilterCriteriaDao |"
				+ " Method : checkDataExist)");
		Statement stmt = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		logger.trace("Here in the logic we check whether data with dates exist or not.");
		try {
			SimpleDateFormat sf = new SimpleDateFormat("dd MMM yyyy");

			String sql = "select report_id,start_date,end_date from report_schedule_log "
					+ " \n where report_id in (select report_id from report_schedule_main where report_type= ? )"
					+ " \n and convert(varchar,start_date,106) = ?"
					+ " \n and convert(varchar,end_date,106) = ?";
			pStmt = con.prepareStatement(sql);
			pStmt.setString(1, reportType);
			pStmt.setString(2, sf.format(startDate));
			pStmt.setString(3, sf.format(endDate));
			/*
			 * String sql =
			 * "select report_id,start_date,end_date from report_schedule_log "
			 * +
			 * " \n where report_id in (select report_id from report_schedule_main where report_type='"
			 * + reportType + "')" +
			 * " \n and convert(varchar,start_date,106) = '" +
			 * sf.format(startDate) + "' " +
			 * " \n and convert(varchar,end_date,106) = '" + sf.format(endDate)
			 * + "'";
			 */
			rs = pStmt.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (Exception e) {
			logger.error("checkDataExist(String, Date, Date, Connection)", e);

			logger.error(e.getMessage());
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(con);
			logger.trace("End :(Class : com.mind.ilex.reports.ReportFilterCriteriaDao |"
					+ " Method : checkDataExist)");
		}

		return false;
	}

	/**
	 * getRFCWithPast - This method returns the report filter criteria object
	 * for yhe report. method fetchs data from the database on the basis of
	 * which it is decided that whether schedule runs or not and is also checks
	 * for how many time it must be run.
	 * 
	 * @param ReportType
	 *            - which specifies the type of report for which the rpeport
	 *            scheduler runs.
	 * @param Connection
	 *            - which is the data base connection.
	 * @param pastTrack
	 *            - which specifies for how many past months scheduler runs if
	 *            its zero then scheduler by passed or not runs.
	 * @return Returns either null or report filter criteria object for the
	 *         report to be run by the report scheduler.
	 */

	public ReportFilterCriteria getRFCWithPast(String reportType,
			Connection con, String pastTrack) {
		logger.trace("Start :(Class : com.mind.ilex.reports.ReportFilterCriteriaDao |"
				+ " Method : getRFCWithPast)");
		Date startDate = new Date();
		Date finalLastDate = new Date();
		// Date newRefractedDate = new Date();
		ReportFilterCriteria reportFilterCriteria = null;
		logger.trace("Here thread try to get date which is far behind the number "
				+ "of months by past track.");
		Calendar cal = this.getPreviousDate(new Integer(pastTrack).intValue(),
				con);
		cal.set(cal.DATE, 1);
		Calendar calForFinish = this.getPreviousDate(
				new Integer(pastTrack).intValue(), con);
		calForFinish.set(calForFinish.DATE,
				calForFinish.getActualMaximum(calForFinish.DATE));

		startDate = cal.getTime();
		finalLastDate = calForFinish.getTime();

		// newRefractedDate = this.getPreviousDate(new
		// Integer(pastTrack).intValue(),con);

		/* lines below sets the start and dates. */
		logger.trace("Setting of start and end dates.");
		// startDate.setDate(1);
		// startDate.setMonth(newRefractedDate.getMonth());
		// startDate.setYear(newRefractedDate.getYear());
		// finalLastDate.setDate(getDays(startDate.getMonth(),
		// startDate.getYear()));
		// finalLastDate.setMonth(startDate.getMonth());
		// finalLastDate.setYear(startDate.getYear());
		try {
			logger.trace("Call to getReportFilterCriteria Method .");
			reportFilterCriteria = this.getReportFilterCriteria(reportType,
					con, pastTrack, startDate, finalLastDate);
		} catch (Exception e) {
			logger.error("getRFCWithPast(String, Connection, String)", e);

			logger.error(e.getMessage());
		}
		logger.trace("End :(Class : com.mind.ilex.reports.ReportFilterCriteriaDao |"
				+ " Method : getRFCWithPast).");
		return reportFilterCriteria;
	}

	/**
	 * getDays - This method returns the no of days in that perticular month for
	 * that perticular year that have been passed to the method .
	 * 
	 * @param month
	 *            - month for which the days are to be find out.
	 * @param year
	 *            - year for which the days are to be find out for the month
	 *            above.
	 * @return Returns the no of days or zero.
	 */

	public int getDays(int month, int year) {
		logger.trace("Start :(Class : com.mind.ilex.reports.ReportFilterCriteriaDao | "
				+ "Method : getDays)");
		if (month == 0 || month == 2 || month == 4 || month == 6 || month == 7
				|| month == 9 || month == 11)
			return 31;
		if (month == 3 || month == 5 || month == 8 || month == 10)
			return 30;
		if (month == 1 && year % 4 == 0)
			return 29;
		if (month == 1)
			return 28;
		logger.trace("End :(Class : com.mind.ilex.reports.ReportFilterCriteriaDao | "
				+ "Method : getDays).");
		return 0;
	}

	/**
	 * compareDate - This method checks whether the first date is greater than
	 * the second or not if yes then true else false.
	 * 
	 * @param date1
	 *            - date to be compared.
	 * @param date2
	 *            - date to be compared.
	 * @return Returns either true or false.
	 */
	public boolean compareDate(Date date1, Date date2) {
		Calendar calForDate1 = Calendar.getInstance();
		Calendar calForDate2 = Calendar.getInstance();
		calForDate1.setTime(date1);
		calForDate2.setTime(date2);
		logger.trace("Start :(Class : com.mind.ilex.reports.ReportFilterCriteriaDao | "
				+ "Method : compareDate)");

		if (calForDate1.get(calForDate1.DATE) > calForDate2
				.get(calForDate2.DATE)
				&& calForDate1.get(calForDate1.MONTH) >= calForDate2
						.get(calForDate2.MONTH)
				&& calForDate1.get(calForDate1.YEAR) >= calForDate2
						.get(calForDate2.YEAR))
			return true;
		// if(date1.getDate() > date2.getDate() && date1.getMonth() >=
		// date2.getMonth() && date1.getYear() >= date2.getYear())
		// return true;
		logger.trace("End :(Class : com.mind.ilex.reports.ReportFilterCriteriaDao | "
				+ "Method : compareDate).");
		return false;
	}

	/**
	 * getPreviousDate - This method returns the date which is that much moonths
	 * previous from the currrent date.
	 * 
	 * @param CuurentDate
	 *            - todays date.
	 * @param noOfMonths
	 *            - no of months which are subtracted from the current date.
	 * @return Returns Date.
	 */
	public Calendar getPreviousDate(int noOfMonths, Connection con) {
		logger.trace("Start :(Class : com.mind.ilex.reports.ReportFilterCriteriaDao |"
				+ " Method : getPreviousDate)");
		Statement stmt = null;
		ResultSet rs = null;
		String months = "-" + String.valueOf(noOfMonths);
		noOfMonths = new Integer(months).intValue();

		// this query gives the date which is far behind by noOfmonths.
		logger.trace("Here we find out the date far behind from the current one "
				+ "by the value in past track .");
		String sql = "select dateadd(mm," + noOfMonths
				+ ",getdate()) as pastDate";

		Calendar cal = null;
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				cal = Calendar.getInstance();
				cal.setTime(rs.getDate("pastDate"));
				// cal.set(calendar.DATE, 7);

				// new GregorianCalendar(rs.getDate("pastDate"));
				// java.sql.Date d=
				// calendar.set(d.getYear(),d.getMonth(), 7);
				// ((rs.getDate("pastDate").getTime()));
				// calendar.set(calendar.DATE,7);
				// System.out.println("gregorian change"+cal);
			}
		} catch (Exception e) {
			logger.error("getPreviousDate(int, Connection)", e);

			logger.error(e.getMessage());
		} finally {
			DBUtil.close(rs, stmt);

		}
		logger.trace("End :(Class : com.mind.ilex.reports.ReportFilterCriteriaDao | "
				+ "Method : getPreviousDate).");
		return cal;
	}

	/**
	 * @name getReportFilterCriteriaForRerun
	 * @purpose This method gets the reportfiltercriteria for the rerunning of
	 *          the report scheduler.
	 * @param reportType
	 *            - Takes Report type parameter.
	 * @param startDate
	 *            - Start date for the report parameters.
	 * @param endDate
	 *            - End date for the report parameters.
	 * @param request
	 *            - HttpServletRequest Object.
	 * @param DataSource
	 *            - An Object of dataSource.
	 * @return Returns ReportFilterCriteria Object.
	 * @returnDescription none
	 */
	public ReportFilterCriteria getReportFilterCriteriaForRerun(
			String reportType, Date startDate, Date endDate,
			DataSource dataSource) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ReportFilterCriteria reportFilterCriteria = null;
		String reportName;
		String criticality;
		String status;
		String requestor;
		String arrivalOptions;
		String msp;
		String helpDesk;
		try {
			String sql = "select * from report_schedule_params where report_type ='"
					+ reportType + "'";
			// DataSource dataSource = (DataSource)
			// request.getSession(false).getAttribute("datasource");
			conn = dataSource.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				reportName = rs.getString("REPORT_NAME");
				criticality = rs.getString("CRITICALITY");
				status = rs.getString("STATUS");
				arrivalOptions = rs.getString("ARRIVAL_OPTIONS");
				requestor = rs.getString("REQUESTOR");
				msp = rs.getString("MSP");
				helpDesk = rs.getString("HELP_DESK");
				reportFilterCriteria = this.setFilterCriteria(reportName,
						reportType, startDate, endDate, criticality, status,
						arrivalOptions, requestor, msp, helpDesk);
			}
		} catch (Exception e) {
			logger.error(
					"getReportFilterCriteriaForRerun(String, Date, Date, HttpServletRequest, DataSource)",
					e);

			logger.error(e.getMessage());
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return reportFilterCriteria;
	}

	/**
	 * @name getFilteredMSAList
	 * @purpose This method took out the filter criteria for which are not
	 *          already in data base.
	 * @param msaList
	 *            - Takes the MSAList
	 * @param reportDateFilterCriteria
	 *            - An Object of ReportFilterCriteria.
	 * @return Returns MSAList.
	 * @returnDescription none
	 */
	public static MSAList getFilteredMSAList(MSAList msaList,
			String reportType, ReportFilterCriteria reportDateFilterCriteria) {
		Connection conn = null;
		MSAList returnMsaList = new MSAList();
		List list = new ArrayList();
		boolean isExist = true;
		try {
			conn = ReportsDao.getStaticConnection();
			for (int i = 0; i < msaList.getMsaArray().length; i++) {
				MSA msa = msaList.getMsaArray()[i];
				// System.out.println("-----msa id------------"+msa.getEntityId());
				isExist = isMSAExist(msa, reportType,
						reportDateFilterCriteria.getStartDate(),
						reportDateFilterCriteria.getEndDate(), conn);
				if (!isExist) {
					list.add(msaList.getMsaArray()[i]);
				}
			}
			MSA[] msaArray = new MSA[list.size()];
			for (int j = 0; j < list.size(); j++) {
				msaArray[j] = (MSA) list.get(j);
				// System.out.println("-----------return msa-----------"+msaArray[j].getEntityId());
			}
			returnMsaList.setMsaArray(msaArray);
		} catch (Exception e) {
			logger.error(
					"getFilteredMSAList(MSAList, String, ReportFilterCriteria)",
					e);

			logger.error(e.getMessage());
		} finally {
			DBUtil.close(conn);
		}
		return returnMsaList;
	}

	/**
	 * @name isMSAExist
	 * @purpose This method checks whether the MSA exist in the data base or
	 *          not.
	 * @param msa
	 *            - TAn Object of MSA.
	 * @param reportType
	 *            - Takes Report type parameter.
	 * @param startDate
	 *            - Start date for the report parameters.
	 * @param endDate
	 *            - End date for the report parameters.
	 * @param conn
	 *            - An Object of Connection.
	 * @return Returns reportId or null.
	 * @returnDescription none
	 */
	public static boolean isMSAExist(MSA msa, String reportType,
			String startDate, String EndDate, Connection conn) {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		boolean isExist = false;
		try {
			String sql = "select iteration_id from report_schedule_detail where iteration_id =? and report_id in"
					+ " \n (select report_id from report_schedule_log where convert(varchar,start_date,101)=?"
					+ " \n and convert(varchar,end_date,101) = ? and report_id in (select report_id from report_schedule_main where report_type=?))";

			// System.out.println("sql"+sql);
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, msa.getEntityId());
			stmt.setString(2, startDate);
			stmt.setString(3, EndDate);
			stmt.setString(4, reportType);
			rs = stmt.executeQuery();
			while (rs.next()) {
				isExist = true;
				break;
			}
			if (isExist)
				return true;
		} catch (Exception e) {
			logger.error("isMSAExist(MSA, String, String, String, Connection)",
					e);

			logger.error(e.getMessage());
		} finally {
			DBUtil.close(rs, stmt);

		}
		return false;
	}
}
