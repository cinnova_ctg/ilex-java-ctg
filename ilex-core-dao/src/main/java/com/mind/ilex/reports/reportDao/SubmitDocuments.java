/*
 * Copyright (C) 2008 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: <This file contains the whole logic for inserting the logic for the generated reports and documents.>
 *
 */

package com.mind.ilex.reports.reportDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.mind.bean.reports.ReportFilterCriteria;
import com.mind.docm.dao.MSA;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * Class SubmitDocuments - This class contains the whole logic for inserting the
 * logic for the generated reports and documents. methods - submitDocuments,
 * updateReportInformation, firstsubmitReportInformation.
 **/
public class SubmitDocuments {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(SubmitDocuments.class);

	/**
	 * Method explanation - This method inserts the information for the
	 * generated documents.
	 * 
	 * @param reportId
	 *            - ReportId for which the document is generated.
	 * @param MSA
	 *            - For which the report is generated.
	 * @param reportFilterCriteria
	 *            - it contains the filter criteria of the object.
	 * @param entity_id
	 *            - unique entity id for the document.
	 * @return nothing.
	 **/
	public void submitDocuments(String reportId, MSA msa,
			ReportFilterCriteria reportFilterCriteria, String entity_id,
			Connection con, String userCode) throws SQLException {
		logger.trace("Start :(Class : com.mind.ilex.reports.reportDao.SubmitDocuments |"
				+ " Method : submitDocuments)");
		// Connection con = ReportsDao.getStaticConnection();
		PreparedStatement stmt = null;
		// inserts the information for the documents generated for the report.
		logger.trace("Here Scheduler the information related to each document generated w.r.t the report .");
		String sql2 = "INSERT INTO REPORT_SCHEDULE_DETAIL (REPORT_ID,ITERATION_ID,RUN_DURATION,CREATION_DATE,"
				+ "CREATED_BY,MODIFICATION_DATE,MODIFIED_BY,ENTITY_ID,APPENDIX_ID)"
				+ " \n VALUES (?,?,?,GETDATE(),?,GETDATE(),?,?,?)";
		try {
			stmt = con.prepareStatement(sql2);
			stmt.setString(1, reportId);
			stmt.setString(2, msa.getEntityId());
			stmt.setLong(3, new Long(0).longValue());
			if (userCode != null) {
				stmt.setInt(4, new Integer(userCode).intValue());
			} else {
				stmt.setInt(4, -1);
			}
			stmt.setInt(5, -1);
			stmt.setString(6, entity_id);
			stmt.setString(7, msa.getAppendix().getEntityId());
			stmt.executeUpdate();
		} catch (Exception e) {
			logger.error(
					"submitDocuments(String, MSA, ReportFilterCriteria, String, Connection, String)",
					e);

			// e.printStackTrace();
			logger.error(e.getMessage());
		} finally {
			DBUtil.close(stmt);
			// DatabaseUtilityDao.close(con);
			logger.trace("End :(Class : com.mind.ilex.reports.reportDao.SubmitDocuments |"
					+ " Method : submitDocuments)");
		}
	}

	/**
	 * Method explanation - This method updates the information the information
	 * for the generated report.
	 * 
	 * @param reportId
	 *            - ReportId for which the document is generated.
	 * @param MSA
	 *            - For which the report is generated.
	 * @param count
	 *            - which takes care of the msa id for which the iteration is
	 *            going on.
	 * @return nothing.
	 **/
	public void updateReportInformation(String reportId, MSA msa,
			ReportFilterCriteria reportFilterCriteria, int count, Connection con)
			throws SQLException {
		logger.trace("Start :(Class : com.mind.ilex.reports.reportDao.SubmitDocuments |"
				+ " Method : updateReportInformation)");
		// Connection con = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;
		String iterationOn = "MSA";
		String maxVal = null;
		try {

			maxVal = this.getMaxReportId(reportFilterCriteria, con);
			// updates the information for report generated for each MSA.
			logger.trace("Now scheduler updates the information related to the report "
					+ "generated w.r.t each MSA.");
			String sql_update = "UPDATE REPORT_SCHEDULE_MAIN "
					+ " \n SET ITERATION_COUNT =?, ITERATION_ON=?"
					+ " \n WHERE REPORT_ID=?";
			pStmt = con.prepareStatement(sql_update);
			pStmt.setInt(1, count);
			pStmt.setString(2, iterationOn);
			pStmt.setInt(3, Integer.parseInt(reportId));
			pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error(
					"updateReportInformation(String, MSA, ReportFilterCriteria, int, Connection)",
					e);

			logger.error(e.getMessage());
		} finally {
			DBUtil.close(pStmt);
			// DatabaseUtilityDao.close(con);
			logger.trace("End :(Class : com.mind.ilex.reports.reportDao.SubmitDocuments |"
					+ " Method : updateReportInformation)");
		}
	}

	/**
	 * Method explanation - This method inserts the information for the
	 * generated report .
	 * 
	 * @param reportFilterCriteria
	 *            - it contains the filter criteria of the object.
	 * @return maximum vvalue of the reportId.
	 **/
	public String submitReportInformation(
			ReportFilterCriteria reportFilterCriteria, Connection con)
			throws SQLException {
		logger.trace("Start :(Class : com.mind.ilex.reports.reportDao.SubmitDocuments |"
				+ " Method : firstsubmitReportInformation)");
		// Connection con = ReportsDao.getStaticConnection();
		PreparedStatement stmt = null;
		String maxVal = null;
		String iterationOn = "MSA";
		try {

			// inserts infomation of the report im main table.
			logger.trace("Insert the information related to the report generated "
					+ "in main and log tables.");
			String sql = "INSERT INTO REPORT_SCHEDULE_MAIN (REPORT_NAME,REPORT_TYPE,TOTAL_RUN_DURATION,"
					+ "ITERATION_COUNT,ITERATION_ON,START_DATE,STARTED_BY)"
					+ " \n VALUES (?,?," + "0,0,?,GETDATE(),-1)";
			// con.setAutoCommit(false);
			stmt = con.prepareStatement(sql);
			stmt.setString(1, reportFilterCriteria.getReportName());
			stmt.setString(2, reportFilterCriteria.getReportType());
			stmt.setString(3, iterationOn);
			int result = stmt.executeUpdate();
			if (result > 0) {
				maxVal = this.getMaxReportId(reportFilterCriteria, con);
				// inserts infomation of the report im log table.
				String sql1 = "INSERT INTO REPORT_SCHEDULE_LOG (REPORT_ID,START_DATE,END_DATE,"
						+ "CRITICALITY,STATUS,REQUESTOR,ARRIVAL_OPTIONS,MSP,HELP_DESK)"
						+ " \n VALUES (?,CONVERT(DATETIME,?)"
						+ ",CONVERT(DATETIME,?),?" + ",?,?" + ",?,?,?)";

				stmt = con.prepareStatement(sql1);
				stmt.setInt(1, Integer.parseInt(maxVal));
				stmt.setString(2, reportFilterCriteria.getStartDate());
				stmt.setString(3, reportFilterCriteria.getEndDate());
				stmt.setString(4, reportFilterCriteria.getCriticality());
				stmt.setString(5, reportFilterCriteria.getStatus());
				stmt.setString(6, reportFilterCriteria.getRequestor());
				stmt.setString(7, reportFilterCriteria.getArrivalOptions());
				stmt.setString(8, reportFilterCriteria.getMsp());
				stmt.setString(9, reportFilterCriteria.getHelpDesk());
				stmt.executeUpdate();
				// con.commit();
				// con.setAutoCommit(true);
			}
			// else
			// {
			// con.rollback();
			// }
		}

		catch (Exception e) {
			logger.error(
					"submitReportInformation(ReportFilterCriteria, Connection)",
					e);

			logger.error(
					"submitReportInformation(ReportFilterCriteria, Connection)",
					e);
		} finally {
			DBUtil.close(stmt);
			// DatabaseUtilityDao.close(con);
			logger.trace("End :(Class : com.mind.ilex.reports.reportDao.SubmitDocuments |"
					+ " Method : firstsubmitReportInformation)");
		}
		return maxVal;
	}

	public void updateReportScheduleDetail(String reportId, String entity_id,
			long runDuration, Connection con, String userCode)
			throws SQLException {
		if (logger.isDebugEnabled()) {
			logger.debug("updateReportScheduleDetail(String, String, long, Connection, String) - in updateReportScheduleDetail and userCode is ---"
					+ userCode);
		}
		logger.trace("Start :(Class : com.mind.ilex.reports.reportDao.SubmitDocuments |"
				+ " Method : updateReportScheduleDetail)");
		// Connection con = ReportsDao.getStaticConnection();
		PreparedStatement stmt = null;
		// inserts the information for the documents generated for the report.
		logger.trace("Here Scheduler the information related to each document generated w.r.t the report .");
		String sql2 = "UPDATE REPORT_SCHEDULE_DETAIL "
				+ " \n SET RUN_DURATION = ?, " + " \n CREATED_BY = ? ,"
				+ " \n MODIFIED_BY = ? "
				+ " \n WHERE ENTITY_ID = ? AND REPORT_ID = ? ";
		try {
			stmt = con.prepareStatement(sql2);
			stmt.setLong(1, runDuration);
			if (userCode != null) {
				stmt.setInt(2, new Integer(userCode).intValue());
				stmt.setInt(3, new Integer(userCode).intValue());
			} else {
				stmt.setInt(2, -1);
				stmt.setInt(3, -1);
			}
			stmt.setString(4, entity_id);
			stmt.setString(5, reportId);
			stmt.executeUpdate();
		} catch (Exception e) {
			logger.error(
					"updateReportScheduleDetail(String, String, long, Connection, String)",
					e);

			logger.error(e.getMessage());
		} finally {
			DBUtil.close(stmt);
			// DatabaseUtilityDao.close(con);
			logger.trace("End :(Class : com.mind.ilex.reports.reportDao.SubmitDocuments |"
					+ " Method : updateReportScheduleDetail)");
		}
	}

	public void updateReportScheduleMain(
			ReportFilterCriteria reportFilterCriteria, long runDuration) {
		logger.trace("Start :(Class : com.mind.ilex.reports.reportDao.SubmitDocuments |"
				+ " Method : updateReportScheduleDetail)");
		Connection con = ReportsDao.getStaticConnection();
		PreparedStatement stmt = null;
		String maxVal = this.getMaxReportId(reportFilterCriteria, con);
		// inserts the information for the documents generated for the report.
		logger.trace("Here Scheduler the information related to each document generated w.r.t the report .");
		String sql2 = "UPDATE REPORT_SCHEDULE_MAIN"
				+ " \n SET TOTAL_RUN_DURATION = ?" + " \n WHERE REPORT_ID = ? ";
		try {
			stmt = con.prepareStatement(sql2);
			stmt.setLong(1, runDuration);
			stmt.setInt(2, Integer.parseInt(maxVal));
			stmt.executeUpdate();
		} catch (Exception e) {
			logger.error(
					"updateReportScheduleMain(ReportFilterCriteria, long)", e);

			logger.error(e.getMessage());
		} finally {
			DBUtil.close(stmt);
			DBUtil.close(con);
			logger.trace("End :(Class : com.mind.ilex.reports.reportDao.SubmitDocuments |"
					+ " Method : updateReportScheduleDetail)");
		}
	}

	public String getMaxReportId(ReportFilterCriteria reportFilterCriteria,
			Connection con) {
		// Connection con = ReportsDao.getStaticConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String maxVal = "";
		try {
			stmt = con
					.prepareStatement("SELECT MAX(REPORT_ID) AS MAXVAL FROM REPORT_SCHEDULE_MAIN WHERE REPORT_TYPE=? AND REPORT_NAME=?");
			stmt.setString(1, reportFilterCriteria.getReportType());
			stmt.setString(2, reportFilterCriteria.getReportName());
			rs = stmt.executeQuery();
			if (rs.next()) {
				maxVal = rs.getString("MAXVAL");
			}
		} catch (Exception e) {
			logger.error("getMaxReportId(ReportFilterCriteria, Connection)", e);

			logger.error(e.getMessage());
		} finally {
			DBUtil.close(rs, stmt);

			// DatabaseUtilityDao.close(con);
		}
		return maxVal;
	}
}
