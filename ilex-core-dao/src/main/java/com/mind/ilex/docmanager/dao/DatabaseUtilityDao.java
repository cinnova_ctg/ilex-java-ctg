//
//
//
//@ Project : DocM - Document Management System - Ilex
//@ File Name : EntityDescriptionDao.java
//@ Date : 12-June-2008
//@ Author : Vishal Kapoor
//
//
package com.mind.ilex.docmanager.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.JobSetUpDTO;
import com.mind.common.EnvironmentSelector;
import com.mind.common.Util;
import com.mind.common.dao.Menu;
import com.mind.common.dao.ViewList;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.newjobdb.dao.TicketDAO;

public class DatabaseUtilityDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DatabaseUtilityDao.class);

	private static ResourceBundle bundle = ResourceBundle
			.getBundle("com.mind.properties.ApplicationResourcesDocM");

	/**
	 * @name getKeyValue
	 * @purpose This method is used to get the value from the
	 *          ApplicationResourcesDocM.
	 * @steps 1. Call to this method will return with the value corresponding to
	 *        the passed key.
	 * @param String
	 * @return String
	 * @returnDescription The value available to the given key in the
	 *                    ApplicationResources otherwise blank.
	 */
	public static String getKeyValue(String key) {
		String value = "";

		if (key != null && !"".equals(key.trim())) {
			key = key.trim();
		} else {
			key = "key.undefined";
		}

		if (bundle != null && bundle.containsKey(key)) {
			value = bundle.getString(key);
		} else {
			value = key;
		}

		return value;
	}

	public static String getKeyVariableValue(String key) {
		String value = "";
		value = EnvironmentSelector.getBundleString(key);
		return value;
	}

	/**
	 * @name getKeyValueDefaultPropertyFile
	 * @purpose This method is used to get the value from the
	 *          ApplicationResources.
	 * @steps 1. Call to this method will return with the value corresponding to
	 *        the passed key.
	 * @param String
	 * @return String
	 * @returnDescription The value available to the given key in the
	 *                    ApplicationResources otherwise blank.
	 */
	public static String getKeyValueDefaultPropertyFile(String key) {
		String value = "";
		ResourceBundle bundle = ResourceBundle
				.getBundle("com.mind.properties.ApplicationResources");
		if (key != null && !key.equals("")) {
			value = bundle.getString(key);
		}
		return value;
	}

	public static void getJobInformation(String jobId, JobSetUpDTO bean,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select lm_js_id,lm_js_title,lm_js_pr_id,lx_pr_title,"
					+ "lx_pr_mm_id,lo_ot_name from lm_job"
					+ " inner join lx_appendix_main on lm_js_pr_id = lx_pr_id"
					+ " inner join lp_msa_main on lp_mm_id = lx_pr_mm_id"
					+ " inner join lo_organization_top on lp_mm_ot_id = lo_ot_id"
					+ " where lm_js_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				bean.setJobId(rs.getString("lm_js_id"));
				bean.setJobName(rs.getString("lm_js_title"));
				bean.setAppendixId(rs.getString("lm_js_pr_id"));
				bean.setAppendixName(rs.getString("lx_pr_title"));
				bean.setMsaId(rs.getString("lx_pr_mm_id"));
				bean.setMsaName(rs.getString("lo_ot_name"));
			}
		} catch (Exception e) {
			logger.error("getJobInformation(String, JobSetUpBean, DataSource)",
					e);

			logger.error("getJobInformation(String, JobSetUpBean, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

	public static void getAppendixInformation(String appendixId,
			JobSetUpDTO bean, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select lam.lx_pr_id,lam.lx_pr_title,lmn.lp_mm_id,lot.lo_ot_name from lp_msa_main lmn join lo_organization_top lot on"
					+ " \n lp_mm_ot_id = lo_ot_id "
					+ " \n join lx_appendix_main lam on"
					+ " \n lmn.lp_mm_id = lam.lx_pr_mm_id"
					+ " \n where lx_pr_id = ?";

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, appendixId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				bean.setAppendixId(rs.getString("lx_pr_id"));
				bean.setAppendixName(rs.getString("lx_pr_title"));
				bean.setMsaId(rs.getString("lp_mm_id"));
				bean.setMsaName(rs.getString("lo_ot_name"));
			}
		} catch (Exception e) {
			logger.error(
					"getAppendixInformation(String, JobSetUpBean, DataSource)",
					e);

			logger.error(
					"getAppendixInformation(String, JobSetUpBean, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

	public static Map<String, Object> setMenu(String jobId, String loginuserid,
			DataSource ds) {

		java.util.Date dateIn = new java.util.Date();
		Map<String, Object> map = new HashMap<String, Object>();

		String jobName = Activitydao.getJobname(ds, jobId);
		String jobViewtype = JobDashboarddao.getJobViewType(jobId, ds);
		String appendixName = "";
		String msaId = "";
		String msaName = "";
		String appendixType = "";
		String projectStatus = "";
		String jobType = "";
		String appendixId = "";
		if (jobId != null && !jobId.equals("")) {

			jobType = JobSetUpDao.getjobType(jobId, ds);
			appendixId = IMdao.getAppendixId(jobId, ds);
			appendixName = Jobdao.getAppendixname(ds, appendixId);
			msaId = IMdao.getMSAId(appendixId, ds);
			msaName = com.mind.dao.PM.Appendixdao.getMsaname(ds, msaId);
			appendixType = Appendixdao.getAppendixPrType(msaId, appendixId, ds);
			projectStatus = Appendixdao.getAppendixcurrentstate(ds, appendixId);
			if (getProjectType(appendixId, ds).equalsIgnoreCase("NetMedX")) {
				map.put("isTicket", "Ticket");
				if (jobId != null && !jobId.equals("")) {
					if (TicketDAO.isOnSite(Integer.parseInt(jobId), ds)) {
						map.put("onsite", "onsite");
					}
					if (TicketDAO
							.isCancelledTicket(Integer.parseInt(jobId), ds)) {
						map.put("alreadyCancelled", "alreadyCancelled");
					}
				}
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("setMenu(String, HttpServletRequest, String, DataSource) - job type"
					+ jobType + " appendix ttype " + appendixType);
		}

		if (jobType.equalsIgnoreCase("Addendum")) {
			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%", jobId,
					"%", "%", ds);
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			String list = Menu.getStatus("Addendum", jobStatus.charAt(0), "",
					appendixId, jobId, "", loginuserid, ds);
			map.put("addendumStatusList", list);
			String list_new = Menu.getStatus("Addendum_New",
					jobStatus.charAt(0), "", appendixId, jobId, "",
					loginuserid, ds);
			map.put("addendumStatusListNew", list_new);
			map.put("Appendix_Id", appendixId);
			map.put("Job_Id", jobId);
			map.put("job_Status", jobStatus);
			map.put("jobViewType", jobViewtype);
			map.put("jobStatus", jobStatus);

		} else if (jobType.equalsIgnoreCase("inscopejob")
				|| (jobType.equalsIgnoreCase("Newjob") && (projectStatus
						.equals("C")
						|| projectStatus.equals("F")
						|| projectStatus.equals("I")
						|| projectStatus.equals("H") || projectStatus
						.equals("O")))) {
			String[] jobStatusAndType = Jobdao.getProjectJobStatusAndType(
					jobId, ds);
			String jobStatus = jobStatusAndType[0];
			String list = Menu.getStatus("prj_job", jobStatus.charAt(0), "",
					appendixId, jobId, "", loginuserid, ds);
			map.put("prjJobStatusList", list);
			String listPrjMenuNew = Menu.getStatus("prj_jobMenuNew", jobStatus.charAt(0), "",
					appendixId, jobId, "", loginuserid, ds);
			map.put("prjJobStatusListMenuNew", listPrjMenuNew);
			map.put("Appendix_Id", appendixId);
			map.put("Job_Id", jobId);
			map.put("appendixType", appendixType);
			map.put("jobStatus", jobStatus);
		} else if (jobType.trim().equalsIgnoreCase("Default")) {
			map.put("Appendix_Id", appendixId);
			map.put("Job_Id", jobId);
			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%", jobId,
					"%", "%", ds);
			String jobStatus = jobStatusAndType[0];
			map.put("jobStatus", jobStatus);
			map.put("appendixType", appendixType);
		} else if (jobType.trim().equalsIgnoreCase("Newjob")) {

			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%", jobId,
					"%", "%", ds);
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			String jobStatusList = "";

			if (jobType1.equals("Default"))
				jobStatusList = "\"Draft\"" + "," + "\"#?Type=Job&Job_Id="
						+ jobId + "&Status='D'\"" + "," + "0";
			if (jobType1.equals("Newjob"))
				jobStatusList = Menu.getStatus("pm_job", jobStatus.charAt(0),
						"", "", jobId, "", loginuserid, ds);
			if (jobType1.equals("inscopejob") && jobStatus.equals("Draft"))
				jobStatusList = "\"Approved\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ jobId + "&Status=A\"" + "," + "0";
			if (jobType1.equals("inscopejob") && jobStatus.equals("Approved"))
				jobStatusList = "\"Draft\""
						+ ","
						+ "\"MenuFunctionChangeStatusAction.do?Type=Job&ref=inscopejob&Job_Id="
						+ jobId + "&Status=D\"" + "," + "0";
			if (jobType1.equals("Addendum"))
				jobStatusList = Menu.getStatus("Addendum", jobStatus.charAt(0),
						"", "", jobId, "", loginuserid, ds);

			appendixType = ViewList.getAppendixtypedesc(appendixId, ds);
			if (logger.isDebugEnabled()) {
				logger.debug("setMenu(String, HttpServletRequest, String, DataSource) - apndxrtype "
						+ appendixType);
			}
			map.put("jobStatus", jobStatus);
			map.put("job_type", jobType1);
			map.put("Job_Id", jobId);
			map.put("appendixType", appendixType);
			map.put("Appendix_Id", appendixId);
			map.put("jobStatusList", jobStatusList);
			map.put("addendum_id", "0");

			if (jobType1.equals("Newjob")) {
				map.put("chkadd", "detailjob");
				map.put("chkaddendum", "detailjob");
				map.put("chkaddendumactivity", "View");
			}
			if (jobType1.equals("inscopejob") || jobType1.equals("addendum")) {
				map.put("chkadd", "inscopejob");
				map.put("chkaddendum", "inscopejob");
				map.put("chkaddendumactivity", "inscopeactivity");
			}

		}
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(appendixId,
				ds);
		if (appendixcurrentstatus != null
				&& (appendixcurrentstatus.trim().equalsIgnoreCase("O")
						|| appendixcurrentstatus.trim().equalsIgnoreCase("F")
						|| appendixcurrentstatus.trim().equalsIgnoreCase("H") || appendixcurrentstatus
						.trim().equalsIgnoreCase("I")))
			map.put("appendixStatus", "prjAppendix");
		else
			map.put("appendixStatus", "pmAppendix");
		if (logger.isDebugEnabled()) {
			logger.debug("setMenu(String, HttpServletRequest, String, DataSource) - appendix status "
					+ appendixcurrentstatus);
		}
		map.put("Job_Id", jobId);
		map.put("jobType", jobType);
		map.put("jobName", jobName);
		map.put("jobViewtype", jobViewtype);
		map.put("appendixId", appendixId);
		map.put("appendixName", appendixName);
		map.put("msaId", msaId);
		map.put("MSA_Id", msaId);
		map.put("msaName", msaName);
		map.put("appendixType", appendixType);

		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("setMenu(String, HttpServletRequest, String, DataSource) - **** setMenu :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return map;
	}

	public static Map<String, Object> setAppendixMenu(String appendixId,
			DataSource ds) {
		Map<String, Object> map = new HashMap<String, Object>();

		String appendixName = Jobdao.getAppendixname(ds, appendixId);
		String msaId = IMdao.getMSAId(appendixId, ds);
		String msaName = com.mind.dao.PM.Appendixdao.getMsaname(ds, msaId);
		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(appendixId, ds);
		map.put("contractDocMenu", contractDocoumentList);
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(appendixId,
				ds);
		map.put("appendixcurrentstatus", appendixcurrentstatus);
		map.put("appendixid", appendixId);
		map.put("msaId", msaId);
		map.put("viewjobtype", "%");
		map.put("ownerId", "%");
		map.put("appendixName", appendixName);
		map.put("msaId", msaId);
		map.put("msaName", msaName);
		return map;
	}

	public static String getLoOtId(long msaId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String lo_ot_id = "";
		try {
			conn = ds.getConnection();
			String sql = "select dbo.func_get_lo_ot_id(?,'msa')";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, msaId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				lo_ot_id = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("getLoOtId(long, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getLoOtId(long, DataSource) - com.mind.ilex.docmanager.dao.getLoOtId()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return lo_ot_id;
	}

	/**
	 * get type is NetMedXType or not.
	 * 
	 * @param msaId
	 * @param ds
	 * @return
	 */
	public static String getProjectType(String appendixId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String type = "";
		logger.debug("Check for Integer.ParseInt(appendixId) Job DatabaseUtilityDao.getProjectType() 66666 =>"
				+ appendixId);
		int appendixIdIntVal = Util.fixNumeric(appendixId);
		logger.debug("Check for Integer.ParseInt(appendixId) Job DatabaseUtilityDao.getProjectType() 7777 =>"
				+ appendixIdIntVal);

		try {
			conn = ds.getConnection();

			String sql = "select lx_pr_type = isnull(lx_pr_type,'0') from lx_appendix_main where lx_pr_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, appendixIdIntVal);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				if (rs.getString("lx_pr_type") != null
						&& rs.getString("lx_pr_type").equals("3")) {
					type = "NetMedX";
				} else {
					type = "Appendix";
				}

			}
		} catch (Exception e) {

			logger.error("getProjectType(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getProjectType(String, DataSource) - com.mind.ilex.docmanager.dao.getProjectType()"
						+ e);
			}
			logger.error("getProjectType(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return type;
	}

	public static Map<String, Object> setNetMedXMenu(String appendixId,
			DataSource ds) {
		Map<String, Object> map = new HashMap<String, Object>();
		java.util.Date dateIn = new java.util.Date();

		String appendixName = Jobdao.getAppendixname(ds, appendixId);
		String msaId = IMdao.getMSAId(appendixId, ds);
		String msaName = com.mind.dao.PM.Appendixdao.getMsaname(ds, msaId);
		if (getProjectType(appendixId, ds).equalsIgnoreCase("NetMedX")) {
			map.put("isTicket", "Ticket");
		}
		map.put("appendixid", appendixId);
		map.put("nettype", "1");
		map.put("msaId", msaId);
		map.put("viewjobtype", "%");
		map.put("ownerId", "%");
		map.put("msaName", msaName);
		map.put("appendixName", appendixName);
		// request.setAttribute("viewjobtype", netmedxForm.getViewjobtype());
		// request.setAttribute("ownerId", netmedxForm.getOwnerId());
		// request.setAttribute("loginUserId",
		// request.getSession(false).getAttribute("userid"));

		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("setNetMedXMenu(String, HttpServletRequest, DataSource) - **** setNetMedXMenu :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return map;
	}

	public static HashMap setPMMenu(String appendixId, String loginuserid,
			DataSource ds) {
		HashMap pmMenu = new HashMap();

		String msaId = IMdao.getMSAId(appendixId, ds);
		String msaName = Appendixdao.getMsaname(ds, msaId);
		String appendixStatus = Appendixdao.getAppendixStatus(appendixId,
				msaId, ds);
		String addendumlist = Appendixdao.getAddendumsIdName(appendixId, ds);
		String addendumid = com.mind.dao.PM.Addendumdao.getLatestAddendumId(
				appendixId, ds);
		String list = Menu.getStatus("pm_appendix", appendixStatus.charAt(0),
				msaId, appendixId, "", "", loginuserid, ds);

		pmMenu.put("appendixStatusList", list);
		pmMenu.put("Appendix_Id", appendixId);
		pmMenu.put("MSA_Id", msaId);
		pmMenu.put("appendixStatus", appendixStatus);
		pmMenu.put("latestaddendumid", addendumid);
		pmMenu.put("addendumlist", addendumlist);
		pmMenu.put("msaName", msaName);
		return pmMenu;

	}
}
