//
//
//
//@ Project : DocM - Document Management System - Ilex
//@ File Name : EntityDescriptionDao.java
//@ Date : 12-June-2008
//@ Author : Vishal Kapoor
//
//
package com.mind.ilex.docmanager.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.docm.EntitySupplementDocs;
import com.mind.bean.docm.Field;
import com.mind.bean.docm.IlexEntity;
import com.mind.common.IlexConstants;
import com.mind.docm.client.ClientOperator;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.docm.dao.Appendix;
import com.mind.docm.dao.Deliverables;
import com.mind.docm.dao.Job;
import com.mind.docm.dao.MSA;
import com.mind.docm.dao.PO;
import com.mind.docm.dao.Supplement;
import com.mind.docm.dao.TransferDocDao;
import com.mind.docm.dao.WO;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.DocumentIdNotFoundException;
import com.mind.docm.exceptions.DocumentIdNotUpdatedException;
import com.mind.docm.exceptions.DocumentNotExistsException;
import com.mind.docm.exceptions.EntityDetailsNotFoundException;
import com.mind.docm.exceptions.EntityFileNameNotFoundException;
import com.mind.docm.exceptions.LibraryIdNotFoundException;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.reports.reportDao.Report;

public class EntityDescriptionDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(EntityDescriptionDao.class);

	/**
	 * @name getEntityDetails
	 * @purpose This method is used to the get the details for the specific
	 *          entity from the Ilex System.
	 * @steps 1. User requests to get the entityDetails initiated by
	 *        EntityManager. 2. In turns command the Database SP to get the
	 *        details.
	 * @param entityId
	 * @param entityType
	 * @return IlexEntity
	 * @returnDescription IlexEntity Object filled with details available from
	 *                    the IlexSystem
	 */
	public static IlexEntity getEntityDetails(String entityId,
			String entityType, DataSource ds)
			throws EntityDetailsNotFoundException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.dao.EntityManager | Method : getEntityDetails)");
		logger.trace("This method is used to the get the details for the specific entity from the Ilex System.");
		IlexEntity ilexEntity = null; // New IlexEntity Object to be filled with
		// Database values for the specific
		// entity
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String typeOfJob = null;
		if (entityType != null && entityId != null) {
			try {
				// System.out.println("entityId /:: "+entityId);
				// System.out.println("entityType /:: "+entityType);
				conn = ds.getConnection();
				cstmt = conn
						.prepareCall("{call dbo.docm_entity_description(?, ?)}");
				cstmt.setString(1, entityId);
				cstmt.setString(2, entityType);
				rs = cstmt.executeQuery();
				if (rs.next()) {
					ilexEntity = new IlexEntity();
					ilexEntity.setEntityId(rs.getString("ilex_entity_id"));
					ilexEntity.setEntityType(rs.getString("ilex_entity_type"));
					ilexEntity.setEntityName(rs.getString("ilex_entity_name"));
					ilexEntity.setEntityIdentifier(rs
							.getString("ilex_entity_identifier"));
					ilexEntity.setEntityCreatedBy(rs
							.getString("ilex_entity_created_by"));
					ilexEntity.setEntityCreatedDate(rs
							.getString("ilex_entity_created_date"));
					ilexEntity.setEntityChangeBy(rs
							.getString("ilex_entity_changed_by"));
					ilexEntity.setEntityChangeDate(rs
							.getString("ilex_entity_changed_date"));
					ilexEntity.setEntityBaseDocument(rs
							.getString("ilex_entity_base_document"));

					ilexEntity.setEntityMSAId(rs.getString("ilex_msa_id"));
					ilexEntity.setEntityAppendixId(rs
							.getString("ilex_appendix_id"));
					ilexEntity.setEntityJobId(rs.getString("ilex_job_id"));
					ilexEntity.setEntityPoId(rs.getString("ilex_po_id"));
					ilexEntity.setEntityWoId(rs.getString("ilex_wo_id"));

					ilexEntity.setEntityNetMedXReportId(rs
							.getString("ilex_dtl_nedx_id"));
					ilexEntity.setReportFltStartDate(rs
							.getString("ilex_dtl_nedx_flt_start_date")); // properties
					// for
					// report
					// entity
					ilexEntity.setReportFltEndDate(rs
							.getString("ilex_dtl_nedx_flt_end_date"));
					ilexEntity.setReportFltCriticality(rs
							.getString("ilex_dtl_nedx_flt_criticality"));
					ilexEntity.setReportFltReportStatus(rs
							.getString("ilex_dtl_nedx_flt_status"));
					ilexEntity.setReportFltRequestor(rs
							.getString("ilex_dtl_nedx_flt_requestor"));
					ilexEntity.setReportFltArrivalOptions(rs
							.getString("ilex_dtl_nedx_flt_arr_oprions"));
					ilexEntity.setReportFltMsp(rs
							.getString("ilex_dtl_nedx_flt_msp"));
					ilexEntity.setReportFltHelpDesk(rs
							.getString("ilex_dtl_nedx_flt_helpdesk"));

					ilexEntity.setMsaName(rs.getString("ilex_msa_name"));
					ilexEntity.setAppendixName(rs
							.getString("ilex_appendix_name"));
					ilexEntity.setJobName(rs.getString("ilex_job_name"));
					ilexEntity
							.setPartnerName(rs.getString("ilex_partner_name"));
					// ilexEntity.setWoPartnerId(rs.getString("ilex_wo_partner_id"));
					// ilexEntity.setDtlNedxIdName(rs.getString("dtl_nedx_id_name"));
				}
			} catch (Exception e) {
				logger.error("getEntityDetails(String, String)", e);

				logger.error(e.getMessage());
			} finally {
				try {
					DBUtil.close(rs, cstmt);
					DBUtil.close(conn);

				} catch (Exception e) {
					logger.error("getEntityDetails(String, String)", e);

					logger.error(e.getMessage());
				}
			}
		}

		if (ilexEntity == null)
			throw new EntityDetailsNotFoundException(
					DatabaseUtilityDao
							.getKeyValue("exception.entitydetailsnotfound"));
		logger.trace("EntityDetails :" + ilexEntity.toString());
		if (ilexEntity.getEntityType() != null
				&& ilexEntity.getEntityType().trim()
						.equalsIgnoreCase("Deliverables")
				&& ilexEntity.getEntityJobId() != null) {
			typeOfJob = TransferDocDao.isDefaultJobOrAddendum(
					ilexEntity.getEntityJobId(), ds);
			if (typeOfJob != null
					&& typeOfJob.trim().equalsIgnoreCase("Default")) {
				ilexEntity.setEntityType("Appendix Supporting Documents");
				ilexEntity.setEntityName("Support - " + " " + "Default");
			} else if (typeOfJob != null
					&& typeOfJob.trim().equalsIgnoreCase("Addendum")) {
				ilexEntity.setEntityType("Appendix Supporting Documents");
				ilexEntity.setEntityName("Support - " + " " + "Addendum");
			}
		}
		return ilexEntity;
	}

	public static IlexEntity getEntityDetails(String entityId, String entityType)
			throws EntityDetailsNotFoundException {
		return getEntityDetails(entityId, entityType,
				DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME));
	}

	public static String getDbCurrentDate() {
		return getDbCurrentDate(DBUtil
				.getDataSource(IlexConstants.ILEX_DS_NAME));
	}

	public static String getDbCurrentDate(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String dbCurrentDate = "";
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select currentdate = getdate()";
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				dbCurrentDate = rs.getString(1);
			}
		} catch (Exception e) {
			System.out.println("Error getting Database System date: " + e);
			logger.error(e.getMessage());
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
		if (dbCurrentDate.length() < 22) {
			dbCurrentDate = dbCurrentDate + "00";
		}
		return dbCurrentDate;
	}

	/**
	 * @name setEntityCorrespondingDocumentId
	 * @purpose This method is used to the set the documentId returned from the
	 *          DocM into the Ilex enitity specific database record.
	 * @steps 1. EntityManager initialtes the call to the method for updation
	 *        into the database. 2. In turns command the Database SP to set the
	 *        details.
	 * @param entityId
	 * @param entityType
	 * @param documentId
	 * @return
	 * @returnDescription
	 */

	public static void setEntityCorrespondingDocumentId(String entityId,
			String entityType, String documentId)
			throws DocumentIdNotUpdatedException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.dao.EntityManager | Method : setEntityCorrespondingDocumentId)");
		logger.trace("This method is used to the set the documentId returned from the DocM into the Ilex enitity specific database record.");
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int retval = 0;
		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();
			retval = stmt.executeUpdate(returnSqlForDocIdInsertion(entityId,
					entityType, documentId));
		} catch (Exception e) {
			logger.error(
					"setEntityCorrespondingDocumentId(String, String, String)",
					e);

			logger.error(e.getMessage());
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error(
						"setEntityCorrespondingDocumentId(String, String, String)",
						e);

				logger.error(e.getMessage());
			}
		}
		if (retval == 0)
			throw new DocumentIdNotUpdatedException(
					DatabaseUtilityDao.getKeyValue("exception.docidnotupdated"));
		logger.trace("End :(Class : com.mind.ilex.docmanager.dao.EntityManager | Method : setEntityCorrespondingDocumentId)");
	}

	/**
	 * @name getEntityCorrespondingDocumentId
	 * @purpose This method is used to the get the documentId from the Ilex
	 *          respected to the entity.
	 * @steps 1. EntityManager initialtes the call to the method for retrieval
	 *        of DocumentId. 2. In turns command the Database SP to get the
	 *        details.
	 * @param entityId
	 * @param entityType
	 * @return String
	 * @returnDescription The doucmentId available in the IlexSystem
	 *                    corresponding to the entity.
	 */
	public static String getEntityCorrespondingDocumentId(String entityId,
			String entityType) throws DocumentIdNotFoundException {
		return getEntityCorrespondingDocumentId(entityId, entityType,
				DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME));
	}

	public static String getEntityCorrespondingDocumentId(String entityId,
			String entityType, DataSource ds)
			throws DocumentIdNotFoundException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.dao.EntityManager | Method : getEntityCorrespondingDocumentId)");
		logger.trace("This method is used to the get the documentId from the Ilex respected to the entity.");
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String documentId = null;
		String sql = "";
		try {
			conn = ds.getConnection();
			sql = getSqlForDocIdInsertion(entityId, entityType);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			logger.trace(sql);
			if (rs.next()) {
				documentId = rs.getString(1);
				// System.out.println("----documentId in query----"+documentId);
			}
			// get the returned documentId
		} catch (Exception e) {
			logger.error("getEntityCorrespondingDocumentId(String, String)", e);

			logger.error(e.getMessage());
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error(
						"getEntityCorrespondingDocumentId(String, String)", e);

				logger.error(e.getMessage());
			}
		}
		if (documentId == null || documentId.equals("")
				|| documentId.equals("0"))
			throw new DocumentIdNotFoundException(
					DatabaseUtilityDao.getKeyValue("exception.docidnotfound"));
		logger.trace("DocumentId :" + documentId);
		logger.trace("End :(Class : com.mind.ilex.docmanager.dao.EntityManager | Method : getEntityCorrespondingDocumentId)");
		return documentId;
	}

	/**
	 * @name getEntityCorrespondingLibraryId
	 * @purpose This method is used to the get the libId from the Ilex respected
	 *          to the entity.
	 * @steps 1. EntityManager initialtes the call to the method for retrieval
	 *        of LibId. 2. In turns command the Database SP to get the details.
	 * @param entityId
	 * @param entityType
	 * @return String
	 * @returnDescription The doucmentId available in the IlexSystem
	 *                    corresponding to the entity.
	 */
	public static String getEntityCorrespondingLibraryId(String entityId,
			String entityType) throws LibraryIdNotFoundException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.dao.EntityManager | Method : getEntityCorrespondingLibraryId)");
		logger.trace("This method is used to the get the libId from the Ilex respected to the entity.");
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String libId = null;
		String sql = "";
		if (logger.isDebugEnabled()) {
			logger.debug("getEntityCorrespondingLibraryId(String, String) - Library Name is : "
					+ entityType.trim());
		}
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			sql = "select dm_lib_id from dm_library_master where dm_lib_desc = '"
					+ entityType.trim() + "'";
			if (logger.isDebugEnabled()) {
				logger.debug("getEntityCorrespondingLibraryId(String, String) - "
						+ sql);
			}
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			logger.trace(sql);
			if (rs.next())
				libId = rs.getString(1);
			// get the returned libId
		} catch (Exception e) {
			logger.error("getEntityCorrespondingLibraryId(String, String)", e);

			logger.error(e.getMessage());
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error("getEntityCorrespondingLibraryId(String, String)",
						e);

				logger.error(e.getMessage());
			}
		}
		if (libId == null || libId.equals("") || libId.equals("0"))
			throw new LibraryIdNotFoundException(
					DatabaseUtilityDao.getKeyValue("exception.libidnotfound"));
		logger.trace("Library :" + libId);
		logger.trace("End :(Class : com.mind.ilex.docmanager.dao.EntityManager | Method : getEntityCorrespondingLibraryId)");
		return libId;
	}

	/**
	 * @name getIlexToDocMMapping
	 * @purpose This method is used as a convertor which converts the IlexEntity
	 *          Object into the MetaData Property of the Document Object.
	 * @steps 1. EntityManager initialtes the call to the method for conversion
	 *        process. 2. In turns corresponds the columns available in the DocM
	 *        System for the Documents with the IlexEntity properties.
	 * @param IlexEntity
	 * @param length
	 * @return Field[]
	 * @returnDescription The array of Field[] property of the MetaData Object.
	 */

	public static Field[] getIlexToDocMMapping(IlexEntity ilexEntity,
			long length, String fileName) {
		logger.trace("(Class : com.mind.ilex.docmanager.EntityDescriptionDao | Method : getIlexToDocMMapping)");
		logger.trace("This method is used to do operations on DocM system/Ilex system related to approval request for the entity from Ilex.");

		if (ilexEntity.getEntityType().equals(
				DatabaseUtilityDao.getKeyValue("ilex.docm.entity.msa"))) {
			return MSA.getIlexToDocMMapping(ilexEntity, length, fileName);
		} else if (ilexEntity.getEntityType().equals(
				DatabaseUtilityDao.getKeyValue("ilex.docm.entity.appendix"))) {
			return Appendix.getIlexToDocMMapping(ilexEntity, length, fileName);
		} else if (ilexEntity.getEntityType().equals(
				DatabaseUtilityDao.getKeyValue("ilex.docm.entity.addendum"))) {
			return Job.getIlexToDocMMapping(ilexEntity, length, fileName);
		} else if (ilexEntity.getEntityType().equals(
				DatabaseUtilityDao.getKeyValue("ilex.docm.entity.po"))) {
			return PO.getIlexToDocMMapping(ilexEntity, length, fileName);
		} else if (ilexEntity.getEntityType().equals(
				DatabaseUtilityDao.getKeyValue("ilex.docm.entity.wo"))) {
			return WO.getIlexToDocMMapping(ilexEntity, length, fileName);
		} else if (ilexEntity.getEntityType().equals(
				DatabaseUtilityDao
						.getKeyValue("ilex.docm.entity.netmedx.report"))) {
			return Report.getIlexToDocMMapping(ilexEntity, length, fileName);
		} else if (ilexEntity.getEntityType()
				.equals(DatabaseUtilityDao
						.getKeyValue("ilex.docm.entity.deliverables"))) {
			return Deliverables.getIlexToDocMMapping(ilexEntity, length,
					fileName);
		} else
			return null;
	}

	public static Field[] getIlexToDocMMapping(IlexEntity ilexEntity,
			long length) {
		return getIlexToDocMMapping(ilexEntity, length, null);
	}

	/**
	 * Method : this method is used to get the details filled for Supplement
	 * Entity
	 * 
	 * @param ilexEntity
	 * @return Field[]
	 */
	public static Field[] getIlexToDocMMapping(IlexEntity ilexEntity) {
		return Supplement.getIlexToDocMMapping(ilexEntity);
	}

	/**
	 * @name getEntityFileName
	 * @purpose This method is used to the return the entityFileName.
	 * @steps 1. EntityDescriptionDao initialtes the call to the method for
	 *        retrieval of FileName.
	 * @param entityId
	 * @param entityType
	 * @return String
	 * @returnDescription The name of the file for the Ilex Entity.
	 */
	public static String getEntityFileName(String entityId, String entityType)
			throws EntityFileNameNotFoundException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.dao.EntityManager | Method : getEntityFileName)");
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select isnull(lx_pr_type,0) from lx_appendix_main where lx_pr_id  ="
				+ entityId;
		String entityFileName = null;
		String rsResult = null;

		if (!entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.appendix"))) {
			if (entityType.equals(DatabaseUtilityDao
					.getKeyValue("ilex.docm.entity.msa")))
				entityFileName = entityType
						+ DatabaseUtilityDao
								.getKeyValue("helping.text.filename.standard")
						+ entityId
						+ DatabaseUtilityDao
								.getKeyValue("helping.text.filename.pdf");
			else {
				if (entityType
						.trim()
						.equalsIgnoreCase(
								DatabaseUtilityDao
										.getKeyValue("ilex.docm.entity.netmedx.report")))
					entityFileName = DatabaseUtilityDao
							.getKeyValue("helping.text.filename.report")
							+ ""
							+ entityId
							+ DatabaseUtilityDao
									.getKeyValue("helping.text.filename.pdf");
				else
					entityFileName = entityType
							+ ""
							+ entityId
							+ DatabaseUtilityDao
									.getKeyValue("helping.text.filename.pdf");
			}
		} else {
			try {
				conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
						.getConnection();
				stmt = conn.createStatement();
				logger.trace(sql);
				rs = stmt.executeQuery(sql);
				if (rs.next()) {
					rsResult = rs.getString(1);
					if (rsResult.equals("3"))
						entityFileName = DatabaseUtilityDao
								.getKeyValue("helping.text.filename.netdmedx")
								+ entityId
								+ DatabaseUtilityDao
										.getKeyValue("helping.text.filename.pdf");
					else if (rsResult.equals("4"))
						entityFileName = DatabaseUtilityDao
								.getKeyValue("helping.text.filename.appendix.hardware")
								+ entityId
								+ DatabaseUtilityDao
										.getKeyValue("helping.text.filename.pdf");
					else
						entityFileName = DatabaseUtilityDao
								.getKeyValue("helping.text.filename.appendix.standard")
								+ entityId
								+ DatabaseUtilityDao
										.getKeyValue("helping.text.filename.pdf");
				}
			} catch (Exception e) {
				logger.error("getEntityFileName(String, String)", e);

				logger.error(e.getMessage());
			} finally {
				try {
					DBUtil.close(rs, stmt);
					DBUtil.close(conn); // Close
					// database
					// conneciton
					// object
				} catch (Exception e) {
					logger.error("getEntityFileName(String, String)", e);

					logger.error(e.getMessage());
				}
			}
		}

		if (entityFileName == null || entityFileName.equals(""))
			throw new EntityFileNameNotFoundException(
					DatabaseUtilityDao
							.getKeyValue("exception.entitynamenotfound"));
		logger.trace("EntityFileName : " + entityFileName);
		logger.trace("End :(Class : com.mind.ilex.docmanager.dao.EntityManager | Method : getEntityFileName)");
		return entityFileName;
	}

	/**
	 * @name returnSqlForDocIdRetrieval
	 * @purpose This method is used to the return the sql to get the documentId
	 *          from the Ilex respected to the entity.
	 * @steps 1. EntityManager initialtes the call to the method for retrieval
	 *        of DocumentId.
	 * @param entityId
	 * @param entityType
	 * @return String
	 * @returnDescription The sql specific to entities.
	 */
	public static String getSqlForDocIdInsertion(String enitityId,
			String entityType) {
		String sql = "";
		if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.msa"))) {
			sql = "select isnull(lp_mm_doc_id,'0') from lp_msa_main where lp_mm_id ="
					+ enitityId;
		} else if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.appendix"))) {
			sql = "select isnull(lx_pr_doc_id,'0') from lx_appendix_main where lx_pr_id ="
					+ enitityId;
		} else if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.addendum"))) {
			sql = "select isnull(lm_js_doc_id,'0') from lm_job where lm_js_id ="
					+ enitityId;
		} else if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.po"))) {
			sql = "select  isnull(lm_po_doc_id,'0') from lm_purchase_order where lm_po_id ="
					+ enitityId;
		} else if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.wo"))) {
			sql = "select isnull(lm_po_wo_doc_id,'0') from lm_purchase_order where lm_po_id ="
					+ enitityId;
		} else if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.netmedx.report"))) {
			sql = "select isnull(document_id,'0') from report_schedule_detail where entity_id = ='"
					+ enitityId + "'";
		} else if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.deliverables"))) {
			// sql =
			// "select podb_mn_dl_doc_id = replace(isnull(podb_mn_dl_doc_id ,'-1'),0,-1) from podb_purchase_order_deliverables where podb_mn_dl_id = '"+enitityId
			// + "'";
			sql = "select podb_mn_dl_doc_id = case when isnull(podb_mn_dl_doc_id ,'-1') = 0 then -1  "
					+ "else isnull(podb_mn_dl_doc_id ,'-1') end from podb_purchase_order_deliverables "
					+ "where podb_mn_dl_id = '" + enitityId + "'";
		}
		return sql;

	}

	/**
	 * @name returnSqlForDocIdInsertion
	 * @purpose This method is used to the return the sql to set the documentId
	 *          respected to the entity.
	 * @steps 1. EntityManager initialtes the call to the method for insertion
	 *        of DocumentId.
	 * @param entityId
	 * @param entityType
	 * @return String
	 * @returnDescription The sql specific to entities.
	 */
	public static String returnSqlForDocIdInsertion(String enitityId,
			String entityType, String docId) {
		String sql = "";
		if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.msa"))) {
			sql = "update lp_msa_main set lp_mm_doc_id =" + docId
					+ " where lp_mm_id =" + enitityId;
		} else if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.appendix"))) {
			sql = "update lx_appendix_main set lx_pr_doc_id =" + docId
					+ " where lx_pr_id =" + enitityId;
		} else if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.addendum"))) {
			sql = "update lm_job set lm_js_doc_id =" + docId
					+ " where lm_js_id =" + enitityId;
		} else if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.po"))) {
			sql = "update lm_purchase_order set lm_po_doc_id =" + docId
					+ " where lm_po_id =" + enitityId;
		} else if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.wo"))) {
			sql = "update lm_purchase_order set lm_po_wo_doc_id =" + docId
					+ " where lm_po_id =" + enitityId;
		} else if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.netmedx.report"))) {
			sql = "update report_schedule_detail set document_id =" + docId
					+ " where entity_id ='" + enitityId + "'";
		} else if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.libraryname.msasupportingdocuments"))
				|| entityType
						.equals(DatabaseUtilityDao
								.getKeyValue("ilex.docm.libraryname.appendixsupportingdocuments"))
				|| entityType.equals(DatabaseUtilityDao
						.getKeyValue("ilex.docm.libraryname.deliverables"))) {
			sql = "update lx_upload_file set lx_file_doc_id = " + docId
					+ " where lx_file_id ='" + enitityId + "'";
		} else if (entityType.equals(DatabaseUtilityDao
				.getKeyValue("ilex.docm.entity.deliverables"))) {
			sql = "update podb_purchase_order_deliverables set podb_mn_dl_doc_id ="
					+ docId + " where entity_id ='" + enitityId + "'";
		}
		return sql;

	}

	/**
	 * @name getFieldValueFordocument
	 * @purpose This method is used to the return the status for documentID.call
	 *          clientoperator getDOcument method.
	 * @param docId
	 * @param fieldName
	 * @return String
	 * @returnDescription return field value
	 */
	public static String getFieldValueFordocument(String docId, String fieldName) {
		String fieldValue = "";
		try {
			com.mind.bean.docm.Document doc = ClientOperator.getDocument(docId,
					"");
			fieldValue = ClientOperator.getDocFieldValue(doc, fieldName);
		} catch (CouldNotCheckDocumentException e) {
			logger.error("getFieldValueFordocument(String, String)", e);

			logger.error("getFieldValueFordocument(String, String)", e);
		} catch (DocumentNotExistsException e) {
			logger.error("getFieldValueFordocument(String, String)", e);

			logger.error("getFieldValueFordocument(String, String)", e);
		}
		return fieldValue;
	}

	/**
	 * Method : method is used to get the details of Supplements Documents
	 * available in Ilex based on fileId.
	 * 
	 * @param fileId
	 * @return EntitySupplementDocs as an Object
	 */
	public static EntitySupplementDocs getSupplementDocs(String fileId) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		EntitySupplementDocs supplement = null;
		String sql = "select * from lx_upload_file where lx_file_id  ="
				+ fileId;
		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				supplement = new EntitySupplementDocs();
				supplement.setFileId(rs.getString("lx_file_id"));
				supplement.setFileName(rs.getString("lx_file_name"));
				supplement.setFileRemarks(rs.getString("lx_file_remarks"));
				supplement.setFileData(rs.getBytes("lx_file_data"));
				supplement
						.setMimeType((rs.getString("lx_file_name").lastIndexOf(
								".") > 0) ? rs.getString("lx_file_name")
								.substring(
										rs.getString("lx_file_name")
												.lastIndexOf(".") + 1,
										rs.getString("lx_file_name").length())
								: "");
			}
		} catch (Exception e) {
			logger.error("getSupplementDocs(String)", e);

			logger.error("getSupplementDocs(String)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn); // Close database
			// conneciton object
		}
		return supplement;
	}

	public static String getMimeType(String fileName) {
		if (fileName != null) {
			return (fileName.lastIndexOf(".") > 0) ? fileName.substring(
					fileName.lastIndexOf(".") + 1, fileName.length()) : "";
		} else {
			return "PDF";
		}
	}
}
