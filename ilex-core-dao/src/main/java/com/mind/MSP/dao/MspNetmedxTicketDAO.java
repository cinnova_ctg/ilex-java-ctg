package com.mind.MSP.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.DeviceInfoBean;
import com.mind.bean.msp.TicketInformation;
import com.mind.common.LabelValue;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.msp.common.MspNetMedxTicketType;

/**
 * The Class MspNetmedxTicketDAO.
 * 
 * @author Vijay Kumar Singh
 */
public class MspNetmedxTicketDAO {

	private static final Logger logger = Logger
			.getLogger(MspNetmedxTicketDAO.class);

	/**
	 * Purpose: Gets the customer list from database.
	 * 
	 * @param DataSource
	 * 
	 * @return ArrayList<LabelValue>
	 */
	public static ArrayList<LabelValue> getCustomerList(DataSource ds) {
		if (logger.isDebugEnabled()) {
			logger.debug("getCustomerList");
		}
		ArrayList<LabelValue> customerList = new ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		customerList.add(new com.mind.common.LabelValue("Select", "0"));
		StringBuilder query = new StringBuilder();
		query.append("select * from wug_customer_list");
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				customerList.add(new LabelValue(rs.getString("msa_name")
						+ MspNetMedxTicketType.SEPARATOR2.getCode()
						+ rs.getString("appendix_name"), rs.getString("msa_id")
						+ MspNetMedxTicketType.SEPARATOR1.getCode()
						+ rs.getString("appendix_id")));
			}
		} catch (Exception e) {
			logger.error("getCustomerList(DataSource)", e);
			logger.error("ds= " + ds);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return customerList;
	}

	/**
	 * Purpose: Find site list on the basis of given site name.
	 * 
	 * @param DataSource
	 * @param String
	 *            - the site name
	 * 
	 * @return ArrayList<LabelValue>
	 */
	public static ArrayList<LabelValue> findSiteList(DataSource ds,
			String searchStr, String appendixId) {
		if (logger.isDebugEnabled()) {
			logger.debug("searchStr= " + searchStr + "\n appendixId= "
					+ appendixId);
		}
		ArrayList<LabelValue> customerList = new ArrayList<LabelValue>();
		customerList.add(new com.mind.common.LabelValue("Select", "0"));
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call wug_msp_site_search_list(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, searchStr);
			cstmt.setInt(3, Integer.valueOf(appendixId.trim()));
			rs = cstmt.executeQuery();
			while (rs.next()) {
				customerList.add(new LabelValue(rs.getString("site_name"), rs
						.getString("device_id")
						+ MspNetMedxTicketType.SEPARATOR1.getCode()
						+ rs.getString("site_id")));
			}
		} catch (Exception e) {
			logger.error("searchStr= " + searchStr + "\n appendixId= "
					+ appendixId + "\n", e);
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		if (customerList.size() == 1) {
			return null;
		}
		return customerList;
	}

	/**
	 * Purpose: Gets the device information.
	 * 
	 * @param Integer
	 *            the ticket id
	 * @param DataSource
	 *            the ds
	 * 
	 * @return DeviceInfoBean the device information
	 */
	public static DeviceInfoBean getDeviceInformation(Integer ticketId,
			DataSource ds) {
		if (logger.isDebugEnabled()) {
			logger.debug("ticketId=" + ticketId);
		}
		DeviceInfoBean deviceInfoBean = new DeviceInfoBean();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call wug_ticket_site_device_information(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, ticketId);
			rs = cstmt.executeQuery();
			if (rs.next()) {
				deviceInfoBean.setCircuitIP(rs.getString("circuit_ip"));
				deviceInfoBean.setCircuitType(rs.getString("circuit_type"));
				deviceInfoBean.setInternetServiceLineNo(rs
						.getString("internet_service_line_no"));
				deviceInfoBean.setIsp(rs.getString("isp"));
				deviceInfoBean.setIspSupportNo(rs.getString("isp_support_no"));
				deviceInfoBean.setJobId(rs.getString("job_id"));
				deviceInfoBean.setLocation(rs.getString("location"));
				deviceInfoBean.setModemType(rs.getString("modem_type"));
				deviceInfoBean
						.setSiteContactNo(rs.getString("site_contact_no"));
			}
		} catch (Exception e) {
			logger.error("ticketId=" + ticketId + "\n", e);
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return deviceInfoBean;
	}// - End of getRealTimeState(DataSource)

	/**
	 * Purpose: Gets the ticket information.
	 * 
	 * @param Integer
	 *            the ticket id
	 * @param DataSource
	 * 
	 * @return TicketInformation
	 */
	public static TicketInformation getTicketInformation(Integer ticketId,
			DataSource ds) {
		if (logger.isDebugEnabled()) {
			logger.debug("ticketId=" + ticketId);
		}
		TicketInformation ticketInformation = new TicketInformation();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call wug_msp_netmedx_ticket_information(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, ticketId);
			rs = cstmt.executeQuery();
			if (rs.next()) {
				ticketInformation
						.setCustomerName(rs.getString("customer_name"));
				ticketInformation.setAppendixId(rs.getString("appendix_id"));
				ticketInformation.setJobId(rs.getString("job_id"));
				ticketInformation.setTicketId(rs.getString("ticket_id"));
				ticketInformation
						.setTicketNumber(rs.getString("ticket_number"));
				ticketInformation.setProblemCategory(rs
						.getString("problem_category"));
				ticketInformation.setProbleDescription(rs
						.getString("problem_description"));
				ticketInformation.setEstimatedEffort(rs
						.getString("estimated_effort"));
			}
		} catch (Exception e) {
			logger.error("ticketId=" + ticketId + "\n ", e);
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		ManageOutageDao.setLatestInstallNotes(ticketInformation, ds);
		return ticketInformation;
	}// - End of getTicketInformation(Integer, DataSource)

	/**
	 * Purpose: Sets the estimated effort.
	 * 
	 * @param jobId
	 * @param estimatedEffort
	 * @param userId
	 * @param ds
	 * @return int
	 */
	public static int setEstimatedEffort(Integer jobId, Float estimatedEffort,
			Integer userId, DataSource ds) {
		if (logger.isDebugEnabled()) {
			logger.debug("jobId=" + jobId + "\n estimatedEffort"
					+ estimatedEffort + "\n userId" + userId);
		}
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int updatedFlag = -1;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call wug_set_resource_quantity(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, jobId);
			cstmt.setFloat(3, estimatedEffort);
			cstmt.setInt(4, userId);
			cstmt.execute();
			updatedFlag = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error("jobId=" + jobId + "\n estimatedEffort"
					+ estimatedEffort + "\n userId" + userId, e);
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return updatedFlag;
	}// - End of setEstimatedEffort

}
