/*
 * 
 */
package com.mind.MSP.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.TreeMap;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.MSPCCReportBean;
import com.mind.common.Util;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.msp.common.EscalationLevel;

public class MSPCCReportDao {
	/** The Constant logger. */
	public static final Logger logger = Logger
			.getLogger(SystemPerformanceDAO.class);

	/**
	 * Get the Result Set Object .
	 * 
	 * @param query
	 *            String (Sql Query)
	 * @param Statement
	 *            (Sql Statement Object))
	 * @return ResultSet
	 */
	public ResultSet getResultSet(String query, Statement stmt) {
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(query);
		} catch (SQLException e) {
			logger.error("Exception occured to get Result Set due to ", e);
		}
		return rs;
	}

	/**
	 * Get the List of MSPCCReportBean which contain the name and value of
	 * groups.
	 * 
	 * @param query
	 *            String (Sql Query)
	 * @param DataSource
	 *            Object
	 * @return ArrayList
	 */

	public static ArrayList<MSPCCReportBean> getList(String query, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<MSPCCReportBean> mspArrayList = new ArrayList<MSPCCReportBean>();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				MSPCCReportBean mspccReportBean = new MSPCCReportBean();
				mspccReportBean.setName(rs.getString(1));
				mspccReportBean.setValue(rs.getString(2));
				if (rs.getString("lr_ri_group").equals("A-1")) {
					mspccReportBean.setErrorValue(rs.getString(3));
				}
				mspArrayList.add(mspccReportBean);
			}

		} catch (Exception e) {
			logger.error(
					"Exception occured to get List from MSPCCReportDao class due to ",
					e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return mspArrayList;
	}

	public static ArrayList<MSPCCReportBean> getHeaderList(String appendixId,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<MSPCCReportBean> mspArrayList = new ArrayList<MSPCCReportBean>();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String query = "";
			query = " select lr_pc_prj_chk_item, lr_pc_prj_item_id ,isnull(cnt,0) from "
					+ " ( select lr_pc_rt_pr_id, lr_pc_prj_chk_item, lr_pc_prj_item_id "
					+ " from lr_project_to_checklist_item_map"
					+ " where lr_pc_rt_pr_id ="
					+ appendixId
					+ " and lr_pc_status = 1"
					+ " )a left outer join ("
					+ " select lr_ci_rt_pr_id, lr_ci_pc_prj_item_id,count(lr_ci_rt_pr_id) as cnt"
					+ " from lr_job_checklist_status"
					+ " where lr_ci_rt_pr_id = "
					+ appendixId
					+ " group by lr_ci_rt_pr_id,lr_ci_pc_prj_item_id"
					+ " ) b"
					+ " on lr_pc_rt_pr_id = lr_ci_rt_pr_id"
					+ " and lr_pc_prj_item_id = lr_ci_pc_prj_item_id";

			rs = stmt.executeQuery(query);
			while (rs.next()) {
				MSPCCReportBean mspccReportBean = new MSPCCReportBean();
				mspccReportBean.setName(rs.getString(1));
				mspccReportBean.setValue(rs.getString(2));
				mspccReportBean.setCount(rs.getString(3));
				mspArrayList.add(mspccReportBean);
			}

		} catch (Exception e) {
			logger.error(
					"Exception occured to get List from MSPCCReportDao class due to ",
					e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return mspArrayList;
	}

	public static MSPCCReportBean getTotalVlaue(
			MSPCCReportBean mspccReportBean, String appendixId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String query = "select sum(cast(lr_ri_reporting_value1 as int)) as completedValue, "
					+ " sum(cast(isnull(lr_ri_reporting_value2,0) as int)) as errorsValue"
					+ " from  lr_msp_activity_based_progress_indicators"
					+ " where lr_ri_rt_pr_id = "
					+ appendixId
					+ " and lr_ri_group ='A-1'";
			rs = stmt.executeQuery(query);
			if (rs.next()) {
				mspccReportBean.setTotalCompleted(rs
						.getString("completedValue"));
				mspccReportBean.setTotalErrors(rs.getString("errorsValue"));
			}

		} catch (Exception e) {
			logger.error(
					"Exception occured to get List from MSPCCReportDao class due to ",
					e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return mspccReportBean;
	}

	public static String[] getConstantValues(String query, DataSource ds) {
		String[] labelValue = new String[2];

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			if (rs.next()) {
				labelValue[0] = rs.getString("lr_ri_name");
				labelValue[1] = rs.getString("lr_ri_reporting_value1");
			}

		} catch (Exception e) {
			logger.error(
					"Exception occured to get List from MSPCCReportDao class due to ",
					e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return labelValue;
	}

	public static ArrayList<TreeMap<String, String>> getSectionBInfo(
			String query, DataSource ds) {
		ArrayList<TreeMap<String, String>> mspCCBList = new ArrayList<TreeMap<String, String>>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		int i = 0;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				TreeMap<String, String> SectionBInfo = new TreeMap<String, String>();

				SectionBInfo.put("Name",
						Util.filter(rs.getString("lr_ci_js_title")));
				SectionBInfo.put("Date", rs.getString("lr_ci_reference_date"));
				SectionBInfo.put("JobOwner", rs.getString("lr_ci_opr"));
				SectionBInfo.put("ErrorCondition",
						rs.getString("lr_ci_error_status"));
				SectionBInfo.put("Actions", EscalationLevel.CHECL_LIST_DATA);
				SectionBInfo.put("JobId", rs.getString("lr_ci_js_id"));
				mspCCBList.add(i, SectionBInfo);
				i++;
			}
		} catch (Exception e) {
			logger.error("getSectionBInfo(DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return mspCCBList;
	}

	public static boolean isConfigured(String query, DataSource ds) {
		boolean isConfigured = false;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			if (rs.next()) {
				isConfigured = true;
			}

		} catch (Exception e) {
			logger.error(
					"Exception occured to get List from MSPCCReportDao class due to ",
					e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return isConfigured;
	}

	public static ArrayList<TreeMap<String, String>> getCheckFilSectionBInfo(
			String query, String[] categoryColumns, DataSource ds) {

		ArrayList<TreeMap<String, String>> mspCCBList = new ArrayList<TreeMap<String, String>>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String prevJobId = "0";

		int i = 0;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			TreeMap<String, String> SectionBInfo = new TreeMap<String, String>();

			SectionBInfo.put("col0", "No Data");
			SectionBInfo.put("col1", "No Data");
			SectionBInfo.put("col2", "No Data");

			while (rs.next()) {
				if ((!rs.getString("lr_ci_js_id").equals(prevJobId))
						&& !prevJobId.equals("0")) {
					mspCCBList.add(i, SectionBInfo);
					i++;
					SectionBInfo = new TreeMap<String, String>();
					SectionBInfo.put("col0", "No Data");
					SectionBInfo.put("col1", "No Data");
					SectionBInfo.put("col2", "No Data");
				}

				SectionBInfo.put("Name",
						Util.filter(rs.getString("lr_ci_js_title")));
				SectionBInfo.put("Actions", EscalationLevel.CHECL_LIST_DATA);
				SectionBInfo.put("JobId", rs.getString("lr_ci_js_id"));

				for (int colNum = 0; colNum < categoryColumns.length; colNum++) {
					if (categoryColumns[colNum].equals(Util.filter(rs
							.getString("lr_jc_name")))) {
						SectionBInfo.put("col" + colNum,
								Util.filter(rs.getString("lr_jc_value")));
						break;
					}
				}

				prevJobId = rs.getString("lr_ci_js_id");
			}

			mspCCBList.add(i, SectionBInfo);

		} catch (Exception e) {
			logger.error("getSectionBInfo(DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return mspCCBList;
	}

}
