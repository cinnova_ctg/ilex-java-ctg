package com.mind.MSP.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.log4j.Logger;

import com.mind.bean.msp.ChangeRequestActionDetails;
import com.mind.bean.msp.PCIChangeControlBean;
import com.mind.common.LabelValue;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.fw.lang.SysException;

public class PCIChangeControlDAO {

	/**
	 * author-Lalit Goyal The Class PCIChangeControlDAO.
	 */

	/** The Constant logger. */
	private static final Logger LOGGER = Logger
			.getLogger(PCIChangeControlDAO.class);

	/**
	 * Gets the search list.
	 * 
	 * @param SearchHistoryBean
	 *            the history bean
	 * @param DataSource
	 *            the ds
	 * 
	 * @return ArrayList<TicketSearchBean> the search list
	 */
	public static List<LabelValue> getCustomerList(DataSource ds)
			throws SysException {
		List<LabelValue> customerList = new ArrayList<LabelValue>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BeanProcessor beanProcessor = new BeanProcessor();

		try {
			conn = ds.getConnection();

			String query = " select distinct wug_gn_msa_id, wug_gn_ilex_customer_name "
					+ " from wug_group_name where wug_gn_ilex_customer_name is not null and "
					+ " wug_gn_msa_id is not null order by wug_gn_ilex_customer_name ";

			// "select lp_mm_id ,  lo_ot_name from "
			// + " lp_msa_main inner join lp_msa_detail  on "
			// + " lp_mm_id = lp_md_mm_id  inner join lo_organization_top on"
			// +
			// " lp_mm_ot_id = lo_ot_id and lo_ot_name not in ('Master Library','NetMedX') and "
			// + " lp_md_status='S' order by lo_ot_name ";
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			customerList.add(new LabelValue("---Select---", "0"));
			while (rs.next()) {
				customerList.add(new LabelValue(rs.getString(2), rs
						.getString(1)));
			}

		} catch (SQLException e) {
			if (conn != null) {
				LOGGER.error(
						"getCustomerList(DataSource ds) SqlException Occure", e);
			}
		} catch (Exception e1) {
			LOGGER.error("getCustomerList(DataSource ds) Exception Occure", e1);
			throw new SysException(e1);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return customerList;
	}

	/**
	 * Gets the change request status list.
	 * 
	 * @param ds
	 *            the ds
	 * @return the change request status list
	 * @throws SysException
	 *             the sys exception
	 */
	public static List<LabelValue> getChangeRequestStatusList(DataSource ds)
			throws SysException {
		List<LabelValue> changeRequestStatusList = new ArrayList<LabelValue>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BeanProcessor beanProcessor = new BeanProcessor();

		try {
			conn = ds.getConnection();
			String query = "select * from pci_change_request_status_master ORDER BY 2 DESC";
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				changeRequestStatusList.add(new LabelValue(rs.getString(2), rs
						.getString(1)));
			}

		} catch (SQLException e) {
			if (conn != null) {
				LOGGER.error(
						"getChangeRequestStatusList(DataSource ds) SqlException Occure",
						e);
			}
		} catch (Exception e1) {
			LOGGER.error(
					"getChangeRequestStatusList(DataSource ds) Exception Occure",
					e1);
			throw new SysException(e1);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return changeRequestStatusList;
	}

	/**
	 * Gets the change req actions status list.
	 * 
	 * @param ds
	 *            the ds
	 * @return the change req actions status list
	 * @throws SysException
	 *             the sys exception
	 */
	public static List<LabelValue> getChangeReqActionsStatusList(DataSource ds)
			throws SysException {
		List<LabelValue> changeReqActionsList = new ArrayList<LabelValue>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BeanProcessor beanProcessor = new BeanProcessor();

		try {
			conn = ds.getConnection();
			String query = "select * from dbo.pci_change_request_action_status_master ORDER BY 2 DESC";
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				changeReqActionsList.add(new LabelValue(rs.getString(2), rs
						.getString(1)));
			}

		} catch (SQLException e) {
			if (conn != null) {
				LOGGER.error(
						"getChangeReqActionsStatusList(DataSource ds) SqlException Occure",
						e);
			}
		} catch (Exception e1) {
			LOGGER.error(
					"getChangeReqActionsStatusList(DataSource ds) Exception Occure",
					e1);
			throw new SysException(e1);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return changeReqActionsList;
	}

	/**
	 * Gets the final disposition status list.
	 * 
	 * @param ds
	 *            the ds
	 * @return the final disposition status list
	 * @throws SysException
	 *             the sys exception
	 */
	public static List<LabelValue> getFinalDispositionStatusList(DataSource ds)
			throws SysException {
		List<LabelValue> finalDispositionStatusList = new ArrayList<LabelValue>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BeanProcessor beanProcessor = new BeanProcessor();

		try {
			conn = ds.getConnection();
			String query = "select * from dbo.pci_change_request_final_approval_dispostion_master order by 2 asc";
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				finalDispositionStatusList.add(new LabelValue(rs.getString(2),
						rs.getString(1)));
			}

		} catch (SQLException e) {
			if (conn != null) {
				LOGGER.error(
						"getFinalDispositionStatusList(DataSource ds) SqlException Occure",
						e);
			}
		} catch (Exception e1) {
			LOGGER.error(
					"getFinalDispositionStatusList(DataSource ds) Exception Occure",
					e1);
			throw new SysException(e1);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return finalDispositionStatusList;
	}

	/**
	 * Gets the site identifier list.
	 * 
	 * @param customerID
	 *            the customer id
	 * @param ds
	 *            the ds
	 * @param siteIdentifierList
	 *            the site identifier list
	 * @return the site identifier list
	 * @throws SysException
	 *             the sys exception
	 */
	public static List<LabelValue> getSiteIdentifierList(String customerID,
			DataSource ds, List<LabelValue> siteIdentifierList)
			throws SysException {
		List<LabelValue> allsiteIdentifierList = siteIdentifierList;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String query = "select lp_sl_id,lp_si_number+','+lp_si_city+','+lp_si_state AS siteIdentifier from lp_site_list where lp_md_mm_id=?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, customerID);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				allsiteIdentifierList.add(new LabelValue(rs.getString(2), rs
						.getString(1)));
			}

		} catch (SQLException e) {
			if (conn != null) {
				LOGGER.error(
						"getSiteIdentifierList(String customerID,DataSource ds) SqlException Occure",
						e);
			}
		} catch (Exception e1) {
			LOGGER.error(
					"getSiteIdentifierList(String customerID,DataSource ds) Exception Occure",
					e1);
			throw new SysException(e1);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return allsiteIdentifierList;
	}

	/**
	 * Gets the static site identifier list.
	 * 
	 * @param ds
	 *            the ds
	 * @return the static site identifier list
	 * @throws SysException
	 *             the sys exception
	 */
	public static List<LabelValue> getStaticSiteIdentifierList(DataSource ds)
			throws SysException {
		List<LabelValue> siteIdentifierList = new ArrayList<LabelValue>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String query = "select pci_chg_req_static_si_id,pci_chg_req_static_si_name from pci_change_request_static_master_sites";
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			siteIdentifierList.add(new LabelValue("---Select---", "0"));
			while (rs.next()) {
				siteIdentifierList.add(new LabelValue(rs.getString(2), rs
						.getString(1)));
			}

		} catch (SQLException e) {
			if (conn != null) {
				LOGGER.error(
						"getSiteIdentifierList(String customerID,DataSource ds) SqlException Occure",
						e);
			}
		} catch (Exception e1) {
			LOGGER.error(
					"getSiteIdentifierList(String customerID,DataSource ds) Exception Occure",
					e1);
			throw new SysException(e1);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return siteIdentifierList;
	}

	/**
	 * Save change request data.
	 * 
	 * @param ds
	 *            the ds
	 * @param bean
	 *            the bean
	 * @return the integer
	 * @throws SysException
	 *             the sys exception
	 */
	public static Integer saveChangeRequestData(DataSource ds,
			PCIChangeControlBean bean) throws SysException {
		if (LOGGER.isDebugEnabled()) {

		}
		Connection conn = null;
		CallableStatement cstmt = null;
		int saveFlag = 0;
		int changeRequestID = 0;
		try {
			conn = ds.getConnection();
			conn.setAutoCommit(false);
			cstmt = conn
					.prepareCall("{?=call pci_change_request_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}"); // 5
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setString(2, bean.getSelectedChangeRequestStatus());
			cstmt.setString(3, bean.getOrgTopId());
			cstmt.setString(4, bean.getSelectedSiteID());
			cstmt.setString(5, bean.getDescriptionOfChange());
			cstmt.setString(6, bean.getReasonForChanges());
			cstmt.setString(7, bean.getResourceImpact());
			cstmt.setString(8, bean.getFinalDispositionName());
			cstmt.setString(9, bean.getFinalDispositionStatusSelected());
			cstmt.setString(10, bean.getRequestorName());
			cstmt.setString(11, bean.getFinalDispositionDate());
			cstmt.setString(12, bean.getFinalDispositionComments());
			cstmt.setString(13, bean.getCurrentMode());
			if (bean.getCurrentMode().equalsIgnoreCase("U")
					|| bean.getCurrentMode().equalsIgnoreCase("D")) {
				cstmt.setString(14, bean.getChangeRequestID());
				cstmt.executeUpdate();
				saveFlag = cstmt.getInt(1);
			} else if (bean.getCurrentMode().equalsIgnoreCase("A")) {
				cstmt.registerOutParameter(14, java.sql.Types.INTEGER);
				cstmt.executeUpdate();
				saveFlag = cstmt.getInt(1);
				changeRequestID = cstmt.getInt(14);
				bean.setChangeRequestID(Integer.toString(changeRequestID));
			}
			conn.commit();
		} catch (SQLException e) {
			if (conn != null) {
				LOGGER.error(
						"getSiteIdentifierList(String customerID,DataSource ds) SqlException Occure",
						e);
				DBUtil.rollback(conn);
				saveFlag = -1;
			}
		} catch (Exception e1) {
			LOGGER.error(
					"getSiteIdentifierList(String customerID,DataSource ds) Exception Occure",
					e1);
			DBUtil.rollback(conn);
			saveFlag = -1;
			throw new SysException(e1);
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return saveFlag;
	}

	/**
	 * Save update change request action data.
	 * 
	 * @param ds
	 *            the ds
	 * @param actiondetail
	 *            the actiondetail
	 * @return the integer
	 * @throws SysException
	 *             the sys exception
	 */
	public static Integer saveUpdateChangeRequestActionData(DataSource ds,
			ChangeRequestActionDetails actiondetail) throws SysException {
		if (LOGGER.isDebugEnabled()) {

		}
		Connection conn = null;
		CallableStatement cstmt = null;
		int saveFlag = 0;
		try {
			conn = ds.getConnection();
			conn.setAutoCommit(false);
			cstmt = conn
					.prepareCall("{?=call pci_change_request_action_manage_01(?,?,?,?,?,?,?,?)}"); // 5
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setString(2, actiondetail.getStatus());
			cstmt.setString(3, actiondetail.getPlannedDate());
			cstmt.setString(4, actiondetail.getActualDate());
			cstmt.setString(5, actiondetail.getNotes());
			cstmt.setString(6, actiondetail.getChangeRequestId());
			cstmt.setString(7, actiondetail.getActionName());
			cstmt.setString(8, actiondetail.getActionMode());
			if (actiondetail.getActionMode().equalsIgnoreCase("U")
					|| actiondetail.getActionMode().equalsIgnoreCase("D")) {
				cstmt.setString(9, actiondetail.getActionID());
				cstmt.executeUpdate();
				saveFlag = cstmt.getInt(1);
			} else if (actiondetail.getActionMode().equalsIgnoreCase("A")) {
				cstmt.setString(9, "0");
				cstmt.executeUpdate();
				saveFlag = cstmt.getInt(1);
			}
			conn.commit();
		}
		// SqlException Handle
		catch (SQLException e) {
			if (conn != null) {
				LOGGER.error(
						"saveUpdateChangeRequestActionData(DataSource ds,ChangeRequestActionDetails actiondetail) SqlException Occure",
						e);
				DBUtil.rollback(conn);
				saveFlag = -1;
			}
		}
		// Exception Other Than SQLException
		catch (Exception e1) {
			LOGGER.error(
					"saveUpdateChangeRequestActionData(DataSource ds,ChangeRequestActionDetails actiondetail) Exception Occure",
					e1);
			DBUtil.rollback(conn);
			saveFlag = -1;
			throw new SysException(e1);
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return saveFlag;
	}

	/**
	 * Gets the change request data.
	 * 
	 * @param changeRequestID
	 *            the change request id
	 * @param ds
	 *            the ds
	 * @return the change request data
	 * @throws SysException
	 *             the sys exception
	 */
	public static PCIChangeControlBean getChangeRequestData(
			String changeRequestID, DataSource ds) throws SysException {
		PCIChangeControlBean bean = new PCIChangeControlBean();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String query = "select pci_chg_req_status_code,pci_chg_req_cust_id,pci_chg_req_des_change,pci_chg_req_res_change,"
					+ "pci_chg_req_resource_impact,pci_chg_req_si_id,pci_chg_req_final_app_comments,pci_chg_req_final_app_name,pci_chg_req_final_app_dis_code,"
					+ "replace(isnull(convert(varchar(10),pci_chg_req_final_app_date,101),''),'01/01/1900' ,'') AS pci_chg_req_final_app_date,pci_chg_req_si_identifier from dbo.pci_change_request where pci_chg_req_id =?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, changeRequestID);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				bean.setSelectedChangeRequestStatus(rs
						.getString("pci_chg_req_status_code"));
				bean.setOrgTopId(rs.getString("pci_chg_req_cust_id"));
				bean.setDescriptionOfChange(rs
						.getString("pci_chg_req_des_change"));
				bean.setReasonForChanges(rs.getString("pci_chg_req_res_change"));
				bean.setResourceImpact(rs
						.getString("pci_chg_req_resource_impact"));
				bean.setSelectedSiteID(rs.getString("pci_chg_req_si_id"));
				bean.setFinalDispositionComments(rs
						.getString("pci_chg_req_final_app_comments"));
				bean.setFinalDispositionDate(rs
						.getString("pci_chg_req_final_app_date"));
				bean.setFinalDispositionName(rs
						.getString("pci_chg_req_final_app_name"));
				bean.setFinalDispositionStatusSelected(rs
						.getString("pci_chg_req_final_app_dis_code"));
				bean.setSelectedSiteIdentifierName(rs
						.getString("pci_chg_req_si_identifier"));

			}

		} catch (SQLException e) {
			if (conn != null) {
				LOGGER.error(
						"getChangeRequestData(String changeRequestID,DataSource ds) SqlException Occure",
						e);
			}
		} catch (Exception e1) {
			LOGGER.error(
					"getChangeRequestData(String changeRequestID,DataSource ds) Exception Occure",
					e1);
			throw new SysException(e1);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return bean;
	}

	/**
	 * Gets the change request action data.
	 * 
	 * @param changeRequestID
	 *            the change request id
	 * @param ds
	 *            the ds
	 * @return the change request action data
	 * @throws SysException
	 *             the sys exception
	 */
	public static List<ChangeRequestActionDetails> getChangeRequestActionData(
			String changeRequestID, DataSource ds) throws SysException {
		List<ChangeRequestActionDetails> actionDetailList = new ArrayList<ChangeRequestActionDetails>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String query = "select pci_change_request_actions_id,pci_chg_req_action_name,pci_chg_req_act_status_code,replace(isnull(convert(varchar(10),pci_chg_req_act_planned_date,101),''),'01/01/1900' ,'') AS pci_chg_req_act_planned_date,replace(isnull(convert(varchar(10),pci_chg_req_act_actual_Date,101),''),'01/01/1900' ,'') AS pci_chg_req_act_actual_Date,"
					+ "pci_chg_req_act_notes,pci_chg_req_action_mode from pci_change_request_actions where pci_chg_req_id=? and pci_chg_req_action_mode<>'D'";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, changeRequestID);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				ChangeRequestActionDetails bean = new ChangeRequestActionDetails();
				bean.setActionID(rs.getString("pci_change_request_actions_id"));
				bean.setActionName(rs.getString("pci_chg_req_action_name"));
				bean.setStatus(rs.getString("pci_chg_req_act_status_code"));
				bean.setPlannedDate(rs
						.getString("pci_chg_req_act_planned_date"));
				bean.setActualDate(rs.getString("pci_chg_req_act_actual_Date"));
				bean.setNotes(rs.getString("pci_chg_req_act_notes"));
				bean.setActionMode(rs.getString("pci_chg_req_action_mode"));
				actionDetailList.add(bean);
			}
		} catch (SQLException e) {
			if (conn != null) {
				LOGGER.error(
						"getChangeRequestActionData(String changeRequestID, DataSource ds) SqlException Occure",
						e);
			}
		} catch (Exception e1) {
			LOGGER.error(
					"getChangeRequestActionData(DataSource ds,PCIChangeControlBean bean) Exception Occure",
					e1);
			throw new SysException(e1);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return actionDetailList;
	}

	/**
	 * Save final disposition.
	 * 
	 * @param ds
	 *            the ds
	 * @param bean
	 *            the bean
	 * @return the integer
	 * @throws SysException
	 *             the sys exception
	 */
	public static Integer saveFinalDisposition(DataSource ds,
			PCIChangeControlBean bean) throws SysException {
		if (LOGGER.isDebugEnabled()) {

		}
		Connection conn = null;
		CallableStatement cstmt = null;
		int saveFlag = 0;

		try {
			conn = ds.getConnection();
			conn.setAutoCommit(false);
			cstmt = conn
					.prepareCall("{?=call pci_change_request_final_disposition_submit_01(?,?,?,?,?)}"); // 5
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setString(2, bean.getFinalDispositionName());
			cstmt.setString(3, bean.getFinalDispositionStatusSelected());
			cstmt.setString(4, bean.getFinalDispositionDate());
			cstmt.setString(5, bean.getFinalDispositionComments());
			cstmt.setString(6, bean.getChangeRequestID());
			cstmt.executeUpdate();
			saveFlag = cstmt.getInt(1);
			conn.commit();
		} catch (SQLException e) {
			if (conn != null) {
				LOGGER.error(
						"saveFinalDisposition(DataSource ds,PCIChangeControlBean bean) SqlException Occure",
						e);
				DBUtil.rollback(conn);
				saveFlag = -1;
			}
		} catch (Exception e1) {
			LOGGER.error(
					"saveFinalDisposition(DataSource ds,PCIChangeControlBean bean) Exception Occure",
					e1);
			DBUtil.rollback(conn);
			saveFlag = -1;
			throw new SysException(e1);
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return saveFlag;
	}
}
