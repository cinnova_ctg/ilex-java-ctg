package com.mind.MSP.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.fw.core.dao.util.DBUtil;

public class MSPISPUpTimeDao {
	public static final Logger logger = Logger.getLogger(MSPISPUpTimeDao.class);

	/**
	 * Gets the table detail.
	 * 
	 * @param query
	 *            the query
	 * @param ds
	 *            the ds
	 * 
	 * @return the table detail
	 */
	public static String getTableDetail(String query, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String tableDaetail = "";
		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				tableDaetail = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error(
					"customer="
							+ "\n getTableDetail(String,String,String,DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return tableDaetail;
	}
}
