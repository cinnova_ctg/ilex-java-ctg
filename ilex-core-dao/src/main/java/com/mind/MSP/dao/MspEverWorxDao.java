package com.mind.MSP.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.TreeMap;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.EverWorx;
import com.mind.fw.core.dao.util.DBUtil;

public class MspEverWorxDao {
	private static final Logger logger = Logger.getLogger(Mspdao.class);

	public static ArrayList<EverWorx> getEverWorxListOld(int status,
			DataSource ds) {
		ArrayList<EverWorx> everWorxList = new ArrayList<EverWorx>();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		int i = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call wug_map_demo_01(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, status);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				EverWorx EverWorx = new EverWorx();
				EverWorx.setName(rs.getString("wug_dv_display_name"));
				EverWorx.setCity(rs.getString("lp_si_city"));
				EverWorx.setState(rs.getString("lp_si_state"));
				EverWorx.setLatitude(rs.getString("lp_si_latitude"));
				EverWorx.setLongitude(rs.getString("lp_si_longitude"));
				EverWorx.setStatus(rs.getInt("status"));
				everWorxList.add(i, EverWorx);
				i++;
			}

		} catch (Exception e) {
			logger.error("status=" + status + " "
					+ "\ngetRealTimeState(DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}

		return everWorxList;
	}

	public static ArrayList<TreeMap<String, String>> getEverWorxList(
			String status, DataSource ds) {
		ArrayList<TreeMap<String, String>> everWorxList = new ArrayList<TreeMap<String, String>>();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		int i = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call wug_map_demo_01(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, status);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				TreeMap<String, String> everWorx = new TreeMap<String, String>();
				everWorx.put("Name", rs.getString("wug_dv_display_name"));
				everWorx.put("City", rs.getString("lp_si_city"));
				everWorx.put("State", rs.getString("lp_si_state"));
				everWorx.put("Latitude", rs.getString("lp_si_latitude"));
				everWorx.put("Longitude", rs.getString("lp_si_longitude"));
				everWorx.put("Status", rs.getString("status"));
				everWorxList.add(i, everWorx);
				i++;
			}
			/* To set Test marker: Start */
			/*
			 * for(int j=i;j<200; j++) { TreeMap<String, String> everWorx = new
			 * TreeMap<String, String>(); everWorx.put("Name", "");
			 * everWorx.put("City", ""); everWorx.put("State", "");
			 * everWorx.put("Latitude", ""); everWorx.put("Longitude", "");
			 * everWorx.put("Status", ""); everWorxList.add(i,everWorx); j++; }
			 */
			/* To set Test marker: End */

		} catch (Exception e) {
			logger.error("status=" + status + " "
					+ "\ngetRealTimeState(DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}

		return everWorxList;
	}

}
