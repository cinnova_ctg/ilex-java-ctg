package com.mind.MSP.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.Bounces;
import com.mind.bean.msp.MonitoringSummary;
import com.mind.bean.msp.MspNetmedxTicket;
import com.mind.bean.msp.RealTimeState;
import com.mind.bean.msp.RealTimeStateInfo;
import com.mind.common.Util;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.msp.common.EscalationLevel;

/**
 * The Class MSPdao.
 */
public class Mspdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Mspdao.class);

	/**
	 * Gets the real time state list from database.
	 * 
	 * @param RealTimeState
	 * @param DataSource
	 * @return
	 */
	public static ArrayList<RealTimeState> getRealTimeState(
			int escalationLevel, DataSource ds) {

		ArrayList<RealTimeState> realTimeStates = new ArrayList<RealTimeState>();
		StringBuilder ticketNumbers = new StringBuilder();

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int i = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call wug_get_inprocess_list_01(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, escalationLevel);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				RealTimeState bean = new RealTimeState();
				bean.setCurrentStatus(rs.getString("wug_ip_current_status"));
				bean.setDisplayName(rs.getString("wug_dv_display_name"));
				bean.setEventCount(rs.getString("wug_ip_event_count"));
				bean.setEventDay(rs.getString("event_day"));
				bean.setEventTime(rs.getString("event_time"));
				bean.setMultipleDays(rs.getString("multiple_days"));
				bean.setNActiveMonitorStateChangeID(rs
						.getString("wug_ip_nActiveMonitorStateChangeID"));
				bean.setWugInstance(rs.getString("instance"));
				bean.setPreviousNoAction(rs.getInt("previous_no_action"));
				bean.setRowsNotDisplayed(rs.getString("rows_not_displayed"));
				// bean.setJobOwner(rs.getString("created_by"));
				bean.setTicketReference(rs.getString("wug_ip_ticket_reference"));
				bean.setNextAction(rs.getString("next_action"));
				/* Set ticket to get its last update by */
				if (!Util.isNullOrBlank(bean.getTicketReference())) {
					ticketNumbers.append("'")
							.append(bean.getTicketReference().trim())
							.append("',");
				}
				bean.setOutageDuration(rs.getString("wug_ip_outage_duration"));
				realTimeStates.add(i, bean);
				i++;
			}

			Map<String, String> lastUpdated = getLastJobUpdatedByList(
					Util.removeLastComm(ticketNumbers.toString()), ds);
			for (RealTimeState timeState : realTimeStates) {
				timeState.setLastAction(lastUpdated.get(timeState
						.getTicketReference()));
			}

		} catch (Exception e) {
			logger.error("escalationLevel=" + escalationLevel + " "
					+ "\ngetRealTimeState(DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return realTimeStates;
	}// - End of getRealTimeState(DataSource)

	// Changes By Yogendra

	public static ArrayList<RealTimeState> managedInitialAssessment(
			int escalationLevel, DataSource ds) {

		ArrayList<RealTimeState> realTimeStates = new ArrayList<RealTimeState>();
		StringBuilder ticketNumbers = new StringBuilder();

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int i = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call wug_get_inprocess_list_02(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, escalationLevel);
			cstmt.setInt(3, EscalationLevel.MANAGED_INIT_ASSESSEMENT);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				RealTimeState bean = new RealTimeState();
				bean.setCurrentStatus(rs.getString("wug_ip_current_status"));
				bean.setDisplayName(rs.getString("wug_dv_display_name"));
				bean.setEventCount(rs.getString("wug_ip_event_count"));
				bean.setEventDay(rs.getString("event_day"));
				bean.setEventTime(rs.getString("event_time"));
				bean.setMultipleDays(rs.getString("multiple_days"));
				bean.setNActiveMonitorStateChangeID(rs
						.getString("wug_ip_nActiveMonitorStateChangeID"));
				bean.setWugInstance(rs.getString("instance"));
				bean.setPreviousNoAction(rs.getInt("previous_no_action"));
				bean.setRowsNotDisplayed(rs.getString("rows_not_displayed"));
				bean.setTicketReference(rs.getString("wug_ip_ticket_reference"));
				/* Set ticket to get its last update by */
				if (!Util.isNullOrBlank(bean.getTicketReference())) {
					ticketNumbers.append("'")
							.append(bean.getTicketReference().trim())
							.append("',");
				}
				bean.setOutageDuration(rs.getString("wug_ip_outage_duration"));
				// added for wug_ip_tc_id
				bean.setTicketId(rs.getString("wug_ip_tc_id"));
				realTimeStates.add(i, bean);
				i++;
			}

			Map<String, String> lastUpdated = getLastJobUpdatedByList(
					Util.removeLastComm(ticketNumbers.toString()), ds);
			for (RealTimeState timeState : realTimeStates) {
				timeState.setLastAction(lastUpdated.get(timeState
						.getTicketReference()));
			}

		} catch (Exception e) {
			logger.error("escalationLevel=" + escalationLevel + " "
					+ "\ngetRealTimeState(DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return realTimeStates;
	}// - End of getRealTimeState(DataSource)

	/**
	 * Gets the real time state list from database.
	 * 
	 * @param RealTimeState
	 * @param DataSource
	 * @return
	 */
	public static ArrayList<RealTimeState> unManagedInitialAssessment(
			int escalationLevel, DataSource ds) {

		ArrayList<RealTimeState> realTimeStates = new ArrayList<RealTimeState>();
		StringBuilder ticketNumbers = new StringBuilder();

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int i = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call wug_get_inprocess_list_02(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, escalationLevel);
			cstmt.setInt(3, EscalationLevel.UNMANAGED_INIT_ASSESSEMENT);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				RealTimeState bean = new RealTimeState();
				bean.setCurrentStatus(rs.getString("wug_ip_current_status"));
				bean.setDisplayName(rs.getString("wug_dv_display_name"));
				bean.setEventCount(rs.getString("wug_ip_event_count"));
				bean.setEventDay(rs.getString("event_day"));
				bean.setEventTime(rs.getString("event_time"));
				bean.setMultipleDays(rs.getString("multiple_days"));
				bean.setNActiveMonitorStateChangeID(rs
						.getString("wug_ip_nActiveMonitorStateChangeID"));
				bean.setWugInstance(rs.getString("instance"));
				bean.setPreviousNoAction(rs.getInt("previous_no_action"));
				bean.setRowsNotDisplayed(rs.getString("rows_not_displayed"));
				bean.setTicketReference(rs.getString("wug_ip_ticket_reference"));
				/* Set ticket to get its last update by */
				if (!Util.isNullOrBlank(bean.getTicketReference())) {
					ticketNumbers.append("'")
							.append(bean.getTicketReference().trim())
							.append("',");
				}
				bean.setOutageDuration(rs.getString("wug_ip_outage_duration"));
				realTimeStates.add(i, bean);
				i++;
			}
			Map<String, String> lastUpdated = getLastJobUpdatedByList(
					Util.removeLastComm(ticketNumbers.toString()), ds);
			for (RealTimeState timeState : realTimeStates) {
				timeState.setLastAction(lastUpdated.get(timeState
						.getTicketReference()));
			}

		} catch (Exception e) {
			logger.error("escalationLevel=" + escalationLevel + " "
					+ "\ngetRealTimeState(DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return realTimeStates;
	}// - End of getRealTimeState(DataSource)

	// End Changes By Yogendra

	/**
	 * Gets the last job updated by list.
	 * 
	 * @param ticketNumbers
	 *            the ticket numbers
	 * @param ds
	 *            the ds
	 * @return the last job updated by list
	 */
	public static Map<String, String> getLastJobUpdatedByList(
			String ticketNumbers, DataSource ds) {
		Map<String, String> lastUpdated = new HashMap<String, String>();
		if (Util.isNullOrBlank(ticketNumbers)) {
			return lastUpdated;
		}
		StringBuilder query = new StringBuilder();
		query.append(" select");
		query.append(" ticket_number = lm_tc_number");
		query.append(" ,last_updated_by = dbo.func_cc_poc_name(isnull(lm_js_changed_by,0))");
		query.append(" from lm_ticket");
		query.append(" inner join lm_job on lm_js_id = lm_tc_js_id");
		query.append(" where");
		query.append(" lm_tc_number in (").append(ticketNumbers).append(")");
		Connection conn = null;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				lastUpdated.put(rs.getString("ticket_number"),
						rs.getString("last_updated_by"));
			}
		} catch (Exception e) {
			logger.error("Query->" + query.toString() + "\n ticketNumbers->"
					+ ticketNumbers + "\n" + e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return lastUpdated;
	}

	/**
	 * Gets the real time state info from database. Calls stored procedure
	 * wug_outage_no_action_insert_01 with 4 output parameter.
	 * 
	 * @param ds
	 *            the DataSource
	 * @return RealTimeStateInfo the real time state info
	 */
	public static RealTimeStateInfo getRealTimeStateInfo(DataSource ds) {

		RealTimeStateInfo realTimeStateInfo = new RealTimeStateInfo();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call wug_get_realtime_parameters_01(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.registerOutParameter(2, java.sql.Types.INTEGER);
			cstmt.registerOutParameter(3, java.sql.Types.INTEGER);
			cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
			cstmt.registerOutParameter(5, java.sql.Types.INTEGER);
			cstmt.execute();
			realTimeStateInfo.setPageFlag(cstmt.getInt(2));
			realTimeStateInfo.setRunTimeAlert(cstmt.getInt(3));
			realTimeStateInfo.setRunTimeDisplay(cstmt.getString(4));
			realTimeStateInfo.setEventCount(cstmt.getInt(5));

		} catch (Exception e) {
			logger.error("getRealTimeStateInfo(DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return realTimeStateInfo;
	}// End of getRealTimeStateInfo(DataSource)

	/**
	 * Sets the No Action. Calls stored procedure wug_outage_no_action_insert_01
	 * with one input.
	 * 
	 * @param nActiveMonitorStateChangeID
	 *            the wug_ip_nActiveMonitor StateChangeID
	 * @param ds
	 *            the DataSource
	 */
	public static int setNoAction(String nActiveMonitorStateChangeID,
			String wugInstance, DataSource ds, String userId) {

		Connection conn = null;
		CallableStatement cstmt = null;
		int noActionFlag = -1;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call wug_outage_no_action_insert_01(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, nActiveMonitorStateChangeID);
			cstmt.setString(3, userId);
			cstmt.setString(4, wugInstance);
			cstmt.execute();
			noActionFlag = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("nActiveMonitorStateChangeID="
					+ nActiveMonitorStateChangeID
					+ "\nsetNoAction(StringDataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return noActionFlag;
	}// End of setNoAction(String, DataSource)

	/**
	 * Gets the site data.
	 * 
	 * @param nActiveMonitorStateChangeID
	 *            the Active Monitor State Change ID
	 * @param DataSource
	 *            the ds
	 * @return String[] the site data String[0] the Site Id String[1] the Msa Id
	 *         String[2] the Appendix Id
	 */
	public static String[] getSiteData(String nActiveMonitorStateChangeID,
			String wugInstance, DataSource ds) {
		String[] siteData = new String[3];
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			pStmt = conn
					.prepareStatement("select * from func_wug_get_site(?,?)");
			pStmt.setString(1, nActiveMonitorStateChangeID);
			pStmt.setString(2, wugInstance);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				siteData[0] = rs.getString("site_id");
				siteData[1] = rs.getString("msa_id");
				siteData[2] = rs.getString("appendix_id");
			}
		} catch (Exception e) {
			logger.error("nActiveMonitorStateChangeID="
					+ nActiveMonitorStateChangeID
					+ "\n setSiteData(long, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return siteData;
	}// End setSiteData(long, DataSource)

	/**
	 * Gets the ticket number of created ticket on the basis of ticket_id.
	 * 
	 * @param String
	 *            the ticket id
	 * @param DataSource
	 *            the ds
	 * @return String the ticket number
	 */
	public static String getTicketNumber(String ticketId, DataSource ds) {
		String ticketNumber = "";
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			pStmt = conn
					.prepareStatement("select isnull(lm_tc_number,'') as ticket_number from lm_ticket where lm_tc_id = ?");
			pStmt.setString(1, ticketId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				ticketNumber = rs.getString("ticket_number");
			}
		} catch (Exception e) {
			logger.error("nActiveMonitorStateChangeID=" + ticketId
					+ "\n getTicketNumber(long, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return ticketNumber;
	}// End setSiteData(long, DataSource)

	/**
	 * Gets the monitoring summary from database.
	 * 
	 * @param DataSource
	 *            the ds
	 * @return MonitoringSummaryBean the monitoring summary
	 */
	public static ArrayList<MonitoringSummary> getMonitoringSummary(
			String fromPage, DataSource ds) {

		ArrayList<MonitoringSummary> MonitoringSummaryList = new ArrayList<MonitoringSummary>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		String subQuery = "";
		if (!Util.isNullOrBlank(fromPage)
				&& fromPage.equalsIgnoreCase("everWorx")) {
			subQuery = " where wug_ms_dv_type = 1";
		}
		try {
			conn = ds.getConnection();
			/*
			 * Query Commented For Change the display on MSP summary screen like
			 * remove the Bounces field and add new filed customer on Screen
			 */

			/*
			 * sql = "select " +
			 * " case when wug_ms_dv_type = 1 then 'Site' else 'Device' end as Line,"
			 * + " wug_ms_cnt_active," + // -- Active " wug_ms_cnt_down," + //
			 * -- Down
			 * " case when wug_ms_cnt_last_get_count > 10 then 1 else 0 end as display_symbol,"
			 * + " wug_ms_cnt_level_0," + // -- Initial Assessment
			 * " wug_ms_cnt_level_1," + // -- Help Desk " wug_ms_cnt_level_2," +
			 * // -- MSP Dispatch " wug_ms_cnt_watch_list," + // -- Watch
			 * " wug_ms_cnt_bounces" + // -- Bounces
			 * " from dbo.wug_monitoring_summary" + subQuery;
			 */

			sql = " SELECT "
					+ " CASE WHEN ISNULL(wug_ms_dv_type, 1) = 1 THEN 'Managed' "
					+ " ELSE 'Unmanaged' END AS Line, "
					+ " WUG_MS_CNT_ACTIVE , " // --Active
					+ " WUG_MS_CNT_DOWN , " // --Down
					+ " CASE WHEN WUG_MS_CNT_LAST_GET_COUNT > 10 THEN 1 ELSE 0 END AS DISPLAY_SYMBOL , "
					// --Display Symbol on 1
					+ " WUG_MS_CNT_LEVEL_0 , " // -- Initial Assessment
					+ " WUG_MS_CNT_LEVEL_1 ," //
					+ " WUG_MS_CNT_LEVEL_2 ," // MSP DISpacth
					+ " WUG_MS_CNT_WATCH_LIST  " // Customer
					+ " FROM dbo.WUG_MONITORING_SUMMARY ";

			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			int i = 0;
			int activeSum = 0;
			int downSum = 0;
			int initAssessmntSum = 0;
			int helpDeskSum = 0;
			int mspDisptchSum = 0;
			int customerSum = 0;
			MonitoringSummary monitoringSummaryTotal = new MonitoringSummary();
			while (rs.next()) {
				MonitoringSummary monitoringSummary = new MonitoringSummary();
				monitoringSummary.setLine(rs.getString("Line"));
				monitoringSummary.setActive(rs.getInt("wug_ms_cnt_active"));
				activeSum = activeSum + monitoringSummary.getActive();
				monitoringSummary.setDown(rs.getInt("wug_ms_cnt_down"));
				downSum = downSum + monitoringSummary.getDown();
				monitoringSummary.setDisplaySymbol(rs.getInt("display_symbol"));
				monitoringSummary.setInitialAssessment(rs
						.getInt("wug_ms_cnt_level_0"));
				initAssessmntSum = initAssessmntSum
						+ monitoringSummary.getInitialAssessment();
				monitoringSummary.setHelpDesk(rs.getInt("wug_ms_cnt_level_1"));
				helpDeskSum = helpDeskSum + monitoringSummary.getHelpDesk();
				monitoringSummary.setMspDispatch(rs
						.getInt("wug_ms_cnt_level_2"));
				mspDisptchSum = mspDisptchSum
						+ monitoringSummary.getMspDispatch();
				monitoringSummary.setWatch(rs.getInt("wug_ms_cnt_watch_list"));
				customerSum = customerSum + monitoringSummary.getWatch();
				// Commented: remove Field(Bounces) From the Screen
				// monitoringSummary.setBounces(rs.getInt("wug_ms_cnt_bounces"));
				MonitoringSummaryList.add(i, monitoringSummary);
				i++;
			}
			monitoringSummaryTotal.setLine("Total");
			monitoringSummaryTotal.setActive(activeSum);
			monitoringSummaryTotal.setDown(downSum);
			monitoringSummaryTotal.setInitialAssessment(initAssessmntSum);
			monitoringSummaryTotal.setHelpDesk(helpDeskSum);
			monitoringSummaryTotal.setMspDispatch(mspDisptchSum);
			monitoringSummaryTotal.setWatch(customerSum);
			MonitoringSummaryList.add(i, monitoringSummaryTotal);

		} catch (Exception e) {
			logger.error("Query:::::::::::::::::::\n" + sql
					+ "\ngetMonitoringSummary(DataSource)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return MonitoringSummaryList;
	} // end of getMonitoringSummary(DataSource)

	/**
	 * Gets the incidents flag(Escalation Level) from database.
	 * 
	 * @param int the escalation Level
	 * @param DataSource
	 *            the ds
	 * @return int the incidents flag
	 */
	public static int getEscalationLevel(int escalationLevel, DataSource ds) {
		int pageFlag = 0;
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			pStmt = conn.prepareStatement("select wug_pf_page_flag"
					+ " from wug_current_status_page_flag"
					+ " where wug_pf_escalation_level = ?");
			pStmt.setInt(1, escalationLevel);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				pageFlag = rs.getInt("wug_pf_page_flag");
			}
		} catch (Exception e) {
			logger.error("EscalationLevel=" + escalationLevel
					+ "\n getIncidentsFlag(long, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return pageFlag;
	}// End getEscalationLevel(long, DataSource)

	/**
	 * Sets the to help desk. the record identified by wug_ip_nActiveMonitor
	 * StateChangeID is switch from �Initial Assessment� state to �Help Desk� by
	 * updating the wug_ip_escalation_level to one(1). The ticket number is also
	 * added this table in the wug_ip_ticket_reference field.
	 * 
	 * @param String
	 *            the n active monitor state change id
	 * @param String
	 *            the ticket number
	 * @param int the escalation level
	 * @param DataSource
	 *            the ds
	 */
	public static void setToHelpDesk(String nActiveMonitorStateChangeID,
			String wugInstance, String ticketNumber, int escalationLevel,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;

		try {
			conn = ds.getConnection();
			pStmt = conn
					.prepareStatement(" update wug_outages_incidents_in_process"
							+ " set wug_ip_escalation_level = ?,"
							+ " wug_ip_ticket_reference = ?"
							+ " where wug_ip_nActiveMonitorStateChangeID = ? and wug_ip_instance = ?");
			pStmt.setInt(1, escalationLevel);
			pStmt.setString(2, ticketNumber);
			pStmt.setString(3, nActiveMonitorStateChangeID);
			pStmt.setString(4, wugInstance);
			pStmt.execute();

		} catch (Exception e) {
			logger.error(
					"nActiveMonitorStateChangeID="
							+ nActiveMonitorStateChangeID
							+ "\n ticketNumber="
							+ ticketNumber
							+ "\n escalationLevel="
							+ escalationLevel
							+ "\n setToHelpDesk(String, String, int escalationLevel, DataSource)",
					e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}// End setToHelpDesk(long, DataSource)

	/**
	 * Gets the bounces list.
	 * 
	 * @param DataSource
	 *            the ds
	 * @return ArrayList<Bounces> the bounces list
	 */
	public static ArrayList<Bounces> getBouncesList(DataSource ds,
			int nameLength) {

		ArrayList<Bounces> bouncesList = new ArrayList<Bounces>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = ds.getConnection();
			sql = " select wug_dv_display_name as Site,"
					+ " convert (varchar(10), wug_ni_max_dStartTime, 101) as Date,"
					+ " wug_ni_down_count as Count"
					+ " from wug_non_intersecting_bounces"
					+ " join dbo.wug_device on wug_ni_dv_id = wug_dv_id"
					+ " order by Date desc";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			int i = 0;

			while (rs.next()) {
				Bounces bounces = new Bounces();
				bounces.setSite(Util.getFixCharacters(rs.getString("Site"),
						nameLength));
				bounces.setDate(rs.getString("Date"));
				bounces.setCount(rs.getString("Count"));
				bouncesList.add(i, bounces);
				i++;
			}
		} catch (Exception e) {
			logger.error("Query:::::::::::::::::::\n" + sql
					+ "\n getBouncesList(DataSource)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return bouncesList;
	} // end of getBouncesList(DataSource)

	/**
	 * Purpose: Find customer reported event list.
	 * 
	 * @param Integer
	 *            monitoringSourceCD
	 * @param DataSource
	 *            ds
	 * @return ArrayList<MspNetmedxTicket>
	 */
	public static ArrayList<MspNetmedxTicket> findCustomerReportedEventList(
			Integer monitoringSourceCD, DataSource ds) {

		ArrayList<MspNetmedxTicket> mspNetmedxTicketList = new ArrayList<MspNetmedxTicket>();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int i = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call wug_customer_reported_event_list(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, monitoringSourceCD);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				MspNetmedxTicket bean = new MspNetmedxTicket();
				bean.setCustomerAppendixName(rs.getString("customer_name"));
				bean.setSiteName(rs.getString("site_name"));
				bean.setCreatedDate(rs.getString("created_date"));
				bean.setOwnerName(rs.getString("owner_name"));
				bean.setTicketNumber(rs.getString("ticket_number"));
				bean.setJobId(rs.getString("job_id"));
				bean.setAppendixId(rs.getString("appendix_id"));
				bean.setTicketId(rs.getString("ticket_id"));
				mspNetmedxTicketList.add(i, bean);
				i++;
			}

		} catch (Exception e) {
			logger.error("monitoringSourceCD=" + monitoringSourceCD + "\n" + e);
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return mspNetmedxTicketList;
	}// - End of findCustomerReportedEventList
}
