package com.mind.MSP.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.DeviceInfoBean;
import com.mind.bean.msp.MspSiteSearchBean;
import com.mind.bean.msp.SiteDetailBean;
import com.mind.bean.msp.SiteInfo;
import com.mind.fw.core.dao.util.DBUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class MspSiteSearchDao.
 */
public class MspSiteSearchDao {

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(Mspdao.class);

	/**
	 * Gets the site search list.
	 * 
	 * @param siteSearchBean
	 *            the sitesearchbean
	 * @param ds
	 *            the ds
	 * 
	 * @return the site search list
	 */
	public static ArrayList<SiteInfo> getSiteSearchList(
			MspSiteSearchBean siteSearchBean, DataSource ds) {

		ArrayList<SiteInfo> siteSearch = new ArrayList<SiteInfo>();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int i = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call wug_search_site(?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, siteSearchBean.getCustomer());
			cstmt.setString(3, siteSearchBean.getSiteNumber());
			cstmt.setString(4, siteSearchBean.getAddress());
			cstmt.setString(5, siteSearchBean.getCity());
			cstmt.setString(6, siteSearchBean.getZipcode());
			cstmt.setString(7, siteSearchBean.getAccountNumber());
			cstmt.setString(8, siteSearchBean.getSitePhoneNumber());
			cstmt.setString(9, siteSearchBean.getSiteInternetLineNumber());
			rs = cstmt.executeQuery();

			while (rs.next()) {
				SiteInfo siteInfo = new SiteInfo();
				siteInfo.setSite_number(rs.getString("wug_dv_display_name"));
				siteInfo.setSite_address(rs.getString("lp_si_address"));
				siteInfo.setSite_city(rs.getString("lp_si_city"));
				siteInfo.setSite_state(rs.getString("lp_si_state"));
				siteInfo.setSite_id(rs.getString("lp_sl_id"));
				siteInfo.setSite_customer(rs.getString("wug_gn_msa_id"));
				siteSearch.add(siteInfo);
				i++;
			}

		} catch (Exception e) {
			logger.error("customer=" + siteSearchBean.toString()
					+ "\n getSiteSearchList(bean,DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return siteSearch;
	}// - End of getSearchList(String,String,String,String,DataSource)

	/**
	 * Gets the device info.
	 * 
	 * @param ds
	 *            the ds
	 * @param sitedetailbean
	 *            the sitedetailbean
	 * 
	 * @return the device info
	 */
	public static DeviceInfoBean getDeviceInfo(SiteDetailBean siteDetailBean,
			DataSource ds) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select wug_dv_display_name,wug_dd_circuit_type,wug_dd_modem_type ,wug_dd_internet_service_line_number,wug_dd_isp,wug_dd_isp_support_number,wug_dd_circuit_ip,wug_dd_site_contact_phone,wug_dd_location,wug_dd_account from wug_device "
				+ "inner join wug_device_detail on wug_device.wug_dv_id = wug_device_detail.wug_dd_dv_id "
				+ "inner join lp_site_list on wug_device.wug_dv_lp_si_id = lp_site_list.lp_sl_id "
				+ " where wug_device.wug_dv_lp_si_id = '"
				+ siteDetailBean.getSiteId()
				+ "' and wug_device.wug_dv_display_name = '"
				+ siteDetailBean.getSiteNumber() + "'";
		DeviceInfoBean deviceBean = new DeviceInfoBean();
		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				deviceBean.setCircuitType(rs.getString("wug_dd_circuit_type"));
				deviceBean.setModemType(rs.getString("wug_dd_modem_type"));
				deviceBean.setInternetServiceLineNo(rs
						.getString("wug_dd_internet_service_line_number"));
				deviceBean.setIsp(rs.getString("wug_dd_isp"));
				deviceBean.setIspSupportNo(rs
						.getString("wug_dd_isp_support_number"));
				deviceBean.setCircuitIP(rs.getString("wug_dd_circuit_ip"));
				deviceBean.setSiteContactNo(rs
						.getString("wug_dd_site_contact_phone"));
				deviceBean.setLocation(rs.getString("wug_dd_location"));
				deviceBean.setAccount(rs.getString("wug_dd_account"));
			}
		} catch (SQLException e) {
			logger.error(
					"SiteSearchDetail = "
							+ siteDetailBean.toString()
							+ "\n getDeviceInfo(SiteDetailBean siteDetailBean ,DataSource ds)",
					e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return deviceBean;

	}

	/**
	 * Gets the site info.
	 * 
	 * @param ds
	 *            the ds
	 * @param siteDetailBean
	 *            the sitedetailbean
	 * 
	 * @return the site info
	 */
	public static SiteInfo getSiteInfo(SiteDetailBean siteDetailBean,
			DataSource ds) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select lp_si_address,lp_si_city,lp_si_state,lp_si_zip_code,lp_si_time_zone,wug_dv_display_name from wug_device "
				+ "inner join wug_device_detail on wug_device.wug_dv_id = wug_device_detail.wug_dd_dv_id "
				+ "inner join lp_site_list on wug_device.wug_dv_lp_si_id = lp_site_list.lp_sl_id "
				+ " where wug_device.wug_dv_lp_si_id = '"
				+ siteDetailBean.getSiteId()
				+ "' and wug_device.wug_dv_display_name = '"
				+ siteDetailBean.getSiteNumber() + "'";
		SiteInfo siteInfoBean = new SiteInfo();
		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				siteInfoBean
						.setSite_number(rs.getString("wug_dv_display_name"));
				siteInfoBean.setSite_city(rs.getString("lp_si_city"));
				siteInfoBean.setSite_address(rs.getString("lp_si_address"));
				siteInfoBean.setSite_state(rs.getString("lp_si_state"));
				siteInfoBean.setSite_zip(rs.getString("lp_si_zip_code"));
				siteInfoBean.setSite_time_zone(rs.getString("lp_si_time_zone"));
			}
		} catch (SQLException e) {
			logger.error(
					"SiteSearchDetail = "
							+ siteDetailBean.toString()
							+ "\n getSiteInfo(SiteDetailBean siteDetailBean ,DataSource ds)",
					e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return siteInfoBean;

	}
}
