package com.mind.MSP.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.PCIControlSearchBean;
import com.mind.bean.msp.PCISearchInfo;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.fw.lang.SysException;

/**
 * 
 * @author yogendrasingh
 * 
 */
public class PCIControlSearchDao {

	/** The Constant logger. */
	private static final Logger LOGGER = Logger
			.getLogger(PCIControlSearchDao.class);

	/**
	 * Gets the site search list.
	 * 
	 * @param siteSearchBean
	 *            the sitesearchbean
	 * @param ds
	 *            the ds
	 * 
	 * @return the site search list
	 */
	public static List<PCISearchInfo> getPCISearchControlDataList(
			PCIControlSearchBean siteSearchBean, DataSource ds)
			throws SysException {

		ArrayList<PCISearchInfo> searchData = new ArrayList<PCISearchInfo>();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int i = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call pci_change_request_search(?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setString(2,
					siteSearchBean.getOrgTopId().equalsIgnoreCase("0") ? ""
							: siteSearchBean.getOrgTopId());
			cstmt.setString(3,
					siteSearchBean.getSelectedSiteID().equals("0") ? ""
							: siteSearchBean.getSelectedSiteID());
			cstmt.setString(
					4,
					siteSearchBean.getSelectedChangeRequestStatus().equals("0") ? ""
							: siteSearchBean.getSelectedChangeRequestStatus());
			cstmt.setString(5, siteSearchBean.getFromDate());
			cstmt.setString(6, siteSearchBean.getToDate());

			rs = cstmt.executeQuery();

			while (rs.next()) {
				PCISearchInfo pciSearchInfo = new PCISearchInfo();
				pciSearchInfo.setCustomer(rs.getString("customer"));
				pciSearchInfo.setStatus(rs.getString("status"));
				pciSearchInfo.setSite(rs.getString("site"));
				pciSearchInfo.setChangeDiscription(rs
						.getString("change_description"));
				pciSearchInfo.setChangeRequestID(rs
						.getString("changerequestid"));
				searchData.add(pciSearchInfo);
				i++;
			}

		} catch (SQLException e) {
			if (conn != null) {
				LOGGER.error(
						"saveFinalDisposition(DataSource ds,PCIChangeControlBean bean) SqlException Occure",
						e);
				DBUtil.rollback(conn);
			}
		} catch (Exception e1) {
			LOGGER.error(
					"saveFinalDisposition(DataSource ds,PCIChangeControlBean bean) Exception Occure",
					e1);
			DBUtil.rollback(conn);
			throw new SysException(e1);
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return searchData;
	}

}
