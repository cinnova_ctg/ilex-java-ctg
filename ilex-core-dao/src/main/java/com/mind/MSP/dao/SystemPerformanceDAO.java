package com.mind.MSP.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.SystemPerformanceBean;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class SystemPerformanceDAO.
 */
public class SystemPerformanceDAO {

	/** The Constant logger. */
	public static final Logger logger = Logger
			.getLogger(SystemPerformanceDAO.class);

	/**
	 * Gets the search list.
	 * 
	 * @param systemPerformanceBean
	 *            the system performance bean
	 * @param ds
	 *            the ds
	 * 
	 * @return the search list
	 */
	public static ArrayList<SystemPerformanceBean> getSearchList(
			SystemPerformanceBean systemPerformanceBean, DataSource ds) {

		ArrayList<SystemPerformanceBean> systemPerformanceBeans = new ArrayList<SystemPerformanceBean>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int i = 0;
		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select convert(varchar(10), wug_dw_cp_date, 101), isnull(wug_dw_cp_sla,0.0)*100, wug_dw_cp_bounces, wug_dw_cp_count from wug_dw_fact_customer_performance_by_day "
							+ "where wug_dw_cp_date > dateadd(dd, -31, getdate()) and wug_dw_cp_msa_id = ?"
							+ "order by wug_dw_cp_date");
			pstmt.setString(1, systemPerformanceBean.getCustomerId());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				SystemPerformanceBean bean = new SystemPerformanceBean();
				bean.setxCordinateDate(rs.getString(1));
				bean.setyCordinateSLA(rs.getString(2));
				bean.setyCordinateBounces(rs.getString(3));
				bean.setyCordinateCount(rs.getString(4));

				systemPerformanceBeans.add(i, bean);
				i++;
			}

		} catch (Exception e) {
			logger.error(
					"customer="
							+ systemPerformanceBean.toString()
							+ "\n getSearchList(String,String,String,String,DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return systemPerformanceBeans;
	}// - End of getSearchList(String,String,String,String,DataSource)

	/**
	 * Gets the goal line data.
	 * 
	 * @param systemPerformanceBean
	 *            the system performance bean
	 * @param ds
	 *            the ds
	 * 
	 * @return the goal line data
	 */
	public static String getGoalLineData(
			SystemPerformanceBean systemPerformanceBean, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String d = "98.98";
		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select isnull(il_ci1_performance_goal, 98.98) from wug_dw_il_core_project_references_msp_sup1 where il_cr1_mm_id = ?");
			pstmt.setString(1, systemPerformanceBean.getCustomerId());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				d = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error(
					"customer="
							+ systemPerformanceBean.toString()
							+ "\n getGoalLineData(String,String,String,String,DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return d;
	}// - End of getSearchList(String,String,String,String,DataSource)

	/**
	 * Gets the table detail.
	 * 
	 * @param systemPerformanceBean
	 *            the system performance bean
	 * @param tableType
	 *            the table type
	 * @param ds
	 *            the ds
	 * 
	 * @return the table detail
	 */
	public static String getTableDetail(
			SystemPerformanceBean systemPerformanceBean, String tableType,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String tableDaetail = "";
		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select wug_dw_cs_table from dbo.wug_dw_fact_customer_performance_summary where wug_dw_cs_msa_id = ? and wug_dw_cs_type = ?");
			pstmt.setString(1, systemPerformanceBean.getCustomerId());
			pstmt.setString(2, tableType);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				tableDaetail = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error(
					"customer="
							+ systemPerformanceBean.toString()
							+ "\n getTableDetail(String,String,String,String,DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return tableDaetail;
	}

	/**
	 * Gets the max count.
	 * 
	 * @param systemPerformanceBean
	 *            the system performance bean
	 * @param ds
	 *            the ds
	 * 
	 * @return the max count
	 */
	public static float[] getMaxCount(
			SystemPerformanceBean systemPerformanceBean, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		float[] maxCount = new float[3];
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call get_wug_max_count(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, systemPerformanceBean.getCustomerId());

			rs = cstmt.executeQuery();
			if (rs.next()) {
				maxCount[0] = rs.getInt(1);
				maxCount[1] = rs.getInt(2);
				maxCount[2] = rs.getFloat(3);

			}
		} catch (Exception e) {
			logger.error(
					"customer="
							+ systemPerformanceBean.toString()
							+ "\n getMaxCount(systemPerformanceBean,DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return maxCount;
	}

}
