package com.mind.MSP.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.LabelValue;
import com.mind.fw.core.dao.util.DBUtil;

public class MspUtilityDao {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(Mspdao.class);

	/**
	 * Drive the option list from given query.
	 * 
	 * @param sql
	 *            Query the query
	 * @param DataSource
	 *            the ds
	 * 
	 * @return the option list
	 */
	public static ArrayList<LabelValue> getOptionList(String query,
			DataSource ds) {
		ArrayList optionList = new java.util.ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		optionList.add(new com.mind.common.LabelValue("Select", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				optionList
						.add(new LabelValue(rs.getString(2), rs.getString(1)));
			}
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("query=" + query
						+ "\n getOptionList(String, DataSource) - " + e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return optionList;
	} // - end of getOptionList() method

	/**
	 * Gets the customer list.
	 * 
	 * @param query
	 *            the query
	 * @param ds
	 *            the ds
	 * 
	 * @return the customer list
	 */
	public static ArrayList<LabelValue> getCustomerList(String query,
			DataSource ds) {
		ArrayList optionList = new java.util.ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		// optionList.add( new com.mind.common.LabelValue( "Select" , "0") );

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				optionList
						.add(new LabelValue(rs.getString(1), rs.getString(2)));
			}
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("query=" + query
						+ "\n getOptionList(String, DataSource) - " + e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return optionList;
	} // - end of getCustomerList method

}
