package com.mind.MSP.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.ChronicSite;
import com.mind.bean.msp.ChronicSitesBean;
import com.mind.fw.core.dao.util.DBUtil;

public class ChronicSiteDAO {

	private static final Logger logger = Logger.getLogger(ChronicSiteDAO.class);

	public static ArrayList<ChronicSite> getChronicSitesData(
			ChronicSitesBean bean, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ChronicSite> list = new ArrayList<ChronicSite>();
		int i = 0;
		String sql = "select wug_dw_ch_customer, wug_dw_ch_site_name, uptime = convert(decimal(7,3), wug_dw_ch_sla*100.0),"
				+ "credits = convert(int, wug_dw_ch_credits) from dbo.wug_dw_fact_chronic_sites_by_customer "
				+ "where wug_dw_ch_customer = ? order by wug_dw_ch_sla ";

		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, bean.getCustomerName());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				ChronicSite chronicSite = new ChronicSite();
				chronicSite.setSite(rs.getString("wug_dw_ch_site_name"));
				chronicSite.setUpTime(rs.getString("uptime"));
				chronicSite.setCredits(rs.getString("credits"));
				list.add(chronicSite);
				i++;
			}

		} catch (Exception e) {
			logger.error("ChronicSite = " + bean.toString()
					+ "\n getChronicSitesData(ChronicSitesBean, DataSource)", e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return list;
	}

}
