package com.mind.MSP.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.DeviceInfoBean;
import com.mind.bean.msp.TicketInformation;
import com.mind.common.ChangeDateFormat;
import com.mind.common.Util;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.msp.common.EscalationLevel;

/**
 * The Class ManageOutageDao.
 */
public class ManageOutageDao {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(Mspdao.class);

	/**
	 * Gets the device information from database.
	 * 
	 * @param nActiveMonitorStateChangeID
	 *            the n active monitor state change id
	 * @param escalationLevel
	 *            the escalation level
	 * @param ds
	 *            the ds
	 * @return the device information
	 */
	public static DeviceInfoBean getDeviceInformation(
			int nActiveMonitorStateChangeID, int escalationLevel,
			int wugInstance, DataSource ds) {

		DeviceInfoBean deviceInfoBean = new DeviceInfoBean();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call wug_site_device_information(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, nActiveMonitorStateChangeID);
			cstmt.setInt(3, escalationLevel);
			cstmt.setInt(4, wugInstance);
			rs = cstmt.executeQuery();
			if (rs.next()) {
				deviceInfoBean.setCircuitIP(rs.getString("circuit_ip"));
				deviceInfoBean.setCircuitType(rs.getString("circuit_type"));
				deviceInfoBean.setInternetServiceLineNo(rs
						.getString("internet_service_line_no"));
				deviceInfoBean.setIsp(rs.getString("isp"));
				deviceInfoBean.setIspSupportNo(rs.getString("isp_support_no"));
				deviceInfoBean.setJobId(rs.getString("job_id"));
				deviceInfoBean.setLocation(rs.getString("location"));
				deviceInfoBean.setModemType(rs.getString("modem_type"));
				deviceInfoBean
						.setSiteContactNo(rs.getString("site_contact_no"));
				deviceInfoBean.setVOIP(rs.getString("voip"));
				deviceInfoBean.setAccountNew(rs.getString("account"));
				deviceInfoBean.setAccessCode(rs.getString("access_code"));
				deviceInfoBean.setHelpDeskNew(rs.getString("helpdesk"));
				deviceInfoBean.setFax(rs.getString("fax"));

			}
		} catch (Exception e) {
			logger.error(
					"nActiveMonitorStateChangeID="
							+ nActiveMonitorStateChangeID
							+ " "
							+ "\n escalationLevel="
							+ escalationLevel
							+ " "
							+ "\n getDeviceInformation(int, int DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return deviceInfoBean;
	}// - End of getRealTimeState(DataSource)

	/**
	 * Gets the ticket information from database.
	 * 
	 * @param int the n active monitor state change id
	 * @param DataSource
	 *            the ds
	 * @return TicketInformation the ticket information
	 */
	public static TicketInformation getTicketInformation(
			int nActiveMonitorStateChangeID, int wugInstance, DataSource ds) {

		TicketInformation ticketInformation = new TicketInformation();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call wug_ticket_information(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, nActiveMonitorStateChangeID);
			cstmt.setInt(3, wugInstance);
			rs = cstmt.executeQuery();
			if (rs.next()) {
				ticketInformation.setAppendixId(rs.getString("appendix_id"));
				ticketInformation.setJobId(rs.getString("job_id"));
				ticketInformation.setTicketId(rs.getString("ticket_id"));
				ticketInformation
						.setTicketNumber(rs.getString("ticket_number"));
				ticketInformation.setProblemCategory(rs
						.getString("problem_category"));
				ticketInformation.setProbleDescription(rs
						.getString("problem_description"));
				ticketInformation.setIssueOwner(rs.getString("issue_owner"));
				ticketInformation.setRootCause(rs.getString("root_cause"));
				ticketInformation.setCorrectiveAction(rs
						.getString("corrective_action"));
				ticketInformation.setEstimatedEffort(rs
						.getString("estimated_efforts"));
				ticketInformation.setCurrentStatus(rs.getString(
						"wug_ip_current_status").toUpperCase());
				ticketInformation.setLastAction(rs.getString("updated_by"));
				ticketInformation.setBackupCircuitStatus(rs
						.getString("backup_circuit_status"));
			}
		} catch (Exception e) {
			logger.error(
					"nActiveMonitorStateChangeID="
							+ nActiveMonitorStateChangeID
							+ " "
							+ "\n getTicketInformation(int, DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		setLatestInstallNotes(ticketInformation, ds);
		return ticketInformation;
	}// - End of getTicketInformation(int, DataSource)

	/**
	 * Sets the latest install notes.
	 * 
	 * @param ticketInformation
	 *            the ticket information
	 * @param ds
	 *            the ds
	 */
	public static void setLatestInstallNotes(
			TicketInformation ticketInformation, DataSource ds) {
		String latestInstallNotes = "-";
		String secondLatestInstallNotes = "-";
		String latestInstallNotesDate = "-";
		String secondLatestInstallNotesDate = "-";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String jobId = ticketInformation.getJobId();
		String sql = "";
		try {
			conn = ds.getConnection();
			sql = " select convert(varchar(10), lm_in_cdate, 1)+' '+convert(varchar(10),lm_in_cdate,108) as lm_in_udate,"
					+ " lm_in_notes from lm_install_notes  where lm_in_js_id = "
					+ jobId
					+ " and  lm_in_id in"
					+ " (select top 2 lm_in_id from lm_install_notes where lm_in_js_id = "
					+ jobId + " ORDER BY lm_in_udate desc)";

			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				ticketInformation.setInstallNotesPresent(true);
				if (rs.isFirst()) {
					latestInstallNotesDate = rs.getString("lm_in_udate") == null
							|| rs.getString("lm_in_udate").equals("") ? "-"
							: ChangeDateFormat
									.getDateFormat(rs.getString("lm_in_udate"),
											"MM/dd/yyyy hh:mm aaa",
											"MM/dd/yy HH:mm:ss");
					latestInstallNotes = rs.getString("lm_in_notes") == null
							|| rs.getString("lm_in_notes").equals("") ? "-"
							: rs.getString("lm_in_notes");
				}
				if (rs.isLast() && !rs.isFirst()) {
					secondLatestInstallNotesDate = rs.getString("lm_in_udate") == null
							|| rs.getString("lm_in_udate").equals("") ? "-"
							: ChangeDateFormat
									.getDateFormat(rs.getString("lm_in_udate"),
											"MM/dd/yyyy hh:mm aaa",
											"MM/dd/yy HH:mm:ss");
					secondLatestInstallNotes = rs.getString("lm_in_notes") == null
							|| rs.getString("lm_in_notes").equals("") ? "-"
							: rs.getString("lm_in_notes");
				}
			}

			ticketInformation.setLatestInstallNotes(Util
					.replaceToBr(latestInstallNotes));
			ticketInformation.setLatestInstallNotesDate(latestInstallNotesDate);
			ticketInformation.setSecondLatestInstallNotesDate(Util
					.replaceToBr(secondLatestInstallNotesDate));
			ticketInformation
					.setSecondLatestInstallNotes(secondLatestInstallNotes);
		} catch (Exception e) {
			logger.error(
					"sql="
							+ sql
							+ "setLatestInstallNotes(String, DataSource, JobDashboardForm)",
					e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		Connection conn2 = null;
		Statement stmt2 = null;
		ResultSet rs2 = null;
		String sql2 = null;
		try {
			conn2 = ds.getConnection();

			sql2 = "select count(*) as installNotesCount from lm_install_notes where lm_in_js_id="
					+ jobId;
			stmt2 = conn2.createStatement();

			rs2 = stmt2.executeQuery(sql2);
			while (rs2.next()) {
				ticketInformation.setInstallNotesCount(rs2.getInt(1));
			}

		} catch (Exception e) {
			logger.error(
					"sql2="
							+ sql2
							+ "setLatestInstallNotes(String, DataSource, JobDashboardForm)",
					e);
		} finally {

			DBUtil.close(rs2, stmt2);
			DBUtil.close(conn2);
		}
	}

	/**
	 * Resolved by help desk. Call SP to Resolved help desk.
	 * 
	 * @param nActiveMonitorStateChangeID
	 *            the n active monitor state change id
	 * @param ticketInformation
	 *            the ticket information
	 * @param ds
	 *            the ds
	 * @return the int
	 */
	public static int setResolvedByHelpDesk(String nActiveMonitorStateChangeID,
			TicketInformation ticketInformation, String wugInstance,
			DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		int returnFlag = -1;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call wug_help_desk_close_01(?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, nActiveMonitorStateChangeID);
			cstmt.setString(3, ticketInformation.getIssueOwner());
			cstmt.setString(4, ticketInformation.getRootCause());
			cstmt.setString(5, ticketInformation.getCorrectiveAction());
			cstmt.setString(6, ticketInformation.getEstimatedEffort());
			cstmt.setString(7, wugInstance);
			cstmt.setString(8, ticketInformation.getBackupCircuitStatus());
			cstmt.execute();
			returnFlag = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"nActiveMonitorStateChangeID="
							+ nActiveMonitorStateChangeID
							+ "\n "
							+ ticketInformation.toString()
							+ "\n setResolvedByHelpDesk(String, TicketInformation, DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return returnFlag;
	}// End of setNoAction(String, DataSource)

	/**
	 * Sets the po to compete.
	 * 
	 * @param jobId
	 *            the job id
	 * @param ds
	 *            the ds
	 * @return the int
	 */
	public static int setTicketToClosed(String loginUserId,
			TicketInformation ticketInformation, String invoiceNumber,
			DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		int returnFlag = -1;
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call wug_set_job_to_close(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, ticketInformation.getJobId());
			cstmt.setString(3, ticketInformation.getEstimatedEffort());
			cstmt.setString(4, invoiceNumber);
			cstmt.setString(5, loginUserId);
			cstmt.execute();
			returnFlag = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("" + ticketInformation.toString() + "\n jobId="
					+ loginUserId
					+ "\n setPoToCompete(StringDataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return returnFlag;
	}// End of setPoToCompete(String, TicketInformation, DataSource)

	/**
	 * Sets the help desk escalate value from level 1 to level 2.
	 * 
	 * @param nActiveMonitorStateChangeID
	 *            The n active monitor state change id.
	 * @param estimatedEffort
	 *            The estimated effort.
	 * @param ds
	 *            DataSource.
	 * @return int
	 */
	public static int setHelpDeskEscalate(String nActiveMonitorStateChangeID,
			String wugInstance, String estimatedEffort, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int returnFlag = -1;

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call wug_help_desk_escalate_01(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, nActiveMonitorStateChangeID);
			cstmt.setString(3, estimatedEffort);
			cstmt.setString(4, wugInstance);
			cstmt.execute();
			returnFlag = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"nActiveMonitorStateChangeID="
							+ nActiveMonitorStateChangeID
							+ "\n EstimatedEffort="
							+ estimatedEffort
							+ "\n setHelpDeskEscalate(String, String, DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}

		return returnFlag;
	}

	/**
	 * Sets the escalate to msp dispatch.
	 * 
	 * @param loginUserId
	 *            login user id
	 * @param ticketInformation
	 *            ticket information
	 * @param ds
	 *            data source.
	 * @return the int
	 */
	public static int setEscalateToMSPDispatch(String loginUserId,
			TicketInformation ticketInformation, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int returnFlag = -1;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call wug_escalate_ticket_to_msp(?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, ticketInformation.getJobId());
			cstmt.setString(3, ticketInformation.getEstimatedEffort());
			cstmt.setString(4, loginUserId);
			cstmt.setInt(5, EscalationLevel.ESCALATE_TICKET_CRITICALITY);
			cstmt.setInt(6,
					EscalationLevel.ESCALATE_TICKET_CRITICALITY_CATEGORY);
			cstmt.setString(7, EscalationLevel.ESCALATE_TICKET_RESOURCELEVEL);
			cstmt.execute();
			returnFlag = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					""
							+ ticketInformation.toString()
							+ "\n jobId="
							+ loginUserId
							+ "\n setEscalateToMSPDispatch(String, TicketInformation, DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return returnFlag;
	}// End of setEscalateToMSPDispatch(String, TicketInformation, DataSource)

	/**
	 * Sets the Resolution and Actions section details(Efforts, Issue Owner,
	 * Root Cause and Resolution) in database.
	 * 
	 * @param nActiveMonitorStateChangeID
	 *            - the n active monitor state change id
	 * @param ticketInformation
	 *            - the ticket information
	 * @param ds
	 *            - the ds
	 * @return the int - to indicate success/error code.
	 */
	public static int setResolutionActionDetails(
			String nActiveMonitorStateChangeID,
			TicketInformation ticketInformation, String wugInstance,
			DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int returnFlag = -1;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call wug_outage_in_process_characteristics_manage_01(?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, nActiveMonitorStateChangeID);
			cstmt.setString(3, ticketInformation.getIssueOwner());
			cstmt.setString(4, ticketInformation.getRootCause());
			cstmt.setString(5, ticketInformation.getCorrectiveAction());
			cstmt.setString(6, ticketInformation.getEstimatedEffort());
			cstmt.setString(7, wugInstance);
			cstmt.setString(8, ticketInformation.getBackupCircuitStatus());
			cstmt.execute();
			returnFlag = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"nActiveMonitorStateChangeID="
							+ nActiveMonitorStateChangeID
							+ "\n "
							+ "\n setResolutionActionDetails(String, TicketInformation, DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return returnFlag;
	}

	public static int addinstallNotesAndUpdateBackupstatus(String jobId,
			String installationNotes, String loginUser,
			TicketInformation ticketInformation,
			String nActiveMonitorStateChangeID, String wugInstance,
			DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		int returnFlag = -1;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call manage_msp_installation_notes_and_backupStatus(?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, jobId);
			cstmt.setString(3, installationNotes);
			cstmt.setString(4, loginUser);
			cstmt.setString(5, ticketInformation.getBackupCircuitStatus());
			cstmt.setString(6, nActiveMonitorStateChangeID);
			cstmt.setString(7, wugInstance);
			cstmt.execute();
			returnFlag = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"nActiveMonitorStateChangeID="
							+ nActiveMonitorStateChangeID
							+ "\n "
							+ "\n setResolutionActionDetails(String, TicketInformation, DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return returnFlag;

	}

	/**
	 * Gets the escalated outage ticket main id(i.e.,
	 * nActiveMonitorStateChangeID).
	 * 
	 * @param jobId
	 *            - the job id
	 * @param ds
	 *            - the ds
	 * @return the escalated outage ticket main id(i.e.,
	 *         nActiveMonitorStateChangeID).
	 */
	public static int[] getEscalatedOutageTicketMainID(String jobId,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int OutageIds[] = new int[2];
		String sql = "select wug_ip_nActiveMonitorStateChangeID,wug_ip_instance from wug_outages_incidents_in_process inner join lm_ticket on lm_tc_number = wug_ip_ticket_reference"
				+ " where lm_tc_js_id = (?) and wug_ip_escalation_level=2";

		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, jobId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				OutageIds[0] = rs.getInt("wug_ip_nActiveMonitorStateChangeID");
				OutageIds[1] = rs.getInt("wug_ip_instance");
			}
		} catch (Exception e) {
			logger.error(
					"jobId="
							+ jobId
							+ "\n checkEscalatedOutageTicket(String, DataSource) - exception",
					e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}

		return OutageIds;
	}

	public static int checkAndSetEscalatedTicketStatusToClose(String jobId,
			String loginuserid, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int returnFlag = -1;

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?= call wug_escalated_ticket_change_status_to_close(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, jobId);
			cstmt.setString(3,
					EscalationLevel.CLOSE_ESCALATE_TICKET_INVOICE_NUMBER);
			cstmt.setString(4, loginuserid);
			cstmt.execute();

			returnFlag = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"jobId= "
							+ jobId
							+ "\n checkAndSetEscalatedTicketStatusToClose(String, DataSource) - exception",
					e);
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}

		return returnFlag;
	}

	public static int getTotalDispatchCount(int jobId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int countDispatches = 0;
		String sql = "select cntDispatch = count(*) from lx_schedule_element where lx_se_type_id = ? and "
				+ "lx_se_type in ('J', 'IJ')";

		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, jobId);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				countDispatches = rs.getInt(1);
			}
		} catch (Exception e) {
			logger.error(
					"jobId= "
							+ jobId
							+ "\n getTotalDispatchCount(String, DataSource) - exception",
					e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return countDispatches;
	}

	public static int updateNextAction(String nextAction, String chngAction,
			String wugInstance, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int countDispatches = 0;
		String sql = "UPDATE wug_outages_incidents_in_process "
				+ " SET wug_cc_na_id =  " + nextAction
				+ " WHERE wug_ip_nActiveMonitorStateChangeID = " + chngAction
				+ " AND wug_ip_instance = " + wugInstance;

		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			countDispatches = pstmt.executeUpdate();

		} catch (Exception e) {
			logger.error(
					"jobId= "
							+ "\n updateNextAction(String,String,String, DataSource) - exception",
					e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return countDispatches;
	}

	public static int saveEfforts(Double efforts, String ticketId,
			String userId, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int effortId = 0;

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{call lm_help_desk_effort_manage_01(?,?,?,?)}");

			cstmt.setString(1, ticketId);
			cstmt.setDouble(2, efforts);
			cstmt.setString(3, userId);
			cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
			cstmt.execute();

			effortId = cstmt.getInt(4);
		} catch (Exception e) {
			logger.error(
					"ticketId= "
							+ ticketId
							+ "\n saveEfforts(Double,String,String DataSource) - exception",
					e);
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}

		return effortId;
	}
}