package com.mind.MSP.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.SearchHistoryBean;
import com.mind.bean.msp.TicketSearchBean;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class MspSearchHistoryDao.
 */
public class MspSearchHistoryDao {

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(Mspdao.class);

	/**
	 * Gets the search list.
	 * 
	 * @param SearchHistoryBean
	 *            the history bean
	 * @param DataSource
	 *            the ds
	 * 
	 * @return ArrayList<TicketSearchBean> the search list
	 */
	public static ArrayList<TicketSearchBean> getSearchList(
			SearchHistoryBean historyBean, DataSource ds) {

		ArrayList<TicketSearchBean> ticketSearch = new ArrayList<TicketSearchBean>();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int i = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call wug_search_closed_ticket(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, historyBean.getCustomer());
			cstmt.setString(3, historyBean.getFrom());
			cstmt.setString(4, historyBean.getTo());
			cstmt.setString(5, historyBean.getSite());
			rs = cstmt.executeQuery();

			while (rs.next()) {
				TicketSearchBean bean = new TicketSearchBean();
				bean.setWugInstance(rs.getString("wug_instance"));
				bean.setNActiveMonitorStateChangeID(rs
						.getString("wug_cp_nActiveMonitorStateChangeID"));
				bean.setEscalationLevel(rs.getString("wug_cp_escalation_level"));
				bean.setDisplayName(rs.getString("wug_dv_display_name"));
				bean.setEventDay(rs.getString("wug_cp_complete_point"));
				bean.setOutageDuration(rs.getString("wug_cp_outage_duration"));
				bean.setCurrentStatus(rs.getString("wug_cp_current_status"));
				bean.setTicketReference(rs.getString("wug_cp_ticket_reference"));
				bean.setTicketId(rs.getString("ticket_id"));
				bean.setJobId(rs.getString("job_id"));

				ticketSearch.add(i, bean);
				i++;
			}

		} catch (Exception e) {
			logger.error(
					"customer="
							+ historyBean.toString()
							+ "\n getSearchList(String,String,String,String,DataSource) - exception ", e); //$NON-NLS-1$
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return ticketSearch;
	}// - End of getSearchList(String,String,String,String,DataSource)
}
