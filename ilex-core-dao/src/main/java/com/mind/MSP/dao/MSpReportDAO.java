package com.mind.MSP.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.MspReportBean;
import com.mind.bean.msp.MspSearch;
import com.mind.bean.msp.RealTimeState;
import com.mind.fw.core.dao.util.DBUtil;

public class MSpReportDAO {
	private static final Logger logger = Logger.getLogger(MSpReportDAO.class);

	/**
	 * Method : getListofProcessedTickets. Description : This method retrieve
	 * day wise count of processed tickets for Ticket Processed report of MSP.
	 * 
	 * @param bean
	 *            : Reference of MspReportBean.
	 * @param ds
	 *            : Reference of DataSource.
	 * @return : reference of CustomerCredits object.
	 */
	public static MspSearch getListofProcessedTickets(MspReportBean bean,
			DataSource ds) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<RealTimeState> listOfprocessedTickets = new ArrayList<RealTimeState>();
		MspSearch processedTickets = new MspSearch();

		int i = 0;
		int countTotalTickets = 0;

		String sql = "select complete_point = convert(varchar(8), wug_cp_complete_point, 1), cnt = count(*) "
				+ "from dbo.wug_outages_incidents_closed where wug_cp_complete_point between ? and cast (? as datetime) and wug_cp_escalation_level > 0 "
				+ "group by convert(varchar(8), wug_cp_complete_point, 1) "
				+ "order by convert(varchar(8), wug_cp_complete_point, 1)";

		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, bean.getFromDate());
			pstmt.setString(2, (bean.getToDate() + " 23:59:59"));
			rs = pstmt.executeQuery();

			while (rs.next()) {
				RealTimeState ticketProcessed = new RealTimeState();

				ticketProcessed.setEventDay(rs.getString("complete_point"));
				ticketProcessed.setEventCount(rs.getString("cnt"));
				listOfprocessedTickets.add(i, ticketProcessed);

				countTotalTickets += Integer.parseInt(ticketProcessed
						.getEventCount());
				i++;
			}

			processedTickets.setSearchList(listOfprocessedTickets);
			processedTickets.setTotal(String.valueOf(countTotalTickets));

		} catch (Exception e) {
			logger.error(
					"MSPProcessedTickets = "
							+ bean.toString()
							+ "\n searchDayWiseProcessedTickets(MspReportBean, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}

		return processedTickets;
	}

}
