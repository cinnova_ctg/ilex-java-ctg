package com.mind.MSP.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.MspReportBean;
import com.mind.bean.msp.MspSearch;
import com.mind.bean.msp.RealTimeState;
import com.mind.fw.core.dao.util.DBUtil;

public class CustomerCreditDAO {
	private static final Logger logger = Logger
			.getLogger(CustomerCreditDAO.class);

	public static MspSearch getCustomerCreditSearch(MspReportBean bean,
			DataSource ds) {
		String sql = "";

		if (bean.getReportType().equalsIgnoreCase("byDay")) {
			sql = "select wug_gn_ilex_customer_name, wug_dv_display_name, "
					+ "wug_cr_dp_date = isnull(convert(varchar(10), wug_cr_dp_date, 101),''), wug_cr_duration "
					+ "from wug_device_customer_credit inner join wug_device on wug_cr_dp_dv_id = wug_dv_id "
					+ "inner join wug_group_name on wug_dv_gn_id = wug_gn_id where wug_cr_dp_date between ? and cast(? as datetime) "
					+ "and wug_gn_msa_id = ?";
		} else if (bean.getReportType().equalsIgnoreCase("summary")) {
			sql = "select wug_dv_display_name, cnt = count(*)"
					+ "from wug_device_customer_credit inner join wug_device on wug_cr_dp_dv_id = wug_dv_id "
					+ "inner join wug_group_name on wug_dv_gn_id = wug_gn_id where wug_cr_dp_date between ? and cast(? as datetime) "
					+ "and wug_gn_msa_id = ? group by wug_dv_display_name order by wug_dv_display_name";
		}

		MspSearch credits = getCreditSearchDetails(bean, sql, ds);
		return credits;
	}

	/**
	 * Gets data(Site, date and duration) for Customer Credit search page.
	 * 
	 * @param bean
	 *            : reference of MspReportBean class
	 * @param ds
	 *            : DataSource.
	 * 
	 * @return CustomerCredits: reference of CustomerCredits class that holds
	 *         total count of matching records and its list.
	 */
	public static MspSearch getCreditSearchDetails(MspReportBean bean,
			String query, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<RealTimeState> searchCreditList = new ArrayList<RealTimeState>();
		MspSearch credits = new MspSearch();
		int i = 0;
		int total = 0;

		if (!query.equals("")) {
			try {
				conn = ds.getConnection();
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, bean.getFromDate());
				pstmt.setString(2, (bean.getToDate() + " 23:59:59"));
				pstmt.setString(3, bean.getCustomerId());

				logger.debug("sql ------- " + query);
				logger.debug("fromDate ------- " + bean.getFromDate());
				logger.debug("toDate ------- "
						+ (bean.getToDate() + " 23:59:59"));
				logger.debug("customerId ------- " + bean.getCustomerId());
				logger.debug("reportType ------- " + bean.getReportType());

				rs = pstmt.executeQuery();
				while (rs.next()) {
					RealTimeState custCredit = new RealTimeState();
					custCredit.setDisplayName(rs
							.getString("wug_dv_display_name"));

					if (bean.getReportType().equalsIgnoreCase("byDay")) {
						custCredit.setEventDay(rs.getString("wug_cr_dp_date"));
						custCredit.setOutageDuration(rs
								.getString("wug_cr_duration"));
					} else if (bean.getReportType().equalsIgnoreCase("summary")) {
						custCredit.setEventCount(rs.getString("cnt"));
						total += Integer.parseInt(rs.getString("cnt"));
					}

					searchCreditList.add(i, custCredit);
					i++;
				}

				credits.setSearchList(searchCreditList);

				if (bean.getReportType().equalsIgnoreCase("byDay")) {
					credits.setTotal(String.valueOf(searchCreditList.size()));
				} else if (bean.getReportType().equalsIgnoreCase("summary")) {
					credits.setTotal(String.valueOf(total));
				}

			} catch (Exception e) {
				logger.error(
						"CustomerCredits = "
								+ bean.toString()
								+ "\n getCustomerCreditSearch(MspReportBean, DataSource)",
						e);

			} finally {
				DBUtil.close(rs, pstmt);
				DBUtil.close(conn);
			}
		} else {
			credits.setSearchList(searchCreditList);
			credits.setTotal(String.valueOf(searchCreditList.size()));
		}

		return credits;
	}

}
