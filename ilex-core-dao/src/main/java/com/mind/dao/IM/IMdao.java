package com.mind.dao.IM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.im.InvoiceJobDetailBean;
import com.mind.common.Decimalroundup;
import com.mind.common.Util;

public class IMdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IMdao.class);

	public static ArrayList getJobDetailList(String id, String type,
			String querystring, String filteropt, String partner_name,
			String powo_number, String from_date, String to_date,
			String invoiceno, String invoice_Flag, String exportStatus,
			DataSource ds) {

		Connection connect = null;
		CallableStatement cstmt1 = null;
		ResultSet rs = null;
		int retval = -1;
		ArrayList valuelist = new ArrayList();

		if (querystring.equals("")) {
			querystring = "order by jobname";
		}

		try {
			int i = 0;
			connect = ds.getConnection();
			cstmt1 = connect
					.prepareCall("{?=call dbo.lp_inv_job_tab(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
			cstmt1.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt1.setString(2, id);
			cstmt1.setString(3, type);
			cstmt1.setString(4, querystring);
			cstmt1.setString(5, filteropt);
			cstmt1.setString(6, partner_name);
			cstmt1.setString(7, powo_number);
			cstmt1.setString(8, from_date);
			cstmt1.setString(9, to_date);
			cstmt1.setString(10, invoiceno);
			cstmt1.setString(11, invoice_Flag);
			cstmt1.setString(12, exportStatus);
			rs = cstmt1.executeQuery();

			while (rs.next()) {
				InvoiceJobDetailBean ijdBean = new InvoiceJobDetailBean();
				ijdBean.setMsaId(rs.getString("lp_mm_id"));
				ijdBean.setMsaName(rs.getString("lo_ot_name"));
				ijdBean.setAppendixId(rs.getString("lm_js_pr_id"));
				ijdBean.setAppendixName(rs.getString("lx_pr_title"));
				ijdBean.setJobId(rs.getString("lm_js_id"));
				ijdBean.setJobName(rs.getString("jobname"));

				ijdBean.setExtendedprice(Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_total_price"), 2));
				ijdBean.setStatus(rs.getString("lm_js_status"));
				ijdBean.setActualstartdate(rs.getString("lx_se_start"));
				ijdBean.setActualcompletedate(rs.getString("lx_se_end"));
				ijdBean.setSubmitteddate(rs.getString("submitted_date"));
				ijdBean.setJob_type(rs.getString("lm_js_prj_status"));
				ijdBean.setPowo(rs.getString("lm_po_number"));

				ijdBean.setPoAuthorizedTotal(Decimalroundup.twodecimalplaces(
						rs.getFloat("pwo_authorised_total_cost"), 2));
				ijdBean.setHid_jobid(rs.getString("lm_js_id"));
				ijdBean.setInvoice_no(rs.getString("lm_js_invoice_no"));
				ijdBean.setAssociated_date(rs
						.getString("lm_js_associated_date"));
				ijdBean.setCreatedby(rs.getString("job_created_by"));

				ijdBean.setSiteNo(rs.getString("lm_si_number"));
				ijdBean.setClosed_date(rs.getString("lm_js_closed_date"));
				ijdBean.setRequest_received_date(rs
						.getString("lm_tc_requested_date"));
				ijdBean.setRequest_received_time(rs
						.getString("lm_tc_requested_time"));

				ijdBean.setPartner_name(rs.getString("lm_po_partner_name"));
				ijdBean.setTicket_id(rs.getString("lm_tc_id"));
				ijdBean.setOut_of_scope_activity_flag(rs
						.getString("out_of_scope_activity_flag"));
				ijdBean.setCustomerref(rs.getString("lm_tc_cust_reference"));
				ijdBean.setExportStatus(rs.getString("lm_js_export_status"));
				ijdBean.setGpImportStatus(rs.getString("in_gi_import_status"));
				ijdBean.setGpImportId(rs.getString("in_gi_id"));
				valuelist.add(i, ijdBean);
				i++;
			}
		} catch (Exception e) {
			logger.error(
					"getJobDetailList(String, String, String, String, String, String, String, String, String, String, String, DataSource)",
					e);
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobDetailList(String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt1 != null)
					cstmt1.close();
				cstmt1 = null;
			} catch (Exception e) {
				logger.warn(
						"getJobDetailList(String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (connect != null)
					connect.close();
				connect = null;
			} catch (Exception e) {
				logger.warn(
						"getJobDetailList(String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static ArrayList getJobSearchList(String poNumber, String invNumber,
			String partnerName, String customerName, String fromDate,
			String toDate, DataSource ds) {

		Connection connect = null;
		CallableStatement cstmt1 = null;
		ResultSet rs = null;
		int retval = -1;
		ArrayList valuelist = new ArrayList();
		DecimalFormat dfcur = new DecimalFormat("$#,###,##0.00");
		/*
		 * System.out.println("2 "+ poNumber ); System.out.println("3 "+
		 * invNumber ); System.out.println("4 "+ partnerName );
		 * System.out.println("5 "+ customerName ); System.out.println("6 "+
		 * fromDate); System.out.println("7 "+ toDate);
		 */
		try {
			int i = 0;
			connect = ds.getConnection();
			cstmt1 = connect
					.prepareCall("{?=call dbo.lp_inv_job_search_tab(?, ?, ?, ?, ?, ?) }");
			cstmt1.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt1.setString(2, poNumber);
			cstmt1.setString(3, invNumber);
			cstmt1.setString(4, partnerName);
			cstmt1.setString(5, customerName);
			cstmt1.setString(6, fromDate);
			cstmt1.setString(7, toDate);
			rs = cstmt1.executeQuery();

			while (rs.next()) {
				InvoiceJobDetailBean ijdBean = new InvoiceJobDetailBean();
				ijdBean.setMsaId(rs.getString("msa_id"));
				ijdBean.setMsaName(rs.getString("customer_name"));
				ijdBean.setAppendixId(rs.getString("appendix_id"));
				ijdBean.setAppendixName(rs.getString("project_name"));
				ijdBean.setJobId(rs.getString("job_id"));
				ijdBean.setTicket_id(rs.getString("ticket_id"));
				ijdBean.setJobName(rs.getString("job_name"));
				ijdBean.setExtendedprice(dfcur.format(rs
						.getFloat("job_extended_price")));
				ijdBean.setActualstartdate(rs.getString("job_actual_start"));
				ijdBean.setActualcompletedate(rs.getString("job_actual_end"));
				ijdBean.setSubmitteddate(rs.getString("job_submitted_date"));
				ijdBean.setJob_type(rs.getString("job_status"));
				ijdBean.setClosed_date(rs.getString("job_closed_date"));
				ijdBean.setOut_of_scope_activity_flag(rs
						.getString("out_of_scope_activity_flag"));

				if (!Util.isNullOrBlank(invNumber)
						|| !Util.isNullOrBlank(customerName)) {
					ijdBean.setExportStatus(rs.getString("job_export_status"));
					ijdBean.setCustomerref(rs
							.getString("job_customer_reference"));
					ijdBean.setInvoice_no(rs.getString("job_inv_no"));
					ijdBean.setAssociated_date(rs.getString("job_invoice_date"));
					ijdBean.setCreatedby(rs.getString("job_owner"));

				}
				if (!Util.isNullOrBlank(poNumber)
						|| !Util.isNullOrBlank(partnerName)) {
					ijdBean.setPoAuthorizedTotal(dfcur.format(rs
							.getFloat("po_authorised_total_cost")));
					ijdBean.setPowo(rs.getString("po_number"));
					ijdBean.setApproved_total(dfcur.format(rs
							.getFloat("inv_approved_total")));
					ijdBean.setPaid_status(rs.getString("inv_status"));
					ijdBean.setPaid_date(rs.getString("inv_paid_date"));
					ijdBean.setCheck_number(rs.getString("inv_check_number"));
					ijdBean.setPartner_name(rs.getString("partner_name"));
				}
				valuelist.add(i, ijdBean);
				i++;
			}
		} catch (Exception e) {
			logger.error(
					"getJobDetailList(String, String, String, String, String, String, String, String, String, String, String, DataSource)",
					e);
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobDetailList(String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt1 != null)
					cstmt1.close();
				cstmt1 = null;
			} catch (Exception e) {
				logger.warn(
						"getJobDetailList(String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (connect != null)
					connect.close();
				connect = null;
			} catch (Exception e) {
				logger.warn(
						"getJobDetailList(String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static String getMSAId(String appendixid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String msaid = "";

		if (appendixid == null || appendixid.equals(""))
			appendixid = "0";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lx_pr_mm_id from lx_appendix_main where lx_pr_id = "
					+ appendixid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				msaid = rs.getString("lx_pr_mm_id");
			}

		} catch (Exception e) {
			logger.error("getMSAId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMSAId(String, DataSource) - Error occured during getting Appendix name  "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn("getMSAId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn("getMSAId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn("getMSAId(String, DataSource) - exception ignored",
						e);
			}

		}
		return msaid;
	}

	public static String getOrganizationTopId(String msaid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String lo_ot_id = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lp_mm_ot_id from lp_msa_main where lp_mm_id = "
					+ msaid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				lo_ot_id = rs.getString("lp_mm_ot_id");
			}

		} catch (Exception e) {
			logger.error("getOrganizationTopId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOrganizationTopId(String, DataSource) - Error occured during getting Organization Top Id  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getOrganizationTopId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getOrganizationTopId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getOrganizationTopId(String, DataSource) - exception ignored",
						e);
			}

		}
		return lo_ot_id;
	}

	public static String getAppendixId(String jobid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String appendixid = "";
		java.util.Date dateIn = new java.util.Date();

		if (jobid.equals("")) {
			jobid = "0";
		}
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_js_pr_id from lm_job where lm_js_id = "
					+ jobid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				appendixid = rs.getString("lm_js_pr_id");
			}

		} catch (Exception e) {
			logger.error("getAppendixId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixId(String, DataSource) - Error occured during getting Appendix name  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAppendixId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixId(String, DataSource) - exception ignored",
						e);
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("getAppendixId(String, DataSource) - **** getAppendixId :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return appendixid;
	}

	public static String getAppendixIdforactivity(String activityid,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String appendixid = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select dbo.func_cc_getappendixidforactivity( '"
					+ activityid + "' )";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				appendixid = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("getAppendixIdforactivity(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixIdforactivity(String, DataSource) - Error occured during getting AppendixIdforactivity");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAppendixIdforactivity(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixIdforactivity(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixIdforactivity(String, DataSource) - exception ignored",
						e);
			}

		}
		return appendixid;
	}

	public static int Status(String Job_Id, String Type, String status,
			String indexvalue, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		String Id = "";
		int val = 0;
		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.cc_status_change(?, ?, ?, ? ) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(Job_Id));
			cstmt.setString(3, Type);
			cstmt.setString(4, status);
			cstmt.setString(5, indexvalue);
			cstmt.execute();
			val = cstmt.getInt(1);
		} catch (SQLException e) {
			logger.error("Status(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("Status(String, String, String, String, DataSource) - Exception caught in changing status"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"Status(String, String, String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("Status(String, String, String, String, DataSource) - Exception caught in changing status"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"Status(String, String, String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("Status(String, String, String, String, DataSource) - Exception caught in changing status"
							+ sql);
				}
			}

		}
		return val;
	}

	public static int setGPImportStatusToComplete(String gpId, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;

		int retvalue = 1;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "UPDATE in_gp_sop_export_import_general "
					+ " SET in_gi_import_status = 3 WHERE in_gi_id = " + gpId;
			stmt.execute(sql);

		} catch (Exception e) {
			logger.error("setGPImportStatusToComplete((String, DataSource)", e);

			retvalue = 0;
			if (logger.isDebugEnabled()) {
				logger.debug("setGPImportStatusToComplete((String, DataSource) - Error occured in  "
						+ e);
			}
		}

		finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"setGPImportStatusToComplete((String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"setGPImportStatusToComplete((String, DataSource) - exception ignored",
						e);
			}

		}
		return retvalue;
	}

	public static int UpdateInvoiceJob(String job_id, String invoice_no,
			String associated_date, String customerref, String userId,
			DataSource ds) {
		int retval;
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		retval = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_inv_job_manage_01(?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, job_id);
			cstmt.setString(3, invoice_no);
			cstmt.setString(4, associated_date);
			cstmt.setString(5, customerref);
			cstmt.setString(6, userId);

			cstmt.execute();
			retval = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"UpdateInvoiceJob(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("UpdateInvoiceJob(String, String, String, String, DataSource) - Error occured during Update Invoice Job "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception exception1) {
				logger.warn(
						"UpdateInvoiceJob(String, String, String, String, DataSource) - exception ignored",
						exception1);
			}
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception exception2) {
				logger.warn(
						"UpdateInvoiceJob(String, String, String, String, DataSource) - exception ignored",
						exception2);
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception exception3) {
				logger.warn(
						"UpdateInvoiceJob(String, String, String, String, DataSource) - exception ignored",
						exception3);
			}
		}
		return retval;
	}

	/*
	 * public static String getCustomerReference(String jobid, DataSource ds) {
	 * 
	 * Connection conn=null; Statement stmt=null; ResultSet rs=null; String
	 * customerreference = "";
	 * 
	 * try{
	 * 
	 * conn=ds.getConnection(); stmt=conn.createStatement();
	 * 
	 * String
	 * sql="select lm_tc_cust_reference from lm_ticket where lm_tc_js_id = "
	 * +jobid;
	 * 
	 * 
	 * rs = stmt.executeQuery(sql);
	 * 
	 * if(rs.next()) {
	 * 
	 * customerreference = rs.getString("lm_tc_cust_reference"); }
	 * 
	 * } catch(Exception e){
	 * System.out.println("Error occured during getting getCustomerReference  "
	 * ); }
	 * 
	 * finally { try { if ( rs!= null ) { rs.close(); rs = null; } }
	 * 
	 * catch( Exception e ) {}
	 * 
	 * 
	 * try { if ( stmt!= null ) stmt.close(); stmt = null; } catch( Exception e
	 * ) {}
	 * 
	 * 
	 * try { if ( conn!= null) conn.close(); conn = null; } catch( Exception e )
	 * {}
	 * 
	 * } return customerreference; }
	 */

	/**
	 * @author amitm
	 * @param jobId
	 * @param ds
	 * @return
	 */

	public static String getTicketId(String jobId, DataSource ds) {

		Connection conne = null;
		Statement stmt = null;
		ResultSet rs = null;
		String ticketid = "";

		try {
			conne = ds.getConnection();
			stmt = conne.createStatement();

			String sql = "select lm_tc_id from lm_ticket where lm_tc_js_id = "
					+ jobId;

			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				ticketid = rs.getString("lm_tc_id");
			}

		} catch (Exception e) {
			logger.error("getTicketId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTicketId(String, DataSource) - Error occured during getting Ticket Id  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
					rs = null;
				}
			}

			catch (Exception e) {
				logger.warn(
						"getTicketId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getTicketId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conne != null)
					conne.close();
			} catch (Exception e) {
				logger.warn(
						"getTicketId(String, DataSource) - exception ignored",
						e);
			}

		}
		return ticketid;
	}

	public static String getJobSiteNumber(String jobId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String siteNumber = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_si_number = isnull(lm_si_number,'') from lm_job inner join lm_site_detail on lm_js_si_id = lm_si_id where lm_js_id = "
					+ jobId;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				siteNumber = rs.getString("lm_si_number");
			}

		} catch (Exception e) {
			logger.error("getJobSiteNumber(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobSiteNumber(String, DataSource) - Error occured during getting Site Number for subject line of PO/WO email: "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobSiteNumber(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobSiteNumber(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobSiteNumber(String, DataSource) - exception ignored",
						e);
			}

		}
		return siteNumber;
	}

public static String getJobSiteCity(String jobId, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String siteCity = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_si_city = isnull(lm_si_city,'') from lm_job inner join lm_site_detail on lm_js_si_id = lm_si_id where lm_js_id = "
					+ jobId;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				siteCity = rs.getString("lm_si_city");
			}

		} catch (Exception e) {
			logger.error("getJobSiteCity(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobSiteCity(String, DataSource) - Error occured during getting Site City for subject line of PO/WO email: "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobSiteCity(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobSiteCity(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobSiteCity(String, DataSource) - exception ignored",
						e);
			}

		}
		return siteCity;
	}
}
