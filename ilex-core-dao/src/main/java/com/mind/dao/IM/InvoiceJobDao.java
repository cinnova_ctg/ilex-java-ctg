package com.mind.dao.IM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.im.InvoicedJobDetails;
import com.mind.fw.core.dao.util.DBUtil;

public class InvoiceJobDao {
	private static final Logger logger = Logger.getLogger(InvoiceJobDao.class);

	/**
	 * Name - getDbSystemDate. Description - This method retrieves database
	 * current date and time.
	 * 
	 * @param ds
	 *            - DataSource.
	 * @return String.
	 */
	public static String getDbSystemDate(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String dbSystemDate = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select systemDate = convert(varchar(10), cast(dbo.func_getSystemTime(null) as datetime), 101)";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				dbSystemDate = rs.getString("systemDate");
			}
		} catch (SQLException e) {
			logger.error(
					"getDbSystemDate(DataSource) - Exception in retrieving database current date:",
					e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return dbSystemDate;
	}

	/**
	 * Name - setCommissionPOInvoice. Description - This method calls
	 * 'lp_list_invoiced_closed_job' db function for displaying list of closed
	 * jobs on 'Commission PO Completion' page.
	 * 
	 * @param invoiceNo
	 * @param ds
	 * @return ArrayList<InvoicedJobDetails>.
	 */
	public static ArrayList<InvoicedJobDetails> getClosedInvoicedJobs(
			String invoiceNo, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<InvoicedJobDetails> invoicedJobs = new ArrayList<InvoicedJobDetails>();
		String sql = "select * from lp_list_invoiced_closed_job(?) order by customer_name, project_name, job_id";
		int i = 0;
		DecimalFormat dfcurDollar = new DecimalFormat("###0.00");
		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, invoiceNo);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				InvoicedJobDetails invoicedJobDetails = new InvoicedJobDetails();
				invoicedJobDetails.setMsaName(rs.getString("customer_name"));
				invoicedJobDetails
						.setAppendixName(rs.getString("project_name"));
				invoicedJobDetails.setJobId(rs.getString("job_id"));
				invoicedJobDetails.setJobTitle(rs.getString("job_name"));
				invoicedJobDetails.setAssociatedDate(rs
						.getString("job_invoice_date"));
				invoicedJobDetails.setClosedDate(rs
						.getString("job_closed_date"));
				invoicedJobDetails.setRevenue(dfcurDollar.format(rs
						.getFloat("job_extended_price")) + "");
				invoicedJobs.add(i, invoicedJobDetails);
				i++;
			}
		} catch (Exception e) {
			logger.error(
					"getClosedInvoicedJobs(String, DataSource) - Error in retrieving closed job list for commission PO invoice page. ",
					e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return invoicedJobs;
	}

	/**
	 * Name - setCommissionPOInvoice. Description - This method calls
	 * 'lp_complete_bulk_job_commission_po_01' procedure to complete quarterly
	 * commission PO(s) for all listed closed invoiced jobs.
	 * 
	 * @param jobIdString
	 *            - String of jobIds.
	 * @param paymentReceieveDate
	 *            - Payment Receive Date.
	 * @param userId
	 *            - Login Id.
	 * @param ds
	 *            - DataSource.
	 * @return - success/failure code (int).
	 */
	public static int setCommissionPOInvoice(String jobIdString,
			String paymentReceieveDate, String userId, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int retInvoiceCommFlag = -1;

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lp_complete_bulk_job_commission_po_01(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, jobIdString);
			cstmt.setString(3, paymentReceieveDate);
			cstmt.setInt(4, Integer.parseInt(userId));
			cstmt.execute();
			retInvoiceCommFlag = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"setCommissionPOInvoice(String, String, String, DataSource - Error occurred while completing quarterly commission PO(s): )",
					e);
			e.printStackTrace();
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}

		return retInvoiceCommFlag;
	}
}
