package com.mind.dao.MasterSite;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.MasterSite.MasterSiteDeviceAttributeBean;
import com.mind.bean.MasterSite.MasterSiteInformationBean;
import com.mind.common.LabelValue;

public class MasterSiteDAO {

	private static final Logger logger = Logger.getLogger(MasterSiteDAO.class);

	public static List<LabelValue> getAllClientsList(DataSource ds) {
		List<LabelValue> clientsList = new ArrayList<LabelValue>();
		String sql = "select lp_mm_id, lo_ot_name  FROM dbo.lp_msa_main "
				+ " INNER JOIN dbo.lo_organization_top ON dbo.lp_msa_main.lp_mm_ot_id = dbo.lo_organization_top.lo_ot_id "
				+ " ORDER BY lo_ot_name";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		LabelValue lb = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				lb = new LabelValue();
				lb.setLabel(rs.getString("lo_ot_name"));
				lb.setValue(rs.getString("lp_mm_id"));
				clientsList.add(lb);
			}
		} catch (Exception e) {
			logger.error(" getAllClientsList " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getAllClientsList " + e.getMessage());
			}
		}
		return clientsList;
	}

	public static List<LabelValue> getAllSitesByClient(String mmId,
			DataSource ds) {
		List<LabelValue> sitesList = new ArrayList<LabelValue>();
		StringBuilder sql = new StringBuilder(" Select lp_sl_id,lp_si_number ");
		sql.append(" from lp_site_list where lp_md_mm_id=" + mmId
				+ " order by lp_si_number ");
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		LabelValue lb = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql.toString());

			while (rs.next()) {
				lb = new LabelValue();
				lb.setLabel(rs.getString("lp_si_number"));
				lb.setValue(rs.getString("lp_sl_id"));
				sitesList.add(lb);
			}
		} catch (Exception e) {
			logger.error(" getAllSitesByClient " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getAllSitesByClient " + e.getMessage());
			}
		}
		return sitesList;
	}

	public static String[] getSiteDetails(String siteId, DataSource ds) {

		String[] siteDetails = new String[5];
		String sql = "SELECT lp_si_number, lp_si_address, lp_si_city, lp_si_state, lp_si_zip_code, "
				+ " isnull(lp_si_phone_no,'') as lp_si_phone_no, isnull(lp_si_pri_email_id,'') as lp_si_pri_email_id, isnull(lp_si_pri_poc, '') as lp_si_pri_poc "
				+ " FROM lp_site_list WHERE lp_sl_id ="
				+ siteId
				+ " order by lp_si_number";

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				siteDetails[0] = rs.getString("lp_si_number");
				siteDetails[1] = rs.getString("lp_si_address");
				if (rs.getString("lp_si_city") != null
						&& !rs.getString("lp_si_city").equals("")) {
					siteDetails[1] = siteDetails[1] + ", "
							+ rs.getString("lp_si_city");
				}
				if (rs.getString("lp_si_state") != null
						&& !rs.getString("lp_si_state").equals("")) {
					siteDetails[1] = siteDetails[1] + ", "
							+ rs.getString("lp_si_state");
				}
				if (rs.getString("lp_si_zip_code") != null
						&& !rs.getString("lp_si_zip_code").equals("")) {
					siteDetails[1] = siteDetails[1] + ", "
							+ rs.getString("lp_si_zip_code");
				}

				siteDetails[2] = rs.getString("lp_si_phone_no");
				siteDetails[3] = rs.getString("lp_si_pri_email_id");
				siteDetails[4] = rs.getString("lp_si_pri_poc");
			}
		} catch (Exception e) {
			logger.error(" getSiteDetails " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getSiteDetails " + e.getMessage());
			}
		}
		return siteDetails;
	}

	public static ArrayList<String[]> getSitesByAddress(String city,
			String state, String zip, String address, DataSource ds) {
		String[] st = null;
		ArrayList<String[]> sitesList = new ArrayList<String[]>();
		StringBuffer sql = new StringBuffer(
				"SELECT lp_sl_id, lp_md_mm_id, lp_si_name, lp_si_number, "
						+ " lp_si_city, lp_si_state, lp_si_zip_code, lp_si_address, lp_si_end_customer "
						+ " FROM lp_site_list   " + " WHERE lp_si_city LIKE '"
						+ city + "%' " + " AND lp_si_state LIKE '" + state
						+ "%' " + " AND lp_si_zip_code LIKE '" + zip + "%' "
						+ " AND lp_si_address LIKE '%" + address + "%' "
						+ " ORDER BY lp_si_number, lp_si_name");

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql.toString());

			while (rs.next()) {
				st = new String[9];
				st[0] = String.valueOf(rs.getString("lp_sl_id"));
				st[1] = String.valueOf(rs.getString("lp_md_mm_id"));
				st[2] = String.valueOf(rs.getString("lp_si_name"));
				st[3] = String.valueOf(rs.getString("lp_si_number"));
				st[4] = String.valueOf(rs.getString("lp_si_city"));
				st[5] = String.valueOf(rs.getString("lp_si_state"));
				st[6] = String.valueOf(rs.getString("lp_si_zip_code"));
				st[7] = String.valueOf(rs.getString("lp_si_address"));
				st[8] = String.valueOf(rs.getString("lp_si_end_customer"));

				sitesList.add(st);
			}
		} catch (Exception e) {
			logger.error(" getSitesByAddress " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getSiteDetails " + e.getMessage());
			}
		}
		return sitesList;
	}

	public static String[] getSiteDetailsBySiteNumber(String siteNumber,
			String mmId, DataSource ds) {
		String[] st = null;
		StringBuffer sql = new StringBuffer(
				"SELECT lp_sl_id, lp_md_mm_id   FROM lp_site_list "
						+ " LEFT JOIN wug_device ON wug_dv_lp_si_id = lp_sl_id "
						+ "WHERE (lp_si_number LIKE ('%"
						+ (siteNumber.replace("_", "[_]").trim())
						+ "%') OR wug_dv_display_name LIKE '%"
						+ siteNumber.replace("_", "[_]").trim() + "%' ) ");
		// + " and wug_dv_display_name is not null "
		if (!mmId.equals("0"))
			sql.append(" AND lp_md_mm_id =  " + mmId);
		sql.append(" ORDER BY lp_si_number, wug_dv_display_name ");

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql.toString());

			if (rs.next()) {
				st = new String[2];
				st[0] = String.valueOf(rs.getInt("lp_sl_id"));
				st[1] = String.valueOf(rs.getInt("lp_md_mm_id"));
			}
		} catch (Exception e) {
			logger.error(" getSiteDetails " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getSiteDetails " + e.getMessage());
			}
		}
		return st;
	}

	public static List<String> getAllSSiteBySiteNumber(String siteNumber,
			DataSource ds) {
		List<String> st = new ArrayList<String>();
		String sql = "SELECT top 20 lp_si_number  +' <font color=''gray''><br />'+lp_si_address +'</font>'  lp_si_number "
				+ " FROM lp_site_list "
				+ " LEFT JOIN wug_device ON wug_dv_lp_si_id = lp_sl_id "
				+ " WHERE (wug_dv_display_name LIKE '"
				+ siteNumber.replace("_", "[_]").trim()
				+ "%' OR lp_si_number LIKE '"
				+ siteNumber.replace("_", "[_]").trim() + "%' ) "
				// + " and wug_dv_display_name is not null "
				+ " ORDER BY lp_si_number, wug_dv_display_name ";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				st.add(rs.getString("lp_si_number"));
			}
		} catch (Exception e) {
			logger.error(" getAllSSiteBySiteNumber " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getAllSSiteBySiteNumber " + e.getMessage());
			}
		}
		return st;
	}

	public static List<String> getAllSSiteBySiteAddress(String siteNumber,
			DataSource ds) {
		List<String> st = new ArrayList<String>();
		String sql = "SELECT top 20 lp_si_number  +' <font color=''gray''><br />'+lp_si_address +'</font>' lp_si_number "
				+ " FROM lp_site_list   "
				+ " WHERE lp_si_address LIKE '"
				+ siteNumber
				+ "%' AND lp_si_address IS NOT NULL AND lp_si_address <> '' "
				+ " ORDER BY lp_si_address ";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				st.add(rs.getString("lp_si_number"));
			}
		} catch (Exception e) {
			logger.error(" getAllSSiteBySiteNumber " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getAllSSiteBySiteNumber " + e.getMessage());
			}
		}
		return st;
	}

	public static List<MasterSiteDeviceAttributeBean> getDeviceAttributeBysite(
			String siteId, DataSource ds) {
		MasterSiteDeviceAttributeBean masterSiteDeviceAttributeBean = null;
		List<MasterSiteDeviceAttributeBean> masterSiteDeviceAttributeList = new ArrayList<MasterSiteDeviceAttributeBean>();
		String sql = "SELECT wug_gn_pr_id, wug_dv_display_name, isnull(wug_nd_sDisplayName_device, '') as wug_nd_sDisplayName_device,   "
				+ " isnull(wug_nd_sNetworkName, '') as wug_nd_sNetworkName, isnull(wug_nd_sNetworkAddress, '') as wug_nd_sNetworkAddress, "
				+ " isnull(wug_dd_circuit_ip, '') as wug_dd_circuit_ip, isnull(wug_dd_circuit_type, '') as wug_dd_circuit_type,  "
				+ " isnull(wug_dd_internet_service_line_number, '') as wug_dd_internet_service_line_number,  "
				+ " isnull(wug_dd_isp, '') as wug_dd_isp, isnull(wug_dd_isp_support_number, '') as wug_dd_isp_support_number, "
				+ " isnull(wug_dd_location, '') as wug_dd_location,   "
				+ " isnull(wug_dd_modem_type, '') as wug_dd_modem_type, isnull(wug_dd_site_contact_phone, '') as wug_dd_site_contact_phone,  "
				+ " isnull(wug_dd_account, '') AS wug_dd_account   "
				+ " FROM master_site_device_attribute_details  "
				+ " WHERE wug_dv_lp_si_id LIKE " + siteId;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				masterSiteDeviceAttributeBean = new MasterSiteDeviceAttributeBean();
				masterSiteDeviceAttributeBean.setAccount(rs
						.getString("wug_dd_account"));
				masterSiteDeviceAttributeBean.setCircuitIP(rs
						.getString("wug_dd_circuit_ip"));
				masterSiteDeviceAttributeBean.setCircuitType(rs
						.getString("wug_dd_circuit_type"));
				masterSiteDeviceAttributeBean.setInternetLineNo(rs
						.getString("wug_dd_internet_service_line_number"));
				masterSiteDeviceAttributeBean
						.setIsp(rs.getString("wug_dd_isp"));
				masterSiteDeviceAttributeBean.setIspSupport(rs
						.getString("wug_dd_isp_support_number"));
				masterSiteDeviceAttributeBean.setLocation(rs
						.getString("wug_dd_location"));
				masterSiteDeviceAttributeBean.setModemType(rs
						.getString("wug_dd_modem_type"));
				masterSiteDeviceAttributeBean.setSiteContactNo(rs
						.getString("wug_dd_site_contact_phone"));
				masterSiteDeviceAttributeBean.setSiteNumber(rs
						.getString("wug_dv_display_name"));
				masterSiteDeviceAttributeList
						.add(masterSiteDeviceAttributeBean);
			}
		} catch (Exception e) {
			logger.error(" getDeviceAttributeBysite " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getDeviceAttributeBysite " + e.getMessage());
			}
		}
		return masterSiteDeviceAttributeList;
	}

	public static ArrayList<MasterSiteInformationBean> getHelpDeskTicketDetails(
			String siteId, DataSource ds) {

		ArrayList<MasterSiteInformationBean> helpDeskDetails = new ArrayList<MasterSiteInformationBean>();

		String sql = " SELECT DISTINCT lm_js_id,lm_js_title, lm_js_create_date, "
				+ " (SELECT count(pod.lm_po_id) FROM podb_purchase_order_deliverables pod "
				+ " LEFT JOIN lm_purchase_order po ON po.lm_po_id = pod.lm_po_id "
				+ " LEFT JOIN lm_job jb ON jb.lm_js_id = po.lm_po_js_id   "
				+ " WHERE jb.lm_js_id = job1.lm_js_id/* AND po.lm_po_id = po1.lm_po_id*/) AS deliverable_count, "
				+ " (SELECT count(lm_in_js_id) FROM lm_install_notes  "
				+ " WHERE lm_in_js_id = job1.lm_js_id "
				+ " ) AS  note_count , lm_js_pr_id, isnull(wug_rs_desc, '') wug_rs_desc /*, lm_po_id */"
				+ " FROM lm_job job1 "
				+ " LEFT JOIN lm_install_notes  ON lm_in_js_id  =lm_js_id "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id   "
				+ " LEFT JOIN lp_site_list ON lp_sl_id  = lm_si_sl_id   "
				+ " LEFT JOIN lm_ticket on lm_js_id = lm_tc_js_id LEFT JOIN lm_help_desk_ticket_details ON lm_hd_tc_id = lm_tc_id LEFT JOIN wug_resolution ON wug_rs_id = lm_hd_resolution "
				+ " WHERE lp_sl_id =   " + siteId
				// +
				// " where lp_sl_id IN (SELECT TOP 5 lp_sl_id FROM lp_site_list) "
				+ " AND lm_js_title LIKE ('%-HD') ORDER BY lm_js_title";

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			MasterSiteInformationBean bean = null;
			while (rs.next()) {
				bean = new MasterSiteInformationBean();
				bean.setJobId(rs.getString("lm_js_id"));
				bean.setTicketName(rs.getString("lm_js_title"));

				bean.setCreatedDate(rs.getDate("lm_js_create_date"));
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				String datenewformat = formatter.format(bean.getCreatedDate());
				bean.setCreatedDateString(datenewformat);

				bean.setDeliverableCount(rs.getInt("deliverable_count"));
				bean.setInstallNoteCount(rs.getInt("note_count"));
				bean.setAppendixId(rs.getString("lm_js_pr_id"));
				bean.setResolutionDesc(rs.getString("wug_rs_desc"));
				// bean.setPoWoId(rs.getString("lm_po_id"));

				helpDeskDetails.add(bean);
			}
		} catch (Exception e) {
			logger.error(" helpDeskDetails " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" helpDeskDetails " + e.getMessage());
			}
		}

		return helpDeskDetails;
	}

	public static ArrayList<MasterSiteInformationBean> getJobDetails(
			String siteId, DataSource ds) {

		ArrayList<MasterSiteInformationBean> helpDeskDetails = new ArrayList<MasterSiteInformationBean>();

		String sql = "SELECT DISTINCT lm_js_id,lm_js_title, lm_js_create_date, "
				+ " (SELECT count(pod.lm_po_id) FROM podb_purchase_order_deliverables pod "
				+ " LEFT JOIN lm_purchase_order po ON po.lm_po_id = pod.lm_po_id "
				+ " LEFT JOIN lm_job jb ON jb.lm_js_id = po.lm_po_js_id   "
				+ " WHERE jb.lm_js_id = job1.lm_js_id /*AND po.lm_po_id = po1.lm_po_id*/) AS deliverable_count, "
				+ " (SELECT count(lm_in_js_id) FROM lm_install_notes  "
				+ " WHERE lm_in_js_id = job1.lm_js_id "
				+ " ) AS  note_count, lm_js_pr_id /*, lm_po_id*/ "
				+ " FROM lm_job job1 "
				+ " LEFT JOIN lm_install_notes  ON lm_in_js_id  =lm_js_id "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id   "
				+ " LEFT JOIN lp_site_list ON lp_sl_id  = lm_si_sl_id   "
				+ " left JOIN lm_ticket on lm_js_id = lm_tc_js_id "
				// +
				// " LEFT JOIN lm_purchase_order po1 ON lm_po_js_id = lm_js_id "
				+ " WHERE lp_sl_id =   " + siteId
				// +
				// " where lp_sl_id IN (SELECT TOP 9000 lp_sl_id FROM lp_site_list) "
				+ " AND lm_js_title NOT  LIKE ('%-HD') ORDER BY lm_js_title";

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			MasterSiteInformationBean bean = null;
			while (rs.next()) {
				bean = new MasterSiteInformationBean();
				bean.setJobId(rs.getString("lm_js_id"));
				bean.setTicketName(rs.getString("lm_js_title"));

				bean.setCreatedDate(rs.getDate("lm_js_create_date"));
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				String datenewformat = formatter.format(bean.getCreatedDate());
				bean.setCreatedDateString(datenewformat);

				bean.setDeliverableCount(rs.getInt("deliverable_count"));
				bean.setInstallNoteCount(rs.getInt("note_count"));
				bean.setAppendixId(rs.getString("lm_js_pr_id"));

				helpDeskDetails.add(bean);
			}
		} catch (Exception e) {
			logger.error(" getJobDetails " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getJobDetails " + e.getMessage());
			}
		}

		return helpDeskDetails;
	}

	public static ArrayList<MasterSiteInformationBean> getDeliverableDetails(
			String siteId, DataSource ds) {

		ArrayList<MasterSiteInformationBean> helpDeskDetails = new ArrayList<MasterSiteInformationBean>();
		String sql = "SELECT lm_js_id, lm_js_title, po.lm_po_id lm_po_id, podb_mn_dl_title, podb_mn_dl_id, "
				+ " pod.podb_mn_dl_created_date podb_mn_dl_created_date, count(lm_js_id) note_count, lm_js_pr_id "
				+ " FROM  lm_job "
				+ " LEFT JOIN lm_install_notes ON lm_js_id = lm_in_js_id "
				+ " LEFT JOIN lm_purchase_order po ON lm_po_js_id = lm_js_id "
				+ " LEFT JOIN podb_purchase_order_deliverables pod ON pod.lm_po_id = po.lm_po_id "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id "
				+ " LEFT JOIN lp_site_list ON lp_sl_id  = lm_si_sl_id "
				+ " WHERE lp_sl_id =  "
				+ siteId
				// +
				// " where lp_sl_id IN (SELECT top 9000 lp_sl_id FROM lp_site_list) "
				+ " AND po.lm_po_id IS NOT NULL AND pod.podb_mn_dl_id IS NOT NULL  "
				+ " GROUP BY po.lm_po_id, lm_in_js_id, lm_js_title, pod.podb_mn_dl_created_date, lm_js_id, podb_mn_dl_title, "
				+ " podb_mn_dl_id, lm_js_pr_id  ORDER BY lm_js_title, lm_po_id, podb_mn_dl_title";

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			MasterSiteInformationBean bean = null;
			while (rs.next()) {
				bean = new MasterSiteInformationBean();
				bean.setJobId(rs.getString("lm_js_id"));
				bean.setTicketName(rs.getString("lm_js_title"));
				bean.setPoWoId(rs.getString("lm_po_id"));
				bean.setDeliverableDesc(rs.getString("podb_mn_dl_title"));

				bean.setCreatedDate(rs.getDate("podb_mn_dl_created_date"));
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				String datenewformat = formatter.format(bean.getCreatedDate());
				bean.setCreatedDateString(datenewformat);

				bean.setDeliverableId(rs.getString("podb_mn_dl_id"));
				bean.setInstallNoteCount(rs.getInt("note_count"));
				bean.setAppendixId(rs.getString("lm_js_pr_id"));

				helpDeskDetails.add(bean);
			}
		} catch (Exception e) {
			logger.error(" getDeliverableDetails " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getDeliverableDetails " + e.getMessage());
			}
		}

		return helpDeskDetails;
	}

	public static ArrayList<String[]> getReasonsChartDate(String siteId,
			DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		String[] reasonsChartDtls = null;
		ArrayList<String[]> lst = new ArrayList<String[]>();
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{call dbo.ccc_netmedx_pie_chart_site_ticket_type_summary(?)}");

			// cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(1, Integer.parseInt(siteId));

			rs = cstmt.executeQuery();
			while (rs.next()) {
				reasonsChartDtls = new String[2];
				reasonsChartDtls[0] = rs.getString(1);
				reasonsChartDtls[1] = rs.getString(2);
				lst.add(reasonsChartDtls);
			}

		}

		catch (Exception e) {
			logger.error("getReasonsChartDate(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getReasonsChartDate(String, DataSource) - Error Occured in deleting Job"
						+ e);
			}
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getReasonsChartDate(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getReasonsChartDate(String, DataSource) - exception ignored",
						sql);
			}

		}

		return lst;
	}

	public static ArrayList<String[]> getResourcesChartDate(String siteId,
			DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		String[] resourcesChartDtls = null;
		ArrayList<String[]> lst = new ArrayList<String[]>();
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{call dbo.ccc_netmedx_pie_chart_resources_site_summary(?)}");

			cstmt.setInt(1, Integer.parseInt(siteId));

			rs = cstmt.executeQuery();
			while (rs.next()) {
				resourcesChartDtls = new String[2];
				resourcesChartDtls[0] = rs.getString(1);
				resourcesChartDtls[1] = rs.getString(2);
				lst.add(resourcesChartDtls);
			}

		}

		catch (Exception e) {
			logger.error("getResourcesChartDate(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourcesChartDate(String, DataSource) - Error Occured in deleting Job"
						+ e);
			}
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getResourcesChartDate(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getResourcesChartDate(String, DataSource) - exception ignored",
						sql);
			}

		}

		return lst;
	}

	public static ArrayList<String[]> getCostChartData(String siteId,
			DataSource ds) {

		ArrayList<String[]> helpDeskDetails = new ArrayList<String[]>();
		String sql = "SELECT DATEPART(wk, lm_js_planned_start_date) weeks, sum(lm_pi_total) amount "
				+ " FROM lm_job   "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id "
				+ " LEFT JOIN lp_site_list ON lp_sl_id = lm_si_sl_id   "
				+ " LEFT JOIN lm_purchase_order ON lm_po_js_id = lm_js_id "
				+ " LEFT JOIN lm_partner_invoice ON lm_pi_po_id = lm_po_id   "
				// +
				// " WHERE lp_sl_id IN (SELECT TOP 9000 lp_sl_id FROM lp_site_list) "
				+ " WHERE lp_sl_id =  "
				+ siteId
				+ " AND lm_pi_total IS NOT NULL   "
				// + " --AND lm_js_planned_start_date >= '12/14/2012' "
				+ " AND lm_js_planned_start_date >  DATEADD(wk,-11, GETDATE()) "
				+ " AND lm_js_planned_start_date < GETDATE()  "
				+ " group by DATEPART( wk, lm_js_planned_start_date) "
				+ " ORDER BY weeks ";

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			String[] arr = null;
			while (rs.next()) {
				arr = new String[2];
				arr[0] = rs.getString("weeks");
				arr[1] = rs.getString("amount");
				helpDeskDetails.add(arr);
			}
		} catch (Exception e) {
			logger.error(" getCostChartData " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getCostChartData " + e.getMessage());
			}
		}

		return helpDeskDetails;
	}

	public static ArrayList<String[]> getAverageTimeTechOnSiteChartData(
			String siteId, DataSource ds) {

		ArrayList<String[]> helpDeskDetails = new ArrayList<String[]>();
		String sql = "SELECT DATEPART(wk, lm_js_planned_start_date) weeks, AVG( datediff(MINUTE , lx_se_start, lx_se_end ) ) /60  dateAvg "
				+ " FROM lx_schedule_element "
				+ " LEFT JOIN lm_job ON lm_js_se_schedule = lx_se_id "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id  "
				+ " LEFT JOIN lp_site_list ON lp_sl_id = lm_si_sl_id   "
				// +
				// " WHERE lp_sl_id IN (SELECT TOP 9000 lp_sl_id FROM lp_site_list) "
				+ " WHERE lp_sl_id =  "
				+ siteId
				// + " --AND lm_js_planned_start_date >= '12/14/2012'  "
				+ " AND lm_js_planned_start_date >  DATEADD(wk,-11, GETDATE()) "
				+ " AND lm_js_planned_start_date < GETDATE()  "
				+ " group by DATEPART( wk, lm_js_planned_start_date) "
				+ " ORDER BY weeks ";

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String[] arr = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				arr = new String[2];
				arr[0] = rs.getString("weeks");
				arr[1] = rs.getString("dateAvg");
				helpDeskDetails.add(arr);
			}
		} catch (Exception e) {
			logger.error(" getAverageTimeTechOnSiteChartData " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getAverageTimeTechOnSiteChartData "
						+ e.getMessage());
			}
		}

		return helpDeskDetails;
	}

	public static ArrayList<String[]> getCompositeOwnerChartData(String siteId,
			DataSource ds) {

		ArrayList<String[]> helpDeskDetails = new ArrayList<String[]>();
		String sql = "SELECT category, count(category) cat_count, DATEPART(wk, lm_js_planned_start_date) weeks "
				+ " FROM customer_required_data_answers  "
				+ " LEFT JOIN lm_job ON lm_js_id = job_id "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id "
				+ " LEFT JOIN lp_site_list ON lp_sl_id = lm_si_sl_id "
				+ " WHERE lp_sl_id = "
				+ siteId // IN (SELECT lp_sl_id FROM lp_site_list)
				+ " AND lm_js_planned_start_date >  DATEADD(wk,-11, GETDATE()) "
				+ " AND lm_js_planned_start_date < GETDATE()  "
				+ " AND category <> '' "
				+ " AND ques_id = 2 "
				+ " group by category, DATEPART( wk, lm_js_planned_start_date) "
				+ " ORDER BY weeks, category";

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String[] arr = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				arr = new String[3];
				arr[0] = rs.getString("category");
				arr[1] = rs.getString("cat_count");
				arr[2] = rs.getString("weeks");
				helpDeskDetails.add(arr);
			}
		} catch (Exception e) {
			logger.error(" getCompositeOwnerChartData " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getCompositeOwnerChartData " + e.getMessage());
			}
		}

		return helpDeskDetails;
	}

	public static ArrayList<String> getDeliverablesImgData(String siteId,
			DataSource ds) {

		ArrayList<String> docIdList = new ArrayList<String>();
		String sql = "select top 3 ddv.dm_doc_id "
				+ " from DocMFiledb.dbo.dm_document_file ddf "
				+ " LEFT JOIN DocMSysdb.dbo.dm_document_version ddv ON ddv.dm_doc_file_id = dm_fl_id "
				+ " LEFT JOIN podb_purchase_order_deliverables pod ON pod.podb_mn_dl_doc_id = ddv.dm_doc_id "
				+ " LEFT JOIN lm_purchase_order po ON po.lm_po_id = pod.lm_po_id "
				+ " LEFT JOIN lm_job lj ON lj.lm_js_id = po.lm_po_js_id  "
				+ " LEFT JOIN lm_site_detail lsd ON lsd.lm_si_id = lj.lm_js_si_id "
				+ " LEFT JOIN lp_site_list lsl ON lsl.lp_sl_id  = lsd.lm_si_sl_id   "
				+ " WHERE lsl.lp_sl_id = "
				+ siteId
				// +
				// " WHERE lsl.lp_sl_id  IN (SELECT lsl2.lp_sl_id FROM ilex.dbo.lp_site_list lsl2) "
				+ " AND pod.podb_mn_dl_status = 2 "
				+ " AND Application <> 'PDF' "
				+ " order by cast(documentVersion as numeric(10)) desc";

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				docIdList.add(rs.getString(1));
			}
		} catch (Exception e) {
			logger.error(" getDeliverablesImgData " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getDeliverablesImgData " + e.getMessage());
			}
		}

		return docIdList;
	}

	public static ArrayList<String[]> getKPIData(String siteId, DataSource ds) {

		String row1 = "Dispatches";
		String row2 = "First Visit Resolution";
		String row3 = "Percentage of First Time Resolution";
		String row4 = "On-Site on Requested Date";
		String row5 = "On Time Percentage";
		String row6 = "Total Visits";
		String row7 = "Unplanned Additional Visits";
		String row8 = "Average Time On-Site";

		ArrayList<String[]> kIPDetails = new ArrayList<String[]>();
		String sql = "SELECT '"
				+ row1
				+ "' name, isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 7 THEN lm_js_id END),0) AS dayJob7, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 30 THEN lm_js_id END),0) AS dayJob30, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 60 THEN lm_js_id END),0) AS dayJob60 "
				+ " FROM lm_job "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id "
				+ " LEFT JOIN lp_site_list ON lp_sl_id = lm_si_sl_id "
				+ " WHERE lp_sl_id =  "
				+ siteId
				// +
				// " where lp_sl_id IN (SELECT TOP 7500 lp_sl_id FROM lp_site_list) "
				+ " UNION "
				+ " SELECT '"
				+ row2
				+ "' name, isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 7 THEN lm_js_id END),0) AS dayJob7, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 30 THEN lm_js_id END),0) AS dayJob30, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 60 THEN lm_js_id END),0) AS dayJob60 "
				+ " from lm_job  "
				+ " LEFT JOIN  customer_required_data_answers ON job_id = lm_js_id "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id "
				+ " LEFT JOIN lp_site_list ON lp_sl_id = lm_si_sl_id "
				+ " /*WHERE job_id = 257548*/ "
				+ " WHERE lp_sl_id =  "
				+ siteId
				// +
				// " where lp_sl_id IN (SELECT TOP 9000 lp_sl_id FROM lp_site_list) "
				+ " AND ques_id = 1 "
				+ " AND ans_value = 1 "
				+ " UNION "
				+ " SELECT  '"
				+ row4
				+ "' name, isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 7 THEN lm_js_id END),0) AS dayJob7, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 30 THEN lm_js_id END),0) AS dayJob30, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 60 THEN lm_js_id END),0) AS dayJob60 "
				+ " FROM lm_job "
				+ " LEFT JOIN lx_schedule_element ON lx_se_id = lm_js_se_schedule "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id    "
				+ " LEFT JOIN lp_site_list ON lp_sl_id = lm_si_sl_id     "
				+ " WHERE lp_sl_id =  "
				+ siteId
				// +
				// " WHERE lp_sl_id IN (SELECT TOP 9000 lp_sl_id FROM lp_site_list) "
				+ " AND DATEADD(dd, 0, DATEDIFF(dd, 0, lx_se_start))<= DATEADD(dd, 0, DATEDIFF(dd, 0, lm_js_planned_start_date)) "
				+ " UNION "
				+ " SELECT  '"
				+ row5
				+ "' name, isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 7 THEN lm_js_id END),0) AS dayJob7, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 30 THEN lm_js_id END),0) AS dayJob30, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 60 THEN lm_js_id END),0) AS dayJob60 "
				+ " FROM lm_job "
				+ " LEFT JOIN lx_schedule_element ON lx_se_id = lm_js_se_schedule "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id    "
				+ " LEFT JOIN lp_site_list ON lp_sl_id = lm_si_sl_id     "
				// +
				// " WHERE lp_sl_id IN (SELECT TOP 9000 lp_sl_id FROM lp_site_list) "
				+ " WHERE lp_sl_id =  "
				+ siteId
				+ " AND lx_se_start<= lm_js_planned_start_date "
				+ " UNION "
				+ " SELECT '"
				+ row6
				+ "' name, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 7 THEN lx_se_type_id END),0) AS dayJob7, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 30 THEN lx_se_type_id END),0) AS dayJob30, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 60 THEN lx_se_type_id END),0) AS dayJob60 "
				+ " FROM lx_schedule_element  "
				+ " LEFT JOIN lm_job ON lm_js_se_schedule = lx_se_id "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id "
				+ " LEFT JOIN lp_site_list ON lp_sl_id = lm_si_sl_id "
				// +
				// " where lp_sl_id IN (SELECT top 900 lp_sl_id FROM lp_site_list) "
				+ " WHERE lp_sl_id =  "
				+ siteId
				+ " UNION  "
				+ " select '"
				+ row7
				+ "' name, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 7 THEN lm_js_id END),0) AS dayJob7, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 30 THEN lm_js_id END),0) AS dayJob30, "
				+ " isnull(count(CASE WHEN lm_js_planned_start_date >= getdate() - 60 THEN lm_js_id END),0) AS dayJob60 "
				+ " from  lm_job "
				+ " LEFT JOIN customer_required_data_answers ON job_id = lm_js_id "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id "
				+ " LEFT JOIN lp_site_list ON lp_sl_id = lm_si_sl_id "
				// +
				// " where lp_sl_id IN (SELECT top 19000 lp_sl_id FROM lp_site_list) "
				+ " WHERE lp_sl_id =  "
				+ siteId
				+ " AND ques_id = 3 "
				+ " AND ans_value = 1 "
				+ " UNION "
				+ " SELECT '"
				+ row8
				+ "' name, (SELECT avg(datediff(minute , lx_se_start, lx_se_end ))/60 "
				+ " FROM lx_schedule_element   "
				+ " LEFT JOIN lm_job ON lm_js_se_schedule = lx_se_id "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id  "
				+ " LEFT JOIN lp_site_list ON lp_sl_id = lm_si_sl_id     "
				// +
				// " WHERE lp_sl_id IN (SELECT TOP 5 lp_sl_id FROM lp_site_list) "
				+ " WHERE lp_sl_id =  "
				+ siteId
				+ " AND lm_js_planned_start_date < GETDATE() -7) dayJob7, "
				+ " (SELECT avg(datediff(minute , lx_se_start, lx_se_end ))/60 "
				+ " FROM lx_schedule_element   "
				+ " LEFT JOIN lm_job ON lm_js_se_schedule = lx_se_id "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id  "
				+ " LEFT JOIN lp_site_list ON lp_sl_id = lm_si_sl_id     "
				// +
				// " WHERE lp_sl_id IN (SELECT TOP 5 lp_sl_id FROM lp_site_list) "
				+ " WHERE lp_sl_id =  "
				+ siteId
				+ " AND lm_js_planned_start_date < GETDATE() -30) dayJob30, "
				+ " (SELECT avg(datediff(minute , lx_se_start, lx_se_end ))/60 "
				+ " FROM lx_schedule_element   "
				+ " LEFT JOIN lm_job ON lm_js_se_schedule = lx_se_id "
				+ " LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id  "
				+ " LEFT JOIN lp_site_list ON lp_sl_id = lm_si_sl_id     "
				// +
				// " WHERE lp_sl_id IN (SELECT TOP 5 lp_sl_id FROM lp_site_list) "
				+ " WHERE lp_sl_id =  " + siteId
				+ " AND lm_js_planned_start_date < GETDATE() -60) dayJob60 ";

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] arr = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				arr = new String[4];
				arr[0] = rs.getString("name");
				arr[1] = rs.getString("dayJob7");
				arr[2] = rs.getString("dayJob30");
				arr[3] = rs.getString("dayJob60");
				if (arr[0].equals(row8)) {// Average Time on-Site condition
					// arr[1] = (arr[1] == null ? "0" : arr[1]) + " hours";
					// arr[2] = (arr[2] == null ? "0" : arr[2]) + " hours";
					// arr[3] = (arr[3] == null ? "0" : arr[3]) + " hours";
				}
				kIPDetails.add(arr);
				if (arr[0].equals(row2)) {// First Visit Resolution condition
					String[] arr1 = new String[4];
					String[] arr2 = kIPDetails.get(0);
					DecimalFormat df = new DecimalFormat();
					df.setMaximumFractionDigits(2);

					arr1[0] = row3;
					if (!arr2[1].equals("0")) {
						arr1[1] = (df
								.format((Double.parseDouble(arr[1]) / Double
										.parseDouble(arr2[1])) * 100))
								+ "%";
					} else {
						arr1[1] = "0%";
					}

					if (!arr2[2].equals("0")) {
						arr1[2] = (df
								.format((Double.parseDouble(arr[2]) / Double
										.parseDouble(arr2[2])) * 100))
								+ "%";
					} else {
						arr1[2] = "0%";
					}

					if (!arr2[3].equals("0")) {
						arr1[3] = (df
								.format((Double.parseDouble(arr[3]) / Double
										.parseDouble(arr2[3])) * 100))
								+ "%";
					} else {
						arr1[3] = "0%";
					}

					kIPDetails.add(arr1);
				}

			}

			for (int i = 0; i < kIPDetails.size(); i++) {
				String[] arrTemp = kIPDetails.get(i);
				if (arrTemp[0].equals(row8)) {// Average Time on-Site condition
					arrTemp[1] = (arrTemp[1] == null ? "0" : arrTemp[1])
							+ " hours";
					arrTemp[2] = (arrTemp[2] == null ? "0" : arrTemp[2])
							+ " hours";
					arrTemp[3] = (arrTemp[3] == null ? "0" : arrTemp[3])
							+ " hours";
					break;
				}
				kIPDetails.set(i, arrTemp);
			}

		} catch (Exception e) {
			logger.error(" getKPIData " + e.getMessage());
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.error(" getKPIData " + e.getMessage());
			}
		}

		return kIPDetails;
	}

}
