/**
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is a data object access class used for managing privilege for a role in User Manager and populating drop down list and data for drop down list.      
*
*/

package com.mind.dao.UM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.DynamicTabularChkBox;

/**
* Methods : getOrganizationTypeName, getOrganizationDisciplineName, getFunctionGroup, getRoleName, getFunctionType, getNumColumns, getRolePrivilege, addFunctionType
*/

public class RolePrivilegedao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(RolePrivilegedao.class);
	
	private Collection functiongroup = null;
	private Collection rolename = null;

/** This method return Organization Type Name for an Organization Type. 
* @param org_type   			   String. the value of organization type
* @param ds   			       	   Object of DataSource
* @return String              	   This method returns string, Organization Type Name.
*/	
	
	public static String getOrganizationTypeName( String org_type, DataSource ds ) throws Exception
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String org_name = null;
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lo_organization_type_name from lo_organization_type where lo_organization_type ='"+org_type+"'";
			rs = stmt.executeQuery(sql);
			if(rs.next())
			{
				org_name = rs.getString("lo_organization_type_name");
			}
		}
		
		catch( Exception e )
		{
			logger.error("getOrganizationTypeName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOrganizationTypeName(String, DataSource) - Error occured during organizaton name" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs != null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.error("getOrganizationTypeName(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getOrganizationTypeName(String, DataSource) - Error occured during closing ResultSet" + e);
				}	
			}
			
			
			try
			{
				if ( stmt != null )
				{
					stmt.close();
				}	
			}
			catch( Exception e )
			{
				logger.error("getOrganizationTypeName(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getOrganizationTypeName(String, DataSource) - Error occured during closing Statement" + e);
				}
			}
			
			
			try
			{
				if ( conn != null)
				{ 
					conn.close();
				}	
			}
			catch( Exception e )
			{
				logger.error("getOrganizationTypeName(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getOrganizationTypeName(String, DataSource) - Error occured during closing Connection" + e);
				}
			}
			
		}
		
		return org_name;
	}

/** This method return Organization Discipline Name for an Organization Discipline Id. 
* @param org_discipline_id   	   String. the value of organization discipline id
* @param ds   			       	   Object of DataSource
* @return String              	   This method returns string, Organization Discipline Name.
*/		
	
	public static String getOrganizationDisciplineName( String org_discipline_id, DataSource ds ) throws Exception
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String org_discipline_name = null;
		try{
			conn = ds.getConnection();
			stmt = conn.createStatement();
				
			String sql = "select lo_od_name from lo_organization_discipline where lo_od_id = "+org_discipline_id;
			
			rs = stmt.executeQuery(sql);
			if(rs.next())
			{
				org_discipline_name = rs.getString("lo_od_name");
			}
			
		}
		
		catch(Exception e){
			logger.error("getOrganizationDisciplineName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOrganizationDisciplineName(String, DataSource) - Error occured during displine");
			}
		}
		
		finally
		{
			try
			{
				if ( rs != null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getOrganizationDisciplineName(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt != null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getOrganizationDisciplineName(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn != null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getOrganizationDisciplineName(String, DataSource) - exception ignored", e);
}
			
		}
		
		return org_discipline_name;
	}
	

/** This method return ArrayList of all Function Group using for dropdown list of function group. 
* @param ds   			       	   Object of DataSource
* @return ArrayList                This method returns ArrayList, arraylist of all function group.
*/	
	
	public static java.util.Collection getFunctionGroup(DataSource ds) 
	{
		
		ArrayList functiongroup = new java.util.ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		functiongroup.add( new com.mind.common.LabelValue( "---Select---" , "0") );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery( "select cc_fu_group_name from cc_function group by cc_fu_group_name order by cc_fu_group_name" );
			while( rs.next() )
			{
				functiongroup.add ( new com.mind.common.LabelValue( rs.getString( "cc_fu_group_name" ), rs.getString( "cc_fu_group_name" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getFunctionGroup(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getFunctionGroup(DataSource) - Exception caught in rerieving Function Group" + e);
			}
		}
		finally
		{
			
			try
			{
				if ( rs!= null ) 
				{
					rs.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getFunctionGroup(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( stmt!= null ) 
				{
					stmt.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getFunctionGroup(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getFunctionGroup(DataSource) - exception ignored", sql);
}
			
		}
		
		return functiongroup;
	}
	
	
/** This method return ArrayList of Role Name for an Organization Discipline Id, using for dropdown list of role name.
* @param org_discipline_id   	   String. the value of organization discipline id
* @param ds   			       	   Object of DataSource
* @return ArrayList                This method returns ArrayList, arraylist of role name.
*/	
	
	
	public static java.util.Collection getRoleName(String org_discipline_id, DataSource ds) 
	{
		
		ArrayList rolename = new java.util.ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		rolename.add( new com.mind.common.LabelValue( "---Select---" , "-1") );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery( "select lo_ro_id, lo_ro_role_desc from lo_role where lo_ro_od_id = "+org_discipline_id+ "order by lo_ro_role_desc" );
			while( rs.next() )
			{
				rolename.add ( new com.mind.common.LabelValue( rs.getString( "lo_ro_role_desc" ), rs.getString( "lo_ro_id" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getRoleName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRoleName(String, DataSource) - Exception caught in rerieving Role Name" + e);
			}
		}
		finally
		{
			
			try
			{
				if ( rs!= null ) 
				{
					rs.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getRoleName(String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( stmt!= null ) 
				{
					stmt.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getRoleName(String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getRoleName(String, DataSource) - exception ignored", sql);
}
			
		}
		
		return rolename;
	}
	
	
	
/** This method return collection of Function Type for a Function Group.
* @param cc_fu_group_name   	   String. the value of function group name
* @param ds   			       	   Object of DataSource
* @return ArrayList                This method returns ArrayList, arraylist of function type.
*/	
	
	public static java.util.ArrayList getFunctionType(String cc_fu_group_name, DataSource ds) 
	{
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList UMList = new ArrayList();
		DynamicTabularChkBox umfn = null;
		try
		{
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lo_view_function_type(?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2,cc_fu_group_name);
			rs  = cstmt.executeQuery();
			while ( rs.next())
			{
				umfn = new DynamicTabularChkBox();
       			if (rs.getString("c1")!= null)
					umfn.setC1(rs.getString("c1"));
				else
					umfn.setC1("");
				if (rs.getString("c2")!= null)
					umfn.setC2(rs.getString("c2"));
				else
					umfn.setC2("");
				if (rs.getString("c2id")!= null)
					umfn.setC2id(rs.getString("c2id"));
				else
					umfn.setC2id("");
				if (rs.getString("c3")!= null)
					umfn.setC3(rs.getString("c3"));
				else
					umfn.setC3("");
				if (rs.getString("c3id")!= null)
					umfn.setC3id(rs.getString("c3id"));
				else
					umfn.setC3id("");
				if (rs.getString("c4")!= null)
					umfn.setC4(rs.getString("c4"));
				else
					umfn.setC4("");
				if (rs.getString("c4id")!= null)
					umfn.setC4id(rs.getString("c4id"));
				else
					umfn.setC4id("");
				if (rs.getString("c5")!= null)
					umfn.setC5(rs.getString("c5"));
				else
					umfn.setC5("");
				if (rs.getString("c5id")!=null)
					umfn.setC5id(rs.getString("c5id"));
				else
					umfn.setC5id("");
				if (rs.getString("c6")!= null)
					umfn.setC6(rs.getString("c6"));
				else
					umfn.setC6("");
				if (rs.getString("c6id")!= null)
					umfn.setC6id(rs.getString("c6id"));
				else
					umfn.setC6id("");
				if (rs.getString("c7")!= null)
					umfn.setC7(rs.getString("c7"));
				else
					umfn.setC7("");
				if (rs.getString("c7id")!= null)
					umfn.setC7id(rs.getString("c7id"));
				else
					umfn.setC7id("");
				if (rs.getString("c8")!= null)
				    umfn.setC8(rs.getString("c8"));
				else
					umfn.setC8("");
				if (rs.getString("c8id")!= null)
					umfn.setC8id(rs.getString("c8id"));
				else
					umfn.setC8id("");
				
				if (rs.getString("c9")!= null)
				    umfn.setC9(rs.getString("c9"));
				else
					umfn.setC9("");
				if (rs.getString("c9id")!= null)
					umfn.setC9id(rs.getString("c9id"));
				else
					umfn.setC9id("");
				
				if (rs.getString("c10")!= null)
				    umfn.setC10(rs.getString("c10"));
				else
					umfn.setC10("");
				if (rs.getString("c10id")!= null)
					umfn.setC10id(rs.getString("c10id"));
				else
					umfn.setC10id("");
				
				if (rs.getString("c11")!= null)
				    umfn.setC11(rs.getString("c11"));
				else
					umfn.setC11("");
				if (rs.getString("c11id")!= null)
					umfn.setC11id(rs.getString("c11id"));
				else
					umfn.setC11id("");
				
				if (rs.getString("c12")!= null)
				    umfn.setC12(rs.getString("c12"));
				else
					umfn.setC12("");
				if (rs.getString("c12id")!= null)
					umfn.setC12id(rs.getString("c12id"));
				else
					umfn.setC12id("");
				
				UMList.add( umfn );
			}
		}
		catch( Exception e )
		{
			logger.error("getFunctionType(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getFunctionType(String, DataSource) - Exception caught in rerieving Function Type" + e);
			}
		}
		
		finally
		{
			
			try
			{
				if ( rs!= null ) 
				{
					rs.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getFunctionType(String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( cstmt!= null ) 
				{
					cstmt.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getFunctionType(String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getFunctionType(String, DataSource) - exception ignored", sql);
}
			
		}
		
		return UMList;	
	}
	
/** This method return int, distinct function name for a Function Group.
* @param cc_fu_group_name   	   String. the value of function group name
* @param ds   			       	   Object of DataSource
* @return int              	   	   This method returns int, value of distinct function name.
*/
	
	
	public static int getNumColumns(String cc_fu_group_name, DataSource ds) 
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int numcols = 0;
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
				
			String sql = "select count(distinct cc_fu_name) as numcols from cc_function where cc_fu_group_name ='"+cc_fu_group_name+"'";
			rs = stmt.executeQuery(sql);
			if(rs.next())
			{
				numcols = rs.getInt("numcols");
			}
		}
		
		catch(Exception e){
			logger.error("getNumColumns(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNumColumns(String, DataSource) - Error occured during Column Nos");
			}
		}
		
		finally
		{
			try
			{
				if ( rs != null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getNumColumns(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt != null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getNumColumns(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn != null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getNumColumns(String, DataSource) - exception ignored", e);
}
			
		}
		
		return numcols;
	}
	
/** This method return int, for checking success and failure.
* @param cc_fu_group_name   	   String. the value of function group name
* @param lo_ro_id   		   	   String. the value of role id
* @param indexvalue   		       String. the value of selectd function types 
* @param ds   			       	   Object of DataSource
* @return int              	   	   This method returns int, for checking success and failure.
*/	

	
	public static int addFunctionType(String cc_fu_group_name, String lo_ro_id, String indexvalue, DataSource ds)
	{
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int retval = -1;
		try{
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lo_function_type_manage_01(?,?,?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2,cc_fu_group_name);
			cstmt.setString(3,lo_ro_id);
			cstmt.setString(4,indexvalue);
			cstmt.execute();
			retval = cstmt.getInt(1);
			
		}
		catch(Exception e){
			logger.error("addFunctionType(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addFunctionType(String, String, String, DataSource) - Error occured during adding" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs != null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("addFunctionType(String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt != null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("addFunctionType(String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn != null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("addFunctionType(String, String, String, DataSource) - exception ignored", e);
}
			
		}
		return retval;
	}	
	
	
/** This method return int array of function type id from role privilege table, for a finction group and role name 
* @param cc_fu_group_name   	   String. the value of function group name
* @param lo_ro_id   		   	   String. the value of role id
* @param ds   			       	   Object of DataSource
* @return int              	   	   This method returns int, value of function types from role priviledge table.
*/
	
	public static int[] getRolePrivilege(String cc_fu_group_name, String lo_ro_id, DataSource ds) 
	{
		
		int cnt = 0;  
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int roleprivilege[] = null;
			
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select count(*) as cnt from dbo.func_lo_role_privilege("+lo_ro_id+", '"+cc_fu_group_name+"')";
			rs = stmt.executeQuery(sql);
			if(rs.next())
			{
			cnt = rs.getInt("cnt");	
			}
			roleprivilege = new int[cnt];
			sql = "select * from dbo.func_lo_role_privilege("+lo_ro_id+", '"+cc_fu_group_name+"')";
			rs = stmt.executeQuery(sql);
			int i = 0;
			while( rs.next() )
			{
				roleprivilege[i] = rs.getInt("lo_rp_fu_id"); 	
				i++;		
			}
			
		}
		catch( Exception e )
		{
			logger.error("getRolePrivilege(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRolePrivilege(String, String, DataSource) - Exception caught in rerieving Role Privilege" + e);
			}
		}
		finally
		{
			
			try
			{
				if ( rs!= null ) 
				{
					rs.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getRolePrivilege(String, String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( stmt!= null ) 
				{
					stmt.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getRolePrivilege(String, String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getRolePrivilege(String, String, DataSource) - exception ignored", sql);
}
			
		}
		
		return roleprivilege;
	}
	
	
}
