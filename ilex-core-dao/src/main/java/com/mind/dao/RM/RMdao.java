/**
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is a data access object class used for managing Add, Update, Delete functionality of all level categories of the Resource Manager.      
*
*/
package com.mind.dao.RM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.rm.ResourceAddBean;
import com.mind.common.Util;


/**
 * Methods : secondLevelCatg,deleteSecondLevelCatg,getResourceType,getCategoryName,getMfgid,getMfgName,
 * thirdLevelCatg,getThirdcatgName,getPpsid,deleteThirdLevelCatg,addItems,deleteResourceItem,
 * getResourceid,getFieldlist,getCnspartno,getStatusName,getCriticalitylist,getCriticalityName,getCriticalityid,checkManufacturer
 */


  public class RMdao 
  {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(RMdao.class);
	  
/** This method adds/updates 2nd level category. 
 * @param iv_mf_ct_id					String. id of 2nd level category
 * @param iv_mf_name 					String. name of 2nd level category
 * @param action 				        String. flag for add or update
 * @param ds   			       	    	Object of DataSource
 * @return int              	   	    This method returns integer, 0 for success and -ve values for error.
 */	  
	public static int secondLevelCatg(String iv_mf_ct_id, String iv_mf_id, String iv_mf_name, String action, DataSource ds)
	{
		
		Connection conn=null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		String catgid="";
		int retval=0;
		Util util = new Util();
		try
			{
			//	Added to change single quotes string to double quotes
			if(iv_mf_name != null) {
				if(iv_mf_name.indexOf("'") >= 0)
					iv_mf_name = util.changeStringToDoubleQuotes(iv_mf_name);
			}
			
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call iv_manufacturer_manage_01(?,?,?,?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2,iv_mf_ct_id);
			cstmt.setString(3,iv_mf_id);
			cstmt.setString(4,iv_mf_name);
			cstmt.setString(5,action);
			cstmt.execute();
			retval = cstmt.getInt(1);
			
		}
		catch(Exception e){
			logger.error("secondLevelCatg(String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("secondLevelCatg(String, String, String, String, DataSource) - Error occured during adding" + e);
			}
		}
		
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("secondLevelCatg(String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("secondLevelCatg(String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("secondLevelCatg(String, String, String, String, DataSource) - exception ignored", e);
}
			
		}return retval;
	}	
	
		
	
/** This method deletes 2nd level category. 
 * @param iv_mf_ct_id					String. id of 2nd level category
 * @param ds   			       	    	Object of DataSource
 * @return int              	   	    This method returns integer, 0 for success and -ve values for error.
 */
	public static int deleteSecondLevelCatg(String iv_mf_id,DataSource ds)
	{
		
		Connection conn=null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		
		int retval=0;
		String mfgid="";
		try{
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call iv_manufacturer_delete_01(?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			
			cstmt.setString(2,iv_mf_id);
			
			cstmt.execute();
			retval = cstmt.getInt(1);
			
		}
		catch(Exception e){
			logger.error("deleteSecondLevelCatg(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteSecondLevelCatg(String, DataSource) - Error occured during deleting  " + e);
			}
			
		}
		finally
		{
				
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("deleteSecondLevelCatg(String, DataSource) - exception ignored", e);
}
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("deleteSecondLevelCatg(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("deleteSecondLevelCatg(String, DataSource) - exception ignored", e);
}
			
		}
		return retval;
	}
	

/** This method gets Resource type(materials/labor/freight/travel). 
 * @param catid							String. id of 1st level category
 * @param ds   			       	   	    Object of DataSource
 * @return String              	   	    This method returns String, resource type.
 */	
	public  static String getResourceType(String catid, DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String resourcetype=null;
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select iv_ct_type from iv_category where iv_ct_id="+catid	;
			
			rs = stmt.executeQuery(sql);
			if(rs.next())
			{
				resourcetype=rs.getString("iv_ct_type");
			}
			
		}
		catch(Exception e){
			logger.error("getResourceType(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourceType(String, DataSource) - Error occured during getting resource type");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getResourceType(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getResourceType(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getResourceType(String, DataSource) - exception ignored", e);
}
			
		}
		return resourcetype;	
	}	
		
	
/** This method gets 1st level category name. 
 * @param catid						String. id of 1st level category
 * @param ds   			       	    Object of DataSource
 * @return String              	   	This method returns String, 1st level category name.
 */	
		public  static String getCategoryName(String catid, DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String catgname="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				String sql="select iv_ct_name from iv_category where iv_ct_id="+catid	;
				rs = stmt.executeQuery(sql);
				while(rs.next())
				{
					 catgname=rs.getString("iv_ct_name");
				}
				
			}
			catch(Exception e){
			logger.error("getCategoryName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCategoryName(String, DataSource) - Error occured during adding");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getCategoryName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getCategoryName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getCategoryName(String, DataSource) - exception ignored", e);
}
				
			}
			return catgname;	
		}	
			
		
/** This method gets 2nd level categoryid. 
 * @param catid						String. id of 1st level category
 * @param mfgname					String. 2nd level category name
 * @param ds   			       	    Object of DataSource
 * @return String              	   	This method returns String, 2nd level categoryid.
 */	
		public  static String getMfgid(String catid,String mfgname, DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String mfgid="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				String sql="select iv_mf_id from iv_manufacturer where iv_mf_name='"+mfgname+"' and iv_mf_ct_id="+catid;
				
				rs = stmt.executeQuery(sql);
				while(rs.next())
				{
					 mfgid=rs.getString("iv_mf_id");
					 
				}
				
			}
			catch(Exception e){
			logger.error("getMfgid(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMfgid(String, String, DataSource) - Error occured during getting mfgid  ");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getMfgid(String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getMfgid(String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getMfgid(String, String, DataSource) - exception ignored", e);
}
				
			}
			return mfgid;	
		}	
		
		
/** This method gets 2nd level category name. 
 * @param mfgid						String. id of 2nd level category
 * @param ds   			       	    Object of DataSource
 * @return String              	   	This method returns String, 2nd level category name.
 */	
		public  static String getMfgName(String mfgid, DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String mfgname="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				
				
				String sql="select iv_mf_name from iv_manufacturer where iv_mf_id="+mfgid	;
				
				rs = stmt.executeQuery(sql);
				while(rs.next())
				{
					 mfgname=rs.getString("iv_mf_name");
					 
					 
				}
				
			}
			catch(Exception e){
			logger.error("getMfgName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMfgName(String, DataSource) - Error occured during adding");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getMfgName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getMfgName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getMfgName(String, DataSource) - exception ignored", e);
}
				
			}
			return mfgname;	
		}	
		
		
/** This method adds/updates 3rd level category. 
 * @param iv_sc_mf_id					String. id of 2nd level category
 * @param iv_sc_id 						String. id of 3rd level category
 * @param iv_sc_name 					String. name of 3rd level category
 * @param action 				        String. flag for add or update
 * @param ds   			       	   		Object of DataSource
 * @return int              	   	    This method returns integer, 0 for success and -ve values for error.
 */
		public static int thirdLevelCatg(String iv_sc_mf_id,String iv_sc_id, String iv_sc_name ,String action ,DataSource ds)
		{
			
			Connection conn=null;
			CallableStatement cstmt=null;
			ResultSet rs=null;
			int retval=0;
			String mfgid="";
			Util util = new Util();
			try
				{
				//	Added to change single quotes string to double quotes
				if(iv_sc_name != null) {
						if(iv_sc_name.indexOf("'") >= 0)
							iv_sc_name = util.changeStringToDoubleQuotes(iv_sc_name);
				}
				
				conn=ds.getConnection();
				cstmt=conn.prepareCall("{?=call  iv_subcategory_manage_01(?,?,?,?)}");
				cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
				cstmt.setString(2,iv_sc_mf_id);
				cstmt.setString(3,iv_sc_id);
				cstmt.setString(4,iv_sc_name );
				cstmt.setString(5,action);
				cstmt.execute();
				retval = cstmt.getInt(1);
				
				
			}
			catch(Exception e){
			logger.error("thirdLevelCatg(String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("thirdLevelCatg(String, String, String, String, DataSource) - Error occured during adding third catg " + e);
			}
			}
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("thirdLevelCatg(String, String, String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( cstmt!= null ) 
						cstmt.close();
				}
				catch( Exception e )
				{
				logger.warn("thirdLevelCatg(String, String, String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("thirdLevelCatg(String, String, String, String, DataSource) - exception ignored", e);
}
				
			}return retval;
		}	
		
		
/** This method gets 3rd level category name. 
 * @param ppsid						String. id of 3rd level category
 * @param ds   			       	    Object of DataSource
 * @return String              	   	This method returns String, 3rd level category name.
 */	
		public  static String getThirdcatgName(String ppsid, DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String thirdcatname="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				String sql="select iv_sc_name from iv_subcategory where iv_sc_id="+ppsid	;
				
				rs = stmt.executeQuery(sql);
				while(rs.next())
				{
					thirdcatname=rs.getString("iv_sc_name");
					 
				}
				
			}
			catch(Exception e){
			logger.error("getThirdcatgName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getThirdcatgName(String, DataSource) - Error occured during updating");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getThirdcatgName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getThirdcatgName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getThirdcatgName(String, DataSource) - exception ignored", e);
}
				
			}
			return thirdcatname;	
		}
		
		
/** This method gets 3rd level category id. 
 * @param mfgid						String. id of 2nd level category
 * @param ppsname					String. 3rd level category name
 * @param ds   			       	    Object of DataSource
 * @return String              	   	This method returns String, 3rd level categoryid.
 */
		public  static String getPpsid(String mfgid,String ppsname, DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String ppsid="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				String sql="select iv_sc_id from iv_subcategory where iv_sc_name='"+ppsname+"' and iv_sc_mf_id="+mfgid;
				
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					 ppsid=rs.getString("iv_sc_id");
					 
				}
				
			}
			catch(Exception e){
			logger.error("getPpsid(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPpsid(String, String, DataSource) - Error occured during getting ppsid  ");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getPpsid(String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getPpsid(String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getPpsid(String, String, DataSource) - exception ignored", e);
}
				
			}
			return ppsid;	
		}	
		
	
		
/** This method deletes 3rd level category. 
 * @param iv_sc_id						String. id of 3rd level category
 * @param ds   			       	    	Object of DataSource
 * @return int              	   	    This method returns integer, 0 for success and -ve values for error.
 */	
		public static int deleteThirdLevelCatg(String iv_sc_id,DataSource ds)
		{
			
			Connection conn=null;
			CallableStatement cstmt = null;
			ResultSet rs=null;
			
			int retval=0;
			String mfgid="";
			try{
				conn=ds.getConnection();
				cstmt=conn.prepareCall("{?=call iv_subcategory_delete_01(?)}");
				cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
				
				cstmt.setString(2,iv_sc_id);
				
				cstmt.execute();
				retval = cstmt.getInt(1);
				
			}
			catch(Exception e){
			logger.error("deleteThirdLevelCatg(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteThirdLevelCatg(String, DataSource) - Error occured during deleting thirdcatg  " + e);
			}
				
			}
			finally
			{
					
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("deleteThirdLevelCatg(String, DataSource) - exception ignored", e);
}
				
				try
				{
					if ( cstmt!= null ) 
						cstmt.close();
				}
				catch( Exception e )
				{
				logger.warn("deleteThirdLevelCatg(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("deleteThirdLevelCatg(String, DataSource) - exception ignored", e);
}
				
			}
			return retval;
		}	
				
	
		
		
/** This method adds/updates 4th level category. 
 * @param iv_rp_id								int. id of Item (4th level category)
 * @param iv_rp_sc_id 							int. id of 3rd level category
 * @param iv_rp_name 							String. name of Item (4th level category)
 * @param iv_rp_mfg_part_number					String. manufacturer part number of Item (4th level category)
 * @param iv_rp_sell_quantity 					String. sellable quantity  of Item (4th level category)
 * @param iv_rp_minimum_quantity 				String. minimum quantity  of Item (4th level category)
 * @param iv_rp_units							String. units of Item (4th level category)
 * @param iv_rp_sell_quantity_weight_in_lbs 	String. weight of sell quantity of Item (4th level category)
 * @param iv_rp_alt_identifier 					String. identifier for Item (4th level category)
 * @param iv_rp_status							String. status of the Item (4th level category)
 * @param iv_cost 								String. cost per unit of Item (4th level category)
 * @param cuser 								int. id of user
 * @param action 				   			    String. flag for add or update
 * @param criticality_id 				   		String. id of the criticality of the labor resource
 * @param ds   			       	  		   	    Object of DataSource
 * @return int              	   	   		    This method returns integer, 0 for success and -ve values for error.
  */
	public static int addItems(int iv_rp_id, int iv_rp_sc_id, String iv_rp_name, String iv_rp_mfg_part_number, String iv_rp_sell_quantity, String iv_rp_minimum_quantity, String iv_rp_units, String iv_rp_sell_quantity_weight_in_lbs, String iv_rp_alt_identifier, String iv_rp_status, String iv_cost, int cuser, String action ,String criticality_id, DataSource ds)
		{
			
			Connection conn=null;
			CallableStatement cstmt=null;
			ResultSet rs=null;
			int retval=0;
			Util util = new Util(); 
			try
				{
				
				// Added to change single quotes string to double quotes
				if(iv_rp_name != null) {
					if(iv_rp_name.indexOf("'") >= 0)
						iv_rp_name = util.changeStringToDoubleQuotes(iv_rp_name);
				}
				
				conn=ds.getConnection();
				cstmt=conn.prepareCall("{?=call iv_resource_pool_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
				cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
				cstmt.setInt(2,iv_rp_id);
				cstmt.setInt(3,iv_rp_sc_id);
				cstmt.setString(4,iv_rp_name );
				cstmt.setString(5,iv_rp_mfg_part_number );
				cstmt.setString(6,iv_rp_sell_quantity );
				cstmt.setString(7,iv_rp_minimum_quantity );
				cstmt.setString(8,iv_rp_units );
				cstmt.setString(9,iv_rp_sell_quantity_weight_in_lbs );
				cstmt.setString(10,iv_rp_alt_identifier );
				cstmt.setString(11,iv_rp_status );
			    cstmt.setString(12,iv_cost );
				cstmt.setInt(13,cuser);
				cstmt.setString(14,action);
				cstmt.setString(15,criticality_id);
				cstmt.execute();
				retval = cstmt.getInt(1);
				
			}
			catch(Exception e){
			logger.error("addItems(int, int, String, String, String, String, String, String, String, String, String, int, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addItems(int, int, String, String, String, String, String, String, String, String, String, int, String, String, DataSource) - Error occured during adding resource items " + e);
			}
			}
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("addItems(int, int, String, String, String, String, String, String, String, String, String, int, String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( cstmt!= null ) 
						cstmt.close();
				}
				catch( Exception e )
				{
				logger.warn("addItems(int, int, String, String, String, String, String, String, String, String, String, int, String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("addItems(int, int, String, String, String, String, String, String, String, String, String, int, String, String, DataSource) - exception ignored", e);
}
				
			}return retval;
		}	
			
		
/** This method deletes 4th level category(items). 
 * @param iv_rp_id						int. id of Item (4th level category)
 * @param cuser 						int. id of user
 * @param ds   			       	   	    Object of DataSource
 * @return int              	   	    This method returns integer, 0 for success and -ve values for error.
 */			
		public static int deleteResourceItem(int iv_rp_id,int cuser,DataSource ds)
		{
			
			Connection conn=null;
			CallableStatement cstmt = null;
			ResultSet rs=null;
			
			int retval=0;
			
			try{
				conn=ds.getConnection();
				cstmt=conn.prepareCall("{?=call iv_resource_pool_delete_01(?,?)}");
				cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
				
				cstmt.setInt(2,iv_rp_id);
				cstmt.setInt(3,cuser);
				
				cstmt.execute();
				retval = cstmt.getInt(1);
				
			}
			catch(Exception e){
			logger.error("deleteResourceItem(int, int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteResourceItem(int, int, DataSource) - Error occured during deleting resourceitem  " + e);
			}
				
			}
			finally
			{
					
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("deleteResourceItem(int, int, DataSource) - exception ignored", e);
}
				
				try
				{
					if ( cstmt!= null ) 
						cstmt.close();
				}
				catch( Exception e )
				{
				logger.warn("deleteResourceItem(int, int, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("deleteResourceItem(int, int, DataSource) - exception ignored", e);
}
				
			}
			return retval;
		}	
		
		
/** This method gets id of 4th level category(items). 
 * @param thirdcatgid 							int. id of 3rd level category
 * @param lname 								String. name of Item (4th level category)
 * @param lidentifier 							String. identifier for Item (4th level category)
 * @param status								String. status of the Item (4th level category)
 * @param sellqty 								String. sellable quantity  of Item (4th level category)
 * @param minqty 								String. minimum quantity  of Item (4th level category)
 * @param unit									String. units of Item (4th level category)
 * @param bcost 								String. cost per unit of Item (4th level category)
 * @param ds   			       	  		    	Object of DataSource
 * @return String              	   	   			This method returns String, id of Item (4th level category).
 */	
		public  static String getResourceid(String thirdcatgid,String lname,String lidentifier,String status,String sellqty,String minqty,String unit,String bcost,DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String resid="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				
				
				String sql="select iv_rp_id from iv_resource_pool where iv_rp_sc_id="+thirdcatgid  +
						   "and iv_rp_name='"+lname+"' and iv_rp_alt_identifier='"+lidentifier+"' "+
						   "and iv_rp_status='"+status+"' " +
						   "and iv_rp_sell_quantity="+sellqty +
						   "and iv_rp_minimum_quantity="+minqty +
						   "and iv_rp_units='"+unit+"'" + 
						   "and iv_cost='"+bcost+"'" ;
				
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					resid=rs.getString("iv_rp_id");
					 
				}
				
			}
			catch(Exception e){
			logger.error("getResourceid(String, String, String, String, String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourceid(String, String, String, String, String, String, String, String, DataSource) - Error occured during getting resid  ");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getResourceid(String, String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getResourceid(String, String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getResourceid(String, String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
				
			}
			return resid;	
		}	
		
		
/** This method gets list of values of fields of a item(4th level category). 
* @param resourceid   			   String. id of item.
* @param ds   			       	   Object of DataSource
* @return String[]             	   This method returns string[], values of all fields of a item.
*/	
		public  static String[] getFieldlist(String resourceid,DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String[] valuelist=new String[15];
			
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				String sql="select  iv_rp_sc_id ,iv_rp_name, iv_rp_alt_identifier,iv_rp_status ,iv_rp_sell_quantity, iv_rp_minimum_quantity, iv_rp_units,iv_cost,iv_rp_mfg_part_number, iv_rp_cns_part_number, iv_crit_name,iv_rp_created_by,iv_rp_create_date,iv_rp_changed_by,iv_rp_change_date from iv_resource_details_list_01 where iv_rp_id="+resourceid;						   		
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					 valuelist[0]=rs.getString("iv_rp_sc_id");
					 valuelist[1]=rs.getString("iv_rp_name");
					 valuelist[2]=rs.getString("iv_rp_alt_identifier");
					 valuelist[3]=rs.getString("iv_rp_status");
					 valuelist[4]=rs.getString("iv_rp_sell_quantity");
					 valuelist[5]=rs.getString("iv_rp_minimum_quantity");
					 valuelist[6]=rs.getString("iv_rp_units");
					 valuelist[7]=rs.getString("iv_cost");
					 valuelist[8]=rs.getString("iv_rp_mfg_part_number");
					 valuelist[9]=rs.getString("iv_rp_cns_part_number");
					 valuelist[10]=rs.getString("iv_crit_name");
					 valuelist[11]=rs.getString("iv_rp_created_by");
					 valuelist[12]=rs.getString("iv_rp_create_date");
					 valuelist[13]=rs.getString("iv_rp_changed_by");
					 valuelist[14]=rs.getString("iv_rp_change_date");
					 
				}
				
			}
			catch(Exception e){
			logger.error("getFieldlist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getFieldlist(String, DataSource) - Error occured during getting fieldlist  ");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getFieldlist(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getFieldlist(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getFieldlist(String, DataSource) - exception ignored", e);
}
				
			}
			return valuelist;	
		}
		
	
/** This method gets cns part number of the item. 
 * @param resid						int. id of 4th level category(item)
 * @param ds   			       	    Object of DataSource
 * @return String              	    This method returns String, cns part number of the item.
 */
		public  static String getCnspartno(String resid,DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String cnsno="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				
				
				String sql="select iv_rp_cns_part_number from lx_resource_hierarchy_01 where iv_rp_id="+resid ;
				
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					cnsno=rs.getString("iv_rp_cns_part_number");
					 
				}
				
			}
			catch(Exception e){
			logger.error("getCnspartno(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCnspartno(String, DataSource) - Error occured during getting cnspartno ");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getCnspartno(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getCnspartno(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getCnspartno(String, DataSource) - exception ignored", e);
}
				
			}
			return cnsno;
		}		
		
		
/** This method gets Status of the item. 
 * @param statusid					int. id of Status
 * @param ds   			       	    Object of DataSource
 * @return String              	    This method returns String, cns part number of the item.
 */
		public  static String getStatusName(String statusid,DataSource ds)
		{
				
				Connection conn=null;
				Statement stmt=null;
				ResultSet rs=null;
				String statusname="";
				try{
					conn=ds.getConnection();
					stmt=conn.createStatement();
					
					
					
					String sql="select cc_status_description  from cc_status_master where cc_status_code='"+statusid+"'" ;
					
					rs = stmt.executeQuery(sql);
					if(rs.next())
					{
						statusname=rs.getString("cc_status_description");
						 
					}
					
				}
				catch(Exception e){
			logger.error("getStatusName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStatusName(String, DataSource) - Error occured during getting statusname ");
			}
				}
				
				finally
				{
					try
					{
						if ( rs!= null ) 
						{
							rs.close();		
						}
					}
					
					catch( Exception e )
					{
				logger.warn("getStatusName(String, DataSource) - exception ignored", e);
}
					
					
					try
					{
						if ( stmt!= null ) 
							stmt.close();
					}
					catch( Exception e )
					{
				logger.warn("getStatusName(String, DataSource) - exception ignored", e);
}
					
					
					try
					{
						if ( conn!= null) 
							conn.close();
					}
					catch( Exception e )
					{
				logger.warn("getStatusName(String, DataSource) - exception ignored", e);
}
					
				}
			return statusname;	
		}	
		
		
/** This method gets Criticality list of the labor resources. 
 * @param ds   			       	    Object of DataSource
 * @return ArrayList              	This method returns ArrayList, Criticality list of the labor resources.
 */
		
		public  static ArrayList getCriticalitylist(DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			ArrayList valuelist=new ArrayList();
			
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				String sql="select  iv_crit_id , iv_crit_name from iv_criticality";						   		
				rs = stmt.executeQuery(sql);
				int i=0;
				
				while(rs.next())
				{
				
					 ResourceAddBean raBean = new ResourceAddBean();
					 raBean.setCriticalityid(rs.getString("iv_crit_id"));
					 raBean.setCriticalityname(rs.getString("iv_crit_name"));
					 
					 valuelist.add(i,raBean);
					 i++;
					 
				}
				ResourceAddBean raBean = new ResourceAddBean();
				raBean.setCriticalityid("0");
				raBean.setCriticalityname("All");
				valuelist.add(i,raBean);
				
				
			}
			catch(Exception e){
			logger.error("getCriticalitylist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCriticalitylist(DataSource) - Error occured during getting Allreportdetails  ");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getCriticalitylist(DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getCriticalitylist(DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getCriticalitylist(DataSource) - exception ignored", e);
}
				
			}
			return valuelist;	
		}
		
		
/** This method gets Criticality name of the labor resources. 
 * @param criticalityid   			id of criticality of the resource
 * @param ds   			       	    Object of DataSource
 * @return String              	This method returns String, Criticality name of the labor resources.
 */
		
		public  static String getCriticalityName(String criticalityid, DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String criticalityname="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				if(criticalityid.equals("0"))
				{
					criticalityname="All";
				}
				else
				{
					String sql="select iv_crit_name from iv_criticality where iv_crit_id="+criticalityid;
					
					rs = stmt.executeQuery(sql);
					while(rs.next())
					{
						criticalityname=rs.getString("iv_crit_name");
						 
					}
				}
				
			}
			catch(Exception e){
			logger.error("getCriticalityName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCriticalityName(String, DataSource) - Error occured during getting criticalityname");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getCriticalityName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getCriticalityName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getCriticalityName(String, DataSource) - exception ignored", e);
}
				
			}
			return criticalityname;	
		}
		
		
/** This method gets Criticality Id of the resources. 
 * @param criticalityname  			name of criticality of the resource
 * @param ds   			       	    Object of DataSource
 * @return ArrayList              	This method returns String, Criticality name of the labor resources.
 */

		public  static String getCriticalityid(String criticalityname, DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String critid="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				if(criticalityname.equals("All"))
				{
					critid="0";
				}
				else
				{
					String sql="select iv_crit_id from iv_criticality where iv_crit_name='"+criticalityname+"'";
					rs = stmt.executeQuery(sql);
					if(rs.next())
					{
						critid=rs.getString("iv_crit_id");
						 
					}
				}
				
			}
			catch(Exception e){
			logger.error("getCriticalityid(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCriticalityid(String, DataSource) - Error occured during getting iv_crit_id  ");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getCriticalityid(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getCriticalityid(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getCriticalityid(String, DataSource) - exception ignored", e);
}
				
			}
			return critid;	
		}
		
		
/** This method checks for a manufacturer(2nd level category) name which is like %field%. 
 * @param subcatid  				manufacturer id(2nd level category id)
 * @param ds   			       	    Object of DataSource
 * @return int              		This method returns int,1 if like field & 0 if not like field.
 */
		
		
		public  static int checkManufacturer(String subcatid, DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			int manufacturername=0;
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				String sql="select  dbo.iv_check__manufacturer_name("+subcatid+")";
				
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					manufacturername=rs.getInt(1);
				}
				
			}
			catch(Exception e){
			logger.error("checkManufacturer(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkManufacturer(String, DataSource) - Error occured during getting resource type");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("checkManufacturer(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("checkManufacturer(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("checkManufacturer(String, DataSource) - exception ignored", e);
}
				
			}
			return manufacturername;	
		}	
			
		
		
   }	
		
