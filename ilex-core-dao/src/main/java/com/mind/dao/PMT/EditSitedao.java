package com.mind.dao.PMT;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import com.mind.bean.pmt.EditSiteBean;;



public class EditSitedao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(EditSitedao.class);
	
	
	/**
	 * This method will retrieve all sites for all MSA's
	 * @author amitm
	 * @param ds
	 * @return
	 */
	
	public static ArrayList getAllSiteList(String msaid, String stateid, String sitename, String sitenumber, String sitecity, String sortqueryclause,  DataSource ds) {
		EditSiteBean editSiteBean = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList allsitelist = new ArrayList();
		
		if( sortqueryclause.equals( "" ) )              
		{
			sortqueryclause = "order by lo_ot_name";
		}
		//System.out.println("id are as follows...............msaid , stateid, sitename  => "+msaid+","+stateid+",  "+sitename+", "+sortqueryclause);
		/*if( msaid!=""  )              
		{
		}
		else
		{
			msaid = "%";
		}
		if( stateid!=""  )              
		{
		}
		else
		{
			stateid = "%";
		}
		if(sitename!="" )
		{
		}
		else
		{
			sitename = "%";
		}*/

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lp_msa_sites_001('"+msaid+"','"+stateid+"','"+sitename+"%','"+sitenumber+"%','"+sitecity+"%') "+sortqueryclause;
					
			//System.out.println("EditSiteDAO  all sites query is =============> "+sql);
			
			rs = stmt.executeQuery(sql);
			int i = 0;

			while (rs.next()) {

				editSiteBean = new EditSiteBean();

				editSiteBean.setMsaName(rs.getString("lo_ot_name"));
				editSiteBean.setSiteName(rs.getString("lp_si_name"));
				editSiteBean.setSiteNumber(rs.getString("lp_si_number"));
				editSiteBean.setSiteid(rs.getString("lp_sl_id"));
				editSiteBean.setMastersiteid(rs.getString("lp_sl_id"));
				editSiteBean.setLocalityFactor(rs.getString("lp_si_locality_factor"));
				editSiteBean.setUnionSite(rs.getString("lp_si_union_site"));
				if(rs.getString("lp_si_union_site").equals("NO"))
					editSiteBean.setUnionSitecombo("N");
				else
					editSiteBean.setUnionSitecombo("Y");
				editSiteBean.setDesignator(rs.getString("lp_si_designator"));
				editSiteBean.setWorklocation(rs.getString("lp_si_work_location"));
				editSiteBean.setAddress(rs.getString("lp_si_address"));
				editSiteBean.setCity(rs.getString("lp_si_city"));
				editSiteBean.setState(rs.getString("lp_si_state"));
				editSiteBean.setStatecombo(rs.getString("lp_si_state"));
				
				editSiteBean.setZipcode(rs.getString("lp_si_zip_code"));
				editSiteBean.setPrimaryPhoneNumber(rs.getString("lp_si_phone_no"));
				editSiteBean.setSecondaryPhoneNumber(rs.getString("lp_si_sec_phone_no"));
				editSiteBean.setCountry(rs.getString("lp_si_country"));
				editSiteBean.setDirections(rs.getString("lp_si_directions"));
				editSiteBean.setSecondaryContactPerson(rs.getString("lp_si_sec_poc"));
				editSiteBean.setPrimaryContactPerson(rs.getString("lp_si_pri_poc"));
				editSiteBean.setPrimaryEmailId(rs.getString("lp_si_pri_email_id"));
				editSiteBean.setSecondaryEmailId(rs.getString("lp_si_sec_email_id"));
				editSiteBean.setSpecialCondition(rs.getString("lp_si_notes"));
				
				
				allsitelist.add(i, editSiteBean);
				i++;
			}

		} catch (Exception e) {
			logger.error("getAllSiteList(String, String, String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAllSiteList(String, String, String, String, String, String, DataSource) - Error occured during getting all site list   ");
			}
			logger.error("getAllSiteList(String, String, String, String, String, String, DataSource)", e);
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn("getAllSiteList(String, String, String, String, String, String, DataSource) - exception ignored", e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn("getAllSiteList(String, String, String, String, String, String, DataSource) - exception ignored", e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn("getAllSiteList(String, String, String, String, String, String, DataSource) - exception ignored", e);
			}

		}
		return allsitelist;
	}

	
	/**
	 * This method will update site of MSA's
	 * @author amitm
	 * @param editSiteBean
	 * @param ds
	 * @return
	 * @throws Exception
	 */
	
	public static int updateSite( EditSiteBean editSiteBean  ,  DataSource ds ) throws Exception
	{
		Connection conn = null;
		CallableStatement cstmt = null;
		
		ResultSet rs = null;
		int val = -1;
		try
		{
			conn = ds.getConnection();
			
			cstmt = conn.prepareCall("{?=call dbo.lp_edit_msa_site(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			
			/*
			System.out.println("=========>exec lp_edit_msa_site "+Float.parseFloat( editSiteBean.getLocalityFactor() )
					//+", "+editSiteBean.getUnionSite()+", "
					+", "+editSiteBean.getUnionSitecombo()+", "
					+editSiteBean.getDesignator()+", "
					+editSiteBean.getWorklocation()+", "
					+editSiteBean.getAddress()+", "
					+editSiteBean.getCity()+", "
					//+editSiteBean.getState()+", "
					+editSiteBean.getStatecombo()+", "
					+editSiteBean.getZipcode()+", "
					+editSiteBean.getSecondaryPhoneNumber()+", "
					+editSiteBean.getPrimaryPhoneNumber()+", "
					+editSiteBean.getCountry()+", "
					+editSiteBean.getDirections()+", "
					+editSiteBean.getPrimaryContactPerson()+", "
					+editSiteBean.getSecondaryContactPerson()+", "
					+editSiteBean.getPrimaryEmailId()+", "
					+editSiteBean.getSecondaryEmailId()+", "
					+editSiteBean.getSpecialCondition()+", "
					+editSiteBean.getSiteName()+", "
					+editSiteBean.getSiteNumber()+", "
					+editSiteBean.getSiteid());
			*/
			
			
			cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			
			cstmt.setFloat( 2 , Float.parseFloat( editSiteBean.getLocalityFactor() ) );
			
			//cstmt.setString( 3 , editSiteBean.getUnionSite() ) ;
			
			cstmt.setString( 3 , editSiteBean.getUnionSitecombo() ) ;
			
			cstmt.setString( 4 ,  editSiteBean.getDesignator() );
			cstmt.setString( 5 , editSiteBean.getWorklocation());
			
			cstmt.setString( 6 , editSiteBean.getAddress() );
			cstmt.setString( 7 , editSiteBean.getCity() );
			//cstmt.setString( 8 , editSiteBean.getState() );
			cstmt.setString(8,editSiteBean.getStatecombo());
			
			cstmt.setString( 9 , editSiteBean.getZipcode() );
			cstmt.setString( 10 , editSiteBean.getSecondaryPhoneNumber());
			
			cstmt.setString( 11 , editSiteBean.getPrimaryPhoneNumber() );
			
			cstmt.setString( 12 , editSiteBean.getCountry() ) ;
			
			cstmt.setString( 13 ,  editSiteBean.getDirections());
			cstmt.setString( 14 , editSiteBean.getPrimaryContactPerson());
			
			cstmt.setString( 15 , editSiteBean.getSecondaryContactPerson() );
			cstmt.setString( 16 , editSiteBean.getPrimaryEmailId() );
			cstmt.setString( 17 , editSiteBean.getSecondaryEmailId() );
			cstmt.setString( 18 , editSiteBean.getSpecialCondition() );
			cstmt.setString( 19 , editSiteBean.getSiteName());
			cstmt.setString( 20 , editSiteBean.getSiteNumber());
			
			cstmt.setString( 21 , editSiteBean.getSiteid());
			
			
			cstmt.execute();
			
			val = cstmt.getInt( 1 );	
		}
		
		catch( Exception e )
		{
			logger.error("updateSite(EditSiteBean, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateSite(EditSiteBean, DataSource) - Error Occured in updating site infornation for MSA " + e);
			}
			logger.error("updateSite(EditSiteBean, DataSource)", e);
		}
		
		finally
		{
			try
			{
				if ( rs != null ) 
				{
					rs.close();		
				}
			}
			
			catch( SQLException sql )
			{
				logger.warn("updateSite(EditSiteBean, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( cstmt != null ) 
					cstmt.close();
			}
			catch( SQLException sql )
			{
				logger.warn("updateSite(EditSiteBean, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn != null ) 
					conn.close();
			}
			catch( SQLException sql )
			{
				logger.warn("updateSite(EditSiteBean, DataSource) - exception ignored", sql);
}
			
		}
		
		return val;
	}


}
