package com.mind.dao.PMT;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.pmt.HideAppendixBean;
import com.mind.bean.pmt.HideMSABean;
import com.mind.bean.pmt.RawVendexViewBean;
import com.mind.bean.pmt.SwitchJobStatusBean;
import com.mind.fw.core.dao.util.DBUtil;

public class EditMasterlDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(EditMasterlDao.class);

	public static String getEmailMessage(String emailType, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String content = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select cc_ed_content from cc_email_detail where cc_ed_type = '"
					+ emailType + "'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				content = rs.getString("cc_ed_content");
			}

		} catch (Exception e) {
			logger.error("getEmailMessage(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEmailMessage(String, DataSource) - Error occured during getting Email Message :::"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getEmailMessage(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getEmailMessage(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getEmailMessage(String, DataSource) - exception ignored",
						e);
			}

		}
		return content;
	}

	public static String getPOTermCond(String type, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String content = null;
		String value = "";
		String termType = "";

		if (type.equals("poterm"))
			termType = "P";
		else if (type.equals("speedterm"))
			termType = "S";
		else if (type.equals("minutemanterm"))
			termType = "M";
		else if (type.equals("inpoterm"))
			termType = "IP";
		else if (type.equals("inspeedterm"))
			termType = "IS";
		else if (type.equals("inminutemanterm"))
			termType = "IM";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select top 1 lm_po_tc_data, lm_po_tc_created_date from lm_po_term_condition where lm_po_tc_type ='"
					+ termType + "' order by lm_po_tc_created_date desc";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				content = new String(rs.getBytes(1));
				;
			}

		} catch (Exception e) {
			logger.error("getPOTermCond(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPOTermCond(String, DataSource) - Error occured during getting PO Term Condition :::"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getPOTermCond(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getPOTermCond(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getPOTermCond(String, DataSource) - exception ignored",
						e);
			}

		}
		return content;
	}

	/**
	 * This method updates email message.
	 * 
	 * @param messageContent
	 *            String. Message Content
	 * @param ds
	 *            Object of DataSource
	 * @return int This method returns integer, 0 for success and -ve values for
	 *         error.
	 */
	public static int updateMessage(String messageContent, String type,
			DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int retval = 0;
		// System.out.println("messageContentmessageContent::::::"+messageContent);
		// System.out.println("typetype::::::"+type);

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call cc_email_manage_01(?, ? )}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, type);
			cstmt.setString(3, messageContent);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("updateMessage(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateMessage(String, String, DataSource) - Error occured during update email message/ po sp instruction ::::"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"updateMessage(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"updateMessage(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"updateMessage(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retval;
	}

	/**
	 * This method updates email message.
	 * 
	 * @param messageContent
	 *            String. Message Content
	 * @param ds
	 *            Object of DataSource
	 * @return int This method returns integer, 0 for success and -ve values for
	 *         error.
	 */
	public static int updatePOTermCond(String type, String messageContent,
			String loginUserId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int retval = 0;
		// System.out.println("messageContentmessageContent::::::"+messageContent);
		// System.out.println("typetype::::::"+loginUserId);

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call cc_po_term_manage_01(?, ?, ? )}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setBytes(2, messageContent.getBytes());
			cstmt.setString(3, loginUserId);
			cstmt.setString(4, type);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"updatePOTermCond(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatePOTermCond(String, String, String, DataSource) - Error occured during update PO Term Condition::::"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"updatePOTermCond(String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"updatePOTermCond(String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"updatePOTermCond(String, String, String, DataSource) - exception ignored",
						e);
			}

		}

		return retval;
	}

	/**
	 * This method retrieves all MSA with details to be shown on Hide MSA Page
	 * in Master Manager.
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns MSA Id, MSA Name, BDM selected for
	 *         that MSA and MSA Status for all MSA.
	 */
	public static ArrayList getMSAList(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		ArrayList msalist = new ArrayList();
		int i = 0;

		try {

			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select msaid, msaname, bdm, msastatus from cc_get_msa_list_01";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				HideMSABean hideMSA = new HideMSABean();
				hideMSA.setMsaId(rs.getString("msaid"));
				hideMSA.setMsaName(rs.getString("msaname"));
				hideMSA.setMsaBDM(rs.getString("bdm"));
				hideMSA.setMsaStatus(rs.getString("msastatus"));
				msalist.add(i, hideMSA);
				i++;
			}

		} catch (Exception e) {
			logger.error("getMSAList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMSAList(DataSource) - Error occurred while retrieving MSA List to hide in Master Manager:"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("getMSAList(DataSource) - exception ignored", e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn("getMSAList(DataSource) - exception ignored", e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn("getMSAList(DataSource) - exception ignored", e);
			}

		}

		return msalist;
	}

	/**
	 * This method retrieves appendices that exists for the selected MSA to be
	 * shown on Hide Appendix page in Master Manager.
	 * 
	 * @param MsaId
	 *            MsaId as String.
	 * @param ds
	 *            Object of DataSource.
	 * @return ArrayList This method returns Appendix Id, Appendix Name, BDM
	 *         selected for that Appendix, Appendix Project Manager and Appendix
	 *         Status for all Appendices that exists for the selected MSA.
	 */
	public static ArrayList getAppendixListForMSA(String MsaId, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		ArrayList appendixlist = new ArrayList();
		int i = 0;

		try {

			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select appendixId = lx_pr_id, appName = lx_pr_title, appBDM = isnull(dbo.func_cc_poc_name(lx_pr_cns_poc),''), "
					+ "appPrMngr = isnull(dbo.func_cc_poc_name(lm_ap_pc_id),''), appStatus = isnull(dbo.func_status_description(lx_pr_status),'') "
					+ "from lx_appendix_main left outer join lm_appendix_poc on lx_pr_id = lm_ap_pr_id "
					+ "where lx_pr_mm_id =" + MsaId + " order by appName";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				HideAppendixBean hideAppendix = new HideAppendixBean();
				hideAppendix.setAppendixId(rs.getString("appendixId"));
				hideAppendix.setAppendixName(rs.getString("appName"));
				hideAppendix.setAppendixBDM(rs.getString("appBDM"));
				hideAppendix.setAppendixPrManager(rs.getString("appPrMngr"));
				hideAppendix.setAppStatus(rs.getString("appStatus"));
				appendixlist.add(i, hideAppendix);
				i++;
			}

		} catch (Exception e) {
			logger.error("getAppendixListForMSA(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixListForMSA(String, DataSource) - Error occurred while retrieving Appendix List for an MSA to hide in Master Manager:"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getAppendixListForMSA(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixListForMSA(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixListForMSA(String, DataSource) - exception ignored",
						e);
			}

		}

		return appendixlist;
	}

	/**
	 * This method populates the MSA combobox on hide Appendix page with those
	 * MSA that are not selected as hidden MSA.
	 * 
	 * @param MsaId
	 *            MsaId as String.
	 * @param ds
	 *            Object of DataSource.
	 * @return ArrayList This method returns MSA Id, MSA Name for all MSA that
	 *         are not checked as hidden on Hide MSA page.
	 */
	public static ArrayList getCustomers(DataSource ds) {
		ArrayList customername = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		customername.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select msaid, msaname from cc_get_msa_list_01 where lp_mm_hide <> 'Y'";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				customername.add(new com.mind.common.LabelValue(rs
						.getString("msaname"), rs.getString("msaid")));
			}
		} catch (Exception e) {
			logger.error("getCustomers(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomers(DataSource) - Exception caught in rerieving customer names in Master Manager"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sqlE) {
				logger.warn("getCustomers(DataSource) - exception ignored",
						sqlE);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sqlE) {
				logger.warn("getCustomers(DataSource) - exception ignored",
						sqlE);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqlE) {
				logger.warn("getCustomers(DataSource) - exception ignored",
						sqlE);
			}

		}
		return customername;
	}

	/**
	 * This method sets the selected MSA/Appendices as hidden in the Database.
	 * 
	 * @param type
	 *            String Object. It could be 'msa'/'appendix'.
	 * @param saveIds
	 *            String Object. Comma separated string of Ids (msa Ids or
	 *            appendix Ids).
	 * @param Id
	 *            String Object. Msa Id in case of type = 'msa'. '0' for type =
	 *            'appendix'.
	 * @param ds
	 *            Object of DataSource.
	 * @return int This method returns 0 on success and -ve value on error.
	 */
	public static int setHiddenDataInMasters(String type, String saveIds,
			String Id, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int retval = -1;

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call cc_hide_master_data_01(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, type);
			cstmt.setInt(3, Integer.parseInt(Id));
			cstmt.setString(4, saveIds);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"setHiddenDataInMasters(String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("setHiddenDataInMasters(String, String, String, DataSource) - Error occured while hidding list of Ids in Master Manager::::"
						+ e);
			}
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"setHiddenDataInMasters(String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"setHiddenDataInMasters(String, String, String, DataSource) - exception ignored",
						e);
			}

		}

		return retval;
	}

	/**
	 * This method retrieve MSA/Appendices from database that are marked as
	 * hidden.
	 * 
	 * @param hiddenType
	 *            String Object. It could be 'MSA'/'Appendix'.
	 * @param Id
	 *            String Object. Msa Id for hiddenType = 'MSA'. '0' for
	 *            hiddenType = 'Appendix'.
	 * @param ds
	 *            Object of DataSource.
	 * @return String[] This method returns String array with Ids of hidden
	 *         MSA/Appendix.
	 */
	public static String[] gethiddenDataInMasters(String hiddenType, String Id,
			DataSource ds) {

		int cnt = 0;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		String hiddenData[] = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (hiddenType.equalsIgnoreCase("MSA")) {
				sql = "select count(*) as cnt from lp_msa_main where isnull(lp_mm_hide,'N') = 'Y'";
			} else if (hiddenType.equalsIgnoreCase("Appendix")) {
				sql = "select count(*) as cnt from lx_appendix_main where isnull(lx_pr_hide,'N') = 'Y' and lx_pr_mm_id="
						+ Id;
			}
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				cnt = rs.getInt("cnt");
			}

			hiddenData = new String[cnt];
			if (hiddenType.equalsIgnoreCase("MSA")) {
				sql = "select lp_mm_id as hiddenIds from lp_msa_main where isnull(lp_mm_hide,'N') = 'Y'";
			} else if (hiddenType.equalsIgnoreCase("Appendix")) {
				sql = "select lx_pr_id as hiddenIds from lx_appendix_main where isnull(lx_pr_hide,'N') = 'Y' and lx_pr_mm_id="
						+ Id;
			}
			rs = stmt.executeQuery(sql);

			int i = 0;
			while (rs.next()) {
				hiddenData[i] = rs.getString("hiddenIds");
				i++;
			}
		} catch (Exception e) {
			logger.error("gethiddenDataInMasters(String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("gethiddenDataInMasters(String, String, DataSource) - Exception caught in rerieving Saved Resources "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"gethiddenDataInMasters(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"gethiddenDataInMasters(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"gethiddenDataInMasters(String, String, DataSource) - exception ignored",
						s);
			}

		}

		return hiddenData;
	}

	public static ArrayList getRawVendexList(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valueList = new ArrayList(); // to store msa list
		try {

			String sql = "select  lo_ot_name,lx_pr_end_customer,lp_mm_id,count(lx_pr_end_customer) as count from lp_msa_main"
					+ "	left outer join lx_appendix_main on lp_mm_id = lx_pr_mm_id and lx_pr_end_customer is not null"
					+ " inner join lo_organization_top  on lo_ot_id = lp_mm_ot_id "
					+ " group by lx_pr_end_customer,lo_ot_name,lp_mm_id order by 1";
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			int i = 0;
			/*
			 * if(i==0){ IRMEditPOForm irmForm = new IRMEditPOForm();
			 * irmForm.setMsaId("0"); irmForm.setMsaName("Not");
			 * valueList.add(i,irmForm); i++; }
			 */
			String previousName = "";
			while (rs.next()) {

				RawVendexViewBean rawBean = new RawVendexViewBean();

				if (previousName.equals(rs.getString("lo_ot_name"))) {
					rawBean.setIsMsa("false");
				} else {
					rawBean.setIsMsa("true");
				}
				rawBean.setMsaId(rs.getString("lp_mm_id") + "^^"
						+ rs.getString("lx_pr_end_customer"));
				// System.out.println(" value of check box from list "+rawForm.getMsaId());//
				// - Msa Id
				rawBean.setMsaName(rs.getString("lo_ot_name")); // - Msa Name
				rawBean.setEndCustomer(rs.getString("lx_pr_end_customer")); // -
																			// End
																			// Customer
				rawBean.setCountendCustomer(rs.getString("count"));

				previousName = rs.getString("lo_ot_name");
				valueList.add(i, rawBean);
				i++;
			}
		} catch (Exception e) {
			System.out.println("Exception in com.mind.IRM.dao.getMsaList() "
					+ e);
			e.printStackTrace();
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
		return valueList;

	}

	/**
	 * This method sets the selected MSA/EndCustomer for RAW Vendex View.
	 * 
	 * @param type
	 *            String Object. It could be
	 *            'Msa^^endCustomer^^endcustomer~MSA'.
	 * @return int This method returns 0 on success and -ve value on error.
	 */
	public static int updateMSAEndCustomer(String ids, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int retval = -1;

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lp_rawvendex_msa_manage_01(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, ids);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("updateMSAEndCustomer(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateMSAEndCustomer(String, DataSource) - Error occured while hidding list of Ids in Master Manager::::"
						+ e);
			}
		}

		finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);

		}

		return retval;
	}

	public static String[] getCheckedEndCustomer(DataSource ds) {

		int cnt = 0;
		Connection conn = null;
		java.sql.PreparedStatement pStmt = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		String sql = null;
		Statement stmt = null;
		String msaEndCustomer[] = null;

		try {
			conn = ds.getConnection();
			sql = "select * from lp_msa_end_customer";
			String sql1 = "select count(*) as count  from lp_msa_end_customer";
			pStmt = conn.prepareStatement(sql);
			rs = pStmt.executeQuery();
			stmt = conn.createStatement();
			rs1 = stmt.executeQuery(sql1);
			if (rs1.next()) {
				msaEndCustomer = new String[rs1.getInt("count")];
			}

			int i = 0;
			String value = "";
			int count = 0;

			while (rs.next()) {
				value = rs.getString("lp_msa_id") + "^^"
						+ rs.getString("lp_msa_end_customer");
				// System.out.println("Checked Value ---"+value);
				msaEndCustomer[i] = value;
				i++;
			}
		} catch (Exception e) {
			logger.error("getCheckedEndCustomer( DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCheckedEndCustomer( DataSource) - Exception caught in rerieving Saved Resources "
						+ e);
			}
		} finally {

			DBUtil.close(rs1, stmt);
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);

		}

		return msaEndCustomer;

	}

	public static boolean isRawVenedexed(String msaId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		boolean result = false;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			// System.out.println("select * from lp_msa_end_customer where lp_msa_id = "+msaId);
			rs = stmt
					.executeQuery("select * from lp_msa_end_customer where lp_msa_id = "
							+ msaId);
			if (rs.next()) {
				result = true;
			}

		} catch (Exception e) {
			logger.error("isRawVenedexed( String,DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("isRawVenedexed( String,DataSource) - Exception caught in rerieving isRawVenedexed "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
		return result;
	}

	public static ArrayList<SwitchJobStatusBean> getDbClosedinvoiceJobs(
			String invoiceNumber, DataSource ds) {

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList<SwitchJobStatusBean> closedInvoicedjobs = new ArrayList<SwitchJobStatusBean>();
		String sql = "";

		try {
			conn = ds.getConnection();
			sql = "select * from dbo.func_lm_get_by_invoice_closed_jobs(?)";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, invoiceNumber);
			rs = pStmt.executeQuery();

			while (rs.next()) {
				SwitchJobStatusBean switchJobStatus = new SwitchJobStatusBean();
				switchJobStatus.setJobId(rs.getString("lm_js_id"));
				switchJobStatus.setJobTitle(rs.getString("lm_js_title"));
				switchJobStatus.setSiteNumber(rs.getString("lm_si_number"));
				switchJobStatus.setSiteAddress(rs.getString("lm_si_address"));
				switchJobStatus.setSiteCity(rs.getString("lm_si_city"));
				switchJobStatus.setSiteState(rs.getString("lm_si_state"));
				switchJobStatus.setMsaName(rs.getString("lo_ot_name"));
				switchJobStatus.setAppendixId(rs.getString("lx_pr_id"));
				closedInvoicedjobs.add(switchJobStatus);
			}
		} catch (Exception e) {
			logger.error("getDbClosedinvoiceJobs( String,DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDbClosedinvoiceJobs( String,DataSource) - Exception caught in retrieving Closed Invoiced Jobs "
						+ e);
			}
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}

		return closedInvoicedjobs;
	}

	public static int switchJobStatus(String jobId, DataSource ds) {
		Connection conn = null;
		CallableStatement cStmt = null;
		ResultSet rs = null;
		int retVal = -1;

		try {
			conn = ds.getConnection();
			cStmt = conn.prepareCall("{?=call lm_switch_job_status(?)}");
			cStmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cStmt.setString(2, jobId);
			cStmt.execute();
			retVal = cStmt.getInt(1);
		} catch (Exception e) {
			logger.error("switchJobStatus( String,DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("switchJobStatus( String,DataSource) - Exception caught in changing Closed job status to Complete. "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cStmt);
			DBUtil.close(conn);

		}
		return retVal;
	}
}
