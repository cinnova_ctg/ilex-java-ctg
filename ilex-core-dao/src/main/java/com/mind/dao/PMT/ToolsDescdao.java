package com.mind.dao.PMT;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.pmt.ToolsDescBean;
import com.mind.fw.core.dao.util.DBUtil;

public class ToolsDescdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ToolsDescdao.class);

	public static ArrayList getToolsList(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new com.mind.common.LabelValue("--Select Tool--", "-1"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_tool_name,cp_tool_id from cp_tools");

			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("cp_tool_name"), rs.getString("cp_tool_id")));
			}
		}

		catch (Exception e) {
			logger.error("getToolsList(DataSource)", e);

			logger.error("getToolsList(DataSource)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn); // Close database conneciton object
		}
		return list;
	}

	public static void getTool(ToolsDescBean tool, String toolId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		if (logger.isDebugEnabled()) {
			logger.debug("getTool(ToolsDescForm, String, DataSource) - get tool method toolId="
					+ toolId);
		}
		// ToolsDescForm tool =new ToolsDescForm();
		try {
			if (toolId.equals("-1")) {
				// tool = new ToolsDescForm();
				tool.setName("");
				tool.setDescription("");
				tool.setIsActive("Y");

			} else {
				conn = ds.getConnection();
				stmt = conn.createStatement();
				if (logger.isDebugEnabled()) {
					logger.debug("getTool(ToolsDescForm, String, DataSource) - select * from cp_tools where cp_tool_id= "
							+ toolId);
				}
				rs = stmt
						.executeQuery("select * from cp_tools where cp_tool_id = "
								+ toolId);
				while (rs.next()) {
					if (logger.isDebugEnabled()) {
						logger.debug("getTool(ToolsDescForm, String, DataSource) - In mode "
								+ rs.getString("cp_tool_id"));
					}
					tool.setToolId(toolId);
					tool.setName(rs.getString("cp_tool_name"));
					tool.setDescription(rs.getString("cp_tool_desc"));
					tool.setIsActive(rs.getString("cp_tool_active"));
				}
			}
		} catch (Exception e) {
			logger.error("getTool(ToolsDescForm, String, DataSource)", e);

			logger.error("getTool(ToolsDescForm, String, DataSource)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
			; // Close database conneciton object
		}
		// return tool;
	}

	public static int manageTool(ToolsDescBean tool, String action,
			String loginUser, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int value = -1;

		try {
			conn = ds.getConnection();
			// cstmt=conn.createStatement();

			if (logger.isDebugEnabled()) {
				logger.debug("manageTool(ToolsDescForm, String, String, DataSource) - "
						+ tool.getToolId());
			}
			if (logger.isDebugEnabled()) {
				logger.debug("manageTool(ToolsDescForm, String, String, DataSource) - "
						+ tool.getName());
			}
			if (logger.isDebugEnabled()) {
				logger.debug("manageTool(ToolsDescForm, String, String, DataSource) - "
						+ tool.getDescription());
			}
			if (logger.isDebugEnabled()) {
				logger.debug("manageTool(ToolsDescForm, String, String, DataSource) - "
						+ tool.getIsActive());
			}
			if (logger.isDebugEnabled()) {
				logger.debug("manageTool(ToolsDescForm, String, String, DataSource) - "
						+ loginUser);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("manageTool(ToolsDescForm, String, String, DataSource) - "
						+ loginUser);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("manageTool(ToolsDescForm, String, String, DataSource) - "
						+ action);
			}
			if (tool.getIsActive() == null) {
				tool.setIsActive("N");
			}

			cstmt = conn.prepareCall("{?=call cp_tools_manage(?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, tool.getToolId());
			cstmt.setString(3, tool.getName());
			cstmt.setString(4, tool.getDescription());
			cstmt.setString(5, tool.getIsActive());
			cstmt.setString(6, loginUser);
			cstmt.setString(7, loginUser);
			cstmt.setString(8, action);

			cstmt.execute();
			value = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"manageTool(ToolsDescForm, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("manageTool(ToolsDescForm, String, String, DataSource) - Exception occured "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn); // Close database conneciton object
		}
		return value;

	}
}
