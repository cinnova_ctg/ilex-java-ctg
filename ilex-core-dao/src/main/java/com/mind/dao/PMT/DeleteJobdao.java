/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* 
*/
package com.mind.dao.PMT;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import sun.net.www.content.text.Generic;

import com.mind.bean.pmt.DeleteJobBean;
import com.mind.common.Util;
import com.mind.fw.core.dao.util.DBUtil;

import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
/**
*  Used for deleting jobs from SqlServer/MySql and inserting the same record in lm_job_deleted_records table in SqlServer.
* 
* @see 
* 
**/

public class DeleteJobdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DeleteJobdao.class);

		/**
	    * Method             Used to search jobs based on their names 
	    * @param             String 			Name of the job to be searched
	    * @param 			 DataSource 		DataSource Object
	    * @return 			 ArrayList 			Which contains the jobs that match with the input parameter
	    * @see   
	    **/
	public static ArrayList getAllJobs(String jobName,DataSource ds){
		ArrayList jobList = new ArrayList();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		DeleteJobBean jobBean = null;
		
		try{
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{call lm_job_detail_01(?)}");
			cstmt.setString(1, Util.singleToDoubleQuotes(jobName));
			rs = cstmt.executeQuery();
			int i = 0;
			
			while(rs.next()){
				jobBean = new 	DeleteJobBean();
				jobBean.setMsaId(rs.getString("lp_mm_id"));
				jobBean.setMsaName(rs.getString("lo_ot_name"));
				jobBean.setAppendixId(rs.getString("lx_pr_id"));
				jobBean.setAppendixName(rs.getString("lx_pr_title"));
				jobBean.setJobId(rs.getString("lm_js_id"));
				jobBean.setJobName(rs.getString("lm_js_title"));
				jobBean.setJobStatus(rs.getString("lm_js_status"));
				jobBean.setJobOwner(rs.getString("lm_js_owner"));
				jobBean.setJobScheduleCheck(rs.getString("lm_js_se_schedule"));
				jobBean.setJobPowoCheck(rs.getString("lm_js_po_check"));
				jobBean.setJobOrgStatus(rs.getString("lm_js_prj_status"));
				jobList.add(i,jobBean);
				i++;
			}
			
		}catch(Exception e){
			logger.error("getAllJobs(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAllJobs(String, DataSource) - Exception occurred while getting jobs from master module" + e);
			}
		}finally{
			try{
				DBUtil.closeCallableStatement(rs, cstmt);
			}
			catch( Exception e ){
				logger.warn("getAllJobs(String, DataSource) - exception ignored", e);
}
			try{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e ){
				logger.warn("getAllJobs(String, DataSource) - exception ignored", e);
}
		}
		return jobList;
	}

		/**
	    * Method             Used to delete job from SqlServer 
	    * @param             Connection			Database connection object
	    * @param 			 String		 		Job Id to be deleted
	    * @return			 int				0 succcessful delete  
	    * @exception         Generic 
	    * @see   
	    **/
	public static int sqlDeleteJob( Connection conn,String Id ) throws Exception
	{
		CallableStatement cstmt = null;
		int val = -1;
		
		try{
			cstmt = conn.prepareCall("{?=call lx_master_job_delete_01(?)}");
			cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			cstmt.setInt( 2 , Integer.parseInt( Id ) );
			cstmt.execute();
			val = cstmt.getInt( 1 );
			
		}catch( Exception e ){
			logger.error("sqlDeleteJob(Connection, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("sqlDeleteJob(Connection, String) - Error occured while deleting Job " + e);
			}
		}
		
		finally{
			try{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( SQLException sql ){
				logger.warn("sqlDeleteJob(Connection, String) - exception ignored", sql);
}
		}
		return val;
	}
		/**
	    * Method             Used to delete job from MySql 
	    * @param             Connection			Database connection object
	    * @param 			 String		 		Job Id to be deleted
	    * @exception         Generic 
	    * @see   
	    **/
	public static void mySqlDeleteJob( Connection conn,String Id ) throws Exception
	{
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		String deleteJob = "delete from il_job_integration where il_ji_lm_js_id  = '"+Id+"'";
		String deleteActivity = "delete from il_activity_integration where il_ai_ji_lm_js_id  = '"+Id+"'";
		String deleteInstallNotes = "delete from il_installation_notes where il_in_ji_lm_js_id  = '"+Id+"'";
		String deleteCustomFields = "delete from il_job_custom_information where il_cn_ji_lm_js_id = '"+Id+"'";
		String sql  = "select 1 from il_job_integration where il_ji_lm_js_id  = ?";
		
		pstmt = conn.prepareStatement(sql);
		pstmt.setString(1,Id);
		rs = pstmt.executeQuery();
		
		if(rs.next()){
			stmt = conn.createStatement();
			stmt.addBatch(deleteActivity);
			stmt.addBatch(deleteInstallNotes);
			stmt.addBatch(deleteCustomFields);
			stmt.addBatch(deleteJob);
			int result[] = stmt.executeBatch();
		}else{
			throw new Exception();
		}
			try{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( SQLException sql1 ){
			logger.warn("mySqlDeleteJob(Connection, String) - exception ignored", sql1);
}
	}
		/**
	    * Method             Used to insert job in the lm_job_deleted_records table 
	    * @param             Connection			Database connection object
	    * @param 			 String		 		Job Id to be deleted
	    * @return			 int				1 succcessful insert  
	    * @exception         Generic 
	    * @see   
	    **/
	public static int insertDeletedJob( Connection conn,String Id ) throws Exception
	{
		CallableStatement cstmt = null;
		int val = -1;
		try{
			cstmt = conn.prepareCall("{?=call lm_job_insert(?)}");
			cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			cstmt.setString( 2 , Id );
			cstmt.execute();
			val = cstmt.getInt( 1 );
			
		}catch( Exception e ){
			logger.error("insertDeletedJob(Connection, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("insertDeletedJob(Connection, String) - Error occured while inserting job in backup table " + e);
			}
		}
		finally{
				try{
					if ( cstmt!= null ) 
						cstmt.close();
				}
				catch( SQLException sql ){
				logger.warn("insertDeletedJob(Connection, String) - exception ignored", sql);
}
			}
		return val;
	}
}
