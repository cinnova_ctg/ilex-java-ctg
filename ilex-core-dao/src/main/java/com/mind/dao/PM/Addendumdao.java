package com.mind.dao.PM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.pm.Activity;
import com.mind.bean.pm.ActivityListBean;

public class Addendumdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Addendumdao.class);

	public static String AddAddendum(String Appendix_Id, String type,
			String from, String loginuserid, DataSource ds) {
		int returnflag = -1;
		int addendumId = 0;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_addendum_manage_01(?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(Appendix_Id));
			cstmt.setString(3, from);
			cstmt.setString(4, type);
			cstmt.setInt(5, Integer.parseInt(loginuserid));
			cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
			// System.out.println(
			// "exec lm_addendum_manage_01 "+Integer.parseInt( Appendix_Id )
			// +",'"+from+"' , '"+type+"' , '"+Integer.parseInt( loginuserid
			// )+"" );

			cstmt.execute();
			returnflag = cstmt.getInt(1);
			addendumId = cstmt.getInt(6);
		} catch (Exception e) {
			logger.error(
					"AddAddendum(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("AddAddendum(String, String, String, String, DataSource) - Error Occured in creating Addendum "
						+ e);
			}
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"AddAddendum(String, String, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"AddAddendum(String, String, String, String, DataSource) - exception ignored",
						sql);
			}

		}
		return returnflag + "," + addendumId;
	}

	public static int CopyAddendumActivities(String activity_flag_id,
			String jobid, String fromflag, String loginuserid, DataSource ds) {
		int returnflag = -1;
		Connection conn = null;
		CallableStatement cstmt = null;
		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call lm_addendum_intermediate(?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setString(2, activity_flag_id);
			cstmt.setInt(3, Integer.parseInt(jobid));
			cstmt.setString(4, fromflag);
			cstmt.setInt(5, Integer.parseInt(loginuserid));

			// System.out.println(
			// "exec lm_addendum_intermediate '"+activity_flag_id
			// +"',"+Integer.parseInt( jobid
			// )+" , '"+fromflag+"' , "+Integer.parseInt( loginuserid )+"" );

			cstmt.execute();

			returnflag = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"CopyAddendumActivities(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("CopyAddendumActivities(String, String, String, String, DataSource) - Error caught in copying Addendum Activities"
						+ e);
			}
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"CopyAddendumActivities(String, String, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"CopyAddendumActivities(String, String, String, String, DataSource) - exception ignored",
						sql);
			}

		}
		return returnflag;
	}

	public static void getLatestAddendumInfo(String Appendix_Id,
			ActivityListBean activitylistBean, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lp_latest_addendum_id( '"
					+ Appendix_Id + "' )";

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				activitylistBean.setLatestaddendum_id(rs.getString("lm_js_id"));
				activitylistBean.setLatestaddendum_name(rs
						.getString("lm_js_title"));
				activitylistBean.setLatestaddendum_type(rs
						.getString("lm_js_type"));
			}

		}

		catch (SQLException e) {
			logger.error(
					"getLatestAddendumInfo(String, ActivityListForm, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getLatestAddendumInfo(String, ActivityListForm, DataSource) - Exception caught in getting activities while getting addendum activities"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getLatestAddendumInfo(String, ActivityListForm, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getLatestAddendumInfo(String, ActivityListForm, DataSource) - Exception caught in getting activities while getting addendum activities"
							+ sql);
				}
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.error(
						"getLatestAddendumInfo(String, ActivityListForm, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getLatestAddendumInfo(String, ActivityListForm, DataSource) - Exception caught in getting activities while getting addendum activities"
							+ sql);
				}

			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.error(
						"getLatestAddendumInfo(String, ActivityListForm, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getLatestAddendumInfo(String, ActivityListForm, DataSource) - Exception caught in getting activities while getting addendum activities"
							+ sql);
				}

			}
		}

		return;
	}

	public static String getLatestAddendumId(String Appendix_Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String id = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lp_latest_addendum_id( '"
					+ Appendix_Id + "' )";

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				id = rs.getString("lm_js_id");

			}

		}

		catch (SQLException e) {
			logger.error(
					"getLatestAddendumId(" + Appendix_Id + ", " + ds + ")", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getLatestAddendumId(String, DataSource) - Exception caught in getting latest addendum id"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getLatestAddendumId(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getLatestAddendumId(String, DataSource) - Exception caught in getting latest addendum id"
							+ sql);
				}
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.error("getLatestAddendumId(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getLatestAddendumId(String, DataSource) - Exception caught in getting latest addendum id"
							+ sql);
				}

			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.error("getLatestAddendumId(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getLatestAddendumId(String, DataSource) - Exception caught in getting latest addendum id"
							+ sql);
				}

			}
		}

		return id;
	}

	public static ArrayList getActivityToCopyList(String Appendix_Id,
			String Job_Id, DataSource ds) {
		ArrayList all_activities = new ArrayList();
		Connection conn = null;
		Activity activity = null;

		Statement stmt = null;
		ResultSet rs2 = null;

		ResultSet rs = null;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		CallableStatement cstmt = null;

		// System.out.println("in getActivityToCopyList Appendix_Id::::::::"+Appendix_Id);
		// System.out.println("in getActivityToCopyList Job_Id::::::::"+Job_Id);

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lm_activity_copy(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, Appendix_Id);
			cstmt.setString(3, Job_Id);
			rs = cstmt.executeQuery();

			stmt = conn.createStatement();
			String qry = "SELECT 1  FROM lm_activity WHERE lm_at_js_id = "
					+ Job_Id
					/*
					 * Used lm_at_lib_id 3 times as one for local DB, 2nd for
					 * staging and 3rd for production so that it may work for
					 * all DBs, Here lm_at_lib_id = 15116 for local for
					 * verification/production lm_at_lib_id = 15549
					 */
					+ " and  (lm_at_lib_id = 15116 OR lm_at_lib_id = 15549) and lm_at_title like('freight')";
			rs2 = stmt.executeQuery(qry);
			boolean isFreightAdded = false;
			if (rs2.next()) {
				int a = rs2.getInt(1);
				if (a == 1) {
					// if freight activity already added
					isFreightAdded = true;
				}
			}

			while (rs.next()) {
				if (rs.getString("lm_at_title").equals("Freight")
						&& isFreightAdded) {
					// if freight activity already added then no need to add it
					// again in activity list.
					continue;
				}
				activity = new Activity();
				// System.out.println("lm_js_id::::::::::::::::"+rs.getString(
				// "lm_js_id" ));
				activity.setTemp_jobid(rs.getString("lm_js_id"));
				activity.setJob_name(rs.getString("lm_js_title"));
				activity.setActivity_Id(rs.getString("lm_at_id"));
				activity.setName(rs.getString("lm_at_title"));
				activity.setActivitytype(rs.getString("lm_at_type"));
				activity.setActivitytypecombo(rs.getString("rew"));
				activity.setStatus(rs.getString("lm_at_status"));

				activity.setEstimated_materialcost(dfcur.format(rs
						.getFloat("lx_ce_material_cost")) + "");
				activity.setEstimated_contractfieldlaborcost(dfcur.format(rs
						.getFloat("lx_ce_ctr_field_labor_cost")) + "");
				activity.setEstimated_cnsfieldlaborcost(dfcur.format(rs
						.getFloat("lx_ce_cns_field_labor_cost")) + "");
				activity.setEstimated_cnshqlaborcost(dfcur.format(rs
						.getFloat("lx_ce_cns_hq_labor_cost")) + "");

				activity.setEstimated_freightcost(dfcur.format(rs
						.getFloat("lx_ce_frieght_cost")) + "");
				activity.setEstimated_travelcost(dfcur.format(rs
						.getFloat("lx_ce_travel_cost")) + "");
				activity.setQuantity(rs.getString("lm_at_quantity"));

				activity.setEstimated_totalcost(dfcur.format(rs
						.getFloat("lx_ce_total_cost")) + "");
				activity.setExtendedprice(dfcur.format(rs
						.getFloat("lx_ce_total_price")) + "");
				activity.setOverheadcost(dfcur.format(rs
						.getFloat("lx_ce_overhead_cost")) + "");

				activity.setListprice(dfcur.format(rs
						.getFloat("lx_ce_list_price")) + "");

				if (activity.getListprice().equals("0.00"))
					activity.setProformamargin("0.00");
				else {
					activity.setProformamargin(dfcur.format(1 - (rs
							.getFloat("lx_ce_total_cost") / rs
							.getFloat("lx_ce_list_price")))
							+ "");
				}
				activity.setEstimated_cnsfieldhqlaborcost(dfcur.format(rs
						.getFloat("lx_ce_cns_field_labor_cost")
						+ rs.getFloat("lx_ce_cns_hq_labor_cost")) + "");
				activity.setFlag(rs.getString("fromflag"));
				activity.setBidFlag(rs.getString("lm_at_bid_flag"));
				all_activities.add(activity);
			}
		}

		catch (SQLException e) {
			logger.error("getActivityToCopyList(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityToCopyList(String, String, DataSource) - Exception caught in getting activities to copy"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getActivityToCopyList(String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityToCopyList(String, String, DataSource) - Exception caught in getting activities to copy"
							+ sql);
				}
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getActivityToCopyList(String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityToCopyList(String, String, DataSource) - Exception caught in getting activities to copy"
							+ sql);
				}
			}
			try {
				conn.close();
			} catch (SQLException sql) {
				logger.error(
						"getActivityToCopyList(String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityToCopyList(String, String, DataSource) - Exception caught in getting activities to copy"
							+ sql);
				}
			}
		}

		return all_activities;
	}

}
