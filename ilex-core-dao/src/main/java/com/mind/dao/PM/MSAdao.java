/* This class defines methods for MSA Creation , Updation , Deletion 
 * Method Name: getMSAList
 * Method Argument: 2 Argument DataSource ds , String querystring
 * Method Description: Retrieve all the MSAs
 * retrun type:ArrayList 

 * Method Name: addMSA
 * Method Argument: 2 Argument MSA msa , DataSource ds
 * Method Description: Create a MSA
 * retrun type:void

 * Method Name: updataMSAList
 * Method Argument: 2 Argument MSA msa , DataSource ds
 * Method Description: Update an MSA
 * retrun type:void

 * Method Name: getMSA
 * Method Argument: 2 Argument String Id , DataSource ds
 * Method Description: Retrieve MSA for Id passed.
 * retrun type:MSA
 * see   setMSA function
 */

package com.mind.dao.PM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;


import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.pm.MSAEditBean;
import  com.mind.bean.Owner;
import com.mind.bean.Status;


public class MSAdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MSAdao.class);

	public static String getProposalTreeSql(String msaOwner, String msaStatus,
			String appendixOwner, String appendixStatus, String monthMsa,
			String weekMsa, String monthAppendix, String weekAppendix,
			String type, String tabCondition, DataSource ds) throws Exception {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String result = "";
		int val = -1;
		try {
			conn = ds.getConnection();
			cstmt =
					conn
							.prepareCall("{?=call dbo.generate_tree(?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			cstmt.registerOutParameter(12, java.sql.Types.VARCHAR);
			cstmt.setString(2, msaOwner);
			cstmt.setString(3, msaStatus);
			cstmt.setString(4, appendixOwner);
			cstmt.setString(5, appendixStatus);
			cstmt.setString(6, monthMsa);
			cstmt.setString(7, weekMsa);
			cstmt.setString(8, monthAppendix);
			cstmt.setString(9, weekAppendix);
			cstmt.setString(10, type);
			cstmt.setString(11, tabCondition);
			cstmt.execute();
			val = cstmt.getInt(1);
			result = cstmt.getString(12);
		} catch (Exception e) {
			logger
					.error(
							"getProposalTreeSql(String, String, String, String, String, String, String, String, String, DataSource)",
							e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("getProposalTreeSql(String, String, String, String, String, String, String, String, String, DataSource) - Error Occured in Getting  Proposal Tree String"
								+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException sql) {
				logger
						.warn(
								"getProposalTreeSql(String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
								sql);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger
						.warn(
								"getProposalTreeSql(String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
								sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger
						.warn(
								"getProposalTreeSql(String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
								sql);
			}
		}

		return result;
	}

	
	public static String getMSAUploadCheck(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String MSAUploadCheck = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql =
					"select lp_md_uploaded_flag from dbo.func_lp_msa_tab('"
							+ Id + "')";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				MSAUploadCheck = (rs.getString("lp_md_uploaded_flag"));
			}
		}

		catch (Exception e) {
			logger.error("getMSAUploadCheck(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("getMSAUploadCheck(String, DataSource) - Exception in retrieving MSA Upload Flag "
								+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getMSAUploadCheck(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getMSAUploadCheck(String, DataSource) - Exception in retrieving MSA Upload Flag"
									+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getMSAUploadCheck(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getMSAUploadCheck(String, DataSource) - Exception in retrieving MSA Upload Flag"
									+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getMSAUploadCheck(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getMSAUploadCheck(String, DataSource) - Exception in retrieving MSA"
									+ sql);
				}
			}

		}
		return MSAUploadCheck;
	}

	public static String getMSAStatus(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String MSAStatus = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql =
					"select lp_md_status from dbo.func_lp_msa_tab('" + Id
							+ "')";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				MSAStatus = (rs.getString("lp_md_status"));
			}
		}

		catch (Exception e) {
			logger.error("getMSAStatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("getMSAStatus(String, DataSource) - Exception in retrieving MSA Status"
								+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getMSAStatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getMSAStatus(String, DataSource) - Exception in retrieving MSA Status"
									+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getMSAStatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getMSAStatus(String, DataSource) - Exception in retrieving MSA Status"
									+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getMSAStatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getMSAStatus(String, DataSource) - Exception in retrieving MSA"
									+ sql);
				}
			}

		}
		return MSAStatus;
	}

	public static ArrayList getOwnerList(String loginid, String otherCheck,
			String ownerType, String id, DataSource ds) {
		ArrayList ownerlist = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Owner msaOwner = null;
		String sql = "";

		/*
		 * System.out.println("Login ID ::" + loginid);
		 * System.out.println("OtherCheck is::" + otherCheck);
		 * System.out.println("OwnerType is ::" + ownerType);
		 * System.out.println("MSA_ID ::" + id);
		 */

		if (otherCheck == null)
			otherCheck = "";

		if (id.equals("-1")) {
			ownerType = "dispatch";
			id = "0";
		}
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql =
					"select * from dbo.func_get_owner_list( '" + loginid
							+ "','" + ownerType + "','" + otherCheck + "','"
							+ id + "' )";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				msaOwner = new Owner();
				msaOwner.setOwnerId(rs.getString("owner_id"));
				msaOwner.setOwnerName(rs.getString("owner_name"));
				ownerlist.add(msaOwner);
			}
		} catch (Exception e) {
			logger.error(
					"getOwnerList(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("getOwnerList(String, String, String, String, DataSource) - Exception caught in rerieving MSA Owner List"
								+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql1) {
				logger
						.warn(
								"getOwnerList(String, String, String, String, DataSource) - exception ignored",
								sql1);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql1) {
				logger
						.warn(
								"getOwnerList(String, String, String, String, DataSource) - exception ignored",
								sql1);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql1) {
				logger
						.warn(
								"getOwnerList(String, String, String, String, DataSource) - exception ignored",
								sql1);
			}

		}
		return ownerlist;
	}

	public static ArrayList getStatusList(String type, DataSource ds) {
		ArrayList statuslist = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Status msa = null;
		String sql = "";
		int i = 0;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select * from dbo.func_get_status_list( '" + type + "' )";

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				msa = new Status();
				msa.setStatusid(rs.getString("cc_fs_status"));
				msa.setStatusdesc(rs.getString("cc_status_description"));
				statuslist.add(msa);
			}
		} catch (Exception e) {
			logger.error("getStatusList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("getStatusList(String, DataSource) - Exception caught in rerieving MSA Status List"
								+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql1) {
				logger
						.warn(
								"getStatusList(String, DataSource) - exception ignored",
								sql1);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql1) {
				logger
						.warn(
								"getStatusList(String, DataSource) - exception ignored",
								sql1);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql1) {
				logger
						.warn(
								"getStatusList(String, DataSource) - exception ignored",
								sql1);
			}

		}
		return statuslist;
	}

	public static ArrayList getMSAList(DataSource ds, String querystring,
			String monthMSA, String weekMSA, String status, String Owner,
			String tabFilter) {
		ArrayList al_MSAs = new ArrayList();
		Connection conn = null;
		MSA msa = null;
		Statement stmt = null;

		/*
		 * System.out.println("MSA::::"); System.out.println("Month:::: "
		 * +monthMSA ); System.out.println("Week:::: " + weekMSA);
		 * System.out.println("Status:::: " + status);
		 * System.out.println("Owner:::: " + Owner);
		 */

		String sql_list =
				"select lp_mm_id , lx_pi_id , msatype , lx_pi_project_type , lx_pi_sales_type , lo_ot_name , lp_md_formal_msa_date ,"
						+ "lp_mm_ot_id , lp_md_cns_sales_pc_id , SalesPOCname , lp_md_union_uplift_factor , lp_md_payment_terms , "
						+ "lp_md_payment_terms_qualifier , lp_md_need_date , lp_md_effective_date , lp_md_planned_duration , "
						+ "lp_md_draft_review , lp_md_business_review , lp_md_business_pc_id , BusinessPOCname , "
						+ "lp_md_contract_pc_id , ContractPOCname , lp_md_status , lp_md_uploaded_flag , lp_md_change_date , "
						+ "lp_md_changed_by, msa_status from dbo.func_lp_msa_tab('%') where 1=1 "
						+ tabFilter + " ";

		// BY default MSA would be sorted by name
		if (querystring.equals("")) {
			querystring = "  order by lo_ot_name";
		}
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (monthMSA != null) {

				sql_list =
						sql_list
								+ "	and	( "
								+ " ( datepart(mm, lp_md_need_date) =  datepart(mm, getdate())and datepart(yy, lp_md_need_date) =  datepart(yy, getdate()) )  "
								+ " or "
								+ " ( datepart(mm, lp_md_effective_date) =  datepart(mm, getdate())and datepart(yy, lp_md_effective_date) =  datepart(yy, getdate()) ) "
								+ " ) ";
			}
			if (weekMSA != null) {

				sql_list =
						sql_list
								+ "	and	( "
								+ " ( datepart(wk, lp_md_need_date) =  datepart(wk, getdate())and datepart(yy, lp_md_need_date) =  datepart(yy, getdate()) )  "
								+ " or "
								+ " ( datepart(wk, lp_md_effective_date) =  datepart(wk, getdate())and datepart(yy, lp_md_effective_date) =  datepart(yy, getdate()) ) "
								+ " ) ";
			}

			if (status != "" && status != null) {
				sql_list = sql_list + "and msa_status in " + status + " ";
			}

			if ((!Owner.contains("%")) && (!Owner.trim().equals(""))
					&& Owner != null)
				sql_list =
						sql_list + " and lp_md_cns_sales_pc_id in " + Owner
								+ " ";

			if (status != null && !status.equals("") && Owner != null
					&& !Owner.equals("")) {
				sql_list = sql_list + querystring;
			} else {
				sql_list =
						"select * from dbo.func_lp_msa_tab('%') where 1!=1"
								+ tabFilter; // This
				// query
				// does
				// not
				// generate
				// any
				// result.
			}

			//System.out.println("Final Query for the MSA is ::: " + sql_list);

			rs = stmt.executeQuery(sql_list);
			al_MSAs = setMSA(rs);

		} catch (SQLException e) {
			logger
					.error(
							"getMSAList(DataSource, String, String, String, String, String)",
							e);

			logger
					.error(
							"getMSAList(DataSource, String, String, String, String, String)",
							e);
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger
						.error(
								"getMSAList(DataSource, String, String, String, String, String)",
								s);

				logger
						.error(
								"getMSAList(DataSource, String, String, String, String, String)",
								s);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger
						.error(
								"getMSAList(DataSource, String, String, String, String, String)",
								sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getMSAList(DataSource, String, String, String, String, String) - Exception caught in getting all MSAs"
									+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger
						.error(
								"getMSAList(DataSource, String, String, String, String, String)",
								sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getMSAList(DataSource, String, String, String, String, String) - Exception caught in getting all MSAs"
									+ sql);
				}
			}

		}
		return al_MSAs;
	}

	public static MSAEditBean getMSA(MSAEditBean msaEditForm,
			String sortQueryClause, DataSource ds) {
		ArrayList al_MSAs = new ArrayList();
		Connection conn = null;
		MSA msa = null;
		Statement stmt = null;

		if (sortQueryClause.equals("")) {
			sortQueryClause = "order by lo_ot_name";
		}
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql =
					"select lp_mm_id , lx_pi_id , msatype , lx_pi_project_type , lx_pi_sales_type , lo_ot_name , lp_md_formal_msa_date ,"
							+ "lp_md_cns_sales_pc_id ,  lp_md_union_uplift_factor , lp_md_payment_terms , "
							+ "lp_md_payment_terms_qualifier , lp_md_need_date , lp_md_effective_date , lp_md_planned_duration , "
							+ "lp_md_draft_review , lp_md_business_review , lp_md_business_pc_id , "
							+ "lp_md_contract_pc_id , lp_md_status , lp_md_uploaded_flag , lp_md_change_date, lp_md_change_time, lp_md_changed_by, "
							+ "lp_md_create_date, lp_md_create_time, lp_md_created_by from dbo.func_lp_msa_tab('"
							+ msaEditForm.getMsaId() + "') " + sortQueryClause;

			// System.out.println("sql:::::::::::"+sql);

			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				msaEditForm.setMsaName(rs.getString("lo_ot_name"));
				msaEditForm.setMsaType(rs.getString("msatype"));
				msaEditForm.setSalesType(rs.getString("lx_pi_sales_type"));
				msaEditForm.setSalesPOC(rs.getString("lp_md_cns_sales_pc_id"));
				msaEditForm.setUnionUpliftFactor(rs
						.getString("lp_md_union_uplift_factor"));

				msaEditForm.setPayTerms(rs.getString("lp_md_payment_terms"));
				msaEditForm.setPayQualifier(rs
						.getString("lp_md_payment_terms_qualifier"));

				msaEditForm.setReqPlannedSch(rs.getString("lp_md_need_date"));
				msaEditForm.setEffPlannedSch(rs
						.getString("lp_md_effective_date"));
				msaEditForm.setSchDays(rs.getString("lp_md_planned_duration"));

				msaEditForm.setDraftReviews(rs.getString("lp_md_draft_review"));
				msaEditForm.setBusinessReviews(rs
						.getString("lp_md_business_review"));

				msaEditForm.setCustPOCBusiness(rs
						.getString("lp_md_business_pc_id"));
				msaEditForm.setCustPOCContract(rs
						.getString("lp_md_contract_pc_id"));

				msaEditForm.setCreateInfo(rs.getString("lp_md_created_by")
						+ " " + rs.getString("lp_md_create_date") + " "
						+ rs.getString("lp_md_create_time"));
				msaEditForm.setChangeInfo(rs.getString("lp_md_changed_by")
						+ " " + rs.getString("lp_md_change_date") + " "
						+ rs.getString("lp_md_change_time"));
			}
		}

		catch (SQLException e) {
			logger.error("getMSA(MSAEditForm, String, DataSource)", e);

			logger.error("getMSA(MSAEditForm, String, DataSource)", e);
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.error("getMSA(MSAEditForm, String, DataSource)", s);

				logger.error("getMSA(MSAEditForm, String, DataSource)", s);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getMSA(MSAEditForm, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getMSA(MSAEditForm, String, DataSource) - Exception caught in getting all MSAs"
									+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getMSA(MSAEditForm, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getMSA(MSAEditForm, String, DataSource) - Exception caught in getting all MSAs"
									+ sql);
				}
			}

		}
		return msaEditForm;
	}

	public static int addMSA(MSA msa, String loginuserid, DataSource ds)
			throws Exception {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int val = -1;
		try {
			conn = ds.getConnection();

			cstmt =
					conn
							.prepareCall("{?=call dbo.lp_msa_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);

			cstmt.setInt(3, Integer.parseInt(msa.getName()));
			cstmt.setString(4, msa.getMsatypecombo());
			cstmt.setString(5, msa.getSalestypecombo());
			cstmt.setString(6, msa.getUnionupliftfactor());
			cstmt.setString(7, "D");
			cstmt.setInt(8, Integer.parseInt(msa.getSalesPOCcombo()));
			cstmt.setInt(9, 0);
			cstmt.setInt(10, 0);

			cstmt.setString(11, msa.getPaytermscombo());
			cstmt.setString(12, msa.getPayqualifiercombo());
			cstmt.setString(13, msa.getBusinessreviewscombo());
			cstmt.setString(14, msa.getDraftreviewscombo());
			cstmt.setString(15, msa.getReqplasch());
			cstmt.setString(16, msa.getEffplasch());
			cstmt.setInt(17, Integer.parseInt(msa.getSchdays()));
			cstmt.setInt(18, Integer.parseInt(loginuserid));
			cstmt.setString(19, "A");

			cstmt.execute();
			val = cstmt.getInt(1);

		}

		catch (Exception e) {
			logger.error("addMSA(MSA, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("addMSA(MSA, String, DataSource) - Error Occured in Inserting MSA"
								+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException sql) {
				logger.warn(
						"addMSA(MSA, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addMSA(MSA, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addMSA(MSA, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int updateMSAList(MSA msa, String loginuserid, DataSource ds)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		CallableStatement cstmt = null;
		int val = -1;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			cstmt =
					conn
							.prepareCall("{?=call dbo.lp_msa_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(msa.getMsa_Id()));

			cstmt.setInt(3, 0);
			cstmt.setString(4, msa.getMsatypecombo());
			cstmt.setString(5, msa.getSalestypecombo());
			cstmt.setString(6, msa.getUnionupliftfactor());

			cstmt.setString(7, "0");
			cstmt.setInt(8, Integer.parseInt(msa.getSalesPOCcombo()));
			cstmt.setInt(9, Integer.parseInt(msa.getCustPocbusinesscombo()));
			cstmt.setInt(10, Integer.parseInt(msa.getCustPoccontractcombo()));
			cstmt.setString(11, msa.getPaytermscombo());
			cstmt.setString(12, msa.getPayqualifiercombo());
			cstmt.setString(13, msa.getBusinessreviewscombo());
			cstmt.setString(14, msa.getDraftreviewscombo());
			cstmt.setString(15, msa.getReqplasch());
			cstmt.setString(16, msa.getEffplasch());
			cstmt.setInt(17, Integer.parseInt(msa.getSchdays()));
			cstmt.setInt(18, Integer.parseInt(loginuserid));
			cstmt.setString(19, "U");
			cstmt.execute();

			val = cstmt.getInt(1);

		}

		catch (Exception e) {
			logger.error("updateMSAList(MSA, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("updateMSAList(MSA, String, DataSource) - Exception caught in updating MSA"
								+ e);
			}
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error("updateMSAList(MSA, String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger
							.debug("updateMSAList(MSA, String, DataSource) - Exception in updating MSA"
									+ e);
				}
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.error("updateMSAList(MSA, String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger
							.debug("updateMSAList(MSA, String, DataSource) - Exception in updating MSA"
									+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("updateMSAList(MSA, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("updateMSAList(MSA, String, DataSource) - Exception in updating MSA"
									+ sql);
				}
			}
		}
		return val;
	}

	public static int editMSA(MSAEditBean msaForm, String loginuserid,
			DataSource ds) throws Exception {
		Connection conn = null;
		Statement stmt = null;
		CallableStatement cstmt = null;
		int val = -1;

		/*
		 * System.out.println(" 2::::"+Integer.parseInt( msaForm.getMsaId() ) );
		 * System.out.println(" 3::::"+Integer.parseInt( msaForm.getOrgTopId())
		 * ); System.out.println(" 4::::"+msaForm.getMsaType() );
		 * System.out.println(" 5::::"+msaForm.getSalesType() );
		 * System.out.println(" 6::::"+msaForm.getUnionUpliftFactor() );
		 * System.out.println(" 7 ::::0" );
		 * System.out.println(" 8::::"+Integer.parseInt( msaForm.getSalesPOC() )
		 * ); System.out.println(" 9::::"+Integer.parseInt(
		 * msaForm.getCustPOCBusiness() ) );
		 * System.out.println(" 10::::"+Integer.parseInt(
		 * msaForm.getCustPOCContract() ) );
		 * System.out.println(" 11::::"+msaForm.getPayTerms() );
		 * System.out.println(" 12::::"+msaForm.getPayQualifier() );
		 * System.out.println(" 13::::"+msaForm.getBusinessReviews() );
		 * System.out.println(" 14::::"+msaForm.getDraftReviews() );
		 * System.out.println(" 15::::"+msaForm.getReqPlannedSch() );
		 * System.out.println(" 16::::"+msaForm.getEffPlannedSch() );
		 * System.out.println(" 17::::"+Integer.parseInt( msaForm.getSchDays() )
		 * ); System.out.println(" 18::::"+Integer.parseInt( loginuserid ) );
		 * System.out.println(" 19::::"+msaForm.getAction() );
		 */

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			cstmt =
					conn
							.prepareCall("{?=call dbo.lp_msa_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(msaForm.getMsaId()));
			cstmt.setInt(3, Integer.parseInt(msaForm.getOrgTopId()));
			cstmt.setString(4, msaForm.getMsaType());
			cstmt.setString(5, msaForm.getSalesType());
			cstmt.setString(6, msaForm.getUnionUpliftFactor());
			cstmt.setString(7, "0");
			cstmt.setInt(8, Integer.parseInt(msaForm.getSalesPOC()));

			cstmt.setInt(9, Integer.parseInt(msaForm.getCustPOCBusiness()));
			cstmt.setInt(10, Integer.parseInt(msaForm.getCustPOCContract()));
			cstmt.setString(11, msaForm.getPayTerms());
			cstmt.setString(12, msaForm.getPayQualifier());
			cstmt.setString(13, msaForm.getBusinessReviews());
			cstmt.setString(14, msaForm.getDraftReviews());
			cstmt.setString(15, msaForm.getReqPlannedSch());
			cstmt.setString(16, msaForm.getEffPlannedSch());
			cstmt.setInt(17, Integer.parseInt(msaForm.getSchDays()));
			cstmt.setInt(18, Integer.parseInt(loginuserid));
			cstmt.setString(19, msaForm.getActionAddUpdate());
			cstmt.execute();

			val = cstmt.getInt(1);

			// System.out.println("val::::::::::"+val);
		}

		catch (Exception e) {
			logger.error("editMSA(MSAEditForm, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("editMSA(MSAEditForm, String, DataSource) - Exception caught in updating MSA"
								+ e);
			}
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error("editMSA(MSAEditForm, String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger
							.debug("editMSA(MSAEditForm, String, DataSource) - Exception in updating MSA"
									+ e);
				}
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.error("editMSA(MSAEditForm, String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger
							.debug("editMSA(MSAEditForm, String, DataSource) - Exception in updating MSA"
									+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("editMSA(MSAEditForm, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("editMSA(MSAEditForm, String, DataSource) - Exception in updating MSA"
									+ sql);
				}
			}
		}
		return val;
	}

	public static MSA getMSA(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		MSA msa = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			/*
			 * String sql =
			 * "select t1.lp_mm_id ,lx_pi_id ,lx_pi_project_type ,lx_pi_sales_type , lo_ot_name , lp_md_formal_msa_date = isnull(convert(varchar(10) ,lp_md_formal_msa_date,101),'') , "
			 * +
			 * "lp_mm_ot_id ,lp_md_cns_sales_pc_id , SalesPOCfirstname ,SalesPOClastname, lp_md_union_uplift_factor , lp_md_payment_terms , "
			 * +
			 * "lp_md_payment_terms_qualifier , lp_md_need_date , lp_md_effective_date , lp_md_planned_duration ,"
			 * +
			 * "lp_md_draft_review , lp_md_business_review ,lp_md_business_pc_id , BusinessPOCfirstname , "
			 * +
			 * "BusinessPOClastname , lp_md_contract_pc_id , ContractPOCfirstname , ContractPOClastname ,lp_md_status"
			 * +
			 * " from (select lp_mm_id ,lx_pi_id , lo_ot_name , lp_md_formal_msa_date = isnull(convert(varchar(10) ,lp_md_formal_msa_date,101),'') , lp_mm_ot_id, lx_pi_project_type ,lx_pi_sales_type , "
			 * +
			 * "lp_md_cns_sales_pc_id , lo_pc_first_name as SalesPOCfirstname , lo_pc_last_name as SalesPOClastname, "
			 * +
			 * "lp_md_union_uplift_factor , lp_md_payment_terms, lp_md_payment_terms_qualifier , replace(convert(varchar(12) , lp_md_need_date,101) , '01/01/1900','') as lp_md_need_date, "
			 * +
			 * "replace(convert(varchar(12), lp_md_effective_date , 101) , '01/01/1900','') as lp_md_effective_date ,"
			 * +
			 * "lp_md_planned_duration , lp_md_draft_review , lp_md_business_review , lp_md_status from lp_msa_detail ,  "
			 * +
			 * "lo_organization_top  ,lp_msa_main , lo_poc,  lx_project_identifier_main where lp_md_cns_sales_pc_id = lo_pc_id "
			 * +
			 * "and lp_mm_ot_id = lo_ot_id and lp_md_mm_id = lp_mm_id and lp_mm_pi_id = lx_pi_id and lp_md_mm_id = lp_mm_id) as t1, "
			 * +
			 * "(select lp_mm_id, lp_md_business_pc_id , lo_pc_first_name as BusinessPOCfirstname , "
			 * +
			 * "lo_pc_last_name as BusinessPOClastname from lp_msa_detail,lp_msa_main  , lo_poc where "
			 * +
			 * "lp_md_business_pc_id = lo_pc_id and lp_md_mm_id = lp_mm_id) t2, (select lp_mm_id, lp_md_contract_pc_id , "
			 * +
			 * "lo_pc_first_name as ContractPOCfirstname , lo_pc_last_name as ContractPOClastname from lp_msa_detail , "
			 * +
			 * "lp_msa_main , lo_poc where lp_md_contract_pc_id = lo_pc_id and lp_md_mm_id = lp_mm_id) t3 where "
			 * +
			 * "t1.lp_mm_id = t2.lp_mm_id and t2.lp_mm_id = t3.lp_mm_id and t3.lp_mm_id = t1.lp_mm_id and t1.lp_mm_id="
			 * +Id;
			 */

			String sql =
					"select lp_mm_id , lx_pi_id , msatype , lx_pi_project_type , lx_pi_sales_type , lo_ot_name , lp_md_formal_msa_date , "
							+ "lp_mm_ot_id , lp_md_cns_sales_pc_id , SalesPOCname , lp_md_union_uplift_factor , lp_md_payment_terms , "
							+ "lp_md_payment_terms_qualifier , lp_md_need_date , lp_md_effective_date , lp_md_planned_duration , "
							+ "lp_md_draft_review , lp_md_business_review , lp_md_business_pc_id , BusinessPOCname , "
							+ "lp_md_contract_pc_id , ContractPOCname , lp_md_status , lp_md_uploaded_flag , lp_md_change_date , "
							+ "lp_md_changed_by from dbo.func_lp_msa_tab('"
							+ Id + "')";

			rs = stmt.executeQuery(sql);
			msa = new MSA();
			ArrayList MSAList = setMSA(rs);

			msa = (MSA) MSAList.get(0);

			sql =
					"select com_desc , com_by , comment_date from dbo.func_last_comment('"
							+ msa.getMsa_Id() + "' , 'MSA')";

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				msa.setLastcomment(rs.getString("com_desc"));
				msa.setLastchangeby(rs.getString("com_by"));
				msa.setLastchangedate(rs.getString("comment_date"));
			}
		}

		catch (Exception e) {
			logger.error("getMSA(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("getMSA(String, DataSource) - Exception in retrieving MSA"
								+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getMSA(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getMSA(String, DataSource) - Exception in retrieving MSA"
									+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getMSA(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getMSA(String, DataSource) - Exception in retrieving MSA"
									+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getMSA(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getMSA(String, DataSource) - Exception in retrieving MSA"
									+ sql);
				}
			}

		}
		return msa;
	}

	public static ArrayList setMSA(ResultSet rs) {
		ArrayList MSAList = new ArrayList();
		MSA msa;
		String temp = "";
		Collection tempoc = null;
		try {
			while (rs.next()) {
				msa = new MSA();
				msa.setMsa_Id(rs.getString("lp_mm_id"));

				msa.setName(rs.getString("lo_ot_name"));
				msa.setOrgid(rs.getString("lp_mm_ot_id"));
				msa.setProjectid(rs.getString("lx_pi_id"));

				msa.setMsatype(rs.getString("lx_pi_project_type"));

				msa.setMsatypecombo(rs.getString("msatype"));
				if (rs.getString("lx_pi_sales_type") != null) {
					if (rs.getString("lx_pi_sales_type").equals("D"))
						temp = "Direct";
					else
						temp = "Channel";
				}
				msa.setSalestype(temp);
				msa.setSalestypecombo(rs.getString("lx_pi_sales_type"));

				msa.setSalesPOC(rs.getString("SalesPOCname"));
				// msa.setSalesPOC( rs.getString( "SalesPOCfirstname" )+ " " +
				// rs.getString( "SalesPOClastname" ) );
				msa.setSalesPOCcombo(rs.getString("lp_md_cns_sales_pc_id"));

				msa.setUnionupliftfactor(rs
						.getString("lp_md_union_uplift_factor"));
				msa.setFormaldocdate(rs.getString("lp_md_formal_msa_date"));

				msa.setPayterms(rs.getString("lp_md_payment_terms"));
				msa.setPaytermscombo(rs.getString("lp_md_payment_terms"));

				msa.setPayqualifier(rs
						.getString("lp_md_payment_terms_qualifier"));
				msa.setPayqualifiercombo(rs
						.getString("lp_md_payment_terms_qualifier"));

				msa.setReqplasch(rs.getString("lp_md_need_date"));
				msa.setEffplasch(rs.getString("lp_md_effective_date"));
				msa.setSchdays(rs.getString("lp_md_planned_duration"));

				if (rs.getString("lp_md_draft_review") != null) {
					if (rs.getString("lp_md_draft_review").equals("S"))
						temp = "Standard";
					else
						temp = "Expedite";
				}
				msa.setDraftreviews(temp);
				msa.setDraftreviewscombo(rs.getString("lp_md_draft_review"));

				if (rs.getString("lp_md_business_review") != null) {
					if (rs.getString("lp_md_business_review").equals("S"))
						temp = "Standard";
					else
						temp = "Expedite";
				}
				msa.setBusinessreviews(temp);
				msa.setBusinessreviewscombo(rs
						.getString("lp_md_business_review"));

				msa.setCustPocbusiness(rs.getString("BusinessPOCname"));
				// msa.setCustPocbusiness( rs.getString( "BusinessPOCfirstname"
				// )+ " " + rs.getString( "BusinessPOClastname" ) );
				msa.setCustPocbusinesscombo(rs
						.getString("lp_md_business_pc_id"));

				msa.setCustPoccontract(rs.getString("ContractPOCname"));
				// msa.setCustPoccontract( rs.getString( "ContractPOCfirstname"
				// )+ " " + rs.getString( "ContractPOClastname" ) );
				msa.setCustPoccontractcombo(rs
						.getString("lp_md_contract_pc_id"));

				msa.setStatus(rs.getString("lp_md_status"));
				msa.setFile_upload_check(rs.getString("lp_md_uploaded_flag"));

				msa.setLastmsamodifieddate(rs.getString("lp_md_change_date"));
				msa.setLastmsamodifiedby(rs.getString("lp_md_changed_by"));

				msa.setPoclist(tempoc);

				MSAList.add(msa);
			}
		} catch (Exception e) {
			logger.error("setMSA(ResultSet)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setMSA(ResultSet) - Exception Caught" + e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error("setMSA(ResultSet)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("setMSA(ResultSet) - Exception Caught" + e);
				}
			}
		}

		return MSAList;
	}

	public static int setformaldocdate(String Id, String date, DataSource ds,
			String type) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int val = -1;

		try {
			conn = ds.getConnection();

			cstmt =
					conn
							.prepareCall("{?=call dbo.lp_msa_formaldocdate( ? , ?, ? ) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(Id));
			cstmt.setString(3, date);
			cstmt.setString(4, type);

			cstmt.execute();
			val = cstmt.getInt(1);

		}

		catch (Exception e) {
			logger.error(
					"setformaldocdate(String, String, DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("setformaldocdate(String, String, DataSource, String) - Exception in setting formaldocumentdate"
								+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sqle) {
				logger.error(
						"setformaldocdate(String, String, DataSource, String)",
						sqle);

				if (logger.isDebugEnabled()) {
					logger
							.debug("setformaldocdate(String, String, DataSource, String) - Exception in setting formaldocumentdate"
									+ sqle);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error(
						"setformaldocdate(String, String, DataSource, String)",
						sqle);

				if (logger.isDebugEnabled()) {
					logger
							.debug("setformaldocdate(String, String, DataSource, String) - Exception in setting formaldocumentdate"
									+ sqle);
				}
			}

		}
		return val;
	}

	public static int deletemsa(String Id, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int val = -1;

		try {
			conn = ds.getConnection();

			cstmt = conn.prepareCall("{?=call dbo.lp_msa_delete_01( ?  ) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(Id));

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("deletemsa(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("deletemsa(String, DataSource) - Exception in DELETING MSA"
								+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sqle) {
				logger.error("deletemsa(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger
							.debug("deletemsa(String, DataSource) - Exception in DELETING MSA"
									+ sqle);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error("deletemsa(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger
							.debug("deletemsa(String, DataSource) - Exception in DELETING MSA"
									+ sqle);
				}
			}

		}
		return val;
	}

	public static String getfile_upload_flag(String MSA_Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String uploadflag = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql =
					"select lp_md_uploaded_flag = isnull( lp_md_uploaded_flag , 'N' ) from lp_msa_detail where lp_md_mm_Id="
							+ MSA_Id;
			rs = stmt.executeQuery(sql);

			if (rs.next())
				uploadflag = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getfile_upload_flag(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("getfile_upload_flag(String, DataSource) - Error Occured in retrieving lp_md_uploaded_flag"
								+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger
						.warn(
								"getfile_upload_flag(String, DataSource) - exception ignored",
								s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger
						.warn(
								"getfile_upload_flag(String, DataSource) - exception ignored",
								s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger
						.warn(
								"getfile_upload_flag(String, DataSource) - exception ignored",
								s);
			}

		}
		return uploadflag;
	}

	public static String getformaldocdatename(DataSource ds, String MSA_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String formaldocdatedate = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql =
					"select lp_md_formal_msa_date = isnull( convert( varchar( 10 ) , lp_md_formal_msa_date , 101 ) , '' ) "
							+ "from lp_msa_detail where lp_md_mm_id=" + MSA_Id;

			rs = stmt.executeQuery(sql);

			if (rs.next())
				formaldocdatedate = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getformaldocdatename(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("getformaldocdatename(DataSource, String) - Error Occured in retrieving formaldocdatedate"
								+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger
						.warn(
								"getformaldocdatename(DataSource, String) - exception ignored",
								s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger
						.warn(
								"getformaldocdatename(DataSource, String) - exception ignored",
								s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger
						.warn(
								"getformaldocdatename(DataSource, String) - exception ignored",
								s);
			}

		}
		return formaldocdatedate;
	}

	public static String getformaldocdateforappendix(DataSource ds,
			String Appendix_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String formaldocdatedate = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql =
					"select lx_pr_date_approved = isnull( convert( varchar( 10 ) , lx_pr_date_approved , 101 ) , '' ) "
							+ "from lx_appendix_main where lx_pr_id="
							+ Appendix_Id;
			// System.out.println("Final Query is :::" + sql);
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				formaldocdatedate = rs.getString(1);
			}
		}

		catch (Exception e) {
			logger.error("getformaldocdateforappendix(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("getformaldocdateforappendix(DataSource, String) - Error Occured in retrieving appendix formaldocdatedate"
								+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger
						.warn(
								"getformaldocdateforappendix(DataSource, String) - exception ignored",
								s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger
						.warn(
								"getformaldocdateforappendix(DataSource, String) - exception ignored",
								s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger
						.warn(
								"getformaldocdateforappendix(DataSource, String) - exception ignored",
								s);
			}

		}
		return formaldocdatedate;
	}

	public static ArrayList getcustomerPOC(String Org_Id, String type,
			DataSource ds) {
		ArrayList POC = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "";
		int tempid = Integer.parseInt(Org_Id);

		query =
				"select * from func_cc_organization_poc_list( " + tempid
						+ " , '" + type + "' )";

		POC.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery(query);

			while (rs.next()) {
				POC.add(new com.mind.common.LabelValue(
						rs.getString("poc_name"), rs.getString("poc_id")));

			}
		} catch (Exception e) {
			logger.error("getcustomerPOC(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("getcustomerPOC(String, String, DataSource) - Exception caught in getting Organisation POC"
								+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.error("getcustomerPOC(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getcustomerPOC(String, String, DataSource) - Exception caught in closing Organisation POC resultset"
									+ sql);
				}
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.error("getcustomerPOC(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getcustomerPOC(String, String, DataSource) - Exception caught in closing Organisation POC statement"
									+ sql);
				}
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.error("getcustomerPOC(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getcustomerPOC(String, String, DataSource) - Exception caught in closing Organisation POC connection"
									+ sql);
				}
			}
		}

		return POC;
	}

	public static String getCurentBdm(String msaId, DataSource ds) {
		String bdmName = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "";
		int tempid = Integer.parseInt(msaId);
		query =
				"select lo_pc_first_name+' '+lo_pc_last_name as lo_pc_name from lp_msa_detail inner join lo_poc on lp_md_cns_sales_pc_id=lo_pc_id where lp_md_mm_id="
						+ tempid;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				bdmName = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("getCurentBdm(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("getCurentBdm(String, DataSource) - Exception caught in getting CurentBdm"
								+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.error("getCurentBdm(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getCurentBdm(String, DataSource) - Exception caught in closing CureentBdm resultset"
									+ sql);
				}
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.error("getCurentBdm(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getCurentBdm(String, DataSource) - Exception caught in closing CureentBdm statement"
									+ sql);
				}
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.error("getCurentBdm(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("getCurentBdm(String, DataSource) - Exception caught in closing CureentBdm connection"
									+ sql);
				}
			}
		}
		return bdmName;

	}

	public static int editMSAForBDM(String msaId, int BdmId, DataSource ds)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		CallableStatement cstmt = null;
		int val = -1;

		// System.out.println(" 2::::"+Integer.parseInt( msaId ));
		// System.out.println(" 3::::"+ BdmId );

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			cstmt = conn.prepareCall("{?=call dbo.lp_msa_bdm_update_01(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, BdmId);
			cstmt.setInt(3, Integer.parseInt(msaId));

			cstmt.execute();

			val = cstmt.getInt(1);

		}

		catch (Exception e) {
			logger.error("editMSAForBDM(String, int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("editMSAForBDM(String, int, DataSource) - Exception caught in updating MSA BDM"
								+ e);
			}
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error("editMSAForBDM(String, int, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger
							.debug("editMSAForBDM(String, int, DataSource) - Exception in updating MSA BDM"
									+ e);
				}
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.error("editMSAForBDM(String, int, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger
							.debug("editMSAForBDM(String, int, DataSource) - Exception in updating MSA BDM"
									+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("editMSAForBDM(String, int, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger
							.debug("editMSAForBDM(String, int, DataSource) - Exception in updating MSA BDM"
									+ sql);
				}
			}
		}
		return val;
	}
}
