package com.mind.dao.PM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.pm.ESAActivityCommissionBean;
import com.mind.bean.pm.ESAActivityList;
import com.mind.bean.pm.ESAEditBean;
import com.mind.bean.pm.ESAList;
import com.mind.common.LabelValue;
import com.mind.fw.core.dao.util.DBUtil;

public class ESADao {
	private static final Logger logger = Logger.getLogger(ESADao.class);

	/**
	 * Gets the ESA agent(partner) list.
	 * 
	 * @param ds
	 *            - data source.
	 * 
	 * @return ArrayList<LabelValue> - ESA agent list.
	 */
	public static ArrayList<LabelValue> getESAAgent(DataSource ds) {
		ArrayList agentCombo = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		agentCombo.add(new com.mind.common.LabelValue("---Select ESA---", "0"));
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from func_get_esa_partner()";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				agentCombo.add(new com.mind.common.LabelValue(rs.getString(2),
						rs.getString(1)));
			}
		} catch (Exception e) {
			logger.error("getESAAgent(DataSource)", e);
		} finally {

			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return agentCombo;
	}

	/**
	 * Gets the ESA details per appendix for ESA SetUp form.
	 * 
	 * @param appendixId
	 *            - appendix id.
	 * @param ds
	 *            - data source.
	 * 
	 * @return ArrayList<ESAList> - array list with ESAList objects.
	 */
	public static ArrayList<ESAList> getAppendixESADetails(String appendixId,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		int recordCounter = 0;
		ArrayList<ESAList> esaAppendixList = new ArrayList<ESAList>();
		String sql = "select * from dbo.func_lp_get_esa_details(?) order by lp_es_seq_no";

		try {
			conn = ds.getConnection();
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, Integer.parseInt(appendixId));
			rs = pStmt.executeQuery();

			for (int i = 0; i < 3; i++) {
				ESAList esaBean = new ESAList();
				esaBean.setEsaId("0");
				esaBean.setEsaSeqNo(i + 1 + "");
				esaBean.setAgent("0");
				esaBean.setCommissionType("Add");
				esaBean.setStatus("1");
				esaAppendixList.add(i, esaBean);
			}

			while (rs.next()) {
				recordCounter++;
				ESAList esaBean = new ESAList();
				esaBean.setEsaId(rs.getString("lp_es_id"));
				esaBean.setEsaSeqNo(rs.getString("lp_es_seq_no"));
				esaBean.setAgent(rs.getString("agent_id"));
				esaBean.setEffectiveDate(rs.getString("effective_date"));
				esaBean.setCommission(rs.getString("commission_rate"));
				esaBean.setCommissionType(rs.getString("commission_type"));
				esaBean.setStatus(rs.getString("agent_status"));
				esaBean.setPayementTerms(rs.getString("payment_terms"));
				esaAppendixList.set(
						Integer.parseInt(esaBean.getEsaSeqNo()) - 1, esaBean);
			}
		} catch (Exception e) {
			logger.error("getAppendixESADetails(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixESADetails(String, DataSource) - Error retrieving ESA details for an appendix  ");
			}
			e.printStackTrace();
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return esaAppendixList;
	}

	/**
	 * Save ESA setup details for an appendix from ESA setup page in database.
	 * 
	 * @param esaBean
	 *            - ESA setup bean.
	 * @param loginuserid
	 *            - login user id.
	 * @param ds
	 *            - data source.
	 * 
	 * @return - return code (int).
	 */
	public static int saveProjectESAinDB(ESAEditBean esaBean,
			String loginuserid, DataSource ds) {
		Connection conn = null;
		CallableStatement cStmt = null;
		ResultSet rs = null;
		String finalEsa = "";
		int retVal = -1;

		try {
			for (int i = 0; i < 3; i++) {
				if ((esaBean.getAgent(i) != null
						&& !esaBean.getAgent(i).equals("") && !esaBean
						.getAgent(i).equals("0"))
						|| (esaBean.getAgent(i).equals("0") && !esaBean
								.getEsaId(i).equals("0"))) {
					/*
					 * System.out.println("values-----"+i);
					 * System.out.println("esaBean.getEsaId(i)-----"
					 * +esaBean.getEsaId(i));
					 * System.out.println("esaBean.getEsaSeqNo(i)-----"
					 * +esaBean.getEsaSeqNo(i));
					 * System.out.println("esaBean.getAgent(i)-----"
					 * +esaBean.getAgent(i));
					 * System.out.println("esaBean.getCommission(i)-----"
					 * +esaBean.getCommission(i));
					 * System.out.println("esaBean.getPayementTerms(i)-----"
					 * +esaBean.getPayementTerms(i));
					 * System.out.println("esaBean.getEffectiveDate(i)-----"
					 * +esaBean.getEffectiveDate(i));
					 * System.out.println("esaBean.getStatus(i)-----"
					 * +esaBean.getStatus(i));
					 * System.out.println("esaBean.getCommissionType(i)-----"
					 * +esaBean.getCommissionType(i));
					 */

					finalEsa = finalEsa
							+ esaBean.getEsaId(i)
							+ ","
							+ ""
							+ esaBean.getEsaSeqNo(i)
							+ ","
							+ ""
							+ esaBean.getAgent(i)
							+ ","
							+ ""
							+ (esaBean.getCommission(i).equals("") ? "0"
									: esaBean.getCommission(i)) + "," + ""
							+ esaBean.getCommissionType(i) + "," + ""
							+ esaBean.getPayementTerms(i) + "," + ""
							+ esaBean.getEffectiveDate(i) + "," + ""
							+ esaBean.getStatus(i) + "~";

				}
			}

			if (finalEsa.length() > 0) {
				finalEsa = finalEsa.substring(0, finalEsa.length() - 1);
			}

			conn = ds.getConnection();
			cStmt = conn.prepareCall("{?=call lp_esa_manage_01(?,?,?)}");
			cStmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cStmt.setInt(2, Integer.parseInt(esaBean.getAppendixId()));
			cStmt.setInt(3, Integer.parseInt(loginuserid));
			cStmt.setString(4, finalEsa);
			cStmt.execute();
			retVal = cStmt.getInt(1);

		} catch (Exception e) {
			logger.error("saveProjectESAinDB(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("saveProjectESAinDB(String, String, DataSource) - Exception caught in saving ESA ");
			}
			e.printStackTrace();
		} finally {
			DBUtil.close(rs, cStmt);
			DBUtil.close(conn);
		}
		return retVal;
	}

	/**
	 * Gets the ESA Activity Commission list for commission form.
	 * 
	 * @param jobId
	 *            - job id.
	 * @param appendixId
	 *            - appendix id.
	 * @param ds
	 *            - data source.
	 * 
	 * @return ArrayList<ESAActivityList> - Array List with ESAActivityList
	 *         objects.
	 */
	public static ArrayList<ESAActivityList> getESAActivityList(String jobId,
			String appendixId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList<ESAActivityList> esaActivityList = new ArrayList<ESAActivityList>();
		String sql = "select * from func_lp_get_esa_activity_commission_list(?,?) ";
		int i = 0;
		double resTotalRevenue = 0.0;
		double esaCost = 0.0;
		double esaRevenue = 0.0;
		DecimalFormat dfcur1 = new DecimalFormat("###.####");
		DecimalFormat dfcur = new DecimalFormat("###,###.##");

		try {
			conn = ds.getConnection();
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, Integer.parseInt(jobId));
			pStmt.setInt(2, Integer.parseInt(appendixId));
			rs = pStmt.executeQuery();

			while (rs.next()) {
				resTotalRevenue = 0.0;
				esaCost = 0.0000;
				esaRevenue = 0.0000;

				ESAActivityList actList = new ESAActivityList();
				actList.setActId(rs.getString("lm_at_id"));
				actList.setActivityName(rs.getString("lm_at_title"));
				actList.setActQty(rs.getString("lm_at_quantity"));
				actList.setResBasis(rs.getString("resource_basis"));
				resTotalRevenue = Double.parseDouble(actList.getResBasis())
						* Double.parseDouble(actList.getActQty());
				actList.setResTotRevenue(String.valueOf(resTotalRevenue));
				actList.setActType(rs.getString("billable_act"));

				actList.setEsaId1(rs.getString("esa1_id"));
				actList.setEsaType1(rs.getString("esa1_type"));
				actList.setCommissionStatus1(rs.getString("esa1_comm_status"));
				actList.setCommissionRate1(rs.getString("esa1_comm_rate"));
				actList.setEsaActActiveStatus1(rs.getString("esa1_active"));
				actList.setEsaTypeLabel1(rs.getString("esa1_type_label"));

				esaCost = Double.parseDouble(actList.getCommissionRate1())
						* resTotalRevenue
						* Integer.parseInt(actList.getCommissionStatus1())
						* Integer.parseInt(actList.getEsaActActiveStatus1());
				if (actList.getEsaType1() != null
						&& actList.getEsaType1().equals("1"))
					esaRevenue = esaCost;
				else
					esaRevenue = 0.0000;

				actList.setEsaCost1(esaCost + "");
				actList.setEsaRevenue1(esaRevenue + "");

				actList.setEsaId2(rs.getString("esa2_id"));
				actList.setEsaType2(rs.getString("esa2_type"));
				actList.setCommissionStatus2(rs.getString("esa2_comm_status"));
				actList.setCommissionRate2(rs.getString("esa2_comm_rate"));
				actList.setEsaActActiveStatus2(rs.getString("esa2_active"));
				actList.setEsaTypeLabel2(rs.getString("esa2_type_label"));

				esaCost = Double.parseDouble(actList.getCommissionRate2())
						* resTotalRevenue
						* Integer.parseInt(actList.getCommissionStatus2())
						* Integer.parseInt(actList.getEsaActActiveStatus2());
				if (actList.getEsaType2() != null
						&& actList.getEsaType2().equals("1"))
					esaRevenue = esaCost;
				else
					esaRevenue = 0.0000;

				actList.setEsaCost2(String.valueOf(esaCost));
				actList.setEsaRevenue2(esaRevenue + "");

				actList.setEsaId3(rs.getString("esa3_id"));
				actList.setEsaType3(rs.getString("esa3_type"));
				actList.setCommissionStatus3(rs.getString("esa3_comm_status"));
				actList.setCommissionRate3(rs.getString("esa3_comm_rate"));
				actList.setEsaActActiveStatus3(rs.getString("esa3_active"));
				actList.setEsaTypeLabel3(rs.getString("esa3_type_label"));

				esaCost = Double.parseDouble(actList.getCommissionRate3())
						* resTotalRevenue
						* Integer.parseInt(actList.getCommissionStatus3())
						* Integer.parseInt(actList.getEsaActActiveStatus3());
				if (actList.getEsaType3() != null
						&& actList.getEsaType3().equals("1"))
					esaRevenue = esaCost;
				else
					esaRevenue = 0.0000;

				actList.setEsaCost3(String.valueOf(esaCost));
				actList.setEsaRevenue3(String.valueOf(esaRevenue));

				esaActivityList.add(i, actList);
				i++;
			}

		} catch (Exception e) {
			logger.error("getESAActivityList(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getESAActivityList(String, String, DataSource) - Error retrieving ESA Activity Commission List ");
			}
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}

		return esaActivityList;
	}

	/**
	 * Gets Commission Type on commission page (Only 2 values 'Add' and
	 * 'Blend').
	 * 
	 * @return ArrayList<LabelValue>- Array List with LabelValue pair objects.
	 */
	public static ArrayList<LabelValue> getCommissionType() {
		ArrayList commissionType = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		commissionType.add(new com.mind.common.LabelValue("Add", "1"));
		commissionType.add(new com.mind.common.LabelValue("Blend", "0"));
		return commissionType;
	}

	/**
	 * Get ESA agents setup for an appendix.
	 * 
	 * @param appendixId
	 *            - appendix id.
	 * @param ds
	 *            - data source.
	 * 
	 * @return the array list with LabelValue pair objects.
	 */
	public static ArrayList<LabelValue> appendixAgents(String appendixId,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList<LabelValue> agents = new ArrayList<LabelValue>();
		int i = 0;
		String sql = "select * from func_lp_appendix_esa_agents(?) order by lp_es_seq_no";

		try {
			conn = ds.getConnection();
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, Integer.parseInt(appendixId));
			rs = pStmt.executeQuery();

			while (rs.next()) {
				agents.add(
						i,
						new LabelValue(rs.getString("agent_name"), rs
								.getString("lp_es_id")));
				i++;
			}
		} catch (SQLException sqle) {
			logger.error("appendixAgents(String, DataSource)", sqle);

			if (logger.isDebugEnabled()) {
				logger.debug("appendixAgents(String, DataSource) - Exception occurred while retrieving agent names for an appendix:");
			}
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return agents;
	}

	/**
	 * Save commission details for each selected ESA-Activity pair from
	 * commission page.
	 * 
	 * @param bean
	 *            - ESA Activity Commission bean.
	 * @param loginuserid
	 *            - login user id.
	 * @param ds
	 *            - data source.
	 * 
	 * @return the int
	 */
	public static int saveCommissionForActESA(ESAActivityCommissionBean bean,
			String loginuserid, DataSource ds) {
		Connection conn = null;
		CallableStatement cStmt = null;
		String esaActString = "";
		String finalString = "";
		int retVal = -1;

		try {
			for (int i = 0; i < bean.getActivityId().length; i++) {

				if (bean.getActivityId(i) != null
						&& !bean.getActivityId(i).equals("")) {
					if (!bean.getEsaId1(i).equals("0")
							&& bean.getEsaActActiveStatus1(i).equals("1")) {
						finalString = finalString + bean.getActivityId(i) + ","
								+ bean.getActivityQty(i) + ","
								+ bean.getEsaId1(i) + ","
								+ bean.getActCommissionType1(i) + ","
								+ bean.getCommissionStatus1(i) + ","
								+ bean.getCommissionRate1(i) + "~";
					}

					if (!bean.getEsaId2(i).equals("0")
							&& bean.getEsaActActiveStatus2(i).equals("1")) {
						finalString = finalString + bean.getActivityId(i) + ","
								+ bean.getActivityQty(i) + ","
								+ bean.getEsaId2(i) + ","
								+ bean.getActCommissionType2(i) + ","
								+ bean.getCommissionStatus2(i) + ","
								+ bean.getCommissionRate2(i) + "~";
					}

					if (!bean.getEsaId3(i).equals("0")
							&& bean.getEsaActActiveStatus3(i).equals("1")) {
						finalString = finalString + bean.getActivityId(i) + ","
								+ bean.getActivityQty(i) + ","
								+ bean.getEsaId3(i) + ","
								+ bean.getActCommissionType3(i) + ","
								+ bean.getCommissionStatus3(i) + ","
								+ bean.getCommissionRate3(i) + "~";
					}
				}
			}

			// System.out.println("finalString------"+finalString);

			conn = ds.getConnection();
			cStmt = conn
					.prepareCall("{?=call lp_esa_act_commission_manage_01(?,?,?,?)}");
			cStmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cStmt.setInt(2, Integer.parseInt(bean.getJobid()));
			cStmt.setString(3, finalString);
			cStmt.setString(4, "cform");
			cStmt.setInt(5, Integer.parseInt(loginuserid));
			cStmt.execute();
			retVal = cStmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"saveCommissionForActESA(ESAActivityCommissionBean, String, DataSource - Error occurred while saving ESA Activity Commission in database: )",
					e);
			if (logger.isDebugEnabled()) {
				logger.debug("saveCommissionForActESA(ESAActivityCommissionBean, String, DataSource - Error occurred while saving ESA Activity Commission in database: )");
			}
			e.printStackTrace();
		} finally {
			DBUtil.close(cStmt);
			DBUtil.close(conn);
		}

		return retVal;
	}

	/**
	 * Gets the commission PO list.
	 * 
	 * @param jobId
	 *            - job id.
	 * @param ds
	 *            - data source.
	 * 
	 * @return ArrayList<String> - list of commission PO Id(s).
	 */
	public static ArrayList<String> getCommissionPOList(String jobId,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<String> commissionPOList = new ArrayList<String>();
		int i = 0;

		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select lm_po_id from lm_purchase_order where lm_po_js_id = ? and isnull(lm_po_commission_flag,0) = 1");
			pstmt.setString(1, jobId);
			rs = pstmt.executeQuery();

			if (rs != null) {
				while (rs.next()) {
					commissionPOList.add(i, rs.getString("lm_po_id"));
					i++;
				}
			}
		} catch (Exception e) {
			logger.error(
					"getCommissionPOList(String, DataSource) - Error occurred while retrieving Commission PO Ids for a job",
					e);
			e.printStackTrace();
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return commissionPOList;
	}

	/**
	 * Gets Payment Terms(Quarterly and Monthly) for ESA on ESA setup page.
	 * 
	 * @param ds
	 *            - data source.
	 * 
	 * @return ArrayList - ESA payment terms
	 */
	public static ArrayList getESAPaymentTerms(DataSource ds) {
		ArrayList terms = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		terms.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select * from lm_pwo_terms_master "
							+ "where lm_pwo_terms_master_data in ('Quarterly 90 Days Arrears/Paid Invoices', 'Monthly 80/20 Net 50/80 All Invoices')");
			while (rs.next()) {
				terms.add(new com.mind.common.LabelValue(rs.getString(2), rs
						.getString(1)));
			}
		} catch (Exception e) {
			logger.error("getESAPaymentTerms(DataSource)", e);
		} finally {
			DBUtil.close(stmt);
			DBUtil.close(conn);

		}

		return terms;
	}
}
