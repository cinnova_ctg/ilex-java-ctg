package com.mind.dao.PM;

import java.util.Collection;

public class MSA 
{
	private static final long serialVersionUID = 3256727286031136818L;
	private String msa_Id = null;
	private String name = null;
	private String orgid = null; 
	private String projectid = null;
	
	private String msatype = null;
	private String msatypecombo = null;
	
	private String salestype = null;
	private String salestypecombo = null;
	
	private String salesPOC = null;
	private String salesPOCcombo = null;
	
	private String unionupliftfactor = null;
	private String formaldocdate = null;
	
	private String payterms = null;
	private String paytermscombo = null;
	private String file_upload_check = null;
	
	private String payqualifier = null;
	private String payqualifiercombo = null;
	
	private String reqplasch = null;
	private String effplasch = null;
	private String schdays = null;
	
	
	private String draftreviews = null;
	private String draftreviewscombo = null;
	
	private String businessreviews = null;
	private String businessreviewscombo = null;
	
	private String custPocbusiness = null;
	private String custPocbusinesscombo = null;
	
	private String custPoccontract = null;
	private String custPoccontractcombo = null;
	
	private String status = null;
	
	private String lastcomment = null;						
	private String lastchangeby = null;
	private String lastchangedate = null;
	
	private String lastmsamodifieddate = null;
	private String lastmsamodifiedby = null; 
	
	private Collection poclist = null;
	
	public void setMsa_Id( String id )
	{
		msa_Id = id;
	}
	
	public String getMsa_Id()
	{
		return msa_Id;
	}
	
	public void setName( String n )
	{
			name = n;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setOrgid( String n )
	{
			orgid = n;
	}
	
	public String getOrgid()
	{
		return orgid;
	}
	
	public void setProjectid( String n )
	{
			projectid = n;
	}
	
	public String getProjectid()
	{
		return projectid;
	}
	
	public void setMsatype( String n )
	{
			msatype = n;
	}
	
	public String getMsatype()
	{
		return msatype;
	}
	
	public void setMsatypecombo( String n )
	{
			msatypecombo = n;
	}
	
	public String getMsatypecombo()
	{
		return msatypecombo;
	}
	

	public void setSalestype( String n )
	{
		salestype = n;
	}
	
	public String getSalestype()
	{
		return salestype;
	}
	
	public void setSalestypecombo( String n )
	{
		salestypecombo = n;
	}
	
	public String getSalestypecombo()
	{
		return salestypecombo;
	}
	
	
	
	public void setSalesPOC( String n )
	{
		salesPOC = n;
	}
	
	public String getSalesPOC()
	{
		return salesPOC;
	}
	
	public void setSalesPOCcombo( String n )
	{
		salesPOCcombo = n;
	}
	
	public String getSalesPOCcombo()
	{
		return salesPOCcombo;
	}
	
	
	
	public String getUnionupliftfactor() {
		return unionupliftfactor;
	}

	public void setUnionupliftfactor( String unionupliftfactor ) {
		this.unionupliftfactor = unionupliftfactor;
	}
	
	public String getFormaldocdate() {
		return formaldocdate;
	}

	public void setFormaldocdate( String formaldocdate ) {
		this.formaldocdate = formaldocdate;
	}

	public void setPayterms( String n )
	{
		payterms = n;
	}
	
	public String getPayterms()
	{
		return payterms;
	}
	
	public void setPaytermscombo( String n )
	{
		paytermscombo = n;
	}
	
	public String getPaytermscombo()
	{
		return paytermscombo;
	}
	
	
	
	public String getFile_upload_check() {
		return file_upload_check;
	}

	public void setFile_upload_check( String file_upload_check ) {
		this.file_upload_check = file_upload_check;
	}

	
	public void setPayqualifier( String n )
	{
		payqualifier = n;
	}
	
	public String getPayqualifier()
	{
		return payqualifier;
	}
	
	
	public String getPayqualifiercombo() {
		return payqualifiercombo;
	}

	public void setPayqualifiercombo( String payqualifiercombo ) {
		this.payqualifiercombo = payqualifiercombo;
	}

	
	
	public void setReqplasch( String n )
	{
		reqplasch = n;
	}
	
	public String getReqplasch()
	{
		return reqplasch;
	}

	public void setEffplasch( String n )
	{
		effplasch = n;
	}
	
	public String getEffplasch()
	{
		return effplasch;
	}
	
	public void setSchdays( String n )
	{
		schdays = n;
	}
	
	public String getSchdays()
	{
		return schdays;
	}
	
	
	
	public void setDraftreviews( String n )
	{
		draftreviews = n;
	}
	
	public String getDraftreviews()
	{
		return draftreviews;
	}
	
	public void setDraftreviewscombo( String n )
	{
		draftreviewscombo = n;
	}
	
	public String getDraftreviewscombo()
	{
		return draftreviewscombo;
	}
	
	
	public void setBusinessreviews( String n )
	{
		businessreviews = n;
	}
	
	public String getBusinessreviews()
	{
		return businessreviews;
	}
	
	public void setBusinessreviewscombo( String n )
	{
		businessreviewscombo = n;
	}
	
	public String getBusinessreviewscombo()
	{
		return businessreviewscombo;
	}
	
	
	
	public void setCustPocbusiness( String n )
	{
		custPocbusiness = n;
	}
	
	public String getCustPocbusiness()
	{
		return custPocbusiness;
	}
	
	public void setCustPocbusinesscombo( String n )
	{
		custPocbusinesscombo = n;
	}
	
	public String getCustPocbusinesscombo()
	{
		return custPocbusinesscombo;
	}
	
	
	public void setCustPoccontract( String n )
	{
		custPoccontract = n;
	}
	
	public String getCustPoccontract()
	{
		return custPoccontract;
	}
	
	public void setCustPoccontractcombo( String n )
	{
		custPoccontractcombo = n;
	}
	
	public String getCustPoccontractcombo()
	{
		return custPoccontractcombo;
	}
	
	
	public void setStatus( String n )
	{
		status = n;
	}
	
	public String getStatus()
	{
		return status;
	}

	public String getLastcomment() {
		return lastcomment;
	}

	public void setLastcomment(String lastcomment) {
		this.lastcomment = lastcomment;
	}

	public String getLastchangeby() {
		return lastchangeby;
	}

	public void setLastchangeby(String lastchangeby) {
		this.lastchangeby = lastchangeby;
	}

	public String getLastchangedate() {
		return lastchangedate;
	}

	public void setLastchangedate(String lastchangedate) {
		this.lastchangedate = lastchangedate;
	}

	
	public String getLastmsamodifiedby() {
		return lastmsamodifiedby;
	}

	public void setLastmsamodifiedby( String lastmsamodifiedby ) {
		this.lastmsamodifiedby = lastmsamodifiedby;
	}

	
	public String getLastmsamodifieddate() {
		return lastmsamodifieddate;
	}

	public void setLastmsamodifieddate( String lastmsamodifieddate ) {
		this.lastmsamodifieddate = lastmsamodifieddate;
	}

	
	public Collection getPoclist() {
		return poclist;
	}

	public void setPoclist( Collection poclist ) {
		this.poclist = poclist;
	}

	
	
}

