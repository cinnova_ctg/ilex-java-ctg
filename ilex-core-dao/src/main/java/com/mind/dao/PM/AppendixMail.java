package com.mind.dao.PM;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.EnvironmentSelector;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Email;

/**
 * Discription: This Class is use to send email to senior project manageres.
 * These emails are based on two appendic event, appendix creation and
 * assignment fo the senior project managers. Methods: appendixCreationMail,
 * projectManagerUpdateMail, getSrProManagerMailId, getPocNameMailId,
 * getAppendixCnsPocId, getDefaultJobId
 *
 * @author vijaykumarsingh
 *
 */
public class AppendixMail {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(AppendixMail.class);

    static ResourceBundle rb = ResourceBundle
            .getBundle("com.mind.properties.ApplicationResources");
    /*- Required smtp authentication
     static String Username = rb.getString( "common.Email.username" );				// - smtp user name8*/
    static String Username = rb.getString("common.Email.username"); // - smtp
    // user name
    static String Userpassword = rb.getString("common.Email.userpassword"); // -
    // smtp
    // password
    static String Smtpservername = EnvironmentSelector
            .getBundleString("common.Email.smtpservername"); // - smtp sever
    // name
    static String Smtpserverport = rb.getString("common.Email.smtpserverport"); // -
    // smtp
    // port
    // no.
    static String commonMailId;
    // - two common email id's (mstuhlreyer@contingent.net,
    // cmattix@contingent.net)
    static String defaultFirstName = EnvironmentSelector
            .getBundleString("appendix.default.Emailfromname");
    static String defaultMailId = EnvironmentSelector
            .getBundleString("appendix.default.Emailid");

    /**
     * Discription : This method is use to send email To all senior project
     * managers and CC to mstuhlreyer@contingent.net, cmattix@contingent.net and
     * CNS Poc, when appendix will be created.
     *
     * @param AppendixName : Appendix Name
     * @param MsaName : Msa Name
     * @param cnsPocId : CNS Poc Id
     * @param ds : Database Object
     * @return : NO
     */
    public static void appendixCreationMail(String AppendixName,
            String MsaName, String cnsPocId, DataSource ds) {

        /*
         * System.out.println("AppendixName = "+AppendixName);
         * System.out.println("cnsPocId = "+cnsPocId);
         * System.out.println("MsaName = "+MsaName);
         */
        String srProManagerMailIds = getSrProManagerMailId("0", ds);
        String[] cnsPocMailId = getPocNameMailId(cnsPocId, ds);
        if (cnsPocMailId[0] == null || cnsPocMailId[0].equals("")) {
            cnsPocMailId[0] = defaultFirstName;
        } // - when appendix poc first Name is not exist
        if (cnsPocMailId[1] == null || cnsPocMailId[1].equals("")) {
            cnsPocMailId[1] = defaultMailId;
        } // - when appendix poc email id is not exist
        commonMailId = EnvironmentSelector
                .getBundleString("appendix.fixed.Emailid1");
        String apdxFxdemailId2 = EnvironmentSelector
                .getBundleString("appendix.fixed.Emailid2");
        if (apdxFxdemailId2 != null && !"".equals(apdxFxdemailId2.trim())) {
            commonMailId = commonMailId
                    + ","
                    + EnvironmentSelector
                    .getBundleString("appendix.fixed.Emailid2");
        }
        String ccmail = commonMailId + "," + cnsPocMailId[1];
        StringBuffer mailContent = new StringBuffer(); // - for make email
        // content.

        mailContent
                .append("The purpose of this email is to inform you that a new appendix has been created for "
                        + MsaName
                        + ". "
                        + "The appendix title is "
                        + AppendixName + ".");
        mailContent.append(rb.getString("appendix.Email.content1"));
        mailContent.append("Thanks,<br>" + cnsPocMailId[0]);

        EmailBean emailBean = new EmailBean();
        emailBean.setUsername(Username);
        emailBean.setUserpassword(Userpassword);
        emailBean.setSmtpservername(Smtpservername);
        emailBean.setSmtpserverport(Smtpserverport);
        emailBean.setTo(srProManagerMailIds);
        emailBean.setCc(ccmail);
        emailBean.setBcc("");
        emailBean.setSubject("New Appendix, " + AppendixName + " for "
                + MsaName);
        emailBean.setFrom(cnsPocMailId[1]);
        emailBean.setContent(mailContent.toString());
        emailBean.setType("prm_appendix");
        emailBean.setChangeStatus("");
        Email.Send(emailBean, "text/html", ds);
    }

    /**
     * Discription : This method is use to send email To assigned Project
     * Manager and CC to all senior project managers,
     * mstuhlreyer@contingent.net, cmattix@contingent.net and CNS Poc, when
     * assign the senior project manager to appendix.
     *
     * @param appendixId : Appendix Id
     * @param AppendixName : Appendix Name
     * @param MsaName : Msa Name
     * @param assignePocId : Assign POC Id
     * @param url : URL of project
     * @param ds : Database Object
     * @return : No
     */
    public static void projectManagerUpdateMail(String appendixId,
            String AppendixName, String MsaName, String assignePocId,
            String url, String loginUserId, DataSource ds) {

        /*
         * System.out.println("appendixId = "+appendixId);
         * System.out.println("AppendixName = "+AppendixName);
         * System.out.println("MsaName = "+MsaName);
         * System.out.println("assignePocId = "+assignePocId);
         * System.out.println("url = "+url);
         */
        String jobid = getDefaultJobId(appendixId, ds);
        url = url
                + "main.jsp?ActivityUpdateAction.do?addendum_id=0&ref=View&Job_Id="
                + jobid + "&module=PM"; // -
        // Http://192.168.20.9/IlexTestRelease/main.jsp?ActivityUpdateAction.do?addendum_id=0&ref=View&Job_Id=123&module=PM
        String appendixCnsPocId = getAppendixCnsPocId(appendixId, ds);
        if (appendixCnsPocId == null || appendixCnsPocId.equals("0")) {
            appendixCnsPocId = loginUserId;
        } // when no poc is assigned to appendix

        String[] assigneProjectManagerMailId = getPocNameMailId(assignePocId,
                ds);
        String srProManagerMailIds = getSrProManagerMailId(assignePocId, ds);
        String[] cnsPocMailId = getPocNameMailId(appendixCnsPocId, ds);
        if (cnsPocMailId[0] == null || cnsPocMailId[0].equals("")) {
            cnsPocMailId[0] = defaultFirstName;
        } // - when appendix poc first Name is not exist
        if (cnsPocMailId[1] == null || cnsPocMailId[0].equals("")) {
            cnsPocMailId[1] = defaultMailId;
        } // - when appendix poc email id is not exist
        commonMailId = EnvironmentSelector
                .getBundleString("appendix.fixed.Emailid1");

        String apdxFxdemailId2 = EnvironmentSelector
                .getBundleString("appendix.fixed.Emailid2");
        if (apdxFxdemailId2 != null && !"".equals(apdxFxdemailId2.trim())) {
            commonMailId = commonMailId
                    + ","
                    + EnvironmentSelector
                    .getBundleString("appendix.fixed.Emailid2");
        }

        String ccmail = srProManagerMailIds + "," + commonMailId + ","
                + cnsPocMailId[1];
        StringBuffer mailContent = new StringBuffer();

        mailContent.append("You were assigned a new appendix for " + MsaName
                + ". " + "The appendix title is " + AppendixName + ".");
        mailContent.append(rb.getString("appendix.Email.content2"));
        mailContent.append("To review the appendix, <a href = " + url
                + ">Click Here</a>.<br><br>");
        mailContent.append("Thanks,<br>" + cnsPocMailId[0]);

        EmailBean emailform = new EmailBean();
        emailform.setUsername(Username);
        emailform.setUserpassword(Userpassword);
        emailform.setSmtpservername(Smtpservername);
        emailform.setSmtpserverport(Smtpserverport);
        emailform.setTo(assigneProjectManagerMailId[1]);
        emailform.setCc(ccmail);
        emailform.setBcc("");
        emailform.setSubject("Appendix " + AppendixName + " for " + MsaName
                + " Assigned to you");
        emailform.setFrom(cnsPocMailId[1]);
        emailform.setContent(mailContent.toString());
        emailform.setType("prm_appendix");
        emailform.setChangeStatus("");
        Email.Send(emailform, "text/html", ds);
    }

    /**
     * Discription : This method get all Senior Project Manager Email Id's form
     * Database.
     *
     * @param action : create/assign
     * @param ds : database object
     * @return : NO
     */
    public static String getSrProManagerMailId(String appendixPocID,
            DataSource ds) {

        String srProManagerMailId = "";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            sql = "select lo_pc_id, lo_pc_email1 from lo_poc where lo_pc_title like 'Sr. Project Manager' and lo_pc_active_user = 1 and lo_pc_id <> "
                    + appendixPocID;
            // System.out.println("sql = "+sql);

            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                srProManagerMailId = srProManagerMailId + ","
                        + rs.getString("lo_pc_email1"); // - Senior Project
                // Manager Email Id's
            }
        } catch (Exception e) {
            logger.error("getSrProManagerMailId(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getSrProManagerMailId(String, DataSource) - Error Occured in com.mind.dao.PM.getSrProManagerMailId() "
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getSrProManagerMailId(String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getSrProManagerMailId(String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getSrProManagerMailId(String, DataSource) - exception ignored",
                        s);
            }

        }

        return srProManagerMailId;
    }

    /**
     * Discription : This method is use to get poc first name and its email id
     * from database.
     *
     * @param pocId : Poc Id
     * @param ds : Database Object
     * @return : String[] cnsPocNameMailId
     */
    public static String[] getPocNameMailId(String pocId, DataSource ds) {

        String[] cnsPocNameMailId = new String[2];
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            sql = "select lo_pc_first_name,lo_pc_email1 from lo_poc where lo_pc_id="
                    + pocId + " and lo_pc_active_user = 1";
            // System.out.println("sql = "+sql);

            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                cnsPocNameMailId[0] = rs.getString("lo_pc_first_name"); // - Poc
                // First
                // Name
                cnsPocNameMailId[1] = rs.getString("lo_pc_email1"); // - Poc
                // Email Id
            }
        } catch (Exception e) {
            logger.error("getPocNameMailId(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getPocNameMailId(String, DataSource) - Error Occured in com.mind.dao.PM.getPocNameMailId() "
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getPocNameMailId(String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getPocNameMailId(String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getPocNameMailId(String, DataSource) - exception ignored",
                        s);
            }

        }

        return cnsPocNameMailId;
    }

    /**
     * Discription : This methoc is use to get appendix poc id from
     * lx_appendix_main table of Database.
     *
     * @param appendixId : Appendix Id
     * @param ds : Database Object
     * @return : String cnspocid
     */
    public static String getAppendixCnsPocId(String appendixId, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";
        String cnspocid = "";

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            sql = "select lx_pr_cns_poc from lx_appendix_main where lx_pr_id="
                    + appendixId;
            // System.out.println("sql = "+sql);

            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                cnspocid = rs.getString("lx_pr_cns_poc"); // Appendix Poc Id
            }
        } catch (Exception e) {
            logger.error("getAppendixCnsPocId(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getAppendixCnsPocId(String, DataSource) - Error Occured in com.mind.dao.PM.getAppendixCnsPocId() "
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getAppendixCnsPocId(String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getAppendixCnsPocId(String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getAppendixCnsPocId(String, DataSource) - exception ignored",
                        s);
            }

        }

        return cnspocid;
    }

    /**
     * Discription : This method is use to get Default Job Id from lm_job table
     * of Database.
     *
     * @param apppendixId : Appendix Id
     * @param ds : Database object
     * @return : String jobid
     */
    public static String getDefaultJobId(String apppendixId, DataSource ds) {

        String jobid = "";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            sql = "select lm_js_id from lm_job where lm_js_pr_id = "
                    + apppendixId + " and lm_js_title like 'Default Job'";
            // System.out.println("sql = "+sql);

            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                jobid = rs.getString("lm_js_id"); // Default Job Id
            }
        } catch (Exception e) {
            logger.error("getDefaultJobId(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getDefaultJobId(String, DataSource) - Error Occured in com.mind.dao.PM.getDefaultJobId() "
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getDefaultJobId(String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getDefaultJobId(String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getDefaultJobId(String, DataSource) - exception ignored",
                        s);
            }

        }

        return jobid;
    }

}
