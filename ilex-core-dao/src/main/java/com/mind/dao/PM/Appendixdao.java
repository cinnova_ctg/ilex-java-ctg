/*


/* This class defines methods for Appendix Creation , Updation , Deletion 
 * Method Name: getAppendixList
 * Method Argument: 3 Argument DataSource ds , String msa_id , String querystring
 * Method Description: Retrieve all the Appendices for the passed MSA Id
 * retrun type:ArrayList 
 * see setAppendix function

 * Method Name: addAppendix
 * Method Argument: 2 Argument Appendix appendix,  DataSource ds
 * Method Description: Create an Appendix
 * retrun type:void

 * Method Name: updataAppendixList
 * Method Argument: 2 Argument Appendix appendix, DataSource ds
 * Method Description: Update Appendix
 * retrun type:void

 * Method Name: getAppendix
 * Method Argument: 3 Argument String msa_id , String appendix_id , DataSource ds
 * Method Description: Retrieve appendix.
 * retrun type:Appendix
 * see   setAppendix function
 */

package com.mind.dao.PM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.PMInworkStateList;
import com.mind.bean.pm.Appendix;
import com.mind.bean.pm.AppendixEditBean;
import com.mind.common.Decimalroundup;
import com.mind.dao.PRM.ManageUpliftdao;

public class Appendixdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Appendixdao.class);

	public static String getAppendixStatus(String appendixId, String msaId,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String appendixStatus = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select  lx_pr_status from dbo.func_lp_appendix_tab('"
					+ msaId + "', '" + appendixId + "')";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				appendixStatus = rs.getString("lx_pr_status");
			}
		} catch (Exception e) {
			logger.error("getAppendixStatus(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixStatus(String, String, DataSource) - Exception in retrieving appendix status"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendixStatus(String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendixStatus(String, String, DataSource) - Exception in retrieving appendix status"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendixStatus(String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendixStatus(String, String, DataSource) - Exception in retrieving appendix status"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendixStatus(String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendixStatus(String, String, DataSource) - Exception in retrieving appendix status"
							+ sql);
				}
			}

		}
		return appendixStatus;
	}

	public static String getCurrentstatus(String appendixid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String currentstatus = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lx_pr_status from lx_appendix_main where lx_pr_id like '"
					+ appendixid + "'";
			// System.out.println("sql:::::::::::::"+sql);
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				currentstatus = rs.getString("lx_pr_status");
			}

			else
				currentstatus = "I";

		} catch (Exception e) {
			logger.error("getCurrentstatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCurrentstatus(String, DataSource) - Error occured during getting current status  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCurrentstatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCurrentstatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCurrentstatus(String, DataSource) - exception ignored",
						e);
			}

		}
		return currentstatus;
	}

	public static ArrayList getAppendixList(DataSource ds, String msa_id,
			String querystring, String monthMSA, String weekMSA, String status,
			String Owner) {
		ArrayList all_appendices = new ArrayList();
		Connection conn = null;
		Appendix appendix = null;
		Statement stmt = null;
		ResultSet rs = null;

		if (querystring.equals("")) {
			querystring = "order by appendixname";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql_list = "select appendix_id , msa_id , msaname , appendixname , lx_pr_type ,app_type, "
					+ "lx_pr_cns_poc , CNSPOCname = dbo.func_cc_poc_name(lx_pr_cns_poc), lx_pd_customer_division , lo_om_division , "
					+ "lx_pd_number_sites , lx_pd_need_date , lx_pd_effective_date , lx_pd_planned_duration , lx_pd_draft_review , "
					+ "lx_pd_business_review ,lx_pd_msp, lx_pr_business_poc , BusinessPOCname =  dbo.func_cc_poc_name(lx_pr_business_poc) , "
					+ "lx_pr_contract_poc , ContractPOCname =  dbo.func_cc_poc_name(lx_pr_contract_poc) , lx_pr_billing_poc , "
					+ "BillingPOCname= dbo.func_cc_poc_name(lx_pr_billing_poc) , lx_pr_status , lx_ce_total_cost , "
					+ "lx_ce_invoice_price , lx_ce_margin , lx_pd_change_date , "
					+ "lx_pd_changed_by , lx_pr_type_name, lx_pr_customer_reference, lm_js_union_uplift_factor, "
					+ "lm_js_expedite24_uplift_factor, lm_js_expedite48_uplift_factor, lm_js_afterhours_uplift_factor,"
					+ "lm_js_wh_uplift_factor,lx_pr_intransit_res,lx_appendix_status,lx_pd_created_by,lx_pd_created_date, lx_pr_test_appendix,lx_pr_abbreviation "
					+ "from dbo.func_lp_appendix_tab('"
					+ msa_id
					+ "' , '%') where 1=1 ";

			if (monthMSA != null) {
				sql_list = sql_list
						+ "	and	( "
						+ " ( datepart(mm, lx_pd_need_date) =  datepart(mm, getdate())and datepart(yy, lx_pd_need_date) =  datepart(yy, getdate()) )  "
						+ " or "
						+ " ( datepart(mm, lx_pd_effective_date) =  datepart(mm, getdate())and datepart(yy, lx_pd_effective_date) =  datepart(yy, getdate()) ) "
						+ " ) ";
			}
			if (weekMSA != null) {

				sql_list = sql_list
						+ "	and	( "
						+ " ( datepart(wk, lx_pd_need_date) =  datepart(wk, getdate())and datepart(yy, lx_pd_need_date) =  datepart(yy, getdate()) )  "
						+ " or "
						+ " ( datepart(wk, lx_pd_effective_date) =  datepart(wk, getdate())and datepart(yy, lx_pd_effective_date) =  datepart(yy, getdate()) ) "
						+ " ) ";
			}
			if (status != null && !status.equals("")) {
				sql_list = sql_list + "and lx_appendix_status in " + status
						+ " ";
			}

			if (Owner != null && (!Owner.contains("%"))
					&& (!Owner.trim().equals("")))
				sql_list = sql_list + " and lx_pr_cns_poc in " + Owner + " ";

			// System.out.println("Final Query for the Appendix is ::: " +
			// sql_list);

			if (status != null && !status.equals("") && Owner != null
					&& !Owner.equals("")) {
				sql_list = sql_list + querystring;
			} else {
				sql_list = " select * from dbo.func_lp_msa_tab('%') where 1!=1 ";

			}

			rs = stmt.executeQuery(sql_list);

			all_appendices = setAppendix(rs);
		}

		catch (SQLException e) {
			logger.error(
					"getAppendixList(DataSource, String, String, String, String, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixList(DataSource, String, String, String, String, String, String) - Exception caught in getting all Appendices"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getAppendixList(DataSource, String, String, String, String, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendixList(DataSource, String, String, String, String, String, String) - Exception caught in getting all Appendices"
							+ sql);
				}
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.error(
						"getAppendixList(DataSource, String, String, String, String, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendixList(DataSource, String, String, String, String, String, String) - Exception caught in getting all Appendices"
							+ sql);
				}
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.error(
						"getAppendixList(DataSource, String, String, String, String, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendixList(DataSource, String, String, String, String, String, String) - Exception caught in getting all Appendices"
							+ sql);
				}
			}

		}

		return all_appendices;
	}

	public static ArrayList setAppendix(ResultSet rs) {
		ArrayList appendixlist = new ArrayList();
		Appendix appendix;
		String temp = "";
		Collection tempoc = null;
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");
		try {
			while (rs.next()) {
				appendix = new Appendix();

				appendix.setAppendix_Id(rs.getString("appendix_id"));

				appendix.setMsa_Id(rs.getString("msa_id"));
				appendix.setMsaname(rs.getString("msaname"));
				appendix.setName(rs.getString("appendixname"));
				appendix.setAppendixtype(rs.getString("lx_pr_type_name"));
				appendix.setAppendixtypecombo(rs.getString("lx_pr_type"));
				appendix.setCnsPOC(rs.getString("CNSPOCname"));

				appendix.setCnsPOCcombo(rs.getString("lx_pr_cns_poc"));
				appendix.setCustomerdivision(rs.getString("lo_om_division"));
				appendix.setCustomerdivisioncombo(rs
						.getString("lx_pd_customer_division"));
				appendix.setSiteno(rs.getString("lx_pd_number_sites"));
				appendix.setCustref(rs.getString("lx_pr_customer_reference"));

				appendix.setReqplasch(rs.getString("lx_pd_need_date"));
				appendix.setEffplasch(rs.getString("lx_pd_effective_date"));
				appendix.setSchdays(rs.getString("lx_pd_planned_duration"));

				if (rs.getString("lx_pd_draft_review") != null) {
					if (rs.getString("lx_pd_draft_review").equals("S"))
						temp = "Standard";
					else
						temp = "Expedite";
				}
				appendix.setDraftreviews(temp);
				appendix.setDraftreviewscombo(rs
						.getString("lx_pd_draft_review"));

				if (rs.getString("lx_pd_business_review") != null) {
					if (rs.getString("lx_pd_business_review").equals("S"))
						temp = "Standard";
					else
						temp = "Expedite";
				}
				appendix.setBusinessreviews(temp);
				appendix.setBusinessreviewscombo(rs
						.getString("lx_pd_business_review"));

				appendix.setCustPocbusiness(rs.getString("BusinessPOCname"));
				appendix.setCustPocbusinesscombo(rs
						.getString("lx_pr_business_poc"));

				appendix.setCustPoccontract(rs.getString("ContractPOCname"));
				appendix.setCustPoccontractcombo(rs
						.getString("lx_pr_contract_poc"));

				appendix.setCustPocbilling(rs.getString("BillingPOCname"));
				appendix.setCustPocbillingcombo(rs
						.getString("lx_pr_billing_poc"));

				appendix.setStatus(rs.getString("lx_pr_status"));
				appendix.setEstimatedcost(Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_total_cost"), 2));
				appendix.setExtendedprice(Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_invoice_price"), 2));

				appendix.setProformamargin(dfcurmargin.format(rs
						.getFloat("lx_ce_margin")) + "");
				appendix.setLastappendixmodifiedby(rs
						.getString("lx_pd_changed_by"));
				appendix.setLastappendixmodifieddate(rs
						.getString("lx_pd_change_date"));
				appendix.setPoclist(tempoc);
				appendix.setUnionupliftfactor(dfcurmargin.format(rs
						.getFloat("lm_js_union_uplift_factor")));
				appendix.setExpedite24upliftfactor(dfcurmargin.format(rs
						.getFloat("lm_js_expedite24_uplift_factor")));
				appendix.setExpedite48upliftfactor(dfcurmargin.format(rs
						.getFloat("lm_js_expedite48_uplift_factor")));
				appendix.setAfterhoursupliftfactor(dfcurmargin.format(rs
						.getFloat("lm_js_afterhours_uplift_factor")));
				appendix.setWhupliftfactor(dfcurmargin.format(rs
						.getFloat("lm_js_wh_uplift_factor")));
				if (rs.getString("lx_pd_msp") != null) {
					if (rs.getString("lx_pd_msp").equals("Y")) {
						temp = "Yes";
					} else {
						temp = "No";
					}
				}
				appendix.setMsp(temp);
				if (rs.getString("lx_pr_intransit_res").equals("Y")) {
					appendix.setTravelAuthorized("Yes");
				} else {
					appendix.setTravelAuthorized("No");
				}
				appendix.setCheckType(rs.getString("app_type"));

				if (rs.getString("lx_pr_test_appendix").equals("1")) {
					appendix.setTestAppendix("Yes");
				} else {
					appendix.setTestAppendix("No");
				}
				appendix.setAppendixTypeBreakout(rs
						.getString("lx_pr_abbreviation"));
				appendixlist.add(appendix);

			}
		} catch (Exception e) {
			logger.error("setAppendix(ResultSet)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setAppendix(ResultSet) - Exception Caught" + e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error("setAppendix(ResultSet)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("setAppendix(ResultSet) - Exception Caught"
							+ e);
				}
			}
		}
		return appendixlist;
	}

	public static Appendix getAppendix(String msa_id, String appendix_id,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Appendix appendix = null;

		String lx_pd_id = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select appendix_id , msa_id ,app_type, msaname ,  appendixname , lx_pr_type , "
					+ "lx_pr_cns_poc , CNSPOCname = dbo.func_cc_poc_name(lx_pr_cns_poc), lx_pd_customer_division , lo_om_division , "
					+ "lx_pd_number_sites , lx_pd_need_date , lx_pd_effective_date , lx_pd_planned_duration , lx_pd_draft_review , "
					+ "lx_pd_business_review ,lx_pd_msp, lx_pr_business_poc , BusinessPOCname =  dbo.func_cc_poc_name(lx_pr_business_poc) , "
					+ "lx_pr_contract_poc , ContractPOCname =  dbo.func_cc_poc_name(lx_pr_contract_poc) , lx_pr_billing_poc , "
					+ "BillingPOCname= dbo.func_cc_poc_name(lx_pr_billing_poc) , lx_pr_status , lx_ce_total_cost , "
					+ "lx_ce_invoice_price , lx_ce_margin , lm_js_union_uplift_factor , lm_js_expedite24_uplift_factor, "
					+ "lm_js_expedite48_uplift_factor, lm_js_afterhours_uplift_factor, lm_js_wh_uplift_factor , lx_pd_change_date , "
					+ "lx_pd_changed_by , lx_pr_type_name, lx_pr_intransit_res, lx_pr_customer_reference, lx_pr_test_appendix,lx_pr_abbreviation "
					+ "from dbo.func_lp_appendix_tab('"
					+ msa_id
					+ "' , '"
					+ appendix_id + "')";

			// System.out.println("sqlllllll22222222222222222"+sql);

			rs = stmt.executeQuery(sql);
			appendix = new Appendix();
			ArrayList appendixlist = setAppendix(rs);

			if (appendixlist.size() > 0) {
				appendix = (Appendix) appendixlist.get(0);

				sql = "select com_desc , com_by , comment_date from dbo.func_last_comment('"
						+ appendix.getAppendix_Id() + "' , 'Appendix')";

				rs = stmt.executeQuery(sql);

				if (rs.next()) {
					appendix.setLastcomment(rs.getString("com_desc"));
					appendix.setLastchangeby(rs.getString("com_by"));
					appendix.setLastchangedate(rs.getString("comment_date"));
				}
			}
		} catch (Exception e) {
			logger.error("getAppendix(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendix(String, String, DataSource) - Exception in retrieving appendix"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendix(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendix(String, String, DataSource) - Exception in retrieving appendix"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendix(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendix(String, String, DataSource) - Exception in retrieving appendix"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendix(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendix(String, String, DataSource) - Exception in retrieving appendix"
							+ sql);
				}
			}

		}
		return appendix;
	}

	public static AppendixEditBean getAppendix(
			AppendixEditBean appendixEditBean, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		DecimalFormat dfcurmargin = new DecimalFormat("0.00");

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select appendix_id , msa_id , msaname ,  appendixname , lx_pr_type , "
					+ "lx_pr_cns_poc , lx_pd_customer_division, lx_pd_number_sites , lx_pd_need_date , lx_pd_effective_date , lx_pd_planned_duration , lx_pd_draft_review , "
					+ "lx_pd_business_review , lx_pd_msp, lx_pr_business_poc , lx_pr_contract_poc , lx_pr_billing_poc , lx_pr_status , lx_ce_total_cost , "
					+ "lx_ce_invoice_price , lx_ce_margin , lm_js_union_uplift_factor , lm_js_expedite24_uplift_factor, "
					+ "lm_js_expedite48_uplift_factor, lm_js_afterhours_uplift_factor, lm_js_wh_uplift_factor , lx_pd_change_date , "
					+ "lx_pd_changed_by , lx_pr_type_name, lx_pr_customer_reference, lx_pd_created_by_name, lx_pd_create_date, lx_pd_create_time, lx_pd_change_time, "
					+ "lx_pr_intransit_res, lx_pr_test_appendix ,lx_pr_abbreviation "
					+ "from dbo.func_lp_appendix_tab('"
					+ appendixEditBean.getMsaId()
					+ "', '"
					+ appendixEditBean.getAppendixId() + "')";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				appendixEditBean.setMsaName(rs.getString("msaname"));
				appendixEditBean.setAppendixName(rs.getString("appendixname"));
				appendixEditBean.setAppendixType(rs.getString("lx_pr_type"));
				appendixEditBean.setCnsPOC(rs.getString("lx_pr_cns_poc"));
				appendixEditBean.setCustomerDivision(rs
						.getString("lx_pd_customer_division"));
				appendixEditBean.setSiteNo(rs.getString("lx_pd_number_sites"));
				appendixEditBean.setReqPlannedSch(rs
						.getString("lx_pd_need_date"));
				appendixEditBean.setEffPlannedSch(rs
						.getString("lx_pd_effective_date"));
				appendixEditBean.setSchDays(rs
						.getString("lx_pd_planned_duration"));
				appendixEditBean.setDraftReviews(rs
						.getString("lx_pd_draft_review"));
				appendixEditBean.setBusinessReviews(rs
						.getString("lx_pd_business_review"));
				appendixEditBean.setCustPocBusiness(rs
						.getString("lx_pr_business_poc"));
				appendixEditBean.setCustPocContract(rs
						.getString("lx_pr_contract_poc"));
				appendixEditBean.setCustPocBilling(rs
						.getString("lx_pr_billing_poc"));
				appendixEditBean.setAppendixPrType(rs
						.getString("lx_pr_type_name"));
				appendixEditBean.setStatus(rs.getString("lx_pr_status"));
				appendixEditBean.setEstimatedCost(Decimalroundup
						.twodecimalplaces(rs.getFloat("lx_ce_total_cost"), 2));
				appendixEditBean
						.setExtendedPrice(Decimalroundup.twodecimalplaces(
								rs.getFloat("lx_ce_invoice_price"), 2));
				appendixEditBean.setProformaMargin(dfcurmargin.format(rs
						.getFloat("lx_ce_margin")) + "");
				appendixEditBean.setUnionUpliftFactor(dfcurmargin.format(rs
						.getFloat("lm_js_union_uplift_factor")));
				appendixEditBean.setExpedite24UpliftFactor(dfcurmargin
						.format(rs.getFloat("lm_js_expedite24_uplift_factor")));
				appendixEditBean.setExpedite48UpliftFactor(dfcurmargin
						.format(rs.getFloat("lm_js_expedite48_uplift_factor")));
				appendixEditBean.setAfterhoursUpliftFactor(dfcurmargin
						.format(rs.getFloat("lm_js_afterhours_uplift_factor")));
				appendixEditBean.setWhUpliftFactor(dfcurmargin.format(rs
						.getFloat("lm_js_wh_uplift_factor")));
				appendixEditBean.setCustRef(rs
						.getString("lx_pr_customer_reference"));
				appendixEditBean.setCreateInfo(rs
						.getString("lx_pd_created_by_name")
						+ " "
						+ rs.getString("lx_pd_create_date")
						+ " "
						+ rs.getString("lx_pd_create_time"));
				appendixEditBean.setChangeInfo(rs.getString("lx_pd_changed_by")
						+ " " + rs.getString("lx_pd_change_date") + " "
						+ rs.getString("lx_pd_change_time"));
				appendixEditBean.setMsp(rs.getString("lx_pd_msp"));
				appendixEditBean.setTravelAuthorized(rs
						.getString("lx_pr_intransit_res"));
				appendixEditBean.setTestAppendix(rs
						.getString("lx_pr_test_appendix"));
				appendixEditBean.setAppendixTypeBreakout(rs
						.getString("lx_pr_abbreviation"));
			}
		} catch (Exception e) {
			logger.error("getAppendix(AppendixEditBean, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendix(AppendixEditBean, DataSource) - Exception in retrieving appendix"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendix(AppendixEditBean, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendix(AppendixEditBean, DataSource) - Exception in retrieving appendix"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendix(AppendixEditForm, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendix(AppendixEditForm, DataSource) - Exception in retrieving appendix"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendix(AppendixEditForm, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendix(AppendixEditForm, DataSource) - Exception in retrieving appendix"
							+ sql);
				}
			}

		}
		return appendixEditBean;
	}

	public static String getAppendixPrType(String Msa_Id, String Appendix_Id,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String appendixPrType = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lx_pr_type_name from dbo.func_lp_appendix_tab('"
					+ Msa_Id + "', '" + Appendix_Id + "')";
			// System.out.println(sql);
			rs = stmt.executeQuery(sql);

			if (rs.next())
				appendixPrType = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getAppendixPrType(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixPrType(String, String, DataSource) - Error Occured in retrieving appendix type"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getAppendixPrType(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixPrType(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixPrType(String, String, DataSource) - exception ignored",
						s);
			}

		}
		return appendixPrType;
	}

	public static String getAppendixPrTypeId(String Msa_Id, String Appendix_Id,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String appendixPrType = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select app_type from dbo.func_lp_appendix_tab('" + Msa_Id
					+ "', '" + Appendix_Id + "')";
			// System.out.println(sql);
			rs = stmt.executeQuery(sql);

			if (rs.next())
				appendixPrType = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getAppendixPrTypeId(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixPrTypeId(String, String, DataSource) - Error Occured in retrieving appendix type"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getAppendixPrTypeId(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixPrTypeId(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixPrTypeId(String, String, DataSource) - exception ignored",
						s);
			}

		}
		return appendixPrType;
	}

	public static String getAppendixPrType(String Appendix_Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String appendixPrType = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select isnull(lx_pr_type,'') from lx_appendix_main where lx_pr_id = "
					+ Appendix_Id;

			// System.out.println(sql);
			rs = stmt.executeQuery(sql);

			if (rs.next())
				appendixPrType = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getAppendixPrType(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixPrType(String, DataSource) - Error Occured in retrieving appendix type"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getAppendixPrType(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixPrType(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixPrType(String, DataSource) - exception ignored",
						s);
			}

		}
		return appendixPrType;
	}

	public static int addAppendix(Appendix appendix, String loginuserid,
			DataSource ds) throws Exception {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int val = -1;
		String netMedxType = null;

		if (appendix.getAppendixtypecombo().equals("3A")
				|| appendix.getAppendixtypecombo().equals("3B")
				|| appendix.getAppendixtypecombo().equals("3C")
				|| appendix.getAppendixtypecombo().equals("3D")) {
			if (appendix.getAppendixtypecombo().equals("3B")) {
				netMedxType = "Silver-Dispatch";
			}

			if (appendix.getAppendixtypecombo().equals("3C")) {
				netMedxType = "Silver-Hourly";
			}

			if (appendix.getAppendixtypecombo().equals("3D")) {
				netMedxType = "Copper";
			}

			appendix.setAppendixtypecombo("3");
		}

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lx_appendix_manage_01(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, 0);
			cstmt.setInt(3, Integer.parseInt(appendix.getMsa_Id()));
			cstmt.setString(4, "D");
			cstmt.setString(5, appendix.getName());
			cstmt.setInt(6, Integer.parseInt(appendix.getAppendixtypecombo()));
			cstmt.setInt(7, Integer.parseInt(appendix.getCnsPOCcombo()));
			cstmt.setInt(8, 0);
			cstmt.setInt(9, 0);
			cstmt.setInt(10, 0);

			cstmt.setString(11, appendix.getDraftreviewscombo());
			cstmt.setString(12, appendix.getBusinessreviewscombo());
			cstmt.setInt(13, Integer.parseInt(appendix.getSiteno()));
			cstmt.setString(14, appendix.getReqplasch());
			cstmt.setString(15, appendix.getEffplasch());
			cstmt.setInt(16, Integer.parseInt(appendix.getSchdays()));
			cstmt.setInt(17,
					Integer.parseInt(appendix.getCustomerdivisioncombo()));
			cstmt.setInt(18, Integer.parseInt(loginuserid));
			cstmt.setString(19, "A");
			cstmt.setString(20, appendix.getCustref());
			cstmt.setString(21, appendix.getUnionupliftfactor());
			cstmt.setString(22, appendix.getExpedite24upliftfactor());
			cstmt.setString(23, appendix.getExpedite48upliftfactor());
			cstmt.setString(24, appendix.getAfterhoursupliftfactor());
			cstmt.setString(25, appendix.getWhupliftfactor());
			cstmt.setString(26, appendix.getMsp());
			cstmt.setString(27, netMedxType);

			cstmt.execute();
			// System.out.println("after exectue");
			val = cstmt.getInt(1);

		}

		catch (Exception e) {
			logger.error("addAppendix(Appendix, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addAppendix(Appendix, String, DataSource) - Error Occured in Inserting Appendix"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException sql) {
				logger.warn(
						"addAppendix(Appendix, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addAppendix(Appendix, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addAppendix(Appendix, String, DataSource) - exception ignored",
						sql);
			}

		}
		return val;
	}

	/*
	 * This function is used to an appendix
	 * 
	 * @param ds DataSource containing the connection
	 * 
	 * @param appendix Appendix object to be updated
	 * 
	 * @return void see Appendix class in com.mind.dao.PM.Appendix
	 * 
	 * @exception Exception
	 */

	public static int editAppendix(AppendixEditBean appendixEditBean,
			String loginuserid, DataSource ds) throws Exception {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int val = -1;
		String netMedxType = null;

		/* Set appendix status */
		if (!appendixEditBean.getAppendixId().equals("0"))
			appendixEditBean.setStatus("0");
		else
			appendixEditBean.setStatus("D");

		if (appendixEditBean.getExpedite24UpliftFactor().equals("")
				|| appendixEditBean.getExpedite48UpliftFactor().equals("")
				|| appendixEditBean.getAfterhoursUpliftFactor().equals("")
				|| appendixEditBean.getWhUpliftFactor().equals("")) {
			ManageUpliftdao.setCheckDefaultUpliftFactor(appendixEditBean, ds);
		}

		if (appendixEditBean.getAppendixType().equals("3A")
				|| appendixEditBean.getAppendixType().equals("3B")
				|| appendixEditBean.getAppendixType().equals("3C")
				|| appendixEditBean.getAppendixType().equals("3D")) {
			if (appendixEditBean.getAppendixType().equals("3B")) {
				netMedxType = "Silver-Dispatch";
			}

			if (appendixEditBean.getAppendixType().equals("3C")) {
				netMedxType = "Silver-Hourly";
			}

			if (appendixEditBean.getAppendixType().equals("3D")) {
				netMedxType = "Copper";
			}

			appendixEditBean.setAppendixType("3");
		}

		try {
			/*
			 * System.out.println("AppId---"+Integer.parseInt(
			 * appendixEditForm.getAppendixId() ) );
			 * System.out.println("MSAId---"+Integer.parseInt(
			 * appendixEditForm.getMsaId() ) ); System.out.println("status---"+
			 * appendixEditForm.getStatus() );
			 * System.out.println("AppName---"+appendixEditForm
			 * .getAppendixName() );
			 * System.out.println("Apptype---"+Integer.parseInt(
			 * appendixEditForm.getAppendixType() ) );
			 * System.out.println("CnsPOC---"+Integer.parseInt(
			 * appendixEditForm.getCnsPOC() ) );
			 * System.out.println("custpoc busin---"+Integer.parseInt(
			 * appendixEditForm.getCustPocBusiness() ) );
			 * System.out.println("Cust poc contract---"+Integer.parseInt(
			 * appendixEditForm.getCustPocContract() ) );
			 * System.out.println("Cust poc billing---"+Integer.parseInt(
			 * appendixEditForm.getCustPocBilling() ) );
			 * System.out.println("reviews--"+appendixEditForm.getDraftReviews()
			 * ); System.out.println("business reviews---"+appendixEditForm.
			 * getBusinessReviews() );
			 * System.out.println("Site no---"+Integer.parseInt(
			 * appendixEditForm.getSiteNo() ) );
			 * System.out.println("reqplannedsch---"
			 * +appendixEditForm.getReqPlannedSch() );
			 * System.out.println("effplannedsch---"
			 * +appendixEditForm.getEffPlannedSch() );
			 * System.out.println("schdays---"+Integer.parseInt(
			 * appendixEditForm.getSchDays() ) );
			 * System.out.println("cust division---"+Integer.parseInt(
			 * appendixEditForm.getCustomerDivision() ) );
			 * System.out.println("login---"+Integer.parseInt( loginuserid ) );
			 * System.out.println("action update/edit---"+appendixEditForm.
			 * getActionUpdateEdit() ); System.out.println("custRef---"+
			 * appendixEditForm.getCustRef() );
			 * System.out.println("UnionUplift---"
			 * +appendixEditForm.getUnionUpliftFactor());
			 * System.out.println("expedite24---"
			 * +appendixEditForm.getExpedite24UpliftFactor());
			 * System.out.println
			 * ("expedite48---"+appendixEditForm.getExpedite48UpliftFactor());
			 * System.out.println("afterhours---"+appendixEditForm.
			 * getAfterhoursUpliftFactor());
			 * System.out.println("whuplift---"+appendixEditForm
			 * .getWhUpliftFactor()); System.out.println("MSP---"+
			 * appendixEditForm.getMsp());
			 * System.out.println("netmedxtype---"+netMedxType);
			 * System.out.println
			 * ("travelauthorized---"+appendixEditForm.getTravelAuthorized());
			 * System
			 * .out.println("testappendix---"+appendixEditForm.getTestAppendix
			 * ());
			 */

			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.lx_appendix_manage_01(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(appendixEditBean.getAppendixId()));
			cstmt.setInt(3, Integer.parseInt(appendixEditBean.getMsaId()));
			cstmt.setString(4, appendixEditBean.getStatus());
			cstmt.setString(5, appendixEditBean.getAppendixName());
			cstmt.setInt(6,
					Integer.parseInt(appendixEditBean.getAppendixType()));
			cstmt.setInt(7, Integer.parseInt(appendixEditBean.getCnsPOC()));
			cstmt.setInt(8,
					Integer.parseInt(appendixEditBean.getCustPocBusiness()));
			cstmt.setInt(9,
					Integer.parseInt(appendixEditBean.getCustPocContract()));
			cstmt.setInt(10,
					Integer.parseInt(appendixEditBean.getCustPocBilling()));
			cstmt.setString(11, appendixEditBean.getDraftReviews());
			cstmt.setString(12, appendixEditBean.getBusinessReviews());
			cstmt.setInt(13, Integer.parseInt(appendixEditBean.getSiteNo()));
			cstmt.setString(14, appendixEditBean.getReqPlannedSch());
			cstmt.setString(15, appendixEditBean.getEffPlannedSch());
			cstmt.setInt(16, Integer.parseInt(appendixEditBean.getSchDays()));
			cstmt.setInt(17,
					Integer.parseInt(appendixEditBean.getCustomerDivision()));
			cstmt.setInt(18, Integer.parseInt(loginuserid));
			cstmt.setString(19, appendixEditBean.getActionUpdateEdit());
			cstmt.setString(20, appendixEditBean.getCustRef());
			cstmt.setString(21, appendixEditBean.getUnionUpliftFactor());
			cstmt.setString(22, appendixEditBean.getExpedite24UpliftFactor());
			cstmt.setString(23, appendixEditBean.getExpedite48UpliftFactor());
			cstmt.setString(24, appendixEditBean.getAfterhoursUpliftFactor());
			cstmt.setString(25, appendixEditBean.getWhUpliftFactor());
			cstmt.setString(26, appendixEditBean.getMsp());
			cstmt.setString(27, netMedxType);
			cstmt.setString(28, appendixEditBean.getTravelAuthorized());
			cstmt.setString(29, appendixEditBean.getTestAppendix());
			// added for appendix type breakout
			cstmt.setString(30, appendixEditBean.getAppendixTypeBreakout());
			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("editAppendix(AppendixEditBean, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("editAppendix(AppendixEditBean, String, DataSource) - Exception caught in updating appendix"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.error(
						"editAppendix(AppendixEditForm, String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("editAppendix(AppendixEditForm, String, DataSource) - Exception caught in updating appendix"
							+ e);
				}
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"editAppendix(AppendixEditForm, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("editAppendix(AppendixEditForm, String, DataSource) - Exception in updating appendix"
							+ sql);
				}
			}
		}
		return val;
	}

	/*
	 * This function is used to an appendix
	 * 
	 * @param ds DataSource containing the connection
	 * 
	 * @param appendix Appendix object to be updated
	 * 
	 * @return void see Appendix class in com.mind.dao.PM.Appendix
	 * 
	 * @exception Exception
	 */

	public static int updateAppendixList(Appendix appendix, String loginuserid,
			DataSource ds) throws Exception {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int val = -1;
		String netMedxType = null;

		if (appendix.getAppendixtypecombo().equals("3A")
				|| appendix.getAppendixtypecombo().equals("3B")
				|| appendix.getAppendixtypecombo().equals("3C")
				|| appendix.getAppendixtypecombo().equals("3D")) {
			if (appendix.getAppendixtypecombo().equals("3B")) {
				netMedxType = "Silver-Dispatch";
			}

			if (appendix.getAppendixtypecombo().equals("3C")) {
				netMedxType = "Silver-Hourly";
			}

			if (appendix.getAppendixtypecombo().equals("3D")) {
				netMedxType = "Copper";
			}

			appendix.setAppendixtypecombo("3");
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.lx_appendix_manage_01(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(appendix.getAppendix_Id()));
			cstmt.setInt(3, Integer.parseInt(appendix.getMsa_Id()));
			cstmt.setString(4, "0");
			cstmt.setString(5, appendix.getName());
			cstmt.setInt(6, Integer.parseInt(appendix.getAppendixtypecombo()));
			cstmt.setInt(7, Integer.parseInt(appendix.getCnsPOCcombo()));
			cstmt.setInt(8,
					Integer.parseInt(appendix.getCustPocbusinesscombo()));
			cstmt.setInt(9,
					Integer.parseInt(appendix.getCustPoccontractcombo()));
			cstmt.setInt(10,
					Integer.parseInt(appendix.getCustPocbillingcombo()));
			cstmt.setString(11, appendix.getDraftreviewscombo());
			cstmt.setString(12, appendix.getBusinessreviewscombo());
			cstmt.setInt(13, Integer.parseInt(appendix.getSiteno()));
			cstmt.setString(14, appendix.getReqplasch());
			cstmt.setString(15, appendix.getEffplasch());
			cstmt.setInt(16, Integer.parseInt(appendix.getSchdays()));
			cstmt.setInt(17,
					Integer.parseInt(appendix.getCustomerdivisioncombo()));
			cstmt.setInt(18, Integer.parseInt(loginuserid));
			cstmt.setString(19, "U");
			cstmt.setString(20, appendix.getCustref());
			cstmt.setString(21, appendix.getUnionupliftfactor());
			cstmt.setString(22, appendix.getExpedite24upliftfactor());
			cstmt.setString(23, appendix.getExpedite48upliftfactor());
			cstmt.setString(24, appendix.getAfterhoursupliftfactor());
			cstmt.setString(25, appendix.getWhupliftfactor());
			cstmt.setString(26, appendix.getMsp());
			cstmt.setString(27, netMedxType);

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("updateAppendixList(Appendix, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateAppendixList(Appendix, String, DataSource) - Exception caught in updating appendix"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.error(
						"updateAppendixList(Appendix, String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("updateAppendixList(Appendix, String, DataSource) - Exception caught in updating appendix"
							+ e);
				}
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"updateAppendixList(Appendix, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("updateAppendixList(Appendix, String, DataSource) - Exception in updating appendix"
							+ sql);
				}
			}
		}
		return val;
	}

	public static String getMsaname(DataSource ds, String MSA_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		java.util.Date dateIn = new java.util.Date();

		String msaname = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select dbo.func_cc_name('" + MSA_Id + "', 'MSA' )";

			rs = stmt.executeQuery(sql);

			if (rs.next())
				msaname = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getMsaname(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsaname(DataSource, String) - Error Occured in retrieving msaname"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getMsaname(DataSource, String) - exception ignored", s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getMsaname(DataSource, String) - exception ignored", s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getMsaname(DataSource, String) - exception ignored", s);
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("getMsaname(DataSource, String) - **** getMsaname :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return msaname;
	}

	public static String getMsaUnionUplift(DataSource ds, String MSA_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String msaname = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lp_md_union_uplift_factor = isnull(lp_md_union_uplift_factor, 1.00) from lp_msa_detail where lp_md_mm_id = "
					+ MSA_Id;

			rs = stmt.executeQuery(sql);

			if (rs.next())
				msaname = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getMsaUnionUplift(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsaUnionUplift(DataSource, String) - Error Occured in retrieving Msa Union Uplift "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getMsaUnionUplift(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getMsaUnionUplift(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getMsaUnionUplift(DataSource, String) - exception ignored",
						s);
			}

		}
		return msaname;
	}

	public static String getAppendixname(DataSource ds, String Appendix_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String appendixname = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select dbo.func_cc_name('" + Appendix_Id + "', 'Appendix' )";

			rs = stmt.executeQuery(sql);

			if (rs.next())
				appendixname = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getAppendixname(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixname(DataSource, String) - Error Occured in retrieving appendixname"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getAppendixname(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixname(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixname(DataSource, String) - exception ignored",
						s);
			}

		}
		return appendixname;
	}

	public static String getAppendixcurrentstate(DataSource ds,
			String Appendix_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String appendixcurrentstate = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lx_pr_status from lx_appendix_main where lx_pr_id="
					+ Appendix_Id;

			rs = stmt.executeQuery(sql);

			if (rs.next())
				appendixcurrentstate = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getAppendixcurrentstate(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixcurrentstate(DataSource, String) - Error Occured in retrieving appendix current state"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getAppendixcurrentstate(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixcurrentstate(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixcurrentstate(DataSource, String) - exception ignored",
						s);
			}

		}
		return appendixcurrentstate;
	}

	public static int deleteappendix(String Id, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int val = -1;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lx_appendix_delete_01( ?  ) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(Id));

			cstmt.execute();
			val = cstmt.getInt(1);

		}

		catch (Exception e) {
			logger.error("deleteappendix(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteappendix(String, DataSource) - Exception in DELETING Appendix"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sqle) {
				logger.error("deleteappendix(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteappendix(String, DataSource) - Exception in DELETING Appendix"
							+ sqle);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error("deleteappendix(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteappendix(String, DataSource) - Exception in DELETING Appendix"
							+ sqle);
				}
			}

		}
		return val;
	}

	public static ArrayList getInworkState(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList state = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from lp_appendix_signed";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				if (state == null)
					state = new ArrayList();
				state.add(new PMInworkStateList(rs.getString("lo_ot_name"), rs
						.getString("lx_pr_id"), rs.getString("lx_pr_title"), rs
						.getString("lx_pr_status")));
			}
		} catch (Exception e) {
			logger.error("getInworkState(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getInworkState(DataSource) - Exception in getInformState"
						+ e);
			}
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sqle) {
				logger.error("getInworkState(DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("getInworkState(DataSource) - SQLException while closing statement in getInformState"
							+ sqle);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error("getInworkState(DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("getInworkState(DataSource) - SQLException while closing connection in getInformState"
							+ sqle);
				}
			}
		}
		return state;
	}

	public static int setInworkState(String id, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;

		int val = -1;
		try {
			conn = ds.getConnection();

			cstmt = conn.prepareCall("{?=call cc_status_change(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, id);
			cstmt.setString(3, "Appendix");
			cstmt.setString(4, "I");

			cstmt.execute();

			val = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("setInworkState(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setInworkState(String, DataSource) - Exception in setInformState"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sqle) {
				logger.error("setInworkState(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("setInworkState(String, DataSource) - SQLException while closing statement in setInformState"
							+ sqle);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error("setInworkState(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("setInworkState(String, DataSource) - SQLException while closing connection in setInformState"
							+ sqle);
				}
			}

		}
		return val;
	}

	public static int setrevisionsummary(String Id, String revisionidentifier,
			DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int val = -1;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lx_generate_revision_identifier_01( ? , ?  ) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(Id));
			cstmt.setInt(3, Integer.parseInt(revisionidentifier));
			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("setrevisionsummary(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setrevisionsummary(String, String, DataSource) - Exception in setting revision identifier"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sqle) {
				logger.error("setrevisionsummary(String, String, DataSource)",
						sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("setrevisionsummary(String, String, DataSource) - Exception in setting revision identifier"
							+ sqle);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error("setrevisionsummary(String, String, DataSource)",
						sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("setrevisionsummary(String, String, DataSource) - Exception in setting revision identifier"
							+ sqle);
				}
			}

		}
		return val;
	}

	public static String getAppendixtype(DataSource ds, String Appendix_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String appendixtype = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lx_pr_type from lx_appendix_main where lx_pr_id="
					+ Appendix_Id;

			rs = stmt.executeQuery(sql);

			if (rs.next())
				appendixtype = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getAppendixtype(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixtype(DataSource, String) - Error Occured in retrieving appendix current state"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getAppendixtype(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixtype(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixtype(DataSource, String) - exception ignored",
						s);
			}

		}
		return appendixtype;
	}

	public static String getmsascheduledate(DataSource ds, String MSA_Id,
			String dateselect) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String scheduledate = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (dateselect.equals("start")) {
				sql = "select lp_md_need_date = isnull( convert( varchar( 10 ) , lp_md_need_date , 101 ) , '' ) "
						+ "from lp_msa_detail where lp_md_mm_id = " + MSA_Id;
			} else
				sql = "select lp_md_effective_date = isnull( convert( varchar( 10 ) , lp_md_effective_date , 101 ) , '' ) "
						+ "from lp_msa_detail where lp_md_mm_id = " + MSA_Id;

			rs = stmt.executeQuery(sql);

			if (rs.next())
				scheduledate = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getmsascheduledate(DataSource, String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getmsascheduledate(DataSource, String, String) - Error Occured in retrieving msa scheduledate"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getmsascheduledate(DataSource, String, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getmsascheduledate(DataSource, String, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getmsascheduledate(DataSource, String, String) - exception ignored",
						s);
			}

		}
		return scheduledate;
	}

	public static String getAddendumsIdName(String Appendix_Id, DataSource ds) {
		return getAddendumsIdName(Appendix_Id, ds, false);
	}

	public static String getAddendumsIdName(String Appendix_Id, DataSource ds, boolean newMenu) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		String addendumlist = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_js_title, lm_js_id from dbo.func_lp_addendum_id_name( '"
					+ Appendix_Id + "' )";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				if (!newMenu) {
					addendumlist = addendumlist + "\""
							+ rs.getString("lm_js_title") + "\"" + ","
							+ "\"javascript:viewaddendumpdf('"
							+ rs.getString("lm_js_id") + "')\"" + "," + "0,";
					addendumlist = addendumlist + "\n";
				} else {
					addendumlist += "<li><a href=\"javascript:viewaddendumpdf('" + rs.getString("lm_js_id") + "')\">" + rs.getString("lm_js_title") + "</a></li>";
				}
			}

			if (!newMenu && addendumlist != "") {
				addendumlist = addendumlist.substring(0,
						addendumlist.length() - 2);
			}
		}

		catch (Exception e) {
			logger.error("getAddendumsIdName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAddendumsIdName(String, DataSource) - Error Occured in generating addendum idname"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getAddendumsIdName(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getAddendumsIdName(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getAddendumsIdName(String, DataSource) - exception ignored",
						s);
			}

		}
		return addendumlist;
	}

	public static String getcontractDocoumentMenu(String Appendix_Id,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		int i = 1;
		
		String appendMenu = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_js_title, lm_js_id, lm_view_type, lx_pr_type,lm_js_status,lm_js_type from dbo.func_lp_addendum_name( '"
					+ Appendix_Id + "' )";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				appendMenu += "<li><a href=\"#\"><span>"+rs.getString("lm_js_title")+"</span></a><ul>";
				if (rs.getString("lm_js_status").equals("D")
						|| rs.getString("lm_js_status").equals("R")
						|| rs.getString("lm_js_status").equals("C")) {
					appendMenu += "<li><a href=\"javascript:alert(canNotSendNonApprovedAddendum);\">Send</a></li>";
					appendMenu += "<li><a href=\"#\"><span>View</span></a><ul>";
				} else {
					if (rs.getString("lm_js_type").equalsIgnoreCase("Addendum")) {
						appendMenu += "<li><a href=\"javascript:sendaddendumemail('" + rs.getString("lm_js_id") + "');\">Send</a></li>";
						appendMenu += "<li><a href=\"#\"><span>View</span></a><ul>";
					} else {
						appendMenu += "<li><a href=\"javascript:sendemail('" + rs.getString("lm_js_id") + "');\">Send</a></li>";
						appendMenu += "<li><a href=\"#\"><span>View</span></a><ul>";
					}
				}

				if (rs.getString("lm_view_type").equals("All")) {
					if (rs.getString("lx_pr_type").equals("1")
							|| rs.getString("lx_pr_type").equals("2")) {
						appendMenu += "<li><a href=\"javascript:viewContractDocument('" + rs.getString("lm_js_id") + "', 'pdf');\">PDF</a></li>";
						appendMenu += "<li><a href=\"javascript:viewContractDocument('" + rs.getString("lm_js_id") + "', 'rtf');\">RTF</a></li>";
						appendMenu += "<li><a href=\"javascript:viewContractDocument('" + rs.getString("lm_js_id") + "', 'html');\">HTML</a></li>";
						appendMenu += "<li><a href=\"javascript:viewContractDocument('" + rs.getString("lm_js_id") + "', 'excel');\">EXCEL</a></li>";
					} else {
						appendMenu += "<li><a href=\"javascript:viewContractDocument('" + rs.getString("lm_js_id") + "', 'pdf');\">PDF</a></li>";
						appendMenu += "<li><a href=\"javascript:viewContractDocument('" + rs.getString("lm_js_id") + "', 'rtf');\">RTF</a></li>";
						appendMenu += "<li><a href=\"javascript:viewContractDocument('" + rs.getString("lm_js_id") + "', 'html');\">HTML</a></li>";
					}
				} else {
					if (rs.getString("lx_pr_type").equals("1")
							|| rs.getString("lx_pr_type").equals("2")) {
						appendMenu += "<li><a href=\"javascript:viewContractDocument('" + rs.getString("lm_js_id") + "', 'pdf');\">PDF</a></li>";

					} else {
						appendMenu += "<li><a href=\"javascript:viewContractDocument('" + rs.getString("lm_js_id") + "', 'pdf');\">PDF</a></li>";
					}
				}
				
				appendMenu += "</ul></li></ul></li>";
				i++;
			}

			appendMenu += "<li><a href=\"javascript:addaddendum('add', 'add');\">New Addendum</a></li>";
		}

		catch (Exception e) {
			logger.error("getcontractDocoumentMenu(" + Appendix_Id + ", " + ds
					+ ")", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getcontractDocoumentMenu(String, DataSource) - Error Occured in generating addendum idname"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getcontractDocoumentMenu(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getcontractDocoumentMenu(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getcontractDocoumentMenu(String, DataSource) - exception ignored",
						s);
			}

		}

		return appendMenu;
	}

}
