package com.mind.dao.PM;

public class Job 
{
	private String appendix_Id = null;
	private String job_Id = null;
	
	private String name = null;
	private String custref = null;
	private String jobtype = null;
	private String union = null;
	private String locality_uplift = null;
	private String union_uplift_factor = null;
	private String site_id = null;
	private String site_name = null;
	private String site_number = null;
	private String site_address = null;
	private String site_city = null;
	private String site_state = null;
	private String site_zipcode = null;
	private String site_POC = null;
	private String site_phone = null;

	private String estimated_cnsfieldlaborcost = null;
	
	
	private String estimated_contractfieldlaborcost = null;
	private String estimated_freightcost = null;
	private String estimated_travelcost = null;
	private String estimated_materialcost = null;
	private String estimated_totalcost = null;
	
	private String extendedprice = null;
	private String proformamargin =null;
	private String estimated_start_schedule = null;
	private String estimated_complete_schedule = null;
	
	private String lastcomment = null;
	private String lastchangeby = null;
	private String lastchangedate = null;
	private String status = null;
	
	private String lastjobmodifiedby = null;
	private String lastjobmodifieddate = null;
	private String CNS_POC = null;
	private String installation_POC = null;
	
	private String addendum_id = null;
	
	private String msa_Id = null;
	private String msaname = null;
	
	
	public String getAppendix_Id() {
		return appendix_Id;
	}
	public void setAppendix_Id( String appendix_Id ) {
		this.appendix_Id = appendix_Id;
	}
	
	
	public String getJob_Id() {
		return job_Id;
	}
	public void setJob_Id( String job_Id ) {
		this.job_Id = job_Id;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName( String name ) {
		this.name = name;
	}
	
	
	public String getJobtype() {
		return jobtype;
	}
	public void setJobtype( String jobtype ) {
		this.jobtype = jobtype;
	}
	
	
	public String getUnion() {
		return union;
	}
	public void setUnion( String union ) {
		this.union = union;
	}
	
	
	public String getLocality_uplift() {
		return locality_uplift;
	}
	public void setLocality_uplift( String locality_uplift ) {
		this.locality_uplift = locality_uplift;
	}
	
	
	public String getSite_id() {
		return site_id;
	}
	public void setSite_id( String site_id ) {
		this.site_id = site_id;
	}
	
	public String getSite_name() {
		return site_name;
	}
	public void setSite_name( String site_name ) {
		this.site_name = site_name;
	}
	
	
	public String getSite_number() {
		return site_number;
	}
	public void setSite_number( String site_number ) {
		this.site_number = site_number;
	}
	
	
	public String getSite_address() {
		return site_address;
	}
	public void setSite_address( String site_address ) {
		this.site_address = site_address;
	}
	
	
	public String getSite_city() {
		return site_city;
	}
	public void setSite_city( String site_city ) {
		this.site_city = site_city;
	}
	
	
	public String getSite_state() {
		return site_state;
	}
	public void setSite_state( String site_state ) {
		this.site_state = site_state;
	}
	
	
	public String getSite_zipcode() {
		return site_zipcode;
	}
	public void setSite_zipcode( String site_zipcode ) {
		this.site_zipcode = site_zipcode;
	}
	
	
	public String getSite_POC() {
		return site_POC;
	}
	public void setSite_POC( String site_POC ) {
		this.site_POC = site_POC;
	}
	
	
	public String getUnion_uplift_factor() {
		return union_uplift_factor;
	}
	public void setUnion_uplift_factor( String union_uplift_factor ) {
		this.union_uplift_factor = union_uplift_factor;
	}
	
	
	public String getSite_phone() {
		return site_phone;
	}
	public void setSite_phone( String site_phone ) {
		this.site_phone = site_phone;
	}
	
	
	public String getEstimated_cnsfieldlaborcost() {
		return estimated_cnsfieldlaborcost;
	}
	public void setEstimated_cnsfieldlaborcost( String estimated_cnsfieldlaborcost ) {
		this.estimated_cnsfieldlaborcost = estimated_cnsfieldlaborcost;
	}
	
	
	public String getEstimated_contractfieldlaborcost() {
		return estimated_contractfieldlaborcost;
	}
	public void setEstimated_contractfieldlaborcost( String estimated_contractfieldlaborcost ) {
		this.estimated_contractfieldlaborcost = estimated_contractfieldlaborcost;
	}
	
	
	public String getEstimated_materialcost() {
		return estimated_materialcost;
	}
	public void setEstimated_materialcost( String estimated_materialcost ) {
		this.estimated_materialcost = estimated_materialcost;
	}
	
	
	public String getEstimated_freightcost() {
		return estimated_freightcost;
	}
	public void setEstimated_freightcost( String estimated_freightcost ) {
		this.estimated_freightcost = estimated_freightcost;
	}
	
	
	public String getEstimated_travelcost() {
		return estimated_travelcost;
	}
	public void setEstimated_travelcost( String estimated_travelcost ) {
		this.estimated_travelcost = estimated_travelcost;
	}
	public String getEstimated_totalcost() {
		return estimated_totalcost;
	}
	public void setEstimated_totalcost( String estimated_totalcost ) {
		this.estimated_totalcost = estimated_totalcost;
	}
	
	
	public String getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice( String extendedprice ) {
		this.extendedprice = extendedprice;
	}
	
	
	public String getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin( String proformamargin ) {
		this.proformamargin = proformamargin;
	}
	
	
	public String getEstimated_start_schedule() {
		return estimated_start_schedule;
	}
	public void setEstimated_start_schedule( String estimated_start_schedule ) {
		this.estimated_start_schedule = estimated_start_schedule;
	}
	
	
	public String getEstimated_complete_schedule() {
		return estimated_complete_schedule;
	}
	public void setEstimated_complete_schedule( String estimated_complete_schedule ) {
		this.estimated_complete_schedule = estimated_complete_schedule;
	}

	public String getLastcomment() {
		return lastcomment;
	}
	public void setLastcomment( String lastcomment ) {
		this.lastcomment = lastcomment;
	}
	
	
	public String getLastchangeby() {
		return lastchangeby;
	}
	public void setLastchangeby( String lastchangeby ) {
		this.lastchangeby = lastchangeby;
	}
	
	
	public String getLastchangedate() {
		return lastchangedate;
	}
	public void setLastchangedate( String lastchangedate ) {
		this.lastchangedate = lastchangedate;
	}
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus( String status ) {
		this.status = status;
	}
	
	
	public String getLastjobmodifiedby() {
		return lastjobmodifiedby;
	}
	public void setLastjobmodifiedby( String lastjobmodifiedby ) {
		this.lastjobmodifiedby = lastjobmodifiedby;
	}
	
	
	public String getLastjobmodifieddate() {
		return lastjobmodifieddate;
	}
	public void setLastjobmodifieddate( String lastjobmodifieddate ) {
		this.lastjobmodifieddate = lastjobmodifieddate;
	}
	
	
	public String getCNS_POC() {
		return CNS_POC;
	}
	public void setCNS_POC( String cns_poc ) {
		CNS_POC = cns_poc;
	}
	
	
	public String getInstallation_POC() {
		return installation_POC;
	}
	public void setInstallation_POC( String installation_POC ) {
		this.installation_POC = installation_POC;
	}
	
	
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id( String addendum_id ) {
		this.addendum_id = addendum_id;
	}
	
	
	public String getMsa_Id() {
		return msa_Id;
	}
	public void setMsa_Id(String msa_Id) {
		this.msa_Id = msa_Id;
	}
	
	
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	public String getCustref() {
		return custref;
	}
	public void setCustref(String custref) {
		this.custref = custref;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
