/*
 * This class is used to diplay activities , insert activities , modify activity
 * and delete activity
 * 
 * see Activity class in com.mind.dao.PM.Activity
 */

package com.mind.dao.PM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.ActivityGridControl;
import com.mind.bean.pm.Activity;
import com.mind.bean.pm.ActivityEditBean;
import com.mind.common.util.MySqlConnection;
import com.mind.fw.core.dao.util.DBUtil;

public class Activitydao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Activitydao.class);

	public static String[] getActivityTypeStatus(String Activity_Id,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] statusAndType = new String[2];
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select * from dbo.func_lp_activity_tab ('%','" + Activity_Id
					+ "','%')";

			rs = stmt.executeQuery(sql);

			if (rs.next())
				statusAndType[0] = rs.getString("lm_at_status");
			statusAndType[1] = rs.getString("lm_at_type");
		}

		catch (Exception e) {
			logger.error("getActivityTypeStatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityTypeStatus(String, DataSource) - Error Occured in retrieving activity type and status"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getActivityTypeStatus(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getActivityTypeStatus(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getActivityTypeStatus(String, DataSource) - exception ignored",
						s);
			}

		}
		return statusAndType;
	}

	/**
	 * Method used to get the array of activityId where bidflag is 1
	 * 
	 * @param Job_Id
	 * @param ds
	 * @return
	 */
	public static String[] getBidFlag(String Job_Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] bidFlag = null;
		String sql = null;
		int i = 0;
		int cnt = 0;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select count(*) as cnt from lm_activity where lm_at_bid_flag = 1 and lm_at_js_id = "
					+ Job_Id;
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				cnt = rs.getInt("cnt");
			}
			bidFlag = new String[cnt];

			sql = "select lm_at_id from lm_activity where lm_at_bid_flag = 1 and lm_at_js_id = "
					+ Job_Id;
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				bidFlag[i] = rs.getString(1);
				i++;
			}
		} catch (Exception e) {
			logger.error("getBidFlag(String, DataSource)", e);

			logger.error("getBidFlag(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return bidFlag;
	}

	public static ArrayList getActivityList(DataSource ds, String Job_Id,
			String activity_Id, String querystring, String flag) {
		ArrayList all_activities = new ArrayList();
		Connection conn = null;
		Activity activity = null;
		Statement stmt = null;
		ResultSet rs = null;
		String addendumflag = "";
		if (querystring.equals("")) {
			// querystring = "order by activityname";
		}

		if (flag.equals("AA") || flag.equals("CA")) {
			addendumflag = "Addendum";
		} else {
			addendumflag = "%";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lp_activity_tab('" + Job_Id
					+ "' , '" + activity_Id + "' , '" + flag + "' )"
					+ querystring;
			if (logger.isDebugEnabled()) {
				logger.debug("getActivityList(DataSource, String, String, String, String) - SQL is  : "
						+ sql);
			}
			rs = stmt.executeQuery(sql);
			all_activities = setActivity(rs);
		}

		catch (SQLException e) {
			logger.error(
					"getActivityList(DataSource, String, String, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityList(DataSource, String, String, String, String) - Exception caught in getting all activities"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getActivityList(DataSource, String, String, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityList(DataSource, String, String, String, String) - Exception caught in getting all activities"
							+ sql);
				}
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.error(
						"getActivityList(DataSource, String, String, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityList(DataSource, String, String, String, String) - Exception caught in getting all activities"
							+ sql);
				}
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.error(
						"getActivityList(DataSource, String, String, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityList(DataSource, String, String, String, String) - Exception caught in getting all activities"
							+ sql);
				}
			}
		}

		return all_activities;
	}

	public static void getActivityDetail(ActivityEditBean activityEditBean,
			DataSource ds, String activity_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList activityDetail = new ArrayList();
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql;
			/*
			 * String sql=
			 * "select lm_at_title,lm_at_type,lx_ce_quantity,lx_ce_material_cost,lx_ce_cns_field_labor_cost,"
			 * +
			 * "lx_ce_ctr_field_labor_cost,lx_ce_frieght_cost,lx_ce_travel_cost,lx_ce_total_cost,"
			 * +
			 * "lx_ce_total_price,lx_ce_overhead_cost,lx_ce_invoice_price,lx_ce_margin,"
			 * +
			 * "lm_at_planned_start_date,lm_at_planned_end_date,lm_at_changed_by,lm_at_change_date,lm_at_status,"
			 * +
			 * "lm_at_id,lx_ce_hq_field_labor_cost,rew from dbo.func_lp_activity_tab ('%','"
			 * +activity_Id+"','%')";
			 */
			// System.out.println("sqlllllllllllllllllllllllllllllll"+sql);
			sql = "select * from dbo.func_lp_activity_tab ('%','" + activity_Id
					+ "','%')";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				activityEditBean.setJobId(rs.getString(1));
				activityEditBean.setActivity_Id(rs.getString(2));
				activityEditBean.setActivityName(rs.getString(3));
				activityEditBean.setActivityType(rs.getString("rew"));
				activityEditBean.setActivityTypeDetail(rs.getString(4));
				activityEditBean.setQuantity(rs.getString(5));
				activityEditBean.setEstimatedMaterialcost(dfcur.format(rs
						.getFloat(6)) + "");
				activityEditBean.setEstimatedCnsfieldlaborcost(dfcur.format(rs
						.getFloat(7)) + "");
				activityEditBean.setEstimatedContractfieldlaborcost(dfcur
						.format(rs.getFloat(8)) + "");
				activityEditBean.setEstimatedFreightcost(dfcur.format(rs
						.getFloat(9)) + "");
				activityEditBean.setEstimatedTravelcost(dfcur.format(rs
						.getFloat(10)) + "");
				activityEditBean.setEstimatedTotalcost(dfcur.format(rs
						.getFloat(11)) + "");
				activityEditBean.setExtendedprice(dfcur.format(rs.getFloat(12))
						+ "");
				activityEditBean.setOverheadcost(dfcur.format(rs.getFloat(13))
						+ "");

				if (activityEditBean.getActivityTypeDetail().equals("Overhead")) {
					activityEditBean.setListprice("0.00");
					activityEditBean.setProformamargin("0.00");
				} else {
					activityEditBean.setListprice(dfcur.format(rs.getFloat(14))
							+ "");
					activityEditBean.setProformamargin(dfcurmargin.format(rs
							.getFloat(15)) + "");
				}

				activityEditBean.setNewStatus(rs.getString(16));
				activityEditBean.setJobType(rs.getString(17));
				activityEditBean.setEstimatedStartSchedule(rs.getString(18));
				activityEditBean.setEstimatedCompleteSchedule(rs.getString(19));
				activityEditBean.setLastactivitymodifiedby(rs.getString(20));
				activityEditBean.setLastactivitymodifieddate(rs.getString(21));
				activityEditBean.setEstimatedCnshqlaborcost(dfcur.format(rs
						.getFloat(22)) + "");
				activityEditBean.setEstimatedCnsfieldhqlaborcost(dfcur
						.format(rs.getFloat(7) + rs.getFloat(22)) + "");
				activityEditBean.setAppendixid(rs.getString("lx_pr_id"));
				activityEditBean.setAppendixName(rs.getString("lx_pr_title"));
				activityEditBean.setMsaId(rs.getString("lp_md_mm_id"));
				activityEditBean.setMsaName(rs.getString("lo_ot_name"));
				activityEditBean.setCreatBy(rs.getString(30));
				activityEditBean.setCreatDate(rs.getString(31));
				activityDetail.add(activityDetail);
			}

			if (activityDetail.size() > 0) {
				sql = "select com_desc , com_by , comment_date from dbo.func_last_comment('"
						+ activityEditBean.getActivity_Id() + "' , 'Activity')";
				rs = stmt.executeQuery(sql);
				if (rs.next()) {
					activityEditBean.setComment(rs.getString("com_desc"));
					activityEditBean.setCommentby(rs.getString("com_by"));
					activityEditBean.setCommentdate(rs
							.getString("comment_date"));
				}
			}
		}

		catch (SQLException e) {
			logger.error(
					"getActivityDetail(ActivityEditForm, DataSource, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityDetail(ActivityEditForm, DataSource, String) - Exception caught in getting an activities"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getActivityDetail(ActivityEditForm, DataSource, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityDetail(ActivityEditForm, DataSource, String) - Exception caught in getting an activities"
							+ sql);
				}
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.error(
						"getActivityDetail(ActivityEditForm, DataSource, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityDetail(ActivityEditForm, DataSource, String) - Exception caught in getting an activities"
							+ sql);
				}
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.error(
						"getActivityDetail(ActivityEditForm, DataSource, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityDetail(ActivityEditForm, DataSource, String) - Exception caught in getting an activities"
							+ sql);
				}
			}
		}
	}

	/**
	 * This method is used to retrieve activity status
	 * 
	 * @param Id
	 *            id of activity
	 * @param ds
	 *            object of DataSource
	 * @return String
	 */

	public static String getActivitystatus(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String status = "";
		java.util.Date dateIn = new java.util.Date();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			// String sql =
			// "select lm_at_status from dbo.func_lp_activity_tab('%' , '"+Id+"' , '%' )";
			String sql = "select cc_status_description from lm_activity "
					+ "inner join cc_status_master on lm_at_status = cc_status_code where lm_at_id = "
					+ Id;
			rs = stmt.executeQuery(sql);
			if (rs.next())
				status = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getActivitystatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivitystatus(String, DataSource) - Exception in retrieving Activity status"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitystatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitystatus(String, DataSource) - Exception in retrieving Activity status"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitystatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitystatus(String, DataSource) - Exception in retrieving Activity status"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitystatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitystatus(String, DataSource) - Exception in retrieving Activity status"
							+ sql);
				}
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("getActivitystatus(String, DataSource) - **** getActivitystatus :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return status;
	}

	public static ArrayList setActivity(ResultSet rs) {
		ArrayList activitylist = new ArrayList();
		Activity activity;
		String temp = "";
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");

		try {
			while (rs.next()) {
				activity = new Activity();

				activity.setJob_Id(rs.getString(1));
				activity.setActivity_Id(rs.getString(2));

				activity.setName(rs.getString(3));
				activity.setActivitytype(rs.getString(4));
				activity.setQuantity(rs.getString(5));
				activity.setEstimated_materialcost(dfcur.format(rs.getFloat(6))
						+ "");
				activity.setEstimated_cnsfieldlaborcost(dfcur.format(rs
						.getFloat(7)) + "");
				activity.setEstimated_contractfieldlaborcost(dfcur.format(rs
						.getFloat(8)) + "");
				activity.setEstimated_freightcost(dfcur.format(rs.getFloat(9))
						+ "");
				activity.setEstimated_travelcost(dfcur.format(rs.getFloat(10))
						+ "");
				activity.setEstimated_totalcost(dfcur.format(rs.getFloat(11))
						+ "");
				activity.setExtendedprice(dfcur.format(rs.getFloat(12)) + "");
				activity.setOverheadcost(dfcur.format(rs.getFloat(13)) + "");

				if (activity.getActivitytype().equals("Overhead")) {
					activity.setListprice("0.00");
					activity.setProformamargin("0.00");
				} else {
					activity.setListprice(dfcur.format(rs.getFloat(14)) + "");
					activity.setProformamargin(dfcurmargin.format(rs
							.getFloat(15)) + "");
				}

				activity.setStatus(rs.getString(16));
				activity.setJobtype(rs.getString(17));
				activity.setEstimated_start_schedule(rs.getString(18));
				activity.setEstimated_complete_schedule(rs.getString(19));
				activity.setLastactivitymodifiedby(rs.getString(20));
				activity.setLastactivitymodifieddate(rs.getString(21));

				activity.setEstimated_cnshqlaborcost(dfcur.format(rs
						.getFloat(22)) + "");

				activity.setEstimated_cnsfieldhqlaborcost(dfcur.format(rs
						.getFloat(7) + rs.getFloat(22))
						+ "");

				activity.setActivitytypecombo(rs.getString("rew"));
				activity.setEstimated_unitcost(dfcur.format(rs
						.getFloat("lx_ce_unit_cost")) + "");
				activity.setEstimated_unitprice(dfcur.format(rs
						.getFloat("lx_ce_unit_price")) + "");

				activity.setAppendix_id(rs.getString("lx_pr_id"));
				activity.setAppendixname(rs.getString("lx_pr_title"));
				activity.setMsa_id(rs.getString("lp_md_mm_id"));
				activity.setMsaname(rs.getString("lo_ot_name"));
				activity.setBidFlag(rs.getString("lm_at_bid_flag")); // - Add
																		// new
																		// field
																		// for
																		// Bid
																		// Flag
																		// on
																		// 08/07/2008
				activity.setCommissionCost(dfcur.format(rs
						.getFloat("commission_cost")) + "");
				activity.setCommissionRevenue(dfcur.format(rs
						.getFloat("commission_revenue")) + "");

				activitylist.add(activity);
			}
		} catch (Exception e) {
			logger.error("setActivity(ResultSet)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setActivity(ResultSet) - Exception Caught in setActivity"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error("setActivity(ResultSet)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("setActivity(ResultSet) - Exception Caught in setActivity"
							+ e);
				}
			}
		}
		return activitylist;
	}

	public static Activity getActivity(String Activity_Id, String flag,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String addendumflag = "";

		Activity activity = null;

		if (flag.equals("AA") || flag.equals("CA")) {
			addendumflag = "Addendum";
		} else {
			addendumflag = "%";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lp_activity_tab('%', '"
					+ Activity_Id + "' , '" + flag + "' )";
			rs = stmt.executeQuery(sql);
			activity = new Activity();
			ArrayList activitylist = setActivity(rs);
			activity = (Activity) activitylist.get(0);

			sql = "select com_desc , com_by , comment_date from dbo.func_last_comment('"
					+ Activity_Id + "' , 'Activity')";
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				activity.setLastcomment(rs.getString("com_desc"));
				activity.setLastchangeby(rs.getString("com_by"));
				activity.setLastchangedate(rs.getString("comment_date"));
			}
		}

		catch (Exception e) {
			logger.error("getActivity(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivity(String, String, DataSource) - Exception in retrieving Activity"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivity(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivity(String, String, DataSource) - Exception in retrieving Activity"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivity(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivity(String, String, DataSource) - Exception in retrieving Activity"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivity(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivity(String, String, DataSource) - Exception in retrieving Activity"
							+ sql);
				}
			}

		}
		return activity;
	}

	public static String getJobname(DataSource ds, String Job_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String jobname = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select dbo.func_cc_name('" + Job_Id + "', 'Job' )";
			rs = stmt.executeQuery(sql);

			if (rs.next())
				jobname = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getJobname(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobname(DataSource, String) - Error Occured in retrieving jobname"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getJobname(DataSource, String) - exception ignored", s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getJobname(DataSource, String) - exception ignored", s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getJobname(DataSource, String) - exception ignored", s);
			}

		}
		return jobname;
	}

	public static String getmsaid(DataSource ds, String Job_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String msa_Id = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lp_mm_id from lp_proposal_hierarchy_01 where lm_js_id="
					+ Job_Id;
			rs = stmt.executeQuery(sql);

			if (rs.next())
				msa_Id = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getmsaid(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getmsaid(DataSource, String) - Error Occured in retrieving msaid"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn("getmsaid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn("getmsaid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn("getmsaid(DataSource, String) - exception ignored",
						s);
			}

		}
		return msa_Id;
	}

	public static ArrayList getActivityListFromlib(DataSource ds,
			String msa_Id, String querystring, String Job_Id) {
		ArrayList all_activities = new ArrayList();
		Connection conn = null;
		Activity activity = null;
		Statement stmt = null;
		ResultSet rs = null;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		if (querystring.equals("")) {
			querystring = "order by lm_at_title";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lp_activity_copy_tab('"
					+ msa_Id + "','%' ,'%' , '" + Job_Id
					+ "') where lm_at_status='Approved'" + querystring;
			// System.out.println("SSSSSs"+sql);

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				activity = new Activity();

				activity.setActivity_Id(rs.getString(2));
				activity.setName(rs.getString(3));
				activity.setStatus(rs.getString(4));
				activity.setActivitytype(rs.getString(5));

				activity.setActivitytypecombo(rs.getString("lm_act_type"));
				activity.setEstimated_materialcost(dfcur.format(rs.getFloat(6))
						+ "");
				activity.setEstimated_contractfieldlaborcost(dfcur.format(rs
						.getFloat(7)) + "");
				activity.setEstimated_cnsfieldlaborcost(dfcur.format(rs
						.getFloat(8)) + "");

				activity.setEstimated_freightcost(dfcur.format(rs.getFloat(9))
						+ "");
				activity.setEstimated_travelcost(dfcur.format(rs.getFloat(10))
						+ "");
				activity.setQuantity(rs.getString(11));

				activity.setEstimated_totalcost(dfcur.format(rs.getFloat(12))
						+ "");
				activity.setExtendedprice(dfcur.format(rs.getFloat(13)) + "");
				activity.setOverheadcost("0.00");
				activity.setListprice(dfcur.format(rs.getFloat(13)) + "");

				if (activity.getListprice().equals("0.00"))
					activity.setProformamargin("0.00");
				else {
					activity.setProformamargin(dfcur.format(1 - (rs
							.getFloat(12) / rs.getFloat(13))) + "");
				}

				activity.setActivity_lib_Id(activity.getActivity_Id());

				activity.setEstimated_cnshqlaborcost(dfcur.format(rs
						.getFloat(18)) + "");

				activity.setEstimated_cnsfieldhqlaborcost(dfcur.format(rs
						.getFloat(8) + rs.getFloat(18))
						+ "");

				// System.out.println("SSSSSsqqqqqqqqqqqqqqqqqq"+activity.getEstimated_cnsfieldhqlaborcost()
				// );

				// job.setSite_name( rs.getString( "lm_si_name" ) );
				// job.setSite_address( rs.getString( "lm_si_address") );
				// job.setSite_city( rs.getString( "lm_si_city" ) );
				// job.setSite_state( rs.getString( "lm_si_state" ) );
				// job.setSite_zipcode( rs.getString( "lm_si_zip_code" ) );

				all_activities.add(activity);
			}
		}

		catch (SQLException e) {
			logger.error(
					"getActivityListFromlib(DataSource, String, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityListFromlib(DataSource, String, String, String) - Exception caught in getting all activities from lib"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getActivityListFromlib(DataSource, String, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityListFromlib(DataSource, String, String, String) - Exception caught in getting all activities from lib"
							+ sql);
				}
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.error(
						"getActivityListFromlib(DataSource, String, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityListFromlib(DataSource, String, String, String) - Exception caught in getting all activities from lib"
							+ sql);
				}
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.error(
						"getActivityListFromlib(DataSource, String, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityListFromlib(DataSource, String, String, String) - Exception caught in getting all activities from lib"
							+ sql);
				}
			}
		}

		return all_activities;
	}

	// fromjobdashboard will be used while copying actities from job dashboard
	// using New Activity link//
	public static String Addactivity(Activity activity,
			String activity_cost_type, String loginuserid, DataSource ds,
			String fromjobdashboard) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;
		int activityId = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.lm_activity_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(activity.getActivity_Id()));
			cstmt.setInt(3, Integer.parseInt(activity.getJob_Id()));
			cstmt.setString(4, activity.getName());
			cstmt.setFloat(5, Float.parseFloat(activity.getQuantity()));
			cstmt.setString(6, activity.getActivitytypecombo());
			cstmt.setFloat(7,
					Float.parseFloat(activity.getEstimated_materialcost()));
			cstmt.setFloat(8,
					Float.parseFloat(activity.getEstimated_cnsfieldlaborcost()));
			cstmt.setFloat(9, Float.parseFloat(activity
					.getEstimated_contractfieldlaborcost()));
			cstmt.setFloat(10,
					Float.parseFloat(activity.getEstimated_freightcost()));
			cstmt.setFloat(11,
					Float.parseFloat(activity.getEstimated_travelcost()));
			cstmt.setFloat(12,
					Float.parseFloat(activity.getEstimated_totalcost()));
			cstmt.setFloat(13, Float.parseFloat(activity.getExtendedprice()));
			cstmt.setFloat(14, Float.parseFloat(activity.getOverheadcost()));
			cstmt.setFloat(15, Float.parseFloat(activity.getListprice()));
			cstmt.setString(16, activity.getStatus());
			cstmt.setString(17, activity_cost_type);
			cstmt.setInt(18, Integer.parseInt(activity.getActivity_lib_Id()));
			if (activity.getEstimated_start_schedule() != null)
				cstmt.setString(19, activity.getEstimated_start_schedule());
			else
				cstmt.setString(19, "");
			if (activity.getEstimated_complete_schedule() != null)
				cstmt.setString(20, activity.getEstimated_complete_schedule());
			else
				cstmt.setString(20, "");
			cstmt.setInt(21, Integer.parseInt(loginuserid));
			cstmt.setString(22, "A");
			cstmt.setInt(23, 0);
			// if( !( activity.getAddendum_id() != null ||
			// activity.getAddendum_id().equals( "" ) ) )
			cstmt.setString(24, activity.getAddendum_id());
			// else
			// cstmt.setString( 23 , "0" );

			// cstmt.setString( 23 , activity.getAddendum_id() );

			if (fromjobdashboard.equals("inscopejob")) {
				cstmt.setString(25, fromjobdashboard);
			} else {
				cstmt.setString(25, null);
			}

			cstmt.setFloat(26, Float.parseFloat("0.0"));
			cstmt.setString(27, null);
			cstmt.setString(28, null);
			cstmt.registerOutParameter(29, java.sql.Types.INTEGER);
			/*
			 * System.out.println( "QUERY 0  "+Integer.parseInt(
			 * activity.getActivity_Id() )+" , " + ""+Integer.parseInt(
			 * activity.getJob_Id() )+" , '"+activity.getName()+"' , " +
			 * ""+Float.parseFloat( activity.getQuantity()
			 * )+" , '"+activity.getActivitytypecombo()+"' , " +
			 * ""+Float.parseFloat( activity.getEstimated_materialcost() )+" , "
			 * + ""+Float.parseFloat( activity.getEstimated_cnsfieldlaborcost()
			 * )+" , " + ""+Float.parseFloat(
			 * activity.getEstimated_contractfieldlaborcost()
			 * )+" , "+Float.parseFloat( activity.getEstimated_freightcost()
			 * )+" , " + ""+Float.parseFloat( activity.getEstimated_travelcost()
			 * )+" , "+Float.parseFloat( activity.getEstimated_totalcost()
			 * )+" , " + ""+Float.parseFloat( activity.getExtendedprice()
			 * )+" , "+Float.parseFloat( activity.getOverheadcost() )+" , " +
			 * ""+Float.parseFloat( activity.getListprice()
			 * )+" , '"+activity.getStatus()+"' , '"+activity_cost_type+"' , " +
			 * ""+Integer.parseInt( activity.getActivity_lib_Id()
			 * )+" , '' , '' , "+Integer.parseInt ( loginuserid )+" , " +
			 * "'A' , '"
			 * +activity.getAddendum_id()+"' , 'null' ,0 ,'null','null'" );
			 */

			cstmt.execute();
			val = cstmt.getInt(1);
			activityId = cstmt.getInt(29);
		}

		catch (Exception e) {
			logger.error(
					"Addactivity(Activity, String, String, DataSource, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("Addactivity(Activity, String, String, DataSource, String) - Error Occured in Inserting Activity"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"Addactivity(Activity, String, String, DataSource, String) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"Addactivity(Activity, String, String, DataSource, String) - exception ignored",
						sql);
			}

		}

		return val + "|" + activityId;
	}

	public static int Updateactivity(Activity activity,
			String activity_cost_type, String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_activity_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(activity.getActivity_Id()));
			cstmt.setInt(3, Integer.parseInt(activity.getJob_Id()));
			cstmt.setString(4, "");
			cstmt.setFloat(5, Float.parseFloat(activity.getQuantity()));
			cstmt.setString(6, activity.getActivitytypecombo());
			cstmt.setFloat(7,
					Float.parseFloat(activity.getEstimated_materialcost()));
			cstmt.setFloat(8,
					Float.parseFloat(activity.getEstimated_cnsfieldlaborcost()));
			cstmt.setFloat(9, Float.parseFloat(activity
					.getEstimated_contractfieldlaborcost()));
			cstmt.setFloat(10,
					Float.parseFloat(activity.getEstimated_freightcost()));
			cstmt.setFloat(11,
					Float.parseFloat(activity.getEstimated_travelcost()));
			cstmt.setFloat(12,
					Float.parseFloat(activity.getEstimated_totalcost()));
			cstmt.setFloat(13, Float.parseFloat(activity.getExtendedprice()));
			cstmt.setFloat(14, Float.parseFloat(activity.getOverheadcost()));
			cstmt.setFloat(15, Float.parseFloat(activity.getListprice()));
			cstmt.setString(16, activity.getStatus());
			cstmt.setString(17, activity_cost_type);
			cstmt.setInt(18, 0);
			cstmt.setString(19, activity.getEstimated_start_schedule());
			cstmt.setString(20, activity.getEstimated_complete_schedule());
			cstmt.setInt(21, Integer.parseInt(loginuserid));
			cstmt.setString(22, "U");
			cstmt.setString(23, activity.getBidFlag());
			cstmt.setInt(24, 0);
			cstmt.setString(25, null);
			cstmt.setFloat(26,
					Float.parseFloat(activity.getEstimated_cnshqlaborcost()));
			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"Updateactivity(Activity, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("Updateactivity(Activity, String, String, DataSource) - Error Occured in Updating Activity"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"Updateactivity(Activity, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"Updateactivity(Activity, String, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	// Update activity vijay:Start

	public static int updateActivity(ActivityEditBean activityEditBean,
			String activity_cost_type, String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;
		/*
		 * System.out.println("2 " + activityEditForm.getActivity_Id() );
		 * System.out.println("3 " + activityEditForm.getJobId() );
		 * System.out.println("4 " + ""); System.out.println("5 " +
		 * Float.parseFloat( activityEditForm.getQuantity() ) );
		 * System.out.println("6 " + activityEditForm.getActivityType() );
		 * System.out.println("7 " + Float.parseFloat(
		 * activityEditForm.getEstimatedMaterialcost() ) );
		 * System.out.println("8 " + Float.parseFloat(
		 * activityEditForm.getEstimatedCnsfieldhqlaborcost() ) );
		 * System.out.println("9 " + Float.parseFloat(
		 * activityEditForm.getEstimatedContractfieldlaborcost() ) );
		 * System.out.println("10 " + Float.parseFloat(
		 * activityEditForm.getEstimatedFreightcost() ) );
		 * System.out.println("11 " + Float.parseFloat(
		 * activityEditForm.getEstimatedTravelcost() ) );
		 * System.out.println("12 " + Float.parseFloat(
		 * activityEditForm.getEstimatedTotalcost() ) );
		 * System.out.println("13  " + Float.parseFloat(
		 * activityEditForm.getExtendedprice() ) ); System.out.println("14 " +
		 * Float.parseFloat( activityEditForm.getOverheadcost() ) );
		 * System.out.println("15 " + Float.parseFloat(
		 * activityEditForm.getListprice() ) ); System.out.println("16 " +
		 * activityEditForm.getNewStatus() ); System.out.println("17  " +
		 * activity_cost_type ); System.out.println("18 " + "0" );
		 * System.out.println("19 " +
		 * activityEditForm.getEstimatedStartSchedule() );
		 * System.out.println("20 " +
		 * activityEditForm.getEstimatedCompleteSchedule() );
		 * System.out.println("21 " + Integer.parseInt ( loginuserid ) );
		 * System.out.println("22 " + "U" ); System.out.println("23 " + "0" );
		 * System.out.println("24 " + "null" ); System.out.println("25 " +
		 * Float.parseFloat( activityEditForm.getEstimatedCnshqlaborcost() ) );
		 */

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_activity_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(activityEditBean.getActivity_Id()));
			cstmt.setInt(3, Integer.parseInt(activityEditBean.getJobId()));
			cstmt.setString(4, "");
			cstmt.setFloat(5, Float.parseFloat(activityEditBean.getQuantity()));
			cstmt.setString(6, activityEditBean.getActivityType());
			cstmt.setFloat(7, Float.parseFloat(activityEditBean
					.getEstimatedMaterialcost()));
			cstmt.setFloat(8, Float.parseFloat(activityEditBean
					.getEstimatedCnsfieldhqlaborcost()));
			cstmt.setFloat(9, Float.parseFloat(activityEditBean
					.getEstimatedContractfieldlaborcost()));
			cstmt.setFloat(10, Float.parseFloat(activityEditBean
					.getEstimatedFreightcost()));
			cstmt.setFloat(11,
					Float.parseFloat(activityEditBean.getEstimatedTravelcost()));
			cstmt.setFloat(12,
					Float.parseFloat(activityEditBean.getEstimatedTotalcost()));
			cstmt.setFloat(13,
					Float.parseFloat(activityEditBean.getExtendedprice()));
			cstmt.setFloat(14,
					Float.parseFloat(activityEditBean.getOverheadcost()));
			cstmt.setFloat(15,
					Float.parseFloat(activityEditBean.getListprice()));
			cstmt.setString(16, activityEditBean.getNewStatus());
			cstmt.setString(17, activity_cost_type);
			cstmt.setInt(18, 0);
			cstmt.setString(19, activityEditBean.getEstimatedStartSchedule());
			cstmt.setString(20, activityEditBean.getEstimatedCompleteSchedule());
			cstmt.setInt(21, Integer.parseInt(loginuserid));
			cstmt.setString(22, "U");
			cstmt.setInt(23, 0);
			cstmt.setString(24, null);
			cstmt.setFloat(25, Float.parseFloat(activityEditBean
					.getEstimatedCnshqlaborcost()));
			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updateActivity(ActivityEditForm, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateActivity(ActivityEditForm, String, String, DataSource) - Error Occured in Updating Activity"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateActivity(ActivityEditForm, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateActivity(ActivityEditForm, String, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	// Update activity vijay:End

	// Add activity vijay:Start
	public static String addActivity(ActivityEditBean activityEditBean,
			String loginuserid, DataSource ds, String fromjobdashboard) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;
		int activityId = 0;
		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_activity_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(activityEditBean.getActivity_Id()));
			cstmt.setInt(3, Integer.parseInt(activityEditBean.getJobId()));
			cstmt.setString(4, activityEditBean.getActivityName());
			cstmt.setFloat(5, Float.parseFloat(activityEditBean.getQuantity()));
			cstmt.setString(6, activityEditBean.getActivityType());
			cstmt.setFloat(7, Float.parseFloat(activityEditBean
					.getEstimatedMaterialcost()));
			cstmt.setFloat(8, Float.parseFloat(activityEditBean
					.getEstimatedCnsfieldhqlaborcost()));
			cstmt.setFloat(9, Float.parseFloat(activityEditBean
					.getEstimatedContractfieldlaborcost()));
			cstmt.setFloat(10, Float.parseFloat(activityEditBean
					.getEstimatedFreightcost()));
			cstmt.setFloat(11,
					Float.parseFloat(activityEditBean.getEstimatedTravelcost()));
			cstmt.setFloat(12,
					Float.parseFloat(activityEditBean.getEstimatedTotalcost()));
			cstmt.setFloat(13,
					Float.parseFloat(activityEditBean.getExtendedprice()));
			cstmt.setFloat(14,
					Float.parseFloat(activityEditBean.getOverheadcost()));
			cstmt.setFloat(15,
					Float.parseFloat(activityEditBean.getListprice()));
			cstmt.setString(16, activityEditBean.getNewStatus());
			cstmt.setString(17, activityEditBean.getActivity_cost_type());
			cstmt.setInt(18,
					Integer.parseInt(activityEditBean.getActivity_lib_Id()));
			if (activityEditBean.getEstimatedStartSchedule() != null)
				cstmt.setString(19,
						activityEditBean.getEstimatedStartSchedule());
			else
				cstmt.setString(19, "");
			if (activityEditBean.getEstimatedCompleteSchedule() != null)
				cstmt.setString(20,
						activityEditBean.getEstimatedCompleteSchedule());
			else
				cstmt.setString(20, "");
			cstmt.setInt(21, Integer.parseInt(loginuserid));
			cstmt.setString(22, "A");
			cstmt.setString(23, "0");
			// if( !( activity.getAddendum_id() != null ||
			// activity.getAddendum_id().equals( "" ) ) )
			cstmt.setString(24, activityEditBean.getAddendum_id());
			// else
			// cstmt.setString( 23 , "0" );

			// cstmt.setString( 23 , activity.getAddendum_id() );

			if (fromjobdashboard.equals("inscopejob")) {
				cstmt.setString(25, fromjobdashboard);
			} else {
				cstmt.setString(25, null);
			}

			cstmt.setFloat(26, Float.parseFloat("0.0"));
			cstmt.setString(27, null);
			cstmt.setString(28, null);
			cstmt.registerOutParameter(29, java.sql.Types.INTEGER);
			/*
			 * System.out.println( "QUERY 0  "+Integer.parseInt(
			 * activity.getActivity_Id() )+" , " + ""+Integer.parseInt(
			 * activity.getJob_Id() )+" , '"+activity.getName()+"' , " +
			 * ""+Float.parseFloat( activity.getQuantity()
			 * )+" , '"+activity.getActivitytypecombo()+"' , " +
			 * ""+Float.parseFloat( activity.getEstimated_materialcost() )+" , "
			 * + ""+Float.parseFloat( activity.getEstimated_cnsfieldlaborcost()
			 * )+" , " + ""+Float.parseFloat(
			 * activity.getEstimated_contractfieldlaborcost()
			 * )+" , "+Float.parseFloat( activity.getEstimated_freightcost()
			 * )+" , " + ""+Float.parseFloat( activity.getEstimated_travelcost()
			 * )+" , "+Float.parseFloat( activity.getEstimated_totalcost()
			 * )+" , " + ""+Float.parseFloat( activity.getExtendedprice()
			 * )+" , "+Float.parseFloat( activity.getOverheadcost() )+" , " +
			 * ""+Float.parseFloat( activity.getListprice()
			 * )+" , '"+activity.getStatus()+"' , '"+activity_cost_type+"' , " +
			 * ""+Integer.parseInt( activity.getActivity_lib_Id()
			 * )+" , '' , '' , "+Integer.parseInt ( loginuserid )+" , " +
			 * "'A' , '"+activity.getAddendum_id()+"' , 'null' " );
			 */

			cstmt.execute();
			val = cstmt.getInt(1);
			activityId = cstmt.getInt(28);
		}

		catch (Exception e) {
			logger.error(
					"addActivity(ActivityEditForm, String, DataSource, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addActivity(ActivityEditForm, String, DataSource, String) - Error Occured in Inserting Activity"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addActivity(ActivityEditForm, String, DataSource, String) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addActivity(ActivityEditForm, String, DataSource, String) - exception ignored",
						sql);
			}

		}

		return val + "|" + activityId;

	}

	// Add Activity vijay:End
	public static int deleteactivity(String Id, DataSource ds, String mySqlUrl) {
		Connection conn = null;
		// Connection conn1=null;
		Statement stmt = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		int val = -1;
		int mySqlRetVal = -1;
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select dbo.func_get_status(" + Id + ")";
			rs = stmt.executeQuery(sql);

			conn.setAutoCommit(false);
			cstmt = conn
					.prepareCall("{?=call dbo.lm_activity_delete_01( ?  ) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(Id));

			cstmt.execute();
			val = cstmt.getInt(1);

			if (rs.next()) {
			} else {
			}
			if (val == 0) {
				if (rs.getString(1).equals("I")) {
					mySqlRetVal = deleteMySqlActivity(Id, mySqlUrl);
					if (mySqlRetVal != 9600) {
						conn.commit();
					} else {
						val = mySqlRetVal;
						conn.rollback();
					}
				} else {
					conn.commit();
				}
			} else {
				conn.rollback();
			}
		}

		catch (Exception e) {
			logger.error("deleteactivity(String, DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteactivity(String, DataSource, String) - Exception in DELETING Activity"
						+ e);
			}
			logger.error("deleteactivity(String, DataSource, String)", e);
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
				DBUtil.close(stmt);
			} catch (SQLException sqle) {
				logger.error("deleteactivity(String, DataSource, String)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteactivity(String, DataSource, String) - Exception in DELETING Activity"
							+ sqle);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error("deleteactivity(String, DataSource, String)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteactivity(String, DataSource, String) - Exception in DELETING Activity"
							+ sqle);
				}
			}

		}
		return val;
	}

	public static int deleteMySqlActivity(String Id, String mySqlUrl) {
		int deletemysqlflag = 9600;
		Connection mySqlConn = null;
		ResultSet mySqlRs = null;
		Statement mySqlStmt = null;
		String MySqlStr = "";
		String MySqlDelStr = "";
		int countDeletedValues = 0;
		int countDeletedValuesForJobActivity = 0;
		try {
			mySqlConn = MySqlConnection.getMySqlConnection(mySqlUrl);
			if (mySqlConn != null) {
				mySqlStmt = mySqlConn.createStatement();
				mySqlConn.setAutoCommit(false);
				MySqlStr = "select * from il_default_job_addendum_activities where il_ac_lm_at_id="
						+ Id;
				mySqlRs = mySqlStmt.executeQuery(MySqlStr);
				if (mySqlRs.next()) {
					MySqlDelStr = "delete  from il_default_job_addendum_activities where il_ac_lm_at_id="
							+ Id;
					countDeletedValues = mySqlStmt.executeUpdate(MySqlDelStr);
					deletemysqlflag = countDeletedValues;
					if (countDeletedValues != 1 && countDeletedValues != 0) {
						mySqlConn.rollback();
						deletemysqlflag = 9600;
					}
				} else {
					MySqlDelStr = "delete  from il_activity_integration where il_ai_lm_at_id="
							+ Id;
					countDeletedValuesForJobActivity = mySqlStmt
							.executeUpdate(MySqlDelStr);
					deletemysqlflag = countDeletedValuesForJobActivity;
					if (countDeletedValuesForJobActivity != 1
							&& countDeletedValuesForJobActivity != 0) {
						mySqlConn.rollback();
						deletemysqlflag = 9600;
					}
				}
				if (deletemysqlflag != 9600) {
					mySqlConn.commit();
				} else {
					mySqlConn.rollback();
				}
			} else {
				if (logger.isDebugEnabled()) {
					logger.debug("deleteMySqlActivity(String, String) - In connection fail block");
				}
				deletemysqlflag = 9600;
			}
		} catch (Exception e) {
			logger.error("deleteMySqlActivity(String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteMySqlActivity(String, String) - Error in establishing connection to MySql"
						+ e);
			}
		} finally {
			closeDbResource(mySqlStmt);
			closeDbResource(mySqlRs);
			closeDbResource(mySqlConn);
		}
		return deletemysqlflag;
	}

	private static void closeDbResource(ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("closeDbResource(ResultSet)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("closeDbResource(ResultSet) - " + e);
			}
		}
	}

	private static void closeDbResource(Statement stmt) {
		try {
			if (stmt != null) {
				stmt.close();
			}
		} catch (SQLException e) {
			logger.error("closeDbResource(Statement)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("closeDbResource(Statement) - " + e);
			}
		}
	}

	private static void closeDbResource(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			logger.error("closeDbResource(Connection)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("closeDbResource(Connection) - " + e);
			}
		}
	}

	public static boolean Checkdefaultjoboverhead(DataSource ds, String Job_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		boolean checkforoverheadactivity = false;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select * from dbo.func_lp_checkdefaultjoboverhead('"
					+ Job_Id + "')";
			rs = stmt.executeQuery(sql);

			if (rs.next())
				checkforoverheadactivity = true;
		}

		catch (Exception e) {
			logger.error("Checkdefaultjoboverhead(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("Checkdefaultjoboverhead(DataSource, String) - Error Occured in retrieving checkdefaultjoboverhead"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"Checkdefaultjoboverhead(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"Checkdefaultjoboverhead(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"Checkdefaultjoboverhead(DataSource, String) - exception ignored",
						s);
			}

		}
		return checkforoverheadactivity;
	}

	public static String getAppendixname(DataSource ds, String Job_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		String appendixid = "";

		String appendixname = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_js_pr_id from lm_job where lm_js_id=" + Job_Id;

			rs = stmt.executeQuery(sql);

			if (rs.next())
				appendixid = rs.getString("lm_js_pr_id");

			sql = "select dbo.func_cc_name('" + appendixid + "', 'Appendix' )";
			rs = stmt.executeQuery(sql);
			if (rs.next())
				appendixname = rs.getString(1);

		}

		catch (Exception e) {
			logger.error("getAppendixname(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixname(DataSource, String) - Error Occured in retrieving appendixname"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getAppendixname(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixname(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixname(DataSource, String) - exception ignored",
						s);
			}

		}
		return appendixname;
	}

	public static String getMsaname(DataSource ds, String MSA_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String msaname = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select dbo.func_cc_name('" + MSA_Id + "', 'MSA' )";

			rs = stmt.executeQuery(sql);

			if (rs.next())
				msaname = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getMsaname(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsaname(DataSource, String) - Error Occured in retrieving msaname"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getMsaname(DataSource, String) - exception ignored", s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getMsaname(DataSource, String) - exception ignored", s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getMsaname(DataSource, String) - exception ignored", s);
			}

		}
		return msaname;
	}

	public static String getJobid(DataSource ds, String Activity_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String jobid = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_at_js_id from lm_activity where lm_at_id="
					+ Activity_Id;
			rs = stmt.executeQuery(sql);

			if (rs.next())
				jobid = rs.getString("lm_at_js_id");
		}

		catch (Exception e) {
			logger.error("getJobid(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobid(DataSource, String) - Error Occured in retrieving jobid"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn("getJobid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn("getJobid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn("getJobid(DataSource, String) - exception ignored",
						s);
			}

		}
		return jobid;
	}

	public static String getActivityname(String Activity_Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String activityname = "";
		java.util.Date dateIn = new java.util.Date();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_at_title = isnull( lm_at_title , '' ) from lm_activity where lm_at_id="
					+ Activity_Id;

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				activityname = rs.getString("lm_at_title");

			}
		}

		catch (Exception e) {
			logger.error("getActivityname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityname(String, DataSource) - Exception in retrieving Activity name"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivityname(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityname(String, DataSource) - Exception in retrieving Activity name"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivityname(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityname(String, DataSource) - Exception in retrieving Activity name"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivityname(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityname(String, DataSource) - Exception in retrieving Activity name"
							+ sql);
				}
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("getActivityname(String, DataSource) - **** getActivityname :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return activityname;
	}

	public static String Checkaddendumfromjobdashboard(DataSource ds,
			String Activity_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String checkaddendumfromjoblevel = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_at_addendum_flag = isnull( lm_at_addendum_flag , '' ) from lm_activity where lm_at_id="
					+ Activity_Id;
			rs = stmt.executeQuery(sql);

			if (rs.next())
				checkaddendumfromjoblevel = rs.getString("lm_at_addendum_flag");
		}

		catch (Exception e) {
			logger.error("Checkaddendumfromjobdashboard(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("Checkaddendumfromjobdashboard(DataSource, String) - Error Occured in retrieving Checkaddendumfromjoblevel"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"Checkaddendumfromjobdashboard(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"Checkaddendumfromjobdashboard(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"Checkaddendumfromjobdashboard(DataSource, String) - exception ignored",
						s);
			}

		}
		return checkaddendumfromjoblevel;
	}

	public static String getjobscheduledate(DataSource ds, String Job_Id,
			String dateselect) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String jobscheduledate = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (dateselect.equals("start")) {
				sql = "select lm_js_planned_start_date = isnull( replace( convert( varchar( 10 ) , lm_js_planned_start_date , 101 ) , '01/01/1900' , '' ) , '' ) "
						+ "from lm_job where lm_js_id = " + Job_Id;
			} else
				sql = "select lm_js_planned_end_date = isnull( replace( convert( varchar( 10 ) , lm_js_planned_end_date , 101 ) , '01/01/1900' , '' ) , '' ) "
						+ "from lm_job where lm_js_id = " + Job_Id;

			rs = stmt.executeQuery(sql);

			if (rs.next())
				jobscheduledate = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getjobscheduledate(DataSource, String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getjobscheduledate(DataSource, String, String) - Error Occured in retrieving jobscheduledate"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getjobscheduledate(DataSource, String, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getjobscheduledate(DataSource, String, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getjobscheduledate(DataSource, String, String) - exception ignored",
						s);
			}

		}
		return jobscheduledate;
	}

	/* customer labor rates */

	public static int checkAppendixname(String appendix_id, String Jobid,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int checkcategory = 0;
		String s = "";
		java.util.Date dateIn = new java.util.Date();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			s = "select dbo.func_lm_check_netmedx_appendix(" + appendix_id
					+ "," + Jobid + ")";
			if (logger.isDebugEnabled()) {
				logger.debug("checkAppendixname(String, String, DataSource) - "
						+ s);
			}
			rs = stmt.executeQuery(s);
			if (rs.next()) {
				checkcategory = rs.getInt(1);
			}
		} catch (Exception e) {
			logger.error("checkAppendixname(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkAppendixname(String, String, DataSource) - Exception caught in checking category name"
						+ e);
			}
		} finally {
			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"checkAppendixname(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"checkAppendixname(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"checkAppendixname(String, String, DataSource) - exception ignored",
						sql);
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("checkAppendixname(String, String, DataSource) - **** checkAppendixname :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return checkcategory;
	}

	public static String getAppendixid(DataSource ds, String Job_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String appendixid = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_js_pr_id from lm_job where lm_js_id=" + Job_Id;
			rs = stmt.executeQuery(sql);

			if (rs.next())
				appendixid = rs.getString("lm_js_pr_id");
		}

		catch (Exception e) {
			logger.error("getAppendixid(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixid(DataSource, String) - Error Occured in retrieving Appendixid"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getAppendixid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixid(DataSource, String) - exception ignored",
						s);
			}

		}
		return appendixid;
	}

	/* customer labor rates */

	public static String getDefaultproformamargin(String type, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String margin = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select dbo.func_cc_default_margin( '" + type + "' )";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				margin = rs.getString(1);

			}
		}

		catch (Exception e) {
			logger.error("getDefaultproformamargin(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDefaultproformamargin(String, DataSource) - Exception in retrieving margin"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getDefaultproformamargin(String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getDefaultproformamargin(String, DataSource) - Exception in retrieving margin"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getDefaultproformamargin(String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getDefaultproformamargin(String, DataSource) - Exception in retrieving margin"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getDefaultproformamargin(String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getDefaultproformamargin(String, DataSource) - Exception in retrieving margin"
							+ sql);
				}
			}

		}
		return margin;
	}

	public static NameId getIdName(String Id, String type, String level,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		NameId idname = null;
		java.util.Date dateIn = new java.util.Date();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select * from dbo.func_cc_id_name( " + Integer.parseInt(Id)
					+ " , '" + type + "' , '" + level + "' )";
			if (logger.isDebugEnabled()) {
				logger.debug("getIdName(String, String, String, DataSource) - "
						+ sql);
			}
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				idname = new NameId();
				idname.setId(rs.getString(1));
				idname.setName(rs.getString(2));
			}
		}

		catch (Exception e) {
			logger.error("getIdName(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getIdName(String, String, String, DataSource) - Error Occured in retrieving idname"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getIdName(String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getIdName(String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getIdName(String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("getIdName(String, String, String, DataSource) - **** getIdName :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return idname;
	}

	/**
	 * Method is used to control the editable compopnents on the Manage Activity
	 * Page.
	 * 
	 * @param jobId
	 * @param ds
	 * @return ActivityGridControl
	 */
	public static ActivityGridControl getActivityGridControl(String jobId,
			DataSource ds) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		ActivityGridControl activitygridconrol = null;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{call lm_activity_grid_manage_01 ( ? ) }");
			cstmt.setString(1, jobId);
			rs = cstmt.executeQuery();
			if (rs.next()) {
				activitygridconrol = new ActivityGridControl();
				activitygridconrol.setActivityModuleFlag(rs
						.getString("moduleflag"));
				activitygridconrol.setActivityAddFlag(rs.getString("addflag"));
				activitygridconrol.setActivityDeleteFlag(rs
						.getString("deleteflag"));
				activitygridconrol.setActivityQuantityFlag(rs
						.getString("quantityflag"));
				activitygridconrol
						.setActivityTypeFlag(rs.getString("typeflag"));
				activitygridconrol.setActivityStatusFlag(rs
						.getString("statusflag"));
				activitygridconrol.setActivityBidFlag(rs.getString("bidflag"));
			}
		} catch (Exception e) {
			logger.error("getActivityGridControl(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityGridControl(String, DataSource) - Error occurred while retrieving the activity grid controls");
			}
			logger.error("getActivityGridControl(String, DataSource)", e);
		} finally {
			;
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return activitygridconrol;
	}

	/**
	 * This method return flag of resources which are editable or not.
	 * 
	 * @param activityId
	 *            -- Activity Id
	 * @param ds
	 *            -- Database Soure
	 * @return isEditable -- True/False
	 */
	public static String getEditableFlag(String activityId, DataSource ds) {
		/*
		 * Flags are false - Not Editable true - Editable
		 */
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String editableResource = "true";
		java.util.Date dateIn = new java.util.Date();
		try {
			conn = ds.getConnection();
			String sql = "select dbo.func_flag_for_resource_edit(?)"; // - 1
																		// parameter
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, activityId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				if (rs.getString(1).equals("false")) {
					editableResource = "false";
				}
			} // - end of if
		} // - end of try
		catch (Exception e) {
			logger.error("getEditableFlag(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEditableFlag(String, DataSource) - Exception in com.mind.dao.PM.Activitydao.getEditableFlag() "
						+ e);
			}
		} // - End of catch
		finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}// - End of finally
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("getEditableFlag(String, DataSource) - **** getEditableFlag :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return editableResource;
	} // - End of getEditableFlag() method.

	/**
	 * @param activityId
	 * @param ds
	 */
	public static void setOOSFlag(String activityId, DataSource ds) {

		Connection conn = null;
		PreparedStatement pStmt = null;

		try {
			conn = ds.getConnection();
			String sql = "update lm_activity set lm_at_oos_flag='Y' where lm_at_id= ?"; // -
																						// 1
																						// parameter
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, activityId);
			pStmt.executeUpdate();
		} // - end of try
		catch (Exception e) {
			logger.error("setOOSFlag(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setOOSFlag(String, DataSource) - Exception in com.mind.dao.PM.Activitydao.setOOSFlag() "
						+ e);
			}
		} // - End of catch
		finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}// - End of finally
	} // - End of getEditableFlag() method.
}
