package com.mind.dao.PM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.pm.SiteSearchBean;
import com.mind.common.LabelValue;

public class SiteSearchdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SiteSearchdao.class);

	public static ArrayList<SiteSearchBean> getSearchsitedetails(int msaid,
			String Sitename, String Siteno, String lo_ot_id, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList<SiteSearchBean> valuelist = new ArrayList<SiteSearchBean>();

		/*
		 * System.out.println("2:::::"+Sitename);
		 * System.out.println("3:::::"+Siteno);
		 * System.out.println("4:::::"+msaid);
		 * System.out.println("5:::::"+lo_ot_id);
		 */
		try {
			conn = ds.getConnection();

			cstmt = conn.prepareCall("{?=call lp_site_search_list(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setString(2, Sitename);
			cstmt.setString(3, Siteno);
			cstmt.setInt(4, msaid);
			cstmt.setString(5, lo_ot_id);

			rs = cstmt.executeQuery();
			while (rs.next()) {
				SiteSearchBean siteForm = new SiteSearchBean();

				siteForm.setSitelistid(rs.getString("lp_sl_id"));

				siteForm.setMsaid(rs.getString("lp_md_mm_id"));

				// siteForm.setSiteid(rs.getString("lp_si_id"));
				// System.out.println("-----xxx2"+rs.getString("lp_si_name"));
				siteForm.setSitename(rs.getString("lp_si_name"));
				// System.out.println("-----xxx3"+rs.getString("lp_si_number"));
				siteForm.setSitenumber(rs.getString("lp_si_number"));

				siteForm.setSitelocalityfactor(rs
						.getString("lp_si_locality_factor"));
				siteForm.setUnionsite(rs.getString("lp_si_union_site"));
				siteForm.setSitedesignator(rs.getString("lp_si_designator"));
				siteForm.setSiteworklocation(rs
						.getString("lp_si_work_location"));
				// System.out.println("-----xxx4"+rs.getString("lp_si_address"));
				siteForm.setSiteaddress(rs.getString("lp_si_address"));
				// System.out.println("-----xxx5"+rs.getString("lp_si_city"));
				siteForm.setSitecity(rs.getString("lp_si_city"));
				// System.out.println("-----xxx6"+rs.getString("lp_si_state"));
				siteForm.setSitestate(rs.getString("lp_si_state"));
				siteForm.setSitestatedesc(rs.getString("lp_si_state_desc"));

				siteForm.setSitezipcode(rs.getString("lp_si_zip_code"));
				siteForm.setSitesecphone(rs.getString("lp_si_sec_phone_no"));
				siteForm.setSitephone(rs.getString("lp_si_phone_no"));
				// System.out.println("-----xxx7"+rs.getString("lp_si_country"));
				siteForm.setSitecountry(rs.getString("lp_si_country"));
				siteForm.setSitedirection(rs.getString("lp_si_directions"));
				siteForm.setSitepripoc(rs.getString("lp_si_pri_poc"));
				siteForm.setSitesecpoc(rs.getString("lp_si_sec_poc"));
				siteForm.setSitepriemail(rs.getString("lp_si_pri_email_id"));
				siteForm.setSitesecemail(rs.getString("lp_si_sec_email_id"));
				siteForm.setSitenotes(rs.getString("lp_si_notes"));
				siteForm.setInstallerpoc(rs.getString("lp_si_installer_poc"));
				siteForm.setSitepoc(rs.getString("lp_si_site_poc"));
				siteForm.setSitelatdeg(rs.getString("lp_si_lat_deg"));
				siteForm.setSitelatmin(rs.getString("lp_si_lat_min"));
				siteForm.setSitelatdirection(rs
						.getString("lp_si_lat_direction"));
				siteForm.setSitelondeg(rs.getString("lp_si_lon_deg"));
				siteForm.setSitelonmin(rs.getString("lp_si_lon_min"));
				siteForm.setSitelondirection(rs
						.getString("lp_si_lon_direction"));
				siteForm.setSitebrand(rs.getString("lp_si_group"));
				siteForm.setSitestatus(rs.getString("lp_si_status"));
				siteForm.setSiteendcustomer(rs.getString("lp_si_end_customer"));
				siteForm.setSitetimezone(rs.getString("lp_si_time_zone"));
				siteForm.setSiteregion(rs.getString("lp_si_region_id"));
				siteForm.setSitecategory(rs.getString("lp_si_category_id"));

				// System.out.println("-----xxx8"+rs.getString("lp_si_site_poc"));

				valuelist.add(siteForm);
			}
			// System.out.println("after proc::::::::::::::::");
			cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("Sitename:::::::2:::::" + Sitename);
			logger.error("Siteno:::::::::3:::::" + Siteno);
			logger.error("msaid::::::::::4:::::" + msaid);
			logger.error("lo_ot_id:::::::5:::::" + lo_ot_id);
			logger.error(
					"getSearchsitedetails(int, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSearchsitedetails(int, String, String, String, DataSource) - Error occured during searching"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getSearchsitedetails(int, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getSearchsitedetails(int, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getSearchsitedetails(int, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static ArrayList<SiteSearchBean> getSearchsitedetails(int msaId,
			String siteName, String siteNo, String lo_ot_id, String siteCity,
			String siteState, String siteBrand, String siteEndCustomer,
			DataSource ds, int latestSiteId) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList<SiteSearchBean> valuelist = new ArrayList<SiteSearchBean>();

		if (siteName != null && !siteName.equals(""))
			siteName = siteName.replaceAll("\'", "\'\'");

		if (siteNo != null && !siteNo.equals(""))
			siteNo = siteNo.replaceAll("\'", "\'\'");

		if (siteBrand != null && !siteBrand.equals(""))
			siteBrand = siteBrand.replaceAll("\'", "\'\'");

		if (siteEndCustomer != null && !siteEndCustomer.equals(""))
			siteEndCustomer = siteEndCustomer.replaceAll("\'", "\'\'");

		/*
		 * System.out.println("enter function");
		 * System.out.println("2::::::::"+siteName);
		 * System.out.println("3::::::::"+siteNo);
		 * System.out.println("4::::::::"+msaId);
		 * System.out.println("5::::::::"+lo_ot_id);
		 * System.out.println("6::::::::"+siteCity);
		 * System.out.println("7::::::::"+siteState);
		 * System.out.println("8::::::::"+siteBrand);
		 * System.out.println("9::::::::"+siteEndCustomer);
		 */

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call lp_site_search_list(?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setString(2, siteName);
			cstmt.setString(3, siteNo);
			cstmt.setInt(4, msaId);
			cstmt.setString(5, lo_ot_id);
			cstmt.setString(6, siteCity);
			cstmt.setString(7, siteState);
			cstmt.setString(8, siteBrand);
			cstmt.setString(9, siteEndCustomer);
			cstmt.setInt(10, latestSiteId);
			int i = 0;
			rs = cstmt.executeQuery();
			while (rs.next()) {
				SiteSearchBean siteForm = new SiteSearchBean();

				siteForm.setSitelistid(rs.getString("lp_sl_id"));
				siteForm.setMsaid(rs.getString("lp_md_mm_id"));
				siteForm.setSitename(rs.getString("lp_si_name"));
				siteForm.setSitenumber(rs.getString("lp_si_number"));
				siteForm.setSitelocalityfactor(rs
						.getString("lp_si_locality_factor"));
				siteForm.setUnionsite(rs.getString("lp_si_union_site"));
				siteForm.setSitedesignator(rs.getString("lp_si_designator"));
				siteForm.setSiteworklocation(rs
						.getString("lp_si_work_location"));
				siteForm.setSiteaddress(rs.getString("lp_si_address"));
				siteForm.setSitecity(rs.getString("lp_si_city"));
				siteForm.setSitestate(rs.getString("lp_si_state"));
				siteForm.setSitestatedesc(rs.getString("lp_si_state_desc"));
				siteForm.setSitezipcode(rs.getString("lp_si_zip_code"));
				siteForm.setSitesecphone(rs.getString("lp_si_sec_phone_no"));
				siteForm.setSitephone(rs.getString("lp_si_phone_no"));
				siteForm.setSitecountry(rs.getString("lp_si_country"));
				siteForm.setSitedirection(rs.getString("lp_si_directions"));
				siteForm.setSitepripoc(rs.getString("lp_si_pri_poc"));
				siteForm.setSitesecpoc(rs.getString("lp_si_sec_poc"));
				siteForm.setSitepriemail(rs.getString("lp_si_pri_email_id"));
				siteForm.setSitesecemail(rs.getString("lp_si_sec_email_id"));
				siteForm.setSitenotes(rs.getString("lp_si_notes"));
				siteForm.setInstallerpoc(rs.getString("lp_si_installer_poc"));
				siteForm.setSitepoc(rs.getString("lp_si_site_poc"));
				siteForm.setSitelatdeg(rs.getString("lp_si_lat_deg"));
				siteForm.setSitelatmin(rs.getString("lp_si_lat_min"));
				siteForm.setSitelatdirection(rs
						.getString("lp_si_lat_direction"));
				siteForm.setSitelondeg(rs.getString("lp_si_lon_deg"));
				siteForm.setSitelonmin(rs.getString("lp_si_lon_min"));
				siteForm.setSitelondirection(rs
						.getString("lp_si_lon_direction"));
				siteForm.setSitebrand(rs.getString("lp_si_group"));
				siteForm.setSitestatus(rs.getString("lp_si_status"));
				siteForm.setSiteendcustomer(rs.getString("lp_si_end_customer"));
				siteForm.setSitetimezone(rs.getString("lp_si_time_zone"));
				siteForm.setSiteregion(rs.getString("lp_si_region_id"));
				siteForm.setSitecategory(rs.getString("lp_si_category_id"));
				valuelist.add(i, siteForm);
				i++;
			}
			cstmt.getInt(1);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(
					"getSearchsitedetails(int, String, String, String, String, String, String, String, DataSource, int)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSearchsitedetails(int, String, String, String, String, String, String, String, DataSource, int) - Error occured during searching"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getSearchsitedetails(int, String, String, String, String, String, String, String, DataSource, int) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getSearchsitedetails(int, String, String, String, String, String, String, String, DataSource, int) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getSearchsitedetails(int, String, String, String, String, String, String, String, DataSource, int) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	/**
	 * For View All Sites
	 * 
	 * @param city_name
	 * @param State_name
	 * @param msaId
	 * @param appendixId
	 * @param ds
	 * @return
	 */

	public static ArrayList<LabelValue> getSiteDetailList(String msaId,
			String appendixId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList<LabelValue> sitedetaillist = new ArrayList<LabelValue>();

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lp_site_search_01(?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, "0");
			cstmt.setString(3, "0");
			cstmt.setString(4, msaId);
			cstmt.setString(5, appendixId);

			rs = cstmt.executeQuery();

			while (rs.next()) {
				sitedetaillist.add(new com.mind.common.LabelValue(rs
						.getString("lp_si_name")
						+ ", "
						+ rs.getString("lp_si_number")
						+ ", "
						+ rs.getString("lp_si_city")
						+ ", "
						+ rs.getString("lp_si_state"), rs.getString(1)));
			}
		}

		catch (Exception e) {
			logger.error("getSiteDetailList(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteDetailList(String, String, DataSource) - Error occured  "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getSiteDetailList(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getSiteDetailList(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getSiteDetailList(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return sitedetaillist;
	}

	public static int getMsaid(DataSource ds, String Appendix_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		int msaid = 0;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lx_pr_mm_id from lx_appendix_main where lx_pr_id="
					+ Appendix_Id;

			rs = stmt.executeQuery(sql);

			if (rs.next())
				msaid = rs.getInt("lx_pr_mm_id");

		}

		catch (Exception e) {
			logger.error("getMsaid(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsaid(DataSource, String) - Error Occured in retrieving appendixid"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn("getMsaid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn("getMsaid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn("getMsaid(DataSource, String) - exception ignored",
						s);
			}

		}
		return msaid;
	}

	public static ArrayList<LabelValue> getSiteStateList(String countryId,
			String msaId, int count, DataSource ds) {
		ArrayList<LabelValue> list = new ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		if (count == 0)
			list.add(new com.mind.common.LabelValue("--Select--", "0"));
		// list.add( new com.mind.common.LabelValue( "Select" , "0" ) );

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select distinct cp_state_id from func_get_site_states('"
							+ countryId + "', " + msaId + ")");
			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("cp_state_id"), rs.getString("cp_state_id")));
			}
		} catch (Exception e) {
			logger.error("getSiteStateList(String, String, int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteStateList(String, String, int, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getSiteStateList(String, String, int, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getSiteStateList(String, String, int, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getSiteStateList(String, String, int, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

}
