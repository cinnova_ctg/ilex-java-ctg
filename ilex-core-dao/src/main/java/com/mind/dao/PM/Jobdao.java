/* This class defines methods for Job Creation , Updation , Deletion
 * Method Name: getJobList
 * Method Argument: 3 Argument DataSource ds , String appendix_id , String querystring
 * Method Description: Retrieve all the jobs for the passed Appendix Id
 * retrun type:ArrayList
 * see setJob function

 * Method Name: addJob
 * Method Argument: 2 Argument Job job,  DataSource ds
 * Method Description: Create a Job
 * retrun type:void

 * Method Name: updataJobList
 * Method Argument: 2 Argument Job job , DataSource ds
 * Method Description: Update a Job
 * retrun type:void

 * Method Name: getJob
 * Method Argument: 3 Argument String appendix_id , String job_id , DataSource ds
 * Method Description: Retrieve a job.
 * retrun type:Job
 * see   setJob function
 */
package com.mind.dao.PM;

import com.mind.bean.pm.Activity;
import com.mind.bean.pm.JobEditBean;
import com.mind.bean.pm.SiteBean;
import com.mind.common.Decimalroundup;
import com.mind.fw.core.dao.util.DBUtil;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

public class Jobdao {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(Jobdao.class);

    public static String[] getJobStatusAndType(String appendix_Id,
            String job_Id, String flag, String addendumflag, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String[] statusAndType = new String[2];

        String lx_pd_id = "";

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "select * from dbo.func_lp_job_tab('" + appendix_Id
                    + "','" + job_Id + "' , '" + flag + "' , '" + addendumflag
                    + "')";
            if (logger.isDebugEnabled()) {
                logger.debug("getJobStatusAndType(String, String, String, String, DataSource) - sql= "
                        + sql);
            }
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                statusAndType[0] = rs.getString("lm_js_status");
                statusAndType[1] = rs.getString("lm_js_type");
            }
        } catch (Exception e) {
            logger.error(
                    "getJobStatusAndType(String, String, String, String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("getJobStatusAndType(String, String, String, String, DataSource) - Exception in retrieving job status and type"
                        + e);
            }
        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getJobStatusAndType(String, String, String, String, DataSource)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getJobStatusAndType(String, String, String, String, DataSource) - Exception in retrieving job status and type"
                            + sql);
                }
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getJobStatusAndType(String, String, String, String, DataSource)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getJobStatusAndType(String, String, String, String, DataSource) - Exception in retrieving job status and type"
                            + sql);
                }
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getJobStatusAndType(String, String, String, String, DataSource)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getJobStatusAndType(String, String, String, String, DataSource) - Exception in retrieving job status and type"
                            + sql);
                }
            }

        }
        return statusAndType;
    }

    public static String[] getProjectJobStatusAndType(String job_Id,
            DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String[] statusAndType = new String[2];

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "select lm_js_prj_status,lm_js_type from lm_job where lm_js_id = "
                    + job_Id;

            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                statusAndType[0] = rs.getString("lm_js_prj_status");
                statusAndType[1] = rs.getString("lm_js_type");
            }
        } catch (Exception e) {
            logger.error("getProjectJobStatusAndType(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getProjectJobStatusAndType(String, DataSource) - Exception in retrieving job status and type"
                        + e);
            }
        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException sql) {
                logger.error("getProjectJobStatusAndType(String, DataSource)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getProjectJobStatusAndType(String, DataSource) - Exception in retrieving job status and type"
                            + sql);
                }
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sql) {
                logger.error("getProjectJobStatusAndType(String, DataSource)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getProjectJobStatusAndType(String, DataSource) - Exception in retrieving job status and type"
                            + sql);
                }
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.error("getProjectJobStatusAndType(String, DataSource)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getProjectJobStatusAndType(String, DataSource) - Exception in retrieving job status and type"
                            + sql);
                }
            }

        }
        return statusAndType;
    }

    public static long[] editJob(JobEditBean job, String loginuserid, DataSource ds) throws Exception {
        Connection conn = null;
        CallableStatement cstmt = null;
        long[] jobVal = new long[2];
        int val = -1;

        String jobEstimatedStart = job.getEstimatedStartSchedule() + " "
                + job.getStartdatehh() + ":" + job.getStartdatemm() + " "
                + job.getStartoptionname();
        job.setEstimatedStartSchedule(jobEstimatedStart);

        String jobEstimatedComplete = job.getEstimatedCompleteSchedule() + " "
                + job.getCompletedatehh() + ":" + job.getCompletedatemm() + " "
                + job.getCompleteoptionname();
        job.setEstimatedCompleteSchedule(jobEstimatedComplete);

        if (job.getActionAddUpdate().equals("U")) {
            job.setStatus("");
            job.setJobType("");
        }
        /*
         * System.out.println("2 "+ Integer.parseInt(job.getJobId()));
         * System.out.println("3 "+ Integer.parseInt( job.getAppendixId() ) );
         * System.out.println("4 "+ job.getJobName() ); System.out.println("5 "+
         * job.getJobType() ); System.out.println("6 "+ job.getStatus() );
         * System.out.println("7 "+ job.getEstimatedStartSchedule() );
         * System.out.println("8 "+ job.getEstimatedCompleteSchedule() );
         * System.out.println("9 "+ Integer.parseInt( loginuserid ) );
         * System.out.println("10 "+ job.getActionAddUpdate() );
         * System.out.println("11 "+ job.getCustRef() );
         * System.out.println("12 "+ 0 ); System.out.println("13 "+ "F" );
         */

        try {
            conn = ds.getConnection();
            cstmt = conn.prepareCall("{?=call dbo.lx_job_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(14, java.sql.Types.INTEGER);
            cstmt.setInt(2, Integer.parseInt(job.getJobId()));
            cstmt.setInt(3, Integer.parseInt(job.getAppendixId()));
            cstmt.setString(4, job.getJobName().trim());
            cstmt.setString(5, job.getJobType());
            cstmt.setString(6, job.getStatus());
            cstmt.setString(7, job.getEstimatedStartSchedule());
            cstmt.setString(8, job.getEstimatedCompleteSchedule());
            cstmt.setInt(9, Integer.parseInt(loginuserid));
            cstmt.setString(10, job.getActionAddUpdate());
            cstmt.setString(11, job.getCustRef());
            cstmt.setInt(12, 0);
            cstmt.setString(13, "F");
            // cstmt.registerOutParameter( 13 , java.sql.Types.INTEGER );
            cstmt.execute();
            jobVal[0] = cstmt.getInt(1);
            jobVal[1] = cstmt.getInt(14);
            addUpdateRequestorEmail(jobVal[1], job.getRequestorIds(), job.getAdditionalRecipientIds(), ds);

        } catch (Exception e) {
            logger.error("editJob(JobEditForm, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("editJob(JobEditForm, String, DataSource) - Error Occured in Adding or Updating Job"
                        + e);
            }
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "editJob(JobEditForm, String, DataSource) - exception ignored",
                        sql);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "editJob(JobEditForm, String, DataSource) - exception ignored",
                        sql);
            }

        }
        return jobVal;
    }

    public static void getJobDetailForEdit(JobEditBean jobEditForm,
            String appendix_Id, String job_Id, String flag,
            String addendumflag, DataSource ds) {
        /*
         * System.out.println("IN New method for job edit page ::::: ");
         *
         * System.out.println(" ::::::::::::::::::;;;::::: ");
         *
         * System.out.println("Appendix ID is ::: " + appendix_Id );
         * System.out.println("Job Id ::::: " + job_Id);
         * System.out.println("Flag ::::: " + flag);
         * System.out.println("Addendumflag ::::: " + addendumflag);
         *
         * System.out.println(" ::::::::::::::::::;;;::::: ");
         */

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        DecimalFormat dfcurmargin = new DecimalFormat("0.00");

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            String sql = "select * from dbo.func_lp_job_tab('" + appendix_Id
                    + "','" + job_Id + "' , '" + flag + "' , '" + addendumflag
                    + "')";
            // System.out.println("sql is :::::" + sql);
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                jobEditForm.setJobName(rs.getString("jobname"));
                jobEditForm.setSiteName(rs.getString("lm_si_name"));
                jobEditForm.setLocalityUplift(rs
                        .getString("lm_si_locality_factor"));
                jobEditForm.setUnion(rs.getString("lm_si_union_site"));
                jobEditForm.setStatus(rs.getString("lm_js_status"));
                jobEditForm
                        .setEstimatedMaterialCost(Decimalroundup
                                .twodecimalplaces(
                                        rs.getFloat("lx_ce_material_cost"), 2));
                jobEditForm.setEstimatedCnsFieldLaborCost(Decimalroundup
                        .twodecimalplaces(
                                rs.getFloat("lx_ce_cns_field_labor_cost"), 2));
                jobEditForm.setEstimatedContractFieldLaborCost(Decimalroundup
                        .twodecimalplaces(
                                rs.getFloat("lx_ce_ctr_field_labor_cost"), 2));
                jobEditForm
                        .setEstimatedFreightCost(Decimalroundup
                                .twodecimalplaces(
                                        rs.getFloat("lx_ce_frieght_cost"), 2));
                jobEditForm.setEstimatedTravelCost(Decimalroundup
                        .twodecimalplaces(rs.getFloat("lx_ce_travel_cost"), 2));
                jobEditForm.setEstimatedTotalCost(Decimalroundup
                        .twodecimalplaces(rs.getFloat("lx_ce_total_cost"), 2));
                jobEditForm.setExtendedPrice(Decimalroundup.twodecimalplaces(
                        rs.getFloat("lx_ce_total_price"), 2));
                jobEditForm.setProformaMargin(dfcurmargin.format(rs
                        .getFloat("lx_ce_margin")) + "");
                jobEditForm.setJobType(rs.getString("lm_js_type"));
                jobEditForm.setEstimatedStartSchedule(rs
                        .getString("lm_js_planned_start_date"));
                jobEditForm.setEstimatedCompleteSchedule(rs
                        .getString("lm_js_planned_end_date"));
                jobEditForm.setUnionUpliftFactor(rs
                        .getString("lp_md_union_uplift_factor"));
                jobEditForm.setLastJobModifiedBy(rs
                        .getString("lm_js_changed_by"));
                jobEditForm.setLastJobModifiedDate(rs
                        .getString("lm_js_change_date"));
                jobEditForm.setCnsPoc(rs.getString("lm_cns_poc"));
                jobEditForm.setInstallationPoc(rs
                        .getString("lm_si_installer_poc"));
                jobEditForm.setMsaId(rs.getString("lp_md_mm_id"));
                jobEditForm.setMsaName(rs.getString("lo_ot_name"));
                jobEditForm
                        .setCustRef(rs.getString("lm_js_customer_reference"));

            }
        } catch (Exception e) {
            logger.error(
                    "getJobDetailForEdit(JobEditBean, String, String, String, String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("getJobDetailForEdit(JobEditBean, String, String, String, String, DataSource) - Exception in retrieving Job"
                        + e);
            }
        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getJobDetailForEdit(JobEditBean, String, String, String, String, DataSource)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getJobDetailForEdit(JobEditBean, String, String, String, String, DataSource) - Exception in retrieving Job"
                            + sql);
                }
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getJobDetailForEdit(JobEditBean, String, String, String, String, DataSource)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getJobDetailForEdit(JobEditBean, String, String, String, String, DataSource) - Exception in retrieving Job"
                            + sql);
                }
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getJobDetailForEdit(JobEditBean, String, String, String, String, DataSource)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getJobDetailForEdit(JobEditBean, String, String, String, String, DataSource) - Exception in retrieving Job"
                            + sql);
                }
            }

        }
    }

    public static ArrayList getJobList(DataSource ds, String Appendix_Id,
            String querystring, String flag, String addendumflag) {
        ArrayList all_jobs = new ArrayList();
        Connection conn = null;
        Job job = null;
        Statement stmt = null;
        ResultSet rs = null;

        if (querystring.equals("")) {
            querystring = "order by jobname";
        }
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql_list = "select * from dbo.func_lp_job_tab('"
                    + Appendix_Id + "' , '%' , '" + flag + "' , '"
                    + addendumflag + "')" + querystring;
            // String sql_list =
            // "select * from dbo.func_lp_job_tab('"+Appendix_Id+"' , '%' , '"+flag+"' , '"+addendumflag+"') where 1=1 ";
            rs = stmt.executeQuery(sql_list);
            all_jobs = setJob(rs);
        } catch (SQLException e) {
            logger.error(
                    "getJobList(DataSource, String, String, String, String)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getJobList(DataSource, String, String, String, String) - Exception caught in getting all Jobs"
                        + e);
            }
        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getJobList(DataSource, String, String, String, String)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getJobList(DataSource, String, String, String, String) - Exception caught in getting all Jobs"
                            + sql);
                }
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getJobList(DataSource, String, String, String, String)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getJobList(DataSource, String, String, String, String) - Exception caught in getting all Jobs"
                            + sql);
                }
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getJobList(DataSource, String, String, String, String)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getJobList(DataSource, String, String, String, String) - Exception caught in getting all Jobs"
                            + sql);
                }
            }

        }

        return all_jobs;
    }

    public static ArrayList setJob(ResultSet rs) {
        ArrayList joblist = new ArrayList();
        Job job;
        String temp = "";
        DecimalFormat dfcurmargin = new DecimalFormat("0.00");
        try {
            while (rs.next()) {
                job = new Job();

                job.setJob_Id(rs.getString(2));

                job.setAppendix_Id(rs.getString(1));
                job.setName(rs.getString(3));
                job.setStatus(rs.getString(4));
                job.setEstimated_materialcost(Decimalroundup.twodecimalplaces(
                        rs.getFloat(5), 2));
                job.setEstimated_cnsfieldlaborcost(Decimalroundup
                        .twodecimalplaces(rs.getFloat(6), 2));
                job.setEstimated_contractfieldlaborcost(Decimalroundup
                        .twodecimalplaces(rs.getFloat(7), 2));
                job.setEstimated_freightcost(Decimalroundup.twodecimalplaces(
                        rs.getFloat(8), 2));
                job.setEstimated_travelcost(Decimalroundup.twodecimalplaces(
                        rs.getFloat(9), 2));
                job.setEstimated_totalcost(Decimalroundup.twodecimalplaces(
                        rs.getFloat(10), 2));
                job.setExtendedprice(Decimalroundup.twodecimalplaces(
                        rs.getFloat(11), 2));

                job.setProformamargin(dfcurmargin.format(rs.getFloat(12)) + "");
                job.setJobtype(rs.getString(13));

                /*
                 * if( job.getJobtype().equals( "inscopejob") ) job.setStatus(
                 * rs.getString ( 31 ) ); else job.setStatus( rs.getString ( 4 )
                 * );
                 */
                job.setEstimated_start_schedule(rs.getString(14));
                job.setEstimated_complete_schedule(rs.getString(15));

                job.setSite_id(rs.getString(16));
                job.setSite_name(rs.getString(17));
                job.setSite_state(rs.getString(18));
                job.setLocality_uplift(rs.getString(19));

                job.setUnion(rs.getString(20));
                job.setSite_number(rs.getString(21));
                job.setSite_address(rs.getString(22));
                job.setSite_city(rs.getString(23));
                job.setSite_zipcode(rs.getString(24));
                job.setSite_POC(rs.getString(25));
                job.setUnion_uplift_factor(rs.getString(26));

                job.setLastjobmodifiedby(rs.getString(27));
                job.setLastjobmodifieddate(rs.getString(28));
                job.setCNS_POC(rs.getString(29));
                job.setInstallation_POC(rs.getString(30));

                job.setMsa_Id(rs.getString("lp_md_mm_id"));
                job.setMsaname(rs.getString("lo_ot_name"));
                job.setCustref(rs.getString("lm_js_customer_reference"));
                joblist.add(job);
            }
        } catch (Exception e) {
            logger.error("setJob(ResultSet)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("setJob(ResultSet) - Exception Caught in setJob"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.error("setJob(ResultSet)", e);

                if (logger.isDebugEnabled()) {
                    logger.debug("setJob(ResultSet) - Exception Caught in setJob"
                            + e);
                }
            }
        }
        return joblist;
    }

    public static Job getJob(String appendix_Id, String job_Id, String flag,
            String addendumflag, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        Job job = null;

        String lx_pd_id = "";

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            /*
             * String sql =
             * "select lm_js_id as Job_Id , lm_js_pr_id as Appendix_Id , lm_js_title as jobname , "
             * +
             * "lm_js_margin , lm_js_status , lm_si_name , lm_si_address , lm_si_city , lm_si_state , lm_si_zip_code , "
             * +
             * "lm_si_country  from lm_job left outer join lm_site_detail on  lm_js_si_id = lm_si_id where "
             * + "lm_js_pr_id ='"+appendix_Id+"' and lm_js_id ="+job_Id;
             */
            String sql = "select * from dbo.func_lp_job_tab('" + appendix_Id
                    + "','" + job_Id + "' , '" + flag + "' , '" + addendumflag
                    + "')";

            rs = stmt.executeQuery(sql);
            job = new Job();
            ArrayList joblist = setJob(rs);

            job = (Job) joblist.get(0);

            sql = "select com_desc , com_by , comment_date from dbo.func_last_comment('"
                    + job.getJob_Id() + "' , 'Job')";

            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                job.setLastcomment(rs.getString("com_desc"));
                job.setLastchangeby(rs.getString("com_by"));
                job.setLastchangedate(rs.getString("comment_date"));
            }
        } catch (Exception e) {
            logger.error("getJob(String, String, String, String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("getJob(String, String, String, String, DataSource) - Exception in retrieving job"
                        + e);
            }
        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getJob(String, String, String, String, DataSource)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getJob(String, String, String, String, DataSource) - Exception in retrieving appendix"
                            + sql);
                }
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getJob(String, String, String, String, DataSource)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getJob(String, String, String, String, DataSource) - Exception in retrieving appendix"
                            + sql);
                }
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getJob(String, String, String, String, DataSource)",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getJob(String, String, String, String, DataSource) - Exception in retrieving appendix"
                            + sql);
                }
            }

        }
        return job;
    }

    public static int updateJobName(String jobId, String jobName,
            String ownerId, String appendixId, DataSource ds) throws Exception {
        Connection conn = null;
        CallableStatement cstmt = null;
        int val = -1;
        /*
         * System.out.println("2= "+Integer.parseInt(jobId) );
         * System.out.println("3= "+jobName); System.out.println("4= " +"U" );
         */
        try {
            conn = ds.getConnection();
            cstmt = conn
                    .prepareCall("{?=call dbo.lx_job_name_change_01(?,?,?,?)}");

            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setInt(2, Integer.parseInt(jobId));
            cstmt.setString(3, jobName);
            cstmt.setInt(4, Integer.parseInt(appendixId));
            cstmt.setInt(5, Integer.parseInt(ownerId));
            cstmt.execute();
            val = cstmt.getInt(1);
        } catch (Exception e) {
            logger.error(
                    "updateJobName(String, String, String, String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("updateJobName(String, String, String, String, DataSource) - Error Occured in Inserting Job"
                        + e);
            }
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "updateJobName(String, String, String, String, DataSource) - exception ignored",
                        sql);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "updateJobName(String, String, String, String, DataSource) - exception ignored",
                        sql);
            }

        }
        return val;
    }

    public static String getAppendixname(DataSource ds, String Appendix_Id) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        if (Appendix_Id.equals("-1") || Appendix_Id.equals("%")) {
            return "All";
        }
        String appendixname = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            sql = "select dbo.func_cc_name('" + Appendix_Id + "', 'Appendix' )";

            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                appendixname = rs.getString(1);
            }
        } catch (Exception e) {
            logger.error("getAppendixname(" + ds + ", " + Appendix_Id + ")", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getAppendixname(DataSource, String) - Error Occured in retrieving appendixname"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getAppendixname(DataSource, String) - exception ignored",
                        s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getAppendixname(DataSource, String) - exception ignored",
                        s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getAppendixname(DataSource, String) - exception ignored",
                        s);
            }

        }
        return appendixname;
    }

    public static void addLogEntry(DataSource ds, String msg, String data) {
        Connection conn = null;
        Statement stmt = null;

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "INSERT INTO lm_log_entry (lm_le_log_id, lm_le_date, lm_le_level, lm_le_source, lm_le_message, lm_le_data, lm_le_direction)"
                    + " VALUES (4, GETDATE(), 0, 'Ilex', '" + msg + "', '" + data + "', 1)";

            stmt.executeUpdate(sql);
        } catch (Exception e) {
            logger.error("addUpdateRequestorEmail(JobId, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("addUpdateRequestorEmail(JobId, String, DataSource) - Error Occured in Inserting Job" + e);
            }
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sql) {
                logger.warn("addUpdateRequestorEmail(JobId, String, DataSource) - exception ignored", sql);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.warn("addUpdateRequestorEmail(JobId, String, DataSource) - exception ignored", sql);
            }
        }
    }

    public static void addUpdateRequestorEmail(long jobId, String requestorIds, String additionalRecipientIds, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;

        try {
            conn = ds.getConnection();

            addLogEntry(ds, "Saving recipients!", requestorIds + " || " + additionalRecipientIds);

            stmt = conn.createStatement();
            String sql = "DELETE FROM ilex_email_requestors WHERE lm_js_id = " + jobId;
            stmt.executeUpdate(sql);

            String[] rIds = requestorIds.split(",");
            for (int i = 0; i < rIds.length; i++) {
                String requestorId = rIds[i];

                if (!requestorId.trim().equals("")) {
                    sql = "INSERT INTO ilex_email_requestors (lm_js_id, lo_pc_id) VALUES (" + jobId
                            + ", " + requestorId + ")";

                    stmt.executeUpdate(sql);
                }
            }

            stmt = conn.createStatement();
            sql = "DELETE FROM ilex_email_additional_recipients WHERE lm_js_id = " + jobId;
            stmt.executeUpdate(sql);

            String[] arIds = additionalRecipientIds.split(",");
            for (int i = 0; i < arIds.length; i++) {
                String additionalRecipientId = arIds[i];

                if (!additionalRecipientId.trim().equals("")) {
                    sql = "INSERT INTO ilex_email_additional_recipients (lm_js_id, lx_pcc_id) VALUES (" + jobId
                            + ", " + additionalRecipientId + ")";

                    stmt.executeUpdate(sql);
                }
            }
        } catch (Exception e) {
            logger.error("addUpdateRequestorEmail(JobId, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("addUpdateRequestorEmail(JobId, String, DataSource) - Error Occured in Inserting Job" + e);
            }
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sql) {
                logger.warn("addUpdateRequestorEmail(JobId, String, DataSource) - exception ignored", sql);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.warn("addUpdateRequestorEmail(JobId, String, DataSource) - exception ignored", sql);
            }
        }
    }

    /*
     * Description: Gets the last work note (install note) for a job.
     *
     * Version History:
     * 01/15/15 - Initial version
     *
     * Parameters:
     * @param (String) jobId - The ID of the job to retrieve the work note for.
     * @param (DataSource) ds - The database data source.
     *
     * Returns:
     * @return (String) The last work note entered for a given job.
     */
    public static String getLatestWorkNote(String jobId, DataSource ds) {
        String work_note = "";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "SELECT TOP 1 lm_in_notes FROM lm_install_notes WHERE lm_in_js_id = " + jobId + " ORDER BY lm_in_id DESC";

            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                work_note = rs.getString("lm_in_notes");
            }
        } catch (Exception e) {
            logger.error("setLatestInstallNotes(String, DataSource, JobDashboardBean)", e);

            logger.error("setLatestInstallNotes(String, DataSource, JobDashboardBean)", e);
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);

        }

        return work_note.trim();
    }

    public static int addJob(Job job, String loginuserid, DataSource ds)
            throws Exception {
        Connection conn = null;
        CallableStatement cstmt = null;
        int val = -1;

        try {
            conn = ds.getConnection();
            cstmt = conn
                    .prepareCall("{?=call dbo.lx_job_manage_01(?,?,?,?,?,?,?,?,?,?,?)}");

            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setInt(2, 0);
            cstmt.setInt(3, Integer.parseInt(job.getAppendix_Id()));
            cstmt.setString(4, job.getName());
            cstmt.setString(5, job.getJobtype());
            cstmt.setString(6, job.getStatus());
            cstmt.setString(7, job.getEstimated_start_schedule());
            cstmt.setString(8, job.getEstimated_complete_schedule());
            cstmt.setInt(9, Integer.parseInt(loginuserid));
            cstmt.setString(10, "A");
            cstmt.setString(11, job.getCustref());
            cstmt.setInt(12, 0);
            cstmt.execute();
            val = cstmt.getInt(1);

        } catch (Exception e) {
            logger.error("addJob(Job, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("addJob(Job, String, DataSource) - Error Occured in Inserting Job"
                        + e);
            }
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "addJob(Job, String, DataSource) - exception ignored",
                        sql);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "addJob(Job, String, DataSource) - exception ignored",
                        sql);
            }

        }
        return val;
    }

    public static int updateJob(Job job, String loginuserid, DataSource ds)
            throws Exception {
        Connection conn = null;
        CallableStatement cstmt = null;
        int val = -1;

        try {
            conn = ds.getConnection();
            cstmt = conn
                    .prepareCall("{?=call dbo.lx_job_manage_01(?,?,?,?,?,?,?,?,?,?,?)}");

            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setInt(2, Integer.parseInt(job.getJob_Id()));
            cstmt.setInt(3, Integer.parseInt(job.getAppendix_Id()));
            cstmt.setString(4, job.getName());
            cstmt.setString(5, "");
            cstmt.setString(6, "");
            cstmt.setString(7, job.getEstimated_start_schedule());
            cstmt.setString(8, job.getEstimated_complete_schedule());
            cstmt.setInt(9, Integer.parseInt(loginuserid));
            cstmt.setString(10, "U");
            cstmt.setString(11, job.getCustref());
            cstmt.setInt(12, 0);
            cstmt.execute();
            val = cstmt.getInt(1);
        } catch (Exception e) {
            logger.error("updateJob(Job, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("updateJob(Job, String, DataSource) - Error Occured in Inserting Job"
                        + e);
            }
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "updateJob(Job, String, DataSource) - exception ignored",
                        sql);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "updateJob(Job, String, DataSource) - exception ignored",
                        sql);
            }

        }
        return val;
    }

    public static int addSite(SiteBean siteform, String loginuserid,
            DataSource ds) throws Exception {
        Connection conn = null;
        CallableStatement cstmt = null;
        int val = -1;

        try {
            conn = ds.getConnection();
            cstmt = conn
                    .prepareCall("{?=call dbo.lx_jobsite_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setInt(2, 0);
            cstmt.setInt(3, Integer.parseInt(siteform.getId2()));
            cstmt.setString(4, siteform.getSite_name());
            cstmt.setString(5, siteform.getLocality_uplift());
            cstmt.setString(6, siteform.getUnion_site());
            cstmt.setString(7, siteform.getSite_designator());
            // System.out.println("Value: "+);
            cstmt.setString(8, siteform.getSite_worklocation());

            cstmt.setString(9, siteform.getSite_address());
            cstmt.setString(10, siteform.getSite_city());
            cstmt.setString(11, siteform.getState());
            cstmt.setString(12, siteform.getSite_zipcode());
            cstmt.setString(13, siteform.getCountry());
            cstmt.setString(14, siteform.getSite_direction());

            cstmt.setString(15, siteform.getSite_notes());
            cstmt.setString(16, siteform.getSite_number());
            cstmt.setInt(17, Integer.parseInt(loginuserid));
            cstmt.setString(18, "A");
            cstmt.setString(19, siteform.getSite_phone());

            cstmt.setString(20, siteform.getSecondary_phone());
            cstmt.setString(21, siteform.getPrimary_Name());
            cstmt.setString(22, siteform.getSecondary_Name());
            cstmt.setString(23, siteform.getPrimary_email());
            cstmt.setString(24, siteform.getSecondary_email());
            cstmt.setString(25, siteform.getSitelatdeg());
            cstmt.setString(26, siteform.getSitelatmin());
            cstmt.setString(27, siteform.getSitelatdirection());
            cstmt.setString(28, siteform.getSitelondeg());
            cstmt.setString(29, siteform.getSitelonmin());
            cstmt.setString(30, siteform.getSitelondirection());
            cstmt.setString(31, siteform.getSitestatus());
            cstmt.setString(32, siteform.getSitegroup());
            cstmt.setString(33, siteform.getSiteendcustomer());
            cstmt.setString(34, siteform.getSitetimezone());
            cstmt.setString(35, siteform.getSiteregion());
            cstmt.setString(36, siteform.getSitecategory());
            cstmt.setString(37, siteform.getSitelistid());

            cstmt.execute();
            val = cstmt.getInt(1);
        } catch (Exception e) {
            logger.error("addSite(SiteForm, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("addSite(SiteForm, String, DataSource) - Error Occured in Inserting Job"
                        + e);
            }
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "addSite(SiteForm, String, DataSource) - exception ignored",
                        sql);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "addSite(SiteForm, String, DataSource) - exception ignored",
                        sql);
            }

        }
        return val;

    }

    public static int updateSite(SiteBean siteform, String loginuserid,
            DataSource ds) {
        Connection conn = null;
        CallableStatement cstmt = null;
        int val = -1;

        /*
         * System.out.println("2:::::::"+Integer.parseInt( siteform.getSite_Id()
         * ) ); System.out.println("3:::::::"+Integer.parseInt(
         * siteform.getId2() ) );
         * System.out.println("4:::::::"+siteform.getSite_name() );
         * System.out.println("5:::::::"+siteform.getLocality_uplift() );
         * System.out.println("6:::::::"+siteform.getUnion_site() );
         * System.out.println("7:::::::"+siteform.getSite_designator() );
         * System.out.println("8:::::::"+siteform.getSite_worklocation() );
         * System.out.println("9:::::::"+siteform.getSite_address() );
         * System.out.println("10:::::::"+siteform.getSite_city() );
         * System.out.println("11:::::::"+siteform.getSite_state() );
         * System.out.println("12:::::::"+siteform.getSite_zipcode() );
         * System.out.println("13:::::::"+siteform.getSite_country() );
         * System.out.println("14:::::::"+siteform.getSite_direction() );
         *
         *
         * System.out.println("15:::::::"+siteform.getSite_notes() );
         * System.out.println("16:::::::"+siteform.getSite_number() );
         * System.out.println("17:::::::"+Integer.parseInt( loginuserid ) );
         * System.out.println("18:::::::U" );
         * System.out.println("19:::::::"+siteform.getSite_phone());
         *
         * System.out.println("20:::::::"+siteform.getSecondary_phone());
         * System.out.println("21:::::::"+siteform.getPrimary_Name());
         * System.out.println("22:::::::"+siteform.getSecondary_Name());
         * System.out.println("23:::::::"+siteform.getPrimary_email());
         * System.out.println("24:::::::"+siteform.getSecondary_email());
         * System.out.println("25:::::::"+siteform.getSitelatdeg());
         * System.out.println("26:::::::"+siteform.getSitelatmin());
         * System.out.println("27:::::::"+siteform.getSitelatdirection());
         * System.out.println("28:::::::"+siteform.getSitelondeg());
         * System.out.println("29:::::::"+siteform.getSitelonmin());
         * System.out.println("30:::::::"+siteform.getSitelondirection());
         * System.out.println("31:::::::"+siteform.getSitestatus());
         * System.out.println("32:::::::"+siteform.getSitegroup());
         * System.out.println("33:::::::"+siteform.getSiteendcustomer());
         * System.out.println("34:::::::"+siteform.getSitetimezone());
         * System.out.println("35:::::::"+siteform.getSiteregion());
         * System.out.println("36:::::::"+siteform.getSitecategory());
         * System.out.println("37:::::::"+siteform.getSitelistid());
         */
        try {

            conn = ds.getConnection();
            cstmt = conn
                    .prepareCall("{?=call dbo.lx_jobsite_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setInt(2, Integer.parseInt(siteform.getSite_Id()));
            cstmt.setInt(3, Integer.parseInt(siteform.getId2()));
            cstmt.setString(4, siteform.getSite_name());
            cstmt.setString(5, siteform.getLocality_uplift());
            cstmt.setString(6, siteform.getUnion_site());
            cstmt.setString(7, siteform.getSite_designator());
            cstmt.setString(8, siteform.getSite_worklocation());
            cstmt.setString(9, siteform.getSite_address());
            cstmt.setString(10, siteform.getSite_city());
            cstmt.setString(11, siteform.getState());
            cstmt.setString(12, siteform.getSite_zipcode());
            cstmt.setString(13, siteform.getCountry());
            cstmt.setString(14, siteform.getSite_direction());

            cstmt.setString(15, siteform.getSite_notes());
            cstmt.setString(16, siteform.getSite_number());
            cstmt.setInt(17, Integer.parseInt(loginuserid));
            cstmt.setString(18, "U");
            cstmt.setString(19, siteform.getSite_phone());

            cstmt.setString(20, siteform.getSecondary_phone());
            cstmt.setString(21, siteform.getPrimary_Name());
            cstmt.setString(22, siteform.getSecondary_Name());
            cstmt.setString(23, siteform.getPrimary_email());
            cstmt.setString(24, siteform.getSecondary_email());
            cstmt.setString(25, siteform.getSitelatdeg());
            cstmt.setString(26, siteform.getSitelatmin());
            cstmt.setString(27, siteform.getSitelatdirection());
            cstmt.setString(28, siteform.getSitelondeg());
            cstmt.setString(29, siteform.getSitelonmin());
            cstmt.setString(30, siteform.getSitelondirection());
            cstmt.setString(31, siteform.getSitestatus());
            cstmt.setString(32, siteform.getSitegroup());
            cstmt.setString(33, siteform.getSiteendcustomer());
            cstmt.setString(34, siteform.getSitetimezone());
            cstmt.setString(35, siteform.getSiteregion());
            cstmt.setString(36, siteform.getSitecategory());
            cstmt.setString(37, siteform.getSitelistid());

            cstmt.execute();
            val = cstmt.getInt(1);

        } catch (Exception e) {
            logger.error("updateSite(SiteForm, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("updateSite(SiteForm, String, DataSource) - Error Occured in Updating Job Job:::::::::"
                        + e);
            }
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "updateSite(SiteForm, String, DataSource) - exception ignored",
                        sql);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "updateSite(SiteForm, String, DataSource) - exception ignored",
                        sql);
            }

        }
        return val;

    }

    public static ArrayList getStateCategoryList(DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        ArrayList valuelist = new ArrayList();

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            String sql = "select cp_country_id, cp_country_name, cp_country_order from lp_country_list where cp_country_id is not null order by cp_country_order,cp_country_name";
            rs = stmt.executeQuery(sql);
            int i = 0;

            if (i == 0) {
                SiteBean siteform = new SiteBean();
                siteform.setSiteCountryId("");
                siteform.setSiteCountryIdName("");
                valuelist.add(i, siteform);
                i++;
            }

            while (rs.next()) {
                SiteBean siteform = new SiteBean();

                siteform.setSiteCountryId(rs.getString("cp_country_id"));
                siteform.setSiteCountryIdName(rs.getString("cp_country_name"));
                valuelist.add(i, siteform);
                i++;
            }

        } catch (Exception e) {
            logger.error("getStateCategoryList(DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getStateCategoryList(DataSource) - Error occured during getting State Category Wise List  ");
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getStateCategoryList(DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getStateCategoryList(DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getStateCategoryList(DataSource) - exception ignored",
                        e);
            }

        }
        return valuelist;
    }

    public static void getRefershedStates(SiteBean siteform, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            String sql = "select cp_state_id,cp_country_id,cp_category_id,cp_region_id  from cp_state where cp_state_id ='"
                    + siteform.getState() + "'";

            // System.out.println("sql::::"+sql);
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                // System.out.println(rs.getString("cp_category_id"));
                siteform.setState(rs.getString("cp_state_id"));
                siteform.setCountry(rs.getString("cp_country_id"));
                siteform.setSitecategory(rs.getString("cp_category_id"));
                siteform.setSiteregion(rs.getString("cp_region_id"));
            }

        } catch (Exception e) {
            logger.error("getRefershedStates(SiteForm, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getRefershedStates(SiteForm, DataSource) - Error occured during getting country , Category  , Region   "
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getRefershedStates(SiteForm, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getRefershedStates(SiteForm, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getRefershedStates(SiteForm, DataSource) - exception ignored",
                        e);
            }

        }

    }

    public static SiteBean getSite(SiteBean siteform, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";
        // DecimalFormat dformat = new DecimalFormat( "0.00" );
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            sql = "select * from dbo.func_lp_jobsite_tab( '"
                    + siteform.getId2() + "' )";

            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                siteform.setSite_Id(rs.getString(1));
                siteform.setSite_name(rs.getString(2));
                siteform.setLocality_uplift(rs.getString(3));
                siteform.setUnion_site(rs.getString(4));
                siteform.setSite_designator(rs.getString(5));
                siteform.setSite_worklocation(rs.getString(6));

                siteform.setSite_address(rs.getString(7));
                siteform.setSite_city(rs.getString(8));
                siteform.setState(rs.getString(9));
                siteform.setSite_zipcode(rs.getString(10));
                siteform.setCountry(rs.getString(11));
                siteform.setSite_direction(rs.getString(12));

                siteform.setPrimary_Name(rs.getString(13));
                siteform.setSecondary_Name(rs.getString(14));

                siteform.setSite_notes(rs.getString(15));
                siteform.setSite_number(rs.getString(18));
                siteform.setSite_phone(rs.getString(19));// For Primary Phone

                siteform.setSecondary_phone(rs.getString(20));// For Secondary
                // Phone
                siteform.setPrimary_email(rs.getString(21));
                siteform.setSecondary_email(rs.getString(22));
                // siteform.setSitelatdeg(dformat.format(rs.getFloat( 23 )));
                siteform.setSitelatdeg(rs.getString(23));
                siteform.setSitelatmin(rs.getString(24));
                siteform.setSitelatdirection(rs.getString(25));
                // siteform.setSitelondeg(dformat.format(rs.getFloat( 26 )));
                siteform.setSitelondeg(rs.getString(26));
                siteform.setSitelonmin(rs.getString(27));
                siteform.setSitelondirection(rs.getString(28));
                siteform.setSitestatus(rs.getString(29));
                siteform.setSitegroup(rs.getString(30));
                siteform.setSiteendcustomer(rs.getString(31));
                siteform.setSitetimezone(rs.getString(32));
                siteform.setSiteregion(rs.getString(33));
                siteform.setSitecategory(rs.getString(34));
                siteform.setSitelistid(rs.getString("lp_sl_id"));
            }

        } catch (Exception e) {
            logger.error("getSite(SiteForm, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getSite(SiteForm, DataSource) - Error in retrieving site");
            }
        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.error("getSite(SiteForm, DataSource)", s);

                if (logger.isDebugEnabled()) {
                    logger.debug("getSite(SiteForm, DataSource) - Exception in retrieving site"
                            + s);
                }
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.error("getSite(SiteForm, DataSource)", s);

                if (logger.isDebugEnabled()) {
                    logger.debug("getSite(SiteForm, DataSource) - Exception in retrieving site"
                            + sql);
                }
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.error("getSite(SiteForm, DataSource)", s);

                if (logger.isDebugEnabled()) {
                    logger.debug("getSite(SiteForm, DataSource) - Exception in retrieving site"
                            + sql);
                }
            }

        }
        return siteform;
    }

    public static void deleteSite(SiteBean siteform, DataSource ds)
            throws Exception {
        Connection conn = null;
        Statement stmt = null;

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "update lm_job set lm_js_si_id = null where lm_js_id="
                    + siteform.getId2();
            stmt.executeUpdate(sql);

            sql = "delete from lm_site_detail where lm_si_id="
                    + siteform.getSite_Id();
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            logger.error("deleteSite(SiteForm, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("deleteSite(SiteForm, DataSource) - Exception caught in deleting site"
                        + e);
            }
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.error("deleteSite(SiteForm, DataSource)", e);

                if (logger.isDebugEnabled()) {
                    logger.debug("deleteSite(SiteForm, DataSource) - Exception caught in deleting site"
                            + e);
                }
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.error("deleteSite(SiteForm, DataSource)", sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("deleteSite(SiteForm, DataSource) - Exception caught in deleting site"
                            + sql);
                }
            }
        }
    }

    public static String getAddendumname(DataSource ds, String Addendum_Id) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        String addendumname = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            sql = "select dbo.func_cc_name('" + Addendum_Id + "', 'Addendum' )";

            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                addendumname = rs.getString(1);
            }
        } catch (Exception e) {
            logger.error("getAddendumname(DataSource, String)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getAddendumname(DataSource, String) - Error Occured in retrieving addendumname"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getAddendumname(DataSource, String) - exception ignored",
                        s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getAddendumname(DataSource, String) - exception ignored",
                        s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getAddendumname(DataSource, String) - exception ignored",
                        s);
            }

        }
        return addendumname;
    }

    public static String getJobname(String Job_Id, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        String jobname = "";

        if (Job_Id.equals("")) {
            return "";
        }

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            sql = "select lm_js_title from lm_job where lm_js_id=" + Job_Id;
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                jobname = rs.getString(1);
            }
        } catch (Exception e) {
            logger.error("getJobname(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getJobname(String, DataSource) - Error Occured in retrieving jobname"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobname(String, DataSource) - exception ignored", s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobname(String, DataSource) - exception ignored", s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobname(String, DataSource) - exception ignored", s);
            }

        }
        return jobname.trim();
    }

    public static String getJobtype(String Job_Id, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        String jobtype = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            sql = "select lm_js_type from lm_job where lm_js_id=" + Job_Id;

            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                jobtype = rs.getString(1);
            }
        } catch (Exception e) {
            logger.error("getJobtype(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getJobtype(String, DataSource) - Error Occured in retrieving jobtype"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobtype(String, DataSource) - exception ignored", s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobtype(String, DataSource) - exception ignored", s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobtype(String, DataSource) - exception ignored", s);
            }

        }
        return jobtype;
    }

    public static String getoplevelname(String Id, String type, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        String name = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            sql = "select dbo.func_cc_above_level_name('" + Id + "' , '" + type
                    + "')";

            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                name = rs.getString(1);
            }
        } catch (Exception e) {
            logger.error("getoplevelname(String, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getoplevelname(String, String, DataSource) - Error Occured in retrieving toplevelname"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getoplevelname(String, String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getoplevelname(String, String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getoplevelname(String, String, DataSource) - exception ignored",
                        s);
            }

        }
        return name;
    }

    public static String getJobsitezipcode(String Job_Id, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        String jobsitezipcode = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            sql = "select dbo.func_lp_jobsite_zipcode( '" + Job_Id + "' )";

            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                jobsitezipcode = rs.getString(1);
            }
        } catch (Exception e) {
            logger.error("getJobsitezipcode(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getJobsitezipcode(String, DataSource) - Error Occured in retrieving jobsitezipcode"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobsitezipcode(String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobsitezipcode(String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobsitezipcode(String, DataSource) - exception ignored",
                        s);
            }

        }
        return jobsitezipcode;
    }

    public static ArrayList getdefaultjobactivities(DataSource ds,
            String Appendix_Id, String Job_Id) {
        ArrayList all_jobs = new ArrayList();
        Connection conn = null;
        Job job = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            sql = "select * from dbo.func_lp_newactivity_copy_tab( '"
                    + Appendix_Id + "' , '" + Job_Id + "' )";
            rs = stmt.executeQuery(sql);

            all_jobs = setActivity(rs);
        } catch (SQLException e) {
            logger.error("getdefaultjobactivities(DataSource, String, String)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("getdefaultjobactivities(DataSource, String, String) - Exception caught in getting default Jobs activities"
                        + e);
            }
        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.error(
                        "getdefaultjobactivities(DataSource, String, String)",
                        s);

                if (logger.isDebugEnabled()) {
                    logger.debug("getdefaultjobactivities(DataSource, String, String) - Exception caught in getting default job activities"
                            + s);
                }
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.error(
                        "getdefaultjobactivities(DataSource, String, String)",
                        s);

                if (logger.isDebugEnabled()) {
                    logger.debug("getdefaultjobactivities(DataSource, String, String) - Exception caught in getting default job activities"
                            + s);
                }
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.error(
                        "getdefaultjobactivities(DataSource, String, String)",
                        s);

                if (logger.isDebugEnabled()) {
                    logger.debug("getdefaultjobactivities(DataSource, String, String) - Exception caught in getting default job activities"
                            + s);
                }
            }

        }

        return all_jobs;
    }

    public static ArrayList setActivity(ResultSet rs) {
        ArrayList activitylist = new ArrayList();
        Activity activity;
        String temp = "";

        try {
            while (rs.next()) {
                activity = new Activity();

                activity.setJob_Id(rs.getString(1));
                activity.setActivity_Id(rs.getString(2));

                activity.setName(rs.getString(3));
                activity.setActivitytype(rs.getString(4));
                if (activity.getActivitytype().equals("Overhead")) {
                    activity.setActivitytypecombo("V");
                } else {
                    activity.setActivitytypecombo(""
                            + activity.getActivitytype().charAt(0));
                }

                activity.setQuantity(rs.getString(5));

                activity.setEstimated_materialcost(Decimalroundup
                        .twodecimalplaces(rs.getFloat(6), 2));
                activity.setEstimated_cnsfieldlaborcost(Decimalroundup
                        .twodecimalplaces(rs.getFloat(7), 2));
                activity.setEstimated_contractfieldlaborcost(Decimalroundup
                        .twodecimalplaces(rs.getFloat(8), 2));
                activity.setEstimated_freightcost(Decimalroundup
                        .twodecimalplaces(rs.getFloat(9), 2));
                activity.setEstimated_travelcost(Decimalroundup
                        .twodecimalplaces(rs.getFloat(10), 2));
                activity.setEstimated_totalcost(Decimalroundup
                        .twodecimalplaces(rs.getFloat(11), 2));
                activity.setExtendedprice(Decimalroundup.twodecimalplaces(
                        rs.getFloat(12), 2));
                activity.setOverheadcost(Decimalroundup.twodecimalplaces(
                        rs.getFloat(13), 2));

                if (activity.getActivitytype().equals("Overhead")) {
                    activity.setListprice(Decimalroundup.twodecimalplaces(
                            Float.parseFloat("0.0000"), 2));
                    activity.setProformamargin(Decimalroundup.twodecimalplaces(
                            Float.parseFloat("0.0000"), 2));
                } else {
                    activity.setListprice(Decimalroundup.twodecimalplaces(
                            rs.getFloat(14), 2));
                    activity.setProformamargin(Decimalroundup.twodecimalplaces(
                            rs.getFloat(15), 2));
                }

                activity.setStatus(rs.getString(16));
                activity.setJobtype(rs.getString(17));
                activity.setActivity_lib_Id(rs.getString(18));

                activity.setEstimated_cnshqlaborcost(Decimalroundup
                        .twodecimalplaces(rs.getFloat(19), 2));
                activity.setEstimated_cnsfieldhqlaborcost(Decimalroundup
                        .twodecimalplaces(rs.getFloat(7) + rs.getFloat(19), 2));

                activitylist.add(activity);
            }
        } catch (Exception e) {
            logger.error("setActivity(ResultSet)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("setActivity(ResultSet) - Exception Caught in setActivity"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.error("setActivity(ResultSet)", e);

                if (logger.isDebugEnabled()) {
                    logger.debug("setActivity(ResultSet) - Exception Caught in setActivity"
                            + e);
                }
            }
        }
        return activitylist;
    }

    public static String getCurrDate(DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";
        String currDate = null;

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            sql = "select isnull(replace(convert(varchar(10), getdate() , 101 ) , '01/01/1900', '' ), '' )";
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                currDate = rs.getString(1);
            }
        } catch (Exception e) {
            logger.error("getCurrDate(DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getCurrDate(DataSource) - Error Occured in retrieving curr Date"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn("getCurrDate(DataSource) - exception ignored", s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn("getCurrDate(DataSource) - exception ignored", s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn("getCurrDate(DataSource) - exception ignored", s);
            }

        }
        return currDate;
    }

    public static String getappendixscheduledate(DataSource ds,
            String Appendix_Id, String dateselect) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        String formaldocdatedate = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            if (dateselect.equals("start")) {
                sql = "select lx_pd_need_date = isnull(replace(convert(varchar(10) , lx_pd_need_date , 101 ) , '01/01/1900' , '' ) , '' ) from lx_appendix_detail where lx_pd_pr_id = "
                        + Appendix_Id;
            } else {
                sql = "select lx_pd_effective_date = isnull(replace(convert(varchar(10) , lx_pd_effective_date , 101 ) , '01/01/1900' , '' ) , '' ) "
                        + "from lx_appendix_detail where lx_pd_pr_id = "
                        + Appendix_Id;
            }

            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                formaldocdatedate = rs.getString(1);
            }
        } catch (Exception e) {
            logger.error("getappendixscheduledate(DataSource, String, String)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("getappendixscheduledate(DataSource, String, String) - Error Occured in retrieving formaldocdatedate"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getappendixscheduledate(DataSource, String, String) - exception ignored",
                        s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getappendixscheduledate(DataSource, String, String) - exception ignored",
                        s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getappendixscheduledate(DataSource, String, String) - exception ignored",
                        s);
            }

        }
        return formaldocdatedate;
    }

    public static String getAppendixCustRef(DataSource ds, String Appendix_Id) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        String customerReference = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            sql = "select isnull(lx_pr_customer_reference, '') from lx_appendix_main where lx_pr_id = "
                    + Appendix_Id;

            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                customerReference = rs.getString(1);
            }
        } catch (Exception e) {
            logger.error("getAppendixCustRef(DataSource, String)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getAppendixCustRef(DataSource, String) - Error Occured in retrieving customer reference"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getAppendixCustRef(DataSource, String) - exception ignored",
                        s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getAppendixCustRef(DataSource, String) - exception ignored",
                        s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getAppendixCustRef(DataSource, String) - exception ignored",
                        s);
            }

        }
        return customerReference;
    }

    public static int deletejob(String Id, DataSource ds) throws Exception {
        Connection conn = null;
        CallableStatement cstmt = null;
        int val = -1;

        try {
            conn = ds.getConnection();
            cstmt = conn.prepareCall("{?=call dbo.lx_job_delete_01(?)}");

            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setInt(2, Integer.parseInt(Id));

            cstmt.execute();
            val = cstmt.getInt(1);
        } catch (Exception e) {
            logger.error("deletejob(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("deletejob(String, DataSource) - Error Occured in deleting Job"
                        + e);
            }
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "deletejob(String, DataSource) - exception ignored",
                        sql);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "deletejob(String, DataSource) - exception ignored",
                        sql);
            }

        }
        return val;

    }

    public static String getJobStatus(String Job_Id, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        String jobStatus = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            sql = "select lm_js_status from lm_job where lm_js_id=" + Job_Id;
            if (logger.isDebugEnabled()) {
                logger.debug("getJobStatus(String, DataSource) - sql to get job status --------"
                        + sql);
            }
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                jobStatus = rs.getString(1);
            }
        } catch (Exception e) {
            logger.error("getJobStatus(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getJobStatus(String, DataSource) - Error Occured in retrieving job Status"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobStatus(String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobStatus(String, DataSource) - exception ignored",
                        s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobStatus(String, DataSource) - exception ignored",
                        s);
            }

        }
        return jobStatus;
    }

    public static int getAppendixESACount(String appendixId, DataSource ds) {
        int esaCount = 0;

        if (appendixId == null || appendixId.equals("")) {
            appendixId = "0";
        }

        try {
            ArrayList list = ESADao.appendixAgents(appendixId, ds);
            esaCount = list.size();
        } catch (Exception e) {
            System.out
                    .println("Error while getting ESA Count for an Appendix::"
                            + e);
        }
        return esaCount;
    }

    public static Timestamp getPODeliverByDate(String poId, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        Timestamp poDeliverByDate = null;
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            sql = "select lm_po_deliver_by_date from lm_purchase_order where lm_po_id = "
                    + poId;

            rs = stmt.executeQuery(sql);

            if (rs.next()) {

                poDeliverByDate = rs.getTimestamp("lm_po_deliver_by_date");
            }
        } catch (Exception e) {
            logger.error("getPODeliverByDate(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getPODeliverByDate(String, DataSource) - Error occured in fetching purchase order deliver by date for subject line of PO/WO email: "
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPODeliverByDate(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPODeliverByDate(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPODeliverByDate(String, DataSource) - exception ignored",
                        e);
            }

        }
        return poDeliverByDate;
    }

    public static String getRequestorEmailByJobId(String jobId, DataSource ds) {
        return "";
    }

    /*
     * Description: Gets the IDs of Requestors associated with a job.
     *
     * Version History:
     * 01/14/15 - Initial version
     *
     * Parameters:
     * @param (String) jobId - The job to pull data for.
     * @param (DataSource) ds - The database data source object.
     *
     * Returns:
     * @return (String) A comma seperated list of requestor IDs.
     */
    public static String getRequestorIdsByJobId(String jobId, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String requestorIds = "";
        java.util.Date dateIn = new java.util.Date();

        if (jobId == null || jobId.equals("")) {
            return "";
        }
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "SELECT lo_pc_id FROM ilex_email_requestors WHERE lm_js_id = " + jobId;
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                if (requestorIds.equals("")) {
                    requestorIds += ",";
                }
                requestorIds += rs.getString("lo_pc_id");
            }
        } catch (Exception e) {
            logger.error("getRequestorIdsByJobId(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getRequestorIdsByJobId(String, DataSource)", e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn("getRequestorIdsByJobId(String, DataSource) - exception ignored", e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn("getRequestorIdsByJobId(String, DataSource) - exception ignored", e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn("getRequestorIdsByJobId(String, DataSource) - exception ignored", e);
            }
        }

        java.util.Date dateOut = new java.util.Date();
        if (logger.isDebugEnabled()) {
            logger.debug("(" + requestorIds + ") - getRequestorIdsByJobId(String, DataSource) - **** getRequestorIdsByJobId :: time taken :: " + (dateOut.getTime() - dateIn.getTime()));
        }
        return requestorIds;
    }

    /*
     * Description: Gets the IDs of additional recipients associated with a job.
     *
     * Version History:
     * 01/14/15 - Initial version
     *
     * Parameters:
     * @param (String) jobId - The job to pull data for.
     * @param (DataSource) ds - The database data source object.
     *
     * Returns:
     * @return (String) A comma seperated list of additional recipient IDs.
     */
    public static String getAdditionalRecipientIdsByJobId(String jobId, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String additionalRecipientIds = "";
        java.util.Date dateIn = new java.util.Date();

        if (jobId == null || jobId.equals("")) {
            return "";
        }
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "SELECT lx_pcc_id FROM ilex_email_additional_recipients WHERE lm_js_id = " + jobId;
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                if (additionalRecipientIds.equals("")) {
                    additionalRecipientIds += ",";
                }
                additionalRecipientIds += rs.getString("lx_pcc_id");
            }
        } catch (Exception e) {
            logger.error("getAdditionalRecipientIdsByJobId(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getAdditionalRecipientIdsByJobId(String, DataSource)", e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn("getAdditionalRecipientIdsByJobId(String, DataSource) - exception ignored", e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn("getAdditionalRecipientIdsByJobId(String, DataSource) - exception ignored", e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn("getAdditionalRecipientIdsByJobId(String, DataSource) - exception ignored", e);
            }
        }

        java.util.Date dateOut = new java.util.Date();
        if (logger.isDebugEnabled()) {
            logger.debug("(" + additionalRecipientIds + ") - getAdditionalRecipientIdsByJobId(String, DataSource) - **** getAdditionalRecipientIdsByJobId :: time taken :: " + (dateOut.getTime() - dateIn.getTime()));
        }
        return additionalRecipientIds;
    }

    /*
     * Description: Gets a list of subscribed email recipients for a job.
     *
     * Version History:
     * 01/14/15 - Initial version
     *
     * Parameters:
     * @param (String) jobId - The job to pull data for.
     * @param (DataSource) ds - The database data source object.
     *
     * Returns:
     * @return (String) A comma seperated list of subscribed email recipients for the specified job.
     */
    public static String getEmailRecipientsByJob(String jobId, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String emailRecipients = "";
        java.util.Date dateIn = new java.util.Date();

        if (jobId == null || jobId.equals("")) {
            return "";
        }
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "SELECT DISTINCT email"
                    + " FROM ("
                    + "     SELECT ISNULL(lo_pc_email1, ISNULL(lo_pc_email2, '')) as email"
                    + "     FROM lo_poc"
                    + "     WHERE lo_pc_id IN ("
                    + "		SELECT lo_pc_id"
                    + "		FROM ilex_email_requestors"
                    + "		WHERE lm_js_id = " + jobId
                    + "     ) AND email_subscribed = 1"
                    + "     UNION"
                    + "     SELECT ISNULL(lx_pcc_email, '') as email"
                    + "     FROM lx_appendix_client_contact"
                    + "     WHERE lx_pcc_id IN ("
                    + "		SELECT lx_pcc_id"
                    + "		FROM ilex_email_additional_recipients"
                    + "		WHERE lm_js_id = " + jobId
                    + "     ) AND email_subscribed = 1"
                    + " ) as a"
                    + " WHERE email != ''";

            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                if (!emailRecipients.equals("")) {
                    emailRecipients += ",";
                }
                emailRecipients += rs.getString("email");
            }
        } catch (Exception e) {
            logger.error("getEmailRecipientsByJob(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getEmailRecipientsByJob(String, DataSource)", e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn("getEmailRecipientsByJob(String, DataSource) - exception ignored", e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn("getEmailRecipientsByJob(String, DataSource) - exception ignored", e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn("getEmailRecipientsByJob(String, DataSource) - exception ignored", e);
            }
        }

        java.util.Date dateOut = new java.util.Date();
        if (logger.isDebugEnabled()) {
            logger.debug("getEmailRecipientsByJob(String, DataSource) - **** getEmailRecipientsByJob :: time taken :: " + (dateOut.getTime() - dateIn.getTime()));
        }
        return emailRecipients;
    }

    // save and send
    public static void saveSendAction(String clientname, String clientemail,
            String appendixid, String type, DataSource ds) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        int val = -1;
        try {
            conn = ds.getConnection();
            String sql = "insert into lx_appendix_client_contact "
                    + "(lx_pcc_name,lx_pcc_email,lx_pcc_appendix_id,lx_pcc_type) values(?,?,?,?)";
            pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, clientname);
            pStmt.setString(2, clientemail);
            pStmt.setString(3, appendixid);
            pStmt.setString(4, type);

            val = pStmt.executeUpdate();
        } catch (Exception e) {
            logger.error("saveSendAction()", e);
        } finally {
            DBUtil.close(rs, pStmt);
            DBUtil.close(conn);
        }
    }

    // getting values
    public static String[] getclienttype(DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String[] statusAndType = new String[2];

        String lx_pd_id = "";

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "select lx_pcc_type from lx_appendix_client_contact";
            if (logger.isDebugEnabled()) {
                logger.debug("getJobStatusAndType(String, String, String, String, DataSource) - sql= "
                        + sql);
            }
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                statusAndType[0] = rs.getString("lx_pcc_type");

            }
        } catch (Exception e) {
            logger.error("getclienttype( DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getclienttype( DataSource) - Exception in retrieving "
                        + e);
            }
        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException sql) {
                logger.error("getclienttype(DataSource)", sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getclienttype( DataSource) - Exception in retrieving"
                            + sql);
                }
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getclienttype( DataSource) - Exception in retrieving",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getclienttype( DataSource) - Exception in retrieving"
                            + sql);
                }
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.error(
                        "getclienttype( DataSource) - Exception in retrieving",
                        sql);

                if (logger.isDebugEnabled()) {
                    logger.debug("getclienttype( DataSource) - Exception in retrieving"
                            + sql);
                }
            }

        }
        return statusAndType;
    }

}
