package com.mind.dao.PM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.pm.FreightEditBean;
import com.mind.bean.pm.LaborEditBean;
import com.mind.bean.pm.MaterialEditBean;
import com.mind.bean.pm.TravelEditBean;
import com.mind.dao.AM.FreightResource;
import com.mind.dao.AM.LaborResource;
import com.mind.dao.AM.MaterialResource;
import com.mind.dao.AM.ResourceFreight;
import com.mind.dao.AM.ResourceLabor;
import com.mind.dao.AM.ResourceMaterial;
import com.mind.dao.AM.ResourceTravel;
import com.mind.dao.AM.TravelResource;
import com.mind.fw.core.dao.util.DBUtil;

public class Resourcedao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Resourcedao.class);

	public static String getResourceStatus(String activity_Id,
			String resource_id, String type, String sessionid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String resourceStatus = "";
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_rs_status from lp_resource_tab( " + activity_Id
					+ " , '" + resource_id + "' , '" + type + "' , '"
					+ sessionid + "' )";
			rs = stmt.executeQuery(sql);

			if (rs.next())
				resourceStatus = rs.getString("lm_rs_status");
		}

		catch (Exception e) {
			logger.error(
					"getResourceStatus(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourceStatus(String, String, String, String, DataSource) - Error Occured in retrieving resource status"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getResourceStatus(String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getResourceStatus(String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getResourceStatus(String, String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		return resourceStatus;
	}

	public static String Checkforlink(String Activity_Id, DataSource ds) {
		String checkforresourcelink = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select  dbo.func_activity_type_status( '" + Activity_Id
					+ "' )";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				checkforresourcelink = rs.getString(1);
			}

		} catch (SQLException e) {
			logger.error("Checkforlink(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("Checkforlink(String, DataSource) - Exception caught in getting activity checkforresourcelink"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.error("Checkforlink(String, DataSource)", s);

				if (logger.isDebugEnabled()) {
					logger.debug("Checkforlink(String, DataSource) - Exception caught in getting activity checkforresourcelink"
							+ s);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				logger.error("Checkforlink(String, DataSource)", s);

				if (logger.isDebugEnabled()) {
					logger.debug("Checkforlink(String, DataSource) - Exception caught in getting activity checkforresourcelink"
							+ s);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException s) {
				logger.error("Checkforlink(String, DataSource)", s);

				if (logger.isDebugEnabled()) {
					logger.debug("Checkforlink(String, DataSource) - Exception caught in getting activity checkforresourcelink"
							+ s);
				}
			}
		}
		if (checkforresourcelink.equals("")) {
			checkforresourcelink = "show";
		}

		return checkforresourcelink;
	}

	public static MaterialResource getMaterialResource(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		MaterialResource materialresource = new MaterialResource();

		ArrayList materiallist = new java.util.ArrayList();

		materiallist.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select iv_ct_name , iv_mf_id , iv_mf_name , iv_rp_mfg_part_number , "
							+ "iv_rp_cns_part_number , iv_cost , iv_rp_sell_quantity , iv_rp_minimum_quantity from "
							+ "lx_resource_hierarchy_01 where iv_ct_type_desc like 'material%' and iv_rp_id="
							+ Id);

			if (rs.next()) {
				materialresource.setMaterialtype(rs.getString(1));
				materialresource.setManufacturername(rs.getString(3));
				materialresource.setManufacturerpartnumber(rs.getString(4));
				materialresource.setCnspartnumber(rs.getString(5));
				materialresource.setEstimatedunitcost(rs.getString(6));
				materialresource.setSellablequantity(rs.getString(7));
				materialresource.setMinimumquantity(rs.getString(8));
			}
		} catch (Exception e) {
			logger.error("getMaterialResource(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMaterialResource(String, DataSource) - Exception caught in rerieving material resource"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getMaterialResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getMaterialResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getMaterialResource(String, DataSource) - exception ignored",
						sql);
			}

		}
		return materialresource;
	}

	public static int addmaterialresource(MaterialResource materialresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			if (materialresource.getMaterialtype().equals("M")
					|| materialresource.getMaterialtype().equals("IM")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);

			cstmt.setInt(4, Integer.parseInt(materialresource.getMaterialid()));

			cstmt.setInt(5, Integer.parseInt(materialresource.getActivity_Id()));

			cstmt.setString(6, materialresource.getCnspartnumber());
			cstmt.setFloat(7, Float.parseFloat(materialresource.getQuantity()));
			cstmt.setFloat(8, 0);
			cstmt.setString(9, materialresource.getMaterialtype());

			cstmt.setFloat(10,
					Float.parseFloat(materialresource.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(materialresource.getProformamargin()));

			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");
			cstmt.setString(15, materialresource.getAddendum_id());

			/*
			 * System.out.println( "Query  0 , 0 , "+Integer.parseInt(
			 * materialresource.getMaterialid() )+" , " + ""+Integer.parseInt(
			 * materialresource.getActivity_Id()
			 * )+" , '"+materialresource.getCnspartnumber()+"' , " +
			 * "'"+Float.parseFloat( materialresource.getQuantity()
			 * )+"' , 0 , '"+materialresource.getMaterialtype()+"' , " +
			 * ""+Float.parseFloat( materialresource.getEstimatedunitcost()
			 * )+" , " + ""+Float.parseFloat(
			 * materialresource.getProformamargin() )+" , 'D' , " +
			 * ""+Integer.parseInt ( loginuserid
			 * )+" , 'A' , "+materialresource.getAddendum_id()+" ");
			 */

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addmaterialresource(MaterialResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addmaterialresource(MaterialResource, String, DataSource) - Error Occured in Inserting Material Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addmaterialresource(MaterialResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addmaterialresource(MaterialResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int updatematerialresource(MaterialResource materialresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;
		/*
		 * System.out.println( "2	"+materialresource.getMaterialid() ) ;
		 * System.out.println( "3	"+materialresource.getMaterialcostlibid() ) ;
		 * System.out.println( "4	"+0 ); System.out.println(
		 * "5	"+materialresource.getActivity_Id() ) ; System.out.println( "6	"+
		 * "" ); System.out.println( "7	"+materialresource.getQuantity() ) ;
		 * System.out.println( "8	"+materialresource.getPrevquantity() ) ;
		 * System.out.println( "9	"+materialresource.getMaterialtype() );
		 * System.out.println( "10	"+materialresource.getEstimatedunitcost() ) ;
		 * System.out.println( "11	"+ materialresource.getProformamargin() ) ;
		 * System.out.println( "12	"+ "" ); System.out.println(
		 * "13	"+loginuserid ) ; System.out.println( "14	"+"U" );
		 * System.out.println( "15	"+ materialresource.getAddendum_id() );
		 */

		try {
			conn = ds.getConnection();

			if (materialresource.getMaterialtype().equals("M")
					|| materialresource.getMaterialtype().equals("IM")) {

				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(materialresource.getMaterialid()));
			cstmt.setInt(3,
					Integer.parseInt(materialresource.getMaterialcostlibid()));
			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(materialresource.getActivity_Id()));

			cstmt.setString(6, "");
			cstmt.setFloat(7, Float.parseFloat(materialresource.getQuantity()));
			cstmt.setFloat(8,
					Float.parseFloat(materialresource.getPrevquantity()));
			cstmt.setString(9, materialresource.getMaterialtype());

			cstmt.setFloat(10,
					Float.parseFloat(materialresource.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(materialresource.getProformamargin()));

			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");
			cstmt.setString(15, materialresource.getAddendum_id());

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updatematerialresource(MaterialResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatematerialresource(MaterialResource, String, DataSource) - Error Occured in Updating Material Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatematerialresource(MaterialResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatematerialresource(MaterialResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	// add for UI:start

	public static int updateresourcematerial(ResourceMaterial resourcematerial,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		/*
		 * System.out.println( "2	"+resourcematerial.getMaterialid() ) ;
		 * System.out.println( "3	"+resourcematerial.getMaterialCostlibid() ) ;
		 * System.out.println( "4	"+0 ); System.out.println(
		 * "5	"+resourcematerial.getActivity_Id() ) ; System.out.println( "6	"+
		 * "" ); System.out.println( "7	"+resourcematerial.getQuantity() ) ;
		 * System.out.println( "8	"+resourcematerial.getMaterialPrevquantity() )
		 * ; System.out.println( "9	"+resourcematerial.getMaterialtype() );
		 * System.out.println(
		 * "10	"+resourcematerial.getMaterialEstimatedunitcost() ) ;
		 * System.out.println( "11	"+
		 * resourcematerial.getMaterialProformamargin() ) ; System.out.println(
		 * "12	"+ "" ); System.out.println( "13	"+loginuserid ) ;
		 * System.out.println( "14	"+"U" ); System.out.println( "15	"+
		 * resourcematerial.getAddendum_id() );
		 */

		try {
			conn = ds.getConnection();

			if (resourcematerial.getMaterialtype().equals("M")
					|| resourcematerial.getMaterialtype().equals("IM")) {

				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(resourcematerial.getMaterialid()));
			cstmt.setInt(3,
					Integer.parseInt(resourcematerial.getMaterialCostlibid()));
			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(resourcematerial.getActivity_Id()));

			cstmt.setString(6, "");
			cstmt.setFloat(7, Float.parseFloat(resourcematerial.getQuantity()));
			cstmt.setFloat(8, Float.parseFloat(resourcematerial
					.getMaterialPrevquantity()));
			cstmt.setString(9, resourcematerial.getMaterialtype());

			cstmt.setFloat(10, Float.parseFloat(resourcematerial
					.getMaterialEstimatedunitcost()));
			cstmt.setFloat(11, Float.parseFloat(resourcematerial
					.getMaterialProformamargin()));
			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");
			cstmt.setString(15, resourcematerial.getAddendum_id());

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updateresourcematerial(ResourceMaterial, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateresourcematerial(ResourceMaterial, String, DataSource) - Error Occured in Updating Material Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateresourcematerial(ResourceMaterial, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateresourcematerial(ResourceMaterial, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int updateresourcelabor(ResourceLabor resourcelabor,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		/*
		 * System.out.println(" Labor Type = "+ resourcelabor.getLabortype() );
		 * System.out.println(" 2 = "+ Integer.parseInt(
		 * resourcelabor.getLaborid() ) ); System.out.println(" 3 = "+
		 * Integer.parseInt( resourcelabor.getLaborcostlibid() ) );
		 * System.out.println(" 4 = "+ 0 ); System.out.println(" 5 = "+
		 * Integer.parseInt( resourcelabor.getActivity_Id() ) );
		 * System.out.println(" 6 = "+ "" ); System.out.println(" 7 = "+
		 * Float.parseFloat( resourcelabor.getLaborQuantityhours() ) );
		 * System.out.println(" 8 = "+ Float.parseFloat(
		 * resourcelabor.getLaborPrevquantityhours() ) );
		 * System.out.println(" 9 = "+ resourcelabor.getLabortype() );
		 * System.out.println(" 10 = "+ Float.parseFloat(
		 * resourcelabor.getLaborEstimatedhourlybasecost() ) );
		 * System.out.println(" 11 = "+ Float.parseFloat(
		 * resourcelabor.getLaborProformamargin() ) );
		 * System.out.println(" 12 = "+ "" ); System.out.println(" 13 = "+
		 * Integer.parseInt ( loginuserid ) ); System.out.println(" 14 = "+ "U"
		 * ); System.out.println(" 15 = "+ resourcelabor.getAddendum_id() );
		 * System.out.println(" 16 = "+ resourcelabor.getLaborTransit() );
		 * System.out.println(" 16 = "+ "N"); System.out.println(" 17 = "+
		 * resourcelabor.getClr_detail_id());
		 */

		try {
			conn = ds.getConnection();

			if (resourcelabor.getLabortype().equals("L")
					|| resourcelabor.getLabortype().equals("IL")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(resourcelabor.getLaborid()));
			cstmt.setInt(3, Integer.parseInt(resourcelabor.getLaborcostlibid()));
			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(resourcelabor.getActivity_Id()));

			cstmt.setString(6, "");
			cstmt.setFloat(7,
					Float.parseFloat(resourcelabor.getLaborQuantityhours()));
			cstmt.setFloat(8,
					Float.parseFloat(resourcelabor.getLaborPrevquantityhours()));

			cstmt.setString(9, resourcelabor.getLabortype());

			cstmt.setFloat(10, Float.parseFloat(resourcelabor
					.getLaborEstimatedhourlybasecost()));
			cstmt.setFloat(11,
					Float.parseFloat(resourcelabor.getLaborProformamargin()));

			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");
			cstmt.setString(15, resourcelabor.getAddendum_id());
			// for transit field
			// System.out.println("Transit value to be saved "+resourcelabor.getLaborTransit());
			if (resourcelabor.getLaborTransit() != null) {
				cstmt.setString(16, resourcelabor.getLaborTransit());
			} else {
				cstmt.setString(16, "N");
			}

			if (resourcelabor.getLabortype().equals("L")
					|| resourcelabor.getLabortype().equals("IL")) {
				cstmt.setString(17, resourcelabor.getClr_detail_id());
			}

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updateresourcelabor(ResourceLabor, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateresourcelabor(ResourceLabor, String, DataSource) - Error Occured in Updating Labor Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateresourcelabor(ResourceLabor, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateresourcelabor(ResourceLabor, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int updateresourcetravel(ResourceTravel resourcetravel,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;
		//
		// System.out.println(" 2 = "+ Integer.parseInt(
		// resourcetravel.getTravelid() ) );
		// System.out.println(" 3 = "+ Integer.parseInt(
		// resourcetravel.getTravelcostlibid() ) );
		// System.out.println(" 4 = "+ 0 );
		// System.out.println(" 5 = "+ Integer.parseInt(
		// resourcetravel.getActivity_Id() ) );
		// System.out.println(" 6 = "+ "" );
		// System.out.println(" 7 = "+ Float.parseFloat(
		// resourcetravel.getTravelQuantity() ) );
		// System.out.println(" 8 = "+ Float.parseFloat(
		// resourcetravel.getTravelPrevquantity() ) );
		// System.out.println(" 9 = "+ resourcetravel.getTraveltype() );
		// System.out.println(" 10 = "+ Float.parseFloat(
		// resourcetravel.getTravelEstimatedunitcost() ) );
		// System.out.println(" 11 = "+ Float.parseFloat(
		// resourcetravel.getTravelProformamargin() ) );
		// System.out.println(" 12 = "+ "" );
		// System.out.println(" 13 = "+ Integer.parseInt ( loginuserid ) );
		// System.out.println(" 14 = "+ "U" );
		// System.out.println(" 15 = "+ resourcetravel.getAddendum_id() );

		try {
			conn = ds.getConnection();

			if (resourcetravel.getTraveltype().equals("T")
					|| resourcetravel.getTraveltype().equals("IT")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(resourcetravel.getTravelid()));
			cstmt.setInt(3,
					Integer.parseInt(resourcetravel.getTravelcostlibid()));
			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(resourcetravel.getActivity_Id()));
			cstmt.setString(6, "");
			cstmt.setFloat(7,
					Float.parseFloat(resourcetravel.getTravelQuantity()));
			cstmt.setFloat(8,
					Float.parseFloat(resourcetravel.getTravelPrevquantity()));
			cstmt.setString(9, resourcetravel.getTraveltype());
			cstmt.setFloat(10, Float.parseFloat(resourcetravel
					.getTravelEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(resourcetravel.getTravelProformamargin()));
			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");
			cstmt.setString(15, resourcetravel.getAddendum_id());
			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updateresourcetravel(ResourceTravel, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateresourcetravel(ResourceTravel, String, DataSource) - Error Occured in UPDATING Travel Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateresourcetravel(ResourceTravel, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateresourcetravel(ResourceTravel, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int updateresourcefreight(ResourceFreight resourcefreight,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			if (resourcefreight.getFreighttype().equals("F")
					|| resourcefreight.getFreighttype().equals("IF")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(resourcefreight.getFreightid()));
			cstmt.setInt(3,
					Integer.parseInt(resourcefreight.getFreightcostlibid()));
			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(resourcefreight.getActivity_Id()));
			cstmt.setString(6, "");
			cstmt.setFloat(7,
					Float.parseFloat(resourcefreight.getFreightQuantity()));
			cstmt.setFloat(8,
					Float.parseFloat(resourcefreight.getFreightPrevquantity()));
			cstmt.setString(9, resourcefreight.getFreighttype());
			cstmt.setFloat(10, Float.parseFloat(resourcefreight
					.getFreightEstimatedunitcost()));
			cstmt.setFloat(11, Float.parseFloat(resourcefreight
					.getFreightProformamargin()));
			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");
			cstmt.setString(15, resourcefreight.getAddendum_id());
			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updateresourcefreight(ResourceFreight, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateresourcefreight(ResourceFreight, String, DataSource) - Error Occured in updating Freight"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateresourcefreight(ResourceFreight, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateresourcefreight(ResourceFreight, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static ArrayList getResourcelist(String activity_Id, String type,
			String resource_id, String querystring, String sessionid,
			String pageFlag, DataSource ds) {
		java.util.Date dateIn = new java.util.Date();
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList resourcelist = new java.util.ArrayList();
		DecimalFormat dfcurmargin = null;
		String sql = "";
		int temp = 0;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurfour = new DecimalFormat("###0.0000");

		if (pageFlag.equals("listpage"))
			dfcurmargin = new DecimalFormat("0.0000000");
		else
			dfcurmargin = new DecimalFormat("0.00");

		if (querystring.equals("")) {
			querystring = "order by iv_rp_name";
		}

		try {
			conn = ds.getConnection();

			sql = "select * from lp_resource_tab_forlist( " + activity_Id
					+ " , '" + resource_id + "' , '" + type + "' , '"
					+ sessionid + "' )" + querystring;
			String sqlp = "select * from lp_resource_tab_forlist(?, ?, ?, ?)"
					+ querystring;
			pStmt = conn.prepareStatement(sqlp);
			pStmt.setString(1, activity_Id);
			pStmt.setString(2, resource_id);
			pStmt.setString(3, type);
			pStmt.setString(4, sessionid);

			// System.out.println("Pstmt SQL is :::::" + sqlp);
			if (logger.isDebugEnabled()) {
				logger.debug("getResourcelist(String, String, String, String, String, String, DataSource) - **** getResourcelist SQL is :::::"
						+ sql);
			}

			// rs = stmt.executeQuery( sql );
			rs = pStmt.executeQuery();

			if (type.equals("M") || type.equals("IM") || type.equals("AM")
					|| type.equals("CM")) {
				ResourceMaterial resourcematerial = null;
				while (rs.next()) {
					resourcematerial = new ResourceMaterial();

					resourcematerial.setMaterialid(rs.getString(1));
					resourcematerial.setMaterialCostlibid(rs.getString(2));

					resourcematerial.setActivity_Id(rs.getString(3));

					resourcematerial.setMaterialname(rs.getString(18) + "-"
							+ rs.getString(4));
					if (resourcematerial.getMaterialname().length() > 45) {
						resourcematerial.setMaterialname(resourcematerial
								.getMaterialname().toString().substring(0, 45));
					}
					resourcematerial.setMaterialtype(rs.getString(5));
					resourcematerial.setMaterialManufacturername(rs
							.getString(6));
					resourcematerial.setMaterialManufacturerpartnumber(rs
							.getString(7));
					resourcematerial.setMaterialCnspartnumber(rs.getString(8));
					resourcematerial.setQuantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcematerial.setMaterialQuantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcematerial.setMaterialPrevquantity(dfcurfour
							.format(rs.getFloat(9)) + "");
					resourcematerial.setMaterialEstimatedunitcost(dfcur
							.format(rs.getFloat(10)) + "");
					resourcematerial.setMaterialEstimatedtotalcost(dfcur
							.format(rs.getFloat(11)) + "");
					resourcematerial.setMaterialProformamargin(dfcurmargin
							.format(rs.getFloat(12)) + "");
					resourcematerial.setMaterialPriceunit(dfcur.format(rs
							.getFloat(13)) + "");
					resourcematerial.setMaterialPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					resourcematerial.setMaterialSellablequantity(rs
							.getString(15));
					resourcematerial.setMaterialMinimumquantity(rs
							.getString(16));
					resourcematerial.setMaterialStatus(rs.getString(17));
					resourcematerial.setResourceType(rs.getString(25));
					resourcematerial.setMaterialFlag(rs.getString("flag"));
					resourcematerial.setFlag_materialid(rs.getString("flag")
							+ rs.getString(1));

					resourcelist.add(resourcematerial);
				}
			}

			if (type.equals("L") || type.equals("IL") || type.equals("AL")
					|| type.equals("CL")) {
				ResourceLabor resourcelabor = null;
				while (rs.next()) {
					resourcelabor = new ResourceLabor();
					resourcelabor.setLaborid(rs.getString(1));
					resourcelabor.setLaborcostlibid(rs.getString(2));
					resourcelabor.setActivity_Id(rs.getString(3));
					resourcelabor.setLaborname(rs.getString(18) + "-"
							+ rs.getString(4));
					if (resourcelabor.getLaborname().length() > 45) {
						resourcelabor.setLaborname(resourcelabor.getLaborname()
								.toString().substring(0, 45));
					}
					resourcelabor.setLabortype(rs.getString(5));
					resourcelabor.setLaborCnspartnumber(rs.getString(8));
					resourcelabor.setLaborQuantityhours(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcelabor.setLaborPrevquantityhours(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcelabor.setLaborEstimatedhourlybasecost(dfcur
							.format(rs.getFloat(10)) + "");
					resourcelabor.setLaborEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					resourcelabor.setLaborProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					resourcelabor.setLaborPriceunit(dfcur.format(rs
							.getFloat(13)) + "");
					resourcelabor.setLaborPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					resourcelabor.setLaborSellablequantity(rs.getString(15));
					resourcelabor.setLaborMinimumquantity(rs.getString(16));
					resourcelabor.setLaborStatus(rs.getString(17));
					resourcelabor.setResourceType(rs.getString(25));
					resourcelabor
							.setLaborTransit(rs.getString("lm_rs_transit"));
					resourcelabor.setLaborFlag(rs.getString("flag"));
					resourcelabor.setFlag_laborid(rs.getString("flag")
							+ rs.getString(1));

					if (resourcelabor.getLaborid().indexOf("~") > 0) {
						resourcelabor
								.setLaborid(resourcelabor.getLaborid()
										.substring(
												0,
												resourcelabor.getLaborid()
														.indexOf("~")));
						resourcelabor
								.setClr_detail_id(resourcelabor.getLaborid()
										.substring(
												resourcelabor.getLaborid()
														.indexOf("~") + 1,
												resourcelabor.getLaborid()
														.length()));
					} else {
						resourcelabor.setLaborid(resourcelabor.getLaborid());
						resourcelabor.setClr_detail_id("0");
					}
					resourcelabor.setTroubleRepairFlag(rs
							.getString("lm_rs_trouble_and_repair_flag"));
					resourcelist.add(resourcelabor);
				}
			}

			if (type.equals("F") || type.equals("IF") || type.equals("AF")
					|| type.equals("CF")) {
				ResourceFreight resourcefreight = null;
				while (rs.next()) {
					resourcefreight = new ResourceFreight();
					resourcefreight.setFreightid(rs.getString(1));
					resourcefreight.setFreightcostlibid(rs.getString(2));
					resourcefreight.setActivity_Id(rs.getString(3));
					resourcefreight.setFreightname(rs.getString(18) + "-"
							+ rs.getString(4));
					if (resourcefreight.getFreightname().length() > 45) {
						resourcefreight.setFreightname(resourcefreight
								.getFreightname().toString().substring(0, 45));
					}
					resourcefreight.setFreighttype(rs.getString(5));
					resourcefreight.setFreightCnspartnumber(rs.getString(8));
					resourcefreight.setFreightQuantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcefreight.setFreightPrevquantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcefreight.setFreightEstimatedunitcost(dfcur.format(rs
							.getFloat(10)) + "");
					resourcefreight.setFreightEstimatedtotalcost(dfcur
							.format(rs.getFloat(11)) + "");
					resourcefreight.setFreightProformamargin(dfcurmargin
							.format(rs.getFloat(12)) + "");
					resourcefreight.setFreightPriceunit(dfcur.format(rs
							.getFloat(13)) + "");
					resourcefreight.setFreightPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					resourcefreight
							.setFreightSellablequantity(rs.getString(15));
					resourcefreight.setFreightMinimumquantity(rs.getString(16));
					resourcefreight.setFreightStatus(rs.getString(17));
					resourcefreight.setResourceType(rs.getString(25));
					resourcefreight.setFreightFlag(rs.getString("flag"));
					resourcefreight.setFlag_freightid(rs.getString("flag")
							+ rs.getString(1));
					resourcelist.add(resourcefreight);
				}
			}

			if (type.equals("T") || type.equals("IT") || type.equals("AT")
					|| type.equals("CT")) {
				ResourceTravel resourcetravel = null;
				while (rs.next()) {
					resourcetravel = new ResourceTravel();
					resourcetravel.setTravelid(rs.getString(1));
					resourcetravel.setTravelcostlibid(rs.getString(2));
					resourcetravel.setActivity_Id(rs.getString(3));
					resourcetravel.setTravelname(rs.getString(18) + "-"
							+ rs.getString(4));
					if (resourcetravel.getTravelname().length() > 45) {
						resourcetravel.setTravelname(resourcetravel
								.getTravelname().toString().substring(0, 45));
					}
					resourcetravel.setTraveltype(rs.getString(6));
					resourcetravel.setTravelCnspartnumber(rs.getString(8));
					resourcetravel.setTravelQuantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcetravel.setTravelPrevquantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcetravel.setTravelEstimatedunitcost(dfcur.format(rs
							.getFloat(10)) + "");
					resourcetravel.setTravelEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					resourcetravel.setTravelProformamargin(dfcurmargin
							.format(rs.getFloat(12)) + "");
					resourcetravel.setTravelPriceunit(dfcur.format(rs
							.getFloat(13)) + "");
					resourcetravel.setTravelPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					resourcetravel.setTravelSellablequantity(rs.getString(15));
					resourcetravel.setTravelMinimumquantity(rs.getString(16));
					resourcetravel.setTravelStatus(rs.getString(17));
					resourcetravel.setResourceType(rs.getString(25));
					resourcetravel.setTravelFlag(rs.getString("flag"));
					resourcetravel.setFlag_travelid(rs.getString("flag")
							+ rs.getString(1));
					resourcelist.add(resourcetravel);
				}
			}

		} catch (Exception e) {
			logger.error(
					"getResourcelist(String, String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourcelist(String, String, String, String, String, String, DataSource) - Exception caught in rerieving material resource list"
						+ e.getMessage());
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getResourcelist(String, String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();

				if (pStmt != null)
					pStmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getResourcelist(String, String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getResourcelist(String, String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("getResourcelist(String, String, String, String, String, String, DataSource) - **** getResourceList :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return resourcelist;
	}

	public static int addmaterialresourceNew(MaterialEditBean materialeditform,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;
		/*
		 * System.out.println("2	"+ "0" ); System.out.println("3	"+ "0" );
		 * System.out.println("4	"+materialeditform.getMaterialid() );
		 * System.out.println("5	" +Integer.parseInt(
		 * materialeditform.getActivity_Id() ) ); System.out.println("6	"
		 * +materialeditform.getCnspartnumber() ); System.out.println("7 	"
		 * +Float.parseFloat( materialeditform.getQuantity() ) );
		 * System.out.println("8 	" +"0" ); System.out.println("9 	"
		 * +materialeditform.getMaterialType() ); System.out.println("10 	"
		 * +Float.parseFloat( materialeditform.getEstimatedunitcost() ) );
		 * System.out.println("11 	" + Float.parseFloat(
		 * materialeditform.getProformamargin() ) ); System.out.println("12 	"
		 * +"D" ); System.out.println("13 	" +Integer.parseInt ( loginuserid )
		 * ); System.out.println("14	" +"A" ); System.out.println("15	" +
		 * materialeditform.getAddendum_id() );
		 */
		try {
			conn = ds.getConnection();

			if (materialeditform.getMaterialType().equals("M")
					|| materialeditform.getMaterialType().equals("IM")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);
			cstmt.setInt(4, Integer.parseInt(materialeditform.getMaterialid()));
			cstmt.setInt(5, Integer.parseInt(materialeditform.getActivity_Id()));
			cstmt.setString(6, materialeditform.getCnspartnumber());
			cstmt.setFloat(7, Float.parseFloat(materialeditform.getQuantity()));
			cstmt.setFloat(8, 0);
			cstmt.setString(9, materialeditform.getMaterialType());
			cstmt.setFloat(10,
					Float.parseFloat(materialeditform.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(materialeditform.getProformamargin()));
			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");
			cstmt.setString(15, materialeditform.getAddendum_id());
			/*
			 * System.out.println( "Query  0 , 0 , "+Integer.parseInt(
			 * materialeditform.getMaterialid() )+" , " + ""+Integer.parseInt(
			 * materialeditform.getActivity_Id()
			 * )+" , '"+materialeditform.getCnspartnumber()+"' , " +
			 * "'"+Float.parseFloat( materialeditform.getQuantity()
			 * )+"' , 0 , '"+materialeditform.getMaterialType()+"' , " +
			 * ""+Float.parseFloat( materialeditform.getEstimatedunitcost()
			 * )+" , " + ""+Float.parseFloat(
			 * materialeditform.getProformamargin() )+" , 'D' , " +
			 * ""+Integer.parseInt ( loginuserid
			 * )+" , 'A' , "+materialeditform.getAddendum_id()+" ");
			 */
			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addmaterialresourceNew(MaterialEditForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addmaterialresourceNew(MaterialEditForm, String, DataSource) - Error Occured in Inserting Material Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addmaterialresourceNew(MaterialEditForm, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addmaterialresourceNew(MaterialEditForm, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int addlaborresourceNew(LaborEditBean laboreditaction,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			if (laboreditaction.getLabortype().equals("L")
					|| laboreditaction.getLabortype().equals("IL")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			/*
			 * System.out.println("laborresource.getLabortype()"+laboreditaction.
			 * getLabortype());
			 * System.out.println("laborresource.getLaborid()  "
			 * +Integer.parseInt( laboreditaction.getLaborid() ) );
			 * System.out.println
			 * ("laborresource.getActivity_Id() "+Integer.parseInt(
			 * laboreditaction.getActivity_Id() ) );
			 * System.out.println("laborresource.getCnspartnumber() "
			 * +laboreditaction.getCnspartnumber() );
			 * System.out.println("laborresource.getQuantityhours() "
			 * +Float.parseFloat( laboreditaction.getQuantityhours() ) );
			 * System.
			 * out.println("laborresource.getLabortype() "+laboreditaction
			 * .getLabortype() );
			 * System.out.println("laborresource.getEstimatedhourlybasecost()"
			 * +Float.parseFloat( laboreditaction.getEstimatedhourlybasecost()
			 * ));
			 * System.out.println("laborresource.getProformamargin() "+Float.
			 * parseFloat( laboreditaction.getProformamargin() ) );
			 * System.out.println("loginuserid ) "+Integer.parseInt (
			 * loginuserid ) );
			 * System.out.println("laborresource.getAddendum_id() "
			 * +laboreditaction.getAddendum_id() );
			 * System.out.println("laborresource.getTransit() "
			 * +laboreditaction.getTransit() );
			 */

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);
			cstmt.setInt(4, Integer.parseInt(laboreditaction.getLaborid()));
			cstmt.setInt(5, Integer.parseInt(laboreditaction.getActivity_Id()));
			cstmt.setString(6, laboreditaction.getCnspartnumber());
			cstmt.setFloat(7,
					Float.parseFloat(laboreditaction.getQuantityhours()));
			cstmt.setFloat(8, 0);
			cstmt.setString(9, laboreditaction.getLabortype());
			cstmt.setFloat(10, Float.parseFloat(laboreditaction
					.getEstimatedhourlybasecost()));
			cstmt.setFloat(11,
					Float.parseFloat(laboreditaction.getProformamargin()));
			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");
			cstmt.setString(15, laboreditaction.getAddendum_id());
			cstmt.setString(16, laboreditaction.getTransit());

			if (laboreditaction.getLabortype().equals("L")
					|| laboreditaction.getLabortype().equals("IL")) {
				cstmt.setString(17, laboreditaction.getClr_detail_id());
			}
			/*
			 * System.out.println( "Query  0 , 0 , "+Integer.parseInt(
			 * laborresource.getLaborid() )+" , " + ""+Integer.parseInt(
			 * laborresource.getActivity_Id()
			 * )+" , '"+laborresource.getCnspartnumber()+"' , " +
			 * "'"+Float.parseFloat( laborresource.getQuantityhours()
			 * )+"' , 0 , '"+laborresource.getLabortype()+"' , " +
			 * ""+Float.parseFloat( laborresource.getEstimatedhourlybasecost()
			 * )+" , " + ""+Float.parseFloat( laborresource.getProformamargin()
			 * )+" , 'D' , " + ""+Integer.parseInt ( loginuserid
			 * )+" , 'A' , "+laborresource.getAddendum_id()+" ");
			 */

			// System.out.println("in addlaborresource laborresource.getTransit()"+laborresource.getTransit());

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addlaborresourceNew(LaborEditForm, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addlaborresourceNew(LaborEditForm, String, DataSource) - Error Occured in Inserting Labor Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addlaborresourceNew(LaborEditForm, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addlaborresourceNew(LaborEditForm, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	// for resources add in main table:Start
	public static int addResources(String resourceid, String activityid,
			String cnspartnumber, String quantity, String resourcetype,
			String estimatedcost, String proformamargin, String loginuserid,
			String addendum_id, String transit, String clr_detail_id,
			DataSource ds) {
		/*
		 * System.out.println("2	"+"0"); System.out.println("3	"+"0");
		 * System.out.println("4	"+resourceid);
		 * System.out.println("5	"+activityid);
		 * System.out.println("6	"+cnspartnumber);
		 * System.out.println("7	"+quantity); System.out.println("8	"+"0");
		 * System.out.println("9	"+resourcetype);
		 * System.out.println("10	"+estimatedcost);
		 * System.out.println("11	"+proformamargin);
		 * System.out.println("12	"+"D"); System.out.println("13	"+loginuserid);
		 * System.out.println("14	"+"A"); System.out.println("15	"+addendum_id);
		 * System.out.println("transit	=	"+transit);
		 * System.out.println("clr_detail_id	=	"+clr_detail_id);
		 */

		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;
		java.util.Date dateIn = new java.util.Date();
		try {
			conn = ds.getConnection();

			if (resourcetype.equals("L") || resourcetype.equals("IL")) {

				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			} else {

				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);
			cstmt.setInt(4, Integer.parseInt(resourceid));
			cstmt.setInt(5, Integer.parseInt(activityid));
			cstmt.setString(6, cnspartnumber);
			cstmt.setFloat(7, Float.parseFloat(quantity));
			cstmt.setFloat(8, 0);
			cstmt.setString(9, resourcetype.trim());
			cstmt.setFloat(10, Float.parseFloat(estimatedcost));
			cstmt.setFloat(11, Float.parseFloat(proformamargin));
			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");
			cstmt.setString(15, addendum_id);
			if (resourcetype.equals("L") || resourcetype.equals("IL")) {
				cstmt.setString(16, transit);
				cstmt.setString(17, clr_detail_id);
			}

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addResources(String, String, String, String, String, String, String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addResources(String, String, String, String, String, String, String, String, String, String, String, DataSource) - Error Occured in Inserting Resources"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addResources(String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addResources(String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						sql);
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("addResources(String, String, String, String, String, String, String, String, String, String, String, DataSource) - **** addResources :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}

		return val;
	}

	// for resources add in main table:end

	public static int addtravelresourceNew(TravelEditBean traveleditform,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			if (traveleditform.getTraveltype().equals("T")
					|| traveleditform.getTraveltype().equals("IT")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);
			cstmt.setInt(4, Integer.parseInt(traveleditform.getTravelid()));
			cstmt.setInt(5, Integer.parseInt(traveleditform.getActivity_Id()));
			cstmt.setString(6, traveleditform.getCnspartnumber());
			cstmt.setFloat(7, Float.parseFloat(traveleditform.getQuantity()));
			cstmt.setFloat(8, 0);
			cstmt.setString(9, traveleditform.getTraveltype());
			cstmt.setFloat(10,
					Float.parseFloat(traveleditform.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(traveleditform.getProformamargin()));
			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");
			cstmt.setString(15, traveleditform.getAddendum_id());

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addtravelresourceNew(TravelEditForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addtravelresourceNew(TravelEditForm, String, DataSource) - Error Occured in Inserting  Travel Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addtravelresourceNew(TravelEditForm, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addtravelresourceNew(TravelEditForm, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int addfreightresourceNew(FreightEditBean freighteditform,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			if (freighteditform.getFreighttype().equals("F")
					|| freighteditform.getFreighttype().equals("IF")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);
			cstmt.setInt(4, Integer.parseInt(freighteditform.getFreightid()));
			cstmt.setInt(5, Integer.parseInt(freighteditform.getActivity_Id()));
			cstmt.setString(6, freighteditform.getCnspartnumber());
			cstmt.setFloat(7, Float.parseFloat(freighteditform.getQuantity()));
			cstmt.setFloat(8, 0);
			cstmt.setString(9, freighteditform.getFreighttype());
			cstmt.setFloat(10,
					Float.parseFloat(freighteditform.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(freighteditform.getProformamargin()));
			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");
			cstmt.setString(15, freighteditform.getAddendum_id());

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addfreightresourceNew(FreightEditForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addfreightresourceNew(FreightEditForm, String, DataSource) - Error Occured in Inserting Freight"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addfreightresourceNew(FreightEditForm, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addfreightresourceNew(FreightEditForm, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int updatematerialresourceNew(
			MaterialEditBean materialreditform, String loginuserid,
			DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;
		/*
		 * System.out.println( "2	"+materialreditform.getMaterialid() ) ;
		 * System.out.println( "3	"+materialreditform.getMaterialcostlibid() ) ;
		 * System.out.println( "4	"+0 ); System.out.println(
		 * "5	"+materialreditform.getActivity_Id() ) ; System.out.println( "6	"+
		 * "" ); System.out.println( "7	"+materialreditform.getQuantity() ) ;
		 * System.out.println( "8	"+materialreditform.getPrevquantity() ) ;
		 * System.out.println( "9	"+materialreditform.getMaterialType() );
		 * System.out.println( "10	"+materialreditform.getEstimatedunitcost() )
		 * ; System.out.println( "11	"+ materialreditform.getProformamargin() )
		 * ; System.out.println( "12	"+ "" ); System.out.println(
		 * "13	"+loginuserid ) ; System.out.println( "14	"+"U" );
		 * System.out.println( "15	"+ materialreditform.getAddendum_id() );
		 */

		try {
			conn = ds.getConnection();

			if (materialreditform.getMaterialType().equals("M")
					|| materialreditform.getMaterialType().equals("IM")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(materialreditform.getMaterialid()));
			cstmt.setInt(3,
					Integer.parseInt(materialreditform.getMaterialcostlibid()));
			cstmt.setInt(4, 0);
			cstmt.setInt(5,
					Integer.parseInt(materialreditform.getActivity_Id()));

			cstmt.setString(6, "");
			cstmt.setFloat(7, Float.parseFloat(materialreditform.getQuantity()));
			cstmt.setFloat(8,
					Float.parseFloat(materialreditform.getPrevquantity()));
			cstmt.setString(9, materialreditform.getMaterialType());
			cstmt.setFloat(10,
					Float.parseFloat(materialreditform.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(materialreditform.getProformamargin()));
			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");
			cstmt.setString(15, materialreditform.getAddendum_id());

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updatematerialresourceNew(MaterialEditForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatematerialresourceNew(MaterialEditForm, String, DataSource) - Error Occured in Updating Material Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatematerialresourceNew(MaterialEditForm, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatematerialresourceNew(MaterialEditForm, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int updatelaborresourceNew(LaborEditBean laboreditform,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		/*
		 * System.out.println(" 2 "+ Integer.parseInt(
		 * laboreditform.getLaborid() ) ); System.out.println(" 3 "+
		 * Integer.parseInt( laboreditform.getLaborcostlibid() ) );
		 * System.out.println(" 4 "+ 0 ); System.out.println(" 5 "+
		 * Integer.parseInt( laboreditform.getActivity_Id() ) );
		 * System.out.println(" 6 "+ "" ); System.out.println(" 7 "+
		 * Float.parseFloat( laboreditform.getQuantityhours() ) );
		 * System.out.println(" 8 "+ Float.parseFloat(
		 * laboreditform.getPrevquantityhours() ) ); System.out.println(" 9 "+
		 * laboreditform.getLabortype() ); System.out.println(" 10 "+
		 * Float.parseFloat( laboreditform.getEstimatedhourlybasecost() ) );
		 * System.out.println(" 11 "+ Float.parseFloat(
		 * laboreditform.getProformamargin() ) ); System.out.println(" 12 "+ ""
		 * ); System.out.println(" 13 "+ Integer.parseInt ( loginuserid ) );
		 * System.out.println(" 14 "+ "U" ); System.out.println(" 15 "+
		 * laboreditform.getAddendum_id() ); //for transit field
		 * //System.out.println
		 * ("Transit value to be saved "+laboreditform.getTransit());
		 * if(laboreditform.getTransit()!=null) { System.out.println(" 16 "+
		 * laboreditform.getTransit() ); } else { System.out.println(" 16 "+
		 * "N"); }
		 * 
		 * if( laboreditform.getLabortype().equals( "L" ) ||
		 * laboreditform.getLabortype().equals( "IL" ) ) {
		 * System.out.println(" 17 "+ laboreditform.getClr_detail_id()); }
		 */

		try {
			conn = ds.getConnection();

			if (laboreditform.getLabortype().equals("L")
					|| laboreditform.getLabortype().equals("IL")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(laboreditform.getLaborid()));
			cstmt.setInt(3, Integer.parseInt(laboreditform.getLaborcostlibid()));
			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(laboreditform.getActivity_Id()));

			cstmt.setString(6, "");
			cstmt.setFloat(7,
					Float.parseFloat(laboreditform.getQuantityhours()));
			cstmt.setFloat(8,
					Float.parseFloat(laboreditform.getPrevquantityhours()));

			cstmt.setString(9, laboreditform.getLabortype());

			cstmt.setFloat(10, Float.parseFloat(laboreditform
					.getEstimatedhourlybasecost()));
			cstmt.setFloat(11,
					Float.parseFloat(laboreditform.getProformamargin()));

			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");
			cstmt.setString(15, laboreditform.getAddendum_id());
			// for transit field
			// System.out.println("Transit value to be saved "+laboreditform.getTransit());
			if (laboreditform.getTransit() != null) {
				cstmt.setString(16, laboreditform.getTransit());
			} else {
				cstmt.setString(16, "N");
			}

			if (laboreditform.getLabortype().equals("L")
					|| laboreditform.getLabortype().equals("IL")) {
				cstmt.setString(17, laboreditform.getClr_detail_id());
			}

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updatelaborresourceNew(LaborEditForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatelaborresourceNew(LaborEditForm, String, DataSource) - Error Occured in Updating Labor Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatelaborresourceNew(LaborEditForm, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatelaborresourceNew(LaborEditForm, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int updatefreightresourceNew(FreightEditBean freighteditform,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;
		/*
		 * System.out.println(" 2 "+ Integer.parseInt(
		 * freighteditform.getFreightid() ) ); System.out.println(" 3 "+
		 * Integer.parseInt( freighteditform.getFreightcostlibid() ) );
		 * System.out.println(" 4 "+ 0 ); System.out.println(" 5 "+
		 * Integer.parseInt( freighteditform.getActivity_Id() ) );
		 * System.out.println(" 6 "+ "" ); System.out.println(" 7 "+
		 * Float.parseFloat( freighteditform.getQuantity() ) );
		 * System.out.println(" 8 "+ Float.parseFloat(
		 * freighteditform.getPrevquantity() ) ); System.out.println(" 9 "+
		 * freighteditform.getFreighttype() ); System.out.println(" 10 "+
		 * Float.parseFloat( freighteditform.getEstimatedunitcost() ) );
		 * System.out.println(" 11 "+ Float.parseFloat(
		 * freighteditform.getProformamargin() ) ); System.out.println(" 12 "+
		 * "" ); System.out.println(" 13 "+ Integer.parseInt ( loginuserid ) );
		 * System.out.println(" 14 "+ "U" ); System.out.println(" 15 "+
		 * freighteditform.getAddendum_id() );
		 */
		try {
			conn = ds.getConnection();

			if (freighteditform.getFreighttype().equals("F")
					|| freighteditform.getFreighttype().equals("IF")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(freighteditform.getFreightid()));
			cstmt.setInt(3,
					Integer.parseInt(freighteditform.getFreightcostlibid()));
			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(freighteditform.getActivity_Id()));
			cstmt.setString(6, "");
			cstmt.setFloat(7, Float.parseFloat(freighteditform.getQuantity()));
			cstmt.setFloat(8,
					Float.parseFloat(freighteditform.getPrevquantity()));
			cstmt.setString(9, freighteditform.getFreighttype());
			cstmt.setFloat(10,
					Float.parseFloat(freighteditform.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(freighteditform.getProformamargin()));
			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");
			cstmt.setString(15, freighteditform.getAddendum_id());

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updatefreightresourceNew(FreightEditForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatefreightresourceNew(FreightEditForm, String, DataSource) - Error Occured in updating Freight"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatefreightresourceNew(FreightEditForm, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatefreightresourceNew(FreightEditForm, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int updatetravelresourceNew(TravelEditBean traveleditform,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;
		/*
		 * System.out.println(" 2 "+ traveleditform.getTravelid() ) ;
		 * System.out.println(" 3 "+ traveleditform.getTravelcostlibid() ) ;
		 * System.out.println(" 4 "+ 0 ); System.out.println(" 5 "+
		 * traveleditform.getActivity_Id() ) ; System.out.println(" 6 "+ "" );
		 * System.out.println(" 7 "+ traveleditform.getQuantity() ) ;
		 * System.out.println(" 8 "+ traveleditform.getPrevquantity() ) ;
		 * System.out.println(" 9 "+ traveleditform.getTraveltype() );
		 * System.out.println(" 10 "+ traveleditform.getEstimatedunitcost() ) ;
		 * System.out.println(" 11 "+ traveleditform.getProformamargin() ) ;
		 * System.out.println(" 12 "+ "" ); System.out.println(" 13 "+
		 * loginuserid ) ; System.out.println(" 14 "+ "U" );
		 * System.out.println(" 15 "+ traveleditform.getAddendum_id() );
		 */

		try {
			conn = ds.getConnection();

			if (traveleditform.getTraveltype().equals("T")
					|| traveleditform.getTraveltype().equals("IT")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(traveleditform.getTravelid()));
			cstmt.setInt(3,
					Integer.parseInt(traveleditform.getTravelcostlibid()));
			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(traveleditform.getActivity_Id()));
			cstmt.setString(6, "");
			cstmt.setFloat(7, Float.parseFloat(traveleditform.getQuantity()));
			cstmt.setFloat(8,
					Float.parseFloat(traveleditform.getPrevquantity()));
			cstmt.setString(9, traveleditform.getTraveltype());
			cstmt.setFloat(10,
					Float.parseFloat(traveleditform.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(traveleditform.getProformamargin()));
			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");
			cstmt.setString(15, traveleditform.getAddendum_id());

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updatetravelresourceNew(TravelEditForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatetravelresourceNew(TravelEditForm, String, DataSource) - Error Occured in UPDATING Travel Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatetravelresourceNew(TravelEditForm, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatetravelresourceNew(TravelEditForm, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	// add for UI:End
	public static LaborResource getLaborResource(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		LaborResource laborresource = new LaborResource();

		ArrayList laborlist = new java.util.ArrayList();

		laborlist.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select iv_ct_name , iv_rp_cns_part_number , iv_cost , iv_rp_sell_quantity , "
							+ "iv_rp_minimum_quantity from lx_resource_hierarchy_01 where iv_ct_type_desc like 'labor%' and iv_rp_id="
							+ Id);

			/*
			 * System.out.println(
			 * "query in get labor:::::select iv_ct_name , iv_rp_cns_part_number , iv_cost , iv_rp_sell_quantity , "
			 * +
			 * "iv_rp_minimum_quantity from lx_resource_hierarchy_01 where iv_ct_type_desc like 'labor%' and iv_rp_id="
			 * +Id);
			 */
			if (rs.next()) {
				laborresource.setLabortype(rs.getString(1));

				laborresource.setCnspartnumber(rs.getString(2));
				laborresource.setEstimatedhourlybasecost(rs.getString(3));
				laborresource.setSellablequantity(rs.getString(4));
				laborresource.setMinimumquantity(rs.getString(5));
			}
		} catch (Exception e) {
			logger.error("getLaborResource(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getLaborResource(String, DataSource) - Exception caught in rerieving laborresource"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getLaborResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getLaborResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getLaborResource(String, DataSource) - exception ignored",
						sql);
			}

		}
		return laborresource;
	}

	public static int addlaborresource(LaborResource laborresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			if (laborresource.getLabortype().equals("L")
					|| laborresource.getLabortype().equals("IL")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			/*
			 * System.out.println(
			 * "ZZZZZ1 in addlaborresource laborresource.getLabortype()"
			 * +laborresource.getLabortype()); System.out.println(
			 * "ZZZZZ2 in addlaborresource Integer.parseInt( laborresource.getLaborid() ) "
			 * +Integer.parseInt( laborresource.getLaborid() ) );
			 * System.out.println(
			 * "ZZZZZ3 in addlaborresource Integer.parseInt( laborresource.getActivity_Id() ) "
			 * +Integer.parseInt( laborresource.getActivity_Id() ) );
			 * System.out.println(
			 * "ZZZZZ4 in addlaborresource laborresource.getCnspartnumber() "
			 * +laborresource.getCnspartnumber() ); System.out.println(
			 * "ZZZZZ5 in addlaborresource Float.parseFloat( laborresource.getQuantityhours() ) "
			 * +Float.parseFloat( laborresource.getQuantityhours() ) );
			 * System.out
			 * .println("ZZZZZ6 in addlaborresource laborresource.getLabortype() "
			 * +laborresource.getLabortype() ); System.out.println(
			 * "ZZZZZ7 in addlaborresource Float.parseFloat( laborresource.getEstimatedhourlybasecost() )"
			 * +Float.parseFloat( laborresource.getEstimatedhourlybasecost() ));
			 * System.out.println(
			 * "ZZZZZ8 in addlaborresource Float.parseFloat( laborresource.getProformamargin() ) "
			 * +Float.parseFloat( laborresource.getProformamargin() ) );
			 * System.out.println(
			 * "ZZZZZ9 in addlaborresource Integer.parseInt ( loginuserid ) "
			 * +Integer.parseInt ( loginuserid ) ); System.out.println(
			 * "ZZZZZ10 in addlaborresource laborresource.getAddendum_id() "
			 * +laborresource.getAddendum_id() ); System.out.println(
			 * "ZZZZZ11 in addlaborresource laborresource.getTransit() "
			 * +laborresource.getTransit() );
			 */

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);
			cstmt.setInt(4, Integer.parseInt(laborresource.getLaborid()));
			cstmt.setInt(5, Integer.parseInt(laborresource.getActivity_Id()));

			cstmt.setString(6, laborresource.getCnspartnumber());
			cstmt.setFloat(7,
					Float.parseFloat(laborresource.getQuantityhours()));
			cstmt.setFloat(8, 0);

			cstmt.setString(9, laborresource.getLabortype());

			cstmt.setFloat(10, Float.parseFloat(laborresource
					.getEstimatedhourlybasecost()));
			cstmt.setFloat(11,
					Float.parseFloat(laborresource.getProformamargin()));

			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");
			cstmt.setString(15, laborresource.getAddendum_id());
			cstmt.setString(16, laborresource.getTransit());

			if (laborresource.getLabortype().equals("L")
					|| laborresource.getLabortype().equals("IL")) {
				cstmt.setString(17, laborresource.getClr_detail_id());
			}
			/*
			 * System.out.println( "Query  0 , 0 , "+Integer.parseInt(
			 * laborresource.getLaborid() )+" , " + ""+Integer.parseInt(
			 * laborresource.getActivity_Id()
			 * )+" , '"+laborresource.getCnspartnumber()+"' , " +
			 * "'"+Float.parseFloat( laborresource.getQuantityhours()
			 * )+"' , 0 , '"+laborresource.getLabortype()+"' , " +
			 * ""+Float.parseFloat( laborresource.getEstimatedhourlybasecost()
			 * )+" , " + ""+Float.parseFloat( laborresource.getProformamargin()
			 * )+" , 'D' , " + ""+Integer.parseInt ( loginuserid
			 * )+" , 'A' , "+laborresource.getAddendum_id()+" ");
			 */

			// System.out.println("in addlaborresource laborresource.getTransit()"+laborresource.getTransit());

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("addlaborresource(LaborResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addlaborresource(LaborResource, String, DataSource) - Error Occured in Inserting Labor Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addlaborresource(LaborResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addlaborresource(LaborResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int updatelaborresource(LaborResource laborresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			if (laborresource.getLabortype().equals("L")
					|| laborresource.getLabortype().equals("IL")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(laborresource.getLaborid()));
			cstmt.setInt(3, Integer.parseInt(laborresource.getLaborcostlibid()));
			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(laborresource.getActivity_Id()));

			cstmt.setString(6, "");
			cstmt.setFloat(7,
					Float.parseFloat(laborresource.getQuantityhours()));
			cstmt.setFloat(8,
					Float.parseFloat(laborresource.getPrevquantityhours()));

			cstmt.setString(9, laborresource.getLabortype());

			cstmt.setFloat(10, Float.parseFloat(laborresource
					.getEstimatedhourlybasecost()));
			cstmt.setFloat(11,
					Float.parseFloat(laborresource.getProformamargin()));

			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");
			cstmt.setString(15, laborresource.getAddendum_id());
			// for transit field
			// System.out.println("Transit value to be saved "+laborresource.getTransit());
			if (laborresource.getTransit() != null) {
				cstmt.setString(16, laborresource.getTransit());
			} else {
				cstmt.setString(16, "N");
			}

			if (laborresource.getLabortype().equals("L")
					|| laborresource.getLabortype().equals("IL")) {
				cstmt.setString(17, laborresource.getClr_detail_id());
			}

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updatelaborresource(LaborResource, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatelaborresource(LaborResource, String, DataSource) - Error Occured in Updating Labor Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatelaborresource(LaborResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatelaborresource(LaborResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static FreightResource getFreightResource(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		FreightResource freightresource = new FreightResource();

		ArrayList freightlist = new java.util.ArrayList();

		freightlist.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select iv_ct_name , iv_rp_cns_part_number , iv_cost , iv_rp_sell_quantity , "
							+ "iv_rp_minimum_quantity from lx_resource_hierarchy_01 where iv_ct_type_desc like 'freight%' and iv_rp_id="
							+ Id);

			if (rs.next()) {
				freightresource.setFreighttype(rs.getString(1));

				freightresource.setCnspartnumber(rs.getString(2));
				freightresource.setEstimatedunitcost(rs.getString(3));
				freightresource.setSellablequantity(rs.getString(4));
				freightresource.setMinimumquantity(rs.getString(5));
			}
		} catch (Exception e) {
			logger.error("getFreightResource(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getFreightResource(String, DataSource) - Exception caught in rerieving freightresource"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getFreightResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getFreightResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getFreightResource(String, DataSource) - exception ignored",
						sql);
			}

		}
		return freightresource;
	}

	public static int addfreightresource(FreightResource freightresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			if (freightresource.getFreighttype().equals("F")
					|| freightresource.getFreighttype().equals("IF")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);

			cstmt.setInt(4, Integer.parseInt(freightresource.getFreightid()));
			cstmt.setInt(5, Integer.parseInt(freightresource.getActivity_Id()));

			cstmt.setString(6, freightresource.getCnspartnumber());
			cstmt.setFloat(7, Float.parseFloat(freightresource.getQuantity()));
			cstmt.setFloat(8, 0);

			cstmt.setString(9, freightresource.getFreighttype());

			cstmt.setFloat(10,
					Float.parseFloat(freightresource.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(freightresource.getProformamargin()));

			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");
			cstmt.setString(15, freightresource.getAddendum_id());

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addfreightresource(FreightResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addfreightresource(FreightResource, String, DataSource) - Error Occured in Inserting Freight"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addfreightresource(FreightResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addfreightresource(FreightResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int updatefreightresource(FreightResource freightresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			if (freightresource.getFreighttype().equals("F")
					|| freightresource.getFreighttype().equals("IF")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(freightresource.getFreightid()));
			cstmt.setInt(3,
					Integer.parseInt(freightresource.getFreightcostlibid()));

			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(freightresource.getActivity_Id()));

			cstmt.setString(6, "");
			cstmt.setFloat(7, Float.parseFloat(freightresource.getQuantity()));
			cstmt.setFloat(8,
					Float.parseFloat(freightresource.getPrevquantity()));

			cstmt.setString(9, freightresource.getFreighttype());

			cstmt.setFloat(10,
					Float.parseFloat(freightresource.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(freightresource.getProformamargin()));

			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");
			cstmt.setString(15, freightresource.getAddendum_id());

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updatefreightresource(FreightResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatefreightresource(FreightResource, String, DataSource) - Error Occured in updating Freight"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatefreightresource(FreightResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatefreightresource(FreightResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static TravelResource getTravelResource(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		TravelResource travelresource = new TravelResource();

		ArrayList travellist = new java.util.ArrayList();

		travellist.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select iv_ct_name , iv_rp_cns_part_number , iv_cost , iv_rp_sell_quantity , "
							+ "iv_rp_minimum_quantity from lx_resource_hierarchy_01 where iv_ct_type_desc like 'travel%' and iv_rp_id="
							+ Id);

			if (rs.next()) {
				travelresource.setTraveltype(rs.getString(1));

				travelresource.setCnspartnumber(rs.getString(2));
				travelresource.setEstimatedunitcost(rs.getString(3));
				travelresource.setSellablequantity(rs.getString(4));
				travelresource.setMinimumquantity(rs.getString(5));
			}
		} catch (Exception e) {
			logger.error("getTravelResource(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTravelResource(String, DataSource) - Exception caught in rerieving travelresource"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getTravelResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getTravelResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getTravelResource(String, DataSource) - exception ignored",
						sql);
			}

		}
		return travelresource;
	}

	public static int addtravelresource(TravelResource travelresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			if (travelresource.getTraveltype().equals("T")
					|| travelresource.getTraveltype().equals("IT")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);

			cstmt.setInt(4, Integer.parseInt(travelresource.getTravelid()));
			cstmt.setInt(5, Integer.parseInt(travelresource.getActivity_Id()));

			cstmt.setString(6, travelresource.getCnspartnumber());
			cstmt.setFloat(7, Float.parseFloat(travelresource.getQuantity()));
			cstmt.setFloat(8, 0);
			cstmt.setString(9, travelresource.getTraveltype());

			cstmt.setFloat(10,
					Float.parseFloat(travelresource.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(travelresource.getProformamargin()));

			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");
			cstmt.setString(15, travelresource.getAddendum_id());

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addtravelresource(TravelResource, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addtravelresource(TravelResource, String, DataSource) - Error Occured in Inserting  Travel Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addtravelresource(TravelResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addtravelresource(TravelResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int updatetravelresource(TravelResource travelresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			if (travelresource.getTraveltype().equals("T")
					|| travelresource.getTraveltype().equals("IT")) {
				cstmt = conn
						.prepareCall("{?=call dbo.lm_resource_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(travelresource.getTravelid()));
			cstmt.setInt(3,
					Integer.parseInt(travelresource.getTravelcostlibid()));

			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(travelresource.getActivity_Id()));

			cstmt.setString(6, "");
			cstmt.setFloat(7, Float.parseFloat(travelresource.getQuantity()));
			cstmt.setFloat(8,
					Float.parseFloat(travelresource.getPrevquantity()));

			cstmt.setString(9, travelresource.getTraveltype());

			cstmt.setFloat(10,
					Float.parseFloat(travelresource.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(travelresource.getProformamargin()));

			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");
			cstmt.setString(15, travelresource.getAddendum_id());

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updatetravelresource(TravelResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatetravelresource(TravelResource, String, DataSource) - Error Occured in UPDATING Travel Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatetravelresource(TravelResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatetravelresource(TravelResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static ArrayList getResourcetabularlist(String activity_Id,
			String type, String resource_id, String querystring,
			String sessionid, String pageFlag, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList resourcelist = new java.util.ArrayList();
		DecimalFormat dfcurmargin = null;
		String sql = "";
		int temp = 0;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurfour = new DecimalFormat("###0.0000");

		if (pageFlag.equals("listpage"))
			dfcurmargin = new DecimalFormat("0.0000000");
		else
			dfcurmargin = new DecimalFormat("0.00");

		if (querystring.equals("")) {
			querystring = "order by iv_rp_name";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select * from lp_resource_tab( " + activity_Id + " , '"
					+ resource_id + "' , '" + type + "' , '" + sessionid
					+ "' )" + querystring;
			// System.out.println("SQL is :::::" + sql);

			rs = stmt.executeQuery(sql);

			if (type.equals("M") || type.equals("IM") || type.equals("AM")
					|| type.equals("CM")) {
				MaterialResource materialresource = null;
				while (rs.next()) {
					materialresource = new MaterialResource();

					materialresource.setMaterialid(rs.getString(1));
					materialresource.setMaterialcostlibid(rs.getString(2));

					materialresource.setActivity_Id(rs.getString(3));

					materialresource.setMaterialname(rs.getString(18) + "-"
							+ rs.getString(4));
					materialresource.setMaterialtype(rs.getString(5));
					materialresource.setManufacturername(rs.getString(6));
					materialresource.setManufacturerpartnumber(rs.getString(7));
					materialresource.setCnspartnumber(rs.getString(8));
					materialresource.setQuantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					materialresource.setPrevquantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					materialresource.setEstimatedunitcost(dfcur.format(rs
							.getFloat(10)) + "");
					materialresource.setEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");

					materialresource.setProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					materialresource.setPriceunit(dfcur.format(rs.getFloat(13))
							+ "");
					materialresource.setPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					materialresource.setSellablequantity(rs.getString(15));
					materialresource.setMinimumquantity(rs.getString(16));
					materialresource.setStatus(rs.getString(17));
					materialresource.setFlag(rs.getString("flag"));
					materialresource.setFlag_materialid(rs.getString("flag")
							+ rs.getString(1));

					resourcelist.add(materialresource);
				}
			}

			if (type.equals("L") || type.equals("IL") || type.equals("AL")
					|| type.equals("CL")) {
				LaborResource laborresource = null;

				while (rs.next()) {
					laborresource = new LaborResource();
					laborresource.setLaborid(rs.getString(1));
					laborresource.setLaborcostlibid(rs.getString(2));
					laborresource.setActivity_Id(rs.getString(3));

					laborresource.setLaborname(rs.getString(18) + "-"
							+ rs.getString(4));
					laborresource.setLabortype(rs.getString(5));

					laborresource.setCnspartnumber(rs.getString(8));

					laborresource.setQuantityhours(dfcurfour.format(rs
							.getFloat(9)) + "");
					laborresource.setPrevquantityhours(dfcurfour.format(rs
							.getFloat(9)) + "");

					laborresource.setEstimatedhourlybasecost(dfcur.format(rs
							.getFloat(10)) + "");
					laborresource.setEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					laborresource.setProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					laborresource.setPriceunit(dfcur.format(rs.getFloat(13))
							+ "");
					laborresource
							.setPriceextended(dfcur.format(rs.getFloat(14))
									+ "");
					laborresource.setSellablequantity(rs.getString(15));
					laborresource.setMinimumquantity(rs.getString(16));
					laborresource.setStatus(rs.getString(17));
					laborresource.setTransit(rs.getString("lm_rs_transit"));
					laborresource.setFlag(rs.getString("flag"));
					laborresource.setFlag_laborid(rs.getString("flag")
							+ rs.getString(1));
					resourcelist.add(laborresource);
				}
			}

			if (type.equals("F") || type.equals("IF") || type.equals("AF")
					|| type.equals("CF")) {
				FreightResource freightresource = null;
				while (rs.next()) {
					freightresource = new FreightResource();
					freightresource.setFreightid(rs.getString(1));
					freightresource.setFreightcostlibid(rs.getString(2));

					freightresource.setActivity_Id(rs.getString(3));

					freightresource.setFreightname(rs.getString(18) + "-"
							+ rs.getString(4));
					freightresource.setFreighttype(rs.getString(5));

					freightresource.setCnspartnumber(rs.getString(8));

					freightresource
							.setQuantity(dfcurfour.format(rs.getFloat(9)) + "");
					freightresource.setPrevquantity(dfcurfour.format(rs
							.getFloat(9)) + "");

					freightresource.setEstimatedunitcost(dfcur.format(rs
							.getFloat(10)) + "");
					freightresource.setEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					freightresource.setProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					freightresource.setPriceunit(dfcur.format(rs.getFloat(13))
							+ "");
					freightresource.setPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					freightresource.setSellablequantity(rs.getString(15));
					freightresource.setMinimumquantity(rs.getString(16));
					freightresource.setStatus(rs.getString(17));
					freightresource.setFlag(rs.getString("flag"));
					freightresource.setFlag_freightid(rs.getString("flag")
							+ rs.getString(1));
					resourcelist.add(freightresource);
				}
			}

			if (type.equals("T") || type.equals("IT") || type.equals("AT")
					|| type.equals("CT")) {
				TravelResource travelresource = null;
				while (rs.next()) {
					travelresource = new TravelResource();
					travelresource.setTravelid(rs.getString(1));
					travelresource.setTravelcostlibid(rs.getString(2));

					travelresource.setActivity_Id(rs.getString(3));

					travelresource.setTravelname(rs.getString(18) + "-"
							+ rs.getString(4));
					travelresource.setTraveltype(rs.getString(5));
					travelresource.setCnspartnumber(rs.getString(8));

					travelresource.setQuantity(dfcurfour.format(rs.getFloat(9))
							+ "");
					travelresource.setPrevquantity(dfcurfour.format(rs
							.getFloat(9)) + "");

					travelresource.setEstimatedunitcost(dfcur.format(rs
							.getFloat(10)) + "");
					travelresource.setEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					travelresource.setProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					travelresource.setPriceunit(dfcur.format(rs.getFloat(13))
							+ "");
					travelresource.setPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					travelresource.setSellablequantity(rs.getString(15));
					travelresource.setMinimumquantity(rs.getString(16));
					travelresource.setStatus(rs.getString(17));
					travelresource.setFlag(rs.getString("flag"));
					travelresource.setFlag_travelid(rs.getString("flag")
							+ rs.getString(1));
					resourcelist.add(travelresource);
				}
			}

		} catch (Exception e) {
			logger.error(
					"getResourcetabularlist(String, String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourcetabularlist(String, String, String, String, String, String, DataSource) - Exception caught in rerieving material resource list"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getResourcetabularlist(String, String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getResourcetabularlist(String, String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getResourcetabularlist(String, String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		return resourcelist;
	}

	public static int getMaterialResourceEdit(
			MaterialEditBean materialeditform, String type, String querystring,
			String sessionid, String pageFlag, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList resourcelist = new java.util.ArrayList();
		DecimalFormat dfcurmargin = null;
		String sql = "";
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurfour = new DecimalFormat("###0.0000");

		if (pageFlag.equals("listpage"))
			dfcurmargin = new DecimalFormat("0.0000000");
		else
			dfcurmargin = new DecimalFormat("0.00");

		if (querystring.equals("")) {
			querystring = "order by iv_rp_name";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select * from lp_resource_tab( "
					+ materialeditform.getActivity_Id() + " , '"
					+ materialeditform.getMaterialid() + "' , '"
					+ materialeditform.getMaterialType() + "' , '" + sessionid
					+ "' )" + querystring;
			rs = stmt.executeQuery(sql);

			if (type.equals("M") || type.equals("IM") || type.equals("AM")
					|| type.equals("CM")) {
				while (rs.next()) {
					materialeditform.setMaterialid(rs.getString(1));
					materialeditform.setMaterialcostlibid(rs.getString(2));
					if (materialeditform.getFlag() == null)
						materialeditform.setActivity_Id(rs.getString(3));
					materialeditform.setMaterialName(rs.getString(18) + "-"
							+ rs.getString(4));
					materialeditform.setMaterialType(rs.getString(5));
					materialeditform.setManufacturername(rs.getString(6));
					materialeditform.setManufacturerpartnumber(rs.getString(7));
					materialeditform.setCnspartnumber(rs.getString(8));
					materialeditform.setQuantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					materialeditform.setPrevquantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					materialeditform.setEstimatedunitcost(dfcur.format(rs
							.getFloat(10)) + "");
					materialeditform.setEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					materialeditform.setProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					materialeditform.setPriceunit(dfcur.format(rs.getFloat(13))
							+ "");
					materialeditform.setPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					materialeditform.setSellablequantity(rs.getString(15));
					materialeditform.setMinimumquantity(rs.getString(16));
					materialeditform.setStatus(rs.getString(17));
					materialeditform.setFlag(rs.getString("flag"));
					materialeditform.setFlag_materialid(rs.getString("flag")
							+ rs.getString(1));
					materialeditform.setCreatedBy(rs
							.getString("lm_rs_created_by"));
					materialeditform.setCreatedDate(rs
							.getString("lm_rs_create_date"));
					materialeditform.setChangedBy(rs
							.getString("lm_rs_changed_by"));
					materialeditform.setChangedDate(rs
							.getString("lm_rs_changed_date"));
				}
			}

		} catch (Exception e) {
			logger.error(
					"getMaterialResourceEdit(MaterialEditForm, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMaterialResourceEdit(MaterialEditForm, String, String, String, String, DataSource) - Exception caught in rerieving material resource list"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getMaterialResourceEdit(MaterialEditForm, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getMaterialResourceEdit(MaterialEditForm, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getMaterialResourceEdit(MaterialEditForm, String, String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		return 1;
	}

	public static int getLaborResourceEdit(LaborEditBean laboreditform,
			String type, String querystring, String sessionid, String pageFlag,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList resourcelist = new java.util.ArrayList();
		DecimalFormat dfcurmargin = null;
		String sql = "";
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurfour = new DecimalFormat("###0.0000");

		if (pageFlag.equals("listpage"))
			dfcurmargin = new DecimalFormat("0.0000000");
		else
			dfcurmargin = new DecimalFormat("0.00");

		if (querystring.equals("")) {
			querystring = "order by iv_rp_name";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select * from lp_resource_tab( "
					+ laboreditform.getActivity_Id() + " , '"
					+ laboreditform.getLaborid() + "' , '"
					+ laboreditform.getLabortype() + "' , '" + sessionid
					+ "' )" + querystring;
			rs = stmt.executeQuery(sql);
			if (type.equals("L") || type.equals("IL") || type.equals("AL")
					|| type.equals("CL")) {
				while (rs.next()) {
					laboreditform.setLaborid(rs.getString(1));
					laboreditform.setLaborcostlibid(rs.getString(2));
					if (laboreditform.getFlag() == null)
						laboreditform.setActivity_Id(rs.getString(3));
					laboreditform.setLaborname(rs.getString(18) + "-"
							+ rs.getString(4));
					laboreditform.setLabortype(rs.getString(5));
					laboreditform.setCnspartnumber(rs.getString(8));
					laboreditform.setQuantityhours(dfcurfour.format(rs
							.getFloat(9)) + "");
					laboreditform.setPrevquantityhours(dfcurfour.format(rs
							.getFloat(9)) + "");
					laboreditform.setEstimatedhourlybasecost(dfcur.format(rs
							.getFloat(10)) + "");
					laboreditform.setEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					laboreditform.setProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					laboreditform.setPriceunit(dfcur.format(rs.getFloat(13))
							+ "");
					laboreditform
							.setPriceextended(dfcur.format(rs.getFloat(14))
									+ "");
					laboreditform.setSellablequantity(rs.getString(15));
					laboreditform.setMinimumquantity(rs.getString(16));
					laboreditform.setStatus(rs.getString(17));
					laboreditform.setTransit(rs.getString("lm_rs_transit"));
					laboreditform.setFlag(rs.getString("flag"));
					laboreditform
							.setCreatedBy(rs.getString("lm_rs_created_by"));
					laboreditform.setCreatedDate(rs
							.getString("lm_rs_create_date"));
					laboreditform
							.setChangedBy(rs.getString("lm_rs_changed_by"));
					laboreditform.setChangedDate(rs
							.getString("lm_rs_changed_date"));
					// laboreditform.setFlag_laborid(rs.getString( "flag"
					// )+rs.getString( 1 ));
				}
			}

		} catch (Exception e) {
			logger.error(
					"getLaborResourceEdit(LaborEditForm, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getLaborResourceEdit(LaborEditForm, String, String, String, String, DataSource) - Exception caught in rerieving labor resource list"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getLaborResourceEdit(LaborEditForm, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getLaborResourceEdit(LaborEditForm, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getLaborResourceEdit(LaborEditForm, String, String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		return 1;
	}

	public static int getFreightResourceEdit(FreightEditBean freightleditform,
			String type, String querystring, String sessionid, String pageFlag,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		DecimalFormat dfcurmargin = null;
		String sql = "";
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurfour = new DecimalFormat("###0.0000");

		if (pageFlag.equals("listpage"))
			dfcurmargin = new DecimalFormat("0.0000000");
		else
			dfcurmargin = new DecimalFormat("0.00");

		if (querystring.equals("")) {
			querystring = "order by iv_rp_name";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select * from lp_resource_tab( "
					+ freightleditform.getActivity_Id() + " , '"
					+ freightleditform.getFreightid() + "' , '"
					+ freightleditform.getFreighttype() + "' , '" + sessionid
					+ "' )" + querystring;
			rs = stmt.executeQuery(sql);

			if (type.equals("F") || type.equals("IF") || type.equals("AF")
					|| type.equals("CF")) {
				while (rs.next()) {
					freightleditform.setFreightid(rs.getString(1));
					freightleditform.setFreightcostlibid(rs.getString(2));
					if (freightleditform.getFlag() == null)
						freightleditform.setActivity_Id(rs.getString(3));
					freightleditform.setFreightname(rs.getString(18) + "-"
							+ rs.getString(4));
					freightleditform.setFreighttype(rs.getString(5));
					freightleditform.setCnspartnumber(rs.getString(8));
					freightleditform.setQuantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					freightleditform.setPrevquantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					freightleditform.setEstimatedunitcost(dfcur.format(rs
							.getFloat(10)) + "");
					freightleditform.setEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					freightleditform.setProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					freightleditform.setPriceunit(dfcur.format(rs.getFloat(13))
							+ "");
					freightleditform.setPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					freightleditform.setSellablequantity(rs.getString(15));
					freightleditform.setMinimumquantity(rs.getString(16));
					freightleditform.setStatus(rs.getString(17));
					freightleditform.setFlag(rs.getString("flag"));
					freightleditform.setFlag_freightid(rs.getString("flag")
							+ rs.getString(1));
					freightleditform.setCreatedBy(rs
							.getString("lm_rs_created_by"));
					freightleditform.setCreatedDate(rs
							.getString("lm_rs_create_date"));
					freightleditform.setChangedBy(rs
							.getString("lm_rs_changed_by"));
					freightleditform.setChangedDate(rs
							.getString("lm_rs_changed_date"));
				}
			}

		} catch (Exception e) {
			logger.error(
					"getFreightResourceEdit(FreightEditForm, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getFreightResourceEdit(FreightEditForm, String, String, String, String, DataSource) - Exception caught in rerieving freight resource list"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getFreightResourceEdit(FreightEditForm, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getFreightResourceEdit(FreightEditForm, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getFreightResourceEdit(FreightEditForm, String, String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		return 1;
	}

	public static int getTravelResourceEdit(TravelEditBean traveleditform,
			String type, String querystring, String sessionid, String pageFlag,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList resourcelist = new java.util.ArrayList();
		DecimalFormat dfcurmargin = null;
		String sql = "";
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurfour = new DecimalFormat("###0.0000");

		if (pageFlag.equals("listpage"))
			dfcurmargin = new DecimalFormat("0.0000000");
		else
			dfcurmargin = new DecimalFormat("0.00");

		if (querystring.equals("")) {
			querystring = "order by iv_rp_name";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select * from lp_resource_tab( "
					+ traveleditform.getActivity_Id() + " , '"
					+ traveleditform.getTravelid() + "' , '"
					+ traveleditform.getTraveltype() + "' , '" + sessionid
					+ "' )" + querystring;
			rs = stmt.executeQuery(sql);
			if (type.equals("T") || type.equals("IT") || type.equals("AT")
					|| type.equals("CT")) {
				while (rs.next()) {
					traveleditform.setTravelid(rs.getString(1));
					traveleditform.setTravelcostlibid(rs.getString(2));
					if (traveleditform.getFlag() == null)
						traveleditform.setActivity_Id(rs.getString(3));
					traveleditform.setTravelname(rs.getString(18) + "-"
							+ rs.getString(4));
					traveleditform.setTraveltype(rs.getString(5));
					traveleditform.setCnspartnumber(rs.getString(8));
					traveleditform.setQuantity(dfcurfour.format(rs.getFloat(9))
							+ "");
					traveleditform.setPrevquantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					traveleditform.setEstimatedunitcost(dfcur.format(rs
							.getFloat(10)) + "");
					traveleditform.setEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					traveleditform.setProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					traveleditform.setPriceunit(dfcur.format(rs.getFloat(13))
							+ "");
					traveleditform.setPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					traveleditform.setSellablequantity(rs.getString(15));
					traveleditform.setMinimumquantity(rs.getString(16));
					traveleditform.setStatus(rs.getString(17));
					traveleditform.setFlag(rs.getString("flag"));
					traveleditform.setFlag_travelid(rs.getString("flag")
							+ rs.getString(1));
					traveleditform.setCreatedBy(rs
							.getString("lm_rs_created_by"));
					traveleditform.setCreatedDate(rs
							.getString("lm_rs_create_date"));
					traveleditform.setChangedBy(rs
							.getString("lm_rs_changed_by"));
					traveleditform.setChangedDate(rs
							.getString("lm_rs_changed_date"));
				}
			}

		} catch (Exception e) {
			logger.error(
					"getTravelResourceEdit(TravelEditForm, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTravelResourceEdit(TravelEditForm, String, String, String, String, DataSource) - Exception caught in rerieving travel resource list"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getTravelResourceEdit(TravelEditForm, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getTravelResourceEdit(TravelEditForm, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getTravelResourceEdit(TravelEditForm, String, String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		return 1;
	}

	public static ArrayList getResourcecombolist(String type,
			String Activity_Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		ArrayList materiallist = new java.util.ArrayList();

		materiallist.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			// String sql =
			// "select iv_rp_id , iv_rp_name , iv_sc_name from lx_resource_hierarchy_01 where iv_ct_type_desc "
			// +
			// "like '"+type+"%' and iv_rp_id is not null and iv_rp_status = 'A' and iv_rp_id not in ( select lm_rs_rp_id from lm_resource where lm_rs_at_id = '"+Activity_Id+"')";
			String sql = "select iv_rp_id , iv_rp_name , iv_sc_name from lx_resource_hierarchy_01 where iv_ct_type_desc "
					+ "like '"
					+ type
					+ "%' and iv_rp_id is not null and iv_rp_status = 'A' ";

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				materiallist
						.add(new com.mind.common.LabelValue(rs
								.getString("iv_sc_name")
								+ "-"
								+ rs.getString("iv_rp_name"), rs
								.getString("iv_rp_id")));
			}
		} catch (Exception e) {
			logger.error("getResourcecombolist(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourcecombolist(String, String, DataSource) - Exception caught in rerieving resource"
						+ e);
			}
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getResourcecombolist(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getResourcecombolist(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getResourcecombolist(String, String, DataSource) - exception ignored",
						sql);
			}

		}
		return materiallist;
	}

	public static int deleteresource(String Id, String userid, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int val = -1;
		java.util.Date dateIn = new java.util.Date();
		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_delete_01( ? , ? ) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(Id));
			cstmt.setInt(3, Integer.parseInt(userid));
			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("deleteresource(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteresource(String, DataSource) - Exception in DELETING resource"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sqle) {
				logger.error("deleteresource(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteresource(String, DataSource) - Exception in DELETING resource"
							+ sqle);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error("deleteresource(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteresource(String, DataSource) - Exception in DELETING resource"
							+ sqle);
				}
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("deleteresource(String, DataSource) - **** deleteResource :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return val;
	}

	public static String checkforaddendum_changeorder(String Id, String type,
			DataSource ds) {
		String checkforaddendum_changeorder = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select  dbo.func_lx_checkforaddendum_changeorder( '" + Id
					+ "' , '" + type + "' )";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				checkforaddendum_changeorder = rs.getString(1);
			}

		} catch (SQLException e) {
			logger.error(
					"checkforaddendum_changeorder(String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkforaddendum_changeorder(String, String, DataSource) - Exception caught in getting checkforaddendum_changeorder"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.error(
						"checkforaddendum_changeorder(String, String, DataSource)",
						s);

				if (logger.isDebugEnabled()) {
					logger.debug("checkforaddendum_changeorder(String, String, DataSource) - Exception caught in getting checkforaddendum_changeorder"
							+ s);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				logger.error(
						"checkforaddendum_changeorder(String, String, DataSource)",
						s);

				if (logger.isDebugEnabled()) {
					logger.debug("checkforaddendum_changeorder(String, String, DataSource) - Exception caught in getting checkforaddendum_changeorder"
							+ s);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException s) {
				logger.error(
						"checkforaddendum_changeorder(String, String, DataSource)",
						s);

				if (logger.isDebugEnabled()) {
					logger.debug("checkforaddendum_changeorder(String, String, DataSource) - Exception caught in getting checkforaddendum_changeorder"
							+ s);
				}
			}
		}

		return checkforaddendum_changeorder;
	}

	public static String checkNetmedxAppendix(String activity_id, DataSource ds) {
		String flag = "N";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		java.util.Date dateIn = new java.util.Date();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select  dbo.func_isNetMedX( '" + activity_id + "')";
			if (logger.isDebugEnabled()) {
				logger.debug("checkNetmedxAppendix(String, DataSource) - "
						+ sql);
			}
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				flag = rs.getString(1);
			}

		} catch (SQLException e) {
			logger.error("checkNetmedxAppendix(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkNetmedxAppendix(String, DataSource) - Exception caught in getting checkNetmedxAppendix"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.error("checkNetmedxAppendix(String, DataSource)", s);

				if (logger.isDebugEnabled()) {
					logger.debug("checkNetmedxAppendix(String, DataSource) - Exception caught in getting checkNetmedxAppendix"
							+ s);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				logger.error("checkNetmedxAppendix(String, DataSource)", s);

				if (logger.isDebugEnabled()) {
					logger.debug("checkNetmedxAppendix(String, DataSource) - Exception caught in getting checkNetmedxAppendix"
							+ s);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException s) {
				logger.error("checkNetmedxAppendix(String, DataSource)", s);

				if (logger.isDebugEnabled()) {
					logger.debug("checkNetmedxAppendix(String, DataSource) - Exception caught in getting checkNetmedxAppendix"
							+ s);
				}
			}
		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("checkNetmedxAppendix(String, DataSource) - **** checkNetmedxAppendix :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return flag;
	}

	public static String[] getSavedTransitResources(String activity_Id,
			String type, String resource_id, String querystring,
			String sessionid, DataSource ds) {

		int cnt = 0;
		Connection conn = null;
		PreparedStatement pStmt = null;
		PreparedStatement pStmt1 = null;
		ResultSet rs = null;
		String sql = null;
		String savedresources[] = null;
		if (querystring.equals("")) {
			querystring = "order by iv_rp_name";
		}
		java.util.Date dateIn = new java.util.Date();
		try {
			conn = ds.getConnection();
			// String sqlp =
			// "select count(*) as cnt from lp_resource_tab(?, ?, ?, ?) ";
			String sqlp = "select count(*) as cnt from lm_resource where lm_rs_at_id = ? and lm_rs_type = ?";
			pStmt = conn.prepareStatement(sqlp);
			pStmt.setString(1, activity_Id);
			// pStmt.setString(2, resource_id);
			pStmt.setString(2, type);
			// pStmt.setString(4, sessionid);

			if (logger.isDebugEnabled()) {
				logger.debug("getSavedTransitResources(String, String, String, String, String, DataSource) - **** getResourcelist SQL is :::::"
						+ sql);
			}

			rs = pStmt.executeQuery();
			// sql =
			// "select count(*) as cnt from lp_resource_tab('"+activity_Id+"' , '"+resource_id+"' , '"+type+"','"+sessionid+"')";
			if (logger.isDebugEnabled()) {
				logger.debug("getSavedTransitResources(String, String, String, String, String, DataSource) - SQL for transit = "
						+ sqlp);
			}
			if (rs.next()) {
				cnt = rs.getInt("cnt");
			}
			savedresources = new String[cnt];

			// sql =
			// "select * from lp_resource_tab('"+activity_Id+"' , '"+resource_id+"' , '"+type+"','"+sessionid+"') where lm_rs_transit = 'Y' "+querystring;;
			// sqlp =
			// "select * from lp_resource_tab(?, ?, ?, ?) where lm_rs_transit = 'Y' "+querystring;
			sqlp = "select lm_rs_id from lm_resource where lm_rs_at_id = ? and lm_rs_type = ? and lm_rs_transit = 'Y' ";
			pStmt1 = conn.prepareStatement(sqlp);
			pStmt1.setString(1, activity_Id);
			pStmt1.setString(2, type);
			if (logger.isDebugEnabled()) {
				logger.debug("getSavedTransitResources(String, String, String, String, String, DataSource) - SQL for transit = "
						+ sqlp);
			}
			rs = pStmt1.executeQuery();
			int i = 0;
			while (rs.next()) {
				savedresources[i] = rs.getString("lm_rs_id");
				i++;
			}
		} catch (Exception e) {
			logger.error(
					"getSavedTransitResources(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSavedTransitResources(String, String, String, String, String, DataSource) - Exception caught in rerieving getSavedTransitResources "
						+ e);
			}
			logger.error(
					"getSavedTransitResources(String, String, String, String, String, DataSource)",
					e);
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"getSavedTransitResources(String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (pStmt1 != null) {
					pStmt1.close();
				}
				if (pStmt != null) {
					pStmt.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"getSavedTransitResources(String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"getSavedTransitResources(String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("getSavedTransitResources(String, String, String, String, String, DataSource) - **** getSavedTransitResources :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return savedresources;
	}

	public static String[] checkTempResources(String activity_Id, String type,
			String resource_id, String querystring, String sessionid,
			DataSource ds) {

		int cnt = 0;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		String savedresources[] = null;
		if (querystring.equals("")) {
			querystring = "order by iv_rp_name";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select count(*) as cnt from lp_resource_tab( '"
					+ activity_Id + "' , '" + resource_id + "' , '" + type
					+ "' , '" + sessionid + "' )";

			// System.out.println("checkTempResources SQL"+sql);
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				cnt = rs.getInt("cnt");
			}
			savedresources = new String[cnt];

			sql = "select * from lp_resource_tab( '" + activity_Id + "' , '"
					+ resource_id + "' , '" + type + "' , '" + sessionid
					+ "' ) " + querystring;
			;

			// System.out.println("checkTempResources SQL "+sql);
			rs = stmt.executeQuery(sql);
			int i = 0;
			while (rs.next()) {
				if (rs.getString("flag").equals("T")) {
					savedresources[i] = rs.getString("flag")
							+ rs.getString("lm_rs_id");
				}
				i++;
			}

		} catch (Exception e) {
			logger.error(
					"checkTempResources(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkTempResources(String, String, String, String, String, DataSource) - Exception caught in rerieving checkTempResources "
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"checkTempResources(String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"checkTempResources(String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"checkTempResources(String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

		}

		return savedresources;
	}

	public static String getResoucePoolName(String resourceId, DataSource ds) {
		String resourceName = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select dbo.func_get_resouce_pool_name(" + resourceId + ")";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				resourceName = rs.getString(1);
			}

		} catch (SQLException e) {
			logger.error("getResoucePoolName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResoucePoolName(String, DataSource) - Exception caught in getting Resouce Pool Name"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.error("getResoucePoolName(String, DataSource)", s);

				if (logger.isDebugEnabled()) {
					logger.debug("getResoucePoolName(String, DataSource) - Exception caught in getting Resouce Pool Name"
							+ s);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				logger.error("getResoucePoolName(String, DataSource)", s);

				if (logger.isDebugEnabled()) {
					logger.debug("getResoucePoolName(String, DataSource) - Exception caught in getting Resouce Pool Name"
							+ s);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException s) {
				logger.error("getResoucePoolName(String, DataSource)", s);

				if (logger.isDebugEnabled()) {
					logger.debug("getResoucePoolName(String, DataSource) - Exception caught in getting Resouce Pool Name"
							+ s);
				}
			}
		}

		return resourceName;
	}

	/**
	 * Get Default Minimum Hour from database.
	 * 
	 * @param actId
	 * @param ds
	 * @return
	 */
	public static String[] getTicketDefaultMinimumHour(String actId,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		String[] ticketDefaultMinimumHour = new String[3];
		if (logger.isDebugEnabled()) {
			logger.debug("getTicketDefaultMinimumHour(String, DataSource) - ----actId-----= "
					+ actId);
		}
		try {
			conn = ds.getConnection();
			String sql = "select lm_ticket_min_labor_hour, "
					+ "lm_ticket_min_travel_hour, " + "lm_request_type "
					+ "from func_get_default_minimum_hour(?)"; // - 1 parameter

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, actId);
			rs = pStmt.executeQuery();

			if (rs.next()) {
				ticketDefaultMinimumHour[0] = rs
						.getString("lm_ticket_min_labor_hour");
				ticketDefaultMinimumHour[1] = rs
						.getString("lm_ticket_min_travel_hour");
				ticketDefaultMinimumHour[2] = rs.getString("lm_request_type");
			} // - end of while

		} // - end of try
		catch (Exception e) {
			logger.error("getTicketDefaultMinimumHour(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTicketDefaultMinimumHour(String, DataSource) - Exception in com.mind.Resourcedao.getDefaultMinimumHour() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		/*
		 * System.out.println("ticketDefaultMinimumHour[0]= "+
		 * ticketDefaultMinimumHour[0]);
		 * System.out.println("ticketDefaultMinimumHour[1]= "
		 * +ticketDefaultMinimumHour[1]);
		 * System.out.println("ticketDefaultMinimumHour[2]= "
		 * +ticketDefaultMinimumHour[2]);
		 */
		return ticketDefaultMinimumHour;
	} // - end of getActivityList() method

}
