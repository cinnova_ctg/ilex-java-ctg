package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.NewWebTicketBean;
import com.mind.bean.prm.ViewTicketSummaryBean;

public class NewWebTicketdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(NewWebTicketdao.class);

	public static ArrayList<NewWebTicketBean> getWebTicketList(DataSource ds)

	{

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<NewWebTicketBean> valuelist = new ArrayList<NewWebTicketBean>();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_web_ticket_tab('%','%')";
			rs = stmt.executeQuery(sql);

			int i = 0;

			while (rs.next()) {
				NewWebTicketBean webTicketForm = new NewWebTicketBean();

				webTicketForm.setMsa_name(rs.getString("msa_name"));
				webTicketForm.setAppendix_id(rs.getString("lx_pr_id"));
				webTicketForm.setJobid(rs.getString("lm_js_id"));
				webTicketForm.setTicket_number(rs.getString("lm_tc_number"));
				webTicketForm.setTicket_id(rs.getString("lm_tc_id"));
				webTicketForm.setSite_name(rs.getString("lm_si_name"));
				webTicketForm.setSite_number(rs.getString("lm_si_number"));
				webTicketForm.setRequestor_name(rs
						.getString("lm_tc_requestor_name"));
				webTicketForm.setRequest_received_date(rs
						.getString("lm_tc_requested_date"));
				webTicketForm.setRequest_received_time(rs
						.getString("lm_tc_requested_time"));
				webTicketForm.setPreferred_arrival_date(rs
						.getString("lm_tc_preferred_arrival"));
				webTicketForm.setPreferred_arrival_time(rs
						.getString("lm_tc_preferred_arrival_time"));
				webTicketForm.setOwner_name(rs.getString("owner_name"));
				webTicketForm.setOwner_id(rs.getString("owner_id"));
				webTicketForm
						.setProjectManager(rs.getString("project_manager"));

				valuelist.add(i, webTicketForm);
				i++;
			}

		} catch (Exception e) {
			logger.error("getWebTicketList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getWebTicketList(DataSource) - Error occured during getting get Web ticket List::::::::::  "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn("getWebTicketList(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn("getWebTicketList(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("getWebTicketList(DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static String getTicketNumber(DataSource ds, String jobId) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String ticketNumber = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_js_title from lm_job where lm_js_id = " + jobId;

			rs = stmt.executeQuery(sql);

			if (rs.next())
				ticketNumber = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getTicketNumber(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTicketNumber(DataSource, String) - Error Occured in retrieving ticket Number"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getTicketNumber(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getTicketNumber(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getTicketNumber(DataSource, String) - exception ignored",
						s);
			}

		}
		return ticketNumber;
	}

	public static String getOwnerEmail(DataSource ds, String POCFNmae,
			String POCLNmae) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String ownerEmail = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lo_pc_email1 from lo_poc where lo_pc_first_name = '"
					+ POCFNmae + "' and lo_pc_last_name = '" + POCLNmae + "'";

			rs = stmt.executeQuery(sql);

			if (rs.next())
				ownerEmail = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getOwnerEmail(DataSource, String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOwnerEmail(DataSource, String, String) - Error Occured in retrieving Owner Email"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getOwnerEmail(DataSource, String, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getOwnerEmail(DataSource, String, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getOwnerEmail(DataSource, String, String) - exception ignored",
						s);
			}

		}
		return ownerEmail;
	}

	public static String getPOCEmail(String POCId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String POCEmail = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lo_pc_email1 from lo_poc where lo_pc_id = " + POCId;

			rs = stmt.executeQuery(sql);

			if (rs.next())
				POCEmail = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getPOCEmail(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPOCEmail(String, DataSource) - Error Occured in retrieving POC Email"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getPOCEmail(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getPOCEmail(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getPOCEmail(String, DataSource) - exception ignored",
						s);
			}

		}
		return POCEmail;
	}

	public static void getWebTicketSummary(String ticket_number,
			ViewTicketSummaryBean viewsummary, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from web_ticket_summary where cc_wtc_tc_number = '"
					+ ticket_number + "'";
			// System.out.println("sql:::::::::::::::::"+sql);

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				viewsummary.setTc_id(rs.getString("cc_wtc_id"));
				viewsummary.setTicket_number(rs.getString("cc_wtc_tc_number"));
				viewsummary.setSite_number(rs.getString("cc_wtc_site_number"));
				viewsummary.setSite_phone(rs.getString("cc_wtc_site_phone"));
				viewsummary.setSite_worklocation(rs
						.getString("cc_wtc_site_worklocation"));
				viewsummary.setPo_number(rs.getString("cc_wtc_po_number"));
				viewsummary
						.setSite_address(rs.getString("cc_wtc_site_address"));
				viewsummary.setSite_city(rs.getString("cc_wtc_site_city"));
				viewsummary.setSite_state(rs.getString("cc_wtc_site_state"));
				viewsummary
						.setSite_zipcode(rs.getString("cc_wtc_site_zipcode"));
				viewsummary.setSite_name(rs.getString("cc_wtc_site_name"));
				viewsummary.setEmail(rs.getString("cc_wtc_email"));
				viewsummary.setContact_person(rs
						.getString("cc_wtc_contact_person"));
				viewsummary.setContact_phone(rs
						.getString("cc_wtc_contact_phone"));
				viewsummary
						.setPrimary_Name(rs.getString("cc_wtc_primary_Name"));
				viewsummary.setPri_site_phone(rs
						.getString("cc_wtc_pri_site_phone"));
				viewsummary.setSecondary_Name(rs
						.getString("cc_wtc_secondary_Name"));
				viewsummary.setSecondary_phone(rs
						.getString("cc_wtc_secondary_phone"));
				viewsummary.setRequested_date(rs
						.getString("cc_wtc_requested_date"));
				viewsummary.setCriticality(rs.getString("cc_wtc_criticality"));
				viewsummary.setArrivalwindowstartdate(rs
						.getString("cc_wtc_arrivalwindowstartdate"));
				viewsummary.setArrivalwindowenddate(rs
						.getString("cc_wtc_arrivalwindowenddate"));
				viewsummary.setArrivaldate(rs.getString("cc_wtc_arrivaldate"));
				viewsummary.setProblem_description(rs
						.getString("cc_wtc_problem_description"));
				viewsummary.setSp_instructions(rs
						.getString("cc_wtc_sp_instructions"));
				viewsummary.setStand_by(rs.getString("cc_wtc_stand_by"));
				viewsummary.setOther_email_info(rs
						.getString("cc_wtc_other_email_info"));
				viewsummary.setCust_specific_fields(rs
						.getString("cc_wtc_cust_specific_fields"));
				viewsummary
						.setCreated_date(rs.getString("cc_wtc_created_date"));
			}
		} catch (Exception e) {
			logger.error(
					"getWebTicketSummary(String, ViewTicketSummaryForm, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getWebTicketSummary(String, ViewTicketSummaryForm, DataSource) - Exception in retrieving Web Ticket Summary"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getWebTicketSummary(String, ViewTicketSummaryForm, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getWebTicketSummary(String, ViewTicketSummaryForm, DataSource) - Exception in retrieving Web Ticket Summary"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getWebTicketSummary(String, ViewTicketSummaryForm, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getWebTicketSummary(String, ViewTicketSummaryForm, DataSource) - Exception in retrieving Web Ticket Summary"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getWebTicketSummary(String, ViewTicketSummaryForm, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getWebTicketSummary(String, ViewTicketSummaryForm, DataSource) - Exception in retrieving Web Ticket Summary"
							+ sql);
				}
			}

		}
	}

	public static int deleteWebTicket(String ticketNumber, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		int deleteTicket = -1;

		try {

			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lm_delete_web_ticket_01(?)}");
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(ticketNumber));
			cstmt.execute();

			deleteTicket = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("deleteWebTicket(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteWebTicket(String, DataSource) - Error occured while deleting web ticket "
						+ e);
			}

		} finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"deleteWebTicket(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"deleteWebTicket(String, DataSource) - exception ignored",
						e);
			}
		}
		return deleteTicket;
	}

}
