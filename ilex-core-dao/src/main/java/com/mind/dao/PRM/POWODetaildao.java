package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.POWOBean;
import com.mind.common.LabelValue;
import com.mind.common.Util;
import com.mind.fw.core.dao.util.DBUtil;

public class POWODetaildao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(POWODetaildao.class);

	public static ArrayList getPWOdetailmasterpotype(DataSource ds) {
		ArrayList masterpritemtype = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		// masterpritemtype.add( new com.mind.common.LabelValue( "---Select---"
		// , "0") );

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery("select * from lm_pwo_type_master");
			while (rs.next()) {
				masterpritemtype.add(new com.mind.common.LabelValue(rs
						.getString(2), rs.getString(1)));
			}
		}

		catch (Exception e) {
			logger.error("getPWOdetailmasterpotype(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPWOdetailmasterpotype(DataSource) - Exception caught in rerieving masterpritemtype"
						+ e);
			}
		}

		finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPWOdetailmasterpotype(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPWOdetailmasterpotype(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPWOdetailmasterpotype(DataSource) - exception ignored",
						sql);
			}

		}
		return masterpritemtype;
	}

	public static String getCheckFieldSheet(String appendix_id, DataSource ds) {
		String checkSheet = "0";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select fs_id from dbo.fieldsheet where fs_pr_id ="
							+ appendix_id);
			if (rs.next()) {
				checkSheet = "1";

			} else {

			}

		} catch (Exception e) {
			logger.error("getCheckFieldSheet(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCheckFieldSheet(String, DataSource) - Exception caught in checking fieldsheet"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCheckFieldSheet(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCheckFieldSheet(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCheckFieldSheet(String, DataSource) - exception ignored",
						sql);
			}

		}
		return checkSheet;
	}

	public static ArrayList getDeliverableList(DataSource ds) {
		ArrayList deliverableList = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cc_dd_id, cc_dd_desc from dbo.cc_deliverable_detail order by cc_dd_id");
			while (rs.next()) {
				deliverableList.add(new com.mind.common.LabelValue(rs
						.getString(2), rs.getString(1)));
			}
		} catch (Exception e) {
			logger.error("getDeliverableList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDeliverableList(DataSource) - Exception caught in rerieving deliverable list"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getDeliverableList(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getDeliverableList(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getDeliverableList(DataSource) - exception ignored",
						sql);
			}

		}
		return deliverableList;
	}

	public static ArrayList getPWOdetailtype(DataSource ds) {
		ArrayList itemtype = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		itemtype.add(new com.mind.common.LabelValue("---Select---", "0"));
		itemtype.add(new LabelValue("Minuteman", "M"));
		itemtype.add(new LabelValue("Speedpay", "S"));
		itemtype.add(new LabelValue("Internal", "N"));
		itemtype.add(new LabelValue("PVS", "P"));

		return itemtype;
	}

	public static ArrayList getPWOdetailterms(DataSource ds) {
		ArrayList terms = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		terms.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery("select * from lm_pwo_terms_master");
			while (rs.next()) {
				terms.add(new com.mind.common.LabelValue(rs.getString(2), rs
						.getString(1)));
			}
		} catch (Exception e) {
			logger.error("getPWOdetailterms(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPWOdetailterms(DataSource) - Exception caught in rerieving terms"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPWOdetailterms(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPWOdetailterms(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPWOdetailterms(DataSource) - exception ignored",
						sql);
			}

		}
		return terms;
	}

	public static ArrayList getPartnerPOC(String partner_id, DataSource ds) {
		ArrayList partnerpoclist = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		partnerpoclist.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from dbo.func_cc_organization_poc_list("
							+ partner_id + ",'Partner')");
			while (rs.next()) {
				partnerpoclist.add(new com.mind.common.LabelValue(rs
						.getString(2), rs.getString(1)));
			}
		} catch (Exception e) {
			logger.error("getPartnerPOC(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerPOC(String, DataSource) - Exception caught in rerieving terms"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPartnerPOC(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPartnerPOC(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPartnerPOC(String, DataSource) - exception ignored",
						sql);
			}

		}
		return partnerpoclist;
	}

	public static ArrayList getSearchResult(String org_type, String org_name,
			String org_division, String appendixid, String checknetmedxtype,
			DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList searchList = new ArrayList();
		searchcontact contactlist = null;
		int app_id = 0;
		if (appendixid != null) {
			app_id = Integer.parseInt(appendixid);
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lo_contact_search( ? , ? , ? ,? ,? )}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, org_type);
			cstmt.setString(3, '%' + org_name + '%');
			cstmt.setString(4, '%' + org_division + '%');
			cstmt.setInt(5, app_id);
			cstmt.setString(6, checknetmedxtype);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				contactlist = new searchcontact();
				contactlist.setOrg_name(rs.getString(3));
				contactlist.setOrg_division(rs.getString(5));
				contactlist.setCity(rs.getString(6));
				contactlist.setState(rs.getString(7));

				searchList.add(contactlist);
			}

		}

		catch (Exception e) {
			logger.error(
					"getSearchResult(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSearchResult(String, String, String, String, String, DataSource) - Error occured during getting customer search list  "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getSearchResult(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getSearchResult(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getSearchResult(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return searchList;
	}

	public static ArrayList getPWOdetailtabular(String jobid, String powoid,
			String masterpoitem, DataSource ds) {
		ArrayList PWOdetaillist = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		POWODetail powodetail = null;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		String sql1 = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql1 = "select * from dbo.func_lm_powo_resource_detail_tab( '"
					+ jobid + "' , '" + powoid + "')";

			rs = stmt.executeQuery(sql1);
			while (rs.next()) {
				powodetail = new POWODetail();
				powodetail.setResource_id(rs.getString(1));
				powodetail.setAct_name(rs.getString(2));
				powodetail.setResource_name(rs.getString(5));
				powodetail.setResource_qty(dfcur.format(rs
						.getFloat("lx_ce_quantity")));
				powodetail.setResource_unitcost(dfcur.format(rs.getFloat(12))
						+ "");
				powodetail.setResource_totalcost(dfcur.format(rs.getFloat(13))
						+ "");
				powodetail.setResource_authorizedunitcost(dfcur.format(rs
						.getFloat(9)) + "");
				powodetail.setResource_authorizedtotalcost(dfcur.format(rs
						.getFloat(10)) + "");
				powodetail.setResource_showonwo(rs.getString(11));
				powodetail.setPvs_supplied(rs.getString("lm_pwo_pvs_supplied"));
				powodetail.setResource_dropshift(rs
						.getString("lm_pwo_drop_shipped"));
				// changed by Seema
				powodetail.setCredit_card(rs.getString("lm_pwo_credit_card"));
				// over
				powodetail.setResource_tempid(rs.getString(1));
				powodetail.setUnitcost(dfcur.format(rs.getFloat("unit_cost"))
						+ "");
				powodetail.setResource_subtotalcost(dfcur.format(rs
						.getFloat("res_subtotalcost")) + "");

				if (rs.getString("lm_pwo_authorised_qty") != null) {
					powodetail.setResource_authorizedqty(dfcur.format(rs
							.getFloat("lm_pwo_authorised_qty")) + "");
				} else {
					if (masterpoitem != null) {
					} else {
						masterpoitem = rs.getString("lm_po_pwo_type_id");
					}

					if (masterpoitem.equals("1")) {
						powodetail.setResource_authorizedqty("0.00");
					} else {
						powodetail.setResource_authorizedqty(dfcur.format(rs
								.getFloat("lm_pwo_authorised_qty")) + "");
					}
				}

				if (rs.getString(11).equalsIgnoreCase("N"))
					powodetail.setResource_tempshowonwo("");
				else
					powodetail.setResource_tempshowonwo(rs.getString(11));

				if (rs.getString("lm_pwo_drop_shipped").equalsIgnoreCase("N"))
					powodetail.setResource_tempdropshift("");
				else
					powodetail.setResource_tempdropshift(rs
							.getString("lm_pwo_drop_shipped"));

				// Start :Added By Amit

				if (rs.getString("lm_pwo_pvs_supplied").equalsIgnoreCase("N")) {
					powodetail.setResource_temp_pvs_supplied("");
				} else
					powodetail.setResource_temp_pvs_supplied(rs
							.getString("lm_pwo_pvs_supplied"));

				powodetail.setAct_qty(dfcur.format(rs.getFloat("actquantity")));
				powodetail.setResource_act_qty(dfcur.format(rs
						.getFloat("res_act_qty")));
				// End : Added By Amit

				// changes made by Seema
				if (rs.getString("lm_pwo_credit_card").equalsIgnoreCase("N"))
					powodetail.setResource_temp_credit_card("");
				else
					powodetail.setResource_temp_credit_card(rs
							.getString("lm_pwo_credit_card"));

				// added by Seema
				powodetail.setType(rs.getString("lm_po_type"));

				powodetail.setResource_type(rs.getString("iv_ct_name"));
				powodetail.setResourceSubCat(rs.getString("iv_mf_name"));

				PWOdetaillist.add(powodetail);

			}
		} catch (Exception e) {
			logger.error(
					"getPWOdetailtabular(String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPWOdetailtabular(String, String, String, DataSource) - Exception caught in rerieving PWOdetaillist"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPWOdetailtabular(String, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPWOdetailtabular(String, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPWOdetailtabular(String, String, String, DataSource) - exception ignored",
						sql);
			}

		}
		return PWOdetaillist;
	}

	public static String getSowAssumption(String id, String type, DataSource ds) {
		String sowassumption = "";
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_po_sow_assumption_default(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(id));
			cstmt.setString(3, type);

			/*
			 * if( type.equals( "sow" ) ) /*sql =
			 * "select lx_sw_sow = rtrim(isnull(lx_sw_sow,'')) from lx_sow  where lx_sw_used_in = '"
			 * +id+"' and lx_sw_type IN " +
			 * "( 'pm_resM' , 'pm_resL' , 'pm_resF' , 'pm_resT' ) order by lx_sw_order"
			 * ;
			 */
			/*
			 * sql =
			 * "select lx_sw_sow =isnull(lx_sw_sow,'') from lx_sow  where lx_sw_used_in = '"
			 * +id+"' and lx_sw_type IN " +
			 * "( 'pm_resM' , 'pm_resL' , 'pm_resF' , 'pm_resT' ) order by lx_sw_order"
			 * ;
			 * 
			 * else
			 */
			/*
			 * sql =
			 * "select lx_as_assumption = rtrim(isnull(lx_as_assumption,'')) from lx_assumption  where lx_as_used_in = '"
			 * +id+"' and lx_as_type " +
			 * "IN ( 'pm_resM' , 'pm_resL' , 'pm_resF' , 'pm_resT' ) order by lx_as_order"
			 * ;
			 *//*
				 * sql =
				 * "select lx_as_assumption =isnull(lx_as_assumption,'') from lx_assumption  where lx_as_used_in = '"
				 * +id+"' and lx_as_type " +
				 * "IN ( 'pm_resM' , 'pm_resL' , 'pm_resF' , 'pm_resT' ) order by lx_as_order"
				 * ;
				 */

			// System.out.println("sql:::::::::::::::::::::::::::::"+sql);

			rs = cstmt.executeQuery();
			while (rs.next()) {
				sowassumption = sowassumption + rs.getString(1) + "\n";
			}
		} catch (Exception e) {
			logger.error("getSowAssumption(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSowAssumption(String, String, DataSource) - Exception caught in rerieving PWOdetaillist"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"getSowAssumption(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"getSowAssumption(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"getSowAssumption(String, String, DataSource) - exception ignored",
						s);
			}

		}
		return sowassumption;
	}

	public static POWOBean getPWOdetaildata(POWOBean powoform, String powoid,
			String appendixType, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		POWODetail powodetail = null;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		String sql = "";
		String deliverbydate = "";
		String tempSpInst = "";
		String tempSpCond = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select * from dbo.func_lm_powo_detail('" + powoid + "')";

			// System.out.println("POWO detail::::::::::::"+sql);

			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				powoform.setPowono(rs.getString("lm_po_number"));
				powoform.setType(rs.getString("lm_po_type"));
				powoform.setIssuedate(rs.getString("lm_po_issue_date"));
				deliverbydate = rs.getString("lm_po_deliver_by_date");
				if (deliverbydate.equals("")) {
					if (appendixType.equalsIgnoreCase("NetMedX"))
						deliverbydate = rs.getString("lm_tc_preferred_arrival");
					else
						deliverbydate = rs
								.getString("lm_js_planned_start_date");
				}
				if (!deliverbydate.equals("")) {
					powoform.setDeliverbydate(deliverbydate.substring(0, 10));
					if (deliverbydate.substring(11, 12).equals(" "))
						powoform.setDeliverbydatehh("0"
								+ deliverbydate.substring(12, 13));
					else
						powoform.setDeliverbydatehh(deliverbydate.substring(11,
								13));
					powoform.setDeliverbydatemm(deliverbydate.substring(14, 16));
					powoform.setDeliverbydateop(deliverbydate.substring(23, 25));
				}

				powoform.setTerms(rs.getString("lm_po_terms"));
				powoform.setMasterpoitem(rs.getString("lm_pwo_type_id"));
				powoform.setShiptoaddress(rs.getString("lm_po_ship_to_address"));
				powoform.setSpeccond(rs.getString("lm_po_special_conditions"));
				powoform.setSow(rs.getString("lm_po_sow"));

				// System.out.println("assumptions:::"+rs.getString("lm_po_assumption")
				// );
				powoform.setAssumption(rs.getString("lm_po_assumption"));

				powoform.setAuthorizedtotal(dfcur.format(rs
						.getFloat("lm_po_authorised_total")) + "");
				powoform.setFcost(powoform.getAuthorizedtotal());
				powoform.setInvoiceno(rs.getString("lm_recon_inv_no"));
				powoform.setInvoicerecddate(rs
						.getString("lm_recon_inv_recd_date"));

				String invoice_total = "";
				invoice_total = rs.getString("lm_recon_invoice_total");

				/*
				 * if( rs.getString("lm_recon_invoice_total") != null )
				 * powoform.setInvoicetotal( dfcur.format(
				 * rs.getFloat("lm_recon_invoice_total") )+"" ); else
				 * powoform.setInvoicetotal(
				 * rs.getString("lm_recon_invoice_total") );
				 */

				if (invoice_total != null)
					powoform.setInvoicetotal(dfcur.format(Float
							.parseFloat(invoice_total)) + "");
				else
					powoform.setInvoicetotal(invoice_total);

				String approved_total = "";
				approved_total = rs.getString("lm_recon_approved_total");

				if (approved_total != null)
					powoform.setApprovedtotal(dfcur.format(Float
							.parseFloat(approved_total)) + "");
				else
					powoform.setApprovedtotal(approved_total);

				/*
				 * if( rs.getString("lm_recon_approved_total") != null )
				 * powoform.setApprovedtotal( dfcur.format(
				 * rs.getFloat("lm_recon_approved_total") )+"" ); else
				 * powoform.setApprovedtotal(
				 * rs.getString("lm_recon_approved_total") );
				 */
				powoform.setInvoicecomments(rs.getString("lm_recon_comments"));
				powoform.setSpecialinstruction(rs
						.getString("lm_po_special_instructions"));
				powoform.setPartnerid(rs.getString("lm_po_partner_id"));
				powoform.setPartnerpoc(rs.getString("lm_po_partner_poc_id"));

				String lm_tc_spec_instruction = "";
				lm_tc_spec_instruction = rs.getString("lm_tc_spec_instruction");
				String pf_sp_instruction = "";
				pf_sp_instruction = rs.getString("pf_sp_instruction");
				String lm_si_spec_condition = "";
				lm_si_spec_condition = rs.getString("lm_si_spec_condition");
				String pf_sp_condition = "";
				pf_sp_condition = rs.getString("pf_sp_condition");
				String lm_tc_problem_desc = "";
				lm_tc_problem_desc = rs.getString("lm_tc_problem_desc");

				/* Set sp instruction and condition for NetMedX project */
				if (appendixType.equals("NetMedX")) {
					if (powoform.getSpecialinstruction() != null) {
					} else {
						if (!lm_tc_spec_instruction.equals("")) {
							tempSpInst = lm_tc_spec_instruction;
						}
						if (!pf_sp_instruction.equals(""))
							tempSpInst = tempSpInst + "\n" + pf_sp_instruction;
						powoform.setSpecialinstruction(tempSpInst);
					}

					if (powoform.getSpeccond() != null) {
					} else {
						if (!lm_si_spec_condition.equals(""))
							tempSpCond = lm_si_spec_condition;
						if (!pf_sp_condition.equals(""))
							tempSpCond = tempSpCond + "\n" + pf_sp_condition;

						powoform.setSpeccond(tempSpCond);
					}

					if (powoform.getSow().equals(""))
						powoform.setSow(lm_tc_problem_desc);

					powoform.setProblemdesc(lm_tc_problem_desc);
				}

				/* Set sp instruction and condition for project */
				else {
					if (powoform.getSpecialinstruction() != null) {
					} else {
						if (!pf_sp_instruction.equals(""))
							tempSpInst = pf_sp_instruction;

						powoform.setSpecialinstruction(tempSpInst);
					}

					if (powoform.getSpeccond() != null) {
					} else {
						if (!pf_sp_condition.equals(""))
							tempSpCond = pf_sp_condition;

						powoform.setSpeccond(tempSpCond);
					}
				}
				powoform.setDeliverable(rs.getString("lm_po_deliverables"));
			}
		} catch (Exception e) {
			logger.error(
					"getPWOdetaildata(POWOForm, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPWOdetaildata(POWOForm, String, String, DataSource) - Exception caught in rerieving PWO detail data"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql1) {
				logger.warn(
						"getPWOdetaildata(POWOForm, String, String, DataSource) - exception ignored",
						sql1);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql1) {
				logger.warn(
						"getPWOdetaildata(POWOForm, String, String, DataSource) - exception ignored",
						sql1);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql1) {
				logger.warn(
						"getPWOdetaildata(POWOForm, String, String, DataSource) - exception ignored",
						sql1);
			}

		}
		return powoform;
	}

	public static String[] getSavedResources(String powoid, String type,
			DataSource ds) {

		int cnt = 0;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		String savedresources[] = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (type.equals("resource"))
				sql = "select count(*) as cnt from lm_purchase_work_order where lm_pwo_po_id ="
						+ powoid;
			if (type.equals("showonwo"))
				sql = "select count(*) as cnt from lm_purchase_work_order where lm_pwo_show_on_wo = 'Y' and lm_pwo_po_id ="
						+ powoid;
			if (type.equals("dropshipped"))
				sql = "select count(*) as cnt from lm_purchase_work_order where lm_pwo_drop_shipped = 'Y' and lm_pwo_po_id ="
						+ powoid;

			// Start :Added By Amit for pvs_supplied
			if (type.equals("pvs_supplied"))
				sql = "select count(*) as cnt from lm_purchase_work_order where lm_pwo_pvs_supplied = 'Y' and lm_pwo_po_id ="
						+ powoid;
			// End : Added By Amit

			// changes made by Seema
			if (type.equals("credit_card"))
				sql = "select count(*) as cnt from lm_purchase_work_order where lm_pwo_credit_card = 'Y' and lm_pwo_po_id ="
						+ powoid;

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				cnt = rs.getInt("cnt");
			}
			savedresources = new String[cnt];
			if (type.equals("resource"))
				sql = "select lm_pwo_rs_id from lm_purchase_work_order where lm_pwo_po_id ="
						+ powoid;
			if (type.equals("showonwo"))
				sql = "select lm_pwo_rs_id from lm_purchase_work_order where lm_pwo_show_on_wo = 'Y' and lm_pwo_po_id ="
						+ powoid;
			if (type.equals("dropshipped"))
				sql = "select lm_pwo_rs_id from lm_purchase_work_order where lm_pwo_drop_shipped = 'Y' and lm_pwo_po_id ="
						+ powoid;

			// Start :Added By Amit for pvs_supplied
			if (type.equals("pvs_supplied"))
				sql = "select lm_pwo_rs_id from lm_purchase_work_order where lm_pwo_pvs_supplied = 'Y' and lm_pwo_po_id ="
						+ powoid;
			// End : Added By Amit

			if (type.equals("credit_card")) {
				sql = "select lm_pwo_rs_id from lm_purchase_work_order where lm_pwo_credit_card = 'Y' and lm_pwo_po_id ="
						+ powoid;
				// System.out.println("66666666666666::::::::::::::::"+powoid);
			}

			rs = stmt.executeQuery(sql);
			int i = 0;
			while (rs.next()) {
				savedresources[i] = rs.getString("lm_pwo_rs_id");
				i++;
			}

		} catch (Exception e) {
			logger.error("getSavedResources(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSavedResources(String, String, DataSource) - Exception caught in rerieving Saved Resources "
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"getSavedResources(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"getSavedResources(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"getSavedResources(String, String, DataSource) - exception ignored",
						s);
			}

		}

		return savedresources;
	}

	public static int updatepowodata(POWOBean powoform, String t1, String t2,
			String t3, String t4, String t5, String deliverbydate,
			String loginuserid, String t6, String t7, String t8, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		Util util = new Util();
		ResultSet rs = null;
		int flag = -1;
		searchcontact contactlist = null;
		try {
			if (powoform.getSpecialinstruction() != null) {
				if (powoform.getSpecialinstruction().indexOf("'") >= 0)
					powoform.setSpecialinstruction(util
							.changeStringToDoubleQuotes(powoform
									.getSpecialinstruction()));
			}

			if (powoform.getSpeccond() != null) {
				if (powoform.getSpeccond().indexOf("'") >= 0)
					powoform.setSpeccond(util
							.changeStringToDoubleQuotes(powoform.getSpeccond()));
			}

			conn = ds.getConnection();
			// cstmt=conn.prepareCall("{?=call lm_po_wo_detail_manage_01( ? , ? ,  ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?, ? , ?,?,?,?,? ,?)}");
			cstmt = conn
					.prepareCall("{?=call lm_po_wo_detail_manage_01( ? , ? ,  ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?, ? , ?,?,?,?,? ,?, ?)}"); // --
																																																// one
																																																// added
																																																// ?
																																																// on
																																																// 19/10/2007
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(powoform.getPowoid()));
			cstmt.setString(3, powoform.getType());
			cstmt.setInt(4, Integer.parseInt(powoform.getMasterpoitem()));
			cstmt.setString(5, powoform.getBulk()); // bulk
			cstmt.setString(6, powoform.getIssuedate());
			cstmt.setString(7, deliverbydate);
			cstmt.setString(8, powoform.getTerms());
			cstmt.setString(9, powoform.getShiptoaddress());
			cstmt.setString(10, powoform.getSpeccond());
			cstmt.setObject(11, powoform.getSow());
			cstmt.setString(12, powoform.getAssumption());
			cstmt.setFloat(13, Float.parseFloat(powoform.getAuthorizedtotal())); // money
			cstmt.setString(14, t1);
			cstmt.setString(15, t2);
			cstmt.setString(16, t3);
			cstmt.setString(17, t4);

			if (powoform.getInvoiceno() != "")
				cstmt.setString(18, powoform.getInvoiceno());
			else
				cstmt.setString(18, null);
			cstmt.setString(19, powoform.getInvoicerecddate());

			if (powoform.getInvoicetotal() != "")
				cstmt.setFloat(20, Float.parseFloat(powoform.getInvoicetotal()));
			else
				cstmt.setFloat(20, -1);

			if (powoform.getApprovedtotal() != "")
				cstmt.setFloat(21,
						Float.parseFloat(powoform.getApprovedtotal()));
			else
				cstmt.setFloat(21, -1);

			cstmt.setString(22, powoform.getInvoicecomments());
			cstmt.setInt(23, Integer.parseInt(loginuserid));
			cstmt.setString(24, "A");
			cstmt.setString(25, powoform.getSpecialinstruction());
			cstmt.setString(26, powoform.getPartnerpoc());

			cstmt.setString(27, t5);
			cstmt.setString(28, t6);
			// Start :Added By Amit
			cstmt.setString(29, t7);
			// changed by Seema
			cstmt.setString(30, t8);
			cstmt.setString(31, powoform.getDeliverable());
			cstmt.setFloat(32,
					Float.parseFloat(powoform.getNonContractEstTotal()));
			if (powoform.isChangePoPayment() == true)
				cstmt.setString(33, "1");
			else
				cstmt.setString(33, "0");

			// End :Added By Amit
			/*
			 * System.out.println(
			 * "QUERY  "+powoform.getPowoid()+" , '"+powoform.getType()+"' , " +
			 * ""+powoform.getMasterpoitem()+" , '"+powoform.getBulk()+"' , '"+
			 * powoform.getIssuedate()+"' , " +
			 * "'"+powoform.getDeliverbydate()+"' , '"
			 * +powoform.getTerms()+"' , '"+powoform.getShiptoaddress()+"' , " +
			 * "'"
			 * +powoform.getSpeccond()+"' , '"+powoform.getSow()+"' , '"+powoform
			 * .getAssumption()+"' , " + ""+ powoform.getAuthorizedtotal()
			 * +" , '"+t1+"' , '"+t2+"' , '"+t3+"' , '"+t4+"'  , " + ""+
			 * powoform
			 * .getInvoiceno()+" , '"+powoform.getInvoicerecddate()+"' , "
			 * +powoform.getInvoicetotal() +" ," + ""+
			 * powoform.getApprovedtotal()
			 * +" , '"+powoform.getInvoicecomments()+
			 * "' , "+loginuserid+" , 'A' , " +
			 * "'"+powoform.getSpecialinstruction
			 * ()+"',"+powoform.getPartnerpoc()
			 * +",'"+t5+"','"+t6+"','"+t7+"','"+t8 );
			 */

			cstmt.execute();

			flag = cstmt.getInt(1);

		}

		catch (Exception e) {
			logger.error(
					"updatepowodata(POWOForm, String, String, String, String, String, String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatepowodata(POWOForm, String, String, String, String, String, String, String, String, String, String, DataSource) - Error occured during inserting powo detail data  "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"updatepowodata(POWOForm, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"updatepowodata(POWOForm, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"updatepowodata(POWOForm, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return flag;
	}

	public static String getPartnername(DataSource ds, String powoid) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String partnername = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select dbo.func_cc_name('" + powoid + "', 'Partner' )";

			rs = stmt.executeQuery(sql);

			if (rs.next())
				partnername = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getPartnername(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnername(DataSource, String) - Error Occured in retrieving partner name"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getPartnername(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getPartnername(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getPartnername(DataSource, String) - exception ignored",
						s);
			}

		}
		return partnername;
	}

	public static String getJobIdFromPoId(DataSource ds, String powoid) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String jobId = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_po_js_id from lm_purchase_order where lm_po_id = "
					+ powoid;

			rs = stmt.executeQuery(sql);

			if (rs.next())
				jobId = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getJobIdFromPoId(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobIdFromPoId(DataSource, String) - Error Occured in retrieving partner name"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getJobIdFromPoId(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getJobIdFromPoId(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getJobIdFromPoId(DataSource, String) - exception ignored",
						s);
			}

		}
		return jobId;
	}

	public static String getNonContractEstTotalCost(String powoId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		DecimalFormat dfcur = new DecimalFormat("###0.00");

		String nonContractEstTotal = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select isnull(lm_po_estimated_noncontract_total,0) from lm_purchase_order where lm_po_id ='"
					+ powoId + "'";
			rs = stmt.executeQuery(sql);

			if (rs.next())
				nonContractEstTotal = dfcur.format(Float.parseFloat(rs
						.getString(1)));
		}

		catch (Exception e) {
			logger.error("getNonContractEstTotalCost(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNonContractEstTotalCost(String, DataSource) - Error Occured in retrieving non Contract Labor Total"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getNonContractEstTotalCost(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getNonContractEstTotalCost(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getNonContractEstTotalCost(String, DataSource) - exception ignored",
						s);
			}

		}
		return nonContractEstTotal;
	}

	public static String getPoPaymentValue(String powoId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		String poPaymentValue = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_po_payment from lm_purchase_order where lm_po_id ='"
					+ powoId + "'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				if (rs.getString("lm_po_payment") != null)
					poPaymentValue = rs.getString("lm_po_payment").trim();
			}
		}

		catch (Exception e) {
			logger.error("getPoPaymentValue(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPoPaymentValue(String, DataSource) - Error Occured in retrieving purchase order payment value"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getPoPaymentValue(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getPoPaymentValue(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getPoPaymentValue(String, DataSource) - exception ignored",
						s);
			}

		}
		return poPaymentValue;
	}

	public static String[] getMinutemanType(String poId, DataSource ds) {
		String[] checkType = new String[2];
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sqlQuery = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sqlQuery = "select lm_po_type,lm_po_issue_date=replace(isnull(convert(varchar(10),lm_po_issue_date,101),''),'01/01/1900', '') from lm_purchase_order where lm_po_id="
					+ poId;
			rs = stmt.executeQuery(sqlQuery);
			if (rs.next()) {
				checkType[0] = rs.getString("lm_po_type");
				checkType[1] = rs.getString("lm_po_issue_date");
			}

		} catch (Exception e) {
			logger.error("getMinutemanType(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMinutemanType(String, DataSource) - Error  occured getting Type of Partner "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"getMinutemanType(String, DataSource) - exception ignored",
						s);
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getMinutemanType(String, DataSource) - exception ignored",
						s);
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getMinutemanType(String, DataSource) - exception ignored",
						s);
			}
		}
		return checkType;

	}

	/**
	 * @name updateEmailPOWO
	 * @purpose This method is used to update the sender details of PO/WO in new
	 *          Email Table.
	 * @steps 1. User requests to send the PO/WO to client as well as to DocM.
	 * 
	 * @param String
	 *            Id
	 * @param String
	 *            type
	 * @param String
	 *            userId
	 * @param DataSource
	 *            ds
	 * @return
	 * @returnDescription
	 */

	public static void updateEmailPOWO(String Id, String type, String userId,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		boolean flag = false;
		Statement poExists = null;
		ResultSet poRs = null;
		Statement woExists = null;
		ResultSet woRs = null;
		Statement poUpdate = null;
		Statement woUpdate = null;
		Statement poInsert = null;
		Statement woInsert = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select 1 from lm_powo_email_audit where lm_pe_at_po_id = "
							+ Id); // Check for Record Existense in EmailTable
			if (rs.next()) {
				if (type.trim().equalsIgnoreCase("PO")) { // Check if the Type
															// is PO
					try {
						poExists = conn.createStatement();
						// check if the PO is already present and we have to
						// update the last values
						poRs = poExists
								.executeQuery("select 1 from lm_powo_email_audit where lm_pe_po_first_sent_by is not null and lm_pe_at_po_id = "
										+ Id);
						if (poRs.next()) {
							poUpdate = conn.createStatement();
							poUpdate.executeUpdate(" update lm_powo_email_audit "
									+ " set lm_pe_po_last_sent_by = "
									+ userId
									+ " ,"
									+ " lm_pe_po_last_sent_date = getdate()"
									+ " where lm_pe_at_po_id = " + Id);
						} else {
							poUpdate = conn.createStatement();
							poUpdate.executeUpdate(" update lm_powo_email_audit "
									+ " set lm_pe_po_first_sent_by = "
									+ userId
									+ " ,"
									+ " lm_pe_po_first_sent_date = getdate(),"
									+ " lm_pe_po_last_sent_by = "
									+ userId
									+ " ,"
									+ " lm_pe_po_last_sent_date = getdate()"
									+ " where lm_pe_at_po_id = " + Id);
						}
					} catch (Exception e) {
						logger.error(
								"updateEmailPOWO(String, String, String, DataSource)",
								e);

						logger.error(
								"updateEmailPOWO(String, String, String, DataSource)",
								e);
					} finally {
						DBUtil.close(poRs); // Close resultset objects
						DBUtil.close(poExists); // Close Statement
												// objects
						DBUtil.close(poUpdate); // Close Statement
												// objects
					}
				} else if (type.trim().equalsIgnoreCase("WO")) { // Check if the
																	// Type is
																	// WO
					try {
						woExists = conn.createStatement();
						// check if the PO is already present and we have to
						// update the last values
						woRs = woExists
								.executeQuery("select 1 from lm_powo_email_audit where lm_pe_wo_first_sent_by is not null and lm_pe_at_po_id = "
										+ Id);
						if (woRs.next()) {
							woUpdate = conn.createStatement();
							woUpdate.executeUpdate(" update lm_powo_email_audit "
									+ " set lm_pe_wo_last_sent_by = "
									+ userId
									+ " ,"
									+ " lm_pe_wo_last_sent_date = getdate()"
									+ " where lm_pe_at_po_id = " + Id);
						} else {
							woUpdate = conn.createStatement();
							woUpdate.executeUpdate(" update lm_powo_email_audit "
									+ " set lm_pe_wo_first_sent_by = "
									+ userId
									+ " ,"
									+ " lm_pe_wo_first_sent_date = getdate(),"
									+ " lm_pe_wo_last_sent_by = "
									+ userId
									+ " ,"
									+ " lm_pe_wo_last_sent_date = getdate()"
									+ " where lm_pe_at_po_id = " + Id);
						}
					} catch (Exception e) {
						logger.error(
								"updateEmailPOWO(String, String, String, DataSource)",
								e);

						logger.error(
								"updateEmailPOWO(String, String, String, DataSource)",
								e);
					} finally {
						DBUtil.close(woRs); // Close resultset objects
						DBUtil.close(woExists); // Close Statement
												// objects
						DBUtil.close(woUpdate); // Close Statement
												// objects
					}
				}
			} else { // When record does not exists in the EmailTable
				if (type.trim().equalsIgnoreCase("PO")) { // Check if the Type
															// is PO
					try {
						poInsert = conn.createStatement();
						poInsert.executeUpdate("insert into lm_powo_email_audit (lm_pe_at_po_id,"
								+ " lm_pe_po_first_sent_by,"
								+ " lm_pe_po_first_sent_date,"
								+ " lm_pe_po_last_sent_by,"
								+ " lm_pe_po_last_sent_date "
								+ " )values("
								+ Id
								+ ","
								+ userId
								+ ",getdate(),"
								+ userId
								+ ",getdate())");
					} catch (Exception e) {
						logger.error(
								"updateEmailPOWO(String, String, String, DataSource)",
								e);

						logger.error(
								"updateEmailPOWO(String, String, String, DataSource)",
								e);
					} finally {
						DBUtil.close(poInsert); // Close Statement
												// objects
					}
				} else if (type.trim().equalsIgnoreCase("WO")) { // Check if the
																	// Type is
																	// WO
					try {
						woInsert = conn.createStatement();
						woInsert.executeUpdate("insert into lm_powo_email_audit (lm_pe_at_po_id,"
								+ " lm_pe_wo_first_sent_by,"
								+ " lm_pe_wo_first_sent_date,"
								+ " lm_pe_wo_last_sent_by,"
								+ " lm_pe_wo_last_sent_date "
								+ " )values("
								+ Id
								+ ","
								+ userId
								+ ",getdate(),"
								+ userId
								+ ",getdate())");
					} catch (Exception e) {
						logger.error(
								"updateEmailPOWO(String, String, String, DataSource)",
								e);

						logger.error(
								"updateEmailPOWO(String, String, String, DataSource)",
								e);
					} finally {
						DBUtil.close(woInsert);
					}
				}
			}

		} catch (Exception e) {
			logger.error("updateEmailPOWO(String, String, String, DataSource)",
					e);

			logger.error("updateEmailPOWO(String, String, String, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, stmt); // Close Statement objects
			DBUtil.close(conn); // Close database conneciton object
		}

	}

	public static String getPartnerType(String partnerId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		String partnerType = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select cp_pt_partner_type = isnull(cp_pt_partner_type,'') from cp_partner where cp_partner_id ='"
					+ partnerId + "'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				partnerType = rs.getString("cp_pt_partner_type").trim();
			}
		}

		catch (Exception e) {
			logger.error("getPartnerType(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerType(String, DataSource) - Error Occured in retrieving partner type"
						+ e);
			}
		}

		finally {

			DBUtil.close(rs, stmt); // Close Statement objects
			DBUtil.close(conn); // Close database conneciton object
		}
		return partnerType;
	}

	public static String getPartnerAssignedPOC(String partnerId, String poId,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		String partnerContact = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select * from dbo.func_cc_organization_poc_list("
					+ partnerId
					+ ",'Partner') inner join lm_purchase_order on lm_po_partner_poc_id = poc_id where lm_po_number ="
					+ poId;
			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerAssignedPOC(String, String, DataSource) - ============================================================================="
						+ sql);
			}
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				partnerContact = rs.getString("poc_name").trim();
			}
		}

		catch (Exception e) {
			logger.error("getPartnerAssignedPOC(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerAssignedPOC(String, String, DataSource) - Error Occured in retrieving partner type"
						+ e);
			}
			logger.error("getPartnerAssignedPOC(String, String, DataSource)", e);
		}

		finally {
			DBUtil.close(rs, stmt); // Close Statement objects
			DBUtil.close(conn); // Close database conneciton object
		}
		return partnerContact;
	}

	public static ArrayList getPVSJustificationList(DataSource ds) {
		ArrayList pVSJustificationList = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		pVSJustificationList.add(new com.mind.common.LabelValue("---Select---",
				"0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from lm_purchase_order_exception where lm_pe_status=1 order by lm_pe_code");
			while (rs.next()) {
				pVSJustificationList.add(new com.mind.common.LabelValue(rs
						.getString("lm_pe_code"), rs.getString("lm_pe_id")));
			}
		} catch (Exception e) {
			logger.error("getPVSJustificationList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPVSJustificationList(DataSource ds) - Exception caught in getting PVS Justification List "
						+ e);
			}
		} finally {

			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
		return pVSJustificationList;
	}

	public static ArrayList getGeographicConstraintList(DataSource ds) {
		ArrayList geographicConstraintList = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		geographicConstraintList.add(new com.mind.common.LabelValue(
				"---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from lm_po_geographic_constraint where lm_gc_status=1 order by lm_gc_code");
			while (rs.next()) {
				geographicConstraintList.add(new com.mind.common.LabelValue(rs
						.getString("lm_gc_code"), rs.getString("lm_gc_id")));
			}
		} catch (Exception e) {
			logger.error(" getGeographicConstraintList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug(" getGeographicConstraintList(DataSource ds) - Exception caught in getting  Geographic ConstraintList "
						+ e);
			}
		} finally {

			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
		return geographicConstraintList;
	}

}
