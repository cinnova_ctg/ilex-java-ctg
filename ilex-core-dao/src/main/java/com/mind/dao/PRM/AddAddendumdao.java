/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is a data object access class used for managing addendum for a project in Project Manager and populating drop down list and data for drop down list.      
*
*/
package com.mind.dao.PRM;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.AddAddendumBean;

/*
 * Methods : getCustomerPOC, setCustpocdetail, Addaddendum, setAddendumdetail, getAddendumList, getAddendumHierarchyList, getAddendum 
 */

public class AddAddendumdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AddAddendumdao.class);

	private Collection custpoc = null;

/** This method returns ArrayList Customer POC for an appendix 
* @param appendixid   			   String. the value of organization type
* @param ds   			       	   object of DataSource
* @return String              	   This method returns string, Organization Type Name.
*/		
	// Get Customer POC list for an appendix
	
	public static java.util.Collection getCustomerPOC( String appendixid , String type , DataSource ds) 
	{
		ArrayList custpoc = new java.util.ArrayList();
		Connection conn = null;
		Connection conn1 = null;
		Statement stmt = null;
		Statement stmt1 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		String query ="";
		String msaid = "";
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
				
			String sql = "select lx_pr_mm_id from lx_appendix_main where lx_pr_id = "+appendixid;
			
			rs = stmt.executeQuery( sql );
			if( rs.next() )
			{
				msaid = rs.getString( "lx_pr_mm_id" );
			}
			
		}
		
		catch( Exception e )
		{
			logger.error("getCustomerPOC(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomerPOC(String, String, DataSource) - Error occured during msa id" + e);
			}
		}
		
		
		
		query = "select poc_id , poc_name from dbo.func_getPOC_tab('"+msaid+"' , '"+type+"')";
		custpoc.add( new com.mind.common.LabelValue( "---Select---" , "-1") );
		
		try
		{
			conn1 = ds.getConnection();
			stmt1 = conn1.createStatement();
			rs1 = stmt1.executeQuery( query );
			
			while( rs1.next() )
			{
				custpoc.add ( new com.mind.common.LabelValue( rs1.getString( "poc_name" ) , rs1.getString( "poc_id" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getCustomerPOC(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomerPOC(String, String, DataSource) - Exception caught in getting POC" + e);
			}
		}
		finally
		{
			
			try
			{
				if( rs != null ) rs.close();
				if( rs1 != null )	rs1.close();
			}
			catch( SQLException sql )
			{
				logger.error("getCustomerPOC(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getCustomerPOC(String, String, DataSource) - Exception caught in closing poc resultset" + sql);
				}
			}
			
			
			try
			{
				if( stmt != null )
					stmt.close();
				if( stmt1 != null )
					stmt1.close();
				
			}
			catch( SQLException sql )
			{
				logger.error("getCustomerPOC(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getCustomerPOC(String, String, DataSource) - Exception caught in closing poc statement" + sql);
				}
			}
			
			try
			{
				if( conn != null )
				 conn.close();
				if( conn1 != null )
				 conn1.close();
			}
			catch( SQLException sql )
			{
				logger.error("getCustomerPOC(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getCustomerPOC(String, String, DataSource) - Exception caught in closing poc connection" + sql);
				}
			}
		}

		return custpoc;
	}
	
	
	// Get Customer POC detail for a POC
	
	public static void setCustpocdetail(AddAddendumBean poc , String customer_poc_id , DataSource ds ) throws Exception
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		
		try{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lo_pc_phone1 , lo_pc_email1 from lo_poc where lo_pc_id = "+customer_poc_id;
			rs = stmt.executeQuery(sql);
			if( rs.next() )
			{
				poc.setCustomer_poc_phone( rs.getString( "lo_pc_phone1" ) );
				poc.setCustomer_poc_email( rs.getString( "lo_pc_email1" ) );
			}
			
		}
		
		catch( Exception e ){
			logger.error("setCustpocdetail(AddAddendumForm, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setCustpocdetail(AddAddendumForm, String, DataSource) - Error occured during poc detail");
			}
		}
		
		finally
		{
			try
			{
				if ( rs != null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("setCustpocdetail(AddAddendumForm, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt != null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("setCustpocdetail(AddAddendumForm, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn != null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("setCustpocdetail(AddAddendumForm, String, DataSource) - exception ignored", e);
}
			
		}
		
	}
	
	// Set Addendum detail for an Addendum used in update addendum
	
	public static void setAddendumdetail( AddAddendumBean addendum , String lx_addendum_id , DataSource ds ) throws Exception
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lx_co_addendum_create_date = isnull( convert( varchar( 10 ), lx_co_addendum_create_date , 101 ) , '' ), " +
			"lx_co_addendum_type , lx_co_addendum_status , lo_pc_id , lo_pc_phone1 = isnull( lo_pc_phone1 , '' ) , " +
			"lo_pc_email1 = isnull( lo_pc_email1 , '' ) from lx_co_addendum_main a left outer join lo_poc b on " +
			"a.lx_co_addendum_customer_poc_id = b.lo_pc_id where lx_co_addendum_id = "+lx_addendum_id;
			
			rs = stmt.executeQuery(sql);
			if(rs.next())
			{
				addendum.setLx_addendum_create_date( rs.getString( "lx_co_addendum_create_date" ) );
				addendum.setCustomer_poc_id( rs.getString( "lo_pc_id" ) );
				addendum.setCustomer_poc_phone( rs.getString( "lo_pc_phone1" ) );
				addendum.setCustomer_poc_phone( rs.getString( "lo_pc_email1" ) );
				addendum.setLx_addendum_status( rs.getString( "lx_co_addendum_status" ) );
				addendum.setLx_co_addendum_type( rs.getString( "lx_co_addendum_type" ) );
			}	
			
		}
		
		catch( Exception e ){
			logger.error("setAddendumdetail(AddAddendumForm, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setAddendumdetail(AddAddendumForm, String, DataSource) - Error occured during poc detail");
			}
		}
		
		finally
		{
			try
			{
				if ( rs != null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("setAddendumdetail(AddAddendumForm, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt != null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("setAddendumdetail(AddAddendumForm, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn != null ) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("setAddendumdetail(AddAddendumForm, String, DataSource) - exception ignored", e);
}
			
		}
		
	}
}	
	
	
		

