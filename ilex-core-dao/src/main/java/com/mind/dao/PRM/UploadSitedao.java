package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.UploadCSVBean;
import com.mind.fw.core.dao.util.DBUtil;

public class UploadSitedao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UploadSitedao.class);

	public static int updateSiteDetail(String msaId, String siteDetail,
			String loginUserId, String endCustomer, String lastRow,
			String latitude, String longitude, String latlongAccuracy,
			String latlongSource, DataSource ds, String separator) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int retval = -1;

		siteDetail = siteDetail + separator;
		/*
		 * System.out.println("2::::"+msaId);
		 * System.out.println("3::::"+siteDetail);
		 * System.out.println("4::::"+loginUserId);
		 * System.out.println("5::::"+separator);
		 * System.out.println("6::::"+endCustomer);
		 * System.out.println("7::::"+lastRow );
		 * System.out.println("8::::"+latitude );
		 * System.out.println("9::::"+longitude );
		 * System.out.println("9::::"+latlongAccuracy );
		 * System.out.println("9::::"+latlongSource );
		 */

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.lp_siteupload_manage01(? , ? , ? , ? , ? , ?, ?, ?, ?, ?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, msaId);
			cstmt.setString(3, siteDetail);
			cstmt.setInt(4, Integer.parseInt(loginUserId));
			cstmt.setString(5, separator);
			cstmt.setString(6, endCustomer);
			cstmt.setString(7, lastRow);
			cstmt.setString(8, latitude);
			cstmt.setString(9, longitude);
			cstmt.setString(10, latlongAccuracy);
			cstmt.setString(11, latlongSource);
			cstmt.execute();
			retval = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"updateSiteDetail(String, String, String, String, String, String, String, String, String, DataSource, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateSiteDetail(String, String, String, String, String, String, String, String, String, DataSource, String) - Error occured during update site detail"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error(
						"updateSiteDetail(String, String, String, String, String, String, String, String, String, DataSource, String)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("updateSiteDetail(String, String, String, String, String, String, String, String, String, DataSource, String) - EXception111111111"
							+ e);
				}
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.error(
						"updateSiteDetail(String, String, String, String, String, String, String, String, String, DataSource, String)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("updateSiteDetail(String, String, String, String, String, String, String, String, String, DataSource, String) - EXception222222"
							+ e);
				}
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.error(
						"updateSiteDetail(String, String, String, String, String, String, String, String, String, DataSource, String)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("updateSiteDetail(String, String, String, String, String, String, String, String, String, DataSource, String) - EXception33333333333"
							+ e);
				}
			}

		}
		return retval;
	}

	/**
	 * @author amitm
	 * @param ds
	 * @return
	 */
	public static ArrayList getSite_table_definition(DataSource ds) {
		UploadCSVBean upLoadCSV = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		DecimalFormat dfcurfour = new DecimalFormat("###0.00");

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lp_upload_site_tab_definition()";
			// System.out.println("sql resource list "+sql);
			rs = stmt.executeQuery(sql);
			int i = 0;

			while (rs.next()) {

				upLoadCSV = new UploadCSVBean();
				upLoadCSV.setFieldName(rs.getString("lp_field_name"));
				upLoadCSV.setDataType(rs.getString("lp_data_type"));
				upLoadCSV.setFieldNameLength(rs.getString("lp_length"));
				upLoadCSV.setFieldNameMandatory(rs.getString("lp_mandatory"));
				valuelist.add(i, upLoadCSV);
				i++;
			}

		} catch (Exception e) {
			logger.error("getSite_table_definition(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSite_table_definition(DataSource) - Error occured during table list"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getSite_table_definition(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getSite_table_definition(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getSite_table_definition(DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static UploadError validaterow(String row, UploadError uploaderror,
			int rowno, String[] datatype, String[] datalength,
			String separator, DataSource ds) {
		String[] temp = row.split(separator);

		String type = "";
		int k = 0;
		int j = 0;
		Float num;
		if (temp != null) {
			if (temp.length != datatype.length) // Check for no of columns in
												// the data
			{
				if (temp.length < datatype.length) {
					uploaderror.setRowno(rowno + "");
					uploaderror
							.setRowerror("Number of columns are less than required.");
				} else {
					uploaderror.setRowno(rowno + "");
					uploaderror
							.setRowerror("Number of columns are more than required.");
				}
				return uploaderror;

			} else {
				for (int i = 0; i < temp.length; i++) {
					type = datatype[i];
					if (type.equals("float")) {

						if (temp[i].equals(""))
							temp[i] = "1";

						try {
							num = new Float(temp[i]);
						} catch (Exception e) {
							logger.error(
									"validaterow(String, UploadError, int, String[], String[], String)",
									e);

							k = i + 1;
							uploaderror.setRowno(rowno + "");
							uploaderror
									.setRowerror("Column"
											+ " "
											+ k
											+ " "
											+ "data should be numeric , Insert failed.");
							return uploaderror;
						}

					} else if (temp[10].equals("US")) {

						if (i == 8) {
							if (temp[8].length() <= 2) {
								int statStatus = -1;
								statStatus = getValidateState(temp[8], ds);
								if (statStatus != 1) {
									// k = 8;{
									uploaderror.setRowno(rowno + "");
									uploaderror
											.setRowerror("The state abbreviation is not valid, please enter correct state abbreviation");
									return uploaderror;
								}

							}

							/*
							 * if (temp[8].length() > 2) { k = 8;
							 * 
							 * uploaderror.setRowno(rowno + ""); uploaderror
							 * .setRowerror(
							 * "The state abbreviation is not valid, please enter correct state abbreviation"
							 * );
							 * 
							 * return uploaderror; }
							 */
						}
					} else {

						if (temp[i].length() > Integer.parseInt(datalength[i])) {

							k = i + 1;
							uploaderror.setRowno(rowno + "");
							uploaderror.setRowerror("Column" + " " + k + " "
									+ "data exceed length , Insert failed");
							return uploaderror;
						}

					}
				}
			}
		}
		return uploaderror;
	}

	// get validate us state list

	public static int getValidateState(String stateId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int stateStatus = -1;
		try {
			conn = ds.getConnection();
			String sql = "SELECT 1 FROM cp_state WHERE cp_country_id = 'US' AND cp_state_id = "
					+ "'" + stateId + "'";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				stateStatus = 1;
			}
		} catch (Exception e) {
			logger.error("getValidateState(String, DataSource)", e);
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error("getValidateState(String, DataSource)", e);
			}

		}
		return stateStatus;
	}

	public static String[] getuploadfiledatatype(DataSource ds) {
		String[] datatype = null;
		String temp = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lp_data_type from lp_site_table_defination";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				temp = temp + rs.getString(1) + ",";
			}

		} catch (Exception e) {
			logger.error("getuploadfiledatatype(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getuploadfiledatatype(DataSource) - Error occured getting upload table data type"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getuploadfiledatatype(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getuploadfiledatatype(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getuploadfiledatatype(DataSource) - exception ignored",
						e);
			}

		}
		datatype = temp.split(",");

		return datatype;
	}

	public static String[] getuploadfiledatalength(DataSource ds) {
		String[] datalength = null;
		String temp = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lp_length from lp_site_table_defination";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				temp = temp + rs.getString(1) + ",";
			}

		} catch (Exception e) {
			logger.error("getuploadfiledatalength(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getuploadfiledatalength(DataSource) - Error occured getting upload table data length"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getuploadfiledatalength(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getuploadfiledatalength(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getuploadfiledatalength(DataSource) - exception ignored",
						e);
			}

		}
		datalength = temp.split(",");

		return datalength;
	}

	public static int uploadsitehistory(String loginuserid, String filename,
			String error, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int retval = -1;

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.lp_siteupload_history01( ? , ? , ? )}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, filename);
			cstmt.setString(3, error);
			cstmt.setInt(4, Integer.parseInt(loginuserid));

			cstmt.execute();
			retval = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"uploadsitehistory(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("uploadsitehistory(String, String, String, DataSource) - Error occured during updating site upload history"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error(
						"uploadsitehistory(String, String, String, DataSource)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("uploadsitehistory(String, String, String, DataSource) - EXception111111111"
							+ e);
				}
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.error(
						"uploadsitehistory(String, String, String, DataSource)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("uploadsitehistory(String, String, String, DataSource) - EXception222222"
							+ e);
				}
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.error(
						"uploadsitehistory(String, String, String, DataSource)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("uploadsitehistory(String, String, String, DataSource) - EXception33333333333"
							+ e);
				}
			}

		}
		return retval;
	}

	public static java.util.Collection getMSAEndCustomerList(DataSource ds,
			String msaid) {

		ArrayList endcustomername = new java.util.ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from dbo.func_lx_endcustomer_name('"
							+ msaid + "')");
			while (rs.next()) {

				endcustomername.add(new com.mind.common.LabelValue(rs
						.getString("lo_ot_name"), rs.getString("lo_ot_name")));
			}
		} catch (Exception e) {
			logger.error("getMSAEndCustomerList(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMSAEndCustomerList(DataSource, String) - Exception caught in retrieving  MSA End Customer Name"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMSAEndCustomerList(DataSource, String) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMSAEndCustomerList(DataSource, String) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMSAEndCustomerList(DataSource, String) - exception ignored",
						sql);
			}

		}

		return endcustomername;
	}

}
