package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.Status;
import com.mind.bean.prm.ChangeJOSBean;
import com.mind.fw.core.dao.util.DBUtil;

public class ChangeJOSDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ChangeJOSDao.class);

	/**
	 * @author OM
	 * @param objChangeJOSForm
	 * @param ds
	 * @return
	 */
	public static ArrayList getJobStateList(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select cp_country_id, cp_country_name, cp_country_order from lp_country_list order by cp_country_order,cp_country_name";
			rs = stmt.executeQuery(sql);
			int i = 0;
			if (i == 0) {
				ChangeJOSBean bulkjobform = new ChangeJOSBean();
				bulkjobform.setCountryId("");
				bulkjobform.setCountryName("");
				valuelist.add(i, bulkjobform);
				i++;
			}

			while (rs.next()) {
				ChangeJOSBean bulkjobform = new ChangeJOSBean();
				bulkjobform.setCountryId(rs.getString("cp_country_id"));
				bulkjobform.setCountryName(rs.getString("cp_country_name"));
				valuelist.add(i, bulkjobform);
				i++;
			}

		} catch (Exception e) {
			logger.error("getJobStateList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobStateList(DataSource) - Error occured during getting BulkJob State Category Wise List  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn("getJobStateList(DataSource) - exception ignored",
						e);
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn("getJobStateList(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn("getJobStateList(DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static int upadteJobOwnerScheduler(ChangeJOSBean form,
			String saveJobId, DataSource ds) {
		ChangeJOSBean changeJOSForm = null;
		Connection conn = null;
		Statement stmt = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int value = 0;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String newOwner = "";
			String newScheduler = "";
			if (form.getNewOwner() == null || form.getNewOwner() == "") {
				newOwner = "";
			} else {
				newOwner = form.getNewOwner();
			}

			if (form.getNewScheduler() == null || form.getNewScheduler() == "") {
				newScheduler = "";
			} else {
				newScheduler = form.getNewScheduler();
			}

			if (logger.isDebugEnabled()) {
				logger.debug("upadteJobOwnerScheduler(ChangeJOSForm, String, DataSource) - JobID-- "
						+ saveJobId);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("upadteJobOwnerScheduler(ChangeJOSForm, String, DataSource) - Job Owner-- "
						+ form.getNewOwner());
			}
			if (logger.isDebugEnabled()) {
				logger.debug("upadteJobOwnerScheduler(ChangeJOSForm, String, DataSource) - Scheduler-- "
						+ form.getNewScheduler());
			}
			if (logger.isDebugEnabled()) {
				logger.debug("upadteJobOwnerScheduler(ChangeJOSForm, String, DataSource) - date-- "
						+ form.getPlanedStartDate());
			}

			// String
			// sql="select * from lm_job inner join lm_site_detail on lm_si_id=lm_js_si_id where lm_js_title <> 'Default Job' and lm_js_pr_id ="+123;
			// System.out.println("SQL  ---"+sql);
			cstmt = conn.prepareCall("{?=call lm_bulk_job_manage_01(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, saveJobId);
			cstmt.setString(3, newOwner);
			cstmt.setString(4, newScheduler);
			cstmt.setString(5, form.getPlanedStartDate());
			cstmt.execute();
			// rs = stmt.executeQuery(sql);
			value = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"upadteJobOwnerScheduler(ChangeJOSForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("upadteJobOwnerScheduler(ChangeJOSForm, String, DataSource) - "
						+ e);
			}

		} finally {
			try {
				DBUtil.close(rs, cstmt);

			} catch (Exception e) {
				logger.warn(
						"upadteJobOwnerScheduler(ChangeJOSForm, String, DataSource) - exception ignored",
						e);
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"upadteJobOwnerScheduler(ChangeJOSForm, String, DataSource) - exception ignored",
						e);
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"upadteJobOwnerScheduler(ChangeJOSForm, String, DataSource) - exception ignored",
						e);
			}
		}
		return value;
	}

	public static java.util.Collection getStatusList(String type, DataSource ds) {
		ArrayList groupname = new java.util.ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Status msa = null;
		String sql = "";
		int i = 0;
		groupname.add(new com.mind.common.LabelValue("--Select--", " "));
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			// sql = "select * from dbo.func_get_status_list( '"+type+"' )";
			sql = "select * from lm_job_new_display_status";

			rs = stmt.executeQuery(sql);
			while (rs.next()) {

				groupname.add(new com.mind.common.LabelValue(rs
						.getString("lm_jnds_status"), rs
						.getString("lm_jnds_status")));
			}
		} catch (Exception e) {
			logger.error("getStatusList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStatusList(String, DataSource) - Exception caught in rerieving Status List"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql1) {
				logger.warn(
						"getStatusList(String, DataSource) - exception ignored",
						sql1);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql1) {
				logger.warn(
						"getStatusList(String, DataSource) - exception ignored",
						sql1);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql1) {
				logger.warn(
						"getStatusList(String, DataSource) - exception ignored",
						sql1);
			}

		}
		return groupname;
	}

	public static ArrayList getSchedulerList(String appendixId, DataSource ds) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList schedulerList = new ArrayList();

		// schedulerList.add(new com.mind.common.LabelValue("--select--", "0"));
		schedulerList.add(new com.mind.common.LabelValue("All", "%"));

		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select distinct dbo.func_cc_poc_name(lm_js_scheduler) as poc_name,lm_js_scheduler as poc_id  from lm_job where dbo.func_cc_poc_name(lm_js_scheduler) is not null and lm_js_type not in ('Default','Addendum') and lm_js_id not in (select lm_tc_js_id from lm_ticket) and lm_js_pr_id=? order by poc_name");
			pstmt.setString(1, appendixId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				schedulerList.add(new com.mind.common.LabelValue(rs
						.getString("poc_name"), rs.getString("poc_id")));
			}

		} catch (Exception e) {
			logger.error("getSchedulerList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSchedulerList(String, DataSource) - Error occured com.mind.dao.PRM.ChangeJOSDao.getSchedulerList() "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return schedulerList;
	}

	public static ArrayList getJobOwnerList(String appendixId, DataSource ds) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList schedulerList = new ArrayList();

		// schedulerList.add(new com.mind.common.LabelValue("--select--", "0"));
		schedulerList.add(new com.mind.common.LabelValue("All", "%"));

		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select distinct dbo.func_cc_poc_name(lm_js_created_by) as poc_name,lm_js_created_by as poc_id  from lm_job where dbo.func_cc_poc_name(lm_js_created_by) is not null and lm_js_type not in ('Default','Addendum') and lm_js_id not in (select lm_tc_js_id from lm_ticket) and lm_js_pr_id=? order by poc_name");
			pstmt.setString(1, appendixId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				schedulerList.add(new com.mind.common.LabelValue(rs
						.getString("poc_name"), rs.getString("poc_id")));
			}

		} catch (Exception e) {
			logger.error("getJobOwnerList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobOwnerList(String, DataSource) - Error occured com.mind.dao.PRM.ChangeJOSDao.getjobOwnerList() "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return schedulerList;
	}

	public static ArrayList getJobList(ChangeJOSBean form, DataSource ds) {
		ChangeJOSBean changeJOSForm = null;
		Connection conn = null;
		Statement stmt = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			/*
			 * System.out.println("AppendixID-- "+form.getAppendixid());
			 * System.out.println("Job Owner-- "+form.getNewSearchOwner());
			 * System.out.println("Scheduler-- "+form.getNewSearchScheduler());
			 * System.out.println("Status-- "+form.getJobSelectStatus());
			 * System.out.println("City--"+form.getCity());
			 * System.out.println("State--"+form.getStateCmb());
			 * System.out.println("SiteNumber--"+form.getSiteNumber());
			 * System.out.println("EndCuctomer--"+form.getJobEndCustomer());
			 * System.out.println("Brand--"+form.getBrand());
			 */

			// String
			// sql="select * from lm_job inner join lm_site_detail on lm_si_id=lm_js_si_id where lm_js_title <> 'Default Job' and lm_js_pr_id ="+123;
			// System.out.println("SQL  ---"+sql);
			cstmt = conn
					.prepareCall("{call lm_job_search_list_01(?,?,?,?,?,?,?,?,?)}");
			// cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			cstmt.setString(1, form.getAppendixid());
			cstmt.setString(2, form.getNewSearchOwner());
			cstmt.setString(3, form.getNewSearchScheduler());
			cstmt.setString(4, form.getJobSelectStatus());
			cstmt.setString(5, form.getCity());
			cstmt.setString(6, form.getStateCmb());
			cstmt.setString(7, form.getSiteNumber());
			cstmt.setString(8, form.getJobEndCustomer());
			cstmt.setString(9, form.getBrand());
			rs = cstmt.executeQuery();
			// rs = stmt.executeQuery(sql);
			int i = 0;
			while (rs.next()) {
				changeJOSForm = new ChangeJOSBean();
				changeJOSForm.setJobId(rs.getString("lm_js_id"));
				changeJOSForm.setJobName(rs.getString("lm_js_title"));
				changeJOSForm.setJobSiteNumber(rs.getString("lm_si_number"));
				changeJOSForm.setJobStatus(rs
						.getString("lm_js_prj_display_status"));
				changeJOSForm.setOwnerJob(rs.getString("Owner"));
				changeJOSForm.setJobScheduler(rs.getString("Scheduler"));
				changeJOSForm.setResultPlannedStart(rs
						.getString("lm_js_planned_start_date"));
				// System.out.println(rs.getString("lm_js_planned_start_date"));
				valuelist.add(i, changeJOSForm);
				i++;
			}
		} catch (Exception e) {
			logger.error("getJobList(ChangeJOSForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobList(ChangeJOSForm, DataSource) - " + e);
			}

		} finally {
			try {
				DBUtil.close(rs, cstmt);
			} catch (Exception e) {
				logger.warn(
						"getJobList(ChangeJOSForm, DataSource) - exception ignored",
						e);
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobList(ChangeJOSForm, DataSource) - exception ignored",
						e);
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobList(ChangeJOSForm, DataSource) - exception ignored",
						e);
			}
		}
		return valuelist;
	}

	public static ArrayList getSchedulerList(String appendixId, String status,
			DataSource ds) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList schedulerList = new ArrayList();

		schedulerList.add(new com.mind.common.LabelValue("--Don't Change--",
				"%"));
		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select * from func_lm_job_team_list(?,?) where poc_id <> 0");
			pstmt.setString(1, appendixId);
			pstmt.setString(2, status);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				schedulerList.add(new com.mind.common.LabelValue(rs
						.getString("poc_name"), rs.getString("poc_id")));
			}

		} catch (Exception e) {
			logger.error("getSchedulerList(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSchedulerList(String, String, DataSource) - Error occured com.mind.dao.PRM.AssignOwnerdao.getSchedulerList() "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return schedulerList;
	}

	public static int getJobCount(String appendixId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int value = 0;
		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select * from lm_job where lm_js_pr_id= ? and lm_js_type not in ('Default','Addendum') and lm_js_id not in (select lm_tc_js_id from lm_ticket) ");
			pstmt.setString(1, appendixId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				value++;
			}
		} catch (Exception e) {
			logger.error("getJobCount(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobCount(String, DataSource) - Error occured com.mind.dao.PRM.AssignOwnerdao.getSchedulerList() "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return value;
	}
}
