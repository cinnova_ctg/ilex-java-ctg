package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.EditOOSActivityBean;
import com.mind.common.Util;
public class EditOOSActivitydao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(EditOOSActivitydao.class);

	public static void getOSSActivityData(EditOOSActivityBean EditOOSActivityform, String activityId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
	
	try {	
		conn = ds.getConnection();
		stmt = conn.createStatement();
		
		sql = "select lm_at_title, lm_ao_date_notified, lm_ao_notification_method, lm_ao_cust_name, lm_ao_date_approved, lm_ao_description from dbo.func_get_oos_activity("+activityId+")";
		
		//System.out.println("sql:::::::::::"+sql);
		
		rs= stmt.executeQuery(sql);
		
		if(rs.next()) {
			EditOOSActivityform.setActivityName(rs.getString("lm_at_title"));
			EditOOSActivityform.setDateNotified(rs.getString("lm_ao_date_notified"));
			EditOOSActivityform.setNotificationMethod(rs.getString("lm_ao_notification_method"));
			EditOOSActivityform.setCustName(rs.getString("lm_ao_cust_name"));
			EditOOSActivityform.setDateApproved(rs.getString("lm_ao_date_approved"));
			EditOOSActivityform.setDescription(rs.getString("lm_ao_description"));
		}
	}
	
	catch( Exception e )
	{
			logger.error("getOSSActivityData(EditOOSActivityForm, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOSSActivityData(EditOOSActivityForm, String, DataSource) - Exception caught in rerieving OSS Activity Data " + e);
			}
	}
	finally
	{
		
		try
		{
			if ( rs!= null ) 
			{
				rs.close();
			}
		}
		catch( SQLException s )
		{
				logger.warn("getOSSActivityData(EditOOSActivityForm, String, DataSource) - exception ignored", s);
}
		
		
		try
		{
			if ( stmt!= null ) 
			{
				stmt.close();
			}
		}
		catch( SQLException s )
		{
				logger.warn("getOSSActivityData(EditOOSActivityForm, String, DataSource) - exception ignored", s);
}
		
		
		try
		{
			if ( conn!= null ) 
			{
				conn.close();
			}
		}
		catch( SQLException s )
		{
				logger.warn("getOSSActivityData(EditOOSActivityForm, String, DataSource) - exception ignored", s);
}
		
	}
  }

	public static ArrayList getNotificationMethodList(DataSource ds)
	{
		ArrayList notificationMethodList = new ArrayList();
		try
		{
			notificationMethodList.add( new com.mind.common.LabelValue( "---Select---" , "") );
			notificationMethodList.add ( new com.mind.common.LabelValue( "Email", "Email") );
			notificationMethodList.add ( new com.mind.common.LabelValue( "Phone", "Phone") );
			notificationMethodList.add ( new com.mind.common.LabelValue( "Fax", "Fax") );
		}
		catch( Exception e )
		{
			logger.error("getNotificationMethodList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNotificationMethodList(DataSource) - Exception caught" + e);
			}
		}
		return notificationMethodList;
	}
	
	public static int updateOOSActivity(String jobId, String activityId, String activityTitle, String dateNotified, String notificationMethod, String custName, String dateApproved, String description,String userid, DataSource ds)
	{
		Connection conn = null;
		CallableStatement cstmt = null;
		Util util = new Util();
		ResultSet rs = null;
		int flag = -1;
		
		//System.out.println("2:::"+Integer.parseInt(jobId));
		//System.out.println("3:::"+Integer.parseInt(activityId));
		//System.out.println("4:::"+activityTitle);
		//System.out.println("5:::"+dateNotified);								
		//System.out.println("6:::"+notificationMethod);
		//System.out.println("7:::"+custName);
		//System.out.println("8:::"+dateApproved);
	
		try {
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call lm_oos_activity_manage_01(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(jobId));
			cstmt.setInt(3, Integer.parseInt(activityId));
			cstmt.setString(4, activityTitle);
			cstmt.setString(5, dateNotified);								
			cstmt.setString(6, notificationMethod);
			cstmt.setString(7, custName);
			cstmt.setString(8, dateApproved);
			cstmt.setString(9, description);
			cstmt.setString(10, userid);
			cstmt.execute();
			
			flag = cstmt.getInt( 1 );	
		}
		
		catch( Exception e ){
			logger.error("updateOOSActivity(String, String, String, String, String, String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateOOSActivity(String, String, String, String, String, String, String, String, String, DataSource) - Error occured during inserting powo detail data  " + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("updateOOSActivity(String, String, String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if (cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("updateOOSActivity(String, String, String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("updateOOSActivity(String, String, String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
			
		}
		return flag;	
	}
	
}
