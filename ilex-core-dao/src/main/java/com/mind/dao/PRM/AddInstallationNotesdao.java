package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class AddInstallationNotesdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(AddInstallationNotesdao.class);

	public static int addinstallnotes(String lm_in_js_id, String lm_in_notes,
			String lm_user, DataSource ds, String lm_date,
			String preScheduleId, String newScheduleId, String scheduleDateType) {

		Connection conn = null;
		CallableStatement cstmt = null;
		String typeId = "0";
		// ResultSet rs=null;
		int retval = 0;
		try {
			conn = ds.getConnection();

			/*
			 * System.out.println(lm_in_js_id); System.out.println(lm_in_notes);
			 * System.out.println(lm_user); System.out.println(lm_date);
			 * System.out.println(preScheduleId);
			 * System.out.println(newScheduleId);
			 * System.out.println(scheduleDateType);
			 */

			// cstmt=conn.prepareCall("{?=call lm_install_notes_manage_01(?,?,?,?)}");
			cstmt = conn
					.prepareCall("{?=call lm_install_notes_manage_01(?,?,?,?,?,?,?,?)}"); // Added
																							// 3
																							// new
																							// fields
																							// to
																							// link
																							// lx_schedule_element
																							// and
																							// lm_install_notes_seema
																							// tables.
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, lm_in_js_id);
			cstmt.setString(3, lm_in_notes);
			cstmt.setString(4, lm_user);
			cstmt.setString(5, lm_date);
			cstmt.setString(6, preScheduleId);
			cstmt.setString(7, newScheduleId);
			cstmt.setString(8, scheduleDateType);
			cstmt.setString(9, typeId);

			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"addinstallnotes(String, String, String, DataSource, String, String, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addinstallnotes(String, String, String, DataSource, String, String, String, String) - Error occured during adding addinstallnotes"
						+ e);
			}
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"addinstallnotes(String, String, String, DataSource, String, String, String, String) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"addinstallnotes(String, String, String, DataSource, String, String, String, String) - exception ignored",
						e);
			}

		}
		return retval;

	}

	public static int addinstallnotes(String lm_in_js_id, String lm_in_notes,
			String lm_user, DataSource ds, String lm_date,
			String preScheduleId, String newScheduleId,
			String scheduleDateType, String typeId) {

		Connection conn = null;
		CallableStatement cstmt = null;
		// ResultSet rs=null;
		int retval = 0;
		try {
			conn = ds.getConnection();

			if (StringUtils.isEmpty(typeId)) {
				typeId = "0";
			}

			/*
			 * System.out.println(lm_in_js_id); System.out.println(lm_in_notes);
			 * System.out.println(lm_user); System.out.println(lm_date);
			 * System.out.println(preScheduleId);
			 * System.out.println(newScheduleId);
			 * System.out.println(scheduleDateType);
			 */

			// cstmt=conn.prepareCall("{?=call lm_install_notes_manage_01(?,?,?,?)}");
			cstmt = conn
					.prepareCall("{?=call lm_install_notes_manage_01(?,?,?,?,?,?,?,?)}"); // Added
																							// 3
																							// new
																							// fields
																							// to
																							// link
																							// lx_schedule_element
																							// and
																							// lm_install_notes_seema
																							// tables.
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, lm_in_js_id);
			cstmt.setString(3, lm_in_notes);
			cstmt.setString(4, lm_user);
			cstmt.setString(5, lm_date);
			cstmt.setString(6, preScheduleId);
			cstmt.setString(7, newScheduleId);
			cstmt.setString(8, scheduleDateType);
			cstmt.setString(9, typeId);

			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"addinstallnotes(String, String, String, DataSource, String, String, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addinstallnotes(String, String, String, DataSource, String, String, String, String) - Error occured during adding addinstallnotes"
						+ e);
			}
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"addinstallnotes(String, String, String, DataSource, String, String, String, String) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"addinstallnotes(String, String, String, DataSource, String, String, String, String) - exception ignored",
						e);
			}

		}
		return retval;

	}

	public static String getAppendixname(String appendixid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String appendixname = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lx_pr_title from lx_appendix_main where lx_pr_id="
					+ appendixid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				appendixname = rs.getString("lx_pr_title");
			}

		} catch (Exception e) {
			logger.error("getAppendixname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixname(String, DataSource) - Error occured during getting Appendix name  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

		}
		return appendixname;
	}

	public static String getJobname(String jobid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String jobname = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_js_title from lm_job where lm_js_id="
					+ jobid + " ";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				jobname = rs.getString("lm_js_title");
			}

		} catch (Exception e) {
			logger.error("getJobname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobname(String, DataSource) - Error occured during getting Job name  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobname(String, DataSource) - exception ignored", e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobname(String, DataSource) - exception ignored", e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobname(String, DataSource) - exception ignored", e);
			}

		}
		return jobname;
	}

}
