package com.mind.dao.PRM;

public class CustomerInfoBean {

	private String infoId = null;
	private String infoParameter = null;
	private String value = null;
	
	public String getInfoId() {
		return infoId;
	}
	public void setInfoId(String infoId) {
		this.infoId = infoId;
	}
	public String getInfoParameter() {
		return infoParameter;
	}
	public void setInfoParameter(String infoParameter) {
		this.infoParameter = infoParameter;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
