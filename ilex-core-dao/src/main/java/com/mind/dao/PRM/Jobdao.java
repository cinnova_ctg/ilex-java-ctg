/**
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This is a data object access class used for managing Job Dashboard and showing ActivityDetailList under the Job.      
 *
 */
package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.JobCompleteBean;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * Methods :
 * getActivitydetaillist,getCurrentstatus,getJobschedule,getAppendixid,
 * getJobname,getPLsummary
 */

public class Jobdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Jobdao.class);

	public static String getCurrentstatus(String jobid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String currentstatus = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_js_prj_status from lm_job where lm_js_id="
					+ jobid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				currentstatus = rs.getString("lm_js_prj_status");
			}

		} catch (Exception e) {
			logger.error("getCurrentstatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCurrentstatus(String, DataSource) - Error occured during getting current status  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCurrentstatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCurrentstatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCurrentstatus(String, DataSource) - exception ignored",
						e);
			}

		}
		return currentstatus;
	}

	public static String getCheck_for_jobactivity_project_status(String type,
			String id, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String check_for_jobactivity_project_status = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select dbo.func_check_for_jobactivity_project_status( '"
					+ type + "' , '" + id + "' )";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				check_for_jobactivity_project_status = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error(
					"getCheck_for_jobactivity_project_status(String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCheck_for_jobactivity_project_status(String, String, DataSource) - Error occured during getting check_for_jobactivity_project_status");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCheck_for_jobactivity_project_status(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCheck_for_jobactivity_project_status(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCheck_for_jobactivity_project_status(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return check_for_jobactivity_project_status;
	}

	public static String[] getJobschedule(String jobid, String type,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] value = new String[2];

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select  lx_se_start,lx_se_end from lx_schedule_list_01 where lx_se_type_id="
					+ jobid + " and lx_se_type = '" + type + "'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				value[0] = rs.getString("lx_se_start");
				value[1] = rs.getString("lx_se_end");
			}

		} catch (Exception e) {
			logger.error("getJobschedule(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobschedule(String, String, DataSource) - Error occured during getting jobschedule  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobschedule(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobschedule(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobschedule(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return value;
	}

	public static String getAppendixid(String jobid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String Appendixid = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_js_pr_id from lm_job where lm_js_id="
					+ jobid + " ";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				Appendixid = rs.getString("lm_js_pr_id");
			}

		} catch (Exception e) {
			logger.error("getAppendixid(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixid(String, DataSource) - Error occured during getting Job name  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAppendixid(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixid(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixid(String, DataSource) - exception ignored",
						e);
			}

		}
		return Appendixid;
	}

	public static String getJobname(String jobid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String jobname = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_js_title from lm_job where lm_js_id="
					+ jobid + " ";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				jobname = rs.getString("lm_js_title");
			}

		} catch (Exception e) {
			logger.error("getJobname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobname(String, DataSource) - Error occured during getting Job name  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobname(String, DataSource) - exception ignored", e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobname(String, DataSource) - exception ignored", e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobname(String, DataSource) - exception ignored", e);
			}

		}
		return jobname;
	}

	// job type name is Default/Newjob
	public static String getJobtypename(String jobid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String jobtypename = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_js_type from lm_job where lm_js_id="
					+ jobid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				jobtypename = rs.getString("lm_js_type");
			}

		} catch (Exception e) {
			logger.error("getJobtypename(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobtypename(String, DataSource) - Error occured during getting Job type name");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobtypename(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobtypename(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobtypename(String, DataSource) - exception ignored",
						e);
			}

		}
		return jobtypename;
	}

	public static String getJobStatusName(JobCompleteBean jobcompleteBean,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String jobstatusname = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_js_status,lm_view_type from dbo.func_lp_prj_jobdashboard_tab( '%' , '"
					+ jobcompleteBean.getJobId() + "' , '%'  )";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				jobcompleteBean.setJobStatus(rs.getString("lm_js_status"));
				jobcompleteBean.setViewType(rs.getString("lm_view_type"));
			}

		} catch (Exception e) {
			logger.error("getJobStatusName(JobCompleteForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobStatusName(JobCompleteForm, DataSource) - Error occured during getting Job status name");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobStatusName(JobCompleteForm, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobStatusName(JobCompleteForm, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobStatusName(JobCompleteForm, DataSource) - exception ignored",
						e);
			}

		}
		return jobstatusname;
	}

	public static void checkJobCompleteStatus(JobCompleteBean jobcompleteBean,
			DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lm_job_completion_check(?) }");

			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2, jobcompleteBean.getJobId());
			rs = cstmt.executeQuery();
			while (rs.next()) {
				jobcompleteBean.setScheduleCheck(rs.getString(1));
				jobcompleteBean
						.setOutStandingDelieverableCheck(rs.getString(2));
				jobcompleteBean.setExpense(rs.getString(3));
				jobcompleteBean.setRevenue(rs.getString(4));
				jobcompleteBean.setInstallNotesCheck(rs.getString(5));
				jobcompleteBean.setCustomerReferenceCheck(rs.getString(6));
				jobcompleteBean.setResourceAllocation(rs.getString(7));
				jobcompleteBean.setSiteContactInfo(rs.getString(8));
				jobcompleteBean.setOverAllStatus(rs.getString(9));
			}
		} catch (Exception e) {
			logger.error("checkJobCompleteStatus(JobCompleteForm, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkJobCompleteStatus(JobCompleteForm, DataSource) - Error Occured in getting job complete status "
						+ e);
			}
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"checkJobCompleteStatus(JobCompleteForm, DataSource) - exception ignored",
						sql);
			}
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"checkJobCompleteStatus(JobCompleteForm, DataSource) - exception ignored",
						sql);
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"checkJobCompleteStatus(JobCompleteForm, DataSource) - exception ignored",
						sql);
			}
		}
	}

	public static void updateJobOffSiteDate(String jobId, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lm_job_offsite_date( ?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, jobId);
			cstmt.execute();
		} catch (SQLException e) {
			logger.error("updateJobOffSiteDate(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateJobOffSiteDate(String, DataSource) - Exception caught in updating job offsite date "
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error("updateJobOffSiteDate(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("updateJobOffSiteDate(String, DataSource) - Exception in updating job offsite date"
							+ sql);
				}
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("updateJobOffSiteDate(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("updateJobOffSiteDate(String, DataSource) - Exception in updating job offsite date"
							+ sql);
				}
			}
		}
	}

	public static String getFinancialFlagCheckforJobCompletion(String jobId,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String checkStatus = "N";

		try {
			conn = ds.getConnection();
			String sql = "select dbo.func_lm_get_financial_check_flag(?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, jobId);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				checkStatus = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error(
					"getFinancialFlagCheckforJobCompletion(String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getFinancialFlagCheckforJobCompletion(String, DataSource) - Error occured during getting financial check flag for job completion");
			}
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return checkStatus;
	}

	public static String getCheckForJobMspHelp(String id, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String checkStatus = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select dbo.lm_job_msp_help_check( '" + id + "' )";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				checkStatus = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("getCheckForJobMspHelp(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCheckForJobMspHelp(String, DataSource) - Error occured during getting msp check for a ticket");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCheckForJobMspHelp(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCheckForJobMspHelp(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCheckForJobMspHelp(String, DataSource) - exception ignored",
						e);
			}

		}
		return checkStatus;
	}

	/**
	 * Gets the custom field status.
	 * 
	 * @param jobId
	 *            the job id
	 * @param appendixId
	 *            the appendix id
	 * @param ds
	 *            the ds
	 * @return the custom field values Count
	 */
	public static int getCustomFieldCount(String jobId, String appendixId,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int customerFieldValueCount = 0;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			StringBuilder sqlQuery = new StringBuilder(
					"SELECT COUNT(lm_ap_cust_info_parameter) as customfieldcount");
			sqlQuery.append(" from lm_appendix_customer_info ");
			sqlQuery.append(" where lm_ap_cust_info_pr_id= '" + appendixId
					+ "' and (lm_ap_cust_info_parameter");
			sqlQuery.append("!=null or lm_ap_cust_info_parameter!='')");
			rs = stmt.executeQuery(sqlQuery.toString());
			if (rs.next()) {
				if (rs.getInt("customfieldcount") > 0) {
					Connection conn2 = null;
					Statement stmt2 = null;
					ResultSet rs2 = null;
					try {
						conn2 = ds.getConnection();
						stmt2 = conn2.createStatement();
						StringBuilder sqlQueryInner = new StringBuilder(
								"SELECT COUNT(lm_ci_value) as customfieldvaluescount");
						sqlQueryInner
								.append(" from  lm_appendix_customer_info ");
						sqlQueryInner
								.append(" left outer join lm_customer_info on  lm_ap_cust_info_id=lm_ci_ap_cust_info_id ");
						sqlQueryInner.append(" and lm_ci_js_id = '" + jobId
								+ "'  where lm_ap_cust_info_pr_id='"
								+ appendixId + "' ");
						sqlQueryInner
								.append(" and (lm_ci_value!=null or lm_ci_value!='')");
						rs2 = stmt2.executeQuery(sqlQueryInner.toString());
						if (rs2.next()) {
							if (rs2.getInt("customfieldvaluescount") > 0) {
								customerFieldValueCount = rs2
										.getInt("customfieldvaluescount");
							} else {
								customerFieldValueCount = 0;
							}
						}

					} catch (Exception e) {
						logger.error(
								"getCustomFieldStatus(String,String, DataSource)",
								e);

						if (logger.isDebugEnabled()) {
							logger.debug("getCustomFieldStatus(String,String, DataSource) - Error occured during getting number of non- empty custom field values ");
						}
					}

					finally {
						try {
							if (rs2 != null) {
								rs2.close();
							}
						}

						catch (Exception e) {
							logger.warn(
									"getCustomFieldStatus(String,String, DataSource) - exception ignored",
									e);
						}

						try {
							if (stmt2 != null)
								stmt2.close();
						} catch (Exception e) {
							logger.warn(
									"getCustomFieldStatus(String,String, DataSource) - exception ignored",
									e);
						}

						try {
							if (conn2 != null)
								conn2.close();
						} catch (Exception e) {
							logger.warn(
									"getCustomFieldStatus(String,String, DataSource) - exception ignored",
									e);
						}

					}

				} else {
					customerFieldValueCount = 0;
				}
			}

		} catch (Exception e) {
			logger.error("getCustomFieldStatus(String,String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomFieldStatus(String,String, DataSource) - Error occured during getting custom field status");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCustomFieldStatus(String,String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCustomFieldStatus(String,String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCustomFieldStatus(String,String, DataSource) - exception ignored",
						e);
			}

		}
		return customerFieldValueCount;
	}
}
