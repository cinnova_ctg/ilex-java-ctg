package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.ChecklistTabBean;
import com.mind.common.IlexConstants;

public class ChecklistTabdao {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger
            .getLogger(ChecklistTabdao.class);

    public static ArrayList getActivityList(String Job_Id, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        CallableStatement cstmt = null;
        ArrayList valuelist = new ArrayList();

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "select lm_at_id , lm_at_title , lm_at_prj_status = dbo.func_status_description(isnull(lm_at_prj_status,'I')) , "
                    + "label = case lm_at_prj_status when 'C' then 'N/A' else 'Yes' end from lm_activity where lm_at_js_id ="
                    + Job_Id;

            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ChecklistTabBean ctBean = new ChecklistTabBean();

                ctBean.setActivityid(rs.getString("lm_at_id"));
                ctBean.setActivityname(rs.getString("lm_at_title"));
                ctBean.setActivitystatus(rs.getString("lm_at_prj_status"));
                ctBean.setLabel(rs.getString("label"));
                valuelist.add(ctBean);
            }
        } catch (Exception e) {
            logger.error("getActivityList(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getActivityList(String, DataSource) - Error occured during getting ActivityList"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getActivityList(String, DataSource) - exception ignored",
                        e);

            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getActivityList(String, DataSource) - exception ignored",
                        e);

            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getActivityList(String, DataSource) - exception ignored",
                        e);

            }

        }
        return valuelist;
    }

    public static String[] getSavedSiteChecklist(String Job_id, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        ArrayList valuelist = new ArrayList();
        String[] siteckecklistid = null;
        String temp = "";
        int i = 0;
        int rows = 0;

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            String sql1 = "select count(*) from lm_complete_activity_list_01 where lm_at_js_id="
                    + Job_id;
            rs = stmt.executeQuery(sql1);
            if (rs.next()) {
                rows = rs.getInt(1);
            }
            siteckecklistid = new String[rows];

            String sql = "select lm_ca_at_id from lm_complete_activity_list_01 where lm_at_js_id="
                    + Job_id;
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                siteckecklistid[i] = rs.getString("lm_ca_at_id");
                i++;
            }
        } catch (Exception e) {
            logger.error("getSavedSiteChecklist(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getSavedSiteChecklist(String, DataSource) - Error occured during getting getSavedSiteChecklist"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getSavedSiteChecklist(String, DataSource) - exception ignored",
                        e);

            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getSavedSiteChecklist(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getSavedSiteChecklist(String, DataSource) - exception ignored",
                        e);
            }

        }
        return siteckecklistid;
    }

    public static int addSiteChecklist(String activity_id_list, String job_id,
            String firstvisitsuccess, String jobCompleteSuccess,
            String siteJobProfessional, String customerFollowup,
            String pocTech, String pocNumber, String pocComments, DataSource ds) {
        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;

        int retval = -1;
        try {
            conn = ds.getConnection();
            // System.out.println(
            // "addSiteChecklist: activityidlist'"+activity_id_list+"'+");
            cstmt = conn
                    .prepareCall("{?=call lm_complete_activity_manage_01( ? , ? , ? ,? ,? ,?,?,?,?  )}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setString(2, activity_id_list);
            cstmt.setString(3, job_id);
            cstmt.setString(4, firstvisitsuccess);
            cstmt.setString(5, jobCompleteSuccess);
            cstmt.setString(6, siteJobProfessional);
            cstmt.setString(7, customerFollowup);
            cstmt.setString(8, pocTech);
            cstmt.setString(9, pocNumber);
            cstmt.setString(10, pocComments);

            cstmt.execute();
            retval = cstmt.getInt(1);

        } catch (Exception e) {
            logger.error(
                    "addSiteChecklist(String, String, String, String, String, String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("addSiteChecklist(String, String, String, String, String, String, DataSource) - Error occured during addSiteChecklist "
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    // Start :Added By Amit
                    rs = null;
                    // End
                }
            } catch (Exception e) {
                logger.warn(
                        "addSiteChecklist(String, String, String, String, String, String, DataSource) - exception ignored",
                        e);

            }
            try {
                if (cstmt != null) {
                    cstmt.close();
                    // Start :Added By Amit
                    cstmt = null;
                    // End
                }

            } catch (Exception e) {
                logger.warn(
                        "addSiteChecklist(String, String, String, String, String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                    // Start :Added By Amit
                    conn = null;
                    // End
                }
            } catch (Exception e) {
                logger.warn(
                        "addSiteChecklist(String, String, String, String, String, String, DataSource) - exception ignored",
                        e);
            }

        }
        return retval;

    }

    public static ArrayList getPartnerQCchecklist(DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        CallableStatement cstmt = null;
        ArrayList valuelist = new ArrayList();
        String opt[] = null;
        String optid[] = null;
        ArrayList arr = null;
        int i = 0;
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            cstmt = conn.prepareCall("{?=call lm_master_qc_checklist() }");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

            rs = cstmt.executeQuery();

            while (rs.next()) {
                arr = new ArrayList();
                ChecklistTabBean ctBean = new ChecklistTabBean();

                ctBean.setGroupname(rs.getString("lm_ci_group"));
                ctBean.setItemname(rs.getString("lm_ci_item_name"));
                ctBean.setOptiontype(rs.getString("lm_ci_option_type"));
                ctBean.setOption(rs.getString("lm_co_option"));
                ctBean.setOptioncheckbox(rs.getString("lm_co_option"));
                ctBean.setOptionid(rs.getString("lm_co_id"));

                opt = rs.getString("lm_co_option").split("-");
                optid = rs.getString("lm_co_id").split("-");

                ctBean.setOptionsperques(opt.length + "");
                ctBean.setPercent(rs.getString("lm_ci_percent"));

                valuelist.add(ctBean);
                i++;
            }
        } catch (Exception e) {
            logger.error("getPartnerQCchecklist(DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getPartnerQCchecklist(DataSource) - Error occured during getPartnerQCchecklist"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnerQCchecklist(DataSource) - exception ignored",
                        e);

            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnerQCchecklist(DataSource) - exception ignored",
                        e);

            }

            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnerQCchecklist(DataSource) - exception ignored",
                        e);

            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnerQCchecklist(DataSource) - exception ignored",
                        e);
            }

        }
        return valuelist;
    }

    public static String getQuesOptions(String partneridlist, String Job_Id,
            DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String QuesOptions = "";
        int flag = 0;
        String[] partnerid = null;
        int i = 0;
        String sql = "";
        if (partneridlist != null) {
            partnerid = partneridlist.split("~");
        }

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            for (int j = 0; j < partnerid.length; j++) {
                flag = 0;
                if (partnerid[j].equals("")) {
                    partnerid[j] = "0";
                }
                if (partnerid[j] != null
                        && !partnerid[j].trim().equalsIgnoreCase("")
                        && !partnerid[j].trim().equalsIgnoreCase("0")) {
                    sql = "select lm_qcp_option_id from lm_qc_checklist_partner where isnull( lm_qcp_js_id , 0 ) = "
                            + Job_Id + " and lm_qcp_partner_id=" + partnerid[j];
                    rs = stmt.executeQuery(sql);

                    if (rs != null) {
                        while (rs.next()) {
                            QuesOptions = QuesOptions
                                    + rs.getString("lm_qcp_option_id") + "-";
                            flag = 1;
                            i++;

                        }
                        if (QuesOptions.length() > 0 && flag == 1) {
                            QuesOptions = QuesOptions.substring(0,
                                    QuesOptions.length() - 1);
                        }
                    } else {

                    }
                    QuesOptions = QuesOptions + "~";
                }
            }

            if (QuesOptions.length() > 0) {
                QuesOptions = QuesOptions
                        .substring(0, QuesOptions.length() - 1);
            }

            // QuesOptions="20-7-6-16-15~~12-6-16-15";
        } catch (Exception e) {
            logger.error("getQuesOptions(String, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getQuesOptions(String, String, DataSource) - Error occured during getting getQuesOptions"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getQuesOptions(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getQuesOptions(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getQuesOptions(String, String, DataSource) - exception ignored",
                        e);
            }

        }
        return QuesOptions;
    }

    public static String getFirstvisitsuccessPartner(String partneridlist,
            String jobid, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String firstvisitsuccess = "";
        String[] partnerid = null;
        int i = 0;
        String sql = "";
        if (partneridlist != null) {
            partnerid = partneridlist.split("~");
        }

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            for (int j = 0; j < partnerid.length; j++) {

                if (partnerid[j].equals("")) {
                    partnerid[j] = "0";
                }
                sql = "select lm_fi_vi_flag = isnull( lm_fi_vi_flag , '' ) from lm_first_visit_success where lm_fi_vi_js_id = "
                        + jobid + " and lm_fi_vi_partner_id = " + partnerid[j];

                // System.out.println("SQL "+sql);
                rs = stmt.executeQuery(sql);

                if (rs != null) {
                    while (rs.next()) {
                        firstvisitsuccess = firstvisitsuccess
                                + rs.getString("lm_fi_vi_flag") + "~";
                    }
                } else {
                    firstvisitsuccess = firstvisitsuccess + "" + "~";
                }
            }
        } catch (Exception e) {
            logger.error(
                    "getFirstvisitsuccessPartner(String, String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("getFirstvisitsuccessPartner(String, String, DataSource) - Error occured during getting getQuesOptions"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getFirstvisitsuccessPartner(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getFirstvisitsuccessPartner(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getFirstvisitsuccessPartner(String, String, DataSource) - exception ignored",
                        e);
            }

        }

        return firstvisitsuccess;
    }

    // public static String getUserRating(String partneridlist, String jobid,
    // DataSource ds) {
    //
    // Connection conn = null;
    // Statement stmt = null;
    // ResultSet rs = null;
    // String userRating = "";
    // String[] partnerid = null;
    // int i = 0;
    // String sql = "";
    // if (partneridlist != null) {
    // partnerid = partneridlist.split("~");
    // }
    //
    // try {
    // conn = ds.getConnection();
    // stmt = conn.createStatement();
    //
    // for (int j = 0; j < partnerid.length; j++) {
    //
    // if (partnerid[j].equals("")) {
    // partnerid[j] = "0";
    // }
    // sql =
    // " SELECT isnull(qc_pj_user_rating,'N') qc_pj_user_rating FROM qc_partner_job_rating"
    // + " WHERE qc_pj_lm_js_id = "
    // + jobid
    // + " AND qc_pj_partner_cp_partner_id = " + partnerid[j];
    //
    // // System.out.println("SQL "+sql);
    // rs = stmt.executeQuery(sql);
    //
    // if (rs != null) {
    // while (rs.next()) {
    // userRating = userRating
    // + rs.getString("qc_pj_user_rating") + "~";
    // }
    // } else {
    // userRating = userRating + "" + "~";
    // }
    // }
    // } catch (Exception e) {
    // logger.error("getUserRating(String, String, DataSource)", e);
    //
    // if (logger.isDebugEnabled()) {
    // logger.debug("getUserRating(String, String, DataSource) - Error occured during getting getQuesOptions"
    // + e);
    // }
    // }
    //
    // finally {
    // try {
    // if (rs != null) {
    // rs.close();
    // }
    // }
    //
    // catch (Exception e) {
    // logger.warn(
    // "getUserRating(String, String, DataSource) - exception ignored",
    // e);
    // }
    //
    // try {
    // if (stmt != null)
    // stmt.close();
    // } catch (Exception e) {
    // logger.warn(
    // "getUserRating(String, String, DataSource) - exception ignored",
    // e);
    // }
    //
    // try {
    // if (conn != null)
    // conn.close();
    // } catch (Exception e) {
    // logger.warn(
    // "getUserRating(String, String, DataSource) - exception ignored",
    // e);
    // }
    //
    // }
    //
    // return userRating;
    // }
    public static String getTechFutureRecommad(String partneridlist,
            String jobid, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String futureJobRecommended = "";
        String[] partnerid = null;
        int i = 0;
        String sql = "";
        if (partneridlist != null) {
            partnerid = partneridlist.split("~");
        }

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            for (int j = 0; j < partnerid.length; j++) {

                if (partnerid[j].equals("")) {
                    partnerid[j] = "0";
                }
                sql = " SELECT isnull(qc_pj_future_jobs,'N') qc_pj_future_jobs FROM qc_partner_job_rating"
                        + " WHERE qc_pj_lm_js_id = "
                        + jobid
                        + " AND qc_pj_partner_cp_partner_id = " + partnerid[j];
                // System.out.println("SQL "+sql);
                rs = stmt.executeQuery(sql);

                if (rs != null) {
                    while (rs.next()) {
                        futureJobRecommended = futureJobRecommended
                                + rs.getString("qc_pj_future_jobs") + "~";
                    }
                } else {
                    futureJobRecommended = futureJobRecommended + "" + "~";
                }
            }
        } catch (Exception e) {
            logger.error("getTechFutureRecommad(String, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getTechFutureRecommad(String, String, DataSource) - Error occured during getting getQuesOptions"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getTechFutureRecommad(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getTechFutureRecommad(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getTechFutureRecommad(String, String, DataSource) - exception ignored",
                        e);
            }

        }

        return futureJobRecommended;
    }

    public static String getOperationRattingComments(String partneridlist,
            String jobid, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String oprRattingComments = "";
        String[] partnerid = null;
        int i = 0;
        String sql = "";
        if (partneridlist != null) {
            partnerid = partneridlist.split("~");
        }

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            for (int j = 0; j < partnerid.length; j++) {

                if (partnerid[j].equals("")) {
                    partnerid[j] = "0";
                }
                sql = " SELECT isnull(qc_pj_op_rating_Comments,'') qc_pj_op_rating_Comments FROM qc_partner_job_rating"
                        + " WHERE qc_pj_lm_js_id = "
                        + jobid
                        + " AND qc_pj_partner_cp_partner_id = " + partnerid[j];
                // System.out.println("SQL "+sql);
                rs = stmt.executeQuery(sql);

                if (rs != null) {
                    while (rs.next()) {
                        oprRattingComments = oprRattingComments
                                + rs.getString("qc_pj_op_rating_Comments")
                                + "~";
                    }
                } else {
                    oprRattingComments = oprRattingComments + "" + "~";
                }
            }
        } catch (Exception e) {
            logger.error(
                    "getOperationRattingComments(String, String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("getOperationRattingComments(String, String, DataSource) - Error occured during getting getQuesOptions"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getOperationRattingComments(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getOperationRattingComments(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getOperationRattingComments(String, String, DataSource) - exception ignored",
                        e);
            }

        }

        return oprRattingComments;
    }

    public static int addPartnerQcChecklist(String partner_id_list,
            String optionid, String lm_sc_user,
            String firstvisitpartnersuccess, String jobid,
            String jobCompletedFlag, DataSource ds) {
        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;

        int jbid = 0;
        if ((jobid != null) || (!"".equals(jobid))) {
            jbid = Integer.parseInt(jobid);
        }

        /*
         * System.out.println( "in SP partner_id_list: '"+partner_id_list+"'");
         * System.out.println( "in SP optionid: '"+optionid+"'");
         * System.out.println( "in SP lm_sc_user: "+lm_sc_user+"");
         * System.out.println( "in SP jobid: '"+jobid+"'"); System.out.println(
         * "in SP firstvisitpartnersuccess: '"+firstvisitpartnersuccess+"'");
         */
        int retval = -1;
        try {
            conn = ds.getConnection();

            cstmt = conn
                    .prepareCall("{?=call lm_partner_qc_checklist_manage_01( ?,?,?,?,?, ?)}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setString(2, partner_id_list);
            cstmt.setString(3, optionid);
            cstmt.setInt(4, Integer.parseInt(lm_sc_user));
            cstmt.setInt(5, jbid);
            cstmt.setString(6, firstvisitpartnersuccess);
            cstmt.setString(7, jobCompletedFlag);

            // System.out.println("::::::::::::::::    '"+partner_id_list+"' , '"+optionid+"' , "+Integer.parseInt(
            // lm_sc_user
            // )+" , '"+jobid+"' , '"+firstvisitpartnersuccess+"' ,'"+jobCompletedFlag+"'");
            cstmt.execute();
            retval = cstmt.getInt(1);
        } catch (Exception e) {
            logger.error("addPartnerQcChecklist( " + partner_id_list + " , "
                    + optionid + ", " + lm_sc_user + " , "
                    + firstvisitpartnersuccess + " , " + jobid + " , "
                    + jobCompletedFlag + ", " + ds + " )", e);

            if (logger.isDebugEnabled()) {
                logger.debug("addPartnerQcChecklist(String, String, String, String, String, DataSource) - Error occured during addPartnerQcChecklist  "
                        + partner_id_list
                        + " , "
                        + optionid
                        + ", "
                        + lm_sc_user
                        + " , "
                        + firstvisitpartnersuccess
                        + " , "
                        + jobid + " , " + jobCompletedFlag + ", " + ds + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addPartnerQcChecklist(String, String, String, String, String, DataSource) - exception ignored",
                        e);

            }

            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addPartnerQcChecklist(String, String, String, String, String, DataSource) - exception ignored",
                        e);

            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addPartnerQcChecklist(String, String, String, String, String, DataSource) - exception ignored",
                        e);
            }

        }
        return retval;
    }

    public static int addPartnerRatingQcChecklist(String partnerIdList,
            String optionId, String jobId, String lm_sc_user, DataSource ds) {
        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;

        int retval = -1;
        try {
            conn = ds.getConnection();

            cstmt = conn
                    .prepareCall("{?=call lm_qc_partner_job_rating( ?, ?, ?, ? )}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setString(2, partnerIdList);
            cstmt.setString(3, optionId);
            cstmt.setInt(4, Integer.parseInt(lm_sc_user));
            cstmt.setString(5, jobId);

            // System.out.println("addPartnerRatingQcChecklist   :::::::::    '"+partnerIdList+"' ,'"+optionId+"',"+Integer.parseInt(
            // lm_sc_user )+" , '"+jobId+"'");
            cstmt.execute();
            retval = cstmt.getInt(1);
        } catch (Exception e) {
            logger.error(
                    "addPartnerRatingQcChecklist(String, String, String, String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("addPartnerRatingQcChecklist(String, String, String, String, DataSource) - Error occured during addPartnerRatingQcChecklist "
                        + e);
            }
            logger.error(
                    "addPartnerRatingQcChecklist(String, String, String, String, DataSource)",
                    e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addPartnerRatingQcChecklist(String, String, String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addPartnerRatingQcChecklist(String, String, String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addPartnerRatingQcChecklist(String, String, String, String, DataSource) - exception ignored",
                        e);
            }

        }
        return retval;
    }

    public static int addPartnerRatingQcChecklist(String partnerIdList,
            String optionId, List<String[]> otherOptions, String jobId,
            String lm_sc_user, DataSource ds) {
        Connection conn = null;
        CallableStatement cstmt = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        int retval = -1;
        try {
            conn = ds.getConnection();

            cstmt = conn
                    .prepareCall("{?=call lm_qc_partner_job_rating( ?, ?, ?, ? )}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setString(2, partnerIdList);
            cstmt.setString(3, optionId);
            cstmt.setInt(4, Integer.parseInt(lm_sc_user));
            cstmt.setString(5, jobId);

            // System.out.println("addPartnerRatingQcChecklist   :::::::::    '"+partnerIdList+"' ,'"+optionId+"',"+Integer.parseInt(
            // lm_sc_user )+" , '"+jobId+"'");
            cstmt.execute();
            retval = cstmt.getInt(1);

            if (otherOptions.size() > 0) {
                for (int i = 0; i < otherOptions.size(); i++) {
                    String[] options = otherOptions.get(i);

                    pstmt = conn
                            .prepareStatement("UPDATE dbo.qc_partner_job_rating SET  qc_pj_future_jobs = ? ,  qc_pj_op_rating_Comments = ? " // ,
                                    // qc_pj_user_rating
                                    // =
                                    // ?
                                    + "  WHERE qc_pj_lm_js_id = ?  "
                                    + " AND qc_pj_partner_cp_partner_id = ? ");

                    pstmt.setString(1, options[1]);// qc_pj_future_jobs
                    pstmt.setString(2, options[2]);// qc_pj_op_rating_Comments
                    // pstmt.setString(3, options[1]);// qc_pj_user_rating
                    pstmt.setString(3, jobId);// qc_pj_lm_js_id
                    pstmt.setString(4, options[0]);// qc_pj_partner_cp_partner_id

                    pstmt.executeUpdate();

                    //
                    // cstmt.execute();
                    // retval = cstmt.getInt(1);
                }
            }
        } catch (Exception e) {
            logger.error(
                    "addPartnerRatingQcChecklist(String, String, String, String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("addPartnerRatingQcChecklist(String, String, String, String, DataSource) - Error occured during addPartnerRatingQcChecklist "
                        + e);
            }
            logger.error(
                    "addPartnerRatingQcChecklist(String, String, String, String, DataSource)",
                    e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addPartnerRatingQcChecklist(String, String, String, String, DataSource) - exception ignored",
                        e);
            }
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addPartnerRatingQcChecklist(String,String,String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addPartnerRatingQcChecklist(String, String, String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addPartnerRatingQcChecklist(String, String, String, String, DataSource) - exception ignored",
                        e);
            }

        }
        return retval;
    }

    public static int addSitePocJobRating(String jobId, String ques1,
            String ques2, DataSource ds) {
        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;

        int retval = -1;
        try {
            conn = ds.getConnection();

            cstmt = conn
                    .prepareCall("{?=call qc_site_poc_job_rating_manage01( ?, ?, ? )}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setInt(2, Integer.parseInt(jobId));
            cstmt.setString(3, ques1);
            cstmt.setString(4, ques2);
            cstmt.execute();
            retval = cstmt.getInt(1);
        } catch (Exception e) {
            logger.error(
                    "addSitePocJobRating(String, String, String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("addSitePocJobRating(String, String, String, DataSource) - Error occured during addSitePocJobRating "
                        + e);
            }
            logger.error(
                    "addSitePocJobRating(String, String, String, DataSource)",
                    e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addSitePocJobRating(String, String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addSitePocJobRating(String, String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addSitePocJobRating(String, String, String, DataSource) - exception ignored",
                        e);
            }

        }
        return retval;
    }

    public static String getSiteid(String jobid, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String siteid = "";

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "select lm_js_si_id from lm_job where lm_js_id="
                    + jobid;
            rs = stmt.executeQuery(sql);

            if (rs.next()) {

                siteid = rs.getString("lm_js_si_id");

            }

        } catch (Exception e) {
            logger.error("getSiteid(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getSiteid(String, DataSource) - Error occured during getting Siteid "
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getSiteid(String, DataSource) - exception ignored", e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getSiteid(String, DataSource) - exception ignored", e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getSiteid(String, DataSource) - exception ignored", e);
            }

        }
        return siteid;
    }

    /**
     * This method gets Partner name.
     *
     * @param pid String.partner id
     * @param ds Object of DataSource
     * @return String This method returns String,partner name.
     */
    public static String getSitename(String siteid, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sitename = "";

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "select lm_si_number = isnull(lm_si_number,'') from lm_site_detail where lm_si_id="
                    + siteid;
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                sitename = rs.getString("lm_si_number");
            }

        } catch (Exception e) {
            logger.error("getSitename(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getSitename(String, DataSource) - Error occured during getting Sitename ");
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getSitename(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getSitename(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getSitename(String, DataSource) - exception ignored",
                        e);
            }

        }
        return sitename;
    }

    public static int getPartnerQCchecklistSize(DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        CallableStatement cstmt = null;

        int i = 0;
        int size = 0;
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            cstmt = conn.prepareCall("{?=call lm_master_qc_checklist()}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

            rs = cstmt.executeQuery();

            while (rs.next()) {
                i++;
            }

            size = i;

        } catch (Exception e) {
            logger.error("getPartnerQCchecklistSize(DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getPartnerQCchecklistSize(DataSource) - Error occured during getPartnerQCchecklistSize"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnerQCchecklistSize(DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnerQCchecklistSize(DataSource) - exception ignored",
                        e);
            }

            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnerQCchecklistSize(DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnerQCchecklistSize(DataSource) - exception ignored",
                        e);
            }

        }
        return size;
    }

    public static int getNoOfPartners(String Job_Id, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        CallableStatement cstmt = null;

        int i = 0;
        int size = 0;
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            // String sql =
            // "select * from dbo.func_lm_appendix_powo_info( '"+Job_Id+"' ) where isnull(lm_po_type,'') <> 'N'";
            String sql = "select * from dbo.func_get_accepted_job_po_list( '"
                    + Job_Id + "' ) ";
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                if (rs.getString("lm_po_partner_id") != null
                        && !rs.getString("lm_po_partner_id").trim()
                        .equalsIgnoreCase("")
                        && !rs.getString("lm_po_partner_id").trim()
                        .equalsIgnoreCase("0")) {
                    i++;
                }

            }

            size = i;

        } catch (Exception e) {
            logger.error("getNoOfPartners(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getNoOfPartners(String, DataSource) - Error occured during getNoOfPartners"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getNoOfPartners(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getNoOfPartners(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getNoOfPartners(String, DataSource) - exception ignored",
                        e);
            }

        }
        return size;
    }

    public static int getNoOfPartnersByPO(String Job_Id, String poId,
            DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        CallableStatement cstmt = null;

        int i = 0;
        int size = 0;
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "select * from dbo.func_lm_appendix_powo_info( '"
                    + Job_Id
                    + "' ) where isnull(lm_po_type,'') <> 'N' and lm_po_id = "
                    + poId + " order by lm_po_id";
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                if (rs.getString("cp_pd_partner_id") != null
                        && !rs.getString("cp_pd_partner_id").trim()
                        .equalsIgnoreCase("")
                        && !rs.getString("cp_pd_partner_id").trim()
                        .equalsIgnoreCase("0")) {
                    i++;
                }

            }

            size = i;

        } catch (Exception e) {
            logger.error("getNoOfPartnersByPO(String, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getNoOfPartnersByPO(String, String, DataSource) - Error occured during getNoOfPartners"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getNoOfPartnersByPO(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getNoOfPartnersByPO(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getNoOfPartnersByPO(String, String, DataSource) - exception ignored",
                        e);
            }

        }
        return size;
    }

    public static String getPartnerName(String Job_Id, String poId,
            DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        CallableStatement cstmt = null;

        String partnernamelist = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "select lm_po_partner_name from dbo.func_lm_appendix_powo_info( '"
                    + Job_Id
                    + "' ) where isnull(lm_po_type,'') <> 'N' and lm_po_id = "
                    + poId;
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                if (rs.getString("lm_po_partner_name") != null
                        && !rs.getString("lm_po_partner_name").trim()
                        .equalsIgnoreCase("")) {
                    partnernamelist = partnernamelist
                            + rs.getString("lm_po_partner_name") + "~";
                }
            }
            if (partnernamelist.length() > 0) {
                partnernamelist = partnernamelist.substring(0,
                        partnernamelist.length() - 1);
            }

        } catch (Exception e) {
            logger.error("getPartnerName(String, String, DataSource)", e);

            logger.error("getPartnerName(String, String, DataSource)", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnerName(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnerName(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnerName(String, String, DataSource) - exception ignored",
                        e);
            }

        }
        return partnernamelist;
    }

    public static String getPartnersNamelist(String Job_Id, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        CallableStatement cstmt = null;

        String partnernamelist = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            // String sql =
            // "select lm_po_partner_name from dbo.func_lm_appendix_powo_info( '"+Job_Id+"' ) where isnull(lm_po_type,'') <> 'N' order by lm_po_id";
            String sql = "select lm_po_partner_name from dbo.func_get_accepted_job_po_list( '"
                    + Job_Id + "' ) order by lm_po_id";
            // System.out.println("SQL--"+sql);
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                if (rs.getString("lm_po_partner_name") != null
                        && !rs.getString("lm_po_partner_name").trim()
                        .equalsIgnoreCase("")) {
                    partnernamelist = partnernamelist
                            + rs.getString("lm_po_partner_name") + "~";
                }

            }

            if (partnernamelist.length() > 0) {
                partnernamelist = partnernamelist.substring(0,
                        partnernamelist.length() - 1);
            }
            String a[] = partnernamelist.split("~");
        } catch (Exception e) {
            logger.error("getPartnersNamelist(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getPartnersNamelist(String, DataSource) - Error occured during getPartnersNamelist"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnersNamelist(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnersNamelist(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnersNamelist(String, DataSource) - exception ignored",
                        e);
            }

        }
        return partnernamelist;
    }

    public static String getPartnersIdlist(String Job_Id, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        CallableStatement cstmt = null;

        String partneridlist = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            // String sql =
            // "select cp_pd_partner_id from dbo.func_lm_appendix_powo_info( '"+Job_Id+"' ) where isnull(lm_po_type,'') <> 'N' order by lm_po_id";
            String sql = "select lm_po_partner_id, lm_po_id from dbo.func_get_accepted_job_po_list('"
                    + Job_Id + "') order by lm_po_id";
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                if (rs.getString("lm_po_partner_id") != null
                        && !rs.getString("lm_po_partner_id").trim()
                        .equalsIgnoreCase("")
                        && !rs.getString("lm_po_partner_id").trim()
                        .equalsIgnoreCase("0")) {
                    partneridlist = partneridlist
                            + rs.getString("lm_po_partner_id") + "~";
                }

            }

            if (partneridlist.length() > 0) {
                partneridlist = partneridlist.substring(0,
                        partneridlist.length() - 1);
            }
        } catch (Exception e) {
            logger.error("getPartnersIdlist(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getPartnersIdlist(String, DataSource) - Error occured during getPartnersIdlist"
                        + e);
            }
            logger.error("getPartnersIdlist(String, DataSource)", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnersIdlist(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnersIdlist(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnersIdlist(String, DataSource) - exception ignored",
                        e);
            }

        }
        return partneridlist;
    }

    public static String getPartnersIds(String Job_Id, String poId,
            DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        CallableStatement cstmt = null;

        String partneridlist = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "select cp_pd_partner_id from dbo.func_lm_appendix_powo_info( '"
                    + Job_Id
                    + "' ) where isnull(lm_po_type,'') <> 'N' and lm_po_id = "
                    + poId + " order by lm_po_id";
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                if (rs.getString("cp_pd_partner_id") != null
                        && !rs.getString("cp_pd_partner_id").trim()
                        .equalsIgnoreCase("")
                        && !rs.getString("cp_pd_partner_id").trim()
                        .equalsIgnoreCase("0")) {
                    partneridlist = partneridlist
                            + rs.getString("cp_pd_partner_id") + "~";
                }
            }

            if (partneridlist.length() > 0) {
                partneridlist = partneridlist.substring(0,
                        partneridlist.length() - 1);
            }

        } catch (Exception e) {
            logger.error("getPartnersIds(String, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getPartnersIds(String, String, DataSource) - Error occured during getPartnersIdlist"
                        + e);
            }
            logger.error("getPartnersIds(String, String, DataSource)", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnersIds(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnersIds(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPartnersIds(String, String, DataSource) - exception ignored",
                        e);
            }

        }
        return partneridlist;
    }

    // public static String getFirstvisitsuccessJob( String jobid , DataSource
    // ds )
    public static String[] getFirstvisitsuccessJob(String jobid, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String firstvisitsuccessjob = "";
        String[] siteQuesList = new String[7];

        int i = 0;
        String sql = null;

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            // sql =
            // "select first_visit_success = isnull( first_visit_success , 'T' ) from lm_job where lm_js_id = "+jobid;
            sql = " select first_visit_success = isnull( first_visit_success , 'T' ), "
                    + " lm_js_complete_success = isnull(lm_js_complete_success,'T'), "
                    + " lm_js_professional = isnull(lm_js_professional,'T'), "
                    + " lm_js_customer_followup = isnull(lm_js_customer_followup, 'T'), "
                    + " lm_js_poc_tech = isnull(lm_js_poc_tech, ''), "
                    + " lm_js_poc_number = isnull(lm_js_poc_number, ''), "
                    + " lm_js_poc_comments = isnull(lm_js_poc_comments, '') "
                    + " from lm_job where lm_js_id = " + jobid;

            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                // firstvisitsuccessjob = rs.getString( 1 );
                siteQuesList[0] = rs.getString("first_visit_success").trim();
                siteQuesList[1] = rs.getString("lm_js_complete_success").trim();
                siteQuesList[2] = rs.getString("lm_js_professional").trim();
                siteQuesList[3] = rs.getString("lm_js_customer_followup")
                        .trim();
                siteQuesList[4] = rs.getString("lm_js_poc_tech").trim();
                siteQuesList[5] = rs.getString("lm_js_poc_number").trim();
                siteQuesList[6] = rs.getString("lm_js_poc_comments").trim();

            }
        } catch (Exception e) {
            logger.error("getFirstvisitsuccessJob(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getFirstvisitsuccessJob(String, DataSource) - Error occured during getting firstvisitsuccessjob"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getFirstvisitsuccessJob(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getFirstvisitsuccessJob(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getFirstvisitsuccessJob(String, DataSource) - exception ignored",
                        e);
            }

        }

        return siteQuesList; // firstvisitsuccessjob;
    }

    public static void addInternalPartnerRatingQcChecklist(
            String partneridlist, String recommadTechFutureJobs, String userid,
            DataSource ds) {
        // TODO Auto-generated method stub
        String[] partnerIdArray = partneridlist
                .split(IlexConstants.SYMBOL_TILT);
        String[] recommadTechFutureJobsArray = recommadTechFutureJobs
                .split(IlexConstants.SYMBOL_TILT);
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String sql = null;

        try {
            conn = ds.getConnection();
            sql = "insert into qc_partner_internal_rating (qc_pj_partner_id,qc_pj_pc_id,qc_pj_date,"
                    + "qc_pj_rating) values(?,?,getdate(),?)";
            pstmt = conn.prepareStatement(sql);

            for (int i = 0; i < partnerIdArray.length; i++) {
                pstmt.setInt(1, Integer.valueOf(partnerIdArray[i]));
                pstmt.setInt(2, Integer.valueOf(userid));
                if (IlexConstants.CHARACTER_Y
                        .equalsIgnoreCase(recommadTechFutureJobsArray[i])) {
                    pstmt.setInt(3, 1);
                } else {
                    pstmt.setInt(3, 0);
                }
                pstmt.executeUpdate();
            }

        } catch (Exception e) {
            logger.error(
                    "addInternalPartnerRatingQcChecklist(String,String,String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("addInternalPartnerRatingQcChecklist(String,String,String, DataSource) - Error occured during getting firstvisitsuccessjob"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addInternalPartnerRatingQcChecklist(String,String,String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addInternalPartnerRatingQcChecklist(String,String,String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addInternalPartnerRatingQcChecklist(String,String,String, DataSource) - exception ignored",
                        e);
            }

        }

        /*
         * for (String element : array) { myStatement.setString(1, element[0]);
         * myStatement.setString(2, element[1]);
         *
         * myStatement.executeUpdate(); }
         */
    }
}
