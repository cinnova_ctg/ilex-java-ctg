package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.fw.core.dao.util.DBUtil;

public class AssignOwnerdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AssignOwnerdao.class);

	/**
	 * @author amitm
	 * @param ds
	 * @param Appendix_Id
	 * @return
	 */
	public static ArrayList getAllOwnername(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lp_all_owners()";

			rs = stmt.executeQuery(sql);

			valuelist.add(new com.mind.common.LabelValue("--Select--", "0"));
			while (rs.next()) {

				valuelist.add(new com.mind.common.LabelValue(rs
						.getString("ownername"), rs.getString("lo_pc_id")));

			}

		} catch (Exception e) {
			logger.error("getAllOwnername(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAllOwnername(DataSource) - Error occured during getting owner name"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn("getAllOwnername(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn("getAllOwnername(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn("getAllOwnername(DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	/**
	 * @author amitm
	 * @param ds
	 * @param jobid
	 * @param newOwnerid
	 */
	public static void changeOwner(DataSource ds, int jobid, int newOwnerid) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		boolean blnInsertStarus = false;

		try {

			conn = ds.getConnection();
			cstmt = conn.prepareCall("{call lm_changeOwner(?,?)}");
			cstmt.setInt(1, jobid);
			cstmt.setInt(2, newOwnerid);

			blnInsertStarus = cstmt.execute();

		} catch (Exception e) {
			logger.error("changeOwner(DataSource, int, int)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("changeOwner(DataSource, int, int) - Error occured during changing owner name"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"changeOwner(DataSource, int, int) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"changeOwner(DataSource, int, int) - exception ignored",
						e);
			}

		}

	}

	/**
	 * @author amitm
	 * @param ds
	 * @return
	 */
	public static ArrayList getAllOwnerForAppendix(String appendixId,
			String userid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			// Change on 27-07-06
			if (appendixId.equals("-1"))
				appendixId = "%";

			String sql = "select * from dbo.func_lp_prj_owner_per_msa('"
					+ appendixId + "','%')";
			// System.out.println(" query is ::::::::::::::::::: >"+sql);
			rs = stmt.executeQuery(sql);
			valuelist.add(new com.mind.common.LabelValue("---Select---", "0"));

			while (rs.next()) {
				valuelist.add(new com.mind.common.LabelValue(rs
						.getString("owener_name"), rs
						.getString("lm_js_created_by")));
			}

		} catch (Exception e) {
			logger.error("getAllOwnerForAppendix(String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAllOwnerForAppendix(String, String, DataSource) - Error occured during getting owner name per appendix"
						+ e);
			}
			logger.error("getAllOwnerForAppendix(String, String, DataSource)",
					e);
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAllOwnerForAppendix(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAllOwnerForAppendix(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAllOwnerForAppendix(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	/**
	 * Discription: This method is use to get job team list for Job Scheduler
	 * Assignment from Database.
	 * 
	 * @param ds
	 *            - Database object
	 * @param appendixId
	 *            - Appendix Id
	 * @return ArrayList - Role List
	 */
	public static ArrayList getSchedulerList(String appendixId, String status,
			DataSource ds) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList schedulerList = new ArrayList();
		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select * from func_lm_job_team_list(?,?)");
			pstmt.setString(1, appendixId);
			pstmt.setString(2, status);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				schedulerList.add(new com.mind.common.LabelValue(rs
						.getString("poc_name"), rs.getString("poc_id")));
			}

		} catch (Exception e) {
			logger.error("getSchedulerList(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSchedulerList(String, String, DataSource) - Error occured com.mind.dao.PRM.AssignOwnerdao.getSchedulerList() "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return schedulerList;
	}

	/**
	 * Discription: This method is use to get all assigned team members list and
	 * its roles from Database.
	 * 
	 * @param jobId
	 *            - Job Id
	 * @param newSchedulerId
	 *            - Assigned member id
	 * @param ds
	 *            - Database object
	 * @return ArrayList - Role List
	 */
	public static int assignScheduler(String jobId, String newSchedulerId,
			String loginUserId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		int val = -1;

		// System.out.println("jobId "+jobId);
		// System.out.println("newSchedulerId "+newSchedulerId);

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_job_scheduler_manage_01(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, jobId);
			cstmt.setString(3, newSchedulerId);
			cstmt.setString(4, loginUserId);
			cstmt.execute();
			val = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error("assignScheduler(String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("assignScheduler(String, String, String, DataSource) - Error occured com.mind.dao.PRM.AssignOwnerdao.assignScheduler() "
						+ e);
			}
		}

		finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return val;
	}

	/**
	 * Discription: This method is use to get current job scheduler name from
	 * Database.
	 * 
	 * @param jobid
	 *            - Job Id
	 * @param ds
	 *            - Database object
	 * @return String - Current Scheduler Name
	 */
	public static String getCurrentScheduler(String jobId, DataSource ds) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String currentScheduler = "";

		// System.out.println("jobId = "+jobId);

		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select curr_scheduler = isnull(dbo.func_cc_poc_name( lm_js_scheduler ),'') "
							+ "from lm_job where lm_js_id = ? ");
			pstmt.setString(1, jobId);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				currentScheduler = rs.getString("curr_scheduler");
			}

		} catch (Exception e) {
			logger.error("getCurrentScheduler(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCurrentScheduler(String, DataSource) - Error occured com.mind.dao.PRM.AssignOwnerdao.getCurrentScheduler() "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return currentScheduler;
	}

}
