package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.SiteManagementBean;
import com.mind.fw.core.dao.util.DBUtil;

public class SiteManagementdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(SiteManagementdao.class);

	public static String getEndCustomer(String app_id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String endcustomer = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select isnull(lx_pr_end_customer,'') as Customer from lx_appendix_main where lx_pr_id = "
							+ app_id);
			while (rs.next()) {
				endcustomer = rs.getString("Customer");
			}
		} catch (Exception e) {
			logger.error("getEndCustomer(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEndCustomer(String, DataSource) - Exception caught in rerieving End Customer Name"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getEndCustomer(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getEndCustomer(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getEndCustomer(String, DataSource) - exception ignored",
						sql);
			}

		}

		return endcustomer;
	}

	public static ArrayList getSiteSearchDetail(
			SiteManagementBean siteMngttForm, String qry, DataSource ds) {
		Connection conn = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		CallableStatement cstmt = null;
		ArrayList displayList = new ArrayList();

		String name = siteMngttForm.getSiteSearchName();
		String number = siteMngttForm.getSiteSearchNumber();
		String city = siteMngttForm.getSiteSearchCity();
		String state = siteMngttForm.getSiteSearchState();
		String zip = siteMngttForm.getSiteSearchZip();
		String country = siteMngttForm.getSiteSearchCountry();
		String brand = siteMngttForm.getSiteSearchGroup();
		String endcustomer = siteMngttForm.getSiteSearchEndCustomer();
		String pageType = siteMngttForm.getPageType();
		String status = siteMngttForm.getSiteSearchStatus();

		if (name != null && !name.equals(""))
			name = name.replaceAll("\'", "\'\'");

		if (number != null && !number.equals(""))
			number = number.replaceAll("\'", "\'\'");

		if (brand != null && !brand.equals(""))
			brand = brand.replaceAll("\'", "\'\'");

		if (endcustomer != null && !endcustomer.equals(""))
			endcustomer = endcustomer.replaceAll("\'", "\'\'");

		String msaid = siteMngttForm.getMsaid();
		String appendixid = siteMngttForm.getAppendixid();

		try {

			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lp_site_search(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, name);
			cstmt.setString(3, number);
			cstmt.setInt(4, Integer.parseInt(msaid));
			cstmt.setString(5, qry);
			cstmt.setString(6, city);
			cstmt.setString(7, state);
			cstmt.setString(8, zip);
			cstmt.setString(9, country);
			cstmt.setString(10, brand);
			cstmt.setString(11, endcustomer);
			cstmt.setString(12, status);
			cstmt.setInt(13, Integer.parseInt(appendixid));
			cstmt.setString(14, pageType);

			rs = cstmt.executeQuery();

			while (rs.next()) {
				SiteManagementBean siteManagementForm = new SiteManagementBean();
				siteManagementForm.setSiteID(rs.getString("lp_sl_id"));
				siteManagementForm.setSiteName(rs.getString("lp_si_name"));
				siteManagementForm.setSiteNumber(rs.getString("lp_si_number"));
				siteManagementForm
						.setSiteAddress(rs.getString("lp_si_address"));
				siteManagementForm.setSiteCity(rs.getString("lp_si_city"));
				siteManagementForm.setState(rs.getString("lp_si_state"));
				siteManagementForm.setSiteZipCode(rs
						.getString("lp_si_zip_code"));
				siteManagementForm.setCountry(rs.getString("lp_si_country"));
				siteManagementForm.setSiteLocalityFactor(rs
						.getString("lp_si_locality_factor"));
				siteManagementForm.setSiteUnion(rs
						.getString("lp_si_union_site"));
				siteManagementForm.setSiteDesignator(rs
						.getString("lp_si_designator"));
				siteManagementForm.setSiteDirections(rs
						.getString("lp_si_directions"));
				siteManagementForm.setSiteWorkLocation(rs
						.getString("lp_si_work_location"));
				siteManagementForm.setSiteCreatedDate(rs
						.getString("lp_si_create_date"));
				siteManagementForm.setSiteCreatedBy(rs
						.getString("lp_si_created_by"));
				siteManagementForm.setSiteChangedDate(rs
						.getString("lp_si_changed_date"));
				siteManagementForm.setSiteChangedBy(rs
						.getString("lp_si_changed_by"));
				siteManagementForm.setSiteInstallerPoc(rs
						.getString("lp_si_installer_poc"));
				siteManagementForm.setSitePoc(rs.getString("lp_si_site_poc"));
				siteManagementForm.setSiteNotes(rs.getString("lp_si_notes"));
				siteManagementForm.setSitePrimaryName(rs
						.getString("lp_si_pri_poc"));
				siteManagementForm.setSitePrimaryPhone(rs
						.getString("lp_si_phone_no"));
				siteManagementForm.setSitePrimaryEmail(rs
						.getString("lp_si_pri_email_id"));
				siteManagementForm.setSiteSecondaryName(rs
						.getString("lp_si_sec_poc"));
				siteManagementForm.setSiteSecondayPhone(rs
						.getString("lp_si_sec_phone_no"));
				siteManagementForm.setSiteSecondaryEmail(rs
						.getString("lp_si_sec_email_id"));
				siteManagementForm.setSite_group(rs.getString("lp_si_group"));
				siteManagementForm.setSite_end_customer(rs
						.getString("lp_si_end_customer"));
				siteManagementForm.setSiteStatus(rs.getString("lp_si_status"));
				siteManagementForm
						.setSite_time_zone(rs.getString("cc_tz_name"));
				siteManagementForm.setSiteJobId(rs.getString("job_id"));
				String priorityFlag = rs.getString("lp_sl_priority_store");
				if (priorityFlag.equals("Null") || priorityFlag.equals("")) {

					siteManagementForm.setCboxsite("N");

				}
				if (priorityFlag.equals("1")) {

					siteManagementForm.setCboxsite("Y");

				} else {

					siteManagementForm.setCboxsite("N");
				}

				displayList.add(siteManagementForm);
			}

		} catch (Exception e) {
			logger.error(
					"getSiteSearchDetail(SiteManagementForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteSearchDetail(SiteManagementForm, String, DataSource) - Error occured during getting Search Result for Site ::::::::::::"
						+ e);
			}
			logger.error(
					"getSiteSearchDetail(SiteManagementForm, String, DataSource)",
					e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getSiteSearchDetail(SiteManagementForm, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getSiteSearchDetail(SiteManagementForm, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getSiteSearchDetail(SiteManagementForm, String, DataSource) - exception ignored",
						s);
			}

		}
		return displayList;
	}

	public static List getSitedisplayDetail(ArrayList displayList,
			String lines_page_size, String current_page_no,
			SiteManagementBean siteMngttForm)

	{
		List valuelist = new ArrayList();
		// ArrayList valuelist = new ArrayList();
		int rowSize = 0;
		int total_pages = 0;
		int counter = 0;
		int index1 = 0;
		int rem = 0;
		int index2;

		rowSize = displayList.size();

		if (current_page_no == null || current_page_no.equals("")
				|| current_page_no.equals("0")) {
			current_page_no = "1";
		}
		if (lines_page_size == null || lines_page_size.equals("")
				|| lines_page_size.equals("0")) {
			lines_page_size = "10";
		}

		total_pages = rowSize / Integer.parseInt(lines_page_size);
		rem = rowSize % Integer.parseInt(lines_page_size);

		if (rowSize % Integer.parseInt(lines_page_size) > 0)
			total_pages = total_pages + 1;

		if (siteMngttForm.getFirst() != null
				&& !siteMngttForm.getFirst().equals("")) {
			index1 = 0;
			current_page_no = "1";
		}

		if (siteMngttForm.getNext() != null
				&& !siteMngttForm.getNext().equals("")) {
			current_page_no = (Integer.parseInt(current_page_no) + 1) + "";
		}

		if (siteMngttForm.getPrevious() != null
				&& !siteMngttForm.getPrevious().equals("")) {
			current_page_no = (Integer.parseInt(current_page_no) - 1) + "";
			if ((Integer.parseInt(current_page_no) == 0 || (Integer
					.parseInt(current_page_no) == 1))) {
				current_page_no = "1";
			}
		}

		if (siteMngttForm.getLast() != null
				&& !siteMngttForm.getLast().equals("")) {
			current_page_no = total_pages + "";
		}

		if (Integer.parseInt(current_page_no) > total_pages)
			current_page_no = (Integer.parseInt(current_page_no) - 1) + "";

		switch (Integer.parseInt(current_page_no)) {
		case 0:
			;
		case 1:
			index1 = 0;
			break;
		default:
			index1 = ((Integer.parseInt(current_page_no) - 1) * Integer
					.parseInt(lines_page_size));
		}

		if (rem > 0 && (Integer.parseInt(current_page_no) == total_pages))
			index2 = index1 + rem;
		else
			index2 = index1 + Integer.parseInt(lines_page_size);

		if (displayList.size() > 0)
			valuelist = displayList.subList(index1, index2);

		siteMngttForm.setTotal_page_size(total_pages + "");
		siteMngttForm.setCurrent_page_no(current_page_no + "");
		siteMngttForm.setLines_per_page(lines_page_size + "");
		siteMngttForm.setOrg_page_no(current_page_no + "");
		siteMngttForm.setOrg_lines_per_page(lines_page_size + "");
		siteMngttForm.setRow_size(rowSize + "");

		return valuelist;
	}

	public static void getSiteCount(SiteManagementBean siteMngtForm,
			String msaid, String pageType, DataSource ds) {
		Connection conn = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{call lp_site_count(?,?)}");
			cstmt.setString(1, msaid);
			cstmt.setString(2, pageType);
			rs = cstmt.executeQuery();

			if (rs.next()) {
				siteMngtForm
						.setActive_sites(rs.getString("lp_si_active_sites"));
				siteMngtForm.setInactive_sites(rs
						.getString("lp_si_inactive_sites"));
				siteMngtForm.setTotal_sites(rs.getString("lp_si_total_sites"));
			}
		}

		catch (Exception e) {
			logger.error(
					"getSiteCount(SiteManagementForm, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteCount(SiteManagementForm, String, String, DataSource) - Error Occured in retrieving count of Sites"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getSiteCount(SiteManagementForm, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getSiteCount(SiteManagementForm, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getSiteCount(SiteManagementForm, String, String, DataSource) - exception ignored",
						s);
			}

		}

	}

	// update prority flag status
	public static void updateSitePriorityStatus(String sid, String status,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "";

		sql = "UPDATE dbo.lp_site_list  SET lp_sl_priority_store =" + status
				+ " WHERE lp_sl_id = " + sid;

		try {
			conn = ds.getConnection();

			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();

		} catch (Exception e) {
			logger.error("updateSitePriorityStatus(String sid, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateSitePriorityStatus(String sid, DataSource) - Exception caught in updateSitePriorityStatus  :"
						+ e);
			}
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}

	}

	// Function that check detail of login user
	public static int getPocRole(String lo_pc_id, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int flag = 0;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select 1 from lo_poc where (lo_pc_title like 'Sr. Project Manager' OR lo_pc_title LIKE 'Account managers'"
					+ " OR lo_pc_title LIKE 'Project Executive' OR lo_pc_title like'NOC Manager'  OR lo_pc_title LIKE 'Director%' "
					+ " )  and  lo_pc_id=" + lo_pc_id;

			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				flag = 1;
			}
		} catch (Exception e) {
			logger.error("getPocRole(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocRole(String, DataSource) - Error occured during getting statusname "
						+ e);
			}

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPocRole(String, DataSource) - exception ignored", e);

			}
			try {
				if (stmt != null)
					stmt.close();

			} catch (Exception e) {
				logger.warn(
						"getPocRole(String, DataSource) - exception ignored", e);

			}

			try {
				if (conn != null)
					conn.close();

			} catch (Exception e) {
				logger.warn(
						"getPocRole(String, DataSource) - exception ignored", e);

			}

		}
		return flag;
	}

}
