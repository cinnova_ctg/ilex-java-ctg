package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.DispatchScheduleList;


public class DispatchScheduledao
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DispatchScheduledao.class);

	public  static int[] addScheduleForPRM(String lx_se_id, String lx_se_type_id, String act_start, String act_end, String lx_se_type, String lx_se_created_by, String lx_se_start, int lx_se_count, DataSource ds)
	{
		Connection conn=null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int[] retValArray = new int[2];
		
		try
			{
			conn=ds.getConnection();
			/*
			System.out.println("lx_se_id::::"+lx_se_id);
			System.out.println("lx_se_type_id::::"+lx_se_type_id);
			System.out.println("act_start::::"+act_start);
			System.out.println("act_end::::"+act_end);
			System.out.println("lx_se_type::::"+lx_se_type);
			System.out.println("lx_se_created_by::::"+lx_se_created_by);
			System.out.println("lx_se_start::::"+lx_se_start);
			*/
					
			//cstmt=conn.prepareCall("{?=call lx_dispatch_schedule_manage_01(?,?,?,?,?,?,?,?)}");
			cstmt=conn.prepareCall("{?=call lx_dispatch_schedule_manage_01(?,?,?,?,?,?,?,?,?)}");			// changes as per installation notes
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2,lx_se_id);
			cstmt.setString(3,lx_se_type_id);
			cstmt.setString(4,act_start);
			cstmt.setString(5,act_end);
			cstmt.setString(6,lx_se_type);
			cstmt.setString(7,lx_se_created_by);
			cstmt.setString(8,lx_se_start);
			cstmt.setInt(9,lx_se_count);
			cstmt.registerOutParameter(10,java.sql.Types.INTEGER);
			cstmt.execute();
			retValArray[0] = cstmt.getInt(1);
			retValArray[1] = cstmt.getInt(10);
			
		}
		catch(Exception e){
			logger.error("addScheduleForPRM(String, String, String, String, String, String, String, int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addScheduleForPRM(String, String, String, String, String, String, String, int, DataSource) - Error occured during adding schedule" + e);
			}
		}
	
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("addScheduleForPRM(String, String, String, String, String, String, String, int, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("addScheduleForPRM(String, String, String, String, String, String, String, int, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("addScheduleForPRM(String, String, String, String, String, String, String, int, DataSource) - exception ignored", e);
}
			
		}
		return retValArray;		
			
			
	}
	
	
	public static ArrayList getDispatchScheduleList(String typeid, String type, DataSource ds)
	{
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		int i = 0;
		
		String scheduleId = null;
		String scheduleNumber = null;
		
		String actStart = null;
		String actEnd = null;
		String plannedStart = null;
		
		String actStartdate = null;
		String actEnddate = null;
		String plannedStartdate = null;
		
		String actStarthh = null;
		String actEndhh = null;
		String plannedStarthh = null;
		
		String actStartmm = null;
		String actEndmm = null;
		String plannedStartmm = null;
		
		String actStartop = null;
		String actEndop = null;
		String plannedStartop = null;
		
		String sqlStr = null;
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			sqlStr = "select lx_se_id, lx_se_start, lx_se_end, lx_se_planned_start from lx_dispatch_schedule_list_01 where lx_se_type_id="+typeid+" AND lx_se_type='"+type+"'";
			
			//System.out.println("Query for schedule list::"+sqlStr);
			
			rs = stmt.executeQuery(sqlStr);
			
			while( rs.next() )
			{
				scheduleId = rs.getString("lx_se_id");
				scheduleNumber = rs.getString("lx_se_id");
				
				actStart=rs.getString("lx_se_start");
				actEnd=rs.getString("lx_se_end");
				plannedStart=rs.getString("lx_se_planned_start");
				
				if(!actStart.equals("")){
					actStartdate = actStart.substring(0, 10);
					if(actStart.substring(11,12).equals(" "))
						actStarthh = "0"+actStart.substring(12,13);
					else
						actStarthh = actStart.substring(11,13);
					actStartmm = actStart.substring(14,16);
					actStartop = actStart.substring(23,25);
				}
				else {
					actStartdate = "";
					actStarthh = " ";
					actStartmm = " ";
					actStartop = " ";
				}
				//System.out.println("Actual Strat111"+actStart);
				if(!actEnd.equals("")){
					actEnddate = actEnd.substring(0, 10);
					if(actEnd.substring(11,12).equals(" "))
						actEndhh = "0"+actEnd.substring(12,13);
					else
						actEndhh = actEnd.substring(11,13);
					actEndmm = actEnd.substring(14,16);
					actEndop = actEnd.substring(23,25);
				}
				else {
					actEnddate = "";
					actEndhh = " ";
					actEndmm = " ";
					actEndop = " ";
				}
				
				if(!plannedStart.equals("")){
					plannedStartdate = plannedStart.substring(0, 10);
					if(plannedStart.substring(11,12).equals(" "))
						plannedStarthh = "0"+plannedStart.substring(12,13);
					else
						plannedStarthh = plannedStart.substring(11,13);
					plannedStartmm = plannedStart.substring(14,16);
					plannedStartop = plannedStart.substring(23,25);
				}
				else {
					plannedStartdate = "";
					plannedStarthh = " ";
					plannedStartmm = " ";
					plannedStartop = " ";
				}
				
				list.add(new DispatchScheduleList(scheduleId, plannedStartdate, plannedStarthh, plannedStartmm, plannedStartop, actStartdate, actStarthh, actStartmm, actStartop, actEnddate, actEndhh, actEndmm, actEndop, scheduleNumber));
				
				i++;
			}
			
			// Add for set ticket preferred arrival date and time as default SPR 339 Release 1.21
			
			if(i == 0) {
				
				sqlStr = "select lm_tc_preferred_arrival = isnull(replace(convert(varchar(10),(isnull(lm_tc_preferred_arrival,'')),101)+''+ substring(convert(varchar(30),lm_tc_preferred_arrival,109),12,15),'01/01/1900 12:00:00:000AM',''),'') from lm_ticket where lm_tc_js_id = "+typeid+"";
				
				rs = stmt.executeQuery(sqlStr);
				
				if(rs.next())
				{
					plannedStart=rs.getString("lm_tc_preferred_arrival");
					
				}	
				
				if(!plannedStart.equals("")){
					plannedStartdate = plannedStart.substring(0, 10);
					if(plannedStart.substring(11,12).equals(" "))
						plannedStarthh = "0"+plannedStart.substring(12,13);
					else
						plannedStarthh = plannedStart.substring(11,13);
					plannedStartmm = plannedStart.substring(14,16);
					plannedStartop = plannedStart.substring(23,25);
				}
				else {
					plannedStartdate = "";
					plannedStarthh = " ";
					plannedStartmm = " ";
					plannedStartop = " ";
				}
				scheduleId = "";
				actStartdate = "";
				actStarthh = " ";
				actStartmm = " ";
				actStartop = " ";
				actEnddate = "";
				actEndhh = " ";
				actEndmm = " ";
				actEndop = " ";
				scheduleNumber = "";
				
				list.add(new DispatchScheduleList(scheduleId, plannedStartdate, plannedStarthh, plannedStartmm, plannedStartop, actStartdate, actStarthh, actStartmm, actStartop, actEnddate, actEndhh, actEndmm, actEndop, scheduleNumber));
			}
		}
		catch(Exception e)
		{
			logger.error("getDispatchScheduleList(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDispatchScheduleList(String, String, DataSource) - Exception caught in get Dispatch Schedule List :" + e);
			}
		}
		finally
		{
			
			try
			{
				if ( rs != null ) 
				rs.close();
			}
			catch( Exception sql )
			{
				logger.warn("getDispatchScheduleList(String, String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( stmt != null ) 
				stmt.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getDispatchScheduleList(String, String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn != null ) 
				conn.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getDispatchScheduleList(String, String, DataSource) - exception ignored", sql);
}
			
		}
		return list;
	}
	
	/** This method deletes 2nd level category. 
	 * @param iv_mf_ct_id					String. id of 2nd level category
	 * @param ds   			       	    	Object of DataSource
	 * @return int              	   	    This method returns integer, 0 for success and -ve values for error.
	 */
		public static int deleteDispatchSchedule(String typeId, String type, String lx_se_id, DataSource ds)
		{
			
			Connection conn=null;
			CallableStatement cstmt = null;
			ResultSet rs=null;
			
			int retval=-1;

			
			//System.out.println("typeId:::::"+typeId);
			//System.out.println("type:::::"+type);
			//System.out.println("lx_se_id:::::"+lx_se_id);
			
			
			try{
				conn=ds.getConnection();
				cstmt=conn.prepareCall("{?=call lx_dispatch_schedule_delete_01(?, ?, ?)}");
				cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
				
				cstmt.setString(2, typeId);
				cstmt.setString(3, type);
				cstmt.setString(4, lx_se_id);
				
				cstmt.execute();
				retval = cstmt.getInt(1);
				
			}
			catch(Exception e){
			logger.error("deleteDispatchSchedule(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteDispatchSchedule(String, String, String, DataSource) - Error occured during deleting Dispatch Schedule" + e);
			}
				
			}
			finally
			{
					
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("deleteDispatchSchedule(String, String, String, DataSource) - exception ignored", e);
}
				
				try
				{
					if ( cstmt!= null ) 
						cstmt.close();
				}
				catch( Exception e )
				{
				logger.warn("deleteDispatchSchedule(String, String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("deleteDispatchSchedule(String, String, String, DataSource) - exception ignored", e);
}
				
			}
			return retval;
		}
	
	public  static String[] getSchedule(String typeid, String type, DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String[] str = new String[3];
		String sql = null;
		int j= 0; 
				
		try
		{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			sql="select * from lx_schedule_element where lx_se_type_id="+typeid+" AND lx_se_type='"+type+"'";
			
			rs = stmt.executeQuery(sql);
			
						
			while(rs.next())
			{
				str[0]=rs.getString("lx_se_id");
				str[1]=rs.getString("lx_se_type");
				str[2]=rs.getString("lx_se_type_id");
				j++;
			}
		
		}
		catch(Exception e){
			logger.error("getSchedule(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSchedule(String, String, DataSource) - Error occured during getting schedule  " + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getSchedule(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getSchedule(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getSchedule(String, String, DataSource) - exception ignored", e);
}
			
		}
		return str;
		
	}
	
	
	
	public static String getCurrentDate()
	{
		String date; 
		Calendar c = Calendar.getInstance();
		date = (c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.DATE)+"/"+c.get(Calendar.YEAR);
		return date;
	}
	
	
	public  static String getAppendixname(String appendixid, DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String appendixname="";
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select lx_pr_title from lx_appendix_main where lx_pr_id="+appendixid;
			
			rs = stmt.executeQuery(sql);
						
			if(rs.next())
			{
			
				appendixname=rs.getString("lx_pr_title");
			}
			
		}
		catch(Exception e){
			logger.error("getAppendixname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixname(String, DataSource) - Error occured during getting Appendix name  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getAppendixname(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getAppendixname(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getAppendixname(String, DataSource) - exception ignored", e);
}
			
		}
		return appendixname;	
	}
	
	public static String getJobname(  String Job_Id , DataSource ds )
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		
		String jobname = "";
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			sql = "select lm_js_title from lm_job where lm_js_id="+Job_Id;
			rs = stmt.executeQuery( sql );
			if( rs.next() )
				jobname = rs.getString( 1 );	
		}
		
		catch( Exception e )
		{
			logger.error("getJobname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobname(String, DataSource) - Error Occured in retrieving jobname" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( SQLException s )
			{
				logger.warn("getJobname(String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( stmt != null ) 
					stmt.close();
			}
			catch( SQLException s )
			{
				logger.warn("getJobname(String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( conn != null ) 
					conn.close();
			}
			catch( SQLException s )
			{
				logger.warn("getJobname(String, DataSource) - exception ignored", s);
}
			
		}
		return jobname;
	}
	
	
	public  static String[] getPMSchedule(String jobid, DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String[] valuelist=new String[8];
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select * from dbo.func_lp_prj_job_tab('%',"+jobid+",'%','%')";	
			//System.out.println("sql getPMSchedule "+sql);
			rs = stmt.executeQuery(sql);
			int i=0;
			
			if(rs.next())
			{
			
				valuelist[0]= rs.getString("lm_js_planned_start_date");
				valuelist[1]= rs.getString("lm_js_planned_end_date");
				
				if(!(rs.getString("lx_act_start_time").equals("")))
				 {
					valuelist[2]=rs.getString("lx_act_start_time").substring(0,1);
					valuelist[3]=rs.getString("lx_act_start_time").substring(3,4);
					valuelist[4]=rs.getString("lx_act_start_time").substring(6,7);
				 }
				
				if(!(rs.getString("lx_act_end_time").equals("")))
				 {
					valuelist[5]=rs.getString("lx_act_end_time").substring(0,1);
					valuelist[6]=rs.getString("lx_act_end_time").substring(3,4);
					valuelist[7]=rs.getString("lx_act_end_time").substring(6,7);
				 }
				
				
				 
			}
			
		}
		catch(Exception e){
			logger.error("getPMSchedule(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPMSchedule(String, DataSource) - Error occured during getting get PM Schedule  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getPMSchedule(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getPMSchedule(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getPMSchedule(String, DataSource) - exception ignored", e);
}
			
		}
		return valuelist;	
	}
	

	//for adding criticality combo in csv report
	public static ArrayList getCriticalitylist(DataSource ds)
	{
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		//list.add( new com.mind.common.LabelValue( "---Select---" , "0") );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery( "select iv_crit_id,iv_crit_name  from iv_criticality ");
			while( rs.next() )
			{
				list.add ( new com.mind.common.LabelValue( rs.getString( "iv_crit_name" ), rs.getString( "iv_crit_id" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getCriticalitylist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCriticalitylist(DataSource) - Exception caught" + e);
			}
		}
		finally
		{
			
			try
			{
				if ( rs!= null ) 
				{
					rs.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getCriticalitylist(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( stmt!= null ) 
				{
					stmt.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getCriticalitylist(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getCriticalitylist(DataSource) - exception ignored", sql);
}
			
		}
		return list;
	}
	
	public static int getSize(String typeid,DataSource ds) {
		 Connection conn=null;
		 ResultSet rs= null;
		 Statement st=null;
		 int length=0;
		 try {
			 conn= ds.getConnection();
			 st=conn.createStatement();
			 rs=st.executeQuery("select count(lx_se_id) as size  from lx_dispatch_schedule_list_01 where lx_se_type_id ="+typeid);
			 if(rs.next()) {
				 length=Integer.parseInt((rs.getString("size")));
			 }
		 } catch(Exception e) {
			logger.error("getSize(String, DataSource)", e);
			 

			if (logger.isDebugEnabled()) {
				logger.debug("getSize(String, DataSource) - Exception caught" + e);
			}
				}
				finally
				{
					
					try
					{
						if ( rs!= null ) 
						{
							rs.close();
						}
					}
					catch( SQLException sql )
					{
				logger.warn("getSize(String, DataSource) - exception ignored", sql);
}
					
					
					try
					{
						if ( st!= null ) 
						{
							st.close();
						}
					}
					catch( SQLException sql )
					{
				logger.warn("getSize(String, DataSource) - exception ignored", sql);
}
					
					
					try
					{
						if ( conn!= null ) 
						{
							conn.close();
						}
					}
					catch( SQLException sql )
					{
				logger.warn("getSize(String, DataSource) - exception ignored", sql);
}
					
				}
	 return length;	 
	}
	
	
	
	
}
