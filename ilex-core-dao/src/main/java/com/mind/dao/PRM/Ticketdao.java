/**
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is a data object access class used for managing ticket creation and updation.      
*
*/
package com.mind.dao.PRM;

import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.TicketDetailBean;
import com.mind.bean.prm.TicketDetailDTO;


/**
* Methods : createTicketNumber, getSelectedDepoProductList, getTicketDetail, getProductListFromDatabase, updateTicketDetail, getDepoCategoryList, getRolePrivilege, addFunctionType
*/

public class Ticketdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Ticketdao.class);

	public static TicketDetailDTO createTicketNumber(String appendixId,TicketDetailDTO ticketdetailForm, DataSource ds) 
	{
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String requested_date = "";
		
		try
		{
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lm_get_ticket_number()}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			rs  = cstmt.executeQuery();
			if ( rs.next())
			{
				ticketdetailForm.setTicket_number(rs.getString("ticket_number"));
				ticketdetailForm.setTicket_id("0");
				ticketdetailForm.setJobid("0");
				ticketdetailForm.setUnion_site("N");
				ticketdetailForm.setLocality_uplift("1.0");
				ticketdetailForm.setSite_country("US");
				if(getMspStatus(appendixId,ds).equals("Y")) {
					ticketdetailForm.setMsp("Y");
				} else {
					ticketdetailForm.setMsp("N");
				}
				ticketdetailForm.setStandby("N");
				ticketdetailForm.setHelpdesk("N");
			}
		}
		catch( Exception e )
		{
			logger.error("createTicketNumber(String, TicketDetailForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("createTicketNumber(String, TicketDetailForm, DataSource) - Exception caught in rerieving Ticket Number" + e);
			}
		}
		
		finally
		{
			
			try
			{
				if ( rs!= null ) 
					rs.close();
			}
			catch( SQLException sql )
			{
				logger.warn("createTicketNumber(String, TicketDetailForm, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( SQLException sql )
			{
				logger.warn("createTicketNumber(String, TicketDetailForm, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn!= null ) 
					conn.close();
			}
			catch( SQLException sql )
			{
				logger.warn("createTicketNumber(String, TicketDetailForm, DataSource) - exception ignored", sql);
}
			
		}
		
		return ticketdetailForm;	
	}	
	
	public  static ArrayList getStateCategoryList(DataSource ds)
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
				
		try{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql="select cp_country_id, cp_country_name, cp_country_order from lp_country_list where cp_country_id is not null order by cp_country_order,cp_country_name";
			rs = stmt.executeQuery(sql);
			int i=0;
			
			if(i == 0){
				TicketDetailDTO ticketdetailForm = new TicketDetailDTO();
				ticketdetailForm.setSiteCountryId("");
				ticketdetailForm.setSiteCountryIdName("");
				valuelist.add(i,ticketdetailForm);
				i++;
			}
			
			while(rs.next())
			{
				TicketDetailDTO ticketdetailForm = new TicketDetailDTO();
				ticketdetailForm.setSiteCountryId(rs.getString("cp_country_id"));
				ticketdetailForm.setSiteCountryIdName(rs.getString("cp_country_name"));
				valuelist.add(i,ticketdetailForm);
				i++;		 
			}
			
		}
		catch(Exception e){
			logger.error("getStateCategoryList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStateCategoryList(DataSource) - Error occured during getting State Category Wise List  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getStateCategoryList(DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getStateCategoryList(DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getStateCategoryList(DataSource) - exception ignored", e);
}
			
		}
		return valuelist;	
	}
	
	public  static ArrayList getCategoryWiseStateList(String cat_id, int count, DataSource ds)
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		if(count == 0)
			valuelist.add(new com.mind.common.LabelValue("--Select--","0"));
		try{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql="select * from dbo.func_state_category('"+cat_id+"')";	
			rs = stmt.executeQuery(sql);
			int i=0;
			
			while(rs.next())
			{
				
				valuelist.add(new com.mind.common.LabelValue( rs.getString("cp_state_id"),rs.getString("cp_state_id")) );
			}
			
		}
		catch(Exception e){
			logger.error("getCategoryWiseStateList(String, int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCategoryWiseStateList(String, int, DataSource) - Error occured during getting State  List  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getCategoryWiseStateList(String, int, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getCategoryWiseStateList(String, int, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getCategoryWiseStateList(String, int, DataSource) - exception ignored", e);
}
			
		}
		return valuelist;	
	}
	
	public static ArrayList getSelectedDepoProductList(String ticket_id, String depo_product_id, String showproduct, DataSource ds) 
	{
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int i = 0;
		ArrayList valuelist = new ArrayList();
		try
		{
			if(depo_product_id.equals("")) depo_product_id = "-1";
			
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lo_get_selected_product_id(?, ?, ?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(ticket_id));
			cstmt.setString(3, depo_product_id);
			cstmt.setString(4, showproduct);
			rs  = cstmt.executeQuery();
			while ( rs.next())
			{
				TicketDetailBean tdBean = new TicketDetailBean();
				
				tdBean.setDe_product_id(rs.getString("dp_prod_id"));
				tdBean.setDe_product_name(rs.getString("dp_prod_name"));
				tdBean.setDepot_quantity(rs.getString("lm_td_quantity"));    
				valuelist.add(i,tdBean);
				i++;		
			}
		}
		catch( Exception e )
		{
			logger.error("getSelectedDepoProductList(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSelectedDepoProductList(String, String, String, DataSource) - Exception caught in rerieving Selected Depo Product List" + e);
			}
		}
		
		finally
		{
			
			try
			{
				if ( rs!= null ) 
					rs.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getSelectedDepoProductList(String, String, String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getSelectedDepoProductList(String, String, String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn!= null ) 
				   conn.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getSelectedDepoProductList(String, String, String, DataSource) - exception ignored", sql);
}
			
		}
		
		return valuelist;	
	}
	
	public static TicketDetailDTO getTicketDetail(TicketDetailDTO ticketdetailForm, String ticket_id, DataSource ds) 
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		DecimalFormat dfcurmargin	= new DecimalFormat( "0.00" );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement(); 
			sql = "select * from dbo.func_ticket_detail_01("+ticket_id+")";
			
			rs  = stmt.executeQuery(sql);
					
			if ( rs.next())
			{ 
				ticketdetailForm.setJobid(rs.getString("lm_tc_js_id"));
				ticketdetailForm.setTicket_number(rs.getString("lm_tc_number"));
				ticketdetailForm.setRequestor_name(rs.getString("lm_tc_requestor_name")+"~"+rs.getString("lm_tc_requestor_email"));
				ticketdetailForm.setRequestor_email(rs.getString("lm_tc_requestor_email"));
				
				ticketdetailForm.setSite_id(rs.getString("lm_si_id"));
				ticketdetailForm.setSite_name(rs.getString("lm_si_name"));
				ticketdetailForm.setSite_number(rs.getString("lm_si_number"));
				ticketdetailForm.setSite_address(rs.getString("lm_si_address"));
				ticketdetailForm.setSite_city(rs.getString("lm_si_city"));
				ticketdetailForm.setSite_state(rs.getString("lm_si_state"));
				ticketdetailForm.setSite_zipcode(rs.getString("lm_si_zip_code"));
				ticketdetailForm.setSite_country(rs.getString("lm_si_country"));
				ticketdetailForm.setLocality_uplift(dfcurmargin.format(rs.getFloat("lm_si_locality_factor"))+"");
				ticketdetailForm.setUnion_site(rs.getString("lm_si_union_site"));
				ticketdetailForm.setSite_worklocation(rs.getString("lm_si_work_location"));
				ticketdetailForm.setSite_direction(rs.getString("lm_si_directions"));
				ticketdetailForm.setPrimary_Name(rs.getString("lm_si_pri_poc"));
				ticketdetailForm.setSecondary_Name(rs.getString("lm_si_sec_poc"));
				ticketdetailForm.setSite_phone(rs.getString("lm_si_phone_no"));
				ticketdetailForm.setSecondary_phone(rs.getString("lm_si_sec_phone_no"));
				ticketdetailForm.setPrimary_email(rs.getString("lm_si_pri_email_id"));
				ticketdetailForm.setSecondary_email(rs.getString("lm_si_sec_email_id"));
				ticketdetailForm.setSite_notes(rs.getString("lm_si_notes"));
				String requested_date = rs.getString("lm_tc_requested_date");
				ticketdetailForm.setSitelatdeg(rs.getString("lm_si_lat_deg"));
				ticketdetailForm.setSitelatmin(rs.getString("lm_si_lat_min"));
				ticketdetailForm.setSitelatdirection(rs.getString("lm_si_lat_direction"));
				ticketdetailForm.setSitelondeg(rs.getString("lm_si_lon_deg"));
				ticketdetailForm.setSitelonmin(rs.getString("lm_si_lon_min"));
				ticketdetailForm.setSitelondirection(rs.getString("lm_si_lon_direction"));
				ticketdetailForm.setSitestatus(rs.getString("lm_si_status"));
				ticketdetailForm.setSitegroup(rs.getString("lm_si_group"));
				ticketdetailForm.setSiteendcustomer(rs.getString("lm_si_end_customer"));
				ticketdetailForm.setSitetimezone(rs.getString("lm_si_time_zone"));
				ticketdetailForm.setSiteregion(rs.getString("lm_si_region_id"));
				ticketdetailForm.setSitecategory(rs.getString("lm_si_category_id"));
			
			if(!requested_date.equals("")) { 
					ticketdetailForm.setRequested_date(requested_date.substring(0, 10));
					if(requested_date.substring(11,12).equals(" "))
						ticketdetailForm.setRequested_datehh("0"+requested_date.substring(12,13));
					else
						ticketdetailForm.setRequested_datehh(requested_date.substring(11,13));
					ticketdetailForm.setRequested_datemm(requested_date.substring(14,16));
					ticketdetailForm.setRequested_dateop(requested_date.substring(23,25));
				}
				
				ticketdetailForm.setStandby(rs.getString("lm_tc_standby"));
				ticketdetailForm.setMsp(rs.getString("lm_tc_msp"));
				
				ticketdetailForm.setHelpdesk(rs.getString("lm_tc_help_desk"));
				ticketdetailForm.setCategory(rs.getString("lm_tc_category"));
				ticketdetailForm.setCustomer_references(rs.getString("lm_tc_cust_reference"));
				ticketdetailForm.setProblem_description(rs.getString("lm_tc_problem_desc"));
				ticketdetailForm.setSpecial_instruction(rs.getString("lm_tc_spec_instruction"));
				ticketdetailForm.setCriticality(rs.getString("lm_tc_criticality"));
				ticketdetailForm.setResourcelevel(rs.getString("lm_tc_resource_level"));
				ticketdetailForm.setCrit_cat_id(rs.getString("lm_tc_crit_cat_id"));
				String arrivaldate = rs.getString("lm_tc_preferred_arrival");
				if(!arrivaldate.equals("")) { 
					ticketdetailForm.setArrivaldate(arrivaldate.substring(0, 10));
					if(arrivaldate.substring(11,12).equals(" "))
						ticketdetailForm.setArrivaldatehh("0"+arrivaldate.substring(12,13));
					else
						ticketdetailForm.setArrivaldatehh(arrivaldate.substring(11,13));
					ticketdetailForm.setArrivaldatemm(arrivaldate.substring(14,16));
					ticketdetailForm.setArrivaldateop(arrivaldate.substring(23,25));
				}
				String arrivalwindowstartdate = rs.getString("lm_tc_preferred_window_start");
				if(!arrivalwindowstartdate.equals("")) { 
					ticketdetailForm.setArrivalwindowstartdate(arrivalwindowstartdate.substring(0, 10));
					if(arrivalwindowstartdate.substring(11,12).equals(" "))
						ticketdetailForm.setArrivalwindowstartdatehh("0"+arrivalwindowstartdate.substring(12,13));
					else
						ticketdetailForm.setArrivalwindowstartdatehh(arrivalwindowstartdate.substring(11,13));
					ticketdetailForm.setArrivalwindowstartdatemm(arrivalwindowstartdate.substring(14,16));
					ticketdetailForm.setArrivalwindowstartdateop(arrivalwindowstartdate.substring(23,25));
				}
				String arrivalwindowenddate = rs.getString("lm_tc_preferred_window_end");
				if(!arrivalwindowenddate.equals("")) { 
					ticketdetailForm.setArrivalwindowenddate(arrivalwindowenddate.substring(0, 10));
					if(arrivalwindowenddate.substring(11,12).equals(" "))
						ticketdetailForm.setArrivalwindowenddatehh("0"+arrivalwindowenddate.substring(12,13));
					else
						ticketdetailForm.setArrivalwindowenddatehh(arrivalwindowenddate.substring(11,13));
					ticketdetailForm.setArrivalwindowenddatemm(arrivalwindowenddate.substring(14,16));
					ticketdetailForm.setArrivalwindowenddateop(arrivalwindowenddate.substring(23,25));
				}
				
				ticketdetailForm.setLo_ot_id(rs.getString("lo_ot_id"));
				ticketdetailForm.setCompany_name(rs.getString("lo_ot_name"));
				ticketdetailForm.setLm_ticket_type(rs.getString("lm_tc_type"));
				
				if(ticketdetailForm.getSite_name().equals("") && ticketdetailForm.getSite_number().equals("")) {
					ticketdetailForm.setUnion_site("N");
					ticketdetailForm.setLocality_uplift("1.0");
					ticketdetailForm.setSite_country("US");	
				}
				
				ticketdetailForm.setSitelistid(rs.getString("lp_sl_id"));
			}
		}
		catch( Exception e )
		{
			logger.error("getTicketDetail(TicketDetailForm, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTicketDetail(TicketDetailForm, String, DataSource) - Exception caught in rerieving Ticket Details" + e);
			}
		}
		
		finally
		{
			
			try
			{
				if ( rs!= null ) 
					rs.close();
			}
			catch( SQLException sql1 )
			{
				logger.warn("getTicketDetail(TicketDetailForm, String, DataSource) - exception ignored", sql1);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( SQLException sql1 )
			{
				logger.warn("getTicketDetail(TicketDetailForm, String, DataSource) - exception ignored", sql1);
}
			
			
			try
			{
				if ( conn!= null ) 
					conn.close();
			}
			catch( SQLException sql1 )
			{
				logger.warn("getTicketDetail(TicketDetailForm, String, DataSource) - exception ignored", sql1);
}
			
		}
		
		return ticketdetailForm;	
	}
	
	
	public static String[] getJobIdFromTicketNo(String ticketNumber, DataSource ds) 
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		String jobTicketIds[] = new String[2];
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement(); 
			sql = "select lm_tc_js_id, lm_tc_id from lm_ticket where lm_tc_number = '"+ticketNumber+"'";
			
			rs  = stmt.executeQuery(sql);
					
			if( rs.next())
			{
				jobTicketIds[0] = rs.getString("lm_tc_js_id");
				jobTicketIds[1] = rs.getString("lm_tc_id");
			}
		}
		catch( Exception e )
		{
			logger.error("getJobIdFromTicketNo(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobIdFromTicketNo(String, DataSource) - Exception caught in getting JobId From Ticket No" + e);
			}
		}
		
		finally
		{
			
			try
			{
				if ( rs!= null ) 
				  rs.close();
			}
			catch( SQLException sql1 )
			{
				logger.warn("getJobIdFromTicketNo(String, DataSource) - exception ignored", sql1);
}
			
			
			try
			{
				if ( stmt!= null ) 
				  stmt.close();
			}
			catch( SQLException sql1 )
			{
				logger.warn("getJobIdFromTicketNo(String, DataSource) - exception ignored", sql1);
}
			
			
			try
			{
				if ( conn!= null ) 
				  conn.close();
			}
			catch( SQLException sql1 )
			{
				logger.warn("getJobIdFromTicketNo(String, DataSource) - exception ignored", sql1);
}
			
		}
		
		return jobTicketIds;
	}
	
	
	public static String[] getProductListFromDatabase(String ticket_id, DataSource ds) 
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		String[] prod_list = null; 
		String prod_id = "";
		int i = 0;
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement(); 
			sql = "select lm_td_prod_id from lm_ticket_depot_quantity where lm_td_tc_id = "+ticket_id;
			
			rs  = stmt.executeQuery(sql);
					
			while ( rs.next())
			{
				prod_id = prod_id+","+rs.getString("lm_td_prod_id");
			i++;
			}
			if(prod_id.indexOf(",") != -1) {
				prod_id = prod_id.substring(1, prod_id.length());
				prod_list = prod_id.split(",");
			}
		}
		catch( Exception e )
		{
			logger.error("getProductListFromDatabase(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getProductListFromDatabase(String, DataSource) - Exception caught in rerieving Product List From Database" + e);
			}
		}
		
		finally
		{
			
			try
			{
				if ( rs!= null ) 
				  rs.close();
			}
			catch( SQLException sql1 )
			{
				logger.warn("getProductListFromDatabase(String, DataSource) - exception ignored", sql1);
}
			
			
			try
			{
				if ( stmt!= null ) 
				  stmt.close();
			}
			catch( SQLException sql1 )
			{
				logger.warn("getProductListFromDatabase(String, DataSource) - exception ignored", sql1);
}
			
			
			try
			{
				if ( conn!= null ) 
				  conn.close();
			}
			catch( SQLException sql1 )
			{
				logger.warn("getProductListFromDatabase(String, DataSource) - exception ignored", sql1);
}
			
		}
		
		return prod_list;	
	}
	
	public static int updateTicketDetail(TicketDetailDTO ticketdetailForm , String requested_date , String arrivaldate , String arrivalwindowstartdate , String arrivalwindowenddate , String loginuserid ,  DataSource ds )
	{
		
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int flag = -1;
		InputStream in = null;
		byte[] buffer1 = null;
		DecimalFormat dfcurmargin	= new DecimalFormat( "0.00" );
		
		if(ticketdetailForm.getSite_id().equals(""))
			ticketdetailForm.setSite_id("0");
		
		
		if(ticketdetailForm.getRequestor_name().indexOf("~") > 0) 
			ticketdetailForm.setRequestor_name(ticketdetailForm.getRequestor_name().substring(0, ticketdetailForm.getRequestor_name().indexOf("~")));
		
		/* System.out.println("2:::::::::::::::::::::::"+ticketdetailForm.getTicket_number() );
		System.out.println("3:::::::::::::::::::::::"+ticketdetailForm.getRequestor_name() );
		System.out.println("4:::::::::::::::::::::::"+ticketdetailForm.getRequestor_email() );
		System.out.println("5:::::::::::::::::::::::"+Integer.parseInt( ticketdetailForm.getSite_id() ) );
		System.out.println("6:::::::::::::::::::::::"+ticketdetailForm.getSite_name() );
		System.out.println("7:::::::::::::::::::::::"+ticketdetailForm.getSite_number() );
		System.out.println("8:::::::::::::::::::::::"+ticketdetailForm.getSite_address() );
		System.out.println("9:::::::::::::::::::::::"+ticketdetailForm.getSite_city() );
		System.out.println("10:::::::::::::::::::::::"+ticketdetailForm.getSite_state() );
		System.out.println("11:::::::::::::::::::::::"+ticketdetailForm.getSite_zipcode() );
		System.out.println("12:::::::::::::::::::::::"+ticketdetailForm.getSite_country() );
		System.out.println("13:::::::::::::::::::::::"+Float.parseFloat(ticketdetailForm.getLocality_uplift()) );
		System.out.println("14:::::::::::::::::::::::"+ticketdetailForm.getUnion_site()  );
		System.out.println("15:::::::::::::::::::::::"+ticketdetailForm.getSite_worklocation()  );  
		System.out.println("16:::::::::::::::::::::::"+ticketdetailForm.getSite_direction()  );  
		System.out.println("17:::::::::::::::::::::::"+ticketdetailForm.getPrimary_Name()  );  
		System.out.println("18:::::::::::::::::::::::"+ticketdetailForm.getSite_phone()  );
		System.out.println("19:::::::::::::::::::::::"+ticketdetailForm.getPrimary_email()  );  
		System.out.println("20:::::::::::::::::::::::"+ticketdetailForm.getSecondary_Name()  );
		System.out.println("21:::::::::::::::::::::::"+ticketdetailForm.getSecondary_phone()  );
		System.out.println("22:::::::::::::::::::::::"+ticketdetailForm.getSecondary_email()  );
		System.out.println("23:::::::::::::::::::::::"+ticketdetailForm.getSite_notes()+":::::::::::::");
		System.out.println("24:::::::::::::::::::::::"+requested_date );
		System.out.println("25:::::::::::::::::::::::"+ticketdetailForm.getStandby() );
		System.out.println("26:::::::::::::::::::::::"+ticketdetailForm.getMsp() );
		System.out.println("27:::::::::::::::::::::::"+ticketdetailForm.getHelpdesk());
		System.out.println("28:::::::::::::::::::::::"+Integer.parseInt(ticketdetailForm.getCategory()) );
		System.out.println("29:::::::::::::::::::::::"+ticketdetailForm.getCustomer_references() );
		System.out.println("30:::::::::::::::::::::::"+ticketdetailForm.getProblem_description() );
		System.out.println("31:::::::::::::::::::::::"+ticketdetailForm.getSpecial_instruction() );
		System.out.println("32:::::::::::::::::::::::"+buffer1 );
		System.out.println("33:::::::::::::::::::::::"+ticketdetailForm.getFilename().getFileName());
		System.out.println("34:::::::::::::::::::::::"+Integer.parseInt( ticketdetailForm.getCriticality() ) );
		System.out.println("35:::::::::::::::::::::::"+Integer.parseInt( ticketdetailForm.getResourcelevel() ) );
		System.out.println("36:::::::::::::::::::::::"+arrivaldate );
		System.out.println("37:::::::::::::::::::::::"+arrivalwindowstartdate ) ;
		System.out.println("38:::::::::::::::::::::::"+arrivalwindowenddate );
		System.out.println("39:::::::::::::::::::::::"+Integer.parseInt( loginuserid ) );
		System.out.println("40:::::::::::::::::::::::"+Integer.parseInt( ticketdetailForm.getCrit_cat_id() ) );
		System.out.println("41:::::::::::::::::::::::"+ticketdetailForm.getCompany_name()); 
		System.out.println("42:::::::::::::::::::::::"+ticketdetailForm.getAppendixid());
		System.out.println("43:::::::::::::::::::::::"+ticketdetailForm.getSitelatdeg());
		System.out.println("44:::::::::::::::::::::::"+ticketdetailForm.getSitelatmin());
		System.out.println("45:::::::::::::::::::::::"+ticketdetailForm.getSitelatdirection());
		System.out.println("46:::::::::::::::::::::::"+ticketdetailForm.getSitelondeg());
		System.out.println("47:::::::::::::::::::::::"+ticketdetailForm.getSitelonmin());
		System.out.println("48:::::::::::::::::::::::"+ticketdetailForm.getSitelondirection());
		System.out.println("49:::::::::::::::::::::::"+ticketdetailForm.getSitestatus());
		System.out.println("50:::::::::::::::::::::::"+ticketdetailForm.getSitegroup());
		System.out.println("51:::::::::::::::::::::::"+ticketdetailForm.getSiteendcustomer());
		System.out.println("52:::::::::::::::::::::::"+ticketdetailForm.getSitetimezone());
		System.out.println("53:::::::::::::::::::::::"+ticketdetailForm.getSiteregion());
		System.out.println("54:::::::::::::::::::::::"+ticketdetailForm.getSitecategory());
		System.out.println("55:::::::::::::::::::::::"+ticketdetailForm.getSitelistid());
		
		//System.out.println("26:::::::::::::::::::::::"+ticketdetailForm.getHelpdesk() );   */
		
		try {
			if(ticketdetailForm.getInputStream() != null) {
				in = ticketdetailForm.getInputStream();
				buffer1 = new byte[ticketdetailForm.getFileSize()];
				in.read(buffer1);	
			}
			
			if(buffer1 == null) buffer1 = "".getBytes();
			
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call lm_job_ticket_manage_01( ? , ? , ? , ? , ? , ?, ? , ? , ?, ? , ? , ? , ? , ? ,? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)}");  // Added 1 parameter for nettype-06/12/2008 
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString( 2 , ticketdetailForm.getTicket_number() );
			cstmt.setString( 3 , ticketdetailForm.getRequestor_name() );
			cstmt.setString( 4 , ticketdetailForm.getRequestor_email() );
			cstmt.setInt( 5 , Integer.parseInt( ticketdetailForm.getSite_id() ) );
			cstmt.setString( 6 , ticketdetailForm.getSite_name() );
			cstmt.setString( 7 , ticketdetailForm.getSite_number() );
			cstmt.setString( 8 , ticketdetailForm.getSite_address() );
			cstmt.setString( 9 , ticketdetailForm.getSite_city() );
			cstmt.setString( 10 , ticketdetailForm.getSite_state() );
			cstmt.setString( 11 , ticketdetailForm.getSite_zipcode() );
			cstmt.setString( 12 , ticketdetailForm.getSite_country() );
			cstmt.setFloat( 13 , Float.parseFloat(ticketdetailForm.getLocality_uplift()));
			cstmt.setString( 14 , ticketdetailForm.getUnion_site()  );
			cstmt.setString( 15 , ticketdetailForm.getSite_worklocation()  );  
			cstmt.setString( 16 , ticketdetailForm.getSite_direction()  );  
			cstmt.setString( 17 , ticketdetailForm.getPrimary_Name()  );  
			cstmt.setString( 18 , ticketdetailForm.getSite_phone()  );
			cstmt.setString( 19 , ticketdetailForm.getPrimary_email()  );  
			cstmt.setString( 20 , ticketdetailForm.getSecondary_Name()  );
			cstmt.setString( 21 , ticketdetailForm.getSecondary_phone()  );
			cstmt.setString( 22 , ticketdetailForm.getSecondary_email()  );
			cstmt.setString( 23 , ticketdetailForm.getSite_notes()  );
			cstmt.setString( 24 , requested_date );
			cstmt.setString( 25 , ticketdetailForm.getStandby() );
			cstmt.setString( 26 , ticketdetailForm.getMsp() );
			cstmt.setString( 27 , ticketdetailForm.getHelpdesk() );
			cstmt.setInt( 28 , Integer.parseInt(ticketdetailForm.getCategory()) );
			cstmt.setString( 29 , ticketdetailForm.getCustomer_references() );
			cstmt.setString( 30 , ticketdetailForm.getProblem_description() );
			cstmt.setString( 31 , ticketdetailForm.getSpecial_instruction() );
			cstmt.setBytes( 32 , buffer1 );
			cstmt.setString( 33 , ticketdetailForm.getFileName());
			cstmt.setInt( 34 , Integer.parseInt( ticketdetailForm.getCriticality() ) );
			
			cstmt.setInt( 35 , Integer.parseInt( ticketdetailForm.getResourcelevel() ) );
			cstmt.setString( 36 , arrivaldate );
			cstmt.setString( 37 , arrivalwindowstartdate ) ;
			cstmt.setString( 38 , arrivalwindowenddate );
			cstmt.setInt( 39 , Integer.parseInt( loginuserid ) );
			cstmt.setInt( 40 , Integer.parseInt( ticketdetailForm.getCrit_cat_id() ) );
			cstmt.setString( 41 , ticketdetailForm.getCompany_name());
			cstmt.setInt( 42 , Integer.parseInt( ticketdetailForm.getAppendixid()));
			cstmt.setString(43,ticketdetailForm.getSitelatdeg());
			cstmt.setString(44,ticketdetailForm.getSitelatmin());
			cstmt.setString( 45, ticketdetailForm.getSitelatdirection());
			cstmt.setString(46,ticketdetailForm.getSitelondeg());
			cstmt.setString(47,ticketdetailForm.getSitelonmin());
			cstmt.setString( 48, ticketdetailForm.getSitelondirection());
			cstmt.setString( 49, ticketdetailForm.getSitestatus());
			cstmt.setString( 50, ticketdetailForm.getSitegroup());
			cstmt.setString( 51, ticketdetailForm.getSiteendcustomer());
			cstmt.setString( 52, ticketdetailForm.getSitetimezone());
			cstmt.setString( 53, ticketdetailForm.getSiteregion());
			cstmt.setString( 54, ticketdetailForm.getSitecategory());
			cstmt.setString( 55, ticketdetailForm.getSitelistid());
			cstmt.setString( 56, ticketdetailForm.getNettype());
			cstmt.execute();
			
			flag = cstmt.getInt( 1 );	
		}
		
		catch( Exception e ){
			logger.error("updateTicketDetail(TicketDetailForm, String, String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateTicketDetail(TicketDetailForm, String, String, String, String, String, DataSource) - Error occured during update ticket " + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("updateTicketDetail(TicketDetailForm, String, String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if (cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("updateTicketDetail(TicketDetailForm, String, String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("updateTicketDetail(TicketDetailForm, String, String, String, String, String, DataSource) - exception ignored", e);
}
			
		}
		return flag;	
	}
	
	
	public  static ArrayList getDepoCategoryList(String msaid, DataSource ds)
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
				
		try{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql="select * from dbo.func_dp_product_category('"+msaid+"')";	
			rs = stmt.executeQuery(sql);
			int i=0;
			
			while(rs.next())
			{
				TicketDetailDTO tdForm = new TicketDetailDTO();
				
				tdForm.setDp_cat_id(rs.getString("dp_cat_id"));
				tdForm.setDp_cat_name(rs.getString("dp_cat_name"));
				 
				valuelist.add(i,tdForm);
				i++;		 
			}
			
		}
		catch(Exception e){
			logger.error("getDepoCategoryList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDepoCategoryList(String, DataSource) - Error occured during getting Depo Category List  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getDepoCategoryList(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getDepoCategoryList(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getDepoCategoryList(String, DataSource) - exception ignored", e);
}
			
		}
		return valuelist;	
	}
	
	
	public static java.util.Collection getStateName(DataSource ds) 
	{
		
		ArrayList statename = new java.util.ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		statename.add( new com.mind.common.LabelValue( "Select State" , "-1") );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery( "select cp_state_id, cp_state_name from cp_state order by cp_state_id" );
			while( rs.next() )
			{
				statename.add(new com.mind.common.LabelValue(rs.getString("cp_state_id")+ " "+rs.getString("cp_state_name"), rs.getString( "cp_state_id" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getStateName(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStateName(DataSource) - Exception caught in rerieving State Name" + e);
			}
		}
		finally
		{
			
			try
			{
				if ( rs!= null ) 
					rs.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getStateName(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( stmt!= null ) 
			    	stmt.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getStateName(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn!= null ) 
					conn.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getStateName(DataSource) - exception ignored", sql);
}
			
		}
		
		return statename;
	}
	
	public  static void getRefershedStates(TicketDetailDTO ticketdetailForm ,DataSource ds)
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
				
		try{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql="select cp_state_id,cp_country_id,cp_category_id,cp_region_id  from cp_state where cp_state_id ='"+ticketdetailForm.getSite_state()+"'";
			
			rs = stmt.executeQuery(sql);
						
			while(rs.next())
			{
				
				ticketdetailForm.setSite_state(rs.getString("cp_state_id"));
				ticketdetailForm.setSite_country(rs.getString("cp_country_id"));
				ticketdetailForm.setSitecategory(rs.getString("cp_category_id"));
				ticketdetailForm.setSiteregion(rs.getString("cp_region_id"));
			}
			
		}
		catch(Exception e){
			logger.error("getRefershedStates(TicketDetailForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRefershedStates(TicketDetailForm, DataSource) - Error occured during getting country , Category  , Region   " + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getRefershedStates(TicketDetailForm, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getRefershedStates(TicketDetailForm, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getRefershedStates(TicketDetailForm, DataSource) - exception ignored", e);
}
			
		}
			
	}
	
	public  static java.util.Collection getDepoProductList(String dp_cat_id, DataSource ds)
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
				
		try{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql="select * from dbo.func_dp_product('"+dp_cat_id+"')";	
			rs = stmt.executeQuery(sql);
			int i=0;
			
			while(rs.next())
			{
				valuelist.add(new com.mind.common.LabelValue(rs.getString("dp_prod_name"), rs.getString( "dp_prod_id" ) ) );
			}
			
		}
		catch(Exception e){
			logger.error("getDepoProductList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDepoProductList(String, DataSource) - Error occured during getting Depo Product List  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getDepoProductList(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getDepoProductList(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getDepoProductList(String, DataSource) - exception ignored", e);
}
			
		}
		return valuelist;	
	}
	
	public  static ArrayList getCriticalityCategorylist(String crit_id ,DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		ArrayList valuelist=new ArrayList();
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select clr_cat_id, clr_cat_short_name from dbo.func_lo_criticality_category("+crit_id+")";						   		
			rs = stmt.executeQuery(sql);
			int i=0;
			
			while(rs.next())
			{
				TicketDetailDTO tdForm = new TicketDetailDTO();
				tdForm.setCrit_cat_id(rs.getString("clr_cat_id"));
				tdForm.setCrit_cat_name(rs.getString("clr_cat_short_name"));
				 
				 valuelist.add(i,tdForm);
				 i++;
			}
		}
		catch(Exception e){
			logger.error("getCriticalityCategorylist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCriticalityCategorylist(String, DataSource) - Error occured during getting Criticality Category list  " + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getCriticalityCategorylist(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getCriticalityCategorylist(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getCriticalityCategorylist(String, DataSource) - exception ignored", e);
}
			
		}
		return valuelist;	
	}
	
	public static int UpdateDepotQuantity(int count, String ticket_id, String product_id, String depot_quantity, DataSource ds)
    {
       	int retval;
        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        retval = 0;
        try
        {
			conn = ds.getConnection();
            cstmt = conn.prepareCall("{?=call lo_depot_quantity_manage_01(?,?,?,?)}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, count);
            cstmt.setInt(3, Integer.parseInt(ticket_id));
            cstmt.setInt(4, Integer.parseInt(product_id));
            cstmt.setFloat(5, Float.parseFloat(depot_quantity));
            cstmt.execute();
            retval = cstmt.getInt(1);
        }
        catch(Exception e)
        {
			logger.error("UpdateDepotQuantity(int, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("UpdateDepotQuantity(int, String, String, String, DataSource) - Error occured during Update Depot Quantity " + e);
			}
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
            }
            catch(Exception exception1) {
				logger.warn("UpdateDepotQuantity(int, String, String, String, DataSource) - exception ignored", exception1);
 }
            try
            {
                if(cstmt != null)
                {
                    cstmt.close();
                }
            }
            catch(Exception exception2) {
				logger.warn("UpdateDepotQuantity(int, String, String, String, DataSource) - exception ignored", exception2);
 }
            try
            {
                if(conn != null)
                {
                    conn.close();
                }
            }
            catch(Exception exception3) {
				logger.warn("UpdateDepotQuantity(int, String, String, String, DataSource) - exception ignored", exception3);
 }
        }
        return retval;
    }
	
	
	public static String getRules( String msaid,DataSource ds ) 
	{
		Connection conn = null;
		Statement stmt=null;
		String rules="";
		
		
		String sql="";
		ResultSet rs=null;
		try
		{
			conn=ds.getConnection();
			stmt = conn.createStatement();
			sql="select * from dbo.func_clr_master_detail("+msaid+")";
			rs=stmt.executeQuery(sql);
			
			if ( rs.next())
			{ 
								
				rules= rs.getString ("clr_rules");
				
			}
			
			
		}
		catch ( Exception e )
		{
			logger.error("getRules(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRules(String, DataSource) - Exception Caught in getRules" + e);
			}
		}
		
		finally
		{
			try
			{
				if( rs != null )
				{
					rs.close();
				}
			}
			
			catch( Exception e )
			{
				logger.error("getRules(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getRules(String, DataSource) - Exception Caught in getclrdetail" + e);
				}
			}
			try
			{
				if( stmt!= null )
				{
					stmt.close();
				}
			}
			catch( Exception e )
			{
				logger.error("getRules(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getRules(String, DataSource) - Exception Caught in getclrdetail" + e);
				}
			}
			
			
			try
			{
				if( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( Exception e )
			{
				logger.error("getRules(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getRules(String, DataSource) - Exception Caught in getclrdetail" + e);
				}
			}
			
		}
		return rules;
	}
	
	public static String getNetMedXAppendixId(String msaid, DataSource ds ) 
	{
		Connection conn = null;
		Statement stmt=null;
		String appendixid = "";
		
		String sql = "";
		ResultSet rs=null;
		try
		{
			conn=ds.getConnection();
			stmt = conn.createStatement();
			sql = "select dbo.func_get_netmedx_appendix_id('"+msaid+"')";
			rs=stmt.executeQuery(sql);
			
			if ( rs.next())
			{ 
				appendixid = rs.getString (1);
			}
			
			
		}
		catch ( Exception e )
		{
			logger.error("getNetMedXAppendixId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNetMedXAppendixId(String, DataSource) - Exception Caught in get NetMedX Appendix Id" + e);
			}
		}
		
		finally
		{
			try
			{
				if( rs != null )
				{
					rs.close();
				}
			}
			
			catch( Exception e )
			{
				logger.error("getNetMedXAppendixId(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getNetMedXAppendixId(String, DataSource) - Exception Caught in get NetMedX Appendix Id" + e);
				}
			}
			try
			{
				if( stmt!= null )
				{
					stmt.close();
				}
			}
			catch( Exception e )
			{
				logger.error("getNetMedXAppendixId(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getNetMedXAppendixId(String, DataSource) - Exception Caught in get NetMedX Appendix Id" + e);
				}
			}
			
			
			try
			{
				if( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( Exception e )
			{
				logger.error("getNetMedXAppendixId(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getNetMedXAppendixId(String, DataSource) - Exception Caught in get NetMedX Appendix Id" + e);
				}
			}
			
		}
		return appendixid;
	}
	
	public static String getMspStatus(String appendixId,DataSource ds) {
		String checkStatus="";
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String sql="";
		try {
			conn=ds.getConnection();
			stmt=conn.createStatement();
			sql="select * from lx_appendix_detail where lx_pd_pr_id="+appendixId;
			rs=stmt.executeQuery(sql);
			if(rs.next()) {
				checkStatus=rs.getString("lx_pd_msp");
			}
		
		} catch (Exception e) {
			logger.error("getMspStatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMspStatus(String, DataSource) - Error caught getting MspValue " + e);
			}
		} finally {

			try
			{
				if( rs != null )
				{
					rs.close();
				}
			}
			
			catch( Exception e )
			{
				logger.error("getMspStatus(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getMspStatus(String, DataSource) - Exception Caught in get MSP " + e);
				}
			}
			try
			{
				if( stmt!= null )
				{
					stmt.close();
				}
			}
			catch( Exception e )
			{
				logger.error("getMspStatus(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getMspStatus(String, DataSource) - Exception Caught in get MSP" + e);
				}
			}
			
			
			try
			{
				if( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( Exception e )
			{
				logger.error("getMspStatus(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getMspStatus(String, DataSource) - Exception Caught in MSP" + e);
				}
			}
			
		
		}
		
		return checkStatus;
	}
	
}
