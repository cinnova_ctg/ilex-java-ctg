package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.Decimalroundup;
import com.mind.bean.prm.AddPoNumber;
import com.mind.bean.prm.AddPoNumberBean;


public class AddPoNumberdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AddPoNumberdao.class);
	
	public static ArrayList getJobDetail(String type_id,String used_for, DataSource ds)
	{
		
		Connection conn=null;
		CallableStatement cstmt=null;
		ResultSet rs=null;
		int retval=-1;
		ArrayList valuelist=new ArrayList();
		try{
			
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call  lm_partner_purchase_order_01(?,?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2,type_id);
			cstmt.setString(3,used_for);
			cstmt.execute();
			retval = cstmt.getInt(1);
			if(retval==0)
			{	
				rs=cstmt.executeQuery();
				if(rs.next())
				{
					AddPoNumberBean apnform = new AddPoNumberBean();
					if(rs.getString("po_number")=="0")
					{
						apnform.setPonumber("");
					}
					else
					{
						
						apnform.setPonumber(rs.getString("po_number"));
					}
					if(rs.getString("wo_number")=="0")
					{
						apnform.setWonumber("");
					}
					else
					{
						
						apnform.setWonumber(rs.getString("wo_number"));
					}
					
					apnform.setPartnername(rs.getString("lm_ap_partner_name"));
					
					apnform.setEstimatedcost(Decimalroundup.twodecimalplaces( rs.getFloat("lx_ce_total_price"),2));
					apnform.setInvoicedcost(Decimalroundup.twodecimalplaces( rs.getFloat("lx_ce_invoice_price"),2));
					
					apnform.setJobname(rs.getString("lm_js_title"));
					apnform.setJobid(rs.getString("lm_js_id"));
					valuelist.add(apnform);
					
				}
			}
			
			
		}
		catch(Exception e){
			logger.error("getJobDetail(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobDetail(String, String, DataSource) - Error occured during  getting jobdetail for po number add" + e);
			}
		}
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getJobDetail(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getJobDetail(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getJobDetail(String, String, DataSource) - exception ignored", e);
}
			
		}return valuelist;
	}	
	
	
	
	public static int checkpartnerassign(String type_id, String used_for, DataSource ds)
	{
		
		Connection conn=null;
		CallableStatement cstmt=null;
		ResultSet rs=null;
		int retval=0;
		
		//System.out.println("type_id:::::"+type_id);
		//System.out.println("used_for:::::"+used_for);
		
		try{
			
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call lm_partner_purchase_order_01(?,?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2,type_id);
			cstmt.setString(3,used_for);
			cstmt.execute();
			retval = cstmt.getInt(1);
						
		}
		catch(Exception e){
			logger.error("checkpartnerassign(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkpartnerassign(String, String, DataSource) - Error occured during  checking partner assign for a job " + e);
			}
		}
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("checkpartnerassign(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("checkpartnerassign(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("checkpartnerassign(String, String, DataSource) - exception ignored", e);
}
			
		}
		return retval;
	}	
	
	
	
	public  static ArrayList getPartnercostlist(String typeid, String type,String ponumber,String wonumber, DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		ArrayList valuelist=new ArrayList();
		AddPoNumber apnbean = null;
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			
			
			String sql="select * from func_partner_purchase_order("+typeid+",'"+type+"','"+ponumber+"','"+wonumber+"')";						   		
			rs = stmt.executeQuery(sql);
			
			int i=1;
			while(rs.next())
			{
				
				apnbean = new AddPoNumber();
				//apnbean.setPartnerid(rs.getString("lm_ap_partner_id"));
				if(rs.getString("po_number")==null||rs.getString("po_number")=="0")
				{
					apnbean.setPonumberact("");
				}
				else
				{
					
					apnbean.setPonumberact(rs.getString("po_number"));
				}
				if(rs.getString("wo_number")==null||rs.getString("wo_number")=="0")
				{
					apnbean.setWonumberact("");
				}
				else
				{
					
					apnbean.setWonumberact(rs.getString("wo_number"));
				}
				
				apnbean.setPartnername(rs.getString("lm_ap_partner_name"));
				
				apnbean.setEstimatedcost(Decimalroundup.twodecimalplaces( rs.getFloat("lx_ce_total_price"),2));
				apnbean.setInvoicedcost(Decimalroundup.twodecimalplaces( rs.getFloat("lx_ce_invoice_price"),2));
				
				apnbean.setActivityname(rs.getString("lm_at_title"));
				apnbean.setActivityid(rs.getString("lm_at_id"));
				apnbean.setHid_actid(rs.getString("lm_at_id"));				
				valuelist.add(apnbean);
				i++;
			}
				
			
		}
		catch(Exception e){
			logger.error("getPartnercostlist(String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnercostlist(String, String, String, String, DataSource) - Error occured during getting partner activity list  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getPartnercostlist(String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getPartnercostlist(String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getPartnercostlist(String, String, String, String, DataSource) - exception ignored", e);
}
			
		}
		return valuelist;	
	}
	
	
	public  static int AddPonumber(String id, String transaction_type, String lm_po_number ,String lm_wo_number ,String user,DataSource ds)
	{
		Connection conn=null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		
		int retval=0;
		try{
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call lm_assign_po_number_01(?,?,?,?,?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2,id);
			cstmt.setString(3,transaction_type);
			cstmt.setString(4,lm_po_number);
			cstmt.setString(5,lm_wo_number);
			cstmt.setString(6,user);
			cstmt.execute();
			retval = cstmt.getInt(1);
			
		}
		catch(Exception e){
			logger.error("AddPonumber(String, String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("AddPonumber(String, String, String, String, String, DataSource) - Error occured during assigning po number " + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("AddPonumber(String, String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("AddPonumber(String, String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("AddPonumber(String, String, String, String, String, DataSource) - exception ignored", e);
}
			
		}
		return retval;		
	}

	/*public static java.util.ArrayList getActivityPartnerCostList(String id, String act_id, String partner_id, String type, DataSource ds) 
	 {
		ArrayList ATList = new ArrayList();
		AddPartnerCostBean ac = new AddPartnerCostBean();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = ""; 
		String temp_str = "";
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql="select * from dbo.func_lx_partner_cost_list("+id+", "+act_id+", "+partner_id+", '"+type+"')";
			//System.out.println("getActivityPartnerCostList::::::::::::"+sql);
			rs = stmt.executeQuery(sql);
			while (rs.next())
			  {
			  ac = new AddPartnerCostBean();
			  ac.setHid_actid(rs.getString(1));
			  ac.setActivityid(rs.getString(1));
			  ac.setActivityname(rs.getString(2));
	          if(rs.getString(3) != null) 
				  ac.setQuantity(rs.getString(3));
	          else 
				  ac.setQuantity("");
			  if(rs.getString(4) != null) 
				  ac.setUnitcost(Decimalroundup.twodecimalplaces(rs.getFloat(4), 2));
	          else 
				  ac.setQuantity("");
			  if(rs.getString(5) != null) 
				  ac.setTotalcost(Decimalroundup.twodecimalplaces(rs.getFloat(5), 2));
	          else 
				  ac.setQuantity("");
			  ATList.add( ac );
			}
		}
		catch( Exception e )
		{
			System.out.println( "Exception caught in rerieving Activity List" + e );
		}
		
		finally
		{
			try
			{
				if ( stmt != null )
				{
					stmt.close();
				}	
			}
			catch( Exception e )
			{
				System.out.println("Error occured during closing Statement "+e);
			}
			
			
			try
			{
				if ( conn != null)
				{ 
					conn.close();
				}	
			}
			catch( Exception e )
			{
				System.out.println("Error occured during closing Connection "+e);
			}
			
		}
		
		return ATList;	
	}
	
	*/
	public  static int AddPartnerCost(String id, String fromtype, String job_id, String quantity, String unitcost, String totalcost, String user, DataSource ds)
	{
		Connection conn=null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int retval=0;
		if(quantity.equals("")) quantity = "0";
		if(unitcost.equals("")) unitcost = "0";
		if(totalcost.equals("")) totalcost = "0";
		
		//System.out.println("id::::::::::::"+id);
		//System.out.println("fromtype::::::::::::"+fromtype);
		//System.out.println("job_id::::::::::::"+job_id);
		//System.out.println("quantity::::::::::::"+quantity);
		//System.out.println("unitcost::::::::::::"+unitcost);
		//System.out.println("totalcost::::::::::::"+totalcost);
		//System.out.println("user::::::::::::"+user);
		
		try {
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call lx_partner_cost_manage_01(?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2,id);
			cstmt.setString(3,fromtype);
			cstmt.setString(4,job_id);
			//System.out.println("000000000000000");
			cstmt.setFloat(5,Float.parseFloat(quantity));
			cstmt.setFloat(6,Float.parseFloat(unitcost));
			cstmt.setFloat(7,Float.parseFloat(totalcost));
			cstmt.setString(8,user);
			//System.out.println("1111111111111111");
			cstmt.execute();
			retval = cstmt.getInt(1);
			
		}
		catch(Exception e){
			logger.error("AddPartnerCost(String, String, String, String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("AddPartnerCost(String, String, String, String, String, String, String, DataSource) - Error occured during add partner cost " + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("AddPartnerCost(String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("AddPartnerCost(String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("AddPartnerCost(String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
			
		}
		return retval;		
	}
	
	
}
