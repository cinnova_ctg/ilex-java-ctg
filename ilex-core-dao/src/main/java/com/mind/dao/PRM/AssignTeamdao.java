package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.fw.core.dao.util.DBUtil;

/**
 * Class Discription: This class is use for database connection for Assignment
 * of Appendix Team.
 * 
 * @deprecated - getPocList(DataSource) - getRoleList(DataSource) -
 *             getAssignTeamList(String, DataSource) - assignRole(String,
 *             String, String, DataSource) - deleteAssignedRole(String, String,
 *             String, DataSource)
 * 
 * @author Vijay Kumar Singh
 * 
 */
public class AssignTeamdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AssignTeamdao.class);
	private static final String PROJECT_MANAGER = "Project Manager";
	private static final String JOB_OWNER = "Job Owner";
	private static final String SCHEDULER = "Scheduler";
	private static final String TEAM_LEAD = "Team Lead";
	private static final String INVOICE_GENERATOR = "Invoice Generator";

	/**
	 * Discription: This method is use to get only those poc list which are
	 * active and with a classification of Project or Dispatch.
	 * 
	 * @param ds
	 *            - Database Object
	 * @return ArrayList - Poc List
	 */
	public static ArrayList getPocList(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList teamList = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from lm_team_list order by poc_name";
			// System.out.println("XXXXX sql "+sql);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				teamList.add(new com.mind.common.LabelValue(rs
						.getString("poc_name"), rs.getString("poc_id")));
			}

		} catch (Exception e) {
			logger.error("getPocList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocList(DataSource) - Error occured com.mind.dao.PRM.AssignTeamdao.getTeamList() "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return teamList;
	}
	public static ArrayList<AssignTeam> getAllPocRoleList(String appendixid,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Statement stmt1 = null;
		ResultSet rs1 = null;
		ArrayList<AssignTeam> teamList = new ArrayList<AssignTeam>();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			stmt1 = conn.createStatement();
			String sql = "SELECT distinct lm_appendix_pc_id FROM lm_appendix_team WHERE  lm_appendix_pr_id="
					+ appendixid + " AND lm_appendix_ro_id NOT IN (1,4)";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				AssignTeam assignTeam = new AssignTeam();
				String userid = rs.getString("lm_appendix_pc_id");
				String userName = getUserName(userid, ds);
				if (!"--Select--".equals(userName)) {
					assignTeam.setPocId(userid);
					assignTeam.setPocName(userName);
					String rolesQuery = "SELECT * FROM lm_appendix_team la left join   lm_team_role_list lt ON la.lm_appendix_ro_id = lt.role_id WHERE lm_appendix_pc_id = "
							+ userid
							+ " AND lm_appendix_pr_id="
							+ appendixid
							+ " ORDER BY lt.role_title";
					rs1 = stmt1.executeQuery(rolesQuery);
					ArrayList<String> roleNames = new ArrayList<String>();
					ArrayList<String> roleIds = new ArrayList<String>();
					while (rs1.next()) {
						String roleId = rs1.getString("lm_appendix_ro_id");
						roleIds.add(roleId);
						String roleName = rs1.getString("role_title");
						roleNames.add(roleName);
					}
					roleNames = checkRoleNames(roleNames);
					assignTeam.setRoleNames(roleNames);
					roleIds = checkRoleIds(roleIds);
					assignTeam.setRoleIds(roleIds);
					teamList.add(assignTeam);
				}
			}
		} catch (Exception e) {
			logger.error("getPocList(DataSource)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("getPocList(DataSource) - Error occured com.mind.dao.PRM.AssignTeamdao.getTeamList() "
						+ e);
			}
		}
		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return teamList;
	}
	private static String getUserName(String userid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String fullName = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT lo_pc_first_name+' '+lo_pc_last_name AS fname FROM lo_poc WHERE lo_pc_id="
					+ userid;
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				fullName = rs.getString("fname");
			}
		} catch (Exception e) {
			logger.error("getPocList(DataSource)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("getPocList(DataSource) - Error occured com.mind.dao.PRM.AssignTeamdao.getTeamList() "
						+ e);
			}
		}
		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return fullName;
	}
	private static ArrayList<String> checkRoleIds(ArrayList<String> roleIds) {
		String[] rolesList = new String[5];
		for (int i = 0; i < roleIds.size(); i++) {
			String role = roleIds.get(i);
			if (role.equals("6")) {
				rolesList[0] = "6";
			} else if (role.equals("2")) {
				rolesList[1] = "2";
			} else if (role.equals("3")) {
				rolesList[2] = "3";
			} else if (role.equals("4")) {
				rolesList[3] = "4";
			} else if (!role.equals("5")) {
				rolesList[4] = "5";
			}
		}
		ArrayList<String> newRolesList = new ArrayList<String>(
				Arrays.asList(rolesList));
		return newRolesList;
	}
	private static ArrayList<String> checkRoleNames(ArrayList<String> roleNames) {
		String[] rolesList = new String[5];
		for (int i = 0; i < roleNames.size(); i++) {
			String role = roleNames.get(i);
			if (role.equals("Invoice Generator")) {
				rolesList[0] = "Invoice Generator";
			} else if (role.equals("Job Owner")) {
				rolesList[1] = "Job Owner";
			} else if (role.equals("Project Manager")) {
				rolesList[2] = "Project Manager";
			} else if (role.equals("Scheduler")) {
				rolesList[3] = "Scheduler";
			} else if (!role.equals("Team Lead")) {
				rolesList[4] = "Team Lead";
			}
		}
		ArrayList<String> newRolesList = new ArrayList<String>(
				Arrays.asList(rolesList));
		return newRolesList;
	}

	/**
	 * Discription: This method is use to get Role list for Assign Team from
	 * Database.
	 * 
	 * @param ds
	 *            - Database object
	 * @return ArrayList - Role List
	 */
	public static ArrayList getRoleList(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList roleList = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from lm_team_role_list where role_id not in (1,4) ";
			// System.out.println("XXXXX sql "+sql);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				roleList.add(new com.mind.common.LabelValue(rs
						.getString("role_title"), rs.getString("role_id")));
			}

		} catch (Exception e) {
			logger.error("getRoleList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRoleList(DataSource) - Error occured com.mind.dao.PRM.AssignTeamdao.getRoleList() "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return roleList;
	}

	/**
	 * Discription: This method is use to get all assign team list and its role
	 * Team from Database.
	 * 
	 * @param ds
	 *            - Database object
	 * @return ArrayList - Role List
	 */
	public static ArrayList getAssignTeamList(String appendixId, DataSource ds) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList assignTeamList = new ArrayList();
		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select * from func_lm_appendix_team_list(?)");
			pstmt.setString(1, appendixId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				AssignTeam assignTeam = new AssignTeam();
				assignTeam.setAppendixId(rs.getString("appendix_id")); // -
																		// Appendix
																		// Id
				assignTeam.setPocId(rs.getString("poc_id")); // - Poc Id
				assignTeam.setPocName(rs.getString("poc_name")); // - Poc Name
				// assignTeam.setRoleId(rs.getString("role_id")); // - Role Id
				// assignTeam.setRoleName(rs.getString("role_name")); // - Role
																	// Name
				assignTeamList.add(assignTeam);
			}

		} catch (Exception e) {
			logger.error("getAssignTeamList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAssignTeamList(String, DataSource) - Error occured com.mind.dao.PRM.AssignTeamdao.getAssignTeamList() "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return assignTeamList;
	}

	/**
	 * Discription: This method is use to get all assign team list and its role
	 * Team from Database.
	 * 
	 * @param ds
	 *            - Database object
	 * @return ArrayList - Role List
	 */
	public static int assignRole(String appendixId, String roleId,
			String pocId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		int val = -1;

		// System.out.println("appendixId "+appendixId);
		// System.out.println("roleId "+roleId );
		// System.out.println("pocId "+pocId);

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_appendix_role_manage_01(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, appendixId);
			cstmt.setString(3, roleId);
			cstmt.setString(4, pocId);
			cstmt.execute();
			val = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error("assignRole(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("assignRole(String, String, String, DataSource) - Error occured com.mind.dao.PRM.AssignTeamdao.getAssignRole() "
						+ e);
			}
		}

		finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return val;
	}

	/**
	 * Discription: This method is use to delet assigned poc from Database.
	 * 
	 * @param ds
	 *            - Database object
	 * @return ArrayList - Role List
	 */
	public static int deleteAssignedRole(String appendixId, String roleId,
			String pocId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		int val = -1;

		// System.out.println("appendixId "+appendixId);
		// System.out.println("roleId "+roleId );
		// System.out.println("pocId "+pocId);

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_appendix_role_delete_01(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, appendixId);
			cstmt.setString(3, roleId);
			cstmt.setString(4, pocId);
			cstmt.execute();
			val = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"deleteAssignedRole(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteAssignedRole(String, String, String, DataSource) - Error occured com.mind.dao.PRM.AssignTeamdao.getAssignRole() "
						+ e);
			}
		}

		finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return val;
	}

	public static void saveCustomerEmail(DataSource ds, String appendixid,
			String customerEmail) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String query = "UPDATE dbo.lx_appendix_main	SET lx_pr_email ='"
					+ customerEmail + "' WHERE lx_pr_id = " + appendixid;
			int result = stmt.executeUpdate(query);
		} catch (Exception e) {
			logger.error("getPocList(DataSource)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("getPocList(DataSource) - Error occured com.mind.dao.PRM.AssignTeamdao.getTeamList() "
						+ e);
			}
		}
		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
	}
	public static String getCustomerEmail(DataSource dataSource,
			String appendixid) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String email = "";
		try {
			conn = dataSource.getConnection();
			stmt = conn.createStatement();
			String query1 = "select lx_pr_email from dbo.lx_appendix_main where lx_pr_id = "
					+ appendixid;
			rs = stmt.executeQuery(query1);
			if (rs.next()) {
				email = rs.getString("lx_pr_email");
			}
		} catch (Exception e) {
			logger.error("getPocList(DataSource)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("getPocList(DataSource) - Error occured com.mind.dao.PRM.AssignTeamdao.getTeamList() "
						+ e);
			}
		}
		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return email;
	}
}
