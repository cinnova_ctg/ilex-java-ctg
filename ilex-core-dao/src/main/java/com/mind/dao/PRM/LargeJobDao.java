package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.BulkJobCreationBean;
import com.mind.common.CheckDateFormat;
import com.mind.fw.core.dao.util.DBUtil;

public class LargeJobDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(LargeJobDao.class);

	public static ArrayList getValidationError(String[][] sitedate, int j) {
		BulkJobCreationBean bulkjobForm = null;
		ArrayList valuelist = new ArrayList();
		try {
			int i = 0;
			while (i < j) {
				bulkjobForm = new BulkJobCreationBean();
				bulkjobForm.setErrorrSiteNumber(sitedate[i][0].trim());
				bulkjobForm.setErrorPlannedStartDate(sitedate[i][1].trim());
				valuelist.add(i, bulkjobForm);
				i++;
			}
		} catch (Exception e) {
			logger.error("getValidationError(String[][], int)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getValidationError(String[][], int) - " + e);
			}
		}
		return valuelist;

	}

	public static String getJobName(BulkJobCreationBean form) {
		String jobName = null;
		if (form.getPrifix().equals("P")) {
			jobName = form.getSiteNumber() + " " + form.getJob_Name();
		} else {
			jobName = form.getJob_Name() + " " + form.getSiteNumber();
		}
		return jobName;
	}

	public static String checkDate(String[] siteDate, String appendixId,
			DataSource ds) {
		String check = "true";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String siteNumber = null;
		String startDate = null;
		boolean dateCheck = true;
		if (siteDate.length == 2) {
			siteNumber = siteDate[0];
			startDate = siteDate[1];
			if (!startDate.equals("\r")) {
				dateCheck = CheckDateFormat.checkFormat(startDate.trim());
			}
		} else {
			siteNumber = siteDate[0];
			startDate = "";
		}
		if (siteDate[0] != null && !siteDate[0].equals(""))
			siteDate[0] = siteDate[0].replaceAll("\'", "\'\'");
		/*
		 * System.out.println("Site Number--"+siteDate[0]+"---");
		 * System.out.println("Start Date--"+startDate);
		 * System.out.println("msaId--"+msaId);
		 */
		// System.out.println("dateCheck---"+dateCheck);
		if (dateCheck) {
			try {
				conn = ds.getConnection();
				pstmt = conn
						.prepareStatement("select  [dbo].[func_lp_site_check](?,?)");
				pstmt.setString(1, siteNumber.trim());
				pstmt.setString(2, appendixId);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					check = rs.getString(1);
				}

			} catch (Exception e) {
				logger.error("checkDate(String[], String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("checkDate(String[], String, DataSource) - Error occured com.mind.dao.PRM.BulkJobCreationDao.checkdate() "
							+ e);
				}
			} finally {
				DBUtil.close(rs, pstmt);
				DBUtil.close(conn);
			}
			return check;
		} else {
			check = "datenotformated";
			return check;
		}
	}

	public static int saveSiteResult(BulkJobCreationBean bulkjobform, int p,
			DataSource ds, String activityid, String loginuserid) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList sitedetaillist = new ArrayList();
		int val = -1;
		int id = 0;
		int idScheduler = 0;
		String endDate = null;
		int siteID = 0;
		if (bulkjobform.getNewOwner() == null
				|| bulkjobform.getNewOwner().equals("")
				|| bulkjobform.getNewOwner().equals("0")) {
			id = 0;
		} else {
			id = Integer.parseInt(bulkjobform.getNewOwner());
		}
		if (bulkjobform.getNewScheduler() == null
				|| bulkjobform.getNewScheduler().equals("")
				|| bulkjobform.getNewScheduler().equals("0")) {
			idScheduler = 0;
		} else {
			idScheduler = Integer.parseInt(bulkjobform.getNewScheduler());
		}
		if (bulkjobform.getSelectedparam() == null
				|| bulkjobform.getSelectedparam().length == 0) {
			siteID = 0;
		} else {
			siteID = Integer.parseInt(bulkjobform.getSelectedparam(p));
		}
		if (bulkjobform.getEndDate() == null
				|| bulkjobform.getEndDate().equals("0")) {
			endDate = "";
		} else {
			endDate = bulkjobform.getEndDate();
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lx_bulkjob_manage_01(?,?,?,?,?,?,?,?)}");
			// cstmt =
			// conn.prepareCall("{?=call lx_bulkjob_manage_01_newforlargejob(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(bulkjobform.getAppendix_Id()));
			cstmt.setString(3, bulkjobform.getJob_Name().trim());
			cstmt.setInt(4, siteID);
			cstmt.setString(5, activityid);
			cstmt.setString(6, bulkjobform.getSite_name());// For radio button
			cstmt.setString(7, bulkjobform.getPrifix());
			cstmt.setInt(8, Integer.parseInt(loginuserid));
			cstmt.setString(9, "A");
			cstmt.setString(10, bulkjobform.getStartDate().trim());
			cstmt.setString(11, endDate);
			cstmt.setInt(12, id);
			cstmt.setInt(13, idScheduler);
			cstmt.setString(14, bulkjobform.getSiteNumber().trim());

			if (logger.isDebugEnabled()) {
				logger.debug("saveSiteResult(BulkJobCreationForm, int, DataSource, String, String) -  "
						+ Integer.parseInt(bulkjobform.getAppendix_Id())
						+ "  , '"
						+ bulkjobform.getJob_Name()
						+ "' , "
						+ siteID
						+ " , '"
						+ activityid
						+ "' , '"
						+ bulkjobform.getSite_name()
						+ "' , '"
						+ bulkjobform.getPrifix()
						+ "' , "
						+ Integer.parseInt(loginuserid)
						+ " , 'A'    , '"
						+ bulkjobform.getStartDate().trim()
						+ "' , '"
						+ endDate
						+ "' , "
						+ id
						+ ", "
						+ idScheduler
						+ ","
						+ bulkjobform.getSiteNumber().trim());
			}

			cstmt.execute();
			val = cstmt.getInt(1);

		}

		catch (Exception e) {
			logger.error(
					"saveSiteResult(BulkJobCreationForm, int, DataSource, String, String)",
					e);

			logger.error(
					"saveSiteResult(BulkJobCreationForm, int, DataSource, String, String)",
					e);
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"saveSiteResult(BulkJobCreationForm, int, DataSource, String, String) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"saveSiteResult(BulkJobCreationForm, int, DataSource, String, String) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"saveSiteResult(BulkJobCreationForm, int, DataSource, String, String) - exception ignored",
						e);
			}

		}
		return val;
	}

}
