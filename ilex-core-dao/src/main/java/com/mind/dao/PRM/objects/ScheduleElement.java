package com.mind.dao.PRM.objects;

public class ScheduleElement {

    private final String jobId;
    private final int dispatchId;
    private String actualStart = "N/A";
    private String actualEnd = "N/A";
    private String plannedStart = "N/A";
    private String plannedEnd = "N/A";

    public ScheduleElement(String jobId, int dispatchId, String actualStart, String actualEnd, String plannedStart, String plannedEnd) {
        this.jobId = jobId;
        this.dispatchId = dispatchId;

        if (actualStart != null && !actualStart.equals("null")) {
            this.actualStart = actualStart;
        }

        if (actualEnd != null && !actualEnd.equals("null")) {
            this.actualEnd = actualEnd;
        }

        if (plannedStart != null && !plannedStart.equals("null")) {
            this.plannedStart = plannedStart;
        }

        if (plannedEnd != null && !plannedEnd.equals("null")) {
            this.plannedEnd = plannedEnd;
        }
    }

    public String getJobId() {
        return jobId;
    }

    public int getDispatchId() {
        return dispatchId;
    }

    public String getActualStart() {
        return actualStart;
    }

    public String getActualEnd() {
        return actualEnd;
    }

    public String getPlannedStart() {
        return plannedStart;
    }

    public String getPlannedEnd() {
        return plannedEnd;
    }
}
