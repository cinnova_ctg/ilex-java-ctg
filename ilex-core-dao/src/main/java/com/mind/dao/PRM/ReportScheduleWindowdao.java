package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.ReportScheduleWindowBean;

public class ReportScheduleWindowdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ReportScheduleWindowdao.class);

//	for adding job status combo in redline/csv report
	public static ArrayList getStatuslist(DataSource ds)
	{
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add( new com.mind.common.LabelValue( "---Select---" , "0") );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery( "select cc_status_code,cc_status_description  from cc_status_master where cc_status_code in('C','F','H','I') ");
			while( rs.next() )
			{
				list.add ( new com.mind.common.LabelValue( rs.getString( "cc_status_description" ), rs.getString( "cc_status_code" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getStatuslist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStatuslist(DataSource) - Exception caught" + e);
			}
		}
		finally
		{
			
			try
			{
				if ( rs!= null ) 
				{
					rs.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getStatuslist(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( stmt!= null ) 
				{
					stmt.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getStatuslist(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getStatuslist(DataSource) - exception ignored", sql);
}
			
		}
		return list;
	}
	
	public static ArrayList getOrganizationPOCList(String lo_ot_id, DataSource ds ) 
	{
		ArrayList poccombo = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		poccombo.add( new com.mind.common.LabelValue( "---Select---" , "0") );
		String sql = "";
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			
			sql = "select * from dbo.func_search_list('"+lo_ot_id+"','C','%', '%%', 'requestor_search')";
			rs = stmt.executeQuery( sql );
			while( rs.next() )
			{
				poccombo.add ( new com.mind.common.LabelValue(rs.getString(2) , rs.getString(2)));
			}
		}
		catch( Exception e )
		{
			logger.error("getOrganizationPOCList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOrganizationPOCList(String, DataSource) - Exception caught in rerieving get Organization POC" + e);
			}
		}
		finally
		{
			
			try
			{
				if ( rs!= null ) 
				{
					rs.close();
				}
			}
			catch( SQLException s )
			{
				logger.warn("getOrganizationPOCList(String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( stmt!= null ) 
				{
					stmt.close();
				}
			}
			catch( SQLException s )
			{
				logger.warn("getOrganizationPOCList(String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( SQLException s )
			{
				logger.warn("getOrganizationPOCList(String, DataSource) - exception ignored", s);
}
			
		}
		return poccombo;
	}
	
	
	
//	for adding criticality description combo in redline/csv report
	public static ArrayList getCriticalityDescList(DataSource ds)
	{
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add( new com.mind.common.LabelValue( "---Select---" , "0") );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery( "select distinct(clr_cat_short_name) from clr_category");
			while( rs.next() )
			{
				list.add ( new com.mind.common.LabelValue( rs.getString( "clr_cat_short_name" ), rs.getString( "clr_cat_short_name" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getCriticalityDescList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCriticalityDescList(DataSource) - Exception caught" + e);
			}
		}
		finally
		{
			
			try
			{
				if ( rs!= null ) 
				{
					rs.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getCriticalityDescList(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( stmt!= null ) 
				{
					stmt.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getCriticalityDescList(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getCriticalityDescList(DataSource) - exception ignored", sql);
}
			
		}
		return list;
	}
	
	
	
	public static ArrayList getYesNoList(DataSource ds)
	{
		ArrayList list = new ArrayList();
		try
		{
			list.add( new com.mind.common.LabelValue( "---Select---" , "0") );
			list.add ( new com.mind.common.LabelValue( "Yes", "Y") );
			list.add ( new com.mind.common.LabelValue( "No", "N") );
		}
		catch( Exception e )
		{
			logger.error("getYesNoList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getYesNoList(DataSource) - Exception caught" + e);
			}
		}
		return list;
	}
	
	public static ArrayList getMsalist(DataSource ds)
	{
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add( new com.mind.common.LabelValue( "---Select---" , "0") );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery( "select lp_mm_id , lo_ot_name  from lp_msa_list_01 order by  lo_ot_name");
			while( rs.next() )
			{
				list.add ( new com.mind.common.LabelValue( rs.getString( "lo_ot_name" ), rs.getString( "lp_mm_id" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getMsalist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsalist(DataSource) - Exception caught in getMsalist" + e);
			}
		}
		finally
		{
			
			try
			{
				if ( rs!= null ) 
				{
					rs.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getMsalist(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( stmt!= null ) 
				{
					stmt.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getMsalist(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getMsalist(DataSource) - exception ignored", sql);
}
			
		}
		return list;
	}
	
	public static ArrayList getAppendixlist(String msaid, DataSource ds)
	{
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add( new com.mind.common.LabelValue( "---Select---" , "0") );
		
		if(msaid != null) {}
		else msaid = "0";	
			
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery( "select lx_pr_id,lx_pr_title  from dbo.lp_appendix_list("+msaid+")");
			while( rs.next() )
			{
				list.add ( new com.mind.common.LabelValue( rs.getString( "lx_pr_title" ), rs.getString( "lx_pr_id" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getAppendixlist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixlist(String, DataSource) - Exception caught in getAppendixlist" + e);
			}
		}
		finally
		{
			
			try
			{
				if ( rs!= null ) 
				{
					rs.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getAppendixlist(String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( stmt!= null ) 
				{
					stmt.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getAppendixlist(String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				if ( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getAppendixlist(String, DataSource) - exception ignored", sql);
}
			
		}
		return list;
	}
	
	
	public static ArrayList getAllOwnername( DataSource ds )
	{
		
	    Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		
			
		try{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			
			
			String sql="select * from dbo.func_lp_all_owners()";	
			
			rs = stmt.executeQuery(sql);
			
			valuelist.add( new com.mind.common.LabelValue( "---Select---" , "0") );
			
			while(rs.next())
			{
				
				
				valuelist.add(new com.mind.common.LabelValue(rs.getString("ownername"), rs.getString("lo_pc_id")));
					 
			}
			
		}
		catch( Exception e ){
			logger.error("getAllOwnername(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAllOwnername(DataSource) - Error occured during getting owner name" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getAllOwnername(DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getAllOwnername(DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getAllOwnername(DataSource) - exception ignored", e);
}
			
		}
		return valuelist;
	}
	
	
	
	public static ArrayList getAccountList( String msaId , String startDate , String endDate , DataSource ds )
	{
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList accountlist = new ArrayList();
		DecimalFormat dfcur	= new DecimalFormat( "###0.00" );
		ReportScheduleWindowBean accountsummary = null;
		try
		{
			conn = ds.getConnection();
			cstmt = conn.prepareCall( "{?=call lo_account_wise_summary( ? , ? , ? )}" );
			
			cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			cstmt.setString( 2 , msaId );
			cstmt.setString( 3 , startDate );
			cstmt.setString( 4 , endDate );
			
			rs  = cstmt.executeQuery();

			while( rs.next() )
			{
				accountsummary = new ReportScheduleWindowBean();
				
				accountsummary.setMsaid( rs.getString( 1 ) );
				accountsummary.setMsaname( rs.getString( 2 ) );
				accountsummary.setAppendixname( rs.getString( 4 ) );
				
				accountsummary.setInworkestcost( rs.getString( 5 ) );
				accountsummary.setInworkrevenue( dfcur.format( rs.getFloat ( 6 ) )+"" );
				accountsummary.setInworkactcost( rs.getString( 7 ) );
				accountsummary.setInworkvgp( dfcur.format( rs.getFloat ( 8 ) )+"" );
				
				accountsummary.setCompleteestcost( rs.getString( 9 ) );
				accountsummary.setCompleterevenue( dfcur.format( rs.getFloat ( 10 ) )+"" );
				accountsummary.setCompleteactcost( rs.getString( 11 ) );
				accountsummary.setCompletevgp( dfcur.format( rs.getFloat ( 12 ) )+"" );
				
				accountsummary.setClosedestcost( rs.getString( 13 ) );
				accountsummary.setClosedrevenue( dfcur.format( rs.getFloat ( 14 ) )+"" );
				accountsummary.setClosedactcost( rs.getString( 15 ) );
				accountsummary.setClosedvgp( dfcur.format( rs.getFloat ( 16 ) )+"" );
				
				accountsummary.setIncompcloestcost( rs.getString( 17 ) );
				accountsummary.setIncompclorevenue( dfcur.format( rs.getFloat ( 18 ) )+"" );
				accountsummary.setIncompcloactcost( rs.getString( 19 ) );
				accountsummary.setIncompclovgp( dfcur.format( rs.getFloat ( 20 ) )+"" );
				
				
				accountlist.add( accountsummary );
			}
		}
		catch( Exception e )
		{
			logger.error("getAccountList(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAccountList(String, String, String, DataSource) - Exception caught in getting Account summary" + e);
			}
		}
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getAccountList(String, String, String, DataSource) - exception ignored", sql);	
			}
			try
			{
				if ( cstmt != null ) 
				{
					cstmt.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getAccountList(String, String, String, DataSource) - exception ignored", sql);	
			}
			try
			{
				if ( conn != null ) 
				{
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getAccountList(String, String, String, DataSource) - exception ignored", sql);
				
			}
			
		}
		return accountlist;
	}
	
	public static ArrayList getOwnerList( String ownerId , String msaId , String startDate , String endDate , DataSource ds )
	{
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList ownerlist = new ArrayList();
		DecimalFormat dfcur	= new DecimalFormat( "###0.00" );
		ReportScheduleWindowBean accountsummary = null;
		try
		{
			conn = ds.getConnection();
			cstmt = conn.prepareCall( "{?=call lo_owner_wise_summary( ? , ? , ? , ? )}" );
			
			cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			cstmt.setString( 2 , ownerId );
			cstmt.setString( 3 , msaId );
			cstmt.setString( 4 , startDate );
			cstmt.setString( 5 , endDate );
			
			rs  = cstmt.executeQuery();

			while( rs.next() )
			{
				accountsummary = new ReportScheduleWindowBean();
				
				accountsummary.setOwnerid( rs.getString( 1 ) );
				accountsummary.setOwnername( rs.getString( 2 ) );
				accountsummary.setMsaid( rs.getString( 3 ) );
				accountsummary.setMsaname( rs.getString( 4 ) );
				accountsummary.setAppendixname( rs.getString( 6 ) );
				
				accountsummary.setInworkestcost( rs.getString( 7 ) );
				accountsummary.setInworkrevenue( dfcur.format( rs.getFloat ( 8 ) )+"" );
				accountsummary.setInworkactcost( rs.getString( 9 ) );
				accountsummary.setInworkvgp( dfcur.format( rs.getFloat ( 10 ) )+"" );
				
				accountsummary.setCompleteestcost( rs.getString( 11 ) );
				accountsummary.setCompleterevenue( dfcur.format( rs.getFloat ( 12 ) )+"" );
				accountsummary.setCompleteactcost( rs.getString( 13 ) );
				accountsummary.setCompletevgp( dfcur.format( rs.getFloat ( 14 ) )+"" );
				
				accountsummary.setClosedestcost( rs.getString( 15 ) );
				accountsummary.setClosedrevenue( dfcur.format( rs.getFloat ( 16 ) )+"" );
				accountsummary.setClosedactcost( rs.getString( 17 ) );
				accountsummary.setClosedvgp( dfcur.format( rs.getFloat ( 18 ) )+"" );
				
				accountsummary.setIncompcloestcost( rs.getString( 19 ) );
				accountsummary.setIncompclorevenue( dfcur.format( rs.getFloat (  20 ) )+"" );
				accountsummary.setIncompcloactcost( rs.getString( 21 ) );
				accountsummary.setIncompclovgp( dfcur.format( rs.getFloat (  22 ) )+"" );
				
				
				ownerlist.add( accountsummary );
			}
		}
		catch( Exception e )
		{
			logger.error("getOwnerList(String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOwnerList(String, String, String, String, DataSource) - Exception caught in getting Owner summary" + e);
			}
		}
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getOwnerList(String, String, String, String, DataSource) - exception ignored", sql);	
			}
			try
			{
				if ( cstmt != null ) 
				{
					cstmt.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getOwnerList(String, String, String, String, DataSource) - exception ignored", sql);	
			}
			try
			{
				if ( conn != null ) 
				{
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.warn("getOwnerList(String, String, String, String, DataSource) - exception ignored", sql);
				
			}
			
		}
		return ownerlist;
	}
}
