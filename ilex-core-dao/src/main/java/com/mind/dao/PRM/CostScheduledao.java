package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.CostScheduleBean;
import com.mind.bean.prm.CostScheduleBean;


public class CostScheduledao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CostScheduledao.class);

	public  static ArrayList getAllcostschedulelist(String aid, DataSource ds)
	{
		
		Connection conn=null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		ArrayList valuelist=new ArrayList();
		
		
		try{
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call lp_project_schedule_details(?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2,aid);
						
			rs = cstmt.executeQuery();
			int i=0;
			
			while(rs.next())
			{
			
				 CostScheduleBean csForm = new CostScheduleBean();
				 
				 csForm.setAppendixname(rs.getString("project_title"));	
				 
				 csForm.setJobname(rs.getString("job_title"));
				
				 csForm.setActivityname(rs.getString("act_title"));
				 if(rs.getString("psd")==null)
				 {
					 csForm.setPlannedstartdate("-");
				 }
				 else
				 {
					 csForm.setPlannedstartdate(rs.getString("psd"));
				 }
				 
				 if(rs.getString("ped")==null)
				 {
					 csForm.setPlannedenddate("-");
				 }
				 else
				 {
					 csForm.setPlannedenddate(rs.getString("ped"));
				 }
				 
				 if(rs.getString("asd")==null)
				 {
					 csForm.setActualstartdate("-");
				 }
				 else
				 {
					 csForm.setActualstartdate(rs.getString("asd"));
				 }
				 
				 if(rs.getString("aed")==null)
				 {
					 csForm.setActualenddate("-"); 
				 }
				 else
				 {
					 csForm.setActualenddate(rs.getString("aed")); 
				 }
				 
				 csForm.setStatus(rs.getString("status"));
				 csForm.setDeviation(rs.getString("deviation"));
				
				 
				
				 valuelist.add(i,csForm);
				 i++;		 
			}
			
		}
		catch(Exception e){
			logger.error("getAllcostschedulelist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAllcostschedulelist(String, DataSource) - Error occured during getting getAllcostschedulelist  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getAllcostschedulelist(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getAllcostschedulelist(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getAllcostschedulelist(String, DataSource) - exception ignored", e);
}
			
		}
		return valuelist;	
	}
	
	

}
