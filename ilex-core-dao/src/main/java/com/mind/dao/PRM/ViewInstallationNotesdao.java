package com.mind.dao.PRM;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.ViewInstallationNotesBean;
import com.mind.common.Util;

public class ViewInstallationNotesdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ViewInstallationNotesdao.class);

	public static ArrayList getNotesdetaillist(String jobid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select  * from dbo.func_lm_install_notes_list("
					+ jobid + ")";
			rs = stmt.executeQuery(sql);
			int i = 0;

			while (rs.next()) {

				ViewInstallationNotesBean vinForm = new ViewInstallationNotesBean();

				vinForm.setInstallnotes(Util.replaceToBr(rs
						.getString("lm_in_notes")));

				vinForm.setCreatedby(rs.getString("lm_in_cuser"));
				vinForm.setCreatedatetime(rs.getString("lm_cdate"));
				vinForm.setTypeId(rs.getString("lm_in_type"));

				valuelist.add(i, vinForm);
				i++;
			}

		} catch (Exception e) {
			logger.error("getNotesdetaillist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNotesdetaillist(String, DataSource) - Error occured during getting getNotesdetaillist  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getNotesdetaillist(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getNotesdetaillist(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getNotesdetaillist(String, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static String getAppendixname(String appendixid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String appendixname = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lx_pr_title from lx_appendix_main where lx_pr_id="
					+ appendixid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				appendixname = rs.getString("lx_pr_title");
			}

		} catch (Exception e) {
			logger.error("getAppendixname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixname(String, DataSource) - Error occured during getting Appendix name  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

		}
		return appendixname;
	}

	public static String getJobname(String jobid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String jobname = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_js_title from lm_job where lm_js_id="
					+ jobid + " ";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				jobname = rs.getString("lm_js_title");
			}

		} catch (Exception e) {
			logger.error("getJobname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobname(String, DataSource) - Error occured during getting Job name  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobname(String, DataSource) - exception ignored", e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobname(String, DataSource) - exception ignored", e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobname(String, DataSource) - exception ignored", e);
			}

		}
		return jobname;
	}

}
