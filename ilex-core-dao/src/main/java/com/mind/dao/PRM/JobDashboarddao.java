package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.ActivityTabularForm;
import com.mind.bean.prm.JobDashboardBean;
import com.mind.bean.prm.ResourceTabularForm;
import com.mind.bean.prm.TicketHistoryBean;
import com.mind.common.Util;
import com.mind.fw.core.dao.util.DBUtil;

public class JobDashboarddao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(JobDashboarddao.class);

	public static int addNewSite(int siteListId, int jobid, String user,
			DataSource ds, int jobSiteId) {
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;
		Util util = new Util();

		try {

			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_newsite_manage_01( ?, ?, ?, ?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, siteListId);
			cstmt.setInt(3, jobid);
			cstmt.setString(4, user);
			cstmt.setInt(5, jobSiteId);
			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error("addNewSite(int, int, String, DataSource, int)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addNewSite(int, int, String, DataSource, int) - Exception caught in adding new site "
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error("addNewSite(int, int, String, DataSource, int)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addNewSite(int, int, String, DataSource, int) - Exception caught in adding new site"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("addNewSite(int, int, String, DataSource, int)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addNewSite(int, int, String, DataSource, int) - Exception caught in adding new site"
							+ sql);
				}
			}

		}
		return val;
	}

	public static ArrayList getActivitytabular(String jobid, DataSource ds,
			String sortqueryclause) {
		ArrayList activitylist = new ArrayList();

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int i = 0;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");
		DecimalFormat dfcurfour = new DecimalFormat("###0.0000");
		if (sortqueryclause.equals("")) {
			// sortqueryclause = "order by lm_at_title";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lp_jobdashboard_activitysummary_tab( '"
					+ jobid + "' )" + sortqueryclause;
			// System.out.println("::::::::::::::::"+sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				ActivityTabularForm activityTabular = new ActivityTabularForm();
				activityTabular.setActivity_id(rs.getString(1));
				activityTabular.setActivity_name(rs.getString(2));
				activityTabular.setQuantity(dfcurfour.format(rs.getFloat(4))
						+ "");
				activityTabular.setType(rs.getString(3));
				activityTabular.setScope(rs.getString(22));
				activityTabular.setEstimatedunitcost(dfcur.format(rs
						.getFloat("unit_cost")) + "");
				activityTabular.setEstimatedtotalcost(dfcur.format(rs
						.getFloat("lx_ce_total_cost")) + "");
				activityTabular.setExtendedunitprice(dfcur.format(rs
						.getFloat("unit_price")) + "");
				activityTabular.setExtendedtotalprice(dfcur.format(rs
						.getFloat("lx_ce_total_price")) + "");
				activityTabular.setMargin(dfcurmargin.format(rs
						.getFloat("lx_ce_margin")) + "");
				activityTabular.setStatus(rs.getString("lm_at_prj_status"));
				activityTabular.setActivity_lx_ce_type(rs
						.getString("lx_ce_type"));
				activityTabular.setAtUpliftCost(dfcur.format(rs
						.getFloat("uplift_unit_cost")) + "");
				activityTabular.setActivity_tempid(rs.getString(1)); // Use for
																		// appliy
																		// uplift
				activityTabular.setActivity_oos_flag(rs
						.getString("lm_at_oos_flag")); // Used for OOS activity
				activitylist.add(i, activityTabular);
				i++;
			}

		} catch (Exception e) {
			logger.error("getActivitytabular(String, DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivitytabular(String, DataSource, String) - Error occured during getting job dashboard activity list");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getActivitytabular(String, DataSource, String) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getActivitytabular(String, DataSource, String) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getActivitytabular(String, DataSource, String) - exception ignored",
						e);
			}

		}
		return activitylist;
	}

	public static ArrayList getResourcetabular(String activityid, DataSource ds) {
		ArrayList resourcelist = new ArrayList();

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");
		DecimalFormat dfcurfour = new DecimalFormat("###0.0000");

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lp_jobdashboard_resourcesummary_tab( '"
					+ activityid + "' )";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				ResourceTabularForm ResourceTabular = new ResourceTabularForm();
				ResourceTabular.setResource_id(rs.getString(1));
				ResourceTabular.setResource_name(rs.getString(4));
				ResourceTabular
						.setResource_qty(dfcurfour.format(rs.getFloat(9)) + "");
				ResourceTabular.setResource_cnspartnumber(rs.getString(8));
				ResourceTabular.setResource_type(rs.getString(5));
				ResourceTabular.setResource_sourcename(rs.getString(6));
				ResourceTabular.setResource_sourcepartno(rs.getString(7));

				ResourceTabular.setResource_estimatedunitcost(dfcur.format(rs
						.getFloat("lx_ce_unit_cost")) + "");
				ResourceTabular.setResource_estimatedtotalcost(dfcur.format(rs
						.getFloat("lx_ce_total_cost")) + "");
				ResourceTabular.setResource_extendedunitprice(dfcur.format(rs
						.getFloat("lx_ce_unit_price")) + "");
				ResourceTabular.setResource_extendedtotalprice(dfcur.format(rs
						.getFloat("lx_ce_total_price")) + "");
				ResourceTabular.setResource_margin(dfcurmargin.format(rs
						.getFloat("lx_ce_margin")) + "");
				ResourceTabular.setResource_lx_ce_type(rs
						.getString("lx_ce_type"));

				ResourceTabular.setResource_partner_name(rs
						.getString("partner_name"));

				ResourceTabular.setResource_drop_shipped(rs
						.getString("lm_pwo_drop_shipped"));

				resourcelist.add(ResourceTabular);

			}

		} catch (Exception e) {
			logger.error("getResourcetabular(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourcetabular(String, DataSource) - Error occured during getting job dashboard resource list"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getResourcetabular(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getResourcetabular(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getResourcetabular(String, DataSource) - exception ignored",
						e);
			}

		}

		return resourcelist;
	}

	public static JobDashboardBean getJobdetailedfinancialinfo(String jobid,
			JobDashboardBean jobdashboard, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lp_prj_jobdashboard_tab( '%' , '"
					+ jobid + "' , '%'  )";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				jobdashboard.setJob_name(rs.getString(3));
				jobdashboard.setJob_status(rs.getString(4));
				jobdashboard.setSite_id(rs.getString("lm_js_si_id"));
				jobdashboard.setJobtype(rs.getString("lm_js_type"));
				jobdashboard.setJob_sitename(rs.getString(6));
				jobdashboard.setJob_siteaddress(rs.getString("lm_si_address"));
				jobdashboard.setJob_sitestate(rs.getString("lm_si_state"));
				jobdashboard.setJob_sitezipcode(rs.getString("lm_si_zip_code"));
				jobdashboard.setJob_sitepoc(rs.getString("lm_site_poc"));
				jobdashboard.setJob_sitecity(rs.getString("lm_si_city"));
				jobdashboard.setJob_sitephoneno(rs.getString("lm_si_phone_no"));
				jobdashboard.setJob_priemailid(rs
						.getString("lm_si_pri_email_id"));
				jobdashboard.setJob_sitesecpoc(rs.getString("lm_site_sec_poc"));
				jobdashboard.setJob_sitesecphone(rs
						.getString("lm_si_sec_phone_no"));
				jobdashboard.setJob_sitesecemailid(rs
						.getString("lm_si_sec_email_id"));
				jobdashboard.setJob_extendedprice(dfcur.format(rs.getFloat(15))
						+ "");
				jobdashboard.setJob_estimatedcost(dfcur.format(rs.getFloat(14))
						+ "");
				jobdashboard.setJob_cnslaborcost(dfcur.format(rs.getFloat(10))
						+ "");
				jobdashboard
						.setJob_fieldlaborcost(dfcur.format(rs.getFloat(11))
								+ "");
				jobdashboard.setJob_materialcost(dfcur.format(rs.getFloat(9))
						+ "");
				jobdashboard.setJob_freightcost(dfcur.format(rs.getFloat(12))
						+ "");
				jobdashboard.setJob_travelcost(dfcur.format(rs.getFloat(13))
						+ "");
				jobdashboard.setJob_proformamarginvgp(dfcurmargin.format(rs
						.getFloat(16)) + "");
				jobdashboard.setJob_actualvgp(dfcurmargin.format(rs
						.getFloat("lx_ce_actual_vgp")) + "");
				jobdashboard.setJob_actualvgp_new(dfcurmargin.format(rs
						.getFloat("lx_ce_actual_vgp_new")) + "");

				jobdashboard.setSitescheduleplanned_startdate(rs
						.getString("lm_js_planned_start_date"));
				jobdashboard.setSitescheduleplanned_startdatetime(rs
						.getString("lx_act_start_time"));
				jobdashboard.setSitescheduleplanned_enddate(rs
						.getString("lm_js_planned_end_date"));
				jobdashboard.setSitescheduleplanned_enddatetime(rs
						.getString("lx_act_end_time"));
				jobdashboard.setSitescheduleactual_startdate(rs
						.getString("lx_se_start"));
				jobdashboard.setSitescheduleactual_startdatetime(rs
						.getString("lx_se_start_time"));
				jobdashboard.setSitescheduleactual_enddate(rs
						.getString("lx_se_end"));
				jobdashboard.setSitescheduleactual_enddatetime(rs
						.getString("lx_se_end_time"));

				jobdashboard.setUpdatedby(rs.getString("job_changed_by"));
				jobdashboard.setUpdatedtime(rs.getString("job_change_time"));
				jobdashboard.setUpdateddate(rs.getString("job_change_date"));
				jobdashboard.setJobcreatedby(rs.getString("job_created_by"));
				jobdashboard.setCreatedate(rs.getString("job_create_date"));
				jobdashboard.setCreatetime(rs.getString("job_create_time"));
				jobdashboard.setJobtype(rs.getString("lm_js_type"));
				jobdashboard.setCustref(rs
						.getString("lm_js_customer_reference"));
				jobdashboard.setJob_sitenumber(rs.getString("lm_si_number"));
				jobdashboard.setJob_invoiceno(rs.getString("lm_js_invoice_no"));
				jobdashboard.setViewType(rs.getString("lm_view_type"));
				jobdashboard.setJobScheduler(rs.getString("lm_js_scheduler"));
				jobdashboard.setJobRseCount(rs.getString("lm_js_rse_count"));
				jobdashboard.setJobTestIndicator(rs
						.getString("lm_js_test_indicator"));
			}

		} catch (Exception e) {
			logger.error(
					"getJobdetailedfinancialinfo(String, JobDashboardForm, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobdetailedfinancialinfo(String, JobDashboardForm, DataSource) - Error occured during getting job dashboard div data"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobdetailedfinancialinfo(String, JobDashboardForm, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobdetailedfinancialinfo(String, JobDashboardForm, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobdetailedfinancialinfo(String, JobDashboardForm, DataSource) - exception ignored",
						e);
			}

		}
		return jobdashboard;
	}

	public static String getJobViewType(String jobId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String viewType = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_view_type from dbo.func_lp_prj_jobdashboard_tab( '%' , '"
					+ jobId + "' , '%'  )";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				viewType = rs.getString("lm_view_type");
			}

		} catch (Exception e) {
			logger.error("getJobViewType(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobViewType(String, DataSource) - Error occured during getting job View Type "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobViewType(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobViewType(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobViewType(String, DataSource) - exception ignored",
						e);
			}

		}
		return viewType;
	}

	public static String getActivityprojectstatus(String activityid,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String status = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_at_prj_status = dbo.func_status_description(isnull(lm_at_prj_status,'I')) from lm_activity where lm_at_id="
					+ activityid;

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				status = rs.getString("lm_at_prj_status");
			}

		} catch (Exception e) {
			logger.error("getActivityprojectstatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityprojectstatus(String, DataSource) - Error occured during getting Activityprojectstatus"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getActivityprojectstatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getActivityprojectstatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getActivityprojectstatus(String, DataSource) - exception ignored",
						e);
			}

		}

		return status;

	}

	public static JobDashboardBean getNetMedXticketdetails(String jobid,
			JobDashboardBean jobdashboard, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_netmedxticket_01( '" + jobid
					+ "' )";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				jobdashboard.setJob_name(rs.getString(3));
				jobdashboard.setJob_status(rs.getString(4));
				jobdashboard.setJobtype(rs.getString("lm_js_type"));

				jobdashboard.setSite_id(rs.getString("lm_js_si_id"));
				jobdashboard.setJob_sitename(rs.getString(6));
				jobdashboard.setJob_sitenumber(rs.getString("lm_si_number"));
				jobdashboard.setJob_siteaddress(rs.getString("lm_si_address"));
				jobdashboard.setJob_sitestate(rs.getString("lm_si_state"));
				jobdashboard.setJob_sitezipcode(rs.getString("lm_si_zip_code"));
				jobdashboard.setJob_sitepoc(rs.getString("lm_site_poc"));
				jobdashboard.setJob_sitecity(rs.getString("lm_si_city"));

				jobdashboard.setJob_sitephoneno(rs.getString("lm_si_phone_no"));
				jobdashboard.setJob_priemailid(rs
						.getString("lm_si_pri_email_id"));
				jobdashboard.setJob_sitesecpoc(rs.getString("lm_site_sec_poc"));
				jobdashboard.setJob_sitesecphone(rs
						.getString("lm_si_sec_phone_no"));
				jobdashboard.setJob_sitesecemailid(rs
						.getString("lm_si_sec_email_id"));

				jobdashboard.setJob_extendedprice(dfcur.format(rs.getFloat(15))
						+ "");
				jobdashboard.setJob_estimatedcost(dfcur.format(rs.getFloat(14))
						+ "");
				jobdashboard.setJob_cnslaborcost(dfcur.format(rs.getFloat(10))
						+ "");
				jobdashboard
						.setJob_fieldlaborcost(dfcur.format(rs.getFloat(11))
								+ "");
				jobdashboard.setJob_materialcost(dfcur.format(rs.getFloat(9))
						+ "");
				jobdashboard.setJob_freightcost(dfcur.format(rs.getFloat(12))
						+ "");
				jobdashboard.setJob_travelcost(dfcur.format(rs.getFloat(13))
						+ "");
				jobdashboard.setJob_proformamarginvgp(dfcurmargin.format(rs
						.getFloat(16)) + "");

				jobdashboard.setJob_netactualprice(dfcur.format(rs
						.getFloat("lx_ce_total_price")) + "");
				jobdashboard.setJob_netauthorizedtotalcost(dfcur.format(rs
						.getFloat("lx_ce_actual_cost")) + "");
				jobdashboard.setJob_netactualvgp(dfcurmargin.format(rs
						.getFloat("lx_ce_actual_vgp")) + "");
				jobdashboard.setJob_invoiceno(rs.getString("lm_js_invoice_no"));

				jobdashboard.setSitescheduleplanned_startdate(rs
						.getString("lm_js_planned_start_date"));
				jobdashboard.setSitescheduleplanned_startdatetime(rs
						.getString("lx_act_start_time"));

				jobdashboard.setSitescheduleplanned_enddate(rs
						.getString("lm_js_planned_end_date"));
				jobdashboard.setSitescheduleplanned_enddatetime(rs
						.getString("lx_act_end_time"));

				jobdashboard.setSitescheduleactual_startdate(rs
						.getString("lx_se_start"));
				jobdashboard.setSitescheduleactual_startdatetime(rs
						.getString("lx_se_start_time"));

				jobdashboard.setSitescheduleactual_enddate(rs
						.getString("lx_se_end"));
				jobdashboard.setSitescheduleactual_enddatetime(rs
						.getString("lx_se_end_time"));

				jobdashboard.setUpdatedby(rs.getString("job_changed_by"));
				jobdashboard.setUpdatedtime(rs.getString("job_change_time"));
				jobdashboard.setUpdateddate(rs.getString("job_change_date"));

				jobdashboard.setViewType(rs.getString("lm_view_type"));

				jobdashboard.setJob_netrequestor(rs
						.getString("lm_tc_requestor_name"));
				jobdashboard.setJob_netspecialcondition(rs
						.getString("lm_si_notes"));

				jobdashboard.setJob_netproblemdesc(rs
						.getString("lm_tc_problem_desc"));
				jobdashboard.setJob_netspecialinstruct(rs
						.getString("lm_tc_spec_instruction"));
				jobdashboard.setJob_netcategory(rs.getString("lm_tc_category"));
				jobdashboard.setJob_netpreferred_arrival_start_date(rs
						.getString("lm_tc_preferred_arrival"));
				jobdashboard.setJob_netpreferred_arrival_start_time(rs
						.getString("lm_tc_preferred_arrival_time"));

				jobdashboard.setJob_netpreferred_arrival_window_start_date(rs
						.getString("lm_tc_preferred_window_start"));
				jobdashboard.setJob_netpreferred_arrival_window_start_time(rs
						.getString("lm_tc_preferred_window_start_time"));
				jobdashboard.setJob_vpreferred_arrival_window_end_date(rs
						.getString("lm_tc_preferred_window_end"));
				jobdashboard.setJob_netpreferred_arrival_window_end_time(rs
						.getString("lm_tc_preferred_window_end_time"));

				jobdashboard.setJob_netcust_reference(rs
						.getString("lm_tc_cust_reference"));
				jobdashboard.setJob_netticketid(rs.getString("lm_tc_id"));

				jobdashboard.setTime_onsite(rs.getString("time_onsite"));
				jobdashboard.setTime_toarrive(rs.getString("time_toarrive"));

				jobdashboard.setLo_ot_name(rs.getString("lo_ot_name"));
				jobdashboard.setJobcreatedby(rs.getString("job_created_by"));
				jobdashboard.setCreatedate(rs.getString("job_create_date"));
				jobdashboard.setCreatetime(rs.getString("job_create_time"));

				// Start :Added By Amit For Recieved Date & time
				jobdashboard.setRequest_received_date(rs
						.getString("lm_tc_requested_date"));
				jobdashboard.setRequest_received_time(rs
						.getString("lm_tc_requested_time"));

				jobdashboard.setTotaltime_onsite(rs
						.getString("totaltime_onsite"));
				jobdashboard.setSchedule_position(rs
						.getString("schedule_position"));
				jobdashboard.setSchedule_total(rs.getInt("schedule_total"));
				// End
				jobdashboard.setCustref(rs
						.getString("lm_js_customer_reference"));
				// jobdashboard.setJobRseCount( rs.getString( "lm_js_rse_count"
				// ) );
				jobdashboard.setJobTestIndicator(rs
						.getString("lm_js_test_indicator"));
			}

		} catch (Exception e) {
			logger.error(
					"getNetMedXticketdetails(String, JobDashboardForm, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNetMedXticketdetails(String, JobDashboardForm, DataSource) - Error occured during getting job dashboard netmedx div data"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getNetMedXticketdetails(String, JobDashboardForm, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getNetMedXticketdetails(String, JobDashboardForm, DataSource) - exception ignored",
						e);
			}

			try {

				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getNetMedXticketdetails(String, JobDashboardForm, DataSource) - exception ignored",
						e);
			}

		}
		return jobdashboard;

	}

	public static ArrayList getTop3installationnotes(String jobid, DataSource ds) {
		ArrayList installationcomment = new ArrayList();

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_top3_installation_notes_01( '"
					+ jobid + "' )";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				JobDashboardBean jobdashboard = new JobDashboardBean();

				jobdashboard.setJob_netinstallation_date(rs
						.getString("lm_in_cdate"));

				jobdashboard.setJob_netinstallation_time(rs
						.getString("lm_in_ctime")
						+ " - "
						+ rs.getString("lm_in_notes"));
				// jobdashboard.setJob_netinstallation_comment(rs.getString(
				// "lm_in_notes"));

				installationcomment.add(jobdashboard);
			}

		} catch (Exception e) {
			logger.error("getTop3installationnotes(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTop3installationnotes(String, DataSource) - Error occured during getting top 3 installation notes"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getTop3installationnotes(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getTop3installationnotes(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getTop3installationnotes(String, DataSource) - exception ignored",
						e);
			}

		}
		return installationcomment;
	}

	public static JobDashboardBean getJobActualCostVGP(String jobid,
			JobDashboardBean jobdashboard, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		DecimalFormat dfcur = new DecimalFormat("###0.00");

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_actual_cost,0 from lm_job_partner_cost where lm_js_id = '"
					+ jobid + "'";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				jobdashboard.setJob_actualcost(dfcur.format(rs
						.getFloat("lm_actual_cost")) + "");
			}
			if (jobdashboard.getJob_actualcost() == null
					|| jobdashboard.getJob_actualcost().equals("")) {
				jobdashboard.setJob_actualcost("0.00");
			}

			// jobdashboard.setJob_actualvgp( Float.parseFloat(
			// jobdashboard.getExtendedtotalprice() ) - Float.parseFloat(
			// jobdashboard.getJob_actualcost() )+"" );

		} catch (Exception e) {
			logger.error(
					"getJobActualCostVGP(String, JobDashboardForm, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobActualCostVGP(String, JobDashboardForm, DataSource) - Error occured during getting job actual cost VGP"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobActualCostVGP(String, JobDashboardForm, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobActualCostVGP(String, JobDashboardForm, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobActualCostVGP(String, JobDashboardForm, DataSource) - exception ignored",
						e);
			}

		}
		return jobdashboard;
	}

	public static ArrayList getTicketHistory(String jobid, DataSource ds) {
		ArrayList tickethistory = new ArrayList();

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		DecimalFormat dfcur = new DecimalFormat("###0.00");

		TicketHistoryBean tickethistoryform = new TicketHistoryBean();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_ticket_history( '" + jobid
					+ "' )";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				TicketHistoryBean ticket = new TicketHistoryBean();

				ticket.setTicketid(rs.getString("lm_tc_id"));
				ticket.setTicketnum(rs.getString("lm_tc_number"));
				ticket.setTicketrequestorname(rs
						.getString("lm_tc_requestor_name"));
				ticket.setTicketrequesteddate(rs
						.getString("lm_tc_requested_date"));
				ticket.setTicketcustreference(rs
						.getString("lm_tc_cust_reference"));
				ticket.setProblemdesc(rs.getString("lm_tc_problem_desc"));
				ticket.setCriticality(rs.getString("lm_tc_criticality"));
				ticket.setStatus(rs.getString("lm_js_prj_status"));
				ticket.setCompleted_date(rs.getString("lm_js_submitted_date"));
				ticket.setFinal_price(dfcur.format(rs
						.getFloat("lx_ce_total_price")));
				if (ticket.getFinal_price().equals("0.00"))
					ticket.setFinal_price("");

				tickethistory.add(ticket);
			}

		} catch (Exception e) {
			logger.error("getTicketHistory(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTicketHistory(String, DataSource) - Error occured during getting ticket history"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getTicketHistory(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getTicketHistory(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getTicketHistory(String, DataSource) - exception ignored",
						e);
			}

		}
		return tickethistory;
	}

	public static int changeJobTestIndicator(String jobId,
			String testIndicator, String loginUserId, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int retVal = -1;

		if (jobId.equals("")) {
			jobId = "0";
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_job_testIndicator_manage_01( ?, ?, ?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(jobId));
			cstmt.setString(3, testIndicator);
			cstmt.setInt(4, Integer.parseInt(loginUserId));
			cstmt.execute();
			retVal = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error(
					"changeJobTestIndicator(String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("changeJobTestIndicator(String, String, String, DataSource) - Exception caught in changing Test Indicator for a job: "
						+ e);
			}
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}

		return retVal;
	}

	/*
	 * public static String getOutofSscopeActivityflag( String job_id ,
	 * DataSource ds ) { Connection conn = null; Statement stmt = null;
	 * ResultSet rs = null; String outofscopeflag = "N"; try {
	 * conn=ds.getConnection(); stmt=conn.createStatement();
	 * 
	 * String sql = "select dbo.func_isOut_Scope_Activity('"+job_id+"')";
	 * 
	 * rs = stmt.executeQuery(sql);
	 * 
	 * while( rs.next() ) { outofscopeflag = rs.getString( 1 ); }
	 * 
	 * } catch(Exception e){ System.out.println(
	 * "Error occured during getting outofscopeflag"+e ); }
	 * 
	 * finally { try { if ( rs!= null ) { rs.close(); } }
	 * 
	 * catch( Exception e ) {}
	 * 
	 * 
	 * try { if ( stmt!= null ) stmt.close(); } catch( Exception e ) {}
	 * 
	 * 
	 * try { if ( conn!= null) conn.close(); } catch( Exception e ) {}
	 * 
	 * }
	 * 
	 * return outofscopeflag; }
	 */

}
