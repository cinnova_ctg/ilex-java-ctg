package com.mind.dao.PRM;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.DepotRequisitionBean;


public class DepotRequisitionDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DepotRequisitionDao.class);
	
	public  static ArrayList getDepoCategoryList(String msaid, DataSource ds)
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
				
		try{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql="select * from dbo.func_dp_product_category('"+msaid+"')";	
			rs = stmt.executeQuery(sql);
			int i=0;
			
			while(rs.next())
			{
				DepotRequisitionBean tdForm = new DepotRequisitionBean();
				
				tdForm.setDp_cat_id(rs.getString("dp_cat_id"));
				tdForm.setDp_cat_name(rs.getString("dp_cat_name"));
				 
				valuelist.add(i,tdForm);
				i++;		 
			}
			
		}
		catch(Exception e){
			logger.error("getDepoCategoryList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDepoCategoryList(String, DataSource) - Error occured during getting Depo Category List  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getDepoCategoryList(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getDepoCategoryList(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getDepoCategoryList(String, DataSource) - exception ignored", e);
}
			
		}
		return valuelist;	
	}
	

}
