package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.TicketsPerMsaBean;

public class TicketsPerMsadao 
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TicketsPerMsadao.class);

	public static ArrayList getMsaticketList(String monthValue, DataSource ds)
	{
		
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList(); 
		DecimalFormat dfcur	= new DecimalFormat( "###0.00" );

		try
		{
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lo_netmedx_ticket_summary(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, monthValue);
			rs  = cstmt.executeQuery();
			int i=0;
			
			while(rs.next())
			{
			
				TicketsPerMsaBean tpmForm = new TicketsPerMsaBean();

				tpmForm.setAppendixid(rs.getString("pr_id"));
				tpmForm.setMsaname(rs.getString("ot_name"));
				tpmForm.setInwork(rs.getString("inwork"));
				tpmForm.setInwork_extprice(dfcur.format(rs.getFloat("inwork_extprice")));
				tpmForm.setInwork_actcost(dfcur.format(rs.getFloat("inwork_actcost")));
				tpmForm.setInwork_actvgp(dfcur.format(rs.getFloat("inwork_actvgp")));
				tpmForm.setComplete(rs.getString("complete"));
				tpmForm.setComplete_extprice(dfcur.format(rs.getFloat("complete_extprice")));
				tpmForm.setComplete_actcost(dfcur.format(rs.getFloat("complete_actcost")));
				tpmForm.setComplete_actvgp(dfcur.format(rs.getFloat("complete_actvgp")));
				tpmForm.setClosed(rs.getString("closed"));
				tpmForm.setClosed_extprice(dfcur.format(rs.getFloat("closed_extprice")));
				tpmForm.setClosed_actcost(dfcur.format(rs.getFloat("closed_actcost")));
				tpmForm.setClosed_actvgp(dfcur.format(rs.getFloat("closed_actvgp")));
				tpmForm.setTotal(rs.getString("total"));
				tpmForm.setTotal_extprice(dfcur.format(rs.getFloat("total_extprice")));
				tpmForm.setTotal_actcost(dfcur.format(rs.getFloat("total_actcost")));
				tpmForm.setTotal_actvgp(dfcur.format(rs.getFloat("total_actvgp")));
							 			
     			valuelist.add(i,tpmForm);
				i++;		 
			}
			
		}
		catch(Exception e){
			logger.error("getMsaticketList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsaticketList(String, DataSource) - Error occured during getting get Msa ticket List::::::::::  " + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getMsaticketList(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getMsaticketList(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) {
					conn.close();
				}
			}
			catch( Exception e )
			{
				logger.warn("getMsaticketList(String, DataSource) - exception ignored", e);
}
			
		}
		return valuelist;	
	}
	
	
	public static java.util.Collection getSixMonth(DataSource ds)
	{
		
		ArrayList monthname = new ArrayList();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try 
		{
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call cc_get_months}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			rs  = cstmt.executeQuery();
			
			while(rs.next())
			{
				monthname.add ( new com.mind.common.LabelValue(rs.getString("month_year_name"), rs.getString("month_year_value")));
			}
		}
		catch(Exception e)
		{
			logger.error("getSixMonth(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSixMonth(DataSource) - Exception caught in rerieving Month Name" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getSixMonth(DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getSixMonth(DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null)  {
					conn.close();
				}
			}
			catch( Exception e )
			{
				logger.warn("getSixMonth(DataSource) - exception ignored", e);
}
			
		}
		
		return monthname;
	}
	
	public static String getMonthName(int monthId)
	{
		String monthname[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
		
		return monthname[monthId-1];
	}
}
