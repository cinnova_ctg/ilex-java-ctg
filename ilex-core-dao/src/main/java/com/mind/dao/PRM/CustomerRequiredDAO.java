package com.mind.dao.PRM;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.mind.bean.prm.CustomerRequireBean;
import com.mind.common.LabelValue;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class CustomerRequiredDAO.
 */
public class CustomerRequiredDAO {

	/** The Constant logger. */
	public static final Logger logger = Logger
			.getLogger(CustomerRequiredDAO.class);

	/**
	 * Gets the failed visit category list.
	 * 
	 * @param category
	 *            the category
	 * @param ds
	 *            the ds
	 * @return the failed visit category list
	 */
	public static List<LabelValue> getFailedVisitCategoryList(String category,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<LabelValue> categoryList = new ArrayList<LabelValue>();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select category_code_id,code_desc from fail_visit_category_codes where category='"
					+ category + "' and active=1";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				categoryList.add(new com.mind.common.LabelValue(rs
						.getString("code_desc"), rs
						.getString("category_code_id")));
			}

		} catch (Exception e) {
			logger.error("getRoleList(String,DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRoleList(String,DataSource) - Error occured getFailedVisitCategoryList(String category ,DataSource ds)  "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return categoryList;
	}

	/**
	 * Insert first visit success data answer into db.
	 * 
	 * @param customerRequireBean
	 *            the customer require bean
	 * @param loginUser
	 *            the login user
	 * @param ds
	 *            the ds
	 */
	public static void insertFirstVisitSuccessData(
			CustomerRequireBean customerRequireBean, String loginUser,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "insert into customer_required_data_answers "
					+ "(job_id,ques_id,ans_value,category,category_value,add_site,user_id) values(?,?,?,?,?,?,?)";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, Integer.parseInt(customerRequireBean.getJobId()));
			pStmt.setInt(2, 1);
			pStmt.setBoolean(3, customerRequireBean.getFirstVisitQuestion()
					.booleanValue());
			pStmt.setString(4, "");
			pStmt.setString(5, "");
			pStmt.setString(6, "");
			pStmt.setString(7, loginUser);
			pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error("insertFirstVisitSuccessData()", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * Insert all customer require data.
	 * 
	 * @param customerRequireBean
	 *            the customer require bean
	 * @param loginUser
	 *            the login user
	 * @param ds
	 *            the ds
	 */
	public static void insertAllCustomerRequireData(
			CustomerRequireBean customerRequireBean, String loginUser,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			StringBuilder sqlQuey = new StringBuilder("");
			String sql1 = " insert into customer_required_data_answers "
					+ " (job_id,ques_id,ans_value,category,category_value,add_site,user_id) values(?,?,?,?,?,?,?) ";

			StringBuilder sql2 = new StringBuilder();

			for (String failedVisitQuestion : customerRequireBean
					.getFailedVisitQuestion()) {
				sql2.append(" insert into customer_required_data_answers ");
				sql2.append(" (job_id,ques_id,ans_value,category,category_value,add_site,user_id) values(?,?,?,?,?,?,?) ");

			}

			String sql3 = " insert into customer_required_data_answers "
					+ " (job_id,ques_id,ans_value,category,category_value,add_site,user_id) values(?,?,?,?,?,?,?) ";

			sqlQuey.append(sql1);
			sqlQuey.append(sql2);
			sqlQuey.append(sql3);
			pStmt = conn.prepareStatement(sqlQuey.toString());
			int paramCount = 0;
			pStmt.setInt(++paramCount,
					Integer.parseInt(customerRequireBean.getJobId()));
			pStmt.setInt(++paramCount, 1);
			pStmt.setBoolean(++paramCount, customerRequireBean
					.getFirstVisitQuestion().booleanValue());
			pStmt.setString(++paramCount, "");
			pStmt.setString(++paramCount, "");
			pStmt.setString(++paramCount, "");
			pStmt.setString(++paramCount, loginUser);

			for (String failedVisitQuestion : customerRequireBean
					.getFailedVisitQuestion()) {

				String catergoryValues = getCategoryValue(failedVisitQuestion,
						customerRequireBean);
				pStmt.setInt(++paramCount,
						Integer.parseInt(customerRequireBean.getJobId()));
				pStmt.setInt(++paramCount, 2);
				pStmt.setBoolean(++paramCount, false);
				pStmt.setString(++paramCount, failedVisitQuestion);
				pStmt.setString(++paramCount, catergoryValues);
				pStmt.setString(++paramCount, "");
				pStmt.setString(++paramCount, loginUser);
			}
			pStmt.setInt(++paramCount,
					Integer.parseInt(customerRequireBean.getJobId()));
			pStmt.setInt(++paramCount, 3);
			pStmt.setBoolean(++paramCount, customerRequireBean
					.getAddSiteVisitQuestion().booleanValue());
			pStmt.setString(++paramCount, "");
			pStmt.setString(++paramCount, "");
			pStmt.setString(++paramCount,
					customerRequireBean.getUnplannedAddVisit());
			pStmt.setString(++paramCount, loginUser);
			pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error("insertAllCustomerRequireData()", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

	private static String getCategoryValue(String failedVisitQuestion,
			CustomerRequireBean customerRequireBean) {
		String categoryValues = "";
		if (failedVisitQuestion.equalsIgnoreCase("Contingent")) {
			categoryValues = StringUtils.join(
					customerRequireBean.getContingentFailVisitArray(), ",");

		}
		if (failedVisitQuestion.equalsIgnoreCase("Client")) {
			categoryValues = StringUtils.join(
					customerRequireBean.getClientFailVisitArray(), ",");

		}
		if (failedVisitQuestion.equalsIgnoreCase("LEC")) {
			categoryValues = StringUtils.join(
					customerRequireBean.getLecFailVisitArray(), ",");

		}
		if (failedVisitQuestion.equalsIgnoreCase("Other3rdParty")) {
			categoryValues = StringUtils.join(
					customerRequireBean.getOtherPartyFailVisitArray(), ",");

		}
		return categoryValues;
	}

	/**
	 * Gets the customer required data.
	 * 
	 * @param jobId
	 *            the job id
	 * @param ds
	 *            the ds
	 * @return the customer required data
	 */
	public static CustomerRequireBean getCustomerRequiredData(String jobId,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		CustomerRequireBean customerRequireBean = new CustomerRequireBean();
		customerRequireBean.setJobId(jobId);
		List<String> category = new ArrayList<String>();
		List<String> categoryValues = new ArrayList<String>();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from customer_required_data_answers where job_id="
					+ jobId;
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				customerRequireBean.setDisplayStatus(true);
				if (rs.getInt("ques_id") == 1) {
					customerRequireBean.setFirstVisitQuestion(rs
							.getBoolean("ans_value"));
				} else if (rs.getInt("ques_id") == 2) {
					category.add(rs.getString("category"));
					categoryValues.add(rs.getString("category_value"));

				} else if (rs.getInt("ques_id") == 3) {
					customerRequireBean.setAddSiteVisitQuestion(rs
							.getBoolean("ans_value"));
					customerRequireBean.setUnplannedAddVisit(rs
							.getString("add_site"));
				}
			}

		} catch (Exception e) {
			logger.error("getCustomerRequiredData(String,DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRoleList(String,DataSource) - Error occured getCustomerRequiredData(String jobId ,DataSource ds)  "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		customerRequireBean.setCategory(category);
		customerRequireBean.setCategoryValues(categoryValues);
		return customerRequireBean;
	}

	/**
	 * Gets the selected category codes.
	 * 
	 * @param category
	 *            the category
	 * @param categoryIdList
	 *            the category id list
	 * @param ds
	 *            the ds
	 * @return the selected category codes
	 */
	public static List<LabelValue> getSelectedCategoryCodes(String category,
			String categoryIdList, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<LabelValue> categoryCodeList = new ArrayList<LabelValue>();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = " select code_desc,code_value from fail_visit_category_codes where category='"
					+ category
					+ "' and category_code_id in ("
					+ categoryIdList
					+ ")";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				categoryCodeList.add(new com.mind.common.LabelValue(rs
						.getString("code_desc"), rs.getString("code_value")));
			}

		} catch (Exception e) {
			logger.error("getSelectedCategoryCodes(String,String,DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSelectedCategoryCodes(String,DataSource) - Error occured getSelectedCategoryCodes(String category ,String categoryIdList,DataSource ds)  "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return categoryCodeList;
	}

	public static void deleteCustomerRequireData(String jobId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "delete from customer_required_data_answers where job_id=?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, Integer.parseInt(jobId));
			pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error("deleteCustomerRequireData", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

}
