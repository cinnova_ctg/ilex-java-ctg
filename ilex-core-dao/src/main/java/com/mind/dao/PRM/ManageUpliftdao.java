package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.pm.AppendixEditBean;
import com.mind.bean.prm.ManageUpliftBean;


import com.mind.bean.prm.UpliftValue;

public class ManageUpliftdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ManageUpliftdao.class);

	public static void setUpliftFactor(ManageUpliftBean manageupliftForm, String appendixId, DataSource ds) {
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		DecimalFormat dfcurmargin = new DecimalFormat( "0.00" );
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			sql = "select lm_js_union_uplift_factor, lm_js_expedite24_uplift_factor, lm_js_expedite48_uplift_factor, " +
					"lm_js_afterhours_uplift_factor, lm_js_wh_uplift_factor from dbo.func_get_uplift_factor("+appendixId+")";
			
			rs = stmt.executeQuery( sql );
			
			while (rs.next()) {
					manageupliftForm.setUnionUplift(dfcurmargin.format(rs.getFloat("lm_js_union_uplift_factor")));
					manageupliftForm.setExpedite24Uplift(dfcurmargin.format(rs.getFloat("lm_js_expedite24_uplift_factor")));
					manageupliftForm.setExpedite48Uplift(dfcurmargin.format(rs.getFloat("lm_js_expedite48_uplift_factor")));
					manageupliftForm.setAfterHoursUplift(dfcurmargin.format(rs.getFloat("lm_js_afterhours_uplift_factor")));
					manageupliftForm.setWhUplift(dfcurmargin.format(rs.getFloat("lm_js_wh_uplift_factor")));
				}	
		}
		
		catch( Exception e )
		{
			logger.error("setUpliftFactor(ManageUpliftForm, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setUpliftFactor(ManageUpliftForm, String, DataSource) - Error Occured in retrieving Uplift Factor" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( SQLException s )
			{
				logger.warn("setUpliftFactor(ManageUpliftForm, String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( stmt != null ) 
					stmt.close();
			}
			catch( SQLException s )
			{
				logger.warn("setUpliftFactor(ManageUpliftForm, String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( conn != null ) 
					conn.close();
			}
			catch( SQLException s )
			{
				logger.warn("setUpliftFactor(ManageUpliftForm, String, DataSource) - exception ignored", s);
}
			
		}
		
	}
	
	public static void setAddendumUpliftFactor(ManageUpliftBean manageupliftForm, String jobId, DataSource ds) {
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		DecimalFormat dfcurmargin = new DecimalFormat( "0.00" );
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			sql = "select lm_js_union_uplift_factor, lm_js_expedite24_uplift_factor, lm_js_expedite48_uplift_factor, " +
					"lm_js_afterhours_uplift_factor, lm_js_wh_uplift_factor from dbo.func_get_addendum_uplift_factor("+jobId+")";
			
			rs = stmt.executeQuery( sql );
			
			while (rs.next()) {
					manageupliftForm.setUnionUplift(dfcurmargin.format(rs.getFloat("lm_js_union_uplift_factor")));
					manageupliftForm.setExpedite24Uplift(dfcurmargin.format(rs.getFloat("lm_js_expedite24_uplift_factor")));
					manageupliftForm.setExpedite48Uplift(dfcurmargin.format(rs.getFloat("lm_js_expedite48_uplift_factor")));
					manageupliftForm.setAfterHoursUplift(dfcurmargin.format(rs.getFloat("lm_js_afterhours_uplift_factor")));
					manageupliftForm.setWhUplift(dfcurmargin.format(rs.getFloat("lm_js_wh_uplift_factor")));
				}	
		}
		
		catch( Exception e )
		{
			logger.error("setAddendumUpliftFactor(ManageUpliftForm, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setAddendumUpliftFactor(ManageUpliftForm, String, DataSource) - Error Occured in retrieving Uplift Factor" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( SQLException s )
			{
				logger.warn("setAddendumUpliftFactor(ManageUpliftForm, String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( stmt != null ) 
					stmt.close();
			}
			catch( SQLException s )
			{
				logger.warn("setAddendumUpliftFactor(ManageUpliftForm, String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( conn != null ) 
					conn.close();
			}
			catch( SQLException s )
			{
				logger.warn("setAddendumUpliftFactor(ManageUpliftForm, String, DataSource) - exception ignored", s);
}
			
		}
		
	}
	
	
	public static void setCheckDefaultUpliftFactor(AppendixEditBean appendixlistform, DataSource ds) {
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		DecimalFormat dfcurmargin = new DecimalFormat( "0.00" );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			if(appendixlistform.getExpedite24UpliftFactor().equals("") ||appendixlistform.getExpedite24UpliftFactor()==null){
				sql = "select cc_uplift_value from cc_uplift_master where cc_uplift_type = 'e2'";
				rs = stmt.executeQuery(sql);
				if(rs.next())
						appendixlistform.setExpedite24UpliftFactor(dfcurmargin.format(rs.getFloat(1)));
			}
			
			if(appendixlistform.getExpedite48UpliftFactor().equals("")||appendixlistform.getExpedite48UpliftFactor()==null){
				sql = "select cc_uplift_value from cc_uplift_master where cc_uplift_type = 'e4'";
				rs = stmt.executeQuery(sql);
				if(rs.next())
					appendixlistform.setExpedite48UpliftFactor(dfcurmargin.format(rs.getFloat(1)));
			}
			
			
			if(appendixlistform.getAfterhoursUpliftFactor().equals("")||appendixlistform.getAfterhoursUpliftFactor()==null){
				sql = "select cc_uplift_value from cc_uplift_master where cc_uplift_type = 'ah'";
				rs = stmt.executeQuery(sql);
				if(rs.next())
					appendixlistform.setAfterhoursUpliftFactor(dfcurmargin.format(rs.getFloat(1)));
			}
			
			if(appendixlistform.getWhUpliftFactor().equals("")||appendixlistform.getWhUpliftFactor()==null){
				sql = "select cc_uplift_value from cc_uplift_master where cc_uplift_type = 'wh'";
				rs = stmt.executeQuery(sql);
				if(rs.next())
					appendixlistform.setWhUpliftFactor(dfcurmargin.format(rs.getFloat(1)));
			
			}
			
		}
		
		catch( Exception e )
		{
			logger.error("setCheckDefaultUpliftFactor(AppendixEditBean, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setCheckDefaultUpliftFactor(AppendixEditBean, DataSource) - Error Occured in retrieving Uplift Factor" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( SQLException s )
			{
				logger.warn("setCheckDefaultUpliftFactor(AppendixEditBean, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( stmt != null ) 
					stmt.close();
			}
			catch( SQLException s )
			{
				logger.warn("setCheckDefaultUpliftFactor(AppendixEditBean, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( conn != null ) 
					conn.close();
			}
			catch( SQLException s )
			{
				logger.warn("setCheckDefaultUpliftFactor(AppendixEditBean, DataSource) - exception ignored", s);
}
			
		}
		
	} 
	
	
	public static void setDefaultUpliftFactor(AppendixEditBean appendixlistform, DataSource ds) {
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		DecimalFormat dfcurmargin = new DecimalFormat( "0.00" );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
						
			sql = "select cc_uplift_value from cc_uplift_master where cc_uplift_type = 'e2'";
			rs = stmt.executeQuery(sql);
			if(rs.next()){
					appendixlistform.setExpedite24UpliftFactor(dfcurmargin.format(rs.getFloat(1)));
			}
			
			
			sql = "select cc_uplift_value from cc_uplift_master where cc_uplift_type = 'e4'";
			rs = stmt.executeQuery(sql);
			if(rs.next())
				appendixlistform.setExpedite48UpliftFactor(dfcurmargin.format(rs.getFloat(1)));
			
			sql = "select cc_uplift_value from cc_uplift_master where cc_uplift_type = 'ah'";
			rs = stmt.executeQuery(sql);
			if(rs.next())
				appendixlistform.setAfterhoursUpliftFactor(dfcurmargin.format(rs.getFloat(1)));
			
			sql = "select cc_uplift_value from cc_uplift_master where cc_uplift_type = 'wh'";
			rs = stmt.executeQuery(sql);
			if(rs.next())
				appendixlistform.setWhUpliftFactor(dfcurmargin.format(rs.getFloat(1)));
		}
		
		catch( Exception e )
		{
			logger.error("setDefaultUpliftFactor(AppendixEditBean, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setDefaultUpliftFactor(AppendixEditBean, DataSource) - Error Occured in retrieving Uplift Factor" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( SQLException s )
			{
				logger.warn("setDefaultUpliftFactor(AppendixEditBean, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( stmt != null ) 
					stmt.close();
			}
			catch( SQLException s )
			{
				logger.warn("setDefaultUpliftFactor(AppendixEditBean, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( conn != null ) 
					conn.close();
			}
			catch( SQLException s )
			{
				logger.warn("setDefaultUpliftFactor(AppendixEditBean, DataSource) - exception ignored", s);
}
			
		}
		
	} 
	
	public static String setDefaultUnionUpliftFactor(DataSource ds) {
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		DecimalFormat dfcurmargin = new DecimalFormat( "0.00" );
		String unionUplift = null;
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
						
			sql = "select cc_uplift_value from cc_uplift_master where cc_uplift_type = 'un'";
			rs = stmt.executeQuery(sql);
			if(rs.next())
				unionUplift = dfcurmargin.format(rs.getFloat(1));
		}
		
		catch( Exception e )
		{
			logger.error("setDefaultUnionUpliftFactor(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setDefaultUnionUpliftFactor(DataSource) - Error Occured in retrieving Union Uplift Factor" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( SQLException s )
			{
				logger.warn("setDefaultUnionUpliftFactor(DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( stmt != null ) 
					stmt.close();
			}
			catch( SQLException s )
			{
				logger.warn("setDefaultUnionUpliftFactor(DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( conn != null ) 
					conn.close();
			}
			catch( SQLException s )
			{
				logger.warn("setDefaultUnionUpliftFactor(DataSource) - exception ignored", s);
}
			
		}
		return unionUplift;
		
	}
	
	public static int updateUpliftFactor(String id, String type, String unionUpliftFactor, String expedite24UpliftFactor, String expedite48UpliftFactor, String afterhoursUpliftFactor, String whUpliftFactor, DataSource ds)
	{	
		Connection conn=null;
		CallableStatement cstmt = null;
		//ResultSet rs=null;
		String catgid="";
		int retval=-1;
		/*
		System.out.println("111111111111::::::::::"+id);
		System.out.println("222222222222::::::::::"+type);
		System.out.println("333333333333::::::::::"+unionUpliftFactor);
		System.out.println("444444444444::::::::::"+expedite24UpliftFactor);
		System.out.println("555555555555::::::::::"+expedite48UpliftFactor);
		System.out.println("666666666666::::::::::"+afterhoursUpliftFactor);
		System.out.println("777777777777::::::::::"+whUpliftFactor);
		*/
		try{
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call lm_uplift_factor_manage_01(?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2, id);
			cstmt.setString(3, type);
			cstmt.setString(4, unionUpliftFactor);
			cstmt.setString(5, expedite24UpliftFactor);
			cstmt.setString(6, expedite48UpliftFactor);
			cstmt.setString(7, afterhoursUpliftFactor);
			cstmt.setString(8, whUpliftFactor);
			
			cstmt.execute();
			retval = cstmt.getInt(1);
		}
		catch(Exception e){
			logger.error("updateUpliftFactor(String, String, String, String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateUpliftFactor(String, String, String, String, String, String, String, DataSource) - Error occured during update uplift factors" + e);
			}
		}

		finally
		{
			/*try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{}*/
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("updateUpliftFactor(String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("updateUpliftFactor(String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
		}
		return retval;
	}
	
	public static int applyUpliftFactor(String t0, String t1, String t2, String t3, String t4, String t5, DataSource ds)
	{	
		Connection conn=null;
		CallableStatement cstmt = null;
		//ResultSet rs=null;
		String catgid="";
		int retval=-1;
		/*
		System.out.println("000000000000::::::::::"+t0);
		System.out.println("111111111111::::::::::"+t1);
		System.out.println("222222222222::::::::::"+t2);
		System.out.println("333333333333::::::::::"+t3);
		System.out.println("444444444444::::::::::"+t4);
		System.out.println("555555555555::::::::::"+t5);
		*/
		try {
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call lm_apply_uplift_manage_01(?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2, t0);
			cstmt.setString(3, t1);
			cstmt.setString(4, t2);
			cstmt.setString(5, t3);
			cstmt.setString(6, t4);
			cstmt.setString(7, t5);
			
			cstmt.execute();
			retval = cstmt.getInt(1);
		}
		catch(Exception e){
			logger.error("applyUpliftFactor(String, String, String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("applyUpliftFactor(String, String, String, String, String, String, DataSource) - Error occured during apply uplift factors" + e);
			}
		}
		finally
		{
			/*try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{}*/
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("applyUpliftFactor(String, String, String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("applyUpliftFactor(String, String, String, String, String, String, DataSource) - exception ignored", e);
}
		}
		return retval;
	}
	
	
	public static void getAppliedUplifts(ManageUpliftBean manageupliftForm, String jobId, DataSource ds) 
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		String appliedUplifts[] = null;
		int cnt = 0;  
		int i = 0;
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			sql = "select count(*) as cnt from lm_activity where lm_at_union_uplift_factor = 'Y' and lm_at_js_id ="+jobId;
			rs = stmt.executeQuery(sql);
			if(rs.next()) {
			cnt = rs.getInt("cnt");	
			}
			
			appliedUplifts = new String[cnt];
			sql = "select lm_at_id from lm_activity where lm_at_union_uplift_factor = 'Y' and lm_at_js_id ="+jobId;
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				appliedUplifts[i] = rs.getString(1); 	
				i++;
			}			
			manageupliftForm.setAtUnionUplift(appliedUplifts);
			i = 0;
			
			sql = "select count(*) as cnt from lm_activity where lm_at_expedite24_uplift_factor = 'Y' and lm_at_js_id ="+jobId;
			rs = stmt.executeQuery(sql);
			if(rs.next()) {
			cnt = rs.getInt("cnt");	
			}
			
			appliedUplifts = new String[cnt];
			sql = "select lm_at_id from lm_activity where lm_at_expedite24_uplift_factor = 'Y' and lm_at_js_id ="+jobId;
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				appliedUplifts[i] = rs.getString(1); 	
				i++;
			}			
			manageupliftForm.setAtExpedite24Uplift(appliedUplifts);
			i = 0;			
			
			
			sql = "select count(*) as cnt from lm_activity where lm_at_expedite48_uplift_factor = 'Y' and lm_at_js_id ="+jobId;
			rs = stmt.executeQuery(sql);
			if(rs.next()) {
			cnt = rs.getInt("cnt");	
			}
			
			appliedUplifts = new String[cnt];
			sql = "select lm_at_id from lm_activity where lm_at_expedite48_uplift_factor = 'Y' and lm_at_js_id ="+jobId;
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				appliedUplifts[i] = rs.getString(1); 	
				i++;
			}			
			manageupliftForm.setAtExpedite48Uplift(appliedUplifts);
			i = 0;
			
			sql = "select count(*) as cnt from lm_activity where lm_at_afterhours_uplift_factor = 'Y' and lm_at_js_id ="+jobId;
			rs = stmt.executeQuery(sql);
			if(rs.next()) {
			cnt = rs.getInt("cnt");	
			}
			
			appliedUplifts = new String[cnt];
			sql = "select lm_at_id from lm_activity where lm_at_afterhours_uplift_factor = 'Y' and lm_at_js_id ="+jobId;
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				appliedUplifts[i] = rs.getString(1); 	
				i++;
			}			
			manageupliftForm.setAtAfterHoursUplift(appliedUplifts);
			i = 0;
			
			sql = "select count(*) as cnt from lm_activity where lm_at_wh_uplift_factor = 'Y' and lm_at_js_id ="+jobId;
			rs = stmt.executeQuery(sql);
			if(rs.next()) {
			cnt = rs.getInt("cnt");	
			}
			
			appliedUplifts = new String[cnt];
			sql = "select lm_at_id from lm_activity where lm_at_wh_uplift_factor = 'Y' and lm_at_js_id ="+jobId;
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				appliedUplifts[i] = rs.getString(1); 	
				i++;
			}			
			manageupliftForm.setAtWhUplift(appliedUplifts);
			i = 0;			
			
		}
		catch( Exception e )
		{
			logger.error("getAppliedUplifts(ManageUpliftForm, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppliedUplifts(ManageUpliftForm, String, DataSource) - Exception caught in rerieving Saved Resources " + e);
			}
		}
		finally
		{
			
			try
			{
				if ( rs!= null ) 
				{
					rs.close();
				}
			}
			catch( SQLException s )
			{
				logger.warn("getAppliedUplifts(ManageUpliftForm, String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( stmt!= null ) 
				{
					stmt.close();
				}
			}
			catch( SQLException s )
			{
				logger.warn("getAppliedUplifts(ManageUpliftForm, String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( SQLException s )
			{
				logger.warn("getAppliedUplifts(ManageUpliftForm, String, DataSource) - exception ignored", s);
}
			
		}
		
	}
	
	public static ArrayList getUpliftFactorList(String jobId, String activityId, String type, DataSource ds) 
	{
			ArrayList upliftFactorList = new ArrayList();
			Connection conn = null;
			UpliftValue uplift = null;
			CallableStatement cstmt = null;
			DecimalFormat dfcurmargin = new DecimalFormat( "0.00" );
			DecimalFormat dfcur	= new DecimalFormat( "###0.00" );
			ResultSet rs = null;
			
			try
			{
				conn = ds.getConnection();
				cstmt = conn.prepareCall("{?=call lm_get_uplift_factors(?, ?, ?)}");
				cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
				cstmt.setString(2, jobId);
				cstmt.setString(3, activityId);
				cstmt.setString(4, type);
				
				rs = cstmt.executeQuery();
				
				while( rs.next() )
				{
					uplift = new UpliftValue();
					uplift.setUpliftFactorName(rs.getString("Uplift"));
					uplift.setUpliftFactorValue(dfcurmargin.format(rs.getFloat("uplift_factor")));
					uplift.setUpliftFactorCost(dfcur.format(rs.getFloat("uplift_cost")));
					
					upliftFactorList.add(uplift);
				}
					
			}
			catch( SQLException e )
			{
			logger.error("getUpliftFactorList(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getUpliftFactorList(String, String, String, DataSource) - Exception caught in getting activity library'sow" + e);
			}
			}
			finally
			{
				try
				{
					if( rs != null )
					{
						rs.close();	
					}
				}
				catch( SQLException sql )
				{
				logger.error("getUpliftFactorList(String, String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getUpliftFactorList(String, String, String, DataSource) - Exception caught in getting activity library'sow" + sql);
				}
				}
				
				
				try
				{
					cstmt.close();
				}
				catch( SQLException sql )
				{
				logger.error("getUpliftFactorList(String, String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getUpliftFactorList(String, String, String, DataSource) - Exception caught in getting activity library'sow" + sql);
				}
				}
				
				
				try
				{
					conn.close();
				}
				catch( SQLException sql )
				{
				logger.error("getUpliftFactorList(String, String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getUpliftFactorList(String, String, String, DataSource) - Exception caught in getting activity library'sow" + sql);
				}
				}
				
			}
			return upliftFactorList;
	}
	
}
