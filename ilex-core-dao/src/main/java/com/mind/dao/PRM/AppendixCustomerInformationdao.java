package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class AppendixCustomerInformationdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(AppendixCustomerInformationdao.class);

	/**
	 * This method gets Appendix Name.
	 * 
	 * @param appendixid
	 *            String. Appendix id
	 * @param ds
	 *            Object of DataSource
	 * @return String This method returns String, Appendix Name.
	 */
	public static String getAppendixname(String appendixid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String appendixname = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lx_pr_title from lx_appendix_main where lx_pr_id="
					+ appendixid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				appendixname = rs.getString("lx_pr_title");
			}

		} catch (Exception e) {
			logger.error("getAppendixname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("getAppendixname(String, DataSource) - Error occured during getting Appendix name  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger
						.warn(
								"getAppendixname(String, DataSource) - exception ignored",
								e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger
						.warn(
								"getAppendixname(String, DataSource) - exception ignored",
								e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger
						.warn(
								"getAppendixname(String, DataSource) - exception ignored",
								e);
			}

		}
		return appendixname;
	}

	public static String[][] getAppendixcustinfo(String appendixid,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[][] custinfodetail = new String[30][2];
		int i = 0;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_ap_cust_info_id, lm_ap_cust_info_parameter from lm_appendix_customer_info where lm_ap_cust_info_pr_id="
					+ appendixid;
			// System.out.println("XXxSql"+sql);
			rs = stmt.executeQuery(sql);

			while (rs.next() && i < 30) {
				if (rs.getString("lm_ap_cust_info_parameter") != null) {
					custinfodetail[i][0] = rs.getString("lm_ap_cust_info_id");
					custinfodetail[i][1] = rs
							.getString("lm_ap_cust_info_parameter");
				} else {
					custinfodetail[i][0] = "0";
					custinfodetail[i][1] = "";
				}
				i++;
			}

			for (int k = 29; k >= i; k--) {
				custinfodetail[k][0] = "0";
				custinfodetail[k][1] = "";
			}

		} catch (Exception e) {
			logger.error("getAppendixcustinfo(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("getAppendixcustinfo(String, DataSource) - Error occured during getting getAppendixCustinfo ");
			}
			logger.error("getAppendixcustinfo(String, DataSource)", e);
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger
						.warn(
								"getAppendixcustinfo(String, DataSource) - exception ignored",
								e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger
						.warn(
								"getAppendixcustinfo(String, DataSource) - exception ignored",
								e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger
						.warn(
								"getAppendixcustinfo(String, DataSource) - exception ignored",
								e);
			}

		}
		return custinfodetail;
	}

	public static int addAppendixCustInfo(String lm_ci_pr_id,
			String parastringId, String parastring, String user, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		int retval = -1;
		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call lm_appendix_customer_info_manage_01(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, lm_ci_pr_id);
			cstmt.setString(3, parastringId);
			cstmt.setString(4, parastring);
			cstmt.setString(5, user);

			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger
					.error(
							"addAppendixCustInfo(String, String, String, String, DataSource)",
							e);

			if (logger.isDebugEnabled()) {
				logger
						.debug("addAppendixCustInfo(String, String, String, String, DataSource) - Error occured during adding addappendixCustInfo"
								+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger
						.warn(
								"addAppendixCustInfo(String, String, String, String, DataSource) - exception ignored",
								e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger
						.warn(
								"addAppendixCustInfo(String, String, String, String, DataSource) - exception ignored",
								e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger
						.warn(
								"addAppendixCustInfo(String, String, String, String, DataSource) - exception ignored",
								e);
			}

		}
		return retval;

	}

}
