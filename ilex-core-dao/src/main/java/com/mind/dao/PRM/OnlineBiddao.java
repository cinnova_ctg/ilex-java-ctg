package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.ProjectOnlineBidBean;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * @author seemaarora
 * 
 */
public class OnlineBiddao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OnlineBiddao.class);

	/**
	 * This method retrieve online bid enabled activity information from default
	 * job and addendums that exists within an appendix.
	 * 
	 * @param appendixId
	 *            Project Id as String.
	 * @param ds
	 *            Database Object.
	 * @return onlineBidInfo as Arraylist.
	 */
	public static ArrayList getProjectOnlineBidInfo(String appendixId,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList onlineBidInfo = new ArrayList();
		int i = 0;
		DecimalFormat dformat = new DecimalFormat("#,##0.00");

		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select * from func_get_project_online_bid_info(?)");
			pstmt.setString(1, appendixId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				ProjectOnlineBidBean projectOnlineBid = new ProjectOnlineBidBean();
				projectOnlineBid.setActivityId(rs.getString("lm_at_id"));
				projectOnlineBid.setActivityName(rs.getString("lm_at_title"));
				projectOnlineBid.setQuantity(rs.getString("lm_at_quantity"));
				projectOnlineBid.setSow(rs.getString("pf_sw_sow"));
				projectOnlineBid
						.setAssumption(rs.getString("pf_as_assumption"));
				projectOnlineBid.setEstimatedCost(dformat.format(rs
						.getFloat("estimated_cost")) + "");
				projectOnlineBid.setMinutemanCost(dformat.format(rs
						.getFloat("minuteman_cost")) + "");
				projectOnlineBid.setSpeedpayCost(dformat.format(rs
						.getFloat("speedpay_cost")) + "");
				onlineBidInfo.add(projectOnlineBid);
			}
		} catch (Exception e) {
			logger.error("getProjectOnlineBidInfo(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getProjectOnlineBidInfo(String, DataSource) - Error occurred while retrieving Online Bid Info: "
						+ e);
			}
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}

		return onlineBidInfo;
	}

	/**
	 * This method gets the status for online bid flag in an appendix.
	 * 
	 * @param appendixId
	 *            Project Id as String.
	 * @param ds
	 *            Database Object.
	 * @return 1 or 0 as int.
	 */
	public static int getProjectStatusForOnlineBid(String appendixId,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		int statusBidFlag = 0;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select lx_pd_bid_flag = isnull(lx_pd_bid_flag,0) from lx_appendix_detail where lx_pd_pr_id = "
					+ appendixId;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				statusBidFlag = Integer
						.parseInt(rs.getString("lx_pd_bid_flag"));
			}
		} catch (Exception e) {
			logger.error("getProjectStatusForOnlineBid(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getProjectStatusForOnlineBid(String, DataSource) - Error occurred while retrieving project status for online bid: "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return statusBidFlag;
	}

	/**
	 * @param appendixId
	 * @param bidFlag
	 * @param ds
	 * @return
	 */
	public static int setProjectOnlineBidFlag(String appendixId, int bidFlag,
			DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int retval = -1;

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call vd_project_online_bidflag_manage_01(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, appendixId);
			cstmt.setInt(3, bidFlag);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("setProjectOnlineBidFlag(String, int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setProjectOnlineBidFlag(String, int, DataSource) - Error occurred while setting online bid flag status for an appendix: "
						+ e);
			}
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return retval;
	}
}
