package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.NetMedXDashboardBean;
import com.mind.common.Decimalroundup;
import com.mind.fw.core.dao.util.DBUtil;

public class NetMedXDashboarddao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(NetMedXDashboarddao.class);

	/**
	 * @param appendixId
	 * @param ds
	 * @param querystring
	 * @param sessionMap
	 * @return
	 */
	public static ArrayList getJobSearchdetaillist(String appendixId,
			DataSource ds, String querystring, Map<String, Object> sessionMap) {
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");
		String state = "";
		String dateOnSite = null;
		String dateOffSite = null;
		String dateComplete = null;
		String dateClosed = null;
		String jobSearchName = null;
		String jobSearchSiteNumber = null;
		String jobSearchCity = "";
		String jobSearchOwner = "";
		String jobSearchDateFrom = "";
		String jobSearchDateTo = "";

		if (sessionMap.get("dateOnSite") != null)
			dateOnSite = sessionMap.get("dateOnSite").toString();

		if (sessionMap.get("dateOffSite") != null)
			dateOffSite = sessionMap.get("dateOffSite").toString();

		if (sessionMap.get("dateComplete") != null)
			dateComplete = sessionMap.get("dateComplete").toString();

		if (sessionMap.get("dateClosed") != null)
			dateClosed = sessionMap.get("dateClosed").toString();

		if (sessionMap.get("job_search_state") != null)
			state = sessionMap.get("job_search_state").toString();

		if (sessionMap.get("job_search_name") != null)
			jobSearchName = sessionMap.get("job_search_name").toString().trim()
					.replace("'", "''");

		if (sessionMap.get("job_search_site_number") != null)
			jobSearchSiteNumber = sessionMap.get("job_search_site_number")
					.toString().trim().replace("'", "''");

		if (sessionMap.get("job_search_city") != null)
			jobSearchCity = sessionMap.get("job_search_city").toString();

		if (sessionMap.get("job_search_owner") != null)
			jobSearchOwner = sessionMap.get("job_search_owner").toString();

		if (sessionMap.get("job_search_date_from") != null)
			jobSearchDateFrom = sessionMap.get("job_search_date_from")
					.toString();

		if (sessionMap.get("job_search_date_to") != null)
			jobSearchDateTo = sessionMap.get("job_search_date_to").toString();

		if (state.equals("0"))
			state = "";

		if (appendixId.equals("-1"))
			appendixId = "%";

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		if (querystring.equals(""))
			querystring = "order by lm_js_title";

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lp_netmedx_ticket_tab( ? , ? , ? , ? , ? , ? , ? , ?, ?, ?, ?, ?, ? )}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, appendixId);
			cstmt.setString(3, jobSearchName);
			cstmt.setString(4, jobSearchSiteNumber);
			cstmt.setString(5, jobSearchCity);
			cstmt.setString(6, state);
			cstmt.setString(7, jobSearchOwner);
			cstmt.setString(8, jobSearchDateFrom);
			cstmt.setString(9, jobSearchDateTo);
			cstmt.setString(10, dateOnSite);
			cstmt.setString(11, dateOffSite);
			cstmt.setString(12, dateComplete);
			cstmt.setString(13, dateClosed);
			cstmt.setString(14, querystring);

			rs = cstmt.executeQuery();

			int i = 0;

			while (rs.next()) {
				NetMedXDashboardBean netmedxdashboard = new NetMedXDashboardBean();
				netmedxdashboard.setLo_ot_name(rs.getString("lo_ot_name"));
				netmedxdashboard.setAppendix_id(rs.getString("lm_js_pr_id"));
				netmedxdashboard.setJobid(rs.getString("lm_js_id"));
				netmedxdashboard.setJobname(rs.getString("lm_js_title"));
				netmedxdashboard.setJob_status(rs.getString("lm_js_status"));
				netmedxdashboard
						.setInvoice_no(rs.getString("lm_js_invoice_no"));
				netmedxdashboard.setJobtype(rs.getString("lm_js_type"));
				netmedxdashboard.setTicket_id(rs.getString("lm_tc_id"));
				netmedxdashboard.setTicket_number(rs.getString("lm_tc_number"));
				netmedxdashboard.setStandby(rs.getString("lm_tc_standby"));
				netmedxdashboard.setMsp(rs.getString("lm_tc_msp"));
				netmedxdashboard.setHelpDesk(rs.getString("lm_tc_help_desk"));
				netmedxdashboard.setSite_name(rs.getString("lm_si_name"));
				netmedxdashboard.setSite_number(rs.getString("lm_si_number"));
				netmedxdashboard.setSite_state(rs.getString("cp_state_name"));
				netmedxdashboard
						.setSite_zipcode(rs.getString("lm_si_zip_code"));
				netmedxdashboard.setSite_city(rs.getString("lm_si_city"));
				netmedxdashboard.setJob_extendedprice(dfcur.format(rs
						.getFloat("lx_ce_total_price")) + "");
				netmedxdashboard.setJob_estimatedcost(dfcur.format(rs
						.getFloat("lx_ce_total_cost")) + "");
				netmedxdashboard.setJob_proformamarginvgp(dfcurmargin.format(rs
						.getFloat("lx_ce_margin")) + "");
				netmedxdashboard.setSitescheduleplanned_startdate(rs
						.getString("lm_js_planned_start_date"));
				netmedxdashboard.setSitescheduleplanned_startdatetime(rs
						.getString("lx_act_start_time"));
				netmedxdashboard.setJob_netrequestor(rs
						.getString("lm_tc_requestor_name"));
				netmedxdashboard.setJob_netproblemdesc(rs
						.getString("lm_tc_problem_desc"));
				netmedxdashboard.setJob_netspecialinstruct(rs
						.getString("lm_tc_spec_instruction"));
				netmedxdashboard.setJob_netcategory(rs
						.getString("lm_al_cat_name"));
				netmedxdashboard.setJob_netcriticality(rs
						.getString("lo_call_criticality_des"));
				netmedxdashboard.setRequest_received_date(rs
						.getString("lm_tc_requested_date"));
				netmedxdashboard.setRequest_received_time(rs
						.getString("lm_tc_requested_time"));
				netmedxdashboard.setHourly_cost(dfcur.format(rs
						.getFloat("hourly_cost")));
				netmedxdashboard.setHourly_price(dfcur.format(rs
						.getFloat("hourly_price")));
				netmedxdashboard.setHourly_vgp(dfcur.format(rs
						.getFloat("hourly_vgp")));
				netmedxdashboard.setResource_level(rs
						.getString("resource_level"));
				netmedxdashboard.setDocumentpresent(NetMedXDashboarddao
						.uploadedFilespresent("%", netmedxdashboard.getJobid(),
								"Job", ds));
				netmedxdashboard.setTicketcreatedby(rs
						.getString("job_created_by"));
				String priorities = rs.getString("lp_sl_priority_store");
				if (priorities.equals(null)) {
					netmedxdashboard.setSite_priorities("N");
				}

				if (priorities.equals("0")) {
					netmedxdashboard.setSite_priorities("N");
				} else {

					netmedxdashboard.setSite_priorities("Y");

				}

				valuelist.add(i, netmedxdashboard);
				i++;
			}
			cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"getJobSearchdetaillist(String, DataSource, String, HttpSession)"
							+ appendixId + ", jobSearchName =" + jobSearchName
							+ ", jobSearchSiteNumber =" + jobSearchSiteNumber
							+ ", jobSearchCity =" + jobSearchCity + ", state ="
							+ state + ", jobSearchOwner =" + jobSearchOwner
							+ ", jobSearchDateFrom =" + jobSearchDateFrom
							+ ", jobSearchDateTo =" + jobSearchDateTo
							+ ", dateOnSite =" + dateOnSite + ", dateOffSite ="
							+ dateOffSite + ", dateComplete =" + dateComplete
							+ ", dateClosed =" + dateClosed + ", querystring ="
							+ querystring, e);

			if (logger.isDebugEnabled()) {
				logger.debug(
						"getJobSearchdetaillist(String, DataSource, String, HttpSession)"
								+ appendixId + ", jobSearchName ="
								+ jobSearchName + ", jobSearchSiteNumber ="
								+ jobSearchSiteNumber + ", jobSearchCity ="
								+ jobSearchCity + ", state =" + state
								+ ", jobSearchOwner =" + jobSearchOwner
								+ ", jobSearchDateFrom =" + jobSearchDateFrom
								+ ", jobSearchDateTo =" + jobSearchDateTo
								+ ", dateOnSite =" + dateOnSite
								+ ", dateOffSite =" + dateOffSite
								+ ", dateComplete =" + dateComplete
								+ ", dateClosed =" + dateClosed
								+ ", querystring =" + querystring, e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobSearchdetaillist(String, DataSource, String, HttpSession) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobSearchdetaillist(String, DataSource, String, HttpSession) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobSearchdetaillist(String, DataSource, String, HttpSession) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static ArrayList getStateCategoryList(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select cp_country_id,cp_country_name, cp_country_order from lp_country_list order by cp_country_order,cp_country_name";
			rs = stmt.executeQuery(sql);
			int i = 0;

			if (i == 0) {
				NetMedXDashboardBean netmedxdashboard = new NetMedXDashboardBean();
				netmedxdashboard.setSiteCountryId("");
				netmedxdashboard.setSiteCountryName("");
				valuelist.add(i, netmedxdashboard);
				i++;
			}

			while (rs.next()) {
				NetMedXDashboardBean netmedxdashboard = new NetMedXDashboardBean();
				netmedxdashboard
						.setSiteCountryId(rs.getString("cp_country_id"));
				netmedxdashboard.setSiteCountryName(rs
						.getString("cp_country_name"));
				valuelist.add(i, netmedxdashboard);
				i++;
			}

		} catch (Exception e) {
			logger.error("getStateCategoryList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStateCategoryList(DataSource) - Error occured during getting State Category Wise List  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	/**
	 * @param appendixid
	 * @param sortqueryclause
	 * @param status
	 * @param userId
	 * @param ds
	 * @param ownerId
	 * @param timeFrame
	 * @return
	 */
	public static ArrayList getNetMedXJobtabular(String appendixid,
			String sortqueryclause, String status, String userId,
			DataSource ds, String ownerId, String timeFrame) {

		ArrayList joblist = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int i = 0;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");

		if (appendixid.equals("-1")) {
			appendixid = "%";
		}
		String sql = "select * from dbo.func_netmedx_ticket_tab('" + appendixid
				+ "','%','%')where 1=1";

		if (userId.equals("All")) {
			userId = "%";
		}

		if (status != null && !status.equals("")) {
			if (status.contains("AI")) {
				status = status.replaceAll("\'AI\'",
						"\'IS\',\'II\',\'IO\',\'ION\',\'IOF\'");
			}
			sql = sql + " and temp_status in " + status;
		}

		if (ownerId != null && !ownerId.equals("")) {
			if (ownerId.contains("%")) {

			} else {
				sql = sql + " and lm_js_created_by in " + ownerId;
			}

		}

		if (timeFrame != null && !timeFrame.equals("")) {
			if (timeFrame.equals("month"))
				sql = sql
						+ "	and	( datepart(mm, lm_js_create_date) =  datepart(mm, getdate()) and datepart(yy, lm_js_create_date) =  datepart(yy, getdate()))  ";

			else if (timeFrame.equals("week"))
				sql = sql
						+ "	and	( datepart(wk, lm_js_create_date) =  datepart(wk, getdate())and datepart(yy, lm_js_create_date) =  datepart(yy, getdate()))  ";
			else {
			}

		}

		if (sortqueryclause.equals("")) {
			sortqueryclause = "	order by lo_call_criticality_des, lm_tc_requested_date, lm_tc_requested_time";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = sql + sortqueryclause;

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				NetMedXDashboardBean netmedxdashboard = new NetMedXDashboardBean();
				netmedxdashboard.setLo_ot_name(rs.getString("lo_ot_name"));
				netmedxdashboard.setAppendix_id(rs.getString("lm_js_pr_id"));
				netmedxdashboard.setJobid(rs.getString("lm_js_id"));
				netmedxdashboard.setJobname(rs.getString("lm_js_title"));
				netmedxdashboard.setJob_status(rs.getString("lm_js_status"));
				netmedxdashboard
						.setInvoice_no(rs.getString("lm_js_invoice_no"));
				netmedxdashboard.setJobtype(rs.getString("lm_js_type"));
				netmedxdashboard.setTicket_id(rs.getString("lm_tc_id"));
				netmedxdashboard.setTicket_number(rs.getString("lm_tc_number"));
				netmedxdashboard.setStandby(rs.getString("lm_tc_standby"));
				netmedxdashboard.setMsp(rs.getString("lm_tc_msp"));
				netmedxdashboard.setHelpDesk(rs.getString("lm_tc_help_desk"));
				netmedxdashboard.setSite_name(rs.getString("lm_si_name"));
				netmedxdashboard.setSite_number(rs.getString("lm_si_number"));
				netmedxdashboard.setSite_state(rs.getString("cp_state_name"));
				netmedxdashboard
						.setSite_zipcode(rs.getString("lm_si_zip_code"));
				netmedxdashboard.setSite_city(rs.getString("lm_si_city"));
				netmedxdashboard.setJob_extendedprice(dfcur.format(rs
						.getFloat("lx_ce_total_price")) + "");
				netmedxdashboard.setJob_estimatedcost(dfcur.format(rs
						.getFloat("lx_ce_total_cost")) + "");
				netmedxdashboard.setJob_proformamarginvgp(dfcurmargin.format(rs
						.getFloat(16)) + "");
				netmedxdashboard.setSitescheduleplanned_startdate(rs
						.getString("lm_js_planned_start_date"));
				netmedxdashboard.setSitescheduleplanned_startdatetime(rs
						.getString("lx_act_start_time"));
				netmedxdashboard.setJob_netrequestor(rs
						.getString("lm_tc_requestor_name"));
				netmedxdashboard.setJob_netproblemdesc(rs
						.getString("lm_tc_problem_desc"));
				netmedxdashboard.setJob_netspecialinstruct(rs
						.getString("lm_tc_spec_instruction"));
				netmedxdashboard.setJob_netcategory(rs
						.getString("lm_al_cat_name"));
				netmedxdashboard.setJob_netcriticality(rs
						.getString("lo_call_criticality_des"));
				netmedxdashboard.setRequest_received_date(rs
						.getString("lm_tc_requested_date"));
				netmedxdashboard.setRequest_received_time(rs
						.getString("lm_tc_requested_time"));
				netmedxdashboard.setHourly_cost(dfcur.format(rs
						.getFloat("hourly_cost")));
				netmedxdashboard.setHourly_price(dfcur.format(rs
						.getFloat("hourly_price")));
				netmedxdashboard.setHourly_vgp(dfcur.format(rs
						.getFloat("hourly_vgp")));
				netmedxdashboard.setResource_level(rs
						.getString("resource_level"));
				/* to show yellow folder for uploaded document */
				netmedxdashboard.setDocumentpresent(NetMedXDashboarddao
						.uploadedFilespresent("%", netmedxdashboard.getJobid(),
								"Job", ds));
				/* to show yellow folder for uploaded document */
				netmedxdashboard.setTicketcreatedby(rs
						.getString("job_created_by"));
				joblist.add(i, netmedxdashboard);
				i++;
			}

		} catch (Exception e) {
			logger.error(
					"getNetMedXJobtabular(String, String, String, String, DataSource, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNetMedXJobtabular(String, String, String, String, DataSource, String, String) - Error occured during getting NetMedX Dashboard Job/Ticket List::::::"
						+ e);
			}
			logger.error(
					"getNetMedXJobtabular(String, String, String, String, DataSource, String, String)",
					e);
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getNetMedXJobtabular(String, String, String, String, DataSource, String, String) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getNetMedXJobtabular(String, String, String, String, DataSource, String, String) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getNetMedXJobtabular(String, String, String, String, DataSource, String, String) - exception ignored",
						e);
			}

		}
		return joblist;
	}

	public static ArrayList getNetMedXJobtabularNew(String appendixid,
			String sortqueryclause, String status, String userId,
			DataSource ds, String ownerId, String timeFrame, String fromDate,
			String toDate) {

		ArrayList joblist = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int i = 0;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");

		if (appendixid.equals("-1")) {
			appendixid = "%";
		}
		String sql = "select * from dbo.func_netmedx_ticket_tab('" + appendixid
				+ "','%','%')where 1=1";

		if (userId.equals("All")) {
			userId = "%";
		}

		if (status != null && !status.equals("")) {
			if (status.contains("AI")) {
				status = status.replaceAll("\'AI\'",
						"\'IS\',\'II\',\'IO\',\'ION\',\'IOF\'");
			}
			sql = sql + " and temp_status in " + status;
		}

		if (ownerId != null && !ownerId.equals("")) {
			if (ownerId.contains("%")) {

			} else {
				sql = sql + " and lm_js_created_by in " + ownerId;
			}

		}

		if (timeFrame != null && !timeFrame.equals("")) {
			if (timeFrame.equals("month"))
				sql = sql
						+ "	and	( datepart(mm, lm_js_create_date) =  datepart(mm, getdate()) and datepart(yy, lm_js_create_date) =  datepart(yy, getdate()))  ";

			else if (timeFrame.equals("week"))
				sql = sql
						+ "	and	( datepart(wk, lm_js_create_date) =  datepart(wk, getdate())and datepart(yy, lm_js_create_date) =  datepart(yy, getdate()))  ";
			else if (timeFrame.equals("custom")) {
				sql = sql + "	and lm_js_create_date >=  '" + fromDate
						+ "'	AND  lm_js_create_date <= '" + toDate + "' ";

			}

		}

		if (sortqueryclause.equals("")) {
			sortqueryclause = "	order by lo_call_criticality_des, lm_tc_requested_date, lm_tc_requested_time";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = sql + sortqueryclause;

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				NetMedXDashboardBean netmedxdashboard = new NetMedXDashboardBean();
				netmedxdashboard.setLo_ot_name(rs.getString("lo_ot_name"));
				netmedxdashboard.setAppendix_id(rs.getString("lm_js_pr_id"));
				netmedxdashboard.setJobid(rs.getString("lm_js_id"));
				netmedxdashboard.setJobname(rs.getString("lm_js_title"));
				netmedxdashboard.setJob_status(rs.getString("lm_js_status"));
				netmedxdashboard
						.setInvoice_no(rs.getString("lm_js_invoice_no"));
				netmedxdashboard.setJobtype(rs.getString("lm_js_type"));
				netmedxdashboard.setTicket_id(rs.getString("lm_tc_id"));
				netmedxdashboard.setTicket_number(rs.getString("lm_tc_number"));
				netmedxdashboard.setStandby(rs.getString("lm_tc_standby"));
				netmedxdashboard.setMsp(rs.getString("lm_tc_msp"));
				netmedxdashboard.setHelpDesk(rs.getString("lm_tc_help_desk"));
				netmedxdashboard.setSite_name(rs.getString("lm_si_name"));
				netmedxdashboard.setSite_number(rs.getString("lm_si_number"));
				netmedxdashboard.setSite_state(rs.getString("cp_state_name"));
				netmedxdashboard
						.setSite_zipcode(rs.getString("lm_si_zip_code"));
				netmedxdashboard.setSite_city(rs.getString("lm_si_city"));
				netmedxdashboard.setJob_extendedprice(dfcur.format(rs
						.getFloat("lx_ce_total_price")) + "");
				netmedxdashboard.setJob_estimatedcost(dfcur.format(rs
						.getFloat("lx_ce_total_cost")) + "");
				netmedxdashboard.setJob_proformamarginvgp(dfcurmargin.format(rs
						.getFloat(16)) + "");
				netmedxdashboard.setSitescheduleplanned_startdate(rs
						.getString("lm_js_planned_start_date"));
				netmedxdashboard.setSitescheduleplanned_startdatetime(rs
						.getString("lx_act_start_time"));
				netmedxdashboard.setJob_netrequestor(rs
						.getString("lm_tc_requestor_name"));
				netmedxdashboard.setJob_netproblemdesc(rs
						.getString("lm_tc_problem_desc"));
				netmedxdashboard.setJob_netspecialinstruct(rs
						.getString("lm_tc_spec_instruction"));
				netmedxdashboard.setJob_netcategory(rs
						.getString("lm_al_cat_name"));
				netmedxdashboard.setJob_netcriticality(rs
						.getString("lo_call_criticality_des"));
				netmedxdashboard.setRequest_received_date(rs
						.getString("lm_tc_requested_date"));
				netmedxdashboard.setRequest_received_time(rs
						.getString("lm_tc_requested_time"));
				netmedxdashboard.setHourly_cost(dfcur.format(rs
						.getFloat("hourly_cost")));
				netmedxdashboard.setHourly_price(dfcur.format(rs
						.getFloat("hourly_price")));
				netmedxdashboard.setHourly_vgp(dfcur.format(rs
						.getFloat("hourly_vgp")));
				netmedxdashboard.setResource_level(rs
						.getString("resource_level"));
				/* to show yellow folder for uploaded document */
				netmedxdashboard.setDocumentpresent(NetMedXDashboarddao
						.uploadedFilespresent("%", netmedxdashboard.getJobid(),
								"Job", ds));
				/* to show yellow folder for uploaded document */
				netmedxdashboard.setTicketcreatedby(rs
						.getString("job_created_by"));
				String priorities = rs.getString("lp_sl_priority_store");
				if (priorities.equals(null)) {
					netmedxdashboard.setSite_priorities("N");
				}

				if (priorities.equals("0")) {
					netmedxdashboard.setSite_priorities("N");
				} else {

					netmedxdashboard.setSite_priorities("Y");

				}

				joblist.add(i, netmedxdashboard);
				i++;
			}

		} catch (Exception e) {
			logger.error(
					"getNetMedXJobtabular(String, String, String, String, DataSource, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNetMedXJobtabular(String, String, String, String, DataSource, String, String) - Error occured during getting NetMedX Dashboard Job/Ticket List::::::"
						+ e);
			}
			logger.error(
					"getNetMedXJobtabular(String, String, String, String, DataSource, String, String)",
					e);
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getNetMedXJobtabular(String, String, String, String, DataSource, String, String) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getNetMedXJobtabular(String, String, String, String, DataSource, String, String) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getNetMedXJobtabular(String, String, String, String, DataSource, String, String) - exception ignored",
						e);
			}

		}
		return joblist;
	}

	/**
	 * @param appendixid
	 * @param ds
	 * @return
	 */
	public static ArrayList getNetMedXDispatchTrendAnalysis(String appendixid,
			DataSource ds) {
		ArrayList trendanalysislist = new ArrayList();

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int i = 0;

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lo_netmedx_dispatch_trend_analysis_01(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, appendixid);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				NetMedXDashboardBean netmedxdashboard = new NetMedXDashboardBean();

				if (rs.getString("c2typename") != null)
					netmedxdashboard.setDa_category_name(rs
							.getString("c2typename"));
				else
					netmedxdashboard.setDa_category_name("");

				if (rs.getString("c2typenamecount") != null)
					netmedxdashboard.setDa_category_count(rs
							.getString("c2typenamecount"));
				else
					netmedxdashboard.setDa_category_count("");

				if (rs.getString("c3typename") != null)
					netmedxdashboard.setDa_state_name(rs
							.getString("c3typename"));
				else
					netmedxdashboard.setDa_state_name("");
				if (rs.getString("c3typenamecount") != null)
					netmedxdashboard.setDa_state_count(rs
							.getString("c3typenamecount"));
				else
					netmedxdashboard.setDa_state_count("");

				if (rs.getString("c4typename") != null)
					netmedxdashboard.setDa_requestor_name(rs
							.getString("c4typename"));
				else
					netmedxdashboard.setDa_requestor_name("");
				if (rs.getString("c4typenamecount") != null)
					netmedxdashboard.setDa_requestor_count(rs
							.getString("c4typenamecount"));
				else
					netmedxdashboard.setDa_requestor_count("");

				if (rs.getString("c5typename") != null)
					netmedxdashboard.setDa_criticality_name(rs
							.getString("c5typename"));
				else
					netmedxdashboard.setDa_criticality_name("");
				if (rs.getString("c5typenamecount") != null)
					netmedxdashboard.setDa_criticality_count(rs
							.getString("c5typenamecount"));
				else
					netmedxdashboard.setDa_criticality_count("");

				trendanalysislist.add(i, netmedxdashboard);
				i++;
			}

		} catch (Exception e) {
			logger.error("getNetMedXDispatchTrendAnalysis(String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNetMedXDispatchTrendAnalysis(String, DataSource) - Error occured during getting NetMedX Dispatch Trend Analysis::::::"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getNetMedXDispatchTrendAnalysis(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getNetMedXDispatchTrendAnalysis(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getNetMedXDispatchTrendAnalysis(String, DataSource) - exception ignored",
						e);
			}

		}
		return trendanalysislist;
	}

	/**
	 * @param msaId
	 * @param appendixId
	 * @param ds
	 */
	public static void deletePassword(String msaId, String appendixId,
			DataSource ds) {

		Statement stmt = null;
		ResultSet rs = null;
		Connection conn = null;

		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "DELETE FROM lm_pr_authentication WHERE lm_pr_auth_msaId ="
					+ msaId + " AND lm_pr_auth_project_id =" + appendixId;
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			logger.error("deletePassword(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deletePassword(String, String, DataSource) - Error caught in getting uploadedFilespresent"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"deletePassword(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"deletePassword(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"deletePassword(String, String, DataSource) - exception ignored",
						s);
			}

		}

	}

	/**
	 * @param msaId
	 * @param AppendixId
	 * @param ds
	 * @return
	 */
	public static ArrayList<String> checkPassword(String msaId,
			String AppendixId, DataSource ds) {
		String pass = "";
		String userName = "";
		ArrayList<String> userCheck = new ArrayList<String>();
		Statement stmt = null;
		ResultSet rs = null;
		Connection conn = null;

		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_pr_auth_password,lm_pr_auth_user_name from lm_pr_authentication where lm_pr_auth_msaId= "
					+ msaId + " and lm_pr_auth_project_id = " + AppendixId;

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				pass = rs.getString(1);
				userName = rs.getString(2);
			}
		} catch (Exception e) {
			logger.error("checkPassword(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkPassword(String, String, DataSource) - Error caught in getting uploadedFilespresent"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"checkPassword(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"checkPassword(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"checkPassword(String, String, DataSource) - exception ignored",
						s);
			}

		}
		userCheck.add(pass);
		userCheck.add(userName);
		return userCheck;

	}

	/**
	 * @param msaId
	 * @param prName
	 * @param password
	 * @param ds
	 */
	public static void setAPIAccessValue(String msaId, String prName,
			String password, String prId, String apiDoc, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		try {
			conn = ds.getConnection();
			String sql = "INSERT INTO dbo.lm_pr_authentication (lm_pr_auth_msaId, lm_pr_auth_access, lm_pr_auth_user_name, lm_pr_auth_password,lm_pr_auth_project_id) "
					+ " VALUES ( ?, ?, ?, ?,?)";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, Integer.parseInt(msaId));
			pStmt.setInt(2, 1);
			pStmt.setString(3, prName);
			pStmt.setString(4, password);
			pStmt.setString(5, prId);
			pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error(
					"setAPIAccessValue(String, String, String, String, DataSource )",
					e);
		} finally {
			try {
				pStmt.close();
				conn.close();
			} catch (Exception e) {
				logger.error("setAPIAccessValue()", e);
			}
		}
	}

	/**
	 * @param appendixid
	 * @param ds
	 * @return
	 */
	public static ArrayList getCompleteDispatchSummary(String appendixid,
			DataSource ds) {
		ArrayList compdispatchlist = new ArrayList();

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int i = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lo_netmedx_dispatch_ticket_summary_01(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, appendixid);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				NetMedXDashboardBean netmedxdashboard = new NetMedXDashboardBean();

				netmedxdashboard.setCd_msa_name(rs.getString("ot_name"));
				netmedxdashboard.setCd_cm_count(rs.getString("cm_count"));
				netmedxdashboard.setCd_avg_count(Decimalroundup
						.twodecimalplaces(rs.getFloat("avg_count"), 2));
				netmedxdashboard.setCd_ttm_count(rs.getString("ttm_count"));

				compdispatchlist.add(i, netmedxdashboard);
				i++;
			}

		} catch (Exception e) {
			logger.error("getCompleteDispatchSummary(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCompleteDispatchSummary(String, DataSource) - Error occured during getting Completeed Dispatch Summary::::::"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCompleteDispatchSummary(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCompleteDispatchSummary(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCompleteDispatchSummary(String, DataSource) - exception ignored",
						e);
			}

		}
		return compdispatchlist;
	}

	/**
	 * @param netmedxdashboardForm
	 * @param appendixid
	 * @param ds
	 * @return
	 */
	public static NetMedXDashboardBean getNetMedXDispatcmFinancialSummary(
			NetMedXDashboardBean netmedxdashboardForm, String appendixid,
			DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int i = 0;
		ResultSet rs = null;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lo_netmedx_dispatch_cm_financial_summary_01(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, appendixid);
			rs = cstmt.executeQuery();

			if (rs.next()) {
				netmedxdashboardForm.setDfs_curr_mnth_request(rs
						.getString("curr_mnth_request"));
				netmedxdashboardForm.setDfs_curr_mnth_hd(rs
						.getString("curr_mnth_hd"));
				netmedxdashboardForm.setDfs_curr_mnth_comp(rs
						.getString("curr_mnth_comp"));
				netmedxdashboardForm.setDfs_curr_mnth_inwork(rs
						.getString("curr_mnth_inwork"));
				netmedxdashboardForm.setDfs_curr_mnth_cancelled(rs
						.getString("curr_mnth_cancelled"));

				netmedxdashboardForm.setDfs_curr_mnth_extprice(dfcur.format(rs
						.getFloat("curr_mnth_extprice")));

				netmedxdashboardForm.setDfs_curr_mnth_actcost(dfcur.format(rs
						.getFloat("curr_mnth_actcost")));
				netmedxdashboardForm.setDfs_curr_mnth_avgp(dfcurmargin
						.format(rs.getFloat("curr_mnth_avgp")));

				netmedxdashboardForm.setDfs_curr_mnth_avgcost(dfcur.format(rs
						.getFloat("curr_mnth_avgcost")));
				netmedxdashboardForm.setDfs_curr_mnth_avgcharge(dfcur.format(rs
						.getFloat("curr_mnth_avgcharge")));

				netmedxdashboardForm.setDfs_curr_mnth_avgtimeonsite(rs
						.getString("curr_mnth_avgtimeonsite"));
				netmedxdashboardForm.setDfs_curr_mnth_avgtimeonarrive(rs
						.getString("curr_mnth_avgtimeonarrive"));
			}

		} catch (Exception e) {
			logger.error(
					"getNetMedXDispatcmFinancialSummary(NetMedXDashboardForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNetMedXDispatcmFinancialSummary(NetMedXDashboardForm, String, DataSource) - Error occured during getting NetMedX Current Month Dispatch Financial Summary::::::"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getNetMedXDispatcmFinancialSummary(NetMedXDashboardForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getNetMedXDispatcmFinancialSummary(NetMedXDashboardForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getNetMedXDispatcmFinancialSummary(NetMedXDashboardForm, String, DataSource) - exception ignored",
						e);
			}

		}
		return netmedxdashboardForm;
	}

	/**
	 * @param netmedxdashboardForm
	 * @param appendixid
	 * @param ds
	 * @return
	 */
	public static NetMedXDashboardBean getNetMedXDispattotalFinancialSummary(
			NetMedXDashboardBean netmedxdashboardForm, String appendixid,
			DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		DecimalFormat dfcur = new DecimalFormat("###0.00");
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lo_netmedx_dispatch_ttmtotal_financial_summary_01(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, appendixid);
			rs = cstmt.executeQuery();

			if (rs.next()) {
				netmedxdashboardForm.setDfs_ttm_total_request(rs
						.getString("ttm_total_request"));
				netmedxdashboardForm.setDfs_ttm_total_hd(rs
						.getString("ttm_total_hd"));
				netmedxdashboardForm.setDfs_ttm_total_comp(rs
						.getString("ttm_total_comp"));
				netmedxdashboardForm.setDfs_ttm_total_inwork(rs
						.getString("ttm_total_inwork"));
				netmedxdashboardForm.setDfs_ttm_total_cancelled(rs
						.getString("ttm_total_cancelled"));
				netmedxdashboardForm.setDfs_ttm_total_extprice(dfcur.format(rs
						.getFloat("ttm_total_extprice")));

				netmedxdashboardForm.setDfs_ttm_total_actcost(dfcur.format(rs
						.getFloat("ttm_total_actcost")));
				netmedxdashboardForm.setDfs_ttm_total_avgp(dfcurmargin
						.format(rs.getFloat("ttm_total_avgp")));

				netmedxdashboardForm.setDfs_ttm_total_avgcost(dfcur.format(rs
						.getFloat("ttm_total_avgcost")));
				netmedxdashboardForm.setDfs_ttm_total_avgcharge(dfcur.format(rs
						.getFloat("ttm_total_avgcharge")));

				netmedxdashboardForm.setDfs_ttm_total_avgtimeonsite(rs
						.getString("ttm_total_avgtimeonsite"));

				netmedxdashboardForm.setDfs_ttm_total_avgtimeonarrive(rs
						.getString("ttm_total_avgtimeonarrive"));

			}

		} catch (Exception e) {
			logger.error(
					"getNetMedXDispattotalFinancialSummary(NetMedXDashboardForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNetMedXDispattotalFinancialSummary(NetMedXDashboardForm, String, DataSource) - Error occured during getting NetMedX Current Month Dispatch Financial Summary::::::"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getNetMedXDispattotalFinancialSummary(NetMedXDashboardForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getNetMedXDispattotalFinancialSummary(NetMedXDashboardForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getNetMedXDispattotalFinancialSummary(NetMedXDashboardForm, String, DataSource) - exception ignored",
						e);
			}

		}
		return netmedxdashboardForm;
	}

	/**
	 * @param totaldiff
	 * @return
	 */
	public static String getAvgDateDiff(String totaldiff) {
		String avgdiff = null;
		double totalhour = 0.0;
		double avgmin = 0.0;
		String chklen = null;
		DecimalFormat dfcur = new DecimalFormat("###0");
		try {
			totalhour = Double.parseDouble(totaldiff.substring(0,
					totaldiff.indexOf(":")))
					+ Double.parseDouble(totaldiff.substring(
							totaldiff.indexOf(":") + 1, totaldiff.length()))
					/ 60;
			if (totalhour != 0.0) {

				avgmin = (totalhour / 12) * 60;
				avgdiff = dfcur.format((avgmin - avgmin % 60) / 60) + ":"
						+ dfcur.format(avgmin % 60);
			}

			else
				avgdiff = "0:00";
		} catch (Exception e) {
			logger.error("getAvgDateDiff(String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAvgDateDiff(String) - Error occured during getting Avgerage Date Difference::::::"
						+ e);
			}
		}

		chklen = avgdiff.substring(avgdiff.indexOf(":") + 1, avgdiff.length());

		if (chklen.length() == 1)
			avgdiff = avgdiff.substring(0, avgdiff.indexOf(":"))
					+ ":0"
					+ avgdiff.substring(avgdiff.indexOf(":") + 1,
							avgdiff.length());

		if (chklen.length() == 0)
			avgdiff = avgdiff.substring(0, avgdiff.indexOf(":")) + ":00";

		return avgdiff;
	}

	/* to indicate uploaded document present with yellow folder */
	/**
	 * @param file_id
	 * @param Id
	 * @param type
	 * @param ds
	 * @return
	 */
	public static String uploadedFilespresent(String file_id, String Id,
			String type, DataSource ds) {
		String present = "false";
		Statement stmt = null;
		ResultSet rs = null;
		Connection conn = null;

		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select * from dbo.func_lx_uploaded_list( '" + file_id
					+ "' , '" + Id + "' , '" + type + "')";

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				present = "true";
			}
		} catch (Exception e) {
			logger.error(
					"uploadedFilespresent(String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("uploadedFilespresent(String, String, String, DataSource) - Error caught in getting uploadedFilespresent"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"uploadedFilespresent(String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"uploadedFilespresent(String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"uploadedFilespresent(String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		return present;
	}

	/**
	 * @param loginUserId
	 * @param ds
	 * @return
	 */
	public static boolean editContactList(String loginUserId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean editContactLst = false;
		try {
			conn = ds.getConnection();
			StringBuilder sqlQuery = new StringBuilder(
					" select lo_ro_role_desc from lo_poc ");
			sqlQuery.append(" join lo_organization_main on lo_pc_om_id  = lo_om_id ");
			sqlQuery.append(" join lo_organization_top  on lo_ot_id = lo_om_ot_id ");
			sqlQuery.append(" join lo_poc_rol on lo_pc_id=lo_pr_pc_id join lo_role on lo_pr_ro_id=lo_ro_id ");
			sqlQuery.append(" where lo_pc_id=? and lo_ot_type = 'N' and lo_ro_role_desc in('Senior Project Manager','Business Development Manager') ");
			pstmt = conn.prepareStatement(sqlQuery.toString());
			pstmt.setString(1, loginUserId);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				editContactLst = true;
			}

		} catch (Exception e) {
			logger.error("editContactList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("editContactList(String, DataSource) - Error occured  while checking edit contact list or not "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return editContactLst;
	}

}
