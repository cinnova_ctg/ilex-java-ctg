package com.mind.dao.PRM;

import java.util.ArrayList;
public class AssignTeam {
	
	private String pocName = null;
	private ArrayList<String> roleNames = null;
	private String pocId = null;
	private ArrayList<String> roleIds = null;
	private String appendixId = null;
	
	
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getPocId() {
		return pocId;
	}
	public void setPocId(String pocId) {
		this.pocId = pocId;
	}
	public String getPocName() {
		return pocName;
	}
	public void setPocName(String pocName) {
		this.pocName = pocName;
	}
	public ArrayList<String> getRoleNames() {
		return roleNames;
	}
	public void setRoleNames(ArrayList<String> roleNames) {
		this.roleNames = roleNames;
	}
	public ArrayList<String> getRoleIds() {
		return roleIds;
	}
	public void setRoleIds(ArrayList<String> roleIds) {
		this.roleIds = roleIds;
	}
}
