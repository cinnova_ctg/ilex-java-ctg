package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.POWOBean;
import com.mind.common.Decimalroundup;
import com.mind.fw.core.dao.util.DBUtil;

public class POWOdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(POWOdao.class);

	/**
	 * This method gets PO WO List under a Job.
	 * 
	 * @param aid
	 *            String. Job id
	 * @param querystring
	 *            String. sorting parameter, according to which powolist would
	 *            get sorted
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, job Detail list.
	 */

	public static ArrayList getPOWOdetaillist(String jobid, String querystring,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");
		if (querystring.equals("")) {
			querystring = "order by lm_po_partner_name";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from dbo.func_lm_powo_tab(" + jobid + ") "
					+ querystring;
			rs = stmt.executeQuery(sql);
			int i = 0;

			while (rs.next()) {
				POWOBean powoForm = new POWOBean();

				powoForm.setPowoid(rs.getString("lm_po_id"));
				powoForm.setPowono(rs.getString("lm_po_number"));
				powoForm.setPartnerid(rs.getString("lm_po_partner_id"));
				powoForm.setSrccompanyname(rs.getString("lm_po_partner_name"));
				powoForm.setType(rs.getString("lm_po_type"));
				if (rs.getString("lm_po_bulk").equals("y"))
					powoForm.setBulk("Yes");
				else
					powoForm.setBulk("No");
				powoForm.setAuthorizedtotal(Decimalroundup.twodecimalplaces(
						rs.getFloat("lm_po_authorised_total"), 2));
				powoForm.setIssuedate(rs.getString("lm_po_issue_date"));
				powoForm.setDeliverbydate(rs.getString("lm_po_deliver_by_date"));
				powoForm.setTerms(rs.getString("lm_po_terms"));
				powoForm.setMasterpoitem(rs.getString("lm_pwo_type_desc"));

				if (rs.getString("lm_recon_invoice_total") != null)
					powoForm.setInvoicetotal(Decimalroundup.twodecimalplaces(
							rs.getFloat("lm_recon_invoice_total"), 2));
				else
					powoForm.setInvoicetotal(rs
							.getString("lm_recon_invoice_total"));

				if (rs.getString("lm_recon_approved_total") != null)
					powoForm.setApprovedtotal(Decimalroundup.twodecimalplaces(
							rs.getFloat("lm_recon_approved_total"), 2));
				else
					powoForm.setApprovedtotal(rs
							.getString("lm_recon_approved_total"));

				powoForm.setInvoicerecddate(rs
						.getString("lm_recon_inv_recd_date"));
				powoForm.setInvoiceno(rs.getString("lm_recon_inv_no"));

				valuelist.add(i, powoForm);
				i++;
			}

		} catch (Exception e) {
			logger.error("getPOWOdetaillist(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPOWOdetaillist(String, String, DataSource) - Error occured during getting PO WO list  "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getPOWOdetaillist(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getPOWOdetaillist(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getPOWOdetaillist(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static ArrayList getPOWOResourcedetaillist(String powoid,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lm_powo_resource_tab( "
					+ powoid + " )";

			rs = stmt.executeQuery(sql);
			int i = 0;

			while (rs.next()) {
				POWOBean powoForm = new POWOBean();

				powoForm.setPowoid(rs.getString("lm_po_id"));
				powoForm.setCnspartnum(rs.getString("lm_rs_iv_cns_part_number"));
				powoForm.setResourcename(rs.getString("iv_rp_name"));
				powoForm.setResourceqty(Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_quantity"), 3));
				powoForm.setResourcetype(rs.getString("iv_ct_name"));
				powoForm.setResourcemfgpart(rs
						.getString("iv_rp_mfg_part_number"));
				powoForm.setResourceathounitcost(Decimalroundup
						.twodecimalplaces(
								rs.getFloat("lm_pwo_authorised_unit_cost"), 2));
				powoForm.setResourceathototalcost(Decimalroundup
						.twodecimalplaces(
								rs.getFloat("lm_pwo_authorised_total_cost"), 2));
				powoForm.setResourceshowon(rs.getString("lm_pwo_show_on_wo"));
				powoForm.setResourcedropshift(rs
						.getString("lm_pwo_drop_shipped"));
				powoForm.setAut_qty(Decimalroundup.twodecimalplaces(
						rs.getFloat("lm_pwo_authorised_qty"), 3));
				powoForm.setType(rs.getString("lm_po_pwo_type_id"));

				powoForm.setEstimatedunitcost(Decimalroundup.twodecimalplaces(
						rs.getFloat("unit_cost"), 2));
				powoForm.setEstimatedtotalcost(Decimalroundup.twodecimalplaces(
						rs.getFloat("total_cost"), 2));
				// changes made by Seema
				powoForm.setResourcepvssupplied(rs
						.getString("lm_pwo_pvs_supplied"));
				powoForm.setResourcecreditcard(rs
						.getString("lm_pwo_credit_card"));

				valuelist.add(i, powoForm);
				i++;
			}

		} catch (Exception e) {
			logger.error("getPOWOResourcedetaillist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPOWOResourcedetaillist(String, DataSource) - Error occured during getting PO WO resource list  "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getPOWOResourcedetaillist(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getPOWOResourcedetaillist(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getPOWOResourcedetaillist(String, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static int deletePOWO(String Id, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int val = -1;

		try {
			conn = ds.getConnection();

			cstmt = conn.prepareCall("{?=call dbo.lm_powo_delete_01( ?  ) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(Id));

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("deletePOWO(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deletePOWO(String, DataSource) - Exception in DELETING POWO"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sqle) {
				logger.error("deletePOWO(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("deletePOWO(String, DataSource) - Exception in DELETING POWO"
							+ sqle);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error("deletePOWO(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("deletePOWO(String, DataSource) - Exception in DELETING POWO"
							+ sqle);
				}
			}

		}
		return val;
	}

	public static boolean checkInternationalPO(String poId, DataSource ds) {
		boolean checkStatus = false;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select * from lm_purchase_order a inner join lm_po_term_condition b on a.lm_po_tc_id=b.lm_po_tc_id and b.lm_po_tc_type in ('IM','IP') where a.lm_po_id = "
					+ poId;
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				checkStatus = true;
			}
		} catch (Exception e) {
			logger.error("checkInternationalPO(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkInternationalPO(String, DataSource) - Error Ocured at teh time of checking International PO "
						+ e);
			}

		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("checkInternationalPO(String, DataSource) - checkStatus = "
					+ checkStatus);
		}
		return checkStatus;
	}

	public static String checkPOStatus(String poId, DataSource ds) {
		String checkStatus = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select isnull(po_status,0) as po_status from lm_purchase_order where lm_po_id = "
					+ poId;
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				checkStatus = rs.getString("po_status");
			}
		} catch (Exception e) {
			logger.error("checkPOStatus(String, DataSource)", e);

		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("checkPOStatus(String, DataSource) - checkStatus = "
					+ checkStatus);
		}
		return checkStatus;
	}

	public static String getPartnerByPOId(String poId, DataSource ds) {

		String partnerId = "0";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "SELECT isnull(lm_po_partner_id, 0) lm_po_partner_id FROM lm_purchase_order WHERE lm_po_id =  "
					+ poId;
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				partnerId = rs.getString("lm_po_partner_id");
			}
		} catch (Exception e) {
			logger.error("getPartnerByPOId(String, DataSource)", e);

		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getPartnerByPOId(String, DataSource) - checkStatus = "
					+ partnerId);
		}
		return partnerId;

	}

	public static void insertAndDeleteRecords(String powoId,
			String get42CharString, DataSource ds) {

		Connection conn = null;
		PreparedStatement pStmt = null;

		String partnerId = getPartnerByPOId(powoId, ds);

		String query = "DELETE FROM ilex_partner_emails "
				+ " WHERE ilex_pe_pet_id IN (SELECT ilex_pet_id FROM ilex_partner_email_type "
				+ " WHERE ilex_pet_used_in_type = 'lm_purchase_order') "
				+ " AND ilex_pe_used_in = ?  AND ilex_pe_partner_id = ?";

		String sql = "INSERT INTO ilex_partner_emails (ilex_pe_pet_id, ilex_pe_partner_id, ilex_pe_used_in, ilex_pe_unique_id) "
				+ "VALUES (?, ?, ?,?)";
		try {

			conn = ds.getConnection();

			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, powoId);
			pStmt.setString(2, partnerId);
			pStmt.executeUpdate();

			// inserting new recoed into DB.
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, new Integer("3"));
			pStmt.setInt(2, Integer.parseInt(partnerId));
			pStmt.setString(3, powoId);
			pStmt.setString(4, get42CharString);
			pStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}

	}
}
