package com.mind.dao.PRM;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.fw.core.dao.util.DBUtil;
import com.mind.newjobdb.dao.JobDAO;

/**
 * The Class InsuranceRequestDao.
 */
public class InsuranceRequestDao {

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(JobDAO.class);

	/**
	 * Save send action.
	 * 
	 * @param jobId
	 *            the job id
	 * @param loginUser
	 *            the login user
	 * @param ds
	 *            the ds
	 */
	public static void saveSendAction(String jobId, String loginUser,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		int val = -1;
		try {
			conn = ds.getConnection();
			String sql = "insert into lm_insurance_certificate_request_sent "
					+ "(lm_in_job_id,lm_in_date_send,lm_in_user_take_action) values(?,getdate(),?)";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobId);
			pStmt.setString(2, loginUser);
			val = pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error(
					"com.mind.dao.PRM.InsuranceRequestDao.saveSendAction()", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

}
