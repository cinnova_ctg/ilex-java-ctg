package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.SiteChecklistDTO;

public class SiteChecklistdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SiteChecklistdao.class);

	public  static ArrayList getSitechecklist(String siteid, DataSource ds,String fromtype,String jobid)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String sql="";
		ArrayList valuelist=new ArrayList();
		
		
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			//String sql="select  * from lm_checklist_item";	
			if(fromtype.equals("S"))
			{
				sql = "select * from dbo.func_lp_sitechecklist( '"+siteid+"' )";
			}
			else
			{
				sql = "select * from dbo.func_lp_QCchecklist( "+siteid+","+jobid+")";
			}
			
			//System.out.println("XXXgetqcchecklist "+sql);
			rs = stmt.executeQuery(sql);
			int i = 0;
			
			while(rs.next())
			{
			
				SiteChecklistDTO scBean = new SiteChecklistDTO();
								 
				scBean.setChecklistitemid(rs.getString("lm_ci_id"));
				 
				scBean.setItemname(rs.getString("lm_ci_item_name"));
				 
				scBean.setChecklistname(rs.getString("lm_ci_name"));
								 
				scBean.setItemdescription(rs.getString("lm_ci_item_description"));
				scBean.setChecklisttype(rs.getString("lm_ci_type"));
				scBean.setChecklistidtype(rs.getString("lm_ci_type")+rs.getString("lm_ci_id"));
				 
				
				 valuelist.add(i,scBean);
				 i++;		 
			}
			
		}
		catch(Exception e){
			logger.error("getSitechecklist(String, DataSource, String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSitechecklist(String, DataSource, String, String) - Error occured during getting Sitechecklist  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getSitechecklist(String, DataSource, String, String) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getSitechecklist(String, DataSource, String, String) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getSitechecklist(String, DataSource, String, String) - exception ignored", e);
}
			
		}
		return valuelist;	
	}
	
	
	/** This method adds incident details for a partner. 
	 * @param cp_ir_tech_id   			       	    String.technician id
	 * @param cp_ir_partner_id   			       	String.partner id
	 * @param cp_ir_date   			       	    	String.date of incident generated
	 * @param cp_ir_end_customer   			       	String.customer id
	 * @param cp_ir_in_id   			       	    String.incident ids separated by commas
	 * @param cp_ir_other   			       	    String.other comments
	 * @param cp_ir_ia_id   			       	    String.action ids separated by commas
	 * @param cp_ir_created_by   			       	String.userid
	 * @param ds   			       	    			Object of DataSource
	 * @return int              	    			This method returns integer, 0 for success and -ve values for error.
	 */	
		public  static int addChecklistitem(String lm_sc_ci_id, String lm_sc_si_id,String jobid,String lm_sc_user,DataSource ds,String fromtype)
		{
			
			
			Connection conn=null;
			CallableStatement cstmt = null;
			ResultSet rs=null;
			//System.out.println("add cheklist='"+lm_sc_ci_id+"',"+lm_sc_si_id+","+jobid+","+lm_sc_user+","+fromtype);
			//System.out.println("XXXXX addChecklistitem "+lm_sc_ci_id+"~"+lm_sc_si_id+"~"+lm_sc_user);
			int retval=0;
			try{
				conn=ds.getConnection();
				if(fromtype.equals("S"))
				{
					cstmt=conn.prepareCall("{?=call lm_site_checklist_manage_01(?,?,?)}");
					cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
					cstmt.setString(2,lm_sc_ci_id);
					cstmt.setString(3,lm_sc_si_id);
					
					cstmt.setString(4,lm_sc_user);
					cstmt.execute();
					retval = cstmt.getInt(1);
					
				}
				else
				{
					cstmt=conn.prepareCall("{?=call lm_QC_checklist_manage_01(?,?,?,?)}");
					cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
					cstmt.setString(2,lm_sc_ci_id);
					cstmt.setString(3,lm_sc_si_id);
					cstmt.setString(4,jobid);
					cstmt.setString(5,lm_sc_user);
					cstmt.execute();
					retval = cstmt.getInt(1);
				}
				
				
				
			}
			catch(Exception e){
			logger.error("addChecklistitem(String, String, String, String, DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addChecklistitem(String, String, String, String, DataSource, String) - Error occured during adding Checklistitem" + e);
			}
			}
			

			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("addChecklistitem(String, String, String, String, DataSource, String) - exception ignored", e);
}
				
				
				try
				{
					if ( cstmt!= null ) 
						cstmt.close();
				}
				catch( Exception e )
				{
				logger.warn("addChecklistitem(String, String, String, String, DataSource, String) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("addChecklistitem(String, String, String, String, DataSource, String) - exception ignored", e);
}
				
			}
			return retval;		
				
				
		}
		
		
		
		/** This method gets Partner name. 
		 * @param pid   			       	String.partner id
		 * @param ds   			       	    Object of DataSource
		 * @return String              	    This method returns String,partner name.
		 */	
			public  static String getSiteid(String jobid,DataSource ds)
			{
				
				Connection conn=null;
				Statement stmt=null;
				ResultSet rs=null;
				String siteid="";
				
				try{
					conn=ds.getConnection();
					stmt=conn.createStatement();
					
					String sql="select lm_js_si_id from lm_job where lm_js_id="+jobid;						   		
					rs = stmt.executeQuery(sql);
					
					if(rs.next())
					{
						
						siteid=rs.getString("lm_js_si_id");
						
					}
					
				}
				catch(Exception e){
			logger.error("getSiteid(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteid(String, DataSource) - Error occured during getting Siteid ");
			}
				}
				
				finally
				{
					try
					{
						if ( rs!= null ) 
						{
							rs.close();		
						}
					}
					
					catch( Exception e )
					{
				logger.warn("getSiteid(String, DataSource) - exception ignored", e);
}
					
					
					try
					{
						if ( stmt!= null ) 
							stmt.close();
					}
					catch( Exception e )
					{
				logger.warn("getSiteid(String, DataSource) - exception ignored", e);
}
					
					
					try
					{
						if ( conn!= null) 
							conn.close();
					}
					catch( Exception e )
					{
				logger.warn("getSiteid(String, DataSource) - exception ignored", e);
}
					
				}
				return siteid;	
			}
			
			
			
			/** This method gets Partner name. 
			 * @param pid   			       	String.partner id
			 * @param ds   			       	    Object of DataSource
			 * @return String              	    This method returns String,partner name.
			 */	
				public  static String getSitename(String siteid,DataSource ds,String fromtype)
				{
					
					Connection conn=null;
					Statement stmt=null;
					ResultSet rs=null;
					String sitename="";
					
					try{
						conn=ds.getConnection();
						stmt=conn.createStatement();
						
						if(fromtype.equals("S"))
						{
							String sql="select lm_si_name from lm_site_detail where lm_si_id="+siteid;						   		
							rs = stmt.executeQuery(sql);
							
							if(rs.next())
							{
								
								sitename=rs.getString("lm_si_name");
								
							}
						}
						else
						{
							String sql="select dbo.func_cc_name(rtrim("+siteid+"),'Partner')";						   		
							rs = stmt.executeQuery(sql);
							
							if(rs.next())
							{
								
								sitename=rs.getString(1);
								
							}
						}
						
					}
					catch(Exception e){
			logger.error("getSitename(String, DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSitename(String, DataSource, String) - Error occured during getting Sitename ");
			}
					}
					
					finally
					{
						try
						{
							if ( rs!= null ) 
							{
								rs.close();		
							}
						}
						
						catch( Exception e )
						{
				logger.warn("getSitename(String, DataSource, String) - exception ignored", e);
}
						
						
						try
						{
							if ( stmt!= null ) 
								stmt.close();
						}
						catch( Exception e )
						{
				logger.warn("getSitename(String, DataSource, String) - exception ignored", e);
}
						
						
						try
						{
							if ( conn!= null) 
								conn.close();
						}
						catch( Exception e )
						{
				logger.warn("getSitename(String, DataSource, String) - exception ignored", e);
}
						
					}
					return sitename;	
				}
				
				
				
				public  static ArrayList getSavedchecklist(String itemidlist, DataSource ds,String fromtype)
				{
					
					Connection conn=null;
					Statement stmt=null;
					ResultSet rs=null;
					String sql = "";
					ArrayList valuelist=new ArrayList();
					
					
					
					try{
						conn=ds.getConnection();
						stmt=conn.createStatement();
						
						if(fromtype.equals("S"))
						{
							sql="select  * from lm_checklist_item where lm_ci_id in("+itemidlist+")";	
						}
						else
						{
							sql="select  * from lm_qc_checklist_item where lm_ci_id in("+itemidlist+")";
						}
												   		
						
						
						
						rs = stmt.executeQuery(sql);
						int i=0;
						
						while(rs.next())
						{
						
							SiteChecklistDTO scBean = new SiteChecklistDTO();
											 
							scBean.setChecklistitemid(rs.getString("lm_ci_id"));
							 
							scBean.setItemname(rs.getString("lm_ci_item_name"));
							 
							scBean.setChecklistname(rs.getString("lm_ci_name"));
											 
							scBean.setItemdescription(rs.getString("lm_ci_item_description"));
							 
							
							 valuelist.add(i,scBean);
							 i++;		 
						}
						
					}
					catch(Exception e){
			logger.error("getSavedchecklist(String, DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSavedchecklist(String, DataSource, String) - Error occured during getting Savedchecklist  ");
			}
					}
					
					finally
					{
						try
						{
							if ( rs!= null ) 
							{
								rs.close();		
							}
						}
						
						catch( Exception e )
						{
				logger.warn("getSavedchecklist(String, DataSource, String) - exception ignored", e);
}
						
						
						try
						{
							if ( stmt!= null ) 
								stmt.close();
						}
						catch( Exception e )
						{
				logger.warn("getSavedchecklist(String, DataSource, String) - exception ignored", e);
}
						
						
						try
						{
							if ( conn!= null) 
								conn.close();
						}
						catch( Exception e )
						{
				logger.warn("getSavedchecklist(String, DataSource, String) - exception ignored", e);
}
						
					}
					return valuelist;	
				}
				
				
				
					public  static String[] getcheckedsiteitem(String siteid,String jobid,DataSource ds,String fromtype)
					{
						
						Connection conn=null;
						Statement stmt=null;
						ResultSet rs1=null;
						ResultSet rs=null;
						String[] siteitemid=null;
						int count=0;
						
						try{
							conn=ds.getConnection();
							stmt=conn.createStatement();
							if(fromtype.equals("S"))
							{
								String sql1="SELECT * from lm_site_checklist where lm_sc_si_id="+siteid;	
								//System.out.println("XXXX--sql1"+sql1);
								rs1 = stmt.executeQuery(sql1);
								while(rs1.next())
								{
									count++;
								}
								
								String sql="select lm_sc_ci_id ,lm_sc_ci_type from lm_site_checklist where lm_sc_si_id="+siteid;	
								//System.out.println("XXXX--sql"+sql);
								rs = stmt.executeQuery(sql);
								int i=0;
																						
								if(count>0)
								{
									
									siteitemid = new String[count];
									while(rs.next())
									{
										siteitemid[i]=rs.getString("lm_sc_ci_type")+rs.getString("lm_sc_ci_id");
										i++;
									}
									
								}
								else
								{
									siteitemid = new String[1];
									siteitemid[0] = "";
								}
							}
							else
							{	
								String sql1="SELECT * from lm_qc_checklist where lm_qc_partner_id="+siteid+"and lm_qc_js_id="+jobid;	
								//System.out.println("XXXX--sql1"+sql1);
								rs1 = stmt.executeQuery(sql1);
								while(rs1.next())
								{
									count++;
								}
								
								String sql="select lm_qc_ci_id,lm_qc_ci_type from lm_qc_checklist where lm_qc_partner_id="+siteid+"and lm_qc_js_id="+jobid;						   		
								//System.out.println("XXXX--sql"+sql);
								rs = stmt.executeQuery(sql);
								int i=0;
																						
								if(count>0)
								{
									
									siteitemid = new String[count];
									while(rs.next())
									{
										siteitemid[i]=rs.getString("lm_qc_ci_type")+rs.getString("lm_qc_ci_id");
										i++;
									}
									
								}
								else
								{
									siteitemid = new String[1];
									siteitemid[0] = "";
								}
								
							}
								
							
							
							
							
						}
						catch(Exception e){
			logger.error("getcheckedsiteitem(String, String, DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getcheckedsiteitem(String, String, DataSource, String) - Error occured during getting getcheckedsiteitem ");
			}
						}
						
						finally
						{
							try
							{
								if ( rs!= null ) 
								{
									rs.close();		
								}
							}
							
							catch( Exception e )
							{
				logger.warn("getcheckedsiteitem(String, String, DataSource, String) - exception ignored", e);
}
							
							try
							{
								if ( rs1!= null ) 
								{
									rs1.close();		
								}
							}
							
							catch( Exception e )
							{
				logger.warn("getcheckedsiteitem(String, String, DataSource, String) - exception ignored", e);
}
							
							
							try
							{
								if ( stmt!= null ) 
									stmt.close();
							}
							catch( Exception e )
							{
				logger.warn("getcheckedsiteitem(String, String, DataSource, String) - exception ignored", e);
}
							
							
							try
							{
								if ( conn!= null) 
									conn.close();
							}
							catch( Exception e )
							{
				logger.warn("getcheckedsiteitem(String, String, DataSource, String) - exception ignored", e);
}
							
						}
						return siteitemid;	
					}
					
		
}
