package com.mind.dao.PRM;

public class PRMAddendumList {

	private String lx_addendum_id = "";
	private String lx_addendum_name = "";
	private String lx_addendum_create_date = "";
	private String customer_poc_id = "";
	private String customer_poc_fname = "";
	private String customer_poc_lname = "";
	private String customer_poc_phone = "";
	private String customer_poc_email = "";
	private String lx_addendum_status = "";
	
	private String lx_addendum_used_in = "";
	private String title = ""; 
	private String lx_se_start = "";
	private String lx_se_end = "";
	private String lx_ce_total_cost = ""; 
	private String lx_ce_total_price = ""; 
	private String lx_ce_margin = ""; 
	private String lx_addendum_det_status = "";
		
	private String lx_addendum_type = "";
	
	private String addendumtype = null;
	
	public String getAddendumtype() {
		return addendumtype;
	}
	public void setAddendumtype(String addendumtype) {
		this.addendumtype = addendumtype;
	}
	public String getCustomer_poc_email() {
		return customer_poc_email;
	}
	public void setCustomer_poc_email(String customer_poc_email) {
		this.customer_poc_email = customer_poc_email;
	}
	public String getCustomer_poc_id() {
		return customer_poc_id;
	}
	public void setCustomer_poc_id(String customer_poc_id) {
		this.customer_poc_id = customer_poc_id;
	}
	public String getCustomer_poc_phone() {
		return customer_poc_phone;
	}
	public void setCustomer_poc_phone(String customer_poc_phone) {
		this.customer_poc_phone = customer_poc_phone;
	}
	public String getLx_addendum_id() {
		return lx_addendum_id;
	}
	public void setLx_addendum_id(String lx_addendum_id) {
		this.lx_addendum_id = lx_addendum_id;
	}
	public String getLx_addendum_name() {
		return lx_addendum_name;
	}
	public void setLx_addendum_name(String lx_addendum_name) {
		this.lx_addendum_name = lx_addendum_name;
	}
	public String getLx_addendum_status() {
		return lx_addendum_status;
	}
	public void setLx_addendum_status(String lx_addendum_status) {
		this.lx_addendum_status = lx_addendum_status;
	}
	public String getLx_addendum_create_date() {
		return lx_addendum_create_date;
	}
	public void setLx_addendum_create_date(String lx_addendum_create_date) {
		this.lx_addendum_create_date = lx_addendum_create_date;
	}
	public String getCustomer_poc_fname() {
		return customer_poc_fname;
	}
	public void setCustomer_poc_fname(String customer_poc_fname) {
		this.customer_poc_fname = customer_poc_fname;
	}
	public String getCustomer_poc_lname() {
		return customer_poc_lname;
	}
	public void setCustomer_poc_lname(String customer_poc_lname) {
		this.customer_poc_lname = customer_poc_lname;
	}
	public String getLx_addendum_det_status() {
		return lx_addendum_det_status;
	}
	public void setLx_addendum_det_status(String lx_addendum_det_status) {
		this.lx_addendum_det_status = lx_addendum_det_status;
	}
	public String getLx_addendum_used_in() {
		return lx_addendum_used_in;
	}
	public void setLx_addendum_used_in(String lx_addendum_used_in) {
		this.lx_addendum_used_in = lx_addendum_used_in;
	}
	public String getLx_ce_margin() {
		return lx_ce_margin;
	}
	public void setLx_ce_margin(String lx_ce_margin) {
		this.lx_ce_margin = lx_ce_margin;
	}
	public String getLx_ce_total_cost() {
		return lx_ce_total_cost;
	}
	public void setLx_ce_total_cost(String lx_ce_total_cost) {
		this.lx_ce_total_cost = lx_ce_total_cost;
	}
	public String getLx_ce_total_price() {
		return lx_ce_total_price;
	}
	public void setLx_ce_total_price(String lx_ce_total_price) {
		this.lx_ce_total_price = lx_ce_total_price;
	}
	public String getLx_se_end() {
		return lx_se_end;
	}
	public void setLx_se_end(String lx_se_end) {
		this.lx_se_end = lx_se_end;
	}
	public String getLx_se_start() {
		return lx_se_start;
	}
	public void setLx_se_start(String lx_se_start) {
		this.lx_se_start = lx_se_start;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public String getLx_addendum_type() {
		return lx_addendum_type;
	}
	public void setLx_addendum_type( String lx_addendum_type ) {
		this.lx_addendum_type = lx_addendum_type;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
