package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.SiteHistoryBean;
import com.mind.bean.prm.SiteHistoryDTO;
import com.mind.common.Decimalroundup;


public class SiteHistorydao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SiteHistorydao.class);
	
	/**
	 * @author amitm
	 * This method will get Site History for Standard Appendix.
	 * @param siteId
	 * @param sortqueryclause
	 * @param ds
	 * @return
	 */
	
	public static ArrayList getSiteHistory(String siteId,String sortqueryclause,
			 DataSource ds) {
		
		//System.out.println("In Sitehistory dao ......getSiteHistory,lp_site_history_tab_01, siteId is "+siteId);
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int val = 0;
		ArrayList valuelist=new ArrayList();
		DecimalFormat dfcurmargin	= new DecimalFormat( "0.00" );
		
		if(siteId.equals(""))
		{
			siteId = "0";
		}
		if( sortqueryclause.equals( "" ) )              
		{
			sortqueryclause = "order by lm_js_planned_start_date";
		}
		
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lp_site_history_tab_01(? ,?)}");
			
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(siteId));
			cstmt.setString(3,sortqueryclause);

			rs = cstmt.executeQuery();

			int i = 0;

			while (rs.next()) {
				SiteHistoryBean siteHistoryBean = new SiteHistoryBean();
				
				siteHistoryBean.setOwner(rs.getString("job_created_by"));
				siteHistoryBean.setCustomerName(rs.getString("lo_ot_name"));
				siteHistoryBean.setAppendixName(rs.getString("lx_pr_title"));
				siteHistoryBean.setJobName(rs.getString("jobname"));
				
				siteHistoryBean.setActualstartdate(rs.getString("lx_se_start"));
				siteHistoryBean.setActualend(rs.getString("lx_se_end"));
				siteHistoryBean.setActualstartdatetime(rs.getString("lx_se_start_time"));
				
				siteHistoryBean.setStatus(rs.getString("lm_js_status"));
				siteHistoryBean.setPartnerName(rs.getString("partner_name"));
				
				
				siteHistoryBean.setSchedulestartdate(rs.getString("lm_js_planned_start_date"));
				siteHistoryBean.setSchedulecomplete(rs.getString("lm_js_planned_end_date"));
				siteHistoryBean.setSchedulestarttime(rs.getString("lx_act_start_time"));
				
			
				siteHistoryBean.setEstimatedcost(Decimalroundup.twodecimalplaces(rs.getFloat("lx_ce_total_cost") , 2 ));
				siteHistoryBean.setExtendedprice(Decimalroundup.twodecimalplaces(rs.getFloat("lx_ce_total_price"),2));
				siteHistoryBean.setProformavgp(dfcurmargin.format(rs.getFloat("lx_ce_margin")));
				siteHistoryBean.setActualcost(Decimalroundup.twodecimalplaces(rs.getFloat("lx_ce_actual_cost"),2));
				siteHistoryBean.setActualvgp(Decimalroundup.twodecimalplaces(rs.getFloat("lx_ce_actual_vgp"),2));
				
				
				valuelist.add(i,siteHistoryBean);
				i++;
			}
		}

		catch (Exception e) {
			logger.error("getSiteHistory(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteHistory(String, String, DataSource) - Error occured during getting site History " + e);
			}
			logger.error("getSiteHistory(String, String, DataSource)", e);
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
					rs = null;
				}
			}

			catch (Exception e) {
				logger.warn("getSiteHistory(String, String, DataSource) - exception ignored", e);
			}

			try {
				if (cstmt != null)
				{
					cstmt.close();
					cstmt  = null;
				}	
			} catch (Exception e) {
				logger.warn("getSiteHistory(String, String, DataSource) - exception ignored", e);
			}

			try {
				if (conn != null)
				{
					conn.close();
					conn = null;
				}
			} catch (Exception e) {
				logger.warn("getSiteHistory(String, String, DataSource) - exception ignored", e);
			}

		}
		return valuelist;
	}

	/**
	 * Following method will show site detail on the basis of jobId
	 * @author amitm
	 * @param siteform
	 * @param ds
	 * @return
	 * @throws Exception
	 */
	public static SiteHistoryDTO getSite ( SiteHistoryDTO siteform , DataSource ds ) throws Exception
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select * from dbo.func_lp_jobsite_tab( '"+siteform.getJobid()+"' )";
			

			rs = stmt.executeQuery( sql );
			
			if( rs.next() )
			{

				siteform.setSite_name( rs.getString( 2 ) );	
				siteform.setLocality_uplift( rs.getString( 3 ) );
				siteform.setUnion_site( rs.getString( 4 ) );

				siteform.setSite_worklocation( rs.getString( 6 ) );
				
				siteform.setSite_address( rs.getString( 7 ) );
				siteform.setSite_city( rs.getString( 8 ) );
				siteform.setSite_state( rs.getString( 9 ) );
				siteform.setSite_zipcode(rs.getString( 10 ) );
				siteform.setSite_country( rs.getString( 11 ) );
				siteform.setSite_direction( rs.getString( 12 ) );
				
				siteform.setPrimary_Name(rs.getString( 13 ));
				siteform.setSecondary_Name(rs.getString( 14 ));
				
				siteform.setSite_notes( rs.getString( 15 ) );
				siteform.setSite_number(rs.getString( 18 ));
				siteform.setSite_phone(rs.getString( 19 ));//For Primary Phone

				siteform.setSecondary_phone(rs.getString( 20 ));//For Secondary Phone
				siteform.setPrimary_email(rs.getString( 21 ));
				siteform.setSecondary_email(rs.getString( 22 ));
							

			}
			
		}
		catch( Exception e )
		{
			logger.error("getSite(SiteHistoryForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSite(SiteHistoryForm, DataSource) - Error in retrieving site information for a job" + e);
			}
			logger.error("getSite(SiteHistoryForm, DataSource)", e);
		}
		finally
		{
			
			try
			{
				if( rs != null )
				{
					rs.close();
				}
			}
			catch( SQLException s )
			{
				logger.error("getSite(SiteHistoryForm, DataSource)", s);

				if (logger.isDebugEnabled()) {
					logger.debug("getSite(SiteHistoryForm, DataSource) - Exception in retrieving site" + s);
				}
				logger.error("getSite(SiteHistoryForm, DataSource)", s);
			}
			
			try
			{
				if( stmt!= null )
				{
					stmt.close();
				}
			}
			catch( SQLException s )
			{
				logger.error("getSite(SiteHistoryForm, DataSource)", s);

				if (logger.isDebugEnabled()) {
					logger.debug("getSite(SiteHistoryForm, DataSource) - Exception in retrieving site" + s);
				}
				logger.error("getSite(SiteHistoryForm, DataSource)", s);
			}
			
			
			try
			{
				if( conn!= null ) 
				{
					conn.close();
				}
			}
			catch( SQLException s )
			{
				logger.error("getSite(SiteHistoryForm, DataSource)", s);

				if (logger.isDebugEnabled()) {
					logger.debug("getSite(SiteHistoryForm, DataSource) - Exception in retrieving site" + s);
				}
				logger.error("getSite(SiteHistoryForm, DataSource)", s);
			}
			
		}
		return siteform;
	}
	
	/**
	 * @author amitm
	 * This Method will get all site History for NetMedx. 
	 * @param siteId
	 * @param sortqueryclause
	 * @param ds
	 * @return
	 */

	public static ArrayList getSiteHistoryNetMedx(String siteId,String sortqueryclause,
			 DataSource ds) {
		
		//System.out.println(" sitehistorydao $$$$$$$$$$getSiteHistoryNetMedx  lp_site_history_NetMedx_ticket_tab,siteId is"+siteId);
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int val = 0;
		ArrayList valuelist=new ArrayList();
		DecimalFormat dfcurmargin	= new DecimalFormat( "0.00" );
		DecimalFormat dfcur	= new DecimalFormat( "###0.00" );
		
		
		if( sortqueryclause.equals( "" ) )              
		{
			sortqueryclause = "order by lm_tc_requested_date";
		}
		
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lp_site_history_NetMedx_ticket_tab(? ,?)}");
			
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(siteId));
			cstmt.setString(3,sortqueryclause);

			rs = cstmt.executeQuery();

			int i = 0;

			while (rs.next()) {
				SiteHistoryBean siteHistoryBean = new SiteHistoryBean();
				
				siteHistoryBean.setOwner(rs.getString("job_created_by"));
				siteHistoryBean.setCustomerName(rs.getString("lo_ot_name"));
				siteHistoryBean.setAppendixName(rs.getString("lx_pr_title"));
				siteHistoryBean.setJobName(rs.getString("jobname"));
				
				siteHistoryBean.setMsp(rs.getString("lm_tc_msp"));
				siteHistoryBean.setCriticality(rs.getString("lo_call_criticality_des"));
				
				
				siteHistoryBean.setStatus(rs.getString("lm_js_status"));
				siteHistoryBean.setPartnerName(rs.getString("partner_name"));
				
				siteHistoryBean.setHourlyCost(dfcur.format( rs.getFloat( "hourly_cost" )));
				siteHistoryBean.setHourlyPrice(dfcur.format( rs.getFloat( "hourly_price" )));
				siteHistoryBean.setHourlyVGP(dfcur.format( rs.getFloat( "hourly_vgp" )));
				siteHistoryBean.setRequestReceivedDate_Ticket(rs.getString( "lm_tc_requested_date" ));	
				siteHistoryBean.setRequestReceivedTime_Ticket(rs.getString("lm_tc_requested_time"));
				siteHistoryBean.setScheduledArrivalDate_Ticket(rs.getString("lm_tc_preferred_arrival"));
				siteHistoryBean.setScheduleArrivalTime_Ticket(rs.getString("lm_tc_preferred_arrival_time"));
				siteHistoryBean.setResource(rs.getString("resource_level"));
				siteHistoryBean.setStandby(rs.getString("lm_tc_standby"));
				
				valuelist.add(i,siteHistoryBean);
				i++;
			}
		}

		catch (Exception e) {
			logger.error("getSiteHistoryNetMedx(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteHistoryNetMedx(String, String, DataSource) - Error occured during getting site History " + e);
			}
			logger.error("getSiteHistoryNetMedx(String, String, DataSource)", e);
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
					rs = null;
				}
			}

			catch (Exception e) {
				logger.warn("getSiteHistoryNetMedx(String, String, DataSource) - exception ignored", e);
			}

			try {
				if (cstmt != null)
				{
					cstmt.close();
					cstmt  = null;
				}	
			} catch (Exception e) {
				logger.warn("getSiteHistoryNetMedx(String, String, DataSource) - exception ignored", e);
			}

			try {
				if (conn != null)
				{
					conn.close();
					conn = null;
				}
			} catch (Exception e) {
				logger.warn("getSiteHistoryNetMedx(String, String, DataSource) - exception ignored", e);
			}

		}
		return valuelist;
	}
}
