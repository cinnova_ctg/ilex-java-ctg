package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.fw.core.dao.util.DBUtil;

public class CustomerInformationdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(CustomerInformationdao.class);

	/**
	 * This method gets Appendix Name.
	 * 
	 * @param appendixid
	 *            String. Appendix id
	 * @param ds
	 *            Object of DataSource
	 * @return String This method returns String, Appendix Name.
	 */
	public static String getAppendixname(String appendixid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String appendixname = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lx_pr_title from lx_appendix_main where lx_pr_id="
					+ appendixid;

			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				appendixname = rs.getString("lx_pr_title");
			}

		} catch (Exception e) {
			logger.error("getAppendixname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixname(String, DataSource) - Error occured during getting Appendix name  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

		}
		return appendixname;
	}

	public static String[] getCustinfo(String appendixid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] custinfodetail = new String[6];
		int i = 0;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_ci_parameter,lm_ci_value from lm_customer_info where lm_ci_js_id="
					+ appendixid;
			// System.out.println("XXxSql"+sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				if (rs.getString("lm_ci_parameter") != null) {
					custinfodetail[i] = rs.getString("lm_ci_parameter");
				} else {
					custinfodetail[i] = "";
				}
				i++;
				if (rs.getString("lm_ci_value") != null) {
					custinfodetail[i] = rs.getString("lm_ci_value");
				} else {
					custinfodetail[i] = "";
				}
				i++;

			}

			for (int k = 5; k >= i; k--) {
				custinfodetail[k] = "";

			}

		} catch (Exception e) {
			logger.error("getCustinfo(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustinfo(String, DataSource) - Error occured during getting getCustinfo ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCustinfo(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCustinfo(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCustinfo(String, DataSource) - exception ignored",
						e);
			}

		}
		return custinfodetail;
	}

	public static int addCustInfo(String lm_ci_pr_id, String parastring,
			String valuestring, String user, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		int retval = -1;
		try {
			conn = ds.getConnection();
			conn.setAutoCommit(false);

			cstmt = conn
					.prepareCall("{?=call lm_customer_info_manage_01(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, lm_ci_pr_id);
			cstmt.setString(3, parastring);
			cstmt.setString(4, valuestring);
			cstmt.setString(5, user);

			cstmt.execute();
			retval = cstmt.getInt(1);
			conn.commit();

		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				logger.error(e);
			}
			logger.error(
					"addCustInfo(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addCustInfo(String, String, String, String, DataSource) - Error occured during adding addCustInfo"
						+ e);
			}
			throw new RuntimeException(e);
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"addCustInfo(String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"addCustInfo(String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"addCustInfo(String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retval;

	}

	public static String[] getCustparameterlist(String appendixid,
			String jobid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] custinfoparaid = new String[30];
		String sql = "";
		int i = 0;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (Appendixdao.checkForSnapon(appendixid, ds))
				sql = "select * from dbo.func_lm_customer_info_snapon ("
						+ appendixid + "," + jobid + ")";
			else
				sql = "select * from dbo.func_lm_customer_info (" + appendixid
						+ "," + jobid + ")";

			// System.out.println("XXxSql"+sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				custinfoparaid[i] = rs.getString("lm_ap_cust_info_id");
				i++;
				custinfoparaid[i] = rs.getString("lm_ap_cust_info_parameter");
				i++;
				custinfoparaid[i] = rs.getString("lm_ci_value");
				i++;

			}

		} catch (Exception e) {
			logger.error("getCustparameterlist(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustparameterlist(String, String, DataSource) - Error occured during getting getCustparameterlist ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCustparameterlist(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCustparameterlist(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCustparameterlist(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return custinfoparaid;
	}

	/**
	 * Description: Get Customer Required Data/Reporting.
	 * 
	 * @param appendixid
	 *            - Appendix Id
	 * @param jobid
	 *            - Job Id
	 * @param ds
	 *            - Database Object
	 * @return ArrayList - requiredDataList
	 */
	public static ArrayList<CustomerInfoBean> getCustomerRequiredData(
			String appendixId, String jobId, DataSource ds) {

		logger.debug("getCustomerRequiredData(String, String, DataSource) - appendixId="
				+ appendixId);
		logger.debug("getCustomerRequiredData(String, String, DataSource) - jobId="
				+ jobId);

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList<CustomerInfoBean> requiredDataList = new ArrayList<CustomerInfoBean>();
		String sql = "";
		int i = 0;
		try {
			conn = ds.getConnection();
			if (Appendixdao.checkForSnapon(appendixId, ds))
				sql = "select * from dbo.func_lm_customer_info_snapon (?,?)";
			else
				sql = "select * from dbo.func_lm_customer_info (?,?)";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, appendixId);
			pStmt.setString(2, jobId);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				if (rs.getString("lm_ap_cust_info_parameter") != null
						&& !rs.getString("lm_ap_cust_info_parameter").trim()
								.equals("")) {
					CustomerInfoBean customerInfoBean = new CustomerInfoBean();
					customerInfoBean.setInfoId(rs
							.getString("lm_ap_cust_info_id"));
					customerInfoBean.setInfoParameter(rs
							.getString("lm_ap_cust_info_parameter"));
					customerInfoBean.setValue(rs.getString("lm_ci_value"));
					requiredDataList.add(i, customerInfoBean);
					i++;
				}

			} // - end of while
		} // - end of try
		catch (Exception e) {
			logger.error("getCustomerRequiredData(String, String, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return requiredDataList;

	}// - end of getCustomerRequiredData()

	public static String getJobname(String Job_Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String jobname = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_js_title from lm_job where lm_js_id=" + Job_Id;
			rs = stmt.executeQuery(sql);
			// System.out.println("---job name sql"+sql);
			if (rs.next())
				jobname = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getJobname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobname(String, DataSource) - Error Occured in retrieving jobname"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getJobname(String, DataSource) - exception ignored", s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getJobname(String, DataSource) - exception ignored", s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getJobname(String, DataSource) - exception ignored", s);
			}

		}
		return jobname;
	}

	/*
	 * public static ArrayList getCustparameterlistgetCustparameterlist(String
	 * appendixid,String jobid,DataSource ds) {
	 * 
	 * Connection conn=null; Statement stmt=null; ResultSet rs=null; ArrayList
	 * valuelist=new ArrayList(); DecimalFormat dfcurmargin = new DecimalFormat(
	 * "0.00" );
	 * 
	 * 
	 * try{ conn=ds.getConnection(); stmt=conn.createStatement();
	 * 
	 * String
	 * sql="select * from dbo.func_lm_customer_info ("+appendixid+","+jobid+")";
	 * System.out.println("sql func_lm_customer_info "+sql); rs =
	 * stmt.executeQuery(sql); int i=0;
	 * 
	 * while(rs.next()) {
	 * 
	 * CustomerInformationForm ahForm = new CustomerInformationForm();
	 * 
	 * ahForm.setAppendixcustinfoid(rs.getString("lm_ap_cust_info_id"));
	 * 
	 * ahForm.setCustParameter1(rs.getString("lm_ap_cust_info_parameter"));
	 * 
	 * valuelist.add(i,ahForm); i++; }
	 * 
	 * } catch(Exception e){
	 * System.out.println("Error occured during getting getJobdetaillist  "); }
	 * 
	 * finally { try { if ( rs!= null ) { rs.close(); } }
	 * 
	 * catch( Exception e ) {}
	 * 
	 * 
	 * try { if ( stmt!= null ) stmt.close(); } catch( Exception e ) {}
	 * 
	 * 
	 * try { if ( conn!= null) conn.close(); } catch( Exception e ) {}
	 * 
	 * } return valuelist; }
	 */

}
