package com.mind.dao.PRM;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.JobNotesBean;
import com.mind.fw.core.dao.util.DBUtil;

public class JobNotesDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JobNotesDao.class);

	public void manipulateNotes(JobNotesBean jobNotesForm, DataSource ds,
			String loginUser) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Timestamp ts = new java.sql.Timestamp(
					new java.util.Date().getTime());
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "update dbo.prj_lvl_job_notes" + "\n set notes = ?,"
					+ "\n updated_by = ?," + "\n updated_date = ?"
					+ "\n where appendix_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobNotesForm.getNotes());
			pStmt.setString(2, loginUser);
			pStmt.setTimestamp(3, ts);
			pStmt.setLong(4, new Long(jobNotesForm.getAppendixId()).longValue());
			int isUpdate = pStmt.executeUpdate();
			if (logger.isDebugEnabled()) {
				logger.debug("manipulateNotes(JobNotesForm, DataSource, String) - isupdate"
						+ isUpdate);
			}
			// int isUpdate = stmt.executeUpdate(sql);
			if (isUpdate <= 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("manipulateNotes(JobNotesForm, DataSource, String) - in tis");
				}
				sql = "insert into dbo.prj_lvl_job_notes"
						+ "\n (appendix_id,notes,created_by,created_date)"
						+ " \n values (?,?,?,?)";

				pStmt = conn.prepareStatement(sql);
				pStmt.setLong(1,
						new Long(jobNotesForm.getAppendixId()).longValue());
				pStmt.setString(2, jobNotesForm.getNotes());
				pStmt.setString(3, loginUser);
				pStmt.setTimestamp(4, ts);
				pStmt.executeUpdate();
			}
		} catch (Exception e) {
			logger.error("manipulateNotes(JobNotesForm, DataSource, String)", e);

			logger.error("manipulateNotes(JobNotesForm, DataSource, String)", e);
		} finally {
			DBUtil.close(stmt);
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

	public void manipulateJobNotes(JobNotesBean jobNotesForm, DataSource ds,
			String loginUser) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Timestamp ts = new java.sql.Timestamp(
					new java.util.Date().getTime());
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "update dbo.job_lvl_job_notes" + "\n set notes = ?,"
					+ "\n updated_by = ?," + "\n updated_date = ?"
					+ "\n where job_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobNotesForm.getNotes());
			pStmt.setString(2, loginUser);
			pStmt.setTimestamp(3, ts);
			pStmt.setLong(4, new Long(jobNotesForm.getJobId()).longValue());
			int isUpdate = pStmt.executeUpdate();
			if (logger.isDebugEnabled()) {
				logger.debug("manipulateJobNotes(JobNotesForm, DataSource, String) - isupdate"
						+ isUpdate);
			}
			// int isUpdate = stmt.executeUpdate(sql);
			if (isUpdate <= 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("manipulateJobNotes(JobNotesForm, DataSource, String) - in tis");
				}
				sql = "insert into dbo.job_lvl_job_notes"
						+ "\n (job_id,notes,created_by,created_date)"
						+ " \n values (?,?,?,?)";

				pStmt = conn.prepareStatement(sql);
				pStmt.setLong(1, new Long(jobNotesForm.getJobId()).longValue());
				pStmt.setString(2, jobNotesForm.getNotes());
				pStmt.setString(3, loginUser);
				pStmt.setTimestamp(4, ts);
				pStmt.executeUpdate();
			}
		} catch (Exception e) {
			logger.error(
					"manipulateJobNotes(JobNotesForm, DataSource, String)", e);

			logger.error(
					"manipulateJobNotes(JobNotesForm, DataSource, String)", e);
		} finally {
			DBUtil.close(stmt);
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

	public void fetchProjectNotes(JobNotesBean jobNotesForm, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select * from dbo.prj_lvl_job_notes where appendix_id=? ";

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobNotesForm.getAppendixId());
			rs = pStmt.executeQuery();
			if (rs.next()) {
				jobNotesForm.setAppendixId(rs.getString("appendix_id"));
				jobNotesForm.setNotes(rs.getString("notes"));
			}

		} catch (Exception e) {
			logger.error("fetchProjectNotes(JobNotesForm, DataSource)", e);

			logger.error("fetchProjectNotes(JobNotesForm, DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);

		}
	}

	public void fetchJobNotes(JobNotesBean jobNotesForm, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select * from dbo.job_lvl_job_notes where job_id=? ";

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobNotesForm.getJobId());
			rs = pStmt.executeQuery();
			if (rs.next()) {
				jobNotesForm.setAppendixId(rs.getString("job_id"));
				jobNotesForm.setNotes(rs.getString("notes"));
			}

		} catch (Exception e) {
			logger.error("fetchJobNotes(JobNotesForm, DataSource)", e);

			logger.error("fetchJobNotes(JobNotesForm, DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);

		}
	}

	public static void getAppendixInformation(String appendixId,
			JobNotesBean jobNotesForm, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select lam.lx_pr_id,lam.lx_pr_title,lmn.lp_mm_id,lot.lo_ot_name from lp_msa_main lmn join lo_organization_top lot on"
					+ " \n lp_mm_ot_id = lo_ot_id "
					+ " \n join lx_appendix_main lam on"
					+ " \n lmn.lp_mm_id = lam.lx_pr_mm_id"
					+ " \n where lx_pr_id = ?";

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, appendixId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				jobNotesForm.setAppendixId(rs.getString("lx_pr_id"));
				jobNotesForm.setAppendixName(rs.getString("lx_pr_title"));
				jobNotesForm.setMsaId(rs.getString("lp_mm_id"));
				jobNotesForm.setMsaName(rs.getString("lo_ot_name"));
			}
		} catch (Exception e) {
			logger.error(
					"getAppendixInformation(String, JobNotesForm, DataSource)",
					e);

			logger.error(
					"getAppendixInformation(String, JobNotesForm, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

	public static void getJobInformation(String jobId, JobNotesBean form,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select lm_js_id,lm_js_title,lm_js_pr_id,lx_pr_title,"
					+ "lx_pr_mm_id,lo_ot_name from lm_job"
					+ " inner join lx_appendix_main on lm_js_pr_id = lx_pr_id"
					+ " inner join lp_msa_main on lp_mm_id = lx_pr_mm_id"
					+ " inner join lo_organization_top on lp_mm_ot_id = lo_ot_id"
					+ " where lm_js_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				form.setJobId(rs.getString("lm_js_id"));
				form.setJobName(rs.getString("lm_js_title"));
				form.setAppendixId(rs.getString("lm_js_pr_id"));
				form.setAppendixName(rs.getString("lx_pr_title"));
				form.setMsaId(rs.getString("lx_pr_mm_id"));
				form.setMsaName(rs.getString("lo_ot_name"));
			}
		} catch (Exception e) {
			logger.error("getJobInformation(String, JobNotesForm, DataSource)",
					e);

			logger.error("getJobInformation(String, JobNotesForm, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

	public static boolean isNetMedx(String appendixId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select lx_pr_type from lx_appendix_main where lx_pr_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, appendixId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				if (rs.getString("lx_pr_type").trim().equalsIgnoreCase("3"))
					return true;
			}
		} catch (Exception e) {
			logger.error("isNetMedx(String, DataSource)", e);

			logger.error("isNetMedx(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return false;
	}
}
