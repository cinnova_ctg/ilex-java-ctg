package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class AddContactdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AddContactdao.class);

	public static String[] getPocDetails(String pocid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] pocdetail = new String[6];

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select IsNull(lo_pc_phone1,'') as lo_pc_phone1,IsNull(lo_pc_phone2,'') as lo_pc_phone2,IsNull(lo_pc_email1,'')as lo_pc_email1,IsNull(lo_pc_email2,'')as lo_pc_email2 ,IsNull(lo_pc_fax,'')as lo_pc_fax,IsNull(lo_pc_title,'') lo_pc_title from lo_poc where lo_pc_id="
					+ pocid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				pocdetail[0] = rs.getString("lo_pc_phone1");
				pocdetail[1] = rs.getString("lo_pc_phone2");
				pocdetail[2] = rs.getString("lo_pc_email1");
				pocdetail[3] = rs.getString("lo_pc_email2");
				pocdetail[4] = rs.getString("lo_pc_fax");
				pocdetail[5] = rs.getString("lo_pc_title");
			}

		} catch (Exception e) {
			logger.error("getPocDetails(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocDetails(String, DataSource) - Error occured during getting getPocDetails ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getPocDetails(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getPocDetails(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getPocDetails(String, DataSource) - exception ignored",
						e);
			}

		}
		return pocdetail;
	}

	public static String getPocid(String typeid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String pocid = "0";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_ap_pc_id from lm_appendix_poc where lm_ap_pr_id="
					+ typeid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				pocid = rs.getString("lm_ap_pc_id");

			}

		} catch (Exception e) {
			logger.error("getPocid(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocid(String, DataSource) - Error occured during getting getPocid ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn("getPocid(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn("getPocid(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn("getPocid(String, DataSource) - exception ignored",
						e);
			}

		}
		return pocid;
	}

	public static int addContact(String lm_ap_pc_id, String lm_ap_cust_pc_id,
			String lm_ap_sales_poc_id, String lm_ap_billing_poc_id,
			String lm_ap_business_poc_id, String lm_ap_contract_poc_id,
			String lm_ap_pr_id, String lm_ap_user, String cnsPrjManagerId,
			String teamLeadId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		int retval = 0;
		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call lm_appendix_contact_manage_01(?,?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, lm_ap_pc_id);
			cstmt.setString(3, lm_ap_cust_pc_id);
			cstmt.setString(4, lm_ap_sales_poc_id);
			cstmt.setString(5, lm_ap_billing_poc_id);
			cstmt.setString(6, lm_ap_business_poc_id);
			cstmt.setString(7, lm_ap_contract_poc_id);
			cstmt.setString(8, lm_ap_pr_id);
			cstmt.setString(9, lm_ap_user);
			cstmt.setString(10, cnsPrjManagerId);
			cstmt.setString(11, teamLeadId);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"addContact(String, String, String, String, String, String, String, String,String,String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addContact(String, String, String, String, String, String, String, String,String,String, DataSource) - Error occured during adding addContact"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"addContact(String, String, String, String, String, String, String, String,String,String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"addContact(String, String, String, String, String, String, String, String,String,String DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"addContact(String, String, String, String, String, String, String, String,String,String,DataSource) - exception ignored",
						e);
			}

		}
		return retval;

	}

	/**
	 * This method gets Appendix Name.
	 * 
	 * @param appendixid
	 *            String. Appendix id
	 * @param ds
	 *            Object of DataSource
	 * @return String This method returns String, Appendix Name.
	 */
	public static String getAppendixname(String appendixid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String appendixname = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lx_pr_title from lx_appendix_main where lx_pr_id="
					+ appendixid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				appendixname = rs.getString("lx_pr_title");
			}

		} catch (Exception e) {
			logger.error("getAppendixname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixname(String, DataSource) - Error occured during getting Appendix name  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

		}
		return appendixname;
	}

	public static String getPocname(String pocid, DataSource ds) {
		String pocname = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "";

		query = "select dbo.func_cc_poc_name(" + pocid + ")";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery(query);

			if (rs.next()) {
				pocname = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("getPocname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocname(String, DataSource) - Exception caught in getting pocname"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.error("getPocname(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPocname(String, DataSource) - Exception caught in closing pocname resultset"
							+ sql);
				}
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.error("getPocname(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPocname(String, DataSource) - Exception caught in closing pocname statement"
							+ sql);
				}
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.error("getPocname(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPocname(String, DataSource) - Exception caught in closing pocname connection"
							+ sql);
				}
			}
		}

		return pocname;
	}

	public static String getCustpocid(String typeid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String pocid = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_ap_cust_pc_id from lm_appendix_poc where lm_ap_pr_id="
					+ typeid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				pocid = rs.getString("lm_ap_cust_pc_id");

			}

		} catch (Exception e) {
			logger.error("getCustpocid(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustpocid(String, DataSource) - Error occured during getting getCustpocid ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCustpocid(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCustpocid(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCustpocid(String, DataSource) - exception ignored",
						e);
			}

		}
		return pocid;
	}

	public static String getCnsPrjManagerId(String typeid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String cnsPrjManagerId = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_appendix_pc_id from lm_appendix_team where lm_appendix_ro_id=1 and lm_appendix_pr_id="
					+ typeid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				cnsPrjManagerId = rs.getString("lm_appendix_pc_id");

			}

		} catch (Exception e) {
			logger.error("getCnsPrjManagerId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCnsPrjManagerId(String, DataSource) - Error occured during getting getCnsPrjManagerId ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCnsPrjManagerId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCnsPrjManagerId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCnsPrjManagerId(String, DataSource) - exception ignored",
						e);
			}

		}
		return cnsPrjManagerId;
	}

	public static String getTeamLeadId(String typeid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String teamLeadId = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = " select lm_appendix_pc_id from lm_appendix_team where lm_appendix_ro_id=4 and lm_appendix_pr_id="
					+ typeid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				teamLeadId = rs.getString("lm_appendix_pc_id");

			}

		} catch (Exception e) {
			logger.error("getTeamLeadId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTeamLeadId(String, DataSource) - Error occured during getting getTeamLeadId ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getTeamLeadId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getTeamLeadId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getTeamLeadId(String, DataSource) - exception ignored",
						e);
			}

		}
		return teamLeadId;
	}

	public static List<String[]> getEscalationContacts(String prId,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<String[]> contactsList = new ArrayList<String[]>();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = " SELECT CNSPOCname name, CNSPOCphone phone, CNSPOCEmail email, CNSPOCTitle title "
					+ " from dbo.func_lp_appendix_poc_new('"
					+ prId
					+ "') "
					+ " UNION  "
					+ " SELECT lo_pc_first_name+ ' ' +lo_pc_last_name name,  IsNull(lo_pc_phone1,IsNull(lo_pc_phone2,'')) as phone, "
					+ " IsNull(lo_pc_email1,IsNull(lo_pc_email2,''))as email, lo_pc_title title "
					+ " from lo_poc "
					+ " JOIN lm_appendix_team ON lm_appendix_pc_id = lo_pc_id "
					+ " where lm_appendix_ro_id IN(1, 4) and lm_appendix_pr_id= "
					+ prId;
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				contactsList.add(new String[] {
						rs.getString("name").equals("") ? "" : rs
								.getString("name") + ", ",
						rs.getString("phone").equals("") ? "" : rs
								.getString("phone") + ", ",
						rs.getString("email"),
						rs.getString("title").equals("") ? "" : rs
								.getString("title") + ", " });
			}

		} catch (Exception e) {
			logger.error("getEscalationContacts(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEscalationContacts(String, DataSource) - Error occured during getting Escalation Contacts ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getEscalationContacts(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getEscalationContacts(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getEscalationContacts(String, DataSource) - exception ignored",
						e);
			}

		}
		return contactsList;
	}
}
