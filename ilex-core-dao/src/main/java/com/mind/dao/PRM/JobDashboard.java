package com.mind.dao.PRM;

import java.util.ArrayList;

public class JobDashboard 
{
	private String activity_id = null;
	private String activity_name = null;
	private String quantity = null;
	private String type = null;
	private String scope = null;
	private String estimatedunitcost = null;
	private String estimatedtotalcost = null;
	private String extendedunitprice = null;
	private String extendedtotalprice = null;
	private String margin = null;
	private String status = null;
	
	private ArrayList resourcelist = null;
	
	
	private String resource_id = null;
	private String resource_name = null;
	private String resource_qty = null;
	private String resource_type = null;
	private String resource_sourcename = null;
	private String resource_sourcepartno = null;
	private String resource_estimatedunitcost = null;
	private String resource_estimatedtotalcost = null;
	private String resource_extendedunitprice = null;
	private String resource_extendedtotalprice = null;
	private String resource_margin = null;
	private String resource_cnspartnumber = null;
	
	
	
	
	
	
	public String getActivity_id() {
		return activity_id;
	}
	public void setActivity_id(String activity_id) {
		this.activity_id = activity_id;
	}
	public String getActivity_name() {
		return activity_name;
	}
	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}
	public String getEstimatedtotalcost() {
		return estimatedtotalcost;
	}
	public void setEstimatedtotalcost(String estimatedtotalcost) {
		this.estimatedtotalcost = estimatedtotalcost;
	}
	public String getEstimatedunitcost() {
		return estimatedunitcost;
	}
	public void setEstimatedunitcost(String estimatedunitcost) {
		this.estimatedunitcost = estimatedunitcost;
	}
	public String getExtendedtotalprice() {
		return extendedtotalprice;
	}
	public void setExtendedtotalprice(String extendedtotalprice) {
		this.extendedtotalprice = extendedtotalprice;
	}
	public String getExtendedunitprice() {
		return extendedunitprice;
	}
	public void setExtendedunitprice(String extendedunitprice) {
		this.extendedunitprice = extendedunitprice;
	}
	public String getMargin() {
		return margin;
	}
	public void setMargin(String margin) {
		this.margin = margin;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	public ArrayList getResourcelist() {
		return resourcelist;
	}
	public void setResourcelist(ArrayList resourcelist) {
		this.resourcelist = resourcelist;
	}
	
	
	
	public String getResource_estimatedtotalcost() {
		return resource_estimatedtotalcost;
	}
	public void setResource_estimatedtotalcost(String resource_estimatedtotalcost) {
		this.resource_estimatedtotalcost = resource_estimatedtotalcost;
	}
	public String getResource_estimatedunitcost() {
		return resource_estimatedunitcost;
	}
	public void setResource_estimatedunitcost(String resource_estimatedunitcost) {
		this.resource_estimatedunitcost = resource_estimatedunitcost;
	}
	public String getResource_extendedtotalprice() {
		return resource_extendedtotalprice;
	}
	public void setResource_extendedtotalprice(String resource_extendedtotalprice) {
		this.resource_extendedtotalprice = resource_extendedtotalprice;
	}
	public String getResource_extendedunitprice() {
		return resource_extendedunitprice;
	}
	public void setResource_extendedunitprice(String resource_extendedunitprice) {
		this.resource_extendedunitprice = resource_extendedunitprice;
	}
	public String getResource_id() {
		return resource_id;
	}
	public void setResource_id(String resource_id) {
		this.resource_id = resource_id;
	}
	public String getResource_margin() {
		return resource_margin;
	}
	public void setResource_margin(String resource_margin) {
		this.resource_margin = resource_margin;
	}
	public String getResource_name() {
		return resource_name;
	}
	public void setResource_name(String resource_name) {
		this.resource_name = resource_name;
	}
	public String getResource_qty() {
		return resource_qty;
	}
	public void setResource_qty(String resource_qty) {
		this.resource_qty = resource_qty;
	}
	public String getResource_sourcename() {
		return resource_sourcename;
	}
	public void setResource_sourcename(String resource_sourcename) {
		this.resource_sourcename = resource_sourcename;
	}
	public String getResource_sourcepartno() {
		return resource_sourcepartno;
	}
	public void setResource_sourcepartno(String resource_sourcepartno) {
		this.resource_sourcepartno = resource_sourcepartno;
	}
	public String getResource_type() {
		return resource_type;
	}
	public void setResource_type(String resource_type) {
		this.resource_type = resource_type;
	}
	public String getResource_cnspartnumber() {
		return resource_cnspartnumber;
	}
	public void setResource_cnspartnumber(String resource_cnspartnumber) {
		this.resource_cnspartnumber = resource_cnspartnumber;
	}
	
	
	
	
	
	
	
	
}
