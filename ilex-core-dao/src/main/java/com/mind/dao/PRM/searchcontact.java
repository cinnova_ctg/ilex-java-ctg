package com.mind.dao.PRM;

public class searchcontact 
{
	private String org_name = null;
	private String org_division = null;
	private String city = null;
	private String state = null;
	
	
	public String getCity() {
		return city;
	}
	public void setCity( String city ) {
		this.city = city;
	}
	
	
	public String getOrg_division() {
		return org_division;
	}
	public void setOrg_division( String org_division ) {
		this.org_division = org_division;
	}
	
	
	public String getOrg_name() {
		return org_name;
	}
	public void setOrg_name( String org_name ) {
		this.org_name = org_name;
	}
	
	
	public String getState() {
		return state;
	}
	public void setState( String state ) {
		this.state = state;
	}
	
	
	
}
