package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.newjobdb.MPOList;
import com.mind.bean.newjobdb.MasterPurchaseOrder;
import com.mind.bean.prm.BulkJobCreationBean;
import com.mind.common.CheckDateFormat;
import com.mind.common.bean.BulkJobBean;
import com.mind.fw.core.dao.util.DBUtil;

public class BulkJobCreationdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(BulkJobCreationdao.class);

	/**
	 * @author amitm
	 * @param objBulkJobCreationForm
	 * @param ds
	 * @return
	 */

	public static ArrayList getSiteDetailList(String city_name,
			String State_name, String msaId, String appendixId,
			String site_number, String orderByQuery, DataSource ds,
			String endCustomer, String brand, String country) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList sitedetaillist = new ArrayList();

		city_name = (city_name != null) ? city_name : "";
		State_name = (State_name != null) ? State_name.trim() : "";
		site_number = (site_number != null) ? site_number : "";
		endCustomer = (endCustomer != null) ? endCustomer.trim() : "";
		brand = (brand != null) ? brand.trim() : "";
		String result = "";

		if (site_number != null && !site_number.equals(""))
			site_number = site_number.replaceAll("\'", "\'\'");

		if (brand != null && !brand.equals(""))
			brand = brand.replaceAll("\'", "\'\'");

		if (endCustomer != null && !endCustomer.equals(""))
			endCustomer = endCustomer.replaceAll("\'", "\'\'");

		// System.out.println("2::::::::::"+city_name);
		// System.out.println("3::::::::::"+State_name);
		// System.out.println("4::::::::::"+site_number);
		// System.out.println("5::::::::::"+msaId);
		// System.out.println("6::::::::::"+appendixId);
		// System.out.println("7::::::::::"+orderByQuery);
		// System.out.println("8::::::::::"+endCustomer);
		// System.out.println("9::::::::::"+brand);
		// System.out.println("10::::::::::"+country);

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lp_site_search_01(?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, city_name);
			cstmt.setString(3, State_name);
			cstmt.setString(4, site_number);
			cstmt.setString(5, msaId);
			cstmt.setString(6, appendixId);
			cstmt.setString(7, orderByQuery);
			cstmt.setString(8, endCustomer);
			cstmt.setString(9, brand);
			cstmt.setString(10, country);
			rs = cstmt.executeQuery();

			int i = 0;

			while (rs.next()) {
				result = "";
				result = rs.getString("lp_si_number") + ", ";

				if (!(rs.getString("lp_si_group").trim()).equals(""))
					result = result + rs.getString("lp_si_group") + ", ";

				result = result + rs.getString("lp_si_city") + ", "
						+ rs.getString("lp_si_state");

				sitedetaillist.add(new com.mind.common.LabelValue(result, rs
						.getString(1)));

			}
		}

		catch (Exception e) {
			logger.error(
					"getSiteDetailList(String, String, String, String, String, String, DataSource, String, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteDetailList(String, String, String, String, String, String, DataSource, String, String, String) - Error occured  "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getSiteDetailList(String, String, String, String, String, String, DataSource, String, String, String) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getSiteDetailList(String, String, String, String, String, String, DataSource, String, String, String) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getSiteDetailList(String, String, String, String, String, String, DataSource, String, String, String) - exception ignored",
						e);
			}

		}
		return sitedetaillist;
	}

	/**
	 * 
	 * @param city_name
	 * @param State_name
	 * @param ds
	 * @return
	 */

	public static ArrayList getActivityList(String activity, DataSource ds) {
		BulkJobBean bulkjobBean = null;
		Connection conn = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		DecimalFormat dfcurfour = new DecimalFormat("###0.00");
		CallableStatement cstmt = null;
		try {
			conn = ds.getConnection();

			cstmt = conn.prepareCall("{?=call lm_activity_copy(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, activity);
			cstmt.setString(3, "%");

			rs = cstmt.executeQuery();
			int i = 0;

			while (rs.next()) {

				bulkjobBean = new BulkJobBean();

				bulkjobBean.setType(rs.getString("lm_at_type"));

				bulkjobBean.setTempjob_id(rs.getString("lm_js_id"));
				bulkjobBean.setTempjob_name(rs.getString("lm_js_title"));

				bulkjobBean.setEstimated_total_cost(dfcurfour.format(rs
						.getFloat("lx_ce_total_cost")) + "");

				bulkjobBean.setExtended_Price(dfcurfour.format(rs
						.getFloat("lx_ce_invoice_price")) + "");

				bulkjobBean.setExtended_sub_total(dfcurfour.format(rs
						.getFloat("lx_ce_total_price")) + "");

				bulkjobBean.setProforma_Margin(dfcurfour.format(rs
						.getFloat("lx_ce_margin")) + "");

				bulkjobBean.setQty(rs.getString("lx_ce_quantity"));

				bulkjobBean.setActivity(rs.getString("lm_at_title"));
				bulkjobBean.setActivity_id(rs.getString("lm_at_id"));

				bulkjobBean.setLibraryId(rs.getString("lm_at_id"));

				valuelist.add(i, bulkjobBean);
				i++;
			}

		} catch (Exception e) {
			logger.error("getActivityList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityList(String, DataSource) - Error occured during getting bulk jobs activities list"
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return valuelist;
	}

	/**
	 * This method will be called on clicking save during bulk job creation.
	 * 
	 * @param ds
	 * @return
	 */

	public static int saveSiteResult(BulkJobCreationBean bulkjobform, int p,
			DataSource ds, String activityid, String loginuserid) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList sitedetaillist = new ArrayList();
		int val = -1;
		int id = 0;
		int idScheduler = 0;
		String endDate = null;
		String siteNumber = null;
		int siteID = 0;
		String jobName = null;

		if (bulkjobform.getJob_Name() != null) {
			jobName = bulkjobform.getJob_Name().trim();
		} else {
			jobName = bulkjobform.getJob_NewName().trim();
		}
		if (bulkjobform.getNewOwner() == null
				|| bulkjobform.getNewOwner().equals("")
				|| bulkjobform.getNewOwner().equals("0")) {
			id = 0;
		} else {
			id = Integer.parseInt(bulkjobform.getNewOwner());
		}
		if (bulkjobform.getNewScheduler() == null
				|| bulkjobform.getNewScheduler().equals("")
				|| bulkjobform.getNewScheduler().equals("0")) {
			idScheduler = 0;
		} else {
			idScheduler = Integer.parseInt(bulkjobform.getNewScheduler());
		}
		if (bulkjobform.getSelectedparam() == null
				|| bulkjobform.getSelectedparam().length == 0) {
			siteID = 0;
		} else {
			siteID = Integer.parseInt(bulkjobform.getSelectedparam(p));
		}
		if (bulkjobform.getEndDate() == null
				|| bulkjobform.getEndDate().equals("0")) {
			endDate = "";
		} else {
			endDate = bulkjobform.getEndDate();
		}
		if (bulkjobform.getSiteNumber() == null) {
			siteNumber = "";
		} else {
			siteNumber = bulkjobform.getSiteNumber().trim();
		}

		/*
		 * System.out.println( " "+Integer.parseInt(
		 * bulkjobform.getAppendix_Id()
		 * )+"  , '"+bulkjobform.getJob_Name()+"' , " +
		 * ""+siteID+" , '"+activityid+"' , '"+bulkjobform.getSite_name()+"' , "
		 * + "'"+bulkjobform.getPrifix()+"' , "+Integer.parseInt(loginuserid)+
		 * " , 'A'   "
		 * +" , '"+bulkjobform.getStartDate().trim()+"' , '"+endDate+"' , "
		 * +id+", "+idScheduler+","+siteNumber);
		 */

		try {
			conn = ds.getConnection();
			// cstmt =
			// conn.prepareCall("{?=call lx_bulkjob_manage_01(?,?,?,?,?,?,?,?)}");
			cstmt = conn
					.prepareCall("{?=call lx_bulkjob_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(bulkjobform.getAppendix_Id()));
			cstmt.setString(3, jobName);
			cstmt.setInt(4, siteID);
			cstmt.setString(5, activityid);
			cstmt.setString(6, bulkjobform.getSite_name());// For radio button
			cstmt.setString(7, bulkjobform.getPrifix());
			cstmt.setInt(8, Integer.parseInt(loginuserid));
			cstmt.setString(9, "A");
			cstmt.setString(10, bulkjobform.getStartDate().trim());
			cstmt.setString(11, endDate);
			cstmt.setInt(12, id);
			cstmt.setInt(13, idScheduler);
			cstmt.setString(14, siteNumber);
			cstmt.setString(15, bulkjobform.getCustRef());

			/*
			 * System.out.println( " "+Integer.parseInt(
			 * bulkjobform.getAppendix_Id() )+"  , '"+jobName+"' , " +
			 * ""+siteID+
			 * " , '"+activityid+"' , '"+bulkjobform.getSite_name()+"' , " +
			 * "'"+bulkjobform.getPrifix()+"' , "+Integer.parseInt(loginuserid)+
			 * " , 'A'   "
			 * +" , '"+bulkjobform.getStartDate().trim()+"' , '"+endDate+"' , "
			 * +id+", "+idScheduler+","+bulkjobform.getSiteNumber().trim());
			 */

			cstmt.execute();
			val = cstmt.getInt(1);

		}

		catch (Exception e) {
			logger.error(
					"saveSiteResult(BulkJobCreationForm, int, DataSource, String, String)",
					e);
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"saveSiteResult(BulkJobCreationForm, int, DataSource, String, String) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"saveSiteResult(BulkJobCreationForm, int, DataSource, String, String) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"saveSiteResult(BulkJobCreationForm, int, DataSource, String, String) - exception ignored",
						e);
			}

		}
		return val;
	}

	public static ArrayList getBulkJobStateList(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select cp_country_id, cp_country_name, cp_country_order from lp_country_list order by cp_country_order,cp_country_name";
			rs = stmt.executeQuery(sql);
			int i = 0;

			if (i == 0) {
				BulkJobCreationBean bulkjobform = new BulkJobCreationBean();
				bulkjobform.setCountryId("");
				bulkjobform.setCountryName("");
				valuelist.add(i, bulkjobform);
				i++;
			}

			while (rs.next()) {
				BulkJobCreationBean bulkjobform = new BulkJobCreationBean();
				bulkjobform.setCountryId(rs.getString("cp_country_id"));
				bulkjobform.setCountryName(rs.getString("cp_country_name"));
				valuelist.add(i, bulkjobform);
				i++;
			}

		} catch (Exception e) {
			logger.error("getBulkJobStateList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getBulkJobStateList(DataSource) - Error occured during getting BulkJob State Category Wise List  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getBulkJobStateList(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getBulkJobStateList(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getBulkJobStateList(DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static String checkDate(String[] siteDate, String appendixId,
			DataSource ds) {
		String check = "true";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String siteNumber = null;
		String startDate = null;
		boolean dateCheck = true;
		if (siteDate.length == 2) {
			siteNumber = siteDate[0];
			startDate = siteDate[1];
			if (!startDate.equals("\r")) {
				dateCheck = CheckDateFormat.checkFormat(startDate.trim());
			}
		} else {
			siteNumber = siteDate[0];
			startDate = "";
		}
		if (siteDate[0] != null && !siteDate[0].equals(""))
			siteDate[0] = siteDate[0].replaceAll("\'", "\'\'");
		/*
		 * System.out.println("Site Number--"+siteDate[0]+"---");
		 * System.out.println("Start Date--"+startDate);
		 * System.out.println("msaId--"+msaId);
		 */
		// System.out.println("dateCheck---"+dateCheck);
		if (dateCheck) {
			try {
				conn = ds.getConnection();
				pstmt = conn
						.prepareStatement("select  [dbo].[func_lp_site_check](?,?)");
				pstmt.setString(1, siteNumber.trim());
				pstmt.setString(2, appendixId);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					check = rs.getString(1);
				}

			} catch (Exception e) {
				logger.error("checkDate(String[], String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("checkDate(String[], String, DataSource) - Error occured com.mind.dao.PRM.BulkJobCreationDao.checkdate() "
							+ e);
				}
			} finally {
				DBUtil.close(rs, pstmt);
				DBUtil.close(conn);
			}
			return check;
		} else {
			check = "datenotformated";
			return check;
		}
	}

	public static ArrayList getValidationError(String[][] sitedate, int j) {
		BulkJobCreationBean bulkjobForm = null;
		ArrayList valuelist = new ArrayList();
		try {
			int i = 0;
			while (i < j) {
				bulkjobForm = new BulkJobCreationBean();
				bulkjobForm.setErrorrSiteNumber(sitedate[i][0].trim());
				bulkjobForm.setErrorPlannedStartDate(sitedate[i][1].trim());
				valuelist.add(i, bulkjobForm);
				i++;
			}
		} catch (Exception e) {
			logger.error("getValidationError(String[][], int)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getValidationError(String[][], int) - " + e);
			}
		}
		return valuelist;

	}

	public static String getJobName(BulkJobCreationBean form) {
		String jobName = null;
		if (form.getJob_Name() != null) {
			jobName = form.getJob_Name().trim();
		} else {
			jobName = form.getJob_NewName().trim();
		}
		if (form.getPrifix().equals("P")) {
			jobName = form.getSiteNumber() + " " + jobName;
		} else {
			jobName = jobName + " " + form.getSiteNumber().trim();
		}
		return jobName;
	}

	public static ArrayList getSearchedPartnerList(String partnerName,
			String partnerKeyWord, DataSource ds) {
		ArrayList pList = new ArrayList();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		if (partnerName != null) {
			partnerName = "%" + partnerName + "%";
		}
		if (partnerKeyWord != null) {
			partnerKeyWord = "%" + partnerKeyWord + "%";
		}

		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select * from func_cp_partner_bulk_job_info(?,?)");
			pstmt.setString(1, partnerName);
			pstmt.setString(2, partnerKeyWord);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				BulkJobCreationBean form = new BulkJobCreationBean();
				form.setPartnerId(rs.getString("cp_partner_id"));
				form.setNamePartner(rs.getString("lo_om_division"));

				form.setPartnerState(rs.getString("lo_ad_state"));
				form.setPartnerCity(rs.getString("lo_ad_city"));
				form.setPartnerCountry(rs.getString("lo_ad_country"));
				form.setPartnerZipcode(rs.getString("lo_ad_zip_code"));
				form.setPocFirstName(rs.getString("lo_pc_first_name"));
				form.setPocLastName(rs.getString("lo_pc_last_name"));
				form.setPartnerType(rs.getString("lo_ot_name"));
				pList.add(form);
			}

		} catch (Exception e) {
			logger.error("getSearchedPartnerList(String, String)", e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}

		return pList;
	}

	public static String getJobIdfromName(String jobName, DataSource ds) {
		String jobId = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select lm_js_id from lm_job where lm_js_title =" + "'"
				+ jobName + "'";
		try {
			conn = ds.getConnection();
			stmt = conn.prepareStatement(sql);
			// pstmt.setString(1, jobName);
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				jobId = rs.getString("lm_js_id");

			}
		} catch (Exception e) {
			logger.error("getJobIdfromName(String, ds)", e);

		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return jobId;
	}

	public static MPOList getListOfMPO(String appendixId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		MPOList mpoList = new MPOList();
		ArrayList<MasterPurchaseOrder> listOfMPOS = new ArrayList<MasterPurchaseOrder>();
		try {
			conn = ds.getConnection();
			String sql = "select * from mp_main where mp_appendix_id = ? and mp_mn_status = 'Approved' order by mp_mn_name";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, appendixId);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				MasterPurchaseOrder masterPurchaseOrder = new MasterPurchaseOrder();
				masterPurchaseOrder.setMpoId(rs.getString("mp_mn_id"));
				masterPurchaseOrder.setMasterPOItem(rs
						.getString("mp_mn_master_po_item"));
				masterPurchaseOrder.setMpoName(rs.getString("mp_mn_name"));
				masterPurchaseOrder.setEstimatedTotalCost(rs
						.getFloat("mp_mn_estimated_cost"));
				masterPurchaseOrder.setStatus(rs.getString("mp_mn_status"));
				if (masterPurchaseOrder.isSelectableForCopyToPO())
					masterPurchaseOrder.setSelectable(true);
				else
					masterPurchaseOrder.setSelectable(false);
				listOfMPOS.add(masterPurchaseOrder);
			}
		} catch (Exception e) {
			logger.error("getListOfMPO(String, DataSource)", e);

			logger.error("getListOfMPO(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		mpoList.setMasterPurchaseOrders(listOfMPOS);
		return mpoList;
	}

	/**
	 * Discription: This method is use to get PO Type List.
	 * 
	 * @param ds
	 *            - Database object
	 * @return ArrayList - Role List
	 */
	public static ArrayList getPOTypeList() {

		// Connection conn = null;
		// PreparedStatement pstmt = null;
		// ResultSet rs = null;
		ArrayList schedulerList = new ArrayList();
		try {
			schedulerList
					.add(new com.mind.common.LabelValue("--Select--", "0"));
			schedulerList.add(new com.mind.common.LabelValue("Speedpay", "S"));
			schedulerList.add(new com.mind.common.LabelValue("PVS", "P"));
		} catch (Exception e) {
			logger.error("getPOTypeList(String, String, DataSource)", e);
		} finally {
		}
		return schedulerList;
	}

	public static void updatePOType(String poType, String poId,
			String loginUserId, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_po_manage_for_bulkjob( ? ,? , ?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, poType);
			cstmt.setInt(3, Integer.parseInt(poId));
			cstmt.setInt(4, Integer.parseInt(loginUserId));
			cstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("getPOTypeList(String, String, DataSource)", e);
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}

	}

	// getting email from table for email
	public static String getRequestorEmailByJobId(String jobId, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String requestorEmail = "";
		java.util.Date dateIn = new java.util.Date();

		if (jobId == null || jobId.equals("")) {
			return "";
		}
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "SELECT jre_requestor_email FROM job_requestor_email WHERE jre_lm_js_id =  "
					+ jobId;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				requestorEmail = rs.getString("jre_requestor_email");
			}

		} catch (Exception e) {
			logger.error("getRequestorEmailByJobId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRequestorEmailByJobId(String, DataSource) - Error occured during getting Appendix name  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getRequestorEmailByJobId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getRequestorEmailByJobId(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getRequestorEmailByJobId(String, DataSource) - exception ignored",
						e);
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("getRequestorEmailByJobId(String, DataSource) - **** getRequestorEmailByJobId :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return requestorEmail;
	}

	// /insertion into requestor table
	public static void inserTORequestorEmail(String jobId, String useremail,
			DataSource ds) throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = ds.getConnection();

			ps = conn
					.prepareStatement("insert into job_requestor_email values(?,?)");
			ps.setString(1, jobId);
			ps.setString(2, useremail);
			ps.executeUpdate();

		}

		catch (Exception e) {
			logger.error("inserTORequestorEmail(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("inserTORequestorEmail(String, String, DataSource) - Error occured during insertion"
						+ e);
			}
		}

		finally {
			DBUtil.closePreparedStatement(ps);
			DBUtil.closeDbConnection(conn);
		}

	}

	// get Site name

	/*
	 * public static String getSiteName(String jobName, DataSource ds) { String
	 * jobId = ""; Connection conn = null; PreparedStatement pstmt = null;
	 * ResultSet rs = null; String sql =
	 * "select  lp_si_name,lp_si_number from lp_site_list where lp_sl_id ="
	 * +bulkjobform.getSelectedparam(); try { conn = ds.getConnection(); pstmt =
	 * conn.prepareStatement(sql); pstmt.setString(1, jobName); rs =
	 * pstmt.executeQuery(); if (rs.next()) { jobId = rs.getString("lm_js_id");
	 * 
	 * } } catch (Exception e) { logger.error("getSiteName(String, ds)", e);
	 * 
	 * } finally { DBUtil.close(rs, pstmt); DBUtil.close(conn); } return jobId;
	 * }
	 */
	// get site id from job id
	public static String getSiteid(String jobid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String siteid = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lp_si_number from lp_site_list where lp_sl_id ="
					+ jobid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				siteid = rs.getString("lp_si_number");

			}

		} catch (Exception e) {
			logger.error("getSiteid(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteid(String, DataSource) - Error occured during getting Siteid "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getSiteid(String, DataSource) - exception ignored", e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getSiteid(String, DataSource) - exception ignored", e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getSiteid(String, DataSource) - exception ignored", e);
			}

		}
		return siteid;
	}

	// get site name from site id

	public static String getSitename(String siteid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sitename = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select  lp_si_name  from lp_site_list where lp_sl_id ="
					+ "'" + siteid + "'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				sitename = rs.getString("lp_si_name");
			}

		} catch (Exception e) {
			logger.error("getSitename(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSitename(String, DataSource) - Error occured during getting Sitename ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getSitename(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getSitename(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getSitename(String, DataSource) - exception ignored",
						e);
			}

		}
		return sitename;
	}

	// get job id from lm_job

	public static String getjobid(String title, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String jobid = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lm_js_id from lm_job where  lm_js_title = "
					+ "'" + title + "'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				jobid = rs.getString("lm_js_id");
			}

		} catch (Exception e) {
			logger.error("getjobid(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getjobid(String, DataSource) - Error occured  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn("getjobid(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn("getjobid(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn("getjobid(String, DataSource) - exception ignored",
						e);
			}

		}
		return jobid;
	}

}