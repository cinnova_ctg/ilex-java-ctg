package com.mind.dao.PRM;

import com.mind.dao.PRM.objects.ScheduleElement;
import com.mind.fw.core.dao.util.DBUtil;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

public class Scheduledao {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(Scheduledao.class);

    /**
     * Set Job schedules into database.
     *
     * @param lx_se_type_id
     * @param act_start
     * @param act_end
     * @param lx_se_type
     * @param lx_se_created_by
     * @param lx_se_start
     * @param lx_se_end
     * @param count
     * @param ds
     * @return
     */
    public static int[] addScheduleForPRM(String lx_se_type_id,
            String act_start, String act_end, String lx_se_type,
            String lx_se_created_by, String lx_se_start, String lx_se_end,
            int count, DataSource ds) {
        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        int retValArray[] = new int[2];

        try {
            conn = ds.getConnection();
            cstmt = conn
                    .prepareCall("{?=call lx_schedule_manage_01(?,?,?,?,?,?,?,?,?)}"); // Total
            // 10
            // ?
            // (1
            // for
            // output
            // parameter)
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setString(2, lx_se_type_id);
            cstmt.setString(3, act_start);
            cstmt.setString(4, act_end);
            cstmt.setString(5, lx_se_type);
            cstmt.setString(6, lx_se_created_by);
            cstmt.setString(7, lx_se_start);
            cstmt.setString(8, lx_se_end);
            cstmt.setInt(9, count);
            cstmt.registerOutParameter(10, java.sql.Types.INTEGER);
            cstmt.execute();
            retValArray[0] = cstmt.getInt(1);
            retValArray[1] = cstmt.getInt(10);

        } catch (Exception e) {
            logger.error(
                    "addScheduleForPRM(String, String, String, String, String, String, String, int, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("addScheduleForPRM(String, String, String, String, String, String, String, int, DataSource) - Error occured during adding schedule"
                        + e);
            }
            logger.error(
                    "addScheduleForPRM(String, String, String, String, String, String, String, int, DataSource)",
                    e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addScheduleForPRM(String, String, String, String, String, String, String, int, DataSource) - exception ignored",
                        e);
            }

            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addScheduleForPRM(String, String, String, String, String, String, String, int, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "addScheduleForPRM(String, String, String, String, String, String, String, int, DataSource) - exception ignored",
                        e);
            }

        }
        return retValArray;

    }

    /*
     * Description: Returns the details for a schedule element based on its job and dispatch number.
     *
     * Version History:
     * 01/16/15 - Initial version
     *
     * Parameters:
     * @param (String) jobId - The job to pull data for.
     * @param (int) dispatchId - The dispatch number to pull data for.
     * @param (DataSource) ds - The database data source object.
     *
     * Returns:
     * @return (ScheduleElement) The data in a shedule element object.
     */
    public static ScheduleElement getScheduleByDispatchId(String jobId, int dispatchId, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        ScheduleElement se = null;

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            // String
            // sql="select * from lx_schedule_list_01 where lx_se_type_id="+typeid+" AND lx_se_type='"+type+"'";
            String sql = "SELECT lx_se_start as actual_start"
                    + "     , lx_se_end as actual_end"
                    + "     , lx_se_planned_start as planned_start"
                    + "     , lx_se_planned_end as planned_end"
                    + " FROM lx_schedule_element"
                    + " WHERE lx_se_type_id = '" + jobId + "'"
                    + "     AND lx_se_sequence = " + dispatchId
                    + "     AND lx_se_type in ('J','IJ')";

            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                se = new ScheduleElement(jobId, dispatchId, rs.getString("actual_start"), rs.getString("actual_end"), rs.getString("planned_start"), rs.getString("planned_end"));
            }

        } catch (Exception e) {
            logger.error("getScheduleByDispatchId(String, String, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getScheduleByDispatchId(String, String, String, DataSource) - Error occured during getting schedule  ");
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn("getScheduleByDispatchId(String, String, String, DataSource) - exception ignored", e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn("getScheduleByDispatchId(String, String, String, DataSource) - exception ignored", e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn("getScheduleByDispatchId(String, String, String, DataSource) - exception ignored", e);
            }

        }

        return se;
    }

    public static String[] getSchedule(String typeid, String type, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String[] str = new String[8];

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            // String
            // sql="select * from lx_schedule_list_01 where lx_se_type_id="+typeid+" AND lx_se_type='"+type+"'";
            String sql = "select * from lx_schedule_list_01 where lx_se_type_id="
                    + typeid;
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                str[0] = rs.getString("lx_se_id");
                str[1] = rs.getString("lx_se_type");
                str[2] = rs.getString("lx_se_type_id");
                str[3] = rs.getString("lx_se_start");
                str[4] = rs.getString("lx_se_end");
                str[5] = rs.getString("lx_se_days");
                str[6] = rs.getString("lx_se_act_start");
                str[7] = rs.getString("lx_se_act_end");
            }

        } catch (Exception e) {
            logger.error("getSchedule(String, String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getSchedule(String, String, DataSource) - Error occured during getting schedule  ");
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getSchedule(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getSchedule(String, String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getSchedule(String, String, DataSource) - exception ignored",
                        e);
            }

        }
        return str;

    }

    public static String getCurrentDate() {
        String date;
        Calendar c = Calendar.getInstance();
        date = (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE) + "/"
                + c.get(Calendar.YEAR);
        return date;
    }

    public static String getAppendixname(String appendixid, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String appendixname = "";

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "select lx_pr_title from lx_appendix_main where lx_pr_id="
                    + appendixid;

            rs = stmt.executeQuery(sql);

            if (rs.next()) {

                appendixname = rs.getString("lx_pr_title");
            }

        } catch (Exception e) {
            logger.error("getAppendixname(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getAppendixname(String, DataSource) - Error occured during getting Appendix name  ");
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getAppendixname(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getAppendixname(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getAppendixname(String, DataSource) - exception ignored",
                        e);
            }

        }
        return appendixname;
    }

    public static String getJobname(String Job_Id, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";

        String jobname = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            sql = "select lm_js_title from lm_job where lm_js_id=" + Job_Id;
            rs = stmt.executeQuery(sql);
            // System.out.println("---job name sql"+sql);
            if (rs.next()) {
                jobname = rs.getString(1);
            }
        } catch (Exception e) {
            logger.error("getJobname(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getJobname(String, DataSource) - Error Occured in retrieving jobname"
                        + e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobname(String, DataSource) - exception ignored", s);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobname(String, DataSource) - exception ignored", s);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException s) {
                logger.warn(
                        "getJobname(String, DataSource) - exception ignored", s);
            }

        }
        return jobname;
    }

    public static String[] getPMSchedule(String jobid, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        // ArrayList valuelist=new ArrayList();
        String[] valuelist = new String[8];
        // ArrayList powolist=new ArrayList();
        // DecimalFormat dfcurmargin = new DecimalFormat( "0.00" );

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            String sql = "select * from dbo.func_lp_prj_job_tab('%'," + jobid
                    + ",'%','%')";
            // System.out.println("sql getPMSchedule "+sql);
            rs = stmt.executeQuery(sql);
            int i = 0;

            if (rs.next()) {

                valuelist[0] = rs.getString("lm_js_planned_start_date");
                valuelist[1] = rs.getString("lm_js_planned_end_date");

                if (!(rs.getString("lx_act_start_time").equals(""))) {
                    valuelist[2] = rs.getString("lx_act_start_time").substring(
                            0, 2);
                    valuelist[3] = rs.getString("lx_act_start_time").substring(
                            3, 5);
                    valuelist[4] = rs.getString("lx_act_start_time").substring(
                            6, 8);
                }

                if (!(rs.getString("lx_act_end_time").equals(""))) {
                    valuelist[5] = rs.getString("lx_act_end_time").substring(0,
                            2);
                    valuelist[6] = rs.getString("lx_act_end_time").substring(3,
                            5);
                    valuelist[7] = rs.getString("lx_act_end_time").substring(6,
                            8);
                }

            }

        } catch (Exception e) {
            logger.error("getPMSchedule(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getPMSchedule(String, DataSource) - Error occured during getting get PM Schedule  ");
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPMSchedule(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPMSchedule(String, DataSource) - exception ignored",
                        e);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.warn(
                        "getPMSchedule(String, DataSource) - exception ignored",
                        e);
            }

        }
        return valuelist;
    }

    // for adding criticality combo in csv report
    public static ArrayList getCriticalitylist(DataSource ds) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        // list.add( new com.mind.common.LabelValue( "---Select---" , "0") );

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();

            rs = stmt
                    .executeQuery("select iv_crit_id,iv_crit_name  from iv_criticality ");
            while (rs.next()) {
                list.add(new com.mind.common.LabelValue(rs
                        .getString("iv_crit_name"), rs.getString("iv_crit_id")));
            }
        } catch (Exception e) {
            logger.error("getCriticalitylist(DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getCriticalitylist(DataSource) - Exception caught"
                        + e);
            }
        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "getCriticalitylist(DataSource) - exception ignored",
                        sql);
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "getCriticalitylist(DataSource) - exception ignored",
                        sql);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sql) {
                logger.warn(
                        "getCriticalitylist(DataSource) - exception ignored",
                        sql);
            }

        }
        return list;
    }

    /**
     * Discription: This method get current schedule date of a job from
     * database.
     *
     * @param jobId - Job Id
     * @param ds - Database Object
     * @return currStartDateTime - Current Schedule date of job
     */
    public static String checkStartDate(String jobId, DataSource ds) {

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String currStartDateTime = "";
        try {
            conn = ds.getConnection();
            pstmt = conn
                    .prepareStatement("select dbo.func_job_start_date_check(?)");
            pstmt.setString(1, jobId); // - Job Id
            rs = pstmt.executeQuery();
            if (rs.next()) {
                currStartDateTime = rs.getString(1);
            }
        } catch (Exception e) {
            logger.error("checkStartDate(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("checkStartDate(String, DataSource) - Error Occured in com.mind.dao.PRM.shceduledao.checkStartDate "
                        + e);
            }
        } finally {
            DBUtil.close(rs, pstmt);
            DBUtil.close(conn);
        }

        return currStartDateTime;
    }

    /**
     * Discription: This mehod is used to store reschedule actions into
     * database..
     *
     * @param jobId - Job Id
     * @param resReason - Reschedule Reason
     * @param oldSchDate - Old Schedule Date
     * @param newSchDate - New Schedule Date
     * @param rseExplanation - Reschedule Explanation
     * @param user - Login user
     * @param ds - Database Objec
     */
    public static void setReschedule(String jobId, String resType,
            String oldSchDate, String newSchDate,
            String rseInternalJustification, String user, DataSource ds) {

        Connection conn = null;
        CallableStatement cstmt = null;
        String installContent = "";
		// if (resType != null && resType.equals("Internal")) { // -
        // 1
        // =
        // Client
        // (Only
        // of
        // client)
		/*
         * if (Integer.valueOf(resReason) == 1) { resReason =
         * "No facility access"; } else if (Integer.valueOf(resReason) == 2) {
         * resReason = "Client supplied equipment not available"; } else if
         * (Integer.valueOf(resReason) == 3) { resReason = "Client not ready"; }
         * else if (Integer.valueOf(resReason) == 4) { resReason = "Other"; }
         */
        /* Content of Install Note */
        /*
         * installContent = "The job was rescheduled to " + newSchDate +
         * " from the original planned date of " + oldSchDate + ". Notes: " +
         * resReason + ".";
         */
        // }
        // System.out.println("2 = "+jobId);
        // System.out.println("3 = "+resType);
        // System.out.println("4 = "+oldSchDate);
        // System.out.println("5 = "+newSchDate);
        // System.out.println("6 = "+user);
        // System.out.println("7 = "+installContent);

        try {
            conn = ds.getConnection();
            cstmt = conn
                    .prepareCall("{?=call lm_job_reschedule_manage_01(?,?,?,?,?,?,?,?)}"); // Total
            // 6
            // ?
            // (1
            // for
            // output
            // parameter)
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setString(2, jobId); // - Job Id
            cstmt.setString(3, resType); // - Reschedule Type
            cstmt.setString(4, oldSchDate); // - Old Schedule Date
            cstmt.setString(5, newSchDate); // - New Schedule Date
            cstmt.setString(6, user); // - User Id
            cstmt.setString(7, installContent); // - Install note
            cstmt.setString(8, ""); // - Install note
            if (resType.equals("Internal")) {
                cstmt.setString(9, rseInternalJustification);
            } else {
                cstmt.setString(9, null);
            }
            // -rseInternalJustification
            cstmt.execute();

        } catch (Exception e) {
            logger.error(
                    "setReschedule(String, String, String, String, String, String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("setReschedule(String, String, String, String, String, String, DataSource) - Error occured in com.mind.dao.PRM.scheduledao.setReschedule() "
                        + e);
            }
        } finally {
            DBUtil.close(cstmt);
            DBUtil.close(conn);
        }
    }

    public static boolean checkPOStatus(String jobId, DataSource ds) {

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean isAccepted = false;
        try {
            conn = ds.getConnection();
            /*
             * String sql = "select lm_po_id from lm_purchase_order " +
             * "inner join lm_po_status_types_master on po_status = po_status_code "
             * +
             * "where lm_po_js_id = ? and po_status_name in('STATUS_ACCEPTED')";
             */
            pstmt = conn
                    .prepareStatement("select isExists = dbo.func_check_accepted_po(?)");
            pstmt.setString(1, jobId); // - Job Id
            rs = pstmt.executeQuery();
            if (rs.next()) {
                if (rs.getString("isExists").equalsIgnoreCase("true")) {
                    isAccepted = true;
                }
            }
        } catch (Exception e) {
            logger.error("checkPOStatus(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("checkPOStatus(String, DataSource) - Error Occured in com.mind.dao.PRM.shceduledao.checkPOStatus() "
                        + e);
            }
        } finally {
            DBUtil.close(rs, pstmt);
            DBUtil.close(conn);
        }

        return isAccepted;
    } // - End of checkPOStatus() method

    /**
     * Mehtod is used to set Actual and End Date.
     *
     * @param jobId -- Job Id
     * @param actualStart -- Actual Start
     * @param actualEnd -- Actual End
     * @param user -- Login user
     * @param ds -- DataSource Object
     * @return int[]
     */
    public static int[] setActualEndDate(String jobId, String actualStart,
            String actualEnd, String user, DataSource ds) {
        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        int retValArray[] = new int[2];

        /*
         * System.out.println("-- Job Id ---"+jobId);
         * System.out.println("-- Actual start date ---"+actualStart);
         * System.out.println("-- Actual end date ---"+actualEnd);
         * System.out.println("-- User Id ---"+user);
         */
        try {
            conn = ds.getConnection();
            cstmt = conn
                    .prepareCall("{?=call lx_js_act_start_end_schedule_manage_01(?,?,?,?,?)}"); // Total
            // 5
            // ?
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setString(2, jobId);
            cstmt.setString(3, actualStart);
            cstmt.setString(4, actualEnd);
            cstmt.setString(5, user);
            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
            cstmt.execute();
            retValArray[0] = cstmt.getInt(1);
            retValArray[1] = cstmt.getInt(6); // - Schedule Id
        } catch (Exception e) {
            logger.error(
                    "setActualEndDate(String, String, String, String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("setActualEndDate(String, String, String, String, DataSource) - Error occured during adding schedule"
                        + e);
            }
            logger.error(
                    "setActualEndDate(String, String, String, String, DataSource)",
                    e);
        } finally {
            DBUtil.close(rs, cstmt);
            DBUtil.close(conn);
        }
        return retValArray;
    } // - End of setActualEndDate() Date Method

    /**
     * Mehtod is used to get no of Dispatches in Ticket or Job.
     *
     * @param jobId -- Job Id
     * @return int
     */
    public static int getDispatchCount(String jobId, DataSource ds) {
        Connection conn = null;
        // CallableStatement cstmt = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int retVal = 0;
        String sql = "";
        int jobIdnumeric = 0;
        if (jobId != null) {
            jobIdnumeric = Integer.parseInt(jobId);

        }
        try {
            conn = ds.getConnection();
            // cstmt=conn.prepareCall("{?=call lx_js_act_start_end_schedule_manage_01(?,?,?,?,?)}");
            // //Total 5 ?
            sql = "select top 1 lx_se_sequence from lx_schedule_element where lx_se_type_id = ? order by 1 desc";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, jobIdnumeric);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retVal = rs.getInt(1);
            }

        } catch (Exception e) {
            logger.error("getDispatchCount(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getDispatchCount(String, DataSource) - Error occured during adding Scheduledao.getDispatchCount"
                        + e);
            }
            logger.error("getDispatchCount(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, pstmt);
            DBUtil.close(conn);
        }
        return retVal;
    } // - End of getDispatchCount() Method

    /**
     * Check All PO status on the time of shcduling of a job. If job has no
     * accepted PO then this job should not mark on site date.
     *
     * @param jobId
     * @param request
     * @param ds
     * @return true/false
     */
    public static boolean isAccepted() {
        return true; // - temp remove this check on crage request.
		/*
         * boolean isAccepted = false;
         *
         * if(onsiteDate != null && !onsiteDate.trim().equals("")){ isAccepted =
         * checkPOStatus(bean.getJobId(), ds); }else{ isAccepted = true; }
         * if(!isAccepted){ bean.setOnsiteDate(null); bean.setOnsiteHour(null);
         * bean.setOnsiteIsAm(null); request.setAttribute("isAccepted",
         * "notAccepted"); // - For show message in Job Schedule Tab. }
         *
         * return isAccepted;
         */
    }
}
