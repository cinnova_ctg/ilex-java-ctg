package com.mind.dao.PRM;

public class POActivityDetail {
	private String po_number = null;
	private String wo_number = null;
	private String activity_id = null;
	private String activity_title = null;
	private String partner_name = null;
	private String lx_ce_total_price = null;
	private String lx_ce_invoice_price = null;
	private String po_remarks = null; 
	private String wo_remarks = null;
	
	
	public String getActivity_id() 
	{
		return activity_id;
	}
	public void setActivity_id(String activity_id)
{
		this.activity_id = activity_id;
	}
	public String getActivity_title() 
	{
		return activity_title;
	}
	public void setActivity_title(String activity_title) 
	{
		this.activity_title = activity_title;
	}
	public String getPo_number() 
	{
		return po_number;
	}
	public void setPo_number(String po_number) 
	{
		this.po_number = po_number;
	}
	public String getLx_ce_invoice_price() {
		return lx_ce_invoice_price;
	}
	public void setLx_ce_invoice_price(String lx_ce_invoice_price) {
		this.lx_ce_invoice_price = lx_ce_invoice_price;
	}
	public String getLx_ce_total_price() {
		return lx_ce_total_price;
	}
	public void setLx_ce_total_price(String lx_ce_total_price) {
		this.lx_ce_total_price = lx_ce_total_price;
	}
	public String getPartner_name() {
		return partner_name;
	}
	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}
	public String getWo_number() {
		return wo_number;
	}
	public void setWo_number(String wo_number) {
		this.wo_number = wo_number;
	}
	public String getPo_remarks() {
		return po_remarks;
	}
	public void setPo_remarks(String po_remarks) {
		this.po_remarks = po_remarks;
	}
	public String getWo_remarks() {
		return wo_remarks;
	}
	public void setWo_remarks(String wo_remarks) {
		this.wo_remarks = wo_remarks;
	}
	
}
