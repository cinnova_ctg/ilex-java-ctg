package com.mind.dao.PRM;

public class POWODetail 
{
	
	private String act_name = null;
	private String resource_id = null;
	private String resource_name = null;
	private String resource_qty = null;
	private String resource_unitcost = null;
	private String resource_totalcost = null;
	private String resource_authorizedunitcost = null;
	private String resource_authorizedtotalcost = null;
	private String resource_showonwo = null;
	private String resource_tempid = null;
	private String resource_tempshowonwo = null;
	private String pvs_supplied = null;
	
	private String resource_temp_pvs_supplied = null;
	
	
	private String resource_dropshift = null;
	private String resource_tempdropshift = null;
	private String resource_act_qty = null;
	
	private String unitcost = null;
	
	private String act_qty = null;
	
	private String resource_authorizedqty = null;
	private String resource_subtotalcost = null;
	//changes made by Seema
	private String credit_card = null;
	private String resource_temp_credit_card = null;
	
	//changes made by Seema
	private String type = null;
	private String resource_type = null;
	private String resourceSubCat = null;
	private String nonContractEstTotal = null;
	
	//over
	
	public String getResourceSubCat() {
		return resourceSubCat;
	}
	public void setResourceSubCat(String resourceSubCat) {
		this.resourceSubCat = resourceSubCat;
	}
	public String getAct_name() {
		return act_name;
	}
	public void setAct_name(String act_name) {
		this.act_name = act_name;
	}
	
	

	public String getResource_id() {
		return resource_id;
	}
	public void setResource_id(String resource_id) {
		this.resource_id = resource_id;
	}
	public String getResource_authorizedtotalcost() {
		return resource_authorizedtotalcost;
	}
	public void setResource_authorizedtotalcost(String resource_authorizedtotalcost) {
		this.resource_authorizedtotalcost = resource_authorizedtotalcost;
	}
	public String getResource_authorizedunitcost() {
		return resource_authorizedunitcost;
	}
	public void setResource_authorizedunitcost(String resource_authorizedunitcost) {
		this.resource_authorizedunitcost = resource_authorizedunitcost;
	}
	public String getResource_name() {
		return resource_name;
	}
	public void setResource_name(String resource_name) {
		this.resource_name = resource_name;
	}
	public String getResource_qty() {
		return resource_qty;
	}
	public void setResource_qty(String resource_qty) {
		this.resource_qty = resource_qty;
	}
	public String getResource_showonwo() {
		return resource_showonwo;
	}
	public void setResource_showonwo(String resource_showonwo) {
		this.resource_showonwo = resource_showonwo;
	}
	public String getResource_totalcost() {
		return resource_totalcost;
	}
	public void setResource_totalcost(String resource_totalcost) {
		this.resource_totalcost = resource_totalcost;
	}
	public String getResource_unitcost() {
		return resource_unitcost;
	}
	public void setResource_unitcost(String resource_unitcost) {
		this.resource_unitcost = resource_unitcost;
	}
	
	public String getResource_tempid() {
		return resource_tempid;
	}
	public void setResource_tempid(String resource_tempid) {
		this.resource_tempid = resource_tempid;
	}
	
	
	public String getResource_tempshowonwo() {
		return resource_tempshowonwo;
	}
	public void setResource_tempshowonwo(String resource_tempshowonwo) {
		this.resource_tempshowonwo = resource_tempshowonwo;
	}
	public String getResource_dropshift() {
		return resource_dropshift;
	}
	public void setResource_dropshift(String resource_dropshift) {
		this.resource_dropshift = resource_dropshift;
	}
	public String getResource_tempdropshift() {
		return resource_tempdropshift;
	}
	public void setResource_tempdropshift(String resource_tempdropshift) {
		this.resource_tempdropshift = resource_tempdropshift;
	}
	public String getResource_act_qty() {
		return resource_act_qty;
	}
	public void setResource_act_qty(String resource_act_qty) {
		this.resource_act_qty = resource_act_qty;
	}
	public String getPvs_supplied() {
		return pvs_supplied;
	}
	public void setPvs_supplied(String pvs_supplied) {
		this.pvs_supplied = pvs_supplied;
	}
	public String getResource_temp_pvs_supplied() {
		return resource_temp_pvs_supplied;
	}
	public void setResource_temp_pvs_supplied(String resource_temp_pvs_supplied) {
		this.resource_temp_pvs_supplied = resource_temp_pvs_supplied;
	}
	public String getUnitcost() {
		return unitcost;
	}
	public void setUnitcost(String unitcost) {
		this.unitcost = unitcost;
	}
	public String getAct_qty() {
		return act_qty;
	}
	public void setAct_qty(String act_qty) {
		this.act_qty = act_qty;
	}
	public String getResource_subtotalcost() {
		return resource_subtotalcost;
	}
	public void setResource_subtotalcost(String resource_subtotalcost) {
		this.resource_subtotalcost = resource_subtotalcost;
	}
	
	public String getResource_authorizedqty() {
		return resource_authorizedqty;
	}
	public void setResource_authorizedqty(String resource_authorizedqty) {
		this.resource_authorizedqty = resource_authorizedqty;
	}
	public String getCredit_card() {
		return credit_card;
	}
	public void setCredit_card(String credit_card) {
		this.credit_card = credit_card;
	}
	public String getResource_temp_credit_card() {
		return resource_temp_credit_card;
	}
	public void setResource_temp_credit_card(String resource_temp_credit_card) {
		this.resource_temp_credit_card = resource_temp_credit_card;
	}
	
	//added by Seema
	public String getResource_type() {
		return resource_type;
	}
	public void setResource_type(String resource_type) {
		this.resource_type = resource_type;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getNonContractEstTotal() {
		return nonContractEstTotal;
	}
	public void setNonContractEstTotal(String nonContractEstTotal) {
		this.nonContractEstTotal = nonContractEstTotal;
	}
	
		
	
	
	
	
	
}
