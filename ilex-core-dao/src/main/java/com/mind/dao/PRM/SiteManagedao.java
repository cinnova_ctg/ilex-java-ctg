package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.prm.SiteManagementBean;
import com.mind.common.LabelValue;

public class SiteManagedao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SiteManagedao.class);

	public static void getSiteInformation(SiteManagementBean siteManage,
			String siteid, DataSource ds) {
		Connection conn = null;
		ResultSet rs = null;
		Statement stmt = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from dbo.func_lp_site_list('" + siteid
					+ "')";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				siteManage.setSiteID("" + siteid);
				siteManage.setSiteName(rs.getString("lp_si_name"));
				siteManage.setSiteNumber(rs.getString("lp_si_number"));
				siteManage.setSiteCity(rs.getString("lp_si_city"));
				siteManage.setState(rs.getString("lp_si_state"));
				siteManage.setSiteZipCode(rs.getString("lp_si_zip_code"));
				siteManage.setCountry(rs.getString("lp_si_country"));
				siteManage.setSiteCategory(rs.getString("lp_si_category_id"));
				siteManage.setSiteRegion(rs.getString("lp_si_region_id"));
				siteManage.setSiteLocalityFactor(rs
						.getString("lp_si_locality_factor"));
				siteManage.setSiteUnion(rs.getString("lp_si_union_site"));
				siteManage.setSiteWorkLocation(rs
						.getString("lp_si_work_location"));
				siteManage.setSiteDirections(rs.getString("lp_si_directions"));
				siteManage.setSitePrimaryEmail(rs
						.getString("lp_si_pri_email_id"));
				siteManage.setSitePrimaryName(rs.getString("lp_si_pri_poc"));
				siteManage.setSitePrimaryPhone(rs.getString("lp_si_phone_no"));
				siteManage.setSiteSecondaryEmail(rs
						.getString("lp_si_sec_email_id"));
				siteManage.setSiteSecondaryName(rs.getString("lp_si_sec_poc"));
				siteManage.setSiteSecondayPhone(rs
						.getString("lp_si_sec_phone_no"));
				siteManage
						.setSiteSpecialConditions(rs.getString("lp_si_notes"));
				siteManage.setSiteAddress(rs.getString("lp_si_address"));
				siteManage.setSiteStatus(rs.getString("lp_si_status"));
				siteManage.setSiteGroup(rs.getString("lp_si_group"));
				siteManage.setSiteEndCustomer(rs
						.getString("lp_si_end_customer"));
				siteManage.setSiteTimeZone(rs.getString("lp_si_time_zone"));
				siteManage.setClientHD_email(rs
						.getString("lp_si_clientHD_email"));
				siteManage.setClientHD_phone(rs
						.getString("lp_si_clientHD_phone_no"));
				siteManage
						.setClientHD_Name(rs.getString("lp_si_clientHD_name"));
			}
		} catch (Exception e) {
			logger.error(
					"getSiteInformation(SiteManagementForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteInformation(SiteManagementForm, String, DataSource) - Error occured during getting siteinfo  "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getSiteInformation(SiteManagementForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getSiteInformation(SiteManagementForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getSiteInformation(SiteManagementForm, String, DataSource) - exception ignored",
						e);
			}

		}

	}

	public static void getJobSiteInformation(SiteManagementBean siteManage,
			String siteid, DataSource ds) {
		Connection conn = null;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from dbo.func_lm_site_list('" + siteid
					+ "')";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				siteManage.setSiteID("" + siteid);
				siteManage.setSiteName(rs.getString("lm_si_name"));
				siteManage.setSiteNumber(rs.getString("lm_si_number"));
				siteManage.setSiteCity(rs.getString("lm_si_city"));
				siteManage.setState(rs.getString("lm_si_state"));
				siteManage.setSiteZipCode(rs.getString("lm_si_zip_code"));
				siteManage.setCountry(rs.getString("lm_si_country"));
				siteManage.setSiteCategory(rs.getString("lm_si_category_id"));
				siteManage.setSiteRegion(rs.getString("lm_si_region_id"));
				siteManage.setSiteLocalityFactor(rs
						.getString("lm_si_locality_factor"));
				siteManage.setSiteUnion(rs.getString("lm_si_union_site"));
				siteManage.setSiteWorkLocation(rs
						.getString("lm_si_work_location"));
				siteManage.setSiteDirections(rs.getString("lm_si_directions"));
				siteManage.setSitePrimaryEmail(rs
						.getString("lm_si_pri_email_id"));
				siteManage.setSitePrimaryName(rs.getString("lm_si_pri_poc"));
				siteManage.setSitePrimaryPhone(rs.getString("lm_si_phone_no"));
				siteManage.setSiteSecondaryEmail(rs
						.getString("lm_si_sec_email_id"));
				siteManage.setSiteSecondaryName(rs.getString("lm_si_sec_poc"));
				siteManage.setSiteSecondayPhone(rs
						.getString("lm_si_sec_phone_no"));
				siteManage
						.setSiteSpecialConditions(rs.getString("lm_si_notes"));
				siteManage.setSiteAddress(rs.getString("lm_si_address"));
				siteManage.setSiteStatus(rs.getString("lm_si_status"));
				siteManage.setSiteGroup(rs.getString("lm_si_group"));
				siteManage.setSiteEndCustomer(rs
						.getString("lm_si_end_customer"));
				siteManage.setSiteTimeZone(rs.getString("lm_si_time_zone"));
			}
		} catch (Exception e) {
			logger.error(
					"getJobSiteInformation(SiteManagementForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobSiteInformation(SiteManagementForm, String, DataSource) - Error occured during getting siteinfo  "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobSiteInformation(SiteManagementForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobSiteInformation(SiteManagementForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobSiteInformation(SiteManagementForm, String, DataSource) - exception ignored",
						e);
			}

		}

	}

	public static void getBlankSite(SiteManagementBean siteManage) {
		siteManage.setSiteID("0");
		siteManage.setCountry("US");
		siteManage.setSiteLocalityFactor("1.0");
		siteManage.setSiteUnion("N");
		siteManage.setSiteStatus("A");
	}

	public static long[] updateSite(SiteManagementBean siteManage,
			String action, String loginUserId, DataSource ds) {
		Connection conn = null;
		long[] retVal = new long[2];
		retVal[0] = -1;
		CallableStatement cstmt = null;

		/*
		 * System.out.println("3::::::::"+Integer.parseInt(siteManage.getMsaid())
		 * ); System.out.println("4::::::::"+siteManage.getSiteName());
		 * System.out.println("5::::::::"+siteManage.getSiteNumber());
		 * System.out.println("6::::::::"+siteManage.getSiteLocalityFactor());
		 * System.out.println("7::::::::"+siteManage.getSiteUnion());
		 * System.out.println("8::::::::"+siteManage.getSiteDesignator());
		 * System.out.println("9::::::::"+siteManage.getSiteWorkLocation());
		 * System.out.println("10::::::::"+siteManage.getSiteAddress());
		 * System.out.println("11::::::::"+siteManage.getSiteCity());
		 * System.out.println("12::::::::"+siteManage.getSiteState());
		 * System.out.println("13::::::::"+siteManage.getSiteZipCode());
		 * System.out.println("14::::::::"+siteManage.getSiteSecondayPhone());
		 * System.out.println("15::::::::"+siteManage.getSitePrimaryPhone());
		 * System.out.println("16::::::::"+siteManage.getSiteCountry());
		 * System.out.println("17::::::::"+siteManage.getSiteDirections());
		 * System.out.println("18::::::::"+siteManage.getSitePrimaryName());
		 * System.out.println("19::::::::"+siteManage.getSiteSecondaryName());
		 * System.out.println("20::::::::"+siteManage.getSitePrimaryEmail());
		 * System.out.println("21::::::::"+siteManage.getSiteSecondaryEmail());
		 * System
		 * .out.println("22::::::::"+siteManage.getSiteSpecialConditions());
		 * System.out.println("23::::::::"+action);
		 * System.out.println("24::::::::"+siteManage.getSiteStatus());
		 * System.out.println("25::::::::"+ loginUserId);
		 * System.out.println("26::::::::"+siteManage.getSiteGroup());
		 * System.out.println("27::::::::"+siteManage.getSiteEndCustomer());
		 * System.out.println("28::::::::"+siteManage.getSiteCategory());
		 * System.out.println("29::::::::"+siteManage.getSiteRegion());
		 * System.out.println("30::::::::"+siteManage.getSiteTimeZone());
		 * System.out.println("31::::::::"+siteManage.getLatitude());
		 * System.out.println("32::::::::"+siteManage.getLongitude());
		 * System.out.println("33::::::::"+siteManage.getLatLongAccuracy());
		 * System.out.println("34::::::::"+siteManage.getLatLongSource());
		 * System.out.println("35::::::::"+siteManage.getJobid());
		 * System.out.println("36::::::::"+siteManage.getFromType());
		 */

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lp_site_master_manage(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(siteManage.getSiteID()));
			cstmt.setInt(3, Integer.parseInt(siteManage.getMsaid()));
			cstmt.setString(4, siteManage.getSiteName());
			cstmt.setString(5, siteManage.getSiteNumber());
			cstmt.setString(6, siteManage.getSiteLocalityFactor());
			cstmt.setString(7, siteManage.getSiteUnion());
			cstmt.setString(8, siteManage.getSiteDesignator());
			cstmt.setString(9, siteManage.getSiteWorkLocation());
			cstmt.setString(10, siteManage.getSiteAddress());
			cstmt.setString(11, siteManage.getSiteCity());
			cstmt.setString(12, siteManage.getState());
			cstmt.setString(13, siteManage.getSiteZipCode());
			cstmt.setString(14, siteManage.getSiteSecondayPhone());
			cstmt.setString(15, siteManage.getSitePrimaryPhone());
			cstmt.setString(16, siteManage.getCountry());
			cstmt.setString(17, siteManage.getSiteDirections());
			cstmt.setString(18, siteManage.getSitePrimaryName());
			cstmt.setString(19, siteManage.getSiteSecondaryName());
			cstmt.setString(20, siteManage.getSitePrimaryEmail());
			cstmt.setString(21, siteManage.getSiteSecondaryEmail());
			cstmt.setString(22, siteManage.getSiteSpecialConditions());
			cstmt.setString(23, action);
			cstmt.setString(24, siteManage.getSiteStatus());
			cstmt.setString(25, loginUserId);
			cstmt.setString(26, siteManage.getSiteGroup());
			cstmt.setString(27, siteManage.getSiteEndCustomer());
			cstmt.setString(28, siteManage.getSiteCategory());
			cstmt.setString(29, siteManage.getSiteRegion());
			cstmt.setString(30, siteManage.getSiteTimeZone());
			cstmt.setString(31, siteManage.getLatitude());
			cstmt.setString(32, siteManage.getLongitude());
			cstmt.setString(33, siteManage.getLatLongAccuracy());
			cstmt.setString(34, siteManage.getLatLongSource());
			cstmt.setString(35, siteManage.getJobid());
			cstmt.setString(36, siteManage.getFromType());
			cstmt.registerOutParameter(37, java.sql.Types.INTEGER);
			cstmt.setString(38, siteManage.getClientHD_Name());
			cstmt.setString(39, siteManage.getClientHD_phone());
			cstmt.setString(40, siteManage.getClientHD_email());
			cstmt.execute();
			retVal[0] = cstmt.getInt(1);
			retVal[1] = cstmt.getInt(37);

		}

		catch (SQLException e) {
			logger.error(
					"updateSite(SiteManagementForm, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateSite(SiteManagementForm, String, String, DataSource) - Exception caught in updating site"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"updateSite(SiteManagementForm, String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("updateSite(SiteManagementForm, String, String, DataSource) - Exception Caught in updating site"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"updateSite(SiteManagementForm, String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("updateSite(SiteManagementForm, String, String, DataSource) - Exception Caught in updating category"
							+ sql);
				}
			}

		}
		return retVal;
	}

	public static int deleteSite(int siteid, DataSource ds) {
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;

		try {

			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lp_site_master_delete(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, siteid);
			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error("deleteSite(int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteSite(int, DataSource) - Exception caught in deleting site"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error("deleteSite(int, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteSite(int, DataSource) - Exception Caught in deleting site"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("deleteSite(int, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteSite(int, DataSource) - Exception Caught in updating category"
							+ sql);
				}
			}

		}
		return val;
	}

	public static ArrayList<SiteManagementBean> getStateCategoryList(
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<SiteManagementBean> valuelist = new ArrayList<SiteManagementBean>();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select cp_country_id, cp_country_name, cp_country_order from lp_country_list order by cp_country_order,cp_country_name";
			rs = stmt.executeQuery(sql);
			int i = 0;

			if (i == 0) {
				SiteManagementBean siteManage = new SiteManagementBean();
				siteManage.setSiteCountryId("");
				siteManage.setSiteCountryIdName("");
				valuelist.add(i, siteManage);
				i++;
			}

			while (rs.next()) {
				SiteManagementBean siteManage = new SiteManagementBean();

				siteManage.setSiteCountryId(rs.getString("cp_country_id"));
				siteManage
						.setSiteCountryIdName(rs.getString("cp_country_name"));
				valuelist.add(i, siteManage);
				i++;
			}

		} catch (Exception e) {
			logger.error("getStateCategoryList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStateCategoryList(DataSource) - Error occured during getting State Category Wise List  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static ArrayList<SiteManagementBean> getStateCategoryList(
			String msaId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<SiteManagementBean> valuelist = new ArrayList<SiteManagementBean>();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select cp_country_id,cp_country_name from func_get_site_country("
							+ msaId + ")");
			int i = 0;

			if (i == 0) {
				SiteManagementBean siteManage = new SiteManagementBean();
				siteManage.setSiteCountryId("");
				siteManage.setSiteCountryIdName("");
				valuelist.add(i, siteManage);
				i++;
			}

			while (rs.next()) {
				SiteManagementBean siteManage = new SiteManagementBean();

				siteManage.setSiteCountryId(rs.getString("cp_country_id"));
				siteManage
						.setSiteCountryIdName(rs.getString("cp_country_name"));
				valuelist.add(i, siteManage);
				i++;
			}

		} catch (Exception e) {
			logger.error("getStateCategoryList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStateCategoryList(String, DataSource) - Error occured during getting State Category Wise List for Site ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getStateCategoryList(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(String, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static ArrayList<LabelValue> getCategoryWiseStateList(String cat_id,
			int count, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<LabelValue> valuelist = new ArrayList<LabelValue>();
		if (count == 0)
			valuelist.add(new LabelValue("--Select--", "0"));
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from dbo.func_state_category('" + cat_id
					+ "')";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				valuelist.add(new LabelValue(rs.getString("cp_state_id"), (rs
						.getString("cp_state_id") + "," + rs
						.getString("cp_country_id"))));
			}

		} catch (Exception e) {
			logger.error("getCategoryWiseStateList(String, int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCategoryWiseStateList(String, int, DataSource) - Error occured during getting State  List  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCategoryWiseStateList(String, int, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCategoryWiseStateList(String, int, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCategoryWiseStateList(String, int, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static void getRefershedStates(SiteManagementBean siteManage,
			String check, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (check.equals("state")) {

				String sql = "select cp_state_id,cp_country_id,cp_category_id,cp_region_id  from cp_state where cp_state_id ='"
						+ siteManage.getState() + "'";

				rs = stmt.executeQuery(sql);

				if (rs.next()) {

					siteManage.setState(rs.getString("cp_state_id"));
					siteManage.setCountry(rs.getString("cp_country_id"));
					siteManage.setSiteCategory(rs.getString("cp_category_id"));
					siteManage.setSiteRegion(rs.getString("cp_region_id"));
				} else {
					siteManage.setState("0");
					siteManage.setCountry("0");
					siteManage.setSiteCategory("0");
					siteManage.setSiteRegion("0");
				}

			}

			if (check.equals("country")) {

				String sql = "select cp_country_id,cp_category_id,cp_region_id  from cp_state where cp_country_id ='"
						+ siteManage.getCountry() + "'";
				rs = stmt.executeQuery(sql);

				if (rs.next()) {

					siteManage.setCountry(rs.getString("cp_country_id"));
					siteManage.setSiteCategory(rs.getString("cp_category_id"));
					siteManage.setSiteRegion(rs.getString("cp_region_id"));
				} else {
					siteManage.setCountry("0");
					siteManage.setSiteCategory("0");
					siteManage.setSiteRegion("0");
				}

			}

		} catch (Exception e) {
			logger.error(
					"getRefershedStates(SiteManagementForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRefershedStates(SiteManagementForm, String, DataSource) - Error occured during getting country , Category  , Region   "
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getRefershedStates(SiteManagementForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getRefershedStates(SiteManagementForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getRefershedStates(SiteManagementForm, String, DataSource) - exception ignored",
						e);
			}

		}

	}

	public static void getWebTicketSiteDetails(SiteManagementBean siteManage,
			String ticketNumber, DataSource ds) {
		Connection conn = null;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from dbo.web_ticket_summary where cc_wtc_tc_number ="
					+ ticketNumber;
			if (logger.isDebugEnabled()) {
				logger.debug("getWebTicketSiteDetails(SiteManagementForm, String, DataSource) - sql for web ticket site details----"
						+ sql);
			}
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				siteManage.setSiteNumber(rs.getString("cc_wtc_site_number"));
				siteManage.setSiteWorkLocation(rs
						.getString("cc_wtc_site_worklocation"));

				siteManage.setSiteAddress(rs.getString("cc_wtc_site_address"));
				siteManage.setSiteCity(rs.getString("cc_wtc_site_city"));
				siteManage.setState(rs.getString("cc_wtc_site_state"));
				siteManage.setSiteZipCode(rs.getString("cc_wtc_site_zipcode"));
				siteManage.setSiteName(rs.getString("cc_wtc_site_name"));

				siteManage.setSitePrimaryName(rs
						.getString("cc_wtc_primary_Name"));
				siteManage.setSitePrimaryPhone(rs
						.getString("cc_wtc_pri_site_phone"));

				siteManage.setSiteSecondaryName(rs
						.getString("cc_wtc_secondary_Name"));
				siteManage.setSiteSecondayPhone(rs
						.getString("cc_wtc_secondary_phone"));

				siteManage.setCountry("");
			}
		} catch (Exception e) {
			logger.error(
					"getWebTicketSiteDetails(SiteManagementForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getWebTicketSiteDetails(SiteManagementForm, String, DataSource) - Error occured during getting siteinfo for web ticket"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getWebTicketSiteDetails(SiteManagementForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getWebTicketSiteDetails(SiteManagementForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getWebTicketSiteDetails(SiteManagementForm, String, DataSource) - exception ignored",
						e);
			}

		}

	}

}
