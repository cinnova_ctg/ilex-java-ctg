package com.mind.dao.PRM;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.newjobdb.JobDashboardBean;

public class JobSetUpDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JobSetUpDao.class);

	public String updateWorkflowChecklist() {
		if (logger.isDebugEnabled()) {
			logger.debug("updateWorkflowChecklist() - Hiiiiiiiiii");
		}
		return "hii";
	}

	public void getActivity(JobDashboardBean bean, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select  top 1* from lm_activity where lm_at_type='V'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				// bean.setActivity_Id(rs.getString("lm_at_id"));
				// bean.setActivitylibrary_Id(rs.getString("lm_at_lib_id"));
			}
		} catch (Exception e) {
			logger.error("getActivity(JobDashboardForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivity(JobDashboardForm, DataSource) - Exception in retrieving job status and type"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivity(JobDashboardForm, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivity(JobDashboardForm, DataSource) - Exception in retrieving job status and type"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivity(JobDashboardForm, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivity(JobDashboardForm, DataSource) - Exception in retrieving job status and type"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivity(JobDashboardForm, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivity(JobDashboardForm, DataSource) - Exception in retrieving job status and type"
							+ sql);
				}
			}

		}
	}

	public static String getjobType(String jsId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String jobType = "";
		java.util.Date dateIn = new java.util.Date();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select  lm_js_type from lm_job where lm_js_id = "
					+ jsId;
			if (logger.isDebugEnabled()) {
				logger.debug("getjobType(String, DataSource) - " + sql);
			}
			rs = stmt.executeQuery(sql);

			if (rs.next())
				jobType = rs.getString(1);
		} catch (Exception e) {
			logger.error("getjobType(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getjobType(String, DataSource) - Exception in retrieving job status and type"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getjobType(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getjobType(String, DataSource) - Exception in retrieving job status and type"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getjobType(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getjobType(String, DataSource) - Exception in retrieving job status and type"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getjobType(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getjobType(String, DataSource) - Exception in retrieving job status and type"
							+ sql);
				}
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("getjobType(String, DataSource) - **** getjobType :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return jobType;
	}

	public static String getJobStatus(JobDashboardBean bean, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String jobStatus = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select  lm_js_prj_display_status from lm_job where lm_js_id = "
					+ bean.getJobId();
			rs = stmt.executeQuery(sql);
			if (rs.next())
				jobStatus = rs.getString(1);
		} catch (Exception e) {
			logger.error("getJobStatus(JobDashboardForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobStatus(JobDashboardForm, DataSource) - Exception in retrieving job status and type"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getJobStatus(JobDashboardForm, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getJobStatus(JobDashboardForm, DataSource) - Exception in retrieving job status and type"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getJobStatus(JobDashboardForm, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getJobStatus(JobDashboardForm, DataSource) - Exception in retrieving job status and type"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getJobStatus(JobDashboardForm, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getJobStatus(JobDashboardForm, DataSource) - Exception in retrieving job status and type"
							+ sql);
				}
			}

		}
		return jobStatus;
	}

	public static String[] getRequestorDetailsByTicketId(String jsId,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] requestorDtls = null;
		java.util.Date dateIn = new java.util.Date();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT lm_tc_id, lm_tc_number, lm_tc_requestor_name, lm_tc_requestor_email FROM lm_ticket WHERE lm_tc_js_id = "
					+ jsId;
			if (logger.isDebugEnabled()) {
				logger.debug("getjobType(String, DataSource) - " + sql);
			}
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				requestorDtls = new String[4];
				requestorDtls[0] = rs.getString("lm_tc_id");
				requestorDtls[1] = rs.getString("lm_tc_number");
				requestorDtls[2] = rs.getString("lm_tc_requestor_name");
				requestorDtls[3] = rs.getString("lm_tc_requestor_email");
			}
			// jobType = rs.getString(1);
		} catch (Exception e) {
			logger.error("getRequestorDetailsByTicketId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRequestorDetailsByTicketId(String, DataSource) - Exception in retrieving job status and type"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getRequestorDetailsByTicketId(String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getRequestorDetailsByTicketId(String, DataSource) - Exception in retrieving job status and type"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getRequestorDetailsByTicketId(String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getRequestorDetailsByTicketId(String, DataSource) - Exception in retrieving job status and type"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getRequestorDetailsByTicketId(String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getRequestorDetailsByTicketId(String, DataSource) - Exception in retrieving job status and type"
							+ sql);
				}
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("getRequestorDetailsByTicketId(String, DataSource) - **** getjobType :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return requestorDtls;
	}
}
