package com.mind.dao.PRM;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.ShippingInstructionSiteInfo;
import com.mind.fw.core.dao.util.DBUtil;

public class ShippingInstructionDAO {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ShippingInstructionDAO.class);

	/**
	 * The purpose of this method is to get shipping instruction data on the
	 * basis of job id and lr_jc_name column of lr_job_custom_field_used table.
	 * 
	 * @param jobId
	 *            the job id
	 * @param ds
	 *            the DataSource
	 * 
	 * @return the shipping instruction data
	 */
	public static HashMap<String, String> getShippingInstructionData(
			String jobId, DataSource ds) {

		logger.debug("getShippingInstructionData(String, String, DataSource) - jobId="
				+ jobId);

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		HashMap<String, String> shippingInstructionMap = new HashMap<String, String>();
		String sql = "";
		String defaultValue = "Not Available";

		try {
			conn = ds.getConnection();
			sql = "SELECT lr_jc_name,lr_jc_value FROM lr_job_custom_field_used where lr_jc_ci_js_id=? "
					+ "and lr_jc_name in('Required Delivery Date','Circuit Type','POTS TYPE', 'Modem Location')";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobId);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				if (rs.getString(2) == null || rs.getString(2).equals(""))
					shippingInstructionMap.put(rs.getString(1), defaultValue);
				else
					shippingInstructionMap
							.put(rs.getString(1), rs.getString(2));
			} // - end of while
		} // - end of try
		catch (Exception e) {
			logger.error(
					"getShippingInstructionData(String, String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return shippingInstructionMap;

	}// - end of getShippingInstructionData()

	/**
	 * This method is use to get the Site information of particular job.
	 * 
	 * @param jobId
	 *            the job id
	 * @param ds
	 *            the DataSource
	 * 
	 * @return the ShippingInstructionSiteInfo bean which has site information.
	 */
	public static ShippingInstructionSiteInfo getJobSiteInfo(String jobId,
			DataSource ds) {

		logger.debug("getJobSiteInfo(String, String, DataSource) - jobId="
				+ jobId);

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ShippingInstructionSiteInfo siteInfo = null;
		String sql = "";

		try {
			conn = ds.getConnection();
			sql = "SELECT lm_si_number = isnull(lm_si_number, ''), lm_si_address = isnull(lm_si_address, ''),"
					+ "lm_si_city = isnull(lm_si_city, ''), lm_si_state = isnull(lm_si_state, ''), "
					+ "lm_si_zip_code = isnull(lm_si_zip_code, ''), lm_si_pri_poc = isnull(lm_si_pri_poc, ''), "
					+ "msa_name = isnull(lo_ot_name, '') "
					+ "from lm_job inner join lm_site_detail on lm_js_si_id = lm_si_id "
					+ "inner join lx_appendix_main on lx_pr_id = lm_js_pr_id "
					+ "inner join lp_msa_main on lp_mm_id = lx_pr_mm_id "
					+ "inner join lo_organization_top on lp_mm_ot_id = lo_ot_id "
					+ "where lm_js_id =?";

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobId);
			rs = pStmt.executeQuery();

			while (rs.next()) {
				siteInfo = new ShippingInstructionSiteInfo();
				siteInfo.setSite_number(rs.getString("lm_si_number"));
				siteInfo.setSite_address(rs.getString("lm_si_address"));
				siteInfo.setSite_city(rs.getString("lm_si_city"));
				siteInfo.setSite_state(rs.getString("lm_si_state"));
				siteInfo.setSite_zip(rs.getString("lm_si_zip_code"));
				siteInfo.setSite_primary_poc(rs.getString("lm_si_pri_poc"));
				siteInfo.setMsaName(rs.getString("msa_name"));
			} // - end of while
		} // - end of try
		catch (Exception e) {
			logger.error(
					"getShippingInstructionData(String, String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return siteInfo;

	}// - end of getShippingInstructionData()
}
