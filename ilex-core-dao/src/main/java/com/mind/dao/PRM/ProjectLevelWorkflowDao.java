package com.mind.dao.PRM;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.newjobdb.ProjectWorkflowItem;
import com.mind.bean.prm.ProjectLevelWorkflowBean;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.newjobdb.dao.ProjectWorkflowChecklist;

public class ProjectLevelWorkflowDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ProjectLevelWorkflowDao.class);

	public static ProjectWorkflowChecklist getProjectWorkflowChecklist(
			long appendixId, DataSource ds) {
		ProjectWorkflowChecklist projectWorkflowChecklist = new ProjectWorkflowChecklist();
		projectWorkflowChecklist
				.setProjectWorkflowItems(fetchPrjCheckListItems(appendixId, ds));
		return projectWorkflowChecklist;
	}

	public static void getAppendixInformation(String appendixId,
			ProjectLevelWorkflowBean jobWorkflowForm, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select lam.lx_pr_id,lam.lx_pr_title,lmn.lp_mm_id,lot.lo_ot_name from lp_msa_main lmn join lo_organization_top lot on"
					+ " \n lp_mm_ot_id = lo_ot_id "
					+ " \n join lx_appendix_main lam on"
					+ " \n lmn.lp_mm_id = lam.lx_pr_mm_id"
					+ " \n where lx_pr_id = ?";

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, appendixId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				jobWorkflowForm.setAppendixId(rs.getString("lx_pr_id"));
				jobWorkflowForm.setAppendixName(rs.getString("lx_pr_title"));
				jobWorkflowForm.setMsaId(rs.getString("lp_mm_id"));
				jobWorkflowForm.setMsaName(rs.getString("lo_ot_name"));
			}
		} catch (Exception e) {
			logger.error(
					"getAppendixInformation(String, ProjectLevelWorkflowForm, DataSource)",
					e);

			logger.error(
					"getAppendixInformation(String, ProjectLevelWorkflowForm, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

	public static ArrayList<ProjectWorkflowItem> fetchPrjCheckListItems(
			long appendixId, DataSource ds) {
		ProjectWorkflowItem projectWorkflowItem;
		ArrayList<ProjectWorkflowItem> projectWorkflowItemList = new ArrayList<ProjectWorkflowItem>();

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select * from dbo.prj_lvl_job_chk_items where appendix_id=? ";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, Long.toString(appendixId));
			rs = pStmt.executeQuery();
			while (rs.next()) {
				projectWorkflowItem = new ProjectWorkflowItem();
				projectWorkflowItem.setItemId(rs.getInt("item_id"));
				projectWorkflowItem.setDescription(rs.getString("description"));
				projectWorkflowItem.setReferenceDate(rs
						.getString("reference_date"));
				projectWorkflowItem.setSequence(rs.getString("sequence"));
				projectWorkflowItem.setAppendixId(Integer.toString(rs
						.getInt("appendix_id")));
				projectWorkflowItem.setUser(rs.getString("user_id"));
				projectWorkflowItemList.add(projectWorkflowItem);
			}

		} catch (Exception e) {
			logger.error("fetchPrjCheckListItems(long, DataSource)", e);

			logger.error("fetchPrjCheckListItems(long, DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);

		}
		return projectWorkflowItemList;
	}

	public boolean upsertCheckListItems(
			ProjectLevelWorkflowBean projectLevelWorkflowForm, DataSource ds,
			String loginUser) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] seq = projectLevelWorkflowForm.getSeq();
		String[] desc = projectLevelWorkflowForm.getDesc();
		String[] item_Id = projectLevelWorkflowForm.getItem_id();
		ArrayList projectWorkflowItemList = ProjectLevelWorkflowDao
				.fetchPrjCheckListItems(
						new Long(projectLevelWorkflowForm.getAppendixId()), ds);
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		int len = (item_Id == null ? 0 : item_Id.length);
		boolean result = true;

		try {
			conn = ds.getConnection();
			for (int i = 0; i < len; i++) {
				for (int j = 0; j < projectWorkflowItemList.size(); j++) {
					ProjectWorkflowItem projectWorkflowItem = (ProjectWorkflowItem) projectWorkflowItemList
							.get(j);
					String tableItemId = String.valueOf(
							projectWorkflowItem.getItemId()).toString();
					if (logger.isDebugEnabled()) {
						logger.debug("upsertCheckListItems(ProjectLevelWorkflowForm, DataSource, String) - item_Id"
								+ item_Id[i] + " : : " + item_Id.length);
					}
					if (tableItemId.equals(item_Id[i])) {
						if (logger.isDebugEnabled()) {
							logger.debug("upsertCheckListItems(ProjectLevelWorkflowForm, DataSource, String) - in update "
									+ item_Id[i]);
						}
						stmt = conn.createStatement();
						String sql = "update dbo.prj_lvl_job_chk_items"
								+ "\n set sequence = ?,"
								+ "\n description = ?," + "\n updated_by = ?,"
								+ "\n updated_date = ?," + "\n user_id = ?,"
								+ "\n reference_date = ?"
								+ "\n where appendix_id = ? "
								+ " and item_id = ?";
						pStmt = conn.prepareStatement(sql);
						pStmt.setString(1, seq[i]);
						pStmt.setString(2, desc[i]);
						pStmt.setString(3, loginUser);
						pStmt.setTimestamp(4, ts);
						pStmt.setString(5, loginUser);
						pStmt.setTimestamp(6, ts);
						pStmt.setLong(
								7,
								new Long(projectLevelWorkflowForm
										.getAppendixId()).longValue());
						pStmt.setLong(8, new Long(item_Id[i]));
						int isUpdate = pStmt.executeUpdate();
						if (isUpdate == 1) {
							result = false;
						}
						if (logger.isDebugEnabled()) {
							logger.debug("upsertCheckListItems(ProjectLevelWorkflowForm, DataSource, String) - isUpdate :: "
									+ isUpdate);
						}
					}
				}
				if (item_Id[i].equals("0")) {
					if (logger.isDebugEnabled()) {
						logger.debug("upsertCheckListItems(ProjectLevelWorkflowForm, DataSource, String) - insert in table seq"
								+ seq[i]);
					}
					String sql = "insert into dbo.prj_lvl_job_chk_items"
							+ "\n (appendix_id,sequence,description,created_by,created_date,reference_date,user_id)"
							+ " \n values (?,?,?,?,?,?,?)";
					pStmt = conn.prepareStatement(sql);
					pStmt.setLong(1,
							new Long(projectLevelWorkflowForm.getAppendixId())
									.longValue());
					pStmt.setFloat(2, new Float(seq[i]).floatValue());
					pStmt.setString(3, desc[i]);
					pStmt.setString(4, loginUser);
					pStmt.setTimestamp(5, ts);
					pStmt.setTimestamp(6, ts);
					pStmt.setString(7, loginUser);
					result = pStmt.execute();
				}
			}
			for (int j = 0; j < projectWorkflowItemList.size(); j++) {
				ProjectWorkflowItem projectWorkflowItem = (ProjectWorkflowItem) projectWorkflowItemList
						.get(j);
				String tableItemId = String.valueOf(
						projectWorkflowItem.getItemId()).toString();
				deleteCheckListItems(ds, projectLevelWorkflowForm, tableItemId,
						item_Id);
			}
		} catch (NumberFormatException e1) {
			logger.error(
					"upsertCheckListItems(ProjectLevelWorkflowForm, DataSource, String)",
					e1);

		} catch (SQLException e1) {
			logger.error(
					"upsertCheckListItems(ProjectLevelWorkflowForm, DataSource, String)",
					e1);

		} finally {
			DBUtil.close(stmt);
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return result;
	}

	public void deleteCheckListItems(DataSource ds,
			ProjectLevelWorkflowBean projectLevelWorkflowForm,
			String tableItemId, String[] item_Id) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		boolean flag = true;
		int len = (item_Id == null ? 0 : item_Id.length);
		for (int k = 0; k < len; k++) {
			if (tableItemId.equals(item_Id[k])) {
				flag = false;
			}
		}
		if (flag == true) {
			if (logger.isDebugEnabled()) {
				logger.debug("deleteCheckListItems(DataSource, ProjectLevelWorkflowForm, String, String[]) - in delete "
						+ tableItemId);
			}
			String sql = "delete from dbo.prj_lvl_job_chk_items"
					+ "\n where appendix_id=?" + " \n and item_id=?";
			try {
				conn = ds.getConnection();
				pStmt = conn.prepareStatement(sql);
				pStmt.setLong(1,
						new Long(projectLevelWorkflowForm.getAppendixId())
								.longValue());
				pStmt.setLong(2, new Long(tableItemId));
				pStmt.execute();
			} catch (NumberFormatException e) {
				logger.error(
						"deleteCheckListItems(DataSource, ProjectLevelWorkflowForm, String, String[])",
						e);

				logger.error(
						"deleteCheckListItems(DataSource, ProjectLevelWorkflowForm, String, String[])",
						e);
			} catch (SQLException e) {
				logger.error(
						"deleteCheckListItems(DataSource, ProjectLevelWorkflowForm, String, String[])",
						e);

				logger.error(
						"deleteCheckListItems(DataSource, ProjectLevelWorkflowForm, String, String[])",
						e);
			} finally {
				DBUtil.close(pStmt);
				DBUtil.close(conn);
			}
		}
	}
}
