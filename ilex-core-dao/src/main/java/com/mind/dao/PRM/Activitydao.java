package com.mind.dao.PRM;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class Activitydao
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Activitydao.class);

	public  static String getCurrentstatus(String activityid, DataSource ds)
	{
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String currentstatus="";
		java.util.Date dateIn = new java.util.Date();
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			
			String sql="select isnull(lm_at_prj_status,'I') from lm_activity where lm_at_id="+activityid;						   		

			if (logger.isDebugEnabled()) {
				logger.debug("getCurrentstatus(String, DataSource) - XXXXX sql " + sql);
			}
			rs = stmt.executeQuery(sql);
						
			if(rs.next())
			{
			
				 currentstatus=rs.getString(1);
				
			}
			
		}
		catch(Exception e){
			logger.error("getCurrentstatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCurrentstatus(String, DataSource) - Error occured during getting current status  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getCurrentstatus(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getCurrentstatus(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getCurrentstatus(String, DataSource) - exception ignored", e);
}
			
		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("getCurrentstatus(String, DataSource) - **** getCurrentstatus :: time taken :: " + (dateOut.getTime() - dateIn.getTime()));
		}
		return currentstatus;	
		
	}
	
	public  static String[] getActivityschedule(String activityid,String type, DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String[] value=new String[2];
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select  lx_se_start,lx_se_end from lx_schedule_list_01 where lx_se_type_id="+activityid+" and lx_se_type = '"+type+"'";						   		
			rs = stmt.executeQuery(sql);
			
			if(rs.next())
			{
				 value[0]=rs.getString("lx_se_start");
				 value[1]=rs.getString("lx_se_end");
			}
			
		}
		catch(Exception e){
			logger.error("getActivityschedule(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityschedule(String, String, DataSource) - Error occured during getting jobschedule  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getActivityschedule(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getActivityschedule(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getActivityschedule(String, String, DataSource) - exception ignored", e);
}
			
		}
		return value;	
	}
	
	
	public  static String getJobid(String activityid,DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String jobid="";
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select lm_at_js_id from lm_activity where lm_at_id="+activityid+" ";						   		
			rs = stmt.executeQuery(sql);
						
			if(rs.next())
			{
			
				jobid=rs.getString("lm_at_js_id");
			}
			
		}
		catch(Exception e){
			logger.error("getJobid(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobid(String, DataSource) - Error occured during getting Job id  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getJobid(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getJobid(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getJobid(String, DataSource) - exception ignored", e);
}
			
		}
		return jobid;	
	}
	
	
	public  static String getJobname(String jobid,DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String jobname="";
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select lm_js_title from lm_job where lm_js_id="+jobid+" ";						   		
			rs = stmt.executeQuery(sql);
						
			if(rs.next())
			{
			
				jobname=rs.getString("lm_js_title");
			}
			
		}
		catch(Exception e){
			logger.error("getJobname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobname(String, DataSource) - Error occured during getting Job name  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getJobname(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getJobname(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getJobname(String, DataSource) - exception ignored", e);
}
			
		}
		return jobname;	
	}
	
	
	public  static String getAppendixid(String jobid,DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String Appendixid="";
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select lm_js_pr_id from lm_job where lm_js_id="+jobid+" ";						   		
			rs = stmt.executeQuery(sql);
						
			if(rs.next())
			{
			
				Appendixid=rs.getString("lm_js_pr_id");
			}
			
		}
		catch(Exception e){
			logger.error("getAppendixid(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixid(String, DataSource) - Error occured during getting Job name  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getAppendixid(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getAppendixid(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getAppendixid(String, DataSource) - exception ignored", e);
}
			
		}
		return Appendixid;	
	}
	
	
	public  static String getAppendixname(String appendixid, DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String appendixname="";
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select lx_pr_title from lx_appendix_main where lx_pr_id="+appendixid;						   		
			rs = stmt.executeQuery(sql);
						
			if(rs.next())
			{
			
				appendixname=rs.getString("lx_pr_title");
			}
			
		}
		catch(Exception e){
			logger.error("getAppendixname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixname(String, DataSource) - Error occured during getting Appendix name  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getAppendixname(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getAppendixname(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getAppendixname(String, DataSource) - exception ignored", e);
}
			
		}
		return appendixname;	
	}
	
	
	public  static String getActivityname(String activityid, DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String activityname="";
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select lm_at_title from lm_activity where lm_at_id="+activityid+" ";						   		
			rs = stmt.executeQuery(sql);
						
			if(rs.next())
			{
			
				activityname=rs.getString("lm_at_title");
			}
			
		}
		catch(Exception e){
			logger.error("getActivityname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityname(String, DataSource) - Error occured during getting activity name  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getActivityname(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getActivityname(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getActivityname(String, DataSource) - exception ignored", e);
}
			
		}
		return activityname;	
	}
	
	
//	job type name is Default/Newjob
	public  static String getJobtypename(String jobid,DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String jobtypename="";
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select lm_js_type from lm_job where lm_js_id="+jobid;						   		
			rs = stmt.executeQuery(sql);
						
			if(rs.next())
			{
			
				jobtypename=rs.getString("lm_js_type");
			}
			
		}
		catch(Exception e){
			logger.error("getJobtypename(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobtypename(String, DataSource) - Error occured during getting Job type name");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getJobtypename(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getJobtypename(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getJobtypename(String, DataSource) - exception ignored", e);
}
			
		}
		return jobtypename;	
	}
	
	
	public  static String getPartnerassigned(String typeid,String type,DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String partnername="";
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			
			String sql="select dbo.func_partner_assigned_name("+typeid+",'"+type+"')";
			//System.out.println("XXXXXSQL partnername "+sql);
			rs = stmt.executeQuery(sql);
						
			if(rs.next())
			{
			
				partnername=rs.getString(1);
			}
			
		}
		catch(Exception e){
			logger.error("getPartnerassigned(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerassigned(String, String, DataSource) - Error occured during getting partner name  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getPartnerassigned(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getPartnerassigned(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getPartnerassigned(String, String, DataSource) - exception ignored", e);
}
			
		}
		return partnername;	
	}
	
	public  static String getPartnerid(String typeid,String type,DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String partnerid="0";
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			
			String sql="select dbo.func_partner_assigned_id("+typeid+",'"+type+"')";
			//System.out.println("XXXXXSQL partnerid "+sql);
			rs = stmt.executeQuery(sql);
						
			if(rs.next())
			{
			
				partnerid=rs.getString(1);
			}
			
		}
		catch(Exception e){
			logger.error("getPartnerid(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerid(String, String, DataSource) - Error occured during getting partner id  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getPartnerid(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getPartnerid(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getPartnerid(String, String, DataSource) - exception ignored", e);
}
			
		}
		return partnerid;	
	}
		
		
	

}
