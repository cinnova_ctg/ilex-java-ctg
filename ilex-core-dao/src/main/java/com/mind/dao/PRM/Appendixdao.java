/**
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: This is a data object access class used for managing Appendix Dashboard and showing JobDetailList under the Appendix.      
 *
 */

package com.mind.dao.PRM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.mind.bean.pm.Appendix;
import com.mind.bean.prm.AppendixHeaderBean;
import com.mind.common.Decimalroundup;
import com.mind.common.bean.Sow;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * Methods :
 * getJobdetaillist,getAppendixname,getCurrentstatus,getAppendixschedule
 * ,getPLsummary,getaddendumjoblist
 */

public class Appendixdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Appendixdao.class);

	/**
	 * This method gets Job Detail List under a Appendix.
	 * 
	 * @param aid
	 *            String. Appendix id
	 * @param querystring
	 *            String. sorting parameter, according to which joblist would
	 *            get sorted
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, job Detail list.
	 */
	public static java.util.Collection getjobOwnerList(String app_id,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new java.util.ArrayList();
		valuelist.add(new com.mind.common.LabelValue("--Select--", " "));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select distinct job_created_by = dbo.func_cc_poc_name(isnull(lm_js_created_by,0)) from lm_job where lm_js_pr_id like '"
					+ app_id + "'";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				if (rs.getString("job_created_by") != null)
					valuelist.add(new com.mind.common.LabelValue(rs
							.getString("job_created_by"), rs
							.getString("job_created_by")));
			}

		} catch (Exception e) {
			logger.error("getjobOwnerList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getjobOwnerList(String, DataSource) - Error occured during getting Job Owner List  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getjobOwnerList(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getjobOwnerList(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getjobOwnerList(String, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static ArrayList getJobSearchdetaillist(String aid, DataSource ds,
			String querystring, Map<String, Object> sessionMap, String msaId) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		int retval = 0;
		int rowsize = 0;

		DecimalFormat dfcurmargin = new DecimalFormat("0.00");

		String state = "";
		String dateOnSite = null;
		String dateOffSite = null;
		String dateComplete = null;
		String dateClosed = null;
		String jobSearchName = null;
		String jobSearchSiteNumber = null;
		String jobSearchCity = "";
		String jobSearchOwner = "";
		String jobSearchDateFrom = "";
		String jobSearchDateTo = "";

		if (sessionMap.get("dateOnSite") != null)
			dateOnSite = sessionMap.get("dateOnSite").toString();

		if (sessionMap.get("dateOffSite") != null)
			dateOffSite = sessionMap.get("dateOffSite").toString();

		if (sessionMap.get("dateComplete") != null)
			dateComplete = sessionMap.get("dateComplete").toString();

		if (sessionMap.get("dateClosed") != null)
			dateClosed = sessionMap.get("dateClosed").toString();

		if (sessionMap.get("job_search_state") != null)
			state = sessionMap.get("job_search_state").toString();

		if (sessionMap.get("job_search_name") != null)
			jobSearchName = sessionMap.get("job_search_name").toString().trim()
					.replace("'", "''");

		if (sessionMap.get("job_search_site_number") != null)
			jobSearchSiteNumber = sessionMap.get("job_search_site_number")
					.toString().trim().replace("'", "''");

		if (sessionMap.get("job_search_city") != null)
			jobSearchCity = sessionMap.get("job_search_city").toString();

		if (sessionMap.get("job_search_owner") != null)
			jobSearchOwner = sessionMap.get("job_search_owner").toString();

		if (sessionMap.get("job_search_date_from") != null)
			jobSearchDateFrom = sessionMap.get("job_search_date_from")
					.toString();

		if (sessionMap.get("job_search_date_to") != null)
			jobSearchDateTo = sessionMap.get("job_search_date_to").toString();

		if (state.equals("0"))
			state = "";

		if (querystring.equals("")) {
			querystring = "order by jobname";
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lp_prj_job_tab( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?, ?, ?, ? )}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, aid);
			cstmt.setString(3, jobSearchName);
			cstmt.setString(4, jobSearchSiteNumber);
			cstmt.setString(5, jobSearchCity);
			cstmt.setString(6, state);
			cstmt.setString(7, jobSearchOwner);
			cstmt.setString(8, jobSearchDateFrom);
			cstmt.setString(9, jobSearchDateTo);
			cstmt.setString(10, dateOnSite);
			cstmt.setString(11, dateOffSite);
			cstmt.setString(12, dateComplete);
			cstmt.setString(13, dateClosed);
			cstmt.setString(14, querystring);
			cstmt.setString(15, msaId);

			rs = cstmt.executeQuery();

			int i = 0;

			while (rs.next()) {

				AppendixHeaderBean ahForm = new AppendixHeaderBean();

				ahForm.setAppendixid(rs.getString("lm_js_pr_id"));
				ahForm.setJobid(rs.getString("lm_js_id"));
				ahForm.setInvoice_no(rs.getString("lm_js_invoice_no"));
				ahForm.setJobname(rs.getString("jobname"));
				ahForm.setSiteid(rs.getString("lm_js_si_id"));
				ahForm.setSitename(rs.getString("lm_si_name"));
				ahForm.setTotal(Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_total_cost"), 2));
				ahForm.setExtendedprice(Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_total_price"), 2));
				ahForm.setActualcosts(Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_actual_cost"), 2));
				ahForm.setActualvgp(Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_actual_vgp"), 2));
				ahForm.setPmargin(dfcurmargin.format(rs
						.getFloat("lx_ce_margin")));
				ahForm.setEcomplete(rs.getString("lx_se_end"));
				ahForm.setStatus(rs.getString("lm_js_status"));
				ahForm.setSiteno(rs.getString("lm_si_number"));
				ahForm.setSitestate(rs.getString("lm_si_state"));
				ahForm.setSitezipcode(rs.getString("lm_si_zip_code"));
				ahForm.setSitecity(rs.getString("lm_si_city"));
				ahForm.setJobtype(rs.getString("lx_ce_type"));
				ahForm.setStartdate(rs.getString("actual_start_date"));
				ahForm.setPlannedenddate(rs.getString("lx_se_end"));
				ahForm.setStartdatetime(rs.getString("actual_start_time"));
				ahForm.setScope(rs.getString("scope"));
				ahForm.setJobupdateddby(rs.getString("job_changed_by"));
				ahForm.setJobupdatedate(rs.getString("job_change_date"));
				ahForm.setJobupdatetime(rs.getString("job_change_time"));
				ahForm.setJobcreatedby(rs.getString("job_created_by"));
				ahForm.setCustref(rs.getString("lm_js_customer_reference"));
				ahForm.setDocumentpresent(NetMedXDashboarddao
						.uploadedFilespresent("%", ahForm.getJobid(), "Job", ds));
				String prop = rs.getString("lp_sl_priority_store");
				if (prop.equals("1")) {

					ahForm.setSite_prop("Y");
				} else {

					ahForm.setSite_prop("N");
				}
				valuelist.add(i, ahForm);
				i++;
			}
			retval = cstmt.getInt(1);
			rowsize = valuelist.size();
		} catch (Exception e) {
			logger.error("getJobSearchdetaillist(" + aid + ", " + ds + ", "
					+ querystring + "," + sessionMap + ", " + msaId + ")", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobSearchdetaillist(String, DataSource, String, HttpSession, String) - Error occured during getting getJobSearchDetaillist  "
						+ e);
			}
			logger.error(
					"getJobSearchdetaillist(String, DataSource, String, HttpSession, String)",
					e);
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobSearchdetaillist(String, DataSource, String, HttpSession, String) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobSearchdetaillist(String, DataSource, String, HttpSession, String) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobSearchdetaillist(String, DataSource, String, HttpSession, String) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static ArrayList getJobdetaillist(String aid, DataSource ds,
			String querystring, String status, String ownerId, String timeFrame) {
		Connection conn = null;
		Statement stmt = null;
		Statement stmt2 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		ArrayList valuelist = new ArrayList();
		String tempStatus = "";
		String statusQuery = "select cc_status_code from cc_status_master where cc_status_description='To Be Scheduled'";

		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat dateFormatDB = new SimpleDateFormat(
				"yyyy-MM-dd hh:mm:ss");
		SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");

		DecimalFormat dfcurmargin = new DecimalFormat("0.00");
		if (querystring.equals("")) {
			querystring = "order by jobname";
		}

		String sql = "select * from dbo.func_lp_prj_job_tab(" + aid
				+ ",'%','%','%') where 1=1";

		if (status != null && !status.equals("")) {
			if (status.contains("AI")) {
				status = status.replaceAll("\'AI\'",
						"\'IS\',\'II\',\'IO\',\'ION\',\'IOF\'");
			}
			sql = sql + " and temp_status in " + status;
		}

		if (ownerId != null && !ownerId.equals("")) {
			if (ownerId.contains("%")) {

			} else {
				sql = sql + " and lm_js_created_by in " + ownerId;
			}

		}

		if (timeFrame != null && !timeFrame.equals("")) {
			if (timeFrame.equals("month"))
				sql = sql
						+ "	and	( datepart(mm, lm_js_create_date) =  datepart(mm, getdate()) and datepart(yy, lm_js_create_date) =  datepart(yy, getdate()))  ";

			else if (timeFrame.equals("week"))
				sql = sql
						+ "	and	( datepart(wk, lm_js_create_date) =  datepart(wk, getdate())and datepart(yy, lm_js_create_date) =  datepart(yy, getdate()))  ";
			else {
			}

		}

		sql = sql + querystring;

		try {
			conn = ds.getConnection();
			stmt2 = conn.createStatement();
			rs2 = stmt2.executeQuery(statusQuery);
			if (rs2.next()) {

				tempStatus = rs2.getString("cc_status_code");
			}

			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			int i = 0;

			while (rs.next()) {

				AppendixHeaderBean ahForm = new AppendixHeaderBean();

				ahForm.setAppendixid(rs.getString("lm_js_pr_id"));

				ahForm.setJobid(rs.getString("lm_js_id"));
				ahForm.setInvoice_no(rs.getString("lm_js_invoice_no"));

				ahForm.setJobname(rs.getString("jobname").trim());

				ahForm.setSiteid(rs.getString("lm_js_si_id"));

				ahForm.setSitename(rs.getString("lm_si_name"));

				ahForm.setTotal(Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_total_cost"), 2));
				ahForm.setExtendedprice(Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_total_price"), 2));
				ahForm.setActualcosts(Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_actual_cost"), 2));
				ahForm.setActualvgp(Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_actual_vgp"), 2));

				ahForm.setPmargin(dfcurmargin.format(rs
						.getFloat("lx_ce_margin")));

				ahForm.setEcomplete(rs.getString("lx_se_end"));

				ahForm.setStatus(rs.getString("lm_js_status"));

				ahForm.setSiteno(rs.getString("lm_si_number"));
				ahForm.setSitestate(rs.getString("lm_si_state"));
				ahForm.setSitezipcode(rs.getString("lm_si_zip_code"));
				ahForm.setSitecity(rs.getString("lm_si_city"));
				ahForm.setJobtype(rs.getString("lx_ce_type"));
				ahForm.setStartdate(rs.getString("lm_js_planned_start_date"));
				ahForm.setPlannedenddate(rs.getString("lm_js_planned_end_date"));

				ahForm.setStartdatetime(rs.getString("lx_act_start_time"));
				ahForm.setScope(rs.getString("scope"));
				ahForm.setJobupdateddby(rs.getString("job_changed_by"));
				ahForm.setJobupdatedate(rs.getString("job_change_date"));
				ahForm.setJobupdatetime(rs.getString("job_change_time"));
				ahForm.setJobcreatedby(rs.getString("job_created_by"));
				ahForm.setCustref(rs.getString("lm_js_customer_reference"));

				/* to show yellow folder for uploaded document */
				ahForm.setDocumentpresent(NetMedXDashboarddao
						.uploadedFilespresent("%", ahForm.getJobid(), "Job", ds));
				/* to show yellow folder for uploaded document */
				// added for Requested Schedule
				if (!StringUtils.isEmpty(rs.getString("requested_date"))
						&& (rs.getString("temp_status")
								.equalsIgnoreCase(tempStatus))) {
					ahForm.setStartdate(dateFormat.format(dateFormatDB.parse(rs
							.getString("requested_date"))));
					ahForm.setStartdatetime(timeFormat.format(dateFormatDB
							.parse(rs.getString("requested_date"))));

				}

				String prop = rs.getString("lp_sl_priority_store");
				if (prop.equals("1")) {

					ahForm.setSite_prop("Y");
				} else {

					ahForm.setSite_prop("N");
				}
				valuelist.add(i, ahForm);
				i++;
			}

		} catch (Exception e) {
			logger.error(
					"getJobdetaillist(String, DataSource, String, String, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobdetaillist(String, DataSource, String, String, String, String) - Error occured during getting getJobdetaillist  "
						+ e);
			}
			logger.error(
					"getJobdetaillist(String, DataSource, String, String, String, String)",
					e);
		}

		finally {
			try {
				if (rs2 != null) {
					rs2.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobdetaillist(String, DataSource, String, String, String, String) - exception ignored",
						e);
			}

			try {
				if (stmt2 != null)
					stmt2.close();
			} catch (Exception e) {
				logger.warn(
						"getJobdetaillist(String, DataSource, String, String, String, String) - exception ignored",
						e);
			}
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getJobdetaillist(String, DataSource, String, String, String, String) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getJobdetaillist(String, DataSource, String, String, String, String) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getJobdetaillist(String, DataSource, String, String, String, String) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	/**
	 * This method gets Appendix Name.
	 * 
	 * @param appendixid
	 *            String. Appendix id
	 * @param ds
	 *            Object of DataSource
	 * @return String This method returns String, Appendix Name.
	 */
	public static String getAppendixname(String appendixid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String appendixname = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lx_pr_title from lx_appendix_main where lx_pr_id="
					+ appendixid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				appendixname = rs.getString("lx_pr_title");
			}

		} catch (Exception e) {
			logger.error("getAppendixname(" + appendixid + ", " + ds + ")", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixname(String, DataSource) - Error occured during getting Appendix name  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixname(String, DataSource) - exception ignored",
						e);
			}

		}
		return appendixname;
	}

	/**
	 * This method gets Current Status of Appendix.
	 * 
	 * @param appendixid
	 *            String. Appendix id
	 * @param ds
	 *            Object of DataSource
	 * @return String This method returns String, Appendix Current Status.
	 */
	public static String getCurrentstatus(String appendixid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String currentstatus = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lx_pr_status from lx_appendix_main where lx_pr_id like '"
					+ appendixid + "'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				currentstatus = rs.getString("lx_pr_status");
			}

			else
				currentstatus = "I";

		} catch (Exception e) {
			logger.error("getCurrentstatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCurrentstatus(String, DataSource) - Error occured during getting current status  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCurrentstatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCurrentstatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCurrentstatus(String, DataSource) - exception ignored",
						e);
			}

		}
		return currentstatus;
	}

	public static String getCurrentMSAstatus(String msaid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String currentstatus = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lp_md_status from lp_msa_detail where lp_md_mm_id = '"
					+ msaid + "'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				currentstatus = rs.getString("lp_md_status");
			}

			else
				currentstatus = "I";

		} catch (Exception e) {
			logger.error("getCurrentMSAstatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCurrentMSAstatus(String, DataSource) - Error occured during getting current status  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCurrentMSAstatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCurrentMSAstatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCurrentMSAstatus(String, DataSource) - exception ignored",
						e);
			}

		}
		return currentstatus;
	}

	/**
	 * This method gets Latest Schedule details of the Appendix.
	 * 
	 * @param appendixid
	 *            String. Appendix id
	 * @param ds
	 *            Object of DataSource
	 * @return String[] This method returns String[], Latest Schedule details.
	 */

	public static String[] getAppendixschedule(String appendixid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] value = new String[2];

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lx_se_start, lx_se_end from lx_schedule_list_01 where lx_se_type_id="
					+ appendixid + " and lx_se_type='P'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				value[0] = rs.getString("lx_se_start");
				value[1] = rs.getString("lx_se_end");
			}

		} catch (Exception e) {
			logger.error("getAppendixschedule(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixschedule(String, DataSource) - Error occured during getting appendixschedule  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAppendixschedule(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixschedule(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixschedule(String, DataSource) - exception ignored",
						e);
			}

		}
		return value;
	}

	/**
	 * This method gets P&L Summary of the Appendix.
	 * 
	 * @param appendixid
	 *            String. Appendix id
	 * @param ds
	 *            Object of DataSource
	 * @return String[] This method returns String[], P&L Summary.
	 */
	public static String[] getPLsummary(String appendixid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] value = new String[12];
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select  * from dbo.func_lp_appendix_cost_summary("
					+ appendixid + ")";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				value[0] = Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_total_cost"), 1);
				value[1] = Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_invoice_price"), 1);
				value[2] = dfcurmargin.format(rs.getFloat("lx_ce_margin"));
				value[3] = Decimalroundup.twodecimalplaces((rs
						.getFloat("lx_ce_cns_hq_labor_cost") + rs
						.getFloat("lx_ce_cns_field_labor_cost")), 1);
				value[4] = Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_ctr_field_labor_cost"), 1);
				value[5] = Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_material_cost"), 1);
				value[6] = Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_frieght_cost"), 1);
				value[7] = Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_travel_cost"), 1);
				value[8] = Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_actual_cost"), 1);
				value[9] = dfcurmargin.format(rs.getFloat("lx_ce_actual_vgpm"));
				value[10] = Decimalroundup.twodecimalplaces(
						rs.getFloat("lx_ce_actual_vgp"), 1);
				value[11] = dfcurmargin.format(rs.getFloat("pmvipp"));
			}

		} catch (Exception e) {
			logger.error("getPLsummary(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPLsummary(String, DataSource) - Error occured during getting P&L summary  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getPLsummary(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getPLsummary(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getPLsummary(String, DataSource) - exception ignored",
						e);
			}

		}
		return value;
	}

	public static ArrayList getStatusreport(DataSource ds, String Id,
			String type) {
		ArrayList sowlist = new ArrayList();
		Connection conn = null;
		Sow sow = null;
		Statement stmt = null;

		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = " select * from dbo.func_view_status_report( '" + Id
					+ "' , '" + type + "' )";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				sow = new Sow();
				sow.setSow_Id(rs.getString(1));

				sow.setSow(rs.getString(3));

				sowlist.add(sow);
			}

		} catch (SQLException e) {
			logger.error("getStatusreport(DataSource, String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStatusreport(DataSource, String, String) - Exception caught in getting status report"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getStatusreport(DataSource, String, String)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getStatusreport(DataSource, String, String) - Exception caught in getting status report"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getStatusreport(DataSource, String, String)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getStatusreport(DataSource, String, String) - Exception caught in getting status report"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getStatusreport(DataSource, String, String)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getStatusreport(DataSource, String, String) - Exception caught in getting status report"
							+ sql);
				}
			}

		}
		return sowlist;
	}

	public static String getStatusreportforemail(DataSource ds, String Id,
			String type) {
		String list = "";
		Connection conn = null;

		Statement stmt = null;

		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = " select * from dbo.func_view_status_report( '" + Id
					+ "' , '" + type + "' )";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				list = list + rs.getString(3);

				list = list + "\n\n";

			}

		} catch (SQLException e) {
			logger.error("getStatusreportforemail(DataSource, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStatusreportforemail(DataSource, String, String) - Exception caught in getting status report for email"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getStatusreportforemail(DataSource, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getStatusreportforemail(DataSource, String, String) - Exception caught in getting status report for email"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getStatusreportforemail(DataSource, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getStatusreportforemail(DataSource, String, String) - Exception caught in getting status report for email"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getStatusreportforemail(DataSource, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getStatusreportforemail(DataSource, String, String) - Exception caught in getting status report for email"
							+ sql);
				}
			}

		}
		return list;
	}

	/**
	 * This method gets Contactsummary of the Appendix.
	 * 
	 * @param appendixid
	 *            String. Appendix id
	 * @param ds
	 *            Object of DataSource
	 * @return String[] This method returns String[], Contactsummary.
	 */
	public static String[] getContactsummary(String appendixid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] value = new String[6];

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.lm_contact_summary(" + appendixid
					+ ")";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				value[0] = rs.getString("POCname");
				value[1] = rs.getString("lo_pc_phone1");
				value[2] = rs.getString("lo_pc_phone2");
				value[3] = rs.getString("lo_pc_email1");
				value[4] = rs.getString("lo_pc_email2");
				value[5] = rs.getString("lo_pc_fax");
			}

		} catch (Exception e) {
			logger.error("getContactsummary(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getContactsummary(String, DataSource) - Error occured during getting getContactsummary  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getContactsummary(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getContactsummary(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getContactsummary(String, DataSource) - exception ignored",
						e);
			}

		}
		return value;
	}

	/**
	 * This method gets MSA Name.
	 * 
	 * @param appendixid
	 *            String. Appendix id
	 * @param ds
	 *            Object of DataSource
	 * @return String This method returns String, Appendix Name.
	 */
	public static String getMsaname(String appendixid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		Statement stmt1 = null;
		ResultSet rs1 = null;
		ResultSet rs = null;
		String sql = "";
		String MSA_Id = "0";
		String msaname = "";
		try {
			conn = ds.getConnection();
			stmt1 = conn.createStatement();
			stmt = conn.createStatement();
			String sql1 = "select lx_pr_mm_id from lx_appendix_main where lx_pr_id = "
					+ appendixid;

			rs1 = stmt1.executeQuery(sql1);

			if (rs1.next()) {
				MSA_Id = rs1.getString("lx_pr_mm_id");
			}

			sql = "select dbo.func_cc_name('" + MSA_Id + "', 'MSA' )";

			rs = stmt.executeQuery(sql);

			if (rs.next())
				msaname = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getMsaname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsaname(String, DataSource) - Error Occured in retrieving msaname"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (rs1 != null) {
					rs1.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getMsaname(String, DataSource) - exception ignored", s);
			}

			try {
				if (stmt != null)
					stmt.close();
				if (stmt1 != null)
					stmt1.close();
			} catch (SQLException s) {
				logger.warn(
						"getMsaname(String, DataSource) - exception ignored", s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getMsaname(String, DataSource) - exception ignored", s);
			}

		}
		return msaname;
	}

	public static String[] getAppendixpocdetails(String appendixid,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] value = new String[12];

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lp_appendix_poc('"
					+ appendixid + "')";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				value[0] = rs.getString("CNSPOCname");
				value[1] = rs.getString("CNSPOCphone");
				value[2] = rs.getString("BusinessPOCname");
				value[3] = rs.getString("BusinessPOCphone");
				value[4] = rs.getString("ContractPOCname");
				value[5] = rs.getString("ContractPOCphone");
				value[6] = rs.getString("BillingPOCname");
				value[7] = rs.getString("BillingPOCphone");
				value[8] = rs.getString("lx_pr_cns_poc");
				value[9] = rs.getString("lx_pr_business_poc");
				value[10] = rs.getString("lx_pr_contract_poc");
				value[11] = rs.getString("lx_pr_billing_poc");

			}

		} catch (Exception e) {
			logger.error("getAppendixpocdetails(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixpocdetails(String, DataSource) - Error occured during getting getAppendixpocdetails  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAppendixpocdetails(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixpocdetails(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixpocdetails(String, DataSource) - exception ignored",
						e);
			}

		}
		return value;
	}

	public static String[] getAppendixcustcnspocdetails(String appendixid,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] value = new String[5];

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lm_appendix_cust_cns_poc('"
					+ appendixid + "')";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				value[0] = rs.getString("CNSPOCname");
				value[1] = rs.getString("CNSPOCphone");
				value[2] = rs.getString("CustPOCname");
				value[3] = rs.getString("CustPOCphone");
				value[4] = rs.getString("CNSPOCid");
			} else
				value[4] = "0";

		} catch (Exception e) {
			logger.error("getAppendixcustcnspocdetails(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixcustcnspocdetails(String, DataSource) - Error occured during getting getAppendixpocdetails  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAppendixcustcnspocdetails(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixcustcnspocdetails(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixcustcnspocdetails(String, DataSource) - exception ignored",
						e);
			}

		}
		return value;
	}

	public static String[] getCustinfo(String appendixid, String jobid,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] custinfodetail = new String[20];
		int i = 0;
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (checkForSnapon(appendixid, ds))
				sql = "select * from dbo.func_lm_customer_info_snapon ("
						+ appendixid + "," + jobid + ")";
			else
				sql = "select * from dbo.func_lm_customer_info (" + appendixid
						+ "," + jobid + ")";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				if (rs.getString("lm_ap_cust_info_parameter") != null) {
					custinfodetail[i] = rs
							.getString("lm_ap_cust_info_parameter");
				} else {
					custinfodetail[i] = "";
				}
				i++;
				if (rs.getString("lm_ci_value") != null) {
					custinfodetail[i] = rs.getString("lm_ci_value");
				} else {
					custinfodetail[i] = "";
				}
				i++;

			}

			for (int k = 19; k >= i; k--) {
				custinfodetail[k] = "";

			}

		} catch (Exception e) {
			logger.error("getCustinfo(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustinfo(String, String, DataSource) - Error occured during getting getCustinfo ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCustinfo(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCustinfo(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCustinfo(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return custinfodetail;
	}

	public static Appendix getAppendix(String msa_id, String appendix_id,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Appendix appendix = null;

		if (appendix_id.equals("-1"))
			appendix_id = "%";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select appendix_id, lx_pr_type, lx_pd_number_sites, lx_pr_status, lx_pr_customer_reference,inwork_jobs,"
					+ "complete_jobs,closed_jobs,total_jobs,pending_web_tickets,to_be_scheduled,scheduled,overdue,lx_pr_end_customer, "
					+ "lx_pr_test_appendix from dbo.func_lp_prj_appendix_tab('"
					+ msa_id + "', '" + appendix_id + "')";

			rs = stmt.executeQuery(sql);
			appendix = new Appendix();

			ArrayList appendixlist = setAppendix(rs);

			if (appendixlist.size() > 0) {
				appendix = (Appendix) appendixlist.get(0);
			}

		} catch (Exception e) {
			logger.error("getAppendix(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendix(String, String, DataSource) - Exception in retrieving appendix"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendix(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendix(String, String, DataSource) - Exception in retrieving appendix"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendix(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendix(String, String, DataSource) - Exception in retrieving appendix"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendix(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendix(String, String, DataSource) - Exception in retrieving appendix"
							+ sql);
				}
			}

		}
		return appendix;
	}

	public static ArrayList getStateCategoryList(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select cp_country_id, cp_country_name, cp_country_order from lp_country_list order by cp_country_order,cp_country_name";
			rs = stmt.executeQuery(sql);
			int i = 0;

			if (i == 0) {
				AppendixHeaderBean appendixHeaderForm = new AppendixHeaderBean();
				appendixHeaderForm.setSiteCountryId("");
				appendixHeaderForm.setSiteCountryName("");
				valuelist.add(i, appendixHeaderForm);
				i++;
			}

			while (rs.next()) {
				AppendixHeaderBean appendixHeaderForm = new AppendixHeaderBean();
				appendixHeaderForm.setSiteCountryId(rs
						.getString("cp_country_id"));
				appendixHeaderForm.setSiteCountryName(rs
						.getString("cp_country_name"));
				valuelist.add(i, appendixHeaderForm);
				i++;
			}

		} catch (Exception e) {
			logger.error("getStateCategoryList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStateCategoryList(DataSource) - Error occured during getting State Category Wise List  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static String getAppendixsitecount(String appendix_id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		int appendixsitecount = 0;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select dbo.func_lx_appendix_site_count('"
					+ appendix_id + "')";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				appendixsitecount = rs.getInt(1);
			}

		} catch (Exception e) {
			logger.error("getAppendixsitecount(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixsitecount(String, DataSource) - Exception in retrieving appendix site count"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendixsitecount(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendixsitecount(String, DataSource) - Exception in retrieving appendix site count"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendixsitecount(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendixsitecount(String, DataSource) - Exception in retrieving appendix site count"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getAppendixsitecount(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAppendixsitecount(String, DataSource) - Exception in retrieving appendix site count"
							+ sql);
				}
			}

		}
		return appendixsitecount + "";
	}

	public static ArrayList setAppendix(ResultSet rs) {
		ArrayList appendixlist = new ArrayList();
		Appendix appendix;

		try {
			while (rs.next()) {
				appendix = new Appendix();
				appendix.setAppendix_Id(rs.getString("appendix_id"));
				appendix.setAppendixtype(rs.getString("lx_pr_type"));
				appendix.setSiteno(rs.getString("lx_pd_number_sites"));
				appendix.setStatus(rs.getString("lx_pr_status"));
				appendix.setCustref(rs.getString("lx_pr_customer_reference"));
				appendix.setInwork_job(rs.getString("inwork_jobs"));
				appendix.setComplete_job(rs.getString("complete_jobs"));
				appendix.setClosed_job(rs.getString("closed_jobs"));
				appendix.setTotal_job(rs.getString("total_jobs"));
				appendix.setPendingWebTicket(rs
						.getString("pending_web_tickets"));
				appendix.setToBeSchedule(rs.getString("to_be_scheduled"));
				appendix.setSchedule(rs.getString("scheduled"));
				appendix.setOverdue(rs.getString("overdue"));
				appendix.setEndcust(rs.getString("lx_pr_end_customer"));
				appendix.setTestAppendix(rs.getString("lx_pr_test_appendix"));

				appendixlist.add(appendix);

			}
		} catch (Exception e) {
			logger.error("setAppendix(ResultSet)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setAppendix(ResultSet) - Exception Caught" + e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error("setAppendix(ResultSet)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("setAppendix(ResultSet) - Exception Caught"
							+ e);
				}
			}
		}
		return appendixlist;
	}

	public static ArrayList getPowoinformation(String jobid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lm_appendix_powo_info('"
					+ jobid + "')";

			rs = stmt.executeQuery(sql);

			int i = 0;

			while (rs.next()) {

				AppendixHeaderBean ahForm = new AppendixHeaderBean();
				ahForm.setPoid(rs.getString("lm_po_id"));
				ahForm.setPonumber(rs.getString("lm_po_number"));

				ahForm.setPartnername(rs.getString("lm_po_partner_name"));

				ahForm.setPartnerpocname(rs.getString("partner_poc_name"));

				ahForm.setPartnerpocphone(rs.getString("partner_poc_phone"));
				ahForm.setPartnerid(rs.getString("cp_pd_partner_id"));
				ahForm.setPowoType(rs.getString("lm_po_type"));
				valuelist.add(i, ahForm);
				i++;
			}

		} catch (Exception e) {
			logger.error("getPowoinformation(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPowoinformation(String, DataSource) - Error occured during getting getPowoinformation  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getPowoinformation(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getPowoinformation(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getPowoinformation(String, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	/**
	 * @author amitm
	 * @param appendixid
	 * @param ds
	 * @return
	 */
	public static Appendix getjobDates(String app_id, DataSource ds) {
		Appendix appendix = new Appendix();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {

			conn = ds.getConnection();
			cstmt = conn.prepareCall("{call lm_job_dates( ? )}");
			cstmt.setString(1, app_id);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				appendix.setEarliestStart(rs
						.getString("lm_js_planned_start_date_earliest"));
				appendix.setEarliestEnd(rs
						.getString("lm_js_planned_end_date_earliest"));
				appendix.setLatestStart(rs
						.getString("lm_js_planned_start_date_latest"));
				appendix.setLatestEnd(rs
						.getString("lm_js_planned_end_date_latest"));
			}
		} catch (Exception e) {
			logger.error("getjobDates(" + app_id + ", " + ds + ")", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getjobDates(String, DataSource) - Error occured during getting getdates for Appendix Jobs  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getjobDates(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getjobDates(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getjobDates(String, DataSource) - exception ignored",
						e);
			}

		}
		return appendix;
	}

	public static String getMsanameFromMsaID(String msaId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String str = "MSA";

		String msaName = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select dbo.func_cc_name('" + msaId + "','" + str
					+ "')";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				msaName = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("getMsanameFromMsaID(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsanameFromMsaID(String, DataSource) - Exception in MSA Name "
						+ e);
			}
			logger.error("getMsanameFromMsaID(String, DataSource)", e);
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getMsanameFromMsaID(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getMsanameFromMsaID(String, DataSource) - Exception in retrieving msa name"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getMsanameFromMsaID(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getMsanameFromMsaID(String, DataSource) - Exception in retrieving msa name"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getMsanameFromMsaID(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getMsanameFromMsaID(String, DataSource) - Exception in retrieving msa name"
							+ sql);
				}
			}

		}
		return msaName;
	}

	public static java.util.Collection getmsaAppendixList(String msaId,
			DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql1 = "";
		list.add(new com.mind.common.LabelValue("All", "0"));

		if (msaId != null) {
		} else
			msaId = "0";

		int msaid = Integer.parseInt(msaId);

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql1 = "select distinct lx_pr_id, lx_pr_title from lx_appendix_main where lx_pr_mm_id ='"
					+ msaid
					+ "' "
					+ "and lx_pr_status in ('C','F','H','I','O') and lx_pr_type <> 3";

			rs = stmt.executeQuery(sql1);
			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("lx_pr_title"), rs.getString("lx_pr_id")));
			}
		} catch (Exception e) {
			logger.error("getmsaAppendixList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getmsaAppendixList(String, DataSource) - Exception caught in search for getAppendixlist"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getmsaAppendixList(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getmsaAppendixList(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getmsaAppendixList(String, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	public static String getNetmedxAppendixid(String msaId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String netmedx = "-2";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lx_pr_id from lx_appendix_main where lx_pr_mm_id ="
					+ msaId + " and lx_pr_type = 3";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				netmedx = rs.getString("lx_pr_id");
			}
		}

		catch (Exception e) {
			logger.error("getNetmedxAppendixid(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNetmedxAppendixid(String, DataSource) - Error Occured in retrieving Appendixid"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getNetmedxAppendixid(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getNetmedxAppendixid(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getNetmedxAppendixid(String, DataSource) - exception ignored",
						s);
			}

		}
		return netmedx;
	}

	public static java.util.Collection getjobOwnerListfromMSA(String app_id,
			DataSource ds, String msaId) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new java.util.ArrayList();
		valuelist.add(new com.mind.common.LabelValue("--Select--", " "));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (app_id.equals("0"))
				app_id = "%";

			String sql = "select distinct job_created_by = dbo.func_cc_poc_name(isnull(lm_js_created_by,0)) "
					+ "from lm_job inner join lx_appendix_main on lx_pr_id = lm_js_pr_id where "
					+ "lm_js_pr_id like '"
					+ app_id
					+ "' and lx_pr_mm_id like '"
					+ msaId
					+ "' and lx_pr_type <> 3 "
					+ "and lm_js_type not in ('Default','Addendum') and lx_pr_status in ('C','F','H','I','O')";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				if (rs.getString("job_created_by") != null)
					valuelist.add(new com.mind.common.LabelValue(rs
							.getString("job_created_by"), rs
							.getString("job_created_by")));
			}

		} catch (Exception e) {
			logger.error("getjobOwnerListfromMSA(String, DataSource, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getjobOwnerListfromMSA(String, DataSource, String) - Error occured during getting Job Owner List  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getjobOwnerListfromMSA(String, DataSource, String) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getjobOwnerListfromMSA(String, DataSource, String) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getjobOwnerListfromMSA(String, DataSource, String) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static String getAppendixCount(String msaId, String whereclause,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String count = "0";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select appendixCount = count(distinct lx_pr_id) from lx_appendix_main where lx_pr_mm_id ='"
					+ msaId
					+ "' and lx_pr_status in ('C','F','H','I','O') "
					+ "and lx_pr_type " + whereclause;
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				if (rs.getString("appendixCount") != null)
					count = rs.getString("appendixCount");
			}

		} catch (Exception e) {
			logger.error("getAppendixCount(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixCount(String, String, DataSource) - Error occured during getting count for appendix  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAppendixCount(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixCount(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixCount(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return count;
	}

	public static String getAppendixPrType(String Msa_Id, String Appendix_Id,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String appendixPrType = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lx_pr_type_name from dbo.func_lp_appendix_tab('"
					+ Msa_Id + "', '" + Appendix_Id + "')";
			rs = stmt.executeQuery(sql);

			if (rs.next())
				appendixPrType = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getAppendixPrType(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixPrType(String, String, DataSource) - Error Occured in retrieving appendix type"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getAppendixPrType(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixPrType(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixPrType(String, String, DataSource) - exception ignored",
						s);
			}

		}
		return appendixPrType;
	}

	/**
	 * Discription: This method is used to get total no. of rescheule count of
	 * appendix.
	 * 
	 * @param appendixId
	 *            - Appendix Id
	 * @param ds
	 *            - Database object
	 * @return rseCount - Reschedule count
	 */
	public static String getResCount(String appendixId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String rseCount = "0";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lx_pr_rse_jobs_count = isnull(lx_pr_rse_jobs_count,0) from lx_appendix_main where lx_pr_id = "
					+ appendixId;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				rseCount = rs.getString("lx_pr_rse_jobs_count");
			}
		}

		catch (Exception e) {
			logger.error("getResCount(" + appendixId + ", " + ds + ")", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResCount(String, DataSource) - Error Occured in com.mind.common.Appendixdao.getResCount() "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return rseCount;
	}

	/**
	 * Discription: This method is used to get snapon appendix id.
	 * 
	 * @param appendixId
	 *            -- Appendix Id
	 * @param ds
	 *            -- Database object
	 * @return boolean -- true/false
	 */
	public static boolean checkForSnapon(String appendixId, DataSource ds) {

		logger.debug("checkForSnapon(String, DataSource) -  appendixId="
				+ appendixId);
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean check = false;
		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select dbo.func_get_snapon_pr_id(?)");
			pstmt.setString(1, appendixId);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				if (rs.getString(1).equals("1"))
					check = true;
			}

		} catch (Exception e) {
			logger.error("checkForSnapon(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkForSnapon(String, DataSource) - Error occured com.mind.dao.Appendixdao.getSnaponSiteNo() "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return check;
	}

	public static boolean editContactList(String loginUserId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean editContactLst = false;
		try {
			conn = ds.getConnection();
			StringBuilder sqlQuery = new StringBuilder(
					" select lo_ro_role_name from lo_poc ");
			sqlQuery.append(" join lo_poc_rol on lo_pc_id=lo_pr_pc_id join lo_role on lo_pr_ro_id=lo_ro_id ");
			sqlQuery.append(" where lo_pc_id=? and lo_ro_role_name='admin' ");
			pstmt = conn.prepareStatement(sqlQuery.toString());
			pstmt.setString(1, loginUserId);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				editContactLst = true;
			}

		} catch (Exception e) {
			logger.error("editContactList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("editContactList(String, DataSource) - Error occured  while checking edit contact list or not "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return editContactLst;
	}

	// getting poc details info

	public static String[] AppendixpocdetailsInfo(String appendixid,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] value = new String[15];

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_lp_appendix_poc_new('"
					+ appendixid + "')";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				value[0] = rs.getString("CNSPOCname");
				value[1] = rs.getString("CNSPOCphone");
				value[2] = rs.getString("BusinessPOCname");
				value[3] = rs.getString("BusinessPOCphone");
				value[4] = rs.getString("ContractPOCname");
				value[5] = rs.getString("ContractPOCphone");
				value[6] = rs.getString("BillingPOCname");
				value[7] = rs.getString("BillingPOCphone");
				value[8] = rs.getString("lx_pr_cns_poc");
				value[9] = rs.getString("lx_pr_business_poc");
				value[10] = rs.getString("lx_pr_contract_poc");
				value[11] = rs.getString("lx_pr_billing_poc");
				value[12] = rs.getString("CNSPOCEmail");
				value[13] = rs.getString("CNSPOCAdd");
				value[14] = rs.getString("CNSPOCTitle");
			}

		} catch (Exception e) {
			logger.error("getAppendixpocdetails(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixpocdetails(String, DataSource) - Error occured during getting getAppendixpocdetails  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getAppendixpocdetails(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixpocdetails(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getAppendixpocdetails(String, DataSource) - exception ignored",
						e);
			}

		}
		return value;
	}

}
