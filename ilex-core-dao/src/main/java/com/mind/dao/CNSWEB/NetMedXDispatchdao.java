package com.mind.dao.CNSWEB;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.cnsweb.NetMedXDispatchBean;


public class NetMedXDispatchdao 
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(NetMedXDispatchdao.class);

	public  static boolean authenticateuser(String username,String password,DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		boolean check=false;
		
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select lo_us_name , lo_us_pass from lo_user where lo_us_name='"+username+"' and lo_us_pass='"+password+"'";						   		
			rs = stmt.executeQuery(sql);
			
			if(rs.next())
			{
				
				check = true;
				
			}
			
		}
		catch(Exception e){
			logger.error("authenticateuser(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("authenticateuser(String, String, DataSource) - Error occured during Authenticating User  ");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("authenticateuser(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("authenticateuser(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("authenticateuser(String, String, DataSource) - exception ignored", e);
}
			
		}
		return check;	
	}
	
	
	public  static boolean getuserdetail(String username,NetMedXDispatchBean formbean,DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		boolean check=false;
		
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select * from func_lo_netmedx_detail('"+username+"')";		   		
			
			rs = stmt.executeQuery(sql);
			
			if(rs.next())
			{
				formbean.setLo_poc_name(rs.getString("poc_name"));
				//formbean.setLo_om_division(rs.getString("lo_ot_name"));
				formbean.setLo_poc_phone(rs.getString("lo_pc_phone1"));
				formbean.setLo_poc_email(rs.getString("lo_pc_email1"));
				formbean.setLo_ad_address11(rs.getString("lo_ad_address1"));
				formbean.setLo_ad_city1(rs.getString("lo_ad_city"));
				formbean.setLo_ad_state1(rs.getString("lo_ad_state"));
				formbean.setLo_ad_zip_code1(rs.getString("lo_ad_zip_code"));
				formbean.setLo_om_id(rs.getString("lo_om_id"));
			}
			
		}
		catch(Exception e){
			logger.error("getuserdetail(String, NetMedXDispatchForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getuserdetail(String, NetMedXDispatchForm, DataSource) - Error occured in getuserdetail: " + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getuserdetail(String, NetMedXDispatchForm, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getuserdetail(String, NetMedXDispatchForm, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getuserdetail(String, NetMedXDispatchForm, DataSource) - exception ignored", e);
}
			
		}
		return check;	
	}
	
	
/** This method insert the details of NetMedXDispatch form 
 *  Partner_EditForm formbean and use formbean method to send the parameter in the stored procedure
 * @param form  		       	    	Object of Partner_editFrom form bean  
 * @param ds   			       	    	Object of DataSource
 * @return ArrayList              	    Contains the objects of McsaDetailBean.
 */
			public  static int setNetMedXDispatchForm(NetMedXDispatchBean form,DataSource ds)
			{
				Connection conn=null;
				CallableStatement stmt=null;
				ResultSet rs=null;
				int retvalue=0;
				String date = "";
				date = form.getLo_nd_arrival_date()+" "+form.getLo_nd_arrival_time_hour()+":"+form.getLo_nd_arrival_time_minute()+" "+form.getLo_nd_arrival_time_option();
				
				try{
					conn=ds.getConnection();
					
					stmt=conn.prepareCall("{?=call lo_netmedx_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
					stmt.registerOutParameter(1,Types.INTEGER);
					
					if (form.getLo_om_id().length()==0)
						stmt.setString( 2,null);//cp_pd_id
					else
						stmt.setString( 2,form.getLo_om_id());//cp_pd_id
					stmt.setString( 3,form.getLo_nd_send_copy());
					stmt.setString( 4,form.getLo_nd_send_email());
					stmt.setString( 5,form.getLo_nd_po_no());
					stmt.setString( 6,form.getLo_site_location());					
					stmt.setString( 7,form.getLo_custticket_number());
					stmt.setString( 8,form.getLo_nd_cust_no());
					
					stmt.setString( 9,date);
					if(!(form.getLo_nd_arrival_win_bw_hour().equals("-1")||form.getLo_nd_arrival_win_bw_minute().equals("-1")))	
						stmt.setString(10,form.getLo_nd_arrival_win_bw_hour()+":"+form.getLo_nd_arrival_win_bw_minute()+" "+form.getLo_nd_arrival_win_bw_option());
					else
						stmt.setString(10,"");
					if(!(form.getLo_nd_arrival_win_and_hour().equals("-1")||form.getLo_nd_arrival_win_and_minute().equals("-1")))
						stmt.setString(11,form.getLo_nd_arrival_win_and_hour()+":"+form.getLo_nd_arrival_win_and_minute()+" "+form.getLo_nd_arrival_win_and_option());
					else
						stmt.setString(11,"");

					stmt.setString(12,form.getLo_nd_call_criticality());
					stmt.setString(13,form.getLo_nd_item_category());				
					stmt.setString(14,form.getLo_nd_summary());
					stmt.setString(15,form.getLo_nd_spec_inst());
					stmt.setString(16,form.getLo_nd_invoice_cust());
					stmt.setString(17,form.getLo_pc_first_name1());
					stmt.setString(18,form.getLo_pc_last_name1());					
					stmt.setString(19,form.getLo_pc_phone11());
					stmt.setString(20,form.getLo_pc_first_name2());
					stmt.setString(21,form.getLo_pc_last_name2());
					stmt.setString(22,form.getLo_pc_phone12());
					byte[] buffer1 = new byte[form.getFileSize()];
					byte[] buffer2 = new byte[form.getFileSize2()];
					byte[] buffer3 = new byte[form.getFileSize3()];
					
					form.getInputstream().read(buffer1);
					form.getInputstream2().read(buffer2);
					form.getInputstream3().read(buffer3);
					
					if(form.getInputstream()!=null)
						stmt.setBytes(23,buffer1);
					else
						stmt.setBytes(23,null);
					
					if(form.getInputstream2()!=null)
						stmt.setBytes(24,buffer2);
					else
						stmt.setBytes(24,null);
					
					if(form.getInputstream3()!=null)
						stmt.setBytes(25,buffer3);
					else
						stmt.setBytes(25,null);
					

					stmt.setString(26,form.getLo_ad_name2());
					
					if(!form.getLo_ad_id2().equals(""))
						stmt.setString(27,form.getLo_ad_id2());
					else
						stmt.setString(27,"0");
					
					stmt.setString(28,form.getLo_ad_address12());
					stmt.setString(29,"");
					stmt.setString(30,form.getLo_ad_city2());
					stmt.setString(31,form.getLo_ad_state2());
					stmt.setString(32,form.getLo_ad_zip_code2());
					stmt.setString(33,form.getLo_ad_country2());
					stmt.setString(34,"0");
					stmt.setString(35,form.getLo_poc_name());
					stmt.setString(36,form.getLo_om_division());
					stmt.setString(37,form.getLo_poc_phone());
					stmt.setString(38,form.getLo_poc_email());
					stmt.setString(39,form.getLo_ad_address11());
					stmt.setString(40,form.getLo_ad_city1());
					stmt.setString(41,form.getLo_ad_state1());
					stmt.setString(42,form.getLo_ad_zip_code1());
					stmt.setString(43,"0");//form.getLo_om_id()
				
					stmt.execute();
					retvalue=stmt.getInt(1);
					//System.out.println("return value of stored procedure lo_netmedx_manage_01() is==="+retvalue);
					
				}
				catch(Exception e){
			logger.error("setNetMedXDispatchForm(NetMedXDispatchForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setNetMedXDispatchForm(NetMedXDispatchForm, DataSource) - Error occured during NetMedXDispatchForm in setNetMedXDispatchForm() :" + e);
			}
				}
				
				finally
				{
					try
					{
						if ( rs!= null ) 
						{
							rs.close();		
						}
					}
					catch( Exception e )
					{
				logger.warn("setNetMedXDispatchForm(NetMedXDispatchForm, DataSource) - exception ignored", e);
}
					
					
					try
					{
						if ( stmt!= null ) 
							stmt.close();
					}
					catch( Exception e )
					{
				logger.warn("setNetMedXDispatchForm(NetMedXDispatchForm, DataSource) - exception ignored", e);
}
					
					
					try
					{
						if ( conn!= null) 
							conn.close();
					}
					catch( Exception e )
					{
				logger.warn("setNetMedXDispatchForm(NetMedXDispatchForm, DataSource) - exception ignored", e);
}
						
				}
			return retvalue;

		}
			
/** This method retrieve the details of Partner in the database by taking formbean parameter of
 *  Partner_EditForm formbean and use formbean setter methods to set the value retrieved by Sql queries
 * @param form  		       	    	Object of Partner_editFrom form bean
 * @param ds   			       	    	Object of DataSource
 */
	public  static int gettNetMedXDispatch_details(NetMedXDispatchBean form,DataSource ds)
	{
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		int retvalue=0;
		String sql="SELECT lo_nd_om_id, lo_nd_send_copy, lo_nd_send_email, lo_nd_site_location," +
				" lo_nd_po_no, lo_nd_ticket_no, lo_nd_cust_no," +
				" CONVERT(varchar(10), lo_nd_arrival_date, 103) AS arrivaldate, " +
				" DATEPART(hh, lo_nd_arrival_date) AS hh_arrival, " +
				" DATEPART(mi, lo_nd_arrival_date) mi_arrival, " +
				" DATEPART(hh, lo_nd_arrival_win_bw) AS hh_bw, " +
				" DATEPART(mi,lo_nd_arrival_win_bw) AS mi_bw, " +
				" DATEPART(hh, lo_nd_arrival_win_and) AS hh_and," +
				" DATEPART(mi, lo_nd_arrival_win_and) AS mi_and," +
				" lo_nd_call_criticality, lo_nd_item_category, lo_nd_summary, lo_nd_spec_inst, " +
				" lo_nd_invoice_cust, lo_nd_primary_pc_name," +
				" lo_nd_primary_last_name, lo_nd_primary_pc_phone, lo_nd_sec_pc_name, " +
				" lo_nd_sec_last_name, lo_nd_sec_pc_phone, lo_nd_alt_comp_name," +
				" lo_nd_alt_comp_ad_id, lo_nd_file1, lo_nd_file2, lo_nd_file3 FROM lo_netmedx_dispatch";
		
		


		try{
			conn= ds.getConnection();
			stmt= conn.createStatement();
			rs= stmt.executeQuery(sql);
			if(rs.next()){				
				retvalue=1;
				form.setLo_om_id(rs.getString("lo_nd_om_id"));
				form.setLo_nd_send_copy(rs.getString("lo_nd_send_copy"));
				form.setLo_nd_send_email(rs.getString("lo_nd_send_email"));
				form.setLo_site_location(rs.getString("lo_nd_site_location"));
				form.setLo_nd_po_no(rs.getString("lo_nd_po_no"));									
				form.setLo_custticket_number(rs.getString("lo_nd_ticket_no"));
				form.setLo_nd_cust_no(rs.getString("lo_nd_cust_no"));
				
				form.setLo_nd_arrival_date(rs.getString("arrivaldate"));

				String hh_arrival=rs.getString("hh_arrival");
				String hh_arrival_op="";
				
				if(Integer.parseInt(hh_arrival) > 12) 
				 {
					hh_arrival = new Integer(Integer.parseInt(hh_arrival)-12).toString();					
					hh_arrival_op = "PM";
				
				}
				 else 
				 {
					 hh_arrival_op = "AM";
					 
				 }
				form.setLo_nd_arrival_time_hour(hh_arrival);
				form.setLo_nd_arrival_time_minute(rs.getString("mi_arrival"));
				form.setLo_nd_arrival_time_option(hh_arrival_op);
				
				String hh_bw=rs.getString("hh_bw");
				String hh_bw_op="";
				
				if(Integer.parseInt(hh_bw) > 12) 
				 {
					hh_bw = new Integer(Integer.parseInt(hh_bw)-12).toString();					
					hh_bw_op = "PM";
				
				}
				 else 
				 {
					 hh_bw_op = "AM";
					 
				 }

				form.setLo_nd_arrival_win_bw_hour(hh_bw);
				form.setLo_nd_arrival_win_bw_minute(rs.getString("mi_bw"));
				form.setLo_nd_arrival_win_bw_option(hh_bw_op);
				
				String hh_and=rs.getString("hh_and");
				String hh_and_op="";
				if(Integer.parseInt(hh_and) > 12) 
				 {
					hh_and = new Integer(Integer.parseInt(hh_and)-12).toString(); 
					hh_and_op = "PM";									
				 }				
				 else 
				 {
					 hh_and_op = "AM";					 
				 }
				form.setLo_nd_arrival_win_and_hour(hh_and);
				form.setLo_nd_arrival_win_and_minute(rs.getString("mi_and"));
				form.setLo_nd_arrival_win_and_option(hh_and_op);
				
				form.setLo_nd_call_criticality(rs.getString("lo_nd_call_criticality"));
				
				form.setLo_nd_item_category(rs.getString("lo_nd_item_category"));				
				form.setLo_nd_summary(rs.getString("lo_nd_summary"));
				form.setLo_nd_spec_inst(rs.getString("lo_nd_spec_inst"));
				form.setLo_nd_invoice_cust(rs.getString("lo_nd_invoice_cust"));
				form.setLo_pc_first_name1(rs.getString("lo_nd_primary_pc_name"));
				form.setLo_pc_last_name1(rs.getString("lo_nd_primary_last_name"));					
				form.setLo_pc_phone11(rs.getString("lo_nd_primary_pc_phone"));
				form.setLo_pc_first_name2(rs.getString("lo_nd_sec_pc_name"));
				form.setLo_pc_last_name2(rs.getString("lo_nd_sec_last_name"));
				form.setLo_pc_phone12(rs.getString("lo_nd_sec_pc_phone"));
				
				form.setLo_ad_name2(rs.getString("lo_nd_alt_comp_name"));
				form.setLo_ad_id2(rs.getString("lo_nd_alt_comp_ad_id"));


			
				
				stmt= conn.createStatement();
				sql="SELECT lo_ad_id, lo_ad_address1, lo_ad_address2, lo_ad_city, lo_ad_state, lo_ad_zip_code, lo_ad_country FROM lo_address WHERE (lo_ad_id ="+form.getLo_ad_id2() +") ";
				rs= stmt.executeQuery(sql);
				if(rs.next()){					
					form.setLo_ad_address12(rs.getString("lo_ad_address1"));
					form.setLo_ad_city2(rs.getString("lo_ad_city"));						
					form.setLo_ad_state2(rs.getString("lo_ad_state"));						
					form.setLo_ad_zip_code2(rs.getString("lo_ad_zip_code"));
				}				
			}
			else{
			}
		}
		catch(Exception e){
			logger.error("gettNetMedXDispatch_details(NetMedXDispatchForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("gettNetMedXDispatch_details(NetMedXDispatchForm, DataSource) - Error occured during getting NetMedXDispatch details in gettNetMedXDispatch_details() :" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			catch( Exception e )
			{
				logger.warn("gettNetMedXDispatch_details(NetMedXDispatchForm, DataSource) - exception ignored", e);
}
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("gettNetMedXDispatch_details(NetMedXDispatchForm, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("gettNetMedXDispatch_details(NetMedXDispatchForm, DataSource) - exception ignored", e);
}
				
		}
	return retvalue;
	}
	
	
	
/** This method retrieve name of the user on the basis of user id provided as parameter
 *  
 * @param userid  		       	    	String value of userid
 * @param ds   			       	    	Object of DataSource
 */	
	
	public  static String getUsername(String userid,DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		boolean check=false;
		String username="";
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			String sql="select lo_us_name from lo_user where lo_us_pc_id="+userid+"";						   		
			//System.out.println("getUsername() query      " +sql);
			rs = stmt.executeQuery(sql);
			
			if(rs.next())
			{
				
				username = rs.getString("lo_us_name");
				
			}
			
		}
		catch(Exception e){
			logger.error("getUsername(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getUsername(String, DataSource) - Error occured during getting Username  " + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getUsername(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getUsername(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getUsername(String, DataSource) - exception ignored", e);
}
			
		}
		return username;	
	}
	
}
