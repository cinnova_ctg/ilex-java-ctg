/**
 * Copyright (C) 2005 MIND All rights reserved. The information contained here
 * in is confidential and proprietary to MIND and forms the part of MIND Project
 * : ILEX Description	: This is a data access object class used for managing
 * edit/locate partner,Add/Delete/view incident report and other functionalities
 * of PVS Manager.
 *
 */
package com.mind.dao.PVS;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.EmailSearchList;
import com.mind.bean.PVSDepotFacility;
import com.mind.bean.PVSPhyAddress;
import com.mind.bean.PVSPocSearchList;
import com.mind.bean.PVSTaxExempt;
import com.mind.bean.PVSToolList;
import com.mind.bean.PVS_CompanyCert_IndustryTechnicalAffiliations;
import com.mind.bean.PVS_CompanyCert_IntegratedTechnologyCertified;
import com.mind.bean.PVS_CompanyCert_StructuredDistribution;
import com.mind.bean.PVS_CompanyCert_Wireless;
import com.mind.bean.PVS_Insurance_Certificate;
import com.mind.bean.PVS_Partner_Edit_TechnicianInfo;
import com.mind.bean.Phone_Email;
import com.mind.bean.RecRest_History;
import com.mind.bean.pvs.AddIncidentBean;
import com.mind.bean.pvs.JobInformationBean;
import com.mind.bean.pvs.McsaDetailBean;
import com.mind.bean.pvs.PartnerDetailBean;
import com.mind.bean.pvs.PartnerSearchMySQLBean;
import com.mind.bean.pvs.PartnerTypeDetailBean;
import com.mind.bean.pvs.Partner_EditBean;
import com.mind.bean.pvs.Partner_SearchBean;
import com.mind.bean.pvs.Partner_Statistics;
import com.mind.common.Decimalroundup;
import com.mind.common.EnvironmentSelector;
import com.mind.common.LabelValue;
import com.mind.common.Util;
import com.mind.common.bean.Fileinfo;
import com.mind.common.dao.Upload;
import com.mind.common.util.MySqlConnection;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.reports.reportDao.ReportsDao;

public class Pvsdao {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Pvsdao.class);

	/**
	 * This method gets total partners.
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return String This method returns String, total partners.
	 */
	public static String getPartners(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String noofpartners = "0";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select col1 , tcount from cp_partner_summary_01 where col1='01'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				noofpartners = rs.getString("tcount");
			}

		} catch (Exception e) {
			logger.error("getPartners(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartners(DataSource) - Error occured during getting partnerlist "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("getPartners(DataSource) - exception ignored", e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn("getPartners(DataSource) - exception ignored", e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("getPartners(DataSource) - exception ignored", e);
			}

		}
		return noofpartners;
	}

	/**
	 * This method gets Qualification Summary list(no of partners in each core
	 * Competence).
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, Qualification list.
	 */
	public static ArrayList getQsummarylist(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList plist = new ArrayList();
		Partner_Statistics P_stats = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select  col1 ,tcount from cp_partner_summary_01 where substring(col1,1,2)='02'";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				P_stats = new Partner_Statistics();
				String labelname = rs.getString("col1").substring(2);
				P_stats.setQsummary(labelname);
				P_stats.setQvalue(rs.getString("tcount"));
				plist.add(P_stats);
			}

		} catch (Exception e) {
			logger.error("getQsummarylist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getQsummarylist(DataSource) - Error occured during getting partnerlist  ");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("getQsummarylist(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn("getQsummarylist(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("getQsummarylist(DataSource) - exception ignored",
						e);
			}

		}
		return plist;
	}

	/**
	 * This method gets Location Summary list(partners grouped by their location
	 * ).
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, Location Summary list.
	 */
	public static ArrayList getLsummarylist(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList plist = new ArrayList();
		Partner_Statistics P_stats = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select  col1 ,tcount from cp_partner_summary_01 where substring(col1,1,2)='03'";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				P_stats = new Partner_Statistics();
				String labelname = rs.getString("col1").substring(2);
				P_stats.setLsummary(labelname);
				P_stats.setLvalue(rs.getString("tcount"));
				plist.add(P_stats);
			}

		} catch (Exception e) {
			logger.error("getLsummarylist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getLsummarylist(DataSource) - Error occured during getting partnerlist  ");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("getLsummarylist(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn("getLsummarylist(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("getLsummarylist(DataSource) - exception ignored",
						e);
			}

		}
		return plist;
	}

	/**
	 * This method gets total no of core competences for which there is a
	 * partner(partner statistics page).
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, core competence list
	 *         grouped by partner.
	 */
	public static String getQrows(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String qrows = "1";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select count(col1) as qrow from cp_partner_summary_01 where substring(col1,1,2)='02'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				qrows = rs.getString("qrow");

			}

		} catch (Exception e) {
			logger.error("getQrows(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getQrows(DataSource) - Error occured during getting partnerlist  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("getQrows(DataSource) - exception ignored", e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn("getQrows(DataSource) - exception ignored", e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("getQrows(DataSource) - exception ignored", e);
			}

		}
		return qrows;
	}

	/**
	 * This method gets total no of states for which there is a partner(partner
	 * statistics page).
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, state list grouped by
	 *         partner.
	 */
	public static String getLrows(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String lrows = "1";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select count(col1) as lrow from cp_partner_summary_01 where substring(col1,1,2)='03'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				lrows = rs.getString("lrow");

			}

		} catch (Exception e) {
			logger.error("getLrows(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getLrows(DataSource) - Error occured during getting partnerlist  ");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("getLrows(DataSource) - exception ignored", e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn("getLrows(DataSource) - exception ignored", e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("getLrows(DataSource) - exception ignored", e);
			}

		}
		return lrows;
	}

	/**
	 * This method gets all incidents which can be reported for a partner.
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, incidents list.
	 */
	public static ArrayList getIncidentlist(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList ilist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select  cp_in_id ,cp_in_desc from cp_incident_master";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				ilist.add(new LabelValue(rs.getString("cp_in_desc"), rs
						.getString("cp_in_id")));
			}

		} catch (Exception e) {
			logger.error("getIncidentlist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getIncidentlist(DataSource) - Error occured during getting incidentlist  ");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("getIncidentlist(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn("getIncidentlist(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("getIncidentlist(DataSource) - exception ignored",
						e);
			}

		}
		return ilist;
	}

	/**
	 * This method gets all actions which can be taken for a partner.
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, action list.
	 */
	public static ArrayList getActionlist(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList alist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select  cp_ia_id , cp_ia_desc from cp_action_master ";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				AddIncidentBean addIncidentForm = new AddIncidentBean();
				String labelname = rs.getString("cp_ia_desc");
				addIncidentForm.setCkboxactionlist(labelname);
				addIncidentForm.setCkboxactionvalue(rs.getString("cp_ia_id"));
				alist.add(addIncidentForm);
			}

		} catch (Exception e) {
			logger.error("getActionlist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActionlist(DataSource) - Error occured during getting actionlist  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("getActionlist(DataSource) - exception ignored", e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn("getActionlist(DataSource) - exception ignored", e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("getActionlist(DataSource) - exception ignored", e);
			}

		}
		return alist;
	}

	/**
	 * This method adds incident details for a partner.
	 * 
	 * @param cp_ir_tech_id
	 *            String.technician id
	 * @param cp_ir_partner_id
	 *            String.partner id
	 * @param cp_ir_date
	 *            String.date of incident generated
	 * @param cp_ir_end_customer
	 *            String.customer id
	 * @param cp_ir_in_id
	 *            String.incident ids separated by commas
	 * @param cp_ir_other
	 *            String.other comments
	 * @param cp_ir_ia_id
	 *            String.action ids separated by commas
	 * @param cp_ir_created_by
	 *            String.userid
	 * @param ds
	 *            Object of DataSource
	 * @return int This method returns integer, 0 for success and -ve values for
	 *         error.
	 */
	public static int addIncident(String cp_ir_tech_id,
			String cp_ir_partner_id, String cp_ir_date,
			String cp_ir_end_customer, String cp_ir_in_id, String cp_ir_other,
			String cp_ir_ia_id, String cp_ir_created_by, String incidenttype,
			String partnerresponse, String cp_ir_js_id, String flag,
			String incidentStatus, int incidentSeverity, String userName,
			DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		int retval = -1;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call cp_incident_manage_01( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, cp_ir_tech_id);
			cstmt.setString(3, cp_ir_partner_id);
			cstmt.setString(4, cp_ir_date);
			cstmt.setString(5, cp_ir_end_customer);
			cstmt.setString(6, cp_ir_in_id);
			cstmt.setString(7, cp_ir_other);
			cstmt.setString(8, cp_ir_ia_id);
			cstmt.setString(9, cp_ir_created_by);
			cstmt.setString(10, incidenttype);
			cstmt.setString(11, partnerresponse);

			if (flag.equals("checklist")) // check whether from checklist or PVS
			// for PVS it is 0
			{
				cstmt.setInt(12, Integer.parseInt(cp_ir_js_id));
			} else {
				cstmt.setInt(12, 0);
			}
			cstmt.setString(13, incidentStatus);
			cstmt.setInt(14, incidentSeverity);
			cstmt.setString(15, userName);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"addIncident(String, String, String, String, String, String, String, String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addIncident(String, String, String, String, String, String, String, String, String, String, String, String, DataSource) - Error occured during adding incident report"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"addIncident(String, String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"addIncident(String, String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"addIncident(String, String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retval;

	}

	/**
	 * This method gets Partner name.
	 * 
	 * @param pid
	 *            String.partner id
	 * @param ds
	 *            Object of DataSource
	 * @return String This method returns String,partner name.
	 */
	public static String getPartnername(String pid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String partnername = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select distinct lo_om_division from lo_organization_hierarchy_01 where cp_partner_id="
					+ pid;

			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				partnername = rs.getString("lo_om_division");

			}

		} catch (Exception e) {
			logger.error("getPartnername(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnername(String, DataSource) - Error occured during getting partnername  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartnername(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartnername(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartnername(String, DataSource) - exception ignored",
						e);
			}

		}
		return partnername;
	}

	/**
	 * This method gets incident report details for a partner.
	 * 
	 * @param inid
	 *            String.Incident report id
	 * @param ds
	 *            Object of DataSource
	 * @return String[] This method returns String[], Incident report details.
	 */
	public static String[] getIncidentreportdetails(String inid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] valueList = new String[21];

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select  lo_us_name, cp_ir_id , cp_partner_name , cp_ir_tech_name ,  convert(VARCHAR(10), cp_ir_date, 101) cp_ir_date , cp_ir_datetime , "
					+ "cp_ir_end_customer_name , cp_incident_id , cp_ir_others , cp_actions_id , cp_ir_incident_type , "
					+ "cp_ir_partner_response , lm_js_title , lx_pr_title , msaname, IsNull(cp_ir_incident_serverity,0)cp_ir_incident_serverity,"
					+ " IsNull(cp_ir_incident_status,0)cp_ir_incident_status, Isnull(cp_ir_in_id,'')cp_ir_in_id,IsNULL(cp_ir_cpd_notes,'')cp_ir_cpd_notes,cp_ir_updated_by, status_description   from cp_incident_list_01 where cp_ir_id="
					+ inid;

			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				valueList[0] = rs.getString("cp_ir_id");
				valueList[1] = rs.getString("cp_partner_name");
				valueList[2] = rs.getString("cp_ir_tech_name");
				valueList[3] = rs.getString("cp_ir_date");
				valueList[4] = rs.getString("cp_ir_datetime");
				valueList[5] = rs.getString("cp_ir_end_customer_name");
				valueList[6] = rs.getString("cp_incident_id");
				valueList[7] = rs.getString("cp_ir_others");
				valueList[8] = rs.getString("cp_actions_id");
				// for incident type
				valueList[9] = rs.getString("cp_ir_incident_type");
				valueList[10] = rs.getString("cp_ir_partner_response");
				valueList[11] = rs.getString("lm_js_title");
				valueList[12] = rs.getString("lx_pr_title");
				valueList[13] = rs.getString("msaname");
				valueList[14] = rs.getString("lo_us_name");
				valueList[15] = rs.getString("cp_ir_incident_serverity");
				valueList[16] = rs.getString("cp_ir_incident_status");
				valueList[17] = rs.getString("cp_ir_in_id");// / Added
				valueList[18] = rs.getString("cp_ir_cpd_notes");
				valueList[19] = rs.getString("cp_ir_updated_by");// By
				valueList[20] = rs.getString("status_description");

			}

		} catch (Exception e) {
			logger.error("getIncidentreportdetails(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getIncidentreportdetails(String, DataSource) - Error occured during getting getIncidenreportdetails  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getIncidentreportdetails(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getIncidentreportdetails(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getIncidentreportdetails(String, DataSource) - exception ignored",
						e);
			}

		}
		return valueList;
	}

	/**
	 * This method gets all incident reports details for a partner.
	 * 
	 * @param pid
	 *            String.Partner id
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, all Incident reports
	 *         details.
	 */
	public static ArrayList getAllreportdetails(String pid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select  lo_us_name, cp_ir_id , cp_partner_name , cp_ir_tech_name , convert(VARCHAR(10), cp_ir_date, 101) cp_ir_date , cp_ir_datetime , "
					+ "cp_ir_end_customer_name , cp_incident_id , cp_ir_others , cp_actions_id , cp_ir_incident_type , "
					+ " cp_ir_partner_response , lm_js_title , lx_pr_title , msaname , IsNull(cp_ir_incident_serverity,0)cp_ir_incident_serverity,"
					+ " IsNull(cp_ir_incident_status,0)cp_ir_incident_status,Isnull(cp_ir_in_id,'')cp_ir_in_id,IsNULL(cp_ir_cpd_notes,'')cp_ir_cpd_notes,cp_ir_updated_by    from cp_incident_list_01 where "
					+ "cp_ir_partner_id=" + pid;

			rs = stmt.executeQuery(sql);
			int i = 0;

			while (rs.next()) {

				AddIncidentBean aiForm = new AddIncidentBean();
				aiForm.setAddedBy(rs.getString("lo_us_name"));// Added By
				aiForm.setInid(rs.getString("cp_ir_id"));
				aiForm.setPartnername(rs.getString("cp_partner_name"));
				aiForm.setTechnicianname(rs.getString("cp_ir_tech_name"));
				aiForm.setDate(rs.getString("cp_ir_date"));
				aiForm.setTime(rs.getString("cp_ir_datetime"));
				aiForm.setEndcustomer(rs.getString("cp_ir_end_customer_name"));
				aiForm.setIncidentlist(rs.getString("cp_incident_id"));
				aiForm.setNotes(rs.getString("cp_ir_others"));
				aiForm.setActionlist(rs.getString("cp_actions_id"));
				aiForm.setIncidenttype(rs.getString("cp_ir_incident_type"));
				aiForm.setPartnerstatus(Pvsdao.getPartnerStatus(pid, ds));
				aiForm.setResponse(rs.getString("cp_ir_partner_response"));
				aiForm.setJobname(rs.getString("lm_js_title"));
				aiForm.setAppendixname(rs.getString("lx_pr_title"));
				aiForm.setMsaname(rs.getString("msaname"));
				aiForm.setIncidentSeverity(String.valueOf(rs
						.getInt("cp_ir_incident_serverity")));
				aiForm.setIncidentStatus(String.valueOf(rs
						.getInt("cp_ir_incident_status")));
				aiForm.setIncidentdrop(rs.getString("cp_ir_in_id"));
				aiForm.setNotecpd(rs.getString("cp_ir_cpd_notes"));
				aiForm.setUpdatedBy(rs.getString("cp_ir_updated_by"));
				valuelist.add(i, aiForm);
				i++;
			}

		} catch (Exception e) {
			logger.error("getAllreportdetails(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAllreportdetails(String, DataSource) - Error occured during getting Allreportdetails  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getAllreportdetails(String, DataSource) - exception ignored",
						e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getAllreportdetails(String, DataSource) - exception ignored",
						e);

			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getAllreportdetails(String, DataSource) - exception ignored",
						e);

			}

		}
		return valuelist;
	}

	// get function that return String Array
	public static String[] Convert(String s) {
		String[] arr = new String[s.length()];
		for (int i = 0; i < s.length(); i++) {
			arr[i] = String.valueOf(s.charAt(i));
		}
		return arr;
	}

	/**
	 * This method deletes incident report of a partner.
	 * 
	 * @param cp_ir_id
	 *            String.Incident report id
	 * @param ds
	 *            Object of DataSource
	 * @return int This method returns integer, 0 for success and -ve values for
	 *         error.
	 */
	public static int deleteIncident(String cp_ir_id, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		int retval = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call cp_incident_delete_01( ? )}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, cp_ir_id);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("deleteIncident(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteIncident(String, DataSource) - Error occured during deleting"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deleteIncident(String, DataSource) - exception ignored",
						e);

			}
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deleteIncident(String, DataSource) - exception ignored",
						e);

			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deleteIncident(String, DataSource) - exception ignored",
						e);

			}
		}
		return retval;

	}

	/**
	 * This method gets core competency name.
	 * 
	 * @param ccid
	 *            int.core Competence id
	 * @param ds
	 *            Object of DataSource
	 * @return String ` This method returns String, core competence id.
	 */
	public static String getCorecompetencyname(int ccid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String corecompetency = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select cp_corecomp_name  from cp_corecompetency where cp_corecomp_id="
					+ ccid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				corecompetency = rs.getString("cp_corecomp_name");
			}
		} catch (Exception e) {
			logger.error("getCorecompetencyname(int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCorecompetencyname(int, DataSource) - Error occured during getting Corecompetencyname  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCorecompetencyname(int, DataSource) - exception ignored",
						e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCorecompetencyname(int, DataSource) - exception ignored",
						e);

			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCorecompetencyname(int, DataSource) - exception ignored",
						e);

			}

		}
		return corecompetency;
	}

	public static int saveRecommendedRestrictedPartner(String pid, String type,
			String mmId, String prId, String createdBy, DataSource ds) {

		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;

		String sql = "insert into cp_partner_recommend_restrict (cp_partner_id, cp_rr_type, "
				+ "cp_rr_mm_id, cp_rr_pr_id, "
				+ "cp_rr_created_date, cp_rr_created_by) VALUES (?, ?, ?, ?, getdate(), ?)";
		try {

			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, Integer.parseInt(pid));
			pStmt.setInt(2, Integer.parseInt(type));
			pStmt.setInt(3, Integer.parseInt(mmId));
			pStmt.setObject(4, null);
			pStmt.setInt(5, Integer.parseInt(createdBy));
			pStmt.executeUpdate();

		} catch (Exception e) {
			logger.error(
					"saveRecommendedRestrictedPartner(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("saveRecommendedRestrictedPartner(String, String, String, String, String, DataSource) - Error in coordinating partner information with the Web.");
			}
		} finally {

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"saveRecommendedRestrictedPartner(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return 0;
	}

	public static int deleteRecommendedRestrictedPartner(String pid,
			String type, DataSource ds) {

		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;

		String delQuery = "DELETE FROM cp_partner_recommend_restrict  WHERE cp_partner_id = ?  AND cp_rr_type = ?";

		try {

			pStmt = conn.prepareStatement(delQuery);
			pStmt.setInt(1, Integer.parseInt(pid));
			pStmt.setInt(2, Integer.parseInt(type));
			pStmt.executeUpdate();

		} catch (Exception e) {
			logger.error(
					"deleteRecommendedRestrictedPartner(String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteRecommendedRestrictedPartner(String, String, DataSource) - Error in coordinating partner information with the Web.");
			}
		} finally {

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deleteRecommendedRestrictedPartner(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return 0;
	}

	public static ArrayList getRecommendedRestrictedPartner(int type,
			String pId, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList partnerList = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = " select DISTINCT lp_mm_id id, lo_ot_name name "
					+ " from func_lp_project_view_01()  "
					+ " where upper(substring(lo_ot_name ,1,1)) between 'A' and 'Z' "
					+ " AND lp_mm_id IN (SELECT DISTINCT cp_rr_mm_id FROM cp_partner_recommend_restrict WHERE cp_rr_type = "
					+ type + "  and cp_partner_id = " + pId + " ) "
					+ " ORDER BY lo_ot_name ";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				partnerList.add(new LabelValue(rs.getString("name"), rs
						.getString("id")));// Adding new label value.
			}
		} catch (Exception e) {
			logger.error("getAllPartners( DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAllPartners(DataSource) - Error occured during getting Corecompetencyname  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("getAllPartners(DataSource) - exception ignored", e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn("getAllPartners(DataSource) - exception ignored", e);

			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("getAllPartners(DataSource) - exception ignored", e);

			}

		}
		return partnerList;
	}

	public static ArrayList getAllPartners(int type, String pId, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList partnerList = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = " select DISTINCT lp_mm_id id, lo_ot_name name "
					+ " from func_lp_project_view_01()  "
					+ " where upper(substring(lo_ot_name ,1,1)) between 'A' and 'Z' "
					+ " AND lp_mm_id NOT IN (SELECT DISTINCT cp_rr_mm_id FROM cp_partner_recommend_restrict  WHERE cp_partner_id = "
					+ pId + ") " + " ORDER BY lo_ot_name";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				partnerList.add(new LabelValue(rs.getString("name"), rs
						.getString("id")));// Adding new label value.
			}
		} catch (Exception e) {
			logger.error("getAllPartners( DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAllPartners(DataSource) - Error occured during getting Corecompetencyname  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("getAllPartners(DataSource) - exception ignored", e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn("getAllPartners(DataSource) - exception ignored", e);

			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("getAllPartners(DataSource) - exception ignored", e);

			}

		}
		return partnerList;
	}

	/**
	 * This method searches partner as per the criteria selected.
	 * 
	 * @param Partnername
	 *            String.partner name
	 * @param keyWord
	 *            String.keyWords
	 * @param partnerType
	 *            String.type of partner
	 * @param mcsaVersionID
	 *            String.mcsa version of the partner
	 * @param zp
	 *            String.zipcode of partner
	 * @param radius
	 *            String.distance
	 * @param MainOffice
	 *            String.MainOffice
	 * @param fieldOffice
	 *            Steing.fieldOffice
	 * @param techZip
	 *            String.technician
	 * @param tech
	 *            String.technician
	 * @param cert
	 *            String.certifications
	 * @param CoreCompt
	 *            String.CoreCompetencyt
	 * @param excludebarred
	 *            String.excludebarred
	 * @param excludeinc
	 *            String.excludeinc
	 * @param partnerRating
	 *            String.rating of partners
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, partner searched as per
	 *         conditions applied.
	 */
	public static ArrayList getSearchpartnerdetails(String Partnername,
			String keyWord, String[] partnerType, String mcsaVersionId,
			String zip, String radius, String MainOffice, String fieldOffice,
			String techZip, String[] tech, String cert, String[] CoreCompt,
			String excludeinc, String excludebarred, String partnerRating,
			String securityCertifications, String queryString, String action,
			DataSource ds, String countyID, String cityName, String powoId,
			String[] recommendedList, String[] restrictedList,
			String[] toolKitArr, String badgeStatus, String recevied) {
		Connection conn = null;
		// Start :Added By Amit
		DecimalFormat dfcur = new DecimalFormat("###0.0");
		DecimalFormat dfcur1 = new DecimalFormat("###.#####");
		DecimalFormat dfcurDollar = new DecimalFormat("#,##0.00");
		// End :
		CallableStatement cstmt = null;
		ResultSet rs = null;
		List listWithBidValue = new ArrayList();
		List listWithOutBidValueActiveParnter = new ArrayList();
		List listWithOutBidValueDornmentParnter = new ArrayList();

		Connection mySqlConn = null;
		Statement mySqlStmt = null;
		ResultSet mySqlRs1 = null;
		Map<String, PartnerSearchMySQLBean> mySQLDBPartnerMap = new HashMap<String, PartnerSearchMySQLBean>();

		int retval = 0;
		if (countyID == null) {
			countyID = "0";
		}
		if (cityName == null) {
			cityName = "0";
		}
		try {

			try {
				String mySqlUrl = EnvironmentSelector
						.getBundleString("appendix.detail.changestatus.mysql.url");
				mySqlConn = MySqlConnection.getMySqlConnection(mySqlUrl);
			} catch (Exception e) {
				logger.error("getSearchpartnerdetails(...Parms...)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getSearchpartnerdetails(...Parms...) - Error in establishing connection to mySQL.");
				}
			}

			if (!badgeStatus.equals("")) {
				mySqlStmt = mySqlConn.createStatement();
				String mySqlQuery = "SELECT * FROM cp_photo_uploads WHERE STATUS  "
						+ badgeStatus;
				mySqlRs1 = mySqlStmt.executeQuery(mySqlQuery);

				PartnerSearchMySQLBean bean = null;
				while (mySqlRs1.next()) {
					bean = new PartnerSearchMySQLBean();
					bean.setPid(mySqlRs1.getString("partner_id"));
					bean.setBadgeName(mySqlRs1.getString("badge_name"));
					bean.setStatus(mySqlRs1.getString("status"));
					mySQLDBPartnerMap.put(bean.getPid(), bean);

				}

			}

			// checking for the partners having badge or without badge
			mySqlStmt = mySqlConn.createStatement();
			String mySqlQuery = "SELECT DISTINCT partner_id FROM cp_photo_uploads WHERE status='Printed'";

			ResultSet keysSet = mySqlStmt.executeQuery(mySqlQuery);
			ArrayList<String> keyList = new ArrayList<String>();
			while (keysSet.next()) {
				keyList.add(keysSet.getString("partner_id"));
			}

			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call cp_partner_search_list_new( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?, ?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, Util.singleToDoubleQuotes(Partnername));
			cstmt.setString(3, Util.singleToDoubleQuotes(keyWord));
			cstmt.setString(4, Util.changeStringToCommaSeperation(partnerType));
			cstmt.setString(5, mcsaVersionId);
			cstmt.setString(6, zip);
			cstmt.setInt(7, Integer.parseInt(radius));
			cstmt.setString(8, Util.singleToDoubleQuotes(MainOffice));
			cstmt.setString(9, Util.singleToDoubleQuotes(fieldOffice));
			cstmt.setString(10, Util.singleToDoubleQuotes(techZip));
			cstmt.setString(11, Util.changeStringToCommaSeperation(tech));
			cstmt.setString(12, Util.singleToDoubleQuotes(cert));
			cstmt.setString(13, Util.changeStringToCommaSeperation(CoreCompt));
			cstmt.setString(14, Util.singleToDoubleQuotes(excludeinc));
			cstmt.setString(15, Util.singleToDoubleQuotes(excludebarred));
			cstmt.setString(16, partnerRating);
			cstmt.setString(17, securityCertifications);
			cstmt.setString(18, queryString);
			cstmt.setString(19, action);
			cstmt.setString(20, countyID);
			cstmt.setString(21, cityName);

			if (powoId != null && !powoId.equals("")) {
				cstmt.setString(22, powoId);
			} else {
				cstmt.setString(22, "0");
			}

			if (recommendedList != null && recommendedList.length > 0) {
				cstmt.setString(23,
						Util.changeStringToCommaSeperation(recommendedList));
			} else {
				cstmt.setString(23, "");
			}

			if (restrictedList != null && restrictedList.length > 0) {
				cstmt.setString(24,
						Util.changeStringToCommaSeperation(restrictedList));
			} else {
				cstmt.setString(24, "");
			}

			if (toolKitArr != null && toolKitArr.length > 0) {
				cstmt.setString(25,
						Util.changeStringToCommaSeperation(toolKitArr));
			} else {
				cstmt.setString(25, "");
			}

			int i = 0;
			int j = 0;
			int k = 0;
			cstmt.setString(26, Util.singleToDoubleQuotes(recevied));
			rs = cstmt.executeQuery();

			Partner_SearchBean psForm = null;
			while (rs.next()) {
				String pId = rs.getString("cp_partner_id");
				if (badgeStatus.equals("LIKE '%'")
						&& mySQLDBPartnerMap.containsKey(pId)) {
					continue;
				} else if (!badgeStatus.equals("")
						&& !badgeStatus.equals("LIKE '%'")
						&& !mySQLDBPartnerMap.containsKey(pId)) {
					continue;
				}
				psForm = new Partner_SearchBean();

				// checking from mysql generated list either this id got badge
				// printed or not
				if (keyList.contains(pId)) {
					psForm.setBadgePrinted("true");
				} else {
					psForm.setBadgePrinted("false");
				}

				psForm.setPid(pId);
				// Changes start For to show update date and create date when
				// LAST DATE not present..
				psForm.setUpdateDate(rs.getString("cp_pt_updated_date"));
				psForm.setCreateDate(rs.getString("cp_pt_created_date"));
				// Changes end For to show update date and create date when
				// LAST DATE not present..
				psForm.setPartnername(rs.getString("lo_om_division"));
				if (psForm.getPartnername().length() > 20) {
					psForm.setPartnernameTemp(psForm.getPartnername()
							.substring(0, 18) + "...");
				} else {
					psForm.setPartnernameTemp(psForm.getPartnername());
				}
				psForm.setCity(rs.getString("lo_ad_city"));
				if (psForm.getCity().length() > 15) {
					psForm.setCityTemp(psForm.getCity().substring(0, 13)
							+ "...");
				} else {
					psForm.setCityTemp(psForm.getCity());
				}
				psForm.setState(rs.getString("lo_ad_state"));
				psForm.setZipcode(rs.getString("lo_ad_zip_code"));
				psForm.setCountry(rs.getString("lo_ad_country"));
				psForm.setIncflag(rs.getString("incident_reported"));
				psForm.setBarredflag(rs.getString("barred_partner"));
				psForm.setPhone(rs.getString("lo_om_phone1"));
				psForm.setStatus(rs.getString("cp_ps_status"));
				psForm.setPartnerType(rs.getString("lo_ot_name"));
				psForm.setJobInfo(rs.getString("chkJob"));
				psForm.setDistance(dfcur.format(rs.getFloat("distance")) + "");
				psForm.setIncTypeFlag(rs.getString("cp_pd_incident_type"));
				psForm.setPartnerAddress1(rs.getString("lo_ad_address1"));
				psForm.setPartnerAddress2(rs.getString("lo_ad_address2"));
				psForm.setPartnerPriName(rs.getString("lo_pc_first_name") + " "
						+ rs.getString("lo_pc_last_name"));
				psForm.setPartnerPriPhone(rs.getString("lo_pc_phone1"));
				psForm.setPartnerPriCellPhone(rs.getString("lo_pc_cell_phone"));
				if (psForm.getPartnerPriCellPhone() != null
						&& psForm.getPartnerPriCellPhone().length() > 12) {
					psForm.setPartnerPriCellPhoneTemp(psForm
							.getPartnerPriCellPhone().substring(0, 10) + "...");
				} else {
					psForm.setPartnerPriCellPhoneTemp(psForm
							.getPartnerPriCellPhone());
				}
				if (psForm.getPartnerPriCellPhone() != null
						&& !psForm.getPartnerPriCellPhone().equals("")) {
					try {
						psForm.setPartnerPriCellPhonePlain(psForm
								.getPartnerPriCellPhone().replaceAll(
										"[,/(/)/.\\-/_/ ]", ""));
						Double.parseDouble(psForm.getPartnerPriCellPhonePlain());
					} catch (Exception e) {
						psForm.setPartnerPriCellPhonePlain("");
						logger.error(e.getMessage());
					}

				}
				psForm.setPartnerPriEmail(rs.getString("lo_pc_email1"));
				psForm.setPartnerAvgRating(rs.getString("qc_ps_average_rating"));
				psForm.setContractedCount(rs.getString("qc_ps_count"));
				psForm.setCompeletedCount(rs
						.getString("qc_ps_completed_satisfactorily_no"));
				psForm.setQuesProfessional(rs
						.getString("qc_ps_professional_no"));
				psForm.setQuesOnTime(rs.getString("qc_ps_on_time_no"));
				psForm.setQuesKnowledgeable(rs
						.getString("qc_ps_knowledgeable_no"));
				psForm.setQuesEquipped(rs
						.getString("qc_ps_properly_equipped_no"));

				psForm.setPartnerAdpo(rs.getString("cp_pd_adpo"));

				psForm.setPartnerLatitude(rs.getString("lo_ad_latitude"));
				psForm.setPartnerLongitude(rs.getString("lo_ad_longitude"));

				psForm.setPartnerLastUsed(rs.getString("cp_pd_last_used"));

				if (psForm.getPartnerLastUsed() == null
						|| psForm.getPartnerLastUsed().equals("")) {
					if (psForm.getUpdateDate() != null
							&& !psForm.getUpdateDate().equals("")) {
						psForm.setPartnerLastUsed(psForm.getUpdateDate());
					} else {
						psForm.setPartnerLastUsed(psForm.getCreateDate());
					}

				}

				psForm.setIcontype(setGMapIconForPartner(
						rs.getString("lo_ot_name"),
						rs.getString("qc_ps_average_rating"),
						rs.getString("cp_pd_status")));
				psForm.setBid(dfcurDollar.format(rs.getFloat("bid")) + "");
				psForm.setVendexMatches(rs.getString("vendex_matches"));
				psForm.setCmboxcorecompetencies(rs
						.getString("cp_pd_core_competency"));
				psForm.setPoCount(rs.getString("po_count"));
				psForm.setLeadMinuteman(rs.getBoolean("cp_pt_lead_minuteman"));
				psForm.setPartnerDeliverableDate(rs
						.getString("podb_partner_uploaded_date"));
				psForm.setDeliverableSource(rs
						.getString("podb_partner_upload_source"));

				// The below two properties for thumbsup & thumbsdown count;
				int thumpsupCount = Integer.valueOf(rs
						.getString("thumbsup_count"));
				int thumpsdownCount = Integer.valueOf(rs
						.getString("thumbsdown_count"));
				psForm.setThumbsUpCount(thumpsupCount == 0 ? "" : String
						.valueOf(thumpsupCount));
				psForm.setThumbsDownCount(thumpsdownCount == 0 ? "" : String
						.valueOf(thumpsdownCount));

				psForm.setOnTimePercentage(rs.getString("on_time_percentage"));

				if (powoId != null && !powoId.equals("")) {
					if (!rs.getString("bid").equals("0")) {
						psForm.setBid(rs.getString("bid"));
						listWithBidValue.add(i, psForm);
						i++;
					} else {
						psForm.setBid("");
						if (!psForm.getStatus().equals("Dormant")) {
							listWithOutBidValueActiveParnter.add(j, psForm);
							j++;
						} else {
							listWithOutBidValueDornmentParnter.add(k, psForm);
							k++;
						}

					}

				} else {
					psForm.setBid("N/A");
					if (!psForm.getStatus().equals("Dormant")) {
						listWithOutBidValueActiveParnter.add(j, psForm);
						j++;
					} else {
						listWithOutBidValueDornmentParnter.add(k, psForm);
						k++;
					}

				}

				// ************************************************************
				// ONLY A MAX OF 10000 records are allowed to be shown
				if (i > 10000) {
					if (logger.isDebugEnabled()) {
						logger.debug("getSearchpartnerdetails(String, String, String, String, String, String, String, String, String, String[], String, String[], String, String, String, String, DataSource) - ONLY A MAX OF 10000 records are allowed to be shown...so exiting search!");
					}
					break;
				}
				// ************************************************************
			}

			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"getSearchpartnerdetails(String, String, String, String, String, String, String, String, String, String[], String, String[], String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSearchpartnerdetails(String, String, String, String, String, String, String, String, String, String[], String, String[], String, String, String, String, DataSource) - Error occured during searching"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (mySqlRs1 != null) {
					mySqlRs1.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchpartnerdetails(String, String, String, String, String, String, String, String, String, String[], String, String[], String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
				if (mySqlStmt != null) {
					mySqlStmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchpartnerdetails(String, String, String, String, String, String, String, String, String, String[], String, String[], String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
				if (mySqlConn != null) {
					mySqlConn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchpartnerdetails(String, String, String, String, String, String, String, String, String, String[], String, String[], String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}

		ArrayList newList = new ArrayList();
		newList.addAll(listWithBidValue);
		newList.addAll(listWithOutBidValueActiveParnter);
		newList.addAll(listWithOutBidValueDornmentParnter);

		return newList;
	}

	/**
	 * This method searches partner as per the criteria selected. Its a
	 * duplicate method of above search method, it adds up partner status
	 * 
	 * @param Partnername
	 *            String.partner name
	 * @param keyWord
	 *            String.keyWords
	 * @param partnerType
	 *            String.type of partner
	 * @param mcsaVersionID
	 *            String.mcsa version of the partner
	 * @param zp
	 *            String.zipcode of partner
	 * @param radius
	 *            String.distance
	 * @param MainOffice
	 *            String.MainOffice
	 * @param fieldOffice
	 *            Steing.fieldOffice
	 * @param techZip
	 *            String.technician
	 * @param tech
	 *            String.technician
	 * @param cert
	 *            String.certifications
	 * @param CoreCompt
	 *            String.CoreCompetencyt
	 * @param excludebarred
	 *            String.excludebarred
	 * @param excludeinc
	 *            String.excludeinc
	 * @param partnerRating
	 *            String.rating of partners
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, partner searched as per
	 *         conditions applied.
	 */
	public static ArrayList getSearchpartnerdetailsWithStatus(
			String Partnername, String keyWord, String[] partnerType,
			String mcsaVersionId, String zip, String radius, String MainOffice,
			String fieldOffice, String techZip, String[] tech, String cert,
			String[] CoreCompt, String excludeinc, String excludebarred,
			String partnerRating, String securityCertifications,
			String queryString, String action, DataSource ds, String countyID,
			String cityName, String powoId, String[] recommendedList,
			String[] restrictedList, String[] toolKitArr, String badgeStatus,
			String partnerStatus, String received, String drugScreen) {
		Connection conn = null;
		// Start :Added By Amit
		DecimalFormat dfcur = new DecimalFormat("###0.0");
		DecimalFormat dfcur1 = new DecimalFormat("###.#####");
		DecimalFormat dfcurDollar = new DecimalFormat("#,##0.00");
		// End :
		CallableStatement cstmt = null;
		ResultSet rs = null;
		List listWithBidValue = new ArrayList();
		List listWithOutBidValueActiveParnter = new ArrayList();
		List listWithOutBidValueDornmentParnter = new ArrayList();

		Connection mySqlConn = null;
		Statement mySqlStmt = null;
		ResultSet mySqlRs1 = null;
		Map<String, PartnerSearchMySQLBean> mySQLDBPartnerMap = new HashMap<String, PartnerSearchMySQLBean>();

		int retval = 0;
		if (countyID == null) {
			countyID = "0";
		}
		if (cityName == null) {
			cityName = "0";
		}

		try {

			try {
				String mySqlUrl = EnvironmentSelector
						.getBundleString("appendix.detail.changestatus.mysql.url");
				mySqlConn = MySqlConnection.getMySqlConnection(mySqlUrl);
			} catch (Exception e) {
				logger.error("getSearchpartnerdetails(...Parms...)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getSearchpartnerdetails(...Parms...) - Error in establishing connection to mySQL.");
				}
			}

			if (mySqlConn != null && !badgeStatus.equals("")) {
				mySqlStmt = mySqlConn.createStatement();
				String mySqlQuery = "SELECT * FROM cp_photo_uploads WHERE STATUS  "
						+ badgeStatus;
				mySqlRs1 = mySqlStmt.executeQuery(mySqlQuery);

				PartnerSearchMySQLBean bean = null;
				while (mySqlRs1.next()) {
					bean = new PartnerSearchMySQLBean();
					bean.setPid(mySqlRs1.getString("partner_id"));
					bean.setBadgeName(mySqlRs1.getString("badge_name"));
					bean.setStatus(mySqlRs1.getString("status"));
					mySQLDBPartnerMap.put(bean.getPid(), bean);

				}

			}

			// checking for the partners having badge or without badge
			ArrayList<String> keyList = null;
			try {
				mySqlStmt = mySqlConn.createStatement();
				String mySqlQuery = "SELECT DISTINCT partner_id FROM cp_photo_uploads WHERE status='Printed'";

				ResultSet keysSet = mySqlStmt.executeQuery(mySqlQuery);
				keyList = new ArrayList<String>();
				while (keysSet.next()) {
					keyList.add(keysSet.getString("partner_id"));
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
			conn = ds.getConnection();
		
			cstmt = conn
					.prepareCall("{?=call cp_partner_search_list_new_with_partner_status_02( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?, ?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, Util.singleToDoubleQuotes(Partnername));
			cstmt.setString(3, Util.singleToDoubleQuotes(keyWord));
			cstmt.setString(4, Util.changeStringToCommaSeperation(partnerType));
			cstmt.setString(5, mcsaVersionId);
			cstmt.setString(6, zip);
			cstmt.setInt(7, Integer.parseInt(radius));
			cstmt.setString(8, Util.singleToDoubleQuotes(MainOffice));
			cstmt.setString(9, Util.singleToDoubleQuotes(fieldOffice));
			cstmt.setString(10, Util.singleToDoubleQuotes(techZip));
			cstmt.setString(11, Util.changeStringToCommaSeperation(tech));
			cstmt.setString(12, Util.singleToDoubleQuotes(cert));
			cstmt.setString(13, Util.changeStringToCommaSeperation(CoreCompt));
			cstmt.setString(14, Util.singleToDoubleQuotes(excludeinc));
			cstmt.setString(15, Util.singleToDoubleQuotes(excludebarred));
			cstmt.setString(16, partnerRating);
			cstmt.setString(17, securityCertifications);
			cstmt.setString(18, queryString);
			cstmt.setString(19, action);
			cstmt.setString(20, countyID);
			cstmt.setString(21, cityName);

			if (powoId != null && !powoId.equals("")) {
				cstmt.setString(22, powoId);
			} else {
				cstmt.setString(22, "0");
			}

			if (recommendedList != null && recommendedList.length > 0) {
				cstmt.setString(23,
						Util.changeStringToCommaSeperation(recommendedList));
			} else {
				cstmt.setString(23, "");
			}

			if (restrictedList != null && restrictedList.length > 0) {
				cstmt.setString(24,
						Util.changeStringToCommaSeperation(restrictedList));
			} else {
				cstmt.setString(24, "");
			}

			if (toolKitArr != null && toolKitArr.length > 0) {
				cstmt.setString(25,
						Util.changeStringToCommaSeperation(toolKitArr));
			} else {
				cstmt.setString(25, "");
			}

			if (partnerStatus.equals("All")) {
				cstmt.setString(26, "All");
			} else {
				cstmt.setString(26, partnerStatus);
			}
			int i = 0;
			int j = 0;
			int k = 0;
			cstmt.setString(27, Util.singleToDoubleQuotes(received));
			cstmt.setString(28, Util.singleToDoubleQuotes(drugScreen));
			rs = cstmt.executeQuery();

			Partner_SearchBean psForm = null;
			while (rs.next()) {
				String pId = rs.getString("cp_partner_id");
				if (badgeStatus.equals("LIKE '%'")
						&& mySQLDBPartnerMap.containsKey(pId)) {
					continue;
				} else if (!badgeStatus.equals("")
						&& !badgeStatus.equals("LIKE '%'")
						&& !mySQLDBPartnerMap.containsKey(pId)) {
					continue;
				}
				psForm = new Partner_SearchBean();

				// checking from mysql generated list either this id got badge
				// printed or not
				if (keyList != null && keyList.contains(pId)) {
					psForm.setBadgePrinted("true");
				} else {
					psForm.setBadgePrinted("false");
				}

				psForm.setPid(pId);
				// Changes start For to show update date and create date when
				// LAST DATE not present..
				psForm.setUpdateDate(rs.getString("cp_pt_updated_date"));
				psForm.setCreateDate(rs.getString("cp_pt_created_date"));
				// Changes end For to show update date and create date when
				// LAST DATE not present..
				psForm.setPartnername(rs.getString("lo_om_division"));
				if (psForm.getPartnername().length() > 20) {
					psForm.setPartnernameTemp(psForm.getPartnername()
							.substring(0, 18) + "...");
				} else {
					psForm.setPartnernameTemp(psForm.getPartnername());
				}
				psForm.setCity(rs.getString("lo_ad_city"));
				if (psForm.getCity().length() > 15) {
					psForm.setCityTemp(psForm.getCity().substring(0, 13)
							+ "...");
				} else {
					psForm.setCityTemp(psForm.getCity());
				}
				psForm.setState(rs.getString("lo_ad_state"));
				psForm.setZipcode(rs.getString("lo_ad_zip_code"));
				psForm.setCountry(rs.getString("lo_ad_country"));
				psForm.setIncflag(rs.getString("incident_reported"));
				psForm.setBarredflag(rs.getString("barred_partner"));
				psForm.setPhone(rs.getString("lo_om_phone1"));
				psForm.setStatus(rs.getString("cp_ps_status"));
				psForm.setPartnerType(rs.getString("lo_ot_name"));
				psForm.setJobInfo(rs.getString("chkJob"));
				psForm.setDistance(dfcur.format(rs.getFloat("distance")) + "");
				psForm.setIncTypeFlag(rs.getString("cp_pd_incident_type"));
				psForm.setPartnerAddress1(rs.getString("lo_ad_address1"));
				psForm.setPartnerAddress2(rs.getString("lo_ad_address2"));
				psForm.setPartnerPriName(rs.getString("lo_pc_first_name") + " "
						+ rs.getString("lo_pc_last_name"));
				psForm.setPartnerPriPhone(rs.getString("lo_pc_phone1"));
				psForm.setPartnerPriCellPhone(rs.getString("lo_pc_cell_phone"));
				if (psForm.getPartnerPriCellPhone() != null
						&& psForm.getPartnerPriCellPhone().length() > 12) {
					psForm.setPartnerPriCellPhoneTemp(psForm
							.getPartnerPriCellPhone().substring(0, 10) + "...");
				} else {
					psForm.setPartnerPriCellPhoneTemp(psForm
							.getPartnerPriCellPhone());
				}
				if (psForm.getPartnerPriCellPhone() != null
						&& !psForm.getPartnerPriCellPhone().equals("")) {
					try {
						psForm.setPartnerPriCellPhonePlain(psForm
								.getPartnerPriCellPhone().replaceAll(
										"[,/(/)/.\\-/_/ ]", ""));
						Double.parseDouble(psForm.getPartnerPriCellPhonePlain());
					} catch (Exception e) {
						psForm.setPartnerPriCellPhonePlain("");
						logger.error(e.getMessage());
					}

				}
				psForm.setPartnerPriEmail(rs.getString("lo_pc_email1"));
				psForm.setPartnerAvgRating(rs.getString("qc_ps_average_rating"));
				psForm.setContractedCount(rs.getString("qc_ps_count"));
				psForm.setCompeletedCount(rs
						.getString("qc_ps_completed_satisfactorily_no"));
				psForm.setQuesProfessional(rs
						.getString("qc_ps_professional_no"));
				psForm.setQuesOnTime(rs.getString("qc_ps_on_time_no"));
				psForm.setQuesKnowledgeable(rs
						.getString("qc_ps_knowledgeable_no"));
				psForm.setQuesEquipped(rs
						.getString("qc_ps_properly_equipped_no"));

				psForm.setPartnerAdpo(rs.getString("cp_pd_adpo"));

				psForm.setPartnerLatitude(rs.getString("lo_ad_latitude"));
				psForm.setPartnerLongitude(rs.getString("lo_ad_longitude"));

				psForm.setPartnerLastUsed(rs.getString("cp_pd_last_used"));
				psForm.setToollist(getToolListwithcpname(pId, ds));
				if (psForm.getPartnerLastUsed() == null
						|| psForm.getPartnerLastUsed().equals("")) {
					if (psForm.getUpdateDate() != null
							&& !psForm.getUpdateDate().equals("")) {
						psForm.setPartnerLastUsed(psForm.getUpdateDate());
					} else {
						psForm.setPartnerLastUsed(psForm.getCreateDate());
					}

				}

				psForm.setIcontype(setGMapIconForPartner(
						rs.getString("lo_ot_name"),
						rs.getString("qc_ps_average_rating"),
						rs.getString("cp_pd_status")));
				psForm.setBid(dfcurDollar.format(rs.getFloat("bid")) + "");
				psForm.setVendexMatches(rs.getString("vendex_matches"));
				psForm.setCmboxcorecompetencies(rs
						.getString("cp_pd_core_competency"));
				psForm.setPoCount(rs.getString("po_count"));
				psForm.setLeadMinuteman(rs.getBoolean("cp_pt_lead_minuteman"));
				psForm.setPartnerDeliverableDate(rs
						.getString("podb_partner_uploaded_date"));
				psForm.setDeliverableSource(rs
						.getString("podb_partner_upload_source"));

				// The below two properties for thumbsup & thumbsdown count;
				int thumpsupCount = Integer.valueOf(rs
						.getString("thumbsup_count"));
				int thumpsdownCount = Integer.valueOf(rs
						.getString("thumbsdown_count"));
				psForm.setThumbsUpCount(thumpsupCount == 0 ? "" : String
						.valueOf(thumpsupCount));
				psForm.setThumbsDownCount(thumpsdownCount == 0 ? "" : String
						.valueOf(thumpsdownCount));

				psForm.setOnTimePercentage(rs.getString("on_time_percentage"));

				if (powoId != null && !powoId.equals("")) {
					if (!rs.getString("bid").equals("0")) {
						psForm.setBid(rs.getString("bid"));
						listWithBidValue.add(i, psForm);
						i++;
					} else {
						psForm.setBid("");
						if (!psForm.getStatus().equals("Dormant")) {
							listWithOutBidValueActiveParnter.add(j, psForm);
							j++;
						} else {
							listWithOutBidValueDornmentParnter.add(k, psForm);
							k++;
						}

					}

				} else {
					psForm.setBid("N/A");
					if (!psForm.getStatus().equals("Dormant")) {
						listWithOutBidValueActiveParnter.add(j, psForm);
						j++;
					} else {
						listWithOutBidValueDornmentParnter.add(k, psForm);
						k++;
					}

				}

				// ************************************************************
				// ONLY A MAX OF 10000 records are allowed to be shown
				if (i > 10000) {
					if (logger.isDebugEnabled()) {
						logger.debug("getSearchpartnerdetails(String, String, String, String, String, String, String, String, String, String[], String, String[], String, String, String, String, DataSource) - ONLY A MAX OF 10000 records are allowed to be shown...so exiting search!");
					}
					break;
				}
				// ************************************************************
			}

			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"getSearchpartnerdetails(String, String, String, String, String, String, String, String, String, String[], String, String[], String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSearchpartnerdetails(String, String, String, String, String, String, String, String, String, String[], String, String[], String, String, String, String, DataSource) - Error occured during searching"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (mySqlRs1 != null) {
					mySqlRs1.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchpartnerdetails(String, String, String, String, String, String, String, String, String, String[], String, String[], String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
				if (mySqlStmt != null) {
					mySqlStmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchpartnerdetails(String, String, String, String, String, String, String, String, String, String[], String, String[], String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
				if (mySqlConn != null) {
					mySqlConn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchpartnerdetails(String, String, String, String, String, String, String, String, String, String[], String, String[], String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		ArrayList newList = new ArrayList();
		newList.addAll(listWithBidValue);
		newList.addAll(listWithOutBidValueActiveParnter);
		newList.addAll(listWithOutBidValueDornmentParnter);

		return newList;
	}

	private static String getAPDO(String pid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String apdo = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select cp_pd_company_name, cp_pd_partner_id, lm_po_type, AVG(datediff(day, lm_pi_invoice_date,lm_pi_gp_paid_date)) as adpo "
					+ "from [dbo].[lm_partner_invoice] as a "
					+ "join lm_purchase_order as b on a.lm_pi_po_id = b.lm_po_id "
					+ "join cp_partner_details as c on b.lm_po_partner_id = c.cp_pd_partner_id "
					+ "WHERE cp_pd_partner_id = "
					+ pid
					+ " AND lm_pi_status = 'Paid' "
					+ "GROUP BY cp_pd_partner_id, cp_pd_company_name, lm_po_type";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				apdo = rs.getNString(3);
			}
		} catch (Exception e) {
			logger.error("getAPDO(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAPDO(String, DataSource) - Error occured during getting APDO  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("ggetAPDO(String, DataSource) - exception ignored",
						e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn("getAPDO(String, DataSource) - exception ignored",
						e);

			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("getAPDO(String, DataSource) - exception ignored",
						e);

			}

		}
		return apdo;
	}

	public static String setGMapIconForPartner(String partnerType,
			String partnerRating, String partnerStatus) {
		StringBuffer imageBuffer = new StringBuffer();
		if (partnerType.equals("Certified Partners - Speedpay Users")) {
			partnerType = "Speedpay";
		}
		imageBuffer = imageBuffer.append(partnerType.trim().substring(0, 1));
		imageBuffer = imageBuffer.append(getImageColor(partnerRating,
				partnerStatus));
		return imageBuffer.toString();
	}

	private static String getImageColor(String partnerRating,
			String partnerStatus) {
		String color = "";

		if (partnerStatus.equals("R")) {
			color = "orange";
			return color;
		} else {
			if (partnerRating.equals("")) {
				partnerRating = "0.00";
			}
			float pRating = Float.parseFloat(partnerRating);
			if (pRating >= 4.51) {
				color = "green";
			} else if (pRating >= 3.51 && pRating <= 4.50) {
				color = "yellow";
			} else if (pRating < 3.51) {
				color = "red";
			} else {
				color = "red";
			}
		}
		return color;
	}

	/**
	 * Method used to get the first 25 partners from the search result
	 * 
	 * @param ArrayList
	 *            partner search result
	 * @return String xml generated from the partner search result
	 * @see
	 * 
	 */
	public static String convertPartnerRatingToXML(ArrayList partnerList) {
		ResourceBundle bundle = ResourceBundle
				.getBundle("com.mind.properties.ApplicationResources");
		String finalXML = "";
		int count = Integer.parseInt(bundle.getString("partnerToDisplay"));
		List miniumPartner;
		if (partnerList.size() >= count) {
			miniumPartner = partnerList.subList(0, count);
		} else {
			miniumPartner = partnerList;
		}
		finalXML = generateXML(miniumPartner);
		return finalXML;
	}

	/**
	 * Method used to generate the xml for the sublist to be displayed in GMap
	 * 
	 * @param ArrayList
	 *            partner search result
	 * @return String xml generated from the partner search result
	 * @see
	 * 
	 */
	public static String generateXML(List plist) {
		Iterator listIterator = plist.iterator();
		String partnerType = "";
		String partnerAddress2 = "";
		StringBuffer partnerXML = new StringBuffer();
		partnerXML.append("\'<?xml version = \"1.0\"?>");
		partnerXML.append("<partners>");

		while (listIterator.hasNext()) {
			Partner_SearchBean partnerDetail = (Partner_SearchBean) listIterator
					.next();
			if (partnerDetail.getPartnerType().equals(
					"Certified Partners - Speedpay Users")) {
				partnerType = "Speedpay";
			} else {
				partnerType = partnerDetail.getPartnerType();
			}

			if (partnerDetail.getPartnerAddress2().equals("")) {
				partnerAddress2 = "";
			} else {
				partnerAddress2 = ", " + partnerDetail.getPartnerAddress2();
			}
			partnerXML.append("<partner");
			partnerXML.append(" partnerId =\"" + partnerDetail.getPid() + "\"");
			partnerXML.append(" lat =\"" + partnerDetail.getPartnerLatitude()
					+ "\"");
			partnerXML.append(" lon =\"" + partnerDetail.getPartnerLongitude()
					+ "\"");
			partnerXML.append(" partnerName =\""
					+ Util.filter(partnerDetail.getPartnername()) + "\"");
			partnerXML.append(" partnerType =\"" + Util.filter(partnerType)
					+ "\"");
			partnerXML.append(" partnerAddress1 =\""
					+ Util.filter(partnerDetail.getPartnerAddress1()) + "\"");
			partnerXML.append(" partnerAddress2 =\""
					+ Util.filter(partnerAddress2) + "\"");
			partnerXML.append(" partnerCity =\""
					+ Util.filter(partnerDetail.getCity()) + "\"");
			partnerXML.append(" partnerState =\""
					+ Util.filter(partnerDetail.getState()) + "\"");
			partnerXML.append(" partnerZipCode =\""
					+ Util.filter(partnerDetail.getZipcode()) + "\"");
			partnerXML.append(" partnerPhone =\""
					+ Util.filter(partnerDetail.getPhone()) + "\"");
			partnerXML.append(" partnerPriName =\""
					+ Util.filter(partnerDetail.getPartnerPriName()) + "\"");
			partnerXML.append(" partnerPriPhone =\""
					+ Util.filter(partnerDetail.getPartnerPriPhone()) + "\"");
			partnerXML.append(" partnerPriCell =\""
					+ Util.filter(partnerDetail.getPartnerPriCellPhone())
					+ "\"");
			partnerXML.append(" partnerPriEmail =\""
					+ Util.filter(partnerDetail.getPartnerPriEmail()) + "\"");
			partnerXML.append(" partnerDistance =\""
					+ Util.filter(partnerDetail.getDistance()) + "\"");
			partnerXML.append(" icontype =\""
					+ Util.filter(partnerDetail.getIcontype()) + "\"");
			partnerXML.append("/>");

		}
		partnerXML.append("</partners>\'");
		return partnerXML.toString();
	}

	public static int updatePartnerBAdgeStatus(String partnerId) {

		String mySqlUrl = EnvironmentSelector
				.getBundleString("appendix.detail.changestatus.mysql.url");

		Connection mySqlConn = null;
		Statement mySqlStmt = null;

		try {
			mySqlConn = MySqlConnection.getMySqlConnection(mySqlUrl);
			mySqlStmt = mySqlConn.createStatement();
			return mySqlStmt
					.executeUpdate("update cp_photo_uploads set status = 'Declined' where status in ('Printed', 'Approved') and partner_id = "
							+ partnerId);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return -1;
	}

	/**
	 * Method used to get the detail specific to a partner based on the partner
	 * id
	 * 
	 * @param PartnerDetailForm
	 *            object of the PartnerDetailForm class
	 * @param DataSource
	 *            datasource object
	 * @return String whether partner exists or not.
	 * @see
	 * 
	 */
	public static String getSearchPartnerDetail(PartnerDetailBean partnerform,
			String url, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String found = "";

		Connection mySqlConn = null;
		Statement mySqlStmt = null;
		ResultSet mySqlRs1 = null;

		String sql = "select * from dbo.func_cp_partner_info_general_tab("
				+ partnerform.getPartnerId() + ")";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				partnerform.setPartnerName(rs.getString("lo_om_division"));
				partnerform.setPartnerAddress1(rs.getString("lo_ad_address1"));
				partnerform.setPartnerAddress2(rs.getString("lo_ad_address2"));
				partnerform.setPartnerCity(rs.getString("lo_ad_city"));
				partnerform.setPartnerState(rs.getString("lo_ad_state"));
				partnerform.setPartnerZipCode(rs.getString("lo_ad_zip_code"));
				partnerform.setPartnerPhone(rs.getString("lo_om_phone1"));
				partnerform.setPartnerPriName(rs.getString("pri_pc_first_name")
						+ " " + rs.getString("pri_pc_last_name"));
				partnerform.setPartnerPriPhone(rs.getString("pri_pc_phone"));
				partnerform.setPartnerPriEmail(rs.getString("pri_pc_email"));
				partnerform.setPartnerType(rs.getString("lo_ot_name"));
				partnerform.setPartnerCellPhone(rs
						.getString("pri_pc_cell_phone"));

				// Badge image related code, setting empty to avoid exception.
				partnerform.setBadgeName("");
				partnerform.setStatus("");
				partnerform.setUpdatedBy("");
				partnerform.setImgName("");
				partnerform.setImgPath("");

				found = "partnerFound";
			}

			String mySqlUrl = EnvironmentSelector
					.getBundleString("appendix.detail.changestatus.mysql.url");
			mySqlConn = MySqlConnection.getMySqlConnection(mySqlUrl);
			mySqlStmt = mySqlConn.createStatement();
			String mySqlQuery = "SELECT * FROM cp_photo_uploads WHERE partner_id ="
					+ partnerform.getPartnerId()
					+ " and  status in('Printed', 'Approved') ORDER BY  updated_on DESC";
			mySqlRs1 = mySqlStmt.executeQuery(mySqlQuery);

			if (mySqlRs1.next()) {

				partnerform.setImgName(partnerform.getImgName()
						+ mySqlRs1.getString("badge_photo_name") + "||||");

				String[] photoName = mySqlRs1.getString("badge_photo").split(
						"/");

				String[] urlArr = url.split("\\.");

				partnerform
						.setImgPath(urlArr[0].replace("ilex", "pcc").replace(
								"http:", "https:")
								+ ".contingent.com/file_manager.php?action=get&type=2&file_name="
								+ photoName[(photoName.length - 1)]);

				partnerform.setBadgeName(partnerform.getBadgeName()
						+ mySqlRs1.getString("badge_name") + "||||");

			}

		} catch (Exception e) {
			logger.error(
					"getSearchPartnerDetail(PartnerDetailForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSearchPartnerDetail(PartnerDetailForm, DataSource) - Error occured during getting Partner details in to be display in the popup window :"
						+ e);
			}
			logger.error(
					"getSearchPartnerDetail(PartnerDetailForm, DataSource)", e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (mySqlRs1 != null) {
					mySqlRs1.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchPartnerDetail(PartnerDetailForm, DataSource) - exception ignored",
						e);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (mySqlStmt != null) {
					mySqlStmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchPartnerDetail(PartnerDetailForm, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
				if (mySqlConn != null) {
					mySqlConn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchPartnerDetail(PartnerDetailForm, DataSource) - exception ignored",
						e);
			}

		}
		return found;
	}

	/**
	 * Last sped pay date for work distribution
	 * 
	 * 
	 * 
	 */
	public static String getLastSpeedPayDate(
			PartnerTypeDetailBean partnerTypebean, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sql = new StringBuffer();

		try {

			sql.append("SELECT top 1 lm_po_issue_date FROM lm_purchase_order inner join "
					+ "qc_partner_job_rating on lm_po_js_id=qc_pj_lm_js_id where qc_pj_partner_cp_partner_id="
					+ partnerTypebean.getPartnerId()
					+ " and lm_po_type='S'  order by lm_po_issue_date desc");

			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql.toString());

			if (rs.next()) {

				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				String date = df.format(rs.getDate("lm_po_issue_date"));
				partnerTypebean.setLastSpeedpayDate(date.toString());

			}

		} catch (Exception e) {
			logger.error(
					"getSearchPartnerDetail(PartnerDetailForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSearchPartnerDetail(PartnerDetailForm, DataSource) - Error occured during getting Partner details in to be display in the popup window :"
						+ e);
			}
			logger.error(
					"getSearchPartnerDetail(PartnerDetailForm, DataSource)", e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getSearchPartnerDetail(PartnerDetailForm, DataSource) - exception ignored",
						e);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getSearchPartnerDetail(PartnerDetailForm, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getSearchPartnerDetail(PartnerDetailForm, DataSource) - exception ignored",
						e);
			}
		}
		return partnerTypebean.getLastSpeedpayDate();
	}

	/*
	 * This method is used for showing the divisions of speedpay jobs (how much
	 * done by m and so on types)
	 */
	public static ArrayList<LabelValue> getSpeedPayWorkDetail(
			PartnerTypeDetailBean partnerTypebean, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		Connection mySqlConn = null;
		Statement mySqlStmt = null;
		ResultSet mySqlRs1 = null;

		ArrayList<LabelValue> returnList = new ArrayList<LabelValue>();

		StringBuffer sql = new StringBuffer();

		sql.append("SELECT count(*) cnt, lm_po_type FROM lm_purchase_order inner join qc_partner_job_rating on lm_po_js_id=qc_pj_lm_js_id where qc_pj_partner_cp_partner_id=");
		sql.append(partnerTypebean.getPartnerId());
		sql.append(" group by lm_po_type ORDER BY lm_po_type desc");

		try {

			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql.toString());

			LabelValue bean = null;
			while (rs.next()) {
				bean = new LabelValue();

				if (rs.getString("lm_po_type") == null) {
					bean.setLabel("Others");
				} else if (rs.getString("lm_po_type").equals("M")) {
					bean.setLabel("Minuteman");
				} else if (rs.getString("lm_po_type").equals("S")) {
					bean.setLabel("Speedpay");
				} else if (rs.getString("lm_po_type").equals("N")) {
					bean.setLabel("Internal");
				} else if (rs.getString("lm_po_type").equals("P")) {
					bean.setLabel("PVS");
				}
				bean.setValue(rs.getString("cnt"));
				returnList.add(bean);
			}

			sql = new StringBuffer();

			sql.append("SELECT top 1 lm_po_issue_date FROM lm_purchase_order inner join "
					+ "qc_partner_job_rating on lm_po_js_id=qc_pj_lm_js_id where qc_pj_partner_cp_partner_id="
					+ partnerTypebean.getPartnerId()
					+ " and lm_po_type='S'  order by lm_po_issue_date desc");

			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql.toString());

			if (rs.next()) {

			}

		} catch (Exception e) {
			logger.error(
					"getSearchPartnerDetail(PartnerDetailForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSearchPartnerDetail(PartnerDetailForm, DataSource) - Error occured during getting Partner details in to be display in the popup window :"
						+ e);
			}
			logger.error(
					"getSearchPartnerDetail(PartnerDetailForm, DataSource)", e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (mySqlRs1 != null) {
					mySqlRs1.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchPartnerDetail(PartnerDetailForm, DataSource) - exception ignored",
						e);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (mySqlStmt != null) {
					mySqlStmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchPartnerDetail(PartnerDetailForm, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
				if (mySqlConn != null) {
					mySqlConn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchPartnerDetail(PartnerDetailForm, DataSource) - exception ignored",
						e);
			}

		}
		return returnList;
	}

	/**
	 * This method gets the state list from the database and return to the
	 * caller as an object of ArrayList
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This object will contain the description of tool list
	 */
	public static ArrayList<LabelValue> getStateList(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new LabelValue("Select", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_state_name , cp_state_id from cp_state ");
			while (rs.next()) {
				list.add(new LabelValue(rs.getString("cp_state_id"), rs
						.getString("cp_state_id")));
			}
		} catch (Exception e) {
			logger.error("getStateList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStateList(DataSource) - Exception caught" + e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn("getStateList(DataSource) - exception ignored", sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn("getStateList(DataSource) - exception ignored", sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn("getStateList(DataSource) - exception ignored", sql);
			}

		}
		return list;
	}

	public static ArrayList getStateCategoryList(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select cp_country_id, cp_country_name, cp_country_order from lp_country_list order by cp_country_order,cp_country_name";
			rs = stmt.executeQuery(sql);
			int i = 0;

			if (i == 0) {
				Partner_EditBean PVSForm = new Partner_EditBean();
				PVSForm.setSiteCountryId("");
				PVSForm.setSiteCountryIdName("");
				valuelist.add(i, PVSForm);
				i++;
			}

			while (rs.next()) {
				Partner_EditBean PVSForm = new Partner_EditBean();
				PVSForm.setSiteCountryId(rs.getString("cp_country_id"));
				PVSForm.setSiteCountryIdName(rs.getString("cp_country_name"));
				valuelist.add(i, PVSForm);
				i++;
			}

		} catch (Exception e) {
			logger.error("getStateCategoryList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStateCategoryList(DataSource) - Error occured during getting Partner State Category Wise List  ");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static ArrayList getCountryStateList(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new LabelValue("--Select--", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_state_name , cp_state_id , cp_country_id from cp_state ");
			while (rs.next()) {
				list.add(new LabelValue(rs.getString("cp_state_id"), rs
						.getString("cp_state_id")
						+ "~"
						+ rs.getString("cp_country_id") + "$"));
			}
		} catch (Exception e) {
			logger.error("getCountryStateList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCountryStateList(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getCountryStateList(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCountryStateList(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCountryStateList(DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	/**
	 * This method gets the state list from the database and zip codes of tax
	 * exempt and return to the caller as an object of ArrayList
	 * 
	 * @param pid
	 *            Partner id
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This object will contain the description of tool list
	 */
	// Added By vishal 21/12/2006
	public static ArrayList getPrimaryNameList(String pid, DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String strquery = "select isnull(lo_pc_first_name, '')+' '+isnull(lo_pc_last_name,'') as lo_pc_name,lo_pc_id,lo_pc_email1,lo_pc_phone1,lo_pc_cell_phone from lo_poc where lo_pc_om_id="
					+ pid;
			rs = stmt.executeQuery(strquery);

			while (rs.next()) {
				list.add(new LabelValue(rs.getString("lo_pc_name"), rs
						.getString("lo_pc_id")
						+ "*"
						+ rs.getString("lo_pc_email1")
						+ "~"
						+ rs.getString("lo_pc_phone1")
						+ "|"
						+ rs.getString("lo_pc_cell_phone") + "$"));
			}
		} catch (Exception e) {
			logger.error("getPrimaryNameList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPrimaryNameList(String, DataSource) - Exception caught in SQL: SELECT lo_pc_id FROM lo_poc  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getPrimaryNameList(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPrimaryNameList(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPrimaryNameList(String, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	// End:

	/**
	 * ********************************************************
	 */
	public static ArrayList getTaxExempt(String pid, DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_state_id , cp_state_name , '' as cp_pt_tax_id from cp_state"
							+ " where cp_state_id not in (select cp_pt_state_id from cp_partner_tax_exempt where cp_pt_partner_id ="
							+ pid
							+ ")"
							+ " union all"
							+ " select a.cp_state_id , a.cp_state_name , isnull( b.cp_pt_tax_id , '' ) as cp_pt_tax_id from cp_state a"
							+ " left outer join cp_partner_tax_exempt b on a.cp_state_id = b.cp_pt_state_id"
							+ " where b.cp_pt_partner_id = "
							+ pid
							+ " order by 1 ");
			while (rs.next()) {
				list.add(new PVSTaxExempt(rs.getString("cp_state_id"), rs
						.getString("cp_pt_tax_id"), rs
						.getString("cp_state_name")));
			}
		} catch (Exception e) {
			logger.error("getTaxExempt(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTaxExempt(String, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getTaxExempt(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getTaxExempt(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getTaxExempt(String, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	/**
	 * This method gets the core competence fields of company from the database
	 * and return to the caller as an object of ArrayList
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This object will contain the description of Core
	 *         Competence
	 */
	public static ArrayList<String[]> getToolKit(DataSource ds, int parentId) {
		ArrayList<String[]> list = new ArrayList<String[]>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String cond = " AND cp_tools_parent_id is null ";

			if (parentId != 0) {
				cond = " AND cp_tools_parent_id = " + parentId;
			}

			rs = stmt
					.executeQuery(" SELECT cp_tool_id, cp_tool_name, cp_tools_parent_id FROM cp_tools  "
							+ " WHERE cp_tool_active = 'Y'" + "  " + cond);
			while (rs.next()) {
				String[] st = new String[3];
				st[0] = rs.getString("cp_tool_id");
				st[1] = rs.getString("cp_tool_name");
				st[2] = rs.getString("cp_tools_parent_id");

				list.add(st);
			}
		} catch (Exception e) {
			logger.error("getToolKit(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getToolKit(DataSource) - Exception caught" + e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn("getToolKit(DataSource) - exception ignored", sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn("getToolKit(DataSource) - exception ignored", sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn("getToolKit(DataSource) - exception ignored", sql);
			}

		}
		return list;
	}

	/**
	 * This method gets the core competence fields of company from the database
	 * and return to the caller as an object of ArrayList
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This object will contain the description of Core
	 *         Competence
	 */
	public static List getCoreCompetancy(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_corecomp_name, cp_corecomp_id  from cp_corecompetency where isnull(cp_corecomp_name,'') <>'' and isnull(cp_corecomp_obsolete,'Y') = 'N' order by cp_corecomp_name");
			while (rs.next()) {
				list.add(new LabelValue(rs.getString("cp_corecomp_name"), rs
						.getString("cp_corecomp_id")));
			}
		} catch (Exception e) {
			logger.error("getCoreCompetancy(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCoreCompetancy(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getCoreCompetancy(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCoreCompetancy(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCoreCompetancy(DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	public static ArrayList getRecommendedRestrictedList(int type, DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "	Select distinct lp_mm_id , "
					+ " lo_ot_name = SUBSTRING(dbo.lo_organization_top.lo_ot_name, 1, 32) "
					+ " from dbo.lx_project_identifier_main inner join dbo.lp_msa_main "
					+ " on lp_mm_pi_id = lx_pi_id inner join dbo.lo_organization_top on lp_mm_ot_id = lo_ot_id "
					+ " LEFT JOIN cp_partner_recommend_restrict ON cp_rr_mm_id = lp_mm_id "
					+ " where " + " isnull(lp_mm_hide,'N') <> 'Y' "
					+ " AND cp_rr_type = " + type + "  ORDER BY lo_ot_name";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				list.add(new LabelValue(rs.getString("lo_ot_name"), rs
						.getString("lp_mm_id")));
			}
		} catch (Exception e) {
			logger.error("getRecommendedRestrictedList(int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRecommendedRestrictedList(int, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getRecommendedRestrictedList(int, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getRecommendedRestrictedList(int, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getRecommendedRestrictedList(int, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	public static Map<Integer, String> getCoreCompetancyWithShortNames(
			DataSource ds) {
		Map<Integer, String> coreComtancyMap = new LinkedHashMap<Integer, String>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from cp_corecompetency where isnull(cp_corecomp_name,'') <>'' and isnull(cp_corecomp_obsolete,'Y') = 'N' order by cp_corecomp_name");
			while (rs.next()) {
				coreComtancyMap.put(rs.getInt("cp_corecomp_id"),
						rs.getString("cp_corecomp_short_name"));
			}
		} catch (Exception e) {
			logger.error("getCoreCompetancy(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCoreCompetancy(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getCoreCompetancy(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCoreCompetancy(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCoreCompetancy(DataSource) - exception ignored",
						sql);
			}

		}
		return coreComtancyMap;
	}

	public static Map<String, String> getCoreCompetancies(DataSource ds) {
		Map<String, String> coreComtancyMap = new LinkedHashMap<String, String>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from cp_corecompetency where isnull(cp_corecomp_name,'') <>'' and isnull(cp_corecomp_obsolete,'Y') = 'N' order by cp_corecomp_name");
			while (rs.next()) {
				coreComtancyMap.put(rs.getString("cp_corecomp_name"),
						rs.getString("cp_corecomp_short_name"));
			}
		} catch (Exception e) {
			logger.error("getCoreCompetancy(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCoreCompetancy(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getCoreCompetancy(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCoreCompetancy(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCoreCompetancy(DataSource) - exception ignored",
						sql);
			}

		}
		return coreComtancyMap;
	}

	/**
	 * This method gets the tool list of company from the database and return to
	 * the caller as an object of ArrayList
	 * 
	 * @param form
	 *            object of Partner_EditForm
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This object will contain the description of tool list
	 */
	public static ArrayList getToolList(Partner_EditBean form, DataSource ds) {

		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_tool_id, cp_tool_name, '' as cp_zip_code from cp_tools"
							+ " where cp_tool_id not in (select cp_tool_id from cp_partner_tool_list where cp_partner_id ="
							+ form.getPid()
							+ ") "
							+ " union all"
							+ " select a.cp_tool_id, a.cp_tool_name, isnull(b.cp_zip_code,'') as cp_zip_code from cp_tools a"
							+ " left outer join cp_partner_tool_list b on a.cp_tool_id = b.cp_tool_id"
							+ " where b.cp_partner_id = "
							+ form.getPid()
							+ " order by cp_tool_name");

			while (rs.next()) {
				list.add(new PVSToolList(rs.getString("cp_tool_id"), rs
						.getString("cp_tool_name"), rs.getString("cp_zip_code")));
			}
			form.setToolList(list);

		} catch (Exception e) {
			logger.error("getToolList(Partner_EditForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getToolList(Partner_EditForm, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getToolList(Partner_EditForm, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getToolList(Partner_EditForm, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getToolList(Partner_EditForm, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	/**
	 * This method gets the equipment list of used engineers and technician from
	 * the database and return to the caller as an object of ArrayList
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This object will contain the description of equipment
	 *         list
	 */
	public static ArrayList getEquipmentList(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("SELECT cc_ed_id , cc_ed_name from cc_equipment_detail ");

			while (rs.next()) {
				list.add(new LabelValue(rs.getString("cc_ed_name"), rs
						.getString("cc_ed_name")));
			}
		} catch (Exception e) {
			logger.error("getEquipmentList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEquipmentList(DataSource) - Exception caught equipment list:"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn("getEquipmentList(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn("getEquipmentList(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn("getEquipmentList(DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	/**
	 * This method gets the Resource Level list of company from the database and
	 * return to the caller as an object of ArrayList
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This object will contain the description of Resource
	 *         Level
	 */
	public static ArrayList getResourceLevel(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		/* Start: Added By Atul 16/01/2007 */

		String sql1 = "select * from dbo.func_cp_resource_level()";
		/* End: Added By Atul 16/01/2007 */
		int count = 0;

		list.add(new LabelValue("---Select---", "0|"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery(sql1);

			while (rs.next()) {
				/* Start: Changed By Atul 16/01/2007 */
				list.add(new LabelValue(rs.getString("cp_res_name"), rs
						.getString("cp_res_id")
						+ "|"
						+ rs.getString("cp_res_name")));

				/* End: Changed By Atul 16/01/2007 */
				count++;
			}

		} catch (Exception e) {
			logger.error("getResourceLevel(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourceLevel(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn("getResourceLevel(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn("getResourceLevel(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn("getResourceLevel(DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	/**
	 * This method gets the Deopot Facilities from the database and return to
	 * the caller as an object of ArrayList String pid Partner id
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This object will contain the description of Depot
	 *         Facilities
	 */
	public static ArrayList getDepotFacilities(String pid, DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_se_id , cp_se_name, '' as cp_zip_code from cp_size_ecdf"
							+ " where cp_se_id not in (select cp_ecdf_se_id from cp_ecdf where cp_ecdf_partner_id ="
							+ pid
							+ ")"
							+ " union all"
							+ " select a.cp_se_id, a.cp_se_name, isnull(b.cp_ecdf_zipcode,'') as cp_zip_code from cp_size_ecdf a"
							+ " left outer join cp_ecdf b on a.cp_se_id = b.cp_ecdf_se_id"
							+ " where b.cp_ecdf_partner_id = "
							+ pid
							+ " order by 1  ");

			while (rs.next()) {
				list.add(new PVSDepotFacility(rs.getString("cp_se_id"), rs
						.getString("cp_se_name"), rs.getString("cp_zip_code")));
			}
		} catch (Exception e) {
			logger.error("getDepotFacilities(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDepotFacilities(String, DataSource) - Exception caught in SQL: SELECT cp_se_id , cp_se_name FROM cp_size_ecdf  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getDepotFacilities(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getDepotFacilities(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getDepotFacilities(String, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	/**
	 * This method gets the Highest Critically Available from the database and
	 * return to the caller as an object of ArrayList
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This object will contain the description of Highest
	 *         Critically Available
	 */
	public static ArrayList getHighestCriticallyAvailable(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select *  from cc_criticality_detail order by cc_cd_id ");

			while (rs.next()) {
				list.add(new LabelValue(rs.getString("cc_cd_name"), rs
						.getString("cc_cd_id")));
			}
		} catch (Exception e) {
			logger.error("getHighestCriticallyAvailable(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getHighestCriticallyAvailable(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getHighestCriticallyAvailable(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getHighestCriticallyAvailable(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getHighestCriticallyAvailable(DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	/**
	 * This method gets the data of company from the database and return to the
	 * caller as an object of ArrayList
	 * 
	 * @param type
	 *            Type of company i.e. wireless,Structured Distribution
	 *            Certified etc.
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This object will contain the description of company
	 *         depending on the type parameter
	 */
	public static ArrayList getCompany(String type, DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from cp_company where cp_comp_id in("
					+ "	Select cp_cert_comp_id from dbo.cp_companycerts where cp_cert_corecomp_id in("
					+ "		select cp_corecomp_id from dbo.cp_corecompetency where cp_corecomp_short_name='"
					+ type + "'))" + "order by cp_comp_name";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				list.add(new LabelValue(rs.getString("cp_comp_name"), rs
						.getString("cp_comp_id")));
			}
		} catch (Exception e) {
			logger.error("getCompany(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCompany(String, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getCompany(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCompany(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCompany(String, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	/**
	 * This method assigns job/activity/resource to a partner.
	 * 
	 * @param id
	 *            String.Jobid/activityid/resourceid
	 * @param partner_id
	 *            String.partner id
	 * @param type
	 *            String.job(J)/activity(A)/resource(R)
	 * @param user
	 *            String.userid
	 * @param ds
	 *            Object of DataSource
	 * @return int ` This method returns int, returns 0 if success otherwise -ve
	 *         values.
	 */
	public static int assignpartner(String job_id, String partner_id,
			String user, String type, String powoId, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		int retval = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_assign_partner_01( ? , ? , ? , ? , ? )}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, job_id);
			cstmt.setString(3, partner_id);
			cstmt.setString(4, user);
			cstmt.setString(5, type);
			cstmt.setString(6, powoId);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"assignpartner(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("assignpartner(String, String, String, String, String, DataSource) - Error occured during assigning partner "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"assignpartner(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"assignpartner(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"assignpartner(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retval;

	}

	/**
	 * 
	 * @param userid
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return int This method returns int, returns 0 if user has no HR role
	 *         else return 1
	 */
	public static int getCurrentUserRoleCountForHRRole(String userId,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int roleCount = 0;
		String partnername = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "SELECT 1 FROM lo_poc_rol "
					+ " JOIN lo_role ON lo_ro_id = lo_pr_ro_id "
					+ " WHERE lo_pr_pc_id = " + userId
					+ " AND lo_ro_role_name = 'HR'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				roleCount = rs.getInt(1);
			}

		} catch (Exception e) {
			logger.error(
					"getCurrentUserRoleCountForHRRole(String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCurrentUserRoleCountForHRRole(String, String, DataSource) - Error occured during getting getCurrentUserRoleCountForHRRole  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCurrentUserRoleCountForHRRole(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCurrentUserRoleCountForHRRole(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCurrentUserRoleCountForHRRole(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return roleCount;
	}

	/**
	 * This method checks whether a partner can be assigned a
	 * job/activity/resource.
	 * 
	 * @param typeid
	 *            String.Jobid/activityid/resourceid
	 * @param type
	 *            String.job(J)/activity(A)/resource(R)
	 * @param ds
	 *            Object of DataSource
	 * @return int This method returns int, returns 0 if partner can be assigned
	 *         otherwise 1.
	 */
	public static int checkassign(String typeid, String type, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int check = 0;
		String partnername = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select dbo.lm_partner_assign( " + typeid + " , '"
					+ type + "' )";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				check = rs.getInt(1);

			}

		} catch (Exception e) {
			logger.error("checkassign(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkassign(String, String, DataSource) - Error occured during getting checkassign  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"checkassign(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"checkassign(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"checkassign(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return check;
	}

	/**
	 * This method returns the details of Mcsa in a arraylist object containing
	 * the McsaDetailBean type of object which is used by MCSA_ViewAction class
	 * to get teh mcsa details
	 * 
	 * @param cp_mcsa_id
	 *            The id of the mcsa.
	 * @param cp_mcsa_data
	 *            The data of the mcsa.
	 * @param cp_mcsa_date
	 *            The date of creation/ modification of mcsa.
	 * @param cuser
	 *            The login id of the user.
	 * @param action
	 *            Type of action to be taken i.e. 'a' for addition and 'u' for
	 *            updation.
	 * @param ds
	 *            Object of DataSource.
	 * @return int return the result of stored procedure.
	 */
	public static int insertMcsa(String cp_mcsa_id, String cp_mcsa_data,
			String cp_mcsa_date, String cuser, String action, DataSource ds) {
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		McsaDetailBean mcsa = null;
		int sp_result = 0;

		try {
			conn = ds.getConnection();
			stmt = conn
					.prepareCall("{?=call cp_manage_mcsa( ? , ? , ? ,? , ? )}");
			stmt.registerOutParameter(1, java.sql.Types.INTEGER);
			stmt.setString(2, cp_mcsa_id);
			stmt.setBytes(3, cp_mcsa_data.getBytes());
			stmt.setString(4, cp_mcsa_date);
			stmt.setString(5, cuser);
			stmt.setString(6, action);

			stmt.execute();
			sp_result = stmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"insertMcsa(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("insertMcsa(String, String, String, String, String, DataSource) - Error occured insertion of new mcsa "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"insertMcsa(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"insertMcsa(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"insertMcsa(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return sp_result;
	}

	/**
	 * This method returns the details of Mcsa in a arraylist object containing
	 * the McsaDetailBean type of object which is used by MCSA_ViewAction class
	 * to get teh mcsa details
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList Contains the objects of McsaDetailBean.
	 */
	public static ArrayList getMcsa(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		McsaDetailBean mcsa = null;
		ArrayList mcsa_details = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "SELECT cp_mcsa_id, cp_mcsa_data, cp_mcsa_version, cp_mcsa_date= isnull(convert(varchar(10) ,cp_mcsa_date,101),''), cp_mcsa_created_by, cp_mcsa_create_date, cp_mcsa_changed_by,"
					+ "cp_mcsa_change_date FROM cp_mcsa ORDER BY cp_mcsa_id DESC";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				if (mcsa_details == null) {
					mcsa_details = new ArrayList();
				}
				mcsa = new McsaDetailBean();
				mcsa.setMcsa_id(rs.getString(1));
				mcsa.setMcsa_data(rs.getBytes(2));
				mcsa.setMcsa_version(rs.getString(3));
				mcsa.setMcsa_date(rs.getString(4));
				mcsa.setMcsa_created_by(rs.getString(5));
				mcsa.setMcsa_create_date(rs.getString(6));
				mcsa.setMcsa_changed_by(rs.getString(7));
				mcsa.setMcsa_change_date(rs.getString(8));
				mcsa_details.add(mcsa);
			}
		} catch (Exception e) {
			logger.error("getMcsa(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMcsa(DataSource) - Error occured during getting mcsa details from cp_msca  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("getMcsa(DataSource) - exception ignored", e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn("getMcsa(DataSource) - exception ignored", e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("getMcsa(DataSource) - exception ignored", e);
			}

		}
		return mcsa_details;
	}

	/**
	 * This method insert the details of Partner in the database by taking
	 * formbean parameter of Partner_EditForm formbean and use formbean method
	 * to send the parameter in the stored procedure
	 * 
	 * @param form
	 *            Object of Partner_editFrom form bean
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList Contains the objects of McsaDetailBean.
	 */
	public static int setPartnerDetail(Partner_EditBean form, String user_id,
			DataSource ds) {
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		int retvalue = -1;
		String temp = "";

		try {
			conn = ds.getConnection();
			stmt = conn
					.prepareCall("{?=call cp_partner_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			stmt.registerOutParameter(1, Types.INTEGER);

			stmt.setString(2, null);// cp_pd_id
			stmt.setInt(3, Integer.parseInt(form.getPid()));
			stmt.setString(4, form.getContactDate());
			stmt.setString(5, form.getTaxId());
			stmt.setString(6, form.getCompanyType());
			stmt.setString(7, form.getDateInc());

			for (int i = 0; i < form.getCmboxCoreCompetency().length; i++) {
				temp = temp + form.getCmboxCoreCompetency(i) + ",";
			}
			temp = temp.substring(0, temp.length() - 1);

			stmt.setString(8, temp);

			if (form.getAddress_id() == null || form.getAddress_id().equals("")) {
				form.setAddress_id("0");
			}
			stmt.setString(9, form.getAddress_id());
			stmt.setString(10, form.getAddress1());
			stmt.setString(11, form.getAddress2());
			stmt.setString(12, form.getCity());
			stmt.setString(13, form.getState());
			stmt.setString(14, form.getZip());
			stmt.setString(15, form.getCountry());
			stmt.setString(16, form.getMainPhone());
			stmt.setString(17, form.getMainFax());
			stmt.setString(18, form.getCompanyURL());

			String index1[] = form.getAfterHoursPhone();
			String combine1 = "";
			for (int i = 0; i < form.getAfterHoursPhone().length; i++) {
				combine1 = combine1 + index1[i] + ",";
			}

			/* added by hamid to remove last separator */
			if (combine1.length() > 0) {
				combine1 = combine1.substring(0, combine1.length() - 1);
			}
			/* added by hamid to remove last separator */
			stmt.setString(19, combine1);
			String index2[] = form.getAfterHoursEmail();
			String combine2 = "";
			for (int i = 0; i < form.getAfterHoursEmail().length; i++) {
				combine2 = combine2 + index2[i] + ",";
			}
			/* added by hamid to remove last separator */
			if (combine2.length() > 0) {
				combine2 = combine2.substring(0, combine2.length() - 1);
			}

			/* added by hamid to remove last separator */
			stmt.setString(20, combine2);

			ArrayList ph_email = new ArrayList();
			for (int i = 0; i < 4; i++) {
				ph_email.add(new Phone_Email(index1[i], index2[i]));
			}
			form.setPh_email(ph_email);

			stmt.setString(21, form.getRegID());
			stmt.setString(22, form.getVendorID());

			if (form.getBlousesQuantity() == null
					|| form.getBlousesQuantity().equals("")) {
				form.setBlousesQuantity("0");
			}
			stmt.setInt(23, Integer.parseInt(form.getBlousesQuantity()));
			stmt.setString(24, form.getCmboxPartnerLevel());
			stmt.setString(25, form.getProjectCode());
			stmt.setString(26, form.getCmboxStatus());
			stmt.setString(27, form.getRdoMCSA());
			stmt.setString(28, form.getRdoW9());
			stmt.setString(29, form.getRdoWorkmens());
			stmt.setString(30, form.getRdoInsurance());
			stmt.setString(31, form.getCmboxContractStatus());
			stmt.setString(32, form.getContractDate());
			stmt.setString(33, form.getContractRenewalDate());
			stmt.setString(34, form.getTruckLogoQuantity());
			stmt.setString(35, form.getBadgesQuantity());
			stmt.setString(36, form.getProgramPackage());

			String[] Faddress1 = form.getPhyFieldAddress1();
			String address1 = "";
			if (form.getPhyFieldAddress1() != null) {
				for (int i = 0; i < form.getPhyFieldAddress1().length; i++) {
					address1 = address1 + Faddress1[i] + ",";
				}
			}
			/* added by hamid to remove last separator */
			if (address1.length() > 0) {
				address1 = address1.substring(0, address1.length() - 1);
			}

			/* added by hamid to remove last separator */
			stmt.setString(37, address1);

			String[] Faddress2 = form.getPhyFieldAddress2();
			String address2 = "";
			if (form.getPhyFieldAddress2() != null) {
				for (int i = 0; i < form.getPhyFieldAddress2().length; i++) {
					address2 = address2 + Faddress2[i] + ",";
				}
			}
			/* added by hamid to remove last separator */
			if (address2.length() > 0) {
				address2 = address2.substring(0, address2.length() - 1);
			}

			/* added by hamid to remove last separator */
			stmt.setString(38, address2);

			String[] Fstate = form.getPhyFieldState();
			String state = "";
			if (form.getPhyFieldState() != null) {
				for (int i = 0; i < form.getPhyFieldState().length; i++) {
					state = state + Fstate[i] + ",";
				}
			}
			/* added by hamid to remove last separator */
			if (state.length() > 0) {
				state = state.substring(0, state.length() - 1);
			}

			/* added by hamid to remove last separator */
			stmt.setString(39, state);
			String[] Fzip = form.getPhyFieldZip();
			String zip = "";
			if (form.getPhyFieldZip() != null) {
				for (int i = 0; i < form.getPhyFieldZip().length; i++) {
					zip = zip + Fzip[i] + ",";
				}
			}
			/* added by hamid to remove last separator */
			if (zip.length() > 0) {
				zip = zip.substring(0, zip.length() - 1);
			}

			/* added by hamid to remove last separator */
			stmt.setString(40, zip);

			String[] Fphone = form.getPhyFieldPhone();
			String phone = "";
			if (form.getPhyFieldPhone() != null) {
				for (int i = 0; i < form.getPhyFieldPhone().length; i++) {
					phone = phone + Fphone[i] + ",";
				}
			}
			/* added by hamid to remove last separator */
			if (phone.length() > 0) {
				phone = phone.substring(0, phone.length() - 1);
			}

			/* added by hamid to remove last separator */
			stmt.setString(41, phone);

			String[] Ffax = form.getPhyFieldFax();
			String fax = "";
			if (form.getPhyFieldFax() != null) {
				for (int i = 0; i < form.getPhyFieldFax().length; i++) {
					fax = fax + Ffax[i] + ",";
				}
			}
			/* added by hamid to remove last separator */
			if (fax.length() > 0) {
				fax = fax.substring(0, fax.length() - 1);
			}

			/* added by hamid to remove last separator */
			stmt.setString(42, fax);

			ArrayList phyaddress = null;
			if (form.getPhyFieldFax() != null) {
				for (int i = 0; i < form.getPhyFieldFax().length; i++) {
					if (phyaddress == null) {
						phyaddress = new ArrayList();
					}
					phyaddress.add(new PVSPhyAddress(Faddress1[i],
							Faddress2[i], Fstate[i], Fzip[i], Fphone[i],
							Ffax[i]));
				}
			}
			form.setPhyAddress(phyaddress);

			stmt.setString(43, "a");
			stmt.setInt(44, 0);
			stmt.setString(45, "");
			stmt.setString(46, form.getPartnerCandidate());
			stmt.setString(47, form.getKeyWord());
			stmt.setString(48, form.getMcsaVersion());

			/* upload file start */
			InputStream in = form.getInputStream();

			byte[] buffer1 = new byte[form.getFileSize()];
			in.read(buffer1);

			stmt.setString(49, form.getFileName());
			stmt.setString(50, form.getDisplayName());
			stmt.setBytes(51, buffer1);

			stmt.setString(52, user_id);
			stmt.setInt(53, Integer.parseInt(form.getPri_id()));
			stmt.setInt(54, Integer.parseInt(form.getSec_id()));

			/* Start: Added By Atul 16/01/2007 */
			stmt.setString(55, form.getPrmEmail());
			stmt.setString(56, form.getPrmPhone());
			stmt.setString(57, form.getPrmMobilePhone());
			stmt.setString(58, form.getSecEmail());
			stmt.setString(59, form.getSecPhone());
			stmt.setString(60, form.getSecMobilePhone());
			if (form.getNegotiatedRate().equals("")) {
				stmt.setDouble(61, 0.0);
			} else {
				stmt.setDouble(61, Double.parseDouble(form.getNegotiatedRate()));
			}
			stmt.setString(62, form.getLat_degree());
			stmt.setString(63, form.getLat_min());
			stmt.setString(64, form.getLat_direction());
			stmt.setString(65, form.getLon_degree());
			stmt.setString(66, form.getLon_min());
			stmt.setString(67, form.getLon_direction());
			stmt.setString(68, form.getRegion());
			stmt.setString(69, form.getCategory());
			stmt.setString(70, form.getTimezone());
			stmt.setString(71, form.getLatlon_source());
			stmt.setString(72, form.getIncidentReport());

			stmt.execute();
			retvalue = stmt.getInt(1);
			form.setDisplayName("");

			/* for file upload start */
			ArrayList uploadedlist = new ArrayList();
			uploadedlist = Upload.getuploadedfiles("%", form.getPid(),
					"Partner", ds);
			if (uploadedlist.size() > 0) {
				form.setUploadedfileslist(uploadedlist);
			}

			/* for file upload end */
			String engName[] = form.getEngName();
			String cmboxResourceLevel[] = form.getCmboxResourceLevel();
			String engZip[] = form.getEngZip();
			String chkboxEquipped[] = form.getChkboxEquipped();
			String rdoUnion[] = form.getRdoUnion();
			String cmboxHighCriticality[] = form.getCmboxHighCriticality();
			String cmboxCertifications[] = form.getCmboxCertifications();
			// seema-19/12/2006
			String tech_id[] = form.getTech_id();
			String equip = "";
			// added by Seema

			String temp1 = "";
			for (int j = 0; j < cmboxCertifications.length; j++) {
				temp1 = temp1 + cmboxCertifications[j] + ",";
			}
			temp1 = temp1.replaceAll(",~,", "~");
			temp1 = temp1.substring(0, temp1.length() - 1);

			if (chkboxEquipped != null) {
				for (int i = 0; i < chkboxEquipped.length; i++) {
					if (chkboxEquipped[i].equals(",")) {
						equip = equip + "~";
					} else {
						equip = equip + chkboxEquipped[i] + ",";
					}

				}
			}

			equip = equip.replaceAll(",~", "~");
			equip = equip.substring(0, equip.length() - 1);

			if (engName != null) {
				chkboxEquipped = equip.split(",", engName.length);

			}

			String engname = "";
			String rdounion = "";
			String criticality = "";
			String resourcelevel = "";
			String resourcelevelName = "";
			String engzip = "";
			String techid = "";
			String equipped = "";

			int index = 0;

			if (form.getEngName() != null) {
				for (int i = 0; i < form.getEngName().length; i++) {
					if (i == (form.getEngName().length - 1)) {
						engname = engname + engName[i];
						rdounion = rdounion + rdoUnion[i];
						criticality = criticality + cmboxHighCriticality[i];
						index = cmboxResourceLevel[i].indexOf("|");
						resourcelevelName = resourcelevelName
								+ cmboxResourceLevel[i].substring(index + 1);
						resourcelevel = resourcelevel
								+ cmboxResourceLevel[i].substring(0, index);
						engzip = engzip + engZip[i];
						techid = techid + tech_id[i];
						equipped = equip;

					} else {

						engname = engname + engName[i] + "~";
						rdounion = rdounion + rdoUnion[i] + "~";
						criticality = criticality + cmboxHighCriticality[i]
								+ "~";
						index = cmboxResourceLevel[i].indexOf("|");
						resourcelevelName = resourcelevelName
								+ cmboxResourceLevel[i].substring(index + 1)
								+ "~";
						resourcelevel = resourcelevel
								+ cmboxResourceLevel[i].substring(0, index)
								+ "~";
						engzip = engzip + engZip[i] + "~";
						techid = techid + tech_id[i] + "~";
						equipped = equip;

					}
				}

				stmt = conn
						.prepareCall("{?=call cp_technician_manage_01(?,?,?,?,?,?,?,?,?,?)}");
				stmt.registerOutParameter(1, Types.INTEGER);

				stmt.setString(2, engname);
				stmt.setString(3, form.getPid());
				stmt.setString(4, rdounion);
				stmt.setString(5, criticality);
				stmt.setString(6, resourcelevel);
				stmt.setString(7, resourcelevelName);
				stmt.setString(8, engzip);
				stmt.setString(9, temp1);
				stmt.setString(10, equipped);
				stmt.setString(11, techid);

				stmt.execute();
				retvalue = stmt.getInt(1);

			}

			String[] chkBoxTools = form.getChkboxTools();
			String[] toolZips = form.getToolZips();

			stmt = conn.prepareCall("{?=call cp_toollist_delete_01(?)}");
			stmt.registerOutParameter(1, Types.INTEGER);
			stmt.setString(2, form.getPid());
			stmt.execute();
			retvalue = stmt.getInt(1);

			if (toolZips != null) {
				for (int i = 0, j = 0; i < toolZips.length; i++) {
					if (!(toolZips[i].trim().length() == 0)) {
						toolZips[j++] = toolZips[i];
					}
				}
			}

			String tools = "";
			String toolzip = "";
			if (form.getChkboxTools() != null) {
				for (int i = 0; i < form.getChkboxTools().length; i++) {
					if (i == (form.getChkboxTools().length - 1)) {
						tools = tools + chkBoxTools[i];
						toolzip = toolzip + toolZips[i];

					} else {
						tools = tools + chkBoxTools[i] + "~";
						toolzip = toolzip + toolZips[i] + "~";

					}

				}
				stmt = conn
						.prepareCall("{?=call cp_toollist_manage_01(?,?,?)}");
				stmt.registerOutParameter(1, Types.INTEGER);

				stmt.setString(2, form.getPid());
				stmt.setString(3, tools);
				stmt.setString(4, toolzip);

				stmt.execute();
				retvalue = stmt.getInt(1);
			}

			String[] chkzipECDF = form.getChkzipECDF();
			String[] zipCodeECDF = form.getZipCodeECDF();

			if (zipCodeECDF != null) {
				for (int i = 0, j = 0; i < zipCodeECDF.length; i++) {
					if (!(zipCodeECDF[i].trim().length() == 0)) {
						zipCodeECDF[j++] = zipCodeECDF[i];
					}
				}
			}

			stmt = conn.prepareCall("{?=call cp_depotfacility_delete_01(?)}");
			stmt.registerOutParameter(1, Types.INTEGER);
			stmt.setString(2, form.getPid());
			stmt.execute();
			retvalue = stmt.getInt(1);

			String zipecdf = "";
			String zipcodeecdf = "";
			if (form.getChkzipECDF() != null) {
				for (int i = 0; i < form.getChkzipECDF().length; i++) {
					if (i == (form.getChkzipECDF().length - 1)) {
						zipecdf = zipecdf + chkzipECDF[i];
						zipcodeecdf = zipcodeecdf + zipCodeECDF[i];

					} else {
						zipecdf = zipecdf + chkzipECDF[i] + "~";
						zipcodeecdf = zipcodeecdf + zipCodeECDF[i] + "~";

					}

				}
				stmt = conn
						.prepareCall("{?=call cp_depotfacility_manage_01(?,?,?,?,?)}");
				stmt.registerOutParameter(1, Types.INTEGER);

				stmt.setString(2, form.getPid());
				stmt.setString(3, zipecdf);
				stmt.setString(4, zipcodeecdf);
				stmt.setString(5, user_id);
				stmt.setString(6, "");

				stmt.execute();
				retvalue = stmt.getInt(1);
			}

			String[] carrier = form.getCarrier();
			String[] policyNumber = form.getPolicyNumber();
			String[] amountGeneral = form.getAmountGeneral();
			String[] amountAutomobile = form.getAmountAutomobile();
			String[] amountUmbrella = form.getAmountUmbrella();

			stmt = conn
					.prepareCall("{?=call cp_InsuranceCertificate_delete_01(?)}");
			stmt.registerOutParameter(1, Types.INTEGER);
			stmt.setString(2, form.getPid());
			stmt.execute();
			retvalue = stmt.getInt(1);

			String carriers = "";
			String policynumbers = "";
			String amtgeneral = "";
			String amtautomobile = "";
			String amtumbrella = "";
			if (form.getCarrier() != null) {
				for (int i = 0; i < form.getCarrier().length; i++) {
					if (i == (form.getCarrier().length - 1)) {
						carriers = carriers + carrier[i];
						policynumbers = policynumbers + policyNumber[i];
						amtgeneral = amtgeneral + amountGeneral[i];
						amtautomobile = amtautomobile + amountAutomobile[i];
						amtumbrella = amtumbrella + amountUmbrella[i];

					} else {
						carriers = carriers + carrier[i] + "~";
						policynumbers = policynumbers + policyNumber[i] + "~";
						amtgeneral = amtgeneral + amountGeneral[i] + "~";
						amtautomobile = amtautomobile + amountAutomobile[i]
								+ "~";
						amtumbrella = amtumbrella + amountUmbrella[i] + "~";

					}
				}
				stmt = conn
						.prepareCall("{?=call cp_InsuranceCertificate_manage_01(?,?,?,?,?,?)}");
				stmt.registerOutParameter(1, Types.INTEGER);

				stmt.setString(2, form.getPid());
				stmt.setString(3, carriers);
				stmt.setString(4, policynumbers);
				stmt.setString(5, amtgeneral);
				stmt.setString(6, amtautomobile);
				stmt.setString(7, amtumbrella);

				stmt.execute();
				retvalue = stmt.getInt(1);
			}

			String[] stateR1 = form.getStateR1();
			String[] taxR1 = form.getTaxR1();

			if (taxR1 != null) {
				for (int i = 0, j = 0; i < taxR1.length; i++) {
					if (!(taxR1[i].trim().length() == 0)) {
						taxR1[j++] = taxR1[i];
					}
				}
			}

			stmt = conn.prepareCall("{?=call cp_taxexempt_delete_01(?)}");
			stmt.registerOutParameter(1, Types.INTEGER);
			stmt.setString(2, form.getPid());
			stmt.execute();
			retvalue = stmt.getInt(1);

			String stater1 = "";
			String taxr1 = "";
			if (form.getStateR1() != null) {
				for (int i = 0; i < form.getStateR1().length; i++) {
					if (i == (form.getStateR1().length - 1)) {
						stater1 = stater1 + stateR1[i];
						taxr1 = taxr1 + taxR1[i];

					} else {
						stater1 = stater1 + stateR1[i] + ",";
						taxr1 = taxr1 + taxR1[i] + ",";

					}

				}
				stmt = conn
						.prepareCall("{?=call cp_taxexempt_manage_01(?,?,?)}");
				stmt.registerOutParameter(1, Types.INTEGER);
				stmt.setString(2, form.getPid());
				stmt.setString(3, stater1);
				stmt.setString(4, taxr1);

				stmt.execute();
				retvalue = stmt.getInt(1);
			}

			stmt = conn
					.prepareCall("{?=call cp_companycertfication_delete_01(?)}");
			stmt.registerOutParameter(1, Types.INTEGER);
			stmt.setString(2, form.getPid());
			stmt.execute();
			retvalue = stmt.getInt(1);

			String[] wlcompany = form.getWlcompany();
			String[] validFromWC = form.getValidFromWC();
			String[] validToWC = form.getValidToWC();

			String wlcomp = "";
			String validfwc = "";
			String validtwc = "";
			if (form.getWlcompany() != null) {
				for (int i = 0; i < form.getWlcompany().length; i++) {
					if (i == (form.getWlcompany().length - 1)) {
						wlcomp = wlcomp + wlcompany[i];
						validfwc = validfwc + validFromWC[i];
						validtwc = validtwc + validToWC[i];

					} else {
						wlcomp = wlcomp + wlcompany[i] + ",";
						validfwc = validfwc + validFromWC[i] + ",";
						validtwc = validtwc + validToWC[i] + ",";

					}

				}
				stmt = conn
						.prepareCall("{?=call cp_companycertfication_manage_01(?,?,?,?,?)}");
				stmt.registerOutParameter(1, Types.INTEGER);

				stmt.setString(2, form.getPid());
				stmt.setString(3, "2");
				stmt.setString(4, wlcomp);
				stmt.setString(5, validfwc);
				stmt.setString(6, validtwc);

				stmt.execute();
				retvalue = stmt.getInt(1);
			}

			String[] sdcompany = form.getSdcompany();
			String[] validFromSD = form.getValidFromSD();
			String[] validToSD = form.getValidToSD();

			String sdcomp = "";
			String validfsd = "";
			String validtsd = "";
			if (form.getSdcompany() != null) {
				for (int i = 0; i < form.getSdcompany().length; i++) {
					if (i == (form.getSdcompany().length - 1)) {
						sdcomp = sdcomp + sdcompany[i];
						validfsd = validfsd + validFromSD[i];
						validtsd = validtsd + validToSD[i];

					} else {
						sdcomp = sdcomp + sdcompany[i] + ",";
						validfsd = validfsd + validFromSD[i] + ",";
						validtsd = validtsd + validToSD[i] + ",";

					}

				}
				stmt = conn
						.prepareCall("{?=call cp_companycertfication_manage_01(?,?,?,?,?)}");
				stmt.registerOutParameter(1, Types.INTEGER);

				stmt.setString(2, form.getPid());
				stmt.setString(3, "1");
				stmt.setString(4, sdcomp);
				stmt.setString(5, validfsd);
				stmt.setString(6, validtsd);

				stmt.execute();
				retvalue = stmt.getInt(1);
			}

			String[] itcompany = form.getItcompany();
			String[] validFromIT = form.getValidFromIT();
			String[] validToIT = form.getValidToIT();

			String itcomp = "";
			String validfit = "";
			String validtit = "";
			if (form.getItcompany() != null) {
				for (int i = 0; i < form.getItcompany().length; i++) {
					if (i == (form.getItcompany().length - 1)) {
						itcomp = itcomp + itcompany[i];
						validfit = validfit + validFromIT[i];
						validtit = validtit + validToIT[i];

					} else {
						itcomp = itcomp + itcompany[i] + ",";
						validfit = validfit + validFromIT[i] + ",";
						validtit = validtit + validToIT[i] + ",";

					}
				}
				stmt = conn
						.prepareCall("{?=call cp_companycertfication_manage_01(?,?,?,?,?)}");
				stmt.registerOutParameter(1, Types.INTEGER);

				stmt.setString(2, form.getPid());
				stmt.setString(3, "8");
				stmt.setString(4, itcomp);
				stmt.setString(5, validfit);
				stmt.setString(6, validtit);
				stmt.execute();
				retvalue = stmt.getInt(1);
			}

			String[] tacompany = form.getTacompany();
			String[] validFromTA = form.getValidFromTA();
			String[] validToTA = form.getValidToTA();
			String tacomp = "";
			String validfta = "";
			String validtta = "";
			if (form.getTacompany() != null) {
				for (int i = 0; i < form.getTacompany().length; i++) {
					if (i == (form.getTacompany().length - 1)) {
						tacomp = tacomp + tacompany[i];
						validfta = validfta + validFromTA[i];
						validtta = validtta + validToTA[i];

					} else {
						tacomp = tacomp + tacompany[i] + ",";
						validfta = validfta + validFromTA[i] + ",";
						validtta = validtta + validToTA[i] + ",";

					}

				}
				stmt = conn
						.prepareCall("{?=call cp_companycertfication_manage_01(?,?,?,?,?)}");
				stmt.registerOutParameter(1, Types.INTEGER);

				stmt.setString(2, form.getPid());
				stmt.setString(3, "9");
				stmt.setString(4, tacomp);
				stmt.setString(5, validfta);
				stmt.setString(6, validtta);
				stmt.execute();
				retvalue = stmt.getInt(1);
			}
		} catch (Exception e) {
			logger.error(
					"setPartnerDetail(Partner_EditForm, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setPartnerDetail(Partner_EditForm, String, DataSource) - Error occured during setting Partner details in setPartnerDetail() :"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setPartnerDetail(Partner_EditForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setPartnerDetail(Partner_EditForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setPartnerDetail(Partner_EditForm, String, DataSource) - exception ignored",
						e);
			}

		}
		return retvalue;

	}

	/**
	 * This method retrieve the details of Partner in the database by taking
	 * formbean parameter of Partner_EditForm formbean and use formbean setter
	 * methods to set the value retrieved by Sql queries
	 * 
	 * @param form
	 *            Object of Partner_editFrom form bean
	 * @param ds
	 *            Object of DataSource
	 */
	public static int getPartnerDetail(Partner_EditBean form, int roleCount,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int retvalue = 0;

		String sql = "select * from dbo.func_cp_partner_info_general_tab("
				+ form.getPid() + ")";
		String temp = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				retvalue = 1;

				form.setSpeedPaymentTerms(rs
						.getBoolean("cp_pt_speedpay_payment_terms"));

				form.setPid(rs.getString("cp_pd_partner_id"));
				form.setTaxId(rs.getString("cp_pd_tax_id"));
				form.setDateInc(rs.getString("cp_pd_date_of_incorporation"));
				form.setCompanyType(rs.getString("cp_pd_type"));

				form.setCmboxCoreCompetency(rs.getString(
						"cp_pd_core_competency").split(","));
				form.setPrimaryFirstName(rs.getString("pri_pc_first_name"));
				form.setPrimaryLastName(rs.getString("pri_pc_last_name"));
				form.setPrmEmail(rs.getString("pri_pc_email"));
				form.setPrmPhone(rs.getString("pri_pc_phone"));
				form.setPrmMobilePhone(rs.getString("pri_pc_cell_phone"));
				form.setSecondaryFirstName(rs.getString("sec_pc_first_name"));
				form.setSecondaryLastName(rs.getString("sec_pc_last_name"));
				form.setSecPhone(rs.getString("sec_pc_phone"));
				form.setSecEmail(rs.getString("sec_pc_email"));
				form.setSecMobilePhone(rs.getString("sec_pc_cell_phone"));
				form.setPri_id(rs.getString("cp_pd_pri"));
				form.setSec_id(rs.getString("cp_pd_sec"));
				form.setOrgid(rs.getString("lo_om_id"));
				form.setOrgTopId(rs.getString("lo_om_ot_id"));
				form.setAddress1(rs.getString("lo_ad_address1"));
				form.setAddress2(rs.getString("lo_ad_address2"));
				form.setCity(rs.getString("lo_ad_city"));
				form.setCountry(rs.getString("lo_ad_country"));
				form.setState(rs.getString("lo_ad_state"));

				form.setZip(rs.getString("lo_ad_zip_code"));

				form.setLat_degree(rs.getString("lo_ad_lat_deg"));
				form.setLat_min(rs.getString("lo_ad_lat_min"));
				form.setLat_direction(rs.getString("lo_ad_lat_direction"));
				form.setLon_degree(rs.getString("lo_ad_lon_deg"));
				form.setLon_min(rs.getString("lo_ad_lon_min"));
				form.setLon_direction(rs.getString("lo_ad_lon_direction"));

				form.setId_address(rs.getString("lo_ad_id"));
				form.setAddress_id(rs.getString("lo_ad_id"));

				form.setWebUserName(rs.getString("pri_username"));
				form.setWebPassword(rs.getString("pri_password"));

				form.setIncidentReport(rs.getString("cp_pd_incident_type"));

				form.setMainFax(rs.getString("lo_om_fax"));
				/* Added by Vishal Kapoor */
				form.setMainPhone(rs.getString("lo_om_phone1"));
				/* End */
				form.setCompanyURL(rs.getString("lo_om_website"));
				// added for leadminuteman and external sales agent

				// The below two properties for thumbsup & thumbsdown count;
				int thumpsupCount = Integer.valueOf(rs
						.getString("thumbsup_count"));
				int thumpsdownCount = Integer.valueOf(rs
						.getString("thumbsdown_count"));
				form.setThumbsUpCount(thumpsupCount == 0 ? "" : String
						.valueOf(thumpsupCount));
				form.setThumbsDownCount(thumpsdownCount == 0 ? "" : String
						.valueOf(thumpsdownCount));
				form.setOnTimePercentage(rs.getString("on_time_percentage"));

				form.setExternalSalesAgent(rs
						.getBoolean("cp_pt_external_sales_agent"));
				form.setLeadMinuteman(rs.getBoolean("cp_pt_lead_minuteman"));

				String split1 = rs.getString("cp_pd_after_hr_phone");
				String[] split1arr = split1.split(",", 5);

				String[] arrAfterHrPhone = new String[4];
				for (int i = 0; i < split1arr.length; i++) {
					if (i < 4) {
						arrAfterHrPhone[i] = split1arr[i];
					}
				}

				if (split1arr.length < 4) {
					for (int i = split1arr.length; i < 4; i++) {
						arrAfterHrPhone[i] = "";
					}
				}
				form.setAfterHoursPhone(arrAfterHrPhone);

				String split2 = rs.getString("cp_pd_after_hr_email");
				String[] split2arr = split2.split(",", 5);

				String[] arrAfterHrEmail = new String[4];
				for (int i = 0; i < split2arr.length; i++) {
					if (i < 4) {
						arrAfterHrEmail[i] = split2arr[i];
					}
				}

				if (split2arr.length < 4) {
					for (int i = split2arr.length; i < 4; i++) {
						arrAfterHrEmail[i] = "";
					}
				}
				form.setAfterHoursEmail(arrAfterHrEmail);

				ArrayList ph_email = new ArrayList();
				for (int i = 0; i < 4; i++) {
					ph_email.add(new Phone_Email(arrAfterHrPhone[i],
							arrAfterHrEmail[i]));
				}
				form.setPh_email(ph_email);

				form.setKeyWord(rs.getString("cp_pd_keyword"));
				form.setCmboxStatus(rs.getString("cp_partner_status"));
				form.setPartnerCreateDate(rs.getString("date_added"));
				form.setPartnerSource(rs.getString("source"));
				// new
				form.setPartnerUpdateDate(rs.getString("ilex_update"));
				form.setPartnerUpdateBy(rs.getString("ilex_update_by"));
				form.setPartnerWebUpdateDate(rs.getString("web_update"));
				// over
				form.setRegistered(rs.getString("registered"));
				form.setRegDate(rs.getString("reg_date"));
				form.setMcsaVersion(rs.getString("cp_pd_mcsaversion"));
				form.setSignedBy(rs.getString("signed_by"));
				form.setRegistrationRenewalDate(rs
						.getString("reg_renewal_date"));
				form.setIncidentReport(rs.getString("cp_pd_incident_type"));
				form.setIncidentReportFiled(rs
						.getString("incident_report_filed"));
				form.setRdoW9(rs.getString("cp_pd_w9w9"));
				form.setW9Uploaded(rs.getString("w9_uploaded"));
				// Changes
				form.setIsInsuranceSubmitted(rs.getString("cp_pd_insurance"));
				form.setIsInsuranceUploaded(rs
						.getString("cp_pd_insurance_uploaded"));

				form.setAcceptAddress(rs.getString("lo_ad_accept_address"));
				form.setMonthlyNewsletter(rs
						.getString("cp_pd_send_newsletter_monthly"));
				form.setAdpo(rs.getString("cp_pd_adpo"));
				form.setIsResumeUploaded(rs.getString("cp_pd_resume_uploaded"));
				form.setIsResumeSubmitted(rs.getString("cp_pd_resume"));
				form.setBackgroundCheckSubmitted(rs
						.getString("cp_backcheck_status"));
				form.setBackgroundCheckUploaded(rs
						.getString("cp_backcheck_status_uploaded"));
				form.setDrugScreenCompletionSubmitted(rs
						.getString("cp_DrugScreen_status"));
				form.setDrugScreenCompletionUploaded(rs
						.getString("cp_DrugScreen_uploaded"));

				form.setGpVendorId(rs.getString("cp_partner_gpvendor_id"));
				form.setVendorSiteCode(rs.getString("cp_pt_vendor_site_code"));
				form.setComcastVendorID(rs.getString("cp_pt_comcast_vendor_id"));
				/* for file upload start */
				ArrayList uploadedlist = new ArrayList();
				uploadedlist = Upload.getuploadedfiles("%", form.getPid(),
						"Partner", ds);

				if (roleCount == 0) {
					for (int i = 0; i < uploadedlist.size(); i++) {
						Fileinfo a = (Fileinfo) uploadedlist.get(i);
						if (a.getFile_remarks().contains("W-9")) {
							uploadedlist.remove(a);
						}
					}
				}

				if (uploadedlist.size() > 0) {
					form.setUploadedfileslist(uploadedlist);
				}

				/* for file upload end */
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				stmt = conn.createStatement();

				sql = "SELECT lo_ad_id, lo_ad_address1, lo_ad_address2, lo_ad_city, lo_ad_state, lo_ad_zip_code, lo_ad_country,lo_ad_lat_deg,lo_ad_lat_min,lo_ad_lat_direction,lo_ad_lon_deg,lo_ad_lon_min,lo_ad_lon_direction,lo_ad_region_id,lo_ad_category_id, lo_ad_tzone FROM lo_address WHERE (lo_ad_id ="
						+ form.getId_address() + ") ";
				rs = stmt.executeQuery(sql);
				if (rs.next()) {
					form.setAddress1(rs.getString("lo_ad_address1"));
					form.setAddress2(rs.getString("lo_ad_address2"));
					form.setCity(rs.getString("lo_ad_city"));
					form.setState(rs.getString("lo_ad_state"));
					form.setZip(rs.getString("lo_ad_zip_code"));
					form.setCountry(rs.getString("lo_ad_country"));
					form.setLat_degree(rs.getString("lo_ad_lat_deg"));

				}
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}

				/* Ended By Vishal Kapoor */
				stmt = conn.createStatement();

				sql = "SELECT cp_pl_partner_id, cp_pl_add1, cp_pl_add2, cp_pl_state, cp_pl_zip_code, cp_pl_phone, cp_pl_fax "
						+ "FROM cp_partner_location_info WHERE     (cp_pl_partner_id ="
						+ form.getPid() + ") ";

				rs = stmt.executeQuery(sql);

				ArrayList phyaddress = null;
				while (rs.next()) {
					if (phyaddress == null) {
						phyaddress = new ArrayList();
					}
					phyaddress.add(new PVSPhyAddress(
							rs.getString("cp_pl_add1"), rs
									.getString("cp_pl_add2"), rs
									.getString("cp_pl_state"), rs
									.getString("cp_pl_zip_code"), rs
									.getString("cp_pl_phone"), rs
									.getString("cp_pl_fax")));

				}
				form.setPhyAddress(phyaddress);
				/* by hamid to reduce active count start */
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				/* by hamid to reduce active count end */

				stmt = conn.createStatement();
				sql = "select cp_tool_id,cp_zip_code from cp_partner_tool_list where( cp_partner_id="
						+ form.getPid() + " ) ";
				rs = stmt.executeQuery(sql);
				String tool_id = "";
				while (rs.next()) {
					tool_id = tool_id + rs.getString("cp_tool_id") + ",";
				}
				form.setChkboxTools(tool_id.split(","));

				/* by hamid to reduce active count start */
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				/* by hamid to reduce active count end */

				stmt = conn.createStatement();
				sql = " select cp_ecdf_se_id from dbo.cp_ecdf where ( cp_ecdf_partner_id="
						+ form.getPid() + " ) ";
				rs = stmt.executeQuery(sql);
				tool_id = "";
				while (rs.next()) {
					tool_id = tool_id + rs.getString("cp_ecdf_se_id") + ",";
				}
				form.setChkzipECDF(tool_id.split(","));

				/* by hamid to reduce active count start */
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				/* by hamid to reduce active count end */

				stmt = conn.createStatement();
				sql = " SELECT cp_pt_state_id, cp_pt_tax_id"
						+ " FROM         cp_partner_tax_exempt"
						+ " WHERE     (cp_pt_partner_id =" + form.getPid()
						+ " ) ";
				rs = stmt.executeQuery(sql);
				String tax_exempt = "";
				while (rs.next()) {
					tax_exempt = tax_exempt + rs.getString("cp_pt_state_id")
							+ ",";
				}
				form.setStateR1(tax_exempt.split(","));

			} else {

				form.setRdoW9("N");
				ArrayList ph_email = new ArrayList();
				ph_email.add(new Phone_Email("", ""));
				ph_email.add(new Phone_Email("", ""));
				ph_email.add(new Phone_Email("", ""));
				ph_email.add(new Phone_Email("", ""));
				form.setPh_email(ph_email);
				form.setCountry("US");

				sql = "select cp_state_id,cp_country_id from cp_state where cp_state_id ='"
						+ form.getState() + "'";
				rs = stmt.executeQuery(sql);

				if (rs.next()) {
					form.setState(rs.getString("cp_state_id"));
					form.setCountry(rs.getString("cp_country_id"));
				} else {
					form.setState("0");
					form.setCountry("0");
				}
			}
		} catch (Exception e) {
			logger.error("getPartnerDetail(Partner_EditForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerDetail(Partner_EditForm, DataSource) - Error occured during getting Partner details in getPartnerDetail() :"
						+ e);
			}
			logger.error("getPartnerDetail(Partner_EditForm, DataSource)", e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartnerDetail(Partner_EditForm, DataSource) - exception ignored",
						e);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartnerDetail(Partner_EditForm, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartnerDetail(Partner_EditForm, DataSource) - exception ignored",
						e);
			}

		}
		return retvalue;
	}

	/**
	 * This method retrieve list of email ids of Partners which can receive a
	 * particular document
	 * 
	 * @param notificationid
	 *            Notification id
	 * @param ds
	 *            Object of DataSource
	 * @return String This method returns String,Email Id list.
	 */
	public static String getEmailnotificationlist(int notificationid,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String emaillist = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select cc_notification_emails from cc_notification where cc_notification_id="
					+ notificationid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {

				emaillist = rs.getString("cc_notification_emails");

			}

		} catch (Exception e) {
			logger.error("getEmailnotificationlist(int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEmailnotificationlist(int, DataSource) - Error occured during getting getEmailnotificationlist  ");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getEmailnotificationlist(int, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getEmailnotificationlist(int, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getEmailnotificationlist(int, DataSource) - exception ignored",
						e);
			}

		}
		return emaillist;
	}

	/**
	 * This method updates/modifies email list.
	 * 
	 * @param cc_notification_id
	 *            int.notification id
	 * @param cc_notification_email
	 *            String.email id list
	 * @param userid
	 *            String.user id
	 * @param ds
	 *            Object of DataSource
	 * @return int This method returns int,returns 0 if success otherwise -ve
	 *         values.
	 */
	public static int updateEmaillist(int cc_notification_id,
			String cc_notification_email, String userid, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		int retval = 0;
		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call cc_change_notification_manage_01(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, cc_notification_id);
			cstmt.setString(3, cc_notification_email);
			cstmt.setString(4, userid);

			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("updateEmaillist(int, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateEmaillist(int, String, String, DataSource) - Error occured during updating email list"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"updateEmaillist(int, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"updateEmaillist(int, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"updateEmaillist(int, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retval;

	}

	/**
	 * This method get the password for verification of the old password entered
	 * by the user
	 * 
	 * @param id
	 *            id of the user
	 * @param ds
	 *            Object of DataSource
	 * @return String value of the exiting password
	 */
	public static String getOldPassword(String id, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String password = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "SELECT lo_us_pass FROM lo_user WHERE lo_us_pc_id ='"
					+ id + "'";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				password = rs.getString("lo_us_pass");
			}

		} catch (Exception e) {
			logger.error("getOldPassword(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOldPassword(String, DataSource) - Error occured in getting password in getOldPassword  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getOldPassword(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getOldPassword(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getOldPassword(String, DataSource) - exception ignored",
						e);
			}

		}
		return password;
	}

	/**
	 * This method set the password on the basis of the id and value of the new
	 * password provided as the parameter
	 * 
	 * @param id
	 *            the unique id of the user
	 * @param newpassword
	 *            the value of new password
	 * @param ds
	 *            Object of DataSource
	 * @return int if successful return 1 else 0
	 */
	public static int setPassword(String id, String newpassword, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;

		int retvalue = 1;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "update lo_user set lo_us_pass='" + newpassword
					+ "' where lo_us_pc_id =" + id;
			stmt.execute(sql);

		} catch (Exception e) {
			logger.error("setPassword(String, String, DataSource)", e);

			retvalue = 0;
			if (logger.isDebugEnabled()) {
				logger.debug("setPassword(String, String, DataSource) - Error occured in setOldPassword()  "
						+ e);
			}
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setPassword(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setPassword(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retvalue;
	}

	/**
	 * This method retrieves the details of the technician of a partner and
	 * create a arraylist object of all the technician and return the array list
	 * 
	 * @param id
	 *            the partner id
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList a collection object containing the details of
	 *         technician
	 */
	public static ArrayList getPartner_technicianInfo(String id, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		Statement stmt2 = null;
		Statement stmt3 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		ArrayList tech_list = new ArrayList();

		int j = 0;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "SELECT  cp_tech_id, cp_tech_name, cp_tech_res_level, cp_tech_res_level_name, cp_tech_zip_code, cp_tech_union, cp_tech_highest_critical, "
					+ "cp_tech_certi_skill,(select cc_cd_name  from cc_criticality_detail where cc_cd_id= cp_tech_highest_critical) as cp_tech_highest_critical_name"
					+ "	FROM    cp_technicians"
					+ "	WHERE   (cp_tech_partner_id = '" + id + "')";

			rs1 = stmt.executeQuery(sql);

			while (rs1.next()) {
				stmt2 = conn.createStatement();

				String sql2 = "select * from dbo.func_cp_tech_equipment_list("
						+ rs1.getString("cp_tech_id") + ")";

				rs2 = stmt2.executeQuery(sql2);
				String arr_equip = "";
				String arr_security_certification = "";
				while (rs2.next()) {
					arr_equip = arr_equip + rs2.getString("cc_ed_name") + ",";

				}

				/* Changes done for Security Certification related. */
				stmt3 = conn.createStatement();
				String sql3 = "select cp_sc_drug_screening_cert,"
						+ " cp_sc_criminal_bgd_cert,"
						+ " cp_sc_harassment_cert,"
						+ " cp_sc_drug_screening_cert_date,"
						+ " cp_sc_criminal_bgd_cert_date,"
						+ " cp_sc_harassment_cert_date, "
						+ " drug_screening_cert_file_id, "
						+ " criminal_bgd_cert_file_id "
						+ " from cp_partners_technicians_vw_01A where tech_id = "
						+ rs1.getString("cp_tech_id");
				rs3 = stmt3.executeQuery(sql3);
				if (rs3.next()) {
					if (tech_list == null) {
						tech_list = new ArrayList();
					}
				}

				tech_list.add(new PVS_Partner_Edit_TechnicianInfo(rs1
						.getString("cp_tech_id"),
						rs1.getString("cp_tech_name"), rs1
								.getString("cp_tech_res_level")
								+ "|"
								+ rs1.getString("cp_tech_res_level_name"), rs1
								.getString("cp_tech_res_level_name"), rs1
								.getString("cp_tech_zip_code"), arr_equip
								.split(","), rs1.getString("cp_tech_union")
								.trim(), rs1
								.getString("cp_tech_highest_critical"), (rs1
								.getString("cp_tech_certi_skill")).split(","),
						rs1.getString("cp_tech_certi_skill"), rs1
								.getString("cp_tech_highest_critical_name"),
						rs1.getString("cp_tech_res_level_name"), rs3
								.getString(1), rs3.getString(2), rs3
								.getString(3), rs3.getString(4), rs3
								.getString(5), rs3.getString(6), rs3
								.getString(7), rs3.getString(8)));
			}
		} catch (Exception e) {
			logger.error("getPartner_technicianInfo(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartner_technicianInfo(String, DataSource) - Error occured in getPartner_technicianInfo()  "
						+ e);
			}
		} finally {
			DBUtil.close(rs3, stmt3);
			try {
				if (rs1 != null) {
					rs1.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartner_technicianInfo(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (rs2 != null) {
					rs2.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartner_technicianInfo(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartner_technicianInfo(String, DataSource) - exception ignored",
						e);
			}
			try {
				if (stmt2 != null) {
					stmt2.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartner_technicianInfo(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartner_technicianInfo(String, DataSource) - exception ignored",
						e);
			}

		}
		return tech_list;
	}

	/**
	 * This method gets theInsurance Certificate from the database and return to
	 * the caller as an object of ArrayList String pid Partner id
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This object will contain the description of Depot
	 *         Facilities
	 */
	public static ArrayList getInsuranceCertificate(String pid, DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("SELECT cp_ic_carrier, cp_ic_policy_no, cp_ic_amount_general, cp_ic_amount_automobile, cp_ic_amount_umbrella"
							+ " FROM         cp_insurance_certificate "
							+ " WHERE     (cp_ic_partner_id = " + pid + ")");

			while (rs.next()) {
				list.add(new PVS_Insurance_Certificate(rs
						.getString("cp_ic_carrier"), rs
						.getString("cp_ic_policy_no"), rs
						.getString("cp_ic_amount_general"), rs
						.getString("cp_ic_amount_automobile"), rs
						.getString("cp_ic_amount_umbrella")));
			}
		} catch (Exception e) {
			logger.error("getInsuranceCertificate(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getInsuranceCertificate(String, DataSource) - Exception caught in Pvsdao.getInsuranceCertificate()  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getInsuranceCertificate(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getInsuranceCertificate(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getInsuranceCertificate(String, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	/**
	 * This method gets the Details of company certification from the the
	 * database on the basis of following parameters and return to the caller as
	 * an object of ArrayList containing the details of certification String pid
	 * Partner id. String cp_type_id Company type id.
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This object will contain the description of Depot
	 *         Facilities
	 */
	public static ArrayList getCompanyCertificationDetails(String pid,
			String cp_type_id, DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("SELECT cp_cd_comp_id, cp_ccert_valid_from, cp_ccert_valid_to,"
							+ "(select cp_comp_name from cp_company where cp_comp_id in  (cp_cd_comp_id)) as company_name"
							+ " FROM cp_companycert_details"
							+ " WHERE (cp_cd_partner_id = "
							+ pid
							+ ") AND (cp_cd_corecomp_id = " + cp_type_id + ")");

			while (rs.next()) {
				if (Integer.parseInt(cp_type_id) == 2) {
					list.add(new PVS_CompanyCert_Wireless(rs
							.getString("cp_cd_comp_id"), rs
							.getString("cp_ccert_valid_from"), rs
							.getString("cp_ccert_valid_to"), rs
							.getString("company_name")));
				}
				if (Integer.parseInt(cp_type_id) == 1) {
					list.add(new PVS_CompanyCert_StructuredDistribution(rs
							.getString("cp_cd_comp_id"), rs
							.getString("cp_ccert_valid_from"), rs
							.getString("cp_ccert_valid_to"), rs
							.getString("company_name")));
				}
				if (Integer.parseInt(cp_type_id) == 8) {
					list.add(new PVS_CompanyCert_IntegratedTechnologyCertified(
							rs.getString("cp_cd_comp_id"), rs
									.getString("cp_ccert_valid_from"), rs
									.getString("cp_ccert_valid_to"), rs
									.getString("company_name")));
				}
				if (Integer.parseInt(cp_type_id) == 9) {
					list.add(new PVS_CompanyCert_IndustryTechnicalAffiliations(
							rs.getString("cp_cd_comp_id"), rs
									.getString("cp_ccert_valid_from"), rs
									.getString("cp_ccert_valid_to"), rs
									.getString("company_name")));
				}

			}
		} catch (Exception e) {
			logger.error(
					"getCompanyCertificationDetails(String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCompanyCertificationDetails(String, String, DataSource) - Exception caught in Pvsdao.getInsuranceCertificate()  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getCompanyCertificationDetails(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCompanyCertificationDetails(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCompanyCertificationDetails(String, String, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	public static ArrayList getPocsearchresult(String pocname, String division,
			DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList searchList = new ArrayList();
		EmailSearchList contactlist = null;

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call cc_poc_contact_search( ? , ? )}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, '%' + pocname + '%');
			cstmt.setString(3, '%' + division + '%');

			rs = cstmt.executeQuery();

			while (rs.next()) {
				contactlist = new EmailSearchList();
				contactlist.setPocname(rs.getString("lo_poc_name"));
				contactlist.setPocid(rs.getString("lo_pc_id"));
				contactlist.setDivison(rs.getString("lo_om_division"));
				contactlist.setEmailid(rs.getString("lo_pc_email1"));

				searchList.add(contactlist);
			}

		} catch (Exception e) {
			logger.error("getPocsearchresult(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocsearchresult(String, String, DataSource) - Error occured during getting getPocsearchresult  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPocsearchresult(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPocsearchresult(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPocsearchresult(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return searchList;
	}

	public static ArrayList getCertificationlist(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_cert_id,cp_cert_comp_tech  from cp_certification order by cp_cert_comp_tech ");
			while (rs.next()) {
				list.add(new LabelValue(rs.getString("cp_cert_comp_tech"), rs
						.getString("cp_cert_comp_tech")));
			}
		} catch (Exception e) {
			logger.error("getCertificationlist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCertificationlist(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCertificationlist(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCertificationlist(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCertificationlist(DataSource) - exception ignored",
						sql);
			}

		}

		return list;
	}

	public static int deletePartnerDetail(String partnerid, DataSource ds) {
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		int retvalue = -1;

		try {
			conn = ds.getConnection();
			stmt = conn.prepareCall("{?=call cp_partner_delete_01(?)}");
			stmt.registerOutParameter(1, Types.INTEGER);

			stmt.setInt(2, Integer.parseInt(partnerid));
			stmt.execute();
			retvalue = stmt.getInt(1);

		} catch (Exception e) {
			logger.error("deletePartnerDetail(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deletePartnerDetail(String, DataSource) - Error occured during deleting Partner details in deletePartnerDetail() :"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deletePartnerDetail(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deletePartnerDetail(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deletePartnerDetail(String, DataSource) - exception ignored",
						e);
			}

		}
		return retvalue;

	}

	public static ArrayList getPartnerstatuslist(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_ps_status_shortname,cp_ps_status from cp_Partner_status order by cp_ps_status");
			while (rs.next()) {
				list.add(new LabelValue(rs.getString("cp_ps_status"), rs
						.getString("cp_ps_status_shortname")));
			}
		} catch (Exception e) {
			logger.error("getPartnerstatuslist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerstatuslist(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getPartnerstatuslist(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPartnerstatuslist(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPartnerstatuslist(DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	/**
	 * This will get MCSA Version List
	 * 
	 * @author amitm
	 * @param ds
	 * @return
	 */
	public static ArrayList getMsaVersionlist(String check, DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		if (check.equals("partnerSearch")) {
			list.add(new LabelValue("--Select--", "0"));
		}

		list.add(new LabelValue("Not Signed", "Not Signed"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_mcsa_id,cp_mcsa_version from cp_mcsa order by cp_mcsa_version");
			while (rs.next()) {
				list.add(new LabelValue(rs.getString("cp_mcsa_version"), rs
						.getString("cp_mcsa_version")));
			}

		} catch (Exception e) {
			logger.error("getMsaVersionlist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsaVersionlist(String, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getMsaVersionlist(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getMsaVersionlist(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getMsaVersionlist(String, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	/**
	 * @author amitm This Method will get Partner Type/Partner Level
	 * @param ds
	 * @return
	 */
	public static ArrayList getPartner_type(DataSource ds) {

		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new LabelValue("Select", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery("select * from dbo.func_cp_partner_type()");
			while (rs.next()) {
				list.add(new LabelValue(rs.getString("cp_partner_type_name"),
						rs.getString("cp_partner_type_id")));
			}
		} catch (Exception e) {
			logger.error("getPartner_type(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartner_type(DataSource) - Exception caught,could not get partner type"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn("getPartner_type(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn("getPartner_type(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn("getPartner_type(DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	public static String getPartnerStatus(String pid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String status = "";
		String statusdesc = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select cp_pd_status from dbo.func_cp_partner_info("
					+ pid + ")";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				status = rs.getString("cp_pd_status");
			}

			sql = "select dbo.func_partner_status_description('" + status
					+ "')";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				statusdesc = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("getPartnerStatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerStatus(String, DataSource) - Error occured in getting getPartnerStatus  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartnerStatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartnerStatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartnerStatus(String, DataSource) - exception ignored",
						e);
			}

		}
		return statusdesc;
	}

	/**
	 * @author amitm
	 * @This Method will give all job/site information for a partner id
	 * @param ds
	 * @return
	 */
	public static ArrayList getjobInformationbyPartnerId(String partnerId,
			String sortqueryclause, DataSource ds) {

		Connection conn = null;
		ResultSet rs = null;
		int retval = 0;
		ArrayList valuelist = new ArrayList();
		CallableStatement cstmt = null;

		DecimalFormat dfcurmargin = new DecimalFormat("0.00");

		try {
			conn = ds.getConnection();

			cstmt = conn.prepareCall("{?=call cp_job_site_of_partner_01(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, partnerId);
			cstmt.setString(3, sortqueryclause);

			rs = cstmt.executeQuery();
			int i = 0;

			while (rs.next()) {

				JobInformationBean jobInformationBean = new JobInformationBean();

				jobInformationBean.setCustomername(rs.getString("lo_ot_name"));
				jobInformationBean.setAppendixName(rs.getString("lx_pr_title"));
				jobInformationBean.setJobName(rs.getString("lm_js_title"));
				jobInformationBean.setOwner(rs.getString("job_created_by"));
				jobInformationBean.setJobstatus(rs.getString("lm_js_status"));
				jobInformationBean.setSitename(rs.getString("lm_si_name"));
				jobInformationBean.setSitenumber(rs.getString("lm_si_number"));
				jobInformationBean.setTempPartnerid(rs
						.getString("lm_po_partner_id"));

				jobInformationBean.setActualend(rs.getString("lx_se_end"));
				jobInformationBean.setActualstartdate(rs
						.getString("lx_se_start"));

				jobInformationBean.setSchedulecomplete(rs
						.getString("lm_js_planned_end_date"));
				jobInformationBean.setSchedulestartdate(rs
						.getString("lm_js_planned_start_date"));

				jobInformationBean.setEstimatedcost(Decimalroundup
						.twodecimalplaces(rs.getFloat("lx_ce_total_cost"), 2));
				jobInformationBean.setExtendedprice(Decimalroundup
						.twodecimalplaces(rs.getFloat("lx_ce_total_price"), 2));
				jobInformationBean.setProformavgp(dfcurmargin.format(rs
						.getFloat("lx_ce_margin")));
				jobInformationBean.setActualcost(Decimalroundup
						.twodecimalplaces(rs.getFloat("lx_ce_actual_cost"), 2));
				jobInformationBean.setActualvgp(Decimalroundup
						.twodecimalplaces(rs.getFloat("lx_ce_actual_vgp"), 2));
				jobInformationBean.setPoNumber(rs.getString("lm_po_number"));
				valuelist.add(i, jobInformationBean);
				i++;
			}
			retval = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"getjobInformationbyPartnerId(String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getjobInformationbyPartnerId(String, String, DataSource) - Error occured during job  list for a partner "
						+ e);
			}
			logger.error(
					"getjobInformationbyPartnerId(String, String, DataSource)",
					e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getjobInformationbyPartnerId(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getjobInformationbyPartnerId(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getjobInformationbyPartnerId(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	/**
	 * @author amitm
	 * @This Method will retrive partner information for a partner id.
	 * @param jobInformationForm
	 * @param partnerId
	 * @param ds
	 * @return
	 */
	public static JobInformationBean getPartnerInformation(
			JobInformationBean jobInformationForm, String partnerId,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from func_cp_partner_info_01(" + partnerId
					+ ")";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				jobInformationForm.setPartnerName(rs
						.getString("lo_om_division"));
				jobInformationForm.setPartnerCity(rs.getString("lo_ad_city"));
				jobInformationForm.setPartnerState(rs.getString("lo_ad_state"));
				jobInformationForm.setPartnerZipCode(rs
						.getString("lo_ad_zip_code"));
				jobInformationForm.setPartnerPhoneNo(rs
						.getString("lo_om_phone1"));
				jobInformationForm.setPartnerStatus(rs
						.getString("cp_ps_status"));
				jobInformationForm.setPartnerType(rs.getString("lo_ot_name"));
			}

		} catch (Exception e) {
			logger.error(
					"getPartnerInformation(JobInformationForm, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerInformation(JobInformationForm, String, DataSource) - Error occured during Partner Information  for a partner id "
						+ e);
			}
			logger.error(
					"getPartnerInformation(JobInformationForm, String, DataSource)",
					e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartnerInformation(JobInformationForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartnerInformation(JobInformationForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartnerInformation(JobInformationForm, String, DataSource) - exception ignored",
						e);
			}

		}
		return jobInformationForm;
	}

	public static void getCpIncidentQcChecklistDefault(
			AddIncidentBean addinciform, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.cp_incident_qcchecklist_default( "
					+ addinciform.getJobid() + " )";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				addinciform.setDate(rs.getString("lx_se_end"));
				addinciform.setHours(rs.getString("lx_se_hour"));
				addinciform.setMinutes(rs.getString("lx_se_minute"));
				addinciform.setAm(rs.getString("lx_se_ampm"));
			}
		} catch (Exception e) {
			logger.error(
					"getCpIncidentQcChecklistDefault(AddIncidentBean, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCpIncidentQcChecklistDefault(AddIncidentBean, DataSource) - Error occured in getting CpIncidentQcChecklistDefault  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCpIncidentQcChecklistDefault(AddIncidentBean, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCpIncidentQcChecklistDefault(AddIncidentBean, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCpIncidentQcChecklistDefault(AddIncidentForm, DataSource) - exception ignored",
						e);
			}

		}
		return;
	}

	// seema-21/12/2006
	public static int deletetechnician(Partner_EditBean form, String Pid,
			String tech_id, DataSource ds) {

		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		int deltech = -1;

		try {
			conn = ds.getConnection();
			stmt = conn.prepareCall("{?=call cp_technician_delete_01(?,?)}");
			stmt.registerOutParameter(1, Types.INTEGER);
			stmt.setString(2, form.getPid());
			stmt.setString(3, tech_id);
			stmt.execute();
			deltech = stmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"deletetechnician(Partner_EditForm, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("deletetechnician(Partner_EditForm, String, String, DataSource) - Error occured during deleting Technician section"
						+ e);
			}

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deletetechnician(Partner_EditForm, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deletetechnician(Partner_EditForm, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deletetechnician(Partner_EditForm, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return deltech;
	}

	public static void getTopLevelPartner(String Pid, String[] topPartner,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lo_ot_type, lo_ot_name, lo_ot_id from lo_organization_top "
					+ "inner join lo_organization_main "
					+ "on lo_organization_top.lo_ot_id = lo_organization_main.lo_om_ot_id "
					+ "inner join cp_partner "
					+ "on cp_partner.cp_partner_om_Id = lo_organization_main.lo_om_id "
					+ "where cp_partner.cp_partner_Id = '" + Pid + "'";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				topPartner[0] = (rs.getString("lo_ot_type"));
				topPartner[1] = (rs.getString("lo_ot_name"));
				topPartner[2] = (rs.getString("lo_ot_id"));
			}

		} catch (Exception e) {
			logger.error("getTopLevelPartner(String, String[], DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTopLevelPartner(String, String[], DataSource) - Error occured during getting Top Level Partner"
						+ e);
			}

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getTopLevelPartner(String, String[], DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getTopLevelPartner(String, String[], DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getTopLevelPartner(String, String[], DataSource) - exception ignored",
						e);
			}

		}

	}

	public static void getRefershList(Partner_EditBean form, String check,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (check.equals("state")) {

				String sql = "select cp_state_id,cp_country_id,cp_category_id,cp_region_id  from cp_state where cp_state_id ='"
						+ form.getState() + "'";
				rs = stmt.executeQuery(sql);

				if (rs.next()) {
					form.setState(rs.getString("cp_state_id"));
					form.setCountry(rs.getString("cp_country_id"));
					form.setCategory(rs.getString("cp_category_id"));
					form.setRegion(rs.getString("cp_region_id"));
				} else {
					form.setState("0");
					form.setCountry("0");
					form.setCategory("0");
					form.setRegion("0");
				}
			}
			if (check.equals("country")) {

				String sql = "select cp_country_id,cp_category_id,cp_region_id  from cp_state where cp_country_id ='"
						+ form.getCountry() + "'";
				rs = stmt.executeQuery(sql);

				if (rs.next()) {
					form.setCountry(rs.getString("cp_country_id"));
					form.setCategory(rs.getString("cp_category_id"));
					form.setRegion(rs.getString("cp_region_id"));
				} else {
					form.setCountry("0");
					form.setCategory("0");
					form.setRegion("0");
				}

			}

			String[] split1arr = form.getAfterHoursPhone();
			String[] split2arr = form.getAfterHoursEmail();

			ArrayList ph_email = new ArrayList();
			for (int i = 0; i < 4; i++) {
				ph_email.add(new Phone_Email(split1arr[i], split2arr[i]));
			}
			form.setPh_email(ph_email);

		} catch (Exception e) {
			logger.error(
					"getRefershList(Partner_EditForm, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRefershList(Partner_EditForm, String, DataSource) - Error occured during getting country , Category  , Region in getRefershList  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getRefershList(Partner_EditForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getRefershList(Partner_EditForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getRefershList(Partner_EditForm, String, DataSource) - exception ignored",
						e);
			}

		}

	}

	public static void getTopLevelPartnerByType(String type,
			String[] topPartner, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lo_ot_type, lo_ot_name, lo_ot_id from lo_organization_top where lo_ot_name = '"
					+ type + "' and lo_ot_type = 'P'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				topPartner[0] = (rs.getString("lo_ot_type"));
				topPartner[1] = (rs.getString("lo_ot_name"));
				topPartner[2] = (rs.getString("lo_ot_id"));
			}

		} catch (Exception e) {
			logger.error(
					"getTopLevelPartnerByType(String, String[], DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTopLevelPartnerByType(String, String[], DataSource) - Error occured during getting Top Level Partner By Type"
						+ e);
			}

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getTopLevelPartnerByType(String, String[], DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getTopLevelPartnerByType(String, String[], DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getTopLevelPartnerByType(String, String[], DataSource) - exception ignored",
						e);
			}

		}

	}

	// this method will update partner details for all tabs of Partner_Edit.jsp
	public static int setPartnerDetails(Partner_EditBean form, String user_id,
			String action, DataSource ds) {
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		int retvalue = -1;
		String temp = "";
		String partnerName = "";

		if (form.getTopPartnerName().equalsIgnoreCase("Minuteman Partners")) {
			partnerName = form.getPartnerLastName() + ", "
					+ form.getPartnerFirstName();
		} else {
			partnerName = form.getPartnerCandidate();
		}

		if (action.equals("Add")) {
			action = "A";
		} else {
			action = "U";
		}

		try {
			conn = ds.getConnection();
			stmt = conn
					.prepareCall("{?=call cp_partner_manage_new_01(?,?,?,?,?,?, ?,?,?,?,?,?, ?,?,?,?,?,?, ?,?,?,?,?,?, ?,?,?,?,?,? ,?,?,?,?,?,? ,?,?,?,?,?,? ,?,?,?, ?,?,?,?,?,?,?,?, ?,?,?, ?,?,?,?,?,?,?, ?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			stmt.registerOutParameter(1, Types.INTEGER);
			stmt.setInt(2, Integer.parseInt(form.getPid()));
			stmt.setString(3, partnerName); // Partner Name

			if (form.getTaxId().equals("")) {
				form.setTaxId(null);
			}

			stmt.setString(4, form.getTaxId());

			if (form.getCompanyType().trim().equals("")) {
				form.setCompanyType(null);
			}

			stmt.setString(5, form.getCompanyType());

			if (form.getDateInc().equals("")) {
				form.setDateInc(null);
			}

			stmt.setString(6, form.getDateInc());

			for (int i = 0; i < form.getCmboxCoreCompetency().length; i++) {
				temp = temp + form.getCmboxCoreCompetency(i) + ",";
			}

			if (temp.length() != 0) {
				temp = temp.substring(0, temp.length() - 1);
			}

			stmt.setString(7, temp);
			if (form.getAddress_id() == null || form.getAddress_id().equals("")) {
				form.setAddress_id("0");
			}
			stmt.setInt(8, Integer.parseInt(form.getAddress_id()));
			stmt.setString(9, form.getAddress1());
			stmt.setString(10, form.getAddress2());
			stmt.setString(11, form.getCity());
			stmt.setString(12, form.getState());
			stmt.setString(13, form.getZip());
			stmt.setString(14, form.getCountry());
			stmt.setString(15, form.getAcceptAddress());
			stmt.setString(16, form.getMainPhone());
			stmt.setString(17, form.getMainFax());
			stmt.setString(18, form.getCompanyURL());

			if (form.getKeyWord().equals("")) {
				form.setKeyWord("");
			}
			stmt.setString(19, form.getKeyWord()); // should be here.

			if (form.getPri_id() == null || form.getPri_id().equals("")) {
				form.setPri_id("0");
			}

			if (form.getSec_id() == null || form.getSec_id().equals("")) {
				form.setSec_id("0");
			}

			stmt.setInt(20, Integer.parseInt(form.getPri_id()));
			stmt.setInt(21, Integer.parseInt(form.getSec_id()));
			stmt.setString(22, form.getPrimaryFirstName());
			stmt.setString(23, form.getPrimaryLastName());
			stmt.setString(24, form.getPrmEmail());
			stmt.setString(25, form.getPrmPhone());
			stmt.setString(26, form.getPrmMobilePhone());
			stmt.setString(27, form.getSecondaryFirstName());
			stmt.setString(28, form.getSecondaryLastName());
			stmt.setString(29, form.getSecEmail());
			stmt.setString(30, form.getSecPhone());
			stmt.setString(31, form.getSecMobilePhone());

			stmt.setString(32, action); // achange Add to A and Update to U
			stmt.setInt(33, Integer.parseInt(form.getOrgTopId()));
			stmt.setInt(34, Integer.parseInt(user_id));
			stmt.setString(35, "I");
			stmt.setString(36, form.getWebUserName());
			stmt.setString(37, form.getWebPassword());
			stmt.registerOutParameter(38, java.sql.Types.INTEGER);
			stmt.registerOutParameter(39, java.sql.Types.VARCHAR);
			stmt.setString(40, form.getLat_degree());
			stmt.setString(41, form.getLat_min());
			stmt.setString(42, form.getLat_direction());
			stmt.setString(43, form.getLon_degree());
			stmt.setString(44, form.getLon_min());
			stmt.setString(45, form.getLon_direction());
			stmt.setString(46, form.getLatlon_source());
			String[] Faddress1 = form.getPhyFieldAddress1();
			String address1 = "";
			if (form.getPhyFieldAddress1() != null) {
				for (int i = 0; i < form.getPhyFieldAddress1().length; i++) {
					address1 = address1 + Faddress1[i] + "~";
				}
			}
			if (address1.length() > 0) {
				address1 = address1.substring(0, address1.length() - 1);
			}

			stmt.setString(47, address1);

			String[] Faddress2 = form.getPhyFieldAddress2();
			String address2 = "";
			if (form.getPhyFieldAddress2() != null) {
				for (int i = 0; i < form.getPhyFieldAddress2().length; i++) {
					address2 = address2 + Faddress2[i] + "~";
				}
			}
			if (address2.length() > 0) {
				address2 = address2.substring(0, address2.length() - 1);
			}

			stmt.setString(48, address2);

			String[] Fstate = form.getPhyFieldState();
			String state = "";
			if (form.getPhyFieldState() != null) {
				for (int i = 0; i < form.getPhyFieldState().length; i++) {
					state = state + Fstate[i] + "~";
				}
			}
			if (state.length() > 0) {
				state = state.substring(0, state.length() - 1);
			}

			stmt.setString(49, state);
			String[] Fzip = form.getPhyFieldZip();
			String zip = "";
			if (form.getPhyFieldZip() != null) {
				for (int i = 0; i < form.getPhyFieldZip().length; i++) {
					zip = zip + Fzip[i] + "~";
				}
			}
			if (zip.length() > 0) {
				zip = zip.substring(0, zip.length() - 1);
			}

			stmt.setString(50, zip);

			String[] Fphone = form.getPhyFieldPhone();
			String phone = "";
			if (form.getPhyFieldPhone() != null) {
				for (int i = 0; i < form.getPhyFieldPhone().length; i++) {
					phone = phone + Fphone[i] + "~";
				}
			}
			if (phone.length() > 0) {
				phone = phone.substring(0, phone.length() - 1);
			}

			stmt.setString(51, phone);

			String[] Ffax = form.getPhyFieldFax();
			String fax = "";
			if (form.getPhyFieldFax() != null) {
				for (int i = 0; i < form.getPhyFieldFax().length; i++) {
					fax = fax + Ffax[i] + "~";
				}
			}
			if (fax.length() > 0) {
				fax = fax.substring(0, fax.length() - 1);
			}

			stmt.setString(52, fax);

			if (form.getAfterHoursPhone() == null) {
				String tempPhone = ",,,";
				form.setAfterHoursPhone(tempPhone.split(",", 5));
			}

			String index1[] = form.getAfterHoursPhone();
			String combine1 = "";
			for (int i = 0; i < form.getAfterHoursPhone().length; i++) {
				combine1 = combine1 + index1[i] + ",";
			}

			if (combine1.length() > 0) {
				combine1 = combine1.substring(0, combine1.length() - 1);
			}
			stmt.setString(53, combine1);

			if (form.getAfterHoursEmail() == null) {
				String tempEmail = ",,,";
				form.setAfterHoursEmail(tempEmail.split(",", 5));
			}

			String index2[] = form.getAfterHoursEmail();
			String combine2 = "";
			for (int i = 0; i < form.getAfterHoursEmail().length; i++) {
				combine2 = combine2 + index2[i] + ",";
			}

			if (combine2.length() > 0) {
				combine2 = combine2.substring(0, combine2.length() - 1);
			}

			ArrayList ph_email = new ArrayList();
			for (int i = 0; i < ph_email.size(); i++) {
				/*
				 * Before there was used 4 here in the loop that was causing the
				 * exception, removed that 4 and added condition
				 * if(i==3){break;}
				 */
				ph_email.add(new Phone_Email(index1[i], index2[i]));
				if (i == 3) {
					break;
				}
			}
			form.setPh_email(ph_email);
			stmt.setString(54, combine2);
			/* upload file start */

			InputStream in = form.getInputStream();
			byte[] buffer1 = new byte[form.getFileSize()];
			if (in != null) {
				in.read(buffer1);
			}

			stmt.setString(55, form.getFileName());
			stmt.setString(56, form.getDisplayName());
			stmt.setBytes(57, buffer1);
			stmt.setString(58, form.getCmboxStatus());

			if (form.getMcsaVersion().equals("0")
					|| form.getMcsaVersion().equals("")) {
				form.setMcsaVersion(null);
			}

			stmt.setString(59, form.getMcsaVersion());
			stmt.setString(60, form.getRdoW9());
			stmt.setString(61, form.getIncidentReport());
			stmt.setString(62, form.getW9Uploaded());
			if (form.getRegistrationRenewalDate().equals("")) {
				form.setRegistrationRenewalDate(null);
			}
			stmt.setString(63, form.getRegistrationRenewalDate());

			stmt.setString(64, form.getLatitude());
			stmt.setString(65, form.getLongitude());
			stmt.setString(66, form.getLatLongAccuracy());

			// start tech
			String engName[] = form.getEngName();
			String cmboxResourceLevel[] = form.getCmboxResourceLevel();
			String engZip[] = form.getEngZip();
			String chkboxEquipped[] = form.getChkboxEquipped();
			String rdoUnion[] = form.getRdoUnion();
			String cmboxHighCriticality[] = form.getCmboxHighCriticality();
			String cmboxCertifications[] = form.getCmboxCertifications();
			String tech_id[] = form.getTech_id();
			String equip = "";

			String temp1 = "";
			for (int j = 0; j < cmboxCertifications.length; j++) {
				temp1 = temp1 + cmboxCertifications[j] + ",";
			}
			temp1 = temp1.replaceAll(",~,", "~");
			temp1 = temp1.substring(0, temp1.length() - 1);

			if (chkboxEquipped != null) {
				for (int i = 0; i < chkboxEquipped.length; i++) {
					if (chkboxEquipped[i].equals(",")) {
						equip = equip + "~";
					} else {
						equip = equip + chkboxEquipped[i] + ",";
					}
				}
			}

			equip = equip.replaceAll(",~", "~");
			equip = equip.substring(0, equip.length() - 1);

			if (engName != null) {
				chkboxEquipped = equip.split(",", engName.length);
			}

			String engname = "";
			String rdounion = "";
			String criticality = "";
			String resourcelevel = "";
			String resourcelevelName = "";
			String engzip = "";
			String techid = "";
			String equipped = "";
			int index = 0;

			if (form.getEngName() != null) {
				for (int i = 0; i < form.getEngName().length; i++) {
					if (i == (form.getEngName().length - 1)) {
						engname = engname + engName[i];
						rdounion = rdounion + rdoUnion[i];
						criticality = criticality + cmboxHighCriticality[i];
						index = cmboxResourceLevel[i].indexOf("|");
						resourcelevelName = resourcelevelName
								+ cmboxResourceLevel[i].substring(index + 1);
						resourcelevel = resourcelevel
								+ cmboxResourceLevel[i].substring(0, index);
						engzip = engzip + engZip[i];
						techid = techid + tech_id[i];
						equipped = equip;
					} else {
						engname = engname + engName[i] + "~";
						rdounion = rdounion + rdoUnion[i] + "~";
						criticality = criticality + cmboxHighCriticality[i]
								+ "~";
						index = cmboxResourceLevel[i].indexOf("|");
						resourcelevelName = resourcelevelName
								+ cmboxResourceLevel[i].substring(index + 1)
								+ "~";
						resourcelevel = resourcelevel
								+ cmboxResourceLevel[i].substring(0, index)
								+ "~";
						engzip = engzip + engZip[i] + "~";
						techid = techid + tech_id[i] + "~";
						equipped = equip;
					}
				}
			}

			stmt.setString(67, engname);
			stmt.setString(68, rdounion);
			stmt.setString(69, criticality);
			stmt.setString(70, resourcelevel);
			stmt.setString(71, resourcelevelName);
			stmt.setString(72, engzip);
			stmt.setString(73, temp1);
			stmt.setString(74, equipped);
			stmt.setString(75, techid);
			// end tech

			String coreCompIds = "";
			String companyIds = "";
			String validFromDates = "";
			String valideToDates = "";

			if (form.getWlcompany() != null) {
				String[] wlcompany = form.getWlcompany();
				String[] validFromWC = form.getValidFromWC();
				String[] validToWC = form.getValidToWC();

				for (int i = 0; i < form.getWlcompany().length; i++) {
					if (i == (form.getWlcompany().length - 1)) {
						companyIds = companyIds + wlcompany[i] + "~";
						validFromDates = validFromDates + validFromWC[i] + "~";
						valideToDates = valideToDates + validToWC[i] + "~";
					} else {
						companyIds = companyIds + wlcompany[i] + ",";
						validFromDates = validFromDates + validFromWC[i] + ",";
						valideToDates = valideToDates + validToWC[i] + ",";
					}
				}
				coreCompIds = coreCompIds + "2~";
			}

			if (form.getSdcompany() != null) {
				String[] sdcompany = form.getSdcompany();
				String[] validFromSD = form.getValidFromSD();
				String[] validToSD = form.getValidToSD();

				for (int i = 0; i < form.getSdcompany().length; i++) {
					if (i == (form.getSdcompany().length - 1)) {
						companyIds = companyIds + sdcompany[i] + "~";
						validFromDates = validFromDates + validFromSD[i] + "~";
						valideToDates = valideToDates + validToSD[i] + "~";
					} else {
						companyIds = companyIds + sdcompany[i] + ",";
						validFromDates = validFromDates + validFromSD[i] + ",";
						valideToDates = valideToDates + validToSD[i] + ",";
					}
				}
				coreCompIds = coreCompIds + "1~";
			}

			if (form.getItcompany() != null) {
				String[] itcompany = form.getItcompany();
				String[] validFromIT = form.getValidFromIT();
				String[] validToIT = form.getValidToIT();

				for (int i = 0; i < form.getItcompany().length; i++) {
					if (i == (form.getItcompany().length - 1)) {
						companyIds = companyIds + itcompany[i] + "~";
						validFromDates = validFromDates + validFromIT[i] + "~";
						valideToDates = valideToDates + validToIT[i] + "~";
					} else {
						companyIds = companyIds + itcompany[i] + ",";
						validFromDates = validFromDates + validFromIT[i] + ",";
						valideToDates = valideToDates + validToIT[i] + ",";
					}
				}
				coreCompIds = coreCompIds + "8~";
			}

			if (form.getTacompany() != null) {
				String[] tacompany = form.getTacompany();
				String[] validFromTA = form.getValidFromTA();
				String[] validToTA = form.getValidToTA();

				for (int i = 0; i < form.getTacompany().length; i++) {
					if (i == (form.getTacompany().length - 1)) {
						companyIds = companyIds + tacompany[i];
						validFromDates = validFromDates + validFromTA[i];
						valideToDates = valideToDates + validToTA[i];
					} else {
						companyIds = companyIds + tacompany[i] + ",";
						validFromDates = validFromDates + validFromTA[i] + ",";
						valideToDates = valideToDates + validToTA[i] + ",";
					}
				}
				coreCompIds = coreCompIds + "9";
			}

			stmt.setString(76, coreCompIds);
			stmt.setString(77, companyIds);
			stmt.setString(78, validFromDates);
			stmt.setString(79, valideToDates);

			String tools = "";
			String toolzip = "";
			String[] chkBoxTools = form.getChkboxTools();
			String[] toolZips = form.getToolZips();

			if (toolZips != null) {
				for (int i = 0, j = 0; i < toolZips.length; i++) {
					if (!(toolZips[i].trim().length() == 0)) {
						toolZips[j++] = toolZips[i];
					}
				}
			}

			if (form.getChkboxTools() != null) {
				for (int i = 0; i < form.getChkboxTools().length; i++) {
					if (i == (form.getChkboxTools().length - 1)) {
						tools = tools + chkBoxTools[i];
						toolzip = toolzip + toolZips[i];
					} else {
						tools = tools + chkBoxTools[i] + "~";
						toolzip = toolzip + toolZips[i] + "~";
					}
				}
			}

			stmt.setString(80, tools);
			stmt.setString(81, toolzip);
			stmt.setString(82, form.getMonthlyNewsletter());

			stmt.setString(83, form.getIsInsuranceSubmitted());
			stmt.setString(84, form.getIsInsuranceUploaded());
			stmt.setInt(85, form.isLeadMinuteman() ? 1 : 0);
			stmt.setInt(86, form.isExternalSalesAgent() ? 1 : 0);
			stmt.setString(87, form.getIsResumeSubmitted());
			stmt.setString(88, form.getIsResumeUploaded());
			stmt.setString(89, form.getGpVendorId().replaceAll("\'", "\'"));

			// saving values in partner details table
			stmt.setString(90, form.getBackgroundCheckSubmitted());
			stmt.setString(91, form.getBackgroundCheckUploaded());
			stmt.setString(92, form.getDrugScreenCompletionSubmitted());
			stmt.setString(93, form.getDrugScreenCompletionUploaded());
			stmt.setInt(94, form.isSpeedPaymentTerms() ? 1 : 0);
			stmt.setInt(95,
					Integer.parseInt(form.getFileuploadStatus() == null ? "0"
							: form.getFileuploadStatus()));
			stmt.setString(96, form.getVendorSiteCode().replaceAll("\'", "\'"));
			stmt.setString(97, form.getComcastVendorID().replaceAll("\'", "\'"));
			stmt.execute();
			retvalue = stmt.getInt(1);

			// If Error occurs during partner update, Restore the user entered
			// values in Form
			if (retvalue != 0) {
				Pvsdao.restoreFormValues(form, ds);
			}
			// Restore values code end
			int retvalue1 = stmt.getInt(38);

			if (retvalue == -90010) {
				if (stmt.getString(39) != null) {
					form.setDuplicatePartnerName(stmt.getString(39));
				}
			}

		} catch (Exception e) {
			logger.error(
					"setPartnerDetails(Partner_EditForm, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("setPartnerDetails(Partner_EditForm, String, String, DataSource) - Error occured during updating Partner details  :"
						+ e);
			}
			logger.error(
					"setPartnerDetails(Partner_EditForm, String, String, DataSource)",
					e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setPartnerDetails(Partner_EditForm, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setPartnerDetails(Partner_EditForm, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setPartnerDetails(Partner_EditForm, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retvalue;

	}

	public static int setPartnerDetailGeneralTab(Partner_EditBean form,
			String user_id, String action, DataSource ds) {
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		int retvalue = -1;
		String temp = "";
		String partnerName = "";

		if (form.getTopPartnerName().equalsIgnoreCase("Minuteman Partners")) {
			partnerName = form.getPartnerLastName() + ", "
					+ form.getPartnerFirstName();
		} else {
			partnerName = form.getPartnerCandidate();
		}

		if (action.equals("Add")) {
			action = "A";
		} else {
			action = "U";
		}

		try {
			conn = ds.getConnection();
			stmt = conn
					.prepareCall("{?=call cp_partner_manage_01_general_tab(?,?,?,?,?,?, ?,?,?,?,?,?, ?,?,?,?,?,?, ?,?,?,?,?,?, ?,?,?,?,?,? ,?,?,?,?,?,? ,?,?,?,?,?,? ,?,?,?,?, ?,?)}");
			/* one more ? for cp_pd_date parameter. */

			stmt.registerOutParameter(1, Types.INTEGER);
			stmt.setInt(2, Integer.parseInt(form.getPid()));
			stmt.setString(3, partnerName); // Partner Name

			if (form.getTaxId().equals("")) {
				form.setTaxId(null);
			}

			stmt.setString(4, form.getTaxId());

			if (form.getCompanyType().trim().equals("")) {
				form.setCompanyType(null);
			}

			stmt.setString(5, form.getCompanyType());

			if (form.getDateInc().equals("")) {
				form.setDateInc(null);
			}

			stmt.setString(6, form.getDateInc());

			for (int i = 0; i < form.getCmboxCoreCompetency().length; i++) {
				temp = temp + form.getCmboxCoreCompetency(i) + ",";
			}
			if (temp.length() != 0) {
				temp = temp.substring(0, temp.length() - 1);
			}

			stmt.setString(7, temp);
			if (form.getAddress_id() == null || form.getAddress_id().equals("")) {
				form.setAddress_id("0");
			}
			stmt.setInt(8, Integer.parseInt(form.getAddress_id()));
			stmt.setString(9, form.getAddress1());
			stmt.setString(10, form.getAddress2());
			stmt.setString(11, form.getCity());
			stmt.setString(12, form.getState());
			stmt.setString(13, form.getZip());
			stmt.setString(14, form.getCountry());
			stmt.setString(15, form.getAcceptAddress());
			stmt.setString(16, form.getMainPhone());
			stmt.setString(17, form.getMainFax());
			stmt.setString(18, form.getCompanyURL());

			if (form.getKeyWord().equals("")) {
				form.setKeyWord(null);
			}
			stmt.setString(19, form.getKeyWord()); // should be here.

			if (form.getPri_id() == null || form.getPri_id().equals("")) {
				form.setPri_id("0");
			}

			if (form.getSec_id() == null || form.getSec_id().equals("")) {
				form.setSec_id("0");
			}

			stmt.setInt(20, Integer.parseInt(form.getPri_id()));
			stmt.setInt(21, Integer.parseInt(form.getSec_id()));
			stmt.setString(22, form.getPrimaryFirstName());
			stmt.setString(23, form.getPrimaryLastName());
			stmt.setString(24, form.getPrmEmail());
			stmt.setString(25, form.getPrmPhone());
			stmt.setString(26, form.getPrmMobilePhone());
			stmt.setString(27, form.getSecondaryFirstName());
			stmt.setString(28, form.getSecondaryLastName());
			stmt.setString(29, form.getSecEmail());
			stmt.setString(30, form.getSecPhone());
			stmt.setString(31, form.getSecMobilePhone());
			stmt.setString(32, action); // achange Add to A and Update to U
			stmt.setInt(33, Integer.parseInt(form.getOrgTopId()));
			stmt.setInt(34, Integer.parseInt(user_id));
			stmt.setString(35, "I");
			stmt.setString(36, form.getWebUserName());
			stmt.setString(37, form.getWebPassword());
			stmt.registerOutParameter(38, java.sql.Types.INTEGER);
			stmt.registerOutParameter(39, java.sql.Types.VARCHAR);

			stmt.setString(40, form.getLat_degree());
			stmt.setString(41, form.getLat_min());
			stmt.setString(42, form.getLat_direction());
			stmt.setString(43, form.getLon_degree());
			stmt.setString(44, form.getLon_min());
			stmt.setString(45, form.getLon_direction());
			stmt.setString(46, form.getLatlon_source());
			stmt.setString(47, form.getLatitude());
			stmt.setString(48, form.getLongitude());
			stmt.setString(49, form.getLatLongAccuracy());

			stmt.execute();
			retvalue = stmt.getInt(1);
			int retvalue1 = stmt.getInt(38);

			if (form.getPid().equals("0") && (stmt.getInt(38) > 0)) {
				form.setPid(stmt.getString(38));
			}

			if (retvalue == -90010) {
				if (stmt.getString(39) != null) {
					form.setDuplicatePartnerName(stmt.getString(39));
				}
			}

		} catch (Exception e) {
			logger.error(
					"setPartnerDetailGeneralTab(Partner_EditForm, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("setPartnerDetailGeneralTab(Partner_EditForm, String, String, DataSource) - Error occured during setting Partner details on General Tab in setPartnerDetailGeneralTab() :"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setPartnerDetailGeneralTab(Partner_EditForm, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setPartnerDetailGeneralTab(Partner_EditForm, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setPartnerDetailGeneralTab(Partner_EditForm, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retvalue;

	}

	public static ArrayList getPOCResult(String division, String type,
			String startdate, String enddate, String searchtype,
			String querystring, String partnerType, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String mfgid = "";
		ArrayList pocSearchList = new java.util.ArrayList();
		PVSPocSearchList pvspocsearchList;
		division = division.replaceAll("\'", "\'\'");
		String sql;
		if (partnerType == null) {
			partnerType = "0";
		}
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{call search_poc_tab(?,?,?,?,?,?,?)}");
			cstmt.setString(1, division);
			cstmt.setString(2, type);
			cstmt.setString(3, startdate);
			cstmt.setString(4, enddate);
			cstmt.setString(5, searchtype);
			cstmt.setString(6, partnerType);
			cstmt.setString(7, querystring);
			rs = cstmt.executeQuery();
			while (rs.next()) {
				pvspocsearchList = new PVSPocSearchList();
				pvspocsearchList
						.setCp_partner_id(rs.getString("cp_partner_id"));
				pvspocsearchList.setLo_ot_name(rs.getString("lo_ot_name"));
				pvspocsearchList.setLo_om_division(rs
						.getString("lo_om_division"));
				pvspocsearchList.setLo_ad_address1(rs
						.getString("lo_ad_address1"));
				pvspocsearchList.setLo_ad_address2(rs
						.getString("lo_ad_address2"));
				pvspocsearchList.setLo_ad_city(rs.getString("lo_ad_city"));
				pvspocsearchList.setLo_ad_state(rs.getString("lo_ad_state"));
				pvspocsearchList
						.setLo_ad_country(rs.getString("lo_ad_country"));
				pvspocsearchList.setLo_ad_zip_code(rs
						.getString("lo_ad_zip_code"));
				pvspocsearchList.setCp_pd_incident_type(rs
						.getString("cp_pd_incident_type"));
				pvspocsearchList.setCp_pd_status(rs
						.getString("cp_partner_status"));
				pvspocsearchList.setCp_pt_date(rs.getString("cp_pt_date"));
				pvspocsearchList.setAddressValidator(rs
						.getString("lo_ad_accept_address"));
				pvspocsearchList.setRegRenewalDate(rs
						.getString("cp_pd_reg_renewal_date"));
				pocSearchList.add(pvspocsearchList);
			}

		} catch (Exception e) {
			logger.error(
					"getPOCResult(String, String, String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPOCResult(String, String, String, String, String, String, String, DataSource) - Error occured during getPOCResult()  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPOCResult(String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPOCResult(String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPOCResult(String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return pocSearchList;
	}

	public static String getDate(String date, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String getdate = "";
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (date.equals("startdate")) {
				sql = "select convert(varchar(10),getdate()-14,101) as sysdate";
			}
			if (date.equals("enddate")) {
				sql = "select convert(varchar(10),getdate(),101) as sysdate";
			}

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				getdate = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("getDate(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDate(String, DataSource) - Error occured in getting getSystemDate  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("getDate(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn("getDate(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("getDate(String, DataSource) - exception ignored",
						e);
			}

		}
		return getdate;
	}

	public static String getAnnualDate(String date, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String getdate = "";
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (date.equals("startdate")) {
				sql = "select convert(varchar(10),getdate(),101) as sysdate";
			}
			if (date.equals("enddate")) {
				sql = "select convert(varchar(10),getdate()+14,101) as sysdate";
			}

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				getdate = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("getAnnualDate(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAnnualDate(String, DataSource) - Error occured in getting getSystemDate  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getAnnualDate(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getAnnualDate(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getAnnualDate(String, DataSource) - exception ignored",
						e);
			}

		}
		return getdate;
	}

	// start
	public static String getStateCountryMatch(String stateId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String countryId = "0";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select cp_state_id,cp_country_id from cp_state where cp_state_id ='"
					+ stateId + "'";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				countryId = rs.getString("cp_country_id");
			}
		} catch (Exception e) {
			logger.error("getStateCountryMatch(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStateCountryMatch(String, DataSource) - Error occured in getStateCountryMatch method during getting country id based on stateid "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateCountryMatch(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateCountryMatch(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateCountryMatch(String, DataSource) - exception ignored",
						e);
			}

		}
		return countryId;
	}

	public static java.util.Collection getIncorporationType(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList incorporationType = new java.util.ArrayList();
		incorporationType.add(new LabelValue("--Select--", ""));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select cp_incorp_name from cp_incorporation_type";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				incorporationType.add(new LabelValue(rs
						.getString("cp_incorp_name"), rs
						.getString("cp_incorp_name")));
			}
		} catch (Exception e) {
			logger.error("getIncorporationType(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getIncorporationType(DataSource) - Error occured in getIncorporationType "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getIncorporationType(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getIncorporationType(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getIncorporationType(DataSource) - exception ignored",
						e);
			}

		}
		return incorporationType;
	}

	// end
	public static String getStateName(String stateId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String stateNameId = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select top 1 cp_state_name from cp_state where cp_state_id='"
					+ stateId + "'";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				stateNameId = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("getStateName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStateName(String, DataSource) - Error occured in getStateName "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateName(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateName(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateName(String, DataSource) - exception ignored",
						e);
			}

		}
		return stateNameId;
	}

	public static int setPartnerDetailInMySql(String pid, String action,
			String convertStatus, String mySqlUrl, DataSource ds) {
		Connection mySqlConn = null;
		Statement mySql = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet mySqlRs = null;
		ResultSet mySqlRs1 = null;

		int retMySqlValue = 1;
		String sql = "";
		String type = "";

		try {
			try {
				mySqlConn = MySqlConnection.getMySqlConnection(mySqlUrl);
			} catch (Exception e) {
				logger.error(
						"setPartnerDetailInMySql(String, String, String, String, DataSource)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("setPartnerDetailInMySql(String, String, String, String, DataSource) - Error in establishing connection to mySQL.");
				}
			}

			if (mySqlConn == null) {
				retMySqlValue = 9600;
			} else {

				try {
					conn = ds.getConnection();
					stmt = conn.createStatement();
					sql = "select * from func_get_cp_partner_web_seed_data('"
							+ pid + "')";
					rs = stmt.executeQuery(sql);
					mySql = mySqlConn.createStatement();
					String mySqlQuery = "select * from cp_user where cp_partner_id="
							+ pid;
					mySqlRs1 = mySql.executeQuery(mySqlQuery);

					if (rs.next()) {
						// Code for convert to minute man change start
						if (convertStatus != null && !convertStatus.equals("")
								&& mySqlRs1 != null && mySqlRs1.next()) {
							type = convertStatus;
						} else {
							if (convertStatus.equals("")) {
								type = rs.getString("partner_type");
							} else {
								retMySqlValue = 9601;
							}
						}

						// Code for convert to minute man change end
						if (retMySqlValue == 1) {
							sql = "insert into cp_user(U_Name, U_Pass, cp_partner_id, partner_name, partner_type, partner_status, partner_registration_status)"
									+ "values('"
									+ rs.getString("U_Name").replaceAll("\'",
											"\'\'")
									+ "','"
									+ rs.getString("U_Pass").replaceAll("\'",
											"\'\'")
									+ "',"
									+ rs.getInt("cp_partner_id")
									+ ",'"
									+ rs.getString("partner_name").replaceAll(
											"\'", "\'\'")
									+ "','"
									+ type
									+ "','"
									+ rs.getString("partner_status")
									+ "','"
									+ rs.getString("partner_registration_status")
									+ "')";
							if (action.equalsIgnoreCase("Update")) {
								String sql1 = "select * from cp_user where cp_partner_id ="
										+ pid;
								mySqlRs = mySql.executeQuery(sql1);

								if (mySqlRs != null && mySqlRs.next()) {
									sql = "update cp_user set U_Name = '"
											+ rs.getString("U_Name")
													.replaceAll("\'", "\'\'")
											+ "', U_Pass = '"
											+ rs.getString("U_Pass")
													.replaceAll("\'", "\'\'")
											+ "', partner_name = '"
											+ rs.getString("partner_name")
													.replaceAll("\'", "\'\'")
											+ "', partner_status = '"
											+ rs.getString("partner_status")
											+ "', partner_registration_status = '"
											+ rs.getString("partner_registration_status")
											+ "',partner_type = '" + type + "'"
											+ "where cp_partner_id ="
											+ Integer.parseInt(pid);
								}
							}

							retMySqlValue = mySql.executeUpdate(sql);
						}
						if (retMySqlValue != 1) {
							retMySqlValue = 9601;
						}
					}

				} catch (Exception e) {
					logger.error(
							"setPartnerDetailInMySql(String, String, String, String, DataSource)",
							e);

					if (logger.isDebugEnabled()) {
						logger.debug("setPartnerDetailInMySql(String, String, String, String, DataSource) - Error in coordinating partner information with the Web.");
					}
				}
			} // else block complete
		} catch (Exception e) {
			logger.error(
					"setPartnerDetailInMySql(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("setPartnerDetailInMySql(String, String, String, String, DataSource) - Error occured in setPartnerDetailInMySql "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (mySqlRs != null) {
					mySqlRs.close();
				}
				if (mySqlRs1 != null) {
					mySqlRs1.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setPartnerDetailInMySql(String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}

				if (mySql != null) {
					mySql.close();
				}

			} catch (Exception e) {
				logger.warn(
						"setPartnerDetailInMySql(String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}

				if (mySqlConn != null) {
					mySqlConn.close();
				}

			} catch (Exception e) {
				logger.warn(
						"setPartnerDetailInMySql(String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retMySqlValue;
	}

	// This method return resource development manager details as a string,
	// which will be used
	// for partner creation mail
	public static String getResourceDevManagerDetails(DataSource ds) {
		StringBuffer rdmDetails = new StringBuffer();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select lo_pc_first_name, lo_pc_last_name, lo_pc_email1, lo_pc_phone1 from dbo.func_cp_res_dev_manager_info()");

			if (rs.next()) {
				rdmDetails.append(rs.getString("lo_pc_first_name") + " "
						+ rs.getString("lo_pc_last_name") + "\n");
				rdmDetails.append(rs.getString("lo_pc_email1") + "\n");
				rdmDetails.append(rs.getString("lo_pc_phone1"));
			}
		} catch (Exception e) {
			logger.error("getResourceDevManagerDetails(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourceDevManagerDetails(DataSource) - Exception caught in Pvsdao.getResourceDevManagerDetails()  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getResourceDevManagerDetails(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getResourceDevManagerDetails(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getResourceDevManagerDetails(DataSource) - exception ignored",
						sql);
			}

		}
		return rdmDetails.toString();
	}

	/**
	 * Discription: This method is use to get Login user email id.
	 * 
	 * @param userid
	 *            Login user id
	 * @param ds
	 *            Database Object
	 * @return String emailId
	 */
	public static String getLoginUserEmail(String userid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String emailId = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select lo_pc_email1 from lo_poc where lo_pc_id="
							+ userid);

			if (rs.next()) {
				emailId = rs.getString("lo_pc_email1");
			}
		} catch (Exception e) {
			logger.error("getLoginUserEmail(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getLoginUserEmail(String, DataSource) - Exception caught in Pvsdao.getLoginUserEmail()  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getLoginUserEmail(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getLoginUserEmail(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getLoginUserEmail(String, DataSource) - exception ignored",
						sql);
			}

		}
		return emailId;
	}

	// This method return organization details as a string, which will be used
	// for partner creation mail
	public static String getOrganizationDetails(DataSource ds) {
		StringBuffer orgDetail = new StringBuffer();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select lo_org_address,lo_org_city,lo_org_state,lo_org_zipcode from lo_organization_address");

			if (rs.next()) {
				orgDetail.append(rs.getString("lo_org_address") + "\n");
				orgDetail.append(rs.getString("lo_org_city") + ", "
						+ rs.getString("lo_org_state") + ", "
						+ rs.getString("lo_org_zipcode") + "\n");
			}
		} catch (Exception e) {
			logger.error("getOrganizationDetails(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOrganizationDetails(DataSource) - Exception caught in Pvsdao.getOrganizationDetails()  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getOrganizationDetails(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getOrganizationDetails(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getOrganizationDetails(DataSource) - exception ignored",
						sql);
			}

		}
		return orgDetail.toString();
	}

	// This method return resource development manager id as a string
	public static String getResourceDevManagerId(DataSource ds) {
		String resDevManagerId = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select lo_pc_id from dbo.func_cp_res_dev_manager_info()");

			if (rs.next()) {
				resDevManagerId = rs.getInt("lo_pc_id") + "";
			}
		} catch (Exception e) {
			logger.error("getResourceDevManagerId(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourceDevManagerId(DataSource) - Exception caught in Pvsdao.getResourceDevManagerId()  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getResourceDevManagerId(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getResourceDevManagerId(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getResourceDevManagerId(DataSource) - exception ignored",
						sql);
			}

		}
		return resDevManagerId;
	}

	// This method return resource development manager email as a string
	public static String getResourceDevManagerEmail(DataSource ds) {
		String resDevManagerEmail = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select lo_pc_email1 from dbo.func_cp_res_dev_manager_info()");

			if (rs.next()) {
				resDevManagerEmail = rs.getString("lo_pc_email1");
			}
		} catch (Exception e) {
			logger.error("getResourceDevManagerEmail(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourceDevManagerEmail(DataSource) - Exception caught in Pvsdao.getResourceDevManagerEmail()  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getResourceDevManagerEmail(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getResourceDevManagerEmail(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getResourceDevManagerEmail(DataSource) - exception ignored",
						sql);
			}

		}
		return resDevManagerEmail;
	}

	// this method returns the resource dev manager mail content as a string
	public static String getResDevManagerMailContent(Partner_EditBean form,
			String user_id, String action, DataSource ds) {
		StringBuffer rdmDetail = new StringBuffer();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		String username = "";

		try {
			if (form.getAct().equalsIgnoreCase("Add")) {
				rdmDetail.append("A PVS Manager account was created by ");
			} else if (form.getAct().equalsIgnoreCase("Update")) {
				rdmDetail.append("A PVS Manager account was updated by ");
			}
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select  (lo_pc_first_name+' '+lo_pc_last_name)"
							+ " created_by from lo_poc where lo_pc_id ="
							+ user_id + "  and lo_pc_first_name <> 'NetMedX'");

			if (rs.next()) {
				rdmDetail.append(rs.getString("created_by") + " on ");
				username = rs.getString("created_by");
			}
			rs2 = stmt
					.executeQuery(" select cp_pt_created_date=replace(convert(varchar(10),cp_pt_created_date,101),'01/01/1900',''), "
							+ "cp_pt_updated_date=replace(convert(varchar(10),cp_pt_updated_date,101),'01/01/1900','') "
							+ " from cp_partner where cp_partner_id="
							+ form.getPid());
			if (rs2.next()) {
				if (form.getAct().equalsIgnoreCase("Add")) {
					rdmDetail.append(rs2.getString("cp_pt_created_date")
							+ " for ");
				} else if (form.getAct().equalsIgnoreCase("Update")) {
					rdmDetail.append(rs2.getString("cp_pt_updated_date")
							+ " for ");
				}
			}
			rdmDetail.append(Pvsdao.getPartnername(form.getPid(), ds) + ", "
					+ form.getCity() + ", " + form.getState());
			if (Pvsdao.getPartnerType(form.getPid(), ds) != null
					&& !Pvsdao.getPartnerType(form.getPid(), ds).trim()
							.equals("")) {
				rdmDetail.append(" as a "
						+ Pvsdao.getPartnerType(form.getPid(), ds)
						+ " Partner.");
			} else {
				rdmDetail.append(".");
			}
			rdmDetail.append("\n\n" + username);
		} catch (Exception e) {
			logger.error(
					"getResDevManagerMailContent(Partner_EditForm, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResDevManagerMailContent(Partner_EditForm, String, String, DataSource) - Exception caught in Pvsdao.getResDevManagerMailContent()  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (rs2 != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getResDevManagerMailContent(Partner_EditForm, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (SQLException sql) {
				logger.warn(
						"getResDevManagerMailContent(Partner_EditForm, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getResDevManagerMailContent(Partner_EditForm, String, String, DataSource) - exception ignored",
						sql);
			}

		}
		return rdmDetail.toString();
	}

	// this method returns the account mail content
	public static String getAccountMailContent(Partner_EditBean form,
			String user_id, String action, DataSource ds) {
		StringBuffer accDetail = new StringBuffer();
		String partnerType = "";
		try {
			if (form.getAct().equalsIgnoreCase("Add")) {
				accDetail.append("A PVS Manager account was created for ");
			} else if (form.getAct().equalsIgnoreCase("Update")) {
				accDetail.append("A PVS Manager account was updated for ");
			}
			accDetail.append(Pvsdao.getPartnername(form.getPid(), ds) + ", "
					+ form.getCity() + ", " + form.getState());
			if (Pvsdao.getPartnerType(form.getPid(), ds) != null
					&& !Pvsdao.getPartnerType(form.getPid(), ds).trim()
							.equals("")) {
				accDetail.append(" as a "
						+ Pvsdao.getPartnerType(form.getPid(), ds)
						+ " Partner.");
			} else {
				accDetail.append(".");
			}

		} catch (Exception e) {
			logger.error(
					"getAccountMailContent(Partner_EditForm, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAccountMailContent(Partner_EditForm, String, String, DataSource) - Exception caught in Pvsdao.getAccountMailContent()  :"
						+ e);
			}
		}
		return accDetail.toString();
	}

	// this method takes the partner id and return the partner type
	public static String getPartnerType(String partnerID, DataSource ds) {
		String partnerType = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select isnull(cp_pt_partner_type,'') as cp_pt_partner_type from cp_partner where cp_partner_id="
							+ partnerID);

			if (rs.next()) {
				partnerType = rs.getString("cp_pt_partner_type");
			}
			if (partnerType.trim().equalsIgnoreCase("M")) {
				partnerType = "Minuteman";
			}
			if (partnerType.trim().equalsIgnoreCase("S")) {
				partnerType = "Standard";
			}
		} catch (Exception e) {
			logger.error("getPartnerType(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerType(String, DataSource) - Exception caught in Pvsdao.getPartnerType()  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getPartnerType(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPartnerType(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPartnerType(String, DataSource) - exception ignored",
						sql);
			}

		}
		return partnerType;
	}

	public static boolean isContactInfoModified(Partner_EditBean form,
			DataSource ds) {
		boolean contactModified = false;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "select lo_ad_address1,lo_ad_address2,lo_ad_city,lo_ad_state,lo_ad_country,"
					+ "lo_ad_zip_code,lo_om_phone1,lo_om_fax from dbo.func_cp_partner_info_general_tab("
					+ form.getPid() + ")";
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery(query);

			if (rs.next()) {
				if (!rs.getString("lo_ad_address1").equals(form.getAddress1())) {
					contactModified = true;
				}
				if (!rs.getString("lo_ad_address2").equals(form.getAddress2())) {
					contactModified = true;
				}
				if (!rs.getString("lo_ad_city").equals(form.getCity())) {
					contactModified = true;
				}
				if (!rs.getString("lo_ad_state").equals(form.getState())) {
					contactModified = true;
				}
				if (!rs.getString("lo_ad_country").equals(form.getCountry())) {
					contactModified = true;
				}
				if (!rs.getString("lo_ad_zip_code").equals(form.getZip())) {
					contactModified = true;
				}
				if (!rs.getString("lo_om_phone1").equals(form.getMainPhone())) {
					contactModified = true;
				}
				if (!rs.getString("lo_om_fax").equals(form.getMainFax())) {
					contactModified = true;
				}
			}
		} catch (Exception e) {
			logger.error("isContactInfoModified(Partner_EditForm, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("isContactInfoModified(Partner_EditForm, DataSource) - Exception caught in Pvsdao.isContactInfoModified()  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"isContactInfoModified(Partner_EditForm, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"isContactInfoModified(Partner_EditForm, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"isContactInfoModified(Partner_EditForm, DataSource) - exception ignored",
						sql);
			}

		}
		return contactModified;
	}

	public static String checkforrole(String userid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String check = "N";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select 'Y' from lo_poc_rol"
					+ " LEFT  join lo_role on lo_pr_ro_id = lo_ro_id"
					+ " LEFT JOIN lo_poc ON lo_pc_id = lo_pr_pc_id"
					+ " LEFT  join lo_classification on lo_pc_role = lo_cl_id"
					+ " where lo_pr_pc_id = "
					+ userid
					+ " and (lo_ro_role_name in ('RDM','RDS') OR lo_cl_role = 'CPD')";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				check = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("checkforrole(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkforrole(String, DataSource) - Exception caught in Pvsdao.getPartnerType()  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"checkforrole(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkforrole(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkforrole(String, DataSource) - exception ignored",
						sql);
			}

		}
		return check;
	}

	public static String checkforAccountORCPDRole(String userId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String check = "N";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("SELECT 'Y' FROM lo_poc "
							+ " join lo_classification on lo_pc_role = lo_cl_id "
							+ " WHERE (lo_cl_role = 'CPD' OR lo_cl_role = 'Accounting') "
							+ " AND lo_pc_id = " + userId);

			if (rs.next()) {
				check = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("checkforAccountORCPDRole(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkforAccountORCPDRole(String, DataSource) - Exception caught in Pvsdao.getPartnerType()  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"checkforAccountORCPDRole(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkforAccountORCPDRole(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkforAccountORCPDRole(String, DataSource) - exception ignored",
						sql);
			}

		}
		return check;
	}

	public static int DeletePartnerInMySql(String pid, String mySqlUrl) {
		Connection mySqlConn = null;
		Statement mySqlStmt = null;
		ResultSet mySqlRs = null;
		int deletePartner = -1;
		String sql = "";

		try {
			try {
				mySqlConn = MySqlConnection.getMySqlConnection(mySqlUrl);
			} catch (Exception e) {
				logger.error("DeletePartnerInMySql(String, String)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("DeletePartnerInMySql(String, String) - Error in establishing connection to mySQL.");
				}
			}

			try {
				mySqlStmt = mySqlConn.createStatement();
				sql = "delete from cp_user where cp_partner_id =" + pid;
				deletePartner = mySqlStmt.executeUpdate(sql);
			} catch (Exception e) {
				logger.error("DeletePartnerInMySql(String, String)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("DeletePartnerInMySql(String, String) - Error occured in Delete Partner from MySql1.");
				}
			}
		} catch (Exception e) {
			logger.error("DeletePartnerInMySql(String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("DeletePartnerInMySql(String, String) - Error occured in Delete Partner from MySql2 "
						+ e);
			}
		} finally {
			try {
				if (mySqlRs != null) {
					mySqlRs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"DeletePartnerInMySql(String, String) - exception ignored",
						e);
			}

			try {
				if (mySqlStmt != null) {
					mySqlStmt.close();
				}

			} catch (Exception e) {
				logger.warn(
						"DeletePartnerInMySql(String, String) - exception ignored",
						e);
			}

			try {
				if (mySqlConn != null) {
					mySqlConn.close();
				}

			} catch (Exception e) {
				logger.warn(
						"DeletePartnerInMySql(String, String) - exception ignored",
						e);
			}

		}
		return deletePartner;
	}

	// This method checks if the physical_email property is null, if it is null,
	// this method stores default
	public static void checkEmptyPhyMail(Partner_EditBean form) {
		if (form.getPh_email() == null) {
			String split1 = ",,,";
			String[] split1arr = split1.split(",", 5);
			if (split1 != null) {
				form.setAfterHoursPhone(split1.split(",", 5));
			}

			String split2 = ",,,";
			String[] split2arr = split2.split(",", 5);
			if (split2 != null) {
				form.setAfterHoursEmail(split2.split(",", 5));
			}
			ArrayList ph_email = new ArrayList();
			for (int i = 0; i < 4; i++) {
				ph_email.add(new Phone_Email(split1arr[i], split2arr[i]));
			}
			form.setPh_email(ph_email);
		}
	}

	// This method takes the values from FormBean and populate its ArrayList
	// properties, if an error occurs during update
	public static void restoreFormValues(Partner_EditBean form, DataSource ds) {
		ArrayList phy_address = new ArrayList();
		if (form.getPhyFieldAddress1() != null) {
			for (int i = 0; i < form.getPhyFieldAddress1().length; i++) {
				phy_address.add(new PVSPhyAddress(
						form.getPhyFieldAddress1()[i], form
								.getPhyFieldAddress2()[i], form
								.getPhyFieldState()[i],
						form.getPhyFieldZip()[i], form.getPhyFieldPhone()[i],
						form.getPhyFieldFax()[i]));
			}
			form.setPhyAddress(phy_address);
		}

		ArrayList uploadedlist = new ArrayList();
		uploadedlist = Upload.getuploadedfiles("%", form.getPid(), "Partner",
				ds);
		if (uploadedlist.size() > 0) {
			form.setUploadedfileslist(uploadedlist);
		}

	}// end restoreFormValues

	public static int convertToMinuteman(String partnerId, String userId,
			char partnerSource, char partnerType, DataSource ds) {
		int returnValue = -1;
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn
					.prepareCall("{?= call cp_partner_convert_to_minuteman(?,?,?,?)}");
			stmt.registerOutParameter(1, Types.INTEGER);
			stmt.setInt(2, Integer.parseInt(partnerId));
			stmt.setString(3, userId);
			stmt.setString(4, String.valueOf(partnerSource));
			stmt.setString(5, String.valueOf(partnerType));
			stmt.execute();
			returnValue = stmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"convertToMinuteman(String, String, char, char, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("convertToMinuteman(String, String, char, char, DataSource) - Error occured during convvert to Minuteman Partner "
						+ e);
			}

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"convertToMinuteman(String, String, char, char, DataSource) - exception ignored",
						e);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"convertToMinuteman(String, String, char, char, DataSource) - exception ignored",
						e);
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"convertToMinuteman(String, String, char, char, DataSource) - exception ignored",
						e);
			}
		}
		return returnValue;
	}

	public static String getTopPartnerType(String pid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String type = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select  dbo.func_get_partner_type(" + pid + ")";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				type = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("getTopPartnerType(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTopPartnerType(String, DataSource) - Error occured in getting getPartnerStatus  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getTopPartnerType(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getTopPartnerType(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getTopPartnerType(String, DataSource) - exception ignored",
						e);
			}

		}
		return type;
	}

	// This method check if the partner is registered or not
	public static String checkPartnerRegStatus(String pid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String regStatus = "N";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select isnull(cp_pd_registration_status,'N') from cp_partner_details where cp_pd_partner_id ="
							+ pid);

			if (rs.next()) {
				regStatus = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("checkPartnerRegStatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkPartnerRegStatus(String, DataSource) - Exception caught in Pvsdao.checkPartnerRegStatus()  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"checkPartnerRegStatus(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkPartnerRegStatus(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkPartnerRegStatus(String, DataSource) - exception ignored",
						sql);
			}

		}
		return regStatus;
	}

	/**
	 * Definetion: this method is use to check whether PO assing to partner or
	 * not.
	 * 
	 * @param pid
	 *            - Partner Id
	 * @param ds
	 *            - Database object
	 * @return - int
	 */
	public static int checkPOAssign(String pid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int checkPoAssign = 0; // its same when this partner is not assign to
		// PO.

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select lm_po_id from lm_purchase_order where lm_po_partner_id = "
							+ pid);

			if (rs.next()) {
				checkPoAssign = -9003;
			}

		} catch (Exception e) {
			logger.error("checkPOAssign(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkPOAssign(String, DataSource) - Exception caught in Pvsdao.checkPOAssign  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"checkPOAssign(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkPOAssign(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkPOAssign(String, DataSource) - exception ignored",
						sql);
			}

		}
		return checkPoAssign;
	}

	/**
	 * Discription: This method is use for make email content.
	 * 
	 * @param sendFor
	 *            ResendAdd or namePass
	 * @param pid
	 *            Partner Id
	 * @param primaryFirstName
	 *            Primary Contacts first name
	 * @param primaryLastName
	 *            Primary Contacts last name
	 * @param partnerType
	 *            Certified Partners or Minutamen Partners
	 * @param webUserName
	 *            Web Account User Name
	 * @param webPassword
	 *            Web Account Password
	 * @param ds
	 *            Database object
	 * @return String content
	 */
	public static String getEmailContent(String sendFor, String pid,
			String primaryFirstName, String primaryLastName,
			String partnerType, String webUserName, String webPassword,
			DataSource ds, String filePath) {
		StringBuffer mailContent = new StringBuffer();
		String content = "";
		ResourceBundle rb = ResourceBundle
				.getBundle("com.mind.properties.ApplicationResources");
		BufferedReader reader = null;
		FileInputStream stream = null;

		if (sendFor.equals("ResendAdd")) {
			try {
				stream = new FileInputStream(new File(filePath));
				reader = new BufferedReader(new InputStreamReader(stream));

				while ((content = reader.readLine()) != null) {

					if (content.trim().equals("&lt;&lt;username&gt;&gt;")) {
						mailContent.append(webUserName);
					} else if (content.trim()
							.equals("&lt;&lt;password&gt;&gt;")) {
						mailContent.append(webPassword);
					} else if (content
							.trim()
							.equals("&lt;&lt;PRIMARY CONTACT FIRST NAME LAST NAME&gt;&gt;")) {
						mailContent.append(primaryFirstName + " "
								+ primaryLastName);
					} else if (content
							.trim()
							.equals("&lt;&lt;RESOURCE DEVELOPMENT COORDINATOR FIRST NAME AND LAST NAME&gt;&gt;")) {
						mailContent.append((Pvsdao
								.getResourceDevManagerDetails(ds)).replaceAll(
								"\n", "<br />"));
					} else if (content
							.trim()
							.equals("&lt;&lt;RESOURCE DEVELOPMENT COORDINATOR EMAIL ADDRESS&gt;&gt;<br />")
							|| content
									.trim()
									.equals("&lt;&lt;RESOURCE DEVELOPMENT COORDINATOR PHONE 1&gt;&gt;<br />")) {
					} else if (content.trim().equals(
							"&lt;&lt;Partner Type&gt;&gt;")) {
						mailContent.append(partnerType);
					} else if (content.trim().equals(
							"&lt;&lt;URIREGISTRATION&gt;&gt;")) {
						String uri = "#";

						if (partnerType.trim().equals("Certified Partners")) {
							uri = "https://pcc.contingent.com/index.php?cmp=cp_signup_v2&amp;ulog=W1Z"
									+ pid + "1759";
						} else if (partnerType.trim().equals(
								"Minuteman Partners")) {
							uri = "https://pcc.contingent.com/index.php?cmp=mm_signup_v2&amp;ulog=W1Z"
									+ pid + "1759";
						}

						mailContent.append(uri);
					} else {
						mailContent.append(content);
					}
				}
			} catch (Exception fe) {
				logger.error(
						"getEmailContent(String, String, String, String, String, String, String, DataSource, String)",
						fe);

				if (logger.isDebugEnabled()) {
					logger.debug("getEmailContent(String, String, String, String, String, String, String, DataSource, String) - Error in getting Email Contents for Partner Email. "
							+ fe);
				}
			} finally {
				try {
					if (reader != null) {
						reader.close();
					}
					if (stream != null) {
						stream.close();
					}
				} catch (Exception e) {
					logger.warn(
							"getEmailContent(String, String, String, String, String, String, String, DataSource, String) - exception ignored",
							e);

				}
			}
		}

		if (sendFor.equals("namePass")) {
			mailContent.append(rb
					.getString("common.Email.Resend.for.pvs.manager.content"));
			mailContent.append("Username = " + webUserName);
			mailContent.append("\n\nPassword = " + webPassword);
			mailContent
					.append("\n\nIf you need any further assistance, please feel free to contact me.");
			mailContent.append("\n\nSincerely,\n\n");
			mailContent.append(Pvsdao.getResourceDevManagerDetails(ds));
			mailContent.append("\nResource Development Coordinator");
			mailContent.append("\nContingent Network Services, LLC\n");
			mailContent.append(Pvsdao.getOrganizationDetails(ds));
			mailContent.append("www.contingent.net\n");
		}

		content = mailContent.toString();
		return content;
	}

	public static ArrayList getSiteDeatils(String jobId, String partnerId,
			String para, String radius, DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int retValue = 0;
		int i = 0;
		if (radius.equals("")) {
			radius = "0";
		}

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lm_site_detail_data (?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(jobId));
			cstmt.setInt(3, Integer.parseInt(partnerId));
			cstmt.setString(4, para);
			cstmt.setInt(5, Integer.parseInt(radius));
			rs = cstmt.executeQuery();
			while (rs.next()) {
				Partner_SearchBean psForm = new Partner_SearchBean();
				psForm.setSiteNumber(rs.getString("lm_si_number"));
				psForm.setSiteAddress(rs.getString("lm_si_address"));
				psForm.setSiteCity(rs.getString("lm_si_city"));
				psForm.setSiteState(rs.getString("lm_si_state"));
				psForm.setZip(rs.getString("lm_si_zip_code"));
				psForm.setSiteLat(rs.getString("lm_si_lat"));
				psForm.setSiteLong(rs.getString("lm_si_lon"));
				psForm.setSiteColor(rs.getString("sitecolor"));
				psForm.setMsaCustomerName(rs.getString("lo_ot_name"));
				psForm.setAppendixName(rs.getString("lx_pr_title"));
				list.add(i, psForm);
				i++;
			}
			retValue = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"getSiteDeatils(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteDeatils(String, String, String, String, DataSource) - Error occured getting Site Information "
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getSiteDeatils(String, String, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getSiteDeatils(String, String, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getSiteDeatils(String, String, String, String, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	public static String convertSiteRatingToXML(String checkPage,
			String Zipcode, String radius, List plist) {
		Iterator listIterator = plist.iterator();
		StringBuffer siteXML = new StringBuffer();
		siteXML.append("\'<?xml version = \"1.0\"?>");
		siteXML.append("<sites>");

		if (checkPage.equals("JOB")) {
			while (listIterator.hasNext()) {
				Partner_SearchBean siteDetail = (Partner_SearchBean) listIterator
						.next();
				siteXML.append("<site");
				siteXML.append(" lat =\"" + siteDetail.getSiteLat() + "\"");
				siteXML.append(" lon =\"" + siteDetail.getSiteLong() + "\"");
				siteXML.append(" siteNumber =\""
						+ Util.filter(siteDetail.getSiteNumber()) + "\"");
				siteXML.append(" siteAddress =\""
						+ Util.filter(siteDetail.getSiteAddress()) + "\"");
				siteXML.append(" siteCity =\""
						+ Util.filter(siteDetail.getSiteCity()) + "," + "\"");
				siteXML.append(" siteState =\""
						+ Util.filter(siteDetail.getSiteState()) + "\"");
				siteXML.append(" siteZipCode =\""
						+ Util.filter(siteDetail.getZip()) + "\"");
				siteXML.append(" icontype =\""
						+ Util.filter(siteDetail.getSiteColor()) + "\"");
				siteXML.append(" msaName =\""
						+ Util.filter(siteDetail.getMsaCustomerName()) + "\"");
				siteXML.append(" appendixName =\""
						+ Util.filter(siteDetail.getAppendixName()) + "\"");
				siteXML.append("/>");
			}
		}
		siteXML.append("</sites>\'");
		return siteXML.toString();

	}

	public static String convertCheckRatingToXML(String Zipcode, String radius,
			String jobId, DataSource ds) {
		String latitudeLong[] = new String[3];
		latitudeLong = getLAtLongFromZip(Zipcode, jobId, ds);
		if (latitudeLong[2] != null && !latitudeLong[2].equals("")) {
			Zipcode = latitudeLong[2];
		}
		StringBuffer checkXML = new StringBuffer();
		checkXML.append("\'<?xml version = \"1.0\"?>");
		checkXML.append("<Cheks>");
		checkXML.append("<check");
		checkXML.append(" Zipcode =\"" + Util.filter(Zipcode) + "\"");
		checkXML.append(" checkLAt =\"" + Util.filter(latitudeLong[0]) + "\"");
		checkXML.append(" checkLong =\"" + Util.filter(latitudeLong[1]) + "\"");
		checkXML.append(" Radius =\"" + Util.filter(radius) + "\"");
		checkXML.append("/>");
		checkXML.append("</Cheks>\'");
		return checkXML.toString();

	}

	public static String[] getLAtLongFromZip(String Zipcode, String jobId,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String str = "";
		String latLong[] = new String[3];
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (jobId == null || jobId.equals("")) {
				str = "select ZIPCode,Longitude,Latitude from cp_address_master where ZIPCode ='"
						+ Zipcode + "'";
			} else {
				str = "select * from func_get_latlong_fromjob("
						+ Integer.parseInt(jobId) + ")";
			}
			rs = stmt.executeQuery(str);
			if (rs.next()) {
				latLong[0] = rs.getString("Latitude");
				latLong[1] = rs.getString("Longitude");
				latLong[2] = rs.getString("ZIPCode");
			}
		} catch (Exception e) {
			logger.error("getLAtLongFromZip(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getLAtLongFromZip(String, String, DataSource) - Error ocured for getting the latlong from Zip: "
						+ e);
			}

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getLAtLongFromZip(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getLAtLongFromZip(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getLAtLongFromZip(String, String, DataSource) - exception ignored",
						sql);
			}

		}

		return latLong;
	}

	/**
	 * Discription: This method is call when re send E-mail to partner. This
	 * method is user to get Bcc email id form propery file.
	 * 
	 * @return
	 */
	public static String getBccEmailId() {

		StringBuffer emailId = new StringBuffer();

		String apdxPvsEmail2 = EnvironmentSelector
				.getBundleString("appendix.pvs.bcc.Emailid2");
		if (apdxPvsEmail2 != null && !"".equals(apdxPvsEmail2.trim())) {
			emailId.append(",");
			emailId.append(apdxPvsEmail2);
		}

		return emailId.toString();
	}

	public static ArrayList getSingleSiteDeatils(String jobId, DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String str = "";
		int retValue = 0;
		int i = 0;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			str = "select * from func_get_latlong_fromjob("
					+ Integer.parseInt(jobId) + ")";
			rs = stmt.executeQuery(str);
			while (rs.next()) {
				Partner_SearchBean psForm = new Partner_SearchBean();
				psForm.setSiteNumber(rs.getString("lm_si_number"));
				psForm.setSiteAddress(rs.getString("lm_si_address"));
				psForm.setSiteCity(rs.getString("lm_si_city"));
				psForm.setSiteState(rs.getString("lm_si_state"));
				psForm.setZip(rs.getString("ZIPCode"));
				psForm.setSiteLat(rs.getString("Latitude"));
				psForm.setSiteLong(rs.getString("Longitude"));
				psForm.setSiteColor(rs.getString("sitecolor"));
				psForm.setMsaCustomerName(rs.getString("lo_ot_name"));
				psForm.setAppendixName(rs.getString("lx_pr_title"));
				list.add(i, psForm);
				i++;
			}

		} catch (Exception e) {
			logger.error("getSingleSiteDeatils(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSingleSiteDeatils(String, DataSource) - Error occured getting single site information: "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return list;
	}

	public static ArrayList getpartnerDeatilsList(int partnerId, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		DecimalFormat dfcur = new DecimalFormat("###0.0");
		String str = "";
		int retVal = 0;
		int i = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?= call lm_partner_detail( ?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, partnerId);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				Partner_SearchBean psForm = new Partner_SearchBean();
				psForm.setPid(rs.getString("cp_partner_id"));
				psForm.setPartnername(rs.getString("lo_om_division"));
				psForm.setCity(rs.getString("lo_ad_city"));
				psForm.setState(rs.getString("lo_ad_state"));
				psForm.setZipcode(rs.getString("lo_ad_zip_code"));
				psForm.setCountry(rs.getString("lo_ad_country"));
				psForm.setPhone(rs.getString("lo_om_phone1"));
				psForm.setStatus(rs.getString("cp_pd_status"));
				psForm.setPartnerType(rs.getString("lo_ot_name"));
				psForm.setDistance(dfcur.format(rs.getFloat("distance")) + "");
				psForm.setPartnerAddress1(rs.getString("lo_ad_address1"));
				psForm.setPartnerAddress2(rs.getString("lo_ad_address2"));
				psForm.setPartnerPriName(rs.getString("lo_pc_first_name") + " "
						+ rs.getString("lo_pc_last_name"));
				psForm.setPartnerPriPhone(rs.getString("lo_pc_phone1"));
				psForm.setPartnerPriCellPhone(rs.getString("lo_pc_cell_phone"));
				psForm.setPartnerPriEmail(rs.getString("lo_pc_email1"));
				psForm.setPartnerAvgRating(rs.getString("qc_ps_average_rating"));
				psForm.setPartnerLatitude(rs.getString("lo_ad_latitude"));
				psForm.setPartnerLongitude(rs.getString("lo_ad_longitude"));
				psForm.setIcontype(setGMapIconForPartner(
						rs.getString("lo_ot_name"),
						rs.getString("qc_ps_average_rating"),
						rs.getString("cp_pd_status")));

				valuelist.add(i, psForm);
				i++;
			}

			retVal = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("getpartnerDeatilsList(int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getpartnerDeatilsList(int, DataSource) - Error occured getting lm_partner_detail: "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return valuelist;
	}

	public static String getJobTitle(String jobid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String jobName = ""; // its same when this partner is not assign to PO.

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select lm_js_title from lm_job where lm_js_id = "
							+ jobid);
			if (rs.next()) {
				jobName = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("getJobTitle(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobTitle(String, DataSource) - Exception caught in Pvsdao.getJobTitle  :"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return jobName;
	}

	public static String getPartnerName(String partnerid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String partnerName = ""; // its same when this partner is not assign to
		// PO.

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select dbo.func_get_partnerName("
					+ partnerid + ")");

			if (rs.next()) {
				partnerName = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("getPartnerName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerName(String, DataSource) - Exception caught in Pvsdao.getPartnerName  :"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return partnerName;
	}

	public static String getPartnerForPO(String poId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String partnerId = "0"; // its same when this partner is not assign to
		// PO.

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lm_po_partner_id = isnull(lm_po_partner_id,0) from lm_purchase_order where lm_po_id = "
					+ poId;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				partnerId = rs.getString("lm_po_partner_id");
			}

		} catch (Exception e) {
			logger.error("getPartnerForPO(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerForPO(String, DataSource) - Exception caught in Pvsdao.getPartnerForPO  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getPartnerForPO(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPartnerForPO(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getPartnerForPO(String, DataSource) - exception ignored",
						sql);
			}

		}
		return partnerId;
	}

	public static ArrayList<LabelValue> getCountryList(DataSource ds) {

		ArrayList<LabelValue> countryList = new ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select distinct cp_country_id,cp_country_name from cp_state where cp_country_id is not null and cp_country_id<>'CA' order by cp_country_name");

			countryList.add(new LabelValue("--Select--", "0"));

			while (rs.next()) {
				countryList.add(new LabelValue(rs.getString("cp_country_name"),
						rs.getString("cp_country_id")));
			}
		} catch (Exception e) {
			logger.error("getCountryList(DataSource)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("getCountryList(DataSource)Exception caught" + e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn("getCountryList( DataSource)exception ignored", sql);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn("getCountryList( DataSource)exception ignored", sql);
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn("getCountryList(DataSource)exception ignored", sql);
			}
		}
		return countryList;

	}

	public static ArrayList<LabelValue> getCityList(String CountryID,
			DataSource ds) {

		ArrayList<LabelValue> cityList = new ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			StringBuffer SQL_QUERY = new StringBuffer();
			SQL_QUERY
					.append(" select distinct lo_ad_city from dbo.cp_partner ");
			SQL_QUERY
					.append(" join dbo.lo_organization_main on cp_partner_om_id = lo_om_id ");
			SQL_QUERY.append(" join lo_address on lo_om_ad_id1 =lo_ad_id ");
			SQL_QUERY.append(" where lo_ad_country =" + "'" + CountryID + "'");

			rs = stmt.executeQuery(SQL_QUERY.toString());

			cityList.add(new LabelValue("--Select--", "0"));
			while (rs.next()) {
				cityList.add(new LabelValue(rs.getString("lo_ad_city"), rs
						.getString("lo_ad_city")));
			}
		} catch (Exception e) {
			logger.error("getCountryList(DataSource)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("getCountryList(DataSource)Exception caught" + e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn("getCountryList( DataSource)exception ignored", sql);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn("getCountryList( DataSource)exception ignored", sql);
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn("getCountryList(DataSource)exception ignored", sql);
			}
		}
		return cityList;
	}

	/**
	 * get CSVReportViewers status
	 * 
	 * @param userId
	 *            the user id
	 * @param ds
	 *            the ds
	 * @return the cSV report viewer status
	 */
	public static String getCSVReportViewerStatus(String userId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String status = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			StringBuilder sqlQuery = new StringBuilder(
					" SELECT 1 as status FROM dbo.lo_poc ");
			sqlQuery.append(" join lo_classification on lo_pc_role = lo_cl_id  ");
			sqlQuery.append(" WHERE lo_pc_id IN ( SELECT EmployeeID ");
			sqlQuery.append(" FROM datawarehouse.dbo.DimEmployee  ");
			sqlQuery.append(" WHERE EmployeeTransactionType IN ('Hire', 'Dept Change') ");
			sqlQuery.append(" AND EffectiveStartDate <= getdate() ");
			sqlQuery.append(" AND EffectiveEndDate >= getdate() ");
			sqlQuery.append(" ) and ((1=case lo_cl_role when 'CPD' then 1 end) ");
			sqlQuery.append(" or (1=case lo_cl_role when 'Projects' then  ");
			sqlQuery.append(" case lo_pc_rank when 'Sr Manager' then 1 end  end )");
			sqlQuery.append(" or (1=case lo_cl_role when 'MSP Billing' then  ");
			sqlQuery.append(" case lo_pc_rank when 'Sr Manager' then 1 end  end )");
			sqlQuery.append(" ) and lo_pc_id=" + userId + "");
			sqlQuery.append(" union ");
			sqlQuery.append(" select 1 as status  from lo_poc ");
			sqlQuery.append(" where lo_pc_first_name='Robert' ");
			sqlQuery.append(" and lo_pc_last_name='Schwieterman' ");
			sqlQuery.append(" and lo_pc_id=" + userId + " ");
			sqlQuery.append(" union "); // Added for allowing CNS Administrators

			sqlQuery.append(" select 1 as status  from lo_poc_rol ");
			sqlQuery.append(" left JOIN lo_poc ON lo_poc.lo_pc_id = lo_poc_rol.lo_pr_pc_id");
			sqlQuery.append(" left JOIN lo_classification ON lo_poc.lo_pc_role = lo_classification.lo_cl_id");
			sqlQuery.append(" where lo_pr_ro_id in (11)");
			sqlQuery.append(" and lo_pr_pc_id=" + userId);
			sqlQuery.append(" and lo_pc_role in (14) ");

			rs = stmt.executeQuery(sqlQuery.toString());

			if (rs.next()) {
				status = rs.getString("status");
			}

		} catch (Exception e) {
			logger.error("getCSVReportViewerStatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCSVReportViewerStatus(String, DataSource) - Exception caught in Pvsdao.getCSVReportViewerStatus  :"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return status;
	}

	public static void setMinutemanPayrollId(Partner_EditBean partner_EditBean,
			DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		String str = "";
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?= call cp_minuteman_payroll_id_manage_01(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.valueOf(partner_EditBean.getPid()));
			cstmt.setInt(3,
					Integer.valueOf(partner_EditBean.getMinutemanPayrollId()));
			cstmt.executeUpdate();

		} catch (Exception e) {
			logger.error(
					"setMinutemanPayrollId(Partner_EditBean partner_EditBean, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("setMinutemanPayrollId(Partner_EditBean partner_EditBean,, DataSource) - setMinutemanPayrollId "
						+ e);
			}
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
	}

	public static String getMinutePayrollId(String partnerid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String status = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			StringBuilder sqlQuery = new StringBuilder(
					" select csh_id from cp_minuteman_payroll_id where cp_partner_id = "
							+ partnerid);

			rs = stmt.executeQuery(sqlQuery.toString());

			if (rs.next()) {
				status = rs.getString("csh_id");
			}

		} catch (Exception e) {
			logger.error("getMinutePayrollId(String partnerid, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMinutePayrollId(String partnerid, DataSource) - Exception caught in Pvsdao.getCSVReportViewerStatus  :"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return status;
	}

	// List from dao
	public static ArrayList<String[]> getToolListwithzip(DataSource ds,
			String partner_id, int parent_id) {
		ArrayList<String[]> list = new ArrayList<String[]>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String cond = " AND cp_tools_parent_id is null ";

		if (parent_id != 0) {
			cond = " AND cp_tools_parent_id = " + parent_id;
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery(" select * from ( select cp_tool_id, cp_tool_name, '' as cp_zip_code, NULL  AS cp_tools_parent_id  from cp_tools"
							+ " where cp_tool_id not in (select cp_tool_id from cp_partner_tool_list where cp_partner_id ="
							+ partner_id
							+ ")  "
							+ cond
							+ " union all"
							+ " select a.cp_tool_id, a.cp_tool_name, isnull(b.cp_zip_code,'') as cp_zip_code, NULL  AS cp_tools_parent_id  from cp_tools a"
							+ " left outer join cp_partner_tool_list b on a.cp_tool_id = b.cp_tool_id"
							+ " where b.cp_partner_id = "
							+ partner_id
							+ cond
							+ " ) aaa  ORDER by CASE WHEN cp_tool_name='Other' THEN 1 ELSE 0 END, cp_tool_name ");

			while (rs.next()) {
				String[] st = new String[4];

				st[0] = rs.getString("cp_tool_id");
				st[1] = rs.getString("cp_tool_name");
				st[2] = rs.getString("cp_zip_code");
				st[3] = rs.getString("cp_tools_parent_id");
				list.add(st);

			}

		} catch (Exception e) {
			logger.error("getToolList(Partner_EditForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getToolList(Partner_EditForm, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getToolList(Partner_EditForm, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getToolList(Partner_EditForm, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getToolList(Partner_EditForm, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	// GET STATUS OF PARTNER
	public static String getPartnersStatus(String partnerid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String partnersStatus = "None";

		try {
			String mySqlUrl = EnvironmentSelector
					.getBundleString("appendix.detail.changestatus.mysql.url");
			conn = MySqlConnection.getMySqlConnection(mySqlUrl);
			stmt = conn.createStatement();

			String sql = "SELECT STATUS FROM cp_photo_uploads WHERE partner_id = "
					+ partnerid;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				partnersStatus = rs.getString("STATUS");
			}

		} catch (Exception e) {
			logger.error("getPartnersStatus(String,DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnersStatus(String,DataSource) - Error in establishing connection to mySQL.");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartners(String,DataSource) - exception ignored", e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartners(String,DataSource) - exception ignored", e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartners(String,DataSource) - exception ignored", e);
			}

		}
		return partnersStatus;
	}

	public static void deleteFileByFileId(String fileId, DataSource ds) {

		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;

		String delQuery = "DELETE FROM lx_upload_file WHERE lx_file_id = ?";

		try {

			pStmt = conn.prepareStatement(delQuery);
			pStmt.setInt(1, Integer.parseInt(fileId));
			pStmt.executeUpdate();

		} catch (Exception e) {
			logger.error("deleteFileByFileId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteFileByFileId(String, DataSource) - Error in coordinating partner information with the Web.");
			}
		} finally {

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deleteFileByFileId(String, DataSource) - exception ignored",
						e);
			}

		}

	}

	// function that return list from cp_tool table
	public static ArrayList getToolListwithcpname(String pid, DataSource ds) {

		ArrayList plist = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_tool_name from cp_tools"
							+ " where cp_tool_id  in (select cp_tool_id from cp_partner_tool_list where cp_partner_id ="
							+ pid + ") " + " order by cp_tool_name");

			while (rs.next()) {
				plist.add(rs.getString("cp_tool_name"));
			}

		} catch (Exception e) {
			logger.error("getToolListwithcpname(Partner_EditForm, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getToolListwithcpname(Partner_EditForm, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getToolListwithcpname(Partner_EditForm, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getToolListwithcpname(Partner_EditForm, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getToolListwithcpname(Partner_EditForm, DataSource) - exception ignored",
						sql);
			}

		}
		return plist;
	}

	public static ArrayList<String[]> getThumbUpOrDownComments(String pid,
			String upOrDown, DataSource ds) {

		ArrayList<String[]> plist = new ArrayList<String[]>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("SELECT qc_pj_op_rating_Comments, convert(VARCHAR(25),qc_pj_date) qc_pj_date, lm_js_title, lo_pc_first_name+' '+ lo_pc_last_name name "
							+ " FROM qc_partner_job_rating  "
							+ " LEFT JOIN lm_job ON lm_js_id = qc_pj_lm_js_id "
							+ " LEFT JOIN lo_poc ON lo_pc_id = lm_js_changed_by  "
							+ " WHERE (qc_pj_op_rating_Comments IS NOT NULL OR qc_pj_op_rating_Comments <> '') "
							+ " AND isnull(qc_pj_future_jobs, 'N') = '"
							+ upOrDown
							+ "' "
							+ " AND qc_pj_partner_cp_partner_id =  "
							+ pid
							+ " ORDER BY qc_pj_date desc ");

			while (rs.next()) {

				try {
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
							"MMM dd yyyy hh:mma");
					Date date = simpleDateFormat.parse(rs
							.getString("qc_pj_date"));
					simpleDateFormat = new SimpleDateFormat(
							"MM/dd/yyyy hh:mm a");
					plist.add(new String[] {
							rs.getString("qc_pj_op_rating_Comments"),
							rs.getString("lm_js_title"),
							simpleDateFormat.format(date), rs.getString("name") });
				} catch (ParseException ex) {
					logger.error("Exception " + ex);
				}

			}

		} catch (Exception e) {
			logger.error(
					"getThumbUpOrDownComments(pid, upOrDownFlag, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getThumbUpOrDownComments(pid, upOrDownFlag, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getThumbUpOrDownComments(pid, upOrDownFlag, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getThumbUpOrDownComments(pid, upOrDownFlag, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getThumbUpOrDownComments(pid, upOrDownFlag, DataSource) - exception ignored",
						sql);
			}

		}
		return plist;
	}

	public static int getFileCount(String fileRemarks, String pid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int count = 0;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			StringBuilder sqlQuery = new StringBuilder(
					"SELECT count(*) as total from	 lx_upload_file WHERE lx_file_remarks LIKE '"
							+ fileRemarks + "' AND lx_file_used_in = " + pid);

			rs = stmt.executeQuery(sqlQuery.toString());

			if (rs.next()) {
				count = rs.getInt("total");
			}

		} catch (Exception e) {
			logger.error("getMinutePayrollId(String partnerid, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMinutePayrollId(String partnerid, DataSource) - Exception caught in Pvsdao.getCSVReportViewerStatus  :"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return count;

	}

	public static void updateFileStatus(String pid, String fileRemarks,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "";
		if (fileRemarks.contains("W-9")) {
			sql = "UPDATE dbo.cp_partner_details SET cp_pd_w9w9 = 'N', cp_pd_w9w9_uploaded = 'N' WHERE cp_pd_partner_id = "
					+ pid;
		} else if (fileRemarks.contains("Insurance")) {
			sql = "UPDATE dbo.cp_partner_details SET cp_pd_insurance = 'N', cp_pd_insurance_uploaded = 'N' WHERE cp_pd_partner_id = "
					+ pid;
		} else if (fileRemarks.contains("Resume")) {
			sql = "UPDATE dbo.cp_partner_details SET cp_pd_resume = 'N', cp_pd_resume_uploaded = 'N' WHERE cp_pd_partner_id = "
					+ pid;
		} else if (fileRemarks.contains("Background")) {
			sql = "UPDATE dbo.cp_partner_details SET cp_backcheck_status = 'N', cp_backcheck_status_uploaded = 'N' WHERE cp_pd_partner_id = "
					+ pid;
		} else if (fileRemarks.contains("Drug")) {
			sql = "UPDATE dbo.cp_partner_details SET cp_DrugScreen_Status = 'N', cp_DrugScreen_uploaded = 'N' WHERE cp_pd_partner_id = "
					+ pid;
		}

		try {
			conn = ds.getConnection();

			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();

		} catch (Exception e) {
			logger.error("getMinutePayrollId(String partnerid, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMinutePayrollId(String partnerid, DataSource) - Exception caught in Pvsdao.getCSVReportViewerStatus  :"
						+ e);
			}
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}

	}

	// /// set details in database from view screen
	public static int setDetails(String id, String partnerResponse,
			String cpdResearchNotes, String did, String incidentStatus,
			String userId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		String sql = "";
		int retvalue = 1;
		if (cpdResearchNotes.contains("'")) {

			cpdResearchNotes = cpdResearchNotes.replaceAll("'", "''");

		}
		if (partnerResponse.contains("'")) {

			partnerResponse = partnerResponse.replaceAll("'", "''");

		}
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "update cp_incident_report set cp_ir_cpd_notes='"
					+ cpdResearchNotes + "'" + "  , cp_ir_partner_response ='"
					+ partnerResponse + "'" + "  , cp_ir_incident_status ='"
					+ incidentStatus + "' " + "  , cp_ir_updated_by ='"
					+ userId + "'" + " where cp_ir_id= '" + did + "'"
					+ " and  cp_ir_partner_id =" + id;

			stmt.execute(sql);
		} catch (Exception e) {
			logger.error("setDetails(String, String, DataSource)", e);
			retvalue = 0;
			if (logger.isDebugEnabled()) {
				logger.debug("setDetails(String, String, DataSource) - Error occured in setOldPassword()  "
						+ e);
			}
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setDetails(String, String, DataSource) - exception ignored",
						e);
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setDetails(String, String, DataSource) - exception ignored",
						e);
			}
		}
		return retvalue;
	}

	public static int setupdateFileStatus(String id, String status, String pid,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "";
		int retvalue = 1;

		sql = "update lx_upload_file set lx_file_uploaded_status='" + status
				+ "'" + " where lx_file_used_in = '" + pid + "'"
				+ " and    lx_file_id =" + id;

		try {
			conn = ds.getConnection();

			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();

		} catch (Exception e) {
			logger.error("setupdateFileStatus(String partnerid, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setupdateFileStatus(String partnerid, DataSource) - Exception caught in Pvsdao.setupdateFileStatus  :"
						+ e);
			}
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return retvalue;

	}

	public static int savecp_technicianPartner(String pid, String techname,
			DataSource ds) {

		Connection conn = ReportsDao.getStaticConnection();

		CallableStatement cstmt = null;
		ResultSet rs = null;
		int cp_techId = 0;

		try {

			cstmt = conn
					.prepareCall("{call cp_Insert_Technician_details(?,?)}");
			cstmt.setString(1, techname);

			cstmt.setInt(2, Integer.parseInt(pid));

			rs = cstmt.executeQuery();
			while (rs.next()) {

				cp_techId = rs.getInt(1);

			}

		} catch (Exception e) {
			logger.error(
					"savecp_technicianPartner(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("savecp_technicianPartner(String, String, DataSource) - Error in coordinating partner information with the Web.");
			}
		} finally {

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"savecp_technicianPartner(String, String,  DataSource) - exception ignored",
						e);
			}

		}
		return cp_techId;
	}

	public static boolean isPartnerRestricted(String pId, String jobId,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		boolean val = true;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			StringBuffer SQL_QUERY = new StringBuffer();
			SQL_QUERY.append(" SELECT isnull(cp_rr_type, 1) cp_rr_type "
					+ " FROM dbo.cp_partner_recommend_restrict "
					+ " JOIN lx_appendix_main ON lx_pr_mm_id = cp_rr_mm_id "
					+ " JOIN lm_job ON lm_js_pr_id = lx_pr_id "
					+ " WHERE cp_partner_id = " + pId + "  AND lm_js_id = "
					+ jobId);

			rs = stmt.executeQuery(SQL_QUERY.toString());

			if (rs.next()) {
				val = rs.getBoolean("cp_rr_type");
			}
		} catch (Exception e) {
			logger.error("isPartnerRestricted(String, String, DataSource)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("isPartnerRestricted(String, String, DataSource)Exception caught"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"isPartnerRestricted(String, String, DataSource)exception ignored",
						sql);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"isPartnerRestricted(String, String, DataSource)exception ignored",
						sql);
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"isPartnerRestricted(String, String, DataSource)exception ignored",
						sql);
			}
		}
		return val;
	}

	// Data Correction of file status
	public static int updatecp_PartnerStatus(String pid, DataSource ds) {

		Connection conn = null;

		CallableStatement cstmt = null;
		ResultSet rs = null;
		int retval = 0;

		try {

			conn = ds.getConnection();
			cstmt = conn.prepareCall("{call sp_UpdateStatus(?)}");
			cstmt.setString(1, pid);
			cstmt.executeUpdate();

		} catch (Exception e) {
			logger.error("updatecp_PartnerStatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatecp_PartnerStatus(String, DataSource) - Error in coordinating partner information with the Web.");
			}
		} finally {

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"updatecp_PartnerStatus(String,  DataSource) - exception ignored",
						e);
			}

		}
		return retval;
	}

	// List of add & delete from history table
	public static ArrayList<RecRest_History> getRecommendedRestrictedPartnerlog(
			String pId, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<RecRest_History> key = new ArrayList<RecRest_History>();
		int i = 0;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = " SELECT DISTINCT  lo_ot_name, cp_rr_type,cp_rr_created_date,(lo_pc_first_name+' '+lo_pc_last_name)cp_rr_created_by,activity FROM  cp_partner_recommend_restrict_history "
					+ "   JOIN lp_msa_main ON lp_msa_main.lp_mm_id = cp_rr_mm_id "
					+ "   JOIN lo_organization_top ON lo_ot_id = lp_mm_ot_id "
					+ "   JOIN lo_poc ON  lo_pc_id=cp_rr_created_by "
					+ "    and cp_partner_id = "
					+ pId
					+ "   ORDER BY cp_rr_created_date ASC  ";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				RecRest_History log = new RecRest_History();
				String name = rs.getString("lo_ot_name");
				log.setName(name);
				String type = rs.getString("cp_rr_type");
				log.setType(type);
				String createduser = rs.getString("cp_rr_created_date");
				Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
						.parse(createduser);
				String dateadd = new SimpleDateFormat("MM/dd/yyyy HH:mm")
						.format(date);
				log.setCreatedUser(dateadd.toString());
				String dateby = rs.getString("cp_rr_created_by");
				log.setDate(dateby);
				String activity = rs.getString("activity");
				log.setActivity(activity);
				key.add(log);
				i++;
			}
		} catch (Exception e) {
			logger.error("getRecommendedRestrictedPartnerlog( DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRecommendedRestrictedPartnerlog(DataSource) - Error occured during getting getRecommendedRestrictedPartnerlog  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getRecommendedRestrictedPartnerlog(DataSource) - exception ignored",
						e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getRecommendedRestrictedPartnerlog(DataSource) - exception ignored",
						e);

			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getRecommendedRestrictedPartnerlog(DataSource) - exception ignored",
						e);

			}

		}
		return key;
	}

	// check for Insert status
	public static int checkpartnerInsert(String pid, String type, String mmId,
			String prId, String createdBy, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		int retval = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{call sp_cp_partner_Insert( ? , ? , ? , ? , ? )}");
			cstmt.setString(1, pid);
			cstmt.setString(2, type);
			cstmt.setString(3, mmId);
			cstmt.setString(4, prId);
			cstmt.setString(5, createdBy);
			cstmt.execute();

		} catch (Exception e) {
			logger.error(
					"checkpartnerInsert(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkpartnerInsert(String, String, String, String, String, DataSource) - Error occured during assigning partner "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"checkpartnerInsert(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"checkpartnerInsert(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"checkpartnerInsert(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retval;

	}

	// / check delete check
	public static int checkpartnerDelete(String pid, String type, String mmid,
			DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		int retval = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{call sp_cp_partner_Delete( ? , ?, ?  )}");
			cstmt.setString(1, pid);
			cstmt.setString(2, type);
			cstmt.setString(3, mmid);
			cstmt.executeUpdate();

		} catch (Exception e) {
			logger.error(
					"checkpartnerDelete(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkpartnerDelete(String, String, String, String, String, DataSource) - Error occured during assigning partner "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"checkpartnerDelete(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"checkpartnerDelete(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"checkpartnerDelete(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retval;

	}

	/**
	 * An alternate of the method getCountryStateList and
	 * SiteManagedao.getCategoryWiseStateList to reduce the database calls.
	 * 
	 * @param ds
	 * @return ArrayList, list of countries and its states.
	 */
	public static ArrayList getCountryStateListMap(DataSource ds) {
		ArrayList<LabelValue> list = new ArrayList();
		ArrayList valuelist = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList tempList = new ArrayList();
		Collection alllist = new ArrayList();
		ArrayList arrayList = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select  cp_state_id , cp_country_id, cp_country_name  from  cp_state ORDER BY  cp_country_id");
			while (rs.next()) {

				if (rs.getString("cp_country_id") != null
						&& (!"".equals(rs.getString("cp_country_id")))) {
					list.add(new LabelValue(rs.getString("cp_country_id") + "-"
							+ rs.getString("cp_country_name"), rs
							.getString("cp_state_id")));
				}
			}

			HashMap map = new HashMap();

			for (LabelValue lv : list) {
				String cIdName = lv.getLabel();
				String[] countries = cIdName.split("-");
				String countryId = countries[0];

				String countryName = countries[1];
				String stateId = lv.getValue();
				if (!map.containsKey(countryId)) {
					Partner_EditBean PVSForm2 = new Partner_EditBean();
					PVSForm2.setSiteCountryId(countryId);
					PVSForm2.setSiteCountryIdName(countryName);
					tempList = new ArrayList();
					tempList.add(new LabelValue(stateId, stateId + ","
							+ countryId));
					PVSForm2.setTempList(tempList);
					map.put(countryId, PVSForm2);
				} else {
					Partner_EditBean bean = (Partner_EditBean) map
							.get(countryId);
					Collection lst = bean.getTempList();
					lst.add(new LabelValue(stateId, stateId + "," + countryId));
				}

			}
			// Add US on top of the List
			Partner_EditBean usStatesBean = (Partner_EditBean) map.get("US");
			map.remove("US");

			alllist = map.values();
			arrayList = new ArrayList(alllist);
			// adding the default initial value in the list
			Partner_EditBean partnerEditBean = new Partner_EditBean();
			partnerEditBean.setSiteCountryId("");
			partnerEditBean.setSiteCountryIdName("");
			List newlist = new ArrayList();
			newlist.add(new LabelValue("--Select--", "0"));
			partnerEditBean.setTempList(newlist);

			arrayList.add(0, partnerEditBean);
			arrayList.add(1, usStatesBean);
		} catch (Exception e) {
			logger.error("getCountryStateListMap(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCountryStateListMap(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getCountryStateListMap(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCountryStateListMap(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCountryStateListMap(DataSource) - exception ignored",
						sql);
			}

		}
		return arrayList;
	}

	public static void saveSecurityCertifications(String techid,
			String engineerName, String drugScreenCert,
			String criminalBackgroundCert, String harassmentCert,
			String drugScreenCertDate, String criminalBackgroundCertDate,
			String harassmentCertDate, DataSource ds) {
		Connection conn = null;
		PreparedStatement stmt = null;
		Statement stmt1 = null;
		ResultSet rs = null;
		String drugScreenFileId = null;
		String criminalBackgroundFileId = null;
		try {
			conn = ds.getConnection();
			if (!(techid == null || "".equals(techid) || "0".equals(techid))) {
				String sqlc = "SELECT 1 FROM cp_technician_security_certification WHERE cp_sc_tech_id="
						+ techid;
				stmt1 = conn.createStatement();
				rs = stmt1.executeQuery(sqlc);
				if (rs.next()) {
					String sqlupdate = "UPDATE dbo.cp_technician_security_certification "
							+ " SET cp_sc_drug_screening_cert="
							+ drugScreenCert
							+ ", cp_sc_criminal_bgd_cert="
							+ criminalBackgroundCert
							+ ", cp_sc_harassment_cert="
							+ harassmentCert
							+ ", cp_sc_drug_screening_cert_date='"
							+ drugScreenCertDate
							+ "', cp_sc_criminal_bgd_cert_date='"
							+ criminalBackgroundCertDate
							+ "', cp_sc_harassment_cert_date='"
							+ harassmentCertDate
							+ "' WHERE cp_sc_tech_id ="
							+ techid;
					stmt = conn.prepareStatement(sqlupdate);
					stmt.executeUpdate();
				}
			} else {
				techid = getTechnicianId(engineerName, ds);
				String sql = "insert into cp_technician_security_certification "
						+ "(cp_sc_tech_id, cp_sc_drug_screening_cert,cp_sc_criminal_bgd_cert, cp_sc_harassment_cert, cp_sc_drug_screening_cert_date, cp_sc_criminal_bgd_cert_date,cp_sc_harassment_cert_date,drug_screening_cert_file_id, criminal_bgd_cert_file_id) "
						+ " VALUES("
						+ techid
						+ ","
						+ drugScreenCert
						+ ","
						+ criminalBackgroundCert
						+ ","
						+ harassmentCert
						+ ",'"
						+ drugScreenCertDate
						+ "','"
						+ criminalBackgroundCertDate
						+ "','"
						+ harassmentCertDate
						+ "',"
						+ drugScreenFileId
						+ ","
						+ criminalBackgroundFileId + ");";
				stmt = conn.prepareStatement(sql);
				boolean insertResult = stmt.execute();
			}
		} catch (Exception e) {
			logger.error("saveSecurityCertifications(DataSource)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("saveSecurityCertifications(DataSource) - Exception caught"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"saveSecurityCertifications(DataSource) - exception ignored",
						sql);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"saveSecurityCertifications(DataSource) - exception ignored",
						sql);
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"saveSecurityCertifications(DataSource) - exception ignored",
						sql);
			}
		}
	}

	public static String saveSecurityFiles(String technicianId,
			String partnerCandidate, String fileType, String fileName,
			String fileRemarks, byte[] in1, String userid, String uploadStatus,
			DataSource ds) {
		Connection conn = null;
		CallableStatement stmt = null;
		String fileId = null;
		ResultSet rs = null;
		int retVal = 0;
		PreparedStatement pstatement = null;
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn
					.prepareCall("{?=call cp_partner_certificate_upload_02(?,?,?,?,?,?,?,?)}");
			stmt.registerOutParameter(1, Types.INTEGER);
			stmt.setInt(2, Integer.parseInt(technicianId));
			stmt.setInt(3, Integer.parseInt(partnerCandidate));
			stmt.setString(4, fileType);
			stmt.setString(5, fileName);
			stmt.setString(6, fileRemarks);
			stmt.setBytes(7, in1);
			stmt.setInt(8, Integer.parseInt(userid));
			stmt.setString(9, uploadStatus);
			stmt.execute();
			retVal = stmt.getInt(1);
			if ("Drug Screen Certification".equals(fileRemarks)) {
				sql = "UPDATE dbo.cp_technician_security_certification "
						+ " SET drug_screening_cert_file_id = " + retVal
						+ " WHERE cp_sc_tech_id = " + technicianId;
			} else {
				sql = "UPDATE dbo.cp_technician_security_certification "
						+ " SET criminal_bgd_cert_file_id = " + retVal
						+ " WHERE cp_sc_tech_id = " + technicianId;
			}
			pstatement = conn.prepareStatement(sql);
			pstatement.executeUpdate();
		} catch (Exception e) {
			logger.error("saveSecurityFiles(DataSource)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("saveSecurityFiles(DataSource) - Exception caught"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sqql) {
				logger.warn(
						"saveSecurityFiles(DataSource) - exception ignored",
						sqql);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sqql) {
				logger.warn(
						"saveSecurityFiles(DataSource) - exception ignored",
						sqql);
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqql) {
				logger.warn(
						"saveSecurityFiles(DataSource) - exception ignored",
						sqql);
			}
		}
		return fileId;
	}

	public static String getFileId(String partner_id, String tech_id,
			String fileType, DataSource dataSource) {
		String sql = "";
		Connection conn = null;
		Statement stmt = null;
		String fileId = null;
		ResultSet rs = null;
		if (fileType != null) {
			if ("DrugScreenCertificate".equals(fileType)) {
				sql = "select drug_screening_cert_file_id as file_id from cp_technician_security_certification where cp_sc_tech_id="
						+ tech_id;
			} else {
				sql = "select criminal_bgd_cert_file_id as file_id from cp_technician_security_certification where cp_sc_tech_id="
						+ tech_id;
			}
		}
		try {
			conn = dataSource.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				fileId = rs.getString("file_id");
			}
		} catch (Exception e) {
			logger.error("getFileId(DataSource)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("getFileId(DataSource) - Exception caught" + e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sqql) {
				logger.warn("getFileId(DataSource) - exception ignored", sqql);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sqql) {
				logger.warn("getFileId(DataSource) - exception ignored", sqql);
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqql) {
				logger.warn("getFileId(DataSource) - exception ignored", sqql);
			}
		}
		return fileId;
	}

	public static void saveFiles(String partnerId, String fileType,
			String fileName, String fileRemarks, byte[] filedata,
			String userid, String uploadStatus, DataSource ds) {
		Connection conn = null;
		CallableStatement stmt = null;
		String fileId = null;
		ResultSet rs = null;
		int retVal = 0;
		PreparedStatement pstatement = null;
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn
					.prepareCall("{?=call cp_partner_certificate_upload_02(?,?,?,?,?,?,?,?)}");
			stmt.registerOutParameter(1, Types.INTEGER);
			stmt.setInt(2, 0);
			stmt.setInt(3, Integer.parseInt(partnerId));
			stmt.setString(4, fileType);
			stmt.setString(5, fileName);
			stmt.setString(6, fileRemarks);
			stmt.setBytes(7, filedata);
			stmt.setInt(8, Integer.parseInt(userid));
			stmt.setString(9, uploadStatus);
			stmt.execute();
			retVal = stmt.getInt(1);
			if ("Drug Screen".equals(fileRemarks)) {
				sql = "Update cp_partner_details set cp_DrugScreen_status='y', cp_DrugScreen_uploaded='y' where cp_pd_id= "
						+ partnerId;
			} else {
				sql = "Update cp_partner_details set cp_backcheck_status='y', cp_backcheck_status_uploaded='y' where cp_pd_id= "
						+ partnerId;
			}
			pstatement = conn.prepareStatement(sql);
			pstatement.executeUpdate();
		} catch (Exception e) {
			logger.error("saveFiles(DataSource)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("saveFiles(DataSource) - Exception caught" + e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sqql) {
				logger.warn("saveFiles(DataSource) - exception ignored", sqql);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sqql) {
				logger.warn("saveFiles(DataSource) - exception ignored", sqql);
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqql) {
				logger.warn("saveFiles(DataSource) - exception ignored", sqql);
			}
		}
	}

	public static String getFileId(String partner_id, String fileType,
			DataSource dataSource) {
		String sql = "";
		Connection conn = null;
		Statement stmt = null;
		String fileId = null;
		ResultSet rs = null;
		if (fileType != null) {
			if ("DrugScreen".equals(fileType)) {
				fileType = "Drug Screen";
			} else {
				fileType = "Background Check";
			}
			sql = "SELECT TOP 1 lx_file_id  FROM lx_upload_file WHERE lx_file_used_in="
					+ partner_id
					+ " AND lx_file_remarks LIKE '"
					+ fileType
					+ "' ORDER BY lx_file_uploaded_date desc";
		}
		try {
			conn = dataSource.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				fileId = rs.getString("lx_file_id");
			}
		} catch (Exception e) {
			logger.error("getFileId(DataSource)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("getFileId(DataSource) - Exception caught" + e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sqql) {
				logger.warn("getFileId(DataSource) - exception ignored", sqql);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sqql) {
				logger.warn("getFileId(DataSource) - exception ignored", sqql);
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqql) {
				logger.warn("getFileId(DataSource) - exception ignored", sqql);
			}
		}
		return fileId;
	}

	private static String getTechnicianId(String engineerName, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		Statement stmt1 = null;
		ResultSet rs = null;
		String drugScreenFileId = null;
		String criminalBackgroundFileId = null;
		String technicianId = null;
		try {
			conn = ds.getConnection();
			String sql = "SELECT TOP 1 cp_tech_id FROM cp_technicians WHERE cp_tech_name='"
					+ engineerName + "' ORDER BY cp_tech_id desc";
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				technicianId = rs.getString("cp_tech_id");
			}
		} catch (Exception e) {
			logger.error("getTechnicianId(DataSource)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("getTechnicianId(DataSource) - Exception caught"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sqql) {
				logger.warn("getTechnicianId(DataSource) - exception ignored",
						sqql);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sqql) {
				logger.warn("getTechnicianId(DataSource) - exception ignored",
						sqql);
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqql) {
				logger.warn("getTechnicianId(DataSource) - exception ignored",
						sqql);
			}
		}
		return technicianId;
	}
}
