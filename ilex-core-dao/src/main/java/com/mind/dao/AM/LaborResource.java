/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: A bean containing getter/setter methods for labor resource 
*
*/


package com.mind.dao.AM;

public class LaborResource 
{
	private String activity_Id = null;
	private String laborid = null;
	private String laborcostlibid = null;
	private String laborname = null;
	private String labortype = null;
	
	
	private String cnspartnumber = null;
	
	private String quantityhours = null;
	private String prevquantityhours = null;
	
	private String estimatedhourlybasecost = null;
	
	
	private String estimatedtotalcost = null;
	private String proformamargin = null;
	private String priceunit = null;
	private String priceextended = null;
	
	private String sellablequantity = null;
	private String minimumquantity = null;
	private String status = null;
	private String subcategory = null;
	
	private String addendum_id = null;
	private String flag = null; // for resource from temporary table or not
	
	//	 for in transit field
	private String transit = null;
	private String checktransit = null;
	
	private String flag_laborid = null; //for setting checkbox checked
	
	private String clr_detail_id = null;
	
	private String activity_name = null;
	private String msa_Id = null;
	private String msaname = null;
	
	
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String activity_Id ) {
		this.activity_Id = activity_Id;
	}
	
	
	public String getCnspartnumber() {
		return cnspartnumber;
	}
	public void setCnspartnumber( String cnspartnumber ) {
		this.cnspartnumber = cnspartnumber;
	}
	
	
	public String getEstimatedhourlybasecost() {
		return estimatedhourlybasecost;
	}
	public void setEstimatedhourlybasecost( String estimatedhourlybasecost ) {
		this.estimatedhourlybasecost = estimatedhourlybasecost;
	}
	
	
	public String getEstimatedtotalcost() {
		return estimatedtotalcost;
	}
	public void setEstimatedtotalcost( String estimatedtotalcost ) {
		this.estimatedtotalcost = estimatedtotalcost;
	}
	
	
	public String getLaborid() {
		return laborid;
	}
	public void setLaborid( String laborid ) {
		this.laborid = laborid;
	}
	
	
	public String getLaborcostlibid() {
		return laborcostlibid;
	}
	public void setLaborcostlibid( String laborcostlibid ) {
		this.laborcostlibid = laborcostlibid;
	}
	
	
	public String getLaborname() {
		return laborname;
	}
	public void setLaborname( String laborname ) {
		this.laborname = laborname;
	}
	
	
	public String getLabortype() {
		return labortype;
	}
	public void setLabortype( String labortype ) {
		this.labortype = labortype;
	}
	
	
	public String getMinimumquantity() {
		return minimumquantity;
	}
	public void setMinimumquantity( String minimumquantity ) {
		this.minimumquantity = minimumquantity;
	}
	
	
	public String getPriceextended() {
		return priceextended;
	}
	public void setPriceextended( String priceextended ) {
		this.priceextended = priceextended;
	}
	
	
	public String getPriceunit() {
		return priceunit;
	}
	public void setPriceunit( String priceunit ) {
		this.priceunit = priceunit;
	}
	
	
	public String getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin( String proformamargin ) {
		this.proformamargin = proformamargin;
	}
	
	
	public String getQuantityhours() {
		return quantityhours;
	}
	public void setQuantityhours( String quantityhours ) {
		this.quantityhours = quantityhours;
	}
	
	
	public String getPrevquantityhours() {
		return prevquantityhours;
	}
	public void setPrevquantityhours( String prevquantityhours ) {
		this.prevquantityhours = prevquantityhours;
	}
	
	
	public String getSellablequantity() {
		return sellablequantity;
	}
	public void setSellablequantity( String sellablequantity ) {
		this.sellablequantity = sellablequantity;
	}
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus( String status ) {
		this.status = status;
	}
	
	
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id( String addendum_id ) {
		this.addendum_id = addendum_id;
	}
	
	
	public String getSubcategory() {
		return subcategory;
	}
	public void setSubcategory( String subcategory ) {
		this.subcategory = subcategory;
	}
	public String getTransit() {
		return transit;
	}
	public void setTransit(String transit) {
		this.transit = transit;
	}
	public String getChecktransit() {
		return checktransit;
	}
	public void setChecktransit(String checktransit) {
		this.checktransit = checktransit;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getFlag_laborid() {
		return flag_laborid;
	}
	public void setFlag_laborid(String flag_laborid) {
		this.flag_laborid = flag_laborid;
	}
	public String getClr_detail_id() {
		return clr_detail_id;
	}
	public void setClr_detail_id(String clr_detail_id) {
		this.clr_detail_id = clr_detail_id;
	}
	public String getActivity_name() {
		return activity_name;
	}
	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}
	public String getMsa_Id() {
		return msa_Id;
	}
	public void setMsa_Id(String msa_Id) {
		this.msa_Id = msa_Id;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	
	
	
}