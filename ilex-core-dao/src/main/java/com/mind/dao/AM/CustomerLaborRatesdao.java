package com.mind.dao.AM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.CustomerLaborRateBean;

public class CustomerLaborRatesdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(CustomerLaborRatesdao.class);

	public static ArrayList getCLRates(String proj_id, String catid,
			int total_res, DataSource ds) {
		ArrayList clrList = new ArrayList();
		CustomerLaborRateBean clrBean;
		DecimalFormat dfcur = new DecimalFormat("###0.00");

		int val = 0;
		int i = 0;
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		// String temp = "";
		int temp = 4;

		int technumbers = 0;
		int engnumbers = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call dbo.iv_clr_tab_02(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, proj_id);
			cstmt.setString(3, catid);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				clrBean = new CustomerLaborRateBean();
				// CLRForm.setCriticalityid( rs.getString ( "Activity_id" ) );
				clrBean.setCriticalityname(rs.getString("clr_crit_name"));
				clrBean.setCriticalitydesc(rs.getString("clr_crit_desc"));
				clrBean.setServicetime(rs.getString("clr_service_time"));
				clrBean.setCategoryshortname(rs.getString("clr_cat_short_name"));
				clrBean.setCategoryname(rs.getString("clr_cat_name"));
				clrBean.setCatid(rs.getString("clr_cat_id"));

				if (total_res >= 1) {
					clrBean.setClrrate0(dfcur.format(rs
							.getFloat("clr_labor_rate0")) + "");

					clrBean.setDetailid0(rs.getString("clr_detail_id0"));
					clrBean.setResourceid0(rs.getString("iv_rp_id0"));
					clrBean.setResourcename0(rs.getString("iv_rp_name0"));
				}

				if (total_res >= 2) {
					clrBean.setClrrate1(dfcur.format(rs
							.getFloat("clr_labor_rate1")) + "");
					clrBean.setDetailid1(rs.getString("clr_detail_id1"));
					clrBean.setResourceid1(rs.getString("iv_rp_id1"));
					clrBean.setResourcename1(rs.getString("iv_rp_name1"));
				}

				if (total_res >= 3) {
					clrBean.setClrrate2(dfcur.format(rs
							.getFloat("clr_labor_rate2")) + "");
					clrBean.setDetailid2(rs.getString("clr_detail_id2"));
					clrBean.setResourceid2(rs.getString("iv_rp_id2"));
					clrBean.setResourcename2(rs.getString("iv_rp_name2"));
				}

				if (total_res >= 4) {
					clrBean.setClrrate3(dfcur.format(rs
							.getFloat("clr_labor_rate3")) + "");
					clrBean.setDetailid3(rs.getString("clr_detail_id3"));
					clrBean.setResourceid3(rs.getString("iv_rp_id3"));
					clrBean.setResourcename3(rs.getString("iv_rp_name3"));
				}

				if (total_res >= 5) {
					clrBean.setClrrate4(dfcur.format(rs
							.getFloat("clr_labor_rate4")) + "");
					clrBean.setDetailid4(rs.getString("clr_detail_id4"));
					clrBean.setResourceid4(rs.getString("iv_rp_id4"));
					clrBean.setResourcename4(rs.getString("iv_rp_name4"));
				}

				if (total_res >= 6) {
					clrBean.setClrrate5(dfcur.format(rs
							.getFloat("clr_labor_rate5")) + "");
					clrBean.setDetailid5(rs.getString("clr_detail_id5"));
					clrBean.setResourceid5(rs.getString("iv_rp_id5"));
					clrBean.setResourcename5(rs.getString("iv_rp_name5"));
				}

				if (total_res >= 7) {
					clrBean.setClrrate6(dfcur.format(rs
							.getFloat("clr_labor_rate6")) + "");
					clrBean.setDetailid6(rs.getString("clr_detail_id6"));
					clrBean.setResourceid6(rs.getString("iv_rp_id6"));
					clrBean.setResourcename6(rs.getString("iv_rp_name6"));
				}

				if (total_res >= 8) {
					clrBean.setClrrate7(dfcur.format(rs
							.getFloat("clr_labor_rate7")) + "");
					clrBean.setDetailid7(rs.getString("clr_detail_id7"));
					clrBean.setResourceid7(rs.getString("iv_rp_id7"));
					clrBean.setResourcename7(rs.getString("iv_rp_name7"));
				}

				if (total_res >= 9) {
					clrBean.setClrrate8(dfcur.format(rs
							.getFloat("clr_labor_rate8")) + "");
					clrBean.setDetailid8(rs.getString("clr_detail_id8"));
					clrBean.setResourceid8(rs.getString("iv_rp_id8"));
					clrBean.setResourcename8(rs.getString("iv_rp_name8"));
				}

				if (total_res >= 10) {
					clrBean.setClrrate9(dfcur.format(rs
							.getFloat("clr_labor_rate9")) + "");
					clrBean.setDetailid9(rs.getString("clr_detail_id9"));
					clrBean.setResourceid9(rs.getString("iv_rp_id9"));
					clrBean.setResourcename9(rs.getString("iv_rp_name9"));
				}

				if (total_res >= 11) {
					clrBean.setClrrate10(dfcur.format(rs
							.getFloat("clr_labor_rate10")) + "");
					clrBean.setDetailid10(rs.getString("clr_detail_id10"));
					clrBean.setResourceid10(rs.getString("iv_rp_id10"));
					clrBean.setResourcename10(rs.getString("iv_rp_name10"));
				}

				if (total_res >= 12) {
					clrBean.setClrrate11(dfcur.format(rs
							.getFloat("clr_labor_rate11")) + "");
					clrBean.setDetailid11(rs.getString("clr_detail_id11"));
					clrBean.setResourceid11(rs.getString("iv_rp_id11"));
					clrBean.setResourcename11(rs.getString("iv_rp_name11"));
				}

				if (total_res >= 13) {
					clrBean.setClrrate12(dfcur.format(rs
							.getFloat("clr_labor_rate12")) + "");
					clrBean.setDetailid12(rs.getString("clr_detail_id12"));
					clrBean.setResourceid12(rs.getString("iv_rp_id12"));
					clrBean.setResourcename12(rs.getString("iv_rp_name12"));
				}

				if (total_res >= 14) {
					clrBean.setClrrate13(dfcur.format(rs
							.getFloat("clr_labor_rate13")) + "");
					clrBean.setDetailid13(rs.getString("clr_detail_id13"));
					clrBean.setResourceid13(rs.getString("iv_rp_id13"));
					clrBean.setResourcename13(rs.getString("iv_rp_name13"));
				}

				if (total_res >= 15) {
					clrBean.setClrrate14(dfcur.format(rs
							.getFloat("clr_labor_rate14")) + "");
					clrBean.setDetailid14(rs.getString("clr_detail_id14"));
					clrBean.setResourceid14(rs.getString("iv_rp_id14"));
					clrBean.setResourcename14(rs.getString("iv_rp_name14"));
				}

				if (total_res >= 16) {
					clrBean.setClrrate15(dfcur.format(rs
							.getFloat("clr_labor_rate15")) + "");
					clrBean.setDetailid15(rs.getString("clr_detail_id15"));
					clrBean.setResourceid15(rs.getString("iv_rp_id15"));
					clrBean.setResourcename15(rs.getString("iv_rp_name15"));
				}
				if (total_res >= 17) {
					clrBean.setClrrate16(dfcur.format(rs
							.getFloat("clr_labor_rate16")) + "");
					clrBean.setDetailid16(rs.getString("clr_detail_id16"));
					clrBean.setResourceid16(rs.getString("iv_rp_id16"));
					clrBean.setResourcename16(rs.getString("iv_rp_name16"));
				}
				if (total_res >= 18) {
					clrBean.setClrrate17(dfcur.format(rs
							.getFloat("clr_labor_rate17")) + "");
					clrBean.setDetailid17(rs.getString("clr_detail_id17"));
					clrBean.setResourceid17(rs.getString("iv_rp_id17"));
					clrBean.setResourcename17(rs.getString("iv_rp_name17"));
				}
				if (total_res >= 19) {
					clrBean.setClrrate18(dfcur.format(rs
							.getFloat("clr_labor_rate18")) + "");
					clrBean.setDetailid18(rs.getString("clr_detail_id18"));
					clrBean.setResourceid18(rs.getString("iv_rp_id18"));
					clrBean.setResourcename18(rs.getString("iv_rp_name18"));
				}
				if (total_res >= 20) {
					clrBean.setClrrate19(dfcur.format(rs
							.getFloat("clr_labor_rate19")) + "");
					clrBean.setDetailid19(rs.getString("clr_detail_id19"));
					clrBean.setResourceid19(rs.getString("iv_rp_id19"));
					clrBean.setResourcename19(rs.getString("iv_rp_name19"));
				}
				if (total_res >= 21) {
					clrBean.setClrrate20(dfcur.format(rs
							.getFloat("clr_labor_rate20")) + "");
					clrBean.setDetailid20(rs.getString("clr_detail_id20"));
					clrBean.setResourceid20(rs.getString("iv_rp_id20"));
					clrBean.setResourcename20(rs.getString("iv_rp_name20"));
				}
				if (total_res >= 22) {
					clrBean.setClrrate21(dfcur.format(rs
							.getFloat("clr_labor_rate21")) + "");
					clrBean.setDetailid21(rs.getString("clr_detail_id21"));
					clrBean.setResourceid21(rs.getString("iv_rp_id21"));
					clrBean.setResourcename21(rs.getString("iv_rp_name21"));
				}
				if (total_res >= 23) {
					clrBean.setClrrate22(dfcur.format(rs
							.getFloat("clr_labor_rate22")) + "");
					clrBean.setDetailid22(rs.getString("clr_detail_id22"));
					clrBean.setResourceid22(rs.getString("iv_rp_id22"));
					clrBean.setResourcename22(rs.getString("iv_rp_name22"));
				}
				if (total_res >= 24) {
					clrBean.setClrrate23(dfcur.format(rs
							.getFloat("clr_labor_rate23")) + "");
					clrBean.setDetailid23(rs.getString("clr_detail_id23"));
					clrBean.setResourceid23(rs.getString("iv_rp_id23"));
					clrBean.setResourcename23(rs.getString("iv_rp_name23"));
				}
				if (total_res >= 25) {
					clrBean.setClrrate24(dfcur.format(rs
							.getFloat("clr_labor_rate24")) + "");
					clrBean.setDetailid24(rs.getString("clr_detail_id24"));
					clrBean.setResourceid24(rs.getString("iv_rp_id24"));
					clrBean.setResourcename24(rs.getString("iv_rp_name24"));
				}
				if (total_res >= 26) {
					clrBean.setClrrate25(dfcur.format(rs
							.getFloat("clr_labor_rate25")) + "");
					clrBean.setDetailid25(rs.getString("clr_detail_id25"));
					clrBean.setResourceid25(rs.getString("iv_rp_id25"));
					clrBean.setResourcename25(rs.getString("iv_rp_name25"));
				}
				if (total_res >= 27) {
					clrBean.setClrrate26(dfcur.format(rs
							.getFloat("clr_labor_rate26")) + "");
					clrBean.setDetailid26(rs.getString("clr_detail_id26"));
					clrBean.setResourceid26(rs.getString("iv_rp_id26"));
					clrBean.setResourcename26(rs.getString("iv_rp_name26"));
				}
				if (total_res >= 28) {
					clrBean.setClrrate27(dfcur.format(rs
							.getFloat("clr_labor_rate27")) + "");
					clrBean.setDetailid27(rs.getString("clr_detail_id27"));
					clrBean.setResourceid27(rs.getString("iv_rp_id27"));
					clrBean.setResourcename27(rs.getString("iv_rp_name27"));
				}
				if (total_res >= 29) {
					clrBean.setClrrate28(dfcur.format(rs
							.getFloat("clr_labor_rate28")) + "");
					clrBean.setDetailid28(rs.getString("clr_detail_id28"));
					clrBean.setResourceid28(rs.getString("iv_rp_id28"));
					clrBean.setResourcename28(rs.getString("iv_rp_name28"));
				}
				if (total_res >= 30) {
					clrBean.setClrrate29(dfcur.format(rs
							.getFloat("clr_labor_rate29")) + "");
					clrBean.setDetailid29(rs.getString("clr_detail_id29"));
					clrBean.setResourceid29(rs.getString("iv_rp_id29"));
					clrBean.setResourcename29(rs.getString("iv_rp_name29"));
				}

				clrList.add(clrBean);
				i++;
			}
		} catch (Exception e) {
			logger.error("getCLRates(String, String, int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCLRates(String, String, int, DataSource) - Exception Caught in getCLRates"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error("getCLRates(String, String, int, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getCLRates(String, String, int, DataSource) - Exception Caught in getCLRates"
							+ e);
				}
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.error("getCLRates(String, String, int, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getCLRates(String, String, int, DataSource) - Exception Caught in getCLRates"
							+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.error("getCLRates(String, String, int, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getCLRates(String, String, int, DataSource) - Exception Caught in getCLRates"
							+ e);
				}
			}

		}
		return clrList;
	}

	public static ArrayList getTechEngtypes(String masid, DataSource ds) {

		ArrayList classtypelist = new ArrayList();
		CustomerLaborRateBean CLRForm = null;
		// CLRForm = new CustomerLaborRateBean();
		Connection conn = null;
		Statement stmt = null;
		String p[] = { "0", "0" };
		int i = 0;
		String sql = "";
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select * from dbo.func_clr_class_type(" + masid + ")";
			rs = stmt.executeQuery(sql);
			String no = "0";
			while (rs.next()) {
				CLRForm = new CustomerLaborRateBean();
				CLRForm.setClassname(rs.getString("clr_class_name"));
				CLRForm.setClassnums(rs.getString("num"));
				classtypelist.add(CLRForm);

			}

		} catch (Exception e) {
			logger.error("getTechEngtypes(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTechEngtypes(String, DataSource) - Exception Caught in setTechEngNos"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error("getTechEngtypes(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getTechEngtypes(String, DataSource) - Exception Caught in setTechEngNos"
							+ e);
				}
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error("getTechEngtypes(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getTechEngtypes(String, DataSource) - Exception Caught in setTechEngNos"
							+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.error("getTechEngtypes(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getTechEngtypes(String, DataSource) - Exception Caught in setTechEngNos"
							+ e);
				}
			}

		}
		return classtypelist;
	}

	public static ArrayList getResourcelevellist(String masid, DataSource ds) {

		ArrayList resourcelist = new ArrayList();
		CustomerLaborRateBean CLRForm = null;
		// CLRForm = new CustomerLaborRateBean();
		Connection conn = null;
		Statement stmt = null;
		String p[] = { "0", "0" };
		int i = 0;
		String sql = "";
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select * from dbo.func_clr_reslevel(" + masid + ")";
			rs = stmt.executeQuery(sql);
			String no = "0";
			while (rs.next()) {
				CLRForm = new CustomerLaborRateBean();
				CLRForm.setResourcename(rs.getString("clr_reslevel_name"));
				resourcelist.add(CLRForm);

			}

		} catch (Exception e) {
			logger.error("getResourcelevellist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourcelevellist(String, DataSource) - Exception Caught in setTechEngNos"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error("getResourcelevellist(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getResourcelevellist(String, DataSource) - Exception Caught in setTechEngNos"
							+ e);
				}
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error("getResourcelevellist(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getResourcelevellist(String, DataSource) - Exception Caught in setTechEngNos"
							+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.error("getResourcelevellist(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getResourcelevellist(String, DataSource) - Exception Caught in setTechEngNos"
							+ e);
				}
			}

		}
		return resourcelist;
	}

	// public static int updateCLRates( String lp_mm_id,String
	// clr_detail_id_list,float min_fee,float travel,float travel_max,float
	// discount,String eff_date,String rules,String action,DataSource ds )
	public static int updateCLRates(String lp_mm_id, String clr_detail_id_list,
			float min_fee, String travel, float travel_max, float discount,
			String eff_date, String rules, String action, String netMedxId,
			DataSource ds) {
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.lm_clr_manage_02( ?,?,?,?,?,?,?,?,?,?) }");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, lp_mm_id);
			cstmt.setString(3, clr_detail_id_list);
			cstmt.setFloat(4, min_fee);
			cstmt.setString(5, travel);
			cstmt.setFloat(6, travel_max);
			cstmt.setFloat(7, discount);
			cstmt.setString(8, eff_date);
			cstmt.setString(9, rules);
			cstmt.setString(10, action);
			cstmt.setString(11, netMedxId);

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error(
					"updateCLRates(String, String, float, String, float, float, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateCLRates(String, String, float, String, float, float, String, String, String, String, DataSource) - Exception caught in adding updateCLRates"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"updateCLRates(String, String, float, String, float, float, String, String, String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("updateCLRates(String, String, float, String, float, float, String, String, String, String, DataSource) - Exception Caught in adding updateCLRates"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"updateCLRates(String, String, float, String, float, float, String, String, String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("updateCLRates(String, String, float, String, float, float, String, String, String, String, DataSource) - Exception Caught in adding updateCLRates"
							+ sql);
				}
			}

		}
		return val;
	}

	public static String getNetmedxorgid(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;

		int i = 0;
		String sql = "";
		String custid = "0";
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select lo_ot_id from lo_organization_top where lo_ot_name = 'NetMedx' and lo_ot_type='C'";
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				custid = rs.getString("lo_ot_id");
			}

		} catch (Exception e) {
			logger.error("getNetmedxorgid(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNetmedxorgid(DataSource) - Exception Caught in getNetmedxorgid"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error("getNetmedxorgid(DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getNetmedxorgid(DataSource) - Exception Caught in getNetmedxorgid"
							+ e);
				}
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error("getNetmedxorgid(DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getNetmedxorgid(DataSource) - Exception Caught in getNetmedxorgid"
							+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.error("getNetmedxorgid(DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getNetmedxorgid(DataSource) - Exception Caught in getNetmedxorgid"
							+ e);
				}
			}

		}
		return custid;
	}

	public static String getOrganizationtype(String id, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;

		int i = 0;
		String sql = "";
		String custname = "";
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select lo_ot_name from lo_organization_top where lo_ot_id="
					+ id;
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				custname = rs.getString("lo_ot_name");
			}

		} catch (Exception e) {
			logger.error("getOrganizationtype(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOrganizationtype(String, DataSource) - Exception Caught in getOrganizationtype"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error("getOrganizationtype(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getOrganizationtype(String, DataSource) - Exception Caught in getOrganizationtype"
							+ e);
				}
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error("getOrganizationtype(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getOrganizationtype(String, DataSource) - Exception Caught in getOrganizationtype"
							+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.error("getOrganizationtype(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getOrganizationtype(String, DataSource) - Exception Caught in getOrganizationtype"
							+ e);
				}
			}

		}
		return custname;
	}

	public static String getNetmedxcatgid(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;

		int i = 0;
		String sql = "";
		String netmedxcatgid = "0";
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select lm_al_cat_id from lm_activity_lib_category where lm_al_cat_name = 'NetMedx'";
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				netmedxcatgid = rs.getString("lm_al_cat_id");
			}

		} catch (Exception e) {
			logger.error("getNetmedxcatgid(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNetmedxcatgid(DataSource) - Exception Caught in getNetmedxcatgid"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error("getNetmedxcatgid(DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getNetmedxcatgid(DataSource) - Exception Caught in getNetmedxcatgid"
							+ e);
				}
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error("getNetmedxcatgid(DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getNetmedxcatgid(DataSource) - Exception Caught in getNetmedxcatgid"
							+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.error("getNetmedxcatgid(DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getNetmedxcatgid(DataSource) - Exception Caught in getNetmedxcatgid"
							+ e);
				}
			}

		}
		return netmedxcatgid;
	}

	/**
	 * This method is used to retrieve activity status
	 * 
	 * @param Id
	 *            id of activity
	 * @param ds
	 *            object of DataSource
	 * @return String
	 */

	public static String getActivitylibstatus(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String status = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lm_at_status from dbo.func_activity_library_tab('%' , '"
					+ Id + "' , '%' )";
			// System.out.println("SQLL"+sql);
			rs = stmt.executeQuery(sql);
			if (rs.next())
				status = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getActivitylibstatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivitylibstatus(String, DataSource) - Exception in retrieving getActivitylibstatus"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitylibstatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitylibstatus(String, DataSource) - Exception in retrieving getActivitylibstatus"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitylibstatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitylibstatus(String, DataSource) - Exception in retrieving getActivitylibstatus"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitylibstatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitylibstatus(String, DataSource) - Exception in retrieving getActivitylibstatus"
							+ sql);
				}
			}

		}
		return status;
	}

	/**
	 * This method is used to retrieve activity status
	 * 
	 * @param Id
	 *            id of activity
	 * @param ds
	 *            object of DataSource
	 * @return String
	 */

	public static String getActivitystatus(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String status = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			// String sql =
			// "select lm_at_status from dbo.func_lp_activity_tab('%' , '"+Id+"' , '%')";

			String sql = "select dbo.func_status_description(isnull(lm_at_status, 'I' )) from lm_activity where lm_at_id="
					+ Id;

			// System.out.println("SQLL"+sql);
			rs = stmt.executeQuery(sql);
			if (rs.next())
				status = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getActivitystatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivitystatus(String, DataSource) - Exception in retrieving Activity status"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitystatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitystatus(String, DataSource) - Exception in retrieving Activity status"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitystatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitystatus(String, DataSource) - Exception in retrieving Activity status"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitystatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitystatus(String, DataSource) - Exception in retrieving Activity status"
							+ sql);
				}
			}

		}
		return status;
	}

	public static String getJobid(DataSource ds, String Activity_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String jobid = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_at_js_id from lm_activity where lm_at_id="
					+ Activity_Id;
			// System.out.println("QUERY"+sql);
			rs = stmt.executeQuery(sql);

			if (rs.next())
				jobid = rs.getString("lm_at_js_id");
		}

		catch (Exception e) {
			logger.error("getJobid(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobid(DataSource, String) - Error Occured in retrieving jobid"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn("getJobid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn("getJobid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn("getJobid(DataSource, String) - exception ignored",
						s);
			}

		}
		return jobid;
	}

	public static String getAppendixname(DataSource ds, String Appendix_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String appendixname = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select dbo.func_cc_name('" + Appendix_Id + "', 'Appendix' )";
			rs = stmt.executeQuery(sql);
			if (rs.next())
				appendixname = rs.getString(1);

		}

		catch (Exception e) {
			logger.error("getAppendixname(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixname(DataSource, String) - Error Occured in retrieving appendixname"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getAppendixname(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixname(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixname(DataSource, String) - exception ignored",
						s);
			}

		}
		return appendixname;
	}

	public static String getAppendixid(DataSource ds, String Job_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		String appendixid = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lm_js_pr_id from lm_job where lm_js_id=" + Job_Id;
			// System.out.println("appendixid sql "+sql);

			rs = stmt.executeQuery(sql);

			if (rs.next())
				appendixid = rs.getString("lm_js_pr_id");

		}

		catch (Exception e) {
			logger.error("getAppendixid(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixid(DataSource, String) - Error Occured in retrieving appendixid"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getAppendixid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getAppendixid(DataSource, String) - exception ignored",
						s);
			}

		}
		return appendixid;
	}

	public static String getMsaid(DataSource ds, String Appendix_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		String msaid = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lx_pr_mm_id from lx_appendix_main where lx_pr_id="
					+ Appendix_Id;

			// System.out.println("msaid sql "+sql);
			rs = stmt.executeQuery(sql);

			if (rs.next())
				msaid = rs.getString("lx_pr_mm_id");

		}

		catch (Exception e) {
			logger.error("getMsaid(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsaid(DataSource, String) - Error Occured in retrieving msaid"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn("getMsaid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn("getMsaid(DataSource, String) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn("getMsaid(DataSource, String) - exception ignored",
						s);
			}

		}
		return msaid;
	}

	public static String getMsaname(DataSource ds, String MSA_Id) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String msaname = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select dbo.func_cc_name('" + MSA_Id + "', 'MSA' )";
			// System.out.println("msaname sql "+sql);
			rs = stmt.executeQuery(sql);

			if (rs.next())
				msaname = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getMsaname(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsaname(DataSource, String) - Error Occured in retrieving msaname"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getMsaname(DataSource, String) - exception ignored", s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getMsaname(DataSource, String) - exception ignored", s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getMsaname(DataSource, String) - exception ignored", s);
			}

		}
		return msaname;
	}

	public static int gettotalresources(String msaid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		int total = 0;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select * from func_clr_total_resources(" + msaid + ")";
			// System.out.println("QUERY"+sql);
			rs = stmt.executeQuery(sql);

			if (rs.next())
				total = rs.getInt(1);
		}

		catch (Exception e) {
			logger.error("gettotalresources(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("gettotalresources(String, DataSource) - Error Occured in retrieving gettotalresources"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"gettotalresources(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"gettotalresources(String, DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"gettotalresources(String, DataSource) - exception ignored",
						s);
			}

		}
		return total;
	}

	public static String[] getclrdetail(String msaid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		String[] p = new String[6];

		int i = 0;
		String sql = "";
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select * from dbo.func_clr_master_detail(" + msaid + ")";
			rs = stmt.executeQuery(sql);
			String no = "0";
			if (rs.next()) {

				p[0] = rs.getString("clr_min_fee");
				// System.out.println("p[0] "+p[0]);
				p[1] = rs.getString("clr_travel");
				// System.out.println("p[1] "+p[1]);
				p[2] = rs.getString("clr_travel_max");
				// System.out.println("p[2] "+p[2]);
				p[3] = rs.getString("clr_discount");
				// System.out.println("p[3] "+p[3]);
				p[4] = rs.getString("clr_from");
				// System.out.println("p[4] "+p[4]);
				p[5] = rs.getString("clr_rules");
				// System.out.println("p[5] "+p[5]);
			}
		} catch (Exception e) {
			logger.error("getclrdetail(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getclrdetail(String, DataSource) - Exception Caught in getclrdetail"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error("getclrdetail(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getclrdetail(String, DataSource) - Exception Caught in getclrdetail"
							+ e);
				}
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error("getclrdetail(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getclrdetail(String, DataSource) - Exception Caught in getclrdetail"
							+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.error("getclrdetail(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getclrdetail(String, DataSource) - Exception Caught in getclrdetail"
							+ e);
				}
			}

		}
		return p;
	}

	public static String getResourcename(String resid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;

		int i = 0;
		String sql = "";
		String custid = "0";
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select iv_res_name from iv_resource_pool where iv_res_id="
					+ resid;
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				custid = rs.getString("lo_ot_id");
			}

		} catch (Exception e) {
			logger.error("getResourcename(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourcename(String, DataSource) - Exception Caught in getNetmedxorgid"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error("getResourcename(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getResourcename(String, DataSource) - Exception Caught in getNetmedxorgid"
							+ e);
				}
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error("getResourcename(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getResourcename(String, DataSource) - Exception Caught in getNetmedxorgid"
							+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.error("getResourcename(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getResourcename(String, DataSource) - Exception Caught in getNetmedxorgid"
							+ e);
				}
			}

		}
		return custid;
	}

	public static ArrayList getDiscountedCLRates(String proj_id, String catid,
			int total_res, DataSource ds) {
		ArrayList CLRlist = new ArrayList();
		CustomerLaborRateBean CLRForm;
		DecimalFormat dfcur = new DecimalFormat("###0.00");

		int val = 0;
		int i = 0;
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		// String temp = "";
		int temp = 4;

		int technumbers = 0;
		int engnumbers = 0;
		String[] tech = new String[5];
		String[] engg = new String[5];
		String[] techrate = new String[5];
		String[] enggrate = new String[5];
		// System.out.println("----Msa_id clr---"+proj_id);
		try {
			conn = ds.getConnection();

			cstmt = conn.prepareCall("{?=call dbo.iv_clr_tab_03(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setString(2, proj_id);
			cstmt.setString(3, catid);

			rs = cstmt.executeQuery();

			while (rs.next()) {
				CLRForm = new CustomerLaborRateBean();
				// CLRForm.setCriticalityid( rs.getString ( "Activity_id" ) );
				CLRForm.setCriticalityname(rs.getString("clr_crit_name"));
				CLRForm.setCriticalitydesc(rs.getString("clr_crit_desc"));
				CLRForm.setServicetime(rs.getString("clr_service_time"));
				CLRForm.setCategoryshortname(rs.getString("clr_cat_short_name"));
				CLRForm.setCategoryname(rs.getString("clr_cat_name"));
				CLRForm.setCatid(rs.getString("clr_cat_id"));

				if (total_res >= 1) {
					CLRForm.setClrrate0(dfcur.format(rs
							.getFloat("clr_labor_rate0")) + "");

					CLRForm.setDetailid0(rs.getString("clr_detail_id0"));
					CLRForm.setResourceid0(rs.getString("iv_rp_id0"));
					CLRForm.setResourcename0(rs.getString("iv_rp_name0"));
				}

				if (total_res >= 2) {
					CLRForm.setClrrate1(dfcur.format(rs
							.getFloat("clr_labor_rate1")) + "");
					CLRForm.setDetailid1(rs.getString("clr_detail_id1"));
					CLRForm.setResourceid1(rs.getString("iv_rp_id1"));
					CLRForm.setResourcename1(rs.getString("iv_rp_name1"));
				}

				if (total_res >= 3) {
					CLRForm.setClrrate2(dfcur.format(rs
							.getFloat("clr_labor_rate2")) + "");
					CLRForm.setDetailid2(rs.getString("clr_detail_id2"));
					CLRForm.setResourceid2(rs.getString("iv_rp_id2"));
					CLRForm.setResourcename2(rs.getString("iv_rp_name2"));
				}

				if (total_res >= 4) {
					CLRForm.setClrrate3(dfcur.format(rs
							.getFloat("clr_labor_rate3")) + "");
					CLRForm.setDetailid3(rs.getString("clr_detail_id3"));
					CLRForm.setResourceid3(rs.getString("iv_rp_id3"));
					CLRForm.setResourcename3(rs.getString("iv_rp_name3"));
				}

				if (total_res >= 5) {
					CLRForm.setClrrate4(dfcur.format(rs
							.getFloat("clr_labor_rate4")) + "");
					CLRForm.setDetailid4(rs.getString("clr_detail_id4"));
					CLRForm.setResourceid4(rs.getString("iv_rp_id4"));
					CLRForm.setResourcename4(rs.getString("iv_rp_name4"));
				}

				if (total_res >= 6) {
					CLRForm.setClrrate5(dfcur.format(rs
							.getFloat("clr_labor_rate5")) + "");
					CLRForm.setDetailid5(rs.getString("clr_detail_id5"));
					CLRForm.setResourceid5(rs.getString("iv_rp_id5"));
					CLRForm.setResourcename5(rs.getString("iv_rp_name5"));
				}

				if (total_res >= 7) {
					CLRForm.setClrrate6(dfcur.format(rs
							.getFloat("clr_labor_rate6")) + "");
					CLRForm.setDetailid6(rs.getString("clr_detail_id6"));
					CLRForm.setResourceid6(rs.getString("iv_rp_id6"));
					CLRForm.setResourcename6(rs.getString("iv_rp_name6"));
				}

				if (total_res >= 8) {
					CLRForm.setClrrate7(dfcur.format(rs
							.getFloat("clr_labor_rate7")) + "");
					CLRForm.setDetailid7(rs.getString("clr_detail_id7"));
					CLRForm.setResourceid7(rs.getString("iv_rp_id7"));
					CLRForm.setResourcename7(rs.getString("iv_rp_name7"));
				}

				if (total_res >= 9) {
					CLRForm.setClrrate8(dfcur.format(rs
							.getFloat("clr_labor_rate8")) + "");
					CLRForm.setDetailid8(rs.getString("clr_detail_id8"));
					CLRForm.setResourceid8(rs.getString("iv_rp_id8"));
					CLRForm.setResourcename8(rs.getString("iv_rp_name8"));
				}

				if (total_res >= 10) {
					CLRForm.setClrrate9(dfcur.format(rs
							.getFloat("clr_labor_rate9")) + "");
					CLRForm.setDetailid9(rs.getString("clr_detail_id9"));
					CLRForm.setResourceid9(rs.getString("iv_rp_id9"));
					CLRForm.setResourcename9(rs.getString("iv_rp_name9"));
				}

				if (total_res >= 11) {
					CLRForm.setClrrate10(dfcur.format(rs
							.getFloat("clr_labor_rate10")) + "");
					CLRForm.setDetailid10(rs.getString("clr_detail_id10"));
					CLRForm.setResourceid10(rs.getString("iv_rp_id10"));
					CLRForm.setResourcename10(rs.getString("iv_rp_name10"));
				}

				if (total_res >= 12) {
					CLRForm.setClrrate11(dfcur.format(rs
							.getFloat("clr_labor_rate11")) + "");
					CLRForm.setDetailid11(rs.getString("clr_detail_id11"));
					CLRForm.setResourceid11(rs.getString("iv_rp_id11"));
					CLRForm.setResourcename11(rs.getString("iv_rp_name11"));
				}

				if (total_res >= 13) {
					CLRForm.setClrrate12(dfcur.format(rs
							.getFloat("clr_labor_rate12")) + "");
					CLRForm.setDetailid12(rs.getString("clr_detail_id12"));
					CLRForm.setResourceid12(rs.getString("iv_rp_id12"));
					CLRForm.setResourcename12(rs.getString("iv_rp_name12"));
				}

				if (total_res >= 14) {
					CLRForm.setClrrate13(dfcur.format(rs
							.getFloat("clr_labor_rate13")) + "");
					CLRForm.setDetailid13(rs.getString("clr_detail_id13"));
					CLRForm.setResourceid13(rs.getString("iv_rp_id13"));
					CLRForm.setResourcename13(rs.getString("iv_rp_name13"));
				}

				if (total_res >= 15) {
					CLRForm.setClrrate14(dfcur.format(rs
							.getFloat("clr_labor_rate14")) + "");
					CLRForm.setDetailid14(rs.getString("clr_detail_id14"));
					CLRForm.setResourceid14(rs.getString("iv_rp_id14"));
					CLRForm.setResourcename14(rs.getString("iv_rp_name14"));
				}

				if (total_res >= 16) {
					CLRForm.setClrrate15(dfcur.format(rs
							.getFloat("clr_labor_rate15")) + "");
					CLRForm.setDetailid15(rs.getString("clr_detail_id15"));
					CLRForm.setResourceid15(rs.getString("iv_rp_id15"));
					CLRForm.setResourcename15(rs.getString("iv_rp_name15"));
				}
				if (total_res >= 17) {
					CLRForm.setClrrate16(dfcur.format(rs
							.getFloat("clr_labor_rate16")) + "");
					CLRForm.setDetailid16(rs.getString("clr_detail_id16"));
					CLRForm.setResourceid16(rs.getString("iv_rp_id16"));
					CLRForm.setResourcename16(rs.getString("iv_rp_name16"));
				}
				if (total_res >= 18) {
					CLRForm.setClrrate17(dfcur.format(rs
							.getFloat("clr_labor_rate17")) + "");
					CLRForm.setDetailid17(rs.getString("clr_detail_id17"));
					CLRForm.setResourceid17(rs.getString("iv_rp_id17"));
					CLRForm.setResourcename17(rs.getString("iv_rp_name17"));
				}
				if (total_res >= 19) {
					CLRForm.setClrrate18(dfcur.format(rs
							.getFloat("clr_labor_rate18")) + "");
					CLRForm.setDetailid18(rs.getString("clr_detail_id18"));
					CLRForm.setResourceid18(rs.getString("iv_rp_id18"));
					CLRForm.setResourcename18(rs.getString("iv_rp_name18"));
				}
				if (total_res >= 20) {
					CLRForm.setClrrate19(dfcur.format(rs
							.getFloat("clr_labor_rate19")) + "");
					CLRForm.setDetailid19(rs.getString("clr_detail_id19"));
					CLRForm.setResourceid19(rs.getString("iv_rp_id19"));
					CLRForm.setResourcename19(rs.getString("iv_rp_name19"));
				}
				if (total_res >= 21) {
					CLRForm.setClrrate20(dfcur.format(rs
							.getFloat("clr_labor_rate20")) + "");
					CLRForm.setDetailid20(rs.getString("clr_detail_id20"));
					CLRForm.setResourceid20(rs.getString("iv_rp_id20"));
					CLRForm.setResourcename20(rs.getString("iv_rp_name20"));
				}
				if (total_res >= 22) {
					CLRForm.setClrrate21(dfcur.format(rs
							.getFloat("clr_labor_rate21")) + "");
					CLRForm.setDetailid21(rs.getString("clr_detail_id21"));
					CLRForm.setResourceid21(rs.getString("iv_rp_id21"));
					CLRForm.setResourcename21(rs.getString("iv_rp_name21"));
				}
				if (total_res >= 23) {
					CLRForm.setClrrate22(dfcur.format(rs
							.getFloat("clr_labor_rate22")) + "");
					CLRForm.setDetailid22(rs.getString("clr_detail_id22"));
					CLRForm.setResourceid22(rs.getString("iv_rp_id22"));
					CLRForm.setResourcename22(rs.getString("iv_rp_name22"));
				}
				if (total_res >= 24) {
					CLRForm.setClrrate23(dfcur.format(rs
							.getFloat("clr_labor_rate23")) + "");
					CLRForm.setDetailid23(rs.getString("clr_detail_id23"));
					CLRForm.setResourceid23(rs.getString("iv_rp_id23"));
					CLRForm.setResourcename23(rs.getString("iv_rp_name23"));
				}
				if (total_res >= 25) {
					CLRForm.setClrrate24(dfcur.format(rs
							.getFloat("clr_labor_rate24")) + "");
					CLRForm.setDetailid24(rs.getString("clr_detail_id24"));
					CLRForm.setResourceid24(rs.getString("iv_rp_id24"));
					CLRForm.setResourcename24(rs.getString("iv_rp_name24"));
				}
				if (total_res >= 26) {
					CLRForm.setClrrate25(dfcur.format(rs
							.getFloat("clr_labor_rate25")) + "");
					CLRForm.setDetailid25(rs.getString("clr_detail_id25"));
					CLRForm.setResourceid25(rs.getString("iv_rp_id25"));
					CLRForm.setResourcename25(rs.getString("iv_rp_name25"));
				}
				if (total_res >= 27) {
					CLRForm.setClrrate26(dfcur.format(rs
							.getFloat("clr_labor_rate26")) + "");
					CLRForm.setDetailid26(rs.getString("clr_detail_id26"));
					CLRForm.setResourceid26(rs.getString("iv_rp_id26"));
					CLRForm.setResourcename26(rs.getString("iv_rp_name26"));
				}
				if (total_res >= 28) {
					CLRForm.setClrrate27(dfcur.format(rs
							.getFloat("clr_labor_rate27")) + "");
					CLRForm.setDetailid27(rs.getString("clr_detail_id27"));
					CLRForm.setResourceid27(rs.getString("iv_rp_id27"));
					CLRForm.setResourcename27(rs.getString("iv_rp_name27"));
				}
				if (total_res >= 29) {
					CLRForm.setClrrate28(dfcur.format(rs
							.getFloat("clr_labor_rate28")) + "");
					CLRForm.setDetailid28(rs.getString("clr_detail_id28"));
					CLRForm.setResourceid28(rs.getString("iv_rp_id28"));
					CLRForm.setResourcename28(rs.getString("iv_rp_name28"));
				}
				if (total_res >= 30) {
					CLRForm.setClrrate29(dfcur.format(rs
							.getFloat("clr_labor_rate29")) + "");
					CLRForm.setDetailid29(rs.getString("clr_detail_id29"));
					CLRForm.setResourceid29(rs.getString("iv_rp_id29"));
					CLRForm.setResourcename29(rs.getString("iv_rp_name29"));
				}

				CLRlist.add(CLRForm);
				i++;
			}
		} catch (Exception e) {
			logger.error(
					"getDiscountedCLRates(String, String, int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDiscountedCLRates(String, String, int, DataSource) - Exception Caught in getDiscountedCLRates"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error(
						"getDiscountedCLRates(String, String, int, DataSource)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("getDiscountedCLRates(String, String, int, DataSource) - Exception Caught in getDiscountedCLRates"
							+ e);
				}
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.error(
						"getDiscountedCLRates(String, String, int, DataSource)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("getDiscountedCLRates(String, String, int, DataSource) - Exception Caught in getDiscountedCLRates"
							+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.error(
						"getDiscountedCLRates(String, String, int, DataSource)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("getDiscountedCLRates(String, String, int, DataSource) - Exception Caught in getDiscountedCLRates"
							+ e);
				}
			}

		}
		return CLRlist;
	}

	public static int getCategoriesPerCriticality(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		int total = 0;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select count(*) from clr_category";
			// System.out.println("QUERY"+sql);

			rs = stmt.executeQuery(sql);

			if (rs.next())
				total = rs.getInt(1) / 5;
		}

		catch (Exception e) {
			logger.error("getCategoriesPerCriticality(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCategoriesPerCriticality(DataSource) - Error Occured in retrieving getCategoriesPerCriticality"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getCategoriesPerCriticality(DataSource) - exception ignored",
						s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getCategoriesPerCriticality(DataSource) - exception ignored",
						s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getCategoriesPerCriticality(DataSource) - exception ignored",
						s);
			}

		}
		return total;
	}

	public static int getTotalCategories(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		int total = 0;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select count(*) from clr_category";
			// System.out.println("QUERY"+sql);

			rs = stmt.executeQuery(sql);

			if (rs.next())
				total = rs.getInt(1);
		}

		catch (Exception e) {
			logger.error("getTotalCategories(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTotalCategories(DataSource) - Error Occured in retrieving getTotalCategories"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException s) {
				logger.warn(
						"getTotalCategories(DataSource) - exception ignored", s);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getTotalCategories(DataSource) - exception ignored", s);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getTotalCategories(DataSource) - exception ignored", s);
			}

		}
		return total;
	}

}
