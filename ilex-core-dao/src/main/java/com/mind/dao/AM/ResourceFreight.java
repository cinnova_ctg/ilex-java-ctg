package com.mind.dao.AM;

public class ResourceFreight {
	
	private String activity_Id = null;
	private String freightid = null;
	private String freightcostlibid = null;
	private String freightname = null;
	private String freighttype = null;
	private String freightCnspartnumber = null;
	private String freightQuantity = null;
	private String freightPrevquantity = null;
	private String freightEstimatedunitcost = null;
	private String freightEstimatedtotalcost = null;
	private String freightProformamargin = null;
	private String freightPriceunit = null;
	private String freightPriceextended = null;
	private String freightSellablequantity = null;
	private String freightMinimumquantity = null;
	private String freightStatus = null;
	private String addendum_id = null;
	private String freightFlag = null; // for resource from temporary table or not
	private String flag_freightid = null; //for setting checkbox checked
	private String activity_name = null;
	private String msa_Id = null;
	private String msaname = null;
	private String resourceType = null;
	
	
	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id(String activity_Id) {
		this.activity_Id = activity_Id;
	}
	public String getActivity_name() {
		return activity_name;
	}
	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}
	public String getFlag_freightid() {
		return flag_freightid;
	}
	public void setFlag_freightid(String flag_freightid) {
		this.flag_freightid = flag_freightid;
	}
	public String getFreightCnspartnumber() {
		return freightCnspartnumber;
	}
	public void setFreightCnspartnumber(String freightCnspartnumber) {
		this.freightCnspartnumber = freightCnspartnumber;
	}
	public String getFreightcostlibid() {
		return freightcostlibid;
	}
	public void setFreightcostlibid(String freightcostlibid) {
		this.freightcostlibid = freightcostlibid;
	}
	public String getFreightEstimatedtotalcost() {
		return freightEstimatedtotalcost;
	}
	public void setFreightEstimatedtotalcost(String freightEstimatedtotalcost) {
		this.freightEstimatedtotalcost = freightEstimatedtotalcost;
	}
	public String getFreightEstimatedunitcost() {
		return freightEstimatedunitcost;
	}
	public void setFreightEstimatedunitcost(String freightEstimatedunitcost) {
		this.freightEstimatedunitcost = freightEstimatedunitcost;
	}
	public String getFreightFlag() {
		return freightFlag;
	}
	public void setFreightFlag(String freightFlag) {
		this.freightFlag = freightFlag;
	}
	public String getFreightid() {
		return freightid;
	}
	public void setFreightid(String freightid) {
		this.freightid = freightid;
	}
	public String getFreightMinimumquantity() {
		return freightMinimumquantity;
	}
	public void setFreightMinimumquantity(String freightMinimumquantity) {
		this.freightMinimumquantity = freightMinimumquantity;
	}
	public String getFreightname() {
		return freightname;
	}
	public void setFreightname(String freightname) {
		this.freightname = freightname;
	}
	public String getFreightPrevquantity() {
		return freightPrevquantity;
	}
	public void setFreightPrevquantity(String freightPrevquantity) {
		this.freightPrevquantity = freightPrevquantity;
	}
	public String getFreightPriceextended() {
		return freightPriceextended;
	}
	public void setFreightPriceextended(String freightPriceextended) {
		this.freightPriceextended = freightPriceextended;
	}
	public String getFreightPriceunit() {
		return freightPriceunit;
	}
	public void setFreightPriceunit(String freightPriceunit) {
		this.freightPriceunit = freightPriceunit;
	}
	public String getFreightProformamargin() {
		return freightProformamargin;
	}
	public void setFreightProformamargin(String freightProformamargin) {
		this.freightProformamargin = freightProformamargin;
	}
	public String getFreightQuantity() {
		return freightQuantity;
	}
	public void setFreightQuantity(String freightQuantity) {
		this.freightQuantity = freightQuantity;
	}
	public String getFreightSellablequantity() {
		return freightSellablequantity;
	}
	public void setFreightSellablequantity(String freightSellablequantity) {
		this.freightSellablequantity = freightSellablequantity;
	}
	public String getFreightStatus() {
		return freightStatus;
	}
	public void setFreightStatus(String freightStatus) {
		this.freightStatus = freightStatus;
	}
	public String getFreighttype() {
		return freighttype;
	}
	public void setFreighttype(String freighttype) {
		this.freighttype = freighttype;
	}
	public String getMsa_Id() {
		return msa_Id;
	}
	public void setMsa_Id(String msa_Id) {
		this.msa_Id = msa_Id;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}

	
	
	
}
