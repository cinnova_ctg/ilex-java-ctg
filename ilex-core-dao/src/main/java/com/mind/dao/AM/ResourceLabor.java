/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: A bean containing getter/setter methods for labor resource 
*
*/


package com.mind.dao.AM;

public class ResourceLabor 
{
	private String activity_Id = null;
	private String laborid = null;
	private String laborcostlibid = null;
	private String laborname = null;
	private String labortype = null;
	
	
	private String laborCnspartnumber = null;
	
	private String laborQuantityhours = null;
	private String laborPrevquantityhours = null;
	
	private String laborEstimatedhourlybasecost = null;
	
	
	private String laborEstimatedtotalcost = null;
	private String laborProformamargin = null;
	private String laborPriceunit = null;
	private String laborPriceextended = null;
	
	private String laborSellablequantity = null;
	private String laborMinimumquantity = null;
	private String laborStatus = null;
	private String laborSubcategory = null;
	
	private String addendum_id = null;
	private String laborFlag = null; // for resource from temporary table or not
	
	//	 for in transit field
	private String laborTransit = null;
	private String laborChecktransit = null;
	
	private String flag_laborid = null; //for setting checkbox checked
	
	private String clr_detail_id = null;
	
	private String activity_name = null;
	private String msa_Id = null;
	private String msaname = null;
	private String resourceType = null;
	private String troubleRepairFlag = null;
	
	
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String activity_Id ) {
		this.activity_Id = activity_Id;
	}
	
	public String getLaborid() {
		return laborid;
	}
	public void setLaborid( String laborid ) {
		this.laborid = laborid;
	}
	
	
	public String getLaborcostlibid() {
		return laborcostlibid;
	}
	public void setLaborcostlibid( String laborcostlibid ) {
		this.laborcostlibid = laborcostlibid;
	}
	
	
	public String getLaborname() {
		return laborname;
	}
	public void setLaborname( String laborname ) {
		this.laborname = laborname;
	}
	
	
	public String getLabortype() {
		return labortype;
	}
	public void setLabortype( String labortype ) {
		this.labortype = labortype;
	}
	
	

	
	
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id( String addendum_id ) {
		this.addendum_id = addendum_id;
	}
	

	public String getLaborChecktransit() {
		return laborChecktransit;
	}
	public void setLaborChecktransit(String laborChecktransit) {
		this.laborChecktransit = laborChecktransit;
	}
	public String getLaborCnspartnumber() {
		return laborCnspartnumber;
	}
	public void setLaborCnspartnumber(String laborCnspartnumber) {
		this.laborCnspartnumber = laborCnspartnumber;
	}
	public String getLaborEstimatedhourlybasecost() {
		return laborEstimatedhourlybasecost;
	}
	public void setLaborEstimatedhourlybasecost(String laborEstimatedhourlybasecost) {
		this.laborEstimatedhourlybasecost = laborEstimatedhourlybasecost;
	}
	public String getLaborEstimatedtotalcost() {
		return laborEstimatedtotalcost;
	}
	public void setLaborEstimatedtotalcost(String laborEstimatedtotalcost) {
		this.laborEstimatedtotalcost = laborEstimatedtotalcost;
	}
	public String getLaborFlag() {
		return laborFlag;
	}
	public void setLaborFlag(String laborFlag) {
		this.laborFlag = laborFlag;
	}
	public String getLaborMinimumquantity() {
		return laborMinimumquantity;
	}
	public void setLaborMinimumquantity(String laborMinimumquantity) {
		this.laborMinimumquantity = laborMinimumquantity;
	}
	public String getLaborPrevquantityhours() {
		return laborPrevquantityhours;
	}
	public void setLaborPrevquantityhours(String laborPrevquantityhours) {
		this.laborPrevquantityhours = laborPrevquantityhours;
	}
	public String getLaborPriceextended() {
		return laborPriceextended;
	}
	public void setLaborPriceextended(String laborPriceextended) {
		this.laborPriceextended = laborPriceextended;
	}
	public String getLaborPriceunit() {
		return laborPriceunit;
	}
	public void setLaborPriceunit(String laborPriceunit) {
		this.laborPriceunit = laborPriceunit;
	}
	public String getLaborProformamargin() {
		return laborProformamargin;
	}
	public void setLaborProformamargin(String laborProformamargin) {
		this.laborProformamargin = laborProformamargin;
	}
	public String getLaborQuantityhours() {
		return laborQuantityhours;
	}
	public void setLaborQuantityhours(String laborQuantityhours) {
		this.laborQuantityhours = laborQuantityhours;
	}
	public String getLaborSellablequantity() {
		return laborSellablequantity;
	}
	public void setLaborSellablequantity(String laborSellablequantity) {
		this.laborSellablequantity = laborSellablequantity;
	}
	public String getLaborStatus() {
		return laborStatus;
	}
	public void setLaborStatus(String laborStatus) {
		this.laborStatus = laborStatus;
	}
	public String getLaborSubcategory() {
		return laborSubcategory;
	}
	public void setLaborSubcategory(String laborSubcategory) {
		this.laborSubcategory = laborSubcategory;
	}
	public String getLaborTransit() {
		return laborTransit;
	}
	public void setLaborTransit(String laborTransit) {
		this.laborTransit = laborTransit;
	}
	public String getFlag_laborid() {
		return flag_laborid;
	}
	public void setFlag_laborid(String flag_laborid) {
		this.flag_laborid = flag_laborid;
	}
	public String getClr_detail_id() {
		return clr_detail_id;
	}
	public void setClr_detail_id(String clr_detail_id) {
		this.clr_detail_id = clr_detail_id;
	}
	public String getActivity_name() {
		return activity_name;
	}
	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}
	public String getMsa_Id() {
		return msa_Id;
	}
	public void setMsa_Id(String msa_Id) {
		this.msa_Id = msa_Id;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	public String getTroubleRepairFlag() {
		return troubleRepairFlag;
	}
	public void setTroubleRepairFlag(String troubleRepairFlag) {
		this.troubleRepairFlag = troubleRepairFlag;
	}
	
	
	
}