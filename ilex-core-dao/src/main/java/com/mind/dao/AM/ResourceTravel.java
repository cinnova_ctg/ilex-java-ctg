package com.mind.dao.AM;

public class ResourceTravel {
	
	
	private String activity_Id = null;
	private String travelid = null;
	private String travelcostlibid = null;
	private String travelname = null;
	private String traveltype = null;
	private String travelCnspartnumber = null;
	private String travelQuantity = null;
	private String travelPrevquantity = null;
	private String travelEstimatedunitcost = null;
	private String travelEstimatedtotalcost = null;
	private String travelProformamargin = null;
	private String travelPriceunit = null;
	private String travelPriceextended = null;
	private String travelSellablequantity = null;
	private String travelMinimumquantity = null;
	private String travelStatus = null;
	private String addendum_id = null;
	private String travelFlag = null; // for resource from temporary table or not
	private String flag_travelid = null; //for setting checkbox checked
	
	private String activity_name = null;
	private String msa_Id = null;
	private String msaname = null;
	private String resourceType = null;
	
	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id(String activity_Id) {
		this.activity_Id = activity_Id;
	}
	public String getActivity_name() {
		return activity_name;
	}
	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id(String addendum_id) {
		this.addendum_id = addendum_id;
	}
	public String getFlag_travelid() {
		return flag_travelid;
	}
	public void setFlag_travelid(String flag_travelid) {
		this.flag_travelid = flag_travelid;
	}
	public String getMsa_Id() {
		return msa_Id;
	}
	public void setMsa_Id(String msa_Id) {
		this.msa_Id = msa_Id;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	public String getTravelEstimatedunitcost() {
		return travelEstimatedunitcost;
	}
	public void setTravelEstimatedunitcost(String travelEstimatedunitcost) {
		this.travelEstimatedunitcost = travelEstimatedunitcost;
	}
	public String getTravelCnspartnumber() {
		return travelCnspartnumber;
	}
	public void setTravelCnspartnumber(String travelCnspartnumber) {
		this.travelCnspartnumber = travelCnspartnumber;
	}
	public String getTravelcostlibid() {
		return travelcostlibid;
	}
	public void setTravelcostlibid(String travelcostlibid) {
		this.travelcostlibid = travelcostlibid;
	}
	public String getTravelEstimatedtotalcost() {
		return travelEstimatedtotalcost;
	}
	public void setTravelEstimatedtotalcost(String travelEstimatedtotalcost) {
		this.travelEstimatedtotalcost = travelEstimatedtotalcost;
	}
	public String getTravelFlag() {
		return travelFlag;
	}
	public void setTravelFlag(String travelFlag) {
		this.travelFlag = travelFlag;
	}
	public String getTravelid() {
		return travelid;
	}
	public void setTravelid(String travelid) {
		this.travelid = travelid;
	}
	public String getTravelMinimumquantity() {
		return travelMinimumquantity;
	}
	public void setTravelMinimumquantity(String travelMinimumquantity) {
		this.travelMinimumquantity = travelMinimumquantity;
	}
	public String getTravelname() {
		return travelname;
	}
	public void setTravelname(String travelname) {
		this.travelname = travelname;
	}
	public String getTravelPrevquantity() {
		return travelPrevquantity;
	}
	public void setTravelPrevquantity(String travelPrevquantity) {
		this.travelPrevquantity = travelPrevquantity;
	}
	public String getTravelPriceextended() {
		return travelPriceextended;
	}
	public void setTravelPriceextended(String travelPriceextended) {
		this.travelPriceextended = travelPriceextended;
	}
	public String getTravelPriceunit() {
		return travelPriceunit;
	}
	public void setTravelPriceunit(String travelPriceunit) {
		this.travelPriceunit = travelPriceunit;
	}
	public String getTravelProformamargin() {
		return travelProformamargin;
	}
	public void setTravelProformamargin(String travelProformamargin) {
		this.travelProformamargin = travelProformamargin;
	}
	public String getTravelQuantity() {
		return travelQuantity;
	}
	public void setTravelQuantity(String travelQuantity) {
		this.travelQuantity = travelQuantity;
	}
	public String getTravelSellablequantity() {
		return travelSellablequantity;
	}
	public void setTravelSellablequantity(String travelSellablequantity) {
		this.travelSellablequantity = travelSellablequantity;
	}
	public String getTravelStatus() {
		return travelStatus;
	}
	public void setTravelStatus(String travelStatus) {
		this.travelStatus = travelStatus;
	}
	public String getTraveltype() {
		return traveltype;
	}
	public void setTraveltype(String traveltype) {
		this.traveltype = traveltype;
	}
	

}
