/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: A bean containing getter/setter methods for activity library 
*
*/

package com.mind.dao.AM;

public class ActivityLibrary 
{
	private String msa_Id = null; 
	private String activity_Id = null;
	private String category_type = null;	
	private String category_typecombo = null;
	private String name = null;
	private String activitytype = null;
	private String activitytypecombo = null;
	private String quantity = null;
	private String duration = null;
	private String estimatedmaterialcost = null;
	private String estimatedcnslabourcost = null;
	private String estimatedcontractlabourcost = null;
	private String estimatedfreightcost = null;
	private String estimatedtravelcost = null;
	private String estimatedtotalcost = null;
	private String extendedprice = null;
	private String proformamargin = null;
	private String sow = null;
	private String assumption = null;
	private String status = null;
	private String cat_Id = null;
	private String msaname = null;
	
	
	public String getMsa_Id() {
		return msa_Id;
	}
	public void setMsa_Id( String msa_Id ) {
		this.msa_Id = msa_Id;
	}
	
	
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String activity_Id ) {
		this.activity_Id = activity_Id;
	}
	public String getName() {
		return name;
	}
	public void setName( String name ) {
		this.name = name;
	}
	
	
	public String getActivitytype() {
		return activitytype;
	}
	public void setActivitytype( String activitytype ) {
		this.activitytype = activitytype;
	}
	
	
	public String getActivitytypecombo() {
		return activitytypecombo;
	}
	public void setActivitytypecombo( String activitytypecombo ) {
		this.activitytypecombo = activitytypecombo;
	}
	
	
	public String getCategory_type() {
		return category_type;
	}
	public void setCategory_type( String category_type ) {
		this.category_type = category_type;
	}
	
	
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity( String quantity ) {
		this.quantity = quantity;
	}
	
	
	public String getDuration() {
		return duration;
	}
	public void setDuration( String duration ) {
		this.duration = duration;
	}
	
	
	public String getEstimatedmaterialcost() {
		return estimatedmaterialcost;
	}
	public void setEstimatedmaterialcost( String estimatedmaterialcost ) {
		this.estimatedmaterialcost = estimatedmaterialcost;
	}
	
	
	public String getEstimatedcnslabourcost() {
		return estimatedcnslabourcost;
	}
	public void setEstimatedcnslabourcost( String estimatedcnslabourcost ) {
		this.estimatedcnslabourcost = estimatedcnslabourcost;
	}
	
	
	public String getEstimatedcontractlabourcost() {
		return estimatedcontractlabourcost;
	}
	public void setEstimatedcontractlabourcost( String estimatedcontractlabourcost ) {
		this.estimatedcontractlabourcost = estimatedcontractlabourcost;
	}
	
	
	public String getEstimatedfreightcost() {
		return estimatedfreightcost;
	}
	public void setEstimatedfreightcost( String estimatedfreightcost ) {
		this.estimatedfreightcost = estimatedfreightcost;
	}
	
	
	public String getEstimatedtravelcost() {
		return estimatedtravelcost;
	}
	public void setEstimatedtravelcost( String estimatedtravelcost ) {
		this.estimatedtravelcost = estimatedtravelcost;
	}
	
	
	public String getEstimatedtotalcost() {
		return estimatedtotalcost;
	}
	public void setEstimatedtotalcost( String estimatedtotalcost ) {
		this.estimatedtotalcost = estimatedtotalcost;
	}
	
	
	public String getExtendedprice() {
		return extendedprice;
	}
	public void setExtendedprice( String extendedprice ) {
		this.extendedprice = extendedprice;
	}
	
	
	public String getProformamargin() {
		return proformamargin;
	}
	public void setProformamargin( String proformamargin ) {
		this.proformamargin = proformamargin;
	}
	
	
	public String getSow() {
		return sow;
	}
	public void setSow( String sow ) {
		this.sow = sow;
	}
	
	
	public String getAssumption() {
		return assumption;
	}
	public void setAssumption( String assumption ) {
		this.assumption = assumption;
	}
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus( String status ) {
		this.status = status;
	}
	
	
	public String getCat_Id() {
		return cat_Id;
	}
	public void setCat_Id( String cat_Id ) {
		this.cat_Id = cat_Id;
	}
	public String getCategory_typecombo() {
		return category_typecombo;
	}
	public void setCategory_typecombo(String category_typecombo) {
		this.category_typecombo = category_typecombo;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}	

	
}
