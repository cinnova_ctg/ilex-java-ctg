package com.mind.dao.AM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.AddClrCategoryBean;
import com.mind.common.Util;

public class AddClrCategorydao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(AddClrCategorydao.class);

	public static String[] getCategoryName(String catid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String catg[] = new String[2];
		int categoryid = 0;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (catid != null) {
				categoryid = Integer.parseInt(catid);
			}

			String sql = "select clr_cat_short_name, clr_cat_name from clr_category where clr_cat_id="
					+ categoryid;

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				catg[0] = rs.getString("clr_cat_short_name");
				catg[1] = rs.getString("clr_cat_name");
			}

		} catch (Exception e) {
			logger.error("getCategoryName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCategoryName(String, DataSource) - Error occured during getCategoryName");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCategoryName(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCategoryName(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCategoryName(String, DataSource) - exception ignored",
						e);
			}

		}
		return catg;
	}

	public static ArrayList getCritServicetimelist(String catname, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		String sql = "";

		try {
			conn = ds.getConnection();

			stmt = conn.createStatement();

			if (catname == null || catname.equals("0")) {
				sql = "select iv_crit_id, iv_crit_name from iv_criticality ";
			} else {
				sql = "select iv_crit_id, iv_crit_name, clr_service_time from func_clr_crit_servicetime_list ('"
						+ catname + "')";
			}

			rs = stmt.executeQuery(sql);
			int i = 0;

			while (rs.next()) {

				AddClrCategoryBean clrCatgBean = new AddClrCategoryBean();

				clrCatgBean.setClrcriticality(rs.getString("iv_crit_name"));
				clrCatgBean.setCriticalityid(rs.getString("iv_crit_id"));

				if (catname == null || catname.equals("0")) {
					clrCatgBean.setClrservicetime("");
				} else {
					clrCatgBean.setClrservicetime(rs
							.getString("clr_service_time"));
				}

				valuelist.add(i, clrCatgBean);
				i++;
			}

		} catch (Exception e) {
			logger.error("getCritServicetimelist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCritServicetimelist(String, DataSource) - Error occured during getting getCritServicetimelist  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCritServicetimelist(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCritServicetimelist(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCritServicetimelist(String, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static int addCategory(String name, String shortname,
			String servicetime, String criticality, DataSource ds) {
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;
		Util util = new Util();

		try {
			// Added to change single quotes string to double quotes
			if (name != null) {
				if (name.indexOf("'") >= 0)
					name = util.changeStringToDoubleQuotes(name);
			}
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.clr_category_manage_01( ? , ? , ? , ? ) }");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, name);
			cstmt.setString(3, shortname);
			cstmt.setString(4, servicetime);
			cstmt.setString(5, criticality);
			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error(
					"addCategory(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addCategory(String, String, String, String, DataSource) - Exception caught in adding addCategory"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"addCategory(String, String, String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addCategory(String, String, String, String, DataSource) - Exception Caught in adding addCategory"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"addCategory(String, String, String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addCategory(String, String, String, String, DataSource) - Exception Caught in adding addCategory"
							+ sql);
				}
			}

		}
		return val;
	}

	public static int deleteCategory(String catshortname, DataSource ds) {
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.clr_category_delete_01( ? ) }");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, catshortname);

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error("deleteCategory(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteCategory(String, DataSource) - Exception caught in  deleteCategory"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error("deleteCategory(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteCategory(String, DataSource) - Exception Caught in deleteCategory"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("deleteCategory(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteCategory(String, DataSource) - Exception Caught in deleteCategory"
							+ sql);
				}
			}

		}
		return val;
	}

}
