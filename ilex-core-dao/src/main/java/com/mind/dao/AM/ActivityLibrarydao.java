/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	:This class is used to manage activity library and Resources     
 *
 */

package com.mind.dao.AM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.am.ActivityLibrary;
import com.mind.common.LabelValue;
import com.mind.common.Util;
import com.mind.common.bean.Assumption;
import com.mind.common.bean.Sow;
import com.mind.fw.core.dao.util.DBUtil;

/*
 * Methods : execute
 */

public class ActivityLibrarydao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ActivityLibrarydao.class);

	/**
	 * This method is used to get activity category
	 * 
	 * @param ds
	 *            object of DataSource
	 * @return ArrayList This method returns object of ArrayList.
	 */
	public static ArrayList getActivitycategorylist(DataSource ds) {
		ArrayList list = new ArrayList();

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new com.mind.common.LabelValue("---Select---", "0"));
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select * from dbo.func_lm_activity_categoy()";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				list.add(new LabelValue(rs.getString(2), rs.getString(1) + "!"
						+ rs.getString(2)));
			}
		} catch (Exception e) {
			logger.error("getActivitycategorylist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivitycategorylist(DataSource) - Exception caught in rerieving activity category"
						+ e);
			}
		} finally {
			try {
				rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getActivitycategorylist(DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getActivitycategorylist(DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getActivitycategorylist(DataSource) - exception ignored",
						s);
			}

		}

		return list;

	}

	/**
	 * This method is used to get activity category
	 * 
	 * @param ds
	 *            object of DataSource
	 * @return ArrayList This method returns object of ArrayList.
	 */
	public static ArrayList getActivitycategorylist_01(DataSource ds) {
		ArrayList list = new ArrayList();

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new com.mind.common.LabelValue("---Select---", "0"));
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select * from dbo.func_lm_activity_categoy() order  by lm_al_cat_name";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				list.add(new LabelValue(rs.getString(2), rs.getString(1)));
			}
		} catch (Exception e) {
			logger.error("getActivitycategorylist_01(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivitycategorylist_01(DataSource) - Exception caught in rerieving activity category"
						+ e);
			}
		} finally {
			try {
				rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getActivitycategorylist_01(DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getActivitycategorylist_01(DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getActivitycategorylist_01(DataSource) - exception ignored",
						s);
			}

		}

		return list;

	}

	public static String getCategoryname(String category_Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String categoryname = "";
		String s = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			s = "select lm_al_cat_name from lm_activity_lib_category where lm_al_cat_id="
					+ category_Id;
			rs = stmt.executeQuery(s);
			if (rs.next()) {
				categoryname = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("getCategoryname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCategoryname(String, DataSource) - Exception caught in rerieving category name"
						+ e);
			}
		} finally {
			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCategoryname(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCategoryname(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCategoryname(String, DataSource) - exception ignored",
						sql);
			}

		}

		return categoryname;
	}

	/**
	 * This method is used to add category
	 * 
	 * @param cat_name
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer stating whether the category is
	 *         successfully added or not
	 */
	public static int addCategory(String cat_name, DataSource ds) {
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;
		Util util = new Util();

		try {
			// Added to change single quotes string to double quotes
			if (cat_name != null) {
				if (cat_name.indexOf("'") >= 0)
					cat_name = util.changeStringToDoubleQuotes(cat_name);
			}

			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.lm_category_manage_01( ? , ? , ? ) }");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, 0);
			cstmt.setString(3, cat_name);
			cstmt.setString(4, "A");
			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error("addCategory(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addCategory(String, DataSource) - Exception caught in adding category"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error("addCategory(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addCategory(String, DataSource) - Exception Caught in adding category"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("addCategory(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addCategory(String, DataSource) - Exception Caught in adding category"
							+ sql);
				}
			}

		}
		return val;
	}

	/**
	 * This method is used to update category
	 * 
	 * @param cat_id
	 *            object of String
	 * @param cat_name
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer stating whether the category is
	 *         successfully updated or not
	 */

	public static int updateCategory(String cat_id, String cat_name,
			String flag, DataSource ds) {
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;
		Util util = new Util();
		try {
			// Added to change single quotes string to double quotes
			if (cat_name != null) {
				if (cat_name.indexOf("'") >= 0)
					cat_name = util.changeStringToDoubleQuotes(cat_name);
			}

			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.lm_category_manage_01( ? , ? , ? ) }");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(cat_id));
			cstmt.setString(3, cat_name);
			cstmt.setString(4, flag);

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error("updateCategory(String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateCategory(String, String, String, DataSource) - Exception caught in updating category"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"updateCategory(String, String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("updateCategory(String, String, String, DataSource) - Exception Caught in updating category"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"updateCategory(String, String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("updateCategory(String, String, String, DataSource) - Exception Caught in updating category"
							+ sql);
				}
			}

		}
		return val;
	}

	/**
	 * This method is used to get activity type
	 * 
	 * @return ArrayList object of ArrayList
	 */

	public static ArrayList getActivitytype() {
		ArrayList activitytype = new java.util.ArrayList();
		activitytype.add(new LabelValue("-Select-", "0"));
		activitytype.add(new LabelValue("Required", "R"));
		activitytype.add(new LabelValue("Optional", "O"));
		activitytype.add(new LabelValue("Non Billable (Required)", "E"));
		activitytype.add(new LabelValue("Non Billable (Optional)", "P"));
		return activitytype;
	}

	/**
	 * This method is used to get activity type for activities to be added under
	 * addendum
	 * 
	 * @return ArrayList object of ArrayList
	 */

	public static ArrayList getActivitytypeforaddendum() {
		ArrayList activitytype = new java.util.ArrayList();
		activitytype.add(new LabelValue("-Select-", "0"));
		// activitytype.add( new LabelValue( "Required" , "R" ) );
		activitytype.add(new LabelValue("Optional", "O"));
		return activitytype;
	}

	/**
	 * This method is used to retrieve resource list used in Activity Library
	 * 
	 * @param type
	 *            object of String ('Material' for Material , Labor for 'Labor'
	 *            , 'Freight' for Freight , 'Travel' for Travel)
	 * @param cat_name
	 *            object of String ( Id of Activity Librarry )
	 * @param ds
	 *            object of DataSource
	 * @return ArrayList This method returns ArrayList of resources
	 */
	public static ArrayList getMateriallist(String type,
			String Activitylibrary_Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String s = "";
		ArrayList materiallist = new java.util.ArrayList();

		materiallist.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			s = "select iv_rp_id , iv_rp_name , iv_sc_name from lx_resource_hierarchy_01 where iv_ct_type_desc "
					+ "like '"
					+ type
					+ "%' and iv_rp_id is not null and iv_rp_status = 'A' and iv_rp_id not in ( select lm_rs_rp_id from "
					+ "lm_resource_library where lm_rs_at_id = '"
					+ Activitylibrary_Id + "')";
			rs = stmt.executeQuery(s);
			while (rs.next()) {
				materiallist
						.add(new com.mind.common.LabelValue(rs
								.getString("iv_sc_name")
								+ "-"
								+ rs.getString("iv_rp_name"), rs
								.getString("iv_rp_id")));
			}
		} catch (Exception e) {
			logger.error("getMateriallist(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMateriallist(String, String, DataSource) - Exception caught in rerieving resource"
						+ e);
			}
		} finally {
			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMateriallist(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMateriallist(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMateriallist(String, String, DataSource) - exception ignored",
						sql);
			}

		}
		return materiallist;
	}

	/**
	 * This method is used to add activity
	 * 
	 * @param activity
	 *            object of ActivityLibrary ( activity to be added )
	 * @param loginuserid
	 *            object of String ( Id of user that has logged in )
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */

	public static int addActivity(ActivityLibrary activity, String loginuserid,
			DataSource ds) throws Exception {
		Connection conn = null;
		CallableStatement cstmt = null;
		Util util = new Util();
		ResultSet rs = null;
		int val = -1;
		try {
			// Added to change single quotes string to double quotes
			if (activity.getName() != null) {
				if (activity.getName().indexOf("'") >= 0)
					activity.setName(util.changeStringToDoubleQuotes(activity
							.getName()));
			}

			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.lm_activity_library_manage_01(?,?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(activity.getMsa_Id()));
			cstmt.setInt(3, Integer.parseInt(activity.getCategory_type()));
			cstmt.setInt(4, 0);
			cstmt.setString(5, activity.getName());
			cstmt.setString(6, "D");
			cstmt.setInt(7,
					Integer.parseInt(activity.getDuration() == null ? "0"
							: activity.getDuration()));
			cstmt.setInt(8,
					Integer.parseInt(activity.getQuantity() == null ? "1"
							: activity.getQuantity()));
			cstmt.setString(9, activity.getActivitytypecombo());
			cstmt.setInt(10, Integer.parseInt(loginuserid));

			cstmt.setString(11, "A");
			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("addActivity(ActivityLibrary, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addActivity(ActivityLibrary, String, DataSource) - Error Occured in Inserting Activity Library"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException sql) {
				logger.warn(
						"addActivity(ActivityLibrary, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addActivity(ActivityLibrary, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addActivity(ActivityLibrary, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	/**
	 * This method is used to update activity
	 * 
	 * @param activity
	 *            object of ActivityLibrary ( activity to be updated )
	 * @param loginuserid
	 *            object of String ( Id of user that has logged in )
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */

	public static int updateActivity(ActivityLibrary activity,
			String loginuserid, DataSource ds) throws Exception {
		Connection conn = null;
		CallableStatement cstmt = null;
		Util util = new Util();
		ResultSet rs = null;
		int val = -1;
		try {
			// Added to change single quotes string to double quotes
			if (activity.getName() != null) {
				if (activity.getName().indexOf("'") >= 0)
					activity.setName(util.changeStringToDoubleQuotes(activity
							.getName()));
			}

			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_activity_library_manage_01(?,?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(activity.getMsa_Id()));

			cstmt.setInt(3, Integer.parseInt(activity.getCategory_type()));

			cstmt.setInt(4, Integer.parseInt(activity.getActivity_Id()));
			cstmt.setString(5, activity.getName());

			cstmt.setString(6, "");
			cstmt.setInt(7,
					Integer.parseInt(activity.getDuration() == null ? "0"
							: activity.getDuration()));
			cstmt.setInt(8, 0);
			cstmt.setString(9, activity.getActivitytypecombo());
			cstmt.setInt(10, Integer.parseInt(loginuserid));

			cstmt.setString(11, "U");
			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("updateActivity(ActivityLibrary, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateActivity(ActivityLibrary, String, DataSource) - Error Occured in Inserting Activity Library"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException sql) {
				logger.warn(
						"updateActivity(ActivityLibrary, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateActivity(ActivityLibrary, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateActivity(ActivityLibrary, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	/**
	 * This method is used to retrieve activity library
	 * 
	 * @param ds
	 *            object of DataSource
	 * @param MSA_Id
	 *            Id of MSA for which library to be retrieved
	 * @param Category_Id
	 *            Id of Category for which library to be retrieved
	 * @param querystring
	 *            string containing order by clause along with sorting
	 *            parameters on which sorting is to be done
	 * @return ArrayList array of objects of activities
	 */

	public static ArrayList getActivitylibrary(DataSource ds, String MSA_Id,
			String Category_Id, String querystring) {
		ArrayList activitylibrary = new ArrayList();
		Connection conn = null;
		ActivityLibrary activity = null;
		Statement stmt = null;
		ResultSet rs = null;

		if (Category_Id.equals("")) {
			Category_Id = "%";
		}

		if (querystring.equals("")) {
			querystring = "order by lm_at_title";
		}
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_activity_library_tab('"
					+ MSA_Id + "' , '%' , '" + Category_Id + "')" + querystring;

			rs = stmt.executeQuery(sql);
			activitylibrary = setActivityLibrary(rs);
		} catch (SQLException e) {
			logger.error(
					"getActivitylibrary(DataSource, String, String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivitylibrary(DataSource, String, String, String) - Exception caught in getting activity library"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getActivitylibrary(DataSource, String, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitylibrary(DataSource, String, String, String) - Exception caught in getting activity library"
							+ sql);
				}
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.error(
						"getActivitylibrary(DataSource, String, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitylibrary(DataSource, String, String, String) - Exception caught in getting activity library"
							+ sql);
				}
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.error(
						"getActivitylibrary(DataSource, String, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitylibrary(DataSource, String, String, String) - Exception caught in getting activity library"
							+ sql);
				}
			}

		}
		return activitylibrary;
	}

	public static ArrayList setActivityLibrary(ResultSet rs) {
		ArrayList activitylibrary = new ArrayList();
		ActivityLibrary activity;
		String temp = "";
		DecimalFormat dfcur = new DecimalFormat("#,##0.00");
		DecimalFormat dfcurmargin = new DecimalFormat("0.00");

		// DecimalFormat dfcur = new DecimalFormat("#0.00");
		try {
			while (rs.next()) {
				activity = new ActivityLibrary();
				activity.setMsa_Id(rs.getString("lm_at_mm_id"));

				activity.setActivity_Id(rs.getString("lm_at_id"));
				activity.setName(rs.getString("lm_at_title"));
				activity.setStatus(rs.getString("lm_at_status"));

				activity.setActivitytype(rs.getString("lm_at_type"));

				// activity.setActivitytypecombo( rs.getString ( "lm_at_type"
				// ).charAt( 0 )+"" );

				activity.setActivitytypecombo(rs.getString("act_abbr_type"));

				// System.out.println(activity.getActivitytype());
				// System.out.println(activity.getActivitytypecombo());

				activity.setQuantity(rs.getString("lm_at_quantity"));

				activity.setEstimatedmaterialcost(dfcur.format(rs
						.getFloat("lx_cle_material_cost")) + "");
				activity.setEstimatedcnslabourcost(dfcur.format(rs
						.getFloat("lx_cle_cns_field_labor_cost")) + "");
				activity.setEstimatedcontractlabourcost(dfcur.format(rs
						.getFloat("lx_cle_ctr_field_labor_cost")) + "");

				activity.setEstimatedfreightcost(dfcur.format(rs
						.getFloat("lx_cle_frieght_cost")) + "");
				activity.setEstimatedtravelcost(dfcur.format(rs
						.getFloat("lx_cle_travel_cost")) + "");
				activity.setEstimatedtotalcost(dfcur.format(rs
						.getFloat("lx_cle_total_cost")) + "");
				activity.setExtendedprice(dfcur.format(rs
						.getFloat("lx_cle_total_price")) + "");
				activity.setProformamargin(dfcurmargin.format(rs
						.getFloat("lx_cle_margin")) + "");
				activity.setCategory_type(rs.getString("lm_al_cat_name"));
				activity.setDuration(rs.getString("lm_at_duration"));
				activity.setCat_Id(rs.getString("lm_al_cat_id"));

				activity.setCategory_typecombo(rs.getString("lm_al_cat_id"));
				activity.setMsaname(rs.getString("lo_ot_name"));

				activitylibrary.add(activity);
			}
		} catch (Exception e) {
			logger.error("setActivityLibrary(ResultSet)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setActivityLibrary(ResultSet) - Exception Caught"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.error("setActivityLibrary(ResultSet)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("setActivityLibrary(ResultSet) - Exception Caught"
							+ e);
				}
			}
		}

		return activitylibrary;
	}

	public static Long getActivityCreationDate(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String creationDate = null;
		long milliseconds = 0;
		// ActivityLibrary activity = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT lm_at_create_date FROM lm_activity_library WHERE lm_at_id = "
					+ Id;

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				creationDate = rs.getString(1);
				SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:SS");
				Date d = f.parse(creationDate);
				milliseconds = d.getTime();
			}

		}

		catch (Exception e) {
			logger.error("getActivityCreationDate(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityCreationDate(String, DataSource) - Exception in retrieving Activity library"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivityCreationDate(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityCreationDate(String, DataSource) - Exception in retrieving Activity library"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivityCreationDate(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityCreationDate(String, DataSource) - Exception in retrieving Activity library"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivityCreationDate(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivityCreationDate(String, DataSource) - Exception in retrieving Activity library"
							+ sql);
				}
			}

		}
		return milliseconds;
	}

	public static ActivityLibrary getActivity(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		ActivityLibrary activity = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from dbo.func_activity_library_tab('%' , '"
					+ Id + "' , '%' )";

			rs = stmt.executeQuery(sql);
			activity = new ActivityLibrary();
			ArrayList activitylist = setActivityLibrary(rs);

			activity = (ActivityLibrary) activitylist.get(0);
		}

		catch (Exception e) {
			logger.error("getActivity(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivity(String, DataSource) - Exception in retrieving Activity library"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivity(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivity(String, DataSource) - Exception in retrieving Activity library"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivity(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivity(String, DataSource) - Exception in retrieving Activity library"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivity(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivity(String, DataSource) - Exception in retrieving Activity library"
							+ sql);
				}
			}

		}
		return activity;
	}

	/**
	 * This method is used to retrieve activity status
	 * 
	 * @param Id
	 *            id of activity
	 * @param ds
	 *            object of DataSource
	 * @return String
	 */

	public static String getActivitystatus(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String status = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lm_at_status from dbo.func_activity_library_tab('%' , '"
					+ Id + "' , '%' )";
			rs = stmt.executeQuery(sql);
			if (rs.next())
				status = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getActivitystatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivitystatus(String, DataSource) - Exception in retrieving Activity status"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitystatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitystatus(String, DataSource) - Exception in retrieving Activity status"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitystatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitystatus(String, DataSource) - Exception in retrieving Activity status"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitystatus(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitystatus(String, DataSource) - Exception in retrieving Activity status"
							+ sql);
				}
			}

		}
		return status;
	}

	/**
	 * This method is used to delete activity
	 * 
	 * @param Id
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int deleteactivity(String Id, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int val = -1;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_activity_library_delete_01( ?  ) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(Id));

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("deleteactivity(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteactivity(String, DataSource) - Exception in DELETING Library"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sqle) {
				logger.error("deleteactivity(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteactivity(String, DataSource) - Exception in DELETING Library"
							+ sqle);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error("deleteactivity(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteactivity(String, DataSource) - Exception in DELETING Library"
							+ sqle);
				}
			}

		}
		return val;
	}

	public static ArrayList getSowlist(DataSource ds,
			String Activitylibrary_Id, String type) {
		ArrayList sowlist = new ArrayList();
		Connection conn = null;
		Sow sow = null;
		Statement stmt = null;

		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = " select * from dbo.func_view_sow('"
					+ Activitylibrary_Id + "' , '" + type + "' )";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				sow = new Sow();
				sow.setSow_Id(rs.getString(1));
				sow.setId(rs.getString(2));

				sow.setSow(rs.getString(3));
				sowlist.add(sow);
			}

		} catch (SQLException e) {
			logger.error("getSowlist(DataSource, String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSowlist(DataSource, String, String) - Exception caught in getting activity library'sow"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getSowlist(DataSource, String, String)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getSowlist(DataSource, String, String) - Exception caught in getting activity library'sow"
							+ sql);
				}
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.error("getSowlist(DataSource, String, String)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getSowlist(DataSource, String, String) - Exception caught in getting activity library'sow"
							+ sql);
				}
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.error("getSowlist(DataSource, String, String)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getSowlist(DataSource, String, String) - Exception caught in getting activity library'sow"
							+ sql);
				}
			}

		}
		return sowlist;
	}

	public static String Getnameforsowassumption(String Id, String type,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sowassumptionname = "";
		String s = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			s = "select dbo.func_sowassumption_name( '" + Id + "' , '" + type
					+ "' )";
			rs = stmt.executeQuery(s);
			if (rs.next()) {
				sowassumptionname = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("Getnameforsowassumption(String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("Getnameforsowassumption(String, String, DataSource) - Exception caught in rerieving sowassumptionname"
						+ e);
			}
		} finally {
			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"Getnameforsowassumption(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"Getnameforsowassumption(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"Getnameforsowassumption(String, String, DataSource) - exception ignored",
						sql);
			}

		}

		return sowassumptionname;
	}

	/**
	 * This method is used to add Statement Of Work
	 * 
	 * @param sow
	 *            object of Sow
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int addsow(Sow sow, DataSource ds) {
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;
		Util util = new Util();

		try {
			// Added to change single quotes string to double quotes
			/*
			 * if(sow.getSow() != null) { if(sow.getSow().indexOf("'") >= 0)
			 * sow.setSow(util.changeStringToDoubleQuotes(sow.getSow())); }
			 */
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_sow_manage( ? , ? , ? , ? ,?) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, Integer.parseInt(sow.getId()));
			cstmt.setString(4, sow.getSow());
			cstmt.setString(5, sow.getType());

			cstmt.setString(6, "A");

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error("addsow(Sow, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addsow(Sow, DataSource) - Exception caught in adding sow"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error("addsow(Sow, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addsow(Sow, DataSource) - Exception Caught in adding sow"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("addsow(Sow, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addsow(Sow, DataSource) - Exception Caught in adding sow"
							+ sql);
				}
			}

		}
		return val;
	}

	/**
	 * This method is used to update State Of Work
	 * 
	 * @param sow
	 *            object of Sow
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int Updatesow(Sow sow, DataSource ds) {
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;
		Util util = new Util();

		try {
			// Added to change single quotes string to double quotes
			/*
			 * if(sow.getSow() != null) { if(sow.getSow().indexOf("'") >= 0)
			 * sow.setSow(util.changeStringToDoubleQuotes(sow.getSow())); }
			 */
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.lm_sow_manage( ? , ? , ? , ? ,?) }");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(sow.getSow_Id()));
			cstmt.setInt(3, 0);
			cstmt.setString(4, sow.getSow());
			cstmt.setString(5, sow.getType());
			cstmt.setString(6, "U");
			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error("Updatesow(Sow, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("Updatesow(Sow, DataSource) - Exception caught in adding sow"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error("Updatesow(Sow, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("Updatesow(Sow, DataSource) - Exception Caught in adding sow"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("Updatesow(Sow, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("Updatesow(Sow, DataSource) - Exception Caught in adding sow"
							+ sql);
				}
			}

		}
		return val;
	}

	/**
	 * This method is used to delete State Of Work
	 * 
	 * @param sow
	 *            object of Sow
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */

	public static int Deletesow(Sow sow, DataSource ds) {
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_sow_manage( ? , ? , ? , ? ,?) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(sow.getSow_Id()));
			cstmt.setInt(3, 0);
			cstmt.setString(4, "");
			cstmt.setString(5, sow.getType());
			cstmt.setString(6, "D");

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error("Deletesow(Sow, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("Deletesow(Sow, DataSource) - Exception caught in deleting sow"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error("Deletesow(Sow, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("Deletesow(Sow, DataSource) - Exception Caught in deleting sow"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("Deletesow(Sow, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("Deletesow(Sow, DataSource) - Exception Caught in deleting sow"
							+ sql);
				}
			}

		}
		return val;
	}

	public static ArrayList getDefaultAssumptionList(DataSource ds) {
		ArrayList defaultAssumptionList = new ArrayList();

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lx_assumption_value from lx_default_assumptions";
			rs = stmt.executeQuery(sql);

			defaultAssumptionList.add(new LabelValue("Select", ""));
			while (rs.next()) {

				defaultAssumptionList.add(new LabelValue(rs
						.getString("lx_assumption_value"), rs
						.getString("lx_assumption_value")));
			}

		} catch (SQLException e) {
			logger.error("getDefaultAssumptionList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDefaultAssumptionList(DataSource) - Exception caught in getting activity library'assumption"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getDefaultAssumptionList(DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getDefaultAssumptionList(DataSource) - Exception caught in getting activity library'assumption"
							+ sql);
				}
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.error("getDefaultAssumptionList(DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getDefaultAssumptionList(DataSource) - Exception caught in getting activity library'assumption"
							+ sql);
				}
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.error("getDefaultAssumptionList(DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getDefaultAssumptionList(DataSource) - Exception caught in getting activity library'assumption"
							+ sql);
				}
			}

		}

		return defaultAssumptionList;
	}

	public static ArrayList getAssumptionlist(DataSource ds,
			String Activitylibrary_Id, String type) {
		ArrayList assumptionlist = new ArrayList();
		Connection conn = null;
		Assumption assumption = null;
		Statement stmt = null;

		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_view_assumption('"
					+ Activitylibrary_Id + "' , '" + type + "' )";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				assumption = new Assumption();
				assumption.setAssumption_Id(rs.getString(1));
				assumption.setId(rs.getString(2));

				assumption.setAssumption(rs.getString(3));
				assumptionlist.add(assumption);
			}

		} catch (SQLException e) {
			logger.error("getAssumptionlist(DataSource, String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAssumptionlist(DataSource, String, String) - Exception caught in getting activity library'assumption"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getAssumptionlist(DataSource, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAssumptionlist(DataSource, String, String) - Exception caught in getting activity library'assumption"
							+ sql);
				}
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.error("getAssumptionlist(DataSource, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAssumptionlist(DataSource, String, String) - Exception caught in getting activity library'assumption"
							+ sql);
				}
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.error("getAssumptionlist(DataSource, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getAssumptionlist(DataSource, String, String) - Exception caught in getting activity library'assumption"
							+ sql);
				}
			}

		}
		return assumptionlist;
	}

	/**
	 * This method is used to add Assumptions
	 * 
	 * @param assumption
	 *            object of Assumption
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int addassumption(Assumption assumption, DataSource ds) {
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;
		Util util = new Util();

		try {
			// Added to change single quotes string to double quotes
			/*
			 * if(assumption.getAssumption() != null) {
			 * if(assumption.getAssumption().indexOf("'") >= 0)
			 * assumption.setAssumption
			 * (util.changeStringToDoubleQuotes(assumption.getAssumption())); }
			 */
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_assumption_manage( ? , ? , ? , ? ,?) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, Integer.parseInt(assumption.getId()));
			cstmt.setString(4, assumption.getAssumption());
			cstmt.setString(5, assumption.getType());

			cstmt.setString(6, "A");

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error("addassumption(Assumption, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addassumption(Assumption, DataSource) - Exception caught in adding assumption"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error("addassumption(Assumption, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addassumption(Assumption, DataSource) - Exception Caught in adding assumption"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("addassumption(Assumption, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addassumption(Assumption, DataSource) - Exception Caught in adding assumption"
							+ sql);
				}
			}

		}
		return val;
	}

	/**
	 * This method is used to update Assumptions
	 * 
	 * @param assumption
	 *            object of Assumption
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int updateAssumption(Assumption assumption, DataSource ds) {
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;
		Util util = new Util();

		try {
			// Added to change single quotes string to double quotes
			/*
			 * if(assumption.getAssumption() != null) {
			 * if(assumption.getAssumption().indexOf("'") >= 0)
			 * assumption.setAssumption
			 * (util.changeStringToDoubleQuotes(assumption.getAssumption())); }
			 */
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_assumption_manage( ? , ? , ? , ? ,?) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(assumption.getAssumption_Id()));
			cstmt.setInt(3, 0);
			cstmt.setString(4, assumption.getAssumption());
			cstmt.setString(5, assumption.getType());
			cstmt.setString(6, "U");

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error("updateAssumption(Assumption, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateAssumption(Assumption, DataSource) - Exception caught in adding assumption"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error("updateAssumption(Assumption, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("updateAssumption(Assumption, DataSource) - Exception Caught in adding assumption"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("updateAssumption(Assumption, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("updateAssumption(Assumption, DataSource) - Exception Caught in adding assumption"
							+ sql);
				}
			}

		}
		return val;
	}

	/**
	 * This method is used to delete Assumptions
	 * 
	 * @param assumption
	 *            object of Assumption
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int deleteAssumption(Assumption assumption, DataSource ds) {
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_assumption_manage( ? , ? , ? , ? ,?) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(assumption.getAssumption_Id()));
			cstmt.setInt(3, 0);
			cstmt.setString(4, "");
			cstmt.setString(5, "");
			cstmt.setString(6, "D");

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (SQLException e) {
			logger.error("deleteAssumption(Assumption, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteAssumption(Assumption, DataSource) - Exception caught in deleting assumption"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sql) {
				logger.error("deleteAssumption(Assumption, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteAssumption(Assumption, DataSource) - Exception Caught in deleting assumption"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("deleteAssumption(Assumption, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteAssumption(Assumption, DataSource) - Exception Caught in deleting assumption"
							+ sql);
				}
			}

		}
		return val;
	}

	/**
	 * This method is used to retrieve material resource for passed resource id
	 * used while refreshing the page
	 * 
	 * @param Id
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return MaterialResource This method returns MaterialResource
	 */
	public static MaterialResource getMaterialResource(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		MaterialResource materialresource = new MaterialResource();

		ArrayList materiallist = new java.util.ArrayList();

		materiallist.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select iv_ct_name , iv_mf_id , iv_mf_name , iv_rp_mfg_part_number , "
							+ "iv_rp_cns_part_number , iv_cost , iv_rp_sell_quantity , iv_rp_minimum_quantity from "
							+ "lx_resource_hierarchy_01 where iv_ct_type_desc like 'material%' and iv_rp_id="
							+ Id);

			if (rs.next()) {
				materialresource.setMaterialtype(rs.getString(1));
				materialresource.setManufacturername(rs.getString(3));
				materialresource.setManufacturerpartnumber(rs.getString(4));
				materialresource.setCnspartnumber(rs.getString(5));
				materialresource.setEstimatedunitcost(rs.getString(6));
				materialresource.setSellablequantity(rs.getString(7));

				materialresource.setMinimumquantity(rs.getString(8));
			}
		} catch (Exception e) {
			logger.error("getMaterialResource(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMaterialResource(String, DataSource) - Exception caught in rerieving material resource"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMaterialResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMaterialResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMaterialResource(String, DataSource) - exception ignored",
						sql);
			}

		}
		return materialresource;
	}

	/**
	 * This method is used to retrieve labor resource for passed resource id
	 * used while refreshing the page
	 * 
	 * @param Id
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return LaborResource This method returns LaborResource
	 */

	public static LaborResource getLaborResource(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		LaborResource laborresource = new LaborResource();

		ArrayList laborlist = new java.util.ArrayList();

		laborlist.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select iv_ct_name , iv_rp_cns_part_number , iv_cost , iv_rp_sell_quantity , "
							+ "iv_rp_minimum_quantity from lx_resource_hierarchy_01 where iv_ct_type_desc like 'labor%' and iv_rp_id="
							+ Id);

			if (rs.next()) {
				laborresource.setLabortype(rs.getString(1));

				laborresource.setCnspartnumber(rs.getString(2));
				laborresource.setEstimatedhourlybasecost(rs.getString(3));
				laborresource.setSellablequantity(rs.getString(4));
				laborresource.setMinimumquantity(rs.getString(5));

			}
		} catch (Exception e) {
			logger.error("getLaborResource(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getLaborResource(String, DataSource) - Exception caught in rerieving laborresource"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getLaborResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getLaborResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getLaborResource(String, DataSource) - exception ignored",
						sql);
			}

		}
		return laborresource;
	}

	/**
	 * This method is used to retrieve freight resource for passed resource id
	 * used while refreshing the page
	 * 
	 * @param Id
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return FreightResource This method returns FreightResource
	 */

	public static FreightResource getFreightResource(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		FreightResource freightresource = new FreightResource();

		ArrayList freightlist = new java.util.ArrayList();

		freightlist.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select iv_ct_name , iv_rp_cns_part_number , iv_cost , iv_rp_sell_quantity , "
							+ "iv_rp_minimum_quantity from lx_resource_hierarchy_01 where iv_ct_type_desc like 'freight%' and iv_rp_id="
							+ Id);

			if (rs.next()) {
				freightresource.setFreighttype(rs.getString(1));

				freightresource.setCnspartnumber(rs.getString(2));
				freightresource.setEstimatedunitcost(rs.getString(3));
				freightresource.setSellablequantity(rs.getString(4));
				freightresource.setMinimumquantity(rs.getString(5));
			}
		} catch (Exception e) {
			logger.error("getFreightResource(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getFreightResource(String, DataSource) - Exception caught in rerieving freightresource"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getFreightResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getFreightResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getFreightResource(String, DataSource) - exception ignored",
						sql);
			}

		}
		return freightresource;
	}

	/**
	 * This method is used to retrieve travel resource for passed resource id
	 * used while refreshing the page
	 * 
	 * @param Id
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return TravelResource This method returns TravelResource
	 */
	public static TravelResource getTravelResource(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		TravelResource travelresource = new TravelResource();

		ArrayList travellist = new java.util.ArrayList();

		travellist.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select iv_ct_name , iv_rp_cns_part_number , iv_cost , iv_rp_sell_quantity , "
							+ "iv_rp_minimum_quantity from lx_resource_hierarchy_01 where iv_ct_type_desc like 'travel%' and iv_rp_id="
							+ Id);

			if (rs.next()) {
				travelresource.setTraveltype(rs.getString(1));

				travelresource.setCnspartnumber(rs.getString(2));
				travelresource.setEstimatedunitcost(rs.getString(3));
				travelresource.setSellablequantity(rs.getString(4));
				travelresource.setMinimumquantity(rs.getString(5));
			}
		} catch (Exception e) {
			logger.error("getTravelResource(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTravelResource(String, DataSource) - Exception caught in rerieving travelresource"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getTravelResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getTravelResource(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getTravelResource(String, DataSource) - exception ignored",
						sql);
			}

		}
		return travelresource;
	}

	/**
	 * This method is used to add material resource
	 * 
	 * @param materialresource
	 *            object of MaterialResource
	 * @param loginuserid
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int addMaterialResource(MaterialResource materialresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_library_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);
			cstmt.setInt(4, Integer.parseInt(materialresource.getMaterialid()));
			cstmt.setInt(5, Integer.parseInt(materialresource.getActivity_Id()));

			cstmt.setString(6, materialresource.getCnspartnumber());
			cstmt.setFloat(7, Float.parseFloat(materialresource.getQuantity()));
			cstmt.setFloat(8, 0);
			cstmt.setString(9, materialresource.getMaterialtype());

			cstmt.setFloat(10,
					Float.parseFloat(materialresource.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(materialresource.getProformamargin()));

			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addMaterialResource(MaterialResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addMaterialResource(MaterialResource, String, DataSource) - Error Occured in Inserting Material Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addMaterialResource(MaterialResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addMaterialResource(MaterialResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	/**
	 * This method is used to update material resource
	 * 
	 * @param materialresource
	 *            object of MaterialResource
	 * @param loginuserid
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int updateMaterialResource(MaterialResource materialresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_library_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(materialresource.getMaterialid()));
			cstmt.setInt(3,
					Integer.parseInt(materialresource.getMaterialcostlibid()));
			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(materialresource.getActivity_Id()));

			cstmt.setString(6, "");
			cstmt.setFloat(
					7,
					Float.parseFloat(materialresource.getQuantity().replaceAll(
							",", "")));
			cstmt.setFloat(8, Float.parseFloat(materialresource
					.getPrevquantity().replaceAll(",", "")));
			cstmt.setString(9, materialresource.getMaterialtype());

			cstmt.setFloat(10, Float.parseFloat(materialresource
					.getEstimatedunitcost().replaceAll(",", "")));
			cstmt.setFloat(11, Float.parseFloat(materialresource
					.getProformamargin().replaceAll(",", "")));

			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updateMaterialResource(MaterialResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateMaterialResource(MaterialResource, String, DataSource) - Error Occured in Updating Material Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateMaterialResource(MaterialResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updateMaterialResource(MaterialResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	/**
	 * This method is used to add labor resource
	 * 
	 * @param laborresource
	 *            object of LaborResource
	 * @param loginuserid
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */

	public static int addLaborresource(LaborResource laborresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_library_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);
			cstmt.setInt(4, Integer.parseInt(laborresource.getLaborid()));
			cstmt.setInt(5, Integer.parseInt(laborresource.getActivity_Id()));

			cstmt.setString(6, laborresource.getCnspartnumber());
			cstmt.setFloat(7,
					Float.parseFloat(laborresource.getQuantityhours()));
			cstmt.setFloat(8, 0);

			cstmt.setString(9, laborresource.getLabortype());

			cstmt.setFloat(10, Float.parseFloat(laborresource
					.getEstimatedhourlybasecost()));
			cstmt.setFloat(11,
					Float.parseFloat(laborresource.getProformamargin()));

			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");

			/*
			 * System.out.println( "Query  0 , 0 , "+Integer.parseInt(
			 * laborresource.getLaborid() )+" , " + ""+Integer.parseInt(
			 * laborresource.getActivity_Id()
			 * )+" , '"+laborresource.getCnspartnumber()+"' , " +
			 * "'"+Float.parseFloat( laborresource.getQuantityhours()
			 * )+"' , 0 , '"+laborresource.getLabortype()+"' , " +
			 * ""+Float.parseFloat( laborresource.getEstimatedhourlybasecost()
			 * )+" , " + ""+Float.parseFloat( laborresource.getProformamargin()
			 * )+" , 'D' , " + ""+Integer.parseInt ( loginuserid )+" , 'A' ");
			 */

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("addLaborresource(LaborResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addLaborresource(LaborResource, String, DataSource) - Error Occured in Inserting Labor Resource"
						+ e);
			}
			logger.error("addLaborresource(LaborResource, String, DataSource)",
					e);
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addLaborresource(LaborResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addLaborresource(LaborResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	/**
	 * This method is used to update labor resource
	 * 
	 * @param laborresource
	 *            object of LaborResource
	 * @param loginuserid
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int updatelaborresource(LaborResource laborresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_library_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(laborresource.getLaborid()));
			cstmt.setInt(3, Integer.parseInt(laborresource.getLaborcostlibid()));
			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(laborresource.getActivity_Id()));

			cstmt.setString(6, "");
			cstmt.setFloat(7, Float.parseFloat(laborresource.getQuantityhours()
					.replaceAll(",", "")));
			cstmt.setFloat(8, Float.parseFloat(laborresource
					.getPrevquantityhours().replaceAll(",", "")));

			cstmt.setString(9, laborresource.getLabortype());

			cstmt.setFloat(10, Float.parseFloat(laborresource
					.getEstimatedhourlybasecost().replaceAll(",", "")));
			cstmt.setFloat(11, Float.parseFloat(laborresource
					.getProformamargin().replaceAll(",", "")));

			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updatelaborresource(LaborResource, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatelaborresource(LaborResource, String, DataSource) - Error Occured in Updating Labor Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatelaborresource(LaborResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatelaborresource(LaborResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	/**
	 * This method is used to add freight resource
	 * 
	 * @param freightresource
	 *            object of FreightResource
	 * @param loginuserid
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int addfreightresource(FreightResource freightresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_library_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);

			cstmt.setInt(4, Integer.parseInt(freightresource.getFreightid()));
			cstmt.setInt(5, Integer.parseInt(freightresource.getActivity_Id()));

			cstmt.setString(6, freightresource.getCnspartnumber());
			cstmt.setFloat(7, Float.parseFloat(freightresource.getQuantity()));
			cstmt.setFloat(8, 0);

			cstmt.setString(9, freightresource.getFreighttype());

			cstmt.setFloat(10,
					Float.parseFloat(freightresource.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(freightresource.getProformamargin()));

			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addfreightresource(FreightResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addfreightresource(FreightResource, String, DataSource) - Error Occured in Inserting Freight"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addfreightresource(FreightResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addfreightresource(FreightResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	/**
	 * This method is used to update freight resource
	 * 
	 * @param freightresource
	 *            object of FreightResource
	 * @param loginuserid
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int updatefreightresource(FreightResource freightresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_library_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(freightresource.getFreightid()));
			cstmt.setInt(3,
					Integer.parseInt(freightresource.getFreightcostlibid()));

			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(freightresource.getActivity_Id()));

			cstmt.setString(6, "");
			cstmt.setFloat(
					7,
					Float.parseFloat(freightresource.getQuantity().replaceAll(
							",", "")));
			cstmt.setFloat(8, Float.parseFloat(freightresource
					.getPrevquantity().replaceAll(",", "")));

			cstmt.setString(9, freightresource.getFreighttype());

			cstmt.setFloat(10, Float.parseFloat(freightresource
					.getEstimatedunitcost().replaceAll(",", "")));
			cstmt.setFloat(11, Float.parseFloat(freightresource
					.getProformamargin().replaceAll(",", "")));

			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updatefreightresource(FreightResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatefreightresource(FreightResource, String, DataSource) - Error Occured in updating Freight"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatefreightresource(FreightResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatefreightresource(FreightResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	/**
	 * This method is used to add travel resource
	 * 
	 * @param travelresource
	 *            object of TravelResource
	 * @param loginuserid
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int addtravelresource(TravelResource travelresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_library_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);

			cstmt.setInt(4, Integer.parseInt(travelresource.getTravelid()));
			cstmt.setInt(5, Integer.parseInt(travelresource.getActivity_Id()));

			cstmt.setString(6, travelresource.getCnspartnumber());
			cstmt.setFloat(7, Float.parseFloat(travelresource.getQuantity()));
			cstmt.setFloat(8, 0);
			cstmt.setString(9, travelresource.getTraveltype());

			cstmt.setFloat(10,
					Float.parseFloat(travelresource.getEstimatedunitcost()));
			cstmt.setFloat(11,
					Float.parseFloat(travelresource.getProformamargin()));

			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addtravelresource(TravelResource, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addtravelresource(TravelResource, String, DataSource) - Error Occured in Inserting  Travel Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addtravelresource(TravelResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addtravelresource(TravelResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	/**
	 * This method is used to update travel resource
	 * 
	 * @param travelresource
	 *            object of TravelResource
	 * @param loginuserid
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */

	public static int updatetravelresource(TravelResource travelresource,
			String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_library_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(travelresource.getTravelid()));
			cstmt.setInt(3,
					Integer.parseInt(travelresource.getTravelcostlibid()));

			cstmt.setInt(4, 0);
			cstmt.setInt(5, Integer.parseInt(travelresource.getActivity_Id()));

			cstmt.setString(6, "");
			cstmt.setFloat(
					7,
					Float.parseFloat(travelresource.getQuantity().replaceAll(
							",", "")));
			cstmt.setFloat(8, Float.parseFloat(travelresource.getPrevquantity()
					.replaceAll(",", "")));

			cstmt.setString(9, travelresource.getTraveltype());

			cstmt.setFloat(10, Float.parseFloat(travelresource
					.getEstimatedunitcost().replaceAll(",", "")));
			cstmt.setFloat(11, Float.parseFloat(travelresource
					.getProformamargin().replaceAll(",", "")));

			cstmt.setString(12, "");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "U");

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"updatetravelresource(TravelResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updatetravelresource(TravelResource, String, DataSource) - Error Occured in UPDATING Travel Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatetravelresource(TravelResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"updatetravelresource(TravelResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	/**
	 * This method is used to retrieve resources exist for an activity for
	 * resources tabular page
	 * 
	 * @param activity_Id
	 *            object of String
	 * @param type
	 *            object of String
	 * @param resource_id
	 *            object of String
	 * @param querystring
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return ArrayList This method returns ArrayList of all the types of
	 *         resources present within the activity based on type parameter
	 */
	public static ArrayList getMaterialResourcelist(String activity_Id,
			String type, String resource_id, String querystring,
			String session_id, String funcflag, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList resourcelist = new java.util.ArrayList();
		String temp = "";
		String sql1 = "";
		DecimalFormat dfcurmargin = null;
		DecimalFormat dfcur = new DecimalFormat("#,##0.00");
		DecimalFormat dfcurfour = new DecimalFormat("###0.0000");

		if (funcflag.equals("listpage"))
			// dfcurmargin = new DecimalFormat( "0.00000000" );
			dfcurmargin = new DecimalFormat("0.0000000");
		else
			dfcurmargin = new DecimalFormat("0.00");

		if (querystring.equals("")) {
			querystring = "order by iv_rp_name";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (funcflag.equals("detailpage")) {
				sql1 = "select * from lx_material_resource_element_detail('"
						+ activity_Id + "' , '" + resource_id + "' , '" + type
						+ "')" + querystring;
			} else if (funcflag.equals("tempList")) {
				sql1 = "select * from lx_material_resource_element_temp('"
						+ activity_Id + "' , '" + resource_id + "' , '" + type
						+ "', '" + session_id + "')" + querystring;
			} else {
				sql1 = "select * from lx_material_resource_element('"
						+ activity_Id + "' , '" + resource_id + "' , '" + type
						+ "', '" + session_id + "')" + querystring;
			}

			rs = stmt.executeQuery(sql1);

			if (type.equals("M")) {
				MaterialResource materialresource = null;
				while (rs.next()) {
					materialresource = new MaterialResource();

					materialresource.setMaterialid(rs.getString(1));
					materialresource.setMaterialcostlibid(rs.getString(2));

					materialresource.setActivity_Id(rs.getString(3));

					materialresource.setMaterialname(rs.getString(18) + "-"
							+ rs.getString(4));
					materialresource.setMaterialtype(rs.getString(5));
					materialresource.setManufacturername(rs.getString(6));
					materialresource.setManufacturerpartnumber(rs.getString(7));
					materialresource.setCnspartnumber(rs.getString(8));
					materialresource.setQuantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					materialresource.setPrevquantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					materialresource.setEstimatedunitcost(dfcur.format(rs
							.getFloat(10)) + "");
					materialresource.setEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					materialresource.setProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					materialresource.setPriceunit(dfcur.format(rs.getFloat(13))
							+ "");
					materialresource.setPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					materialresource.setSellablequantity(rs.getString(15));
					materialresource.setMinimumquantity(rs.getString(16));
					materialresource.setStatus(rs.getString(17));
					/* for multile resource select start */
					materialresource.setFlag(rs.getString("flag"));
					/* for multile resource select end */
					/* for checkbox checked start */
					materialresource.setFlag_materialid(rs.getString("flag")
							+ rs.getString(1));
					/* for checkbox checked end */

					materialresource.setActivity_Id(rs.getString("lm_at_id"));
					materialresource.setActivity_name(rs
							.getString("lm_at_title"));
					materialresource.setMsa_Id(rs.getString("lm_at_mm_id"));
					materialresource.setMsaname(rs.getString("lo_ot_name"));

					resourcelist.add(materialresource);
				}
			}

			if (type.equals("L")) {
				LaborResource laborresource = null;
				while (rs.next()) {
					laborresource = new LaborResource();
					laborresource.setLaborid(rs.getString(1));
					laborresource.setLabortype(rs.getString(5));

					laborresource.setCnspartnumber(rs.getString(8));

					laborresource.setQuantityhours(dfcurfour.format(rs
							.getFloat(9)) + "");
					laborresource.setPrevquantityhours(dfcurfour.format(rs
							.getFloat(9)) + "");
					laborresource.setLaborname(rs.getString(18) + "-"
							+ rs.getString(4));
					laborresource.setEstimatedhourlybasecost(dfcur.format(rs
							.getFloat(10)) + "");
					laborresource.setEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					laborresource.setProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					laborresource.setPriceunit(dfcur.format(rs.getFloat(13))
							+ "");
					laborresource
							.setPriceextended(dfcur.format(rs.getFloat(14))
									+ "");
					laborresource.setSellablequantity(rs.getString(15));
					laborresource.setMinimumquantity(rs.getString(16));
					laborresource.setStatus(rs.getString(17));
					/* for multile resource select start */
					laborresource.setFlag(rs.getString("flag"));
					/* for multile resource select end */
					/* for checkbox checked start */
					laborresource.setFlag_laborid(rs.getString("flag")
							+ rs.getString(1));
					/* for checkbox checked end */

					laborresource.setActivity_Id(rs.getString("lm_at_id"));
					laborresource.setActivity_name(rs.getString("lm_at_title"));
					laborresource.setMsa_Id(rs.getString("lm_at_mm_id"));
					laborresource.setMsaname(rs.getString("lo_ot_name"));

					resourcelist.add(laborresource);
				}
			}

			if (type.equals("F")) {
				FreightResource freightresource = null;
				while (rs.next()) {
					freightresource = new FreightResource();
					freightresource.setFreightid(rs.getString(1));
					freightresource.setFreightcostlibid(rs.getString(2));

					freightresource.setActivity_Id(rs.getString(3));

					freightresource.setFreightname(rs.getString(18) + "-"
							+ rs.getString(4));
					freightresource.setFreighttype(rs.getString(5));

					freightresource.setCnspartnumber(rs.getString(8));

					freightresource
							.setQuantity(dfcurfour.format(rs.getFloat(9)) + "");
					freightresource.setPrevquantity(dfcurfour.format(rs
							.getFloat(9)) + "");

					freightresource.setEstimatedunitcost(dfcur.format(rs
							.getFloat(10)) + "");
					freightresource.setEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					freightresource.setProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					freightresource.setPriceunit(dfcur.format(rs.getFloat(13))
							+ "");
					freightresource.setPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					freightresource.setSellablequantity(rs.getString(15));
					freightresource.setMinimumquantity(rs.getString(16));
					freightresource.setStatus(rs.getString(17));
					/* for multile resource select start */
					freightresource.setFlag(rs.getString("flag"));
					/* for multile resource select end */
					/* for checkbox checked start */
					freightresource.setFlag_freightid(rs.getString("flag")
							+ rs.getString(1));
					/* for checkbox checked end */

					freightresource.setActivity_Id(rs.getString("lm_at_id"));
					freightresource.setActivity_name(rs
							.getString("lm_at_title"));
					freightresource.setMsa_Id(rs.getString("lm_at_mm_id"));
					freightresource.setMsaname(rs.getString("lo_ot_name"));

					resourcelist.add(freightresource);
				}
			}

			if (type.equals("T")) {
				TravelResource travelresource = null;
				while (rs.next()) {
					travelresource = new TravelResource();
					travelresource.setTravelid(rs.getString(1));
					travelresource.setTravelcostlibid(rs.getString(2));

					travelresource.setActivity_Id(rs.getString(3));

					travelresource.setTravelname(rs.getString(18) + "-"
							+ rs.getString(4));
					travelresource.setTraveltype(rs.getString(5));
					travelresource.setCnspartnumber(rs.getString(8));

					travelresource.setQuantity(dfcurfour.format(rs.getFloat(9))
							+ "");
					travelresource.setPrevquantity(dfcurfour.format(rs
							.getFloat(9)) + "");

					travelresource.setEstimatedunitcost(dfcur.format(rs
							.getFloat(10)) + "");
					travelresource.setEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					travelresource.setProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					travelresource.setPriceunit(dfcur.format(rs.getFloat(13))
							+ "");
					travelresource.setPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					travelresource.setSellablequantity(rs.getString(15));
					travelresource.setMinimumquantity(rs.getString(16));
					travelresource.setStatus(rs.getString(17));
					/* for multile resource select start */
					travelresource.setFlag(rs.getString("flag"));
					/* for multile resource select end */
					/* for checkbox checked start */
					travelresource.setFlag_travelid(rs.getString("flag")
							+ rs.getString(1));
					/* for checkbox checked end */

					travelresource.setActivity_Id(rs.getString("lm_at_id"));
					travelresource
							.setActivity_name(rs.getString("lm_at_title"));
					travelresource.setMsa_Id(rs.getString("lm_at_mm_id"));
					travelresource.setMsaname(rs.getString("lo_ot_name"));

					resourcelist.add(travelresource);
				}
			}

		} catch (Exception e) {
			logger.error(
					"getMaterialResourcelist(String, String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMaterialResourcelist(String, String, String, String, String, String, DataSource) - Exception caught in rerieving material resource list"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMaterialResourcelist(String, String, String, String, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMaterialResourcelist(String, String, String, String, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMaterialResourcelist(String, String, String, String, String, String, DataSource) - exception ignored",
						sql);
			}

		}
		return resourcelist;
	}

	/**
	 * This method is used to delete resource
	 * 
	 * @param Id
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int deleteresource(String Id, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int val = -1;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_library_delete_01( ?  ) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(Id));

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("deleteresource(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteresource(String, DataSource) - Exception in DELETING resource"
						+ e);
			}
		} finally {
			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sqle) {
				logger.error("deleteresource(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteresource(String, DataSource) - Exception in DELETING resource"
							+ sqle);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error("deleteresource(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteresource(String, DataSource) - Exception in DELETING resource"
							+ sqle);
				}
			}

		}
		return val;
	}

	/* customer labor rates */

	public static int checkCategoryname(String msa_id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int checkcategory = 0;
		String s = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			s = "select dbo.lm_check_netmedx_category(" + msa_id + ")";

			rs = stmt.executeQuery(s);
			if (rs.next()) {
				checkcategory = rs.getInt(1);
			}
		} catch (Exception e) {
			logger.error("checkCategoryname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkCategoryname(String, DataSource) - Exception caught in checking category name"
						+ e);
			}
		} finally {
			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"checkCategoryname(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"checkCategoryname(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"checkCategoryname(String, DataSource) - exception ignored",
						sql);
			}

		}

		return checkcategory;
	}

	public static ArrayList getMasterlibactivities(String MSA_Id, DataSource ds) {
		ArrayList masterlibactivities = new ArrayList();
		Connection conn = null;
		ActivityLibrary activity = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_master_tolib_copy_tab('"
					+ MSA_Id + "')";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				activity = new ActivityLibrary();

				activity.setActivity_Id(rs.getString("lm_at_id"));
				activity.setName(rs.getString("lm_at_title"));
				activity.setStatus(rs.getString("lm_at_status"));

				activity.setActivitytype(rs.getString("lm_at_type"));

				activity.setActivitytypecombo(rs.getString("lm_at_type")
						.charAt(0) + "");

				activity.setQuantity(rs.getString("lm_at_quantity"));

				activity.setEstimatedmaterialcost(rs
						.getString("lx_cle_material_cost"));
				activity.setEstimatedcnslabourcost(rs
						.getString("lx_cle_cns_field_labor_cost"));
				activity.setEstimatedcontractlabourcost(rs
						.getString("lx_cle_ctr_field_labor_cost"));

				activity.setEstimatedfreightcost(rs
						.getString("lx_cle_frieght_cost"));
				activity.setEstimatedtravelcost(rs
						.getString("lx_cle_travel_cost"));
				activity.setEstimatedtotalcost(rs
						.getString("lx_cle_total_cost"));
				activity.setExtendedprice(rs.getString("lx_cle_total_price"));
				activity.setProformamargin(rs.getString("lx_cle_margin"));
				activity.setCategory_type(rs.getString("lm_al_cat_name"));
				activity.setDuration(rs.getString("lm_at_duration"));
				activity.setCat_Id(rs.getString("lm_al_cat_id"));

				masterlibactivities.add(activity);
			}

		} catch (SQLException e) {
			logger.error("getMasterlibactivities(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMasterlibactivities(String, DataSource) - Exception caught in getting Master Library Activities"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getMasterlibactivities(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getMasterlibactivities(String, DataSource) - Exception caught in getting Master Library Activities"
							+ sql);
				}
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.error("getMasterlibactivities(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getMasterlibactivities(String, DataSource) - Exception caught in getting Master Library Activities"
							+ sql);
				}
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.error("getMasterlibactivities(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getMasterlibactivities(String, DataSource) - Exception caught in getting Master Library Activities"
							+ sql);
				}
			}

		}
		return masterlibactivities;
	}

	public static int addMastertoactivitylib(ActivityLibrary activity,
			String loginuserid, DataSource ds) throws Exception {
		Connection conn = null;
		CallableStatement cstmt = null;
		Util util = new Util();
		ResultSet rs = null;
		int val = -1;
		try {
			// Added to change single quotes string to double quotes
			if (activity.getName() != null) {
				if (activity.getName().indexOf("'") >= 0)
					activity.setName(util.changeStringToDoubleQuotes(activity
							.getName()));
			}
			/*
			 * System.out.println("2:::::::::::"+Integer.parseInt(activity.getMsa_Id
			 * ()));
			 * System.out.println("3:::::::::::"+Integer.parseInt(activity.
			 * getCat_Id()));
			 * System.out.println("4:::::::::::"+Integer.parseInt(
			 * activity.getActivity_Id()));
			 * System.out.println("5:::::::::::"+activity.getName());
			 * System.out.println("6:::::::::::D");
			 * System.out.println("7:::::::::::"
			 * +Integer.parseInt(activity.getDuration()));
			 * System.out.println("8:::::::::::"
			 * +Integer.parseInt(activity.getQuantity()));
			 * System.out.println("9:::::::::::"
			 * +activity.getActivitytypecombo());
			 * System.out.println("10:::::::::::"
			 * +Integer.parseInt(loginuserid));
			 * System.out.println("11:::::::::::A");
			 */
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_mastertolibcopy_manage_01(?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, Integer.parseInt(activity.getMsa_Id()));
			cstmt.setInt(3, Integer.parseInt(activity.getCat_Id()));
			cstmt.setInt(4, Integer.parseInt(activity.getActivity_Id()));
			cstmt.setString(5, activity.getName());
			cstmt.setString(6, "D");
			cstmt.setInt(7, Integer.parseInt(activity.getDuration()));
			cstmt.setInt(8, Integer.parseInt(activity.getQuantity()));
			cstmt.setString(9, activity.getActivitytypecombo());
			cstmt.setInt(10, Integer.parseInt(loginuserid));

			cstmt.setString(11, "A");
			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addMastertoactivitylib(ActivityLibrary, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addMastertoactivitylib(ActivityLibrary, String, DataSource) - Error Occured in Inserting Activity Library"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException sql) {
				logger.warn(
						"addMastertoactivitylib(ActivityLibrary, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addMastertoactivitylib(ActivityLibrary, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addMastertoactivitylib(ActivityLibrary, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static String Checkformasterlibcopy(String msa_id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String checkformastercopy = "";
		String s = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			s = "select dbo.func_check_for_masterlibrary(" + msa_id + ")";

			rs = stmt.executeQuery(s);
			if (rs.next()) {
				checkformastercopy = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("Checkformasterlibcopy(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("Checkformasterlibcopy(String, DataSource) - Exception caught in checking master lib copy"
						+ e);
			}
		} finally {
			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"Checkformasterlibcopy(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"Checkformasterlibcopy(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"Checkformasterlibcopy(String, DataSource) - exception ignored",
						sql);
			}

		}

		return checkformastercopy;
	}

	/**
	 * This method is used to retrieve activity status
	 * 
	 * @param Id
	 *            id of activity
	 * @param ds
	 *            object of DataSource
	 * @return String
	 */

	public static String getActivitylibraryname(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String activitylibraryname = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lm_at_title = isnull( lm_at_title , '' ) from lm_activity_library where lm_at_id="
					+ Id;
			rs = stmt.executeQuery(sql);
			if (rs.next())
				activitylibraryname = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getActivitylibraryname(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivitylibraryname(String, DataSource) - Exception in retrieving Activity library name"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitylibraryname(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitylibraryname(String, DataSource) - Exception in retrieving Activity library name"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitylibraryname(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitylibraryname(String, DataSource) - Exception in retrieving Activity library name"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getActivitylibraryname(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getActivitylibraryname(String, DataSource) - Exception in retrieving Activity library name"
							+ sql);
				}
			}

		}
		return activitylibraryname;
	}

	public static int deleteTempResources(String sessionid, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		if (logger.isDebugEnabled()) {
			logger.debug("deleteTempResources(String, DataSource) - in side ---------------------------------------------------------------------------------------------");
		}
		ResultSet rs = null;
		int val = -1;
		java.util.Date dateIn = new java.util.Date();
		try {
			conn = ds.getConnection();

			cstmt = conn.prepareCall("{?=call dbo.temp_resource_delete_01(?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setString(2, sessionid);

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("deleteTempResources(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteTempResources(String, DataSource) - Error Occured in deleteTempResources"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (SQLException sql) {
				logger.warn(
						"deleteTempResources(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"deleteTempResources(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"deleteTempResources(String, DataSource) - exception ignored",
						sql);
			}

		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("deleteTempResources(String, DataSource) - **** deleteTempResources :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return val;
	}

	public static String[] checkTempResourcesAM(String activity_Id,
			String type, String resource_id, String querystring,
			String sessionid, DataSource ds) {

		int cnt = 0;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		String savedresources[] = null;
		if (querystring.equals("")) {
			querystring = "order by iv_rp_name";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			// sql =
			// "select count(*) as cnt from lp_resource_tab('"+activity_Id+"' , '"+resource_id+"' , '"+type+"','"+sessionid+"')";
			sql = "select count(*) as cnt from lx_material_resource_element('"
					+ activity_Id + "' , '" + resource_id + "' , '" + type
					+ "', '" + sessionid + "')";
			// System.out.println("checkTempResourcesAM SQL"+sql);
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				cnt = rs.getInt("cnt");
			}
			savedresources = new String[cnt];

			sql = "select * from lx_material_resource_element('" + activity_Id
					+ "' , '" + resource_id + "' , '" + type + "', '"
					+ sessionid + "')" + querystring;

			// System.out.println("checkTempResources SQL "+sql);
			rs = stmt.executeQuery(sql);
			int i = 0;
			while (rs.next()) {
				if (rs.getString("flag").equals("T")) {
					savedresources[i] = rs.getString("flag")
							+ rs.getString("lm_rs_id");
				}

				i++;
			}

		} catch (Exception e) {
			logger.error(
					"checkTempResourcesAM(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkTempResourcesAM(String, String, String, String, String, DataSource) - Exception caught in rerieving checkTempResourcesAM "
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException s) {
				logger.warn(
						"checkTempResourcesAM(String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"checkTempResourcesAM(String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"checkTempResourcesAM(String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

		}

		return savedresources;
	}

	public static ArrayList<LabelValue> getActivityCopyMsaList(DataSource ds,
			String msaId) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		ArrayList<LabelValue> msaList = new ArrayList<LabelValue>();
		msaList.add(new LabelValue("Select", "0"));

		sql = "select * from func_get_msalist_for_AM_activity_copy() where msa_id <> "
				+ msaId + " order by lm_sq, msa_name";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				msaList.add(new LabelValue(rs.getString("msa_name"), rs
						.getString("msa_id")));
			}

		} catch (Exception e) {
			logger.error(
					"getActivityCopyMsaList - Error retrieving MSA list for copying Activities of an MSA to another MSA",
					e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return msaList;
	}

	public static ArrayList getAMResourceLibList(String activity_Id,
			String type, String resource_id, String querystring,
			String session_id, String funcflag, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList resourcelist = new java.util.ArrayList();
		String temp = "";
		String sql1 = "";
		DecimalFormat dfcurmargin = null;
		DecimalFormat dfcur = new DecimalFormat("#,##0.00");
		DecimalFormat dfcurfour = new DecimalFormat("###0.0000");

		if (funcflag.equals("listpage"))
			// dfcurmargin = new DecimalFormat( "0.00000000" );
			dfcurmargin = new DecimalFormat("0.0000000");
		else
			dfcurmargin = new DecimalFormat("0.00");

		if (querystring.equals("")) {
			querystring = "order by iv_rp_name";
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (funcflag.equals("detailpage")) {
				sql1 = "select * from lx_material_resource_element_detail('"
						+ activity_Id + "' , '" + resource_id + "' , '" + type
						+ "')" + querystring;
			} else {
				sql1 = "select * from lx_material_resource_element('"
						+ activity_Id + "' , '" + resource_id + "' , '" + type
						+ "', '" + session_id + "')" + querystring;
			}
			rs = stmt.executeQuery(sql1);

			if (type.equals("M") || type.equals("IM") || type.equals("AM")
					|| type.equals("CM")) {
				ResourceMaterial resourcematerial = null;
				while (rs.next()) {
					resourcematerial = new ResourceMaterial();
					resourcematerial.setMaterialid(rs.getString(1));
					resourcematerial.setMaterialCostlibid(rs.getString(2));
					resourcematerial.setActivity_Id(rs.getString(3));
					resourcematerial.setMaterialname(rs.getString(18) + "-"
							+ rs.getString(4));
					if (resourcematerial.getMaterialname().length() > 45) {
						resourcematerial.setMaterialname(resourcematerial
								.getMaterialname().toString().substring(0, 45));
					}
					resourcematerial.setMaterialtype(rs.getString(5));
					resourcematerial.setMaterialManufacturername(rs
							.getString(6));
					resourcematerial.setMaterialManufacturerpartnumber(rs
							.getString(7));
					resourcematerial.setMaterialCnspartnumber(rs.getString(8));
					resourcematerial.setQuantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcematerial.setMaterialQuantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcematerial.setMaterialPrevquantity(dfcurfour
							.format(rs.getFloat(9)) + "");
					resourcematerial.setMaterialEstimatedunitcost(dfcur
							.format(rs.getFloat(10)) + "");
					resourcematerial.setMaterialEstimatedtotalcost(dfcur
							.format(rs.getFloat(11)) + "");
					resourcematerial.setMaterialProformamargin(dfcurmargin
							.format(rs.getFloat(12)) + "");
					resourcematerial.setMaterialPriceunit(dfcur.format(rs
							.getFloat(13)) + "");
					resourcematerial.setMaterialPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					resourcematerial.setMaterialSellablequantity(rs
							.getString(15));
					resourcematerial.setMaterialMinimumquantity(rs
							.getString(16));
					resourcematerial.setMaterialStatus(rs.getString(17));
					resourcematerial.setResourceType(rs.getString(24));
					resourcematerial.setMaterialFlag(rs.getString("flag"));
					resourcematerial.setFlag_materialid(rs.getString("flag")
							+ rs.getString(1));

					resourcelist.add(resourcematerial);
				}
			}

			if (type.equals("L") || type.equals("IL") || type.equals("AL")
					|| type.equals("CL")) {
				ResourceLabor resourcelabor = null;
				while (rs.next()) {
					resourcelabor = new ResourceLabor();
					resourcelabor.setLaborid(rs.getString(1));
					resourcelabor.setLaborcostlibid(rs.getString(2));
					resourcelabor.setActivity_Id(rs.getString(3));
					resourcelabor.setLaborname(rs.getString(18) + "-"
							+ rs.getString(4));
					if (resourcelabor.getLaborname().length() > 45) {
						resourcelabor.setLaborname(resourcelabor.getLaborname()
								.toString().substring(0, 45));
					}
					resourcelabor.setLabortype(rs.getString(5));
					resourcelabor.setLaborCnspartnumber(rs.getString(8));
					resourcelabor.setLaborQuantityhours(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcelabor.setLaborPrevquantityhours(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcelabor.setLaborEstimatedhourlybasecost(dfcur
							.format(rs.getFloat(10)) + "");
					resourcelabor.setLaborEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					resourcelabor.setLaborProformamargin(dfcurmargin.format(rs
							.getFloat(12)) + "");
					resourcelabor.setLaborPriceunit(dfcur.format(rs
							.getFloat(13)) + "");
					resourcelabor.setLaborPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					resourcelabor.setLaborSellablequantity(rs.getString(15));
					resourcelabor.setLaborMinimumquantity(rs.getString(16));
					resourcelabor.setLaborStatus(rs.getString(17));
					resourcelabor.setResourceType(rs.getString(24));
					// resourcelabor.setLaborTransit(rs.getString("lm_rs_transit"));
					resourcelabor.setLaborFlag(rs.getString("flag"));
					resourcelabor.setFlag_laborid(rs.getString("flag")
							+ rs.getString(1));

					if (resourcelabor.getLaborid().indexOf("~") > 0) {
						resourcelabor
								.setLaborid(resourcelabor.getLaborid()
										.substring(
												0,
												resourcelabor.getLaborid()
														.indexOf("~")));
						resourcelabor
								.setClr_detail_id(resourcelabor.getLaborid()
										.substring(
												resourcelabor.getLaborid()
														.indexOf("~") + 1,
												resourcelabor.getLaborid()
														.length()));
					} else {
						resourcelabor.setLaborid(resourcelabor.getLaborid());
						resourcelabor.setClr_detail_id("0");
					}
					// resourcelabor.setTroubleRepairFlag(rs.getString(
					// "lm_rs_trouble_and_repair_flag" ));
					resourcelabor.setTroubleRepairFlag("");
					resourcelist.add(resourcelabor);
				}
			}
			if (type.equals("T") || type.equals("IT") || type.equals("AT")
					|| type.equals("CT")) {

				ResourceTravel resourcetravel = null;
				while (rs.next()) {
					resourcetravel = new ResourceTravel();
					resourcetravel.setTravelid(rs.getString(1));
					resourcetravel.setTravelcostlibid(rs.getString(2));
					resourcetravel.setActivity_Id(rs.getString(3));
					resourcetravel.setTravelname(rs.getString(18) + "-"
							+ rs.getString(4));
					if (resourcetravel.getTravelname().length() > 45) {
						resourcetravel.setTravelname(resourcetravel
								.getTravelname().toString().substring(0, 45));
					}
					resourcetravel.setTraveltype(rs.getString(6));
					resourcetravel.setTravelCnspartnumber(rs.getString(8));
					resourcetravel.setTravelQuantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcetravel.setTravelPrevquantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcetravel.setTravelEstimatedunitcost(dfcur.format(rs
							.getFloat(10)) + "");
					resourcetravel.setTravelEstimatedtotalcost(dfcur.format(rs
							.getFloat(11)) + "");
					resourcetravel.setTravelProformamargin(dfcurmargin
							.format(rs.getFloat(12)) + "");
					resourcetravel.setTravelPriceunit(dfcur.format(rs
							.getFloat(13)) + "");
					resourcetravel.setTravelPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					resourcetravel.setTravelSellablequantity(rs.getString(15));
					resourcetravel.setTravelMinimumquantity(rs.getString(16));
					resourcetravel.setTravelStatus(rs.getString(17));
					resourcetravel.setResourceType(rs.getString(24));
					resourcetravel.setTravelFlag(rs.getString("flag"));
					resourcetravel.setFlag_travelid(rs.getString("flag")
							+ rs.getString(1));
					resourcelist.add(resourcetravel);
				}
			}
			if (type.equals("F") || type.equals("IF") || type.equals("AF")
					|| type.equals("CF")) {

				ResourceFreight resourcefreight = null;
				while (rs.next()) {
					resourcefreight = new ResourceFreight();
					resourcefreight.setFreightid(rs.getString(1));
					resourcefreight.setFreightcostlibid(rs.getString(2));
					resourcefreight.setActivity_Id(rs.getString(3));
					resourcefreight.setFreightname(rs.getString(18) + "-"
							+ rs.getString(4));
					if (resourcefreight.getFreightname().length() > 45) {
						resourcefreight.setFreightname(resourcefreight
								.getFreightname().toString().substring(0, 45));
					}
					resourcefreight.setFreighttype(rs.getString(5));
					resourcefreight.setFreightCnspartnumber(rs.getString(8));
					resourcefreight.setFreightQuantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcefreight.setFreightPrevquantity(dfcurfour.format(rs
							.getFloat(9)) + "");
					resourcefreight.setFreightEstimatedunitcost(dfcur.format(rs
							.getFloat(10)) + "");
					resourcefreight.setFreightEstimatedtotalcost(dfcur
							.format(rs.getFloat(11)) + "");
					resourcefreight.setFreightProformamargin(dfcurmargin
							.format(rs.getFloat(12)) + "");
					resourcefreight.setFreightPriceunit(dfcur.format(rs
							.getFloat(13)) + "");
					resourcefreight.setFreightPriceextended(dfcur.format(rs
							.getFloat(14)) + "");
					resourcefreight
							.setFreightSellablequantity(rs.getString(15));
					resourcefreight.setFreightMinimumquantity(rs.getString(16));
					resourcefreight.setFreightStatus(rs.getString(17));
					resourcefreight.setResourceType(rs.getString(24));
					resourcefreight.setFreightFlag(rs.getString("flag"));
					resourcefreight.setFlag_freightid(rs.getString("flag")
							+ rs.getString(1));
					resourcelist.add(resourcefreight);
				}

			}

		} catch (Exception e) {
			logger.error(
					"getMaterialResourcelist(String, String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMaterialResourcelist(String, String, String, String, String, String, DataSource) - Exception caught in rerieving material resource list"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMaterialResourcelist(String, String, String, String, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMaterialResourcelist(String, String, String, String, String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getMaterialResourcelist(String, String, String, String, String, String, DataSource) - exception ignored",
						sql);
			}

		}
		return resourcelist;
	}

	/**
	 * This method is used to add material resource
	 * 
	 * @param materialresource
	 *            object of MaterialResource
	 * @param loginuserid
	 *            object of String
	 * @param ds
	 *            object of DataSource
	 * @return int This method returns integer value( 0 for success and negative
	 *         value for failure )
	 */
	public static int addMaterialResource(MaterialResource materialresource,
			String activityId, String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_library_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);
			cstmt.setInt(4, Integer.parseInt(materialresource.getMaterialid()));
			cstmt.setInt(5, Integer.parseInt(activityId));

			cstmt.setString(6, materialresource.getCnspartnumber());
			cstmt.setFloat(
					7,
					Float.parseFloat(materialresource.getQuantity().replaceAll(
							",", "")));
			cstmt.setFloat(8, 0);
			cstmt.setString(9, "M");

			cstmt.setFloat(10, Float.parseFloat(materialresource
					.getEstimatedunitcost().replaceAll(",", "")));
			cstmt.setFloat(11, Float.parseFloat(materialresource
					.getProformamargin().replaceAll(",", "")));

			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addMaterialResource(MaterialResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addMaterialResource(MaterialResource, String, DataSource) - Error Occured in Inserting Material Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addMaterialResource(MaterialResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addMaterialResource(MaterialResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int addLaborresource(LaborResource laborresource,
			String activityId, String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_library_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);
			cstmt.setInt(4, Integer.parseInt(laborresource.getLaborid()));
			cstmt.setInt(5, Integer.parseInt(activityId));

			cstmt.setString(6, laborresource.getCnspartnumber());
			cstmt.setFloat(7, Float.parseFloat(laborresource.getQuantityhours()
					.replaceAll(",", "")));
			cstmt.setFloat(8, 0);

			cstmt.setString(9, "L");

			cstmt.setFloat(10, Float.parseFloat(laborresource
					.getEstimatedhourlybasecost().replaceAll(",", "")));
			cstmt.setFloat(11, Float.parseFloat(laborresource
					.getProformamargin().replaceAll(",", "")));

			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");

			/*
			 * System.out.println( "Query  0 , 0 , "+Integer.parseInt(
			 * laborresource.getLaborid() )+" , " + ""+Integer.parseInt(
			 * laborresource.getActivity_Id()
			 * )+" , '"+laborresource.getCnspartnumber()+"' , " +
			 * "'"+Float.parseFloat( laborresource.getQuantityhours()
			 * )+"' , 0 , '"+laborresource.getLabortype()+"' , " +
			 * ""+Float.parseFloat( laborresource.getEstimatedhourlybasecost()
			 * )+" , " + ""+Float.parseFloat( laborresource.getProformamargin()
			 * )+" , 'D' , " + ""+Integer.parseInt ( loginuserid )+" , 'A' ");
			 */

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error("addLaborresource(LaborResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addLaborresource(LaborResource, String, DataSource) - Error Occured in Inserting Labor Resource"
						+ e);
			}
			logger.error("addLaborresource(LaborResource, String, DataSource)",
					e);
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addLaborresource(LaborResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addLaborresource(LaborResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int addtravelresource(TravelResource travelresource,
			String activityId, String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_library_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);

			cstmt.setInt(4, Integer.parseInt(travelresource.getTravelid()));
			cstmt.setInt(5, Integer.parseInt(activityId));

			cstmt.setString(6, travelresource.getCnspartnumber());
			cstmt.setFloat(
					7,
					Float.parseFloat(travelresource.getQuantity().replaceAll(
							",", "")));
			cstmt.setFloat(8, 0);
			cstmt.setString(9, "T");

			cstmt.setFloat(10, Float.parseFloat(travelresource
					.getEstimatedunitcost().replaceAll(",", "")));
			cstmt.setFloat(11, Float.parseFloat(travelresource
					.getProformamargin().replaceAll(",", "")));

			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");

			cstmt.execute();
			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addtravelresource(TravelResource, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addtravelresource(TravelResource, String, DataSource) - Error Occured in Inserting  Travel Resource"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addtravelresource(TravelResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addtravelresource(TravelResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}

	public static int addfreightresource(FreightResource freightresource,
			String activityId, String loginuserid, DataSource ds) {
		int val = -1;
		Connection conn = null;
		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lm_resource_library_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);
			cstmt.setInt(3, 0);

			cstmt.setInt(4, Integer.parseInt(freightresource.getFreightid()));
			cstmt.setInt(5, Integer.parseInt(activityId));

			cstmt.setString(6, freightresource.getCnspartnumber());
			cstmt.setFloat(
					7,
					Float.parseFloat(freightresource.getQuantity().replaceAll(
							",", "")));
			cstmt.setFloat(8, 0);

			cstmt.setString(9, "F");

			cstmt.setFloat(10, Float.parseFloat(freightresource
					.getEstimatedunitcost().replaceAll(",", "")));
			cstmt.setFloat(11, Float.parseFloat(freightresource
					.getProformamargin().replaceAll(",", "")));

			cstmt.setString(12, "D");
			cstmt.setInt(13, Integer.parseInt(loginuserid));
			cstmt.setString(14, "A");

			cstmt.execute();

			val = cstmt.getInt(1);
		}

		catch (Exception e) {
			logger.error(
					"addfreightresource(FreightResource, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addfreightresource(FreightResource, String, DataSource) - Error Occured in Inserting Freight"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"addfreightresource(FreightResource, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"addfreightresource(FreightResource, String, DataSource) - exception ignored",
						sql);
			}

		}

		return val;
	}
}
