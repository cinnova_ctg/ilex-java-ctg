/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: A bean containing getter/setter methods for material resource 
*
*/

package com.mind.dao.AM;

public class ResourceMaterial 
{
	private String activity_Id = null;
	private String activity_name = null;
	
	private String materialid = null;
	private String materialCostlibid = null;
	private String materialname = null;
	private String materialtype = null;
	
	
	private String materialManufacturername = null;
	private String materialManufacturerpartnumber = null;
	private String materialCnspartnumber = null;
	
	private String quantity = null;
	private String materialQuantity = null;
	private String materialPrevquantity = null;
	private String materialPriceextended = null;
	private String materialEstimatedunitcost = null;
	private String materialEstimatedtotalcost = null;
	private String materialProformamargin = null;
	private String materialPriceunit = null;
	
	
	private String materialSellablequantity = null;
	private String materialMinimumquantity = null;
	private String materialStatus = null;
	
	private String addendum_id = null;
	
	private String materialFlag = null; // for resource from temporary table or not
	private String flag_materialid = null; //for setting checkbox checked
	
	private String msa_Id = null;
	private String msaname = null;
	private String resourceType = null;
	
	
	public String getMaterialFlag() {
		return materialFlag;
	}
	public void setMaterialFlag(String flag) {
		this.materialFlag = flag;
	}
	public String getActivity_Id() {
		return activity_Id;
	}
	public void setActivity_Id( String activity_Id ) {
		this.activity_Id = activity_Id;
	}
	
	
	public String getMaterialCnspartnumber() {
		return materialCnspartnumber;
	}
	public void setMaterialCnspartnumber( String cnspartnumber ) {
		this.materialCnspartnumber = cnspartnumber;
	}
	
	
	public String getMaterialEstimatedtotalcost() {
		return materialEstimatedtotalcost;
	}
	public void setMaterialEstimatedtotalcost( String estimatedtotalcost ) {
		this.materialEstimatedtotalcost = estimatedtotalcost;
	}
	
	
	public String getMaterialEstimatedunitcost() {
		return materialEstimatedunitcost;
	}
	public void setMaterialEstimatedunitcost( String estimatedunitcost ) {
		this.materialEstimatedunitcost = estimatedunitcost;
	}
	
	
	public String getMaterialManufacturername() {
		return materialManufacturername;
	}
	public void setMaterialManufacturername( String manufacturername ) {
		this.materialManufacturername = manufacturername;
	}
	
	
	public String getMaterialManufacturerpartnumber() {
		return materialManufacturerpartnumber;
	}
	public void setMaterialManufacturerpartnumber( String manufacturerpartnumber ) {
		this.materialManufacturerpartnumber = manufacturerpartnumber;
	}
	
	
	public String getMaterialid() {
		return materialid;
	}
	public void setMaterialid( String materialid ) {
		this.materialid = materialid;
	}
	
	
	public String getMaterialCostlibid() {
		return materialCostlibid;
	}
	public void setMaterialCostlibid( String materialcostlibid ) {
		this.materialCostlibid = materialcostlibid;
	}
	
	
	public String getMaterialname() {
		return materialname;
	}
	public void setMaterialname( String materialname ) {
		this.materialname = materialname;
	}
	
	
	public String getMaterialtype() {
		return materialtype;
	}
	public void setMaterialtype( String materialtype ) {
		this.materialtype = materialtype;
	}
	

	
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity( String quantity ) {
		this.quantity = quantity;
	}

	public String getMaterialPriceunit() {
		return materialPriceunit;
	}
	public void setMaterialPriceunit(String priceunitMaterial) {
		this.materialPriceunit = priceunitMaterial;
	}
	public String getMaterialProformamargin() {
		return materialProformamargin;
	}
	public void setMaterialProformamargin(String proformamarginMaterial) {
		this.materialProformamargin = proformamarginMaterial;
	}
	public String getMaterialPrevquantity() {
		return materialPrevquantity;
	}
	public void setMaterialPrevquantity( String prevquantity ) {
		this.materialPrevquantity = prevquantity;
	}
	
	
	public String getMaterialMinimumquantity() {
		return materialMinimumquantity;
	}
	public void setMaterialMinimumquantity( String minimumquantity ) {
		this.materialMinimumquantity = minimumquantity;
	}
	
	
	public String getMaterialSellablequantity() {
		return materialSellablequantity;
	}
	public void setMaterialSellablequantity( String sellablequantity ) {
		this.materialSellablequantity = sellablequantity;
	}
	
	
	public String getMaterialStatus() {
		return materialStatus;
	}
	public void setMaterialStatus( String status ) {
		this.materialStatus = status;
	}
	
	
	public String getAddendum_id() {
		return addendum_id;
	}
	public void setAddendum_id( String addendum_id ) {
		this.addendum_id = addendum_id;
	}
	public String getFlag_materialid() {
		return flag_materialid;
	}
	public void setFlag_materialid(String flag_materialid) {
		this.flag_materialid = flag_materialid;
	}
	public String getActivity_name() {
		return activity_name;
	}
	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}
	public String getMsa_Id() {
		return msa_Id;
	}
	public void setMsa_Id(String msa_Id) {
		this.msa_Id = msa_Id;
	}
	public String getMsaname() {
		return msaname;
	}
	public void setMsaname(String msaname) {
		this.msaname = msaname;
	}
	public String getMaterialQuantity() {
		return materialQuantity;
	}
	public void setMaterialQuantity(String quantityMaterial) {
		this.materialQuantity = quantityMaterial;
	}
	public String getMaterialPriceextended() {
		return materialPriceextended;
	}
	public void setMaterialPriceextended(String materialPriceextended) {
		this.materialPriceextended = materialPriceextended;
	}
	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	
	
	
	
	
	
}
