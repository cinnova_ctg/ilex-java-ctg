package com.mind.dao.AM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class AddClrResleveldao 
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AddClrResleveldao.class);

	public static int addResLevel( String classid , String name, String shortname,int reslevelid,DataSource ds )
	{
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;
		try
		{
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call dbo.clr_reslevel_manage_01( ? , ? , ? , ? ) }" );
			cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			cstmt.setInt( 2 , reslevelid );
			cstmt.setString( 3 , name );
			cstmt.setString( 4 , shortname);
			cstmt.setString( 5 , classid);
			cstmt.execute();	
			val = cstmt.getInt( 1 );
		}
		
		catch( SQLException e )
		{
			logger.error("addResLevel(String, String, String, int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addResLevel(String, String, String, int, DataSource) - Exception caught in adding addResLevel" + e);
			}
		}
		finally
		{
			try
			{
				if( cstmt != null )
				{
					cstmt.close();
				}	
			}
			catch( SQLException sql )
			{
				logger.error("addResLevel(String, String, String, int, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addResLevel(String, String, String, int, DataSource) - Exception Caught in adding addResLevel" + sql);
				}
			}
			
			
			try
			{
				if( conn != null )
				{	
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.error("addResLevel(String, String, String, int, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addResLevel(String, String, String, int, DataSource) - Exception Caught in adding addResLevel" + sql);
				}
			}
			
		}
		return val;	
	}
	
	public static int deleteResLevel( String reslevelid,DataSource ds )
	{
		Connection conn = null;
		int val = -1;
		CallableStatement cstmt = null;
		try
		{
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call dbo.clr_reslevel_delete_01( ? ) }" );
			cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			cstmt.setString( 2 , reslevelid );
			
			cstmt.execute();	
			val = cstmt.getInt( 1 );
		}
		
		catch( SQLException e )
		{
			logger.error("deleteResLevel(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteResLevel(String, DataSource) - Exception caught in deleteResLevel" + e);
			}
		}
		finally
		{
			try
			{
				if( cstmt != null )
				{
					cstmt.close();
				}	
			}
			catch( SQLException sql )
			{
				logger.error("deleteResLevel(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteResLevel(String, DataSource) - Exception Caught in deleteResLevel" + sql);
				}
			}
			
			
			try
			{
				if( conn != null )
				{	
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.error("deleteResLevel(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("deleteResLevel(String, DataSource) - Exception Caught in deleteResLevel" + sql);
				}
			}
			
		}
		return val;	
	}
	
	
	public  static String[] getResourcelevelName(String resid, DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String res[]=new String[2];
		int resourceid=0;
		
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			if(resid!=null)
			{
				resourceid=Integer.parseInt(resid);
			}
			
			String sql="select clr_reslevel_name, clr_reslevel_short_name from clr_reslevel where clr_reslevel_id="+resourceid	;
			//System.out.println("SQL "+sql);
			rs = stmt.executeQuery(sql);
			if(rs.next())
			{
				res[0]=rs.getString("clr_reslevel_name");
				res[1]=rs.getString("clr_reslevel_short_name");
			}
			
		}
		catch(Exception e){
			logger.error("getResourcelevelName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourcelevelName(String, DataSource) - Error occured during getResourcelevelName");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getResourcelevelName(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getResourcelevelName(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getResourcelevelName(String, DataSource) - exception ignored", e);
}
			
		}
		return res;	
	}
	
		

}
