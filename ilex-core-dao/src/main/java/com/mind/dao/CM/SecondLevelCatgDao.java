package com.mind.dao.CM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.cm.AddPocBean;
import com.mind.common.DynamicTabularChkBox;



public class SecondLevelCatgDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SecondLevelCatgDao.class);
	
	
	/**
	 * This function will execute the stored procedure to add or update the
	 * new customer and return the result.	 * 
	 */
	public static int addFirstLevelCatg(String id,String type,String element , String action ,DataSource ds)
	{	
		
		Connection conn=null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		String catgid="";
		int retval=0;
		try{
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call lo_organization_top_manage_01(?,?,?,?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			cstmt.setString(2,id);
			cstmt.setString(3,type);
			cstmt.setString(4,element);
			cstmt.setString(5,action);
			cstmt.execute();
			retval = cstmt.getInt(1);
			
		}
		catch(Exception e){
			logger.error("addFirstLevelCatg(String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addFirstLevelCatg(String, String, String, String, DataSource) - Error occured during adding" + e);
			}
		}
		
		
	

		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("addFirstLevelCatg(String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("addFirstLevelCatg(String, String, String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("addFirstLevelCatg(String, String, String, String, DataSource) - exception ignored", e);
}
			
		}return retval;
	}	


	public static int deleteFirstLevelCatg(String id,String type,DataSource ds)
	{
		Connection conn=null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		
		int retval=0;
		String mfgid="";
		try{
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call lo_organization_top_delete_01(?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			
			cstmt.setString(2,id);
			//cstmt.setString(3,type);
			cstmt.execute();
			retval = cstmt.getInt(1);
			
		}
		catch(Exception e){
			logger.error("deleteFirstLevelCatg(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteFirstLevelCatg(String, String, DataSource) - Error occured during deleting  " + e);
			}
			
		}
		finally
		{
				
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("deleteFirstLevelCatg(String, String, DataSource) - exception ignored", e);
}
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("deleteFirstLevelCatg(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("deleteFirstLevelCatg(String, String, DataSource) - exception ignored", e);
}
			
		}
		return retval;
	}
	
	
	public static int deleteSecondLevelCatg(String id,DataSource ds)
	{
		Connection conn=null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		
		int retval=0;
		String mfgid="";
		try{
			conn=ds.getConnection();
			cstmt=conn.prepareCall("{?=call lo_organization_main_delete_01(?)}");
			cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
			
			cstmt.setString(2,id);
			//cstmt.setString(3,type);
			cstmt.execute();
			retval = cstmt.getInt(1);
			
		}
		catch(Exception e){
			logger.error("deleteSecondLevelCatg(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteSecondLevelCatg(String, DataSource) - Error occured during deleting  " + e);
			}
			
		}
		finally
		{
				
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("deleteSecondLevelCatg(String, DataSource) - exception ignored", e);
}
			
			try
			{
				if ( cstmt!= null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("deleteSecondLevelCatg(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("deleteSecondLevelCatg(String, DataSource) - exception ignored", e);
}
			
		}
		return retval;
	}
	
	public  static String getCustomerName(String catid, DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String resourcetype=null;
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			
			
			String sql="select lo_ot_name from lo_organization_top where lo_ot_id="+catid	;
			rs = stmt.executeQuery(sql);
			if(rs.next())
			{
				resourcetype=rs.getString("lo_ot_name");
			}
			
		}
		catch(Exception e){
			logger.error("getCustomerName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomerName(String, DataSource) - Error occured during adding");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getCustomerName(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getCustomerName(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getCustomerName(String, DataSource) - exception ignored", e);
}
			
		}
		return resourcetype;	
	}	

	public  static String getCustomerName(String custid,String org_type, DataSource ds)
	{
		
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		String custname="";
		try{
			conn=ds.getConnection();
			stmt=conn.createStatement();
			
			
			
			String sql="select lo_ot_name from lo_organization_top where lo_ot_id='"+custid+"'and lo_ot_type='"+org_type+"'";
			rs = stmt.executeQuery(sql);
			while(rs.next())
			{
				custname=rs.getString("lo_ot_name");
			}
			
		}
		catch(Exception e){
			logger.error("getCustomerName(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomerName(String, String, DataSource) - Error occured during adding");
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getCustomerName(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt!= null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getCustomerName(String, String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn!= null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getCustomerName(String, String, DataSource) - exception ignored", e);
}
			
		}
		return custname;	
	}	
		
	
		
		public  static String getCategoryName(String catid, DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String catgname="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				
				
				String sql="select iv_ct_name from iv_category where iv_ct_id="+catid	;
				rs = stmt.executeQuery(sql);
				while(rs.next())
				{
					 catgname=rs.getString("iv_ct_name");
				}
				
			}
			catch(Exception e){
			logger.error("getCategoryName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCategoryName(String, DataSource) - Error occured during adding");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getCategoryName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getCategoryName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getCategoryName(String, DataSource) - exception ignored", e);
}
				
			}
			return catgname;	
		}	
			
		
		
		public  static String getCustomerid(String catid,String mfgname, DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String mfgid="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				
				
				String sql="select lo_ot_id from lo_organization_top where lo_ot_name='"+mfgname+"' and lo_ot_type='"+catid+"'";
				rs = stmt.executeQuery(sql);
				while(rs.next())
				{
					 mfgid=rs.getString("lo_ot_id");					 
				}
				
			}
			catch(Exception e){
			logger.error("getCustomerid(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomerid(String, String, DataSource) - Error occured during getting mfgid  ");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getCustomerid(String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getCustomerid(String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getCustomerid(String, String, DataSource) - exception ignored", e);
}
				
			}
			return mfgid;	
		}	
		
		
		
		public  static String getDivisionName(String divisionid, DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String divisionname="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				
				
				String sql="select lo_om_division from lo_organization_main where lo_om_id="+divisionid;
				rs = stmt.executeQuery(sql);
				while(rs.next())
				{
					 divisionname=rs.getString("lo_om_division");
					 if (divisionname==null)
						 divisionname="";
				}				
			}
			catch(Exception e){
			logger.error("getDivisionName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDivisionName(String, DataSource) - Error occured during adding");
			}
			}			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getDivisionName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getDivisionName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getDivisionName(String, DataSource) - exception ignored", e);
}
				
			}
			return divisionname;	
		}	
		
		
		public  static String getThirdcatgName(String ppsid, DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String thirdcatname="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();			
				
				
				String sql="select iv_sc_name from iv_subcategory where iv_sc_id="+ppsid	;
				rs = stmt.executeQuery(sql);
				while(rs.next())
				{
					thirdcatname=rs.getString("iv_sc_name");
					 
				}
				
			}
			catch(Exception e){
			logger.error("getThirdcatgName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getThirdcatgName(String, DataSource) - Error occured during updating");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getThirdcatgName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getThirdcatgName(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getThirdcatgName(String, DataSource) - exception ignored", e);
}
				
			}
			return thirdcatname;	
		}
		
		
		
		public  static String getPpsid(String mfgid,String ppsname, DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String ppsid="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				
				
				String sql="select iv_sc_id from iv_subcategory where iv_sc_name='"+ppsname+"' and iv_sc_mf_id="+mfgid;
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					 ppsid=rs.getString("iv_sc_id");
					 
				}
				
			}
			catch(Exception e){
			logger.error("getPpsid(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPpsid(String, String, DataSource) - Error occured during getting ppsid  ");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getPpsid(String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getPpsid(String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getPpsid(String, String, DataSource) - exception ignored", e);
}
				
			}
			return ppsid;	
		}	
		
		
		
		public static void updateThirdLvlCategory(String index,String ppstype ,DataSource ds)
		{
			
			Connection conn = null;
			Statement stmt = null;
			
			String sql="";
			
			try
			{
				conn = ds.getConnection();
				stmt = conn.createStatement();
				sql="update iv_subcategory set iv_sc_name='"+ppstype+"' where iv_sc_id=" +index;
				stmt.executeUpdate(sql);
				
			}
			catch( Exception e )
			{
			logger.error("updateThirdLvlCategory(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateThirdLvlCategory(String, String, DataSource) - Exception caught" + e);
			}
			}
			finally
			{
							
				try
				{
					stmt.close();
				}
				catch( SQLException e )
				{
				logger.warn("updateThirdLvlCategory(String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					conn.close();
				}
				catch( SQLException e )
				{
				logger.warn("updateThirdLvlCategory(String, String, DataSource) - exception ignored", e);
}
				
			}
			
		}
		
		
		
		
		
		
		public static int deleteThirdLevelCatg(String iv_sc_id,DataSource ds)
		{
			Connection conn=null;
			CallableStatement cstmt = null;
			ResultSet rs=null;
			
			int retval=0;
			String mfgid="";
			try{
				conn=ds.getConnection();
				cstmt=conn.prepareCall("{?=call iv_subcategory_delete_01(?)}");
				cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
				
				cstmt.setString(2,iv_sc_id);
				
				cstmt.execute();
				retval = cstmt.getInt(1);
				
			}
			catch(Exception e){
			logger.error("deleteThirdLevelCatg(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteThirdLevelCatg(String, DataSource) - Error occured during deleting thirdcatg  " + e);
			}
				
			}
			finally
			{
					
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("deleteThirdLevelCatg(String, DataSource) - exception ignored", e);
}
				
				try
				{
					if ( cstmt!= null ) 
						cstmt.close();
				}
				catch( Exception e )
				{
				logger.warn("deleteThirdLevelCatg(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("deleteThirdLevelCatg(String, DataSource) - exception ignored", e);
}
				
			}
			return retval;
		}	
				

		
		public static int addItems(int iv_rp_id, int iv_rp_sc_id, String iv_rp_name, String iv_rp_mfg_part_number, String iv_rp_sell_quantity, String iv_rp_minimum_quantity, String iv_rp_units, String iv_rp_sell_quantity_weight_in_lbs, String iv_rp_alt_identifier, String iv_rp_status, String iv_cost, int cuser, String action , DataSource ds)
		{
			
		
			
			Connection conn=null;
			CallableStatement cstmt=null;
			ResultSet rs=null;
			int retval=0;
			
			try{
				
				conn=ds.getConnection();
				cstmt=conn.prepareCall("{?=call iv_resource_pool_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
				cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
				cstmt.setInt(2,iv_rp_id);
				cstmt.setInt(3,iv_rp_sc_id);
				cstmt.setString(4,iv_rp_name );
				cstmt.setString(5,iv_rp_mfg_part_number );
				cstmt.setString(6,iv_rp_sell_quantity );
				cstmt.setString(7,iv_rp_minimum_quantity );
				cstmt.setString(8,iv_rp_units );
				cstmt.setString(9,iv_rp_sell_quantity_weight_in_lbs );
				cstmt.setString(10,iv_rp_alt_identifier );
				cstmt.setString(11,iv_rp_status );
			    cstmt.setString(12,iv_cost );
				cstmt.setInt(13,cuser);
				cstmt.setString(14,action);
				cstmt.execute();
				retval = cstmt.getInt(1);
			}
			catch(Exception e){
			logger.error("addItems(int, int, String, String, String, String, String, String, String, String, String, int, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addItems(int, int, String, String, String, String, String, String, String, String, String, int, String, DataSource) - Error occured during adding resource items " + e);
			}
			}
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("addItems(int, int, String, String, String, String, String, String, String, String, String, int, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( cstmt!= null ) 
						cstmt.close();
				}
				catch( Exception e )
				{
				logger.warn("addItems(int, int, String, String, String, String, String, String, String, String, String, int, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("addItems(int, int, String, String, String, String, String, String, String, String, String, int, String, DataSource) - exception ignored", e);
}
				
			}return retval;
		}	
			
		
		public static int deleteResourceItem(int iv_rp_id,int cuser,DataSource ds)
		{
			Connection conn=null;
			CallableStatement cstmt = null;
			ResultSet rs=null;
			
			int retval=0;
			
			try{
				conn=ds.getConnection();
				cstmt=conn.prepareCall("{?=call iv_resource_pool_delete_01(?,?)}");
				cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
				
				cstmt.setInt(2,iv_rp_id);
				cstmt.setInt(3,cuser);
				
				cstmt.execute();
				retval = cstmt.getInt(1);
				
			}
			catch(Exception e){
			logger.error("deleteResourceItem(int, int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteResourceItem(int, int, DataSource) - Error occured during deleting resourceitem  " + e);
			}
				
			}
			finally
			{
					
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("deleteResourceItem(int, int, DataSource) - exception ignored", e);
}
				
				try
				{
					if ( cstmt!= null ) 
						cstmt.close();
				}
				catch( Exception e )
				{
				logger.warn("deleteResourceItem(int, int, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("deleteResourceItem(int, int, DataSource) - exception ignored", e);
}
				
			}
			return retval;
		}	
		
		public  static String getResourceid(String thirdcatgid,String lname,String lidentifier,String status,String sellqty,String minqty,String unit,String bcost,DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String resid="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				
				
				String sql="select iv_rp_id from iv_resource_pool where iv_rp_sc_id="+thirdcatgid  +
						   "and iv_rp_name='"+lname+"' and iv_rp_alt_identifier='"+lidentifier+"' "+
						   "and iv_rp_status='"+status+"' " +
						   "and iv_rp_sell_quantity="+sellqty +
						   "and iv_rp_minimum_quantity="+minqty +
						   "and iv_rp_units='"+unit+"'" + 
						   "and iv_cost='"+bcost+"'" ;
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					resid=rs.getString("iv_rp_id");
					 
				}
				
			}
			catch(Exception e){
			logger.error("getResourceid(String, String, String, String, String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourceid(String, String, String, String, String, String, String, String, DataSource) - Error occured during getting resid  ");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getResourceid(String, String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getResourceid(String, String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getResourceid(String, String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
				
			}
			return resid;	
		}	
		
		
		public  static String[] getFieldlist(String resourceid,DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String[] valuelist=new String[8];
			
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				
				
				String sql="select  iv_rp_sc_id ,iv_rp_name, iv_rp_alt_identifier,iv_rp_status ,iv_rp_sell_quantity, iv_rp_minimum_quantity, iv_rp_units,iv_cost  from iv_resource_pool where iv_rp_id="+resourceid;						   		
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					 valuelist[0]=rs.getString("iv_rp_sc_id");
					 valuelist[1]=rs.getString("iv_rp_name");
					 valuelist[2]=rs.getString("iv_rp_alt_identifier");
					 valuelist[3]=rs.getString("iv_rp_status");
					 valuelist[4]=rs.getString("iv_rp_sell_quantity");
					 valuelist[5]=rs.getString("iv_rp_minimum_quantity");
					 valuelist[6]=rs.getString("iv_rp_units");
					 valuelist[7]=rs.getString("iv_cost");
					 
					
					 
				}
				
			}
			catch(Exception e){
			logger.error("getFieldlist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getFieldlist(String, DataSource) - Error occured during getting fieldlist  ");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getFieldlist(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getFieldlist(String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getFieldlist(String, DataSource) - exception ignored", e);
}
				
			}
			return valuelist;	
		}
		
		
		public static void updateResource(String id3,String lname,String lidentifier,String status,String sellqty,String minqty,String unit,DataSource ds)
		{
			
			Connection conn = null;
			Statement stmt = null;
			
			String sql="";
			
			try
			{
				conn = ds.getConnection();
				stmt = conn.createStatement();
				sql="update iv_resource_pool set iv_rp_name='"+lname+"',iv_rp_alt_identifier='"+lidentifier+"',iv_rp_status='"+status+"',iv_rp_sell_quantity='"+sellqty+"',iv_rp_minimum_quantity='"+minqty+"',iv_rp_units='"+unit+"' where iv_rp_id=" +id3;
				stmt.executeUpdate(sql);
				
			}
			catch( Exception e )
			{
			logger.error("updateResource(String, String, String, String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateResource(String, String, String, String, String, String, String, DataSource) - Exception caught" + e);
			}
			}
			finally
			{
							
				try
				{
					stmt.close();
				}
				catch( SQLException e )
				{
				logger.warn("updateResource(String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
				
				
				try
				{
					conn.close();
				}
				catch( SQLException e )
				{
				logger.warn("updateResource(String, String, String, String, String, String, String, DataSource) - exception ignored", e);
}
				
			}
			
		}
		
		
		public  static String getCnspartno(int resid,DataSource ds)
		{
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String cnsno="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				
				
				
				String sql="select iv_rp_cns_part_number from lx_resource_hierarchy_01 where iv_rp_id="+resid ;
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					cnsno=rs.getString("iv_rp_cns_part_number");
					 
				}
				
			}
			catch(Exception e){
			logger.error("getCnspartno(int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCnspartno(int, DataSource) - Error occured during getting cnspartno ");
			}
			}
			
			finally
			{
				try
				{
					if ( rs!= null ) 
					{
						rs.close();		
					}
				}
				
				catch( Exception e )
				{
				logger.warn("getCnspartno(int, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( stmt!= null ) 
						stmt.close();
				}
				catch( Exception e )
				{
				logger.warn("getCnspartno(int, DataSource) - exception ignored", e);
}
				
				
				try
				{
					if ( conn!= null) 
						conn.close();
				}
				catch( Exception e )
				{
				logger.warn("getCnspartno(int, DataSource) - exception ignored", e);
}
				
			}
			return cnsno;
		}
		
		public  static String getStatusName(String statusid,DataSource ds)
		{
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String statusname="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				String sql="select cc_status_discription  from cc_status_master where cc_status_code='"+statusid+"'" ;
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					statusname=rs.getString("cc_status_discription");
				
				}
				
			}
			catch(Exception e){
			logger.error("getStatusName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStatusName(String, DataSource) - Error occured during getting statusname ");
			}
			
			}
			finally
			{
				try
				{
					if ( rs!= null )
					{
						rs.close();
						
					}
					
				}
				
				catch( Exception e )
				{
				logger.warn("getStatusName(String, DataSource) - exception ignored", e);
					
				}
				try
				{
					if ( stmt!= null )					
							stmt.close();
					}
				catch( Exception e )
				{
				logger.warn("getStatusName(String, DataSource) - exception ignored", e);
					
				}
				
				try
				{
					if ( conn!= null)
						conn.close();
					}
					catch( Exception e )
					{
				logger.warn("getStatusName(String, DataSource) - exception ignored", e);
						
					}
					
				}
			return statusname;
			
		}
		
		public  static String[] getDivisonInfo(String divison_id,DataSource ds)
		{
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String[] statusname={null,null,null,null,null,null,null};
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				String sql="select * from lo_address where lo_ad_id "+
				"in( select lo_om_ad_id1 from lo_organization_main where lo_om_id='"+divison_id+"')";
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					statusname[0]=rs.getString("lo_ad_id");
					statusname[1]=rs.getString("lo_ad_address1");
					statusname[2]=rs.getString("lo_ad_address2");
					statusname[3]=rs.getString("lo_ad_city");
					statusname[4]=rs.getString("lo_ad_state");
					statusname[5]=rs.getString("lo_ad_zip_code");
					statusname[6]=rs.getString("lo_ad_country");
				}
					
				
			}
			catch(Exception e){
			logger.error("getDivisonInfo(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDivisonInfo(String, DataSource) - Error occured during getting statusname ");
			}
				
			}
			finally
			{
				try{
					if ( rs!= null )						
						{
							rs.close();		
						}
					}
				catch( Exception e )
				{
				logger.warn("getDivisonInfo(String, DataSource) - exception ignored", e);
					
				}
				try
				{
					if ( stmt!= null )	
						stmt.close();
					
				}
				catch( Exception e )
				{
				logger.warn("getDivisonInfo(String, DataSource) - exception ignored", e);
					
				}
				
				try
				{
					if ( conn!= null)						
							conn.close();
					
				}
				catch( Exception e )
				{
				logger.warn("getDivisonInfo(String, DataSource) - exception ignored", e);
					
				}
					
				}
			return statusname;	
		}
		public  static String getDivisonName(String divison_id,DataSource ds)
		{
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String statusname="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				String sql1="select lo_om_division from lo_organization_main where lo_om_id='"+divison_id+"'";
				rs = stmt.executeQuery(sql1);
				if(rs.next())
				{
					statusname=rs.getString("lo_om_division");
				}
					
				
			}
			catch(Exception e){
			logger.error("getDivisonName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDivisonName(String, DataSource) - Error occured during getting statusname " + e);
			}
				
			}
			finally
			{
				try{
					if ( rs!= null )						
						{
							rs.close();		
						}
					}
				catch( Exception e )
				{
				logger.warn("getDivisonName(String, DataSource) - exception ignored", e);
					
				}
				try
				{
					if ( stmt!= null )	
						stmt.close();
					
				}
				catch( Exception e )
				{
				logger.warn("getDivisonName(String, DataSource) - exception ignored", e);
					
				}
				
				try
				{
					if ( conn!= null)						
							conn.close();
					
				}
				catch( Exception e )
				{
				logger.warn("getDivisonName(String, DataSource) - exception ignored", e);
					
				}
					
				}
			return statusname;	
		}	
		
		public  static String getDivisonid(String divison_name,DataSource ds)
		{
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String statusname="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				String sql="select lo_om_id from lo_organization_main where lo_om_division='"+divison_name+"'";
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					statusname=rs.getString("lo_om_id");
				}
			}
			catch(Exception e){
			logger.error("getDivisonid(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDivisonid(String, DataSource) - Error occured during getting statusname " + e);
			}
				
			}
			finally
			{
				try{
					if ( rs!= null )						
						{
							rs.close();		
						}
					}
				catch( Exception e )
				{
				logger.warn("getDivisonid(String, DataSource) - exception ignored", e);
					
				}
				try
				{
					if ( stmt!= null )	
						stmt.close();
					
				}
				catch( Exception e )
				{
				logger.warn("getDivisonid(String, DataSource) - exception ignored", e);
					
				}
				
				try
				{
					if ( conn!= null)						
							conn.close();
					
				}
				catch( Exception e )
				{
				logger.warn("getDivisonid(String, DataSource) - exception ignored", e);
					
				}
					
				}
			return statusname;
			
		}
		
		
		public static java.util.ArrayList getRoleName(String org_type, DataSource ds) 
		{
			Connection conn = null;
			CallableStatement cstmt = null;
			ResultSet rs = null;
			ArrayList RoleList = new ArrayList();
			DynamicTabularChkBox cmcb = null;
			
			try
			{
				conn = ds.getConnection();
				cstmt = conn.prepareCall("{?=call lo_role_type(?)}");
				cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
				cstmt.setString(2,org_type);
				rs  = cstmt.executeQuery();
				while ( rs.next())
				{
					cmcb = new DynamicTabularChkBox();
	       			if (rs.getString("c1")!=null)
						cmcb.setC1(rs.getString("c1"));
					else
						cmcb.setC1("");
					if (rs.getString("c2")!=null)
						cmcb.setC2(rs.getString("c2"));
					else
						cmcb.setC2("");
					if (rs.getString("c2id")!=null)
						cmcb.setC2id(rs.getString("c2id"));
					else
						cmcb.setC2id("");
					if (rs.getString("c3")!=null)
						cmcb.setC3(rs.getString("c3"));
					else
						cmcb.setC3("");
					if (rs.getString("c3id")!=null)
						cmcb.setC3id(rs.getString("c3id"));
					else
						cmcb.setC3id("");
					if (rs.getString("c4")!=null)
						cmcb.setC4(rs.getString("c4"));
					else
						cmcb.setC4("");
					if (rs.getString("c4id")!=null)
						cmcb.setC4id(rs.getString("c4id"));
					else
						cmcb.setC4id("");
					if (rs.getString("c5")!=null)
						cmcb.setC5(rs.getString("c5"));
					else
						cmcb.setC5("");
					if (rs.getString("c5id")!=null)
						cmcb.setC5id(rs.getString("c5id"));
					else
						cmcb.setC5id("");
					if (rs.getString("c6")!=null)
						cmcb.setC6(rs.getString("c6"));
					else
						cmcb.setC6("");
					if (rs.getString("c6id")!=null)
						cmcb.setC6id(rs.getString("c6id"));
					else
						cmcb.setC6id("");
					if (rs.getString("c7")!=null)
						cmcb.setC7(rs.getString("c7"));
					else
						cmcb.setC7("");
					if (rs.getString("c7id")!=null)
						cmcb.setC7id(rs.getString("c7id"));
					else
						cmcb.setC7id("");
					RoleList.add( cmcb );
				}
			}
			catch( Exception e )
			{
			logger.error("getRoleName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRoleName(String, DataSource) - Exception caught in rerieving Role Name" + e);
			}
			}
			
			finally
			{
				
				try
				{
					rs.close();
				}
				catch( SQLException sql )
				{
				logger.warn("getRoleName(String, DataSource) - exception ignored", sql);
}
				
				
				try
				{
					cstmt.close();
				}
				catch( SQLException sql )
				{
				logger.warn("getRoleName(String, DataSource) - exception ignored", sql);
}
				
				
				try
				{
					conn.close();
				}
				catch( SQLException sql )
				{
				logger.warn("getRoleName(String, DataSource) - exception ignored", sql);
}
				
			}
			
			return RoleList;	
		}
		
		public static String getPocId( String org_discipline_id, String firstname,String lastname, DataSource ds){
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String poc_id="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				String sql="select lo_pc_id from lo_poc where lo_pc_first_name='"+firstname+"' and lo_pc_last_name='"+lastname+"' and lo_pc_om_id="+org_discipline_id;
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					poc_id=rs.getString("lo_pc_id");
				}
			}
			catch(Exception e){
			logger.error("getPocId(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocId(String, String, String, DataSource) - Error occured during getting statusname " + e);
			}
				
			}
			finally
			{
				try{
					if ( rs!= null )						
						{
							rs.close();		
						}
					}
				catch( Exception e )
				{
				logger.warn("getPocId(String, String, String, DataSource) - exception ignored", e);
					
				}
				try
				{
					if ( stmt!= null )	
						stmt.close();
					
				}
				catch( Exception e )
				{
				logger.warn("getPocId(String, String, String, DataSource) - exception ignored", e);
					
				}
				
				try
				{
					if ( conn!= null)						
							conn.close();
					
				}
				catch( Exception e )
				{
				logger.warn("getPocId(String, String, String, DataSource) - exception ignored", e);
					
				}
					
				}
			return poc_id;			
		}

		public static String getPocaddressid( String org_discipline_id, String firstname,String lastname, DataSource ds){
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			String poc_ad_id="";
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				String sql="select lo_pc_ad_id from lo_poc where lo_pc_first_name='"+firstname+"' and lo_pc_last_name='"+lastname+"' and lo_pc_om_id="+org_discipline_id;
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					poc_ad_id=rs.getString("lo_pc_id");
				}
			}
			catch(Exception e){
			logger.error("getPocaddressid(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocaddressid(String, String, String, DataSource) - Error occured during getting statusname " + e);
			}
				
			}
			finally
			{
				try{
					if ( rs!= null )						
						{
							rs.close();		
						}
					}
				catch( Exception e )
				{
				logger.warn("getPocaddressid(String, String, String, DataSource) - exception ignored", e);
					
				}
				try
				{
					if ( stmt!= null )	
						stmt.close();
					
				}
				catch( Exception e )
				{
				logger.warn("getPocaddressid(String, String, String, DataSource) - exception ignored", e);
					
				}
				
				try
				{
					if ( conn!= null)						
							conn.close();
					
				}
				catch( Exception e )
				{
				logger.warn("getPocaddressid(String, String, String, DataSource) - exception ignored", e);
					
				}
					
				}
			return poc_ad_id;			
		}
		
		public static void getPocdetails( AddPocBean poc,String lo_pc_id,DataSource ds){
			
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
			ArrayList poc_ad_id=null;
			try{
				conn=ds.getConnection();
				stmt=conn.createStatement();
				String sql="select * from lo_poc where lo_pc_id='"+lo_pc_id+"'";
				rs = stmt.executeQuery(sql);
				if(rs.next())
				{
					poc.setLo_pc_id(rs.getString("Lo_pc_id"));
					poc.setLo_pc_ad_id(rs.getString("Lo_pc_ad_id"));
					poc.setFirstname(rs.getString("Lo_pc_first_name"));
					poc.setLastname(rs.getString("lo_pc_last_name"));
					poc.setTitle(rs.getString("Lo_pc_title"));
					poc.setPhone1(rs.getString("Lo_pc_phone1"));
					poc.setPhone2(rs.getString("Lo_pc_phone2"));
					poc.setFax(rs.getString("Lo_pc_fax"));
					poc.setEmail1(rs.getString("Lo_pc_email1"));
					poc.setEmail2(rs.getString("Lo_pc_email2"));
					poc.setMobile(rs.getString("Lo_pc_cell_phone"));
					poc.setFirstaddressline(rs.getString("Lo_pc_address1"));
					poc.setSecondaddressline(rs.getString("Lo_pc_address2"));
					poc.setCity(rs.getString("Lo_pc_city"));
					poc.setState(rs.getString("Lo_pc_state"));
					poc.setZip(rs.getString("Lo_pc_zip_code"));
					poc.setCountry(rs.getString("Lo_pc_country"));
				}
				sql="select * from lo_user where lo_us_pc_id='"+lo_pc_id+"'";
				rs = stmt.executeQuery(sql);
				if(rs.next()){
					poc.setUser_name(rs.getString("lo_us_name"));
					poc.setPassword(rs.getString("lo_us_pass"));					
				}
			}
			catch(Exception e){
			logger.error("getPocdetails(AddPocForm, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocdetails(AddPocForm, String, DataSource) - Error occured during getting statusname " + e);
			}
				
			}
			finally
			{
				try{
					if ( rs!= null )						
						{
							rs.close();		
						}
					}
				catch( Exception e )
				{
				logger.warn("getPocdetails(AddPocForm, String, DataSource) - exception ignored", e);
					
				}
				try
				{
					if ( stmt!= null )	
						stmt.close();
					
				}
				catch( Exception e )
				{
				logger.warn("getPocdetails(AddPocForm, String, DataSource) - exception ignored", e);
					
				}
				
				try
				{
					if ( conn!= null)						
							conn.close();
					
				}
				catch( Exception e )
				{
				logger.warn("getPocdetails(AddPocForm, String, DataSource) - exception ignored", e);
					
				}
					
				}		
		}		



public static String[] getPocRoleArray(String lo_pc_id, DataSource ds){
	
	Connection conn=null;
	Statement stmt=null;
	ResultSet rs=null;
	String poc_ad_id="";
	String[] strArr=null;
	try{
		conn=ds.getConnection();
		stmt=conn.createStatement();
		String sql="select lo_ro_id,lo_ro_role_desc from lo_role_desc_01 where lo_ro_id in"+
					"(select lo_pr_ro_id from lo_poc_rol where lo_pr_pc_id='"+lo_pc_id+"')";
		rs = stmt.executeQuery(sql);
		int flag=0;
		while(rs.next())			
		{
			flag=1;
			poc_ad_id=poc_ad_id+rs.getString("lo_ro_id")+",";
		}
		if (flag==1)
		strArr=poc_ad_id.split(",");
	}
	catch(Exception e){
			logger.error("getPocRoleArray(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocRoleArray(String, DataSource) - Error occured during getting statusname " + e);
			}
		
	}
	finally
	{
		try{
			if ( rs!= null )						
				{
					rs.close();		
				}
			}
		catch( Exception e )
		{
				logger.warn("getPocRoleArray(String, DataSource) - exception ignored", e);
			
		}
		try
		{
			if ( stmt!= null )	
				stmt.close();
			
		}
		catch( Exception e )
		{
				logger.warn("getPocRoleArray(String, DataSource) - exception ignored", e);
			
		}
		
		try
		{
			if ( conn!= null)						
					conn.close();
			
		}
		catch( Exception e )
		{
				logger.warn("getPocRoleArray(String, DataSource) - exception ignored", e);
			
		}
			
		}
	return strArr;			
 }



public static int deletePoc( String lo_pc_id, DataSource ds){
	
	Connection conn=null;
	CallableStatement cstmt = null;
	ResultSet rs=null;	
	int retval=0;	
	
	try{
		conn=ds.getConnection();
		cstmt=conn.prepareCall("{?=call lo_poc_delete_01(?)}");
		cstmt.registerOutParameter(1,java.sql.Types.INTEGER);
		
		cstmt.setString(2,lo_pc_id);
		cstmt.execute();
		retval = cstmt.getInt(1);
		
	}
	catch(Exception e){
			logger.error("deletePoc(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deletePoc(String, DataSource) - Error occured during deleting  " + e);
			}
		
	}
	finally
	{
			
		try
		{
			if ( rs!= null ) 
			{
				rs.close();		
			}
		}
		
		catch( Exception e )
		{
				logger.warn("deletePoc(String, DataSource) - exception ignored", e);
}
		
		try
		{
			if ( cstmt!= null ) 
				cstmt.close();
		}
		catch( Exception e )
		{
				logger.warn("deletePoc(String, DataSource) - exception ignored", e);
}
		
		
		try
		{
			if ( conn!= null) 
				conn.close();
		}
		catch( Exception e )
		{
				logger.warn("deletePoc(String, DataSource) - exception ignored", e);
}
		
	}
	return retval;
	}


public static String Rolename( String indexvalue, DataSource ds){
	
	Connection conn=null;
	Statement stmt = null;
	ResultSet rs=null;	
	String retval="";	
	
	try{
		conn=ds.getConnection();
		stmt=conn.createStatement();
		String sql = "select dbo.func_cp_incident ('"+indexvalue+"','','U')";
			if (logger.isDebugEnabled()) {
				logger.debug("Rolename(String, DataSource) - sql:::::: " + sql);
			}
		rs = stmt.executeQuery(sql);
		if(rs.next())
		retval=rs.getString(1);
	
	}
	catch(Exception e){
			logger.error("Rolename(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("Rolename(String, DataSource) - Error occured getting role description  " + e);
			}
		
	}
	finally
	{
			
		try
		{
			if ( rs!= null ) 
			{
				rs.close();		
			}
		}
		
		catch( Exception e )
		{
				logger.warn("Rolename(String, DataSource) - exception ignored", e);
}
		
		try
		{
			if ( stmt!= null ) 
				stmt.close();
		}
		catch( Exception e )
		{
				logger.warn("Rolename(String, DataSource) - exception ignored", e);
}
		
		
		try
		{
			if ( conn!= null) 
				conn.close();
		}
		catch( Exception e )
		{
				logger.warn("Rolename(String, DataSource) - exception ignored", e);
}
		
	}
	return retval;
	}
}