package com.mind.dao.CM;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.cm.AddDivisonBean;
import com.mind.bean.cm.AddPocBean;
import com.mind.bean.cm.SearchBean;
import com.mind.common.CMPocSearchList;
import com.mind.common.CMSearchList;
import com.mind.common.DynamicTabularChkBox;
import com.mind.common.Util;

public class CustomerManagerDao {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(CustomerManagerDao.class);

	public static int addFirstLevelCatg(String id, String type, String element,
			String action, DataSource ds, String workCenter) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int retval = -1;

		try {

			// Added to change single quotes string to double quotes
			if (element != null) {
				if (element.indexOf("'") >= 0) {
					element = Util.changeStringToDoubleQuotes(element);
				}
			}

			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lo_organization_top_manage_01(?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, id);
			cstmt.setString(3, type);
			cstmt.setString(4, element);
			cstmt.setString(5, action);
			cstmt.setString(6, workCenter);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"addFirstLevelCatg(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addFirstLevelCatg(String, String, String, String, DataSource) - Error occured during adding First Level Catg"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"addFirstLevelCatg(String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"addFirstLevelCatg(String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"addFirstLevelCatg(String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retval;
	}

	/**
	 * This method is used to call the store procedure which add or update the
	 * Division and returns an int data to signify the result. of store
	 * procedure.
	 * 
	 * @param lo_om_id
	 *            the id of the division (in case of updation only otherwise it
	 *            is null).
	 * @param lo_om_ot_id
	 *            the id of the organization under which the organization
	 *            exists.
	 * @param lo_om_name
	 *            this value is used to determine whether it is for new addition
	 *            or update.
	 * @param address_id
	 *            name of the division.
	 * @param address1
	 *            First address line of the division.
	 * @param address2
	 *            Second address line of the division.
	 * @param city
	 *            city of the division.
	 * @param state
	 *            state of the division.
	 * @param zip
	 *            zipcode of the division's address.
	 * @param country
	 *            country of the division.
	 * @param action
	 *            this value is used to determine whether it is for new addition
	 *            or update.
	 * @param ds
	 *            Object of DataSource
	 * @return int the integer returned by the stored procedure.
	 */
	public static int addSecondLevelCatg(String lo_om_id, String lo_om_ot_id,
			String lo_om_name, String address_id, String address1,
			String address2, String city, String state, String zip,
			String country, String action, String website, String conMinuteman,
			String lat_deg, String lat_min, String lat_direction,
			String lon_deg, String lon_min, String lon_direction,
			String timezone, String latlonFound, String[] coreCompetency,
			String userId, String partnerSource, String accept_address,
			String latitude, String longitude, String latLongAccuracy,
			DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int retval = -1;

		String competency = "";
		if (coreCompetency != null) {
			for (int i = 0; i < coreCompetency.length; i++) {

				competency = competency + coreCompetency[i] + ",";
			}
			competency = competency.substring(0, competency.length() - 1);
		}

		if (accept_address.equals("")) {
			accept_address = null;
		}

		try {
			// Added to change single quotes string to double quotes
			if (lo_om_name != null) {
				if (lo_om_name.indexOf("'") >= 0) {
					lo_om_name = Util.changeStringToDoubleQuotes(lo_om_name);
				}
			}

			if (address1 != null) {
				if (address1.indexOf("'") >= 0) {
					address1 = Util.changeStringToDoubleQuotes(address1);
				}
			}

			if (address2 != null) {
				if (address2.indexOf("'") >= 0) {
					address2 = Util.changeStringToDoubleQuotes(address2);
				}
			}

			if (city != null) {
				if (city.indexOf("'") >= 0) {
					city = Util.changeStringToDoubleQuotes(city);
				}
			}

			conn = ds.getConnection();

			// Removed 3 parameters
			cstmt = conn
					.prepareCall("{?=call lo_organization_main_manage_01_pvs_manager(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?)}");// Removed
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, lo_om_id);
			cstmt.setString(3, lo_om_ot_id);
			cstmt.setString(4, lo_om_name);
			cstmt.setString(5, address_id);
			cstmt.setString(6, address1);
			cstmt.setString(7, address2);
			cstmt.setString(8, city);
			cstmt.setString(9, state);
			cstmt.setString(10, zip);
			cstmt.setString(11, country);
			cstmt.setString(12, action);
			cstmt.setString(13, lat_deg);
			cstmt.setString(14, lat_min);
			cstmt.setString(15, lat_direction);
			cstmt.setString(16, lon_deg);
			cstmt.setString(17, lon_min);
			cstmt.setString(18, lon_direction);
			cstmt.setInt(19, Integer.parseInt(timezone));
			cstmt.setString(20, latlonFound);
			cstmt.setString(21, competency);
			cstmt.setString(22, website);
			cstmt.setString(23, userId);
			cstmt.setString(24, partnerSource);
			cstmt.setString(25, accept_address);
			cstmt.setString(26, conMinuteman);

			cstmt.setString(27, latitude);
			cstmt.setString(28, longitude);
			cstmt.setString(29, latLongAccuracy);

			cstmt.execute();
			retval = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"addSecondLevelCatg(String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String[], String, String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addSecondLevelCatg(String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String[], String, String, String, String, String, String, DataSource) - Error occured during adding Second Level Catg"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"addSecondLevelCatg(String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String[], String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"addSecondLevelCatg(String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String[], String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"addSecondLevelCatg(String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String[], String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retval;
	}

	// added by vijay
	public static ArrayList getStateCategoryList(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select cp_country_id, cp_country_name, cp_country_order from lp_country_list order by cp_country_order,cp_country_name";
			rs = stmt.executeQuery(sql);
			int i = 0;

			if (i == 0) {
				AddDivisonBean addDivision = new AddDivisonBean();
				addDivision.setSiteCountryId("");
				addDivision.setSiteCountryIdName("");
				valuelist.add(i, addDivision);
				i++;
			}

			while (rs.next()) {
				AddDivisonBean addDivision = new AddDivisonBean();

				addDivision.setSiteCountryId(rs.getString("cp_country_id"));
				addDivision.setSiteCountryIdName(rs
						.getString("cp_country_name"));
				valuelist.add(i, addDivision);
				i++;
			}

		} catch (Exception e) {
			logger.error("getStateCategoryList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStateCategoryList(DataSource) - Error occured during getting State Category Wise List  ");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateCategoryList(DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	/**
	 * This method is used to call the store procedure which delete the top
	 * level customer, CNS and partner and returns an int data to show the
	 * output.
	 * 
	 * @param id
	 *            the id of the top level organization.
	 * @param type
	 *            type of organization i.e. customer/CNS/Partner.
	 * @param ds
	 *            Object of DataSource
	 * @return int the integer returned by the stored procedure.
	 */
	public static int deleteFirstLevelCatg(String id, String type, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		int retval = -1;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lo_organization_top_delete_01(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, id);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("deleteFirstLevelCatg(String, String, DataSource)", e);

			retval = -9001;
			if (logger.isDebugEnabled()) {
				logger.debug("deleteFirstLevelCatg(String, String, DataSource) - Error occured during deleting First Level Catg "
						+ e);
			}

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deleteFirstLevelCatg(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deleteFirstLevelCatg(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deleteFirstLevelCatg(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retval;
	}

	/**
	 * This method is used to call the store procedure which delete the division
	 * and returns an int data to show the output.
	 * 
	 * @param id
	 *            the id of the division.
	 * @param ds
	 *            Object of DataSource
	 * @return int the integer returned by the stored procedure.
	 */
	public static int deleteSecondLevelCatg(String id, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		int retval = -1;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lo_organization_main_delete_01(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setString(2, id);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("deleteSecondLevelCatg(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deleteSecondLevelCatg(String, DataSource) - Error occured during deleting Second Level Catg "
						+ e);
			}

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deleteSecondLevelCatg(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deleteSecondLevelCatg(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deleteSecondLevelCatg(String, DataSource) - exception ignored",
						e);
			}

		}
		return retval;
	}

	/**
	 * This method is used to get the name of the customer from the database by
	 * providing customer id as an argument.
	 * 
	 * @param catid
	 *            the id of the top level organization.
	 * @param ds
	 *            Object of DataSource.
	 * @return String the name of the customer.
	 */
	public static String getCustomerName(String catid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String resourcetype = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lo_ot_name from lo_organization_top where lo_ot_id = "
					+ catid;
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				resourcetype = rs.getString("lo_ot_name");
			}

		} catch (Exception e) {
			logger.error("getCustomerName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomerName(String, DataSource) - Error occured during get Customer Name 1");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCustomerName(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCustomerName(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCustomerName(String, DataSource) - exception ignored",
						e);
			}

		}
		return resourcetype;
	}

	public static void getRefreshState(AddPocBean addPOCBean, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (addPOCBean.getRefreshType().equals("state")) {
				String sql = "select cp_country_id, cp_category_id, cp_region_id from cp_state where cp_state_id ='"
						+ addPOCBean.getState() + "'";

				rs = stmt.executeQuery(sql);

				if (rs.next()) {
					addPOCBean.setCountry(rs.getString("cp_country_id"));
					addPOCBean.setCategory_id(rs.getString("cp_category_id"));
					addPOCBean.setRegion_id(rs.getString("cp_region_id"));
				} else {
					addPOCBean.setCountry("0");
					addPOCBean.setCategory_id("0");
					addPOCBean.setRegion_id("0");
				}
			}

			if (addPOCBean.getRefreshType().equals("country")) {
				String sql = "select cp_category_id, cp_region_id from cp_state where cp_country_id ='"
						+ addPOCBean.getCountry() + "'";
				rs = stmt.executeQuery(sql);

				if (rs.next()) {
					addPOCBean.setCategory_id(rs.getString("cp_category_id"));
					addPOCBean.setRegion_id(rs.getString("cp_region_id"));
				} else {
					addPOCBean.setCategory_id("0");
					addPOCBean.setRegion_id("0");
				}
			}

		} catch (Exception e) {
			logger.error("getRefreshState(AddPocForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRefreshState(AddPocForm, DataSource) - Error occured during getting POC country , Category  , Region "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getRefreshState(AddPocForm, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getRefreshState(AddPocForm, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getRefreshState(AddPocForm, DataSource) - exception ignored",
						e);
			}

		}

	}

	/**
	 * This method is used to call the store procedure which delete the top
	 * level customer, CNS and partner and returns an int data to show the
	 * output.
	 * 
	 * @param cusid
	 *            the id of the top level organization.
	 * @param org_type
	 *            type of organization i.e. customer/CNS/Partner.
	 * @param ds
	 *            Object of DataSource.
	 * @return String the name of the customer.
	 */
	public static String getCustomerName(String custid, String org_type,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String custname = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lo_ot_name from lo_organization_top where lo_ot_id='"
					+ custid
					+ "' and lo_ot_type='"
					+ org_type
					+ "' and lo_ot_name <> rtrim(lo_ot_id)";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				custname = rs.getString("lo_ot_name");
			}

		} catch (Exception e) {
			logger.error("getCustomerName(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomerName(String, String, DataSource) - Error occured during get Customer Name 2");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCustomerName(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCustomerName(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCustomerName(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return custname;
	}

	public static String getTimeZoneName(String type, String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String TZName = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select dbo.func_cc_time_zone_name('" + type + "', "
					+ Id + ")";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				TZName = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("getTimeZoneName(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTimeZoneName(String, String, DataSource) - Error occured during get Time Zone Name");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getTimeZoneName(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getTimeZoneName(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getTimeZoneName(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return TZName;
	}

	public static String getClassification(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String lo_cp_role = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lo_pc_role from lo_poc where lo_pc_id='" + Id
					+ "'";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				lo_cp_role = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("getClassification(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getClassification(String, DataSource) - Error occured during get lo_cp_role");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getClassification(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getClassification(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getClassification(String, DataSource) - exception ignored",
						e);
			}

		}
		return lo_cp_role;
	}

	public static String getEmpStatus(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String lo_cp_role = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select isnull(lo_pc_active_user,0) from lo_poc where lo_pc_id='"
					+ Id + "'";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				lo_cp_role = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("getEmpStatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEmpStatus(String, DataSource) - Error occured during get Emp Status");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getEmpStatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getEmpStatus(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getEmpStatus(String, DataSource) - exception ignored",
						e);
			}

		}
		return lo_cp_role;
	}

	/**
	 * This method is for getting the customer id.
	 * 
	 * @param catid
	 *            the id of the top level organization.
	 * @param org_type
	 *            type of organization i.e. customer/CNS/Partner.
	 * @param ds
	 *            Object of DataSource.
	 * @return String the name of the customer.
	 */
	public static String getCustomerid(String catid, String org_type,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String mfgid = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lo_ot_id from lo_organization_top where lo_ot_name='"
					+ org_type + "' and lo_ot_type='" + catid + "'";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				mfgid = rs.getString("lo_ot_id");
			}

		} catch (Exception e) {
			logger.error("getCustomerid(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomerid(String, String, DataSource) - Error occured during getting Customer id  ");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCustomerid(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCustomerid(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getCustomerid(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return mfgid;
	}

	/**
	 * This method is used for retrieving the division name.
	 * 
	 * @param divisionid
	 *            the id of the division.
	 * @param ds
	 *            Object of DataSource.
	 * @return String the name of the division.
	 */
	public static String getDivisionName(String divisionid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String divisionname = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select lo_om_division from lo_organization_main where lo_om_id="
					+ divisionid;
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				divisionname = rs.getString("lo_om_division");
				if (divisionname == null) {
					divisionname = "";
				}
			}
		} catch (Exception e) {
			logger.error("getDivisionName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDivisionName(String, DataSource) - Error occured during get Division Name");
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getDivisionName(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getDivisionName(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getDivisionName(String, DataSource) - exception ignored",
						e);
			}

		}
		return divisionname;
	}

	/**
	 * This method is used to call the store procedure which add or update the
	 * person of contact and returns an int data to show the result
	 * 
	 * @param lo_pc_id
	 *            id of the Poc.
	 * @param chk_rol
	 *            a non-null value if the role assigned check box is checked.
	 * @param indexvalue
	 *            the id of the roles assigned to the Poc.
	 * @param chk_off_add
	 *            a non-null value if the use office address check box is
	 *            checked.
	 * @param lo_pc_ad_id
	 *            address id of the Poc.
	 * @param lo_pc_unique_address
	 *            unique address of the Poc.
	 * @param lo_pc_om_id
	 *            id of the Poc's Division.
	 * @param lo_pc_first_name
	 *            Poc's first name.
	 * @param lo_pc_mi
	 *            Poc's middle name.
	 * @param lo_pc_last_name
	 *            Poc's last name.
	 * @param lo_pc_title
	 *            title of the Poc.
	 * @param lo_pc_phone1
	 *            Poc's first phone.
	 * @param lo_pc_phone2
	 *            Poc's second phone.
	 * @param lo_pc_fax
	 *            Poc's fax number.
	 * @param lo_pc_email1
	 *            Poc's first email address.
	 * @param lo_pc_email2
	 *            Poc's second email address.
	 * @param lo_pc_cell_phone
	 *            Poc's mobile number.
	 * @param lo_pc_address1
	 *            Poc's first line address.
	 * @param lo_pc_address2
	 *            Poc's second line address.
	 * @param lo_pc_city
	 *            Poc's city.
	 * @param lo_pc_state
	 *            Poc's state.
	 * @param lo_pc_zip_code
	 *            Poc's zipcode.
	 * @param lo_pc_country
	 *            Poc's country.
	 * @param lo_pc_user_name
	 *            Poc's user name.
	 * @param lo_pc_password
	 *            Poc's password.
	 * @param action
	 *            for addition of record submit 'a' and for updation of the
	 *            record submit 'u'.
	 * @param DataSource
	 *            ds Poc's mobile number.
	 * @return int the integer returned by the stored procedure.
	 */
	public static String setThirdLevelCatg(String lo_pc_id, String chk_rol,
			String indexvalue, String chk_off_add, String lo_pc_ad_id,
			String lo_pc_unique_address, String lo_pc_om_id,
			String lo_pc_first_name, String lo_pc_mi, String lo_pc_last_name,
			String lo_pc_title, String lo_pc_phone1, String lo_pc_phone2,
			String lo_pc_fax, String lo_pc_email1, String lo_pc_email2,
			String lo_pc_cell_phone, String lo_pc_address1,
			String lo_pc_address2, String lo_pc_city, String lo_pc_state,
			String lo_pc_zip_code, String lo_pc_country, String lo_pc_lat_deg,
			String lo_pc_lat_min, String lo_pc_lat_direction,
			String lo_pc_lon_deg, String lo_pc_lon_min,
			String lo_pc_lon_direction, String lo_pc_time_zone,
			String lo_pc_user_name, String lo_pc_password,
			String lo_pc_latlon_found, String action, String pvsContact,
			String classificationId, String empStatus, String userId,
			DataSource ds, String rank, String invoiceApproval) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int retval = -1;
		int chkrole = -1;
		String updatedrole = "";
		String userName = "";

		if (lo_pc_user_name == null) {
			lo_pc_user_name = "";
		}

		try {

			// Added to change single quotes string to double quotes
			if (lo_pc_first_name != null) {
				if (lo_pc_first_name.indexOf("'") >= 0) {
					lo_pc_first_name = Util
							.changeStringToDoubleQuotes(lo_pc_first_name);
				}
			}

			if (lo_pc_last_name != null) {
				if (lo_pc_last_name.indexOf("'") >= 0) {
					lo_pc_last_name = Util
							.changeStringToDoubleQuotes(lo_pc_last_name);
				}
			}

			if (lo_pc_address1 != null) {
				if (lo_pc_address1.indexOf("'") >= 0) {
					lo_pc_address1 = Util
							.changeStringToDoubleQuotes(lo_pc_address1);
				}
			}

			if (lo_pc_address2 != null) {
				if (lo_pc_address2.indexOf("'") >= 0) {
					lo_pc_address2 = Util
							.changeStringToDoubleQuotes(lo_pc_address2);
				}
			}

			if (lo_pc_city != null) {
				if (lo_pc_city.indexOf("'") >= 0) {
					lo_pc_city = Util.changeStringToDoubleQuotes(lo_pc_city);
				}
			}

			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lo_poc_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			if (chk_rol == null) {
				chk_rol = "d";
			}
			cstmt.setString(2, lo_pc_id);
			cstmt.setString(3, chk_rol);
			cstmt.setString(4, indexvalue);
			cstmt.setString(5, chk_off_add);
			cstmt.setString(6, lo_pc_ad_id);
			cstmt.setString(7, lo_pc_unique_address);
			cstmt.setString(8, lo_pc_om_id);
			cstmt.setString(9, lo_pc_first_name);
			cstmt.setString(10, lo_pc_mi);
			cstmt.setString(11, lo_pc_last_name);
			cstmt.setString(12, lo_pc_title);
			cstmt.setString(13, lo_pc_phone1);
			cstmt.setString(14, lo_pc_phone2);
			cstmt.setString(15, lo_pc_fax);
			cstmt.setString(16, lo_pc_email1);
			cstmt.setString(17, lo_pc_email2);
			cstmt.setString(18, lo_pc_cell_phone);
			cstmt.setString(19, lo_pc_address1);
			cstmt.setString(20, lo_pc_address2);
			cstmt.setString(21, lo_pc_city);
			cstmt.setString(22, lo_pc_state);
			cstmt.setString(23, lo_pc_zip_code);
			cstmt.setString(24, lo_pc_country);
			cstmt.setString(25, lo_pc_user_name);
			cstmt.registerOutParameter(25, java.sql.Types.VARCHAR);
			/*
			 * Changes Made to make the lo_pc_user_name of both input and output
			 * type
			 */
			cstmt.setString(26, lo_pc_password);
			cstmt.setString(27, action);
			cstmt.setString(28, lo_pc_lat_deg);
			cstmt.setString(29, lo_pc_lat_min);
			cstmt.setString(30, lo_pc_lat_direction);
			cstmt.setString(31, lo_pc_lon_deg);
			cstmt.setString(32, lo_pc_lon_min);
			cstmt.setString(33, lo_pc_lon_direction);
			cstmt.setString(34, lo_pc_time_zone);
			cstmt.setString(35, lo_pc_latlon_found);
			cstmt.setString(36, pvsContact);
			cstmt.setString(37, classificationId);
			cstmt.setString(38, empStatus);
			cstmt.registerOutParameter(39, java.sql.Types.INTEGER);
			cstmt.registerOutParameter(40, java.sql.Types.VARCHAR);
			cstmt.setString(41, userId);
			cstmt.setString(42, "I");
			// added for CNS
			cstmt.setString(43, rank);
			cstmt.setString(44, invoiceApproval);
			cstmt.execute();
			retval = cstmt.getInt(1);
			chkrole = cstmt.getInt(39);
			userName = cstmt.getString(25);
			updatedrole = cstmt.getString(40);
		} catch (Exception e) {
			logger.error(
					"setThirdLevelCatg(String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("setThirdLevelCatg(String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, DataSource) - Error occured during adding third catg ::::::::::"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setThirdLevelCatg(String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setThirdLevelCatg(String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"setThirdLevelCatg(String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retval + "," + chkrole + "," + updatedrole + "," + userName;
	}

	/**
	 * This method is used for retrieving the division information.
	 * 
	 * @param divisionid
	 *            the id of the division.
	 * @param ds
	 *            Object of DataSource.
	 * @return String[] a string array containing the information of the
	 *         division.
	 */
	public static String[] getDivisonInfo(String divison_id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] statusname = { null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null, null,
				null, null, null };
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from dbo.func_cp_partner_division_info("
					+ divison_id + ")";
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				statusname[0] = rs.getString("lo_ad_id");
				statusname[1] = rs.getString("lo_ad_address1");
				statusname[2] = rs.getString("lo_ad_address2");
				statusname[3] = rs.getString("lo_ad_city");
				statusname[4] = rs.getString("lo_ad_state");
				statusname[5] = rs.getString("lo_ad_zip_code");
				statusname[6] = rs.getString("lo_ad_country");
				statusname[7] = rs.getString("cp_pd_core_competency");
				statusname[8] = rs.getString("lo_ad_tzone");
				statusname[9] = rs.getString("cc_tz_name");
				statusname[10] = rs.getString("lo_ad_lat_deg");
				statusname[11] = rs.getString("lo_ad_lat_min");
				statusname[12] = rs.getString("lo_ad_lat_direction");
				statusname[13] = rs.getString("lo_ad_lon_deg");
				statusname[14] = rs.getString("lo_ad_lon_min");
				statusname[15] = rs.getString("lo_ad_lon_direction");
				statusname[16] = rs.getString("lo_ad_accept_address");

				statusname[17] = rs.getString("lo_ad_latitude");
				statusname[18] = rs.getString("lo_ad_longitude");
				statusname[19] = rs.getString("lo_ad_lat_long_accuracy");
				statusname[20] = rs.getString("lo_ad_latlon_source");
			}
		} catch (Exception e) {
			logger.error("getDivisonInfo(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDivisonInfo(String, DataSource) - Error occured during getting Divison Info ");
			}

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getDivisonInfo(String, DataSource) - exception ignored",
						e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getDivisonInfo(String, DataSource) - exception ignored",
						e);

			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getDivisonInfo(String, DataSource) - exception ignored",
						e);

			}

		}
		return statusname;
	}

	public static void getRefershedStates(AddDivisonBean addivision,
			String check, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (check.equals("state")) {
				String sql = "select cp_state_id,cp_country_id,cp_category_id,cp_region_id  from cp_state where cp_state_id ='"
						+ addivision.getState() + "'";
				rs = stmt.executeQuery(sql);

				if (rs.next()) {
					addivision.setCountry(rs.getString("cp_country_id"));
					addivision.setCategory_id(rs.getString("cp_category_id"));
					addivision.setRegion_id(rs.getString("cp_region_id"));
				} else {
					addivision.setCountry("0");
					addivision.setCategory_id("0");
					addivision.setRegion_id("0");
				}
			}
			if (check.equals("country")) {

				String sql = "select cp_country_id,cp_category_id,cp_region_id  from cp_state where cp_country_id ='"
						+ addivision.getCountry() + "'";
				rs = stmt.executeQuery(sql);

				if (rs.next()) {
					addivision.setCategory_id(rs.getString("cp_category_id"));
					addivision.setRegion_id(rs.getString("cp_region_id"));
				} else {
					addivision.setCategory_id("0");
					addivision.setRegion_id("0");
				}

			}

		} catch (Exception e) {
			logger.error(
					"getRefershedStates(AddDivisonForm, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRefershedStates(AddDivisonForm, String, DataSource) - Error occured during getting country , Category  , Region   "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getRefershedStates(AddDivisonForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getRefershedStates(AddDivisonForm, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getRefershedStates(AddDivisonForm, String, DataSource) - exception ignored",
						e);
			}

		}

	}

	/**
	 * This method is used for retrieving the division name.
	 * 
	 * @param divisionid
	 *            the id of the division.
	 * @param ds
	 *            Object of DataSource.
	 * @return String name of the division.
	 */
	public static String getDivisonName(String divison_id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String statusname = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql1 = "select lo_om_division from lo_organization_main where lo_om_id='"
					+ divison_id + "'";
			rs = stmt.executeQuery(sql1);
			if (rs.next()) {
				statusname = rs.getString("lo_om_division");
			}

		} catch (Exception e) {
			logger.error("getDivisonName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDivisonName(String, DataSource) - Error occured during getting Divison Name "
						+ e);
			}

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getDivisonName(String, DataSource) - exception ignored",
						e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getDivisonName(String, DataSource) - exception ignored",
						e);

			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getDivisonName(String, DataSource) - exception ignored",
						e);

			}

		}
		return statusname;
	}

	/**
	 * This method is used for retrieving the division id.
	 * 
	 * @param division_name
	 *            the id of the division.
	 * @param ds
	 *            Object of DataSource.
	 * @return String name of the division.
	 */
	public static String getDivisonid(String divison_name, String city,
			String state, String zipCode, int customerid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String divisionName = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select dbo.func_lo_get_division_id('" + divison_name
					+ "','" + city + "','" + state + "','" + zipCode + "',"
					+ customerid + ")";
			if (logger.isDebugEnabled()) {
				logger.debug("getDivisonid(String, String, String, String, int, DataSource) - sql::::::::"
						+ sql);
			}
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				divisionName = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error(
					"getDivisonid(String, String, String, String, int, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDivisonid(String, String, String, String, int, DataSource) - Error occured during getting Divison id "
						+ e);
			}

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getDivisonid(String, String, String, String, int, DataSource) - exception ignored",
						e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getDivisonid(String, String, String, String, int, DataSource) - exception ignored",
						e);

			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getDivisonid(String, String, String, String, int, DataSource) - exception ignored",
						e);

			}

		}
		return divisionName;

	}

	/**
	 * This method is used for getting the role list for assigning the role to
	 * Poc .
	 * 
	 * @param org_type
	 *            the id of the division.
	 * @param ds
	 *            Object of DataSource.
	 * @return ArrayList the values of the role in the ArrayList.
	 */
	public static ArrayList getRoleName(String org_type, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList RoleList = new ArrayList();
		DynamicTabularChkBox cmcb = null;

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lo_role_type(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, org_type);
			rs = cstmt.executeQuery();
			while (rs.next()) {
				cmcb = new DynamicTabularChkBox();
				if (rs.getString("c1") != null) {
					cmcb.setC1(rs.getString("c1"));
				} else {
					cmcb.setC1("");
				}
				if (rs.getString("c2") != null) {
					cmcb.setC2(rs.getString("c2"));
				} else {
					cmcb.setC2("");
				}
				if (rs.getString("c2id") != null) {
					cmcb.setC2id(rs.getString("c2id"));
				} else {
					cmcb.setC2id("");
				}
				if (rs.getString("c3") != null) {
					cmcb.setC3(rs.getString("c3"));
				} else {
					cmcb.setC3("");
				}
				if (rs.getString("c3id") != null) {
					cmcb.setC3id(rs.getString("c3id"));
				} else {
					cmcb.setC3id("");
				}
				if (rs.getString("c4") != null) {
					cmcb.setC4(rs.getString("c4"));
				} else {
					cmcb.setC4("");
				}
				if (rs.getString("c4id") != null) {
					cmcb.setC4id(rs.getString("c4id"));
				} else {
					cmcb.setC4id("");
				}
				if (rs.getString("c5") != null) {
					cmcb.setC5(rs.getString("c5"));
				} else {
					cmcb.setC5("");
				}
				if (rs.getString("c5id") != null) {
					cmcb.setC5id(rs.getString("c5id"));
				} else {
					cmcb.setC5id("");
				}
				if (rs.getString("c6") != null) {
					cmcb.setC6(rs.getString("c6"));
				} else {
					cmcb.setC6("");
				}
				if (rs.getString("c6id") != null) {
					cmcb.setC6id(rs.getString("c6id"));
				} else {
					cmcb.setC6id("");
				}
				if (rs.getString("c7") != null) {
					cmcb.setC7(rs.getString("c7"));
				} else {
					cmcb.setC7("");
				}
				if (rs.getString("c7id") != null) {
					cmcb.setC7id(rs.getString("c7id"));
				} else {
					cmcb.setC7id("");
				}
				RoleList.add(cmcb);
			}
		} catch (Exception e) {
			logger.error("getRoleName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRoleName(String, DataSource) - Exception caught in rerieving Role Name"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getRoleName(String, DataSource) - exception ignored",
						sql);
			}

			try {
				cstmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getRoleName(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getRoleName(String, DataSource) - exception ignored",
						sql);
			}

		}

		return RoleList;
	}

	/**
	 * This method is used for getting the Poc id.
	 * 
	 * @param org_discipline_id
	 *            the id of the division.
	 * @param firstname
	 *            first name of the poc.
	 * @param lastname
	 *            last name of the poc.
	 * @param ds
	 *            Object of DataSource.
	 * @return String id of the poc.
	 */
	public static String getPocId(String org_discipline_id, String firstname,
			String lastname, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String poc_id = "";
		if (firstname != null && !firstname.equals("")) {
			firstname = firstname.replaceAll("\'", "\'\'");
		}

		if (lastname != null && !lastname.equals("")) {
			lastname = lastname.replaceAll("\'", "\'\'");
		}
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lo_pc_id from lo_poc where lo_pc_first_name='"
					+ firstname + "' and lo_pc_last_name='" + lastname
					+ "' and lo_pc_om_id=" + org_discipline_id;

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				poc_id = rs.getString("lo_pc_id");
			}

		} catch (Exception e) {
			logger.error("getPocId(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocId(String, String, String, DataSource) - Error occured during getting Poc Id "
						+ e);
			}

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPocId(String, String, String, DataSource) - exception ignored",
						e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getPocId(String, String, String, DataSource) - exception ignored",
						e);

			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getPocId(String, String, String, DataSource) - exception ignored",
						e);

			}

		}
		return poc_id;
	}

	/**
	 * This method is used for getting the Poc address id.
	 * 
	 * @param org_discipline_id
	 *            the id of the division.
	 * @param firstname
	 *            first name of the poc.
	 * @param lastname
	 *            last name of the poc.
	 * @param ds
	 *            Object of DataSource.
	 * @return String id of the poc's address.
	 */
	public static String getPocaddressid(String org_discipline_id,
			String firstname, String lastname, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String poc_ad_id = "";

		if (firstname != null && !firstname.equals("")) {
			firstname = firstname.replaceAll("\'", "\'\'");
		}
		if (lastname != null && !lastname.equals("")) {
			lastname = lastname.replaceAll("\'", "\'\'");
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lo_pc_ad_id from lo_poc where lo_pc_first_name='"
					+ firstname
					+ "' and lo_pc_last_name='"
					+ lastname
					+ "' and lo_pc_om_id=" + org_discipline_id;

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				poc_ad_id = rs.getString("lo_pc_ad_id");
			}

		} catch (Exception e) {
			logger.error("getPocaddressid(String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocaddressid(String, String, String, DataSource) - Error occured during getting Poc address id "
						+ e);
			}

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPocaddressid(String, String, String, DataSource) - exception ignored",
						e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getPocaddressid(String, String, String, DataSource) - exception ignored",
						e);

			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getPocaddressid(String, String, String, DataSource) - exception ignored",
						e);

			}

		}
		return poc_ad_id;
	}

	/**
	 * This method is used for getting the Poc details.
	 * 
	 * @param Poc
	 *            the formbean of the poc containing alldetails of the Poc.
	 * @param lo_pc_id
	 *            id of the poc.
	 * @param ds
	 *            Object of DataSource.
	 * @return void
	 */
	public static void getPocdetails(AddPocBean poc, String lo_pc_id,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from func_cp_partner_poc_details('"
					+ lo_pc_id + "')";

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				poc.setLo_pc_id(rs.getString("lo_pc_id"));
				poc.setLo_pc_ad_id(rs.getString("lo_pc_ad_id"));
				poc.setFirstname(rs.getString("lo_pc_first_name"));
				poc.setLastname(rs.getString("lo_pc_last_name"));
				poc.setTitle(rs.getString("lo_pc_title"));
				poc.setPhone1(rs.getString("lo_pc_phone1"));
				poc.setPhone2(rs.getString("lo_pc_phone2"));
				poc.setFax(rs.getString("lo_pc_fax"));
				poc.setEmail1(rs.getString("lo_pc_email1"));
				poc.setEmail2(rs.getString("lo_pc_email2"));
				poc.setMobile(rs.getString("lo_pc_cell_phone"));
				poc.setFirstaddressline(rs.getString("lo_pc_address1"));
				poc.setSecondaddressline(rs.getString("lo_pc_address2"));
				poc.setCity(rs.getString("lo_pc_city"));
				poc.setState(rs.getString("lo_pc_state"));
				poc.setZip(rs.getString("lo_pc_zip_code"));
				poc.setCountry(rs.getString("lo_pc_country"));
				poc.setTimeZoneId(rs.getString("lo_pc_time_zone"));
				poc.setPvsContact(rs.getString("pvs_contact"));
				// added to get rank and invoice approval
				poc.setRank(rs.getString("lo_pc_rank"));
				poc.setInvoiceApproval(rs.getString("lo_pc_invoice_approval"));
			}

			sql = "select * from lo_user where lo_us_pc_id='" + lo_pc_id + "'";

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				poc.setUser_name(rs.getString("lo_us_name"));
				poc.setPassword(rs.getString("lo_us_pass"));
			}
		} catch (Exception e) {
			logger.error("getPocdetails(AddPocBean, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocdetails(AddPocBean, String, DataSource) - Error occured during getting Poc details "
						+ e);
			}

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPocdetails(AddPocBean, String, DataSource) - exception ignored",
						e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getPocdetails(AddPocBean, String, DataSource) - exception ignored",
						e);

			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getPocdetails(AddPocBean, String, DataSource) - exception ignored",
						e);

			}

		}
	}

	/**
	 * This method is used for getting the role list for assigning the role to
	 * Poc .
	 * 
	 * @param lo_pc_id
	 *            poc id.
	 * @param ds
	 *            Object of DataSource.
	 * @return String[] Poc's assigned role as array String.
	 */
	public static String[] getPocRoleArray(String lo_pc_id, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String poc_ad_id = "";
		String[] strArr = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lo_ro_id,lo_ro_role_desc from lo_role_desc_01 where lo_ro_id in"
					+ "(select lo_pr_ro_id from lo_poc_rol where lo_pr_pc_id='"
					+ lo_pc_id + "')";

			rs = stmt.executeQuery(sql);
			int flag = 0;
			while (rs.next()) {
				flag = 1;
				poc_ad_id = poc_ad_id + rs.getString("lo_ro_id") + ",";
			}
			if (flag == 1) {
				strArr = poc_ad_id.split(",");
			}

		} catch (Exception e) {
			logger.error("getPocRoleArray(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPocRoleArray(String, DataSource) - Error occured during getting Poc Role Array "
						+ e);
			}

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPocRoleArray(String, DataSource) - exception ignored",
						e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getPocRoleArray(String, DataSource) - exception ignored",
						e);

			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getPocRoleArray(String, DataSource) - exception ignored",
						e);

			}

		}
		return strArr;
	}

	/**
	 * This method is used for deleting Poc .
	 * 
	 * @param lo_pc_id
	 *            poc id.
	 * @param ds
	 *            Object of DataSource.
	 * @return int specifying the result of deletion.
	 */
	public static int deletePoc(String lo_pc_id, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int retval = -1;

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lo_poc_delete_01(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setString(2, lo_pc_id);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("deletePoc(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deletePoc(String, DataSource) - Error occured during deleting Poc "
						+ e);
			}

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deletePoc(String, DataSource) - exception ignored", e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deletePoc(String, DataSource) - exception ignored", e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"deletePoc(String, DataSource) - exception ignored", e);
			}

		}
		return retval;
	}

	/**
	 * This method is used for getting the role name separated by the ',' for
	 * assigning the role to Poc .
	 * 
	 * @param indexvalue
	 *            role id assigned to poc.
	 * @param ds
	 *            Object of DataSource.
	 * @return String the name of the roles in a single string separated by ','.
	 */
	public static String Rolename(String indexvalue, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String retval = "";

		try {
			if (!"".equals(indexvalue.trim())) {
				conn = ds.getConnection();
				stmt = conn.createStatement();

				String sql = "select dbo.func_cp_incident('"
						+ indexvalue.trim() + "','','U')";
				rs = stmt.executeQuery(sql);
				if (rs.next()) {
					retval = rs.getString(1);
				}
			}

		} catch (Exception e) {
			logger.error("Rolename(String, DataSource)", e);
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn("Rolename(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn("Rolename(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn("Rolename(String, DataSource) - exception ignored",
						e);
			}

		}
		return retval;
	}

	/**
	 * This method is used to get the State name by providing the state code as
	 * parameter the database by providing customer id as an argument.
	 * 
	 * @param stateid
	 *            the state id
	 * @param ds
	 *            Object of DataSource.
	 * @return String the name of the State.
	 */
	public static String getStateName(String stateid, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String stateName = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "SELECT cp_state_name FROM cp_state WHERE (cp_state_id='"
					+ stateid + "')";

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				stateName = rs.getString("cp_state_name");
			}

		} catch (Exception e) {
			logger.error("getStateName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStateName(String, DataSource) - Error occured during getting state Name "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateName(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateName(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getStateName(String, DataSource) - exception ignored",
						e);
			}

		}
		return stateName;
	}

	/**
	 * This method is for getting results on the search parameter entered by the
	 * user .
	 * 
	 * @param org
	 *            organization type parameter.
	 * @param type
	 *            type parameter entered by the user.
	 * @param division
	 *            division parameter entered by the user.
	 * @param poc
	 *            poc parameter entered by the user.
	 * @param ds
	 *            Object of DataSource.
	 * @return ArrayList results of the search .
	 */
	public static ArrayList getSearchResult(String org, String type,
			String division, String org_discipline_id, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList<CMSearchList> searchList = null;

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lo_search(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, org);
			cstmt.setString(3, type);
			cstmt.setString(4, division);
			cstmt.setInt(5, Integer.parseInt(org_discipline_id));
			rs = cstmt.executeQuery();

			while (rs.next()) {
				if (searchList == null) {
					searchList = new ArrayList<CMSearchList>();
				}
				String poc_type = "";
				if (rs.getInt("contact") == 0) {
					poc_type = "Add";
				} else {
					poc_type = "View";
				}

				searchList.add(new CMSearchList(rs.getString("lo_ot_id"), rs
						.getString("lo_ot_type"), rs.getString("lo_ot_name"),
						rs.getString("lo_om_id"), rs
								.getString("lo_om_division"), rs
								.getString("lo_ad_city"), rs
								.getString("lo_ad_state"), "0", poc_type, rs
								.getString("cp_pd_incident_type"), rs
								.getString("cp_partner_status")));
			}

		} catch (Exception e) {
			logger.error(
					"getSearchResult(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSearchResult(String, String, String, String, DataSource) - Error occured during getting customer search list  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchResult(String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchResult(String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getSearchResult(String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return searchList;
	}

	/**
	 * This method is for getting results for poc on the search parameter
	 * division entered by the user .
	 * 
	 * @param division
	 *            division parameter.
	 * @param poc
	 *            poc parameter.
	 * @param ds
	 *            Object of DataSource.
	 * @return ArrayList results of the search .
	 */
	public static ArrayList getPOCSearchResult(String division, String poc,
			DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList<CMPocSearchList> pocSearchList = null;

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lo_search_poc_list(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, division);
			cstmt.setString(3, poc);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				if (pocSearchList == null) {
					pocSearchList = new ArrayList<CMPocSearchList>();
				}

				pocSearchList.add(new CMPocSearchList(rs.getString("lo_pc_id"),
						rs.getString("contact"), rs.getString("lo_pc_phone"),
						rs.getString("lo_pc_fax"),
						rs.getString("lo_pc_email1"), rs
								.getString("lo_pc_address1"), rs
								.getString("lo_pc_address2"), rs
								.getString("lo_pc_city"), rs
								.getString("lo_pc_state"), rs
								.getString("lo_pc_zip_code"), rs
								.getString("lo_pc_country")));
			}

		} catch (Exception e) {
			logger.error("getPOCSearchResult(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPOCSearchResult(String, String, DataSource) - Error occured during getPOCSearchResult()  "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPOCSearchResult(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPOCSearchResult(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPOCSearchResult(String, String, DataSource) - exception ignored",
						e);
			}

		}
		return pocSearchList;
	}

	public static String getWebsite(String divison_id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String website = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql1 = "select lo_om_website from lo_organization_main where lo_om_id='"
					+ divison_id + "'";
			rs = stmt.executeQuery(sql1);
			if (rs.next()) {
				website = rs.getString("lo_om_website");
			}

		} catch (Exception e) {
			logger.error("getWebsite(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getWebsite(String, DataSource) - Error occured during get Web site "
						+ e);
			}

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getWebsite(String, DataSource) - exception ignored", e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getWebsite(String, DataSource) - exception ignored", e);

			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getWebsite(String, DataSource) - exception ignored", e);

			}

		}
		return website;
	}

	public static void getPartnerId(SearchBean searchBean, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lo_ot_id, lo_ot_name from lo_organization_top where lo_ot_type = 'P'";

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				if (rs.getString("lo_ot_name").equals("Certified Partners")) {
					searchBean.setCertifiedId(rs.getString("lo_ot_id"));
				}
				if (rs.getString("lo_ot_name").equals("Minuteman Partners")) {
					searchBean.setMinutemanId(rs.getString("lo_ot_id"));
				}
			}
		} catch (Exception e) {
			logger.error("getPartnerId(SearchForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerId(SearchForm, DataSource) - Error occured during getting Partner Id "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPartnerId(SearchForm, DataSource) - exception ignored",
						e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getPartnerId(SearchForm, DataSource) - exception ignored",
						e);

			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getPartnerId(SearchForm, DataSource) - exception ignored",
						e);

			}

		}
	}

	public static String getPVSPartnerId(String partnerId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		String resultId = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select cp_partner_id from cp_partner where cp_partner_om_id="
					+ partnerId;
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				resultId = rs.getString("cp_partner_id");
			}
		} catch (Exception e) {
			logger.error("getPVSPartnerId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPVSPartnerId(String, DataSource) - Error is getting partnerId"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"getPVSPartnerId(String, DataSource) - exception ignored",
						e);

			}
			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getPVSPartnerId(String, DataSource) - exception ignored",
						e);

			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (Exception e) {
				logger.warn(
						"getPVSPartnerId(String, DataSource) - exception ignored",
						e);

			}

		}
		return resultId;

	}

	public static int checkPOAssign(String cp_om_id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int checkPoAssign = 0; // its same when this partner is not assign to
		// PO.

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lm_po_id from lo_organization_main inner join cp_partner on lo_om_id=cp_partner_om_id inner join lm_purchase_order "
					+ "on cp_partner_id = lm_po_partner_id where lo_om_id = "
					+ cp_om_id;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				checkPoAssign = -9003;
			}

		} catch (Exception e) {
			logger.error("checkPOAssign(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkPOAssign(String, DataSource) - Exception caught in CustomerManagerDao.checkPOAssign  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"checkPOAssign(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkPOAssign(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkPOAssign(String, DataSource) - exception ignored",
						sql);
			}

		}
		return checkPoAssign;
	}

	public static String getEndCustomerNameByMSAId(String msaId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String customerName = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT lo_ot_name FROM lo_organization_top "
					+ " INNER JOIN lp_msa_main ON lo_ot_id = lp_mm_ot_id"
					+ " WHERE lp_mm_pi_id = " + msaId;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				customerName = rs.getString("lo_ot_name");
			}

		} catch (Exception e) {
			logger.error("getEndCustomerNameByMSAId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEndCustomerNameByMSAId(String, DataSource) - Exception caught in CustomerManagerDao.checkPOAssign  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getEndCustomerNameByMSAId(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getEndCustomerNameByMSAId(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getEndCustomerNameByMSAId(String, DataSource) - exception ignored",
						sql);
			}

		}
		return customerName;
	}

	public static void updateHeadCountTerminationDate(String hcId, DataSource ds) {
		Connection conn = null;
		Statement st = null;
		try {

			conn = ds.getConnection();
			st = conn.createStatement();
			String sql = "UPDATE dbo.rpt_personal_headcount "
					+ " SET rpt_hc_termination_date =  getDate() "
					+ " WHERE rpt_hc_id =  " + hcId;

			st.executeUpdate(sql);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
		} finally {

			try {
				if (st != null) {
					st.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"updateHeadCountTerminationDate - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"updateHeadCountTerminationDate - exception ignored",
						sql);
			}

		}

	}

	public static void addIntoHeadCount(String deptName, AddPocBean addPocBean,
			DataSource ds) {
		Connection conn = null;
		Statement st = null;
		try {

			String terminatedDate = "NULL";
			if (addPocBean.getEmpStatus().equals("0")) {
				terminatedDate = "getDate()";
			}

			conn = ds.getConnection();
			st = conn.createStatement();
			String sql = "INSERT INTO dbo.rpt_personal_headcount  "
					+ " (rpt_hc_lo_pc_id, rpt_hc_f_name, rpt_hc_l_name, rpt_hc_is_active_user, rpt_hc_department, "
					+ " rpt_hc_rank, rpt_hc_department_change_date, rpt_hc_termination_date, rpt_hc_title) "
					+ " VALUES (" + addPocBean.getLo_pc_id() + ", '"
					+ addPocBean.getFirstname() + "', '"
					+ addPocBean.getLastname() + "', "
					+ addPocBean.getEmpStatus() + ", '" + deptName + "', '"
					+ addPocBean.getRank() + "', getDate(), " + terminatedDate
					+ ", '" + addPocBean.getTitle() + "'" + ")";

			logger.info(sql);
			st.executeUpdate(sql);

		} catch (Exception ex) {
			logger.error(ex.getMessage());

		} finally {
			try {
				if (st != null) {
					st.close();
				}
			} catch (SQLException sql) {
				logger.warn("addIntoHeadCount - exception ignored", sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn("addIntoHeadCount - exception ignored", sql);
			}

		}

	}

	public static String[] getHeadCountIdByUserId(String userId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] customerName = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT rpt_hc_id, rpt_hc_department, rpt_hc_rank,rpt_hc_is_active_user,rpt_hc_title"
					+ " FROM rpt_personal_headcount "
					+ " WHERE rpt_hc_lo_pc_id =  "
					+ userId
					+ " AND rpt_hc_termination_date IS NULL  ";
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				customerName = new String[5];
				customerName[0] = rs.getString("rpt_hc_id");
				customerName[1] = rs.getString("rpt_hc_department");
				customerName[2] = rs.getString("rpt_hc_rank");
				customerName[3] = rs.getString("rpt_hc_is_active_user");
				customerName[4] = rs.getString("rpt_hc_title");
			}

		} catch (Exception e) {
			logger.error("getHeadCountIdByUserId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getHeadCountIdByUserId(String, DataSource) - Exception caught in CustomerManagerDao.checkPOAssign  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getHeadCountIdByUserId(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getHeadCountIdByUserId(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getHeadCountIdByUserId(String, DataSource) - exception ignored",
						sql);
			}

		}
		return customerName;
	}

	public static Date getTerminationDate(String id, DataSource ds) {
		Date terminationDate = null;
		Connection conn = null;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT lo_pc_termination_date FROM lo_poc "
					+ " WHERE lo_pc_id =  " + id;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				terminationDate = rs.getDate("lo_pc_termination_date");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return terminationDate;
	}

	public static Boolean isDeptChanged(String lo_pc_id, String rank,
			String dept_Id, DataSource ds) {
		Boolean isSameDept = false;// consider that result is not found in DB.
		Connection conn = null;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT 1 FROM lo_poc WHERE  " + " lo_pc_id =  "
					+ lo_pc_id + " AND lo_pc_rank = '" + rank
					+ "' AND lo_pc_role = " + dept_Id;
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				isSameDept = true;
				/*
				 * will return true if found
				 */
			} else {
				isSameDept = false;// false means not found in DB
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isSameDept;
	}

	// add function to update status
	public static void updateHeadCountEmpStatus(String userId,
			String emp_status, DataSource ds) {
		Connection conn = null;
		Statement st = null;
		try {

			conn = ds.getConnection();
			st = conn.createStatement();
			String sql = "UPDATE dbo.rpt_personal_headcount "
					+ " SET rpt_hc_is_active_user = " + emp_status
					+ " WHERE rpt_hc_lo_pc_id   =  " + userId;

			st.executeUpdate(sql);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		} finally {

			try {
				if (st != null) {
					st.close();
				}
			} catch (SQLException sql) {
				logger.warn("updateHeadCountEmpStatus - exception ignored", sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn("updateHeadCountEmpStatus - exception ignored", sql);
			}

		}

	}
}
