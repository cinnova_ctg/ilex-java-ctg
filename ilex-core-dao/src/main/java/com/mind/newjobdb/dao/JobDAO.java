package com.mind.newjobdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.im.InvoiceJobDetailDTO;
import com.mind.bean.msp.SiteInfo;
import com.mind.bean.newjobdb.Activity;
import com.mind.bean.newjobdb.CostInfo;
import com.mind.bean.newjobdb.CustomerReferenceInfo;
import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.InstallNotes;
import com.mind.bean.newjobdb.JobActivities;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobInvoiceSummary;
import com.mind.bean.newjobdb.JobPlanningNote;
import com.mind.bean.newjobdb.PlanningNote;
import com.mind.bean.newjobdb.ScheduleInfo;
import com.mind.bean.newjobdb.TeamInfo;
import com.mind.common.ChangeDateFormat;
import com.mind.common.LabelValue;
import com.mind.common.Util;
import com.mind.common.dao.AddComment;
import com.mind.common.dao.DynamicComboDao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * @purpose This class is used to contain the functionality for the data access
 * related to Job
 */
public class JobDAO {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(JobDAO.class);

    /**
     * getGeneralJobInfo - This method returns the GeneralJobInfo object for the
     * Job Id. method fetchs data from the database on the basis of Job Id.
     *
     * @param jobId - the Job Id for which the GeneralJobInfo is required.
     * @param DataSource - DataSource for creating desired DB Connection.
     * @return Returns the GeneralJobInfo Object.
     */
    public GeneralJobInfo getGeneralJobInfo(long jobId, DataSource ds) {
        GeneralJobInfo generalJobInfo = new GeneralJobInfo();
        generalJobInfo.setScheduleInfo(this.getScheduleInfo(jobId, ds));
        generalJobInfo.setCustomerReferenceInfo(this.getCustomerReferenceInfo(
                jobId, ds));
        generalJobInfo.setTeamInfo(this.getTeamInfo(jobId, ds));
        generalJobInfo.setSiteInfo(this.getSiteInfo(Long.valueOf(jobId)
                .toString(), ds));
        generalJobInfo.setJobStatus(this.getJobStatus(jobId, ds));
        String[] addendumStatus = this.getAddendumJobStatus(jobId, ds);
        generalJobInfo.setIsAddendumApproved(this
                .isAddendumApproved(addendumStatus));
        generalJobInfo.setAddendumStatus(addendumStatus[1]);
        generalJobInfo.setJobId(jobId);
        return generalJobInfo;
    }

    /**
     * Get Addendum is Approved or not.
     *
     * @param addendumStatus
     * @return
     */
    private static String isAddendumApproved(String[] addendumStatus) {
        if (addendumStatus[0] != null) {
            if (addendumStatus[0].equals("D") || addendumStatus[0].equals("R")
                    || addendumStatus[0].equals("C")) {
                return "N";
            } else {
                return "Y";
            }
        }
        return "";
    }

    /**
     * getScheduleInfo - This method returns the ScheduleInfo object for the Job
     * Id. method fetchs data from the database on the basis of Job Id.
     *
     * @param jobId - the Job Id for which the ScheduleInfo is required.
     * @param DataSource - DataSource for creating desired DB Connection.
     * @return Returns the ScheduleInfo Object.
     */
    public static ScheduleInfo getScheduleInfo(long jobId, DataSource ds) {
        String prjDisplayStatus = null;
        ScheduleInfo scheduleInfo = new ScheduleInfo();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        Statement stmtActual = null;
        ResultSet rsActual = null;
        try {
            conn = ds.getConnection();
            String sql = "select lm_js_se_schedule,lm_js_rse_count=isnull(lm_js_rse_count,0),lm_js_rse_client_count=isnull(lm_js_rse_client_count,0),lm_js_rse_contingent_count=isnull(lm_js_rse_contingent_count,0),lm_js_rse_internal_count=isnull(lm_js_rse_internal_count,0),lm_js_prj_display_status, replace(CONVERT(VARCHAR(20), isnull(lm_js_planned_start_date,''), 100),'Jan  1 1900 12:00AM','') as lm_js_planned_start_date, replace(CONVERT(VARCHAR(20), isnull(lm_js_planned_end_date,''), 100),'Jan  1 1900 12:00AM','') as lm_js_planned_end_date from lm_job where lm_js_id ="
                    + "'" + jobId + "'";
            String sqlActual = "select lx_se_id, replace(Convert(Varchar(25),isnull(convert(datetime ,lx_se_act_start),''),100),'Jan  1 1900 12:00AM','')  as lx_se_act_start, "
                    + " replace(Convert(Varchar(25),isnull(convert(datetime ,lx_se_act_end),''),100),'Jan  1 1900 12:00AM','') as lx_se_act_end "
                    + "from lx_schedule_list_01 where lx_se_type_id="
                    + jobId
                    + " and lx_se_id = (select max(lx_se_id) from lx_schedule_list_01 where lx_se_type_id="
                    + jobId + ")";
            if (logger.isDebugEnabled()) {
                logger.debug("getScheduleInfo(long, DataSource) - sqlActual: "
                        + sqlActual);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("getScheduleInfo(long, DataSource) - sql: " + sql);
            }
            stmt = conn.createStatement();
            stmtActual = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                prjDisplayStatus = rs.getString("lm_js_prj_display_status");
                scheduleInfo
                        .setReScheduleCount(rs.getString("lm_js_rse_count"));
                scheduleInfo.setReScheduleClientCount(rs
                        .getString("lm_js_rse_client_count"));
                scheduleInfo.setReScheduleContingentCount(rs
                        .getString("lm_js_rse_contingent_count"));
                scheduleInfo.setReScheduleInternalCount(rs
                        .getString("lm_js_rse_internal_count"));

                scheduleInfo.setScheduleId(rs.getString("lm_js_se_schedule"));
                // Setting schedule type based on Project Status
                if (prjDisplayStatus != null
                        && (prjDisplayStatus.equals("Confirmed") || prjDisplayStatus
                        .equals("Scheduled"))) {
                    scheduleInfo.setScheduleType("Planned Schedule");

                    // IlexTimestamp.converToTimestamp();
                    if (rs.getString("lm_js_planned_start_date") != null
                            && !rs.getString("lm_js_planned_start_date")
                            .equals("")) {
                        scheduleInfo.setStartDate(ChangeDateFormat
                                .getDateFormat(rs
                                        .getString("lm_js_planned_start_date"),
                                        "MM/dd/yyyy hh:mm aaa",
                                        "MMM dd yyyy hh:mmaaa"));
                    } else {
                        scheduleInfo.setStartDate("");
                    }
                    if (rs.getString("lm_js_planned_end_date") != null
                            && !rs.getString("lm_js_planned_end_date").equals(
                                    "")) {
                        scheduleInfo
                                .setEndDate(ChangeDateFormat.getDateFormat(
                                                rs.getString("lm_js_planned_end_date"),
                                                "MM/dd/yyyy hh:mm aaa",
                                                "MMM dd yyyy hh:mmaaa"));
                    } else {
                        scheduleInfo.setEndDate("");
                    }

                } else if (prjDisplayStatus != null
                        && (prjDisplayStatus.equals("Not Scheduled")
                        || prjDisplayStatus.equals("Hold") || prjDisplayStatus
                        .equals("Cancelled"))) {
                    scheduleInfo.setScheduleType("No schedule available");
                } else {
                    rsActual = stmtActual.executeQuery(sqlActual);
                    scheduleInfo.setScheduleType("Actual Schedule");
                    if (rsActual.next()) {
                        if (rsActual.getString("lx_se_act_start") != null
                                && !rsActual.getString("lx_se_act_start")
                                .equals("")) {
                            scheduleInfo.setStartDate(ChangeDateFormat
                                    .getDateFormat(rsActual
                                            .getString("lx_se_act_start"),
                                            "MM/dd/yyyy hh:mm aaa",
                                            "MMM dd yyyy hh:mmaaa"));
                        } else {
                            scheduleInfo.setStartDate("");
                        }
                        if (rsActual.getString("lx_se_act_end") != null
                                && !rsActual.getString("lx_se_act_end").equals(
                                        "")) {
                            scheduleInfo
                                    .setEndDate(ChangeDateFormat.getDateFormat(
                                                    rsActual.getString("lx_se_act_end"),
                                                    "MM/dd/yyyy hh:mm aaa",
                                                    "MMM dd yyyy hh:mmaaa"));
                        } else {
                            scheduleInfo.setEndDate("");
                        }
                    }
                }

                if (scheduleInfo.getScheduleType().equals("Planned Schedule")
                        || scheduleInfo.getScheduleType().equals(
                                "Actual schedule")) {
                    if (scheduleInfo.getReScheduleCount() != null
                            && !scheduleInfo.getReScheduleCount().equals("")
                            && !scheduleInfo.getReScheduleCount().equals("0")) {
                    } else {
                        scheduleInfo.setScheduleType(scheduleInfo
                                .getScheduleType() + ":");
                    }
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("getScheduleInfo(long, DataSource) - prjDisplayStatus ");
                }

                // System.out.println("scheduleInfo getDate "+
                // rs.getDate("lx_se_end"));
            }

        } catch (Exception e) {
            logger.error("getScheduleInfo(long, DataSource)", e);
        } finally {
            try {
                DBUtil.close(stmtActual);
                // Closing DB Resources
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;

                }
            } catch (Exception e) {
                logger.error("getScheduleInfo(long, DataSource)", e);
            }

        }
        return scheduleInfo;
    }

    /*
     * private ScheduleInfo getScheduleInfo(long jobId, DataSource ds){ String
     * prjDisplayStatus = null; ScheduleInfo scheduleInfo = new ScheduleInfo();
     * Connection conn = null; Statement stmt = null; ResultSet rs = null; try{
     * conn = ds.getConnection(); String sql =
     * "select lm_js_prj_display_status,CONVERT(VARCHAR(10), lx_se_act_start, 101) as lx_se_act_start,CONVERT(VARCHAR(10), lx_se_act_end, 101) as lx_se_act_end,CONVERT(VARCHAR(10), lx_se_act_end, 101) as lx_se_act_end, CONVERT(VARCHAR(10), lx_se_end, 101) as lx_se_end from lx_schedule_list_01 where lx_se_type_id="
     * +"'"+jobId+"'"; stmt = conn.createStatement(); rs =
     * stmt.executeQuery(sql); if(rs.next()){ prjDisplayStatus =
     * rs.getString("lm_js_prj_display_status"); //Setting schedule type based
     * on Project Status if(prjDisplayStatus != null &&
     * (prjDisplayStatus.equals("Confirmed") ||
     * prjDisplayStatus.equals("Scheduled")) ){
     * scheduleInfo.setScheduleType("Planned Schedule");
     * scheduleInfo.setStartDate(rs.getString("lx_se_start"));
     * scheduleInfo.setEndDate(rs.getString("lx_se_end")); } else
     * if(prjDisplayStatus != null && (prjDisplayStatus.equals("Not Scheduled")
     * || prjDisplayStatus.equals("Hold") ||
     * prjDisplayStatus.equals("Cancelled")) ){
     * scheduleInfo.setScheduleType("No schedule available"); } else {
     * scheduleInfo.setScheduleType("Actual schedule");
     * scheduleInfo.setStartDate(rs.getString("lx_se_act_start"));
     * scheduleInfo.setEndDate(rs.getString("lx_se_act_end")); }
     * System.out.println("prjDisplayStatus ");
     *
     * // System.out.println("scheduleInfo getDate "+ rs.getDate("lx_se_end"));
     * }
     *
     * }catch(Exception e){ e.printStackTrace(); }finally{ try{ // Closing DB
     * Resources if(rs!=null){ rs.close(); rs = null; } if(stmt!=null){
     * stmt.close(); stmt = null; } if(conn!=null){ conn.close(); conn = null;
     *
     * } }catch(Exception e){ e.printStackTrace(); }
     *
     * } return scheduleInfo; }
     */
    /**
     * getCustomerReferenceInfo - This method returns the CustomerReferenceInfo
     * object for the Job Id. method fetchs data from the database on the basis
     * of Job Id.
     *
     * @param jobId - the Job Id for which the CustomerReferenceInfo is
     * required.
     * @param DataSource - DataSource for creating desired DB Connection.
     * @return Returns the CustomerReferenceInfo Object.
     */
    private CustomerReferenceInfo getCustomerReferenceInfo(long jobId,
            DataSource ds) {
        CustomerReferenceInfo customerReferenceInfo = new CustomerReferenceInfo();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = ds.getConnection();
            String sql = "select lm_js_customer_reference from lm_job where lm_js_id="
                    + "'" + jobId + "'";
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                customerReferenceInfo.setReference(rs
                        .getString("lm_js_customer_reference"));

                if (logger.isDebugEnabled()) {
                    logger.debug("getCustomerReferenceInfo(long, DataSource) - "
                            + rs.getString("lm_js_customer_reference")
                            + " :: lm_js_customer_reference");
                }
            }

        } catch (Exception e) {
            logger.error("getCustomerReferenceInfo(long, DataSource)", e);

            logger.error("getCustomerReferenceInfo(long, DataSource)", e);
        } finally {
            try {
                // Closing DB Resources
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;

                }
            } catch (Exception e) {
                logger.error("getCustomerReferenceInfo(long, DataSource)", e);

                logger.error("getCustomerReferenceInfo(long, DataSource)", e);
            }

        }
        return customerReferenceInfo;

    }

    /**
     * getJobStatus - This method returns the Job Status as String for the Job
     * Id. method fetchs data from the database on the basis of Job Id.
     *
     * @param jobId - the Job Id for which the CustomerReferenceInfo is
     * required.
     * @param DataSource - DataSource for creating desired DB Connection.
     * @return Returns the Job Status as String.
     */
    private String getJobStatus(long jobId, DataSource ds) {
        String jobStatus = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = ds.getConnection();
            String sql = "select isnull(lm_js_prj_display_status,'Not Scheduled') as lm_js_prj_display_status from lm_job where lm_js_id="
                    + "'" + jobId + "'";
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                jobStatus = rs.getString("lm_js_prj_display_status");
                if (logger.isDebugEnabled()) {
                    logger.debug("getJobStatus(long, DataSource) - "
                            + rs.getString("lm_js_prj_display_status")
                            + " :: lm_js_prj_display_status");
                }
            }

        } catch (Exception e) {
            logger.error("getJobStatus(long, DataSource)", e);

            logger.error("getJobStatus(long, DataSource)", e);
        } finally {
            try {
                // Closing DB Resources
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;

                }
            } catch (Exception e) {
                logger.error("getJobStatus(long, DataSource)", e);

                logger.error("getJobStatus(long, DataSource)", e);
            }

        }
        return jobStatus;

    }

    /**
     * Method used to get the status of Addendum in Project Manager
     *
     * @param jobId
     * @param ds
     * @return
     */
    private String[] getAddendumJobStatus(long jobId, DataSource ds) {
        String[] jobStatus = new String[2];
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {
            conn = ds.getConnection();
            String sql = "select lm_js_status,cc_status_description "
                    + "from lm_job inner join cc_status_master on cc_status_code = lm_js_status "
                    + "where lm_js_id = ? and lm_js_type = ? ";
            pStmt = conn.prepareStatement(sql);
            pStmt.setLong(1, jobId);
            pStmt.setString(2, "Addendum");
            rs = pStmt.executeQuery();
            if (rs.next()) {
                jobStatus[0] = rs.getString("lm_js_status"); // - Status Code
                jobStatus[1] = rs.getString("cc_status_description"); // -
                // Status
                // Description
            }
        } catch (Exception e) {
            logger.error("getAddendumJobStatus(long, DataSource)", e);

            logger.error("getAddendumJobStatus(long, DataSource)", e);
        } finally {
            DBUtil.close(rs, pStmt);
            DBUtil.close(conn);
        }
        return jobStatus;
    }

    public TeamInfo getTeamInfo(long jobId, DataSource ds) {
        TeamInfo teamInfo = new TeamInfo();
        String jobStatus = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        ArrayList schedulers = new ArrayList();
        try {
            conn = ds.getConnection();
            String sql = "select * from dbo.func_lp_prj_jobdashboard_tab( '%' , '"
                    + jobId + "' , '%'  )";
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                teamInfo.setOwner(rs.getString("job_created_by"));
                schedulers.add(getCurrentScheduler(Long.valueOf(jobId)
                        .toString(), ds));
                teamInfo.setSchedulers(schedulers);
            }

        } catch (Exception e) {
            logger.error("getTeamInfo(long, DataSource)", e);

            logger.error("getTeamInfo(long, DataSource)", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (Exception e) {
                logger.error("getTeamInfo(long, DataSource)", e);

                logger.error("getTeamInfo(long, DataSource)", e);
            }

        }
        return teamInfo;
    }

    public static String getCurrentScheduler(String jobId, DataSource ds) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String currentScheduler = "";

        // System.out.println("jobId = "+jobId);
        try {
            conn = ds.getConnection();
            pstmt = conn
                    .prepareStatement("select curr_scheduler = isnull(dbo.func_cc_poc_name( lm_js_scheduler ),'') "
                            + "from lm_job where lm_js_id = ? ");
            pstmt.setString(1, jobId);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                currentScheduler = rs.getString("curr_scheduler");
            }

        } catch (Exception e) {
            logger.error("getCurrentScheduler(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getCurrentScheduler(String, DataSource) - Error occured com.mind.dao.PRM.AssignOwnerdao.getCurrentScheduler() "
                        + e);
            }
        } finally {
            DBUtil.close(rs, pstmt);
            DBUtil.close(conn);

        }
        return currentScheduler;
    }

	public static SiteInfo getSiteInfo(String jobId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String currentScheduler = "";
		SiteInfo siteInfo = new SiteInfo();
		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select *,lp_sl_priority_store from lm_job left outer join lm_site_detail "
							+ " on lm_js_si_id=lm_si_id "
							+ "   	LEFT  join lp_site_list  on lp_site_list.lp_sl_id=lm_site_detail.lm_si_sl_id    "
							+ "   where lm_js_id=? ");
			pstmt.setString(1, jobId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				siteInfo.setSite_number(rs.getString("lm_si_number"));
				siteInfo.setSite_city(rs.getString("lm_si_city"));
				siteInfo.setSite_country(rs.getString("lm_si_country"));
				siteInfo.setSite_state(rs.getString("lm_si_state"));
				siteInfo.setSite_name(rs.getString("lm_si_name"));
				siteInfo.setSite_address(rs.getString("lm_si_address"));
				siteInfo.setSite_zip(rs.getString("lm_si_zip_code"));
				siteInfo.setSite_primary_poc(rs.getString("lm_si_pri_poc"));
				siteInfo.setSite_secondary_poc(rs.getString("lm_si_sec_poc"));
				siteInfo.setSite_primary_poc_email(rs
						.getString("lm_si_pri_email_id"));
				siteInfo.setSite_secondary_poc_email(rs
						.getString("lm_si_sec_email_id"));
				siteInfo.setSite_primary_phone(rs.getString("lm_si_phone_no"));
				siteInfo.setSite_secondary_phone(rs
						.getString("lm_si_sec_phone_no"));
				siteInfo.setSite_latitude(rs.getString("lm_si_latitude"));
				siteInfo.setSite_longitude(rs.getString("lm_si_longitude"));
				siteInfo.setSite_time_zone(Util.getListValue(
						(ArrayList<LabelValue>) DynamicComboDao.gettimezone(ds),
						rs.getString("lm_si_time_zone"), "value"));
				String priority = rs.getString("lp_sl_priority_store");
				if (priority.equals("0")) {

					siteInfo.setPriority("N");

				} else {

					siteInfo.setPriority("Y");
				}

				if (rs.getString("lm_si_country") != null
						&& rs.getString("lm_si_country").equalsIgnoreCase("US")) {
					siteInfo.setUS(true);
				} else {
					siteInfo.setUS(false);
				}
			}
		} catch (Exception e) {
			logger.error("getSiteInfo(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getSiteInfo(String, DataSource) - Error occured com.mind.dao.PRM.AssignOwnerdao.getSiteInfo() "
                        + e);
            }
        } finally {
            DBUtil.close(rs, pstmt);
            DBUtil.close(conn);
        }
        return siteInfo;
    }

    public PlanningNote getJobPlanningNotes(long jobId, DataSource ds) {
        PlanningNote planningNote = new JobPlanningNote();
        planningNote.setNote(this.getJobNotes(jobId, ds));
        return planningNote;
    }

    public static String getLatestInstallNotesOld(String jobId, DataSource ds) {
        String latestInstallNotes = "";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = ds.getConnection();
            String sql = "select lm_in_notes = isnull(lm_in_notes,'') from lm_install_notes where lm_in_js_id ="
                    + jobId
                    + "  and lm_in_id = (select max(lm_in_id) from lm_install_notes where lm_in_js_id = "
                    + jobId + ") ";
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                latestInstallNotes = rs.getString("lm_in_notes");
            }
        } catch (Exception e) {
            logger.error("getLatestInstallNotesOld(String, DataSource)", e);

            logger.error("getLatestInstallNotesOld(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }
        return latestInstallNotes.trim();
    }

    public static ArrayList<InstallNotes> getInstallNotes(String jobId,
            DataSource ds) {
        ArrayList<InstallNotes> installNotesAll = new ArrayList<InstallNotes>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = ds.getConnection();
            String sql = "select convert(varchar(10), lm_in_cdate, 1)+' '+convert(varchar(10),lm_in_cdate,108) as lm_in_cdate , lm_in_notes, uname=dbo.func_cc_poc_name(lm_in_cuser)"
                    + " from  lm_install_notes where lm_in_js_id = "
                    + jobId
                    + " order by lm_in_cdate desc";
            if (logger.isDebugEnabled()) {
                logger.debug("getInstallNotes(String, DataSource) - Sql for installation view -"
                        + sql);
            }
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                InstallNotes installNotes = new InstallNotes();
                if (logger.isDebugEnabled()) {
                    logger.debug("getInstallNotes(String, DataSource) - ************************** = "
                            + rs.getString("lm_in_cdate"));
                }
                installNotes.setDate(ChangeDateFormat.getDateFormat(
                        rs.getString("lm_in_cdate"), "MM/dd/yyyy hh:mm aaa",
                        "MM/dd/yy HH:mm:ss"));
                installNotes.setInstallNote(Util.replaceToBr(rs
                        .getString("lm_in_notes")));
                installNotes.setUser(rs.getString("uname"));
                installNotesAll.add(installNotes);
            }
        } catch (Exception e) {
            logger.error("getInstallNotes(String, DataSource)", e);

            logger.error("getInstallNotes(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }
        return installNotesAll;
    }

    public static void setOniseOffsite(JobDashboardBean bean, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String actualStart = "";
        String actualEnd = "";
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            // String
            // sql="select * from lx_schedule_list_01 where lx_se_type_id="+typeid+" AND lx_se_type='"+type+"'";
            // String sql =
            // "select lx_se_act_start, lx_se_act_end,lx_se_sequence from lx_schedule_list_01 where lx_se_type_id="
            // + bean.getJobId();
            String sql = "SELECT TOP 1 lx_se_act_start, lx_se_act_end,lx_se_sequence from lx_schedule_list_01 where lx_se_type_id = "
                    + bean.getJobId() + " ORDER BY lx_se_act_start DESC";
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                actualStart = rs.getString("lx_se_act_start");
                actualEnd = rs.getString("lx_se_act_end");
                bean.setCountDispatch(rs.getString("lx_se_sequence"));
            }

            IlexTimestamp onsite = IlexTimestamp.converToTimestamp(actualStart);
            IlexTimestamp offsite = IlexTimestamp.converToTimestamp(actualEnd);
            bean.setOnsiteDate((onsite.getDate().equalsIgnoreCase("01/01/1900")) ? ""
                    : onsite.getDate());
            bean.setOnsiteHour(onsite.getHour());
            bean.setOnsiteMinute(onsite.getMinute());
            bean.setOnsiteIsAm(onsite.isAM() == true ? "1" : "0");

            bean.setOffsiteDate((offsite.getDate()
                    .equalsIgnoreCase("01/01/1900")) ? "" : offsite.getDate());
            bean.setOffsiteHour(offsite.getHour());
            bean.setOffsiteMinute(offsite.getMinute());
            bean.setOffsiteIsAm(offsite.isAM() == true ? "1" : "0");

            if (logger.isDebugEnabled()) {
                logger.debug("setOniseOffsite(JobDashboardBean, DataSource) - actstart1111111= "
                        + bean.getOnsiteDate()
                        + " "
                        + bean.getOnsiteHour()
                        + ":"
                        + bean.getOnsiteMinute()
                        + " "
                        + bean.getOnsiteIsAm());
            }
            if (logger.isDebugEnabled()) {
                logger.debug("setOniseOffsite(JobDashboardBean, DataSource) - actend1111111= "
                        + bean.getOffsiteDate()
                        + " "
                        + bean.getOffsiteHour()
                        + ":"
                        + bean.getOffsiteMinute()
                        + " "
                        + bean.getOffsiteIsAm());
            }

        } catch (Exception e) {
            logger.error("setOniseOffsite(JobDashboardBean, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("setOniseOffsite(JobDashboardBean, DataSource) - Error occured during getting schedule  ");
            }
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }

    }

    public String getJobNotes(long jobId, DataSource ds) {
        String jobNotes = "";
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        String sql = "";
        try {
            conn = ds.getConnection();
            sql = getSqlForNotesByJobType(jobId, ds);
            pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, String.valueOf(jobId));
            rs = pStmt.executeQuery();
            if (rs.next()) {
                jobNotes = rs.getString("notes");
            }
        } catch (Exception e) {
            logger.error("getJobNotes(long, DataSource)", e);

            logger.error("getJobNotes(long, DataSource)", e);
        } finally {
            DBUtil.close(rs, pStmt);
            DBUtil.close(conn);

        }
        return jobNotes;
    }

    /**
     * Method used to get the sql for job notes based on job type
     *
     * @param jobId
     * @param ds
     * @return
     */
    public static String getSqlForNotesByJobType(long jobId, DataSource ds) {
        String sql = "";
        if (JobSetUpDao.getjobType(String.valueOf(jobId), ds).equals("Default")
                || JobSetUpDao.getjobType(String.valueOf(jobId), ds).equals(
                        "Addendum")) {
            sql = "select notes from prj_lvl_job_notes "
                    + " inner join lm_job on appendix_id = lm_js_pr_id and lm_js_id = ?";
        } else {
            sql = "Select * from dbo.job_lvl_job_notes where job_id = ?";
        }
        // System.out.println("sql-------------------"+sql);
        return sql;
    }

    // this method takes jobId and returns SiteId if a site is assigned to the
    // job, else blank script
    public static String getSiteId(String jobId, DataSource ds) {
        String siteId = "";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        if (jobId == null || jobId.equals("")) {
            return siteId;
        }

        try {
            conn = ds.getConnection();
            String sql = "select lm_js_si_id from lm_job where lm_js_id ="
                    + jobId;
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                siteId = rs.getString("lm_js_si_id");
            }
        } // - end of try
        catch (Exception e) {
            logger.error("getSiteId(String, DataSource)", e);

            logger.error("getSiteId(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }

        return siteId;
    }

    // thiss method takes jobId and return trues if job is a ticket
    public static String getTicketId(String jobId, DataSource ds) {
        String ticketId = "";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        if (jobId == null || jobId.equals("")) {
            return ticketId;
        }
        java.util.Date dateIn = new java.util.Date();

        try {
            conn = ds.getConnection();
            String sql = "select lm_tc_id from lm_ticket where lm_tc_js_id ="
                    + jobId;
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                ticketId = rs.getString(1);
            }
        } // - end of try
        catch (Exception e) {
            logger.error("getTicketId(String, DataSource)", e);

            logger.error("getTicketId(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }
        java.util.Date dateOut = new java.util.Date();
        if (logger.isDebugEnabled()) {
            logger.debug("getTicketId(String, DataSource) - **** getTicketId :: time taken :: "
                    + (dateOut.getTime() - dateIn.getTime()));
        }
        return ticketId;
    }

    /**
     * Discription: This mehtod return activity list from Database on the basis
     * of Job Id.
     *
     * @param jobId -- Job Id
     * @param ds -- Database Object
     * @return Object of JobActivities
     */
    public JobActivities getActivityList(String jobId, DataSource ds) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        ArrayList actList = new ArrayList();
        JobActivities jobActivities = new JobActivities();
        try {
            conn = ds.getConnection();
            String sql = "select * from dbo.func_lp_jobdashboard_activitysummary_tab(?)"; // -
            // 1
            // parameter

            pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, jobId);
            rs = pStmt.executeQuery();

            while (rs.next()) {
                Activity act = new Activity();
                CostInfo estCost = new CostInfo();
                CostInfo extCost = new CostInfo();

                act.setActivityId(rs.getLong("lm_at_id"));
                act.setActivityName(rs.getString("lm_at_title"));
                act.setScope(rs.getString("scope"));
                act.setUplifts(rs.getFloat("uplift_cost"));
                act.setQuantity(rs.getFloat("lx_ce_quantity"));
                estCost.setUnit(rs.getFloat("unit_cost"));
                estCost.setTotal(rs.getFloat("lx_ce_total_cost"));
                extCost.setUnit(rs.getFloat("unit_price"));
                extCost.setTotal(rs.getFloat("lx_ce_total_price"));
                act.setExtendedCostInfo(extCost);
                act.setEstimatedCostInfo(estCost);
                act.setActivityLibraryId(rs.getString("lm_at_lib_id"));
                act.setActivity_oos_flag(rs.getString("lm_at_oos_flag")); // Used
                // for
                // OOS
                // activity
                act.setActivity_lx_ce_type(rs.getString("lx_ce_type"));
                act.setActivityType(rs.getString("rew"));
                actList.add(act);

            } // - end of while
            jobActivities.setActivities(actList);
        } // - end of try
        catch (Exception e) {
            logger.error("getActivityList(String, DataSource)", e);

            logger.error("getActivityList(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, pStmt);
            DBUtil.close(conn);
        }

        return jobActivities;
    } // - end of getActivityList() method

    public static void addComments(String comments, String Job_Id,
            String loginuserid, DataSource ds) {
        JobDAO.addJobComments(comments, Job_Id, loginuserid, ds);
    }

    public static void addJobComments(String comments, String Job_Id,
            String loginuserid, DataSource ds) {
        AddComment.addcomment(null, null, Job_Id, null, null, "Job", comments,
                loginuserid, ds);
    }

    public static void writeNode(String name, String value, StringBuffer sb) {
        if (value == null || value.trim().equals("")) {
            value = "-";
        }
        sb.append("<" + name + ">" + value + "</" + name + ">\n");
    }

    public static String setLatestInstallNotes(
            JobDashboardBean jobDashboardForm, DataSource ds) {
        return setLatestInstallNotes(jobDashboardForm.getJobId(), ds,
                jobDashboardForm);
    }

    public static String setLatestComments(Map<String, Object> map,
            JobDashboardBean jobDashboardForm, DataSource ds) {
        return setLatestComments(jobDashboardForm.getJobId(), map, ds,
                jobDashboardForm);
    }

    public static String setLatestInstallNotes(String jobId, DataSource ds) {
        return setLatestInstallNotes(jobId, ds, null);
    }

    public static String getNextAction(String nActiveMonitorStateChangeID,
            String wugInstance, DataSource ds) {

        String jobIndicator = "0";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = ds.getConnection();
            String sql = "SELECT wug_cc_na_id FROM wug_outages_incidents_in_process WHERE "
                    + " wug_ip_nActiveMonitorStateChangeID = "
                    + nActiveMonitorStateChangeID
                    + " AND wug_ip_instance = "
                    + wugInstance;
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                jobIndicator = rs.getString("wug_cc_na_id");
            }
        } // - end of try
        catch (Exception e) {
            logger.error("getNextAction(String,String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getNextAction(String,String, DataSource) - Error occurred while retrieving job test indicator: "
                        + e);
            }
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }

        return jobIndicator;
    }

    public static String setLatestInstallNotes(String jobId, DataSource ds,
            JobDashboardBean jobDashboardForm) {
        StringBuffer sb = new StringBuffer();
        String latestInstallNotes = "-";
        String secondLatestInstallNotes = "-";
        String latestInstallNotesDate = "-";
        String secondLatestInstallNotesDate = "-";
        String latestInstallNotesType = "0";
        String secondLatestInstallNotesType = "0";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        java.util.Date dateIn = new java.util.Date();

        // ChangeDateFormat.getDateFormat(rsActual.getString("lx_se_act_start"),
        // "MM/dd/yyyy hh:mm aaa", "MM/dd/yyyy hh:mm:ss")
        try {
            conn = ds.getConnection();
            String sql = "select convert(varchar(10), lm_in_cdate, 1)+' '+convert(varchar(10),lm_in_cdate,108) as lm_in_udate,"
                    + " lm_in_notes, lm_in_type from lm_install_notes  where lm_in_js_id = "
                    + jobId
                    + " and  lm_in_id in "
                    + "(select top 2 lm_in_id from lm_install_notes where lm_in_js_id = "
                    + jobId + " ORDER BY lm_in_udate desc)" + "";

            if (logger.isDebugEnabled()) {
                logger.debug("setLatestInstallNotes(String, DataSource, JobDashboardBean) - install note sql: "
                        + sql);
            }
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                if (rs.isFirst()) {
                    latestInstallNotesDate = rs.getString("lm_in_udate") == null
                            || rs.getString("lm_in_udate").equals("") ? "-"
                            : ChangeDateFormat
                            .getDateFormat(rs.getString("lm_in_udate"),
                                    "MM/dd/yyyy hh:mm aaa",
                                    "MM/dd/yy HH:mm:ss");
                    latestInstallNotes = rs.getString("lm_in_notes") == null
                            || rs.getString("lm_in_notes").equals("") ? "-"
                            : rs.getString("lm_in_notes");
                    latestInstallNotesType = rs.getString("lm_in_type");
                }
                if (rs.isLast() && !rs.isFirst()) {
                    secondLatestInstallNotesDate = rs.getString("lm_in_udate") == null
                            || rs.getString("lm_in_udate").equals("") ? "-"
                            : ChangeDateFormat
                            .getDateFormat(rs.getString("lm_in_udate"),
                                    "MM/dd/yyyy hh:mm aaa",
                                    "MM/dd/yy HH:mm:ss");
                    secondLatestInstallNotes = rs.getString("lm_in_notes") == null
                            || rs.getString("lm_in_notes").equals("") ? "-"
                            : rs.getString("lm_in_notes");
                    secondLatestInstallNotesType = rs.getString("lm_in_type");
                }
            }

            if (jobDashboardForm != null) {
                jobDashboardForm.setLatestInstallNotes(Util
                        .replaceToBr(latestInstallNotes));
                jobDashboardForm
                        .setLatestInstallNotesDate(latestInstallNotesDate);
                jobDashboardForm
                        .setLatestInstallNotesType(latestInstallNotesType);
                jobDashboardForm
                        .setSecondLatestInstallNotesDate(secondLatestInstallNotesDate);
                jobDashboardForm.setSecondLatestInstallNotes(Util
                        .replaceToBr(secondLatestInstallNotes));
                jobDashboardForm
                        .setSecondLatestInstallNotesType(secondLatestInstallNotesType);

            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("setLatestInstallNotes(String, DataSource, JobDashboardBean) - Install Notes ----------------------------");
                }

				// TODO - now can replace XML manual writing to JAXB XML writing
                // Start-------------
				/* Code for convert object to XML using JAXB */
                // JobInstallationNote instrallationNotes = new
                // JobInstallationNote();
                // instrallationNotes
                // .setLatestInstallNotesDate(latestInstallNotesDate);
                // instrallationNotes.setLatestInstallNotes(latestInstallNotes);
                // instrallationNotes
                // .setSecondLatestInstallNotesDate(secondLatestInstallNotesDate);
                // instrallationNotes
                // .setSecondLatestInstallNotes(secondLatestInstallNotes);
                // Class<?>[] classes = new Class[] { JobInstallationNote.class
                // };
                // sb.append(XmlUtill.getXmlCode(instrallationNotes, classes));
                // -------------------------------------End
                sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                sb.append("<installNotesInfo generated=\"ulq\">\n");
                JobDAO.writeNode("latestInstallNotesDate",
                        latestInstallNotesDate, sb);
                JobDAO.writeNode("latestInstallNotes", Util.filterAjaxXML(Util
                        .replaceToBr(latestInstallNotes)), sb);
                JobDAO.writeNode("latestInstallNotesType",
                        latestInstallNotesType, sb);
                JobDAO.writeNode("secondLatestInstallNotesDate",
                        secondLatestInstallNotesDate, sb);
                JobDAO.writeNode("secondLatestInstallNotes", Util
                        .filterAjaxXML(Util
                                .replaceToBr(secondLatestInstallNotes)), sb);
                JobDAO.writeNode("secondLatestInstallNotesType",
                        secondLatestInstallNotesType, sb);
                if (logger.isDebugEnabled()) {
                    logger.debug("setLatestInstallNotes(String, DataSource, JobDashboardBean) - installNotesInfo generated xml: "
                            + sb.toString());
                }
            }

        } catch (Exception e) {
            logger.error(
                    "setLatestInstallNotes(String, DataSource, JobDashboardBean)",
                    e);

            logger.error(
                    "setLatestInstallNotes(String, DataSource, JobDashboardBean)",
                    e);
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);

        }
        Connection conn2 = null;
        Statement stmt2 = null;
        ResultSet rs2 = null;
        String sql2 = null;
        String installNotesCount = null;
        try {
            conn2 = ds.getConnection();

            sql2 = "select count(*) as installNotesCount from lm_install_notes where lm_in_js_id="
                    + jobId;
            stmt2 = conn2.createStatement();

            rs2 = stmt2.executeQuery(sql2);
            if (rs2.next()) {
                installNotesCount = rs2.getString("installNotesCount");
            }

        } catch (Exception e) {
            logger.error(
                    "sql2="
                    + sql2
                    + "setLatestInstallNotes(String, DataSource, JobDashboardForm)",
                    e);
        } finally {

            DBUtil.close(rs2, stmt2);
            DBUtil.close(conn2);
        }

        JobDAO.writeNode("installNotesCount", installNotesCount, sb);
        sb.append("</installNotesInfo>");

        java.util.Date dateOut = new java.util.Date();
        if (logger.isDebugEnabled()) {
            logger.debug("setLatestInstallNotes(String, DataSource, JobDashboardBean) - **** setLatestInstallNotes :: time taken :: "
                    + (dateOut.getTime() - dateIn.getTime()));
        }
        // request.setAttribute("latestInstallNotes",
        // Util.htmlFilter(Util.replaceToBr(latestInstallNotes)) );
        // request.setAttribute("secondLatestInstallNotes",
        // Util.htmlFilter(Util.replaceToBr(secondLatestInstallNotes)) );
        if (jobDashboardForm != null) {
            jobDashboardForm.setInstallNoteCount(installNotesCount);
        }

        return sb != null ? sb.toString() : "";
    }

    public static String setLatestComments(String jobId,
            Map<String, Object> map, DataSource ds,
            JobDashboardBean jobDashboardForm) {

        StringBuffer sb = new StringBuffer();
        String latestCommentsNotes = "";
        String latestCommentsDate = "";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        java.util.Date dateIn = new java.util.Date();

        try {
            conn = ds.getConnection();
            String sql = "select top 1 convert(varchar(10), lm_job_com_date, 1)+' '+convert(varchar(10),lm_job_com_date,108) as lm_comments_udate,lm_job_com_desc from lm_job_comment where lm_job_js_id = "
                    + jobId + " order by 1 desc";
            // System.out.println("Comments: "+sql);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                latestCommentsDate = rs.getString("lm_comments_udate") == null
                        || rs.getString("lm_comments_udate").equals("") ? "-"
                        : ChangeDateFormat.getDateFormat(
                                rs.getString("lm_comments_udate"),
                                "MM/dd/yyyy hh:mm aaa", "MM/dd/yy hh:mm:ss");
                latestCommentsNotes = rs.getString("lm_job_com_desc") == null
                        || rs.getString("lm_job_com_desc").equals("") ? "-"
                        : rs.getString("lm_job_com_desc");
            }
            if (jobDashboardForm != null) {
                jobDashboardForm.setLatestComments(Util
                        .replaceToBr(latestCommentsNotes));
                jobDashboardForm.setLatestCommentsDate(latestCommentsDate);
            } else {
                sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                sb.append("<CommentsInfo generated=\"ulq\">\n");
                JobDAO.writeNode("latestCommentsDate", latestCommentsDate, sb);
                JobDAO.writeNode("latestCommentsNotes", Util.filterAjaxXML(Util
                        .replaceToBr(latestCommentsNotes)), sb);
                sb.append("</CommentsInfo>");
                // System.out.println("installNotesInfo generated xml: "+
                // sb.toString());
            }
        } catch (Exception e) {
            logger.error(
                    "setLatestComments(String, DataSource, JobDashboardBean)",
                    e);

            logger.error(
                    "setLatestComments(String, DataSource, JobDashboardBean)",
                    e);
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);

        }
        java.util.Date dateOut = new java.util.Date();
        if (logger.isDebugEnabled()) {
            logger.debug("setLatestComments(String, DataSource, JobDashboardBean) - **** setLatestComments :: time taken :: "
                    + (dateOut.getTime() - dateIn.getTime()));
        }
        map.put("latestComments",
                Util.htmlFilter(Util.replaceToBr(latestCommentsNotes)));
        return sb != null ? sb.toString() : "";

    }

    public static String getJobTestIndicator(String jobId, DataSource ds) {
        String jobIndicator = "";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = ds.getConnection();
            String sql = "select lm_js_test_indicator = isnull(lm_js_test_indicator,'') from lm_job where lm_js_id ="
                    + jobId;
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                jobIndicator = rs.getString(1);
            }
        } // - end of try
        catch (Exception e) {
            logger.error("getJobTestIndicator(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getJobTestIndicator(String, DataSource) - Error occurred while retrieving job test indicator: "
                        + e);
            }
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }

        return jobIndicator;
    }

    public static String getPOCompletionCheck(String poId, DataSource ds) {
        String allowPOCompletion = "0";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = ds.getConnection();
            String sql = "select isExists = 1 from podb_purchase_order_deliverables po_del "
                    + "inner join lm_purchase_order po on po.lm_po_id = po_del.lm_po_id "
                    + "where po.lm_po_id ="
                    + poId
                    + " and podb_mn_dl_type <> 'Supporting' and "
                    + "lm_po_type in ('M','S','P') and (podb_mn_dl_status is null or podb_mn_dl_status <> 2)";

            // String poType = PODao.getPOTypeByPOId(poId, ds);
            // if (poType.equals("Material")) {
            // sql =
            // " select isExists = 1 from podb_purchase_order_deliverables po_del  "
            // +
            // " inner join lm_purchase_order po on po.lm_po_id = po_del.lm_po_id "
            // +
            // " LEFT JOIN lm_pwo_type_master  ON lm_pwo_type_id = lm_po_pwo_type_id "
            // +
            // " LEFT JOIN lm_po_status_types_master ON po_status_code = po_status "
            // + " WHERE lm_pwo_type_desc = 'Material' "
            // +
            // " AND po_del.podb_mn_dl_title NOT IN ('Before Work', 'After Work', 'Work Order', 'Equipment Cabinet/Rack') "
            // + " AND po.lm_po_id = "+
            // poId+"  and po_del.podb_mn_dl_type <> 'Supporting' "
            // + " and lm_po_type in ('M','S','P')  "
            // + " and (podb_mn_dl_status is null or podb_mn_dl_status <> 2) ";
            // }
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                if (rs.getString("isExists").equals("1")) {
                    allowPOCompletion = "1";
                }
            }
        } // - end of try
        catch (Exception e) {
            logger.error("getPOCompletionCheck(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getPOCompletionCheck(String, DataSource) - Error occurred while retrieving PO Completion check status: "
                        + e);
            }
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }

        return allowPOCompletion;
    }

    public static ArrayList getInvoiceActResourceList(String type,
            String jobId, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";
        ArrayList rsList = new ArrayList();
        try {
            conn = ds.getConnection();

            if (type != null && type.equals("job")) {
                sql = "SELECT lm_at_title AS NAME,lm_activity_type_description AS TYPE,lm_at_quantity AS quantity,lx_ce_type,lx_ce_unit_price AS price "
                        + " FROM dbo.lm_job "
                        + "INNER JOIN lm_activity ON lm_js_id = lm_at_js_id"
                        + " INNER JOIN  lx_cost_element ON lx_ce_used_in = lm_at_id"
                        + " inner join lm_activity_type on lm_at_type = lm_activity_type_code"
                        + " WHERE lm_js_id ="
                        + jobId
                        + " AND lx_ce_type in ('IA','A') and  lm_activity_type_description <> 'Overhead'";
            } else {
                sql = "SELECT iv_rp_name AS NAME ,lm_rs_type AS TYPE ,lm_rs_quantity AS quantity,lx_ce_type,"
                        + " lx_ce_total_price AS price,lm_rs_iv_cns_part_number,lm_rs_transit "
                        + " FROM dbo.lm_ticket"
                        + " INNER JOIN lm_activity ON lm_tc_js_id = lm_at_js_id "
                        + " INNER JOIN lm_resource ON lm_rs_at_id = lm_at_id  "
                        + " INNER JOIN iv_resource_pool ON iv_rp_id = lm_rs_rp_id "
                        + " INNER JOIN  lx_cost_element ON lx_ce_used_in = lm_rs_id "
                        + " WHERE lm_tc_js_id ="
                        + jobId
                        + " AND lx_ce_type in ('Il','L','IF','F','IM','M','IT','T')";
            }
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            String resType = "";
            while (rs.next()) {
                InvoiceJobDetailDTO form = new InvoiceJobDetailDTO();
                form.setRsName(rs.getString("NAME"));
                if (rs.getString("TYPE") != null
                        && (rs.getString("TYPE").equals("L") || rs.getString(
                                "TYPE").equals("IL"))) {
                    resType = "L";
                    form.setRsType(resType);
                } else if (rs.getString("TYPE") != null
                        && (rs.getString("TYPE").equals("M") || rs.getString(
                                "TYPE").equals("IM"))) {
                    resType = "M";
                    form.setRsType(resType);
                } else if (rs.getString("TYPE") != null
                        && (rs.getString("TYPE").equals("F") || rs.getString(
                                "TYPE").equals("IF"))) {
                    resType = "F";
                    form.setRsType(resType);
                } else if (rs.getString("TYPE") != null
                        && (rs.getString("TYPE").equals("T")
                        || rs.getString("TYPE").equals("IT") || rs
                        .getString("TYPE").contains("IT"))) {
                    resType = "T";
                    form.setRsType(resType);
                } else {
                    form.setRsType(rs.getString("TYPE"));
                }
                if (type != null && type.equals("ticket")) {
                    form.setCnsPartNo(rs.getString("lm_rs_iv_cns_part_number"));
                    form.setRsTransit(rs.getString("lm_rs_transit"));
                    if (rs.getString("lm_rs_transit") != null
                            && rs.getString("lm_rs_transit").equals("Y")) {
                        form.setRsName(rs.getString("NAME") + " - In Transit");
                        form.setRsType("T");
                    }
                }
                form.setRsquantity(rs.getString("quantity"));
                form.setRsPrice(rs.getString("price"));
                rsList.add(form);

            }

        } catch (Exception e) {
            logger.error("getInvoiceActResourceList(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }
        return rsList;

    }

    public static void setJobTicketInfo(InvoiceJobDetailDTO form, String jobId,
            DataSource ds) {
        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        try {
            conn = ds.getConnection();
            cstmt = conn.prepareCall("{?=call inv_job_ticket_info( ? )}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setInt(2, Integer.parseInt(jobId));

            rs = cstmt.executeQuery();

            while (rs.next()) {
                form.setSiteName(rs.getString("lm_si_name"));
                form.setSiteNumber(rs.getString("lm_si_number"));
                form.setSiteAddress(rs.getString("lm_si_address"));
                form.setSiteCity(rs.getString("lm_si_city"));
                form.setSiteState(rs.getString("lm_si_state"));
                form.setSiteCountry(rs.getString("lm_si_country"));
                form.setSiteZipCode(rs.getString("lm_si_zip_code"));
                form.setCustref(rs.getString("lm_js_customer_reference"));
                form.setTypeJob(rs.getString("job_type"));
                form.setRequestorName(rs.getString("lm_tc_requestor_name"));
                form.setRequestType(rs.getString("lm_request_type"));
                form.setProblemdesc(rs.getString("lm_tc_problem_desc"));
                form.setTimeOnSite(rs.getString("time_onsite"));
                form.setBillingInfo(rs.getString("billing_info"));
            }

        } catch (Exception e) {
            logger.error("setJobTicketInfo(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, cstmt);
            DBUtil.close(conn);
        }
    }

    public static ArrayList getScheduledDetail(String jobId, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";
        ArrayList rsList = new ArrayList();
        try {
            conn = ds.getConnection();
            /*
             * sql =
             * "SELECT CONVERT(VARCHAR(10), DATEDIFF(mi,lx_se_start,lx_se_end)/60)+':'+ CASE WHEN DATEDIFF(mi,lx_se_start,lx_se_end)%60 < 10"
             * +
             * " THEN '0'+CONVERT(VARCHAR(10),DATEDIFF(mi,lx_se_start,lx_se_end)%60)"
             * +
             * " ELSE CONVERT(VARCHAR(10),DATEDIFF(mi,lx_se_start,lx_se_end)%60) END AS onsite_time,"
             * +
             * " lx_se_start = replace(Convert(Varchar(25),isnull(convert(datetime ,lx_se_start),''),100),'Jan  1 1900 12:00AM','')  , "
             * +
             * " lx_se_end = replace(Convert(Varchar(25),isnull(convert(datetime ,lx_se_end),''),100),'Jan  1 1900 12:00AM','')  ,  "
             * +
             * " lx_se_sequence FROM lx_schedule_element WHERE lx_se_type IN ('J','IJ','AJ','CJ') and lx_se_type_id = "
             * +jobId;
             */
            sql = "SELECT 	CASE When  replace(convert(varchar(10),isnull(lx_se_planned_start,''),101),'01/01/1900','') = '' then '0:00'"
                    + " else CONVERT(VARCHAR(10), DATEDIFF(mi,lx_se_start,lx_se_end)/60)+':'+ CASE WHEN DATEDIFF(mi,lx_se_start,lx_se_end)%60 < 10 "
                    + " THEN '0'+ CONVERT(VARCHAR(10),DATEDIFF(mi,lx_se_start,lx_se_end)%60)"
                    + " ELSE CONVERT(VARCHAR(10),DATEDIFF(mi,lx_se_start,lx_se_end)%60) END "
                    + " end AS onsite_time, lx_se_start = replace(Convert(Varchar(25),isnull(convert(datetime ,lx_se_start),''),100),'Jan  1 1900 12:00AM',''),"
                    + " lx_se_end = replace(Convert(Varchar(25),isnull(convert(datetime ,lx_se_end),''),100),'Jan  1 1900 12:00AM',''), lx_se_sequence FROM lx_schedule_element "
                    + " WHERE lx_se_type_id = "
                    + jobId
                    + " and lx_se_type IN ('J','IJ','AJ','CJ')";

            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            // System.out.println(sql);
            while (rs.next()) {
                InvoiceJobDetailDTO form = new InvoiceJobDetailDTO();
                if ((rs.getString("lx_se_start") == null || rs.getString(
                        "lx_se_start").equals(""))
                        || (rs.getString("lx_se_end") == null || rs.getString(
                                "lx_se_end").equals(""))) {
                    form.setOnSite("0:00");
                } else {
                    form.setOnSite(rs.getString("onsite_time"));
                }

                if (rs.getString("lx_se_start") != null
                        && !rs.getString("lx_se_start").equals("")) {
                    form.setStartDate(ChangeDateFormat.getDateFormat(
                            rs.getString("lx_se_start"),
                            "MM/dd/yyyy hh:mm aaa", "MMM dd yyyy hh:mmaaa"));
                } else {
                    form.setStartDate("");
                }

                if (rs.getString("lx_se_end") != null
                        && !rs.getString("lx_se_end").equals("")) {
                    form.setStartEnd(ChangeDateFormat.getDateFormat(
                            rs.getString("lx_se_end"), "MM/dd/yyyy hh:mm aaa",
                            "MMM dd yyyy hh:mmaaa"));
                } else {
                    form.setStartEnd("");
                }

                form.setSequenceNo(rs.getString("lx_se_sequence"));
                rsList.add(form);
            }

        } catch (Exception e) {
            logger.error("getScheduledDetail(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }
        return rsList;

    }

    /**
     * Get invoice summary date for close tab.
     *
     * @param jobId
     * @param ds
     * @return
     */
    public static JobInvoiceSummary getInvoiceSummary(String jobId,
            DataSource ds) {

        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        JobInvoiceSummary invoiceSummary = new JobInvoiceSummary();

        try {
            conn = ds.getConnection();
            cstmt = conn.prepareCall("{?=call lm_job_invoice_summary(?)}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setString(2, jobId);
            rs = cstmt.executeQuery();

            while (rs.next()) {
                invoiceSummary.setInvNumber(rs.getString("inv_number"));
                invoiceSummary.setInvDate(rs.getString("inv_date"));
                invoiceSummary.setInvAge(rs.getString("inv_age"));
                invoiceSummary.setInvAmount(rs.getString("inv_amount"));
            }
            invoiceSummary.setInvDiscount("");

        } catch (Exception e) {
            logger.error("setJobTicketInfo(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, cstmt);
            DBUtil.close(conn);
        }
        return invoiceSummary;
    } // - getInvoiceSummary()

    /**
     * Gets the job owner name.
     *
     * @param jobId the job id
     * @param ds the ds
     *
     * @return the job owner name
     */
    public static String[] getJobOwnerName(String jobId, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String[] jobInfo = new String[2];
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            rs = stmt
                    .executeQuery("select * from lm_get_job_info where lm_js_id="
                            + jobId);
            if (rs.next()) {
                jobInfo[0] = rs.getString("owner_name");
                jobInfo[1] = rs.getString("owner_phone");
            }

        } catch (Exception e) {
            logger.error("getJobOwnerName(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }
        return jobInfo;
    }

    public static String getEmailId(String userId, DataSource ds) {
        String userMailId = "";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = ds.getConnection();
            String sql = "SELECT lo_pc_email1 FROM lo_poc WHERE lo_pc_id = "
                    + userId;
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                userMailId = rs.getString(1);
            }
        } // - end of try
        catch (Exception e) {
            logger.error("getJobTestIndicator(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getJobTestIndicator(String, DataSource) - Error occurred while retrieving job test indicator: "
                        + e);
            }
        } finally {
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
                logger.error("getEmailId(String, DataSource)", e);
            }

        }

        return userMailId;
    }

    public static String getAppendixType(String jobId, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String appendixType = null;
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            rs = stmt
                    .executeQuery("select lx_pr_type  from lm_job join lx_appendix_main on lm_js_pr_id = lx_pr_id where lm_js_id="
                            + jobId);
            if (rs.next()) {
                appendixType = rs.getString("lx_pr_type");
            }

        } catch (Exception e) {
            logger.error("getAppendixType(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }
        return appendixType;
    }
}
