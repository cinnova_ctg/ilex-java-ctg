package com.mind.newjobdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.pvs.Partner_SearchBean;
import com.mind.common.Util;
import com.mind.dao.PVS.Pvsdao;
import com.mind.fw.core.dao.util.DBUtil;

public class VendexDAO {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(VendexDAO.class);

	public static ArrayList getpartnerList(String jobId, String distance,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ArrayList valuelist = new ArrayList();
		DecimalFormat dfcur = new DecimalFormat("###0.0");
		int i = 0;
		String zipCode = "";
		try {
			conn = ds.getConnection();
			String sql = "select lm_si_zip_code from lm_job inner join lm_site_detail on lm_js_si_id = lm_si_id where lm_js_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobId);
			rs1 = pStmt.executeQuery();
			if (logger.isDebugEnabled()) {
				logger.debug("getpartnerList(String, String, DataSource) - Job Id ---------------"
						+ jobId);
			}
			if (rs1.next()) {
				zipCode = rs1.getString(1);
			}
			if (zipCode != null && !(zipCode.equals(""))) {

				cstmt = conn
						.prepareCall("{?=call cp_partner_search_list( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?)}");
				cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
				cstmt.setString(2, Util.singleToDoubleQuotes(""));
				// System.out.println("RSSS111  ");
				cstmt.setString(3, Util.singleToDoubleQuotes(""));
				cstmt.setString(4, "B");
				cstmt.setString(5, "0");
				// System.out.println("RSSS222  ");
				cstmt.setString(6, zipCode);
				// System.out.println("RSSS444  ");
				cstmt.setInt(7, Integer.parseInt(distance));
				// System.out.println("RSSS333  ");
				cstmt.setString(8, Util.singleToDoubleQuotes("Y"));
				cstmt.setString(9, Util.singleToDoubleQuotes("N"));
				cstmt.setString(10, Util.singleToDoubleQuotes("N"));
				cstmt.setString(11, "");
				cstmt.setString(12, Util.singleToDoubleQuotes("0"));
				cstmt.setString(13, "");
				cstmt.setString(14, Util.singleToDoubleQuotes(""));
				cstmt.setString(15, Util.singleToDoubleQuotes("Y"));
				cstmt.setString(16, "N");
				cstmt.setString(17, "N");
				cstmt.setString(18, ",distance asc");
				cstmt.setString(19, "true");

				rs = cstmt.executeQuery();

				while (rs.next()) {
					if (logger.isDebugEnabled()) {
						logger.debug("getpartnerList(String, String, DataSource) - "
								+ i);
					}
					Partner_SearchBean psForm = new Partner_SearchBean();
					psForm.setPid(rs.getString("cp_partner_id"));
					// System.out.println("in side result--------"+rs.getString(
					// "lo_om_division" ));
					psForm.setPartnername(rs.getString("lo_om_division"));
					psForm.setCity(rs.getString("lo_ad_city"));
					psForm.setState(rs.getString("lo_ad_state"));
					psForm.setZipcode(rs.getString("lo_ad_zip_code"));
					psForm.setCountry(rs.getString("lo_ad_country"));
					psForm.setPhone(rs.getString("lo_om_phone1"));
					psForm.setStatus(rs.getString("cp_pd_status"));
					psForm.setPartnerType(rs.getString("lo_ot_name"));
					psForm.setDistance(dfcur.format(rs.getFloat("distance"))
							+ "");
					psForm.setPartnerAddress1(rs.getString("lo_ad_address1"));
					psForm.setPartnerAddress2(rs.getString("lo_ad_address2"));
					psForm.setPartnerPriName(rs.getString("lo_pc_first_name")
							+ " " + rs.getString("lo_pc_last_name"));
					psForm.setPartnerPriPhone(rs.getString("lo_pc_phone1"));
					psForm.setPartnerPriCellPhone(rs
							.getString("lo_pc_cell_phone"));
					psForm.setPartnerPriEmail(rs.getString("lo_pc_email1"));
					psForm.setPartnerAvgRating(rs
							.getString("qc_ps_average_rating"));
					psForm.setPartnerLatitude(rs.getString("lo_ad_latitude"));
					psForm.setPartnerLongitude(rs.getString("lo_ad_longitude"));
					psForm.setIcontype(Pvsdao.setGMapIconForPartner(
							rs.getString("lo_ot_name"),
							rs.getString("qc_ps_average_rating"),
							rs.getString("cp_pd_status")));
					valuelist.add(i, psForm);
					i++;
				}
				System.out.println("");
			}

		} catch (Exception e) {
			logger.error("getpartnerList(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getpartnerList(String, String, DataSource) - Error occured getting lm_partner_detail: "
						+ e);
			}
			logger.error("getpartnerList(String, String, DataSource)", e);
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(pStmt);
			DBUtil.close(conn);

		}
		return valuelist;
	}

	public static ArrayList getCurrentSite(String jobId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		DecimalFormat dfcur = new DecimalFormat("###0.0");
		int i = 0;
		try {
			conn = ds.getConnection();
			String sql = "select lo_ot_name,"
					+ "lx_pr_title,"
					+ "lm_si_number,"
					+ "lm_si_address,"
					+ "lm_si_city,"
					+ "lm_si_state,"
					+ "lm_si_zip_code,"
					+ "lm_si_lat = isnull(lm_si_latitude,'0'),"
					+ "lm_si_lon = isnull(lm_si_longitude,'0'),"
					+ "sitecolor='Black' "
					+ "from lm_job inner join lx_appendix_main on lx_pr_id = lm_js_pr_id "
					+ "inner join lp_msa_main on lp_mm_id = lx_pr_mm_id "
					+ "inner join lo_organization_top on lo_ot_id = lp_mm_ot_id "
					+ "inner join lm_site_detail on lm_si_id = lm_js_si_id "
					+ "where lm_js_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobId);
			rs = pStmt.executeQuery();

			while (rs.next()) {
				Partner_SearchBean psForm = new Partner_SearchBean();
				psForm.setSiteNumber(rs.getString("lm_si_number"));
				psForm.setSiteAddress(rs.getString("lm_si_address"));
				psForm.setSiteCity(rs.getString("lm_si_city"));
				psForm.setSiteState(rs.getString("lm_si_state"));
				psForm.setZip(rs.getString("lm_si_zip_code"));
				psForm.setSiteLat(rs.getString("lm_si_lat"));
				psForm.setSiteLong(rs.getString("lm_si_lon"));
				psForm.setSiteColor(rs.getString("sitecolor"));
				psForm.setMsaCustomerName(rs.getString("lo_ot_name"));
				psForm.setAppendixName(rs.getString("lx_pr_title"));
				list.add(i, psForm);
				i++;
			}

		} catch (Exception e) {
			logger.error("getCurrentSite(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCurrentSite(String, DataSource) - Error occured getting lm_partner_detail: "
						+ e);
			}
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return list;
	}

	/**
	 * Method used to generate the xml of the Draft and Not assigned to any
	 * partner PO to be displayed in GMap Partner bubble.
	 * 
	 * @param String
	 *            Job Id
	 * @return String Xml generated from the Po result
	 * @see
	 **/

	public static String getJobPOnotAssignedtoPartner(String JobId,
			DataSource ds) {
		String returnXml = "";
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pStmt = null;
		try {
			conn = ds.getConnection();
			String sqlQuery = "select lm_po_number from lm_purchase_order where lm_po_js_id = ? and (lm_po_partner_id is  null and po_status = 0)";
			pStmt = conn.prepareStatement(sqlQuery);
			pStmt.setString(1, JobId);
			rs = pStmt.executeQuery();

			StringBuffer poXML = new StringBuffer();
			poXML.append("\'<?xml version = \"1.0\"?>");
			poXML.append("<pos>");
			while (rs.next()) {
				poXML.append("<po");
				poXML.append(" poId =\"" + rs.getString(1) + "\"");
				poXML.append(" jobId =\"" + JobId + "\"");
				poXML.append("/>");
			}
			poXML.append("</pos>\'");
			returnXml = poXML.toString();

		} catch (Exception e) {
			logger.error("getJobPOnotAssignedtoPartner(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobPOnotAssignedtoPartner(String, DataSource) - Error occured in VendexDAO.getJobPOnotAssignedtoPartner: "
						+ e);
			}
			logger.error("getJobPOnotAssignedtoPartner(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return returnXml;
	}

	/**
	 * Method used to generate the xml of the MSA Site List.
	 * 
	 * @param String
	 *            Job Id
	 * @return String Xml generated from the Po result
	 * @see
	 **/
	public static String getSiteListOfMSA(String msaId, String endCustomer,
			String zipCode, String radius, DataSource ds) {
		String returnXml = "";
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		try {
			conn = ds.getConnection();

			cstmt = conn.prepareCall("{?=call lp_rawview_site_list(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(msaId));
			cstmt.setString(3, endCustomer);
			cstmt.setString(4, zipCode);
			cstmt.setInt(5, Integer.parseInt(radius));

			// System.out.println("RSSS111  ");
			/*
			 * System.out.println("MSA Id = "+msaId);
			 * System.out.println("endCustomer = "+endCustomer);
			 * System.out.println("zipCode = "+zipCode);
			 * System.out.println("radius = "+radius);
			 */

			rs = cstmt.executeQuery();

			StringBuffer siteXML = new StringBuffer();
			siteXML.append("\'<?xml version = \"1.0\"?>");
			siteXML.append("<sites>");

			while (rs.next()) {
				siteXML.append("<site");
				siteXML.append(" lat =\"" + rs.getString(2) + "\"");
				siteXML.append(" lon =\"" + rs.getString(3) + "\"");
				siteXML.append(" siteNumber =\"" + Util.filter(rs.getString(4))
						+ "\"");
				siteXML.append(" siteAddress =\""
						+ Util.filter(rs.getString(5)) + "\"");
				siteXML.append(" siteCity =\"" + Util.filter(rs.getString(6))
						+ "," + "\"");
				siteXML.append(" siteState =\"" + Util.filter(rs.getString(7))
						+ "\"");
				siteXML.append(" siteZipCode =\""
						+ Util.filter(rs.getString(8)) + "\"");
				siteXML.append(" icontype =\"" + Util.filter("Blue") + "\"");
				siteXML.append(" msaName =\"" + Util.filter(rs.getString(9))
						+ "\"");
				siteXML.append(" appendixName =\"" + Util.filter("") + "\"");
				siteXML.append("/>");
			}
			siteXML.append("</sites>\'");
			returnXml = siteXML.toString();

		} catch (Exception e) {
			logger.error("getSiteListOfMSA(String, DataSource)", e);

		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return returnXml;
	}

	/**
	 * Method used to generate the partner List of a MSA's Site around 50 mile.
	 * 
	 * @param String
	 *            Job Id
	 * @return String Xml generated from the Po result
	 * @see
	 **/
	public static ArrayList getallSitePartnerList(String msaId,
			String endCustomer, String zipCode, String radius, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		DecimalFormat dfcur = new DecimalFormat("###0.0");
		int i = 0;
		try {
			conn = ds.getConnection();

			cstmt = conn.prepareCall("{?=call lp_site_raw_partner(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(msaId));
			cstmt.setString(3, endCustomer);
			cstmt.setString(4, zipCode);
			cstmt.setInt(5, Integer.parseInt(radius));

			// System.out.println("RSSS111  ");

			rs = cstmt.executeQuery();

			while (rs.next()) {
				if (logger.isDebugEnabled()) {
					logger.debug("getpartnerList(String, String, DataSource) - "
							+ i);
				}
				Partner_SearchBean psForm = new Partner_SearchBean();
				psForm.setPid(rs.getString("cp_partner_id"));
				// System.out.println("in side result--------"+rs.getString(
				// "lo_om_division" ));
				psForm.setPartnername(rs.getString("lo_om_division"));
				psForm.setCity(rs.getString("lo_ad_city"));
				psForm.setState(rs.getString("lo_ad_state"));
				psForm.setZipcode(rs.getString("lo_ad_zip_code"));
				psForm.setCountry(rs.getString("lo_ad_country"));
				psForm.setPhone(rs.getString("lo_om_phone1"));
				psForm.setStatus(rs.getString("cp_pd_status"));
				psForm.setPartnerType(rs.getString("lo_ot_name"));
				psForm.setDistance("");
				psForm.setPartnerAddress1(rs.getString("lo_ad_address1"));
				psForm.setPartnerAddress2(rs.getString("lo_ad_address2"));
				psForm.setPartnerPriName(rs.getString("lo_pc_first_name") + " "
						+ rs.getString("lo_pc_last_name"));
				psForm.setPartnerPriPhone(rs.getString("lo_pc_phone1"));
				psForm.setPartnerPriCellPhone(rs.getString("lo_pc_cell_phone"));
				psForm.setPartnerPriEmail(rs.getString("lo_pc_email1"));
				psForm.setPartnerAvgRating(rs.getString("qc_ps_average_rating"));
				psForm.setPartnerLatitude(rs.getString("lo_ad_latitude"));
				psForm.setPartnerLongitude(rs.getString("lo_ad_longitude"));
				psForm.setIcontype(Pvsdao.setGMapIconForPartner(
						rs.getString("lo_ot_name"),
						rs.getString("qc_ps_average_rating"),
						rs.getString("cp_pd_status")));
				valuelist.add(i, psForm);
				i++;
			}
			System.out.println("In side Partner List Call ----------" + i);

		} catch (Exception e) {
			logger.error("getallSitePartnerList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getallSitePartnerList(String, DataSource) - Error occured getting lm_partner_detail: "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return valuelist;
	}

	public static java.util.Collection getEndCustomerList(String msaId,
			DataSource ds) {

		ArrayList groupname = new java.util.ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		groupname.add(new com.mind.common.LabelValue("--Select--", " "));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			/*
			 * rs = stmt.executeQuery(
			 * "select msaname,lx_pr_end_customer,msaid from cc_get_msa_list_01 inner join lx_appendix_main on msaid = lx_pr_mm_id where lx_pr_end_customer is not null and msaid ="
			 * +msaId +" group by lx_pr_end_customer,msaname,msaid");
			 */
			rs = stmt
					.executeQuery("select *   from lp_msa_end_customer where lp_msa_end_customer <> 'null' and lp_msa_id = "
							+ msaId);
			while (rs.next()) {
				groupname.add(new com.mind.common.LabelValue(rs
						.getString("lp_msa_end_customer"), rs
						.getString("lp_msa_end_customer")));
			}
		} catch (Exception e) {
			logger.error("getEndCustomerList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEndCustomerList(String, DataSource) - Exception caught in rerieving End Customer Name "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return groupname;

	}

	public static java.util.Collection getRadius() {
		ArrayList valuelist = new java.util.ArrayList();
		valuelist.add(new com.mind.common.LabelValue("25", "25"));
		valuelist.add(new com.mind.common.LabelValue("50", "50"));
		valuelist.add(new com.mind.common.LabelValue("100", "100"));
		valuelist.add(new com.mind.common.LabelValue("150", "150"));
		return valuelist;
	}

}
