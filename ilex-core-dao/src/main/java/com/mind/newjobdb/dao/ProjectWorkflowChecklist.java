package com.mind.newjobdb.dao;

import java.util.ArrayList;

import javax.sql.DataSource;

import com.mind.bean.newjobdb.ProjectWorkflowItem;



/**
 * @purpose This class is used for the representation of 
 * 			the data access for the Job Level Workflow Checklist.
 */
public class ProjectWorkflowChecklist {
	private ArrayList<ProjectWorkflowItem> projectWorkflowItems;
	public static void copyToJob(DataSource ds,String appendixId,String jobId,String userId) {
		ProjectLevelWorkflowDAO.copyToJob(ds, appendixId, jobId, userId);
	}
	public ArrayList<ProjectWorkflowItem> getProjectWorkflowItems() {
		return projectWorkflowItems;
	}
	public void setProjectWorkflowItems(
			ArrayList<ProjectWorkflowItem> projectWorkflowItems) {
		this.projectWorkflowItems = projectWorkflowItems;
	}
}
