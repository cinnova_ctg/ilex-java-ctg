package com.mind.newjobdb.dao;

import java.io.Serializable;
import java.util.ArrayList;

import javax.sql.DataSource;

import com.mind.bean.mpo.PODTO;
import com.mind.bean.newjobdb.GeneralPOSetup;
import com.mind.dao.PRM.POWODetaildao;

public class POGeneralInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String poNumber = "";
	private String poType = "";
	private String masterPOItem = "";
	private String masterPOItemDesc = "";
	private String terms = "";
	private String termsDesc = "";
	private String revisionLevel = "";
	private GeneralPOSetup generalPOSetup = null;
	private String partner = "";
	private String partnerId = "";
	private String contact = "";
	private String contactPoc = "";
	private int poStatus = 0;
	private String poStatusName;
	private String authorizedPaymentMethod = "";
	private String parentTabType = "";
	private int pvsJustification;
	private int geographicConstraint;
	private String[] cmboxCoreCompetency;;

	public static ArrayList<String> getPOTypeList(DataSource ds) {
		ArrayList pWOdetailtype = POWODetaildao.getPWOdetailtype(ds);
		return pWOdetailtype;
	}

	public static ArrayList<String> getTermsList(DataSource ds) {
		ArrayList pWOdetailterms = POWODetaildao.getPWOdetailterms(ds);
		return pWOdetailterms;
	}

	public static ArrayList<String> getMasterPOItemList(DataSource ds) {
		ArrayList pWOdetailmasterpotype = POWODetaildao
				.getPWOdetailmasterpotype(ds);
		return pWOdetailmasterpotype;
	}

	public static ArrayList<String> getContactList(PODTO bean, DataSource ds) {
		return null;
	}

	public String getAuthorizedPaymentMethod() {
		return authorizedPaymentMethod;
	}

	public void setAuthorizedPaymentMethod(String authorizedPaymentMethod) {
		this.authorizedPaymentMethod = authorizedPaymentMethod;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public GeneralPOSetup getGeneralPOSetup() {
		return generalPOSetup;
	}

	public void setGeneralPOSetup(GeneralPOSetup generalPOSetup) {
		this.generalPOSetup = generalPOSetup;
	}

	public String getMasterPOItem() {
		return masterPOItem;
	}

	public void setMasterPOItem(String masterPOItem) {
		this.masterPOItem = masterPOItem;
	}

	public String getParentTabType() {
		return parentTabType;
	}

	public void setParentTabType(String parentTabType) {
		this.parentTabType = parentTabType;
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public int getPoStatus() {
		return poStatus;
	}

	public void setPoStatus(int poStatus) {
		this.poStatus = poStatus;
	}

	public String getPoType() {
		return poType;
	}

	public void setPoType(String poType) {
		this.poType = poType;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getContactPoc() {
		return contactPoc;
	}

	public void setContactPoc(String contactPoc) {
		this.contactPoc = contactPoc;
	}

	public String getPoStatusName() {
		return poStatusName;
	}

	public void setPoStatusName(String poStatusName) {
		this.poStatusName = poStatusName;
	}

	public String getMasterPOItemDesc() {
		return masterPOItemDesc;
	}

	public void setMasterPOItemDesc(String masterPOItemDesc) {
		this.masterPOItemDesc = masterPOItemDesc;
	}

	public String getTermsDesc() {
		return termsDesc;
	}

	public void setTermsDesc(String termsDesc) {
		this.termsDesc = termsDesc;
	}

	public String getRevisionLevel() {
		return revisionLevel;
	}

	public void setRevisionLevel(String revisionLevel) {
		this.revisionLevel = revisionLevel;
	}

	public int getPvsJustification() {
		return pvsJustification;
	}

	public void setPvsJustification(int pvsJustification) {
		this.pvsJustification = pvsJustification;
	}

	public int getGeographicConstraint() {
		return geographicConstraint;
	}

	public void setGeographicConstraint(int geographicConstraint) {
		this.geographicConstraint = geographicConstraint;
	}

	public String[] getCmboxCoreCompetency() {
		return cmboxCoreCompetency;
	}

	public void setCmboxCoreCompetency(String[] cmboxCoreCompetency) {
		this.cmboxCoreCompetency = cmboxCoreCompetency;
	}

}
