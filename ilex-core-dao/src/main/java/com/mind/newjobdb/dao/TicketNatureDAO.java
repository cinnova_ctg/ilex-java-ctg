package com.mind.newjobdb.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.LabelValue;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * @purpose This class is used to interact with the the TICKET Nature info
 *          related database tables.
 */
public class TicketNatureDAO {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(TicketNatureDAO.class);

	/**
	 * Discription: This method return list of Problem Category.
	 * 
	 * @param ds
	 *            -- DataBase Object
	 * @return List of Problem Category.
	 */
	public static ArrayList<LabelValue> getProblemCategories(DataSource ds) {
		ArrayList problemcategory = new java.util.ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		problemcategory.add(new com.mind.common.LabelValue("Select", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select * from cc_problem_category_detail where cc_pc_status  = 'A' order by cc_pc_desc");
			while (rs.next()) {
				problemcategory.add(new LabelValue(rs.getString(2), rs
						.getString(1)));
			}
		} catch (Exception e) {
			logger.error("getProblemCategories(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getProblemCategories(DataSource) - Exception in com.mind.newjobdb.dao.getProblemCategory()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return problemcategory;
	} // - end of getProblemCategory() method

	/**
	 * Discription: This method return list of Next Actions String.
	 * 
	 * @param ds
	 *            -- DataBase Object
	 * @return List of next actions.
	 */
	public static ArrayList<LabelValue> getNextActionList(DataSource ds) {
		ArrayList<LabelValue> nextActionList = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		nextActionList.add(new com.mind.common.LabelValue("Select", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select * from cc_next_action_details order by cc_na_desc");
			while (rs.next()) {
				nextActionList.add(new LabelValue(rs.getString(2), rs
						.getString(1)));
			}
		} catch (Exception e) {
			logger.error("getNextActionList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNextActionList(DataSource) - Exception in com.mind.newjobdb.dao.getNextActionList()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return nextActionList;
	} // - end of getNextActionList() method

	public void findSupportingDocs() {

	}
}
