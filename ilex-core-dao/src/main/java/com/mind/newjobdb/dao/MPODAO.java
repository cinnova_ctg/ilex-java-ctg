package com.mind.newjobdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.newjobdb.GeneralPOSetup;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.MPOList;
import com.mind.bean.newjobdb.MasterPurchaseOrder;
import com.mind.bean.newjobdb.POActivityManager;
import com.mind.bean.newjobdb.PODeliverable;
import com.mind.bean.newjobdb.PODocument;
import com.mind.bean.newjobdb.POResource;
import com.mind.bean.newjobdb.RequiredEquipment;
import com.mind.fw.core.dao.util.DBUtil;

public class MPODAO {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MPODAO.class);

	public static ArrayList<RequiredEquipment> getRequiredEquipmentFromMPO(
			String mpoId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList<RequiredEquipment> listOfRequiredEquipment = new ArrayList<RequiredEquipment>();
		try {
			conn = ds.getConnection();
			String sql = "select mp_tools_id from mp_work_order_tools  where mp_mn_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, mpoId);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				RequiredEquipment requiredEquipment = new RequiredEquipment();
				requiredEquipment.setId(rs.getString("mp_tools_id"));
				listOfRequiredEquipment.add(requiredEquipment);
			}
		} catch (Exception e) {
			logger.error("getRequiredEquipmentFromMPO(String, DataSource)", e);

			logger.error("getRequiredEquipmentFromMPO(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return listOfRequiredEquipment;

	}

	public static GeneralPOSetup getCopyToBeMPODates(String jobId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		GeneralPOSetup generalPOSetup = null;
		try {
			conn = ds.getConnection();
			String sql = "select  isnull(replace(convert(varchar(10),getdate(),101)+''+substring(convert(varchar(30),getdate(),109),12,15),'01/01/1900 12:00:00:000AM',''),'') as currentDate"
					+ " \n ,isnull(replace(convert(varchar(10),lm_js_planned_start_date,101)+''+substring(convert(varchar(30),lm_js_planned_start_date,109),12,15),'01/01/1900 12:00:00:000AM',''),'') as lm_js_planned_start_date from lm_job where lm_js_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				generalPOSetup = new GeneralPOSetup();
				generalPOSetup.setDeliverByDate(IlexTimestamp
						.converToTimestamp(rs
								.getString("lm_js_planned_start_date")));
				generalPOSetup.setIssueDate(IlexTimestamp.converToTimestamp(rs
						.getString("currentDate")));
			}
		} catch (Exception e) {
			logger.error("getCopyToBeMPODates(String, DataSource)", e);

			logger.error("getCopyToBeMPODates(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return generalPOSetup;
	}

	public static ArrayList<POResource> getResourcesFromMPO(String mpoId,
			String jobId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<POResource> listOfPOResource = new ArrayList<POResource>();
		try {
			conn = ds.getConnection();
			String sql = "select *	 from func_lm_powo_resource_from_mpo('"
					+ jobId + "' ,'" + mpoId + "' )";
			if (logger.isDebugEnabled()) {
				logger.debug("getResourcesFromMPO(String, String, DataSource) - getResourcesFromMPO sql = "
						+ sql);
			}
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				POResource pOResource = new POResource();
				pOResource.setActivityId(rs.getString("lm_at_id"));
				pOResource.setResourceId(rs.getString("lm_rs_id"));
				pOResource.setActivityName(rs.getString("lm_at_title"));
				pOResource.setResourceName(rs
						.getString("mp_ar_resource_classification"));
				pOResource.setActivityQuantity(Float.parseFloat(rs
						.getString("mp_ar_at_estimated_qty")));
				pOResource.setResourceQuantity(Float.parseFloat(rs
						.getString("mp_ar_rs_estimated_qty")));
				pOResource.setEstimatedCostUnit(Float.parseFloat(rs
						.getString("mp_ar_estimated_unit_cost")));
				pOResource.setEstimatedCostTotal(pOResource
						.getActivityQuantity()
						* pOResource.getEstimatedCostUnit());
				pOResource.setAsPlannedQuantity(Float.parseFloat(rs
						.getString("mp_ar_planed_qty")));
				pOResource.setAsPlannedUnitCost(Float.parseFloat(rs
						.getString("mp_ar_planed_unit_cost")));
				pOResource.setAsplannedSubTotal(pOResource
						.getAsPlannedQuantity()
						* pOResource.getAsPlannedUnitCost());
				pOResource.setShowOnWO(TicketDAO.getBoolean(rs
						.getString("mp_ar_show_on_work_order")));
				listOfPOResource.add(pOResource);
			}
		} catch (Exception e) {
			logger.error("getResourcesFromMPO(String, String, DataSource)", e);

			logger.error("getResourcesFromMPO(String, String, DataSource)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return listOfPOResource;

	}

	public static String getPoSpecialConditionsFromMPO(MasterPurchaseOrder mpo) {
		return null;
	}

	public static String getWoNatureOfActivityFromMPO(MasterPurchaseOrder mpo) {
		return null;
	}

	public static String getWoSpecialInstructionsFromMPO(MasterPurchaseOrder mpo) {
		return null;
	}

	public static ArrayList<PODeliverable> getPODeliverablesFromMPO(
			String mpoId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList<PODeliverable> listOfPODeliverables = new ArrayList<PODeliverable>();
		try {
			conn = ds.getConnection();
			String sql = "select mp_mn_dl_id,mp_mn_dl_title, mp_mn_dl_type, mp_mn_dl_format,mp_mn_dl_desc from mp_purchase_order_deliverables  where mp_mn_id =  ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, mpoId);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				PODeliverable pODeliverable = new PODeliverable();
				pODeliverable.setDevId(rs.getString("mp_mn_dl_id"));
				pODeliverable.setTitle(rs.getString("mp_mn_dl_title"));
				pODeliverable.setType(rs.getString("mp_mn_dl_type"));
				pODeliverable.setFormat(rs.getString("mp_mn_dl_format"));
				pODeliverable.setDescription(rs.getString("mp_mn_dl_desc"));
				listOfPODeliverables.add(pODeliverable);
			}
		} catch (Exception e) {
			logger.error("getPODeliverablesFromMPO(String, DataSource)", e);

			logger.error("getPODeliverablesFromMPO(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return listOfPODeliverables;

	}

	public static POActivityManager getPOActivityManagerFromMPO(String mpoId,
			String jobId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		POActivityManager pOActivityManager = new POActivityManager();
		pOActivityManager.setAuthorizedTotalCost(0.0F);
		pOActivityManager.setPoResources(MPOCopier.getResourcesFromMPO(mpoId,
				jobId, ds));
		try {
			conn = ds.getConnection();
			String sql = "select mp_mn_estimated_cost from mp_main where mp_mn_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, mpoId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				pOActivityManager.setAuthorizedTotalCost(Float.parseFloat(rs
						.getString("mp_mn_estimated_cost") == null ? "0.0" : rs
						.getString("mp_mn_estimated_cost")));
				pOActivityManager.setMpoTotalCost(new Float(rs
						.getString("mp_mn_estimated_cost")));
			}
		} catch (Exception e) {
			logger.error(
					"getPOActivityManagerFromMPO(String, String, DataSource)",
					e);

			logger.error(
					"getPOActivityManagerFromMPO(String, String, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
			;
		}
		return pOActivityManager;
	}

	public static POGeneralInfo getPoGeneralInfoFromMPO(String mpoId,
			IlexTimestamp deliverByDate, IlexTimestamp issueDate, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		POGeneralInfo pOGeneralInfo = new POGeneralInfo();
		GeneralPOSetup generalPOSetup = new GeneralPOSetup();
		generalPOSetup.setIssueDate(issueDate);
		generalPOSetup.setDeliverByDate(deliverByDate);
		pOGeneralInfo.setGeneralPOSetup(generalPOSetup);
		try {
			conn = ds.getConnection();
			String sql = "select mp_mn_master_po_item = isnull(mp_mn_master_po_item,'') from mp_main where mp_mn_id = ?";
			if (logger.isDebugEnabled()) {
				logger.debug("getPoGeneralInfoFromMPO(String, IlexTimestamp, IlexTimestamp, DataSource) - POGeneralInfo sql = select mp_mn_master_po_item from mp_main where mp_mn_id = "
						+ mpoId);
			}
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, mpoId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				pOGeneralInfo.setMasterPOItem(getMasterPOItemId(rs
						.getString("mp_mn_master_po_item")));
			}
		} catch (Exception e) {
			logger.error(
					"getPoGeneralInfoFromMPO(String, IlexTimestamp, IlexTimestamp, DataSource)",
					e);

			logger.error(
					"getPoGeneralInfoFromMPO(String, IlexTimestamp, IlexTimestamp, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return pOGeneralInfo;
	}

	/**
	 * @param masterPOItemName
	 * @return
	 */
	private static String getMasterPOItemId(String masterPOItemName) {
		String masterPOItemId = null;
		if (masterPOItemName.equalsIgnoreCase("fp")) {
			masterPOItemId = "2";
		} else if (masterPOItemName.equalsIgnoreCase("ht")) {
			masterPOItemId = "1";
		} else if (masterPOItemName.equalsIgnoreCase("mt")) {
			masterPOItemId = "3";
		} else if (masterPOItemName.equalsIgnoreCase("si")) {
			masterPOItemId = "4";
		}
		return masterPOItemId;
	}

	public static ArrayList<PODocument> getPODocumentsFromMPO(String mpoId,
			DataSource ds) {

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList<PODocument> listOfPODocs = new ArrayList<PODocument>();
		try {
			conn = ds.getConnection();
			String sql = "select * from mp_document  where mp_mn_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, mpoId);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				PODocument pODocument = new PODocument();
				pODocument.setDocId(rs.getString("mp_doc_id"));
				pODocument.setDate(rs.getString("mp_doc_created_date"));
				String isIncludedWithPO = rs.getString("doc_with_po");
				if (isIncludedWithPO.trim().equals("1")) {
					pODocument.setIncludedWithPO(true);
				} else {
					pODocument.setIncludedWithPO(false);
				}
				String isIncludedWithWO = rs.getString("doc_with_wo");
				if (isIncludedWithWO.trim().equals("1")) {
					pODocument.setIncludedWithWO(true);
				} else {
					pODocument.setIncludedWithWO(false);
				}
				listOfPODocs.add(pODocument);
			}
		} catch (Exception e) {
			logger.error("getPODocumentsFromMPO(String, DataSource)", e);

			logger.error("getPODocumentsFromMPO(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return listOfPODocs;
	}

	public static String getPoSpecialConditionsFromMPO(String mpoId,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String specialConditions = null;
		try {
			conn = ds.getConnection();
			String sql = "select condition from mp_purchase_order_detail where mp_mn_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, mpoId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				specialConditions = rs.getString("condition");
			}
		} catch (Exception e) {
			logger.error("getPoSpecialConditionsFromMPO(String, DataSource)", e);

			logger.error("getPoSpecialConditionsFromMPO(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return specialConditions;

	}

	public static PoWoInfo setWorkOrderDetailFromMPO(String mpoId,
			PoWoInfo pWoInfo, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select mp_wo_nt_of_act,mp_wo_si from mp_work_order_detail where mp_mn_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, mpoId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				pWoInfo.setWoNatureOfActivity(rs.getString("mp_wo_nt_of_act"));
				pWoInfo.setWoSpecialInstructions(rs.getString("mp_wo_si"));
			}
		} catch (Exception e) {
			logger.error(
					"setWorkOrderDetailFromMPO(String, PoWoInfo, DataSource)",
					e);

			logger.error(
					"setWorkOrderDetailFromMPO(String, PoWoInfo, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return pWoInfo;

	}

	public static MPOList getListOfMPO(String appendixId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		MPOList mpoList = new MPOList();
		ArrayList<MasterPurchaseOrder> listOfMPOS = new ArrayList<MasterPurchaseOrder>();
		try {
			conn = ds.getConnection();
			String sql = "select * from mp_main where mp_appendix_id = ? order by mp_mn_name";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, appendixId);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				MasterPurchaseOrder masterPurchaseOrder = new MasterPurchaseOrder();
				masterPurchaseOrder.setMpoId(rs.getString("mp_mn_id"));
				masterPurchaseOrder.setMasterPOItem(rs
						.getString("mp_mn_master_po_item"));
				masterPurchaseOrder.setMpoName(rs.getString("mp_mn_name"));
				masterPurchaseOrder.setEstimatedTotalCost(rs
						.getFloat("mp_mn_estimated_cost"));
				masterPurchaseOrder.setStatus(rs.getString("mp_mn_status"));
				if (masterPurchaseOrder.isSelectableForCopyToPO())
					masterPurchaseOrder.setSelectable(true);
				else
					masterPurchaseOrder.setSelectable(false);
				listOfMPOS.add(masterPurchaseOrder);
			}
		} catch (Exception e) {
			logger.error("getListOfMPO(String, DataSource)", e);

			logger.error("getListOfMPO(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		mpoList.setMasterPurchaseOrders(listOfMPOS);
		return mpoList;
	}

	public static String isValidForCopyToPO(String mpoId, String jobId,
			DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String flag = "";
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lm_mpo_to_po_check(?,?)}"); // -
																			// 14
																			// Parameter
																			// &
																			// one
																			// out
																			// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, jobId);
			cstmt.setString(3, mpoId);
			rs = cstmt.executeQuery();

			if (rs.next()) {
				flag = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("isValidForCopyToPO(String, String, DataSource)", e);

			logger.error("isValidForCopyToPO(String, String, DataSource)", e);
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("isValidForCopyToPO(String, String, DataSource) - flag = "
					+ flag);
		}
		return flag.trim();
	}

	public static MasterPurchaseOrder getMPO(String pmoId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		MasterPurchaseOrder masterPurchaseOrder = new MasterPurchaseOrder();
		try {
			conn = ds.getConnection();
			String sql = "select * from mp_main where mp_mn_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, pmoId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				masterPurchaseOrder.setMpoId(rs.getString("mp_mn_id"));
				masterPurchaseOrder.setMasterPOItem(rs
						.getString("mp_mn_master_po_item"));
				masterPurchaseOrder.setMpoName(rs.getString("mp_mn_name"));
				masterPurchaseOrder.setEstimatedTotalCost(rs
						.getFloat("mp_mn_estimated_cost"));
				masterPurchaseOrder.setStatus(rs.getString("mp_mn_status"));
				if (masterPurchaseOrder.isSelectableForCopyToPO())
					masterPurchaseOrder.setSelectable(true);
				else
					masterPurchaseOrder.setSelectable(false);
			}
		} catch (Exception e) {
			logger.error("getMPO(String, DataSource)", e);

			logger.error("getMPO(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return masterPurchaseOrder;
	}

}