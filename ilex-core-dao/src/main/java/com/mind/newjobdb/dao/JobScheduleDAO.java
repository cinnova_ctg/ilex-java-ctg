package com.mind.newjobdb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.DispatchScheduleList;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.fw.core.dao.util.DBUtil;

public class JobScheduleDAO {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JobScheduleDAO.class);

	public static void setScheduleDates(JobDashboardBean bean, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String actualStart = "";
		String actualEnd = "";
		String scheduleStart = "";
		String scheduleEnd = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			// String
			// sql="select * from lx_schedule_list_01 where lx_se_type_id="+typeid+" AND lx_se_type='"+type+"'";
			String sql = "select * from lx_schedule_list_01 where lx_se_type_id="
					+ bean.getJobId();
			String sql2 = "select start_date = replace(convert(varchar(10),(isnull( lm_js_planned_start_date ,'')),101)+''+ substring(convert(varchar(30),isnull(lm_js_planned_start_date,''),109),12,15),'01/01/1900 12:00:00:000AM',''),"
					+ "end_date = replace(convert(varchar(10),(isnull( lm_js_planned_end_date ,'')),101)+''+ substring(convert(varchar(30),isnull(lm_js_planned_end_date,''),109),12,15),'01/01/1900 12:00:00:000AM','') "
					+ "from lm_job where lm_js_id=" + bean.getJobId();
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				if (rs.getString("lx_se_type_Id") != null) {
					bean.setScheduleid(rs.getString("lx_se_Id"));
				}
				actualStart = rs.getString("lx_se_act_start");
				actualEnd = rs.getString("lx_se_act_end");
				scheduleStart = rs.getString("lx_se_start");
				scheduleEnd = rs.getString("lx_se_end");

			} else {
				if (logger.isDebugEnabled()) {
					logger.debug("setScheduleDates(JobDashboardForm, DataSource) - sql2= "
							+ sql2);
				}
				rs = stmt.executeQuery(sql2);
				if (rs.next()) {
					scheduleStart = rs.getString("start_date");
					scheduleEnd = rs.getString("end_date");
				}

			}

			IlexTimestamp onsite = IlexTimestamp.converToTimestamp(actualStart);
			IlexTimestamp offsite = IlexTimestamp.converToTimestamp(actualEnd);
			IlexTimestamp start = IlexTimestamp
					.converToTimestamp(scheduleStart);
			IlexTimestamp end = IlexTimestamp.converToTimestamp(scheduleEnd);
			bean.setOnsiteDate((onsite.getDate().equalsIgnoreCase("01/01/1900")) ? ""
					: onsite.getDate());
			bean.setOnsiteHour(onsite.getHour());
			bean.setOnsiteMinute(onsite.getMinute());
			bean.setOnsiteIsAm(onsite.isAM() == true ? "1" : "0");

			bean.setOffsiteDate((offsite.getDate()
					.equalsIgnoreCase("01/01/1900")) ? "" : offsite.getDate());
			bean.setOffsiteHour(offsite.getHour());
			bean.setOffsiteMinute(offsite.getMinute());
			bean.setOffsiteIsAm(offsite.isAM() == true ? "1" : "0");

			bean.setScheduleStartDate((start.getDate()
					.equalsIgnoreCase("01/01/1900")) ? "" : start.getDate());
			bean.setScheduleStartHour(start.getHour());
			bean.setScheduleStartMinute(start.getMinute());
			bean.setScheduleStartIsAm(start.isAM() == true ? "1" : "0");

			bean.setScheduleEndDate((end.getDate()
					.equalsIgnoreCase("01/01/1900")) ? "" : end.getDate());
			bean.setScheduleEndHour(end.getHour());
			bean.setScheduleEndMinute(end.getMinute());
			bean.setScheduleEndIsAm(end.isAM() == true ? "1" : "0");

			if (logger.isDebugEnabled()) {
				logger.debug("setScheduleDates(JobDashboardForm, DataSource) - actstart1111111= "
						+ bean.getOnsiteDate()
						+ " "
						+ bean.getOnsiteHour()
						+ ":"
						+ bean.getOnsiteMinute()
						+ " "
						+ bean.getOnsiteIsAm());
			}
			if (logger.isDebugEnabled()) {
				logger.debug("setScheduleDates(JobDashboardForm, DataSource) - actend1111111= "
						+ bean.getOffsiteDate()
						+ " "
						+ bean.getOffsiteHour()
						+ ":"
						+ bean.getOffsiteMinute()
						+ " "
						+ bean.getOffsiteIsAm());
			}
			if (logger.isDebugEnabled()) {
				logger.debug("setScheduleDates(JobDashboardForm, DataSource) - ScheduleStartt1111111= "
						+ bean.getScheduleStartDate()
						+ " "
						+ bean.getScheduleStartHour()
						+ ":"
						+ bean.getScheduleStartMinute()
						+ " "
						+ bean.getScheduleStartIsAm());
			}
			if (logger.isDebugEnabled()) {
				logger.debug("setScheduleDates(JobDashboardForm, DataSource) - ScheduleEnd1111111= "
						+ bean.getScheduleEndDate()
						+ " "
						+ bean.getScheduleEndHour()
						+ ":"
						+ bean.getScheduleEndMinute()
						+ " "
						+ bean.getScheduleEndIsAm());
			}
		} catch (Exception e) {
			logger.error("setScheduleDates(JobDashboardForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setScheduleDates(JobDashboardForm, DataSource) - Error occured during getting schedule  "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
	}

	/**
	 * This method return list all Dispatch Schedules dates for a Job.
	 * 
	 * @param jobId
	 *            -- Job Id
	 * @param type
	 *            -- Job Type
	 * @param ds
	 *            -- DataSource Object
	 * @return ArrayList -- List of Dispatch Dates
	 */
	public static ArrayList getJobDispatchScheduleList(String jobId,
			String type, DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		int i = 0;
		String scheduleId = null;
		String scheduleNumber = null;
		StringBuffer sql = new StringBuffer();

		try {
			conn = ds.getConnection();
			sql.append("select lx_se_id, "
					+ "lx_se_planned_start, "
					+ "lx_se_planned_end, "
					+ "lx_se_start, "
					+ "lx_se_end "
					+ "from lx_dispatch_schedule_list_01 "
					+ "where lx_se_type_id = ? and lx_se_type in ('J','IJ')  and (lx_se_start <> '' or lx_se_end <> '' or lx_se_planned_start <> '' or lx_se_planned_end <> '') order by lx_se_id");
			pStmt = conn.prepareStatement(sql.toString());
			pStmt.setString(1, jobId);
			// pStmt.setString(2,type);
			rs = pStmt.executeQuery();

			while (rs.next()) {
				IlexTimestamp plannedStartStamp = new IlexTimestamp();
				IlexTimestamp plannedEndStamp = new IlexTimestamp();
				IlexTimestamp actualStartStamp = new IlexTimestamp();
				IlexTimestamp actualEndStamp = new IlexTimestamp();
				plannedStartStamp = IlexTimestamp.converToTimestamp(rs
						.getString("lx_se_planned_start"));
				plannedEndStamp = IlexTimestamp.converToTimestamp(rs
						.getString("lx_se_planned_end"));
				actualStartStamp = IlexTimestamp.converToTimestamp(rs
						.getString("lx_se_start"));
				actualEndStamp = IlexTimestamp.converToTimestamp(rs
						.getString("lx_se_end"));
				scheduleId = rs.getString("lx_se_id");
				scheduleNumber = rs.getString("lx_se_id");

				if (logger.isDebugEnabled()) {
					logger.debug("getJobDispatchScheduleList(String, String, DataSource) - YYYYYYYYYY= "
							+ IlexTimestamp.getOP(plannedStartStamp));
				}
				list.add(
						i,
						new DispatchScheduleList(scheduleId, plannedStartStamp
								.getDate(), plannedStartStamp.getHour(),
								plannedStartStamp.getMinute(), IlexTimestamp
										.getOP(plannedStartStamp),
								plannedEndStamp.getDate(), plannedEndStamp
										.getHour(),
								plannedEndStamp.getMinute(), IlexTimestamp
										.getOP(plannedEndStamp),
								actualStartStamp.getDate(), actualStartStamp
										.getHour(), actualStartStamp
										.getMinute(), IlexTimestamp
										.getOP(actualStartStamp),
								actualEndStamp.getDate(), actualEndStamp
										.getHour(), actualEndStamp.getMinute(),
								IlexTimestamp.getOP(actualEndStamp),
								scheduleNumber));
				i++;
			}

			/*
			 * When there is no entry into lx_schedule_element table for this
			 * job get planned start and planned end dates from lm_job table.
			 */
			if (i == 0) {
				sql.setLength(0);
				sql.append("select "
						+ "lm_js_planned_start_date = isnull(replace(convert(varchar(10),(isnull( lm_js_planned_start_date ,getdate())),101)+''+ substring(convert(varchar(30),lm_js_planned_start_date,109),12,15),'01/01/1900 12:00:00:000AM',''),''), "
						+ "lm_js_planned_end_date = isnull(replace(convert(varchar(10),(isnull( lm_js_planned_end_date ,getdate())),101)+''+ substring(convert(varchar(30),lm_js_planned_end_date,109),12,15),'01/01/1900 12:00:00:000AM',''),'')	"
						+ "from lm_job Where lm_js_id =?");
				pStmt = conn.prepareStatement(sql.toString());
				pStmt.setString(1, jobId);
				rs = pStmt.executeQuery();

				if (rs.next()) {
					IlexTimestamp plannedStartStamp = new IlexTimestamp();
					IlexTimestamp plannedEndStamp = new IlexTimestamp();
					plannedStartStamp = IlexTimestamp.converToTimestamp(rs
							.getString("lm_js_planned_start_date"));
					plannedEndStamp = IlexTimestamp.converToTimestamp(rs
							.getString("lm_js_planned_end_date"));

					list.add(
							i,
							new DispatchScheduleList("", plannedStartStamp
									.getDate(), plannedStartStamp.getHour(),
									plannedStartStamp.getMinute(),
									IlexTimestamp.getOP(plannedStartStamp),
									plannedEndStamp.getDate(), plannedEndStamp
											.getHour(), plannedEndStamp
											.getMinute(), IlexTimestamp
											.getOP(plannedEndStamp), "", "",
									"", "", "", "", "", "", ""));
				}
			}
		} catch (Exception e) {
			logger.error(
					"getJobDispatchScheduleList(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobDispatchScheduleList(String, String, DataSource) - Exception caught in get Dispatch Schedule List :"
						+ e);
			}
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getJobDispatchScheduleList(String, String, DataSource) - getJobDispatchScheduleList.Size= "
					+ list.size());
		}
		return list;
	}// End getJobDispatchScheduleList() method

}
