package com.mind.newjobdb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.newjobdb.ChecklistStatusType;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * @purpose This class is used for the representation of the data access for the
 *          Project/Appendix Level Workflow Checklist.
 */
public class ProjectLevelWorkflowDAO extends WorkflowChecklistDAO {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ProjectLevelWorkflowDAO.class);

	public static void copyToJob(DataSource ds, String appendixId,
			String jobId, String userId) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			Timestamp ts = new java.sql.Timestamp(
					new java.util.Date().getTime());
			conn = ds.getConnection();
			String sql = "select item_id,sequence,description,reference_date,user_id,created_by,created_date from prj_lvl_job_chk_items  where appendix_id =?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, appendixId);
			rs = pStmt.executeQuery();
			if (logger.isDebugEnabled()) {
				logger.debug("copyToJob(DataSource, String, String, String) - coming here 123");
			}
			while (rs.next()) {
				sql = "insert into job_lvl_job_chk_items (job_id,prj_lvl_chk_item_id,sequence,description,"
						+ "item_status_code,last_item_status_code,created_by,created_date, updated_by, updated_date) "
						+ " values (?,?,?,?,?,?,?,getdate(),?,getdate())";
				/*
				 * sql =
				 * "insert into job_lvl_job_chk_items (job_id,prj_lvl_chk_item_id,sequence,description,reference_date,user_id,"
				 * +
				 * "item_status_code,last_item_status_code,created_by,created_date)"
				 * + "\n (select ?,item_id,sequence,description,?,?," +
				 * "?,?,?,? from dbo.prj_lvl_job_chk_items where appendix_id =?)"
				 * ;
				 */
				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, jobId);
				pStmt.setString(2, rs.getString("item_id"));
				pStmt.setString(3, rs.getString("sequence"));
				pStmt.setString(4, rs.getString("description"));
				// pStmt.setString(5,rs.getString("reference_date"));
				pStmt.setInt(5, ChecklistStatusType.PENDING_STATUS);
				pStmt.setInt(6, ChecklistStatusType.PENDING_STATUS);
				pStmt.setString(7, userId);
				// pStmt.setString(10,rs.getString("reference_date"));
				pStmt.setString(8, userId);
				// pStmt.setString(12,rs.getString("reference_date"));
				pStmt.executeUpdate();
			}

		} catch (Exception e) {
			logger.error("copyToJob(DataSource, String, String, String)", e);

			logger.error("copyToJob(DataSource, String, String, String)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}

	}
}
