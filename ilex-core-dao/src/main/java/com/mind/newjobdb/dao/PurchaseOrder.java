package com.mind.newjobdb.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.msp.SiteInfo;
import com.mind.bean.newjobdb.PartnerInfo;
import com.mind.newjobdb.dao.PoWoInfo;
import com.mind.common.LabelValue;
import com.mind.common.POStatusTypes;
import com.mind.docm.dao.EntityManager;
import  com.mind.bean.newjobdb.CostVariations;
import  com.mind.bean.newjobdb.EInvoice;
import  com.mind.newjobdb.util.InvalidPOActionException;
import  com.mind.bean.newjobdb.POActionTypes;
import  com.mind.newjobdb.dao.POAuthorizedCostInfo;
import  com.mind.newjobdb.dao.PODeliverables;
import  com.mind.newjobdb.dao.PODocumentManager;
import  com.mind.bean.newjobdb.POFooterInfo;

/**
 * @purpose This class is used for the representation of the Purchase Order.
 */
public class PurchaseOrder {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(PurchaseOrder.class);

	private String poNumber;
	private String poId;
	private String mpoId;
	private String overrideMinutemanCostChecked;

	/**
	 * Used when reissuing a PO by creating a new copy-PO with a backwards link
	 * to the cancelled PO,
	 */
	private String cancelledPONumber;

	private String woNumber;
	private int status;
	/**
	 * Each PO will need to maintain a previous status column to support the
	 * undo function.
	 */
	private int previousStatus;

	/**
	 * The revision field for a PO is intended to track changes to a PO after it
	 * has been transitioned to the partner review status. It will be
	 * implemented as a simple incrementing field that could be versioned the
	 * same way DocM documents are numbered and that can have numeric/float
	 * values.
	 * 
	 * By default, a PO will be assigned a revision level of 0 (zero).
	 * 
	 * A PO revision applies to the Reissue functions.
	 * 
	 */
	private String revision;

	private String costingType;
	private float authorizedCost;
	private String partnerContact;
	private String primaryContact;

	private CostVariations costVariations = new CostVariations();
	private ArrayList<LabelValue> availableActions = new ArrayList<LabelValue>();

	private POAuthorizedCostInfo poAuthorizedCostInfo = null;
	private PODocumentManager poDocumentManager = null;
	private PoWoInfo powoInfo = null;
	private PODeliverables poDeliverables = null;
	private POFooterInfo pOFooterInfo = null;
	private String masterPOItemType = null;
	private EInvoice eInvoice = null;
	private SiteInfo siteInfo = null;

	private String customerDelCount = null;
	private String internalDelCount = null;
	private String totalDelCount = null;

	private String commissionPO = null;
	private PartnerInfo partnerInfo;

	public String getCommissionPO() {
		return commissionPO;
	}

	public void setCommissionPO(String commissionPO) {
		this.commissionPO = commissionPO;
	}

	public String getCustomerDelCount() {
		return customerDelCount;
	}

	public void setCustomerDelCount(String customerDelCount) {
		this.customerDelCount = customerDelCount;
	}

	public String getInternalDelCount() {
		return internalDelCount;
	}

	public void setInternalDelCount(String internalDelCount) {
		this.internalDelCount = internalDelCount;
	}

	public String getTotalDelCount() {
		return totalDelCount;
	}

	public void setTotalDelCount(String totalDelCount) {
		this.totalDelCount = totalDelCount;
	}

	public String getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public EInvoice getEInvoice() {
		return eInvoice;
	}

	public void setEInvoice(EInvoice invoice) {
		eInvoice = invoice;
	}

	public boolean isPartnerAssigned() {
		return (this.partnerContact != null && !this.partnerContact.equals(""));
	}

	public CostVariations getCostVariations() {
		return costVariations;
	}

	public void setCostVariations(CostVariations costVariations) {
		this.costVariations = costVariations;
	}

	public float getAuthorizedCost() {
		return authorizedCost;
	}

	public void setAuthorizedCost(float authorizedCost) {
		this.authorizedCost = authorizedCost;
	}

	public String getCostingType() {
		return costingType;
	}

	public void setCostingType(String costingType) {
		this.costingType = costingType;
	}

	public String getPartnerContact() {
		return partnerContact;
	}

	public void setPartnerContact(String partnerContact) {
		this.partnerContact = partnerContact;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public int getPreviousStatus() {
		return previousStatus;
	}

	public void setPreviousStatus(int previousStatus) {
		this.previousStatus = previousStatus;
	}

	public String getRevision() {
		return revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getWoNumber() {
		return woNumber;
	}

	public void setWoNumber(String woNumber) {
		this.woNumber = woNumber;
	}

	/**
	 * The edit function will display the PO dashboard defaulting to the
	 * Authorized Cost Tab
	 * 
	 * There is no status transition triggered by this function.
	 * 
	 * Essentially calls the PurchaseOrderDAO for data access. Any business
	 * logic lies here.
	 */
	public void edit() {

	}

	/**
	 * The assign functions will display the partner search function.
	 * 
	 * Resources assigned to a PO will carry the PO revision level when assigned
	 * to the PO.
	 * 
	 * There is no status transition triggered by this function.
	 * 
	 * Essentially calls the PurchaseOrderDAO for data access. Any business
	 * logic lies here.
	 */
	public void assign() {

	}

	/**
	 * The send function will send the PO to the partner�s primary contact.
	 * Upon successful email sent, the PO is transitioned to the 'Partner
	 * Review' status.
	 * 
	 * 
	 * The send function will only apply to a PO with an assigned partner. It
	 * will display the current PO/WO email form. The email form will have all
	 * PO defined document shown as attachments which will not be editable from
	 * the email form.
	 * 
	 * The user will interact with this form per current requirements.
	 * 
	 * Submission of the email form will send the PO/WO to the partner and
	 * transition the PO to partner review status.
	 * 
	 * This is also the formal trigger point where the PO is entered into the
	 * DocM system
	 * 
	 * Essentially calls the PurchaseOrderDAO for data access. Any business
	 * logic lies here.
	 */
	public void send() {

	}

	/**
	 * Delete will delete the PO.
	 * 
	 * The delete function will delete a PO which is in the draft state. In the
	 * simplest case, the PO was created, but never sent. In this situation, the
	 * PO is deleted without record.
	 * 
	 * Essentially calls the PurchaseOrderDAO for data access. Any business
	 * logic lies here.
	 */
	public static void delete(String powoId, DataSource ds)
			throws InvalidPOActionException {
		PurchaseOrderDAO.deletePurchaseOrder(powoId, ds);
	}

	/**
	 * View PO (as PO) � Display the PO PDF
	 * 
	 * 
	 * Essentially calls the PurchaseOrderDAO for data access. Any business
	 * logic lies here.
	 */
	public void viewPO() {

	}

	/**
	 * View WO (as WO) � Display the WO PDF
	 * 
	 * 
	 * Essentially calls the PurchaseOrderDAO for data access. Any business
	 * logic lies here.
	 */
	public void viewWO() {

	}

	/**
	 * Checks if the new PO status is business-logically allowed or not.
	 * 
	 */
	public static boolean isTransitionAllowed(int previousPOStatusType,
			int newPOStatusType, DataSource ds) {
		boolean isTransitionAllowed = false;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select 1 from lm_po_next_status where current_po_status ="
					+ previousPOStatusType
					+ " and po_status_after_action ="
					+ newPOStatusType;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				isTransitionAllowed = true;
			}

		} catch (Exception e) {
			logger.error("isTransitionAllowed(int, int, DataSource)", e);

			logger.error("isTransitionAllowed(int, int, DataSource)", e);
		} finally {
			try {
				// Closing DB Resources
				if (rs != null) {
					rs.close();
					rs = null;
				}
				if (stmt != null) {
					stmt.close();
					stmt = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;

				}
			} catch (Exception e) {
				logger.error("isTransitionAllowed(int, int, DataSource)", e);

				logger.error("isTransitionAllowed(int, int, DataSource)", e);
			}

		}
		return isTransitionAllowed;
	}

	/**
	 * The PO being reassigned is a new PO which was created as a copy of the
	 * original PO.
	 * 
	 * Copy PO is an interim function that is called from reissue function.
	 */
	private static int copyPO(String powoId, String loginUserId,
			String partnerId, DataSource ds) throws InvalidPOActionException {
		int currentStatusType = PurchaseOrderDAO.getCurrentStatus(powoId, ds);
		int newPoWoId = -1;
		boolean allowed = true;
		allowed = PurchaseOrder.isActionAllowed(currentStatusType,
				POActionTypes.ACTION_REASSIGN, ds);
		if (!allowed) {
			throw new InvalidPOActionException();
		}
		allowed = PurchaseOrderDAO.isReAssignAllowed(powoId, partnerId, ds);
		if (!allowed) {
			throw new InvalidPOActionException();
		}
		PurchaseOrder.cancel(powoId, loginUserId, ds);
		newPoWoId = PurchaseOrderDAO.copyPurchaseOrder(loginUserId, powoId,
				partnerId, ds);
		return newPoWoId;
	}

	public static void setNewStatus(String powoId, String loginuserid,
			int newPOStatusType, DataSource ds) throws InvalidPOActionException {
		PurchaseOrderDAO.changePOWOStatus(powoId, loginuserid, newPOStatusType,
				ds);
	}

	/**
	 * If a PO was previously sent to a partner (transitioned out of draft on at
	 * least one occasion) the PO may be reissued. In this scenario, the PO
	 * cannot be deleted. This PO may be cancelled.
	 * 
	 * See associated change to the addition/deletion of resources. The PO is
	 * transitioned to the 'Draft' status.
	 * 
	 * A PO revision applies to the Reissue functions. When a PO is reissued,
	 * the revision field will increment.
	 * 
	 * The reissue consists of a state transition to draft with additional
	 * actions to update the PO revision level.
	 * 
	 */
	public static void reIssue(String powoId, String loginuserid, DataSource ds)
			throws InvalidPOActionException {
		PurchaseOrderDAO.updatePORevision(powoId, ds);
		PurchaseOrder.setNewStatus(powoId, loginuserid,
				POStatusTypes.STATUS_DRAFT, ds);
		PurchaseOrderDAO.setDocumentSuperseded(powoId, ds);
		// if(PurchaseOrderDAO.isPOResourceChangable(powoId, ds)){
		// PurchaseOrderDAO.changePOResourceRevision(powoId, ds);
		// }
	}

	/**
	 * If a PO was previously sent to a partner (transitioned out of draft on at
	 * least one occasion) the PO may be reassigned. In this scenario, the
	 * original PO was already cancelled. The PO being reassigned is a new PO
	 * which was created as a copy of the original PO. This PO may be deleted.
	 * 
	 * The reassign function performs the following steps: i. Cancels the
	 * current PO, ii. Creates a new PO with a backwards link to the cancelled
	 * PO, iii. Copies the content of the cancelled PO to the new PO defaulting
	 * the new PO to the draft state, and iv. Drives the user to the Assign
	 * screen
	 * 
	 * Resources assigned to a PO will carry the PO revision level when assigned
	 * to the PO.
	 * 
	 * The reassign transition consists of a cancel and copy actions
	 */
	public static int reAssign(String powoId, String loginUserId,
			String partnerId, DataSource ds) throws InvalidPOActionException {
		int newPoWoId = PurchaseOrder
				.copyPO(powoId, loginUserId, partnerId, ds);
		return newPoWoId;
	}

	/**
	 * The cancel function changes the PO to the 'Cancelled' status
	 */
	public static void cancel(String powoId, String loginuserid, DataSource ds)
			throws InvalidPOActionException {
		PurchaseOrder.setNewStatus(powoId, loginuserid,
				POStatusTypes.STATUS_CANCELLED, ds);
		try {
			PurchaseOrderDAO.setDocumentSuperseded(powoId, ds);
			// EntityManager.setEntityToDraft(powoId,
			// DatabaseUtilityDao.getKeyValue("ilex.docm.entity.po"));
		} catch (Exception e) {
			logger.error("cancel(String, String, DataSource)", e);

			logger.error("cancel(String, String, DataSource)", e);
		}

	}

	/**
	 * The accept function will change the PO to the 'Accepted' status.
	 * 
	 * Marking a job Onsite will look at the ideal status of all POs is
	 * 'Accepted' or as a minimum at least one PO in an Accepted status.
	 */
	public static void accept(String powoId, String loginuserid, DataSource ds)
			throws InvalidPOActionException {
		PurchaseOrder.setNewStatus(powoId, loginuserid,
				POStatusTypes.STATUS_ACCEPTED, ds);
	}

	/**
	 * The complete function will change the PO to the 'Complete' status.
	 * 
	 * If a job contains a PO without deliverables, marking the job complete
	 * will mark the PO complete.
	 */
	public static void complete(String powoId, String loginuserid, DataSource ds)
			throws InvalidPOActionException {
		PurchaseOrder.setNewStatus(powoId, loginuserid,
				POStatusTypes.STATUS_COMPLETED, ds);
	}

	/**
	 * The isActionAllowed function will return true if the action is allowed
	 * for the current status of PO
	 * 
	 */
	public static boolean isActionAllowed(int currentStatusType, int newAction,
			DataSource ds) {
		boolean allowed = false;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select is_allowed from lm_po_allowed_actions where current_po_status ="
					+ currentStatusType + " and action_on_po =" + newAction;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				if (rs.getString("is_allowed") != null
						&& (Integer.parseInt(rs.getString("is_allowed")) == 1)) {
					allowed = true;
				}
			}

		} catch (Exception e) {
			logger.error("isActionAllowed(int, int, DataSource)", e);

			logger.error("isActionAllowed(int, int, DataSource)", e);
		} finally {
			try {
				// Closing DB Resources
				if (rs != null) {
					rs.close();
					rs = null;
				}
				if (stmt != null) {
					stmt.close();
					stmt = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;

				}
			} catch (Exception e) {
				logger.error("isActionAllowed(int, int, DataSource)", e);

				logger.error("isActionAllowed(int, int, DataSource)", e);
			}

		}
		return allowed;
	}

	/**
	 * The getNextStatus method() will return the status of the po after the
	 * action else -1
	 * 
	 */
	public static int getNextStatus(int currentStatusType, int newAction,
			DataSource ds) throws InvalidPOActionException {
		if (!isActionAllowed(currentStatusType, newAction, ds)) {
			throw new InvalidPOActionException();
		}

		int nextStatus = -1;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select po_status_after_action from lm_po_next_status where current_po_status ="
					+ currentStatusType + " and action_on_po =" + newAction;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				if (rs.getString("po_status_after_action") != null
						&& (Integer.parseInt(rs
								.getString("po_status_after_action")) != -1)) {
					nextStatus = Integer.parseInt(rs
							.getString("po_status_after_action"));
				}
			}

		} catch (Exception e) {
			logger.error("getNextStatus(int, int, DataSource)", e);

			logger.error("getNextStatus(int, int, DataSource)", e);
		} finally {
			try {
				// Closing DB Resources
				if (rs != null) {
					rs.close();
					rs = null;
				}
				if (stmt != null) {
					stmt.close();
					stmt = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;

				}
			} catch (Exception e) {
				logger.error("getNextStatus(int, int, DataSource)", e);

				logger.error("getNextStatus(int, int, DataSource)", e);
			}

		}
		return nextStatus;
	}

	/**
	 * The email function will be used to send a copy of a PO in the Accepted or
	 * later status. This is different from the send function which also
	 * transitions the PO to Partner Review.
	 * 
	 * There is no status transition triggered by this function.
	 */
	public void email() {

	}

	/**
	 * The undo function will reverse the cancel function for the following
	 * situations: 'Partner Review transition to Cancelled', 'Accepted
	 * transition to Cancelled', and 'In Work transition to Cancelled'. The PO
	 * is transitioned to from status in these three situations. Each PO will
	 * need to maintain a previous status column to support this function.
	 */
	public static void undo(String powoId, String loginuserid, DataSource ds)
			throws InvalidPOActionException {
		boolean allowed = PurchaseOrder.isActionAllowed(
				PurchaseOrderDAO.getCurrentStatus(powoId, ds),
				POActionTypes.ACTION_UNDO, ds);
		if (allowed) {
			PurchaseOrder.setNewStatus(powoId, loginuserid,
					PurchaseOrderDAO.getPreviousStatus(powoId, ds), ds);
			int currentSatus = PurchaseOrderDAO.getCurrentStatus(powoId, ds);
			if (currentSatus == POStatusTypes.STATUS_PARTNER_REVIEW
					|| currentSatus == POStatusTypes.STATUS_ACCEPTED
					|| currentSatus == POStatusTypes.STATUS_CANCELLED) {
				try {
					EntityManager.setEntityToApproved(powoId, "PO");
				} catch (Exception e) {
					logger.error("undo(String, String, DataSource)", e);

					if (logger.isDebugEnabled()) {
						logger.debug("undo(String, String, DataSource) - Exception "
								+ e);
					}
				}
				try {
					EntityManager.setEntityToApproved(powoId, "WO");
				} catch (Exception e) {
					logger.error("undo(String, String, DataSource)", e);

					if (logger.isDebugEnabled()) {
						logger.debug("undo(String, String, DataSource) - Exception "
								+ e);
					}
				}
			}
		}
	}

	public static void savePOAuthorizedCostInfo(PurchaseOrder purchaseOrder,
			String userId, DataSource ds, String changedPOResources,
			String appendixType) {
		PurchaseOrderDAO.savePOAuthorizedCostInfo(purchaseOrder, userId, ds,
				changedPOResources, appendixType);
	}

	public static void savePOAuthorizedCostInfo(PurchaseOrder purchaseOrder,
			String userId, DataSource ds) {
		PurchaseOrderDAO.savePOAuthorizedCostInfo(purchaseOrder, userId, ds,
				"", "");
	}

	public static void savePurchaseWorkOrder(PurchaseOrder purchaseOrder,
			String userId, DataSource ds) {
		PurchaseOrderDAO.savePurchaseWorkOrder(purchaseOrder, userId, ds);
	}

	public static void savePurchaseWorkOrderFP(PurchaseOrder purchaseOrder,
			String userId, DataSource ds) {
		PurchaseOrderDAO.savePOAuthorizedCostFPInfo(purchaseOrder, userId, ds);
	}

	public static void savePODocuments(PurchaseOrder purchaseOrder,
			String userId, DataSource ds) {
		if (logger.isDebugEnabled()) {
			logger.debug("savePODocuments(PurchaseOrder, String, DataSource) - inside savePODocuments()");
		}
		PurchaseOrderDAO.savePODocument(purchaseOrder, userId, ds);
	}

	public static void savePODeliverables(PurchaseOrder purchaseOrder,
			String userId, DataSource ds) {
		if (logger.isDebugEnabled()) {
			logger.debug("savePODeliverables(PurchaseOrder, String, DataSource) - inside savePODeliverables()");
		}
		PurchaseOrderDAO.savePODeliverables(purchaseOrder, userId, ds);
	}

	public static boolean isCopiedFromMPO(String powoId, DataSource ds) {
		return PurchaseOrderDAO.isCopiedFromMPO(powoId, ds);
	}

	// public static CostVariations getCostVariations(PurchaseOrder
	// purchaseOrder) {
	// return purchaseOrder.getCostVariations() ;
	// }
	//
	public String getCancelledPONumber() {
		return cancelledPONumber;
	}

	/**
	 * Used when reissuing a PO by creating a new copy-PO with a backwards link
	 * to the cancelled PO,
	 */
	public void setCancelledPONumber(String cancelledPONumber) {
		this.cancelledPONumber = cancelledPONumber;
	}

	public String getPoId() {
		return poId;
	}

	public void setPoId(String poId) {
		this.poId = poId;
	}

	public ArrayList<LabelValue> getAvailableActions() {
		return availableActions;
	}

	public void setAvailableActions(ArrayList<LabelValue> availableActions) {
		this.availableActions = availableActions;
	}

	public POAuthorizedCostInfo getPoAuthorizedCostInfo() {
		return poAuthorizedCostInfo;
	}

	public void setPoAuthorizedCostInfo(
			POAuthorizedCostInfo poAuthorizedCostInfo) {
		this.poAuthorizedCostInfo = poAuthorizedCostInfo;
	}

	public PODeliverables getPoDeliverables() {
		return poDeliverables;
	}

	public void setPoDeliverables(PODeliverables poDeliverables) {
		this.poDeliverables = poDeliverables;
	}

	public PODocumentManager getPoDocumentManager() {
		return poDocumentManager;
	}

	public void setPoDocumentManager(PODocumentManager poDocumentManager) {
		this.poDocumentManager = poDocumentManager;
	}

	public PoWoInfo getPowoInfo() {
		return powoInfo;
	}

	public void setPowoInfo(PoWoInfo powoInfo) {
		this.powoInfo = powoInfo;
	}

	public POFooterInfo getPOFooterInfo() {
		return pOFooterInfo;
	}

	public void setPOFooterInfo(POFooterInfo footerInfo) {
		pOFooterInfo = footerInfo;
	}

	public String getMpoId() {
		return mpoId;
	}

	public void setMpoId(String mpoId) {
		this.mpoId = mpoId;
	}

	public String getMasterPOItemType() {
		return masterPOItemType;
	}

	public void setMasterPOItemType(String masterPOItemType) {
		this.masterPOItemType = masterPOItemType;
	}

	public PartnerInfo getPartnerInfo() {
		return partnerInfo;
	}

	public void setPartnerInfo(PartnerInfo partnerInfo) {
		this.partnerInfo = partnerInfo;
	}

	public SiteInfo getSiteInfo() {
		return siteInfo;
	}

	public void setSiteInfo(SiteInfo siteInfo) {
		this.siteInfo = siteInfo;
	}

	public String getOverrideMinutemanCostChecked() {
		return overrideMinutemanCostChecked;
	}

	public void setOverrideMinutemanCostChecked(
			String overrideMinutemanCostChecked) {
		this.overrideMinutemanCostChecked = overrideMinutemanCostChecked;
	}
}
