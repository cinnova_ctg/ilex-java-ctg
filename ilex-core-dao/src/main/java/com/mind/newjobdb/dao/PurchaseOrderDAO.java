package com.mind.newjobdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.PO.dao.PODao;
import com.mind.bean.masterScheduler.AutoCompleteMasterSchedulerTaskBean;
import com.mind.bean.mpo.PODTO;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.POActionTypes;
import com.mind.bean.newjobdb.POActivityManager;
import com.mind.bean.newjobdb.PODeliverable;
import com.mind.bean.newjobdb.PODocument;
import com.mind.bean.newjobdb.POFooterInfo;
import com.mind.bean.newjobdb.POResource;
import com.mind.bean.newjobdb.POResourceRevision;
import com.mind.bean.newjobdb.POType;
import com.mind.bean.newjobdb.RequiredEquipment;
import com.mind.common.EnvironmentSelector;
import com.mind.common.LabelValue;
import com.mind.common.PODeliverableStatusTypes;
import com.mind.common.POStatusTypes;
import com.mind.common.Util;
import com.mind.dao.PRM.POWOdao;
import com.mind.docm.client.ClientOperator;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.reports.reportDao.ReportsDao;
import com.mind.masterSchedulerTask.MasterSchedulerTaskUtil;
import com.mind.newjobdb.util.InvalidPOActionException;

/**
 * @purpose This class is used to contain the functionality for the data access
 *          related to Purchase Orders
 */
public class PurchaseOrderDAO {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(PurchaseOrderDAO.class);

	/**
	 * This method gets Purchase Order List under a Job.
	 * 
	 * @param jobId
	 *            - Job id
	 * @param ds
	 *            - Object of DataSource
	 * @return ArrayList<PurchaseOrder> - This method returns ArrayList,
	 *         Purchase Order.
	 */
	public static ArrayList<PurchaseOrder> getPurchaseOrderList(String jobId,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList<PurchaseOrder> purchaseOrderList = new ArrayList<PurchaseOrder>();

		try {
			conn = ds.getConnection();
			String sql = "select * from dbo.func_lm_powo_tab(?)";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobId);
			rs = pStmt.executeQuery();
			int i = 0;
			while (rs.next()) {

				PurchaseOrder purchaseOrder = new PurchaseOrder();
				POType pvsType = new POType(null);
				POType minutemanType = new POType(pvsType);
				POType speedpayType = new POType(pvsType);

				purchaseOrder.setPoId(rs.getString("lm_po_id"));
				purchaseOrder.setPoNumber(rs.getString("lm_po_number"));
				purchaseOrder.setWoNumber(rs.getString("lm_po_number"));
				purchaseOrder.setStatus(Integer.parseInt(rs
						.getString("po_status") != null ? rs
						.getString("po_status") : "0"));
				purchaseOrder.setPreviousStatus(Integer.parseInt(rs
						.getString("previous_status") != null ? rs
						.getString("previous_status") : "0"));
				purchaseOrder.setRevision((Integer.parseInt(rs
						.getString("revision") != null ? rs
						.getString("revision") : "0")) == 0 ? "" : rs
						.getString("revision"));
				purchaseOrder.setPartnerContact(rs
						.getString("lm_po_partner_name"));
				purchaseOrder.setPrimaryContact(!rs
						.getString("primary_contact").trim().equals("") ? "/"
						+ rs.getString("primary_contact") : "");
				purchaseOrder.setMasterPOItemType(rs
						.getString("lm_pwo_type_desc"));
				purchaseOrder.setCostingType(rs.getString("lm_po_type"));
				purchaseOrder.setAuthorizedCost(rs
						.getFloat("lm_po_authorised_total"));
				setContractLaborCost(pvsType, minutemanType, speedpayType,
						rs.getString("total_contract_labor_cost"));
				setHourlyTravelCost(pvsType, minutemanType, speedpayType,
						rs.getString("total_hourly_travel_cost"));
				purchaseOrder.getCostVariations().setPVSType(pvsType);
				purchaseOrder.getCostVariations().setMinutemanType(
						minutemanType);
				purchaseOrder.getCostVariations().setSpeedpayType(speedpayType);

				// if(purchaseOrder.getStatus()==POStatusTypes.STATUS_DRAFT) {
				ArrayList<LabelValue> availableActions = getListOfAvailableActions(
						purchaseOrder.getPoId(),
						rs.getString("po_status") != null ? rs
								.getString("po_status") : "0",
						purchaseOrder.isPartnerAssigned(),
						rs.getString("revision") != null ? rs
								.getString("revision") : "0",
						purchaseOrder.getMasterPOItemType(),
						rs.getString("lm_po_type"), ds);
				// if(purchaseOrder.getPartnerContact()!=null &&
				// !purchaseOrder.getPartnerContact().equals("")) {
				// //remove the Assign option
				// Iterator iter = availableActions.iterator();
				// int idx = -1;
				// while(iter.hasNext()) {
				// LabelValue label = (LabelValue) iter.next();
				// idx++;
				// if(label.getValue()!=null &&
				// label.getValue().equalsIgnoreCase("Assign")) {
				// availableActions.remove(idx);
				// break;
				// }
				// }
				// }

				purchaseOrder.setAvailableActions(availableActions);
				purchaseOrder.setCustomerDelCount(rs.getString("cust_del"));
				purchaseOrder.setInternalDelCount(rs.getString("int_del"));
				purchaseOrder.setTotalDelCount(rs.getString("total_del"));
				purchaseOrder.setCommissionPO(rs.getString("commission_po"));
				// }
				purchaseOrderList.add(i, purchaseOrder);
				i++;
			} // - end of while block
		} // - end of try block
		catch (Exception e) {
			logger.error("getPurchaseOrderList(String, DataSource)", e);
		} // - end of catch block
		finally {

			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return purchaseOrderList;
	} // - end of getPurchaseOrderList() method.

	/**
	 * @param pvsType
	 * @param minutemanType
	 * @param speedpayType
	 * @param contractLaborCost
	 */
	private static void setContractLaborCost(POType pvsType,
			POType minutemanType, POType speedpayType, String contractLaborCost) {

		String[] strArray = contractLaborCost.split("~");
		pvsType.setContractLaborCost(Float.parseFloat(strArray[0]));
		minutemanType.setContractLaborCost(Float.parseFloat(strArray[1]));
		speedpayType.setContractLaborCost(Float.parseFloat(strArray[2]));

	}

	/**
	 * @param pvsType
	 * @param minutemanType
	 * @param speedpayType
	 * @param hourlyTravelCost
	 */
	private static void setHourlyTravelCost(POType pvsType,
			POType minutemanType, POType speedpayType, String hourlyTravelCost) {
		String[] strArray = hourlyTravelCost.split("~");
		pvsType.setHourlyTravelCost(Float.parseFloat(strArray[0]));
		minutemanType.setHourlyTravelCost(Float.parseFloat(strArray[1]));
		speedpayType.setHourlyTravelCost(Float.parseFloat(strArray[2]));

	}

	/**
	 * This method completes the Purchase Order Status.
	 * 
	 * @param powoId
	 *            - POWO id
	 * @param ds
	 *            - Object of DataSource
	 * @param loginuserid
	 *            - id of the login user *
	 */
	public static void completePOWOStatus(String powoId, String loginuserid,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = ds.getConnection();
			String sql = "update lm_purchase_order set po_status ="
					+ POStatusTypes.STATUS_COMPLETED + " ,"
					+ " lm_po_changed_by = " + loginuserid
					+ ",lm_po_change_date = getdate() ,"
					+ " previous_status = po_status where lm_po_id = " + powoId
					+ " " + " and po_status = " + POStatusTypes.STATUS_ACCEPTED;
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			logger.error("completePOWOStatus(String, String, DataSource)", e);
		} finally {

			DBUtil.close(stmt);
			DBUtil.close(conn);
			// try {
			// // Closing DB Resources
			// if (stmt != null) {
			// stmt.close();
			// stmt = null;
			// }
			// if (conn != null) {
			// conn.close();
			// conn = null;
			//
			// }
			// } catch (Exception e) {
			// logger.error("completePOWOStatus(String, String, DataSource)",
			// e);
			// }

		}

	} // - end of completePOWOStatus() method.

        /**
	 * This method checks to see if a partner is an agent.
	 * 
	 * @param powoid
	 *            - The id of the purchase order.
	 * @param ds
	 *            - Object of DataSource
	 */
	public static int checkAgentStatus(String powoid,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int retval = -1;
                
                logger.info(retval);
                
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select cp_pd_id from cp_partner_details" +
                                        " join cp_partner on cp_pd_partner_id = cp_partner_id" +
                                        " join lm_purchase_order on lm_po_partner_id = cp_partner_id" +
                                        " where cp_pd_core_competency like '%19%' and lm_po_id = " + powoid;
                        
                                        logger.info(sql);
                        
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				retval = rs.getInt(1);
			}
		} catch (Exception e) {
			logger.error("checkAgentStatus(String, DataSource)", e);
		} finally {

			DBUtil.close(stmt);
			DBUtil.close(conn);

		}
                
                if(retval != -1){
                   retval = 1;
                }
                
                logger.info(retval);
                
                
                return retval;
                
	} // - end of checkAgentStatus() method.
        
	/**
	 * This method changes the Purchase Order Status to accepted.
	 * 
	 * @param powoId
	 *            - POWO id
	 * @param ds
	 *            - Object of DataSource
	 * @param loginuserid
	 *            - id of the login user *
	 */
	public static void acceptPOWOStatus(String powoId, String loginuserid,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = ds.getConnection();
			String sql = "update lm_purchase_order set po_status ="
					+ POStatusTypes.STATUS_ACCEPTED + " ,"
					+ " lm_po_changed_by = " + loginuserid
					+ ",lm_po_change_date = getdate() ,"
					+ " previous_status = po_status where lm_po_id = " + powoId
					+ " " + " and po_status = "
					+ POStatusTypes.STATUS_PARTNER_REVIEW;
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			logger.error("acceptPOWOStatus(String, String, DataSource)", e);
		} finally {
			// try {
			// // Closing DB Resources
			// if (stmt != null) {
			// stmt.close();
			// stmt = null;
			// }
			// if (conn != null) {
			// conn.close();
			// conn = null;
			//
			// }
			// } catch (Exception e) {
			// logger.error("acceptPOWOStatus(String, String, DataSource)", e);
			// }
			DBUtil.close(stmt);
			DBUtil.close(conn);

		}

	} // - end of acceptPOWOStatus() method.

	/**
	 * Discription: This method is used to update Purchase Order revision.
	 * 
	 * @param ds
	 *            -- Data Base Object
	 * @return retval -- Int
	 */
	public static int updatePORevision(String powoId, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int retval = -1;

		if (logger.isDebugEnabled()) {
			logger.debug("updatePORevision(String, DataSource) - powoId = "
					+ powoId);
		}
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lm_update_po_revision(?)}"); // Parameter
																			// 1
																			// output
																			// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, powoId);
			cstmt.execute();
			retval = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error("updatePORevision(String, DataSource)", e);
		}

		finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return retval;
	}

	/**
	 * Discription: This method is used to store Purchase Order into database.
	 * 
	 * @param jobId
	 *            -- Job Id
	 * @param loginUserId
	 *            -- Login User Id
	 * @param action
	 *            -- Add
	 * @param ds
	 *            -- Data Base Object
	 * @return retval -- Int
	 */
	public static String upsertPurchaseOrder(String jobId, String loginUserId,
			String action, String mopId, String powoId, String partnerId,
			String travelDistance, String travelDuration, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int retval = -1;
		if (logger.isDebugEnabled()) {
			logger.debug("upsertPurchaseOrder(String, String, String, String, String, String, DataSource) - jobId = "
					+ jobId);
			logger.debug("upsertPurchaseOrder(String, String, String, String, String, String, DataSource) - loginUserId = "
					+ loginUserId);
			logger.debug("upsertPurchaseOrder(String, String, String, String, String, String, DataSource) - action = "
					+ action);
			logger.debug("upsertPurchaseOrder(String, String, String, String, String, String, DataSource) - mopId = "
					+ mopId);
			logger.debug("upsertPurchaseOrder(String, String, String, String, String, String, DataSource) - powoId = "
					+ powoId);
			logger.debug("upsertPurchaseOrder(String, String, String, String, String, String, DataSource) - partnerId = "
					+ partnerId);
			logger.debug("upsertPurchaseOrder(String, String, String, String, String, String, String, String, DataSource) - travelDistance = " + travelDistance); //$NON-NLS-1$
			logger.debug("upsertPurchaseOrder(String, String, String, String, String, String, String, String, DataSource) - travelDuration= " + travelDuration); //$NON-NLS-1$
		}
		String poId = "";

		try {

			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_add_po_wo_manage_01(?,?,?,?,?,?,?,?)}"); // -
																						// 5
																						// Parameter
																						// &
																						// 2
																						// output
																						// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, jobId);
			cstmt.setString(3, loginUserId);
			cstmt.setString(4, action);
			cstmt.setString(5, powoId);
			cstmt.setString(6, partnerId);
			cstmt.setString(7, mopId.trim().equalsIgnoreCase("") ? null : mopId);
			cstmt.setString(8, travelDistance);
			cstmt.setString(9, travelDuration);
			cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
			cstmt.execute();
			retval = cstmt.getInt(1);
			poId = cstmt.getString(5);
			if (logger.isDebugEnabled()) {
				logger.debug("upsertPurchaseOrder(String, String, String, String, String, String, DataSource) - poId  "
						+ poId);
			}
		} catch (Exception e) {
			logger.error(
					"upsertPurchaseOrder(String, String, String, String, String, String, DataSource)",
					e);
		}

		finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return poId;
	}

	public static String getMinutemanOverrideCostValue(String poId,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int retval = -1;
		Integer minutemanOverrideCost = null;
		try {

			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lm_po_minuteman_cost_override from lm_purchase_order where lm_po_id ="
					+ poId;
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				minutemanOverrideCost = rs.getInt(1);
			}
		} catch (Exception e) {
			logger.error(
					"upsertPurchaseOrder(String, String, String, String, String, String, DataSource)",
					e);
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
		return minutemanOverrideCost.toString();
	}

	/**
	 * This method deleted Purchase Order.
	 * 
	 * @param powoId
	 *            - POWO id
	 * @param ds
	 *            - Object of DataSource
	 * 
	 */
	public static void deletePurchaseOrder(String powoId, DataSource ds)
			throws InvalidPOActionException {
		int currentPOStatus = -1;
		currentPOStatus = PurchaseOrderDAO.getCurrentStatus(powoId, ds);

		// dirty check... need to change
		if (currentPOStatus == -1)
			currentPOStatus = 0;

		boolean allowDelete = PurchaseOrder.isActionAllowed(currentPOStatus,
				POActionTypes.ACTION_DELETE, ds);
		if (allowDelete) {
			POWOdao.deletePOWO(powoId, ds);
		} else {
			throw new InvalidPOActionException();
		}
	} // - end of deletePurchaseOrder() method.

	public static POFooterInfo getPoFooterInfo(String powoId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		POFooterInfo pOFooterInfo = new POFooterInfo();
		try {
			conn = ds.getConnection();
			String sql = "select CONVERT(VARCHAR(10), lm_po_create_date, 1) as lm_po_create_date,CONVERT(VARCHAR(10), "
					+ "lm_po_change_date, 1) as lm_po_change_date,"
					+ "isnull(mp_mn_name,'') mp_mn_name, lm_po_created_by = isnull(dbo.func_cc_poc_name(lm_po_created_by),''), "
					+ "lm_po_changed_by = isnull(dbo.func_cc_poc_name(lm_po_changed_by),'') from lm_purchase_order "
					+ "left outer join mp_main on mp_mn_id = mpo_id where lm_po_id ="
					+ powoId;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				pOFooterInfo.setPoFooterCreateDate(rs
						.getString("lm_po_create_date"));
				pOFooterInfo.setPoFooterChangeDate(rs
						.getString("lm_po_change_date"));
				pOFooterInfo.setPoFooterCreateBy(rs
						.getString("lm_po_created_by"));
				pOFooterInfo.setPoFooterChangeBy(rs
						.getString("lm_po_changed_by"));

				String poFooterSource = "New";
				if (!rs.getString("mp_mn_name").equals("")) {
					poFooterSource = rs.getString("mp_mn_name").trim();
				}

				pOFooterInfo.setPoFooterSource(poFooterSource);
			}
		} catch (Exception e) {
			logger.error("getPoFooterInfo(String, DataSource)", e);
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error("getPoFooterInfo(String, DataSource)", e);
			}

		}
		return pOFooterInfo;
	}

	// This method takes the poId and returns the previous status of the po
	public static int getPreviousStatus(String powoId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int poStatus = -1;
		try {
			conn = ds.getConnection();
			String sql = "select previous_status from lm_purchase_order where lm_po_id = "
					+ powoId;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				if (rs.getString("previous_status") != null) {
					poStatus = Integer
							.parseInt(rs.getString("previous_status"));
				}
			}
		} catch (Exception e) {
			logger.error("getPreviousStatus(String, DataSource)", e);
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error("getPreviousStatus(String, DataSource)", e);
			}

		}
		return poStatus;
	}

	public static String getPORevision(String powoId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String poRevision = "";
		try {
			conn = ds.getConnection();
			String sql = "select revision from lm_purchase_order where lm_po_id = "
					+ powoId;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				poRevision = rs.getString("revision");
			}
		} catch (Exception e) {
			logger.error("getPORevision(String, DataSource)", e);
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error("getPORevision(String, DataSource)", e);
			}

		}
		return poRevision;
	}

	// This method takes the poId and returns the current status
	public static int getCurrentStatus(String powoId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int poStatus = -1;
		try {
			conn = ds.getConnection();
			String sql = "select po_status from lm_purchase_order where lm_po_id = "
					+ powoId;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				poStatus = Integer.parseInt(rs.getString("po_status"));
			}
		} catch (Exception e) {
			logger.error("getCurrentStatus(String, DataSource)", e);
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error("getCurrentStatus(String, DataSource)", e);
			}

		}
		return poStatus;
	}

	/**
	 * This method changes the Purchase Order Status.
	 * 
	 * @param powoId
	 *            - POWO id
	 * @param ds
	 *            - Object of DataSource
	 * @param loginuserid
	 *            - id of the login user
	 * @param poStatus
	 *            - the status of the po to be set
	 */
	public static void changePOWOStatus(String powoId, String loginuserid,
			int newPOStatusType, DataSource ds) throws InvalidPOActionException {
		int currentPOStatus = -1;
		boolean allowChange = true;
		currentPOStatus = PurchaseOrderDAO.getCurrentStatus(powoId, ds);
		// commented since currently po_status is null, hence throws
		// InvalidPOActionException
		// boolean allowChange =
		// PurchaseOrder.isTransitionAllowed(currentPOStatus, newPOStatusType,
		// ds);
		if (!allowChange) {
			throw new InvalidPOActionException();
		}

		Connection conn = null;
		CallableStatement cstmt = null;
		Statement stmt = null;
		StringBuilder sql;
		ResultSet rs = null;
		boolean isPureInternal = false;
		try {
			conn = ds.getConnection();

			sql = new StringBuilder(
					"select 1 from lm_purchase_order po where case when lm_po_type='N' and ( ");
			sql.append(" isnull(lm_po_pwo_type_id,-1) <> 3 and po.lm_po_id not in ");
			sql.append(" (select lm_pwo_po_id from lm_purchase_work_order join ");
			sql.append(" dbo.lm_resource on lm_pwo_rs_id=lm_rs_id join dbo.iv_resource_pool ");
			sql.append(
					" on lm_rs_rp_id=iv_rp_id where iv_rp_name like '%lift%')) then 1 else 0 end =1 and lm_po_id= ")
					.append(powoId);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql.toString());
			if (rs.next()) {
				isPureInternal = true;
			}
			if (isPureInternal && newPOStatusType == 3) {
				cstmt = conn
						.prepareCall("{?=call dbo.set_accepted_po_to_complete( ? ,?, ?) }");
				cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
				cstmt.setInt(2, -1);
				cstmt.setInt(3, Integer.parseInt(powoId));
				cstmt.setInt(4, Integer.parseInt(loginuserid));
				cstmt.execute();

			} else {
				cstmt = conn
						.prepareCall("{?=call lm_change_powo_status(?,?,?)}");
				cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
				cstmt.setInt(2, newPOStatusType);
				cstmt.setString(3, loginuserid);
				cstmt.setString(4, powoId);

				if (!cstmt.execute()) {
					/*
					 * cstmt.execute(); returns false if it update any record in
					 * DB that is why added condition on false value.
					 */

					// preparing bean for master scheduler auto complete task.
					AutoCompleteMasterSchedulerTaskBean bean = new AutoCompleteMasterSchedulerTaskBean();

					bean.setApiUser(EnvironmentSelector
							.getBundleString("api.username"));

					bean.setApiUserPassword(EnvironmentSelector
							.getBundleString("api.password"));

					bean.setUserId(loginuserid);
					bean.setTaskTypeId("9"); // Hard coaded task type ID
					bean.setTaskTypeKey(getJobId(powoId, ds));
					bean.setUrl(EnvironmentSelector
							.getBundleString("api.secure.url"));
					// call for master scheduler
					String responseResult = MasterSchedulerTaskUtil
							.sendAPIRequest(bean);

					logger.info(responseResult);

				}
			}

		} catch (Exception e) {
			logger.error("changePOWOStatus(String, String, int, DataSource)", e);
		} finally {

			DBUtil.close(cstmt);
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
			// try {
			// // Closing DB Resources
			// if (cstmt != null) {
			// cstmt.close();
			// cstmt = null;
			// }
			// if (stmt != null) {
			// stmt.close();
			// stmt = null;
			// }
			// if (rs != null) {
			// rs.close();
			// rs = null;
			// }
			// if (conn != null) {
			// conn.close();
			// conn = null;
			//
			// }
			// } catch (Exception e) {
			// logger.error(
			// "changePOWOStatus(String, String, int, DataSource)", e);
			// }

		}

	} // - end of changePOWOStatus() method.

	private static ArrayList<LabelValue> getListOfAvailableActions(String poId,
			String status, DataSource ds) {
		return PurchaseOrderDAO.getListOfAvailableActions(poId, status, true,
				"0", "", "", ds);
	}

	public static boolean isPOResourceChangable(String powoId, DataSource ds) {
		boolean isPOResourceChangable = false;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select 1 from  lm_purchase_order where previous_status=1 and po_status=0 and revision = 1 and lm_po_id = "
					+ powoId;
			rs = stmt.executeQuery(sql);
			if (logger.isDebugEnabled()) {
				logger.debug("isPOResourceChangable(String, DataSource) - sqlsqlsqlsqlsqlsql: "
						+ sql);
			}
			if (rs.next()) {
				if (logger.isDebugEnabled()) {
					logger.debug("isPOResourceChangable(String, DataSource) - resource is changeable");
				}
				isPOResourceChangable = true;
			}

		} catch (Exception e) {
			logger.error("isPOResourceChangable(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return isPOResourceChangable;
	}

	public static ArrayList<LabelValue> getListOfAvailableActions(String poId,
			String status, boolean isPartnerAssigned, String revision,
			String masterPOItemType, String poType, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList<LabelValue> availableOptionsList = new ArrayList<LabelValue>();
		boolean isUndoAllowed = isUndoAllowed(poId,
				POStatusTypes.STATUS_CANCELLED, ds);

		StringBuffer sql = new StringBuffer();
		try {
			if (status != null) {
				conn = ds.getConnection();
				if (isUndoAllowed) { // - when is not allowed allowed
					sql.append("select action_on_po from lm_po_allowed_actions where is_allowed=1 and current_po_status = ? and action_on_po != 13");
				} else {
					sql.setLength(0);
					sql.append("select action_on_po from lm_po_allowed_actions where is_allowed=1 and current_po_status = ?");
				}
				// String sql =
				// "select action_on_po from lm_po_allowed_actions where is_allowed=1 and current_po_status = ?";
				pStmt = conn.prepareStatement(sql.toString());
				pStmt.setString(1, status);
				rs = pStmt.executeQuery();
				availableOptionsList.add(new LabelValue("-- Actions --", "0"));
				while (rs.next()) {
					String availableOption = POActionTypes.getActionNames(rs
							.getInt("action_on_po"));
					String value = availableOption;
					if (availableOption.length() > 1) {
						value = availableOption.substring(0, 1).toUpperCase()
								+ availableOption.substring(1,
										availableOption.length());
					} else if (availableOption.length() == 1) {
						value = availableOption.toUpperCase();
					}
					if (!(isPartnerAssigned == false && value
							.equalsIgnoreCase(POActionTypes
									.getActionNames(POActionTypes.ACTION_SEND)))) {
						// if(!((!revision.trim().equals("0") &&
						// availableOption.trim().equalsIgnoreCase("delete")) ||
						// (!revision.trim().equals("0") &&
						// availableOption.trim().equalsIgnoreCase("assign")))){
						if (!((!revision.trim().equals("0") && availableOption
								.trim().equalsIgnoreCase("delete")))) {
							availableOptionsList.add(new LabelValue(value,
									availableOption));
						}

					}
					// availableOptionsList.add(new
					// LabelValue(availableOption,availableOption));
				}
				if (isUndoAllowed) { // - when is not allowed allowed
					availableOptionsList.add(new LabelValue("Undo",
							"UndoNotAllowed"));
				}

				// when status is partner Review.
				/*
				 * if(Integer.parseInt(status) ==
				 * POStatusTypes.STATUS_PARTNER_REVIEW){
				 * availableOptionsList.add(new LabelValue("Accept","Accept"));
				 * }
				 */

				// Added new PO Action Process Checklist if partner is assigned
				// to a PO

				if (Integer.parseInt(status) != POStatusTypes.STATUS_DRAFT) {
					if (isPartnerAssigned == true && !status.equals("4")
							&& poType != null && !poType.equals("Internal")) {
						availableOptionsList.add(new LabelValue(
								"Process Checklist", "Process Checklist"));
					}
				}
				if (Integer.parseInt(status) != POStatusTypes.STATUS_DRAFT
						&& Integer.parseInt(status) != POStatusTypes.STATUS_CANCELLED) {
					if (masterPOItemType != null
							&& masterPOItemType
									.equalsIgnoreCase("Hourly Services")) {
						availableOptionsList.add(new LabelValue(
								"Update Quantity", "Update Quantity"));
					}
				}
				availableOptionsList.add(new LabelValue("View PO History",
						"View PO History"));
				availableOptionsList.add(new LabelValue("View WO History",
						"View WO History"));
			}
		} catch (Exception e) {
			logger.error(
					"getListOfAvailableActions(String, String, boolean, String, String, String, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return availableOptionsList;
	}

	public static boolean isReAssignAllowed(String poId, String partnerId,
			DataSource ds) {
		boolean allowed = true;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select lm_po_partner_id from lm_purchase_order where lm_po_id = "
					+ poId;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				if (rs.getString("lm_po_partner_id") != null
						&& (rs.getString("lm_po_partner_id").equals(partnerId))) {
					allowed = false;
				}
			}

		} catch (Exception e) {
			logger.error("isReAssignAllowed(String, String, DataSource)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return allowed;
	}

	/**
	 * Create new PO/WO and copy reassigned PO/WO's data into this new PO/WO
	 * data.
	 * 
	 * @param loginUserId
	 * @param powoId
	 * @param partnerId
	 * @param ds
	 * @return
	 */
	public static int copyPurchaseOrder(String loginUserId, String powoId,
			String partnerId, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int retval = -1;
		// System.out.println("loginUserId = "+loginUserId);
		// System.out.println("powoId = "+powoId);
		// System.out.println("partnerId = "+partnerId);

		try {

			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lm_copy_po_wo_01(?,?,?)}"); // - 5
																			// Parameter
																			// &
																			// 1
																			// output
																			// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, loginUserId);
			cstmt.setString(3, powoId);
			cstmt.setString(4, partnerId);
			cstmt.execute();
			retval = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"copyPurchaseOrder(String, String, String, DataSource)", e);
		}

		finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return retval;
	}

	public static String getPoSpecialConditions(PODTO bean) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String special_condition = "";
		try {
			conn = ReportsDao.getStaticConnection();
			String sql = "select  lm_po_special_conditions from dbo.lm_purchase_order  where lm_po_id = ? ";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, new Integer(bean.getPoId()).intValue());
			rs = pStmt.executeQuery();
			if (rs.next()) {
				special_condition = rs.getString("lm_po_special_conditions");

			}
		} catch (Exception e) {
			logger.error("getPoSpecialConditions(PODTO)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return special_condition;
	}

	public static String getPoDefaultSpecialConditions(PODTO bean) {

		return null;
	}

	public static String getWoDefaultNatureOfActivity() {
		return null;
	}

	public static String getWoSpecialInstructions(PODTO bean) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String special_instruction = "";
		try {
			conn = ReportsDao.getStaticConnection();
			String sql = "select  lm_po_special_instructions from dbo.lm_purchase_order  where lm_po_id = ? ";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, new Integer(bean.getPoId()).intValue());
			rs = pStmt.executeQuery();
			if (rs.next()) {
				special_instruction = rs
						.getString("lm_po_special_instructions");
			}
		} catch (Exception e) {
			logger.error("getWoSpecialInstructions(PODTO)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return special_instruction;
	}

	public static String getWoDefaultSpecialInstructions(PODTO bean) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String special_instruction = "";
		try {
			conn = ReportsDao.getStaticConnection();
			String sql = "select  lm_po_special_instructions from dbo.lm_purchase_order pod where pod.lm_po_id = ? ";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, new Integer(bean.getPoId()).intValue());
			rs = pStmt.executeQuery();
			if (rs.next()) {
				special_instruction = rs
						.getString("lm_po_special_instructions");
			}
		} catch (Exception e) {
			logger.error("getWoDefaultSpecialInstructions(PODTO)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return special_instruction;
	}

	private static String[] getResourceInfoFromDB(String poId, DataSource ds) {

		String[] resourceInfoFromDB = new String[5];
		StringBuffer resourceId = new StringBuffer();
		StringBuffer authorizedUnitCost = new StringBuffer();
		StringBuffer showOnWo = new StringBuffer();
		StringBuffer authorizedQuantity = new StringBuffer();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lm_pwo_rs_id, lm_pwo_show_on_wo = case when lm_pwo_show_on_wo = 'Y' "
					+ " then convert(varchar,lm_pwo_rs_id) else '' end, lm_pwo_authorised_unit_cost, "
					+ "lm_pwo_authorised_qty   from lm_purchase_work_order where lm_pwo_po_id ="
					+ poId;
			if (logger.isDebugEnabled()) {
				logger.debug("getResourceInfoFromDB(String, DataSource) - sql: "
						+ sql);
			}
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				resourceId.append(rs.getString("lm_pwo_rs_id") + ",");
				authorizedUnitCost.append(rs
						.getFloat("lm_pwo_authorised_unit_cost") + ",");
				authorizedQuantity.append(rs.getFloat("lm_pwo_authorised_qty")
						+ ",");
				showOnWo.append(rs.getString("lm_pwo_show_on_wo") + ",");
			}
			resourceInfoFromDB[0] = resourceId.toString();
			resourceInfoFromDB[1] = authorizedUnitCost.toString();
			resourceInfoFromDB[2] = showOnWo.toString();
			resourceInfoFromDB[3] = authorizedQuantity.toString();
		} catch (Exception e) {
			logger.error("getResourceInfoFromDB(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
			;
		}

		return resourceInfoFromDB;
	}

	public static String getChangedPOResourceIds(PurchaseOrder purchaseOrder,
			String userId, DataSource ds) {
		String newAddedUpdatedResIds = "";
		String[] resourceChangesDetail = new String[3];
		StringBuffer updatedResourceId = new StringBuffer();
		StringBuffer deletedResourceId = new StringBuffer();
		StringBuffer newlyAddedResourceId = new StringBuffer();
		boolean isFixedPriceType = isFixedPriceType(purchaseOrder
				.getPoAuthorizedCostInfo().getPoGeneralInfo().getMasterPOItem());
		String[] resoruceInfo = getResourceInfo(purchaseOrder
				.getPoAuthorizedCostInfo().getPoActivityManager()
				.getPoResources());
		String checkedResourceIds = resoruceInfo[0]; // resource id
		String unitCostOfCheckedResources = resoruceInfo[1]; // unit cost
		String resourceIdsOfCheckedShowOnWo = resoruceInfo[3]; // resource id
																// whose show on
																// wo is checked
		String quantityOfCheckedResources = resoruceInfo[4]; // quantity

		String[] checkedResourceIdsArr = checkedResourceIds.split(",");
		String[] unitCostOfCheckedResourcesArr = unitCostOfCheckedResources
				.split(",");
		String[] resourceIdsOfCheckedShowOnWoArr = resourceIdsOfCheckedShowOnWo
				.split(",");
		String[] quantityOfCheckedResourcesArr = quantityOfCheckedResources
				.split(",");
		HashMap<String, POResourceRevision> checkedResourceHM = new HashMap<String, POResourceRevision>();
		for (int i = 0; i < checkedResourceIdsArr.length; i++) {
			POResourceRevision poResourceRevision = new POResourceRevision();
			poResourceRevision.setResourceId(checkedResourceIdsArr[i]);
			poResourceRevision
					.setResourceUnitCost(Float
							.parseFloat((unitCostOfCheckedResourcesArr[i]
									.equals("") ? "0"
									: unitCostOfCheckedResourcesArr[i])));
			poResourceRevision
					.setResourceQuantity(Float
							.parseFloat((quantityOfCheckedResourcesArr[i]
									.equals("") ? "0"
									: quantityOfCheckedResourcesArr[i])));
			if (resourceIdsOfCheckedShowOnWo.indexOf(checkedResourceIdsArr[i]) != -1) {
				poResourceRevision.setResourceShowOnWo("1");
			} else {
				poResourceRevision.setResourceShowOnWo("0");
			}
			checkedResourceHM.put(checkedResourceIdsArr[i], poResourceRevision);

		}

		String[] resoruceInfoFromDB = PurchaseOrderDAO.getResourceInfoFromDB(
				purchaseOrder.getPoId(), ds);

		String dbResourceIds = resoruceInfoFromDB[0]; // resource id
		String dbResourcesUnitCost = resoruceInfoFromDB[1]; // unit cost
		String dbShowOnWo = resoruceInfoFromDB[2]; // resource id whose show on
													// wo is checked
		String dbResourcesQuantity = resoruceInfoFromDB[3]; // quantity

		String[] dbResourceIdsArr = dbResourceIds.split(",");
		String[] dbResourcesUnitCostArr = dbResourcesUnitCost.split(",");
		String[] dbShowOnWoArr = dbShowOnWo.split(",");
		String[] dbResourcesQuantityArr = dbResourcesQuantity.split(",");

		HashMap<String, POResourceRevision> dbResourceHM = new HashMap<String, POResourceRevision>();
		for (int i = 0; i < dbResourceIdsArr.length; i++) {
			POResourceRevision poResourceRevision = new POResourceRevision();
			poResourceRevision.setResourceId(dbResourceIdsArr[i]);
			poResourceRevision
					.setResourceUnitCost(Float.parseFloat(!dbResourcesUnitCostArr[i]
							.equals("") ? dbResourcesUnitCostArr[i] : "0"));
			poResourceRevision
					.setResourceQuantity(Float.parseFloat(!dbResourcesQuantityArr[i]
							.equals("") ? dbResourcesQuantityArr[i] : "0"));
			if (dbShowOnWo.indexOf(dbResourceIdsArr[i]) != -1) {
				poResourceRevision.setResourceShowOnWo("1");
			} else {
				poResourceRevision.setResourceShowOnWo("0");
			}
			dbResourceHM.put(dbResourceIdsArr[i], poResourceRevision);
		}

		// Checking for updated and deleted
		Iterator dbItr = dbResourceHM.keySet().iterator();
		while (dbItr.hasNext()) {
			String dbResourceIdKey = (String) dbItr.next();
			POResourceRevision poResourceRevisiondb = dbResourceHM
					.get(dbResourceIdKey);
			if (checkedResourceHM.containsKey(dbResourceIdKey)) {
				// Checking for updated po resources
				POResourceRevision poResourceRevisionChecked = checkedResourceHM
						.get(dbResourceIdKey);
				if (poResourceRevisiondb.getResourceQuantity() != poResourceRevisionChecked
						.getResourceQuantity()) {
					updatedResourceId.append(poResourceRevisionChecked
							.getResourceId() + ",");
				} else if (poResourceRevisiondb.getResourceUnitCost() != poResourceRevisionChecked
						.getResourceUnitCost()) {
					updatedResourceId.append(poResourceRevisionChecked
							.getResourceId() + ",");
				} else if (poResourceRevisiondb.getResourceShowOnWo() != poResourceRevisionChecked
						.getResourceShowOnWo()) {
					updatedResourceId.append(poResourceRevisionChecked
							.getResourceId() + ",");
				} else {

				}
			}
			if (!checkedResourceHM.containsKey(dbResourceIdKey)) {
				// Checking for deleted resources
				deletedResourceId.append(poResourceRevisiondb.getResourceId()
						+ ",");
			}
		}

		// Checking for newly added resource ids
		Iterator checkedResourceItr = checkedResourceHM.keySet().iterator();
		while (checkedResourceItr.hasNext()) {
			String checkedResourceIdKey = (String) checkedResourceItr.next();
			POResourceRevision poResourceRevisionChecked = checkedResourceHM
					.get(checkedResourceIdKey);
			if (!dbResourceHM.containsKey(checkedResourceIdKey)) {
				newlyAddedResourceId.append(poResourceRevisionChecked
						.getResourceId() + ",");
			}
		}

		newAddedUpdatedResIds = newlyAddedResourceId.toString()
				+ updatedResourceId.toString();
		String[] newAndUpdatedResIdsArr = newAddedUpdatedResIds.split(",");
		PurchaseOrderDAO.changePOResRevForUpdateAndNew(purchaseOrder.getPoId(),
				newAndUpdatedResIdsArr, ds);

		String deletedResourceIds = deletedResourceId.toString();
		String[] deletedResIdsArr = deletedResourceIds.split(",");
		PurchaseOrderDAO.changePOResRevForDeleted(purchaseOrder.getPoId(),
				deletedResIdsArr, ds);

		/*
		 * System.out.println("purchaseOrder.getRevision(): "+purchaseOrder.
		 * getRevision());
		 * System.out.println("purchaseOrder.getPoId(): "+purchaseOrder
		 * .getPoId());
		 * System.out.println("updatedResourceId++++ "+updatedResourceId
		 * .toString());
		 * System.out.println("deletedResourceId++++ "+deletedResourceId
		 * .toString());
		 * System.out.println("newlyAddedResourceId++++ "+newlyAddedResourceId
		 * .toString());
		 * 
		 * System.out.println("checkedResourceHM: "+checkedResourceHM);
		 * System.out.println("dbResourceHM: "+dbResourceHM);
		 * 
		 * System.out.println("checkedResourceIds: "+checkedResourceIds);
		 * System.
		 * out.println("unitCostOfCheckedResources: "+unitCostOfCheckedResources
		 * ); System.out.println("resourceIdsOfCheckedShowOnWo: "+
		 * resourceIdsOfCheckedShowOnWo);
		 * System.out.println("quantityOfCheckedResources: "
		 * +quantityOfCheckedResources);
		 * 
		 * System.out.println("dbResourceIds: "+dbResourceIds);
		 * System.out.println("dbResourcesUnitCost: "+dbResourcesUnitCost);
		 * System.out.println("dbShowOnWo: "+dbShowOnWo);
		 * System.out.println("dbResourcesQuantity: "+dbResourcesQuantity);
		 */

		return newAddedUpdatedResIds;
	}

	public static void changePOResRevForUpdateAndNew(String poId,
			String[] newAndUpdatedResIds, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		String poRevison = PurchaseOrderDAO.getPORevision(poId, ds);
		if (logger.isDebugEnabled()) {
			logger.debug("changePOResRevForUpdateAndNew(String, String[], DataSource) - poRevison::- "
					+ poRevison);
		}
		try {
			conn = ds.getConnection();
			pstmt1 = conn
					.prepareStatement("delete from lm_purchase_work_order_deleted where lm_pwo_po_id = ? and lm_pwo_rs_id = ?");
			for (int i = 0; i < newAndUpdatedResIds.length; i++) {
				pstmt1.setString(1, poId);
				pstmt1.setString(2, newAndUpdatedResIds[i]);
				if (newAndUpdatedResIds[i] != null
						&& !newAndUpdatedResIds[i].trim().equals(""))
					pstmt1.execute();
			}
			pstmt = conn
					.prepareStatement("update lm_purchase_work_order set lm_pwo_rs_revision = ? where lm_pwo_po_id = ? and lm_pwo_rs_id = ?");
			if (logger.isDebugEnabled()) {
				logger.debug("changePOResRevForUpdateAndNew(String, String[], DataSource) - poId:  "
						+ poId);
			}
			for (int i = 0; i < newAndUpdatedResIds.length; i++) {
				pstmt.setString(1, poRevison);
				pstmt.setString(2, poId);
				pstmt.setString(3, newAndUpdatedResIds[i]);
				if (newAndUpdatedResIds[i] != null
						&& !newAndUpdatedResIds[i].trim().equals(""))
					pstmt.execute();
			}
		} catch (Exception e) {
			logger.error(
					"changePOResRevForUpdateAndNew(String, String[], DataSource)",
					e);
		} finally {
			DBUtil.close(pstmt);
			DBUtil.close(pstmt1);
			DBUtil.close(conn);
		}
	}

	public static void changePOResourceRevision(String poId, DataSource ds) {
		String[] resoruceInfoFromDB = PurchaseOrderDAO.getResourceInfoFromDB(
				poId, ds);
		String dbResourceIds = resoruceInfoFromDB[0]; // resource ids
		PurchaseOrderDAO.changePOResRevForUpdateAndNew(poId,
				dbResourceIds.split(","), ds);
	}

	public static void changePOResRevForDeleted(String poId,
			String[] deletedResIds, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		try {
			conn = ds.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn
					.prepareStatement("update pwo_synchronize set syn_date_time = getdate()");
			pstmt.execute();

			pstmt = conn
					.prepareStatement("delete from lm_purchase_work_order_deleted where lm_pwo_po_id = ? and lm_pwo_rs_id = ?");
			for (int i = 0; i < deletedResIds.length; i++) {
				if (deletedResIds[i] != null && !deletedResIds[i].equals("")) {
					if (logger.isDebugEnabled()) {
						logger.debug("changePOResRevForDeleted(String, String[], DataSource) - deleted resource ids: "
								+ deletedResIds[i]);
					}
					pstmt.setString(1, poId);
					pstmt.setString(2, deletedResIds[i]);
					if (deletedResIds[i] != null
							&& !deletedResIds[i].trim().equals(""))
						pstmt.execute();
				}
			}
			pstmt1 = conn
					.prepareStatement("insert into lm_purchase_work_order_deleted(lm_pwo_po_id"
							+ ",lm_pwo_rs_id"
							+ ",lm_pwo_authorised_unit_cost"
							+ ",lm_pwo_authorised_total_cost"
							+ ",lm_pwo_show_on_wo"
							+ ",lm_pwo_drop_shipped"
							+ ",lm_pwo_authorised_qty"
							+ ",lm_pwo_pvs_supplied"
							+ ",lm_pwo_credit_card"
							+ ",lm_pwo_rs_revision) "
							+ "(select lm_pwo_po_id"
							+ ",lm_pwo_rs_id"
							+ ",lm_pwo_authorised_unit_cost"
							+ ",lm_pwo_authorised_total_cost"
							+ ",lm_pwo_show_on_wo"
							+ ",lm_pwo_drop_shipped"
							+ ",lm_pwo_authorised_qty"
							+ ",lm_pwo_pvs_supplied"
							+ ",lm_pwo_credit_card"
							+ ",(select revision from lm_purchase_order where lm_po_id = ?)"
							+ "from lm_purchase_work_order where lm_pwo_po_id = ? and lm_pwo_rs_id = ?)");

			for (int i = 0; i < deletedResIds.length; i++) {

				if (logger.isDebugEnabled()) {
					logger.debug("changePOResRevForDeleted(String, String[], DataSource) - deleted resource ids: "
							+ deletedResIds[i]);
				}
				pstmt1.setString(1, poId);
				pstmt1.setString(2, poId);
				pstmt1.setString(3, deletedResIds[i]);
				if (deletedResIds[i] != null
						&& !deletedResIds[i].trim().equals(""))
					pstmt1.execute();
			}
			conn.commit();
		} catch (Exception e) {
			DBUtil.rollback(conn);
			logger.error(
					"changePOResRevForDeleted(String, String[], DataSource)", e);
		} finally {
			DBUtil.close(pstmt);
			DBUtil.close(pstmt1);
			DBUtil.close(conn);
		}
	}

	/**
	 * Discription: This method is used to Update PO/WO record in database.
	 * 
	 * @param purchaseOrder
	 * @param userId
	 * @param ds
	 */
	public static void savePOAuthorizedCostInfo(PurchaseOrder purchaseOrder,
			String userId, DataSource ds, String changedPOResources,
			String appendixType) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		/**
		 * resoruceInfo[0] - Resource Id's - 1212,2009,678687,398,.............
		 * resoruceInfo[1] - Planned Unit Cost -
		 * 24.00,21.30,45.00,67,90,.......... resoruceInfo[2] - Planned Sub
		 * Total - 24.00,21.30,45.00,67,90,.......... resoruceInfo[3] - Show On
		 * Wo - 1212,2009,678687,398,............. resoruceInfo[4] - Quantity -
		 * 1,2,6,3,.............
		 * */
		boolean isFixedPriceType = isFixedPriceType(purchaseOrder
				.getPoAuthorizedCostInfo().getPoGeneralInfo().getMasterPOItem());
		String[] resoruceInfo = getResourceInfo(purchaseOrder
				.getPoAuthorizedCostInfo().getPoActivityManager()
				.getPoResources());

		int flag = -1;

		if (logger.isDebugEnabled()) {
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) - -----------IN PurchaseOrderDAO------------");
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  2= "
					+ purchaseOrder.getPoId());
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  3= "
					+ purchaseOrder.getPoAuthorizedCostInfo()
							.getPoGeneralInfo().getPoType());
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  4= "
					+ purchaseOrder.getPoAuthorizedCostInfo()
							.getPoGeneralInfo().getMasterPOItem());
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  5= "
					+ IlexTimestamp.getDateTimeType2(purchaseOrder
							.getPoAuthorizedCostInfo().getPoGeneralInfo()
							.getGeneralPOSetup().getIssueDate()));
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  6= "
					+ IlexTimestamp.getDateTimeType2(purchaseOrder
							.getPoAuthorizedCostInfo().getPoGeneralInfo()
							.getGeneralPOSetup().getDeliverByDate()));
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  7= "
					+ purchaseOrder.getPoAuthorizedCostInfo()
							.getPoGeneralInfo().getTerms());
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  8= "
					+ purchaseOrder.getPoAuthorizedCostInfo()
							.getPoActivityManager().getAuthorizedTotalCost());
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  9= "
					+ resoruceInfo[0]);
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  10= "
					+ resoruceInfo[1]);
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  11= "
					+ resoruceInfo[2]);
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  12= "
					+ resoruceInfo[3]);
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  13= "
					+ resoruceInfo[4]);
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  14= "
					+ userId);
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  15= "
					+ isBlank(purchaseOrder.getPoAuthorizedCostInfo()
							.getPoGeneralInfo().getContactPoc()));
			logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) -  16= "
					+ changedPOResources);
		}

		try {
			float cost;
			// if(purchaseOrder.getPoAuthorizedCostInfo().getPoGeneralInfo().getMasterPOItem()!=null
			// &&
			// purchaseOrder.getPoAuthorizedCostInfo().getPoGeneralInfo().getMasterPOItem().trim().equalsIgnoreCase("2"))
			if (purchaseOrder.getPoAuthorizedCostInfo().getPoGeneralInfo()
					.getMasterPOItem() != null
					&& purchaseOrder.getPoAuthorizedCostInfo()
							.getPoGeneralInfo().getMasterPOItem().trim()
							.equalsIgnoreCase("2")
					&& !appendixType.equals("")
					&& !appendixType.equals("NetMedX"))
				cost = purchaseOrder.getPoAuthorizedCostInfo()
						.getPoActivityManager().getAuthorizedCostInput();
			else
				cost = purchaseOrder.getPoAuthorizedCostInfo()
						.getPoActivityManager().getAuthorizedTotalCost();
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_po_wo_detail_manage_02(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"); // -
																												// 14
																												// Parameter
																												// &
																												// one
																												// out
																												// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, purchaseOrder.getPoId());
			cstmt.setString(3, purchaseOrder.getPoAuthorizedCostInfo()
					.getPoGeneralInfo().getPoType());
			cstmt.setString(4, purchaseOrder.getPoAuthorizedCostInfo()
					.getPoGeneralInfo().getMasterPOItem());
			cstmt.setString(
					5,
					IlexTimestamp.getDateTimeType2(purchaseOrder
							.getPoAuthorizedCostInfo().getPoGeneralInfo()
							.getGeneralPOSetup().getIssueDate()));
			cstmt.setString(
					6,
					IlexTimestamp.getDateTimeType2(purchaseOrder
							.getPoAuthorizedCostInfo().getPoGeneralInfo()
							.getGeneralPOSetup().getDeliverByDate()));
			cstmt.setString(7, purchaseOrder.getPoAuthorizedCostInfo()
					.getPoGeneralInfo().getTerms());
			cstmt.setFloat(8, cost);
			// cstmt.setFloat( 8 ,
			// purchaseOrder.getPoAuthorizedCostInfo().getPoActivityManager().getAuthorizedTotalCost());
			cstmt.setString(9, resoruceInfo[0]);
			cstmt.setString(10, resoruceInfo[1]);
			cstmt.setString(11, resoruceInfo[2]);
			cstmt.setString(12, resoruceInfo[3]);
			cstmt.setString(13, resoruceInfo[4]);
			cstmt.setString(14, userId);
			cstmt.setString(15, isBlank(purchaseOrder.getPoAuthorizedCostInfo()
					.getPoGeneralInfo().getContactPoc()));
			cstmt.setString(16, changedPOResources);
			cstmt.setString(17, purchaseOrder.getOverrideMinutemanCostChecked());
			// added for PVS Justification
			cstmt.setInt(18, purchaseOrder.getPoAuthorizedCostInfo()
					.getPoGeneralInfo().getPvsJustification());
			// added for geographic constraint
			cstmt.setInt(19, purchaseOrder.getPoAuthorizedCostInfo()
					.getPoGeneralInfo().getGeographicConstraint());
			cstmt.execute();
			flag = cstmt.getInt(1);
			// added for core competancy
			deleteCoreCompetencyData(purchaseOrder.getPoId(), conn);
			if (purchaseOrder.getPoAuthorizedCostInfo().getPoGeneralInfo()
					.getGeographicConstraint() == 4)// for Core Competency Not
													// Available
			{
				insertCoreCompetancyData(purchaseOrder, conn);
			}
		} catch (Exception e) {
			logger.error(
					"savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("savePOAuthorizedCostInfo(PurchaseOrder, String, DataSource, String) - Exception in com.mind.newjobdb.dao.savePOAuthorizedCostInfo()  "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}

	} // - end of method savePOAuthorizedCostInfo()

	/**
	 * Discription: This method is used to Update PO/WO record in database.
	 * 
	 * @param purchaseOrder
	 * @param userId
	 * @param ds
	 */
	public static void savePOAuthorizedCostFPInfo(PurchaseOrder purchaseOrder,
			String userId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		/**
		 * resoruceInfo[0] - Resource Id's - 1212,2009,678687,398,.............
		 * resoruceInfo[1] - Planned Unit Cost -
		 * 24.00,21.30,45.00,67,90,.......... resoruceInfo[2] - Planned Sub
		 * Total - 24.00,21.30,45.00,67,90,.......... resoruceInfo[3] - Show On
		 * Wo - 1212,2009,678687,398,............. resoruceInfo[4] - Show On Wo
		 * - 1,2,6,3,.............
		 * */
		String[] resoruceInfo = getResourceInfo(purchaseOrder
				.getPoAuthorizedCostInfo().getPoActivityManager()
				.getPoResources());
		int flag = -1;

		if (logger.isDebugEnabled()) {
			logger.debug("savePOAuthorizedCostFPInfo(PurchaseOrder, String, DataSource) - -----------IN PurchaseOrderDAO------------");
			logger.debug("savePOAuthorizedCostFPInfo(PurchaseOrder, String, DataSource) -  2= "
					+ purchaseOrder.getPoId());
			logger.debug("savePOAuthorizedCostFPInfo(PurchaseOrder, String, DataSource) -  9= "
					+ resoruceInfo[0]);
			logger.debug("savePOAuthorizedCostFPInfo(PurchaseOrder, String, DataSource) -  13= "
					+ resoruceInfo[4]);
		}

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call lm_powo_fp_manage_01(?,?,?,?)}"); // -
																				// 14
																				// Parameter
																				// &
																				// one
																				// out
																				// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, purchaseOrder.getPoId());
			cstmt.setString(3, resoruceInfo[0]);
			cstmt.setString(4, resoruceInfo[4]);
			cstmt.setString(5, userId);
			cstmt.execute();
			flag = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"savePOAuthorizedCostFPInfo(PurchaseOrder, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("savePOAuthorizedCostFPInfo(PurchaseOrder, String, DataSource) - Exception in com.mind.newjobdb.dao.savePOAuthorizedCostFPInfo()  "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);

		}

	} // - end of method savePOAuthorizedCostInfo()

	/**
	 * Discription: This method retures String[] of comma seperated string.
	 * 
	 * @param poResource
	 * @return
	 */

	/**
	 * Discription: This method retures String[] of comma seperated string.
	 * 
	 * @param poResource
	 * @return
	 */
	private static String[] getResourceInfo(ArrayList<POResource> poResource) {

		String[] strArray = new String[5];
		StringBuffer resourceId = new StringBuffer();
		StringBuffer authorizedUnitCost = new StringBuffer();
		StringBuffer authorizedSubTotal = new StringBuffer();
		StringBuffer showOnWo = new StringBuffer();
		StringBuffer authorizedQuantity = new StringBuffer();

		Iterator iterator = poResource.iterator();
		while (iterator.hasNext()) {
			POResource resource = (POResource) iterator.next();
			resourceId.append(resource.getResourceId() + ",");
			authorizedUnitCost.append(resource.getAsPlannedUnitCost() + ",");
			authorizedQuantity.append(resource.getAsPlannedQuantity() + ",");
			if (resource.isShowOnWO())
				showOnWo.append(resource.getResourceId() + ",");
		}
		strArray[0] = resourceId.toString();
		strArray[1] = authorizedUnitCost.toString();
		strArray[2] = authorizedSubTotal.toString();
		strArray[3] = Util.removeLastComm(showOnWo.toString());
		strArray[4] = authorizedQuantity.toString();

		return strArray;
	} // - end of method getResourceInfo()

	/**
	 * Set true false for PO item types.
	 * 
	 * @param masterPOItemType
	 * @return
	 */
	private static boolean isFixedPriceType(String masterPOItemType) {
		if (masterPOItemType != null && masterPOItemType.equals("2")) {
			return true;
		} else {
			return false;
		}
	} // - end of method isFixedPriceType()

	/**
	 * Convert blank string to null.
	 * 
	 * @param str
	 * @return
	 */
	private static String isBlank(String str) {
		if (str != null && str.equals("")) {
			str = str.trim();
			str = null;
		}
		return str;
	} // - end of method isBlank()

	/**
	 * Discription: This method is used to Update PO/WO details record in
	 * database.
	 * 
	 * @param purchaseOrder
	 * @param userId
	 * @param ds
	 */
	public static void savePurchaseWorkOrder(PurchaseOrder purchaseOrder,
			String userId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int flag = -1;
		String toolIds = getToolIds(purchaseOrder.getPowoInfo()
				.getRequiredEquipments());
		if (logger.isDebugEnabled()) {
			logger.debug("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) - -----------IN savePurchaseWorkOrder------------");
			logger.debug("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) -  2="
					+ purchaseOrder.getPoId());
			logger.debug("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) -  3="
					+ userId);
			logger.debug("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) -  4="
					+ purchaseOrder.getPowoInfo().getPoSpecialConditions());
			logger.debug("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) -  5="
					+ purchaseOrder.getPowoInfo().getWoNatureOfActivity());
			logger.debug("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) -  6="
					+ purchaseOrder.getPowoInfo().getWoSpecialInstructions());
			logger.debug("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) -  7="
					+ toolIds);
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_po_wo_manage_01(?,?,?,?,?,?)}"); // -
																				// 6
																				// Parameter
																				// &
																				// one
																				// out
																				// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, purchaseOrder.getPoId());
			cstmt.setString(3, userId);
			cstmt.setString(4, purchaseOrder.getPowoInfo()
					.getPoSpecialConditions());
			cstmt.setString(5, purchaseOrder.getPowoInfo()
					.getWoNatureOfActivity());

			cstmt.setString(6, purchaseOrder.getPowoInfo()
					.getWoSpecialInstructions());
			cstmt.setString(7, toolIds);
			cstmt.execute();
			flag = cstmt.getInt(1);

			if (logger.isDebugEnabled()) {
				logger.debug("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) - flag = "
						+ flag);
			}
		} catch (Exception e) {
			logger.error("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) - -----------IN savePurchaseWorkOrder------------");
			logger.error("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) -  2="
					+ purchaseOrder.getPoId());
			logger.error("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) -  3="
					+ userId);
			logger.error("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) -  4="
					+ purchaseOrder.getPowoInfo().getPoSpecialConditions());
			logger.error("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) -  5="
					+ purchaseOrder.getPowoInfo().getWoNatureOfActivity());
			logger.error("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) -  6="
					+ purchaseOrder.getPowoInfo().getWoSpecialInstructions());
			logger.error("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) -  7="
					+ toolIds);
			logger.error(
					"savePurchaseWorkOrder(PurchaseOrder, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("savePurchaseWorkOrder(PurchaseOrder, String, DataSource) - Exception in com.mind.newjobdb.dao.setPurchaseOrderWorkOrder()  "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
	} // - end of method savePurchaseWorkOrder()

	/**
	 * Discription: This method retures String of comma seperated Tool ids
	 * string.
	 * 
	 * @param poResource
	 * @return
	 */
	private static String getToolIds(
			ArrayList<RequiredEquipment> requiredEquipments) {

		StringBuffer toolIds = new StringBuffer();
		if (requiredEquipments != null) {
			Iterator iterator = requiredEquipments.iterator();

			while (iterator.hasNext()) {
				RequiredEquipment equipment = (RequiredEquipment) iterator
						.next();
				toolIds.append(equipment.getId() + ",");
			}
		}
		if (toolIds != null)
			return toolIds.toString();
		else
			return "";
	} // - end of method getToolIds()

	/**
	 * Discription: This method is used to Insert/Update PO/WO document record
	 * in database.
	 * 
	 * @param purchaseOrder
	 * @param userId
	 * @param ds
	 */
	public static void savePODocument(PurchaseOrder purchaseOrder,
			String userId, DataSource ds) {
		String sql = null;
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		PreparedStatement pStmt = null;
		Connection conn = null;
		ArrayList<PODocument> poDocumentsAL = purchaseOrder
				.getPoDocumentManager().getPoDocuments();
		try {
			conn = ds.getConnection();
			for (int i = 0; i < poDocumentsAL.size(); i++) {
				String doc_id = PODao.getDocIdFromMPO(purchaseOrder.getPoId(),
						poDocumentsAL.get(i).getDocId());
				if (doc_id.equals("")) {
					sql = "insert into podb_document (lm_po_id,podb_doc_id,doc_with_po,doc_with_wo,podb_doc_created_by,"
							+ "podb_doc_created_date,podb_doc_updated_by,podb_doc_updated_date)"
							+ " \n values (?,?,?,?,?,?,?,?)";
					pStmt = conn.prepareStatement(sql);
					pStmt.setInt(1, Integer.parseInt(purchaseOrder.getPoId()));
					pStmt.setInt(2,
							Integer.parseInt(poDocumentsAL.get(i).getDocId()));
					pStmt.setString(
							3,
							poDocumentsAL.get(i).isIncludedWithPO() == true ? "1"
									: "0");
					pStmt.setString(
							4,
							poDocumentsAL.get(i).isIncludedWithWO() == true ? "1"
									: "0");
					pStmt.setString(5, userId);
					pStmt.setTimestamp(6, ts);
					pStmt.setString(7, userId);
					pStmt.setTimestamp(8, ts);
					pStmt.executeUpdate();

				} else if (!doc_id.equals("")) {
					sql = "update podb_document set podb_doc_id = ? ,"
							+ " doc_with_po = ?, doc_with_wo = ?,"
							+ " podb_doc_updated_by = ?, podb_doc_updated_date = ?"
							+ " where lm_po_id = ? and podb_doc_id = ?";
					try {
						pStmt = conn.prepareStatement(sql);
						pStmt.setInt(1, Integer.parseInt(poDocumentsAL.get(i)
								.getDocId()));
						pStmt.setString(2, poDocumentsAL.get(i)
								.isIncludedWithPO() == true ? "1" : "0");
						pStmt.setString(3, poDocumentsAL.get(i)
								.isIncludedWithWO() == true ? "1" : "0");
						pStmt.setString(4, userId);
						pStmt.setTimestamp(5, ts);
						pStmt.setInt(6,
								Integer.parseInt(purchaseOrder.getPoId()));
						pStmt.setInt(7, Integer.parseInt(poDocumentsAL.get(i)
								.getDocId()));
						pStmt.executeUpdate();

					} catch (NumberFormatException e) {
						logger.error(
								"savePODocument(PurchaseOrder, String, DataSource)",
								e);
					} catch (SQLException e) {
						logger.error(
								"savePODocument(PurchaseOrder, String, DataSource)",
								e);
					}
				}
			}
		} catch (Exception e) {
			logger.error("savePODocument(PurchaseOrder, String, DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * Discription: This method is used to Insert/Update PO/WO document record
	 * in database.
	 * 
	 * @param purchaseOrder
	 * @param userId
	 * @param ds
	 */
	public static void savePODeliverables(PurchaseOrder purchaseOrder,
			String userId, DataSource ds) {
		PreparedStatement pStmt = null;
		Connection conn = null;
		String sql = "";
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		ArrayList<PODeliverable> poDeliverablesAL = purchaseOrder
				.getPoDeliverables().getPoDeliverable();
		try {
			for (int i = 0; i < poDeliverablesAL.size(); i++) {
				conn = ds.getConnection();
				sql = "update dbo.podb_purchase_order_deliverables "
						+ " \n set podb_mn_dl_title=?,"
						+ " \n podb_mn_dl_type = ?,"
						+ " \n podb_mn_dl_format = ?,"
						+ " \n podb_mn_dl_desc = ?,"
						+ " \n podb_mn_dl_updated_by = ?,"
						+ " \n podb_mn_dl_updated_date = ?"
						+ "\n where lm_po_id = ? and podb_mn_dl_id = ?";
				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, poDeliverablesAL.get(i).getTitle());
				pStmt.setString(2, poDeliverablesAL.get(i).getType());
				pStmt.setString(3, poDeliverablesAL.get(i).getFormat());
				pStmt.setString(4, poDeliverablesAL.get(i).getDescription());
				pStmt.setString(5, userId);
				pStmt.setTimestamp(6, ts);
				if (poDeliverablesAL.get(i).getDevId().trim()
						.equalsIgnoreCase("")) {
					pStmt.setString(7, "-1");
					pStmt.setString(8, "-1");
				} else {
					pStmt.setString(7, purchaseOrder.getPoId());
					pStmt.setString(8, poDeliverablesAL.get(i).getDevId());
				}
				if (logger.isDebugEnabled()) {
					logger.debug("savePODeliverables(PurchaseOrder, String, DataSource) - "
							+ sql);
				}
				// int result = stmt.executeUpdate(sql);
				int result = pStmt.executeUpdate();
				if (logger.isDebugEnabled()) {
					logger.debug("savePODeliverables(PurchaseOrder, String, DataSource) - result"
							+ result);
				}
				if (result <= 0) {
					sql = "insert into dbo.podb_purchase_order_deliverables (lm_po_id,podb_mn_dl_title,podb_mn_dl_type,"
							+ "podb_mn_dl_format,podb_mn_dl_desc,podb_mn_dl_created_by,podb_mn_dl_created_date,podb_mn_dl_updated_by,podb_mn_dl_updated_date) values"
							+ "\n (?,?,?,?,?,?,?,?,?)";

					pStmt = conn.prepareStatement(sql);
					pStmt.setString(1, purchaseOrder.getPoId());
					pStmt.setString(2, poDeliverablesAL.get(i).getTitle());
					pStmt.setString(3, poDeliverablesAL.get(i).getType());
					pStmt.setString(4, poDeliverablesAL.get(i).getFormat());
					pStmt.setString(5, poDeliverablesAL.get(i).getDescription());
					pStmt.setString(6, userId);
					pStmt.setTimestamp(7, ts);
					pStmt.setString(8, userId);
					pStmt.setTimestamp(9, ts);
					pStmt.executeUpdate();
				}
			}

		} catch (Exception e) {
			logger.error(
					"savePODeliverables(PurchaseOrder, String, DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}

	}

	public static boolean isPODeliverablesExist(String powoId, DataSource ds) {
		boolean exist = false;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select 1 from podb_purchase_order_deliverables where lm_po_id = "
					+ powoId;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				exist = true;
			}
		} catch (Exception e) {
			logger.error("isPODeliverablesExist(String, DataSource)", e);
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error("isPODeliverablesExist(String, DataSource)", e);
			}

		}
		return exist;
	}

	public static String getPOContact(String pocId, DataSource ds) {
		String pocContact = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select poc_name = dbo.func_cc_poc_name(" + pocId
					+ "),poc_email = dbo.func_cc_poc_email(" + pocId
					+ "),poc_cell = dbo.func_cc_poc_cell(" + pocId + ")";
			if (logger.isDebugEnabled()) {
				logger.debug("getPOContact(String, DataSource) - sql: " + sql);
			}
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				if (rs.getString("poc_name") != null
						&& !rs.getString("poc_name").trim().equals("")) {
					pocContact = pocContact + rs.getString("poc_name").trim();
				}
				if (rs.getString("poc_email") != null
						&& !rs.getString("poc_email").trim().equals("")) {
					pocContact = pocContact + ", "
							+ rs.getString("poc_email").trim();
				}
				if (rs.getString("poc_cell") != null
						&& !rs.getString("poc_cell").equals("")) {
					pocContact = pocContact + ", "
							+ rs.getString("poc_cell").trim();
				}
			}
		} catch (Exception e) {
			logger.error("getPOContact(String, DataSource)", e);
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error("getPOContact(String, DataSource)", e);
			}

		}
		return pocContact;
	}

	public static void deletePODeliverable(PODTO bean, String loginuserid,
			DataSource ds) {

		if (logger.isDebugEnabled()) {
			logger.debug("deletePODeliverable(PODTO, String, DataSource) - inside delete");
		}
		PreparedStatement pStmt = null;
		Connection conn = null;
		String sql = "";
		try {
			conn = ds.getConnection();
			sql = "delete from podb_purchase_order_deliverables where lm_po_id= ? and podb_mn_dl_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, bean.getPoId());
			pStmt.setString(2, bean.getDevId());
			pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error("deletePODeliverable(PODTO, String, DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	public static void deletePODeliverable(String devId, DataSource ds) {

		if (logger.isDebugEnabled()) {
			logger.debug("deletePODeliverable(String, DataSource) - inside delete");
		}
		PreparedStatement pStmt = null;
		Connection conn = null;
		String sql = "";
		try {
			conn = ds.getConnection();
			sql = "delete from podb_purchase_order_deliverables where  podb_mn_dl_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, devId);
			pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error("deletePODeliverable(String, DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	public static void savePODeliverables(PODTO bean, String loginuserid,
			DataSource ds) {
		PreparedStatement pStmt = null;
		Connection conn = null;
		String sql = "";
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		try {
			conn = ds.getConnection();
			sql = "update dbo.podb_purchase_order_deliverables "
					+ " \n set podb_mn_dl_title=?,"
					+ " \n podb_mn_dl_type = ?," + " \n podb_mn_dl_format = ?,"
					+ " \n podb_mn_dl_desc = ?,"
					+ " \n podb_mn_dl_updated_by = ?,"
					+ " \n podb_mn_dl_updated_date = ?,"
					+ " \n podb_mn_dl_status = ?,"
					+ " \n podb_mn_mobile_upload_allowed = ?"
					+ "\n where lm_po_id = ? and podb_mn_dl_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, bean.getDevTitle());
			pStmt.setString(2, bean.getDevType());
			pStmt.setString(3, bean.getDevFormat());
			pStmt.setString(4, bean.getDevDescription());
			pStmt.setString(5, loginuserid);
			pStmt.setTimestamp(6, ts);
			pStmt.setString(7, PODeliverableStatusTypes.STATUS_PENDING + "");
			pStmt.setString(8, String.valueOf(bean.getMobileUploadAllowed()));
			if (bean.getDevId().trim().equalsIgnoreCase("")) {
				pStmt.setString(9, "-1");
				pStmt.setString(10, "-1");
			} else {
				pStmt.setString(9, bean.getPoId());
				pStmt.setString(10, bean.getDevId());
			}
			if (logger.isDebugEnabled()) {
				logger.debug("savePODeliverables(PODTO, String, DataSource) - "
						+ sql);
			}
			// int result = stmt.executeUpdate(sql);
			int result = pStmt.executeUpdate();
			if (logger.isDebugEnabled()) {
				logger.debug("savePODeliverables(PODTO, String, DataSource) - result"
						+ result);
			}
			if (result <= 0) {
				sql = "insert into dbo.podb_purchase_order_deliverables (lm_po_id,podb_mn_dl_title,podb_mn_dl_type,"
						+ "podb_mn_dl_format,podb_mn_dl_desc,podb_mn_dl_created_by,podb_mn_dl_created_date,podb_mn_dl_updated_by,podb_mn_dl_updated_date,podb_mn_dl_status,podb_mn_mobile_upload_allowed) values"
						+ "\n (?,?,?,?,?,?,?,?,?,?,?)";

				if (logger.isDebugEnabled()) {
					logger.debug("savePODeliverables(PODTO, String, DataSource) - poid----"
							+ bean.getPoId()
							+ "devtitl---"
							+ bean.getDevTitle());
				}

				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, bean.getPoId());
				pStmt.setString(2, bean.getDevTitle());
				pStmt.setString(3, bean.getDevType());
				pStmt.setString(4, bean.getDevFormat());
				pStmt.setString(5, bean.getDevDescription());
				pStmt.setString(6, loginuserid);
				pStmt.setTimestamp(7, ts);
				pStmt.setString(8, loginuserid);
				pStmt.setTimestamp(9, ts);
				pStmt.setString(10, PODeliverableStatusTypes.STATUS_PENDING
						+ "");
				pStmt.setString(11,
						String.valueOf(bean.getMobileUploadAllowed()));
				pStmt.executeUpdate();
			}

		} catch (Exception e) {
			logger.error("savePODeliverables(PODTO, String, DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	public static void savePODeliverablesDocs(PODTO bean, String loginuserid,
			DataSource ds) {
		PreparedStatement pStmt = null;
		Connection conn = null;
		String sql = "";
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		try {
			conn = ds.getConnection();
			sql = "update dbo.podb_purchase_order_deliverables "
					+ " \n set podb_mn_dl_doc_id = ?,"
					+ " \n podb_mn_dl_status = ?,"
					+ " \n podb_mn_dl_updated_by = ?,"
					+ " \n podb_mn_dl_updated_date = ?"
					+ "\n where lm_po_id = ? and podb_mn_dl_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, bean.getPoDocId());
			pStmt.setString(2, PODeliverableStatusTypes.STATUS_UPLOADED + "");
			pStmt.setString(3, loginuserid);
			pStmt.setTimestamp(4, ts);
			pStmt.setString(5, bean.getPoId());
			pStmt.setString(6, bean.getDevId());

			if (logger.isDebugEnabled()) {
				logger.debug("savePODeliverablesDocs(PODTO, String, DataSource) - "
						+ sql);
			}
			pStmt.executeUpdate();

		} catch (Exception e) {
			logger.error("savePODeliverablesDocs(PODTO, String, DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	public static POActivityManager getPOResources(String poId, DataSource ds) {
		POActivityManager poActivityManager = new POActivityManager();
		PreparedStatement pStmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String sql = "";
		ArrayList<POResource> poResourcesList = new ArrayList();

		try {
			conn = ds.getConnection();
			sql = "select * from dbo.func_lm_po_wo_resource_tab( ? )";
			if (logger.isDebugEnabled()) {
				logger.debug("getPOResources(String, DataSource) - getPOResources = select * from dbo.func_lm_po_wo_resource_tab( "
						+ poId + " )");
			}
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, poId);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				POResource poResource = new POResource();
				poResource.setResourceId(rs.getString("resource_id"));
				poResource.setActivityId(rs.getString("activity_id"));
				poResource.setActivityName(rs.getString("activity_name"));
				poResource.setResourceName(rs.getString("resource_name"));
				poResource
						.setRevision((rs.getString("revision").equals("0")) ? ""
								: rs.getString("revision"));
				if (logger.isDebugEnabled()) {
					logger.debug("getPOResources(String, DataSource) - poResource: "
							+ poResource.getRevision());
				}
				poResource
						.setActivityQuantity(rs.getFloat("activity_quantity"));
				poResource.setTotalQuantity(rs.getFloat("total_quantity"));
				poResource
						.setResourceQuantity(rs.getFloat("resource_quantity"));
				poResource.setEstimatedCostUnit(rs
						.getFloat("estimated_cost_unit"));
				poResource.setEstimatedCostTotal(rs
						.getFloat("estimated_cost_total"));
				poResource.setAsPlannedQuantity(rs
						.getFloat("as_planned_quantity"));
				poResource.setAsPlannedUnitCost(rs
						.getFloat("as_planned_unit_cost"));
				poResource.setAsplannedSubTotal(rs
						.getFloat("as_planned_sub_total"));
				poResource.setShowOnWO(TicketDAO.getBoolean(rs
						.getString("is_show_on_wo")));
				poResource
						.setResourceSubName(rs.getString("resource_sub_name"));
				poResource.setType(rs.getString("resource_type"));
				poResource.setInTransit(rs.getString("in_transit"));
				poResourcesList.add(poResource);
			}
			poActivityManager.setPoResources(poResourcesList);
		} catch (Exception e) {
			logger.error("getPOResources(String, DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		return poActivityManager;
	}

	public static boolean isCopiedFromMPO(String powoId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		boolean copiedFromMPO = false;
		try {
			conn = ds.getConnection();
			String sql = "select isnull(mpo_id,-1) as mpo_id from lm_purchase_order where lm_po_id = "
					+ powoId;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				if (rs.getString("mpo_id") != null
						&& !rs.getString("mpo_id").trim().equals("-1")) {
					copiedFromMPO = true;
				}
			}
		} catch (Exception e) {
			logger.error("isCopiedFromMPO(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return copiedFromMPO;

	}

	/**
	 * Set Document to Superseded.
	 * 
	 * @param powoId
	 * @param ds
	 */
	public static void setDocumentSuperseded(String powoId, DataSource ds) {
		String[] powoDocIds = getDocIds(powoId, ds);

		if (powoDocIds[0] != null) {
			try {
				ClientOperator.setDocumentToDraft(powoDocIds[0]);
			} catch (Exception e) {
				logger.error("setDocumentSuperseded(String, DataSource)", e);
			}
		}
		if (powoDocIds[1] != null) {
			try {
				ClientOperator.setDocumentToDraft(powoDocIds[1]);
			} catch (Exception e) {
				logger.error("setDocumentSuperseded(String, DataSource)", e);
			}
		}
	} // - end of setDocumentSuperseded()

	/**
	 * Get both Po and Wo doc id from lm_purchase_order.
	 * 
	 * @param powoId
	 * @param loginuserid
	 * @param ds
	 * @return
	 */
	public static String[] getDocIds(String powoId, DataSource ds) {
		PreparedStatement pStmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String sql = "";
		String[] powoDocIds = new String[2];
		try {
			conn = ds.getConnection();
			sql = "select lm_po_doc_id,lm_po_wo_doc_id from lm_purchase_order where lm_po_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, powoId);
			rs = pStmt.executeQuery();

			while (rs.next()) {
				powoDocIds[0] = rs.getString("lm_po_doc_id");
				powoDocIds[1] = rs.getString("lm_po_wo_doc_id");
			}
		} catch (Exception e) {
			logger.error("getDocIds(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDocIds(String, DataSource) - Exception in com.mind.newjobdb.dao.getDocIds()");
			}
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return powoDocIds;
	} // - end of getDocIds()

	/**
	 * Discription: This method is check Undo Action is allowed for a PO or not.
	 * 
	 * @param powoId
	 * @return
	 */
	public static boolean isUndoAllowed(String powoId, int action, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		boolean isAllowed = true;
		try {
			conn = ReportsDao.getStaticConnection();
			String sql = "select dbo.func_lm_po_undo_allowed(?,?) ";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, powoId);
			pStmt.setInt(2, action);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				if (rs.getString(1).equalsIgnoreCase("false")) {
					isAllowed = false;
				}

			}
		} catch (Exception e) {
			logger.error("isUndoAllowed(String, int, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("isUndoAllowed(String, int, DataSource) - BBBBBBBBBBBBBBBBBBBBB= "
					+ isAllowed);
		}
		return isAllowed;
	}

	/**
	 * Discription: This method updates the Po type to fixes Price PO if Project
	 * is (type 1 and type 2 appendixes.).
	 * 
	 * @param powoId
	 * @return
	 */
	public static void updatePurchaseOrder(String poId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		Statement stmt = null;
		ResultSet rs1 = null;
		try {
			conn = ds.getConnection();

			stmt = conn.createStatement();
			rs1 = stmt
					.executeQuery("select * from lm_purchase_order inner join lm_job on lm_js_id =  lm_po_js_id"
							+ " inner join lx_appendix_main on lx_pr_id = lm_js_pr_id"
							+ " where lm_po_id ="
							+ poId
							+ " and lx_pr_type in (1,2)");
			if (rs1.next()) {
				pStmt = conn
						.prepareStatement("update lm_purchase_order set lm_po_pwo_type_id = 2 where lm_po_id = ?");
				pStmt.setInt(1, Integer.parseInt(poId));
				pStmt.executeUpdate();
			}
		} catch (Exception e) {
			logger.error("updatePurchaseOrder(String, int, DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(rs1, stmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * This method is use to update the PO's Partner distance and duration from
	 * site location.
	 * 
	 * @param distance
	 *            the distance
	 * @param duration
	 *            the duration
	 * @param poId
	 *            the po id
	 * @param ds
	 *            the ds
	 */
	public static void updatePOInfo(String distance, String duration,
			String poId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		System.out.println("distance=== " + distance);
		try {
			conn = ds.getConnection();
			pStmt = conn
					.prepareStatement("update lm_purchase_order set lm_po_job_travel_distance  = ?,"
							+ " lm_po_job_travel_duration  = ? where lm_po_id = ?");
			pStmt.setInt(1, Integer.parseInt(distance));
			pStmt.setInt(2, Integer.parseInt(duration));
			pStmt.setInt(3, Integer.parseInt(poId));
			pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error("updatePOInfo(String, String, String, DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}

	}

	public static boolean isPoDeliverableExistForMobile(String pOId,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		boolean isAllowed = false;
		try {
			conn = ds.getConnection();
			pStmt = conn
					.prepareStatement("select podb_mn_mobile_upload_allowed from podb_purchase_order_deliverables where lm_po_id =  ?");
			pStmt.setInt(1, Integer.parseInt(pOId));
			rs = pStmt.executeQuery();
			while (rs.next()) {
				if (("y").equals(rs.getString("podb_mn_mobile_upload_allowed"))) {
					isAllowed = true;
					return isAllowed;
				}

			}

		} catch (Exception e) {
			logger.error("isPoDeliverableExistForMobile(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return isAllowed;

	}

	public static int getParnterId(String powoId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int parnterId = -1;
		try {
			conn = ds.getConnection();
			String sql = "select lm_po_partner_id from lm_purchase_order where lm_po_id = "
					+ powoId;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				parnterId = Integer.parseInt(rs.getString("lm_po_partner_id"));
			}
		} catch (Exception e) {
			logger.error("getParnterId(String powoId, DataSource ds)", e);
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error("getParnterId(String powoId, DataSource ds)", e);
			}

		}
		return parnterId;
	}

	public static String getAuthString(String parnterId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String authString = "";
		try {
			conn = ds.getConnection();
			String sql = "select lm_pa_authentication from lm_partner_authentication where lm_pa_partner_id = "
					+ parnterId;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				authString = rs.getString("lm_pa_authentication");
			}
		} catch (Exception e) {
			logger.error(" getAuthString(String parnterId, DataSource ds)", e);
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error(" getAuthString(String parnterId, DataSource ds)",
						e);
			}

		}
		return authString;
	}

	public static String getPoId(String jobId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String poId = "";
		try {
			conn = ds.getConnection();
			String sql = "select lm_po_id from lm_purchase_order where lm_po_js_id ="
					+ jobId;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				poId = rs.getString("lm_po_id");
			}
		} catch (Exception e) {
			logger.error(" getPoId(String jobId, DataSource ds)", e);
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error(" getPoId(String jobId, DataSource ds)", e);
			}

		}
		return poId;
	}

	/**
	 * Insert default deliverables when a PO is created.
	 * 
	 * @param poId
	 *            the po id
	 * @param title
	 *            the title
	 * @param userId
	 *            the user id
	 * @param ds
	 *            the ds
	 */
	public static void insertDefaultPODeliverable(String poId, String title,
			String deliverableDesc, String userId, DataSource ds) {
		PreparedStatement pStmt = null;
		Connection conn = null;
		String sql = "";
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		try {
			conn = ds.getConnection();
			sql = "insert into dbo.podb_purchase_order_deliverables (lm_po_id,podb_mn_dl_title,podb_mn_dl_type,"
					+ "podb_mn_dl_format,podb_mn_dl_desc,podb_mn_dl_created_by,podb_mn_dl_created_date,podb_mn_dl_updated_by,podb_mn_dl_updated_date,podb_mn_dl_status,podb_mn_mobile_upload_allowed) values"
					+ "\n (?,?,?,?,?,?,?,?,?,?,?)";

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, poId);
			pStmt.setString(2, title);
			pStmt.setString(3, "Customer");
			pStmt.setString(4, "Image");
			pStmt.setString(5, deliverableDesc);
			pStmt.setString(6, userId);
			pStmt.setTimestamp(7, ts);
			pStmt.setString(8, userId);
			pStmt.setTimestamp(9, ts);
			pStmt.setString(10, PODeliverableStatusTypes.STATUS_PENDING + "");
			pStmt.setString(11, "y");
			pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error(
					"insertDefaultPODeliverable(String poId,String title,String userId, DataSource ds)",
					e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}

	}

	public static void deleteCoreCompetencyData(String poId, Connection conn) {
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			String sql = "delete from lm_gc_core_competency where lm_gc_po_id=?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, Integer.parseInt(poId));
			pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error("deleteCoreCompetencyData for purchase order", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

	public static void insertCoreCompetancyData(PurchaseOrder purchaseOrder,
			Connection connection) {
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			String sqlQuey = " insert into lm_gc_core_competency "
					+ " (lm_gc_po_id,lm_gc_corecomp_id) values(?,?) ";
			pStmt = connection.prepareStatement(sqlQuey);
			for (String coreCompetancyValue : purchaseOrder
					.getPoAuthorizedCostInfo().getPoGeneralInfo()
					.getCmboxCoreCompetency()) {
				pStmt.setInt(1, Integer.parseInt(purchaseOrder.getPoId()));
				pStmt.setInt(2, Integer.parseInt(coreCompetancyValue));
				pStmt.addBatch();
			}
			pStmt.executeBatch();
		} catch (Exception e) {
			logger.error("insertCoreCompetancyData()", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(connection);
		}
	}

	public static String getJobId(String poId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String jobId = "";
		try {
			conn = ds.getConnection();
			String sql = "select lm_po_js_id from lm_purchase_order where lm_po_id ="
					+ poId;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				jobId = rs.getString("lm_po_js_id");
			}
		} catch (Exception e) {
			logger.error(" getJobId(String jobId, DataSource ds)", e);
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error(" getJobId(String jobId, DataSource ds)", e);
			}

		}
		return jobId;
	}

}