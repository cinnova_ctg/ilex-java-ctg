package com.mind.newjobdb.dao;

import java.io.Serializable;

import com.mind.bean.newjobdb.POActivityManager;





public class POAuthorizedCostInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private POGeneralInfo poGeneralInfo;
	private POActivityManager poActivityManager;
	
	public POActivityManager getPoActivityManager() {
		return poActivityManager;
	}
	public void setPoActivityManager(POActivityManager poActivityManager) {
		this.poActivityManager = poActivityManager;
	}
	public POGeneralInfo getPoGeneralInfo() {
		return poGeneralInfo;
	}
	public void setPoGeneralInfo(POGeneralInfo poGeneralInfo) {
		this.poGeneralInfo = poGeneralInfo;
	}
	
}
