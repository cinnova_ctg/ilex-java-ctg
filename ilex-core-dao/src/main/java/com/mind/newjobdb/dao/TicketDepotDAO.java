package com.mind.newjobdb.dao;

import java.util.ArrayList;

import com.mind.bean.newjobdb.TicketDepotProduct;
import com.mind.bean.newjobdb.TicketDepotRequisition;

/**
 * @purpose This class is used to interact with the  
 * 			the TICKET Depot database tables.
 */
public class TicketDepotDAO {
	public ArrayList<TicketDepotProduct> getTicketDepotProducts () {
		return null;
	}
	
	public TicketDepotRequisition getTicketDepotRequisition(long ticketId) {
		return null;
	}
	
	public void addQuantity(long ticketId, 
			TicketDepotRequisition ticketDepotRequisition) {

	}
}
