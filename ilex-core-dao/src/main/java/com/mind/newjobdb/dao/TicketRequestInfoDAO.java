package com.mind.newjobdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.newjobdb.ChangeTicketRequestTypeBean;
import com.mind.bean.newjobdb.ResourceLevel;
import com.mind.common.LabelValue;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;

/**
 * @purpose This class is used to interact with the the TICKET Request Info
 *          related database tables.
 */
public class TicketRequestInfoDAO {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(TicketRequestInfoDAO.class);

	/**
	 * Discription: This mehtod is used to fill Requestors list.
	 * 
	 * @param msaId
	 *            -- Msa Id
	 * @param ds
	 *            -- Database Object
	 * @return list of Requestors.
	 */
	static DataSource dsStatic = null;

	/**
	 * Description - This method is a public interface to set Datasource object
	 * as a static object.
	 * 
	 * @param ds
	 *            - DataSource Object. return - None.
	 */
	public static void setDataSource(DataSource ds) {
		dsStatic = ds;
	}

	public static ArrayList<LabelValue> getRequestors(long msaId, DataSource ds) {
		dsStatic = ds;
		ArrayList<LabelValue> poccombo = new ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		poccombo.add(new com.mind.common.LabelValue("---Select---", "0"));
		String lo_ot_id = DatabaseUtilityDao.getLoOtId(msaId, ds);

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from dbo.func_search_list('" + lo_ot_id
					+ "','C','%', '%%', 'requestor_search')";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				poccombo.add(new com.mind.common.LabelValue(rs.getString(2), rs
						.getString(2) + "~" + rs.getString(3)));
			}
		} catch (Exception e) {
			logger.error("getRequestors(long, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRequestors(long, DataSource) - Exception caught in rerieving get Organization POC"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return poccombo;
	} // - End of getRequestors() mehtod

	/**
	 * Discription: This method is used to get list of Request type
	 * 
	 * @param ds
	 *            -- DataSource Object
	 * @return List of Request Type.
	 */
	public static ArrayList<LabelValue> getRequestTypes(DataSource ds) {

		ArrayList<LabelValue> requestname = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		requestname.add(new com.mind.common.LabelValue("Select", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select * from lm_request_type");
			while (rs.next()) {
				requestname.add(new com.mind.common.LabelValue(rs
						.getString("lm_request_type"), rs
						.getString("lm_request_id")));
			}
		} catch (Exception e) {
			logger.error("getRequestTypes(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRequestTypes(DataSource) - Exception in com.mind.newjobdb.dao.getRequestTypes()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return requestname;
	} // - end of getRequestTypes() method

	/**
	 * Discription: This mehtod is used to fill Criticalities list.
	 * 
	 * @param msaId
	 *            -- Msa Id
	 * @param ds
	 *            -- DataSource Object
	 * @return list of Criticalities.
	 */
	public static ArrayList<LabelValue> getCriticalities(long msaId,
			DataSource ds) {

		ArrayList<LabelValue> criticalityname = new ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		criticalityname.add(new com.mind.common.LabelValue("Select", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select * from dbo.func_lo_criticality_name('"
							+ msaId + "')");
			while (rs.next()) {
				criticalityname.add(new com.mind.common.LabelValue(rs
						.getString("lo_call_criticality_des"), rs
						.getString("lo_call_criticality_id")));
			}
		} catch (Exception e) {
			logger.error("getCriticalities(long, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCriticalities(long, DataSource) - Exception in com.mind.newjobdb.dao.getCriticalities()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return criticalityname;
	} // - end of getCriticalities() method

	/**
	 * Discripiton: This method return list of Resources.
	 * 
	 * @param msaId
	 *            -- Msa Id
	 * @param criticalityId
	 *            -- Criticality Id
	 * @param isPPS
	 *            -- True/False
	 * @param ds
	 *            -- DataSource Object
	 * @return lsit of Resources.
	 */

	// public static ArrayList<LabelValue> getResources(long appendixId, String
	// criticalityId, boolean isPPS, DataSource ds) {
	// if(dsStatic != null){
	// ds = dsStatic ;
	// }
	// ArrayList resourcelevel = new java.util.ArrayList();
	// Connection conn = null;
	// Statement stmt = null;
	// ResultSet rs = null;
	// resourcelevel.add( new com.mind.common.LabelValue( "Select" , "0") );
	// String popValue = TicketDAO.getPPSValue(criticalityId, isPPS, ds);
	//
	//
	// try{
	// conn = ds.getConnection();
	// stmt = conn.createStatement();
	// rs = stmt.executeQuery(
	// "select * from dbo.func_lo_resource_level('"+appendixId+"',"+popValue+")"
	// );
	// while( rs.next() ){
	// resourcelevel.add( new LabelValue( rs.getString( 2 ) , rs.getString( 1 )
	// ) );
	// }
	// }
	// catch( Exception e ){
	// logger.error("getResources(long, String, boolean, DataSource)", e);
	//
	// if (logger.isDebugEnabled()) {
	// logger.debug("getResources(long, String, boolean, DataSource) - Exception in com.mind.newjobdb.dao.getResources()"
	// + e);
	// }
	// }
	// finally{
	// DatabaseUtilityDao.close(conn);
	// DatabaseUtilityDao.close(stmt);
	// DatabaseUtilityDao.closeResultSet(rs);
	// }
	// return resourcelevel;
	// } // - end of getResources() method

	public static ArrayList<ResourceLevel> getResources(
			Map<String, Object> map, long appendixId, String criticalityId,
			boolean isPPS, DataSource ds) {
		if (dsStatic != null) {
			ds = dsStatic;
		}
		ResourceLevel resourcelevel = new ResourceLevel();
		resourcelevel.setOptionGroup(getOptionGroupLebal(appendixId,
				criticalityId, ds));
		String popValue = TicketDAO.getPPSValue(criticalityId, isPPS, ds);

		if (resourcelevel.getOptionGroup().size() > 0) {
			for (int i = 0; i < resourcelevel.getOptionGroup().size(); i++) {
				String classResName = ((ResourceLevel) resourcelevel
						.getOptionGroup().get(i)).getResName();
				((ResourceLevel) resourcelevel.getOptionGroup().get(i))
						.setResourceList(getResourceList(i, appendixId,
								popValue, classResName, ds));
			}
		}
		map.put("resourceList", resourcelevel.getOptionGroup());
		return resourcelevel.getOptionGroup();
	} // - end of getResources() method

	public static ArrayList<ResourceLevel> getOptionGroupLebal(long appendixId,
			String criticalityId, DataSource ds) {
		if (dsStatic != null) {
			ds = dsStatic;
		}
		ArrayList<ResourceLevel> resourcelevel = new ArrayList<ResourceLevel>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		// resourcelevel.add( new com.mind.common.LabelValue( "Select" , "0") );
		int i = 0;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select distinct lm_tr_class_name = isnull(lm_tr_class_name,'') from lm_ticket_reslevel");
			while (rs.next()) {
				if (i == 0) {
					ResourceLevel optionGroup = new ResourceLevel();
					optionGroup.setResName(" ");
					resourcelevel.add(i, optionGroup);
					i++;
				}
				ResourceLevel optionGroup = new ResourceLevel();
				optionGroup.setResName(rs.getString(1));
				resourcelevel.add(i, optionGroup);
				i++;
			}
		} catch (Exception e) {
			logger.error("getResources(long, String, boolean, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResources(long, String, boolean, DataSource) - Exception in com.mind.newjobdb.dao.getResources()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return resourcelevel;
	} // - end of getResources() method

	/**
	 * @param appendixId
	 * @param popValue
	 * @param classResName
	 * @param ds
	 * @return
	 */
	public static ArrayList<LabelValue> getResourceList(int i, long appendixId,
			String popValue, String classResName, DataSource ds) {

		ArrayList<LabelValue> resoucelist = new ArrayList<LabelValue>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		if (i == 0)
			resoucelist.add(new com.mind.common.LabelValue("Select", "0"));

		int j = 0;
		try {
			conn = ds.getConnection();
			String sql = "select * from dbo.func_lo_resource_level(?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, appendixId);
			pstmt.setString(2, popValue);
			pstmt.setString(3, classResName);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				// System.out.println("resource list= "+rs.getString("resource_name")+", "+
				// rs.getString( "iv_rp_id" ) );
				resoucelist.add(
						j,
						new com.mind.common.LabelValue(rs
								.getString("resource_name"), rs
								.getString("iv_rp_id")));
				j++;
			}
		} catch (Exception e) {
			logger.error("getResourceList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourceList(String, DataSource) - Exception in com.mind.newjobdb.dao.getResourceList()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return resoucelist;
	}

	public void findSite() {

	}

	/**
	 * This method get current criticality for a ticket.
	 * 
	 * @param jobId
	 * @param ds
	 * @return ticketData
	 */
	public static String[] getTicketData(String jobId, DataSource ds) {
		/*
		 * ticketData[0] - Criticality Level ticketData[1] - Resource Level
		 * ticketData[2] - Help Desk
		 */

		String[] ticketData = new String[3];
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			String sql = "select lm_tc_criticality,"
					+ "lm_tc_resource_level, "
					+ "lm_tc_help_desk = isnull(lm_tc_help_desk,'N')"
					+ "from lm_ticket inner join lm_job on lm_js_id = lm_tc_js_id "
					+ "where lm_js_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, jobId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				ticketData[0] = rs.getString("lm_tc_criticality");
				ticketData[1] = rs.getString("lm_tc_resource_level");
				ticketData[2] = rs.getString("lm_tc_help_desk").trim();
			}
		} catch (Exception e) {
			logger.error("getTicketData(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTicketData(String, DataSource) - Exception in com.mind.newjobdb.dao.getCriticality()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return ticketData;
	} // - end of getCriticalities() method

	public static void getRequestType(String jobId,
			ChangeTicketRequestTypeBean from, DataSource ds) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			String sql = "select lm_tc_request_type from lm_ticket where lm_tc_js_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, jobId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				if (rs.getString(1) != null) {
					from.setRequestType(rs.getString(1));

				} else {
					from.setRequestType("0");
				}
			}
		} catch (Exception e) {
			logger.error(
					"getRequestType(String, ChangeTicketRequestTypeForm, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRequestType(String, ChangeTicketRequestTypeForm, DataSource) - Exception in com.mind.newjobdb.dao.getRequestType()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}

	} // - end of getCriticalities() method

	/**
	 * Save criticality and resource level.
	 * 
	 * @param jobId
	 * @param criticality
	 * @param resourcelevel
	 * @param loginuserid
	 * @param ds
	 * @return
	 */
	public static int updateCricalityResource(String jobId, String criticality,
			String resourcelevel, String loginuserid, String helpDesk,
			DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		int flag = -1;
		/*
		 * System.out.println("jobId ="+jobId);
		 * System.out.println("criticality ="+criticality);
		 * System.out.println("resourcelevel ="+resourcelevel);
		 * System.out.println("loginuserid ="+loginuserid);
		 * System.out.println("cat_id ="+TicketDAO.getPPSValue(criticality,
		 * true, ds));
		 */
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_ticket_criticality_resource_manage_01(?,?,?,?,?,	?)}"); // -
																										// 6
																										// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, jobId);
			cstmt.setString(3, criticality);
			cstmt.setString(4, resourcelevel);
			cstmt.setString(5, loginuserid);
			cstmt.setString(6, TicketDAO.getPPSValue(criticality, true, ds));
			cstmt.setString(7, helpDesk);
			cstmt.execute();
			flag = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"updateCricalityResource(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateCricalityResource(String, String, String, String, String, DataSource) - Exception in com.mind.newjobdb.dao.updateTicket()"
						+ e);
			}
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return flag;
	} // - end of updateTicket() method

	/**
	 * Save Request Type of Ticket.
	 * 
	 * @param requestType
	 * @param jobId
	 * @param loginuserid
	 * @param ds
	 * @return
	 */
	public static int updateRequestType(String requestType, String jobId,
			String loginuserid, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		int flag = -1;

		/*
		 * System.out.println("requestType ="+requestType);
		 * System.out.println("jobId ="+jobId);
		 * System.out.println("loginuserid ="+loginuserid);
		 * System.out.println("cat_id ============================================"
		 * );
		 */

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_ticket_requesttype_manage_01(?,?,?)}"); // -
																						// 3
																						// parameter
																						// +
																						// on
																						// out
																						// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, requestType);
			cstmt.setString(3, jobId);
			cstmt.setString(4, loginuserid);
			cstmt.execute();
			flag = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"updateRequestType(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("updateRequestType(String, String, String, DataSource) - Exception in com.mind.newjobdb.dao.updateRequestType()"
						+ e);
			}
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);
		}
		return flag;
	} // - end of updateTicket() method

	// Return name of

	public static String getRequestTypesWithName(DataSource ds, String type) {

		String requestname = new String();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		// requestname.add(new com.mind.common.LabelValue("Select", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select lm_request_type from lm_request_type  WHERE lm_request_id="
							+ type);
			while (rs.next()) {
				requestname = rs.getString("lm_request_type");

			}
		} catch (Exception e) {
			logger.error("getRequestTypes(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRequestTypes(DataSource) - Exception in com.mind.newjobdb.dao.getRequestTypes()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return requestname;
	} // - end of getReque

	// get crtical name
	public static String getCriticalitiesname(String Id, DataSource ds) {

		String criticalityname = new String();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT   iv_crit_name  from iv_criticality"
					+ " WHERE iv_crit_id=" + Id);
			while (rs.next()) {
				criticalityname = rs.getString("iv_crit_name");

			}
		} catch (Exception e) {
			logger.error("getCriticalitiesname(long, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCriticalitiesname(long, DataSource) - Exception in com.mind.newjobdb.dao.getCriticalities()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return criticalityname;
	} // - end of

	public static String getProblemCategoriesname(DataSource ds, String Id) {
		String problemcategory = new String();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("SELECT cc_pc_desc  from cc_problem_category_detail where cc_pc_status  = 'A' AND cc_pc_id="
							+ Id);
			while (rs.next()) {
				problemcategory = rs.getString("cc_pc_desc");

			}
		} catch (Exception e) {
			logger.error("getProblemCategoriesname(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getProblemCategoriesname(DataSource) - Exception in com.mind.newjobdb.dao.getProblemCategory()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return problemcategory;
	} // - end of getProblemCategory() method
		// getting values of email from lx_appendix contact table

	public static ArrayList<LabelValue> getAdditionalReciptant(DataSource ds) {

		ArrayList<LabelValue> requestname = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		requestname.add(new com.mind.common.LabelValue("Select", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select lx_pcc_name,lx_pcc_email from lx_appendix_client_contact");
			while (rs.next()) {
				requestname
						.add(new com.mind.common.LabelValue(rs
								.getString("lx_pcc_email"), rs
								.getString("lx_pcc_name")));
			}
		} catch (Exception e) {
			logger.error("getAdditionalReciptant(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAdditionalReciptant(DataSource) - Exception in getAdditionalReciptant()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return requestname;
	} // - end of getRequestTypes() method

	// LATEST FUNCTION
	public static ArrayList<LabelValue> getClientEmailByType(String appendixId,
			String type, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<LabelValue> list = new java.util.ArrayList<LabelValue>();
		;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "SELECT lx_pcc_name, lx_pcc_email "
					+ " FROM lx_appendix_client_contact  "
					+ " WHERE lx_pcc_type = '" + type + "' "
					+ " AND lx_pcc_email IS NOT NULL  "
					+ " AND lx_pcc_appendix_id =  " + appendixId;
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				list.add(new LabelValue(rs.getString("lx_pcc_name") + ": "
						+ rs.getString("lx_pcc_email"), rs
						.getString("lx_pcc_email")));
			}

		} catch (Exception e) {
			logger.error("getClientEmailByType(String, String , DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getClientEmailByType(String, String , DataSource) - Error occured during getting current status  ");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getClientEmailByType(String, String , DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getClientEmailByType(String, String , DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getClientEmailByType(String, String , DataSource) - exception ignored",
						e);
			}

		}
		return list;
	}

}
