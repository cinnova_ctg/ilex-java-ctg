package com.mind.newjobdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.newjobdb.CostAnalysis;
import com.mind.bean.newjobdb.FinancialAnalysis;
import com.mind.fw.core.dao.util.DBUtil;

public class FinancialAnalysisDAO {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(FinancialAnalysisDAO.class);

	/**
	 * Discription: Get Revenue and VGPM section data of Job Financial Tab from
	 * database.
	 * 
	 * @param financialAnalysis
	 *            - Object of FinancialAnalysis
	 * @param jobId
	 *            - Job Id
	 * @param ds
	 *            - DataSource Object
	 * @return Object of FinancialAnalysis
	 */
	public static FinancialAnalysis setRevenueVGPM(
			FinancialAnalysis financialAnalysis, String jobId, DataSource ds) {

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			String sql = "select * from func_lm_js_financial_revenue_vgpm_view_01(?)"; // -
																						// 1
																						// parameter
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobId);
			rs = pStmt.executeQuery();

			if (rs.next()) {
				financialAnalysis.setExtendedPrice(rs
						.getFloat("extended_price"));
				financialAnalysis.setJobDiscount(rs.getFloat("job_discount"));
				financialAnalysis.setInvoicePrice(rs.getFloat("invoice_price"));
				financialAnalysis.setJobReserver(rs.getFloat("job_reserve"));
				financialAnalysis
						.setProFormaVGPM(rs.getFloat("pro_farma_vgpm"));
				financialAnalysis.setActualVGPM(rs.getFloat("actual_vgpm"));
				financialAnalysis.setAppendixActualVGPM(rs
						.getFloat("actual_appendix_vgpm"));
				financialAnalysis.setActualVGPM(rs.getFloat("actual_vgpm"));
			} // - end of while
		} // - end of try
		catch (Exception e) {
			logger.error(
					"setRevenueVGPM(FinancialAnalysis, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setRevenueVGPM(FinancialAnalysis, String, DataSource) - Exception in com.mind.newjobdb.dao.FinancialAnalysisDAO.setRevenueVGPM() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);

		}
		return financialAnalysis;
	} // - end of setRevenueVGPM() method

	/**
	 * Discription: Get Cost section data of Job Financial Tab from database.
	 * 
	 * @param financialAnalysis
	 *            - Object of FinancialAnalysis
	 * @param jobId
	 *            - Job Id
	 * @param ds
	 *            - DataSource Object
	 * @return Object of FinancialAnalysis
	 */
	public static FinancialAnalysis setCost(
			FinancialAnalysis financialAnalysis, String jobId, DataSource ds) {

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			String sql = "select * from func_lm_js_financial_cost_view_01(?)"; // -
																				// 1
																				// parameter
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobId);
			rs = pStmt.executeQuery();

			if (rs.next()) {
				financialAnalysis.setCorporateLaborEstimatedCost(rs
						.getFloat("estimated_corporate_labor_cost"));
				financialAnalysis.setContractLaborEstimatedCost(rs
						.getFloat("estimated_contract_labor_cost"));
				financialAnalysis.setCorporateLaborActualCost(rs
						.getFloat("actual_corporate_labor_cost"));
				financialAnalysis.setContractLaborActualCost(rs
						.getFloat("actual_contract_labor_cost"));
				financialAnalysis.setTotalActualCost(rs
						.getFloat("total_actual_cost"));
				financialAnalysis.setTotalEstimatedCost(rs
						.getFloat("total_estimated_cost"));
				financialAnalysis.setContractFreightActualCost(rs
						.getFloat("actual_contract_frieght_cost"));
			} // - end of while
			financialAnalysis.setCorporateMaterialEstimatedCost(0.0f);
			financialAnalysis.setContractMaterialEstimatedCost(0.0f);
			financialAnalysis.setCorporateMaterialActualCost(0.0f);
			financialAnalysis.setContractMaterialActualCost(0.0f);
			financialAnalysis.setCorporateTravelEstimatedCost(0.0f);
			financialAnalysis.setContractTravelEstimatedCost(0.0f);
			financialAnalysis.setCorporateTravelActualCost(0.0f);
			financialAnalysis.setContractTravelActualCost(0.0f);
			financialAnalysis.setCorporateFreightEstimatedCost(0.0f);
			financialAnalysis.setContractFreightEstimatedCost(0.0f);
			financialAnalysis.setCorporateFreightActualCost(0.0f);

			financialAnalysis
					.setContractTotalActualCost(getTotalContractActualCost(financialAnalysis));
			financialAnalysis
					.setCorporateTotalActualCost(getTotalCorporateActualCost(financialAnalysis));
			financialAnalysis
					.setContractTotalEstimatedCost(getTotalContractEstimatedCost(financialAnalysis));
			financialAnalysis
					.setCorporateTotalEstimatedCost(getTotalCorporateEstimatedCost(financialAnalysis));
		} // - end of try
		catch (Exception e) {
			logger.error("setCost(FinancialAnalysis, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setCost(FinancialAnalysis, String, DataSource) - Exception in com.mind.newjobdb.dao.FinancialAnalysisDAO.setCost() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);

		}
		return financialAnalysis;
	} // - end of setCost() method

	public static float getTotalContractEstimatedCost(
			FinancialAnalysis financialAnalysis) {
		float totalCost = 0.0f;
		totalCost = financialAnalysis.getContractLaborEstimatedCost()
				+ financialAnalysis.getContractFreightEstimatedCost()
				+ financialAnalysis.getContractMaterialEstimatedCost()
				+ financialAnalysis.getContractTravelEstimatedCost();
		return totalCost;
	}

	public static float getTotalContractActualCost(
			FinancialAnalysis financialAnalysis) {
		float totalCost = 0.0f;
		totalCost = financialAnalysis.getContractLaborActualCost()
				+ financialAnalysis.getContractFreightActualCost()
				+ financialAnalysis.getContractMaterialActualCost()
				+ financialAnalysis.getContractTravelActualCost();
		return totalCost;
	}

	public static float getTotalCorporateEstimatedCost(
			FinancialAnalysis financialAnalysis) {
		float totalCost = 0.0f;
		totalCost = financialAnalysis.getCorporateLaborEstimatedCost()
				+ financialAnalysis.getCorporateFreightEstimatedCost()
				+ financialAnalysis.getCorporateMaterialEstimatedCost()
				+ financialAnalysis.getCorporateTravelEstimatedCost();
		return totalCost;
	}

	public static float getTotalCorporateActualCost(
			FinancialAnalysis financialAnalysis) {
		float totalCost = 0.0f;
		totalCost = financialAnalysis.getCorporateLaborActualCost()
				+ financialAnalysis.getCorporateFreightActualCost()
				+ financialAnalysis.getCorporateMaterialActualCost()
				+ financialAnalysis.getCorporateTravelActualCost();
		return totalCost;
	}

	public static float getContractCost(FinancialAnalysis financialAnalysis) {
		float contractCost = 0.0f;
		float totalCost = 0.0f;
		totalCost = financialAnalysis.getContractLaborEstimatedCost()
				+ financialAnalysis.getContractFreightEstimatedCost()
				+ financialAnalysis.getContractMaterialEstimatedCost()
				+ financialAnalysis.getContractTravelEstimatedCost();
		contractCost = (totalCost * 100) / financialAnalysis.getExtendedPrice();
		return contractCost;
	}

	public static float getCorporateCost(FinancialAnalysis financialAnalysis) {
		float corporateCost = 0.0f;
		float totalCost = 0.0f;
		totalCost = financialAnalysis.getCorporateLaborEstimatedCost()
				+ financialAnalysis.getCorporateFreightEstimatedCost()
				+ financialAnalysis.getCorporateMaterialEstimatedCost()
				+ financialAnalysis.getCorporateTravelEstimatedCost();
		corporateCost = (totalCost * 100)
				/ financialAnalysis.getExtendedPrice();
		return corporateCost;
	}

	public static float getVGP(FinancialAnalysis financialAnalysis) {
		float vgp = 0.0f;
		float totalVGP = 0.0f;
		totalVGP = 100 - (financialAnalysis.getCorporateCost()
				+ financialAnalysis.getContractCost() + financialAnalysis
				.getJobDiscount());
		vgp = totalVGP;
		return vgp;
	}

	public static float getDiscount(FinancialAnalysis financialAnalysis) {
		float discount = 0.0f;
		discount = (financialAnalysis.getDiscountCost() * 100)
				/ financialAnalysis.getExtendedPrice();
		return discount;
	}

	/**
	 * Discription: Get Labor Analysis section data of Job Financial Tab from
	 * database.
	 * 
	 * @param financialAnalysis
	 *            - Object of FinancialAnalysis
	 * @param jobId
	 *            - Job Id
	 * @param ds
	 *            - DataSource Object
	 * @return Object of FinancialAnalysis
	 */
	public static void setLaborAnalysis(FinancialAnalysis financialAnalysis,
			String jobId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList<CostAnalysis> costAnalysisList = new ArrayList<CostAnalysis>();

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_js_financial_labor_analysis_view_01(?)}"); // -
																						// 1
																						// Parameter
																						// &
																						// one
																						// out
																						// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, jobId);
			rs = cstmt.executeQuery();
			while (rs.next()) {
				CostAnalysis costAnalysis = new CostAnalysis();
				costAnalysis.setLaborResource(rs.getString("resource_name"));
				costAnalysis.setJob(rs.getFloat("job_average"));
				costAnalysis.setProject(rs.getFloat("project_average"));
				costAnalysisList.add(costAnalysis);
			}
			financialAnalysis.setCostAnalysis(costAnalysisList);

		} catch (Exception e) {
			logger.error(
					"setLaborAnalysis(FinancialAnalysis, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("setLaborAnalysis(FinancialAnalysis, String, DataSource) - Exception in com.mind.newjobdb.dao.FinancialAnalysisDAO.setLaborAnalysis() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
	} // - end of method setLaborAnalysis()

	/**
	 * Discription: Get Usage Analysis section data of Job Financial Tab from
	 * database.
	 * 
	 * @param financialAnalysis
	 *            - Object of FinancialAnalysis
	 * @param jobId
	 *            - Job Id
	 * @param ds
	 *            - DataSource Object
	 * @return Object of FinancialAnalysis
	 */
	public static void setUsagesAnalysis(FinancialAnalysis financialAnalysis,
			String jobId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_js_financial_resource_analysis(?)}"); // -
																					// 1
																					// Parameter
																					// &
																					// one
																					// out
																					// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, jobId);
			rs = cstmt.executeQuery();
			if (rs.next()) {
				financialAnalysis.setMinutemanLaborTravel(rs
						.getFloat("minuteman_actual_cost"));
				financialAnalysis.setSpeedpayLaborTravel(rs
						.getFloat("speedpay_actual_cost"));
				financialAnalysis.setMinutemanUsage(rs
						.getFloat("minuteman_usage"));
				financialAnalysis.setSpeedpayUsage(rs
						.getFloat("speedpay_usage"));
				financialAnalysis.setMinutemanCostSavings(rs
						.getFloat("minuteman_cost_savings"));
				financialAnalysis.setSpeedpayCostSavings(rs
						.getFloat("speedpay_cost_savings"));
			}

		} catch (Exception e) {
			logger.error(
					"setUsagesAnalysis(FinancialAnalysis, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("setUsagesAnalysis(FinancialAnalysis, String, DataSource) - Exception in com.mind.newjobdb.dao.FinancialAnalysisDAO.setUsagesAnalysis() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
	} // - end of method setUsagesAnalysis()

} // - End of Class
