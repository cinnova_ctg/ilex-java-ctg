package com.mind.newjobdb.dao;

import java.util.ArrayList;

import javax.sql.DataSource;

import com.mind.bean.newjobdb.GeneralPOSetup;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.MPOList;
import com.mind.bean.newjobdb.PODocument;
import com.mind.bean.newjobdb.POResource;
import com.mind.bean.newjobdb.RequiredEquipment;

public class MPOCopier {
	private GeneralPOSetup generalPOSetup;
	private MPOList mpoList;
	
	public static PurchaseOrder getPurchaseOrderFromMPO(String powoId,String mpoId,String jobId,IlexTimestamp deliverByDate, IlexTimestamp issueDate,DataSource ds) {
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		purchaseOrder.setPoId(powoId);
		purchaseOrder.setMpoId(mpoId);
		POGeneralInfo pOGeneralInfo = MPODAO.getPoGeneralInfoFromMPO(mpoId, deliverByDate, issueDate, ds);
		purchaseOrder.setPoAuthorizedCostInfo(MPOCopier.getPOAuthorizedCostInfoFromMPO(mpoId, jobId, pOGeneralInfo, ds));
		purchaseOrder.setPoDeliverables(MPOCopier.getPODeliverablesFromMPO(mpoId, pOGeneralInfo, ds));
		purchaseOrder.setPoDocumentManager(MPOCopier.getPODocumentManagerFromMPO(mpoId, pOGeneralInfo, ds));
		purchaseOrder.setPowoInfo(MPOCopier.getPWoInfoFromMPO(mpoId, pOGeneralInfo, ds));
		
		return purchaseOrder;
	}
	
	public static String validateCopyToPO(String mpoId, String jobId, DataSource ds) {
		String flag = "";
		flag = MPODAO.isValidForCopyToPO(mpoId,jobId,ds);
		return flag;
	}

	public GeneralPOSetup getGeneralPOSetup() {
		return generalPOSetup;
	}

	public void setGeneralPOSetup(GeneralPOSetup generalPOSetup) {
		this.generalPOSetup = generalPOSetup;
	}

	public MPOList getMpoList(String appendixId, DataSource ds) {
		return MPODAO.getListOfMPO(appendixId, ds);
	}
	public MPOList getMpoList() {
		return mpoList;
	}	

	public void setMpoList(MPOList mpoList) {
		this.mpoList = mpoList;
	}

	public static ArrayList<RequiredEquipment> getRequiredEquipmentFromMPO(String mpoId, DataSource ds) {		
		return MPODAO.getRequiredEquipmentFromMPO(mpoId, ds);
	}
	
	public static PoWoInfo getPWoInfoFromMPO(String mpoId,POGeneralInfo pOGeneralInfo, DataSource ds) {
		PoWoInfo pWoInfo = new PoWoInfo();
		pWoInfo.setPoGeneralInfo(pOGeneralInfo);
		pWoInfo.setPoSpecialConditions(MPODAO.getPoSpecialConditionsFromMPO(mpoId, ds));
		pWoInfo.setRequiredEquipments(MPODAO.getRequiredEquipmentFromMPO(mpoId, ds));		
		return MPODAO.setWorkOrderDetailFromMPO(mpoId, pWoInfo, ds);		
	}
	
	public static PODocumentManager getPODocumentManagerFromMPO(String mpoId,POGeneralInfo poGeneralInfo, DataSource ds) {
		PODocumentManager pODocumentManager = new PODocumentManager();
		pODocumentManager.setPoGeneralInfo(poGeneralInfo);
		pODocumentManager.setPoDocument(MPODAO.getPODocumentsFromMPO(mpoId, ds));
		return pODocumentManager;		
	}
	
	public static String getPoSpecialConditionsFromMPO(String mpoId, DataSource ds) {
		return MPODAO.getPoSpecialConditionsFromMPO(mpoId, ds);
	}
	
	public static POAuthorizedCostInfo getPOAuthorizedCostInfoFromMPO(String mpoId,String jobId,POGeneralInfo pOGeneralInfo ,DataSource ds) {
		POAuthorizedCostInfo pOAuthorizedCostInfo = new POAuthorizedCostInfo();
		pOAuthorizedCostInfo.setPoActivityManager(MPODAO.getPOActivityManagerFromMPO(mpoId, jobId, ds));
		pOAuthorizedCostInfo.setPoGeneralInfo(pOGeneralInfo);
		return pOAuthorizedCostInfo;		
	}
	
	public static ArrayList<POResource> getResourcesFromMPO(String mpoId, String jobId, DataSource ds) {
		return MPODAO.getResourcesFromMPO(mpoId, jobId, ds);
	}
	
	public static PODeliverables getPODeliverablesFromMPO(String mpoId,POGeneralInfo poGeneralInfo, DataSource ds) {
		PODeliverables pODeliverables = new PODeliverables();
		pODeliverables.setPoDeliverable(MPODAO.getPODeliverablesFromMPO(mpoId, ds));
		pODeliverables.setPoGeneralInfo(poGeneralInfo);
		return pODeliverables;
	}
			
	public static ArrayList<PODocument> getPODocumentsFromMPO(String mpoId, DataSource ds) {
		return MPODAO.getPODocumentsFromMPO(mpoId, ds);		
	}
}
