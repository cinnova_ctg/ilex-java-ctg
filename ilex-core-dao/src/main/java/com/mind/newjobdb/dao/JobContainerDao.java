package com.mind.newjobdb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.fw.core.dao.util.DBUtil;

public class JobContainerDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(JobContainerDao.class);

	public static String getJobStatus(String jobId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String jobStatus = null;
		java.util.Date dateIn = new java.util.Date();

		try {
			conn = ds.getConnection();
			String sql = "select lm_js_prj_display_status from lm_job where lm_js_id=?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				jobStatus = rs.getString("lm_js_prj_display_status");
			}
		} catch (Exception e) {
			logger.error("getJobStatus(String, DataSource)", e);

			logger.error("getJobStatus(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		java.util.Date dateOut = new java.util.Date();
		if (logger.isDebugEnabled()) {
			logger.debug("getJobStatus(String, DataSource) - **** getJobStatus :: time taken :: "
					+ (dateOut.getTime() - dateIn.getTime()));
		}
		return jobStatus;
	}

}
