package com.mind.newjobdb.dao;

import java.util.ArrayList;
import java.util.Iterator;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.newjobdb.ChecklistStatusType;
import com.mind.bean.newjobdb.JobWorkflowItem;



/**
 * @purpose This class is used for the representation of 
 * 			the Job Workflow Checlist.
 */
public class JobWorkflowChecklist {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JobWorkflowChecklist.class);

	private String allItemStatus;
	private String itemListSize;
	private ArrayList<JobWorkflowItem> jobWorkflowItems;
	
	public ArrayList<JobWorkflowItem> getJobWorkflowItems() {
		return jobWorkflowItems;
	}

	public void setJobWorkflowItems(ArrayList<JobWorkflowItem> jobWorkflowItems) {
		this.jobWorkflowItems = jobWorkflowItems;
	}

	public JobWorkflowChecklist getTopPendingItems(int numOfTops) {
		return getTopPendingItems(numOfTops, false);
	}
	
	public JobWorkflowChecklist getTopPendingItems(int numOfTops, boolean showEvenIfNoPending) {
		if (logger.isDebugEnabled()) {
			logger.debug("getTopPendingItems(int, boolean) - coming");
		}
		Iterator iter = this.jobWorkflowItems.iterator();
		ArrayList<JobWorkflowItem> topPendingItems = new ArrayList<JobWorkflowItem>(); 
		while(iter.hasNext()) {
			JobWorkflowItem item = (JobWorkflowItem)iter.next();
			if(item.getItemStatusCode() ==ChecklistStatusType.PENDING_STATUS && topPendingItems.size()<numOfTops) {
				topPendingItems.add(item); 
			}
		}
		
		//if no pending item then also show 
		if(topPendingItems.size()<2 && showEvenIfNoPending) {
			JobWorkflowItem item = new JobWorkflowItem();
			//item.setItemStatusCode(-1);
			item.setItemStatus("");
			if(topPendingItems.size()==0) {
				topPendingItems.add(item);
			}
			topPendingItems.add(item);
		}
		
		JobWorkflowChecklist topCheckList = new JobWorkflowChecklist();
		topCheckList.setJobWorkflowItems(topPendingItems);
		topCheckList.setAllItemStatus(this.getAllItemStatus());
		topCheckList.setItemListSize(this.getItemListSize());
		return topCheckList;
	}
	
	public static void setNewlyCompletedItem(JobWorkflowItem jobWorkflowItem, DataSource ds,String userId) {
		JobLevelWorkflowDAO.completeItemStatus(jobWorkflowItem, ds,userId);		
	}
	
	public static void undoLastAction(JobWorkflowItem jobWorkflowItem, DataSource ds,String userId) {
		if (logger.isDebugEnabled()) {
			logger.debug("undoLastAction(JobWorkflowItem, DataSource, String) - in undo");
		}
		JobLevelWorkflowDAO.undoLastAction(jobWorkflowItem, ds,userId);
	}
	
	public static void setNewlySkippedItem(JobWorkflowItem jobWorkflowItem, DataSource ds,String userId) {
		JobLevelWorkflowDAO.setNewlySkippedItem(jobWorkflowItem, ds,userId);
	}
	
	public static void setNewlyNotApplicableItem(JobWorkflowItem jobWorkflowItem, DataSource ds,String userId) {
		JobLevelWorkflowDAO.setNewlyNotApplicableItem(jobWorkflowItem, ds,userId);
	}
	
	public JobWorkflowItem getLastAction() {
		return null;
	}

	public String getAllItemStatus() {
		return allItemStatus;
	}

	public void setAllItemStatus(String allItemStatus) {
		this.allItemStatus = allItemStatus;
	}

	public String getItemListSize() {
		return itemListSize;
	}

	public void setItemListSize(String itemListSize) {
		this.itemListSize = itemListSize;
	}
}
