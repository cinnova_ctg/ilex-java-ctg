package com.mind.newjobdb.dao;

import java.util.ArrayList;
import java.util.Iterator;

import javax.sql.DataSource;

import com.mind.bean.newjobdb.CostVariations;
import com.mind.bean.newjobdb.POType;

/**
 * @purpose This class is used for the representation of the Purchase Order
 *          Lists.
 */
public class PurchaseOrderList {
	private ArrayList<PurchaseOrder> purchaseOrders = new ArrayList<PurchaseOrder>();

	public ArrayList<PurchaseOrder> getPurchaseOrders() {
		return purchaseOrders;
	}

	public void setPurchaseOrders(ArrayList<PurchaseOrder> purchaseOrders) {
		this.purchaseOrders = purchaseOrders;
	}

	/**
	 * The copy function is used to open the functionality for inheriting MPO
	 * activities into the current job.
	 * 
	 * POs created from a MPO will be assigned a revision level of 0, the
	 * default value.
	 * 
	 * All resources carried with the MPO will be assigned the default revision
	 * level as well.
	 * 
	 * The PO copy function will copy a MPO definition to create a new job level
	 * PO. The new PO will be defaulted to the Draft status. This function will
	 * drive the user to the PO Edit screen.
	 */
	public static String copyNewPOFromMPO(String jobId, String loginUserId,
			String action, String mpoId, String powoId, String partnerId,
			String travelDistance, String travelDuration, DataSource ds) {
		String newPowoId = PurchaseOrderDAO.upsertPurchaseOrder(jobId,
				loginUserId, action, mpoId, powoId, partnerId, travelDistance,
				travelDuration, ds);
		return newPowoId;
	}

	/**
	 * The build function is used to open the funcitonality to add an activity
	 * into the current job from scratch (when no activities present).
	 * 
	 * The build PO function will create a new PO. The new PO will be defaulted
	 * to the Draft status. This function will drive the user to the PO Edit
	 * screen
	 */
	public static String buildNewPO(String jobId, String loginUserId,
			String action, String powoId, String partnerId,
			String travelDistance, String travelDuration, DataSource ds) {
		String poId = PurchaseOrderDAO.upsertPurchaseOrder(jobId, loginUserId,
				action, "", powoId, partnerId, travelDistance, travelDuration,
				ds);
		return poId;
	}

	public static String buildNewPOMinutemanOverrideCostValue(String poId,
			DataSource ds) {

		String minutemanOverrideCost = PurchaseOrderDAO
				.getMinutemanOverrideCostValue(poId, ds);
		return minutemanOverrideCost;
	}

	public static CostVariations getTotalCostVariations(
			PurchaseOrderList purchaseOrderList) {
		CostVariations totalCostVariations = new CostVariations();
		Iterator iter = purchaseOrderList.purchaseOrders.iterator();

		while (iter.hasNext()) {
			PurchaseOrder purchaseOrder = (PurchaseOrder) iter.next();
			addCostVariations(purchaseOrder.getCostVariations(),
					totalCostVariations);
		}
		return totalCostVariations;

	}

	private static void addCostVariations(CostVariations toBeAdded,
			CostVariations addedTo) {
		addPOType(toBeAdded.getPVSType(), addedTo.getPVSType());
		addPOType(toBeAdded.getMinutemanType(), addedTo.getMinutemanType());
		addPOType(toBeAdded.getSpeedpayType(), addedTo.getSpeedpayType());
	}

	private static void addPOType(POType toBeAdded, POType addedTo) {
		addedTo.setContractLaborCost(toBeAdded.getContractLaborCost()
				+ addedTo.getContractLaborCost());
		addedTo.setHourlyTravelCost(toBeAdded.getHourlyTravelCost()
				+ addedTo.getHourlyTravelCost());
		addedTo.setTotalCost(
		// toBeAdded.getTotalCost()
		+addedTo.getTotalCost());
	}
}
