package com.mind.newjobdb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.newjobdb.ChecklistStatusType;
import com.mind.bean.newjobdb.JobWorkflowItem;

/**
 * @purpose This class is used for the representation of the data access for the
 *          Job Level Workflow Checklist.
 */
public class JobLevelWorkflowDAO extends WorkflowChecklistDAO {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(JobLevelWorkflowDAO.class);

	public static JobWorkflowChecklist getJobWorkflowChecklist(long jobId,
			DataSource ds) {
		boolean isComplete = true;
		if (logger.isDebugEnabled()) {
			logger.debug("getJobWorkflowChecklist(long, DataSource) - 10");
		}
		JobWorkflowChecklist jobWorkflowChecklist = new JobWorkflowChecklist();
		jobWorkflowChecklist.setJobWorkflowItems(getJobWorkflowItem(jobId, ds));
		jobWorkflowChecklist.setItemListSize(String
				.valueOf(jobWorkflowChecklist.getJobWorkflowItems().size()));
		if (logger.isDebugEnabled()) {
			logger.debug("getJobWorkflowChecklist(long, DataSource) - item list soze-----"
					+ jobWorkflowChecklist.getItemListSize());
		}

		JobWorkflowItem jobWorkflowItem = null;
		for (int i = 0; i < jobWorkflowChecklist.getJobWorkflowItems().size(); i++) {
			jobWorkflowItem = jobWorkflowChecklist.getJobWorkflowItems().get(i);
			if (jobWorkflowItem.getItemStatusCode() == ChecklistStatusType.PENDING_STATUS) {
				isComplete = false;
				break;
			}
		}
		if (isComplete == true
				&& jobWorkflowChecklist.getJobWorkflowItems().size() > 0)
			jobWorkflowChecklist.setAllItemStatus("complete");
		return jobWorkflowChecklist;

	}

	private static void setItemStatus(int newStatus,
			JobWorkflowItem jobWorkflowItem, DataSource ds, String userId) {
		long itemId = jobWorkflowItem.getItemId();
		if (logger.isDebugEnabled()) {
			logger.debug("setItemStatus(int, JobWorkflowItem, DataSource, String) - coming here");
		}
		Connection conn = null;
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		PreparedStatement pStmt = null;
		try {
			conn = ds.getConnection();
			String sql = "update job_lvl_job_chk_items set "
					+ "item_status_code=" + newStatus + " ,updated_by = ?"
					+ " ,updated_date = ?" + " ,user_id = ?"
					+ " ,reference_date = ?"
					+ ", last_item_status_code=item_status_code"
					+ " where item_id=" + "'" + itemId + "'";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userId);
			pStmt.setTimestamp(2, ts);
			pStmt.setString(3, userId);
			pStmt.setTimestamp(4, ts);
			pStmt.executeUpdate();

		} catch (Exception e) {
			logger.error(
					"setItemStatus(int, JobWorkflowItem, DataSource, String)",
					e);

			// logger.error(
			// "setItemStatus(int, JobWorkflowItem, DataSource, String)",
			// e);
		} finally {
			try {
				// Closing DB Resources
				if (pStmt != null) {
					pStmt.close();
					pStmt = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;

				}
			} catch (Exception e) {
				logger.error(
						"setItemStatus(int, JobWorkflowItem, DataSource, String)",
						e);

				// logger.error(
				// "setItemStatus(int, JobWorkflowItem, DataSource, String)",
				// e);
			}

		}
	}

	public static void completeItemStatus(JobWorkflowItem jobWorkflowItem,
			DataSource ds, String userId) {
		setItemStatus(ChecklistStatusType.COMPLETED_STATUS, jobWorkflowItem,
				ds, userId);
	}

	public static void undoLastAction(JobWorkflowItem jobWorkflowItem,
			DataSource ds, String userId) {
		long itemId = jobWorkflowItem.getItemId();
		if (logger.isDebugEnabled()) {
			logger.debug("undoLastAction(JobWorkflowItem, DataSource, String) - coming here");
		}
		Connection conn = null;
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		PreparedStatement pStmt = null;
		try {
			conn = ds.getConnection();
			String sql = "update job_lvl_job_chk_items set "
					+ "item_status_code=last_item_status_code"
					+ " ,updated_by = ?" + " ,updated_date = ?"
					+ " ,user_id = ?" + " ,reference_date = ?"
					+ " where item_id=" + "'" + itemId + "'";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userId);
			pStmt.setTimestamp(2, ts);
			pStmt.setString(3, null);
			pStmt.setTimestamp(4, null);
			pStmt.executeUpdate();

		} catch (Exception e) {
			logger.error("undoLastAction(JobWorkflowItem, DataSource, String)",
					e);

			// logger.error("undoLastAction(JobWorkflowItem, DataSource, String)",
			// e);
		} finally {
			try {
				// Closing DB Resources
				if (pStmt != null) {
					pStmt.close();
					pStmt = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;

				}
			} catch (Exception e) {
				logger.error(
						"undoLastAction(JobWorkflowItem, DataSource, String)",
						e);

				// logger.error(
				// "undoLastAction(JobWorkflowItem, DataSource, String)",
				// e);
			}

		}
	}

	public static void setNewlySkippedItem(JobWorkflowItem jobWorkflowItem,
			DataSource ds, String userId) {
		setItemStatus(ChecklistStatusType.SKIPPED_STATUS, jobWorkflowItem, ds,
				userId);
	}

	public static void setNewlyNotApplicableItem(
			JobWorkflowItem jobWorkflowItem, DataSource ds, String userId) {
		setItemStatus(ChecklistStatusType.NOT_APPLICABLE_STATUS,
				jobWorkflowItem, ds, userId);
	}

	public static JobWorkflowItem getJobWorkflowItemObj(long jobId,
			long itemId, DataSource ds) {

		JobWorkflowItem jobWorkflowItem = new JobWorkflowItem();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select item_id, description, isnull(dbo.func_cc_poc_name( user_id ),'') as user_id, item_status_code,last_item_status_code,CONVERT(VARCHAR(10), reference_date, 1) as reference_date from job_lvl_job_chk_items where job_id="
					+ "'"
					+ jobId
					+ "' and item_id="
					+ "'"
					+ itemId
					+ "' order by sequence,reference_date,description desc";
			if (logger.isDebugEnabled()) {
				logger.debug("getJobWorkflowItemObj(long, long, DataSource) - sql: "
						+ sql);
			}
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				jobWorkflowItem.setItemId(rs.getInt("item_id"));
				jobWorkflowItem.setDescription(rs.getString("description"));
				jobWorkflowItem.setUser(rs.getString("user_id"));
				jobWorkflowItem.setItemStatusCode(Integer.parseInt(rs
						.getString("item_status_code")));
				jobWorkflowItem
						.setReferenceDate(rs.getString("reference_date"));
				jobWorkflowItem.setProjectWorkflowItemId(rs.getInt("item_id"));
				jobWorkflowItem.setLastItemStatusCode(Integer.parseInt(rs
						.getString("last_item_status_code")));

			}

		} catch (Exception e) {
			logger.error("getJobWorkflowItemObj(long, long, DataSource)", e);

			// logger.error("getJobWorkflowItemObj(long, long, DataSource)", e);
		} finally {
			try {
				// Closing DB Resources
				if (rs != null) {
					rs.close();
					rs = null;
				}
				if (stmt != null) {
					stmt.close();
					stmt = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;

				}
			} catch (Exception e) {
				logger.error("getJobWorkflowItemObj(long, long, DataSource)", e);

				// logger.error("getJobWorkflowItemObj(long, long, DataSource)",
				// e);
			}

		}
		return jobWorkflowItem;
	}

	public static ArrayList<JobWorkflowItem> getJobWorkflowItem(long jobId,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<JobWorkflowItem> jobWorkflowItemAl = new ArrayList<JobWorkflowItem>();
		try {
			conn = ds.getConnection();
			String sql = "select sequence,item_id, description, isnull(dbo.func_cc_poc_name( user_id ),'') as user_id, "
					+ "item_status_code,last_item_status_code,CONVERT(VARCHAR(10), reference_date, 1) as reference_date,updated_by,"
					+ "updated_date,created_by,created_date from job_lvl_job_chk_items where job_id="
					+ "'"
					+ jobId
					+ "'"
					+ " order by sequence,reference_date,description ";

			logger.info("getJobWorkflowItem(long, DataSource) - " + sql);

			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			SimpleDateFormat sf = new SimpleDateFormat("MM/dd/yyyy");
			while (rs.next()) {
				JobWorkflowItem jobWorkflowItem = new JobWorkflowItem();
				jobWorkflowItem.setItemId(rs.getInt("item_id"));
				jobWorkflowItem.setDescription(rs.getString("description"));
				jobWorkflowItem.setUser(rs.getString("user_id"));
				jobWorkflowItem.setItemStatusCode(Integer.parseInt(rs
						.getString("item_status_code")));
				jobWorkflowItem.setLastItemStatusCode(Integer.parseInt(rs
						.getString("last_item_status_code")));
				jobWorkflowItem
						.setReferenceDate(rs.getString("reference_date"));
				jobWorkflowItem.setProjectWorkflowItemId(rs.getInt("item_id"));
				jobWorkflowItem.setUpdatedBy(rs.getString("updated_by"));
				jobWorkflowItem
						.setUpdatedDate(rs.getDate("updated_date") != null ? new Date(
								sf.format(rs.getDate("updated_date"))) : null);
				jobWorkflowItem.setCreatedBy(rs.getString("created_by"));
				jobWorkflowItem
						.setCreatedDate(rs.getDate("created_date") != null ? new Date(
								sf.format(rs.getDate("created_date"))) : null);
				jobWorkflowItem.setItemStatus(ChecklistStatusType
						.getStatusName(new Integer(rs
								.getString("item_status_code"))));
				jobWorkflowItem.setSequence(rs.getString("sequence"));
				jobWorkflowItemAl.add(jobWorkflowItem);

			}

		} catch (Exception e) {
			logger.error("getJobWorkflowItem(long, DataSource)", e);
			// logger.error("getJobWorkflowItem(long, DataSource)", e);
		} finally {
			try {
				// Closing DB Resources
				if (rs != null) {
					rs.close();
					rs = null;
				}
				if (stmt != null) {
					stmt.close();
					stmt = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;

				}
			} catch (Exception e) {
				logger.error("getJobWorkflowItem(long, DataSource)", e);

				// logger.error("getJobWorkflowItem(long, DataSource)", e);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("getJobWorkflowItem(long, DataSource) - jobWorkflowItemAlxxxxx: "
						+ jobWorkflowItemAl);
			}
		}
		return jobWorkflowItemAl;
	}
	/*
	 * public static ArrayList getWorkflowChecklistItem(long jobId, DataSource
	 * ds){ WorkflowChecklistItem workflowChecklistItem = new
	 * WorkflowChecklistItem(); Connection conn = null; Statement stmt = null;
	 * ResultSet rs = null; ArrayList<WorkflowChecklistItem>
	 * workflowChecklistItemAl = new ArrayList<WorkflowChecklistItem>(); try{
	 * conn = ds.getConnection(); String sql =
	 * "select item_id, description, [user_name], item_status_code, CONVERT(VARCHAR(10), reference_date, 101) as reference_date  from job_lvl_job_chk_items where lm_js_id="
	 * +"'"+jobId+"'"; stmt = conn.createStatement(); rs =
	 * stmt.executeQuery(sql); while(rs.next()){
	 * workflowChecklistItem.setItemId(rs.getInt("item_id"));
	 * workflowChecklistItem.setDescription(rs.getString("description"));
	 * workflowChecklistItem.setUser(rs.getString("user_name"));
	 * workflowChecklistItem.setReferenceDate(rs.getString("reference_date"));
	 * workflowChecklistItemAl.add(workflowChecklistItem);
	 * 
	 * }
	 * 
	 * }catch(Exception e){ e.printStackTrace(); }finally{ try{ //Closing DB
	 * Resources if(rs!=null){ rs.close(); rs = null; } if(stmt!=null){
	 * stmt.close(); stmt = null; } if(conn!=null){ conn.close(); conn = null;
	 * 
	 * } }catch(Exception e){ e.printStackTrace(); }
	 * System.out.println("workflowChecklistItemAl.size():::  "
	 * +workflowChecklistItemAl.size()); } return workflowChecklistItemAl; }
	 */
}
