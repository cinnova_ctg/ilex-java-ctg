package com.mind.newjobdb.dao;

import java.io.Serializable;
import java.util.ArrayList;

import com.mind.bean.newjobdb.PODeliverable;




public class PODeliverables implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private POGeneralInfo poGeneralInfo;
	private ArrayList<PODeliverable> poDeliverable;
	
	

	public POGeneralInfo getPoGeneralInfo() {
		return poGeneralInfo;
	}

	public void setPoGeneralInfo(POGeneralInfo poGeneralInfo) {
		this.poGeneralInfo = poGeneralInfo;
	}

	public static void editDeliverable(PODeliverable poDeliverable) {
	
	}
	
	public static void deleteDeliverable(PODeliverable poDeliverable) {
	
	}
	
	public static void addDeliverable(PODeliverable poDeliverable) {
	
	}
	
	public static ArrayList<PODeliverable> getPODeliverables() {
		return null;
	}
	
	public void acceptAll(PODeliverable poDeliverable) {
	
	}

	public ArrayList<PODeliverable> getPoDeliverable() {
		return poDeliverable;
	}

	public void setPoDeliverable(ArrayList<PODeliverable> poDeliverable) {
		this.poDeliverable = poDeliverable;
	}
}
