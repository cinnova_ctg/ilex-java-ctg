package com.mind.newjobdb.dao;

import java.io.Serializable;
import java.util.ArrayList;

import com.mind.bean.newjobdb.PODocument;




public class PODocumentManager implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private POGeneralInfo poGeneralInfo;
	private ArrayList<PODocument> poDocument;
	
	public static PODocument addDocument() {
		return null;
	}
	
	public ArrayList<PODocument> getPoDocuments() {
		return this.poDocument;
	}
	
	public static void updateDocument(PODocument poDocument) {
	
	}
	
	public static void removeDocument(PODocument poDocument) {
	
	}

	public POGeneralInfo getPoGeneralInfo() {
		return poGeneralInfo;
	}

	public void setPoGeneralInfo(POGeneralInfo poGeneralInfo) {
		this.poGeneralInfo = poGeneralInfo;
	}

	public ArrayList<PODocument> getPoDocument() {
		return poDocument;
	}

	public void setPoDocument(ArrayList<PODocument> poDocument) {
		this.poDocument = poDocument;
	}

	
}
