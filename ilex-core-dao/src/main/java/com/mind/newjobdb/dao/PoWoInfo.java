package com.mind.newjobdb.dao;

import java.io.Serializable;
import java.util.ArrayList;

import com.mind.bean.newjobdb.RequiredEquipment;





public class PoWoInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private POGeneralInfo poGeneralInfo;
	private String poSpecialConditions;
	private String woNatureOfActivity;
	private String woSpecialInstructions;
	private ArrayList<RequiredEquipment> requiredEquipments;
	
	public static void savePoWoInfo() {
	
	}
	
	public static String getPoDefaultSpecialConditions() {
		return null;
	}
	
	public static String getWoDefaultNatureOfActivity() {
		return null;
	}
	
	public static String  getWoDefaultSpecialInstructions() {
		return null;
	}
	
	public static ArrayList<RequiredEquipment> getRequiredEquipments(String id) {
		return null;
	}

	public POGeneralInfo getPoGeneralInfo() {
		return poGeneralInfo;
	}

	public void setPoGeneralInfo(POGeneralInfo poGeneralInfo) {
		this.poGeneralInfo = poGeneralInfo;
	}

	public String getPoSpecialConditions() {
		return poSpecialConditions;
	}

	public void setPoSpecialConditions(String poSpecialConditions) {
		this.poSpecialConditions = poSpecialConditions;
	}

	public ArrayList<RequiredEquipment> getRequiredEquipments() {
		return requiredEquipments;
	}

	public void setRequiredEquipments(
			ArrayList<RequiredEquipment> requiredEquipments) {
		this.requiredEquipments = requiredEquipments;
	}

	public String getWoNatureOfActivity() {
		return woNatureOfActivity;
	}

	public void setWoNatureOfActivity(String woNatureOfActivity) {
		this.woNatureOfActivity = woNatureOfActivity;
	}

	public String getWoSpecialInstructions() {
		return woSpecialInstructions;
	}

	public void setWoSpecialInstructions(String woSpecialInstructions) {
		this.woSpecialInstructions = woSpecialInstructions;
	}
}
