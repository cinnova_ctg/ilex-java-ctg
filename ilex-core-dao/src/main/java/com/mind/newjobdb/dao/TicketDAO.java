package com.mind.newjobdb.dao;

import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.TicketActivitySummary;
import com.mind.bean.newjobdb.TicketRequestInfo;
import com.mind.bean.newjobdb.TicketRequestNature;
import com.mind.bean.newjobdb.TicketResourceSummary;
import com.mind.bean.newjobdb.TicketScheduleInfo;
import com.mind.bean.newjobdb.TicketTab;
import com.mind.common.POStatusTypes;
import com.mind.common.Util;
import com.mind.dao.PM.Jobdao;
import com.mind.fw.core.dao.util.DBUtil;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

/**
 * @purpose This class is used to interact with the the TICKET database tables.
 */
public class TicketDAO {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(TicketDAO.class);

    /**
     * *
     * Method used to check whether po exists for a job which is not in
     * Draft/Cancelled State.
     *
     * @param jobId
     * @param ds
     * @return
     */
    public static boolean isPOAssignedTOJob(String jobId, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        boolean poexists = false;
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            String sql = "select lm_po_id from lm_purchase_order where lm_po_js_id = "
                    + jobId
                    + " and isnull(po_status,0) not in ("
                    + POStatusTypes.STATUS_DRAFT
                    + ","
                    + POStatusTypes.STATUS_DRAFT + ")";
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                poexists = true;
            }

        } catch (Exception e) {
            logger.error("isPOAssignedTOJob(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }
        return poexists;
    }

    /**
     * Discription: This method is used to save/update a ticket detail into
     * database.
     *
     * @param ticketTab -- Object of TicketTab
     * @param ds
     */
    public static String[] updateTicket(TicketTab ticketTab, long loginId, DataSource ds) {

        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        int flag = -1;
        String jobId = "";
        String ticketId = "";
        Date recievedDate;
        String[] returnArray = new String[3];

        // System.out.println("2= " + ticketTab.getTicketNumber());
        // System.out.println("3= " +
        // ticketTab.getRequestInfo().getRequestor());
        // System.out.println("4= "
        // + ticketTab.getRequestInfo().getRequestorEmail());
        // System.out.println("5= " + ticketTab.getRequestInfo().getSite());
        // System.out.println("6= "
        // + IlexTimestamp.getDateTimeType2(ticketTab.getScheduleInfo()
        // .getRequestedTimestamp()));
        // System.out.println("7= "
        // + getStringValue(ticketTab.getRequestInfo().isStandBy()));
        // System.out.println("8= "
        // + getStringValue(ticketTab.getRequestInfo().isMSP()));
        // System.out.println("9= "
        // + getStringValue(ticketTab.getRequestInfo().isHelpDesk()));
        // System.out.println("10= "
        // + ticketTab.getRequestNature().getProblemCategory());
        // System.out.println("11= "
        // + ticketTab.getRequestInfo().getCustomerReference());
        // System.out.println("12= "
        // + ticketTab.getRequestNature().getProblemDescription());
        // System.out.println("13= "
        // + ticketTab.getRequestNature().getSpecialInstructions());
        // System.out
        // .println("14= " + ticketTab.getRequestInfo().getCriticality());
        // System.out.println("15= " +
        // ticketTab.getRequestInfo().getResource());
        // System.out.println("16= "
        // + IlexTimestamp.getDateTimeType2(ticketTab.getScheduleInfo()
        // .getPreferredTimestamp())); //
        // System.out.println("17= "
        // + IlexTimestamp.getDateTimeType2(ticketTab.getScheduleInfo()
        // .getWindowToTimestamp())); // System.out.println("18= "+
        // IlexTimestamp.getDateTimeType2(ticketTab.getScheduleInfo()
        // .getWindowFromTimestamp());
        // System.out.println("17= "
        // + IlexTimestamp.getDateTimeType2(ticketTab.getScheduleInfo()
        // .getWindowFromTimestamp()));
        // System.out.println("18= "
        // + IlexTimestamp.getDateTimeType2(ticketTab.getScheduleInfo()
        // .getWindowToTimestamp()));
        // System.out.println("19= " + loginId);
        // System.out.println("20= "
        // + getPPSValue(ticketTab.getRequestInfo().getCriticality(),
        // true, ds));
        // System.out.println("21= " + ticketTab.getAppendixId());
        // System.out
        // .println("22= " + ticketTab.getRequestInfo().getRequestType());
        // if (ticketTab.getRequestInfo().isPPS()) {
        // System.out.println("23= " + "Y");
        // } else {
        // System.out.println("23= " + "N");
        // }
        // System.out.println("24= " + ticketTab.getWebTicket());
        // System.out.println("25= " + ticketTab.getMonitoringSourceCd());
        // System.out.println("26= " + ticketTab.getDeviceId());
        try {

            conn = ds.getConnection();
            cstmt = conn
                    .prepareCall("{?=call lm_job_ticket_manage_02(?,?,?,?, ?,?,?,?, ?,?,?,?, ?,?,?,?, ?,?,?,?, ?,?,?,?,	?,?,?,?)}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setString(2, ticketTab.getTicketNumber());
            cstmt.setString(3, getRequesterName(ticketTab.getRequestInfo()
                    .getRequestor()));
            cstmt.setString(4, ticketTab.getRequestInfo().getRequestorEmail());
            cstmt.setString(5, ticketTab.getRequestInfo().getSite());
            cstmt.setString(6, IlexTimestamp.getDateTimeType2(ticketTab
                    .getScheduleInfo().getRequestedTimestamp()));
            cstmt.setString(7, getStringValue(ticketTab.getRequestInfo()
                    .isStandBy()));
            cstmt.setString(8, getStringValue(ticketTab.getRequestInfo()
                    .isMSP()));
            cstmt.setString(9, getStringValue(ticketTab.getRequestInfo()
                    .isHelpDesk()));
            cstmt.setString(10, ticketTab.getRequestNature()
                    .getProblemCategory());
            cstmt.setString(11, ticketTab.getRequestInfo()
                    .getCustomerReference());
            cstmt.setString(12, ticketTab.getRequestNature()
                    .getProblemDescription());
            cstmt.setString(13, ticketTab.getRequestNature()
                    .getSpecialInstructions());
            cstmt.setString(14, ticketTab.getRequestInfo().getCriticality());
            cstmt.setString(15, ticketTab.getRequestInfo().getResource());
            cstmt.setString(16, IlexTimestamp.getDateTimeType2(ticketTab
                    .getScheduleInfo().getPreferredTimestamp()));
            cstmt.setString(17, IlexTimestamp.getDateTimeType2(ticketTab
                    .getScheduleInfo().getWindowFromTimestamp()));
            cstmt.setString(18, IlexTimestamp.getDateTimeType2(ticketTab
                    .getScheduleInfo().getWindowToTimestamp()));
            cstmt.setFloat(19, loginId); // - login user id
            cstmt.setString(
                    20,
                    getPPSValue(ticketTab.getRequestInfo().getCriticality(),
                            true, ds));
            cstmt.setFloat(21, ticketTab.getAppendixId());
            cstmt.setString(22, ticketTab.getRequestInfo().getRequestType());

            if (ticketTab.getRequestInfo().isPPS()) {
                cstmt.setString(23, "Y");
            } else {
                cstmt.setString(23, "N");
            }

            cstmt.setString(24, ticketTab.getWebTicket());
            cstmt.setInt(
                    25,
                    ticketTab.getMonitoringSourceCd() != null ? ticketTab
                    .getMonitoringSourceCd() : Integer.valueOf(-1));
            cstmt.setLong(26,
                    ticketTab.getDeviceId() != null ? ticketTab.getDeviceId()
                    : Long.valueOf(-1));

            cstmt.registerOutParameter(27, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(28, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(29, java.sql.Types.TIMESTAMP);
            cstmt.execute();

            jobId = cstmt.getString(27);
            ticketId = cstmt.getString(28);
            recievedDate = cstmt.getTimestamp(29);
            returnArray[0] = jobId;
            returnArray[1] = ticketId;
            SimpleDateFormat sd = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
            returnArray[2] = sd.format(recievedDate);
            flag = cstmt.getInt(1);

            if (flag == 0) {
                ticketTab.setUpdateCompleteTicket("0");
                ticketTab.setWebTicket("");
            }
        } catch (Exception e) {
            logger.error("updateTicket(TicketTab, long, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("updateTicket(TicketTab, long, DataSource) - Exception in com.mind.newjobdb.dao.updateTicket()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, cstmt);
            DBUtil.close(conn);
        }
        return returnArray;

    } // - end of updateTicket() method

    /**
     * Discription: This method is used to get detail info from database on the
     * basis of ticket Id.
     *
     * @param ticketId -- Ticket Id
     * @param ds -- Database Object
     * @return Object of TicketTab class.
     */
    public static TicketTab getTicket(long appendixId, long ticketId,
            DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        TicketTab ticketTab = new TicketTab();
        ticketTab.setTicketId(ticketId);
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            String sql = "select * from dbo.func_ticket_detail_01(" + ticketId
                    + ")";
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                TicketRequestInfo requestInfo = new TicketRequestInfo();
                TicketScheduleInfo scheduleInfo = new TicketScheduleInfo();
                TicketRequestNature requestNature = new TicketRequestNature();

                ticketTab.setTicketId(rs.getLong("lm_tc_id"));
                ticketTab.setJobId(rs.getLong("lm_tc_js_id"));
                ticketTab.setTicketNumber(rs.getString("lm_tc_number"));

                String jobId = new Long(ticketTab.getJobId()).toString();
                ticketTab.setRequestorIds(Jobdao.getRequestorIdsByJobId(jobId, ds));
                ticketTab.setAdditionalRecipientIds(Jobdao.getAdditionalRecipientIdsByJobId(jobId, ds));

                requestInfo.setRequestor(rs.getString("lm_tc_requestor_name"));
                requestInfo.setRequestorEmail(rs
                        .getString("lm_tc_requestor_email"));

                requestInfo.setRequestType(rs.getString("lm_tc_request_type"));
                requestInfo.setResource(rs.getString("lm_tc_resource_level"));
                scheduleInfo
                        .setRequestedTimestamp(IlexTimestamp
                                .converToTimestamp(rs
                                        .getString("lm_tc_requested_date")));
                requestInfo
                        .setStandBy(getBoolean(rs.getString("lm_tc_standby")));

                if (rs.getString("nonPPS") != null) {
                    if (rs.getString("nonPPS").equals("Y")) {
                        requestInfo.setPPS(false);
                    } else {
                        requestInfo.setPPS(true);
                    }
                } else {
                    requestInfo.setPPS(true);
                }

                requestInfo.setMSP(getBoolean(rs.getString("lm_tc_msp")));
                requestInfo.setHelpDesk(getBoolean(rs
                        .getString("lm_tc_help_desk")));
                requestNature
                        .setProblemCategory(rs.getString("lm_tc_category"));
                requestNature.setProblemDescription(rs
                        .getString("lm_tc_problem_desc"));
                requestInfo.setCustomerReference(rs
                        .getString("lm_tc_cust_reference"));
                requestNature.setSpecialInstructions(rs
                        .getString("lm_tc_spec_instruction"));
                requestInfo.setCriticality(rs.getString("lm_tc_criticality"));
                scheduleInfo.setPreferredTimestamp(IlexTimestamp
                        .converToTimestamp(rs
                                .getString("lm_tc_preferred_arrival")));
                scheduleInfo.setWindowToTimestamp(IlexTimestamp
                        .converToTimestamp(rs
                                .getString("lm_tc_preferred_window_end")));
                scheduleInfo.setWindowFromTimestamp(IlexTimestamp
                        .converToTimestamp(rs
                                .getString("lm_tc_preferred_window_start")));
                scheduleInfo.setReceivedTimestamp(IlexTimestamp
                        .converToTimestamp(rs.getString("lm_js_create_date")));

                if (rs.getString("lm_tc_source").equals("WEB")
                        && rs.getString("ticket_creator").equals(
                                "Not Assigned Dispatch")) {
                    ticketTab.setUpdateCompleteTicket("1");
                } else {
                    ticketTab.setUpdateCompleteTicket("0");
                }

                ticketTab.setRequestInfo(requestInfo);
                ticketTab.setScheduleInfo(scheduleInfo);
                ticketTab.setRequestNature(requestNature);
                ticketTab.setAppendixId(appendixId);
            }
        } catch (Exception e) {
            logger.error("getTicket(long, long, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getTicket(long, long, DataSource) - Exception in com.mind.newjobdb.dao.getTicket()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }
        return ticketTab;

    } // - end of getTicket() method

    /**
     * Discription: This method return true/false boolean value on the basis of
     * Y/N.
     *
     * @param str -- Y?N
     * @return check -- true/false
     */
    public static boolean getBoolean(String str) {
        boolean check = false;

        if (str.equals("Y")) {
            check = true;
        }
        return check;
    }

    /**
     * Discription: This method return Y/N.
     *
     * @param str -- Y?N
     * @return check -- true/false
     */
    private static String getStringValue(boolean check) {
        String value = "N";

        if (check) {
            value = "Y";
        }
        return value;
    }

    /**
     * Discription: This method is used to data from Database when a new Ticket
     * is created.
     *
     * @param appendixId -- Appendix Id
     * @param ds -- Database Object
     * @return Object of TicketTab class.
     */
    public static TicketTab createNewTicket(long appendixId, DataSource ds) {

        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        TicketTab ticketTab = new TicketTab();
        try {
            conn = ds.getConnection();
            cstmt = conn.prepareCall("{?=call lm_get_ticket_number()}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            rs = cstmt.executeQuery();
            if (rs.next()) {

                TicketRequestInfo ticketRequestInfo = new TicketRequestInfo();
                ticketTab.setTicketNumber(rs.getString("ticket_number"));
                ticketTab.setTicketId(0);
                ticketTab.setJobId(0);
                // ticketRequestInfo.setMSP(
                // getBoolean(getMspStatus(appendixId,ds)) );
                ticketRequestInfo.setMSP(false);
                ticketRequestInfo.setStandBy(false);
                ticketRequestInfo.setHelpDesk(false);
                ticketRequestInfo.setPPS(true);
                if (!getNetMedXCutReference(appendixId, ds).equals("")) {
                    ticketRequestInfo
                            .setCustomerReference(getNetMedXCutReference(
                                            appendixId, ds));
                }
                ticketTab.getScheduleInfo().setReceivedTimestamp(
                        IlexTimestamp.getSystemDateTime());
                ticketTab.getScheduleInfo().setRequestedTimestamp(
                        IlexTimestamp.getSystemDateTime());
                ticketTab.setRequestInfo(ticketRequestInfo);
            }
            ticketTab.setAppendixId(appendixId);
        } catch (Exception e) {
            logger.error("createNewTicket(long, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("createNewTicket(long, DataSource) - Exception in com.mind.newjobdb.dao.createNewTicket()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, cstmt);
            DBUtil.close(conn);
        }

        return ticketTab;
    } // end of createNewTicket() method

    /**
     * Discription: This method is used to check MSP for a appendix on the basis
     * of appendix id.
     *
     * @param appendixId -- Appendix id
     * @param ds -- Datasource Object
     * @return String -- Y/N
     */
    public static String getMspStatus(long appendixId, DataSource ds) {

        String checkStatus = "";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            String sql = "select * from lx_appendix_detail where lx_pd_pr_id="
                    + appendixId;
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                checkStatus = rs.getString("lx_pd_msp");
            }
        } catch (Exception e) {
            logger.error("getMspStatus(long, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getMspStatus(long, DataSource) - Exception in com.mind.newjobdb.dao.getMspStatus()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }
        return checkStatus;
    } // - end of getMspStatus() method

    private static String getRequesterName(String str) {
        if (str.indexOf("~") > -1) {
            return str.substring(0, str.indexOf("~"));
        } else {
            return str;
        }
    }

    /**
     * @param criticalityId
     * @param ds
     * @return
     */
    private static boolean getPPSCheck(String criticalityId, DataSource ds) {
        boolean check = false;
        check = getPPS(criticalityId, ds);
        return check;
    }

    /**
     * @param catId
     * @param isPPS
     * @param ds
     * @return
     */
    public static String getPPSValue(String criticalityId, boolean isPPS,
            DataSource ds) {
        String value = "";
        String[] pps = getPPSDate(criticalityId, ds);
        if (isPPS) {
            value = pps[0];
        } else {
            value = pps[1];
        }
        return value;
    }

    /**
     * Method is used to check whether PPS/Non-PPS is selected.
     *
     * @param criticalityId
     * @param ds
     * @return
     */
    public static boolean getPPS(String criticalityId, DataSource ds) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        boolean result = false;
        try {
            conn = ds.getConnection();
            String sql = "select clr_cat_short_name from clr_category where clr_cat_id = ?"; // -
            // 1
            // parameter
            pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, criticalityId);
            rs = pStmt.executeQuery();
            if (rs.next()) {
                result = (rs.getString("clr_cat_short_name").equals("PPS")) ? true
                        : false;
            }
        } // - end of try
        catch (Exception e) {
            logger.error("getPPS(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getPPS(String, DataSource) - Exception in com.mind.newjobdb.dao.getMspStatus()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, pStmt);
            DBUtil.close(conn);
        }
        return result;
    } // - end of getActivityList() method

    /**
     * @param criticalityId
     * @param ds
     * @return
     */
    public static String[] getPPSDate(String criticalityId, DataSource ds) {

        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;

        String[] pps = new String[2];
        pps[0] = "1";
        pps[1] = "2";
        int i = 0;

        try {
            conn = ds.getConnection();
            String sql = "select clr_cat_id from dbo.func_lo_criticality_category(?)"; // -
            // 1
            // parameter
            pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, criticalityId);
            rs = pStmt.executeQuery();

            while (rs.next()) {
                pps[i] = rs.getString("clr_cat_id");
                i++;
            } // - end of while
        } // - end of try
        catch (Exception e) {
            logger.error("getPPSDate(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getPPSDate(String, DataSource) - Exception in com.mind.newjobdb.dao.getPPSDate()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, pStmt);
            DBUtil.close(conn);
        }

        return pps;
    } // - end of getActivityList() method

    public static String getNewJobId(DataSource ds) {
        String jobId = "";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            String sql = "select job_id = max(lm_js_id) from lm_job";
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                jobId = rs.getString("job_id");
            }
        } catch (Exception e) {
            logger.error("getNewJobId(DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getNewJobId(DataSource) - Exception in com.mind.newjobdb.dao.getNewJobId()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }
        return jobId;
    }

    /**
     * @param msaid
     * @param ds
     * @return
     */
    public static String getRules(String id, DataSource ds) {
        Connection conn = null;
        Statement stmt = null;
        String rules = "";
        String sql = "";
        ResultSet rs = null;
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            sql = "select * from dbo.func_clr_master_detail(" + id + ")";
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                rules = rs.getString("clr_rules");
            }
        } catch (Exception e) {
            logger.error("getRules(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getRules(String, DataSource) - Exception in com.mind.newjobdb.dao.getPPSDate()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }
        return rules;
    }

    /**
     * This method get Ticket Activity summary from Execute Tab.
     *
     * @param jobId
     * @param mopId
     * @param powoId
     * @param partnerId
     * @param ds
     * @return
     */
    public static TicketActivitySummary getTicketActivitySummary(String jobId,
            DataSource ds) {
        TicketActivitySummary ticketActivitySummary = new TicketActivitySummary();
        getTicketResourceSummary(jobId,
                ticketActivitySummary.getTicketResourceSummary(), ds);
        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;

        try {

            conn = ds.getConnection();
            cstmt = conn
                    .prepareCall("{?=call lm_ticket_activity_summary_view_01(?)}"); // -
            // 5
            // Parameter
            // &
            // 1
            // output
            // parameter
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setString(2, jobId);
            rs = cstmt.executeQuery();

            if (rs != null && rs.next()) {
                ticketActivitySummary
                        .setActivityId(rs.getString("activity_id"));
                ticketActivitySummary.setTotal_extended_price(rs
                        .getString("total_extended_price"));
                ticketActivitySummary.setActual_cost(rs
                        .getString("actual_cost"));
                ticketActivitySummary.setVgpm(rs.getString("vgpm"));
                ticketActivitySummary.setUnion_uplift_factor(rs
                        .getString("union_uplift_factor"));
                ticketActivitySummary.setExpedite24_uplift_factor(rs
                        .getString("expedite24_uplift_factor"));
                ticketActivitySummary.setExpedite48_uplift_factor(rs
                        .getString("expedite48_uplift_factor"));
                ticketActivitySummary.setNon_pps_uplift_factor(rs
                        .getString("non_pps_uplift_factor"));
                ticketActivitySummary.setInternational_uplift_factor(rs
                        .getString("international_uplift_factor"));
            }
        } catch (Exception e) {
            logger.error("getTicketActivitySummary(String, DataSource)", e);
        } finally {
            DBUtil.close(rs, cstmt);
            DBUtil.close(conn);

        }
        return ticketActivitySummary;
    }

    /**
     * This method get Ticket Resource Summary.
     *
     * @param jobId
     * @param ds
     * @return
     */
    public static void getTicketResourceSummary(String jobId,
            ArrayList<TicketResourceSummary> ticketResourceSummary,
            DataSource ds) {
        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;

        try {

            conn = ds.getConnection();
            cstmt = conn
                    .prepareCall("{?=call lm_ticket_resource_summary_view_01(?)}"); // -
            // 5
            // Parameter
            // &
            // 1
            // output
            // parameter
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setString(2, jobId);
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                TicketResourceSummary resourceSummary = new TicketResourceSummary();
                resourceSummary.setResource_name(rs.getString("resource_name"));
                resourceSummary.setResource_quantity(rs
                        .getString("resource_quantity"));
                resourceSummary.setResource_unit_price(rs
                        .getString("resource_unit_price"));
                resourceSummary.setResource_total_price(rs
                        .getString("resource_total_price"));
                ticketResourceSummary.add(resourceSummary);
            }
        } catch (Exception e) {
            logger.error(
                    "getTicketResourceSummary(String, ArrayList, DataSource)",
                    e);
        } finally {
            DBUtil.close(rs, cstmt);
            DBUtil.close(conn);

        }
    }

    /**
     * To get Schedule Detail for excute tab.
     *
     * @param jobId
     * @param executeTab
     * @param ds
     */
    public static void getScheduleDetail(String jobId, ExecuteTab executeTab,
            DataSource ds) {
        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;

        try {

            conn = ds.getConnection();
            cstmt = conn
                    .prepareCall("{?=call lm_js_schedule_summary_view_01(?)}"); // -
            // 5
            // Parameter
            // &
            // 1
            // output
            // parameter
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setString(2, jobId);
            rs = cstmt.executeQuery();

            if (rs != null && rs.next()) {
                executeTab.getGeneralJobInfo().getScheduleInfo()
                        .setTimeOnSite(rs.getString("time_on_site"));
                executeTab.getGeneralJobInfo().getScheduleInfo()
                        .setTimeToRespond(rs.getString("time_to_respond"));
                executeTab.getGeneralJobInfo().getScheduleInfo()
                        .setCriticalityActual(rs.getString("iv_crit_name"));
                executeTab.getGeneralJobInfo().getScheduleInfo()
                        .setCriticalitySuggested("");
            }
        } catch (Exception e) {
            logger.error("getScheduleDetail(String, ExecuteTab, DataSource)", e);
        } finally {
            DBUtil.close(rs, cstmt);
            DBUtil.close(conn);
        }
    }

    /**
     * Discription: This method is used to cancel the Ticket.
     *
     * @param jobId -- Job Id
     * @param userid -- Login User Id
     * @param ds -- Database Object
     * @return null
     */
    public static int cancelTicket(long jobId, long userId, DataSource ds) {

        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        int returnvalue = 0;
        java.util.Date dateIn = new java.util.Date();

        try {
            conn = ds.getConnection();
            cstmt = conn.prepareCall("{?=call lm_ticket_cancel_01(?,?)}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.setLong(2, jobId);
            cstmt.setLong(3, userId);
            cstmt.execute();
            returnvalue = cstmt.getInt(1);
        } catch (Exception e) {
            logger.error("cancelTicket(long, long, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("cancelTicket(long, long, DataSource) - Exception in com.mind.newjobdb.dao.getPPSDate()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, cstmt);
            DBUtil.close(conn);
        }
        java.util.Date dateOut = new java.util.Date();
        if (logger.isDebugEnabled()) {
            logger.debug("cancelTicket(long, long, DataSource) - **** cancelTicket :: time taken :: "
                    + (dateOut.getTime() - dateIn.getTime()));
        }
        return returnvalue;
    } // end of createNewTicket() method

    public static boolean isOnSite(long jobId, DataSource ds) {

        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        boolean isCancellable = false;
        try {
            conn = ds.getConnection();
            String sql = "select dbo.func_check_ticket_for_cancellation(?)";
            pStmt = conn.prepareStatement(sql);
            pStmt.setLong(1, jobId);
            rs = pStmt.executeQuery();

            if (rs.next()) {
                if (rs.getString(1).equals("1")) {
                    isCancellable = true;
                }
            }
        } catch (Exception e) {
            logger.error("isOnSite(long, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("isOnSite(long, DataSource) - Exception in com.mind.newjobdb.dao.getPPSDate()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, pStmt);
            DBUtil.close(conn);
        }
        return isCancellable;
    } // end of createNewTicket() method

    /**
     * This method return Mpo id and PO id on the basis of Job Id.
     *
     * @param jobId
     * @param ds
     * @return
     */
    public static String[] getMpoPOId(String jobId, DataSource ds) {

        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;

        String[] mpoPOId = new String[3];
        try {
            conn = ds.getConnection();
            pStmt = conn
                    .prepareStatement("select * from func_get_default_mpo_po_id(?)");
            pStmt.setString(1, jobId);
            rs = pStmt.executeQuery();

            if (rs.next()) {
                mpoPOId[0] = rs.getString("mp_mn_id");
                mpoPOId[1] = rs.getString("lm_po_id");
                mpoPOId[2] = rs.getString("mp_appendix_id");
            } // - end of while
        } // - end of try
        catch (Exception e) {
            logger.error("getMpoPOId(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getMpoPOId(String, DataSource) - Exception in com.mind.newjobdb.dao.getPPSDate()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, pStmt);
            DBUtil.close(conn);
        }

        return mpoPOId;
    } // - end of getActivityList() method

    /**
     * Method is used to retrieve request type for a ticket.
     *
     * @param job Id
     * @param ds
     * @return Ticket request type.
     */
    public static String getTicketRequestType(String jobId, DataSource ds) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        String requestType = "";

        try {
            conn = ds.getConnection();

            String sql = "select lm_request_type from lm_ticket inner join lm_request_type "
                    + "on lm_tc_request_type = lm_request_id where lm_tc_js_id = ?"; // -
            // 1
            // parameter.

            pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, jobId);
            rs = pStmt.executeQuery();
            if (rs.next()) {
                requestType = rs.getString("lm_request_type");
            }
        } // - end of try
        catch (Exception e) {
            logger.error("getTicketRequestType(String, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getTicketRequestType(String, DataSource) - Exception in com.mind.newjobdb.dao.getPPSDate()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, pStmt);
            DBUtil.close(conn);
        }
        return requestType;
    } // - end of getTicketRequestType() method

    /**
     * Method is used to retrieve request type for a ticket.
     *
     * @param job Id
     * @param ds
     * @return Ticket request type.
     */
    public static boolean isCancelledTicket(long jobId, DataSource ds) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        boolean isCancelled = false;
        String sql = "";
        try {
            conn = ds.getConnection();
            sql = "select * from lm_resource inner join lm_activity on lm_at_id = lm_rs_at_id "
                    + "inner join iv_resource_pool on lm_rs_rp_id = iv_rp_id where lm_at_js_id = "
                    + jobId + " and iv_rp_name = 'Cancellation Fee' ";
            if (logger.isDebugEnabled()) {
                logger.debug("isCancelledTicket(long, DataSource)" + sql);
            }

            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                isCancelled = true;
            }
        } catch (Exception e) {
            logger.error("isCancelledTicket(long, DataSource) --" + sql, e);

            if (logger.isDebugEnabled()) {
                logger.debug("isCancelledTicket(long, DataSource) - Exception in com.mind.newjobdb.dao.getPPSDate()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, stmt);
            DBUtil.close(conn);
        }
        return isCancelled;
    } // end of createNewTicket() method

    /**
     * Discription: This method is used to get Customer Reference of
     * NetMedX/Appendix.
     *
     * @param appendixId -- Appendix Id
     * @param ds -- Database Object
     * @return String Customer Reference of NetMedX/Appendix.
     */
    public static String getNetMedXCutReference(long appendixId, DataSource ds) {

        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        String custReference = "";
        try {
            conn = ds.getConnection();
            String sql = "select isnull(lx_pr_customer_reference,'') from lx_appendix_main where lx_pr_id = ? ";
            pStmt = conn.prepareStatement(sql);
            pStmt.setLong(1, appendixId);
            rs = pStmt.executeQuery();

            if (rs.next()) {
                custReference = rs.getString(1);
            }
        } catch (Exception e) {
            logger.error("getNetMedXCutReference(long, DataSource)", e);

            if (logger.isDebugEnabled()) {
                logger.debug("getNetMedXCutReference(long, DataSource) - Exception in com.mind.newjobdb.dao.getPPSDate()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, pStmt);
            DBUtil.close(conn);
        }

        return custReference;
    } // end of getNetMedXCutReference() method

    /**
     * Concate Ticket Special Instraction and SOW with MPO Special Instraction.
     *
     * @param purchaseOrder
     * @param jobId
     * @param ds
     */
    public static void concateTicketSpecInstructionSOW(
            PurchaseOrder purchaseOrder, String jobId, DataSource ds) {

        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        String ticketSpecInstruction = "";
        String ticketProblemDesc = "";
        try {
            conn = ds.getConnection();
            String sql = "select "
                    + "lm_tc_spec_instruction = isnull(lm_tc_spec_instruction,''), "
                    + "lm_tc_problem_desc = isnull(lm_tc_problem_desc,'') "
                    + "from lm_ticket " + "where lm_tc_js_id = ? ";
            pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, jobId);
            rs = pStmt.executeQuery();

            if (rs.next()) {

                ticketSpecInstruction = Util.isNullOrBlank(rs
                        .getString("lm_tc_spec_instruction")) == true ? "" : rs
                        .getString("lm_tc_spec_instruction") + "\n\n";
                ticketProblemDesc = Util.isNullOrBlank(rs
                        .getString("lm_tc_problem_desc")) == true ? "" : rs
                        .getString("lm_tc_problem_desc") + "\n\n";
            }
        } catch (Exception e) {
            logger.error(
                    "concateTicketSpecInstructionSOW(PurchaseOrder, String, DataSource)",
                    e);

            if (logger.isDebugEnabled()) {
                logger.debug("concateTicketSpecInstructionSOW(PurchaseOrder, String, DataSource) - Exception in com.mind.newjobdb.dao.getPPSDate()"
                        + e);
            }
        } finally {
            DBUtil.close(rs, pStmt);
            DBUtil.close(conn);
        }
        purchaseOrder.getPowoInfo().setWoSpecialInstructions(
                (ticketSpecInstruction + purchaseOrder.getPowoInfo()
                .getWoSpecialInstructions()).trim());
        purchaseOrder.getPowoInfo().setWoNatureOfActivity(
                (ticketProblemDesc + purchaseOrder.getPowoInfo()
                .getWoNatureOfActivity()).trim());
    } // end of getNetMedXCutReference() method

}
