package com.mind.newjobdb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.mpo.PODTO;
import com.mind.bean.newjobdb.POResource;
import com.mind.common.Util;
import com.mind.fw.core.dao.util.DBUtil;

public class POResourceDAO {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(POResourceDAO.class);

	public ArrayList<POResource> getAllPossibleResources(PODTO bean,
			DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList<POResource> listOfPoResources = new ArrayList<POResource>();
		try {
			conn = ds.getConnection();
			String sql = "select * from dbo.func_lm_powo_resource_detail_tab(?,?)";
			if (logger.isDebugEnabled()) {
				logger.debug("getAllPossibleResources(PODTO, DataSource) - YYYYYYYYYYYYYY--> select * from dbo.func_lm_powo_resource_detail_tab("
						+ bean.getJobId() + "," + bean.getPoId() + ")");
			}
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, bean.getJobId());
			pStmt.setString(2, bean.getPoId());
			rs = pStmt.executeQuery();
			while (rs.next()) {
				POResource poResource = new POResource();
				poResource.setActivityId(rs.getString("lm_at_id"));
				poResource.setActivityName(Util.truncateString(
						rs.getString("lm_at_title"), 32));
				poResource.setActivityQuantity(rs.getFloat("actquantity"));
				poResource.setResourceId(rs.getString("lm_rs_id"));
				poResource.setResourceName(rs.getString("iv_rp_name"));
				poResource.setAsPlannedQuantity(rs
						.getFloat("lm_pwo_authorised_qty"));
				poResource.setAsPlannedUnitCost(rs
						.getFloat("lm_pwo_authorised_unit_cost"));
				poResource.setResourceQuantity(rs.getFloat("lx_ce_quantity"));
				poResource.setResourceSubName(rs.getString("iv_ct_name").trim()
						.equalsIgnoreCase("Travel") ? rs
						.getString("iv_mf_name") : rs.getString("iv_ct_name"));
				poResource
						.setResourceSubNamePre(rs.getString("iv_ct_name_pre"));
				poResource.setResourceSubId(rs.getString("iv_ct_id"));
				poResource
						.setRevision((rs.getString("revision").equals("0")) ? ""
								: rs.getString("revision"));
				poResource.setType(rs.getString("iv_ct_type_desc"));
				poResource.setTypeId(rs.getString("iv_ct_type"));
				poResource.setShowOnWO(Util.getBooleanval(rs
						.getString("lm_pwo_show_on_wo")));
				poResource.setAsplannedSubTotal(rs
						.getFloat("lm_pwo_authorised_total_cost"));
				poResource.setEstimatedCostTotal(rs
						.getFloat("lx_ce_total_cost"));
				poResource.setEstimatedCostUnit(rs.getFloat("unit_cost"));
				poResource.setInTransit(rs.getString("in_transit"));
				listOfPoResources.add(poResource);
			}
		} catch (Exception e) {
			logger.error("getAllPossibleResources(PODTO, DataSource)", e);

			logger.error("getAllPossibleResources(PODTO, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return listOfPoResources;
	}

	public static boolean isResourceExist(String jobId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		boolean isResource = false;
		try {
			conn = ds.getConnection();
			String sql = "select count(*) as cnt from (select lm_rs_id from lm_resource lr inner join "
					+ " \n (select lm_at_js_id,lm_at_id,lm_at_title from lm_activity where lm_at_js_id = ?)act "
					+ " \n on lr.lm_rs_at_id  = act.lm_at_id "
					+ " \n left outer join lx_resource_hierarchy_01 on lx_resource_hierarchy_01.iv_rp_id = lm_rs_rp_id "
					+ " \n left join lx_cost_element  on lx_ce_used_in = lm_rs_id "
					+ " \n and lx_ce_type in ('L', 'M', 'F', 'T', 'IL', 'IM', 'IF', 'IT', 'CL', 'CM', 'CF', 'CT', 'AL', 'AM', 'AF', 'AT')"
					+ " \n where lm_rs_id not in ("
					+ " \n select lm_pwo_rs_id  from lm_purchase_work_order where lm_pwo_po_id in ("
					+ " \n select lm_po_id from lm_purchase_order where lm_po_js_id = ?))) rs ";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, jobId);
			pStmt.setString(2, jobId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				if (rs.getInt("cnt") > 0)
					isResource = true;
			}
		} catch (Exception e) {
			logger.error("isResourceExist(String, DataSource)", e);

			logger.error("isResourceExist(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return isResource;
	}

	public ArrayList<POResource> getPoResources() {
		return null;
	}
}
