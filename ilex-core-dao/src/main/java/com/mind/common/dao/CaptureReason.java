package com.mind.common.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.fw.core.dao.util.DBUtil;

public class CaptureReason {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CaptureReason.class);

	public static int updateReasonForChange(String Id, String type,
			String reason, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int retval = 0;
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (type.trim().equalsIgnoreCase("MSA"))
				sql = "update lp_msa_detail set lp_md_reason_for_change = '"
						+ reason + "' where lp_md_id =" + Id;
			else if (type.trim().equalsIgnoreCase("Appendix"))
				sql = "update lx_appendix_detail set lx_pd_reason_for_change = '"
						+ reason + "' where lx_pd_pr_id =" + Id;
			else if (type.trim().equalsIgnoreCase("prj_job"))
				sql = "update lm_job set lm_js_reason_for_change = '" + reason
						+ "' where lm_js_id =" + Id;
			if (logger.isDebugEnabled()) {
				logger.debug("updateReasonForChange(String, String, String, DataSource) - "
						+ sql);
			}
			retval = stmt.executeUpdate(sql);

		} catch (Exception e) {
			logger.error(
					"updateReasonForChange(String, String, String, DataSource)",
					e);

			logger.error(
					"updateReasonForChange(String, String, String, DataSource)",
					e);
		} finally {
			DBUtil.close(stmt);
			DBUtil.close(conn);
		}
		return retval;
	}

}
