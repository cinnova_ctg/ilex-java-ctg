package com.mind.common.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.StateListBean;
import com.mind.dao.PM.Jobdao;
import com.mind.fw.core.dao.util.DBUtil;

public class UtilityDao {
	private static final Logger logger = Logger.getLogger(Jobdao.class);

	public static ArrayList getStateCategoryList(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select cp_country_id, cp_country_name, cp_country_order from lp_country_list where cp_country_id is not null order by cp_country_order,cp_country_name";
			rs = stmt.executeQuery(sql);
			int i = 0;

			if (i == 0) {
				StateListBean stateListBean = new StateListBean();
				stateListBean.setSiteCountryId("");
				stateListBean.setSiteCountryIdName("");
				valuelist.add(i, stateListBean);
				i++;
			}

			while (rs.next()) {
				StateListBean stateListBean = new StateListBean();
				stateListBean.setSiteCountryId(rs.getString("cp_country_id"));
				stateListBean.setSiteCountryIdName(rs
						.getString("cp_country_name"));
				valuelist.add(i, stateListBean);
				i++;
			}

		} catch (Exception e) {
			logger.error("getStateCategoryList(DataSource)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return valuelist;
	}
}
