package com.mind.common.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.bean.Schedule;
public class ScheduleViewUpdate 
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ScheduleViewUpdate.class);

	public static Schedule viewschedule( String Appendix_Id , String Job_Id , String Type , DataSource ds )
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		Schedule schedule = new Schedule();
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "";
			
			if( Type.equals( "Appendix" ) )
			{
				sql = "";
			}
			
			if( Type.equals( "Job" ) )
			{
				sql = " select ";
			}
			
			rs = stmt.executeQuery( sql );
				
			while ( rs.next() )
			{
					schedule.setScheduled_need_date( rs.getString( "" ) );
					schedule.setScheduled_effective_date( rs.getString( "" ) );
					schedule.setScheduled_planned_days(rs.getString( "" ) );
			}
		}
		catch( Exception e )
		{
			logger.error("viewschedule(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("viewschedule(String, String, String, DataSource) - Exception in retrieving schedule" + e);
			}
		}
		
		finally
		{
			try
			{
				
				if( stmt != null)
				{
					stmt.close();
				}	
			}
			catch( SQLException sql )
			{
				logger.error("viewschedule(String, String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("viewschedule(String, String, String, DataSource) - Exception in retrieving schedule" + sql);
				}
			}
			
			
			try
			{
				if(conn!=null)
				{	
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.error("viewschedule(String, String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("viewschedule(String, String, String, DataSource) - Exception in retrieving schedule" + sql);
				}
			}
			
		}
		return schedule;
	}
}
