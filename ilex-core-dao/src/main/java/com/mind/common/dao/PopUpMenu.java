package com.mind.common.dao;





public class PopUpMenu extends PopUpMenudao
{

	private String[] str_ar_hyperlink = null;
	private String[] str_ar_label = null;
	private int[] int_ar_length = null;
	private String[] str_ar_hyperlinkInfo = null;
	private int int_total_length = 800;
	private int int_td_added = 0;
	private  int pv_int_menuStart = 0;
	private String pv_str_toptclass = null;
	private String pv_str_tclass = null;
	private String pv_str_toptd_class = "toprow";
	private String pv_str_td_class = "toprow1";	
	private String pv_str_hyperlink_class = "drop";
	private String pv_str_filepath = null;
	private String[] str_ar_tdId = null;


	


	public void setpropFile(String filePath )
	{
		pv_str_filepath = filePath;

	}
	
	public void setTopTableClass(String toptClass )
	{
		pv_str_toptclass = toptClass;

	}

	public void setMenuStartLen(int menuStart )
	{
		pv_int_menuStart = menuStart;

	}

	public void setTableClass(String tClass )
	{
		pv_str_tclass = tClass;

	}

	public void setHyperlinkclass(String aClass)
	{
		pv_str_hyperlink_class = aClass;
		
	}

	public void setTopTdclass(String tdClass)
	{
		pv_str_toptd_class = tdClass;
		
	}
	public void setTdclass(String tdClass)
	{
		pv_str_td_class = tdClass;
		
	}
	

	public void setHyperlinkArray(String[] hyperlink)
	{
		str_ar_hyperlink = new String[hyperlink.length];
		str_ar_hyperlink = hyperlink;
	}

	public void setHyperlinkInfoArray(String[] hyperlinkInfo)
	{
		str_ar_hyperlinkInfo = new String[hyperlinkInfo.length];
		str_ar_hyperlinkInfo = hyperlinkInfo;
	}


	public void setLabelArray(String[] label)
	{
		if(pv_str_filepath != null)
		{
			
			if(label!=null)
			{
				
				str_ar_label = new String[label.length] ;
				
				str_ar_label = readDatafromFile(pv_str_filepath,label);
				
			}
		}
		else
		{
			str_ar_label = label;
			
		}
	}
/*
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
       <table width="" border="0" cellspacing="0" cellpadding="0" height="18">
        <tr align="center"> 
          
          <td  width=120 align=left><a href="ResourceAdd.do?f=<%=request.getParameter("resourceflag")%>&id=<%= request.getParameter("id")%>&id1=<%=request.getParameter("id1") %>&id2=<%=ppsid2 %>&function=add"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','../images/about1b.gif',1);" class="drop"> <bean:message bundle="RM" key="<%=menu_label1 %>"/> </a></td>
        
		  <td  width="200"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="drop"><bean:message bundle="RM" key="<%=menu_label2 %>"/> </a></td>
		</tr>
      </table>
    </td>
  </tr>
</table>


*/

	private void calculateTdLength()
	{
		int_ar_length = new int[str_ar_label.length];
		for(int i=0; i<str_ar_label.length;i++)
		{
			//int_ar_length[i] = (int)Math.round((str_ar_label[i].length()) * (100.00/(int_total_length-pv_int_menuStart)));
			int_ar_length[i] = (str_ar_label[i].length());
			int_td_added = int_td_added + int_ar_length[i];
			
		}

		

	}



	public String generatePopUpTopLevel()
	{
		String menu="<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n<tr >\n<td class = "+pv_str_toptd_class+" width='"+int_td_added+"%'>\n";
		if(str_ar_label!=null)
		{
		
			menu = menu+"<table  border='0' cellspacing='1' cellpadding='1'>\n<tr align=left>\n"; 
			str_ar_tdId = new String[str_ar_label.length];
			calculateTdLength();
			
			for(int i=0; i<str_ar_label.length;i++)
			{
			    //menu = menu+"<td  id=pop"+i+" width = '"+(str_ar_label[i].length()+1)+"%' align=left>";
				menu = menu+"<td  id=pop"+i+" width = '"+int_ar_length[i] +"%' align=left class = "+pv_str_td_class+">";
				menu = menu+"<a  href="+str_ar_hyperlink[i]+" "+str_ar_hyperlinkInfo[i]+"  class="+pv_str_hyperlink_class+" >";
				menu = menu+" "+str_ar_label[i]+"</a></td>";
				str_ar_tdId[i] = "pop"+i ;
			}
			menu = menu+"</tr>\n</table>"; 
			int_td_added = 100-int_td_added;
			menu = menu+"<td width='"+int_td_added+"%'>&nbsp;</td>"; 
			
		}

		menu = menu+ "</td>\n</tr>\n</table>";
		return menu;

	}


	public String[] getTdIdArray()
	{
		
		return str_ar_tdId;
		
	}
	


}