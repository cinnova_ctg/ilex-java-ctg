package com.mind.common.dao;

import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.UploadBean;
import com.mind.common.bean.Fileinfo;

public class Upload {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Upload.class);

	public static int fileupload(UploadBean uploadform, String used_type,
			String loginuserid, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		// ResultSet rs = null;
		int val = -1;

		try {
			conn = ds.getConnection();
			InputStream in = uploadform.getIn();

			byte[] buffer1 = new byte[uploadform.getFileSize()];

			in.read(buffer1);
			cstmt = conn
					.prepareCall("{?=call dbo.lx_file_upload_01(?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);

			cstmt.setInt(2, 0);

			cstmt.setInt(3, Integer.parseInt(uploadform.getId1()));
			cstmt.setString(4, used_type);
			cstmt.setString(5, uploadform.getFileName());
			cstmt.setString(6, uploadform.getRemarks());
			cstmt.setBytes(7, buffer1);
			cstmt.setString(8, uploadform.getFormaldocdate());
			cstmt.setString(9, uploadform.getStatus());
			cstmt.setInt(10, Integer.parseInt(loginuserid));
			cstmt.setString(11, "A");

			cstmt.execute();
			val = cstmt.getInt(1);

			/*
			 * conn = ds.getConnection(); InputStream in =
			 * file.getInputStream(); byte[] buffer1 = new
			 * byte[file.getFileSize()]; in.read( buffer1 );
			 * 
			 * stmt = conn.prepareStatement(
			 * "insert into file_master( File_Size , File_Name , File_Contents) values(?,?,?)"
			 * );
			 * 
			 * stmt.setLong( 1,file.getFileSize() ); stmt.setString(
			 * 2,file.getFileName() ); stmt.setBytes( 3,buffer1 );
			 * 
			 * stmt.executeUpdate();
			 */
		} catch (Exception e) {
			logger.error("fileupload(UploadForm, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("fileupload(UploadForm, String, String, DataSource) - Exception in uploading file"
						+ e);
			}
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"fileupload(UploadForm, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"fileupload(UploadForm, String, String, DataSource) - exception ignored",
						e);
			}
		}
		return val;
	}

	public static ArrayList<Fileinfo> getuploadedfiles(String file_id,
			String Id, String type, DataSource ds) {
		ArrayList<Fileinfo> uploadedfiles = new ArrayList<Fileinfo>();
		Statement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		Fileinfo fileinfo = null;
		// String filestatus = "";
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select * from dbo.func_lx_uploaded_list( '" + file_id
					+ "' , '" + Id + "' , '" + type + "')";

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				fileinfo = new Fileinfo();

				fileinfo.setFile_id(rs.getString(1));
				fileinfo.setFile_name(rs.getString(2));
				fileinfo.setFile_remarks(rs.getString(3));
				fileinfo.setFile_uploaded_by(rs.getString(5));
				fileinfo.setFile_uploaded_date(rs.getString(6));
				// filestatus = ;
				// if (filestatus.equals("1")) {
				//
				// filestatus = "received";
				// } else if (filestatus.equals("2")) {
				// filestatus = "approved/current";
				//
				// } else if (filestatus.equals("3")) {
				// filestatus = "denied";
				//
				// } else {
				//
				// filestatus = "archieved";
				// }
				fileinfo.setFile_status(rs.getString(7));
				uploadedfiles.add(fileinfo);
			}
		} catch (Exception e) {
			logger.error(
					"getuploadedfiles(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getuploadedfiles(String, String, String, DataSource) - Error caught in getting file info"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"getuploadedfiles(String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getuploadedfiles(String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getuploadedfiles(String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		return uploadedfiles;
	}

	public static Fileinfo getuploadedfiledata(String Id, String type,
			String check_for_uploaded_file, DataSource ds) {
		Statement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		Fileinfo fileinfo = new Fileinfo();
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (type.equals("MSA")) {
				if (check_for_uploaded_file.equals("U")) {
					sql = "select lp_md_uploaded_file_name , lp_md_uploaded_file_data from lp_msa_detail where lp_md_mm_id="
							+ Id;
				}

			}

			else {
				sql = "select lx_file_name , lx_file_data from lx_upload_file where lx_file_id="
						+ Id;
			}

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				fileinfo.setFile_name(rs.getString(1));
				fileinfo.setFile_data(rs.getBytes(2));
			}
		} catch (Exception e) {
			logger.error(
					"getuploadedfiledata(String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getuploadedfiledata(String, String, String, DataSource) - Error caught in getting file data"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"getuploadedfiledata(String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getuploadedfiledata(String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getuploadedfiledata(String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		return fileinfo;
	}

	public static ArrayList<com.mind.common.LabelValue> getNextstatuslist(
			String type, char currentstate, String loginuserid, DataSource ds) {
		ArrayList<com.mind.common.LabelValue> nextstatuslist = new ArrayList<com.mind.common.LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		nextstatuslist.add(new com.mind.common.LabelValue("---Select---", "0"));
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select cc_changed_status , cc_status_description from "
					+ "dbo.func_change_status_list('" + type + "' , '"
					+ currentstate + "' , '" + loginuserid + "')";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				nextstatuslist.add(new com.mind.common.LabelValue(rs
						.getString("cc_status_description"), rs
						.getString("cc_changed_status")));
			}
		} catch (Exception e) {
			logger.error("getNextstatuslist(String, char, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getNextstatuslist(String, char, String, DataSource) - Exception caught in rerieving nextstatuslist"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getNextstatuslist(String, char, String, DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getNextstatuslist(String, char, String, DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getNextstatuslist(String, char, String, DataSource) - exception ignored",
						s);
			}

		}
		return nextstatuslist;
	}

	public static int Deleteuploadedfile(String file_id, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		// ResultSet rs = null;
		int val = -1;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dbo.lx_file_upload_delete_01(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, file_id);
			cstmt.execute();
			val = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error("Deleteuploadedfile(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("Deleteuploadedfile(String, DataSource) - Exception in deleting uploaded files"
						+ e);
			}
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"Deleteuploadedfile(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"Deleteuploadedfile(String, DataSource) - exception ignored",
						e);
			}
		}
		return val;
	}

	public static String getFileUploadcheck(String file_id, String Id,
			String type, DataSource ds) {
		String uploadcheck = "N";
		Statement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		// Fileinfo fileinfo = null;
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select * from dbo.func_lx_uploaded_list( '" + file_id
					+ "' , '" + Id + "' , '" + type + "')";

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				uploadcheck = "U";
			}
		} catch (Exception e) {
			logger.error(
					"getFileUploadcheck(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getFileUploadcheck(String, String, String, DataSource) - Error caught in getting file info"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException s) {
				logger.warn(
						"getFileUploadcheck(String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getFileUploadcheck(String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getFileUploadcheck(String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		return uploadcheck;
	}

}
