package com.mind.common.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;



public class Customersortparamdao 
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Customersortparamdao.class);

	public static String[] Customersortparam( String appendix_id ,  DataSource ds )
	{
		String[] custsortparam = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String temp = "";
		
		if(appendix_id.equals("") || appendix_id.equals("-1")) {
			appendix_id = "%";
		}	
				
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "";
			
			if(appendix_id.equals("%"))
				sql = "select lm_ap_cust_info_parameter from lm_appendix_customer_info";
			else
				sql = "select lm_ap_cust_info_parameter from lm_appendix_customer_info where lm_ap_cust_info_pr_id="+appendix_id;
			rs = stmt.executeQuery( sql );
			
			while ( rs.next() )
			{
				temp = temp + "," + rs.getString( "lm_ap_cust_info_parameter" );
			}
			
			if(temp.length()>1)
			{
				temp = temp.substring( 1 , temp.length()  );
				custsortparam = temp.split( "," );
			}
			
		}
		
		catch( Exception e )
		{
			logger.error("Customersortparam(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("Customersortparam(String, DataSource) - Exception in retrieving lm_ap_cust_info_parameter" + e);
			}
		}
		
		finally
		{
			try
			{
				
				if( stmt != null)
				{
					stmt.close();
				}	
			}
			catch( SQLException sql )
			{
				logger.error("Customersortparam(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("Customersortparam(String, DataSource) - Exception Caught" + sql);
				}
			}
			
			
			try
			{
				if(conn!=null)
				{	
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.error("Customersortparam(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("Customersortparam(String, DataSource) - Exception Caught" + sql);
				}
			}
			
		}
		return custsortparam;
		
	}
}
