package com.mind.common.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;



public class AddComment 
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AddComment.class);

	public static int addcomment( String MSA_Id , String Appendix_Id , String Job_Id , String Activity_Id , String Resource_Id , String Type , String comment , String loginuserid , DataSource ds )
	{
		Connection conn = null;
		String Id = "";
		int val = -1;
		CallableStatement cstmt = null;
		
		if( Type.equals( "Jobdashboard" ) )
		{
			Type = "Job";
		}
		if( Type.equals( "MSA" ) )
		{
			Id = MSA_Id;
		}
		
		if( Type.equals( "Appendix" ) )
		{
			Id = Appendix_Id;
		}
		
		if( Type.equals( "Job" ) )
		{
			Id = Job_Id;
		}
		
		if( Type.equals( "Activity" ) )
		{
			Id = Activity_Id;
		}
		
		if( Type.contains( "pm_res" ) )
		{
			Id = Resource_Id;
		}
		
		if( Type.equals( "prm_appendix" ) )
		{
			Id = Appendix_Id;
			Type = "Appendix";
		}
		
		try
		{
			conn = ds.getConnection();
			
			cstmt = conn.prepareCall("{?=call dbo.add_comment( ? , ? , ? , ? ) }" );
			
			cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			
			cstmt.setInt( 2 , Integer.parseInt( Id ) );
			cstmt.setString( 3 , Type );
			cstmt.setString( 4 , comment );
			cstmt.setString( 5 , loginuserid );
			
			cstmt.execute();	
			val = cstmt.getInt( 1 );
		}
		
		catch( SQLException e )
		{
			logger.error("addcomment(String, String, String, String, String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addcomment(String, String, String, String, String, String, String, String, DataSource) - Exception caught in adding comment" + e);
			}
		}
		finally
		{
			try
			{
				if( cstmt != null )
				{
					cstmt.close();
				}	
			}
			catch( SQLException sql )
			{
				logger.error("addcomment(String, String, String, String, String, String, String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addcomment(String, String, String, String, String, String, String, String, DataSource) - Exception Caught in adding comment" + sql);
				}
			}
			
			
			try
			{
				if( conn != null )
				{	
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.error("addcomment(String, String, String, String, String, String, String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addcomment(String, String, String, String, String, String, String, String, DataSource) - Exception Caught in adding comment" + sql);
				}
			}
			
		}
		return val;	
	}	
	
	public static int addCustomerReference( String typeId , String type , String custRef , String loginuserid , DataSource ds )
	{
		Connection conn = null;
		String Id = "";
		int val = -1;
		CallableStatement cstmt = null;
		
		//System.out.println("typeId::::"+typeId);
		//System.out.println("type::::"+type);
		//System.out.println("custRef::::"+custRef);
		//System.out.println("loginuserid::::"+loginuserid);
		
		try
		{
			conn = ds.getConnection();
			
			cstmt = conn.prepareCall("{?=call dbo.add_customer_reference( ? , ? , ? , ? ) }" );
			
			cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			
			cstmt.setInt( 2 , Integer.parseInt( typeId ) );
			cstmt.setString( 3 , type );
			cstmt.setString( 4 , custRef );
			cstmt.setString( 5 , loginuserid );
			
			cstmt.execute();	
			val = cstmt.getInt( 1 );
		}
		
		catch( SQLException e )
		{
			logger.error("addCustomerReference(String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addCustomerReference(String, String, String, String, DataSource) - Exception caught in adding Customer Reference" + e);
			}
		}
		finally
		{
			try
			{
				if( cstmt != null )
				{
					cstmt.close();
				}	
			}
			catch( SQLException sql )
			{
				logger.error("addCustomerReference(String, String, String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addCustomerReference(String, String, String, String, DataSource) - Exception Caught in adding Customer Reference" + sql);
				}
			}
			
			
			try
			{
				if( conn != null )
				{	
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.error("addCustomerReference(String, String, String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addCustomerReference(String, String, String, String, DataSource) - Exception Caught in adding Customer Reference" + sql);
				}
			}
			
		}
		return val;	
	}	
	public static int addEndCustomer( String typeId , String type , String endCust , String loginuserid , DataSource ds )
	{
		Connection conn = null;
		String Id = "";
		int val = -1;
		CallableStatement cstmt = null;
		
		//System.out.println("typeId::::"+typeId);
		//System.out.println("type::::"+type);
		//System.out.println("custRef::::"+custRef);
		//System.out.println("loginuserid::::"+loginuserid);
		
		try
		{
			conn = ds.getConnection();
			
			cstmt = conn.prepareCall("{?=call dbo.add_end_customer( ? , ? , ? , ? ) }" );
			
			cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			
			cstmt.setInt( 2 , Integer.parseInt( typeId ) );
			cstmt.setString( 3 , type );
			cstmt.setString( 4 , endCust );
			cstmt.setString( 5 , loginuserid );
			
			cstmt.execute();	
			val = cstmt.getInt( 1 );
		}
		
		catch( SQLException e )
		{
			logger.error("addEndCustomer(String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addEndCustomer(String, String, String, String, DataSource) - Exception caught in adding End Customer" + e);
			}
		}
		finally
		{
			try
			{
				if( cstmt != null )
				{
					cstmt.close();
				}	
			}
			catch( SQLException sql )
			{
				logger.error("addEndCustomer(String, String, String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addEndCustomer(String, String, String, String, DataSource) - Exception Caught in adding End Customer" + sql);
				}
			}
			
			
			try
			{
				if( conn != null )
				{	
					conn.close();
				}
			}
			catch( SQLException sql )
			{
				logger.error("addEndCustomer(String, String, String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("addEndCustomer(String, String, String, String, DataSource) - Exception Caught in adding Customer Reference" + sql);
				}
			}
			
		}
		return val;	
	}
	public static String getCustomerReference(String typeId, String type, DataSource ds)
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		
		String customerReference = "";
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			if(type.equals("Appendix"))
				sql = "select lx_pr_customer_reference from lx_appendix_main where lx_pr_id = "+typeId;
			else
				sql = "select lm_js_customer_reference from lm_job where lm_js_id = "+typeId;
			
			
			rs = stmt.executeQuery( sql );
			
			if( rs.next() )
				customerReference = rs.getString( 1 );	
		}
		
		catch( Exception e )
		{
			logger.error("getCustomerReference(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomerReference(String, String, DataSource) - Error Occured in retrieving customer Reference" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( SQLException s )
			{
				logger.warn("getCustomerReference(String, String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( stmt != null ) 
					stmt.close();
			}
			catch( SQLException s )
			{
				logger.warn("getCustomerReference(String, String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( conn != null ) 
					conn.close();
			}
			catch( SQLException s )
			{
				logger.warn("getCustomerReference(String, String, DataSource) - exception ignored", s);
}
			
		}
		return customerReference;
	}
	public static String getEndCustomer(String typeId, String type, DataSource ds)
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		
		String endCustomer = "";
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select lx_pr_end_customer from lx_appendix_main where lx_pr_id = "+typeId;
					
			rs = stmt.executeQuery( sql );
			
			if( rs.next() )
				endCustomer = rs.getString( 1 );	
		}
		
		catch( Exception e )
		{
			logger.error("getEndCustomer(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEndCustomer(String, String, DataSource) - Error Occured in retrieving end customer" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs!= null ) 
				{
					rs.close();		
				}
			}
			
			catch( SQLException s )
			{
				logger.warn("getEndCustomer(String, String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( stmt != null ) 
					stmt.close();
			}
			catch( SQLException s )
			{
				logger.warn("getEndCustomer(String, String, DataSource) - exception ignored", s);
}
			
			
			try
			{
				if ( conn != null ) 
					conn.close();
			}
			catch( SQLException s )
			{
				logger.warn("getEndCustomer(String, String, DataSource) - exception ignored", s);
}
			
		}
		return endCustomer;
	}
	
	
	
}
