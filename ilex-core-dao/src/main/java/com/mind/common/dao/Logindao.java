package com.mind.common.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.LoginForm;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;

public class Logindao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Logindao.class);

	public static boolean isRoleSeniorProjectManager(String userid,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		boolean check = false;
		int count;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select count(lo_pc_title) from lo_poc where  lo_pc_title in ('Sr. Project Manager','Project Manager') and lo_pc_id="
							+ userid);

			if (rs.next()) {
				count = rs.getInt(1);
				if (count != 0)
					check = true;// User Logged in is Senior Project manager
				else
					check = false;// User Logged in is Non Senior Project
									// Manager
			}

		} catch (Exception e) {
			logger.error("checkRDMRole(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkRDMRole(String, DataSource) - Exception caught in Logindao for checkRDMRole  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"checkRDMRole(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkRDMRole(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkRDMRole(String, DataSource) - exception ignored",
						sql);
			}

		}
		return check;
	}

	public static LoginForm getUserInfo(String username, DataSource ds)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		LoginForm formdetail = new LoginForm();
		boolean chkXmlFile = false;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lo_us_pc_id, lo_pc_email1 , username from func_lo_login_user_info('"
					+ username + "')";
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				formdetail.setLogin_user_id(rs.getString("lo_us_pc_id"));
				formdetail.setLogin_user_email(rs.getString("lo_pc_email1"));
				formdetail.setLogin_user_name(rs.getString("username"));
			}
		}

		catch (Exception e) {
			logger.error("getUserInfo(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getUserInfo(String, DataSource) - Error occured during project identifier"
						+ e);
			}
		}

		finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error("getUserInfo(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getUserInfo(String, DataSource) - Error occured during closing Statement"
							+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.error("getUserInfo(String, DataSource)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getUserInfo(String, DataSource) - Error occured during closing Connection"
							+ e);
				}
			}

		}
		return formdetail;
	}

	public static LoginForm getUserInfoFromDocM(String username)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		LoginForm formdetail = new LoginForm();
		boolean chkXmlFile = false;
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = conn.createStatement();
			String sql = "select dm_lo_us_pc_id, dm_lo_pc_email1 ,username from func_lo_login_user_info('"
					+ username + "')";
			if (logger.isDebugEnabled()) {
				logger.debug("getUserInfoFromDocM(String) - " + sql);
			}
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				formdetail.setLogin_user_id(rs.getString("dm_lo_us_pc_id"));
				formdetail.setLogin_user_email(rs.getString("dm_lo_pc_email1"));
				formdetail.setLogin_user_name(rs.getString("username"));
			}
		}

		catch (Exception e) {
			logger.error("getUserInfoFromDocM(String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getUserInfoFromDocM(String) - Error occured during project identifier"
						+ e);
			}
		}

		finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error("getUserInfoFromDocM(String)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getUserInfoFromDocM(String) - Error occured during closing Statement"
							+ e);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.error("getUserInfoFromDocM(String)", e);

				if (logger.isDebugEnabled()) {
					logger.debug("getUserInfoFromDocM(String) - Error occured during closing Connection"
							+ e);
				}
			}

		}
		return formdetail;
	}

	public static String checkRDMRole(String userid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String check = "N";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			// rs = stmt.executeQuery(
			// "select 'Y' from lo_poc_rol inner join lo_role on lo_pr_ro_id = lo_ro_id where lo_pr_pc_id = "+7129+" and"
			// +
			// " lo_ro_role_name in ('RDM','RDS')");

			rs = stmt
					.executeQuery("select 'Y' from lo_poc_rol inner join lo_role on lo_pr_ro_id = lo_ro_id where lo_pr_pc_id = "
							+ userid + " and" + " lo_ro_role_name in ('RDM')");

			if (rs.next()) {
				check = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("checkRDMRole(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkRDMRole(String, DataSource) - Exception caught in Logindao for checkRDMRole  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"checkRDMRole(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkRDMRole(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkRDMRole(String, DataSource) - exception ignored",
						sql);
			}

		}
		return check;
	}

	public static String checkRoleForUser(String userid, String roleName,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String check = "N";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			// rs = stmt.executeQuery(
			// "select 'Y' from lo_poc_rol inner join lo_role on lo_pr_ro_id = lo_ro_id where lo_pr_pc_id = "+7129+" and"
			// +
			// " lo_ro_role_name in ('RDM','RDS')");

			rs = stmt
					.executeQuery("select top 1 lo_ro_role_name from lo_poc_rol inner join lo_role on lo_pr_ro_id = lo_ro_id where lo_pr_pc_id = "
							+ userid);
			if (rs.next()) {
				if (rs.getString(1) != null && rs.getString(1).equals(roleName)) {
					check = "Y";
				}
			}

		} catch (Exception e) {
			logger.error("checkRoleForUser(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkRoleForUser(String, String, DataSource) - Exception caught in Logindao for checkRDMRole  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"checkRoleForUser(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkRoleForUser(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkRoleForUser(String, String, DataSource) - exception ignored",
						sql);
			}

		}
		return check;
	}

	public static String checkRdmRdsRole(String userid, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String check = "N";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select 'Y' from lo_poc_rol inner join lo_role on lo_pr_ro_id = lo_ro_id where lo_pr_pc_id = "
							+ userid + " and" + " lo_ro_role_name in ('HR')");
			// + " lo_ro_role_name in ('RDM','RDS')");

			if (rs.next()) {
				check = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("checkRdmRdsRole(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkRdmRdsRole(String, DataSource) - Exception caught in Logindao for checkRdmRdsRole  :"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"checkRdmRdsRole(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkRdmRdsRole(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"checkRdmRdsRole(String, DataSource) - exception ignored",
						sql);
			}

		}
		return check;
	}

	public static ArrayList getManagerList(String userId, DataSource ds) {
		ArrayList managerList = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "";
		// query =
		// "select rl_mm_mg_nm from role_manager_mapping where rl_mm_nm = '"+roleName+"' order by rl_mm_mg_order";
		query = "select  distinct cc_fu_type, cc_fu_id, cc_fu_order_by_number  from lo_role_privilege a, lo_poc_rol b, cc_function c "
				+ "where b.lo_pr_pc_id = "
				+ userId
				+ " and a.lo_rp_ro_id = (select top 1 lo_pr_ro_id from lo_poc_rol where lo_pr_pc_id="
				+ userId
				+ ")  and a.lo_rp_fu_id = c.cc_fu_id "
				+ "and c.cc_fu_group_name = 'Menu Manager' order by cc_fu_order_by_number";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				managerList.add(new String(rs.getString("cc_fu_type")));
			}
			// managerList= resultSetAsList(rs);
		} catch (Exception e) {
			logger.error("getManagerList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getManagerList(String, DataSource) - Exception caught in getting Maanager List "
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.error("getManagerList(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getManagerList(String, DataSource) - Exception caught in closing Maanager Listresultset"
							+ sql);
				}
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.error("getManagerList(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getManagerList(String, DataSource) - Exception caught in closing Maanager List statement"
							+ sql);
				}
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.error("getManagerList(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getManagerList(String, DataSource) - Exception caught in closing Maanager List connection"
							+ sql);
				}
			}
		}

		return managerList;
	}

	public static ArrayList resultSetAsList(ResultSet rs) {
		ArrayList inner = null;
		ArrayList outer = new ArrayList();
		try {

			while (rs.next()) {
				inner = new ArrayList();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					inner.add(new String(rs.getBytes(rs.getMetaData()
							.getColumnName(i))));
				}
				outer.add(inner);
			}
		} catch (SQLException e) {
			logger.error("resultSetAsList(ResultSet)", e);

			logger.error("resultSetAsList(ResultSet)", e);
		}
		return outer;
	}
}
