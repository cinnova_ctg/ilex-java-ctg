package com.mind.common.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.IlexConstants;
import com.mind.fw.core.dao.util.DBUtil;

public class DocMIlexDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DocMIlexDao.class);

	public static final String MSAApproved = "APS"; // ( i.e Approved/Pending
													// Customer Review/Signed )
	public static final String AppendixApproved = "APSI"; // ( i.e
															// Approved/Pending
															// Customer
															// Review/Signed/Inwork
															// )
	public static final String prjJobApproved = "APSI"; // ( i.e
														// Approved/Pending
														// Customer
														// Review/Signed/Inwork)

	public static ArrayList getMsalist() {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select lam.lx_pr_id,lmn.lp_mm_id,lot.lo_ot_name from lp_msa_main lmn join lo_organization_top lot on"
							+ " \n lp_mm_ot_id = lo_ot_id "
							+ " \n join lx_appendix_main lam on"
							+ " \n lmn.lp_mm_id = lam.lx_pr_mm_id"
							+ " \n and lx_pr_type = 3"
							+ " \n order by lot.lo_ot_name");
			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("lo_ot_name"), rs.getString("lp_mm_id")
						+ "~" + rs.getString("lx_pr_id") + "~"
						+ rs.getString("lo_ot_name")));
			}
		}

		catch (Exception e) {
			logger.error("getMsalist()", e);

			logger.error("getMsalist()", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return list;
	}

	public static java.util.Collection getMonthName() {

		ArrayList monthname = new java.util.ArrayList();
		monthname.add(new com.mind.common.LabelValue("--", ""));
		monthname.add(new com.mind.common.LabelValue("Jan", "1"));
		monthname.add(new com.mind.common.LabelValue("Feb", "2"));
		monthname.add(new com.mind.common.LabelValue("Mar", "3"));
		monthname.add(new com.mind.common.LabelValue("Apr", "4"));
		monthname.add(new com.mind.common.LabelValue("May", "5"));
		monthname.add(new com.mind.common.LabelValue("Jun", "6"));
		monthname.add(new com.mind.common.LabelValue("Jul", "7"));
		monthname.add(new com.mind.common.LabelValue("Aug", "8"));
		monthname.add(new com.mind.common.LabelValue("Sep", "9"));
		monthname.add(new com.mind.common.LabelValue("Oct", "10"));
		monthname.add(new com.mind.common.LabelValue("Nov", "11"));
		monthname.add(new com.mind.common.LabelValue("Dec", "12"));

		return monthname;
	}

	public static java.util.Collection getReportName() {
		ArrayList reportname = new java.util.ArrayList();
		reportname.add(new com.mind.common.LabelValue("--Select--", ""));
		reportname.add(new com.mind.common.LabelValue(
				"Detailed Summary Report For NetMedX", "1"));
		return reportname;
	}

	public static String getStatus(String type, String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String status = "";
		String sql = null;

		if (Id == null || Id.equals(""))
			Id = "0";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (type.equals("MSA"))
				sql = "select lp_md_status from lp_msa_detail where lp_md_id ="
						+ Id;
			else if (type.equals("Appendix"))
				sql = "select lx_pr_status from lx_appendix_main where lx_pr_id ="
						+ Id;
			else if (type.equals("Addendum"))
				sql = "select lm_js_status from lm_job where lm_js_id =" + Id;
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				status = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("getStatus(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStatus(String, String, DataSource) - Exception occurred while getting the status: Class :com.mind.common.dao"
						+ e);
			}
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getStatus(String, String, DataSource) - exception ignored",
						e);
			}
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
				logger.warn(
						"getStatus(String, String, DataSource) - exception ignored",
						e);
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getStatus(String, String, DataSource) - exception ignored",
						e);
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getStatus(String, String, DataSource) - Status of the MSA Is : "
					+ status);
		}
		return status;
	}

	public static boolean isEntityApproved(String type, String Id,
			String viewType, String jobId, DataSource ds) {
		// System.out.println("type :" + type);
		// System.out.println("Id :" + Id);
		// System.out.println("viewType :" + viewType);
		// System.out.println("jobId :" + jobId);
		boolean check = false;
		if (jobId.trim().equalsIgnoreCase("0"))
			jobId = "";
		if (viewType != null && viewType.equalsIgnoreCase("PDF")) {
			if (jobId != null && !jobId.trim().equalsIgnoreCase("")
					&& getjobType(jobId, ds).equalsIgnoreCase("Addendum")) {
				if (prjJobApproved.indexOf(getStatus("Addendum", jobId, ds)
						.trim()) != -1)
					check = true;
			} else {
				if (type.equals("MSA")
						&& (MSAApproved.indexOf(getStatus(type, Id, ds).trim()) != -1))
					check = true;
				else if (type.equals("Appendix")
						&& (AppendixApproved.indexOf(getStatus(type, Id, ds)
								.trim()) != -1))
					check = true;
			}
		}
		return check;
	}

	public static String getjobType(String Id, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String jobType = null;
		String sql = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (Id != "") {
				sql = "select lm_js_type from lm_job where lm_js_id = " + Id;
			}
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				jobType = rs.getString(1);
			}
		} catch (Exception e) {
			logger.warn("getjobType(String, DataSource) - exception ignored", e);

		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getjobType(String, DataSource) - exception ignored", e);
			}
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
				logger.warn(
						"getjobType(String, DataSource) - exception ignored", e);
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getjobType(String, DataSource) - exception ignored", e);
			}
		}

		return jobType;
	}
}
