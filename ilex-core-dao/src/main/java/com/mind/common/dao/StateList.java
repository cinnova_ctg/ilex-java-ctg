package com.mind.common.dao;

import java.util.ArrayList;

import javax.sql.DataSource;

import com.mind.common.StateListBean;
import com.mind.dao.PRM.SiteManagedao;

public class StateList {
	
	public static ArrayList getStateList(DataSource ds){
		StateListBean stateBean = new StateListBean();
		
		stateBean.setInitialStateCategory( UtilityDao.getStateCategoryList(ds) );
		String tempcat = "";
		if(stateBean.getInitialStateCategory().size() > 0){
			for(int i = 0; i < stateBean.getInitialStateCategory().size(); i++) {
				tempcat = ((StateListBean) stateBean.getInitialStateCategory().get(i)).getSiteCountryId();
				((StateListBean) stateBean.getInitialStateCategory().get(i)).setTempList(SiteManagedao.getCategoryWiseStateList(tempcat,i, ds));
			}	
		}
		return stateBean.getInitialStateCategory();
	}

}
