/*
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: This is a data access object class used for populating comboboxes used in resource manager.     
*
*/
package com.mind.common.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.LabelValue;

/*
 * Methods : getSubcatglist,getThirdcatglist,getStatuslist
 */

public class DynamicComboDaoRM
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DynamicComboDaoRM.class);
	
/** This method gets list of all 2nd level categories to populate the combo box. 
 * @param id							String. id of 1st level category
 * @param ds   			       	    	Object of DataSource
 * @return ArrayList              	    This method returns ArrayList,list of 2nd level categories.
 */	  
	public ArrayList getSubcatglist(String id, DataSource ds)
	{
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add( new com.mind.common.LabelValue( "---Select---" , "0") );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery( "select iv_mf_name,iv_mf_id  from iv_manufacturer where iv_mf_ct_id = '"+id+"' order by iv_mf_name" );
			while( rs.next() )
			{
				list.add ( new com.mind.common.LabelValue( rs.getString( "iv_mf_name" ), rs.getString( "iv_mf_id" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getSubcatglist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSubcatglist(String, DataSource) - Exception caught" + e);
			}
		}
		finally
		{
			
			try
			{
				rs.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getSubcatglist(String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				stmt.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getSubcatglist(String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				conn.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getSubcatglist(String, DataSource) - exception ignored", sql);
}
			
		}
		return list;
	}
	
	
/** This method gets list of all 3rd level categories to populate the combo box. 
 * @param id1							String. id of 2nd level category
 * @param ds   			       	    	Object of DataSource
 * @return ArrayList              	    This method returns ArrayList,list of 3rd level categories.
 */	 
	
	public ArrayList getThirdcatglist(String id1, DataSource ds)
	{
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add( new com.mind.common.LabelValue( "---Select---" , "0") );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery( "select iv_sc_name,iv_sc_id  from iv_subcategory where iv_sc_mf_id = '"+id1+"' order by iv_sc_name" );
			while( rs.next() )
			{
				list.add ( new com.mind.common.LabelValue( rs.getString( "iv_sc_name" ), rs.getString( "iv_sc_id" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getThirdcatglist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getThirdcatglist(String, DataSource) - Exception caught" + e);
			}
		}
		finally
		{
			
			try
			{
				rs.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getThirdcatglist(String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				stmt.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getThirdcatglist(String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				conn.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getThirdcatglist(String, DataSource) - exception ignored", sql);
}
			
		}
		return list;
	}
	
	
	
	
	/** This method gets list of status of resource to populate the combo box. 
	 * @param mgrtype						String. identifier for resource manager. to get list according to a particular manager.
	 * @param ds   			       	    	Object of DataSource
	 * @return ArrayList              	    This method returns ArrayList,list of status of resouce.
	 */	 
	
	public ArrayList getStatuslist(String mgrtype, DataSource ds)
	{
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		//list.add( new com.mind.common.LabelValue( "---Select---" , "0") );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql;
			sql="select *  from cc_status_master where cc_function like '%"+mgrtype+"%' ";
			
			rs = stmt.executeQuery( sql );
			
			while( rs.next() )
			{
				list.add ( new com.mind.common.LabelValue( rs.getString( "cc_status_description" ), rs.getString( "cc_status_code" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getStatuslist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStatuslist(String, DataSource) - Exception caught" + e);
			}
		}
		finally
		{
			
			try
			{
				rs.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getStatuslist(String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				stmt.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getStatuslist(String, DataSource) - exception ignored", sql);
}
			
			
			try
			{
				conn.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getStatuslist(String, DataSource) - exception ignored", sql);
}
			
		}
		return list;
	}
	
	
	
}
