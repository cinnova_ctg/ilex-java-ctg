package com.mind.common.dao;

import java.io.InputStream;
import java.net.ConnectException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.Node;

import org.apache.log4j.Logger;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/*
 * Copyright (C) 2005 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * 
 **/

/**
 * Class Explanation: RestClint class is for get Latitude and Longitude on the
 * basis of Address from google with the help of googlekey. or get Latitude and
 * Longitude on the basis of Zip Code form Database table cp_address_master.
 * 
 * @author MIND
 **/

public class RestClient {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(RestClient.class);

	/**
	 * Method Explanation: This method is use to search latitude and longitude
	 * from google/database on the basis of Address/Zip Code. First it's search
	 * Latitude/Longitude from google on the basis of Address, City, State, and
	 * return Latitude/Longitude in case google doesn't return
	 * Latitude/Longitude then again this method call database and search
	 * Latitude/Longitude on the basis of Zip Code from cp_partner_table and
	 * return Latitude/Longitude.
	 * 
	 * @param address
	 *            - Frist line address city - City Name state - State id country
	 *            - Country id zipcode - Zip Code ds - Databse object
	 * @return String[] - String[0]=Accuracy , String[1]= Source, String[2]=
	 *         Latitude, String[3]= Longitude, String[4]= true/false
	 * @see
	 */

	public static String[] getLatitudeLongitude(String address, String city,
			String state, String country, String zipcode, DataSource ds) {

		// String googleKey =
		// "ABQIAAAAvWa-T17jvPeaqSB_tdW4-BS82OmwBDBgdEDcpxYX-S_1TYbAYxQ6Ur5U-TWlrMAPtk270jDnWchWoQ";
		String LatLong = "";
		String searchStr = "";

		String[] result = new String[5];
		String[] resultAddress = new String[5];
		String[] resultZip = new String[5];
		boolean addresscheck = true;
		boolean zipcheck = true;

		if (address != null && !address.equals("")) {
			searchStr += address + "+";
		}
		if (city != null && !city.equals("")) {
			searchStr += city + "+";
		}
		if (state != null && !state.equals("")) {
			searchStr += state + "+";
		}
		if (country != null && !country.equals("")) {
			searchStr += country + "+";
		}

		if (country != null && (country.equals("US") || country.equals("CA"))) {
			if (zipcode != null && !zipcode.equals("")) {
				searchStr += zipcode;
			}
		} else {
			searchStr = searchStr.substring(0, searchStr.length() - 1);
		}

		if (searchStr != null && !searchStr.equals("")) {
			searchStr = searchStr.replaceAll(" ", "+");
		}

		searchStr = searchStr.replaceAll("#", " ");
		searchStr = searchStr.replaceAll(" ", "");

		// Call Google: Start
		try {
			// New Code Api url
			String endpointAddress = "http://maps.googleapis.com/maps/api/geocode/xml?address="
					+ searchStr + "&sensor=false";

			// + "&output=xml&key="
			// + EnvironmentSelector.getBundleString("googlekey."
			// + IlexConstants.SERVER_NAME);

			// Old Deprecated Api url
			/*
			 * String endpointAddress = "http://maps.google.com/maps/geo?q=" +
			 * searchStr + "&output=xml&key=" +
			 * EnvironmentSelector.getBundleString("googlekey." +
			 * IlexConstants.SERVER_NAME);
			 */

			/*
			 * URL url = new URL(endpointAddress); InputStream in =
			 * url.openStream(); StreamSource source = new StreamSource(in);
			 * ByteArrayOutputStream bos = new ByteArrayOutputStream();
			 * StreamResult sr = new StreamResult(bos); Transformer trans =
			 * TransformerFactory.newInstance() .newTransformer(); Properties
			 * oprops = new Properties();
			 * oprops.put(OutputKeys.OMIT_XML_DECLARATION, "no");
			 * trans.setOutputProperties(oprops); trans.transform(source, sr);
			 * String xmlStr = bos.toString(); bos.close();
			 */
			// String code = xmlStr.substring(xmlStr.indexOf("<code>") + 6,
			// xmlStr.indexOf("</code>"));

			// ////////////////////////////////////////////////////////////
			//

			// code final

			// //////////////////////////////////////////

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

			// tsuji:note [b] change this
			// Document doc = docBuilder.parse (new File("book.xml"));
			URL url = new URL(endpointAddress);
			InputStream stream = url.openStream();
			/*
			 * String xml = "<GeocodeResponse>" + "<status>OK</status>" +
			 * "<geometry>" + "<location>" + "<lat>40.1351907</lat>" +
			 * "<lng>-82.0221884</lng>" + "</location>" + "</geometry>" +
			 * "</GeocodeResponse>";
			 */
			Document doc = docBuilder.parse(stream);
			// / Document doc = docBuilder.parse(xml);
			// normalize text representation
			doc.getDocumentElement().normalize();
			// System.out.println("Root element of the map file  is "
			// + doc.getDocumentElement().getNodeName());

			String lat = null;
			String lng = null;
			// -------getting value of long&lati from api
			NodeList nodeLst = doc.getElementsByTagName("location");

			for (int i = 0; i < nodeLst.getLength(); i++) {
				org.w3c.dom.Node node = nodeLst.item(i);
				Element e = (Element) node;
				org.w3c.dom.Node childNode = nodeLst.item(i);

				NodeList nodeList = e.getElementsByTagName("lat");
				lat = nodeList.item(0).getChildNodes().item(0).getNodeValue();
				// System.out.println("lat: "
				// + nodeList.item(0).getChildNodes().item(0)
				// .getNodeValue());

				nodeList = e.getElementsByTagName("lng");
				lng = nodeList.item(0).getChildNodes().item(0).getNodeValue();
				// System.out.println("lng: "
				// + nodeList.item(0).getChildNodes().item(0)
				// .getNodeValue());
				// System.out.println("data in lat & lng" + i + ": "
				// + childNode.getTextContent());
			}

			if (lat != null && !lat.equals("")) {

				result[0] = "Street Address";
				result[1] = "Google";
				result[2] = lng + "";
				result[3] = lat + "";

			} else {
				addresscheck = false;
			}

			/*
			 * for (int s = 0; s < nodeLst.getLength(); s++) {
			 * 
			 * org.w3c.dom.Node fstNode = nodeLst.item(s);
			 * 
			 * if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
			 * 
			 * Element fstElmnt = (Element) fstNode; NodeList fstNmElmntLst =
			 * fstElmnt .getElementsByTagName("firstname"); Element fstNmElmnt =
			 * (Element) fstNmElmntLst.item(0); NodeList fstNm =
			 * fstNmElmnt.getChildNodes(); System.out.println("First Name : " +
			 * ((Node) fstNm.item(0)).getNodeValue()); NodeList lstNmElmntLst =
			 * fstElmnt .getElementsByTagName("lastname"); Element lstNmElmnt =
			 * (Element) lstNmElmntLst.item(0); NodeList lstNm =
			 * lstNmElmnt.getChildNodes(); System.out.println("Last Name : " +
			 * ((Node) lstNm.item(0)).getNodeValue());
			 * 
			 * NodeList conditionList = doc .getElementsByTagName("geometry");
			 * 
			 * for (int k = 0; k < conditionList.getLength(); ++k) { Element
			 * condition = (Element) conditionList.item(k); String conditionText
			 * = condition.getFirstChild() .getNodeValue(); } //
			 * //////////////////////////// NodeList Location =
			 * doc.getElementsByTagName("location"); String conditionTextlat =
			 * ((org.w3c.dom.Node) Location) .getFirstChild().getNodeValue();
			 * String conditionTextlng = ((org.w3c.dom.Node) Location)
			 * .getFirstChild().getNodeValue(); for (int i = 0; i <
			 * Location.getLength(); i++) { Element location = (Element)
			 * Location.item(i); String lat =
			 * location.getElementsByTagName("lat") .item(0).getTextContent();
			 * String lng = location.getElementsByTagName("lng")
			 * .item(0).getTextContent();
			 */
			// System.out.println(lat.getTextContent() +
			// lng.getTextContent());
			/*
			 * NodeList listlng = doc.getElementsByTagName("lng");
			 * 
			 * System.out.println(listOf.getNodeValue().trim());
			 * 
			 * int total = listOf.getLength(); Element latElement =(Element)
			 * lastNameList.item(0); NodeList textLNList
			 * =lastNameElement.getChildNodes(); System.out
			 * .println("Last Name : " + ((Node) textLNList.item(0))
			 * .getNodeValue().trim());
			 */

			/*
			 * result[0] = "Street Address"; result[1] = "Google";
			 */
			/*
			 * result[2] = listOf; result[3] = listlng;
			 */

			// System.out.println("Total no of latitude : " +
			// total);
			/*
			 * for (int s = 0; s < s.getLength(); s++) { Node firstPersonNode =
			 * (Node) listOf.item(s); if (firstPersonNode.getNodeType() ==
			 * Node.ELEMENT_NODE) { Element firstElement = (Element)
			 * firstPersonNode; // ------- NodeList firstNameList = firstElement
			 * .getElementsByTagName("first"); Element firstNameElement =
			 * (Element) firstNameList .item(0); NodeList textFNList =
			 * firstElement.getChildNodes(); System.out.println("First Name : "
			 * + ((Node) textFNList.item(0)).getNodeValue() .trim()); // -------
			 * 
			 * NodeList lastNameList = firstElement
			 * .getElementsByTagName("last"); Element lastNameElement =
			 * (Element) lastNameList .item(0); NodeList textLNList =
			 * lastNameElement.getChildNodes();
			 * System.out.println("Last Name : " + ((Node)
			 * textLNList.item(0)).getNodeValue() .trim());
			 * 
			 * // ----
			 * 
			 * NodeList ageList = firstElement.getElementsByTagName("age");
			 * Element ageElement = (Element) ageList.item(0); NodeList
			 * textAgeList = ageElement.getChildNodes();
			 * System.out.println("Age : " + ((Node)
			 * textAgeList.item(0)).getNodeValue() .trim());
			 * 
			 * 
			 * 
			 * DocumentBuilder db = DocumentBuilderFactory.newInstance()
			 * .newDocumentBuilder(); InputSource is = new InputSource();
			 * is.setCharacterStream(new StringReader(xmlStr));
			 * 
			 * Document doc = db.parse(is); NodeList nodes =
			 * doc.getElementsByTagName("lat");
			 * 
			 * for (int i = 0; i < nodes.getLength(); i++) { Element element =
			 * (Element) nodes.item(i);
			 * 
			 * NodeList name = element.getElementsByTagName("lng"); Element line
			 * = (Element) name.item(0);
			 * 
			 * System.out .println("Name: " +
			 * getCharacterDataFromElement(line));
			 * 
			 * 
			 * NodeList title = element.getElementsByTagName("title"); line =
			 * (Element) title.item(0); System.out.println("Title: " +
			 * getCharacterDataFromElement(line));
			 * 
			 * 
			 * // /////////////////////////////////////////////////// //////////
			 * 
			 * old code if (code != null && code.trim().equals("200")) { int
			 * accuracy = Integer.parseInt(xmlStr.substring(
			 * xmlStr.indexOf("Accuracy=") + 10, xmlStr.indexOf("Accuracy=") +
			 * 11)); if (xmlStr.indexOf("<coordinates>") > -1) { String
			 * coordinates = xmlStr.substring( xmlStr.indexOf("<coordinates>") +
			 * 13, xmlStr.indexOf("</coordinates>")); LatLong = coordinates; }
			 * if (accuracy > 6) {
			 * 
			 * // * This block of code will execute when accuracy of the // *
			 * address is more than 6.
			 * 
			 * if (LatLong != null && !LatLong.equals("")) { double longdeg =
			 * Double.parseDouble((LatLong.substring( 0,
			 * LatLong.indexOf(',')))); // Longitude in // decimal format double
			 * latdeg = Double.parseDouble(LatLong.substring(
			 * LatLong.indexOf(',') + 1, LatLong.lastIndexOf(','))); // Latitude
			 * in // decimal format
			 * 
			 * resultAddress[0] = "Street Address"; // Accuracy resultAddress[1]
			 * = "Google"; // Source resultAddress[2] = longdeg + "";
			 * resultAddress[3] = latdeg + ""; } } else { addresscheck = false;
			 * }
			 * 
			 * } else { addresscheck = false; }
			 * 
			 * 
			 * result[0] = resultAddress[0]; result[1] = resultAddress[1];
			 * result[2] = resultAddress[2]; result[3] = resultAddress[3];
			 * 
			 * }
			 * 
			 * }
			 */

			/*
			 * } }
			 * 
			 * }
			 */
		} catch (ConnectException e) {
			logger.error(
					"getLatitudeLongitude(String, String, String, String, String, DataSource)",
					e);

			addresscheck = false;
		} catch (Exception e) {
			logger.error(
					"getLatitudeLongitude(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getLatitudeLongitude(String, String, String, String, String, DataSource) - Exception in geting Latitude and Longitude though Address"
						+ e);
			}
		}
		// Call Google: End

		// Call Database: Start
		if (!addresscheck) { // its run when google return null value of
								// lat/long or accuracy in less then 7.
			if (country != null
					&& (country.equals("US") || country.equals("CA"))) {
				Connection conn = null;
				Statement stmt = null;
				ResultSet rs = null;

				try {
					conn = ds.getConnection();
					stmt = conn.createStatement();
					if (zipcode.indexOf('\'') >= 0)
						zipcode = zipcode.replaceAll("\'", "\'\'");
					String sql = "select top 1 Latitude,Longitude from cp_address_master where ZIPcode='"
							+ zipcode + "'";
					rs = stmt.executeQuery(sql);

					if (rs.next()) {
						resultZip[0] = "Zip Code"; // Accuracy
						resultZip[1] = "Zip Code"; // Source
						resultZip[2] = rs.getString("Longitude");// Longitude in
																	// decimal
																	// farmat
						resultZip[3] = rs.getString("Latitude");// Latitude in
																// decimal
																// farmat
					} else {
						zipcheck = false;
					}

					result[0] = resultZip[0];
					result[1] = resultZip[1];
					result[2] = resultZip[2];
					result[3] = resultZip[3];
				} catch (Exception e) {
					logger.error(
							"getLatitudeLongitude(String, String, String, String, String, DataSource)",
							e);

					if (logger.isDebugEnabled()) {
						logger.debug("getLatitudeLongitude(String, String, String, String, String, DataSource) - Error occured during getting ZIPCode from database "
								+ e);
					}
				}

				finally {
					try {
						if (rs != null) {
							rs.close();
						}
					} catch (Exception e) {
						logger.warn(
								"getLatitudeLongitude(String, String, String, String, String, DataSource) - exception ignored",
								e);
					}

					try {
						if (stmt != null)
							stmt.close();
					}

					catch (Exception e) {
						logger.warn(
								"getLatitudeLongitude(String, String, String, String, String, DataSource) - exception ignored",
								e);
					}

					try {
						if (conn != null)
							conn.close();
					}

					catch (Exception e) {
						logger.warn(
								"getLatitudeLongitude(String, String, String, String, String, DataSource) - exception ignored",
								e);
					}
				} // End of finally block
			} else { // Else of Country check.
				zipcheck = false;
			} // End of zipCode.
		} // End of block if(!addresscheck).
			// Call Database: End

		if (!zipcheck) {
			result[0] = "None"; // Accuracy
			result[1] = "None"; // Source
			result[2] = resultAddress[2]; // Longitude
			result[3] = resultAddress[3]; // Latitude
		}

		if (!addresscheck) {
			result[4] = "false"; // when google return address accuracy less
									// then 7 or null.
		} else {
			result[4] = "true";// when google return address accuracy greater
								// then 6.
		}
		/*
		 * System.out.println("Accuracy="+result[0]);
		 * System.out.println("source="+result[1]);
		 * System.out.println("Longitude[X]="+result[2]);
		 * System.out.println("Latitude[Y]="+result[3]);
		 * System.out.println("accuracy_check="+result[4]);
		 */
		return result;
	}

	// read
	public static String getCharacterDataFromElement(Element e) {
		Node child = (Node) e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}
}
