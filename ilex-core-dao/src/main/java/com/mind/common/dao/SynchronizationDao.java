package com.mind.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.mind.common.IlexConstants;
import com.mind.common.SyncForm;
import com.mind.common.SyncMapping;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.fw.core.dao.util.DBUtil;

public class SynchronizationDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(SynchronizationDao.class);

	public static void syncOrganizationType() {
		ArrayList list = new ArrayList();
		Connection ilexConn = null;
		Connection docmConn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		Statement stmt = null;
		ResultSet rs = null;
		int updateResult = 0;
		int insertResult = 0;
		try {
			ilexConn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			docmConn = DatabaseUtils
					.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = ilexConn.createStatement();
			// Prepare Statement for Update Query
			pstmt = docmConn
					.prepareStatement(" update dm_lo_organization_type "
							+ " set dm_lo_organization_type_is_external = ?, "
							+ " dm_lo_organization_type_name = ? "
							+ " where dm_lo_organization_type_ilexkey = ?");
			// Prepare Statement for Insert Query
			pstmt1 = docmConn
					.prepareStatement("insert into dm_lo_organization_type values (?,?,?,?)");

			rs = stmt.executeQuery("select * from lo_organization_type ");
			while (rs.next()) {
				SyncForm syncform = new SyncForm();
				syncform.setOrgType(rs.getString("lo_organization_type"));
				syncform.setOrgTypeName(rs
						.getString("lo_organization_type_name"));

				// Update or insert the DocM database table based on the record
				// existence in the Ilex Tables
				try {
					if (SyncMapping.checkRecordExists("lo_organization_type",
							syncform, docmConn)) { // This one is for update
						pstmt.setString(1, "Ilex");
						pstmt.setString(2, syncform.getOrgTypeName());
						pstmt.setString(3, syncform.getOrgType());
						updateResult = pstmt.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("syncOrganizationType() - updateResult "
									+ updateResult);
						}
					} else { // This one is for insert
						pstmt1.setString(1, syncform.getOrgType());
						pstmt1.setString(2, syncform.getOrgTypeName());
						pstmt1.setString(3, "Ilex");
						pstmt1.setString(4, syncform.getOrgType());
						insertResult = pstmt1.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("syncOrganizationType() - insertResult "
									+ insertResult);
						}
					}
				} catch (Exception e) {
					logger.error("syncOrganizationType()", e);
					logger.error("syncOrganizationType()", e);
				}
			}
		} catch (Exception e) {
			logger.error("syncOrganizationType()", e);

			logger.error("syncOrganizationType()", e);
		} finally {
			DBUtil.close(rs, stmt); // Close resultset,stament objects
			DBUtil.close(pstmt); // Close prepareStatement objects
			DBUtil.close(pstmt1); // Close prepareStatement objects
			DBUtil.close(ilexConn); // Close database conneciton object
			DBUtil.close(docmConn); // Close database conneciton object
		}
	}

	public static void syncOrganizationDicipline() {
		ArrayList list = new ArrayList();
		Connection ilexConn = null;
		Connection docmConn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		Statement stmt = null;
		ResultSet rs = null;
		int updateResult = 0;
		int insertResult = 0;
		try {
			ilexConn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			docmConn = DatabaseUtils
					.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = ilexConn.createStatement();
			pstmt = docmConn
					.prepareStatement("update dm_lo_organization_discipline "
							+ "	set dm_lo_od_is_external = ? , "
							+ "	dm_lo_od_name = ?," + " dm_lo_ot_type = ? "
							+ " where dm_lo_od_ilexkey = ?");
			pstmt1 = docmConn
					.prepareStatement("insert into dm_lo_organization_discipline values (?,?,?,?)");
			rs = stmt.executeQuery("select * from lo_organization_discipline");
			while (rs.next()) {
				SyncForm syncform = new SyncForm();
				syncform.setOrgDispId(rs.getString("lo_od_id"));
				syncform.setOrgDispName(rs.getString("lo_od_name"));
				syncform.setOrgType(SyncMapping
						.getDocMKeyCorrespondingToIlexKey(
								"lo_organization_type",
								rs.getString("lo_ot_type"), docmConn));
				try {
					// Update or insert the DocM database table based on the
					// record existence
					if (SyncMapping.checkRecordExists(
							"lo_organization_discipline", syncform, docmConn)) {
						pstmt.setString(1, "Ilex");
						// pstmt.setString(2,syncform.getOrgDispId());
						pstmt.setString(2, syncform.getOrgDispName());
						pstmt.setString(3, syncform.getOrgType());
						pstmt.setString(4, syncform.getOrgDispId());
						updateResult = pstmt.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("syncOrganizationDicipline() - updateResult "
									+ updateResult);
						}
					} else {
						pstmt1.setString(1, syncform.getOrgDispName());
						pstmt1.setString(2, syncform.getOrgType());
						pstmt1.setString(3, "Ilex");
						pstmt1.setString(4, syncform.getOrgDispId());
						insertResult = pstmt1.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("syncOrganizationDicipline() - insertResult "
									+ insertResult);
						}
					}
				} catch (Exception e) {
					logger.error("syncOrganizationDicipline()", e);
					logger.error("syncOrganizationDicipline()", e);
				}
			}
		} catch (Exception e) {
			logger.error("syncOrganizationDicipline()", e);

			logger.error("syncOrganizationDicipline()", e);
		} finally {
			DBUtil.close(rs, stmt); // Close resultset,stament objects
			DBUtil.close(pstmt); // Close prepareStatement
									// objects
			DBUtil.close(pstmt1); // Close prepareStatement
									// objects
			DBUtil.close(ilexConn); // Close database conneciton
									// object
			DBUtil.close(docmConn); // Close database conneciton
									// object
		}
	}

	public static void syncLoRole() {
		ArrayList list = new ArrayList();
		Connection ilexConn = null;
		Connection docmConn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		Statement stmt = null;
		ResultSet rs = null;
		int updateResult = 0;
		int insertResult = 0;
		try {
			ilexConn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			docmConn = DatabaseUtils
					.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = ilexConn.createStatement();
			pstmt = docmConn
					.prepareStatement("update dm_lo_role set dm_lo_ro_is_external = ? , "
							+ " dm_lo_ro_od_id = ? ,  "
							+ " dm_lo_ro_role_desc = ? , "
							+ " dm_lo_ro_role_name  = ?"
							+ " where dm_lo_ro_ilexkey = ?");
			pstmt1 = docmConn
					.prepareStatement("insert into dm_lo_role values(?,?,?,?,?)");

			rs = stmt.executeQuery("select * from lo_role");
			while (rs.next()) {
				SyncForm syncform = new SyncForm();
				syncform.setRoleId(rs.getString("lo_ro_id"));
				// Replace this discipline id with the DocM discipline Id
				syncform.setOrgDispId(SyncMapping
						.getDocMKeyCorrespondingToIlexKey("lo_role",
								rs.getString("lo_ro_od_id"), docmConn));
				syncform.setRoleDesc(rs.getString("lo_ro_role_desc"));
				syncform.setRoleName(rs.getString("lo_ro_role_name"));

				try {
					if (SyncMapping.checkRecordExists("lo_role", syncform,
							docmConn)) {
						pstmt.setString(1, "Ilex");
						pstmt.setString(2, syncform.getOrgDispId());
						pstmt.setString(3, syncform.getRoleDesc());
						pstmt.setString(4, syncform.getRoleName());
						pstmt.setString(5, syncform.getRoleId());
						updateResult = pstmt.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("syncLoRole() - updateResult "
									+ updateResult);
						}
					} else {
						pstmt1.setString(1, syncform.getOrgDispId());
						pstmt1.setString(2, syncform.getRoleDesc());
						pstmt1.setString(3, syncform.getRoleName());
						pstmt1.setString(4, "Ilex");
						pstmt1.setString(5, syncform.getRoleId());
						insertResult = pstmt1.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("syncLoRole() - insertResult "
									+ insertResult);
						}
					}
				} catch (Exception e) {
					logger.error("syncLoRole()", e);
					logger.error("syncLoRole()", e);
				}
			}
		} catch (Exception e) {
			logger.error("syncLoRole()", e);

			logger.error("syncLoRole()", e);
		} finally {
			DBUtil.close(rs, stmt); // Close resultset,stament objects
			DBUtil.close(pstmt); // Close prepareStatement
									// objects
			DBUtil.close(pstmt1); // Close prepareStatement
									// objects
			DBUtil.close(ilexConn); // Close database conneciton
									// object
			DBUtil.close(docmConn); // Close database conneciton
									// object
		}
	}

	public static void syncFunctionListFromDocMToIlex() {
		ArrayList list = new ArrayList();
		Connection ilexConn = null;
		Connection docmConn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		int updateResult = 0;
		int insertResult = 0;
		try {
			ilexConn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			docmConn = DatabaseUtils
					.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = docmConn.createStatement();

			rs = stmt
					.executeQuery("select * from dm_cc_function where dm_cc_fu_is_external  = 'Ilex' ");
			while (rs.next()) {
				SyncForm syncform = new SyncForm();
				syncform.setFunctionId(rs.getString("dm_cc_fu_id"));
				syncform.setFunctionGroupName(rs
						.getString("dm_cc_fu_group_name"));
				syncform.setFunctionName(rs.getString("dm_cc_fu_name"));
				syncform.setFunctionTypeGroup(rs
						.getString("dm_cc_fu_type_group"));
				syncform.setFunctionType(rs.getString("dm_cc_fu_type"));

				pstmt = ilexConn
						.prepareStatement("select 1 from cc_function where cc_fu_type_docmKey = ?");
				pstmt1 = ilexConn
						.prepareStatement("insert into cc_function values (?,?,?,?,?,?)");
				pstmt2 = ilexConn
						.prepareStatement("update cc_function set cc_fu_group_name = ?,"
								+ " cc_fu_name = ? ,"
								+ " cc_fu_type_group = ?,"
								+ " cc_fu_type = ?,"
								+ " cc_fu_type_is_external = ? "
								+ " where cc_fu_type_docmKey = ?");
				pstmt.setString(1, syncform.getFunctionId());
				rs1 = pstmt.executeQuery();
				try {
					if (rs1.next()) {
						pstmt2.setString(1, syncform.getFunctionGroupName());
						pstmt2.setString(2, syncform.getFunctionName());
						pstmt2.setString(3, syncform.getFunctionTypeGroup());
						pstmt2.setString(4, syncform.getFunctionType());
						pstmt2.setString(5, "DocM");
						pstmt2.setString(6, syncform.getFunctionId());
						updateResult = pstmt2.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("syncFunctionListFromDocMToIlex() - updateResult "
									+ updateResult);
						}
					} else {
						pstmt1.setString(1, syncform.getFunctionGroupName());
						pstmt1.setString(2, syncform.getFunctionName());
						pstmt1.setString(3, syncform.getFunctionTypeGroup());
						pstmt1.setString(4, syncform.getFunctionType());
						pstmt1.setString(5, "DocM");
						pstmt1.setString(6, syncform.getFunctionId());
						insertResult = pstmt1.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("syncFunctionListFromDocMToIlex() - insertResult "
									+ insertResult);
						}
					}
				} catch (Exception e) {
					logger.error("syncFunctionListFromDocMToIlex()", e);
					logger.error("syncFunctionListFromDocMToIlex()", e);
				}
			}
		} catch (Exception e) {
			logger.error("syncFunctionListFromDocMToIlex()", e);

			logger.error("syncFunctionListFromDocMToIlex()", e);
		} finally {
			DBUtil.close(rs, stmt); // Close resultset,stament objects
			DBUtil.close(pstmt); // Close prepareStatement
									// objects
			DBUtil.close(pstmt1); // Close prepareStatement
									// objects
			DBUtil.close(pstmt2);
			DBUtil.close(ilexConn); // Close database conneciton
									// object
			DBUtil.close(docmConn); // Close database conneciton
									// object
		}
	}

	public static void synCcFunction() {
		ArrayList list = new ArrayList();
		Connection ilexConn = null;
		Connection docmConn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		Statement stmt = null;
		ResultSet rs = null;
		int updateResult = 0;
		int insertResult = 0;
		try {
			ilexConn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			docmConn = DatabaseUtils
					.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = ilexConn.createStatement();
			pstmt = docmConn.prepareStatement("update dm_cc_function"
					+ " set dm_cc_fu_is_external = ? ,"
					+ " dm_cc_fu_ilexkey = ? "
					+ " where dm_cc_fu_group_name= ? and "
					+ " dm_cc_fu_name = ? and "
					+ " dm_cc_fu_type_group  = ? and " + " dm_cc_fu_type = ?");
			rs = stmt
					.executeQuery("select * from cc_function where cc_fu_type_is_external = 'DocM' ");
			while (rs.next()) {
				SyncForm syncform = new SyncForm();
				syncform.setFunctionId(rs.getString("cc_fu_id"));
				syncform.setFunctionGroupName(rs.getString("cc_fu_group_name"));
				syncform.setFunctionName(rs.getString("cc_fu_name"));
				syncform.setFunctionTypeGroup(rs.getString("cc_fu_type_group"));
				syncform.setFunctionType(rs.getString("cc_fu_type"));
				syncform.setFunctionIdDocM(rs.getString("cc_fu_type_docmKey"));
				try {
					if (SyncMapping.checkRecordExists("cc_function", syncform,
							docmConn)) {
						pstmt.setString(1, "Ilex");
						pstmt.setString(2, syncform.getFunctionId());
						pstmt.setString(3, syncform.getFunctionGroupName());
						pstmt.setString(4, syncform.getFunctionName());
						pstmt.setString(5, syncform.getFunctionTypeGroup());
						pstmt.setString(6, syncform.getFunctionType());
						updateResult = pstmt.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("synCcFunction() - updateResult "
									+ updateResult);
						}
					}
				} catch (Exception e) {
					logger.error("synCcFunction()", e);
					logger.error("synCcFunction()", e);
				}
			}
		} catch (Exception e) {
			logger.error("synCcFunction()", e);

			logger.error("synCcFunction()", e);
		} finally {
			DBUtil.close(rs, stmt); // Close resultset,stament objects
			DBUtil.close(pstmt); // Close prepareStatement
									// objects
			DBUtil.close(pstmt1); // Close prepareStatement
									// objects
			DBUtil.close(ilexConn); // Close database conneciton
									// object
			DBUtil.close(docmConn); // Close database conneciton
									// object
		}
	}

	public static void synRolePriviledge() {
		ArrayList list = new ArrayList();
		Connection ilexConn = null;
		Connection docmConn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		Statement stmt = null;
		ResultSet rs = null;
		int updateResult = 0;
		int insertResult = 0;
		int deleteResult = 0;
		try {
			ilexConn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			docmConn = DatabaseUtils
					.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = ilexConn.createStatement();
			pstmt = docmConn
					.prepareStatement("delete from dm_lo_role_privilege where dm_lo_rp_is_external = 'Ilex'");
			pstmt1 = docmConn
					.prepareStatement("insert into dm_lo_role_privilege values(?,?,?)");
			rs = stmt
					.executeQuery("select lo_rp_ro_id,lo_rp_fu_id from lo_role_privilege "
							+ " inner join  cc_function "
							+ " on lo_rp_fu_id = cc_fu_id "
							+ " where cc_fu_type_is_external = 'DocM'");
			deleteResult = pstmt.executeUpdate();
			if (logger.isDebugEnabled()) {
				logger.debug("synRolePriviledge() - Delete Result "
						+ deleteResult);
			}
			while (rs.next()) {
				SyncForm syncform = new SyncForm();
				if (logger.isDebugEnabled()) {
					logger.debug("synRolePriviledge() - Ilex Role : "
							+ rs.getString("lo_rp_ro_id"));
				}
				if (logger.isDebugEnabled()) {
					logger.debug("synRolePriviledge() - Ilex Fucntion : "
							+ rs.getString("lo_rp_fu_id"));
				}

				syncform.setRoleId(SyncMapping
						.getDocMKeyCorrespondingToIlexKey("lo_role_privilegeR",
								rs.getString("lo_rp_ro_id"), docmConn));
				syncform.setFunctionId(SyncMapping
						.getDocMKeyCorrespondingToIlexKey("lo_role_privilegeF",
								rs.getString("lo_rp_fu_id"), docmConn));

				if (logger.isDebugEnabled()) {
					logger.debug("synRolePriviledge() - DocM Role : "
							+ syncform.getRoleId());
				}
				if (logger.isDebugEnabled()) {
					logger.debug("synRolePriviledge() - DocM Fucntion : "
							+ syncform.getFunctionId());
				}
				try {
					if (SyncMapping.checkRecordExists("lo_role_privilege",
							syncform, docmConn)) {
						// System.out.println("Mapping is There ");
					} else {
						pstmt1.setString(1, syncform.getRoleId());
						pstmt1.setString(2, syncform.getFunctionId());
						pstmt1.setString(3, "Ilex");
						insertResult = pstmt1.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("synRolePriviledge() - insertResult "
									+ insertResult);
						}
					}
				} catch (Exception e) {
					logger.error("synRolePriviledge()", e);
					logger.error("synRolePriviledge()", e);
				}
			}

		} catch (Exception e) {
			logger.error("synRolePriviledge()", e);

			logger.error("synRolePriviledge()", e);
		} finally {
			DBUtil.close(rs, stmt); // Close resultset,stament objects
			DBUtil.close(pstmt); // Close prepareStatement
									// objects
			DBUtil.close(pstmt1); // Close prepareStatement
									// objects
			DBUtil.close(ilexConn); // Close database conneciton
									// object
			DBUtil.close(docmConn); // Close database conneciton
									// object
		}
	}

	public static void synloPoc() {
		ArrayList list = new ArrayList();
		Connection ilexConn = null;
		Connection docmConn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		Statement stmt = null;
		ResultSet rs = null;
		int updateResult = 0;
		int insertResult = 0;
		int deleteResult = 0;
		try {
			ilexConn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			docmConn = DatabaseUtils
					.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = ilexConn.createStatement();
			pstmt = docmConn.prepareStatement("update dm_lo_poc "
					+ "set dm_lo_pc_is_external = ? ,"
					+ " dm_lo_pc_first_name  = ?, "
					+ " dm_lo_pc_last_name  = ?, " + " dm_lo_pc_title  = ? ,  "
					+ " dm_lo_pc_created_date  = ?"
					+ " where  dm_lo_pc_ilexkey = ?");
			pstmt1 = docmConn
					.prepareStatement("insert into dm_lo_poc values (?,?,?,?,?,?) ");
			rs = stmt.executeQuery("select * from lo_poc");
			while (rs.next()) {
				SyncForm syncform = new SyncForm();
				syncform.setLoPcId(rs.getString("lo_pc_id"));
				syncform.setLoPcFirstName(rs.getString("lo_pc_first_name"));
				syncform.setLoPcLastName(rs.getString("lo_pc_last_name"));
				syncform.setLoPcTitle(rs.getString("lo_pc_title"));
				syncform.setLoPcCreatedDate(rs.getString("lo_pc_created_date"));

				// System.out.println(rs.getString("lo_pc_id"));
				// System.out.println(rs.getString("lo_pc_first_name"));
				// System.out.println(rs.getString("lo_pc_last_name"));
				// System.out.println(rs.getString("lo_pc_title"));
				// System.out.println(rs.getString("lo_pc_created_date"));

				try {
					if (SyncMapping.checkRecordExists("lo_poc", syncform,
							docmConn)) { // This one is for update
						pstmt.setString(1, "Ilex");
						pstmt.setString(2, syncform.getLoPcFirstName());
						pstmt.setString(3, syncform.getLoPcLastName());
						pstmt.setString(4, syncform.getLoPcTitle());
						pstmt.setString(5, syncform.getLoPcCreatedDate());
						pstmt.setString(6, syncform.getLoPcId());
						updateResult = pstmt.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("synloPoc() - updateResult "
									+ updateResult);
						}
					} else {
						pstmt1.setString(1, syncform.getLoPcFirstName());
						pstmt1.setString(2, syncform.getLoPcLastName());
						pstmt1.setString(3, syncform.getLoPcTitle());
						pstmt1.setString(4, syncform.getLoPcCreatedDate());
						pstmt1.setString(5, "Ilex");
						pstmt1.setString(6, syncform.getLoPcId());
						insertResult = pstmt1.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("synloPoc() - insertResult "
									+ insertResult);
						}
					}
				} catch (Exception e) {
					logger.error("synloPoc()", e);

					logger.error("synloPoc()", e);
				}
			}
		} catch (Exception e) {
			logger.warn("synloPoc() - exception ignored", e);

		} finally {
			DBUtil.close(rs, stmt); // Close resultset,stament objects
			DBUtil.close(pstmt); // Close prepareStatement
									// objects
			DBUtil.close(pstmt1); // Close prepareStatement
									// objects
			DBUtil.close(ilexConn); // Close database conneciton
									// object
			DBUtil.close(docmConn); // Close database conneciton
									// object
		}
	}

	public static void syncloPocRole() {
		ArrayList list = new ArrayList();
		Connection ilexConn = null;
		Connection docmConn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		Statement stmt = null;
		ResultSet rs = null;
		int updateResult = 0;
		int insertResult = 0;
		int deleteResult = 0;
		try {
			ilexConn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			docmConn = DatabaseUtils
					.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = ilexConn.createStatement();
			pstmt = docmConn
					.prepareStatement("delete from dm_lo_poc_rol where dm_lo_pr_is_external = 'Ilex'");
			pstmt1 = docmConn
					.prepareStatement("insert into dm_lo_poc_rol values (?,?,?)");
			deleteResult = pstmt.executeUpdate();
			if (logger.isDebugEnabled()) {
				logger.debug("syncloPocRole() - Delete Result " + deleteResult);
			}
			rs = stmt.executeQuery("select * from lo_poc_rol");
			while (rs.next()) {
				SyncForm syncform = new SyncForm();
				syncform.setLoPcId(SyncMapping
						.getDocMKeyCorrespondingToIlexKey("lo_poc_rolP",
								rs.getString("lo_pr_pc_id"), docmConn));
				syncform.setRoleId(SyncMapping
						.getDocMKeyCorrespondingToIlexKey("lo_poc_rolR",
								rs.getString("lo_pr_ro_id"), docmConn));
				try {
					if (SyncMapping.checkRecordExists("lo_poc_rol", syncform,
							docmConn)) {
						if (logger.isDebugEnabled()) {
							logger.debug("syncloPocRole() - Nothing to do");
						}
					} else {
						pstmt1.setString(1, syncform.getLoPcId());
						pstmt1.setString(2, syncform.getRoleId());
						pstmt1.setString(3, "Ilex");
						insertResult = pstmt1.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("syncloPocRole() - insertResult "
									+ insertResult);
						}
					}
				} catch (Exception e) {
					logger.error("syncloPocRole()", e);

					logger.error("syncloPocRole()", e);
				}
			}
		} catch (Exception e) {
			logger.error("syncloPocRole()", e);

			logger.error("syncloPocRole()", e);
		} finally {
			DBUtil.close(rs, stmt); // Close resultset,stament objects
			DBUtil.close(pstmt); // Close prepareStatement
									// objects
			DBUtil.close(pstmt1); // Close prepareStatement
									// objects
			DBUtil.close(ilexConn); // Close database conneciton
									// object
			DBUtil.close(docmConn); // Close database conneciton
									// object
		}
	}

	public static void syncLoUser() {
		ArrayList list = new ArrayList();
		Connection ilexConn = null;
		Connection docmConn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		Statement stmt = null;
		ResultSet rs = null;
		int updateResult = 0;
		int insertResult = 0;
		try {
			ilexConn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			docmConn = DatabaseUtils
					.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = ilexConn.createStatement();
			pstmt = docmConn.prepareStatement("update dm_lo_user"
					+ " set dm_lo_us_name = ? ," + " dm_lo_us_pass = ?,"
					+ " dm_lo_us_is_external = ?"
					+ " where dm_lo_us_ilexkey = ?");
			pstmt1 = docmConn
					.prepareStatement("insert into dm_lo_user values (?,?,?,?,?)");
			rs = stmt.executeQuery("select * from lo_user");
			while (rs.next()) {
				SyncForm syncform = new SyncForm();
				syncform.setLoPcId(rs.getString("lo_us_pc_id"));
				syncform.setLoUsPcId(SyncMapping
						.getDocMKeyCorrespondingToIlexKey("lo_user",
								rs.getString("lo_us_pc_id"), docmConn));
				syncform.setLoUsPcName(rs.getString("lo_us_name"));
				syncform.setLoUsPcPass(rs.getString("lo_us_pass"));

				if (logger.isDebugEnabled()) {
					logger.debug("syncLoUser() - Ilex Primary Key "
							+ syncform.getLoPcId());
				}
				if (logger.isDebugEnabled()) {
					logger.debug("syncLoUser() - DocM Primary Key "
							+ syncform.getLoUsPcId());
				}
				if (logger.isDebugEnabled()) {
					logger.debug("syncLoUser() - username "
							+ syncform.getLoUsPcName());
				}
				if (logger.isDebugEnabled()) {
					logger.debug("syncLoUser() - password"
							+ syncform.getLoUsPcPass());
				}

				try {
					if (SyncMapping.checkRecordExists("lo_user", syncform,
							docmConn)) {
						pstmt.setString(1, syncform.getLoUsPcName());
						pstmt.setString(2, syncform.getLoUsPcPass());
						pstmt.setString(3, "Ilex");
						pstmt.setString(4, syncform.getLoPcId());
						updateResult = pstmt.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("syncLoUser() - updateResult "
									+ updateResult);
						}
					} else {
						pstmt1.setString(1, syncform.getLoUsPcId());
						pstmt1.setString(2, syncform.getLoUsPcName());
						pstmt1.setString(3, syncform.getLoUsPcPass());
						pstmt1.setString(4, "Ilex");
						pstmt1.setString(5, syncform.getLoPcId());
						insertResult = pstmt1.executeUpdate();
						if (logger.isDebugEnabled()) {
							logger.debug("syncLoUser() - insertResult "
									+ insertResult);
						}
					}
					// try{
					// Thread.sleep(500);
					// }catch(Exception e){e.printStackTrace();}
				} catch (Exception e) {
					logger.error("syncLoUser()", e);

					logger.error("syncLoUser()", e);
				}
			}
		} catch (Exception e) {
			logger.error("syncLoUser()", e);

			logger.error("syncLoUser()", e);
		} finally {
			DBUtil.close(rs, stmt); // Close resultset,stament objects
			DBUtil.close(pstmt); // Close prepareStatement
									// objects
			DBUtil.close(pstmt1); // Close prepareStatement
									// objects
			DBUtil.close(ilexConn); // Close database conneciton
									// object
			DBUtil.close(docmConn); // Close database conneciton
									// object
		}
	}

	public static void syncLoUsAuth() {
		ArrayList list = new ArrayList();
		Connection ilexConn = null;
		Connection docmConn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		Statement stmt = null;
		ResultSet rs = null;
		int updateResult = 0;
		int insertResult = 0;
		int deleteResult = 0;
		try {
			ilexConn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			docmConn = DatabaseUtils
					.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = ilexConn.createStatement();
			pstmt = docmConn
					.prepareStatement("delete from dm_lo_us_authentication where dm_lo_us_is_external = 'Ilex'");
			pstmt1 = docmConn
					.prepareStatement("insert into dm_lo_us_authentication values (?,?,?)");
			rs = stmt.executeQuery("select * from lo_us_authentication");
			deleteResult = pstmt.executeUpdate();
			if (logger.isDebugEnabled()) {
				logger.debug("syncLoUsAuth() - Delete Result " + deleteResult);
			}
			while (rs.next()) {
				SyncForm syncform = new SyncForm();
				syncform.setLoUsPcName(rs.getString("lo_us_name"));
				syncform.setRoleName(rs.getString("lo_ro_role_name"));
				try {
					pstmt1.setString(1, syncform.getLoUsPcName());
					pstmt1.setString(2, syncform.getRoleName());
					pstmt1.setString(3, "Ilex");
					insertResult = pstmt1.executeUpdate();
					if (logger.isDebugEnabled()) {
						logger.debug("syncLoUsAuth() - insertResult"
								+ insertResult);
					}
				} catch (Exception e) {
					logger.error("syncLoUsAuth()", e);

					logger.error("syncLoUsAuth()", e);
				}
			}
		} catch (Exception e) {
			logger.error("syncLoUsAuth()", e);

			logger.error("syncLoUsAuth()", e);
		} finally {
			DBUtil.close(rs, stmt); // Close resultset,stament objects
			DBUtil.close(pstmt); // Close prepareStatement
									// objects
			DBUtil.close(pstmt1); // Close prepareStatement
									// objects
			DBUtil.close(ilexConn); // Close database conneciton
									// object
			DBUtil.close(docmConn); // Close database conneciton
									// object
		}
	}
}
