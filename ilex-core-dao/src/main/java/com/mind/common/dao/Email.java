package com.mind.common.dao;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.EnvironmentSelector;
import com.mind.common.SmtpAuthenticator;
import com.mind.common.bean.EmailBean;
import com.mind.dao.PM.Jobdao;
import com.mind.docm.dao.DocumentMasterDao;

public class Email {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Email.class);

	public static boolean isEmailAllowed(String email,
			String[] allowed_address_list) {
		for (int j = 0; j < allowed_address_list.length; j++) {
			String allowed_address = allowed_address_list[j];
			if (email.contains(allowed_address)) {
				return true;
			}
		}

		return false;
	}

	public static String Send(EmailBean emailform, DataSource ds) {
		// byte[] buffer1 = null;
		// Connection conn = null;
		// Statement stmt = null;
		// ResultSet rs = null;
		int changestatus = -1;
		int emailstatus = 0;

		Properties props = System.getProperties();
		props.put("mail.smtp.host", emailform.getSmtpservername());
		props.put("mail.smtp.port", emailform.getSmtpserverport());
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.localhost", emailform.getSmtpservername());
		props.put("mail.debug", "false");

		SmtpAuthenticator auth = new SmtpAuthenticator(emailform.getUsername(),
				emailform.getUserpassword());
		Session session1;
		if (System.getSecurityManager() != null) {
			session1 = Session.getDefaultInstance(props, auth);
		} else {
			session1 = Session.getInstance(props, auth);
		}
		session1.setDebug(false);

		MimeMessage message = new MimeMessage(session1);

		try {

			message.setFrom(new InternetAddress(EnvironmentSelector
					.getBundleString("email.from")));

			String allowed_addresses = EnvironmentSelector
					.getBundleString("email.allowed_addresses");

			message.addRecipients(Message.RecipientType.TO, InternetAddress
					.parse(EnvironmentSelector.getBundleString("email.cc")));

			Jobdao.addLogEntry(ds, "1. allowed_addresses", allowed_addresses);
			Jobdao.addLogEntry(ds,
					"1. EnvironmentSelector.getBundleString(\"email.from\")",
					EnvironmentSelector.getBundleString("email.from"));
			Jobdao.addLogEntry(ds,
					"1. EnvironmentSelector.getBundleString(\"email.cc\")",
					EnvironmentSelector.getBundleString("email.cc"));

			if (allowed_addresses.equals("*")) {
				Jobdao.addLogEntry(ds, "1. Adding Recipients",
						emailform.getTo());
				Jobdao.addLogEntry(ds, "1. Adding Recipients",
						emailform.getCc());
				Jobdao.addLogEntry(ds, "1. Adding Recipients",
						emailform.getBcc());
				message.addRecipients(Message.RecipientType.TO,
						InternetAddress.parse(emailform.getTo()));
				message.addRecipients(Message.RecipientType.CC,
						InternetAddress.parse(emailform.getCc()));
				message.addRecipients(Message.RecipientType.BCC,
						InternetAddress.parse(emailform.getBcc()));
			} else {
				// Loop through the To, CC, and BCC lists to see if they are
				// allowed (as per the list in the properties file)
				String[] allowed_address_list = allowed_addresses.split(",");

				if (emailform.getTo() != null) {
					String[] possible_tos = emailform.getTo().split(",");
					for (int i = 0; i < possible_tos.length; i++) {
						String possible_to = possible_tos[i];
						if (Email.isEmailAllowed(possible_to,
								allowed_address_list)) {
							Jobdao.addLogEntry(ds, "1. Adding Recipient",
									possible_to);
							message.addRecipients(Message.RecipientType.TO,
									InternetAddress.parse(possible_to));
						}
					}
				}

				if (emailform.getCc() != null) {
					String[] possible_ccs = emailform.getCc().split(",");
					for (int i = 0; i < possible_ccs.length; i++) {
						String possible_cc = possible_ccs[i];
						if (Email.isEmailAllowed(possible_cc,
								allowed_address_list)) {
							Jobdao.addLogEntry(ds, "1. Adding Recipient",
									possible_cc);
							message.addRecipients(Message.RecipientType.CC,
									InternetAddress.parse(possible_cc));
						}
					}
				}

				if (emailform.getBcc() != null) {
					String[] possible_bccs = emailform.getBcc().split(",");
					for (int i = 0; i < possible_bccs.length; i++) {
						String possible_bcc = possible_bccs[i];
						if (Email.isEmailAllowed(possible_bcc,
								allowed_address_list)) {
							Jobdao.addLogEntry(ds, "1. Adding Recipient",
									possible_bcc);
							message.addRecipients(Message.RecipientType.BCC,
									InternetAddress.parse(possible_bcc));
						}
					}
				}
			}

			// System.out.println("From ::: " + emailform.getFrom());
			// System.out.println("To ::: " + emailform.getTo());
			// System.out.println("Cc ::: " + emailform.getCc());
			// System.out.println("Bcc ::: " + emailform.getBcc());
			message.setSubject(emailform.getSubject());

			Multipart multipart = null;

			// Create part one
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			MimeBodyPart messageBodyPartForMsa = new MimeBodyPart();
			MimeBodyPart messageBodyPartA = new MimeBodyPart();
			MimeBodyPart messageBodyPartB = new MimeBodyPart();

			if (emailform.getType().equals("partner_create")) {
				/* Send Email to Partner when a new Partner is created in Ilex. */
				multipart = new MimeMultipart();
				messageBodyPart.setContent(emailform.getContent(), "text/html");
				multipart.addBodyPart(messageBodyPart);
				BodyPart attachImag = null;

				if (emailform.getFile1() != null) {
					multipart.addBodyPart(attachFile(emailform.getFile1(), ""));
				}

				if (emailform.getFilename1() != null) {
					attachImag = new MimeBodyPart();
					javax.activation.DataSource attachSource = new FileDataSource(
							emailform.getFilename1());
					attachImag.setDataHandler(new DataHandler(attachSource));
					attachImag.setFileName(emailform.getFilename1());
					attachImag.setHeader("Content-ID", "<contingent_logo>");
					multipart.addBodyPart(attachImag);
				}

				if (emailform.getFilename2() != null) {
					attachImag = new MimeBodyPart();
					javax.activation.DataSource attachSource1 = new FileDataSource(
							emailform.getFilename2());
					attachImag.setDataHandler(new DataHandler(attachSource1));
					attachImag.setFileName(emailform.getFilename2());
					attachImag.setHeader("Content-ID", "<button1>");
					multipart.addBodyPart(attachImag);
				}

				if (emailform.getFilenameA() != null) {
					attachImag = new MimeBodyPart();
					javax.activation.DataSource attachSource2 = new FileDataSource(
							emailform.getFilenameA());
					attachImag.setDataHandler(new DataHandler(attachSource2));
					attachImag.setFileName(emailform.getFilenameA());
					attachImag.setHeader("Content-ID", "<contingent_cp_logo>");
					multipart.addBodyPart(attachImag);
				}

				if (emailform.getFilenameB() != null) {
					attachImag = new MimeBodyPart();
					javax.activation.DataSource attachSource3 = new FileDataSource(
							emailform.getFilenameB());
					attachImag.setDataHandler(new DataHandler(attachSource3));
					attachImag.setFileName(emailform.getFilenameB());
					attachImag.setHeader("Content-ID", "<partner_connection>");
					multipart.addBodyPart(attachImag);
				}

			} else {
				/* Send email at time other than Partner Creation. */
				multipart = new MimeMultipart();
				if (emailform.getContentType() != null
						&& !emailform.getContentType().isEmpty()) {
					messageBodyPart.setContent(emailform.getContent(),
							emailform.getContentType());
				} else {
					messageBodyPart.setText(emailform.getContent());
				}

				multipart.addBodyPart(messageBodyPart);

				if (emailform.getType().equals("prm_appendix")
						|| emailform.getType().equals("prm_job")
						|| emailform.getType().equals("prm_activity")
						|| emailform.getType().equals("webTicket")) {

				} else {
					if (emailform.getFile() != null) {
						messageBodyPart = new MimeBodyPart();
						javax.activation.DataSource source = new FileDataSource(
								emailform.getFile());
						messageBodyPart.setDataHandler(new DataHandler(source));
						messageBodyPart.setFileName(emailform.getFilenameX());
						multipart.addBodyPart(messageBodyPart);
					}

					/* by hamid for sending attachment start */
					if (emailform.getIncludeMsa() != null) {
						// for Msa attachment with appendix: start
						javax.activation.DataSource sourceformsa = new FileDataSource(
								emailform.getFileForMsa());
						messageBodyPartForMsa.setDataHandler(new DataHandler(
								sourceformsa));
						messageBodyPartForMsa.setFileName(emailform
								.getFilenameForMsa());
						multipart.addBodyPart(messageBodyPartForMsa);
						// for Msa attachment with appendix: end
					}

					if (emailform.getFile2() != null) {
						messageBodyPart = new MimeBodyPart();
						javax.activation.DataSource source2 = new FileDataSource(
								emailform.getFile2());
						messageBodyPart
								.setDataHandler(new DataHandler(source2));
						messageBodyPart.setFileName(emailform.getFilename2());
						multipart.addBodyPart(messageBodyPart);
					}

					/* by hamid for sending attachment end */
					// Start :Added By Amit
					if (emailform.getFileA() != null) {
						messageBodyPart = new MimeBodyPart();
						javax.activation.DataSource source3 = new FileDataSource(
								emailform.getFileA());
						messageBodyPartA
								.setDataHandler(new DataHandler(source3));
						messageBodyPartA.setFileName(emailform.getFilenameA());
						multipart.addBodyPart(messageBodyPartA);
					}

					if (emailform.getFileB() != null) {
						messageBodyPart = new MimeBodyPart();
						javax.activation.DataSource source4 = new FileDataSource(
								emailform.getFileB());
						messageBodyPartB
								.setDataHandler(new DataHandler(source4));
						messageBodyPartB.setFileName(emailform.getFilenameB());
						multipart.addBodyPart(messageBodyPartB);
					}

					// End
					// for PO/WO
					if (emailform.getFile1() != null) {
						messageBodyPart = new MimeBodyPart();
						javax.activation.DataSource source1 = new FileDataSource(
								emailform.getFile1());
						messageBodyPart
								.setDataHandler(new DataHandler(source1));
						messageBodyPart.setFileName(emailform.getFilename1());
						multipart.addBodyPart(messageBodyPart);
					}

					// Add for pd_W9W9 attachment.
					if (emailform.getFilePdW9W9() != null) {
						messageBodyPart = new MimeBodyPart();
						javax.activation.DataSource source1 = new FileDataSource(
								emailform.getFilePdW9W9());
						messageBodyPart
								.setDataHandler(new DataHandler(source1));
						messageBodyPart.setFileName(emailform
								.getFileNamePdW9W9());
						multipart.addBodyPart(messageBodyPart);
					}
					/*
					 * Attach all Document which are included whith POs/WOs in
					 * docM
					 */
					if (emailform.getDocuments() != null
							&& emailform.getDocuments().length > 0) {
						for (int i = 0; i < emailform.getDocuments().length; i++) {
							if (emailform.getDocumentNames(i) != null
									&& !emailform.getDocumentNames(i)
											.equals("")) {
								messageBodyPart = new MimeBodyPart();
								javax.activation.DataSource docMSource = new FileDataSource(
										emailform.getDocuments(i));
								messageBodyPart.setDataHandler(new DataHandler(
										docMSource));
								messageBodyPart.setFileName(emailform
										.getDocumentNames(i));
								multipart.addBodyPart(messageBodyPart);
							}
						}
					}

					// multipart.addBodyPart(messageBodyPart);
				}
			}

			File location = null;
			if (emailform.getContent().contains("cid:icon")) {
				try {
					String jobId = emailform.getJobId() == null ? emailform
							.getId3() : emailform.getJobId();
					String[] arr = DocumentMasterDao.getFileByJobId(jobId);
					if (arr != null) {

						String filePath = System.getProperty("catalina.base");
						filePath = filePath + "\\temp\\"
								+ System.currentTimeMillis() + "\\";

						System.out.println("--File location is " + filePath);

						File file = new File(filePath + arr[4]);

						byte[] bArray = DocumentMasterDao.openFile(arr[0]);

						if (!file.exists()) {
							location = new File(filePath);
							location.mkdirs();
							file.createNewFile();
						}

						InputStream in = new ByteArrayInputStream(bArray);
						BufferedImage bImageFromConvert = ImageIO.read(in);
						ImageIO.write(bImageFromConvert, arr[3], file);

						// Multipart multipart = new MimeMultipart("related");
						// Create the HTML message part and add it to the email
						// content.
						// MimeBodyPart messageBodyPart = new MimeBodyPart();
						multipart.addBodyPart(messageBodyPart);

						// Read the image from a file and add it to the email
						// content.
						// Note the "<>" added around the content ID used in the
						// HTML
						// above.
						// Setting Content-Type does not seem to be required.
						// I guess it is determined from the file type
						MimeBodyPart iconBodyPart = new MimeBodyPart();
						javax.activation.DataSource iconDataSource = new FileDataSource(
								file);
						iconBodyPart.setDataHandler(new DataHandler(
								iconDataSource));
						iconBodyPart.setDisposition(Part.INLINE);
						iconBodyPart.setContentID("<icon>");
						iconBodyPart.addHeader("Content-Type", "image/"
								+ arr[3]);
						multipart.addBodyPart(iconBodyPart);

					} else {
						emailform.getContent().replace(
								"<p><img src='cid:icon'></p>", "");
					}
				} catch (Exception e) {
					System.out
							.println("---------------------" + e.getMessage());
					System.out.println(".............................");
					logger.error("......................................"
							+ e.getMessage());
				}

			}

			message.setContent(multipart);
			message.saveChanges();
			message.setSentDate(new java.util.Date());

			logger.debug("=====[ Sending Email ]=====");
			logger.debug("From: " + message.getFrom());
			logger.debug("To: " + message.getAllRecipients());
			logger.debug("Subject: " + message.getSubject());

			Jobdao.addLogEntry(ds, "1. Emailing: From", message.getFrom()
					.toString());
			Address[] adds = message.getAllRecipients();
			for (int i = 0; i < adds.length; i++) {
				Jobdao.addLogEntry(ds, "1. Emailing: To", adds[i].toString());
			}
			Jobdao.addLogEntry(ds, "1. Emailing: Subject", message.getSubject());

			// Send message
			Transport.send(message);
			if (location != null && location.exists()) {
				try {
					delete(location);
				} catch (Exception ex) {
					logger.error("" + ex.getMessage());
				}
			}

		} catch (SendFailedException sfe) {
			logger.error("Send(EmailForm, DataSource)", sfe);

			emailstatus = -90040;
			Address[] invalidaddress = sfe.getInvalidAddresses();

			if (invalidaddress != null) {
				for (int i = 0; i < invalidaddress.length; i++) {
					if (logger.isDebugEnabled()) {
						logger.debug("Send(EmailForm, DataSource) - invalidaddress"
								+ invalidaddress[i].toString());
					}
				}
			}
		} catch (MessagingException mex) {
			logger.error("Send(EmailForm, DataSource)", mex);

			emailstatus = -90040;
			if (logger.isDebugEnabled()) {
				logger.debug("Send(EmailForm, DataSource) - EXCEPTION" + mex);
			}
		} finally {
			if (emailform.getType().equals("prm_appendix")
					|| emailform.getType().equals("prm_job")
					|| emailform.getType().equals("prm_activity")
					|| emailform.getType().equals("webTicket")
					|| emailform.getType().equals("partner_create")) {

			} else {

				if (emailform.getFile() != null) {
					// emailform.getFile().delete();
				}

				if (emailform.getDocuments() != null
						&& emailform.getDocuments().length > 0) {
					for (int i = 0; i < emailform.getDocuments().length; i++) {
						if (emailform.getDocuments(i) != null
								&& emailform.getDocuments(i).exists()) {
							// emailform.getDocuments(i).delete();
						}
					}
				}

			}

			if (emailstatus != -90040) {
				if (emailform.getType().equals("MSA")
						&& emailform.getChangeStatus().equals("true")) {
					changestatus = ChangeStatus.Status(emailform.getId1(), "",
							"", "", "", "MSA", "P", "N", ds, "");
				}
				if (emailform.getType().equals("Appendix")
						&& emailform.getChangeStatus().equals("true")) {
					changestatus = ChangeStatus.Status("", emailform.getId2(),
							"", "", "", "Appendix", "P", "N", ds, "");
				}
				if (emailform.getType().equals("PRMAppendix")
						&& emailform.getChangeStatus().equals("true")) {
					String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
							emailform.getId3(), "%", "%", ds);
					String jobType = jobStatusAndType[1];

					if (jobType.trim().equalsIgnoreCase("Addendum")) {
						changestatus = ChangeStatus.Status("", "",
								emailform.getId3(), "", "", "prj_job", "P",
								"N", ds, "", null);
					}
				}
				if (emailform.getChangeStatus() != null
						&& emailform.getChangeStatus().equals("false")) {
					changestatus = 0;
				}
			}
		}

		return emailstatus + "," + changestatus;

	}

	private static void delete(File file) throws IOException {

		if (file.isDirectory()) {

			// directory is empty, then delete it
			if (file.list().length == 0) {

				file.delete();
				System.out.println("Directory is deleted : "
						+ file.getAbsolutePath());

			} else {

				// list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					// construct the file structure
					File fileDelete = new File(file, temp);

					// recursive delete
					delete(fileDelete);
				}

				// check the directory again, if empty then delete it
				if (file.list().length == 0) {
					file.delete();
					System.out.println("Directory is deleted : "
							+ file.getAbsolutePath());
				}
			}

		} else {
			// if file, then delete it
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}
	}

	/**
	 * Discription : This method is use to send mail which will have HTML
	 * content. This is over loaded method.
	 * 
	 * @param emailform
	 *            : Bean Class
	 * @param contentType
	 *            : text/html
	 */
	public static void Send(EmailBean emailform, String contentType,
			DataSource ds) {

		Properties props = System.getProperties();
		// - for smtp authenticatio
		props.put("mail.smtp.host", emailform.getSmtpservername());
		props.put("mail.smtp.port", emailform.getSmtpserverport());
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.localhost", emailform.getSmtpservername());
		props.put("mail.debug", "false");
		SmtpAuthenticator auth = new SmtpAuthenticator(emailform.getUsername(),
				emailform.getUserpassword());
		Session session1;
		if (System.getSecurityManager() != null) {
			session1 = Session.getDefaultInstance(props, auth);
		} else {
			session1 = Session.getInstance(props, auth);
		}
		String fontOpen = "<html><body><font color='black' style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 14px;padding-top: 0px;'>";
		String fontClose = "</font></body></html>";
		session1.setDebug(false);
		MimeMessage message = new MimeMessage(session1);

		try {

			message.setFrom(new InternetAddress(EnvironmentSelector
					.getBundleString("email.from")));

			String allowed_addresses = EnvironmentSelector
					.getBundleString("email.allowed_addresses");

			message.addRecipients(Message.RecipientType.TO, InternetAddress
					.parse(EnvironmentSelector.getBundleString("email.cc")));

			Jobdao.addLogEntry(ds, "2. allowed_addresses", allowed_addresses);
			Jobdao.addLogEntry(ds,
					"2. EnvironmentSelector.getBundleString(\"email.from\")",
					EnvironmentSelector.getBundleString("email.from"));
			Jobdao.addLogEntry(ds,
					"2. EnvironmentSelector.getBundleString(\"email.cc\")",
					EnvironmentSelector.getBundleString("email.cc"));

			if (allowed_addresses.equals("*")) {
				Jobdao.addLogEntry(ds, "2. Adding Recipients",
						emailform.getTo());
				Jobdao.addLogEntry(ds, "2. Adding Recipients",
						emailform.getCc());
				Jobdao.addLogEntry(ds, "2. Adding Recipients",
						emailform.getBcc());
				message.addRecipients(Message.RecipientType.TO,
						InternetAddress.parse(emailform.getTo()));
				message.addRecipients(Message.RecipientType.CC,
						InternetAddress.parse(emailform.getCc()));
				message.addRecipients(Message.RecipientType.BCC,
						InternetAddress.parse(emailform.getBcc()));
			} else {
				// Loop through the To, CC, and BCC lists to see if they are
				// allowed (as per the list in the properties file)
				String[] allowed_address_list = allowed_addresses.split(",");

				if (emailform.getTo() != null) {
					String[] possible_tos = emailform.getTo().split(",");
					for (int i = 0; i < possible_tos.length; i++) {
						String possible_to = possible_tos[i];
						if (Email.isEmailAllowed(possible_to,
								allowed_address_list)) {
							Jobdao.addLogEntry(ds, "1. Adding Recipient",
									possible_to);
							message.addRecipients(Message.RecipientType.TO,
									InternetAddress.parse(possible_to));
						}
					}
				}

				if (emailform.getCc() != null) {
					String[] possible_ccs = emailform.getCc().split(",");
					for (int i = 0; i < possible_ccs.length; i++) {
						String possible_cc = possible_ccs[i];
						if (Email.isEmailAllowed(possible_cc,
								allowed_address_list)) {
							Jobdao.addLogEntry(ds, "1. Adding Recipient",
									possible_cc);
							message.addRecipients(Message.RecipientType.CC,
									InternetAddress.parse(possible_cc));
						}
					}
				}

				if (emailform.getBcc() != null) {
					String[] possible_bccs = emailform.getBcc().split(",");
					for (int i = 0; i < possible_bccs.length; i++) {
						String possible_bcc = possible_bccs[i];
						if (Email.isEmailAllowed(possible_bcc,
								allowed_address_list)) {
							Jobdao.addLogEntry(ds, "1. Adding Recipient",
									possible_bcc);
							message.addRecipients(Message.RecipientType.BCC,
									InternetAddress.parse(possible_bcc));
						}
					}
				}
			}

			message.setSubject(emailform.getSubject()); // - set Subject
			message.setContent(fontOpen + emailform.getContent() + fontClose,
					contentType);
			message.saveChanges();
			message.setSentDate(new java.util.Date());

			logger.debug("=====[ Sending Email ]=====");
			logger.debug("From: " + message.getFrom());
			logger.debug("To: " + message.getAllRecipients());
			logger.debug("Subject: " + message.getSubject());

			Jobdao.addLogEntry(ds, "2. Emailing: From", message.getFrom()
					.toString());
			Address[] adds = message.getAllRecipients();
			for (int i = 0; i < adds.length; i++) {
				Jobdao.addLogEntry(ds, "2. Emailing: To", adds[i].toString());
			}
			Jobdao.addLogEntry(ds, "2. Emailing: Subject", message.getSubject());

			Transport.send(message);// - Send message
		}// - end of try
		catch (SendFailedException sfe) {
			logger.error("Send(EmailForm, String)", sfe);

			Address[] invalidaddress = sfe.getInvalidAddresses();

			if (invalidaddress != null) {
				for (int i = 0; i < invalidaddress.length; i++) {
					if (logger.isDebugEnabled()) {
						logger.debug("Send(EmailForm, String) - invalidaddress"
								+ invalidaddress[i].toString());
					}
				}
			}
		}// - end of catch
		catch (MessagingException mex) {
			logger.error("Send(EmailForm, String)", mex);

			if (logger.isDebugEnabled()) {
				logger.debug("Send(EmailForm, String) - EXCEPTION" + mex);
			}
		} // - end of catch

	}

	public static boolean isValidEmailAddress(String aEmailAddress) {
		if (aEmailAddress == null) {
			return false;
		}
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(aEmailAddress);
			if (!hasNameAndDomain(aEmailAddress)) {
				result = false;
			}
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

	private static boolean hasNameAndDomain(String aEmailAddress) {
		String[] tokens = aEmailAddress.split("@");
		return tokens.length == 2 && tokens[0].trim().length() > 0
				&& tokens[1].trim().length() > 0;
	}

	private static MimeBodyPart attachFile(File file, String ContentID)
			throws MessagingException {
		logger.debug("com.mind.common.Email.attachFile File Path:-> "
				+ file.getPath());
		logger.debug("com.mind.common.Email.attachFile File Name:-> "
				+ file.getName());
		MimeBodyPart attachImag = new MimeBodyPart();
		javax.activation.DataSource attachSource = new FileDataSource(file);
		attachImag.setDataHandler(new DataHandler(attachSource));
		attachImag.setFileName(file.getName());
		attachImag.setHeader("Content-ID", ContentID);
		return attachImag;
	}

	public static String checkIsPdW9W9(String poId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String pdW9W9 = "Y";
		// int i = 0;
		// int rows = 0;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			// String sql = "select cp_pd_w9w9 from cp_partner_details where "
			// +
			// "cp_pd_partner_id=(select lm_po_partner_id from lm_purchase_order "
			// + "where lm_po_id=" + poId + ")";
			String sql = "select isnull(cp_pd_w9w9, 'Y') cp_pd_w9w9"
					+ " from cp_partner_details  "
					+ " LEFT JOIN cp_partner ON cp_partner_id = cp_pd_partner_id "
					+ " LEFT JOIN lo_organization_main on lo_om_id = cp_partner_om_id "
					+ " LEFT JOIN lo_address on lo_om_ad_id1 = lo_ad_id "
					+ " where cp_pd_partner_id=(select lm_po_partner_id from lm_purchase_order "
					+ " where lm_po_id=  " + poId
					+ "  ) AND lo_ad_country='US'";

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				pdW9W9 = rs.getString("cp_pd_w9w9");
			}
		} catch (Exception e) {
			logger.error("checkIsPdW9W9(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkIsPdW9W9(String, DataSource) - Error occured during getting cp_pd_w9w9"
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.warn(
						"checkIsPdW9W9(String, DataSource) - exception ignored",
						e);

			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.warn(
						"checkIsPdW9W9(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				logger.warn(
						"checkIsPdW9W9(String, DataSource) - exception ignored",
						e);
			}

		}
		return pdW9W9;
	}

	/*
	 * Description: Method used for sending emails for the email integration
	 * project.
	 * 
	 * Version History: 01/16/15 - Initial documentation.
	 * 
	 * Parameters:
	 * 
	 * @param (EmailBean) emailform - The email parameters (subject, to, from,
	 * body, etc).
	 * 
	 * @param (String) contentType - The content type to use for the email.
	 * 
	 * @param (String) htmlBody - The content of the email.
	 * 
	 * @param (Map<String, String>) mapInlineImages - Inline images to attach to
	 * the email.
	 * 
	 * @param (DataSource) ds - The data source for the database.
	 * 
	 * Returns: Nothing
	 */
	public static void send(EmailBean emailform, String contentType,
			String htmlBody, Map<String, String> mapInlineImages, DataSource ds) {
		try {
			// sets SMTP server properties
			Properties props = new Properties();
			props.put("mail.smtp.host", emailform.getSmtpservername());
			props.put("mail.smtp.port", emailform.getSmtpserverport());
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.localhost", emailform.getSmtpservername());
			props.put("mail.debug", "false");

			// creates a new session with an authenticator
			SmtpAuthenticator auth = new SmtpAuthenticator(
					emailform.getUsername(), emailform.getUserpassword());
			Session session = Session.getInstance(props, auth);
			String fontOpen = "<html><body><font color='black' style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 14px;padding-top: 0px;'>";
			String fontClose = "</font></body></html>";

			// creates a new e-mail message
			Message msg = new MimeMessage(session);

			// Pull in always from address from properties file.
			String fromAddress = EnvironmentSelector
					.getBundleString("email.from");
			msg.setFrom(new InternetAddress(fromAddress));

			// Pull in the list of allowed email addresses to send to.
			String allowed_addresses = EnvironmentSelector
					.getBundleString("email.allowed_addresses");

			// Pull in always CC users from properties file.
			msg.addRecipients(Message.RecipientType.CC, InternetAddress
					.parse(EnvironmentSelector.getBundleString("email.cc")));

			/*
			 * DEBUG BEGIN
			 */
			Jobdao.addLogEntry(ds, "3. allowed_addresses", allowed_addresses);
			Jobdao.addLogEntry(ds,
					"3. EnvironmentSelector.getBundleString(\"email.from\")",
					EnvironmentSelector.getBundleString("email.from"));
			Jobdao.addLogEntry(ds,
					"3. EnvironmentSelector.getBundleString(\"email.cc\")",
					EnvironmentSelector.getBundleString("email.cc"));
			/*
			 * DEBUG END
			 */

			ArrayList<String> recipients = new ArrayList<String>();

			// If allowed_addresses equals *, then all addresses are allowed.
			if (allowed_addresses.equals("*")) {
				/*
				 * DEBUG BEGIN
				 */
				Jobdao.addLogEntry(ds, "3. Adding Recipients",
						emailform.getTo());
				/*
				 * DEBUG END
				 */

				if (emailform.getTo() != null) {
					recipients.addAll(Arrays.asList(emailform.getTo()
							.split(",")));
				}
			} else {
				// Loop through the To, CC, and BCC lists to see if they are
				// allowed (as per the list in the properties file)
				String[] allowed_address_list = allowed_addresses.split(",");

				if (emailform.getTo() != null) {
					String[] possible_tos = emailform.getTo().split(",");
					for (int i = 0; i < possible_tos.length; i++) {
						String possible_to = possible_tos[i];
						if (Email.isEmailAllowed(possible_to,
								allowed_address_list)) {
							// Debug statement
							Jobdao.addLogEntry(ds, "3. Adding Recipient",
									possible_to);

							recipients.add(possible_to);
						}
					}
				}
			}

			// Rebuild comma seperated list from allowed recipients.
			String allRecipients = "";
			for (int i = 0; i < recipients.size(); i++) {
				if (!allRecipients.equals("")) {
					allRecipients += ",";
				}
				allRecipients += recipients.get(i);
			}

			// Add from email to reply list.
			if (!allRecipients.equals("")) {
				allRecipients += ",";
			}
			allRecipients += fromAddress;

			msg.setSubject(emailform.getSubject());
			msg.setSentDate(new Date());

			msg.setReplyTo(InternetAddress.parse(allRecipients));

			for (int i = 0; i < recipients.size(); i++) {
				String recipient = recipients.get(i);
				msg.setRecipient(Message.RecipientType.TO, new InternetAddress(
						recipient));

				// creates message part
				MimeBodyPart messageBodyPart = new MimeBodyPart();

				String bodyContent = fontOpen + htmlBody;

				bodyContent += "<table width=990 cellspacing=0 cellpadding=0 border=0>"
						+ "<tbody>"
						+ "<tr><td style=\"font-size: 11pt; font-family: Calibri, \'Arial Narrow\'; word-break: keep-all; text-align: right;\">"
						+ "<a href=\"http://contingent.com/subscription.php?action=unsubscribe&email="
						+ recipient
						+ "\">Unsubscribe</a>"
						+ "</td></tr>"
						+ "</tbody>" + "</table>";

				bodyContent += fontClose;
				// TODO Add unsubscript here!!!!

				messageBodyPart.setContent(bodyContent, "text/html");

				// creates multi-part
				Multipart multipart = new MimeMultipart();
				multipart.addBodyPart(messageBodyPart);

				// adds inline image attachments
				if (mapInlineImages != null && mapInlineImages.size() > 0) {
					Set<String> setImageID = mapInlineImages.keySet();

					for (String contentId : setImageID) {
						MimeBodyPart imagePart = new MimeBodyPart();
						imagePart
								.setHeader("Content-ID", "<" + contentId + ">");
						imagePart.setDisposition(MimeBodyPart.INLINE);

						String imageFilePath = mapInlineImages.get(contentId);
						try {
							imagePart.attachFile(imageFilePath);
						} catch (IOException ex) {
							ex.printStackTrace();
						}

						multipart.addBodyPart(imagePart);
					}
				}

				msg.setContent(multipart);

				logger.debug("=====[ Sending Email ]=====");
				logger.debug("From: " + fromAddress);
				logger.debug("To: " + recipient);
				logger.debug("Subject: " + msg.getSubject());

				Jobdao.addLogEntry(ds, "3. Emailing: From", fromAddress);
				Jobdao.addLogEntry(ds, "3. Emailing: To", recipient);
				Jobdao.addLogEntry(ds, "3. Emailing: Subject", msg.getSubject());

				Transport.send(msg);
			}
		} catch (Exception e) {
			Jobdao.addLogEntry(ds, "3. An exception occurred", e.getMessage());
			logger.debug(
					"send(EmailBean emailform, String contentType, String htmlBody, Map<String, String> mapInlineImages, DataSource ds) - Exception",
					e);
		}
	}

}
