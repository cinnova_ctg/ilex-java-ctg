package com.mind.common.dao;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.MenuController;
import com.mind.common.MenuElement;

public class MenuTree {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MenuTree.class);

	private int strarr_level[] = null;

	private String strarr_prevlevel[] = { "", "", "", "" };

	private String strarr_img[] = null;

	private String strarr_diffImg[][] = null;

	private int strarr_diffImgfromDb[][] = null;

	private int strarr_diffUrlfromDb[][] = null;

	private String strarr_url[] = null;

	private int strarr_label[] = null;

	private int image_width = 0;

	private int image_height = 0;

	private int image_border = 0;

	private String strarr_urlparam[][] = null;

	private String target = "";

	private String str_query = null;

	private String str_headerLabel = "";

	private String str_headerurl = null;

	private String str_headerImg = null;

	private ResultSet rs = null;

	private Connection conn = null;

	private Statement stmt = null;

	private MenuElement menuElement = null;

	private MenuElement rootElement = null;

	private MenuController mc = null;

	private PrintWriter pw = null;

	public MenuElement getRootElement() {
		return rootElement;
	}

	public MenuController getMenuController() {
		return mc;
	}

	public PrintWriter getPw() {
		return pw;
	}

	public void setPw(PrintWriter pw) {
		this.pw = pw;
	}

	public MenuTree() {
		strarr_diffImg = null;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String t) {
		target = t;
	}

	public int[] getStrarr_level() {
		return strarr_level;
	}

	public void setStrarr_level(int[] a) {
		strarr_level = a;
	}

	public int[] getStrarr_label() {
		return strarr_label;
	}

	public void setStrarr_label(int[] a) {
		// strarr_label=new String[a.length];
		strarr_label = a;
	}

	public String[][] getStrarr_urlparam() {
		return strarr_urlparam;
	}

	public void setStrarr_urlparam(String[][] a) {
		strarr_urlparam = a;
	}

	public String[] getStrarr_img() {
		return strarr_img;
	}

	public void setStrarr_img(String[] a) {
		strarr_img = a;
	}

	public int[][] getStrarr_diffUrlSameLevelDb() {
		return strarr_diffUrlfromDb;
	}

	public void setStrarr_diffUrlSameLevelDb(int[][] a) {
		strarr_diffUrlfromDb = a;
	}

	public int[][] getStrarr_diffImgSameLevelDb() {
		return strarr_diffImgfromDb;
	}

	public void setStrarr_diffImgSameLevelDb(int[][] a) {
		strarr_diffImgfromDb = a;
	}

	public String[][] getStrarr_diffImgSameLevel() {
		return strarr_diffImg;
	}

	public void setStrarr_diffImgSameLevel(String[][] a) {
		strarr_diffImg = a;
	}

	public String[] getStrarr_url() {
		return strarr_url;
	}

	public void setStrarr_url(String[] a) {
		// strarr_url = new String[a.length];
		strarr_url = a;
	}

	public String getStr_query() {
		return str_query;
	}

	public void setStr_query(String a) {
		str_query = a;
	}

	public String getStr_headerLabel() {
		return str_headerLabel;
	}

	public void setStr_headerLabel(String a) {
		str_headerLabel = a;
	}

	public String getStr_headerurl() {
		return str_headerurl;
	}

	public void setStr_headerImg(String a) {
		str_headerImg = a;
	}

	public String getStr_headerImg() {
		return str_headerImg;
	}

	public void setStr_headerurl(String a) {
		str_headerurl = a;
	}

	public void setRs(ResultSet s) {
		rs = s;
	}

	public ResultSet getRs(DataSource ds) {
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(getStr_query());
		} catch (SQLException e) {
			logger.error("getRs(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRs(DataSource) - Exception caught while retrieving result set: "
						+ e);
			}
		}
		return rs;
	}

	public void setImage_width(int s) {
		image_width = s;
	}

	public int getImage_width() {
		return image_width;
	}

	public void setImage_height(int s) {
		image_height = s;
	}

	public int getImage_height() {
		return image_height;
	}

	public void setImage_border(int s) {
		image_border = s;
	}

	public int getImage_border() {
		return image_border;
	}

	public void setHeader() {

		if ((str_headerurl == null) || (str_headerurl.equals(""))) {
			menuElement = new MenuElement(
					"<b>" + getStr_headerLabel() + "</b>", "", "<img src='"
							+ str_headerImg + "' width=" + getImage_width()
							+ " height=" + getImage_height() + " border="
							+ getImage_border() + ">", "#", null);

		} else {
			menuElement = new MenuElement("<a href='" + str_headerurl
					+ "' target='" + target + "'><b>" + getStr_headerLabel()
					+ "</b></a>", "", "<img src='" + str_headerImg + "' width="
					+ getImage_width() + " height=" + getImage_height()
					+ " border=" + getImage_border() + ">", "#", null);

		}
		rootElement = menuElement;
	}

	public void setElement(String label, String url, String img, String target) {

		if (url.equals("")) {
			menuElement = menuElement.add(new MenuElement(label, "",
					"<img src='" + img + "' width=" + getImage_width()
							+ " height=" + getImage_height() + " border="
							+ getImage_border() + ">", "#", menuElement));
		} else {
			menuElement = menuElement.add(new MenuElement("<a href='" + url
					+ "' target='" + target + "'>" + label + "</a>", "",
					"<img src='" + img + "' width=" + getImage_width()
							+ " height=" + getImage_height() + " border="
							+ getImage_border() + ">", "#", menuElement));
		}
	}

	// start

	public void setElementPV(String label, String url, String img,
			String target, String moduleName, String incident, String status) {
		String image1 = "";
		String image2 = "";
		String title = "";
		String addImageStr = "";

		if (incident.equals("Y"))
			image1 = "images/2.gif";

		if (status.equals("R"))
			image2 = "images/1.gif";

		if (url.equals("")) {
			title = label;
			if (!image1.equals(""))
				addImageStr = addImageStr + " " + "<img src='" + image1
						+ "' border=" + 0 + " >";
			if (!image2.equals(""))
				addImageStr = addImageStr + " " + "<img src='" + image2
						+ "' border=" + 0 + " >";
		} else {
			title = "<a href='" + url + "' target='" + target + "'>" + label
					+ "</a>";
			if (!image1.equals(""))
				addImageStr = addImageStr + " " + "<img src='" + image1
						+ "' border=" + 0 + " >";
			if (!image2.equals(""))
				addImageStr = addImageStr + " " + "<img src='" + image2
						+ "' border=" + 0 + " >";
		}

		menuElement = menuElement.add(new MenuElement(title, "", "<img src='"
				+ img + "' width=" + getImage_width() + " height="
				+ getImage_height() + " border=" + getImage_border() + ">",
				"#", menuElement, addImageStr));
	}

	// stop

	public String generateMenu(DataSource ds, String moduleName) {
		String url = "";
		boolean tochangelevelflag = true;
		int prevprinted = 0;
		int i = 0;
		boolean haveresultset = false;
		boolean isnullset = false;
		int imageCounter = -1;
		String img = "";
		String str = "";
		StringTokenizer st = null;
		boolean resultsetvalue = false;
		boolean changeLevelFlag = false;
		String cnsDivison = "";

		setHeader();
		try {
			rs = getRs(ds);
			if (rs != null) {
				haveresultset = true;

				while (rs.next()) {

					resultsetvalue = true;
					// Change Level
					int levelDiff = 0;
					int count, levelLast = 0;
					if (strarr_prevlevel != null) {
						for (count = 0; count < strarr_level.length; count++) {
							String currstr = rs.getString(strarr_level[count]) == null ? ""
									: rs.getString(strarr_level[count]);
							String prevstr = strarr_prevlevel[count] == null ? ""
									: strarr_prevlevel[count];
							/*
							 * if (!prevstr.equals(currstr) &&
							 * currstr.equals("0") && moduleName.equals("AM")) {
							 * levelDiff = levelDiff >
							 * strarr_level.length-count?
							 * levelDiff:strarr_level.length-count; }
							 */

							if (!prevstr.equals(currstr) && !prevstr.equals("")) {
								levelDiff = levelDiff > strarr_level.length
										- count ? levelDiff
										: strarr_level.length - count;
							}

							if (prevstr.equals(currstr)
									&& !prevstr.equals("")
									&& (moduleName.equals("CM") || moduleName
											.equals("CM1"))
									&& rs.getString(1).trim().equals("N")
									&& count == 2
									&& (!cnsDivison.equals(rs
											.getString("lo_om_division")))) {
								levelDiff = levelDiff > strarr_level.length
										- count ? levelDiff
										: strarr_level.length - count;
							}

							if (!prevstr.equals(""))
								levelLast = count + 1;
						}
					}
					levelDiff = levelLast + levelDiff - strarr_level.length >= 0 ? levelLast
							+ levelDiff - strarr_level.length
							: 0;

					for (count = 0; count < levelDiff; count++) {
						if (menuElement.parent != null) {
							menuElement = menuElement.parent;
						}
					}
					// End Change Level

					// forsinglerow:
					for (i = 0; i < strarr_level.length; i++) {
						changeLevelFlag = false;
						if (strarr_prevlevel[i].equals(rs
								.getString(strarr_level[i]))) { // url
							if ((moduleName.equals("CM") || moduleName
									.equals("CM1"))
									&& rs.getString(1).trim().equals("N")
									&& (i == 2)) {

								if (!cnsDivison.equals(rs
										.getString("lo_om_division"))) {
									changeLevelFlag = true;
								} else {
									tochangelevelflag = false; // same value so
									// same level
									isnullset = false; // value is not null
								}
							} else {
								tochangelevelflag = false; // same value so same
								// level
								isnullset = false; // value is not null
							}
						} else {
							// if(moduleName.equals("AM") &&
							// (rs.getString(strarr_label[i])==null?"":rs.getString(strarr_level[i])).equals("0"))
							// {
							if (moduleName.equals("AM")
									&& (rs.getString(strarr_label[i]) == null ? ""
											: rs.getString(strarr_label[i]))
											.equals("NetMedX")
									&& (rs.getString(7) == null ? "" : rs
											.getString(7)).equals("1")) {
								changeLevelFlag = false;
							} else {
								changeLevelFlag = true;
							}
						}

						if (changeLevelFlag) {
							// for changing the image as per the constatnt level
							// data
							if (strarr_diffImg != null) {
								for (int l = 0; l < strarr_diffImg[0].length; l++) {
									if (i == (int) Integer
											.parseInt(strarr_diffImg[0][l])) {
										imageCounter++;
										img = strarr_diffImg[1][imageCounter];
										break;
									} else {

										img = strarr_img[i];
									}
								}

							} else {
								if (strarr_diffImgfromDb != null) {
									for (int k = 0; k < strarr_diffImgfromDb[0].length; k++) {

										if (i == (strarr_diffImgfromDb[0][k])) {
											img = rs.getString(strarr_diffImgfromDb[1][k]);

											break;
										} else {

											img = strarr_img[i];
										}
									}
								} else {
									img = strarr_img[i];
								}
							}

							if (rs.getString(strarr_level[i]) != null) // if
							// current
							// value
							// is
							// not
							// null
							// then
							// it is
							// to be
							// printed
							{
								// read the data and send for the total url
								// string to be attached.

								if (strarr_urlparam != null) {
									st = new StringTokenizer(
											strarr_urlparam[1][i], "~");
									url = "";
									String namevalue = "";
									do {
										namevalue = st.nextToken();
										url = url
												+ namevalue.substring(0,
														namevalue.indexOf('|'));
										url = url
												+ "="
												+ rs.getString((int) Integer.parseInt(namevalue.substring(
														namevalue.indexOf('|') + 1,
														namevalue.length())))
												+ "&";

									} while (st.hasMoreTokens());
									// url = url.replaceAll("/", "-");
									url = url.substring(0, url.length() - 1);
									/* Start: Added By Atul 08/01/2007 */
									if (strarr_url[i].equals("")) {
										url = "";
										if (strarr_diffUrlfromDb != null) {
											for (int k = 0; k < strarr_diffUrlfromDb[0].length; k++) {

												if (i == (strarr_diffUrlfromDb[0][k])) {
													url = rs.getString(strarr_diffUrlfromDb[1][k]);
													break;
												} else {
													url = strarr_url[i] + "?"
															+ url;
												}
											}
										}
									} else {
										url = strarr_url[i] + "?" + url;
									}
									/* End: Added By Atul 16/01/2007 */
								}

								// end of url
								if (moduleName.equals("PV")
										|| (i == 2 && moduleName.equals("CM"))) {
									String incident = "";
									String status = "";
									if (rs.getString("cp_pd_incident_type") != null)
										incident = rs
												.getString("cp_pd_incident_type");
									if (rs.getString("cp_partner_status") != null)
										status = rs
												.getString("cp_partner_status");

									setElementPV(rs.getString(strarr_label[i]),
											url, img, getTarget(), moduleName,
											incident, status);
								} else {
									setElement(rs.getString(strarr_label[i]),
											url, img, getTarget());

								}
								strarr_prevlevel[i] = rs
										.getString(strarr_level[i]);
								for (int j = i + 1; j < strarr_level.length; j++) {
									strarr_prevlevel[j] = "";
								}
								prevprinted = i + 1;
								tochangelevelflag = true;
								isnullset = false;
							} // end of isnull if
							else {

								isnullset = true;
								i = 0;
								break;// forsinglerow;

							} // end of isnull else

						} // end of matching else
					} // end of for

					if ((moduleName.equals("CM") || moduleName.equals("CM1"))) {
						cnsDivison = rs.getString("lo_om_division");
					}

					i = 0;
					tochangelevelflag = true;
				}// end of while

			} // end of if

		}

		catch (Exception e) {
			logger.error("generateMenu(DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("generateMenu(DataSource, String) - Exception caught"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"generateMenu(DataSource, String) - exception ignored",
						sql);

			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"generateMenu(DataSource, String) - exception ignored",
						sql);

			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"generateMenu(DataSource, String) - exception ignored",
						sql);

			}
		}

		mc = new MenuController();
		mc.moduleName = moduleName;

		return "";
	}

	public String getCurrState() {
		if (mc != null)
			return (mc.currState);
		return ("");
	}

	public String getExpansionState() {
		if (mc != null)
			return (mc.expansionState);
		return ("");
	}

}
