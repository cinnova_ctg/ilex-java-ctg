package com.mind.common.dao;

public class SynchronizationDaoTest {

	public void testSyncOrganizationType() {
		SynchronizationDao.syncOrganizationType();
	}
	
	public void testSyncOrganizationDicipline(){
		SynchronizationDao.syncOrganizationDicipline();
	}

	public void testSyncLoRole(){
		SynchronizationDao.syncLoRole();
	}
	
	public void testSyncFunctionListFromDocMToIlex(){
		SynchronizationDao.syncFunctionListFromDocMToIlex();
	}
	
	public void testSynCcFunction(){
		SynchronizationDao.synCcFunction();
	}
	public void testSynRolePriviledge(){
		SynchronizationDao.synRolePriviledge();
	}
	public void testSynloPoc(){
		SynchronizationDao.synloPoc();
	}
	public void testSyncloPocRole(){
		SynchronizationDao.syncloPocRole();
	}
	
	public void testSyncLoUser(){
		SynchronizationDao.syncLoUser();
	}
	
	public void testSyncLoUsAuth(){
		SynchronizationDao.syncLoUsAuth();
	}
	
}
