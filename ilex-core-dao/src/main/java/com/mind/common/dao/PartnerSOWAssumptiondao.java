package com.mind.common.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.PartnerSISCBean;
import com.mind.bean.PartnerSOWAssumptionBean;

public class PartnerSOWAssumptiondao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(PartnerSOWAssumptiondao.class);

	public static ArrayList getPartnerSOWAssumptionList(String fromId,
			String fromType, String viewType, DataSource ds) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		ArrayList valuelist = new ArrayList();

		// System.out.println("fromId::::::::"+fromId);
		// System.out.println("fromType::::::::"+fromType);
		// System.out.println("viewType::::::::"+viewType);

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call pf_partner_sowassumption_list(?, ?, ?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, fromId);
			cstmt.setString(3, fromType);
			cstmt.setString(4, viewType);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				PartnerSOWAssumptionBean partnerSOWassumptionform = new PartnerSOWAssumptionBean();

				partnerSOWassumptionform.setActCategory(rs
						.getString("lm_al_cat_name"));
				partnerSOWassumptionform.setActId(rs.getString("lm_at_id"));
				partnerSOWassumptionform
						.setActName(rs.getString("lm_at_title"));
				partnerSOWassumptionform.setActStatus(rs
						.getString("lm_at_status"));
				partnerSOWassumptionform.setActType(rs.getString("lm_at_type"));
				partnerSOWassumptionform.setActQty(rs
						.getString("lm_at_quantity"));
				partnerSOWassumptionform.setSOWAssumptionId(rs
						.getString("pf_data_id"));
				partnerSOWassumptionform.setSOWAssumptionValue(rs
						.getString("pf_data_value"));

				valuelist.add(partnerSOWassumptionform);
			}
		} catch (Exception e) {
			logger.error(
					"getPartnerSOWAssumptionList(String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerSOWAssumptionList(String, String, String, DataSource) - Error occured during getting Partner SOW Assumption List"
						+ e);
			}
		}

		finally {

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getPartnerSOWAssumptionList(String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getPartnerSOWAssumptionList(String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getPartnerSOWAssumptionList(String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return valuelist;
	}

	public static String getDefaultPartnerSOWAssumption(String Id,
			String fromType, String viewType, String pfType, DataSource ds) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		String dataValue = "";

		// System.out.println("Id::::::::"+Id);
		// System.out.println("fromType::::::::"+fromType);
		// System.out.println("viewType::::::::"+viewType);
		// System.out.println("pfType::::::::"+pfType);

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call pf_get_partner_default_value(?, ?, ?, ?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, Id);
			cstmt.setString(3, fromType);
			cstmt.setString(4, viewType);
			cstmt.setString(5, pfType);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				dataValue = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error(
					"getDefaultPartnerSOWAssumption(String, String, String, String, DataSource)",
					e);
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getDefaultPartnerSOWAssumption(String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getDefaultPartnerSOWAssumption(String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getDefaultPartnerSOWAssumption(String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return dataValue;
	}

	public static int managePartnerSOWAssumption(String actId, String fromType,
			String partnerInformation, String viewType, String loginUserId,
			DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int retval = 0;

		// System.out.println("2,"+actId);
		// System.out.println("3,"+partnerInformation);
		// System.out.println("4,"+fromType);
		// System.out.println("5,"+loginUserId);
		// System.out.println("6,"+viewType);

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call pf_sow_assumption_manage_01(?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, actId);
			cstmt.setString(3, partnerInformation);
			cstmt.setString(4, fromType);
			cstmt.setString(5, loginUserId);
			cstmt.setString(6, viewType);

			cstmt.execute();
			retval = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"managePartnerSOWAssumption(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("managePartnerSOWAssumption(String, String, String, String, String, DataSource) - Error occured during adding manage Partner SOW Assumption"
						+ e);
			}
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"managePartnerSOWAssumption(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"managePartnerSOWAssumption(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}
		}
		return retval;
	}

	public static int managePartnerSISC(String appendixId, String fromType,
			String partnerSI, String partnerSC, String loginUserId,
			DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int retval = 0;

		// System.out.println("2,"+actId);
		// System.out.println("3,"+partnerInformation);
		// System.out.println("4,"+fromType);
		// System.out.println("5,"+loginUserId);
		// System.out.println("6,"+viewType);

		try {
			conn = ds.getConnection();

			cstmt = conn.prepareCall("{?=call pf_scsi_manage_01(?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, appendixId);
			cstmt.setString(3, fromType);
			cstmt.setString(4, partnerSI);
			cstmt.setString(5, partnerSC);
			cstmt.setString(6, loginUserId);

			cstmt.execute();
			retval = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"managePartnerSISC(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("managePartnerSISC(String, String, String, String, String, DataSource) - Error occured during adding manage Partner SCSI"
						+ e);
			}
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"managePartnerSISC(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"managePartnerSISC(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}
		}
		return retval;
	}

	public static int deletePartnerSOWAssumption(String SOWAssumptionId,
			String viewType, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int retval = 0;

		// System.out.println("2,"+SOWAssumptionId);
		// System.out.println("3,"+viewType);

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call pf_sow_assumption_delete_01(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, SOWAssumptionId);
			cstmt.setString(3, viewType);

			cstmt.execute();
			retval = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"deletePartnerSOWAssumption(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deletePartnerSOWAssumption(String, String, DataSource) - Error occured during deleting Partner SOW Assumption"
						+ e);
			}
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"deletePartnerSOWAssumption(String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"deletePartnerSOWAssumption(String, String, DataSource) - exception ignored",
						e);
			}
		}
		return retval;
	}

	public static int deletePartnerSISC(String appendixId, String viewType,
			String fromType, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		int retval = 0;

		// System.out.println("2,"+SOWAssumptionId);
		// System.out.println("3,"+viewType);
		// System.out.println("4,"+fromType);

		try {
			conn = ds.getConnection();

			cstmt = conn.prepareCall("{?=call pf_sisc_delete_01(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, appendixId);
			cstmt.setString(3, viewType);
			cstmt.setString(4, fromType);

			cstmt.execute();
			retval = cstmt.getInt(1);
		} catch (Exception e) {
			logger.error(
					"deletePartnerSISC(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("deletePartnerSISC(String, String, String, DataSource) - Error occured during deleting Partner SISC"
						+ e);
			}
		}

		finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"deletePartnerSISC(String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"deletePartnerSISC(String, String, String, DataSource) - exception ignored",
						e);
			}
		}
		return retval;
	}

	public static String getPartnerDataValue(String actId, String fromType,
			String dataType, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String dataValue = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select dbo.func_pf_get_partner_data_value(" + actId
					+ ", '" + fromType + "', '" + dataType + "')";
			// System.out.println("sql::::::::::::::::"+sql);
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				dataValue = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error(
					"getPartnerDataValue(String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerDataValue(String, String, String, DataSource) - Error occured during getting Partner Data Value::"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getPartnerDataValue(String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getPartnerDataValue(String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getPartnerDataValue(String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return dataValue;
	}

	public static void getPartnerSISCValue(PartnerSISCBean partnerSISCForm,
			String appendixId, String fromType, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String dataValue = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			String sql = "select * from dbo.func_pf_get_partner_sisc_value("
					+ appendixId + ", '" + fromType + "')";
			// System.out.println("sql::::::::::::::::"+sql);
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				partnerSISCForm.setSCSIId(rs.getString("pf_sisc_id"));
				partnerSISCForm.setSCValue(rs.getString("pf_sisc_spcondition"));
				partnerSISCForm.setSIValue(rs
						.getString("pf_sisc_spinstruction"));
			} else
				partnerSISCForm.setSCSIId("0");

		} catch (Exception e) {
			logger.error(
					"getPartnerSISCValue(PartnerSISCBean, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerSISCValue(PartnerSISCBean, String, String, DataSource) - Error occured during getting Partner Data Value::"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getPartnerSISCValue(PartnerSISCBean, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getPartnerSISCValue(PartnerSISCBean, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getPartnerSISCValue(PartnerSISCBean, String, String, DataSource) - exception ignored",
						e);
			}

		}
	}

	public static String getPartnerActivityName(String activityId,
			String fromType, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String activityName = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select dbo.func_pf_get_activity_name(" + activityId
					+ ", '" + fromType + "')";

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				activityName = rs.getString(1);
			}
		}

		catch (Exception e) {
			logger.error("getPartnerActivityName(String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerActivityName(String, String, DataSource) - Exception in retrieving Activity name"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getPartnerActivityName(String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPartnerActivityName(String, String, DataSource) - Exception in retrieving Activity name"
							+ sql);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getPartnerActivityName(String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPartnerActivityName(String, String, DataSource) - Exception in retrieving Activity name"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"getPartnerActivityName(String, String, DataSource)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPartnerActivityName(String, String, DataSource) - Exception in retrieving Activity name"
							+ sql);
				}
			}

		}
		return activityName;
	}
}
