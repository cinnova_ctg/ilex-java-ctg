package com.mind.common.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.IlexConstants;
import com.mind.common.LabelValue;
import com.mind.common.bean.POCNameEmail;
import com.mind.dao.PM.MSAdao;
import com.mind.fw.core.dao.util.DBUtil;

public class ViewList {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ViewList.class);

	public static ArrayList getMsatype(DataSource ds) {
		ArrayList msatype = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		msatype.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from lp_msa_type where lp_msa_type_name in ('Standard','Hardware Only')");
			while (rs.next()) {
				msatype.add(new com.mind.common.LabelValue(rs
						.getString("lp_msa_type_name"), rs
						.getString("lp_msa_type_name_id")));
			}
		} catch (Exception e) {
			logger.error("getMsatype(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsatype(DataSource) - Exception caught in rerieving msatype"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn("getMsatype(DataSource) - exception ignored", sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn("getMsatype(DataSource) - exception ignored", sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn("getMsatype(DataSource) - exception ignored", sql);
			}

		}
		return msatype;
	}

	public static ArrayList getAppendixtype(DataSource ds) {
		ArrayList appendixtype = new ArrayList();
		appendixtype.add(new com.mind.common.LabelValue("---Select---", "0"));
		appendixtype.add(new com.mind.common.LabelValue("I Multi-Site", "1"));
		appendixtype.add(new com.mind.common.LabelValue("II Single Site", "2"));
		appendixtype.add(new com.mind.common.LabelValue("NetMedX - Standard",
				"3A"));
		appendixtype.add(new com.mind.common.LabelValue(
				"NetMedX - Silver Dispatch", "3B"));
		appendixtype.add(new com.mind.common.LabelValue(
				"NetMedX - Silver Hourly", "3C"));
		appendixtype.add(new com.mind.common.LabelValue("NetMedX - Copper",
				"3D"));
		appendixtype.add(new com.mind.common.LabelValue("Hardware", "4"));

		/*
		 * Connection conn = null; Statement stmt = null; ResultSet rs = null;
		 * appendixtype.add( new com.mind.common.LabelValue( "---Select---" ,
		 * "0") );
		 * 
		 * try { conn = ds.getConnection(); stmt = conn.createStatement();
		 * 
		 * rs = stmt.executeQuery( "select * from lp_appendix_type" ); while(
		 * rs.next() ) { appendixtype.add ( new com.mind.common.LabelValue(
		 * rs.getString( "lp_appendix_type_name_desc" ) , rs.getString(
		 * "lp_appendix_type_name_id" ) ) ); } } catch( Exception e ) {
		 * System.out.println( "Exception caught in rerieving appendixtype" + e
		 * ); } finally {
		 * 
		 * try { if ( rs!= null ) { rs.close(); } } catch( SQLException sql ) {}
		 * 
		 * 
		 * try { if ( stmt!= null ) { stmt.close(); } } catch( SQLException sql
		 * ) {}
		 * 
		 * 
		 * try { if ( conn!= null ) { conn.close(); } } catch( SQLException sql
		 * ) {}
		 * 
		 * }
		 */
		return appendixtype;
	}

	// for fill Activity combo-box in AcivityEdit.jsp by vijay:start

	public static ArrayList getActivitytype(DataSource ds) {
		ArrayList activitytype = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		activitytype.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from lm_activity_type where lm_activity_type_code <>'V' ");
			while (rs.next()) {
				activitytype.add(new com.mind.common.LabelValue(rs
						.getString("lm_activity_type_description"), rs
						.getString("lm_activity_type_code")));
			}
		} catch (Exception e) {
			logger.error("getActivitytype(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivitytype(DataSource) - Exception caught in rerieving activitytype"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn("getActivitytype(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn("getActivitytype(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn("getActivitytype(DataSource) - exception ignored",
						sql);
			}

		}
		return activitytype;
	}

	// for fill Activity combo-box in AcivityEdit.jsp by vijay:End

	public static String getAppendixtypedesc(String Id, DataSource ds) {
		String appendixtypedesc = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select dbo.func_lp_appendix_type( lx_pr_type ) from lx_appendix_main where lx_pr_id="
					+ Id;
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				appendixtypedesc = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("getAppendixtypedesc(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixtypedesc(String, DataSource) - Exception caught in rerieving appendixtypedesc"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.warn(
						"getAppendixtypedesc(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				logger.warn(
						"getAppendixtypedesc(String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.warn(
						"getAppendixtypedesc(String, DataSource) - exception ignored",
						e);
			}

		}
		return appendixtypedesc;
	}

	public static String CheckAppendixType(String Appendix_Id, DataSource ds) {
		String type = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select dbo.func_lm_check_netmedx_appendix_02( '"
							+ Appendix_Id + "' )");
			while (rs.next()) {
				type = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("CheckAppendixType(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("CheckAppendixType(String, DataSource) - Exception caught in rerieving CheckAppendixType"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"CheckAppendixType(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"CheckAppendixType(String, DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"CheckAppendixType(String, DataSource) - exception ignored",
						sql);
			}

		}
		return type;
	}

	public static boolean getIsNetMedXNew(String Id) {
		boolean netMedXCheck = false;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();

			sql = "select isnull(lx_pd_xml,'') from lx_appendix_detail where lx_pd_pr_id = "
					+ Id;
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				if (rs.getString(1).trim().equalsIgnoreCase("Old"))
					netMedXCheck = false;
				else
					netMedXCheck = true;
			}
		} catch (Exception e) {
			logger.error("getIsNetMedXNew(String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getIsNetMedXNew(String) - Exception caught in rerieving newNetMedxType "
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.warn("getIsNetMedXNew(String) - exception ignored", e);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				logger.warn("getIsNetMedXNew(String) - exception ignored", e);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.warn("getIsNetMedXNew(String) - exception ignored", e);
			}

		}
		return netMedXCheck;
	}

	public static ArrayList getCustomers(DataSource ds) {
		ArrayList customername = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		customername.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select lo_ot_id , lo_ot_name from lp_customer_list_01");
			while (rs.next()) {
				customername.add(new com.mind.common.LabelValue(rs
						.getString("lo_ot_name"), rs.getString("lo_ot_id")));
			}
		} catch (Exception e) {
			logger.error("getCustomers(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomers(DataSource) - Exception caught in rerieving customer"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql) {
				logger.warn("getCustomers(DataSource) - exception ignored", sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn("getCustomers(DataSource) - exception ignored", sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn("getCustomers(DataSource) - exception ignored", sql);
			}

		}
		return customername;
	}

	// NEW FUNCTION

	public static ArrayList getCustPOCEmail(String MSA_Id, String Appendix_Id,
			String Type, DataSource ds) {
		ArrayList custpocemail = new ArrayList();
		POCNameEmail pocnameemail;
		String sql = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String blank = "-";
		String tempemail = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			if (Type.equals("MSA") || Type.equals("Send POWO")
					|| Type.equals("prm_job") || Type.equals("Invoice Request")
					|| Type.equals("webTicket")) {
				sql = "select *  from dbo.func_cc_poc_name_email( '" + MSA_Id
						+ "' ,'" + Type + "' )";

				if (Type.equals("prm_job")) {
					sql = "select *  from dbo.func_cc_poc_name_email( '"
							+ Appendix_Id + "' ,'" + Type + "' )";
				}

				if (Type.equals("Invoice Request")) {
					sql = "select *  from dbo.func_cc_poc_name_email( '"
							+ MSA_Id + "' ,'MSA' )";
				}

				if (Type.equals("webTicket")) {
					sql = "select lo_pc_first_name+' '+lo_pc_last_name, lo_pc_email1 from lo_poc where lo_pc_id = "
							+ MSA_Id;
				}

				rs = stmt.executeQuery(sql);
				while (rs.next()) {
					pocnameemail = new POCNameEmail();

					if (tempemail.equals(rs.getString(2))) {
					} else {
						pocnameemail.setName(rs.getString(1));
						pocnameemail.setEmail(rs.getString(2));
						custpocemail.add(pocnameemail);
					}
					tempemail = rs.getString(2);
				}

			}

			if (Type.equals("Appendix") || Type.equals("PRMAppendix")
					|| Type.equals("prm_appendix")
					|| Type.equals("prm_activity")) {
				sql = "select * from dbo.func_cc_poc_name_email( '"
						+ Appendix_Id + "' , '" + Type + "' )";

				// sql =
				// "select  businesspoc_name , businesspoc_email , contractpoc_name , "
				// +
				// "contractpoc_email , billingpoc_name , billingpoc_email ,cnspoc_name , cnspoc_email  from dbo.func_cc_poc_name_email('"+Appendix_Id+"' ,'"+Type+"')";
				// System.out.println("LLLLLLLLLLLLLLLLLLLLLLL      "+sql);
				rs = stmt.executeQuery(sql);

				while (rs.next()) {
					pocnameemail = new POCNameEmail();

					if (tempemail.equals(rs.getString(2))) {
					} else {
						pocnameemail.setName(rs.getString(1));
						pocnameemail.setEmail(rs.getString(2));
						custpocemail.add(pocnameemail);
					}
					tempemail = rs.getString(2);
				}
			}
			// rs.close();
			// stmt.close();
			/*
			 * stmt = conn.createStatement(); sql =
			 * "select cns_director , cns_director_email  from dbo.func_cc_director_name_email()"
			 * ;
			 * 
			 * rs = stmt.executeQuery( sql );
			 * 
			 * while( rs.next() ) { pocnameemail = new POCNameEmail();
			 * 
			 * if(!( rs.getString(1).equals( "" )|| rs.getString( 1 ) == null )
			 * ) { pocnameemail.setName( rs.getString(1) );
			 * pocnameemail.setEmail( rs.getString(2) );
			 * 
			 * } else { pocnameemail.setName( "+" ); pocnameemail.setEmail( "+"
			 * );
			 * 
			 * }
			 * 
			 * custpocemail.add( pocnameemail ); }
			 */
		} catch (Exception e) {
			logger.error("getCustPOCEmail(String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustPOCEmail(String, String, String, DataSource) - Exception caught in retrieving POC Email"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sql1) {
				logger.warn(
						"getCustPOCEmail(String, String, String, DataSource) - exception ignored",
						sql1);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql1) {
				logger.warn(
						"getCustPOCEmail(String, String, String, DataSource) - exception ignored",
						sql1);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql1) {
				logger.warn(
						"getCustPOCEmail(String, String, String, DataSource) - exception ignored",
						sql1);
			}
		}
		return custpocemail;
	}

	/**
	 * Discription: This method is use to get cns active user list, with title
	 * of Sr. Project Manager or with - classification of Sales.
	 * 
	 * @param type
	 *            - Sr. Project manager/ Sales
	 * @param ds
	 *            - Database Object
	 * @return ArrayList - List of active user.
	 */
	public static ArrayList getProjectManagerPOC(String type, DataSource ds) {
		ArrayList activePOC = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "";
		// Changes By Lalit Goyal to add lo_pc_id = 14, name = Bob Supinger.
		// manually in case of Sales POC
		if ("sales".equalsIgnoreCase(type)) {
			query = "select * from (select poc_id, poc_name from dbo.func_get_appendix_prj_manager('"
					+ type
					+ "') union select lo_pc_id as poc_id ,(lo_pc_first_name+' '+lo_pc_last_name) as poc_name from lo_poc where lo_pc_id = 14)A order by poc_name";
		}
		// System.out.println("query::::::::::::::::::"+query);
		else {
			query = "select poc_id, poc_name from dbo.func_get_appendix_prj_manager('"
					+ type + "') order by poc_name";
		}
		activePOC.add(new com.mind.common.LabelValue("---Select---", "0")); // -
																			// ComboBox
																			// first
																			// value
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				activePOC.add(new com.mind.common.LabelValue(rs
						.getString("poc_name"), rs.getString("poc_id")));
			}
		} catch (Exception e) {
			logger.error("getProjectManagerPOC(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getProjectManagerPOC(String, DataSource) - Exception caught in getting Project Manager POC"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.error("getProjectManagerPOC(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getProjectManagerPOC(String, DataSource) - Exception caught in closing Project Manager poc resultset"
							+ sql);
				}
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.error("getProjectManagerPOC(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getProjectManagerPOC(String, DataSource) - Exception caught in closing Project Manager poc statement"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getProjectManagerPOC(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getProjectManagerPOC(String, DataSource) - Exception caught in closing Project Manager poc connection"
							+ sql);
				}
			}
		}

		return activePOC;
	}

	public static ArrayList getPOC(String Type, String Id, DataSource ds) {
		ArrayList POC = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "";

		query = "select poc_id, poc_name from dbo.func_getPOC_tab('" + Id
				+ "' , '" + Type + "') order by poc_name";

		// System.out.println("query::::::::::::::::::"+query);

		POC.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery(query);

			while (rs.next()) {
				POC.add(new com.mind.common.LabelValue(
						rs.getString("poc_name"), rs.getString("poc_id")));
				// POC.add ( new com.mind.common.LabelValue( rs.getString(
				// "lo_pc_first_name" )+ " "+rs.getString( "lo_pc_last_name" ) ,
				// rs.getString( "lo_pc_id" ) ) );
			}
		} catch (Exception e) {
			logger.error("getPOC(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPOC(String, String, DataSource) - Exception caught in getting POC"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.error("getPOC(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPOC(String, String, DataSource) - Exception caught in closing poc resultset"
							+ sql);
				}
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.error("getPOC(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPOC(String, String, DataSource) - Exception caught in closing poc statement"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getPOC(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPOC(String, String, DataSource) - Exception caught in closing poc connection"
							+ sql);
				}
			}
		}

		return POC;
	}

	public static ArrayList getSitePOC(String Id, String type, DataSource ds) {
		ArrayList POC = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "";

		query = "select poc_id, poc_name from dbo.func_getSitePOC_tab('" + Id
				+ "', '" + type + "') order by poc_name";
		// System.out.println("XXXXquery "+query);

		POC.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery(query);

			while (rs.next()) {
				POC.add(new com.mind.common.LabelValue(
						rs.getString("poc_name"), rs.getString("poc_id")));
				// POC.add ( new com.mind.common.LabelValue( rs.getString(
				// "lo_pc_first_name" )+ " "+rs.getString( "lo_pc_last_name" ) ,
				// rs.getString( "lo_pc_id" ) ) );
			}
		} catch (Exception e) {
			logger.error("getSitePOC(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSitePOC(String, String, DataSource) - Exception caught in getting POC"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.error("getSitePOC(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getSitePOC(String, String, DataSource) - Exception caught in closing poc resultset"
							+ sql);
				}
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.error("getSitePOC(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getSitePOC(String, String, DataSource) - Exception caught in closing poc statement"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getSitePOC(String, String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getSitePOC(String, String, DataSource) - Exception caught in closing poc connection"
							+ sql);
				}
			}
		}

		return POC;
	}

	public static ArrayList getPOCbasedonrole(String checkPage, DataSource ds) {
		ArrayList POC = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "";

		if (checkPage.equals("MSA"))
			query = "select lo_pc_id , lo_poc_name , lo_pc_email1 from dbo.func_lp_pocbdmformsa_list() order by 2";
		else
			query = "select lo_pc_id , lo_poc_name , lo_pc_email1 from dbo.func_lp_pocbasedonrole_list('N','Business Development')";

		POC.add(new com.mind.common.LabelValue("--Select--", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery(query);

			while (rs.next()) {
				POC.add(new com.mind.common.LabelValue(rs
						.getString("lo_poc_name"), rs.getString("lo_pc_id")));
				// POC.add ( new com.mind.common.LabelValue( rs.getString(
				// "lo_pc_first_name" )+ " "+rs.getString( "lo_pc_last_name" ) ,
				// rs.getString( "lo_pc_id" ) ) );
			}
		} catch (Exception e) {
			logger.error("getPOCbasedonrole(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPOCbasedonrole(String, DataSource) - Exception caught in getting POC"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.error("getPOCbasedonrole(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPOCbasedonrole(String, DataSource) - Exception caught in closing poc resultset"
							+ sql);
				}
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.error("getPOCbasedonrole(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPOCbasedonrole(String, DataSource) - Exception caught in closing poc statement"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getPOCbasedonrole(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPOCbasedonrole(String, DataSource) - Exception caught in closing poc connection"
							+ sql);
				}
			}
		}

		return POC;
	}

	public static ArrayList getCustomerdivision(String Id, DataSource ds) {
		ArrayList customerdivision = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		// String query =
		// "select lo_om_id , lo_om_division from lp_msa_main, lo_organization_top , lo_organization_main "
		// +
		// "where lp_mm_ot_id = lo_ot_id and lo_ot_id = lo_om_ot_id and lp_mm_id ="+Id;

		String query = "select lo_om_id , lo_om_division from dbo.func_customer_division('"
				+ Id + "')";

		customerdivision
				.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery(query);

			while (rs.next()) {
				customerdivision
						.add(new com.mind.common.LabelValue(rs
								.getString("lo_om_division"), rs
								.getString("lo_om_id")));
			}
		} catch (Exception e) {
			logger.error("getCustomerdivision(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomerdivision(String, DataSource) - Exception caught in getting Customer Division"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCustomerdivision(String, DataSource) - exception ignored",
						sql);

			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCustomerdivision(String, DataSource) - exception ignored",
						sql);

			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getCustomerdivision(String, DataSource) - exception ignored",
						sql);

			}
		}
		return customerdivision;
	}

	public static String checkforrole(String id, String role, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int tempid = Integer.parseInt(id);
		String check = "";
		String sql = "";

		sql = "select dbo.func_check_for_role_assigned ( " + tempid + " , '"
				+ role + "' )";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery(sql);
			if (rs.next())
				check = rs.getString(1);

		}

		catch (Exception e) {
			logger.error("checkforrole(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkforrole(String, String, DataSource) - Exception in check role of user"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sqle) {
				logger.error("checkforrole(String, String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("checkforrole(String, String, DataSource) - Exception in check role of user"
							+ sqle);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sqle) {
				logger.error("checkforrole(String, String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("checkforrole(String, String, DataSource) - Exception in check role of user"
							+ sqle);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error("checkforrole(String, String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("checkforrole(String, String, DataSource) - Exception in check role of user"
							+ sqle);
				}
			}

		}
		return check;
	}

	public static String getInitialStatus(String type, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String initialstatus = "";
		String sql = "";

		sql = "select status_code , status_description from dbo.func_initialstatus('"
				+ type + "')";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery(sql);
			if (rs.next())
				initialstatus = rs.getString(2);

		}

		catch (Exception e) {
			logger.error("getInitialStatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getInitialStatus(String, DataSource) - Exception in retrieving initialstatus"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sqle) {
				logger.error("getInitialStatus(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("getInitialStatus(String, DataSource) - Exception in retrieving initialstatus"
							+ sqle);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sqle) {
				logger.error("getInitialStatus(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("getInitialStatus(String, DataSource) - Exception in retrieving initialstatus"
							+ sqle);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error("getInitialStatus(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("getInitialStatus(String, DataSource) - Exception in retrieving initialstatus"
							+ sqle);
				}
			}

		}
		return initialstatus;
	}

	public static String getDefaultdate(String type, DataSource ds) {
		Connection conn = null;
		ResultSet rs = null;

		String date = "";
		String sql = "";

		CallableStatement cstmt = null;

		try {
			conn = ds.getConnection();

			cstmt = conn
					.prepareCall("{?=call dbo.lp_defaultdate_tab( ? , ? ) }");

			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);

			cstmt.setString(3, type);

			cstmt.execute();

			date = cstmt.getString(2);

		}

		catch (Exception e) {
			logger.error("getDefaultdate(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDefaultdate(String, DataSource) - Exception in retrieving date"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException sqle) {
				logger.error("getDefaultdate(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("getDefaultdate(String, DataSource) - Exception in retrieving date"
							+ sqle);
				}
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
			} catch (SQLException sqle) {
				logger.error("getDefaultdate(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("getDefaultdate(String, DataSource) - Exception in retrieving date"
							+ sqle);
				}
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sqle) {
				logger.error("getDefaultdate(String, DataSource)", sqle);

				if (logger.isDebugEnabled()) {
					logger.debug("getDefaultdate(String, DataSource) - Exception in retrieving date"
							+ sqle);
				}
			}
		}
		return date;
	}

	public static ArrayList getPOCbasedonroleforBdm(String msaId, DataSource ds) {
		ArrayList POC = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "";
		String bdmName = "";
		bdmName = MSAdao.getCurentBdm(msaId, ds);

		query = "select lo_pc_id , lo_poc_name , lo_pc_email1 from dbo.func_lp_pocbdmformsa_list() where lo_poc_name <> '"
				+ bdmName + "' order by 2";

		POC.add(new com.mind.common.LabelValue("-Select New BDM-", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery(query);

			while (rs.next()) {
				POC.add(new com.mind.common.LabelValue(rs
						.getString("lo_poc_name"), rs.getString("lo_pc_id")));
				// POC.add ( new com.mind.common.LabelValue( rs.getString(
				// "lo_pc_first_name" )+ " "+rs.getString( "lo_pc_last_name" ) ,
				// rs.getString( "lo_pc_id" ) ) );
			}
		} catch (Exception e) {
			logger.error("getPOCbasedonroleforBdm(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPOCbasedonroleforBdm(String, DataSource) - Exception caught in getting POC"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.error("getPOCbasedonroleforBdm(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPOCbasedonroleforBdm(String, DataSource) - Exception caught in closing poc resultset"
							+ sql);
				}
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.error("getPOCbasedonroleforBdm(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPOCbasedonroleforBdm(String, DataSource) - Exception caught in closing poc statement"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.error("getPOCbasedonroleforBdm(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getPOCbasedonroleforBdm(String, DataSource) - Exception caught in closing poc connection"
							+ sql);
				}
			}
		}

		return POC;
	}

	/**
	 * Gets the appendix type breakout list.
	 * 
	 * @param ds
	 *            the ds
	 * @return the appendix type breakout list
	 */
	public static List<LabelValue> getAppendixTypeBreakoutList(DataSource ds) {
		List<LabelValue> appendixtypelist = new ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "select lp_ab_name from lp_appendix_type_breakout where lp_ab_active=1 order by lp_ab_name";
		appendixtypelist
				.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery(query);

			while (rs.next()) {
				appendixtypelist.add(new com.mind.common.LabelValue(rs
						.getString("lp_ab_name"), rs.getString("lp_ab_name")));
			}
		} catch (Exception e) {
			logger.error("getAppendixTypeBreakoutList( DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixTypeBreakoutList( DataSource) - Exception caught in getting Appendixtype breakout List"
						+ e);
			}
		} finally {

			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getAppendixTypeBreakoutList( DataSource) - exception ignored",
						sql);

			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getAppendixTypeBreakoutList( DataSource) - exception ignored",
						sql);

			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getAppendixTypeBreakoutList( DataSource) - exception ignored",
						sql);

			}
		}
		return appendixtypelist;
	}

}
