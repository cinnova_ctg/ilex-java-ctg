package com.mind.common.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.LabelValue;

public class DynamicComboDaoAM {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DynamicComboDaoAM.class);

	public static ArrayList getClassNameCombo(DataSource ds) 
	{
		
		ArrayList classname = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		classname.add( new com.mind.common.LabelValue( "----Select----" , "0") );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery( "select clr_class_id, clr_class_name from clr_class_master order by clr_class_name" );
			while( rs.next() )
			{
				classname.add(new com.mind.common.LabelValue(rs.getString("clr_class_name"), rs.getString( "clr_class_id" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getClassNameCombo(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getClassNameCombo(DataSource) - Exception caught in rerieving getClassNameCombo" + e);
			}
		}
		finally
		{
			
			try
			{
				rs.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getClassNameCombo(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				stmt.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getClassNameCombo(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				conn.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getClassNameCombo(DataSource) - exception ignored", sql);
}
			
		}
		
		return classname;
	}
	
	
	public static ArrayList getResourceNameCombo(DataSource ds) 
	{
		
		ArrayList resourcename = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		resourcename.add( new com.mind.common.LabelValue( "----Select----" , "0") );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery( "select clr_reslevel_id, clr_reslevel_name from clr_reslevel order by clr_reslevel_name" );
			while( rs.next() )
			{
				resourcename.add(new com.mind.common.LabelValue(rs.getString("clr_reslevel_name"), rs.getString( "clr_reslevel_id" ) ) );
			}
		}
		catch( Exception e )
		{
			logger.error("getResourceNameCombo(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourceNameCombo(DataSource) - Exception caught in rerieving getResourceNameCombo" + e);
			}
		}
		finally
		{
			
			try
			{
				rs.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getResourceNameCombo(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				stmt.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getResourceNameCombo(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				conn.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getResourceNameCombo(DataSource) - exception ignored", sql);
}
			
		}
		
		return resourcename;
	}
	
	
	public static ArrayList getCategoryNameCombo(DataSource ds) 
	{
		
		ArrayList categoryname = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		categoryname.add( new com.mind.common.LabelValue( "----Select----" , "0") );
		
		try
		{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery( "select  distinct(clr_cat_short_name) from clr_category " );
			while( rs.next() )
			{
				categoryname.add(new com.mind.common.LabelValue(rs.getString("clr_cat_short_name"), rs.getString( "clr_cat_short_name" )) );
			}
		}
		catch( Exception e )
		{
			logger.error("getCategoryNameCombo(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCategoryNameCombo(DataSource) - Exception caught in rerieving getCategoryNameCombo" + e);
			}
		}
		finally
		{
			
			try
			{
				rs.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getCategoryNameCombo(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				stmt.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getCategoryNameCombo(DataSource) - exception ignored", sql);
}
			
			
			try
			{
				conn.close();
			}
			catch( SQLException sql )
			{
				logger.warn("getCategoryNameCombo(DataSource) - exception ignored", sql);
}
			
		}
		
		return categoryname;
	}
	
}
