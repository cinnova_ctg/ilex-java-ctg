package com.mind.common.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class Menu {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Menu.class);

	public static String getStatus(String type, char currentstate,
			String MSA_Id, String Appendix_Id, String Type_Id,
			String resource_id, String loginuserid, DataSource ds) {
		Connection conn = null;

		boolean newMenu = false;
		if (type.equals("prj_jobMenuNew")) {
			type = "prj_job";
			newMenu = true;
		} else if (type.equals("Addendum_New")) {
			type = "Addendum";
			newMenu = true;
		} else if (type.equals("pm_act_new")) {
			type = "pm_act";
			newMenu = true;
		} else if (type.equals("pm_appendix_new")) {
			type = "pm_appendix";
			newMenu = true;
		} else if (type.equals("pm_job_new")) {
			type = "pm_job";
			newMenu = true;
		} else if (type.equals("pm_msa")) {
			type = "pm_msa";
			newMenu = true;
		} else if (type.equals("pm_appendix")) {
			type = "pm_appendix";
			newMenu = true;
		}

		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		String str = "";
		String changestatuslist = "";
		boolean poCompleteCheck = false;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (!StringUtils.isEmpty(Type_Id)) {
				poCompleteCheck = new Menu().getPOStatusComplete(Type_Id, ds);
			}
			if (type.contains("pm_res") || type.contains("jobdashboard_res")
					|| type.equals("Addendum") || type.equals("Addendum_New")) {
				if (type.contains("pm_res")) {
					sql = "select cc_changed_status , cc_status_description from "
							+ "dbo.func_change_status_list('"
							+ type.substring(0, type.length() - 1)
							+ "' , '"
							+ currentstate + "' , '" + loginuserid + "')";
				} else {
					if (type.equals("Addendum") || type.equals("Addendum_New")) {
						/*
						 * sql =
						 * "select cc_changed_status , cc_status_description from "
						 * +
						 * "dbo.func_change_status_list( 'pm_job' , '"+currentstate
						 * +"' , '"+loginuserid+"')";
						 */
						sql = "select cc_changed_status , cc_status_description from "
								+ "dbo.func_change_status_list( 'pm_appendix' , '"
								+ currentstate + "' , '" + loginuserid + "')";
					} else {
						sql = "select cc_changed_status , cc_status_description from "
								+ "dbo.func_change_status_list('M' , '"
								+ currentstate + "' , '" + loginuserid + "')";
					}
				}

			} else {
				sql = "select cc_changed_status , cc_status_description from "
						+ "dbo.func_change_status_list('" + type + "' , '"
						+ currentstate + "' , '" + loginuserid + "')";
			}

			rs = stmt.executeQuery(sql);

			boolean trimOutput = true;
			if (rs != null) {
				while (rs.next()) {
					/*
					 * if (type.equals("pm_msa")) { changestatuslist +=
					 * "<li><a href=\"MenuFunctionChangeStatusAction.do?Type=Appendix&MSA_Id="
					 * + MSA_Id + "&Status=" + rs.getString(1) + "\">" +
					 * rs.getString("cc_status_description") + "</a></li>";
					 * changestatuslist = changestatuslist + "\n"; }
					 */

					if (newMenu && type.equals("pm_msa")) {
						trimOutput = false;
						changestatuslist += "<li><a href=\"MenuFunctionChangeStatusAction.do?Type=Appendix&MSA_Id="
								+ MSA_Id
								+ "&Status="
								+ rs.getString(1)
								+ "\">"
								+ rs.getString("cc_status_description")
								+ "</a></li>";
					}

					if (!newMenu && type.equals("pm_appendix")) {
						changestatuslist = changestatuslist
								+ "\""
								+ rs.getString("cc_status_description")
								+ "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?Type=Appendix&MSA_Id="
								+ MSA_Id + "&Appendix_Id=" + Appendix_Id
								+ "&Status=" + rs.getString(1) + "\"" + ","
								+ "0,";
						changestatuslist = changestatuslist + "\n";
					}

					if (newMenu && type.equals("pm_appendix")) {
						trimOutput = false;
						changestatuslist += "<li><a href=\"MenuFunctionChangeStatusAction.do?Type=Appendix&MSA_Id="
								+ MSA_Id
								+ "&Appendix_Id="
								+ Appendix_Id
								+ "&Status="
								+ rs.getString(1)
								+ "\">"
								+ rs.getString("cc_status_description")
								+ "</a></li>";
					}

					if (type.equals("pm_job")) {
						changestatuslist = changestatuslist
								+ "\""
								+ rs.getString("cc_status_description")
								+ "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?Type=Job&Job_Id="
								+ Type_Id + "&Status=" + rs.getString(1) + "\""
								+ "," + "0,";
						changestatuslist = changestatuslist + "\n";
					}

					if (newMenu && type.equals("pm_job")) {
						trimOutput = false;
						changestatuslist += "<li><a href=\"MenuFunctionChangeStatusAction.do?Type=Job&Job_Id="
								+ Type_Id
								+ "&Status="
								+ rs.getString(1)
								+ "\">"
								+ rs.getString("cc_status_description")
								+ "</a></li>";
					}

					if (!newMenu && type.equals("Addendum")) {
						changestatuslist = changestatuslist
								+ "\""
								+ rs.getString("cc_status_description")
								+ "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?Type=prj_job&jobid="
								+ Type_Id + "&Status=" + rs.getString(1) + "\""
								+ "," + "0,";
						changestatuslist = changestatuslist + "\n";
					}

					if (newMenu && type.equals("Addendum")) {
						trimOutput = false;
						changestatuslist += "<li><a href=\"MenuFunctionChangeStatusAction.do?Type=prj_job&jobid="
								+ Type_Id
								+ "&Status="
								+ rs.getString(1)
								+ "\">"
								+ rs.getString("cc_status_description")
								+ "</a></li>";
					}

					if (!newMenu && type.equals("pm_act")) {
						changestatuslist = changestatuslist + "\""
								+ rs.getString("cc_status_description") + "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?ref="
								+ resource_id + "&Type=Activity&Activity_Id="
								+ Type_Id + "&Status=" + rs.getString(1) + "\""
								+ "," + "0,";
						changestatuslist = changestatuslist + "\n";
					}

					if (newMenu && type.equals("pm_act")) {
						trimOutput = false;
						changestatuslist += "<li><a href=\"MenuFunctionChangeStatusAction.do?ref="
								+ resource_id
								+ "&Type=Activity&Activity_Id="
								+ Type_Id
								+ "&Status="
								+ rs.getString(1)
								+ "\">"
								+ rs.getString("cc_status_description")
								+ "</a></li>";
					}

					if (type.contains("pm_res")) {
						changestatuslist = changestatuslist + "\""
								+ rs.getString("cc_status_description") + "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?Type="
								+ type + "&resource_Id=" + resource_id
								+ "&Status=" + rs.getString(1) + "\"" + ","
								+ "0,";
						changestatuslist = changestatuslist + "\n";
					}

					if (type.equals("am")) {
						trimOutput = false;
						changestatuslist += "<li><a href=\"MenuFunctionChangeStatusAction.do?Type=am&Activitylibrary_Id="
								+ Type_Id
								+ "&Status="
								+ rs.getString(1)
								+ "\">"
								+ rs.getString("cc_status_description")
								+ "</a></li>";
					}

					if (type.equals("M")) {
						trimOutput = false;
						changestatuslist += "<li><a href=\"MenuFunctionChangeStatusAction.do?Type=M&resource_Id="
								+ resource_id
								+ "&Status="
								+ rs.getString(1)
								+ "\">"
								+ rs.getString("cc_status_description")
								+ "</a></li>";
					}

					if (type.equals("L")) {
						trimOutput = false;
						changestatuslist += "<li><a href=\"MenuFunctionChangeStatusAction.do?Type=L&resource_Id="
								+ resource_id
								+ "&Status="
								+ rs.getString(1)
								+ "\">"
								+ rs.getString("cc_status_description")
								+ "</a></li>";
					}

					if (type.equals("F")) {
						trimOutput = false;
						changestatuslist += "<li><a href=\"MenuFunctionChangeStatusAction.do?Type=F&resource_Id="
								+ resource_id
								+ "&Status="
								+ rs.getString(1)
								+ "\">"
								+ rs.getString("cc_status_description")
								+ "</a></li>";
					}

					if (type.equals("T")) {
						trimOutput = false;
						changestatuslist += "<li><a href=\"MenuFunctionChangeStatusAction.do?Type=T&resource_Id="
								+ resource_id
								+ "&Status="
								+ rs.getString(1)
								+ "\">"
								+ rs.getString("cc_status_description")
								+ "</a></li>";
					}

					// Type_id is a common id for appendix/job/activity in PRM
					// Manager
					if (type.equals("prj_appendix")) {
						changestatuslist = changestatuslist + "\""
								+ rs.getString("cc_status_description") + "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?Type="
								+ type + "&appendixid=" + Type_Id + "&Status="
								+ rs.getString(1) + "\"" + "," + "0,";
						changestatuslist = changestatuslist + "\n";
					}

					if (!newMenu && type.equals("prj_job")) {
						// Start :Added By Amit First Job success
						if (rs.getString(1).trim().equalsIgnoreCase("F")) {
							changestatuslist = changestatuslist
									+ "\""
									+ rs.getString("cc_status_description")
									+ "\""
									+ ","
									+ "\"JobDashboardAction.do?isClicked=5&tabId=5&function=view&appendixid="
									+ Appendix_Id + "&ticketType=&jobid="
									+ Type_Id + "\"" + "," + "0,";
							changestatuslist = changestatuslist + "\n";
						} else {
							// End
							if (poCompleteCheck
									&& (rs.getString(1).trim()
											.equalsIgnoreCase("H") || rs
											.getString(1).trim()
											.equalsIgnoreCase("C"))) {

							} else {
								changestatuslist = changestatuslist
										+ "\""
										+ rs.getString("cc_status_description")
										+ "\""
										+ ","
										+ "\"MenuFunctionChangeStatusAction.do?Type="
										+ type + "&jobid=" + Type_Id
										+ "&Status=" + rs.getString(1) + "\""
										+ "," + "0,";
								changestatuslist = changestatuslist + "\n";
							}
						}
					}

					if (newMenu && type.equals("prj_job")) {
						trimOutput = false;
						// Start :Added By Amit First Job success
						if (rs.getString(1).trim().equalsIgnoreCase("F")) {
							changestatuslist += "<li><a href=\"JobDashboardAction.do?isClicked=5&tabId=5&function=view&appendixid="
									+ Appendix_Id
									+ "&ticketType=&jobid="
									+ Type_Id
									+ "\">"
									+ rs.getString("cc_status_description")
									+ "</a></li>";
						} else {
							// End
							if (poCompleteCheck
									&& (rs.getString(1).trim()
											.equalsIgnoreCase("H") || rs
											.getString(1).trim()
											.equalsIgnoreCase("C"))) {

							} else {
								changestatuslist += "<li><a href=\"MenuFunctionChangeStatusAction.do?Type="
										+ type
										+ "&jobid="
										+ Type_Id
										+ "&Status="
										+ rs.getString(1)
										+ "\">"
										+ rs.getString("cc_status_description")
										+ "</a></li>";
							}
						}
					}

					if (type.equals("jobdashboard_resM")) {
						changestatuslist = changestatuslist
								+ "\""
								+ rs.getString("cc_status_description")
								+ "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?&resource_Id="
								+ resource_id + "&ref=" + Type_Id + "&from="
								+ MSA_Id + "&jobid=" + Appendix_Id + "&Type="
								+ type + "&Status=" + rs.getString(1) + "\""
								+ "," + "0,";
						changestatuslist = changestatuslist + "\n";
					}

					if (type.equals("jobdashboard_resL")) {
						changestatuslist = changestatuslist + "\""
								+ rs.getString("cc_status_description") + "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?ref="
								+ Type_Id + "&from=" + MSA_Id + "&jobid="
								+ Appendix_Id + "&Type=" + type
								+ "&resource_Id=" + resource_id + "&Status="
								+ rs.getString(1) + "\"" + "," + "0,";
						changestatuslist = changestatuslist + "\n";
					}

					if (type.equals("jobdashboard_resF")) {
						changestatuslist = changestatuslist + "\""
								+ rs.getString("cc_status_description") + "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?ref="
								+ Type_Id + "&from=" + MSA_Id + "&jobid="
								+ Appendix_Id + "&Type=" + type
								+ "&resource_Id=" + resource_id + "&Status="
								+ rs.getString(1) + "\"" + "," + "0,";
						changestatuslist = changestatuslist + "\n";
					}

					if (type.equals("jobdashboard_resT")) {
						changestatuslist = changestatuslist + "\""
								+ rs.getString("cc_status_description") + "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?ref="
								+ Type_Id + "&from=" + MSA_Id + "&jobid="
								+ Appendix_Id + "&Type=" + type
								+ "&resource_Id=" + resource_id + "&Status="
								+ rs.getString(1) + "\"" + "," + "0,";
						changestatuslist = changestatuslist + "\n";
					}

					if (type.equals("prj_act")) {
						changestatuslist = changestatuslist + "\""
								+ rs.getString("cc_status_description") + "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?ref="
								+ resource_id + "&from=" + MSA_Id + "&jobid="
								+ Appendix_Id + "&Type=" + type
								+ "&activityid=" + Type_Id + "&Status="
								+ rs.getString(1) + "\"" + "," + "0,";
						changestatuslist = changestatuslist + "\n";
					}

					/* Customer Labor Rates */
					if (type.equals("AM")) {
						changestatuslist = changestatuslist
								+ "\""
								+ rs.getString("cc_status_description")
								+ "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?Type=AM&MSA_Id="
								+ MSA_Id + "&Activitylibrary_Id=" + Type_Id
								+ "&Status=" + rs.getString(1) + "\"" + ","
								+ "0,";
						changestatuslist = changestatuslist + "\n";
					}

					if (type.equals("AM_EDIT")) {
						changestatuslist = changestatuslist
								+ "\""
								+ rs.getString("cc_status_description")
								+ "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?Type=AM_EDIT&MSA_Id="
								+ MSA_Id + "&Activitylibrary_Id=" + Type_Id
								+ "&Status=" + rs.getString(1) + "\"" + ","
								+ "0,";
						changestatuslist = changestatuslist + "\n";
					}

					if (type.equals("PM")) {
						changestatuslist = changestatuslist
								+ "\""
								+ rs.getString("cc_status_description")
								+ "\""
								+ ","
								+ "\"MenuFunctionChangeStatusAction.do?Type=PM&MSA_Id="
								+ MSA_Id + "&Activity_Id=" + Type_Id
								+ "&Status=" + rs.getString(1) + "\"" + ","
								+ "0,";
						changestatuslist = changestatuslist + "\n";
					}
					/* Customer Labor Rates */

				}
			}

			if (trimOutput && changestatuslist != "") {

				changestatuslist = changestatuslist.substring(0,
						changestatuslist.length() - 2);
			}
		} catch (SQLException e) {
			logger.error(
					"getStatus(String, char, String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStatus(String, char, String, String, String, String, String, DataSource) - Exception caught "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.error(
						"getStatus(String, char, String, String, String, String, String, DataSource)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("getStatus(String, char, String, String, String, String, String, DataSource) - Exception caught"
							+ e);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				logger.error(
						"getStatus(String, char, String, String, String, String, String, DataSource)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("getStatus(String, char, String, String, String, String, String, DataSource) - Exception caught"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error(
						"getStatus(String, char, String, String, String, String, String, DataSource)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("getStatus(String, char, String, String, String, String, String, DataSource) - Exception caught"
							+ sql);
				}
			}

		}
		return changestatuslist;

	}

	boolean getPOStatusComplete(String Type_Id, DataSource ds) {
		Connection conn = null;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from lm_purchase_order join lm_job on lm_po_js_id=lm_js_id where lm_js_id="
					+ Type_Id + " and po_status=3";

			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			logger.error("POStatusComplete(String, DataSource)", e);
		} finally {
			DbUtils.closeQuietly(conn, stmt, rs);

		}
		return false;

	}

	public static ArrayList getStatusrm(String type, String currentstate,
			String loginuserid, DataSource ds) {
		Connection conn = null;

		Statement stmt = null;
		Statement stmt1 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		String sql1 = "";
		String sql = "";
		ArrayList changestatuslist = new ArrayList();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			stmt1 = conn.createStatement();

			sql = "select cc_changed_status , cc_status_description from "
					+ "dbo.func_change_status_list('" + type + "' , '"
					+ currentstate + "' , '" + loginuserid + "')";

			rs = stmt.executeQuery(sql);

			sql1 = "select cc_status_description from cc_status_master where cc_status_code='"
					+ currentstate + "'";
			rs1 = stmt1.executeQuery(sql1);
			if (rs1.next()) {
				changestatuslist.add(new com.mind.common.LabelValue(rs1
						.getString("cc_status_description"), currentstate));
			}

			if (rs != null) {
				while (rs.next()) {

					changestatuslist.add(new com.mind.common.LabelValue(rs
							.getString("cc_status_description"), rs
							.getString("cc_changed_status")));

				}
			}

		} catch (SQLException e) {
			logger.error("getStatusrm(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStatusrm(String, String, String, DataSource) - Exception caught "
						+ e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (rs1 != null) {
					rs1.close();
				}
			} catch (SQLException e) {
				logger.error("getStatusrm(String, String, String, DataSource)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("getStatusrm(String, String, String, DataSource) - Exception caught"
							+ e);
				}
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
				if (stmt1 != null) {
					stmt1.close();
				}
			} catch (SQLException e) {
				logger.error("getStatusrm(String, String, String, DataSource)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("getStatusrm(String, String, String, DataSource) - Exception caught"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error("getStatusrm(String, String, String, DataSource)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("getStatusrm(String, String, String, DataSource) - Exception caught"
							+ sql);
				}
			}

		}
		return changestatuslist;

	}

}
