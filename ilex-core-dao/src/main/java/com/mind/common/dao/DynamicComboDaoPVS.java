package com.mind.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.ilex.reports.reportDao.ReportsDao;

public class DynamicComboDaoPVS {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DynamicComboDaoPVS.class);

	public ArrayList getTechnicianlist(String pid, DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_tech_name,cp_tech_id  from cp_technicians where cp_tech_partner_id = '"
							+ pid + "' order by cp_tech_name");
			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("cp_tech_name"), rs.getString("cp_tech_id")));
			}
		} catch (Exception e) {
			logger.error("getTechnicianlist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTechnicianlist(String, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getTechnicianlist(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getTechnicianlist(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getTechnicianlist(String, DataSource) - exception ignored",
						sql);
			}

		}
		list.add(new com.mind.common.LabelValue("Not Listed", "-1"));
		return list;
	}

	public ArrayList getAmpmlist() {
		ArrayList list = new ArrayList();

		list.add(new com.mind.common.LabelValue("AM", "AM"));
		list.add(new com.mind.common.LabelValue("PM", "PM"));

		return list;
	}

	public ArrayList getCustomerlist(String identifier, String partnerId,
			DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		list.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select lo_ot_name, lo_ot_id  from dbo.func_get_incident_end_customer('"
					+ identifier + "', " + partnerId + ")";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("lo_ot_name"), rs.getString("lo_ot_id")));
			}
		} catch (Exception e) {
			logger.error("getCustomerlist(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCustomerlist(String, String, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql1) {
				logger.warn(
						"getCustomerlist(String, String, DataSource) - exception ignored",
						sql1);
			}

			try {
				stmt.close();
			} catch (SQLException sql1) {
				logger.warn(
						"getCustomerlist(String, String, DataSource) - exception ignored",
						sql1);
			}

			try {
				conn.close();
			} catch (SQLException sql1) {
				logger.warn(
						"getCustomerlist(String, String, DataSource) - exception ignored",
						sql1);
			}

		}
		return list;
	}

	public ArrayList getCompetencylist(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select *  from cp_corecompetency order by cp_corecomp_name ");
			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("cp_corecomp_name"), rs
						.getString("cp_corecomp_id")));
			}
		} catch (Exception e) {
			logger.error("getCompetencylist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCompetencylist(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCompetencylist(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCompetencylist(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCompetencylist(DataSource) - exception ignored",
						sql);
			}

		}

		return list;
	}

	public ArrayList getCertificationlist(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select *  from cp_certification order by cp_cert_comp_tech ");
			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("cp_cert_comp_tech"), rs
						.getString("cp_cert_id")));
			}
		} catch (Exception e) {
			logger.error("getCertificationlist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCertificationlist(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCertificationlist(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCertificationlist(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCertificationlist(DataSource) - exception ignored",
						sql);
			}

		}

		return list;
	}

	public ArrayList getPartnerCertificationlist(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		list.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select *  from cp_company order by cp_comp_name ");
			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("cp_comp_name"), rs.getString("cp_comp_id")));
			}
		} catch (Exception e) {
			logger.error("getPartnerCertificationlist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerCertificationlist(DataSource) - Exception caught"
						+ e);
			}
			logger.error("getPartnerCertificationlist(DataSource)", e);
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getPartnerCertificationlist(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getPartnerCertificationlist(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getPartnerCertificationlist(DataSource) - exception ignored",
						sql);
			}

		}

		return list;
	}

	public ArrayList getPartnertypelist(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select *  from lo_organization_top where lo_ot_type='P' order by lo_ot_name ");
			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("lo_ot_name"), rs.getString("lo_ot_id")));
			}
		} catch (Exception e) {
			logger.error("getPartnertypelist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnertypelist(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getPartnertypelist(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getPartnertypelist(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getPartnertypelist(DataSource) - exception ignored",
						sql);
			}

		}

		return list;
	}

	public ArrayList getFormlist(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select *  from cc_notification order by cc_notification_form ");
			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("cc_notification_form"), rs
						.getString("cc_notification_id")));
			}
		} catch (Exception e) {
			logger.error("getFormlist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getFormlist(DataSource) - Exception caught" + e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn("getFormlist(DataSource) - exception ignored", sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn("getFormlist(DataSource) - exception ignored", sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn("getFormlist(DataSource) - exception ignored", sql);
			}

		}

		return list;
	}

	// Start :Added By Amit
	public ArrayList getUnionSite() {
		ArrayList list = new ArrayList();

		list.add(new com.mind.common.LabelValue("YES", "Y"));
		list.add(new com.mind.common.LabelValue("NO", "N"));

		return list;
	}

	// End : Added By Amit

	/**
	 * @ This will give all listing of all msa's
	 * 
	 * @author amitm
	 */
	public ArrayList getMSAlist(DataSource ds) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String qry = "select * from dbo.func_lp_msa_list_001() order by lo_ot_name";
		list.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			// System.out.println("Getting query is for getting msa list for Combo Box =========> "+qry);
			rs = stmt.executeQuery(qry);
			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("lo_ot_name"), rs.getString("lp_mm_id")));
			}
		} catch (Exception e) {
			logger.error("getMSAlist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMSAlist(DataSource) - Exception caught" + e);
			}
			logger.error("getMSAlist(DataSource)", e);
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn("getMSAlist(DataSource) - exception ignored", sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn("getMSAlist(DataSource) - exception ignored", sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn("getMSAlist(DataSource) - exception ignored", sql);
			}

		}
		return list;
	}

	// Insert record in cp technician name
	public static int saveRecommendedRestrictedPartner(String pid, String type,
			String mmId, String prId, String createdBy, DataSource ds) {

		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;

		String sql = "insert into cp_partner_recommend_restrict (cp_partner_id, cp_rr_type, "
				+ "cp_rr_mm_id, cp_rr_pr_id, "
				+ "cp_rr_created_date, cp_rr_created_by) VALUES (?, ?, ?, ?, getdate(), ?)";
		try {

			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, Integer.parseInt(pid));
			pStmt.setInt(2, Integer.parseInt(type));
			pStmt.setInt(3, Integer.parseInt(mmId));
			pStmt.setObject(4, null);
			pStmt.setInt(5, Integer.parseInt(createdBy));
			pStmt.executeUpdate();

		} catch (Exception e) {
			logger.error(
					"saveRecommendedRestrictedPartner(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("saveRecommendedRestrictedPartner(String, String, String, String, String, DataSource) - Error in coordinating partner information with the Web.");
			}
		}

		finally {

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"saveRecommendedRestrictedPartner(String, String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return 0;
	}
}
