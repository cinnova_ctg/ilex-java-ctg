package com.mind.common.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.OpenSearchWindowBean;
import com.mind.bean.SearchProblemCategoryBean;
import com.mind.common.LabelValue;
import com.mind.common.bean.OpenSearchWindow;

public class OpenSearchWindowdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(OpenSearchWindowdao.class);

	public static ArrayList getCategory(String type, String clr, DataSource ds) {
		ArrayList category = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		category.add(new com.mind.common.LabelValue("---Select---", "0"));
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			// sql =
			// "select iv_ct_id , iv_ct_name from iv_category where iv_ct_type like '%"+type+"%'"
			// +"and iv_ct_name like '%Contract%'";

			sql = "select iv_ct_id , iv_ct_name from iv_category where iv_ct_type like '%"
					+ type + "%'";

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				category.add(new com.mind.common.LabelValue(rs
						.getString("iv_ct_name"), rs.getString("iv_ct_id")));
			}
		} catch (Exception e) {
			logger.error("getCategory(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCategory(String, String, DataSource) - Exception caught in rerieving Category"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getCategory(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getCategory(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getCategory(String, String, DataSource) - exception ignored",
						s);
			}

		}
		return category;
	}

	public static ArrayList getSubcategory(String category_id, String clr,
			DataSource ds) {
		ArrayList subcategory = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		subcategory.add(new com.mind.common.LabelValue("---Select---", "0"));
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			// sql =
			// "select iv_mf_id , iv_mf_name from iv_manufacturer where iv_mf_ct_id="+category_id+" and iv_mf_name like '%unschedule%'";

			sql = "select iv_mf_id , iv_mf_name from iv_manufacturer where iv_mf_ct_id="
					+ category_id;

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				subcategory.add(new com.mind.common.LabelValue(rs
						.getString("iv_mf_name"), rs.getString("iv_mf_id")));
			}
		} catch (Exception e) {
			logger.error("getSubcategory(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSubcategory(String, String, DataSource) - Exception caught in rerieving Subcategory"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getSubcategory(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getSubcategory(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getSubcategory(String, String, DataSource) - exception ignored",
						s);
			}

		}
		return subcategory;
	}

	public static ArrayList getOrganizationPOCList(String lo_ot_id,
			DataSource ds) {
		ArrayList poccombo = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		poccombo.add(new com.mind.common.LabelValue("---Select---", "0"));
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select * from dbo.func_search_list('" + lo_ot_id
					+ "','C','%', '%%', 'requestor_search')";
			// System.out.println("poc search ::::::::::::"+sql);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				poccombo.add(new com.mind.common.LabelValue(rs.getString(2), rs
						.getString(2) + "~" + rs.getString(3)));
			}
		} catch (Exception e) {
			logger.error("getOrganizationPOCList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOrganizationPOCList(String, DataSource) - Exception caught in rerieving get Organization POC"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getOrganizationPOCList(String, DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getOrganizationPOCList(String, DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getOrganizationPOCList(String, DataSource) - exception ignored",
						s);
			}

		}
		return poccombo;
	}

	public static ArrayList getSubsubcategory(String subcategory_id,
			DataSource ds) {
		ArrayList subsubcategory = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		subsubcategory.add(new com.mind.common.LabelValue("---Select---", "0"));
		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select iv_sc_id , iv_sc_name from iv_subcategory where iv_sc_mf_id="
					+ subcategory_id;
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				subsubcategory.add(new com.mind.common.LabelValue(rs
						.getString("iv_sc_name"), rs.getString("iv_sc_id")));
			}
		} catch (Exception e) {
			logger.error("getSubsubcategory(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSubsubcategory(String, DataSource) - Exception caught in rerieving Subsubcategory"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getSubsubcategory(String, DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getSubsubcategory(String, DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getSubsubcategory(String, DataSource) - exception ignored",
						s);
			}

		}
		return subsubcategory;
	}

	public static ArrayList getResourcelist(
			OpenSearchWindowBean opensearchwindoform, DataSource ds) {
		ArrayList resourcelist = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String category = "%%";
		String subcategory = "%%";
		String subsubcategory = "%%";
		OpenSearchWindow opensearchwindow = null;

		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (opensearchwindoform.getCategory().equals("0")) {
				category = "%%";
			} else {
				category = opensearchwindoform.getCategory();
			}

			if (opensearchwindoform.getSubcategory().equals("0")) {
				subcategory = "%%";
			} else {
				subcategory = opensearchwindoform.getSubcategory();
			}

			if (opensearchwindoform.getSubsubcategory().equals("0")) {
				subsubcategory = "%%";
			} else {
				subsubcategory = opensearchwindoform.getSubsubcategory();
			}

			sql = "select * from dbo.func_cc_search_resource_list( '"
					+ opensearchwindoform.getFrom() + "' , " + "'" + category
					+ "' , '" + subcategory + "' , " + "'" + subsubcategory
					+ "' , '%" + opensearchwindoform.getKeyword() + "%' , "
					+ "'" + opensearchwindoform.getActivity_Id() + "' , '"
					+ opensearchwindoform.getType() + "%" + "')";

			// System.out.println("sql"+sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				opensearchwindow = new OpenSearchWindow();
				opensearchwindow.setResource_id(rs.getString(1));
				opensearchwindow.setResource_name(rs.getString(2));
				opensearchwindow.setManufacturer_part_number(rs.getString(3));
				opensearchwindow.setUnit_cost(rs.getString(4));
				opensearchwindow.setCns_part_number(rs.getString(5));
				opensearchwindow.setSellable_quantity(rs.getString(6));
				opensearchwindow.setMinimum_quantity(rs.getString(7));
				opensearchwindow.setAlternate_identifier(rs.getString(8));

				resourcelist.add(opensearchwindow);
			}
		} catch (Exception e) {
			logger.error("getResourcelist(OpenSearchWindowForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourcelist(OpenSearchWindowForm, DataSource) - Exception caught in rerieving resourcelist"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getResourcelist(OpenSearchWindowForm, DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getResourcelist(OpenSearchWindowForm, DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getResourcelist(OpenSearchWindowForm, DataSource) - exception ignored",
						s);
			}

		}
		return resourcelist;
	}

	public static ArrayList getPOClist(String id, String lo_ot_id, String type,
			String keyword, String search_type, DataSource ds) {
		ArrayList problecategorylist = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String poc = "%%";

		SearchProblemCategoryBean searchwindow = null;

		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (id.equals("0")) {
				poc = "%%";
			} else {
				poc = id;
			}

			sql = "select * from dbo.func_search_list('" + lo_ot_id + "','"
					+ type + "','" + poc + "', '%" + keyword + "%', '"
					+ search_type + "')";

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				searchwindow = new SearchProblemCategoryBean();
				searchwindow.setProblemcategory_id(rs.getString(1));
				searchwindow.setProblemcategory_desc(rs.getString(2));
				searchwindow.setPoc_email(rs.getString(3));
				problecategorylist.add(searchwindow);
			}
		} catch (Exception e) {
			logger.error(
					"getPOClist(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPOClist(String, String, String, String, String, DataSource) - Exception caught in rerieving get POC list"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getPOClist(String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getPOClist(String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getPOClist(String, String, String, String, String, DataSource) - exception ignored",
						s);
			}

		}
		return problecategorylist;
	}

	public static ArrayList getProblemCategorylist(String id, String keyword,
			DataSource ds) {
		ArrayList problecategorylist = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String category = "%%";

		SearchProblemCategoryBean searchwindow = null;

		String sql = "";

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			if (id.equals("-1")) {
				category = "%%";
			} else {
				category = id;
			}

			sql = "select * from dbo.func_cc_search_prob_cate_list('"
					+ category + "', '%" + keyword + "%')";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				searchwindow = new SearchProblemCategoryBean();
				searchwindow.setProblemcategory_id(rs.getString(1));
				searchwindow.setProblemcategory_desc(rs.getString(2));

				problecategorylist.add(searchwindow);
			}
		} catch (Exception e) {
			logger.error("getProblemCategorylist(String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getProblemCategorylist(String, String, DataSource) - Exception caught in rerieving resourcelist"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException s) {
				logger.warn(
						"getProblemCategorylist(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				stmt.close();
			} catch (SQLException s) {
				logger.warn(
						"getProblemCategorylist(String, String, DataSource) - exception ignored",
						s);
			}

			try {
				conn.close();
			} catch (SQLException s) {
				logger.warn(
						"getProblemCategorylist(String, String, DataSource) - exception ignored",
						s);
			}

		}
		return problecategorylist;
	}

	public static java.util.Collection getMSANameList(DataSource ds) {

		ArrayList criticalityname = new java.util.ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		criticalityname.add(new com.mind.common.LabelValue("Select", "0"));
		String sql = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			// sql =
			// "select lo_ot_id, lo_ot_name from lo_organization_top group by lo_ot_id, lo_ot_name order by lo_ot_name";
			sql = "select poc_id , poc_name from dbo.func_search_list('0', '0', '%', '%', 'company_search')";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				criticalityname.add(new com.mind.common.LabelValue(rs
						.getString("poc_name"), rs.getString("poc_id")));
			}
		} catch (Exception e) {
			logger.error("getMSANameList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMSANameList(DataSource) - Exception caught in retrieving MSA Name List"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql1) {
				logger.warn("getMSANameList(DataSource) - exception ignored",
						sql1);
			}

			try {
				stmt.close();
			} catch (SQLException sql1) {
				logger.warn("getMSANameList(DataSource) - exception ignored",
						sql1);
			}

			try {
				conn.close();
			} catch (SQLException sql1) {
				logger.warn("getMSANameList(DataSource) - exception ignored",
						sql1);
			}

		}

		return criticalityname;

	}

}
