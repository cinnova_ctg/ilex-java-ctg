package com.mind.common.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.pvs.Partner_SearchBean;
import com.mind.common.LabelValue;

public class DynamicComboDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DynamicComboDao.class);

	public static java.util.Collection<LabelValue> getStateName(DataSource ds) {

		ArrayList<LabelValue> statename = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		statename.add(new com.mind.common.LabelValue("Select State", "-1"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_state_id, cp_state_name from cp_state order by cp_state_id");
			while (rs.next()) {
				statename.add(new com.mind.common.LabelValue(rs
						.getString("cp_state_id")
						+ " "
						+ rs.getString("cp_state_name"), rs
						.getString("cp_state_id")));
			}
		} catch (Exception e) {
			logger.error("getStateName(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStateName(DataSource) - Exception caught in rerieving State Name"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn("getStateName(DataSource) - exception ignored", sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn("getStateName(DataSource) - exception ignored", sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn("getStateName(DataSource) - exception ignored", sql);
			}

		}

		return statename;
	}

	public static java.util.Collection<LabelValue> getExportStatusList(
			DataSource ds) {

		ArrayList<LabelValue> exportStatusList = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		exportStatusList.add(new com.mind.common.LabelValue("--Select--", ""));
		exportStatusList.add(new com.mind.common.LabelValue("All", "%"));
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select distinct lm_js_export_status from lm_job where isnull(lm_js_export_status, '') <> ''");
			while (rs.next()) {
				exportStatusList.add(new com.mind.common.LabelValue(rs
						.getString("lm_js_export_status"), rs
						.getString("lm_js_export_status")));
			}
		} catch (Exception e) {
			logger.error("getExportStatusList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getExportStatusList(DataSource) - Exception caught in rerieving Country List"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getExportStatusList(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getExportStatusList(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getExportStatusList(DataSource) - exception ignored",
						sql);
			}

		}

		return exportStatusList;
	}

	public static java.util.Collection<LabelValue> getCountryList(DataSource ds) {

		ArrayList<LabelValue> countryList = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		countryList.add(new com.mind.common.LabelValue("--Select--", " "));
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select distinct cp_country_id, cp_country_order from cp_state where cp_country_id is not null order by cp_country_order");
			while (rs.next()) {
				countryList.add(new com.mind.common.LabelValue(rs
						.getString("cp_country_id"), rs
						.getString("cp_country_id")));
			}
		} catch (Exception e) {
			logger.error("getCountryList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCountryList(DataSource) - Exception caught in rerieving Country List"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn("getCountryList(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn("getCountryList(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn("getCountryList(DataSource) - exception ignored",
						sql);
			}

		}

		return countryList;
	}

	public static java.util.Collection<LabelValue> getGroupNameListSiteSearch(
			String msaId, DataSource ds) {

		ArrayList<LabelValue> groupname = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		groupname.add(new com.mind.common.LabelValue("--Select--", " "));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select distinct isnull(lp_si_group,'') as lp_si_group from lp_site_list where lp_si_group <> '' and lp_md_mm_id = "
							+ msaId);
			while (rs.next()) {
				groupname
						.add(new com.mind.common.LabelValue(rs
								.getString("lp_si_group"), rs
								.getString("lp_si_group")));
			}
		} catch (Exception e) {
			logger.error("getGroupNameListSiteSearch(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getGroupNameListSiteSearch(String, DataSource) - Exception caught in rerieving Group Name Site Search"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getGroupNameListSiteSearch(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getGroupNameListSiteSearch(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getGroupNameListSiteSearch(String, DataSource) - exception ignored",
						sql);
			}

		}

		return groupname;
	}

	public static java.util.Collection<LabelValue> getSiteGroupNameList(
			String msaId, DataSource ds) {

		ArrayList<LabelValue> groupname = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		groupname.add(new com.mind.common.LabelValue("--Select--", " "));
		groupname.add(new com.mind.common.LabelValue("All", "%"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select distinct lp_si_group from lp_site_list where isnull(lp_si_group,'') <> '' and lp_md_mm_id = "
							+ msaId);
			while (rs.next()) {
				groupname
						.add(new com.mind.common.LabelValue(rs
								.getString("lp_si_group"), rs
								.getString("lp_si_group")));
			}
		} catch (Exception e) {
			logger.error("getSiteGroupNameList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteGroupNameList(String, DataSource) - Exception caught in rerieving Group Name"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getSiteGroupNameList(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getSiteGroupNameList(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getSiteGroupNameList(String, DataSource) - exception ignored",
						sql);
			}

		}

		return groupname;
	}

	public static java.util.Collection<LabelValue> getSiteGroupNameList(
			DataSource ds) {

		ArrayList<LabelValue> groupname = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		groupname.add(new com.mind.common.LabelValue("--Select--", " "));
		groupname.add(new com.mind.common.LabelValue("All", "%"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select distinct isnull(lp_si_group,'') as lp_si_group from lp_site_list where lp_si_group <>''");
			while (rs.next()) {
				groupname
						.add(new com.mind.common.LabelValue(rs
								.getString("lp_si_group"), rs
								.getString("lp_si_group")));
			}
		} catch (Exception e) {
			logger.error("getSiteGroupNameList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteGroupNameList(DataSource) - Exception caught in rerieving Group Name"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getSiteGroupNameList(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getSiteGroupNameList(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getSiteGroupNameList(DataSource) - exception ignored",
						sql);
			}

		}

		return groupname;
	}

	public static java.util.Collection<LabelValue> getEndCustomerList(
			DataSource ds, String msaid, String msaname) {

		ArrayList<LabelValue> endcustomername = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		endcustomername.add(new com.mind.common.LabelValue("--Select--", " "));
		// endcustomername.add( new com.mind.common.LabelValue( msaname ,
		// msaname) );

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from dbo.func_lx_endcustomer_name('"
							+ msaid + "')");
			while (rs.next()) {

				endcustomername.add(new com.mind.common.LabelValue(rs
						.getString("lo_ot_name"), rs.getString("lo_ot_name")));
			}
		} catch (Exception e) {
			logger.error("getEndCustomerList(" + ds + "," + msaid + ","
					+ msaname + ")", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEndCustomerList(DataSource, String, String) - Exception caught in retrieving  End Customer Name"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getEndCustomerList(DataSource, String, String) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getEndCustomerList(DataSource, String, String) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getEndCustomerList(DataSource, String, String) - exception ignored",
						sql);
			}

		}

		return endcustomername;
	}

	public static java.util.Collection<LabelValue> getSiteEndCustomerList(
			DataSource ds, String msaid, String msaname) {

		ArrayList<LabelValue> endcustomername = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		endcustomername.add(new LabelValue("--Select--", " "));
		endcustomername.add(new LabelValue("All", "%"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from dbo.func_lx_endcustomer_name('"
							+ msaid + "')");
			while (rs.next()) {
				endcustomername.add(new LabelValue(rs.getString("lo_ot_name"),
						rs.getString("lo_ot_name")));
			}
		} catch (Exception e) {
			logger.error("getSiteEndCustomerList(DataSource, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSiteEndCustomerList(DataSource, String, String) - Exception caught in retrieving  End Customer Name"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getSiteEndCustomerList(DataSource, String, String) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getSiteEndCustomerList(DataSource, String, String) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getSiteEndCustomerList(DataSource, String, String) - exception ignored",
						sql);
			}

		}

		return endcustomername;
	}

	public static java.util.Collection<LabelValue> getMonthName() {

		ArrayList<LabelValue> monthname = new java.util.ArrayList<LabelValue>();
		monthname.add(new com.mind.common.LabelValue("Month", ""));
		monthname.add(new com.mind.common.LabelValue("Jan", "1"));
		monthname.add(new com.mind.common.LabelValue("Feb", "2"));
		monthname.add(new com.mind.common.LabelValue("Mar", "3"));
		monthname.add(new com.mind.common.LabelValue("Apr", "4"));
		monthname.add(new com.mind.common.LabelValue("May", "5"));
		monthname.add(new com.mind.common.LabelValue("Jun", "6"));
		monthname.add(new com.mind.common.LabelValue("Jul", "7"));
		monthname.add(new com.mind.common.LabelValue("Aug", "8"));
		monthname.add(new com.mind.common.LabelValue("Sep", "9"));
		monthname.add(new com.mind.common.LabelValue("Oct", "10"));
		monthname.add(new com.mind.common.LabelValue("Nov", "11"));
		monthname.add(new com.mind.common.LabelValue("Dec", "12"));

		return monthname;
	}

	public static java.util.Collection<LabelValue> getlondirec() {

		ArrayList<LabelValue> valuelist = new java.util.ArrayList<LabelValue>();
		valuelist.add(new com.mind.common.LabelValue("N", "N"));
		valuelist.add(new com.mind.common.LabelValue("S", "S"));
		return valuelist;
	}

	public static java.util.Collection<LabelValue> getlatdirec() {

		ArrayList<LabelValue> valuelist = new java.util.ArrayList<LabelValue>();
		valuelist.add(new com.mind.common.LabelValue("E", "E"));
		valuelist.add(new com.mind.common.LabelValue("W", "W"));
		return valuelist;
	}

	public static java.util.Collection<LabelValue> getPartnerTypeCombo() {
		ArrayList<LabelValue> valuelist = new java.util.ArrayList<LabelValue>();
		valuelist.add(new com.mind.common.LabelValue("--Select--", "0"));
		valuelist.add(new com.mind.common.LabelValue("Certified", "S"));
		valuelist.add(new com.mind.common.LabelValue("Speedpay", "SS"));
		valuelist.add(new com.mind.common.LabelValue("Minuteman", "M"));
		valuelist.add(new com.mind.common.LabelValue("All", "B"));
		return valuelist;
	}

	public static java.util.Collection<LabelValue> getRadius() {
		ArrayList<LabelValue> valuelist = new java.util.ArrayList<LabelValue>();
		valuelist.add(new com.mind.common.LabelValue("--Select--", "0"));
		valuelist.add(new com.mind.common.LabelValue("10", "10"));
		valuelist.add(new com.mind.common.LabelValue("25", "25"));
		valuelist.add(new com.mind.common.LabelValue("50", "50"));
		valuelist.add(new com.mind.common.LabelValue("75", "75"));
		valuelist.add(new com.mind.common.LabelValue("100", "100"));
		valuelist.add(new com.mind.common.LabelValue("250", "250"));
		valuelist.add(new com.mind.common.LabelValue("500", "500"));
		return valuelist;
	}

	public static java.util.ArrayList<Partner_SearchBean> getCertificationTypes(
			DataSource ds) {
		ArrayList<Partner_SearchBean> certifications = new java.util.ArrayList<Partner_SearchBean>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select cp_corecomp_id,cp_corecomp_name from cp_corecompetency where cp_corecomp_short_name in ('W','SD','IT','ITA')");
			int i = 0;

			if (i == 0) {
				Partner_SearchBean pSForm = new Partner_SearchBean();
				pSForm.setCertificationsId("");
				pSForm.setCertificationType("");
				certifications.add(i, pSForm);
				i++;
			}

			while (rs.next()) {
				Partner_SearchBean pSForm = new Partner_SearchBean();
				pSForm.setCertificationsId(rs.getString(1));
				pSForm.setCertificationType(rs.getString(2));
				certifications.add(i, pSForm);
				i++;
			}

		} catch (Exception E) {
			logger.error("getCertificationTypes(DataSource)", E);

			if (logger.isDebugEnabled()) {
				logger.debug("getCertificationTypes(DataSource) - Error Occured while getting All Certifications ::: "
						+ E.getMessage());
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCertificationTypes(DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCertificationTypes(DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCertificationTypes(DataSource) - exception ignored",
						e);
			}

		}
		return certifications;
	}

	public static ArrayList<LabelValue> getCategoryWiseCertifications(
			String catId, int count, DataSource ds) {
		ArrayList<LabelValue> valuelist = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		if (catId.equals(""))
			catId = "0";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select distinct cp_comp_id,cp_comp_name from cp_companycerts "
					+ "left outer join cp_company on cp_cert_comp_id = cp_comp_id "
					+ "where cp_cert_corecomp_id = '"
					+ catId
					+ "' order by cp_comp_name asc";
			rs = stmt.executeQuery(sql);

			if (count == 0)
				valuelist
						.add(new com.mind.common.LabelValue("--Select--", "0"));
			else {
				while (rs.next()) {
					valuelist.add(new com.mind.common.LabelValue(rs
							.getString(2), rs.getString(1)));
				}
			}

		} catch (Exception E) {
			logger.error(
					"getCategoryWiseCertifications(String, int, DataSource)", E);

			if (logger.isDebugEnabled()) {
				logger.debug("getCategoryWiseCertifications(String, int, DataSource) - Error Occured while getting All Certifications Category Wise  ::: "
						+ E.getMessage());
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getCategoryWiseCertifications(String, int, DataSource) - exception ignored",
						e);
			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				logger.warn(
						"getCategoryWiseCertifications(String, int, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getCategoryWiseCertifications(String, int, DataSource) - exception ignored",
						e);
			}

		}

		return valuelist;
	}

	public static java.util.Collection<LabelValue> getsitestatus() {

		ArrayList<LabelValue> valuelist = new java.util.ArrayList<LabelValue>();
		valuelist.add(new com.mind.common.LabelValue("A", "A"));
		valuelist.add(new com.mind.common.LabelValue("I", "I"));
		return valuelist;
	}

	public static java.util.Collection<LabelValue> getstatuslist() {
		ArrayList<LabelValue> valuelist = new java.util.ArrayList<LabelValue>();
		valuelist.add(new com.mind.common.LabelValue("All", " "));
		valuelist.add(new com.mind.common.LabelValue("Active", "A"));
		valuelist.add(new com.mind.common.LabelValue("Inactive", "I"));
		return valuelist;
	}

	public static java.util.Collection<LabelValue> gettimezone(DataSource ds) {

		ArrayList<LabelValue> timezone = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		timezone.add(new com.mind.common.LabelValue("--Select--", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery("select * from cc_time_zone");
			while (rs.next()) {

				timezone.add(new com.mind.common.LabelValue(rs
						.getString("cc_tz_name"), rs.getString("cc_tz_id")));
			}
		} catch (Exception e) {
			logger.error("gettimezone(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("gettimezone(DataSource) - Exception caught in retrieving  Time Zone List"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn("gettimezone(DataSource) - exception ignored", sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn("gettimezone(DataSource) - exception ignored", sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn("gettimezone(DataSource) - exception ignored", sql);
			}

		}

		return timezone;
	}

	public static java.util.Collection<LabelValue> getClassification(
			DataSource ds) {

		ArrayList<LabelValue> classification = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		classification.add(new com.mind.common.LabelValue("--Select--", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from lo_classification where lo_cl_active=1 order by lo_cl_role");
			while (rs.next()) {

				classification.add(new com.mind.common.LabelValue(rs
						.getString("lo_cl_role"), rs.getString("lo_cl_id")));
			}
		} catch (Exception e) {
			logger.error("getClassification(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getClassification(DataSource) - Exception caught in retrieving  Time Zone List"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getClassification(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getClassification(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getClassification(DataSource) - exception ignored",
						sql);
			}

		}

		return classification;
	}

	/*
	 * 
	 * public static java.util.Collection getCountryList() {
	 * 
	 * ArrayList valuelist = new java.util.ArrayList(); valuelist.add ( new
	 * com.mind.common.LabelValue("--Select--"," ")); valuelist.add ( new
	 * com.mind.common.LabelValue("US","US")); valuelist.add ( new
	 * com.mind.common.LabelValue("CA","CA")); valuelist.add ( new
	 * com.mind.common.LabelValue("MX","MX")); return valuelist; }
	 */

	public static java.util.Collection<LabelValue> getCategoryList(DataSource ds) {

		ArrayList<LabelValue> valuelist = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		valuelist.add(new com.mind.common.LabelValue("--Select--", " "));
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select distinct cp_category_id from cp_state where cp_category_id is not null");
			while (rs.next()) {
				valuelist.add(new com.mind.common.LabelValue(rs
						.getString("cp_category_id"), rs
						.getString("cp_category_id")));
			}
		} catch (Exception e) {
			logger.error("getCategoryList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCategoryList(DataSource) - Exception caught in rerieving Category List"
						+ e);
			}

		} finally {
			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn("getCategoryList(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn("getCategoryList(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn("getCategoryList(DataSource) - exception ignored",
						sql);
			}
		}

		return valuelist;
	}

	public static java.util.Collection<LabelValue> getRegionList(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<LabelValue> valuelist = new java.util.ArrayList<LabelValue>();
		valuelist.add(new com.mind.common.LabelValue("--Select--", " "));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select distinct cp_region_id from cp_state where cp_region_id is not null");
			while (rs.next()) {
				valuelist.add(new com.mind.common.LabelValue(rs
						.getString("cp_region_id"), rs
						.getString("cp_region_id")));
			}
		} catch (Exception e) {
			logger.error("getRegionList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRegionList(DataSource) - Exception caught in rerieving Region List"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn("getRegionList(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn("getRegionList(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn("getRegionList(DataSource) - exception ignored",
						sql);
			}
		}

		return valuelist;
	}

	public static java.util.Collection<LabelValue> getDayName() {

		ArrayList<LabelValue> dayname = new java.util.ArrayList<LabelValue>();
		int i = 0;
		dayname.add(new com.mind.common.LabelValue("Day", "-1"));
		try {
			for (i = 1; i <= 31; i++) {
				dayname.add(new com.mind.common.LabelValue("" + i, "" + i));
			}
		} catch (Exception e) {
			logger.error("getDayName()", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDayName() - Exception caught in rerieving Day Name"
						+ e);
			}
		}

		return dayname;
	}

	public static java.util.Collection<LabelValue> getHourName() {

		ArrayList<LabelValue> hourname = new java.util.ArrayList<LabelValue>();
		int i = 0;
		hourname.add(new com.mind.common.LabelValue("Hour", "  "));
		try {
			for (i = 1; i <= 12; i++) {
				if (i < 10)
					hourname.add(new com.mind.common.LabelValue("0" + i, "0"
							+ i));
				else
					hourname.add(new com.mind.common.LabelValue("" + i, "" + i));
			}
		} catch (Exception e) {
			logger.error("getHourName()", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getHourName() - Exception caught in rerieving Hour Name"
						+ e);
			}
		}

		return hourname;
	}

	public static java.util.Collection<LabelValue> getMinuteName() {

		ArrayList<LabelValue> minutename = new java.util.ArrayList<LabelValue>();
		int i = 0;
		minutename.add(new com.mind.common.LabelValue("Minutes", "  "));

		for (i = 0; i <= 59; i++) {
			if (i < 10)
				minutename
						.add(new com.mind.common.LabelValue("0" + i, "0" + i));
			else
				minutename.add(new com.mind.common.LabelValue("" + i, "" + i));
		}

		// minutename.add ( new com.mind.common.LabelValue("00","00"));
		// minutename.add ( new com.mind.common.LabelValue("15","15"));
		// minutename.add ( new com.mind.common.LabelValue("30","30"));
		// minutename.add ( new com.mind.common.LabelValue("45","45"));

		return minutename;
	}

	public static java.util.Collection<LabelValue> getOptionName() {

		ArrayList<LabelValue> optionname = new java.util.ArrayList<LabelValue>();
		optionname.add(new com.mind.common.LabelValue("AM", "AM"));
		optionname.add(new com.mind.common.LabelValue("PM", "PM"));

		return optionname;
	}

	public static java.util.Collection<LabelValue> getCriticalityName(
			String msaid, DataSource ds) {

		ArrayList<LabelValue> criticalityname = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		criticalityname.add(new com.mind.common.LabelValue("Select", "-1"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from dbo.func_lo_criticality_name('"
							+ msaid + "')");
			while (rs.next()) {
				criticalityname.add(new com.mind.common.LabelValue(rs
						.getString("lo_call_criticality_des"), rs
						.getString("lo_call_criticality_id")));
			}
		} catch (Exception e) {
			logger.error("getCriticalityName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCriticalityName(String, DataSource) - Exception caught in retrieving criticalityName IN getCriticalityName()"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCriticalityName(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCriticalityName(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getCriticalityName(String, DataSource) - exception ignored",
						sql);
			}

		}

		return criticalityname;

	}

	public static java.util.Collection<LabelValue> getActivityCategory(
			DataSource ds) {

		ArrayList<LabelValue> activitycategory = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		activitycategory.add(new com.mind.common.LabelValue("Select", "-1"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from dbo.func_lm_activity_categoy()");
			while (rs.next()) {
				activitycategory.add(new LabelValue(rs.getString(2), rs
						.getString(1)));
			}
		} catch (Exception e) {
			logger.error("getActivityCategory(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActivityCategory(DataSource) - Exception caught in retrieving criticalityName IN getCriticalityName()"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getActivityCategory(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getActivityCategory(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getActivityCategory(DataSource) - exception ignored",
						sql);
			}

		}

		return activitycategory;

	}

	public static java.util.Collection<LabelValue> getResourceLevel(
			String appendixId, String crit_cat_id, DataSource ds) {

		ArrayList<LabelValue> resourcelevel = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		resourcelevel.add(new com.mind.common.LabelValue("Select", "-1"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt.executeQuery("select * from dbo.func_lo_resource_level('"
					+ appendixId + "'," + crit_cat_id + ")");

			while (rs.next()) {
				resourcelevel.add(new LabelValue(rs.getString(2), rs
						.getString(1)));
			}
		} catch (Exception e) {
			logger.error("getResourceLevel(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getResourceLevel(String, String, DataSource) - Exception caught in retrieving Resource Level"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getResourceLevel(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getResourceLevel(String, String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getResourceLevel(String, String, DataSource) - exception ignored",
						sql);
			}

		}

		return resourcelevel;

	}

	public static java.util.Collection<LabelValue> getProblemCategory(
			DataSource ds) {

		ArrayList<LabelValue> problemcategory = new java.util.ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		problemcategory.add(new com.mind.common.LabelValue("Select", "-1"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select * from cc_problem_category_detail where cc_pc_status  = 'A' order by cc_pc_desc");
			while (rs.next()) {
				problemcategory.add(new LabelValue(rs.getString(2), rs
						.getString(1)));
			}
		} catch (Exception e) {
			logger.error("getProblemCategory(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getProblemCategory(DataSource) - Exception caught in retrieving criticalityName IN getting Problem Category"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getProblemCategory(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getProblemCategory(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getProblemCategory(DataSource) - exception ignored",
						sql);
			}

		}

		return problemcategory;

	}

	public static Collection<LabelValue> getCNSPOCTitles(DataSource ds) {

		ArrayList<LabelValue> POCTitles = new ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		POCTitles.add(new com.mind.common.LabelValue("--Select--", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select * from lo_employee_job_titles order by lo_emp_jt_title");

			while (rs.next()) {
				POCTitles.add(new com.mind.common.LabelValue(rs
						.getString("lo_emp_jt_title"), rs
						.getString("lo_emp_jt_title")));
			}
		} catch (Exception e) {
			logger.error("getCNSPOCTitles(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCNSPOCTitles(DataSource) - Exception caught in retrieving  CNS POC Titles"
						+ e);
			}
		} finally {
			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn("getCNSPOCTitles(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn("getCNSPOCTitles(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn("getCNSPOCTitles(DataSource) - exception ignored",
						sql);
			}
		}

		return POCTitles;
	}

	/**
	 * Gets the rank list.
	 * 
	 * @param ds
	 *            the ds
	 * @param departmentId
	 *            the department id
	 * @return the rank list
	 */
	public static List<String> getRankList(DataSource ds, String departmentId) {

		List<String> rankList = new ArrayList<String>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select lo_ra_rank_value  from lo_rank where lo_ra_cl_id="
							+ departmentId + "");
			while (rs.next()) {

				rankList.add(rs.getString("lo_ra_rank_value"));
			}
		} catch (Exception e) {
			logger.error("getRankList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRankList(DataSource) - Exception caught in retrieving Rank List"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn("getRankList(DataSource) - exception ignored", sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn("getRankList(DataSource) - exception ignored", sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn("getRankList(DataSource) - exception ignored", sql);
			}

		}

		return rankList;
	}
}
