package com.mind.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class Authenticationdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(Authenticationdao.class);

	public static boolean getPageSecurity(String user_id, String cc_fu_name,
			String cc_fu_type, DataSource ds) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean pagesecurity = false;

		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select 1 from lo_role_privilege a, lo_poc_rol b, cc_function c where b.lo_pr_pc_id = ? and a.lo_rp_ro_id = b.lo_pr_ro_id and a.lo_rp_fu_id = c.cc_fu_id and c.cc_fu_name = ? and c.cc_fu_type = ?");
			pstmt.setString(1, user_id);
			pstmt.setString(2, cc_fu_name);
			pstmt.setString(3, cc_fu_type);
			rs = pstmt.executeQuery();
			if (rs.next())
				pagesecurity = true;
		}

		catch (Exception e) {
			logger.error("getPageSecurity(String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPageSecurity(String, String, String, DataSource) - Error occured during Page Security");
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"getPageSecurity(String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
				logger.warn(
						"getPageSecurity(String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"getPageSecurity(String, String, String, DataSource) - exception ignored",
						e);
			}

		}

		return pagesecurity;
	}

}
