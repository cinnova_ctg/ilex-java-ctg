package com.mind.common.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.bean.Checklist;

public class Checklistdao 
{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Checklistdao.class);

	
	public  static ArrayList getQCchecklist( DataSource ds )
	{
		
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		ArrayList valuelist = new ArrayList();
		
		
		
		try{
			conn = ds.getConnection();
			//String sql = "select lm_ci_id,lm_ci_group,lm_ci_item_name,lm_ci_option_type,lm_ci_type from dbo.func_lm_master_appendix_qc_site_checklist("+Appendix_Id+")";						   		
			cstmt = conn.prepareCall("{?=call lm_master_qc_checklist(  )}");
			cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			rs = cstmt.executeQuery();
			//rs = stmt.executeQuery( sql );
			
			
			while( rs.next() )
			{
			
				Checklist scBean = new Checklist();
								 
				scBean.setChecklistid( rs.getString( "lm_ci_id" ) );
				//scBean.setChecklistname( rs.getString( "lm_ci_group" ) );
				scBean.setGroupname( rs.getString( "lm_ci_group" ) );
				scBean.setItemname( rs.getString( "lm_ci_item_name" ) );			 
				//scBean.setItemdesc( rs.getString( "lm_ci_option_type" ) );
				scBean.setOptiontype( rs.getString( "lm_ci_option_type" ) );
				scBean.setChecklisttype( rs.getString( "lm_ci_type" ) );
				scBean.setOption(rs.getString( "lm_co_option" ));
				scBean.setChecklistidtype(scBean.getChecklisttype()+ scBean.getChecklistid() );
				
				valuelist.add( scBean );
						 
			}
			
		}
		catch( Exception e ){
			logger.error("getQCchecklist(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getQCchecklist(DataSource) - Error occured during getting QCchecklist" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs != null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getQCchecklist(DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt != null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getQCchecklist(DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn != null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getQCchecklist(DataSource) - exception ignored", e);
}
			
		}
		return valuelist;	
	}
	
	
	
	
	public  static String[] getQCchecklistforappendix( String Appendix_Id , DataSource ds )
	{
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valuelist = new ArrayList();
		String[] appendixckecklistid = null;
		String temp = "";
		
		try{
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			
			String sql = "select lm_ci_app_ci_id,lm_ci_type from lm_appendix_qcchecklist where lm_ci_app_pr_id="+Appendix_Id;						   		
			//System.out.println("getQCchecklistforappendix SQL "+sql);
			rs = stmt.executeQuery( sql );
			
			
			while( rs.next() )
			{
			
				Checklist scBean = new Checklist();
								 
				temp = temp + rs.getString( "lm_ci_type" )+rs.getString( "lm_ci_app_ci_id" ); 
				temp = temp + ",";
				
				valuelist.add( scBean );
						 
			}
			
			appendixckecklistid = temp.split( "," );
			
		}
		catch( Exception e ){
			logger.error("getQCchecklistforappendix(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getQCchecklistforappendix(String, DataSource) - Error occured during getting Sitechecklist" + e);
			}
		}
		
		finally
		{
			try
			{
				if ( rs != null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("getQCchecklistforappendix(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( stmt != null ) 
					stmt.close();
			}
			catch( Exception e )
			{
				logger.warn("getQCchecklistforappendix(String, DataSource) - exception ignored", e);
}
			
			
			try
			{
				if ( conn != null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("getQCchecklistforappendix(String, DataSource) - exception ignored", e);
}
			
		}
		return appendixckecklistid;	
	}
		
	
	public  static int addappendixChecklistitem( String appendix_checklist_id , String lm_sc_flag , DataSource ds , String itemname )
	{
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		
		int retval = -1;
		try{
			conn = ds.getConnection();
			
			cstmt = conn.prepareCall( "{?=call lm_master_checklist_manage_01( ? , ? , ? )}" );
			cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			cstmt.setString( 2 , appendix_checklist_id );
			cstmt.setString( 3 , lm_sc_flag );
			cstmt.setString( 4 , itemname );
			cstmt.execute();
			retval = cstmt.getInt(1);
			
		}
		catch( Exception e ){
			logger.error("addappendixChecklistitem(String, String, DataSource, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addappendixChecklistitem(String, String, DataSource, String) - Error occured during updating Master Checklistitem" + e);
			}
		}
		

		finally
		{
			try
			{
				if ( rs != null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("addappendixChecklistitem(String, String, DataSource, String) - exception ignored", e);
}
			
			
			try
			{
				if ( cstmt != null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("addappendixChecklistitem(String, String, DataSource, String) - exception ignored", e);
}
			
			
			try
			{
				if ( conn != null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("addappendixChecklistitem(String, String, DataSource, String) - exception ignored", e);
}
			
		}
		return retval;		
			
			
	}
	
	public  static int addAppendixChecklist( String group , String itemname , String options , String option_type , DataSource ds )
	{
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		
		int retval = -1;
		try{
			conn = ds.getConnection();
			
			cstmt = conn.prepareCall("{?=call lm_master_qcchecklist_add_01( ? , ? , ? , ? )}");
			cstmt.registerOutParameter( 1 , java.sql.Types.INTEGER );
			cstmt.setString( 2 , group );
			cstmt.setString( 3 , itemname );
			cstmt.setString( 4 , options );
			cstmt.setString( 5 , option_type );
			
			//System.out.println( " '"+group+"' , '"+itemname+"' , '"+options+"' , '"+option_type+"'" );
			
			cstmt.execute();
			retval = cstmt.getInt( 1 );
			
		}
		catch( Exception e ){
			logger.error("addAppendixChecklist(String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("addAppendixChecklist(String, String, String, String, DataSource) - Error occured during adding master Checklist item " + e);
			}
		}
		

		finally
		{
			try
			{
				if ( rs != null ) 
				{
					rs.close();		
				}
			}
			
			catch( Exception e )
			{
				logger.warn("addAppendixChecklist(String, String, String, String, DataSource) - exception ignored", e);
				
			}
			try
			{
				if ( cstmt != null ) 
					cstmt.close();
			}
			catch( Exception e )
			{
				logger.warn("addAppendixChecklist(String, String, String, String, DataSource) - exception ignored", e);
				
			}
			
			try
			{
				if ( conn != null) 
					conn.close();
			}
			catch( Exception e )
			{
				logger.warn("addAppendixChecklist(String, String, String, String, DataSource) - exception ignored", e);
				
			}
		}
		return retval;				
	}	
}
