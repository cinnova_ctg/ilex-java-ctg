package com.mind.common.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.util.MySqlConnection;

public class ChangeStatus {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ChangeStatus.class);

	/*
	 * Following method is used to change the status of Appendix and Addendum.
	 * If status of Appendix is Inwork(I) then some information of Appendix and
	 * Default Job of that Appendix and Activities of that Default Job should be
	 * transfer from Ilex Application to Web Application.
	 */
	public static int Status(String MSA_Id, String Appendix_Id, String Job_Id,
			String Activitylibrary_Id, String resource_Id, String Type,
			String status, String firstjobsuccess, DataSource ds,
			String mySqlUrl) {
		return Status(MSA_Id, Appendix_Id, Job_Id, Activitylibrary_Id,
				resource_Id, Type, status, firstjobsuccess, ds, mySqlUrl, null);
	}

	public static int Status(String MSA_Id, String Appendix_Id, String Job_Id,
			String Activitylibrary_Id, String resource_Id, String Type,
			String status, String firstjobsuccess, DataSource ds,
			String mySqlUrl, String userId) {
		Connection conn = null;
		CallableStatement cstmt = null;
		CallableStatement cstmt2 = null;
		Statement stmt = null;
		Statement stmtDefault = null;
		ResultSet rs = null;
		ResultSet rsDefault = null;

		Connection mySqlConn = null;
		Statement mySQL = null;
		Statement mySqlDefault = null;
		ResultSet mySqlRs = null;

		String sql1 = "";

		// for default job entry into mysql table

		int activityId = 0;
		int acJobId = 0;
		String acTitle = "";
		String mySqlForActivity = "";
		int msaCheck = 0;
		int appendixCheck = 0;
		int defaultCheck = 0;
		int actCheck = 0;

		String Id = "";
		int val = 0;
		int val2 = 0;
		int mySqlVal = 0;
		int retVal = -1;

		String msaName = "";
		String appendixName = "";
		String endCustomer = "";
		int job_id = 0;
		String job_title = "";
		int app_type = 0;
		String bdm_name = "";
		String prm_name = "";
		String approved_Date = "";
		String sqlfordefault = "";

		// Addendum Change
		String addType = "";
		Connection connAdd = null;
		Statement stmtAdd = null;
		ResultSet rsAdd = null;

		if (Type.equals("MSA")) {
			Id = MSA_Id;
		}

		if (Type.equals("Appendix")) {
			Id = Appendix_Id;
		}

		if (Type.equals("Job")) {
			Id = Job_Id;
		}

		if (Type.equals("Activity")) {
			Id = Activitylibrary_Id;
		}

		if (Type.contains("pm_res")) {
			Id = resource_Id;
		}

		if (Type.equals("am")) {
			Id = Activitylibrary_Id;
		}

		if (Type.equals("M")) {
			Id = resource_Id;
		}

		if (Type.equals("L")) {
			Id = resource_Id;
		}

		if (Type.equals("F")) {
			Id = resource_Id;
		}

		if (Type.equals("T")) {
			Id = resource_Id;
		}

		if (Type.equals("T")) {
			Id = resource_Id;
		}

		if (Type.equals("prj_appendix")) {
			Id = Appendix_Id;
		}
		if (Type.equals("prj_job")) {
			Id = Job_Id;

			try {

				String strAdd = "";
				connAdd = ds.getConnection();
				stmtAdd = connAdd.createStatement();
				strAdd = "select * from lm_job where lm_js_type like '%Addendum%' and lm_js_id="
						+ Id;
				rsAdd = stmtAdd.executeQuery(strAdd);
				if (rsAdd.next()) {
					addType = "Addendum";
				}
			} catch (Exception e) {
				logger.error(
						"Status(String, String, String, String, String, String, String, String, DataSource, String, String)",
						e);

				if (logger.isDebugEnabled()) {
					logger.debug("Status(String, String, String, String, String, String, String, String, DataSource, String, String) - Error geting Addendum Type");
				}
			} finally {
				try {
					if (rsAdd != null)
						rsAdd.close();

				} catch (Exception e) {
					logger.warn(
							"Status(String, String, String, String, String, String, String, String, DataSource, String, String) - exception ignored",
							e);
				}
				try {
					if (stmtAdd != null) {
						stmtAdd.close();
					}
				} catch (Exception e) {
					logger.warn(
							"Status(String, String, String, String, String, String, String, String, DataSource, String, String) - exception ignored",
							e);
				}
				try {
					if (connAdd != null) {
						connAdd.close();
					}
				} catch (Exception e) {
					logger.warn(
							"Status(String, String, String, String, String, String, String, String, DataSource, String, String) - exception ignored",
							e);

				}

			}

		}
		if (Type.equals("prj_act")) {
			Id = Activitylibrary_Id;
		}

		if (Type.equals("jobdashboard_resM")
				|| Type.equals("jobdashboard_resL")
				|| Type.equals("jobdashboard_resF")
				|| Type.equals("jobdashboard_resT")) {
			Id = resource_Id;
		}

		/* customer labor rates */
		if (Type.equals("AM") || Type.equals("AM_EDIT") || Type.equals("PM")) {
			Id = Activitylibrary_Id;
		}

		/* customer labor rates */

		try {
			if (status.equals("I")
					&& (Type.equals("Appendix") || addType.equals("Addendum"))) {
				try {
					mySqlConn = MySqlConnection.getMySqlConnection(mySqlUrl);
				} catch (Exception e) {
					logger.error(
							"Status(String, String, String, String, String, String, String, String, DataSource, String, String)",
							e);

					if (logger.isDebugEnabled()) {
						logger.debug("Status(String, String, String, String, String, String, String, String, DataSource, String, String) - Error in establishing connection to mySQL.");
					}
				}
			}

			if (mySqlConn == null && status.equals("I")
					&& (Type.equals("Appendix") || addType.equals("Addendum"))) {
				retVal = 9600;
			} else {
				try {
					if (logger.isDebugEnabled()) {
						logger.debug("Status(String, String, String, String, String, String, String, String, DataSource, String, String) -  Id ::: "
								+ Id);
					}
					if (logger.isDebugEnabled()) {
						logger.debug("Status(String, String, String, String, String, String, String, String, DataSource, String, String) -  Type ::: "
								+ Type);
					}
					if (logger.isDebugEnabled()) {
						logger.debug("Status(String, String, String, String, String, String, String, String, DataSource, String, String) -  status ::: "
								+ status);
					}
					if (logger.isDebugEnabled()) {
						logger.debug("Status(String, String, String, String, String, String, String, String, DataSource, String, String) -  firstjobsuccess ::: "
								+ firstjobsuccess);
					}
					if (logger.isDebugEnabled()) {
						logger.debug("Status(String, String, String, String, String, String, String, String, DataSource, String, String) -  userId ::: "
								+ userId);
					}
					conn = ds.getConnection();
					conn.setAutoCommit(false);
					cstmt = conn
							.prepareCall("{?=call dbo.cc_status_change( ? , ? , ? ,? , ?,?) }");
					cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
					cstmt.setInt(2, Integer.parseInt(Id));
					cstmt.setString(3, Type);
					cstmt.setString(4, status);
					cstmt.setString(5, "");
					cstmt.setString(6, firstjobsuccess);
					cstmt.setString(7, userId);
					cstmt.execute();
					val = cstmt.getInt(1);
					// SP called for setting accepted POs status to Complete
					// when job is completed
					if (status.equals("F")) {
						cstmt2 = conn
								.prepareCall("{?=call dbo.set_accepted_po_to_complete( ? ,?, ?) }");
						cstmt2.registerOutParameter(1, java.sql.Types.INTEGER);
						cstmt2.setInt(2, Integer.parseInt(Job_Id));
						cstmt2.setInt(3, -1);
						cstmt2.setInt(4, Integer.parseInt(userId));
						cstmt2.execute();
						val2 = cstmt2.getInt(1);
					}
				} catch (SQLException e) {
					logger.error(
							"Status(String, String, String, String, String, String, String, String, DataSource, String, String)",
							e);

					if (logger.isDebugEnabled()) {
						logger.debug("Status(String, String, String, String, String, String, String, String, DataSource, String, String) - Exception caught in changing status"
								+ e);
					}
					logger.error(
							"Status(String, String, String, String, String, String, String, String, DataSource, String, String)",
							e);
				}
				/* start */

				try {
					if (val == 0 && status.equals("I")
							&& Type.equals("Appendix")) {
						stmt = conn.createStatement();
						String sql = "select * from func_mysql_msa_appendix_01("
								+ Appendix_Id + "," + MSA_Id + ")";
						rs = stmt.executeQuery(sql);

						if (rs.next()) {
							msaName = rs.getString("lo_ot_name");
							appendixName = rs.getString("lx_pr_title");
							endCustomer = rs.getString("lx_pr_end_customer");
							app_type = rs.getInt("lx_pr_type");
							bdm_name = rs.getString("bdm_name");
							prm_name = rs.getString("prmanager_name");
							approved_Date = rs.getString("lx_pr_date_approved");
						}

						if (msaName != null && !msaName.equals(""))
							msaName = msaName.replaceAll("\'", "\'\'");

						if (appendixName != null && !appendixName.equals(""))
							appendixName = appendixName
									.replaceAll("\'", "\'\'");

						if (endCustomer != null && !endCustomer.equals(""))
							endCustomer = endCustomer.replaceAll("\'", "\'\'");

						if (bdm_name != null && !bdm_name.equals(""))
							bdm_name = bdm_name.replaceAll("\'", "\'\'");

						if (prm_name != null && !prm_name.equals(""))
							prm_name = prm_name.replaceAll("\'", "\'\'");

						// Retrieve default job for the Appendix from Ilex.
						stmtDefault = conn.createStatement();
						String sqldefault = "select distinct lm_js_id,lm_js_pr_id,lm_js_title from func_mysql_copy_defaultjob_and_addendum_activity_01 ("
								+ Appendix_Id + ") where lm_js_type='Default'";
						rsDefault = stmtDefault.executeQuery(sqldefault);

						if (rsDefault.next()) {
							job_id = rsDefault.getInt("lm_js_id");
							job_title = rsDefault.getString("lm_js_title");
						}
						// Retrieve activities for Default Job from Ilex.
						String sqlactivity = "select lm_at_id,lm_js_id,lm_at_title from  func_mysql_copy_defaultjob_and_addendum_activity_01 ("
								+ Appendix_Id + ") where lm_js_type='Default'";
						rsDefault = stmtDefault.executeQuery(sqlactivity);

						mySQL = mySqlConn.createStatement();
						mySqlDefault = mySqlConn.createStatement();

						sql1 = "select * from il_core_customer_references where il_ci_lp_mm_id = "
								+ MSA_Id;
						mySqlRs = mySQL.executeQuery(sql1);

						mySqlConn.setAutoCommit(false);
						if (mySqlRs.next()) {
							sql1 = "select * from il_core_project_references where  il_ci_lx_pr_id = "
									+ Appendix_Id;
							mySqlRs = mySQL.executeQuery(sql1);
							if (mySqlRs.next()) {
								mySqlVal = 9614;
							} else {
								sql1 = "insert into il_core_project_references(il_ci_lx_pr_id,il_ci_lp_mm_id,il_ci_lx_pr_title,il_ci_lx_pr_end_customer,il_ci_lx_pr_type,il_ci_bdm,il_ci_project_manager,il_ci_lx_pd_date_approved) "
										+ "values("
										+ Appendix_Id
										+ ", "
										+ MSA_Id
										+ ", '"
										+ appendixName
										+ "', '"
										+ endCustomer
										+ "',"
										+ app_type
										+ ",'"
										+ bdm_name
										+ "','"
										+ prm_name
										+ "','"
										+ approved_Date
										+ "')";
								appendixCheck = mySQL.executeUpdate(sql1);

								if (appendixCheck != 1) {
									mySqlVal = 9600;
								} else {

									if (job_title != null
											&& !job_title.equals(""))
										job_title = job_title.replaceAll("\'",
												"\'\'");

									// for default job entry.
									sqlfordefault = "insert into il_default_job_and_addendums (il_da_lm_js_id,il_da_ci_lx_pr_id,il_da_lm_js_title)"
											+ "values("
											+ job_id
											+ ","
											+ Appendix_Id
											+ ",'"
											+ job_title
											+ "')";
									defaultCheck = mySqlDefault
											.executeUpdate(sqlfordefault);
									if (defaultCheck != 1) {
										mySqlVal = 96001;
									} else {
										// insert default job activities to
										// MySql table.
										while (rsDefault.next()) {
											if (rsDefault.getString("lm_at_id") != null) {
												activityId = rsDefault
														.getInt("lm_at_id");
												acJobId = rsDefault
														.getInt("lm_js_id");
												acTitle = rsDefault
														.getString("lm_at_title");

												if (acTitle != null
														&& !acTitle.equals(""))
													acTitle = acTitle
															.replaceAll("\'",
																	"\'\'");

												mySqlForActivity = "insert into il_default_job_addendum_activities (il_ac_lm_at_id,il_ac_da_lm_js_id,il_ac_lm_at_title)"
														+ "values("
														+ activityId
														+ ","
														+ acJobId
														+ ",'"
														+ acTitle + "')";
												actCheck = mySqlDefault
														.executeUpdate(mySqlForActivity);
											} else {
												actCheck = 1;
											}
											if (actCheck != 1) {
												mySqlVal = 96002;
												break;
											}
										}
									}
								}
							}
						} else {
							sql1 = "insert into il_core_customer_references(il_ci_lp_mm_id,il_ci_lo_ot_name)"
									+ "values("
									+ MSA_Id
									+ ", '"
									+ msaName
									+ "')";
							msaCheck = mySQL.executeUpdate(sql1);

							if (msaCheck != 1) {
								mySqlVal = 9600;
							} else {
								sql1 = "select * from il_core_project_references where  il_ci_lx_pr_id = "
										+ Appendix_Id;
								mySqlRs = mySQL.executeQuery(sql1);
								if (mySqlRs.next()) {
									mySqlVal = 9614;
								} else {

									sql1 = "insert into il_core_project_references(il_ci_lx_pr_id,il_ci_lp_mm_id,il_ci_lx_pr_title,il_ci_lx_pr_end_customer,il_ci_lx_pr_type,il_ci_bdm,il_ci_project_manager,il_ci_lx_pd_date_approved) "
											+ "values("
											+ Appendix_Id
											+ ", "
											+ MSA_Id
											+ ", '"
											+ appendixName
											+ "', '"
											+ endCustomer
											+ "',"
											+ app_type
											+ ",'"
											+ bdm_name
											+ "','"
											+ prm_name
											+ "','"
											+ approved_Date + "')";
									appendixCheck = mySQL.executeUpdate(sql1);

									if (appendixCheck != 1) {
										mySqlVal = 9600;
									} else {

										// for default job entry.
										sqlfordefault = "insert into il_default_job_and_addendums (il_da_lm_js_id,il_da_ci_lx_pr_id,il_da_lm_js_title)"
												+ "values("
												+ job_id
												+ ","
												+ Appendix_Id
												+ ",'"
												+ job_title + "')";
										defaultCheck = mySqlDefault
												.executeUpdate(sqlfordefault);
										if (defaultCheck != 1) {
											mySqlVal = 96001;
										} else {
											// for activity
											while (rsDefault.next()) {
												activityId = rsDefault
														.getInt("lm_at_id");
												acJobId = rsDefault
														.getInt("lm_js_id");
												acTitle = rsDefault
														.getString("lm_at_title");

												mySqlForActivity = "insert into il_default_job_addendum_activities (il_ac_lm_at_id,il_ac_da_lm_js_id,il_ac_lm_at_title)"
														+ "values("
														+ activityId
														+ ","
														+ acJobId
														+ ",'"
														+ acTitle + "')";
												actCheck = mySqlDefault
														.executeUpdate(mySqlForActivity);
												if (actCheck != 1) {
													mySqlVal = 96002;
													break;
												}
											}
										}
									}
								}
							}
						}

						if (mySqlVal == 0 || mySqlVal == -9011) {
							mySqlConn.commit();
						} else {
							mySqlConn.rollback();
						}

					} else { // for Addendum Check it.....
						if (val == 0 && status.equals("I")
								&& Type.equals("prj_job")) {
							stmt = conn.createStatement();
							String sqlCheckAddendum = "select lm_js_type,lm_js_pr_id from lm_job where lm_js_id="
									+ Job_Id;
							rs = stmt.executeQuery(sqlCheckAddendum);
							if (rs.next()) {
								if (rs.getString("lm_js_type").equals(
										"Addendum")) {
									String appendixId = rs
											.getString("lm_js_pr_id");
									sql1 = "select * from il_core_project_references where  il_ci_lx_pr_id = "
											+ appendixId;
									mySQL = mySqlConn.createStatement();
									mySqlRs = mySQL.executeQuery(sql1);
									if (!mySqlRs.next()) {
										mySqlVal = 9614;
									} else {
										mySqlRs = null;
										sql1 = "select * from il_default_job_and_addendums where il_da_lm_js_id = "
												+ Job_Id;
										mySqlRs = mySQL.executeQuery(sql1);
										if (mySqlRs.next()) {
											mySqlVal = 96004;
										} else {
											mySqlConn.setAutoCommit(false);
											mySqlVal = 0;
											// Start Copy a Addendum job into
											// default job addendum table.

											mySqlDefault = mySqlConn
													.createStatement();
											stmtDefault = conn
													.createStatement();
											String sqldefault = "select distinct lm_js_id,lm_js_pr_id,lm_js_title from  func_mysql_copy_defaultjob_and_addendum_activity_01 ("
													+ rs.getString("lm_js_pr_id")
													+ ") where lm_js_type='Addendum' and lm_js_id="
													+ Job_Id;
											rsDefault = stmtDefault
													.executeQuery(sqldefault);
											if (rsDefault.next()) {
												job_id = rsDefault
														.getInt("lm_js_id");
												job_title = rsDefault
														.getString("lm_js_title");
											}
											String sqlactivity = "select lm_at_id,lm_js_id,lm_at_title from  func_mysql_copy_defaultjob_and_addendum_activity_01 ("
													+ rs.getString("lm_js_pr_id")
													+ ") where lm_js_type='Addendum' and lm_js_id="
													+ Job_Id;
											rsDefault = stmtDefault
													.executeQuery(sqlactivity);

											// for default job //Addendum job
											// entry.
											sqlfordefault = "insert into il_default_job_and_addendums (il_da_lm_js_id,il_da_ci_lx_pr_id,il_da_lm_js_title)"
													+ "values("
													+ job_id
													+ ","
													+ rs.getString("lm_js_pr_id")
													+ ",'" + job_title + "')";
											defaultCheck = mySqlDefault
													.executeUpdate(sqlfordefault);
											if (defaultCheck != 1) {
												mySqlVal = 96004;
											} else {
												// for activity insert
												while (rsDefault.next()) {
													if (rsDefault
															.getString("lm_at_id") != null) {
														activityId = rsDefault
																.getInt("lm_at_id");
														acJobId = rsDefault
																.getInt("lm_js_id");
														acTitle = (rsDefault
																.getString("lm_at_title"))
																.replace("'",
																		"''");

														mySqlForActivity = "insert into il_default_job_addendum_activities (il_ac_lm_at_id,il_ac_da_lm_js_id,il_ac_lm_at_title)"
																+ "values("
																+ activityId
																+ ","
																+ acJobId
																+ ",'"
																+ acTitle
																+ "')";
														actCheck = mySqlDefault
																.executeUpdate(mySqlForActivity);
													} else
														actCheck = 1;

													if (actCheck != 1) {
														mySqlVal = 96002;
														break;
													}
												}
											}
											/*
											 * To rollback the insert of table
											 * il_default_job_and_addendums if
											 * activities are not saved for any
											 * reason.
											 */
											if (mySqlVal == 0)
												mySqlConn.commit();
											else
												mySqlConn.rollback();
										}
									}
								} else {
								}
							}
						}
					}

					retVal = val;
					if (val2 == 0 && val == 0
							&& (mySqlVal == 0 || mySqlVal == -9011)) {
						conn.commit();
					} else {
						conn.rollback();
						if (val2 == 0 && val == 0
								&& (mySqlVal != 0 && mySqlVal != -9011)) {
							retVal = mySqlVal;
						}
					}

				} catch (Exception sqle) {
					logger.error(
							"Status(String, String, String, String, String, String, String, String, DataSource, String, String)",
							sqle);

					if (logger.isDebugEnabled()) {
						logger.debug("Status(String, String, String, String, String, String, String, String, DataSource, String, String) - Exception caught in making entry to MySql tables"
								+ sqle);
					}
					logger.error(
							"Status(String, String, String, String, String, String, String, String, DataSource, String, String)",
							sqle);
				}
			}
		} catch (Exception e) {
			logger.error(
					"Status(String, String, String, String, String, String, String, String, DataSource, String, String)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("Status(String, String, String, String, String, String, String, String, DataSource, String, String) - Exception caught in Creating Connection to MySQL");
			}
		}
		/* end */
		finally {
			try {
				if (rs != null) {
					rs.close();
				}

				if (mySqlRs != null) {
					mySqlRs.close();
				}
				if (rsDefault != null) {
					rsDefault.close();
				}

			} catch (SQLException sql) {
				logger.error(
						"Status(String, String, String, String, String, String, String, String, DataSource, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("Status(String, String, String, String, String, String, String, String, DataSource, String, String) - Exception caught in closing resultset"
							+ sql);
				}
			}

			try {
				if (cstmt != null) {
					cstmt.close();
				}
				if (cstmt2 != null) {
					cstmt2.close();
				}

				if (mySQL != null) {
					mySQL.close();
				}
				if (mySqlDefault != null) {
					mySqlDefault.close();
				}

				if (stmt != null) {
					stmt.close();
				}
				if (stmtDefault != null) {
					stmtDefault.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"Status(String, String, String, String, String, String, String, String, DataSource, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("Status(String, String, String, String, String, String, String, String, DataSource, String, String) - Exception caught in closing statements"
							+ sql);
				}
			}

			try {
				if (conn != null) {
					conn.close();
				}

				if (mySqlConn != null) {
					mySqlConn.close();
				}
			} catch (SQLException sql) {
				logger.error(
						"Status(String, String, String, String, String, String, String, String, DataSource, String, String)",
						sql);

				if (logger.isDebugEnabled()) {
					logger.debug("Status(String, String, String, String, String, String, String, String, DataSource, String, String) - Exception caught in closing connections"
							+ sql);
				}
			}

		}

		return retVal;
	}

}
