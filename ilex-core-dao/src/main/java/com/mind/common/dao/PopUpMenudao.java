package com.mind.common.dao;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PopUpMenudao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(PopUpMenudao.class);

	String [] str_ar_labels = null;
	int [] int_ar_labelslen = null;

	float flt_pixelperfont = 1;


	public void setPixelperFont(float i)
	{
		
		flt_pixelperfont = i;
	}


	public String[] readDatafromFile(String filepath,String[] labelarray)
	{
		
		
		try
		{
			str_ar_labels = new String [labelarray.length];
			int_ar_labelslen = new int [labelarray.length];

			File fileloc = new File(filepath);
			FileInputStream propfile = new FileInputStream(fileloc);
			
			Properties prop = new Properties();
			prop.load(propfile);

			for (int i=0;i<labelarray.length;i++)
				{
			
					str_ar_labels[i]= prop.getProperty(labelarray[i])==null?"":prop.getProperty(labelarray[i]);
					int_ar_labelslen[i] = str_ar_labels[i].length();
				}
	} //end of try
	catch(Exception se)
	{
			logger.error("readDatafromFile(String, String[])", se);

			if (logger.isDebugEnabled()) {
				logger.debug("readDatafromFile(String, String[]) - Error getting values from property file: " + se.toString());
			}

	}
		return str_ar_labels;
		
	}
	
	public int[] getLabelLenArray()
	{
		
		return int_ar_labelslen;
		
	}
	
	public int getlabellength()
	{
		return int_ar_labelslen.length;
	}

	
	

}
