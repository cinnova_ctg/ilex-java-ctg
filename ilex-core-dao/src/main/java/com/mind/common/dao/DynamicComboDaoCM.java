package com.mind.common.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.LabelValue;

public class DynamicComboDaoCM {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DynamicComboDaoCM.class);

	public static ArrayList getFirstcatglist(String type, DataSource ds) {
		ArrayList<LabelValue> list = new ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select lo_ot_name, lo_ot_id  from lo_organization_top where lo_ot_type='"
							+ type
							+ "' and lo_ot_name <> rtrim(lo_ot_id) order by lo_ot_name");
			while (rs.next()) {
				if (!rs.getString("lo_ot_name").equals("NetMedX"))
					list.add(new LabelValue(rs.getString("lo_ot_name"), rs
							.getString("lo_ot_id")));
			}
		} catch (Exception e) {
			logger.error("getFirstcatglist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getFirstcatglist(String, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getFirstcatglist(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getFirstcatglist(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getFirstcatglist(String, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	public static int addFirstLevelCatg(String id, String type, String element,
			String action, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int retval = 0;
		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lo_organization_top_manage_01(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, id);
			cstmt.setString(3, type);
			cstmt.setString(4, element);
			cstmt.setString(5, action);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(
					"addFirstLevelCatg(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("addFirstLevelCatg(String, String, String, String, DataSource) - Error occured during adding"
						+ e);
			}
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}
			}

			catch (Exception e) {
				logger.warn(
						"addFirstLevelCatg(String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
				logger.warn(
						"addFirstLevelCatg(String, String, String, String, DataSource) - exception ignored",
						e);
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				logger.warn(
						"addFirstLevelCatg(String, String, String, String, DataSource) - exception ignored",
						e);
			}

		}
		return retval;
	}

	public static ArrayList getSecondcatglist(String custid, DataSource ds) {
		ArrayList<LabelValue> list = new ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select  lo_om_id, lo_om_division from	lo_organization_main where lo_om_ot_id='"
							+ custid
							+ "' and	lo_om_division<>'NetMedX' order by lo_om_division");

			while (rs.next()) {
				list.add(new LabelValue(rs.getString("lo_om_division"), rs
						.getString("lo_om_id")));
			}
		} catch (Exception e) {
			logger.error("getSecondcatglist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSecondcatglist(String, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (Exception sql) {
				logger.error("getSecondcatglist(String, DataSource)", sql);

				if (logger.isDebugEnabled()) {
					logger.debug("getSecondcatglist(String, DataSource) - Exception caught"
							+ sql);
				}
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getSecondcatglist(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getSecondcatglist(String, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	public ArrayList getThirdcatglist(String id1, DataSource ds) {
		ArrayList<LabelValue> list = new ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		list.add(new LabelValue("---Select---", "0"));

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select iv_sc_name,iv_sc_id  from iv_subcategory where iv_sc_mf_id = '"
							+ id1 + "' order by iv_sc_name");
			while (rs.next()) {
				list.add(new LabelValue(rs.getString("iv_sc_name"), rs
						.getString("iv_sc_id")));
			}
		} catch (Exception e) {
			logger.error("getThirdcatglist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getThirdcatglist(String, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getThirdcatglist(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getThirdcatglist(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getThirdcatglist(String, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	public ArrayList getStatuslist(String mgrtype, DataSource ds) {
		ArrayList<LabelValue> list = new ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql;
			sql = "select *  from cc_status_master where cc_function like '%"
					+ mgrtype + "%' ";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				list.add(new LabelValue(rs.getString("cc_status_discription"),
						rs.getString("cc_status_code")));
			}
		} catch (Exception e) {
			logger.error("getStatuslist(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getStatuslist(String, DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getStatuslist(String, DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getStatuslist(String, DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getStatuslist(String, DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}

	public static List<LabelValue> getWrokCenterList(DataSource ds) {
		List<LabelValue> list = new ArrayList<LabelValue>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("SELECT work_center_id, work_center_name FROM work_center");
			while (rs.next()) {

				list.add(new LabelValue(rs.getString("work_center_name"), rs
						.getString("work_center_id")));
			}
		} catch (Exception e) {
			logger.error("getWrokCenterList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getWrokCenterList(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"getWrokCenterList(DataSource) - exception ignored",
						sql);
			}

			try {
				stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"getWrokCenterList(DataSource) - exception ignored",
						sql);
			}

			try {
				conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"getWrokCenterList(DataSource) - exception ignored",
						sql);
			}

		}
		return list;
	}
}
