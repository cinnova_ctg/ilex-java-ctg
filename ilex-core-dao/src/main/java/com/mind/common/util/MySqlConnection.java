package com.mind.common.util;

import java.sql.Connection;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.IlexConstants;
import com.mind.fw.core.dao.util.DBUtil;

public class MySqlConnection {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MySqlConnection.class);

	public static Connection getMySqlConnection(String mySqlUrl) throws Exception{
        Connection mySqlConn=null;
		try {
			DataSource ds = DBUtil.getDataSource(IlexConstants.CONTINGENT_DS_NAME);
			mySqlConn = ds.getConnection();
		}catch(Exception e){
			logger.error("getMySqlConnection(String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMySqlConnection(String) - Error in establishing connection to mySQL.");
			}
		}
		return mySqlConn;
	}

}

