package com.mind.docm.connection;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.docm.constants.DocumentConstants;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class DbConnectionFileSys.
 */
public class DbConnectionFileSys {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DbConnectionFileSys.class);

	// Use Case
	/**
	 * @name getConnection
	 * @purpose This method is used to create the connection for DocMFileSysdb
	 *          database.
	 * @steps 1. craete the object of drivermanager class. 2. get the jdbcodbc
	 *        driver. 3. register the driver.
	 * @param none
	 * @returnDescription Returns the connection object.
	 * @throws SQLException
	 */
	public static synchronized Connection getConnection() throws SQLException {
		Connection result = null;
		result = getDataSource().getConnection();
		return result;
	}

	public static synchronized DataSource getDataSource() throws SQLException {
		DataSource ds = DBUtil.getDataSource(DocumentConstants.DOCM_FILE_SYSDB);
		return ds;
	}
}
