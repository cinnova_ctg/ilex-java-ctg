package com.mind.docm.connection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.docm.constants.DocumentConstants;

/**
 * The Class DatabaseUtils.
 */
public class DatabaseUtils {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DatabaseUtils.class);

	/**
	 * @name getRecord
	 * @purpose This method is used to get record in query using a specific
	 *          database.
	 * @steps 1. create the connection object for the given database name. 2.
	 *        create the statement object. 3. get the resultset object using the
	 *        executeQuery method.
	 * @param query
	 * @return useDatabase
	 * @returnDescription The returned resultset object.
	 */
	public static ResultSet getRecord(String query, Statement st) {
		ResultSet rs = null;
		try {
			rs = st.executeQuery(query);
		} catch (Exception e) {
			logger.error("getRecord(String, Statement)", e);
		}
		return rs;
	}

	/**
	 * Gets the connection.
	 * 
	 * @param useDatabase
	 *            the use database
	 * 
	 * @return the connection
	 */
	public static Connection getConnection(String useDatabase) {
		Connection conn = null;
		try {
			if (useDatabase.equals(DocumentConstants.DOCM_FILE_SYSDB))
				conn = DbConnectionFileSys.getConnection();
			else if (useDatabase.equals(DocumentConstants.DOCM_SYSDB))
				conn = DbConnectionDocSys.getConnection();
		} catch (SQLException e) {
			logger.error("getConnection(String)", e);
		}

		return conn;
	}

	public static DataSource getDataSource(String useDatabase) {
		DataSource ds = null;
		try {
			if (useDatabase.equals(DocumentConstants.DOCM_FILE_SYSDB))
				ds = DbConnectionFileSys.getDataSource();
			else if (useDatabase.equals(DocumentConstants.DOCM_SYSDB))
				ds = DbConnectionDocSys.getDataSource();
		} catch (SQLException e) {
			logger.error("getConnection(String)", e);
		}

		return ds;
	}

	// Use Case
	/**
	 * @name updateRecord
	 * @purpose This method is used to update record in database.
	 * @steps 1. create the connection object for the given database name. 2.
	 *        create the statement object. 3. call the executeUpdate method for
	 *        the query.
	 * @param query
	 * @return useDatabase
	 * @returnDescription No of Rows updated.
	 */
	public static int updateRecord(String query, Statement st)
			throws SQLException {
		if (logger.isDebugEnabled()) {
			logger.debug("updateRecord(String, Statement) - SQL UPDATE "
					+ query);
		}
		int num = 0;
		try {
			num = st.executeUpdate(query);
			st.close();
		} catch (SQLException e) {
			logger.error("updateRecord(String, Statement)", e);
		}
		return num;
	}
}
