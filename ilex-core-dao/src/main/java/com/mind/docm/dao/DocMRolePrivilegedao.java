package com.mind.docm.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.mind.common.DynamicTabularChkBox;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class DocMRolePrivilegedao.
 */
public class DocMRolePrivilegedao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DocMRolePrivilegedao.class);

	/** The functiongroup. */
	private Collection functiongroup = null;

	/** The rolename. */
	private Collection rolename = null;

	/**
	 * This method return Organization Type Name for an Organization Type.
	 * 
	 * @param org_type
	 *            String. the value of organization type
	 * @param ds
	 *            Object of DataSource
	 * @return String This method returns string, Organization Type Name.
	 */

	public static String getOrganizationTypeName(String org_type)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String org_name = null;
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = conn.createStatement();
			String sql = "select dm_lo_organization_type_name from dm_lo_organization_type where dm_lo_organization_type ='"
					+ org_type + "'";
			if (logger.isDebugEnabled()) {
				logger.debug("getOrganizationTypeName(String) - " + sql);
			}
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				org_name = rs.getString("dm_lo_organization_type_name");
			}
		}

		catch (Exception e) {
			logger.error("getOrganizationTypeName(String)", e);
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return org_name;
	}

	/**
	 * This method return Organization Discipline Name for an Organization
	 * Discipline Id.
	 * 
	 * @param org_discipline_id
	 *            String. the value of organization discipline id
	 * @param ds
	 *            Object of DataSource
	 * @return String This method returns string, Organization Discipline Name.
	 */

	public static String getOrganizationDisciplineName(String org_discipline_id)
			throws Exception {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String org_discipline_name = null;
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = conn.createStatement();

			String sql = "select dm_lo_od_name from dm_lo_organization_discipline where dm_lo_od_id = "
					+ org_discipline_id;
			if (logger.isDebugEnabled()) {
				logger.debug("getOrganizationDisciplineName(String) - " + sql);
			}
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				org_discipline_name = rs.getString("dm_lo_od_name");
			}

		}

		catch (Exception e) {
			logger.error("getOrganizationDisciplineName(String)", e);
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return org_discipline_name;
	}

	/**
	 * This method return ArrayList of all Function Group using for dropdown
	 * list of function group.
	 * 
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, arraylist of all
	 *         function group.
	 */

	public static java.util.Collection getFunctionGroup() {

		ArrayList functiongroup = new java.util.ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		functiongroup.add(new com.mind.common.LabelValue("---Select---", "0"));

		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select dm_cc_fu_group_name from dm_cc_function where dm_cc_fu_is_external <> 'Ilex' group by dm_cc_fu_group_name order by dm_cc_fu_group_name");
			while (rs.next()) {
				functiongroup.add(new com.mind.common.LabelValue(rs
						.getString("dm_cc_fu_group_name"), rs
						.getString("dm_cc_fu_group_name")));
			}
		} catch (Exception e) {
			logger.error("getFunctionGroup()", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return functiongroup;
	}

	/**
	 * This method return ArrayList of Role Name for an Organization Discipline
	 * Id, using for dropdown list of role name.
	 * 
	 * @param org_discipline_id
	 *            String. the value of organization discipline id
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, arraylist of role name.
	 */

	public static java.util.Collection getRoleName(String org_discipline_id) {

		ArrayList rolename = new java.util.ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		rolename.add(new com.mind.common.LabelValue("---Select---", "-1"));

		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select dm_lo_ro_id, dm_lo_ro_role_desc from dm_lo_role where dm_lo_ro_od_id = "
							+ org_discipline_id
							+ " order by dm_lo_ro_role_desc");
			while (rs.next()) {
				rolename.add(new com.mind.common.LabelValue(rs
						.getString("dm_lo_ro_role_desc"), rs
						.getString("dm_lo_ro_id")));
			}
		} catch (Exception e) {
			logger.error("getRoleName(String)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return rolename;
	}

	/**
	 * This method return collection of Function Type for a Function Group.
	 * 
	 * @param cc_fu_group_name
	 *            String. the value of function group name
	 * @param ds
	 *            Object of DataSource
	 * @return ArrayList This method returns ArrayList, arraylist of function
	 *         type.
	 */

	public static java.util.ArrayList getFunctionType(String cc_fu_group_name) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList UMList = new ArrayList();
		DynamicTabularChkBox umfn = null;
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			cstmt = conn.prepareCall("{?=call dm_view_function_type(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, cc_fu_group_name);
			rs = cstmt.executeQuery();
			while (rs.next()) {
				umfn = new DynamicTabularChkBox();
				if (rs.getString("c1") != null)
					umfn.setC1(rs.getString("c1"));
				else
					umfn.setC1("");
				if (rs.getString("c2") != null)
					umfn.setC2(rs.getString("c2"));
				else
					umfn.setC2("");
				if (rs.getString("c2id") != null)
					umfn.setC2id(rs.getString("c2id"));
				else
					umfn.setC2id("");
				if (rs.getString("c3") != null)
					umfn.setC3(rs.getString("c3"));
				else
					umfn.setC3("");
				if (rs.getString("c3id") != null)
					umfn.setC3id(rs.getString("c3id"));
				else
					umfn.setC3id("");
				if (rs.getString("c4") != null)
					umfn.setC4(rs.getString("c4"));
				else
					umfn.setC4("");
				if (rs.getString("c4id") != null)
					umfn.setC4id(rs.getString("c4id"));
				else
					umfn.setC4id("");
				if (rs.getString("c5") != null)
					umfn.setC5(rs.getString("c5"));
				else
					umfn.setC5("");
				if (rs.getString("c5id") != null)
					umfn.setC5id(rs.getString("c5id"));
				else
					umfn.setC5id("");
				if (rs.getString("c6") != null)
					umfn.setC6(rs.getString("c6"));
				else
					umfn.setC6("");
				if (rs.getString("c6id") != null)
					umfn.setC6id(rs.getString("c6id"));
				else
					umfn.setC6id("");
				if (rs.getString("c7") != null)
					umfn.setC7(rs.getString("c7"));
				else
					umfn.setC7("");
				if (rs.getString("c7id") != null)
					umfn.setC7id(rs.getString("c7id"));
				else
					umfn.setC7id("");
				if (rs.getString("c8") != null)
					umfn.setC8(rs.getString("c8"));
				else
					umfn.setC8("");
				if (rs.getString("c8id") != null)
					umfn.setC8id(rs.getString("c8id"));
				else
					umfn.setC8id("");
				UMList.add(umfn);
			}
		} catch (Exception e) {
			logger.error("getFunctionType(String)", e);
		}

		finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}

		return UMList;
	}

	/**
	 * This method return int, distinct function name for a Function Group.
	 * 
	 * @param cc_fu_group_name
	 *            String. the value of function group name
	 * @param ds
	 *            Object of DataSource
	 * @return int This method returns int, value of distinct function name.
	 */

	public static int getNumColumns(String cc_fu_group_name) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int numcols = 0;
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = conn.createStatement();

			String sql = "select count(distinct dm_cc_fu_name) as numcols from dm_cc_function where dm_cc_fu_group_name ='"
					+ cc_fu_group_name + "'";
			if (logger.isDebugEnabled()) {
				logger.debug("getNumColumns(String) - " + sql);
			}
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				numcols = rs.getInt("numcols");
			}
		}

		catch (Exception e) {
			logger.error("getNumColumns(String)", e);
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return numcols;
	}

	/**
	 * This method return int, for checking success and failure.
	 * 
	 * @param cc_fu_group_name
	 *            String. the value of function group name
	 * @param lo_ro_id
	 *            String. the value of role id
	 * @param indexvalue
	 *            String. the value of selectd function types
	 * @param ds
	 *            Object of DataSource
	 * @return int This method returns int, for checking success and failure.
	 */

	public static int addFunctionType(String cc_fu_group_name, String lo_ro_id,
			String indexvalue) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int retval = -1;

		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			cstmt = conn
					.prepareCall("{?=call dm_function_type_manage_01(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, cc_fu_group_name);
			cstmt.setString(3, lo_ro_id);
			cstmt.setString(4, indexvalue);
			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error("addFunctionType(String, String, String)", e);
		}

		finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return retval;
	}

	/**
	 * This method return int array of function type id from role privilege
	 * table, for a finction group and role name
	 * 
	 * @param cc_fu_group_name
	 *            String. the value of function group name
	 * @param lo_ro_id
	 *            String. the value of role id
	 * @param ds
	 *            Object of DataSource
	 * @return int This method returns int, value of function types from role
	 *         priviledge table.
	 */

	public static int[] getRolePrivilege(String cc_fu_group_name,
			String lo_ro_id) {

		int cnt = 0;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int roleprivilege[] = null;

		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = conn.createStatement();
			String sql = "select count(*) as cnt from dbo.func_dm_role_privilege("
					+ lo_ro_id + ", '" + cc_fu_group_name + "')";
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				cnt = rs.getInt("cnt");
			}
			roleprivilege = new int[cnt];
			sql = "select * from dbo.func_dm_role_privilege(" + lo_ro_id
					+ ", '" + cc_fu_group_name + "')";
			rs = stmt.executeQuery(sql);
			int i = 0;
			while (rs.next()) {
				roleprivilege[i] = rs.getInt("dm_lo_rp_fu_id");
				i++;
			}

		} catch (Exception e) {
			logger.error("getRolePrivilege(String, String)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}

		return roleprivilege;
	}
}
