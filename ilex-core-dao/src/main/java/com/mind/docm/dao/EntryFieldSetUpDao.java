package com.mind.docm.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.mind.bean.docm.SchemaSetupField;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class EntryFieldSetUpDao.
 */
public class EntryFieldSetUpDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(EntryFieldSetUpDao.class);

	// Use Case
	/**
	 * @name entryFieldSetUpList
	 * @purpose This method is used to get the entry field set up list for the
	 *          given libraries.
	 * @steps 1. create the connection object using DatabaseUtils class. 2.
	 *        create the statement object using DatabaseUtils class. 3. Call the
	 *        getRecord method of DatabaseUtils. 4. Create an Object of
	 *        ArrayList. 5. return the ArrayList object which contains the entry
	 *        field setup list.
	 * @param libId
	 * @return ArrayList
	 * @returnDescription Returns the ArrayList Object which contains library.
	 * @throws None
	 */
	public ArrayList entryFieldSetUpList(String libId) {
		ArrayList aList = new ArrayList();
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet result = null;
		try {
			st = conn.createStatement();
			String query = "select a.dm_lib_md_fl_id,b.dm_md_fl_nm,c.dm_tgrp_desc,a.dm_lib_md_entry_fl, "
					+ " a.dm_lib_md_unq_across_rev_fl,a.dm_lib_md_unq_across_lib_fl,a.dm_lib_md_compul_fl,b.dm_md_fl_desc from "
					+ " dm_library_metadata_setup a join dm_metadata_fields_list b on "
					+ " a.dm_lib_md_fl_id=b.dm_md_fl_id AND dm_lib_md_lib_id="
					+ Integer.parseInt(libId)
					+ " join dm_taxonomy_group c on b.dm_md_fl_tgrp_id= c.dm_tgrp_id";
			result = DatabaseUtils.getRecord(query, st);
			while (result.next()) {
				SchemaSetupField entryField = new SchemaSetupField();
				entryField.setEntryFieldId(result.getString(1));
				entryField.setEntryFieldName(result.getString(2));
				entryField.setTaxonomyGroup(result.getString(3));
				entryField.setEntryField(result.getString(4));
				entryField.setUniqueAcrossRevision(result.getString(5));
				entryField.setUniqueAcrossLibrary(result.getString(6));
				entryField.setCompulsoryField(result.getString(7));
				entryField.setDescription(result.getString(8));
				aList.add(entryField);
			}
		} catch (SQLException e) {
			logger.error("entryFieldSetUpList(String)", e);
		} finally {
			DBUtil.close(result, st);
			DBUtil.close(conn);
		}
		return aList;
	}
}