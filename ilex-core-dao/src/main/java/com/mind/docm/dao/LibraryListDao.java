package com.mind.docm.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.mind.bean.docm.Library;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class LibraryListDao.
 */
public class LibraryListDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(LibraryListDao.class);

	// Use Case
	/**
	 * @name libraryMasterList
	 * @purpose This method is used to get the list of libraries for left tree
	 *          menu.
	 * @steps 1. create the connection object using DatabaseUtils class. 2.
	 *        create the statement object using DatabaseUtils class. 3. Call the
	 *        getRecord method of DatabaseUtils. 4. Create an Object of
	 *        ArrayList. 5. return the ArrayList object.
	 * @param libId
	 * @return ArrayList
	 * @returnDescription Returns the ArrayList Object which contains library.
	 * @throws None
	 */
	public ArrayList libraryMasterList() {
		ArrayList aList = new ArrayList();
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet result = null;
		try {
			st = conn.createStatement();
			String query = "select dm_lib_id,dm_lib_desc from dm_library_master order by dm_lib_desc";
			result = DatabaseUtils.getRecord(query, st);
			while (result.next()) {
				Library newLibrary = new Library();
				newLibrary.setLibraryId(result.getString(1));
				newLibrary.setLibraryName(result.getString(2));
				if (result.getString(2).length() > 23)
					newLibrary.setLibraryChar(result.getString(2).substring(0,
							23)
							+ "...");
				else
					newLibrary.setLibraryChar(result.getString(2));
				aList.add(newLibrary);
			}
		} catch (SQLException e) {
			logger.error("libraryMasterList()", e);
		} finally {
			DBUtil.close(result, st);
			DBUtil.close(conn);
		}
		return aList;
	}
}
