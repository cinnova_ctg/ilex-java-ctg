package com.mind.docm.dao;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.mind.bean.docm.Document;
import com.mind.bean.docm.DocumentAddDTO;
import com.mind.bean.docm.DynamicComboDocM;
import com.mind.bean.docm.LabelValue;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.docm.utils.DocumentUtility;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;

/**
 * The Class DocumentAddDao.
 */
public class DocumentAddDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DocumentAddDao.class);
	private static Statement pStmt;

	/**
	 * Gets the all library name.
	 * 
	 * @param chgLibrary
	 *            the chg library
	 * 
	 * @return the all library name
	 */
	public static ArrayList getAllLibraryName(boolean chgLibrary) {
		ArrayList al = new ArrayList();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = "";
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		if (chgLibrary == true)
			sql = "select dm_lib_id,dm_lib_desc from dm_library_master where doc_identifier_fld_id is null order by dm_lib_id";
		else
			sql = "select dm_lib_id,dm_lib_desc from dm_library_master order by dm_lib_id";
		al.add(new LabelValue("--Select--", "0"));
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				al.add(new LabelValue(rs.getString(2), rs.getString(1)));
			}
		} catch (SQLException e) {
			logger.error("getAllLibraryName(boolean)", e);
		} finally {
			DBUtil.close(rs, pst);
			DBUtil.close(conn);
		}
		return al;
	}

	/**
	 * Gets the all meta data type.
	 * 
	 * @param metaDataTypeId
	 *            the meta data type id
	 * 
	 * @return the all meta data type
	 */
	public static ArrayList getAllMetaDataType(int metaDataTypeId) {
		ArrayList al = new ArrayList();
		ArrayList inner = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String length = "";
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		String sql = "select * from dm_metadata_type where dm_md_type_id="
				+ metaDataTypeId;
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				inner = new ArrayList();
				if (rs.getString(1).equals("1") || rs.getString(1).equals("2")
						|| rs.getString(1).equals("6")
						|| rs.getString(1).equals("8")
						|| rs.getString(1).equals("7")
						|| rs.getString(1).equals("11")) {
					inner.add("textbox");
					inner.add(rs.getString(3).substring(0,
							rs.getString(3).indexOf("(")));
					if (rs.getString(3).contains("(")
							&& rs.getString(3).contains(",")) {
						length = rs.getString(3).substring(
								rs.getString(3).indexOf("(") + 1,
								rs.getString(3).indexOf(","));
						inner.add(length);
					} else if (rs.getString(3).contains("(")
							&& rs.getString(3).contains(")")) {
						length = rs.getString(3).substring(
								rs.getString(3).indexOf("(") + 1,
								rs.getString(3).indexOf(")"));
						inner.add(length);
					}
				} else if (rs.getString(1).equals("3")) {
					inner.add("checkbox");
					inner.add("bit");
					inner.add("");
				} else if (rs.getString(1).equals("4")) {
					inner.add("datetime");
					inner.add("date");
					inner.add("");
				} else if (rs.getString(1).equals("12")) {
					inner.add("combo");
					inner.add("select");
					inner.add("");
				}
				al.add(inner);
			}
		} catch (SQLException e) {
			logger.error("getAllMetaDataType(int)", e);
		} finally {
			DBUtil.close(rs, pst);
			DBUtil.close(conn);
		}
		return al;
	}

	/**
	 * Gets the all taxonomy group.
	 * 
	 * @return the all taxonomy group
	 */
	public static ArrayList getAllTaxonomyGroup() {
		ArrayList al = new ArrayList();
		ArrayList inner = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		String sql = "select * from dm_taxonomy_group order by dm_tgrp_id";
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				inner = new ArrayList();
				inner.add(rs.getString(1));
				inner.add(rs.getString(2));
				al.add(inner);
			}
		} catch (SQLException e) {
			logger.error("getAllTaxonomyGroup()", e);
		} finally {
			DBUtil.close(rs, pst);
			DBUtil.close(conn);
		}
		return al;
	}

	/**
	 * Gets the all meta data fields.
	 * 
	 * @param bean
	 *            the bean
	 * @param request
	 *            the request
	 * @param libId
	 *            the lib id
	 * 
	 * @return the all meta data fields
	 */
	public static ArrayList getAllMetaDataFields(DocumentAddDTO bean,
			Map<String, Object> attrmap, Map<String, String> paramMap, int libId) {
		ArrayList al = new ArrayList();
		ArrayList inner = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		String sql = "select * from dbo.dm_document_metadata_Add_dtl_view where dm_lib_md_lib_id="
				+ libId + " ORDER BY DM_MD_FL_TGRP_ID";

		logger.trace(sql);
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				inner = new ArrayList();
				inner.add(rs.getString(1));
				inner.add(rs.getString(2));
				inner.add(rs.getString(3));
				inner.add(rs.getString(4));
				inner.add(rs.getString(5));
				if (bean.getSource().equals("Ilex")) {
					inner.add(setValueToAddDoc(bean, attrmap, paramMap,
							rs.getString(4), rs.getString(6)));
				} else if (bean.getSource().equals("DocM")) {
					inner.add(rs.getString(6));
				}
				inner.add(rs.getString(7));
				inner.add(rs.getString(8));
				inner.add(getAllMetaDataType(rs.getInt(9)));
				inner.add(rs.getString(10));
				inner.add(rs.getString(11));
				inner.add(rs.getString(12));
				inner.add(rs.getString(13));
				inner.add(selectCombo(rs.getString(2)));
				al.add(inner);
			}
		} catch (SQLException e) {
			logger.error(
					"getAllMetaDataFields(DocumentAddBean, HttpServletRequest, int)",
					e);
		} finally {
			DBUtil.close(rs, pst);
			DBUtil.close(conn);
		}
		return al;
	}

	/**
	 * Update document master.
	 * 
	 * @param libId
	 *            the lib id
	 * @param docId
	 *            the doc id
	 */
	public static void updateDocumentMaster(String libId, String docId) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		String query = "update dm_document_master set dm_doc_lib_id="
				+ Integer.parseInt(libId) + " where dm_doc_id="
				+ Integer.parseInt(docId);
		try {
			st = conn.createStatement();
			st.execute(query);
		} catch (SQLException e) {
			logger.error("updateDocumentMaster(String, String)", e);
		} finally {
			DBUtil.close(st);
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * Select combo.
	 * 
	 * @param fieldid
	 *            the fieldid
	 * 
	 * @return the array list
	 */
	public static ArrayList selectCombo(String fieldid) {
		ArrayList al = new ArrayList();
		ArrayList inner = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);

		String sql = "select * from dm_metadata_field_domain where field_id=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, fieldid);
			rs = pst.executeQuery();
			while (rs.next()) {
				inner = new ArrayList();
				inner.add(rs.getString(1));
				inner.add(rs.getString(2));
				al.add(inner);
			}
		} catch (SQLException e) {
			logger.error("selectCombo(String)", e);
		} finally {
			DBUtil.close(rs, pst);
			DBUtil.close(conn);
		}
		return al;
	}

	/**
	 * Gets the link doc id.
	 * 
	 * @param docId
	 *            the doc id
	 * 
	 * @return the link doc id
	 */
	public static ArrayList getLinkDocId(String docId) {
		ArrayList al = new ArrayList();
		ArrayList inner = null;
		Statement st = null;
		ResultSet rs = null;
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		String query = "select distinct isnull(title,''),dm_doc_id from dm_document_version where dm_doc_id in ("
				+ " select distinct LinkToOtherDocId  from dm_document_version a where dm_doc_id="
				+ docId
				+ ") union"
				+ " select title,dm_doc_id from dm_document_version where LinkToOtherDocId="
				+ docId;

		try {
			st = conn.createStatement();
			rs = st.executeQuery(query);
			while (rs.next()) {
				inner = new ArrayList();
				inner.add(rs.getString(1));
				inner.add(rs.getString(2));
				al.add(inner);
			}
		} catch (SQLException e) {
			logger.error("getLinkDocId(String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return al;
	}

	/**
	 * Property util.
	 * 
	 * @param doc
	 *            the doc
	 * @param bean
	 *            the bean
	 */
	public static void propertyUtil(Document doc, Object bean) {

		Class cls = bean.getClass();
		Field[] field = cls.getDeclaredFields();
		String name = null;
		com.mind.bean.docm.Field[] fields = doc.getMetaData().getFields();
		for (int i = 0; i < field.length; i++) {
			try {
				name = field[i].getName();
				for (int j = 0; j < fields.length; j++) {
					if (name.equalsIgnoreCase(fields[j].getName())) {
						PropertyUtils.setSimpleProperty(bean, name,
								fields[j].getValue());
						logger.trace(name + " value " + fields[j].getValue());
					}
				}
			} catch (Exception e) {
				logger.error("propertyUtil(Document, Object)", e);

				logger.trace("" + e);
			}
		}
	}

	/**
	 * Populate data.
	 * 
	 * @param list
	 *            the list
	 * @param request
	 *            the request
	 */
	public void populateData(ArrayList list, Map<String, Object> map,
			Map<String, String> paramMap) {
		ArrayList taxonomy_group = DocumentAddDao.getAllTaxonomyGroup();
		map.put("taxonomy_group", taxonomy_group);
		for (int i = 0; i < list.size(); i++) {
			String key = (String) ((ArrayList) list.get(i)).get(3);
			if (paramMap.get(key) != null) {
				((ArrayList) list.get(i)).set(5, paramMap.get(key));
			}
		}
		map.put("metadata_field_list", list);
	}

	/**
	 * Sets the menu for entity.
	 * 
	 * @param bean
	 *            the bean
	 * @param request
	 *            the request
	 */
	public void setMenuForEntity(DocumentAddDTO bean,
			Map<String, Object> attrmap, Map<String, String> paramMap) {

		String entityType = (attrmap.get("entityType") == null ? paramMap.get(
				"entityType").toString() : (String) attrmap.get("entityType"));
		String entityId = (attrmap.get("entityId") == null ? paramMap.get(
				"entityId").toString() : (String) attrmap.get("entityId"));

		bean.setType(entityType);

		if (bean.getType().equalsIgnoreCase("MSA")) {
			bean.setMsaId(entityId);
		} else if (bean.getType().equalsIgnoreCase("Appendix")) {
			bean.setAppendixId(entityId);
			;
		} else if (bean.getType().equalsIgnoreCase("Addendum")) {
			bean.setJobId(entityId);
		} else if (bean.getType().equalsIgnoreCase("PO")) {
			bean.setPoId(entityId);
		} else if (bean.getType().equalsIgnoreCase("WO")) {
			bean.setWoId(entityId);
		}

		else if (isLibrary(bean)) {
			String libName = (attrmap.get("linkLibName") == null ? paramMap
					.get("linkLibName").toString() : (String) attrmap
					.get("linkLibName"));

			bean.setLinkLibNameFromIlex(libName.trim());
			bean.setType(bean.getLinkLibNameFromIlex());

			bean.setMsaId((bean.getType().equalsIgnoreCase("msa") ? entityId
					: ""));
			bean.setAppendixId((bean.getType().equalsIgnoreCase("Appendix") ? entityId
					: ""));
			bean.setJobId((bean.getType().equalsIgnoreCase("Addendum")
					|| bean.getType().equalsIgnoreCase("Job")
					|| isLibrary(bean) ? entityId : ""));

			bean.setPoId(bean.getType().equalsIgnoreCase("PO") ? entityId : "");

		} else if (bean.getType().equalsIgnoreCase("Engineering Support")) {
			String libName = (attrmap.get("linkLibName") == null ? paramMap
					.get("linkLibName").toString() : (String) attrmap
					.get("linkLibName"));
			bean.setLinkLibNameFromIlex(libName.trim());
			bean.setType(bean.getLinkLibNameFromIlex());
			bean.setAppendixId((bean.getType().equalsIgnoreCase("Appendix") ? entityId
					: ""));
		} else if (bean.getType().equalsIgnoreCase("PO")) {
			String libName = (attrmap.get("linkLibName") == null ? paramMap
					.get("linkLibName").toString() : (String) attrmap
					.get("linkLibName"));
			bean.setLinkLibNameFromIlex(libName.trim());
			bean.setType(bean.getLinkLibNameFromIlex());
			bean.setPoId((bean.getType().equalsIgnoreCase("PO") ? entityId : ""));
		}

	}

	/**
	 * Other doc link.
	 * 
	 * @param entityFieldId
	 *            the entity field id
	 * @param entityIdValue
	 *            the entity id value
	 * 
	 * @return the string
	 */
	public static String otherDocLink(String entityFieldId, int entityIdValue) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;

		ResultSet rs = null;
		String linkedDoc = "";
		String query = "select dm_doc_id  from dm_document_version where "
				+ entityFieldId + "=" + entityIdValue
				+ " and status='Approved' ";
		try {
			st = conn.createStatement();
			rs = DatabaseUtils.getRecord(query, st);
			if (rs.next()) {
				linkedDoc = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("otherDocLink(String, int)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return linkedDoc;
	}

	/**
	 * Populate text.
	 * 
	 * @param type
	 *            the type
	 * @param request
	 *            the request
	 * @param libId
	 *            the lib id
	 * @param bean
	 *            the bean
	 */
	public void populateText(String type, Map<String, Object> attrmap,
			Map<String, String> paramMap, Map<String, Object> map,
			String libId, DocumentAddDTO bean) {
		DynamicComboDocM dynamicCombo = new DynamicComboDocM();
		ArrayList taxonomy_group = DocumentAddDao.getAllTaxonomyGroup();
		map.put("taxonomy_group", taxonomy_group);
		if (type.equals("metaDataList")) {
			ArrayList libName = DocumentAddDao.getAllLibraryName(true);
			dynamicCombo.setLibrarylist(libName);
			map.put("libNameCombo", dynamicCombo);
		}
		if (type.equals("viewDocument")) {
			ArrayList metaDataFieldList = DocumentAddDao.getAllMetaDataFields(
					bean, attrmap, paramMap, Integer.parseInt(libId));
			map.put("metadata_field_list", metaDataFieldList);
		}
		if (type.equals("modifyDocument")) {
			ArrayList libNameList = null;
			String libName = DocumentUtility.getLibraryName(libId);
			map.put("libName", libName);
			if (!bean.getSource().equals("Ilex"))
				libNameList = getAllLibraryName(true);
			else if (bean.getSource().equals("Ilex"))
				libNameList = getAllLibraryName(false);
			dynamicCombo.setLibrarylist(libNameList);
			map.put("libNameCombo", dynamicCombo);
			ArrayList metaDataFieldList = DocumentAddDao.getAllMetaDataFields(
					bean, attrmap, paramMap, Integer.parseInt(libId));
			map.put("metadata_field_list", metaDataFieldList);
		}
		if (type.equals("moveDocument")) {
			if (libId != null && !libId.trim().equalsIgnoreCase("")) {
				map.put("libraryID", paramMap.get("libraryID"));
				String libName = DocumentUtility.getLibraryName(libId);

				map.put("libId", libId);
				map.put("libName", libName);
				ArrayList libNameList = DocumentAddDao.getAllLibraryName(true);
				// libNameList.add(new com.mind.docm.LabelValue(libName,request
				// .getParameter("libId")));
				dynamicCombo.setLibrarylist(libNameList);
				map.put("libNameCombo", dynamicCombo);

				ArrayList metaDataFieldList = DocumentAddDao
						.getAllMetaDataFields(bean, attrmap, paramMap,
								Integer.parseInt(libId));
				map.put("metadata_field_list", metaDataFieldList);
			}
		}
	}

	/**
	 * Sets the value to add doc.
	 * 
	 * @param bean
	 *            the bean
	 * @param request
	 *            the request
	 * @param name
	 *            the name
	 * @param defaultvalue
	 *            the defaultvalue
	 * 
	 * @return the string
	 */
	public static String setValueToAddDoc(DocumentAddDTO bean,
			Map<String, Object> attrmap, Map<String, String> paramMap,
			String name, String defaultvalue) {
		String msaId = (attrmap.get("msaId") == null ? "" : (String) attrmap
				.get("msaId"));
		String appendixId = (attrmap.get("appendixId") == null ? ""
				: (String) attrmap.get("appendixId"));
		String jobId = (attrmap.get("jobId") == null ? "" : (String) attrmap
				.get("jobId"));
		String entityType = (attrmap.get("entityType") == null ? paramMap
				.get("entityType") : (String) attrmap.get("entityType"));
		String libName = (attrmap.get("linkLibName") == null ? (paramMap
				.get("linkLibName") == null ? "" : paramMap.get("linkLibName"))
				: (String) attrmap.get("linkLibName"));

		if (name.equalsIgnoreCase("MSA_ID"))
			defaultvalue = bean.getMsaId();
		if (name.equalsIgnoreCase("MSA_NAME"))
			defaultvalue = (bean.getMSA_Name());
		if (name.equalsIgnoreCase("APPENDIX_ID"))
			defaultvalue = bean.getAppendixId();
		if (name.equalsIgnoreCase("APPENDIX_NAME"))
			defaultvalue = (bean.getAppendix_Name());
		if (name.equalsIgnoreCase("JOB_ID"))
			defaultvalue = bean.getJobId();
		if (name.equalsIgnoreCase("JOB_NAME"))
			defaultvalue = bean.getJob_Name();
		if (name.equalsIgnoreCase("PO_ID"))
			defaultvalue = bean.getPoId();
		if (name.equalsIgnoreCase("WO_ID"))
			defaultvalue = bean.getWO_ID();
		if (name.equalsIgnoreCase("Partner_Name"))
			defaultvalue = bean.getPartner_Name();
		if (name.equalsIgnoreCase("LinkToOtherDocId")) {
			if (entityType != null && entityType.equals("Supplement")
					&& !libName.equalsIgnoreCase("Job")) {
				if (!msaId.equals("")) {
					defaultvalue = otherDocLink("MSA_ID",
							Integer.parseInt(msaId));
				} else if (!appendixId.equals("")) {
					defaultvalue = otherDocLink("Appendix_ID",
							Integer.parseInt(appendixId));
				} else if (!jobId.equals("")) {
					defaultvalue = otherDocLink("JOB_ID",
							Integer.parseInt(jobId));
				}
			}
		}
		return defaultvalue;
	}

	/**
	 * Gets the partner name.
	 * 
	 * @param ds
	 *            the ds
	 * @param powoid
	 *            the powoid
	 * 
	 * @return the partner name
	 */
	public static String getPartnerName(DataSource ds, String powoid) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";

		String partnerName = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			sql = "select c.lo_om_division from lm_purchase_order a left join cp_partner b"
					+ "  on a.lm_po_partner_id=b.cp_partner_id left join lo_organization_main c on"
					+ " b.cp_partner_om_id=c.lo_om_id"
					+ " where lm_po_id="
					+ powoid;
			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerName(DataSource, String) - " + sql);
			}
			rs = stmt.executeQuery(sql);

			if (rs.next())
				partnerName = rs.getString(1);
		}

		catch (Exception e) {
			logger.error("getPartnerName(DataSource, String)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return partnerName;
	}

	/**
	 * Checks if is library.
	 * 
	 * @param bean
	 *            the bean
	 * 
	 * @return true, if is library
	 */
	public boolean isLibrary(DocumentAddDTO bean) {
		boolean library = false;
		library = (bean
				.getType()
				.equalsIgnoreCase(
						DatabaseUtilityDao
								.getKeyValue("ilex.docm.libraryname.appendixsupportingdocuments"))
				|| bean.getType()
						.equalsIgnoreCase(
								DatabaseUtilityDao
										.getKeyValue("ilex.docm.libraryname.msasupportingdocuments")) || bean
				.getType()
				.equalsIgnoreCase(
						DatabaseUtilityDao
								.getKeyValue("ilex.docm.libraryname.deliverables")));
		return library;
	}
}
