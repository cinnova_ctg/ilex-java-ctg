package com.mind.docm.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class DocumentHistoryDao.
 */
public class DocumentHistoryDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DocumentHistoryDao.class);

	/**
	 * Gets the library id and name.
	 * 
	 * @param documentId
	 *            the document id
	 * 
	 * @return the library id and name
	 */
	public static String[] getLibraryIdAndName(String documentId) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet rs = null;
		String lib[] = new String[2];
		lib[0] = null;
		lib[1] = null;
		String query = "select dm_lib_id,isnull(dm_lib_desc,'') from dm_library_master where dm_lib_id  in ("
				+ "select dm_doc_lib_id from dm_document_master where dm_doc_id="
				+ documentId + ")";
		logger.trace(query);

		try {
			st = conn.createStatement();
			rs = DatabaseUtils.getRecord(query, st);
			if (rs.next()) {
				lib[0] = rs.getString(1);
				lib[1] = rs.getString(2);
			}
		} catch (SQLException e) {
			logger.error("getLibraryIdAndName(String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return lib;
	}

	/**
	 * Gets the document history.
	 * 
	 * @param documentId
	 *            the document id
	 * 
	 * @return the document history
	 */
	public static ArrayList getDocumentHistory(String documentId) {
		ArrayList rowList = new ArrayList();
		Connection conn = null;
		ResultSet rs = null;
		Statement st = null;
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			st = conn.createStatement();
			ArrayList colList = new ArrayList();

			String query = "select dm_doc_id,dm_doc_file_id,MIME,FileName, Status";
			TreeMap fieldNameToDescriptionMap = DocumentMasterDao
					.getFieldNameToDescriptionMap();

			query += ", DocumentVersion '"
					+ new String(
							(String) fieldNameToDescriptionMap
									.get("documentversion")).replaceAll("'",
							"''") + "'";
			query += ", Title '"
					+ new String(
							(String) fieldNameToDescriptionMap.get("title"))
							.replaceAll("'", "''") + "'";
			query += ", Status '"
					+ new String(
							(String) fieldNameToDescriptionMap.get("status"))
							.replaceAll("'", "''") + "'";
			query += ", comment '"
					+ new String(
							(String) fieldNameToDescriptionMap.get("comment"))
							.replaceAll("'", "''") + "'";
			query += ", convert(varchar(10),AddDate,101) '"
					+ new String(
							(String) fieldNameToDescriptionMap.get("adddate"))
							.replaceAll("'", "''") + "'";
			query += ", AddUser '"
					+ new String(
							(String) fieldNameToDescriptionMap.get("adduser"))
							.replaceAll("'", "''") + "'";

			query += ", convert(varchar(10),updatedocumentdate,101) '"
					+ new String(
							(String) fieldNameToDescriptionMap
									.get("updatedocumentdate")).replaceAll("'",
							"''") + "'";
			query += ", UpdateDocumentUser '"
					+ new String(
							(String) fieldNameToDescriptionMap
									.get("updatedocumentuser")).replaceAll("'",
							"''") + "'";
			query += " from  dm_document_version where dm_doc_id="
					+ documentId
					+ " order by cast(substring(DocumentVersion,0,charindex('.',DocumentVersion)) as numeric) ,"
					+ " cast(substring(DocumentVersion,charindex('.',DocumentVersion)+1,len(DocumentVersion)) as numeric)";

			logger.trace(query);

			rs = DatabaseUtils.getRecord(query, st);
			ResultSetMetaData rsm = rs.getMetaData();
			int countCol = rsm.getColumnCount();
			for (int i = 1; i <= countCol; i++) {
				colList.add(rsm.getColumnName(i));
			}
			rowList.add(colList);
			while (rs.next()) {
				colList = new ArrayList();
				for (int i = 1; i <= countCol; i++) {
					colList.add(rs.getString(i));
				}
				rowList.add(colList);
			}
		} catch (SQLException e) {
			logger.error("getDocumentHistory(String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return rowList;
	}

}
