package com.mind.docm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.docm.DocumentType;
import com.mind.common.IlexConstants;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class TransferDocDao.
 */
public class TransferDocDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TransferDocDao.class);

	/** The type. */
	private String type = null;

	// For MSA/Appendix/Addendum

	/** The image path. */
	private String imagePath = null;

	/** The font path. */
	private String fontPath = null;

	/** The html image path. */
	private String htmlImagePath = null;

	/** The orginial type. */
	private String orginialType = null;

	// For POWO
	/** The image path1. */
	private String imagePath1 = null;

	/** The WO nw config img. */
	private String WONwConfigImg = null;

	/** The WO nw config imgheader. */
	private String WONwConfigImgheader = null;

	/** The ic chrysler. */
	private String icChrysler = null;

	/** The ic snap on. */
	private String icSnapOn = null;

	/** The ic compu com. */
	private String icCompuCom = null;

	/** The image bullet path. */
	private String imageBulletPath = null;

	// Exception Cases
	/** The count. */
	int count = 0;

	/** The Doc m. */
	int DocM = 0;

	/** The buffer exception. */
	int bufferException = 0;

	/** The count of document transfered. */
	int countOfDocumentTransfered = 0;

	/** The file not found exception. */
	int fileNotFoundException = 0;

	/** The document uploaded. */
	int documentUploaded = 0;

	/** The xml modified. */
	int xmlModified = 0;

	/** The document superseded. */
	int documentSuperseded = 0;

	// DataSource
	/** The ds. */
	private DataSource ds = null;

	/***
	 * Method :method used to get the query for the documents to be inserted.
	 * 
	 * @param Sstring
	 */

	public synchronized String getSqlQuery(String type) {
		String sql = null;

		if (type.equals("MSA"))
			sql = "select lp_md_id,lp_js_id=0,lp_mm_type = 'MSA',fromType = 'PM',lp_md_uploaded_flag	 = isnull(lp_md_uploaded_flag,'') from lp_msa_detail"
					+ " inner join lp_msa_main on lp_mm_id = lp_md_mm_id"
					+ " where lp_md_status not in ('D','R') and lp_mm_doc_id is null";
		else if (type.equals("Appendix"))
			sql = "select lx_pr_id, lm_js_id,lx_pr_type = 'Appendix', "
					+ " fromType = case	when lx_pr_status in ('I','O','F') then 'PRM'"
					+ " else 'PM' end,"
					+ " lx_pd_uploaded_flag = isnull(lx_pd_uploaded_flag,'')	"
					+ " from lx_appendix_main "
					+ " inner join lx_appendix_detail on lx_pr_id = lx_pd_pr_id "
					+ " left outer join lm_job on lx_pr_id = lm_js_pr_id "
					+ " where lx_pr_status not in ('D','R') and lm_js_type = 'Default' and lx_pr_doc_id is null";
		else if (type.equals("Addendum"))
			sql = "select lx_pr_id,lm_js_id,lx_pr_type = 'Appendix' , fromType = 'PRM',  lx_pd_uploaded_flag = isnull(lx_pd_uploaded_flag,'')	"
					+ " from lx_appendix_main"
					+ " inner join lx_appendix_detail on lx_pr_id = lx_pd_pr_id	"
					+ " left outer join lm_job on lx_pr_id = lm_js_pr_id"
					+ " where lm_js_status not in ('D','R') and lm_js_type = 'Addendum' and lm_js_doc_id is null";
		else if (type.equals("PO"))
			sql = "select lm_po_id,'PO' from lm_purchase_order where lm_po_create_date  > dateadd(mm,-9,getdate()) and lm_po_doc_id is null";
		else if (type.equals("WO"))
			sql = "select lm_po_id,'WO' from lm_purchase_order where lm_po_create_date  > dateadd(mm,-9,getdate()) and lm_po_wo_doc_id is null";
		return sql;

	}

	/***
	 * Method :method used to log the details into database for which document
	 * has been transfered
	 * 
	 * @param Id
	 * @param Type
	 * @param docId
	 */
	public static synchronized void logSuccessMessage(String Id, String Type,
			String docId) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int insert;
		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();
			insert = stmt
					.executeUpdate("insert into report_transfer_doc values ("
							+ Integer.parseInt(Id) + ",'" + Type + "',"
							+ Integer.parseInt(docId) + ",getdate())");
		} catch (Exception e) {
			logger.error("logSuccessMessage(String, String, String)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
	}

	/***
	 * Method :this method returns the list fill of DocumentType Objects
	 * 
	 * @param Type
	 * @return ArrayList
	 * 
	 */

	public synchronized ArrayList moveDocumentsList(String type) {
		String sql = getSqlQuery(type);
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			list = getDocument(rs, type);
		} catch (Exception e) {
			logger.error("moveDocumentsList(String)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return list;
	}

	/***
	 * Method : method used to the ArrayList filled with DocumentType objects.
	 * This is used while transfering the documents.
	 */

	public synchronized ArrayList getDocument(ResultSet rs, String Type) {
		DocumentType documenttype = null;
		ArrayList list = new ArrayList();
		try {
			if (Type.equalsIgnoreCase("PO") || Type.equalsIgnoreCase("WO")) {
				while (rs.next()) {
					count++;
					documenttype = new DocumentType();
					documenttype.setId(rs.getString(1));
					documenttype.setType(rs.getString(2));
					list.add(documenttype);
				}
			} else {
				while (rs.next()) {
					count++;
					documenttype = new DocumentType();
					documenttype.setId(rs.getString(1));
					documenttype.setJobId(rs.getString(2));
					documenttype.setType(rs.getString(3));
					documenttype.setFromType(rs.getString(4));
					documenttype.setIsXmlModified(rs.getString(5));
					list.add(documenttype);
				}
			}
		} catch (Exception e) {
			logger.error("getDocument(ResultSet, String)", e);
		}
		return list;
	}

	/***
	 * Method : get the ArrayList filled with cancelled entites from ILEX
	 * 
	 * @param type
	 */

	public synchronized ArrayList getCancelledEntites(String type) {
		String sql = getUpdationQuery(type);
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				DocumentType document = new DocumentType();
				document.setId(rs.getString(1));
				document.setDocId(rs.getString(2));
				document.setType(rs.getString(3));
				list.add(document);
			}
		} catch (Exception e) {
			logger.error("getCancelledEntites(String)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return list;
	}

	/***
	 * Method : get the queries for cancelled entites
	 * 
	 * @param type
	 */

	public synchronized String getUpdationQuery(String type) {
		String sql = "";
		if (type.equalsIgnoreCase("MSA"))
			sql = "select lp_md_id,lp_mm_doc_id,'MSA' as Type from"
					+ " lp_msa_main inner join lp_msa_detail"
					+ " on lp_mm_id = lp_md_mm_id"
					+ " where lp_md_status = 'C' and lp_mm_doc_id is not null";
		else if (type.equalsIgnoreCase("Appendix"))
			sql = "select lx_pr_id,lx_pr_doc_id,'Appendix' as Type from"
					+ " lx_appendix_main where lx_pr_status = 'C' and lx_pr_doc_id is not null";
		else if (type.equalsIgnoreCase("Addendum"))
			sql = "select lm_js_id,lm_js_doc_id,'Job' as Type from "
					+ " lm_job where lm_js_status = 'C' and lm_js_type = 'Addendum' and lm_js_doc_id is not null ";
		return sql;
	}

	/***
	 * Method : performs the updation query in DocM database for cancelled
	 * entites
	 * 
	 * @param Id
	 * @param docId
	 * @param type
	 */

	public synchronized void updating(String Id, String docId, String type) {
		String sql = "update dm_document_version set Status = 'Superseded' "
				+ " where dm_doc_id = ? and DocumentVersion = ? and Type = ? ";
		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		int updateResult;
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, Integer.parseInt(docId));
			pstmt.setString(2, "1.0");
			pstmt.setString(3, type);
			updateResult = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("updating(String, String, String)", e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);

		}
	}

	/**
	 * Sets the document summary.
	 */
	public synchronized void setDocumentSummary() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int insertResult;
		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			pstmt = conn
					.prepareStatement("insert into report_transfer_doc_detail  values (?,?,?,?,?,?,?,?,?,?)");
			pstmt.setString(1, orginialType);
			pstmt.setInt(2, count);
			pstmt.setInt(3, documentUploaded);
			pstmt.setInt(4, xmlModified);
			pstmt.setInt(5, fileNotFoundException);
			pstmt.setInt(6, countOfDocumentTransfered);
			pstmt.setInt(7, documentSuperseded);
			pstmt.setInt(8, bufferException);
			pstmt.setInt(9, DocM);
			pstmt.setTimestamp(10,
					new java.sql.Timestamp(new java.util.Date().getTime()));
			insertResult = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("setDocumentSummary()", e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * Checks if is default job or addendum.
	 * 
	 * @param id
	 *            the id
	 * 
	 * @return the string
	 */
	public static String isDefaultJobOrAddendum(String id) {
		return isDefaultJobOrAddendum(id,
				DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME));
	}

	public static String isDefaultJobOrAddendum(String id, DataSource ds) {
		Connection conn = null;
		Statement pstmt = null;
		ResultSet rs = null;
		String returnVal = null;
		try {
			conn = ds.getConnection();
			String sql = "select lm_js_type from lm_job where lm_js_id = '"
					+ id + "'";
			pstmt = conn.createStatement();
			rs = pstmt.executeQuery(sql);
			while (rs.next()) {
				if (rs.getString("lm_js_type") != null
						&& !rs.getString("lm_js_type").trim()
								.equalsIgnoreCase("")
						&& rs.getString("lm_js_type").trim()
								.equalsIgnoreCase("Default")) {
					returnVal = "Default";
				} else if (rs.getString("lm_js_type").trim()
						.equalsIgnoreCase("Addendum"))
					returnVal = "Addendum";
			}
		} catch (Exception e) {
			logger.error("isDefaultJobOrAddendum(String)", e);
		} finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}
		return returnVal;
	}

}
