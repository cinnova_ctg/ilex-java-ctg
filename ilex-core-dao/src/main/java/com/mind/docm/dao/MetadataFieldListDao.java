package com.mind.docm.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.mind.bean.docm.Field;
import com.mind.bean.docm.MetaData;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class MetadataFieldListDao.
 */
public class MetadataFieldListDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(MetadataFieldListDao.class);

	// Use Case
	/**
	 * @name metadataFieldsList
	 * @purpose This method is used to get the list of metadata fields.
	 * @steps 1. create the connection object using DatabaseUtils class. 2.
	 *        create the statement object using DatabaseUtils class. 3. Call the
	 *        getRecord method of DatabaseUtils. 4. Create an Object of
	 *        ArrayList. 5. return the ArrayList object.
	 * @param libId
	 * @return ArrayList
	 * @returnDescription Returns the ArrayList Object which contains all the
	 *                    metadata fields List.
	 * @throws None
	 */
	public MetaData metadataFieldsList() {
		ArrayList aList = new ArrayList();
		MetaData newMdata = new MetaData();
		Connection conn = null;
		Statement st = null;
		conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);

		ResultSet result = null;
		try {
			st = conn.createStatement();
			String query = "select dm_md_fl_id,(select dm_tgrp_desc from dm_taxonomy_group "
					+ " where dm_tgrp_id=a.dm_md_fl_tgrp_id)"
					+ " as taxonomy_group,dm_md_fl_nm,dm_md_fl_desc,"
					+ "(select dm_md_type_desc from dm_metadata_type where dm_md_type_id=a.dm_md_fl_type_id) "
					+ " as field_type,dm_md_fl_dval from dm_metadata_fields_list a "
					+ "order by taxonomy_group";
			result = DatabaseUtils.getRecord(query, st);
			while (result.next()) {
				Field field = new Field();
				field.setField_id(result.getString(1));
				field.setField_name(result.getString(3));
				field.setField_type(result.getString(5));
				field.setDescription(result.getString(4));
				field.setTax_group(result.getString(2));
				field.setDefault_value(result.getString(6));
				aList.add(field);
			}
			newMdata.setFields(com.mind.docm.util.DocumentUtility
					.fieldArray(aList));
		} catch (SQLException e) {
			logger.error("metadataFieldsList()", e);
		} finally {
			DBUtil.close(result, st);
			DBUtil.close(conn);

		}
		return newMdata;
	}

}
