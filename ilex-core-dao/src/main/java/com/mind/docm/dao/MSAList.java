//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : MSAList.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.docm.dao;

import java.io.Serializable;


public class MSAList implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MSA[] msaArray;

	public MSA[] getMsaArray() {
		return msaArray;
	}

	public void setMsaArray(MSA[] msaArray) {
		this.msaArray = msaArray;
	}
}
