//
//
//
//@ Project : DocM - Document Management System - Ilex
//@ File Name : EntityDescriptionDao.java
//@ Date : 12-June-2008
//@ Author : Vishal Kapoor
//
//
package com.mind.docm.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.docm.DocMSearchBean;
import com.mind.common.IlexConstants;
import com.mind.common.dao.Menu;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;

import com.mind.docm.dao.EntityManager;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.docm.exceptions.LibraryIdNotFoundException;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class DocMSeacrhDao.
 */
public class DocMSeacrhDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DocMSeacrhDao.class);

	/**
	 * @name getColumns
	 * @purpose This method is used to the get the metadata fields list to be
	 *          used on the DocumentSearch Page
	 * @steps 1. User requests Search the Documents in the DocM. 2. An internal
	 *        call is made to this method to get the fieldType combo box for
	 *        Search capability.
	 * @param HttpServletRequest
	 * @param DocMSearchForm
	 * @return 
	 * @return
	 * @returnDescription This just set the list in the request scope.
	 */
	public Map<String, Object> getColumns() {
		logger
				.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getColumns)");
		Statement stmt = null;
		ResultSet rs = null;
		List outerList = null;
		Map<String,Object>map=new HashMap<String, Object>();
		// Get the Connection to the Database
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		try {
			String sql = "select dmfl.*,dtg.dm_tgrp_desc from dbo.dm_metadata_fields_list dmfl left outer join dbo.dm_taxonomy_group dtg"
					+ " \n on dmfl.dm_md_fl_tgrp_id = dtg.dm_tgrp_id";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			outerList = new ArrayList();
			while (rs.next()) {
				List innerList = new ArrayList();
				innerList.add(rs.getString("dm_md_fl_id")); // 0
				innerList.add(rs.getString("dm_md_fl_desc")); // 1
				innerList.add(rs.getString("dm_md_fl_nm")); // 2
				innerList.add(rs.getString("dm_md_fl_desc")); // 3
				innerList.add(rs.getString("dm_md_fl_type_id")); // 4
				innerList.add(rs.getString("dm_md_fl_dval")); // 5
				innerList.add(rs.getString("dm_tgrp_desc")); // 6
				innerList.add(rs.getString("dm_md_fl_nm")); // 7
				innerList.add(rs.getString("dm_md_fl_tgrp_id")); // 8
				innerList.add(rs.getString("dm_md_fl_type_id")); // 9
				innerList.add(rs.getString("dm_md_fl_id") + "~"
						+ rs.getString("dm_md_fl_type_id")); // 10
				outerList.add(innerList);
			}
			// Set the list in the request Attribute to be made available in JSP
			map.put("filterColumnList", outerList);
		} catch (SQLException sqle) {
			logger
					.error("getColumns(HttpServletRequest, DocMSearchBean)",
							sqle);
			logger.error(sqle.getMessage());
		} catch (Exception sqle) {
			logger
					.error("getColumns(HttpServletRequest, DocMSearchBean)",
							sqle);
			logger.error(sqle.getMessage());
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn); // Close database
								// conneciton object
			logger.trace("End :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getColumns)");
		}
		return map;
	}

	/**
	 * @name getlibraryList
	 * @purpose This method is used to the get the library list for the Document
	 *          Search Page.
	 * @steps 1. User requests Search the Documents in the DocM. 2. An internal
	 *        call is made to this method to get the library list combo box for
	 *        Search capability.
	 * @param
	 * @param
	 * @return ArrayList
	 * @returnDescription ArrayList containing the libraryList.
	 */

	public static ArrayList getlibraryList() {
		logger
				.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getlibraryList)");
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String status = null;
		String sql = null;

		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select dm_lib_id,dm_lib_desc from dm_library_master order by dm_lib_desc,dm_lib_id");
			// Any is added to give the user the capability to search the
			// document in any library.
			list
					.add(new com.mind.bean.docm.LabelValue("--Any Library--",
							""));
			while (rs.next()) {
				list.add(new com.mind.bean.docm.LabelValue(rs
						.getString("dm_lib_desc"), rs.getString("dm_lib_id")));
			}
		} catch (Exception e) {
			logger.error("getlibraryList()", e);
			logger.error(e.getMessage());
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);// Close database
								// conneciton object
		}
		logger
				.trace("End :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getlibraryList)");
		return list;
	}

	/**
	 * @name getQueryForDocuments
	 * @purpose This method is used to the generate the query top search for
	 *          documents based on user input values in Document Search Page.
	 * @steps 1. User requests Search the Documents in the DocM.
	 * @param String
	 *            [] fieldName
	 * @param String
	 *            [] fieldOperator
	 * @param String
	 *            [] fieldText
	 * @param String
	 *            [] fieldLogic
	 * @param String
	 *            libId
	 * @return String
	 * @returnDescription String containg the query to search for documents.
	 */

	public static String getQueryForDocuments(String[] fieldName,
			String[] fieldOperator, String[] fieldText, String[] fieldLogic,
			String libId, boolean showAllColumns) {
		return getQueryForDocuments(fieldName, fieldOperator, fieldText,
				fieldLogic, libId, showAllColumns, null);
	}

	/**
	 * Gets the query for documents.
	 * 
	 * @param fieldName
	 *            the field name
	 * @param fieldOperator
	 *            the field operator
	 * @param fieldText
	 *            the field text
	 * @param fieldLogic
	 *            the field logic
	 * @param libId
	 *            the lib id
	 * @param showAllColumns
	 *            the show all columns
	 * @param request
	 *            the request
	 * 
	 * @return the query for documents
	 */
	public static String getQueryForDocuments(String[] fieldName,
			String[] fieldOperator, String[] fieldText, String[] fieldLogic,
			String libId, boolean showAllColumns,Map<String, Object> attrmap) {

		logger
				.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getQueryForDocuments)");
		StringBuffer sqlquery = new StringBuffer();
		// Get the Column Names to be appear in the Search Result
		String fieldNames = "";

		if (logger.isDebugEnabled()) {
			logger
					.debug("getQueryForDocuments(String[], String[], String[], String[], String, boolean, HttpServletRequest) - in MPO test for TITLE");
		}
		String fieldNamesForMPO[] = null;
		String title = DocMSeacrhDao.getIdDataTypeIdPair("MPOTitle") + "~11";
		String version = DocMSeacrhDao.getIdDataTypeIdPair("MPOVersion") + "~2";
		boolean isTitle = isExists(title, fieldName);
		boolean isVersion = isExists(version, fieldName);
		if (isTitle && isVersion) {
			fieldNamesForMPO = new String[fieldName.length];
			System.arraycopy(fieldName, 0, fieldNamesForMPO, 0,
					fieldName.length);
			// copyArray(fieldNamesForMPO,fieldName);

		} else if (isTitle) {
			fieldNamesForMPO = new String[fieldName.length + 1];
			System.arraycopy(fieldName, 0, fieldNamesForMPO, 0,
					fieldName.length);
			// copyArray(fieldNamesForMPO,fieldName);
			fieldNamesForMPO[fieldNamesForMPO.length - 1] = DocMSeacrhDao
					.getIdDataTypeIdPair("MPOVersion")
					+ "~2";
		} else if (isVersion) {
			fieldNamesForMPO = new String[fieldName.length + 1];
			System.arraycopy(fieldName, 0, fieldNamesForMPO, 0,
					fieldName.length);
			// copyArray(fieldNamesForMPO,fieldName);
			fieldNamesForMPO[fieldNamesForMPO.length - 1] = DocMSeacrhDao
					.getIdDataTypeIdPair("MPOTitle")
					+ "~11";
		} else {
			fieldNamesForMPO = new String[fieldName.length + 2];
			System.arraycopy(fieldName, 0, fieldNamesForMPO, 0,
					fieldName.length);
			// copyArray(fieldNamesForMPO,fieldName);
			fieldNamesForMPO[fieldNamesForMPO.length - 2] = DocMSeacrhDao
					.getIdDataTypeIdPair("MPOVersion")
					+ "~2";
			fieldNamesForMPO[fieldNamesForMPO.length - 1] = DocMSeacrhDao
					.getIdDataTypeIdPair("MPOTitle")
					+ "~11";

		}

		fieldNames = DocumentMasterDao.getFieldNameToDescriptionQuery(
				fieldNamesForMPO, showAllColumns);

		// Get the length of any array as they all are having equal length to
		// iterate for.
		int length = fieldName.length;

		sqlquery = sqlquery
				.append("select dm_document_version.dm_doc_id,dm_doc_file_id,MIME,FileName,dm_doc_lib_id,status,dm_lib_desc 'Library Name' "
						+ fieldNames
						+ " from dm_document_master "
						+ " inner join dm_library_master on "
						+ " dm_document_master.dm_doc_lib_id  = dm_lib_id"
						+ " inner join dm_document_version on "
						+ " dm_document_master.dm_doc_id = dm_document_version.dm_doc_id ");

		// Iterate through the arrays and fill the String
		for (int i = 0; i < length; i++) {

			// Attach the where clause for the first time in the query
			if (i == 0)
				sqlquery = (libId.equals("")) ? sqlquery.append("where ")
						: sqlquery.append("where ( ");

			sqlquery = sqlquery.append(" ("
					+ getfieldName(fieldName[i],
							getStringFromFieldOperator(fieldOperator[i]))
					+ " "
					+ getStringFromFieldOperator(fieldOperator[i])
					+ " "
					+ getFiledText(fieldOperator[i], fieldText[i].replaceAll(
							"'", "''")) + " )");

			// Check for the first time where we don't have to attach the
			// logical operator in the query
			if (length == 1)
				logger.trace("No Logical operator defined");
			else {
				if (fieldLogic.length != i) {
					sqlquery = sqlquery.append(" " + fieldLogic[i]);
				}
			}
		}

		// Attach the libId condition depending on whether library is selected
		// or not
		sqlquery = (libId.equals("")) ? sqlquery.append(" ") : sqlquery
				.append(" ) and dm_doc_lib_id = " + libId);

		if (attrmap != null) {
			sqlquery = sqlquery.append(checkSupplementType(attrmap));
		}

		sqlquery
				.append(" order by dm_document_version.dm_doc_id desc, "
						+ " cast(substring(DocumentVersion,0,charindex('.',DocumentVersion)) as numeric) ,"
						+ " cast(substring(DocumentVersion,charindex('.',DocumentVersion)+1,len(DocumentVersion)) as numeric)");

		logger.trace(sqlquery.toString());
		logger
				.trace("End :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getQueryForDocuments)");
		// Return the final query
		// System.out.println(sqlquery.toString());
		return sqlquery.toString();
	}

	/**
	 * @name getfiledName
	 * @purpose This method is used as the helping mathod to generate the query
	 *          for search result
	 * @steps 1. User requests Search the Documents in the DocM.
	 * @param String
	 *            fieldId
	 * @return String
	 * @returnDescription Return the field name corresponding to the fieldId.
	 */
	public static String getfieldName(String fieldId, String operator) {
		logger
				.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getfiledName)");
		String fieldName = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String status = null;
		String sql = null;
		String[] field = fieldId.split("~");
		String dateCheck = "";
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = conn.createStatement();
			// Query to get the field name
			rs = stmt
					.executeQuery("select dm_md_fl_nm from dm_metadata_fields_list where dm_md_fl_id = "
							+ field[0]);
			if (rs.next()) {
				if (field[1].trim().equals("4")
						&& operator.trim().equalsIgnoreCase("="))
					dateCheck = rs.getString("dm_md_fl_nm");
				fieldName = (dateCheck.trim().equalsIgnoreCase("")) ? rs
						.getString("dm_md_fl_nm") : "convert(varchar(10),"
						+ dateCheck + ",101)";
			}
		} catch (Exception e) {
			logger.error("getfieldName(String, String)", e);
			logger.error(e.getMessage());
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn); // Close database
								// conneciton object
		}
		logger.trace(fieldName);
		logger
				.trace("End :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getfiledName)");
		return fieldName;
	}

	/**
	 * @name getStringFromFieldOperator
	 * @purpose This method is used as the helping mathod to generate the query
	 *          for search result
	 * @steps 1. User requests Search the Documents in the DocM.
	 * @param String
	 *            operatorId
	 * @return String
	 * @returnDescription Return the operator to be used to generate the query
	 *                    depends on individual condition.
	 */

	public static String getStringFromFieldOperator(String operatorId) {
		logger
				.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getStringFromFieldOperator)");

		String operator = null;
		if (operatorId.trim().equalsIgnoreCase("=")
				|| operatorId.trim().equalsIgnoreCase("is Empty"))
			operator = "=";
		else if (operatorId.trim().equalsIgnoreCase("!="))
			operator = "<>";
		else if (operatorId.trim().equalsIgnoreCase("like"))
			operator = "like";
		else if (operatorId.trim().equalsIgnoreCase("not like"))
			operator = "not like";
		else if (operatorId.trim().equalsIgnoreCase("greaterThan"))
			operator = ">";
		else if (operatorId.trim().equalsIgnoreCase(">"))
			operator = ">";
		else if (operatorId.trim().equalsIgnoreCase("<"))
			operator = "<";
		else if (operatorId.trim().equalsIgnoreCase("lessThan"))
			operator = "<";
		else if (operatorId.trim().equalsIgnoreCase("is null"))
			operator = "is null";
		logger
				.trace("End :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getStringFromFieldOperator)");

		return operator;
	}

	/**
	 * @name getFiledText
	 * @purpose This method is used as the helping mathod to generate the query
	 *          for search result
	 * @steps 1. User requests Search the Documents in the DocM.
	 * @param String
	 *            filedOperator
	 * @param String
	 *            filedtext
	 * @return String
	 * @returnDescription Return the string to be searched for the individual
	 *                    condition in Document Search Page
	 */

	public static String getFiledText(String filedOperator, String filedtext) {
		logger
				.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getFiledText)");
		// Append the % sign for Contains and Not Contains clause
		if (getStringFromFieldOperator(filedOperator).trim().equalsIgnoreCase(
				"like")
				|| getStringFromFieldOperator(filedOperator).trim()
						.equalsIgnoreCase("not like"))
			return "'%" + filedtext + "%'";
		else if (getStringFromFieldOperator(filedOperator).trim()
				.equalsIgnoreCase("is null"))
			return "";
		else
			return "'" + filedtext + "'";
	}

	/**
	 * @name getResultList
	 * @purpose This method is used to get the ArrayList based on the Search
	 *          Condition Specified
	 * @steps 1. User requests Search the Documents in the DocM.
	 * @param String
	 *            query
	 * @return ArrayList
	 * @returnDescription Return the ArrayList containing the Search Result.
	 */

	public static ArrayList getResultList(String query) {
		logger
				.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getResultList)");
		ArrayList documentlist = new java.util.ArrayList();
		ArrayList colList = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			ResultSetMetaData rsm = rs.getMetaData();
			int countCol = rsm.getColumnCount();
			for (int i = 1; i <= countCol; i++) {
				colList.add(rsm.getColumnName(i));
			}
			documentlist.add(colList);
			while (rs.next()) {
				colList = new ArrayList();
				for (int i = 1; i <= countCol; i++) {
					colList.add(rs.getString(i));
				}
				documentlist.add(colList);
			}
		} catch (Exception e) {
			logger.error("getResultList(String)", e);
			logger.error(e.getMessage());
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		logger
				.trace("End :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getResultList)");
		return documentlist;
	}

	/**
	 * @name getResultByUserRights
	 * @purpose This method is used to filter the ResultList based on the user
	 *          rights specific result
	 * @steps 1. User requests Search the Documents in the DocM.
	 * @param ArrayList
	 *            list
	 * @param ArrayList
	 *            libList
	 * @param HttpServletRequest
	 *            request
	 * @return ArrayList
	 * @returnDescription Return the ArrayList containing the Search Result
	 *                    based on user specific rights.
	 */

	public static ArrayList getResultByUserRights(ArrayList list,
			ArrayList libList,Map<String, Object> map) {
		
		logger
				.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getResultByUserRights)");
		ArrayList newList = new ArrayList();
		HashMap libMap = new HashMap();
		newList.add(list.get(0));
		for (int intIndex = 1; intIndex < list.size(); intIndex++) {
			ArrayList documentlist = (ArrayList) list.get(intIndex);
			// Fill the Map that contains the whole inoformation about records
			// in the list
			if (libMap.containsKey(documentlist.get(6).toString())) {
				Long value = (Long) libMap.get(documentlist.get(6).toString());
				long incremented = value.longValue() + 1;
				value = Long.valueOf(incremented);
				libMap.remove(documentlist.get(6).toString());
				libMap.put(documentlist.get(6).toString(), value);
			} else {
				libMap.put(documentlist.get(6).toString(), new Long(1));
			}
			if (libList.contains(documentlist.get(6).toString())) {
				newList.add(list.get(intIndex));
			}
		}
		map.put("libMap", libMap);
		list = newList;
		logger
				.trace("End :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getResultByUserRights)");
		return list;
	}

	/**
	 * @name getUserSpecificLibList
	 * @purpose This method is used to get the list of libraries for which the
	 *          user has right to see document list.
	 * @steps 1. User requests Search the Documents in the DocM.
	 * @param String
	 *            userId
	 * @return ArrayList
	 * @returnDescription Return the ArrayList which the user has rights to see.
	 */

	public static ArrayList getUserSpecificLibList(String userId) {
		logger
				.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getResultByUserRights)");
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String dbName = DatabaseUtilityDao.getKeyValue("DbName");
		try {
			String query = "";
			String fieldName = "";
			if (dbName.trim().equalsIgnoreCase("ilex")) {
				conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME).getConnection();
				stmt = conn.createStatement();
				query = "select cc_fu_name from lo_role_privilege a, lo_poc_rol b, cc_function c "
						+ " where b.lo_pr_pc_id = "
						+ userId
						+ " and a.lo_rp_ro_id = b.lo_pr_ro_id"
						+ " and a.lo_rp_fu_id = c.cc_fu_id"
						+ " and c.cc_fu_name in (select distinct cc_fu_name  from cc_function where cc_fu_group_name = 'Ilex Library' )"
						+ " and c.cc_fu_type = 'Document List'";
				fieldName = "cc_fu_name";
			} else {
				conn = DatabaseUtils
						.getConnection(DocumentConstants.DOCM_SYSDB);
				stmt = conn.createStatement();
				query = "select dm_cc_fu_name from dm_lo_role_privilege a, dm_lo_poc_rol b, dm_cc_function c "
						+ " where b.dm_lo_pr_pc_id = "
						+ userId
						+ " and a.dm_lo_rp_ro_id = b.dm_lo_pr_ro_id"
						+ " and a.dm_lo_rp_fu_id = c.dm_cc_fu_id"
						+ " and c.dm_cc_fu_name in (select distinct dm_cc_fu_name  from dm_cc_function where dm_cc_fu_group_name = 'Ilex Library' )"
						+ " and c.dm_cc_fu_type = 'Document List'";
				fieldName = "dm_cc_fu_name";
			}
			// System.out.println(query);
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				list.add(rs.getString(fieldName));
			}
		} catch (Exception e) {
			logger.error("getUserSpecificLibList(String)", e);
			logger.error(e.getMessage());
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);// Close database
								// conneciton object
		}
		logger
				.trace("End :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getResultByUserRights)");
		return list;
	}

	/**
	 * Gets the title from doc id.
	 * 
	 * @param docId
	 *            the doc id
	 * 
	 * @return the title from doc id
	 */
	public static String getTitleFromDocId(String docId) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st=null;
		 
		ResultSet rs = null;
		String query = "select isnull(title,'') from dm_document_version where dm_doc_id = "
				+ docId;
		// System.out.println(query);
		String title = null;
		try {
			st=conn.createStatement();
			rs = DatabaseUtils.getRecord(query, st);
			if (rs.next()) {
				title = rs.getString(1);
				// System.out.println("libId:: "+title);
			}
		} catch (Exception e) {
			logger.error("getTitleFromDocId(String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return title;
	}

	/**
	 * Gets the id data type id pair.
	 * 
	 * @param type
	 *            the type
	 * 
	 * @return the id data type id pair
	 */
	public static String getIdDataTypeIdPair(String type) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st =null;
		
		ResultSet rs = null;
		String sql = null;
		String id = "";
		if (type.trim().equalsIgnoreCase("MSA"))
			sql = "select dm_md_fl_id from dm_metadata_fields_list where dm_md_fl_nm='MSA_Id'";
		else if (type.trim().equalsIgnoreCase("Appendix"))
			sql = "select dm_md_fl_id from dm_metadata_fields_list where dm_md_fl_nm='Appendix_Id'";
		else if (type.trim().equalsIgnoreCase("Addendum"))
			sql = "select dm_md_fl_id from dm_metadata_fields_list where dm_md_fl_nm='Job_Id'";
		else if (type.trim().equalsIgnoreCase("Job"))
			sql = "select dm_md_fl_id from dm_metadata_fields_list where dm_md_fl_nm='Job_Id'";
		else if (type.trim().equalsIgnoreCase("MPO"))
			sql = "select dm_md_fl_id from dm_metadata_fields_list where dm_md_fl_nm='Status'";
		else if (type.trim().equalsIgnoreCase("MPOTitle"))
			sql = "select dm_md_fl_id from dm_metadata_fields_list where dm_md_fl_nm='Title'";
		else if (type.trim().equalsIgnoreCase("MPOVersion"))
			sql = "select dm_md_fl_id from dm_metadata_fields_list where dm_md_fl_nm='DocumentVersion'";
		else if (type.trim().equalsIgnoreCase("PO"))
			sql = "select dm_md_fl_id from dm_metadata_fields_list where dm_md_fl_nm='PO_ID'";
		try {
			st=conn.createStatement();
			rs = DatabaseUtils.getRecord(sql, st);
			if (rs.next()) {
				id = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("getIdDataTypeIdPair(String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return id;
	}

	/**
	 * Method : this method is used to add the extra condition to check which
	 * entity supplement is this. Logic is : If MSA then Appendix Id should be
	 * null If Appendix then jobId should be null If Job then Po Id should be
	 * null
	 * 
	 * @param request
	 * @return
	 */
	private static String checkSupplementType(Map<String, Object> attrmap) {
		String sql = " ";
		if (attrmap.get("linkLibName").toString().trim()
				.equalsIgnoreCase("MSA")) {
			sql = " and Appendix_Id is null";
		} else if (attrmap.get("linkLibName").toString().trim()
				.equalsIgnoreCase("Appendix")) {
			sql = " and Job_Id is null";
		} else if (attrmap.get("linkLibName").toString().trim()
				.equalsIgnoreCase("Job")
				|| attrmap.get("linkLibName").toString().trim()
						.equalsIgnoreCase("Addendum")) {
			sql = " and PO_Id is null";
		}
		return sql;
	}

	/**
	 * Checks if is exists.
	 * 
	 * @param data
	 *            the data
	 * @param array
	 *            the array
	 * 
	 * @return true, if is exists
	 */
	public static boolean isExists(String data, String[] array) {
		for (int i = 0; i < array.length; i++) {
			if (data.equalsIgnoreCase(array[i]))
				return true;
		}
		return false;
	}

	/**
	 * Copy array.
	 * 
	 * @param array1
	 *            the array1
	 * @param array2
	 *            the array2
	 */
	public static void copyArray(String[] array1, String[] array2) {
		for (int i = 0; i < array2.length; i++) {
			array1[i] = array2[i];
			// System.out.println("array val---"+array1[i]);
		}
	}

	/**
	 * Method : method is used to set the search condition from Ilex when
	 * searching for supplements documents regarding any entity
	 * 
	 * @param request
	 * @param form
	 */

	public static void setSearchConditionForSupplementsFromIlex(
			Map<String, Object> attrmap, DocMSearchBean form) {
		String fieldName = "";
		String fieldText = "";
		String fieldColumnName[] = new String[1];
		String fieldCondition[] = { "=" };
		String fieldColumnText[] = new String[1];

		form.setSelectedMSA(attrmap.get("libId").toString());
		if (attrmap.get("linkLibName").toString().trim()
				.equalsIgnoreCase("MSA")) {
			fieldName = DocMSeacrhDao.getIdDataTypeIdPair("MSA") + "~1";
			fieldText = attrmap.get("msaId").toString();
		} else if (attrmap.get("linkLibName").toString().trim()
				.equalsIgnoreCase("Appendix")) {
			fieldName = DocMSeacrhDao.getIdDataTypeIdPair("Appendix") + "~1";
			fieldText = attrmap.get("appendixId").toString();
		} else if (attrmap.get("linkLibName").toString().trim()
				.equalsIgnoreCase("Addendum")) {
			fieldName = DocMSeacrhDao.getIdDataTypeIdPair("Addendum") + "~1";
			fieldText = attrmap.get("jobId").toString();
		} else if (attrmap.get("linkLibName").toString().trim()
				.equalsIgnoreCase("Job")) {
			fieldName = DocMSeacrhDao.getIdDataTypeIdPair("Job") + "~1";
			fieldText = attrmap.get("jobId").toString();
		}
		fieldColumnName[0] = fieldName;
		fieldColumnText[0] = fieldText;
		form.setSelectedFilterColumns(fieldColumnName);
		form.setFilterCondition(fieldCondition);
		form.setFilterType(fieldColumnText);
		form.setSearchNow("search");
		form.setShowAllColumns("1");
	}

	/**
	 * Method : method is used when the search button is pressed to search for
	 * documents
	 * 
	 * @param request
	 * @param docmsearchform
	 * @param loginId
	 */
	public static Map<String,Object> whenSearchButtonPressed(Map<String, Object> attrmap,Map<String,Object> innermap,
			DocMSearchBean docmsearchform, String loginId) {
		Map<String,Object>map=new HashMap<String, Object>();
		String query = null;
		String[] fieldTypeId = null;
		boolean showAllColumns = false;

		if (docmsearchform.getShowAllColumns() != null
				&& docmsearchform.getShowAllColumns().equals("1")) {
			showAllColumns = true;
		}

		if (docmsearchform.getIsShow() != null
				&& !docmsearchform.getIsShow().trim().equalsIgnoreCase("")
				&& docmsearchform.getIsShow().trim().equalsIgnoreCase("Y")) {
			map.put("isShow", "isShow");;
		}
		if (docmsearchform.getSelectedFilterColumns() != null) {
			int length = docmsearchform.getSelectedFilterColumns().length;
			fieldTypeId = new String[length];
			// FieldType Array should get populated back on the JSP after
			// clicking the Search button.
			for (int i = 0; i < length; i++) {
				String fieldId = docmsearchform.getSelectedFilterColumns()[i]
						.substring(0,
								docmsearchform.getSelectedFilterColumns()[i]
										.indexOf("~"));
				String fieldType = docmsearchform.getSelectedFilterColumns()[i]
						.substring(docmsearchform.getSelectedFilterColumns()[i]
								.indexOf("~") + 1);
				fieldTypeId[i] = fieldType;
			}
			docmsearchform.setFieldTypeId(fieldTypeId);
			if (attrmap.get("searchSupplements") != null) {
				query = getQueryForDocuments(docmsearchform
						.getSelectedFilterColumns(), docmsearchform
						.getFilterCondition(), docmsearchform.getFilterType(),
						docmsearchform.getLogicalCondition(), docmsearchform
								.getSelectedMSA(), showAllColumns, attrmap);
			} else {
				query = getQueryForDocuments(docmsearchform
						.getSelectedFilterColumns(), docmsearchform
						.getFilterCondition(), docmsearchform.getFilterType(),
						docmsearchform.getLogicalCondition(), docmsearchform
								.getSelectedMSA(), showAllColumns);
			}
			map.put("documentList", getResultByUserRights(
					DocMSeacrhDao.getResultList(query), DocMSeacrhDao
					.getUserSpecificLibList(loginId), innermap));
			
			logger.trace(query);
			docmsearchform.setLinkLibNameFromIlex("DocM");
		}
				return map;
	}

	/**
	 * Method :method is used to set the Ilex Menu on DocM Forms
	 * 
	 * @param request
	 * @param docmsearchform
	 * @param loginuserid
	 * @param ds
	 * @return 
	 */

	public static Map<String, Object> setIlexMenu(Map<String, Object> attrmap,
			DocMSearchBean docmsearchform, String loginuserid, DataSource ds) {
		Map<String, Object> map = new HashMap<String, Object>();
		String libName = attrmap.get("linkLibName").toString().trim();
		docmsearchform.setLinkLibNameFromIlex(libName.trim());

		if (logger.isDebugEnabled()) {
			logger
					.debug("setIlexMenu(HttpServletRequest, DocMSearchForm, String, DataSource) - link libName >>> ???? "
							+ libName);
		}
		// request.setAttribute("setLinkLibNameFromIlex",libName.trim());
		if (libName.trim().equalsIgnoreCase("MSA")
				|| libName.trim().equalsIgnoreCase("MSA Supporting Documents")) {
			docmsearchform.setMsaId(attrmap.get("msaId").toString());
			docmsearchform.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					ds, docmsearchform.getMsaId()));
			map.put("MSA_Id", docmsearchform.getMsaId());
			String msaStatus = MSAdao.getMSAStatus(docmsearchform.getMsaId(),
					ds);
			String msaUploadCheck = MSAdao.getMSAUploadCheck(docmsearchform
					.getMsaId(), ds);
			String list = Menu.getStatus("pm_msa", msaStatus.charAt(0),
					docmsearchform.getMsaId(), "", "", "", loginuserid, ds);
			map.put("list", list);
			map.put("status", msaStatus);
			map.put("uploadstatus", msaUploadCheck);

			docmsearchform.setEntityId(docmsearchform.getMsaId());
			docmsearchform.setEntityType(docmsearchform
					.getLinkLibNameFromIlex());
			// docmsearchform.setFromPage(docmsearchform.getFromPage());

		} else if (libName.trim().equalsIgnoreCase("Appendix")
				|| libName.trim().equalsIgnoreCase(
						"Appendix Supporting Documents")) {
			// docmsearchform.setFromPage(request.getAttribute("fromPage").toString());
			docmsearchform.setAppendixId(attrmap.get("appendixId")
					.toString());
			docmsearchform.setMsaId(IMdao.getMSAId(docmsearchform
					.getAppendixId(), ds));
			String appendixStatus = Appendixdao.getAppendixStatus(
					docmsearchform.getAppendixId(), docmsearchform.getMsaId(),
					ds);
			docmsearchform.setAppendixName(Jobdao.getAppendixname(ds,
					docmsearchform.getAppendixId()));
			docmsearchform.setAppendixStatusChecking(appendixStatus);
			docmsearchform.setAppendixType(Appendixdao.getAppendixPrType(
					docmsearchform.getAppendixId(), ds));

			if (docmsearchform.getAppendixStatusChecking().trim()
					.equalsIgnoreCase("Inwork")) {
				String contractDocoumentList = com.mind.dao.PM.Appendixdao
						.getcontractDocoumentMenu(docmsearchform
								.getAppendixId(), ds);
				map.put("contractDocMenu", contractDocoumentList);
				String appendixcurrentstatus = Appendixdao.getCurrentstatus(
						docmsearchform.getAppendixId(), ds);
				map.put("appendixcurrentstatus",
						appendixcurrentstatus);
				docmsearchform.setMsaId(IMdao.getMSAId(docmsearchform
						.getAppendixId(), ds));
				map.put("appendixid", docmsearchform
						.getAppendixId());
				map.put("msaId", docmsearchform.getMsaId());
				map.put("nettype", "");

			} else {
				String addendumlist = Appendixdao.getAddendumsIdName(
						docmsearchform.getAppendixId(), ds);
				String addendumid = com.mind.dao.PM.Addendumdao
						.getLatestAddendumId(docmsearchform.getAppendixId(), ds);
				String appendixType = Appendixdao.getAppendixPrType(
						docmsearchform.getAppendixId(), ds);
				String list = Menu.getStatus("pm_appendix", appendixStatus
						.charAt(0), docmsearchform.getMsaId(), docmsearchform
						.getAppendixId(), "", "", loginuserid, ds);
				map.put("appendixStatusList", list);
				map.put("Appendix_Id", docmsearchform
						.getAppendixId());
				map.put("MSA_Id", docmsearchform.getMsaId());
				map.put("appendixStatus", appendixStatus);
				map.put("latestaddendumid", addendumid);
				map.put("addendumlist", addendumlist);
				map.put("appendixType", appendixType);
			}

			docmsearchform.setEntityId(docmsearchform.getAppendixId());
			docmsearchform.setEntityType(docmsearchform
					.getLinkLibNameFromIlex());
			// docmsearchform.setFromPage(docmsearchform.getFromPage());

		} else if (libName.trim().equalsIgnoreCase("Addendum")) {
			docmsearchform.setJobId(attrmap.get("jobId").toString());
			String[] jobStatusAndType = Jobdao.getJobStatusAndType("%",
					docmsearchform.getJobId(), "%", "%", ds);
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];
			docmsearchform.setJobName(Activitydao.getJobname(ds, docmsearchform
					.getJobId()));
			String jobViewtype = JobDashboarddao.getJobViewType(docmsearchform
					.getJobId(), ds);
			docmsearchform.setAppendixId(IMdao.getAppendixId(docmsearchform
					.getJobId(), ds));
			docmsearchform.setMsaId(IMdao.getMSAId(docmsearchform
					.getAppendixId(), ds));
			docmsearchform.setAppendixName(Jobdao.getAppendixname(ds,
					docmsearchform.getAppendixId()));
			docmsearchform.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					ds, docmsearchform.getMsaId()));
			String list = Menu.getStatus("Addendum", jobStatus.charAt(0), "",
					docmsearchform.getAppendixId(), docmsearchform.getJobId(),
					"", loginuserid, ds);
			map.put("addendumStatusList", list);
			String list_new = Menu.getStatus("Addendum_New",
					jobStatus.charAt(0), "", docmsearchform.getAppendixId(),
					docmsearchform.getJobId(), "", loginuserid, ds);
			map.put("addendumStatusListNew", list_new);
			map.put("Appendix_Id", docmsearchform.getAppendixId());
			map.put("Job_Id", docmsearchform.getJobId());
			map.put("job_Status", jobStatus);
			map.put("jobViewType", jobViewtype);

			docmsearchform.setEntityId(docmsearchform.getJobId());
			docmsearchform.setEntityType(docmsearchform
					.getLinkLibNameFromIlex());
			// docmsearchform.setFromPage(docmsearchform.getFromPage());

		} else if (libName.trim().equalsIgnoreCase("Job")) {
			docmsearchform.setJobId(attrmap.get("jobId").toString());
			String[] jobStatusAndType = Jobdao.getProjectJobStatusAndType(
					docmsearchform.getJobId(), ds);
			String jobStatus = jobStatusAndType[0];
			String jobType1 = jobStatusAndType[1];

			docmsearchform.setJobName(Activitydao.getJobname(ds, docmsearchform
					.getJobId()));
			docmsearchform.setAppendixId(IMdao.getAppendixId(docmsearchform
					.getJobId(), ds));
			docmsearchform.setMsaId(IMdao.getMSAId(docmsearchform
					.getAppendixId(), ds));
			docmsearchform.setAppendixType(Appendixdao.getAppendixPrType(
					docmsearchform.getAppendixId(), ds));
			docmsearchform.setAppendixName(Jobdao.getAppendixname(ds,
					docmsearchform.getAppendixId()));
			docmsearchform.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
					ds, docmsearchform.getMsaId()));
			if (jobStatus != null) {
				String list = Menu.getStatus("prj_job", jobStatus.charAt(0),
						"", docmsearchform.getAppendixId(), docmsearchform
								.getJobId(), "", loginuserid, ds);
				map.put("prjJobStatusList", list);
				
				String listPrjMenuNew = Menu.getStatus("prj_jobMenuNew", jobStatus.charAt(0),
						"", docmsearchform.getAppendixId(), docmsearchform
								.getJobId(), "", loginuserid, ds);
				map.put("prjJobStatusListMenuNew", listPrjMenuNew);
			}
			map.put("Appendix_Id", docmsearchform.getAppendixId());
			map.put("Job_Id", docmsearchform.getJobId());
			map.put("appendixType", docmsearchform
					.getAppendixType());

			docmsearchform.setEntityId(docmsearchform.getJobId());
			docmsearchform.setEntityType(docmsearchform
					.getLinkLibNameFromIlex());

		}
		docmsearchform.setSourceType("Ilex");
		return map;
	}

	/**
	 * Method : this method is used to set the search condition from MPO
	 * 
	 * @param request
	 * @param docmsearchform
	 */
	public static void setSearchConditionForMpo(Map<String, String> paramMap,
			DocMSearchBean docmsearchform) {

		if (paramMap.get("page") != null
				&& paramMap.get("page").equals("mpo")) {
			String fieldName = "";
			String fieldText = "";
			String fieldColumnName[] = new String[2];
			String fieldCondition[] = new String[2];
			String fieldColumnText[] = new String[2];
			String fieldLogicalCondition[] = new String[1];
			docmsearchform.setSelectedMSA("");

			fieldName = DocMSeacrhDao.getIdDataTypeIdPair("MPO") + "~12"; // Document
																			// Status
																			// Equals
																			// Approved
			fieldCondition[0] = "=";
			fieldText = "Approved";
			fieldColumnName[0] = fieldName;
			fieldColumnText[0] = fieldText;
			fieldLogicalCondition[0] = "and";

			if (paramMap.get("view") == null) {
				fieldName = DocMSeacrhDao.getIdDataTypeIdPair("Appendix")
						+ "~1"; // AppendixId Equals AppendixId from Request
								// Parameter
				fieldCondition[1] = "=";
				fieldText = paramMap.get("appendixId").toString();
				fieldColumnName[1] = fieldName;
				fieldColumnText[1] = fieldText;
			}
			if (paramMap.get("view") != null
					&& paramMap.get("view").equals("po")) {
				fieldName = DocMSeacrhDao.getIdDataTypeIdPair("Appendix")
						+ "~1"; // AppendixId Equals AppendixId from Request
								// Parameter
				fieldCondition[1] = "=";
				fieldText = paramMap.get("appendixId").toString();
				fieldColumnName[1] = fieldName;
				fieldColumnText[1] = fieldText;
			}

			docmsearchform.setSelectedFilterColumns(fieldColumnName);
			docmsearchform.setFilterCondition(fieldCondition);
			docmsearchform.setFilterType(fieldColumnText);
			docmsearchform.setLogicalCondition(fieldLogicalCondition);

			try {
				docmsearchform.setSelectedMSA(EntityManager.getLibraryId("",
						paramMap.get("entityType").toString()));
			} catch (LibraryIdNotFoundException e) {
				logger
						.error(
								"setSearchConditionForMpo(HttpServletRequest, DocMSearchBean)",
								e);
			}

			docmsearchform.setSearchNow("search");
		}

	}

}
