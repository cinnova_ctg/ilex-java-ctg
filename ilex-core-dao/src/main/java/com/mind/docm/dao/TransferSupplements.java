package com.mind.docm.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.mind.bean.docm.IlexEntity;
import com.mind.common.IlexConstants;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;

/**
 * The Class TransferSupplements.
 */
public class TransferSupplements implements Runnable {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(TransferSupplements.class);

	/** The supplements. */
	static String[] supplements = { "MSA", "Appendix", "Job", "J" };

	/**
	 * Method: this method is called when we run this class
	 * 
	 * @param args
	 */

	// public static void main(String args[]){
	// startTransfer();
	// }

	public void run() {
		startTransfer();
	}

	/**
	 * Method : this method return the ArrayList of llexEntity(Id,Type) objects
	 * stored in it. Id would be basically the fileId Type would be basically
	 * the Library Name where to move the documents
	 * 
	 * @param sql
	 * @return
	 */
	public synchronized ArrayList getArrayList(String sql) {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				IlexEntity ilexentity = new IlexEntity();
				ilexentity.setEntityId(rs.getString(1));
				ilexentity.setEntityType(rs.getString(2));
				list.add(ilexentity);
			}
		} catch (Exception e) {
			logger.error("getArrayList(String)", e);

			logger.error("getArrayList(String)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
		return list;
	}

	/**
	 * Method : this method is used to get the sql String based on the Type
	 * whose supplements we have to move to DocM.
	 * 
	 * @param type
	 * @return sql string to be executed for the type of Supplements to be
	 *         moved.
	 */
	public synchronized String getSqlForSupplemnts(String type) {
		String sql = "";
		if (type.equalsIgnoreCase("Job") || type.equalsIgnoreCase("J"))
			sql = "select lx_file_id,'"
					+ DatabaseUtilityDao.getKeyValue("ilex.lib.job.support")
					+ "' from lx_upload_file where lx_file_used_type ='"
					+ type
					+ "' and DATALENGTH(lx_file_data) > 0 and lx_file_uploaded_date >  '2008-01-01 00:00:00.000' and lx_file_doc_id is null";
		else if (type.equalsIgnoreCase("MSA"))
			sql = "select lx_file_id,'"
					+ DatabaseUtilityDao.getKeyValue("ilex.lib.msa.support")
					+ "' from lx_upload_file where lx_file_used_type ='"
					+ type
					+ "' and DATALENGTH(lx_file_data) > 0 and lx_file_uploaded_date > '2008-01-01 00:00:00.000' and lx_file_doc_id is null";
		else if (type.equalsIgnoreCase("Appendix"))
			sql = "select lx_file_id,'"
					+ DatabaseUtilityDao
							.getKeyValue("ilex.lib.appendix.support")
					+ "' from lx_upload_file where lx_file_used_type ='"
					+ type
					+ "' and DATALENGTH(lx_file_data) > 0 and lx_file_uploaded_date > '2008-01-01 00:00:00.000' and lx_file_doc_id is null";
		return sql;
	}

	/**
	 * Method : this method iterates over the ArrayList and make a call to DocM
	 * to insert the particualar document. The document id that is returned is
	 * saved in the lx_upload_file as a reference
	 * 
	 * @param list
	 */
	public synchronized void iterateAndInsertDocument(ArrayList list) {
		Iterator it = list.iterator();
		while (it.hasNext()) {
			IlexEntity ilexentity = (IlexEntity) it.next();
			try {
				String Id = EntityManager.approveEntity(
						ilexentity.getEntityId(), ilexentity.getEntityType());
				TransferDocDao.logSuccessMessage(ilexentity.getEntityId(),
						ilexentity.getEntityType(), Id);
			} catch (Exception e) {
				logger.error("iterateAndInsertDocument(ArrayList)", e);

				logger.error("iterateAndInsertDocument(ArrayList)", e);
			}
		}
	}

	/**
	 * Method : this method is called to start the transfer of Supplements
	 * Documents from Ilex to DocM Supplements of MSA -> Sales Support
	 * Supplements of Appendix -> Sales Support Supplements of Job ->
	 * Deliverables
	 * 
	 */
	public synchronized void startTransfer() {
		ArrayList list = new ArrayList();
		for (int i = 0; i < supplements.length; i++) {
			String sql = getSqlForSupplemnts(supplements[i]);
			list = getArrayList(sql);
			iterateAndInsertDocument(list);
		}
	}

}
