//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : EntityManager.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.docm.dao;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.docm.Document;
import com.mind.bean.docm.Field;
import com.mind.bean.docm.IlexEntity;
import com.mind.bean.docm.MetaData;
import com.mind.bean.mpo.PODTO;
import com.mind.common.IlexConstants;
import com.mind.docm.client.ClientOperator;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.docm.exceptions.ControlledTypeException;
import com.mind.docm.exceptions.CouldNotAddToRepositoryException;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.CouldNotMarkDeleteException;
import com.mind.docm.exceptions.CouldNotPhysicallyDeleteException;
import com.mind.docm.exceptions.CouldNotReplaceException;
import com.mind.docm.exceptions.CouldNotSupercedeDocumentException;
import com.mind.docm.exceptions.DocMFormatException;
import com.mind.docm.exceptions.DocumentIdNotFoundException;
import com.mind.docm.exceptions.DocumentIdNotUpdatedException;
import com.mind.docm.exceptions.DocumentNotApprovedException;
import com.mind.docm.exceptions.DocumentNotExistsException;
import com.mind.docm.exceptions.EntityDetailsNotFoundException;
import com.mind.docm.exceptions.EntityFileNameNotFoundException;
import com.mind.docm.exceptions.LibraryIdNotFoundException;
import com.mind.docm.exceptions.NotAbleToInsertDocument;
import com.mind.docm.utils.DocumentUtility;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.ilex.docmanager.dao.EntityDescriptionDao;

public class EntityManager {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(EntityManager.class);

	// Use Case
	/**
	 * @name deleteEntity
	 * @purpose This method is used to do operations on DocM system/Ilex system
	 *          related to deletion request for the entity from Ilex.
	 * @steps 1. User requests to delete the entity by providing the entityId.
	 *        2. EntityManager then commands the ClientOperator to delete the
	 *        document from the DocM system. 3. Further process is available
	 *        within the ClientOperator's deleteDocument operation. 4. In case
	 *        the deletion is not successful, then according Exception is thrown
	 *        to halt the operation and intimate the user.
	 * @param entityId
	 * @return none
	 * @returnDescription none
	 */
	public static void deleteEntity(String entityId, String entityType,
			boolean deletePhysically, String userId)
			throws CouldNotCheckDocumentException, DocumentNotExistsException,
			CouldNotMarkDeleteException, CouldNotPhysicallyDeleteException,
			DocumentIdNotFoundException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : deleteEntity)");
		logger.trace("This method is used to do operations on DocM system/Ilex system related to deletion request for the entity from Ilex.");
		String documentId = null;
		documentId = getDocumentId(entityId, entityType);
		logger.trace("DocumentId." + documentId);
		ClientOperator.deleteDocument(documentId, deletePhysically, userId);
		logger.trace("End :(Class : com.mind.ilex.docmanager.EntityManager | Method : deleteEntity)");
	}

	// Use Case
	/**
	 * @name approveEntity
	 * @purpose This method is used to do operations on DocM system/Ilex system
	 *          related to approval request for the entity from Ilex.
	 * @steps 1. User requests to approve the entity by providing the entityId.
	 *        2. EntityManager then gets the ilexEntity object for the entityId
	 *        provided. 3. EntityManager then converts the ilexEntity object to
	 *        the appropriate DocM's document object format. 4. EntityManager
	 *        then commands the ClientOperator to add the document to the DocM
	 *        system. 5. Further process is available within the
	 *        ClientOperator's addDocument operation. 6. Once the document is
	 *        added, its doucmentId is stored into the IlexEntity databse (e.g.
	 *        for MSA, Appendix etc) 7. In case the approval is not successful,
	 *        then according Exception is thrown to halt the operation and
	 *        intimate the user.
	 * @param entityId
	 * @return none
	 * @returnDescription none
	 */
	public static String approveEntity(String entityId, String entityType,
			byte[] ilexGeneratedPDF, String fileName)
			throws NotAbleToInsertDocument, ControlledTypeException,
			DocMFormatException, CouldNotAddToRepositoryException,
			CouldNotReplaceException, CouldNotCheckDocumentException,
			EntityDetailsNotFoundException, DocumentIdNotUpdatedException

	{
		String existingdocId = null;
		Document existingDocument = null;
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : ApproveEntity)");
		logger.trace("This method is used to do operations on DocM system/Ilex system related to approval request for the entity from Ilex.");
		IlexEntity ilexEntity = getIlexEntity(entityId, entityType);
		Document document = convertToDocument(ilexEntity, ilexGeneratedPDF,
				fileName); // Conversion of IlexEntity to DocM Object.
		if (fileName != null) {
			try {
				existingdocId = getDocumentId(entityId, entityType);
				existingDocument = ClientOperator
						.getDocument(existingdocId, "");
				setDataInDocument(document, existingDocument);
			} catch (DocumentIdNotFoundException e) {
				logger.debug("approveEntity(String, String, byte[], String)", e);

				logger.error(e.getMessage());
			} catch (DocumentNotExistsException e) {
				logger.error("approveEntity(String, String, byte[], String)", e);

				logger.error(e.getMessage());
			}
		}
		String documentId = null;
		documentId = String.valueOf(ClientOperator.addDocument(document));
		if (documentId == null || documentId.equals(""))
			throw new NotAbleToInsertDocument(
					DatabaseUtilityDao
							.getKeyValue("exception.documentinsertion"));

		logger.trace("DocumentId :" + documentId);
		setDocumentId(entityId, entityType, documentId);
		logger.trace("End :(Class : com.mind.ilex.docmanager.EntityManager | Method : ApproveEntity)");
		return documentId;
	}

	public static String approveEntity(String entityId, String entityType,
			byte[] ilexGeneratedPDF) throws NotAbleToInsertDocument,
			ControlledTypeException, DocMFormatException,
			CouldNotAddToRepositoryException, CouldNotReplaceException,
			CouldNotCheckDocumentException, EntityDetailsNotFoundException,
			DocumentIdNotUpdatedException {
		return approveEntity(entityId, entityType, ilexGeneratedPDF, null);

	}

	public static String approveEntity(PODTO bean, String entityType,
			byte[] ilexGeneratedPDF, String fileName)
			throws EntityDetailsNotFoundException,
			CouldNotAddToRepositoryException, DocMFormatException,
			CouldNotCheckDocumentException {

		return approveEntity(bean, entityType, ilexGeneratedPDF, fileName,
				DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME),
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_SYSDB),
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_FILE_SYSDB));
	}

	public static String approveEntity(PODTO bean, String entityType,
			byte[] ilexGeneratedPDF, String fileName, DataSource dataSource,
			DataSource docmDataSource, DataSource docMFileDataSource)
			throws EntityDetailsNotFoundException,
			CouldNotAddToRepositoryException, DocMFormatException,
			CouldNotCheckDocumentException {
		String existingdocId = null;
		String documentId = null;
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : ApproveEntity)");
		logger.trace("This method is used to do operations on DocM system/Ilex system related to approval request for the entity from Ilex.");
		IlexEntity ilexEntity = getIlexEntity(bean.getDevId(), entityType,
				dataSource);
		ilexEntity.setEntityCreatedDate(bean.getDevDate());
		ilexEntity.setEntityName(bean.getDevTitle());
		ilexEntity.setJobName(bean.getJob_Name());
		ilexEntity.setEntityJobId(bean.getJobId());
		ilexEntity.setAppendixName(bean.getAppendix_Name());
		ilexEntity.setEntityAppendixId(bean.getAppendixId());
		ilexEntity.setMsaName(bean.getMSA_Name());
		ilexEntity.setEntityMSAId(bean.getMsaId());
		ilexEntity.setEntityWoId(bean.getWO_ID());
		ilexEntity.setPartnerName(bean.getPartner_Name());
		ilexEntity.setEntityPoId(bean.getPoId());
		ilexEntity.setEntityChangeBy(bean.getUserName());
		ilexEntity.setEntityCreatedBy(null);
		if (ilexEntity.getEntityChangeDate() == null
				|| ilexEntity.getEntityChangeDate().equals("")) {
			ilexEntity.setEntityChangeDate(EntityDescriptionDao
					.getDbCurrentDate(dataSource));
		}
		Document document = convertToDocument(ilexEntity, ilexGeneratedPDF,
				fileName); // Conversion of IlexEntity to DocM Object.

		try {
			existingdocId = getDocumentId(bean.getDevId(), entityType,
					dataSource);
			if (!existingdocId.equals("-1")) {
				try {
					documentId = String.valueOf(ClientOperator
							.implicitModifyDocumentForPODeliverables(
									existingdocId, document,
									bean.getUserName(), docmDataSource,
									docMFileDataSource));
				} catch (CouldNotReplaceException e) {
					e.printStackTrace();
				} catch (CouldNotCheckDocumentException e) {
					e.printStackTrace();
				} catch (DocMFormatException e) {
					e.printStackTrace();
				} catch (DocumentNotExistsException e) {
					e.printStackTrace();
				}
			} else {
				Document newDocument = DocumentUtility.getSearchableDocument(
						document, docmDataSource);
				documentId = String.valueOf(DocumentUtility.addToRepository(
						newDocument, docmDataSource, docMFileDataSource));
				logger.trace("DocumentId :" + documentId);
				logger.trace("End :(Class : com.mind.ilex.docmanager.EntityManager | Method : ApproveEntity)");

			}

		} catch (DocumentIdNotFoundException e) {
			logger.debug("approveEntity(String, String, byte[], String)", e);
			logger.error(e.getMessage());
		}

		return documentId;

	}

	// explicitModifyDocument

	/**
	 * Method : This method is used to insert the supplement documents in the
	 * DocM Method convert the entityId,entityType into IlexEntity.This
	 * IlexEntity is further gets converted into document object that would get
	 * inserted into DocM
	 * 
	 * @param entityId
	 * @param entityType
	 * @return
	 * @throws NotAbleToInsertDocument
	 * @throws ControlledTypeException
	 * @throws DocMFormatException
	 * @throws CouldNotAddToRepositoryException
	 * @throws CouldNotReplaceException
	 * @throws CouldNotCheckDocumentException
	 * @throws EntityDetailsNotFoundException
	 * @throws DocumentIdNotUpdatedException
	 */
	public static String approveEntity(String entityId, String entityType)
			throws NotAbleToInsertDocument, ControlledTypeException,
			DocMFormatException, CouldNotAddToRepositoryException,
			CouldNotReplaceException, CouldNotCheckDocumentException,
			EntityDetailsNotFoundException, DocumentIdNotUpdatedException

	{
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : ApproveEntity)");
		logger.trace("This method is used to do operations on DocM system/Ilex system related to approval request for the entity from Ilex.");

		IlexEntity ilexEntity = getIlexEntity(entityId, entityType);
		ilexEntity.setEntitySupplementDocs(EntityDescriptionDao
				.getSupplementDocs(ilexEntity.getEntityId()));

		Document document = convertToDocument(ilexEntity); // Conversion of
		// IlexEntity to
		// DocM Object.
		String documentId = null;
		documentId = String.valueOf(ClientOperator.addDocument(document));
		if (documentId == null || documentId.equals(""))
			throw new NotAbleToInsertDocument(
					DatabaseUtilityDao
							.getKeyValue("exception.documentinsertion"));

		logger.trace("DocumentId :" + documentId);
		setDocumentId(entityId, entityType, documentId);
		logger.trace("End :(Class : com.mind.ilex.docmanager.EntityManager | Method : ApproveEntity)");
		return documentId;
	}

	// Use Case
	/**
	 * @name approveEntity
	 * @purpose This method is used to do operations on DocM system/Ilex system
	 *          related to approval request for the entity from Ilex. This is
	 *          the overloaded method used for report generation.
	 * @steps 1. User requests to approve the entity by providing the entityId.
	 *        2. EntityManager then gets the ilexEntity object for the entityId
	 *        provided. 3. EntityManager then converts the ilexEntity object to
	 *        the appropriate DocM's document object format. 4. EntityManager
	 *        then commands the ClientOperator to add the document to the DocM
	 *        system. 5. Further process is available within the
	 *        ClientOperator's addDocument operation. 6. Once the document is
	 *        added, its doucmentId is stored into the IlexEntity databse (e.g.
	 *        for MSA, Appendix etc) 7. In case the approval is not successful,
	 *        then according Exception is thrown to halt the operation and
	 *        intimate the user.
	 * @param entityId
	 * @return none
	 * @returnDescription none
	 */

	public static String approveEntity(String entityId, String entityType,
			byte[] ilexGeneratedPDF, int ilexThread)
			throws NotAbleToInsertDocument, ControlledTypeException,
			DocMFormatException, CouldNotAddToRepositoryException,
			CouldNotReplaceException, CouldNotCheckDocumentException,
			EntityDetailsNotFoundException, DocumentIdNotUpdatedException

	{
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : ApproveEntity overloaded method)");
		logger.trace("This method is used to do operations on DocM system/Ilex system related to approval request for the entity from Ilex.");
		IlexEntity ilexEntity = getIlexEntity(entityId, entityType);
		Document document = convertToDocument(ilexEntity, ilexGeneratedPDF); // Conversion
		// of
		// IlexEntity
		// to
		// DocM
		// Object.
		String documentId = null;
		documentId = String.valueOf(ClientOperator.addDocument(document));
		if (documentId == null || documentId.equals(""))
			throw new NotAbleToInsertDocument(
					DatabaseUtilityDao
							.getKeyValue("exception.documentinsertion"));

		logger.trace("DocumentId :" + documentId);
		setDocumentId(entityId, entityType, documentId);
		logger.trace("End :(Class : com.mind.ilex.docmanager.EntityManager | Method : ApproveEntity overloaded method)");
		return documentId;
	}

	// Use Case
	/**
	 * @name setEntityToDraft
	 * @purpose This method is used to do operations on DocM system/Ilex system
	 *          related to setting the entity to draft state from (say approved
	 *          state) in Ilex.
	 * @steps 1. User requests to set the entity to draft by providing the
	 *        entityId. 2. EntityManager then gets the documentId stored inside
	 *        of this ilexEntity . 3. EntityManager then commands the
	 *        ClientOperator to set the document to 'draft' (superceded) in the
	 *        DocM system. 4. Further process is available within the
	 *        ClientOperator's setDocumentToDraft operation. 5. In case the
	 *        state-change is not successful, then according Exception is thrown
	 *        to halt the operation and intimate the user.
	 * @param entityId
	 * @return none
	 * @returnDescription none
	 */
	public static void setEntityToDraft(String entityId, String entityType)
			throws DocumentIdNotFoundException, DocumentNotExistsException,
			CouldNotSupercedeDocumentException, CouldNotCheckDocumentException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : setEntityToDraft)");
		logger.trace("This method is used to do operations on DocM system/Ilex system related to setting the entity to draft state from (say approved state) in Ilex.");
		String documentId = null;
		documentId = getDocumentId(entityId, entityType);
		logger.trace("DocumentId :" + documentId);
		ClientOperator.setDocumentToDraft(documentId);
		logger.trace("End :(Class : com.mind.ilex.docmanager.EntityManager | Method : setEntityToDraft)");
	}

	// Use Case
	/**
	 * @name setEntityToApproved
	 * @purpose This method is used to do operations on DocM system/Ilex system
	 *          related to setting the entity to draft/cancel state from (say
	 *          approved state) in Ilex.
	 * @steps 1. User requests to set the entity to draft/cancel by providing
	 *        the entityId. 2. EntityManager then gets the documentId stored
	 *        inside of this ilexEntity . 3. EntityManager then commands the
	 *        ClientOperator to set the document to 'Approved' (superceded) in
	 *        the DocM system. 4. Further process is available within the
	 *        ClientOperator's setEntityToApproved operation. 5. In case the
	 *        state-change is not successful, then according Exception is thrown
	 *        to halt the operation and intimate the user.
	 * @param entityId
	 * @return none
	 * @returnDescription none
	 */
	public static void setEntityToApproved(String entityId, String entityType)
			throws DocumentIdNotFoundException, DocumentNotExistsException,
			CouldNotSupercedeDocumentException, CouldNotCheckDocumentException,
			DocumentNotApprovedException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : setEntityToApproved)");
		logger.trace("This method is used to do operations on DocM system/Ilex system related to setting the entity to draft/cancel state from (say approved state) in Ilex.");
		String documentId = null;
		documentId = getDocumentId(entityId, entityType);
		logger.trace("DocumentId :" + documentId);
		ClientOperator.setDocumentToApproved(documentId);
		logger.trace("End :(Class : com.mind.ilex.docmanager.EntityManager | Method : setEntityToDraft)");
	}

	/**
	 * @name setDocumentId
	 * @purpose This method is used to the set the DocumentId returned from the
	 *          DocM System in the IlexSystem
	 * @steps 1. User requests to set the DocumentId for the specific entity. 2.
	 *        In turn it commands the EntityDescriptionDao set the entity
	 *        corressponding documentId.
	 * @param entityId
	 * @param entityType
	 * @param documentId
	 * @return
	 * @returnDescription
	 */

	public static void setDocumentId(String entityId, String entityType,
			String documentId) throws DocumentIdNotUpdatedException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : setDocumentId)");
		logger.trace("This method is used to the set the DocumentId returned from the DocM System in the IlexSystem.");
		EntityDescriptionDao.setEntityCorrespondingDocumentId(entityId,
				entityType, documentId); // Database call to set the documentId
		// for a specific entity.
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : setDocumentId)");
	}

	/**
	 * @name getDocumentId
	 * @purpose This method is used to the get the DocumentId from the
	 *          IlexSystem by providing the entityId,enitityType
	 * @steps 1. User requests to get the DocumentId. 2. In turn it commands the
	 *        EntityDescriptionDao get the entity correspopnding documentId.
	 * @param entityId
	 * @param entityType
	 * @return String
	 * @returnDescription The Id of the Document available in the Ilex
	 *                    corresponding the entityId,entityType
	 */

	public static String getDocumentId(String entityId, String entityType,
			DataSource ds) throws DocumentIdNotFoundException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : getDocumentId)");
		logger.trace("This method is used to the get the DocumentId from the IlexSystem by providing the entityId,enitityType.");
		String documentId = "";
		documentId = EntityDescriptionDao.getEntityCorrespondingDocumentId(
				entityId, entityType, ds); // Database call to get the
											// documentId.
		logger.trace("DocumentId :" + documentId);
		logger.trace("End :(Class : com.mind.ilex.docmanager.EntityManager | Method : getDocumentId)");
		return documentId;
	}

	public static String getDocumentId(String entityId, String entityType)
			throws DocumentIdNotFoundException {

		return getDocumentId(entityId, entityType,
				DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME));
	}

	public static String getLibraryId(String entityId, String entityType)
			throws LibraryIdNotFoundException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : getDocumentId)");
		logger.trace("This method is used to the get the DocumentId from the IlexSystem by providing the entityId,enitityType.");
		String libId = "";
		libId = EntityDescriptionDao.getEntityCorrespondingLibraryId(entityId,
				entityType);
		logger.trace("LibraryId :" + libId);
		logger.trace("End :(Class : com.mind.ilex.docmanager.EntityManager | Method : getDocumentId)");
		return libId;
	}

	/**
	 * @name getDocument
	 * @purpose This method is used to the get the Document Object from the DocM
	 *          providing the entityId,enitityType
	 * @steps 1. User requests to get the Document Object. 2. It first gets the
	 *        documentId corresponding to the Entity. 3. Further process is
	 *        available within the ClientOperator's getDocument operation.
	 * @param entityId
	 * @param entityType
	 * @return Document Object
	 * @returnDescription The Document Object corresponding to the entityId and
	 *                    entityType.
	 */
	public static com.mind.bean.docm.Document getDocument(String entityId,
			String entityType) throws DocumentIdNotFoundException,
			CouldNotCheckDocumentException, DocumentNotExistsException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : getDocument)");
		logger.trace("This method is used to the get the DocumentId from the IlexSystem by providing the entityId,enitityType.");
		Document document = null;
		String documentId = null;
		documentId = getDocumentId(entityId, entityType); // get the documentId
		// corresponding to
		// the IlexEntity
		// porperties.
		logger.trace("DocumentId :" + documentId);
		document = ClientOperator.getDocument(documentId, "");
		logger.trace("End :(Class : com.mind.ilex.docmanager.EntityManager | Method : getDocument)");
		return document;
	}

	// Use Case
	/**
	 * @name downloadEntityFile
	 * @purpose This method is used to do operations on DocM system/Ilex system
	 *          related to downloading the document file corresponding to the
	 *          ilexEntity.
	 * @steps 1. User requests to get the file by providing the entityId. 2.
	 *        EntityManager then commands to get the document object
	 *        corresponding to the Ilex Entity. 3. In turn it commands the
	 *        ClientOperator to get the document from the DocM system. 4.
	 *        Further process is available within the ClientOperator's
	 *        getDocument operation. 5. Once the document is returned, then
	 *        EntityManager gets the file from this document object and returns
	 *        to the user (download). 6. In case the operation is not
	 *        successful, then according Exception is thrown to halt the
	 *        operation and intimate the user.
	 * @param entityId
	 * @return File
	 * @returnDescription The file corresponding to the document from DocM
	 *                    system.
	 */
	public static byte[] downloadEntityFile(String entityId, String entityType)
			throws DocumentIdNotFoundException, CouldNotCheckDocumentException,
			DocumentNotExistsException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : downloadEntityFile)");
		logger.trace("This method is used to do operations on DocM system/Ilex system related to downloading the document file corresponding to the ilexEntity.");
		Document document = null;
		document = getDocument(entityId, entityType);
		logger.trace("End :(Class : com.mind.ilex.docmanager.EntityManager | Method : downloadEntityFile)");
		if (document != null) {
			return document.getFileBytes();
		} else {
			return null;
		}
	}

	/**
	 * @name getIlexEntity
	 * @purpose This method is used to get the IlexEntity Object.
	 * @steps 1. User requests to get the IlexEntity by providing the entityid
	 *        and entityType. 2. In turn it commands the EntityDescriptionDao
	 *        get the details of the corresponding entity.
	 * @param entityId
	 * @param entityType
	 * @return IlexEntity Object
	 * @returnDescription The IlexEntity Object corresponding to the entityId
	 *                    and entityType.
	 */

	public static IlexEntity getIlexEntity(String entityId, String entityType)
			throws EntityDetailsNotFoundException {
		return getIlexEntity(entityId, entityType,
				DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME));
	}

	public static IlexEntity getIlexEntity(String entityId, String entityType,
			DataSource ds) throws EntityDetailsNotFoundException {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : getIlexEntity)");
		logger.trace("This method is used to the IlexEntity Object.");
		IlexEntity ilexEntity = null;
		ilexEntity = EntityDescriptionDao.getEntityDetails(entityId,
				entityType, ds); // call to the get the
		// IlexEntity
		// Details available
		// in the
		// IlexDatabase
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : getIlexEntity)");
		return ilexEntity;
	}

	/**
	 * @name convertToDocument
	 * @purpose This method is used to convert the IlexEntity Object into the
	 *          DocM Document Object.
	 * @steps 1. User requests to get the Document Object by providing the
	 *        Object of IlexEntity and the pdf contents. 2. In turn it commands
	 *        the EntityDescriptionDao to the mapping of IlexEntity to Document
	 *        Object MetaData Properties.
	 * @param IlexEntity
	 * @param byte[]
	 * @return Document Object
	 * @returnDescription The Document Object corresponding to the IlexEntity
	 *                    Object.
	 */

	public static com.mind.bean.docm.Document convertToDocument(
			IlexEntity ilexEntity, byte[] generatedPDF, String fileName) {
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : convertToDocument)");
		logger.trace("This method is used to convert the IlexEntity Object into the DocM Document Object.");
		Document document = new Document();
		MetaData metaData = new MetaData();
		metaData.setFields(EntityDescriptionDao.getIlexToDocMMapping(
				ilexEntity, (generatedPDF == null ? 0 : generatedPDF.length),
				fileName)); // get the corresponding mapping for IlexEntity to
		// MetaData Field[]
		try {
			if (fileName == null) {
				document.setFileName(fileNameSpecificToEntity(
						ilexEntity.getEntityId(), ilexEntity.getEntityType()));
			} else {
				document.setFileName(fileName);
			}
		} catch (EntityFileNameNotFoundException e) {
			logger.error("convertToDocument(IlexEntity, byte[], String)", e);
			logger.error(e.getMessage());
		}
		document.setFileBytes(generatedPDF);
		document.setMetaData(metaData);
		logger.trace("File Name : convertToDocument)" + document.getFileName());
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : convertToDocument)");
		return document;
	}

	public static com.mind.bean.docm.Document convertToDocument(
			IlexEntity ilexEntity, byte[] generatedPDF) {
		return convertToDocument(ilexEntity, generatedPDF, null);
	}

	/**
	 * Method: method converts the IlexEntity into the Document Object for
	 * Supplements Documents
	 * 
	 * @param ilexEntity
	 * @return
	 */
	public static com.mind.bean.docm.Document convertToDocument(
			IlexEntity ilexEntity) {
		Document document = new Document();
		MetaData metaData = new MetaData();
		metaData.setFields(EntityDescriptionDao
				.getIlexToDocMMapping(ilexEntity));
		document.setFileBytes(ilexEntity.getEntitySupplementDocs()
				.getFileData());
		document.setMetaData(metaData);
		document.setFileName(ilexEntity.getEntitySupplementDocs().getFileName());
		return document;
	}

	// Use Case
	/**
	 * @name fileNameSpecificToEntity
	 * @purpose This method is used to get the generated PDF file name by
	 *          ilexEntity specific.
	 * @steps 1. User requests to get the filename by providing the entityType.
	 *        2. In turn it commands the EntityDescriptionDao to get the file
	 *        name from the Application Resources
	 * @param entityType
	 * @param entityId
	 * @return String
	 * @returnDescription The filename corresponding to the entityType of DocM
	 *                    system.
	 */
	public static String fileNameSpecificToEntity(String entityId,
			String entityType) throws EntityFileNameNotFoundException {
		logger.trace("Start :(Class : EntityManager | Method : fileNameSpecificToEntity)");
		logger.trace("This method is used to get the generated PDF file name by ilexEntity specific.");
		String fileName = "";
		fileName = EntityDescriptionDao.getEntityFileName(entityId, entityType);
		logger.trace("Start :(Class : com.mind.ilex.docmanager.EntityManager | Method : fileNameSpecificToEntity)");
		return fileName;
	}

	public static String getFieldValue(String docId, String fieldName) {
		String fieldValue = "";
		fieldValue = EntityDescriptionDao.getFieldValueFordocument(docId,
				fieldName);
		return fieldValue;
	}

	public static void setDataInDocument(Document newDocument,
			Document existingDocument) {
		String addDate = "";
		String addUser = "";
		String addMethod = "";
		String existingAddDate = "";
		String existingAddUser = "";
		String existingAddMethod = "";
		Field[] newDocumentField = newDocument.getMetaData().getFields();
		Field[] existingDocumentField = newDocument.getMetaData().getFields();
		for (int i = 0; i < existingDocumentField.length; i++) {
			if (existingDocumentField[i]
					.getName()
					.trim()
					.equalsIgnoreCase(
							DatabaseUtilityDao
									.getKeyValue("ilex.docm.entitymanager.adddate"))) {
				existingAddDate = existingDocumentField[i].getValue();
			}
			if (existingDocumentField[i]
					.getName()
					.trim()
					.equalsIgnoreCase(
							DatabaseUtilityDao
									.getKeyValue("ilex.docm.entitymanager.adduser"))) {
				existingAddUser = existingDocumentField[i].getValue();
			}
			if (existingDocumentField[i]
					.getName()
					.trim()
					.equalsIgnoreCase(
							DatabaseUtilityDao
									.getKeyValue("ilex.docm.entitymanager.addmethod"))) {
				existingAddMethod = existingDocumentField[i].getValue();
			}
		}
		for (int i = 0; i < newDocumentField.length; i++) {
			if (newDocumentField[i]
					.getName()
					.trim()
					.equalsIgnoreCase(
							DatabaseUtilityDao
									.getKeyValue("ilex.docm.entitymanager.adddate"))) {
				addDate = newDocumentField[i].getValue();
				newDocumentField[i].setValue(existingAddDate);
			}
			if (newDocumentField[i]
					.getName()
					.trim()
					.equalsIgnoreCase(
							DatabaseUtilityDao
									.getKeyValue("ilex.docm.entitymanager.adduser"))) {
				addUser = newDocumentField[i].getValue();
				newDocumentField[i].setValue(existingAddUser);
			}
			if (newDocumentField[i]
					.getName()
					.trim()
					.equalsIgnoreCase(
							DatabaseUtilityDao
									.getKeyValue("ilex.docm.entitymanager.addmethod"))) {
				addMethod = newDocumentField[i].getValue();
				newDocumentField[i].setValue(existingAddMethod);
			}

		}
		for (int i = 0; i < newDocumentField.length; i++) {
			if (newDocumentField[i]
					.getName()
					.trim()
					.equalsIgnoreCase(
							DatabaseUtilityDao
									.getKeyValue("ilex.docm.entitymanager.updatedocumentdate"))) {
				newDocumentField[i].setValue(addDate);
			}
			if (newDocumentField[i]
					.getName()
					.trim()
					.equalsIgnoreCase(
							DatabaseUtilityDao
									.getKeyValue("ilex.docm.entitymanager.updatedocumentuser"))) {
				newDocumentField[i].setValue(addUser);
			}
			if (newDocumentField[i]
					.getName()
					.trim()
					.equalsIgnoreCase(
							DatabaseUtilityDao
									.getKeyValue("ilex.docm.entitymanager.updatemethod"))) {
				newDocumentField[i].setValue(addMethod);
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("setDataInDocument(Document, Document) - name-- "
					+ existingAddDate + " -val- " + addDate);
		}
	}
}
