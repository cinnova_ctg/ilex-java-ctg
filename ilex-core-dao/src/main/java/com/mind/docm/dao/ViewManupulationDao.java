/*
 * Copyright (C) 2008 MIND
 * All rights reserved.
 * The information contained here in is confidential and
 * proprietary to MIND and forms the part of MIND
 * Project	: ILEX
 * Description	: <This file contains the whole logic for the database intraction 
 * 							for the library add/modify/delete/view.>
 *
 */

package com.mind.docm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mind.bean.docm.ViewManupulationDTO;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * Class ViewManupulationDao - This file contains the whole logic for the
 * database intraction for the library add/modify/delete/view. methods -
 * getColumns, insertData, getViewId, setOtherFields, fetchInsertedData,
 * getDataForOtherFields, getPreCondition , deleteData, isViewExist
 */
public class ViewManupulationDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ViewManupulationDao.class);

	/**
	 * getColumns - This method picks out all available columns.
	 * 
	 * @param request
	 *            - This is HttpServletRequest parameter.
	 * @param ViewManupulationBean
	 *            - FormBean object.
	 * @return Returns None.
	 **/

	public void getColumns(Map<String, Object> map) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : getColumns)");
		Statement stmt = null;
		ResultSet rs = null;
		List outerList = null;

		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		try {
			String sql = "select dmfl.*,dtg.dm_tgrp_desc from dbo.dm_metadata_fields_list dmfl left outer join dbo.dm_taxonomy_group dtg"
					+ " \n on dmfl.dm_md_fl_tgrp_id = dtg.dm_tgrp_id";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			outerList = new ArrayList();
			// Getting Data for all columns and put it into list.
			while (rs.next()) {
				List innerList = new ArrayList();
				innerList.add(rs.getString("dm_md_fl_id")); // 0
				innerList.add(rs.getString("dm_md_fl_desc")); // 1
				innerList.add(rs.getString("dm_md_fl_nm")); // 2
				innerList.add(rs.getString("dm_md_fl_desc")); // 3
				innerList.add(rs.getString("dm_md_fl_type_id")); // 4
				innerList.add(rs.getString("dm_md_fl_dval")); // 5
				innerList.add(rs.getString("dm_tgrp_desc")); // 6
				innerList.add(rs.getString("dm_md_fl_nm")); // 7
				innerList.add(rs.getString("dm_md_fl_tgrp_id")); // 8
				innerList.add(rs.getString("dm_md_fl_type_id")); // 9
				innerList.add(rs.getString("dm_md_fl_id") + "~"
						+ rs.getString("dm_md_fl_type_id")); // 10
				outerList.add(innerList);
			}
			map.put("columnList", outerList);
			map.put("selectedColumList", outerList);
			map.put("filterColumnList", outerList);
			map.put("filterFieldSize",
					getFieldSize("dbo.dm_library_view_filter",
							"dm_lib_vw_flt_fld_value", conn));
			map.put("viewFieldSize",
					getFieldSize("dbo.dm_library_view_master", "dm_lib_vw_nm",
							conn));
		} catch (SQLException sqle) {
			logger.error(
					"getColumns(HttpServletRequest, ViewManupulationBean)",
					sqle);

			logger.error(sqle.getMessage());
		} catch (Exception sqle) {
			logger.error(
					"getColumns(HttpServletRequest, ViewManupulationBean)",
					sqle);

			logger.error(sqle.getMessage());
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		logger.trace("End :(Class : com.mind.docm.dao.ViewManupulationDao | Method : getColumns)");
	}

	/**
	 * insertData - This method helps in inserting and modifying data for
	 * library view.
	 * 
	 * @param request
	 *            - This is HttpServletRequest parameter.
	 * @param ViewManupulationBean
	 *            - FormBean object.
	 * @return Returns None.
	 **/
	public void insertData(Map<String, Object> map, ViewManupulationDTO bean) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		PreparedStatement pStmt = null;
		String returnCondChar;
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		try {
			String viewId = bean.getViewId();

			String sql = "";
			conn.setAutoCommit(false);
			// Updates the information regarding view w.r.t libraray.
			if (bean.getFunction().equalsIgnoreCase("update")) {
				sql = "update dbo.dm_library_view_master"
						+ "\n set dm_lib_vw_nm = ?, dm_lib_vw_desc  = ?"
						+ " \n where dm_lib_vw_lib_id = ? and dm_lib_vw_id = ?";
				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, bean.getViewName());
				pStmt.setString(2, bean.getViewName());
				pStmt.setInt(3, new Integer(bean.getLibId()).intValue());
				pStmt.setInt(4, new Integer(bean.getViewId()).intValue());
				pStmt.executeUpdate();
				// Updates the information regarding view detials.
				sql = "update dbo.dm_library_view_detail"
						+ " \n set dm_lib_vw_dtl_fl_show = ?"
						+ " \n where dm_lib_vw_dtl_md_fl_id = ? and dm_lib_vw_dtl_lib_vw_id = ?";
				for (int i = 0; i < bean.getIdValues().length; i++) {
					/*
					 * sql = "update dbo.dm_library_view_detail" +
					 * " \n set dm_lib_vw_dtl_fl_show ='"
					 * +bean.getColumnCheckValue()[i]
					 * +"'  where dm_lib_vw_dtl_md_fl_id = "
					 * +bean.getIdValues()[i
					 * ]+" and dm_lib_vw_dtl_lib_vw_id ="+viewId;
					 * 
					 * stmt = conn.createStatement(); stmt.executeUpdate(sql);
					 */
					// System.out.println("in update loop"+sql);
					// System.out.println();
					pStmt = conn.prepareStatement(sql);
					pStmt.setString(1, bean.getColumnCheckValue()[i]);
					pStmt.setInt(2,
							new Integer(bean.getIdValues()[i]).intValue());
					pStmt.setInt(3, new Integer(viewId).intValue());
					pStmt.executeUpdate();
				}
				map.put("updated", "update");
			}
			// if its an add operation then a new view is added
			// w.r.t libraray.
			if (bean.getFunction().equalsIgnoreCase("add")) {
				String insertViewName = "insert into dbo.dm_library_view_master (dm_lib_vw_lib_id,dm_lib_vw_nm,dm_lib_vw_desc)"
						+ "\n values(?,?,?)";
				pStmt = conn.prepareStatement(insertViewName,
						Statement.RETURN_GENERATED_KEYS);
				pStmt.setInt(1, new Integer(bean.getLibId()).intValue());
				pStmt.setString(2, bean.getViewName());
				pStmt.setString(3, bean.getViewName());
				int result = pStmt.executeUpdate();
				if (result > 0) {
					ResultSet rs = pStmt.getGeneratedKeys();
					if (rs.next()) {
						viewId = rs.getString(1);
					}
					// viewId = getViewId(bean.getLibId(),conn);
					bean.setViewId(viewId);
					// if its an add operation then details of view added.
					sql = "insert into dbo.dm_library_view_detail (dm_lib_vw_dtl_lib_vw_id,dm_lib_vw_dtl_md_fl_id,dm_lib_vw_dtl_fl_show,"
							+ " \n dm_lib_vw_dtl_seq_no,dm_lib_vw_dtl_sort_ord,dm_lib_vw_dtl_grp_ord) values (?,?,?,?,?,?)";
					for (int i = 0; i < bean.getIdValues().length; i++) {
						// System.out.println("in insert loop"+i);
						pStmt = conn.prepareStatement(sql);
						pStmt.setInt(1, new Integer(viewId).intValue());
						pStmt.setInt(2,
								new Integer(bean.getIdValues()[i]).intValue());
						pStmt.setString(3, bean.getColumnCheckValue()[i]);
						pStmt.setInt(4, 0);
						pStmt.setInt(5, 0);
						pStmt.setInt(6, 0);
						pStmt.executeUpdate();
					}
					map.put("updated", "save");
				} else {
					throw new SQLException();
				}
			}
			// other fields are also set w.r.t view and library
			for (int i = 0; i < bean.getSelectedColumns().length; i++) {
				setOtherFields("dm_lib_vw_dtl_seq_no", conn,
						bean.getSelectedColumns()[i], i + 1, viewId);
			}
			for (int i = 0; i < bean.getIdValues().length; i++) {
				setOtherFields("dm_lib_vw_dtl_sort_ord", conn,
						bean.getIdValues()[i], 0, viewId);
			}
			if (bean.getSelectedSortColumns() != null) {
				String order = "";

				for (int j = 0; j < bean.getSelectedSortColumns().length; j++) {
					if (bean.getSelectedSortColumns()[j] != null
							&& !bean.getSelectedSortColumns()[j].trim()
									.equalsIgnoreCase("none")
							&& !bean.getSelectedSortColumns()[j].trim()
									.equalsIgnoreCase("")) {
						if (new Integer(bean.getSortDirrection()[j]).intValue() > 0)
							order = String.valueOf(j + 1);
						else
							order = "-" + String.valueOf(j + 1);
						setOtherFields("dm_lib_vw_dtl_sort_ord", conn,
								bean.getSelectedSortColumns()[j], new Integer(
										order).intValue(), viewId);
					}
				}
			}
			for (int i = 0; i < bean.getIdValues().length; i++) {
				setOtherFields("dm_lib_vw_dtl_grp_ord", conn,
						bean.getIdValues()[i], 0, viewId);
			}
			if (bean.getSelectedGroupColumns() != null) {

				for (int j = 0; j < bean.getSelectedGroupColumns().length; j++) {
					if (bean.getSelectedGroupColumns()[j] != null
							&& !bean.getSelectedGroupColumns()[j].trim()
									.equalsIgnoreCase("none")
							&& !bean.getSelectedGroupColumns()[j].trim()
									.equalsIgnoreCase(""))
						setOtherFields("dm_lib_vw_dtl_grp_ord", conn,
								bean.getSelectedGroupColumns()[j], j + 1,
								viewId);
				}
			}
			// first deletes all the entries from dm_library_view_filter
			// then insert each time.
			sql = "delete from dbo.dm_library_view_filter where dm_lib_vw_flt_lib_vw_id =?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, new Integer(bean.getViewId()).intValue());
			pStmt.executeUpdate();

			if (bean.getSelectedFilterColumns() != null) {
				String fieldId = null;
				String fieldTypeId = null;
				sql = "insert into dbo.dm_library_view_filter"
						+ " \n (dm_lib_vw_flt_lib_vw_id,dm_lib_vw_flt_pre_cndn,dm_lib_vw_flt_fld_id,dm_lib_vw_flt_comparator,dm_lib_vw_flt_fld_value)"
						+ " \n values (?,?,?,?,?)";
				for (int i = 0; i < bean.getSelectedFilterColumns().length; i++) {
					if (bean.getSelectedFilterColumns()[i] != null
							&& !bean.getSelectedFilterColumns()[i].trim()
									.equalsIgnoreCase("none")
							&& !bean.getSelectedFilterColumns()[i].trim()
									.equalsIgnoreCase("")) {

						if (!bean.getSelectedFilterColumns()[i]
								.equalsIgnoreCase("Any")) {
							fieldId = bean.getSelectedFilterColumns()[i]
									.substring(0, bean
											.getSelectedFilterColumns()[i]
											.indexOf("~"));
							fieldTypeId = bean.getSelectedFilterColumns()[i]
									.substring(bean.getSelectedFilterColumns()[i]
											.indexOf("~") + 1);
						}
						pStmt = conn.prepareStatement(sql);
						pStmt.setInt(1, new Integer(viewId).intValue());
						returnCondChar = bean.getFilterCondition()[i];
						if (bean.getFilterCondition()[i].trim()
								.equalsIgnoreCase("greaterThan")
								|| bean.getFilterCondition()[i].trim()
										.equalsIgnoreCase("lessThan"))
							returnCondChar = getPreCondition(bean
									.getFilterCondition()[i]);
						pStmt.setString(2, returnCondChar);
						if (!bean.getSelectedFilterColumns()[i].trim()
								.equalsIgnoreCase("Any"))
							pStmt.setInt(3, new Integer(fieldId).intValue());
						else
							pStmt.setInt(3, 0);
						if (bean.getLogicalCondition() != null) {
							if (i == 0) {
								pStmt.setString(4, "");
							} else if (!bean.getLogicalCondition()[i - 1]
									.trim().equalsIgnoreCase("")
									&& bean.getLogicalCondition()[i - 1] != null) {
								pStmt.setString(4,
										bean.getLogicalCondition()[i - 1]);
							}
						} else {
							pStmt.setString(4, "");
						}
						pStmt.setString(5, bean.getFilterType()[i]);
						pStmt.executeUpdate();
					}
				}
			}
			conn.commit();

			bean.setFunction("view");
			fetchInsertedData(map, bean);

		}

		catch (SQLException sqle) {
			logger.error(
					"insertData(HttpServletRequest, ViewManupulationBean)",
					sqle);

			try {
				conn.rollback();
			} catch (Exception e) {
				logger.error(sqle.getMessage());
				logger.error(
						"insertData(HttpServletRequest, ViewManupulationBean)",
						e);
			}
			map.put("exception", "exception");
		} catch (Exception sqle) {
			logger.error(
					"insertData(HttpServletRequest, ViewManupulationBean)",
					sqle);

			try {
				conn.rollback();
			} catch (Exception e) {
				logger.error(
						"insertData(HttpServletRequest, ViewManupulationBean)",
						e);

				logger.error(sqle.getMessage());
				logger.error(
						"insertData(HttpServletRequest, ViewManupulationBean)",
						e);
			}
			map.put("exception", "exception");
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		logger.trace("End :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
	}

	/**
	 * getViewId - This method helps in fetching viewId for the perticular
	 * libraray.
	 * 
	 * @param libId
	 *            - This is Library Id.
	 * @param conn
	 *            - Connection object.
	 * @return Returns viewId.
	 **/
	public static String getViewId(String libId, Connection conn)
			throws SQLException, Exception {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : getViewId)");
		Statement stmt = null;
		ResultSet rs = null;
		String viewId = "";
		// helps in getting max val of the view id.
		try {
			String sql = "select max(dm_lib_vw_id) as viewId from dbo.dm_library_view_master where dm_lib_vw_lib_id = "
					+ new Integer(libId).intValue();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next())
				viewId = rs.getString("viewId");
		} catch (Exception e) {
			logger.error("getViewId(String, Connection)", e);

			throw e;
		} finally {
			DBUtil.close(rs, stmt);
		}
		logger.trace("End :(Class : com.mind.docm.dao.ViewManupulationDao | Method : getViewId)");
		return viewId;
	}

	/**
	 * setOtherFields - This method helps in setting data for some columns in
	 * dbo.dm_library_view_detail table.
	 * 
	 * @param columnName
	 *            - for whome the data has to be set.
	 * @param conn
	 *            - Connection Object.
	 * @param columnValue
	 *            - data for the above coumn name parameter.
	 * @param sequenceOrder
	 *            - related to logic.
	 * @param viewId
	 *            - id for whome all this thing is done.
	 * @return Returns None.
	 **/

	public static void setOtherFields(String columnName, Connection conn,
			String columnValue, int sequenceOrder, String viewId)
			throws SQLException, Exception {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : setOtherFields)");
		// System.out.println("in setOtherFields");
		String sql = "";
		Statement stmt = null;
		// helps in setting other fields using general method.
		sql = "update dm_library_view_detail" + " \n set " + columnName + " = "
				+ sequenceOrder + " where dm_lib_vw_dtl_md_fl_id ="
				+ columnValue + " and dm_lib_vw_dtl_lib_vw_id=" + viewId;
		// System.out.println(sql);
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			logger.error(
					"setOtherFields(String, Connection, String, int, String)",
					e);

			throw e;
		} finally {
			DBUtil.close(stmt);
		}
		logger.trace("End :(Class : com.mind.docm.dao.ViewManupulationDao | Method : setOtherFields)");

	}

	/**
	 * fetchInsertedData - This method helps picking up the data inserted.
	 * 
	 * @param request
	 *            - This is HttpServletRequest parameter.
	 * @param ViewManupulationBean
	 *            - FormBean object.
	 * @return Returns None.
	 **/
	public void fetchInsertedData(Map<String, Object> map,
			ViewManupulationDTO bean) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : fetchInsertedData)");
		Statement stmt = null;
		ResultSet rs = null;
		List outerList = null;
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		try {

			// System.out.println("libid----"+libraryId);
			// helps in getting data for the selected view.
			String sql = "";
			sql = "select dmfl.*,dlvd.*,dtg.dm_tgrp_desc from dbo.dm_metadata_fields_list dmfl join dbo.dm_library_view_detail dlvd on"
					+ " \n dmfl.dm_md_fl_id = dlvd.dm_lib_vw_dtl_md_fl_id"
					+ " \n and dm_lib_vw_dtl_lib_vw_id ="
					+ bean.getViewId()
					+ " left outer join dbo.dm_taxonomy_group dtg"
					+ " \n on dmfl.dm_md_fl_tgrp_id = dtg.dm_tgrp_id";

			// System.out.println(sql);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			outerList = new ArrayList();
			while (rs.next()) {
				List innerList = new ArrayList();
				innerList.add(rs.getString("dm_md_fl_id")); // 0
				innerList.add(rs.getString("dm_md_fl_desc")); // 1
				innerList.add(rs.getString("dm_md_fl_nm")); // 2
				innerList.add(rs.getString("dm_md_fl_desc")); // 3
				innerList.add(rs.getString("dm_md_fl_type_id")); // 4
				innerList.add(rs.getString("dm_md_fl_dval")); // 5
				innerList.add(rs.getString("dm_tgrp_desc")); // 6
				innerList.add(rs.getString("dm_md_fl_nm")); // 7
				innerList.add(rs.getString("dm_md_fl_tgrp_id")); // 8
				innerList.add(rs.getString("dm_md_fl_type_id")); // 9
				innerList.add(rs.getString("dm_md_fl_id") + "~"
						+ rs.getString("dm_md_fl_type_id")); // 10
				innerList.add(rs.getString("dm_lib_vw_dtl_fl_show"));// 11
				outerList.add(innerList);
			}
			map.put("columnList", outerList);
			map.put("filterColumnList", outerList);
			sql = "select dmfl.*,dlvd.*,dtg.dm_tgrp_desc from dbo.dm_metadata_fields_list dmfl join dbo.dm_library_view_detail dlvd on"
					+ " \n dmfl.dm_md_fl_id = dlvd.dm_lib_vw_dtl_md_fl_id"
					+ " \n and dm_lib_vw_dtl_lib_vw_id ="
					+ bean.getViewId()
					+ " left outer join dbo.dm_taxonomy_group dtg"
					+ " \n on dmfl.dm_md_fl_tgrp_id = dtg.dm_tgrp_id"
					+ " \n order by dlvd.dm_lib_vw_dtl_seq_no";
			rs = stmt.executeQuery(sql);
			List selectColumnlist = new ArrayList();
			while (rs.next()) {
				List innerList = new ArrayList();
				innerList.add(rs.getString("dm_lib_vw_dtl_md_fl_id")); // 0
				innerList.add(rs.getString("dm_md_fl_desc")); // 1
				innerList.add(rs.getString("dm_lib_vw_dtl_fl_show"));// 2
				selectColumnlist.add(innerList);
			}
			map.put("selectedColumList", selectColumnlist);
			map.put("filterFieldSize",
					getFieldSize("dbo.dm_library_view_filter",
							"dm_lib_vw_flt_fld_value", conn));
			map.put("viewFieldSize",
					getFieldSize("dbo.dm_library_view_master", "dm_lib_vw_nm",
							conn));

			// helps in getting data for other fields for the selected view.
			sql = "select dlvd.dm_lib_vw_dtl_id from dbo.dm_library_view_detail dlvd where dlvd.dm_lib_vw_dtl_sort_ord!=0 and dlvd.dm_lib_vw_dtl_lib_vw_id ="
					+ bean.getViewId();
			String detailIdForSorColumn[] = getDataForOtherFields(conn, sql);
			bean.setDetailIdForSortColumn(detailIdForSorColumn);
			sql = "select dlvd.dm_lib_vw_dtl_md_fl_id from dbo.dm_library_view_detail dlvd where dlvd.dm_lib_vw_dtl_sort_ord!=0 and dlvd.dm_lib_vw_dtl_lib_vw_id ="
					+ bean.getViewId();
			String sortColumn[] = getDataForOtherFields(conn, sql);
			bean.setSelectedSortColumns(sortColumn);
			sql = "select dlvd.dm_lib_vw_dtl_sort_ord from dbo.dm_library_view_detail dlvd where dlvd.dm_lib_vw_dtl_sort_ord!=0 and dlvd.dm_lib_vw_dtl_lib_vw_id ="
					+ bean.getViewId();
			String sortColumnDir[] = getDataForOtherFields(conn, sql);
			bean.setSortDirrection(sortColumnDir);
			sql = "select dlvd.dm_lib_vw_dtl_id from dbo.dm_library_view_detail dlvd where dlvd.dm_lib_vw_dtl_grp_ord!=0 and dlvd.dm_lib_vw_dtl_lib_vw_id ="
					+ bean.getViewId() + " order by dlvd.dm_lib_vw_dtl_grp_ord";
			String detailIdForGrpColumn[] = getDataForOtherFields(conn, sql);
			bean.setDetailIdForGroupColumn(detailIdForGrpColumn);
			sql = "select dlvd.dm_lib_vw_dtl_md_fl_id from dbo.dm_library_view_detail dlvd where dlvd.dm_lib_vw_dtl_grp_ord!=0 and dlvd.dm_lib_vw_dtl_lib_vw_id ="
					+ bean.getViewId() + " order by dlvd.dm_lib_vw_dtl_grp_ord";
			String grpColumn[] = getDataForOtherFields(conn, sql);
			bean.setSelectedGroupColumns(grpColumn);
			sql = "select dlvf.dm_lib_vw_flt_id from dbo.dm_library_view_filter dlvf where  dlvf.dm_lib_vw_flt_lib_vw_id ="
					+ bean.getViewId();
			String filterId[] = getDataForOtherFields(conn, sql);
			bean.setFilterId(filterId);

			sql = "select isnull(convert(varchar,dm_lib_vw_flt_fld_id)+'~'+convert(varchar,dm_md_fl_type_id),0) as dm_lib_vw_flt_fld_id "
					+ " \n from dbo.dm_library_view_filter left outer join dbo.dm_metadata_fields_list on "
					+ " \n dm_lib_vw_flt_fld_id = dm_md_fl_id "
					+ " \n where  dm_lib_vw_flt_lib_vw_id =" + bean.getViewId();
			String filterColumn[] = getDataForOtherFields(conn, sql);
			bean.setSelectedFilterColumns(filterColumn);
			sql = "select  isnull(dm_md_fl_type_id,0)"
					+ " \n from dbo.dm_library_view_filter left outer join dbo.dm_metadata_fields_list on "
					+ " \n dm_lib_vw_flt_fld_id = dm_md_fl_id "
					+ " \n where  dm_lib_vw_flt_lib_vw_id =" + bean.getViewId();
			String fieldTypeId[] = getDataForOtherFields(conn, sql);
			bean.setFieldTypeId(fieldTypeId);
			sql = "select dlvf.dm_lib_vw_flt_pre_cndn"
					+ " \n from dbo.dm_library_view_filter dlvf where  dlvf.dm_lib_vw_flt_lib_vw_id ="
					+ bean.getViewId();
			String preCon[] = getDataForOtherFields(conn, sql);
			bean.setFilterCondition(preCon);
			sql = "select dlvf.dm_lib_vw_flt_comparator"
					+ " \n from dbo.dm_library_view_filter dlvf where  dlvf.dm_lib_vw_flt_lib_vw_id ="
					+ bean.getViewId();
			String comp[] = getDataForOtherFields(conn, sql);
			bean.setLogicalCondition(comp);
			sql = "select dm_lib_vw_flt_fld_value "
					+ " \n from dbo.dm_library_view_filter dlvf where  dlvf.dm_lib_vw_flt_lib_vw_id ="
					+ bean.getViewId();
			String match[] = getDataForOtherFields(conn, sql);
			bean.setFilterType(match);
		}

		catch (SQLException sqle) {
			logger.error(
					"fetchInsertedData(HttpServletRequest, ViewManupulationBean)",
					sqle);

			logger.error(sqle.getMessage());
		} catch (Exception sqle) {
			logger.error(
					"fetchInsertedData(HttpServletRequest, ViewManupulationBean)",
					sqle);
			logger.error(sqle.getMessage());
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		logger.trace("End :(Class : com.mind.docm.dao.ViewManupulationDao | Method : fetchInsertedData)");

	}

	/**
	 * getDataForOtherFields - General method picking up the data for all the
	 * fields.
	 * 
	 * @param Connection
	 *            - Connection Object.
	 * @param sql
	 *            - Sql Query.
	 * @return Returns String Array.
	 **/
	public static String[] getDataForOtherFields(Connection conn, String sql)
			throws SQLException {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : getDataForOtherFields)");
		Statement stmt = null;
		ResultSet rs = null;
		String[] returnArray = null;
		try {
			// System.out.println(sql);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			List innerList = new ArrayList();
			while (rs.next()) {
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					innerList.add(rs.getString(rs.getMetaData()
							.getColumnName(i))); // 0
				}

			}
			if (innerList.size() > 0) {
				returnArray = new String[innerList.size()];
				for (int i = 0; i < returnArray.length; i++) {
					if (((String) innerList.get(i)).equals(">"))
						returnArray[i] = "greaterThan";
					else if (((String) innerList.get(i)).equals("<"))
						returnArray[i] = "lessThan";
					else
						returnArray[i] = (String) innerList.get(i);
					// System.out.println("return val"+returnArray[i]);
				}
			}

		} catch (Exception e) {
			logger.error("getDataForOtherFields(Connection, String)", e);
			logger.error(e.getMessage());
		} finally {
			DBUtil.close(rs, stmt);

		}
		logger.trace("End :(Class : com.mind.docm.dao.ViewManupulationDao | Method : getDataForOtherFields)");
		return returnArray;
	}

	/**
	 * getPreCondition - This method helps in picking condition.
	 * 
	 * @param String
	 *            - given condition.
	 * @return Returns String.
	 **/
	public String getPreCondition(String cond) {
		if (cond.trim().equalsIgnoreCase("greaterThan"))
			return ">";
		if (cond.trim().equalsIgnoreCase("lessThan"))
			return "<";
		return null;

	}

	/**
	 * deleteData - This method helps in deleting data for the view.
	 * 
	 * @param request
	 *            - This is HttpServletRequest parameter.
	 * @param ViewManupulationBean
	 *            - FormBean object.
	 * @return Returns None.
	 **/
	public void deleteData(Map<String, Object> map, ViewManupulationDTO bean) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : deleteData)");
		Connection conn = null;
		Statement stmt = null;
		try {
			// helps in deleting view from libraray.
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			stmt = conn.createStatement();
			String deleteQuery1 = "delete from dbo.dm_library_view_filter where dm_lib_vw_flt_lib_vw_id ="
					+ bean.getViewId();
			stmt.addBatch(deleteQuery1);
			String deleteQuery2 = "delete from dbo.dm_library_view_detail where dm_lib_vw_dtl_lib_vw_id ="
					+ bean.getViewId();
			stmt.addBatch(deleteQuery2);
			String deleteQuery3 = "delete from dbo.dm_library_view_master where dm_lib_vw_id ="
					+ bean.getViewId()
					+ " and dm_lib_vw_lib_id="
					+ bean.getLibId();
			stmt.addBatch(deleteQuery3);
			stmt.executeBatch();
			map.put("delete", "delete");
		} catch (SQLException sqle) {
			logger.error(
					"deleteData(HttpServletRequest, ViewManupulationBean)",
					sqle);
			logger.error(sqle.getMessage());
		} catch (Exception e) {
			logger.error(
					"deleteData(HttpServletRequest, ViewManupulationBean)", e);
			logger.error(e.getMessage());
		} finally {
			DBUtil.close(stmt);
			DBUtil.close(conn);
		}
		logger.trace("End :(Class : com.mind.docm.dao.ViewManupulationDao | Method : deleteData)");
	}

	/**
	 * isViewExist - This method checks whether view with that name already
	 * exists or not.
	 * 
	 * @param libId
	 *            - Library ID.
	 * @param viewName
	 *            - view Name.
	 * @param viewId
	 *            - viewId.
	 * @param function
	 *            - this tells we are in which mode add or update.
	 * @return Returns boolean.
	 **/
	public boolean isViewExist(String libId, String viewName, String viewId,
			String function) throws SQLException, Exception {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : isViewExist)");
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		String sql = "";
		try {
			// helps in checking whether this view already exists or not.
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);

			if (function.equalsIgnoreCase("update")) {
				sql = "select * from dbo.dm_library_view_master where upper(dm_lib_vw_nm) = ? and dm_lib_vw_lib_id = ? and dm_lib_vw_id! = ?";
				stmt = conn.prepareStatement(sql);
				stmt.setString(1, viewName);
				stmt.setInt(2, new Integer(libId).intValue());
				stmt.setInt(3, new Integer(viewId).intValue());
			} else {
				sql = "select * from dbo.dm_library_view_master where upper(dm_lib_vw_nm) = ? and dm_lib_vw_lib_id = ?";
				stmt = conn.prepareStatement(sql);
				stmt.setString(1, viewName);
				stmt.setInt(2, new Integer(libId).intValue());
			}
			rs = stmt.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (Exception e) {
			logger.error("isViewExist(String, String, String, String)", e);
			logger.error(e.getMessage());
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
		logger.trace("End :(Class : com.mind.docm.dao.ViewManupulationDao | Method : isViewExist)");
		return false;
	}

	/**
	 * Gets the field size.
	 * 
	 * @param tableName
	 *            the table name
	 * @param columnName
	 *            the column name
	 * @param conn
	 *            the conn
	 * 
	 * @return the field size
	 */
	public String getFieldSize(String tableName, String columnName,
			Connection conn) {
		Statement stmt = null;
		ResultSet rs = null;
		String columnWidth = "";
		try {
			// helps in getting information for size of the fields.
			stmt = conn.createStatement();
			String sql = "select top 1 " + columnName + " from " + tableName
					+ "";

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				columnWidth = String.valueOf(rs.getMetaData()
						.getColumnDisplaySize(1));
			}

		} catch (Exception e) {
			logger.error("getFieldSize(String, String, Connection)", e);
		} finally {
			DBUtil.close(rs, stmt);

		}
		return columnWidth;
	}

}
