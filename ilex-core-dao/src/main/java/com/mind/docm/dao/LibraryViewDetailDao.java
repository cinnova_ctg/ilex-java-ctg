package com.mind.docm.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.mind.bean.docm.LibraryView;
import com.mind.bean.docm.ViewLibraryDTO;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class LibraryViewDetailDao.
 */
public class LibraryViewDetailDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(LibraryViewDetailDao.class);

	// Use Case
	/**
	 * @name libraryViewList
	 * @purpose This method is used to get the list of all views
	 * @steps 1. create the connection object using DatabaseUtils class. 2.
	 *        create the statement object using DatabaseUtils class. 3. Call the
	 *        getRecord method of DatabaseUtils. 4. Create an Object of
	 *        ArrayList. 5. return the ArrayList object.
	 * @param libId
	 * @return ArrayList
	 * @returnDescription Returns the ArrayList Object which contains all the
	 *                    view list in the given library through
	 *                    dm_library_view_master table.
	 * @throws None
	 */
	public ArrayList libraryViewList(String libId) {
		ArrayList aList = new ArrayList();
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet result = null;
		try {
			st = conn.createStatement();
			String query = "select dm_lib_vw_id,dm_lib_vw_nm from dm_library_view_master where dm_lib_vw_lib_id="
					+ Integer.parseInt(libId)
					+ " and dm_lib_vw_nm='Default View' order by dm_lib_vw_nm";
			result = DatabaseUtils.getRecord(query, st);
			while (result.next()) {
				LibraryView viewLibrary = new LibraryView();
				viewLibrary.setLibraryViewId(result.getString(1));
				viewLibrary.setLibraryViewName(result.getString(2));
				aList.add(viewLibrary);
			}
			query = "select dm_lib_vw_id,dm_lib_vw_nm from dm_library_view_master where dm_lib_vw_lib_id="
					+ Integer.parseInt(libId)
					+ " and not dm_lib_vw_nm='Default View'  order by dm_lib_vw_nm";
			result = DatabaseUtils.getRecord(query, st);
			while (result.next()) {
				LibraryView viewLibrary = new LibraryView();
				viewLibrary.setLibraryViewId(result.getString(1));
				viewLibrary.setLibraryViewName(result.getString(2));
				aList.add(viewLibrary);
			}
		} catch (SQLException e) {
			logger.error("libraryViewList(String)", e);
		} finally {
			DBUtil.close(result, st);
			DBUtil.close(conn);

		}
		return aList;
	}

	/**
	 * Gets the default view id.
	 * 
	 * @param libId
	 *            the lib id
	 * 
	 * @return the default view id
	 */
	public long getDefaultViewId(String libId) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet result = null;
		long viewId = -1;
		try {
			st = conn.createStatement();
			String query = "select dm_lib_vw_id from dm_library_view_master where dm_lib_vw_lib_id="
					+ Integer.parseInt(libId)
					+ " and dm_lib_vw_nm='Default View' order by dm_lib_vw_nm";
			result = DatabaseUtils.getRecord(query, st);
			if (result.next()) {
				viewId = result.getLong("dm_lib_vw_id");
			}
		} catch (SQLException e) {
			logger.error("getDefaultViewId(String)", e);
		} finally {
			DBUtil.close(result, st);
			DBUtil.close(conn);

		}
		return viewId;
	}

	/**
	 * Gets the view name.
	 * 
	 * @param viewId
	 *            the view id
	 * 
	 * @return the view name
	 */
	public String getViewName(String viewId) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet result = null;
		String viewName = "";
		try {
			st = conn.createStatement();
			String query = "select dm_lib_vw_nm from dm_library_view_master where dm_lib_vw_id="
					+ Integer.parseInt(viewId);
			result = DatabaseUtils.getRecord(query, st);
			if (result.next()) {
				viewName = result.getString("dm_lib_vw_nm");
			}
		} catch (SQLException e) {
			logger.error("getViewName(String)", e);
		} finally {
			DBUtil.close(result, st);
			DBUtil.close(conn);

		}
		return viewName;
	}

	/**
	 * Sets the library detail.
	 * 
	 * @param bean
	 *            the bean
	 * @param libId
	 *            the lib id
	 */
	public void setLibraryDetail(ViewLibraryDTO bean, String libId) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet result = null;
		try {
			st = conn.createStatement();
			String query = "select * from dm_library_master where dm_lib_id="
					+ Integer.parseInt(libId);
			result = DatabaseUtils.getRecord(query, st);
			while (result.next()) {
				bean.setLibraryId(result.getString("dm_lib_id"));
				bean.setLibraryName(result.getString("dm_lib_desc"));
				bean.setNotifyEmail(result.getString("notify_email"));
				bean.setNotifiable(result.getString("can_notify"));
				bean.setDoc_identifier_fl_id(result
						.getString("doc_identifier_fld_id"));
			}
		} catch (SQLException e) {
			logger.error("setLibraryDetail(ViewLibraryDTO, String)", e);
		} finally {
			DBUtil.close(result, st);
			DBUtil.close(conn);

		}
	}
}
