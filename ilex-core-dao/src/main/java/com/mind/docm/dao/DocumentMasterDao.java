package com.mind.docm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.mind.bean.docm.DocumentMasterDTO;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class DocumentMasterDao.
 */
public class DocumentMasterDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DocumentMasterDao.class);

	/** The Constant GROUP_NAME_WHEN_NULL. */
	public static final String GROUP_NAME_WHEN_NULL = "-- NULL --";

	/** The Constant GROUP_NAME_WHEN_EMPTY. */
	public static final String GROUP_NAME_WHEN_EMPTY = "-- EMPTY --";

	/**
	 * @name getGroupOrdering
	 * @purpose This method returns the list of fields by which the records are
	 *          to be grouped by. (group by f1,f2,f3 => elements of result will
	 *          be "f1" "f2" "f3"
	 * @param libId
	 * @param viewId
	 * @return ArrayList (containing the fields by whom the grouping is to be
	 *         done)
	 */
	public static ArrayList getGroupOrdering(String libId, String viewId) {
		ArrayList groups = new ArrayList();
		Connection conn = null;
		ResultSet rs = null;
		Statement st = null;
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			st = conn.createStatement();
			String query = "select dm_md_fl_nm from dm_library_view_fields"
					+ " where dm_lib_vw_lib_id=" + Integer.parseInt(libId)
					+ " and dm_lib_vw_id=" + viewId
					+ " and dm_lib_vw_dtl_grp_ord>0"
					+ " order by dm_lib_vw_dtl_grp_ord";
			logger.trace(query);
			rs = DatabaseUtils.getRecord(query, st);
			while (rs.next()) {
				String fieldName = rs.getString("dm_md_fl_nm");
				groups.add(fieldName);
			}
		} catch (Exception e) {
			logger.error("getGroupOrdering(String, String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return groups;
	}

	public static String[] getFileByJobId(String jobId) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_FILE_SYSDB);

		String[] arr = null;
		Statement st = null;
		ResultSet rs = null;
		byte[] fileBytes = null;
		String qry = "SELECT ddf.dm_fl_id AS fileId, ddf.dm_fl_data AS fileData, ddv.dm_doc_id AS docId, "
				+ " ddv.MIME AS mime, ddv.FileName AS fileName "
				+ " from DocMFiledb.dbo.dm_document_file ddf "
				+ " LEFT JOIN DocMSysdb.dbo.dm_document_version ddv ON ddv.dm_doc_file_id = dm_fl_id "
				+ " LEFT JOIN ilex.dbo.podb_purchase_order_deliverables pod ON pod.podb_mn_dl_doc_id = ddv.dm_doc_id "
				+ " LEFT JOIN ilex.dbo.lm_purchase_order po ON po.lm_po_id = pod.lm_po_id "
				+ " LEFT JOIN ilex.dbo.lm_job lj ON lj.lm_js_id = po.lm_po_js_id  "
				+ " WHERE lj.lm_js_id = "
				+ jobId
				// +
				// " WHERE lj.lm_js_id IN (SELECT TOP 100 lm_js_id FROM ilex.dbo.lm_job ORDER BY lm_js_id DESC) "
				+ " AND pod.podb_mn_dl_title =  'Equipment Cabinet/Rack'  "
				+ " AND pod.podb_mn_dl_doc_id IS NOT NULL "
				+ " ORDER BY ddf.dm_fl_id DESC ";
		try {
			st = conn.createStatement();
			rs = DatabaseUtils.getRecord(qry, st);
			if (rs.next()) {
				arr = new String[5];
				arr[0] = rs.getString("fileId");
				fileBytes = rs.getBytes("fileData");
				arr[1] = fileBytes.toString();
				arr[2] = rs.getString("docId");
				arr[3] = rs.getString("mime");
				arr[4] = rs.getString("fileName");
			}
			logger.trace("byteFile :::::: " + fileBytes);
		} catch (SQLException e) {
			logger.error("openFile(String)", e);
		} finally {
			DBUtil.closeStatement(rs, st);
			DBUtil.closeDbConnection(conn);
		}
		return arr;
	}

	/**
	 * @name getSortFields
	 * @purpose This method returns the list of fields by which the records are
	 *          to be sorted by. (order by f1,f2,f3 => elements of result will
	 *          be "f1" "f2" "f3"
	 * @param libId
	 * @param viewId
	 * @return ArrayList (containing the fields by whom the ordering is to be
	 *         done)
	 */
	public static ArrayList getSortFields(String libId, String viewId) {
		ArrayList sortFields = new ArrayList();
		Connection conn = null;
		ResultSet rs = null;
		Statement st = null;
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			st = conn.createStatement();
			String query = "select dm_md_fl_nm from dm_library_view_fields"
					+ " where dm_lib_vw_lib_id=" + Integer.parseInt(libId)
					+ " and dm_lib_vw_id=" + viewId
					+ " and dm_lib_vw_dtl_sort_ord>0"
					+ " order by dm_lib_vw_dtl_sort_ord";
			logger.trace(query);
			rs = DatabaseUtils.getRecord(query, st);
			while (rs.next()) {
				String fieldName = rs.getString("dm_md_fl_nm");
				sortFields.add(fieldName);
			}
		} catch (Exception e) {
			logger.error("getSortFields(String, String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return sortFields;
	}

	/**
	 * @name getDocumentList
	 * @purpose This method returns the list of documents corresponding to the
	 *          group field selected and other filter criteria
	 * @param parentsFieldNameValueMap
	 * @param libId
	 * @param viewId
	 * @param findStatus
	 * @return ArrayList of the document rows (i.e.ArrayList of row
	 *         fields/values)
	 */
	public static ArrayList getDocumentList(ArrayList parentsFieldNameValueMap,
			String libId, String viewId, String findStatus) {
		return getDocumentDataList(false, null, parentsFieldNameValueMap,
				libId, viewId, findStatus);
	}

	/**
	 * @name getGroupValueList
	 * @purpose This method returns the list of distinct values corresponding to
	 *          the group field selected and other filter criteria
	 * @param groupFieldName
	 * @param parentsFieldNameValueMap
	 * @param libId
	 * @param viewId
	 * @param findStatus
	 * @return ArrayList of the group field values (i.e.ArrayList of row
	 *         fields/values)
	 */
	public static ArrayList getGroupValueList(String groupFieldName,
			ArrayList parentsFieldNameValueMap, String libId, String viewId,
			String findStatus) {
		return getDocumentDataList(true, groupFieldName,
				parentsFieldNameValueMap, libId, viewId, findStatus);
	}

	/**
	 * @name getDocumentDataList
	 * @purpose This method returns the list of documents corresponding to the
	 *          group field selected and other filter criteria Note: If
	 *          isGroupList, then the distinct values of groupFieldName are
	 *          returned. else all the documents are returned corresponding to
	 *          the group field selected and other filter criteria
	 * @param isGroupList
	 * @param groupFieldName
	 * @param parentsFieldNameValueMap
	 * @param libId
	 * @param viewId
	 * @param findStatus
	 * @return ArrayList of the document rows (i.e.ArrayList of row
	 *         fields/values)
	 */
	private static ArrayList getDocumentDataList(boolean isGroupList,
			String groupFieldName, ArrayList parentsFieldNameValueMap,
			String libId, String viewId, String findStatus) {
		ArrayList rowList = new ArrayList();
		Connection conn = null;
		ResultSet rs = null;
		Statement st = null;
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			String status = "";
			if (findStatus.equals("marked"))
				status = "'Approved ','Draft'";
			else if (findStatus.equals("approved"))
				status = "'Superseded ','Deleted'";
			st = conn.createStatement();
			ArrayList colList = new ArrayList();
			logger.trace("status" + status);

			String query = "select ";
			HashMap fieldNameToDescriptionMap = getFieldNameToDescriptionMap(
					libId, viewId);
			String groupFieldDescription = "";
			if (!isGroupList) {
				String fieldListToShowLibraryView = getFieldListToShowLibraryView(
						libId, viewId);
				// fill headings
				query += "a.dm_doc_id,a.dm_doc_file_id,a.MIME,a.FileName,a.DocumentVersion,";
				query += fieldListToShowLibraryView;
			} else {
				query += "distinct rtrim(ltrim(" + groupFieldName + ")) "
						+ groupFieldName + " ";
			}

			query += " from  dm_document_version a join dm_document_master b "
					+ " on a.dm_doc_id=b.dm_doc_id "
					+ " where b.dm_doc_lib_id=" + Integer.parseInt(libId)
					+ " and a.status not in (" + status + ") ";

			String filterCriteriaBasedOnGroups = getFilterCriteriaBasedOnGroups(parentsFieldNameValueMap);
			query += filterCriteriaBasedOnGroups;

			String filterCriteriaBasedOnView = getFilterCriteriaBasedOnView(
					libId, viewId);
			query += filterCriteriaBasedOnView;

			if (!isGroupList) {
				String sortCriteria = getSortCriteria(libId, viewId);
				query += sortCriteria;
			}

			logger.trace(query);

			if (logger.isDebugEnabled()) {
				logger.debug("getDocumentDataList(boolean, String, ArrayList, String, String, String) - "
						+ query);
			}

			rs = DatabaseUtils.getRecord(query, st);
			ResultSetMetaData rsm = rs.getMetaData();
			int countCol = rsm.getColumnCount();
			for (int i = 1; i <= countCol; i++) {
				colList.add(rsm.getColumnName(i));
			}
			if (isGroupList) {
				colList.add("Field Description");
				groupFieldDescription = rsm.getColumnName(1).toLowerCase();
			}
			String fieldDescription = (String) fieldNameToDescriptionMap
					.get(groupFieldDescription);
			rowList.add(colList);
			while (rs.next()) {
				colList = new ArrayList();
				for (int i = 1; i <= countCol; i++) {
					String value = rs.getString(i);

					// if this is a group then the null value or empty value
					// should be treated as EMPTY/NULL
					if (isGroupList && value == null) {
						value = GROUP_NAME_WHEN_NULL;
					} else if (isGroupList && value != null
							&& value.trim().equals("")) {
						value = GROUP_NAME_WHEN_EMPTY;
					}

					colList.add(value);
				}
				if (isGroupList) {
					colList.add(fieldDescription);
				}
				rowList.add(colList);
			}
		} catch (SQLException e) {
			logger.error(
					"getDocumentDataList(boolean, String, ArrayList, String, String, String)",
					e);

			logger.error(
					"getDocumentDataList(boolean, String, ArrayList, String, String, String)",
					e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return rowList;
	}

	/**
	 * Gets the sort criteria.
	 * 
	 * @param libId
	 *            the lib id
	 * @param viewId
	 *            the view id
	 * 
	 * @return the sort criteria
	 */
	private static String getSortCriteria(String libId, String viewId) {
		String sortCriteria = "";
		ArrayList sortFields = getSortFields(libId, viewId);
		Iterator sortFieldsIter = sortFields.iterator();
		int sortFieldCount = 0;
		while (sortFieldsIter.hasNext()) {
			if (sortFieldCount == 0) {
				sortCriteria += " order by ";
			} else {
				sortCriteria += ", ";
			}
			sortCriteria += (String) sortFieldsIter.next();
			sortFieldCount++;
		}
		return sortCriteria;
	}

	/**
	 * Gets the filter criteria based on groups.
	 * 
	 * @param parentsFieldNameValueMap
	 *            the parents field name value map
	 * 
	 * @return the filter criteria based on groups
	 */
	private static String getFilterCriteriaBasedOnGroups(
			ArrayList parentsFieldNameValueMap) {
		String filterCriteriaBasedOnGroups = "";
		// add the filter condition based on the groupings being done
		if (parentsFieldNameValueMap != null
				&& parentsFieldNameValueMap.size() > 0) {
			Iterator mapIter = parentsFieldNameValueMap.iterator();
			while (mapIter.hasNext()) {
				ArrayList inner = (ArrayList) mapIter.next();
				String fieldName = (String) inner.get(0);
				String fieldValue = (String) inner.get(1);
				if (!fieldName.equals("Documents")) {
					if (!fieldValue.equalsIgnoreCase(GROUP_NAME_WHEN_NULL)
							&& !fieldValue
									.equalsIgnoreCase(GROUP_NAME_WHEN_EMPTY)) {
						fieldValue.replaceAll("'", "''");
						filterCriteriaBasedOnGroups += " and " + fieldName
								+ "='" + fieldValue.replaceAll("'", "''")
								+ "' ";
					} else {
						fieldValue.replaceAll("'", "''");
						if (fieldValue.equalsIgnoreCase(GROUP_NAME_WHEN_NULL)) {
							filterCriteriaBasedOnGroups += " and " + fieldName
									+ " is null ";
						} else if (fieldValue
								.equalsIgnoreCase(GROUP_NAME_WHEN_EMPTY)) {
							filterCriteriaBasedOnGroups += " and rtrim(ltrim("
									+ fieldName + "))='' ";
						}
					}
				} else {
					// skip
				}
			}
		}
		return filterCriteriaBasedOnGroups;
	}

	/**
	 * Document master list.
	 * 
	 * @param libId
	 *            the lib id
	 * @param viewId
	 *            the view id
	 * @param findStatus
	 *            the find status
	 * 
	 * @return the array list
	 */
	public static ArrayList documentMasterList(String libId, String viewId,
			String findStatus) {
		return getDocumentList(null, libId, viewId, findStatus);
	}

	/**
	 * Open file.
	 * 
	 * @param fileId
	 *            the file id
	 * 
	 * @return the byte[]
	 */
	public static byte[] openFile(String fileId) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_FILE_SYSDB);
		Statement st = null;
		ResultSet rs = null;
		byte[] fileBytes = null;
		try {
			st = conn.createStatement();
			rs = DatabaseUtils.getRecord(
					"select dm_fl_data from dm_document_file where dm_fl_id="
							+ Integer.parseInt(fileId), st);
			if (rs.next()) {
				fileBytes = rs.getBytes(1);
			}
			logger.trace("byteFile :::::: " + fileBytes);
		} catch (SQLException e) {
			logger.error("openFile(String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return fileBytes;
	}

	/**
	 * getFilterCriteriaBasedOnView - This method helps in making the filter
	 * condition for the view.
	 * 
	 * @param libId
	 *            - Library id.
	 * @param viewId
	 *            - view Id.
	 * @return Returns String condition.
	 **/
	public static String getFilterCriteriaBasedOnView(String libId,
			String viewId) {
		// Logging.trace("NOT USING ANY FILTER CRITERIA BASED ON VIEW ID! NEED TO FILL THE METHOD!");
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		StringBuffer filterCond = new StringBuffer();
		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			logger.trace("view Id" + viewId);
			String sql = "select * from dbo.dm_library_view_filter left outer join dbo.dm_metadata_fields_list on "
					+ " \n dm_lib_vw_flt_fld_id = dm_md_fl_id "
					+ " \n left outer join dbo.dm_library_view_detail on "
					+ " \n dm_lib_vw_flt_lib_vw_id = dm_lib_vw_dtl_lib_vw_id and "
					+ " \n dm_lib_vw_flt_fld_id =  dm_lib_vw_dtl_md_fl_id "
					+ " \n where dm_lib_vw_flt_lib_vw_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, new Integer(viewId).intValue());
			rs = pStmt.executeQuery();
			String fieldText = null;
			String dateCheck = "";
			String fieldName = "";
			filterCond.append(" and (");
			while (rs.next()) {
				if (!rs.getString("dm_lib_vw_flt_fld_id").trim()
						.equalsIgnoreCase("0")) {
					if (rs.getString("dm_lib_vw_flt_comparator") != null
							&& !rs.getString("dm_lib_vw_flt_comparator").trim()
									.equalsIgnoreCase("")) {
						filterCond.append(" ");
						filterCond.append(rs
								.getString("dm_lib_vw_flt_comparator"));
						filterCond.append(" ");
					}
					filterCond.append("(");
					if (rs.getString("dm_md_fl_type_id").trim().equals("4")
							&& rs.getString("dm_lib_vw_flt_pre_cndn").trim()
									.equalsIgnoreCase("="))
						dateCheck = rs.getString("dm_md_fl_nm");
					fieldName = (dateCheck.trim().equalsIgnoreCase("")) ? rs
							.getString("dm_md_fl_nm") : "convert(varchar(10),"
							+ dateCheck + ",101)";
					filterCond.append(fieldName);
					filterCond.append(" ");
					filterCond.append(rs.getString("dm_lib_vw_flt_pre_cndn"));
					filterCond.append(" ");
					fieldText = DocMSeacrhDao.getFiledText(
							rs.getString("dm_lib_vw_flt_pre_cndn"),
							rs.getString("dm_lib_vw_flt_fld_value"));
					filterCond.append(fieldText);
					filterCond.append(")");
				}
			}
			filterCond.append(")");
			logger.trace("filterCond-------- " + filterCond);
			if (filterCond.toString().equals(" and ()")) {
				logger.trace("in if -------" + filterCond);
				return "";
			}

			logger.trace("filterCond" + filterCond);
		} catch (Exception e) {
			logger.error("getFilterCriteriaBasedOnView(String, String)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return filterCond.toString();
	}

	/**
	 * Gets the field list to show library view.
	 * 
	 * @param libId
	 *            the lib id
	 * @param viewId
	 *            the view id
	 * 
	 * @return the field list to show library view
	 */
	public static String getFieldListToShowLibraryView(String libId,
			String viewId) {
		String returnSQL = "";

		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		ResultSet rs = null;
		Statement st = null;
		String query = "select dm_md_fl_nm,dm_md_fl_desc from dm_library_view_fields"
				+ " where dm_lib_vw_lib_id="
				+ Integer.parseInt(libId)
				+ " and dm_lib_vw_id="
				+ viewId
				+ " and dm_lib_vw_dtl_fl_show='Y'"
				+ " order by "
				+ " case when dm_lib_vw_dtl_seq_no=0 then " + "100 " // a
																		// default
																		// max
																		// value
																		// put
																		// so
																		// that
																		// these
																		// fields
																		// come
																		// in
																		// last
				+ "else dm_lib_vw_dtl_seq_no end ";
		logger.trace(query);
		try {
			st = conn.createStatement();

			rs = DatabaseUtils.getRecord(query, st);
			boolean isFirst = true;
			while (rs.next()) {
				if (!isFirst) {
					returnSQL += ", ";
				}
				String descr = rs.getString("dm_md_fl_desc");
				descr.replaceAll("'", "''");

				String fieldName = rs.getString("dm_md_fl_nm");
				returnSQL += fieldName + " '" + descr + "'";
				isFirst = false;
			}
		} catch (Exception e) {
			logger.error("getFieldListToShowLibraryView(String, String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);

		}
		logger.trace(returnSQL);
		return returnSQL;
	}

	/**
	 * Gets the field name to description map.
	 * 
	 * @return the field name to description map
	 */
	public static TreeMap getFieldNameToDescriptionMap() {
		TreeMap map = new TreeMap();
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		ResultSet rs = null;
		Statement st = null;
		String query = "select dm_md_fl_nm,dm_md_fl_desc from dm_metadata_fields_list order by dm_md_fl_id";
		logger.trace(query);
		try {
			st = conn.createStatement();

			rs = DatabaseUtils.getRecord(query, st);
			boolean isFirst = true;
			while (rs.next()) {
				String descr = rs.getString("dm_md_fl_desc");

				String fieldName = rs.getString("dm_md_fl_nm");
				map.put(fieldName.toLowerCase(), descr);
			}
		} catch (Exception e) {
			logger.error("getFieldNameToDescriptionMap()", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return map;
	}

	/**
	 * Gets the field name to description query.
	 * 
	 * @return the field name to description query
	 */
	public static String getFieldNameToDescriptionQuery() {
		return getFieldNameToDescriptionQuery(null, true);
	}

	/**
	 * Gets the field name to description query.
	 * 
	 * @param limitedFields
	 *            the limited fields
	 * @param showAllColumns
	 *            the show all columns
	 * 
	 * @return the field name to description query
	 */
	public static String getFieldNameToDescriptionQuery(String[] limitedFields,
			boolean showAllColumns) {

		Connection conn = null;
		ResultSet rs = null;
		Statement st = null;
		String query = "select dm_md_fl_id,dm_md_fl_nm,dm_md_fl_desc from dm_metadata_fields_list order by dm_md_fl_id";
		StringBuffer fieldNames = new StringBuffer();

		try {
			conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
			st = conn.createStatement();

			rs = DatabaseUtils.getRecord(query, st);
			while (rs.next()) {
				String field = rs.getString("dm_md_fl_nm");
				boolean addThisField = true;
				if (limitedFields != null && !showAllColumns) {
					String fieldId = rs.getString("dm_md_fl_id");

					boolean found = false;
					for (int i = 0; i < limitedFields.length; i++) {
						if (limitedFields[i] != null) {
							String[] fieldIdAndType = limitedFields[i]
									.split("~");
							if (fieldIdAndType != null
									&& fieldIdAndType[0] != null
									&& fieldIdAndType[0]
											.equalsIgnoreCase(fieldId)) {
								found = true;
								break;
							}
						}
					}
					addThisField = found;
				}
				if (addThisField) {
					fieldNames.append(" , "
							+ " "
							+ field
							+ " '"
							+ rs.getString("dm_md_fl_desc").replaceAll("'",
									"''") + "' ");
				}
			}
		} catch (Exception e) {
			logger.error("getFieldNameToDescriptionQuery(String[], boolean)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return fieldNames.toString();
	}

	/**
	 * Gets the field name to description map.
	 * 
	 * @param libId
	 *            the lib id
	 * @param viewId
	 *            the view id
	 * 
	 * @return the field name to description map
	 */
	public static HashMap getFieldNameToDescriptionMap(String libId,
			String viewId) {
		HashMap map = new HashMap();
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		ResultSet rs = null;
		Statement st = null;
		String query = "select dm_md_fl_nm,dm_md_fl_desc from dm_library_view_fields";
		if (libId != null && viewId != null) {
			query += " where dm_lib_vw_lib_id=" + Integer.parseInt(libId)
					+ " and dm_lib_vw_id=" + viewId;
		}
		logger.trace(query);
		try {
			st = conn.createStatement();
			rs = DatabaseUtils.getRecord(query, st);
			boolean isFirst = true;
			while (rs.next()) {
				String descr = rs.getString("dm_md_fl_desc");

				String fieldName = rs.getString("dm_md_fl_nm");
				map.put(fieldName.toLowerCase(), descr);
			}
		} catch (Exception e) {
			logger.error("getFieldNameToDescriptionMap(String, String)", e);

			logger.error("getFieldNameToDescriptionMap(String, String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return map;
	}

	// Use Case
	/**
	 * @name getLibraryName
	 * @purpose This method is used to get the library name corresponding to the
	 *          library id.
	 * @steps 1. This method is called internally from the DocM methods where
	 *        the library name is required.
	 * @param libId
	 * @return String
	 * @returnDescription The name of the library.
	 */

	public ArrayList documentListForView(String libId, String viewId) {
		ArrayList al = new ArrayList();
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		String viewQuery = "select  ";
		String query = "select * from dm_library_view_fields "
				+ " where dm_lib_vw_lib_id=" + Integer.parseInt(libId)
				+ " and dm_lib_vw_id=" + Integer.parseInt(viewId)
				+ " order by dm_lib_vw_dtl_seq_no";
		logger.trace(query);

		try {
			st = conn.createStatement();
			ResultSet rs = DatabaseUtils.getRecord(query, st);
			ResultSetMetaData rsm = rs.getMetaData();
			int colCount = rsm.getColumnCount();
			while (rs.next()) {
				for (int i = 1; i <= colCount; i++) {
					viewQuery += ((rs.getString(6).equals("Y")) ? ((i == 1 ? ""
							: ",") + rs.getString(2)) : "")
							+ " from dm_document_version "
							+ (rs.getString(10) != null ? " where "
									+ rs.getString(10) : "")
							+ (!rs.getString(8).equals("0") ? " order by "
									+ (i == 1 ? "" : ",") + rs.getString(2)
									: "")
							+ (!rs.getString(9).equals("0") ? " group by "
									+ (i == 1 ? "" : ",") + rs.getString(2)
									: "");
				}
			}
		} catch (SQLException e) {
			logger.error("documentListForView(String, String)", e);
		}
		return al;
	}

	/**
	 * Document group for view.
	 * 
	 * @param libId
	 *            the lib id
	 * @param viewId
	 *            the view id
	 * 
	 * @return the array list
	 */
	public ArrayList documentGroupForView(int libId, int viewId) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ArrayList groupNm = new ArrayList();
		String query = "select * from dm_library_view_fields "
				+ " where dm_lib_vw_lib_id=" + libId + " and dm_lib_vw_id="
				+ viewId + "where dm_lib_vw_dtl_grp_ord!=0"
				+ " order by dm_lib_vw_dtl_grp_ord";
		logger.trace(query);

		try {
			st = conn.createStatement();
			ResultSet rs = DatabaseUtils.getRecord(query, st);
			ResultSetMetaData rsm = rs.getMetaData();
			int colCount = rsm.getColumnCount();
			while (rs.next()) {
				groupNm.add(rs.getString(2));
			}
		} catch (SQLException e) {
			logger.error("documentGroupForView(int, int)", e);
		}
		return groupNm;
	}

	/**
	 * Document group data.
	 * 
	 * @param al
	 *            the al
	 * @param libId
	 *            the lib id
	 * 
	 * @return the array list
	 */
	public ArrayList documentGroupData(ArrayList al, int libId) {
		ArrayList mainGroup = new ArrayList();
		ArrayList condition = new ArrayList();
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		for (int i = 0; i < al.size(); i++) {
			String query = "select "
					+ (String) al.get(i)
					+ ",dm_doc_id from d_document_version where dm_doc_id in "
					+ "(select dm_doc_id from dm_document_master where dm_doc_lib_id="
					+ libId;
			for (int j = 0; j < condition.size(); j++) {
				query += " and " + condition.get(j);
			}
			query += " order by " + (String) al.get(i);

			try {
				st = conn.createStatement();
				ResultSet rs = DatabaseUtils.getRecord(query, st);
				ResultSetMetaData rsm = rs.getMetaData();
				int colCount = rsm.getColumnCount();
				while (rs.next()) {
					ArrayList group = new ArrayList();
					group.add(rs.getString(1));
					group.add(rs.getString(2));
					mainGroup.add(group);
				}
			} catch (SQLException e) {
				logger.error("documentGroupData(ArrayList, int)", e);
			}
		}
		return mainGroup;
	}

	/**
	 * Gets the file data.
	 * 
	 * @param fileId
	 *            the file id
	 * @param formbean
	 *            the formbean
	 * 
	 * @return the file data
	 */
	public void getFileData(String fileId, DocumentMasterDTO formbean) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet rs = null;
		String query = "select distinct isnull(filename,''),ISNULL(MIME,'pdf'),isnull(application,'') 'application',"
				+ " isnull(SIZE,'0') 'size' from dm_document_version where dm_doc_file_id = "
				+ fileId;
		logger.trace(query);
		String fileName = null;
		try {
			st = conn.createStatement();
			rs = DatabaseUtils.getRecord(query, st);
			if (rs.next()) {
				formbean.setFilename(rs.getString(1));
				formbean.setMIME(rs.getString(2));
				formbean.setApplication(rs.getString(3));
				formbean.setSize(rs.getString(4));
			}
		} catch (Exception e) {
			logger.error("getFileData(String, DocumentMasterBean)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
	}

	// ////////////get data for view file for accept
	public static String getFileDataForAcceptCase(String fileId) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet rs = null;
		String query = "select distinct isnull(filename,''),ISNULL(MIME,'pdf')"
				+ "  from dm_document_version where dm_doc_file_id = " + fileId;
		logger.trace(query);
		String fileName = null;
		try {
			st = conn.createStatement();
			rs = DatabaseUtils.getRecord(query, st);
			if (rs.next()) {
				// formbean.setFilename(rs.getString(1));
				fileName = rs.getString(2);

			}
		} catch (Exception e) {
			logger.error("getFileData(String, DocumentMasterBean)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return fileName;
	}

}
