package com.mind.docm.dao;

import java.util.ArrayList;

import com.mind.bean.docm.Field;
import com.mind.bean.docm.IlexEntity;
import com.mind.docm.util.DocumentUtility;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.ilex.docmanager.dao.EntityDescriptionDao;

public class Deliverables {
	public static Field[] getIlexToDocMMapping(IlexEntity ilexEntity,long length, String fileName){
		ArrayList fieldsList = new ArrayList();
		Field[] fields = null;
		
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.title"),ilexEntity.getEntityName()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.documentdate"),ilexEntity.getEntityCreatedDate()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.type"),ilexEntity.getEntityType()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.status"),"Approved"));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.author"),ilexEntity.getEntityCreatedBy()));//ilexEntity.getEntityCreatedBy()
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.category"),"Contract"));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.remarks"),"Changed by Ilex"));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.source"),"Ilex"));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.adddate"),ilexEntity.getEntityChangeDate()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.adduser"),ilexEntity.getEntityChangeBy()));//ilexEntity.getEntityChangeBy()
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.addmethod"),"Implicit"));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.changecontroltype"),"1"));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.updatedocumentdate"),null));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.updatedocumentuser"),null));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.updatemethod"),null));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.previousdocumentreference"),null));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.deletedate"),null));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.deleteuser"),null));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.viewonweb"),"1"));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.archivedate"),null));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.archivefilename"),null));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.basedocument"),ilexEntity.getEntityBaseDocument()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.application"),EntityDescriptionDao.getMimeType(fileName)));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.size"),""+length));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.mime"),EntityDescriptionDao.getMimeType(fileName)));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.msaid"),ilexEntity.getEntityMSAId()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.appendixid"),ilexEntity.getEntityAppendixId()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.jobid"),ilexEntity.getEntityJobId()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.activityid"),null));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.poid"),ilexEntity.getEntityPoId()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.woid"),ilexEntity.getEntityWoId()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.invoiceid"),null));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.changecontrolid"),null));
		
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitynetmedx.id"),ilexEntity.getEntityNetMedXReportId()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("netmedx.report.flt.startdate"),ilexEntity.getReportFltStartDate()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("netmedx.report.flt.enddate"),ilexEntity.getReportFltEndDate()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("netmedx.report.flt.criticality"),ilexEntity.getReportFltCriticality()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("netmedx.report.flt.status"),ilexEntity.getReportFltReportStatus()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("netmedx.report.flt.requestor"),ilexEntity.getReportFltRequestor()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("netmedx.report.flt.arrival.options"),ilexEntity.getReportFltArrivalOptions()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("netmedx.report.flt.msp"),ilexEntity.getReportFltMsp()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("netmedx.report.flt.helpdesk"),ilexEntity.getReportFltHelpDesk()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.msaname"),ilexEntity.getMsaName()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.appendixname"),ilexEntity.getAppendixName()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.jobname"),ilexEntity.getJobName()));
		fieldsList.add(new Field(DatabaseUtilityDao.getKeyValue("ilex.docm.entitymanager.partnername"),ilexEntity.getPartnerName()));
		
		
		fields = DocumentUtility.fieldArray(fieldsList);
		return fields;
	}

}
