//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : DocumentUtility.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.docm.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.docm.Document;
import com.mind.bean.docm.Field;
import com.mind.bean.docm.MetaData;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.docm.exceptions.CouldNotAddToRepositoryException;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.CouldNotMarkDeleteException;
import com.mind.docm.exceptions.CouldNotPhysicallyDeleteException;
import com.mind.docm.exceptions.CouldNotReplaceException;
import com.mind.docm.exceptions.CouldNotSupercedeDocumentException;
import com.mind.docm.exceptions.DocMFormatException;
import com.mind.docm.exceptions.DocumentNotApprovedException;
import com.mind.docm.util.VersionSchema;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;

/**
 * The Class DocumentUtility.
 */
public class DocumentUtility {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DocumentUtility.class);

	// Use Case
	/**
	 * @name addToRepository
	 * @purpose This method is used to add of a new document to the document
	 *          repository.
	 * @steps 1. Checks from DocumentUtility if the Document is invalid. 2.
	 *        Checks the documnet is valid document or not. 3. Checks the id
	 *        fields are not exists in docuemnt. 4. Checks the compulsary fields
	 *        are exists in docuemnt for library.
	 * @param newDocument
	 * @returnDescription Returns the documentId as reference to the document
	 *                    repository location for the added document (or the
	 *                    document that got replaced)
	 * @throws CouldNotAddToRepositoryException
	 * @throws DocMFormatException
	 * @throws CouldNotCheckDocumentException
	 */
	public static int addToRepository(com.mind.bean.docm.Document newDoc,
			DataSource docmDataSource, DataSource docMFileDataSource)
			throws CouldNotAddToRepositoryException, DocMFormatException,
			CouldNotCheckDocumentException {
		logger.trace("Start :(Class : com.mind.docm.utils.DocumentUtility | Method : addToRepository)");
		logger.trace("This method is used to add a new document to the document repository.");
		int documentId = 0;
		String libValue = "";
		MetaData metaData = newDoc.getMetaData();
		Field[] field = metaData.getFields();
		for (int i = 0; i < field.length; i++) {
			if (field[i].getName().equalsIgnoreCase("Type")) {
				libValue = (field[i].getValue());
			}
		}
		documentId = documentMasterInsert(newDoc, libValue, docmDataSource,
				docMFileDataSource);
		return documentId;
	}

	public static int addToRepository(com.mind.bean.docm.Document newDoc)
			throws CouldNotAddToRepositoryException, DocMFormatException,
			CouldNotCheckDocumentException {
		return addToRepository(newDoc,
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_SYSDB),
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_FILE_SYSDB));
	}

	// Use Case
	/**
	 * @name replaceDocument
	 * @purpose This method is used to replace a document to the document
	 *          repository.
	 * @steps 1. Checks from DocumentUtility if the Document is invalid. 2.
	 *        Checks the documnet is valid document or not. 3. Checks the id
	 *        fields are not exists in docuemnt. 4. Checks the compulsary fields
	 *        are exists in docuemnt for library.
	 * @param documentId
	 * @param newDocument
	 * @returnDescription Returns the documentId as reference to the document
	 *                    repository location for the document that got replaced
	 * @throws CouldNotReplaceException
	 * @throws DocMFormatException
	 * @throws CouldNotCheckDocumentException
	 */
	public static void replaceDocument(int documentId,
			com.mind.bean.docm.Document newDoc)
			throws CouldNotReplaceException, DocMFormatException,
			CouldNotCheckDocumentException {
		logger.trace("Start :(Class : com.mind.docm.utils.DocumentUtility | Method : replaceDocument)");
		logger.trace("This method is used to replace an existing document in document repository.");
		try {
			markDocumentSuperseded(documentId);
		} catch (CouldNotSupercedeDocumentException e) {
			logger.error("replaceDocument(int, com.mind.docm.Document)", e);
		}
		documentMasterUpdate(newDoc, documentId);
	}

	// Use Case
	/**
	 * @name replaceDocument
	 * @purpose This method is used to replace a document to the document
	 *          repository.
	 * @steps 1. Checks from DocumentUtility if the Document is invalid. 2.
	 *        Checks the documnet is valid document or not. 3. Checks the id
	 *        fields are not exists in docuemnt. 4. Checks the compulsary fields
	 *        are exists in docuemnt for library.
	 * @param documentId
	 * @param newDocument
	 * @returnDescription Returns the documentId as reference to the document
	 *                    repository location for the document that got replaced
	 * @throws CouldNotReplaceException
	 * @throws DocMFormatException
	 * @throws CouldNotCheckDocumentException
	 */
	public static void replaceDocument(int documentId,
			com.mind.bean.docm.Document newDoc, String updateUser,
			DataSource docmDataSource, DataSource docMFileDataSource)
			throws CouldNotReplaceException, DocMFormatException,
			CouldNotCheckDocumentException {
		logger.trace("Start :(Class : com.mind.docm.utils.DocumentUtility | Method : replaceDocument)");
		logger.trace("This method is used to replace an existing document in document repository.");
		try {
			markDocumentSuperseded(documentId, updateUser, docmDataSource);
		} catch (CouldNotSupercedeDocumentException e) {
			logger.error("replaceDocument(int, com.mind.docm.Document)", e);
		}
		documentMasterUpdate(newDoc, documentId, docmDataSource,
				docMFileDataSource);
	}

	public static void replaceDocument(int documentId,
			com.mind.bean.docm.Document newDoc, String updateUser)
			throws CouldNotReplaceException, DocMFormatException,
			CouldNotCheckDocumentException {
		replaceDocument(documentId, newDoc, updateUser,
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_SYSDB),
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_FILE_SYSDB));
	}

	// Use Case
	/**
	 * @name markDocumentSuperceded
	 * @purpose This method is used mark document as superceded to the document
	 *          repository.
	 * @steps 1. Update the record for the given id and set the document status
	 *        as superceded.
	 * @param documentId
	 * @returnDescription None
	 * @throws CouldNotSupercedeDocumentException
	 */
	public static void markDocumentSuperseded(int documentId)
			throws CouldNotSupercedeDocumentException {
		logger.trace("Start :(Class : com.mind.docm.utils.DocumentUtility | Method : markDocumentSuperseded)");
		logger.trace("This method is used to mark document as Superseded in document repository.");
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		String newMetadataVersion = getMaxDocumentVersion(documentId, null,
				conn, false);
		String query = "update " + DocumentConstants.DOCUMENT_VERSION
				+ " set status='" + DocumentConstants.SUPERSEDED
				+ "', UpdateDocumentDate=getdate()  where dm_doc_id="
				+ documentId + " and " + " DocumentVersion='"
				+ newMetadataVersion + "'";
		logger.trace("Query for MarkDocumentSuperseded " + query);
		try {
			st = conn.createStatement();
			int status = DatabaseUtils.updateRecord(query, st);
			if (status == 0) {
				throw new CouldNotSupercedeDocumentException(
						"Can not update status as superceded.");
			}
		} catch (SQLException e) {
			logger.error("markDocumentSuperseded(int)", e);
		} finally {
			DBUtil.close(st);
			DBUtil.close(conn);
		}
	}

	// Use Case
	/**
	 * @name markDocumentSuperceded
	 * @purpose This method is used mark document as superceded to the document
	 *          repository.
	 * @steps 1. Update the record for the given id and set the document status
	 *        as superceded.
	 * @param documentId
	 * @returnDescription None
	 * @throws CouldNotSupercedeDocumentException
	 */
	public static void markDocumentSuperseded(int documentId,
			String updateUser, DataSource docmDataSource)
			throws CouldNotSupercedeDocumentException {
		logger.trace("Start :(Class : com.mind.docm.utils.DocumentUtility | Method : markDocumentSuperseded)");
		logger.trace("This method is used to mark document as Superseded in document repository.");
		Connection conn = null;
		Statement st = null;
		try {
			conn = docmDataSource.getConnection();
		} catch (SQLException e1) {
			logger.error(
					"markDocumentSuperseded(int documentId, String updateUser, DataSource docmDataSource)",
					e1);

		}
		String newMetadataVersion = getMaxDocumentVersion(documentId, null,
				conn, false);

		String query = "update " + DocumentConstants.DOCUMENT_VERSION
				+ " set status='" + DocumentConstants.SUPERSEDED
				+ "', UpdateDocumentDate=getdate(),  "
				+ "UpdateDocumentUser ='" + updateUser + "',"
				+ "UpdateMethod ='Implicit'" + "  where dm_doc_id="
				+ documentId + " and " + " DocumentVersion='"
				+ newMetadataVersion + "'";
		logger.trace("Query for MarkDocumentSuperseded " + query);
		try {
			st = conn.createStatement();
			int status = DatabaseUtils.updateRecord(query, st);
			if (status == 0) {
				throw new CouldNotSupercedeDocumentException(
						"Can not update status as superceded.");
			}
		} catch (SQLException e) {
			logger.error("markDocumentSuperseded(int)", e);
		} finally {
			DBUtil.close(st);
			DBUtil.close(conn);
		}
	}

	public static void markDocumentSuperseded(int documentId, String updateUser)
			throws CouldNotSupercedeDocumentException {
		markDocumentSuperseded(documentId, updateUser,
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_SYSDB));
	}

	// Use Case
	/**
	 * @name markDocumentApproved
	 * @purpose This method is used mark document as approved to the document
	 *          repository.
	 * @steps 1. Update the record for the given id and set the document status
	 *        as approved.
	 * @param documentId
	 * @returnDescription None
	 * @throws DocumentNotApprovedException
	 */

	public static void markDocumentApproved(int documentId)
			throws DocumentNotApprovedException {
		logger.trace("Start :(Class : com.mind.docm.utils.DocumentUtility | Method : markDocumentApproved)");
		logger.trace("This method is used to mark document as Approved in document repository.");
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		String newMetadataVersion = getMaxDocumentVersion(documentId, null,
				conn, false);
		String query = "update " + DocumentConstants.DOCUMENT_VERSION
				+ " set status='" + DocumentConstants.APPROVED
				+ "' where dm_doc_id=" + documentId + " and "
				+ " DocumentVersion='" + newMetadataVersion + "'";
		logger.trace("Query for MarkDocumentApproved " + query);
		try {
			st = conn.createStatement();
			int status = DatabaseUtils.updateRecord(query, st);
			if (status == 0) {
				throw new DocumentNotApprovedException(
						"Can not update status as approved.");
			}
		} catch (SQLException e) {
			logger.error("markDocumentApproved(int)", e);
		} finally {
			DBUtil.close(st);
			DBUtil.close(conn);

		}
	}

	// Use Case
	/**
	 * @name physicallyDeleteDocument
	 * @purpose This method is used physically delete document from document
	 *          repository.
	 * @steps 1. select fileid for document which is going to delete. 2. delete
	 *        document from dm_document_master table of DocMsYSdb. 3. delete
	 *        document from dm_document_version table of DocMsYSdb. 4. delete
	 *        file record from dm_document_file table of DocMFileSysdb.
	 * @param documentId
	 * @returnDescription None
	 * @throws CouldNotPhysicallyDeleteException
	 * @throws DocMFormatException
	 */
	public static void physicallyDeleteDocument(String documentId, String userId)
			throws CouldNotPhysicallyDeleteException, DocMFormatException {
		logger.trace("Start :(Class : com.mind.docm.utils.DocumentUtility | Method : physicallyDeleteDocument)");
		logger.trace("This method is used to physically delete document from document repository.");
		int file_id = 0;
		int result = 0;
		ResultSet resultSet = null;
		Statement stFile = null;
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Connection connFile = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_FILE_SYSDB);
		Statement st = null;
		String selectQuery = "select isnull(dm_doc_file_id,0) from "
				+ DocumentConstants.DOCUMENT_VERSION + " where dm_doc_id="
				+ Integer.parseInt(documentId);
		if (logger.isDebugEnabled()) {
			logger.debug("physicallyDeleteDocument(String, String) - delete query "
					+ selectQuery);
		}
		try {
			st = conn.createStatement();
			resultSet = DatabaseUtils.getRecord(selectQuery, st);
			while (resultSet.next()) {
				file_id = resultSet.getInt(1);
				if (file_id != 0) {
					// connFile.setAutoCommit(false);b
					String deleteFromFiledb = "Delete "
							+ DocumentConstants.DOCUMENT_FILE
							+ " where dm_fl_id=" + file_id;
					stFile = connFile.createStatement();
					result = DatabaseUtils.updateRecord(deleteFromFiledb,
							stFile);
					logger.trace("result::" + result);
				}
			}
			try {
				markAsDeleted(documentId, true, userId);
			} catch (CouldNotMarkDeleteException e) {
				logger.error("physicallyDeleteDocument(String, String)", e);
				throw new CouldNotPhysicallyDeleteException(
						"Can not delete document.");
			}
		} catch (SQLException e) {
			logger.error("physicallyDeleteDocument(String, String)", e);
			throw new CouldNotPhysicallyDeleteException(
					"Can not delete document.");
		} finally {
			DBUtil.close(st);
			DBUtil.close(stFile);
			DBUtil.close(resultSet);
			DBUtil.close(conn);
			DBUtil.close(connFile);

		}
	}

	// Use Case
	/**
	 * @name markAsDeleted
	 * @purpose This method is used mark document status as deleted in document
	 *          repository.
	 * @steps 1. select fileid for document which is going to delete. 2. update
	 *        the document status as deleted in dm_document_version table of
	 *        DocMsYSdb.
	 * @param documentId
	 * @returnDescription None
	 * @throws CouldNotMarkDeleteException
	 */
	public static void markAsDeleted(String documentId, boolean setFileIdnull,
			String userId) throws CouldNotMarkDeleteException {
		logger.trace("Start :(Class : com.mind.docm.utils.DocumentUtility | Method : markAsDeleted)");
		logger.trace("This method is used to change the status of document in document repository.");
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Connection conn1 = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		Statement st1 = null;
		String newMetadataVersion = getMaxDocumentVersion(
				Integer.parseInt(documentId), null, conn, false);
		String query = "update " + DocumentConstants.DOCUMENT_VERSION
				+ " set status='Deleted'" + ",deletedate = getdate()"
				+ ",deleteuser = '" + userId + "'"
				+ (setFileIdnull == true ? ",dm_doc_file_id=null " : "")
				+ " where dm_doc_id='" + documentId + "' and "
				+ " DocumentVersion='" + newMetadataVersion + "'";

		logger.trace(query + "update query");
		try {
			st = conn.createStatement();
			st1 = conn1.createStatement();
			if (setFileIdnull == true) {
				String updateFileId = "update "
						+ DocumentConstants.DOCUMENT_VERSION + " set  "
						+ ("dm_doc_file_id=null ") + " where dm_doc_id='"
						+ documentId + "'";
				DatabaseUtils.updateRecord(updateFileId, st1);
			}
			DatabaseUtils.updateRecord(query, st);

		} catch (SQLException e) {
			logger.error("markAsDeleted(String, boolean, String)", e);
			throw new CouldNotMarkDeleteException(
					"Can not update status as deleted.");
		} finally {
			DBUtil.close(st);
			DBUtil.close(conn);
			DBUtil.close(st1);
			DBUtil.close(conn1);
		}
	}

	// Use Case
	/**
	 * @name isDocumentExists
	 * @purpose This method is used to check the existence of document in
	 *          document repository.
	 * @steps 1. check in document master table the availability of document id.
	 *        2. get the doc_lib_id for existingDocumentId. 3. check the
	 *        document is unique across library in dm_library_metadata_setup
	 *        table.
	 * @param documentId
	 * @returnDescription String object of documentId.
	 * @throws CouldNotCheckDocumentException
	 */
	public static boolean isDocumentExists(String existingDocumentId,
			DataSource docmDataSource) throws CouldNotCheckDocumentException {
		logger.trace("Start :(Class : com.mind.docm.utils.DocumentUtility | Method : isDocumentExists)");
		logger.trace("This method is used to check the existence of document in document repository based on document id.");
		boolean isDocumentExists = false;
		Connection conn = null;
		Statement st = null;
		ResultSet resultSet = null;
		try {
			conn = docmDataSource.getConnection();
			st = conn.createStatement();
			String getLib_id = "select dm_doc_lib_id from "
					+ DocumentConstants.DOCUMENT_MASTER + " where dm_doc_id="
					+ Integer.parseInt(existingDocumentId);
			logger.trace("getLib_id" + getLib_id);
			resultSet = DatabaseUtils.getRecord(getLib_id, st);
			if (resultSet.next()) {
				isDocumentExists = true;
			}
		} catch (SQLException e) {
			logger.error("isDocumentExists(String)", e);
			throw new CouldNotCheckDocumentException(
					"No control type Found for the document id or Not valid document Id");
		} finally {
			DBUtil.close(st);
			DBUtil.close(resultSet);
			DBUtil.close(conn);
		}
		return isDocumentExists;
	}

	public static boolean isDocumentExists(String existingDocumentId)
			throws CouldNotCheckDocumentException {
		return isDocumentExists(existingDocumentId,
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_SYSDB));
	}

	// Use Case
	/**
	 * @name isDocumentExists
	 * @purpose This method is used to check the existence of document in
	 *          document repository.
	 * @steps 1. Call the getExistingDocumentId method of document utility. 2.
	 *        if the exsisting document id is not null and empty then set the
	 *        isDocumentExists variable to true.
	 * @param document
	 * @returnDescription Returns the isDocumentExists as reference of document
	 *                    exist or not.
	 * @throws CouldNotCheckDocumentException
	 */
	public static boolean isDocumentExists(com.mind.bean.docm.Document document)
			throws CouldNotCheckDocumentException {
		logger.trace("Start :(Class : com.mind.docm.utils.DocumentUtility | Method : isDocumentExists)");
		logger.trace("This method is used to check the existence of document in document repository based on document object.");
		boolean isDocumentExists = false;
		int ExistingDocumentId = 0;
		ExistingDocumentId = DocumentUtility.getExistingDocumentId(document);
		if (ExistingDocumentId != 0) {
			isDocumentExists = true;
		}
		logger.trace("isDocumentExists----------------" + isDocumentExists);
		return isDocumentExists;
	}

	public static boolean isDocumentExists(
			com.mind.bean.docm.Document document, DataSource docmDataSource)
			throws CouldNotCheckDocumentException {
		logger.trace("Start :(Class : com.mind.docm.utils.DocumentUtility | Method : isDocumentExists)");
		logger.trace("This method is used to check the existence of document in document repository based on document object.");
		boolean isDocumentExists = false;
		int ExistingDocumentId = 0;
		ExistingDocumentId = DocumentUtility.getExistingDocumentId(document,
				docmDataSource);
		if (ExistingDocumentId != 0) {
			isDocumentExists = true;
		}
		logger.trace("isDocumentExists----------------" + isDocumentExists);
		return isDocumentExists;
	}

	// Use Case
	/**
	 * @name isDocumentControlledType
	 * @purpose This method is used to check the control type of document in
	 *          document repository.
	 * @steps 1. Call the isDocumentExists method of document utility. 2. check
	 *        the existence of document. 3. If document exists then get the
	 *        changeControlledType.
	 * @param documentId
	 * @returnDescription Returns the isDocumentControlled flag as reference of
	 *                    document control or not.
	 * @throws CouldNotCheckDocumentException
	 */
	public static boolean isDocumentControlledType(String documentId)
			throws CouldNotCheckDocumentException {
		logger.trace("Start :(Class : com.mind.docm.utils.DocumentUtility | Method : isDocumentControlledType)");
		logger.trace("This method is used to get the control type of a document based on document id.");
		boolean isDocumentControlledType = false;
		int controlType = 0;
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet resultSet = null;
		boolean docExists = isDocumentExists(documentId);
		if (documentId == null)
			documentId = "";
		if (docExists == true && !documentId.equals("")) {
			String query = "select ChangeControlType from "
					+ DocumentConstants.DOCUMENT_VERSION + " where dm_doc_id="
					+ Integer.parseInt(documentId)
					+ " order by documentversion desc";
			try {
				st = conn.createStatement();
				resultSet = DatabaseUtils.getRecord(query, st);
				if (resultSet.next()) {
					controlType = resultSet.getInt(1);
				} else {
					throw new CouldNotCheckDocumentException(
							"No control type Found for the document id or Not valid document Id");
				}
			} catch (SQLException e) {
				logger.error("isDocumentControlledType(String)", e);
			} finally {
				DBUtil.close(resultSet, st);
				DBUtil.close(conn);
			}
		}
		if (controlType == 1) {
			isDocumentControlledType = true;
		}
		return isDocumentControlledType;
	}

	// Use Case
	/**
	 * @name isDocumentControlledType
	 * @purpose This method is used to check the control type of document in
	 *          document repository.
	 * @steps 1. Call the getExistingDocumentId method of document utility get
	 *        the existing document id for the document. 2. check the existence
	 *        of document. 3. If document exists then get the
	 *        changeControlledType.
	 * @param document
	 * @returnDescription Returns the isDocumentControlled flag as reference of
	 *                    document control or not.
	 * @throws CouldNotCheckDocumentException
	 */
	public static boolean isDocumentControlledType(
			com.mind.bean.docm.Document document)
			throws CouldNotCheckDocumentException {
		boolean isDocumentControlledType = false;
		int controlType = 0;
		String status = "";
		Connection conn = null;
		Statement st = null;
		ResultSet resultSet = null;
		int documentId = getExistingDocumentId(document);
		if (documentId != 0) {
			String query = "select ChangeControlType, Status from "
					+ DocumentConstants.DOCUMENT_VERSION + " where dm_doc_id="
					+ documentId;
			try {
				conn = DatabaseUtils
						.getConnection(DocumentConstants.DOCM_SYSDB);
				logger.trace("query for Controlled Type--------" + query);
				st = conn.createStatement();
				resultSet = DatabaseUtils.getRecord(query, st);
				if (resultSet.next()) {
					controlType = resultSet.getInt(1);
					status = resultSet.getString(2);
					if (controlType == 1
							&& (!(status.equals("Deleted") || status
									.equals("Superseded")))) {
						isDocumentControlledType = true;
					}
				} else {
					throw new CouldNotCheckDocumentException(
							"No control type Found for the document id or Not valid document Id");
				}
			} catch (SQLException e) {
				logger.error(
						"isDocumentControlledType(com.mind.docm.Document)", e);
			} finally {
				DBUtil.close(resultSet, st);
				DBUtil.close(conn);
			}
		} else {
			throw new CouldNotCheckDocumentException(
					"No control type Found for the document id or Not valid document Id");
		}
		return isDocumentControlledType;
	}

	// Use Case
	/**
	 * @name getExistingDocumentId
	 * @purpose This method is used to check the document exist in document
	 *          repository.
	 * @steps 1. check the document id exists for fields in metadata of
	 *        newDocument. 2. Get the Metadata form newDocument Object. 3. Get
	 *        the Fields Array from Metadata Object. 4. get the fields name and
	 *        its value to craete the query for metadata fields search. 5. get
	 *        the value of existing documentId.
	 * @param newDocument
	 * @returnDescription Returns the string object of documentId.
	 * @throws CouldNotCheckDocumentException
	 */
	public static int getExistingDocumentId(com.mind.bean.docm.Document newDoc)
			throws CouldNotCheckDocumentException {
		int existingDocumentId = 0;
		String libValue = "";
		String type_id = "";
		String type_value = "";
		MetaData metaData = newDoc.getMetaData();
		Field[] field = metaData.getFields();
		for (int i = 0; i < field.length; i++) {
			if (field[i].getName().equalsIgnoreCase("Type")) {
				libValue = (field[i].getValue());
			}
		}
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet resultSet = null;
		String query = "select dm_md_fl_nm from dm_metadata_fields_list a join dm_library_master b"
				+ " on b.doc_identifier_fld_id=a.dm_md_fl_id and b.dm_lib_desc='"
				+ libValue + "'";
		logger.trace(query);

		try {
			st = conn.createStatement();
			resultSet = DatabaseUtils.getRecord(query, st);

			if (resultSet.next()) {
				type_id = resultSet.getString(1);
			}
			if (type_id == null || type_id.trim().equalsIgnoreCase(""))
				return existingDocumentId;
			for (int i = 0; i < field.length; i++) {
				if (field[i].getName().equalsIgnoreCase(type_id)) {
					type_value = (field[i].getValue());
				}
			}
			boolean isNumeric = true;
			try {
				Long.parseLong(type_value);
			} catch (NumberFormatException e) {
				logger.error("getExistingDocumentId(com.mind.docm.Document)", e);

				isNumeric = false;
			}

			String query1 = "select dm_doc_id from "
					+ DocumentConstants.DOCUMENT_VERSION + " where type='"
					+ libValue + "' and " + type_id + "=";

			if (isNumeric) {
				query1 += type_value;
			} else {
				query1 += "'" + type_value.replaceAll("'", "''") + "'";
			}

			logger.trace("query1 -------------" + query1);
			resultSet = DatabaseUtils.getRecord(query1, st);
			if (resultSet.next()) {
				existingDocumentId = resultSet.getInt(1);
			}
			logger.trace("ExistingDocumentId ::: " + existingDocumentId);
		} catch (Exception e) {
			logger.error("getExistingDocumentId(com.mind.docm.Document)", e);
		} finally {
			DBUtil.close(resultSet, st);
			DBUtil.close(conn);

		}
		return existingDocumentId;
	}

	public static int getExistingDocumentId(com.mind.bean.docm.Document newDoc,
			DataSource docmDataSource) throws CouldNotCheckDocumentException {
		int existingDocumentId = 0;
		String libValue = "";
		String type_id = "";
		String type_value = "";
		MetaData metaData = newDoc.getMetaData();
		Field[] field = metaData.getFields();
		for (int i = 0; i < field.length; i++) {
			if (field[i].getName().equalsIgnoreCase("Type")) {
				libValue = (field[i].getValue());
			}
		}
		Connection conn = null;
		Statement st = null;
		ResultSet resultSet = null;
		String query = "select dm_md_fl_nm from dm_metadata_fields_list a join dm_library_master b"
				+ " on b.doc_identifier_fld_id=a.dm_md_fl_id and b.dm_lib_desc='"
				+ libValue + "'";
		logger.trace(query);

		try {
			conn = docmDataSource.getConnection();
			st = conn.createStatement();
			resultSet = DatabaseUtils.getRecord(query, st);

			if (resultSet.next()) {
				type_id = resultSet.getString(1);
			}
			if (type_id == null || type_id.trim().equalsIgnoreCase(""))
				return existingDocumentId;
			for (int i = 0; i < field.length; i++) {
				if (field[i].getName().equalsIgnoreCase(type_id)) {
					type_value = (field[i].getValue());
				}
			}
			boolean isNumeric = true;
			try {
				Long.parseLong(type_value);
			} catch (NumberFormatException e) {
				logger.error("getExistingDocumentId(com.mind.docm.Document)", e);

				isNumeric = false;
			}

			String query1 = "select dm_doc_id from "
					+ DocumentConstants.DOCUMENT_VERSION + " where type='"
					+ libValue + "' and " + type_id + "=";

			if (isNumeric) {
				query1 += type_value;
			} else {
				query1 += "'" + type_value.replaceAll("'", "''") + "'";
			}

			logger.trace("query1 -------------" + query1);
			resultSet = DatabaseUtils.getRecord(query1, st);
			if (resultSet.next()) {
				existingDocumentId = resultSet.getInt(1);
			}
			logger.trace("ExistingDocumentId ::: " + existingDocumentId);
		} catch (Exception e) {
			logger.error("getExistingDocumentId(com.mind.docm.Document)", e);
		} finally {
			DBUtil.close(resultSet, st);
			DBUtil.close(conn);

		}
		return existingDocumentId;
	}

	// Use Case
	/**
	 * @name getDocument
	 * @purpose This method is used to get the document exist in document
	 *          repository.
	 * @steps 1. get the metadata fields from database to get the document on
	 *        the bases of documentId. 2. create a field array from the metadata
	 *        fields. 3. create the metadata object and set the field array in
	 *        it. 4. create the document object and return it. 5. get the file
	 *        object from DocMFileSysdb on bases of fileId for documentId
	 * @param newDocument
	 * @returnDescription Returns the document for the documentId.
	 * @throws CouldNotCheckDocumentException
	 */
	public static Document getDocument(String docId, String docVersion)
			throws CouldNotCheckDocumentException {
		ArrayList fieldArray = new ArrayList();
		Document document = new Document();
		String newMetadataVersion = "";
		MetaData mdata = new MetaData();
		int doc_file_id = 0;
		Connection conn = null;
		Connection connFile = null;
		Statement st = null;
		Statement stFile = null;
		ResultSet resultSet = null;
		int documentId = Integer.parseInt(docId);
		conn = DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB);
		if (docVersion.equals(""))
			newMetadataVersion = getMaxDocumentVersion(Integer.parseInt(docId),
					document, conn, false);
		else
			newMetadataVersion = docVersion;
		String query = "select * from " + DocumentConstants.DOCUMENT_VERSION
				+ " where dm_doc_id=" + documentId + " and DocumentVersion='"
				+ newMetadataVersion + "'";
		logger.trace("newMetadataVersion:: " + query);
		try {
			st = conn.createStatement();
			resultSet = DatabaseUtils.getRecord(query, st);
			ResultSetMetaData resultSet1 = resultSet.getMetaData();
			int colNo = resultSet1.getColumnCount();
			if (resultSet.next()) {
				for (int i = 1; i <= colNo; i++) {
					if (resultSet1.getColumnName(i).equals("dm_doc_file_id"))
						doc_file_id = resultSet.getInt(i);
					if (resultSet1.getColumnName(i)
							.equalsIgnoreCase("FileName"))
						document.setFileName(resultSet.getString(i));
					if (resultSet1.getColumnName(i).equalsIgnoreCase("MIME"))
						document.setMimeType(resultSet.getString(i));
					Field field = new Field();
					field.setName(resultSet1.getColumnName(i));
					field.setValue(resultSet.getString(i));
					fieldArray.add(field);
				}
			} else {
				throw new CouldNotCheckDocumentException(
						"No document exists in database for this documentId or invalid document id.");
			}
		} catch (SQLException e) {
			logger.error("getDocument(String, String)", e);
		} finally {
			DBUtil.close(resultSet, st);
			DBUtil.close(conn);
			// try {
			// resultSet.close();
			// } catch (Exception e) {
			// e.getStackTrace();
			// }
			// try {
			//
			// st.close();
			//
			// } catch (Exception e) {
			// e.getStackTrace();
			// }
			// try {
			// conn.close();
			//
			// } catch (Exception e) {
			// e.getStackTrace();
			// }
		}
		Object[] obj = fieldArray.toArray();
		Field[] field = new Field[obj.length];
		for (int i = 0; i < obj.length; i++) {
			field[i] = (Field) obj[i];
		}
		mdata.setFields(field);
		document.setMetaData(mdata);
		ResultSet rs2 = null;
		String queryForDocMFileSysdb = "select dm_fl_data from "
				+ DocumentConstants.DOCUMENT_FILE + " where dm_fl_id="
				+ doc_file_id;
		try {
			connFile = DatabaseUtils
					.getConnection(DocumentConstants.DOCM_FILE_SYSDB);
			stFile = connFile.createStatement();
			rs2 = DatabaseUtils.getRecord(queryForDocMFileSysdb, stFile);
			if (rs2.next()) {
				byte[] file_data = rs2.getBytes("dm_fl_data");
				document.setFileBytes(file_data);
			} else {
				throw new CouldNotCheckDocumentException(
						"No record Exists for this fileId or Invalid fileId.");
			}
		} catch (CouldNotCheckDocumentException e) {
			logger.debug("getDocument(String, String)", e);
			throw new CouldNotCheckDocumentException(
					"No record Exists for this fileId or Invalid fileId.");
		} catch (Exception e) {
			logger.error("getDocument(String, String)", e);
			logger.trace("" + e);
		} finally {
			DBUtil.close(rs2, stFile);
			DBUtil.close(connFile);
			try {
				rs2.close();
			} catch (Exception e) {
				e.getStackTrace();
			}
			try {

				stFile.close();

			} catch (Exception e) {
				e.getStackTrace();
			}
			try {
				connFile.close();

			} catch (Exception e) {
				e.getStackTrace();
			}
		}
		return document;
	}

	// Use Case
	/**
	 * @name getDocument
	 * @purpose This method is used to get the document exist in document
	 *          repository.
	 * @steps 1. Compare the metadata fields name in database with the metadata
	 *        fields name in document are same or not. 2. collect the number of
	 *        metadata fields name same in database and document. 3. If the
	 *        number of metadata fields name same in database and document are
	 *        equal then retrun true else false.
	 * @param newDocument
	 * @returnDescription Returns the boolean variable document is valid or not.
	 * @throws CouldNotCheckDocumentException
	 */
	public static boolean checkInvalidDocument(Document doc)
			throws CouldNotCheckDocumentException {
		MetaData mdata = doc.getMetaData();
		boolean valid = false;
		Field[] field = mdata.getFields();
		int rec = 0;
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet resultSet = null;
		String query = "select count(*) from "
				+ DocumentConstants.METADATA_LIST + "  where dm_md_fl_nm in ('";
		for (int i = 0; i < field.length; i++) {
			query += (i == 0 ? "" : ",'") + field[i].getName() + "'";
		}
		query += ")";
		logger.trace(query);
		logger.trace("Query for checkInvalidDocument ::::: " + query);
		try {
			st = conn.createStatement();
			resultSet = DatabaseUtils.getRecord(query, st);
			if (resultSet.next()) {
				rec = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			logger.error("checkInvalidDocument(Document)", e);
		} finally {
			DBUtil.close(resultSet, st);
			DBUtil.close(conn);

		}
		logger.trace(rec + " :: " + field.length);
		if (rec == field.length) {
			valid = true;
		} else {
			throw new CouldNotCheckDocumentException(
					"This document is not valid document.");
		}
		return valid;
	}

	// Use Case
	/**
	 * @name checkIdExistence
	 * @purpose This method is used check that dm_doc_id,dm_doc_file_id this
	 *          type of fields are not in document object.
	 * @steps 1. create a loop for all the field name exists in document. 2. if
	 *        field name does not contain dm_doc_id,dm_doc_lib_id and
	 *        dm_doc_file_id field then field object inserted in arraylist else
	 *        not. 3. the convert the array list in field array and return it.
	 * @param Document
	 * @param fileId
	 * @returnDescription Docum
	 * @throws DocMFormatException
	 */
	public static Document getSearchableDocument(Document doc,
			DataSource docmDataSource) {

		MetaData mdata = doc.getMetaData();
		Field[] field = mdata.getFields();
		ArrayList aList = new ArrayList();
		ArrayList main = new ArrayList();
		Connection conn = null;
		Statement st = null;
		ResultSet resultSet = null;

		String query = "select dm_md_fl_nm from dbo.dm_metadata_fields_list where dm_md_fl_nm in ('";
		for (int i = 0; i < field.length; i++) {
			query += (i == 0 ? "" : ",'") + field[i].getName() + "'";
		}
		query += ")";
		logger.trace(query);
		try {
			conn = docmDataSource.getConnection();
			st = conn.createStatement();

			resultSet = DatabaseUtils.getRecord(query, st);
			while (resultSet.next()) {
				aList.add(resultSet.getString(1));
			}
		} catch (SQLException e) {
			logger.error("getSearchableDocument(Document)", e);
		} finally {
			DBUtil.close(resultSet, st);
			DBUtil.close(conn);
		}
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < aList.size(); j++) {
				if (field[i].getName().equalsIgnoreCase((String) aList.get(j))) {
					logger.trace(field[i].getName() + " :: Field Value: "
							+ field[i].getValue());
					main.add(field[i]);

				}
			}
		}
		Field[] newField = com.mind.docm.util.DocumentUtility.fieldArray(main);
		MetaData newMetadata = new MetaData();
		Document newDoc = new Document();
		newMetadata.setFields(newField);
		newDoc.setMetaData(newMetadata);
		newDoc.setFileBytes(doc.getFileBytes());
		newDoc.setFileName(doc.getFileName());
		return newDoc;
	}

	public static Document getSearchableDocument(Document doc) {

		return getSearchableDocument(doc,
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_SYSDB));
	}

	// Use Case
	/**
	 * @name getCompulsaryFieldForLibrary
	 * @purpose This method is used to get all the compulsary fields for a
	 *          library.
	 * @steps 1. select the library id for the given library name. 2. get all
	 *        the metadata field name from the dm_metadata_fields_list table
	 *        where compulsary field is yes and library id matches. 3. Add all
	 *        the compulsary metadat fields in ArrayList object. 4. convert it
	 *        to string array and return it.
	 * @param fieldArrayList
	 * @returnDescription Field[]
	 * @throws None
	 */
	public static String[] getCompulsaryFieldForLibrary(String lib_name,
			DataSource docmDataSource) {
		ArrayList aList = new ArrayList();
		Connection conn = null;
		Statement st = null;
		ResultSet resultSet = null;
		ResultSet resultSet1 = null;
		int lib_id = 0;
		try {
			conn = docmDataSource.getConnection();
			st = conn.createStatement();
			String selectQuery = "select dm_lib_id from dm_library_master "
					+ " where dm_lib_desc='" + lib_name + "'";
			logger.trace("Query for compulsary field " + selectQuery);
			resultSet1 = DatabaseUtils.getRecord(selectQuery, st);
			if (resultSet1.next()) {
				lib_id = resultSet1.getInt(1);
			}
			String query = "select dm_md_fl_nm from "
					+ DocumentConstants.METADATA_LIST
					+ " where dm_md_fl_id in "
					+ " (select dm_lib_md_fl_id from "
					+ DocumentConstants.LIBRARY_METADATA_SETUP
					+ " where dm_lib_md_lib_id=" + lib_id
					+ " and dm_lib_md_compul_fl='Y')";
			logger.trace("Query for compulsary field " + query);
			logger.trace("compulsary field :: " + query);
			resultSet = DatabaseUtils.getRecord(query, st);
			while (resultSet.next()) {
				aList.add(resultSet.getString(1));
				logger.trace("compulsary field :: " + resultSet.getString(1));
			}
		} catch (SQLException e) {
			logger.error("getCompulsaryFieldForLibrary(String)", e);
		} finally {
			DBUtil.close(st);
			DBUtil.close(resultSet);
			DBUtil.close(resultSet1);
			DBUtil.close(conn);
		}
		Object[] obj = aList.toArray();
		String[] mDataDesc = new String[obj.length];
		for (int i = 0; i < obj.length; i++) {
			mDataDesc[i] = (String) obj[i];
		}
		return mDataDesc;
	}

	public static String[] getCompulsaryFieldForLibrary(String lib_name) {
		return getCompulsaryFieldForLibrary(lib_name,
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_SYSDB));
	}

	// Use Case
	/**
	 * @name checkCompusaryFieldsInDoc
	 * @purpose This method is used to check that the compulsary fields are
	 *          exist or not in document.
	 * @steps 1. create a loop for all the metadats field exists in document. 2.
	 *        get the name of field and then check whether it exists in
	 *        compulsaryField array or not.
	 * @param Document
	 * @param compulsaryField
	 *            Array
	 * @returnDescription A boolean variable which refer that fieldExists or not
	 *                    in document
	 * @throws
	 */
	public static boolean checkCompulsaryFieldsInDoc(Document doc,
			String[] compulsaryField) {
		boolean exist = false;
		MetaData mdata = doc.getMetaData();
		Field[] field = mdata.getFields();
		for (int i = 0; i < field.length; i++) {
			String name = field[i].getName();
			for (int j = 0; j < compulsaryField.length; j++) {
				if (name.equalsIgnoreCase(compulsaryField[j])) {
					exist = true;
				}
				if (exist == false) {
					break;
				}
			}
		}
		return exist;
	}

	// Use Case
	/**
	 * @name getArrayListDesc
	 * @purpose This method is used get the desc according ArrayList.
	 * @steps 1. create a loop for ArrayList parameter. 2. get the inner
	 *        arrayList of arraylist parameter. 3. return the description.
	 * @param Document
	 * @param compulsaryField
	 *            Array
	 * @returnDescription A boolean variable which refer that fieldExists or not
	 *                    in document
	 * @throws
	 */
	public String getArrayListDesc(ArrayList aList) {
		ArrayList inner = null;
		String id = "";
		String desc = "";
		for (int i = 0; i < aList.size(); i++) {
			inner = (ArrayList) aList.get(i);
			for (int j = 0; j < inner.size(); j++) {
				id = (String) inner.get(1);
				desc = (String) inner.get(1);
			}
		}
		return null;
	}

	// Use Case
	/**
	 * @name documentMasterInsert
	 * @purpose This method is used to add of a new document to the document
	 *          repository.
	 * @steps 1. Call the documentUtility insertInDocumentMaster method to
	 *        insert a document in dm_document_master table for the given
	 *        library name and get the documentId. 2. Call the documentUtility
	 *        insertInDocumentFileDb method to insert a document in
	 *        dm_document_file table and get the fileId. if document is not to
	 *        be inserted in dm_document_file then call documentUtility
	 *        getMaxDocumentFileId method to retrieve latest file id of existing
	 *        document against latest document version. 3. Call the
	 *        documentUtility insertInDocumentVersion method to insert a
	 *        document in dm_document_version table for the given docId and
	 *        fileId.
	 * @param newDocument
	 * @param libraryName
	 * @returnDescription Returns the documentId as reference to the document
	 *                    repository location for the added document.
	 * @throws None
	 */
	public static int documentMasterInsert(
			com.mind.bean.docm.Document newDocument, String libraryName,
			DataSource docmDataSource, DataSource docMFileDataSource) {
		int doc_id = 0;
		String doc_file_id = "";
		Connection conDocMaster = null;
		Connection conFileSys = null;

		try {
			conDocMaster = docmDataSource.getConnection();
			conDocMaster.setAutoCommit(false);

			conFileSys = docMFileDataSource.getConnection();
			conFileSys.setAutoCommit(false);

			doc_id = insertInDocumentMaster(libraryName, conDocMaster);
			if ((newDocument.getFileBytes() != null)) {
				doc_file_id = insertInDocumentFileDb(newDocument, conFileSys);
			} else {
				// Get latest file id of existing document against latest
				// document version in case a new file is not to be uploaded
				doc_file_id = getMaxDocumentFileId(doc_id, newDocument,
						docmDataSource);
			}
			insertInDocumentVersion(newDocument, Integer.valueOf(doc_id)
					.toString(), doc_file_id, conDocMaster);

			conDocMaster.commit();
			conFileSys.commit();

		} catch (DocMFormatException e) {
			logger.error(
					"documentMasterInsert(com.mind.docm.Document, String)", e);
			DBUtil.rollback(conDocMaster);
			DBUtil.rollback(conFileSys);
		} catch (SQLException sqle) {
			logger.error(
					"documentMasterInsert(com.mind.docm.Document, String)",
					sqle);
			DBUtil.rollback(conDocMaster);
			DBUtil.rollback(conFileSys);
		} catch (Exception e) {
			logger.error(
					"documentMasterInsert(com.mind.docm.Document, String)", e);
			DBUtil.rollback(conDocMaster);
			DBUtil.rollback(conFileSys);
		} finally {
			DBUtil.close(conDocMaster);
			DBUtil.close(conFileSys);
		}
		return doc_id;
	}

	public static int documentMasterInsert(
			com.mind.bean.docm.Document newDocument, String libraryName) {
		return documentMasterInsert(newDocument, libraryName,
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_SYSDB),
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_FILE_SYSDB));
	}

	// Use Case
	/**
	 * @name documentMasterUpdate
	 * @purpose This method is used to update document record in the document
	 *          repository.
	 * @steps 1. Call the documentUtility insertInDocumentFileDb method to
	 *        insert a document in dm_document_file table and get the fileId. if
	 *        document is not to be inserted in dm_document_file then call
	 *        documentUtility getMaxDocumentFileId method to retrieve latest
	 *        file id of existing document against latest document version. 2.
	 *        Call the documentUtility insertInDocumentVersion method to insert
	 *        a document in dm_document_version table for the given docId and
	 *        fileId.
	 * @param newDocument
	 * @param docId
	 * @returnDescription Returns the documentId as reference to the document
	 *                    repository location for document that got replaced.
	 * @throws None
	 */
	public static int documentMasterUpdate(Document newDocument, int docId) {
		return documentMasterUpdate(newDocument, docId,
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_SYSDB),
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_FILE_SYSDB));
	}

	public static int documentMasterUpdate(Document newDocument, int docId,
			DataSource docmDataSource, DataSource docMFileDataSource) {
		Connection conFileSys = null;
		Connection conDocVersion = null;

		String doc_file_id = "";
		logger.trace("docId document master update " + docId);

		try {
			conFileSys = docMFileDataSource.getConnection();
			conFileSys.setAutoCommit(false);

			if (newDocument.getFileBytes() != null) {
				doc_file_id = insertInDocumentFileDb(newDocument, conFileSys);
			} else {
				doc_file_id = getMaxDocumentFileId(docId, newDocument);
			}
			conDocVersion = docmDataSource.getConnection();
			conDocVersion.setAutoCommit(false);
			insertInDocumentVersion(newDocument, Integer.valueOf(docId)
					.toString(), doc_file_id, conDocVersion);

			conFileSys.commit();
			conDocVersion.commit();

		} catch (DocMFormatException e) {
			logger.error("documentMasterUpdate(Document, int)", e);
			DBUtil.rollback(conFileSys);
			DBUtil.rollback(conDocVersion);
		} catch (SQLException sqle) {
			logger.error(
					"documentMasterInsert(com.mind.docm.Document, String)",
					sqle);
			DBUtil.rollback(conFileSys);
			DBUtil.rollback(conDocVersion);
		} catch (Exception e) {
			logger.error("documentMasterUpdate(Document, int)", e);
			DBUtil.rollback(conFileSys);
			DBUtil.rollback(conDocVersion);
		} finally {
			DBUtil.close(conFileSys);
			DBUtil.close(conDocVersion);
		}
		return docId;
	}

	// Use Case
	/**
	 * @name insertInDocumentMaster
	 * @purpose This method is used to get a new documentId against a library in
	 *          which this document will be saved.
	 * @steps 1. Get the libraryId corresponding to the library Name. 2. Insert
	 *        a new record with library id in document master and retrieve newly
	 *        created document id.
	 * @param libraryName
	 * @param conn
	 * @returnDescription Returns the documentId.
	 * @throws DocMFormatException
	 *             , SQLException
	 */
	public static int insertInDocumentMaster(String libraryName, Connection conn)
			throws DocMFormatException, SQLException {
		int doc_id = 0;
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet resultSet = null;
		int lib_id = 0;
		try {
			String selectQuery = "(select dm_lib_id from dm_library_master "
					+ " where dm_lib_desc=?)";

			String insertQuery = "insert into "
					+ DocumentConstants.DOCUMENT_MASTER
					+ " (dm_doc_lib_id) values (?)";

			pst = conn.prepareStatement(selectQuery);

			pst.setString(1, libraryName);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				lib_id = rs.getInt(1);
			}

			// Insert record.
			pst1 = conn.prepareStatement(insertQuery,
					Statement.RETURN_GENERATED_KEYS);
			pst1.setInt(1, lib_id);
			int status = pst1.executeUpdate();

			if (status == 0) {
				throw new DocMFormatException("No Library Found");
			}

			// Get identity column value.
			rs = pst1.getGeneratedKeys();
			while (rs.next()) {
				doc_id = rs.getBigDecimal(1).intValue();
			}

			pst.close();
			pst1.close();
		} catch (SQLException e) {
			throw e;
		} finally {
			DBUtil.close(resultSet);
		}
		return doc_id;
	}

	// Use Case
	/**
	 * @name insertInDocumentVersion
	 * @purpose This method is used to insert the metadata field of document
	 *          fileId of inserted document in document repository.
	 * @steps 1. Get latest document version from dm_doc_version for existing
	 *        docid. 2. insert new record in dm_document_version table with
	 *        fileId and new version.
	 * @param Document
	 *            .
	 * @param docId
	 *            .
	 * @param fileId
	 *            .
	 * @param Connection
	 *            .
	 * @returnDescription None.
	 * @throws DocMFormatException
	 *             , SQLException.
	 */
	public static void insertInDocumentVersion(Document doc, String docId,
			String fileId, Connection connectionDocSys)
			throws DocMFormatException, SQLException {
		PreparedStatement pst1 = null;
		String newMetadataVersion = "";
		MetaData mdata = doc.getMetaData();
		Field[] fields = mdata.getFields();
		int status = 0;

		// get new version for document.
		newMetadataVersion = getMaxDocumentVersion(Integer.parseInt(docId),
				doc, connectionDocSys, true);

		String insertQuery = "insert into "
				+ DocumentConstants.DOCUMENT_VERSION
				+ " (dm_doc_id,filename,DocumentVersion,dm_doc_file_id,";

		for (int i = 0; i < fields.length; i++) {
			insertQuery += (i == 0 ? " " : ",") + fields[i].getName();
		}
		insertQuery += ") values (";
		for (int i = 0; i < fields.length; i++) {
			insertQuery += (i == 0 ? "?,?,?,?,?" : ",?");
		}
		insertQuery += " )";
		logger.trace("insert query in docuemnt version:; " + insertQuery);

		int docid = Integer.parseInt(docId);
		try {
			pst1 = connectionDocSys.prepareStatement(insertQuery);
			pst1.setInt(1, docid);
			pst1.setString(2, doc.getFileName());
			pst1.setString(3, newMetadataVersion);
			pst1.setString(4, fileId);
			for (int i = 0; i < fields.length; i++) {
				pst1.setString(i + 5,
						((fields[i].getValue() == null || fields[i].getValue()
								.equals("")) ? null : fields[i].getValue()));
			}
			status = pst1.executeUpdate();
			if (status == 0) {
				throw new DocMFormatException("Failed Insertion");
			}
		} catch (SQLException e1) {
			throw e1;
		} finally {
			DBUtil.close(pst1);
		}
	}

	// Use Case
	/**
	 * @name insertInDocumentFileDb
	 * @purpose This method is used to insert the file data in dm_document_file
	 *          table.
	 * @steps 1. Get the file data from document object. 2. insert the file data
	 *        in dm_document_file table and retrieve the newly created fileId.
	 * @param Document
	 * @param Connection
	 * @returnDescription FileId as String value.
	 * @throws DocMFormatException
	 *             , SQLException
	 */

	public static String insertInDocumentFileDb(Document doc,
			Connection connectionFileSys) throws DocMFormatException,
			SQLException {
		PreparedStatement pst1 = null;
		ResultSet rs = null;

		byte[] byteFile = doc.getFileBytes();
		String sql = "";
		int fileId = 0;
		logger.trace("fileId" + fileId);

		try {
			sql = "insert into " + DocumentConstants.DOCUMENT_FILE
					+ "(dm_fl_data) values (?)";
			pst1 = connectionFileSys.prepareStatement(sql,
					Statement.RETURN_GENERATED_KEYS);
			pst1.setBytes(1, byteFile);
			if (byteFile != null) {
				int status = pst1.executeUpdate();
				if (status != 0) {
					rs = pst1.getGeneratedKeys();
					while (rs.next()) {
						fileId = rs.getBigDecimal(1).intValue();
					}
				} else {
					throw new DocMFormatException("Failed Insertion");
				}
			}
		} catch (SQLException e1) {
			throw e1;
		} finally {
			DBUtil.close(pst1);
		}
		return String.valueOf(fileId);
	}

	// Use Case
	/**
	 * @name getMaxDocumentVersion
	 * @purpose This method is used to get the max document version
	 * @steps 1. Get the major version and minor version for the document id. 2.
	 *        if the insertion in document version table is needed then get the
	 *        first record of document version which is higher then other.If the
	 *        document has file then increase the major version. and if the file
	 *        does not exists then increase the minor version. 3. Return it to
	 *        the calling method. 4. If the insertion in dm_document_version
	 *        table is false then return the maximum document version.
	 * @param Document
	 *            ,documentId,insertInVersionTable
	 * @returnDescription String type maximum documentVersion
	 * @throws None
	 */
	public static String getMaxDocumentVersion(int docId, Document doc,
			boolean insertInVersionTable) {
		return getMaxDocumentVersion(docId, doc,
				DatabaseUtils.getConnection(DocumentConstants.DOCM_SYSDB),
				insertInVersionTable);
	}

	public static String getMaxDocumentVersion(int docId, Document doc,
			Connection docmConnection, boolean insertInVersionTable) {
		ResultSet resultSet = null;
		String max_doc_version = "";
		String new_mdata_version = "1.0";
		Connection conn = null;
		Statement st = null;
		int count = 0;
		VersionSchema schema = new VersionSchema();
		String query = "select cast(substring(DocumentVersion,0,charindex('.',DocumentVersion)) as numeric) as majorVersion,"
				+ " cast(substring(DocumentVersion,charindex('.',DocumentVersion)+1,len(DocumentVersion)) as numeric) as minorVersion"
				+ " from "
				+ DocumentConstants.DOCUMENT_VERSION
				+ " where dm_doc_id="
				+ docId
				+ " order by majorVersion desc,minorVersion desc";
		logger.trace(query);
		try {
			conn = docmConnection;
			st = conn.createStatement();

			resultSet = st.executeQuery(query);
			while (resultSet.next()) {
				count++;
				if (count == 1) {
					max_doc_version = resultSet.getString(1) + "."
							+ resultSet.getString(2);
					if (insertInVersionTable) {
						if (doc.getFileBytes() == null) {
							new_mdata_version = schema
									.getNewMdataVersion(max_doc_version);
						} else if (doc.getFileBytes() != null) {
							new_mdata_version = Float.toString((Float.valueOf(
									resultSet.getString(1)).floatValue() + 1f));
						}
					} else {
						new_mdata_version = max_doc_version;
					}
				}
			}
		} catch (SQLException e) {
			logger.error("getMaxDocumentVersion(int, Document, boolean)", e);
		} finally {
			DBUtil.close(resultSet, st);
		}
		return new_mdata_version;
	}

	// Use Case
	/**
	 * @name getMaxDocumentFileId
	 * @purpose This method is used to get the latest file id against latest
	 *          version of a document.
	 * @param Document
	 *            ,docId
	 * @returnDescription String type max FileId
	 * @throws None
	 */
	public static String getMaxDocumentFileId(int docId, Document doc,
			DataSource docmDataSource) {
		Connection conn = null;
		Statement st = null;
		ResultSet resultSet = null;
		String newFileVersion = "";
		try {
			conn = docmDataSource.getConnection();
			st = conn.createStatement();
			resultSet = null;
			String query = "select max(dm_doc_file_id) from "
					+ DocumentConstants.DOCUMENT_VERSION + " where dm_doc_id="
					+ docId;
			logger.trace("For null :: " + query);
			resultSet = st.executeQuery(query);
			if (resultSet.next()) {
				newFileVersion = resultSet.getString(1);
			}

		} catch (SQLException e) {
			logger.error("getMaxDocumentFileId(int, Document)", e);
		} finally {
			DBUtil.close(resultSet, st);
			DBUtil.close(conn);

		}
		return newFileVersion;
	}

	public static String getMaxDocumentFileId(int docId, Document doc) {
		return getMaxDocumentFileId(docId, doc,
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_SYSDB));
	}

	/**
	 * Gets the current java sql date.
	 * 
	 * @return the current java sql date
	 */
	public static java.sql.Date getCurrentJavaSqlDate() {
		java.util.Date today = new java.util.Date();
		return new java.sql.Date(today.getTime());
	}

	/**
	 * Gets the library name.
	 * 
	 * @param libId
	 *            the lib id
	 * 
	 * @return the library name
	 */
	public static String getLibraryName(String libId) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet rs = null;
		String query = "select isnull(dm_lib_desc,'') from dm_library_master where dm_lib_id = "
				+ libId;
		String libName = null;
		try {
			st = conn.createStatement();
			rs = DatabaseUtils.getRecord(query, st);
			if (rs.next()) {
				libName = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("getLibraryName(String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);

		}
		return libName;
	}

	/**
	 * Sets the update user.
	 * 
	 * @param doc
	 *            the new update user
	 */
	public static void setUpdateUser(Document doc) {
		Field fields[] = doc.getMetaData().getFields();
		String updateDate = "";
		String updateUser = "";
		for (int i = 0; i < fields.length; i++) {
			if (fields[i]
					.getName()
					.trim()
					.equalsIgnoreCase(
							DatabaseUtilityDao
									.getKeyValue("ilex.docm.entitymanager.adddate")))
				updateDate = fields[i].getValue();
			if (fields[i]
					.getName()
					.trim()
					.equalsIgnoreCase(
							DatabaseUtilityDao
									.getKeyValue("ilex.docm.entitymanager.adduser")))
				updateUser = fields[i].getValue();
			if (fields[i]
					.getName()
					.trim()
					.equalsIgnoreCase(
							DatabaseUtilityDao
									.getKeyValue("ilex.docm.entitymanager.updatedocumentdate")))
				fields[i].setValue(updateDate);
			if (fields[i]
					.getName()
					.trim()
					.equalsIgnoreCase(
							DatabaseUtilityDao
									.getKeyValue("ilex.docm.entitymanager.updatedocumentuser"))) {
				fields[i].setValue(updateUser);
			}
		}
	}

	/**
	 * Gets the fields value.
	 * 
	 * @param doc
	 *            the doc
	 * @param fieldName
	 *            the field name
	 * 
	 * @return the fields value
	 */
	public static String getFieldsValue(Document doc, String fieldName) {
		String fieldValue = "";
		Field[] fields = doc.getMetaData().getFields();
		for (int i = 0; i < fields.length; i++) {
			if (fields[i].getName().equalsIgnoreCase(fieldName)) {
				fieldValue = fields[i].getValue();
			}
		}
		return fieldValue;
	}

	/**
	 * Sets the fields value.
	 * 
	 * @param doc
	 *            the doc
	 * @param fieldName
	 *            the field name
	 * @param fieldValue
	 *            the field value
	 */
	public static void setFieldsValue(Document doc, String fieldName,
			String fieldValue) {
		Field[] fields = doc.getMetaData().getFields();
		for (int i = 0; i < fields.length; i++) {
			if (fields[i].getName().equalsIgnoreCase(fieldName)) {
				fields[i].setValue(fieldValue);
			}
		}
	}

	/**
	 * Gets the file id.
	 * 
	 * @param id
	 *            the id
	 * @param type
	 *            the type
	 * 
	 * @return the file id
	 */
	public static String getFileId(String id, String type) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet rs = null;
		String fileId = "";
		String query = "select max(dm_doc_file_id) from dm_document_version  where po_id='"
				+ id + "' and type='" + type + "'";
		try {
			st = conn.createStatement();
			rs = DatabaseUtils.getRecord(query, st);
			if (rs.next()) {
				fileId = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("getFileId(String, String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);

		}
		return fileId;
	}

	/**
	 * Gets the file id from doc.
	 * 
	 * @param docId
	 *            the doc id
	 * 
	 * @return the file id from doc
	 */
	public static String getfileIdFromDoc(String docId) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet rs = null;
		String fileId = "";
		// String query =
		// "select dm_doc_file_id from dm_document_version  where dm_doc_id='"+docId+"'";
		String query = "select top 1 dm_doc_file_id from dm_document_version  where dm_doc_id='"
				+ docId
				+ "' order by cast(documentVersion as numeric(10)) desc";
		try {
			st = conn.createStatement();
			rs = DatabaseUtils.getRecord(query, st);
			if (rs.next()) {
				fileId = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("getfileIdFromDoc(String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);

		}
		return fileId;
	}
}
