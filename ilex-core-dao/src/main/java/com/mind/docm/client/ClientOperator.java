//
//
//
//  @ Project : DocM - Document Management System - Ilex
//  @ File Name : ClientOperator.java
//  @ Date : 12-June-2008
//  @ Author : Tanuj Mittal
//
//

package com.mind.docm.client;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.lowagie.text.DocumentException;
import com.mind.bean.docm.Document;
import com.mind.bean.docm.Field;
import com.mind.bean.docm.MetaData;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.docm.exceptions.ControlledTypeException;
import com.mind.docm.exceptions.CouldNotAddToRepositoryException;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.CouldNotMarkDeleteException;
import com.mind.docm.exceptions.CouldNotPhysicallyDeleteException;
import com.mind.docm.exceptions.CouldNotReplaceException;
import com.mind.docm.exceptions.CouldNotSupercedeDocumentException;
import com.mind.docm.exceptions.DocMFormatException;
import com.mind.docm.exceptions.DocumentNotApprovedException;
import com.mind.docm.exceptions.DocumentNotExistsException;
import com.mind.docm.utils.DocumentUtility;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class ClientOperator.
 */
public class ClientOperator {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ClientOperator.class);

	// Use Case
	/**
	 * @name addDocument
	 * @purpose This method is used to request addition of a new document to the
	 *          document repository. Document may be replaced in some situations
	 *          however.
	 * @steps 1. Checks from DocumentUtility if the Document exists or not. 2.
	 *        If the document exists, then the DocumentUtility is commanded to
	 *        add document to the repository. 3. If the document was added then
	 *        its documentId from the repository is returned to the caller of
	 *        this method. 4. If the document does not exists already then it
	 *        checks from the DocumentUtility if the document is a controlled
	 *        type or not. 5. If the document is a controlled type then addition
	 *        is not allowed and the process is halted by throwing an Exception.
	 *        6. If the document is not a controlled type then document is
	 *        replaced by DocumentUtility. 7. The documentId of the replaced
	 *        document from the repository is returned to the caller of this
	 *        method. 8. Note: The same operation also works for the addition of
	 *        a suppliment document corresponding to an Ilex Entity.
	 * @param newDocument
	 * @return documentId
	 * @returnDescription Returns the documentId as reference to the document
	 *                    repository location for the added document (or the
	 *                    document that got replaced)
	 * @throws ControlledTypeException
	 * @throws CouldNotAddToRepositoryException
	 * @throws CouldNotReplaceException
	 * @throws CouldNotCheckDocumentException
	 * @throws DocMFormatException
	 */
	public static int addDocument(Document newDoc)
			throws ControlledTypeException, CouldNotAddToRepositoryException,
			CouldNotReplaceException, CouldNotCheckDocumentException,
			DocMFormatException {
		int documentId = 0;
		Document newDocument = DocumentUtility.getSearchableDocument(newDoc);
		boolean valid = checkDocument(newDocument);
		if (valid) {
			if (DocumentUtility.isDocumentExists(newDocument)) {
				if (DocumentUtility.isDocumentControlledType(newDocument)) {
					throw new ControlledTypeException("Controlled");
				} else {
					int existingDocumentId = DocumentUtility
							.getExistingDocumentId(newDocument);
					documentId = existingDocumentId;
					DocumentUtility.replaceDocument(existingDocumentId,
							newDocument);
				}
			} else {
				documentId = DocumentUtility.addToRepository(newDocument);
			}
		} else {
			throw new DocMFormatException(
					"Compulsary Metadata Fields are not Exists.");
		}
		return documentId;
	}

	/**
	 * Explicit add document.
	 * 
	 * @param newDoc
	 *            the new doc
	 * 
	 * @return the int
	 * 
	 * @throws ControlledTypeException
	 *             the controlled type exception
	 * @throws CouldNotAddToRepositoryException
	 *             the could not add to repository exception
	 * @throws CouldNotReplaceException
	 *             the could not replace exception
	 * @throws CouldNotCheckDocumentException
	 *             the could not check document exception
	 * @throws DocMFormatException
	 *             the doc m format exception
	 * @throws DocumentException
	 *             the document exception
	 */
	public static int explicitAddDocument(Document newDoc)
			throws ControlledTypeException, CouldNotAddToRepositoryException,
			CouldNotReplaceException, CouldNotCheckDocumentException,
			DocMFormatException, DocumentException {
		int documentId = 0;
		Document newDocument = DocumentUtility.getSearchableDocument(newDoc);
		boolean valid = checkDocument(newDocument);
		if (valid) {
			if (DocumentUtility.isDocumentExists(newDocument)) {
				throw new DocumentException(
						"Document Already exists with the same MSA ID.");
			} else {
				documentId = DocumentUtility.addToRepository(newDocument);
			}
		} else {
			throw new DocMFormatException(
					"Compulsary Metadata Fields are not Exists.");
		}
		return documentId;
	}

	public static int explicitAddDocument(Document newDoc,
			DataSource docmDataSource, DataSource docMFileDataSource)
			throws ControlledTypeException, CouldNotAddToRepositoryException,
			CouldNotReplaceException, CouldNotCheckDocumentException,
			DocMFormatException, DocumentException {
		int documentId = 0;
		Document newDocument = DocumentUtility.getSearchableDocument(newDoc,
				docmDataSource);
		boolean valid = checkDocument(newDocument, docmDataSource);
		if (valid) {
			if (DocumentUtility.isDocumentExists(newDocument, docmDataSource)) {
				throw new DocumentException(
						"Document Already exists with the same MSA ID.");
			} else {
				documentId = DocumentUtility.addToRepository(newDocument,
						docmDataSource, docMFileDataSource);
			}
		} else {
			throw new DocMFormatException(
					"Compulsary Metadata Fields are not Exists.");
		}
		return documentId;
	}

	/**
	 * Implicit modify document for po deliverables.
	 * 
	 * @param documentId
	 *            the document id
	 * @param newDoc
	 *            the new doc
	 * @param updateUser
	 *            the update user
	 * 
	 * @return the int
	 * 
	 * @throws CouldNotReplaceException
	 *             the could not replace exception
	 * @throws CouldNotCheckDocumentException
	 *             the could not check document exception
	 * @throws DocMFormatException
	 *             the doc m format exception
	 * @throws DocumentNotExistsException
	 *             the document not exists exception
	 */
	public static int implicitModifyDocumentForPODeliverables(
			String documentId, Document newDoc, String updateUser,
			DataSource docmDataSource, DataSource docMFileDataSource)
			throws CouldNotReplaceException, CouldNotCheckDocumentException,
			DocMFormatException, DocumentNotExistsException {
		explicitModifyDocument(documentId, newDoc, updateUser, docmDataSource,
				docMFileDataSource);
		return Integer.parseInt(documentId);
	}

	public static int implicitModifyDocumentForPODeliverables(
			String documentId, Document newDoc, String updateUser)
			throws CouldNotReplaceException, CouldNotCheckDocumentException,
			DocMFormatException, DocumentNotExistsException {
		return implicitModifyDocumentForPODeliverables(documentId, newDoc,
				updateUser,
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_SYSDB),
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_FILE_SYSDB));
	}

	/**
	 * Explicit modify document.
	 * 
	 * @param documentId
	 *            the document id
	 * @param newDoc
	 *            the new doc
	 * 
	 * @return the int
	 * 
	 * @throws CouldNotReplaceException
	 *             the could not replace exception
	 * @throws CouldNotCheckDocumentException
	 *             the could not check document exception
	 * @throws DocMFormatException
	 *             the doc m format exception
	 * @throws DocumentNotExistsException
	 *             the document not exists exception
	 */
	public static int explicitModifyDocument(String documentId, Document newDoc)
			throws CouldNotReplaceException, CouldNotCheckDocumentException,
			DocMFormatException, DocumentNotExistsException {
		Document newDocument = DocumentUtility.getSearchableDocument(newDoc);
		boolean valid = checkDocument(newDocument);
		if (valid) {
			if (DocumentUtility.isDocumentExists(documentId)) {
				DocumentUtility.replaceDocument(Integer.parseInt(documentId),
						newDocument);
			} else {
				throw new DocumentNotExistsException("Document Not Exist.");
			}
		} else {
			throw new DocMFormatException(
					"Compulsary Metadata Fields are not Exists.");
		}
		return Integer.parseInt(documentId);
	}

	// Use Case
	/**
	 * @name explicitModifyDocument
	 * @purpose To replace an existing version with a new version of document.
	 * @steps
	 * @param documentId
	 * @param newDoc
	 * @param updateUser
	 * @return int
	 * @returnDescription document id.
	 * @throws CouldNotReplaceException
	 * @throws CouldNotCheckDocumentException
	 * @throws DocMFormatException
	 * @throws DocumentNotExistsException
	 */

	public static int explicitModifyDocument(String documentId,
			Document newDoc, String updateUser)
			throws CouldNotReplaceException, CouldNotCheckDocumentException,
			DocMFormatException, DocumentNotExistsException {
		return explicitModifyDocument(documentId, newDoc, updateUser,
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_SYSDB),
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_FILE_SYSDB));
	}

	public static int explicitModifyDocument(String documentId,
			Document newDoc, String updateUser, DataSource docmDataSource,
			DataSource docMFileDataSource) throws CouldNotReplaceException,
			CouldNotCheckDocumentException, DocMFormatException,
			DocumentNotExistsException {
		Document newDocument = DocumentUtility.getSearchableDocument(newDoc,
				docmDataSource);
		boolean valid = checkDocument(newDocument, docmDataSource);
		if (valid) {
			if (DocumentUtility.isDocumentExists(documentId, docmDataSource)) {
				DocumentUtility.replaceDocument(Integer.parseInt(documentId),
						newDocument, updateUser, docmDataSource,
						docMFileDataSource);
			} else {
				throw new DocumentNotExistsException("Document Not Exist.");
			}
		} else {
			throw new DocMFormatException(
					"Compulsary Metadata Fields are not Exists.");
		}
		return Integer.parseInt(documentId);
	}

	// Use Case
	/**
	 * @name setDocumentToDraft
	 * @purpose This method is used to request setting the document to draft
	 *          state. The document is marked to superceded state.
	 * @steps 1. Checks from DocumentUtility if the Document exists or not. 2.
	 *        If the document exists, then the DocumentUtility is commanded to
	 *        mark the document as superceded. 3. If the document does not
	 *        exists already then the process is halted by throwing an
	 *        Exception.
	 * @param documentId
	 * @return none
	 * @returnDescription none
	 * @throws DocumentNotExistsException
	 * @throws CouldNotSupercedeDocumentException
	 * @throws CouldNotCheckDocumentException
	 */
	public static void setDocumentToDraft(String documentId)
			throws DocumentNotExistsException,
			CouldNotSupercedeDocumentException, CouldNotCheckDocumentException {
		if (DocumentUtility.isDocumentExists(documentId)) {
			DocumentUtility
					.markDocumentSuperseded(Integer.parseInt(documentId));
		} else {
			throw new DocumentNotExistsException("No Document Found");
		}
	}

	// Use Case
	/**
	 * @name setDocumentToApproved
	 * @purpose This method is used to request setting the document to Approved
	 *          state. The document is marked to approved state.
	 * @steps 1. Checks from DocumentUtility if the Document exists or not. 2.
	 *        If the document exists, then the DocumentUtility is commanded to
	 *        mark the document as approved. 3. If the document does not exists
	 *        already then the process is halted by throwing an Exception.
	 * @param documentId
	 * @return none
	 * @returnDescription none
	 * @throws DocumentNotExistsException
	 * @throws CouldNotSupercedeDocumentException
	 * @throws CouldNotCheckDocumentException
	 * @throws DocumentNotApprovedException
	 */

	public static void setDocumentToApproved(String documentId)
			throws DocumentNotExistsException, DocumentNotApprovedException,
			CouldNotSupercedeDocumentException, CouldNotCheckDocumentException {
		if (DocumentUtility.isDocumentExists(documentId)) {
			DocumentUtility.markDocumentApproved(Integer.parseInt(documentId));
		} else {
			throw new DocumentNotExistsException("No Document Found");
		}
	}

	// Use Case
	/**
	 * @name deleteDocument
	 * @purpose This method is used to request deletion of a document from the
	 *          repository. Based on situation it may be physically deleted or
	 *          else just marked as deleted.
	 * @steps 1. Checks from DocumentUtility if the Document exists or not. 2.
	 *        If the document exists, then it checks from the DocumentUtility if
	 *        the document is a controlled type or not. 3. If the document is a
	 *        controlled type then DocumentUtility is commanded to 'mark' the
	 *        document as deleted. 4. If the document is not a controlled type
	 *        then document is physically deleted by DocumentUtility. 5. If the
	 *        document does not exists already, then the process is halted by
	 *        throwing an Exception
	 * @param documentId
	 * @return none
	 * @returnDescription none
	 * @throws DocumentNotExistsException
	 * @throws CouldNotPhysicallyDeleteException
	 * @throws CouldNotCheckDocumentException
	 * @throws CouldNotMarkDeleteException
	 */
	public static void deleteDocument(String documentId, String userId)
			throws DocumentNotExistsException,
			CouldNotPhysicallyDeleteException, CouldNotCheckDocumentException,
			CouldNotMarkDeleteException {
		deleteDocument(documentId, false, userId);
	}

	// Use Case
	/**
	 * @name deleteDocument
	 * @purpose This method is used the overloaded method to request deletion of
	 *          a document from the repository. Based on situation it may be
	 *          physically deleted or else just marked as deleted.
	 * @steps 1. Checks from DocumentUtility if the Document exists or not. 2.
	 *        If the document exists, then it checks from the DocumentUtility if
	 *        the document is a controlled type or not. 3. If the document is a
	 *        controlled type then DocumentUtility is commanded to 'mark' the
	 *        document as deleted. 4. If the document is not a controlled type
	 *        then document is physically deleted by DocumentUtility. 5. If the
	 *        document does not exists already, then the process is halted by
	 *        throwing an Exception
	 * @param documentId
	 * @return none
	 * @returnDescription none
	 * @throws DocumentNotExistsException
	 * @throws CouldNotPhysicallyDeleteException
	 * @throws CouldNotCheckDocumentException
	 * @throws CouldNotMarkDeleteException
	 */
	public static void deleteDocument(String documentId,
			boolean deletePhysically, String userId)
			throws DocumentNotExistsException,
			CouldNotPhysicallyDeleteException, CouldNotCheckDocumentException,
			CouldNotMarkDeleteException {
		if (documentId != null && !documentId.equals("0")
				&& !documentId.equals("")) {
			if (DocumentUtility.isDocumentExists(documentId)) {
				if (deletePhysically) {
					try {
						DocumentUtility.physicallyDeleteDocument(documentId,
								userId);
					} catch (DocMFormatException e) {
						logger.error("deleteDocument(String, boolean, String)",
								e);

						logger.error("deleteDocument(String, boolean, String)",
								e);
					}
				} else {
					if (DocumentUtility.isDocumentControlledType(documentId)) {
						DocumentUtility
								.markAsDeleted(documentId, false, userId);
					} else {
						try {
							DocumentUtility.physicallyDeleteDocument(
									documentId, userId);
						} catch (DocMFormatException e) {
							logger.error(
									"deleteDocument(String, boolean, String)",
									e);

							logger.error(
									"deleteDocument(String, boolean, String)",
									e);
						}
					}
				}
			} else {
				throw new DocumentNotExistsException("No document exists");
			}
		}
	}

	// Use Case
	/**
	 * @name getDocument
	 * @purpose This method is used to request the document from the repository
	 *          so that its file may be 'viewed' or metaData managed.
	 * @steps 1. Checks from DocumentUtility if the Document exists or not. 2.
	 *        If the document exists, then the DocumentUtility is commanded to
	 *        return the document object. 3. If the document does not exists
	 *        already then the process is halted by throwing an Exception. 4.
	 *        The document object is returned to the caller of this method.
	 * @param documentId
	 * @return Document
	 * @returnDescription The returned Document contains the file as well as the
	 *                    MetaData
	 * @throws CouldNotCheckDocumentException
	 * @throws DocumentNotExistsException
	 */
	public static com.mind.bean.docm.Document getDocument(String documentId,
			String docVersion) throws CouldNotCheckDocumentException,
			DocumentNotExistsException {
		com.mind.bean.docm.Document document = null;

		if (DocumentUtility.isDocumentExists(documentId)) {
			document = DocumentUtility.getDocument(documentId, docVersion);
		} else {
			throw new DocumentNotExistsException("No document exists");
		}
		return document;
	}

	/**
	 * Check document.
	 * 
	 * @param newDocument
	 *            the new document
	 * 
	 * @return true, if successful
	 */
	public static boolean checkDocument(Document newDocument) {
		return checkDocument(newDocument,
				DatabaseUtils.getDataSource(DocumentConstants.DOCM_SYSDB));
	}

	public static boolean checkDocument(Document newDocument,
			DataSource docmDataSource) {
		boolean validDoc = false;
		String libValue = "";
		boolean valid = false;
		// try {
		// valid = DocumentUtility.checkInvalidDocument(newDocument);
		// } catch (CouldNotCheckDocumentException e) {
		// e.printStackTrace();
		// }
		MetaData metaData = (MetaData) newDocument.getMetaData();
		Field[] field = metaData.getFields();
		for (int i = 0; i < field.length; i++) {
			if (field[i].getName().equals("Type")) {
				libValue = (field[i].getValue());
			}
		}
		String[] mDataField = DocumentUtility.getCompulsaryFieldForLibrary(
				libValue, docmDataSource);
		boolean compulsaryField = DocumentUtility.checkCompulsaryFieldsInDoc(
				newDocument, mDataField);
		logger.trace(compulsaryField + "::" + valid);

		if (compulsaryField == true)
			validDoc = true;

		return validDoc;
	}

	/**
	 * Gets the doc field value.
	 * 
	 * @param doc
	 *            the doc
	 * @param fieldName
	 *            the field name
	 * 
	 * @return the doc field value
	 */
	public static String getDocFieldValue(Document doc, String fieldName) {
		String fieldValue = DocumentUtility.getFieldsValue(doc, fieldName);
		return fieldValue;
	}

	/**
	 * Gets the approved doc for mpo.
	 * 
	 * @param docIdList
	 *            the doc id list
	 * 
	 * @return the approved doc for mpo
	 * @throws SQLException
	 */
	public static ArrayList getApprovedDocForMPO(String docIdList) {
		Connection conn = DatabaseUtils
				.getConnection(DocumentConstants.DOCM_SYSDB);
		Statement st = null;
		ResultSet rs = null;
		ArrayList al = new ArrayList();

		String query = "select isnull(title,''),adddate,status,dm_doc_id from dm_document_version"
				+ " where dm_doc_id in ("
				+ docIdList
				+ ") and status='Approved'";
		if (logger.isDebugEnabled()) {
			logger.debug("getApprovedDocForMPO(String) - " + query);
		}
		try {
			st = conn.createStatement();
			rs = DatabaseUtils.getRecord(query, st);
			while (rs.next()) {
				ArrayList inner = new ArrayList();
				inner.add(rs.getString(1));
				inner.add(rs.getString(2));
				inner.add(rs.getString(3));
				inner.add(rs.getString(4));
				al.add(inner);
			}

		} catch (Exception e) {
			logger.error("getApprovedDocForMPO(String)", e);

			logger.error("getApprovedDocForMPO(String)", e);
		} finally {
			DBUtil.close(rs, st);
			DBUtil.close(conn);
		}
		return al;
	}

	/**
	 * Gets the doc list for mpo.
	 * 
	 * @param docId
	 *            the doc id
	 * 
	 * @return the doc list for mpo
	 */
	public static ArrayList getDocListForMpo(String docId) {
		Document doc = null;
		ArrayList outerList = new ArrayList();
		try {
			doc = ClientOperator.getDocument(docId, "");
		} catch (CouldNotCheckDocumentException e) {
			logger.error("getDocListForMpo(String)", e);

			logger.error("getDocListForMpo(String)", e);
		} catch (DocumentNotExistsException e) {
			logger.error("getDocListForMpo(String)", e);

			logger.error("getDocListForMpo(String)", e);
		}
		if (doc != null) {
			MetaData mdata = doc.getMetaData();
			Field[] fields = mdata.getFields();
			for (int i = 0; i < fields.length; i++) {
				if (fields[i].getName().equalsIgnoreCase("title"))
					outerList.add(fields[i].getValue());
				if (fields[i].getName().equalsIgnoreCase("status"))
					outerList.add(fields[i].getValue());
				if (fields[i].getName().equalsIgnoreCase("adddate"))
					outerList.add(fields[i].getValue());
			}
		}
		return outerList;
	}
}
