package com.mind.RPT.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.rpt.JobOwnerSummaryBean;
import com.mind.common.ChangeDateFormat;
import com.mind.common.LabelValue;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.rpt.common.LiveSqlConnection;

/**
 * Discription: This class is use to perform database operation for RPT module.
 * Methods: - getSalesSummary(), getCurrentQuarter(), getLoginName(),
 * getJobPerformance(), getServiceTimeInterval(), getEInvoiceStatus(),
 * getAppendixName(), getOwnerName(), getMsaName(), getMsaList(),
 * getJobOwnerList(), getAppendixTileList() getJobSummaryScheduledTab(),
 * getJobSummaryOnsiteTab(), getJobSummaryOffsiteTab(),
 * getJobSummaryCompleteTab(), getJobSummaryNotscheduledTab(), getCCCTabInfo(),
 * getSupportAppendixList(), getDates(), getDateRange(), getcurrMonYear(),
 * getcalendarData() getMonthDay(), getDayData(), getListTitle(),
 * getToFromDate(), getJobSummaryConfirmedTab(), getProjectTeam(), getcurrDate
 * 
 * @author Vijay Kumar Singh
 * 
 */
public class RPTdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(RPTdao.class);

	/**
	 * Discription: This method calls a Database Function and gets the Sales
	 * Summary Data on the basis of between given data range.
	 * 
	 * @param todate
	 *            - To Date
	 * @param fromdate
	 *            - From Date
	 * @param ds
	 *            - Database Object
	 * @return - An ArrayList
	 */
	public static ArrayList getSalesSummary(String todate, String fromdate,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		DecimalFormat dfcur = new DecimalFormat("#,###,##0.00");
		ArrayList summarydate = new ArrayList(); // - for store SalesSummary
													// list
		SummaryList slist;
		try {

			String sql = "select * from func_dw_get_seles_info('" + todate
					+ "','" + fromdate + "')";
			// System.out.println("sql "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				slist = new SummaryList();
				slist.setLm_si_name(rs.getString("lm_si_name")); // - Site Name
				slist.setLm_js_title(rs.getString("lm_js_title")); // - Job
																	// Title
				slist.setLm_si_number(rs.getString("lm_si_number")); // - Site
																		// Number
				slist.setLm_si_state(rs.getString("lm_si_state")); // - Site
																	// State
				slist.setLm_js_closed_date(rs.getString("lm_js_closed_date")); // -
																				// Close
																				// Date
				slist.setLm_js_invoice_no(rs.getString("lm_js_invoice_no")); // -
																				// Invoice
																				// Number
				slist.setLx_ce_invoice_price("$"
						+ dfcur.format(Float.parseFloat(rs
								.getString("lx_ce_invoice_price")))); // -
																		// Invoice
																		// Price
				summarydate.add(slist);
			}
		} catch (Exception e) {
			logger.error("getSalesSummary(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSalesSummary(String, String, DataSource) - Exception in com.mind.RPT.dao.getSelesSummary() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return summarydate;
	}

	/**
	 * Discription: This method is use to get current quarter month and current
	 * year from Database.
	 * 
	 * @param ds
	 *            - Database Object
	 * @return - String[]
	 */
	public static String[] getCurrentQuarter(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] currentQuarter = new String[2]; // - for current quarter and
													// year
		try {

			String sql = "select current_quarter = datename(qq,getDate()), current_year= datename(yy,getDate())";
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				currentQuarter[0] = rs.getString("current_quarter"); // -
																		// current
																		// quarter
				currentQuarter[1] = rs.getString("current_year"); // - current
																	// year
			}
		} catch (Exception e) {
			logger.error("getCurrentQuarter(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCurrentQuarter(DataSource) - Exception in com.mind.RPT.dao.getCurrentQuarter() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return currentQuarter;
	}

	/**
	 * Discription: This method is use to get login user first and last name
	 * form database on the basis of login user id.
	 * 
	 * @param loginId
	 *            - Login user id
	 * @param ds
	 *            - Database object
	 * @return - String
	 */
	public static String getLoginName(String loginId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String loginName = ""; // - for store login name
		try {

			String sql = "select lo_pc_first_name, lo_pc_last_name from lo_poc where lo_pc_id="
					+ loginId;
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				loginName = rs.getString("lo_pc_first_name") + " "
						+ rs.getString("lo_pc_last_name"); // - Login user First
															// Name Second Name
			}
		} catch (Exception e) {
			logger.error("getLoginName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getLoginName(String, DataSource) - Exception in com.mind.RPT.dao.getLoginName() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return loginName;

	}

	/**
	 * Discription: This method is get Appendix Name from database on the basis
	 * of appendix id.
	 * 
	 * @param appendixId
	 *            - Appendix Id
	 * @param ds
	 *            - Database Object
	 * @return String - Appendix Name
	 */
	public static String getAppendixName(String appendixId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String appendixName = ""; // - for store appendix name
		try {

			String sql = "select lx_pr_title from lx_appendix_main where lx_pr_id="
					+ appendixId;
			// System.out.println("sql= "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				appendixName = rs.getString("lx_pr_title"); // - appendix name
			}
		} catch (Exception e) {
			logger.error("getAppendixName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixName(String, DataSource) - Exception in com.mind.RPT.dao.getAppendixName() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return appendixName;

	}

	/**
	 * Discription: This method return Job Owner Name from database on the basis
	 * of Owner id.
	 * 
	 * @param ownerId
	 *            - Job Owner Id
	 * @param ds
	 *            - Database Object
	 * @return String - Owner Name
	 */
	public static String getOwnerName(String ownerId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String ownerName = ""; // - for store owner name
		try {

			String sql = "select lo_pc_first_name,lo_pc_last_name from lo_poc where lo_pc_id="
					+ ownerId;
			// System.out.println("sql= "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				ownerName = rs.getString("lo_pc_first_name") + " "
						+ rs.getString("lo_pc_last_name"); // - Owner first last
															// name
			}
		} catch (Exception e) {
			logger.error("getOwnerName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOwnerName(String, DataSource) - Exception in com.mind.RPT.dao.getOwnerName() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return ownerName;

	}

	/**
	 * Discription: This method is get Msa Name form database on the basis of
	 * Appendix id.
	 * 
	 * @param appendixId
	 *            - Appendix id
	 * @param ds
	 *            - Database Object
	 * @return String - Msa Name
	 */
	public static String getMsaName(String appendixId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String msaName = ""; // for msa name
		try {

			String sql = "select lo_ot_name from lp_msa_main inner join lo_organization_top on  lo_ot_id = lp_mm_ot_id "
					+ "inner join lx_appendix_main on lp_mm_id = lx_pr_mm_id where lx_pr_id="
					+ appendixId;
			// System.out.println("sql= "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				msaName = rs.getString("lo_ot_name"); // - Msa name
			}
		} catch (Exception e) {
			logger.error("getMsaName(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsaName(String, DataSource) - Exception in com.mind.RPT.dao.getMsaName() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return msaName;

	}

	/**
	 * Discription: This method get Msa List form database.
	 * 
	 * @param ds
	 *            - Database object
	 * @return ArrayList - Msa List
	 */
	public static ArrayList getMsaList(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList valueList = new ArrayList(); // to store msa list
		try {

			String sql = "select lp_mm_id, lo_ot_name from dw_msa_list";
			// System.out.println("sql= "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			int i = 0;
			if (i == 0) {
				JobOwnerSummaryBean jobsummary = new JobOwnerSummaryBean();
				jobsummary.setMsaId("0");
				jobsummary.setMsaName("");
				valueList.add(i, jobsummary);
				i++;
			}
			while (rs.next()) {
				JobOwnerSummaryBean jobsummary = new JobOwnerSummaryBean();
				jobsummary.setMsaId(rs.getString("lp_mm_id")); // - Msa Id
				jobsummary.setMsaName(rs.getString("lo_ot_name")); // - Msa Name
				valueList.add(i, jobsummary);
				i++;
			}
		} catch (Exception e) {
			logger.error("getMsaList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getMsaList(DataSource) - Exception in com.mind.RPT.dao.getMsaList() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return valueList;

	}

	/**
	 * Discription: This method is use get Job Performance data from database on
	 * the basis of Login user id.
	 * 
	 * @param loginId
	 *            - Login user id
	 * @param ds
	 *            - Database object
	 * @return ArrayList - Job Performance data
	 */
	public static ArrayList getJobPerformance(String loginId, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		DashboardBean dashboardbean;
		ArrayList performancelist = new ArrayList(); // - for store Job
														// Performance data
		DecimalFormat dfcur = new DecimalFormat("#,###,##0.00");
		try {

			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call dw_job_performance(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, loginId);
			rs = cstmt.executeQuery();
			while (rs.next()) {
				dashboardbean = new DashboardBean();
				dashboardbean.setColName(rs.getString("job_status")); // - Job
																		// Status:
																		// Pending,Scheduled,Onsite,Offsite,Complete,Closed
				dashboardbean.setCount(rs.getString("count_status")); // - sum
																		// of
																		// every
																		// Job
																		// Status

				if (!rs.getString("count_status").equals("")
						&& Integer.parseInt(rs.getString("count_status")) < 1) { // when
																					// count_status
																					// is
																					// 0
					dashboardbean.setRevenue("N/A");
					dashboardbean.setAveVGPM("N/A");
				} else {
					if (!rs.getString("total_revenu").equals("")) { // when
																	// total in
																	// not blank
						dashboardbean.setRevenue("$"
								+ dfcur.format(Float.parseFloat(rs
										.getString("total_revenu")))); // Total
																		// Revenu
					} else {
						dashboardbean.setRevenue(rs.getString("total_revenu")); // -
																				// Total
																				// Revenu
					}

					dashboardbean.setAveVGPM(rs.getString("ave_vgmp")); // -
																		// Average
																		// VGMP
				}
				performancelist.add(dashboardbean);
			}
		} catch (Exception e) {
			logger.error("getJobPerformance(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobPerformance(String, DataSource) - Exception in com.mind.RPT.dao.getJobPerformance() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return performancelist;
	}

	/**
	 * Discription: This method is use to get total service time interval in
	 * month from database on the basis of Login user id.
	 * 
	 * @param loginId
	 *            - Login user id
	 * @param ds
	 *            - Database object
	 * @return String - total service time
	 */
	public static String getServiceTimeInterval(String loginId, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String interval = ""; // - to store time interval
		DecimalFormat dfcurmargin = new DecimalFormat("0.0");
		try {

			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call dw_month_interval(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, loginId);
			rs = cstmt.executeQuery();
			if (rs.next()) {
				interval = dfcurmargin
						.format(Float.parseFloat(rs.getString(1))); // - Total
																	// service
																	// time
																	// interval
																	// in no. of
																	// month
			}
		} catch (Exception e) {
			logger.error("getServiceTimeInterval(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getServiceTimeInterval(String, DataSource) - Exception in com.mind.RPT.dao.getServiceTimeInterval() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return interval;
	}

	/**
	 * Discription: This method is use to get eInvoice Status data from database
	 * on the basis of Login user id.
	 * 
	 * @param loginId
	 *            - Login user id
	 * @param ds
	 *            - Database object
	 * @return ArrayList- eInvoice data
	 */
	public static ArrayList getEInvoiceStatus(String loginId, DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		DashboardBean dashboardbean;
		ArrayList performancelist = new ArrayList();
		DecimalFormat dfcur = new DecimalFormat("#,###,##0.00");
		try {

			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call dw_einvoice_status(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, loginId);
			rs = cstmt.executeQuery();
			while (rs.next()) {
				dashboardbean = new DashboardBean();
				dashboardbean.setColName(rs.getString("pending_status")); // -
																			// Job
																			// Status:
																			// Pending,Scheduled,Onsite,Offsite,Complete,Closed
				dashboardbean.setCount(rs.getString("count_status")); // - sum
																		// of
																		// every
																		// Job
																		// Status

				if (!rs.getString("count_status").equals("")
						&& Integer.parseInt(rs.getString("count_status")) < 1) { // when
																					// count_status
																					// is
																					// 0
					dashboardbean.setRevenue("N/A");
					dashboardbean.setAveVGPM("N/A");
				} else {
					if (!rs.getString("total_cost").equals("")) { // - when
																	// total in
																	// not blank
						dashboardbean.setRevenue("$"
								+ dfcur.format(Float.parseFloat(rs
										.getString("total_cost")))); // - Total
																		// Revenu
					} else {
						dashboardbean.setRevenue(rs.getString("total_cost")); // -
																				// Total
																				// Revenu
					}

					dashboardbean.setAveVGPM(rs.getString("ave_delay")); // -
																			// Average
																			// VGMP
				}
				performancelist.add(dashboardbean);
			}
		} catch (Exception e) {
			logger.error("getEInvoiceStatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEInvoiceStatus(String, DataSource) - Exception in com.mind.RPT.dao.getEInvoiceStatus() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return performancelist;
	}

	/**
	 * Discription: This method get list of job owner list form database.
	 * 
	 * @param ds
	 *            - Database object
	 * @return ArrayList- Owner List
	 */
	public static ArrayList getJobOwnerList(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList ownerList = new ArrayList(); // to Store Job Owner List

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from dbo.func_dw_job_owner_list()";
			// System.out.println(" query is ::::::::::::::::::: >"+sql);
			rs = stmt.executeQuery(sql);
			ownerList.add(new com.mind.common.LabelValue("---Select---", "0"));

			while (rs.next()) {
				ownerList.add(new com.mind.common.LabelValue(rs
						.getString("owner_name"), rs.getString("owner_id"))); // -
																				// Owner
																				// Name
																				// and
																				// Owner
																				// name
			}

		} catch (Exception e) {
			logger.error("getJobOwnerList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobOwnerList(DataSource) - Exception in com.mind.RPT.dao.getJobOwnerList "
						+ e);
			}
			logger.error("getJobOwnerList(DataSource)", e);
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return ownerList;
	}

	/**
	 * Discription: This method get Appendix List from database on the basis of
	 * msa id.
	 * 
	 * @param ds
	 *            - Database object
	 * @return ArrayList- Appendix List
	 */
	public static ArrayList getAppendixTitleList(String msaId, int count,
			DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList appendixList = new ArrayList(); // - to store appendix list
		if (count == 0) {
			appendixList.add(new com.mind.common.LabelValue("--Select--", "0")); // -
																					// fist
																					// row
																					// of
																					// ArrayList
		}

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = "select lx_pr_id,lx_pr_title from lx_appendix_main where lx_pr_status in ('I') and lx_pr_mm_id="
					+ msaId + " order by lx_pr_title";
			// System.out.println(" query is ::::::::::::::::::: >"+sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				appendixList.add(new com.mind.common.LabelValue(rs
						.getString("lx_pr_title"), rs.getString("lx_pr_id"))); // -
																				// appendix
																				// title
																				// ,
																				// appendix
																				// id
			}

		} catch (Exception e) {
			logger.error("getAppendixTitleList(String, int, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAppendixTitleList(String, int, DataSource) - Exception in com.mind.RPT.dao.getAppendixTileList "
						+ e);
			}
			logger.error("getAppendixTitleList(String, int, DataSource)", e);
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return appendixList;
	}

	/**
	 * Discription: This method get data for Job Summary Scheduled tab form
	 * database.
	 * 
	 * @param ownerId
	 *            - Owner Id
	 * @param appendixid
	 *            - Appendix Id
	 * @param toDate
	 *            - To Date
	 * @param fromDate
	 *            - From Date
	 * @param ds
	 *            - Datbase Object
	 * @return ArrayList - list job summay data according to tab.
	 */
	public static ArrayList getJobSummaryScheduledTab(String ownerId,
			String appendixid, String fromDate, String toDate, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();

		// System.out.println("ownerId= "+ownerId);
		// System.out.println("appendixid= "+appendixid);
		// System.out.println("toDate = "+toDate);
		// System.out.println("fromDate = "+fromDate);

		if (toDate == null) {
			toDate = "";
		}
		if (fromDate == null) {
			fromDate = "";
		}
		if (ownerId == null || ownerId.equals("")) {
			ownerId = "0";
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_job_owner_summary_scheduled_tab(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(ownerId));
			cstmt.setString(3, appendixid);
			cstmt.setString(4, fromDate);
			cstmt.setString(5, toDate);
			rs = cstmt.executeQuery();

			while (rs != null && rs.next()) {
				JobOwnerSummary JobSummary = new JobOwnerSummary();
				JobSummary.setAppendix_id(rs.getString("appendix_id")); // -
																		// Appendix
																		// Id
				JobSummary.setJob_id(rs.getString("job_id")); // - Job Id
				JobSummary.setJob_name(rs.getString("job_name")); // - Job Name
				JobSummary.setTicket_id(rs.getString("ticket_id")); // - Ticket
																	// Id
				JobSummary.setPlanned_start_date(rs
						.getString("planned_start_date")); // - Planned Start
															// Date
				JobSummary.setPlanned_start_time(rs
						.getString("planned_start_time")); // - Planned Start
															// Time
				JobSummary.setSite_city(rs.getString("site_city")); // - Job
																	// Site city
																	// name
				JobSummary.setSite_id(rs.getString("site_id")); // - Job Site id
				JobSummary.setSite_name(rs.getString("site_name")); // - Job
																	// Site Name
				JobSummary.setSite_number(rs.getString("site_number")); // - Job
																		// Site
																		// Number
				JobSummary.setSite_state(rs.getString("site_state")); // - Site
																		// State
				JobSummary.setSite_time_zone(rs.getString("site_time_zone")); // -
																				// Site
																				// Time
																				// Zone
				JobSummary.setMsa_title(rs.getString("msa_title"));
				JobSummary.setAppendix_title(rs.getString("appendix_title"));
				JobSummary.setProject_type(rs.getString("project_type"));
				list.add(JobSummary);
			}

		} catch (Exception e) {
			logger.error(
					"getJobSummaryScheduledTab(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobSummaryScheduledTab(String, String, String, String, DataSource) - Exception in com.mind.RPT.dao.getJobSummaryScheduledTab() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return list;

	}

	/**
	 * Discription: This method get data for Onsite tab of Job Summary form
	 * database.
	 * 
	 * @param ownerId
	 *            - Owner Id
	 * @param appendixid
	 *            - Appendix Id
	 * @param toDate
	 *            - To Date
	 * @param fromDate
	 *            - From Date
	 * @param ds
	 *            - Datbase Object
	 * @return ArrayList - list job summay data according to tab.
	 */
	public static ArrayList getJobSummaryOnsiteTab(String ownerId,
			String appendixid, String fromDate, String toDate, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();

		// System.out.println("ownerId= "+ownerId);
		// System.out.println("appendixid= "+appendixid);
		// System.out.println("toDate = "+toDate);
		// System.out.println("fromDate = "+fromDate);

		if (toDate == null) {
			toDate = "";
		}
		if (fromDate == null) {
			fromDate = "";
		}
		if (ownerId == null || ownerId.equals("")) {
			ownerId = "0";
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_job_owner_summary_onsite_tab(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(ownerId));
			cstmt.setString(3, appendixid);
			cstmt.setString(4, fromDate);
			cstmt.setString(5, toDate);
			rs = cstmt.executeQuery();

			while (rs != null && rs.next()) {
				JobOwnerSummary JobSummary = new JobOwnerSummary();
				JobSummary.setActual_start_date(rs
						.getString("actual_start_date")); // - Actual Start Date
				JobSummary.setActual_start_time(rs
						.getString("actual_start_time")); // - Actual Start Time
				JobSummary.setAppendix_id(rs.getString("appendix_id")); // -
																		// Appendix
																		// Id
				JobSummary.setJob_id(rs.getString("job_id")); // - Job Id
				JobSummary.setJob_name(rs.getString("job_name")); // - Job Name
				JobSummary.setTicket_id(rs.getString("ticket_id")); // - Ticket
																	// Id
				JobSummary.setPlanned_start_date(rs
						.getString("planned_start_date")); // - Planned Start
															// Date
				JobSummary.setPlanned_start_time(rs
						.getString("planned_start_time")); // - Planned Start
															// Time
				JobSummary.setSite_city(rs.getString("site_city")); // - Job
																	// Site city
																	// name
				JobSummary.setSite_id(rs.getString("site_id")); // - Job Site id
				JobSummary.setSite_name(rs.getString("site_name")); // - Job
																	// Site Name
				JobSummary.setSite_number(rs.getString("site_number")); // - Job
																		// Site
																		// Number
				JobSummary.setSite_state(rs.getString("site_state")); // - Site
																		// State
				JobSummary.setSite_time_zone(rs.getString("site_time_zone")); // -
																				// Site
																				// Time
																				// Zone
				JobSummary.setMsa_title(rs.getString("msa_title"));
				JobSummary.setAppendix_title(rs.getString("appendix_title"));
				JobSummary.setProject_type(rs.getString("project_type"));
				list.add(JobSummary);
			}

		} catch (Exception e) {
			logger.error(
					"getJobSummaryOnsiteTab(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobSummaryOnsiteTab(String, String, String, String, DataSource) - Exception in com.mind.RPT.dao.getJobSummaryOnsiteTab() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return list;

	}

	/**
	 * Discription: This method get data for Offsite tab of Job Summary
	 * Scheduled form database.
	 * 
	 * @param ownerId
	 *            - Owner Id
	 * @param appendixid
	 *            - Appendix Id
	 * @param toDate
	 *            - To Date
	 * @param fromDate
	 *            - From Date
	 * @param ds
	 *            - Datbase Object
	 * @return ArrayList - list job summay data according to tab.
	 */
	public static ArrayList getJobSummaryOffsiteTab(String ownerId,
			String appendixid, String fromDate, String toDate, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		DecimalFormat decimal = new DecimalFormat("0.0");
		// System.out.println("ownerId= "+ownerId);
		// System.out.println("appendixid= "+appendixid);
		// System.out.println("toDate = "+toDate);
		// System.out.println("fromDate = "+fromDate);

		if (toDate == null) {
			toDate = "";
		}
		if (fromDate == null) {
			fromDate = "";
		}
		if (ownerId == null || ownerId.equals("")) {
			ownerId = "0";
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_job_owner_summary_offsite_tab(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(ownerId));
			cstmt.setString(3, appendixid);
			cstmt.setString(4, fromDate);
			cstmt.setString(5, toDate);
			rs = cstmt.executeQuery();

			while (rs != null && rs.next()) {
				JobOwnerSummary JobSummary = new JobOwnerSummary();
				JobSummary.setActual_start_date(rs
						.getString("actual_start_date")); // - Actual Start Date
				JobSummary.setActual_start_time(rs
						.getString("actual_start_time")); // - Actual Start Time
				JobSummary.setActual_end_date(rs.getString("actual_end_date")); // -
																				// Actual
																				// End
																				// Date
				JobSummary.setActual_end_time(rs.getString("actual_end_time")); // -
																				// Actual
																				// End
																				// Time
				JobSummary.setAppendix_id(rs.getString("appendix_id")); // -
																		// Appendix
																		// Id
				JobSummary.setJob_id(rs.getString("job_id")); // - Job Id
				JobSummary.setJob_name(rs.getString("job_name")); // - Job Name
				JobSummary.setTicket_id(rs.getString("ticket_id")); // - Ticket
																	// Id
				JobSummary.setSite_city(rs.getString("site_city")); // - Job
																	// Site city
																	// name
				JobSummary.setSite_id(rs.getString("site_id")); // - Job Site id
				JobSummary.setSite_name(rs.getString("site_name")); // - Job
																	// Site Name
				JobSummary.setSite_number(rs.getString("site_number")); // - Job
																		// Site
																		// Number
				JobSummary.setSite_state(rs.getString("site_state")); // - Site
																		// State
				JobSummary.setSite_time_zone(rs.getString("site_time_zone")); // -
																				// Site
																				// Time
																				// Zone
				JobSummary.setTime_on(decimal.format(Float.parseFloat(rs
						.getString("time_on")))); // - Total time on Site as
													// Hour XX.X
				JobSummary.setMsa_title(rs.getString("msa_title"));
				JobSummary.setAppendix_title(rs.getString("appendix_title"));
				JobSummary.setProject_type(rs.getString("project_type"));
				list.add(JobSummary);
			}

		} catch (Exception e) {
			logger.error(
					"getJobSummaryOffsiteTab(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobSummaryOffsiteTab(String, String, String, String, DataSource) - Exception in com.mind.RPT.dao.getJobSummaryOffsiteTab() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return list;

	}

	/**
	 * Discription: This method get data for Complete tab of Job Summary
	 * Scheduled form database.
	 * 
	 * @param ownerId
	 *            - Owner Id
	 * @param appendixid
	 *            - Appendix Id
	 * @param toDate
	 *            - To Date
	 * @param fromDate
	 *            - From Date
	 * @param ds
	 *            - Datbase Object
	 * @return ArrayList - list job summay data according to tab.
	 */
	public static ArrayList getJobSummaryCompleteTab(String ownerId,
			String appendixid, String fromDate, String toDate, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		DecimalFormat dfcur = new DecimalFormat("#,###,##0.00");
		DecimalFormat decimal = new DecimalFormat("0.0");
		// System.out.println("ownerId= "+ownerId);
		// System.out.println("appendixid= "+appendixid);
		// System.out.println("toDate = "+toDate);
		// System.out.println("fromDate = "+fromDate);

		if (toDate == null) {
			toDate = "";
		}
		if (fromDate == null) {
			fromDate = "";
		}
		if (ownerId == null || ownerId.equals("")) {
			ownerId = "0";
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_job_owner_summary_complete_tab(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(ownerId));
			cstmt.setString(3, appendixid);
			cstmt.setString(4, fromDate);
			cstmt.setString(5, toDate);
			rs = cstmt.executeQuery();

			while (rs != null && rs.next()) {
				JobOwnerSummary JobSummary = new JobOwnerSummary();
				JobSummary.setActual_cost("$"
						+ dfcur.format(Float.parseFloat(rs
								.getString("actual_cost")))); // - Actual Cost
				JobSummary.setActual_end_date(rs.getString("actual_end_date")); // -
																				// Actual
																				// End
																				// Date
				JobSummary.setActual_end_time(rs.getString("actual_end_time")); // -
																				// Actual
																				// End
																				// Time
				JobSummary.setActual_start_date(rs
						.getString("actual_start_date")); // - Actual Start Date
				JobSummary.setActual_start_time(rs
						.getString("actual_start_time")); // - Actual End Time
				JobSummary.setAppendix_id(rs.getString("appendix_id")); // -
																		// Appendix
																		// Id
				JobSummary.setEstimated_cost("$"
						+ dfcur.format(Float.parseFloat(rs
								.getString("estimated_cost")))); // - Estimated
																	// Cost
				JobSummary.setInvoiced_price("$"
						+ dfcur.format(Float.parseFloat(rs
								.getString("invoiced_price")))); // - Invoiced
																	// price
				JobSummary.setJob_id(rs.getString("job_id")); // - Job Id
				JobSummary.setJob_name(rs.getString("job_name")); // - Job Name
				JobSummary.setTicket_id(rs.getString("ticket_id")); // - Ticket
																	// Id
				JobSummary.setSite_city(rs.getString("site_city")); // - Job
																	// Site city
																	// name
				JobSummary.setSite_id(rs.getString("site_id")); // - Job Site id
				JobSummary.setSite_name(rs.getString("site_name")); // - Job
																	// Site Name
				JobSummary.setSite_number(rs.getString("site_number")); // - Job
																		// Site
																		// Number
				JobSummary.setSite_state(rs.getString("site_state")); // - Site
																		// State
				JobSummary.setSite_time_zone(rs.getString("site_time_zone")); // -
																				// Site
																				// Time
																				// Zone
				JobSummary.setTime_on(decimal.format(Float.parseFloat(rs
						.getString("time_on")))); // - Total time on Site as
													// Hour XX.X
				JobSummary.setMsa_title(rs.getString("msa_title"));
				JobSummary.setAppendix_title(rs.getString("appendix_title"));
				JobSummary.setProject_type(rs.getString("project_type"));
				list.add(JobSummary);
			}

		} catch (Exception e) {
			logger.error(
					"getJobSummaryCompleteTab(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobSummaryCompleteTab(String, String, String, String, DataSource) - Exception in com.mind.RPT.dao.getJobSummaryCompleteTab() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return list;

	}

	/**
	 * Discription: This method get data for Notscheduled tab of Job Summary
	 * Scheduled form database.
	 * 
	 * @param ownerId
	 *            - Owner Id
	 * @param appendixid
	 *            - Appendix Id
	 * @param toDate
	 *            - To Date
	 * @param fromDate
	 *            - From Date
	 * @param ds
	 *            - Datbase Object
	 * @return ArrayList - list job summay data according to tab.
	 */
	public static ArrayList getJobSummaryNotscheduledTab(String ownerId,
			String appendixid, String fromDate, String toDate, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		SimpleDateFormat dateFormatDB = new SimpleDateFormat(
				"yyyy-MM-dd hh:mm:ss");

		// System.out.println("ownerId= "+ownerId);
		// System.out.println("appendixid= "+appendixid);
		// System.out.println("toDate = "+toDate);
		// System.out.println("fromDate = "+fromDate);

		if (toDate == null) {
			toDate = "";
		}
		if (fromDate == null) {
			fromDate = "";
		}
		if (ownerId == null || ownerId.equals("")) {
			ownerId = "0";
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_job_owner_summary_notscheduled_tab(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(ownerId));
			cstmt.setString(3, appendixid);
			cstmt.setString(4, fromDate);
			cstmt.setString(5, toDate);
			rs = cstmt.executeQuery();

			while (rs != null && rs.next()) {
				JobOwnerSummary JobSummary = new JobOwnerSummary();
				JobSummary.setAppendix_id(rs.getString("appendix_id")); // -
																		// Appendix
																		// Id
				JobSummary.setCreated(rs.getString("created")); // - Created
																// date
				JobSummary.setJob_id(rs.getString("job_id")); // - Job Id
				JobSummary.setTicket_id(rs.getString("ticket_id")); // - Ticket
																	// Id
				JobSummary.setJob_name(rs.getString("job_name")); // - Job Name
				JobSummary.setSite_city(rs.getString("site_city")); // - Job
																	// Site city
																	// name
				JobSummary.setSite_id(rs.getString("site_id")); // - Job Site id
				JobSummary.setSite_name(rs.getString("site_name")); // - Job
																	// Site Name
				JobSummary.setSite_number(rs.getString("site_number")); // - Job
																		// Site
																		// Number
				JobSummary.setSite_state(rs.getString("site_state")); // - Site
																		// State
				JobSummary.setSite_time_zone(rs.getString("site_time_zone")); // -
																				// Site
																				// Time
																				// Zone
				JobSummary.setMsa_title(rs.getString("msa_title"));
				JobSummary.setAppendix_title(rs.getString("appendix_title"));
				JobSummary.setProject_type(rs.getString("project_type"));
				// added for requested schedule date
				if (rs.getDate("requested_date") != null)
					JobSummary.setRequested_date(dateFormat.format(dateFormatDB
							.parse(rs.getString("requested_date"))));
				list.add(JobSummary);
			}

		} catch (Exception e) {
			logger.error(
					"getJobSummaryNotscheduledTab(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobSummaryNotscheduledTab(String, String, String, String, DataSource) - Exception in com.mind.RPT.dao.getJobSummaryNotscheduledTab() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return list;

	}

	/**
	 * Discription: This method get data for ccc tab from MySQL.
	 * 
	 * @param mySqlUrl
	 *            - Url of My SQL
	 * @param ownerId
	 *            - Owner Id
	 * @param toDate
	 *            - To Date
	 * @param fromDate
	 *            - From Date
	 * @param appendixid
	 *            - Appendix id
	 * @param ds
	 *            - Database Object
	 * @return ArrayList - list of ccc tab data
	 */
	public static ArrayList getCCCTabInfo(String mySqlUrl, String ownerId,
			String toDate, String fromDate, String appendixid, DataSource ds) {

		Connection mySqlConn = null;
		Statement mySqlStmt = null;
		ResultSet rs = null;
		String sql = "";
		String ownername = "";
		String from = "1900-01-01"; // - when toDate is null or blank
		String to = "9999-01-01"; // - when fromDate is null or blank
		ArrayList list = new ArrayList();
		String allActiveOwnerName = RPTdao.getAllActiveOwnerName(ds);

		if (ownerId != null && !ownerId.equals("0")) {
			ownername = RPTdao.getOwnerName(ownerId, ds); // - get Owner name
		}
		if (toDate != null && !toDate.equals("")) {
			to = ChangeDateFormat.getDateFormat(toDate, "yyyy-MM-dd",
					"MM/dd/yyyy"); // - covert dtate form mm/dd/yyyy to
									// yyyy-mm-dd
		}
		if (fromDate != null && !fromDate.equals("")) {
			from = ChangeDateFormat.getDateFormat(fromDate, "yyyy-MM-dd",
					"MM/dd/yyyy"); // - covert dtate form mm/dd/yyyy to
									// yyyy-mm-dd
		}

		try {
			mySqlConn = com.mind.common.util.MySqlConnection
					.getMySqlConnection(mySqlUrl);
			int i = 0;
			mySqlStmt = mySqlConn.createStatement();
			sql = "SELECT "
					+ "il_ji_lm_si_number, "
					+ // - Site Number
					"il_ji_lm_si_city, "
					+ // - Site City
					"il_ji_lm_si_state, "
					+ // - Site State
					"il_ji_status, "
					+ // - Job Status
					"ifnull(date_format(il_ji_date_scheduled,'%m/%d/%Y'),'') as il_ji_date_scheduled, "
					+ // - Scheduled Date mm/dd/yy
					"ifnull(date_format(il_ji_date_onsite,'%m/%d/%Y %H:%i %p'),'') as il_ji_date_onsite, "
					+ // - OnSite Date mm/dd//yy
					"ifnull(date_format(il_ji_date_offsite,'%m/%d/%Y %H:%i %p'),'') as il_ji_date_offsite, "
					+ // - OffSite Date mm/dd//yy
					"ifnull(date_format(il_ji_date_complete,'%m/%d/%Y'),'') as il_ji_date_complete, "
					+ // - Job Complete Date mm/dd/yy
					"ifnull(date_format(il_ji_date_invoiced,'%m/%d/%Y'),'') as il_ji_date_invoiced, "
					+ // - Job Invoiced Date mm/dd/yy
					"il_ji_lm_js_invoice_no, "
					+ // - Jog Invoice Number
					"il_ji_ci_lx_pr_id, "
					+ // - Appendix Id
					"il_ci_lx_pr_title, "
					+ "il_ci_lo_ot_name "
					+ "FROM il_job_integration i "
					+ // - Table Name li_job_integration
					"inner join  il_core_project_references p on p.il_ci_lx_pr_id = il_ji_ci_lx_pr_id "
					+ "inner join il_core_customer_references m on m.il_ci_lp_mm_id = p.il_ci_lp_mm_id "
					+ "where ifnull(il_ji_date_scheduled,'1900-01-01')>='"
					+ from
					+ "' and ifnull(il_ji_date_scheduled,'1900-01-01') <= '"
					+ to + "' ";
			if (ownerId != null && !ownerId.equals("0")) {
				sql = sql + "and il_ji_created_by='" + ownername + "' ";
			} else {
				sql = sql + "and il_ji_created_by in (" + allActiveOwnerName
						+ ") ";
			}

			if (appendixid != null && !appendixid.equals("0")) {
				sql = sql + "and il_ji_ci_lx_pr_id=" + appendixid + " ";
			}
			sql = sql
					+ "order by il_ci_lo_ot_name,il_ci_lx_pr_title,il_ji_date_scheduled";
			// System.out.println("sql= "+sql);

			rs = mySqlStmt.executeQuery(sql);
			while (rs.next()) {
				JobOwnerSummary JobSummary = new JobOwnerSummary();
				JobSummary.setMs_appendix_id(rs.getString("il_ji_ci_lx_pr_id"));
				JobSummary
						.setMs_site_number(rs.getString("il_ji_lm_si_number"));
				JobSummary.setMs_site_city(rs.getString("il_ji_lm_si_city"));
				JobSummary.setMs_site_state(rs.getString("il_ji_lm_si_state"));
				JobSummary.setMs_job_status(rs.getString("il_ji_status"));
				JobSummary.setMs_scheduled_date(rs
						.getString("il_ji_date_scheduled"));
				JobSummary.setMs_onsite_date(rs.getString("il_ji_date_onsite"));
				JobSummary.setMs_offsite_date(rs
						.getString("il_ji_date_offsite"));
				JobSummary.setMs_job_complete_date(rs
						.getString("il_ji_date_complete"));
				JobSummary.setMs_job_invoiced_date(rs
						.getString("il_ji_date_invoiced"));
				JobSummary.setMs_job_invoiced_no(rs
						.getString("il_ji_lm_js_invoice_no"));
				JobSummary.setMsa_appendix_title(rs
						.getString("il_ci_lo_ot_name")
						+ ", "
						+ rs.getString("il_ci_lx_pr_title"));
				list.add(JobSummary);
			}

		} catch (Exception e) {
			logger.error(
					"getCCCTabInfo(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCCCTabInfo(String, String, String, String, String, DataSource) - Exception in com.mind.RPT.dao.getCCCTabInfo() "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, mySqlStmt);
			DBUtil.close(mySqlConn);
		}
		return list;
	}

	/**
	 * Discription: This method is use to get all active user name from
	 * Database.
	 * 
	 * @param ds
	 *            - Database Object
	 * @return String - Active Owner Name
	 */
	public static String getAllActiveOwnerName(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String activeOwnerName = ""; // - for current quarter and year
		try {

			String sql = "select owner_name from dbo.func_dw_job_owner_list()";
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				activeOwnerName = activeOwnerName + "'"
						+ rs.getString("owner_name") + "',"; // - All Active
																// owner's name
			}
		} catch (Exception e) {
			logger.error("getAllActiveOwnerName(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getAllActiveOwnerName(DataSource) - Exception in com.mind.RPT.dao.getAllActiveOwnerName() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		if (!activeOwnerName.equals("") && activeOwnerName.lastIndexOf(',') > 0) {
			activeOwnerName = activeOwnerName.substring(0,
					activeOwnerName.lastIndexOf(','));
		}
		return activeOwnerName;
	}

	/**
	 * Discription: This method get job and its site info List form database for
	 * Support section.
	 * 
	 * @param todate
	 *            - To Date
	 * @param fromdate
	 *            - From Date
	 * @param appedixid
	 *            - Appendix Id
	 * @return ArrayList- Appendix List
	 */
	public static ArrayList getSupportAppendixList(String fromdate,
			String todate, String appedixid) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList summarydate = new ArrayList();
		Support support;
		DecimalFormat dfcur = new DecimalFormat("#,###,##0.00");
		try {

			conn = LiveSqlConnection.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_get_appendix_jobsite_summary_list(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(appedixid));
			cstmt.setString(3, fromdate);
			cstmt.setString(4, todate);
			rs = cstmt.executeQuery();
			while (rs.next()) {
				support = new Support();
				support.setJobName(rs.getString("lm_js_title")); // - Job Name
				support.setSiteAddress(rs.getString("lm_si_address")); // - Job
																		// Site
																		// Address
				support.setSiteCity(rs.getString("lm_si_city")); // - Job Site
																	// City
				support.setSiteState(rs.getString("lm_si_state")); // - Job Site
																	// State
				support.setSiteZip(rs.getString("lm_si_zip_code")); // - Job
																	// Site Zip
																	// Code
				support.setQwestRequest(rs
						.getString("lm_js_customer_reference")); // - Customer
																	// Reference
				support.setLastSiteVisit(rs.getString("lx_se_start") + " "
						+ rs.getString("lx_se_start_time"));// - Actual
															// start/end date
				support.setInstall("$"
						+ dfcur.format(Float.parseFloat(rs.getString("Install")))); // Install
																					// Invoice
																					// Price
				support.setTnm("$"
						+ dfcur.format(Float.parseFloat(rs.getString("T&M")))); // T&M
																				// Invoice
																				// Price
				support.setTotal("$"
						+ dfcur.format(Float.parseFloat(rs.getString("Total")))); // Total
																					// Invoice
																					// Price
				summarydate.add(support);
			}
		} catch (Exception e) {
			logger.error("getSupportAppendixList(String, String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSupportAppendixList(String, String, String) - Exception in com.mind.RPT.dao.getSupportAppendixList() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return summarydate;
	}

	/**
	 * Discription: This method get Ticket and its site information list form
	 * database for Support section.
	 * 
	 * @param todate
	 *            - To Date
	 * @param fromdate
	 *            - From Date
	 * @param appedixid
	 *            - Appendix Id
	 * @return ArrayList- Appendix List
	 */
	public static ArrayList getSupportNetMedX(String fromdate, String todate,
			String appedixid) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList summarydate = new ArrayList();
		Support support;

		// System.out.println("todate = "+todate);
		// System.out.println("fromdate = "+fromdate);
		// System.out.println("appedixid = "+appedixid);

		try {

			conn = LiveSqlConnection.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_get_netmedx_jobsite_summary_list(?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(appedixid));
			cstmt.setString(3, fromdate);
			cstmt.setString(4, todate);
			rs = cstmt.executeQuery();
			while (rs.next()) {
				support = new Support();
				support.setSiteNumber(rs.getString("lm_si_number")); // - Job
																		// Name
				support.setSiteCity(rs.getString("lm_si_city")); // - Job Site
																	// City
				support.setSiteState(rs.getString("lm_si_state")); // - Job Site
																	// State
				support.setTicketProDesc(rs.getString("lm_tc_problem_desc")); // -
																				// Job
																				// Site
																				// Zip
																				// Code
				support.setOnSiteDate(rs.getString("lx_se_start"));
				support.setOnSiteTime(rs.getString("lx_se_start_time"));// -
																		// Actual
																		// start/end
																		// date
				support.setOffSiteTime(rs.getString("lx_se_end_time"));
				support.setClosingSummary(rs.getString("lm_in_notes"));
				summarydate.add(support);
			}
		} catch (Exception e) {
			logger.error("getSupportNetMedX(String, String, String)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSupportNetMedX(String, String, String) - Exception in com.mind.RPT.dao.getSupportNetMedX() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return summarydate;
	}

	/**
	 * Discription: This method get data range of getdate-5 and getdate+24 and 5
	 * more dates/days according to condition, from database
	 * 
	 * @param format
	 *            - mm/dd/yy or Mon dd
	 * @param ds
	 *            - Database Object
	 * @return String[] - Date Range
	 */
	public static String[] getDates(String fromDate, String format,
			DataSource ds) {
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String[] dates = new String[35]; // - to store 35 days or dates
		int i = -1;
		fromDate = ChangeDateFormat.getDateFormat(fromDate,
				"yyyy-MM-dd HH:mm:ss.ms", "MM/dd/yyyy");
		try {

			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call dw_get_dates(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, format);
			cstmt.setString(3, fromDate);
			rs = cstmt.executeQuery();
			while (rs.next()) {
				dates[++i] = rs.getString(1); // - Dates
			}
		} catch (Exception e) {
			logger.error("getDates(String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDates(String, String, DataSource) - Exception in com.mind.RPT.dao.getDateList() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return dates;
	}

	/**
	 * Discription: This method get data range of getdate-5 and getdate+24.
	 * 
	 * @param ds
	 *            - Database object
	 * @return String[] - Date Range
	 */
	public static String[] getDateRange(String fromDate, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String[] daterange = new String[30]; // - to store 30 dates
		fromDate = ChangeDateFormat.getDateFormat(fromDate,
				"yyyy-MM-dd HH:mm:ss.ms", "MM/dd/yyyy");
		int i = -1;
		try {

			String sql = "declare @count int declare @date datetime "
					+ "set @date = '"
					+ fromDate
					+ "' "
					+ "set @date = @date "
					+ "declare @table table(date varchar(10)) "
					+ "set @count = 0 while(@count < 30) "
					+ "begin "
					+ "insert into @table select convert(varchar(10),@date+@count,101) "
					+ "set @count = @count+1 " + "end "
					+ "select * from @table";
			// System.out.println("sql "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				daterange[++i] = rs.getString(1); // - dates
			}
		} catch (Exception e) {
			logger.error("getDateRange(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDateRange(String, DataSource) - Exception in com.mind.RPT.dao.getDateRange() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return daterange;
	}

	/**
	 * Discription: This method get current Date from Database.
	 * 
	 * @param ds
	 *            - Database Object
	 * @return String - MonYYYY
	 */
	public static String getcurrMonYear(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String currMonYear = ""; // - to Store monyear

		try {
			String sql = "select convert(varchar(20),getdate()-5,101)";
			// System.out.println("sql "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				currMonYear = rs.getString(1); // - form date mm/dd/yy to Mon YY
			}
		} catch (Exception e) {
			logger.error("getcurrMonYear(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getcurrMonYear(DataSource) - Exception in com.mind.RPT.dao.getcurrMonYear() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return currMonYear;
	}

	/**
	 * Discription: This method get current Date from Database.
	 * 
	 * @param ds
	 *            - Database Object
	 * @return String - MonYYYY
	 */
	public static String getcurrDate(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String currDate = ""; // - to Store monyear

		try {
			String sql = "select convert(varchar(10),getdate(),101)";
			// System.out.println("sql "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				currDate = rs.getString(1); // - form date mm/dd/yy to Mon YY
			}
		} catch (Exception e) {
			logger.error("getcurrDate(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getcurrDate(DataSource) - Exception in com.mind.RPT.dao.getcurrDate() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return currDate;
	}

	/**
	 * Discription: This method check login user is in all active employees and
	 * its termination within the last 60 days.
	 * 
	 * @param ds
	 *            - Database Object
	 * @return String - Job owner id
	 */
	public static String getActiovOwnerId(String loginId, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String OwnerId = "0"; // - to Store monyear

		try {
			String sql = "select top 1 lm_js_created_by"
					+ " from lm_job inner join lo_poc on lo_pc_id=lm_js_created_by"
					+ " where dbo.func_cc_poc_name(lm_js_created_by) is not null"
					+ " and (lo_pc_termination_date >= getdate()-60 or lo_pc_termination_date is null)"
					+ " and lo_pc_role in (5,6) and lm_js_created_by = "
					+ loginId;
			// System.out.println("sql "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				OwnerId = rs.getString(1); // - form date mm/dd/yy to Mon YY
			}
		} catch (Exception e) {
			logger.error("getActiovOwnerId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getActiovOwnerId(String, DataSource) - Exception in com.mind.RPT.dao.getcurrMonYear() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return OwnerId;
	}

	/**
	 * Discription: This method get toDate or fromDate from Database.
	 * 
	 * @param ds
	 *            - Database Object
	 * @return String - date
	 */
	public static String getToFromDate(String str, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String date = ""; // - to Store date
		String sql = "";

		try {
			if (str.endsWith("fromDate")) {
				sql = "select convert(varchar(20),getdate()-29,101)";
			}
			if (str.endsWith("toDate")) {
				sql = "select convert(varchar(20),getdate(),101)";
			}
			// System.out.println("sql "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				date = rs.getString(1); // - date mm/dd/yy
			}
		} catch (Exception e) {
			logger.error("getToFromDate(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getToFromDate(String, DataSource) - Exception in com.mind.RPT.dao.getToFromDate() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return date;
	}

	/**
	 * Discription: This method is use to create a calendar month. Get All
	 * current calendar dates form database and store dates into TreeMap.
	 * 
	 * @param ds
	 *            - Database Object
	 * @return TreeMap - Current calendar Dates
	 */
	public static TreeMap getcalendarData(String fromDate, DataSource ds) {

		TreeMap week = new TreeMap();
		String[] dates = RPTdao.getDates(fromDate, "dates", ds); // - All
																	// current
																	// calendar
																	// dates

		for (int j = 0; j < 5; j++) { // - create only 5 weeks for calendar
			week.put(j, getMonthDay(dates, j, ds)); // - Store 7 days into a
													// week
		}
		return week;
	}

	/**
	 * Discription: This method is use to create 7 days of every week of
	 * calendar.
	 * 
	 * @param dates
	 *            - Dates of current calendar month
	 * @param j
	 *            - week no.
	 * @param ds
	 *            - Database object
	 * @return TreeMap - Dates of week
	 */
	public static TreeMap getMonthDay(String[] dates, int j, DataSource ds) {

		TreeMap monthDay = new TreeMap(); // - to store week days

		int count = -1;
		int range = -1;
		switch (j) {
		case 0:
			count = 0;
			range = 6; // - 1 week range
			break;
		case 1:
			count = 7;
			range = 13; // - 2 week range
			break;
		case 2:
			count = 14;
			range = 20; // - 3 week range
			break;
		case 3:
			count = 21;
			range = 27; // - 4 week range
			break;
		case 4:
			count = 28;
			range = 34; // - 5 week range
			break;
		}
		for (int i = count; i <= range; i++) {
			monthDay.put(i, dates[i]);
		}
		return monthDay;
	}

	/**
	 * Discription: This method is use to get list of Site data of Sheduled,
	 * Offsite and Onsite jobs. All Site data is between range of getdate-5 and
	 * getdate+24.
	 * 
	 * @param appendixId
	 *            - Appendix id
	 * @param ownerId
	 *            - Owner Id
	 * @param ds
	 *            - Database Object
	 * @return ArrayList - Site data list
	 */
	public static ArrayList getDayData(String appendixId, String ownerId,
			String fromDate, String toDate, String srPMI, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList calendarData = new ArrayList(); // - to Store site date
		CalendarData caldata;

		// System.out.println("appendixId = "+appendixId);
		// System.out.println("ownerId = "+ownerId);

		try {

			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_job_owner_summary_calendar_info(?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, ownerId);
			cstmt.setString(3, appendixId);
			cstmt.setString(4, fromDate);
			cstmt.setString(5, toDate);
			cstmt.setString(6, srPMI);
			rs = cstmt.executeQuery();
			while (rs.next()) {
				caldata = new CalendarData();
				caldata.setDate_list(rs.getString("date_list")); // - Planned
																	// Start
																	// Data or
																	// Actual
																	// Start
																	// Date
				caldata.setPlanned_start_time(rs
						.getString("planned_start_time")); // - Planned Start
															// Time
				caldata.setActual_start_time(rs.getString("actual_start_time")); // -
																					// Actual
																					// Start
																					// Time
				caldata.setAppendix_id(rs.getString("appendix_id")); // -
																		// Appendix
																		// Id
				caldata.setJob_id(rs.getString("job_id")); // - Job Id
				caldata.setTicket_id(rs.getString("ticket_id")); // - Ticket Id
				caldata.setSite_number(rs.getString("site_number")); // - Site
																		// Number
				caldata.setStatus(rs.getString("status")); // - Job States
				caldata.setAppendix_title(rs.getString("appendix_title")); // -
																			// Appendix
																			// Name
				caldata.setProject_type(rs.getString("project_type")); // -
																		// Project
																		// Type
				calendarData.add(caldata);
			}
		} catch (Exception e) {
			logger.error(
					"getDayData(String, String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDayData(String, String, String, String, DataSource) - Exception in com.mind.RPT.dao.getDayData() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return calendarData;

	}

	public static boolean checkUser(String loginId, DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		boolean statusUser = true;// - Check that user is right or not

		try {
			String sql = "select * from lo_poc where lo_pc_role in (5,6) and lo_pc_id = "
					+ loginId;
			// System.out.println("sql "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				statusUser = false;
			}
		} catch (Exception e) {
			logger.error("checkUser(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("checkUser(String, DataSource) - Exception in com.mind.RPT.dao.checkUser() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return statusUser;
	}

	/**
	 * Discription: This method get data for Job Summary Confirmed tab form
	 * database.
	 * 
	 * @param ownerId
	 *            - Owner Id
	 * @param appendixid
	 *            - Appendix Id
	 * @param toDate
	 *            - To Date
	 * @param fromDate
	 *            - From Date
	 * @param ds
	 *            - Datbase Object
	 * @return ArrayList - list job summay data according to tab.
	 */
	public static ArrayList getJobSummaryConfirmedTab(String ownerId,
			String appendixid, String fromDate, String toDate, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		JobOwnerSummary jobSummary;
		// System.out.println("ownerId= "+ownerId);
		// System.out.println("appendixid= "+appendixid);
		// System.out.println("toDate = "+toDate);
		// System.out.println("fromDate = "+fromDate);

		if (toDate == null) {
			toDate = "";
		}
		if (fromDate == null) {
			fromDate = "";
		}
		if (ownerId == null || ownerId.equals("")) {
			ownerId = "0";
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_job_owner_summary_confirmed_tab(?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(ownerId));
			cstmt.setString(3, appendixid);
			cstmt.setString(4, fromDate);
			cstmt.setString(5, toDate);
			rs = cstmt.executeQuery();

			while (rs != null && rs.next()) {
				jobSummary = new JobOwnerSummary();
				jobSummary.setAppendix_id(rs.getString("appendix_id")); // -
																		// Appendix
																		// Id
				jobSummary.setJob_id(rs.getString("job_id")); // - Job Id
				jobSummary.setJob_name(rs.getString("job_name")); // - Job Name
				jobSummary.setTicket_id(rs.getString("ticket_id")); // - Ticket
																	// Id
				jobSummary.setPlanned_start_date(rs
						.getString("planned_start_date")); // - Planned Start
															// Date
				jobSummary.setPlanned_start_time(rs
						.getString("planned_start_time")); // - Planned Start
															// Time
				jobSummary.setSite_city(rs.getString("site_city")); // - Job
																	// Site city
																	// name
				jobSummary.setSite_id(rs.getString("site_id")); // - Job Site id
				jobSummary.setSite_name(rs.getString("site_name")); // - Job
																	// Site Name
				jobSummary.setSite_number(rs.getString("site_number")); // - Job
																		// Site
																		// Number
				jobSummary.setSite_state(rs.getString("site_state")); // - Site
																		// State
				jobSummary.setSite_time_zone(rs.getString("site_time_zone")); // -
																				// Site
																				// Time
																				// Zone
				jobSummary.setMsa_title(rs.getString("msa_title"));
				jobSummary.setAppendix_title(rs.getString("appendix_title"));
				jobSummary.setPartner_name(rs.getString("partner_name"));
				jobSummary.setPoNo(rs.getString("po_no"));
				jobSummary.setDelivery(rs.getString("delivery"));
				jobSummary.setProject_type(rs.getString("project_type"));
				list.add(jobSummary);
			}

		} catch (Exception e) {
			logger.error(
					"getJobSummaryConfirmedTab(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getJobSummaryConfirmedTab(String, String, String, String, DataSource) - Exception in com.mind.RPT.dao.getJobSummaryConfirmedTab() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return list;

	}

	/**
	 * Discription: This method is get all team member list for all appendix
	 * from database.
	 * 
	 * @param ds
	 *            - Database Object
	 * @return ArrayList - List of Project Team Member
	 */
	public static ArrayList getProjectTeam(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		list.add(new com.mind.common.LabelValue("--Select--", "0")); // - fist
																		// row
																		// of
																		// List

		try {
			String sql = "select * from dw_prj_team_member";
			// System.out.println("sql= "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("poc_name"), rs.getString("poc_id"))); // -
																			// rest
																			// row
																			// of
																			// ArrayList
			}
		} catch (Exception e) {
			logger.error("getProjectTeam(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getProjectTeam(DataSource) - Exception in com.mind.RPT.dao.getProjectTeam() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return list;

	}

	/**
	 * Discription: This method is get all sheduler list for all job from
	 * database.
	 * 
	 * @param ds
	 *            - Database Object
	 * @return ArrayList - List of Scheduler
	 */
	public static ArrayList getScheduler(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		list.add(new com.mind.common.LabelValue("--Select--", "0")); // - fist
																		// row
																		// of
																		// List

		try {
			String sql = "select * from dw_js_scheduler";
			// System.out.println("sql= "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("poc_name"), rs.getString("poc_id"))); // -
																			// rest
																			// row
																			// of
																			// ArrayList
			}
		} catch (Exception e) {
			logger.error("getScheduler(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getScheduler(DataSource) - Exception in com.mind.RPT.dao.getScheduler() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return list;

	}

	public static List<LabelValue> getSrPrjManagerList(DataSource ds) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<LabelValue> srPrjManagerList = new ArrayList<LabelValue>();

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			StringBuilder sqlQuery = new StringBuilder(
					"	select lo_pc_id ,lo_pc_first_name,lo_pc_last_name ");
			sqlQuery.append("  from lo_poc join lo_poc_rol on lo_pc_id = lo_pr_pc_id and lo_pr_ro_id = ");
			sqlQuery.append(" (select lo_ro_id from lo_role role where lo_ro_role_name='SPM') ");
			sqlQuery.append(" where lo_pc_id in (select distinct lm_ap_pc_id from lm_appendix_poc  ");
			sqlQuery.append(" where lm_ap_pc_id not in (7,32)) order by lo_pc_last_name ");
			rs = stmt.executeQuery(sqlQuery.toString());
			srPrjManagerList.add(new LabelValue("---Select---", "0"));

			while (rs.next()) {
				srPrjManagerList.add(new LabelValue(rs
						.getString("lo_pc_first_name")
						+ " "
						+ rs.getString("lo_pc_last_name"), rs
						.getString("lo_pc_id")));

			}

		} catch (Exception e) {
			logger.error("getSrPrjManagerList(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSrPrjManagerList(DataSource) - Exception in com.mind.RPT.dao.getSrPrjManagerList "
						+ e);
			}
			logger.error("getSrPrjManagerList(DataSource)", e);
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return srPrjManagerList;
	}
}
