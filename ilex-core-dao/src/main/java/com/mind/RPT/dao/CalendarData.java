package com.mind.RPT.dao;

public class CalendarData {
	
	private String date_list = null;
	private String planned_start_time = null;
	private String actual_start_time = null;
	private String appendix_id = null;
	private String job_id = null;
	private String ticket_id = null;
	private String site_number = null;
	private String status = null;
	private String appendix_title = null;
	private String project_type = null;
	
	public String getActual_start_time() {
		return actual_start_time;
	}
	public void setActual_start_time(String actual_start_time) {
		this.actual_start_time = actual_start_time;
	}
	public String getAppendix_id() {
		return appendix_id;
	}
	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}
	public String getDate_list() {
		return date_list;
	}
	public void setDate_list(String date_list) {
		this.date_list = date_list;
	}
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}
	public String getPlanned_start_time() {
		return planned_start_time;
	}
	public void setPlanned_start_time(String planned_start_time) {
		this.planned_start_time = planned_start_time;
	}
	public String getSite_number() {
		return site_number;
	}
	public void setSite_number(String site_number) {
		this.site_number = site_number;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAppendix_title() {
		return appendix_title;
	}
	public void setAppendix_title(String appendix_title) {
		this.appendix_title = appendix_title;
	}
	public String getTicket_id() {
		return ticket_id;
	}
	public void setTicket_id(String ticket_id) {
		this.ticket_id = ticket_id;
	}
	public String getProject_type() {
		return project_type;
	}
	public void setProject_type(String project_type) {
		this.project_type = project_type;
	}
	
	
	

}
