package com.mind.RPT.dao;

public class SummaryList {

	private String lm_si_name = null;
	private String lm_js_title = null;
	private String lm_si_number = null;
	private String lm_si_state = null;
	private String lm_js_closed_date = null;
	private String lm_js_invoice_no = null;
	private String lx_ce_invoice_price = null;
	
	
	
	public String getLm_js_closed_date() {
		return lm_js_closed_date;
	}
	public void setLm_js_closed_date(String lm_js_closed_date) {
		this.lm_js_closed_date = lm_js_closed_date;
	}
	public String getLm_js_invoice_no() {
		return lm_js_invoice_no;
	}
	public void setLm_js_invoice_no(String lm_js_invoice_no) {
		this.lm_js_invoice_no = lm_js_invoice_no;
	}
	public String getLm_js_title() {
		return lm_js_title;
	}
	public void setLm_js_title(String lm_js_title) {
		this.lm_js_title = lm_js_title;
	}
	public String getLm_si_name() {
		return lm_si_name;
	}
	public void setLm_si_name(String lm_si_name) {
		this.lm_si_name = lm_si_name;
	}
	public String getLm_si_number() {
		return lm_si_number;
	}
	public void setLm_si_number(String lm_si_number) {
		this.lm_si_number = lm_si_number;
	}
	public String getLm_si_state() {
		return lm_si_state;
	}
	public void setLm_si_state(String lm_si_state) {
		this.lm_si_state = lm_si_state;
	}
	public String getLx_ce_invoice_price() {
		return lx_ce_invoice_price;
	}
	public void setLx_ce_invoice_price(String lx_ce_invoice_price) {
		this.lx_ce_invoice_price = lx_ce_invoice_price;
	}
	
	
	
	

}
