package com.mind.RPT.dao;

public class DailyBuzzReport {
	
	private String msa_title = null;
	private String appendix_title = null;
	private String job_id = null;
	private String job_name = null;
	private String planned_start_date = null;
	private String planned_start_time = null;
	private String site_number = null;
	private String site_city = null;
	private String site_state = null;
	private String appendix_id = null;
	private String actual_start_date = null;
	private String actual_start_time = null;
	private String actual_end_date = null;
	private String actual_end_time = null;
	
	
	public String getActual_end_date() {
		return actual_end_date;
	}
	public void setActual_end_date(String actual_end_date) {
		this.actual_end_date = actual_end_date;
	}
	public String getActual_end_time() {
		return actual_end_time;
	}
	public void setActual_end_time(String actual_end_time) {
		this.actual_end_time = actual_end_time;
	}
	public String getActual_start_date() {
		return actual_start_date;
	}
	public void setActual_start_date(String actual_start_date) {
		this.actual_start_date = actual_start_date;
	}
	public String getActual_start_time() {
		return actual_start_time;
	}
	public void setActual_start_time(String actual_start_time) {
		this.actual_start_time = actual_start_time;
	}
	public String getAppendix_id() {
		return appendix_id;
	}
	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}
	public String getAppendix_title() {
		return appendix_title;
	}
	public void setAppendix_title(String appendix_title) {
		this.appendix_title = appendix_title;
	}
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}
	public String getJob_name() {
		return job_name;
	}
	public void setJob_name(String job_name) {
		this.job_name = job_name;
	}
	public String getMsa_title() {
		return msa_title;
	}
	public void setMsa_title(String msa_title) {
		this.msa_title = msa_title;
	}
	public String getPlanned_start_date() {
		return planned_start_date;
	}
	public void setPlanned_start_date(String planned_start_date) {
		this.planned_start_date = planned_start_date;
	}
	public String getPlanned_start_time() {
		return planned_start_time;
	}
	public void setPlanned_start_time(String planned_start_time) {
		this.planned_start_time = planned_start_time;
	}
	public String getSite_city() {
		return site_city;
	}
	public void setSite_city(String site_city) {
		this.site_city = site_city;
	}
	public String getSite_number() {
		return site_number;
	}
	public void setSite_number(String site_number) {
		this.site_number = site_number;
	}
	public String getSite_state() {
		return site_state;
	}
	public void setSite_state(String site_state) {
		this.site_state = site_state;
	}
}
