package com.mind.RPT.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.rpt.SnaponInfo;
import com.mind.bean.rpt.XMLTagName;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.rpt.common.UtilityMethods;

public class SnapOndao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SnapOndao.class);

	/**
	 * Discription: This method iterater list.
	 * 
	 * @param list
	 * @return
	 */
	public static ArrayList setSnapOn(ArrayList list, DataSource ds) {
		/**
		 * Get existance site numbers for snapon from database. First iterator
		 * list. Check the existance of site number on every iteration. Call a
		 * method for store list data to database on every iteration. Calulate
		 * Created, Updated, Not found, Having any blank field on every
		 * iteration. Save this Calculated value into an ArryaList for return.
		 */

		int createdCount = 0;
		int updatedCount = 0;
		int notfountCount = 0;
		String action = "";
		StringBuffer notfoundSiNo = new StringBuffer(); // - to store comma
														// seperated string
		StringBuffer foundSiNo = new StringBuffer(); // - to store comma
														// seperated string
		ArrayList info = new ArrayList(); // - to save summary
		SnaponInfo snaponInfo = new SnaponInfo();
		String[] siteNo = getSnaponSiteNo("0", ds).split("~!~"); // -
																	// 5000~!~3000~!~5000~!~..............
		String tempDealerNumber = "";

		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {

			XMLTagName xmlTagName = (XMLTagName) iterator.next();

			/* check the existance of site number */
			tempDealerNumber = removeFirstChar(xmlTagName.getDealernumber());
			if (siteExist(siteNo, tempDealerNumber)) { // - when site is exist
				action = saveSnapOn(tempDealerNumber,
						xmlTagName.getDefaultgatewayaddress(),
						xmlTagName.getPrimarydns(),
						xmlTagName.getSecondarydns(),
						xmlTagName.getStaticgatewayaddress(),
						xmlTagName.getSubnetmask(),
						xmlTagName.getBroadcastid(), xmlTagName.getNetworkid(),
						xmlTagName.getIprange(),
						xmlTagName.getSerManagerName(),
						xmlTagName.getSerManagerPhone(),
						xmlTagName.getItAdminName(),
						xmlTagName.getItAdminPhone(),
						xmlTagName.getOktoconfig(), xmlTagName.getPcData(), ds);

				if (action.trim().equals("A")) {
					createdCount = createdCount + 1;
				} // end of if
				else if (action.trim().equals("U")) {
					updatedCount = updatedCount + 1;
				} // - end of else if

				if (xmlTagName.getDefaultgatewayaddress().equals("")
						|| xmlTagName.getPrimarydns().equals("")
						|| xmlTagName.getSecondarydns().equals("")
						|| xmlTagName.getStaticgatewayaddress().equals("")
						|| xmlTagName.getSubnetmask().equals("")
						|| xmlTagName.getBroadcastid().equals("")
						|| xmlTagName.getNetworkid().equals("")
						|| xmlTagName.getIprange().equals("")) {

					foundSiNo.append(tempDealerNumber + ", ");
				} // -end of if

			} // - end of if
			else { // - when site is not exist
				notfountCount = notfountCount + 1;
				notfoundSiNo = notfoundSiNo.append(tempDealerNumber + ", ");
			} // - end of else

		} // - end of while

		/* Save Summary */
		snaponInfo.setCreated(createdCount + "");
		snaponInfo.setUpdated(updatedCount + "");
		snaponInfo.setNotFound(notfountCount + "");
		snaponInfo.setNotFoundsiteNo(UtilityMethods.removeLastComm(notfoundSiNo
				.toString()));
		snaponInfo.setFoundsiteNo(UtilityMethods.removeLastComm(foundSiNo
				.toString()));
		info.add(snaponInfo);

		return info;
	}

	/**
	 * Discription: This method is used to save dealer's record into database.
	 * 
	 * @param dealerNumber
	 *            -- dealerNumber
	 * @param defaultGatewayaddress
	 *            -- Default Gatewayaddress
	 * @param primaryDns
	 *            -- Primary Dns
	 * @param secondaryDns
	 *            -- Secondary Dns
	 * @param staticGatewayaddress
	 *            -- Static Gatewayaddress
	 * @param subnetMask
	 *            -- Subnet Mask
	 * @param iprange
	 *            -- IP Range
	 * @param serManagerName
	 *            -- Service Manager Name
	 * @param serManagerPhone
	 *            -- Service Manager Phone
	 * @param itAdminName
	 *            -- IT Administrator Name
	 * @param itAdminPhone
	 *            -- IT Administrator Phone
	 * @param oktoconfig
	 *            -- Oktoconfiguration
	 * @param pcData
	 *            -- '|' & '~' seperated data
	 * @param ds
	 *            -- Database Object
	 * @return action -- A/U
	 */
	private static String saveSnapOn(String dealerNumber,
			String defaultGatewayaddress, String primaryDns,
			String secondaryDns, String staticGatewayaddress,
			String subnetMask, String broadcastId, String networkId,
			String iprange, String serManagerName, String serManagerPhone,
			String itAdminName, String itAdminPhone, String oktoconfig,
			String pcData, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		String action = "";

		/*
		 * System.out.println("2 "+ dealerNumber ); System.out.println("3 "+
		 * staticGatewayaddress ); System.out.println("4 "+ subnetMask );
		 * System.out.println("5 "+ defaultGatewayaddress );
		 * System.out.println("6 "+ primaryDns ); System.out.println("7 "+
		 * secondaryDns ); System.out.println("8 "+ broadcastId);
		 * System.out.println("9 "+ networkId ); System.out.println("10 "+
		 * iprange ); System.out.println("11 "+ serManagerName );
		 * System.out.println("12 "+ serManagerPhone );
		 * System.out.println("13 "+ itAdminName ); System.out.println("14 "+
		 * itAdminPhone ); System.out.println("15 "+ oktoconfig );
		 * System.out.println("16 "+ pcData );
		 */

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_snapon_manage_01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"); // -
																									// 16
																									// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, dealerNumber);
			cstmt.setString(3, staticGatewayaddress);
			cstmt.setString(4, subnetMask);
			cstmt.setString(5, defaultGatewayaddress);
			cstmt.setString(6, primaryDns);
			cstmt.setString(7, secondaryDns);
			cstmt.setString(8, broadcastId);
			cstmt.setString(9, networkId);
			cstmt.setString(10, iprange);
			cstmt.setString(11, serManagerName);
			cstmt.setString(12, serManagerPhone);
			cstmt.setString(13, itAdminName);
			cstmt.setString(14, itAdminPhone);
			cstmt.setString(15, oktoconfig);
			cstmt.setString(16, pcData);
			cstmt.registerOutParameter(17, java.sql.Types.VARCHAR);
			cstmt.execute();
			action = cstmt.getString(17);

		} catch (Exception e) {
			logger.error(
					"saveSnapOn(String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("saveSnapOn(String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, DataSource) - Error occured com.mind.RPT.dao.saveSnapOn() "
						+ e);
			}
		}

		finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);

		}
		return action;
	}

	/**
	 * Discription: This method is used to remove first char when first char is
	 * 0 of given string.
	 * 
	 * @param str
	 *            -- Delear name
	 * @return str -- Delear name
	 */
	private static String removeFirstChar(String str) {
		if (str.length() > 1) {
			if (str.substring(0, 1).equals("0")) {
				str = str.substring(1);
			}
		}
		return str;
	}

	/**
	 * Discription: This method is used to compare a string value to an string
	 * array's value.
	 * 
	 * @param array
	 *            -- An array of site numbers
	 * @param str
	 *            -- A site number of compare
	 * @return check -- true when site number in site numbers array
	 */
	private static boolean siteExist(String[] array, String str) {
		boolean check = false;
		for (int i = 0; i < array.length; i++) {
			if (array[i].equals(str)) {
				check = true;
			}
		}
		return check;
	}

	/**
	 * Discription: This method is used to get all site number which are using
	 * in snapon and seperate all with ~!~.
	 * 
	 * @param appendixId
	 *            -- Appendix Id
	 * @param ds
	 *            -- Database object
	 * @return Stirng -- ~!~ seperated string of site numbers
	 */
	private static String getSnaponSiteNo(String appendixId, DataSource ds) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select * from func_lm_snapon_siteno(?)");
			pstmt.setString(1, appendixId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				sb.append(rs.getString("site_number") + "~!~"); // - Site
																// Number~!~Site
																// Number~!~SiteNumber~!~
			}

		} catch (Exception e) {
			logger.error("getSnaponSiteNo(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSnaponSiteNo(String, DataSource) - Error occured com.mind.RPT.dao.getSnaponSiteNo() "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);

		}
		return sb.toString();
	}
}
