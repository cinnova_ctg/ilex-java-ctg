package com.mind.RPT.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.bean.rpt.SnaponCallBean;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * Discription: This class is use to perform database operation for Snap-on Pre
 * Call Tracking Report and Form. Methods: - getDailyCnfJobList(), getList(),
 * addList(), getResult(), getDailySchJobList(), setSnaponCall(),
 * getSnaponCallInfo(), getSnCallStatus(), getResult(), getDealerContact()
 * 
 * @author MIND
 */
public class DailySnaponReportdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DailySnaponReportdao.class);

	/**
	 * Discription: This method is return list of confirm Snapon jobs list.
	 * 
	 * @param date
	 *            -- Date
	 * @param ds
	 *            -- Database object
	 * @return allSnaponJobs -- List of confirm snapon job list
	 */
	public static ArrayList getCnfJobList(String date, DataSource ds) {
		/**
		 * First get list of all confirm jobs from database and store it into an
		 * Arraylist(tempList). Divided this confirm job list according to
		 * comfirm call status and store into defferent ArrayList. Add all these
		 * ArrayList into an single ArrayList(allSnaponJobs) to return.
		 */
		ArrayList allSnaponJobs = new ArrayList(3);
		ArrayList day5Call = new ArrayList();
		ArrayList hour24Call = new ArrayList();
		ArrayList reayToGo = new ArrayList();
		ArrayList escalate = new ArrayList();
		ArrayList rescheduled = new ArrayList();
		ArrayList tempList = new ArrayList();
		tempList = getDailyCnfJobList(date, ds);

		Iterator iterator = tempList.iterator();
		while (iterator.hasNext()) {
			DailySnapon dailySnapon = (DailySnapon) iterator.next();

			if (dailySnapon.getCall_status().equals("5_day_call_pending"))
				day5Call.add(getList(dailySnapon));

			else if (dailySnapon.getCall_status()
					.equals("24_hour_call_pending"))
				hour24Call.add(getList(dailySnapon));

			else if (dailySnapon.getCall_status().equals("pre_calls_completed"))
				reayToGo.add(getList(dailySnapon));

			else if (dailySnapon.getCall_status().equals("escalate"))
				escalate.add(getList(dailySnapon));

			else if (dailySnapon.getCall_status().equals("rescheduled"))
				rescheduled.add(getList(dailySnapon));

		} // - End of while loop
		allSnaponJobs.add(addList(day5Call, hour24Call, reayToGo, escalate,
				rescheduled)); // - list of all confirm jobs

		return allSnaponJobs;
	} // - end of getCnfJobList() method

	/**
	 * Discription: Only for Store into object.
	 * 
	 * @param dailySnapon
	 * @return
	 */
	private static DailySnapon getList(DailySnapon dailySnapon) {
		DailySnapon snapon = new DailySnapon();
		snapon.setJob_id(dailySnapon.getJob_id());
		snapon.setAppendix_id(dailySnapon.getAppendix_id());
		snapon.setJob_name(dailySnapon.getJob_name());
		snapon.setPlanned_start_date(dailySnapon.getPlanned_start_date());
		snapon.setPlanned_start_time(dailySnapon.getPlanned_start_time());
		snapon.setSite_city(dailySnapon.getSite_city());
		snapon.setSite_number(dailySnapon.getSite_number());
		snapon.setSite_state(dailySnapon.getSite_state());
		snapon.setDay5_1(dailySnapon.getDay5_1());
		snapon.setDay5_2(dailySnapon.getDay5_2());
		snapon.setHour24(dailySnapon.getHour24());
		return snapon;
	} // - End of getList() method

	/**
	 * Discription: This method is used to store all kind of confirm job into an
	 * single ArrayList.
	 * 
	 * @param day5Call
	 *            -- list of 5 Day Call Pending jobs
	 * @param hour24Call
	 *            -- 24 Hour call Pending jobs
	 * @param completeCall
	 *            -- Reay to Go jobs
	 * @param escalate
	 *            -- Escalate jobs
	 * @param rescheduled
	 *            -- Rescheduled jobs
	 * @return allSnapon -- return list of all kind of confirm jobs
	 */
	private static DailySnapon addList(ArrayList day5Call,
			ArrayList hour24Call, ArrayList completeCall, ArrayList escalate,
			ArrayList rescheduled) {
		/**
		*/
		DailySnapon allSnapon = new DailySnapon();
		allSnapon.setDay5CallList(day5Call); // - list of 5 Day Call Pending
												// jobs
		allSnapon.setHour24CallList(hour24Call); // - 24 Hour call Pending jobs
		allSnapon.setReadyToGo(completeCall); // - Reay to Go jobs
		allSnapon.setEscalate(escalate); // - Escalate jobs
		allSnapon.setRescheduled(rescheduled); // - Rescheduled jobs
		return allSnapon;
	} // - End of addList() method

	/**
	 * Discription: This method get Job list for Snapon form database on the
	 * basis of Confirm Starus/Date.
	 * 
	 * @param date
	 *            - Date
	 * @param ds
	 *            - Datbase Object
	 * @return ArrayList - list of all Snapon confirm jobs.
	 */
	private static ArrayList getDailyCnfJobList(String date, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_daily_snapon_cnf_jobs_view_01(?)}"); // -
																					// 1
																					// parameters
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, date);
			rs = cstmt.executeQuery();
			list = getResult(rs, "cnf"); // - Get list of confirmed jobs

		} catch (Exception e) {
			logger.error("getDailyCnfJobList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDailyCnfJobList(String, DataSource) - Exception in com.mind.RPT.dao.DailyBuzzReportdao.getDailyCnfJobList() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return list;

	} // - End of getDailyCnfJobList() method

	/**
	 * Discription: This method get Job list for Snapon form database on the
	 * basis of Scheduled Starus/Date.
	 * 
	 * @param date
	 *            - Date
	 * @param ds
	 *            - Datbase Object
	 * @return ArrayList - list of all Snapon Scheduled jobs.
	 */
	public static ArrayList getSchJobList(String date, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_daily_snapon_sch_jobs_view_01(?)}"); // -
																					// 1
																					// parameters
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, date);
			rs = cstmt.executeQuery();
			list = getResult(rs, "sch"); // - Get scheduled job list

		} catch (Exception e) {
			logger.error("getSchJobList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSchJobList(String, DataSource) - Exception in com.mind.RPT.dao.DailyBuzzReportdao.getDailySchJobList() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return list;

	} // - End of getDailySchJobList() method

	/**
	 * Discription This method is used to store resultset values into an
	 * ArrayList.
	 * 
	 * @param rs
	 *            -- List of confirm or shceduled jobs
	 * @return list -- List of confirm or shceduled jobs
	 * @throws Exception
	 */
	private static ArrayList getResult(ResultSet rs, String status)
			throws Exception {

		ArrayList list = new ArrayList(); // - To Store ResultSet value

		while (rs != null && rs.next()) {
			DailySnapon dailySnapon = new DailySnapon();
			if (status.equals("cnf")) { // - For Confirm jobs only
				dailySnapon.setCall_status(rs.getString("call_status"));
				dailySnapon.setDay5_1(rs.getString("day5_1")); // - 5_1 Day Call
																// Pending
				dailySnapon.setDay5_2(rs.getString("day5_2")); // - 5_2 Day Call
																// Pending
				dailySnapon.setHour24(rs.getString("hour24")); // - 24 Hour Call
																// Pending
			} // - End of if
			dailySnapon.setJob_id(rs.getString("job_id")); // - Job Id
			dailySnapon.setAppendix_id(rs.getString("appendix_id")); // -
																		// Appendix
																		// Id
			dailySnapon.setJob_name(rs.getString("job_name")); // - Job Name
			dailySnapon.setPlanned_start_date(rs
					.getString("planned_start_date")); // - Planned Start Date
			dailySnapon.setPlanned_start_time(rs
					.getString("planned_start_time")); // - Planned Start Time
			dailySnapon.setSite_city(rs.getString("site_city")); // - Job Site
																	// city name
			dailySnapon.setSite_number(rs.getString("site_number")); // - Job
																		// Site
																		// Number
			dailySnapon.setSite_state(rs.getString("site_state")); // - Site
																	// State
			list.add(dailySnapon);
		} // - End of While

		return list;
	} // - End of getResultSet() method

	/**
	 * Discription: This method get info of Snapon call form database on the
	 * basis of job id.
	 * 
	 * @param date
	 *            - Date
	 * @param ds
	 *            - Datbase Object
	 * @return ArrayList - list of all Snapon Scheduled jobs.
	 */
	public static SnaponCallBean getSnaponCallInfo(SnaponCallBean snaponCall,
			DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		String[] info = getDealerContact(snaponCall.getJobId(), ds); // - Get
																		// job's
																		// Dealer
																		// info

		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call dw_get_snapon_call_info(?)}"); // -
																				// 1
																				// parameters
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, snaponCall.getJobId());
			rs = cstmt.executeQuery();
			getResult(snaponCall, rs, info);
		} // - End of try
		catch (Exception e) {
			logger.error("getSnaponCallInfo(SnaponCallForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSnaponCallInfo(SnaponCallForm, DataSource) - Exception in com.mind.RPT.dao.DailyBuzzReportdao.getSnaponCallInfo() "
						+ e);
			}
		} // - End of catch
		finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		} // - End of finally

		return snaponCall;

	} // - End of getDailySchJobList() method

	/**
	 * Discription: This method fill the resultset values into formbean element.
	 * 
	 * @param snaponCall
	 * @param rs
	 * @param info
	 * @throws SQLException
	 */
	private static void getResult(SnaponCallBean snaponCall, ResultSet rs,
			String[] info) throws SQLException {
		/**
		 * Store ResultSet into FormBean. Check Contact filed of all statuses.
		 * If Call-1 contact is null then store Default contact into Call-1
		 * contact. If Call-2 contact is null then store Call-1 contact into
		 * Call-2 contact. If Call-2 status is Not Applicable then store Call-1
		 * contact into 24-Call contact. If 24-Call contact is null the store
		 * Call-2 contact into 24-Call contact. Set Default Dealer Name and
		 * Pirmary contact.
		 **/
		if (rs.next()) {
			snaponCall.setCall1Status(rs.getString("call1Status"));
			snaponCall.setCall1Contact(rs.getString("call1Contact"));
			snaponCall.setCall2Status(rs.getString("call2Status"));
			snaponCall.setCall2Contact(rs.getString("call2Contact"));
			snaponCall.setCall12Status(rs.getString("call12Status"));
			snaponCall.setCall12Contact(rs.getString("call12Contact"));
			snaponCall.setEscalate(rs.getString("escalate"));
			snaponCall.setReschedule(rs.getString("reschedule"));
			snaponCall.setOccurance(rs.getString("occurance"));
		} // - End of If
		else {
			snaponCall.setOccurance("1T"); // - when job is not exists into
											// table
		} // - End of else

		if (snaponCall.getCall1Contact() == null) {
			snaponCall.setCall1Contact(info[1] + " " + info[2]);
		} // - End of If
		if (snaponCall.getCall1Status() != null
				&& snaponCall.getCall1Status().equals("Failed")
				&& snaponCall.getCall12Status() == null) {
			snaponCall.setCall12Status("Confirmed");
		} // - End of If
		if (snaponCall.getCall12Contact() == null) {
			snaponCall.setCall12Contact(snaponCall.getCall1Contact());
		} // - End of If
		/* Fill Call 2 Contact when its call first/third time */
		if (snaponCall.getOccurance().equals("3T")
				&& snaponCall.getCall12Status().equals("Not Applicable")) {
			snaponCall.setCall2Contact(snaponCall.getCall1Contact());
		} // - End of else If
		else if (snaponCall.getCall2Contact() == null) {
			snaponCall.setCall2Contact(snaponCall.getCall12Contact());
		} // - End of If

		/* Set some fixed value for Display purpose only */
		snaponCall.setDealerName(info[0]);
		snaponCall.setPriContact(info[1] + ", " + info[2]);
	}

	/**
	 * Discription: This method is used to get snapon call status for fill
	 * combo-box, form database.
	 * 
	 * @param callType
	 *            -- Call Status Type
	 * @param ds
	 *            -- Database Object
	 * @return list -- List of snapon call status
	 */
	public static ArrayList getSnCallStatus(String callType, DataSource ds) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		try {
			conn = ds.getConnection();
			pstmt = conn
					.prepareStatement("select * from func_dw_get_call_status(?)");
			pstmt.setString(1, callType);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				list.add(new com.mind.common.LabelValue(rs
						.getString("sn_call_status"), rs
						.getString("sn_call_status")));
			} // - End of while

		} // - End of try
		catch (Exception e) {
			logger.error("getSnCallStatus(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSnCallStatus(String, DataSource) - Error occured com.mind.RPT.dao.getSnaponSiteNo() "
						+ e);
			}
		} // - End of catch
		finally {

			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		} // - End of finally
		return list;
	} // - End of getSnCallStatus() method

	/**
	 * Discription: This method to store snapon information into database.
	 * 
	 * @param date
	 *            - Date
	 * @param ds
	 *            - Datbase Object
	 * @return int - No of Updated/Inserted snapon call.
	 */
	public static int setSnaponCall(SnaponCallBean snaponCall, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		int val = -1;
		setRseEsca(snaponCall); // - Set Call status accouding to Reschedue and
								// Escalate.

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_sn_calls_manage_01(?,?,?,?,?,?,?,?,?,?,?)}"); // -
																							// 11
																							// parameters
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, snaponCall.getJobId()); // - Job Id
			cstmt.setString(3, snaponCall.getAppendixId()); // - Appendix Id
			cstmt.setString(4, snaponCall.getCall1Status()); // - Call 1 Status
			cstmt.setString(5, snaponCall.getCall1Contact()); // - Call 1
																// Contact
			cstmt.setString(6, snaponCall.getCall2Status()); // - Call 2 Status
			cstmt.setString(7, snaponCall.getCall2Contact()); // - Call 2
																// Contact
			cstmt.setString(8, snaponCall.getCall12Status()); // - Call 1_2
																// Status
			cstmt.setString(9, snaponCall.getCall12Contact()); // - Call 1_2
																// Contact
			cstmt.setString(10, snaponCall.getEscalate()); // - Escalate
			cstmt.setString(11, snaponCall.getReschedule()); // - Reschedule
			cstmt.setString(12, snaponCall.getFrom().trim()); // - Action
			cstmt.execute();
			val = cstmt.getInt(1);

		} // - End of try
		catch (Exception e) {
			logger.error("setSnaponCall(SnaponCallForm, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("setSnaponCall(SnaponCallForm, DataSource) - Exception in com.mind.RPT.dao.DailyBuzzReportdao.setSnaponCall() "
						+ e);
			}
		} // - End of catch
		finally {

			DBUtil.close(cstmt);
			DBUtil.close(conn);
		} // - End of finally
		return val;

	} // - End of getDailyCnfJobList() method

	/**
	 * Discription: This method Set Call status accouding to Reschedue and
	 * Escalate.
	 * 
	 * @param snaponCall
	 *            -- Form Bean Object
	 */
	private static void setRseEsca(SnaponCallBean snaponCall) {
		/**
		 * Fist check Reschedule/Escalate event. If status is changed to
		 * Reschedule or Escalate then check Occurance number. If it is first
		 * occurance then store null into Call-1 status. If it is second
		 * occurance then store null into Call-2 status. If it is third
		 * occurance then store null into Call-24 status.
		 * **/
		boolean setNull = false;

		if ((snaponCall.getReschedule() != null && snaponCall.getReschedule()
				.equals("Y"))
				|| (snaponCall.getEscalate() != null && snaponCall
						.getEscalate().equals("Y"))) {
			setNull = true;
		} // - End of If

		if (snaponCall.getOccurance().equals("1T") && setNull)
			snaponCall.setCall1Status(null);
		else if (snaponCall.getOccurance().equals("2T") && setNull)
			snaponCall.setCall12Status(null);
		else if (snaponCall.getOccurance().equals("3T") && setNull)
			snaponCall.setCall2Status(null);

	} // - End of setRseEsca() method

	/**
	 * Discription: This method get work location and primary contact first
	 * name, Last Name, Phone Number form database.
	 * 
	 * @param jobId
	 *            -- Job Id
	 * @param ds
	 *            -- Database Object
	 * @return info[] -- info of primary contact info of site.
	 */
	public static String[] getDealerContact(String jobId, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String info[] = new String[3];

		try {
			String sql = "select lm_si_work_location, lm_si_pri_poc, lm_si_phone_no "
					+ "from lm_job inner join lm_site_detail on lm_si_id = lm_js_si_id "
					+ "where lm_js_id = " + jobId;
			// System.out.println("sql= "+sql);
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				info[0] = rs.getString("lm_si_work_location"); // - Work
																// Location
				info[1] = rs.getString("lm_si_pri_poc"); // - Primary Poc Name
				info[2] = rs.getString("lm_si_phone_no"); // - Primary Phone No
			} // - End of If
		} // - End of try
		catch (Exception e) {
			logger.error("getDealerContact(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getDealerContact(String, DataSource) - Exception in com.mind.RPT.dao.getDealerContact() "
						+ e);
			}
		} // - End of catch
		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		} // - End of finally
		return info;

	} // - getDealerContact

	/**
	 * Discription: This method check Call1 Status on Snap-on Pre-call Tracking
	 * is confermed or not.
	 * 
	 * @param jobId
	 *            -- Job Id
	 * @param ds
	 *            -- Database Object
	 * @return info[] -- Boolean value if status is Confermed then True else
	 *         False.
	 */

	public static boolean isCall1Status(String jobId, DataSource ds) {
		boolean isCall1staus = false;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String str = "   select lm_sn_call_1_status=isnull(lm_sn_call_1_status,''),"
					+ "lm_sn_call_1_2_status=isnull(lm_sn_call_1_2_status,''),"
					+ "lm_sn_call_2_status=isnull(lm_sn_call_2_status,''),"
					+ "lm_sn_escalate=isnull(lm_sn_escalate,''),"
					+ "lm_sn_reschedule=isnull(lm_sn_reschedule,'') from lm_snapon_site_calls where lm_sn_js_id="
					+ jobId;
			rs = stmt.executeQuery(str);
			// System.out.println("String --"+str);
			if (rs.next()) {
				// System.out.println("'"+rs.getString("lm_sn_call_2_status")+"'");
				if ((rs.getString("lm_sn_call_1_status").equals("Confirmed") || rs
						.getString("lm_sn_call_1_2_status").equals("Confirmed"))
						&& (!rs.getString("lm_sn_reschedule").equals("Y") && !rs
								.getString("lm_sn_escalate").equals("Y"))
						&& (rs.getString("lm_sn_call_2_status").equals(""))) {
					isCall1staus = true;
				}
			}
		} catch (Exception e) {
			logger.error("isCall1Status(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("isCall1Status(String, DataSource) - Error ocured in method DailySnaponReportdao.isCall1Status()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return isCall1staus;
	}

} // - End of Class DailySnaponReportdao
