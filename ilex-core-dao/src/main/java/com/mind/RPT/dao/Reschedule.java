package com.mind.RPT.dao;


public class Reschedule {
	
	private String msaTitle = null;
	private String appendixTitle = null;
	private String monDate = null;
	private String executed = null;
	private String rescheduled = null;
	private String inwork = null;
	private String total = null;
	private String rescheduleRate = null;
	private String billableRschedules = null;
	
	
	public String getAppendixTitle() {
		return appendixTitle;
	}
	public void setAppendixTitle(String appendixTitle) {
		this.appendixTitle = appendixTitle;
	}
	public String getBillableRschedules() {
		return billableRschedules;
	}
	public void setBillableRschedules(String billableRschedules) {
		this.billableRschedules = billableRschedules;
	}
	public String getExecuted() {
		return executed;
	}
	public void setExecuted(String executed) {
		this.executed = executed;
	}
	public String getMonDate() {
		return monDate;
	}
	public void setMonDate(String monDate) {
		this.monDate = monDate;
	}
	public String getMsaTitle() {
		return msaTitle;
	}
	public void setMsaTitle(String msaTitle) {
		this.msaTitle = msaTitle;
	}
	public String getRescheduled() {
		return rescheduled;
	}
	public void setRescheduled(String rescheduled) {
		this.rescheduled = rescheduled;
	}
	public String getRescheduleRate() {
		return rescheduleRate;
	}
	public void setRescheduleRate(String rescheduleRate) {
		this.rescheduleRate = rescheduleRate;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getInwork() {
		return inwork;
	}
	public void setInwork(String inwork) {
		this.inwork = inwork;
	}
	

}
