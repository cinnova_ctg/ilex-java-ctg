package com.mind.RPT.dao;

import java.util.Collection;

public class DailySnapon {
	
	private String call_status = null;
	private String job_id = null;
	private String appendix_id = null;
	private String job_name = null;
	private String planned_start_date = null;
	private String planned_start_time = null;
	private String site_number = null;
	private String site_city = null;
	private String site_state = null;
	private String day5_1 = null;
	private String day5_2 = null;
	private String hour24 = null;
	private Collection day5CallList = null;
	private Collection hour24CallList = null;
	private Collection readyToGo = null;
	private Collection escalate = null;
	private Collection rescheduled = null;
	
	
	public Collection getRescheduled() {
		return rescheduled;
	}
	public void setRescheduled(Collection rescheduled) {
		this.rescheduled = rescheduled;
	}
	public String getCall_status() {
		return call_status;
	}
	public void setCall_status(String call_status) {
		this.call_status = call_status;
	}
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}
	public String getJob_name() {
		return job_name;
	}
	public void setJob_name(String job_name) {
		this.job_name = job_name;
	}
	public String getPlanned_start_date() {
		return planned_start_date;
	}
	public void setPlanned_start_date(String planned_start_date) {
		this.planned_start_date = planned_start_date;
	}
	public String getPlanned_start_time() {
		return planned_start_time;
	}
	public void setPlanned_start_time(String planned_start_time) {
		this.planned_start_time = planned_start_time;
	}
	public String getSite_city() {
		return site_city;
	}
	public void setSite_city(String site_city) {
		this.site_city = site_city;
	}
	public String getSite_number() {
		return site_number;
	}
	public void setSite_number(String site_number) {
		this.site_number = site_number;
	}
	public String getSite_state() {
		return site_state;
	}
	public void setSite_state(String site_state) {
		this.site_state = site_state;
	}
	public String getAppendix_id() {
		return appendix_id;
	}
	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}
	public Collection getReadyToGo() {
		return readyToGo;
	}
	public void setReadyToGo(Collection readyToGo) {
		this.readyToGo = readyToGo;
	}
	public Collection getDay5CallList() {
		return day5CallList;
	}
	public void setDay5CallList(Collection day5CallList) {
		this.day5CallList = day5CallList;
	}
	public Collection getHour24CallList() {
		return hour24CallList;
	}
	public void setHour24CallList(Collection hour24CallList) {
		this.hour24CallList = hour24CallList;
	}
	public Collection getEscalate() {
		return escalate;
	}
	public void setEscalate(Collection escalate) {
		this.escalate = escalate;
	}
	public String getDay5_1() {
		return day5_1;
	}
	public void setDay5_1(String day5_1) {
		this.day5_1 = day5_1;
	}
	public String getDay5_2() {
		return day5_2;
	}
	public void setDay5_2(String day5_2) {
		this.day5_2 = day5_2;
	}
	public String getHour24() {
		return hour24;
	}
	public void setHour24(String hour24) {
		this.hour24 = hour24;
	}
	
	

}
