package com.mind.RPT.dao;

public class JobOwnerSummary {

	private String job_name = null;
	private String planned_start_date = null;
	private String planned_start_time = null;
	private String site_number = null;
	private String site_city = null;
	private String site_state = null;
	private String site_time_zone = null;
	private String actual_start_date = null;
	private String actual_start_time = null;
	private String actual_end_date = null;
	private String actual_end_time = null;
	private String time_on = null;
	private String estimated_cost = null;
	private String actual_cost = null;
	private String invoiced_price = null;
	private String created = null;
	private String job_id = null;
	private String appendix_id = null;
	private String ticket_id = null;
	private String site_id = null;
	private String site_name = null;

	private String ms_site_number = null;
	private String ms_site_city = null;
	private String ms_site_state = null;
	private String ms_job_status = null;
	private String ms_scheduled_date = null;
	private String ms_onsite_date = null;
	private String ms_offsite_date = null;
	private String ms_job_complete_date = null;
	private String ms_job_invoiced_date = null;
	private String ms_job_invoiced_no = null;
	private String ms_appendix_id = null;
	private String msa_title = null;
	private String appendix_title = null;
	private String msa_appendix_title = null;
	private String poNo = null;
	private String delivery = null;
	private String project_type = null;
	private String requested_date;

	private String partner_name = null;

	public String getActual_cost() {
		return actual_cost;
	}

	public void setActual_cost(String actual_cost) {
		this.actual_cost = actual_cost;
	}

	public String getActual_end_date() {
		return actual_end_date;
	}

	public void setActual_end_date(String actual_end_date) {
		this.actual_end_date = actual_end_date;
	}

	public String getActual_end_time() {
		return actual_end_time;
	}

	public void setActual_end_time(String actual_end_time) {
		this.actual_end_time = actual_end_time;
	}

	public String getActual_start_date() {
		return actual_start_date;
	}

	public void setActual_start_date(String actual_start_date) {
		this.actual_start_date = actual_start_date;
	}

	public String getActual_start_time() {
		return actual_start_time;
	}

	public void setActual_start_time(String actual_start_time) {
		this.actual_start_time = actual_start_time;
	}

	public String getAppendix_id() {
		return appendix_id;
	}

	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getEstimated_cost() {
		return estimated_cost;
	}

	public void setEstimated_cost(String estimated_cost) {
		this.estimated_cost = estimated_cost;
	}

	public String getInvoiced_price() {
		return invoiced_price;
	}

	public void setInvoiced_price(String invoiced_price) {
		this.invoiced_price = invoiced_price;
	}

	public String getJob_id() {
		return job_id;
	}

	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}

	public String getJob_name() {
		return job_name;
	}

	public void setJob_name(String job_name) {
		this.job_name = job_name;
	}

	public String getPlanned_start_date() {
		return planned_start_date;
	}

	public void setPlanned_start_date(String planned_start_date) {
		this.planned_start_date = planned_start_date;
	}

	public String getSite_city() {
		return site_city;
	}

	public void setSite_city(String site_city) {
		this.site_city = site_city;
	}

	public String getSite_id() {
		return site_id;
	}

	public void setSite_id(String site_id) {
		this.site_id = site_id;
	}

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public String getSite_number() {
		return site_number;
	}

	public void setSite_number(String site_number) {
		this.site_number = site_number;
	}

	public String getSite_state() {
		return site_state;
	}

	public void setSite_state(String site_state) {
		this.site_state = site_state;
	}

	public String getSite_time_zone() {
		return site_time_zone;
	}

	public void setSite_time_zone(String site_time_zone) {
		this.site_time_zone = site_time_zone;
	}

	public String getTime_on() {
		return time_on;
	}

	public void setTime_on(String time_on) {
		this.time_on = time_on;
	}

	public String getMs_appendix_id() {
		return ms_appendix_id;
	}

	public void setMs_appendix_id(String ms_appendix_id) {
		this.ms_appendix_id = ms_appendix_id;
	}

	public String getMs_job_complete_date() {
		return ms_job_complete_date;
	}

	public void setMs_job_complete_date(String ms_job_complete_date) {
		this.ms_job_complete_date = ms_job_complete_date;
	}

	public String getMs_job_invoiced_date() {
		return ms_job_invoiced_date;
	}

	public void setMs_job_invoiced_date(String ms_job_invoiced_date) {
		this.ms_job_invoiced_date = ms_job_invoiced_date;
	}

	public String getMs_job_invoiced_no() {
		return ms_job_invoiced_no;
	}

	public void setMs_job_invoiced_no(String ms_job_invoiced_no) {
		this.ms_job_invoiced_no = ms_job_invoiced_no;
	}

	public String getMs_offsite_date() {
		return ms_offsite_date;
	}

	public void setMs_offsite_date(String ms_offsite_date) {
		this.ms_offsite_date = ms_offsite_date;
	}

	public String getMs_onsite_date() {
		return ms_onsite_date;
	}

	public void setMs_onsite_date(String ms_onsite_date) {
		this.ms_onsite_date = ms_onsite_date;
	}

	public String getMs_scheduled_date() {
		return ms_scheduled_date;
	}

	public void setMs_scheduled_date(String ms_scheduled_date) {
		this.ms_scheduled_date = ms_scheduled_date;
	}

	public String getMs_site_city() {
		return ms_site_city;
	}

	public void setMs_site_city(String ms_site_city) {
		this.ms_site_city = ms_site_city;
	}

	public String getMs_job_status() {
		return ms_job_status;
	}

	public void setMs_job_status(String ms_job_status) {
		this.ms_job_status = ms_job_status;
	}

	public String getMs_site_number() {
		return ms_site_number;
	}

	public void setMs_site_number(String ms_site_number) {
		this.ms_site_number = ms_site_number;
	}

	public String getMs_site_state() {
		return ms_site_state;
	}

	public void setMs_site_state(String ms_site_state) {
		this.ms_site_state = ms_site_state;
	}

	public String getPlanned_start_time() {
		return planned_start_time;
	}

	public void setPlanned_start_time(String planned_start_time) {
		this.planned_start_time = planned_start_time;
	}

	public String getAppendix_title() {
		return appendix_title;
	}

	public void setAppendix_title(String appendix_title) {
		this.appendix_title = appendix_title;
	}

	public String getMsa_title() {
		return msa_title;
	}

	public void setMsa_title(String msa_title) {
		this.msa_title = msa_title;
	}

	public String getMsa_appendix_title() {
		return msa_appendix_title;
	}

	public void setMsa_appendix_title(String msa_appendix_title) {
		this.msa_appendix_title = msa_appendix_title;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public String getDelivery() {
		return delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public String getTicket_id() {
		return ticket_id;
	}

	public void setTicket_id(String ticket_id) {
		this.ticket_id = ticket_id;
	}

	public String getProject_type() {
		return project_type;
	}

	public void setProject_type(String project_type) {
		this.project_type = project_type;
	}

	public String getRequested_date() {
		return requested_date;
	}

	public void setRequested_date(String requested_date) {
		this.requested_date = requested_date;
	}

}
