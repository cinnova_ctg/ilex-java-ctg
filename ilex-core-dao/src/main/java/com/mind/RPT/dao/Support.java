package com.mind.RPT.dao;

public class Support {
	
	private String jobName = null;
	private String siteAddress = null;
	private String siteCity = null;
	private String siteState = null;
	private String siteZip = null;
	private String qwestRequest = null;
	private String lastSiteVisit = null;
	private String install = null;
	private String tnm = null;
	private String total = null;
	
	private String siteNumber = null;
	private String ticketProDesc = null;
	private String onSiteDate = null;
	private String onSiteTime = null;
	private String offSiteTime = null;
	private String closingSummary = null;
	
	
	
	public String getInstall() {
		return install;
	}
	public void setInstall(String install) {
		this.install = install;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	
	public String getLastSiteVisit() {
		return lastSiteVisit;
	}
	public void setLastSiteVisit(String lastSiteVisit) {
		this.lastSiteVisit = lastSiteVisit;
	}
	public String getQwestRequest() {
		return qwestRequest;
	}
	public void setQwestRequest(String qwestRequest) {
		this.qwestRequest = qwestRequest;
	}
	public String getSiteAddress() {
		return siteAddress;
	}
	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}
	public String getSiteCity() {
		return siteCity;
	}
	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}
	public String getSiteState() {
		return siteState;
	}
	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}
	public String getSiteZip() {
		return siteZip;
	}
	public void setSiteZip(String siteZip) {
		this.siteZip = siteZip;
	}
	public String getTnm() {
		return tnm;
	}
	public void setTnm(String tnm) {
		this.tnm = tnm;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getClosingSummary() {
		return closingSummary;
	}
	public void setClosingSummary(String closingSummary) {
		this.closingSummary = closingSummary;
	}
	public String getOffSiteTime() {
		return offSiteTime;
	}
	public void setOffSiteTime(String offSiteTime) {
		this.offSiteTime = offSiteTime;
	}
	public String getOnSiteDate() {
		return onSiteDate;
	}
	public void setOnSiteDate(String onSiteDate) {
		this.onSiteDate = onSiteDate;
	}
	public String getOnSiteTime() {
		return onSiteTime;
	}
	public void setOnSiteTime(String onSiteTime) {
		this.onSiteTime = onSiteTime;
	}
	public String getSiteNumber() {
		return siteNumber;
	}
	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}
	public String getTicketProDesc() {
		return ticketProDesc;
	}
	public void setTicketProDesc(String ticketProDesc) {
		this.ticketProDesc = ticketProDesc;
	}
	
	

}
