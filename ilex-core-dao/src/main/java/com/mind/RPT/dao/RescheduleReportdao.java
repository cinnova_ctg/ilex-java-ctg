package com.mind.RPT.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.fw.core.dao.util.DBUtil;

public class RescheduleReportdao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(RescheduleReportdao.class);

	/**
	 * Discription: This Method is use to get list of Reschedule job and it's
	 * information.
	 * 
	 * @param appendixId
	 *            - Appendix Id
	 * @param ds
	 *            - Database Object
	 * @return ArrayList - List of Reschedule jobs
	 */
	public static ArrayList getRescheduleList(String appendixId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		DecimalFormat dfcur = new DecimalFormat("#,###,##0.00"); // - for
																	// decimal
																	// no.
																	// format
		DecimalFormat dfcur2 = new DecimalFormat("#,###,##0"); // - for decimal
																// no. format
		try {
			conn = ds.getConnection();
			cstmt = conn.prepareCall("{?=call dw_reschedule_report(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, appendixId);
			rs = cstmt.executeQuery();

			while (rs != null && rs.next()) {
				Reschedule reschedule = new Reschedule();
				reschedule.setMsaTitle(rs.getString("msa_title")); // - Msa Name
				reschedule.setAppendixTitle(rs.getString("appendix_title")); // -
																				// Appendix
																				// Name
				reschedule.setMonDate(rs.getString("mon_date")); // - Data of
																	// Monday of
																	// each week
				reschedule.setExecuted(rs.getString("executed")); // - No. of
																	// Executed
																	// Jobs
				reschedule.setRescheduled(rs.getString("rescheduled")); // - No.
																		// of
																		// Reschedule
																		// Jobs
				reschedule.setInwork(rs.getString("inwork")); // - No. of Inwork
																// Jobs
				reschedule.setTotal(rs.getString("total")); // - Total
				reschedule.setRescheduleRate(dfcur.format(Float.parseFloat(rs
						.getString("reschedule_rate")))); // - Reschedule Rate
				reschedule.setBillableRschedules(dfcur2.format(Float
						.parseFloat(rs.getString("billable_rschedules")))); // -
																			// Billable
																			// Reschedule
				list.add(reschedule);
			}

		} catch (Exception e) {
			logger.error("getRescheduleList(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getRescheduleList(String, DataSource) - Exception in com.mind.RPT.RescheduleReportdao.getRescheduleList() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return list;
	}
}
