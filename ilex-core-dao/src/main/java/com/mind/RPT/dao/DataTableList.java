package com.mind.RPT.dao;

public class DataTableList {		
	private String field = null; 
	private String preVal = null; 
	private String postVal = null; 	
	private String sauditID = null; 
	private String selectTable = null;
	private String soperation = null;
	private String user = null;
	private String dateTime = null;
	private String tablename = null;
	private String fieldName = null;
	private String operation = null;
	private String auditTxnID = null;
	private String updatedBy = null;
	private String soper = null;
	private String sdateTime = null;
	
	public String getTablename() {
		return tablename;
	}
	public void setTablename(String tablename) {
		this.tablename = tablename;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getSauditID() {
		return sauditID;
	}
	public void setSauditID(String sauditID) {
		this.sauditID = sauditID;
	}
	public String getSelectTable() {
		return selectTable;
	}
	public void setSelectTable(String selectTable) {
		this.selectTable = selectTable;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getSoperation() {
		return soperation;
	}
	public void setSoperation(String soperation) {
		this.soperation = soperation;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getPostVal() {
		return postVal;
	}
	public void setPostVal(String postVal) {
		this.postVal = postVal;
	}
	public String getPreVal() {
		return preVal;
	}
	public void setPreVal(String preVal) {
		this.preVal = preVal;
	}
	public String getAuditTxnID() {
		return auditTxnID;
	}
	public void setAuditTxnID(String auditTxnID) {
		this.auditTxnID = auditTxnID;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getSdateTime() {
		return sdateTime;
	}
	public void setSdateTime(String sdateTime) {
		this.sdateTime = sdateTime;
	}
	public String getSoper() {
		return soper;
	}
	public void setSoper(String soper) {
		this.soper = soper;
	}
}
