package com.mind.RPT.dao;

public class DashboardBean {
	
	private String colName = null;
	private String jobStatus = null;
	private String count = null;
	private String revenue = null;
	private String aveVGPM = null;
	
	
	public String getAveVGPM() {
		return aveVGPM;
	}
	public void setAveVGPM(String aveVGPM) {
		this.aveVGPM = aveVGPM;
	}
	public String getColName() {
		return colName;
	}
	public void setColName(String colName) {
		this.colName = colName;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
	public String getRevenue() {
		return revenue;
	}
	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}
	
	
	

}
