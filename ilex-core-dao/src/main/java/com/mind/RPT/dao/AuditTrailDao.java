package com.mind.RPT.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.fw.core.dao.util.DBUtil;

public class AuditTrailDao {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AuditTrailDao.class);

	public static ArrayList getTableData(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList data = new ArrayList();
		DataTableList Dlist = new DataTableList();
		Dlist.setTablename("---Select---");
		data.add(Dlist);
		try {
			String sql = "select tableName from audit_master";
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Dlist = new DataTableList();
				Dlist.setTablename(rs.getString("tablename"));
				data.add(Dlist);
			}
		} catch (Exception e) {
			logger.error("getTableData(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTableData(DataSource) - Exception in com.mind.RPT.dao.getTableData() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
		return data;
	}

	public static ArrayList getTableDataField(String tn, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList data1 = new ArrayList();
		DataTableList Dlist1 = new DataTableList();
		Dlist1.setFieldName("---All---");
		data1.add(Dlist1);
		try {

			String sql = "select distinct atd.fieldName from audit_header ah, audit_txn at, audit_txndetail atd where ah.auditID=at.auditID and at.auditTxnID=atd.auditTxnID and ah.tableName='"
					+ tn + "'";
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Dlist1 = new DataTableList();
				Dlist1.setFieldName(rs.getString("fieldName"));
				data1.add(Dlist1);
			}
		} catch (Exception e) {
			logger.error("getTableDataField(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTableDataField(String, DataSource) - Exception in com.mind.RPT.dao.getTableDataField() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return data1;

	}

	public static ArrayList getTableDataOperation(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList dataOperation = new ArrayList();
		DataTableList operation = new DataTableList();
		operation.setOperation("---All---");
		dataOperation.add(operation);
		try {
			String sql = "select distinct at.operation from audit_header ah, audit_txn at, audit_txndetail atd where ah.auditID=at.auditID and at.auditTxnID=atd.auditTxnID";
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				operation = new DataTableList();
				operation.setOperation(rs.getString("operation"));
				dataOperation.add(operation);
			}
		} catch (Exception e) {
			logger.error("getTableDataOperation(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getTableDataOperation(DataSource) - Exception in com.mind.RPT.dao.getTableDataOperation() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return dataOperation;

	}

	public static ArrayList getUserNames(DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList userNames = new ArrayList();
		DataTableList users = new DataTableList();
		users.setUpdatedBy("---All---");
		userNames.add(users);
		try {
			String sql = "select distinct at.updatedBy from audit_header ah, audit_txn at, audit_txndetail atd where ah.auditID=at.auditID and at.auditTxnID=atd.auditTxnID";
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				users = new DataTableList();
				users.setUpdatedBy(rs.getString("updatedBy"));
				userNames.add(users);
			}
		} catch (Exception e) {
			logger.error("getUserNames(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getUserNames(DataSource) - Exception in com.mind.RPT.dao.getUserNames() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return userNames;

	}

	public static ArrayList getData(String tn, String oper, String fn,
			String users, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		ArrayList searchData = new ArrayList();
		DataTableList SData;
		if (oper.equals("---All---")) {
			oper = "";
		}
		if (fn.equals("---All---")) {
			fn = "";
		}
		if (users.equals("---All---")) {
			users = "";
		}
		try {
			sql = "select distinct atd.auditTxnID,ah.tableName,at.operation,at.updatedBy ,at.updateDate from audit_header ah, audit_txn at, audit_txndetail atd where ah.auditID=at.auditID and at.auditTxnID=atd.auditTxnID and ah.tableName='"
					+ tn
					+ "' and atd.fieldName LIKE N'"
					+ fn
					+ "%'  AND at.operation LIKE N'"
					+ oper
					+ "%' AND at.updatedBy LIKE N'"
					+ users
					+ "%' order by at.updateDate DESC ";
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				SData = new DataTableList();
				SData.setSauditID(rs.getString("auditTxnID"));
				SData.setSelectTable(rs.getString("tableName"));
				SData.setSoperation(rs.getString("operation"));
				SData.setUser(rs.getString("updatedBy"));
				SData.setDateTime(rs.getString("updateDate"));
				searchData.add(SData);
			}
		} catch (Exception e) {
			logger.error("getData(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getData(String, String, String, String, DataSource) - Exception in com.mind.RPT.dao.getData() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return searchData;
	}

	public static ArrayList getFieldData(String tn, String oper, String fn,
			String users, String auditIDS, DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "";
		ArrayList fieldData = new ArrayList();
		DataTableList FData;
		if (oper.equals("---All---")) {
			oper = "";
		}
		if (fn.equals("---All---")) {
			fn = "";
		}
		if (users.equals("---All---")) {
			users = "";
		}
		try {
			sql = "select distinct atd.auditTxnID,atd.fieldName,atd.preVal,atd.postVal,at.operation,at.updateDate from audit_header ah, audit_txn at, audit_txndetail atd where ah.auditID=at.auditID and at.auditTxnID=atd.auditTxnID and ah.tableName='"
					+ tn
					+ "' and at.operation LIKE N'"
					+ oper
					+ "%' AND atd.fieldName LIKE N'"
					+ fn
					+ "%' AND at.updatedBy LIKE N'"
					+ users
					+ "%' order by at.updateDate DESC";
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				FData = new DataTableList();
				FData.setAuditTxnID(rs.getString("auditTxnID"));
				FData.setField(rs.getString("fieldName"));
				FData.setPreVal(rs.getString("preVal"));
				FData.setPostVal(rs.getString("postVal"));
				FData.setSoper(rs.getString("operation"));
				FData.setSdateTime(rs.getString("updateDate"));
				fieldData.add(FData);
			}
		} catch (Exception e) {
			logger.error(
					"getFieldData(String, String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("getFieldData(String, String, String, String, String, DataSource) - Exception in com.mind.RPT.dao.getFieldData() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return fieldData;

	}
}
