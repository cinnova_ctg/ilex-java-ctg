package com.mind.RPT.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.fw.core.dao.util.DBUtil;

// TODO: Auto-generated Javadoc
/**
 * Discription: This class is use to perform database operation for Daily Buzz
 * Report. Methods: - getSchJobList, getCnfJobList, getOnsiteJobList,
 * getOffsiteJobList
 * 
 * @author Vijay Kumar Singh
 */
public class DailyBuzzReportdao {

	/** Logger for this class. */
	private static final Logger logger = Logger
			.getLogger(DailyBuzzReportdao.class);

	/**
	 * Gets the sch job list.
	 * 
	 * @param ownerId
	 *            the owner id
	 * @param appendixid
	 *            the appendixid
	 * @param date
	 *            the date
	 * @param srPrjManagerId
	 *            the sr prj manager id
	 * @param ds
	 *            the ds
	 * @return the sch job list
	 */
	public static ArrayList getSchJobList(String ownerId, String appendixid,
			String date, String srPrjManagerId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();

		if (ownerId == null || ownerId.equals("")) {
			ownerId = "0";
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_daily_sch_jobs_view_01(?,?,?,?)}"); // -
																					// 4
																					// parameters
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(ownerId));
			cstmt.setString(3, appendixid);
			cstmt.setString(4, date);
			cstmt.setString(5, srPrjManagerId);
			rs = cstmt.executeQuery();

			while (rs != null && rs.next()) {
				DailyBuzzReport dailyBuzzReport = new DailyBuzzReport();
				dailyBuzzReport.setAppendix_id(rs.getString("appendix_id")); // -
																				// Appendix
																				// Id
				dailyBuzzReport.setJob_id(rs.getString("job_id")); // - Job Id
				dailyBuzzReport.setJob_name(rs.getString("job_name")); // - Job
																		// Name
				dailyBuzzReport.setPlanned_start_date(rs
						.getString("planned_start_date")); // - Planned Start
															// Date
				dailyBuzzReport.setPlanned_start_time(rs
						.getString("planned_start_time")); // - Planned Start
															// Time
				dailyBuzzReport.setSite_city(rs.getString("site_city")); // -
																			// Job
																			// Site
																			// city
																			// name
				dailyBuzzReport.setSite_number(rs.getString("site_number")); // -
																				// Job
																				// Site
																				// Number
				dailyBuzzReport.setSite_state(rs.getString("site_state")); // -
																			// Site
																			// State
				dailyBuzzReport.setMsa_title(rs.getString("msa_title"));
				dailyBuzzReport.setAppendix_title(rs
						.getString("appendix_title"));
				list.add(dailyBuzzReport);
			}

		} catch (Exception e) {
			logger.error("getSchJobList(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSchJobList(String, String, String, DataSource) - Exception in com.mind.RPT.dao.DailyBuzzReportdao.getSchJobList() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return list;

	}

	/**
	 * Gets the cnf job list.
	 * 
	 * @param ownerId
	 *            the owner id
	 * @param appendixid
	 *            the appendixid
	 * @param date
	 *            the date
	 * @param srPrjManagerId
	 *            the sr prj manager id
	 * @param ds
	 *            the ds
	 * @return the cnf job list
	 */
	public static ArrayList getCnfJobList(String ownerId, String appendixid,
			String date, String srPrjManagerId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();

		if (ownerId == null || ownerId.equals("")) {
			ownerId = "0";
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_daily_cnf_jobs_view_01(?,?,?,?)}"); // -
																					// 4
																					// parameters
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(ownerId));
			cstmt.setString(3, appendixid);
			cstmt.setString(4, date);
			cstmt.setString(5, srPrjManagerId);
			rs = cstmt.executeQuery();

			while (rs != null && rs.next()) {
				DailyBuzzReport dailyBuzzReport = new DailyBuzzReport();
				dailyBuzzReport.setAppendix_id(rs.getString("appendix_id")); // -
																				// Appendix
																				// Id
				dailyBuzzReport.setJob_id(rs.getString("job_id")); // - Job Id
				dailyBuzzReport.setJob_name(rs.getString("job_name")); // - Job
																		// Name
				dailyBuzzReport.setPlanned_start_date(rs
						.getString("planned_start_date")); // - Planned Start
															// Date
				dailyBuzzReport.setPlanned_start_time(rs
						.getString("planned_start_time")); // - Planned Start
															// Time
				dailyBuzzReport.setSite_city(rs.getString("site_city")); // -
																			// Job
																			// Site
																			// city
																			// name
				dailyBuzzReport.setSite_number(rs.getString("site_number")); // -
																				// Job
																				// Site
																				// Number
				dailyBuzzReport.setSite_state(rs.getString("site_state")); // -
																			// Site
																			// State
				dailyBuzzReport.setMsa_title(rs.getString("msa_title"));
				dailyBuzzReport.setAppendix_title(rs
						.getString("appendix_title"));
				list.add(dailyBuzzReport);
			}

		} catch (Exception e) {
			logger.error("getCnfJobList(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getCnfJobList(String, String, String, DataSource) - Exception in com.mind.RPT.dao.DailyBuzzReportdao.getCnfJobList() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return list;

	}

	/**
	 * Gets the onsite job list.
	 * 
	 * @param ownerId
	 *            the owner id
	 * @param appendixid
	 *            the appendixid
	 * @param date
	 *            the date
	 * @param srPrjManagerId
	 *            the sr prj manager id
	 * @param ds
	 *            the ds
	 * @return the onsite job list
	 */
	public static ArrayList getOnsiteJobList(String ownerId, String appendixid,
			String date, String srPrjManagerId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();

		if (ownerId == null || ownerId.equals("")) {
			ownerId = "0";
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_daily_onsite_jobs_view_01(?,?,?,?)}"); // -
																					// 4
																					// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(ownerId));
			cstmt.setString(3, appendixid);
			cstmt.setString(4, date);
			cstmt.setString(5, srPrjManagerId);
			rs = cstmt.executeQuery();

			while (rs != null && rs.next()) {
				DailyBuzzReport dailyBuzzReport = new DailyBuzzReport();
				dailyBuzzReport.setAppendix_id(rs.getString("appendix_id")); // -
																				// Appendix
																				// Id
				dailyBuzzReport.setJob_id(rs.getString("job_id")); // - Job Id
				dailyBuzzReport.setJob_name(rs.getString("job_name")); // - Job
																		// Name
				dailyBuzzReport.setActual_start_date(rs
						.getString("actual_start_date")); // - Actual Start Date
				dailyBuzzReport.setActual_start_time(rs
						.getString("actual_start_time")); // - Actual Start Time
				dailyBuzzReport.setSite_city(rs.getString("site_city")); // -
																			// Job
																			// Site
																			// city
																			// name
				dailyBuzzReport.setSite_number(rs.getString("site_number")); // -
																				// Job
																				// Site
																				// Number
				dailyBuzzReport.setSite_state(rs.getString("site_state")); // -
																			// Site
																			// State
				dailyBuzzReport.setMsa_title(rs.getString("msa_title"));
				dailyBuzzReport.setAppendix_title(rs
						.getString("appendix_title"));
				list.add(dailyBuzzReport);
			}

		} catch (Exception e) {
			logger.error(
					"getOnsiteJobList(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOnsiteJobList(String, String, String, DataSource) - Exception in com.mind.RPT.dao.DailyBuzzReportdao.getOnsiteJobList() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return list;

	}

	/**
	 * Discription: This method get Onsite Job list form database on the basis
	 * of Date/Appendix/Owner.
	 * 
	 * @param ownerId
	 *            - Owner Id
	 * @param appendixid
	 *            - Appendix Id
	 * @param date
	 *            - Date
	 * @param ds
	 *            - Datbase Object
	 * @return ArrayList - list job summay data according to tab.
	 */
	public static ArrayList getOffsiteJobList(String ownerId,
			String appendixid, String date, String srPrjManagerId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();

		if (ownerId == null || ownerId.equals("")) {
			ownerId = "0";
		}

		try {
			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call dw_daily_offsite_jobs_view_01(?,?,?,?)}"); // -
																						// 4
																						// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(ownerId));
			cstmt.setString(3, appendixid);
			cstmt.setString(4, date);
			cstmt.setString(5, srPrjManagerId);
			rs = cstmt.executeQuery();

			while (rs != null && rs.next()) {
				DailyBuzzReport dailyBuzzReport = new DailyBuzzReport();
				dailyBuzzReport.setAppendix_id(rs.getString("appendix_id")); // -
																				// Appendix
																				// Id
				dailyBuzzReport.setJob_id(rs.getString("job_id")); // - Job Id
				dailyBuzzReport.setJob_name(rs.getString("job_name")); // - Job
																		// Name
				dailyBuzzReport.setActual_end_date(rs
						.getString("actual_end_date")); // - Actual End Date
				dailyBuzzReport.setActual_end_time(rs
						.getString("actual_end_time")); // - Actual End Time
				dailyBuzzReport.setSite_city(rs.getString("site_city")); // -
																			// Job
																			// Site
																			// city
																			// name
				dailyBuzzReport.setSite_number(rs.getString("site_number")); // -
																				// Job
																				// Site
																				// Number
				dailyBuzzReport.setSite_state(rs.getString("site_state")); // -
																			// Site
																			// State
				dailyBuzzReport.setMsa_title(rs.getString("msa_title"));
				dailyBuzzReport.setAppendix_title(rs
						.getString("appendix_title"));
				list.add(dailyBuzzReport);
			}

		} catch (Exception e) {
			logger.error(
					"getOffsiteJobList(String, String, String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getOffsiteJobList(String, String, String, DataSource) - Exception in com.mind.RPT.dao.DailyBuzzReportdao.getOffsiteJobList() "
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return list;

	}

}
