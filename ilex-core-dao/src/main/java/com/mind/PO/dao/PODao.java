package com.mind.PO.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.MPO.dao.MPODao;
import com.mind.bean.mpo.MPODTO;
import com.mind.bean.mpo.PODTO;
import com.mind.bean.newjobdb.EInvoiceItems;
import com.mind.bean.newjobdb.EInvoiceSummary;
import com.mind.bean.newjobdb.GeneralPOSetup;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.PODeliverable;
import com.mind.bean.newjobdb.POResource;
import com.mind.bean.newjobdb.PartnerInfo;
import com.mind.bean.po.FreqUsedDeliverableDTO;
import com.mind.common.IlexConstants;
import com.mind.common.LabelValue;
import com.mind.common.PODeliverableStatusTypes;
import com.mind.common.POStatusTypes;
import com.mind.common.Util;
import com.mind.dao.PRM.POWODetail;
import com.mind.dao.PRM.POWODetaildao;
import com.mind.docm.client.ClientOperator;
import com.mind.docm.dao.ViewManupulationDao;
import com.mind.docm.utils.DocumentUtility;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.reports.reportDao.ReportsDao;
import com.mind.newjobdb.dao.POGeneralInfo;
import com.mind.newjobdb.dao.POResourceDAO;
import com.mind.newjobdb.dao.PoWoInfo;
import com.mind.newjobdb.dao.PurchaseOrder;
import com.mind.newjobdb.dao.PurchaseOrderDAO;

/**
 * The Class PODao.
 */
public class PODao {

	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(PODao.class);

	/**
	 * Gets the activity list.
	 * 
	 * @param bean
	 *            the bean
	 * @param ds
	 *            the ds
	 * @return the activity list
	 */
	public static ArrayList<POResource> getActivityList(PODTO bean,
			DataSource ds) {
		POResourceDAO poResourceDAO = new POResourceDAO();
		ArrayList<POResource> listOfPOResource = poResourceDAO
				.getAllPossibleResources(bean, ds);
		return listOfPOResource;

	}

	/**
	 * Gets the pOWO info.
	 * 
	 * @param bean
	 *            the bean
	 * @param ds
	 *            the ds
	 * @return the pOWO info
	 */
	public PoWoInfo getPOWOInfo(PODTO bean, DataSource ds) {

		bean.setSpecial_condition(PurchaseOrderDAO.getPoSpecialConditions(bean));
		bean.setWoSI(PurchaseOrderDAO.getWoSpecialInstructions(bean));
		bean.setPartner(POWODetaildao.getPartnername(ds, bean.getPoId()));

		return null;

	}

	/**
	 * Gets the purchase order.
	 * 
	 * @param poId
	 *            the po id
	 * @param ds
	 *            the ds
	 * @param appendixType
	 *            the appendix type
	 * @return the purchase order
	 */
	public static PurchaseOrder getPurchaseOrder(String poId, DataSource ds,
			String appendixType) {
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		purchaseOrder.setPoId(poId);

		PoWoInfo poWoInfo = PODao.getPWOdetaildata(poId, appendixType, ds);
		// added for core competency start
		String[] coreCompetancy = PODao
				.getSelectedCoreCompetencyForPO(poId, ds);
		poWoInfo.getPoGeneralInfo().setCmboxCoreCompetency(coreCompetancy);
		// added for core competency end
		purchaseOrder.setPowoInfo(poWoInfo);
		PartnerInfo partnerInfo = new PartnerInfo();

		if (!Util.isNullOrBlank(poWoInfo.getPoGeneralInfo().getPartnerId())) {
			partnerInfo = PODao.getPartnerInfo(poWoInfo.getPoGeneralInfo()
					.getPartnerId(), ds);
			purchaseOrder.setPartnerInfo(partnerInfo);
		}
		return purchaseOrder;
	}

	/**
	 * Gets the data for work order.
	 * 
	 * @param bean
	 *            the bean
	 * @param tabId
	 *            the tab id
	 * @return the data for work order
	 */
	public void getDataForWorkOrder(PODTO bean, String tabId) {
		logger.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getColumns)");
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String woRE[] = null;

		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			getREList(bean);
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery(" SELECT B.LX_SW_SOW AS LX_SW_SOW,A.LM_PO_SPECIAL_INSTRUCTIONS AS LM_PO_SPECIAL_INSTRUCTIONS  FROM "
							+ " LM_PURCHASE_ORDER A INNER JOIN LX_SOW B ON A.LM_PO_ID=B.LX_SW_USED_IN AND "
							+ " A.LM_PO_ID=" + bean.getPoId());
			if (logger.isDebugEnabled()) {
				logger.debug("getDataForWorkOrder(HttpServletRequest, PODTO, String) - rs---"
						+ rs);
			}

			if (rs.next()) {
				if (logger.isDebugEnabled()) {
					logger.debug("getDataForWorkOrder(HttpServletRequest, PODTO, String) - in not null");
				}
				bean.setWoNOA(rs.getString("LX_SW_SOW"));
				bean.setWoSI(rs.getString("LM_PO_SPECIAL_INSTRUCTIONS"));
			} else {
				bean.setWoNOA("");
				bean.setWoSI("");
			}

			String sql = "select podb_tools_id from dbo.PODB_WORK_ORDER_TOOLS where lm_po_id="
					+ bean.getPoId();
			woRE = ViewManupulationDao.getDataForOtherFields(conn, sql);

			bean.setWoRE(woRE);
		} catch (SQLException e) {
			logger.error(
					"getDataForWorkOrder(HttpServletRequest, PODTO, String)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * Gets the rE list.
	 * 
	 * @param bean
	 *            the bean
	 * @return the rE list
	 */
	public static void getREList(MPODTO bean) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();
			String sql = "select * from dbo.cp_tools order by cp_tool_name";
			rs = stmt.executeQuery(sql);
			// request.setAttribute("woREList",resultSetAsList(rs));
			// System.out.println("wolist1111"+request.getAttribute("woREList"));
			bean.setWoToolsList(MPODao.resultSetAsList(rs));
			if (logger.isDebugEnabled()) {
				logger.debug("getREList(HttpServletRequest, MPODTO) - wolist"
						+ bean.getWoToolsList());
			}
		} catch (SQLException e) {
			logger.error("getREList(HttpServletRequest, MPODTO)", e);

			logger.error("getREList(HttpServletRequest, MPODTO)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * Featch doc list for po.
	 * 
	 * @param mpoId
	 *            the mpo id
	 * @return the array list
	 */
	public ArrayList featchDocListForPO(int mpoId) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		PreparedStatement pStmt = null;
		Connection conn = ReportsDao.getStaticConnection();
		ArrayList outerList = new ArrayList();
		ResultSet rs = null;
		String query = "select podb_doc_id,doc_with_po,doc_with_wo "
				+ "from podb_document where podb_doc_id =?";
		try {
			pStmt = conn.prepareStatement(query);
			pStmt.setInt(1, mpoId);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				ArrayList innerList = ClientOperator.getDocListForMpo(rs
						.getString(1));
				innerList.add(rs.getString(2));
				innerList.add(rs.getString(3));
				innerList.add(rs.getString(1));
				outerList.add(innerList);
			}
		} catch (Exception e) {
			logger.error("featchDocListForPO(int)", e);

			logger.error("featchDocListForPO(int)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return outerList;
	}

	/**
	 * P o detail save.
	 * 
	 * @param sessionMap
	 *            the session map
	 * @param map
	 *            the map
	 * @param bean
	 *            the bean
	 * @param tableName
	 *            the table name
	 * @param tabId
	 *            the tab id
	 */
	public void pODetailSave(Map<String, Object> sessionMap,
			Map<String, Object> map, PODTO bean, String tableName, String tabId) {
		if (tabId != null && !tabId.trim().equalsIgnoreCase("")
				&& tabId.trim().equalsIgnoreCase("2")) {
			insertDocForMPO(sessionMap, bean);
		}
		fetchDataForMPO(map, bean, tabId);
	}

	/**
	 * Insert doc for mpo.
	 * 
	 * @param sessionMap
	 *            the session map
	 * @param bean
	 *            the bean
	 */
	public void insertDocForMPO(Map<String, Object> sessionMap, PODTO bean) {
		String sql = null;
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		String loginUserName = (String) sessionMap.get("username");
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		PreparedStatement pStmt = null;
		Connection conn = ReportsDao.getStaticConnection();
		String[] docIdList = bean.getListOfDoc_Id();
		String[] include_po = bean.getInclude_with_po();
		String[] document_id = bean.getDocument_id();
		String[] include_wo = bean.getInclude_with_wo();
		if (logger.isDebugEnabled()) {
			logger.debug("insertDocForMPO(HttpServletRequest, PODTO) - length"
					+ docIdList.length);
		}
		try {
			for (int i = 0; i < docIdList.length; i++) {
				if (logger.isDebugEnabled()) {
					logger.debug("insertDocForMPO(HttpServletRequest, PODTO) - inside insert"
							+ document_id[i]
							+ ":"
							+ include_po[i]
							+ ":"
							+ include_wo[i] + "::" + docIdList[i]);
				}
				String doc_id = getDocIdFromMPO(bean.getPoId(), document_id[i]);
				if (doc_id.equals("")) {
					sql = "insert into podb_document (lm_po_id,podb_doc_id,doc_with_po,doc_with_wo,podb_doc_created_by,"
							+ "podb_doc_created_date,podb_doc_updated_by,podb_doc_updated_date)"
							+ " \n values (?,?,?,?,?,?,?,?)";
					try {
						if (docIdList[i].equalsIgnoreCase("Y")) {
							pStmt = conn.prepareStatement(sql);
							pStmt.setInt(1,
									new Integer(bean.getPoId()).intValue());
							pStmt.setInt(2, Integer.parseInt(document_id[i]));
							pStmt.setString(3, include_po[i]);
							pStmt.setString(4, include_wo[i]);
							pStmt.setString(5, loginUserName);
							pStmt.setTimestamp(6, ts);
							pStmt.setString(7, loginUserName);
							pStmt.setTimestamp(8, ts);
							pStmt.executeUpdate();
						}
					} catch (Exception e) {
						logger.error(
								"insertDocForMPO(HttpServletRequest, PODTO)", e);

						logger.error(
								"insertDocForMPO(HttpServletRequest, PODTO)", e);
					}
				} else if (!doc_id.equals("")) {
					sql = "update podb_document" + " \n set podb_doc_id = ? ,"
							+ " \n doc_with_po = ?," + " \n doc_with_wo = ?,"
							+ " \n podb_doc_updated_by = ?,"
							+ " \n podb_doc_updated_date = ?"
							+ " \n where lm_po_id = ? and podb_doc_id = ?";
					if (docIdList[i].equalsIgnoreCase("Y")) {
						try {
							pStmt = conn.prepareStatement(sql);
							pStmt.setInt(1, Integer.parseInt(document_id[i]));
							pStmt.setString(2, include_po[i]);
							pStmt.setString(3, include_wo[i]);
							pStmt.setString(4, loginUserName);
							pStmt.setTimestamp(5, ts);
							pStmt.setInt(6,
									new Integer(bean.getPoId()).intValue());
							pStmt.setInt(7, Integer.parseInt(document_id[i]));
							pStmt.executeUpdate();
						} catch (NumberFormatException e) {
							logger.error(
									"insertDocForMPO(HttpServletRequest, PODTO)",
									e);

							logger.error(
									"insertDocForMPO(HttpServletRequest, PODTO)",
									e);
						} catch (SQLException e) {
							logger.error(
									"insertDocForMPO(HttpServletRequest, PODTO)",
									e);

							logger.error(
									"insertDocForMPO(HttpServletRequest, PODTO)",
									e);
						}
					}

				}
				if (logger.isDebugEnabled()) {
					logger.debug("insertDocForMPO(HttpServletRequest, PODTO) - "
							+ sql);
				}
			}
		} catch (Exception e) {
			logger.error("insertDocForMPO(HttpServletRequest, PODTO)", e);

			logger.error("insertDocForMPO(HttpServletRequest, PODTO)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * Fetch data for mpo.
	 * 
	 * @param map
	 *            the map
	 * @param bean
	 *            the bean
	 * @param tabId
	 *            the tab id
	 */
	public void fetchDataForMPO(Map<String, Object> map, PODTO bean,
			String tabId) {
		if (tabId != null && !tabId.trim().equalsIgnoreCase("")
				&& tabId.trim().equals("2")) {
			List<List<String>> al = featchDocListForMPO(Integer.parseInt(bean
					.getPoId()));
			if (logger.isDebugEnabled()) {
				logger.debug("fetchDataForMPO(HttpServletRequest, PODTO, String) - al"
						+ al);
			}
			map.put("docDetailPageList", al);
		}
	}

	/**
	 * Gets the doc id from mpo.
	 * 
	 * @param poId
	 *            the po id
	 * @param docId
	 *            the doc id
	 * @return the doc id from mpo
	 */
	public static String getDocIdFromMPO(String poId, String docId) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		PreparedStatement pStmt = null;
		String doc_id = "";
		ResultSet result = null;
		Connection conn = ReportsDao.getStaticConnection();
		String query = "select isnull(podb_doc_id,'') as podb_doc_id from dbo.podb_document where lm_po_id=? and podb_doc_id=?";
		try {
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, poId);
			pStmt.setString(2, docId);
			result = pStmt.executeQuery();
			while (result.next()) {
				doc_id = (result.getString("podb_doc_id"));
			}
		} catch (Exception e) {
			logger.error("getDocIdFromMPO(String, String)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		return doc_id;
	}

	/**
	 * Featch doc list for mpo.
	 * 
	 * @param mpoId
	 *            the mpo id
	 * @return the array list
	 */
	public static List<List<String>> featchDocListForMPO(int mpoId) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		PreparedStatement pStmt = null;
		Connection conn = ReportsDao.getStaticConnection();
		List<List<String>> outerList = new ArrayList<List<String>>();
		ResultSet rs = null;
		String query = "SELECT podb_doc_id,doc_with_po,doc_with_wo, isnull(dm_doc_file_id, 0) dm_doc_file_id, Title, DocumentDate, Status "
				+ " from dbo.podb_document  "
				+ " LEFT JOIN DocMSysDb.dbo.dm_document_version ON dm_doc_id = podb_doc_id "
				+ " where lm_po_id=?";

		try {

			List<String> innerList = null;
			pStmt = conn.prepareStatement(query);
			pStmt.setInt(1, mpoId);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				innerList = new ArrayList<String>();
				innerList.add(rs.getString("Title"));
				innerList.add(rs.getString("Status"));
				innerList.add(rs.getString("DocumentDate"));
				innerList.add(rs.getString("doc_with_po"));
				innerList.add(rs.getString("doc_with_wo"));

				innerList.add(rs.getString("podb_doc_id"));
				innerList.add(rs.getString("dm_doc_file_id"));

				outerList.add(innerList);
			}
		} catch (Exception e) {
			logger.error("featchDocListForMPO(int)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return outerList;
	}

	/**
	 * Update doc form mpo.
	 * 
	 * @param bean
	 *            the bean
	 * @param sessionMap
	 *            the session map
	 * @return the int
	 */
	public int updateDocFormMPO(PODTO bean, Map<String, Object> sessionMap) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		String loginUserName = (String) sessionMap.get("username");
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		PreparedStatement pStmt = null;
		int result = 0;
		Connection conn = ReportsDao.getStaticConnection();
		if (logger.isDebugEnabled()) {
			logger.debug("updateDocFormMPO(PODTO, HttpServletRequest) - doc_id"
					+ bean.getDoc_id());
		}
		String query = "update podb_document set " + " \n doc_with_po = ?,"
				+ " \n doc_with_wo = ?," + " \n podb_doc_updated_by = ?,"
				+ " \n podb_doc_updated_date = ?"
				+ " \n where lm_po_id = ? and podb_doc_id=?";
		if (logger.isDebugEnabled()) {
			logger.debug("updateDocFormMPO(PODTO, HttpServletRequest) - po"
					+ bean.getIn_with_po());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("updateDocFormMPO(PODTO, HttpServletRequest) - wo"
					+ bean.getIn_with_wo());
		}
		try {
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, bean.getIn_with_po());
			pStmt.setString(2, bean.getIn_with_wo());
			pStmt.setString(3, loginUserName);
			pStmt.setTimestamp(4, ts);
			pStmt.setString(5, bean.getPoId());
			pStmt.setString(6, bean.getDoc_id());
			result = pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error("updateDocFormMPO(PODTO, HttpServletRequest)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		return result;
	}

	/**
	 * Delete doc form mpo.
	 * 
	 * @param bean
	 *            the bean
	 * @return the int
	 */
	public int deleteDocFormMPO(PODTO bean) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		PreparedStatement pStmt = null;
		int result = 0;
		Connection conn = ReportsDao.getStaticConnection();
		String query = "delete from dbo.podb_document where lm_po_id=? and podb_doc_id=?";
		try {
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, bean.getPoId());
			pStmt.setString(2, bean.getDoc_id());
			result = pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error("deleteDocFormMPO(PODTO)", e);
			e.printStackTrace();
			// logger.error("deleteDocFormMPO(PODTO)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		return result;
	}

	/**
	 * Gets the partner id.
	 * 
	 * @param poId
	 *            the po id
	 * @param ds
	 *            the ds
	 * @return the partner id
	 */
	public static String getPartnerId(String poId, DataSource ds) {
		String partner_id = "";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select lm_po_partner_id from dbo.lm_purchase_order where lm_po_id ="
							+ poId);
			if (rs.next()) {
				partner_id = rs.getString(1);

			}

		} catch (Exception e) {
			logger.error("getPartnerId(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getPartnerId(String, DataSource) - Exception caught in checking fieldsheet"
						+ e);
			}
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return partner_id;
	}

	// general po info
	/**
	 * Gets the pW odetaildata.
	 * 
	 * @param powoid
	 *            the powoid
	 * @param appendixType
	 *            the appendix type
	 * @param ds
	 *            the ds
	 * @return the pW odetaildata
	 */
	public static PoWoInfo getPWOdetaildata(String powoid, String appendixType,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		POWODetail powodetail = null;
		PoWoInfo poWoInfo = new PoWoInfo();
		POGeneralInfo pOGeneralInfo = new POGeneralInfo();
		GeneralPOSetup generalPOSetup = new GeneralPOSetup();
		IlexTimestamp ilexTimestamp = new IlexTimestamp();

		DecimalFormat dfcur = new DecimalFormat("###0.00");
		String sql = "";
		String deliverbydate = "";
		String tempSpInst = "";
		String tempSpCond = "";
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			sql = "select * from dbo.func_lm_powo_detail('" + powoid + "')";

			if (logger.isDebugEnabled()) {
				logger.debug("getPWOdetaildata(String, String, DataSource) - POWO detail::::::::::::"
						+ sql);
			}

			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				pOGeneralInfo.setPoNumber(rs.getString("lm_po_number"));
				pOGeneralInfo.setPoType(rs.getString("lm_po_type"));
				pOGeneralInfo.setRevisionLevel((rs.getString("revision"))
						.equals("0") ? "" : rs.getString("revision"));
				if (logger.isDebugEnabled()) {
					logger.debug("getPWOdetaildata(String, String, DataSource) - pOGeneralInfo.setPoType: "
							+ pOGeneralInfo.getPoType());
				}
				/*
				 * pOGeneralInfo.SET.setIssuedate(
				 * rs.getString("lm_po_issue_date") ); deliverbydate =
				 * rs.getString("lm_po_deliver_by_date");
				 * if(deliverbydate.equals("")) {
				 * if(appendixType.equalsIgnoreCase("NetMedX")) deliverbydate =
				 * rs.getString("lm_tc_preferred_arrival"); else deliverbydate =
				 * rs.getString("lm_js_planned_start_date"); }
				 * if(!deliverbydate.equals("")) {
				 * powoform.setDeliverbydate(deliverbydate.substring(0, 10));
				 * if(deliverbydate.substring(11,12).equals(" "))
				 * powoform.setDeliverbydatehh
				 * ("0"+deliverbydate.substring(12,13)); else
				 * powoform.setDeliverbydatehh(deliverbydate.substring(11,13));
				 * powoform.setDeliverbydatemm(deliverbydate.substring(14,16));
				 * powoform.setDeliverbydateop(deliverbydate.substring(23,25));
				 * }
				 */

				pOGeneralInfo.setTerms(rs.getString("lm_po_terms"));
				pOGeneralInfo.setTermsDesc(rs
						.getString("lm_pwo_terms_master_data"));
				pOGeneralInfo
						.setMasterPOItem(rs.getString("lm_po_pwo_type_id"));
				pOGeneralInfo.setMasterPOItemDesc(rs
						.getString("lm_pwo_type_desc"));
				// powoform.setShiptoaddress(
				// rs.getString("lm_po_ship_to_address") );
				poWoInfo.setPoSpecialConditions(rs
						.getString("lm_po_special_conditions"));
				poWoInfo.setWoNatureOfActivity(rs.getString("lm_po_sow"));

				// System.out.println("assumptions:::"+rs.getString("lm_po_assumption")
				// );
				// powoform.setAssumption( rs.getString("lm_po_assumption") );

				// powoform.setAuthorizedtotal( dfcur.format(
				// rs.getFloat("lm_po_authorised_total") )+"" );
				// powoform.setFcost( powoform.getAuthorizedtotal());
				// powoform.setInvoiceno(rs.getString("lm_recon_inv_no"));
				// powoform.setInvoicerecddate(
				// rs.getString("lm_recon_inv_recd_date") );

				String invoice_total = "";
				invoice_total = rs.getString("lm_recon_invoice_total");

				/*
				 * if( rs.getString("lm_recon_invoice_total") != null )
				 * powoform.setInvoicetotal( dfcur.format(
				 * rs.getFloat("lm_recon_invoice_total") )+"" ); else
				 * powoform.setInvoicetotal(
				 * rs.getString("lm_recon_invoice_total") );
				 */

				/*
				 * if( invoice_total != null ) powoform.setInvoicetotal(
				 * dfcur.format(Float.parseFloat(invoice_total) )+"" ); else
				 * powoform.setInvoicetotal( invoice_total );
				 * 
				 * String approved_total = ""; approved_total =
				 * rs.getString("lm_recon_approved_total");
				 * 
				 * if( approved_total != null ) powoform.setApprovedtotal(
				 * dfcur.format( Float.parseFloat(approved_total))+"" ); else
				 * powoform.setApprovedtotal( approved_total );
				 * 
				 * if( rs.getString("lm_recon_approved_total") != null )
				 * powoform.setApprovedtotal( dfcur.format(
				 * rs.getFloat("lm_recon_approved_total") )+"" ); else
				 * powoform.setApprovedtotal(
				 * rs.getString("lm_recon_approved_total") );
				 * 
				 * powoform.setInvoicecomments(
				 * rs.getString("lm_recon_comments") );
				 */

				generalPOSetup.setDeliverByDate(IlexTimestamp
						.converToTimestamp(rs
								.getString("lm_po_deliver_by_date")));

				generalPOSetup.setIssueDate(IlexTimestamp.converToTimestamp(rs
						.getString("lm_po_issue_date")));

				poWoInfo.setWoSpecialInstructions(rs
						.getString("lm_po_special_instructions"));
				pOGeneralInfo.setPartnerId(rs.getString("lm_po_partner_id"));
				pOGeneralInfo.setPartner(POWODetaildao.getPartnername(ds,
						pOGeneralInfo.getPartnerId()));
				pOGeneralInfo.setContact(PurchaseOrderDAO.getPOContact(
						rs.getString("lm_po_partner_poc_id"), ds));
				pOGeneralInfo.setContactPoc(rs
						.getString("lm_po_partner_poc_id"));
				String lm_tc_spec_instruction = "";
				lm_tc_spec_instruction = rs.getString("lm_tc_spec_instruction");
				String pf_sp_instruction = "";
				pf_sp_instruction = rs.getString("pf_sp_instruction");
				String lm_si_spec_condition = "";
				lm_si_spec_condition = rs.getString("lm_si_spec_condition");
				String pf_sp_condition = "";
				pf_sp_condition = rs.getString("pf_sp_condition");
				String lm_tc_problem_desc = "";
				lm_tc_problem_desc = rs.getString("lm_tc_problem_desc");

				/* Set sp instruction and condition for NetMedX project */
				if (appendixType.equals("NetMedX")) {
					if (poWoInfo.getWoSpecialInstructions() != null) {
					} else {
						if (!lm_tc_spec_instruction.equals("")) {
							tempSpInst = lm_tc_spec_instruction;
						}
						if (!pf_sp_instruction.equals(""))
							tempSpInst = tempSpInst + "\n" + pf_sp_instruction;
						poWoInfo.setWoSpecialInstructions(tempSpInst);
					}

					if (poWoInfo.getPoSpecialConditions() != null) {
					} else {
						if (!lm_si_spec_condition.equals(""))
							tempSpCond = lm_si_spec_condition;
						if (!pf_sp_condition.equals(""))
							tempSpCond = tempSpCond + "\n" + pf_sp_condition;

						poWoInfo.setPoSpecialConditions(tempSpCond);
					}

					if (poWoInfo.getWoNatureOfActivity().equals(""))
						poWoInfo.setWoNatureOfActivity(lm_tc_problem_desc);

					// powoform.setProblemdesc(lm_tc_problem_desc);
				}

				/* Set sp instruction and condition for project */
				else {
					if (poWoInfo.getWoSpecialInstructions() != null) {
					} else {
						if (!pf_sp_instruction.equals(""))
							tempSpInst = pf_sp_instruction;

						poWoInfo.setWoSpecialInstructions(tempSpInst);
					}

					if (poWoInfo.getPoSpecialConditions() != null) {
					} else {
						if (!pf_sp_condition.equals(""))
							tempSpCond = pf_sp_condition;

						poWoInfo.setPoSpecialConditions(tempSpCond);
					}
				}
				// powoform.setDeliverable(rs.getString("lm_po_deliverables"));

				pOGeneralInfo.setPoStatus(rs.getInt("po_status"));
				pOGeneralInfo.setPoStatusName(POStatusTypes
						.getStatusName(pOGeneralInfo.getPoStatus()));
				// added for PVS Justification
				pOGeneralInfo.setPvsJustification(rs
						.getInt("lm_po_pvs_justification"));
				// added for geographic constraint
				pOGeneralInfo.setGeographicConstraint(rs
						.getInt("lm_po_geographic_constraint"));
			}

			pOGeneralInfo.setGeneralPOSetup(generalPOSetup);
			poWoInfo.setPoGeneralInfo(pOGeneralInfo);
		} catch (Exception e) {
			logger.error("getPWOdetaildata(String, String, DataSource)", e);

			logger.error("getPWOdetaildata(String, String, DataSource)", e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);

		}
		return poWoInfo;
	}

	// Delivrables

	/**
	 * Gets the general po detail list for delivrables.
	 * 
	 * @param map
	 *            the map
	 * @param bean
	 *            the bean
	 * @param tabId
	 *            the tab id
	 * @return the general po detail list for delivrables
	 */
	public void getGeneralPODetailListForDelivrables(Map<String, Object> map,
			PODTO bean, String tabId) {
		logger.trace("Start :(Class : com.mind.docm.dao.DocMSeacrhDao | Method : getColumns)");
		if (logger.isDebugEnabled()) {
			logger.debug("getGeneralPODetailListForDelivrables(HttpServletRequest, PODTO, String) - in getGeneralMPODetailListForDelivrables");
		}
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		ArrayList<ArrayList> outer = new ArrayList<ArrayList>();
		ArrayList<String> inner = null;
		String fileId = "";
		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery(" select isnull(lm_pwo_type_desc, '') as lm_pwo_type_desc, pod.lm_po_id,podb_mn_dl_id,podb_mn_dl_title,podb_mn_dl_type,"
							+ " podb_mn_dl_format,podb_mn_dl_desc,isnull(podb_mn_mobile_upload_allowed,'n') as podb_mn_mobile_upload_allowed,"
							+ " isnull(podb_mn_source,'') as podb_mn_source ,isnull(podb_mn_dl_status,0) as podb_mn_dl_status,"
							+ " podb_mn_dl_doc_id, podb_mn_dl_updated_date as podb_mn_dl_updated_date "
							+ " , podb_mn_dl_created_date as podb_mn_dl_created_date "
							+ " from "
							+ " dbo.podb_purchase_order_deliverables pod "
							+ " LEFT JOIN lm_purchase_order lpo ON lpo.lm_po_id = pod.lm_po_id "
							+ " LEFT JOIN lm_pwo_type_master  ON lm_pwo_type_id = lm_po_pwo_type_id "
							+ "where pod.lm_po_id=" + bean.getPoId());
			while (rs.next()) {
				if (rs.getString("lm_pwo_type_desc").equals("Material")) {

					if (rs.getString("podb_mn_dl_title").equals("Before Work")) {
						continue;
					} else if (rs.getString("podb_mn_dl_title").equals(
							"After Work")) {
						continue;
					} else if (rs.getString("podb_mn_dl_title").equals(
							"Work Order")) {
						continue;
					} else if (rs.getString("podb_mn_dl_title").equals(
							"Equipment Cabinet/Rack")) {
						continue;
					}
				}
				inner = new ArrayList<String>();
				inner.add(rs.getString("lm_po_id"));
				inner.add(rs.getString("podb_mn_dl_id"));
				inner.add(rs.getString("podb_mn_dl_title"));
				inner.add(rs.getString("podb_mn_dl_type"));
				inner.add(rs.getString("podb_mn_dl_format"));
				inner.add(rs.getString("podb_mn_dl_desc"));
				inner.add(PODeliverableStatusTypes.getStatusName(Integer
						.parseInt(rs.getString("podb_mn_dl_status"))));
				inner.add(rs.getString("podb_mn_dl_doc_id"));

				// date will be shown only in case of accepted deliverables
				// if (rs.getString("podb_mn_dl_status").equals(
				// PODeliverableStatusTypes.STATUS_ACCEPTED + "")) {
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm aaa");
				if (rs.getObject("podb_mn_dl_updated_date") != null) {
					inner.add(df.format(rs.getObject("podb_mn_dl_updated_date")));
				} else {
					inner.add("");
				}
				if (rs.getString("podb_mn_dl_doc_id") != null) {
					fileId = DocumentUtility.getfileIdFromDoc(rs
							.getString("podb_mn_dl_doc_id"));
				}
				inner.add(fileId);
				if (rs.getString("podb_mn_mobile_upload_allowed").equals("1")
						|| rs.getString("podb_mn_mobile_upload_allowed")
								.equals("true")
						|| rs.getString("podb_mn_mobile_upload_allowed")
								.equals("y")
						|| rs.getString("podb_mn_mobile_upload_allowed")
								.equals("yes")) {
					inner.add("y");
				} else {
					inner.add("n");
				}

				if (rs.getString("podb_mn_source") == null
						|| "".equals(rs.getString("podb_mn_source")))
					inner.add("Internal");
				else
					inner.add(rs.getString("podb_mn_source"));
				inner.add(rs.getString("podb_mn_dl_status"));

				inner.add(df.format(rs.getDate("podb_mn_dl_created_date")));
				outer.add(inner);
			}
			map.put("devList", outer);

		} catch (SQLException e) {
			logger.error(
					"getGeneralPODetailListForDelivrables(HttpServletRequest, PODTO, String)",
					e);

			logger.error(
					"getGeneralPODetailListForDelivrables(HttpServletRequest, PODTO, String)",
					e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * Change deliverable status.
	 * 
	 * @param devId
	 *            the dev id
	 * @param devStatusCode
	 *            the dev status code
	 * @param poId
	 *            the po id
	 * @param loginUser
	 *            the login user
	 * @param ds
	 *            the ds
	 */
	public static void changeDeliverableStatus(String devId,
			String devStatusCode, String poId, String loginUser, DataSource ds) {
		PreparedStatement pStmt = null;
		String sql = "";
		Connection conn = null;
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		sql = "update dbo.podb_purchase_order_deliverables set "
				+ " \n podb_mn_dl_status = ?,"
				+ " \n podb_mn_dl_updated_by = ?,"
				+ " \n podb_mn_dl_updated_date = ?"
				+ "\n where lm_po_id = ? and podb_mn_dl_id = ?";
		try {
			conn = ds.getConnection();
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, devStatusCode);
			pStmt.setString(2, loginUser);
			pStmt.setTimestamp(3, ts);
			pStmt.setString(4, poId);
			pStmt.setString(5, devId);

			pStmt.executeUpdate();
		} catch (SQLException e) {
			logger.error(
					"changeDeliverableStatus(String, String, String, String, DataSource)",
					e);

			logger.error(
					"changeDeliverableStatus(String, String, String, String, DataSource)",
					e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * Save general mpo detail list for delivrables.
	 * 
	 * @param sessionMap
	 *            the session map
	 * @param bean
	 *            the bean
	 * @param tabId
	 *            the tab id
	 */
	public void saveGeneralMPODetailListForDelivrables(
			Map<String, Object> sessionMap, PODTO bean, String tabId) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		PreparedStatement pStmt = null;
		Statement stmt = null;
		String sql = "";
		ResultSet rs1 = null;
		Connection conn = ReportsDao.getStaticConnection();
		String loginuserid = (String) sessionMap.get("userid");
		if (logger.isDebugEnabled()) {
			logger.debug("saveGeneralMPODetailListForDelivrables(HttpServletRequest, PODTO, String) - login userId"
					+ loginuserid);
		}
		String loginUserName = (String) sessionMap.get("username");
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		int result = 0;
		try {
			sql = "update dbo.podb_purchase_order_deliverables "
					+ " \n set podb_mn_dl_title=?,"
					+ " \n podb_mn_dl_type = ?," + " \n podb_mn_dl_format = ?,"
					+ " \n podb_mn_dl_desc = ?,"
					+ " \n podb_mn_dl_updated_by = ?,"
					+ " \n podb_mn_dl_updated_date = ?"
					+ "\n where lm_po_id = ? and podb_mn_dl_id = ?";

			try {
				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, bean.getDevTitle());
				pStmt.setString(2, bean.getDevType());
				pStmt.setString(3, bean.getDevFormat());
				pStmt.setString(4, bean.getDevDescription());
				pStmt.setString(5, loginUserName);
				pStmt.setTimestamp(6, ts);
				if (bean.getDevId().trim().equalsIgnoreCase("")) {
					pStmt.setString(7, "-1");
					pStmt.setString(8, "-1");
				} else {
					pStmt.setString(7, bean.getPoId());
					pStmt.setString(8, bean.getDevId());
				}

				result = pStmt.executeUpdate();
			} catch (SQLException e) {
				logger.error(
						"saveGeneralMPODetailListForDelivrables(HttpServletRequest, PODTO, String)",
						e);

				logger.error(
						"saveGeneralMPODetailListForDelivrables(HttpServletRequest, PODTO, String)",
						e);
			}
			try {
				if (result <= 0) {
					sql = "insert into dbo.podb_purchase_order_deliverables (lm_po_id,podb_mn_dl_title,podb_mn_dl_type,"
							+ "podb_mn_dl_format,podb_mn_dl_desc,podb_mn_dl_created_by,podb_mn_dl_created_date,podb_mn_dl_updated_by,podb_mn_dl_updated_date) values"
							+ "\n (?,?,?,?,?,?,?,?,?)";
					pStmt = conn.prepareStatement(sql);
					pStmt.setString(1, bean.getPoId());
					pStmt.setString(2, bean.getDevTitle());
					pStmt.setString(3, bean.getDevType());
					pStmt.setString(4, bean.getDevFormat());
					pStmt.setString(5, bean.getDevDescription());
					pStmt.setString(6, loginUserName);
					pStmt.setTimestamp(7, ts);
					pStmt.setString(8, loginUserName);
					pStmt.setTimestamp(9, ts);
					pStmt.executeUpdate();
				}
			} catch (SQLException e) {
				logger.error(
						"saveGeneralMPODetailListForDelivrables(HttpServletRequest, PODTO, String)",
						e);

				logger.error(
						"saveGeneralMPODetailListForDelivrables(HttpServletRequest, PODTO, String)",
						e);
			}
		} catch (Exception e) {
			logger.error(
					"saveGeneralMPODetailListForDelivrables(HttpServletRequest, PODTO, String)",
					e);

			logger.error(
					"saveGeneralMPODetailListForDelivrables(HttpServletRequest, PODTO, String)",
					e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * Gets the document details.
	 * 
	 * @param poId
	 *            the po id
	 * @param devId
	 *            the dev id
	 * @return the document details
	 */
	public static PODTO getDocumentDetails(String poId, String devId) {
		logger.trace("Start :getDocumentId(String poId,String devId)");
		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String sql = "";
		PODTO poDTO = new PODTO();
		if (logger.isDebugEnabled()) {
			logger.debug("getDocumentDetails(String poId,String devId) - connection"
					+ conn);
		}
		sql = "select podb_mn_dl_title,podb_mn_dl_format,podb_mn_dl_doc_id,lm_po_js_id  from podb_purchase_order_deliverables  join lm_purchase_order on podb_purchase_order_deliverables.lm_po_id=lm_purchase_order.lm_po_id";
		sql += " where podb_mn_dl_id=? and podb_purchase_order_deliverables.lm_po_id=? ";
		try {
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, Integer.valueOf(devId));
			pStmt.setInt(2, Integer.valueOf(poId));
			rs = pStmt.executeQuery();
			if (rs.next()) {
				poDTO.setDoc_id(rs.getString("podb_mn_dl_doc_id"));
				poDTO.setDevTitle(rs.getString("podb_mn_dl_title"));
				poDTO.setDevFormat(rs.getString("podb_mn_dl_format"));
				poDTO.setJobId(rs.getString("lm_po_js_id"));

			}

		} catch (SQLException e) {
			logger.error("getDocumentDetails(String poId,String devId)", e);

		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return poDTO;
	}

	/**
	 * Generalfetch data.
	 * 
	 * @param bean
	 *            the bean
	 * @param tabId
	 *            the tab id
	 * @param devId
	 *            the dev id
	 */
	public void generalfetchData(PODTO bean, String tabId, String devId) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		Connection conn = ReportsDao.getStaticConnection();
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String sql = "";
		if (logger.isDebugEnabled()) {
			logger.debug("generalfetchData(HttpServletRequest, PODTO, String, String) - connection"
					+ conn);
		}
		try {
			if (!tabId.trim().equalsIgnoreCase("") && tabId != null
					&& tabId.trim().equalsIgnoreCase("3")) {
				if (devId == null) {
					sql = "select podb_mn_dl_id, lm_po_id, podb_mn_dl_title, podb_mn_dl_type, podb_mn_dl_format, podb_mn_dl_desc from podb_purchase_order_deliverables mpod where lm_po_id= ? ";
					pStmt = conn.prepareStatement(sql);
					if (logger.isDebugEnabled()) {
						logger.debug("generalfetchData(HttpServletRequest, PODTO, String, String) - id"
								+ bean.getPoId()
								+ "--- appen---"
								+ bean.getMp_appendix_id());
					}
					pStmt.setInt(1, new Integer(bean.getPoId()).intValue());
				} else {
					sql = "select podb_mn_dl_id, lm_po_id, podb_mn_dl_title, podb_mn_dl_type, podb_mn_dl_format, podb_mn_dl_desc,isnull(podb_mn_mobile_upload_allowed ,'n') as podb_mn_mobile_upload_allowed, dev_date = convert(varchar(10),getdate(),101) from podb_purchase_order_deliverables mpod where lm_po_id= ? and podb_mn_dl_id = ?";
					pStmt = conn.prepareStatement(sql);
					if (logger.isDebugEnabled()) {
						logger.debug("generalfetchData(HttpServletRequest, PODTO, String, String) - id"
								+ bean.getPoId()
								+ "--- appen---"
								+ bean.getMp_appendix_id());
					}
					pStmt.setInt(1, new Integer(bean.getPoId()).intValue());
					pStmt.setInt(2, new Integer(devId));
				}

				if (logger.isDebugEnabled()) {
					logger.debug("generalfetchData(HttpServletRequest, PODTO, String, String) - sql sql: "
							+ sql);
				}
				if (logger.isDebugEnabled()) {
					logger.debug("generalfetchData(HttpServletRequest, PODTO, String, String) - devId: "
							+ devId);
				}
				if (logger.isDebugEnabled()) {
					logger.debug("generalfetchData(HttpServletRequest, PODTO, String, String) - poid: "
							+ bean.getPoId());
				}
				rs = pStmt.executeQuery();
				if (rs.next()) {
					bean.setPoId(rs.getString("lm_po_id"));
					bean.setDevId(rs.getString("podb_mn_dl_id"));
					bean.setDevTitle(rs.getString("podb_mn_dl_title"));
					bean.setDevType(rs.getString("podb_mn_dl_type"));
					bean.setDevFormat(rs.getString("podb_mn_dl_format"));
					bean.setDevDescription(rs.getString("podb_mn_dl_desc"));
					bean.setDevDate(rs.getString("dev_date"));
					bean.setMobileUploadAllowed(rs.getString(
							"podb_mn_mobile_upload_allowed").charAt(0));
				} else {
					// bean.setMpoId("");
					bean.setDevId("");
					bean.setDevTitle("");
					bean.setDevType("");
					bean.setDevFormat("");
					bean.setDevDescription("");
					bean.setDevDate("");
					bean.setMobileUploadAllowed('n');
				}
			}
		} catch (Exception e) {
			logger.error(
					"generalfetchData(HttpServletRequest, PODTO, String, String)",
					e);

			logger.error(
					"generalfetchData(HttpServletRequest, PODTO, String, String)",
					e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * Discription: This method is used to get resource id for a po.
	 * 
	 * @param poId
	 *            -- PO Id
	 * @param ds
	 *            -- Database Object
	 * @return String[] -- Resource Ids
	 */
	public static String[] getResouceIdByPO(String poId, DataSource ds) {

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		StringBuffer resourceList = new StringBuffer();
		try {
			conn = ds.getConnection();
			String sql = "select distinct lm_activity.lm_at_lib_id from lm_purchase_work_order"
					+ " inner join lm_resource on lm_rs_id = lm_pwo_rs_id"
					+ " inner join lm_activity on lm_at_id = lm_rs_at_id "
					+ " where lm_pwo_po_id  = ?";
			pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, poId);
			rs = pStmt.executeQuery();

			// pStmt1=conn.prepareStatement("Select getDate()");
			while (rs.next()) {

				resourceList.append(rs.getString(1) + ",");
			}

		} catch (Exception e) {
			logger.error("getResouceIdByPO(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		String[] resourceIds = Util.removeLastComm(resourceList.toString())
				.split(",");
		if (resourceIds.length == 1 && resourceList.toString().equals(""))
			resourceIds = new String[0];

		return resourceIds;
	}

	/**
	 * Gets the mPO total cost.
	 * 
	 * @param poId
	 *            the po id
	 * @param ds
	 *            the ds
	 * @return the mPO total cost
	 */
	public static float getMPOTotalCost(String poId, DataSource ds) {

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		float totalCost = 0.0f;
		try {
			conn = ds.getConnection();
			String sql = "select isnull(mp_mn_estimated_cost,0.0) from mp_main "
					+ "inner join lm_purchase_order on mpo_id = mp_mn_id "
					+ "where lm_po_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, poId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				totalCost = rs.getFloat(1);
			}
		} catch (Exception e) {
			logger.error("getMPOTotalCost(String, DataSource)", e);

			logger.error("getMPOTotalCost(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return totalCost;
	}

	/**
	 * Gets the pO authorized total cost.
	 * 
	 * @param poId
	 *            the po id
	 * @param ds
	 *            the ds
	 * @return the pO authorized total cost
	 */
	public static Float[] getPOAuthorizedTotalCost(String poId, DataSource ds) {

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		Float[] showAuthorizedTotalCost = new Float[2];
		try {
			conn = ds.getConnection();
			String sql = "select lm_po_authorised_total = isnull(lm_po_authorised_total,0.0), "
					+ "diff = isnull(lm_po_authorised_total,0.0) - isnull(sum(lm_pwo_authorised_total_cost),0.0) "
					+ "from lm_purchase_order left outer join lm_purchase_work_order on lm_po_id = lm_pwo_po_id "
					+ "where lm_po_id = ? group by lm_pwo_po_id, lm_po_authorised_total";

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, poId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				showAuthorizedTotalCost[0] = rs
						.getFloat("lm_po_authorised_total");
				showAuthorizedTotalCost[1] = rs.getFloat("diff");
			}
		} catch (Exception e) {
			logger.error("getPOAuthorizedTotalCost(String, DataSource)", e);

			logger.error("getPOAuthorizedTotalCost(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return showAuthorizedTotalCost;
	}

	/**
	 * Gets the pO deliverable.
	 * 
	 * @param poId
	 *            the po id
	 * @param devId
	 *            the dev id
	 * @param ds
	 *            the ds
	 * @return the pO deliverable
	 */
	public static PODeliverable getPODeliverable(String poId, String devId,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		PODeliverable poDeliverable = new PODeliverable();

		try {
			conn = DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME)
					.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery(" select convert(varchar(10), podb_mn_dl_updated_date, 1) as podb_mn_dl_updated_date,lm_po_id,podb_mn_dl_id,podb_mn_dl_title,podb_mn_dl_type,"
							+ "podb_mn_dl_format,podb_mn_dl_desc,podb_mn_dl_status,podb_mn_dl_doc_id from "
							+ "dbo.podb_purchase_order_deliverables where lm_po_id="
							+ poId + " and podb_mn_dl_id=" + devId);
			if (rs.next()) {
				poDeliverable.setDevId(rs.getString("podb_mn_dl_id"));
				poDeliverable.setTitle(rs.getString("podb_mn_dl_title"));
				poDeliverable.setType(rs.getString("podb_mn_dl_type"));
				poDeliverable.setFormat(rs.getString("podb_mn_dl_format"));
				poDeliverable.setDescription(rs.getString("podb_mn_dl_desc"));
				poDeliverable.setDate(rs.getString("podb_mn_dl_updated_date"));
				poDeliverable.setDocId(rs.getString("podb_mn_dl_doc_id"));
				poDeliverable.setStatus(PODeliverableStatusTypes
						.getStatusName(Integer.parseInt(rs
								.getString("podb_mn_dl_status"))));
			}

		} catch (Exception e) {
			logger.error(
					"getPODeliverable(HttpServletRequest, String, String, DataSource)",
					e);

			logger.error(
					"getPODeliverable(HttpServletRequest, String, String, DataSource)",
					e);
		} finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		return poDeliverable;
	}

	/**
	 * Gets the standard contact line.
	 * 
	 * @param loginUserId
	 *            the login user id
	 * @param ds
	 *            the ds
	 * @return the standard contact line
	 */
	public static String getStandardContactLine(String loginUserId,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String specialContactLine = "";
		try {
			conn = ds.getConnection();
			String sql = "select poc_name = isnull(dbo.func_cc_poc_name("
					+ loginUserId
					+ "),''),poc_cell = isnull(dbo.func_cc_poc_cell("
					+ loginUserId
					+ "),''),poc_phone = isnull(dbo.func_cc_poc_phone("
					+ loginUserId + "),'')";
			if (logger.isDebugEnabled()) {
				logger.debug("getStandardContactLine(String, DataSource) - sql: "
						+ sql);
			}
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				specialContactLine = "Please call " + rs.getString("poc_name")
						+ " with Contingent upon arrival on site at ";
				if (!rs.getString("poc_phone").equals("")) {
					specialContactLine = specialContactLine
							+ rs.getString("poc_phone") + " or ";
				}
				if (!rs.getString("poc_cell").equals("")) {
					specialContactLine = specialContactLine
							+ rs.getString("poc_cell") + " (cell)";
				}
			}
		} catch (Exception e) {
			logger.error("getStandardContactLine(String, DataSource)", e);

			logger.error("getStandardContactLine(String, DataSource)", e);
		} finally {
			try {
				DBUtil.close(rs, stmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error("getStandardContactLine(String, DataSource)", e);

				logger.error("getStandardContactLine(String, DataSource)", e);
			}

		}
		return specialContactLine;
	}

	/**
	 * Checks if is editable po.
	 * 
	 * @param poId
	 *            the po id
	 * @param ds
	 *            the ds
	 * @return true, if is editable po
	 */
	public boolean isEditablePO(String poId, DataSource ds) {
		if (logger.isDebugEnabled()) {
			logger.debug("isEditablePO(String, DataSource) - -------- poStatus -------"
					+ poId);
		}
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean isEditable = false;
		try {
			conn = ds.getConnection();
			String sql = "select action_on_po from lm_po_allowed_actions where is_allowed=1 and current_po_status in (select po_status from lm_purchase_order where lm_po_id = ?) and action_on_po=3";
			if (logger.isDebugEnabled()) {
				logger.debug("isEditablePO(String, DataSource) - sql: " + sql);
			}
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, poId);
			rs = pstmt.executeQuery();
			if (rs.next())
				isEditable = true;
		} catch (Exception e) {
			logger.error("isEditablePO(String, DataSource)", e);

			logger.error("isEditablePO(String, DataSource)", e);
		} finally {
			try {
				DBUtil.close(rs, pstmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error("isEditablePO(String, DataSource)", e);

				logger.error("isEditablePO(String, DataSource)", e);
			}

		}
		return isEditable;
	}

	/**
	 * Get eInvoice summary data from database.
	 * 
	 * @param poId
	 *            the po id
	 * @param ds
	 *            the ds
	 * @return the e invoice summary
	 */
	public static EInvoiceSummary getEInvoiceSummary(String poId, DataSource ds) {

		EInvoiceSummary eInvoiceSummary = new EInvoiceSummary();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		DecimalFormat dfcur = new DecimalFormat("######0.00");

		try {
			conn = ds.getConnection();
			String sql = "select * from func_get_partner_eInvoice_summary(?) order by invoice_number";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, poId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				eInvoiceSummary.setInvoiceId(rs.getString("invoice_id"));
				eInvoiceSummary
						.setInvoiceNumber(rs.getString("invoice_number"));
				eInvoiceSummary.setInvoiceType(rs.getString("invoice_type"));
				eInvoiceSummary
						.setInvoiceStatus(rs.getString("invoice_status"));
				eInvoiceSummary.setInvoiceDate(rs.getString("invoice_date"));
				eInvoiceSummary.setInvoiceReceivedDate(rs
						.getString("invoice_recived_date"));
				eInvoiceSummary.setInvoicePartnerComment(rs
						.getString("invoice_partner_comment"));
				eInvoiceSummary.setInvoiceRecivedBy(rs
						.getString("invoice_recived_by"));
				eInvoiceSummary.setInvoiceApprovedDate(rs
						.getString("invoice_approved_date"));
				eInvoiceSummary.setInvoiceApprovedBy(rs
						.getString("invoice_approved_by"));
				eInvoiceSummary.setInvoiceExplanation(rs
						.getString("invoice_explanation"));
				eInvoiceSummary.setInvoicePaidDate(rs
						.getString("invoice_paid_date"));
				eInvoiceSummary.setInvoiceCheckNumber(rs
						.getString("invoice_check_number"));
				eInvoiceSummary.setInvoiceAuthorisedTotal(rs
						.getString("invoice_authorised_total"));
				eInvoiceSummary.setInvoiceApprovedTotal(dfcur.format(Float
						.parseFloat(rs.getString("invoice_approved_total"))));
				eInvoiceSummary.setInvoiceTotal(rs.getString("invoice_total"));
				eInvoiceSummary.setInvoiceLastUpdatedBy(rs
						.getString("invoice_last_updated_by"));
				eInvoiceSummary.setInvoiceLastUpdate(rs
						.getString("invoice_last_update"));
				eInvoiceSummary.setLineTaxAmount(rs
						.getString("line_tax_amount"));
				eInvoiceSummary.setLineTotal(rs.getString("line_total"));
			}
		} catch (Exception e) {
			logger.error("getEInvoiceSummary(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEInvoiceSummary(String, DataSource) - Exception in com.mind.PO.dao.setEInvoiceSummary "
						+ e);
			}
			logger.error("getEInvoiceSummary(String, DataSource)", e);
		} finally {
			try {
				DBUtil.close(rs, pstmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error("getEInvoiceSummary(String, DataSource)", e);

				logger.error("getEInvoiceSummary(String, DataSource)", e);
			}

		}
		return eInvoiceSummary;
	}// - End of setEInvoiceSummary

	/**
	 * Get eInvoice Line Items from database.
	 * 
	 * @param poId
	 *            the po id
	 * @param ds
	 *            the ds
	 * @return the e invoice items
	 */
	public static ArrayList<EInvoiceItems> getEInvoiceItems(String poId,
			DataSource ds) {
		ArrayList<EInvoiceItems> eInvoiceItems = new ArrayList<EInvoiceItems>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			String sql = "select * from func_get_partner_invoice_line_item(?) order by line_item";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, poId);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				EInvoiceItems items = new EInvoiceItems();
				items.setLineItem(rs.getString("line_item"));
				items.setLineType(rs.getString("line_type"));
				items.setLineDiscription(rs.getString("line_discription"));
				items.setLineQuantity(rs.getString("line_quantity"));
				items.setLineUnitCost(rs.getString("line_unit_cost"));
				items.setLineTotalCost(rs.getString("line_total_cost"));
				eInvoiceItems.add(items);
			}

		} catch (Exception e) {
			logger.error("getEInvoiceItems(String, DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getEInvoiceItems(String, DataSource) - Exception in com.mind.PO.dao.setEInvoiceSummary "
						+ e);
			}
			logger.error("getEInvoiceItems(String, DataSource)", e);
		} finally {
			try {
				DBUtil.close(rs, pstmt);
				DBUtil.close(conn);
			} catch (Exception e) {
				logger.error("getEInvoiceItems(String, DataSource)", e);

				logger.error("getEInvoiceItems(String, DataSource)", e);
			}

		}

		return eInvoiceItems;
	} // - End of getEInvoiceItemsList();

	/**
	 * Set Approved Total.
	 * 
	 * @param piId
	 *            the pi id
	 * @param comments
	 *            the comments
	 * @param approvedTotal
	 *            the approved total
	 * @param userId
	 *            the user id
	 * @param ds
	 *            the ds
	 * @return the int
	 */
	public static int setApprovedTotal(String piId, String comments,
			String approvedTotal, String userId, DataSource ds) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int flag = -1;
		try {

			conn = ds.getConnection();
			cstmt = conn
					.prepareCall("{?=call lm_pi_approve_partner_invoice_approved_01(?,?,?,?,?)}"); // -
																									// 5
																									// parameter
																									// +
																									// on
																									// out
																									// parameter
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, piId);
			cstmt.setString(3, comments);
			cstmt.setString(4, approvedTotal);
			cstmt.setString(5, userId);
			cstmt.setString(6, "");
			cstmt.execute();
			flag = cstmt.getInt(1);

			if (logger.isDebugEnabled()) {
				logger.debug("setApprovedTotal(String, String, String, String, DataSource) - Ticket Update Flag= "
						+ flag);
			}
		} catch (Exception e) {
			logger.error(
					"setApprovedTotal(String, String, String, String, DataSource)",
					e);

			if (logger.isDebugEnabled()) {
				logger.debug("setApprovedTotal(String, String, String, String, DataSource) - Exception in com.mind.PO.dao.setApprovedTotal()"
						+ e);
			}
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);
		}
		return flag;

	} // - end of setApprovedTotal() method

	/**
	 * Gets the po deliverable type.
	 * 
	 * @return the po deliverable type
	 */
	public static ArrayList<LabelValue> getPoDeliverableType() {
		ArrayList<LabelValue> poDeliverableType = new ArrayList<LabelValue>();
		// poDeliverableType.add(new LabelValue("--Select--", ""));
		poDeliverableType.add(new LabelValue("Customer", "Customer"));
		poDeliverableType.add(new LabelValue("Internal", "Internal"));
		// poDeliverableType.add( new LabelValue("Other", "Other"));

		return poDeliverableType;
	}

	/**
	 * Gets the po deliverable status list.
	 * 
	 * @return the po deliverable status list
	 */
	public static ArrayList<LabelValue> getPoDeliverableStatusList() {
		ArrayList<LabelValue> poDeliverableStatus = new ArrayList<LabelValue>();
		poDeliverableStatus.add(new LabelValue("Accepted", "2"));
		poDeliverableStatus.add(new LabelValue("Pending", "0"));
		poDeliverableStatus.add(new LabelValue("Uploaded", "1"));
		poDeliverableStatus.add(new LabelValue("Rejected", "3"));
		return poDeliverableStatus;
	}

	/**
	 * Update deliverable doc id.
	 * 
	 * @param devId
	 *            the dev id
	 * @param poId
	 *            the po id
	 * @param docId
	 *            the doc id
	 * @param ds
	 *            the ds
	 */
	public static void updateDeliverableDocId(String devId, String poId,
			String docId, DataSource ds) {
		PreparedStatement pStmt = null;
		String sql = "";
		Connection conn = null;
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		sql = "update dbo.podb_purchase_order_deliverables set "
				+ " \n podb_mn_dl_doc_id = ?"
				+ "\n where lm_po_id = ? and podb_mn_dl_id = ?";
		try {
			conn = ds.getConnection();
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, docId);
			pStmt.setString(2, poId);
			pStmt.setString(3, devId);

			pStmt.executeUpdate();
		} catch (SQLException e) {
			logger.error(
					"updateDeliverableDocId(String, String, String, DataSource)",
					e);

			logger.error(
					"updateDeliverableDocId(String, String, String, DataSource)",
					e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * Save po deliverables upload.
	 * 
	 * @param bean
	 *            the bean
	 * @param userId
	 *            the user id
	 * @param docId
	 *            the doc id
	 * @param ds
	 *            the ds
	 */
	public void savePODeliverablesUpload(PODTO bean, String userId,
			String docId, DataSource ds) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");

		PreparedStatement pStmt = null;
		String sql = "";

		if (docId != null && docId.equals("")) {
			docId = "-1";
		}
		Connection conn = null;
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
		int result = 0;
		try {
			conn = ds.getConnection();
			sql = "update dbo.podb_purchase_order_deliverables "
					+ " \n set podb_mn_dl_title=?,"
					+ " \n podb_mn_dl_type = ?," + " \n podb_mn_dl_format = ?,";
			if (bean.getDevDescription() != null
					&& !"".equals(bean.getDevDescription().trim())) {
				sql += " \n podb_mn_dl_desc = ?,";
			}

			sql += " \n podb_mn_dl_updated_by = ?,"
					+ " \n podb_mn_dl_updated_date = ?,"
					+ " \n podb_mn_dl_status = ?,"
					+ " \n podb_mn_dl_doc_id = ?,"
					+ " \n podb_mn_source =?, podb_mn_mobile_upload_allowed = ?"
					+ "\n where podb_mn_dl_id = ?";

			if (bean.getPoId() != null && !"".equals(bean.getPoId())) {
				sql += " AND lm_po_id = ?";
			}
			try {
				int positionCount = 1;

				pStmt = conn.prepareStatement(sql);
				pStmt.setString(positionCount++, bean.getDevTitle());
				pStmt.setString(positionCount++, bean.getDevType());
				pStmt.setString(positionCount++, bean.getDevFormat());
				if (bean.getDevDescription() != null
						&& !"".equals(bean.getDevDescription().trim())) {
					pStmt.setString(positionCount++, bean.getDevDescription());
				}
				pStmt.setString(positionCount++, userId);
				pStmt.setTimestamp(positionCount++, ts);
				pStmt.setInt(positionCount++,
						Integer.parseInt(bean.getDevStatus()));
				pStmt.setInt(positionCount++, Integer.parseInt(docId));
				pStmt.setString(positionCount++, bean.getUploadSource());
				pStmt.setBoolean(positionCount++, bean.isDevMobile());

				if (bean.getDevId().trim().equalsIgnoreCase("")) {
					pStmt.setString(positionCount++, "-1");
				} else {
					pStmt.setString(positionCount++, bean.getDevId());
				}

				if (bean.getPoId() != null && !"".equals(bean.getPoId())) {
					pStmt.setString(positionCount++, bean.getPoId());
				}

				result = pStmt.executeUpdate();
			} catch (SQLException e) {
				System.out.println("Error uploading PO Deliverable details: "
						+ e);
			}
		} catch (Exception e) {
			System.out.println("Error uploading PO Deliverable details: " + e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
	}

	/**
	 * Save po deliverables upload.
	 * 
	 * @param bean
	 *            the bean
	 * @param userId
	 *            the user id
	 * @param docId
	 *            the doc id
	 */
	public void savePODeliverablesUpload(PODTO bean, String userId, String docId) {

		savePODeliverablesUpload(bean, userId, docId,
				DBUtil.getDataSource(IlexConstants.ILEX_DS_NAME));

	}

	/**
	 * Creates the po supporting deliverable upload.
	 * 
	 * @param bean
	 *            the bean
	 * @param userId
	 *            the user id
	 * @return the string[]
	 */
	public String[] createPOSupportingDeliverableUpload(PODTO bean,
			String userId) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");

		PreparedStatement pStmt = null;
		ResultSet rs1 = null;
		String sql = "";
		int result = 0;
		int deliverableId = 0;
		String[] retVal = new String[2];

		Connection conn = ReportsDao.getStaticConnection();
		Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());

		try {
			sql = "insert into dbo.podb_purchase_order_deliverables ("
					+ "lm_po_id, podb_mn_dl_title, podb_mn_dl_type, "
					+ "podb_mn_dl_format, podb_mn_dl_desc, "
					+ "podb_mn_dl_created_by, podb_mn_dl_created_date, "
					+ "podb_mn_dl_status, podb_mn_mobile_upload_allowed) values (?,?,?,?,?,?,?,?,?)";

			try {
				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, bean.getPoId());
				pStmt.setString(2, bean.getDevTitle());
				pStmt.setString(3, bean.getDevType());
				pStmt.setString(4, bean.getDevFormat());
				pStmt.setString(5, bean.getDevDescription());
				pStmt.setString(6, userId);
				pStmt.setString(7, bean.getDevDate());
				pStmt.setInt(8, Integer.parseInt(bean.getDevStatus()));
				pStmt.setBoolean(9, bean.isDevMobile());
				result = pStmt.executeUpdate();

				if (result > 0) {
					sql = "select top 1 deliverableId = podb_mn_dl_id from podb_purchase_order_deliverables "
							+ "where lm_po_id = ? and podb_mn_dl_title = ? and podb_mn_dl_type = ? and podb_mn_dl_format = ?";

					pStmt = conn.prepareStatement(sql);
					pStmt.setString(1, bean.getPoId());
					pStmt.setString(2, bean.getDevTitle());
					pStmt.setString(3, bean.getDevType());
					pStmt.setString(4, bean.getDevFormat());
					rs1 = pStmt.executeQuery();

					if (rs1.next()) {
						deliverableId = rs1.getInt("deliverableId");
					}

					retVal[0] = deliverableId + "";
					retVal[1] = "deliverableId";
				} else {
					retVal[0] = result + "";
					retVal[1] = "Error";
				}

			} catch (SQLException e) {
				System.out
						.println("Error uploading PO Supporting Deliverable details: "
								+ e);
			}
		} catch (Exception e) {
			System.out
					.println("Error uploading PO Supporting Deliverable details: "
							+ e);
		} finally {
			DBUtil.close(rs1, pStmt);
			DBUtil.close(conn);
		}

		return retVal;
	}

	/**
	 * Discription: This method is used to check PO is saved or not.
	 * 
	 * @param poId
	 *            -- PO Id
	 * @param ds
	 *            -- Database Object
	 * @return boolean -- true or false
	 */
	public static boolean isPOSaved(String poId, DataSource ds) {

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		boolean checkPOSaved = false;
		try {
			conn = ds.getConnection();
			String sql = "select * from lm_purchase_work_order where lm_pwo_po_id = ?";
			pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, poId);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				checkPOSaved = true;
			}
		} catch (Exception e) {
			logger.error("checkPOSaved(String, DataSource)", e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return checkPOSaved;
	}

	/**
	 * Gets the partner info.
	 * 
	 * @param partnerId
	 *            the partner id
	 * @param ds
	 * @return String the partner info
	 */
	public static PartnerInfo getPartnerInfo(String partnerId, DataSource ds) {

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		PartnerInfo partnerInfo = new PartnerInfo();
		try {
			conn = ds.getConnection();
			String sql = "select * from func_cp_partner_info(?)";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, partnerId);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				partnerInfo.setPartnerAddress(rs.getString("lo_ad_address1"));
				partnerInfo.setPartnerCountry(rs.getString("lo_ad_country"));
				partnerInfo.setPartnerState(rs.getString("lo_ad_state"));
				partnerInfo.setPartnerCity(rs.getString("lo_ad_city"));
				partnerInfo.setPartnerLatitude(rs.getString("lo_ad_latitude"));
				partnerInfo
						.setPartnerLongitude(rs.getString("lo_ad_longitude"));
			}
		} catch (Exception e) {
			logger.error("getPartnerInfo(String, DataSource)" + e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return partnerInfo;
	}

	private static Boolean getPOStatusByPOId(String poId, String poType,
			DataSource ds) {
		String poStatus = "";

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			String sql = " SELECT po_status_name "
					+ " FROM lm_purchase_order  "
					+ " LEFT JOIN lm_po_status_types_master ON po_status_code = po_status "
					+ " WHERE lm_po_id = ? ";

			if (poType.equals("Material")) {
				sql = " SELECT po_status_name "
						+ " FROM lm_purchase_order a "
						+ " LEFT JOIN podb_purchase_order_deliverables b ON a.lm_po_id = b.lm_po_id "
						+ " LEFT JOIN lm_pwo_type_master  ON lm_pwo_type_id = lm_po_pwo_type_id "
						+ " LEFT JOIN lm_po_status_types_master ON po_status_code = po_status "
						+ " WHERE lm_pwo_type_desc = 'Material' "
						+ " AND podb_mn_dl_title NOT IN ('Before Work', 'After Work', 'Work Order', 'Equipment Cabinet/Rack') "
						+ " AND a.lm_po_id = ? ";
			}

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, poId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				poStatus = rs.getString("po_status_name");
				if (poStatus.equals("STATUS_ACCEPTED")) {
					return true;
				}

			}
		} catch (Exception e) {
			logger.error("markPOComplete(String, DataSource)" + e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}

		return false;
	}

	public static String getPOTypeByPOId(String poId, DataSource ds) {
		String poType = "";

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "SELECT lm_pwo_type_desc "
					+ " FROM lm_purchase_order "
					+ " LEFT JOIN lm_pwo_type_master  ON lm_pwo_type_id = lm_po_pwo_type_id "
					+ " WHERE lm_po_id = ? ";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, poId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				poType = rs.getString("lm_pwo_type_desc");
				return poType;
			}
		} catch (Exception e) {
			logger.error("markPOComplete(String, DataSource)" + e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}

		return poType;
	}

	public static String getDeliverableAcceptCount(String poId, DataSource ds) {
		String poType = "";

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "SELECT  count(*)  AS deliverableAcceptCount"
					+ " FROM lm_purchase_order a  "
					+ " LEFT JOIN podb_purchase_order_deliverables b ON a.lm_po_id = b.lm_po_id "
					+ " LEFT JOIN lm_pwo_type_master  ON lm_pwo_type_id = lm_po_pwo_type_id "
					+ " LEFT JOIN lm_po_status_types_master ON po_status_code = po_status "
					+ " WHERE lm_pwo_type_desc = 'Material' "
					+ " AND podb_mn_dl_title NOT IN ('Before Work', 'After Work', 'Work Order', 'Equipment Cabinet/Rack') "
					+ " AND podb_mn_dl_status <> 2 " + " AND a.lm_po_id =  ? ";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, poId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				poType = rs.getString("deliverableAcceptCount");
				return poType;
			}
		} catch (Exception e) {
			logger.error("getDeliverableAcceptCount(String, DataSource)" + e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}

		return poType;
	}

	/**
	 * Gets the freq used deliverable.
	 * 
	 * @param ds
	 *            the ds
	 * @return the freq used deliverable
	 */
	public static List<FreqUsedDeliverableDTO> getFreqUsedDeliverable(
			DataSource ds) {
		List<FreqUsedDeliverableDTO> freqUsedDeliverables = new ArrayList<FreqUsedDeliverableDTO>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		// list.add( new com.mind.common.LabelValue( "---Select---" , "0" ) );

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();

			rs = stmt
					.executeQuery("select del_title,del_description from podb_freq_used_deliverable");
			while (rs.next()) {
				FreqUsedDeliverableDTO freqUsedDeliverableDTO = new FreqUsedDeliverableDTO();

				freqUsedDeliverableDTO.setDeliverableDescription(rs
						.getString("del_description"));
				freqUsedDeliverableDTO.setDeliverableTitle(rs
						.getString("del_title"));
				freqUsedDeliverables.add(freqUsedDeliverableDTO);
			}
		} catch (Exception e) {
			logger.error("getFreqUsedDeliverable(DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getFreqUsedDeliverable(DataSource) - Exception caught"
						+ e);
			}
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception sql) {
				logger.warn(
						"getFreqUsedDeliverable(DataSource) - exception ignored",
						sql);
			}

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getFreqUsedDeliverable(DataSource) - exception ignored",
						sql);
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException sql) {
				logger.warn(
						"getFreqUsedDeliverable(DataSource) - exception ignored",
						sql);
			}

		}
		return freqUsedDeliverables;
	}

	/**
	 * Gets the selected core competency for po.
	 * 
	 * @param poId
	 *            the po id
	 * @param ds
	 *            the ds
	 * @return the selected core competency for po
	 */
	public static String[] getSelectedCoreCompetencyForPO(String poId,
			DataSource ds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<String> selectedCompetencyList = new ArrayList<String>();
		String[] selectedCoreCompetency;
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql = " select lm_gc_corecomp_id from lm_gc_core_competency where lm_gc_po_id="
					+ poId;
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				selectedCompetencyList.add(rs.getString("lm_gc_corecomp_id"));
			}

		} catch (Exception e) {
			logger.error("getSelectedCoreCompetencyForPO(String,DataSource)", e);

			if (logger.isDebugEnabled()) {
				logger.debug("getSelectedCoreCompetencyForPO(String,DataSource) - Error occured getSelectedCoreCompetencyForPO(String category ,DataSource ds)  "
						+ e);
			}
		}

		finally {
			DBUtil.close(rs, stmt);
			DBUtil.close(conn);
		}
		selectedCoreCompetency = selectedCompetencyList
				.toArray(new String[] {});
		return selectedCoreCompetency;
	}

	/**
	 * Mark po complete when all deliverables are in Accepted State and return
	 * true if marked po complete else false.
	 * 
	 * @param poId
	 *            the po id
	 * @param loginuserid
	 *            the loginuserid
	 * @param ds
	 *            the ds
	 * @return true, if successful
	 */
	public static boolean markPOComplete(String poId, String loginuserid,
			DataSource ds) {
		boolean jobInHoldState = getJobInHoldStatus(poId, ds);
		// String poType = getPOTypeByPOId(poId, ds);
		// int chkCount = 0;
		// if (poType.equals("Material")) {
		// chkCount = 4;
		// }

		if (!jobInHoldState) {// && getPOStatusByPOId(poId, poType, ds))

			Connection conn = null;
			PreparedStatement pStmt = null;
			ResultSet rs = null;
			try {
				conn = ds.getConnection();
				String sql = "select count(*) from podb_purchase_order_deliverables where (podb_mn_dl_status <> 2) and lm_po_id =?";
				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, poId);
				rs = pStmt.executeQuery();
				if (rs.next()) {
					int count = rs.getInt(1);
					if (count == 0) {
						PurchaseOrderDAO.changePOWOStatus(poId, loginuserid,
								POStatusTypes.STATUS_COMPLETED, ds);
						return true;
					}

				}
			} catch (Exception e) {
				logger.error("markPOComplete(String, DataSource)" + e);
			} finally {
				DBUtil.close(rs, pStmt);
				DBUtil.close(conn);
			}
		}
		return false;

	}

	/**
	 * Gets the job in hold status.
	 * 
	 * @param poId
	 *            the po id
	 * @param ds
	 *            the ds
	 * @return the job in hold status
	 */
	public static boolean getJobInHoldStatus(String poId, DataSource ds) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try {
			conn = ds.getConnection();
			String sql = "select * from lm_purchase_order join lm_job on lm_po_js_id=lm_js_id where lm_po_id=? and lm_js_prj_status='H' ";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, poId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (Exception e) {
			logger.error("getJobInHoldStatus(String, DataSource)" + e);
		} finally {
			DBUtil.close(rs, pStmt);
			DBUtil.close(conn);
		}
		return false;
	}

	public String isCalendarValueExist(String poId, DataSource ds) {

		PreparedStatement pStmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String statusOfvalue = "Insert";

		try {
			String sqlSelect = "select 1 from po_noc_calendar where po_nc_lm_po_id = ?";

			conn = ds.getConnection();
			pStmt = conn.prepareStatement(sqlSelect);
			pStmt.setString(1, poId);
			rs = pStmt.executeQuery();
			if (rs.next()) {/*
							 * this is to set the link title for NOC Calendar:
							 * if record found, DELETE link should displyed on
							 * po screen else ADD link should displayed.
							 */
				statusOfvalue = "Delete";
			}

		} catch (Exception e) {
			logger.error("isCalendarValueExist(String , DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
			DBUtil.close(rs);
		}
		return statusOfvalue;
	}

	/**
	 * Insert doc for mpo.
	 * 
	 * @param sessionMap
	 *            the session map
	 * @param bean
	 *            the bean
	 */
	public void insertNocCalendarRef(String poId, DataSource ds) {

		PreparedStatement pStmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try {
			String sql = "INSERT INTO dbo.po_noc_calendar (po_nc_lm_po_id, po_nc_date_added) "
					+ " VALUES (?, getdate())";

			String sqlSelect = "select 1 from po_noc_calendar where po_nc_lm_po_id = ?";

			conn = ds.getConnection();
			pStmt = conn.prepareStatement(sqlSelect);
			pStmt.setString(1, poId);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				sql = "UPDATE dbo.po_noc_calendar "
						+ " SET po_nc_date_added = getdate() "
						+ " WHERE po_nc_lm_po_id = ?";
			}

			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, new Integer(poId));
			pStmt.executeUpdate();

		} catch (Exception e) {
			logger.error("insertNocCalendarRef(String , DataSource)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
			DBUtil.close(rs);
		}
	}

	// delete functions
	public int deleteNocCalendarRef(String poId, DataSource ds) {
		logger.trace("Start :(Class : com.mind.docm.dao.ViewManupulationDao | Method : insertData)");
		PreparedStatement pStmt = null;
		int result = 0;
		Connection conn = ReportsDao.getStaticConnection();
		String query = "delete from dbo.po_noc_calendar where po_nc_lm_po_id=?";
		try {
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, poId);
			result = pStmt.executeUpdate();
		} catch (Exception e) {
			logger.error("deleteNocCalendarRef(String)", e);

			logger.error("deleteNocCalendarRef(string)", e);
		} finally {
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
		return result;
	}
}
