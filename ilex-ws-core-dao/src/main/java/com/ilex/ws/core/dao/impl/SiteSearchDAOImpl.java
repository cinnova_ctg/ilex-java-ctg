package com.ilex.ws.core.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.dao.SiteSearchDAO;
import com.mind.bean.pm.SiteBean;
import com.mind.bean.pm.SiteSearchBean;
import com.mind.bean.prm.JobSetUpDTO;
import com.mind.bean.prm.SiteManagementBean;
import com.mind.common.LabelValue;
import com.mind.common.dao.DynamicComboDao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PM.SiteSearchdao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.dao.PRM.SiteManagedao;
import com.mind.dao.PRM.SiteManagementdao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;

/**
 * The Class SiteSearchDAOImpl.
 */
@Repository
public class SiteSearchDAOImpl implements SiteSearchDAO {

	/** The jdbc template ilex. */
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.SiteSearchDAO#getSearchSiteDetails(int,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, int)
	 */
	@Override
	public List<SiteSearchBean> getSearchSiteDetails(int msaId,
			String siteName, String siteNo, String lo_ot_id, String siteCity,
			String siteState, String siteBrand, String siteEndCustomer,
			int latestSiteId) {
		return SiteSearchdao.getSearchsitedetails(msaId, siteName, siteNo,
				lo_ot_id, siteCity, siteState, siteBrand, siteEndCustomer,
				jdbcTemplateIlex.getDataSource(), latestSiteId);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.SiteSearchDAO#getEndCustomer(java.lang.String)
	 */
	@Override
	public String getEndCustomer(String appendixId) {
		return SiteManagementdao.getEndCustomer(appendixId,
				jdbcTemplateIlex.getDataSource());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.SiteSearchDAO#getMsaId(java.lang.String)
	 */
	@Override
	public int getMsaId(String appendixId) {
		return SiteSearchdao.getMsaid(jdbcTemplateIlex.getDataSource(),
				appendixId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.SiteSearchDAO#getCountryList(java.lang.String)
	 */
	@Override
	public List<SiteManagementBean> getCountryList(String msaId) {
		return SiteManagedao.getStateCategoryList(msaId,
				jdbcTemplateIlex.getDataSource());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.SiteSearchDAO#getSiteEndCustomerList(java.lang.String
	 * )
	 */
	@Override
	public List<LabelValue> getSiteEndCustomerList(String msaId) {

		return (List<LabelValue>) DynamicComboDao.getSiteEndCustomerList(
				jdbcTemplateIlex.getDataSource(), msaId, null);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.SiteSearchDAO#getGroupNameListSiteSearch(java.lang
	 * .String)
	 */
	@Override
	public List<LabelValue> getGroupNameListSiteSearch(String msaId) {
		return (List<LabelValue>) DynamicComboDao.getGroupNameListSiteSearch(
				msaId, jdbcTemplateIlex.getDataSource());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.SiteSearchDAO#updateSite(com.mind.bean.pm.SiteBean,
	 * java.lang.String)
	 */
	@Override
	public int updateSite(SiteBean siteform, String loginuserid) {
		return Jobdao.updateSite(siteform, loginuserid,
				jdbcTemplateIlex.getDataSource());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.SiteSearchDAO#getJobInformation(java.lang.String)
	 */
	@Override
	public JobSetUpDTO getJobInformation(String jobId) {
		JobSetUpDTO jobSetUpDTO = new JobSetUpDTO();
		DatabaseUtilityDao.getJobInformation(jobId, jobSetUpDTO,
				jdbcTemplateIlex.getDataSource());

		return jobSetUpDTO;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.SiteSearchDAO#getSiteStateList(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<LabelValue> getSiteStateList(String countryId, String msaId) {
		return SiteSearchdao.getSiteStateList(countryId, msaId, 1,
				jdbcTemplateIlex.getDataSource());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.SiteSearchDAO#getSiteDetails(java.lang.String)
	 */
	@Override
	public SiteBean getSiteDetails(String jobId) {

		SiteBean siteBean = new SiteBean();
		siteBean.setId2(jobId);

		siteBean = Jobdao.getSite(siteBean, jdbcTemplateIlex.getDataSource());

		return siteBean;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.SiteSearchDAO#addNewSite(int, int, int,
	 * java.lang.String)
	 */
	@Override
	public void addNewSite(int siteListId, int siteId, int jobId, String userId) {
		JobDashboarddao.addNewSite(siteListId, jobId, userId,
				jdbcTemplateIlex.getDataSource(), siteId);

	}

}
