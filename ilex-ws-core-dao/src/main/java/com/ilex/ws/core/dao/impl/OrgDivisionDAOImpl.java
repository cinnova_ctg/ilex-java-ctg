package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.MsaClientVO;
import com.ilex.ws.core.bean.OrgDivisionVO;
import com.ilex.ws.core.dao.OrgDivisionDAO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.MessageKeyConstant;

/**
 * The Class OrgDivisionDAOImpl.
 */
@Repository
public class OrgDivisionDAOImpl implements OrgDivisionDAO {

	/** The jdbc template ilex. */
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.OrgDivisionDAO#getDivisions(com.ilex.ws.core.bean
	 * .MsaClientVO)
	 */
	@Override
	public List<OrgDivisionVO> getDivisions(MsaClientVO argclient) {
		StringBuilder sqlQuery = new StringBuilder(
				"select lo_om_id,lo_om_division,lo_ad_address1,lo_ad_address2,");
		sqlQuery.append(" lo_ad_city,lo_ad_state,lo_ad_zip_code,lo_ad_country");
		sqlQuery.append(" from lo_organization_main");
		sqlQuery.append(" join lo_address on lo_om_ad_id1=lo_ad_id ");
		sqlQuery.append(" where lo_om_ot_id=?");
		Object[] params = new Object[] { argclient.getClientId() };
		List<OrgDivisionVO> orgDivisionVO;

		try {
			orgDivisionVO = jdbcTemplateIlex.query(sqlQuery.toString(), params,
					new DivisionMapper());
		} catch (EmptyResultDataAccessException ex) {
			throw new IlexBusinessException(
					MessageKeyConstant.CLIENT_DIVISION_NOT_FOUND);
		}

		if (orgDivisionVO == null) {
			throw new IlexBusinessException(
					MessageKeyConstant.CLIENT_DIVISION_NOT_FOUND);
		}
		return orgDivisionVO;
	}

	/**
	 * The Class DivisionMapper.
	 */
	private static final class DivisionMapper implements
			RowMapper<OrgDivisionVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public OrgDivisionVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			OrgDivisionVO orgDivisonVO = new OrgDivisionVO();
			orgDivisonVO.setDivisionId(rs.getString("lo_om_id"));
			orgDivisonVO.setAddress1(rs.getString("lo_ad_address1"));
			orgDivisonVO.setAddress2(rs.getString("lo_ad_address2"));
			orgDivisonVO.setCity(rs.getString("lo_ad_city"));
			orgDivisonVO.setCountry(rs.getString("lo_ad_country"));
			orgDivisonVO.setDivisionName(rs.getString("lo_om_division"));
			orgDivisonVO.setState(rs.getString("lo_ad_state"));
			orgDivisonVO.setZipCode(rs.getString("lo_ad_zip_code"));

			return orgDivisonVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.OrgDivisionDAO#getDivisionDetail(com.ilex.ws.core
	 * .bean.OrgDivisionVO)
	 */
	@Override
	public OrgDivisionVO getDivisionDetail(OrgDivisionVO argdivision) {
		StringBuilder sqlQuery = new StringBuilder(
				"select lo_om_id,lo_om_division,lo_ad_address1,lo_ad_address2,");
		sqlQuery.append(" lo_ad_city,lo_ad_state,lo_ad_zip_code,lo_ad_country");
		sqlQuery.append(" from lo_organization_main");
		sqlQuery.append(" join lo_address on lo_om_ad_id1=lo_ad_id ");
		sqlQuery.append(" where lo_om_id=?");
		Object[] params = new Object[] { argdivision.getDivisionId() };
		OrgDivisionVO orgDivisionVO;

		try {
			orgDivisionVO = jdbcTemplateIlex.queryForObject(
					sqlQuery.toString(), params, new DivisionMapper());
		} catch (EmptyResultDataAccessException ex) {
			throw new IlexBusinessException(
					MessageKeyConstant.CLIENT_DIVISION_NOT_FOUND);
}

		return orgDivisionVO;
	}
}
