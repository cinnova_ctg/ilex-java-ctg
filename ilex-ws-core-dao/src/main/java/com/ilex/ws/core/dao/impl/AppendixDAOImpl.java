package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.AppendixDetailVo;
import com.ilex.ws.core.bean.InvBTMClientVO;
import com.ilex.ws.core.dao.AppendixDAO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.MessageKeyConstant;

@Repository
public class AppendixDAOImpl implements AppendixDAO {
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	@Override
	public List<AppendixDetailVo> getAppendixList(InvBTMClientVO invBTMClientVO) {

		StringBuilder sqlQuery = new StringBuilder(
				"select lx_pr_title ,lx_pr_id ,lx_pr_type from lx_appendix_main");
		sqlQuery.append(" where lx_pr_mm_id =? and ");
		sqlQuery.append(" lx_pr_status = 'I'  and ");
		sqlQuery.append(" lx_pr_id not in (select in_su_pr_id from in_invoice_setup where in_su_mm_id = ?)");
		Object[] params = new Object[] { invBTMClientVO.getMmId(),
				invBTMClientVO.getMmId() };

		List<AppendixDetailVo> appendixDetailVoList = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new AppendixDetailMapper());

		if (appendixDetailVoList == null || appendixDetailVoList.size() == 0) {
			throw new IlexBusinessException(
					MessageKeyConstant.APPENDIX_NOT_FOUND);
		}
		return appendixDetailVoList;

	}

	@Override
	public AppendixDetailVo getAppendixDetail(int appendixId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select lx_pr_title ,lx_pr_id ,lx_pr_type from lx_appendix_main");
		sqlQuery.append(" where lx_pr_id =?");

		Object[] params = new Object[] { appendixId };
		AppendixDetailVo appendixDetailVo;
		try {
			appendixDetailVo = jdbcTemplateIlex.queryForObject(
					sqlQuery.toString(), params, new AppendixDetailMapper());
		} catch (EmptyResultDataAccessException ex) {
			throw new IlexBusinessException(
					MessageKeyConstant.APPENDIX_NOT_FOUND);
		}

		return appendixDetailVo;

	}

	/**
	 * The Class AppendixDetailMapper.
	 */
	private static final class AppendixDetailMapper implements
			RowMapper<AppendixDetailVo> {

		@Override
		public AppendixDetailVo mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			AppendixDetailVo appendixDetailVo = new AppendixDetailVo();
			appendixDetailVo.setAppendixId(rs.getInt("lx_pr_id"));
			appendixDetailVo.setAppendixName(rs.getString("lx_pr_title"));
			appendixDetailVo.setAppendixType(rs.getString("lx_pr_type"));
			return appendixDetailVo;
		}
	}

}
