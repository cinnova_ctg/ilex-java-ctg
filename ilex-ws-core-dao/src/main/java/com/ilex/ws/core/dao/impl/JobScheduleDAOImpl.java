package com.ilex.ws.core.dao.impl;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.dao.JobScheduleDAO;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.dao.PRM.Scheduledao;

@Repository
public class JobScheduleDAOImpl implements JobScheduleDAO {

	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	@Override
	public JobDashboardBean setScheduleDates(String jobId) {
		JobDashboardBean bean = new JobDashboardBean();
		bean.setJobId(jobId);
		com.mind.newjobdb.dao.JobScheduleDAO.setScheduleDates(bean,
				jdbcTemplateIlex.getDataSource());

		return bean;

	}

	@Override
	public int[] addScheduleForPRM(String lx_se_type_id, String act_start,
			String act_end, String lx_se_type, String lx_se_created_by,
			String lx_se_start, String lx_se_end, int count) {
		return Scheduledao.addScheduleForPRM(lx_se_type_id, act_start, act_end,
				lx_se_type, lx_se_created_by, lx_se_start, lx_se_end, count,
				jdbcTemplateIlex.getDataSource());
	}

	@Override
	public void setReschedule(String jobId, String resType, String oldSchDate,
			String newSchDate, String resReason, String user) {
		Scheduledao.setReschedule(jobId, resType, oldSchDate, newSchDate,
				resReason, user, jdbcTemplateIlex.getDataSource());

	}
}
