package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.InvBTMClientVO;
import com.ilex.ws.core.bean.InvBTMContactVO;
import com.ilex.ws.core.dao.InvBTMContactDAO;

@Repository
public class InvBTMContactDAOImpl implements InvBTMContactDAO {
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.InvBTMContactDAO#insertBTMContactDetails(com.ilex
	 * .ws.core.bean.InvBTMContactVO)
	 */
	@Override
	public void insertBTMContactDetails(InvBTMContactVO invBTMContactVO) {
		StringBuilder sqlQuery = new StringBuilder(
				"insert into in_bill_to_contact_master (in_ct_pc_id,in_ct_mbt_id,in_ct_status,");
		sqlQuery.append("in_ct_first_name,in_ct_last_name,");
		sqlQuery.append("in_ct_phone,in_ct_cell,in_ct_fax,in_ct_email");
		sqlQuery.append(") values (?,?,?,?,?,?,?,?,?)");

		Object[] params = new Object[] { invBTMContactVO.getPocId(),
				invBTMContactVO.getMbtId(), invBTMContactVO.isStatus() ? 1 : 0,
				invBTMContactVO.getFirstName(), invBTMContactVO.getLastName(),
				invBTMContactVO.getPhone(), invBTMContactVO.getCell(),
				invBTMContactVO.getFax(), invBTMContactVO.getEmail() };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.InvBTMContactDAO#getBTMContactDetails(com.ilex.ws
	 * .core.bean.InvBTMClientVO)
	 */
	@Override
	public List<InvBTMContactVO> getBTMContactDetails(
			final InvBTMClientVO invBTMClientVO) {
		StringBuilder sqlQuery = new StringBuilder(
				"select * from in_bill_to_contact_master where in_ct_mbt_id=? ");
		Object[] params = new Object[] { invBTMClientVO.getMbtClientId() };

		List<InvBTMContactVO> invBTMContactVOList = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new ContactDetailMapper());

		return invBTMContactVOList;

	}

	/**
	 * The Class ContactDetailMapper.
	 */
	private static final class ContactDetailMapper implements
			RowMapper<InvBTMContactVO> {

		@Override
		public InvBTMContactVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvBTMContactVO invBTMContactVO = new InvBTMContactVO();
			invBTMContactVO.setCell(rs.getString("in_ct_cell"));
			invBTMContactVO.setEmail(rs.getString("in_ct_email"));
			invBTMContactVO.setFax(rs.getString("in_ct_fax"));
			invBTMContactVO.setFirstName(rs.getString("in_ct_first_name"));
			invBTMContactVO.setInvContactId(rs.getInt("in_ct_id"));
			invBTMContactVO.setLastName(rs.getString("in_ct_last_name"));
			invBTMContactVO.setMbtId(rs.getInt("in_ct_mbt_id"));
			invBTMContactVO.setPhone(rs.getString("in_ct_phone"));
			invBTMContactVO.setPocId(rs.getInt("in_ct_pc_id"));
			invBTMContactVO.setStatus(rs.getBoolean("in_ct_status"));
			return invBTMContactVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.InvBTMContactDAO#updateBTMContactDetails(com.ilex
	 * .ws.core.bean.InvBTMContactVO)
	 */
	@Override
	public void updateBTMContactDetails(InvBTMContactVO invBTMContactVO) {
		StringBuilder sqlQuery = new StringBuilder(
				" update in_bill_to_contact_master set in_ct_first_name=?,in_ct_last_name=?,in_ct_phone=?,");
		sqlQuery.append(" in_ct_cell=?,in_ct_fax=?,in_ct_email=?,in_ct_status=?");
		sqlQuery.append(" where in_ct_id=?");

		Object[] params = new Object[] { invBTMContactVO.getFirstName(),
				invBTMContactVO.getLastName(), invBTMContactVO.getPhone(),
				invBTMContactVO.getCell(), invBTMContactVO.getFax(),
				invBTMContactVO.getEmail(), invBTMContactVO.isStatus() ? 1 : 0,
				invBTMContactVO.getInvContactId() };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

}
