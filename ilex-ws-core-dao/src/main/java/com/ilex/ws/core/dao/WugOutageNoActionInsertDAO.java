package com.ilex.ws.core.dao;

public interface WugOutageNoActionInsertDAO {

	String getWUGId(String nActiveMonitorStateChangeID, String wugInstance);

	void insertUpdateWugOutageNoAction(String nActiveMonitorStateChangeID,
			String wugInstance, String wugNaDvId, String userId);

	void upadateMonitoringSummary();

}
