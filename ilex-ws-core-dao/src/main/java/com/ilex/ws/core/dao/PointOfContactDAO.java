package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.OrgDivisionVO;
import com.ilex.ws.core.bean.PointOfContactVO;

/**
 * The Interface PointOfContactDAO.
 */
public interface PointOfContactDAO {

	/**
	 * Gets the contact list.
	 * 
	 * @param division
	 *            the division
	 * @return the contact list
	 */
	public List<PointOfContactVO> getContactList(OrgDivisionVO division);

	/**
	 * Gets the contact detail.
	 * 
	 * @param pointOfContactDTO
	 *            the point of contact dto
	 * @return the contact detail
	 */
	public PointOfContactVO getContactDetail(PointOfContactVO pointOfContactVO);

}
