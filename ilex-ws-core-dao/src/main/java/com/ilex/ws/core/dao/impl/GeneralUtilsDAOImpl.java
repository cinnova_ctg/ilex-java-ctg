package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.CountryVO;
import com.ilex.ws.core.bean.StateVO;
import com.ilex.ws.core.dao.GeneralUtilsDAO;

@Repository
public class GeneralUtilsDAOImpl implements GeneralUtilsDAO {
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	@Override
	public List<CountryVO> getCountrylist() {
		StringBuilder sqlQuery = new StringBuilder(
				"select distinct cp_country_id,cp_country_name from cp_state where cp_country_id ");
		sqlQuery.append(" is not null and cp_country_id<>'CA' order by cp_country_name");
		List<CountryVO> countryList = jdbcTemplateIlex.query(
				sqlQuery.toString(), new CountryListMapper());
		return countryList;

	}

	private static final class CountryListMapper implements
			RowMapper<CountryVO> {

		@Override
		public CountryVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			CountryVO country = new CountryVO();
			country.setCountryName(rs.getString("cp_country_name"));
			country.setCountryId(rs.getString("cp_country_id"));
			return country;
		}
	}

	@Override
	public List<StateVO> getStateList() {
		StringBuilder sqlQuery = new StringBuilder(
				"select cp_state_name , cp_state_id from cp_state");
		List<StateVO> stateList = jdbcTemplateIlex.query(sqlQuery.toString(),
				new StateListMapper());
		return stateList;

	}

	private static final class StateListMapper implements RowMapper<StateVO> {

		@Override
		public StateVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			StateVO stateVO = new StateVO();
			stateVO.setStateId(rs.getString("cp_state_id"));
			stateVO.setStateName(rs.getString("cp_state_name"));
			return stateVO;
		}
	}
}
