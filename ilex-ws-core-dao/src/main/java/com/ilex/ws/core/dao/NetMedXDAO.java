package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.NetMedXConsolidatorRespVO;
import com.ilex.ws.core.bean.NetMedXIntegrationResponseVO;
import com.ilex.ws.core.bean.NetMedXIntegrationVO;
import com.mind.bean.docm.Document;

public interface NetMedXDAO {

	/**
	 * Gets the latitude longitude.
	 * 
	 * @param address
	 *            the address
	 * @param city
	 *            the city
	 * @param state
	 *            the state
	 * @param country
	 *            the country
	 * @param zipcode
	 *            the zipcode
	 * @return the latitude longitude
	 */
	String[] getLatitudeLongitude(String address, String city, String state,
			String country, String zipcode);

	/**
	 * Netmedx user auth.
	 * 
	 * @param userName
	 *            the user name
	 * @param pass
	 *            the pass
	 * @param prId
	 *            the pr id
	 * @return the net med x integration response vo
	 */
	public NetMedXIntegrationResponseVO netmedxUserAuth(String userName,
			String pass, int prId);

	/**
	 * Creates the job for NetMedX.
	 * 
	 * @param netMedXIntegrationVO
	 *            the net med x integration vo
	 * @return the net med x integration response vo
	 */
	NetMedXIntegrationResponseVO createJobForNetMedX(
			NetMedXIntegrationVO netMedXIntegrationVO);

	/**
	 * Upload file.
	 * 
	 * @param doc
	 *            the doc
	 * @return the int
	 */
	int uploadFile(Document doc);

	/**
	 * Insert doc entry to po document.
	 * 
	 * @param poId
	 *            the po id
	 * @param docId
	 *            the doc id
	 * @param userName
	 *            the user name
	 */
	void insertDocEntryToPoDocumentNetMedX(int docId, String userName, int jobId);

	/**
	 * Gets the job owner name.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the job owner name
	 */
	String getJobOwnerName(int jobId);

	/**
	 * Gets the net med x job intergration.
	 * 
	 * @param projectId
	 *            the project id
	 * @param whereClause
	 *            the where clause
	 * @param orderBy
	 *            the order by
	 * @param limit
	 *            the limit
	 * @return the net med x job intergration
	 */
	public List<NetMedXConsolidatorRespVO> getNetMedXJobIntergration(
			String projectId, String whereClause, String orderBy, String limit);

	/**
	 * Gets the job count dao.
	 * 
	 * @param projectId
	 *            the project id
	 * @return the job count dao
	 */
	public String getJobCountDAO(String projectId, String whereClause,
			String orderBy);

}
