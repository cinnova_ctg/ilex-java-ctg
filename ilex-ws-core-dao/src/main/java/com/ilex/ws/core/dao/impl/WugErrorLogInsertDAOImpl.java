package com.ilex.ws.core.dao.impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.dao.WugErrorLogInsertDAO;

@Repository
public class WugErrorLogInsertDAOImpl implements WugErrorLogInsertDAO {
	/** The jdbc template ilex. */
	@Resource(name = "jdbcTemplateIlex")
	private JdbcTemplate jdbcTemplateIlex;

	/** The Constant logger. */
	public static final Logger logger = Logger
			.getLogger(WugErrorLogInsertDAOImpl.class);

	@Override
	public void insertErrorLog(String code, String data,
			String deleteDeltaMonths) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("insert into wug_error_log( ");
		sqlQuery.append(" wug_el_date, wug_el_code, wug_el_data, wug_el_delete_date ) ");
		sqlQuery.append(" values(getdate(),?,?,dateadd(mm, ?, getdate()))");

		Object[] params = new Object[] { code, data,
				Integer.parseInt(deleteDeltaMonths) };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

}
