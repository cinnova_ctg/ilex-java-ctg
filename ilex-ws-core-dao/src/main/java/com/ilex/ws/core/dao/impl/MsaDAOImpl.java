package com.ilex.ws.core.dao.impl;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.dao.MsaDAO;
import com.mind.dao.PM.Appendixdao;

@Repository
public class MsaDAOImpl implements MsaDAO {

	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	@Override
	public String getMsaname(String msaId) {
		return Appendixdao.getMsaname(jdbcTemplateIlex.getDataSource(), msaId);

	}
}
