package com.ilex.ws.core.dao;

import java.util.List;
import java.util.Map;

import com.ilex.ws.core.bean.NewDispatchVO;
import com.mind.bean.newjobdb.ResourceLevel;
import com.mind.bean.prm.NewWebTicketBean;
import com.mind.bean.prm.ViewTicketSummaryBean;
import com.mind.common.LabelValue;

/**
 * The Interface NewWebTicketDAO.
 */
public interface NewWebTicketDAO {

	/**
	 * Gets the web ticket list.
	 * 
	 * @return the web ticket list
	 */
	List<NewWebTicketBean> getWebTicketList();

	/**
	 * Gets the web ticket summary.
	 * 
	 * @param ticketNumber
	 *            the ticket number
	 * @return the web ticket summary
	 */
	ViewTicketSummaryBean getWebTicketSummary(String ticketNumber);

	/**
	 * Gets the requestors.
	 * 
	 * @param msaId
	 *            the msa id
	 * @return the requestors
	 */
	List<LabelValue> getRequestors(long msaId);

	/**
	 * Gets the problem categories.
	 * 
	 * @return the problem categories
	 */
	List<LabelValue> getProblemCategories();

	/**
	 * Gets the resources.
	 * 
	 * @param map
	 *            the map
	 * @param appendixId
	 *            the appendix id
	 * @param criticalityId
	 *            the criticality id
	 * @param isPPS
	 *            the is pps
	 * @return the resources
	 */
	List<ResourceLevel> getResources(Map<String, Object> map, long appendixId,
			String criticalityId, boolean isPPS);

	/**
	 * Gets the criticalities.
	 * 
	 * @param msaId
	 *            the msa id
	 * @return the criticalities
	 */
	List<LabelValue> getCriticalities(long msaId);

	/**
	 * Gets the request types.
	 * 
	 * @return the request types
	 */
	List<LabelValue> getRequestTypes();

	/**
	 * Gets the ticket info.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the ticket info
	 */
	NewDispatchVO getTicketInfo(long ticketId);

	/**
	 * Update ticket.
	 * 
	 * @param newDispatchVO
	 *            the new dispatch vo
	 * @param userLoginId
	 *            the user login id
	 */
	void updateTicket(NewDispatchVO newDispatchVO, Long userLoginId);

	/**
	 * Update ticket job owner.
	 * 
	 * @param newDispatchVO
	 *            the new dispatch vo
	 * @param userLoginId
	 *            the user login id
	 */
	void updateTicketJobOwner(NewDispatchVO newDispatchVO, Long userLoginId);

	/**
	 * Gets the rules.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @return the rules
	 */
	String getRules(Long appendixId);

	/**
	 * Gets the states.
	 * 
	 * @return the states
	 */
	List<LabelValue> getStates();

}
