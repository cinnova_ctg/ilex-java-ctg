package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.InvBTMClientVO;
import com.ilex.ws.core.bean.InvSearchVO;
import com.ilex.ws.core.dao.util.Page;

/**
 * The Interface InvBTMClientDAO.
 */
public interface InvBTMClientDAO {

	/**
	 * Insert bill to master client details.
	 * 
	 * @param invBTMClientVO
	 *            the invbtmclientvo
	 * @return the int
	 */
	int insertBTMClientDetails(InvBTMClientVO invBTMClientVO);

	/**
	 * Gets the client list.
	 * 
	 * @return the client list
	 */
	public List<InvBTMClientVO> getClientList();

	/**
	 * Gets the client details.
	 * 
	 * @param clientId
	 *            the client id
	 * @return the client details
	 */
	public InvBTMClientVO getClientDetails(String clientId);

	/**
	 * Update btm client details.
	 * 
	 * @param invBTMClientVO
	 *            the inv btm client vo
	 */
	void updateBTMClientDetails(InvBTMClientVO invBTMClientVO);

	/**
	 * Gets the client list.
	 * 
	 * @param searchKey
	 *            the search key
	 * @param pageNo
	 *            the page no
	 * @param pageSize
	 *            the page size
	 * @return the client list
	 */
	Page<InvBTMClientVO> getClientList(String searchKey, final int pageNo,
			final int pageSize);

	/**
	 * Update btm client contact status.
	 * 
	 * @param mbtClientId
	 *            the mbt client id
	 * @param activeContact
	 *            the active contact
	 */
	void updateBTMClientContactStatus(int mbtClientId, boolean activeContact);

	/**
	 * Gets the client list for appendix invoice setup.
	 * 
	 * @param searchKey
	 *            the search key
	 * @param pageNo
	 *            the page no
	 * @param pageSize
	 *            the page size
	 * @return the client list for appendix invoice setup
	 */
	Page<InvSearchVO> getClientListForAppendixInvSetup(String searchKey,
			int pageNo, int pageSize);

}
