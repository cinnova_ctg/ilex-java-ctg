package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.InvCountryListVO;

public interface InvMasterCountryDAO {

	/**
	 * Gets the country list.
	 * 
	 * @return the country list
	 */
	List<InvCountryListVO> getCountryList();

}
