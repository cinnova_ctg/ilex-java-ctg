package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.AppendixDetailVo;
import com.ilex.ws.core.bean.InvBTMClientVO;

public interface AppendixDAO {

	public List<AppendixDetailVo> getAppendixList(InvBTMClientVO invBTMClientVO);

	AppendixDetailVo getAppendixDetail(int appendixId);

}
