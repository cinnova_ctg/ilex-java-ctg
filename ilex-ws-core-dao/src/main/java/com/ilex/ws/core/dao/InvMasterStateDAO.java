package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.InvStateListVO;

public interface InvMasterStateDAO {

	/**
	 * Gets the state list.
	 * 
	 * @return the state list
	 */
	List<InvStateListVO> getStateList();

}
