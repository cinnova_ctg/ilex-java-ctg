package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.InvMasterLineItemDetailsVO;
import com.ilex.ws.core.dao.InvMasterLineItemDetailsDAO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.MessageKeyConstant;

@Repository
public class InvMasterLineItemDetailsDAOImpl implements
		InvMasterLineItemDetailsDAO {
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	@Override
	public List<InvMasterLineItemDetailsVO> getLineItemDetails(
			String projectType) {
		StringBuilder sqlQuery = new StringBuilder(
				"select * from in_master_line_item_description where in_mi_project_type = ? and in_mi_active = 1 order by in_mi_name");
		Object[] params = new Object[] { projectType };

		List<InvMasterLineItemDetailsVO> invMasterLineItemDetailsVOList = jdbcTemplateIlex
				.query(sqlQuery.toString(), params, new LineItemDetailMapper());

		if (invMasterLineItemDetailsVOList == null
				|| invMasterLineItemDetailsVOList.size() == 0) {
			throw new IlexBusinessException(
					MessageKeyConstant.LINE_ITEM_NOT_FOUND);
		}
		return invMasterLineItemDetailsVOList;
	}

	/**
	 * The Class LineItemDetailMapper.
	 */
	private static final class LineItemDetailMapper implements
			RowMapper<InvMasterLineItemDetailsVO> {

		@Override
		public InvMasterLineItemDetailsVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvMasterLineItemDetailsVO invMasterLineItemDetailsVO = new InvMasterLineItemDetailsVO();
			invMasterLineItemDetailsVO.setProjectType(rs
					.getString("in_mi_project_type"));
			invMasterLineItemDetailsVO.setLineItemActive(rs
					.getBoolean("in_mi_active"));
			invMasterLineItemDetailsVO.setLineItemDescription(rs
					.getString("in_mi_description"));
			invMasterLineItemDetailsVO.setLineItemName(rs
					.getString("in_mi_name"));
			return invMasterLineItemDetailsVO;
		}
	}

}
