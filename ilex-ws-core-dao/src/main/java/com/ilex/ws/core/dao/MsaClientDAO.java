package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.MsaClientVO;

/**
 * The Interface MsaClientDAO.
 */
public interface MsaClientDAO {

	/**
	 * Gets the client list.
	 * 
	 * @return the clientList
	 */
	public List<MsaClientVO> getClientList();

	/**
	 * Gets the client detail.
	 * 
	 * @param clientDetail
	 *            the client detail
	 * @return the client detail
	 */
	public MsaClientVO getClientDetail(MsaClientVO clientDetail);

	MsaClientVO getClientDetailById(String msaId);
}
