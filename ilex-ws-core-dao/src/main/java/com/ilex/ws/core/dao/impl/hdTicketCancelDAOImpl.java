package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.WugOutInProcessVO;
import com.ilex.ws.core.dao.HDTicketCancelDAO;

@Repository
public class hdTicketCancelDAOImpl implements HDTicketCancelDAO {

	/** The jdbc template ilex. */
	@Resource(name = "jdbcTemplateIlex")
	private JdbcTemplate jdbcTemplateIlex;

	/**
	 * The Constant logger.
	 */
	public static final Logger logger = Logger
			.getLogger(hdTicketCancelDAOImpl.class);

	@Override
	public void insertInWugNoAction(String ticketId, String userId) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery = new StringBuilder("  insert into wug_outage_no_action ");
		sqlQuery.append("  select wug_ip_dv_id, wug_ip_dStartTime, wug_ip_nActiveMonitorStateChangeID, 1,getdate(),? ");
		sqlQuery.append(" from wug_outages_incidents_in_process");
		sqlQuery.append(" where wug_ip_tc_id=?");
		Object[] params = new Object[] { userId, ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	@Override
	public WugOutInProcessVO getDataFromInProcess(String ticketId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(" select wug_ip_nActiveMonitorStateChangeID, wug_ip_instance,wug_ip_dv_id, wug_ip_dStartTime, wug_ip_current_status, wug_ip_outage_duration, ");
		sqlQuery.append(" wug_ip_event_count from wug_outages_incidents_in_process");
		sqlQuery.append(" where wug_ip_tc_id = ?");

		Object[] param = new Object[] { ticketId };
		WugOutInProcessVO wugOutInProcessVO = new WugOutInProcessVO();
		wugOutInProcessVO = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), param, new WugOutInProcessMapper());

		WugOutInProcessVO wugOutInProcessHistory = new WugOutInProcessVO();
		sqlQuery = new StringBuilder(
				" select  max(wug_ip_nActiveMonitorStateChangeID_new) as 'wug_ip_nActiveMonitorStateChangeID_new', ");
		sqlQuery.append(" max(wug_ih_event_time) as 'wug_ih_event_time'");
		sqlQuery.append("  from wug_outages_incidents_in_process_history ");
		sqlQuery.append("  where wug_ip_nActiveMonitorStateChangeID = ? ");
		sqlQuery.append("  and wug_ih_instance = ? ");

		param = new Object[] {
				wugOutInProcessVO.getnActiveMonitorStateChangeID(),
				wugOutInProcessVO.getWugInstance() };

		wugOutInProcessHistory = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), param, new WugOutInProcessHistoryMapper());

		wugOutInProcessVO
				.setnActiveMonitorStateChangeIdNew(wugOutInProcessHistory
						.getnActiveMonitorStateChangeIdNew());
		wugOutInProcessVO.setIhEventTime(wugOutInProcessHistory
				.getIhEventTime());

		return wugOutInProcessVO;

	}

	private static final class WugOutInProcessMapper implements
			RowMapper<WugOutInProcessVO> {

		public WugOutInProcessVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			WugOutInProcessVO wugOutInProcessVO = new WugOutInProcessVO();
			wugOutInProcessVO.setDvId(rs.getString("wug_ip_dv_id"));
			wugOutInProcessVO.setdStartTime(rs.getString("wug_ip_dStartTime"));
			wugOutInProcessVO.setCurrentStatus(rs
					.getString("wug_ip_current_status"));
			wugOutInProcessVO.setOutageDuration(rs
					.getString("wug_ip_outage_duration"));
			wugOutInProcessVO.setEventCount(rs.getString("wug_ip_event_count"));
			wugOutInProcessVO.setnActiveMonitorStateChangeID(rs
					.getString("wug_ip_nActiveMonitorStateChangeID"));
			wugOutInProcessVO.setWugInstance(rs.getString("wug_ip_instance"));
			return wugOutInProcessVO;
		}
	}

	private static final class WugOutInProcessHistoryMapper implements
			RowMapper<WugOutInProcessVO> {

		public WugOutInProcessVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			WugOutInProcessVO wugOutInProcessHistory = new WugOutInProcessVO();
			wugOutInProcessHistory.setnActiveMonitorStateChangeIdNew(rs
					.getString("wug_ip_nActiveMonitorStateChangeID_new"));
			wugOutInProcessHistory.setIhEventTime(rs
					.getString("wug_ih_event_time"));
			return wugOutInProcessHistory;
		}
	}

	@Override
	public int getEventTestCount(String nActiveMonitorStateChangeID,
			String wugInstance) {
		StringBuilder sqlQuery = new StringBuilder(
				" select count(*) from wug_outages_incidents_closed ");
		sqlQuery.append("  where wug_cp_nActiveMonitorStateChangeID_1 = ? ");
		sqlQuery.append(" and isnull(wug_cp_instance,1) = ? ");
		Object[] param = new Object[] { nActiveMonitorStateChangeID,
				wugInstance };
		int eventTest = jdbcTemplateIlex
				.queryForInt(sqlQuery.toString(), param);

		return eventTest;
	}

	@Override
	public void insertWugOutClosedRecord(WugOutInProcessVO wugOutInProcessVO) {
		StringBuilder sqlQuery = new StringBuilder(
				" insert into wug_outages_incidents_closed( ");
		sqlQuery.append(" wug_cp_nActiveMonitorStateChangeID_1,wug_cp_nActiveMonitorStateChangeID_2, wug_cp_dv_id, ");
		sqlQuery.append(" wug_cp_js_id,wug_cp_ticket_reference,wug_cp_issue_owner, ");
		sqlQuery.append(" wug_cp_root_couse,wug_cp_corrective_action,wug_ip_effort_minutes, ");
		sqlQuery.append(" wug_cp_outage_duration,wug_cp_escalation_level,wug_cp_current_status,");
		sqlQuery.append(" wug_cp_initiation_point,wug_cp_complete_point,wug_cp_event_count,");
		sqlQuery.append(" wug_cp_instance,wug_cp_backup_status,wug_cp_tc_id) ");
		sqlQuery.append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		sqlQuery.append("");
		Object[] param = new Object[] {
				wugOutInProcessVO.getnActiveMonitorStateChangeID(),
				wugOutInProcessVO.getnActiveMonitorStateChangeIdNew(),
				wugOutInProcessVO.getDvId(), null,
				wugOutInProcessVO.getTicketReference(),
				wugOutInProcessVO.getIssueOwner(),
				wugOutInProcessVO.getRootCouse(),
				wugOutInProcessVO.getCorrectiveAction(),
				wugOutInProcessVO.getEffortMinutes(),
				wugOutInProcessVO.getOutageDuration(),
				wugOutInProcessVO.getEscalationLevel(),
				wugOutInProcessVO.getCurrentStatus(),
				wugOutInProcessVO.getdStartTime(),
				wugOutInProcessVO.getIhEventTime(),
				wugOutInProcessVO.getEventCount(),
				wugOutInProcessVO.getWugInstance(),
				wugOutInProcessVO.getWugBackupStatus(),
				wugOutInProcessVO.getTicketId() };

		jdbcTemplateIlex.update(sqlQuery.toString(), param);

	}

	@Override
	public void insertWugOutClosedHistoryRecord(
			String nActiveMonitorStateChangeID, String wugInstance) {
		StringBuilder sqlQuery = new StringBuilder(
				" insert into wug_outages_incidents_closed_history( ");
		sqlQuery.append(" wug_cp_nActiveMonitorStateChangeID_1,wug_ch_nActiveMonitorStateChangeID_new,wug_ch_transition_from, ");
		sqlQuery.append(" wug_ch_transition_to,wug_ch_event_time,wug_ch_down_duration, ");
		sqlQuery.append(" wug_ch_instance,wug_ch_from_sStateName,wug_ch_to_sStateName) ");
		sqlQuery.append(" select wug_ip_nActiveMonitorStateChangeID, ");
		sqlQuery.append(" wug_ip_nActiveMonitorStateChangeID_new,wug_ih_transition_from,wug_ih_transition_to, ");
		sqlQuery.append(" wug_ih_event_time,wug_ih_down_duration,wug_ih_instance, ");
		sqlQuery.append(" wug_ih_from_sStateName,wug_ih_to_sStateName ");
		sqlQuery.append(" from wug_outages_incidents_in_process_history ");
		sqlQuery.append(" where wug_ip_nActiveMonitorStateChangeID = ? ");
		sqlQuery.append(" and wug_ih_instance = ? ");
		Object[] param = new Object[] { nActiveMonitorStateChangeID,
				wugInstance };

		jdbcTemplateIlex.update(sqlQuery.toString(), param);

	}

	@Override
	public void deleteWugOutagesInProcessHistory(
			String nActiveMonitorStateChangeID, String wugInstance) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(" delete from wug_outages_incidents_in_process_history ");
		sqlQuery.append("  where wug_ip_nActiveMonitorStateChangeID = ? and wug_ih_instance = ?");
		Object[] param = new Object[] { nActiveMonitorStateChangeID,
				wugInstance };
		jdbcTemplateIlex.update(sqlQuery.toString(), param);

	}

	@Override
	public void deleteWugOutagesInProcess(String nActiveMonitorStateChangeID,
			String wugInstance) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(" delete from wug_outages_incidents_in_process ");
		sqlQuery.append("  where wug_ip_nActiveMonitorStateChangeID = ? and wug_ip_instance = ?");
		Object[] param = new Object[] { nActiveMonitorStateChangeID,
				wugInstance };
		jdbcTemplateIlex.update(sqlQuery.toString(), param);

	}

	@Override
	public void deleteFromHDTables(String ticketId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(" delete from lm_help_desk_next_action_history ");
		sqlQuery.append("  where lm_na_tc_id = ?");
		Object[] param = new Object[] { ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), param);

		sqlQuery = new StringBuilder();
		sqlQuery.append(" delete from lm_help_desk_change_history ");
		sqlQuery.append("  where lm_ch_tc_id = ?");
		param = new Object[] { ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), param);

		sqlQuery = new StringBuilder();
		sqlQuery.append(" delete from lm_help_desk_effort ");
		sqlQuery.append("  where lm_ef_tc_id = ?");
		param = new Object[] { ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), param);

		sqlQuery = new StringBuilder();
		sqlQuery.append(" delete from lm_help_desk_work_notes ");
		sqlQuery.append("  where lm_wn_tc_id = ?");
		param = new Object[] { ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), param);

		sqlQuery = new StringBuilder();
		sqlQuery.append(" delete from lm_help_desk_ticket_details ");
		sqlQuery.append("  where lm_hd_tc_id = ?");
		param = new Object[] { ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), param);

		Long jobid = getJobIdByTicket(ticketId);
		if (jobid > 0) {
			logger.error(" --------- ------------------ This is msg that job "
					+ jobid
					+ " is going to delete. ---------------------------");
			sqlQuery = new StringBuilder();
			sqlQuery.append("delete lm_job where lm_js_id= ?");
			param = new Object[] { jobid };
			jdbcTemplateIlex.update(sqlQuery.toString(), param);
		}

		sqlQuery = new StringBuilder();
		sqlQuery.append(" delete from lm_ticket ");
		sqlQuery.append("  where lm_tc_id = ?");
		param = new Object[] { ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), param);

	}

	private Long getJobIdByTicket(String ticketNumber) {
		String sqlQuery = "SELECT Top 1 isnull(lm_tc_js_id, 0) lm_tc_js_id FROM lm_ticket WHERE lm_tc_id = ? order by lm_tc_js_id desc";

		Object[] params = new Object[] { ticketNumber };
		Long jobId = jdbcTemplateIlex.queryForLong(sqlQuery, params);

		return jobId;
	}
}
