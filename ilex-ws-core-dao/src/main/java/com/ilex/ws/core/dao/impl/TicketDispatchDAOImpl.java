package com.ilex.ws.core.dao.impl;

import javax.annotation.Resource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.dao.TicketDispatchDAO;

/**
 * The Class TicketDispatchDAOImpl.
 */
@Repository
public class TicketDispatchDAOImpl implements TicketDispatchDAO {

	/** The jdbc template ilex. */
	@Resource(name = "jdbcTemplateIlex")
	private JdbcTemplate jdbcTemplateIlex;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.TicketDispatchDAO#getCustomerName(java.lang.Long)
	 */
	@Override
	public String getCustomerName(Long appendixId) {

		StringBuilder customerNameSelect = new StringBuilder(
				"select lo_ot_name from lo_organization_top");
		customerNameSelect.append(" join lp_msa_main on ");
		customerNameSelect.append(" lp_mm_ot_id =lo_ot_id");
		customerNameSelect
				.append(" join lx_appendix_main on lp_mm_id=lx_pr_mm_id where lx_pr_id=?");
		String customerName;

		Object[] appendixIdParam = new Object[] { appendixId };

		try {

			customerName = jdbcTemplateIlex.queryForObject(
					customerNameSelect.toString(), appendixIdParam,
					String.class);

		} catch (EmptyResultDataAccessException ex) {
			return "";
		}
		return customerName;
	}

}
