package com.ilex.ws.core.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.InvBTMClientVO;
import com.ilex.ws.core.bean.InvSearchVO;
import com.ilex.ws.core.dao.InvBTMClientDAO;
import com.ilex.ws.core.dao.util.Page;
import com.ilex.ws.core.dao.util.PaginationHelper;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.MessageKeyConstant;

@Repository
public class InvBTMClientDAOImpl implements InvBTMClientDAO {
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.InvBTMClientDAO#getClientList()
	 */
	@Override
	public List<InvBTMClientVO> getClientList() {
		StringBuilder sqlQuery = new StringBuilder(
				"select distinct in_mbt_id,in_mbt_company,in_mbt_city,in_mbt_state,in_mbt_country from in_bill_to_master");
		sqlQuery.append(" join lx_appendix_main on  in_mbt_mm_id = lx_pr_mm_id");
		sqlQuery.append(" where lx_pr_status = 'I'");
		sqlQuery.append(" order by in_mbt_company ");
		List<InvBTMClientVO> invBTMClientVOList = jdbcTemplateIlex.query(
				sqlQuery.toString(), new ClientListMapper());

		if (invBTMClientVOList == null || invBTMClientVOList.size() == 0) {
			throw new IlexBusinessException(
					MessageKeyConstant.CLIENT_DETAIL_NOT_FOUND);
		}
		return invBTMClientVOList;
	}

	/**
	 * The Class ClientListMapper.
	 */
	private static final class ClientListMapper implements
			ParameterizedRowMapper<InvBTMClientVO> {

		@Override
		public InvBTMClientVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvBTMClientVO invBTMClientVO = new InvBTMClientVO();
			invBTMClientVO.setMbtClientId(Integer.valueOf(rs
					.getString("in_mbt_id")));
			invBTMClientVO.setClientName(rs.getString("in_mbt_company"));
			invBTMClientVO.setCity(rs.getString("in_mbt_city"));
			invBTMClientVO.setState(rs.getString("in_mbt_state"));
			invBTMClientVO.setCountry(rs.getString("in_mbt_country"));
			return invBTMClientVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.InvBTMClientDAO#getClientDetails(java.lang.String)
	 */
	@Override
	public InvBTMClientVO getClientDetails(String clientId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select * from in_bill_to_master ");
		sqlQuery.append(" where in_mbt_id = ?");
		Object[] params = new Object[] { clientId };
		InvBTMClientVO invBTMClientVO;

		try {
			invBTMClientVO = jdbcTemplateIlex.queryForObject(
					sqlQuery.toString(), params, new ClientDetailMapper());
		} catch (EmptyResultDataAccessException ex) {
			throw new IlexBusinessException(
					MessageKeyConstant.CLIENT_DETAIL_NOT_FOUND);
		}
		return invBTMClientVO;
	}

	/**
	 * The Class ClientDetailMapper.
	 */
	private static final class ClientDetailMapper implements
			RowMapper<InvBTMClientVO> {

		@Override
		public InvBTMClientVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvBTMClientVO invBTMClientVO = new InvBTMClientVO();
			invBTMClientVO.setMbtClientId(Integer.valueOf(rs
					.getString("in_mbt_id")));
			invBTMClientVO.setClientName(rs.getString("in_mbt_company"));
			invBTMClientVO.setStatus(rs.getBoolean("in_mbt_status"));
			invBTMClientVO.setAddress1(rs.getString("in_mbt_address1"));
			invBTMClientVO.setAddress2(rs.getString("in_mbt_address2"));
			invBTMClientVO.setCity(rs.getString("in_mbt_city"));
			invBTMClientVO.setCountry(rs.getString("in_mbt_country"));
			invBTMClientVO.setDeliveryMethod(rs
					.getInt("in_mbt_delivery_method"));
			invBTMClientVO.setContactExist(rs.getBoolean("in_mbt_er_contact"));
			invBTMClientVO
					.setDeliveryExist(rs.getBoolean("in_mbt_er_delivery"));
			invBTMClientVO.setMmId(rs.getInt("in_mbt_mm_id"));
			invBTMClientVO.setIntegrated(rs.getBoolean("in_mbt_integrated"));
			invBTMClientVO.setState(rs.getString("in_mbt_state"));
			invBTMClientVO.setZipCode(rs.getString("in_mbt_zip_code"));
			invBTMClientVO.setClientId((Integer.valueOf(rs
					.getString("in_mbt_om_id"))));
			return invBTMClientVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.InvBTMClientDAO#insertBTMClientDetails(com.ilex.
	 * ws.core.bean.InvBTMClientVO)
	 */
	@Override
	public int insertBTMClientDetails(final InvBTMClientVO invBTMClientVO) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		final StringBuilder sqlQuery = new StringBuilder(
				"insert into in_bill_to_master (in_mbt_status,in_mbt_mm_id,");
		sqlQuery.append("in_mbt_om_id,in_mbt_reference_date,in_mbt_integrated,");
		sqlQuery.append("in_mbt_er_delivery,in_mbt_er_contact,");
		sqlQuery.append("in_mbt_company,in_mbt_delivery_method,");
		sqlQuery.append("in_mbt_address1,in_mbt_address2,in_mbt_city,in_mbt_state,");
		sqlQuery.append("in_mbt_zip_code,in_mbt_country");
		sqlQuery.append(") values (?,?,?,getDate(),?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						sqlQuery.toString(), new String[] { "id" });
				ps.setBoolean(1, invBTMClientVO.isStatus());
				ps.setInt(2, invBTMClientVO.getMmId());
				ps.setInt(3, invBTMClientVO.getClientId());
				ps.setBoolean(4, invBTMClientVO.isIntegrated());
				ps.setBoolean(5, invBTMClientVO.isDeliveryExist());
				ps.setBoolean(6, invBTMClientVO.isContactExist());
				ps.setString(7, invBTMClientVO.getClientName());
				ps.setInt(8, invBTMClientVO.getDeliveryMethod());
				ps.setString(9, invBTMClientVO.getAddress1());
				ps.setString(10, invBTMClientVO.getAddress2());
				ps.setString(11, invBTMClientVO.getCity());
				ps.setString(12, invBTMClientVO.getState());
				ps.setString(13, invBTMClientVO.getZipCode());
				ps.setString(14, invBTMClientVO.getCountry());

				return ps;
			}
		};

		jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);

		int insertedClientId = keyHolder.getKey().intValue();
		return insertedClientId;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.InvBTMClientDAO#updateBTMClientDetails(com.ilex.
	 * ws.core.bean.InvBTMClientVO)
	 */
	@Override
	public void updateBTMClientDetails(InvBTMClientVO invBTMClientVO) {
		StringBuilder sqlQuery = new StringBuilder(
				" update in_bill_to_master set in_mbt_company=?,in_mbt_status=?,in_mbt_delivery_method=?,");
		sqlQuery.append(" in_mbt_address1=?,in_mbt_address2=?,in_mbt_city=?,in_mbt_state=?,in_mbt_zip_code=?");
		sqlQuery.append(" ,in_mbt_country=?,in_mbt_reference_date=getDate(),in_mbt_er_delivery=?");
		sqlQuery.append(" where in_mbt_id=?");

		Object[] params = new Object[] { invBTMClientVO.getClientName(),
				invBTMClientVO.isStatus() ? 1 : 0,
				invBTMClientVO.getDeliveryMethod(),
				invBTMClientVO.getAddress1(), invBTMClientVO.getAddress2(),
				invBTMClientVO.getCity(), invBTMClientVO.getState(),
				invBTMClientVO.getZipCode(), invBTMClientVO.getCountry(),
				invBTMClientVO.isDeliveryExist() ? 1 : 0,
				invBTMClientVO.getMbtClientId() };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.InvBTMClientDAO#getClientList(java.lang.String,
	 * int, int)
	 */
	@Override
	public Page<InvBTMClientVO> getClientList(String searchKey,
			final int pageNo, final int pageSize) {
		PaginationHelper<InvBTMClientVO> ph = new PaginationHelper<InvBTMClientVO>();
		StringBuilder sqlQueryForCount = new StringBuilder(
				"select count(*) from in_bill_to_master ");
		sqlQueryForCount.append("where in_mbt_company like '%" + searchKey
				+ "%'");
		StringBuilder sqlQuery = new StringBuilder(
				"select * from in_bill_to_master ");
		sqlQuery.append("where in_mbt_company like '%" + searchKey + "%' ");
		sqlQuery.append("order by in_mbt_company asc");
		return ph.fetchPage(jdbcTemplateIlex, sqlQueryForCount.toString(),
				sqlQuery.toString(), new Object[] {}, pageNo, pageSize,
				new ClientListMapper());
	}

	@Override
	public Page<InvSearchVO> getClientListForAppendixInvSetup(
			final String searchKey, final int pageNo, final int pageSize) {
		PaginationHelper<InvSearchVO> ph = new PaginationHelper<InvSearchVO>();
		// Escaping single quote
		String search = searchKey.replaceAll("'", "''");

		StringBuilder sqlQueryForCount = new StringBuilder(
				"select count(*)  from in_bill_to_address join in_invoice_setup on in_bt_id= in_su_bt_id ");
		sqlQueryForCount
				.append(" join lx_appendix_main on in_su_pr_id=lx_pr_id");
		sqlQueryForCount.append(" where in_bt_company like '%" + search + "%'");
		StringBuilder sqlQuery = new StringBuilder(
				"select in_bt_id,lx_pr_id,in_bt_company,lx_pr_title,in_su_id from in_bill_to_address join in_invoice_setup on in_bt_id= in_su_bt_id ");
		sqlQuery.append(" join lx_appendix_main on in_su_pr_id=lx_pr_id");
		sqlQuery.append(" where in_bt_company like '%" + search + "%'");
		sqlQuery.append(" order by  in_bt_company ,lx_pr_title");
		return ph.fetchPage(jdbcTemplateIlex, sqlQueryForCount.toString(),
				sqlQuery.toString(), new Object[] {}, pageNo, pageSize,
				new AppendixInvSetupClientListMapper());
	}

	/**
	 * The Class AppendixInvSetupClientListMapper.
	 */
	private static final class AppendixInvSetupClientListMapper implements
			ParameterizedRowMapper<InvSearchVO> {
		@Override
		public InvSearchVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			InvSearchVO invSearchVO = new InvSearchVO();
			invSearchVO.setAppendixId(rs.getInt("lx_pr_id"));
			invSearchVO.setAppendixName(rs.getString("lx_pr_title"));
			invSearchVO.setClientName(rs.getString("in_bt_company"));
			invSearchVO.setInvBillToAddressId(rs.getInt("in_bt_id"));
			invSearchVO.setInvSetupId(rs.getInt("in_su_id"));

			return invSearchVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.InvBTMClientDAO#updateBTMClientContactStatus(int,
	 * boolean)
	 */
	@Override
	public void updateBTMClientContactStatus(int mbtClientId,
			boolean activeContact) {
		StringBuilder sqlQuery = new StringBuilder(
				" update in_bill_to_master set in_mbt_er_contact=?");
		sqlQuery.append(" where in_mbt_id=?");
		Object[] params = new Object[] { activeContact ? 1 : 0, mbtClientId };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);
	}
}
