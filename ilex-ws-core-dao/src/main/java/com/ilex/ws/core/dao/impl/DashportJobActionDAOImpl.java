package com.ilex.ws.core.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.InvoiceJobDetailVO;
import com.ilex.ws.core.bean.JobInformationVO;
import com.ilex.ws.core.dao.DashportJobActionDAO;
import com.ilex.ws.core.dto.LabelValue;
import com.ilex.ws.core.exception.IlexSystemException;
import com.ilex.ws.core.util.DateUtils;
import com.mind.bean.prm.ViewInstallationNotesBean;
import com.mind.bean.pvs.PartnerDetailBean;
import com.mind.bean.pvs.Partner_SearchBean;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.CustomerInfoBean;
import com.mind.dao.PRM.CustomerInformationdao;
import com.mind.dao.PRM.ViewInstallationNotesdao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class DashportJobActionDAOImpl.
 */
@Repository
public class DashportJobActionDAOImpl implements DashportJobActionDAO {

	/** The jdbc template ilex. */
	@Resource(name = "jdbcTemplateIlex")
	private JdbcTemplate jdbcTemplateIlex;
	/** The Constant logger. */
	public static final Logger logger = Logger
			.getLogger(DashportJobActionDAOImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#getInvoiceActResourceList(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public List<InvoiceJobDetailVO> getInvoiceActResourceList(String type,
			String jobId) {
		String sqlQuery;
		List<InvoiceJobDetailVO> invoiceJobDetailVOs = new ArrayList<InvoiceJobDetailVO>();
		if (type != null && type.equals("job")) {
			sqlQuery = "SELECT lm_at_title AS NAME,lm_activity_type_description AS TYPE,lm_at_quantity AS quantity,lx_ce_type,lx_ce_unit_price AS price "
					+ " FROM dbo.lm_job "
					+ "INNER JOIN lm_activity ON lm_js_id = lm_at_js_id"
					+ " INNER JOIN  lx_cost_element ON lx_ce_used_in = lm_at_id"
					+ " inner join lm_activity_type on lm_at_type = lm_activity_type_code"
					+ " WHERE lm_js_id ="
					+ jobId
					+ " AND lx_ce_type in ('IA','A') and  lm_activity_type_description <> 'Overhead'";
			invoiceJobDetailVOs = jdbcTemplateIlex.query(sqlQuery.toString(),
					new InvoiceActResourceListJobExtractor());
		} else {
			sqlQuery = "SELECT iv_rp_name AS NAME ,lm_rs_type AS TYPE ,lm_rs_quantity AS quantity,lx_ce_type,"
					+ " lx_ce_total_price AS price,lm_rs_iv_cns_part_number,lm_rs_transit "
					+ " FROM dbo.lm_ticket"
					+ " INNER JOIN lm_activity ON lm_tc_js_id = lm_at_js_id "
					+ " INNER JOIN lm_resource ON lm_rs_at_id = lm_at_id  "
					+ " INNER JOIN iv_resource_pool ON iv_rp_id = lm_rs_rp_id "
					+ " INNER JOIN  lx_cost_element ON lx_ce_used_in = lm_rs_id "
					+ " WHERE lm_tc_js_id ="
					+ jobId
					+ " AND lx_ce_type in ('Il','L','IF','F','IM','M','IT','T')";
			invoiceJobDetailVOs = jdbcTemplateIlex.query(sqlQuery.toString(),
					new InvoiceActResourceListTicketExtractor());
		}
		return invoiceJobDetailVOs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#setJobTicketInfo(com.ilex.ws
	 * .core.bean.InvoiceJobDetailVO, java.lang.String)
	 */
	@Override
	public void setJobTicketInfo(InvoiceJobDetailVO invoiceJobDetailVO,
			String jobId) {

		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			conn = jdbcTemplateIlex.getDataSource().getConnection();
			cstmt = conn.prepareCall("{?=call inv_job_ticket_info( ? )}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, Integer.parseInt(jobId));
			rs = cstmt.executeQuery();
			while (rs.next()) {
				invoiceJobDetailVO.setSiteName(rs.getString("lm_si_name"));
				invoiceJobDetailVO.setSiteNumber(rs.getString("lm_si_number"));
				invoiceJobDetailVO
						.setSiteAddress(rs.getString("lm_si_address"));
				invoiceJobDetailVO.setSiteCity(rs.getString("lm_si_city"));
				invoiceJobDetailVO.setSiteState(rs.getString("lm_si_state"));
				invoiceJobDetailVO
						.setSiteCountry(rs.getString("lm_si_country"));
				invoiceJobDetailVO.setSiteZipCode(rs
						.getString("lm_si_zip_code"));
				invoiceJobDetailVO.setCustref(rs
						.getString("lm_js_customer_reference"));
				invoiceJobDetailVO.setTypeJob(rs.getString("job_type"));
				invoiceJobDetailVO.setRequestorName(rs
						.getString("lm_tc_requestor_name"));
				invoiceJobDetailVO.setRequestType(rs
						.getString("lm_request_type"));
				invoiceJobDetailVO.setProblemdesc(rs
						.getString("lm_tc_problem_desc"));
				invoiceJobDetailVO.setTimeOnSite(rs.getString("time_onsite"));
				invoiceJobDetailVO.setBillingInfo(rs.getString("billing_info"));
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new IlexSystemException(e.getMessage(), e);
		} finally {
			DBUtil.close(rs, cstmt);
			DBUtil.close(conn);

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#getScheduledDetail(java.lang
	 * .String)
	 */
	@Override
	public List<InvoiceJobDetailVO> getScheduledDetail(String jobId) {
		String sqlQuery = "SELECT 	CASE When  replace(convert(varchar(10),isnull(lx_se_planned_start,''),101),'01/01/1900','') = '' then '0:00'"
				+ " else CONVERT(VARCHAR(10), DATEDIFF(mi,lx_se_start,lx_se_end)/60)+':'+ CASE WHEN DATEDIFF(mi,lx_se_start,lx_se_end)%60 < 10 "
				+ " THEN '0'+ CONVERT(VARCHAR(10),DATEDIFF(mi,lx_se_start,lx_se_end)%60)"
				+ " ELSE CONVERT(VARCHAR(10),DATEDIFF(mi,lx_se_start,lx_se_end)%60) END "
				+ " end AS onsite_time, lx_se_start = replace(Convert(Varchar(25),isnull(convert(datetime ,lx_se_start),''),100),'Jan  1 1900 12:00AM',''),"
				+ " lx_se_end = replace(Convert(Varchar(25),isnull(convert(datetime ,lx_se_end),''),100),'Jan  1 1900 12:00AM',''), lx_se_sequence FROM lx_schedule_element "
				+ " WHERE lx_se_type_id = "
				+ jobId
				+ " and lx_se_type IN ('J','IJ','AJ','CJ')";

		List<InvoiceJobDetailVO> rsList = jdbcTemplateIlex.query(sqlQuery,
				new ScheduledDetailExtractor());
		return rsList;
	}

	/**
	 * The Class ScheduledDetailExtractor.
	 */
	public class ScheduledDetailExtractor implements
			ResultSetExtractor<List<InvoiceJobDetailVO>> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.ResultSetExtractor#extractData(java
		 * .sql.ResultSet)
		 */
		public List<InvoiceJobDetailVO> extractData(ResultSet rs)
				throws SQLException, DataAccessException {
			List<InvoiceJobDetailVO> rsList = new ArrayList<InvoiceJobDetailVO>();
			while (rs.next()) {
				InvoiceJobDetailVO invoiceJobDetailVO = new InvoiceJobDetailVO();
				if ((rs.getString("lx_se_start") == null || rs.getString(
						"lx_se_start").equals(""))
						|| (rs.getString("lx_se_end") == null || rs.getString(
								"lx_se_end").equals(""))) {
					invoiceJobDetailVO.setOnSite("0:00");
				} else {
					invoiceJobDetailVO.setOnSite(rs.getString("onsite_time"));
				}

				if (rs.getString("lx_se_start") != null
						&& !rs.getString("lx_se_start").equals("")) {
					invoiceJobDetailVO.setStartDate(DateUtils.getDateFormat(
							rs.getString("lx_se_start"),
							"MM/dd/yyyy hh:mm aaa", "MMM dd yyyy hh:mmaaa"));
				} else {
					invoiceJobDetailVO.setStartDate("");
				}

				if (rs.getString("lx_se_end") != null
						&& !rs.getString("lx_se_end").equals("")) {
					invoiceJobDetailVO.setStartEnd(DateUtils.getDateFormat(
							rs.getString("lx_se_end"), "MM/dd/yyyy hh:mm aaa",
							"MMM dd yyyy hh:mmaaa"));
				} else {
					invoiceJobDetailVO.setStartEnd("");
				}

				invoiceJobDetailVO
						.setSequenceNo(rs.getString("lx_se_sequence"));
				rsList.add(invoiceJobDetailVO);
			}
			return rsList;
		}
	}

	/**
	 * Gets the invoice activity resource list of job.
	 * 
	 * @param type
	 *            the type
	 * @param jobId
	 *            the job id
	 * @return the invoice act resource list job
	 */
	public List<InvoiceJobDetailVO> getInvoiceActResourceListJOb(String type,
			String jobId) {
		String sqlQuery = "SELECT lm_at_title AS NAME,lm_activity_type_description AS TYPE,lm_at_quantity AS quantity,lx_ce_type,lx_ce_unit_price AS price "
				+ " FROM dbo.lm_job "
				+ "INNER JOIN lm_activity ON lm_js_id = lm_at_js_id"
				+ " INNER JOIN  lx_cost_element ON lx_ce_used_in = lm_at_id"
				+ " inner join lm_activity_type on lm_at_type = lm_activity_type_code"
				+ " WHERE lm_js_id ="
				+ jobId
				+ " AND lx_ce_type in ('IA','A') and  lm_activity_type_description <> 'Overhead'";
		List<InvoiceJobDetailVO> invoiceJobDetailVOs = jdbcTemplateIlex.query(
				sqlQuery.toString(), new InvoiceActResourceListJobExtractor());
		return invoiceJobDetailVOs;
	}

	/**
	 * The Class InvoiceActResourceListJobExtractor.
	 */
	public class InvoiceActResourceListJobExtractor implements
			ResultSetExtractor<List<InvoiceJobDetailVO>> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.ResultSetExtractor#extractData(java
		 * .sql.ResultSet)
		 */
		public List<InvoiceJobDetailVO> extractData(ResultSet rs)
				throws SQLException, DataAccessException {
			List<InvoiceJobDetailVO> invoiceJobDetailVOs = new ArrayList<InvoiceJobDetailVO>();
			String resType = "";
			while (rs.next()) {
				InvoiceJobDetailVO invoiceJobDetailVO = new InvoiceJobDetailVO();
				invoiceJobDetailVO.setRsName(rs.getString("NAME"));
				if (rs.getString("TYPE") != null
						&& (rs.getString("TYPE").equals("L") || rs.getString(
								"TYPE").equals("IL"))) {
					resType = "L";
					invoiceJobDetailVO.setRsType(resType);
				} else if (rs.getString("TYPE") != null
						&& (rs.getString("TYPE").equals("M") || rs.getString(
								"TYPE").equals("IM"))) {
					resType = "M";
					invoiceJobDetailVO.setRsType(resType);
				} else if (rs.getString("TYPE") != null
						&& (rs.getString("TYPE").equals("F") || rs.getString(
								"TYPE").equals("IF"))) {
					resType = "F";
					invoiceJobDetailVO.setRsType(resType);
				} else if (rs.getString("TYPE") != null
						&& (rs.getString("TYPE").equals("T")
								|| rs.getString("TYPE").equals("IT") || rs
								.getString("TYPE").contains("IT"))) {
					resType = "T";
					invoiceJobDetailVO.setRsType(resType);
				} else {
					invoiceJobDetailVO.setRsType(rs.getString("TYPE"));
				}
				invoiceJobDetailVO.setRsquantity(rs.getString("quantity"));
				invoiceJobDetailVO.setRsPrice(rs.getString("price"));
				invoiceJobDetailVOs.add(invoiceJobDetailVO);
			}
			return invoiceJobDetailVOs;
		}
	}

	/**
	 * Gets the invoice activity resource list of ticket.
	 * 
	 * @param type
	 *            the type
	 * @param jobId
	 *            the job id
	 * @return the invoice act resource list ticket
	 */
	public List<InvoiceJobDetailVO> getInvoiceActResourceListTicket(
			String type, String jobId) {
		String sqlQuery = "SELECT iv_rp_name AS NAME ,lm_rs_type AS TYPE ,lm_rs_quantity AS quantity,lx_ce_type,"
				+ " lx_ce_total_price AS price,lm_rs_iv_cns_part_number,lm_rs_transit "
				+ " FROM dbo.lm_ticket"
				+ " INNER JOIN lm_activity ON lm_tc_js_id = lm_at_js_id "
				+ " INNER JOIN lm_resource ON lm_rs_at_id = lm_at_id  "
				+ " INNER JOIN iv_resource_pool ON iv_rp_id = lm_rs_rp_id "
				+ " INNER JOIN  lx_cost_element ON lx_ce_used_in = lm_rs_id "
				+ " WHERE lm_tc_js_id ="
				+ jobId
				+ " AND lx_ce_type in ('Il','L','IF','F','IM','M','IT','T')";
		List<InvoiceJobDetailVO> invoiceJobDetailVOs = jdbcTemplateIlex.query(
				sqlQuery.toString(),
				new InvoiceActResourceListTicketExtractor());
		return invoiceJobDetailVOs;
	}

	/**
	 * The Class InvoiceActResourceListTicketExtractor.
	 */
	public class InvoiceActResourceListTicketExtractor implements
			ResultSetExtractor<List<InvoiceJobDetailVO>> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.ResultSetExtractor#extractData(java
		 * .sql.ResultSet)
		 */
		public List<InvoiceJobDetailVO> extractData(ResultSet rs)
				throws SQLException, DataAccessException {
			List<InvoiceJobDetailVO> invoiceJobDetailVOs = new ArrayList<InvoiceJobDetailVO>();
			String resType = "";
			while (rs.next()) {
				InvoiceJobDetailVO invoiceJobDetailVO = new InvoiceJobDetailVO();
				invoiceJobDetailVO.setRsName(rs.getString("NAME"));
				if (rs.getString("TYPE") != null
						&& (rs.getString("TYPE").equals("L") || rs.getString(
								"TYPE").equals("IL"))) {
					resType = "L";
					invoiceJobDetailVO.setRsType(resType);
				} else if (rs.getString("TYPE") != null
						&& (rs.getString("TYPE").equals("M") || rs.getString(
								"TYPE").equals("IM"))) {
					resType = "M";
					invoiceJobDetailVO.setRsType(resType);
				} else if (rs.getString("TYPE") != null
						&& (rs.getString("TYPE").equals("F") || rs.getString(
								"TYPE").equals("IF"))) {
					resType = "F";
					invoiceJobDetailVO.setRsType(resType);
				} else if (rs.getString("TYPE") != null
						&& (rs.getString("TYPE").equals("T")
								|| rs.getString("TYPE").equals("IT") || rs
								.getString("TYPE").contains("IT"))) {
					resType = "T";
					invoiceJobDetailVO.setRsType(resType);
				} else {
					invoiceJobDetailVO.setRsType(rs.getString("TYPE"));
				}
				invoiceJobDetailVO.setCnsPartNo(rs
						.getString("lm_rs_iv_cns_part_number"));
				invoiceJobDetailVO.setRsTransit(rs.getString("lm_rs_transit"));
				if (rs.getString("lm_rs_transit") != null
						&& rs.getString("lm_rs_transit").equals("Y")) {
					invoiceJobDetailVO.setRsName(rs.getString("NAME")
							+ " - In Transit");
					invoiceJobDetailVO.setRsType("T");
				}
				invoiceJobDetailVO.setRsquantity(rs.getString("quantity"));
				invoiceJobDetailVO.setRsPrice(rs.getString("price"));
				invoiceJobDetailVOs.add(invoiceJobDetailVO);
			}
			return invoiceJobDetailVOs;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#addinstallnotes(java.lang.String
	 * , java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public int addinstallnotes(String lm_in_js_id, String lm_in_notes,
			String lm_user, String lm_date, String preScheduleId,
			String newScheduleId, String scheduleDateType, String typeId) {

		Connection conn = null;
		CallableStatement cstmt = null;
		int retval = 0;
		try {
			conn = jdbcTemplateIlex.getDataSource().getConnection();
			if (StringUtils.isEmpty(typeId)) {
				typeId = "0";
			}
			cstmt = conn
					.prepareCall("{?=call lm_install_notes_manage_01(?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, lm_in_js_id);
			cstmt.setString(3, lm_in_notes);
			cstmt.setString(4, lm_user);
			cstmt.setString(5, lm_date);
			cstmt.setString(6, preScheduleId);
			cstmt.setString(7, newScheduleId);
			cstmt.setString(8, scheduleDateType);
			cstmt.setString(9, typeId);

			cstmt.execute();
			retval = cstmt.getInt(1);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new IlexSystemException(e.getMessage(), e);
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);

		}
		return retval;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#updateJobCategoryPriority(com
	 * .ilex.ws.core.bean.JobInformationVO, java.lang.String)
	 */
	@Override
	public void updateJobCategoryPriority(JobInformationVO jobInformationVO,
			String userId) {

		StringBuilder sqlQuery = new StringBuilder(
				"update  lm_job set  lm_js_priority=?,lm_js_sequence_number=?, lm_js_change_date=getDate(),lm_js_changed_by=?");
		sqlQuery.append(" where lm_js_id=?");

		Object[] params = new Object[] {
				jobInformationVO.getJobPriority().trim(),
				jobInformationVO.getJobCategory().trim(), userId,
				jobInformationVO.getJobId() };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#getJobCategoryPriority(java
	 * .lang.String)
	 */
	@Override
	public JobInformationVO getJobCategoryPriority(String jobId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select lm_js_sequence_number ,lm_js_priority,lm_js_id from lm_job");
		sqlQuery.append(" where lm_js_id =?");
		Object[] params = new Object[] { jobId };

		JobInformationVO jobInformationVO = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), params, new JobInfoMapper());

		return jobInformationVO;
	}

	/**
	 * The Class JobInfoMapper.
	 */
	private static final class JobInfoMapper implements
			RowMapper<JobInformationVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		@Override
		public JobInformationVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			JobInformationVO jobInformationVO = new JobInformationVO();
			jobInformationVO.setJobCategory(rs
					.getString("lm_js_sequence_number"));
			jobInformationVO.setJobPriority(rs.getString("lm_js_priority"));
			jobInformationVO.setJobId(rs.getString("lm_js_id"));

			return jobInformationVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#getJobChangedTime(java.lang
	 * .String)
	 */
	@Override
	public String getJobChangedTime(String jobId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select lm_js_change_date from lm_job");
		sqlQuery.append(" where lm_js_id =?");
		Object[] params = new Object[] { jobId };

		String jobChangedTime = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), params, String.class);

		return jobChangedTime;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#getAppendixCategories(java.
	 * lang.String)
	 */
	@Override
	public List<String> getAppendixCategories(String jobId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select distinct lm_js_sequence_number from lm_job where (lm_js_sequence_number is not null and lm_js_sequence_number!='') and  lm_js_pr_id =(select lx_pr_id from lx_appendix_main join lm_job on lm_js_pr_id=lx_pr_id");
		sqlQuery.append("  where lm_js_id=?)");
		Object[] params = new Object[] { jobId };

		List<String> appendixCategories = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new RowMapper<String>() {

					public String mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						return rs.getString("lm_js_sequence_number");
					}
				});

		return appendixCategories;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#getAppendixPriorities(java.
	 * lang.String)
	 */
	@Override
	public List<String> getAppendixPriorities(String jobId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select distinct lm_js_priority from lm_job where (lm_js_priority is not null and lm_js_priority!='') and  lm_js_pr_id =(select lx_pr_id from lx_appendix_main join lm_job on lm_js_pr_id=lx_pr_id");
		sqlQuery.append("  where lm_js_id=?)");
		Object[] params = new Object[] { jobId };

		List<String> appendixPriorities = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new RowMapper<String>() {

					public String mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						return rs.getString("lm_js_priority");
					}
				});

		return appendixPriorities;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#getJOList(java.lang.String)
	 */
	@Override
	public List<LabelValue> getJOList(String appendixId) {
		String sqlQuery = " select * from func_lm_job_team_list(?,'Job Owner') ";
		Object[] params = new Object[] { appendixId };
		List<LabelValue> getJOList = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new JOListMapper());
		return getJOList;
	}

	/**
	 * The Class JOListMapper.
	 */
	private static final class JOListMapper implements RowMapper<LabelValue> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public LabelValue mapRow(ResultSet rs, int rowNum) throws SQLException {
			LabelValue labelValue = new LabelValue();
			labelValue.setLabel(rs.getString("poc_name"));
			labelValue.setValue(rs.getString("poc_id"));
			return labelValue;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.DashportJobActionDAO#updateJobOwner(int, int)
	 */
	@Override
	public void updateJobOwner(int jobId, int newJobOwnerId) {
		Connection conn = null;
		CallableStatement cstmt = null;
		try {
			conn = jdbcTemplateIlex.getDataSource().getConnection();
			cstmt = conn.prepareCall("{call lm_changeOwner(?,?)}");
			cstmt.setInt(1, jobId);
			cstmt.setInt(2, newJobOwnerId);
			cstmt.execute();
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new IlexSystemException(e.getMessage(), e);
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#getCustomerFieldData(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public List<CustomerInfoBean> getCustomerFieldData(String jobId,
			String appendixId) {
		List<CustomerInfoBean> requiredDataList = CustomerInformationdao
				.getCustomerRequiredData(appendixId, jobId,
						jdbcTemplateIlex.getDataSource());
		return requiredDataList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#checkForSnapon(java.lang.String
	 * )
	 */
	@Override
	public boolean checkForSnapon(String appendixId) {
		boolean isSnapon = Appendixdao.checkForSnapon(appendixId,
				jdbcTemplateIlex.getDataSource());
		return isSnapon;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#updateCustomerFiledInfo(java
	 * .lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public int updateCustomerFiledInfo(String jobId,
			String appendixCustomerIds, String custmereValues,
			String loginuserid) {
		int statusvalue = CustomerInformationdao.addCustInfo(jobId,
				appendixCustomerIds.trim(), custmereValues.trim(), loginuserid,
				jdbcTemplateIlex.getDataSource());
		return statusvalue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#getInstallationNotesList(java
	 * .lang.String)
	 */
	@Override
	public List<ViewInstallationNotesBean> getInstallationNotesList(String jobId) {
		List<ViewInstallationNotesBean> installationNotesList = ViewInstallationNotesdao
				.getNotesdetaillist(jobId, jdbcTemplateIlex.getDataSource());
		return installationNotesList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#getpartnersSearchList(com.mind
	 * .bean.pvs.Partner_SearchBean, java.lang.String, java.lang.String)
	 */
	@Override
	public List<Partner_SearchBean> getpartnersSearchList(
			Partner_SearchBean partner_SearchBean, String sortqueryclause,
			String zipcode) {
		String badgeStatus = "";// Added to get badge status from
								// mySql DB. Using none this time.
		List<Partner_SearchBean> partnerSearchList = Pvsdao
				.getSearchpartnerdetails(partner_SearchBean.getName(),
						partner_SearchBean.getSearchTerm(),
						partner_SearchBean.getCheckedPartnerName(),
						partner_SearchBean.getMcsaVersion(), zipcode,
						partner_SearchBean.getRadius(),
						partner_SearchBean.getMainOffice(),
						partner_SearchBean.getFieldOffice(),
						partner_SearchBean.getTechniciansZip(),
						partner_SearchBean.getTechnicians(),
						partner_SearchBean.getCertifications(),
						partner_SearchBean.getCoreCompetency(),
						partner_SearchBean.getCbox1(),
						partner_SearchBean.getCbox2(),
						partner_SearchBean.getPartnerRating(),
						partner_SearchBean.getSecurityCertiftn(),
						sortqueryclause, partner_SearchBean.getAssign(),
						jdbcTemplateIlex.getDataSource(),
						partner_SearchBean.getSelectedCountryName(),
						partner_SearchBean.getSelectedCityName(),
						partner_SearchBean.getBid(),
						partner_SearchBean.getRecommendedArr(),
						partner_SearchBean.getRestrictedArr(),
						partner_SearchBean.getToolKitArr(), badgeStatus, "N");

		return partnerSearchList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DashportJobActionDAO#getPartnerDetails(com.mind.
	 * bean.pvs.PartnerDetailBean)
	 */
	@Override
	public String getPartnerDetails(PartnerDetailBean partnerDetailBean,
			String url) {
		String checkPartner = Pvsdao.getSearchPartnerDetail(partnerDetailBean,
				url, jdbcTemplateIlex.getDataSource());
		return checkPartner;
	}
}