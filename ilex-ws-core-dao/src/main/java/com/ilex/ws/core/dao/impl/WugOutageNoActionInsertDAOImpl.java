package com.ilex.ws.core.dao.impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.dao.WugOutageNoActionInsertDAO;

@Repository
public class WugOutageNoActionInsertDAOImpl implements
		WugOutageNoActionInsertDAO {
	public static final Logger logger = Logger
			.getLogger(WugOutageNoActionInsertDAOImpl.class);

	/** The jdbc template ilex. */
	@Resource(name = "jdbcTemplateIlex")
	private JdbcTemplate jdbcTemplateIlex;

	@Override
	public String getWUGId(String nActiveMonitorStateChangeID,
			String wugInstance) {
		StringBuilder sqlQuery = new StringBuilder("");
		sqlQuery.append(" select isnull(wug_ip_dv_id, 0) from wug_outages_incidents_in_process ");
		sqlQuery.append(" where wug_ip_nActiveMonitorStateChangeID = ? and isnull(wug_ip_instance, 1) = ? ");
		Object[] params = new Object[] { nActiveMonitorStateChangeID,
				wugInstance };
		String wugId = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
				params, String.class);
		return wugId;
	}

	@Override
	public void insertUpdateWugOutageNoAction(
			String nActiveMonitorStateChangeID, String wugInstance,
			String wugNaDvIdArg, String userId) {
		// boolean wugNaDvIdExist = false;
		StringBuilder sqlQuery = new StringBuilder();
		// sqlQuery.append(" select top 1 wug_na_dv_id from wug_outage_no_action where wug_na_dv_id = ?");
		// Object[] params = new Object[] { wugNaDvIdArg };
		// try {
		// jdbcTemplateIlex.queryForInt(sqlQuery.toString(), params);
		// wugNaDvIdExist = true;
		// } catch (EmptyResultDataAccessException e) {
		// wugNaDvIdExist = false;
		// }
		// if (wugNaDvIdExist) {
		/*
		 * update existing record Update based on the wug_na_dv_id field.
		 * Updating time will chain this together to catch any that try to slip
		 * through the 36 window.
		 */
		// sqlQuery = new StringBuilder("  update wug_outage_no_action ");
		// sqlQuery.append("  set wug_na_dStartTime = wug_ip_dStartTime, ");
		// sqlQuery.append(" wug_na_nActiveMonitorStateChangeLogID = wug_ip_nActiveMonitorStateChangeID,");
		// sqlQuery.append(" wug_na_count = wug_na_count+1,");
		// sqlQuery.append(" wug_na_dt =getdate(),");
		// sqlQuery.append(" wug_na_pc_id =? ");
		// sqlQuery.append(" from wug_outages_incidents_in_process");
		// sqlQuery.append(" where wug_na_dv_id = ?");
		// params = new Object[] { userId, wugNaDvIdArg };
		// jdbcTemplateIlex.update(sqlQuery.toString(), params);
		//
		// } else {
		sqlQuery = new StringBuilder("  insert into wug_outage_no_action ");
		sqlQuery.append("  select wug_ip_dv_id, wug_ip_dStartTime, wug_ip_nActiveMonitorStateChangeID, 1,getdate(),? ");
		sqlQuery.append(" from wug_outages_incidents_in_process");
		sqlQuery.append(" where wug_ip_nActiveMonitorStateChangeID = ? ");
		sqlQuery.append(" and isnull(wug_ip_instance, 1) = ?");
		Object[] params = new Object[] { userId, nActiveMonitorStateChangeID,
				wugInstance };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);
		// }

	}

	@Override
	public void upadateMonitoringSummary() {
		StringBuilder sqlQuery = new StringBuilder(
				"  update wug_monitoring_summary ");
		sqlQuery.append("  set wug_ms_cnt_watch_list = wug_ms_cnt_watch_list+1 ");
		sqlQuery.append(" where wug_ms_dv_type = 1");
		jdbcTemplateIlex.update(sqlQuery.toString());

	}

}
