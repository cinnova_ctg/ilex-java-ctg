package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.MsaClientVO;
import com.ilex.ws.core.bean.OrgDivisionVO;

/**
 * The Interface OrgDivisionDAO.
 */
public interface OrgDivisionDAO {

	/**
	 * Gets the divisions.
	 * 
	 * @param client
	 *            the client
	 * @return the divisions
	 */
	public List<OrgDivisionVO> getDivisions(MsaClientVO client);

	/**
	 * Gets the division detail.
	 * 
	 * @param division
	 *            the division
	 * @return the division detail
	 */
	public OrgDivisionVO getDivisionDetail(OrgDivisionVO division);

}
