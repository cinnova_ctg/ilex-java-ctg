package com.ilex.ws.core.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.InvBillToAddressVO;
import com.ilex.ws.core.dao.InvBillToAddressDAO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.MessageKeyConstant;

@Repository
public class InvBillToAddressDAOImpl implements InvBillToAddressDAO {
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	@Override
	public int insertBillToAddressDetails(
			final InvBillToAddressVO invBillToAddressVO) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		final StringBuilder sqlQuery = new StringBuilder(
				"insert into in_bill_to_address (in_bt_company,in_bt_address1,");
		sqlQuery.append("in_bt_address2,in_bt_city,in_bt_state,");
		sqlQuery.append("in_bt_zip_code,in_bt_country,");
		sqlQuery.append("in_bt_delivery_method,in_bt_mbt_id,");
		sqlQuery.append("in_bt_first_name,in_bt_last_name,in_bt_phone,in_bt_cell,");
		sqlQuery.append("in_bt_fax,in_bt_email,");
		sqlQuery.append("in_bt_ct_id,in_bt_er_delivery,in_bt_er_contact");
		sqlQuery.append(") values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						sqlQuery.toString(), new String[] { "id" });
				ps.setString(1, invBillToAddressVO.getClientName());
				ps.setString(2, invBillToAddressVO.getAddress1());
				ps.setString(3, invBillToAddressVO.getAddress2());
				ps.setString(4, invBillToAddressVO.getCity());
				ps.setString(5, invBillToAddressVO.getState());
				ps.setString(6, invBillToAddressVO.getZipCode());
				ps.setString(7, invBillToAddressVO.getCountry());
				ps.setInt(8, invBillToAddressVO.getDeliveryMethod());
				ps.setInt(9, invBillToAddressVO.getMbtId());
				ps.setString(10, invBillToAddressVO.getFirstName());
				ps.setString(11, invBillToAddressVO.getLastName());
				ps.setString(12, invBillToAddressVO.getPhone());
				ps.setString(13, invBillToAddressVO.getCell());
				ps.setString(14, invBillToAddressVO.getFax());
				ps.setString(15, invBillToAddressVO.getEmail());
				if (invBillToAddressVO.getInvContactId() == null) {
					ps.setNull(16, java.sql.Types.INTEGER);
				} else {
					ps.setInt(16, invBillToAddressVO.getInvContactId());
				}

				ps.setBoolean(17, invBillToAddressVO.isDeliveryExist());
				ps.setBoolean(18, invBillToAddressVO.isContactExist());

				return ps;
			}
		};

		jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);

		int insertedAddressId = keyHolder.getKey().intValue();
		return insertedAddressId;

	}

	@Override
	public void updateBillToAddressDetails(
			final InvBillToAddressVO invBillToAddressVO) {
		final StringBuilder sqlQuery = new StringBuilder(
				"update in_bill_to_address set in_bt_company=?,in_bt_address1=?,");
		sqlQuery.append(" in_bt_address2=?,in_bt_city=?,in_bt_state=?,");
		sqlQuery.append(" in_bt_zip_code=?,in_bt_country=?,");
		sqlQuery.append(" in_bt_delivery_method=?,");
		sqlQuery.append(" in_bt_first_name=?,in_bt_last_name=?,in_bt_phone=?,in_bt_cell=?,");
		sqlQuery.append(" in_bt_fax=?,in_bt_email=?, in_bt_er_delivery=?");
		if (invBillToAddressVO.getInvContactId() != null) {
			sqlQuery.append(" ,in_bt_ct_id=?");
		}
		sqlQuery.append(" where in_bt_id=?");
		Object[] params;
		if (invBillToAddressVO.getInvContactId() != null) {
			params = new Object[] { invBillToAddressVO.getClientName(),
					invBillToAddressVO.getAddress1(),
					invBillToAddressVO.getAddress2(),
					invBillToAddressVO.getCity(),
					invBillToAddressVO.getState(),
					invBillToAddressVO.getZipCode(),
					invBillToAddressVO.getCountry(),
					invBillToAddressVO.getDeliveryMethod(),
					invBillToAddressVO.getFirstName(),
					invBillToAddressVO.getLastName(),
					invBillToAddressVO.getPhone(),
					invBillToAddressVO.getCell(), invBillToAddressVO.getFax(),
					invBillToAddressVO.getEmail(),
					invBillToAddressVO.isDeliveryExist(),
					invBillToAddressVO.getInvContactId(),
					invBillToAddressVO.getInvBillToAddressId()

			};
		} else {
			params = new Object[] { invBillToAddressVO.getClientName(),
					invBillToAddressVO.getAddress1(),
					invBillToAddressVO.getAddress2(),
					invBillToAddressVO.getCity(),
					invBillToAddressVO.getState(),
					invBillToAddressVO.getZipCode(),
					invBillToAddressVO.getCountry(),
					invBillToAddressVO.getDeliveryMethod(),
					invBillToAddressVO.getFirstName(),
					invBillToAddressVO.getLastName(),
					invBillToAddressVO.getPhone(),
					invBillToAddressVO.getCell(), invBillToAddressVO.getFax(),
					invBillToAddressVO.getEmail(),
					invBillToAddressVO.isDeliveryExist(),

					invBillToAddressVO.getInvBillToAddressId(), };
		}

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	@Override
	public InvBillToAddressVO getBillToAddressDetails(
			final int invBillToAddressId) {

		StringBuilder sqlQuery = new StringBuilder(
				"select * from in_bill_to_address");
		sqlQuery.append(" where in_bt_id =?");

		Object[] params = new Object[] { invBillToAddressId };
		InvBillToAddressVO invBillToAddressVO;
		try {
			invBillToAddressVO = jdbcTemplateIlex.queryForObject(
					sqlQuery.toString(), params, new InvBillToAddresssMapper());
		} catch (EmptyResultDataAccessException ex) {
			throw new IlexBusinessException(
					MessageKeyConstant.CLIENT_DETAIL_NOT_FOUND);
		}

		return invBillToAddressVO;

	}

	/**
	 * The Class InvBillToAddresssMapper.
	 */
	private static final class InvBillToAddresssMapper implements
			RowMapper<InvBillToAddressVO> {

		@Override
		public InvBillToAddressVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvBillToAddressVO invBillToAddressVO = new InvBillToAddressVO();
			invBillToAddressVO.setInvBillToAddressId(rs.getInt("in_bt_id"));
			invBillToAddressVO.setClientName(rs.getString("in_bt_company"));
			invBillToAddressVO.setAddress1(rs.getString("in_bt_address1"));
			invBillToAddressVO.setAddress2(rs.getString("in_bt_address2"));
			invBillToAddressVO.setCity(rs.getString("in_bt_city"));
			invBillToAddressVO.setState(rs.getString("in_bt_state"));
			invBillToAddressVO.setZipCode(rs.getString("in_bt_zip_code"));
			invBillToAddressVO.setCountry(rs.getString("in_bt_country"));
			invBillToAddressVO.setDeliveryMethod(rs
					.getInt("in_bt_delivery_method"));
			invBillToAddressVO.setMbtId(rs.getInt("in_bt_mbt_id"));
			invBillToAddressVO.setFirstName(rs.getString("in_bt_first_name"));
			invBillToAddressVO.setLastName(rs.getString("in_bt_last_name"));
			invBillToAddressVO.setPhone(rs.getString("in_bt_phone"));
			invBillToAddressVO.setCell(rs.getString("in_bt_cell"));
			invBillToAddressVO.setFax(rs.getString("in_bt_fax"));
			invBillToAddressVO.setEmail(rs.getString("in_bt_email"));
			invBillToAddressVO.setInvContactId(rs.getInt("in_bt_ct_id"));
			invBillToAddressVO.setDeliveryExist(rs
					.getBoolean("in_bt_er_delivery"));
			invBillToAddressVO.setContactExist(rs
					.getBoolean("in_bt_er_contact"));
			return invBillToAddressVO;
		}
	}

}
