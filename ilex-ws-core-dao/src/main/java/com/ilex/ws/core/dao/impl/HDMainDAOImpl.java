package com.ilex.ws.core.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.CircuitStatusVO;
import com.ilex.ws.core.bean.EventCorrelatorVO;
import com.ilex.ws.core.bean.HDAuditTrailVO;
import com.ilex.ws.core.bean.HDBillableVO;
import com.ilex.ws.core.bean.HDCategoryVO;
import com.ilex.ws.core.bean.HDConfigurationItemVO;
import com.ilex.ws.core.bean.HDCubeStatusVO;
import com.ilex.ws.core.bean.HDCustomerDetailVO;
import com.ilex.ws.core.bean.HDEffortVO;
import com.ilex.ws.core.bean.HDHistoryVO;
import com.ilex.ws.core.bean.HDMonitoringTimeVO;
import com.ilex.ws.core.bean.HDNextActionVO;
import com.ilex.ws.core.bean.HDPendingAckTableVO;
import com.ilex.ws.core.bean.HDTicketLookUpVO;
import com.ilex.ws.core.bean.HDTicketVO;
import com.ilex.ws.core.bean.HDWIPTableVO;
import com.ilex.ws.core.bean.HDWorkNoteVO;
import com.ilex.ws.core.bean.IncidentStateVO;
import com.ilex.ws.core.bean.OutageDetailVO;
import com.ilex.ws.core.bean.ProvisioningDetailVO;
import com.ilex.ws.core.bean.ResolutionsVO;
import com.ilex.ws.core.bean.SiteDetailVO;
import com.ilex.ws.core.bean.StateTailVO;
import com.ilex.ws.core.bean.TicketStateVO;
import com.ilex.ws.core.bean.WugDetailsVO;
import com.ilex.ws.core.bean.WugOutInProcessVO;
import com.ilex.ws.core.dao.HDMainDAO;
import com.ilex.ws.core.dto.HDInstallNoteVO;
import com.ilex.ws.core.dto.LabelValue;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.HDTicketStatus;
import com.ilex.ws.core.util.MessageKeyConstant;
import com.mind.MSP.dao.Mspdao;
import com.mind.bean.msp.RealTimeState;
import com.mind.bean.msp.RealTimeStateInfo;
import com.mind.common.EnvironmentSelector;
import com.mind.common.util.MySqlConnection;
import com.mind.msp.common.EscalationLevel;

// TODO: Auto-generated Javadoc
/**
 * The Class HDMainDAOImpl.
 */
@Repository
public class HDMainDAOImpl implements HDMainDAO {

	/**
	 * The Constant logger.
	 */
	public static final Logger logger = Logger.getLogger(HDMainDAOImpl.class);

	/**
	 * The jdbc template ilex.
	 */
	@Resource(name = "jdbcTemplateIlex")
	private JdbcTemplate jdbcTemplateIlex;

	/**
	 * The jdbc template contingent db.
	 */
	@Resource(name = "jdbcTemplateContingentDB")
	private JdbcTemplate jdbcTemplateContingentDB;

	@Resource(name = "jdbcTemplatePivotDB")
	private JdbcTemplate jdbcTemplatePivotDB;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getBackupCircuitStatus(java.lang.String)
	 */
	@Override
	public String getBackupCircuitStatus(String siteNum) {
		Connection mySqlConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String url = EnvironmentSelector
				.getBundleString("appendix.detail.changestatus.mysql.url");
		String backupStatus = "N/A";
		try {
			mySqlConn = MySqlConnection.getMySqlConnection(url);

			StringBuilder sqlQuery = new StringBuilder(
					"SELECT dw_iw_ip_backup_status FROM iw_outage_in_process WHERE dw_iw_ip_site LIKE '%"
							+ siteNum + "%'");

			pstmt = mySqlConn.prepareStatement(sqlQuery.toString());
			rs = pstmt.executeQuery();
			if (rs.next()) {

				backupStatus = rs.getString(1);
			}

			// eventTest = mySqlConn.queryForInt();
		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {

			try {
				rs.close();
				pstmt.close();
				mySqlConn.close();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

		return backupStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getHDCustomers()
	 */
	@Override
	public List<HDCustomerDetailVO> getHDCustomers() {

		StringBuilder sqlQuery = new StringBuilder(
				"select * from lm_help_desk_create_ticket_customer_list_view order by lo_ot_name, lx_pr_title");

		List<HDCustomerDetailVO> hdCustomers = jdbcTemplateIlex.query(
				sqlQuery.toString(), new HDCustomer());
		return hdCustomers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getAuditTrail(java.lang.String)
	 */
	@Override
	public List<HDAuditTrailVO> getAuditTrail(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder(
				"SELECT lm_hd_at_id, lm_hd_at_ticket_id, convert(VARCHAR(25),lm_hd_at_update_date) lm_hd_at_update_date, lm_hd_at_next_action, lm_hd_at_short_description, "
						+ " lm_hd_at_circuit_status ,isnull(lm_hd_at_point_of_failure, '') lm_hd_at_point_of_failure ,lm_hd_at_root_cause, "
						+ " lm_hd_at_resolution, lm_hd_at_updated_by, lm_hd_at_incident_status, "
						+ " isnull(lo_pc_first_name, '')+' ' + isnull(lo_pc_last_name, '') as userName, "
						+ " isnull(wug_rs_desc, '') AS resolutionDesc, isnull(wug_rc_desc, '') rootCauseDesc, "
						+ " isnull(lm_st_status, '') incidentStateDesc, isnull(lm_na_next_action, '') nextActionDesc "
						+ " FROM lm_help_desk_audit_trail "
						+ " LEFT JOIN lo_poc ON lo_pc_id = lm_hd_at_updated_by "
						+ " LEFT JOIN wug_resolution ON wug_rs_id = lm_hd_at_resolution "
						+ " LEFT JOIN wug_root_cause ON wug_rc_id = lm_hd_at_root_cause "
						+ " LEFT JOIN lm_help_desk_ticket_status ON lm_st_status_abbreviation = lm_hd_at_incident_status "
						+ " LEFT JOIN lm_help_desk_ticket_next_actions ON lm_na_id = lm_hd_at_next_action "
						+ " WHERE lm_hd_at_ticket_id =   "
						+ ticketId
						+ " ORDER BY CONVERT(DATETIME , lm_hd_at_update_date) Desc");

		List<HDAuditTrailVO> hDAuditTrailList = jdbcTemplateIlex.query(
				sqlQuery.toString(), new HDAuditTrail());
		return hDAuditTrailList;
	}

	/**
	 * The Class HDCustomer.
	 */
	private static final class HDAuditTrail implements
			RowMapper<HDAuditTrailVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDAuditTrailVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			HDAuditTrailVO hDAuditTrailVO = new HDAuditTrailVO();

			hDAuditTrailVO.setId(rs.getString("lm_hd_at_id"));
			hDAuditTrailVO.setTicketId(rs.getString("lm_hd_at_ticket_id"));

			// hDAuditTrailVO.setChangeDate(rs.getDate("lm_hd_at_update_date"));
			// SimpleDateFormat formatter = new SimpleDateFormat(
			// "MM/dd/yyyy hh:mm aaa");
			// String datenewformat = formatter.format(rs
			// .getDate("lm_hd_at_update_date"));
			try {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
						"MMM dd yyyy hh:mma");
				Date date = simpleDateFormat.parse(rs
						.getString("lm_hd_at_update_date"));
				simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
				// System.out.println("date : " +
				// simpleDateFormat.format(date));
				hDAuditTrailVO.setChangeDateString(simpleDateFormat
						.format(date));
			} catch (ParseException ex) {
				System.out.println("Exception " + ex);
				hDAuditTrailVO.setChangeDateString("");
			}

			// hDAuditTrailVO.setChangeDateString(datenewformat);
			hDAuditTrailVO.setNextAction(rs.getString("lm_hd_at_next_action"));
			hDAuditTrailVO.setShortDesc(rs
					.getString("lm_hd_at_short_description"));
			hDAuditTrailVO.setBackUpCctState(rs
					.getString("lm_hd_at_circuit_status"));
			hDAuditTrailVO.setPointOfFailure(rs
					.getString("lm_hd_at_point_of_failure"));
			hDAuditTrailVO.setRootCause(rs.getString("lm_hd_at_root_cause"));
			hDAuditTrailVO.setResolution(rs.getString("lm_hd_at_resolution"));
			hDAuditTrailVO.setUpdatedBy(rs.getString("lm_hd_at_updated_by"));
			hDAuditTrailVO.setIncidentState(rs
					.getString("lm_hd_at_incident_status"));
			hDAuditTrailVO.setUserName(rs.getString("userName"));

			if (hDAuditTrailVO.getBackUpCctState() != null) {
				if (hDAuditTrailVO.getBackUpCctState().equals("0")) {
					hDAuditTrailVO.setBackUpCctStateDesc("Down");
				} else if (hDAuditTrailVO.getBackUpCctState().equals("1")) {
					hDAuditTrailVO.setBackUpCctStateDesc("Up");
				} else {
					hDAuditTrailVO.setBackUpCctStateDesc("Not Applicable");
				}
			} else {
				hDAuditTrailVO.setBackUpCctStateDesc("Not Applicable");
			}

			if (hDAuditTrailVO.getPointOfFailure().equals("1")) {
				hDAuditTrailVO.setPointOfFailureDesc("Customer");
			} else if (hDAuditTrailVO.getPointOfFailure().equals("2")) {
				hDAuditTrailVO.setPointOfFailureDesc("Contingent");
			} else if (hDAuditTrailVO.getPointOfFailure().equals("3")) {
				hDAuditTrailVO.setPointOfFailureDesc("ISP");
			} else {
				hDAuditTrailVO.setPointOfFailureDesc("");
			}

			hDAuditTrailVO.setIncidentStateDesc(rs
					.getString("incidentStateDesc"));
			hDAuditTrailVO.setNextActionDesc(rs.getString("nextActionDesc"));
			hDAuditTrailVO.setResolutionDesc(rs.getString("resolutionDesc"));
			hDAuditTrailVO.setRootCauseDesc(rs.getString("rootCauseDesc"));

			return hDAuditTrailVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#saveAuditTrail(com.ilex.ws.core.bean.
	 * HDAuditTrailVO)
	 */
	@Override
	public void saveAuditTrail(HDAuditTrailVO hdAuditTrailVO) {

		StringBuilder sqlQuery2 = new StringBuilder();

		sqlQuery2
				.append("insert into lm_help_desk_audit_trail ( lm_hd_at_ticket_id, lm_hd_at_update_date, lm_hd_at_next_action, lm_hd_at_short_description, ");
		sqlQuery2
				.append(" lm_hd_at_circuit_status, lm_hd_at_point_of_failure, lm_hd_at_root_cause, lm_hd_at_resolution, lm_hd_at_updated_by,  ");
		sqlQuery2
				.append(" lm_hd_at_incident_status) values(?, getdate(), ?, ?, ?, ?, ?, ?, ?, ?)");

		Object[] params = new Object[] { hdAuditTrailVO.getTicketId(),
				hdAuditTrailVO.getNextAction(), hdAuditTrailVO.getShortDesc(),
				hdAuditTrailVO.getBackUpCctState(),
				hdAuditTrailVO.getPointOfFailure(),
				hdAuditTrailVO.getRootCause(), hdAuditTrailVO.getResolution(),
				hdAuditTrailVO.getUpdatedBy(),
				hdAuditTrailVO.getIncidentState() };

		jdbcTemplateIlex.update(sqlQuery2.toString(), params);

	}

	/**
	 * The Class HDCustomer.
	 */
	private static final class HDCustomer implements
			RowMapper<HDCustomerDetailVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDCustomerDetailVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			HDCustomerDetailVO hdCustomerDetailVO = new HDCustomerDetailVO();
			hdCustomerDetailVO.setAppendixId(rs.getLong("lx_pr_id"));
			hdCustomerDetailVO.setAppendixName(rs.getString("lx_pr_title"));
			hdCustomerDetailVO.setApppendicBreakOut(rs
					.getString("appendix_type_breakout"));
			hdCustomerDetailVO.setCustomerName(rs.getString("lo_ot_name"));
			hdCustomerDetailVO.setMsaId(rs.getLong("lp_mm_id"));
			// hdCustomerDetailVO.setProjectEmail(rs.getString("lx_pr_email"));

			return hdCustomerDetailVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getSites(java.lang.Long)
	 */
	@Override
	public List<SiteDetailVO> getSites(Long msaId) {
		StringBuilder sqlQuery = new StringBuilder(
				"Select lp_sl_id,lp_si_number,lp_si_address,lp_si_city,lp_si_state, lp_si_pri_poc, lp_si_phone_no, lp_si_pri_email_id,lp_si_country, lp_si_zip_code ");
		sqlQuery.append(" from lp_site_list where lp_md_mm_id=? AND (lp_si_status = 'A' or lp_si_status is NULL) order by lp_si_number ");
		Object[] param = new Object[] { msaId };

		List<SiteDetailVO> sites = jdbcTemplateIlex.query(sqlQuery.toString(),
				param, new HDSiteMapper());
		return sites;
	}

	/**
	 * The Class HDSiteMapper.
	 */
	private static final class HDSiteMapper implements RowMapper<SiteDetailVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public SiteDetailVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			SiteDetailVO siteDetailVO = new SiteDetailVO();
			siteDetailVO.setSiteId(rs.getString("lp_sl_id"));
			siteDetailVO.setSiteAddress(rs.getString("lp_si_address"));
			siteDetailVO.setSiteCity(rs.getString("lp_si_city"));
			siteDetailVO.setState(rs.getString("lp_si_state"));
			siteDetailVO.setSiteNumber(rs.getString("lp_si_number"));
			siteDetailVO.setPrimaryName(rs.getString("lp_si_pri_poc"));
			siteDetailVO.setSitePhone(rs.getString("lp_si_phone_no"));
			siteDetailVO.setPrimaryEmail(rs.getString("lp_si_pri_email_id"));
			siteDetailVO.setCountry(rs.getString("lp_si_country"));
			siteDetailVO.setSiteZipcode(rs.getString("lp_si_zip_code"));
			return siteDetailVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getIncidentStates()
	 */
	@Override
	public List<IncidentStateVO> getIncidentStates(boolean all, String actual) {
		String sqlQuery = "SELECT  lm_st_status, lm_st_status_abbreviation, selectable FROM lm_help_desk_ticket_status WHERE (lm_st_active = 1"
				+ (all ? "" : " AND lm_st_display_on_active_form = 1")
				+ ")"
				+ (actual != null ? " OR lm_st_status_abbreviation = '"
						+ actual + "'" : "")
				+ " order by lm_st_display_on_active_form_order";

		List<IncidentStateVO> incidentStateList = jdbcTemplateIlex.query(
				sqlQuery, new IncidentStateMapper());
		return incidentStateList;
	}

	/**
	 * The Class IncidentStateMapper.
	 */
	private static final class IncidentStateMapper implements
			RowMapper<IncidentStateVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public IncidentStateVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			IncidentStateVO labelValue = new IncidentStateVO();
			labelValue.setLabel(rs.getString("lm_st_status"));
			labelValue.setValue(rs.getString("lm_st_status_abbreviation"));
			labelValue.setEnabled(1);
			labelValue.setSelectable(rs.getInt("selectable"));
			return labelValue;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getResolutions()
	 */
	@Override
	public List<ResolutionsVO> getResolutions() {
		List<ResolutionsVO> resolutionList = jdbcTemplateIlex.query(
				"SELECT * FROM wug_resolution", new ResolutionMapper());
		return resolutionList;
	}

	/**
	 * The Class ResolutionMapper.
	 */
	private static final class ResolutionMapper implements
			RowMapper<ResolutionsVO> {
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */

		public ResolutionsVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			ResolutionsVO labelValue = new ResolutionsVO();
			labelValue.setLabel(rs.getString("wug_rs_desc"));
			labelValue.setValue(rs.getString("wug_rs_id"));
			labelValue.setStatus(rs.getString("wug_rs_status"));
			return labelValue;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getConfigurationItems()
	 */
	@Override
	public List<HDConfigurationItemVO> getConfigurationItems() {
		StringBuilder sqlQuery = new StringBuilder(
				"select lm_ci_id, lm_ci_config_item from  dbo.lm_help_desk_ticket_config_items where lm_ci_active = 1 order by lm_ci_config_item ");

		List<HDConfigurationItemVO> configurationItems = jdbcTemplateIlex
				.query(sqlQuery.toString(), new HDConfigurationItem());
		return configurationItems;
	}

	/**
	 * The Class HDConfigurationItem.
	 */
	private static final class HDConfigurationItem implements
			RowMapper<HDConfigurationItemVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDConfigurationItemVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			HDConfigurationItemVO hdConfigurationItemVO = new HDConfigurationItemVO();
			hdConfigurationItemVO.setConfigurationItem(rs
					.getString("lm_ci_config_item"));
			hdConfigurationItemVO.setConfigurationItemId(rs
					.getString("lm_ci_id"));
			return hdConfigurationItemVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getCategories()
	 */
	@Override
	public List<HDCategoryVO> getCategories() {
		StringBuilder sqlQuery = new StringBuilder(
				"select lm_ct_id, lm_ct_category from dbo.lm_help_desk_ticket_categories where lm_ct_active = 1 order by lm_ct_category ");

		List<HDCategoryVO> categories = jdbcTemplateIlex.query(
				sqlQuery.toString(), new HDCategory());
		return categories;
	}

	/**
	 * The Class HDCategory.
	 */
	private static final class HDCategory implements RowMapper<HDCategoryVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDCategoryVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			HDCategoryVO hdCategoryVO = new HDCategoryVO();
			hdCategoryVO.setCategory(rs.getString("lm_ct_category"));
			hdCategoryVO.setCategoryId(rs.getString("lm_ct_id"));
			return hdCategoryVO;
		}
	}

	/**
	 * Gets the ticket type look up.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @param ticketTypeId
	 *            the ticket type id
	 * @return the ticket type look up
	 */
	private HDTicketLookUpVO getTicketTypeLookUp(Long appendixId,
			Integer ticketTypeId) {
		HDTicketLookUpVO hdTicketLookUpVO = new HDTicketLookUpVO();

		String existanceCheckSelect = "select 1 from lm_help_desk_ticket_type_lookup_values where lm_lv_pr_id = ? and lm_lv_hd_type = ?";

		Object[] params = new Object[] { appendixId, ticketTypeId };

		try {
			jdbcTemplateIlex.queryForInt(existanceCheckSelect, params);
		} catch (EmptyResultDataAccessException ex) {
			params = new Object[] { 0l, ticketTypeId };
		}

		StringBuilder selectTicketTypeLookup = new StringBuilder(
				"select cust_reference,msp,help_desk,category");

		selectTicketTypeLookup
				.append(" from lm_help_desk_ticket_type_lookup_values");
		selectTicketTypeLookup.append(" where lm_lv_pr_id = ?");
		selectTicketTypeLookup.append(" and lm_lv_hd_type = ?");
		try {
			hdTicketLookUpVO = jdbcTemplateIlex.queryForObject(
					selectTicketTypeLookup.toString(), params,
					new TicketTypeLookUpMapper());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hdTicketLookUpVO;

	}

	/**
	 * The Class TicketTypeLookUpMapper.
	 */
	private static final class TicketTypeLookUpMapper implements
			RowMapper<HDTicketLookUpVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDTicketLookUpVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			HDTicketLookUpVO hdTicketLookUpVO = new HDTicketLookUpVO();
			hdTicketLookUpVO.setCategory(rs.getString("category"));
			hdTicketLookUpVO.setCustomerReference(rs
					.getString("cust_reference"));
			hdTicketLookUpVO.setHelpDesk(rs.getString("help_desk"));
			hdTicketLookUpVO.setMsp(rs.getString("msp"));

			return hdTicketLookUpVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#avaliableIlexTicketNumber()
	 */
	@Override
	public String avaliableIlexTicketNumber() {
		String ticketNumberQry = "exec get_avaliable_ilex_ticket_number";
		int ticketNumber = jdbcTemplateIlex.queryForInt(ticketNumberQry);

		return StringUtils.leftPad(String.valueOf(ticketNumber), 8, "0");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#insertTicketToIlex(com.ilex.ws.core.bean
	 * .HDTicketVO)
	 */
	@Override
	public Long insertTicketToIlex(final HDTicketVO hdTicketVO) {

		String selectTicketTypeId = "select lm_ty_id from lm_help_desk_ticket_types where lm_ty_type = ?";
		String resourceSelect = "select  iv_rp_id from dbo.iv_resource_pool where iv_rp_name = 'Customer Care Agent'";
		int resource;
		try {

			resource = jdbcTemplateIlex.queryForInt(resourceSelect);
		} catch (EmptyResultDataAccessException ex) {
			resource = 0;
		}

		final Integer resourceId = resource;

		// final String ticketNumber = avaliableIlexTicketNumber();
		final String ticketNumber = hdTicketVO.getTicketNumber();

		Object[] params = new Object[] { hdTicketVO.getTicketType() };
		Integer ticketTypeId = jdbcTemplateIlex.queryForInt(selectTicketTypeId,
				params);

		final HDTicketLookUpVO hdTicketLookUpVO = getTicketTypeLookUp(
				hdTicketVO.getAppendixId(), ticketTypeId);

		KeyHolder keyHolder = new GeneratedKeyHolder();
		final StringBuilder sqlQuery = new StringBuilder(
				"insert into lm_ticket (lm_tc_number,lm_tc_requestor_name,");
		sqlQuery.append("lm_tc_requestor_email,lm_tc_requested_date,lm_tc_standby,");
		sqlQuery.append("lm_tc_msp,lm_tc_help_desk,");
		sqlQuery.append("lm_tc_category,lm_tc_cust_reference,");
		sqlQuery.append("lm_tc_problem_desc,lm_tc_preferred_arrival,lm_tc_preferred_window_start,lm_tc_preferred_window_end,");
		sqlQuery.append("lm_tc_criticality,lm_tc_resource_level,");
		sqlQuery.append("lm_tc_crit_cat_id,lm_tc_summary,lm_tc_type,");
		sqlQuery.append("lm_tc_request_type,lm_tc_cust_specific_fields,lm_tc_msp_monitoring_source");
		sqlQuery.append(") values (?,?,?,getdate(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						sqlQuery.toString(), new String[] { "id" });
				ps.setString(1, ticketNumber);
				ps.setString(2, hdTicketVO.getContactName());
				ps.setString(3, hdTicketVO.getEmailAddress()); // ps.setString(3,
				// hdTicketVO.getEmailAddressAdditional());
				ps.setString(4, "N"); // Default to 'N' for lm_tc_standby
				ps.setString(5, hdTicketLookUpVO.getMsp());
				ps.setString(6, hdTicketLookUpVO.getHelpDesk());
				ps.setString(7, hdTicketLookUpVO.getCategory());
				ps.setString(8, hdTicketLookUpVO.getCustomerReference());
				ps.setString(9, hdTicketVO.getProblemDescription());

				if (hdTicketVO.getPreferredArrivalTimeDate() == null) {
					ps.setNull(10, Types.TIMESTAMP);
				} else {
					ps.setTimestamp(10, new java.sql.Timestamp(hdTicketVO
							.getPreferredArrivalTimeDate().getTime()));
				}
				if (hdTicketVO.getPreferredArrivalWindowFromDate() == null) {
					ps.setNull(11, Types.TIMESTAMP);
				} else {
					ps.setTimestamp(11, new java.sql.Timestamp(hdTicketVO
							.getPreferredArrivalWindowFromDate().getTime()));
				}
				if (hdTicketVO.getPreferredArrivalWindowToDate() == null) {
					ps.setNull(12, Types.TIMESTAMP);
				} else {
					ps.setTimestamp(12, new java.sql.Timestamp(hdTicketVO
							.getPreferredArrivalWindowToDate().getTime()));
				}

				ps.setString(13, "1");// lm_tc_criticality default to 1
				ps.setInt(14, resourceId);
				ps.setString(15, "1");// lm_tc_crit_cat_id default to 1
				ps.setString(16, null);// ticket summary to null
				ps.setString(17, "Manual");
				ps.setString(18, "1");// lm_tc_request_type to 1

				ps.setString(19, null);// @lm_tc_cust_specific_fields to null
				ps.setString(20, "1");// @lm_tc_msp_monitoring_source = 1

				return ps;
			}
		};

		jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);

		Long ticketId = keyHolder.getKey().longValue();
		return ticketId;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getTicketNumber(java.lang.Long)
	 */
	@Override
	public String getTicketNumber(Long ticketId) {
		String selectTicketTypeId = "select lm_tc_number from lm_ticket where lm_tc_id=?";
		Object[] params = new Object[] { ticketId };

		String ticketNumber = jdbcTemplateIlex.queryForObject(
				selectTicketTypeId, params, String.class);

		return ticketNumber;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getTicketPriority(java.lang.Long,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int getTicketPriority(Long appendixId, Integer impact,
			Integer urgency) {

		String existanceCheckSelect = "select 1 from lm_help_desk_ticket_priority where lm_tp_pr_id = ? and lm_tp_impact = ? and lm_tp_urgency = ?";

		Object[] params = new Object[] { appendixId, impact, urgency };

		try {
			jdbcTemplateIlex.queryForInt(existanceCheckSelect, params);
		} catch (EmptyResultDataAccessException ex) {
			params = new Object[] { 0l, impact, urgency };
		}

		StringBuilder selectTicketTypeLookup = new StringBuilder(
				"select lm_tp_priority from lm_help_desk_ticket_priority");

		selectTicketTypeLookup
				.append("  where lm_tp_pr_id = ? and lm_tp_impact = ? and lm_tp_urgency = ?");

		int priority = jdbcTemplateIlex.queryForInt(
				selectTicketTypeLookup.toString(), params);

		return priority;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#insertHDTicketDetails(java.lang.Long,
	 * com.ilex.ws.core.bean.HDTicketVO, java.lang.String)
	 */
	@Override
	public void insertHDTicketDetails(Long ticketId, HDTicketVO hdTicketVO,
			String userId) {
		String selectTicketTypeId = "select lm_ty_id from lm_help_desk_ticket_types where lm_ty_type = ?";
		String selectTicketSourceId = "select lm_sr_id from lm_help_desk_ticket_sources where lm_sr_source = ?";
		String selectTicketStatusId = "select  lm_st_id from lm_help_desk_ticket_status where lm_st_status_abbreviation = '"
				+ hdTicketVO.getTicketStatus() + "' and lm_st_active = 1";

		Object[] ticketTypeParam = new Object[] { hdTicketVO.getTicketType() };
		Object[] ticketSourceParam = new Object[] { hdTicketVO
				.getTicketSource() };
		Integer ticketTypeId = jdbcTemplateIlex.queryForInt(selectTicketTypeId,
				ticketTypeParam);

		Integer ticketSourceId = jdbcTemplateIlex.queryForInt(
				selectTicketSourceId, ticketSourceParam);

		Integer ticketStatusId = jdbcTemplateIlex
				.queryForInt(selectTicketStatusId);

		String existanceCheckSelect = "	select 1 from lm_help_desk_ticket_details where lm_hd_tc_id = ?";

		Object[] params = new Object[] { ticketId };

		try {
			jdbcTemplateIlex.queryForInt(existanceCheckSelect, params);
		} catch (EmptyResultDataAccessException ex) {
			final StringBuilder sqlQuery = new StringBuilder(
					"insert into lm_help_desk_ticket_details (lm_hd_tc_id,lm_hd_source,");
			sqlQuery.append("lm_hd_type,lm_hd_customer_identifier,lm_hd_assigned_to,");
			sqlQuery.append("lm_hd_impact,lm_hd_urgency, ");
			sqlQuery.append("lm_hd_short_description,lm_hd_ci, ");
			sqlQuery.append("lm_hd_caller_name,lm_hd_caller_phone,lm_hd_caller_email,lm_hd_manual_ticket_reason, ");
			sqlQuery.append("lm_hd_circuit_status,lm_hd_lp_mm_id, ");
			sqlQuery.append("lm_hd_lx_pr_id,lm_hd_lp_sl_id,lm_hd_billable, ");
			sqlQuery.append("lm_hd_category,lm_hd_next_action,lm_hd_next_action_due,lm_hd_priority, ");
			sqlQuery.append("lm_hd_opened,lm_hd_status,lm_hd_tier,lm_hd_next_action_overdue, ");
			sqlQuery.append("lm_hd_update_date,lm_hd_updated_by,lm_hd_point_of_failure,lm_hd_root_cause,lm_hd_resolution ");
			sqlQuery.append(", lm_hd_assign_group_id, lm_hd_contact_type_id,lm_hd_provider_ref ");
			sqlQuery.append(") values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getDate(),?,?,?,getDate(),?,?,?,?,?,?,?)");

			Object[] insertParams = new Object[] {
					ticketId,
					ticketSourceId,
					ticketTypeId,
					hdTicketVO.getCustomerTicketReference(),
					userId,
					hdTicketVO.getImpact(),
					hdTicketVO.getUrgency(),
					hdTicketVO.getShortDescription(),
					hdTicketVO.getConfigurationItem(),
					hdTicketVO.getContactName(),
					hdTicketVO.getPhoneNumber(),
					hdTicketVO.getEmailAddress(), // hdTicketVO.getEmailAddressAdditional(),
					hdTicketVO.getManuallyCreationReason(),
					hdTicketVO.getBackUpCktStatus(),
					hdTicketVO.getMsaId(),
					hdTicketVO.getAppendixId(),
					hdTicketVO.getSiteId(),
					hdTicketVO.getBillingStatus(),
					hdTicketVO.getCategory(),
					hdTicketVO.getNextAction(),
					hdTicketVO.getNextActionDueDate(),
					hdTicketVO.getPriority(),
					ticketStatusId,
					1,
					1,
					userId,
					(hdTicketVO.getPointOfFailureId().isEmpty() ? null
							: Integer.valueOf(hdTicketVO.getPointOfFailureId())),
					(hdTicketVO.getRootCauseId().isEmpty() ? null : Integer
							.valueOf(hdTicketVO.getRootCauseId())),
					(hdTicketVO.getResolutionId().isEmpty() ? null : Integer
							.valueOf(hdTicketVO.getResolutionId())),
					hdTicketVO.getAssignmentGroupId(),
					hdTicketVO.getContactTypeId(),
					hdTicketVO.getProviderRefNo() };
			jdbcTemplateIlex.update(sqlQuery.toString(), insertParams);

			// next action history
			StringBuilder sqlQuery2 = new StringBuilder();

			sqlQuery2.append("insert into lm_help_desk_next_action_history (");
			sqlQuery2
					.append(" lm_na_tc_id,lm_na_action,lm_na_due_date,	lm_na_created_by,lm_na_inserted)");
			sqlQuery2.append(" values(	?,?,?,?,getdate())");

			params = new Object[] { ticketId, hdTicketVO.getNextAction(),
					hdTicketVO.getNextActionDueDate(), userId };

			jdbcTemplateIlex.update(sqlQuery2.toString(), params);

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#updateHDTicketDetails(java.lang.Long,
	 * com.ilex.ws.core.bean.HDTicketVO, java.lang.String)
	 */
	@Override
	public void updateHDTicketDetails(Long ticketId, HDTicketVO hdTicketVO,
			String userId) {
		String selectTicketTypeId = "select lm_ty_id from lm_help_desk_ticket_types where lm_ty_type = ?";
		// String selectTicketSourceId =
		// "select lm_sr_id from lm_help_desk_ticket_sources where lm_sr_source = "
		// + hdTicketVO.getTicketSource();
		String selectTicketStatusId = "select  lm_st_id from lm_help_desk_ticket_status where lm_st_status_abbreviation = '"
				+ hdTicketVO.getTicketStatus() + "' and lm_st_active = 1";

		Object[] ticketTypeParam = new Object[] { hdTicketVO.getTicketType() };
		// Object[] ticketSourceParam = new Object[] { hdTicketVO
		// .getTicketSource() };

		Integer ticketTypeId = jdbcTemplateIlex.queryForInt(selectTicketTypeId,
				ticketTypeParam);

		Integer ticketSourceId = Integer.parseInt(hdTicketVO.getTicketSource());
		// System.out.println("selectTicketStatusId = " + selectTicketStatusId);

		Integer ticketStatusId = jdbcTemplateIlex
				.queryForInt(selectTicketStatusId);

		StringBuilder sqlQuery4 = new StringBuilder();
		sqlQuery4
				.append("select  lm_hd_status from lm_help_desk_ticket_details where lm_hd_tc_id = ?");

		Object[] params1 = new Object[] { ticketId };

		int oldStatus = jdbcTemplateIlex.queryForInt(sqlQuery4.toString(),
				params1);
		// String existanceCheckSelect =
		// "	select 1 from lm_help_desk_ticket_details where lm_hd_tc_id = ?";
		//
		Object[] params = new Object[] { ticketId };
		//
		// try {
		// jdbcTemplateIlex.queryForInt(existanceCheckSelect, params);
		// } catch (EmptyResultDataAccessException ex) {
		final StringBuilder sqlQuery = new StringBuilder(
				"update lm_help_desk_ticket_details SET lm_hd_source = ?,");
		sqlQuery.append("lm_hd_type = ?,lm_hd_customer_identifier = ?,");
		sqlQuery.append("lm_hd_impact = ?,lm_hd_urgency = ?,");
		sqlQuery.append("lm_hd_short_description = ?,lm_hd_ci = ?,");
		sqlQuery.append("lm_hd_caller_name = ?,lm_hd_caller_phone = ?,lm_hd_caller_email = ?,lm_hd_manual_ticket_reason = ?,");
		sqlQuery.append("lm_hd_circuit_status = ?,lm_hd_lp_mm_id = ?,");
		sqlQuery.append("lm_hd_lx_pr_id = ?,lm_hd_lp_sl_id = ?,lm_hd_billable = ?,");
		sqlQuery.append("lm_hd_category = ?,lm_hd_next_action = ?,lm_hd_next_action_due = ?,lm_hd_priority = ?,");
		sqlQuery.append("lm_hd_status = ?,lm_hd_tier = ?,lm_hd_next_action_overdue = ?,lm_hd_update_date = getDate(),lm_hd_updated_by = ?,lm_hd_point_of_failure = ?,lm_hd_root_cause = ?,lm_hd_resolution = ?");
		sqlQuery.append(", lm_hd_assign_group_id = ?, lm_hd_contact_type_id = ?, lm_hd_provider_ref = ?,lm_hd_ext_system_status= 0");
		sqlQuery.append("where lm_hd_tc_id = ?");

		Object[] insertParams = new Object[] {
				ticketSourceId,
				ticketTypeId,
				hdTicketVO.getCustomerTicketReference(),
				hdTicketVO.getImpact(),
				hdTicketVO.getUrgency(),
				hdTicketVO.getShortDescription(),
				hdTicketVO.getConfigurationItem(),
				hdTicketVO.getContactName(),
				hdTicketVO.getPhoneNumber(),
				hdTicketVO.getEmailAddress(), // hdTicketVO.getEmailAddressAdditional(),
				hdTicketVO.getManuallyCreationReason(),
				hdTicketVO.getBackUpCktStatus(),
				hdTicketVO.getMsaId(),
				hdTicketVO.getAppendixId(),
				hdTicketVO.getSiteId(),
				hdTicketVO.getBillingStatus(),
				hdTicketVO.getCategory(),
				hdTicketVO.getNextAction(),
				hdTicketVO.getNextActionDueDate(),
				hdTicketVO.getPriority(),
				ticketStatusId,
				1,
				0,
				userId,
				(hdTicketVO.getPointOfFailureId().isEmpty() ? null : Integer
						.valueOf(hdTicketVO.getPointOfFailureId())),
				(hdTicketVO.getRootCauseId().isEmpty() ? null : Integer
						.valueOf(hdTicketVO.getRootCauseId())),
				(hdTicketVO.getResolutionId().isEmpty() ? null : Integer
						.valueOf(hdTicketVO.getResolutionId())),
				hdTicketVO.getAssignmentGroupId(),
				hdTicketVO.getContactTypeId(), hdTicketVO.getProviderRefNo(),
				ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), insertParams);

		// next action history
		StringBuilder sqlQuery2 = new StringBuilder();

		sqlQuery2.append("insert into lm_help_desk_next_action_history (");
		sqlQuery2
				.append(" lm_na_tc_id,lm_na_action,lm_na_due_date,	lm_na_created_by,lm_na_inserted)");
		sqlQuery2.append(" values(	?,?,?,?,getdate())");

		params = new Object[] { ticketId, hdTicketVO.getNextAction(),
				hdTicketVO.getNextActionDueDate(), userId };

		jdbcTemplateIlex.update(sqlQuery2.toString(), params);

		// update lm_ticket with problem description
		StringBuilder sqlQuery3 = new StringBuilder("update lm_ticket set ");
		sqlQuery3.append(" lm_tc_problem_desc=?");
		sqlQuery3.append(" where lm_tc_id=?");

		params = new Object[] { hdTicketVO.getProblemDescription(),
				hdTicketVO.getTicketId() };

		jdbcTemplateIlex.update(sqlQuery3.toString(), params);

		HDHistoryVO hdHistoryVO = new HDHistoryVO();
		hdHistoryVO.setEvent("16");
		hdHistoryVO.setFromData(null);
		hdHistoryVO.setFromReference(String.valueOf(oldStatus));
		hdHistoryVO.setInitiator(userId);
		hdHistoryVO.setTicketId(Long.valueOf(ticketId));
		hdHistoryVO.setToData(null);
		hdHistoryVO.setToReference(String.valueOf(ticketStatusId));

		if (StringUtils.isNotBlank(hdTicketVO.getPrimaryState())
				&& !hdTicketVO.getPrimaryState().toLowerCase().contains("down")) {
			/*
			 * If site status not down then remove the entry from
			 * wug_outages_incidents_in_process table and add it into
			 * wug_outages_incident_close table.
			 */
			closeWUGOutageIncident(ticketId.toString(),
					hdTicketVO.getTicketStatus());
		}

		manageChangeHistory(hdHistoryVO);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getSiteDetails(java.lang.String)
	 */
	@Override
	public SiteDetailVO getSiteDetails(String siteId) {
		StringBuilder sqlQuery = new StringBuilder(
				"Select * from lp_site_list where lp_sl_id=? ");
		Object[] param = new Object[] { siteId };

		SiteDetailVO sites = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), param, new HDSiteCompleteDetailMapper());
		return sites;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getPendingAcKCustData()
	 */
	@Override
	public List<HDPendingAckTableVO> getPendingAcKCustData(String siteNum) {
		StringBuilder sqlQuery = new StringBuilder("select");
		sqlQuery.append(" lm_hd_opened, lp_si_number, lm_sr_source, lo_ot_name, lp_si_address, lp_si_city, lp_si_state,");
		sqlQuery.append(" lx_pr_title,lp_si_country, lp_si_zip_code, lp_si_pri_poc, lp_si_phone_no, lp_si_pri_email_id, lm_hd_tc_id, lm_hd_status, lm_tc_number,isnull(lp_sl_priority_store,0) as lp_sl_priority_store");
		sqlQuery.append(" from");
		sqlQuery.append(" (");
		sqlQuery.append(" SELECT *");
		sqlQuery.append(" FROM lm_help_desk_ticket_details");
		sqlQuery.append(" WHERE lm_hd_status = 1 AND lm_hd_next_action_overdue = 0 AND (lm_hd_source = 4 OR lm_hd_source = 3)");
		sqlQuery.append(" ) as a");
		sqlQuery.append(" join");
		sqlQuery.append(" (");
		sqlQuery.append(" SELECT *");
		sqlQuery.append(" FROM lp_site_list");
		sqlQuery.append(" WHERE lp_si_number like '%");
		sqlQuery.append(siteNum);
		sqlQuery.append("%'");
		sqlQuery.append(" ) as b ON lm_hd_lp_sl_id = lp_sl_id");
		sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id");
		sqlQuery.append(" join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id");
		sqlQuery.append(" join lo_organization_top on lp_mm_ot_id = lo_ot_id");
		sqlQuery.append(" join lm_help_desk_ticket_sources on lm_hd_source = lm_sr_id");
		sqlQuery.append(" join lx_appendix_main on lm_hd_lx_pr_id= lx_pr_id");
		sqlQuery.append(" join lm_help_desk_ticket_status on lm_hd_status = lm_st_id");

		List<HDPendingAckTableVO> hdPendingAckTableVOList = jdbcTemplateIlex
				.query(sqlQuery.toString(), new PendingAckCustMapper());
		return hdPendingAckTableVOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getPendingAcKSecurityData()
	 */
	@Override
	public List<HDPendingAckTableVO> getPendingAcKSecurityData(String siteNum) {
		String sqlQuery = "SELECT lm_hd_opened, lp_si_number, lm_sr_source, lo_ot_name, lp_si_address, lp_si_city, lp_si_state,"
				+ " lx_pr_title,lp_si_country, lp_si_zip_code, lp_si_pri_poc, lp_si_phone_no, lp_si_pri_email_id, lm_hd_tc_id, lm_hd_status, lm_tc_number,isnull(lp_sl_priority_store,0) as lp_sl_priority_store"
				+ " from ( SELECT * FROM lm_help_desk_ticket_details WHERE lm_hd_status = 1 AND lm_hd_next_action_overdue = 0 AND lm_hd_source = 7 ) as a"
				+ " join ( SELECT * FROM lp_site_list WHERE lp_si_number like '%"
				+ siteNum
				+ "%' ) as b ON lm_hd_lp_sl_id = lp_sl_id"
				+ " join lm_ticket on lm_hd_tc_id = lm_tc_id"
				+ " join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id"
				+ " join lo_organization_top on lp_mm_ot_id = lo_ot_id"
				+ " join lm_help_desk_ticket_sources on lm_hd_source = lm_sr_id"
				+ " join lx_appendix_main on lm_hd_lx_pr_id= lx_pr_id"
				+ " join lm_help_desk_ticket_status on lm_hd_status = lm_st_id";

		List<HDPendingAckTableVO> hdPendingAckTableVOList = jdbcTemplateIlex
				.query(sqlQuery.toString(), new PendingAckCustMapper());
		return hdPendingAckTableVOList;
	}

	/**
	 * The Class PendingAckCustMapper.
	 */
	private static final class PendingAckCustMapper implements
			RowMapper<HDPendingAckTableVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDPendingAckTableVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			HDPendingAckTableVO hdPendingAckTableVO = new HDPendingAckTableVO();
			hdPendingAckTableVO
					.setOpenedDateDB(rs.getTimestamp("lm_hd_opened"));
			hdPendingAckTableVO.setSiteNumber(rs.getString("lp_si_number"));
			hdPendingAckTableVO.setSource(rs.getString("lm_sr_source"));
			hdPendingAckTableVO.setCustomer(rs.getString("lo_ot_name"));
			hdPendingAckTableVO.setSiteAddress(rs.getString("lp_si_address"));
			hdPendingAckTableVO.setSiteCity(rs.getString("lp_si_city"));
			hdPendingAckTableVO.setSiteState(rs.getString("lp_si_state"));
			hdPendingAckTableVO.setSiteCountry(rs.getString("lp_si_country"));
			hdPendingAckTableVO.setSiteZipcode(rs.getString("lp_si_zip_code"));
			hdPendingAckTableVO.setContactName(rs.getString("lp_si_pri_poc"));
			hdPendingAckTableVO.setContactPhoneNumber(rs
					.getString("lp_si_phone_no"));
			hdPendingAckTableVO.setContactEmailAddress(rs
					.getString("lp_si_pri_email_id"));
			hdPendingAckTableVO.setTicketId(rs.getString("lm_hd_tc_id"));
			hdPendingAckTableVO.setAppendixName(rs.getString("lx_pr_title"));
			hdPendingAckTableVO.setIlexNumber(rs.getString("lm_tc_number"));
			hdPendingAckTableVO.setSite_prop(rs
					.getString("lp_sl_priority_store"));
			return hdPendingAckTableVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getPendingAcKAlertData()
	 */
	@Override
	public List<HDPendingAckTableVO> getPendingAcKAlertData(String siteNum) {
		StringBuilder sqlQuery = new StringBuilder(
				"select 'Network Change' lm_al_alert, ");
		sqlQuery.append(" lm_hd_opened, lp_si_number, lm_sr_source, lo_ot_name, lp_si_address, lp_si_city, lp_si_state,");
		sqlQuery.append(" lx_pr_title,lp_si_country, lp_si_zip_code, lp_si_pri_poc, lp_si_phone_no, lp_si_pri_email_id, lm_hd_tc_id, lm_hd_status, lm_tc_number,isnull(lp_sl_priority_store,0) as lp_sl_priority_store");
		sqlQuery.append(" from");
		sqlQuery.append(" (");
		sqlQuery.append(" SELECT *");
		sqlQuery.append(" FROM lm_help_desk_ticket_details");
		sqlQuery.append(" WHERE lm_hd_source = 5 and lm_hd_next_action_overdue = 0 ");
		sqlQuery.append(" ) as a");
		sqlQuery.append(" join");
		sqlQuery.append(" (");
		sqlQuery.append(" SELECT *");
		sqlQuery.append(" FROM lp_site_list");
		sqlQuery.append(" WHERE lp_si_number like '%");
		sqlQuery.append(siteNum);
		sqlQuery.append("%'");
		sqlQuery.append(" ) as b ON lm_hd_lp_sl_id = lp_sl_id");
		sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id");
		sqlQuery.append(" join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id");
		sqlQuery.append(" join lo_organization_top on lp_mm_ot_id = lo_ot_id");
		sqlQuery.append(" join lm_help_desk_ticket_sources on lm_hd_source = lm_sr_id");
		sqlQuery.append(" join lx_appendix_main on lm_hd_lx_pr_id= lx_pr_id");
		sqlQuery.append(" join lm_help_desk_ticket_status on lm_hd_status = lm_st_id");

		List<HDPendingAckTableVO> hdPendingAckTableVOList = jdbcTemplateIlex
				.query(sqlQuery.toString(), new PendingAckAlertMapper());
		return hdPendingAckTableVOList;
	}

	/**
	 * The Class PendingAckAlertMapper.
	 */
	private static final class PendingAckAlertMapper implements
			RowMapper<HDPendingAckTableVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDPendingAckTableVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			HDPendingAckTableVO hdPendingAckTableVO = new HDPendingAckTableVO();
			hdPendingAckTableVO
					.setOpenedDateDB(rs.getTimestamp("lm_hd_opened"));
			hdPendingAckTableVO.setSiteNumber(rs.getString("lp_si_number"));
			hdPendingAckTableVO.setState(rs.getString("lm_al_alert"));
			hdPendingAckTableVO.setCustomer(rs.getString("lo_ot_name"));
			hdPendingAckTableVO.setSiteAddress(rs.getString("lp_si_address"));
			hdPendingAckTableVO.setSiteCity(rs.getString("lp_si_city"));
			hdPendingAckTableVO.setSiteState(rs.getString("lp_si_state"));
			hdPendingAckTableVO.setSiteCountry(rs.getString("lp_si_country"));
			hdPendingAckTableVO.setSiteZipcode(rs.getString("lp_si_zip_code"));
			hdPendingAckTableVO.setContactName(rs.getString("lp_si_pri_poc"));
			hdPendingAckTableVO.setContactPhoneNumber(rs
					.getString("lp_si_phone_no"));
			hdPendingAckTableVO.setContactEmailAddress(rs
					.getString("lp_si_pri_email_id"));
			hdPendingAckTableVO.setTicketId(rs.getString("lm_hd_tc_id"));
			hdPendingAckTableVO.setAppendixName(rs.getString("lx_pr_title"));
			hdPendingAckTableVO.setIlexNumber(rs.getString("lm_tc_number"));
			hdPendingAckTableVO.setSite_prop(rs
					.getString("lp_sl_priority_store"));
			return hdPendingAckTableVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getPendingAcKMonitorData()
	 */
	@Override
	public List<HDPendingAckTableVO> getMonitoringDateData(String siteNum) {
		StringBuilder sqlQuery = new StringBuilder(
				"SELECT lm_hd_tc_id, lm_tc_number,lm_hd_opened, lm_hd_circuit_status "
						+ " , timeCount  , lp_si_number, lm_na_next_action,	isnull(lp_sl_priority_store,0)lp_sl_priority_store  "
						+ " FROM lm_help_desk_ticket_details  "
						+ " LEFT JOIN lm_ticket ON lm_ticket.lm_tc_id = lm_hd_tc_id "
						+ " LEFT JOIN lm_help_desk_ticket_next_actions ON lm_na_id = lm_hd_next_action "
						+ " LEFT JOIN lp_site_list ON lm_hd_lp_sl_id = lp_sl_id "
						+ " JOIN  ( "
						+ " select sum(DATEDIFF(minute,lm_hd_tmh_start_date,isnull(lm_hd_tmh_end_date, getdate()))) timeCount, lm_hd_tmh_tc_id "
						+ " from lm_help_desk_ticket_monitoring_history  "
						+ " GROUP BY lm_hd_tmh_tc_id) aaa ON lm_hd_tc_id = aaa.lm_hd_tmh_tc_id ");
		// sqlQuery.append(" WHERE lx_pr_id " + inClause
		// + "  (SELECT pr_id FROM lm_help_desk_tier2_customers) ");
		// if (inClause.equals("in")) {
		// sqlQuery.append(" ORDER BY lo_ot_name, lm_hd_opened ");
		// }

		List<HDPendingAckTableVO> hdPendingAckTableVOList = jdbcTemplateIlex
				.query(sqlQuery.toString(), new MonitoringDateMapper());

		return hdPendingAckTableVOList;
	}

	/**
	 * The Class PendingAckMonitorMapper.
	 */
	private static final class MonitoringDateMapper implements
			RowMapper<HDPendingAckTableVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDPendingAckTableVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			HDPendingAckTableVO hdPendingAckTableVO = new HDPendingAckTableVO();

			hdPendingAckTableVO.setSiteState(rs
					.getString("lm_hd_circuit_status"));
			hdPendingAckTableVO.setTicketId(rs.getString("lm_hd_tc_id"));
			hdPendingAckTableVO.setIlexNumber(rs.getString("lm_tc_number"));
			float val = Float.parseFloat(rs.getString("timeCount"));
			if (val >= 60) {
				val = val / 60;
				DecimalFormat df = new DecimalFormat();
				df.setMaximumFractionDigits(2);
				hdPendingAckTableVO.setDateDiff(df.format(val) + " Hours");
			} else {
				hdPendingAckTableVO.setDateDiff(val + " Minute");
			}

			hdPendingAckTableVO
					.setOpenedDateDB(rs.getTimestamp("lm_hd_opened"));
			hdPendingAckTableVO.setNextActionLabel(rs
					.getString("lm_na_next_action"));
			// hdPendingAckTableVO.setSiteDisplayName(rs.getString("wug_ip_name"));
			hdPendingAckTableVO.setSiteNumber(rs.getString("lp_si_number"));
			hdPendingAckTableVO.setSite_prop(rs
					.getString("lp_sl_priority_store"));
			// hdPendingAckTableVO.setState(rs.getString("wug_ip_sStateName"));
			// hdPendingAckTableVO.setCustomer(rs.getString("lo_ot_name"));
			// hdPendingAckTableVO.setSiteAddress(rs.getString("lp_si_address"));
			// hdPendingAckTableVO.setSiteCity(rs.getString("lp_si_city"));

			// hdPendingAckTableVO.setSiteCountry(rs.getString("lp_si_country"));
			// hdPendingAckTableVO.setSiteZipcode(rs.getString("lp_si_zip_code"));
			// hdPendingAckTableVO.setContactName(rs.getString("lp_si_pri_poc"));
			// hdPendingAckTableVO.setContactPhoneNumber(rs
			// .getString("lp_si_phone_no"));
			// hdPendingAckTableVO.setContactEmailAddress(rs
			// .getString("lp_si_pri_email_id"));
			// hdPendingAckTableVO.setAppendixName(rs.getString("lx_pr_title"));
			return hdPendingAckTableVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getPendingAcKMonitorData()
	 */
	@Override
	public List<HDPendingAckTableVO> getPendingAcKMonitorData(String siteNum,
			String inClause) {
		StringBuilder sqlQuery = new StringBuilder("select");
		sqlQuery.append(" lm_hd_opened, wug_ip_name, lm_sr_source, lo_ot_name, lp_si_address, lp_si_city, lp_si_state,");
		sqlQuery.append(" lx_pr_title,lp_si_country, lp_si_zip_code, lp_si_pri_poc, lp_si_phone_no, lp_si_pri_email_id, lm_hd_tc_id, lm_hd_status, lm_tc_number");
		sqlQuery.append(" ,wug_ip_sStateName, lp_si_number, datediff(minute,lm_hd_opened, getdate()) dateDiff,isnull(lp_sl_priority_store,0) as lp_sl_priority_store  from ");
		sqlQuery.append(" (");
		sqlQuery.append(" SELECT *");
		sqlQuery.append(" FROM lm_help_desk_ticket_details");
		sqlQuery.append(" WHERE lm_hd_source = 2 AND lm_hd_next_action_overdue = 0 AND lm_hd_status = 1");
		sqlQuery.append(" ) as a");
		sqlQuery.append(" join");
		sqlQuery.append(" (");
		sqlQuery.append(" SELECT *");
		sqlQuery.append(" FROM lp_site_list");
		sqlQuery.append(" WHERE lp_si_number like '%");
		sqlQuery.append(siteNum);
		sqlQuery.append("%'");
		sqlQuery.append(" ) as b ON lm_hd_lp_sl_id = lp_sl_id");
		sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id");
		sqlQuery.append(" join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id");
		sqlQuery.append(" join lo_organization_top on lp_mm_ot_id = lo_ot_id");
		sqlQuery.append(" join lm_help_desk_ticket_sources on lm_hd_source = lm_sr_id");
		sqlQuery.append(" join lx_appendix_main on lm_hd_lx_pr_id= lx_pr_id");
		sqlQuery.append(" join lm_help_desk_ticket_status on lm_hd_status = lm_st_id");
		sqlQuery.append(" join wug_outages_incidents_in_process on lm_hd_tc_id = wug_ip_tc_id");
		sqlQuery.append(" WHERE lx_pr_id " + inClause
				+ "  (SELECT pr_id FROM lm_help_desk_tier2_customers) ");
		if (inClause.equals("in")) {
			sqlQuery.append(" ORDER BY lo_ot_name, lm_hd_opened ");
		}

		List<HDPendingAckTableVO> hdPendingAckTableVOList = jdbcTemplateIlex
				.query(sqlQuery.toString(), new PendingAckMonitorMapper());

		return hdPendingAckTableVOList;
	}

	/**
	 * The Class PendingAckMonitorMapper.
	 */
	private static final class PendingAckMonitorMapper implements
			RowMapper<HDPendingAckTableVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDPendingAckTableVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			HDPendingAckTableVO hdPendingAckTableVO = new HDPendingAckTableVO();
			hdPendingAckTableVO
					.setOpenedDateDB(rs.getTimestamp("lm_hd_opened"));
			hdPendingAckTableVO.setSiteDisplayName(rs.getString("wug_ip_name"));
			hdPendingAckTableVO.setSiteNumber(rs.getString("lp_si_number"));
			hdPendingAckTableVO.setState(rs.getString("wug_ip_sStateName"));
			hdPendingAckTableVO.setCustomer(rs.getString("lo_ot_name"));
			hdPendingAckTableVO.setSiteAddress(rs.getString("lp_si_address"));
			hdPendingAckTableVO.setSiteCity(rs.getString("lp_si_city"));
			hdPendingAckTableVO.setSiteState(rs.getString("lp_si_state"));
			hdPendingAckTableVO.setSiteCountry(rs.getString("lp_si_country"));
			hdPendingAckTableVO.setSiteZipcode(rs.getString("lp_si_zip_code"));
			hdPendingAckTableVO.setContactName(rs.getString("lp_si_pri_poc"));
			hdPendingAckTableVO.setContactPhoneNumber(rs
					.getString("lp_si_phone_no"));
			hdPendingAckTableVO.setContactEmailAddress(rs
					.getString("lp_si_pri_email_id"));
			hdPendingAckTableVO.setTicketId(rs.getString("lm_hd_tc_id"));
			hdPendingAckTableVO.setAppendixName(rs.getString("lx_pr_title"));
			hdPendingAckTableVO.setIlexNumber(rs.getString("lm_tc_number"));
			hdPendingAckTableVO.setDateDiff(rs.getString("dateDiff"));
			hdPendingAckTableVO.setSite_prop(rs
					.getString("lp_sl_priority_store"));
			return hdPendingAckTableVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getNextActions()
	 */
	@Override
	public List<HDNextActionVO> getNextActions() {
		StringBuilder sqlQuery = new StringBuilder(
				"select lm_na_id, lm_na_next_action from dbo.lm_help_desk_ticket_next_actions where lm_na_active = 1 order by lm_na_next_action");

		List<HDNextActionVO> nextActions = jdbcTemplateIlex.query(
				sqlQuery.toString(), new HDNextAction());
		return nextActions;
	}

	/**
	 * The Class HDNextAction.
	 */
	private static final class HDNextAction implements
			RowMapper<HDNextActionVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDNextActionVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			HDNextActionVO nextAction = new HDNextActionVO();
			nextAction.setNextAction(rs.getString("lm_na_next_action"));
			nextAction.setNextActionId(rs.getString("lm_na_id"));

			return nextAction;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getAckCustTicketDetails(java.lang.String)
	 */
	@Override
	public List<HDTicketVO> getAckCustTicketDetails(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder(
				"select lm_hd_tc_id, lm_ty_type, lm_hd_billable,lm_hd_caller_name, lm_hd_caller_phone, lm_hd_caller_email,");
		sqlQuery.append(" lm_hd_lp_sl_id, lm_tc_preferred_arrival, lm_tc_preferred_window_start, lm_tc_preferred_window_end,");
		sqlQuery.append(" lm_hd_customer_identifier, lm_hd_category, lm_hd_short_description, lm_hd_ci,");
		sqlQuery.append(" lm_tc_problem_desc, lp_sl_id, lp_si_number,lp_si_address, lp_si_city, lp_si_state, lp_si_zip_code, lp_si_country,");
		sqlQuery.append(" lm_ts_number, lm_ts_address, lm_ts_city, lm_ts_state, lm_ts_country, lm_ts_zip_code, lo_ot_name, lx_pr_title,");
		sqlQuery.append(" wug_ip_backup_status,lm_hd_priority,lm_hd_impact, lm_hd_urgency, lm_hd_next_action,");
		sqlQuery.append(" lm_hd_next_action_due, lm_wn_note, lm_hd_lp_mm_id, lm_hd_lx_pr_id,wug_ip_sStateName,lm_hd_circuit_status,lm_tc_number");
		sqlQuery.append(" from");
		sqlQuery.append(" ( SELECT * FROM lm_help_desk_ticket_details WHERE lm_hd_tc_id=? ) as a");
		sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id");
		sqlQuery.append(" join lm_help_desk_ticket_types on lm_hd_type = lm_ty_id");
		sqlQuery.append(" join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id");
		sqlQuery.append(" join lo_organization_top on lp_mm_ot_id = lo_ot_id");
		sqlQuery.append(" join lx_appendix_main on lm_hd_lx_pr_id = lx_pr_id");
		sqlQuery.append(" left outer join wug_outages_incidents_in_process on lm_hd_tc_id = wug_ip_tc_id");
		sqlQuery.append(" left outer join lm_help_desk_work_notes on lm_hd_tc_id = lm_wn_tc_id");
		sqlQuery.append(" left outer join lp_site_list on lm_hd_lp_sl_id = lp_sl_id");
		sqlQuery.append(" left outer join lm_help_desk_temp_site_details on lm_hd_tc_id = lm_ts_tc_id");

		Object[] params = new Object[] { ticketId };

		List<HDTicketVO> hdTicketVOList = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new AckCustTicketMapper());

		return hdTicketVOList;
	}

	/**
	 * The Class AckCustTicketMapper.
	 */
	private static final class AckCustTicketMapper implements
			RowMapper<HDTicketVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDTicketVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			HDTicketVO hdTicketVO = new HDTicketVO();
			hdTicketVO.setBillingStatus(rs.getBoolean("lm_hd_billable"));
			hdTicketVO.setCallerName(rs.getString("lm_hd_caller_name"));
			hdTicketVO.setCallerPhone(rs.getString("lm_hd_caller_phone"));
			hdTicketVO.setCallerEmail(rs.getString("lm_hd_caller_email"));
			hdTicketVO.setSiteId(rs.getString("lm_hd_lp_sl_id"));
			hdTicketVO.setPreferredArrivalTimeDate(rs
					.getDate("lm_tc_preferred_arrival"));
			hdTicketVO.setPreferredArrivalWindowFromDate(rs
					.getDate("lm_tc_preferred_window_start"));
			hdTicketVO.setPreferredArrivalWindowToDate(rs
					.getDate("lm_tc_preferred_window_end"));
			hdTicketVO.setCustomerTicketReference(rs
					.getString("lm_hd_customer_identifier"));
			hdTicketVO.setCategory(rs.getString("lm_hd_category"));
			hdTicketVO.setShortDescription(rs
					.getString("lm_hd_short_description"));
			hdTicketVO.setConfigurationItem(rs.getString("lm_hd_ci"));
			hdTicketVO
					.setProblemDescription(rs.getString("lm_tc_problem_desc"));
			hdTicketVO.setSiteNumber(rs.getString("lp_si_number"));
			hdTicketVO.setSiteAddress(rs.getString("lp_si_address"));
			hdTicketVO.setSiteCity(rs.getString("lp_si_city"));
			hdTicketVO.setSiteState(rs.getString("lp_si_state"));
			hdTicketVO.setSiteZipcode(rs.getString("lp_si_zip_code"));
			hdTicketVO.setSiteCountry(rs.getString("lp_si_country"));
			hdTicketVO.setSiteNumberTemp(rs.getString("lm_ts_number"));
			hdTicketVO.setSiteAddressTemp(rs.getString("lm_ts_address"));
			hdTicketVO.setSiteCityTemp(rs.getString("lm_ts_city"));
			hdTicketVO.setSiteStateTemp(rs.getString("lm_ts_state"));
			hdTicketVO.setSiteZipcodeTemp(rs.getString("lm_ts_zip_code"));
			hdTicketVO.setSiteCountryTemp(rs.getString("lm_ts_country"));
			hdTicketVO.setCustomerName(rs.getString("lo_ot_name"));
			hdTicketVO.setAppendixName(rs.getString("lx_pr_title"));
			hdTicketVO.setTicketType(rs.getString("lm_ty_type"));
			hdTicketVO.setBackUpCktStatus(rs.getString("wug_ip_backup_status"));
			hdTicketVO.setPriority(rs.getString("lm_hd_priority"));
			hdTicketVO.setImpact(rs.getString("lm_hd_impact"));
			hdTicketVO.setUrgency(rs.getString("lm_hd_urgency"));
			hdTicketVO.setNextAction(rs.getString("lm_hd_next_action"));
			hdTicketVO
					.setNextActionDueDate(rs.getDate("lm_hd_next_action_due"));
			hdTicketVO.setCustomerWorkNotes(rs.getString("lm_wn_note"));
			hdTicketVO.setAppendixId(rs.getLong("lm_hd_lx_pr_id"));
			hdTicketVO.setMsaId(rs.getLong("lm_hd_lp_mm_id"));
			hdTicketVO.setTicketId(rs.getString("lm_hd_tc_id"));
			hdTicketVO.setState(rs.getString("wug_ip_sStateName"));
			hdTicketVO.setDevice(rs.getString("lm_hd_circuit_status"));
			hdTicketVO.setTicketNumber(rs.getString("lm_tc_number"));

			return hdTicketVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getHdWipTableData(java.lang.String)
	 */
	@Override
	public List<HDWIPTableVO> getHdWipTableData(String cubeType, String siteNum) {

		/*
		 * String dateCondition = " AND lm_hd_next_action_due > GETDATE() "; if
		 * (cubeType.equals("True")) { dateCondition =
		 * " AND DATEDIFF ( minute , lm_hd_next_action_due , getdate() ) >= 60 "
		 * ; }
		 * 
		 * StringBuilder sqlQuery = new StringBuilder("select DATEDIFF ");
		 * sqlQuery.append(
		 * " (minute , lm_hd_next_action_due , getdate() ) as dateDiff, lm_hd_opened, lm_hd_update_date, isnull(wug_ip_name ,lp_si_number) as 'lp_si_number', lo_ot_name, lp_si_address, lp_si_city, lp_si_state,"
		 * ); sqlQuery.append(
		 * " lp_si_country, lp_si_zip_code, lp_si_pri_poc, lp_si_phone_no, lp_si_pri_email_id, lm_hd_tc_id,lm_hd_priority,"
		 * );
		 * sqlQuery.append("  lx_pr_title,lm_na_next_action,lm_hd_next_action_due,"
		 * ); sqlQuery.append(
		 * " isnull(lo_pc_last_name, 'Not Assigned') as lo_pc_last_name,");
		 * sqlQuery.append(
		 * " lm_hd_caller_name,lm_hd_short_description, lm_tc_number, lm_hd_session_id,lm_hd_next_action,lm_st_status_abbreviation,"
		 * ); sqlQuery.append(
		 * " lm_hd_circuit_status, lm_hd_status, lm_sr_id,wug_ip_backup_status,isnull(wug_ip_sStateName, 'Up') as wug_ip_sStateName,lm_hd_next_action_overdue, lm_hd_ext_system_status "
		 * ); sqlQuery.append(
		 * " ,lm_hd_agm_name,lm_hd_under_review, lm_tc_number, lp_mm_id, isnull(lm_hd_client_type, 'HQ') as clientType, "
		 * ); sqlQuery.append(" isnull(lm_hd_ecl_color, '') lm_hd_ecl_color ");
		 * sqlQuery.append(" from lm_help_desk_ticket_details");
		 * sqlQuery.append(" join lp_site_list on lm_hd_lp_sl_id = lp_sl_id");
		 * sqlQuery.append(" join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id");
		 * sqlQuery
		 * .append(" join lo_organization_top on lp_mm_ot_id = lo_ot_id");
		 * sqlQuery
		 * .append(" join lm_help_desk_ticket_sources on lm_hd_source = lm_sr_id"
		 * );
		 * sqlQuery.append(" join lx_appendix_main on lm_hd_lx_pr_id= lx_pr_id"
		 * ); sqlQuery.append(
		 * " join lm_help_desk_ticket_status on lm_hd_status = lm_st_id");
		 * sqlQuery.append(
		 * " join lm_help_desk_ticket_next_actions on lm_hd_next_action = lm_na_id"
		 * ); sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id");
		 * sqlQuery.append(" left join lo_poc on lm_hd_updated_by = lo_pc_id");
		 * sqlQuery.append(
		 * " left outer join wug_outages_incidents_in_process on lm_hd_tc_id = wug_ip_tc_id"
		 * ); sqlQuery.append(
		 * " left join lm_help_desk_assignment_group_master on lm_hd_agm_id = lm_hd_assign_group_id"
		 * ); sqlQuery.append(
		 * " LEFT JOIN lm_help_desk_client_type ON lm_hd_ct_mm_id = lp_mm_id ");
		 * sqlQuery.append(
		 * " LEFT JOIN lm_help_desk_event_colors ON lm_hd_ecl_pc_id = lm_hd_updated_by OR lm_hd_ecl_pr_id = lx_pr_id "
		 * ); sqlQuery.append(
		 * " where lm_hd_status not in (1,6,8,10,11/*New, Resolved, Cancelled, Complete, Closed) "
		 * ); sqlQuery.append(
		 * " and lm_hd_next_action_overdue = ? and lp_si_number like '%" +
		 * siteNum + "%'  " + dateCondition);
		 * sqlQuery.append(" order by lm_hd_priority, lm_hd_next_action_due");
		 * 
		 * Object[] params = new Object[]{Boolean.valueOf(cubeType)};
		 * 
		 * List<HDWIPTableVO> wipTableData = jdbcTemplateIlex.query(
		 * sqlQuery.toString(), params, new HDWipTableMapper());
		 */

		// New Query Wip data

		List<HDWIPTableVO> wipTableData = new ArrayList<HDWIPTableVO>();
		StringBuilder sqlQuery = new StringBuilder();
		// New Query Result FOR Wip Data TABLE
		if (cubeType.equals("False")) {
			sqlQuery = new StringBuilder(
					"select datediff ( minute , lm_hd_next_action_due , getdate() ) as dateDiff, lm_hd_opened, lm_hd_update_date, ");
			sqlQuery.append(" isnull(wug_ip_name ,lp_si_number) as 'lp_si_number', lo_ot_name, lp_si_address, lp_si_city, lp_si_state, lp_si_country, lp_si_zip_code,");
			sqlQuery.append(" lp_si_pri_poc, lp_si_phone_no, lp_si_pri_email_id, lm_hd_tc_id,lm_hd_priority, lx_pr_title,lm_na_next_action,lm_hd_next_action_due, ");
			sqlQuery.append(" isnull(LEFT(lo_pc_first_name, 1)+' '+lo_pc_last_name, 'Not Assigned') as lo_pc_last_name, lm_hd_caller_name,lm_hd_short_description, lm_tc_number, ");
			sqlQuery.append(" lm_hd_session_id,lm_hd_next_action,lm_st_status_abbreviation, lm_hd_circuit_status, lm_hd_status, lm_sr_id,wug_ip_backup_status,");
			sqlQuery.append(" isnull(wug_ip_sStateName, 'Up') as wug_ip_sStateName,lm_hd_next_action_overdue, lm_hd_ext_system_status ,lm_hd_agm_name,lm_hd_under_review, ");
			sqlQuery.append(" lp_mm_id, isnull(lm_hd_client_type, 'HQ') as clientType, isnull(lm_hd_ecl_color, '') lm_hd_ecl_color,isnull(lp_sl_priority_store,0)lp_sl_priority_store ");
			sqlQuery.append(" from lm_help_desk_ticket_details   ");
			sqlQuery.append(" join lp_site_list on lm_hd_lp_sl_id = lp_sl_id ");
			sqlQuery.append(" join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id ");
			sqlQuery.append(" join lo_organization_top on lp_mm_ot_id = lo_ot_id ");
			sqlQuery.append(" join lm_help_desk_ticket_sources on lm_hd_source = lm_sr_id ");
			sqlQuery.append(" join lx_appendix_main on lm_hd_lx_pr_id= lx_pr_id ");
			sqlQuery.append(" join lm_help_desk_ticket_status on lm_hd_status = lm_st_id ");
			sqlQuery.append(" join lm_help_desk_ticket_next_actions on lm_hd_next_action = lm_na_id ");
			sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id left join lo_poc on lm_hd_updated_by = lo_pc_id ");
			sqlQuery.append(" left join wug_outages_incidents_in_process on lm_hd_tc_id = wug_ip_tc_id ");
			sqlQuery.append(" left join lm_help_desk_assignment_group_master on lm_hd_agm_id = lm_hd_assign_group_id ");
			sqlQuery.append(" left join lm_help_desk_client_type on lm_hd_ct_mm_id = lp_mm_id ");
			sqlQuery.append(" LEFT JOIN lm_help_desk_event_colors ON lm_hd_ecl_pc_id = lm_hd_updated_by OR lm_hd_ecl_pr_id = lx_pr_id ");

			sqlQuery.append(" where lm_st_is_wip = 1 and lm_st_id <> 12 and lm_hd_next_action_due > dateadd(hour,1,getdate()) ");

			sqlQuery.append(" order by lm_hd_priority, lm_hd_next_action_due ");

			wipTableData = jdbcTemplateIlex.query(sqlQuery.toString(),
					new HDWipTableMapper());

			return wipTableData;
		}

		if (cubeType.equals("True")) {
			// Over DUE
			sqlQuery = new StringBuilder(
					" select lm_tc_js_id,datediff ( minute , lm_hd_next_action_due , getdate() ) as dateDiff, lm_hd_opened, lm_hd_update_date, ");
			sqlQuery.append(" isnull(wug_ip_name ,lp_si_number) as 'lp_si_number', lo_ot_name, lp_si_address, lp_si_city, lp_si_state, lp_si_country, lp_si_zip_code,");
			sqlQuery.append(" lp_si_pri_poc, lp_si_phone_no, lp_si_pri_email_id, lm_hd_tc_id,lm_hd_priority, lx_pr_title,lm_na_next_action,lm_hd_next_action_due, ");
			sqlQuery.append(" isnull(LEFT(lo_pc_first_name, 1)+' '+lo_pc_last_name, 'Not Assigned') as lo_pc_last_name, lm_hd_caller_name,lm_hd_short_description, lm_tc_number, ");
			sqlQuery.append(" lm_hd_session_id,lm_hd_next_action,lm_st_status_abbreviation, lm_hd_circuit_status, lm_hd_status, lm_sr_id,wug_ip_backup_status,");
			sqlQuery.append(" isnull(wug_ip_sStateName, 'Up') as wug_ip_sStateName,lm_hd_next_action_overdue, lm_hd_ext_system_status ,lm_hd_agm_name, ");
			sqlQuery.append(" lm_hd_under_review, lp_mm_id, isnull(lm_hd_client_type, 'HQ')clientType, isnull(lm_hd_ecl_color, '') lm_hd_ecl_color,isnull(lp_sl_priority_store,0) as lp_sl_priority_store ");
			sqlQuery.append(" from lm_help_desk_ticket_details ");
			sqlQuery.append(" join lp_site_list on lm_hd_lp_sl_id = lp_sl_id ");
			sqlQuery.append(" join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id ");
			sqlQuery.append(" join lo_organization_top on lp_mm_ot_id = lo_ot_id ");
			sqlQuery.append(" join lm_help_desk_ticket_sources on lm_hd_source = lm_sr_id  ");
			sqlQuery.append(" join lx_appendix_main on lm_hd_lx_pr_id= lx_pr_id  ");
			sqlQuery.append(" join lm_help_desk_ticket_status on lm_hd_status = lm_st_id ");
			sqlQuery.append(" join lm_help_desk_ticket_next_actions on lm_hd_next_action = lm_na_id ");
			sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id ");
			sqlQuery.append(" left join lo_poc on lm_hd_updated_by = lo_pc_id  ");
			sqlQuery.append(" left join wug_outages_incidents_in_process on lm_hd_tc_id = wug_ip_tc_id ");
			sqlQuery.append(" left join lm_help_desk_assignment_group_master on lm_hd_agm_id = lm_hd_assign_group_id  ");
			sqlQuery.append(" left join lm_help_desk_client_type on lm_hd_ct_mm_id = lp_mm_id  ");
			sqlQuery.append(" LEFT JOIN lm_help_desk_event_colors ON lm_hd_ecl_pc_id = lm_hd_updated_by OR lm_hd_ecl_pr_id = lx_pr_id ");
			//
			sqlQuery.append(" where (lm_st_is_wip = 1 and lm_hd_next_action_due <= getdate()) or (lm_st_id = 1/*New*/ and lm_tc_js_id <> 0) or lm_st_id = 12/*Reopened*/ ");

			wipTableData = jdbcTemplateIlex.query(sqlQuery.toString(),
					new HDWipTableMapper());

		}
		return wipTableData;

	}

	@Override
	public boolean getnextactionduestatus() {
		StringBuilder sqlQuery = new StringBuilder("select COUNT(*) ");
		sqlQuery.append(" from lm_help_desk_ticket_details");
		sqlQuery.append(" where lm_hd_status not in (1,6,8,10,11)  ");
		sqlQuery.append(" AND lm_hd_next_action_due <= GETDATE()  AND DATEDIFF ( second , lm_hd_next_action_due , getdate() ) <= 15 ");
		sqlQuery.append(" AND lm_hd_next_action_overdue = 'false'");

		int oldStatus = jdbcTemplateIlex.queryForInt(sqlQuery.toString());
		if (oldStatus >= 1) {
			return true;
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getHdWipNextActionTableData(java.lang.
	 * String)
	 */
	@Override
	public List<HDWIPTableVO> getHdWipNextActionTableData(String siteNum) {

		/*
		 * StringBuilder sqlQuery = new StringBuilder("select DATEDIFF ");
		 * sqlQuery.append(
		 * " (minute , lm_hd_next_action_due , getdate() ) as dateDiff,  lm_hd_opened, lm_hd_update_date, "
		 * ); sqlQuery.append(
		 * " isnull(wug_ip_name ,lp_si_number) as 'lp_si_number', lo_ot_name, lp_si_address, lp_si_city, lp_si_state,"
		 * ); sqlQuery.append(
		 * " lp_si_country, lp_si_zip_code, lp_si_pri_poc, lp_si_phone_no, lp_si_pri_email_id, lm_hd_tc_id,lm_hd_priority,"
		 * );
		 * sqlQuery.append(" lx_pr_title,lm_na_next_action,lm_hd_next_action_due,"
		 * ); sqlQuery.append(
		 * " isnull(lo_pc_last_name, 'Not Assigned') as lo_pc_last_name,");
		 * sqlQuery.append(
		 * " lm_hd_caller_name,lm_hd_short_description, lm_tc_number, lm_hd_session_id,lm_hd_next_action,lm_st_status_abbreviation,"
		 * ); sqlQuery.append(
		 * " lm_hd_circuit_status, lm_hd_status, lm_sr_id,wug_ip_backup_status,isnull(wug_ip_sStateName, 'Up') as wug_ip_sStateName, "
		 * ); sqlQuery.append(
		 * " lm_hd_next_action_overdue, lm_hd_ext_system_status ,lm_hd_agm_name,lm_hd_under_review, lm_tc_number, lp_mm_id, isnull(lm_hd_client_type, 'HQ') as clientType , isnull(lm_hd_ecl_color, '') lm_hd_ecl_color "
		 * ); sqlQuery.append(" from"); sqlQuery.append(
		 * " ( SELECT * FROM lm_help_desk_ticket_details WHERE lm_hd_status not in (1,6,8,10,11) AND lm_hd_next_action_due <= GETDATE() AND lm_hd_next_action_overdue = 'false' ) as a"
		 * ); sqlQuery.append(" join"); sqlQuery.append(
		 * " ( SELECT * FROM lp_site_list WHERE lp_si_number like '%");
		 * sqlQuery.append(siteNum.trim());
		 * sqlQuery.append("%' ) as b on lm_hd_lp_sl_id = lp_sl_id");
		 * sqlQuery.append(" join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id");
		 * sqlQuery
		 * .append(" join lo_organization_top on lp_mm_ot_id = lo_ot_id");
		 * sqlQuery
		 * .append(" join lm_help_desk_ticket_sources on lm_hd_source = lm_sr_id"
		 * );
		 * sqlQuery.append(" join lx_appendix_main on lm_hd_lx_pr_id= lx_pr_id"
		 * ); sqlQuery.append(
		 * " join lm_help_desk_ticket_status on lm_hd_status = lm_st_id");
		 * sqlQuery.append(
		 * " join lm_help_desk_ticket_next_actions on lm_hd_next_action = lm_na_id"
		 * ); sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id");
		 * sqlQuery.append(" left join lo_poc on lm_hd_updated_by = lo_pc_id");
		 * sqlQuery.append(
		 * " left outer join wug_outages_incidents_in_process on lm_hd_tc_id = wug_ip_tc_id"
		 * ); sqlQuery.append(
		 * " left join lm_help_desk_assignment_group_master on lm_hd_agm_id = lm_hd_assign_group_id"
		 * ); sqlQuery.append(
		 * " LEFT JOIN lm_help_desk_client_type ON lm_hd_ct_mm_id = lp_mm_id ");
		 * sqlQuery.append(
		 * " LEFT JOIN lm_help_desk_event_colors ON lm_hd_ecl_pc_id = lm_hd_updated_by OR lm_hd_ecl_pr_id = lx_pr_id "
		 * );
		 * sqlQuery.append(" order by lm_hd_priority, lm_hd_next_action_due");
		 */
		// sqlQuery.append(" AND DATEDIFF ( minute , lm_hd_next_action_due , getdate() ) < 60  ");
		// Object[] params = new Object[] { Boolean.valueOf(cubeType) };

		StringBuilder sqlQuery = new StringBuilder(
				"select datediff ( minute , lm_hd_next_action_due , getdate() ) as dateDiff, lm_hd_opened, lm_hd_update_date, ");
		sqlQuery.append(" isnull(wug_ip_name ,lp_si_number) as 'lp_si_number', lo_ot_name, lp_si_address, lp_si_city, lp_si_state, lp_si_country, lp_si_zip_code, ");
		sqlQuery.append(" lp_si_pri_poc, lp_si_phone_no, lp_si_pri_email_id, lm_hd_tc_id,lm_hd_priority, lx_pr_title,lm_na_next_action,lm_hd_next_action_due,");
		sqlQuery.append(" isnull(LEFT(lo_pc_first_name, 1)+' '+lo_pc_last_name, 'Not Assigned') as lo_pc_last_name, lm_hd_caller_name,lm_hd_short_description, lm_tc_number, lm_hd_session_id,");
		sqlQuery.append(" lm_hd_next_action,lm_st_status_abbreviation, lm_hd_circuit_status, lm_hd_status, lm_sr_id,wug_ip_backup_status,");
		sqlQuery.append(" isnull(wug_ip_sStateName, 'Up') as wug_ip_sStateName, lm_hd_next_action_overdue, lm_hd_ext_system_status ,lm_hd_agm_name,");
		sqlQuery.append(" lm_hd_under_review, lm_tc_number, lp_mm_id, isnull(lm_hd_client_type, 'HQ') as clientType, isnull(lm_hd_ecl_color, '') lm_hd_ecl_color,isnull(lp_sl_priority_store,0) as lp_sl_priority_store ");
		// sqlQuery.append(" case when wug_ip_current_status = 11 then 1 else 0 end ord, isnull(lm_hd_ecl_color, '') lm_hd_ecl_color ");
		sqlQuery.append(" from lm_help_desk_ticket_details ");
		sqlQuery.append(" join lp_site_list as b on lm_hd_lp_sl_id = lp_sl_id  ");
		sqlQuery.append(" join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id join lo_organization_top on lp_mm_ot_id = lo_ot_id  ");
		sqlQuery.append(" join lm_help_desk_ticket_sources on lm_hd_source = lm_sr_id  ");
		sqlQuery.append(" join lx_appendix_main on lm_hd_lx_pr_id= lx_pr_id ");
		sqlQuery.append(" join lm_help_desk_ticket_status on lm_hd_status = lm_st_id  ");
		sqlQuery.append(" join lm_help_desk_ticket_next_actions on lm_hd_next_action = lm_na_id ");
		sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id left join lo_poc on lm_hd_updated_by = lo_pc_id");
		sqlQuery.append(" left join wug_outages_incidents_in_process on lm_hd_tc_id = wug_ip_tc_id  ");
		sqlQuery.append(" left join lm_help_desk_assignment_group_master on lm_hd_agm_id = lm_hd_assign_group_id ");
		sqlQuery.append(" left join lm_help_desk_client_type on lm_hd_ct_mm_id = lp_mm_id ");
		sqlQuery.append(" LEFT JOIN lm_help_desk_event_colors ON lm_hd_ecl_pc_id = lm_hd_updated_by OR lm_hd_ecl_pr_id = lx_pr_id ");
		sqlQuery.append(" where lm_st_is_wip = 1 and lm_st_id <> 12  and lm_hd_next_action_due > getdate() and lm_hd_next_action_due <= dateadd(hour,1,getdate()) ");
		sqlQuery.append(" order by lm_hd_priority, lm_hd_next_action_due ");
		/*
		 * sqlQuery.append(" where lm_hd_status = 6 ");
		 * 
		 * sqlQuery.append("  order by ord desc, lm_hd_update_date desc ");
		 */

		List<HDWIPTableVO> wipTableData = jdbcTemplateIlex.query(
				sqlQuery.toString(), new HDWipTableMapper());

		if (wipTableData.size() > 0 && isOneHourPassed(wipTableData)) {
			return getHdWipNextActionTableData(siteNum);
		}

		return wipTableData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getHdResolvedTableData(java.lang.
	 * String)
	 */
	@Override
	public List<HDWIPTableVO> getHdResolvedTableData(String siteNum) {
		StringBuilder sqlQuery = new StringBuilder(
				"select datediff ( minute , lm_hd_update_date , getdate() ) as dateDiff, lm_hd_opened, lm_hd_update_date, ");
		sqlQuery.append("isnull(wug_ip_name ,lp_si_number) as 'lp_si_number', lo_ot_name, lp_si_address, ");
		sqlQuery.append(" lp_si_city, lp_si_state, lp_si_country, lp_si_zip_code, lp_si_pri_poc, lp_si_phone_no, lp_si_pri_email_id, lm_hd_tc_id,lm_hd_priority,");
		sqlQuery.append(" lx_pr_title,lm_na_next_action,lm_hd_next_action_due, isnull(LEFT(lo_pc_first_name, 1)+' '+lo_pc_last_name, 'Not Assigned') as lo_pc_last_name, lm_hd_caller_name,");
		sqlQuery.append(" lm_hd_short_description, lm_tc_number, lm_hd_session_id,lm_hd_next_action,lm_st_status_abbreviation, lm_hd_circuit_status, lm_hd_status,");
		sqlQuery.append(" lm_sr_id,wug_ip_backup_status,isnull(wug_ip_sStateName, 'Up') as wug_ip_sStateName , lm_hd_next_action_overdue, lm_hd_ext_system_status ,");
		sqlQuery.append(" lm_hd_agm_name,lm_hd_under_review, lm_tc_number, lp_mm_id, isnull(lm_hd_client_type, 'HQ') as clientType, ");
		sqlQuery.append(" case when wug_ip_current_status = 11 then 1 else 0 end ord, isnull(lm_hd_ecl_color, '') lm_hd_ecl_color,isnull(lp_sl_priority_store,0) as lp_sl_priority_store ");
		// sqlQuery.append(" lp_mm_id, isnull(lm_hd_client_type, 'HQ') as clientType, CASE WHEN wug_ip_current_status = 11 THen 1 ELSe 0 END ord, isnull(lm_hd_ecl_color, '') lm_hd_ecl_color ");
		sqlQuery.append(" from lm_help_desk_ticket_details");
		sqlQuery.append(" join lp_site_list on lm_hd_lp_sl_id = lp_sl_id ");
		sqlQuery.append(" join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id");
		sqlQuery.append(" join lo_organization_top on lp_mm_ot_id = lo_ot_id ");
		sqlQuery.append(" join lm_help_desk_ticket_sources on lm_hd_source = lm_sr_id ");
		sqlQuery.append(" join lx_appendix_main on lm_hd_lx_pr_id= lx_pr_id ");
		sqlQuery.append(" join lm_help_desk_ticket_status on lm_hd_status = lm_st_id");
		sqlQuery.append(" join lm_help_desk_ticket_next_actions on lm_hd_next_action = lm_na_id");
		sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id");
		// sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id");
		sqlQuery.append(" left join lo_poc on lm_hd_updated_by = lo_pc_id ");
		sqlQuery.append(" left join wug_outages_incidents_in_process on lm_hd_tc_id = wug_ip_tc_id ");
		sqlQuery.append(" left join lm_help_desk_assignment_group_master on lm_hd_agm_id = lm_hd_assign_group_id ");
		sqlQuery.append(" left join lm_help_desk_client_type on lm_hd_ct_mm_id = lp_mm_id ");
		sqlQuery.append("  LEFT JOIN lm_help_desk_event_colors ON lm_hd_ecl_pc_id = lm_hd_updated_by OR lm_hd_ecl_pr_id = lx_pr_id ");
		// sqlQuery.append(siteNum.trim());
		sqlQuery.append(" where lm_hd_status = 6 ");
		// sqlQuery.append(" /*AND lm_hd_next_action_overdue = 'false' */");
		// sqlQuery.append(" AND DATEDIFF ( minute , lm_hd_next_action_due , getdate() ) < 60  ");
		sqlQuery.append(" order by ord desc, lm_hd_update_date desc  ");
		// Object[] params = new Object[] { Boolean.valueOf(cubeType) };

		List<HDWIPTableVO> wipTableData = jdbcTemplateIlex.query(
				sqlQuery.toString(), new HDWipTableMapper());

		// if (wipTableData.size() > 0 && isdayPassed(wipTableData)) {
		// return getHdResolvedTableData(siteNum);
		// }
		return wipTableData;
	}

	private void closeWUGOutageIncident(String ticketId, String statusId) {

		if (ticketId == null || ticketId.equals("")
				|| !statusId.equals("Closed")) {
			/*
			 * if status is not 11 i-e closed then return else remove entry from
			 * wug_outages_incidents_in_process and add to table
			 * wug_outages_incident_close
			 */
			return;
		}

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("INSERT INTO dbo.wug_outages_incident_close ");
		sqlQuery.append("SELECT * FROM wug_outages_incidents_in_process WHERE wug_ip_tc_id = ?");

		Object[] params = new Object[] { ticketId };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

		sqlQuery = new StringBuilder(
				"DELETE FROM wug_outages_incidents_in_process WHERE wug_ip_tc_id = ?");
		params = new Object[] { ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/**
	 * Checks if is one hour passed.
	 * 
	 * @param wipTableData
	 *            the wip table data
	 * @return true, if is one hour passed
	 */
	private boolean isOneHourPassed(List<HDWIPTableVO> wipTableData) {

		boolean retValue = false;
		for (int i = 0; i < wipTableData.size(); i++) {
			HDWIPTableVO hdwipTableVO = wipTableData.get(i);
			if (!hdwipTableVO.getDateDiff().equals("")
					&& Integer.parseInt(hdwipTableVO.getDateDiff()) >= 60
					&& !hdwipTableVO.isNextActionOverDue()) {
				int priority = Integer.parseInt(hdwipTableVO.getPriority());

				if (priority > 1) {
					updatePriorityAndOverdueStatus(hdwipTableVO.getTicketId(),
							--priority);
				} else {
					updatePriorityAndOverdueStatus(hdwipTableVO.getTicketId(),
							1);
				}
				retValue = true;
			}
		}
		return retValue;
	}

	/**
	 * Update priority and overdue status.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param priority
	 *            the priority
	 */
	private void updatePriorityAndOverdueStatus(String ticketId, int priority) {

		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("UPDATE dbo.lm_help_desk_ticket_details "
				+ " SET lm_hd_next_action_overdue = 1, lm_hd_priority = ?  "
				+ " WHERE lm_hd_tc_id =  ? ");

		Object[] params = new Object[] { priority, ticketId };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);
	}

	/**
	 * The Class HDWipTableMapper.
	 */
	private static final class HDWipTableMapper implements
			RowMapper<HDWIPTableVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDWIPTableVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			HDWIPTableVO hdWIPTableVO = new HDWIPTableVO();
			hdWIPTableVO.setOpenedDbDate(rs.getTimestamp("lm_hd_opened"));
			hdWIPTableVO.setUpdatedDbDate(rs.getTimestamp("lm_hd_update_date"));
			hdWIPTableVO.setSiteNumber(rs.getString("lp_si_number"));
			if (hdWIPTableVO.getSiteNumber() == null
					|| "".equals(hdWIPTableVO.getSiteNumber())) {
				hdWIPTableVO.setSiteNumber(rs.getString("lo_ot_name"));
			}
			hdWIPTableVO.setStatus(rs.getString("lm_st_status_abbreviation"));
			hdWIPTableVO.setCustomer(rs.getString("lo_ot_name"));
			hdWIPTableVO.setSiteAddress(rs.getString("lp_si_address"));
			hdWIPTableVO.setSiteCity(rs.getString("lp_si_city"));
			hdWIPTableVO.setSiteState(rs.getString("lp_si_state"));
			hdWIPTableVO.setSiteCountry(rs.getString("lp_si_country"));
			hdWIPTableVO.setSiteZipcode(rs.getString("lp_si_zip_code"));
			hdWIPTableVO.setContactName(rs.getString("lp_si_pri_poc"));
			hdWIPTableVO.setContactPhoneNumber(rs.getString("lp_si_phone_no"));
			hdWIPTableVO.setContactEmailAddress(rs
					.getString("lp_si_pri_email_id"));
			hdWIPTableVO.setTicketId(rs.getString("lm_hd_tc_id"));
			hdWIPTableVO.setPriority(rs.getString("lm_hd_priority"));
			hdWIPTableVO.setNextAction(rs.getString("lm_na_next_action"));
			hdWIPTableVO.setNextActionDueDateDB(rs
					.getTimestamp("lm_hd_next_action_due"));
			hdWIPTableVO.setCca_tss(rs.getString("lo_pc_last_name"));
			hdWIPTableVO.setShortDescription(rs
					.getString("lm_hd_short_description"));
			hdWIPTableVO.setIlexNumber(rs.getString("lm_tc_number"));
			hdWIPTableVO.setSessionId(rs.getString("lm_hd_session_id"));
			hdWIPTableVO.setNextActionId(rs.getString("lm_hd_next_action"));
			hdWIPTableVO.setDevice(rs.getString("lm_hd_circuit_status"));
			hdWIPTableVO.setTicketState(rs.getString("lm_hd_status"));
			hdWIPTableVO.setCircuitStatus(rs.getString("wug_ip_backup_status"));
			hdWIPTableVO.setTicketSource(rs.getString("lm_sr_id"));
			hdWIPTableVO.setPrimaryState(rs.getString("wug_ip_sStateName"));
			hdWIPTableVO.setAppendixName(rs.getString("lx_pr_title"));
			hdWIPTableVO.setNextActionOverDue(rs
					.getBoolean("lm_hd_next_action_overdue"));
			hdWIPTableVO.setExtSystemStatus(rs
					.getBoolean("lm_hd_ext_system_status"));
			hdWIPTableVO
					.setAssignmentGroupName(rs.getString("lm_hd_agm_name") == null ? ""
							: rs.getString("lm_hd_agm_name"));
			hdWIPTableVO.setDateDiff(rs.getString("dateDiff"));
			hdWIPTableVO.setUnderReview(rs.getString("lm_hd_under_review"));
			hdWIPTableVO.setTicketNumber(rs.getString("lm_tc_number"));
			hdWIPTableVO.setMmId(rs.getString("lp_mm_id"));
			hdWIPTableVO.setClientType(rs.getString("clientType"));
			hdWIPTableVO.setEventColor(rs.getString("lm_hd_ecl_color"));
			hdWIPTableVO.setSite_prop(rs.getString("lp_sl_priority_store"));
			return hdWIPTableVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getActiveTickets(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<HDWIPTableVO> getActiveTickets(String userId, String sessionId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select isnull(wug_ip_name ,lp_si_number) as 'lp_si_number', lo_ot_name, lm_hd_tc_id,lm_hd_priority,lm_hd_next_action_due,wug_ip_sStateName,wug_ip_backup_status,lm_hd_next_action_overdue");
		sqlQuery.append(" from ( SELECT * FROM lm_help_desk_ticket_details WHERE lm_hd_assigned_to = ? and lm_hd_session_id is not null) as a");
		sqlQuery.append(" join lp_site_list on lm_hd_lp_sl_id = lp_sl_id");
		sqlQuery.append(" join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id");
		sqlQuery.append(" join lo_organization_top on lp_mm_ot_id = lo_ot_id");
		sqlQuery.append(" left outer join wug_outages_incidents_in_process on lm_hd_tc_id = wug_ip_tc_id ");

		Object[] params = new Object[] { userId };

		List<HDWIPTableVO> activeTicketsData = jdbcTemplateIlex.query(
				sqlQuery.toString(), params,
				new HDWipTableMapperActiveTickets());

		return activeTicketsData;
	}

	/**
	 * The Class HDWipTableMapperActiveTickets.
	 */
	private static final class HDWipTableMapperActiveTickets implements
			RowMapper<HDWIPTableVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDWIPTableVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			HDWIPTableVO hdWIPTableVO = new HDWIPTableVO();
			hdWIPTableVO.setSiteNumber(rs.getString("lp_si_number"));
			hdWIPTableVO.setCustomer(rs.getString("lo_ot_name"));
			hdWIPTableVO.setTicketId(rs.getString("lm_hd_tc_id"));
			hdWIPTableVO.setPriority(rs.getString("lm_hd_priority"));
			hdWIPTableVO.setNextActionDueDateDB(rs
					.getTimestamp("lm_hd_next_action_due"));
			hdWIPTableVO.setCircuitStatus(rs.getString("wug_ip_backup_status"));
			hdWIPTableVO.setPrimaryState(rs.getString("wug_ip_sStateName"));
			hdWIPTableVO.setNextActionOverDue(rs
					.getBoolean("lm_hd_next_action_overdue"));
			return hdWIPTableVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#saveEfforts(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void saveEfforts(String ticketId, String effortValue,
			Boolean ppsFlag, String userId, Boolean isBillable) {

		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("insert into lm_help_desk_effort( ");
		sqlQuery.append(" lm_ef_tc_id, lm_ef_effort, lm_ef_date_time, lm_ef_non_pps_flag, lm_ef_recorded_by,lm_ef_billable ) ");
		sqlQuery.append(" values(?,?, getdate(),?,?,?)");

		Object[] params = new Object[] { ticketId, effortValue, ppsFlag,
				userId, isBillable };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

		sqlQuery = new StringBuilder("update lm_help_desk_ticket_details set ");
		sqlQuery.append(" lm_hd_assigned_to=?");
		sqlQuery.append(" where lm_hd_tc_id=?");

		params = new Object[] { userId, ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);
	}

	// //////////////

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#saveEffortsforHD(java.lang.String,
	 * java.lang.String, java.lang.Boolean, java.lang.String, java.lang.Boolean)
	 */
	@Override
	public void saveEffortsforHD(String ticketId, String effortStartDate,
			Boolean ppsFlag, String userId, Boolean isBillable) {

		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		Date parsedDate = null;
		try {
			parsedDate = format.parse(effortStartDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		java.sql.Timestamp sqlDate = new java.sql.Timestamp(
				parsedDate.getTime());
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("insert into lm_help_desk_effort( ");
		sqlQuery.append(" lm_ef_tc_id, lm_ef_effort, lm_ef_date_time, lm_ef_non_pps_flag, lm_ef_recorded_by,lm_ef_billable ) ");
		sqlQuery.append(" values(?,?, ?,?,?,?)");

		Object[] params = new Object[] { ticketId, 0, sqlDate, ppsFlag, userId,
				isBillable };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

		sqlQuery = new StringBuilder("update lm_help_desk_ticket_details set ");
		sqlQuery.append(" lm_hd_assigned_to=?");
		sqlQuery.append(" where lm_hd_tc_id=?");

		params = new Object[] { userId, ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);
	}

	/**
	 * The Constant AVERAGE_TIME_IN_MINUTES.
	 */
	private final static double AVERAGE_TIME_IN_MINUTES = 365.24 * 24 * 60 * 60;

	@Override
	public String getEffortStatus(String ticketId) {

		String effortsts = "stop";
		String qry = "select TOP 1 lm_ef_stop_date_time from lm_help_desk_effort where lm_ef_tc_id= "
				+ Integer.parseInt(ticketId) + " order by lm_ef_id desc";
		Date sqlEffortValue = null;
		try {
			sqlEffortValue = jdbcTemplateIlex.queryForObject(qry.toString(),
					new RowMapper<Date>() {
						public Date mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							return rs.getDate("lm_ef_stop_date_time");
						};
					});
		} catch (Exception e) {

			System.out.println(e.getMessage());
		}

		if (sqlEffortValue != null) {
			effortsts = "restart";

		}

		return effortsts;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#updateEfforts(java.lang.String,
	 * java.lang.String, java.lang.Boolean, java.lang.String, java.lang.Boolean)
	 */
	@Override
	public String updateEfforts(String ticketId, String effortEndDate,
			Boolean ppsFlag, String userId, Boolean isBillable) {
		String effortValue = "0";
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		Date parsedDate = null;
		try {
			parsedDate = format.parse(effortEndDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		java.sql.Timestamp sqlstartDate = null;

		java.sql.Timestamp sqlEndDate = new java.sql.Timestamp(
				parsedDate.getTime());
		String qry = "select TOP 1 lm_ef_date_time from lm_help_desk_effort where lm_ef_tc_id= "
				+ Integer.parseInt(ticketId)
				+ " and lm_ef_effort = 0 order by lm_ef_id desc";

		try {
			sqlstartDate = jdbcTemplateIlex.queryForObject(qry.toString(),
					new RowMapper<java.sql.Timestamp>() {
						public java.sql.Timestamp mapRow(ResultSet rs,
								int rowNum) {
							try {
								return rs.getTimestamp("lm_ef_date_time");
							} catch (SQLException e) {
								e.printStackTrace();
								return null;
							}
						};
					});
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (sqlstartDate != null && sqlstartDate.compareTo(sqlEndDate) < 0) {
			effortValue = String.valueOf((sqlEndDate.getTime() - sqlstartDate
					.getTime()) / 60000);
		}

		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("update lm_help_desk_effort set ");
		sqlQuery.append("lm_ef_effort = ?, lm_ef_stop_date_time = ?  ");
		sqlQuery.append(" where lm_ef_tc_id = ? and lm_ef_effort = 0 and lm_ef_date_time = ? ");

		Object[] params = new Object[] { effortValue, sqlEndDate, ticketId,
				sqlstartDate };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

		sqlQuery = new StringBuilder("update lm_help_desk_ticket_details set ");
		sqlQuery.append(" lm_hd_assigned_to=?");
		sqlQuery.append(" where lm_hd_tc_id=?");

		params = new Object[] { userId, ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

		return effortValue;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#saveWorkNotes(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void saveWorkNotes(String ticketId, String workNotes, String userId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("insert into lm_help_desk_work_notes (");
		sqlQuery.append("lm_wn_tc_id, lm_wn_note, lm_wn_type, lm_wn_dt, lm_wn_user )");

		sqlQuery.append(" values (?,?,0,getdate(),?) ");
		Object[] params = new Object[] { ticketId, workNotes, userId };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getCubeStatus()
	 */
	@Override
	public HDCubeStatusVO getCubeStatus() {
		StringBuilder sqlQuery = new StringBuilder(
				"select lm_cb_update_count,lm_cb_name from lm_help_desk_cube_display_status");

		HDCubeStatusVO hdCubeStatusVO = jdbcTemplateIlex.query(
				sqlQuery.toString(), new CubeStatusResultSetExtractor());

		return hdCubeStatusVO;
	}

	/**
	 * The Class CubeStatusResultSetExtractor.
	 */
	private static final class CubeStatusResultSetExtractor implements
			ResultSetExtractor<HDCubeStatusVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.ResultSetExtractor#extractData(java
		 * .sql.ResultSet)
		 */
		@Override
		public HDCubeStatusVO extractData(ResultSet rs) throws SQLException,
				DataAccessException {

			HDCubeStatusVO hdCubeStatusVO = new HDCubeStatusVO();
			while (rs.next()) {
				switch (rs.getString("lm_cb_name")) {
				case "PA-Alert":
					hdCubeStatusVO.setPendingAckAlertCubeStatus(rs
							.getString("lm_cb_update_count"));
					break;
				case "PA-Customer":
					hdCubeStatusVO.setPendingAckCustCubeStatus(rs
							.getString("lm_cb_update_count"));
					break;
				case "PA-Monitor":
					hdCubeStatusVO.setPendingAckMonitorCubeStatus(rs
							.getString("lm_cb_update_count"));
					break;
				case "WIP":
					hdCubeStatusVO.setWipCubeStatus(rs
							.getString("lm_cb_update_count"));
					break;
				case "WIP-OD":
					hdCubeStatusVO.setWipODCubeStatus(rs
							.getString("lm_cb_update_count"));
					break;
				}
			}
			return hdCubeStatusVO;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getTicketStates()
	 */
	@Override
	public List<TicketStateVO> getTicketStates() {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select lm_st_id, lm_st_status");
		sqlQuery.append(" from lm_help_desk_ticket_status");
		sqlQuery.append(" where lm_st_active = 1");
		sqlQuery.append(" and lm_st_display_on_active_form = 1");
		sqlQuery.append(" order by lm_st_display_on_active_form_order");

		List<TicketStateVO> ticketStateVOList = jdbcTemplateIlex.query(
				sqlQuery.toString(), new RowMapper<TicketStateVO>() {
					public TicketStateVO mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						TicketStateVO ticketStateVO = new TicketStateVO();

						ticketStateVO.setStateId(rs.getString("lm_st_id"));
						ticketStateVO.setTicketState(rs
								.getString("lm_st_status"));

						return ticketStateVO;
					};
				});

		return ticketStateVOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getCircuitStatus()
	 */
	@Override
	public List<CircuitStatusVO> getCircuitStatus() {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select lm_cs_id, lm_cs_circuit_status ");
		sqlQuery.append(" from lm_help_desk_circuit_status");
		sqlQuery.append(" order by lm_cs_circuit_status");

		List<CircuitStatusVO> circuitStatusVOList = jdbcTemplateIlex.query(
				sqlQuery.toString(), new RowMapper<CircuitStatusVO>() {
					public CircuitStatusVO mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						CircuitStatusVO circuitStatusVO = new CircuitStatusVO();

						circuitStatusVO.setCircuitId(rs.getString("lm_cs_id"));
						circuitStatusVO.setCircuitStatus(rs
								.getString("lm_cs_circuit_status"));

						return circuitStatusVO;
					};
				});

		return circuitStatusVOList;
	}

	@Override
	public String getSiteNumber(String SiteID) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select  lp_si_number from lp_site_list where lp_sl_id = ?");

		Object[] params = new Object[] { SiteID };

		String siteNum = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
				params, String.class);

		return siteNum;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#updateState(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void manageTicketDetailStatus(String ticketId, String state,
			String userId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select  lm_hd_status from lm_help_desk_ticket_details where lm_hd_tc_id = ?");

		Object[] params = new Object[] { ticketId };

		int oldStatus = jdbcTemplateIlex.queryForInt(sqlQuery.toString(),
				params);

		sqlQuery = new StringBuilder();
		sqlQuery.append("update lm_help_desk_ticket_details");
		sqlQuery.append(" set lm_hd_status = ?,");
		sqlQuery.append(" lm_hd_update_date = getdate(),");
		sqlQuery.append(" lm_hd_updated_by = ?,");
		sqlQuery.append(" lm_hd_assigned_to=?");
		sqlQuery.append(" where lm_hd_tc_id = ?");

		params = new Object[] { state, userId, userId, ticketId };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

		HDHistoryVO hdHistoryVO = new HDHistoryVO();
		hdHistoryVO.setEvent("16");
		hdHistoryVO.setFromData(null);
		hdHistoryVO.setFromReference(String.valueOf(oldStatus));
		hdHistoryVO.setInitiator(userId);
		hdHistoryVO.setTicketId(Long.valueOf(ticketId));
		hdHistoryVO.setToData(null);
		hdHistoryVO.setToReference(state);

		manageChangeHistory(hdHistoryVO);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#updateCircuitStatus(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void updateCircuitStatus(String ticketId, String cktStatus,
			String userId) {

		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("update lm_help_desk_ticket_details");
		sqlQuery.append(" set lm_hd_circuit_status = ?,");
		sqlQuery.append(" lm_hd_update_date = getdate(),");
		sqlQuery.append(" lm_hd_updated_by = ?");
		sqlQuery.append(" where lm_hd_tc_id = ?");

		Object[] params = new Object[] { cktStatus, userId, ticketId };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getTicketSummary(java.lang.Long)
	 */
	@Override
	public HDTicketVO getTicketSummary(Long ticketId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select lp_si_number, lo_ot_name, lm_hd_provider_ref, lp_si_address, lp_si_city, lp_si_state,lp_si_country, lp_si_zip_code, lm_hd_opened, ");
		sqlQuery.append(" lm_hd_tc_id,lm_hd_priority,lm_hd_customer_identifier,lm_hd_category,");
		sqlQuery.append(" lx_pr_title,lm_hd_caller_phone,lm_tc_requestor_email as lm_hd_caller_email,lm_hd_billable,lm_hd_ci,");
		sqlQuery.append(" lm_hd_caller_name,lm_hd_short_description, lm_tc_number,lm_tc_problem_desc, lm_hd_impact,lm_hd_urgency,");
		sqlQuery.append(" lm_hd_circuit_status,wug_ip_backup_status, lm_hd_point_of_failure,wug_io_desc, lm_hd_root_cause,wug_rc_desc, ");
		sqlQuery.append(" lm_hd_resolution,wug_rs_desc, lm_ty_type,lm_ci_config_item,lm_ct_category,lm_sr_id,lm_st_status,lm_hd_complete_first_call,");
		sqlQuery.append(" wug_io_id,wug_rs_id,wug_rc_id,lp_si_notes,lm_hd_lp_sl_id,isnull(wug_ip_sStateName, 'Up') as wug_ip_sStateName,");
		sqlQuery.append(" lm_hd_next_action,lm_hd_next_action_due,lm_hd_lx_pr_id,lm_hd_lp_mm_id,lm_hd_lp_sl_id, lm_hd_assign_group_id, lm_hd_contact_type_id "); // ,
		// lx_pr_email");
		// lx_pr_email
		// sqlQuery.append(" ,isnull(lp_si_clientHD_phone_no, '') lp_si_clientHD_phone_no, ");
		// sqlQuery.append(" isnull(lp_si_clientHD_name, '') lp_si_clientHD_name, isnull(lp_si_clientHD_email, '') lp_si_clientHD_email ");
		sqlQuery.append(" from ( SELECT * FROM lm_help_desk_ticket_details WHERE lm_hd_tc_id = ? ) as a");
		sqlQuery.append(" join lp_site_list on lm_hd_lp_sl_id = lp_sl_id");
		sqlQuery.append(" join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id");
		sqlQuery.append(" join lo_organization_top on lp_mm_ot_id = lo_ot_id");
		sqlQuery.append(" join lx_appendix_main on lm_hd_lx_pr_id= lx_pr_id");
		sqlQuery.append(" join lm_help_desk_ticket_sources on lm_hd_source = lm_sr_id");
		sqlQuery.append(" left outer join wug_outages_incidents_in_process on wug_ip_tc_id = lm_hd_tc_id");
		sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id");
		sqlQuery.append(" left join lo_poc on lm_hd_assigned_to = lo_pc_id");
		sqlQuery.append(" join lm_help_desk_ticket_types on lm_ty_id=lm_hd_type");
		sqlQuery.append(" join lm_help_desk_ticket_config_items on lm_ci_id = lm_hd_ci");
		sqlQuery.append(" join lm_help_desk_ticket_categories on lm_ct_id = lm_hd_category");
		sqlQuery.append(" left outer join wug_issue_owner on wug_io_id= lm_hd_point_of_failure");
		sqlQuery.append(" left outer join wug_resolution on wug_rs_id= lm_hd_resolution");
		sqlQuery.append(" left outer join wug_root_cause on wug_rc_id= lm_hd_root_cause");
		sqlQuery.append(" join lm_help_desk_ticket_status on lm_hd_status = lm_st_id");

		Object[] params = new Object[] { ticketId };

		HDTicketVO hdTicketVO = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), params, new TicketSummaryMapper());

		return hdTicketVO;
	}

	/*
	 * add query for helpdesk Additional Recipitant
	 */
	@Override
	public List<IncidentStateVO> getEmailAddress(Long appendixId,
			String chkstatus) {

		String sql = "SELECT lx_pcc_name, lx_pcc_email "
				+ " FROM lx_appendix_client_contact  "
				+ " WHERE lx_pcc_type = '" + chkstatus + "' "
				+ " AND lx_pcc_email IS NOT NULL  "
				+ " AND lx_pcc_appendix_id =  " + appendixId;

		List<LabelValue> list = new ArrayList<LabelValue>();

		List<IncidentStateVO> incidentStateList = jdbcTemplateIlex.query(sql,
				new EmailMapper());
		return incidentStateList;

	}

	private static final class EmailMapper implements
			RowMapper<IncidentStateVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public IncidentStateVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			IncidentStateVO labelValue = new IncidentStateVO();
			labelValue.setLabel(rs.getString("lx_pcc_name") + ": "
					+ rs.getString("lx_pcc_email"));
			labelValue.setValue(rs.getString("lx_pcc_email"));
			// labelValue.setEnabled(1);
			return labelValue;
		}
	}

	/**
	 * The Class TicketSummaryMapper.
	 */
	private static final class TicketSummaryMapper implements
			RowMapper<HDTicketVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDTicketVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			HDTicketVO hdTicketVO = new HDTicketVO();
			hdTicketVO.setSiteNumber(rs.getString("lp_si_number"));
			hdTicketVO.setCustomerName(rs.getString("lo_ot_name"));
			hdTicketVO.setSiteAddress(rs.getString("lp_si_address"));
			hdTicketVO.setSiteCity(rs.getString("lp_si_city"));
			hdTicketVO.setSiteState(rs.getString("lp_si_state"));
			hdTicketVO.setSiteCountry(rs.getString("lp_si_country"));
			hdTicketVO.setSiteZipcode(rs.getString("lp_si_zip_code"));
			hdTicketVO.setTicketId(rs.getString("lm_hd_tc_id"));
			hdTicketVO.setPriority(rs.getString("lm_hd_priority"));
			hdTicketVO.setCustomerTicketReference(rs
					.getString("lm_hd_customer_identifier"));
			hdTicketVO.setCategory(rs.getString("lm_ct_category"));
			hdTicketVO.setAppendixName(rs.getString("lx_pr_title"));
			hdTicketVO.setCallerPhone(rs.getString("lm_hd_caller_phone"));
			hdTicketVO.setCallerEmail(rs.getString("lm_hd_caller_email"));
			hdTicketVO.setCallerName(rs.getString("lm_hd_caller_name"));
			hdTicketVO.setBillingStatus(rs.getBoolean("lm_hd_billable"));
			hdTicketVO.setConfigurationItem(rs.getString("lm_ci_config_item"));
			hdTicketVO.setShortDescription(rs
					.getString("lm_hd_short_description"));
			hdTicketVO.setTicketNumber(rs.getString("lm_tc_number"));
			hdTicketVO
					.setProblemDescription(rs.getString("lm_tc_problem_desc"));
			hdTicketVO.setImpact(rs.getString("lm_hd_impact"));
			hdTicketVO.setUrgency(rs.getString("lm_hd_urgency"));
			hdTicketVO.setBackUpCktStatus((StringUtils.isBlank(rs
					.getString("lm_hd_circuit_status")) ? rs
					.getString("wug_ip_backup_status") : rs
					.getString("lm_hd_circuit_status")));

			if (!StringUtils.isBlank(hdTicketVO.getBackUpCktStatus())) {
				if ("0".equals(hdTicketVO.getBackUpCktStatus())) {
					hdTicketVO.setDevice("Down");
				} else if ("1".equals(hdTicketVO.getBackUpCktStatus())) {
					hdTicketVO.setDevice("Up");
				} else {

					hdTicketVO.setDevice(rs.getString("wug_ip_backup_status"));
				}
			}
			hdTicketVO.setPointOfFailure(rs.getString("wug_io_desc"));
			hdTicketVO.setRootCause(rs.getString("wug_rc_desc"));
			hdTicketVO.setResolution(rs.getString("wug_rs_desc"));
			hdTicketVO.setTicketType(rs.getString("lm_ty_type"));
			hdTicketVO.setTicketSource(rs.getString("lm_sr_id"));
			hdTicketVO.setTicketStatus(rs.getString("lm_st_status"));
			if (rs.getString("lm_hd_complete_first_call") != null) {
				hdTicketVO.setLmCompleteFirstCall(rs
						.getBoolean("lm_hd_complete_first_call"));
			}
			hdTicketVO.setPointOfFailureId(rs.getString("wug_io_id"));
			hdTicketVO.setRootCauseId(rs.getString("wug_rc_id"));
			hdTicketVO.setResolutionId(rs.getString("wug_rs_id"));
			hdTicketVO.setSiteWorkNotes(rs.getString("lp_si_notes"));
			hdTicketVO.setSiteId(rs.getString("lm_hd_lp_sl_id"));
			hdTicketVO.setPrimaryState(rs.getString("wug_ip_sStateName"));
			hdTicketVO.setNextAction(rs.getString("lm_hd_next_action"));
			hdTicketVO
					.setNextActionDueDate(rs.getDate("lm_hd_next_action_due"));
			hdTicketVO.setAppendixId(rs.getLong("lm_hd_lx_pr_id"));
			hdTicketVO.setMsaId(rs.getLong("lm_hd_lp_mm_id"));
			hdTicketVO.setSiteId(rs.getString("lm_hd_lp_sl_id"));
			hdTicketVO.setAssignmentGroupId(rs
					.getString("lm_hd_assign_group_id") == null ? "0" : rs
					.getString("lm_hd_assign_group_id"));
			hdTicketVO
					.setContactTypeId(rs.getString("lm_hd_contact_type_id") == null ? "0"
							: rs.getString("lm_hd_contact_type_id"));
			hdTicketVO.setCreatedOnDate(rs.getDate("lm_hd_opened"));
			if (hdTicketVO.getCreatedOnDate() != null) {
				SimpleDateFormat format = new SimpleDateFormat(
						"MM/dd/yyyy hh:mm aaa");
				hdTicketVO.setCreatedOnDateString(format.format(hdTicketVO
						.getCreatedOnDate()));
			}

			/* To save the last values to generate the email body */
			hdTicketVO.setLastCreateTicketNextActionDate(hdTicketVO
					.getNextActionDueDate().toString());
			hdTicketVO.setLastNextAction(hdTicketVO.getNextAction());
			hdTicketVO.setLastNextActionDueDate(hdTicketVO
					.getNextActionDueDate().toString());
			hdTicketVO.setLastTicketStatus(hdTicketVO.getTicketStatus());
			hdTicketVO.setProviderRefNo(rs.getString("lm_hd_provider_ref"));

			// hdTicketVO.setClientHDContactName(rs
			// .getString("lp_si_clientHD_name"));
			// hdTicketVO.setClientHDEmailAddress(rs
			// .getString("lp_si_clientHD_email"));
			// hdTicketVO.setClientHDPhoneNumber(rs
			// .getString("lp_si_clientHD_phone_no"));
			// hdTicketVO.setEmailAddress(rs.getString("lx_pr_email"));
			/* To save the last values to generate the email body */
			return hdTicketVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getWorkNotes(java.lang.Long)
	 */
	@Override
	public List<HDMonitoringTimeVO> getDateTimeListForMonitoringDAO(
			Long ticketId) {
		StringBuilder sqlQuery = new StringBuilder(
				"SELECT lm_hd_tmh_id, lm_hd_tmh_start_date, isnull(lm_hd_tmh_end_date, getDate()) lm_hd_tmh_end_date,  ");
		sqlQuery.append(" isnull(datediff(minute, lm_hd_tmh_start_date, isnull(lm_hd_tmh_end_date, getDate())), 0) AS diff ");
		sqlQuery.append(" FROM lm_help_desk_ticket_monitoring_history WHERE lm_hd_tmh_tc_id = ? ");
		sqlQuery.append(" ORDER BY lm_hd_tmh_id ");

		Object[] params = new Object[] { ticketId };

		List<HDMonitoringTimeVO> hdMonitoringTimeDTOLst = jdbcTemplateIlex
				.query(sqlQuery.toString(), params,
						new RowMapper<HDMonitoringTimeVO>() {

							@Override
							public HDMonitoringTimeVO mapRow(ResultSet rs,
									int rowNum) throws SQLException {
								HDMonitoringTimeVO hdMonitoringTimeVO = new HDMonitoringTimeVO();

								hdMonitoringTimeVO.setStartDate(rs
										.getString("lm_hd_tmh_start_date"));
								hdMonitoringTimeVO.setEndDate(rs
										.getString("lm_hd_tmh_end_date"));
								hdMonitoringTimeVO.setDateDiff(rs
										.getString("diff"));
								float val = Float.parseFloat(rs
										.getString("diff"));
								if (val >= 60) {
									val = val / 60;
									DecimalFormat df = new DecimalFormat();
									df.setMaximumFractionDigits(2);
									hdMonitoringTimeVO.setDateDiff(df
											.format(val) + " H");
								} else {
									hdMonitoringTimeVO.setDateDiff(val + " M");
								}

								return hdMonitoringTimeVO;
							}
						});

		return hdMonitoringTimeDTOLst;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getWorkNotes(java.lang.Long)
	 */
	@Override
	public List<HDWorkNoteVO> getWorkNotes(Long ticketId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select lm_wn_note, lm_wn_dt,LEFT(lo_pc_first_name, 1)+' '+lo_pc_last_name lo_pc_last_name");
		sqlQuery.append(" from (SELECT * FROM lm_help_desk_work_notes WHERE lm_wn_tc_id = ?) as a");
		sqlQuery.append(" left outer join lo_poc on lo_pc_id = lm_wn_user");
		sqlQuery.append(" order by  CONVERT(DATETIME , lm_wn_dt)  DESC");

		Object[] params = new Object[] { ticketId };

		List<HDWorkNoteVO> hdWorkNoteVOList = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new RowMapper<HDWorkNoteVO>() {

					@Override
					public HDWorkNoteVO mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						HDWorkNoteVO hdWorkNoteVO = new HDWorkNoteVO();

						hdWorkNoteVO.setWorkNote(rs.getString("lm_wn_note"));
						hdWorkNoteVO.setWorkNoteDate(rs
								.getTimestamp("lm_wn_dt"));
						hdWorkNoteVO.setUserLastName(rs
								.getString("lo_pc_last_name"));

						return hdWorkNoteVO;
					}
				});

		return hdWorkNoteVOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getTicketDetailsForEdit(java.lang.String)
	 */
	// get install Notes
	@Override
	public List<HDInstallNoteVO> getInstallNotes(Long ticketId) {

		StringBuilder sqlQuery = new StringBuilder(
				"SELECT  lm_in_cdate, lm_in_notes from  lm_install_notes");
		sqlQuery.append(" INNER JOIN lm_ticket ON lm_tc_js_id = lm_in_js_id ");
		sqlQuery.append(" WHERE lm_tc_id = ?  ");
		sqlQuery.append(" order by CONVERT(DATETIME , lm_in_cdate) desc");

		Object[] params = new Object[] { ticketId };

		List<HDInstallNoteVO> hdInstallNoteVOList = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new RowMapper<HDInstallNoteVO>() {

					@Override
					public HDInstallNoteVO mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						HDInstallNoteVO hdInstallNoteVO = new HDInstallNoteVO();
						hdInstallNoteVO.setInstallNoteDate(rs
								.getTimestamp("lm_in_cdate"));
						hdInstallNoteVO.setInstallNote(rs
								.getString("lm_in_notes"));

						return hdInstallNoteVO;
					}
				});

		return hdInstallNoteVOList;
	}

	@Override
	public HDTicketVO getTicketDetailsForEdit(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder(
				"select lm_hd_tc_id,lm_hd_customer_identifier,lm_hd_category,");
		sqlQuery.append(" lm_hd_caller_phone,lm_hd_caller_email,lm_hd_billable,lm_hd_ci,");
		sqlQuery.append(" lm_hd_caller_name,lm_hd_short_description, lm_tc_problem_desc");
		sqlQuery.append(" from ( SELECT * FROM lm_help_desk_ticket_details where lm_hd_tc_id = ?) as a");
		sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id");

		Object[] params = new Object[] { ticketId };

		HDTicketVO hdTicketVO = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), params, new EditTicketDetailsMapper());

		return hdTicketVO;
	}

	/**
	 * The Class EditTicketDetailsMapper.
	 */
	private static final class EditTicketDetailsMapper implements
			RowMapper<HDTicketVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDTicketVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			HDTicketVO hdTicketVO = new HDTicketVO();
			hdTicketVO.setTicketId(rs.getString("lm_hd_tc_id"));
			hdTicketVO.setCustomerTicketReference(rs
					.getString("lm_hd_customer_identifier"));
			hdTicketVO.setCategory(rs.getString("lm_hd_category"));
			hdTicketVO.setCallerPhone(rs.getString("lm_hd_caller_phone"));
			hdTicketVO.setCallerEmail(rs.getString("lm_hd_caller_email"));
			hdTicketVO.setCallerName(rs.getString("lm_hd_caller_name"));
			hdTicketVO.setBillingStatus(rs.getBoolean("lm_hd_billable"));
			hdTicketVO.setConfigurationItem(rs.getString("lm_hd_ci"));
			hdTicketVO.setShortDescription(rs
					.getString("lm_hd_short_description"));
			hdTicketVO
					.setProblemDescription(rs.getString("lm_tc_problem_desc"));

			return hdTicketVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#editTicketDetails(com.ilex.ws.core.bean
	 * .HDTicketVO)
	 */
	@Override
	public void editTicketDetails(HDTicketVO hdTicketVO) {
		StringBuilder sqlQuery = new StringBuilder(
				"update lm_help_desk_ticket_details set ");
		sqlQuery.append(" lm_hd_caller_name=?,");
		sqlQuery.append(" lm_hd_caller_phone=?,");
		sqlQuery.append(" lm_hd_caller_email=?,");
		sqlQuery.append(" lm_hd_category=?,");
		sqlQuery.append(" lm_hd_short_description=?,");
		sqlQuery.append(" lm_hd_ci=?,");
		sqlQuery.append(" lm_hd_customer_identifier=?,");
		sqlQuery.append(" lm_hd_billable=?");
		sqlQuery.append(" where lm_hd_tc_id=?");

		Object[] params = new Object[] { hdTicketVO.getCallerName(),
				hdTicketVO.getCallerPhone(), hdTicketVO.getCallerEmail(),
				hdTicketVO.getCategory(), hdTicketVO.getShortDescription(),
				hdTicketVO.getConfigurationItem(),
				hdTicketVO.getCustomerTicketReference(),
				hdTicketVO.getBillingStatus(), hdTicketVO.getTicketId() };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

		sqlQuery = new StringBuilder("update lm_ticket set ");
		sqlQuery.append(" lm_tc_problem_desc=?");
		sqlQuery.append(" where lm_tc_id=?");

		params = new Object[] { hdTicketVO.getProblemDescription(),
				hdTicketVO.getTicketId() };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getPriorityDetailsForEdit(java.lang.String
	 * )
	 */
	@Override
	public HDTicketVO getPriorityDetailsForEdit(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder("select ");
		sqlQuery.append(" lm_hd_tc_id,lm_hd_priority,lm_hd_impact,lm_hd_urgency,lm_hd_lx_pr_id  from lm_help_desk_ticket_details");
		sqlQuery.append(" where lm_hd_tc_id = ?");

		Object[] params = new Object[] { ticketId };

		HDTicketVO hdTicketVO = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), params, new RowMapper<HDTicketVO>() {

					@Override
					public HDTicketVO mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						HDTicketVO hdTicketVO = new HDTicketVO();
						hdTicketVO.setPriority(rs.getString("lm_hd_priority"));
						hdTicketVO.setImpact(rs.getString("lm_hd_impact"));
						hdTicketVO.setUrgency(rs.getString("lm_hd_urgency"));
						hdTicketVO.setTicketId(rs.getString("lm_hd_tc_id"));
						hdTicketVO.setAppendixId(rs.getLong("lm_hd_lx_pr_id"));
						return hdTicketVO;
					}

				});

		return hdTicketVO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#editPrioritySection(com.ilex.ws.core.bean
	 * .HDTicketVO, java.lang.String)
	 */
	@Override
	public void editPrioritySection(HDTicketVO hdTicketVO, String userId) {
		int priority = getTicketPriority(
				Long.valueOf(hdTicketVO.getAppendixId()),
				Integer.valueOf(hdTicketVO.getImpact()),
				Integer.valueOf(hdTicketVO.getUrgency()));

		StringBuilder sqlQuery = new StringBuilder(
				"update lm_help_desk_ticket_details set ");
		sqlQuery.append(" lm_hd_priority=?,");
		sqlQuery.append(" lm_hd_impact=?,");
		sqlQuery.append(" lm_hd_urgency=?,");
		sqlQuery.append(" lm_hd_assigned_to=?");
		sqlQuery.append(" where lm_hd_tc_id=?");

		Object[] params = new Object[] { priority, hdTicketVO.getImpact(),
				hdTicketVO.getUrgency(), userId, hdTicketVO.getTicketId() };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getBackupDetailsForEdit(java.lang.String)
	 */
	@Override
	public HDTicketVO getBackupDetailsForEdit(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder("select ");
		sqlQuery.append(" lm_hd_tc_id,lm_hd_circuit_status,wug_ip_backup_status ");
		sqlQuery.append(" from ( SELECT * FROM lm_help_desk_ticket_details where lm_hd_tc_id = ?) as a");
		sqlQuery.append(" left outer join wug_outages_incidents_in_process on wug_ip_tc_id = lm_hd_tc_id");

		Object[] params = new Object[] { ticketId };

		HDTicketVO hdTicketVO = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), params, new RowMapper<HDTicketVO>() {

					@Override
					public HDTicketVO mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						HDTicketVO hdTicketVO = new HDTicketVO();
						hdTicketVO.setBackUpCktStatus(rs
								.getString("wug_ip_backup_status"));
						hdTicketVO.setDevice(rs
								.getString("lm_hd_circuit_status"));
						hdTicketVO.setTicketId(rs.getString("lm_hd_tc_id"));
						return hdTicketVO;
					}

				});

		return hdTicketVO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#editBackupDetails(com.ilex.ws.core.bean
	 * .HDTicketVO, java.lang.String)
	 */
	@Override
	public void editBackupDetails(HDTicketVO hdTicketVO, String userId) {
		StringBuilder sqlQuery = new StringBuilder(
				"update wug_outages_incidents_in_process set ");
		sqlQuery.append(" wug_ip_backup_status=?");
		sqlQuery.append(" where wug_ip_tc_id=?");

		Object[] params = new Object[] { hdTicketVO.getBackUpCktStatus(),
				hdTicketVO.getTicketId() };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

		sqlQuery = new StringBuilder("update lm_help_desk_ticket_details set ");
		sqlQuery.append(" lm_hd_assigned_to=?");
		sqlQuery.append(" where lm_hd_tc_id=?");

		params = new Object[] { userId, hdTicketVO.getTicketId() };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getResolutionDetailsForEdit(java.lang.
	 * String)
	 */
	@Override
	public HDTicketVO getResolutionDetailsForEdit(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder("select ");
		sqlQuery.append(" lm_hd_tc_id,lm_hd_point_of_failure,lm_hd_root_cause,lm_hd_resolution ");
		sqlQuery.append(" from lm_help_desk_ticket_details");
		sqlQuery.append(" where lm_hd_tc_id = ?");

		Object[] params = new Object[] { ticketId };

		HDTicketVO hdTicketVO = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), params, new RowMapper<HDTicketVO>() {

					@Override
					public HDTicketVO mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						HDTicketVO hdTicketVO = new HDTicketVO();
						hdTicketVO.setPointOfFailure(rs
								.getString("lm_hd_point_of_failure"));
						hdTicketVO.setRootCause(rs
								.getString("lm_hd_root_cause"));
						hdTicketVO.setResolution(rs
								.getString("lm_hd_resolution"));
						hdTicketVO.setTicketId(rs.getString("lm_hd_tc_id"));
						return hdTicketVO;
					}

				});

		return hdTicketVO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getPointOfFailureList()
	 */
	@Override
	public Map<String, String> getPointOfFailureList() {
		String sqlQuery = "select wug_io_id, wug_io_desc from wug_issue_owner";

		Map<String, String> pointOfFailureList = jdbcTemplateIlex.query(
				sqlQuery, new ResultSetExtractor<Map<String, String>>() {

					@Override
					public Map<String, String> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						Map<String, String> tempMap = new HashMap<>();
						while (rs.next()) {
							String wugId = rs.getString("wug_io_id");
							String wugDesc = rs.getString("wug_io_desc");
							tempMap.put(wugId, wugDesc);
						}
						return tempMap;
					}
				});
		return pointOfFailureList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getRootCauseList()
	 */
	@Override
	public Map<String, String> getRootCauseList() {
		String sqlQuery = "select wug_rc_id, wug_rc_desc from wug_root_cause";

		Map<String, String> rootCauseList = jdbcTemplateIlex.query(sqlQuery,
				new ResultSetExtractor<Map<String, String>>() {

					@Override
					public Map<String, String> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						Map<String, String> tempMap = new HashMap<>();
						while (rs.next()) {
							String wugId = rs.getString("wug_rc_id");
							String wugDesc = rs.getString("wug_rc_desc");
							tempMap.put(wugId, wugDesc);
						}
						return tempMap;
					}
				});
		return rootCauseList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getResolutionList()
	 */
	@Override
	public Map<String, String> getResolutionList() {
		String sqlQuery = "select wug_rs_id, wug_rs_desc from wug_resolution";

		Map<String, String> resolutionList = jdbcTemplateIlex.query(sqlQuery,
				new ResultSetExtractor<Map<String, String>>() {

					@Override
					public Map<String, String> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						Map<String, String> tempMap = new HashMap<>();
						while (rs.next()) {
							String wugId = rs.getString("wug_rs_id");
							String wugDesc = rs.getString("wug_rs_desc");
							tempMap.put(wugId, wugDesc);
						}
						return tempMap;
					}
				});
		return resolutionList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#editResolutionDetails(com.ilex.ws.core
	 * .bean.HDTicketVO, java.lang.String)
	 */
	@Override
	public void editResolutionDetails(HDTicketVO hdTicketVO, String userId) {
		StringBuilder sqlQuery = new StringBuilder(
				"update lm_help_desk_ticket_details set ");
		sqlQuery.append(" lm_hd_point_of_failure=?,");
		sqlQuery.append(" lm_hd_root_cause=?,");
		sqlQuery.append(" lm_hd_resolution=?,");
		sqlQuery.append(" lm_hd_assigned_to=?");
		sqlQuery.append(" where lm_hd_tc_id=?");

		Object[] params = new Object[] { hdTicketVO.getPointOfFailure(),
				hdTicketVO.getRootCause(), hdTicketVO.getResolution(), userId,
				hdTicketVO.getTicketId() };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getStateTail(java.lang.String)
	 */
	@Override
	public StateTailVO getStateTail(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select  ceiling(sum(cast(wug_ip_outage_duration AS float))/60) as downMinutes from wug_outages_incidents_in_process where  wug_ip_tc_id =?");
		Object[] params = new Object[] { ticketId };
		Long downMinutes = jdbcTemplateIlex.queryForLong(sqlQuery.toString(),
				params);

		sqlQuery = new StringBuilder();
		sqlQuery.append("select top(25) wug_ih_event_time, wug_ih_to_sStateName, wug_ih_backup_status");
		sqlQuery.append(" from ( SELECT * FROM lm_help_desk_ticket_details where lm_hd_tc_id = ?) as hd");
		sqlQuery.append(" join wug_outages_incidents_in_process a on lm_hd_tc_id = wug_ip_tc_id");
		sqlQuery.append(" join wug_outages_incidents_in_process_history b on a.wug_ip_nActiveMonitorStateChangeID = b.wug_ip_nActiveMonitorStateChangeID");
		sqlQuery.append(" order by CONVERT(DATETIME , wug_ih_event_time)  desc");

		List<WugDetailsVO> wugDetailsVOList = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new RowMapper<WugDetailsVO>() {

					@Override
					public WugDetailsVO mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						WugDetailsVO wugDetailsVO = new WugDetailsVO();
						wugDetailsVO.setBackUpStatus(rs.getString(3));
						wugDetailsVO.setWugDateDB(rs
								.getTimestamp("wug_ih_event_time"));
						wugDetailsVO.setWugState(rs
								.getString("wug_ih_to_sStateName"));
						return wugDetailsVO;
					}
				});

		StateTailVO stateTailVO = new StateTailVO();
		stateTailVO.setDownMinutes(downMinutes);
		stateTailVO.setWugDetailsVOList(wugDetailsVOList);

		return stateTailVO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getTotalEffort(java.lang.Long)
	 */
	@Override
	public Double getTotalEffort(Long ticketId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select isnull(sum (lm_ef_effort), 0.0)");
		sqlQuery.append(" from lm_help_desk_effort");
		sqlQuery.append(" where lm_ef_tc_id = ?");

		Object[] params = new Object[] { ticketId };
		Double sumOfEfforts = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), params, Double.class);

		return sumOfEfforts;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getBillableStatus(java.lang.Long)
	 */
	@Override
	public Boolean getBillableStatus(Long ticketId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select lm_hd_billable from lm_help_desk_ticket_details");
		sqlQuery.append(" where lm_hd_tc_id = ?");

		Object[] params = new Object[] { ticketId };

		Boolean billingStatus = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), params, Boolean.class);

		return billingStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getEffectiveDateforPPS(java.lang.Long)
	 */
	@Override
	public Date getEffectiveDateforPPS(Long ticketId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select dateadd(hour, isnull(convert(numeric(10), cc_utc), -5)+5, getdate())");
		sqlQuery.append(" from ( SELECT * FROM lm_help_desk_ticket_details where lm_hd_tc_id = ?) as a");
		sqlQuery.append(" join dbo.lp_site_list on lm_hd_lp_sl_id = lp_sl_id");
		sqlQuery.append(" join dbo.cc_time_zone on lp_si_time_zone = cc_tz_id");

		Object[] params = new Object[] { ticketId };
		Date effectiveDate;
		try {
			effectiveDate = jdbcTemplateIlex.queryForObject(
					sqlQuery.toString(), params, Date.class);
		} catch (EmptyResultDataAccessException e) {
			effectiveDate = null;
		}

		return effectiveDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#manageChangeHistory(com.ilex.ws.core.bean
	 * .HDHistoryVO)
	 */
	@Override
	public void manageChangeHistory(HDHistoryVO hdHistoryVO) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("insert into lm_help_desk_change_history(");
		sqlQuery.append(" lm_ch_tc_id, lm_ch_event,lm_ch_initiator,lm_ch_to_reference,lm_ch_to_data,lm_ch_from_reference,");
		sqlQuery.append(" lm_ch_from_data,lm_ch_change_date)");
		sqlQuery.append(" values(?,?,?,?,?,?,?,getdate())");

		Object[] params = new Object[] { hdHistoryVO.getTicketId(),
				hdHistoryVO.getEvent(), hdHistoryVO.getInitiator(),
				hdHistoryVO.getToReference(), hdHistoryVO.getToData(),
				hdHistoryVO.getFromReference(), hdHistoryVO.getFromData() };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#ackTicket(com.ilex.ws.core.bean.HDTicketVO
	 * , java.lang.String)
	 */
	@Override
	public void ackTicket(HDTicketVO hdTicketVO, String userId) {
		StringBuilder queryForNewStatus = new StringBuilder();
		queryForNewStatus
				.append("select lm_hd_status from lm_help_desk_ticket_details where lm_hd_tc_id=?");
		Object[] paramsForNewStatus = new Object[] { hdTicketVO.getTicketId() };
		int status = 0;
		try {
			status = jdbcTemplateIlex.queryForInt(queryForNewStatus.toString(),
					paramsForNewStatus);
		} catch (EmptyResultDataAccessException e) {
			logger.info("Status is null while ack ticket for ticket id : "
					+ hdTicketVO.getTicketId());
		}

		if (status != Integer.parseInt(HDTicketStatus.NEW.getTicketStatus())) {
			throw new IlexBusinessException(
					MessageKeyConstant.TICKET_ALREADY_ACKNOWLEDGED);
		}
		int priority = getTicketPriority(
				Long.valueOf(hdTicketVO.getAppendixId()),
				Integer.valueOf(hdTicketVO.getImpact()),
				Integer.valueOf(hdTicketVO.getUrgency()));
		StringBuilder sqlQuery = new StringBuilder(
				"update lm_help_desk_ticket_details set ");
		sqlQuery.append("lm_hd_next_action_overdue = 1 , "
				+ "lm_hd_caller_name=?,");
		sqlQuery.append(" lm_hd_caller_phone=?,");
		sqlQuery.append(" lm_hd_caller_email=?,");
		sqlQuery.append(" lm_hd_category=?,");
		sqlQuery.append(" lm_hd_short_description=?,");
		sqlQuery.append(" lm_hd_ci=?,");
		sqlQuery.append(" lm_hd_impact=?,");
		sqlQuery.append(" lm_hd_urgency=?,");

		sqlQuery.append(" lm_hd_priority=?,");
		sqlQuery.append(" lm_hd_update_date=getDate(),");
		sqlQuery.append(" lm_hd_updated_by=?,");
		sqlQuery.append(" lm_hd_complete_first_call=?");
		sqlQuery.append(" where lm_hd_tc_id=?");

		Object[] params = new Object[] { hdTicketVO.getCallerName(),
				hdTicketVO.getCallerPhone(), hdTicketVO.getCallerEmail(),
				hdTicketVO.getCategory(), hdTicketVO.getShortDescription(),
				hdTicketVO.getConfigurationItem(), hdTicketVO.getImpact(),
				hdTicketVO.getUrgency(), priority, userId,
				hdTicketVO.getLmCompleteFirstCall(), hdTicketVO.getTicketId() };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

		sqlQuery = new StringBuilder("update lm_ticket set ");
		sqlQuery.append(" lm_tc_problem_desc=?");
		sqlQuery.append(" where lm_tc_id=?");

		params = new Object[] { hdTicketVO.getProblemDescription(),
				hdTicketVO.getTicketId() };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

		if ("hdPendingAckMonitor".equalsIgnoreCase(hdTicketVO.getAckCubeType())) {
			sqlQuery = new StringBuilder(
					"update wug_outages_incidents_in_process set ");
			sqlQuery.append(" wug_ip_backup_status=?");
			sqlQuery.append(" where wug_ip_tc_id=?");

			params = new Object[] { hdTicketVO.getBackUpCktStatus(),
					hdTicketVO.getTicketId() };

			jdbcTemplateIlex.update(sqlQuery.toString(), params);
		}

	}

	@Override
	public List<HDEffortVO> getEffortsDetail(Long ticketId) {
		StringBuilder sqlQuery = new StringBuilder(
				"SELECT STUFF(CONVERT(CHAR(20), lm_ef_date_time, 22), 7, 2, YEAR(lm_ef_date_time)) as lm_ef_date_time,STUFF(CONVERT(CHAR(20), lm_ef_stop_date_time, 22), 7, 2, YEAR(lm_ef_stop_date_time)) as lm_ef_stop_date_time,LEFT(lo_pc_first_name, 1)+' '+lo_pc_last_name as worked_by,");
		sqlQuery.append(" convert(decimal(8,2), lm_ef_effort) as effort,lm_ef_billable");
		sqlQuery.append(" from ( SELECT * FROM lm_help_desk_effort where lm_ef_tc_id = ?) as a");
		sqlQuery.append(" join lo_poc on lm_ef_recorded_by = lo_pc_id");
		sqlQuery.append(" order by lm_ef_tc_id");

		Object[] params = new Object[] { ticketId };

		List<HDEffortVO> hdEffortVOList = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new RowMapper<HDEffortVO>() {

					@Override
					public HDEffortVO mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						HDEffortVO hdEffortVO = new HDEffortVO();
						hdEffortVO.setBillable(rs.getBoolean("lm_ef_billable"));
						hdEffortVO.setEffort(rs.getString("effort"));
						hdEffortVO.setEffortEndDate((rs
								.getString("lm_ef_stop_date_time") != null) ? rs
								.getString("lm_ef_stop_date_time") : "n/a");
						hdEffortVO.setEffortDate(rs
								.getString("lm_ef_date_time"));
						hdEffortVO.setWorkedBy(rs.getString("worked_by"));
						return hdEffortVO;
					}
				});

		return hdEffortVOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getEfforts(java.lang.Long)
	 */
	@Override
	public List<HDEffortVO> getEfforts(Long ticketId) {
		StringBuilder sqlQuery = new StringBuilder(
				"Select  convert(varchar(10), lm_ef_date_time, 101) as lm_ef_date_time, LEFT(lo_pc_first_name, 1)+' '+lo_pc_last_name as worked_by,");
		sqlQuery.append(" convert(decimal(8,2), lm_ef_effort) as effort,lm_ef_billable");
		sqlQuery.append(" from ( SELECT * FROM lm_help_desk_effort where lm_ef_tc_id = ?) as a");
		sqlQuery.append(" join lo_poc on lm_ef_recorded_by = lo_pc_id");
		sqlQuery.append(" order by CONVERT(DATETIME , lm_ef_date_time) ");

		Object[] params = new Object[] { ticketId };

		List<HDEffortVO> hdEffortVOList = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new RowMapper<HDEffortVO>() {

					@Override
					public HDEffortVO mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						HDEffortVO hdEffortVO = new HDEffortVO();
						hdEffortVO.setBillable(rs.getBoolean("lm_ef_billable"));
						hdEffortVO.setEffort(rs.getString("effort"));
						hdEffortVO.setEffortDate(rs
								.getString("lm_ef_date_time"));
						hdEffortVO.setWorkedBy(rs.getString("worked_by"));
						return hdEffortVO;
					}
				});

		return hdEffortVOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getBillableInfo(java.lang.Long)
	 */
	@Override
	public HDBillableVO getBillableInfo(Long ticketId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select lm_ef_billable, convert(decimal(8,2), sum(lm_ef_effort)) as effort ");
		sqlQuery.append(" from ( SELECT * FROM lm_help_desk_effort where lm_ef_tc_id = ?) as a");
		sqlQuery.append(" join lo_poc on lm_ef_recorded_by = lo_pc_id ");
		sqlQuery.append(" group by lm_ef_billable ");

		Object[] params = new Object[] { ticketId };

		HDBillableVO hdBillableVO = jdbcTemplateIlex.query(sqlQuery.toString(),
				params, new BillableExtractor());
		return hdBillableVO;
	}

	/**
	 * The Class BillableExtractor.
	 */
	public class BillableExtractor implements ResultSetExtractor<HDBillableVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.ResultSetExtractor#extractData(java
		 * .sql.ResultSet)
		 */
		@Override
		public HDBillableVO extractData(ResultSet rs) throws SQLException,
				DataAccessException {
			HDBillableVO hdBillableVO = new HDBillableVO();
			while (rs.next()) {
				if (rs.getBoolean("lm_ef_billable") == true) {
					hdBillableVO.setBillable(rs.getString("effort"));
				} else if (rs.getBoolean("lm_ef_billable") == false) {
					hdBillableVO.setNonBillable(rs.getString("effort"));
				}
			}
			return hdBillableVO;
		}

	}

	@Override
	public void updateTicketToclosed(String ticketId, String status,
			Boolean isOverDue) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(" update lm_help_desk_ticket_details set");
		sqlQuery.append(" lm_hd_status=" + status);
		sqlQuery.append(", lm_hd_next_action_overdue='" + isOverDue + "'");
		sqlQuery.append(" where lm_hd_tc_id=?");

		Object[] params = new Object[] { ticketId };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#updateTicketToResolved(com.ilex.ws.core
	 * .bean.HDTicketVO, java.lang.String)
	 */
	@Override
	public void updateTicketToResolved(HDTicketVO hdTicketVO, String userId) {
		StringBuilder sqlQuery = new StringBuilder();
		Object[] params;
		if (hdTicketVO.getTicketStatus().contains("RES")) {// /
			sqlQuery.append(" update lm_help_desk_ticket_details set");
			sqlQuery.append(" lm_hd_status=?,");
			sqlQuery.append(" lm_hd_complete_first_call=?,");
			sqlQuery.append(" lm_hd_point_of_failure=?,");
			sqlQuery.append(" lm_hd_root_cause=?,");
			sqlQuery.append(" lm_hd_resolution=?,");
			sqlQuery.append(" lm_hd_update_date=getDate(),");
			sqlQuery.append(" lm_hd_updated_by=?,");
			sqlQuery.append(" lm_hd_session_id=?,");
			sqlQuery.append(" lm_hd_next_action_overdue=?,");
			sqlQuery.append(" lm_hd_assigned_to=?,lm_hd_ext_system_status= 0");
			sqlQuery.append(" where lm_hd_tc_id=?");

			params = new Object[] { HDTicketStatus.RESOLVED.getTicketStatus(),
					hdTicketVO.getLmCompleteFirstCall(),
					hdTicketVO.getPointOfFailureId(),
					hdTicketVO.getRootCauseId(), hdTicketVO.getResolutionId(),
					userId, null, false, userId, hdTicketVO.getTicketId() };
		} else {

			sqlQuery.append(" update lm_help_desk_ticket_details set");
			sqlQuery.append(" lm_hd_status=?,");
			sqlQuery.append(" lm_hd_complete_first_call=?,");
			sqlQuery.append(" lm_hd_update_date=getDate(),");
			sqlQuery.append(" lm_hd_updated_by=?,");
			sqlQuery.append(" lm_hd_session_id=?,");
			sqlQuery.append(" lm_hd_next_action_overdue=?,");
			sqlQuery.append(" lm_hd_assigned_to=?,lm_hd_ext_system_status= 0");
			sqlQuery.append(" where lm_hd_tc_id=?");

			params = new Object[] { "11", hdTicketVO.getLmCompleteFirstCall(),
					userId, null, false, userId, hdTicketVO.getTicketId() };
		}
		jdbcTemplateIlex.update(sqlQuery.toString(), params);
		// for monitoring tickets with an a record in
		// wug_outage_incidents_in_process
		if (hdTicketVO.getTicketSource().equalsIgnoreCase("2")) {
			sqlQuery = new StringBuilder();
			sqlQuery.append(" update wug_outages_incidents_in_process set");
			sqlQuery.append(" wug_ip_issue_owner=?,");
			sqlQuery.append(" wug_ip_corrective_action=?,");
			sqlQuery.append(" wug_ip_root_couse=? ");
			sqlQuery.append(" where wug_ip_tc_id=?");

			params = new Object[] { hdTicketVO.getPointOfFailureId(),
					hdTicketVO.getResolutionId(), hdTicketVO.getRootCauseId(),
					hdTicketVO.getTicketId() };
			jdbcTemplateIlex.update(sqlQuery.toString(), params);
		}
		// Update Cube Status Table
		sqlQuery = new StringBuilder();
		sqlQuery.append(" update lm_help_desk_cube_display_status ");
		sqlQuery.append(" set lm_cb_update_count=lm_cb_update_count+1 ");
		sqlQuery.append(" where lm_cb_name in ('WIP','WIP-OD') ");
		jdbcTemplateIlex.update(sqlQuery.toString());
		// Summarize Effort

		sqlQuery = new StringBuilder();
		sqlQuery.append(" select sum(lm_ef_effort) from lm_help_desk_effort where lm_ef_tc_id=?");
		params = new Object[] { hdTicketVO.getTicketId() };
		String efforts = null;
		try {
			efforts = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
					params, String.class);
		} catch (EmptyResultDataAccessException e) {
			logger.debug(e.getMessage());
			efforts = "0.0";
		}
		sqlQuery = new StringBuilder();
		sqlQuery.append(" select top 1 lm_rs_id");
		sqlQuery.append(" from ( SELECT * FROM lm_ticket ");
		sqlQuery.append(" join lm_activity on lm_at_js_id = lm_tc_js_id");
		sqlQuery.append(" join lm_resource on lm_rs_at_id = lm_at_id");
		sqlQuery.append(" WHERE lm_tc_id=? ) as a");
		params = new Object[] { hdTicketVO.getTicketId() };

		String resourceId = null;
		try {
			Map<String, Object> ticketInfoMap = jdbcTemplateIlex.queryForMap(
					sqlQuery.toString(), params);
			resourceId = ticketInfoMap.get("lm_rs_id").toString();
		} catch (EmptyResultDataAccessException e) {
			logger.debug(e.getMessage());
		}
		sqlQuery = new StringBuilder();
		sqlQuery.append(" update lm_resource set lm_rs_quantity =? where lm_rs_id =? ");
		params = new Object[] { efforts, resourceId };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

		sqlQuery = new StringBuilder();
		sqlQuery.append("update lx_cost_element set lx_ce_quantity=? where lx_ce_used_in=? and lx_ce_type='IL' ");
		params = new Object[] { efforts, resourceId };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#addNewSite(com.ilex.ws.core.bean.SiteDetailVO
	 * , java.lang.String)
	 */
	@Override
	public Long addNewSite(final SiteDetailVO siteDetailVO, final String userId) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		final StringBuilder sqlQuery = new StringBuilder(
				" INSERT INTO lp_site_list (lp_md_mm_id ,lp_si_name,lp_si_number ,lp_si_locality_factor");
		sqlQuery.append(" ,lp_si_union_site,lp_si_designator,lp_si_work_location");
		sqlQuery.append(" ,lp_si_address,lp_si_city,lp_si_state,lp_si_zip_code");
		sqlQuery.append(" ,lp_si_sec_phone_no,lp_si_phone_no,lp_si_country,lp_si_directions");
		sqlQuery.append(" ,lp_si_pri_poc,lp_si_sec_poc,lp_si_pri_email_id,lp_si_sec_email_id");
		sqlQuery.append(" ,lp_si_notes,lp_si_create_date,lp_si_created_by,lp_si_changed_date");
		sqlQuery.append(" ,lp_si_changed_by,lp_si_installer_poc,lp_si_site_poc,lp_si_lat_deg");
		sqlQuery.append(" ,lp_si_lat_min,lp_si_lat_direction,lp_si_lon_deg,lp_si_lon_min");
		sqlQuery.append(" ,lp_si_lon_direction,lp_si_status,lp_si_group,lp_si_end_customer");
		sqlQuery.append(" ,lp_si_time_zone,lp_si_region_id,lp_si_category_id,lp_si_latlong_source");
		sqlQuery.append(" ,lp_si_inactive_check,lp_si_latitude,lp_si_longitude,lp_si_latlong_accuracy)");
		sqlQuery.append("  VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getDate(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						sqlQuery.toString(), new String[] { "id" });
				ps.setInt(1, Integer.parseInt(siteDetailVO.getMsaId()));
				ps.setString(2, siteDetailVO.getSiteName());
				ps.setString(3, siteDetailVO.getSiteNumber());
				ps.setFloat(4,
						Float.parseFloat(siteDetailVO.getLocalityUplift()));
				ps.setString(5, siteDetailVO.getUnionSite());
				ps.setString(6, null);
				ps.setString(7, null);
				ps.setString(8, siteDetailVO.getSiteAddress());
				ps.setString(9, siteDetailVO.getSiteCity());
				ps.setString(10, siteDetailVO.getState());
				ps.setString(11, siteDetailVO.getSiteZipcode());
				ps.setString(12, null);
				ps.setString(13, siteDetailVO.getSitePhone());
				ps.setString(14, siteDetailVO.getCountry());
				ps.setString(15, null);
				ps.setString(16, siteDetailVO.getPrimaryName());
				ps.setString(17, null);
				ps.setString(18, siteDetailVO.getPrimaryEmail());
				ps.setString(19, null);
				ps.setString(20, null);
				ps.setInt(21, Integer.parseInt(userId));
				ps.setString(22, null);
				ps.setString(23, null);
				ps.setString(24, null);
				ps.setString(25, null);
				ps.setString(26, null);
				ps.setString(27, null);
				ps.setString(28, null);
				ps.setString(29, null);
				ps.setString(30, null);
				ps.setString(31, null);
				ps.setString(32, null);
				ps.setString(33, null);
				ps.setString(34, siteDetailVO.getSiteEndCustomer());
				ps.setString(35, null);
				ps.setString(36, null);
				ps.setString(37, null);
				ps.setString(38, null);
				ps.setString(39, null);
				ps.setString(40, null);
				ps.setString(41, null);
				ps.setString(42, null);
				return ps;
			}
		};

		jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);

		Long siteId = keyHolder.getKey().longValue();
		return siteId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#copySiteToLmSiteDetail(com.ilex.ws.core
	 * .bean.SiteDetailVO, java.lang.String)
	 */
	@Override
	public Long copySiteToLmSiteDetail(final SiteDetailVO siteDetailVO,
			final String userId) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		final StringBuilder sqlQuery = new StringBuilder(
				" INSERT INTO lm_site_detail(");
		sqlQuery.append(" lm_si_name,lm_si_number,lm_si_locality_factor");
		sqlQuery.append(" ,lm_si_union_site,lm_si_designator,lm_si_work_location");
		sqlQuery.append(" ,lm_si_address,lm_si_city,lm_si_state");
		sqlQuery.append(" ,lm_si_zip_code,lm_si_sec_phone_no,lm_si_phone_no");
		sqlQuery.append(" ,lm_si_country,lm_si_directions,lm_si_pri_poc");
		sqlQuery.append(" ,lm_si_sec_poc,lm_si_pri_email_id,lm_si_sec_email_id");
		sqlQuery.append(" ,lm_si_notes,lm_si_create_date,lm_si_created_by");
		sqlQuery.append(" ,lm_si_changed_date,lm_si_changed_by,lm_si_installer_poc");
		sqlQuery.append(" ,lm_si_site_poc,lm_si_lat_deg,lm_si_lat_min");
		sqlQuery.append(" ,lm_si_lat_direction,lm_si_lon_deg,lm_si_lon_min");
		sqlQuery.append(" ,lm_si_lon_direction,lm_si_status,lm_si_group");
		sqlQuery.append(" ,lm_si_end_customer,lm_si_time_zone,lm_si_region_id");
		sqlQuery.append(" ,lm_si_category_id,lm_si_sl_id,lm_si_latitude,lm_si_longitude)");
		sqlQuery.append("  VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						sqlQuery.toString(), new String[] { "id" });
				ps.setString(1, siteDetailVO.getSiteName());
				ps.setString(2, siteDetailVO.getSiteNumber());
				ps.setString(3, siteDetailVO.getLocalityUplift());
				ps.setString(4, siteDetailVO.getUnionSite());
				ps.setString(5, siteDetailVO.getSiteDesignator());
				ps.setString(6, siteDetailVO.getSiteWorkLocation());
				ps.setString(7, siteDetailVO.getSiteAddress());
				ps.setString(8, siteDetailVO.getSiteCity());
				ps.setString(9, siteDetailVO.getState());
				ps.setString(10, siteDetailVO.getSiteZipcode());
				ps.setString(11, siteDetailVO.getSecondaryPhone());
				ps.setString(12, siteDetailVO.getSitePhone());
				ps.setString(13, siteDetailVO.getCountry());
				ps.setString(14, siteDetailVO.getSiteDirection());
				ps.setString(15, siteDetailVO.getPrimaryName());
				ps.setString(16, siteDetailVO.getSecondaryName());
				ps.setString(17, siteDetailVO.getPrimaryEmail());
				ps.setString(18, siteDetailVO.getSecondaryEmail());
				ps.setString(19, siteDetailVO.getSiteNotes());
				ps.setString(20, siteDetailVO.getCreateDate());
				ps.setString(21, siteDetailVO.getCreateBy());
				ps.setString(22, siteDetailVO.getChangeDate());
				ps.setString(23, siteDetailVO.getChangeBy());
				ps.setString(24, siteDetailVO.getInstallerPoc());
				ps.setString(25, siteDetailVO.getSitePoc());
				ps.setString(26, siteDetailVO.getSiteLatDeg());
				ps.setString(27, siteDetailVO.getSiteLatMin());
				ps.setString(28, siteDetailVO.getSiteLatDirection());
				ps.setString(29, siteDetailVO.getSiteLonDeg());
				ps.setString(30, siteDetailVO.getSiteLonMin());
				ps.setString(31, siteDetailVO.getSiteLonDirection());
				ps.setString(32, siteDetailVO.getSiteStatus());
				ps.setString(33, siteDetailVO.getSiteGroup());
				ps.setString(34, siteDetailVO.getSiteEndCustomer());
				ps.setString(35, siteDetailVO.getSiteTimeZone());
				ps.setString(36, siteDetailVO.getSiteRegion());
				ps.setString(37, siteDetailVO.getSiteCategory());
				ps.setString(38, siteDetailVO.getSiteId());
				ps.setString(39, siteDetailVO.getLatitude());
				ps.setString(40, siteDetailVO.getLongitude());
				return ps;
			}
		};

		jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);

		Long siteId = keyHolder.getKey().longValue();
		return siteId;
	}

	/**
	 * The Class HDSiteCompleteDetailMapper.
	 */
	private static final class HDSiteCompleteDetailMapper implements
			RowMapper<SiteDetailVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public SiteDetailVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			SiteDetailVO siteDetailVO = new SiteDetailVO();
			siteDetailVO.setSiteId(rs.getString("lp_sl_id"));
			siteDetailVO.setMsaId(rs.getString("lp_md_mm_id"));
			siteDetailVO.setSiteName(rs.getString("lp_si_name"));
			siteDetailVO.setSiteNumber(rs.getString("lp_si_number"));
			siteDetailVO.setLocalityUplift(rs
					.getString("lp_si_locality_factor"));
			siteDetailVO.setUnionSite(rs.getString("lp_si_union_site"));
			siteDetailVO.setSiteDesignator(rs.getString("lp_si_designator"));
			siteDetailVO.setSiteWorkLocation(rs
					.getString("lp_si_work_location"));
			siteDetailVO.setSiteAddress(rs.getString("lp_si_address"));
			siteDetailVO.setSiteCity(rs.getString("lp_si_city"));
			siteDetailVO.setState(rs.getString("lp_si_state"));
			siteDetailVO.setSiteZipcode(rs.getString("lp_si_zip_code"));
			siteDetailVO.setSecondaryPhone(rs.getString("lp_si_sec_phone_no"));
			siteDetailVO.setSitePhone(rs.getString("lp_si_phone_no"));
			siteDetailVO.setCountry(rs.getString("lp_si_country"));
			siteDetailVO.setSiteDirection(rs.getString("lp_si_directions"));
			siteDetailVO.setPrimaryName(rs.getString("lp_si_pri_poc"));
			siteDetailVO.setSecondaryName(rs.getString("lp_si_sec_poc"));
			siteDetailVO.setPrimaryEmail(rs.getString("lp_si_pri_email_id"));
			siteDetailVO.setSecondaryEmail(rs.getString("lp_si_sec_email_id"));
			siteDetailVO.setSiteNotes(rs.getString("lp_si_notes"));
			siteDetailVO.setCreateDate(rs.getString("lp_si_create_date"));
			siteDetailVO.setCreateBy(rs.getString("lp_si_created_by"));
			siteDetailVO.setChangeDate(rs.getString("lp_si_changed_date"));
			siteDetailVO.setChangeBy(rs.getString("lp_si_changed_by"));
			siteDetailVO.setInstallerPoc(rs.getString("lp_si_installer_poc"));
			siteDetailVO.setSitePoc(rs.getString("lp_si_site_poc"));
			siteDetailVO.setSiteLatDeg(rs.getString("lp_si_lat_deg"));
			siteDetailVO.setSiteLatMin(rs.getString("lp_si_lat_min"));
			siteDetailVO.setSiteLatDirection(rs
					.getString("lp_si_lat_direction"));
			siteDetailVO.setSiteLonDeg(rs.getString("lp_si_lon_deg"));
			siteDetailVO.setSiteLonMin(rs.getString("lp_si_lon_min"));
			siteDetailVO.setSiteLonDirection(rs
					.getString("lp_si_lon_direction"));
			siteDetailVO.setSiteStatus(rs.getString("lp_si_status"));
			siteDetailVO.setSiteGroup(rs.getString("lp_si_group"));
			siteDetailVO.setSiteEndCustomer(rs.getString("lp_si_end_customer"));
			siteDetailVO.setSiteTimeZone(rs.getString("lp_si_time_zone"));
			siteDetailVO.setSiteRegion(rs.getString("lp_si_region_id"));
			siteDetailVO.setSiteCategory(rs.getString("lp_si_category_id"));
			siteDetailVO.setSiteLatLon(rs.getString("lp_si_latlong_source"));
			siteDetailVO.setInactiveCheck(rs.getString("lp_si_inactive_check"));
			siteDetailVO.setLatitude(rs.getString("lp_si_latitude"));
			siteDetailVO.setLongitude(rs.getString("lp_si_longitude"));
			siteDetailVO.setLatLongAccuracy(rs
					.getString("lp_si_latlong_accuracy"));
			siteDetailVO.setWorkNotes(rs.getString("lp_si_notes"));

			return siteDetailVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#checkSiteExist(com.ilex.ws.core.bean.
	 * SiteDetailVO)
	 */
	@Override
	public boolean checkSiteExist(SiteDetailVO siteDetailVO) {
		boolean siteExist = false;
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(" select top 1 lp_sl_id from lp_site_list");
		sqlQuery.append(" where lp_si_number=? and lp_md_mm_id=? and lp_si_end_customer=?");
		Object[] params = new Object[] { siteDetailVO.getSiteNumber(),
				siteDetailVO.getMsaId(), siteDetailVO.getSiteEndCustomer() };
		String siteId = null;
		try {
			siteId = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
					params, String.class);
		} catch (EmptyResultDataAccessException e) {
			siteId = null;
		}
		if (StringUtils.isNotBlank(siteId)) {
			siteExist = true;
		}
		return siteExist;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#cancelTicket(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void cancelTicket(String ticketId, String userId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select lm_st_id from lm_help_desk_ticket_status where lm_st_status = 'Cancelled'");

		Long statusId = jdbcTemplateIlex.queryForLong(sqlQuery.toString());

		sqlQuery = new StringBuilder();
		sqlQuery.append("update lm_help_desk_ticket_details ");
		sqlQuery.append("set lm_hd_status=?, ");
		sqlQuery.append("lm_hd_update_date=getdate(), ");
		sqlQuery.append("lm_hd_updated_by=? ");
		sqlQuery.append("where lm_hd_tc_id=? ");

		Object[] params = new Object[] { statusId, userId, ticketId };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#saveNoAction(java.lang.String)
	 */
	@Override
	public void saveNoAction(String ticketId) {
		/*
		 * if (ticketId == null || ticketId.equals("")) { ticketId = "0"; }
		 * StringBuilder sqlQuery = new StringBuilder(); //
		 * sqlQuery.append("delete lm_help_desk_ticket_details where lm_hd_tc_id=?"
		 * ); sqlQuery.append(
		 * "UPDATE lm_help_desk_ticket_details set lm_hd_status = 8 WHERE lm_hd_tc_id = ?"
		 * );
		 * 
		 * Object[] params = new Object[] { ticketId };
		 * 
		 * jdbcTemplateIlex.update(sqlQuery.toString(), params);
		 */

		/*
		 * As we are now canceling the ticket (not deleting) so related things
		 * should not get deleted too.
		 */
		// change deletion process of job 2014-05-26
		/*
		 * Long jobid = getJobIdByTicket(ticketId); if (jobid > 0) { sqlQuery =
		 * new StringBuilder();
		 * sqlQuery.append("delete lm_job where lm_js_id= ?"); params = new
		 * Object[] { jobid }; jdbcTemplateIlex.update(sqlQuery.toString(),
		 * params); } // End of changes sqlQuery = new StringBuilder();
		 * sqlQuery.append("delete lm_ticket where lm_tc_id=?");
		 * 
		 * params = new Object[] { ticketId };
		 * 
		 * jdbcTemplateIlex.update(sqlQuery.toString(), params);
		 * 
		 * // added delete from other related tables sqlQuery = new
		 * StringBuilder(); sqlQuery.append(
		 * "delete lm_help_desk_next_action_history where lm_na_tc_id=?");
		 * params = new Object[] { ticketId };
		 * jdbcTemplateIlex.update(sqlQuery.toString(), params);
		 * 
		 * sqlQuery = new StringBuilder();
		 * sqlQuery.append("delete lm_help_desk_alerts where lm_al_tc_id=?");
		 * params = new Object[] { ticketId };
		 * jdbcTemplateIlex.update(sqlQuery.toString(), params);
		 * 
		 * sqlQuery = new StringBuilder(); sqlQuery.append(
		 * "delete lm_help_desk_temp_site_details where lm_ts_tc_id=?"); params
		 * = new Object[] { ticketId };
		 * jdbcTemplateIlex.update(sqlQuery.toString(), params);
		 * 
		 * sqlQuery = new StringBuilder();
		 * sqlQuery.append("delete lm_help_desk_change_history where lm_ch_tc_id=?"
		 * ); params = new Object[] { ticketId };
		 * jdbcTemplateIlex.update(sqlQuery.toString(), params);
		 * 
		 * sqlQuery = new StringBuilder();
		 * sqlQuery.append("delete lm_help_desk_effort where lm_ef_tc_id=?");
		 * params = new Object[] { ticketId };
		 * jdbcTemplateIlex.update(sqlQuery.toString(), params);
		 * 
		 * sqlQuery = new StringBuilder();
		 * sqlQuery.append("delete lm_help_desk_work_notes where lm_wn_tc_id=?"
		 * ); params = new Object[] { ticketId };
		 * jdbcTemplateIlex.update(sqlQuery.toString(), params);
		 */
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getNoActionsDaysData(java.lang.String)
	 */
	@Override
	public Map<String, String> getNoActionsDaysData(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select wug_na_dStartTime as 'Last Occurrence', wug_na_count as 'Occurrences'");
		sqlQuery.append(" from (SELECT * FROM lm_help_desk_ticket_details WHERE lm_hd_tc_id=?) as a");
		sqlQuery.append(" join wug_outages_incidents_in_process on lm_hd_tc_id = wug_ip_tc_id");
		sqlQuery.append(" join wug_outage_no_action on wug_ip_dv_id = wug_na_dv_id");

		Object[] params = new Object[] { ticketId };
		Map<String, Object> dataMap;
		try {
			dataMap = jdbcTemplateIlex.queryForMap(sqlQuery.toString(), params);
		} catch (EmptyResultDataAccessException e) {
			throw new IlexBusinessException(
					MessageKeyConstant.NO_ACTION_DATA_NOT_FOUND);
		}
		Map<String, String> returnMap = new HashMap<>();
		returnMap.put("LastOccurrence", dataMap.get("Last Occurrence")
				.toString());
		returnMap.put("Occurrences", dataMap.get("Occurrences").toString());

		return returnMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#updateSite(com.ilex.ws.core.bean.SiteDetailVO
	 * , java.lang.String)
	 */
	@Override
	public void updateSite(SiteDetailVO siteDetailVO, String userId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(" update lp_site_list");
		sqlQuery.append(" set lp_si_notes = ? ");
		sqlQuery.append(" where lp_sl_id = ?");

		Object[] params = new Object[] { siteDetailVO.getWorkNotes(),
				siteDetailVO.getSiteId() };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);

		StringBuilder sqlQuery2 = new StringBuilder();
		sqlQuery2.append(" update lm_site_detail");
		sqlQuery2.append(" set lm_si_notes = ? ");
		sqlQuery2.append(" where lm_si_sl_id = ?");

		jdbcTemplateIlex.update(sqlQuery2.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getWUGInformation(com.ilex.ws.core.bean
	 * .HDTicketVO)
	 */
	@Override
	public WugDetailsVO getWUGInformation(HDTicketVO hdTicketVO) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(" Select TOP 1  wug_dd_isp_support_number, wug_dd_circuit_ip, ");
		sqlQuery.append(" wug_dd_modem_type,IsNull(wug_dd_apc_address,'') wug_dd_apc_address ");
		sqlQuery.append(" FROM wug_device ");
		sqlQuery.append(" join wug_device_detail on wug_dv_id = wug_dd_dv_id ");
		sqlQuery.append(" JOIN lp_site_list ON lp_sl_id = wug_dv_lp_si_id  ");
		sqlQuery.append(" where lp_si_number = ? ");

		Object[] param = new Object[] { hdTicketVO.getSiteNumber() };
		WugDetailsVO wugDetailsVO = new WugDetailsVO();
		try {
			wugDetailsVO = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
					param, new WUGDetailMapper());
		} catch (EmptyResultDataAccessException e) {
			logger.debug(e);
		} catch (IncorrectResultSizeDataAccessException e) {
			logger.debug(e);
		} catch (Exception e) {
			logger.debug(e);
		}
		return wugDetailsVO;
	}

	/**
	 * The Class WUGDetailMapper.
	 */
	private static final class WUGDetailMapper implements
			RowMapper<WugDetailsVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public WugDetailsVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			WugDetailsVO wugDetailsVO = new WugDetailsVO();
			wugDetailsVO.setWugHelpDesk(rs
					.getString("wug_dd_isp_support_number"));
			wugDetailsVO.setWugCircuitIp(rs.getString("wug_dd_circuit_ip"));
			wugDetailsVO.setWugModemType(rs.getString("wug_dd_modem_type"));
			wugDetailsVO.setWugApcAddress(rs.getString("wug_dd_apc_address"));
			return wugDetailsVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#ProvisioningDetail(java.lang.String)
	 */
	@Override
	public ProvisioningDetailVO ProvisioningDetail(String siteId) {
		System.out.println("your site id " + siteId);
		StringBuilder sqlQuery = new StringBuilder(
				"select provider_name as ISP, c.ip_type as IpType,ip_user_name as IpUserName,");
		sqlQuery.append(" ip_pw as IpPassword, usable_ip as UsableIp, gateway as Gateway,");
		sqlQuery.append(" subnet_mask as SubNetMask, dns as PrmiaryDNS, secondary_dns as SecondaryDNS, circuit_number as CircuitNumber ");
		sqlQuery.append(" FROM");
		sqlQuery.append(" ( SELECT * FROM msc_resource_info WHERE site_id = ? and resource_name like '%broadband%' AND telecom_service = 1 ) as a");
		sqlQuery.append(" INNER JOIN ");
		sqlQuery.append(" ( SELECT * FROM msc_activity WHERE site_id = ? and deleted !=1 ) as b");
		sqlQuery.append(" ON b.parent_activity_id = a.parent_activity_id ");
		sqlQuery.append(" AND b.site_id = a.site_id ");
		sqlQuery.append(" AND b.project_id = a.project_id AND provider_name <> '' ");
		sqlQuery.append(" LEFT OUTER JOIN  msp_ip_type as c ON  c.id = a.ip_type");

		Object[] param = new Object[] { siteId, siteId };
		ProvisioningDetailVO provisioningDetailVO = new ProvisioningDetailVO();
		try {
			provisioningDetailVO = jdbcTemplatePivotDB.queryForObject(
					sqlQuery.toString(), param, new ProvisioningDetailMapper());
		} catch (EmptyResultDataAccessException e) {
			logger.debug(e);
		} catch (IncorrectResultSizeDataAccessException e) {
			logger.debug(e);
		} catch (Exception e) {
			logger.debug(e);
		}
		return provisioningDetailVO;
	}

	/**
	 * The Class ProvisioningDetailMapper.
	 */
	private static final class ProvisioningDetailMapper implements
			RowMapper<ProvisioningDetailVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public ProvisioningDetailVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			ProvisioningDetailVO provisioningDetailVO = new ProvisioningDetailVO();
			provisioningDetailVO.setCircuit(rs.getString("CircuitNumber"));
			provisioningDetailVO.setGateway(rs.getString("Gateway"));
			provisioningDetailVO.setIpPassword(rs.getString("IpPassword"));
			provisioningDetailVO.setIpType(rs.getString("IpType"));
			provisioningDetailVO.setIpUsername(rs.getString("IpUserName"));
			provisioningDetailVO.setPrimaryDNS(rs.getString("PrmiaryDNS"));
			provisioningDetailVO.setSecondayDNS(rs.getString("SecondaryDNS"));
			provisioningDetailVO.setSubnetMask(rs.getString("SubNetMask"));
			provisioningDetailVO.setUseableIp(rs.getString("UsableIp"));
			return provisioningDetailVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getWUGOutInProcessData(java.lang.String)
	 */
	@Override
	public WugOutInProcessVO getWUGOutInProcessData(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder(
				" select  wug_ip_nActiveMonitorStateChangeID,wug_ip_backup_status,wug_ip_instance ");
		sqlQuery.append(" from wug_outages_incidents_in_process where wug_ip_tc_id = ?");
		Object[] param = new Object[] { ticketId };
		WugOutInProcessVO wugOutInProcessVO = new WugOutInProcessVO();
		try {
			wugOutInProcessVO = jdbcTemplateIlex.queryForObject(
					sqlQuery.toString(), param,
					new WugOutInProcessDetailMapper());
		} catch (EmptyResultDataAccessException e) {
			logger.debug(e);
		} catch (IncorrectResultSizeDataAccessException e) {
			logger.debug(e);
		} catch (Exception e) {
			logger.debug(e);
		}
		return wugOutInProcessVO;
	}

	/**
	 * The Class WugOutInProcessDetailMapper.
	 */
	private static final class WugOutInProcessDetailMapper implements
			RowMapper<WugOutInProcessVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public WugOutInProcessVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			WugOutInProcessVO wugOutInProcessVO = new WugOutInProcessVO();
			wugOutInProcessVO.setnActiveMonitorStateChangeID(rs
					.getString("wug_ip_nActiveMonitorStateChangeID"));
			wugOutInProcessVO.setWugBackupStatus(rs
					.getString("wug_ip_backup_status"));
			wugOutInProcessVO.setWugInstance(rs.getString("wug_ip_instance"));
			return wugOutInProcessVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#jobUpdateForTicketToResolved(com.ilex.
	 * ws.core.bean.HDTicketVO, java.lang.String)
	 */
	@Override
	public void jobUpdateForTicketToResolved(HDTicketVO hdTicketVO,
			String userId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(" select lm_tc_js_id from lm_ticket where lm_tc_id =? ");
		Object[] params = new Object[] { hdTicketVO.getTicketId() };
		String jobId = null;
		try {
			jobId = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
					params, String.class);
		} catch (EmptyResultDataAccessException e) {
			logger.debug(e.getMessage());
		}
		// determine dispatch
		sqlQuery = new StringBuilder();
		sqlQuery.append(" select lm_at_id from lm_activity where ");
		sqlQuery.append(" lm_at_title='Troubleshoot and Repair' and lm_at_js_id=? ");
		params = new Object[] { jobId };
		boolean noDispatch = false;
		try {
			jdbcTemplateIlex.queryForObject(sqlQuery.toString(), params,
					String.class);
			noDispatch = true;
		} catch (EmptyResultDataAccessException e) {
			logger.debug(e.getMessage());
		}
		if (noDispatch) {
			// complete job
			sqlQuery = new StringBuilder();
			if (hdTicketVO.getTicketStatus().contains("Closed")) {
				sqlQuery.append(" update lm_job set lm_js_prj_status='O', ");
			} else {
				sqlQuery.append(" update lm_job set lm_js_prj_status='F', ");
			}
			sqlQuery.append(" lm_js_changed_by=?,lm_js_offsite_date=getDate(),lm_js_submitted_date=getDate(),lm_js_change_date=getDate() ");
			sqlQuery.append(" where lm_js_id= ?");
			params = new Object[] { userId, jobId };
			jdbcTemplateIlex.update(sqlQuery.toString(), params);

			sqlQuery = new StringBuilder();
			sqlQuery.append(" update lx_schedule_element  set lx_se_end =getDate() ");
			sqlQuery.append(" where lx_se_type_id= ?");
			params = new Object[] { jobId };
			jdbcTemplateIlex.update(sqlQuery.toString(), params);

		}
		// determine job is complete and non billable
		// determine dispatch
		sqlQuery = new StringBuilder("select lm_js_prj_status");
		sqlQuery.append(" from (SELECT * FROM lm_job WHERE lm_js_id= ? AND lm_js_prj_status='F' ) as a");
		sqlQuery.append(" join lm_ticket on lm_tc_js_id = lm_js_id");
		sqlQuery.append(" join lm_help_desk_ticket_details on lm_hd_tc_id = lm_tc_id");
		sqlQuery.append(" where lm_hd_billable=0");
		params = new Object[] { jobId };
		boolean jobCompBillable = false;
		try {
			jdbcTemplateIlex.queryForObject(sqlQuery.toString(), params,
					String.class);
			jobCompBillable = true;
		} catch (EmptyResultDataAccessException e) {
			logger.debug(e.getMessage());
		}
		if (jobCompBillable) {
			sqlQuery = new StringBuilder();
			sqlQuery.append(" update lm_job set lm_js_prj_status='F', ");
			sqlQuery.append(" lm_js_changed_by=?,lm_js_associated_date=getDate(),lm_js_closed_date=getDate(),");
			sqlQuery.append(" lm_js_invoice_no='Help Desk - '+ CONVERT(CHAR(23),  GETDATE(), 121), lm_js_change_date=getDate()");
			sqlQuery.append(" where lm_js_id= ?");
			params = new Object[] { userId, jobId };
			jdbcTemplateIlex.update(sqlQuery.toString(), params);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getRealTimeStateList()
	 */
	@Override
	public List<RealTimeState> getRealTimeStateList() {
		List<RealTimeState> realTimeStateList = Mspdao
				.managedInitialAssessment(EscalationLevel.INITIAL_ASSESSMENT,
						jdbcTemplateIlex.getDataSource());
		List<RealTimeState> realTimeStateListUpStatus = new ArrayList<RealTimeState>();
		for (RealTimeState realTimeState : realTimeStateList) {
			if (realTimeState.getCurrentStatus().equalsIgnoreCase("up")) {
				realTimeStateListUpStatus.add(realTimeState);
			}
		}

		return realTimeStateListUpStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getLastUpdateEventDetail()
	 */
	@Override
	public RealTimeStateInfo getLastUpdateEventDetail() {
		RealTimeStateInfo realTimeStateInfo = Mspdao
				.getRealTimeStateInfo(jdbcTemplateIlex.getDataSource());
		return realTimeStateInfo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#wugInfoMap(java.lang.String)
	 */
	@Override
	public Map<String, Object> wugInfoMap(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select  wug_ip_nActiveMonitorStateChangeID, wug_ip_instance from wug_outages_incidents_in_process where wug_ip_tc_id = ?");

		Object[] params = new Object[] { ticketId };

		Map<String, Object> wugInfoMap = jdbcTemplateIlex.queryForMap(
				sqlQuery.toString(), params);
		return wugInfoMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#updateCubeDisplayStatus(java.lang.String)
	 */
	@Override
	public void updateCubeDisplayStatus(String cubesList) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(" update lm_help_desk_cube_display_status ");
		sqlQuery.append(" set lm_cb_update_count=lm_cb_update_count+1 ");
		sqlQuery.append(" where lm_cb_name in (" + cubesList + ")");
		jdbcTemplateIlex.update(sqlQuery.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#checkTicketStatusBeforeNoAction(java.lang
	 * .String)
	 */
	@Override
	public void checkTicketStatusBeforeNoAction(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		String ticketStatusQuery = "select lm_hd_status from lm_help_desk_ticket_details  where  lm_hd_tc_id=?";
		Object[] params = new Object[] { ticketId };
		String ticketStatus = null;
		try {
			ticketStatus = jdbcTemplateIlex.queryForObject(ticketStatusQuery,
					params, String.class);
		} catch (EmptyResultDataAccessException e) {
			logger.error(e);
		}
		logger.info("Ticket lm_hd_status value is " + ticketStatus
				+ " before No action,for Ticket_Id=" + ticketId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getSiteIdFromJob(java.lang.Long)
	 */
	@Override
	public Long getSiteIdFromJob(Long jobId) {
		String sqlQuery = "Select lm_js_si_id from lm_job where lm_js_id=?";

		Object[] params = new Object[] { jobId };
		Long siteId = jdbcTemplateIlex.queryForLong(sqlQuery, params);

		return siteId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getJobIdByTicketNumber(java.lang.Long)
	 */
	@Override
	public Long getJobIdByTicketNumber(String ticketNumber) {
		String sqlQuery = "SELECT Top 1 isnull(lm_tc_js_id, 0) lm_tc_js_id FROM lm_ticket WHERE lm_tc_number = ? order by lm_tc_js_id desc";

		Object[] params = new Object[] { ticketNumber };
		Long jobId = jdbcTemplateIlex.queryForLong(sqlQuery, params);

		return jobId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#updateSite(java.lang.Long,
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void updateSite(Long siteDetailId, String primaryName,
			String sitePhone, String primaryEmail) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("update lm_site_detail");
		sqlQuery.append(" set lm_si_pri_poc = ?,");
		sqlQuery.append(" lm_si_pri_email_id=?, ");
		sqlQuery.append(" lm_si_phone_no=?");
		sqlQuery.append(" where lm_si_id=?");

		Object[] params = new Object[] { primaryName, primaryEmail, sitePhone,
				siteDetailId };

		jdbcTemplateIlex.update(sqlQuery.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getCustomerListForCancelAll()
	 */
	@Override
	public List<HDCustomerDetailVO> getCustomerListForCancelAll() {
		StringBuilder sqlQuery = new StringBuilder("");
		sqlQuery.append(" select distinct lp_mm_id, lo_ot_name from ap_appendix_list_01 ");
		sqlQuery.append(" where lp_mm_id in (select distinct lm_hd_lp_mm_id ");
		sqlQuery.append(" from lm_help_desk_ticket_details");
		sqlQuery.append(" where lm_hd_source = 2 and lm_hd_status = 1) AND lo_ot_name NOT LIKE 'Brinker International' ");
		List<HDCustomerDetailVO> customerListVOs = jdbcTemplateIlex.query(
				sqlQuery.toString(), new HDCustomerList());
		return customerListVOs;
	}

	/**
	 * The Class HDCustomerList.
	 */
	private static final class HDCustomerList implements
			RowMapper<HDCustomerDetailVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public HDCustomerDetailVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			HDCustomerDetailVO customerDetailVO = new HDCustomerDetailVO();
			customerDetailVO.setMsaId(rs.getLong("lp_mm_id"));
			customerDetailVO.setCustomerName(rs.getString("lo_ot_name"));
			return customerDetailVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getAllOutageEvents(java.lang.String,
	 * java.lang.String, java.lang.String, int)
	 */
	@Override
	public List<OutageDetailVO> getAllOutageEvents(String customers,
			String startDate, String endDate, int duration) {
		StringBuilder sqlQuery = new StringBuilder("");
		sqlQuery.append(" select lm_hd_tc_id, wug_ip_name from lm_help_desk_ticket_details ");
		sqlQuery.append(" join wug_outages_incidents_in_process on lm_hd_tc_id = wug_ip_tc_id ");
		sqlQuery.append(" where lm_hd_source = 2 and lm_hd_status = 1 ");
		sqlQuery.append(" and lm_hd_lp_mm_id in(" + customers + ")");
		if (StringUtils.isNotBlank(startDate)) {
			sqlQuery.append(" and lm_hd_opened between '" + startDate
					+ "' and '" + endDate + "'");
		}
		sqlQuery.append(" and wug_ip_outage_duration <?");
		sqlQuery.append(" and wug_ip_sStateName = 'Up' ");
		Object[] params = new Object[] { duration };
		List<OutageDetailVO> outageListVOs = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new OutageDetailMapper());
		return outageListVOs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getFalseOutageEvents(java.lang.String,
	 * java.lang.String, java.lang.String, int)
	 */
	@Override
	public List<OutageDetailVO> getFalseOutageEvents(String customerList,
			String startDate, String endDate, int duration) {
		StringBuilder sqlQuery = new StringBuilder(
				"select top 200 lm_hd_tc_id, wug_ip_name");
		sqlQuery.append(" from (SELECT * FROM lm_help_desk_ticket_details");
		sqlQuery.append(" WHERE lm_hd_source = 2 and lm_hd_status = 1  and lm_hd_lp_mm_id in(");
		sqlQuery.append(customerList);
		sqlQuery.append(")");
		if (StringUtils.isNotBlank(startDate)) {
			sqlQuery.append(" and lm_hd_opened between '");
			sqlQuery.append(startDate);
			sqlQuery.append("' and '");
			sqlQuery.append(endDate + "' ");

		}
		sqlQuery.append(" ) as a");
		sqlQuery.append(" join");
		sqlQuery.append(" (SELECT * FROM wug_outages_incidents_in_process WHERE wug_ip_dStartTime >= DATEADD(month, -1, GETDATE()) AND wug_ip_outage_duration < ? AND wug_ip_sStateName = 'Up') as b");
		sqlQuery.append(" on lm_hd_tc_id = wug_ip_tc_id");
		sqlQuery.append(" order by lm_hd_lp_mm_id, wug_ip_name ");

		Object[] params = new Object[] { duration };
		List<OutageDetailVO> outageListVOs = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new OutageDetailMapper());
		return outageListVOs;
	}

	/**
	 * The Class OutageDetailMapper.
	 */
	private static final class OutageDetailMapper implements
			RowMapper<OutageDetailVO> {
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */

		public OutageDetailVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			OutageDetailVO outageDetailVO = new OutageDetailVO();
			outageDetailVO.setTicketId(rs.getString("lm_hd_tc_id"));
			outageDetailVO.setWugIpName(rs.getString("wug_ip_name"));
			return outageDetailVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#validateOutage(java.lang.String,
	 * java.lang.String, java.lang.String, int, java.lang.String)
	 */
	@Override
	public boolean validateOutage(String customerList, String startTimeFrame,
			String endTimeFrame, int duration, String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}

		StringBuilder sqlQuery = new StringBuilder(
				"select lm_hd_tc_id, wug_ip_name");
		sqlQuery.append(" from (SELECT * FROM lm_help_desk_ticket_details");
		sqlQuery.append(" WHERE lm_hd_tc_id = ? and lm_hd_source = 2 and lm_hd_status = 1  and lm_hd_lp_mm_id in(");
		sqlQuery.append(customerList);
		sqlQuery.append(")");
		if (StringUtils.isNotBlank(startTimeFrame)) {
			sqlQuery.append(" and lm_hd_opened between '");
			sqlQuery.append(startTimeFrame);
			sqlQuery.append("' and '");
			sqlQuery.append(endTimeFrame + "' ");

		}
		sqlQuery.append(") as a");
		sqlQuery.append(" join");
		sqlQuery.append(" (SELECT * FROM wug_outages_incidents_in_process WHERE wug_ip_dStartTime >= DATEADD(month, -1, GETDATE()) AND wug_ip_outage_duration < ? AND wug_ip_sStateName = 'Up') as b");
		sqlQuery.append(" on lm_hd_tc_id = wug_ip_tc_id");
		sqlQuery.append(" and lm_hd_tc_id =? ");

		Object[] params = new Object[] { ticketId, duration, ticketId };

		Long ticketIdNew = null;
		try {
			ticketIdNew = jdbcTemplateIlex.queryForLong(sqlQuery.toString(),
					params);
		} catch (EmptyResultDataAccessException e) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#getNextActionOverdue(java.lang.String)
	 */
	@Override
	public boolean getNextActionOverdue(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select lm_hd_next_action_overdue from lm_help_desk_ticket_details where lm_hd_tc_id=?");
		Object[] params = new Object[] { ticketId };
		boolean nextActionOverdue = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), params, Boolean.class);
		return nextActionOverdue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#isTicketUnderReviewByOther(java.lang.String
	 * )
	 */
	@Override
	public boolean isTicketUnderReviewByOther(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select lm_hd_under_review from lm_help_desk_ticket_details where lm_hd_tc_id=?");
		Object[] params = new Object[] { ticketId };
		Boolean isUnderReview = false;
		try {
			isUnderReview = jdbcTemplateIlex.queryForObject(
					sqlQuery.toString(), Boolean.class, params);
		} catch (EmptyResultDataAccessException e) {
			isUnderReview = false;
		}

		return isUnderReview;
	}

	@Override
	public String lockbyuser(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select isnull(lm_hd_assigned_to,0) from lm_help_desk_ticket_details where lm_hd_tc_id=?");
		Object[] params = new Object[] { ticketId };
		int userId = 0;
		String userName = "";
		try {
			userId = jdbcTemplateIlex.queryForInt(sqlQuery.toString(), params);
			if (userId != 0) {
				sqlQuery = new StringBuilder();
				sqlQuery.append("SELECT lo_pc_first_name+' '+lo_pc_last_name as userName FROM lo_poc WHERE lo_pc_id = ?");
				params = new Object[] { userId };
				userName = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
						params, String.class);
			}

		} catch (EmptyResultDataAccessException e) {
			// isUnderReview = false;
			logger.debug(e.getMessage());
		}

		return userName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#isTicketCancelled(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean isTicketCancelled(String ackCubeType, String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		Object[] params;
		Boolean isCancelled = false;
		if (ackCubeType.equals("hdPendingAckCust")) {
			sqlQuery.append("select lm_hd_status from lm_help_desk_ticket_details where lm_hd_tc_id=?");
			params = new Object[] { ticketId };
			int status = jdbcTemplateIlex.queryForInt(sqlQuery.toString(),
					params);

			if (status == 8) {
				isCancelled = true;
			} else {
				isCancelled = false;
			}
		} else {
			sqlQuery.append("select 1 from lm_help_desk_ticket_details where lm_hd_tc_id=?");
			params = new Object[] { ticketId };
			try {
				jdbcTemplateIlex.queryForInt(sqlQuery.toString(), params);
			} catch (EmptyResultDataAccessException e) {
				isCancelled = true;
			}
		}
		return isCancelled;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#isTicketAcknowledged(java.lang.String)
	 */
	@Override
	public boolean isTicketAcknowledged(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select lm_hd_status from lm_help_desk_ticket_details where lm_hd_tc_id=?");
		Object[] params = new Object[] { ticketId };
		int hdStatus = 0;
		Boolean isAcknowledged;
		hdStatus = jdbcTemplateIlex.queryForInt(sqlQuery.toString(), params);
		if (hdStatus == 1) // status for new or unacknowledged tickets is 1
		{
			isAcknowledged = false;
		} else {
			isAcknowledged = true;
		}
		return isAcknowledged;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDMainDAO#setTicketUnderReview(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void setTicketUnderReview(String ticketId, String userId,
			String hdSessionTrackerId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("update lm_help_desk_ticket_details set lm_hd_under_review = 1,");
		sqlQuery.append(" lm_hd_session_id=?,");
		sqlQuery.append(" lm_hd_assigned_to=?,");
		sqlQuery.append(" lm_hd_update_date=getdate()");
		sqlQuery.append(" where lm_hd_tc_id=?");
		Object[] params = new Object[] { hdSessionTrackerId, userId, ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#clearReviewStatus(java.lang.String)
	 */
	@Override
	public void clearReviewStatus(String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			ticketId = "0";
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("update lm_help_desk_ticket_details set lm_hd_under_review = 0,");
		sqlQuery.append(" lm_hd_session_id=null,");
		sqlQuery.append(" lm_hd_assigned_to=null,");
		sqlQuery.append(" lm_hd_update_date=getdate()");
		sqlQuery.append(" where lm_hd_tc_id=?");
		Object[] params = new Object[] { ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	private Long getJobIdByTicket(String ticketNumber) {
		String sqlQuery = "SELECT Top 1 isnull(lm_tc_js_id, 0) lm_tc_js_id FROM lm_ticket WHERE lm_tc_id = ? order by lm_tc_js_id desc";

		Object[] params = new Object[] { ticketNumber };
		Long jobId = jdbcTemplateIlex.queryForLong(sqlQuery, params);

		return jobId;
	}

	public String getClientEmailAddress(Long appendixId) {
		String selectCaller = "SELECT  IsNULL(lx_pr_email,'') lx_pr_email  FROM lx_appendix_main  where lx_pr_id=? ";
		Object[] params = new Object[] { appendixId };
		String emailCaller = "";
		try {
			emailCaller = jdbcTemplateIlex.queryForObject(selectCaller, params,
					String.class);
		} catch (EmptyResultDataAccessException e) {
			emailCaller = "";
		}

		return emailCaller;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#saveAuditTrail(com.ilex.ws.core.bean.
	 * HDAuditTrailVO)
	 */
	@Override
	public void saveEventCorrelator(EventCorrelatorVO vo) {

		StringBuilder sqlQuery2 = new StringBuilder();
		int recordexistence = 0;
		Object[] params;
		recordexistence = getEventCorrelatorExistence(vo);
		if (recordexistence == 0) {
			sqlQuery2
					.append("INSERT INTO lm_help_desk_event_correlator (lm_hd_ec_type, lm_hd_ec_mm_id, lm_hd_ec_pr_id, lm_hd_ec_pof, "
							+ " lm_hd_ec_duration, lm_hd_ec_threshold, lm_hd_ec_color, lm_hd_ec_added_by, lm_hd_ec_updated_by, lm_hd_ec_updated_date) VALUES "
							+ " (?,?,?,?,?,?,?,?,?,getdate())");
			params = new Object[] { vo.getType(), vo.getMmId(), vo.getPrId(),
					vo.getCondition(), vo.getDuration(), vo.getThreshold(),
					vo.getColor(), vo.getAddedBy(), vo.getUpdatedBy() };

		} else {

			sqlQuery2.append("update  lm_help_desk_event_correlator  "
					+ " set lm_hd_ec_color =? ," + " lm_hd_ec_updated_by = ? ,"
					+ " lm_hd_ec_updated_date=getdate()"
					+ " where  lm_hd_ec_id = ?");
			/*
			 * SET lm_hd_ec_color = 'Green' , lm_hd_ec_updated_by = 4 ,
			 * lm_hd_ec_updated_date = getdate() WHERE lm_hd_ec_id = 18
			 */

			params = new Object[] { vo.getColor(), vo.getUpdatedBy(),
					recordexistence };

			/*
			 * StringBuilder sqlQuery = new StringBuilder(); sqlQuery.append(
			 * "update lm_help_desk_ticket_details set lm_hd_under_review = 0,"
			 * ); sqlQuery.append(" lm_hd_session_id=null,");
			 * sqlQuery.append(" lm_hd_assigned_to=null,");
			 * sqlQuery.append(" lm_hd_update_date=getdate()");
			 * sqlQuery.append(" where lm_hd_tc_id=?"); Object[] params = new
			 * Object[] { ticketId };
			 * jdbcTemplateIlex.update(sqlQuery.toString(), params);
			 */
		}

		jdbcTemplateIlex.update(sqlQuery2.toString(), params);

	}

	// get record exit check
	public int getEventCorrelatorExistence(EventCorrelatorVO vo) {
		String sqlQuery = "";
		int jobId = 0;
		if (vo.getType().equals("Customer")) {
			try {
				sqlQuery = "SELECT  lm_hd_ec_id     FROM lm_help_desk_event_correlator WHERE lm_hd_ec_pof =? and lm_hd_ec_mm_id = ? and lm_hd_ec_pr_id = ? and lm_hd_ec_duration = ? and lm_hd_ec_threshold =?";
				Object[] params = new Object[] { vo.getCondition(),
						vo.getMmId(), vo.getPrId(), vo.getDuration(),
						vo.getThreshold() };

				jobId = jdbcTemplateIlex.queryForInt(sqlQuery, params);

				// jobId = true;
				// }
			} catch (EmptyResultDataAccessException e) {
				return jobId;

			}
		} else if (vo.getType().equals("Staff")) {

			try {
				sqlQuery = "SELECT  lm_hd_ec_id     FROM lm_help_desk_event_correlator WHERE lm_hd_ec_mm_id Is Null  and lm_hd_ec_duration = ?  and lm_hd_ec_threshold =? ";
				Object[] params = new Object[] { vo.getDuration(),
						vo.getThreshold() };
				jobId = jdbcTemplateIlex.queryForInt(sqlQuery, params);

				// if (jobId != 1) {
				// flag = true;
				// }
			} catch (EmptyResultDataAccessException e) {
				return jobId;

			}

		}

		return jobId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.HDMainDAO#getEventCorrelator()
	 */
	@Override
	public List<EventCorrelatorVO> getEventCorrelator(String type) {

		StringBuilder sqlQuery = new StringBuilder(
				"SELECT lm_hd_ec_id, lm_hd_ec_type, isnull(lm_hd_ec_mm_id, -1) lm_hd_ec_mm_id, "
						+ " isnull(lm_hd_ec_pr_id, -1) lm_hd_ec_pr_id, isnull(lm_hd_ec_pof, 0) lm_hd_ec_pof,   "
						+ " lm_hd_ec_duration, lm_hd_ec_threshold, lm_hd_ec_color, lm_hd_ec_added_by, lm_hd_ec_updated_by, "
						+ " lm_hd_ec_updated_date,  lo_pc_first_name+ ' '+lo_pc_last_name pocName,  "
						+ " CASE "
						+ " 	WHEN lm_hd_ec_pr_id = 0 THEN 'All' "
						+ " 	else isnull(lx_pr_title, 'NOC')  "
						+ " END lx_pr_title,  "
						+ " CASE "
						+ " 	WHEN lm_hd_ec_mm_id = 0 THEN 'All' "
						+ " 	else isnull(lx_pr_end_customer, 'Staff Member') "
						+ " END lx_pr_end_customer, "
						+ " isnull(wug_io_desc,'Updated By NOC') wug_io_desc "
						+ " FROM lm_help_desk_event_correlator  Left JOIN lo_poc ON lo_pc_id = lm_hd_ec_updated_by "
						+ " left JOIN lx_appendix_main ON lx_pr_id = lm_hd_ec_pr_id "
						+ " LEFT JOIN wug_issue_owner ON wug_io_id = lm_hd_ec_pof  ORDER BY lm_hd_ec_updated_date desc  ");

		List<EventCorrelatorVO> vo = jdbcTemplateIlex.query(
				sqlQuery.toString(), new EventCorrelator());
		return vo;
	}

	/**
	 * The Class HDCustomer.
	 */
	private static final class EventCorrelator implements
			RowMapper<EventCorrelatorVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public EventCorrelatorVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			EventCorrelatorVO vo = new EventCorrelatorVO();
			vo.setId(rs.getString("lm_hd_ec_id"));
			vo.setAddedBy(rs.getString("lm_hd_ec_added_by"));
			vo.setColor(rs.getString("lm_hd_ec_color"));
			vo.setCondition(rs.getString("lm_hd_ec_pof"));
			vo.setDuration(rs.getString("lm_hd_ec_duration"));
			vo.setMmId(rs.getString("lm_hd_ec_mm_id"));
			vo.setPrId(rs.getString("lm_hd_ec_pr_id"));
			vo.setThreshold(rs.getString("lm_hd_ec_threshold"));
			vo.setType(rs.getString("lm_hd_ec_type"));
			vo.setUpdatedBy(rs.getString("lm_hd_ec_updated_by"));
			vo.setUpdateDate(rs.getString("lm_hd_ec_updated_date"));
			vo.setPocName(rs.getString("pocName"));
			vo.setProjectTitle(rs.getString("lx_pr_title"));
			vo.setCustomerTitle(rs.getString("lx_pr_end_customer"));
			vo.setPofTitle(rs.getString("wug_io_desc"));

			return vo;
		}
	}

	@Override
	public void deleteEventCorrelator(String id) {
		Object[] params = new Object[] { id };

		StringBuilder st = new StringBuilder(
				"DELETE FROM lm_help_desk_event_correlator WHERE lm_hd_ec_id = ?");
		params = new Object[] { id };
		jdbcTemplateIlex.update(st.toString(), params);

	}

}
