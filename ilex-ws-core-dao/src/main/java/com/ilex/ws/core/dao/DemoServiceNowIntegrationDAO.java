package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.IlexTicketVO;
import com.ilex.ws.core.bean.ServiceNowTicketDetailVO;
import com.ilex.ws.core.bean.ServiceNowTicketVO;
import com.ilex.ws.core.util.HelpDeskTicketType;
import com.ilex.ws.servicenow.dto.ResponseDTO;

/**
 * The Interface DemoServiceNowIntegrationDAO.
 */
public interface DemoServiceNowIntegrationDAO {

	/**
	 * Insert ticket to ilex.
	 * 
	 * @param serviceNowTicketVO
	 *            the service now ticket vo
	 * @return the int
	 */
	int insertTicketToIlex(ServiceNowTicketVO serviceNowTicketVO);

	/**
	 * Update ticket status.
	 * 
	 * @param insertStatus
	 *            the insert status
	 * @param ticketId
	 *            the ticket id
	 */
	void updateTicketStatus(boolean insertStatus, int ticketId);

	/**
	 * Insert service now response.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param responseDTO
	 *            the response dto
	 */
	void insertServiceNowResponse(int ticketId, ResponseDTO responseDTO);

	/**
	 * Gets the ilex tickets.
	 * 
	 * @return the ilex tickets
	 */
	List<IlexTicketVO> getTicketDetails(HelpDeskTicketType helpDeskTicketType);

	void addComment(String comments, String workNotes, String ticketNumber);

	ServiceNowTicketDetailVO getServiceNowComments(String ticketNumber);

}
