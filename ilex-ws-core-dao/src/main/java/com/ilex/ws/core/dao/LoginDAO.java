package com.ilex.ws.core.dao;

import com.mind.bean.LoginForm;

public interface LoginDAO {

	LoginForm getUserInfo(String Username);

	LoginForm getUserInfoByUserId(String userId);

	String checkUserRoleByRole(String userid, String role);

}
