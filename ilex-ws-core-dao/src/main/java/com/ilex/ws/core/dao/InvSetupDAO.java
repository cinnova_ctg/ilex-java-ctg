package com.ilex.ws.core.dao;

import com.ilex.ws.core.bean.InvBillToAddressVO;
import com.ilex.ws.core.bean.InvSetupVO;

/**
 * The Interface InvSetupDAO.
 */
public interface InvSetupDAO {

	/**
	 * Insert inv setup details.
	 * 
	 * @param invSetupVO
	 *            the inv setup vo
	 * @return the int
	 */
	int insertInvSetupDetails(InvSetupVO invSetupVO);

	/**
	 * Gets the inv setup details.
	 * 
	 * @param invSetupId
	 *            the inv setup id
	 * @return the inv setup details
	 */
	InvSetupVO getInvSetupDetails(int invSetupId);

	/**
	 * Delete inv setup details.
	 * 
	 * @param invBillToAddressId
	 *            the inv bill to address id
	 */
	void deleteInvSetupDetails(int invBillToAddressId);

	/**
	 * Update inv setup profile.
	 * 
	 * @param invSetupVO
	 *            the inv setup vo
	 */
	void updateInvSetupProfile(InvSetupVO invSetupVO);

	/**
	 * Update line item details.
	 * 
	 * @param invSetupVO
	 *            the inv setup vo
	 */
	void updateLineItemDetails(InvSetupVO invSetupVO);

	/**
	 * Update summary sheet details.
	 * 
	 * @param invSetupVO
	 *            the inv setup vo
	 */
	void updateSummarySheetDetails(InvSetupVO invSetupVO);

	/**
	 * Update client info details.
	 * 
	 * @param invSetupVO
	 *            the inv setup vo
	 */
	void updateClientInfoDetails(InvSetupVO invSetupVO);

	InvSetupVO getInvSetupDetails(InvBillToAddressVO invBillToAddressVO);

}
