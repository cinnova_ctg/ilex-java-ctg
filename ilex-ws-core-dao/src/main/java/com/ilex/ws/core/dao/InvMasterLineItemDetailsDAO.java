package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.InvMasterLineItemDetailsVO;

/**
 * The Interface InvMasterLineItemDetailsDAO.
 */
public interface InvMasterLineItemDetailsDAO {

	/**
	 * Gets the line item details.
	 * 
	 * @param projectType
	 *            the project type
	 * @return the line item details
	 */
	List<InvMasterLineItemDetailsVO> getLineItemDetails(String projectType);

}
