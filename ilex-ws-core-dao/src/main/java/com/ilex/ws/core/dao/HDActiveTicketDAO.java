package com.ilex.ws.core.dao;

import java.util.Date;
import java.util.List;

import com.ilex.ws.core.bean.HDSessionVO;
import com.ilex.ws.core.bean.HDTicketVO;
import com.ilex.ws.core.bean.HDWIPTableVO;
import com.ilex.ws.core.bean.TicketDropResponseVO;

public interface HDActiveTicketDAO {

	String getHDSessionTrackerId(String userId, String sessionId,
			String domainName);

	TicketDropResponseVO isTicketAvailableForDrop(Long ticketId,
			String hdSessionTrackerId, String userId);

	void removeTicketLock(HDSessionVO hdSessionVO);

	Boolean closeActiveTicket(Long ticketId, String userId);

	/**
	 * Gets the hDWIP ticket details.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the hDWIP ticket details
	 */
	List<HDWIPTableVO> getHDWIPTicketDetails(String ticketId);

	/**
	 * Update next action.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param nextAction
	 *            the next action
	 * @param nextActionDueDate
	 *            the next action due date
	 * @param userId
	 *            the user id
	 */
	void updateNextAction(String ticketId, String nextAction,
			Date nextActionDueDate, String userId);

	HDTicketVO getTicketDetailForDashboard(String ticketId);

}
