package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.dao.AppendixCustomerInfo;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.MessageKeyConstant;

@Repository
public class AppendixCustomerInfoImpl implements AppendixCustomerInfo {
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	@Override
	public List<String> getCustInfoParameter(int appendixId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select lm_ap_cust_info_parameter from dbo.lm_appendix_customer_info");
		sqlQuery.append(" where lm_ap_cust_info_pr_id = ? and lm_ap_cust_info_parameter<>''");
		sqlQuery.append(" order by lm_ap_cust_info_parameter");
		Object[] params = new Object[] { appendixId };

		List<String> custInfoParameterList = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new AppendixCustomerInfoMapper());

		if (custInfoParameterList == null || custInfoParameterList.size() == 0) {
			throw new IlexBusinessException(
					MessageKeyConstant.CUSTOMER_INFO_NOT_FOUND);
		}
		return custInfoParameterList;
	}

	/**
	 * The Class AppendixCustomerInfoMapper.
	 */
	private static final class AppendixCustomerInfoMapper implements
			RowMapper<String> {

		@Override
		public String mapRow(ResultSet rs, int rowNum) throws SQLException {
			return rs.getString("lm_ap_cust_info_parameter");
		}
	}

}
