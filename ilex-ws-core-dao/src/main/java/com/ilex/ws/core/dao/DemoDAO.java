package com.ilex.ws.core.dao;

import com.ilex.ws.core.bean.UserDTO;

public interface DemoDAO {

	public UserDTO getUserInfo(String userId);

}
