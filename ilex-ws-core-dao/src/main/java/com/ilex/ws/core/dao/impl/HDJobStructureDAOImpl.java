package com.ilex.ws.core.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.HDTicketVO;
import com.ilex.ws.core.bean.ResourceDetailVO;
import com.ilex.ws.core.dao.HDJobStructureDAO;

/**
 * The Class HDMainDAOImpl.
 */
@Repository
public class HDJobStructureDAOImpl implements HDJobStructureDAO {

	/** The Constant logger. */
	public static final Logger logger = Logger
			.getLogger(HDJobStructureDAOImpl.class);

	/** The jdbc template ilex. */
	@Resource(name = "jdbcTemplateIlex")
	private JdbcTemplate jdbcTemplateIlex;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDJobStructureDAO#insertJobRecord(com.ilex.ws.core
	 * .bean.HDTicketVO, java.lang.Long)
	 */
	@Override
	public Long insertJobRecord(final HDTicketVO hdTicketVO, final Long userId) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		final StringBuilder sqlQuery = new StringBuilder(
				"insert into lm_job(lm_js_si_id,lm_js_pr_id,");
		sqlQuery.append("lm_js_sequence_number,lm_js_title,lm_js_status,");
		sqlQuery.append("lm_js_prj_status,lm_js_margin,");
		sqlQuery.append("lm_js_priority,lm_js_planned_start_date,lm_js_planned_end_date,");
		sqlQuery.append("lm_js_se_schedule,lm_js_changed_by,lm_js_created_by,lm_js_create_date,lm_js_change_date,");
		sqlQuery.append("lm_js_type,lm_js_activity_used,lm_js_submitted_date,lm_js_invoice_no,lm_js_associated_date,lm_js_netmedx_job_flag,");
		sqlQuery.append("lm_js_customer_reference,lm_js_closed_date,first_visit_success,lm_js_addendum_flag,lm_js_union_uplift_factor,lm_js_expedite24_uplift_factor,");
		sqlQuery.append("lm_js_expedite48_uplift_factor,lm_js_afterhours_uplift_factor,lm_js_wh_uplift_factor,lm_js_export_status,lm_js_complete_success,lm_js_professional,lm_js_customer_followup,lm_js_offsite_date,lm_js_scheduler,");
		sqlQuery.append("lm_js_cnf_status,lm_js_prj_display_status,lm_js_rse_count,lm_js_rse_date,lm_js_test_indicator,lm_js_doc_id,lm_js_reason_for_change,lm_js_poc_tech,lm_js_poc_number");
		sqlQuery.append(") values (?,?,?,?,?,?,?,?,getDate(),?,?,?,?,getDate(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						sqlQuery.toString(), new String[] { "id" });
				ps.setString(1, hdTicketVO.getSiteId());// lm_js_si_id
				ps.setLong(2, hdTicketVO.getAppendixId());// lm_js_pr_id
				ps.setString(3, null);// lm_js_sequence_number to null
				ps.setString(4, hdTicketVO.getTicketNumber() + "-HD");
				ps.setString(5, "A");// lm_js_status to A
				ps.setString(6, "I");// lm_js_prj_status To I
				ps.setString(7, null);// lm_js_margin
				ps.setString(8, null);// lm_js_priority

				ps.setString(9, null);// lm_js_planned_end_date
				ps.setString(10, null);// lm_js_se_schedule
				ps.setString(11, null);// lm_js_changed_by
				ps.setLong(12, userId);// lm_js_created_by

				ps.setString(13, null);// lm_js_change_date
				ps.setString(14, "inscopejob");// lm_js_type
				ps.setString(15, "-1");// lm_js_activity_used
				ps.setString(16, null);// lm_js_submitted_date
				ps.setString(17, null);// lm_js_invoice_no
				ps.setString(18, null);// lm_js_associated_date
				ps.setString(19, "F");// lm_js_netmedx_job_flag
				ps.setString(20, hdTicketVO.getCustomerTicketReference());
				ps.setString(21, null);// lm_js_closed_date
				ps.setString(22, null);// first_visit_success
				ps.setString(23, null);// lm_js_addendum_flag
				ps.setString(24, null);// lm_js_union_uplift_factor
				ps.setString(25, null);// lm_js_expedite24_uplift_factor
				ps.setString(26, null);// lm_js_expedite48_uplift_factor
				ps.setString(27, null);// lm_js_afterhours_uplift_factor
				ps.setString(28, null);// lm_js_wh_uplift_factor
				ps.setString(29, null);// lm_js_export_status
				ps.setString(30, null);// lm_js_complete_success
				ps.setString(31, null);// lm_js_professional
				ps.setString(32, null);// lm_js_customer_followup
				ps.setString(33, null);// lm_js_offsite_date
				ps.setString(34, null);// lm_js_scheduler
				ps.setString(35, null);// lm_js_cnf_status
				ps.setString(36, null);// lm_js_prj_display_status
				ps.setString(37, null);// lm_js_rse_count
				ps.setString(38, null);// lm_js_rse_date
				ps.setString(39, "None");// lm_js_test_indicator
				ps.setString(40, null);// lm_js_doc_id
				ps.setString(41, null);// lm_js_reason_for_change
				ps.setString(42, null);// lm_js_poc_tech
				ps.setString(43, null);// lm_js_poc_number
				return ps;
			}
		};

		jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);

		Long jobId = keyHolder.getKey().longValue();
		return jobId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDJobStructureDAO#insertActivityRecord(java.lang
	 * .Long, java.lang.Long)
	 */
	@Override
	public Long insertActivityRecord(final Long jobId, final Long userId,
			final Boolean isTicketManualCreateAndBillable) {

		KeyHolder keyHolder = new GeneratedKeyHolder();
		final StringBuilder sqlQuery = new StringBuilder(
				"insert into lm_activity(lm_at_js_id,");
		sqlQuery.append("lm_at_title,lm_at_se_schedule,lm_at_status,");
		sqlQuery.append("lm_at_prj_status,lm_at_priority,");
		sqlQuery.append("lm_at_visibility,lm_at_created_by,lm_at_create_date,");
		sqlQuery.append("lm_at_margin,lm_at_quantity,lm_at_changed_by,lm_at_change_date,lm_at_type,");
		sqlQuery.append("lm_at_lib_id,lm_at_planned_start_date,lm_at_planned_end_date,lm_at_addendum_flag,lm_at_copy_flag,lm_at_union_uplift_factor,");
		sqlQuery.append("lm_at_expedite24_uplift_factor,lm_at_expedite48_uplift_factor,lm_at_afterhours_uplift_factor,lm_at_wh_uplift_factor,lm_at_oos_flag,lm_at_bid_flag");
		sqlQuery.append(") values (?,?,?,?,?,?,?,?,getDate(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						sqlQuery.toString(), new String[] { "id" });
				ps.setLong(1, jobId);
				ps.setString(2, "Troubleshoot and Repair");
				ps.setString(3, null);// lm_at_se_schedule
				ps.setString(4, "A");// lm_at_status to A
				ps.setString(5, "I");// lm_at_prj_status To I
				ps.setString(6, null);// lm_at_priority
				ps.setString(7, null);// lm_at_visibility
				ps.setLong(8, userId);// lm_at_created_by

				ps.setString(9, null);// lm_at_margin
				ps.setString(10, "1");// lm_at_quantity
				ps.setString(11, null);// lm_at_changed_by
				ps.setString(12, null);// lm_at_change_date
				if (isTicketManualCreateAndBillable) {
					ps.setString(13, "O");// lm_at_type
				} else {
					ps.setString(13, "E");// lm_at_type
				}
				ps.setInt(14, 0);// lm_at_lib_id
				ps.setString(15, null);// lm_at_planned_start_date
				ps.setString(16, null);// lm_at_planned_end_date
				ps.setString(17, null);// lm_at_addendum_flag
				ps.setString(18, null);// lm_at_copy_flag
				ps.setString(19, "N");// lm_at_union_uplift_factor
				ps.setString(20, "N");// lm_at_expedite24_uplift_factor
				ps.setString(21, "N");// lm_at_expedite48_uplift_factor
				ps.setString(22, "N");// lm_at_afterhours_uplift_factor
				ps.setString(23, "N");// lm_at_wh_uplift_factor
				ps.setString(24, null);// lm_at_oos_flag
				ps.setBoolean(25, false);// lm_at_bid_flag

				return ps;
			}
		};

		jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);

		Long activityId = keyHolder.getKey().longValue();
		return activityId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDJobStructureDAO#insertResourceRecord(com.ilex.
	 * ws.core.bean.HDTicketVO, java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public ResourceDetailVO insertResourceRecord(final HDTicketVO hdTicketVO,
			final Long activityId, final Long jobId, final Long userId) {

		Long ticketId = Long.valueOf(hdTicketVO.getTicketId());

		// fetching resource level
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select lm_tc_resource_level from lm_ticket where lm_tc_id=?");

		Object[] params = new Object[] { ticketId };
		final Long resourceLevel = jdbcTemplateIlex.queryForLong(
				sqlQuery.toString(), params);

		// Fetching clr detail id
		sqlQuery = new StringBuilder();
		sqlQuery.append("select clr_detail_id");
		sqlQuery.append(" from clr_master a");
		sqlQuery.append(" join clr_detail b on a.clr_master_id = b.clr_master_id");
		sqlQuery.append(" join lm_ticket on clr_cat_id = lm_tc_crit_cat_id");
		sqlQuery.append(" join lm_help_desk_ticket_details on lm_tc_id= lm_hd_tc_id");
		sqlQuery.append(" where lm_tc_id = ? and iv_rp_id = ?");
		sqlQuery.append(" and a.clr_netmedx_id = lm_hd_lx_pr_id");
		// sqlQuery.append(" and (a.clr_netmedx_id = lm_hd_lx_pr_id or a.clr_netmedx_id is null)");

		params = new Object[] { ticketId, resourceLevel };

		final Long clrDetailId = jdbcTemplateIlex.queryForLong(
				sqlQuery.toString(), params);

		// Fetching labour cost
		sqlQuery = new StringBuilder();
		sqlQuery.append("select clr_labor_rate");
		sqlQuery.append(" from clr_master a");
		sqlQuery.append(" join clr_detail b on a.clr_master_id = b.clr_master_id");
		sqlQuery.append(" join lm_ticket on clr_cat_id = lm_tc_crit_cat_id");
		sqlQuery.append(" join lm_help_desk_ticket_details on lm_tc_id= lm_hd_tc_id");
		sqlQuery.append(" where lm_tc_id = ? and iv_rp_id = ?");
		sqlQuery.append(" and a.clr_netmedx_id = lm_hd_lx_pr_id");
		// sqlQuery.append(" and (a.clr_netmedx_id = lm_hd_lx_pr_id or a.clr_netmedx_id is null)");

		params = new Object[] { ticketId, resourceLevel };

		final Double labourCost = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), params, Double.class);

		// Fetching partNumber
		sqlQuery = new StringBuilder();
		sqlQuery.append("select iv_rp_cns_part_number  from iv_resource_pool where iv_rp_id=?");

		params = new Object[] { resourceLevel };

		final String partNumber = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), params, String.class);

		// Fetching cost
		sqlQuery = new StringBuilder();
		sqlQuery.append("select iv_cost  from iv_resource_pool where iv_rp_id=?");

		params = new Object[] { resourceLevel };

		final Double cost = jdbcTemplateIlex.queryForObject(
				sqlQuery.toString(), params, Double.class);

		KeyHolder keyHolder = new GeneratedKeyHolder();

		final StringBuilder sqlQueryResource = new StringBuilder();
		sqlQueryResource.append("insert into lm_resource (");
		sqlQueryResource
				.append(" lm_rs_at_id,lm_rs_rp_id,lm_rs_iv_cns_part_number,lm_rs_lib_id,lm_rs_status,lm_rs_quantity");
		sqlQueryResource
				.append("  ,lm_rs_type,lm_rs_priority,lm_rs_margin,lm_rs_ce_cost,lm_rs_created_by,lm_rs_create_date");
		sqlQueryResource
				.append(" ,lm_rs_sw_sow,lm_rs_out_of_scope,lm_rs_as_assumptions,lm_rs_changed_date,lm_rs_changed_by");
		sqlQueryResource
				.append(" ,lm_rs_transit,lm_rs_clr_detail_id,lm_rs_trouble_and_repair_flag) ");
		sqlQueryResource
				.append(" values(?,?,?,?,?,?,?,?,?,?,?,getdate(),?,?,?,?,?,?,?,?)");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						sqlQueryResource.toString(), new String[] { "id" });
				ps.setLong(1, activityId);// lm_rs_at_id
				ps.setLong(2, resourceLevel);// lm_rs_rp_id
				ps.setString(3, partNumber);// lm_rs_iv_cns_part_number
				ps.setString(4, null);// [lm_rs_lib_id]
				ps.setString(5, "D");// [lm_rs_status]
				ps.setInt(6, 0);// [lm_rs_quantity]
				ps.setString(7, "IL");// [lm_rs_type]
				ps.setString(8, null);// [lm_rs_priority]
				ps.setDouble(9, 1 - (cost / labourCost));// [lm_rs_margin]
				ps.setDouble(10, cost);// [lm_rs_ce_cost]
				ps.setLong(11, userId);// [lm_rs_created_by]
				ps.setString(12, null);// [lm_rs_sw_sow]
				ps.setString(13, null);// [lm_rs_out_of_scope]
				ps.setString(14, null);// [lm_rs_as_assumptions]
				ps.setString(15, null);// [lm_rs_changed_date]
				ps.setString(16, null);// [lm_rs_changed_by]
				ps.setString(17, "N");// [lm_rs_transit]
				ps.setLong(18, clrDetailId);// [lm_rs_clr_detail_id]
				ps.setString(19, "N");// [lm_rs_trouble_and_repair_flag]

				return ps;
			}
		};

		jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);
		Long resourceId = keyHolder.getKey().longValue();

		ResourceDetailVO resourceDetailVO = new ResourceDetailVO();
		resourceDetailVO.setResourceId(resourceId);
		resourceDetailVO.setUnitPrice(labourCost);
		resourceDetailVO.setUnitCost(cost);
		resourceDetailVO.setResourceType("IL");
		return resourceDetailVO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDJobStructureDAO#insertResourceCostElementRecord
	 * (com.ilex.ws.core.bean.ResourceDetailVO)
	 */
	@Override
	public void insertResourceCostElementRecord(
			final ResourceDetailVO resourceDetailVO) {

		KeyHolder keyHolder = new GeneratedKeyHolder();

		final StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("insert into lx_cost_element  (");
		sqlQuery.append(" lx_ce_used_in,lx_ce_type,lx_ce_date,lx_ce_quantity,lx_ce_margin,lx_ce_invoice_price,lx_ce_list_price,lx_ce_total_price");
		sqlQuery.append("  ,lx_ce_unit_price,lx_ce_total_cost,lx_ce_unit_cost,lx_ce_overhead_cost,lx_ce_locality_uplift,lx_ce_union_uplift,lx_ce_non_pps_cost");
		sqlQuery.append(" ,lx_ce_addendum_cost,lx_ce_addendum_price,lx_ce_change_order_cost,lx_ce_change_order_price,lx_ce_frieght_cost,lx_ce_taxes");
		sqlQuery.append(" ,lx_ce_tax_rate,lx_ce_cns_hq_labor_cost,lx_ce_cns_field_labor_cost,lx_ce_ctr_field_labor_cost,lx_ce_material_cost,lx_ce_travel_cost)");
		sqlQuery.append(" values(?,?,getdate(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						sqlQuery.toString(), new String[] { "id" });
				ps.setLong(1, resourceDetailVO.getResourceId());// [lx_ce_used_in]
				ps.setString(2, resourceDetailVO.getResourceType());// [lx_ce_type]
				ps.setDouble(3, 0);// [lx_ce_quantity]
				ps.setDouble(4,
						1 - (resourceDetailVO.getUnitCost() / resourceDetailVO
								.getUnitPrice()));// [lx_ce_margin]
				ps.setString(5, null);// [lx_ce_invoice_price]
				ps.setString(6, null);// [lx_ce_list_price]
				ps.setDouble(7, 0.00);// [lx_ce_total_price]
				ps.setDouble(8, resourceDetailVO.getUnitPrice());// [lx_ce_unit_price]
				ps.setDouble(9, 0.00);// [lx_ce_total_cost]
				ps.setDouble(10, resourceDetailVO.getUnitCost());// [lx_ce_unit_cost]
				ps.setString(11, null);// [lx_ce_overhead_cost]
				ps.setString(12, null);// [lx_ce_locality_uplift]
				ps.setString(13, null);// [lx_ce_union_uplift]
				ps.setString(14, null);// [lx_ce_non_pps_cost]
				ps.setString(15, null);// [lx_ce_addendum_cost]
				ps.setString(16, null);// [lx_ce_addendum_price]
				ps.setString(17, null);// [lx_ce_change_order_cost]
				ps.setString(18, null);// [lx_ce_change_order_price]
				ps.setString(19, null);// [lx_ce_frieght_cost]
				ps.setString(20, null);// [lx_ce_taxes]
				ps.setString(21, null);// [lx_ce_tax_rate]
				ps.setDouble(22, 0.00);// [lx_ce_cns_hq_labor_cost]
				ps.setDouble(23, 0.00);// [lx_ce_cns_field_labor_cost]
				ps.setString(24, null);// [lx_ce_ctr_field_labor_cost]
				ps.setString(25, null);// [lx_ce_material_cost]
				ps.setString(26, null);// [lx_ce_travel_cost]

				return ps;
			}
		};

		jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDJobStructureDAO#insertJobActivityCostElementRecord
	 * (java.lang.Long, java.lang.Long)
	 */
	@Override
	public void insertJobActivityCostElementRecord(final Long jobId,
			final Long activityId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select lm_at_quantity from lm_activity where lm_at_id=?");
		Object[] params = new Object[] { activityId };

		int activityQuantity = 0;
		try {
			activityQuantity = jdbcTemplateIlex.queryForInt(
					sqlQuery.toString(), params);
		} catch (EmptyResultDataAccessException e) {
			activityQuantity = 1;
		}

		final int quantity = activityQuantity;

		// Job entry

		final StringBuilder sqlQueryJob = new StringBuilder();
		sqlQueryJob.append("insert into lx_cost_element  (");
		sqlQueryJob
				.append(" lx_ce_used_in,lx_ce_type,lx_ce_date,lx_ce_quantity");
		sqlQueryJob
				.append("  ,lx_ce_overhead_cost,lx_ce_locality_uplift,lx_ce_union_uplift,lx_ce_non_pps_cost");
		sqlQueryJob
				.append(" ,lx_ce_addendum_cost,lx_ce_addendum_price,lx_ce_change_order_cost,lx_ce_change_order_price,lx_ce_frieght_cost,lx_ce_taxes");
		sqlQueryJob
				.append(" ,lx_ce_tax_rate,lx_ce_ctr_field_labor_cost,lx_ce_material_cost,lx_ce_travel_cost)");
		sqlQueryJob
				.append(" values(?,?,getdate(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						sqlQueryJob.toString(), new String[] { "id" });
				ps.setLong(1, jobId);// lx_ce_used_in
				ps.setString(2, "IJ");// lx_ce_type
				ps.setInt(3, quantity);// lx_ce_quantity
				ps.setString(4, null);// lx_ce_overhead_cost
				ps.setString(5, null);// lx_ce_locality_uplift
				ps.setString(6, null);// lx_ce_union_uplift
				ps.setString(7, null);// lx_ce_non_pps_cost
				ps.setString(8, null);// lx_ce_addendum_cost
				ps.setString(9, null);// lx_ce_addendum_price
				ps.setString(10, null);// lx_ce_change_order_cost
				ps.setString(11, null);// lx_ce_change_order_price
				ps.setString(12, null);// lx_ce_frieght_cost
				ps.setString(13, null);// lx_ce_taxes
				ps.setString(14, null);// lx_ce_tax_rate
				ps.setString(15, null);// lx_ce_ctr_field_labor_cost
				ps.setString(16, null);// lx_ce_material_cost
				ps.setString(17, null);// lx_ce_travel_cost

				return ps;
			}
		};

		jdbcTemplateIlex.update(preparedStatementCreator);

		// Activity entry
		final StringBuilder sqlQueryActivity = new StringBuilder();
		sqlQueryActivity.append("insert into lx_cost_element  (");
		sqlQueryActivity
				.append(" lx_ce_used_in,lx_ce_type,lx_ce_date,lx_ce_quantity");
		sqlQueryActivity
				.append("  ,lx_ce_overhead_cost,lx_ce_locality_uplift,lx_ce_union_uplift,lx_ce_non_pps_cost");
		sqlQueryActivity
				.append(" ,lx_ce_addendum_cost,lx_ce_addendum_price,lx_ce_change_order_cost,lx_ce_change_order_price,lx_ce_frieght_cost,lx_ce_taxes");
		sqlQueryActivity
				.append(" ,lx_ce_tax_rate,lx_ce_ctr_field_labor_cost,lx_ce_material_cost,lx_ce_travel_cost)");
		sqlQueryActivity
				.append(" values(?,?,getdate(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						sqlQueryActivity.toString(), new String[] { "id" });
				ps.setLong(1, activityId);// lx_ce_used_in
				ps.setString(2, "IA");// lx_ce_type
				ps.setInt(3, quantity);// lx_ce_quantity
				ps.setString(4, null);// lx_ce_overhead_cost
				ps.setString(5, null);// lx_ce_locality_uplift
				ps.setString(6, null);// lx_ce_union_uplift
				ps.setString(7, null);// lx_ce_non_pps_cost
				ps.setString(8, null);// lx_ce_addendum_cost
				ps.setString(9, null);// lx_ce_addendum_price
				ps.setString(10, null);// lx_ce_change_order_cost
				ps.setString(11, null);// lx_ce_change_order_price
				ps.setString(12, null);// lx_ce_frieght_cost
				ps.setString(13, null);// lx_ce_taxes
				ps.setString(14, null);// lx_ce_tax_rate
				ps.setString(15, null);// lx_ce_ctr_field_labor_cost
				ps.setString(16, null);// lx_ce_material_cost
				ps.setString(17, null);// lx_ce_travel_cost

				return ps;
			}
		};

		jdbcTemplateIlex.update(preparedStatementCreator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDJobStructureDAO#updateTicketWithJobId(java.lang
	 * .Long, java.lang.Long)
	 */
	@Override
	public void updateTicketWithJobId(Long jobId, Long ticketId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("update lm_ticket set lm_tc_js_id= ? where lm_tc_id=?");

		Object[] params = new Object[] { jobId, ticketId };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.HDJobStructureDAO#createJobStructure(com.ilex.ws
	 * .core.bean.HDTicketVO, java.lang.Long)
	 */
	@Override
	public void createJobStructure(HDTicketVO hdTicketVO, Long userId,
			Boolean isTicketManualCreate) {
		Boolean isTicketManualCreateAndBillable = false;
		if (hdTicketVO.getBillingStatus() == true
				&& isTicketManualCreate == true) {
			isTicketManualCreateAndBillable = true;
		}
		Long jobId = insertJobRecord(hdTicketVO, Long.valueOf(userId));
		hdTicketVO.setJobId(jobId);
		Long activityId = insertActivityRecord(jobId, Long.valueOf(userId),
				isTicketManualCreateAndBillable);
		try {
			ResourceDetailVO resourceDetailVO = insertResourceRecord(
					hdTicketVO, activityId, jobId, Long.valueOf(userId));

			insertResourceCostElementRecord(resourceDetailVO);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		insertJobActivityCostElementRecord(jobId, activityId);
		updateTicketWithJobId(jobId, Long.valueOf(hdTicketVO.getTicketId()));
		String plannedStartDate = getStartDate(jobId);
		insertScheduleElement(jobId, plannedStartDate, null, "IJ", userId,
				plannedStartDate, null, null);
		insertSiteDetailRecord(Long.valueOf(hdTicketVO.getSiteId()),
				Long.valueOf(userId), jobId, 0l);

	}

	private void insertScheduleElement(Long jobId, String plannedStartDate,
			String endDate, String type, Long userId, String pmStartDate,
			String pmEndDate, String count) {

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplateIlex);
		simpleJdbcCall.withFunctionName("lx_schedule_manage_01");
		simpleJdbcCall.withReturnValue();

		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("lx_se_type_id", jobId);
		mapSqlParameterSource.addValue("lx_se_start", plannedStartDate);
		mapSqlParameterSource.addValue("lx_se_end", endDate);
		mapSqlParameterSource.addValue("lx_se_type", type);
		mapSqlParameterSource.addValue("lx_se_created_by", userId);
		mapSqlParameterSource.addValue("lx_se_pm_start_date", pmStartDate);
		mapSqlParameterSource.addValue("lx_se_pm_end_date", pmEndDate);
		mapSqlParameterSource.addValue("lx_se_count", count);

		Map<String, Object> returnMap = simpleJdbcCall
				.execute(mapSqlParameterSource);
	}

	private String getStartDate(Long jobId) {
		String sqlQuery = "select  lm_js_planned_start_date from lm_job where lm_js_id=?";

		Object[] params = new Object[] { jobId };

		String plannedStartDate = jdbcTemplateIlex.queryForObject(sqlQuery,
				params, String.class);

		return plannedStartDate;
	}

	private void insertSiteDetailRecord(Long lpSiteId, Long userId, Long jobId,
			Long lmSiteId) {
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplateIlex);
		simpleJdbcCall.withFunctionName("lm_newsite_manage_01");
		simpleJdbcCall.withReturnValue();

		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("site_list_id", lpSiteId, Types.INTEGER);
		mapSqlParameterSource.addValue("jobid", jobId, Types.INTEGER);
		mapSqlParameterSource.addValue("cuser", userId, Types.INTEGER);
		mapSqlParameterSource.addValue("siteId", lmSiteId, Types.INTEGER);

		Map<String, Object> returnMap = simpleJdbcCall
				.execute(mapSqlParameterSource);
	}
}
