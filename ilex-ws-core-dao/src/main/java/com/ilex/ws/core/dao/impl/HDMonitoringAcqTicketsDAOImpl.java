package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.HdAquiredTicketVO;
import com.ilex.ws.core.dao.HDMonitoringAcqTicketsDAO;

@Repository
public class HDMonitoringAcqTicketsDAOImpl implements HDMonitoringAcqTicketsDAO {

    @Resource(name = "jdbcTemplateIlex")
    private JdbcTemplate jdbcTemplateIlex;

    @Override
    public List<HdAquiredTicketVO> getAcquiredTickets() {

		StringBuilder sqlQuery = new StringBuilder(
				" select lm_hd_tc_id,lm_tc_number,session_Id,domain_server,lm_hd_assigned_to,left(lo_pc_first_name,1)+' '+left(lo_pc_last_name,10) as userName, lm_hd_status ");
		sqlQuery.append(" from dbo.lm_help_desk_ticket_details");
		sqlQuery.append(" join lm_ticket on lm_hd_tc_id=lm_tc_id");
		sqlQuery.append(" left join lm_help_desk_ticket_session_tracker on session_tracker_id=lm_hd_session_id");
		sqlQuery.append(" join lo_poc on lo_pc_id=lm_hd_assigned_to");
		sqlQuery.append(" where lm_hd_under_review ='1' ");
		/*
		 * sqlQuery.append("union"); // All tickets are availble on Help Desk
		 * Screen sqlQuery.append(
		 * "select lm_hd_tc_id,lm_tc_number,session_Id,domain_server,lm_hd_assigned_to,left(lo_pc_first_name,1)+' '+left(lo_pc_last_name,10) as userName, lm_hd_status "
		 * ); sqlQuery.append(" from dbo.lm_help_desk_ticket_details");
		 * sqlQuery.append(" join lm_ticket on lm_hd_tc_id=lm_tc_id");
		 * sqlQuery.append(
		 * " join lm_help_desk_ticket_session_tracker on session_tracker_id IS NOT NULL "
		 * ); sqlQuery.append(" join lo_poc on lo_pc_id=lm_hd_assigned_to");
		 * sqlQuery.append(" where lm_hd_under_review ='1' ");
		 */

        List<HdAquiredTicketVO> tickets = jdbcTemplateIlex.query(
                sqlQuery.toString(), new HDAquiredticketMapper());

        return tickets;

    }

    private static final class HDAquiredticketMapper implements
            ParameterizedRowMapper<HdAquiredTicketVO> {

        /*
         * (non-Javadoc)
         *
         * @see
         * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
         * int)
         */
        @Override
        public HdAquiredTicketVO mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            HdAquiredTicketVO hdAquiredTicketVO = new HdAquiredTicketVO();
            hdAquiredTicketVO.setHostName(rs.getString("domain_server"));
            hdAquiredTicketVO.setSessionId(rs.getString("session_Id"));
            hdAquiredTicketVO.setTicketId(rs.getLong("lm_hd_tc_id"));
            hdAquiredTicketVO.setTicketNumber(rs.getString("lm_tc_number"));
            hdAquiredTicketVO.setUserId(rs.getString("lm_hd_assigned_to"));
            hdAquiredTicketVO.setUserName(rs.getString("userName"));
            hdAquiredTicketVO.setTicketStatus(rs.getString("lm_hd_status"));

            return hdAquiredTicketVO;
        }
    }

    @Override
    public void releaseLock(Long ticketId) {
        String updateQuery = "update lm_help_desk_ticket_details set lm_hd_assigned_to=Null, lm_hd_session_id=null, lm_hd_under_review=0 where lm_hd_tc_id=?";
        Object[] param = new Object[]{ticketId};
        jdbcTemplateIlex.update(updateQuery, param);

    }

}
