package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.OrgDivisionVO;
import com.ilex.ws.core.bean.PointOfContactVO;
import com.ilex.ws.core.dao.PointOfContactDAO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.MessageKeyConstant;

/**
 * The Class PointOfContactDAOImpl.
 */
@Repository
public class PointOfContactDAOImpl implements PointOfContactDAO {

	/** The jdbc template ilex. */
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.PointOfContactDAO#getContactList(com.ilex.ws.core
	 * .bean.OrgDivisionVO)
	 */
	@Override
	public List<PointOfContactVO> getContactList(OrgDivisionVO argDivision) {
		StringBuilder sqlQuery = new StringBuilder(
				"select lo_pc_id,lo_pc_first_name,");
		sqlQuery.append(" lo_pc_last_name,lo_pc_phone1,");
		sqlQuery.append(" lo_pc_cell_phone,lo_pc_email1,lo_pc_fax");
		sqlQuery.append(" from lo_poc ");
		sqlQuery.append(" where lo_pc_om_id=?");
		Object[] params = new Object[] { argDivision.getDivisionId() };
		List<PointOfContactVO> pointOfContactVO;

		try {
			pointOfContactVO = jdbcTemplateIlex.query(sqlQuery.toString(),
					params, new DivisionMapper());
		} catch (EmptyResultDataAccessException ex) {
			throw new IlexBusinessException(MessageKeyConstant.POC_NOT_FOUND);
		}

		if (pointOfContactVO == null) {
			throw new IlexBusinessException(MessageKeyConstant.POC_NOT_FOUND);
		}
		return pointOfContactVO;
	}

	/**
	 * The Class DivisionMapper.
	 */
	private static final class DivisionMapper implements
			RowMapper<PointOfContactVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public PointOfContactVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			PointOfContactVO pointOfContactVO = new PointOfContactVO();
			pointOfContactVO.setPocId(rs.getString("lo_pc_id"));
			pointOfContactVO.setCellNo(rs.getString("lo_pc_cell_phone"));
			pointOfContactVO.setEmailID(rs.getString("lo_pc_email1"));
			pointOfContactVO.setFaxNo(rs.getString("lo_pc_fax"));
			pointOfContactVO.setFirstName(rs.getString("lo_pc_first_name"));
			pointOfContactVO.setLastName(rs.getString("lo_pc_last_name"));
			pointOfContactVO.setPhoneNo(rs.getString("lo_pc_phone1"));

			return pointOfContactVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.PointOfContactDAO#getContactDetail(com.ilex.ws.core
	 * .bean.PointOfContactVO)
	 */
	@Override
	public PointOfContactVO getContactDetail(PointOfContactVO argPointOfContact) {
		StringBuilder sqlQuery = new StringBuilder(
				"select lo_pc_id,lo_pc_first_name,");
		sqlQuery.append(" lo_pc_last_name,lo_pc_phone1,");
		sqlQuery.append(" lo_pc_cell_phone,lo_pc_email1,lo_pc_fax");
		sqlQuery.append(" from lo_poc ");
		sqlQuery.append(" where lo_pc_id=?");
		Object[] params = new Object[] { argPointOfContact.getPocId() };
		PointOfContactVO pointOfContactVO;

		try {
			pointOfContactVO = jdbcTemplateIlex.queryForObject(
					sqlQuery.toString(), params, new DivisionMapper());
		} catch (EmptyResultDataAccessException ex) {
			throw new IlexBusinessException(MessageKeyConstant.POC_NOT_FOUND);
}

		return pointOfContactVO;
	}

}
