package com.ilex.ws.core.dao;

// TODO: Auto-generated Javadoc
/**
 * The Interface TicketDispatchDAO.
 */
public interface TicketDispatchDAO {

	/**
	 * Gets the customer name.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @return the customer name
	 */
	String getCustomerName(Long appendixId);

}
