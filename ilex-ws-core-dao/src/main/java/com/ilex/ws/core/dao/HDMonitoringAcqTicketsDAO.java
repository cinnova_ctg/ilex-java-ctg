package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.HdAquiredTicketVO;

public interface HDMonitoringAcqTicketsDAO {

    List<HdAquiredTicketVO> getAcquiredTickets();

    void releaseLock(Long ticketId);

}
