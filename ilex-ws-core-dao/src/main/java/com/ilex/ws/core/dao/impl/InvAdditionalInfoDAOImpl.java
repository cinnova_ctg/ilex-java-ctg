package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.InvAdditionalInfoVO;
import com.ilex.ws.core.bean.InvSetupVO;
import com.ilex.ws.core.dao.InvAdditionalInfoDAO;

@Repository
public class InvAdditionalInfoDAOImpl implements InvAdditionalInfoDAO {
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	@Override
	public void insertAdditionalInfo(InvAdditionalInfoVO invAdditionalInfoVO) {
		StringBuilder sqlQuery = new StringBuilder(
				"insert into in_invoice_additional_information_setup (in_as_su_id,in_as_name,in_as_level,");
		sqlQuery.append("in_as_value,in_as_description");
		sqlQuery.append(") values (?,?,?,?,?)");

		Object[] params = new Object[] { invAdditionalInfoVO.getInvSetupId(),
				invAdditionalInfoVO.getName(), invAdditionalInfoVO.getLevel(),
				invAdditionalInfoVO.getValue(),
				invAdditionalInfoVO.getDescription() };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	@Override
	public void updateAdditionalInfo(InvAdditionalInfoVO invAdditionalInfoVO) {
		StringBuilder sqlQuery = new StringBuilder(
				"update  in_invoice_additional_information_setup in_as_name=?,in_as_level=?,");
		sqlQuery.append(" in_as_value=?,in_as_description=?");
		sqlQuery.append(" where in_as_id=?");

		Object[] params = new Object[] { invAdditionalInfoVO.getName(),
				invAdditionalInfoVO.getLevel(), invAdditionalInfoVO.getValue(),
				invAdditionalInfoVO.getDescription(),
				invAdditionalInfoVO.getAdditionalInfoId() };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	@Override
	public void deleteAdditionalInfo(final int invAdditionalInfoId) {
		StringBuilder deleteAdditionalInfoTable = new StringBuilder(
				"delete from in_invoice_additional_information_setup where in_as_id = ? ");
		Object[] params = new Object[] { invAdditionalInfoId };
		jdbcTemplateIlex.update(deleteAdditionalInfoTable.toString(), params);

	}

	@Override
	public void deleteAdditionalInfo(final InvSetupVO invSetupVO) {
		StringBuilder deleteAdditionalInfoTable = new StringBuilder(
				"delete from in_invoice_additional_information_setup where in_as_su_id = ? ");
		Object[] params = new Object[] { invSetupVO.getInvoiceSetupId() };
		jdbcTemplateIlex.update(deleteAdditionalInfoTable.toString(), params);

	}

	@Override
	public List<InvAdditionalInfoVO> getAdditionalInfo(InvSetupVO invSetupVO) {
		StringBuilder sqlQuery = new StringBuilder(
				"select* from in_invoice_additional_information_setup");
		sqlQuery.append(" where in_as_su_id =?");
		Object[] params = new Object[] { invSetupVO.getInvoiceSetupId() };

		List<InvAdditionalInfoVO> invAdditionalInfoVOList = jdbcTemplateIlex
				.query(sqlQuery.toString(), params, new AdditionalInfoMapper());

		return invAdditionalInfoVOList;
	}

	/**
	 * The Class AdditionalInfoMapper.
	 */
	private static final class AdditionalInfoMapper implements
			RowMapper<InvAdditionalInfoVO> {

		@Override
		public InvAdditionalInfoVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvAdditionalInfoVO invAdditionalInfoVO = new InvAdditionalInfoVO();
			invAdditionalInfoVO.setAdditionalInfoId(rs.getInt("in_as_id"));
			invAdditionalInfoVO.setInvSetupId(rs.getInt("in_as_su_id"));
			invAdditionalInfoVO.setName(rs.getString("in_as_name"));
			invAdditionalInfoVO.setLevel(rs.getString("in_as_level"));
			invAdditionalInfoVO.setValue(rs.getString("in_as_value"));
			invAdditionalInfoVO.setDescription(rs
					.getString("in_as_description"));
			return invAdditionalInfoVO;
		}
	}

}
