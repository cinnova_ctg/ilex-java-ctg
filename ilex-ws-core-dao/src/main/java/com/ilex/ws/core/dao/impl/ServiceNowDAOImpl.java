package com.ilex.ws.core.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.HDTicketLookUpVO;
import com.ilex.ws.core.bean.HDTicketVO;
import com.ilex.ws.core.bean.HDWorkNoteVO;
import com.ilex.ws.core.bean.IncidentVO;
import com.ilex.ws.core.bean.TicketStateVO;
import com.ilex.ws.core.dao.HDMainDAO;
import com.ilex.ws.core.dao.ServiceNowDAO;
import com.ilex.ws.core.dto.IncidentDTO;
import com.ilex.ws.servicenow.dto.ProactiveIncidentResponseDTO;
import com.ilex.ws.servicenow.dto.ProactiveIncidentTxrResponseDTO;
import com.ilex.ws.servicenow.dto.UpdateIncidentResponseDTO;
import com.ilex.ws.servicenow.dto.UpdateIncidentTxrResponseDTO;

/**
 * The Class ServiceNowDAOImpl.
 */
@Repository
public class ServiceNowDAOImpl implements ServiceNowDAO {

    /**
     * The Constant logger.
     */
    public static final Logger logger = Logger.getLogger(ServiceNowDAOImpl.class);

    /**
     * The jdbc template ilex.
     */
    @Resource(name = "jdbcTemplateIlex")
    private JdbcTemplate jdbcTemplateIlex;

    @Resource
    HDMainDAO hdMainDAO;

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.dao.ServiceNowDAO#insertProactiveIncidentToIlex(com.
     * ilex.ws.core.bean.IncidentVO, int)
     */
    @Override
    public Map<String, Long> insertProactiveIncidentToIlex(final IncidentDTO proactiveIncidentDTO, final int userId) {
        Map<String, Long> response = new HashMap<>();
        KeyHolder keyHolder = new GeneratedKeyHolder();
        final StringBuilder snMainQuery = new StringBuilder(" insert into ig_service_now_main ");
        snMainQuery.append(" (u_ticket_number,u_category,u_description ");
        snMainQuery.append(" ,u_impact,u_short_description,u_site_number ");
        snMainQuery.append(" ,u_state,u_symptom,u_type ");
        snMainQuery.append(" ,u_urgency,u_vendor_key,u_configuration_item ");
        snMainQuery.append(" ,servicenow_identifier,created_date,created_by ");
        snMainQuery.append(" ,integration_type,integration_status,lp_msa_id,lx_pr_id,integration_status_msg,u_work_notes,vTaskNumber) ");
        snMainQuery.append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,getdate(),?,?,?,?,?,?,?,?)");
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(snMainQuery.toString(), new String[]{"id"});
                ps.setString(1, proactiveIncidentDTO.getTicketNumber());
                ps.setString(2, proactiveIncidentDTO.getCategory());
                ps.setString(3, proactiveIncidentDTO.getDescription());
                ps.setString(4, proactiveIncidentDTO.getImpact());
                ps.setString(5, proactiveIncidentDTO.getShortDescription());
                ps.setString(6, proactiveIncidentDTO.getSiteNumber());
                ps.setString(7, proactiveIncidentDTO.getState());
                ps.setString(8, proactiveIncidentDTO.getSymptom());
                ps.setString(9, proactiveIncidentDTO.getType());
                ps.setString(10, proactiveIncidentDTO.getUrgency());
                ps.setString(11, proactiveIncidentDTO.getVendorKey());
                ps.setString(12, proactiveIncidentDTO.getConfigurationItem());
                ps.setString(13, proactiveIncidentDTO.getServiceNowSysId());
                ps.setInt(14, userId);
                ps.setString(15, proactiveIncidentDTO.getIntegrationType());
                ps.setString(16, proactiveIncidentDTO.getIntegrationStatus());
                ps.setString(17, proactiveIncidentDTO.getMsaId());
                ps.setString(18, proactiveIncidentDTO.getAppendixId());
                ps.setString(19, proactiveIncidentDTO.getIntegrationStatusMsg());
                ps.setString(20, proactiveIncidentDTO.getWorkNotes());
                ps.setString(21, proactiveIncidentDTO.getTicketNumber());
                return ps;
            }
        };

        jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);
        final Long incidentId = keyHolder.getKey().longValue();
        response.put("createIdentifier", incidentId);
        return response;

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.dao.ServiceNowDAO#updateProactiveSuccessfulResponse(
     * com.ilex.ws.servicenow.dto.ProactiveIncidentResponseDTO, int,
     * java.lang.Long)
     */
    @Override
    public void updateProactiveSuccessfulResponse(ProactiveIncidentResponseDTO responseDTO, int userId, Long createIdentifier) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" update ig_service_now_main set ");
        sqlQuery.append(" servicenow_identifier=?,integration_status=?,vTaskNumber=? ");
        sqlQuery.append(" where create_identifier=? ");
        Object[] params = new Object[]{responseDTO.getSysId(), "Successful", responseDTO.getTicketNumber(), createIdentifier};
        jdbcTemplateIlex.update(sqlQuery.toString(), params);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.dao.ServiceNowDAO#updateProactiveUnSuccessResponse(com
     * .ilex.ws.servicenow.dto.ProactiveIncidentResponseDTO, int,
     * java.lang.Long)
     */
    @Override
    public void updateProactiveUnSuccessResponse(
            ProactiveIncidentResponseDTO responseDTO, int userId,
            Long createIdentifier) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" update ig_service_now_main set ");
        sqlQuery.append(" servicenow_identifier=?,integration_status=?,vTaskNumber=?,integration_status_msg=? ");
        sqlQuery.append(" where create_identifier=? ");
        Object[] params = new Object[]{responseDTO.getSysId(), "Error", null,
            responseDTO.getErrorDescription(), createIdentifier};
        jdbcTemplateIlex.update(sqlQuery.toString(), params);

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.dao.ServiceNowDAO#getIncidentsList(java.lang.String,
     * java.lang.String)
     */
    @Override
    public List<IncidentVO> getIncidentsList(String integrationType,
            String msaId) {
        StringBuilder sqlQuery = new StringBuilder("");
        sqlQuery.append(" select top 25 u_ticket_number, servicenow_identifier, created_date, ");
        sqlQuery.append(" integration_status,u_site_number, left(u_short_description, 25) as 'u_short_description',create_identifier,vTaskNumber ");
        sqlQuery.append(" from ig_service_now_main ");
        sqlQuery.append(" where integration_type =? and lp_msa_id =? ");
        sqlQuery.append(" order by create_identifier desc ");
        Object[] param = new Object[]{integrationType, msaId};
        List<IncidentVO> incidentVOList = jdbcTemplateIlex.query(
                sqlQuery.toString(), param, new IncidentListDetailMapper());
        return incidentVOList;
    }

    /**
     * The Class IncidentListDetailMapper.
     */
    private static final class IncidentListDetailMapper implements
            RowMapper<IncidentVO> {

        /*
         * (non-Javadoc)
         *
         * @see
         * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
         * int)
         */
        public IncidentVO mapRow(ResultSet rs, int rowNum) throws SQLException {
            IncidentVO incidentVO = new IncidentVO();
            incidentVO.setTicketNumber(rs.getString("u_ticket_number"));
            incidentVO.setServiceNowTicketId(rs
                    .getString("servicenow_identifier"));
            incidentVO.setCreatedDate(rs.getTimestamp("created_date"));
            incidentVO.setIntegrationStatus(rs.getString("integration_status"));
            incidentVO.setSiteNumber(rs.getString("u_site_number"));
            incidentVO.setShortDescription(rs.getString("u_short_description"));
            incidentVO.setCreateIdentifier(rs.getString("create_identifier"));
            incidentVO.setvTaskNumber(rs.getString("vTaskNumber"));
            return incidentVO;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ilex.ws.core.dao.ServiceNowDAO#getTicketStates()
     */
    @Override
    public List<TicketStateVO> getTicketStates() {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("select lm_st_id, lm_st_status");
        sqlQuery.append(" from lm_help_desk_ticket_status");
        sqlQuery.append(" where lm_st_active = 1");
        sqlQuery.append(" order by lm_st_display_on_active_form_order");

        List<TicketStateVO> ticketStateVOList = jdbcTemplateIlex.query(
                sqlQuery.toString(), new RowMapper<TicketStateVO>() {
                    public TicketStateVO mapRow(ResultSet rs, int rowNum)
                    throws SQLException {
                        TicketStateVO ticketStateVO = new TicketStateVO();

                        ticketStateVO.setStateId(rs.getString("lm_st_id"));
                        ticketStateVO.setTicketState(rs
                                .getString("lm_st_status"));

                        return ticketStateVO;
                    }
                ;
        });

		return ticketStateVOList;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ilex.ws.core.dao.ServiceNowDAO#getCreateIncidentErrorLogs()
     */
    @Override
    public List<IncidentVO> getCreateIncidentErrorLogs() {
        StringBuilder sqlQuery = new StringBuilder("");
        sqlQuery.append(" select top 25 u_ticket_number, servicenow_identifier,vTaskNumber, created_date, integration_type, integration_status_msg ");
        sqlQuery.append(" from dbo.ig_service_now_main ");
        sqlQuery.append(" where integration_status = 'Error' ");
        sqlQuery.append(" order by created_date desc ");
        List<IncidentVO> createIncidentErrorVOList = jdbcTemplateIlex.query(
                sqlQuery.toString(), new CreateIncidentErrorLogMapper());
        return createIncidentErrorVOList;
    }

    /**
     * The Class CreateIncidentErrorLogMapper.
     */
    private static final class CreateIncidentErrorLogMapper implements
            RowMapper<IncidentVO> {

        /*
         * (non-Javadoc)
         *
         * @see
         * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
         * int)
         */
        public IncidentVO mapRow(ResultSet rs, int rowNum) throws SQLException {
            IncidentVO incidentVO = new IncidentVO();
            incidentVO.setTicketNumber(rs.getString("u_ticket_number"));
            incidentVO.setServiceNowTicketId(rs
                    .getString("servicenow_identifier"));
            incidentVO.setvTaskNumber(rs.getString("vTaskNumber"));
            incidentVO.setCreatedDate(rs.getTimestamp("created_date"));
            incidentVO.setIntegrationType(rs.getString("integration_type"));
            incidentVO.setIntegrationStatusMsg(rs
                    .getString("integration_status_msg"));
            return incidentVO;
        }
    }

    @Override
    public String validateSiteNumberForCFA(IncidentVO reactiveIncidentVO) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" select top 1 lp_sl_id  from lp_site_list ");
        sqlQuery.append(" where lp_si_number like '%"
                + reactiveIncidentVO.getSiteNumber() + "%' and lp_md_mm_id =? ");
        Object[] params = new Object[]{reactiveIncidentVO.getMsaId()};
        String siteId = null;
        try {
            siteId = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
                    params, String.class);

        } catch (EmptyResultDataAccessException e) {
            logger.debug(e.getMessage());
            siteId = null;
        }
        return siteId;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.dao.ServiceNowDAO#insertReactiveIncidentToIlex(com.ilex
     * .ws.core.bean.IncidentVO, java.lang.String)
     */
    @Override
    public Long insertReactiveIncidentToIlex(
            final IncidentVO reactiveIncidentVO, final String ilexTicketNumber) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        final StringBuilder snMainQuery = new StringBuilder(
                " insert into ig_service_now_main ");
        snMainQuery.append(" (u_ticket_number,u_category,u_description ");
        snMainQuery.append(" ,u_impact,u_short_description,u_site_number ");
        snMainQuery.append(" ,u_state,u_symptom,u_type ");
        snMainQuery.append(" ,u_urgency,u_vendor_key,u_configuration_item ");
        snMainQuery.append(" ,servicenow_identifier,created_date,created_by ");
        snMainQuery
                .append(" ,integration_type,integration_status,lp_msa_id,lx_pr_id,integration_status_msg,u_work_notes,vTaskNumber,lp_sl_id) ");
        snMainQuery
                .append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,getdate(),?,?,?,?,?,?,?,?,?)");
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(
                    Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(
                        snMainQuery.toString(), new String[]{"id"});
                ps.setString(1, ilexTicketNumber);
                ps.setString(2, reactiveIncidentVO.getCategory());
                ps.setString(3, reactiveIncidentVO.getDescription());
                ps.setString(4, reactiveIncidentVO.getImpact());
                ps.setString(5, reactiveIncidentVO.getShortDescription());
                ps.setString(6, reactiveIncidentVO.getSiteNumber());
                ps.setString(7, reactiveIncidentVO.getState());
                ps.setString(8, reactiveIncidentVO.getSymptom());
                ps.setString(9, reactiveIncidentVO.getType());
                ps.setString(10, reactiveIncidentVO.getUrgency());
                ps.setString(11, reactiveIncidentVO.getVendorKey());
                ps.setString(12, reactiveIncidentVO.getConfigurationItem());
                ps.setString(13, reactiveIncidentVO.getVendorKey());
                ps.setString(14, null);
                ps.setString(15, reactiveIncidentVO.getIntegrationType());
                ps.setString(16, reactiveIncidentVO.getIntegrationStatus());
                ps.setString(17, reactiveIncidentVO.getMsaId());
                ps.setString(18, reactiveIncidentVO.getAppendixId());
                ps.setString(19, reactiveIncidentVO.getIntegrationStatusMsg());
                ps.setString(20, reactiveIncidentVO.getWorkNotes());
                ps.setString(21, reactiveIncidentVO.getTicketNumber());
                ps.setString(22, reactiveIncidentVO.getLpSiteListId());
                return ps;
            }
        };

        jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);
        final Long incidentId = keyHolder.getKey().longValue();
        return incidentId;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ilex.ws.core.dao.ServiceNowDAO#getIncidentDetail(java.lang.Long)
     */
    @Override
    public IncidentVO getIncidentDetail(Long incidentId) {
        StringBuilder sqlQuery = new StringBuilder(
                " select * from ig_service_now_main where create_identifier=? ");
        Object[] param = new Object[]{incidentId};
        IncidentVO incidentVO = new IncidentVO();
        incidentVO = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
                param, new IncidentDetailMapper());
        return incidentVO;
    }

    /**
     * The Class IncidentDetailMapper.
     */
    private static final class IncidentDetailMapper implements
            RowMapper<IncidentVO> {

        /*
         * (non-Javadoc)
         *
         * @see
         * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
         * int)
         */
        public IncidentVO mapRow(ResultSet rs, int rowNum) throws SQLException {
            IncidentVO incidentVO = new IncidentVO();
            incidentVO.setTicketNumber(rs.getString("u_ticket_number"));
            incidentVO
                    .setServiceNowSysId(rs.getString("servicenow_identifier"));
            incidentVO.setvTaskNumber(rs.getString("vTaskNumber"));
            incidentVO.setCreateIdentifier(rs.getString("create_identifier"));
            incidentVO.setIntegrationType(rs.getString("integration_type"));
            incidentVO.setSiteNumber(rs.getString("u_site_number"));
            incidentVO.setCategory(rs.getString("u_category"));
            incidentVO.setImpact(rs.getString("u_impact"));
            incidentVO.setUrgency(rs.getString("u_urgency"));
            incidentVO.setConfigurationItem(rs
                    .getString("u_configuration_item"));
            incidentVO.setShortDescription(rs.getString("u_short_description"));
            incidentVO.setDescription(rs.getString("u_description"));
            incidentVO.setWorkNotes(rs.getString("u_work_notes"));
            incidentVO.setCreatedDate(rs.getTimestamp("created_date"));
            incidentVO.setMsaId(rs.getString("lp_msa_id"));
            incidentVO.setIntegrationStatusMsg(rs
                    .getString("integration_status_msg"));
            incidentVO.setIntegrationStatus(rs.getString("integration_status"));
            incidentVO.setAppendixId(rs.getString("lx_pr_id"));
            incidentVO.setLpSiteListId(rs.getString("lp_sl_id"));
            return incidentVO;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ilex.ws.core.dao.ServiceNowDAO#getUpdateIncidentErrorLogs()
     */
    @Override
    public List<IncidentVO> getUpdateIncidentErrorLogs() {
        StringBuilder sqlQuery = new StringBuilder("");
        sqlQuery.append(" select top 25 u_ticket_number, vTaskNumber,ig_servicenow_updates.created_date,trigger_source, ");
        sqlQuery.append(" ig_servicenow_updates.integration_status_msg from dbo.ig_service_now_main ");
        sqlQuery.append(" join dbo.ig_servicenow_updates  ");
        sqlQuery.append(" on ig_service_now_main.create_identifier = ig_servicenow_updates.create_identifier ");
        sqlQuery.append(" where ig_servicenow_updates.integration_status = 'Error' ");
        sqlQuery.append(" order by ig_servicenow_updates.created_date desc");
        List<IncidentVO> updateIncidentErrorVOList = jdbcTemplateIlex.query(
                sqlQuery.toString(), new UpdateIncidentErrorLogMapper());
        return updateIncidentErrorVOList;
    }

    /**
     * The Class UpdateIncidentErrorLogMapper.
     */
    private static final class UpdateIncidentErrorLogMapper implements
            RowMapper<IncidentVO> {

        /*
         * (non-Javadoc)
         *
         * @see
         * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
         * int)
         */
        public IncidentVO mapRow(ResultSet rs, int rowNum) throws SQLException {
            IncidentVO incidentVO = new IncidentVO();
            incidentVO.setTicketNumber(rs.getString("u_ticket_number"));
            incidentVO.setvTaskNumber(rs.getString("vTaskNumber"));
            incidentVO.setCreatedDate(rs.getTimestamp("created_date"));
            incidentVO.setTriggerSource(rs.getString("trigger_source"));
            incidentVO.setIntegrationStatusMsg(rs
                    .getString("integration_status_msg"));
            return incidentVO;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.dao.ServiceNowDAO#insertIncidentUpdatetRecordToIlex(
     * com.ilex.ws.core.bean.IncidentVO, java.lang.String)
     */
    @Override
    public Map<String, Long> insertIncidentUpdatetRecordToIlex(
            final IncidentVO incidentVO, final String userId) {
        Map<String, Long> response = new HashMap<>();
        KeyHolder keyHolder = new GeneratedKeyHolder();
        final StringBuilder snMainQuery = new StringBuilder(
                " INSERT INTO ig_servicenow_updates ");
        snMainQuery.append(" (create_identifier,u_work_notes,u_state ");
        snMainQuery.append(" ,trigger_source,created_date,created_by ");
        snMainQuery
                .append(" ,integration_status,integration_status_msg,servicenow_identifier) ");
        snMainQuery.append(" VALUES (?,?,?,?,getDate(),?,?,?,?) ");
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(
                    Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(
                        snMainQuery.toString(), new String[]{"id"});
                ps.setString(1, incidentVO.getCreateIdentifier());
                ps.setString(2, incidentVO.getWorkNotes());
                ps.setString(3, incidentVO.getState());
                ps.setString(4, incidentVO.getTriggerSource());
                ps.setString(5, userId);
                ps.setString(6, incidentVO.getIntegrationStatus());
                ps.setString(7, incidentVO.getIntegrationStatusMsg());
                ps.setString(8, incidentVO.getServiceNowSysId());
                return ps;
            }
        };

        jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);
        final Long updateIdentifier = keyHolder.getKey().longValue();
        response.put("updateIdentifier", updateIdentifier);
        return response;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.dao.ServiceNowDAO#setIncidentUpdateUnsuccessResponse
     * (com.ilex.ws.servicenow.dto.UpdateIncidentResponseDTO, int,
     * java.lang.Long)
     */
    @Override
    public void setIncidentUpdateUnsuccessResponse(
            UpdateIncidentResponseDTO responseDTO, int userId,
            Long updateIdentifier) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" update ig_servicenow_updates set ");
        sqlQuery.append(" integration_status=?,integration_status_msg=? ");
        sqlQuery.append(" where update_identifier=? ");
        Object[] params = new Object[]{"Error",
            responseDTO.getErrorDescription(), updateIdentifier};
        jdbcTemplateIlex.update(sqlQuery.toString(), params);

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.dao.ServiceNowDAO#setIncidentUpdateSuccessResponse(com
     * .ilex.ws.servicenow.dto.UpdateIncidentResponseDTO, int, java.lang.Long)
     */
    @Override
    public void setIncidentUpdateSuccessResponse(
            UpdateIncidentResponseDTO responseDTO, int userId,
            Long updateIdentifier) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" update ig_servicenow_updates set ");
        sqlQuery.append(" servicenow_identifier=?,integration_status=? ");
        sqlQuery.append(" where update_identifier=? ");
        Object[] params = new Object[]{responseDTO.getSysId(), "Successful",
            updateIdentifier};
        jdbcTemplateIlex.update(sqlQuery.toString(), params);

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.dao.ServiceNowDAO#getIncidentUpdates(java.lang.Long)
     */
    @Override
    public List<IncidentVO> getIncidentUpdates(Long createIdentifier) {
        StringBuilder sqlQuery = new StringBuilder("");
        sqlQuery.append(" select created_date,trigger_source,u_state,u_work_notes from ig_servicenow_updates ");
        sqlQuery.append(" where  create_identifier=? ");
        sqlQuery.append(" order by created_date desc ");
        Object[] param = new Object[]{createIdentifier};
        List<IncidentVO> incidentUpdates = jdbcTemplateIlex.query(
                sqlQuery.toString(), param, new IncidentUpdatesMapper());
        return incidentUpdates;
    }

    /**
     * The Class IncidentUpdatesMapper.
     */
    private static final class IncidentUpdatesMapper implements
            RowMapper<IncidentVO> {

        /*
         * (non-Javadoc)
         *
         * @see
         * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
         * int)
         */
        public IncidentVO mapRow(ResultSet rs, int rowNum) throws SQLException {
            IncidentVO incidentVO = new IncidentVO();
            incidentVO.setCreatedDate(rs.getTimestamp("created_date"));
            incidentVO.setTriggerSource(rs.getString("trigger_source"));
            incidentVO.setState(rs.getString("u_state"));
            incidentVO.setWorkNotes(rs.getString("u_work_notes"));
            return incidentVO;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.dao.ServiceNowDAO#getIncidentCreateIdentifier(java.lang
     * .String, java.lang.String)
     */
    @Override
    public Long getIncidentCreateIdentifier(String vTaskNumber,
            String ticketNumber) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" select create_identifier from ig_service_now_main ");
        sqlQuery.append(" where vTaskNumber =? ");
        sqlQuery.append(" and u_ticket_number =? ");
        Object[] params = new Object[]{vTaskNumber, ticketNumber};
        Long createIdentifier = null;
        try {
            createIdentifier = jdbcTemplateIlex.queryForObject(
                    sqlQuery.toString(), params, Long.class);
        } catch (EmptyResultDataAccessException e) {
            logger.debug(e.getMessage());
            createIdentifier = null;
        }
        return createIdentifier;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.dao.ServiceNowDAO#checkStateMarkedResolved(java.lang
     * .Long)
     */
    @Override
    public boolean checkStateMarkedResolved(Long createIdentifier) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" select create_identifier from ig_service_now_main ");
        sqlQuery.append(" where u_state='Resolved' and create_identifier=? ");
        Object[] params = new Object[]{createIdentifier};
        boolean stateMarkedResolved = false;
        try {
            jdbcTemplateIlex.queryForObject(sqlQuery.toString(), params,
                    String.class);
            stateMarkedResolved = true;
        } catch (EmptyResultDataAccessException e) {
            logger.debug(e.getMessage());
        }
        return stateMarkedResolved;
    }

    @Override
    public String validateSiteNumberForTXR(IncidentVO reactiveIncidentVO) {
        String siteNumber = reactiveIncidentVO.getSiteNumber();
        if (StringUtils.isNotBlank(siteNumber)) {
            siteNumber = siteNumber.replaceAll("\\D+", "");
            siteNumber = siteNumber.replaceAll("^0*", "");
        }
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" select top 1 lp_sl_id  from lp_site_list ");
        sqlQuery.append(" where lp_md_mm_id = ? and lp_si_number like 'TXR#%' ");
        sqlQuery.append(" and convert(int, substring(lp_si_number, charindex('#', ");
        sqlQuery.append(" lp_si_number)+1, len(lp_si_number))) = ? ");
        Object[] params = new Object[]{reactiveIncidentVO.getMsaId(),
            siteNumber};
        String siteId = null;
        try {
            siteId = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
                    params, String.class);
        } catch (EmptyResultDataAccessException e) {
            logger.debug(e.getMessage());
            siteId = null;
        }
        return siteId;
    }

    @Override
    public void updateProactiveUnSuccessTxrResponse(
            ProactiveIncidentTxrResponseDTO responseDTO, int userId,
            Long createIdentifier) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" update ig_service_now_main set ");
        sqlQuery.append(" servicenow_identifier=?,integration_status=?,vTaskNumber=?,integration_status_msg=? ");
        sqlQuery.append(" where create_identifier=? ");
        Object[] params = new Object[]{responseDTO.getSysId(), "Error", null,
            responseDTO.getErrorDescription(), createIdentifier};
        jdbcTemplateIlex.update(sqlQuery.toString(), params);

    }

    @Override
    public void updateProactiveSuccessfulTxrResponse(
            ProactiveIncidentTxrResponseDTO responseDTO, int userId,
            Long createIdentifier) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" update ig_service_now_main set ");
        sqlQuery.append(" servicenow_identifier=?,integration_status=?,vTaskNumber=? ");
        sqlQuery.append(" where create_identifier=? ");
        Object[] params = new Object[]{responseDTO.getSysId(), "Successful", responseDTO.getTicketNumber(), createIdentifier};
        jdbcTemplateIlex.update(sqlQuery.toString(), params);
    }

    @Override
    public void setIncidentUpdateUnsuccessTxrResponse(
            UpdateIncidentTxrResponseDTO responseDTO, int userId,
            Long updateIdentifier) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" update ig_servicenow_updates set ");
        sqlQuery.append(" integration_status=?,integration_status_msg=? ");
        sqlQuery.append(" where update_identifier=? ");
        Object[] params = new Object[]{"Error",
            responseDTO.getErrorDescription(), updateIdentifier};
        jdbcTemplateIlex.update(sqlQuery.toString(), params);

    }

    @Override
    public void setIncidentUpdateSuccessTxrResponse(
            UpdateIncidentTxrResponseDTO responseDTO, int userId,
            Long updateIdentifier) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" update ig_servicenow_updates set ");
        sqlQuery.append(" servicenow_identifier=?,integration_status=? ");
        sqlQuery.append(" where update_identifier=? ");
        Object[] params = new Object[]{responseDTO.getSysId(), "Successful",
            updateIdentifier};
        jdbcTemplateIlex.update(sqlQuery.toString(), params);

    }

    @Override
    public boolean checkIntegrationType(String appendixId) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" select count(*) from ig_integration_mapping ");
        sqlQuery.append(" where ig_lx_pr_id = ? and ig_im_type = 'ServiceNow' ");
        sqlQuery.append(" and ig_im_status = 'Active' ");
        Object[] params = new Object[]{appendixId};
        boolean activeIntegration = false;
        int count = 0;
        try {
            count = jdbcTemplateIlex.queryForInt(sqlQuery.toString(), params);
            if (count >= 1) {
                activeIntegration = true;
            }
        } catch (EmptyResultDataAccessException e) {
            logger.debug(e.getMessage());
        }
        return activeIntegration;
    }

    @Override
    public IncidentVO getHdTicketDetailForCreateIncident(String ticketId) {
        StringBuilder sqlQuery = new StringBuilder(
                " select lm_tc_number,lm_hd_short_description,lm_tc_problem_desc,lm_hd_impact,lp_si_number, ");
        sqlQuery.append(" lm_hd_lp_sl_id,lm_hd_urgency,lm_ci_config_item,lm_hd_lp_mm_id,lm_hd_lx_pr_id ");
        sqlQuery.append(" from lm_ticket join lm_help_desk_ticket_details on lm_tc_id=lm_hd_tc_id ");
        sqlQuery.append(" left outer join lm_help_desk_ticket_config_items on lm_hd_ci=lm_ci_id ");
        sqlQuery.append(" inner join lp_site_list on lm_hd_lp_sl_id = lp_sl_id ");
        sqlQuery.append(" where lm_tc_id=? ");
        Object[] param = new Object[]{ticketId};
        IncidentVO incidentVO = new IncidentVO();
        incidentVO = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
                param, new HdTicketDetailMapper());
        return incidentVO;
    }

    /**
     * The Class HdTicketDetailMapper.
     */
    private static final class HdTicketDetailMapper implements
            RowMapper<IncidentVO> {

        public IncidentVO mapRow(ResultSet rs, int rowNum) throws SQLException {
            IncidentVO incidentVO = new IncidentVO();
            incidentVO.setTicketNumber(rs.getString("lm_tc_number"));
            incidentVO.setShortDescription(rs
                    .getString("lm_hd_short_description"));
            incidentVO.setDescription(rs.getString("lm_tc_problem_desc"));
            incidentVO.setImpact(rs.getString("lm_hd_impact"));
            incidentVO.setSiteNumber(rs.getString("lm_hd_lp_sl_id"));
            incidentVO.setSiteDisplayNumber(rs.getString("lp_si_number"));
            incidentVO.setUrgency(rs.getString("lm_hd_urgency"));
            incidentVO.setConfigurationItem(rs.getString("lm_ci_config_item"));
            incidentVO.setMsaId(rs.getString("lm_hd_lp_mm_id"));
            incidentVO.setAppendixId(rs.getString("lm_hd_lx_pr_id"));
            return incidentVO;
        }
    }

    @Override
    public String getLatestWorkNote(String ticketId) {
        String sqlQuery = "select top 1 lm_wn_note from lm_help_desk_work_notes where lm_wn_tc_id=? order by lm_wn_dt desc";
        String workNotes = null;
        Object[] param = new Object[]{ticketId};
        try {
            workNotes = jdbcTemplateIlex.queryForObject(sqlQuery, param,
                    String.class);
        } catch (EmptyResultDataAccessException e) {
            logger.debug(e.getMessage());
        }
        return workNotes;
    }

    @Override
    public String getSiteNumber(String siteId) {
        String sqlQuery = "select lp_si_number from lp_site_list where lp_sl_id =? ";
        String siteNumber = null;
        Object[] param = new Object[]{siteId};
        try {
            siteNumber = jdbcTemplateIlex.queryForObject(sqlQuery, param,
                    String.class);
        } catch (EmptyResultDataAccessException e) {
            logger.debug(e.getMessage());
        }
        return siteNumber;
    }

    @Override
    public void setHdUpdateFlag(String ticketId) {

        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" update lm_help_desk_ticket_details");
        sqlQuery.append(" set lm_hd_ext_system_update = getdate(),");
        sqlQuery.append(" lm_hd_ext_system_status = 1 ");
        sqlQuery.append(" where lm_hd_tc_id = ?");

        Object[] params = new Object[]{ticketId};

        jdbcTemplateIlex.update(sqlQuery.toString(), params);

    }

    @Override
    public IncidentVO getHdTicketDetailForUpdateIncident(String ticketId) {
        StringBuilder sqlQuery = new StringBuilder(
                " select lm_tc_number,lm_hd_lp_mm_id,lm_hd_lx_pr_id,lm_st_status,create_identifier,u_site_number,lo_ot_name,vTaskNumber ");
        sqlQuery.append(" from lm_ticket join lm_help_desk_ticket_details on lm_tc_id=lm_hd_tc_id ");
        sqlQuery.append(" left outer join lm_help_desk_ticket_status on lm_hd_status=lm_st_id ");
        sqlQuery.append(" left outer join ig_service_now_main on lm_tc_number=u_ticket_number ");
        sqlQuery.append(" left outer join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id ");
        sqlQuery.append(" left outer join lo_organization_top on lp_mm_ot_id = lo_ot_id ");
        sqlQuery.append(" where lm_tc_id=? ");
        Object[] param = new Object[]{ticketId};
        IncidentVO incidentVO = new IncidentVO();
        incidentVO = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
                param, new HdTicketUpdateDetailMapper());
        return incidentVO;
    }

    private static final class HdTicketUpdateDetailMapper implements
            RowMapper<IncidentVO> {

        public IncidentVO mapRow(ResultSet rs, int rowNum) throws SQLException {
            IncidentVO incidentVO = new IncidentVO();
            incidentVO.setTicketNumber(rs.getString("lm_tc_number"));
            incidentVO.setMsaId(rs.getString("lm_hd_lp_mm_id"));
            incidentVO.setAppendixId(rs.getString("lm_hd_lx_pr_id"));
            incidentVO.setState(rs.getString("lm_st_status"));
            incidentVO.setCreateIdentifier(rs.getString("create_identifier"));
            incidentVO.setSiteNumber(rs.getString("u_site_number"));
            incidentVO.setCustomerName(rs.getString("lo_ot_name"));
            incidentVO.setvTaskNumber(rs.getString("vTaskNumber"));
            return incidentVO;
        }
    }

    @Override
    public IncidentVO getIncidentUpdateDetail(String updateIdentifier) {
        StringBuilder sqlQuery = new StringBuilder(
                " select integration_status,integration_status_msg,u_state,u_work_notes from ig_servicenow_updates ");
        sqlQuery.append(" where  update_identifier=? ");
        Object[] param = new Object[]{updateIdentifier};
        IncidentVO incidentVO = new IncidentVO();
        incidentVO = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
                param, new IncidentUpdateDetailMapper());
        return incidentVO;
    }

    /**
     * The Class HdTicketDetailMapper.
     */
    private static final class IncidentUpdateDetailMapper implements
            RowMapper<IncidentVO> {

        public IncidentVO mapRow(ResultSet rs, int rowNum) throws SQLException {
            IncidentVO incidentVO = new IncidentVO();
            incidentVO.setIntegrationStatus(rs.getString("integration_status"));
            incidentVO.setIntegrationStatusMsg(rs
                    .getString("integration_status_msg"));
            incidentVO.setState(rs.getString("u_state"));
            incidentVO.setWorkNotes(rs.getString("u_work_notes"));
            return incidentVO;
        }
    }

    @Override
    public Long insertTicket(final String ilexTicketNumber,
            final String problemDescription, String appendixId) {

        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("select lm_ty_id from lm_help_desk_ticket_types where lm_ty_type='MSP Help Desk'");
        Long ticketTypeId = jdbcTemplateIlex.queryForLong(sqlQuery.toString());

        final HDTicketLookUpVO hdTicketLookUpVO = getTicketTypeLookUp(
                appendixId, ticketTypeId);

        String resourceSelect = "select  iv_rp_id from dbo.iv_resource_pool where iv_rp_name = 'Customer Care Agent'";
        final int resourceId = jdbcTemplateIlex.queryForInt(resourceSelect);

        KeyHolder keyHolder = new GeneratedKeyHolder();
        final StringBuilder sqlQuery1 = new StringBuilder(
                "insert into lm_ticket (lm_tc_number,lm_tc_js_id,lm_tc_requestor_name,");
        sqlQuery1
                .append(" lm_tc_requestor_email,lm_tc_problem_desc,lm_tc_preferred_arrival,lm_tc_preferred_window_start,");
        sqlQuery1
                .append(" lm_tc_preferred_window_end, lm_tc_cust_reference, lm_tc_type, lm_tc_category, lm_tc_criticality,");
        sqlQuery1
                .append(" lm_tc_resource_level, lm_tc_crit_cat_id, lm_tc_requested_date,lm_tc_standby,");
        sqlQuery1
                .append(" lm_tc_msp,lm_tc_help_desk, lm_tc_request_type,lm_tc_msp_monitoring_source,");
        sqlQuery1
                .append(" lm_tc_spec_instruction, lm_tc_summary, lm_tc_cust_specific_fields,lm_tc_wug_dv_id");
        sqlQuery1
                .append(") values (?,?,?,?,?,getdate(),?,?,?,?,?,?,?,?,getdate(),?,?,?,?,?,?,?,?,?)");
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(
                    Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(
                        sqlQuery1.toString(), new String[]{"id"});
                ps.setString(1, ilexTicketNumber);
                ps.setString(2, null);
                ps.setString(3, null);
                ps.setString(4, null);
                ps.setString(5, problemDescription);
                ps.setString(6, null);
                ps.setString(7, null);
                ps.setString(8, hdTicketLookUpVO.getCustomerReference());
                ps.setString(9, "MSP Help Desk");

                ps.setString(10, hdTicketLookUpVO.getCategory());
                ps.setString(11, "1");// lm_tc_criticality default to 1
                ps.setInt(12, resourceId);

                ps.setString(13, "1");// lm_tc_crit_cat_id default to 1
                ps.setString(14, "N");
                ps.setString(15, hdTicketLookUpVO.getMsp());
                ps.setString(16, hdTicketLookUpVO.getHelpDesk());
                ps.setString(17, "1");// lm_tc_request_type to 1
                ps.setString(18, "1");

                ps.setString(19, null);
                ps.setString(20, null);
                ps.setString(21, null);
                ps.setString(22, null);

                return ps;
            }
        };

        jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);

        Long ticketId = keyHolder.getKey().longValue();
        return ticketId;
    }

    private HDTicketLookUpVO getTicketTypeLookUp(String appendixId,
            Long ticketTypeId) {
        HDTicketLookUpVO hdTicketLookUpVO = new HDTicketLookUpVO();

        String existanceCheckSelect = "select 1 from lm_help_desk_ticket_type_lookup_values where lm_lv_pr_id = ? and lm_lv_hd_type = ?";

        Object[] params = new Object[]{appendixId, ticketTypeId};

        try {
            jdbcTemplateIlex.queryForInt(existanceCheckSelect, params);
        } catch (EmptyResultDataAccessException ex) {
            params = new Object[]{0l, ticketTypeId};
        }

        StringBuilder selectTicketTypeLookup = new StringBuilder(
                "select cust_reference,msp,help_desk,category");

        selectTicketTypeLookup
                .append(" from lm_help_desk_ticket_type_lookup_values");
        selectTicketTypeLookup.append(" where lm_lv_pr_id = ?");
        selectTicketTypeLookup.append(" and lm_lv_hd_type = ?");
        hdTicketLookUpVO = jdbcTemplateIlex.queryForObject(
                selectTicketTypeLookup.toString(), params,
                new TicketTypeLookUpMapper());
        return hdTicketLookUpVO;
    }

    private static final class TicketTypeLookUpMapper implements
            RowMapper<HDTicketLookUpVO> {

        /*
         * (non-Javadoc)
         *
         * @see
         * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
         * int)
         */
        public HDTicketLookUpVO mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            HDTicketLookUpVO hdTicketLookUpVO = new HDTicketLookUpVO();
            hdTicketLookUpVO.setCategory(rs.getString("category"));
            hdTicketLookUpVO.setCustomerReference(rs
                    .getString("cust_reference"));
            hdTicketLookUpVO.setHelpDesk(rs.getString("help_desk"));
            hdTicketLookUpVO.setMsp(rs.getString("msp"));

            return hdTicketLookUpVO;
        }
    }

    @Override
    public HDTicketVO getSnMessage(Long ticketId) {
        HDTicketVO hdTicketVO = new HDTicketVO();
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("select lm_hd_ext_system_update from lm_help_desk_ticket_details ");
        sqlQuery.append(" where lm_hd_tc_id = ? ");

        Object[] params = new Object[]{ticketId};
        hdTicketVO = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
                params, new SnMessageMapper());

        sqlQuery = new StringBuilder();
        sqlQuery.append("select  top(1) lm_wn_note,lm_wn_type from lm_help_desk_work_notes where lm_wn_tc_id=? and lm_wn_type=1 order by lm_wn_dt desc");
        HDWorkNoteVO hdWorkNoteVO = new HDWorkNoteVO();
        try {
            hdWorkNoteVO = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
                    params, new LatestWorkNoteMapper());
        } catch (EmptyResultDataAccessException e) {
            logger.debug(e.getMessage());
        }
        if (hdWorkNoteVO == null) {
            hdTicketVO.setTicketSource("");
            hdTicketVO.setWorkNotes("");
        } else {
            hdTicketVO.setWorkNotes(hdWorkNoteVO.getWorkNote());
            if ("1".equalsIgnoreCase(hdWorkNoteVO.getWorkNoteType())) {
                hdTicketVO.setTicketSource("ServiceNow");
            } else {
                hdTicketVO.setTicketSource("Internal Error");
            }
        }

        return hdTicketVO;
    }

    private static final class SnMessageMapper implements RowMapper<HDTicketVO> {

        @Override
        public HDTicketVO mapRow(ResultSet rs, int rowNum) throws SQLException {
            HDTicketVO hdTicketVO = new HDTicketVO();
            hdTicketVO.setExtSystemUpdateDate(rs
                    .getTimestamp("lm_hd_ext_system_update"));
            return hdTicketVO;
        }
    }

    private static final class LatestWorkNoteMapper implements
            RowMapper<HDWorkNoteVO> {

        @Override
        public HDWorkNoteVO mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            HDWorkNoteVO hdWorkNoteVO = new HDWorkNoteVO();
            hdWorkNoteVO.setWorkNote(rs.getString("lm_wn_note"));
            hdWorkNoteVO.setWorkNoteType(rs.getString("lm_wn_type"));
            return hdWorkNoteVO;
        }
    }

    @Override
    public void clearIntegratedTicketStatus(Long ticketId) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("update lm_help_desk_ticket_details");
        sqlQuery.append(" set lm_hd_ext_system_status= 0");
        sqlQuery.append(" where lm_hd_tc_id=?");

        Object[] params = new Object[]{ticketId};

        jdbcTemplateIlex.update(sqlQuery.toString(), params);
    }

    @Override
    public HDTicketVO getTicketDetailsFromTicketNumber(String ticketNumber) {
        StringBuilder sqlQuery = new StringBuilder(
                " select lm_tc_id,lm_st_status ");
        sqlQuery.append(" from lm_ticket join lm_help_desk_ticket_details on lm_tc_id=lm_hd_tc_id ");
        sqlQuery.append(" left outer join lm_help_desk_ticket_status on lm_hd_status=lm_st_id ");
        sqlQuery.append(" where lm_tc_number=? ");
        Object[] param = new Object[]{ticketNumber};
        HDTicketVO hdTicketVO = new HDTicketVO();
        hdTicketVO = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
                param, new TicketDetailMapper());
        return hdTicketVO;
    }

    private static final class TicketDetailMapper implements
            RowMapper<HDTicketVO> {

        public HDTicketVO mapRow(ResultSet rs, int rowNum) throws SQLException {
            HDTicketVO hdTicketVO = new HDTicketVO();
            hdTicketVO.setTicketId(rs.getString("lm_tc_id"));
            hdTicketVO.setState(rs.getString("lm_st_status"));
            return hdTicketVO;
        }
    }

    @Override
    public String getTicketStateId(String ticketState) {
        String sqlQuery = "select lm_st_id from lm_help_desk_ticket_status where lm_st_status = ? ";
        String ticketStateId = null;
        Object[] param = new Object[]{ticketState};
        try {
            ticketStateId = jdbcTemplateIlex.queryForObject(sqlQuery, param,
                    String.class);
        } catch (EmptyResultDataAccessException e) {
            logger.debug(e.getMessage());
        }
        return ticketStateId;
    }

    @Override
    public void updateTicketState(String newTicketStateId, String ticketId) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append(" update lm_help_desk_ticket_details ");
        sqlQuery.append(" set lm_hd_status = ? where lm_hd_tc_id = ? ");
        Object[] params = new Object[]{newTicketStateId, ticketId};
        jdbcTemplateIlex.update(sqlQuery.toString(), params);

    }

    @Override
    public void insertWorkNotesFromSnToIlex(String ticketId, String workNotes,
            String wnType, String userId) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("insert into lm_help_desk_work_notes (");
        sqlQuery.append("lm_wn_tc_id, lm_wn_note, lm_wn_type, lm_wn_dt, lm_wn_user )");
        sqlQuery.append(" values (?,?,?,getdate(),?) ");
        Object[] params = new Object[]{ticketId, workNotes, wnType, userId};
        jdbcTemplateIlex.update(sqlQuery.toString(), params);

    }

    @Override
    public void insertHDTicket(IncidentVO reactiveIncidentVO,
            Long ticketIdFromLmTicket) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("select lm_sr_id from lm_help_desk_ticket_sources where lm_sr_source = 'ServiceNow'");
        Long ticketSourceId = jdbcTemplateIlex
                .queryForLong(sqlQuery.toString());

        sqlQuery = new StringBuilder();
        sqlQuery.append("select lm_st_id from lm_help_desk_ticket_status where lm_st_status = 'New'");
        Long statusId = jdbcTemplateIlex.queryForLong(sqlQuery.toString());

        int ticketPriority = hdMainDAO.getTicketPriority(
                Long.valueOf(reactiveIncidentVO.getAppendixId()),
                Integer.valueOf(reactiveIncidentVO.getImpact()),
                Integer.valueOf(reactiveIncidentVO.getUrgency()));

        Long categoryId = null;
        sqlQuery = new StringBuilder();
        sqlQuery.append("select lm_ct_id from lm_help_desk_ticket_categories where lm_ct_category ='"
                + reactiveIncidentVO.getCategory() + "'");
        try {
            categoryId = jdbcTemplateIlex.queryForLong(sqlQuery.toString());
        } catch (EmptyResultDataAccessException e) {
            logger.info("Category not found in the list of lm_help_desk_ticket_categories and now default to 3 (Network)");
            categoryId = 3l;
        }

        Long configurationId = null;
        sqlQuery = new StringBuilder();
        sqlQuery.append("select lm_ci_id from lm_help_desk_ticket_config_items where lm_ci_config_item ='"
                + reactiveIncidentVO.getConfigurationItem() + "'");
        try {
            configurationId = jdbcTemplateIlex
                    .queryForLong(sqlQuery.toString());
        } catch (EmptyResultDataAccessException e) {
            logger.info("CI number is not found in the list of lm_help_desk_ticket_config_items and now default to 2");
            configurationId = 2l;
        }
        sqlQuery = new StringBuilder();
        sqlQuery.append("insert into lm_help_desk_ticket_details (lm_hd_tc_id, lm_hd_source, lm_hd_type,");
        sqlQuery.append(" lm_hd_customer_identifier, lm_hd_assigned_to, lm_hd_impact, lm_hd_urgency, lm_hd_short_description,");
        sqlQuery.append(" lm_hd_ci, lm_hd_caller_name, lm_hd_caller_phone, lm_hd_caller_email, lm_hd_manual_ticket_reason,");
        sqlQuery.append(" lm_hd_circuit_status, lm_hd_lp_mm_id, lm_hd_lx_pr_id, lm_hd_lp_sl_id, lm_hd_billable, lm_hd_category,");
        sqlQuery.append(" lm_hd_next_action, lm_hd_next_action_due, lm_hd_next_action_overdue, lm_hd_status, lm_hd_opened,");
        sqlQuery.append(" lm_hd_tier, lm_hd_priority, lm_hd_complete_first_call, lm_hd_ext_system_update, lm_hd_ext_system_status,");
        sqlQuery.append(" lm_hd_updated_by, lm_hd_update_date, lm_hd_session_id, lm_hd_resolution, lm_hd_point_of_failure, lm_hd_root_cause)");
        sqlQuery.append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getdate(),?,?,?,?,?,?,?,?,?,?,?)");

        Object[] params = new Object[]{ticketIdFromLmTicket, ticketSourceId,
            1, reactiveIncidentVO.getTicketNumber(), null,
            reactiveIncidentVO.getImpact(),
            reactiveIncidentVO.getUrgency(),
            reactiveIncidentVO.getShortDescription(), configurationId,
            null, null, null, null, null, reactiveIncidentVO.getMsaId(),
            reactiveIncidentVO.getAppendixId(),
            reactiveIncidentVO.getLpSiteListId(), 0, categoryId, null,
            null, 0, statusId, 1, ticketPriority, 0, null, 0, null, null,
            null, null, null, null};
        jdbcTemplateIlex.update(sqlQuery.toString(), params);
    }

    @Override
    public void logIntegration(int direction, int level, String msg, String data) {
        String sql = "INSERT INTO integration_log (ig_log_date, ig_log_direction, ig_log_level, ig_log_source, ig_log_message, ig_log_data) VALUES (GETDATE(), ?, ?, 'Ilex', ?, ?)";

        Object[] params = new Object[]{direction, level, msg, data};
        jdbcTemplateIlex.update(sql, params);
    }
}
