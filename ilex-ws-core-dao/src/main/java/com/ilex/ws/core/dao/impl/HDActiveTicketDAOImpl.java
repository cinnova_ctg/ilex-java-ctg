package com.ilex.ws.core.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.ilex.ws.core.bean.HDSessionVO;
import com.ilex.ws.core.bean.HDTicketVO;
import com.ilex.ws.core.bean.HDWIPTableVO;
import com.ilex.ws.core.bean.TicketDropResponseVO;
import com.ilex.ws.core.dao.HDActiveTicketDAO;
import java.sql.DatabaseMetaData;
import java.util.logging.Level;
import javax.sql.DataSource;

@Repository
public class HDActiveTicketDAOImpl implements HDActiveTicketDAO {

    public static final Logger logger = Logger
            .getLogger(HDActiveTicketDAOImpl.class);

    @Resource
    private JdbcTemplate jdbcTemplateIlex;

    @Override
    public String getHDSessionTrackerId(final String userId,
            final String sessionId, final String domainName) {

        String selectQuery = "select session_tracker_id from  lm_help_desk_ticket_session_tracker where user_id=? and session_Id=?";

        Object[] params = new Object[]{userId, sessionId};
        String hdSessionTrackerId = "0";
        try {
            hdSessionTrackerId = jdbcTemplateIlex.queryForObject(selectQuery,
                    params, String.class);
        } catch (EmptyResultDataAccessException ex) {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            final StringBuilder insertQuery = new StringBuilder(
                    "insert into lm_help_desk_ticket_session_tracker (user_id,session_Id, domain_server");
            insertQuery.append(") values (?,?,?)");
            PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(
                        Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(
                            insertQuery.toString(), new String[]{"id"});
                    ps.setString(1, userId);
                    ps.setString(2, sessionId);
                    ps.setString(3, domainName);

                    return ps;
                }
            };

            jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);

            hdSessionTrackerId = String.valueOf(keyHolder.getKey());

        }

        return hdSessionTrackerId;

    }

    @Override
    public TicketDropResponseVO isTicketAvailableForDrop(Long ticketId,
            String hdSessionTrackerId, String userId) {

        TicketDropResponseVO ticketDropResponseVO = new TicketDropResponseVO();

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplateIlex);
        simpleJdbcCall.withFunctionName("lm_help_desk_drop_ticket_action_01");

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("lm_tc_id", ticketId, Types.NUMERIC);
        mapSqlParameterSource.addValue("user_session_id", hdSessionTrackerId,
                Types.VARCHAR);
        mapSqlParameterSource.addValue("lo_pc_id", userId, Types.NUMERIC);

        simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("drop_status",
                Types.SMALLINT));
        simpleJdbcCall.addDeclaredParameter(new SqlOutParameter(
                "assigned_to_user", Types.VARCHAR));

        Map<String, Object> outputMap = simpleJdbcCall
                .execute(mapSqlParameterSource);

        int dropStatus = (int) outputMap.get("drop_status");

        if (dropStatus == 1) {
            ticketDropResponseVO.setDropStatus(true);
        } else {
            ticketDropResponseVO.setDropStatus(false);

            String assignedToUser = (String) outputMap.get("assigned_to_user");
            ticketDropResponseVO.setAssignedUser(assignedToUser);
        }

        return ticketDropResponseVO;
    }

    @Override
    public void removeTicketLock(HDSessionVO hdSessionVO) {

        try {
            DataSource dsTemp = jdbcTemplateIlex.getDataSource();
            Connection conTemp = dsTemp.getConnection();
            DatabaseMetaData dmdTemp = conTemp.getMetaData();
            logger.info("********** URL : " + dmdTemp.getURL());
            logger.debug("********** User : " + dmdTemp.getUserName());
        } catch (SQLException ex) {
            logger.debug(ex);
        }

        List<String> hdSessionTrackerIds = getHDSessionTrackerIds(hdSessionVO);

        if (!CollectionUtils.isEmpty(hdSessionTrackerIds)) {

            StringBuilder updateTicketDetails = new StringBuilder(
                    "update lm_help_desk_ticket_details set lm_hd_session_id=null, lm_hd_under_review=0");
            updateTicketDetails.append(" where lm_hd_session_id in ("
                    + StringUtils.join(hdSessionTrackerIds, ",") + ")");

            StringBuilder deleteHdSessions = new StringBuilder(
                    "delete lm_help_desk_ticket_session_tracker where session_tracker_id in ("
                    + StringUtils.join(hdSessionTrackerIds, ",") + ")");

            jdbcTemplateIlex.update(updateTicketDetails.toString());
            jdbcTemplateIlex.update(deleteHdSessions.toString());
        }

    }

    public List<String> getHDSessionTrackerIds(HDSessionVO hdSessionVO) {

        StringBuilder selectQuery = new StringBuilder(
                "select session_tracker_id from lm_help_desk_ticket_session_tracker");

        List<Object> paramList = new ArrayList<>();
        selectQuery.append(" where 1=1");
        if (StringUtils.isNotBlank(hdSessionVO.getUserId())) {
            selectQuery.append(" and user_id=?");
            paramList.add(hdSessionVO.getUserId());
        }
        if (StringUtils.isNotBlank(hdSessionVO.getSessionId())) {
            selectQuery.append(" and session_Id=?");
            paramList.add(hdSessionVO.getSessionId());
        }
        if (StringUtils.isNotBlank(hdSessionVO.getDomainName())) {
            selectQuery.append(" and domain_server=?");
            paramList.add(hdSessionVO.getDomainName());
        }

        List<String> hdSessionTrackerIds = jdbcTemplateIlex.queryForList(
                selectQuery.toString(), paramList.toArray(), String.class);

        return hdSessionTrackerIds;
    }

    @Override
    public Boolean closeActiveTicket(Long ticketId, String userId) {

        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("update lm_help_desk_ticket_details");
        sqlQuery.append(" set lm_hd_session_id = null,");
        sqlQuery.append(" lm_hd_update_date = getdate()");
        sqlQuery.append(" where lm_hd_tc_id = ? and lm_hd_assigned_to = ? ");

        Object[] params = new Object[]{ticketId, userId};

        jdbcTemplateIlex.update(sqlQuery.toString(), params);

        return true;

    }

    @Override
    public List<HDWIPTableVO> getHDWIPTicketDetails(String ticketId) {
        StringBuilder sqlQuery = new StringBuilder(
                "select sum(lm_ef_effort)as efforts, lm_hd_opened, lp_si_number, lo_ot_name, lp_si_address, lp_si_city, lp_si_state,");
        sqlQuery.append(" lp_si_country, lp_si_zip_code, lp_si_pri_poc, lp_si_phone_no, lp_si_pri_email_id, lm_hd_tc_id,lm_hd_priority,");
        sqlQuery.append(" lm_na_next_action,lm_hd_next_action_due,");
        sqlQuery.append(" isnull(lo_pc_last_name, 'Not Assigned') as lo_pc_last_name,");
        sqlQuery.append(" lm_hd_caller_name,lm_hd_short_description, lm_tc_number, lm_hd_session_id, lm_wn_note,lm_hd_next_action");
        sqlQuery.append(" from lm_help_desk_ticket_details");
        sqlQuery.append(" join lp_site_list on lm_hd_lp_sl_id = lp_sl_id");
        sqlQuery.append(" join lp_msa_main on lm_hd_lp_mm_id = lp_mm_id");
        sqlQuery.append(" join lo_organization_top on lp_mm_ot_id = lo_ot_id");
        sqlQuery.append(" join lm_help_desk_ticket_sources on lm_hd_source = lm_sr_id");
        sqlQuery.append(" join lm_help_desk_ticket_status on lm_hd_status = lm_st_id");
        sqlQuery.append(" join lm_help_desk_ticket_next_actions on lm_hd_next_action = lm_na_id");
        sqlQuery.append(" join lm_ticket on lm_hd_tc_id = lm_tc_id");
        sqlQuery.append(" left join lo_poc on lm_hd_assigned_to = lo_pc_id");
        sqlQuery.append(" left outer join lm_help_desk_work_notes on lm_hd_tc_id = lm_wn_tc_id");
        sqlQuery.append(" left outer join lm_help_desk_effort on lm_hd_tc_id = lm_ef_tc_id");
        sqlQuery.append(" where lm_hd_tc_id = ?");
        sqlQuery.append(" group by lm_ef_tc_id,lm_hd_opened, lp_si_number, lo_ot_name, lp_si_address, lp_si_city, lp_si_state, lp_si_country, lp_si_zip_code,");
        sqlQuery.append(" lp_si_pri_poc, lp_si_phone_no, lp_si_pri_email_id, lm_hd_tc_id,lm_hd_priority, lm_na_next_action,lm_hd_next_action_due, lo_pc_last_name,");
        sqlQuery.append(" lm_hd_caller_name,lm_hd_short_description, lm_tc_number, lm_hd_session_id, lm_wn_note,lm_hd_next_action");

        Object[] params = new Object[]{ticketId};

        List<HDWIPTableVO> wipTicketData = jdbcTemplateIlex.query(
                sqlQuery.toString(), params, new HDWipTicketMapper());

        return wipTicketData;
    }

    /**
     * The Class HDWipTicketMapper.
     */
    private static final class HDWipTicketMapper implements
            RowMapper<HDWIPTableVO> {

        /*
         * (non-Javadoc)
         *
         * @see
         * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
         * int)
         */
        public HDWIPTableVO mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            HDWIPTableVO hdWIPTableVO = new HDWIPTableVO();
            hdWIPTableVO.setOpenedDbDate(rs.getTimestamp("lm_hd_opened"));
            hdWIPTableVO.setSiteNumber(rs.getString("lp_si_number"));
            hdWIPTableVO.setStatus("State");
            hdWIPTableVO.setCustomer(rs.getString("lo_ot_name"));
            hdWIPTableVO.setSiteAddress(rs.getString("lp_si_address"));
            hdWIPTableVO.setSiteCity(rs.getString("lp_si_city"));
            hdWIPTableVO.setSiteState(rs.getString("lp_si_state"));
            hdWIPTableVO.setSiteCountry(rs.getString("lp_si_country"));
            hdWIPTableVO.setSiteZipcode(rs.getString("lp_si_zip_code"));
            hdWIPTableVO.setContactName(rs.getString("lp_si_pri_poc"));
            hdWIPTableVO.setContactPhoneNumber(rs.getString("lp_si_phone_no"));
            hdWIPTableVO.setContactEmailAddress(rs
                    .getString("lp_si_pri_email_id"));
            hdWIPTableVO.setTicketId(rs.getString("lm_hd_tc_id"));
            hdWIPTableVO.setPriority(rs.getString("lm_hd_priority"));
            hdWIPTableVO.setNextAction(rs.getString("lm_na_next_action"));
            hdWIPTableVO.setNextActionDueDateDB(rs
                    .getTimestamp("lm_hd_next_action_due"));
            hdWIPTableVO.setCca_tss(rs.getString("lo_pc_last_name"));
            hdWIPTableVO.setShortDescription(rs
                    .getString("lm_hd_short_description"));
            hdWIPTableVO.setIlexNumber(rs.getString("lm_tc_number"));
            hdWIPTableVO.setSessionId(rs.getString("lm_hd_session_id"));
            hdWIPTableVO.setWorkNotes(rs.getString("lm_wn_note"));
            hdWIPTableVO.setSumOfEfforts(rs.getString("efforts"));
            hdWIPTableVO.setNextActionId(rs.getString("lm_hd_next_action"));

            return hdWIPTableVO;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ilex.ws.core.dao.HDActiveTicketDAO#updateNextAction(java.lang.String,
     * java.lang.String, java.util.Date, java.lang.String)
     */
    @Override
    public void updateNextAction(String ticketId, String nextAction,
            Date nextActionDueDate, String userId) {

        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("update lm_help_desk_ticket_details");
        sqlQuery.append(" set lm_hd_next_action = ?,");
        sqlQuery.append(" lm_hd_next_action_due = ?,");
        sqlQuery.append(" lm_hd_update_date = getdate(),");
        sqlQuery.append(" lm_hd_updated_by = ?,");
        sqlQuery.append(" lm_hd_next_action_overdue ='0',");
        sqlQuery.append(" lm_hd_assigned_to=?");
        sqlQuery.append(" where lm_hd_tc_id = ?");

        Object[] params = new Object[]{Integer.valueOf(nextAction),
            new java.sql.Timestamp(nextActionDueDate.getTime()),
            Long.valueOf(userId), userId, Long.valueOf(ticketId)};

        jdbcTemplateIlex.update(sqlQuery.toString(), params);

        sqlQuery = new StringBuilder();

        sqlQuery.append("insert into lm_help_desk_next_action_history (");
        sqlQuery.append(" lm_na_tc_id,lm_na_action,lm_na_due_date,	lm_na_created_by,lm_na_inserted)");
        sqlQuery.append(" values(	?,?,?,?,getdate())");

        params = new Object[]{Long.valueOf(ticketId),
            Integer.valueOf(nextAction),
            new java.sql.Timestamp(nextActionDueDate.getTime()),
            Long.valueOf(userId)};

        jdbcTemplateIlex.update(sqlQuery.toString(), params);

    }

    @Override
    public HDTicketVO getTicketDetailForDashboard(String ticketId) {
        StringBuilder sqlQuery = new StringBuilder("select ");
        sqlQuery.append(" lm_tc_id,lm_js_id,lx_pr_id from lm_ticket join lm_job on lm_tc_js_id=lm_js_id ");
        sqlQuery.append(" join lx_appendix_main on lm_js_pr_id=lx_pr_id ");
        sqlQuery.append(" where lm_tc_id=? ");
        Object[] params = new Object[]{ticketId};
        HDTicketVO hdTicketVO = new HDTicketVO();
        try {
            hdTicketVO = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
                    params, new TicketDetailForDashboardMapper());
        } catch (EmptyResultDataAccessException e) {
            logger.debug(e);
        }
        return hdTicketVO;
    }

    private static final class TicketDetailForDashboardMapper implements
            RowMapper<HDTicketVO> {

        public HDTicketVO mapRow(ResultSet rs, int rowNum) throws SQLException {
            HDTicketVO hdTicketVO = new HDTicketVO();
            hdTicketVO.setTicketId(rs.getString("lm_tc_id"));
            hdTicketVO.setJobId(rs.getLong("lm_js_id"));
            hdTicketVO.setAppendixId(rs.getLong("lx_pr_id"));
            return hdTicketVO;
        }
    }

}
