package com.ilex.ws.core.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.NewDispatchVO;
import com.ilex.ws.core.bean.TicketScheduleInfo;
import com.ilex.ws.core.dao.NewWebTicketDAO;
import com.ilex.ws.core.dto.IlexTimestamp;
import com.ilex.ws.core.exception.IlexSystemException;
import com.mind.bean.newjobdb.ResourceLevel;
import com.mind.bean.prm.NewWebTicketBean;
import com.mind.bean.prm.ViewTicketSummaryBean;
import com.mind.common.LabelValue;
import com.mind.dao.PRM.NewWebTicketdao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.newjobdb.dao.TicketDAO;
import com.mind.newjobdb.dao.TicketNatureDAO;
import com.mind.newjobdb.dao.TicketRequestInfoDAO;

/**
 * The Class NewWebTicketDAOImpl.
 */
@Repository
public class NewWebTicketDAOImpl implements NewWebTicketDAO {

	/** The Constant logger. */
	private static final Logger logger = Logger
			.getLogger(NewWebTicketDAOImpl.class);

	/** The jdbc template ilex. */
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.NewWebTicketDAO#getWebTicketList()
	 */
	@Override
	public List<NewWebTicketBean> getWebTicketList() {

		return NewWebTicketdao.getWebTicketList(jdbcTemplateIlex
				.getDataSource());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.NewWebTicketDAO#getWebTicketSummary(java.lang.String
	 * )
	 */
	@Override
	public ViewTicketSummaryBean getWebTicketSummary(String ticketNumber) {
		ViewTicketSummaryBean viewTicketSummaryBean = new ViewTicketSummaryBean();
		NewWebTicketdao.getWebTicketSummary(ticketNumber,
				viewTicketSummaryBean, jdbcTemplateIlex.getDataSource());
		return viewTicketSummaryBean;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.NewWebTicketDAO#getRequestors(long)
	 */
	@Override
	public List<LabelValue> getRequestors(long msaId) {
		return TicketRequestInfoDAO.getRequestors(msaId,
				jdbcTemplateIlex.getDataSource());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.NewWebTicketDAO#getRequestTypes()
	 */
	@Override
	public List<LabelValue> getRequestTypes() {
		return TicketRequestInfoDAO.getRequestTypes(jdbcTemplateIlex
				.getDataSource());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.NewWebTicketDAO#getCriticalities(long)
	 */
	@Override
	public List<LabelValue> getCriticalities(long msaId) {
		return TicketRequestInfoDAO.getCriticalities(msaId,
				jdbcTemplateIlex.getDataSource());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.NewWebTicketDAO#getResources(java.util.Map,
	 * long, java.lang.String, boolean)
	 */
	@Override
	public List<ResourceLevel> getResources(Map<String, Object> map,
			long appendixId, String criticalityId, boolean isPPS) {
		return TicketRequestInfoDAO.getResources(map, appendixId,
				criticalityId, isPPS, jdbcTemplateIlex.getDataSource());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.NewWebTicketDAO#getProblemCategories()
	 */
	@Override
	public List<LabelValue> getProblemCategories() {
		return TicketNatureDAO.getProblemCategories(jdbcTemplateIlex
				.getDataSource());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.NewWebTicketDAO#getTicketInfo(long)
	 */
	@Override
	public NewDispatchVO getTicketInfo(long ticketId) {

		String sql = "select * from dbo.func_ticket_detail_01(" + ticketId
				+ ")";
		NewDispatchVO newDispatchVO = jdbcTemplateIlex.queryForObject(sql,
				new NewDispatchTicketInfoMapper());
		return newDispatchVO;

	}

	/**
	 * The Class NewDispatchTicketInfoMapper.
	 */
	private static final class NewDispatchTicketInfoMapper implements
			RowMapper<NewDispatchVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		public NewDispatchVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			NewDispatchVO newDispatchVO = new NewDispatchVO();
			TicketScheduleInfo ticketScheduleInfo = new TicketScheduleInfo();
			newDispatchVO.setTicketId(rs.getLong("lm_tc_id"));
			newDispatchVO.setJobId(rs.getLong("lm_tc_js_id"));
			newDispatchVO.setTicketNumber(rs.getString("lm_tc_number"));
			newDispatchVO.setRequestor(rs.getString("lm_tc_requestor_name"));
			newDispatchVO.setRequestorEmail(rs
					.getString("lm_tc_requestor_email"));
			newDispatchVO.setRequestType(rs.getString("lm_tc_request_type"));
			newDispatchVO.setResource(rs.getString("lm_tc_resource_level"));
			newDispatchVO.setStandBy(rs.getBoolean("lm_tc_standby"));

			if (rs.getString("nonPPS") != null) {
				if (rs.getString("nonPPS").equals("Y")) {
					newDispatchVO.setPps(false);
				} else {
					newDispatchVO.setPps(true);
				}
			} else {
				newDispatchVO.setPps(true);
			}

			newDispatchVO.setMsp(rs.getBoolean("lm_tc_msp"));
			newDispatchVO.setHelpDesk(rs.getBoolean("lm_tc_help_desk"));

			newDispatchVO.setProblemCategory(rs.getString("lm_tc_category"));
			newDispatchVO.setProblemDescription(rs
					.getString("lm_tc_problem_desc"));
			newDispatchVO.setCustomerReference(rs
					.getString("lm_tc_cust_reference"));
			newDispatchVO.setSpecialInstructions(rs
					.getString("lm_tc_spec_instruction"));
			newDispatchVO.setCriticality(rs.getString("lm_tc_criticality"));
			ticketScheduleInfo.setRequestedTimestamp(IlexTimestamp
					.converToTimestamp(rs.getString("lm_tc_requested_date")));

			ticketScheduleInfo
					.setPreferredTimestamp(IlexTimestamp.converToTimestamp(rs
							.getString("lm_tc_preferred_arrival")));
			ticketScheduleInfo.setWindowToTimestamp(IlexTimestamp
					.converToTimestamp(rs
							.getString("lm_tc_preferred_window_end")));
			ticketScheduleInfo.setWindowFromTimestamp(IlexTimestamp
					.converToTimestamp(rs
							.getString("lm_tc_preferred_window_start")));
			ticketScheduleInfo.setReceivedTimestamp(IlexTimestamp
					.converToTimestamp(rs.getString("lm_js_create_date")));

			newDispatchVO.setTicketScheduleInfo(ticketScheduleInfo);

			return newDispatchVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.NewWebTicketDAO#updateTicket(com.ilex.ws.core.bean
	 * .NewDispatchVO, java.lang.Long)
	 */
	@Override
	public void updateTicket(NewDispatchVO newDispatchVO, Long userLoginId) {
		Connection conn = null;
		CallableStatement cstmt = null;
		try {
			conn = jdbcTemplateIlex.getDataSource().getConnection();

			cstmt = conn
					.prepareCall("{?=call lm_job_ticket_manage_02(?,?,?,?, ?,?,?,?, ?,?,?,?, ?,?,?,?, ?,?,?,?, ?,?,?,?,	?,?,?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, newDispatchVO.getTicketNumber());
			cstmt.setString(3, getRequesterName(newDispatchVO.getRequestor()));
			cstmt.setString(4, newDispatchVO.getRequestorEmail());
			cstmt.setString(5, newDispatchVO.getSiteListId());
			cstmt.setString(6, IlexTimestamp.getDateTimeType2(newDispatchVO
					.getTicketScheduleInfo().getRequestedTimestamp()));
			cstmt.setBoolean(7, newDispatchVO.isStandBy());
			cstmt.setBoolean(8, newDispatchVO.isMsp());
			cstmt.setBoolean(9, newDispatchVO.isHelpDesk());
			cstmt.setString(10, newDispatchVO.getProblemCategory());
			cstmt.setString(11, newDispatchVO.getCustomerReference());
			cstmt.setString(12, newDispatchVO.getProblemDescription());
			cstmt.setString(13, newDispatchVO.getSpecialInstructions());
			cstmt.setString(14, newDispatchVO.getCriticality());
			cstmt.setString(15, newDispatchVO.getResource());
			cstmt.setString(16, IlexTimestamp.getDateTimeType2(newDispatchVO
					.getTicketScheduleInfo().getPreferredTimestamp()));
			cstmt.setString(17, IlexTimestamp.getDateTimeType2(newDispatchVO
					.getTicketScheduleInfo().getWindowFromTimestamp()));
			cstmt.setString(18, IlexTimestamp.getDateTimeType2(newDispatchVO
					.getTicketScheduleInfo().getWindowToTimestamp()));
			cstmt.setFloat(19, userLoginId); // - login user id
			cstmt.setString(20,
					getPPSValue(newDispatchVO.getCriticality(), true));
			cstmt.setFloat(21, newDispatchVO.getAppendixId());
			cstmt.setString(22, newDispatchVO.getRequestType());

			if (newDispatchVO.isPps()) {
				cstmt.setString(23, "Y");
			} else {
				cstmt.setString(23, "N");
			}

			cstmt.setString(24, newDispatchVO.getWebTicket());
			cstmt.setInt(
					25,
					newDispatchVO.getMonitoringSourceCd() != null ? newDispatchVO
							.getMonitoringSourceCd() : Integer.valueOf(-1));
			cstmt.setLong(
					26,
					newDispatchVO.getDeviceId() != null ? newDispatchVO
							.getDeviceId() : Long.valueOf(-1));

			cstmt.registerOutParameter(27, java.sql.Types.VARCHAR);
			cstmt.registerOutParameter(28, java.sql.Types.VARCHAR);
			cstmt.registerOutParameter(29, java.sql.Types.TIMESTAMP);
			cstmt.execute();

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new IlexSystemException(e.getMessage(), e);
		} finally {
			DBUtil.close(cstmt);
			DBUtil.close(conn);

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.NewWebTicketDAO#updateTicketJobOwner(com.ilex.ws
	 * .core.bean.NewDispatchVO, java.lang.Long)
	 */
	@Override
	public void updateTicketJobOwner(NewDispatchVO newDispatchVO,
			Long userLoginId) {
		StringBuilder updateJobOwner = new StringBuilder(
				"update lm_job set  lm_js_changed_by=?,lm_js_created_by=?,lm_js_change_date=getDate() where lm_js_id=?");
		Object[] params = new Object[] { userLoginId, userLoginId,
				newDispatchVO.getJobId() };

		jdbcTemplateIlex.update(updateJobOwner.toString(), params);
	}

	/**
	 * Gets the requester name.
	 * 
	 * @param str
	 *            the str
	 * @return the requester name
	 */
	private static String getRequesterName(String str) {
		if (str.indexOf("~") > -1)
			return str.substring(0, str.indexOf("~"));
		else
			return str;
	}

	/**
	 * Gets the pPS value.
	 * 
	 * @param criticalityId
	 *            the criticality id
	 * @param isPPS
	 *            the is pps
	 * @return the pPS value
	 */
	public String getPPSValue(String criticalityId, boolean isPPS) {
		String value = "";
		String[] pps = getPPSDate(criticalityId);
		if (isPPS) {
			value = pps[0];
		} else {
			value = pps[1];
		}
		return value;
	}

	/**
	 * Gets the pPS date.
	 * 
	 * @param criticalityId
	 *            the criticality id
	 * @return the pPS date
	 */
	public String[] getPPSDate(String criticalityId) {
		return TicketDAO.getPPSDate(criticalityId,
				jdbcTemplateIlex.getDataSource());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.NewWebTicketDAO#getRules(java.lang.Long)
	 */
	@Override
	public String getRules(Long appendixId) {

		return TicketDAO.getRules(String.valueOf(appendixId),
				jdbcTemplateIlex.getDataSource());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.NewWebTicketDAO#getStates()
	 */
	@Override
	public List<LabelValue> getStates() {
		return Pvsdao.getStateList(jdbcTemplateIlex.getDataSource());
	}

}
