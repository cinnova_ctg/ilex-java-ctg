package com.ilex.ws.core.dao;

import java.util.List;

public interface AppendixCustomerInfo {

	List<String> getCustInfoParameter(int appendixId);

}
