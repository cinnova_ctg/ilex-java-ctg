package com.ilex.ws.core.dao;


public interface WugErrorLogInsertDAO {
	void insertErrorLog(String code, String data, String deleteDeltaMonths);
}
