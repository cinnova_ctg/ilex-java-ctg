package com.ilex.ws.core.dao;

import com.ilex.ws.core.bean.InvBillToAddressVO;

public interface InvBillToAddressDAO {

	int insertBillToAddressDetails(InvBillToAddressVO invBillToAddressVO);

	InvBillToAddressVO getBillToAddressDetails(int invBillToAddressId);

	void updateBillToAddressDetails(InvBillToAddressVO invBillToAddressVO);

}
