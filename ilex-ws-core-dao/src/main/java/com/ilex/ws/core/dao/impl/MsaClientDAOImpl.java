package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.MsaClientVO;
import com.ilex.ws.core.dao.MsaClientDAO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.MessageKeyConstant;

@Repository
public class MsaClientDAOImpl implements MsaClientDAO {
	public static final Logger logger = Logger
			.getLogger(MsaClientDAOImpl.class);
	/** The jdbc template ilex. */
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.MasterBTMDAO#getClientList()
	 */

	public List<MsaClientVO> getClientList() {
		StringBuilder sqlQuery = new StringBuilder(
				"select lp_mm_id,lo_ot_id,lo_ot_name,lp_md_status,lp_md_formal_msa_date,lp_md_payment_terms");
		sqlQuery.append(" from lp_msa_main");
		sqlQuery.append(" join lp_msa_detail on lp_mm_id=lp_md_mm_id");
		sqlQuery.append(" join lo_organization_top on lp_mm_ot_id =lo_ot_id ");
		sqlQuery.append(" where lp_md_formal_msa_date>dateadd(mm,-12,getdate())");
		sqlQuery.append(" and lp_mm_id not in(select in_mbt_mm_id from in_bill_to_master)");
		sqlQuery.append(" order by lo_ot_name");
		List<MsaClientVO> msaClientVOList = jdbcTemplateIlex.query(
				sqlQuery.toString(), new ClientDetailMapper());

		if (msaClientVOList == null || msaClientVOList.size() == 0) {
			throw new IlexBusinessException(
					MessageKeyConstant.CLIENT_DETAIL_NOT_FOUND);
		}
		return msaClientVOList;
	}

	/**
	 * The Class ClientDetailMapper.
	 */
	private static final class ClientDetailMapper implements
			RowMapper<MsaClientVO> {

		public MsaClientVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			MsaClientVO msaClientVO = new MsaClientVO();
			msaClientVO.setClientName(rs.getString("lo_ot_name"));
			msaClientVO.setClientId(String.valueOf((rs.getInt("lo_ot_id"))));
			msaClientVO.setMmId(rs.getInt("lp_mm_id"));
			return msaClientVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.MsaClientDAO#getClientDetail(com.ilex.ws.core.bean
	 * .MsaClientVO)
	 */
	@Override
	public MsaClientVO getClientDetail(MsaClientVO argMsaClientVO) {
		StringBuilder sqlQuery = new StringBuilder(
				"select lo_ot_id,lo_ot_name,lp_mm_id");

		sqlQuery.append(" from lp_msa_main");
		sqlQuery.append(" join lp_msa_detail on lp_mm_id=lp_md_mm_id");
		sqlQuery.append(" join lo_organization_top on lp_mm_ot_id =lo_ot_id");
		sqlQuery.append(" where lp_md_formal_msa_date>dateadd(mm,-12,getdate())");
		sqlQuery.append(" and lp_mm_id not in(select in_mbt_mm_id from in_bill_to_master)");
		sqlQuery.append(" and lo_ot_id=?");
		Object[] params = new Object[] { argMsaClientVO.getClientId() };
		MsaClientVO msaClientVO;

		try {
			msaClientVO = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
					params, new ClientDetailMapper());
		} catch (EmptyResultDataAccessException ex) {
			throw new IlexBusinessException(
					MessageKeyConstant.CLIENT_DETAIL_NOT_FOUND);
		}

		return msaClientVO;
	}

	@Override
	public MsaClientVO getClientDetailById(String msaId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select lo_ot_name,lo_ot_id,lp_mm_id from lp_msa_main ");

		sqlQuery.append(" join lo_organization_top ");
		sqlQuery.append(" on lp_mm_ot_id=lo_ot_id");
		sqlQuery.append(" where lp_mm_id=?");
		Object[] params = new Object[] { msaId };
		MsaClientVO msaClientVO = new MsaClientVO();
		try {
			msaClientVO = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
					params, new ClientDetailMapper());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return msaClientVO;
	}

}
