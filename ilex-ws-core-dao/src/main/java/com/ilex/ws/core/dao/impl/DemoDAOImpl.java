package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.UserDTO;
import com.ilex.ws.core.dao.DemoDAO;
import com.ilex.ws.core.exception.IlexBusinessException;

@Repository
public class DemoDAOImpl implements DemoDAO {

	@Resource(name = "jdbcTemplateIlex")
	private JdbcTemplate jdbcTemplateIlex;

	public UserDTO getUserInfo(String userId) {
		String sqlQuery = "select * from lo_user where lo_us_pc_id =?";
		Object[] params = new Object[] { userId };
		List<UserDTO> userList = jdbcTemplateIlex.query(sqlQuery, params,
				new UserMapper());

		if (userList == null || userList.size() == 0) {
			throw new IlexBusinessException("e.user.not.found");
		}
		return userList.get(0);
	}

	private static final class UserMapper implements RowMapper<UserDTO> {

		public UserDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			UserDTO user = new UserDTO();
			user.setUserId(rs.getString("lo_us_pc_id"));
			user.setUserName(rs.getString("lo_us_name"));
			user.setUserPassword("********");
			return user;
		}
	}

}
