package com.ilex.ws.core.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.MacWorxDetailVO;
import com.ilex.ws.core.bean.MacWorxIntergrationResponseVO;
import com.ilex.ws.core.bean.MacWorxIntergrationVO;
import com.ilex.ws.core.dao.MacWorxDAO;
import com.ilex.ws.core.exception.IlexSystemException;
import com.lowagie.text.DocumentException;
import com.mind.bean.docm.Document;
import com.mind.common.dao.RestClient;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.docm.client.ClientOperator;
import com.mind.docm.dao.DocumentAddDao;
import com.mind.docm.exceptions.ControlledTypeException;
import com.mind.docm.exceptions.CouldNotAddToRepositoryException;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.CouldNotReplaceException;
import com.mind.docm.exceptions.DocMFormatException;
import com.mind.fw.core.dao.util.DBUtil;

@Repository
public class MacWorxDAOImpl implements MacWorxDAO {
	public static final Logger logger = Logger.getLogger(MacWorxDAOImpl.class);
	@Resource
	private JdbcTemplate jdbcTemplateIlex;
	@Resource
	private JdbcTemplate jdbcTemplateDocmSys;
	@Resource
	private JdbcTemplate jdbcTemplateDocMFileSys;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.MACWorXDAO#getLatitudeLongitude(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String[] getLatitudeLongitude(String address, String city,
			String state, String country, String zipcode) {
		String[] latLongDetails = RestClient
				.getLatitudeLongitude(address, city, state, country, zipcode,
						jdbcTemplateIlex.getDataSource());

		return latLongDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.MACWorXDAO#createJobForMegaPath(com.ilex.ws.core
	 * .bean.MacWorxIntergrationVO)
	 */
	@Override
	public MacWorxIntergrationResponseVO createJobForMegaPath(
			MacWorxIntergrationVO macWorxIntergrationVO) {
		Connection con = null;
		CallableStatement cstmt = null;
		MacWorxIntergrationResponseVO macWorxIntergrationResponseVO = new MacWorxIntergrationResponseVO();
		try {

			con = jdbcTemplateIlex.getDataSource().getConnection();
			cstmt = con
					.prepareCall("{call dbo.lm_job_create_macx_megapath_02(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setInt(1, macWorxIntergrationVO.getMacxProjectId());
			cstmt.setString(2, macWorxIntergrationVO.getMacxScheduleDate());
			cstmt.setString(3, macWorxIntergrationVO.getMacxScheduleTime());
			cstmt.setString(4, macWorxIntergrationVO.getMacxSiteNumber());
			cstmt.setString(5, macWorxIntergrationVO.getMacxSiteLocation());
			cstmt.setString(6, macWorxIntergrationVO.getMacxAddress1());
			cstmt.setString(7, macWorxIntergrationVO.getMacxAddress2());
			cstmt.setString(8, macWorxIntergrationVO.getMacxCity());
			cstmt.setString(9, macWorxIntergrationVO.getMacxState());
			cstmt.setString(10, macWorxIntergrationVO.getMacxZipCode());
			cstmt.setString(11, macWorxIntergrationVO.getMacxPocName1());
			cstmt.setString(12, macWorxIntergrationVO.getMacxPocPhone1());
			cstmt.setString(13, macWorxIntergrationVO.getMacxPocEmail1());
			cstmt.setString(14, macWorxIntergrationVO.getMacxPocName2());
			cstmt.setString(15, macWorxIntergrationVO.getMacxPocPhone2());
			cstmt.setString(16, macWorxIntergrationVO.getMacxPocEmail2());
			cstmt.setString(17, macWorxIntergrationVO.getMacxActivityList());
			cstmt.setString(18, macWorxIntergrationVO.getMacxActivityQty());
			cstmt.setString(19, macWorxIntergrationVO.getMacxRequestor());
			cstmt.setString(20, macWorxIntergrationVO.getMacxCriticality());
			cstmt.setString(21,
					macWorxIntergrationVO.getMacSpecialInstructions());
			cstmt.setString(22, macWorxIntergrationVO.getMacxInstallNotes());
			cstmt.setString(23, macWorxIntergrationVO.getMacxPoNumber());
			cstmt.setString(24, macWorxIntergrationVO.getLatLongSource());
			if (macWorxIntergrationVO.getLatitude() == null) {
				cstmt.setNull(25, java.sql.Types.NULL);
			} else {
				cstmt.setFloat(25,
						Float.parseFloat(macWorxIntergrationVO.getLatitude()));
			}

			if (macWorxIntergrationVO.getLongitude() == null) {
				cstmt.setNull(26, java.sql.Types.NULL);
			} else {
				cstmt.setFloat(26,
						Float.parseFloat(macWorxIntergrationVO.getLongitude()));
			}

			cstmt.setString(27, macWorxIntergrationVO.getLatLongAccuracy());
			cstmt.registerOutParameter(28, java.sql.Types.VARCHAR);
			cstmt.registerOutParameter(29, java.sql.Types.NUMERIC);
			cstmt.registerOutParameter(30, java.sql.Types.NUMERIC);
			cstmt.registerOutParameter(31, java.sql.Types.INTEGER);
			cstmt.setString(32, macWorxIntergrationVO.getMacxNonPps());
			cstmt.execute();
			macWorxIntergrationResponseVO.setTicketNumber(cstmt.getString(28));
			macWorxIntergrationResponseVO.setJobId(cstmt.getInt(29));
			macWorxIntergrationResponseVO.setPoId(cstmt.getInt(30));
			macWorxIntergrationResponseVO.setStatus(cstmt.getInt(31));

		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new IlexSystemException(e.getMessage(), e);
		} finally {
			DBUtil.close(con);
			DBUtil.close(cstmt);
		}
		return macWorxIntergrationResponseVO;

	}

	@Override
	public String getJobOwnerName(int jobId) {
		String sqlQuery = "select lo_pc_first_name,lo_pc_last_name from lo_poc where lo_pc_id=";
		sqlQuery += "(select lm_js_created_by from lm_job where lm_js_id=?)";
		Object[] params = new Object[] { jobId };
		String jobOwnerName;
		try {
			jobOwnerName = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
					params, new JobOwnerMapper());
		} catch (EmptyResultDataAccessException ex) {
			jobOwnerName = "";
		}

		return jobOwnerName;

	}

	private static final class JobOwnerMapper implements RowMapper<String> {

		@Override
		public String mapRow(ResultSet rs, int rowNum) throws SQLException {
			StringBuilder firstName = new StringBuilder(
					rs.getString("lo_pc_first_name"));
			StringBuilder lastName = new StringBuilder(
					rs.getString("lo_pc_last_name"));

			return firstName.append(" ").append(lastName).toString();
		}

	}

	@Override
	public int uploadFile(Document doc) {
		int docId;
		try {
			docId = ClientOperator.explicitAddDocument(doc,
					jdbcTemplateDocmSys.getDataSource(),
					jdbcTemplateDocMFileSys.getDataSource());
		} catch (ControlledTypeException e) {
			logger.error(e);
			return 0;
		} catch (CouldNotAddToRepositoryException e) {
			logger.error(e);
			return 0;
		} catch (CouldNotReplaceException e) {
			logger.error(e);
			return 0;
		} catch (CouldNotCheckDocumentException e) {
			logger.error(e);
			return 0;
		} catch (DocMFormatException e) {
			logger.error(e);
			return 0;
		} catch (DocumentException e) {
			logger.error(e);
			return 0;
		}

		return docId;

	}

	@Override
	public void insertDocEntryToPoDocument(final int poId, final int docId,
			final String userName) {
		final StringBuilder sql = new StringBuilder(
				"insert into podb_document ( lm_po_id,podb_doc_id,doc_with_po,doc_with_wo,podb_doc_created_by,");
		sql.append(" podb_doc_created_date,podb_doc_updated_by,podb_doc_updated_date)");
		sql.append("   values (?,?,?,?,?,getDate(),?,getDate())");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql
						.toString());
				ps.setInt(1, poId);
				ps.setInt(2, docId);
				ps.setString(3, "1"); // TO Include with PO
				ps.setString(4, "1"); // TO Include with WO
				ps.setString(5, userName);
				ps.setString(6, userName);
				return ps;
			}
		};
		jdbcTemplateIlex.update(preparedStatementCreator);

	}

	@Override
	public MacWorxDetailVO getMetaDataForMacWorx(String jobId, String poId) {
		MacWorxDetailVO macWorxDetailVO = new MacWorxDetailVO();

		macWorxDetailVO.setJobName(Activitydao.getJobname(
				jdbcTemplateIlex.getDataSource(), jobId));
		macWorxDetailVO.setPartnerName(DocumentAddDao.getPartnerName(
				jdbcTemplateIlex.getDataSource(), poId));
		String appendixId = IMdao.getAppendixId(jobId,
				jdbcTemplateIlex.getDataSource());
		String msaId = IMdao.getMSAId(appendixId,
				jdbcTemplateIlex.getDataSource());
		macWorxDetailVO.setAppendixId(appendixId);
		macWorxDetailVO.setMsaId(msaId);
		macWorxDetailVO.setAppendixName(Jobdao.getAppendixname(
				jdbcTemplateIlex.getDataSource(), appendixId));
		macWorxDetailVO.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
				jdbcTemplateIlex.getDataSource(), msaId));

		return macWorxDetailVO;

	}

	@Override
	public int macWorxUserAuth(String userName, String password, int prId) {

		// NetMedXIntegrationResponseVO netMedXIntegrationResponseVO = new
		// NetMedXIntegrationResponseVO();
		int accesslvl = 0;// 0 means invalid access
		String netmedxAccess = "select lm_pr_auth_access from lm_pr_authentication where lm_pr_auth_user_name = '"
				+ userName
				+ "' and lm_pr_auth_password = '"
				+ password
				+ "' and lm_pr_auth_project_id = " + prId;

		try {
			accesslvl = jdbcTemplateIlex.queryForInt(netmedxAccess);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return accesslvl;

	}

}
