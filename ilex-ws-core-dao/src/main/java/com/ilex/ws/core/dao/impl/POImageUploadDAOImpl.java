package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.POImageDetailVO;
import com.ilex.ws.core.bean.POImageUploadVO;
import com.ilex.ws.core.dao.POImageUploadDAO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.MessageKeyConstant;
import com.mind.PO.dao.PODao;
import com.mind.bean.mpo.PODTO;
import com.mind.common.PODeliverableStatusTypes;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.POWODetaildao;
import com.mind.docm.dao.DocumentAddDao;
import com.mind.docm.dao.EntityManager;
import com.mind.docm.exceptions.CouldNotAddToRepositoryException;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.DocMFormatException;
import com.mind.docm.exceptions.EntityDetailsNotFoundException;

@Repository
public class POImageUploadDAOImpl implements POImageUploadDAO {
	@Resource
	private JdbcTemplate jdbcTemplateIlex;
	@Resource
	private JdbcTemplate jdbcTemplateDocmSys;
	@Resource
	private JdbcTemplate jdbcTemplateDocMFileSys;

	
	public String getPartnerId(String authenticationString) {
		String sqlQuery = "select lm_pa_partner_id from lm_partner_authentication where lm_pa_authentication= ?";
		Object[] params = new Object[] { authenticationString };
		String partnerId = null;
		try {
			partnerId = jdbcTemplateIlex.queryForObject(sqlQuery, params,
					String.class);
		} catch (EmptyResultDataAccessException ex) {
			throw new IlexBusinessException(
					MessageKeyConstant.PARTNER_NOT_FOUND);
		}

		if (partnerId == null) {
			throw new IlexBusinessException(
					MessageKeyConstant.PARTNER_NOT_FOUND);
		}

		return partnerId;
	}

	
	public List<POImageDetailVO> getPOImageDetail(String poId, String partnerId) {
		StringBuilder sqlQuery = new StringBuilder(
				"select podb_mn_dl_id as image_number,podb_mn_dl_title as image_name, podb_mn_dl_desc AS description,isnull(lm_si_city, '') AS city,isnull(lm_si_end_customer, '') AS customer "
						+ ",lm_js_create_date AS job_create_date  ");
		sqlQuery.append(" from lm_purchase_order as  a");
		sqlQuery.append(" join podb_purchase_order_deliverables as  b on a.lm_po_id=b.lm_po_id");
		sqlQuery.append(" JOIN lm_job  ON lm_job.lm_js_id = a.lm_po_js_id");
		sqlQuery.append(" LEFT JOIN lm_site_detail ON lm_si_id = lm_js_si_id ");
		sqlQuery.append(" where a.lm_po_id =? ");
		sqlQuery.append(" and lm_po_partner_id=?");
		sqlQuery.append(" and isnull(podb_mn_dl_status,0)<> "
				+ String.valueOf((PODeliverableStatusTypes.STATUS_ACCEPTED)));
		sqlQuery.append(" and isnull(podb_mn_mobile_upload_allowed,'n')='y'");
		Object[] params = new Object[] { poId, partnerId };
		List<POImageDetailVO> imageDetailList = jdbcTemplateIlex.query(
				sqlQuery.toString(), params, new POImageDetailMapper());

		if (imageDetailList == null || imageDetailList.size() == 0) {
			throw new IlexBusinessException(
					MessageKeyConstant.IMAGE_DETAIL_NOT_FOUND);
		}
		return imageDetailList;
	}

	private static final class POImageDetailMapper implements
			RowMapper<POImageDetailVO> {

		public POImageDetailVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			POImageDetailVO imageDetail = new POImageDetailVO();
			imageDetail.setImageName(rs.getString("image_name"));
			imageDetail.setImageNumber(rs.getString("image_number"));
			imageDetail.setDescription(rs.getString("description"));
			imageDetail.setCity(rs.getString("city"));
			imageDetail.setCustomer(rs.getString("customer"));
			imageDetail.setCreateDate(rs.getString("job_create_date"));
			return imageDetail;
		}
	}

	
	public boolean poImageUpload(POImageUploadVO pOImageUploadVO) {
		String docId = "";
		PODTO poDTO = new PODTO();
		PODao dao = new PODao();
		poDTO.setDevId(pOImageUploadVO.getImageNumber());
		poDTO.setDevTitle(pOImageUploadVO.getImageName());
		DateFormat dateFormat = new SimpleDateFormat(
				MessageKeyConstant.ILEX_WS_DATE_TIME_FORMAT);
		Calendar cal = Calendar.getInstance();
		poDTO.setDevDate(dateFormat.format(cal.getTime()));
		poDTO.setUserName(getPartnerId(pOImageUploadVO.getAuthentication()));
		poDTO.setDevFormat(MessageKeyConstant.UPLOADTYPE);
		poDTO.setPoId(pOImageUploadVO.getPoId());
		poDTO.setJobId(POWODetaildao.getJobIdFromPoId(
				jdbcTemplateIlex.getDataSource(), poDTO.getPoId()));
		poDTO.setJob_Name(Activitydao.getJobname(
				jdbcTemplateIlex.getDataSource(), poDTO.getJobId()));
		poDTO.setWO_ID(poDTO.getPoId());
		poDTO.setPartner_Name(DocumentAddDao.getPartnerName(
				jdbcTemplateIlex.getDataSource(), poDTO.getPoId()));
		poDTO.setAppendixId(IMdao.getAppendixId(poDTO.getJobId(),
				jdbcTemplateIlex.getDataSource()));
		poDTO.setMsaId(IMdao.getMSAId(poDTO.getAppendixId(),
				jdbcTemplateIlex.getDataSource()));
		poDTO.setAppendix_Name(Jobdao.getAppendixname(
				jdbcTemplateIlex.getDataSource(), poDTO.getAppendixId()));
		poDTO.setMSA_Name(com.mind.dao.PM.Appendixdao.getMsaname(
				jdbcTemplateIlex.getDataSource(), poDTO.getMsaId()));
		poDTO.setDevDescription(pOImageUploadVO.getDescription());
	//	poDTO.setDevId("467097");
	//	pOImageUploadVO.setImageNumber("467097");

		try {
			docId = EntityManager.approveEntity(poDTO,
					MessageKeyConstant.DELIVERABLES, pOImageUploadVO.getFile(),
					pOImageUploadVO.getFileName(),
					jdbcTemplateIlex.getDataSource(),
					jdbcTemplateDocmSys.getDataSource(),
					jdbcTemplateDocMFileSys.getDataSource());
		} catch (DocMFormatException e) {
			throw new IlexBusinessException(MessageKeyConstant.DOCUMENT_FORMAT);

		} catch (CouldNotAddToRepositoryException e) {
			throw new IlexBusinessException(
					MessageKeyConstant.COULD_NOT_ADD_TO_REPOSITORY);
		} catch (CouldNotCheckDocumentException e) {
			throw new IlexBusinessException(
					MessageKeyConstant.COULD_NOT_CHECK_DOCUMENT);
		} catch (EntityDetailsNotFoundException e) {
			throw new IlexBusinessException(
					MessageKeyConstant.ENTITY_DETAILS_NOT_FOUND);
		}
		// set source of uploading deliverable to Mobile Application version
		// (using ws
		// Application and status to uploaded)
		poDTO.setUploadSource(MessageKeyConstant.UPLOAD_SOURCE);
		poDTO.setDevStatus(String
				.valueOf(PODeliverableStatusTypes.STATUS_UPLOADED));
		dao.savePODeliverablesUpload(poDTO, poDTO.getUserName(), docId,
				jdbcTemplateIlex.getDataSource());
		return true;
	}


	public int getPODeliverableStatus(String imageNumber) {
		String sqlQuery = "select podb_mn_dl_status from podb_purchase_order_deliverables where podb_mn_dl_id= ?";
		Object[] params = new Object[] { imageNumber };
		int deliverableStatus = -1;
		try {
			deliverableStatus = jdbcTemplateIlex.queryForInt(sqlQuery, params);
		} catch (EmptyResultDataAccessException ex) {
			throw new IlexBusinessException(
					MessageKeyConstant.DELIVERABLE_STATUS_NOT_FOUND);
		}
		return deliverableStatus;
	}
}
