package com.ilex.ws.core.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ilex.ws.core.bean.CircuitStatusVO;
import com.ilex.ws.core.bean.EventCorrelatorVO;
import com.ilex.ws.core.bean.HDAuditTrailVO;
import com.ilex.ws.core.bean.HDBillableVO;
import com.ilex.ws.core.bean.HDCategoryVO;
import com.ilex.ws.core.bean.HDConfigurationItemVO;
import com.ilex.ws.core.bean.HDCubeStatusVO;
import com.ilex.ws.core.bean.HDCustomerDetailVO;
import com.ilex.ws.core.bean.HDEffortVO;
import com.ilex.ws.core.bean.HDHistoryVO;
import com.ilex.ws.core.bean.HDMonitoringTimeVO;
import com.ilex.ws.core.bean.HDNextActionVO;
import com.ilex.ws.core.bean.HDPendingAckTableVO;
import com.ilex.ws.core.bean.HDTicketVO;
import com.ilex.ws.core.bean.HDWIPTableVO;
import com.ilex.ws.core.bean.HDWorkNoteVO;
import com.ilex.ws.core.bean.IncidentStateVO;
import com.ilex.ws.core.bean.OutageDetailVO;
import com.ilex.ws.core.bean.ProvisioningDetailVO;
import com.ilex.ws.core.bean.ResolutionsVO;
import com.ilex.ws.core.bean.SiteDetailVO;
import com.ilex.ws.core.bean.StateTailVO;
import com.ilex.ws.core.bean.TicketStateVO;
import com.ilex.ws.core.bean.WugDetailsVO;
import com.ilex.ws.core.bean.WugOutInProcessVO;
import com.ilex.ws.core.dto.HDInstallNoteVO;
import com.mind.bean.msp.RealTimeState;
import com.mind.bean.msp.RealTimeStateInfo;

// TODO: Auto-generated Javadoc
/**
 * The Interface HDMainDAO.
 */
public interface HDMainDAO {

	/**
	 * Gets the hD customers.
	 * 
	 * @return the hD customers
	 */
	List<HDCustomerDetailVO> getHDCustomers();

	public boolean getnextactionduestatus();

	/**
	 * Gets the backup circuit status.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the backup circuit status
	 */
	String getBackupCircuitStatus(String siteNum);

	/**
	 * Gets the sites.
	 * 
	 * @param msaId
	 *            the msa id
	 * @return the sites
	 */
	List<SiteDetailVO> getSites(Long msaId);

	/**
	 * Gets the configuration items.
	 * 
	 * @return the configuration items
	 */
	List<HDConfigurationItemVO> getConfigurationItems();

	/**
	 * Gets the categories.
	 * 
	 * @return the categories
	 */
	List<HDCategoryVO> getCategories();

	/**
	 * Insert ticket to ilex.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @return the long
	 */
	Long insertTicketToIlex(HDTicketVO hdTicketVO);

	/**
	 * Insert hd ticket details.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 */
	void insertHDTicketDetails(Long ticketId, HDTicketVO hdTicketVO,
			String userId);

	/**
	 * Update hd ticket details.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 */
	void updateHDTicketDetails(Long ticketId, HDTicketVO hdTicketVO,
			String userId);

	/**
	 * Gets the site details.
	 * 
	 * @param siteId
	 *            the site id
	 * @return the site details
	 */
	SiteDetailVO getSiteDetails(String siteId);

	/**
	 * Gets data for pending ack cust cube.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the pending ac k cust data
	 */
	List<HDPendingAckTableVO> getPendingAcKCustData(String siteNum);

	/**
	 * Gets data for pending ack security cube.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the pending ack security data
	 */
	List<HDPendingAckTableVO> getPendingAcKSecurityData(String siteNum);

	/**
	 * Gets data for pending ack alert cube.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the pending ac k alert data
	 */
	List<HDPendingAckTableVO> getPendingAcKAlertData(String siteNum);

	/**
	 * Gets data for pending ack monitor cube.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the pending ac k monitor data
	 */
	List<HDPendingAckTableVO> getPendingAcKMonitorData(String siteNum,
			String inClause);

	/**
	 * Gets the list of next actions.
	 * 
	 * @return the next actions
	 */
	List<HDNextActionVO> getNextActions();

	/**
	 * Gets ticket details for tickets displayed under pending ack cubes.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the ack cust ticket details
	 */
	List<HDTicketVO> getAckCustTicketDetails(String ticketId);

	/**
	 * Gets the hd wip table data.
	 * 
	 * @param cubeId
	 *            the cube id
	 * @param siteNum
	 *            the site num
	 * @return the hd wip table data
	 */
	List<HDWIPTableVO> getHdWipTableData(String cubeId, String siteNum);

	/**
	 * Gets the hd wip next action table data.
	 * 
	 * @param siteNum
	 *            the site num
	 * @return the hd wip next action table data
	 */
	public List<HDWIPTableVO> getHdWipNextActionTableData(String siteNum);

	/**
	 * @param siteNum
	 * @return
	 */
	public List<HDWIPTableVO> getHdResolvedTableData(String siteNum);

	/**
	 * Gets the active tickets.
	 * 
	 * @param userId
	 *            the user id
	 * @param sessionId
	 *            the session id
	 * @return the active tickets
	 */
	List<HDWIPTableVO> getActiveTickets(String userId, String sessionId);

	/**
	 * Save efforts.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param effortValue
	 *            the effort value
	 * @param ppsFlag
	 *            the pps flag
	 * @param userId
	 *            the user id
	 * @param isBillable
	 *            the is billable
	 */
	void saveEfforts(String ticketId, String effortValue, Boolean ppsFlag,
			String userId, Boolean isBillable);

	/**
	 * Save work notes.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param workNotes
	 *            the work notes
	 * @param userId
	 *            the user id
	 */
	void saveWorkNotes(String ticketId, String workNotes, String userId);

	/**
	 * Gets the cube status for refresh logic.
	 * 
	 * @return the cube status
	 */
	HDCubeStatusVO getCubeStatus();

	/**
	 * Gets the ticket states for drop down.
	 * 
	 * @return the ticket states
	 */
	List<TicketStateVO> getTicketStates();

	/**
	 * Gets the circuit status for drop down.
	 * 
	 * @return the circuit status
	 */
	List<CircuitStatusVO> getCircuitStatus();

	/**
	 * Update circuit status of active ticket.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param cktStatus
	 *            the ckt status
	 * @param userId
	 *            the user id
	 */
	void updateCircuitStatus(String ticketId, String cktStatus, String userId);

	/**
	 * Gets the ticket summary.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the ticket summary
	 */
	HDTicketVO getTicketSummary(Long ticketId);

	/**
	 * Gets the work notes.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the work notes
	 */
	List<HDWorkNoteVO> getWorkNotes(Long ticketId);

	List<HDInstallNoteVO> getInstallNotes(Long ticketId);

	/**
	 * Gets the ticket details for edit.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the ticket details for edit
	 */
	HDTicketVO getTicketDetailsForEdit(String ticketId);

	/**
	 * Edits the ticket details.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 */
	void editTicketDetails(HDTicketVO hdTicketVO);

	/**
	 * Gets the priority details for edit.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the priority details for edit
	 */
	HDTicketVO getPriorityDetailsForEdit(String ticketId);

	/**
	 * Edits the priority section.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 */
	void editPrioritySection(HDTicketVO hdTicketVO, String userId);

	/**
	 * Gets the backup details for edit.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the backup details for edit
	 */
	HDTicketVO getBackupDetailsForEdit(String ticketId);

	/**
	 * Edits the backup details.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 */
	void editBackupDetails(HDTicketVO hdTicketVO, String userId);

	/**
	 * Gets the resolution details for edit.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the resolution details for edit
	 */
	HDTicketVO getResolutionDetailsForEdit(String ticketId);

	/**
	 * Gets the point of failure list.
	 * 
	 * @return the point of failure list
	 */
	Map<String, String> getPointOfFailureList();

	/**
	 * Gets the root cause list.
	 * 
	 * @return the root cause list
	 */
	Map<String, String> getRootCauseList();

	/**
	 * Gets the resolution list.
	 * 
	 * @return the resolution list
	 */
	Map<String, String> getResolutionList();

	/**
	 * Edits the resolution details.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 */
	void editResolutionDetails(HDTicketVO hdTicketVO, String userId);

	/**
	 * Gets the state tail.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the state tail
	 */
	StateTailVO getStateTail(String ticketId);

	/**
	 * Gets the total of efforts for a particular ticket.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the total effort
	 */
	Double getTotalEffort(Long ticketId);

	/**
	 * Gets the billable status.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the billable status
	 */
	Boolean getBillableStatus(Long ticketId);

	/**
	 * Gets the effective date for calculation of pps.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the effective datefor pps
	 */
	Date getEffectiveDateforPPS(Long ticketId);

	/**
	 * Manage all inserts for ticket history events.
	 * 
	 * @param hdHistoryVO
	 *            the hd history vo
	 */
	void manageChangeHistory(HDHistoryVO hdHistoryVO);

	/**
	 * Manage ticket detail status.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param state
	 *            the state
	 * @param userId
	 *            the user id
	 */
	void manageTicketDetailStatus(String ticketId, String state, String userId);

	/**
	 * Save audit trail.
	 * 
	 * @param hdAuditTrailVO
	 *            the hd audit trail vo
	 */
	void saveAuditTrail(HDAuditTrailVO hdAuditTrailVO);

	/**
	 * Gets the audit trail.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the audit trail
	 */
	List<HDAuditTrailVO> getAuditTrail(String ticketId);

	/**
	 * Gets the efforts.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the efforts
	 */
	List<HDEffortVO> getEfforts(Long ticketId);

	List<HDEffortVO> getEffortsDetail(Long ticketId);

	/**
	 * Gets the billable info.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the billable info
	 */
	HDBillableVO getBillableInfo(Long ticketId);

	/**
	 * Update ticket to resolved.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 */
	void updateTicketToResolved(HDTicketVO hdTicketVO, String userId);

	/**
	 * Adds the new site.
	 * 
	 * @param siteDetailVO
	 *            the site detail vo
	 * @param userId
	 *            the user id
	 * @return the long
	 */
	Long addNewSite(SiteDetailVO siteDetailVO, String userId);

	/**
	 * Copy site to lm site detail.
	 * 
	 * @param siteDetailVO
	 *            the site detail vo
	 * @param userId
	 *            the user id
	 * @return the long
	 */
	Long copySiteToLmSiteDetail(SiteDetailVO siteDetailVO, String userId);

	/**
	 * Check site exist.
	 * 
	 * @param siteDetailVO
	 *            the site detail vo
	 * @return true, if successful
	 */
	boolean checkSiteExist(SiteDetailVO siteDetailVO);

	/**
	 * Cancel ticket.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param userId
	 *            the user id
	 */
	void cancelTicket(String ticketId, String userId);

	/**
	 * Save no action.
	 * 
	 * @param ticketId
	 *            the ticket id
	 */
	void saveNoAction(String ticketId);

	/**
	 * Gets the no actions days data.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the no actions days data
	 */
	Map<String, String> getNoActionsDaysData(String ticketId);

	/**
	 * Gets the ticket number.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the ticket number
	 */
	String getTicketNumber(Long ticketId);

	/**
	 * Ack ticket.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 */
	void ackTicket(HDTicketVO hdTicketVO, String userId);

	/**
	 * Update site.
	 * 
	 * @param siteDetailVO
	 *            the site detail vo
	 * @param userId
	 *            the user id
	 */
	void updateSite(SiteDetailVO siteDetailVO, String userId);

	/**
	 * Gets the wUG information.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @return the wUG information
	 */
	WugDetailsVO getWUGInformation(HDTicketVO hdTicketVO);

	/**
	 * Provisioning detail.
	 * 
	 * @param siteId
	 *            the site id
	 * @return the provisioning detail vo
	 */
	ProvisioningDetailVO ProvisioningDetail(String siteId);

	/**
	 * Gets the wUG out in process data.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the wUG out in process data
	 */
	WugOutInProcessVO getWUGOutInProcessData(String ticketId);

	/**
	 * Job update for ticket to resolved.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 */
	void jobUpdateForTicketToResolved(HDTicketVO hdTicketVO, String userId);

	/**
	 * Gets the real time state list.
	 * 
	 * @return the real time state list
	 */
	List<RealTimeState> getRealTimeStateList();

	/**
	 * Gets the last update event detail.
	 * 
	 * @return the last update event detail
	 */
	RealTimeStateInfo getLastUpdateEventDetail();

	/**
	 * Wug info map.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the map
	 */
	Map<String, Object> wugInfoMap(String ticketId);

	/**
	 * Update cube display status.
	 * 
	 * @param cubes
	 *            the cubes
	 */
	void updateCubeDisplayStatus(String cubes);

	/**
	 * Check ticket status before no action.
	 * 
	 * @param ticketId
	 *            the ticket id
	 */
	void checkTicketStatusBeforeNoAction(String ticketId);

	/**
	 * Gets the site id from job.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the site id from job
	 */
	Long getSiteIdFromJob(Long jobId);

	public Long getJobIdByTicketNumber(String ticketNumber);

	/**
	 * Update site.
	 * 
	 * @param siteDetailId
	 *            the site detail id
	 * @param primaryName
	 *            the primary name
	 * @param sitePhone
	 *            the site phone
	 * @param primaryEmail
	 *            the primary email
	 */
	void updateSite(Long siteDetailId, String primaryName, String sitePhone,
			String primaryEmail);

	/**
	 * Avaliable ilex ticket number.
	 * 
	 * @return the string
	 */
	String avaliableIlexTicketNumber();

	/**
	 * Gets the customer list for cancel all.
	 * 
	 * @return the customer list for cancel all
	 */
	List<HDCustomerDetailVO> getCustomerListForCancelAll();

	/**
	 * Gets the all outage events.
	 * 
	 * @param customers
	 *            the customers
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param duration
	 *            the duration
	 * @return the all outage events
	 */
	List<OutageDetailVO> getAllOutageEvents(String customers, String startDate,
			String endDate, int duration);

	/**
	 * Gets the false outage events.
	 * 
	 * @param customerList
	 *            the customer list
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param duration
	 *            the duration
	 * @return the false outage events
	 */
	List<OutageDetailVO> getFalseOutageEvents(String customerList,
			String startDate, String endDate, int duration);

	/**
	 * Validate outage.
	 * 
	 * @param customerList
	 *            the customer list
	 * @param startTimeFrame
	 *            the start time frame
	 * @param endTimeFrame
	 *            the end time frame
	 * @param duration
	 *            the duration
	 * @param ticketId
	 *            the ticket id
	 * @return true, if successful
	 */
	boolean validateOutage(String customerList, String startTimeFrame,
			String endTimeFrame, int duration, String ticketId);

	/**
	 * Gets the next action overdue.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the next action overdue
	 */
	boolean getNextActionOverdue(String ticketId);

	/**
	 * Gets the effort status.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the effort status
	 */
	String getEffortStatus(String ticketId);

	/**
	 * Gets the ticket priority.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @param impact
	 *            the impact
	 * @param urgency
	 *            the urgency
	 * @return the ticket priority
	 */
	int getTicketPriority(Long appendixId, Integer impact, Integer urgency);

	/**
	 * Update efforts.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param effortValue
	 *            the effort value
	 * @param ppsFlag
	 *            the pps flag
	 * @param userId
	 *            the user id
	 * @param isBillable
	 *            the is billable
	 * @return the string
	 */
	String updateEfforts(String ticketId, String effortValue, Boolean ppsFlag,
			String userId, Boolean isBillable);

	/**
	 * Save effortsfor hd.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param effortStartDate
	 *            the effort start date
	 * @param ppsFlag
	 *            the pps flag
	 * @param userId
	 *            the user id
	 * @param isBillable
	 *            the is billable
	 */
	public void saveEffortsforHD(String ticketId, String effortStartDate,
			Boolean ppsFlag, String userId, Boolean isBillable);

	/**
	 * Checks if is ticket under review by other.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return true, if is ticket under review by other
	 */
	boolean isTicketUnderReviewByOther(String ticketId);

	/**
	 * Checks if is ticket cancelled.
	 * 
	 * @param ackCubeType
	 *            the ack cube type
	 * @param ticketId
	 *            the ticket id
	 * @return true, if is ticket cancelled
	 */
	boolean isTicketCancelled(String ackCubeType, String ticketId);

	/**
	 * Checks if is ticket acknowledged.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return true, if is ticket acknowledged
	 */
	boolean isTicketAcknowledged(String ticketId);

	/**
	 * Sets the ticket under review.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param userId
	 *            the user id
	 * 
	 * @param hdSessionTrackerId
	 *            the hd session tracker id
	 */
	void setTicketUnderReview(String ticketId, String userId,
			String hdSessionTrackerId);

	/**
	 * Clear review status.
	 * 
	 * @param ticketId
	 *            the ticket id
	 */
	void clearReviewStatus(String ticketId);

	/**
	 * Gets the incident states.
	 * 
	 * @param all
	 *            Pull all incident states or just those displayed on the form
	 * @param actual
	 *            Also include this specific state. If null ignored
	 * 
	 * @return the incident states
	 */
	public List<IncidentStateVO> getIncidentStates(boolean all, String actual);

	/**
	 * Gets the incident resolutions.
	 * 
	 * @return the incident resolutions
	 */
	public List<ResolutionsVO> getResolutions();

	String lockbyuser(String ticketId);

	String getSiteNumber(String SiteID);

	public void updateTicketToclosed(String ticketId, String status,
			Boolean isOverDue);

	public List<HDPendingAckTableVO> getMonitoringDateData(String siteNum);

	public List<HDMonitoringTimeVO> getDateTimeListForMonitoringDAO(
			Long ticketId);

	public List<IncidentStateVO> getEmailAddress(Long appendixId,
			String chkstatus);

	String getClientEmailAddress(Long appendixId);

	void saveEventCorrelator(EventCorrelatorVO dto);

	int getEventCorrelatorExistence(EventCorrelatorVO dto);

	public List<EventCorrelatorVO> getEventCorrelator(String type);

	public void deleteEventCorrelator(String id);

}
