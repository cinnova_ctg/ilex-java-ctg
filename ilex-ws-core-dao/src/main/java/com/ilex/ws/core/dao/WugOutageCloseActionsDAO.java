package com.ilex.ws.core.dao;

import com.ilex.ws.core.bean.WugOutInProcessVO;

public interface WugOutageCloseActionsDAO {
	WugOutInProcessVO getDataFromInProcess(String nActiveMonitorStateChangeID,
			String wugInstance);

	int getEventTestCount(String nActiveMonitorStateChangeID, String wugInstance);

	void insertWugOutClosedRecord(WugOutInProcessVO wugOutInProcessVO);

	void insertWugOutClosedHistoryRecord(String nActiveMonitorStateChangeID,
			String wugInstance);

	void deleteWugOutagesInProcessHistory(String nActiveMonitorStateChangeID,
			String wugInstance);

	void deleteWugOutagesInProcess(String nActiveMonitorStateChangeID,
			String wugInstance);
}
