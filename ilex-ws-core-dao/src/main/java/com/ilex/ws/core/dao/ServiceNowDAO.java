package com.ilex.ws.core.dao;

import java.util.List;
import java.util.Map;

import com.ilex.ws.core.bean.HDTicketVO;
import com.ilex.ws.core.bean.IncidentVO;
import com.ilex.ws.core.bean.TicketStateVO;
import com.ilex.ws.core.dto.IncidentDTO;
import com.ilex.ws.servicenow.dto.ProactiveIncidentResponseDTO;
import com.ilex.ws.servicenow.dto.ProactiveIncidentTxrResponseDTO;
import com.ilex.ws.servicenow.dto.UpdateIncidentResponseDTO;
import com.ilex.ws.servicenow.dto.UpdateIncidentTxrResponseDTO;

/**
 * The Interface ServiceNowDAO.
 */
public interface ServiceNowDAO {

    // Logging Constants
    public static int ERROR_LEVEL = 0;
    public static int WARNING_LEVEL = 1;
    public static int INFORMATION_LEVEL = 2;

    public static int OUTBOUND = 0;
    public static int INBOUND = 1;

    /**
     * Insert proactive incident to ilex.
     *
     * @param proactiveIncidentVO the proactive incident vo
     * @param userId the user id
     * @return the map
     */
    Map<String, Long> insertProactiveIncidentToIlex(IncidentDTO proactiveIncidentDTO, int userId);

    /**
     * Update proactive successful response.
     *
     * @param responseDTO the response dto
     * @param userId the user id
     * @param createIdentifier the create identifier
     */
    void updateProactiveSuccessfulResponse(
            ProactiveIncidentResponseDTO responseDTO, int userId,
            Long createIdentifier);

    /**
     * Gets the incidents list.
     *
     * @param integrationType the integration type
     * @param msaId the msa id
     * @return the incidents list
     */
    List<IncidentVO> getIncidentsList(String integrationType, String msaId);

    /**
     * Gets the ticket states.
     *
     * @return the ticket states
     */
    List<TicketStateVO> getTicketStates();

    /**
     * Update proactive incident unsuccess response.
     *
     * @param responseDTO the response dto
     * @param userId the user id
     * @param createIdentifier the create identifier
     */
    void updateProactiveUnSuccessResponse(
            ProactiveIncidentResponseDTO responseDTO, int userId,
            Long createIdentifier);

    /**
     * Gets the creates the incident error logs.
     *
     * @return the creates the incident error logs
     */
    List<IncidentVO> getCreateIncidentErrorLogs();

    /**
     * Validate site number for cfa.
     *
     * @param reactiveIncidentVO the reactive incident vo
     * @return the string
     */
    String validateSiteNumberForCFA(IncidentVO reactiveIncidentVO);

    /**
     * Insert reactive incident to ilex.
     *
     * @param reactiveIncidentVO the reactive incident vo
     * @param ilexTicketNumber the ilex ticket number
     * @return the long
     */
    Long insertReactiveIncidentToIlex(IncidentVO reactiveIncidentVO,
            String ilexTicketNumber);

    /**
     * Gets the incident detail.
     *
     * @param incidentId the incident id
     * @return the incident detail
     */
    IncidentVO getIncidentDetail(Long incidentId);

    /**
     * Gets the update incident error logs.
     *
     * @return the update incident error logs
     */
    List<IncidentVO> getUpdateIncidentErrorLogs();

    /**
     * Insert incident updatet record to ilex.
     *
     * @param incidentVO the incident vo
     * @param userId the user id
     * @return the map
     */
    Map<String, Long> insertIncidentUpdatetRecordToIlex(IncidentVO incidentVO,
            String userId);

    /**
     * Sets the incident update unsuccess response.
     *
     * @param responseDTO the response dto
     * @param userId the user id
     * @param updateIdentifier the update identifier
     */
    void setIncidentUpdateUnsuccessResponse(
            UpdateIncidentResponseDTO responseDTO, int userId,
            Long updateIdentifier);

    /**
     * Sets the incident update success response.
     *
     * @param responseDTO the response dto
     * @param userId the user id
     * @param updateIdentifier the update identifier
     */
    void setIncidentUpdateSuccessResponse(
            UpdateIncidentResponseDTO responseDTO, int userId,
            Long updateIdentifier);

    /**
     * Gets the incident updates.
     *
     * @param createIdentifier the create identifier
     * @return the incident updates
     */
    List<IncidentVO> getIncidentUpdates(Long createIdentifier);

    /**
     * Gets the incident create identifier.
     *
     * @param vTaskNumber the v task number
     * @param ticketNumber the ticket number
     * @return the incident create identifier
     */
    Long getIncidentCreateIdentifier(String vTaskNumber, String ticketNumber);

    /**
     * Check state marked resolved.
     *
     * @param createIdentifier the create identifier
     * @return true, if successful
     */
    boolean checkStateMarkedResolved(Long createIdentifier);

    String validateSiteNumberForTXR(IncidentVO reactiveIncidentVO);

    void updateProactiveUnSuccessTxrResponse(
            ProactiveIncidentTxrResponseDTO responseDTO, int userId,
            Long createIdentifier);

    void updateProactiveSuccessfulTxrResponse(
            ProactiveIncidentTxrResponseDTO responseDTO, int userId,
            Long createIdentifier);

    void setIncidentUpdateUnsuccessTxrResponse(
            UpdateIncidentTxrResponseDTO responseDTO, int userId,
            Long updateIdentifier);

    void setIncidentUpdateSuccessTxrResponse(
            UpdateIncidentTxrResponseDTO responseDTO, int userId,
            Long updateIdentifier);

    boolean checkIntegrationType(String appendixId);

    IncidentVO getHdTicketDetailForCreateIncident(String ticketId);

    String getLatestWorkNote(String ticketId);

    String getSiteNumber(String siteId);

    void setHdUpdateFlag(String ticketId);

    IncidentVO getHdTicketDetailForUpdateIncident(String ticketId);

    IncidentVO getIncidentUpdateDetail(String updateIdentifier);

    Long insertTicket(String ilexTicketNumber, String problemDescription,
            String appendixId);

    HDTicketVO getSnMessage(Long ticketId);

    void clearIntegratedTicketStatus(Long ticketId);

    HDTicketVO getTicketDetailsFromTicketNumber(String ticketNumber);

    String getTicketStateId(String ticketState);

    void updateTicketState(String newTicketStateId, String ticketId);

    void insertWorkNotesFromSnToIlex(String ticketId, String workNotes,
            String wnType, String userId);

    void insertHDTicket(IncidentVO reactiveIncidentVO, Long ticketIdFromLmTicket);

    void logIntegration(int direction, int level, String msg, String data);
}
