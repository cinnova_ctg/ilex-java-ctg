package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.InvoiceJobDetailVO;
import com.ilex.ws.core.bean.JobInformationVO;
import com.ilex.ws.core.dto.LabelValue;
import com.mind.bean.prm.ViewInstallationNotesBean;
import com.mind.bean.pvs.PartnerDetailBean;
import com.mind.bean.pvs.Partner_SearchBean;
import com.mind.dao.PRM.CustomerInfoBean;

/**
 * The Interface DashportJobActionDAO.
 */
public interface DashportJobActionDAO {

	/**
	 * Gets the invoice activity resource list.
	 * 
	 * @param type
	 *            the type
	 * @param jobId
	 *            the job id
	 * @return the invoice activity resource list
	 */
	List<InvoiceJobDetailVO> getInvoiceActResourceList(String type, String jobId);

	/**
	 * Sets the job ticket information.
	 * 
	 * @param invoiceJobDetailVO
	 *            the invoice job detail vo
	 * @param jobId
	 *            the job id
	 */
	void setJobTicketInfo(InvoiceJobDetailVO invoiceJobDetailVO, String jobId);

	/**
	 * Gets the scheduled detail.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the scheduled detail
	 */
	List<InvoiceJobDetailVO> getScheduledDetail(String jobId);

	/**
	 * Add installation notes.
	 * 
	 * @param lm_in_js_id
	 *            the lm_in_js_id
	 * @param lm_in_notes
	 *            the lm_in_notes
	 * @param lm_user
	 *            the lm_user
	 * @param lm_date
	 *            the lm_date
	 * @param preScheduleId
	 *            the pre schedule id
	 * @param newScheduleId
	 *            the new schedule id
	 * @param scheduleDateType
	 *            the schedule date type
	 * @param typeId
	 *            the type id
	 * @return the int
	 */
	int addinstallnotes(String lm_in_js_id, String lm_in_notes, String lm_user,
			String lm_date, String preScheduleId, String newScheduleId,
			String scheduleDateType, String typeId);

	/**
	 * Update job category priority.
	 * 
	 * @param jobInformationVO
	 *            the job information vo
	 * @param userId
	 *            the user id
	 */
	void updateJobCategoryPriority(JobInformationVO jobInformationVO,
			String userId);

	/**
	 * Gets the job category priority.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the job category priority
	 */
	JobInformationVO getJobCategoryPriority(String jobId);

	/**
	 * Gets the job changed time.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the job changed time
	 */
	String getJobChangedTime(String jobId);

	/**
	 * Gets the appendix categories.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the appendix categories
	 */
	List<String> getAppendixCategories(String jobId);

	/**
	 * Gets the appendix priorities.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the appendix priorities
	 */
	List<String> getAppendixPriorities(String jobId);

	/**
	 * Gets the jO list.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @return the jO list
	 */
	List<LabelValue> getJOList(String appendixId);

	/**
	 * Update job owner.
	 * 
	 * @param jobId
	 *            the job id
	 * @param newJobOwnerId
	 *            the new job owner id
	 */
	void updateJobOwner(int jobId, int newJobOwnerId);

	/**
	 * Gets the customer field data.
	 * 
	 * @param jobId
	 *            the job id
	 * @param appendixId
	 *            the appendix id
	 * @return the customer field data
	 */
	public List<CustomerInfoBean> getCustomerFieldData(String jobId,
			String appendixId);

	/**
	 * Check for snapon.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @return true, if successful
	 */
	boolean checkForSnapon(String appendixId);

	/**
	 * Update customer field values.
	 * 
	 * @param jobId
	 *            the job id
	 * @param appendixCustomerIds
	 *            the appendix customer ids
	 * @param custmereValues
	 *            the custmere values
	 * @param loginuserid
	 *            the loginuserid
	 * @return the int
	 */
	int updateCustomerFiledInfo(String jobId, String appendixCustomerIds,
			String custmereValues, String loginuserid);

	/**
	 * Gets the installation notes list.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the installation notes list
	 */
	List<ViewInstallationNotesBean> getInstallationNotesList(String jobId);

	/**
	 * Gets the partners search list.
	 * 
	 * @param partner_SearchBean
	 *            the partner_ search bean
	 * @param sortqueryclause
	 *            the sortqueryclause
	 * @param zipcode
	 *            the zipcode
	 * @return the partners search list
	 */
	List<Partner_SearchBean> getpartnersSearchList(
			Partner_SearchBean partner_SearchBean, String sortqueryclause,
			String zipcode);

	/**
	 * Gets the partner details.
	 * 
	 * @param partnerDetailBean
	 *            the partner detail bean
	 * @return the partner details
	 */
	String getPartnerDetails(PartnerDetailBean partnerDetailBean, String url);
}
