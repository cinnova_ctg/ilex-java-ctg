package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.InvCountryListVO;
import com.ilex.ws.core.dao.InvMasterCountryDAO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.MessageKeyConstant;

@Repository
public class InvMasterCountryDAOImpl implements InvMasterCountryDAO {
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.InvMasterCountryDAO#getCountryList()
	 */
	@Override
	public List<InvCountryListVO> getCountryList() {
		StringBuilder sqlQuery = new StringBuilder(
				"select * from in_master_country_list");

		List<InvCountryListVO> invCountryList = jdbcTemplateIlex.query(
				sqlQuery.toString(), new StateListMapper());
		if (invCountryList == null || invCountryList.size() == 0) {
			throw new IlexBusinessException(
					MessageKeyConstant.COUNTRY_LIST_NOT_FOUND);
		}

		return invCountryList;

	}

	/**
	 * The Class ClientDetailMapper.
	 */
	private static final class StateListMapper implements
			RowMapper<InvCountryListVO> {

		@Override
		public InvCountryListVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvCountryListVO invCountry = new InvCountryListVO();
			invCountry.setCountryName(rs.getString("in_mc_long_name"));
			invCountry.setCountryShortName(rs
					.getString("in_mc_iso2_abbreviation"));
			return invCountry;
		}
	}

}
