package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.InvAdditionalInfoVO;
import com.ilex.ws.core.bean.InvSetupVO;

public interface InvAdditionalInfoDAO {

	void insertAdditionalInfo(InvAdditionalInfoVO invAdditionalInfoVO);

	List<InvAdditionalInfoVO> getAdditionalInfo(InvSetupVO invSetupVO);

	void updateAdditionalInfo(InvAdditionalInfoVO invAdditionalInfoVO);

	void deleteAdditionalInfo(int invAdditionalInfoId);

	void deleteAdditionalInfo(InvSetupVO invSetupVO);

}
