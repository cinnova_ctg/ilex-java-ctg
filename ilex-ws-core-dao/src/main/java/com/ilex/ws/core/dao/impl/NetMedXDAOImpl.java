package com.ilex.ws.core.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.NetMedXConsolidatorRespVO;
import com.ilex.ws.core.bean.NetMedXIntegrationResponseVO;
import com.ilex.ws.core.bean.NetMedXIntegrationVO;
import com.ilex.ws.core.dao.NetMedXDAO;
import com.ilex.ws.core.exception.IlexSystemException;
import com.lowagie.text.DocumentException;
import com.mind.bean.docm.Document;
import com.mind.common.EnvironmentSelector;
import com.mind.common.dao.RestClient;
import com.mind.common.util.MySqlConnection;
import com.mind.docm.client.ClientOperator;
import com.mind.docm.exceptions.ControlledTypeException;
import com.mind.docm.exceptions.CouldNotAddToRepositoryException;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.CouldNotReplaceException;
import com.mind.docm.exceptions.DocMFormatException;
import com.mind.fw.core.dao.util.DBUtil;

/**
 * The Class NetMedXDAOImpl.
 */
@Repository
public class NetMedXDAOImpl implements NetMedXDAO {
	public static final Logger logger = Logger.getLogger(NetMedXDAOImpl.class);
	@Resource
	private JdbcTemplate jdbcTemplateIlex;
	@Resource
	private JdbcTemplate jdbcTemplateDocmSys;
	@Resource
	private JdbcTemplate jdbcTemplateDocMFileSys;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.MACWorXDAO#getLatitudeLongitude(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String[] getLatitudeLongitude(String address, String city,
			String state, String country, String zipcode) {
		String[] latLongDetails = RestClient
				.getLatitudeLongitude(address, city, state, country, zipcode,
						jdbcTemplateIlex.getDataSource());

		return latLongDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.MACWorXDAO#createJobForMegaPath(com.ilex.ws.core
	 * .bean.MacWorxIntergrationVO)
	 */
	@Override
	public NetMedXIntegrationResponseVO createJobForNetMedX(
			NetMedXIntegrationVO netMedXIntegrationVO) {
		Connection con = null;
		CallableStatement cstmt = null;
		NetMedXIntegrationResponseVO netMedXIntegrationResponseVO = new NetMedXIntegrationResponseVO();

		netMedXIntegrationResponseVO = netmedxUserAuth(
				netMedXIntegrationVO.getUserName(),
				netMedXIntegrationVO.getUserPass(),
				netMedXIntegrationVO.getAppendixid());
		if (netMedXIntegrationResponseVO.getApiAccess() != 0) {

			try {

				con = jdbcTemplateIlex.getDataSource().getConnection();
				cstmt = con
						.prepareCall("{call  dbo.lm_automated_ticket_manage_NetMedX(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
				cstmt.setInt(1, netMedXIntegrationVO.getAppendixid());
				cstmt.setString(2, netMedXIntegrationVO.getSite_number());
				cstmt.setString(3, netMedXIntegrationVO.getSite_phone());
				cstmt.setString(4, netMedXIntegrationVO.getSite_worklocation());
				cstmt.setString(5, netMedXIntegrationVO.getPo_number());
				cstmt.setString(6, netMedXIntegrationVO.getSite_address());
				cstmt.setString(7, netMedXIntegrationVO.getSite_city());
				cstmt.setString(8, netMedXIntegrationVO.getSite_state());
				cstmt.setString(9, netMedXIntegrationVO.getSite_zipcode());
				cstmt.setString(10, netMedXIntegrationVO.getSite_name());
				cstmt.setString(11, netMedXIntegrationVO.getEmail());
				cstmt.setString(12, netMedXIntegrationVO.getContact_person());
				cstmt.setString(13, netMedXIntegrationVO.getContact_phone());
				cstmt.setString(14, netMedXIntegrationVO.getPrimary_Name());
				cstmt.setString(15, netMedXIntegrationVO.getPri_site_phone());
				cstmt.setString(16, netMedXIntegrationVO.getSecondary_Name());
				cstmt.setString(17, netMedXIntegrationVO.getSecondary_phone());
				cstmt.setString(18, netMedXIntegrationVO.getRequested_date());
				cstmt.setString(19, netMedXIntegrationVO.getCriticality());
				cstmt.setString(20,
						netMedXIntegrationVO.getArrivalwindowstartdate());
				cstmt.setString(21,
						netMedXIntegrationVO.getArrivalwindowenddate());
				cstmt.setString(22, netMedXIntegrationVO.getArrivaldate());
				cstmt.setString(23,
						netMedXIntegrationVO.getProblem_description());
				cstmt.setString(24,
						netMedXIntegrationVO.getSpecial_instruction());
				cstmt.setString(25, netMedXIntegrationVO.getStand_by());
				cstmt.setString(26, netMedXIntegrationVO.getOther_email_info());
				cstmt.setString(27,
						netMedXIntegrationVO.getCust_specific_fields());
				cstmt.setString(28, netMedXIntegrationVO.getSilver_bucket());
				cstmt.registerOutParameter(29, java.sql.Types.VARCHAR);
				cstmt.registerOutParameter(30, java.sql.Types.VARCHAR);
				cstmt.registerOutParameter(31, java.sql.Types.NUMERIC);
				cstmt.execute();
				netMedXIntegrationResponseVO.setTicketNumber(cstmt
						.getString(29));
				netMedXIntegrationResponseVO.setJobId(Integer.parseInt(cstmt
						.getString(30)));
				netMedXIntegrationResponseVO.setPoId(cstmt.getInt(31));
				netMedXIntegrationResponseVO.setAttachmentStatus(0); // showing
																		// that
				// uptill
				// now no
				// attachments
				// has been
				// entertained.
				// macWorxIntergrationResponseVO.setTicketNumber();
				// macWorxIntergrationResponseVO.setJobId(cstmt.getInt(29));
				// macWorxIntergrationResponseVO.setPoId(cstmt.getInt(30));
				// macWorxIntergrationResponseVO.setAttachmentStatus(cstmt.getInt(31));

			} catch (SQLException e) {
				logger.error(e.getMessage());
				throw new IlexSystemException(e.getMessage(), e);
			} finally {
				DBUtil.close(con);
				DBUtil.close(cstmt);
			}
		}

		return netMedXIntegrationResponseVO;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.NetMedXDAO#getNetMedXJobIntergration(java.lang.String
	 * , java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<NetMedXConsolidatorRespVO> getNetMedXJobIntergration(
			String projectId, String whereClause, String orderBy, String limit) {
		Connection mySqlConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String qry = "";

		List<NetMedXConsolidatorRespVO> netMedXConsolidatorRespList = new ArrayList<NetMedXConsolidatorRespVO>();

		try {

			String url = EnvironmentSelector
					.getBundleString("appendix.detail.changestatus.mysql.url");
			mySqlConn = MySqlConnection.getMySqlConnection(url);

			qry = "SELECT * FROM il_job_integration WHERE il_ji_ci_lx_pr_id = '"
					+ projectId
					+ "' AND il_ji_lm_si_number!= '' "
					+ " AND il_ji_lm_js_title NOT LIKE ('%-HD') "
					+ whereClause
					+ orderBy + limit;

			// qry = "SELECT * FROM il_job_integration LIMIT 0 , 10";

			pstmt = mySqlConn.prepareStatement(qry);
			rs = pstmt.executeQuery();
			NetMedXConsolidatorRespVO consolidatorRespVO = null;

			while (rs.next()) {
				consolidatorRespVO = new NetMedXConsolidatorRespVO();
				consolidatorRespVO.setJobId(rs.getString(1));
				consolidatorRespVO.setProjectId(rs.getString(2));
				consolidatorRespVO.setJobTitle(rs.getString(3));
				consolidatorRespVO.setIncludeOutOfScope(rs.getString(4));
				consolidatorRespVO.setStatus(rs.getString(5));
				consolidatorRespVO.setRequestedDate(rs.getString(6));
				consolidatorRespVO.setScheduledDate(rs.getString(7));
				consolidatorRespVO.setOnsiteDate(rs.getString(8));
				consolidatorRespVO.setOffsiteDate(rs.getString(9));
				consolidatorRespVO.setInvoiceDate(rs.getString(10));
				consolidatorRespVO.setCompletionDate(rs.getString(11));
				consolidatorRespVO.setClosedDate(rs.getString(12));
				consolidatorRespVO.setInvoicePrice(rs.getString(13));
				consolidatorRespVO.setInvoiceNumber(rs.getString(14));
				consolidatorRespVO.setCustomerRef(rs.getString(15));
				consolidatorRespVO.setSiteNumber(rs.getString(16));
				consolidatorRespVO.setSiteAddress(rs.getString(17));
				consolidatorRespVO.setSiteCity(rs.getString(18));
				consolidatorRespVO.setSiteCountry(rs.getString(19));
				consolidatorRespVO.setSiteZip(rs.getString(20));
				consolidatorRespVO.setLatitudeRadiud(rs.getString(21));
				consolidatorRespVO.setLongitudeRadius(rs.getString(22));
				consolidatorRespVO.setCreatedBy(rs.getString(23));
				consolidatorRespVO.setUpdatedBy(rs.getString(24));
				consolidatorRespVO.setJobUpdatedBy(rs.getString(25));
				consolidatorRespVO.setSiteState(rs.getString(26));
				consolidatorRespVO.setWorkLocation(rs.getString(27));
				consolidatorRespVO.setMsp(rs.getString(28));
				consolidatorRespVO.setHelpDesk(rs.getString(29));
				consolidatorRespVO.setIssueOwner(rs.getString(30));
				consolidatorRespVO.setRootCause(rs.getString(31));
				consolidatorRespVO.setCorrectiveAciton(rs.getString(32));
				consolidatorRespVO.setPrePOC(rs.getString(33));
				consolidatorRespVO.setPhoneNumber(rs.getString(34));
				netMedXConsolidatorRespList.add(consolidatorRespVO);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return netMedXConsolidatorRespList;
	}

	/**
	 * Convert stringto sq ldate.
	 * 
	 * @param dateInString
	 *            the date in string
	 * @return the java.sql. date
	 */
	private java.sql.Date convertStringtoSQLdate(String dateInString) {
		SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
		Date date;

		try {

			date = formatter.parse(dateInString);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}

		return new java.sql.Date(date.getTime());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.NetMedXDAO#getJobOwnerName(int)
	 */
	@Override
	public String getJobOwnerName(int jobId) {
		String sqlQuery = "select lo_pc_first_name,lo_pc_last_name from lo_poc where lo_pc_id=";
		sqlQuery += "(select lm_js_created_by from lm_job where lm_js_id=?)";
		Object[] params = new Object[] { jobId };
		String jobOwnerName;
		try {
			jobOwnerName = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
					params, new JobOwnerMapper());
		} catch (EmptyResultDataAccessException ex) {
			jobOwnerName = "";
		}

		return jobOwnerName;

	}

	/**
	 * The Class JobOwnerMapper.
	 */
	private static final class JobOwnerMapper implements RowMapper<String> {

		@Override
		public String mapRow(ResultSet rs, int rowNum) throws SQLException {
			StringBuilder firstName = new StringBuilder(
					rs.getString("lo_pc_first_name"));
			StringBuilder lastName = new StringBuilder(
					rs.getString("lo_pc_last_name"));

			return firstName.append(" ").append(lastName).toString();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.NetMedXDAO#uploadFile(com.mind.bean.docm.Document)
	 */
	@Override
	public int uploadFile(Document doc) {
		int docId;
		try {
			docId = ClientOperator.explicitAddDocument(doc,
					jdbcTemplateDocmSys.getDataSource(),
					jdbcTemplateDocMFileSys.getDataSource());
		} catch (ControlledTypeException e) {
			logger.error(e);
			return 0;
		} catch (CouldNotAddToRepositoryException e) {
			logger.error(e);
			return 0;
		} catch (CouldNotReplaceException e) {
			logger.error(e);
			return 0;
		} catch (CouldNotCheckDocumentException e) {
			logger.error(e);
			return 0;
		} catch (DocMFormatException e) {
			logger.error(e);
			return 0;
		} catch (DocumentException e) {
			logger.error(e);
			return 0;
		}

		return docId;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.NetMedXDAO#netmedxUserAuth(com.ilex.ws.core.bean
	 * .NetMedXIntegrationVO)
	 */
	@Override
	public NetMedXIntegrationResponseVO netmedxUserAuth(String userName,
			String password, int prId) {

		NetMedXIntegrationResponseVO netMedXIntegrationResponseVO = new NetMedXIntegrationResponseVO();
		int accesslvl = 0;
		String netmedxAccess = "select lm_pr_auth_access from lm_pr_authentication where lm_pr_auth_user_name = '"
				+ userName
				+ "' and lm_pr_auth_password = '"
				+ password
				+ "' and lm_pr_auth_project_id = " + prId;

		try {
			accesslvl = jdbcTemplateIlex.queryForInt(netmedxAccess);
			netMedXIntegrationResponseVO.setApiAccess(accesslvl);
			if (accesslvl != 1) {
				netMedXIntegrationResponseVO.setTicketNumber("Invalid Access");
				netMedXIntegrationResponseVO.setApiAccess(accesslvl);

			}
		} catch (Exception e) {
			netMedXIntegrationResponseVO
					.setTicketNumber("Exception Occured. Invalid Credentials");
			netMedXIntegrationResponseVO.setApiAccess(accesslvl);

		}

		return netMedXIntegrationResponseVO;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.NetMedXDAO#insertDocEntryToPoDocumentNetMedX(int,
	 * java.lang.String, int)
	 */
	@Override
	public void insertDocEntryToPoDocumentNetMedX(final int docId,
			final String userName, final int jobId) {
		final StringBuilder sql = new StringBuilder(
				"insert into podb_netmedx_document ( lm_js_id, podb_doc_id,doc_with_po,doc_with_wo,podb_doc_created_by,");
		sql.append(" podb_doc_created_date,podb_doc_updated_by,podb_doc_updated_date)");
		sql.append("   values (?,?,?,?,?,getDate(),?,getDate())");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql
						.toString());

				ps.setInt(1, jobId);
				ps.setInt(2, docId);
				ps.setString(3, "1"); // TO Include with PO
				ps.setString(4, "1"); // TO Include with WO
				ps.setString(5, userName);
				ps.setString(6, userName);
				return ps;
			}
		};
		jdbcTemplateIlex.update(preparedStatementCreator);

	}

	@Override
	public String getJobCountDAO(String projectId, String whereClause,
			String orderBy) {
		// SELECT COUNT(*) FROM TABLE_NAME

		Connection mySqlConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String url = EnvironmentSelector
				.getBundleString("appendix.detail.changestatus.mysql.url");
		int eventTest = 0;
		try {
			mySqlConn = MySqlConnection.getMySqlConnection(url);

			StringBuilder sqlQuery = new StringBuilder(
					" select count(*) from il_job_integration WHERE il_ji_ci_lx_pr_id = '"
							+ projectId
							+ "'  AND il_ji_lm_si_number!= ''  AND il_ji_lm_js_title NOT LIKE ('%-HD') "
							+ whereClause + orderBy);

			pstmt = mySqlConn.prepareStatement(sqlQuery.toString());
			rs = pstmt.executeQuery();
			if (rs.next()) {

				eventTest = rs.getInt(1);
			}

			// eventTest = mySqlConn.queryForInt();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				rs.close();
				pstmt.close();
				mySqlConn.close();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

		return Integer.toString(eventTest);
	}

}
