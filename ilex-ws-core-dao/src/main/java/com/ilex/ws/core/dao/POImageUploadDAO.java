package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.POImageDetailVO;
import com.ilex.ws.core.bean.POImageUploadVO;

/**
 * The Interface POImageUploadDAO.
 */
public interface POImageUploadDAO {

	/**
	 * Gets the partner id.
	 * 
	 * @param authString
	 *            the authString
	 * @return the partner id
	 */
	public String getPartnerId(final String authString);

	/**
	 * Gets the deliverables images details (name and number of images for the
	 * poId).
	 * 
	 * @param poId
	 *            the po id
	 * @param partnerId
	 *            the partner id
	 * @return the pO image detail
	 */
	public List<POImageDetailVO> getPOImageDetail(final String poId,
			final String partnerId);

	/**
	 * Upload deliverable images for the PO.
	 * 
	 * @param fileUploadVO
	 *            the file upload vo
	 * @return true, if successful
	 */
	public boolean poImageUpload(final POImageUploadVO fileUploadVO);

	/**
	 * Gets the status of PO deliverable for the imageNumber
	 * 
	 * @param imageNumber
	 *            the imageNumber
	 * @return the status
	 */
	public int getPODeliverableStatus(final String imageNumber);

}
