package com.ilex.ws.core.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;

import org.springframework.asm.Type;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.InvBillToAddressVO;
import com.ilex.ws.core.bean.InvSetupVO;
import com.ilex.ws.core.dao.InvSetupDAO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.MessageKeyConstant;

/**
 * The Class InvSetupDAOImpl.
 */
@Repository
public class InvSetupDAOImpl implements InvSetupDAO {

	/** The jdbc template ilex. */
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.InvSetupDAO#insertInvSetupDetails(com.ilex.ws.core
	 * .bean.InvSetupVO)
	 */
	@Override
	public int insertInvSetupDetails(final InvSetupVO invSetupVO) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		final StringBuilder sqlQuery = new StringBuilder(
				"insert into in_invoice_setup (in_su_bt_id,in_su_mm_id,in_su_pr_id,");
		sqlQuery.append("in_su_page_layout,in_su_contingent_address,in_su_type,");
		sqlQuery.append("in_su_type_grouping,in_su_include_outstanding_balance,");
		sqlQuery.append("in_su_customer_reference_source,in_su_customer_reference_name,");
		sqlQuery.append("in_su_customer_reference_value,in_su_customer_reference_poc,in_su_timeframe,");
		sqlQuery.append("in_su_line_item_format,in_su_summary_sheet,");
		sqlQuery.append("in_su_approval_level,in_su_collect_tax,in_su_er_bill_to,");
		sqlQuery.append("in_su_er_customer_reference,in_su_reference_date");
		sqlQuery.append(") values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getDate())");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						sqlQuery.toString(), new String[] { "id" });
				ps.setInt(1, invSetupVO.getBillToAddressId());
				ps.setInt(2, invSetupVO.getMmId());
				ps.setInt(3, invSetupVO.getAppendixId());
				ps.setString(4, invSetupVO.getPageLayout());
				ps.setString(5, invSetupVO.getContingentAddress());
				ps.setString(6, invSetupVO.getInvoiceSetupType());
				ps.setString(7, invSetupVO.getInvoiceSetupGroupingType());
				ps.setBoolean(8, invSetupVO.isOutstandingBalanceExist());
				ps.setString(9, invSetupVO.getCustomerReferenceSource());
				ps.setString(10, invSetupVO.getCustomerReferenceName());
				ps.setString(11, invSetupVO.getCustomerReferenceValue());
				ps.setString(12, invSetupVO.getCustomerReferencePoc());
				ps.setString(13, invSetupVO.getTimeFrame());
				ps.setString(14, invSetupVO.getLineItemFormat());
				ps.setString(15, invSetupVO.getSummarySheet());
				ps.setString(16, invSetupVO.getApprovalLevel());
				ps.setFloat(17, invSetupVO.getCollectedTax());
				if (invSetupVO.isBillToReferenceExist() != null) {
					ps.setBoolean(18, invSetupVO.isBillToReferenceExist());
				} else {
					ps.setNull(18, Type.BOOLEAN);
				}
				if (invSetupVO.isCustomerReferenceExist() != null) {
					ps.setBoolean(19, invSetupVO.isCustomerReferenceExist());
				} else {
					ps.setNull(19, Type.BOOLEAN);
				}

				return ps;
			}
		};

		jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);

		int insertedSetupId = keyHolder.getKey().intValue();
		return insertedSetupId;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.InvSetupDAO#getInvSetupDetails(int)
	 */
	@Override
	public InvSetupVO getInvSetupDetails(final int invSetupId) {

		StringBuilder sqlQuery = new StringBuilder(
				"select * from in_invoice_setup");
		sqlQuery.append(" where in_su_id =?");

		Object[] params = new Object[] { invSetupId };
		InvSetupVO invSetupVO;
		try {
			invSetupVO = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
					params, new InvSetupMapper());
		} catch (EmptyResultDataAccessException ex) {
			throw new IlexBusinessException(
					MessageKeyConstant.INVOICE_DETAIL_NOT_FOUND);
		}

		return invSetupVO;
	}

	@Override
	public InvSetupVO getInvSetupDetails(
			final InvBillToAddressVO invBillToAddressVO) {

		StringBuilder sqlQuery = new StringBuilder(
				"select * from in_invoice_setup");
		sqlQuery.append(" where in_su_bt_id =?");

		Object[] params = new Object[] { invBillToAddressVO
				.getInvBillToAddressId() };
		InvSetupVO invSetupVO;
		try {
			invSetupVO = jdbcTemplateIlex.queryForObject(sqlQuery.toString(),
					params, new InvSetupMapper());
		} catch (EmptyResultDataAccessException ex) {
			throw new IlexBusinessException(
					MessageKeyConstant.INVOICE_DETAIL_NOT_FOUND);
		}

		return invSetupVO;
	}

	/**
	 * The Class InvSetupMapper.
	 */
	private static final class InvSetupMapper implements RowMapper<InvSetupVO> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		@Override
		public InvSetupVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			InvSetupVO invSetupVO = new InvSetupVO();
			invSetupVO.setInvoiceSetupId(rs.getInt("in_su_id"));
			invSetupVO.setBillToAddressId(rs.getInt("in_su_bt_id"));
			invSetupVO.setMmId(rs.getInt("in_su_mm_id"));
			invSetupVO.setAppendixId(rs.getInt("in_su_pr_id"));
			invSetupVO.setPageLayout(rs.getString("in_su_page_layout"));
			invSetupVO.setContingentAddress(rs
					.getString("in_su_contingent_address"));
			invSetupVO.setInvoiceSetupType(rs.getString("in_su_type"));
			invSetupVO.setInvoiceSetupGroupingType(rs
					.getString("in_su_type_grouping"));
			invSetupVO.setOutstandingBalanceExist(rs
					.getBoolean("in_su_include_outstanding_balance"));
			invSetupVO.setCustomerReferenceSource(rs
					.getString("in_su_customer_reference_source"));
			invSetupVO.setCustomerReferenceName(rs
					.getString("in_su_customer_reference_name"));
			invSetupVO.setCustomerReferenceValue(rs
					.getString("in_su_customer_reference_value"));
			invSetupVO.setCustomerReferencePoc(rs
					.getString("in_su_customer_reference_poc"));
			invSetupVO.setTimeFrame(rs.getString("in_su_timeframe"));
			invSetupVO
					.setLineItemFormat(rs.getString("in_su_line_item_format"));
			invSetupVO.setSummarySheet(rs.getString("in_su_summary_sheet"));
			invSetupVO.setApprovalLevel(rs.getString("in_su_approval_level"));
			invSetupVO.setCollectedTax(rs.getFloat("in_su_collect_tax"));
			invSetupVO.setBillToReferenceExist(rs
					.getBoolean("in_su_er_bill_to"));
			invSetupVO.setCustomerReferenceExist(rs
					.getBoolean("in_su_er_customer_reference"));
			invSetupVO.setInvoiceSetupReferenceDate(rs
					.getString("in_su_reference_date"));
			return invSetupVO;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.InvSetupDAO#deleteInvSetupDetails(int)
	 */
	@Override
	public void deleteInvSetupDetails(final int invBillToAddressId) {
		StringBuilder deleteAdditionalInfoTable = new StringBuilder(
				"delete from in_invoice_additional_information_setup where ");
		deleteAdditionalInfoTable
				.append("in_as_su_id in (select in_su_id from in_invoice_setup where in_su_bt_id = ? )");

		StringBuilder deleteInvoiceTable = new StringBuilder(
				"delete from in_invoice_setup where in_su_bt_id = ? ");

		StringBuilder deleteInvoiceBillToAddressTable = new StringBuilder(
				"delete from in_bill_to_address where in_bt_id = ? ");

		Object[] params = new Object[] { invBillToAddressId };
		jdbcTemplateIlex.update(deleteAdditionalInfoTable.toString(), params);
		jdbcTemplateIlex.update(deleteInvoiceTable.toString(), params);
		jdbcTemplateIlex.update(deleteInvoiceBillToAddressTable.toString(),
				params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.InvSetupDAO#updateInvSetupProfile(com.ilex.ws.core
	 * .bean.InvSetupVO)
	 */
	@Override
	public void updateInvSetupProfile(final InvSetupVO invSetupVO) {
		StringBuilder sqlQuery = new StringBuilder(
				"update in_invoice_setup set in_su_page_layout=?,in_su_contingent_address=?,in_su_type=?,");
		sqlQuery.append("in_su_type_grouping=?,in_su_timeframe=?,in_su_include_outstanding_balance=?,in_su_collect_tax=?");
		sqlQuery.append(" where in_su_id=?");
		Object[] params = new Object[] { invSetupVO.getPageLayout(),
				invSetupVO.getContingentAddress(),
				invSetupVO.getInvoiceSetupType(),
				invSetupVO.getInvoiceSetupGroupingType(),
				invSetupVO.getTimeFrame(),
				invSetupVO.isOutstandingBalanceExist() ? 1 : 0,
				invSetupVO.getCollectedTax(), invSetupVO.getInvoiceSetupId() };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.InvSetupDAO#updateClientInfoDetails(com.ilex.ws.
	 * core.bean.InvSetupVO)
	 */
	@Override
	public void updateClientInfoDetails(final InvSetupVO invSetupVO) {
		StringBuilder sqlQuery = new StringBuilder(
				"update in_invoice_setup set in_su_customer_reference_source=?,");
		sqlQuery.append("in_su_customer_reference_name=?,in_su_customer_reference_value=?,in_su_customer_reference_poc=?");
		sqlQuery.append(" where in_su_id=?");
		Object[] params = new Object[] {
				invSetupVO.getCustomerReferenceSource(),
				invSetupVO.getCustomerReferenceName(),
				invSetupVO.getCustomerReferenceValue(),
				invSetupVO.getCustomerReferencePoc(),
				invSetupVO.getInvoiceSetupId() };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.InvSetupDAO#updateLineItemDetails(com.ilex.ws.core
	 * .bean.InvSetupVO)
	 */
	@Override
	public void updateLineItemDetails(final InvSetupVO invSetupVO) {
		StringBuilder sqlQuery = new StringBuilder(
				"update in_invoice_setup set in_su_line_item_format=?");
		sqlQuery.append(" where in_su_id=?");
		Object[] params = new Object[] { invSetupVO.getLineItemFormat(),
				invSetupVO.getInvoiceSetupId() };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.InvSetupDAO#updateSummarySheetDetails(com.ilex.ws
	 * .core.bean.InvSetupVO)
	 */
	@Override
	public void updateSummarySheetDetails(final InvSetupVO invSetupVO) {
		StringBuilder sqlQuery = new StringBuilder(
				"update in_invoice_setup set in_su_summary_sheet=?");
		sqlQuery.append(" where in_su_id=?");
		Object[] params = new Object[] { invSetupVO.getSummarySheet(),
				invSetupVO.getInvoiceSetupId() };
		jdbcTemplateIlex.update(sqlQuery.toString(), params);

	}

}
