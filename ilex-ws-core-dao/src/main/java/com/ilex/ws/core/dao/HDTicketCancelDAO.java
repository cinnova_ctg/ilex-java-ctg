package com.ilex.ws.core.dao;

import com.ilex.ws.core.bean.WugOutInProcessVO;

public interface HDTicketCancelDAO {

	void insertInWugNoAction(String ticketId, String userId);

	WugOutInProcessVO getDataFromInProcess(String ticketId);

	int getEventTestCount(String nActiveMonitorStateChangeID, String wugInstance);

	void insertWugOutClosedRecord(WugOutInProcessVO wugOutInProcessVO);

	void insertWugOutClosedHistoryRecord(String nActiveMonitorStateChangeID,
			String wugInstance);

	void deleteWugOutagesInProcessHistory(String nActiveMonitorStateChangeID,
			String wugInstance);

	void deleteWugOutagesInProcess(String nActiveMonitorStateChangeID,
			String wugInstance);

	void deleteFromHDTables(String ticketId);

}
