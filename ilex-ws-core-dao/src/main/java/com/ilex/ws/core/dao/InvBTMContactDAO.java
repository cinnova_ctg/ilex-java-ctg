package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.InvBTMClientVO;
import com.ilex.ws.core.bean.InvBTMContactVO;

public interface InvBTMContactDAO {

	/**
	 * Insert btm contact details.
	 * 
	 * @param invBTMContactVO
	 *            the inv btm contact vo
	 */
	void insertBTMContactDetails(InvBTMContactVO invBTMContactVO);

	/**
	 * Gets the bTM contact details.
	 * 
	 * @param argInvBTMClientVO
	 *            the arg inv btm client vo
	 * @return the bTM contact details
	 */
	List<InvBTMContactVO> getBTMContactDetails(InvBTMClientVO argInvBTMClientVO);

	/**
	 * Update btm contact details.
	 * 
	 * @param invBTMContactVO
	 *            the inv btm contact vo
	 */
	void updateBTMContactDetails(InvBTMContactVO invBTMContactVO);

}
