package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.InvStateListVO;
import com.ilex.ws.core.dao.InvMasterStateDAO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.MessageKeyConstant;

@Repository
public class InvMasterStateDAOImpl implements InvMasterStateDAO {

	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.core.dao.InvMasterStateDAO#getStateList()
	 */
	@Override
	public List<InvStateListVO> getStateList() {
		StringBuilder sqlQuery = new StringBuilder(
				"select * from in_master_state_list");

		List<InvStateListVO> invStateList = jdbcTemplateIlex.query(
				sqlQuery.toString(), new StateListMapper());
		if (invStateList == null || invStateList.size() == 0) {
			throw new IlexBusinessException(
					MessageKeyConstant.STATE_LIST_NOT_FOUND);
		}

		return invStateList;

	}

	/**
	 * The Class ClientDetailMapper.
	 */
	private static final class StateListMapper implements
			RowMapper<InvStateListVO> {

		@Override
		public InvStateListVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvStateListVO invStateListVO = new InvStateListVO();
			invStateListVO.setCountryShortName(rs
					.getString("in_mc_iso2_abbreviation"));
			invStateListVO.setStateShortName(rs
					.getString("in_ms_common_abbrev"));
			invStateListVO.setStateName(rs.getString("in_ma_long_name"));
			return invStateListVO;
		}
	}
}
