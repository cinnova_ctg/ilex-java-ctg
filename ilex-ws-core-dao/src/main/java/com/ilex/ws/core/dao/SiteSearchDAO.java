package com.ilex.ws.core.dao;

import java.util.List;

import com.mind.bean.pm.SiteBean;
import com.mind.bean.pm.SiteSearchBean;
import com.mind.bean.prm.JobSetUpDTO;
import com.mind.bean.prm.SiteManagementBean;
import com.mind.common.LabelValue;

// TODO: Auto-generated Javadoc
/**
 * The Interface SiteSearchDAO.
 */
public interface SiteSearchDAO {

	/**
	 * Gets the search site details.
	 * 
	 * @param msaId
	 *            the msa id
	 * @param siteName
	 *            the site name
	 * @param siteNo
	 *            the site no
	 * @param lo_ot_id
	 *            the lo_ot_id
	 * @param siteCity
	 *            the site city
	 * @param siteState
	 *            the site state
	 * @param siteBrand
	 *            the site brand
	 * @param siteEndCustomer
	 *            the site end customer
	 * @param latestSiteId
	 *            the latest site id
	 * @return the search site details
	 */
	List<SiteSearchBean> getSearchSiteDetails(int msaId, String siteName,
			String siteNo, String lo_ot_id, String siteCity, String siteState,
			String siteBrand, String siteEndCustomer, int latestSiteId);

	/**
	 * Gets the end customer.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @return the end customer
	 */
	String getEndCustomer(String appendixId);

	/**
	 * Gets the msa id.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @return the msa id
	 */
	int getMsaId(String appendixId);

	/**
	 * Gets the country list.
	 * 
	 * @param msaId
	 *            the msa id
	 * @return the country list
	 */
	List<SiteManagementBean> getCountryList(String msaId);

	/**
	 * Gets the site end customer list.
	 * 
	 * @param msaId
	 *            the msa id
	 * @return the site end customer list
	 */
	List<LabelValue> getSiteEndCustomerList(String msaId);

	/**
	 * Gets the group name list site search.
	 * 
	 * @param msaId
	 *            the msa id
	 * @return the group name list site search
	 */
	List<LabelValue> getGroupNameListSiteSearch(String msaId);

	/**
	 * Update site.
	 * 
	 * @param siteform
	 *            the siteform
	 * @param loginuserid
	 *            the loginuserid
	 * @return the int
	 */
	int updateSite(SiteBean siteform, String loginuserid);

	/**
	 * Gets the job information.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the job information
	 */
	JobSetUpDTO getJobInformation(String jobId);

	/**
	 * Gets the site state list.
	 * 
	 * @param countryId
	 *            the country id
	 * @param msaId
	 *            the msa id
	 * @return the site state list
	 */
	List<LabelValue> getSiteStateList(String countryId, String msaId);

	/**
	 * Gets the site details.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the site details
	 */
	SiteBean getSiteDetails(String jobId);

	/**
	 * Adds the new site.
	 * 
	 * @param siteListId
	 *            the site list id
	 * @param siteId
	 *            the site id
	 * @param jobId
	 *            the job id
	 * @param userId
	 *            the user id
	 */
	void addNewSite(int siteListId, int siteId, int jobId, String userId);

}
