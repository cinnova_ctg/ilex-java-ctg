package com.ilex.ws.core.dao;

import com.ilex.ws.core.bean.MacWorxDetailVO;
import com.ilex.ws.core.bean.MacWorxIntergrationResponseVO;
import com.ilex.ws.core.bean.MacWorxIntergrationVO;
import com.mind.bean.docm.Document;

// TODO: Auto-generated Javadoc
/**
 * The Interface MacWorxDAO.
 */
public interface MacWorxDAO {

	/**
	 * Gets the latitude longitude.
	 * 
	 * @param address
	 *            the address
	 * @param city
	 *            the city
	 * @param state
	 *            the state
	 * @param country
	 *            the country
	 * @param zipcode
	 *            the zipcode
	 * @return the latitude longitude
	 */
	String[] getLatitudeLongitude(String address, String city, String state,
			String country, String zipcode);

	/**
	 * Creates the job for mega path.
	 * 
	 * @param macWorxIntergrationVO
	 *            the mac worx intergration vo
	 * @return the mac worx intergration response vo
	 */
	MacWorxIntergrationResponseVO createJobForMegaPath(
			MacWorxIntergrationVO macWorxIntergrationVO);

	/**
	 * Upload file.
	 * 
	 * @param doc
	 *            the doc
	 * @return the int
	 */
	int uploadFile(Document doc);

	/**
	 * Gets the meta data for mac worx.
	 * 
	 * @param jobId
	 *            the job id
	 * @param poId
	 *            the po id
	 * @return the meta data for mac worx
	 */
	MacWorxDetailVO getMetaDataForMacWorx(String jobId, String poId);

	/**
	 * Insert doc entry to po document.
	 * 
	 * @param poId
	 *            the po id
	 * @param docId
	 *            the doc id
	 * @param userName
	 *            the user name
	 */
	void insertDocEntryToPoDocument(int poId, int docId, String userName);

	/**
	 * Gets the job owner name.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the job owner name
	 */
	String getJobOwnerName(int jobId);

	/**
	 * Mac worx user auth.
	 * 
	 * @param userName
	 *            the user name
	 * @param password
	 *            the password
	 * @param prId
	 *            the pr id
	 * @return the int
	 */
	int macWorxUserAuth(String userName, String password, int prId);
}
