package com.ilex.ws.core.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.IlexTicketVO;
import com.ilex.ws.core.bean.ServiceNowTicketDetailVO;
import com.ilex.ws.core.bean.ServiceNowTicketVO;
import com.ilex.ws.core.dao.DemoServiceNowIntegrationDAO;
import com.ilex.ws.core.util.HelpDeskTicketType;
import com.ilex.ws.servicenow.dto.ResponseDTO;

/**
 * The Class DemoServiceNowIntegrationDAOImpl.
 */
@Repository
public class DemoServiceNowIntegrationDAOImpl implements
		DemoServiceNowIntegrationDAO {

	/** The jdbc template ilex. */
	@Resource(name = "jdbcTemplateIlex")
	private JdbcTemplate jdbcTemplateIlex;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DemoServiceNowIntegrationDAO#insertTicketToIlex(
	 * com.ilex.ws.core.bean.ServiceNowTicketVO)
	 */
	@Override
	public int insertTicketToIlex(final ServiceNowTicketVO serviceNowTicketVO) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		final StringBuilder sqlQuery = new StringBuilder(
				"insert into ilex_ticket (Category,incident_state,");
		sqlQuery.append(" caused_by,short_description,active,serviceNowTicket,serviceNowTicketSuccess");
		sqlQuery.append(") values (?,?,?,?,?,?,?)");
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						sqlQuery.toString(), new String[] { "id" });
				ps.setString(1, serviceNowTicketVO.getCategory());
				ps.setString(2, serviceNowTicketVO.getIncidentState());
				ps.setString(3, serviceNowTicketVO.getCausedBy());
				ps.setString(4, serviceNowTicketVO.getShortDescription());
				ps.setBoolean(5, true);
				ps.setBoolean(6, serviceNowTicketVO.isServiceNowTicket());
				ps.setBoolean(7, false);
				return ps;
			}
		};

		jdbcTemplateIlex.update(preparedStatementCreator, keyHolder);

		int ticketId = keyHolder.getKey().intValue();
		return ticketId;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DemoServiceNowIntegrationDAO#updateTicketStatus(
	 * boolean, int)
	 */
	@Override
	public void updateTicketStatus(boolean insertStatus, int ticketId) {
		StringBuilder updateQuery = new StringBuilder();
		Object[] param = new Object[] { insertStatus, ticketId };
		updateQuery
				.append(" update ilex_ticket set serviceNowTicketSuccess=? where ticket_id=?");
		jdbcTemplateIlex.update(updateQuery.toString(), param);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.core.dao.DemoServiceNowIntegrationDAO#insertServiceNowResponse
	 * (int, com.ilex.ws.servicenow.dto.ResponseDTO)
	 */
	@Override
	public void insertServiceNowResponse(int ticketId, ResponseDTO responseDTO) {
		StringBuilder insertQuery = new StringBuilder();
		Object[] param = new Object[] { ticketId,
				responseDTO.getDisplayValue(), responseDTO.getSysId() };
		insertQuery
				.append(" insert into serviceNow_ticket(ilex_ticket_id,serviceNow_ticket_id,servicenow_sys_id) values(?,?,?)");
		jdbcTemplateIlex.update(insertQuery.toString(), param);

	}

	@Override
	public List<IlexTicketVO> getTicketDetails(
			HelpDeskTicketType helpDeskTicketType) {

		Object[] param = new Object[] { helpDeskTicketType.toString() };
		StringBuilder selectQuery = new StringBuilder(
				"select top 10 * from serviceNow_ticket");
		selectQuery.append(" join ilex_ticket on ticket_id=ilex_ticket_id");
		selectQuery
				.append(" where serviceNowTicket=? order  by ilex_ticket_id desc");

		List<IlexTicketVO> ilexTickets = jdbcTemplateIlex.query(
				selectQuery.toString(), param, new RowMapper<IlexTicketVO>() {

					@Override
					public IlexTicketVO mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						IlexTicketVO ilexTicketVO = new IlexTicketVO();
						ilexTicketVO.setIlexTicketId(rs
								.getInt("ilex_ticket_id"));
						ilexTicketVO.setServiceNowSysId(rs
								.getString("servicenow_sys_id"));
						ilexTicketVO.setServiceNowTicketId(rs
								.getString("serviceNow_ticket_id"));
						ilexTicketVO.setCategory(rs.getString("Category"));
						ilexTicketVO.setIncidentState(rs
								.getString("incident_state"));
						ilexTicketVO.setShortDescription(rs
								.getString("short_description"));

						return ilexTicketVO;
					}

				});

		return ilexTickets;
	}

	@Override
	public ServiceNowTicketDetailVO getServiceNowComments(String ticketNumber) {
		Object[] selectParam = new Object[] { ticketNumber };

		StringBuilder selectQuery = new StringBuilder(
				"select * from serviceNowTicketComments where  ticketNumber=?");
		ServiceNowTicketDetailVO ticketDetails = new ServiceNowTicketDetailVO();
		try {
			ticketDetails = jdbcTemplateIlex.queryForObject(
					selectQuery.toString(), selectParam,
					new RowMapper<ServiceNowTicketDetailVO>() {

						@Override
						public ServiceNowTicketDetailVO mapRow(ResultSet rs,
								int rowNum) throws SQLException {
							ServiceNowTicketDetailVO ticketDetail = new ServiceNowTicketDetailVO();
							ticketDetail.setComments(rs.getString("comments"));
							ticketDetail.setWorkNotes(rs.getString("workNotes"));
							ticketDetail.setTicketNumber(rs
									.getString("ticketNumber"));
							return ticketDetail;
						}

					});
		} catch (EmptyResultDataAccessException ex) {
			ticketDetails.setComments("");
			ticketDetails.setWorkNotes("");
		}
		return ticketDetails;

	}

	@Override
	public void addComment(String comments, String workNotes,
			String ticketNumber) {
		StringBuilder insertQuery = new StringBuilder();
		Object[] insertParam = new Object[] { ticketNumber, comments, workNotes };

		StringBuilder deleteQuery = new StringBuilder();
		Object[] delteParam = new Object[] { ticketNumber };

		deleteQuery
				.append(" delete serviceNowTicketComments where ticketNumber=?");
		jdbcTemplateIlex.update(deleteQuery.toString(), delteParam);

		insertQuery
				.append(" insert into serviceNowTicketComments(ticketNumber,comments,workNotes) values(?,?,?)");
		jdbcTemplateIlex.update(insertQuery.toString(), insertParam);
	}

}
