package com.ilex.ws.core.dao;

import com.ilex.ws.core.bean.HDTicketVO;
import com.ilex.ws.core.bean.ResourceDetailVO;

/**
 * The Interface HDJobStructureDAO containing all the methods required for
 * creating a job structure.
 */
public interface HDJobStructureDAO {

	/**
	 * Insert job record.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 * @return the long
	 */
	Long insertJobRecord(HDTicketVO hdTicketVO, Long userId);

	/**
	 * Insert activity record.
	 * 
	 * @param jobId
	 *            the job id
	 * @param userId
	 *            the user id
	 * @return the long
	 */
	Long insertActivityRecord(Long jobId, Long userId,
			Boolean isTicketManualCreateAndBillable);

	/**
	 * Insert resource record.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param activityId
	 *            the activity id
	 * @param jobId
	 *            the job id
	 * @param userId
	 *            the user id
	 * @return the resource detail vo
	 */
	ResourceDetailVO insertResourceRecord(HDTicketVO hdTicketVO,
			Long activityId, Long jobId, Long userId);

	/**
	 * Insert resource cost element record.
	 * 
	 * @param resourceDetailVO
	 *            the resource detail vo
	 */
	void insertResourceCostElementRecord(ResourceDetailVO resourceDetailVO);

	/**
	 * Insert job activity cost element record.
	 * 
	 * @param jobId
	 *            the job id
	 * @param activityId
	 *            the activity id
	 */
	void insertJobActivityCostElementRecord(Long jobId, Long activityId);

	/**
	 * Update ticket with job id.
	 * 
	 * @param jobId
	 *            the job id
	 * @param ticketId
	 *            the ticket id
	 */
	void updateTicketWithJobId(Long jobId, Long ticketId);

	/**
	 * Creates the job structure.
	 * 
	 * @param hdTicketVO
	 *            the hd ticket vo
	 * @param userId
	 *            the user id
	 */
	void createJobStructure(HDTicketVO hdTicketVO, Long userId,
			Boolean isManualTicketCreate);
}
