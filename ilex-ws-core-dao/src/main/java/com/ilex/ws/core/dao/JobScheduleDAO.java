package com.ilex.ws.core.dao;

import com.mind.bean.newjobdb.JobDashboardBean;

public interface JobScheduleDAO {

	JobDashboardBean setScheduleDates(String jobId);

	int[] addScheduleForPRM(String lx_se_type_id, String act_start,
			String act_end, String lx_se_type, String lx_se_created_by,
			String lx_se_start, String lx_se_end, int count);

	void setReschedule(String jobId, String resType, String oldSchDate,
			String newSchDate, String resReason, String user);

}
