package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.bean.WugOutInProcessVO;
import com.ilex.ws.core.dao.WugOutageCloseActionsDAO;

@Repository
public class WugOutageCloseActionsDAOImpl implements WugOutageCloseActionsDAO {
	public static final Logger logger = Logger
			.getLogger(WugOutageCloseActionsDAOImpl.class);

	/** The jdbc template ilex. */
	@Resource(name = "jdbcTemplateIlex")
	private JdbcTemplate jdbcTemplateIlex;

	@Override
	public WugOutInProcessVO getDataFromInProcess(
			String nActiveMonitorStateChangeID, String wugInstance) {
		StringBuilder sqlQuery = new StringBuilder(
				" select wug_ip_dv_id, wug_ip_dStartTime, wug_ip_escalation_level, wug_ip_current_status, wug_ip_outage_duration, ");
		sqlQuery.append(" wug_ip_event_count, wug_ip_ticket_reference,wug_ip_nPivotActiveMonitorTypeToDeviceID,wug_ip_tc_id from wug_outages_incidents_in_process");
		sqlQuery.append(" where wug_ip_nActiveMonitorStateChangeID = ? ");
		sqlQuery.append(" and isnull(wug_ip_instance,1) = ?");
		Object[] param = new Object[] { nActiveMonitorStateChangeID,
				wugInstance };
		WugOutInProcessVO wugOutInProcessVO = new WugOutInProcessVO();
		try {
			wugOutInProcessVO = jdbcTemplateIlex.queryForObject(
					sqlQuery.toString(), param, new WugOutInProcessMapper());
		} catch (EmptyResultDataAccessException e) {
			logger.debug(e);
		} catch (IncorrectResultSizeDataAccessException e) {
			logger.debug(e);
		}
		/*
		 * Lookup values from in process history
		 * 
		 * wug_ip_nActiveMonitorStateChangeID_new - need maxwug_ih_event_time -
		 * need max
		 */
		WugOutInProcessVO wugOutInProcessHistory = new WugOutInProcessVO();
		sqlQuery = new StringBuilder(
				" select  max(wug_ip_nActiveMonitorStateChangeID_new) as 'wug_ip_nActiveMonitorStateChangeID_new', ");
		sqlQuery.append(" max(wug_ih_event_time) as 'wug_ih_event_time'");
		sqlQuery.append("  from wug_outages_incidents_in_process_history ");
		sqlQuery.append("  where wug_ip_nActiveMonitorStateChangeID = ? ");
		sqlQuery.append("  and isnull(wug_ih_instance,1) = ? ");

		try {
			wugOutInProcessHistory = jdbcTemplateIlex.queryForObject(
					sqlQuery.toString(), param,
					new WugOutInProcessHistoryMapper());
		} catch (EmptyResultDataAccessException e) {
			logger.debug(e);
		} catch (IncorrectResultSizeDataAccessException e) {
			logger.debug(e);
		}
		wugOutInProcessVO
				.setnActiveMonitorStateChangeIdNew(wugOutInProcessHistory
						.getnActiveMonitorStateChangeIdNew());
		wugOutInProcessVO.setIhEventTime(wugOutInProcessHistory
				.getIhEventTime());

		return wugOutInProcessVO;
	}

	@Override
	public int getEventTestCount(String nActiveMonitorStateChangeID,
			String wugInstance) {
		StringBuilder sqlQuery = new StringBuilder(
				" select count(*) from wug_outages_incidents_closed ");
		sqlQuery.append("  where wug_cp_nActiveMonitorStateChangeID_1 = ? ");
		sqlQuery.append(" and isnull(wug_cp_instance,1) = ? ");
		Object[] param = new Object[] { nActiveMonitorStateChangeID,
				wugInstance };
		int eventTest = jdbcTemplateIlex
				.queryForInt(sqlQuery.toString(), param);

		return eventTest;
	}

	private static final class WugOutInProcessMapper implements
			RowMapper<WugOutInProcessVO> {

		public WugOutInProcessVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			WugOutInProcessVO wugOutInProcessVO = new WugOutInProcessVO();
			wugOutInProcessVO.setDvId(rs.getString("wug_ip_dv_id"));
			wugOutInProcessVO.setdStartTime(rs.getString("wug_ip_dStartTime"));
			wugOutInProcessVO.setEscalationLevel(rs
					.getString("wug_ip_escalation_level"));
			wugOutInProcessVO.setCurrentStatus(rs
					.getString("wug_ip_current_status"));
			wugOutInProcessVO.setOutageDuration(rs
					.getString("wug_ip_outage_duration"));
			wugOutInProcessVO.setEventCount(rs.getString("wug_ip_event_count"));
			wugOutInProcessVO.setTicketReference(rs
					.getString("wug_ip_ticket_reference"));
			wugOutInProcessVO.setWugihnPivotActiveMonitorTypeToDeviceID(rs
					.getString("wug_ip_nPivotActiveMonitorTypeToDeviceID"));
			wugOutInProcessVO.setTicketId(rs.getString("wug_ip_tc_id"));
			return wugOutInProcessVO;
		}
	}

	private static final class WugOutInProcessHistoryMapper implements
			RowMapper<WugOutInProcessVO> {

		public WugOutInProcessVO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			WugOutInProcessVO wugOutInProcessHistory = new WugOutInProcessVO();
			wugOutInProcessHistory.setnActiveMonitorStateChangeIdNew(rs
					.getString("wug_ip_nActiveMonitorStateChangeID_new"));
			wugOutInProcessHistory.setIhEventTime(rs
					.getString("wug_ih_event_time"));
			return wugOutInProcessHistory;
		}
	}

	@Override
	public void insertWugOutClosedRecord(WugOutInProcessVO wugOutInProcessVO) {
		StringBuilder sqlQuery = new StringBuilder(
				" insert into wug_outages_incidents_closed( ");
		sqlQuery.append(" wug_cp_nActiveMonitorStateChangeID_1,wug_cp_nActiveMonitorStateChangeID_2, wug_cp_dv_id, ");
		sqlQuery.append(" wug_cp_js_id,wug_cp_ticket_reference,wug_cp_issue_owner, ");
		sqlQuery.append(" wug_cp_root_couse,wug_cp_corrective_action,wug_ip_effort_minutes, ");
		sqlQuery.append(" wug_cp_outage_duration,wug_cp_escalation_level,wug_cp_current_status,");
		sqlQuery.append(" wug_cp_initiation_point,wug_cp_complete_point,wug_cp_event_count,");
		sqlQuery.append(" wug_cp_instance,wug_cp_backup_status,wug_ih_nPivotActiveMonitorTypeToDeviceID,wug_cp_tc_id) ");
		sqlQuery.append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		sqlQuery.append("");
		Object[] param = new Object[] {
				wugOutInProcessVO.getnActiveMonitorStateChangeID(),
				wugOutInProcessVO.getnActiveMonitorStateChangeIdNew(),
				wugOutInProcessVO.getDvId(), null,
				wugOutInProcessVO.getTicketReference(),
				wugOutInProcessVO.getIssueOwner(),
				wugOutInProcessVO.getRootCouse(),
				wugOutInProcessVO.getCorrectiveAction(),
				wugOutInProcessVO.getEffortMinutes(),
				wugOutInProcessVO.getOutageDuration(),
				wugOutInProcessVO.getEscalationLevel(),
				wugOutInProcessVO.getCurrentStatus(),
				wugOutInProcessVO.getdStartTime(),
				wugOutInProcessVO.getIhEventTime(),
				wugOutInProcessVO.getEventCount(),
				wugOutInProcessVO.getWugInstance(),
				wugOutInProcessVO.getWugBackupStatus(),
				wugOutInProcessVO.getWugihnPivotActiveMonitorTypeToDeviceID(),
				wugOutInProcessVO.getTicketId() };
		jdbcTemplateIlex.update(sqlQuery.toString(), param);

	}

	@Override
	public void insertWugOutClosedHistoryRecord(
			String nActiveMonitorStateChangeID, String wugInstance) {
		StringBuilder sqlQuery = new StringBuilder(
				" insert into wug_outages_incidents_closed_history( ");
		sqlQuery.append(" wug_cp_nActiveMonitorStateChangeID_1,wug_ch_nActiveMonitorStateChangeID_new,wug_ch_transition_from, ");
		sqlQuery.append(" wug_ch_transition_to,wug_ch_event_time,wug_ch_down_duration, ");
		sqlQuery.append(" wug_ch_instance,wug_ch_from_sStateName,wug_ch_to_sStateName) ");
		sqlQuery.append(" select wug_ip_nActiveMonitorStateChangeID, ");
		sqlQuery.append(" wug_ip_nActiveMonitorStateChangeID_new,wug_ih_transition_from,wug_ih_transition_to, ");
		sqlQuery.append(" wug_ih_event_time,wug_ih_down_duration,wug_ih_instance, ");
		sqlQuery.append(" wug_ih_from_sStateName,wug_ih_to_sStateName ");
		sqlQuery.append(" from wug_outages_incidents_in_process_history ");
		sqlQuery.append(" where wug_ip_nActiveMonitorStateChangeID = ? ");
		sqlQuery.append(" and isnull(wug_ih_instance,1) = ? ");
		Object[] param = new Object[] { nActiveMonitorStateChangeID,
				wugInstance };
		jdbcTemplateIlex.update(sqlQuery.toString(), param);

	}

	@Override
	public void deleteWugOutagesInProcessHistory(
			String nActiveMonitorStateChangeID, String wugInstance) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(" delete from wug_outages_incidents_in_process_history ");
		sqlQuery.append("  where wug_ip_nActiveMonitorStateChangeID = ? and isnull(wug_ih_instance,1) = ?");
		Object[] param = new Object[] { nActiveMonitorStateChangeID,
				wugInstance };
		jdbcTemplateIlex.update(sqlQuery.toString(), param);

	}

	@Override
	public void deleteWugOutagesInProcess(String nActiveMonitorStateChangeID,
			String wugInstance) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(" delete from wug_outages_incidents_in_process ");
		sqlQuery.append("  where wug_ip_nActiveMonitorStateChangeID = ? and isnull(wug_ip_instance,1) = ?");
		Object[] param = new Object[] { nActiveMonitorStateChangeID,
				wugInstance };
		jdbcTemplateIlex.update(sqlQuery.toString(), param);

	}

}
