package com.ilex.ws.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ilex.ws.core.dao.LoginDAO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.util.MessageKeyConstant;
import com.mind.bean.LoginForm;

@Repository
public class LoginDAOImpl implements LoginDAO {
	@Resource
	private JdbcTemplate jdbcTemplateIlex;

	@Override
	public LoginForm getUserInfo(String userName) {

		String sql = "select lo_us_pc_id, lo_pc_email1 , username from func_lo_login_user_info('"
				+ userName + "')";
		List<LoginForm> loginFormList;

		loginFormList = jdbcTemplateIlex.query(sql, new LoginMapper());
		if (loginFormList == null || loginFormList.size() == 0) {
			throw new IlexBusinessException(MessageKeyConstant.USER_NOT_FOUND);
		}

		return loginFormList.get(0);

	}

	/**
	 * The Class LoginMapper.
	 */
	private static final class LoginMapper implements RowMapper<LoginForm> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
		 * int)
		 */
		@Override
		public LoginForm mapRow(ResultSet rs, int rowNum) throws SQLException {
			LoginForm loginForm = new LoginForm();
			loginForm.setLogin_user_email(rs.getString("lo_pc_email1"));
			loginForm.setLogin_user_id(rs.getString("lo_us_pc_id"));
			loginForm.setLogin_user_name(rs.getString("username"));

			return loginForm;
		}
	}

	@Override
	public LoginForm getUserInfoByUserId(String userId) {

		String sql = "select lo_us_pc_id, lo_pc_email1, username = dbo.func_cc_poc_name(lo_us_pc_id) "
				+ " from lo_user a  "
				+ " inner join lo_us_authentication b "
				+ " on a.lo_us_name = b.lo_us_name "
				+ " inner join lo_poc c "
				+ " on a.lo_us_pc_id = c.lo_pc_id "
				+ " AND lo_pc_id = "
				+ userId;
		List<LoginForm> loginFormList;

		loginFormList = jdbcTemplateIlex.query(sql, new LoginMapper());
		if (loginFormList == null || loginFormList.size() == 0) {
			throw new IlexBusinessException(MessageKeyConstant.USER_NOT_FOUND);
		}

		return loginFormList.get(0);

	}

	@Override
	public String checkUserRoleByRole(String userid, String role) {

		String sql = "select 1 from lo_poc_rol inner join lo_role on lo_pr_ro_id = lo_ro_id where lo_pr_pc_id = "
				+ userid + " and" + " lo_ro_role_name in ('" + role + "')";

		try {
			int val = jdbcTemplateIlex.queryForInt(sql);
			if (val == 1) {
				return "Y";
			}
		} catch (Exception e) {
			System.out.println("Role not found...");
		}
		return "";
	}
}
