package com.ilex.ws.core.dao;

import java.util.List;

import com.ilex.ws.core.bean.CountryVO;
import com.ilex.ws.core.bean.StateVO;

public interface GeneralUtilsDAO {
	List<CountryVO> getCountrylist();

	List<StateVO> getStateList();

}
