package com.ilex.report.pdf.template.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.ilex.invoice.reporting.dto.InvoiceDetailsDTO;
import com.ilex.report.pdf.template.PDFTemplateA;

public class TemplateATest {

	@Test
	public void singlePageReport() throws FileNotFoundException, IOException {

		TemplateATestData.populateBaseData();

		List<InvoiceDetailsDTO> invoiceDetList = new ArrayList<InvoiceDetailsDTO>();

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		TemplateATestData.invoiceReportDTO
				.setInvoiceDetailsDTOs(invoiceDetList);

		PDFTemplateA pdfTemplateA = new PDFTemplateA();
		byte[] byteArray = pdfTemplateA
				.generatePDF(TemplateATestData.invoiceReportDTO);
		File pdfFile = new File("C:/temp/templateA_1_singlePage.pdf");
		if (!pdfFile.exists()) {
			pdfFile.createNewFile();
		}
		IOUtils.write(byteArray, new FileOutputStream(pdfFile));
	}

	@Test
	public void multiPageReport() throws FileNotFoundException, IOException {

		TemplateATestData.populateBaseData();

		List<InvoiceDetailsDTO> invoiceDetList = new ArrayList<InvoiceDetailsDTO>();

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		TemplateATestData.invoiceReportDTO
				.setInvoiceDetailsDTOs(invoiceDetList);

		PDFTemplateA pdfTemplateA = new PDFTemplateA();
		byte[] byteArray = pdfTemplateA
				.generatePDF(TemplateATestData.invoiceReportDTO);
		File pdfFile = new File("C:/temp/templateA_2_multiplePages.pdf");
		if (!pdfFile.exists()) {
			pdfFile.createNewFile();
		}
		IOUtils.write(byteArray, new FileOutputStream(pdfFile));
	}

	@Test
	public void footerOnNewPage() throws FileNotFoundException, IOException {

		TemplateATestData.populateBaseData();

		List<InvoiceDetailsDTO> invoiceDetList = new ArrayList<InvoiceDetailsDTO>();

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);

		TemplateATestData.invoiceReportDTO
				.setInvoiceDetailsDTOs(invoiceDetList);

		PDFTemplateA pdfTemplateA = new PDFTemplateA();

		byte[] byteArray = pdfTemplateA
				.generatePDF(TemplateATestData.invoiceReportDTO);
		File pdfFile = new File("C:/temp/templateA_3_keepTogether.pdf");
		if (!pdfFile.exists()) {
			pdfFile.createNewFile();
		}
		IOUtils.write(byteArray, new FileOutputStream(pdfFile));
	}

	@Test
	public void keepTogether() throws FileNotFoundException, IOException {

		TemplateATestData.populateBaseData();

		List<InvoiceDetailsDTO> invoiceDetList = new ArrayList<InvoiceDetailsDTO>();

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);

		invoiceDetList.add(TemplateATestData.invoiceDetDto5);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		TemplateATestData.invoiceReportDTO
				.setInvoiceDetailsDTOs(invoiceDetList);

		PDFTemplateA pdfTemplateA = new PDFTemplateA();

		byte[] byteArray = pdfTemplateA
				.generatePDF(TemplateATestData.invoiceReportDTO);
		File pdfFile = new File("C:/temp/templateA_4_footerOnNewPage.pdf");
		if (!pdfFile.exists()) {
			pdfFile.createNewFile();
		}
		IOUtils.write(byteArray, new FileOutputStream(pdfFile));
	}

	@Test
	public void netMedXLongReport() throws FileNotFoundException, IOException {

		TemplateATestData.populateBaseData();

		List<InvoiceDetailsDTO> invoiceDetList = new ArrayList<InvoiceDetailsDTO>();

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.netMedXLongInvoiceDetDto);

		TemplateATestData.invoiceReportDTO
				.setInvoiceDetailsDTOs(invoiceDetList);

		PDFTemplateA pdfTemplateA = new PDFTemplateA();
		byte[] byteArray = pdfTemplateA
				.generatePDF(TemplateATestData.invoiceReportDTO);
		File pdfFile = new File("C:/temp/templateA_5_NetMedX_long.pdf");
		if (!pdfFile.exists()) {
			pdfFile.createNewFile();
		}
		IOUtils.write(byteArray, new FileOutputStream(pdfFile));
	}

	@Test
	public void invoiceTestDataSheet1() throws FileNotFoundException,
			IOException {

		PDFTemplateA pdfTemplateA = new PDFTemplateA();
		byte[] byteArray = pdfTemplateA.generatePDF(TemplateATestData
				.getInvoiceTestDataSheet1());
		File pdfFile = new File("C:/temp/templateA_6_invoiceTestDataSheet1.pdf");
		if (!pdfFile.exists()) {
			pdfFile.createNewFile();
		}
		IOUtils.write(byteArray, new FileOutputStream(pdfFile));
	}

	@Test
	public void invoiceTestDataSheet4() throws FileNotFoundException,
			IOException {
		TemplateATestData.populateBaseTestDataSheet4();

		List<InvoiceDetailsDTO> invoiceDetList = new ArrayList<InvoiceDetailsDTO>();

		invoiceDetList.add(TemplateATestData.invoiceDetDtoTest6);
		invoiceDetList.add(TemplateATestData.invoiceDetDtoTest7);

		TemplateATestData.invoiceReportDTO
				.setInvoiceDetailsDTOs(invoiceDetList);

		PDFTemplateA pdfTemplateA = new PDFTemplateA();

		byte[] byteArray = pdfTemplateA
				.generatePDF(TemplateATestData.invoiceReportDTO);

		File pdfFile = new File("C:/temp/templateA_8_invoiceTestDataSheet4.pdf");
		if (!pdfFile.exists()) {
			pdfFile.createNewFile();
		}
		IOUtils.write(byteArray, new FileOutputStream(pdfFile));

	}

	@Test
	public void invoiceTestDataSheet2() throws FileNotFoundException,
			IOException {
		PDFTemplateA pdfTemplateA = new PDFTemplateA();
		byte[] byteArray = pdfTemplateA.generatePDF(TemplateATestData
				.populateBaseTestDataSheet2());
		File pdfFile = new File("C:/temp/templateA_7_invoiceTestDataSheet2.pdf");
		if (!pdfFile.exists()) {
			pdfFile.createNewFile();
		}
		IOUtils.write(byteArray, new FileOutputStream(pdfFile));

	}
}
