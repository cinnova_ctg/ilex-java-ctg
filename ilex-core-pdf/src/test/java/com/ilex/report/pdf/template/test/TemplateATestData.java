package com.ilex.report.pdf.template.test;

import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import com.ilex.invoice.reporting.dto.AddressDTO;
import com.ilex.invoice.reporting.dto.AppendixDTO;
import com.ilex.invoice.reporting.dto.HyperLinkDTO;
import com.ilex.invoice.reporting.dto.InvoiceDetailsDTO;
import com.ilex.invoice.reporting.dto.InvoiceReportDTO;
import com.ilex.invoice.reporting.dto.OutStandingBalanceDTO;

public class TemplateATestData {

	public static InvoiceDetailsDTO invoiceDetDto1 = new InvoiceDetailsDTO();
	public static InvoiceDetailsDTO invoiceDetDto2 = new InvoiceDetailsDTO();
	public static InvoiceDetailsDTO invoiceDetDto3 = new InvoiceDetailsDTO();
	public static InvoiceDetailsDTO invoiceDetDto4 = new InvoiceDetailsDTO();
	public static InvoiceDetailsDTO invoiceDetDto5 = new InvoiceDetailsDTO();
	public static InvoiceDetailsDTO invoiceDetDtoTest6 = new InvoiceDetailsDTO();
	public static InvoiceDetailsDTO invoiceDetDtoTest7 = new InvoiceDetailsDTO();

	public static InvoiceDetailsDTO netMedXLongInvoiceDetDto = new InvoiceDetailsDTO();
	public static InvoiceReportDTO invoiceReportDTO = new InvoiceReportDTO();

	public static void populateBaseData() {

		invoiceReportDTO.setAppendixDTO(TemplateATestData.getAppendixRef());
		invoiceReportDTO.setBillToAddressDTO(TemplateATestData
				.getBillToAddress());
		invoiceReportDTO.setHeaderAddressDTO(TemplateATestData
				.getContingentAddress());
		invoiceReportDTO.setInvoiceHyperLinkDTO(TemplateATestData
				.getInvoiceHyperLink());
		invoiceReportDTO.setOutStandingBalanceDTO(TemplateATestData
				.getOutstandingRangeValue());
		invoiceReportDTO.setShipToAddressDTO(TemplateATestData
				.getShipToAddress());
		invoiceReportDTO.setOffsiteDate(TemplateATestData.getOffsiteDate());
		TemplateATestData.populateContingentLogo(invoiceReportDTO);
		TemplateATestData.populateInvoiceNumberAndDate(invoiceReportDTO);
		TemplateATestData.populateInvoiceRemarks(invoiceReportDTO);
		TemplateATestData.populateInvoiceTotals(invoiceReportDTO);
		TemplateATestData.populatePaymentTerms(invoiceReportDTO);
		TemplateATestData.populateCustReferenceNameAndValue(invoiceReportDTO);

		invoiceDetDto1.setQuantity((float) 1.0);
		invoiceDetDto1.setDiscount((float) 29.50);
		invoiceDetDto1.setUnitPrice(210200.69);
		invoiceDetDto1.setExtendedPrice(2425.23);
		invoiceDetDto1.setDescription("Lorem Ipsum 1");

		invoiceDetDto2.setQuantity((float) 2.0);
		invoiceDetDto2.setDiscount((float) 393.50);
		invoiceDetDto2.setUnitPrice(3860.69);
		invoiceDetDto2.setExtendedPrice(3565468.23);
		invoiceDetDto2.setDescription("Lorem Ipsum 2");

		invoiceDetDto3.setQuantity((float) 23.0);
		invoiceDetDto3.setDiscount((float) 434.50);
		invoiceDetDto3.setUnitPrice(4560.69);
		invoiceDetDto3.setExtendedPrice(4898425.23);
		invoiceDetDto3.setDescription("Lorem Ipsum 3");

		invoiceDetDto4.setQuantity((float) 11.0);
		invoiceDetDto4.setDiscount((float) 523.50);
		invoiceDetDto4.setUnitPrice(54300.69);
		invoiceDetDto4.setExtendedPrice(5432325.23);
		invoiceDetDto4.setDescription("Lorem Ipsum 4");

		invoiceDetDto5.setQuantity((float) 4.0);
		invoiceDetDto5.setDiscount((float) 645.50);
		invoiceDetDto5.setUnitPrice(6463.69);
		invoiceDetDto5.setExtendedPrice(78637);
		invoiceDetDto5
				.setDescription("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum");

		netMedXLongInvoiceDetDto.setQuantity((float) 1.0);
		netMedXLongInvoiceDetDto.setDiscount((float) 0.00);
		netMedXLongInvoiceDetDto.setUnitPrice(71.00);
		netMedXLongInvoiceDetDto.setExtendedPrice(71.00);
		StringBuilder netMedXLongInvoiceDesc = new StringBuilder(
				"Travel - Field Technician 2-C5\n\n");
		netMedXLongInvoiceDesc.append("Details:\n");
		netMedXLongInvoiceDesc.append("Type: NetMedX-123").append("\n");
		netMedXLongInvoiceDesc.append("Site: XYZ").append("\n");
		netMedXLongInvoiceDesc.append("Requested: 01/01/12 12:15").append("\n");
		netMedXLongInvoiceDesc.append("Requestor: John Smith").append("\n");
		netMedXLongInvoiceDesc.append("Problem Category: ABC").append("\n");
		netMedXLongInvoiceDesc.append("Summary: Closing note").append("\n");
		netMedXLongInvoiceDesc.append("Additional Information").append("\n");
		netMedXLongInvoiceDesc
				.append("                 Ticket Number:  AB1111-0");
		netMedXLongInvoiceDetDto.setDescription(netMedXLongInvoiceDesc
				.toString());

	}

	public static void populateContingentLogo(InvoiceReportDTO invoiceReportDTO) {
		URL imageURL = Thread.currentThread().getContextClassLoader()
				.getResource("images/contingentLogo.png");
		invoiceReportDTO.setHeaderLogo(imageURL.getFile());
	}

	public static void populateInvoiceNumberAndDate(
			InvoiceReportDTO invoiceReportDTO) {
		Date InvoiceDate = new java.util.Date();
		invoiceReportDTO.setInvoiceNumber("123456");
		invoiceReportDTO.setInvoiceDate(InvoiceDate);
	}

	public static AddressDTO getContingentAddress() {
		AddressDTO addressDTO = new AddressDTO();
		addressDTO.setName("CONTINGENT NETWORK SERVICES,LLC");
		addressDTO.setAddressLine1("4400 PORT UNION ROAD");
		addressDTO.setCity("WEST CHESTER");
		addressDTO.setState("OH");
		addressDTO.setZipCode("45011");
		addressDTO.setCountry("US");
		addressDTO.setPhone("(800) 506-9609 (513) 860-2573 ");
		addressDTO.setFax("FAX (513) 860-2105");

		HyperLinkDTO hyperLinkDTO = new HyperLinkDTO();
		hyperLinkDTO.setAddress("medius.contingent.com");
		addressDTO.setUrl(hyperLinkDTO);
		return addressDTO;
	}

	public static AddressDTO getBillToAddress() {

		AddressDTO billToADDTO = new AddressDTO();
		billToADDTO.setName("Company");
		billToADDTO.setAddressLine1("AddressLine1");
		billToADDTO.setAddressLine2("AddressLine2");
		billToADDTO.setCity("City");
		billToADDTO.setState("State");
		billToADDTO.setZipCode("Zipcode");
		billToADDTO.setCountry("Country");
		return billToADDTO;
	}

	public static AddressDTO getShipToAddress() {
		AddressDTO shipToADDTO = new AddressDTO();
		shipToADDTO.setName("Company");
		shipToADDTO.setAddressLine1("AddressLine1");
		shipToADDTO.setAddressLine2("AddressLine2");
		shipToADDTO.setCity("City");
		shipToADDTO.setState("State");
		shipToADDTO.setZipCode("Zipcode");
		shipToADDTO.setCountry("Country");
		return shipToADDTO;
	}

	public static void populateCustReferenceNameAndValue(
			InvoiceReportDTO invoiceReportDTO) {
		invoiceReportDTO.setCustomerReferenceValue("Customer Reference Value");
		invoiceReportDTO.setCustomerReferenceName("Customer Reference Name");
	}

	public static void populatePaymentTerms(InvoiceReportDTO invoiceReportDTO) {
		invoiceReportDTO.setPaymentTerms("Payment Terms");
	}

	public static Date getOffsiteDate() {
		GregorianCalendar calander = new GregorianCalendar(2011, 12, 25);

		return calander.getTime();
	}

	public static void populateInvoiceRemarks(InvoiceReportDTO invoiceReportDTO) {
		invoiceReportDTO
				.setInvoiceRemarks("For all details pertaining to this invoice, Please click on the following link.");
	}

	public static HyperLinkDTO getInvoiceHyperLink() {
		HyperLinkDTO hypLink = new HyperLinkDTO();
		hypLink.setText("For details pertaining to this invoice, please click on the following link.");
		hypLink.setAddress("www.contingent.com/invoice");
		return hypLink;
	}

	public static AppendixDTO getAppendixRef() {
		// Date AppendixDate = new java.util.Date();
		AppendixDTO appendixDto = new AppendixDTO();
		appendixDto.setTitle("Appendix");
		// appendixDto.setDate(AppendixDate);
		return appendixDto;
	}

	public static List<OutStandingBalanceDTO> getOutstandingRangeValue() {
		List<OutStandingBalanceDTO> outStandingBalanceList = new ArrayList<OutStandingBalanceDTO>();
		OutStandingBalanceDTO outStanBalDto = new OutStandingBalanceDTO();
		OutStandingBalanceDTO outStanBalDto2 = new OutStandingBalanceDTO();
		OutStandingBalanceDTO outStanBalDto3 = new OutStandingBalanceDTO();
		outStanBalDto.setRange("30-60");
		outStanBalDto.setValue(100000.00);
		outStanBalDto2.setRange("60-90");
		outStanBalDto2.setValue(100000.00);
		outStanBalDto3.setRange("Over 90");
		outStanBalDto3.setValue(100000.00);
		outStandingBalanceList.add(outStanBalDto);
		outStandingBalanceList.add(outStanBalDto2);
		outStandingBalanceList.add(outStanBalDto3);
		return outStandingBalanceList;
	}

	public static void populateInvoiceTotals(InvoiceReportDTO invoiceReportDTO) {
		invoiceReportDTO.setSubTotal(187.50);
		invoiceReportDTO.setTax(10.00);
		invoiceReportDTO.setTotal(197.50);
	}

	public static InvoiceReportDTO getInvoiceTestDataSheet1() {
		InvoiceReportDTO invoiceReportDTO = new InvoiceReportDTO();
		populateContingentLogo(invoiceReportDTO);
		invoiceReportDTO.setHeaderAddressDTO(getContingentAddress());
		invoiceReportDTO.setInvoiceNumber("999991");
		invoiceReportDTO.setInvoiceDate((new GregorianCalendar(2011, 12, 25))
				.getTime());
		invoiceReportDTO.setBillToAddressDTO(getBillToAddress());
		invoiceReportDTO.setShipToAddressDTO(getShipToAddress());
		invoiceReportDTO.setCustomerReferenceName("Purchase Order");
		invoiceReportDTO.setCustomerReferenceValue("PO-155012");
		invoiceReportDTO.setPaymentTerms("45 Days");
		invoiceReportDTO.setOffsiteDate((new GregorianCalendar(2011, 12, 25))
				.getTime());
		invoiceReportDTO.setInvoiceHyperLinkDTO(getInvoiceHyperLink());

		AppendixDTO appendixDTO = new AppendixDTO();
		appendixDTO.setTitle("HAN Logistics Services");

		invoiceReportDTO.setAppendixDTO(appendixDTO);
		invoiceReportDTO.setOutStandingBalanceDTO(getOutstandingRangeValue());
		invoiceReportDTO.setSubTotal(25727.36);
		invoiceReportDTO.setTax(737);
		invoiceReportDTO.setTotal(26464.36);

		// Setting Invoice Details from CSV file
		try {
			URL fileURL = Thread.currentThread().getContextClassLoader()
					.getResource("InvoiceTestDataSheet1.csv");

			ICsvBeanReader inFile = new CsvBeanReader(new FileReader(
					fileURL.getFile()), CsvPreference.EXCEL_PREFERENCE);
			try {
				final String[] header = inFile.getCSVHeader(true);
				List<InvoiceDetailsDTO> invoiceDetailsDTOs = new ArrayList<InvoiceDetailsDTO>();
				InvoiceDetailsDTO invoiceDetailsDTO;
				while ((invoiceDetailsDTO = inFile.read(
						InvoiceDetailsDTO.class, header, userProcessors)) != null) {
					invoiceDetailsDTOs.add(invoiceDetailsDTO);
				}
				invoiceReportDTO.setInvoiceDetailsDTOs(invoiceDetailsDTOs);

			} finally {
				inFile.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return invoiceReportDTO;
	}

	public static void populateBaseTestDataSheet4() {

		AppendixDTO appendixDto = new AppendixDTO();
		appendixDto.setTitle("HAN Logistics Services");

		invoiceReportDTO.setAppendixDTO(appendixDto);

		invoiceReportDTO.setBillToAddressDTO(TemplateATestData
				.getBillToAddress());
		invoiceReportDTO.setShipToAddressDTO(TemplateATestData
				.getShipToAddress());
		invoiceReportDTO.setHeaderAddressDTO(TemplateATestData
				.getContingentAddress());
		invoiceReportDTO.setInvoiceHyperLinkDTO(TemplateATestData
				.getInvoiceHyperLink());
		invoiceReportDTO.setOutStandingBalanceDTO(TemplateATestData
				.getOutstandingRangeValue());

		invoiceReportDTO.setOffsiteDate((new GregorianCalendar(2011, 11, 25))
				.getTime());

		TemplateATestData.populateContingentLogo(invoiceReportDTO);

		invoiceReportDTO.setInvoiceNumber("999991");
		invoiceReportDTO.setInvoiceDate((new GregorianCalendar(2011, 11, 25))
				.getTime());

		invoiceReportDTO.setPaymentTerms("45 Days");

		invoiceReportDTO
				.setCustomerReferenceValue("See each line item under description");
		invoiceReportDTO.setCustomerReferenceName("Order Number");

		invoiceReportDTO.setSubTotal(177.50);
		invoiceReportDTO.setTax(0.00);
		invoiceReportDTO.setTotal(177.50);

		TemplateATestData.populateInvoiceRemarks(invoiceReportDTO);

		invoiceDetDtoTest6.setQuantity((float) 1.5);
		invoiceDetDtoTest6.setUnitPrice(71);
		invoiceDetDtoTest6.setExtendedPrice(106.5);
		invoiceDetDtoTest6.setDescription("Field Technician 2-C5");

		StringBuilder nestedInvoiceDetail = new StringBuilder(
				"Travel - Field Technician 2-C5" + "\n\n");
		nestedInvoiceDetail.append("Details: " + "\n");
		nestedInvoiceDetail.append("Type:  NetMedX-Copper" + "\n");
		nestedInvoiceDetail.append("Site:   SR-71" + "\n");
		nestedInvoiceDetail.append("Requested:  12/24/11" + "\n");
		nestedInvoiceDetail.append("Requestor:  Chris Williams" + "\n");
		nestedInvoiceDetail.append("Problem Category:  Repair Modem" + "\n");
		nestedInvoiceDetail
				.append("Summary:  Technician completed the installation of te modem and test connection.  Visit successful."
						+ "\n");
		nestedInvoiceDetail.append("Additional Information" + "\n");
		nestedInvoiceDetail.append("Serial Number:  123456678" + "\n");
		nestedInvoiceDetail.append("Test Out # :  AC - 13000");

		invoiceDetDtoTest7.setQuantity((float) 1);
		invoiceDetDtoTest7.setDescription(nestedInvoiceDetail.toString());
		invoiceDetDtoTest7.setUnitPrice(71);
		invoiceDetDtoTest7.setExtendedPrice(71);

	}

	public static InvoiceReportDTO populateBaseTestDataSheet2() {

		InvoiceReportDTO invoiceReportDTO = new InvoiceReportDTO();

		populateContingentLogo(invoiceReportDTO);
		invoiceReportDTO.setHeaderAddressDTO(getContingentAddress());
		invoiceReportDTO.setInvoiceNumber("999991");
		invoiceReportDTO.setInvoiceDate((new GregorianCalendar(2011, 12, 25))
				.getTime());
		invoiceReportDTO.setBillToAddressDTO(getBillToAddress());
		invoiceReportDTO.setShipToAddressDTO(getShipToAddress());
		invoiceReportDTO.setCustomerReferenceName("Order Number");
		invoiceReportDTO
				.setCustomerReferenceValue("See each line item under description");
		invoiceReportDTO.setPaymentTerms("45 Days");
		invoiceReportDTO.setOffsiteDate((new GregorianCalendar(2011, 12, 25))
				.getTime());
		invoiceReportDTO.setInvoiceHyperLinkDTO(getInvoiceHyperLink());

		AppendixDTO appendixDTO = new AppendixDTO();
		appendixDTO.setTitle("HAN Logistics Services");

		invoiceReportDTO.setAppendixDTO(appendixDTO);
		invoiceReportDTO.setOutStandingBalanceDTO(getOutstandingRangeValue());
		invoiceReportDTO.setSubTotal(27977.18);
		invoiceReportDTO.setTax(0);
		invoiceReportDTO.setTotal(27977.18);

		// Setting Invoice Details from CSV file
		try {
			URL fileURL = Thread.currentThread().getContextClassLoader()
					.getResource("InvoiceTestDataSheet2.csv");

			ICsvBeanReader inFile = new CsvBeanReader(new FileReader(
					fileURL.getFile()), CsvPreference.EXCEL_PREFERENCE);
			try {
				final String[] header = inFile.getCSVHeader(true);
				List<InvoiceDetailsDTO> invoiceDetailsDTOs = new ArrayList<InvoiceDetailsDTO>();
				InvoiceDetailsDTO invoiceDetailsDTO;
				while ((invoiceDetailsDTO = inFile.read(
						InvoiceDetailsDTO.class, header, userProcessors)) != null) {
					invoiceDetailsDTOs.add(invoiceDetailsDTO);
				}
				invoiceReportDTO.setInvoiceDetailsDTOs(invoiceDetailsDTOs);

			} finally {
				inFile.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return invoiceReportDTO;

	}

	static final CellProcessor[] userProcessors = new CellProcessor[] {
			new ParseDouble(), null, new Optional(new ParseDouble()),
			new ParseDouble(), new ParseDouble() };
}
