package com.ilex.invoice.reporting.dto;

import java.io.Serializable;

public class HyperLinkDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 508074245767999493L;
	private String text;
	private String address;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
