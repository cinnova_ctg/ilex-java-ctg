package com.ilex.invoice.reporting.common;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Utilities;

public class IlexReportPDFConstants {
	// FONT constants
	public static final Font STANDARD_FONT = new Font(FontFamily.HELVETICA, 8,
			Font.NORMAL, BaseColor.BLACK);
	public static final Font HEADER_FOOTER_STANDARD_BOLD_FONT = new Font(
			FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
	public static final Font TABLE_STANDARD_HEADER_FONT = new Font(
			FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font STANDARD_BLUE_FONT = new Font(
			FontFamily.HELVETICA, 8, Font.NORMAL, new BaseColor(28, 52, 116));
	public static final Font STANDARD_BOLD_BLUE_FONT = new Font(
			FontFamily.HELVETICA, 8, Font.BOLD, new BaseColor(28, 52, 116));
	public static final Font STANDARD_BOLD_FONT = new Font(
			FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
	public static final Font HEADER_BLUE_FONT = new Font(FontFamily.HELVETICA,
			12, Font.BOLD, new BaseColor(28, 52, 116));
	public static final Font HEADER_FOOTER_STANDARD_BOLD_BLUE_FONT = new Font(
			FontFamily.HELVETICA, 8, Font.BOLD, new BaseColor(28, 52, 116));
	public static final Font HEADER_FOOTER_STANDARD_BLUE_FONT = new Font(
			FontFamily.HELVETICA, 8, Font.NORMAL, new BaseColor(28, 52, 116));
	public static final Font TABLE_STANDARD_HEADER_WHITE_FONT = new Font(
			FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.WHITE);

	// COLOR constants
	public static final BaseColor TABLE_STANDARD_HEADER_NAVYBLUE_COLOR = new BaseColor(
			28, 52, 116);

	// PAGE dimension constants
	public static final Rectangle PAGE_SIZE = PageSize.A4;
	public static final float PAGE_LEFT_MARGIN = Utilities.inchesToPoints(0.5f);
	public static final float PAGE_TOP_MARGIN = Utilities.inchesToPoints(0.5f);
	public static final float PAGE_RIGHT_MARGIN = Utilities
			.inchesToPoints(0.5f);
	public static final float PAGE_BOTTOM_MARGIN = Utilities
			.inchesToPoints(0.5f);

	// PADDING constants
	public static final float X_PADDING = 0;
	public static final float Y_PADDING = 10;
	public static final float LINE_SPACING = Utilities.inchesToPoints(0.15f);

	// TEMPLATE A constants
	public static final float TEMPLATE_A_TABLE_WIDTH = 600f;
	public static final float[] TEMPLATE_A_CUST_REF_TAB_DIM = new float[] { 2,
			1, 1 };
	public static final float[] TEMPLATE_A_INV_DET_TAB_DIM = new float[] { 1f,
			3.5f, 1f, 1f, 1f };
	public static final float[] TEMPLATE_A_INV_DET_FOOTER_DIM = new float[] {
			5.5f, 1f, 1f };
}
