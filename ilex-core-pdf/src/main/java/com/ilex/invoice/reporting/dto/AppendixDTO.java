package com.ilex.invoice.reporting.dto;

import java.io.Serializable;
import java.util.Date;

public class AppendixDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5546657406170958115L;
	private String title;
	private Date date;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
