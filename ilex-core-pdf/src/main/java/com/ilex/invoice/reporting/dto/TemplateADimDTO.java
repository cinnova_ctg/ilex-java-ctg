package com.ilex.invoice.reporting.dto;

import java.io.Serializable;

public class TemplateADimDTO implements Serializable {
	/**
	 * This class stores Template A dimensions
	 */
	private static final long serialVersionUID = -2754529955192104804L;
	private SectionDimDTO repHeaderFirstPage = new SectionDimDTO();
	private SectionDimDTO repHeaderOtherPages = new SectionDimDTO();
	private SectionDimDTO invAddress = new SectionDimDTO();
	private SectionDimDTO custReference = new SectionDimDTO();
	private SectionDimDTO invDetail = new SectionDimDTO();
	private SectionDimDTO invDetailFooter = new SectionDimDTO();

	public SectionDimDTO getRepHeaderFirstPage() {
		return repHeaderFirstPage;
	}

	public void setRepHeaderFirstPage(SectionDimDTO reportHeaderTable) {
		this.repHeaderFirstPage = reportHeaderTable;
	}

	public SectionDimDTO getInvAddress() {
		return invAddress;
	}

	public void setInvAddress(SectionDimDTO invAddressTable) {
		this.invAddress = invAddressTable;
	}

	public SectionDimDTO getCustReference() {
		return custReference;
	}

	public void setCustReference(SectionDimDTO custRefTable) {
		this.custReference = custRefTable;
	}

	public SectionDimDTO getInvDetail() {
		return invDetail;
	}

	public void setInvDetail(SectionDimDTO invDetailTable) {
		this.invDetail = invDetailTable;
	}

	public void setInvDetailFooter(SectionDimDTO reportFooterTable) {
		this.invDetailFooter = reportFooterTable;
	}

	public SectionDimDTO getInvDetailFooter() {
		return invDetailFooter;
	}

	public void setRepHeaderOtherPages(SectionDimDTO repHeaderOtherPages) {
		this.repHeaderOtherPages = repHeaderOtherPages;
	}

	public SectionDimDTO getRepHeaderOtherPages() {
		return repHeaderOtherPages;
	}

}