package com.ilex.invoice.reporting.dto;

import java.io.Serializable;

public class SectionDimDTO implements Serializable {

	/**
	 * This class stores invoice sections dimensions
	 */
	private static final long serialVersionUID = 1L;
	private float height;
	private float width;
	private float yline;

	public void setHeight(float height) {
		this.height = height;
	}

	public float getHeight() {
		return height;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getWidth() {
		return width;
	}

	public void setYline(float yLine) {
		this.yline = yLine;
	}

	public float getYline() {
		return yline;
	}
}
