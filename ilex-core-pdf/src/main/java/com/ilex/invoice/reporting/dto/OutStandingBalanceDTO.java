package com.ilex.invoice.reporting.dto;

import java.io.Serializable;

public class OutStandingBalanceDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -541994737042547006L;
	private String range;
	private double value;

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

}
