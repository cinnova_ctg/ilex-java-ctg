package com.ilex.invoice.reporting.dto;

import java.io.Serializable;

public class InvoiceDetailsDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4948758452185292054L;
	private float quantity;
	private String description;
	private float discount;
	private double unitPrice;
	private double extendedPrice;

	public float getQuantity() {
		return quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity.floatValue();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getDiscount() {
		return discount;
	}

	public void setDiscount(float discount) {
		this.discount = discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount.floatValue();
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public double getExtendedPrice() {
		return extendedPrice;
	}

	public void setExtendedPrice(double extendedPrice) {
		this.extendedPrice = extendedPrice;
	}

}
