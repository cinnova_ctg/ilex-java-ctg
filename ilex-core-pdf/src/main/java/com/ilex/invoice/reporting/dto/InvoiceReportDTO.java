package com.ilex.invoice.reporting.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class InvoiceReportDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2650536888552666760L;
	private String headerLogo;
	private AddressDTO headerAddressDTO;
	private String invoiceNumber;
	private Date invoiceDate;
	private AddressDTO billToAddressDTO;
	private AddressDTO shipToAddressDTO;
	private String customerReferenceName;
	private String customerReferenceValue;
	private String paymentTerms;
	private Date offsiteDate;
	private List<InvoiceDetailsDTO> invoiceDetailsDTOs;
	private String invoiceRemarks;
	private HyperLinkDTO invoiceHyperLinkDTO;
	private AppendixDTO appendixDTO;
	private List<OutStandingBalanceDTO> outStandingBalanceDTO;
	private double subTotal;
	private double tax;
	private double total;

	public String getHeaderLogo() {
		return headerLogo;
	}

	public void setHeaderLogo(String contingentLogo) {
		this.headerLogo = contingentLogo;
	}

	public AddressDTO getHeaderAddressDTO() {
		return headerAddressDTO;
	}

	public void setHeaderAddressDTO(AddressDTO contingentAddressDTO) {
		this.headerAddressDTO = contingentAddressDTO;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getOffsiteDate() {
		return offsiteDate;
	}

	public void setOffsiteDate(Date offsiteDate) {
		this.offsiteDate = offsiteDate;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public AddressDTO getBillToAddressDTO() {
		return billToAddressDTO;
	}

	public void setBillToAddressDTO(AddressDTO billToAddressDTO) {
		this.billToAddressDTO = billToAddressDTO;
	}

	public AddressDTO getShipToAddressDTO() {
		return shipToAddressDTO;
	}

	public void setShipToAddressDTO(AddressDTO shipToAddressDTO) {
		this.shipToAddressDTO = shipToAddressDTO;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public List<InvoiceDetailsDTO> getInvoiceDetailsDTOs() {
		return invoiceDetailsDTOs;
	}

	public void setInvoiceDetailsDTOs(List<InvoiceDetailsDTO> invoiceDetailsDTOs) {
		this.invoiceDetailsDTOs = invoiceDetailsDTOs;
	}

	public String getInvoiceRemarks() {
		return invoiceRemarks;
	}

	public void setInvoiceRemarks(String invoiceRemarks) {
		this.invoiceRemarks = invoiceRemarks;
	}

	public HyperLinkDTO getInvoiceHyperLinkDTO() {
		return invoiceHyperLinkDTO;
	}

	public void setInvoiceHyperLinkDTO(HyperLinkDTO invoiceHyperLinkDTO) {
		this.invoiceHyperLinkDTO = invoiceHyperLinkDTO;
	}

	public AppendixDTO getAppendixDTO() {
		return appendixDTO;
	}

	public void setAppendixDTO(AppendixDTO appendixDTO) {
		this.appendixDTO = appendixDTO;
	}

	public List<OutStandingBalanceDTO> getOutStandingBalanceDTO() {
		return outStandingBalanceDTO;
	}

	public void setOutStandingBalanceDTO(
			List<OutStandingBalanceDTO> outStandingBalanceDTO) {
		this.outStandingBalanceDTO = outStandingBalanceDTO;
	}

	public double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getCustomerReferenceName() {
		return customerReferenceName;
	}

	public void setCustomerReferenceName(String customerReferenceName) {
		this.customerReferenceName = customerReferenceName;
	}

	public String getCustomerReferenceValue() {
		return customerReferenceValue;
	}

	public void setCustomerReferenceValue(String customerReferenceValue) {
		this.customerReferenceValue = customerReferenceValue;
	}

}
