package com.ilex.report.pdf.template;

import java.text.SimpleDateFormat;

import com.ilex.invoice.reporting.common.IlexReportPDFConstants;
import com.ilex.invoice.reporting.dto.InvoiceReportDTO;
import com.ilex.invoice.reporting.dto.OutStandingBalanceDTO;
import com.ilex.invoice.reporting.dto.TemplateADimDTO;
import com.ilex.report.pdf.helper.PDFUtils;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mind.fw.util.PropertyFileUtil;

/**
 * Class to generate TemplateA Invoice Detail Footer.
 */
class PDFTemplateAInvDetailFooter {

	/**
	 * Prepares invoice detail footer.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @param document
	 *            the document
	 * @param PdfWriter
	 *            the writer
	 * @param templateADimDTO
	 *            the template a dim dto
	 * @throws DocumentException
	 *             the document exception
	 */
	public void prepareInvDetailFooter(InvoiceReportDTO invoiceReportDTO,
			Document document, PdfWriter writer, TemplateADimDTO templateADimDTO)
			throws DocumentException {

		PdfPTable invDetailBottomTable = getReportFooterTable(invoiceReportDTO,
				document);

		float yLine = templateADimDTO.getInvDetail().getYline();

		invDetailBottomTable.writeSelectedRows(0, -1,
				document.left(IlexReportPDFConstants.X_PADDING), yLine
						+ document.bottom(), writer.getDirectContent());

	}

	/**
	 * Gets the report footer table.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @param document
	 *            the document
	 * @return the report footer table
	 */
	private PdfPTable getReportFooterTable(InvoiceReportDTO invoiceReportDTO,
			Document document) {
		PdfPTable invDetailBottomTable = new PdfPTable(
				IlexReportPDFConstants.TEMPLATE_A_INV_DET_FOOTER_DIM);

		float tableWidth = document.right() - document.left() - 2
				* IlexReportPDFConstants.X_PADDING;

		invDetailBottomTable.setTotalWidth(tableWidth);
		invDetailBottomTable.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell leftCell = getLeftCell(invoiceReportDTO);
		leftCell.setBorderWidthLeft(0);
		leftCell.setPaddingLeft(0);

		PdfPCell middleCell = getMiddleCell(invoiceReportDTO);

		PdfPCell rightCell = getRightCell(invoiceReportDTO);

		invDetailBottomTable.addCell(leftCell);
		invDetailBottomTable.addCell(middleCell);
		invDetailBottomTable.addCell(rightCell);
		return invDetailBottomTable;
	}

	/**
	 * Gets the left cell of the footer.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @return the left cell
	 */
	private PdfPCell getLeftCell(InvoiceReportDTO invoiceReportDTO) {
		PdfPCell leftCell = new PdfPCell();
		leftCell.setBorderWidthLeft(0);
		// leftCell.setPadding(0);;

		PDFUtils.setDefaultCellLayout(leftCell);

		PdfPTable leftTable = new PdfPTable(new float[] { 5.5f });
		leftTable.setWidthPercentage(100f);

		PdfPCell referenceCell = getReferenceCell(invoiceReportDTO);
		referenceCell.setBorderWidthLeft(0);
		referenceCell.setPadding(0);
		leftTable.addCell(referenceCell);

		leftCell.addElement(leftTable);

		return leftCell;

	}

	/**
	 * Gets the reference cell.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @return the reference cell
	 */
	private PdfPCell getReferenceCell(InvoiceReportDTO invoiceReportDTO) {

		PdfPCell appendixCell = new PdfPCell();
		appendixCell.setBorderWidthRight(0);
		appendixCell.setBorderWidthLeft(0);
		appendixCell.setBorderWidthBottom(0);
		appendixCell.setBorderWidthTop(0);
		appendixCell.setPaddingTop(0);
		appendixCell.setPaddingLeft(0);

		PdfPTable nestedAppendix = new PdfPTable(new float[] { 0.64f, 1, 1 });
		nestedAppendix.setWidthPercentage(50f);
		nestedAppendix.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell reference = new PdfPCell(new Phrase(
				PropertyFileUtil.getAppProperty("ilex.pdf.footer.reference"),
				IlexReportPDFConstants.HEADER_FOOTER_STANDARD_BOLD_FONT));
		reference.setBorder(0);
		reference.setPaddingLeft(0);
		reference.setPaddingRight(0);

		SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy");
		String appendixDate = "";
		if (invoiceReportDTO.getAppendixDTO().getDate() != null) {
			appendixDate = ft.format(invoiceReportDTO.getAppendixDTO()
					.getDate());
		}
		PdfPCell referenceAppendix = new PdfPCell(new Phrase(invoiceReportDTO
				.getAppendixDTO().getTitle() + " " + appendixDate,
				IlexReportPDFConstants.STANDARD_FONT));
		referenceAppendix.setColspan(2);
		referenceAppendix.setBorder(0);
		referenceAppendix.setPaddingLeft(0);

		referenceAppendix.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell outstandingBal = new PdfPCell(
				new Phrase(
						PropertyFileUtil
								.getAppProperty("ilex.pdf.footer.outStandingBalance"),
						IlexReportPDFConstants.HEADER_FOOTER_STANDARD_BOLD_BLUE_FONT));
		outstandingBal.setColspan(3);
		outstandingBal.setBorder(0);
		outstandingBal.setPaddingLeft(0);

		nestedAppendix.addCell(reference);
		nestedAppendix.addCell(referenceAppendix);
		nestedAppendix.addCell(outstandingBal);

		PdfPTable outstandingBalance = new PdfPTable(new float[] { 1, 1, 1 });
		outstandingBalance.setWidthPercentage(58);
		outstandingBalance.setHorizontalAlignment(Element.ALIGN_LEFT);

		for (OutStandingBalanceDTO outStandingBalanceDTO : invoiceReportDTO
				.getOutStandingBalanceDTO()) {
			PdfPCell outstanRange = new PdfPCell(new Phrase(
					outStandingBalanceDTO.getRange(),
					IlexReportPDFConstants.HEADER_FOOTER_STANDARD_BLUE_FONT));
			outstanRange.setBorder(0);
			outstanRange.setPaddingLeft(0);
			outstandingBalance.addCell(outstanRange);

		}
		if (invoiceReportDTO.getOutStandingBalanceDTO().size() == 2) {
			PdfPCell temp = new PdfPCell(new Phrase(""));
			temp.setBorder(0);
			temp.setPaddingLeft(0);
			outstandingBalance.addCell(temp);
		}
		if (invoiceReportDTO.getOutStandingBalanceDTO().size() == 1) {
			PdfPCell temp = new PdfPCell(new Phrase(""));
			temp.setBorder(0);
			temp.setPaddingLeft(0);
			outstandingBalance.addCell(temp);
			PdfPCell temp1 = new PdfPCell(new Phrase(""));
			temp1.setBorder(0);
			temp1.setPaddingLeft(0);
			outstandingBalance.addCell(temp);
		}

		for (OutStandingBalanceDTO outStandingBalanceDTO : invoiceReportDTO
				.getOutStandingBalanceDTO()) {
			PdfPCell outstanValue = new PdfPCell(new Phrase(
					String.valueOf(outStandingBalanceDTO.getValue()),
					IlexReportPDFConstants.STANDARD_FONT));
			outstanValue.setBorder(0);
			outstanValue.setPaddingLeft(0);
			outstandingBalance.addCell(outstanValue);

		}
		if (invoiceReportDTO.getOutStandingBalanceDTO().size() == 2) {
			PdfPCell temp1 = new PdfPCell(new Phrase(""));
			temp1.setBorder(0);
			temp1.setPaddingLeft(0);
			outstandingBalance.addCell(temp1);
		}
		if (invoiceReportDTO.getOutStandingBalanceDTO().size() == 1) {
			PdfPCell temp1 = new PdfPCell(new Phrase(""));
			temp1.setBorder(0);
			temp1.setPaddingLeft(0);
			nestedAppendix.addCell(temp1);
			PdfPCell temp = new PdfPCell(new Phrase(""));
			temp.setBorder(0);
			temp.setPaddingLeft(0);
			outstandingBalance.addCell(temp1);
		}

		appendixCell.addElement(nestedAppendix);
		appendixCell.addElement(outstandingBalance);
		return appendixCell;
	}

	/**
	 * Gets the right cell of the footer.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @return the right cell
	 */
	private PdfPCell getRightCell(InvoiceReportDTO invoiceReportDTO) {
		PdfPCell rightCell = new PdfPCell();
		PDFUtils.setDefaultCellLayout(rightCell);
		rightCell.enableBorderSide(Rectangle.RIGHT);
		rightCell.enableBorderSide(Rectangle.LEFT);

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPCell subTotalVal = new PdfPCell(new Phrase(String.format("%.2f",
				invoiceReportDTO.getSubTotal()),
				IlexReportPDFConstants.STANDARD_FONT));
		PDFUtils.setDefaultCellLayout(subTotalVal);
		subTotalVal.setHorizontalAlignment(Element.ALIGN_RIGHT);
		subTotalVal.setPaddingRight(10f);
		// subTotalVal.setPaddingTop(6f);

		PdfPCell taxVal = new PdfPCell(new Phrase(String.format("%.2f",
				invoiceReportDTO.getTax()),
				IlexReportPDFConstants.STANDARD_FONT));
		PDFUtils.setDefaultCellLayout(taxVal);
		taxVal.setHorizontalAlignment(Element.ALIGN_RIGHT);
		taxVal.setPaddingRight(10f);
		taxVal.setPaddingTop(6f);

		PdfPCell totalVal = new PdfPCell(new Phrase(String.format("%.2f",
				invoiceReportDTO.getTotal()),
				IlexReportPDFConstants.STANDARD_FONT));
		PDFUtils.setDefaultCellLayout(totalVal);
		totalVal.setHorizontalAlignment(Element.ALIGN_RIGHT);
		totalVal.setPaddingRight(10f);
		totalVal.setPaddingTop(6f);

		rightTable.addCell(subTotalVal);
		rightTable.addCell(taxVal);
		rightTable.addCell(totalVal);

		rightCell.addElement(rightTable);

		return rightCell;
	}

	/**
	 * Gets the middle Total Section Label cell.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @return the middle cell
	 */
	private PdfPCell getMiddleCell(InvoiceReportDTO invoiceReportDTO) {
		PdfPCell middleCell = new PdfPCell();

		PDFUtils.setDefaultCellLayout(middleCell);

		PdfPTable middleTable = new PdfPTable(new float[] { 1f });
		middleTable.setWidthPercentage(100f);

		PdfPCell subTotal = new PdfPCell(new Phrase(
				PropertyFileUtil.getAppProperty("ilex.pdf.footer.subTotal"),
				IlexReportPDFConstants.HEADER_FOOTER_STANDARD_BOLD_BLUE_FONT));
		PDFUtils.setDefaultCellLayout(subTotal);
		subTotal.setPaddingLeft(20);

		PdfPCell tax = new PdfPCell(new Phrase(
				PropertyFileUtil.getAppProperty("ilex.pdf.footer.tax"),
				IlexReportPDFConstants.HEADER_FOOTER_STANDARD_BOLD_BLUE_FONT));
		PDFUtils.setDefaultCellLayout(tax);
		tax.setPaddingLeft(20);
		tax.setPaddingTop(6f);

		PdfPCell total = new PdfPCell(new Phrase(
				PropertyFileUtil.getAppProperty("ilex.pdf.footer.total"),
				IlexReportPDFConstants.HEADER_FOOTER_STANDARD_BOLD_BLUE_FONT));
		PDFUtils.setDefaultCellLayout(total);
		total.setPaddingLeft(20);
		total.setPaddingTop(6f);

		middleTable.addCell(subTotal);
		middleTable.addCell(tax);
		middleTable.addCell(total);
		middleCell.addElement(middleTable);

		return middleCell;

	}

	/**
	 * Gets the height.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @param document
	 *            the document
	 * @return the height
	 */
	public float getHeight(InvoiceReportDTO invoiceReportDTO, Document document) {
		PdfPTable invDetailBottomTable = getReportFooterTable(invoiceReportDTO,
				document);

		return invDetailBottomTable.calculateHeights();
	}

}
