package com.ilex.report.pdf.template;

import com.ilex.invoice.reporting.common.IlexReportPDFConstants;
import com.ilex.invoice.reporting.dto.InvoiceReportDTO;
import com.ilex.invoice.reporting.dto.TemplateADimDTO;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mind.fw.util.PropertyFileUtil;

/**
 * Class to generate TemplateA Invoice Address.
 */
class PDFTemplateAInvAddress {

	/**
	 * Prepares invoice Bill To & Ship To address.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @param document
	 *            the document
	 * @param writer
	 *            the writer
	 * @param templateADimDTO
	 *            the template a dim dto
	 */
	public void prepareInvAddress(InvoiceReportDTO invoiceReportDTO,
			Document document, PdfWriter writer, TemplateADimDTO templateADimDTO) {

		Font textFont = IlexReportPDFConstants.STANDARD_FONT;
		Font billADDHeadFont = IlexReportPDFConstants.HEADER_BLUE_FONT;

		/* billToShipToData table */
		PdfPTable billToShipToData = new PdfPTable(2);
		billToShipToData.setWidthPercentage(90);
		billToShipToData.setHorizontalAlignment(Element.ALIGN_CENTER);
		billToShipToData
				.setTotalWidth(IlexReportPDFConstants.TEMPLATE_A_TABLE_WIDTH);
		billToShipToData.setLockedWidth(true);

		/* billToLabel */
		PdfPCell billToLabel = new PdfPCell(new Phrase(
				PropertyFileUtil.getAppProperty("ilex.pdf.billTo"),
				billADDHeadFont));
		billToLabel.setBorder(0);
		billToLabel.setRowspan(1);
		billToLabel.setPaddingLeft(4f);
		billToLabel.setFixedHeight(25f);

		/* shipToLabel */
		PdfPCell shipToLabel = new PdfPCell(new Phrase(
				PropertyFileUtil.getAppProperty("ilex.pdf.shipTo"),
				billADDHeadFont));
		shipToLabel.setBorder(0);
		shipToLabel.setPaddingLeft(63f);
		shipToLabel.setRowspan(1);
		shipToLabel.setFixedHeight(25f);

		billToShipToData.addCell(billToLabel);
		billToShipToData.addCell(shipToLabel);

		/* billTo */
		PdfPCell billTo = new PdfPCell();
		billTo.setHorizontalAlignment(Element.ALIGN_LEFT);

		/* shipTo */
		PdfPCell shipTo = new PdfPCell();
		shipTo.setPaddingLeft(49f);
		billTo.setBorder(0);
		shipTo.setBorder(0);

		PdfPTable billToNestedTable = new PdfPTable(new float[] { 2.5f });
		billToNestedTable.setWidthPercentage(90);
		billToNestedTable.setHorizontalAlignment(Element.ALIGN_LEFT);

		String billCityState = invoiceReportDTO.getBillToAddressDTO().getCity()
				+ ", " + invoiceReportDTO.getBillToAddressDTO().getState()
				+ ", " + invoiceReportDTO.getBillToAddressDTO().getZipCode()
				+ ", " + invoiceReportDTO.getBillToAddressDTO().getCountry();

		PdfPCell billCompany = new PdfPCell(new Phrase(invoiceReportDTO
				.getBillToAddressDTO().getName(), textFont));
		PdfPCell billaddress1 = new PdfPCell(new Phrase(invoiceReportDTO
				.getBillToAddressDTO().getAddressLine1(), textFont));
		PdfPCell billaddress2 = new PdfPCell(new Phrase(invoiceReportDTO
				.getBillToAddressDTO().getAddressLine2(), textFont));
		PdfPCell billCity = new PdfPCell(new Phrase(billCityState, textFont));
		billCompany.setBorder(0);
		billaddress1.setBorder(0);
		billaddress2.setBorder(0);
		billCity.setBorder(0);

		billToNestedTable.addCell(billCompany);
		billToNestedTable.addCell(billaddress1);
		billToNestedTable.addCell(billaddress2);
		billToNestedTable.addCell(billCity);
		billTo.addElement(billToNestedTable);

		PdfPTable shipToNestedTable = new PdfPTable(new float[] { 2.5f });
		shipToNestedTable.setWidthPercentage(90);

		String shipCityState = invoiceReportDTO.getShipToAddressDTO().getCity()
				+ ", " + invoiceReportDTO.getShipToAddressDTO().getState()
				+ ", " + invoiceReportDTO.getShipToAddressDTO().getZipCode()
				+ ", " + invoiceReportDTO.getShipToAddressDTO().getCountry();

		PdfPCell shipCompany = new PdfPCell(new Phrase(invoiceReportDTO
				.getShipToAddressDTO().getName(), textFont));
		PdfPCell shipSite = new PdfPCell(new Phrase("site", textFont));
		PdfPCell shipaddress1 = new PdfPCell(new Phrase(invoiceReportDTO
				.getShipToAddressDTO().getAddressLine1(), textFont));
		PdfPCell shipaddress2 = new PdfPCell(new Phrase(invoiceReportDTO
				.getShipToAddressDTO().getAddressLine2(), textFont));
		PdfPCell shipCity = new PdfPCell(new Phrase(shipCityState, textFont));
		shipCompany.setBorder(0);
		shipSite.setBorder(0);
		shipaddress1.setBorder(0);
		shipaddress2.setBorder(0);
		shipCity.setBorder(0);
		shipToNestedTable.addCell(shipCompany);
		shipToNestedTable.addCell(shipSite);
		shipToNestedTable.addCell(shipaddress1);
		shipToNestedTable.addCell(shipaddress2);
		shipToNestedTable.addCell(shipCity);
		shipTo.addElement(shipToNestedTable);
		billToShipToData.addCell(billTo);
		billToShipToData.addCell(shipTo);
		billToShipToData
				.writeSelectedRows(
						0,
						-1,
						document.left(IlexReportPDFConstants.X_PADDING),
						document.top()
								- (templateADimDTO.getRepHeaderFirstPage()
										.getHeight() + IlexReportPDFConstants.Y_PADDING),
						writer.getDirectContent());

		templateADimDTO.getInvAddress().setHeight(
				billToShipToData.getTotalHeight());

	}
}
