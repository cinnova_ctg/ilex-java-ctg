package com.ilex.report.pdf.helper;

import java.io.File;

import org.apache.commons.lang.RandomStringUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPCell;

// TODO: Auto-generated Javadoc
/**
 * The Class PDFUtils.
 */
public class PDFUtils {

	/**
	 * Draws a rectangle.
	 * 
	 * @param content
	 *            the direct content layer
	 * @param x
	 *            -axis the x
	 * @param y
	 *            -axis the y
	 * @param width
	 *            the width of the rectangle
	 * @param height
	 *            the height of the rectangle
	 */
	public static void drawRectangle(PdfContentByte content, float x, float y,
			float width, float height) {
		drawRectangle(content, x, y, width, height, BaseColor.BLACK);
	}

	/**
	 * Draws rectangle.
	 * 
	 * @param content
	 *            the content
	 * @param x
	 *            -axis the x
	 * @param y
	 *            -axis the y
	 * @param width
	 *            the width
	 */
	public static void drawRectangle(PdfContentByte content, float x, float y,
			float width) {
		float height = 0.50f;
		drawRectangle(content, x, y, width, height, BaseColor.BLACK);
	}

	/**
	 * Draws rectangle.
	 * 
	 * @param content
	 *            the content
	 * @param x
	 *            -axis the x
	 * @param y
	 *            -axis the y
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 * @param fillColor
	 *            the fill color
	 */
	public static void drawRectangle(PdfContentByte content, float x, float y,
			float width, float height, BaseColor fillColor) {
		content.saveState();
		PdfGState state = new PdfGState();
		content.setGState(state);
		content.setColorFill(fillColor);
		content.setColorStroke(fillColor);
		content.setLineWidth(height);
		content.rectangle(x, y, width, 0);
		content.fillStroke();
		content.restoreState();
	}

	/**
	 * Sets the default cell layout.
	 * 
	 * @param pdfCell
	 *            the new default cell layout
	 */
	public static void setDefaultCellLayout(PdfPCell pdfCell) {
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setBorderWidth(0.5f);
		pdfCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pdfCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		pdfCell.setUseAscender(true);
		pdfCell.setUseDescender(true);
		pdfCell.setUseBorderPadding(true);
	}

	/**
	 * Gets the temporary file.
	 * 
	 * @return the temporary file
	 */
	public static File getTempFile() {
		String ext = "pdf";
		String dirName = System.getProperty("ilex.temp.path");
		if (dirName == null || "".equals(dirName.trim())) {
			dirName = System.getProperty("user.home") + "/ilex_temp";
		}
		File dir = new File(dirName);
		// Make the directory If the File Does not Exists
		if (!dir.exists()) {
			dir.mkdir();
		}

		String name = String.format("%s.%s",
				RandomStringUtils.randomAlphanumeric(8), ext);
		name = "ilex_pdf_" + name;
		File file = new File(dir, name);
		// Delete the File On Exit
		file.deleteOnExit();
		return file;
	}
}
