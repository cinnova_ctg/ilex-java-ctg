package com.ilex.report.pdf.template;

import java.io.FileNotFoundException;

import com.ilex.invoice.reporting.common.IlexReportPDFConstants;
import com.ilex.invoice.reporting.dto.InvoiceDetailsDTO;
import com.ilex.invoice.reporting.dto.InvoiceReportDTO;
import com.ilex.invoice.reporting.dto.TemplateADimDTO;
import com.ilex.report.pdf.helper.PDFUtils;
import com.itextpdf.text.Anchor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mind.fw.util.PropertyFileUtil;

/**
 * Class to generate TemplateA Invoice Detail.
 */
class PDFTemplateAInvDetail {

	/**
	 * Prepares invoice detail table with invoice data.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @param document
	 *            the document
	 * @param writer
	 *            the writer
	 * @param templateADimDTO
	 *            the template a dim dto
	 * @throws DocumentException
	 *             the document exception
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public void prepareInvoiceDetailTable(InvoiceReportDTO invoiceReportDTO,
			Document document, PdfWriter writer, TemplateADimDTO templateADimDTO)
			throws DocumentException, FileNotFoundException {
		float tableWidth = document.right() - document.left() - 2
				* IlexReportPDFConstants.X_PADDING;

		PdfPTable invDetailTabel = getInvDetailTable(invoiceReportDTO);

		ColumnText column = new ColumnText(writer.getDirectContent());
		column.addElement(invDetailTabel);

		int count = 0;
		float height = 0;
		float footerHeight = 0;
		float tableFooterHeight = getFooterRow(invoiceReportDTO, document, 0)
				.calculateHeights();
		float rptFooterHeigth = templateADimDTO.getInvDetailFooter()
				.getHeight();
		int status = ColumnText.START_COLUMN;
		// render the column as long as it has content
		while (ColumnText.hasMoreText(status)) {
			// add the top-level header to each new page
			if (count == 0) {
				height = templateADimDTO.getRepHeaderFirstPage().getHeight()
						+ templateADimDTO.getInvAddress().getHeight()
						+ templateADimDTO.getCustReference().getHeight()
						+ (2 * IlexReportPDFConstants.Y_PADDING);
				footerHeight = tableFooterHeight + rptFooterHeigth;
			} else {
				height = templateADimDTO.getRepHeaderOtherPages().getHeight()
						+ IlexReportPDFConstants.Y_PADDING * 3;
				footerHeight = tableFooterHeight;
			}
			// set the dimensions of the current column

			column.setSimpleColumn(
					document.left(IlexReportPDFConstants.X_PADDING),
					document.bottom() + footerHeight,
					document.right(IlexReportPDFConstants.X_PADDING),
					document.top() - height, 0f, Element.ALIGN_CENTER);
			// render as much content as possible
			column.setFilledWidth(tableWidth);
			status = column.go();

			// Set InvoiceDetail table's Y position
			templateADimDTO.getInvDetail().setYline(column.getYLine());

			// Add table footer
			addTableFooter(invoiceReportDTO, writer, document, templateADimDTO,
					footerHeight);

			if (ColumnText.hasMoreText(status)) {
				count++;
				document.newPage();
			}
		}
	}

	/**
	 * Gets the invoice detail data table.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @return the invoice detail pdf table
	 */
	private PdfPTable getInvDetailTable(InvoiceReportDTO invoiceReportDTO) {

		PdfPTable nested = new PdfPTable(
				IlexReportPDFConstants.TEMPLATE_A_INV_DET_TAB_DIM);
		setHeaderRow(nested, invoiceReportDTO);

		for (InvoiceDetailsDTO invoiceDetailsDTO : invoiceReportDTO
				.getInvoiceDetailsDTOs()) {

			PdfPCell quantityValue = new PdfPCell(new Phrase(
					String.valueOf(invoiceDetailsDTO.getQuantity()),
					IlexReportPDFConstants.STANDARD_FONT));

			PDFUtils.setDefaultCellLayout(quantityValue);
			quantityValue.enableBorderSide(4);
			quantityValue.setPaddingRight(10f);
			quantityValue.setHorizontalAlignment(Element.ALIGN_RIGHT);
			quantityValue.setVerticalAlignment(Element.ALIGN_TOP);

			PdfPCell descValue = new PdfPCell();
			// Check if invoice detail is of NetMedX long format type
			if (invoiceDetailsDTO.getDescription().indexOf("Details") != -1) {
				String netMedXLongDesc = invoiceDetailsDTO.getDescription();
				Phrase descPhrase = new Phrase(
						IlexReportPDFConstants.LINE_SPACING);
				descPhrase.add(new Chunk(netMedXLongDesc.substring(0,
						netMedXLongDesc.indexOf("Details")),
						IlexReportPDFConstants.STANDARD_FONT));
				descPhrase.add(new Chunk("Details:",
						IlexReportPDFConstants.STANDARD_BOLD_FONT));
				descPhrase.add(new Chunk(netMedXLongDesc
						.substring(netMedXLongDesc.indexOf("Details:") + 8),
						IlexReportPDFConstants.STANDARD_FONT));

				descValue.addElement(descPhrase);
			} else {
				descValue
						.addElement(new Phrase(invoiceDetailsDTO
								.getDescription(),
								IlexReportPDFConstants.STANDARD_FONT));
			}
			PDFUtils.setDefaultCellLayout(descValue);
			descValue.enableBorderSide(Rectangle.RIGHT);
			descValue.enableBorderSide(Rectangle.LEFT);
			descValue.setPaddingLeft(10f);
			descValue.setPaddingRight(10f);

			PdfPCell idiscountValue = new PdfPCell(new Phrase(String.format(
					"%.2f", invoiceDetailsDTO.getDiscount()),
					IlexReportPDFConstants.STANDARD_FONT));
			PDFUtils.setDefaultCellLayout(idiscountValue);
			idiscountValue.enableBorderSide(Rectangle.RIGHT);
			idiscountValue.setHorizontalAlignment(Element.ALIGN_RIGHT);
			idiscountValue.setVerticalAlignment(Element.ALIGN_TOP);
			idiscountValue.setPaddingRight(10f);

			PdfPCell iUnitPriceVal = new PdfPCell(new Phrase(String.format(
					"$%.2f", invoiceDetailsDTO.getUnitPrice()),
					IlexReportPDFConstants.STANDARD_FONT));
			PDFUtils.setDefaultCellLayout(iUnitPriceVal);
			iUnitPriceVal.enableBorderSide(Rectangle.RIGHT);
			iUnitPriceVal.setHorizontalAlignment(Element.ALIGN_RIGHT);
			iUnitPriceVal.setVerticalAlignment(Element.ALIGN_TOP);
			iUnitPriceVal.setPaddingRight(10f);

			PdfPCell iExtendedPriceVal = new PdfPCell(new Phrase(String.format(
					"$%.2f", invoiceDetailsDTO.getExtendedPrice()),
					IlexReportPDFConstants.STANDARD_FONT));
			PDFUtils.setDefaultCellLayout(iExtendedPriceVal);
			iExtendedPriceVal.enableBorderSide(Rectangle.RIGHT);
			iExtendedPriceVal.setHorizontalAlignment(Element.ALIGN_RIGHT);
			iExtendedPriceVal.setVerticalAlignment(Element.ALIGN_TOP);
			iExtendedPriceVal.setPaddingRight(10f);

			nested.addCell(quantityValue);
			nested.addCell(descValue);
			nested.addCell(idiscountValue);
			nested.addCell(iUnitPriceVal);
			nested.addCell(iExtendedPriceVal);

		}
		return nested;
	}

	/**
	 * Sets the header row.
	 * 
	 * @param invDetailTable
	 *            the nested
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 */
	private void setHeaderRow(PdfPTable invDetailTable,
			InvoiceReportDTO invoiceReportDTO) {
		PdfPCell quantityLabel = new PdfPCell(
				new Phrase(PropertyFileUtil
						.getAppProperty("ilex.pdf.invoiceDetails.quantity"),
						IlexReportPDFConstants.TABLE_STANDARD_HEADER_WHITE_FONT));
		PDFUtils.setDefaultCellLayout(quantityLabel);
		quantityLabel.enableBorderSide(Rectangle.LEFT);
		quantityLabel.enableBorderSide(Rectangle.RIGHT);
		quantityLabel.enableBorderSide(Rectangle.TOP);
		quantityLabel.setHorizontalAlignment(Element.ALIGN_CENTER);
		quantityLabel
				.setBackgroundColor(IlexReportPDFConstants.TABLE_STANDARD_HEADER_NAVYBLUE_COLOR);
		quantityLabel.setFixedHeight(25F);
		// quantityLabel.setPaddingTop(5f);
		quantityLabel.setVerticalAlignment(Element.ALIGN_MIDDLE);

		PdfPCell descriptionLabel = new PdfPCell(
				new Phrase(PropertyFileUtil
						.getAppProperty("ilex.pdf.invoiceDetails.description"),
						IlexReportPDFConstants.TABLE_STANDARD_HEADER_WHITE_FONT));
		PDFUtils.setDefaultCellLayout(descriptionLabel);
		descriptionLabel.enableBorderSide(Rectangle.RIGHT);
		descriptionLabel.enableBorderSide(Rectangle.TOP);
		descriptionLabel.setHorizontalAlignment(Element.ALIGN_CENTER);
		descriptionLabel
				.setBackgroundColor(IlexReportPDFConstants.TABLE_STANDARD_HEADER_NAVYBLUE_COLOR);
		descriptionLabel.setFixedHeight(25F);
		// descriptionLabel.setPaddingTop(5f);
		descriptionLabel.setVerticalAlignment(Element.ALIGN_MIDDLE);

		PdfPCell discountLabel = new PdfPCell(
				new Phrase(PropertyFileUtil
						.getAppProperty("ilex.pdf.invoiceDetails.discount"),
						IlexReportPDFConstants.TABLE_STANDARD_HEADER_WHITE_FONT));
		PDFUtils.setDefaultCellLayout(discountLabel);
		discountLabel.enableBorderSide(Rectangle.RIGHT);
		discountLabel.enableBorderSide(Rectangle.TOP);
		discountLabel.setHorizontalAlignment(Element.ALIGN_CENTER);
		discountLabel
				.setBackgroundColor(IlexReportPDFConstants.TABLE_STANDARD_HEADER_NAVYBLUE_COLOR);
		discountLabel.setFixedHeight(25F);
		// discountLabel.setPaddingTop(5f);
		discountLabel.setVerticalAlignment(Element.ALIGN_MIDDLE);

		PdfPCell unitPriceLabel = new PdfPCell(
				new Phrase(PropertyFileUtil
						.getAppProperty("ilex.pdf.invoiceDetails.unitPrice"),
						IlexReportPDFConstants.TABLE_STANDARD_HEADER_WHITE_FONT));
		PDFUtils.setDefaultCellLayout(unitPriceLabel);
		unitPriceLabel.enableBorderSide(Rectangle.RIGHT);
		unitPriceLabel.enableBorderSide(Rectangle.TOP);
		unitPriceLabel.setHorizontalAlignment(Element.ALIGN_CENTER);
		unitPriceLabel
				.setBackgroundColor(IlexReportPDFConstants.TABLE_STANDARD_HEADER_NAVYBLUE_COLOR);
		unitPriceLabel.setFixedHeight(25F);
		// unitPriceLabel.setPaddingTop(6f);
		unitPriceLabel.setVerticalAlignment(Element.ALIGN_MIDDLE);

		PdfPCell extendedPriceLabel = new PdfPCell(
				new Phrase(
						PropertyFileUtil
								.getAppProperty("ilex.pdf.invoiceDetails.extendedPrice"),
						IlexReportPDFConstants.TABLE_STANDARD_HEADER_WHITE_FONT));
		PDFUtils.setDefaultCellLayout(extendedPriceLabel);
		extendedPriceLabel.enableBorderSide(Rectangle.RIGHT);
		extendedPriceLabel.enableBorderSide(Rectangle.TOP);
		extendedPriceLabel.setHorizontalAlignment(Element.ALIGN_CENTER);
		extendedPriceLabel
				.setBackgroundColor(IlexReportPDFConstants.TABLE_STANDARD_HEADER_NAVYBLUE_COLOR);
		extendedPriceLabel.setFixedHeight(25F);
		// extendedPriceLabel.setPaddingTop(6f);
		extendedPriceLabel.setVerticalAlignment(Element.ALIGN_MIDDLE);

		invDetailTable.setWidthPercentage(100f);
		invDetailTable.setHorizontalAlignment(Element.ALIGN_CENTER);
		invDetailTable.addCell(quantityLabel);
		invDetailTable.addCell(descriptionLabel);
		invDetailTable.addCell(discountLabel);
		invDetailTable.addCell(unitPriceLabel);
		invDetailTable.addCell(extendedPriceLabel);

		this.setEmptyHeaderRow(invDetailTable);

		invDetailTable.setHeaderRows(2);
	}

	/**
	 * Gets table footer row.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @param document
	 *            the document
	 * @param height
	 *            height of invoice detail footer table. Set this value to 0 to
	 *            get set default height.
	 * @return the footer row
	 */
	private PdfPTable getFooterRow(InvoiceReportDTO invoiceReportDTO,
			Document document, float height) {
		PdfPTable nested = new PdfPTable(
				IlexReportPDFConstants.TEMPLATE_A_INV_DET_TAB_DIM);

		float tableWidth = document.right() - document.left() - 2
				* IlexReportPDFConstants.X_PADDING;

		nested.setTotalWidth(tableWidth);

		PdfPCell emptyCell = new PdfPCell();
		PDFUtils.setDefaultCellLayout(emptyCell);
		emptyCell.enableBorderSide(Rectangle.LEFT);
		emptyCell.enableBorderSide(Rectangle.RIGHT);
		emptyCell.enableBorderSide(Rectangle.BOTTOM);
		emptyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		if (height > 0) {
			emptyCell.setFixedHeight(height);
		}

		Paragraph hyperLinkPara = new Paragraph();
		hyperLinkPara.add(new Phrase(invoiceReportDTO.getInvoiceHyperLinkDTO()
				.getText() + "\n", IlexReportPDFConstants.STANDARD_FONT));

		Anchor anchor = new Anchor(invoiceReportDTO.getInvoiceHyperLinkDTO()
				.getAddress(), IlexReportPDFConstants.STANDARD_FONT);
		anchor.setReference(invoiceReportDTO.getInvoiceHyperLinkDTO()
				.getAddress());

		hyperLinkPara.add(anchor);

		PdfPCell hyperLinkCell = new PdfPCell(hyperLinkPara);
		PDFUtils.setDefaultCellLayout(hyperLinkCell);
		hyperLinkCell.setPaddingTop(5f);
		hyperLinkCell.setPaddingBottom(5f);
		hyperLinkCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
		hyperLinkCell.enableBorderSide(Rectangle.LEFT);
		hyperLinkCell.enableBorderSide(Rectangle.RIGHT);
		hyperLinkCell.enableBorderSide(Rectangle.BOTTOM);
		hyperLinkCell.setPaddingLeft(10f);
		hyperLinkCell.setPaddingRight(5f);
		if (height > 0) {
			emptyCell.setFixedHeight(height);
		}

		nested.addCell(emptyCell);
		nested.addCell(hyperLinkCell);
		nested.addCell(emptyCell);
		nested.addCell(emptyCell);
		nested.addCell(emptyCell);

		return nested;
	}

	/**
	 * Adds table footer. Call this method at end of table on each page.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @param writer
	 *            the writer
	 * @param document
	 *            the document
	 * @param templateADimDTO
	 *            the template a dim dto
	 * @param footerHeight
	 *            the height
	 */
	private void addTableFooter(InvoiceReportDTO invoiceReportDTO,
			PdfWriter writer, Document document,
			TemplateADimDTO templateADimDTO, float footerHeight) {

		float footerGap = templateADimDTO.getInvDetail().getYline()
				- templateADimDTO.getInvDetailFooter().getHeight();
		if (footerGap > footerHeight) {
			footerHeight = footerGap;
		}

		PdfPTable footerTable = getFooterRow(invoiceReportDTO, document,
				footerHeight);

		float yLine = templateADimDTO.getInvDetail().getYline();

		int PageNo = writer.getPageNumber();
		if (PageNo == 1) {
			footerTable.writeSelectedRows(0, -1,
					document.left(IlexReportPDFConstants.X_PADDING), yLine
							+ document.bottom(), writer.getDirectContent());

		}

		else {
			footerTable.writeSelectedRows(0, -1,
					document.left(IlexReportPDFConstants.X_PADDING), yLine,
					writer.getDirectContent());
		}

		// Reset InvoiceDetail table's Y position
		templateADimDTO.getInvDetail().setYline(
				templateADimDTO.getInvDetail().getYline() - footerHeight);

	}

	/**
	 * Sets the empty header row for vertical spacing.
	 * 
	 * @param invDetailTable
	 *            the new empty header row
	 */
	private void setEmptyHeaderRow(PdfPTable invDetailTable) {

		PdfPCell emptyCell = new PdfPCell();
		PDFUtils.setDefaultCellLayout(emptyCell);
		emptyCell.enableBorderSide(Rectangle.LEFT);
		emptyCell.enableBorderSide(Rectangle.RIGHT);
		emptyCell.enableBorderSide(Rectangle.TOP);
		emptyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		emptyCell.setFixedHeight(7f);

		invDetailTable.addCell(emptyCell);
		invDetailTable.addCell(emptyCell);
		invDetailTable.addCell(emptyCell);
		invDetailTable.addCell(emptyCell);
		invDetailTable.addCell(emptyCell);
	}
}
