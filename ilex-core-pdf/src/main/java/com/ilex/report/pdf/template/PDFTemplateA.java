package com.ilex.report.pdf.template;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

import com.ilex.invoice.reporting.common.IlexReportPDFConstants;
import com.ilex.invoice.reporting.dto.InvoiceReportDTO;
import com.ilex.invoice.reporting.dto.TemplateADimDTO;
import com.ilex.report.pdf.helper.PDFMultiplePageException;
import com.ilex.report.pdf.helper.PDFThreadLocal;
import com.ilex.report.pdf.helper.PDFUtils;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfDestination;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Main class for generating PDF in Template A format.
 */
public class PDFTemplateA {

	/**
	 * Generates Template A PDF.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @return pdf contents as byte array.
	 */
	public byte[] generatePDF(InvoiceReportDTO invoiceReportDTO) {
		try {
			// Default set to single page
			PDFThreadLocal.pageNumbers.set(false);
			return generateInvPDF(invoiceReportDTO);
		} catch (PDFMultiplePageException mpe) {
			// In case exception is thrown due to multiple pages, enable page
			// numbers and create pdf.
			PDFThreadLocal.pageNumbers.set(true);
			return generateInvPDF(invoiceReportDTO);
		}
	}

	/**
	 * Generates Template A Invoice PDF.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @return pdf contents as byte array.
	 */
	private byte[] generateInvPDF(InvoiceReportDTO invoiceReportDTO) {
		File tempFile = PDFUtils.getTempFile();
		Document document = null;
		OutputStream pdfOut = null;
		InputStream pdfIn = null;
		try {
			TemplateADimDTO templateADimDTO = new TemplateADimDTO();

			document = new Document(IlexReportPDFConstants.PAGE_SIZE,
					IlexReportPDFConstants.PAGE_LEFT_MARGIN,
					IlexReportPDFConstants.PAGE_TOP_MARGIN,
					IlexReportPDFConstants.PAGE_RIGHT_MARGIN,
					IlexReportPDFConstants.PAGE_BOTTOM_MARGIN);

			pdfOut = new FileOutputStream(tempFile);

			PdfWriter writer = PdfWriter.getInstance(document, pdfOut);

			PDFTemplateARptHeader pdfTemplateAHeader = new PDFTemplateARptHeader();
			PDFTemplateAInvAddress pdfTemplateAInvAddress = new PDFTemplateAInvAddress();
			PDFTemplateACustReference pdfTemplateACustomerRef = new PDFTemplateACustReference();
			PDFTemplateAInvDetail pdfTemplateAInvDetail = new PDFTemplateAInvDetail();
			PDFTemplateAInvDetailFooter invDetFooter = new PDFTemplateAInvDetailFooter();

			pdfTemplateAHeader.setInvoiceReportDTO(invoiceReportDTO);
			pdfTemplateAHeader.setTemplateADimDTO(templateADimDTO);

			// Set height of various sections
			templateADimDTO.getRepHeaderFirstPage().setHeight(
					pdfTemplateAHeader.getPageHeaderHeight(1));

			templateADimDTO.getRepHeaderOtherPages().setHeight(
					pdfTemplateAHeader.getPageHeaderHeight(2));

			templateADimDTO.getInvDetailFooter().setHeight(
					invDetFooter.getHeight(invoiceReportDTO, document));

			writer.setPageEvent(pdfTemplateAHeader);

			document.open();

			pdfTemplateAInvAddress.prepareInvAddress(invoiceReportDTO,
					document, writer, templateADimDTO);

			pdfTemplateACustomerRef.prepareCustomerRefTable(invoiceReportDTO,
					document, writer, templateADimDTO);

			pdfTemplateAInvDetail.prepareInvoiceDetailTable(invoiceReportDTO,
					document, writer, templateADimDTO);

			PdfAction action = PdfAction.gotoLocalPage(1, new PdfDestination(
					PdfDestination.FIT, 0, 10000, 1), writer);
			writer.setOpenAction(action);
			writer.setViewerPreferences(PdfWriter.FitWindow
					| PdfWriter.CenterWindow);

			document.close();

			pdfIn = new FileInputStream(tempFile);

			return IOUtils.toByteArray(pdfIn);
		} catch (DocumentException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			if (document != null) {
				try {
					document.close();
				} catch (Exception e) {
					// Do nothing
				}
			}

			IOUtils.closeQuietly(pdfOut);
			IOUtils.closeQuietly(pdfIn);

			try {
				if (tempFile.exists()) {
					tempFile.delete();
				}
			} catch (Exception e) {
				// DO nothing
			}

		}
		return null;
	}
}
