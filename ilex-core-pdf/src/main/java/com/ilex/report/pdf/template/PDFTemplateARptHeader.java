package com.ilex.report.pdf.template;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;

import com.ilex.invoice.reporting.common.IlexReportPDFConstants;
import com.ilex.invoice.reporting.dto.InvoiceReportDTO;
import com.ilex.invoice.reporting.dto.TemplateADimDTO;
import com.ilex.report.pdf.helper.PDFMultiplePageException;
import com.ilex.report.pdf.helper.PDFThreadLocal;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.mind.fw.util.PropertyFileUtil;

/**
 * Class to generate TemplateA Report Header.
 */
class PDFTemplateARptHeader extends PdfPageEventHelper {

	/** The total. */
	private PdfTemplate total;

	/** The invoice report dto. */
	private InvoiceReportDTO invoiceReportDTO;

	private TemplateADimDTO templateADimDTO;

	public TemplateADimDTO getTemplateADimDTO() {
		return templateADimDTO;
	}

	public void setTemplateADimDTO(TemplateADimDTO templateADimDTO) {
		this.templateADimDTO = templateADimDTO;
	}

	/**
	 * Sets the invoice report dto.
	 * 
	 * @param invoiceReportDTO
	 *            the new invoice report dto
	 */
	public void setInvoiceReportDTO(InvoiceReportDTO invoiceReportDTO) {
		this.invoiceReportDTO = invoiceReportDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.itextpdf.text.pdf.PdfPageEventHelper#onOpenDocument(com.itextpdf.
	 * text.pdf.PdfWriter, com.itextpdf.text.Document)
	 */
	@Override
	public void onOpenDocument(PdfWriter writer, Document document) {
		total = writer.getDirectContent().createTemplate(30, 16);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(com.itextpdf.text.
	 * pdf.PdfWriter, com.itextpdf.text.Document)
	 */
	@Override
	public void onEndPage(PdfWriter writer, Document document) {

		int currentPageNumber = writer.getPageNumber();

		if (currentPageNumber > 1 && !PDFThreadLocal.pageNumbers.get()) {
			throw new PDFMultiplePageException();
		}

		PdfPTable headerTable = getHeaderTable(currentPageNumber);
		PDFTemplateAInvDetailFooter invDetFooter = new PDFTemplateAInvDetailFooter();

		headerTable.writeSelectedRows(0, -1,
				document.left(IlexReportPDFConstants.X_PADDING),
				document.top(), writer.getDirectContent());
		if (currentPageNumber == 1) {
			try {

				invDetFooter.prepareInvDetailFooter(invoiceReportDTO, document,
						writer, templateADimDTO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Gets the header table.
	 * 
	 * @param currentPageNumber
	 *            the current page number
	 * @return the header table
	 */
	private PdfPTable getHeaderTable(int currentPageNumber) {
		PdfPTable headerTable = new PdfPTable(2);

		headerTable
				.setTotalWidth(IlexReportPDFConstants.TEMPLATE_A_TABLE_WIDTH);
		headerTable.setLockedWidth(true);
		headerTable.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell leftCell = getLeftCell(currentPageNumber);

		PdfPCell rightCell = getRightCell(currentPageNumber);
		rightCell.setPaddingRight(50);

		// set rightCell Aligned with the Contingent Address for only Page1
		if (currentPageNumber == 1) {
			rightCell.setPaddingTop(57);
		}
		// set rightCell Aligned with the Contingent Logo for Rest of the pages
		// of pdf file
		else {
			rightCell.setPaddingTop(10);
		}
		headerTable.addCell(leftCell);
		headerTable.addCell(rightCell);
		return headerTable;
	}

	/**
	 * Gets the left cell.
	 * 
	 * @param currentPageNumber
	 *            the current page number
	 * @return the left cell
	 */
	private PdfPCell getLeftCell(int currentPageNumber) {
		PdfPTable nestedTable = new PdfPTable(1);
		nestedTable
				.setTotalWidth(IlexReportPDFConstants.TEMPLATE_A_TABLE_WIDTH / 2);

		nestedTable.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell cellImg = getHeaderLogo();
		nestedTable.addCell(cellImg);

		// first page header
		if (currentPageNumber == 1) {
			PdfPCell firstPageHeaderCell = getFirstPageHeader(nestedTable);
			firstPageHeaderCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			nestedTable.addCell(firstPageHeaderCell);
		}

		PdfPCell leftCell = new PdfPCell();
		leftCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		leftCell.setBorder(0);
		leftCell.addElement(nestedTable);

		return leftCell;
	}

	/**
	 * Gets the first page header.
	 * 
	 * @param headerTable
	 *            the header table
	 * @return the first page header
	 */
	private PdfPCell getFirstPageHeader(PdfPTable headerTable) {
		PdfPCell headerAddressCell = new PdfPCell();
		headerAddressCell.setBorder(0);

		PdfPTable headerAddress = new PdfPTable(1);
		headerAddress.setWidthPercentage(100f);
		headerAddress.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell nameCell = new PdfPCell(new Phrase(invoiceReportDTO
				.getHeaderAddressDTO().getName(),
				IlexReportPDFConstants.STANDARD_BOLD_BLUE_FONT));
		nameCell.setBorder(0);
		nameCell.setPaddingTop(0);
		nameCell.setPaddingBottom(2);

		PdfPCell adressLine1Cell = new PdfPCell(new Phrase(invoiceReportDTO
				.getHeaderAddressDTO().getAddressLine1(),
				IlexReportPDFConstants.STANDARD_BLUE_FONT));
		adressLine1Cell.setBorder(0);
		adressLine1Cell.setPaddingTop(0);
		adressLine1Cell.setPaddingBottom(2);

		String addressStr = invoiceReportDTO.getHeaderAddressDTO().getCity()
				+ ", " + invoiceReportDTO.getHeaderAddressDTO().getState()
				+ "  " + invoiceReportDTO.getHeaderAddressDTO().getZipCode()
				+ "  " + invoiceReportDTO.getHeaderAddressDTO().getCountry();
		PdfPCell addressLine2Cell = new PdfPCell(new Phrase(addressStr,
				IlexReportPDFConstants.STANDARD_BLUE_FONT));
		addressLine2Cell.setBorder(0);
		addressLine2Cell.setPaddingTop(0);
		addressLine2Cell.setPaddingBottom(2);

		String PhoneAndFax = invoiceReportDTO.getHeaderAddressDTO().getPhone()
				+ "" + invoiceReportDTO.getHeaderAddressDTO().getFax();
		PdfPCell contactNumberCell = new PdfPCell(new Phrase(PhoneAndFax,
				IlexReportPDFConstants.STANDARD_BLUE_FONT));
		contactNumberCell.setBorder(0);
		contactNumberCell.setPaddingTop(0);
		contactNumberCell.setPaddingBottom(2);

		PdfPCell headerURL = new PdfPCell(new Phrase(invoiceReportDTO
				.getHeaderAddressDTO().getUrl().getAddress(),
				IlexReportPDFConstants.STANDARD_BLUE_FONT));
		headerURL.setBorder(0);
		headerURL.setPaddingTop(0);
		headerURL.setPaddingBottom(0);

		headerAddress.addCell(nameCell);
		headerAddress.addCell(adressLine1Cell);
		headerAddress.addCell(addressLine2Cell);
		headerAddress.addCell(contactNumberCell);
		headerAddress.addCell(headerURL);

		headerAddressCell.addElement(headerAddress);

		return headerAddressCell;
	}

	/**
	 * Gets the right cell of header table.
	 * 
	 * @param currentPageNumber
	 *            the current page number
	 * @return the right cell
	 */
	private PdfPCell getRightCell(int currentPageNumber) {

		PdfPTable nestedTable = new PdfPTable(new float[] { 0.4f, 0.5f });
		nestedTable.setWidthPercentage(50);
		// nestedTable
		// .setTotalWidth(IlexReportPDFConstants.TEMPLATE_A_TABLE_WIDTH / 2);

		// row 1
		PdfPCell invoiceLabel = getInvLabel();
		invoiceLabel.setPaddingLeft(0);
		invoiceLabel.setPaddingRight(0);

		nestedTable.addCell(invoiceLabel);

		PdfPCell invoiceNumber = getInvNumber();
		invoiceNumber.setPaddingLeft(0);
		invoiceNumber.setPaddingRight(0);
		nestedTable.addCell(invoiceNumber);

		// row 2
		PdfPCell invoiceDateLabel = getInvDateLabel();
		invoiceDateLabel.setPaddingLeft(0);
		invoiceDateLabel.setPaddingRight(0);
		nestedTable.addCell(invoiceDateLabel);

		PdfPCell invoiceDate = getInvDate();
		invoiceDate.setPaddingLeft(0);
		invoiceDate.setPaddingRight(0);
		nestedTable.addCell(invoiceDate);

		if (PDFThreadLocal.pageNumbers.get()) {
			// row 3
			PdfPCell pageLabel = getPageLabel();
			pageLabel.setPaddingLeft(0);
			pageLabel.setPaddingRight(0);
			nestedTable.addCell(pageLabel);

			PdfPCell pageNumber = new PdfPCell(
					getPageNumberingPhrase(currentPageNumber));
			pageNumber.setBorder(0);
			pageNumber.setPaddingLeft(0);
			pageNumber.setPaddingRight(0);
			pageNumber.setPaddingTop(6f);
			nestedTable.addCell(pageNumber);
		}

		PdfPCell invoiceCell = new PdfPCell();
		invoiceCell.setBorder(0);
		invoiceCell.setPadding(0);
		invoiceCell.addElement(nestedTable);

		return invoiceCell;
	}

	/**
	 * Gets the page label for multiple page reports.
	 * 
	 * @return pageLabel
	 */
	private PdfPCell getPageLabel() {
		PdfPCell pageLabel = new PdfPCell(new Phrase(
				PropertyFileUtil.getAppProperty("ilex.pdf.header.invoicePage"),
				IlexReportPDFConstants.HEADER_BLUE_FONT));
		pageLabel.setPaddingLeft(0);
		pageLabel.setPaddingTop(2f);
		pageLabel.setPaddingRight(0);
		pageLabel.setBorder(0);
		return pageLabel;
	}

	/**
	 * Gets the invoice number.
	 * 
	 * @return invoiceNumber
	 */
	private PdfPCell getInvNumber() {
		PdfPCell invoiceNumber = new PdfPCell(new Phrase(
				invoiceReportDTO.getInvoiceNumber(),
				IlexReportPDFConstants.STANDARD_BOLD_FONT));
		invoiceNumber.setPaddingTop(12f);
		invoiceNumber.setPaddingLeft(0);
		invoiceNumber.setPaddingRight(0);
		invoiceNumber.setBorder(0);
		return invoiceNumber;
	}

	/**
	 * Gets the invoice label.
	 * 
	 * @return invoiceLabel
	 */
	private PdfPCell getInvLabel() {
		PdfPCell invoiceLabel = new PdfPCell(new Phrase(
				PropertyFileUtil.getAppProperty("ilex.pdf.header.invoice"),
				IlexReportPDFConstants.HEADER_BLUE_FONT));
		invoiceLabel.setPaddingLeft(0);
		invoiceLabel.setPaddingTop(8f);
		invoiceLabel.setPaddingRight(0);
		invoiceLabel.setBorder(0);
		return invoiceLabel;
	}

	/**
	 * Gets the invoice date label.
	 * 
	 * @return invoiceDateLabel
	 */
	private PdfPCell getInvDateLabel() {
		PdfPCell invoiceDateLabel = new PdfPCell(new Phrase(
				PropertyFileUtil.getAppProperty("ilex.pdf.header.invoiceDate"),
				IlexReportPDFConstants.HEADER_BLUE_FONT));
		invoiceDateLabel.setPaddingLeft(0);
		invoiceDateLabel.setPaddingTop(2f);
		invoiceDateLabel.setPaddingRight(0);
		invoiceDateLabel.setBorder(0);
		return invoiceDateLabel;
	}

	/**
	 * Gets the invoice date.
	 * 
	 * @return invoiceDate
	 */
	private PdfPCell getInvDate() {
		SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy");
		String invDate = ft.format(invoiceReportDTO.getInvoiceDate());
		PdfPCell invoiceDate = new PdfPCell(new Phrase(invDate,
				IlexReportPDFConstants.STANDARD_BOLD_FONT));
		invoiceDate.setPaddingTop(6f);
		invoiceDate.setPaddingLeft(0);
		invoiceDate.setPaddingRight(0);
		invoiceDate.setBorder(0);
		return invoiceDate;
	}

	/**
	 * Gets the header logo.
	 * 
	 * @return the header logo
	 */
	private PdfPCell getHeaderLogo() {
		Chunk imdb = new Chunk("Image",
				IlexReportPDFConstants.TABLE_STANDARD_HEADER_FONT);
		Image img = null;
		try {
			img = Image.getInstance(String.format(
					invoiceReportDTO.getHeaderLogo(), imdb));
		} catch (BadElementException e1) {
			e1.printStackTrace();
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		img.scaleToFit(140, 84);
		img.setRotationDegrees(0);

		PdfPCell cellImg = new PdfPCell(img);
		cellImg.setPadding(0);
		cellImg.setBorder(0);
		cellImg.setPaddingLeft(15f);
		return cellImg;
	}

	/**
	 * Gets the page numbering phrase.
	 * 
	 * @param currentPageNumber
	 *            the currentPageNumber
	 * 
	 * @return the page number
	 */
	private Phrase getPageNumberingPhrase(int currentPageNumber) {
		Phrase pageNumber = new Phrase(
				String.format("%d / ", currentPageNumber),
				IlexReportPDFConstants.STANDARD_BOLD_FONT);
		Image totalPageNum;
		try {
			totalPageNum = Image.getInstance(total);
		} catch (BadElementException e) {
			throw new ExceptionConverter(e);
		}
		pageNumber.add(new Chunk(totalPageNum, 0, 0));
		return pageNumber;
	}

	/**
	 * Gets the page header height.
	 * 
	 * @param pageNumber
	 *            the page number
	 * @return the page header height
	 */
	public float getPageHeaderHeight(int pageNumber) {
		if (pageNumber <= 0) {
			pageNumber = 1;
		}

		// disable page numbers to compute height
		boolean enablePageNumbers = PDFThreadLocal.pageNumbers.get();
		PDFThreadLocal.pageNumbers.set(false);

		PdfPTable headerTable = getHeaderTable(pageNumber);

		// Restore page number setting
		PDFThreadLocal.pageNumbers.set(enablePageNumbers);

		return headerTable.calculateHeights();
	}

	@Override
	public void onCloseDocument(PdfWriter writer, Document document) {

		String numberOfPages = String.valueOf(writer.getPageNumber() - 1);
		ColumnText.showTextAligned(total, Element.ALIGN_LEFT, new Phrase(
				numberOfPages, IlexReportPDFConstants.STANDARD_BOLD_FONT), 0,
				0, 0);
	}

}
