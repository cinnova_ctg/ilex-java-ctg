package com.ilex.report.pdf.template;

import java.text.SimpleDateFormat;

import com.ilex.invoice.reporting.common.IlexReportPDFConstants;
import com.ilex.invoice.reporting.dto.InvoiceReportDTO;
import com.ilex.invoice.reporting.dto.TemplateADimDTO;
import com.ilex.report.pdf.helper.PDFUtils;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mind.fw.util.PropertyFileUtil;

/**
 * Class to generate TemplateA Customer Reference.
 */
class PDFTemplateACustReference {

	/**
	 * Prepares customer reference table.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @param document
	 *            the document
	 * @param PdfWriter
	 *            the writer
	 * @param templateADimDTO
	 *            the template a dim dto
	 * @throws DocumentException
	 *             the document exception
	 */
	public void prepareCustomerRefTable(InvoiceReportDTO invoiceReportDTO,
			Document document, PdfWriter writer, TemplateADimDTO templateADimDTO)
			throws DocumentException {

		PdfPTable invoiceHeaderTable = new PdfPTable(
				IlexReportPDFConstants.TEMPLATE_A_CUST_REF_TAB_DIM);
		invoiceHeaderTable.setHorizontalAlignment(Element.ALIGN_CENTER);
		invoiceHeaderTable.setTotalWidth(document.right() - document.left() - 2
				* IlexReportPDFConstants.X_PADDING);

		setTableHeader(invoiceReportDTO, invoiceHeaderTable);

		populateRows(invoiceReportDTO, invoiceHeaderTable);

		float height = templateADimDTO.getRepHeaderFirstPage().getHeight()
				+ templateADimDTO.getInvAddress().getHeight() + 2
				* IlexReportPDFConstants.Y_PADDING;

		invoiceHeaderTable.writeSelectedRows(0, -1, document.left()
				+ IlexReportPDFConstants.X_PADDING, document.top() - height,
				writer.getDirectContent());

		templateADimDTO.getCustReference().setHeight(
				invoiceHeaderTable.getTotalHeight());
	}

	/**
	 * Generates row containing values(second row).
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @param invoiceHeaderTable
	 *            the invoice header table
	 */
	private void populateRows(InvoiceReportDTO invoiceReportDTO,
			PdfPTable invoiceHeaderTable) {
		PdfPCell custRefValue = new PdfPCell(new Phrase(
				invoiceReportDTO.getCustomerReferenceValue(),
				IlexReportPDFConstants.TABLE_STANDARD_HEADER_FONT));
		PDFUtils.setDefaultCellLayout(custRefValue);
		custRefValue.enableBorderSide(Rectangle.LEFT);
		custRefValue.enableBorderSide(Rectangle.RIGHT);
		custRefValue.setFixedHeight(25F);
		custRefValue.setPaddingLeft(10f);
		// custRefValue.setPaddingTop(5f);
		custRefValue.setVerticalAlignment(Element.ALIGN_MIDDLE);

		PdfPCell terms = new PdfPCell(new Phrase(
				invoiceReportDTO.getPaymentTerms(),
				IlexReportPDFConstants.TABLE_STANDARD_HEADER_FONT));
		PDFUtils.setDefaultCellLayout(terms);
		terms.enableBorderSide(Rectangle.RIGHT);
		terms.setFixedHeight(25F);
		terms.setHorizontalAlignment(Element.ALIGN_CENTER);
		// terms.setPaddingTop(5f);
		terms.setVerticalAlignment(Element.ALIGN_MIDDLE);

		SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy");
		String offsiteDate = ft.format(invoiceReportDTO.getOffsiteDate());
		PdfPCell offsiteDateCell = new PdfPCell(new Phrase(offsiteDate,
				IlexReportPDFConstants.TABLE_STANDARD_HEADER_FONT));
		PDFUtils.setDefaultCellLayout(offsiteDateCell);
		offsiteDateCell.enableBorderSide(Rectangle.RIGHT);
		offsiteDateCell.setFixedHeight(25F);
		offsiteDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// offsiteDateCell.setPaddingTop(5f);
		offsiteDateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		invoiceHeaderTable.addCell(custRefValue);
		invoiceHeaderTable.addCell(terms);
		invoiceHeaderTable.addCell(offsiteDateCell);
	}

	/**
	 * Sets Table First header Row.
	 * 
	 * @param invoiceReportDTO
	 *            the invoice report dto
	 * @param invoiceHeaderTable
	 *            the invoice header table
	 */
	private void setTableHeader(InvoiceReportDTO invoiceReportDTO,
			PdfPTable invoiceHeaderTable) {
		PdfPCell custRefName = new PdfPCell(new Phrase(
				invoiceReportDTO.getCustomerReferenceName(),
				IlexReportPDFConstants.TABLE_STANDARD_HEADER_WHITE_FONT));
		PDFUtils.setDefaultCellLayout(custRefName);
		custRefName.enableBorderSide(Rectangle.LEFT);
		custRefName.enableBorderSide(Rectangle.TOP);
		custRefName.enableBorderSide(Rectangle.RIGHT);

		custRefName.setPaddingLeft(10f);
		custRefName
				.setBackgroundColor(IlexReportPDFConstants.TABLE_STANDARD_HEADER_NAVYBLUE_COLOR);
		custRefName.setFixedHeight(25F);
		// custRefName.setPaddingTop(5f);
		custRefName.setVerticalAlignment(Element.ALIGN_MIDDLE);

		PdfPCell paymentLabel = new PdfPCell(
				new Phrase(
						PropertyFileUtil
								.getAppProperty("ilex.pdf.invoiceDetails.paymentTerms"),
						IlexReportPDFConstants.TABLE_STANDARD_HEADER_WHITE_FONT));
		PDFUtils.setDefaultCellLayout(paymentLabel);
		paymentLabel.enableBorderSide(Rectangle.RIGHT);
		paymentLabel.enableBorderSide(Rectangle.TOP);
		paymentLabel
				.setBackgroundColor(IlexReportPDFConstants.TABLE_STANDARD_HEADER_NAVYBLUE_COLOR);
		paymentLabel.setHorizontalAlignment(Element.ALIGN_CENTER);
		paymentLabel.setFixedHeight(25F);
		// paymentLabel.setPaddingTop(5f);
		paymentLabel.setVerticalAlignment(Element.ALIGN_MIDDLE);

		PdfPCell reqShipDateLabel = new PdfPCell(
				new Phrase(
						PropertyFileUtil
								.getAppProperty("ilex.pdf.invoiceDetails.requiredShipDate"),
						IlexReportPDFConstants.TABLE_STANDARD_HEADER_WHITE_FONT));
		PDFUtils.setDefaultCellLayout(reqShipDateLabel);
		reqShipDateLabel.enableBorderSide(Rectangle.RIGHT);
		reqShipDateLabel.enableBorderSide(Rectangle.TOP);
		reqShipDateLabel
				.setBackgroundColor(IlexReportPDFConstants.TABLE_STANDARD_HEADER_NAVYBLUE_COLOR);
		reqShipDateLabel.setHorizontalAlignment(Element.ALIGN_CENTER);
		reqShipDateLabel.setFixedHeight(25F);
		// reqShipDateLabel.setPaddingTop(5f);
		reqShipDateLabel.setVerticalAlignment(Element.ALIGN_MIDDLE);

		invoiceHeaderTable.addCell(custRefName);
		invoiceHeaderTable.addCell(paymentLabel);
		invoiceHeaderTable.addCell(reqShipDateLabel);
	}

}
