package com.ilex.ws.servicenow.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class UpdateIncidentResponseDTO.Used to get response for Ilex to Service
 * Now incident update Interaction for TXR.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "sysId", "table", "displayName",
		"displayValue", "status", "statusMessage", "errorMessage",
		"errorNumber", "errorDescription", "vTaskNumber", "ticketNumber" })
@XmlRootElement(name = "insertResponse", namespace = "http://www.service-now.com/u_contingentupdateincident")
public class UpdateIncidentTxrResponseDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@XmlElement(name = "sys_id", required = true, namespace = "http://www.service-now.com/u_contingentupdateincident")
	protected String sysId;
	@XmlElement(required = true, namespace = "http://www.service-now.com/u_contingentupdateincident")
	protected String table;
	@XmlElement(name = "display_name", required = true, namespace = "http://www.service-now.com/u_contingentupdateincident")
	protected String displayName;
	@XmlElement(name = "display_value", required = true, namespace = "http://www.service-now.com/u_contingentupdateincident")
	protected String displayValue;
	@XmlElement(required = true, namespace = "http://www.service-now.com/u_contingentupdateincident")
	protected String status;
	@XmlElement(name = "status_message", namespace = "http://www.service-now.com/u_contingentupdateincident")
	protected String statusMessage;
	@XmlElement(name = "error_message", namespace = "http://www.service-now.com/u_contingentupdateincident")
	protected String errorMessage;
	@XmlElement(name = "error_number", namespace = "http://www.service-now.com/u_contingentupdateincident")
	protected String errorNumber;
	@XmlElement(name = "error_description", namespace = "http://www.service-now.com/u_contingentupdateincident")
	protected String errorDescription;
	@XmlElement(name = "vTaskNumber", namespace = "http://www.service-now.com/u_contingentupdateincident")
	protected String vTaskNumber;
	@XmlElement(name = "ticketNumber", namespace = "http://www.service-now.com/u_contingentupdateincident")
	protected String ticketNumber;

	/**
	 * Gets the value of the sysId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSysId() {
		return sysId;
	}

	/**
	 * Sets the value of the sysId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSysId(String value) {
		this.sysId = value;
	}

	/**
	 * Gets the value of the table property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTable() {
		return table;
	}

	/**
	 * Sets the value of the table property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTable(String value) {
		this.table = value;
	}

	/**
	 * Gets the value of the displayName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Sets the value of the displayName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDisplayName(String value) {
		this.displayName = value;
	}

	/**
	 * Gets the value of the displayValue property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDisplayValue() {
		return displayValue;
	}

	/**
	 * Sets the value of the displayValue property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDisplayValue(String value) {
		this.displayValue = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value) {
		this.status = value;
	}

	/**
	 * Gets the value of the statusMessage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * Sets the value of the statusMessage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatusMessage(String value) {
		this.statusMessage = value;
	}

	/**
	 * Gets the value of the errorMessage property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Sets the value of the errorMessage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setErrorMessage(String value) {
		this.errorMessage = value;
	}

	/**
	 * Gets the value of the errorNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getErrorNumber() {
		return errorNumber;
	}

	/**
	 * Sets the value of the errorNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setErrorNumber(String value) {
		this.errorNumber = value;
	}

	/**
	 * Gets the value of the errorDescription property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * Sets the value of the errorDescription property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setErrorDescription(String value) {
		this.errorDescription = value;
	}

	/**
	 * Gets the value of the vTaskNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVTaskNumber() {
		return vTaskNumber;
	}

	/**
	 * Sets the value of the vTaskNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVTaskNumber(String value) {
		this.vTaskNumber = value;
	}

	/**
	 * Gets the value of the ticketNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTicketNumber() {
		return ticketNumber;
	}

	/**
	 * Sets the value of the ticketNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTicketNumber(String value) {
		this.ticketNumber = value;
	}

}
