package com.ilex.ws.core.bean;

import java.io.Serializable;
import java.util.Date;

public class HDWIPTableVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date openedDbDate;
	private Date updatedDbDate;
	private String siteNumber;
	private String priority;
	private String status;
	private String nextAction;
	private String nextActionId;
	private Date nextActionDueDateDB;
	private String cca_tss;
	private String shortDescription;
	private String customer;
	private String ilexNumber;
	private String customerNumber;
	private String lastTd;
	private String ticketStatus;
	private String workNotes;
	private String source;
	private String siteAddress;
	private String siteCity;
	private String siteState;
	private String siteCountry;
	private String siteZipcode;
	private String contactPhoneNumber;
	private String contactName;
	private String contactEmailAddress;
	private String ticketId;
	private String sessionId;
	private String sumOfEfforts;
	private String ticketState;
	private String circuitStatus;
	private String ticketSource;
	private String device;
	private String primaryState;
	private String appendixName;
	private String overdueStatus;
	private String siteStatus;
	private Date nextActionDate;
	private boolean nextActionOverDue;
	private boolean extSystemStatus;
	private String assignmentGroupId;
	private String assignmentGroupName;
	private String dateDiff;
	private String underReview;

	private String clientType;

	private String mmId;

	private String ticketNumber;

	private String eventColor;
	private String hdeclColor;
	private String site_prop;

	public boolean isNextActionOverDue() {
		return nextActionOverDue;
	}

	public void setNextActionOverDue(boolean nextActionOverDue) {
		this.nextActionOverDue = nextActionOverDue;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNextAction() {
		return nextAction;
	}

	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}

	public String getCca_tss() {
		return cca_tss;
	}

	public void setCca_tss(String cca_tss) {
		this.cca_tss = cca_tss;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getIlexNumber() {
		return ilexNumber;
	}

	public void setIlexNumber(String ilexNumber) {
		this.ilexNumber = ilexNumber;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getLastTd() {
		return lastTd;
	}

	public void setLastTd(String lastTd) {
		this.lastTd = lastTd;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public String getWorkNotes() {
		return workNotes;
	}

	public void setWorkNotes(String workNotes) {
		this.workNotes = workNotes;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getSiteCountry() {
		return siteCountry;
	}

	public void setSiteCountry(String siteCountry) {
		this.siteCountry = siteCountry;
	}

	public String getSiteZipcode() {
		return siteZipcode;
	}

	public void setSiteZipcode(String siteZipcode) {
		this.siteZipcode = siteZipcode;
	}

	public String getContactPhoneNumber() {
		return contactPhoneNumber;
	}

	public void setContactPhoneNumber(String contactPhoneNumber) {
		this.contactPhoneNumber = contactPhoneNumber;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public Date getOpenedDbDate() {
		return openedDbDate;
	}

	public void setOpenedDbDate(Date openedDbDate) {
		this.openedDbDate = openedDbDate;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Date getNextActionDueDateDB() {
		return nextActionDueDateDB;
	}

	public void setNextActionDueDateDB(Date nextActionDueDateDB) {
		this.nextActionDueDateDB = nextActionDueDateDB;
	}

	public String getSumOfEfforts() {
		return sumOfEfforts;
	}

	public void setSumOfEfforts(String sumOfEfforts) {
		this.sumOfEfforts = sumOfEfforts;
	}

	public String getNextActionId() {
		return nextActionId;
	}

	public void setNextActionId(String nextActionId) {
		this.nextActionId = nextActionId;
	}

	public String getTicketState() {
		return ticketState;
	}

	public void setTicketState(String ticketState) {
		this.ticketState = ticketState;
	}

	public String getCircuitStatus() {
		return circuitStatus;
	}

	public void setCircuitStatus(String circuitStatus) {
		this.circuitStatus = circuitStatus;
	}

	public String getTicketSource() {
		return ticketSource;
	}

	public void setTicketSource(String ticketSource) {
		this.ticketSource = ticketSource;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getPrimaryState() {
		return primaryState;
	}

	public void setPrimaryState(String primaryState) {
		this.primaryState = primaryState;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getOverdueStatus() {
		return overdueStatus;
	}

	public void setOverdueStatus(String overdueStatus) {
		this.overdueStatus = overdueStatus;
	}

	public String getSiteStatus() {
		return siteStatus;
	}

	public void setSiteStatus(String siteStatus) {
		this.siteStatus = siteStatus;
	}

	public Date getNextActionDate() {
		return nextActionDate;
	}

	public void setNextActionDate(Date nextActionDate) {
		this.nextActionDate = nextActionDate;
	}

	public boolean isExtSystemStatus() {
		return extSystemStatus;
	}

	public void setExtSystemStatus(boolean extSystemStatus) {
		this.extSystemStatus = extSystemStatus;
	}

	public String getAssignmentGroupName() {
		return assignmentGroupName;
	}

	public void setAssignmentGroupName(String assignmentGroupName) {
		this.assignmentGroupName = assignmentGroupName;
	}

	public String getAssignmentGroupId() {
		return assignmentGroupId;
	}

	public void setAssignmentGroupId(String assignmentGroupId) {
		this.assignmentGroupId = assignmentGroupId;
	}

	public String getDateDiff() {
		return dateDiff;
	}

	public void setDateDiff(String dateDiff) {
		this.dateDiff = dateDiff;
	}

	public Date getUpdatedDbDate() {
		return updatedDbDate;
	}

	public void setUpdatedDbDate(Date updatedDbDate) {
		this.updatedDbDate = updatedDbDate;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getUnderReview() {
		return underReview;
	}

	public void setUnderReview(String underReview) {
		this.underReview = underReview;
	}

	public String getMmId() {
		return mmId;
	}

	public void setMmId(String mmId) {
		this.mmId = mmId;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public String getEventColor() {
		return eventColor;
	}

	public void setEventColor(String eventColor) {
		this.eventColor = eventColor;
	}

	public String getSite_prop() {
		return site_prop;
	}

	public void setSite_prop(String site_prop) {
		this.site_prop = site_prop;
	}

}
