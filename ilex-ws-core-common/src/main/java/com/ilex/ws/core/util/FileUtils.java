package com.ilex.ws.core.util;

public class FileUtils {

	/**
	 * Gets the file extension.
	 * 
	 * @param fileName
	 *            the file name
	 * @return the file extension
	 */
	public static String getFileExtension(String fileName) {
		String extension;
		int dotPos = (fileName.lastIndexOf(".") + 1);
		extension = fileName.substring(dotPos);
		return extension;
	}

}
