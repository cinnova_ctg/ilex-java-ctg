package com.ilex.ws.core.dto;

import java.io.Serializable;

public class BTMContactListDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String invContactId[];
	private String firstName[];
	private String lastName[];
	private String phone[];
	private String cell[];
	private String fax[];
	private String email[];
	private String status[];
	private int clientID;

	public String[] getInvContactId() {
		return invContactId;
	}

	public void setInvContactId(String[] invContactId) {
		this.invContactId = invContactId;
	}

	public String[] getFirstName() {
		return firstName;
	}

	public void setFirstName(String[] firstName) {
		this.firstName = firstName;
	}

	public String[] getLastName() {
		return lastName;
	}

	public void setLastName(String[] lastName) {
		this.lastName = lastName;
	}

	public String[] getPhone() {
		return phone;
	}

	public void setPhone(String[] phone) {
		this.phone = phone;
	}

	public String[] getCell() {
		return cell;
	}

	public void setCell(String[] cell) {
		this.cell = cell;
	}

	public String[] getFax() {
		return fax;
	}

	public void setFax(String[] fax) {
		this.fax = fax;
	}

	public String[] getEmail() {
		return email;
	}

	public void setEmail(String[] email) {
		this.email = email;
	}

	public String[] getStatus() {
		return status;
	}

	public void setStatus(String[] status) {
		this.status = status;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public int getClientID() {
		return clientID;
	}

}
