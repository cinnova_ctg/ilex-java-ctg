package com.ilex.ws.core.bean;

import java.io.Serializable;

public class NewDispatchVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String requestor;
	private String requestorEmail;
	private String requestType;
	private String criticality;
	private String resource;
	private boolean pps;
	private boolean standBy;
	private boolean msp;
	private boolean helpDesk;
	private String customerReference;
	private String siteListId;
	private String problemCategory;
	private String problemDescription;
	private String specialInstructions;
	private String ticketRules;
	private String customerSpecificDetails;
	private String requestSummary;
	private Long ticketId;
	private Long appendixId;
	private Long jobId;
	private String ticketNumber;
	private String webTicket;
	private String updateCompleteTicket = "0";
	private Integer monitoringSourceCd = null;
	private Long deviceId = null;
	private TicketScheduleInfo ticketScheduleInfo;

	public boolean isPps() {
		return pps;
	}

	public void setPps(boolean pps) {
		this.pps = pps;
	}

	public boolean isStandBy() {
		return standBy;
	}

	public void setStandBy(boolean standBy) {
		this.standBy = standBy;
	}

	public boolean isMsp() {
		return msp;
	}

	public void setMsp(boolean msp) {
		this.msp = msp;
	}

	public boolean isHelpDesk() {
		return helpDesk;
	}

	public void setHelpDesk(boolean helpDesk) {
		this.helpDesk = helpDesk;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public Long getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(Long appendixId) {
		this.appendixId = appendixId;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String getRequestorEmail() {
		return requestorEmail;
	}

	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getCriticality() {
		return criticality;
	}

	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public String getProblemCategory() {
		return problemCategory;
	}

	public void setProblemCategory(String problemCategory) {
		this.problemCategory = problemCategory;
	}

	public String getProblemDescription() {
		return problemDescription;
	}

	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}

	public String getSpecialInstructions() {
		return specialInstructions;
	}

	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public String getTicketRules() {
		return ticketRules;
	}

	public void setTicketRules(String ticketRules) {
		this.ticketRules = ticketRules;
	}

	public String getCustomerSpecificDetails() {
		return customerSpecificDetails;
	}

	public void setCustomerSpecificDetails(String customerSpecificDetails) {
		this.customerSpecificDetails = customerSpecificDetails;
	}

	public String getRequestSummary() {
		return requestSummary;
	}

	public void setRequestSummary(String requestSummary) {
		this.requestSummary = requestSummary;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public TicketScheduleInfo getTicketScheduleInfo() {
		return ticketScheduleInfo;
	}

	public void setTicketScheduleInfo(TicketScheduleInfo ticketScheduleInfo) {
		this.ticketScheduleInfo = ticketScheduleInfo;
	}

	public String getWebTicket() {
		return webTicket;
	}

	public void setWebTicket(String webTicket) {
		this.webTicket = webTicket;
	}

	public String getUpdateCompleteTicket() {
		return updateCompleteTicket;
	}

	public void setUpdateCompleteTicket(String updateCompleteTicket) {
		this.updateCompleteTicket = updateCompleteTicket;
	}

	public Integer getMonitoringSourceCd() {
		return monitoringSourceCd;
	}

	public void setMonitoringSourceCd(Integer monitoringSourceCd) {
		this.monitoringSourceCd = monitoringSourceCd;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public String getSiteListId() {
		return siteListId;
	}

	public void setSiteListId(String siteListId) {
		this.siteListId = siteListId;
	}

}
