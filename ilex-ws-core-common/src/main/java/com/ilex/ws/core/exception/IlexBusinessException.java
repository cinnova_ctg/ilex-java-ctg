/*
 * <h4>Description</h4>
 * All business exceptions should be thrown using this class.
 *
 * @author tarungupta
 */
package com.ilex.ws.core.exception;

public class IlexBusinessException extends IlexException {

	private static final long serialVersionUID = -8817025373765348946L;

	public IlexBusinessException(String key) {
		super(ErrorType.BUSINESS, key);
		this.key = key;
	}

	public IlexBusinessException(String key, String[] args) {
		super(ErrorType.BUSINESS, key);
		this.key = key;
		this.errorArgs = args;
	}

	public IlexBusinessException(String key, String defaultMessage) {
		super(ErrorType.BUSINESS, key);
		this.key = key;
		this.defaultMessage = defaultMessage;
	}

	public IlexBusinessException(String key, String[] args, String defaultMessage) {
		super(ErrorType.BUSINESS, defaultMessage);
		this.key = key;
		this.errorArgs = args;
		this.defaultMessage = defaultMessage;
	}
}
