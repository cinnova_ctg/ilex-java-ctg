package com.ilex.ws.core.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class MsaClientDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String clientName;
	private String clientId;
	private int mmId;

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientId() {
		return clientId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public void setMmId(int mmId) {
		this.mmId = mmId;
}

	public int getMmId() {
		return mmId;
	}

}
