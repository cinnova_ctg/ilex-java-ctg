package com.ilex.ws.core.bean;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class HDEffortVO.
 */
public class HDEffortVO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The effort. */
	private String effort;

	/** The effort date. */
	private String effortDate;

	/** The billable. */
	boolean billable;

	/** The worked by. */
	private String workedBy;

	/** The effort end date. */
	private String effortEndDate;

	/**
	 * Gets the effort.
	 * 
	 * @return the effort
	 */
	public String getEffort() {
		return effort;
	}

	/**
	 * Sets the effort.
	 * 
	 * @param effort
	 *            the new effort
	 */
	public void setEffort(String effort) {
		this.effort = effort;
	}

	/**
	 * Gets the effort date.
	 * 
	 * @return the effort date
	 */
	public String getEffortDate() {
		return effortDate;
	}

	/**
	 * Sets the effort date.
	 * 
	 * @param effortDate
	 *            the new effort date
	 */
	public void setEffortDate(String effortDate) {
		this.effortDate = effortDate;
	}

	/**
	 * Checks if is billable.
	 * 
	 * @return true, if is billable
	 */
	public boolean isBillable() {
		return billable;
	}

	/**
	 * Sets the billable.
	 * 
	 * @param billable
	 *            the new billable
	 */
	public void setBillable(boolean billable) {
		this.billable = billable;
	}

	/**
	 * Gets the worked by.
	 * 
	 * @return the worked by
	 */
	public String getWorkedBy() {
		return workedBy;
	}

	/**
	 * Sets the worked by.
	 * 
	 * @param workedBy
	 *            the new worked by
	 */
	public void setWorkedBy(String workedBy) {
		this.workedBy = workedBy;
	}

	/**
	 * Gets the effort end date.
	 * 
	 * @return the effort end date
	 */
	public String getEffortEndDate() {
		return effortEndDate;
	}

	/**
	 * Sets the effort end date.
	 * 
	 * @param effortEndDate
	 *            the new effort end date
	 */
	public void setEffortEndDate(String effortEndDate) {
		this.effortEndDate = effortEndDate;
	}

}
