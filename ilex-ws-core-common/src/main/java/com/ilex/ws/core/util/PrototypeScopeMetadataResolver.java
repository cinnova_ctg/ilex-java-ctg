/*
 * <h4>Description</h4>
 * Adds prototype scope to beans
 *
 * @author tarungupta
 */
package com.ilex.ws.core.util;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ScopeMetadata;
import org.springframework.context.annotation.ScopeMetadataResolver;

/**
 * The Class PrototypeScopeMetadataResolver.
 */
public class PrototypeScopeMetadataResolver implements ScopeMetadataResolver {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.context.annotation.ScopeMetadataResolver#
	 * resolveScopeMetadata
	 * (org.springframework.beans.factory.config.BeanDefinition)
	 */
	public ScopeMetadata resolveScopeMetadata(BeanDefinition definition) {
		ScopeMetadata scopeMetadata = new ScopeMetadata();
		scopeMetadata.setScopeName(BeanDefinition.SCOPE_PROTOTYPE);

		return scopeMetadata;
	}

}
