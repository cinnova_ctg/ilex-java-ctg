package com.ilex.ws.core.dto;

import java.io.Serializable;

public class HdBillableDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String billable = "0.00";
	private String nonBillable = "0.00";

	public String getBillable() {
		return billable;
	}

	public void setBillable(String billable) {
		this.billable = billable;
	}

	public String getNonBillable() {
		return nonBillable;
	}

	public void setNonBillable(String nonBillable) {
		this.nonBillable = nonBillable;
	}

}
