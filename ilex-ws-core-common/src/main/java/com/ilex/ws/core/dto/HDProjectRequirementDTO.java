package com.ilex.ws.core.dto;

import java.io.Serializable;

public class HDProjectRequirementDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long prjRequirementId;
	private Long requirementId;
	private Long prjId;
	private boolean prjRequirementStatus;
	private String prjRequirementDate;
	private String requirement;
	private String projectNotes;
	private boolean requirementSelected;
	private String requirementDescription;

	public String getRequirement() {
		return requirement;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	public String getProjectNotes() {
		return projectNotes;
	}

	public void setProjectNotes(String projectNotes) {
		this.projectNotes = projectNotes;
	}

	public Long getPrjRequirementId() {
		return prjRequirementId;
	}

	public void setPrjRequirementId(Long prjRequirementId) {
		this.prjRequirementId = prjRequirementId;
	}

	public Long getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(Long requirementId) {
		this.requirementId = requirementId;
	}

	public Long getPrjId() {
		return prjId;
	}

	public void setPrjId(Long prjId) {
		this.prjId = prjId;
	}

	public boolean isPrjRequirementStatus() {
		return prjRequirementStatus;
	}

	public void setPrjRequirementStatus(boolean prjRequirementStatus) {
		this.prjRequirementStatus = prjRequirementStatus;
	}

	public String getPrjRequirementDate() {
		return prjRequirementDate;
	}

	public void setPrjRequirementDate(String prjRequirementDate) {
		this.prjRequirementDate = prjRequirementDate;
	}

	public boolean isRequirementSelected() {
		return requirementSelected;
	}

	public void setRequirementSelected(boolean requirementSelected) {
		this.requirementSelected = requirementSelected;
	}

	public String getRequirementDescription() {
		return requirementDescription;
	}

	public void setRequirementDescription(String requirementDescription) {
		this.requirementDescription = requirementDescription;
	}

}
