package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.Date;

public class HDInstallNoteVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String installNote;
	private Date installNoteDate;
	private String userLastName;
	private String workNoteType;

	public String getInstallNote() {
		return installNote;
	}

	public void setInstallNote(String installNote) {
		this.installNote = installNote;
	}

	public Date getInstallNoteDate() {
		return installNoteDate;
	}

	public void setInstallNoteDate(Date installNoteDate) {
		this.installNoteDate = installNoteDate;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getWorkNoteType() {
		return workNoteType;
	}

	public void setWorkNoteType(String workNoteType) {
		this.workNoteType = workNoteType;
	}

}
