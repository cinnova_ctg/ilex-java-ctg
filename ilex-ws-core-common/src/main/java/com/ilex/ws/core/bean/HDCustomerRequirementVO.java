package com.ilex.ws.core.bean;

import java.io.Serializable;

public class HDCustomerRequirementVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String customerName;
	private Long projectId;
	private String projectName;
	private Integer requirementCount;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getRequirementCount() {
		return requirementCount;
	}

	public void setRequirementCount(Integer requirementCount) {
		this.requirementCount = requirementCount;
	}

}
