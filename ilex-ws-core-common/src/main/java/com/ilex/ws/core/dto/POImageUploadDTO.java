package com.ilex.ws.core.dto;

import java.io.Serializable;

public class POImageUploadDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String authentication;
	private String imagenumber;
	private String name;
	private String status;
	private String lm_po_id;
	private String description;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLm_po_id() {
		return lm_po_id;
	}

	public void setLm_po_id(String lm_po_id) {
		this.lm_po_id = lm_po_id;
	}

	public String getAuthentication() {
		return authentication;
	}

	public void setAuthentication(String authentication) {
		this.authentication = authentication;
	}

	public String getImagenumber() {
		return imagenumber;
	}

	public void setImagenumber(String imagenumber) {
		this.imagenumber = imagenumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
