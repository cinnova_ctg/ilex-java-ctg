package com.ilex.ws.core.dto;

import java.io.Serializable;

public class InvAdditionalInfoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int additionalInfoId;
	private Integer invSetupId;
	private String name;
	private String level;
	private String value;
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setAdditionalInfoId(Integer additionalInfoId) {
		this.additionalInfoId = additionalInfoId;
	}

	public Integer getAdditionalInfoId() {
		return additionalInfoId;
	}

	public void setInvSetupId(Integer invSetupId) {
		this.invSetupId = invSetupId;
	}

	public Integer getInvSetupId() {
		return invSetupId;
	}

}
