/*
 * <h4>Description</h4>
 *
 * @author tarungupta
 */
package com.ilex.ws.core.util;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.util.ClassUtils;

/**
 * The Class NameGeneratorImpl.
 */
public class NameGeneratorImpl implements BeanNameGenerator {

	/**
	 * Suffix of entity class name.
	 */
	private static final String IMPL_SUFFIX = "Impl";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.beans.factory.support.BeanNameGenerator#generateBeanName
	 * (org.springframework.beans.factory.config.BeanDefinition,
	 * org.springframework.beans.factory.support.BeanDefinitionRegistry)
	 */
	public String generateBeanName(BeanDefinition definition,
			BeanDefinitionRegistry registry) {
		try {
			String propertyName = ClassUtils.getShortNameAsProperty(Class
					.forName(definition.getBeanClassName()));

			int pos = propertyName.indexOf(IMPL_SUFFIX);
			if (pos == -1)
				return propertyName;

			return propertyName.substring(0, pos);

		} catch (ClassNotFoundException e) {
			// No chance to reach here.
			throw new IllegalStateException("Class "
					+ definition.getBeanClassName() + " not found.");
		}
	}

}
