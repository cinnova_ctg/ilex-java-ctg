package com.ilex.ws.core.bean;

import java.io.Serializable;

public class HDCustAppendixVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String customerName;
	private String appendixTitle;
	private String customerAppendix;
	private Long appendixId;
	private Long customerId;
	private String customerInitial;
	private Long msaId;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAppendixTitle() {
		return appendixTitle;
	}

	public void setAppendixTitle(String appendixTitle) {
		this.appendixTitle = appendixTitle;
	}

	public String getCustomerAppendix() {
		return customerAppendix;
	}

	public void setCustomerAppendix(String customerAppendix) {
		this.customerAppendix = customerAppendix;
	}

	public Long getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(Long appendixId) {
		this.appendixId = appendixId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerInitial() {
		return customerInitial;
	}

	public void setCustomerInitial(String customerInitial) {
		this.customerInitial = customerInitial;
	}

	public Long getMsaId() {
		return msaId;
	}

	public void setMsaId(Long msaId) {
		this.msaId = msaId;
	}

}
