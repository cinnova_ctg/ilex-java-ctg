package com.ilex.ws.core.util;

/**
 * The Enum HDTicketStatus.
 */
public enum HDTicketStatus {

	NEW("1"), WIP("2"), DISPATCH_REQUESTED("3"), DISPATCH_SCHEDULED("4"), DISPATCH_ONSITE(
			"5"), RESOLVED("6"), AWAITING_RESPONSE_ACTION("7"), CANCELLED("8");

	private String ticketStatus;

	HDTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public String getTicketStatus() {
		return this.ticketStatus;
	}

}
