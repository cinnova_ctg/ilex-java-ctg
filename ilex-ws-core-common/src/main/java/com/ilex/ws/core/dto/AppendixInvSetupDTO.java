package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;

public class AppendixInvSetupDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private InvBTMClientDTO client;
	private AppendixDetailDTO appendix;
	private String requestReference;
	private InvSetupDTO invSetupDetails;
	private List<InvBTMContactDTO> contactDetailsList = LazyList.decorate(
			new ArrayList<InvBTMContactDTO>(),
			FactoryUtils.instantiateFactory(InvBTMContactDTO.class));

	private Integer selectedClient;
	private List<InvAdditionalInfoDTO> additionalInformationList = LazyList
			.decorate(new ArrayList<InvAdditionalInfoDTO>(),
					FactoryUtils.instantiateFactory(InvAdditionalInfoDTO.class));
	private InvBillToAddressDTO billToAddress;

	public InvBillToAddressDTO getBillToAddress() {
		return billToAddress;
	}

	public void setBillToAddress(InvBillToAddressDTO billToAddress) {
		this.billToAddress = billToAddress;
	}

	public Integer getSelectedClient() {
		return selectedClient;
	}

	public void setSelectedClient(Integer selectedClient) {
		this.selectedClient = selectedClient;
	}

	public List<InvBTMContactDTO> getContactDetailsList() {
		return contactDetailsList;
	}

	public void setContactDetailsList(List<InvBTMContactDTO> contactDetailsList) {
		this.contactDetailsList = contactDetailsList;
	}

	public InvSetupDTO getInvSetupDetails() {
		return invSetupDetails;
	}

	public void setInvSetupDetails(InvSetupDTO invSetupDetails) {
		this.invSetupDetails = invSetupDetails;
	}

	public String getRequestReference() {
		return requestReference;
	}

	public void setRequestReference(String requestReference) {
		this.requestReference = requestReference;
	}

	public AppendixDetailDTO getAppendix() {
		return appendix;
	}

	public void setAppendix(AppendixDetailDTO appendix) {
		this.appendix = appendix;
	}

	public InvBTMClientDTO getClient() {
		return client;
	}

	public void setClient(InvBTMClientDTO client) {
		this.client = client;
	}

	public void setAdditionalInformationList(
			List<InvAdditionalInfoDTO> additionalInformationList) {
		this.additionalInformationList = additionalInformationList;
	}

	public List<InvAdditionalInfoDTO> getAdditionalInformationList() {
		return additionalInformationList;
	}

}
