/*******************************************************************************
 * Copyright (c) 2010 *All rights reserved.
 * The information contained here in is confidential.
 * 
 * Created by:	himanshujain
 * Created on:	Feb 25, 2011
 * Description:	
 * 
 * @author himanshujain
 ******************************************************************************/
package com.ilex.ws.core.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.ilex.ws.core.exception.IlexSystemException;

//This DateUtils class will handles Date related utilities
public class DateUtils {

	private final static String[] SUPPORTED_DATE_FORMATS = { "MM/dd/yyyy",
			"dd-MMM-yyyy", "dd-mm-yyyy", "MM/dd/yyyy hh:mm a",
			"MM/dd/yyyy hh:mm", "MM-dd-yyyy hh:mm", "MM-dd-yyyy hh:mm a" };

	/**
	 * 
	 * @param date
	 *            the Date
	 * @return the string
	 */
	public static String convertDateToString(Date date, String dateFormat) {
		DateFormat formatter = new SimpleDateFormat(dateFormat);
		String dateToString = formatter.format(date);
		return dateToString;

	}

	public static Date convertStringToDate(String strDate) {

		if (strDate.contains("/")) {
			strDate = strDate.replaceAll("/", "-");
		}
		try {
			Date date = org.apache.commons.lang.time.DateUtils.parseDate(
					strDate, SUPPORTED_DATE_FORMATS);
			return date;
		} catch (ParseException e) {
			throw new IlexSystemException("", e,
					"Date format not supported or invalid date: " + strDate);
		}
	}

	public static Timestamp convertStringToTimeStamp(String strDate) {

		try {
			Date date = org.apache.commons.lang.time.DateUtils.parseDate(
					strDate, SUPPORTED_DATE_FORMATS);

			java.sql.Timestamp timestamp = new java.sql.Timestamp(
					date.getTime());

			return timestamp;
		} catch (ParseException e) {
			throw new IlexSystemException("", e,
					"Date format not supported or invalid date: " + strDate);
		}
	}

	public static Date getLowerDate(String strDate) {

		Date lowerDate = convertStringToDate(strDate);
		return lowerDate;

	}

	public static Date getUpperDate(String strDate) {

		Date date = getLowerDate(strDate);

		Date upperDate = new Date(date.getTime() + 86400 * 1000 - 1);

		return upperDate;

	}

	/**
	 * Gets the upper date.
	 * 
	 * @param strDate
	 *            the str date
	 * @return the upper date
	 */
	public static String convertDateToString() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_hhmmss");
		String strDate = sdf.format(new java.util.Date(System
				.currentTimeMillis()));
		return strDate;

	}

	/**
	 * Discription: This method take date, output date format and input date
	 * format. This method change format of date.
	 * 
	 * @param INPUT_DATE
	 *            - Date
	 * @param MY_DATE_FORMAT
	 *            - Output Date Format
	 * @param INPUT_DATE_FORMAT
	 *            - Input Date Format
	 * @return String - Date
	 */
	public static String getDateFormat(String INPUT_DATE,
			String MY_DATE_FORMAT, String INPUT_DATE_FORMAT) {

		String expDate = "";
		try {

			DateFormat df = new SimpleDateFormat();
			Date d = new SimpleDateFormat(INPUT_DATE_FORMAT).parse(INPUT_DATE);
			expDate = new SimpleDateFormat(MY_DATE_FORMAT).format(d);

		} catch (Exception e) {
			expDate = "catn,t fromat " + INPUT_DATE;
		}
		return expDate;
	}
}
