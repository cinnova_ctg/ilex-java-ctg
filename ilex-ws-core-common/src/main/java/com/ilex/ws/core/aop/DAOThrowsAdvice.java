/*
 * <h4>Description</h4>
 * This class handles exceptions from DAO layer and convert them to LATSystemException
 * 
 * @author tarungupta
 */
package com.ilex.ws.core.aop;

import java.lang.reflect.Method;

import org.springframework.aop.ThrowsAdvice;
import org.springframework.dao.DataAccessException;

import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.exception.IlexSystemException;

/**
 * The Class DAOThrowsAdvice.
 */
public class DAOThrowsAdvice implements ThrowsAdvice {

	/**
	 * After throwing.
	 * 
	 * @param ex
	 *            the ex
	 */
	public void afterThrowing(IlexSystemException ex) {
		throw ex;
	}

	/**
	 * After throwing.
	 * 
	 * @param ex
	 *            the ex
	 */
	public void afterThrowing(IlexBusinessException ex) {
		throw ex;
	}

	/**
	 * After throwing.
	 * 
	 * @param ex
	 *            the ex
	 */
	public void afterThrowing(DataAccessException ex) {
		String[] errorArgs = { ex.getClass().getSimpleName(), ex.getMessage() };
		IlexSystemException sdfpEx = new IlexSystemException(
				"errors.system.dataAccess", errorArgs, ex, ex.getMessage());
		throw sdfpEx;
	}

	/**
	 * After throwing.
	 * 
	 * @param method
	 *            the method
	 * @param args
	 *            the args
	 * @param target
	 *            the target
	 * @param ex
	 *            the ex
	 */
	public void afterThrowing(Method method, Object[] args, Object target,
			RuntimeException ex) {
		String[] errorArgs = { ex.getClass().getSimpleName(), ex.getMessage() };
		IlexSystemException sdfpEx = new IlexSystemException(
				"errors.system.dao", errorArgs, ex, ex.getMessage());
		throw sdfpEx;
	}

}
