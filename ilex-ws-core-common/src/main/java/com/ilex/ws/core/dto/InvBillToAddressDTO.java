package com.ilex.ws.core.dto;

import java.io.Serializable;

public class InvBillToAddressDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int invBillToAddressId;
	private String clientName;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zipCode;
	private String country;
	private int deliveryMethod;
	private int mbtId;
	private String firstName;
	private String lastName;
	private String phone;
	private String cell;
	private String fax;
	private String email;
	private Integer invContactId;
	private boolean deliveryExist;
	private boolean contactExist;

	public int getInvBillToAddressId() {
		return invBillToAddressId;
	}

	public void setInvBillToAddressId(int invBillToAddressId) {
		this.invBillToAddressId = invBillToAddressId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(int deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public int getMbtId() {
		return mbtId;
	}

	public void setMbtId(int mbtId) {
		this.mbtId = mbtId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCell() {
		return cell;
	}

	public void setCell(String cell) {
		this.cell = cell;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getInvContactId() {
		return invContactId;
	}

	public void setInvContactId(Integer invContactId) {
		this.invContactId = invContactId;
	}

	public boolean isDeliveryExist() {
		return deliveryExist;
	}

	public void setDeliveryExist(boolean deliveryExist) {
		this.deliveryExist = deliveryExist;
	}

	public boolean isContactExist() {
		return contactExist;
	}

	public void setContactExist(boolean contactExist) {
		this.contactExist = contactExist;
	}

}
