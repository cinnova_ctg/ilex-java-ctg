package com.ilex.ws.core.bean;

import java.io.Serializable;
import java.util.Date;

public class IncidentVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String siteNumber;
    private String siteDisplayNumber;
    private String category;
    private String configurationItem;
    private String shortDescription;
    private String description;
    private String state;
    private String impact;
    private String urgency;
    private String symptom;
    private String type;
    private String vendorKey;
    private String ticketNumber;
    private String ticketId;
    private String serviceNowTicketId;
    private String serviceNowSysId;
    private String workNotes;
    private String appendixId;
    private String msaId;
    private Date createdDate;
    private String integrationStatus;
    private String integrationType;
    private String integrationStatusMsg;
    private String vTaskNumber;
    private String triggerSource;
    private String createIdentifier;
    private String customerName;
    private String lpSiteListId;

    public String getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(String integrationType) {
        this.integrationType = integrationType;
    }

    public String getIntegrationStatusMsg() {
        return integrationStatusMsg;
    }

    public void setIntegrationStatusMsg(String integrationStatusMsg) {
        this.integrationStatusMsg = integrationStatusMsg;
    }

    public String getSiteNumber() {
        return siteNumber;
    }

    public void setSiteNumber(String siteNumber) {
        this.siteNumber = siteNumber;
    }

    public String getSiteDisplayNumber() {
        return siteDisplayNumber;
    }

    public void setSiteDisplayNumber(String siteDisplayNumber) {
        this.siteDisplayNumber = siteDisplayNumber;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getConfigurationItem() {
        return configurationItem;
    }

    public void setConfigurationItem(String configurationItem) {
        this.configurationItem = configurationItem;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getImpact() {
        return impact;
    }

    public void setImpact(String impact) {
        this.impact = impact;
    }

    public String getUrgency() {
        return urgency;
    }

    public void setUrgency(String urgency) {
        this.urgency = urgency;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVendorKey() {
        return vendorKey;
    }

    public void setVendorKey(String vendorKey) {
        this.vendorKey = vendorKey;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getServiceNowTicketId() {
        return serviceNowTicketId;
    }

    public void setServiceNowTicketId(String serviceNowTicketId) {
        this.serviceNowTicketId = serviceNowTicketId;
    }

    public String getServiceNowSysId() {
        return serviceNowSysId;
    }

    public void setServiceNowSysId(String serviceNowSysId) {
        this.serviceNowSysId = serviceNowSysId;
    }

    public String getWorkNotes() {
        return workNotes;
    }

    public void setWorkNotes(String workNotes) {
        this.workNotes = workNotes;
    }

    public String getAppendixId() {
        return appendixId;
    }

    public void setAppendixId(String appendixId) {
        this.appendixId = appendixId;
    }

    public String getMsaId() {
        return msaId;
    }

    public void setMsaId(String msaId) {
        this.msaId = msaId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getIntegrationStatus() {
        return integrationStatus;
    }

    public void setIntegrationStatus(String integrationStatus) {
        this.integrationStatus = integrationStatus;
    }

    public String getvTaskNumber() {
        return vTaskNumber;
    }

    public void setvTaskNumber(String vTaskNumber) {
        this.vTaskNumber = vTaskNumber;
    }

    public String getTriggerSource() {
        return triggerSource;
    }

    public void setTriggerSource(String triggerSource) {
        this.triggerSource = triggerSource;
    }

    public String getCreateIdentifier() {
        return createIdentifier;
    }

    public void setCreateIdentifier(String createIdentifier) {
        this.createIdentifier = createIdentifier;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getLpSiteListId() {
        return lpSiteListId;
    }

    public void setLpSiteListId(String lpSiteListId) {
        this.lpSiteListId = lpSiteListId;
    }

}
