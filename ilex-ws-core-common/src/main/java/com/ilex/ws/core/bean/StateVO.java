package com.ilex.ws.core.bean;

import java.io.Serializable;

public class StateVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String countryId;
	private String stateName;
	private String stateId;

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
}
