package com.ilex.ws.core.bean;

import java.io.Serializable;

public class MsaClientVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String clientName;
	private String clientId;
	private int mmId;

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setMmId(int mmId) {
		this.mmId = mmId;
}

	public int getMmId() {
		return mmId;
	}

}
