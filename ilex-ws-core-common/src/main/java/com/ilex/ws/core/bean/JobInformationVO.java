package com.ilex.ws.core.bean;

import java.io.Serializable;

public class JobInformationVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String jobId;
	private String installationNotes;
	private String jobCategory;
	private String jobPriority;

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getInstallationNotes() {
		return installationNotes;
	}

	public void setInstallationNotes(String installationNotes) {
		this.installationNotes = installationNotes;
	}

	public String getJobCategory() {
		return jobCategory;
	}

	public void setJobCategory(String jobCategory) {
		this.jobCategory = jobCategory;
	}

	/**
	 * @return the jobPriority
	 */
	public String getJobPriority() {
		return jobPriority;
	}

	/**
	 * @param jobPriority
	 *            the jobPriority to set
	 */
	public void setJobPriority(String jobPriority) {
		this.jobPriority = jobPriority;
	}

}
