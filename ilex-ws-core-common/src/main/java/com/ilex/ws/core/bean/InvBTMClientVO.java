package com.ilex.ws.core.bean;

import java.io.Serializable;

public class InvBTMClientVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String clientName;
	private boolean status;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zipCode;
	private String country;
	private int deliveryMethod;
	private int mmId;
	private int clientId;
	private boolean deliveryExist;
	private boolean integrated;
	private boolean contactExist;
	private int mbtClientId;

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(int deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public int getMmId() {
		return mmId;
	}

	public void setMmId(int mmId) {
		this.mmId = mmId;
	}

	public void setIntegrated(boolean integrated) {
		this.integrated = integrated;
	}

	public boolean isIntegrated() {
		return integrated;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientName() {
		return clientName;
	}

	public void setDeliveryExist(boolean deliveryExist) {
		this.deliveryExist = deliveryExist;
	}

	public boolean isDeliveryExist() {
		return deliveryExist;
	}

	public void setContactExist(boolean contactExist) {
		this.contactExist = contactExist;
	}

	public boolean isContactExist() {
		return contactExist;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getClientId() {
		return clientId;
	}

	public void setMbtClientId(int mbtClientId) {
		this.mbtClientId = mbtClientId;
	}

	public int getMbtClientId() {
		return mbtClientId;
	}

}
