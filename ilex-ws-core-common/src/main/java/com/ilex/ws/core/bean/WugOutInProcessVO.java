package com.ilex.ws.core.bean;

import java.io.Serializable;

public class WugOutInProcessVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nActiveMonitorStateChangeID;
	private String dvId;
	private String dStartTime;
	private String escalationLevel;
	private String currentStatus;
	private String outageDuration;
	private String eventCount;
	private String ticketReference;
	private String issueOwner;
	private String rootCouse;
	private String correctiveAction;
	private String effortMinutes;
	private String nActiveMonitorStateChangeIdNew;
	private String ihEventTime;
	private String wugInstance;
	private String wugBackupStatus;
	private String wugihnPivotActiveMonitorTypeToDeviceID;
	private String ticketId;

	public String getWugInstance() {
		return wugInstance;
	}

	public void setWugInstance(String wugInstance) {
		this.wugInstance = wugInstance;
	}

	public String getWugBackupStatus() {
		return wugBackupStatus;
	}

	public void setWugBackupStatus(String wugBackupStatus) {
		this.wugBackupStatus = wugBackupStatus;
	}

	public String getIhEventTime() {
		return ihEventTime;
	}

	public void setIhEventTime(String ihEventTime) {
		this.ihEventTime = ihEventTime;
	}

	public String getnActiveMonitorStateChangeIdNew() {
		return nActiveMonitorStateChangeIdNew;
	}

	public void setnActiveMonitorStateChangeIdNew(
			String nActiveMonitorStateChangeIdNew) {
		this.nActiveMonitorStateChangeIdNew = nActiveMonitorStateChangeIdNew;
	}

	public String getnActiveMonitorStateChangeID() {
		return nActiveMonitorStateChangeID;
	}

	public void setnActiveMonitorStateChangeID(
			String nActiveMonitorStateChangeID) {
		this.nActiveMonitorStateChangeID = nActiveMonitorStateChangeID;
	}

	public String getDvId() {
		return dvId;
	}

	public void setDvId(String dvId) {
		this.dvId = dvId;
	}

	public String getdStartTime() {
		return dStartTime;
	}

	public void setdStartTime(String dStartTime) {
		this.dStartTime = dStartTime;
	}

	public String getEscalationLevel() {
		return escalationLevel;
	}

	public void setEscalationLevel(String escalationLevel) {
		this.escalationLevel = escalationLevel;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getOutageDuration() {
		return outageDuration;
	}

	public void setOutageDuration(String outageDuration) {
		this.outageDuration = outageDuration;
	}

	public String getEventCount() {
		return eventCount;
	}

	public void setEventCount(String eventCount) {
		this.eventCount = eventCount;
	}

	public String getTicketReference() {
		return ticketReference;
	}

	public void setTicketReference(String ticketReference) {
		this.ticketReference = ticketReference;
	}

	public String getIssueOwner() {
		return issueOwner;
	}

	public void setIssueOwner(String issueOwner) {
		this.issueOwner = issueOwner;
	}

	public String getRootCouse() {
		return rootCouse;
	}

	public void setRootCouse(String rootCouse) {
		this.rootCouse = rootCouse;
	}

	public String getCorrectiveAction() {
		return correctiveAction;
	}

	public void setCorrectiveAction(String correctiveAction) {
		this.correctiveAction = correctiveAction;
	}

	public String getEffortMinutes() {
		return effortMinutes;
	}

	public void setEffortMinutes(String effortMinutes) {
		this.effortMinutes = effortMinutes;
	}

	public String getWugihnPivotActiveMonitorTypeToDeviceID() {
		return wugihnPivotActiveMonitorTypeToDeviceID;
	}

	public void setWugihnPivotActiveMonitorTypeToDeviceID(
			String wugihnPivotActiveMonitorTypeToDeviceID) {
		this.wugihnPivotActiveMonitorTypeToDeviceID = wugihnPivotActiveMonitorTypeToDeviceID;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

}
