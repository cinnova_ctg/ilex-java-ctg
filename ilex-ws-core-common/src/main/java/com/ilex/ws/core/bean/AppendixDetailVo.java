package com.ilex.ws.core.bean;

import java.io.Serializable;

public class AppendixDetailVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int appendixId;
	private String appendixName;
	private String appendixType;

	public int getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(int appendixId) {
		this.appendixId = appendixId;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getAppendixType() {
		return appendixType;
	}

	public void setAppendixType(String appendixType) {
		this.appendixType = appendixType;
	}

}
