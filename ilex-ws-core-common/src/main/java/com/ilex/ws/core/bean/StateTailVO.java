package com.ilex.ws.core.bean;

import java.io.Serializable;
import java.util.List;

public class StateTailVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long downMinutes;
	List<WugDetailsVO> wugDetailsVOList;

	public Long getDownMinutes() {
		return downMinutes;
	}

	public void setDownMinutes(Long downMinutes) {
		this.downMinutes = downMinutes;
	}

	public List<WugDetailsVO> getWugDetailsVOList() {
		return wugDetailsVOList;
	}

	public void setWugDetailsVOList(List<WugDetailsVO> wugDetailsVOList) {
		this.wugDetailsVOList = wugDetailsVOList;
	}

}
