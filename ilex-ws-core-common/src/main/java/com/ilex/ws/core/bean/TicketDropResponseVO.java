package com.ilex.ws.core.bean;

import java.io.Serializable;

public class TicketDropResponseVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean dropStatus;
	private String assignedUser;

	public boolean isDropStatus() {
		return dropStatus;
	}

	public void setDropStatus(boolean dropStatus) {
		this.dropStatus = dropStatus;
	}

	public String getAssignedUser() {
		return assignedUser;
	}

	public void setAssignedUser(String assignedUser) {
		this.assignedUser = assignedUser;
	}

}
