package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.List;

public class JobInformationDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String jobId;
	private String installationNotes;
	private String jobCategory;
	private String jobPriority;
	private String appendixId;
	private String newJobOwnerId;

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getNewJobOwnerId() {
		return newJobOwnerId;
	}

	public void setNewJobOwnerId(String newJobOwnerId) {
		this.newJobOwnerId = newJobOwnerId;
	}

	private List<String> jobCategories;
	private List<String> jobPriorities;
	private String jobCategorySelect;
	private String jobPrioritySelect;

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getInstallationNotes() {
		return installationNotes;
	}

	public void setInstallationNotes(String installationNotes) {
		this.installationNotes = installationNotes;
	}

	public String getJobCategory() {
		return jobCategory;
	}

	public void setJobCategory(String jobCategory) {
		this.jobCategory = jobCategory;
	}



	public List<String> getJobCategories() {
		return jobCategories;
	}

	public void setJobCategories(List<String> jobCategories) {
		this.jobCategories = jobCategories;
	}

	public List<String> getJobPriorities() {
		return jobPriorities;
	}

	public void setJobPriorities(List<String> jobPriorities) {
		this.jobPriorities = jobPriorities;
	}

	public String getJobCategorySelect() {
		return jobCategorySelect;
	}

	public void setJobCategorySelect(String jobCategorySelect) {
		this.jobCategorySelect = jobCategorySelect;
	}

	public String getJobPrioritySelect() {
		return jobPrioritySelect;
	}

	public void setJobPrioritySelect(String jobPrioritySelect) {
		this.jobPrioritySelect = jobPrioritySelect;
	}

	/**
	 * @return the jobPriority
	 */
	public String getJobPriority() {
		return jobPriority;
	}

	/**
	 * @param jobPriority the jobPriority to set
	 */
	public void setJobPriority(String jobPriority) {
		this.jobPriority = jobPriority;
	}
}
