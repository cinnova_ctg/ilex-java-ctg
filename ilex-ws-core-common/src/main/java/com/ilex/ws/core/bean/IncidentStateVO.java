package com.ilex.ws.core.bean;

import java.io.Serializable;

public class IncidentStateVO implements Serializable {

    private String label;
    private String value;
    private int enabled;
    private int selectable;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public int getSelectable() {
        return selectable;
    }

    public void setSelectable(int selectable) {
        this.selectable = selectable;
    }

}
