package com.ilex.ws.core.dto;

import java.io.Serializable;

public class SchedulerDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String jobId;
	private String rseType;
	private String rseReason = null;

	private String scheduleId = null;

	private String oldstartdate = null;
	private String oldstartdatehh = null;
	private String oldstartdatemm = null;
	private String oldstartdateop = null;

	private String startdate = null;
	private String startdatehh = null;
	private String startdatemm = null;
	private String startdateop = null;

	private String scheduleEnddate = null;
	private String scheduleEndhh = null;
	private String scheduleEndmm = null;
	private String scheduleEndop = null;

	private String actstartdate = null;
	private String actstartdatehh = null;
	private String actstartdatemm = null;
	private String actstartdateop = null;

	private String actenddate = null;
	private String actenddatehh = null;
	private String actenddatemm = null;
	private String actenddateop = null;

	private String scheduleNumber = null;

	public SchedulerDTO(String scheduleId, String startdate,
			String startdatehh, String startdatemm, String startdateop,
			String actstartdate, String actstartdatehh, String actstartdatemm,
			String actstartdateop, String actenddate, String actenddatehh,
			String actenddatemm, String actenddateop, String scheduleNumber) {
		this.scheduleId = scheduleId;
		this.startdate = startdate;
		this.startdatehh = startdatehh;
		this.startdatemm = startdatemm;
		this.startdateop = startdateop;
		this.actstartdate = actstartdate;
		this.actstartdatehh = actstartdatehh;
		this.actstartdatemm = actstartdatemm;
		this.actstartdateop = actstartdateop;
		this.actenddate = actenddate;
		this.actenddatehh = actenddatehh;
		this.actenddatemm = actenddatemm;
		this.actenddateop = actenddateop;
		this.scheduleNumber = scheduleNumber;
	}

	public SchedulerDTO(String scheduleId, String startdate,
			String startdatehh, String startdatemm, String startdateop,
			String scheduleEnddate, String scheduleEndhh, String scheduleEndmm,
			String scheduleEndop, String actstartdate, String actstartdatehh,
			String actstartdatemm, String actstartdateop, String actenddate,
			String actenddatehh, String actenddatemm, String actenddateop,
			String scheduleNumber) {

		this.scheduleId = scheduleId;
		this.startdate = startdate;
		this.startdatehh = startdatehh;
		this.startdatemm = startdatemm;
		this.startdateop = startdateop;
		this.scheduleEnddate = scheduleEnddate;
		this.scheduleEndhh = scheduleEndhh;
		this.scheduleEndmm = scheduleEndmm;
		this.scheduleEndop = scheduleEndop;
		this.actstartdate = actstartdate;
		this.actstartdatehh = actstartdatehh;
		this.actstartdatemm = actstartdatemm;
		this.actstartdateop = actstartdateop;
		this.actenddate = actenddate;
		this.actenddatehh = actenddatehh;
		this.actenddatemm = actenddatemm;
		this.actenddateop = actenddateop;
		this.scheduleNumber = scheduleNumber;
	}

	public SchedulerDTO() {
		super();
	}

	public String getActenddate() {
		return actenddate;
	}

	public void setActenddate(String actenddate) {
		this.actenddate = actenddate;
	}

	public String getActenddatehh() {
		return actenddatehh;
	}

	public void setActenddatehh(String actenddatehh) {
		this.actenddatehh = actenddatehh;
	}

	public String getActenddatemm() {
		return actenddatemm;
	}

	public void setActenddatemm(String actenddatemm) {
		this.actenddatemm = actenddatemm;
	}

	public String getActenddateop() {
		return actenddateop;
	}

	public void setActenddateop(String actenddateop) {
		this.actenddateop = actenddateop;
	}

	public String getActstartdate() {
		return actstartdate;
	}

	public void setActstartdate(String actstartdate) {
		this.actstartdate = actstartdate;
	}

	public String getActstartdatehh() {
		return actstartdatehh;
	}

	public void setActstartdatehh(String actstartdatehh) {
		this.actstartdatehh = actstartdatehh;
	}

	public String getActstartdatemm() {
		return actstartdatemm;
	}

	public void setActstartdatemm(String actstartdatemm) {
		this.actstartdatemm = actstartdatemm;
	}

	public String getActstartdateop() {
		return actstartdateop;
	}

	public void setActstartdateop(String actstartdateop) {
		this.actstartdateop = actstartdateop;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getStartdatehh() {
		return startdatehh;
	}

	public void setStartdatehh(String startdatehh) {
		this.startdatehh = startdatehh;
	}

	public String getStartdatemm() {
		return startdatemm;
	}

	public void setStartdatemm(String startdatemm) {
		this.startdatemm = startdatemm;
	}

	public String getStartdateop() {
		return startdateop;
	}

	public void setStartdateop(String startdateop) {
		this.startdateop = startdateop;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getScheduleNumber() {
		return scheduleNumber;
	}

	public void setScheduleNumber(String scheduleNumber) {
		this.scheduleNumber = scheduleNumber;
	}

	public String getScheduleEnddate() {
		return scheduleEnddate;
	}

	public void setScheduleEnddate(String scheduleEnddate) {
		this.scheduleEnddate = scheduleEnddate;
	}

	public String getScheduleEndhh() {
		return scheduleEndhh;
	}

	public void setScheduleEndhh(String scheduleEndhh) {
		this.scheduleEndhh = scheduleEndhh;
	}

	public String getScheduleEndmm() {
		return scheduleEndmm;
	}

	public void setScheduleEndmm(String scheduleEndmm) {
		this.scheduleEndmm = scheduleEndmm;
	}

	public String getScheduleEndop() {
		return scheduleEndop;
	}

	public void setScheduleEndop(String scheduleEndop) {
		this.scheduleEndop = scheduleEndop;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getRseType() {
		return rseType;
	}

	public void setRseType(String rseType) {
		this.rseType = rseType;
	}

	public String getRseReason() {
		return rseReason;
	}

	public void setRseReason(String rseReason) {
		this.rseReason = rseReason;
	}

	public String getOldstartdate() {
		return oldstartdate;
	}

	public void setOldstartdate(String oldstartdate) {
		this.oldstartdate = oldstartdate;
	}

	public String getOldstartdatehh() {
		return oldstartdatehh;
	}

	public void setOldstartdatehh(String oldstartdatehh) {
		this.oldstartdatehh = oldstartdatehh;
	}

	public String getOldstartdatemm() {
		return oldstartdatemm;
	}

	public void setOldstartdatemm(String oldstartdatemm) {
		this.oldstartdatemm = oldstartdatemm;
	}

	public String getOldstartdateop() {
		return oldstartdateop;
	}

	public void setOldstartdateop(String oldstartdateop) {
		this.oldstartdateop = oldstartdateop;
	}

}
