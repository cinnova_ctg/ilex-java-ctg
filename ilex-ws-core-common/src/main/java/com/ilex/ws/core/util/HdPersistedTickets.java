package com.ilex.ws.core.util;

import java.io.Serializable;
import java.util.Map;

public class HdPersistedTickets implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String activeTicketId;
	private Map<String, String> wipTickets;

	public Map<String, String> getWipTickets() {
		return wipTickets;
	}

	public void setWipTickets(Map<String, String> wipTickets) {
		this.wipTickets = wipTickets;
	}

	public String getActiveTicketId() {
		return activeTicketId;
	}

	public void setActiveTicketId(String activeTicketId) {
		this.activeTicketId = activeTicketId;
	}

}
