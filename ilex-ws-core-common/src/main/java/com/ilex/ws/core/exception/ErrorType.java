/*
 * <h4>Description</h4>
 * Lists type of custom exceptions
 *
 * @author tarungupta
 */
package com.ilex.ws.core.exception;

public enum ErrorType {

	BUSINESS, SYSTEM;
}
