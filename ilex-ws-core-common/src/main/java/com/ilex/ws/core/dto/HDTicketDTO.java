package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class HDTicketDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String customerName;
	private String appendixName;
	private Long msaId;
	private Long appendixId;
	private String ticketType;
	private Boolean billingStatus;
	private String contactName;
	private String phoneNumber;
	private String emailAddress;
	private String clientEmailAddress;
	private String customerTicketReference;
	private String preferredArrivalTime;
	private String preferredArrivalWindowTo;
	private String preferredArrivalWindowFrom;
	private String category;
	private String shortDescription;
	private String configurationItem;
	private String problemDescription;
	private String manuallyCreationReason;
	private String currentCircuitStatus;
	private String priority;
	private String impact;
	private String urgency;
	private String nextAction;
	private String createTicketNextActionTime;
	private String createTicketNextActionUnit;
	private String createTicketNextActionDate;
	private String efforts;
	private String workNotes;
	private String ticketSource;
	private SiteDetailDTO siteDetailDTO;
	private String ackCubeType;
	private String ticketId;
	private String callerName;
	private String callerPhone;
	private String callerEmail;
	private String siteId;
	private String backUpCktStatus;
	private String customerWorkNotes;
	private String primaryStatus;
	private String ticketNumber;
	private String device;
	private String pointOfFailure;
	private String rootCause;
	private String resolution;
	private List<HDWorkNoteDTO> hdWorkNoteDTOList;
	private List<HDInstallNoteDTO> hdInstallNoteDTOList;

	public List<HDInstallNoteDTO> getHdInstallNoteDTOList() {
		return hdInstallNoteDTOList;
	}

	public void setHdInstallNoteDTOList(
			List<HDInstallNoteDTO> hdInstallNoteDTOList) {
		this.hdInstallNoteDTOList = hdInstallNoteDTOList;
	}

	private List<HDAuditTrailDTO> hdAuditTrailList;
	private String ticketStatus;
	private Boolean lmCompleteFirstCall;
	private String pointOfFailureId;
	private String rootCauseId;
	private String resolutionId;
	private List<HDEffortDTO> hdEffortDTOList;
	private String closingWorkNote;
	private HdBillableDTO hdBillableDTO;
	private boolean error;
	private String errorMessage;
	private WugDetailsDTO wugDetailsDTO;
	private ProvisioningDetailDTO provisioningDetailDTO;
	private String primaryState;
	private Long jobId;
	private String extSystemUpdate;
	private String testNameTemp = "";
	private String isFromMain = "";

	private String assignmentGroupId;
	private String contactTypeId;
	private Date createdOnDate;
	private String createdOnDateString;
	private String providerRefNo;

	private String snErrorWhileClosingTicket;

	// private String client1stContact;
	// private String clientHDPhoneNumber;
	// private String clientHDEmailAddress;
	// private String clientHDContactName;

	private List<MasterSiteAssetsDeployedDTO> deviceAttribList;

	/*
	 * Saves the last values of the given status to generate the email body
	 */

	// public String getClient1stContact() {
	// return client1stContact;
	// }
	//
	// public void setClient1stContact(String client1stContact) {
	// this.client1stContact = client1stContact;
	// }
	//
	// public String getClientHDPhoneNumber() {
	// return clientHDPhoneNumber;
	// }
	//
	// public void setClientHDPhoneNumber(String clientHDPhoneNumber) {
	// this.clientHDPhoneNumber = clientHDPhoneNumber;
	// }
	//
	// public String getClientHDEmailAddress() {
	// return clientHDEmailAddress;
	// }
	//
	// public void setClientHDEmailAddress(String clientHDEmailAddress) {
	// this.clientHDEmailAddress = clientHDEmailAddress;
	// }
	//
	// public String getClientHDContactName() {
	// return clientHDContactName;
	// }
	//
	// public void setClientHDContactName(String clientHDContactName) {
	// this.clientHDContactName = clientHDContactName;
	// }

	private String lastCreateTicketNextActionDate;
	private String lastTicketStatus;
	private String lastNextAction;
	private String lastNextActionDueDate;

	/*
	 * Saves the last values of the given status to generate the email body
	 */

	public String getAssignmentGroupId() {
		return assignmentGroupId;
	}

	public void setAssignmentGroupId(String assignmentGroupId) {
		this.assignmentGroupId = assignmentGroupId;
	}

	public String getContactTypeId() {
		return contactTypeId;
	}

	public void setContactTypeId(String contactTypeId) {
		this.contactTypeId = contactTypeId;
	}

	public Date getCreatedOnDate() {
		return createdOnDate;
	}

	public void setCreatedOnDate(Date createdOnDate) {
		this.createdOnDate = createdOnDate;
	}

	public WugDetailsDTO getWugDetailsDTO() {
		return wugDetailsDTO;
	}

	public void setWugDetailsDTO(WugDetailsDTO wugDetailsDTO) {
		this.wugDetailsDTO = wugDetailsDTO;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public HdBillableDTO getHdBillableDTO() {
		return hdBillableDTO;
	}

	public void setHdBillableDTO(HdBillableDTO hdBillableDTO) {
		this.hdBillableDTO = hdBillableDTO;
	}

	public String getClosingWorkNote() {
		return closingWorkNote;
	}

	public void setClosingWorkNote(String closingWorkNote) {
		this.closingWorkNote = closingWorkNote;
	}

	public String getPointOfFailureId() {
		return pointOfFailureId;
	}

	public void setPointOfFailureId(String pointOfFailureId) {
		this.pointOfFailureId = pointOfFailureId;
	}

	public String getRootCauseId() {
		return rootCauseId;
	}

	public void setRootCauseId(String rootCauseId) {
		this.rootCauseId = rootCauseId;
	}

	public String getResolutionId() {
		return resolutionId;
	}

	public void setResolutionId(String resolutionId) {
		this.resolutionId = resolutionId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public Long getMsaId() {
		return msaId;
	}

	public void setMsaId(Long msaId) {
		this.msaId = msaId;
	}

	public Long getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(Long appendixId) {
		this.appendixId = appendixId;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getCustomerTicketReference() {
		return customerTicketReference;
	}

	public void setCustomerTicketReference(String customerTicketReference) {
		this.customerTicketReference = customerTicketReference;
	}

	public String getPreferredArrivalTime() {
		return preferredArrivalTime;
	}

	public void setPreferredArrivalTime(String preferredArrivalTime) {
		this.preferredArrivalTime = preferredArrivalTime;
	}

	public String getPreferredArrivalWindowTo() {
		return preferredArrivalWindowTo;
	}

	public void setPreferredArrivalWindowTo(String preferredArrivalWindowTo) {
		this.preferredArrivalWindowTo = preferredArrivalWindowTo;
	}

	public String getPreferredArrivalWindowFrom() {
		return preferredArrivalWindowFrom;
	}

	public void setPreferredArrivalWindowFrom(String preferredArrivalWindowFrom) {
		this.preferredArrivalWindowFrom = preferredArrivalWindowFrom;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getConfigurationItem() {
		return configurationItem;
	}

	public void setConfigurationItem(String configurationItem) {
		this.configurationItem = configurationItem;
	}

	public String getProblemDescription() {
		return problemDescription;
	}

	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}

	public String getManuallyCreationReason() {
		return manuallyCreationReason;
	}

	public void setManuallyCreationReason(String manuallyCreationReason) {
		this.manuallyCreationReason = manuallyCreationReason;
	}

	public String getCurrentCircuitStatus() {
		return currentCircuitStatus;
	}

	public void setCurrentCircuitStatus(String currentCircuitStatus) {
		this.currentCircuitStatus = currentCircuitStatus;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public String getNextAction() {
		return nextAction;
	}

	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}

	public String getEfforts() {
		return efforts;
	}

	public void setEfforts(String efforts) {
		this.efforts = efforts;
	}

	public String getWorkNotes() {
		return workNotes;
	}

	public void setWorkNotes(String workNotes) {
		this.workNotes = workNotes;
	}

	public String getTicketSource() {
		return ticketSource;
	}

	public void setTicketSource(String ticketSource) {
		this.ticketSource = ticketSource;
	}

	public Boolean getBillingStatus() {
		return billingStatus;
	}

	public void setBillingStatus(Boolean billingStatus) {
		this.billingStatus = billingStatus;
	}

	public String getCreateTicketNextActionTime() {
		return createTicketNextActionTime;
	}

	public void setCreateTicketNextActionTime(String createTicketNextActionTime) {
		this.createTicketNextActionTime = createTicketNextActionTime;
	}

	public String getCreateTicketNextActionUnit() {
		return createTicketNextActionUnit;
	}

	public void setCreateTicketNextActionUnit(String createTicketNextActionUnit) {
		this.createTicketNextActionUnit = createTicketNextActionUnit;
	}

	public String getCreateTicketNextActionDate() {
		return createTicketNextActionDate;
	}

	public void setCreateTicketNextActionDate(String createTicketNextActionDate) {
		this.createTicketNextActionDate = createTicketNextActionDate;
	}

	public SiteDetailDTO getSiteDetailDTO() {
		return siteDetailDTO;
	}

	public void setSiteDetailDTO(SiteDetailDTO siteDetailDTO) {
		this.siteDetailDTO = siteDetailDTO;
	}

	public String getAckCubeType() {
		return ackCubeType;
	}

	public void setAckCubeType(String ackCubeType) {
		this.ackCubeType = ackCubeType;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getCallerName() {
		return callerName;
	}

	public void setCallerName(String callerName) {
		this.callerName = callerName;
	}

	public String getCallerPhone() {
		return callerPhone;
	}

	public void setCallerPhone(String callerPhone) {
		this.callerPhone = callerPhone;
	}

	public String getCallerEmail() {
		return callerEmail;
	}

	public void setCallerEmail(String callerEmail) {
		this.callerEmail = callerEmail;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getBackUpCktStatus() {
		return backUpCktStatus;
	}

	public void setBackUpCktStatus(String backUpCktStatus) {
		this.backUpCktStatus = backUpCktStatus;
	}

	public String getCustomerWorkNotes() {
		return customerWorkNotes;
	}

	public void setCustomerWorkNotes(String customerWorkNotes) {
		this.customerWorkNotes = customerWorkNotes;

	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getPrimaryStatus() {
		return primaryStatus;
	}

	public void setPrimaryStatus(String primaryStatus) {
		this.primaryStatus = primaryStatus;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getPointOfFailure() {
		return pointOfFailure;
	}

	public void setPointOfFailure(String pointOfFailure) {
		this.pointOfFailure = pointOfFailure;
	}

	public String getRootCause() {
		return rootCause;
	}

	public void setRootCause(String rootCause) {
		this.rootCause = rootCause;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public List<HDWorkNoteDTO> getHdWorkNoteDTOList() {
		return hdWorkNoteDTOList;
	}

	public void setHdWorkNoteDTOList(List<HDWorkNoteDTO> hdWorkNoteDTOList) {
		this.hdWorkNoteDTOList = hdWorkNoteDTOList;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public Boolean getLmCompleteFirstCall() {
		return lmCompleteFirstCall;
	}

	public void setLmCompleteFirstCall(Boolean lmCompleteFirstCall) {
		this.lmCompleteFirstCall = lmCompleteFirstCall;
	}

	public List<HDEffortDTO> getHdEffortDTOList() {
		return hdEffortDTOList;
	}

	public void setHdEffortDTOList(List<HDEffortDTO> hdEffortDTOList) {
		this.hdEffortDTOList = hdEffortDTOList;
	}

	public ProvisioningDetailDTO getProvisioningDetailDTO() {
		return provisioningDetailDTO;
	}

	public void setProvisioningDetailDTO(
			ProvisioningDetailDTO provisioningDetailDTO) {
		this.provisioningDetailDTO = provisioningDetailDTO;
	}

	public String getPrimaryState() {
		return primaryState;
	}

	public void setPrimaryState(String primaryState) {
		this.primaryState = primaryState;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public String getExtSystemUpdate() {
		return extSystemUpdate;
	}

	public void setExtSystemUpdate(String extSystemUpdate) {
		this.extSystemUpdate = extSystemUpdate;
	}

	public String getTestNameTemp() {
		return testNameTemp;
	}

	public void setTestNameTemp(String testNameTemp) {
		this.testNameTemp = testNameTemp;
	}

	public String getIsFromMain() {
		return isFromMain;
	}

	public void setIsFromMain(String isFromMain) {
		this.isFromMain = isFromMain;
	}

	public List<HDAuditTrailDTO> getHdAuditTrailList() {
		return hdAuditTrailList;
	}

	public void setHdAuditTrailList(List<HDAuditTrailDTO> hdAuditTrailList) {
		this.hdAuditTrailList = hdAuditTrailList;
	}

	public String getCreatedOnDateString() {
		return createdOnDateString;
	}

	public void setCreatedOnDateString(String createdOnDateString) {
		this.createdOnDateString = createdOnDateString;
	}

	public String getLastCreateTicketNextActionDate() {
		return lastCreateTicketNextActionDate;
	}

	public void setLastCreateTicketNextActionDate(
			String lastCreateTicketNextActionDate) {
		this.lastCreateTicketNextActionDate = lastCreateTicketNextActionDate;
	}

	public String getLastTicketStatus() {
		return lastTicketStatus;
	}

	public void setLastTicketStatus(String lastTicketStatus) {
		this.lastTicketStatus = lastTicketStatus;
	}

	public String getLastNextAction() {
		return lastNextAction;
	}

	public void setLastNextAction(String lastNextAction) {
		this.lastNextAction = lastNextAction;
	}

	public String getLastNextActionDueDate() {
		return lastNextActionDueDate;
	}

	public void setLastNextActionDueDate(String lastNextActionDueDate) {
		this.lastNextActionDueDate = lastNextActionDueDate;
	}

	public String getProviderRefNo() {
		return providerRefNo;
	}

	public void setProviderRefNo(String providerRefNo) {
		this.providerRefNo = providerRefNo;
	}

	public String getSnErrorWhileClosingTicket() {
		return snErrorWhileClosingTicket;
	}

	public void setSnErrorWhileClosingTicket(String snErrorWhileClosingTicket) {
		this.snErrorWhileClosingTicket = snErrorWhileClosingTicket;
	}

	public List<MasterSiteAssetsDeployedDTO> getDeviceAttribList() {
		return deviceAttribList;
	}

	public void setDeviceAttribList(
			List<MasterSiteAssetsDeployedDTO> deviceAttribList) {
		this.deviceAttribList = deviceAttribList;
	}

	public String getClientEmailAddress() {
		return clientEmailAddress;
	}

	public void setClientEmailAddress(String clientEmailAddress) {
		this.clientEmailAddress = clientEmailAddress;
	}
}
