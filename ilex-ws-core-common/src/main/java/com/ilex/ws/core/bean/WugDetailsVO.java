package com.ilex.ws.core.bean;

import java.io.Serializable;
import java.util.Date;

public class WugDetailsVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date wugDateDB;
	private String wugState;
	private String backUpStatus;
	private String wugHelpDesk;
	private String wugApcAddress;
	private String wugModemType;
	private String wugCircuitIp;

	public String getWugHelpDesk() {
		return wugHelpDesk;
	}

	public void setWugHelpDesk(String wugHelpDesk) {
		this.wugHelpDesk = wugHelpDesk;
	}

	public String getWugApcAddress() {
		return wugApcAddress;
	}

	public void setWugApcAddress(String wugApcAddress) {
		this.wugApcAddress = wugApcAddress;
	}

	public String getWugModemType() {
		return wugModemType;
	}

	public void setWugModemType(String wugModemType) {
		this.wugModemType = wugModemType;
	}

	public String getWugCircuitIp() {
		return wugCircuitIp;
	}

	public void setWugCircuitIp(String wugCircuitIp) {
		this.wugCircuitIp = wugCircuitIp;
	}

	public Date getWugDateDB() {
		return wugDateDB;
	}

	public void setWugDateDB(Date wugDateDB) {
		this.wugDateDB = wugDateDB;
	}

	public String getWugState() {
		return wugState;
	}

	public void setWugState(String wugState) {
		this.wugState = wugState;
	}

	public String getBackUpStatus() {
		return backUpStatus;
	}

	public void setBackUpStatus(String backUpStatus) {
		this.backUpStatus = backUpStatus;
	}

}
