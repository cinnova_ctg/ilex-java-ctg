package com.ilex.ws.core.bean;

import java.io.Serializable;

public class IlexTicketVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String serviceNowTicketId;
	private int ilexTicketId;
	private String serviceNowSysId;
	private String category;
	private String incidentState;
	private String shortDescription;
	private String active;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getIncidentState() {
		return incidentState;
	}

	public void setIncidentState(String incidentState) {
		this.incidentState = incidentState;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getServiceNowTicketId() {
		return serviceNowTicketId;
	}

	public void setServiceNowTicketId(String serviceNowTicketId) {
		this.serviceNowTicketId = serviceNowTicketId;
	}

	public int getIlexTicketId() {
		return ilexTicketId;
	}

	public void setIlexTicketId(int ilexTicketId) {
		this.ilexTicketId = ilexTicketId;
	}

	public String getServiceNowSysId() {
		return serviceNowSysId;
	}

	public void setServiceNowSysId(String serviceNowSysId) {
		this.serviceNowSysId = serviceNowSysId;
	}

}
