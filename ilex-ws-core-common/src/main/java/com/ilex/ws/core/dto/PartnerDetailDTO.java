package com.ilex.ws.core.dto;

import java.io.Serializable;

public class PartnerDetailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String partnerId = null;
	private String partnerName = null;
	private String partnerAddress1 = null;
	private String partnerAddress2 = null;
	private String partnerCity = null;
	private String partnerState = null;
	private String partnerZipCode = null;
	private String partnerPhone = null;
	private String partnerPriName = null;
	private String partnerPriPhone = null;
	private String partnerPriCellPhone = null;
	private String partnerPriEmail = null;
	private String partnerType = null;
	private String partnerCellPhone = null;

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getPartnerAddress1() {
		return partnerAddress1;
	}

	public void setPartnerAddress1(String partnerAddress1) {
		this.partnerAddress1 = partnerAddress1;
	}

	public String getPartnerAddress2() {
		return partnerAddress2;
	}

	public void setPartnerAddress2(String partnerAddress2) {
		this.partnerAddress2 = partnerAddress2;
	}

	public String getPartnerCity() {
		return partnerCity;
	}

	public void setPartnerCity(String partnerCity) {
		this.partnerCity = partnerCity;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerPhone() {
		return partnerPhone;
	}

	public void setPartnerPhone(String partnerPhone) {
		this.partnerPhone = partnerPhone;
	}

	public String getPartnerPriCellPhone() {
		return partnerPriCellPhone;
	}

	public void setPartnerPriCellPhone(String partnerPriCellPhone) {
		this.partnerPriCellPhone = partnerPriCellPhone;
	}

	public String getPartnerPriEmail() {
		return partnerPriEmail;
	}

	public void setPartnerPriEmail(String partnerPriEmail) {
		this.partnerPriEmail = partnerPriEmail;
	}

	public String getPartnerPriName() {
		return partnerPriName;
	}

	public void setPartnerPriName(String partnerPriName) {
		this.partnerPriName = partnerPriName;
	}

	public String getPartnerPriPhone() {
		return partnerPriPhone;
	}

	public void setPartnerPriPhone(String partnerPriPhone) {
		this.partnerPriPhone = partnerPriPhone;
	}

	public String getPartnerZipCode() {
		return partnerZipCode;
	}

	public void setPartnerZipCode(String partnerZipCode) {
		this.partnerZipCode = partnerZipCode;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getPartnerState() {
		return partnerState;
	}

	public void setPartnerState(String partnerState) {
		this.partnerState = partnerState;
	}

	public String getPartnerCellPhone() {
		return partnerCellPhone;
	}

	public void setPartnerCellPhone(String partnerCellPhone) {
		this.partnerCellPhone = partnerCellPhone;
	}

}
