package com.ilex.ws.core.dto;

import java.io.Serializable;

public class NewWebTicketDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String msa_name = null;
	private String appendix_id = null;
	private String ticket_number = null;
	private String ticket_id = null;
	private String site_name = null;
	private String site_number = null;
	
	private String requestor_name = null;
	private String request_received_date = null;
	private String request_received_time = null;
	private String preferred_arrival_date = null;
	private String preferred_arrival_time = null;
	
	private String jobid = null;
	private String owner_name = null;
	private String owner_id = null;
	
	private String projectManager = null;
	
	
	public String getProjectManager() {
		return projectManager;
	}
	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}
	public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}
	public String getMsa_name() {
		return msa_name;
	}
	public void setMsa_name(String msa_name) {
		this.msa_name = msa_name;
	}
	public String getOwner_name() {
		return owner_name;
	}
	public void setOwner_name(String owner_name) {
		this.owner_name = owner_name;
	}
	public String getPreferred_arrival_date() {
		return preferred_arrival_date;
	}
	public void setPreferred_arrival_date(String preferred_arrival_date) {
		this.preferred_arrival_date = preferred_arrival_date;
	}
	public String getPreferred_arrival_time() {
		return preferred_arrival_time;
	}
	public void setPreferred_arrival_time(String preferred_arrival_time) {
		this.preferred_arrival_time = preferred_arrival_time;
	}
	public String getRequest_received_date() {
		return request_received_date;
	}
	public void setRequest_received_date(String request_received_date) {
		this.request_received_date = request_received_date;
	}
	public String getRequest_received_time() {
		return request_received_time;
	}
	public void setRequest_received_time(String request_received_time) {
		this.request_received_time = request_received_time;
	}
	public String getRequestor_name() {
		return requestor_name;
	}
	public void setRequestor_name(String requestor_name) {
		this.requestor_name = requestor_name;
	}
	public String getSite_name() {
		return site_name;
	}
	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}
	public String getSite_number() {
		return site_number;
	}
	public void setSite_number(String site_number) {
		this.site_number = site_number;
	}
	public String getTicket_number() {
		return ticket_number;
	}
	public void setTicket_number(String ticket_number) {
		this.ticket_number = ticket_number;
	}
	public String getOwner_id() {
		return owner_id;
	}
	public void setOwner_id(String owner_id) {
		this.owner_id = owner_id;
	}
	public String getTicket_id() {
		return ticket_id;
	}
	public void setTicket_id(String ticket_id) {
		this.ticket_id = ticket_id;
	}
	public String getAppendix_id() {
		return appendix_id;
	}
	public void setAppendix_id(String appendix_id) {
		this.appendix_id = appendix_id;
	}

}


