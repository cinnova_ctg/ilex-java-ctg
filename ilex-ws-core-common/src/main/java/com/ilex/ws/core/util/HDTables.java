package com.ilex.ws.core.util;

/**
 * The Enum HDTables.
 */
public enum HDTables {

	/** The hd pending acnowledgement for customers. */
	HD_PENDING_ACNOWLEDGEMENT_CUST("hdPendingAckCustTable"),

	/** The hd pending acnowledgement for alerts. */
	HD_PENDING_ACNOWLEDGEMENT_ALERT("hdPendingAckAlertTable"),

	/** The hd pending acnowledgement for monitoring. */
	HD_PENDING_ACNOWLEDGEMENT_MONITOR("hdPendingAckMonitorTable"),

	HD_PENDING_ACNOWLEDGEMENT_MONITOR_TIER2("hdPendingAckMonitorTier2Table"),

	HD_MONITOR_DATE_TIME("monitoringDateTimeDetailsTable"),

	/** The hd wip. */
	HD_WIP("hdWipTable"),

	/** The hd wip overdue. */
	HD_WIP_OVERDUE("hdWipOverDueTable"),

	/** The hd wip next action. */
	HD_WIP_NEXT_ACTION("hdWipNextActionTable"),

	HD_WIP_RESOLVED("hdResolvedTable"),
	/** The hd wip id that is being used in database. */
	HD_WIP_ID("False"),

	/** The hd wip overdue id that is being used in database. */
	HD_WIP_OVERDUE_ID("True"),

	/** The hd wip next action id that is being used in database. */
	HD_WIP_NEXT_ACTION_ID("True");

	HDTables(String tableId) {
		this.tableId = tableId;
	}

	private String tableId;

	public String getCubeId() {
		return tableId;
	}

	/**
	 * Gets the table from table id.
	 * 
	 * @param cubeId
	 *            the cube id
	 * @return the table from table id
	 */
	public static HDTables getTableFromTableId(String tableId) {
		if (tableId != null) {
			for (HDTables HDTablesType : HDTables.values()) {
				if (tableId.equalsIgnoreCase(HDTablesType.tableId)) {
					return HDTablesType;
				}
			}
		}
		return null;
	}
}
