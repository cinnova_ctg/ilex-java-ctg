package com.ilex.ws.core.bean;

import java.io.Serializable;

public class ServiceNowTicketDetailVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String ticketNumber;
	private String workNotes;
	private String comments;

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getWorkNotes() {
		return workNotes;
	}

	public void setWorkNotes(String workNotes) {
		this.workNotes = workNotes;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
