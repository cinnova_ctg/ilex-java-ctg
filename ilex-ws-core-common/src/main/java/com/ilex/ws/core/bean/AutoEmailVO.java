package com.ilex.ws.core.bean;

import java.io.Serializable;

public class AutoEmailVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String conversationIdentifier;
	private String subject;
	private String messageText;
	private String fromAddress;
	private String toAddresses;
	private String ccAddresses;
	private boolean conversationFromIlex;
	private String date;
	private String messageCount;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public String getToAddresses() {
		return toAddresses;
	}

	public void setToAddresses(String toAddresses) {
		this.toAddresses = toAddresses;
	}

	public String getCcAddresses() {
		return ccAddresses;
	}

	public void setCcAddresses(String ccAddresses) {
		this.ccAddresses = ccAddresses;
	}

	public boolean isConversationFromIlex() {
		return conversationFromIlex;
	}

	public void setConversationFromIlex(boolean conversationFromIlex) {
		this.conversationFromIlex = conversationFromIlex;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getConversationIdentifier() {
		return conversationIdentifier;
	}

	public void setConversationIdentifier(String conversationIdentifier) {
		this.conversationIdentifier = conversationIdentifier;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMessageCount() {
		return messageCount;
	}

	public void setMessageCount(String messageCount) {
		this.messageCount = messageCount;
	}

}
