package com.ilex.ws.core.dto;

import java.io.Serializable;

public class MasterSiteAssetsDeployedDTO implements Serializable {

	private String date;
	private String clValue;
	private String quantity;
	private String srNumber;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getClValue() {
		return clValue;
	}

	public void setClValue(String clValue) {
		this.clValue = clValue;
	}

	public String getSrNumber() {
		return srNumber;
	}

	public void setSrNumber(String srNumber) {
		this.srNumber = srNumber;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

}
