package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.List;

public class HDCustomerViewDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	List<HDCustomerSetupDTO> hdCustomerSetupDTOList;
	private Integer pageCount;
	private Integer pageIndex;

	public List<HDCustomerSetupDTO> getHdCustomerSetupDTOList() {
		return hdCustomerSetupDTOList;
	}

	public void setHdCustomerSetupDTOList(
			List<HDCustomerSetupDTO> hdCustomerSetupDTOList) {
		this.hdCustomerSetupDTOList = hdCustomerSetupDTOList;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public Integer getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

}
