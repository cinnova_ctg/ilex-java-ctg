package com.ilex.ws.core.dto;

import java.io.Serializable;

public class TicketDispatchDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long userId;
	private String userName;
	private String companyName;
	private String email;
	private Boolean sendEmail;
	private String requestorName;
	private String requestorPhone;
	private String siteNumber;
	private String sitePhone;
	private String siteLocation;
	private String siteAddress;
	private String siteCity;
	private String siteCountry;
	private String siteState;
	private String siteZipCode;
	private String siteName;
	private String poNumber;
	private Boolean onsiteFieldRequest;
	private Boolean remoteHelpDeskRequest;
	private Long appendixId;
	private Long msaId;

	private String preferredDate;
	private String preferredHour;
	private String preferredMinute;
	private String preferredIsAm;
	private String callCriticality;
	private String windowFromDate;
	private String windowFromHour;
	private String windowFromMinute;
	private String windowFromIsAm;
	private String windowToDate;
	private String windowToHour;
	private String windowToMinute;
	private String windowToIsAm;
	private String problemSummary;
	private String specialInstructions;
	private String pocName;
	private String pocPhone;
	private String pocName2;
	private String pocPhone2;
	private Boolean standBy;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getSendEmail() {
		return sendEmail;
	}

	public void setSendEmail(Boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getRequestorPhone() {
		return requestorPhone;
	}

	public void setRequestorPhone(String requestorPhone) {
		this.requestorPhone = requestorPhone;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getSitePhone() {
		return sitePhone;
	}

	public void setSitePhone(String sitePhone) {
		this.sitePhone = sitePhone;
	}

	public String getSiteLocation() {
		return siteLocation;
	}

	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteCountry() {
		return siteCountry;
	}

	public void setSiteCountry(String siteCountry) {
		this.siteCountry = siteCountry;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getSiteZipCode() {
		return siteZipCode;
	}

	public void setSiteZipCode(String siteZipCode) {
		this.siteZipCode = siteZipCode;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public Boolean getOnsiteFieldRequest() {
		return onsiteFieldRequest;
	}

	public void setOnsiteFieldRequest(Boolean onsiteFieldRequest) {
		this.onsiteFieldRequest = onsiteFieldRequest;
	}

	public Boolean getRemoteHelpDeskRequest() {
		return remoteHelpDeskRequest;
	}

	public void setRemoteHelpDeskRequest(Boolean remoteHelpDeskRequest) {
		this.remoteHelpDeskRequest = remoteHelpDeskRequest;
	}

	public Long getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(Long appendixId) {
		this.appendixId = appendixId;
	}

	public Long getMsaId() {
		return msaId;
	}

	public void setMsaId(Long msaId) {
		this.msaId = msaId;
	}

	public String getPreferredDate() {
		return preferredDate;
	}

	public void setPreferredDate(String preferredDate) {
		this.preferredDate = preferredDate;
	}

	public String getPreferredHour() {
		return preferredHour;
	}

	public void setPreferredHour(String preferredHour) {
		this.preferredHour = preferredHour;
	}

	public String getPreferredMinute() {
		return preferredMinute;
	}

	public void setPreferredMinute(String preferredMinute) {
		this.preferredMinute = preferredMinute;
	}

	public String getPreferredIsAm() {
		return preferredIsAm;
	}

	public void setPreferredIsAm(String preferredIsAm) {
		this.preferredIsAm = preferredIsAm;
	}

	public String getCallCriticality() {
		return callCriticality;
	}

	public void setCallCriticality(String callCriticality) {
		this.callCriticality = callCriticality;
	}

	public String getWindowFromDate() {
		return windowFromDate;
	}

	public void setWindowFromDate(String windowFromDate) {
		this.windowFromDate = windowFromDate;
	}

	public String getWindowFromHour() {
		return windowFromHour;
	}

	public void setWindowFromHour(String windowFromHour) {
		this.windowFromHour = windowFromHour;
	}

	public String getWindowFromMinute() {
		return windowFromMinute;
	}

	public void setWindowFromMinute(String windowFromMinute) {
		this.windowFromMinute = windowFromMinute;
	}

	public String getWindowFromIsAm() {
		return windowFromIsAm;
	}

	public void setWindowFromIsAm(String windowFromIsAm) {
		this.windowFromIsAm = windowFromIsAm;
	}

	public String getWindowToDate() {
		return windowToDate;
	}

	public void setWindowToDate(String windowToDate) {
		this.windowToDate = windowToDate;
	}

	public String getWindowToHour() {
		return windowToHour;
	}

	public void setWindowToHour(String windowToHour) {
		this.windowToHour = windowToHour;
	}

	public String getWindowToMinute() {
		return windowToMinute;
	}

	public void setWindowToMinute(String windowToMinute) {
		this.windowToMinute = windowToMinute;
	}

	public String getWindowToIsAm() {
		return windowToIsAm;
	}

	public void setWindowToIsAm(String windowToIsAm) {
		this.windowToIsAm = windowToIsAm;
	}

	public String getProblemSummary() {
		return problemSummary;
	}

	public void setProblemSummary(String problemSummary) {
		this.problemSummary = problemSummary;
	}

	public String getSpecialInstructions() {
		return specialInstructions;
	}

	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public String getPocName() {
		return pocName;
	}

	public void setPocName(String pocName) {
		this.pocName = pocName;
	}

	public String getPocPhone() {
		return pocPhone;
	}

	public void setPocPhone(String pocPhone) {
		this.pocPhone = pocPhone;
	}

	public String getPocName2() {
		return pocName2;
	}

	public void setPocName2(String pocName2) {
		this.pocName2 = pocName2;
	}

	public String getPocPhone2() {
		return pocPhone2;
	}

	public void setPocPhone2(String pocPhone2) {
		this.pocPhone2 = pocPhone2;
	}

	public Boolean getStandBy() {
		return standBy;
	}

	public void setStandBy(Boolean standBy) {
		this.standBy = standBy;
	}

}
