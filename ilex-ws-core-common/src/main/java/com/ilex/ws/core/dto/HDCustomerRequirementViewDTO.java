package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.List;

public class HDCustomerRequirementViewDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<HDCustomerRequirementDTO> customers;
	private Integer pageCount;
	private Integer pageIndex;

	public List<HDCustomerRequirementDTO> getCustomers() {
		return customers;
	}

	public void setCustomers(List<HDCustomerRequirementDTO> customers) {
		this.customers = customers;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public Integer getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

}
