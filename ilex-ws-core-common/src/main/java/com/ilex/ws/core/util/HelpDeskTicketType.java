package com.ilex.ws.core.util;

public enum HelpDeskTicketType {

	ILEX_TICKET("0"), SERVICE_NOW_TICKET("1");
	private String value;

	HelpDeskTicketType(String value) {
		this.setValue(value);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {

		switch (this) {
		case ILEX_TICKET:
			return ILEX_TICKET.getValue();
		case SERVICE_NOW_TICKET:
			return SERVICE_NOW_TICKET.getValue();
		}
		return super.toString();

	}

}
