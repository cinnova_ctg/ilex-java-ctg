package com.ilex.ws.core.dto;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class NetMedXIntegrationDTO.
 */
public class NetMedXIntegrationDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user name. */
	private String userName;

	/** The user pass. */
	private String userPass;

	/** The appendixid. */
	private Integer appendixid;

	/** The site_number. */
	private String site_number;

	/** The site_phone. */
	private String site_phone;

	/** The site_worklocation. */
	private String site_worklocation;

	/** The po_number. */
	private String po_number;

	/** The site_address. */
	private String site_address;

	/** The site_city. */
	private String site_city;

	/** The site_state. */
	private String site_state;

	/** The site_zipcode. */
	private String site_zipcode;

	/** The site_name. */
	private String site_name;

	/** The email. */
	private String email;

	/** The contact_person. */
	private String contact_person;

	/** The contact_phone. */
	private String contact_phone;

	/** The primary_ name. */
	private String primary_Name;

	/** The pri_site_phone. */
	private String pri_site_phone;

	/** The secondary_ name. */
	private String secondary_Name;

	/** The secondary_phone. */
	private String secondary_phone;

	/** The requested_date. */
	private String requested_date;

	/** The criticality. */
	private String criticality;

	/** The arrivalwindowstartdate. */
	private String arrivalwindowstartdate;

	/** The arrivalwindowenddate. */
	private String arrivalwindowenddate;

	/** The arrivaldate. */
	private String arrivaldate;

	/** The problem_description. */
	private String problem_description;

	/** The special_instruction. */
	private String special_instruction;

	/** The stand_by. */
	private String stand_by;

	/** The other_email_info. */
	private String other_email_info;

	/** The cust_specific_fields. */
	private String cust_specific_fields;

	/** The silver_bucket. */
	private String silver_bucket;

	/**
	 * Gets the appendixid.
	 * 
	 * @return the appendixid
	 */
	public int getAppendixid() {
		return appendixid;
	}

	/**
	 * Sets the appendixid.
	 * 
	 * @param appendixid
	 *            the new appendixid
	 */
	public void setAppendixid(int appendixid) {
		this.appendixid = appendixid;
	}

	/**
	 * Gets the site_number.
	 * 
	 * @return the site_number
	 */
	public String getSite_number() {
		return site_number;
	}

	/**
	 * Sets the site_number.
	 * 
	 * @param site_number
	 *            the new site_number
	 */
	public void setSite_number(String site_number) {
		this.site_number = site_number;
	}

	/**
	 * Gets the site_phone.
	 * 
	 * @return the site_phone
	 */
	public String getSite_phone() {
		return site_phone;
	}

	/**
	 * Sets the site_phone.
	 * 
	 * @param site_phone
	 *            the new site_phone
	 */
	public void setSite_phone(String site_phone) {
		this.site_phone = site_phone;
	}

	/**
	 * Gets the site_worklocation.
	 * 
	 * @return the site_worklocation
	 */
	public String getSite_worklocation() {
		return site_worklocation;
	}

	/**
	 * Sets the site_worklocation.
	 * 
	 * @param site_worklocation
	 *            the new site_worklocation
	 */
	public void setSite_worklocation(String site_worklocation) {
		this.site_worklocation = site_worklocation;
	}

	/**
	 * Gets the po_number.
	 * 
	 * @return the po_number
	 */
	public String getPo_number() {
		return po_number;
	}

	/**
	 * Sets the po_number.
	 * 
	 * @param po_number
	 *            the new po_number
	 */
	public void setPo_number(String po_number) {
		this.po_number = po_number;
	}

	/**
	 * Gets the site_address.
	 * 
	 * @return the site_address
	 */
	public String getSite_address() {
		return site_address;
	}

	/**
	 * Sets the site_address.
	 * 
	 * @param site_address
	 *            the new site_address
	 */
	public void setSite_address(String site_address) {
		this.site_address = site_address;
	}

	/**
	 * Gets the site_city.
	 * 
	 * @return the site_city
	 */
	public String getSite_city() {
		return site_city;
	}

	/**
	 * Sets the site_city.
	 * 
	 * @param site_city
	 *            the new site_city
	 */
	public void setSite_city(String site_city) {
		this.site_city = site_city;
	}

	/**
	 * Gets the site_state.
	 * 
	 * @return the site_state
	 */
	public String getSite_state() {
		return site_state;
	}

	/**
	 * Sets the site_state.
	 * 
	 * @param site_state
	 *            the new site_state
	 */
	public void setSite_state(String site_state) {
		this.site_state = site_state;
	}

	/**
	 * Gets the site_zipcode.
	 * 
	 * @return the site_zipcode
	 */
	public String getSite_zipcode() {
		return site_zipcode;
	}

	/**
	 * Sets the site_zipcode.
	 * 
	 * @param site_zipcode
	 *            the new site_zipcode
	 */
	public void setSite_zipcode(String site_zipcode) {
		this.site_zipcode = site_zipcode;
	}

	/**
	 * Gets the site_name.
	 * 
	 * @return the site_name
	 */
	public String getSite_name() {
		return site_name;
	}

	/**
	 * Sets the site_name.
	 * 
	 * @param site_name
	 *            the new site_name
	 */
	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	/**
	 * Gets the email.
	 * 
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 * 
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the contact_person.
	 * 
	 * @return the contact_person
	 */
	public String getContact_person() {
		return contact_person;
	}

	/**
	 * Sets the contact_person.
	 * 
	 * @param contact_person
	 *            the new contact_person
	 */
	public void setContact_person(String contact_person) {
		this.contact_person = contact_person;
	}

	/**
	 * Gets the contact_phone.
	 * 
	 * @return the contact_phone
	 */
	public String getContact_phone() {
		return contact_phone;
	}

	/**
	 * Sets the contact_phone.
	 * 
	 * @param contact_phone
	 *            the new contact_phone
	 */
	public void setContact_phone(String contact_phone) {
		this.contact_phone = contact_phone;
	}

	/**
	 * Gets the primary_ name.
	 * 
	 * @return the primary_ name
	 */
	public String getPrimary_Name() {
		return primary_Name;
	}

	/**
	 * Sets the primary_ name.
	 * 
	 * @param primary_Name
	 *            the new primary_ name
	 */
	public void setPrimary_Name(String primary_Name) {
		this.primary_Name = primary_Name;
	}

	/**
	 * Gets the pri_site_phone.
	 * 
	 * @return the pri_site_phone
	 */
	public String getPri_site_phone() {
		return pri_site_phone;
	}

	/**
	 * Sets the pri_site_phone.
	 * 
	 * @param pri_site_phone
	 *            the new pri_site_phone
	 */
	public void setPri_site_phone(String pri_site_phone) {
		this.pri_site_phone = pri_site_phone;
	}

	/**
	 * Gets the secondary_ name.
	 * 
	 * @return the secondary_ name
	 */
	public String getSecondary_Name() {
		return secondary_Name;
	}

	/**
	 * Sets the secondary_ name.
	 * 
	 * @param secondary_Name
	 *            the new secondary_ name
	 */
	public void setSecondary_Name(String secondary_Name) {
		this.secondary_Name = secondary_Name;
	}

	/**
	 * Gets the secondary_phone.
	 * 
	 * @return the secondary_phone
	 */
	public String getSecondary_phone() {
		return secondary_phone;
	}

	/**
	 * Sets the secondary_phone.
	 * 
	 * @param secondary_phone
	 *            the new secondary_phone
	 */
	public void setSecondary_phone(String secondary_phone) {
		this.secondary_phone = secondary_phone;
	}

	/**
	 * Gets the requested_date.
	 * 
	 * @return the requested_date
	 */
	public String getRequested_date() {
		return requested_date;
	}

	/**
	 * Sets the requested_date.
	 * 
	 * @param requested_date
	 *            the new requested_date
	 */
	public void setRequested_date(String requested_date) {
		this.requested_date = requested_date;
	}

	/**
	 * Gets the criticality.
	 * 
	 * @return the criticality
	 */
	public String getCriticality() {
		return criticality;
	}

	/**
	 * Sets the criticality.
	 * 
	 * @param criticality
	 *            the new criticality
	 */
	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}

	/**
	 * Gets the arrivalwindowstartdate.
	 * 
	 * @return the arrivalwindowstartdate
	 */
	public String getArrivalwindowstartdate() {
		return arrivalwindowstartdate;
	}

	/**
	 * Sets the arrivalwindowstartdate.
	 * 
	 * @param arrivalwindowstartdate
	 *            the new arrivalwindowstartdate
	 */
	public void setArrivalwindowstartdate(String arrivalwindowstartdate) {
		this.arrivalwindowstartdate = arrivalwindowstartdate;
	}

	/**
	 * Gets the arrivalwindowenddate.
	 * 
	 * @return the arrivalwindowenddate
	 */
	public String getArrivalwindowenddate() {
		return arrivalwindowenddate;
	}

	/**
	 * Sets the arrivalwindowenddate.
	 * 
	 * @param arrivalwindowenddate
	 *            the new arrivalwindowenddate
	 */
	public void setArrivalwindowenddate(String arrivalwindowenddate) {
		this.arrivalwindowenddate = arrivalwindowenddate;
	}

	/**
	 * Gets the arrivaldate.
	 * 
	 * @return the arrivaldate
	 */
	public String getArrivaldate() {
		return arrivaldate;
	}

	/**
	 * Sets the arrivaldate.
	 * 
	 * @param arrivaldate
	 *            the new arrivaldate
	 */
	public void setArrivaldate(String arrivaldate) {
		this.arrivaldate = arrivaldate;
	}

	/**
	 * Gets the problem_description.
	 * 
	 * @return the problem_description
	 */
	public String getProblem_description() {
		return problem_description;
	}

	/**
	 * Sets the problem_description.
	 * 
	 * @param problem_description
	 *            the new problem_description
	 */
	public void setProblem_description(String problem_description) {
		this.problem_description = problem_description;
	}

	/**
	 * Gets the special_instruction.
	 * 
	 * @return the special_instruction
	 */
	public String getSpecial_instruction() {
		return special_instruction;
	}

	/**
	 * Sets the special_instruction.
	 * 
	 * @param special_instruction
	 *            the new special_instruction
	 */
	public void setSpecial_instruction(String special_instruction) {
		this.special_instruction = special_instruction;
	}

	/**
	 * Gets the stand_by.
	 * 
	 * @return the stand_by
	 */
	public String getStand_by() {
		return stand_by;
	}

	/**
	 * Sets the stand_by.
	 * 
	 * @param stand_by
	 *            the new stand_by
	 */
	public void setStand_by(String stand_by) {
		this.stand_by = stand_by;
	}

	/**
	 * Gets the other_email_info.
	 * 
	 * @return the other_email_info
	 */
	public String getOther_email_info() {
		return other_email_info;
	}

	/**
	 * Sets the other_email_info.
	 * 
	 * @param other_email_info
	 *            the new other_email_info
	 */
	public void setOther_email_info(String other_email_info) {
		this.other_email_info = other_email_info;
	}

	/**
	 * Gets the cust_specific_fields.
	 * 
	 * @return the cust_specific_fields
	 */
	public String getCust_specific_fields() {
		return cust_specific_fields;
	}

	/**
	 * Sets the cust_specific_fields.
	 * 
	 * @param cust_specific_fields
	 *            the new cust_specific_fields
	 */
	public void setCust_specific_fields(String cust_specific_fields) {
		this.cust_specific_fields = cust_specific_fields;
	}

	/**
	 * Gets the silver_bucket.
	 * 
	 * @return the silver_bucket
	 */
	public String getSilver_bucket() {
		return silver_bucket;
	}

	/**
	 * Sets the silver_bucket.
	 * 
	 * @param silver_bucket
	 *            the new silver_bucket
	 */
	public void setSilver_bucket(String silver_bucket) {
		this.silver_bucket = silver_bucket;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPass() {
		return userPass;
	}

	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}

}
