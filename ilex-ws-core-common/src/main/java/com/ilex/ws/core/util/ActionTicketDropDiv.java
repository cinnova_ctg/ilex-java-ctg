package com.ilex.ws.core.util;

public enum ActionTicketDropDiv {

	WIP_1("WIP1"), WIP_2("WIP2"), WIP_3("WIP3"), WIP_4("WIP4"), WIP_5("WIP5"), WIP_6(
			"WIP6"), WIP_7("WIP7"), WIP_8("WIP8");

	private String divId;

	ActionTicketDropDiv(String divId) {
		this.divId = divId;
	}

	public String getDivId() {
		return divId;
	}
}
