package com.ilex.ws.core.dto;

import java.io.Serializable;

public class HDTableParentDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HDTableHeaderDTO hdWIPTableDTO;
	private HDTableHeaderDTO hdPendingAckDTO;
	private int leftDivWidth;
	private int rightDivWidth;

	public int getLeftDivWidth() {
		return leftDivWidth;
	}

	public void setLeftDivWidth(int leftDivWidth) {
		this.leftDivWidth = leftDivWidth;
	}

	public int getRightDivWidth() {
		return rightDivWidth;
	}

	public void setRightDivWidth(int rightDivWidth) {
		this.rightDivWidth = rightDivWidth;
	}

	public HDTableHeaderDTO getHdWIPTableDTO() {
		return hdWIPTableDTO;
	}

	public void setHdWIPTableDTO(HDTableHeaderDTO hdWIPTableDTO) {
		this.hdWIPTableDTO = hdWIPTableDTO;
	}

	public HDTableHeaderDTO getHdPendingAckDTO() {
		return hdPendingAckDTO;
	}

	public void setHdPendingAckDTO(HDTableHeaderDTO hdPendingAckDTO) {
		this.hdPendingAckDTO = hdPendingAckDTO;
	}

}
