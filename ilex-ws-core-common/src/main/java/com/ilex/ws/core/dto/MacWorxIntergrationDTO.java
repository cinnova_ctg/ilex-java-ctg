package com.ilex.ws.core.dto;

import java.io.Serializable;

public class MacWorxIntergrationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer macxProjectId;
	private String macxRequestor;
	private String macxSiteNumber;
	private String macxSiteLocation;

	public String getMacxPoNumber() {
		return macxPoNumber;
	}

	public void setMacxPoNumber(String macxPoNumber) {
		this.macxPoNumber = macxPoNumber;
	}

	private String macxAddress1;
	private String macxAddress2;
	private String macxCity;
	private String macxState;
	private String macxZipCode;
	private String macxCriticality;
	private String macxScheduleDate;
	private String macxScheduleTime;
	private String macSpecialInstructions;
	private String macxPocName1;
	private String macxPocPhone1;
	private String macxPocEmail1;
	private String macxPocName2;
	private String macxPocPhone2;
	private String macxPocEmail2;
	private String macxActivityList;
	private String macxActivityQty;
	private String macxInstallNotes;
	private String macxNonPps;
	private String macxPoNumber;

	public Integer getMacxProjectId() {
		return macxProjectId;
	}

	public void setMacxProjectId(Integer macxProjectId) {
		this.macxProjectId = macxProjectId;
	}

	public String getMacxRequestor() {
		return macxRequestor;
	}

	public void setMacxRequestor(String macxRequestor) {
		this.macxRequestor = macxRequestor;
	}

	public String getMacxSiteNumber() {
		return macxSiteNumber;
	}

	public void setMacxSiteNumber(String macxSiteNumber) {
		this.macxSiteNumber = macxSiteNumber;
	}

	public String getMacxSiteLocation() {
		return macxSiteLocation;
	}

	public void setMacxSiteLocation(String macxSiteLocation) {
		this.macxSiteLocation = macxSiteLocation;
	}

	public String getMacxAddress1() {
		return macxAddress1;
	}

	public void setMacxAddress1(String macxAddress1) {
		this.macxAddress1 = macxAddress1;
	}

	public String getMacxAddress2() {
		return macxAddress2;
	}

	public void setMacxAddress2(String macxAddress2) {
		this.macxAddress2 = macxAddress2;
	}

	public String getMacxCity() {
		return macxCity;
	}

	public void setMacxCity(String macxCity) {
		this.macxCity = macxCity;
	}

	public String getMacxState() {
		return macxState;
	}

	public void setMacxState(String macxState) {
		this.macxState = macxState;
	}

	public String getMacxZipCode() {
		return macxZipCode;
	}

	public void setMacxZipCode(String macxZipCode) {
		this.macxZipCode = macxZipCode;
	}

	public String getMacxCriticality() {
		return macxCriticality;
	}

	public void setMacxCriticality(String macxCriticality) {
		this.macxCriticality = macxCriticality;
	}

	public String getMacxScheduleDate() {
		return macxScheduleDate;
	}

	public void setMacxScheduleDate(String macxScheduleDate) {
		this.macxScheduleDate = macxScheduleDate;
	}

	public String getMacxScheduleTime() {
		return macxScheduleTime;
	}

	public void setMacxScheduleTime(String macxScheduleTime) {
		this.macxScheduleTime = macxScheduleTime;
	}

	public String getMacxPocName1() {
		return macxPocName1;
	}

	public void setMacxPocName1(String macxPocName1) {
		this.macxPocName1 = macxPocName1;
	}

	public String getMacxPocPhone1() {
		return macxPocPhone1;
	}

	public void setMacxPocPhone1(String macxPocPhone1) {
		this.macxPocPhone1 = macxPocPhone1;
	}

	public String getMacxPocEmail1() {
		return macxPocEmail1;
	}

	public void setMacxPocEmail1(String macxPocEmail1) {
		this.macxPocEmail1 = macxPocEmail1;
	}

	public String getMacxPocName2() {
		return macxPocName2;
	}

	public void setMacxPocName2(String macxPocName2) {
		this.macxPocName2 = macxPocName2;
	}

	public String getMacxPocPhone2() {
		return macxPocPhone2;
	}

	public void setMacxPocPhone2(String macxPocPhone2) {
		this.macxPocPhone2 = macxPocPhone2;
	}

	public String getMacxPocEmail2() {
		return macxPocEmail2;
	}

	public void setMacxPocEmail2(String macxPocEmail2) {
		this.macxPocEmail2 = macxPocEmail2;
	}

	public String getMacxActivityList() {
		return macxActivityList;
	}

	public void setMacxActivityList(String macxActivityList) {
		this.macxActivityList = macxActivityList;
	}

	public String getMacxActivityQty() {
		return macxActivityQty;
	}

	public void setMacxActivityQty(String macxActivityQty) {
		this.macxActivityQty = macxActivityQty;
	}

	public String getMacxInstallNotes() {
		return macxInstallNotes;
	}

	public void setMacxInstallNotes(String macxInstallNotes) {
		this.macxInstallNotes = macxInstallNotes;
	}

	public String getMacSpecialInstructions() {
		return macSpecialInstructions;
	}

	public void setMacSpecialInstructions(String macSpecialInstructions) {
		this.macSpecialInstructions = macSpecialInstructions;
	}

	public String getMacxNonPps() {
		return macxNonPps;
	}

	public void setMacxNonPps(String macxNonPps) {
		this.macxNonPps = macxNonPps;
	}

}
