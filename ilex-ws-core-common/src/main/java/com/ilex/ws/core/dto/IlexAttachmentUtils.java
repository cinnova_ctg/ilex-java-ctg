package com.ilex.ws.core.dto;

import java.io.File;

public class IlexAttachmentUtils {
	private String fileName;
	private File file;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

}
