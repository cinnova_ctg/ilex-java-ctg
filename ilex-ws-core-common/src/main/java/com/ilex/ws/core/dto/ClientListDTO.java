package com.ilex.ws.core.dto;

import java.util.List;

public class ClientListDTO {
	private List<InvBTMClientDTO> invBTMClientDTOList;
	private Integer pageCount;
	private Integer pageIndex;
	private String clientSearchKeyword;

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setInvBTMClientDTOList(List<InvBTMClientDTO> invBTMClientDTOList) {
		this.invBTMClientDTOList = invBTMClientDTOList;
	}

	public List<InvBTMClientDTO> getInvBTMClientDTOList() {
		return invBTMClientDTOList;
	}

	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	public Integer getPageIndex() {
		return pageIndex;
	}

	public void setClientSearchKeyword(String clientSearchKeyword) {
		this.clientSearchKeyword = clientSearchKeyword;
	}

	public String getClientSearchKeyword() {
		return clientSearchKeyword;
	}

}
