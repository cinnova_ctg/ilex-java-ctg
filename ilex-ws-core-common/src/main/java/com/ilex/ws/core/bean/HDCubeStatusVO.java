package com.ilex.ws.core.bean;

import java.io.Serializable;

public class HDCubeStatusVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pendingAckCustCubeStatus;
	private String pendingAckAlertCubeStatus;
	private String pendingAckMonitorCubeStatus;
	private String wipCubeStatus;
	private String wipODCubeStatus;

	public String getPendingAckCustCubeStatus() {
		return pendingAckCustCubeStatus;
	}

	public void setPendingAckCustCubeStatus(String pendingAckCustCubeStatus) {
		this.pendingAckCustCubeStatus = pendingAckCustCubeStatus;
	}

	public String getPendingAckMonitorCubeStatus() {
		return pendingAckMonitorCubeStatus;
	}

	public void setPendingAckMonitorCubeStatus(
			String pendingAckMonitorCubeStatus) {
		this.pendingAckMonitorCubeStatus = pendingAckMonitorCubeStatus;
	}

	public String getWipCubeStatus() {
		return wipCubeStatus;
	}

	public void setWipCubeStatus(String wipCubeStatus) {
		this.wipCubeStatus = wipCubeStatus;
	}

	public String getWipODCubeStatus() {
		return wipODCubeStatus;
	}

	public void setWipODCubeStatus(String wipODCubeStatus) {
		this.wipODCubeStatus = wipODCubeStatus;
	}

	public String getPendingAckAlertCubeStatus() {
		return pendingAckAlertCubeStatus;
	}

	public void setPendingAckAlertCubeStatus(String pendingAckAlertCubeStatus) {
		this.pendingAckAlertCubeStatus = pendingAckAlertCubeStatus;
	}

}
