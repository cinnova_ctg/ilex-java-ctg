package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;
import org.apache.commons.lang.builder.ToStringBuilder;

public class MasterBTMDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MsaClientDTO client;
	private OrgDivisionDTO division;
	private PointOfContactDTO poc;
	private int deliveryMethod;
	private InvBTMClientDTO btmClientDTO;
	private InvBTMContactDTO btmContactDTO;
	private List<InvBTMContactDTO> invBTMContactList = LazyList.decorate(
			new ArrayList<InvBTMContactDTO>(),
			FactoryUtils.instantiateFactory(InvBTMContactDTO.class));;

	public List<InvBTMContactDTO> getInvBTMContactList() {
		return invBTMContactList;
	}

	public void setInvBTMContactList(List<InvBTMContactDTO> invBTMContactList) {
		this.invBTMContactList = invBTMContactList;
	}

	public MsaClientDTO getClient() {
		return client;
	}

	public void setClient(MsaClientDTO client) {
		this.client = client;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public void setDivision(OrgDivisionDTO division) {
		this.division = division;
	}

	public OrgDivisionDTO getDivision() {
		return division;
	}

	public void setPoc(PointOfContactDTO poc) {
		this.poc = poc;
	}

	public PointOfContactDTO getPoc() {
		return poc;
	}

	public void setDeliveryMethod(int deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public int getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setBtmClientDTO(InvBTMClientDTO btmClientDTO) {
		this.btmClientDTO = btmClientDTO;
	}

	public InvBTMClientDTO getBtmClientDTO() {
		return btmClientDTO;
	}

	public void setBtmContactDTO(InvBTMContactDTO btmContactDTO) {
		this.btmContactDTO = btmContactDTO;
	}

	public InvBTMContactDTO getBtmContactDTO() {
		return btmContactDTO;
	}

}
