package com.ilex.ws.core.bean;

import java.io.Serializable;

public class CountryVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String countryName;
	private String countryId;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
}
