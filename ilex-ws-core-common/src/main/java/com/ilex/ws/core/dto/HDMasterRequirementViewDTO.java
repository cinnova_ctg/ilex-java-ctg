package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.List;

public class HDMasterRequirementViewDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	List<HDMasterRequirementDTO> hdMasterRequirements;
	private Integer pageCount;
	private Integer pageIndex;

	public List<HDMasterRequirementDTO> getHdMasterRequirements() {
		return hdMasterRequirements;
	}

	public void setHdMasterRequirements(
			List<HDMasterRequirementDTO> hdMasterRequirements) {
		this.hdMasterRequirements = hdMasterRequirements;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public Integer getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

}
