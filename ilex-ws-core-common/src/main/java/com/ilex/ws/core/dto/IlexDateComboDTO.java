package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.List;

public class IlexDateComboDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<LabelValue> hours;
	private List<LabelValue> minutes;
	private List<LabelValue> options;

	public List<LabelValue> getHours() {
		return hours;
	}

	public void setHours(List<LabelValue> hours) {
		this.hours = hours;
	}

	public List<LabelValue> getMinutes() {
		return minutes;
	}

	public void setMinutes(List<LabelValue> minutes) {
		this.minutes = minutes;
	}

	public List<LabelValue> getOptions() {
		return options;
	}

	public void setOptions(List<LabelValue> options) {
		this.options = options;
	}

}
