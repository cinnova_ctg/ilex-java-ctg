package com.ilex.ws.core.dto;

import java.io.Serializable;

public class HDTableHeaderDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int tableWidth;
	private int openedDateWidth;
	private int siteNumberWidth;
	private int priorityWidth;
	private int statusWidth;
	private int nextActionWidth;
	private int nextActionDueDateWidth;
	private int ccaWidth;
	private int shortDescWidth;
	private int customerWidth;
	private int ilexNumberWidth;
	private int customerNumberWidth;
	private int stateWidth;
	private int sourceWidth;
	private int esclateLinkWidth;

	public int getStateWidth() {
		return stateWidth;
	}

	public void setStateWidth(int stateWidth) {
		this.stateWidth = stateWidth;
	}

	public int getSourceWidth() {
		return sourceWidth;
	}

	public void setSourceWidth(int sourceWidth) {
		this.sourceWidth = sourceWidth;
	}

	public int getOpenedDateWidth() {
		return openedDateWidth;
	}

	public void setOpenedDateWidth(int openedDateWidth) {
		this.openedDateWidth = openedDateWidth;
	}

	public int getSiteNumberWidth() {
		return siteNumberWidth;
	}

	public void setSiteNumberWidth(int siteNumberWidth) {
		this.siteNumberWidth = siteNumberWidth;
	}

	public int getPriorityWidth() {
		return priorityWidth;
	}

	public void setPriorityWidth(int priorityWidth) {
		this.priorityWidth = priorityWidth;
	}

	public int getNextActionWidth() {
		return nextActionWidth;
	}

	public void setNextActionWidth(int nextActionWidth) {
		this.nextActionWidth = nextActionWidth;
	}

	public int getNextActionDueDateWidth() {
		return nextActionDueDateWidth;
	}

	public void setNextActionDueDateWidth(int nextActionDueDateWidth) {
		this.nextActionDueDateWidth = nextActionDueDateWidth;
	}

	public int getCcaWidth() {
		return ccaWidth;
	}

	public void setCcaWidth(int ccaWidth) {
		this.ccaWidth = ccaWidth;
	}

	public int getShortDescWidth() {
		return shortDescWidth;
	}

	public void setShortDescWidth(int shortDescWidth) {
		this.shortDescWidth = shortDescWidth;
	}

	public int getIlexNumberWidth() {
		return ilexNumberWidth;
	}

	public void setIlexNumberWidth(int ilexNumberWidth) {
		this.ilexNumberWidth = ilexNumberWidth;
	}

	public int getCustomerNumberWidth() {
		return customerNumberWidth;
	}

	public void setCustomerNumberWidth(int customerNumberWidth) {
		this.customerNumberWidth = customerNumberWidth;
	}

	public int getTableWidth() {
		return tableWidth;
	}

	public void setTableWidth(int tableWidth) {
		this.tableWidth = tableWidth;
	}

	public int getCustomerWidth() {
		return customerWidth;
	}

	public void setCustomerWidth(int customerWidth) {
		this.customerWidth = customerWidth;
	}

	public int getStatusWidth() {
		return statusWidth;
	}

	public void setStatusWidth(int statusWidth) {
		this.statusWidth = statusWidth;
	}

	public int getEsclateLinkWidth() {
		return esclateLinkWidth;
	}

	public void setEsclateLinkWidth(int esclateLinkWidth) {
		this.esclateLinkWidth = esclateLinkWidth;
	}

}
