package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class IlexEmailTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<String> mailTo = new ArrayList<String>();
	private List<String> mailCc = new ArrayList<String>();
	private List<String> mailBcc = new ArrayList<String>();
	private List<IlexAttachmentUtils> mailAttachments = new ArrayList<IlexAttachmentUtils>();

	private String mailFrom;
	private String mailText;
	private String mailSubject;
	private String messageId;

	public List<IlexAttachmentUtils> getMailAttachments() {
		return mailAttachments;
	}

	public List<String> getMailTo() {
		return mailTo;
	}

	public void setMailTo(List<String> mailTo) {
		this.mailTo = mailTo;
	}

	public List<String> getMailCc() {
		return mailCc;
	}

	public void setMailCc(List<String> mailCc) {
		this.mailCc = mailCc;
	}

	public List<String> getMailBcc() {
		return mailBcc;
	}

	public String getMailFrom() {
		return mailFrom;
	}

	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}

	public String getMailText() {
		return mailText;
	}

	public void setMailText(String mailText) {
		this.mailText = mailText;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public void addAttachment(IlexAttachmentUtils attachment) {
		mailAttachments.add(attachment);
	}

	public void addMailTo(String to) {
		mailTo.add(to);
	}

	public void addMainCc(String cc) {
		mailCc.add(cc);
	}

	public void addMailBCc(String bcc) {
		mailBcc.add(bcc);
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

}
