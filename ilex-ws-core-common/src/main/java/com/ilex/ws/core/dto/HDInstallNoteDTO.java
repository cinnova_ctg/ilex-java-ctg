package com.ilex.ws.core.dto;

import java.io.Serializable;

public class HDInstallNoteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String installNote;
	private String installNoteDateStr;
	private String userLastName;

	public String getInstallNote() {
		return installNote;
	}

	public void setInstallNote(String installNote) {
		this.installNote = installNote;
	}

	public String getInstallNoteDateStr() {
		return installNoteDateStr;
	}

	public void setInstallNoteDateStr(String installNoteDateStr) {
		this.installNoteDateStr = installNoteDateStr;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

}
