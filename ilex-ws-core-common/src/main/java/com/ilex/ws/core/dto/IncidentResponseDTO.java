package com.ilex.ws.core.dto;

import java.io.Serializable;

/**
 * The Class IncidentResponseDTO. Used for getting Ilex To ServiceNow Response
 * or vice versa.
 */
public class IncidentResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String sys_id;
	private String table;
	private String display_name;
	private String display_value;
	private String status;
	private String ticketNumber;
	private String vTaskNumber;
	private String error_description;
	private String error_number;
	private String error_message;

	public String getSys_id() {
		return sys_id;
	}

	public void setSys_id(String sys_id) {
		this.sys_id = sys_id;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getDisplay_name() {
		return display_name;
	}

	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}

	public String getDisplay_value() {
		return display_value;
	}

	public void setDisplay_value(String display_value) {
		this.display_value = display_value;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getvTaskNumber() {
		return vTaskNumber;
	}

	public void setvTaskNumber(String vTaskNumber) {
		this.vTaskNumber = vTaskNumber;
	}

	public String getError_description() {
		return error_description;
	}

	public void setError_description(String error_description) {
		this.error_description = error_description;
	}

	public String getError_number() {
		return error_number;
	}

	public void setError_number(String error_number) {
		this.error_number = error_number;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}

}
