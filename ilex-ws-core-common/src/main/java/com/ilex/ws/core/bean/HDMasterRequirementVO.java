package com.ilex.ws.core.bean;

import java.io.Serializable;
import java.util.List;

public class HDMasterRequirementVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long requirementId;
	private String requirement;
	private boolean status;
	private String description;
	private List<String> deviceType;

	public Long getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(Long requirementId) {
		this.requirementId = requirementId;
	}

	public String getRequirement() {
		return requirement;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(List<String> deviceType) {
		this.deviceType = deviceType;
	}

}
