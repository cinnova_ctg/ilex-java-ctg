package com.ilex.ws.core.dto;

import java.io.Serializable;

public class SiteSearchDetailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String msaId;
	private String siteName;
	private String siteNumber;
	private String siteAddress;
	private String siteCity;
	private String siteState;
	private String siteStateDesc;
	private String siteCountry;
	private String siteListId;
	private String siteLocalityFactor;
	private String unionSite;
	private String siteDesignator;
	private String siteWorkLocation;
	private String siteZipCode;
	private String siteSecPhone;
	private String sitePhone;
	private String siteDirection;
	private String sitePriPoc;
	private String siteSecPoc;
	private String sitePriEmail;
	private String siteSecEmail;
	private String siteNotes;
	private String installerPoc;
	private String sitePoc;
	private String siteLatDeg;
	private String siteLatMin;
	private String siteLatDirection;
	private String siteLonDeg;
	private String siteLonMin;
	private String siteLonDirection;
	private String siteBrand;
	private String siteStatus;
	private String siteCategory;
	private String siteRegion;
	private String siteEndCustomer;
	private String siteTimeZone;
	private String siteId;

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getSiteStateDesc() {
		return siteStateDesc;
	}

	public void setSiteStateDesc(String siteStateDesc) {
		this.siteStateDesc = siteStateDesc;
	}

	public String getSiteCountry() {
		return siteCountry;
	}

	public void setSiteCountry(String siteCountry) {
		this.siteCountry = siteCountry;
	}

	public String getSiteListId() {
		return siteListId;
	}

	public void setSiteListId(String siteListId) {
		this.siteListId = siteListId;
	}

	public String getSiteLocalityFactor() {
		return siteLocalityFactor;
	}

	public void setSiteLocalityFactor(String siteLocalityFactor) {
		this.siteLocalityFactor = siteLocalityFactor;
	}

	public String getUnionSite() {
		return unionSite;
	}

	public void setUnionSite(String unionSite) {
		this.unionSite = unionSite;
	}

	public String getSiteDesignator() {
		return siteDesignator;
	}

	public void setSiteDesignator(String siteDesignator) {
		this.siteDesignator = siteDesignator;
	}

	public String getSiteWorkLocation() {
		return siteWorkLocation;
	}

	public void setSiteWorkLocation(String siteWorkLocation) {
		this.siteWorkLocation = siteWorkLocation;
	}

	public String getSiteZipCode() {
		return siteZipCode;
	}

	public void setSiteZipCode(String siteZipCode) {
		this.siteZipCode = siteZipCode;
	}

	public String getSiteSecPhone() {
		return siteSecPhone;
	}

	public void setSiteSecPhone(String siteSecPhone) {
		this.siteSecPhone = siteSecPhone;
	}

	public String getSitePhone() {
		return sitePhone;
	}

	public void setSitePhone(String sitePhone) {
		this.sitePhone = sitePhone;
	}

	public String getSiteDirection() {
		return siteDirection;
	}

	public void setSiteDirection(String siteDirection) {
		this.siteDirection = siteDirection;
	}

	public String getSitePriPoc() {
		return sitePriPoc;
	}

	public void setSitePriPoc(String sitePriPoc) {
		this.sitePriPoc = sitePriPoc;
	}

	public String getSiteSecPoc() {
		return siteSecPoc;
	}

	public void setSiteSecPoc(String siteSecPoc) {
		this.siteSecPoc = siteSecPoc;
	}

	public String getSitePriEmail() {
		return sitePriEmail;
	}

	public void setSitePriEmail(String sitePriEmail) {
		this.sitePriEmail = sitePriEmail;
	}

	public String getSiteSecEmail() {
		return siteSecEmail;
	}

	public void setSiteSecEmail(String siteSecEmail) {
		this.siteSecEmail = siteSecEmail;
	}

	public String getSiteNotes() {
		return siteNotes;
	}

	public void setSiteNotes(String siteNotes) {
		this.siteNotes = siteNotes;
	}

	public String getInstallerPoc() {
		return installerPoc;
	}

	public void setInstallerPoc(String installerPoc) {
		this.installerPoc = installerPoc;
	}

	public String getSitePoc() {
		return sitePoc;
	}

	public void setSitePoc(String sitePoc) {
		this.sitePoc = sitePoc;
	}

	public String getSiteLatDeg() {
		return siteLatDeg;
	}

	public void setSiteLatDeg(String siteLatDeg) {
		this.siteLatDeg = siteLatDeg;
	}

	public String getSiteLatMin() {
		return siteLatMin;
	}

	public void setSiteLatMin(String siteLatMin) {
		this.siteLatMin = siteLatMin;
	}

	public String getSiteLatDirection() {
		return siteLatDirection;
	}

	public void setSiteLatDirection(String siteLatDirection) {
		this.siteLatDirection = siteLatDirection;
	}

	public String getSiteLonDeg() {
		return siteLonDeg;
	}

	public void setSiteLonDeg(String siteLonDeg) {
		this.siteLonDeg = siteLonDeg;
	}

	public String getSiteLonMin() {
		return siteLonMin;
	}

	public void setSiteLonMin(String siteLonMin) {
		this.siteLonMin = siteLonMin;
	}

	public String getSiteLonDirection() {
		return siteLonDirection;
	}

	public void setSiteLonDirection(String siteLonDirection) {
		this.siteLonDirection = siteLonDirection;
	}

	public String getSiteBrand() {
		return siteBrand;
	}

	public void setSiteBrand(String siteBrand) {
		this.siteBrand = siteBrand;
	}

	public String getSiteStatus() {
		return siteStatus;
	}

	public void setSiteStatus(String siteStatus) {
		this.siteStatus = siteStatus;
	}

	public String getSiteCategory() {
		return siteCategory;
	}

	public void setSiteCategory(String siteCategory) {
		this.siteCategory = siteCategory;
	}

	public String getSiteRegion() {
		return siteRegion;
	}

	public void setSiteRegion(String siteRegion) {
		this.siteRegion = siteRegion;
	}

	public String getSiteEndCustomer() {
		return siteEndCustomer;
	}

	public void setSiteEndCustomer(String siteEndCustomer) {
		this.siteEndCustomer = siteEndCustomer;
	}

	public String getSiteTimeZone() {
		return siteTimeZone;
	}

	public void setSiteTimeZone(String siteTimeZone) {
		this.siteTimeZone = siteTimeZone;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

}
