package com.ilex.ws.core.dto;

import java.io.Serializable;

public class IlexTicketDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String serviceNowTicketId;
	private String category;
	private String comments;
	private String workNotes;
	private String shortDescription;

	public String getServiceNowTicketId() {
		return serviceNowTicketId;
	}

	public void setServiceNowTicketId(String serviceNowTicketId) {
		this.serviceNowTicketId = serviceNowTicketId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getWorkNotes() {
		return workNotes;
	}

	public void setWorkNotes(String workNotes) {
		this.workNotes = workNotes;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
