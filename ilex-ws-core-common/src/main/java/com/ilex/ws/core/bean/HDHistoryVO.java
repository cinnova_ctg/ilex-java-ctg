package com.ilex.ws.core.bean;

import java.io.Serializable;

public class HDHistoryVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long ticketId;
	private String event;
	private String initiator;
	private String toReference;
	private String toData;
	private String fromReference;
	private String fromData;

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getInitiator() {
		return initiator;
	}

	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}

	public String getToReference() {
		return toReference;
	}

	public void setToReference(String toReference) {
		this.toReference = toReference;
	}

	public String getToData() {
		return toData;
	}

	public void setToData(String toData) {
		this.toData = toData;
	}

	public String getFromReference() {
		return fromReference;
	}

	public void setFromReference(String fromReference) {
		this.fromReference = fromReference;
	}

	public String getFromData() {
		return fromData;
	}

	public void setFromData(String fromData) {
		this.fromData = fromData;
	}

}
