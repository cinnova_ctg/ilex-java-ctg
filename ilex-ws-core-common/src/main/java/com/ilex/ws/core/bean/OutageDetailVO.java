package com.ilex.ws.core.bean;

import java.io.Serializable;

public class OutageDetailVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String ticketId;
	String wugIpName;

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getWugIpName() {
		return wugIpName;
	}

	public void setWugIpName(String wugIpName) {
		this.wugIpName = wugIpName;
	}

}
