package com.ilex.ws.core.bean;

import java.io.Serializable;

public class TicketStateVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String stateId;
	private String ticketState;

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getTicketState() {
		return ticketState;
	}

	public void setTicketState(String ticketState) {
		this.ticketState = ticketState;
	}

}
