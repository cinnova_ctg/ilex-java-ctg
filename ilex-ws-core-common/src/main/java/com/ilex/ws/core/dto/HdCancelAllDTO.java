package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;

public class HdCancelAllDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pointOfFailureId;
	private String rootCauseId;
	private String resolutionId;
	private List<HDCustomerDetailDTO> customerList;
	private int duration;
	private String timeFrameStart;
	private String timeFrameEnd;
	List<OutageDetailDTO> outageList = LazyList.decorate(
			new ArrayList<OutageDetailDTO>(),
			FactoryUtils.instantiateFactory(OutageDetailDTO.class));;

	public List<OutageDetailDTO> getOutageList() {
		return outageList;
	}

	public void setOutageList(List<OutageDetailDTO> outageList) {
		this.outageList = outageList;
	}

	public String getTimeFrameStart() {
		return timeFrameStart;
	}

	public void setTimeFrameStart(String timeFrameStart) {
		this.timeFrameStart = timeFrameStart;
	}

	public String getTimeFrameEnd() {
		return timeFrameEnd;
	}

	public void setTimeFrameEnd(String timeFrameEnd) {
		this.timeFrameEnd = timeFrameEnd;
	}

	public String getPointOfFailureId() {
		return pointOfFailureId;
	}

	public void setPointOfFailureId(String pointOfFailureId) {
		this.pointOfFailureId = pointOfFailureId;
	}

	public String getRootCauseId() {
		return rootCauseId;
	}

	public void setRootCauseId(String rootCauseId) {
		this.rootCauseId = rootCauseId;
	}

	public String getResolutionId() {
		return resolutionId;
	}

	public void setResolutionId(String resolutionId) {
		this.resolutionId = resolutionId;
	}

	public List<HDCustomerDetailDTO> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<HDCustomerDetailDTO> customerList) {
		this.customerList = customerList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

}
