package com.ilex.ws.core.bean;

import java.io.Serializable;

public class InvSearchVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int invBillToAddressId;
	private String clientName;
	private String appendixName;
	private int appendixId;
	private int invSetupId;

	public int getInvBillToAddressId() {
		return invBillToAddressId;
	}

	public void setInvBillToAddressId(int invBillToAddressId) {
		this.invBillToAddressId = invBillToAddressId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public int getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(int appendixId) {
		this.appendixId = appendixId;
	}

	public void setInvSetupId(int invSetupId) {
		this.invSetupId = invSetupId;
	}

	public int getInvSetupId() {
		return invSetupId;
	}

}
