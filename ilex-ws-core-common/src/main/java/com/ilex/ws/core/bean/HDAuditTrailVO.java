package com.ilex.ws.core.bean;

import java.io.Serializable;
import java.util.Date;

public class HDAuditTrailVO implements Serializable {
	private String id;
	private String ticketId;
	private String changeDateString;
	private Date changeDate;
	private String incidentState;
	private String shortDesc;
	private String nextAction;
	private String backUpCctState;
	private String pointOfFailure;
	private String resolution;
	private String rootCause;
	private String updatedBy;
	private String userName;

	private String incidentStateDesc;
	private String nextActionDesc;
	private String backUpCctStateDesc;
	private String pointOfFailureDesc;
	private String resolutionDesc;
	private String rootCauseDesc;

	public String getIncidentStateDesc() {
		return incidentStateDesc;
	}

	public void setIncidentStateDesc(String incidentStateDesc) {
		this.incidentStateDesc = incidentStateDesc;
	}

	public String getNextActionDesc() {
		return nextActionDesc;
	}

	public void setNextActionDesc(String nextActionDesc) {
		this.nextActionDesc = nextActionDesc;
	}

	public String getBackUpCctStateDesc() {
		return backUpCctStateDesc;
	}

	public void setBackUpCctStateDesc(String backUpCctStateDesc) {
		this.backUpCctStateDesc = backUpCctStateDesc;
	}

	public String getPointOfFailureDesc() {
		return pointOfFailureDesc;
	}

	public void setPointOfFailureDesc(String pointOfFailureDesc) {
		this.pointOfFailureDesc = pointOfFailureDesc;
	}

	public String getResolutionDesc() {
		return resolutionDesc;
	}

	public void setResolutionDesc(String resolutionDesc) {
		this.resolutionDesc = resolutionDesc;
	}

	public String getRootCauseDesc() {
		return rootCauseDesc;
	}

	public void setRootCauseDesc(String rootCauseDesc) {
		this.rootCauseDesc = rootCauseDesc;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getChangeDateString() {
		return changeDateString;
	}

	public void setChangeDateString(String changeDateString) {
		this.changeDateString = changeDateString;
	}

	public Date getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	public String getIncidentState() {
		return incidentState;
	}

	public void setIncidentState(String incidentState) {
		this.incidentState = incidentState;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getNextAction() {
		return nextAction;
	}

	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}

	public String getBackUpCctState() {
		return backUpCctState;
	}

	public void setBackUpCctState(String backUpCctState) {
		this.backUpCctState = backUpCctState;
	}

	public String getPointOfFailure() {
		return pointOfFailure;
	}

	public void setPointOfFailure(String pointOfFailure) {
		this.pointOfFailure = pointOfFailure;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getRootCause() {
		return rootCause;
	}

	public void setRootCause(String rootCause) {
		this.rootCause = rootCause;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
