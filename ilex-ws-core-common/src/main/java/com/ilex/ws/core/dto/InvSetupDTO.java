package com.ilex.ws.core.dto;

import java.io.Serializable;

public class InvSetupDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int invoiceSetupId;
	private int appendixId;
	private int mmId;
	private String pageLayout;
	private String contingentAddress;
	private String invoiceSetupType;
	private String invoiceSetupGroupingType;
	private boolean outstandingBalanceExist;
	private String customerReferenceSource;
	private String customerReferenceName;
	private String customerReferenceValue;
	private String customerReferencePoc;
	private String lineItemFormat;
	private String summarySheet;
	private String approvalLevel;
	private float collectedTax;

	public Boolean isBillToReferenceExist() {
		return billToReferenceExist;
	}

	public void setBillToReferenceExist(Boolean billToReferenceExist) {
		this.billToReferenceExist = billToReferenceExist;
	}

	public Boolean isCustomerReferenceExist() {
		return customerReferenceExist;
	}

	public void setCustomerReferenceExist(Boolean customerReferenceExist) {
		this.customerReferenceExist = customerReferenceExist;
	}

	private Boolean billToReferenceExist;
	private Boolean customerReferenceExist;
	private String invoiceSetupReferenceDate;
	private String timeFrame;
	private Integer billToAddressId;

	public Integer getBillToAddressId() {
		return billToAddressId;
	}

	public void setBillToAddressId(Integer billToAddressId) {
		this.billToAddressId = billToAddressId;
	}

	public String getTimeFrame() {
		return timeFrame;
	}

	public void setTimeFrame(String timeFrame) {
		this.timeFrame = timeFrame;
	}

	public int getInvoiceSetupId() {
		return invoiceSetupId;
	}

	public void setInvoiceSetupId(int invoiceSetupId) {
		this.invoiceSetupId = invoiceSetupId;
	}

	public int getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(int appendixId) {
		this.appendixId = appendixId;
	}

	public int getMmId() {
		return mmId;
	}

	public void setMmId(int mmId) {
		this.mmId = mmId;
	}

	public String getPageLayout() {
		return pageLayout;
	}

	public void setPageLayout(String pageLayout) {
		this.pageLayout = pageLayout;
	}

	public String getContingentAddress() {
		return contingentAddress;
	}

	public void setContingentAddress(String contingentAddress) {
		this.contingentAddress = contingentAddress;
	}

	public String getInvoiceSetupType() {
		return invoiceSetupType;
	}

	public void setInvoiceSetupType(String invoiceSetupType) {
		this.invoiceSetupType = invoiceSetupType;
	}

	public String getInvoiceSetupGroupingType() {
		return invoiceSetupGroupingType;
	}

	public void setInvoiceSetupGroupingType(String invoiceSetupGroupingType) {
		this.invoiceSetupGroupingType = invoiceSetupGroupingType;
	}

	public boolean isOutstandingBalanceExist() {
		return outstandingBalanceExist;
	}

	public void setOutstandingBalanceExist(boolean outstandingBalanceExist) {
		this.outstandingBalanceExist = outstandingBalanceExist;
	}

	public String getCustomerReferenceSource() {
		return customerReferenceSource;
	}

	public void setCustomerReferenceSource(String customerReferenceSource) {
		this.customerReferenceSource = customerReferenceSource;
	}

	public String getCustomerReferenceName() {
		return customerReferenceName;
	}

	public void setCustomerReferenceName(String customerReferenceName) {
		this.customerReferenceName = customerReferenceName;
	}

	public String getCustomerReferenceValue() {
		return customerReferenceValue;
	}

	public void setCustomerReferenceValue(String customerReferenceValue) {
		this.customerReferenceValue = customerReferenceValue;
	}

	public String getCustomerReferencePoc() {
		return customerReferencePoc;
	}

	public void setCustomerReferencePoc(String customerReferencePoc) {
		this.customerReferencePoc = customerReferencePoc;
	}

	public String getLineItemFormat() {
		return lineItemFormat;
	}

	public void setLineItemFormat(String lineItemFormat) {
		this.lineItemFormat = lineItemFormat;
	}

	public String getSummarySheet() {
		return summarySheet;
	}

	public void setSummarySheet(String summarySheet) {
		this.summarySheet = summarySheet;
	}

	public String getApprovalLevel() {
		return approvalLevel;
	}

	public void setApprovalLevel(String approvalLevel) {
		this.approvalLevel = approvalLevel;
	}

	public float getCollectedTax() {
		return collectedTax;
	}

	public void setCollectedTax(float collectedTax) {
		this.collectedTax = collectedTax;
	}

	public String getInvoiceSetupReferenceDate() {
		return invoiceSetupReferenceDate;
	}

	public void setInvoiceSetupReferenceDate(String invoiceSetupReferenceDate) {
		this.invoiceSetupReferenceDate = invoiceSetupReferenceDate;
	}

}
