package com.ilex.ws.core.dto;

import java.io.Serializable;

public class ProvisioningDetailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String ipType;
	private String ipUsername;
	private String ipPassword;
	private String useableIp;
	private String gateway;
	private String subnetMask;
	private String primaryDNS;
	private String secondayDNS;
	private String circuit;

	public String getIpType() {
		return ipType;
	}

	public void setIpType(String ipType) {
		this.ipType = ipType;
	}

	public String getIpUsername() {
		return ipUsername;
	}

	public void setIpUsername(String ipUsername) {
		this.ipUsername = ipUsername;
	}

	public String getIpPassword() {
		return ipPassword;
	}

	public void setIpPassword(String ipPassword) {
		this.ipPassword = ipPassword;
	}

	public String getUseableIp() {
		return useableIp;
	}

	public void setUseableIp(String useableIp) {
		this.useableIp = useableIp;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getSubnetMask() {
		return subnetMask;
	}

	public void setSubnetMask(String subnetMask) {
		this.subnetMask = subnetMask;
	}

	public String getPrimaryDNS() {
		return primaryDNS;
	}

	public void setPrimaryDNS(String primaryDNS) {
		this.primaryDNS = primaryDNS;
	}

	public String getSecondayDNS() {
		return secondayDNS;
	}

	public void setSecondayDNS(String secondayDNS) {
		this.secondayDNS = secondayDNS;
	}

	public String getCircuit() {
		return circuit;
	}

	public void setCircuit(String circuit) {
		this.circuit = circuit;
	}

}
