package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.List;

public class HDCustomerRequirementDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String customerName;
	private Long projectId;
	private String projectName;
	private Integer requirementCount;
	List<HDProjectRequirementDTO> previousSelectedRequirements;
	List<HDProjectRequirementDTO> previousDeselectedRequirements;
	List<HDProjectRequirementDTO> masterRequirements;

	public List<HDProjectRequirementDTO> getPreviousSelectedRequirements() {
		return previousSelectedRequirements;
	}

	public void setPreviousSelectedRequirements(
			List<HDProjectRequirementDTO> previousSelectedRequirements) {
		this.previousSelectedRequirements = previousSelectedRequirements;
	}

	public List<HDProjectRequirementDTO> getPreviousDeselectedRequirements() {
		return previousDeselectedRequirements;
	}

	public void setPreviousDeselectedRequirements(
			List<HDProjectRequirementDTO> previousDeselectedRequirements) {
		this.previousDeselectedRequirements = previousDeselectedRequirements;
	}

	public List<HDProjectRequirementDTO> getMasterRequirements() {
		return masterRequirements;
	}

	public void setMasterRequirements(
			List<HDProjectRequirementDTO> masterRequirements) {
		this.masterRequirements = masterRequirements;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getRequirementCount() {
		return requirementCount;
	}

	public void setRequirementCount(Integer requirementCount) {
		this.requirementCount = requirementCount;
	}

}
