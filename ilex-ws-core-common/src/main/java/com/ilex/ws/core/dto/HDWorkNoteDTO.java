package com.ilex.ws.core.dto;

import java.io.Serializable;

public class HDWorkNoteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String workNote;
	private String workNoteDateStr;
	private String userLastName;

	public String getWorkNote() {
		return workNote;
	}

	public void setWorkNote(String workNote) {
		this.workNote = workNote;
	}

	public String getWorkNoteDateStr() {
		return workNoteDateStr;
	}

	public void setWorkNoteDateStr(String workNoteDateStr) {
		this.workNoteDateStr = workNoteDateStr;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

}
