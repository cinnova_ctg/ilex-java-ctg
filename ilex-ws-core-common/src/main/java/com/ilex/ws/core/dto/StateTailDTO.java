package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.List;

public class StateTailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long downMinutes;
	List<WugDetailsDTO> wugDetailsDTOList;

	public Long getDownMinutes() {
		return downMinutes;
	}

	public void setDownMinutes(Long downMinutes) {
		this.downMinutes = downMinutes;
	}

	public List<WugDetailsDTO> getWugDetailsDTOList() {
		return wugDetailsDTOList;
	}

	public void setWugDetailsDTOList(List<WugDetailsDTO> wugDetailsDTOList) {
		this.wugDetailsDTOList = wugDetailsDTOList;
	}

}
