package com.ilex.ws.core.dto;

import java.io.Serializable;

public class InvBTMContactDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer invContactId;
	private String firstName;
	private String lastName;
	private String phone;
	private String cell;
	private String fax;
	private String email;
	private int pocId;
	private int mbtId;
	private boolean status;

	public Integer getInvContactId() {
		return invContactId;
	}

	public void setInvContactId(Integer invContactId) {
		this.invContactId = invContactId;
	}

	public int getPocId() {
		return pocId;
	}

	public void setPocId(int pocId) {
		this.pocId = pocId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCell() {
		return cell;
	}

	public void setCell(String cell) {
		this.cell = cell;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getMbtId() {
		return mbtId;
	}

	public void setMbtId(int mbtId) {
		this.mbtId = mbtId;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
