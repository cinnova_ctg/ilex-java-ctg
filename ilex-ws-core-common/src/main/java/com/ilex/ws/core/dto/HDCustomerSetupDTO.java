package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.List;

public class HDCustomerSetupDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String customerName;
	private String appendixTitle;
	private Long customerId;
	private Long appendixId;
	private Long msaId;
	private String communicationType;
	private String activeMonitorApproach;
	private String customerType;
	private String downStateName;
	private String downStateCode;
	private String requestReference;
	private String status;
	private List<PrjGroupIntegrationDTO> prjGroupInterationDTOs;
	private List<CustMonitorKeyDTO> monitoringKeyDTOs;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAppendixTitle() {
		return appendixTitle;
	}

	public void setAppendixTitle(String appendixTitle) {
		this.appendixTitle = appendixTitle;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(Long appendixId) {
		this.appendixId = appendixId;
	}

	public Long getMsaId() {
		return msaId;
	}

	public void setMsaId(Long msaId) {
		this.msaId = msaId;
	}

	public String getActiveMonitorApproach() {
		return activeMonitorApproach;
	}

	public void setActiveMonitorApproach(String activeMonitorApproach) {
		this.activeMonitorApproach = activeMonitorApproach;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getDownStateName() {
		return downStateName;
	}

	public void setDownStateName(String downStateName) {
		this.downStateName = downStateName;
	}

	public String getDownStateCode() {
		return downStateCode;
	}

	public void setDownStateCode(String downStateCode) {
		this.downStateCode = downStateCode;
	}

	public String getRequestReference() {
		return requestReference;
	}

	public void setRequestReference(String requestReference) {
		this.requestReference = requestReference;
	}

	public List<PrjGroupIntegrationDTO> getPrjGroupInterationDTOs() {
		return prjGroupInterationDTOs;
	}

	public void setPrjGroupInterationDTOs(
			List<PrjGroupIntegrationDTO> prjGroupInterationDTOs) {
		this.prjGroupInterationDTOs = prjGroupInterationDTOs;
	}

	public List<CustMonitorKeyDTO> getMonitoringKeyDTOs() {
		return monitoringKeyDTOs;
	}

	public void setMonitoringKeyDTOs(List<CustMonitorKeyDTO> monitoringKeyDTOs) {
		this.monitoringKeyDTOs = monitoringKeyDTOs;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCommunicationType() {
		return communicationType;
	}

	public void setCommunicationType(String communicationType) {
		this.communicationType = communicationType;
	}

}
