package com.ilex.ws.core.util;

public enum BackUpStatus {

	DOWN("0"), UP("1"), NOT_APPLICABLE("99");
	private String backUpStatus;

	BackUpStatus(String backUpStatus) {
		this.backUpStatus = backUpStatus;
	}

	public String getBackUpStatus() {
		return this.backUpStatus;
	}
}