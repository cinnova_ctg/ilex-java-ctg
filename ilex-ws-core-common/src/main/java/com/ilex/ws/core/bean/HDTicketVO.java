package com.ilex.ws.core.bean;

import java.io.Serializable;
import java.util.Date;

public class HDTicketVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String customerName;
	private String appendixName;
	private Long msaId;
	private Long appendixId;
	private String ticketType;
	private String contactName;
	private String phoneNumber;
	private String emailAddress;
	private String siteId;
	private String customerTicketReference;
	private Date preferredArrivalTimeDate;
	private Date preferredArrivalWindowToDate;
	private Date preferredArrivalWindowFromDate;
	private String category;
	private String shortDescription;
	private String configurationItem;
	private String problemDescription;
	private String manuallyCreationReason;
	private String currentCircuitStatus;
	private String priority;
	private String impact;
	private String urgency;
	private String nextAction;
	private Date nextActionDueDate;
	private String efforts;
	private String workNotes;
	private String ticketSource;
	private int criticality;
	private int resourceLevelId;
	private int criticalityId;
	private Boolean billingStatus;
	private String ticketId;
	private String callerName;
	private String callerPhone;
	private String callerEmail;
	private String siteNumber;
	private String siteAddress;
	private String siteCity;
	private String siteState;
	private String siteCountry;
	private String siteZipcode;
	private String siteNumberTemp;
	private String siteAddressTemp;
	private String siteCityTemp;
	private String siteStateTemp;
	private String siteCountryTemp;
	private String siteZipcodeTemp;
	private String backUpCktStatus;
	private String ackCubeType;
	private String customerWorkNotes;
	private String state;
	private String ticketNumber;
	private String device;
	private String pointOfFailure;
	private String rootCause;
	private String resolution;
	private String ticketStatus;
	private Boolean lmCompleteFirstCall;
	private String pointOfFailureId;
	private String rootCauseId;
	private String resolutionId;
	private String siteWorkNotes;
	private String primaryState;
	private Long jobId;
	private Date extSystemUpdateDate;
	private String testNameTemp = "";
	private String isFromMain = "";
	private String assignmentGroupId;
	private String contactTypeId;
	private Date createdOnDate;
	private String createdOnDateString;
	private String providerRefNo;

	private String snErrorWhileClosingTicket;

	// private String client1stContact;
	// private String clientHDPhoneNumber;
	// private String clientHDEmailAddress;
	// private String clientHDContactName;

	/*
	 * Saves the last values of the given status to generate the email body
	 */

	// public String getClient1stContact() {
	// return client1stContact;
	// }
	//
	// public void setClient1stContact(String client1stContact) {
	// this.client1stContact = client1stContact;
	// }
	//
	// public String getClientHDPhoneNumber() {
	// return clientHDPhoneNumber;
	// }
	//
	// public void setClientHDPhoneNumber(String clientHDPhoneNumber) {
	// this.clientHDPhoneNumber = clientHDPhoneNumber;
	// }
	//
	// public String getClientHDEmailAddress() {
	// return clientHDEmailAddress;
	// }
	//
	// public void setClientHDEmailAddress(String clientHDEmailAddress) {
	// this.clientHDEmailAddress = clientHDEmailAddress;
	// }
	//
	// public String getClientHDContactName() {
	// return clientHDContactName;
	// }
	//
	// public void setClientHDContactName(String clientHDContactName) {
	// this.clientHDContactName = clientHDContactName;
	// }

	private String lastCreateTicketNextActionDate;
	private String lastTicketStatus;
	private String lastNextAction;
	private String lastNextActionDueDate;

	/*
	 * Saves the last values of the given status to generate the email body
	 */

	public String getLastCreateTicketNextActionDate() {
		return lastCreateTicketNextActionDate;
	}

	public void setLastCreateTicketNextActionDate(
			String lastCreateTicketNextActionDate) {
		this.lastCreateTicketNextActionDate = lastCreateTicketNextActionDate;
	}

	public String getLastTicketStatus() {
		return lastTicketStatus;
	}

	public void setLastTicketStatus(String lastTicketStatus) {
		this.lastTicketStatus = lastTicketStatus;
	}

	public String getLastNextAction() {
		return lastNextAction;
	}

	public void setLastNextAction(String lastNextAction) {
		this.lastNextAction = lastNextAction;
	}

	public String getAssignmentGroupId() {
		return assignmentGroupId;
	}

	public void setAssignmentGroupId(String assignmentGroupId) {
		this.assignmentGroupId = assignmentGroupId;
	}

	public String getContactTypeId() {
		return contactTypeId;
	}

	public void setContactTypeId(String contactTypeId) {
		this.contactTypeId = contactTypeId;
	}

	public Date getCreatedOnDate() {
		return createdOnDate;
	}

	public void setCreatedOnDate(Date createdOnDate) {
		this.createdOnDate = createdOnDate;
	}

	public String getPointOfFailureId() {
		return pointOfFailureId;
	}

	public void setPointOfFailureId(String pointOfFailureId) {
		this.pointOfFailureId = pointOfFailureId;
	}

	public String getRootCauseId() {
		return rootCauseId;
	}

	public void setRootCauseId(String rootCauseId) {
		this.rootCauseId = rootCauseId;
	}

	public String getResolutionId() {
		return resolutionId;
	}

	public void setResolutionId(String resolutionId) {
		this.resolutionId = resolutionId;
	}

	public int getCriticality() {
		return criticality;
	}

	public void setCriticality(int criticality) {
		this.criticality = criticality;
	}

	public int getResourceLevelId() {
		return resourceLevelId;
	}

	public void setResourceLevelId(int resourceLevelId) {
		this.resourceLevelId = resourceLevelId;
	}

	public int getCriticalityId() {
		return criticalityId;
	}

	public void setCriticalityId(int criticalityId) {
		this.criticalityId = criticalityId;
	}

	public Date getPreferredArrivalTimeDate() {
		return preferredArrivalTimeDate;
	}

	public void setPreferredArrivalTimeDate(Date preferredArrivalTimeDate) {
		this.preferredArrivalTimeDate = preferredArrivalTimeDate;
	}

	public Date getPreferredArrivalWindowToDate() {
		return preferredArrivalWindowToDate;
	}

	public void setPreferredArrivalWindowToDate(
			Date preferredArrivalWindowToDate) {
		this.preferredArrivalWindowToDate = preferredArrivalWindowToDate;
	}

	public Date getPreferredArrivalWindowFromDate() {
		return preferredArrivalWindowFromDate;
	}

	public void setPreferredArrivalWindowFromDate(
			Date preferredArrivalWindowFromDate) {
		this.preferredArrivalWindowFromDate = preferredArrivalWindowFromDate;
	}

	public Date getNextActionDueDate() {
		return nextActionDueDate;
	}

	public void setNextActionDueDate(Date nextActionDueDate) {
		this.nextActionDueDate = nextActionDueDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public Long getMsaId() {
		return msaId;
	}

	public void setMsaId(Long msaId) {
		this.msaId = msaId;
	}

	public Long getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(Long appendixId) {
		this.appendixId = appendixId;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getCustomerTicketReference() {
		return customerTicketReference;
	}

	public void setCustomerTicketReference(String customerTicketReference) {
		this.customerTicketReference = customerTicketReference;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getConfigurationItem() {
		return configurationItem;
	}

	public void setConfigurationItem(String configurationItem) {
		this.configurationItem = configurationItem;
	}

	public String getProblemDescription() {
		return problemDescription;
	}

	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}

	public String getManuallyCreationReason() {
		return manuallyCreationReason;
	}

	public void setManuallyCreationReason(String manuallyCreationReason) {
		this.manuallyCreationReason = manuallyCreationReason;
	}

	public String getCurrentCircuitStatus() {
		return currentCircuitStatus;
	}

	public void setCurrentCircuitStatus(String currentCircuitStatus) {
		this.currentCircuitStatus = currentCircuitStatus;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public String getNextAction() {
		return nextAction;
	}

	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}

	public String getEfforts() {
		return efforts;
	}

	public void setEfforts(String efforts) {
		this.efforts = efforts;
	}

	public String getWorkNotes() {
		return workNotes;
	}

	public void setWorkNotes(String workNotes) {
		this.workNotes = workNotes;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getTicketSource() {
		return ticketSource;
	}

	public void setTicketSource(String ticketSource) {
		this.ticketSource = ticketSource;
	}

	public Boolean getBillingStatus() {
		return billingStatus;
	}

	public void setBillingStatus(Boolean billingStatus) {
		this.billingStatus = billingStatus;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getCallerName() {
		return callerName;
	}

	public void setCallerName(String callerName) {
		this.callerName = callerName;
	}

	public String getCallerPhone() {
		return callerPhone;
	}

	public void setCallerPhone(String callerPhone) {
		this.callerPhone = callerPhone;
	}

	public String getCallerEmail() {
		return callerEmail;
	}

	public void setCallerEmail(String callerEmail) {
		this.callerEmail = callerEmail;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getSiteCountry() {
		return siteCountry;
	}

	public void setSiteCountry(String siteCountry) {
		this.siteCountry = siteCountry;
	}

	public String getSiteZipcode() {
		return siteZipcode;
	}

	public void setSiteZipcode(String siteZipcode) {
		this.siteZipcode = siteZipcode;
	}

	public String getSiteNumberTemp() {
		return siteNumberTemp;
	}

	public void setSiteNumberTemp(String siteNumberTemp) {
		this.siteNumberTemp = siteNumberTemp;
	}

	public String getSiteAddressTemp() {
		return siteAddressTemp;
	}

	public void setSiteAddressTemp(String siteAddressTemp) {
		this.siteAddressTemp = siteAddressTemp;
	}

	public String getSiteCityTemp() {
		return siteCityTemp;
	}

	public void setSiteCityTemp(String siteCityTemp) {
		this.siteCityTemp = siteCityTemp;
	}

	public String getSiteStateTemp() {
		return siteStateTemp;
	}

	public void setSiteStateTemp(String siteStateTemp) {
		this.siteStateTemp = siteStateTemp;
	}

	public String getSiteCountryTemp() {
		return siteCountryTemp;
	}

	public void setSiteCountryTemp(String siteCountryTemp) {
		this.siteCountryTemp = siteCountryTemp;
	}

	public String getSiteZipcodeTemp() {
		return siteZipcodeTemp;
	}

	public void setSiteZipcodeTemp(String siteZipcodeTemp) {
		this.siteZipcodeTemp = siteZipcodeTemp;
	}

	public String getBackUpCktStatus() {
		return backUpCktStatus;
	}

	public void setBackUpCktStatus(String backUpCktStatus) {
		this.backUpCktStatus = backUpCktStatus;
	}

	public String getAckCubeType() {
		return ackCubeType;
	}

	public void setAckCubeType(String ackCubeType) {
		this.ackCubeType = ackCubeType;
	}

	public String getCustomerWorkNotes() {
		return customerWorkNotes;
	}

	public void setCustomerWorkNotes(String customerWorkNotes) {
		this.customerWorkNotes = customerWorkNotes;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getPointOfFailure() {
		return pointOfFailure;
	}

	public void setPointOfFailure(String pointOfFailure) {
		this.pointOfFailure = pointOfFailure;
	}

	public String getRootCause() {
		return rootCause;
	}

	public void setRootCause(String rootCause) {
		this.rootCause = rootCause;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public Boolean getLmCompleteFirstCall() {
		return lmCompleteFirstCall;
	}

	public void setLmCompleteFirstCall(Boolean lmCompleteFirstCall) {
		this.lmCompleteFirstCall = lmCompleteFirstCall;
	}

	public String getSiteWorkNotes() {
		return siteWorkNotes;
	}

	public void setSiteWorkNotes(String siteWorkNotes) {
		this.siteWorkNotes = siteWorkNotes;
	}

	public String getPrimaryState() {
		return primaryState;
	}

	public void setPrimaryState(String primaryState) {
		this.primaryState = primaryState;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public Date getExtSystemUpdateDate() {
		return extSystemUpdateDate;
	}

	public void setExtSystemUpdateDate(Date extSystemUpdateDate) {
		this.extSystemUpdateDate = extSystemUpdateDate;
	}

	public String getTestNameTemp() {
		return testNameTemp;
	}

	public void setTestNameTemp(String testNameTemp) {
		this.testNameTemp = testNameTemp;
	}

	public String getIsFromMain() {
		return isFromMain;
	}

	public void setIsFromMain(String isFromMain) {
		this.isFromMain = isFromMain;
	}

	public String getCreatedOnDateString() {
		return createdOnDateString;
	}

	public void setCreatedOnDateString(String createdOnDateString) {
		this.createdOnDateString = createdOnDateString;
	}

	public String getLastNextActionDueDate() {
		return lastNextActionDueDate;
	}

	public void setLastNextActionDueDate(String lastNextActionDueDate) {
		this.lastNextActionDueDate = lastNextActionDueDate;
	}

	public String getProviderRefNo() {
		return providerRefNo;
	}

	public void setProviderRefNo(String providerRefNo) {
		this.providerRefNo = providerRefNo;
	}

	public String getSnErrorWhileClosingTicket() {
		return snErrorWhileClosingTicket;
	}

	public void setSnErrorWhileClosingTicket(String snErrorWhileClosingTicket) {
		this.snErrorWhileClosingTicket = snErrorWhileClosingTicket;
	}

}
