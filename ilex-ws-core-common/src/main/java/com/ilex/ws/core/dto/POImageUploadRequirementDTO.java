package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.List;

public class POImageUploadRequirementDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String status;
	private String lm_po_Id;
	private String authentication;
	private String description;
	private List<POImageDetailDTO> images;
	private String city;
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	private String customer;
	private String createDate;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLm_po_Id() {
		return lm_po_Id;
	}

	public void setLm_po_Id(String lm_po_Id) {
		this.lm_po_Id = lm_po_Id;
	}

	public String getAuthentication() {
		return authentication;
	}

	public void setAuthentication(String authentication) {
		this.authentication = authentication;
	}

	public List<POImageDetailDTO> getImages() {
		return images;
	}

	public void setImages(List<POImageDetailDTO> images) {
		this.images = images;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
