package com.ilex.ws.core.dto;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class NetMedXIntegrationResponseDTO.
 */
public class NetMedXIntegrationResponseDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The ticket number. */
	private String ticketNumber;

	/** The attachment status. */
	private int attachmentStatus;

	/** The po id. */
	private int poId;

	/** The job id. */
	private int jobId;

	/** The api access. */
	private int apiAccess;

	/**
	 * Gets the ticket number.
	 * 
	 * @return the ticket number
	 */
	public String getTicketNumber() {
		return ticketNumber;
	}

	/**
	 * Sets the ticket number.
	 * 
	 * @param ticketNumber
	 *            the new ticket number
	 */
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	/**
	 * Gets the job id.
	 * 
	 * @return the job id
	 */
	public int getJobId() {
		return jobId;
	}

	/**
	 * Sets the job id.
	 * 
	 * @param jobId
	 *            the new job id
	 */
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	/**
	 * Gets the api access.
	 * 
	 * @return the api access
	 */
	public int getApiAccess() {
		return apiAccess;
	}

	/**
	 * Sets the api access.
	 * 
	 * @param apiAccess
	 *            the new api access
	 */
	public void setApiAccess(int apiAccess) {
		this.apiAccess = apiAccess;
	}

	/**
	 * Gets the attachment status.
	 * 
	 * @return the attachment status
	 */
	public int getAttachmentStatus() {
		return attachmentStatus;
	}

	/**
	 * Sets the attachment status.
	 * 
	 * @param attachmentStatus
	 *            the new attachment status
	 */
	public void setAttachmentStatus(int attachmentStatus) {
		this.attachmentStatus = attachmentStatus;
	}

	public int getPoId() {
		return poId;
	}

	public void setPoId(int poId) {
		this.poId = poId;
	}

}
