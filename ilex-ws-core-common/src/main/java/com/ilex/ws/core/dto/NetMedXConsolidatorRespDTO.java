package com.ilex.ws.core.dto;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class NetMedXConsolidatorRespDTO.
 */
public class NetMedXConsolidatorRespDTO implements Serializable {

	/** The job id. */
	private String jobId;

	/** The project id. */
	private String projectId;// appendix id

	/** The job title. */
	private String jobTitle;

	/** The include out of scope. */
	private String includeOutOfScope;

	/** The status. */
	private String status;

	/** The requested date. */
	private String requestedDate;

	/** The scheduled date. */
	private String scheduledDate;

	/** The onsite date. */
	private String onsiteDate;

	/** The offsite date. */
	private String offsiteDate;

	/** The invoice date. */
	private String invoiceDate;

	/** The completion date. */
	private String completionDate;

	/** The closed date. */
	private String closedDate;

	/** The invoice price. */
	private String invoicePrice;

	/** The invoice number. */
	private String invoiceNumber;

	/** The customer ref. */
	private String customerRef;

	/** The site number. */
	private String siteNumber;

	/** The site address. */
	private String siteAddress;

	/** The site city. */
	private String siteCity;

	/** The site country. */
	private String siteCountry;

	/** The site zip. */
	private String siteZip;

	/** The latitude radiud. */
	private String latitudeRadiud;

	/** The longitude radius. */
	private String longitudeRadius;

	/** The created by. */
	private String createdBy;

	/** The updated by. */
	private String updatedBy;

	/** The job updated by. */
	private String jobUpdatedBy;

	/** The site state. */
	private String siteState;

	/** The work location. */
	private String workLocation;

	/** The msp. */
	private String msp;

	/** The help desk. */
	private String helpDesk;

	/** The issue owner. */
	private String issueOwner;

	/** The root cause. */
	private String rootCause;

	/** The corrective aciton. */
	private String correctiveAciton;

	/** The pre poc. */
	private String prePOC;

	/** The phone number. */
	private String phoneNumber;

	/**
	 * Gets the job id.
	 * 
	 * @return the job id
	 */
	public String getJobId() {
		return jobId;
	}

	/**
	 * Sets the job id.
	 * 
	 * @param jobId
	 *            the new job id
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	/**
	 * Gets the project id.
	 * 
	 * @return the project id
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * Sets the project id.
	 * 
	 * @param projectId
	 *            the new project id
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Gets the job title.
	 * 
	 * @return the job title
	 */
	public String getJobTitle() {
		return jobTitle;
	}

	/**
	 * Sets the job title.
	 * 
	 * @param jobTitle
	 *            the new job title
	 */
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	/**
	 * Gets the include out of scope.
	 * 
	 * @return the include out of scope
	 */
	public String getIncludeOutOfScope() {
		return includeOutOfScope;
	}

	/**
	 * Sets the include out of scope.
	 * 
	 * @param includeOutOfScope
	 *            the new include out of scope
	 */
	public void setIncludeOutOfScope(String includeOutOfScope) {
		this.includeOutOfScope = includeOutOfScope;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the requested date.
	 * 
	 * @return the requested date
	 */
	public String getRequestedDate() {
		return requestedDate;
	}

	/**
	 * Sets the requested date.
	 * 
	 * @param requestedDate
	 *            the new requested date
	 */
	public void setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
	}

	/**
	 * Gets the scheduled date.
	 * 
	 * @return the scheduled date
	 */
	public String getScheduledDate() {
		return scheduledDate;
	}

	/**
	 * Sets the scheduled date.
	 * 
	 * @param scheduledDate
	 *            the new scheduled date
	 */
	public void setScheduledDate(String scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	/**
	 * Gets the onsite date.
	 * 
	 * @return the onsite date
	 */
	public String getOnsiteDate() {
		return onsiteDate;
	}

	/**
	 * Sets the onsite date.
	 * 
	 * @param onsiteDate
	 *            the new onsite date
	 */
	public void setOnsiteDate(String onsiteDate) {
		this.onsiteDate = onsiteDate;
	}

	/**
	 * Gets the offsite date.
	 * 
	 * @return the offsite date
	 */
	public String getOffsiteDate() {
		return offsiteDate;
	}

	/**
	 * Sets the offsite date.
	 * 
	 * @param offsiteDate
	 *            the new offsite date
	 */
	public void setOffsiteDate(String offsiteDate) {
		this.offsiteDate = offsiteDate;
	}

	/**
	 * Gets the invoice date.
	 * 
	 * @return the invoice date
	 */
	public String getInvoiceDate() {
		return invoiceDate;
	}

	/**
	 * Sets the invoice date.
	 * 
	 * @param invoiceDate
	 *            the new invoice date
	 */
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	/**
	 * Gets the completion date.
	 * 
	 * @return the completion date
	 */
	public String getCompletionDate() {
		return completionDate;
	}

	/**
	 * Sets the completion date.
	 * 
	 * @param completionDate
	 *            the new completion date
	 */
	public void setCompletionDate(String completionDate) {
		this.completionDate = completionDate;
	}

	/**
	 * Gets the closed date.
	 * 
	 * @return the closed date
	 */
	public String getClosedDate() {
		return closedDate;
	}

	/**
	 * Sets the closed date.
	 * 
	 * @param closedDate
	 *            the new closed date
	 */
	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}

	/**
	 * Gets the invoice price.
	 * 
	 * @return the invoice price
	 */
	public String getInvoicePrice() {
		return invoicePrice;
	}

	/**
	 * Sets the invoice price.
	 * 
	 * @param invoicePrice
	 *            the new invoice price
	 */
	public void setInvoicePrice(String invoicePrice) {
		this.invoicePrice = invoicePrice;
	}

	/**
	 * Gets the invoice number.
	 * 
	 * @return the invoice number
	 */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	/**
	 * Sets the invoice number.
	 * 
	 * @param invoiceNumber
	 *            the new invoice number
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * Gets the customer ref.
	 * 
	 * @return the customer ref
	 */
	public String getCustomerRef() {
		return customerRef;
	}

	/**
	 * Sets the customer ref.
	 * 
	 * @param customerRef
	 *            the new customer ref
	 */
	public void setCustomerRef(String customerRef) {
		this.customerRef = customerRef;
	}

	/**
	 * Gets the site number.
	 * 
	 * @return the site number
	 */
	public String getSiteNumber() {
		return siteNumber;
	}

	/**
	 * Sets the site number.
	 * 
	 * @param siteNumber
	 *            the new site number
	 */
	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	/**
	 * Gets the site address.
	 * 
	 * @return the site address
	 */
	public String getSiteAddress() {
		return siteAddress;
	}

	/**
	 * Sets the site address.
	 * 
	 * @param siteAddress
	 *            the new site address
	 */
	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	/**
	 * Gets the site city.
	 * 
	 * @return the site city
	 */
	public String getSiteCity() {
		return siteCity;
	}

	/**
	 * Sets the site city.
	 * 
	 * @param siteCity
	 *            the new site city
	 */
	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	/**
	 * Gets the site country.
	 * 
	 * @return the site country
	 */
	public String getSiteCountry() {
		return siteCountry;
	}

	/**
	 * Sets the site country.
	 * 
	 * @param siteCountry
	 *            the new site country
	 */
	public void setSiteCountry(String siteCountry) {
		this.siteCountry = siteCountry;
	}

	/**
	 * Gets the site zip.
	 * 
	 * @return the site zip
	 */
	public String getSiteZip() {
		return siteZip;
	}

	/**
	 * Sets the site zip.
	 * 
	 * @param siteZip
	 *            the new site zip
	 */
	public void setSiteZip(String siteZip) {
		this.siteZip = siteZip;
	}

	/**
	 * Gets the latitude radiud.
	 * 
	 * @return the latitude radiud
	 */
	public String getLatitudeRadiud() {
		return latitudeRadiud;
	}

	/**
	 * Sets the latitude radiud.
	 * 
	 * @param latitudeRadiud
	 *            the new latitude radiud
	 */
	public void setLatitudeRadiud(String latitudeRadiud) {
		this.latitudeRadiud = latitudeRadiud;
	}

	/**
	 * Gets the longitude radius.
	 * 
	 * @return the longitude radius
	 */
	public String getLongitudeRadius() {
		return longitudeRadius;
	}

	/**
	 * Sets the longitude radius.
	 * 
	 * @param longitudeRadius
	 *            the new longitude radius
	 */
	public void setLongitudeRadius(String longitudeRadius) {
		this.longitudeRadius = longitudeRadius;
	}

	/**
	 * Gets the created by.
	 * 
	 * @return the created by
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the created by.
	 * 
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the updated by.
	 * 
	 * @return the updated by
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updated by.
	 * 
	 * @param updatedBy
	 *            the new updated by
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the job updated by.
	 * 
	 * @return the job updated by
	 */
	public String getJobUpdatedBy() {
		return jobUpdatedBy;
	}

	/**
	 * Sets the job updated by.
	 * 
	 * @param jobUpdatedBy
	 *            the new job updated by
	 */
	public void setJobUpdatedBy(String jobUpdatedBy) {
		this.jobUpdatedBy = jobUpdatedBy;
	}

	/**
	 * Gets the site state.
	 * 
	 * @return the site state
	 */
	public String getSiteState() {
		return siteState;
	}

	/**
	 * Sets the site state.
	 * 
	 * @param siteState
	 *            the new site state
	 */
	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	/**
	 * Gets the work location.
	 * 
	 * @return the work location
	 */
	public String getWorkLocation() {
		return workLocation;
	}

	/**
	 * Sets the work location.
	 * 
	 * @param workLocation
	 *            the new work location
	 */
	public void setWorkLocation(String workLocation) {
		this.workLocation = workLocation;
	}

	/**
	 * Gets the msp.
	 * 
	 * @return the msp
	 */
	public String getMsp() {
		return msp;
	}

	/**
	 * Sets the msp.
	 * 
	 * @param msp
	 *            the new msp
	 */
	public void setMsp(String msp) {
		this.msp = msp;
	}

	/**
	 * Gets the help desk.
	 * 
	 * @return the help desk
	 */
	public String getHelpDesk() {
		return helpDesk;
	}

	/**
	 * Sets the help desk.
	 * 
	 * @param helpDesk
	 *            the new help desk
	 */
	public void setHelpDesk(String helpDesk) {
		this.helpDesk = helpDesk;
	}

	/**
	 * Gets the issue owner.
	 * 
	 * @return the issue owner
	 */
	public String getIssueOwner() {
		return issueOwner;
	}

	/**
	 * Sets the issue owner.
	 * 
	 * @param issueOwner
	 *            the new issue owner
	 */
	public void setIssueOwner(String issueOwner) {
		this.issueOwner = issueOwner;
	}

	/**
	 * Gets the root cause.
	 * 
	 * @return the root cause
	 */
	public String getRootCause() {
		return rootCause;
	}

	/**
	 * Sets the root cause.
	 * 
	 * @param rootCause
	 *            the new root cause
	 */
	public void setRootCause(String rootCause) {
		this.rootCause = rootCause;
	}

	/**
	 * Gets the corrective aciton.
	 * 
	 * @return the corrective aciton
	 */
	public String getCorrectiveAciton() {
		return correctiveAciton;
	}

	/**
	 * Sets the corrective aciton.
	 * 
	 * @param correctiveAciton
	 *            the new corrective aciton
	 */
	public void setCorrectiveAciton(String correctiveAciton) {
		this.correctiveAciton = correctiveAciton;
	}

	/**
	 * Gets the pre poc.
	 * 
	 * @return the pre poc
	 */
	public String getPrePOC() {
		return prePOC;
	}

	/**
	 * Sets the pre poc.
	 * 
	 * @param prePOC
	 *            the new pre poc
	 */
	public void setPrePOC(String prePOC) {
		this.prePOC = prePOC;
	}

	/**
	 * Gets the phone number.
	 * 
	 * @return the phone number
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Sets the phone number.
	 * 
	 * @param phoneNumber
	 *            the new phone number
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
