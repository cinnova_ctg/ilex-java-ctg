package com.ilex.ws.core.bean;

import java.io.Serializable;

public class HDNextActionVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nextActionId;
	private String nextAction;

	public String getNextActionId() {
		return nextActionId;
	}

	public void setNextActionId(String nextActionId) {
		this.nextActionId = nextActionId;
	}

	public String getNextAction() {
		return nextAction;
	}

	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}

}
