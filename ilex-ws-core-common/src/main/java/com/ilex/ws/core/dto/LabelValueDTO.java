package com.ilex.ws.core.dto;

import java.io.Serializable;

public class LabelValueDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3545793278051103030L;
	private String label = null;
	private String value = null;

	public LabelValueDTO() {

	}

	public LabelValueDTO(String a, String b) {
		label = a;
		value = b;
	}

	public void setLabel(String l) {
		label = l;
	}

	public String getLabel() {
		return label;
	}

	public void setValue(String v) {
		value = v;
	}

	public String getValue() {
		return value;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof LabelValueDTO) {
			if (this.getLabel().equals(((LabelValueDTO) o).getLabel())
					&& this.getValue().equals(((LabelValueDTO) o).getValue()))
				return true;
		}
		return false;
	}
}
