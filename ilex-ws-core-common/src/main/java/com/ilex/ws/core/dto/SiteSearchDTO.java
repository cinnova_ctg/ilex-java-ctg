package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class SiteSearchDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String siteId;
	private String siteName;
	private String siteNumber;
	private String siteState;
	private String siteCity;
	private String siteBrand;
	private String siteEndCustomer;
	private List<LabelValueDTO> siteEndCustomers;
	Map<String, List<LabelValueDTO>> statesMap;
	private List<LabelValueDTO> siteBrands;
	List<SiteSearchDetailDTO> siteSearchDetails;
	private String jobId;
	private String jobTitle;
	private String appendixId;
	private String appendixTitle;
	private String msaId;
	private String msaName;

	private String customerRef;

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteBrand() {
		return siteBrand;
	}

	public void setSiteBrand(String siteBrand) {
		this.siteBrand = siteBrand;
	}

	public String getSiteEndCustomer() {
		return siteEndCustomer;
	}

	public void setSiteEndCustomer(String siteEndCustomer) {
		this.siteEndCustomer = siteEndCustomer;
	}

	public List<LabelValueDTO> getSiteEndCustomers() {
		return siteEndCustomers;
	}

	public List<SiteSearchDetailDTO> getSiteSearchDetails() {
		return siteSearchDetails;
	}

	public void setSiteSearchDetails(List<SiteSearchDetailDTO> siteSearchDetails) {
		this.siteSearchDetails = siteSearchDetails;
	}

	public void setSiteEndCustomers(List<LabelValueDTO> siteEndCustomers) {
		this.siteEndCustomers = siteEndCustomers;
	}

	public List<LabelValueDTO> getSiteBrands() {
		return siteBrands;
	}

	public void setSiteBrands(List<LabelValueDTO> siteBrands) {
		this.siteBrands = siteBrands;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getAppendixTitle() {
		return appendixTitle;
	}

	public void setAppendixTitle(String appendixTitle) {
		this.appendixTitle = appendixTitle;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	public Map<String, List<LabelValueDTO>> getStatesMap() {
		return statesMap;
	}

	public void setStatesMap(Map<String, List<LabelValueDTO>> statesMap) {
		this.statesMap = statesMap;
	}

	public String getCustomerRef() {
		return customerRef;
	}

	public void setCustomerRef(String customerRef) {
		this.customerRef = customerRef;
	}

}
