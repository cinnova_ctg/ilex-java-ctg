package com.ilex.ws.core.util;

public enum HdCubeDisplayStatus {
	PA_ALERT("\'PA-Alert\'"), PA_CUSTOMER("\'PA-Customer\'"), PA_MONITOR(
			"\'PA-Monitor\'"), WIP("\'WIP\'"), WIP_OD("\'WIP-OD\'");

	private String cubeName;

	HdCubeDisplayStatus(String cubeName) {
		this.cubeName = cubeName;
	}

	public String getCubeName() {
		return this.cubeName;
	}

}
