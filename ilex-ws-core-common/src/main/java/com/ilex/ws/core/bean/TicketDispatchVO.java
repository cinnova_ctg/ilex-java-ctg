package com.ilex.ws.core.bean;

import java.io.Serializable;

public class TicketDispatchVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long userId;
	private String userName;
	private String companyName;
	private String email;
	private Boolean sendEmail;
	private String requestorName;
	private String requestorPhone;
	private String siteNumber;
	private String sitePhone;
	private String siteLocation;
	private String siteAddress;
	private String siteCity;
	private String siteCountry;
	private String siteState;
	private String siteZipCode;
	private String poNumber;
	private Boolean onsiteFieldRequest;
	private Boolean remoteHelpDeskRequest;
	private Long appendixId;
	private Long msaId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getSendEmail() {
		return sendEmail;
	}

	public void setSendEmail(Boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getRequestorPhone() {
		return requestorPhone;
	}

	public void setRequestorPhone(String requestorPhone) {
		this.requestorPhone = requestorPhone;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getSitePhone() {
		return sitePhone;
	}

	public void setSitePhone(String sitePhone) {
		this.sitePhone = sitePhone;
	}

	public String getSiteLocation() {
		return siteLocation;
	}

	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteCountry() {
		return siteCountry;
	}

	public void setSiteCountry(String siteCountry) {
		this.siteCountry = siteCountry;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getSiteZipCode() {
		return siteZipCode;
	}

	public void setSiteZipCode(String siteZipCode) {
		this.siteZipCode = siteZipCode;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public Boolean getOnsiteFieldRequest() {
		return onsiteFieldRequest;
	}

	public void setOnsiteFieldRequest(Boolean onsiteFieldRequest) {
		this.onsiteFieldRequest = onsiteFieldRequest;
	}

	public Boolean getRemoteHelpDeskRequest() {
		return remoteHelpDeskRequest;
	}

	public void setRemoteHelpDeskRequest(Boolean remoteHelpDeskRequest) {
		this.remoteHelpDeskRequest = remoteHelpDeskRequest;
	}

	public Long getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(Long appendixId) {
		this.appendixId = appendixId;
	}

	public Long getMsaId() {
		return msaId;
	}

	public void setMsaId(Long msaId) {
		this.msaId = msaId;
	}

}
