package com.ilex.ws.core.dto;

import java.io.Serializable;

public class ResolutionsDTO implements Serializable {

    private String label;
    private String value;
    private String status;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
