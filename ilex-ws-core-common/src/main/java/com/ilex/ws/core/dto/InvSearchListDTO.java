package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.List;

public class InvSearchListDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<InvSearchDTO> invSearchDTOList;
	private Integer pageCount;
	private Integer pageIndex;
	private String clientSearchKeyword;

	public List<InvSearchDTO> getInvSearchDTOList() {
		return invSearchDTOList;
	}

	public void setInvSearchDTOList(List<InvSearchDTO> invSearchDTOList) {
		this.invSearchDTOList = invSearchDTOList;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public Integer getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	public String getClientSearchKeyword() {
		return clientSearchKeyword;
	}

	public void setClientSearchKeyword(String clientSearchKeyword) {
		this.clientSearchKeyword = clientSearchKeyword;
	}

}
