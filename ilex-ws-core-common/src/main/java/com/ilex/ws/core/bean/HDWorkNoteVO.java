package com.ilex.ws.core.bean;

import java.io.Serializable;
import java.util.Date;

public class HDWorkNoteVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String workNote;
	private Date workNoteDate;
	private String userLastName;
	private String workNoteType;

	public String getWorkNote() {
		return workNote;
	}

	public void setWorkNote(String workNote) {
		this.workNote = workNote;
	}

	public Date getWorkNoteDate() {
		return workNoteDate;
	}

	public void setWorkNoteDate(Date workNoteDate) {
		this.workNoteDate = workNoteDate;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getWorkNoteType() {
		return workNoteType;
	}

	public void setWorkNoteType(String workNoteType) {
		this.workNoteType = workNoteType;
	}

}
