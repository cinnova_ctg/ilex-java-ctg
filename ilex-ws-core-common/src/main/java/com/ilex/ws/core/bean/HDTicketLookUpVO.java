package com.ilex.ws.core.bean;

import java.io.Serializable;

public class HDTicketLookUpVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String customerReference;
	private String msp;
	private String helpDesk;
	private String category;

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public String getMsp() {
		return msp;
	}

	public void setMsp(String msp) {
		this.msp = msp;
	}

	public String getHelpDesk() {
		return helpDesk;
	}

	public void setHelpDesk(String helpDesk) {
		this.helpDesk = helpDesk;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
