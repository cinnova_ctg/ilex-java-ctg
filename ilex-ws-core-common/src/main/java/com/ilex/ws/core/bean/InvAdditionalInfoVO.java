package com.ilex.ws.core.bean;

import java.io.Serializable;

public class InvAdditionalInfoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int additionalInfoId;
	private String name;
	private String level;
	private String value;
	private String description;
	private Integer invSetupId;

	public int getAdditionalInfoId() {
		return additionalInfoId;
	}

	public void setAdditionalInfoId(int additionalInfoId) {
		this.additionalInfoId = additionalInfoId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getInvSetupId() {
		return invSetupId;
	}

	public void setInvSetupId(Integer invSetupId) {
		this.invSetupId = invSetupId;
	}

}
