package com.ilex.ws.core.bean;

import java.io.Serializable;

public class NetMedXConsolidatorRequestVO implements Serializable {
	private String jobId;
	private String projectId;// appendix id
	private String jobTitle;
	private String includeOutOfScope;
	private String status;
	private String requestedDate;
	private String scheduledDate;
	private String onsiteDate;
	private String offsiteDate;
	private String invoiceDate;
	private String completionDate;
	private String closedDate;
	private String invoicePrice;
	private String invoiceNumber;
	private String customerRef;
	private String siteNumber;
	private String siteAddress;
	private String siteCity;
	private String siteCountry;
	private String siteZip;
	private String latitudeRadiud;
	private String longitudeRadius;
	private String createdBy;
	private String updatedBy;
	private String jobUpdatedBy;
	private String siteState;
	private String workLocation;
	private String msp;
	private String helpDesk;
	private String issueOwner;
	private String rootCause;
	private String correctiveAciton;
	private String prePOC;
	private String phoneNumber;
	private String archivedPeriod;
	private int startLimit;
	private int endLimit;

	public String getArchivedPeriod() {
		return archivedPeriod;
	}

	public void setArchivedPeriod(String archivedPeriod) {
		this.archivedPeriod = archivedPeriod;
	}

	public int getStartLimit() {
		return startLimit;
	}

	public void setStartLimit(int startLimit) {
		this.startLimit = startLimit;
	}

	public int getEndLimit() {
		return endLimit;
	}

	public void setEndLimit(int endLimit) {
		this.endLimit = endLimit;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	private String sortOrder;
	private String sortBy;

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getIncludeOutOfScope() {
		return includeOutOfScope;
	}

	public void setIncludeOutOfScope(String includeOutOfScope) {
		this.includeOutOfScope = includeOutOfScope;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
	}

	public String getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(String scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	public String getOnsiteDate() {
		return onsiteDate;
	}

	public void setOnsiteDate(String onsiteDate) {
		this.onsiteDate = onsiteDate;
	}

	public String getOffsiteDate() {
		return offsiteDate;
	}

	public void setOffsiteDate(String offsiteDate) {
		this.offsiteDate = offsiteDate;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(String completionDate) {
		this.completionDate = completionDate;
	}

	public String getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}

	public String getInvoicePrice() {
		return invoicePrice;
	}

	public void setInvoicePrice(String invoicePrice) {
		this.invoicePrice = invoicePrice;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getCustomerRef() {
		return customerRef;
	}

	public void setCustomerRef(String customerRef) {
		this.customerRef = customerRef;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteCountry() {
		return siteCountry;
	}

	public void setSiteCountry(String siteCountry) {
		this.siteCountry = siteCountry;
	}

	public String getSiteZip() {
		return siteZip;
	}

	public void setSiteZip(String siteZip) {
		this.siteZip = siteZip;
	}

	public String getLatitudeRadiud() {
		return latitudeRadiud;
	}

	public void setLatitudeRadiud(String latitudeRadiud) {
		this.latitudeRadiud = latitudeRadiud;
	}

	public String getLongitudeRadius() {
		return longitudeRadius;
	}

	public void setLongitudeRadius(String longitudeRadius) {
		this.longitudeRadius = longitudeRadius;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getJobUpdatedBy() {
		return jobUpdatedBy;
	}

	public void setJobUpdatedBy(String jobUpdatedBy) {
		this.jobUpdatedBy = jobUpdatedBy;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getWorkLocation() {
		return workLocation;
	}

	public void setWorkLocation(String workLocation) {
		this.workLocation = workLocation;
	}

	public String getMsp() {
		return msp;
	}

	public void setMsp(String msp) {
		this.msp = msp;
	}

	public String getHelpDesk() {
		return helpDesk;
	}

	public void setHelpDesk(String helpDesk) {
		this.helpDesk = helpDesk;
	}

	public String getIssueOwner() {
		return issueOwner;
	}

	public void setIssueOwner(String issueOwner) {
		this.issueOwner = issueOwner;
	}

	public String getRootCause() {
		return rootCause;
	}

	public void setRootCause(String rootCause) {
		this.rootCause = rootCause;
	}

	public String getCorrectiveAciton() {
		return correctiveAciton;
	}

	public void setCorrectiveAciton(String correctiveAciton) {
		this.correctiveAciton = correctiveAciton;
	}

	public String getPrePOC() {
		return prePOC;
	}

	public void setPrePOC(String prePOC) {
		this.prePOC = prePOC;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
