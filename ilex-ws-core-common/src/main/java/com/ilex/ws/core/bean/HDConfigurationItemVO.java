package com.ilex.ws.core.bean;

import java.io.Serializable;

public class HDConfigurationItemVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String configurationItemId;
	private String configurationItem;

	public String getConfigurationItem() {
		return configurationItem;
	}

	public void setConfigurationItem(String configurationItem) {
		this.configurationItem = configurationItem;
	}

	public String getConfigurationItemId() {
		return configurationItemId;
	}

	public void setConfigurationItemId(String configurationItemId) {
		this.configurationItemId = configurationItemId;
	}

}
