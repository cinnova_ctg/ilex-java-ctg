package com.ilex.ws.core.bean;

import java.io.Serializable;

public class CustMonitorKeyVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String monitorKeyParameter;
	private boolean monitorKeyMandatory;
	private String monitoringKeyValue;

	public String getMonitorKeyParameter() {
		return monitorKeyParameter;
	}

	public void setMonitorKeyParameter(String monitorKeyParameter) {
		this.monitorKeyParameter = monitorKeyParameter;
	}

	public boolean isMonitorKeyMandatory() {
		return monitorKeyMandatory;
	}

	public void setMonitorKeyMandatory(boolean monitorKeyMandatory) {
		this.monitorKeyMandatory = monitorKeyMandatory;
	}

	public String getMonitoringKeyValue() {
		return monitoringKeyValue;
	}

	public void setMonitoringKeyValue(String monitoringKeyValue) {
		this.monitoringKeyValue = monitoringKeyValue;
	}

}
