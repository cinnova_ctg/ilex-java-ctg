package com.ilex.ws.core.dto;

import java.io.Serializable;

public class HDMonitoringTimeDTO implements Serializable {
	private String startDate;
	private String endDate;
	private String dateDiff;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDateDiff() {
		return dateDiff;
	}

	public void setDateDiff(String dateDiff) {
		this.dateDiff = dateDiff;
	}

}
