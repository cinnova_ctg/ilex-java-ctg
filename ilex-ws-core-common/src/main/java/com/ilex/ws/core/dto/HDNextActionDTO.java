package com.ilex.ws.core.dto;

import java.io.Serializable;

public class HDNextActionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nextActionId;
	private String nextAction;

	public String getNextActionId() {
		return nextActionId;
	}

	public void setNextActionId(String nextActionId) {
		this.nextActionId = nextActionId;
	}

	public String getNextAction() {
		return nextAction;
	}

	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}

}
