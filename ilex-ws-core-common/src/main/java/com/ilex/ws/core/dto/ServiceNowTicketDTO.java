package com.ilex.ws.core.dto;

import java.io.Serializable;

public class ServiceNowTicketDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String category;
	private String incidentState;
	private String shortDescription;
	private String active;
	private String ticketId;
	private String causedBy;
	private boolean serviceNowTicket;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getIncidentState() {
		return incidentState;
	}

	public void setIncidentState(String incidentState) {
		this.incidentState = incidentState;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getCausedBy() {
		return causedBy;
	}

	public void setCausedBy(String causedBy) {
		this.causedBy = causedBy;
	}

	public boolean isServiceNowTicket() {
		return serviceNowTicket;
	}

	public void setServiceNowTicket(boolean serviceNowTicket) {
		this.serviceNowTicket = serviceNowTicket;
	}

}
