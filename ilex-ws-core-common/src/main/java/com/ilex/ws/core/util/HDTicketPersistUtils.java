package com.ilex.ws.core.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class HDTicketPersistUtils {

	private static Map<String, HdPersistedTickets> persistedTickets = new ConcurrentHashMap<>();

	public static HdPersistedTickets get(String sessionId) {
		return persistedTickets.get(sessionId);
	}

	public static void removeTicket(String sessionId) {
		persistedTickets.remove(sessionId);
	}

	public static void persistUserTickets(String sessionId,
			HdPersistedTickets userTickets) {
		persistedTickets.put(sessionId, userTickets);
	}

	public static boolean isSessionTicketsActive(String sessionId) {
		return persistedTickets.containsKey(sessionId);
	}

}
