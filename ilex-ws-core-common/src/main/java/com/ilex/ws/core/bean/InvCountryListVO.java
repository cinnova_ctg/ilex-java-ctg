package com.ilex.ws.core.bean;

import java.io.Serializable;

public class InvCountryListVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String countryName;
	private String countryShortName;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public void setCountryShortName(String countryShortName) {
		this.countryShortName = countryShortName;
	}

	public String getCountryShortName() {
		return countryShortName;
	}

}
