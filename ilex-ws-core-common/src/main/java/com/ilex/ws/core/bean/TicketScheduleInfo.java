package com.ilex.ws.core.bean;

import java.io.Serializable;

import com.ilex.ws.core.dto.IlexTimestamp;

/**
 * @purpose This class is used for the representation of the TICKET Schedule
 *          Info.
 */
public class TicketScheduleInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IlexTimestamp requestedTimestamp = new IlexTimestamp();
	private IlexTimestamp preferredTimestamp = new IlexTimestamp();
	private IlexTimestamp windowFromTimestamp = new IlexTimestamp();
	private IlexTimestamp windowToTimestamp = new IlexTimestamp();
	private IlexTimestamp receivedTimestamp = new IlexTimestamp();

	public IlexTimestamp getPreferredTimestamp() {
		return preferredTimestamp;
	}

	public void setPreferredTimestamp(IlexTimestamp preferredTimestamp) {
		this.preferredTimestamp = preferredTimestamp;
	}

	public IlexTimestamp getReceivedTimestamp() {
		return receivedTimestamp;
	}

	public void setReceivedTimestamp(IlexTimestamp receivedTimestamp) {
		this.receivedTimestamp = receivedTimestamp;
	}

	public IlexTimestamp getRequestedTimestamp() {
		return requestedTimestamp;
	}

	public void setRequestedTimestamp(IlexTimestamp requestedTimestamp) {
		this.requestedTimestamp = requestedTimestamp;
	}

	public IlexTimestamp getWindowFromTimestamp() {
		return windowFromTimestamp;
	}

	public void setWindowFromTimestamp(IlexTimestamp windowFromTimestamp) {
		this.windowFromTimestamp = windowFromTimestamp;
	}

	public IlexTimestamp getWindowToTimestamp() {
		return windowToTimestamp;
	}

	public void setWindowToTimestamp(IlexTimestamp windowToTimestamp) {
		this.windowToTimestamp = windowToTimestamp;
	}
}
