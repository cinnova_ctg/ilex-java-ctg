package com.ilex.ws.core.bean;

import java.io.Serializable;

public class InvMasterLineItemDetailsVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String projectType;
	private String lineItemName;
	private String lineItemDescription;
	private boolean lineItemActive;

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getLineItemName() {
		return lineItemName;
	}

	public void setLineItemName(String lineItemName) {
		this.lineItemName = lineItemName;
	}

	public String getLineItemDescription() {
		return lineItemDescription;
	}

	public void setLineItemDescription(String lineItemDescription) {
		this.lineItemDescription = lineItemDescription;
	}

	public boolean isLineItemActive() {
		return lineItemActive;
	}

	public void setLineItemActive(boolean lineItemActive) {
		this.lineItemActive = lineItemActive;
	}

}
