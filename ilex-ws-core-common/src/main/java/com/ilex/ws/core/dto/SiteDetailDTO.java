package com.ilex.ws.core.dto;

import java.io.Serializable;

public class SiteDetailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appendixId;
	private String jobId;
	private String siteId;
	private String siteNumber;
	private String siteName;
	private String siteDesignator;
	private String siteWorkLocation;
	private String siteAddress;
	private String siteCity;
	private String state;
	private String country;
	private String stateSelect;
	private String siteZipcode;
	private String siteNotes;
	private String siteDirection;
	private String sitePhone;
	private String localityUplift;
	private String unionSite;
	private String primaryEmail;
	private String secondaryEmail;
	private String secondaryPhone;
	private String primaryName;
	private String secondaryName;
	private String appendixName;
	private String jobName;

	private String nettype;
	private String viewjobtype;
	private String ownerId;
	private String addendumId;

	// 02/28/2007
	private String siteLatDeg;
	private String siteLatMin;
	private String siteLatDirection;
	private String siteLonDeg;
	private String siteLonMin;
	private String siteLonDirection;

	private String siteStatus;
	private String siteCategory;
	private String siteRegion;
	private String siteGroup;
	private String siteEndCustomer;
	private String siteTimeZone;
	private String siteCountryId;
	private String siteCountryIdName;
	private String msaDd;
	private String msaName;
	private String siteLatLon;
	private String siteListId;
	private String msaId;
	private String installerPoc;
	private String sitePoc;
	private String latitude;
	private String longitude;
	private String createDate;
	private String createBy;
	private String changeDate;
	private String changeBy;
	private String inactiveCheck;
	private String latLongAccuracy;
	private String workNotes;
	private String ticketId;

	// Field for carry date field of daily Report
	private String date;

	// private String client1stContact;
	// private String clientHDPhoneNumber;
	// private String clientHDEmailAddress;
	// private String clientHDContactName;

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSiteDesignator() {
		return siteDesignator;
	}

	public void setSiteDesignator(String siteDesignator) {
		this.siteDesignator = siteDesignator;
	}

	public String getSiteWorkLocation() {
		return siteWorkLocation;
	}

	public void setSiteWorkLocation(String siteWorkLocation) {
		this.siteWorkLocation = siteWorkLocation;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getStateSelect() {
		return stateSelect;
	}

	public void setStateSelect(String stateSelect) {
		this.stateSelect = stateSelect;
	}

	public String getSiteZipcode() {
		return siteZipcode;
	}

	public void setSiteZipcode(String siteZipcode) {
		this.siteZipcode = siteZipcode;
	}

	public String getSiteNotes() {
		return siteNotes;
	}

	public void setSiteNotes(String siteNotes) {
		this.siteNotes = siteNotes;
	}

	public String getSiteDirection() {
		return siteDirection;
	}

	public void setSiteDirection(String siteDirection) {
		this.siteDirection = siteDirection;
	}

	public String getSitePhone() {
		return sitePhone;
	}

	public void setSitePhone(String sitePhone) {
		this.sitePhone = sitePhone;
	}

	public String getLocalityUplift() {
		return localityUplift;
	}

	public void setLocalityUplift(String localityUplift) {
		this.localityUplift = localityUplift;
	}

	public String getUnionSite() {
		return unionSite;
	}

	public void setUnionSite(String unionSite) {
		this.unionSite = unionSite;
	}

	public String getPrimaryEmail() {
		return primaryEmail;
	}

	public void setPrimaryEmail(String primaryEmail) {
		this.primaryEmail = primaryEmail;
	}

	public String getSecondaryEmail() {
		return secondaryEmail;
	}

	public void setSecondaryEmail(String secondaryEmail) {
		this.secondaryEmail = secondaryEmail;
	}

	public String getSecondaryPhone() {
		return secondaryPhone;
	}

	public void setSecondaryPhone(String secondaryPhone) {
		this.secondaryPhone = secondaryPhone;
	}

	public String getPrimaryName() {
		return primaryName;
	}

	public void setPrimaryName(String primaryName) {
		this.primaryName = primaryName;
	}

	public String getSecondaryName() {
		return secondaryName;
	}

	public void setSecondaryName(String secondaryName) {
		this.secondaryName = secondaryName;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getNettype() {
		return nettype;
	}

	public void setNettype(String nettype) {
		this.nettype = nettype;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getAddendumId() {
		return addendumId;
	}

	public void setAddendumId(String addendumId) {
		this.addendumId = addendumId;
	}

	public String getSiteLatDeg() {
		return siteLatDeg;
	}

	public void setSiteLatDeg(String siteLatDeg) {
		this.siteLatDeg = siteLatDeg;
	}

	public String getSiteLatMin() {
		return siteLatMin;
	}

	public void setSiteLatMin(String siteLatMin) {
		this.siteLatMin = siteLatMin;
	}

	public String getSiteLatDirection() {
		return siteLatDirection;
	}

	public void setSiteLatDirection(String siteLatDirection) {
		this.siteLatDirection = siteLatDirection;
	}

	public String getSiteLonDeg() {
		return siteLonDeg;
	}

	public void setSiteLonDeg(String siteLonDeg) {
		this.siteLonDeg = siteLonDeg;
	}

	public String getSiteLonMin() {
		return siteLonMin;
	}

	public void setSiteLonMin(String siteLonMin) {
		this.siteLonMin = siteLonMin;
	}

	public String getSiteLonDirection() {
		return siteLonDirection;
	}

	public void setSiteLonDirection(String siteLonDirection) {
		this.siteLonDirection = siteLonDirection;
	}

	public String getSiteStatus() {
		return siteStatus;
	}

	public void setSiteStatus(String siteStatus) {
		this.siteStatus = siteStatus;
	}

	public String getSiteCategory() {
		return siteCategory;
	}

	public void setSiteCategory(String siteCategory) {
		this.siteCategory = siteCategory;
	}

	public String getSiteRegion() {
		return siteRegion;
	}

	public void setSiteRegion(String siteRegion) {
		this.siteRegion = siteRegion;
	}

	public String getSiteGroup() {
		return siteGroup;
	}

	public void setSiteGroup(String siteGroup) {
		this.siteGroup = siteGroup;
	}

	public String getSiteEndCustomer() {
		return siteEndCustomer;
	}

	public void setSiteEndCustomer(String siteEndCustomer) {
		this.siteEndCustomer = siteEndCustomer;
	}

	public String getSiteTimeZone() {
		return siteTimeZone;
	}

	public void setSiteTimeZone(String siteTimeZone) {
		this.siteTimeZone = siteTimeZone;
	}

	public String getSiteCountryId() {
		return siteCountryId;
	}

	public void setSiteCountryId(String siteCountryId) {
		this.siteCountryId = siteCountryId;
	}

	public String getSiteCountryIdName() {
		return siteCountryIdName;
	}

	public void setSiteCountryIdName(String siteCountryIdName) {
		this.siteCountryIdName = siteCountryIdName;
	}

	public String getMsaDd() {
		return msaDd;
	}

	public void setMsaDd(String msaDd) {
		this.msaDd = msaDd;
	}

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	public String getSiteLatLon() {
		return siteLatLon;
	}

	public void setSiteLatLon(String siteLatLon) {
		this.siteLatLon = siteLatLon;
	}

	public String getSiteListId() {
		return siteListId;
	}

	public void setSiteListId(String siteListId) {
		this.siteListId = siteListId;
	}

	public String getInstallerPoc() {
		return installerPoc;
	}

	public void setInstallerPoc(String installerPoc) {
		this.installerPoc = installerPoc;
	}

	public String getSitePoc() {
		return sitePoc;
	}

	public void setSitePoc(String sitePoc) {
		this.sitePoc = sitePoc;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(String changeDate) {
		this.changeDate = changeDate;
	}

	public String getChangeBy() {
		return changeBy;
	}

	public void setChangeBy(String changeBy) {
		this.changeBy = changeBy;
	}

	public String getInactiveCheck() {
		return inactiveCheck;
	}

	public void setInactiveCheck(String inactiveCheck) {
		this.inactiveCheck = inactiveCheck;
	}

	public String getLatLongAccuracy() {
		return latLongAccuracy;
	}

	public void setLatLongAccuracy(String latLongAccuracy) {
		this.latLongAccuracy = latLongAccuracy;
	}

	public String getWorkNotes() {
		return workNotes;
	}

	public void setWorkNotes(String workNotes) {
		this.workNotes = workNotes;
	}

	// public String getClient1stContact() {
	// return client1stContact;
	// }
	//
	// public void setClient1stContact(String client1stContact) {
	// this.client1stContact = client1stContact;
	// }
	//
	// public String getClientHDPhoneNumber() {
	// return clientHDPhoneNumber;
	// }
	//
	// public void setClientHDPhoneNumber(String clientHDPhoneNumber) {
	// this.clientHDPhoneNumber = clientHDPhoneNumber;
	// }
	//
	// public String getClientHDEmailAddress() {
	// return clientHDEmailAddress;
	// }
	//
	// public void setClientHDEmailAddress(String clientHDEmailAddress) {
	// this.clientHDEmailAddress = clientHDEmailAddress;
	// }
	//
	// public String getClientHDContactName() {
	// return clientHDContactName;
	// }
	//
	// public void setClientHDContactName(String clientHDContactName) {
	// this.clientHDContactName = clientHDContactName;
	// }

}
