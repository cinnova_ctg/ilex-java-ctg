/*
 * <h4>Description</h4>
 * All system exception are wrapped to this class.
 * 
 * @author tarungupta
 */
package com.ilex.ws.core.exception;

public class IlexSystemException extends IlexException {

	private static final long serialVersionUID = 2763187418243954651L;

	public IlexSystemException(String key, String[] args, String defaultMessage) {
		super(ErrorType.SYSTEM, defaultMessage);
		this.key = key;
		this.errorArgs = args;
		this.defaultMessage = defaultMessage;
	}

	public IlexSystemException(String key) {
		super(ErrorType.SYSTEM, key);
		this.key = key;
	}

	public IlexSystemException(String key, String[] args) {
		super(ErrorType.SYSTEM, key);
		this.key = key;
		this.errorArgs = args;
	}

	public IlexSystemException(String key, String defaultMessage) {
		super(ErrorType.SYSTEM, key);
		this.key = key;
		this.defaultMessage = defaultMessage;
	}

	public IlexSystemException(String key, Throwable th) {
		super(ErrorType.SYSTEM, key, th);
		this.key = key;
	}

	public IlexSystemException(String key, Throwable th, String defaultMessage) {
		super(ErrorType.SYSTEM, key, th);
		this.key = key;
		this.defaultMessage = defaultMessage;
	}

	public IlexSystemException(String key, String[] args, Throwable th,
			String defaultMessage) {
		super(ErrorType.SYSTEM, key, th);
		this.key = key;
		this.errorArgs = args;
		this.defaultMessage = defaultMessage;
	}
}
