package com.ilex.ws.core.dto;

import java.io.Serializable;

public class OutageDetailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String ticketId;
	String wugIpName;
	boolean checked;

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getWugIpName() {
		return wugIpName;
	}

	public void setWugIpName(String wugIpName) {
		this.wugIpName = wugIpName;
	}

}
