package com.ilex.ws.core.dto;

import java.io.Serializable;
import java.util.List;

public class NewDispatchDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String requestor;
	private String requestorEmail;
	private String requestType;
	private String criticality;
	private String resource;
	private boolean pps;
	private boolean standBy;
	private boolean msp;
	private boolean helpDesk;
	private String customerReference;
	private String problemCategory;
	private String problemDescription;
	private String specialInstructions;
	private String ticketRules;
	private String requestedDate;
	private String requestedHour;
	private String requestedMinute;
	private String requestedIsAm;
	private String preferredDate;
	private String preferredHour;
	private String preferredMinute;
	private String preferredIsAm;
	private String windowFromDate;
	private String windowFromHour;
	private String windowFromMinute;
	private String windowFromIsAm;
	private String windowToDate;
	private String windowToHour;
	private String windowToMinute;
	private String windowToIsAm;
	private String receivedDate;
	private String receivedHour;
	private String receivedMinute;
	private String receivedIsAm;
	private String customerSpecificDetails;
	private String requestSummary;
	private String arrivalDate;
	private List<LabelValueDTO> requestors;
	private List<LabelValueDTO> requestTypes;
	private List<LabelValueDTO> criticalities;
	private List<LabelValueDTO> problemCategories;
	private List<LabelValueDTO> requestedHourList = null;
	private List<LabelValueDTO> requestedMinuteList = null;
	private List<LabelValueDTO> preferredHourList = null;
	private List<LabelValueDTO> preferredMinuteList = null;
	private List<LabelValueDTO> windowFromHourList = null;
	private List<LabelValueDTO> windowFromMinuteList = null;
	private List<LabelValueDTO> windowToHourList = null;
	private List<LabelValueDTO> windowToMinuteList = null;
	private Long ticketId;
	private Long appendixId;
	private Long jobId;
	private String ticketNumber;
	private SiteDetailDTO siteDetails;

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public Long getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(Long appendixId) {
		this.appendixId = appendixId;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String getRequestorEmail() {
		return requestorEmail;
	}

	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getCriticality() {
		return criticality;
	}

	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public boolean isPps() {
		return pps;
	}

	public void setPps(boolean pps) {
		this.pps = pps;
	}

	public boolean isStandBy() {
		return standBy;
	}

	public void setStandBy(boolean standBy) {
		this.standBy = standBy;
	}

	public boolean isMsp() {
		return msp;
	}

	public void setMsp(boolean msp) {
		this.msp = msp;
	}

	public boolean isHelpDesk() {
		return helpDesk;
	}

	public void setHelpDesk(boolean helpDesk) {
		this.helpDesk = helpDesk;
	}

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public String getProblemCategory() {
		return problemCategory;
	}

	public void setProblemCategory(String problemCategory) {
		this.problemCategory = problemCategory;
	}

	public String getProblemDescription() {
		return problemDescription;
	}

	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}

	public String getSpecialInstructions() {
		return specialInstructions;
	}

	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public String getTicketRules() {
		return ticketRules;
	}

	public void setTicketRules(String ticketRules) {
		this.ticketRules = ticketRules;
	}

	public String getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
	}

	public String getRequestedHour() {
		return requestedHour;
	}

	public void setRequestedHour(String requestedHour) {
		this.requestedHour = requestedHour;
	}

	public String getRequestedMinute() {
		return requestedMinute;
	}

	public void setRequestedMinute(String requestedMinute) {
		this.requestedMinute = requestedMinute;
	}

	public String getRequestedIsAm() {
		return requestedIsAm;
	}

	public void setRequestedIsAm(String requestedIsAm) {
		this.requestedIsAm = requestedIsAm;
	}

	public String getPreferredDate() {
		return preferredDate;
	}

	public void setPreferredDate(String preferredDate) {
		this.preferredDate = preferredDate;
	}

	public String getPreferredHour() {
		return preferredHour;
	}

	public void setPreferredHour(String preferredHour) {
		this.preferredHour = preferredHour;
	}

	public String getPreferredMinute() {
		return preferredMinute;
	}

	public void setPreferredMinute(String preferredMinute) {
		this.preferredMinute = preferredMinute;
	}

	public String getPreferredIsAm() {
		return preferredIsAm;
	}

	public void setPreferredIsAm(String preferredIsAm) {
		this.preferredIsAm = preferredIsAm;
	}

	public String getWindowFromDate() {
		return windowFromDate;
	}

	public void setWindowFromDate(String windowFromDate) {
		this.windowFromDate = windowFromDate;
	}

	public String getWindowFromHour() {
		return windowFromHour;
	}

	public void setWindowFromHour(String windowFromHour) {
		this.windowFromHour = windowFromHour;
	}

	public String getWindowFromMinute() {
		return windowFromMinute;
	}

	public void setWindowFromMinute(String windowFromMinute) {
		this.windowFromMinute = windowFromMinute;
	}

	public String getWindowFromIsAm() {
		return windowFromIsAm;
	}

	public void setWindowFromIsAm(String windowFromIsAm) {
		this.windowFromIsAm = windowFromIsAm;
	}

	public String getWindowToDate() {
		return windowToDate;
	}

	public void setWindowToDate(String windowToDate) {
		this.windowToDate = windowToDate;
	}

	public String getWindowToHour() {
		return windowToHour;
	}

	public void setWindowToHour(String windowToHour) {
		this.windowToHour = windowToHour;
	}

	public String getWindowToMinute() {
		return windowToMinute;
	}

	public void setWindowToMinute(String windowToMinute) {
		this.windowToMinute = windowToMinute;
	}

	public String getWindowToIsAm() {
		return windowToIsAm;
	}

	public void setWindowToIsAm(String windowToIsAm) {
		this.windowToIsAm = windowToIsAm;
	}

	public String getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}

	public String getReceivedHour() {
		return receivedHour;
	}

	public void setReceivedHour(String receivedHour) {
		this.receivedHour = receivedHour;
	}

	public String getReceivedMinute() {
		return receivedMinute;
	}

	public void setReceivedMinute(String receivedMinute) {
		this.receivedMinute = receivedMinute;
	}

	public String getReceivedIsAm() {
		return receivedIsAm;
	}

	public void setReceivedIsAm(String receivedIsAm) {
		this.receivedIsAm = receivedIsAm;
	}

	public String getCustomerSpecificDetails() {
		return customerSpecificDetails;
	}

	public void setCustomerSpecificDetails(String customerSpecificDetails) {
		this.customerSpecificDetails = customerSpecificDetails;
	}

	public String getRequestSummary() {
		return requestSummary;
	}

	public void setRequestSummary(String requestSummary) {
		this.requestSummary = requestSummary;
	}

	public List<LabelValueDTO> getRequestors() {
		return requestors;
	}

	public void setRequestors(List<LabelValueDTO> requestors) {
		this.requestors = requestors;
	}

	public List<LabelValueDTO> getRequestTypes() {
		return requestTypes;
	}

	public void setRequestTypes(List<LabelValueDTO> requestTypes) {
		this.requestTypes = requestTypes;
	}

	public List<LabelValueDTO> getCriticalities() {
		return criticalities;
	}

	public void setCriticalities(List<LabelValueDTO> criticalities) {
		this.criticalities = criticalities;
	}

	public List<LabelValueDTO> getProblemCategories() {
		return problemCategories;
	}

	public void setProblemCategories(List<LabelValueDTO> problemCategories) {
		this.problemCategories = problemCategories;
	}

	public List<LabelValueDTO> getRequestedHourList() {
		return requestedHourList;
	}

	public void setRequestedHourList(List<LabelValueDTO> requestedHourList) {
		this.requestedHourList = requestedHourList;
	}

	public List<LabelValueDTO> getRequestedMinuteList() {
		return requestedMinuteList;
	}

	public void setRequestedMinuteList(List<LabelValueDTO> requestedMinuteList) {
		this.requestedMinuteList = requestedMinuteList;
	}

	public List<LabelValueDTO> getPreferredHourList() {
		return preferredHourList;
	}

	public void setPreferredHourList(List<LabelValueDTO> preferredHourList) {
		this.preferredHourList = preferredHourList;
	}

	public List<LabelValueDTO> getPreferredMinuteList() {
		return preferredMinuteList;
	}

	public void setPreferredMinuteList(List<LabelValueDTO> preferredMinuteList) {
		this.preferredMinuteList = preferredMinuteList;
	}

	public List<LabelValueDTO> getWindowFromHourList() {
		return windowFromHourList;
	}

	public void setWindowFromHourList(List<LabelValueDTO> windowFromHourList) {
		this.windowFromHourList = windowFromHourList;
	}

	public List<LabelValueDTO> getWindowFromMinuteList() {
		return windowFromMinuteList;
	}

	public void setWindowFromMinuteList(List<LabelValueDTO> windowFromMinuteList) {
		this.windowFromMinuteList = windowFromMinuteList;
	}

	public List<LabelValueDTO> getWindowToHourList() {
		return windowToHourList;
	}

	public void setWindowToHourList(List<LabelValueDTO> windowToHourList) {
		this.windowToHourList = windowToHourList;
	}

	public List<LabelValueDTO> getWindowToMinuteList() {
		return windowToMinuteList;
	}

	public void setWindowToMinuteList(List<LabelValueDTO> windowToMinuteList) {
		this.windowToMinuteList = windowToMinuteList;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public SiteDetailDTO getSiteDetails() {
		return siteDetails;
	}

	public void setSiteDetails(SiteDetailDTO siteDetails) {
		this.siteDetails = siteDetails;
	}

}
