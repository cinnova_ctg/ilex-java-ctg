package com.ilex.ws.core.util;

import java.util.ArrayList;
import java.util.List;

import com.ilex.ws.core.dto.IlexDateComboDTO;
import com.ilex.ws.core.dto.LabelValue;

public class IlexDateUtils {

	/**
	 * Gets the options name for date AM and PM
	 * 
	 * @return the options
	 */
	public static List<LabelValue> getOptions() {

		List<LabelValue> optionName = new ArrayList<LabelValue>();
		optionName.add(new LabelValue("AM", "AM"));
		optionName.add(new LabelValue("PM", "PM"));

		return optionName;
	}

	/**
	 * Gets the hours.
	 * 
	 * @return the hours
	 */
	public static List<LabelValue> getHours() {
		List<LabelValue> hourList = new ArrayList<LabelValue>();
		String val = "";
		for (int i = 0; i <= 12; i++) {
			if (i < 10)
				val = "0" + String.valueOf(i);
			else
				val = String.valueOf(i);
			hourList.add(new LabelValue(val, val));
		}
		return hourList;
	}

	/**
	 * Gets the minutes.
	 * 
	 * @return the minutes
	 */
	public static List<LabelValue> getMinutes() {
		List<LabelValue> minuteList = new ArrayList<LabelValue>();
		String val = "";
		for (int i = 0; i <= 59; i++) {
			if (i < 10)
				val = "0" + String.valueOf(i);
			else
				val = String.valueOf(i);
			minuteList.add(new LabelValue(val, val));

		}
		return minuteList;
	}

	public static IlexDateComboDTO fillIlexDateCombo() {
		IlexDateComboDTO dateCombo = new IlexDateComboDTO();
		dateCombo.setHours(IlexDateUtils.getHours());
		dateCombo.setMinutes(IlexDateUtils.getMinutes());
		dateCombo.setOptions(IlexDateUtils.getOptions());
		return dateCombo;
	}

}
