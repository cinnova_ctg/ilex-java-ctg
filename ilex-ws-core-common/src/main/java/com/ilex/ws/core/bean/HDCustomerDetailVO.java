package com.ilex.ws.core.bean;

import java.io.Serializable;

public class HDCustomerDetailVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long appendixId;
	private Long msaId;
	private String appendixName;
	private String customerName;
	private String apppendicBreakOut;

	public Long getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(Long appendixId) {
		this.appendixId = appendixId;
	}

	public Long getMsaId() {
		return msaId;
	}

	public void setMsaId(Long msaId) {
		this.msaId = msaId;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getApppendicBreakOut() {
		return apppendicBreakOut;
	}

	public void setApppendicBreakOut(String apppendicBreakOut) {
		this.apppendicBreakOut = apppendicBreakOut;
	}

}
