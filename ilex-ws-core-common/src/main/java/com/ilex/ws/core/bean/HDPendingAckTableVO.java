package com.ilex.ws.core.bean;

import java.io.Serializable;
import java.util.Date;

public class HDPendingAckTableVO implements Serializable {

	/**
     *
     */
	private static final long serialVersionUID = 1L;
	private Date openedDateDB;
	private String state;
	private String source;
	private String customer;
	private String siteDisplayName;
	private String siteNumber;
	private String siteAddress;
	private String siteCity;
	private String siteState;
	private String siteCountry;
	private String siteZipcode;
	private String contactPhoneNumber;
	private String contactName;
	private String contactEmailAddress;
	private String ticketId;
	private String lastTd;
	private String status;
	private String appendixName;
	private String ilexNumber;
	private String dateDiff;
	private String dateDiffWithoutIcon;
	private String site_prop;
	private String nextActionLabel;

	public Date getOpenedDateDB() {
		return openedDateDB;
	}

	public void setOpenedDateDB(Date openedDateDB) {
		this.openedDateDB = openedDateDB;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getSiteDisplayName() {
		return siteDisplayName;
	}

	public void setSiteDisplayName(String siteDisplayName) {
		this.siteDisplayName = siteDisplayName;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getSiteCountry() {
		return siteCountry;
	}

	public void setSiteCountry(String siteCountry) {
		this.siteCountry = siteCountry;
	}

	public String getSiteZipcode() {
		return siteZipcode;
	}

	public void setSiteZipcode(String siteZipcode) {
		this.siteZipcode = siteZipcode;
	}

	public String getLastTd() {
		return lastTd;
	}

	public void setLastTd(String lastTd) {
		this.lastTd = lastTd;
	}

	public String getContactPhoneNumber() {
		return contactPhoneNumber;
	}

	public void setContactPhoneNumber(String contactPhoneNumber) {
		this.contactPhoneNumber = contactPhoneNumber;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getIlexNumber() {
		return ilexNumber;
	}

	public void setIlexNumber(String ilexNumber) {
		this.ilexNumber = ilexNumber;
	}

	public String getDateDiff() {
		return dateDiff;
	}

	public void setDateDiff(String dateDiff) {
		this.dateDiff = dateDiff;
	}

	public String getNextActionLabel() {
		return nextActionLabel;
	}

	public void setNextActionLabel(String nextActionLabel) {
		this.nextActionLabel = nextActionLabel;
	}

	public String getDateDiffWithoutIcon() {
		return dateDiffWithoutIcon;
	}

	public void setDateDiffWithoutIcon(String dateDiffWithoutIcon) {
		this.dateDiffWithoutIcon = dateDiffWithoutIcon;
	}

	public String getSite_prop() {
		return site_prop;
	}

	public void setSite_prop(String site_prop) {
		this.site_prop = site_prop;
	}

}
