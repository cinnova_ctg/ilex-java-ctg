package com.ilex.ws.core.bean;

import java.io.Serializable;

public class PrjGroupIntegrationVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String prjGroupIntegrationId;
	private String groupName;
	private String wugInstance;
	private String groupIntegrationDate;
	private String integrationStatus;

	public String getPrjGroupIntegrationId() {
		return prjGroupIntegrationId;
	}

	public void setPrjGroupIntegrationId(String prjGroupIntegrationId) {
		this.prjGroupIntegrationId = prjGroupIntegrationId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getWugInstance() {
		return wugInstance;
	}

	public void setWugInstance(String wugInstance) {
		this.wugInstance = wugInstance;
	}

	public String getGroupIntegrationDate() {
		return groupIntegrationDate;
	}

	public void setGroupIntegrationDate(String groupIntegrationDate) {
		this.groupIntegrationDate = groupIntegrationDate;
	}

	public String getIntegrationStatus() {
		return integrationStatus;
	}

	public void setIntegrationStatus(String integrationStatus) {
		this.integrationStatus = integrationStatus;
	}

}
