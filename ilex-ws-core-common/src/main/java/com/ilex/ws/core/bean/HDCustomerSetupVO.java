package com.ilex.ws.core.bean;

import java.io.Serializable;

public class HDCustomerSetupVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String customerName;
	private String appendixTitle;
	private Long customerId;
	private Long appendixId;
	private Long msaId;
	private String activeMonitorApproach;
	private String customerType;
	private String downStateName;
	private String downStateCode;
	private String status;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAppendixTitle() {
		return appendixTitle;
	}

	public void setAppendixTitle(String appendixTitle) {
		this.appendixTitle = appendixTitle;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(Long appendixId) {
		this.appendixId = appendixId;
	}

	public Long getMsaId() {
		return msaId;
	}

	public void setMsaId(Long msaId) {
		this.msaId = msaId;
	}

	public String getActiveMonitorApproach() {
		return activeMonitorApproach;
	}

	public void setActiveMonitorApproach(String activeMonitorApproach) {
		this.activeMonitorApproach = activeMonitorApproach;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getDownStateName() {
		return downStateName;
	}

	public void setDownStateName(String downStateName) {
		this.downStateName = downStateName;
	}

	public String getDownStateCode() {
		return downStateCode;
	}

	public void setDownStateCode(String downStateCode) {
		this.downStateCode = downStateCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
