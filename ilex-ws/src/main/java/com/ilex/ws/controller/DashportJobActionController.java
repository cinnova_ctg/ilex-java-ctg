package com.ilex.ws.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.core.dto.JobInformationDTO;
import com.ilex.ws.core.dto.SchedulerDTO;

/**
 * The Interface DashportJobActionController.
 */
public interface DashportJobActionController {

	/**
	 * Gets the job details for info purpose.
	 * 
	 * @param jobId
	 *            the job id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the job info
	 */
	ModelAndView getJobInfo(String jobId, ModelMap model,
			HttpServletRequest request);

	/**
	 * Open installation notes Screen to add notes.
	 * 
	 * @param jobId
	 *            the job id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView enterInstallNotes(String jobId, ModelMap model,
			HttpServletRequest request);

	/**
	 * Adds the installation notes to datatbase.
	 * 
	 * @param jobInformationDTO
	 *            the job information dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 */
	void addInstallNotes(JobInformationDTO jobInformationDTO, ModelMap model,
			HttpServletRequest request, HttpServletResponse response);

	/**
	 * Gets the scheduled planned date.
	 * 
	 * @param jobId
	 *            the job id
	 * @param schedulerDTO
	 *            the scheduler dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the schedule planned date
	 */
	ModelAndView getSchedulePlannedDate(String jobId,
			SchedulerDTO schedulerDTO, BindingResult result, ModelMap model);

	/**
	 * Save scheduler information.
	 * 
	 * @param schedulerDTO
	 *            the scheduler dto
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 */
	void saveSchedulerInfo(SchedulerDTO schedulerDTO,
			HttpServletRequest request, HttpServletResponse response);

	/**
	 * Gets the re schedule planned date.
	 * 
	 * @param jobId
	 *            the job id
	 * @param schedulerDTO
	 *            the scheduler dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the re schedule planned date
	 */
	ModelAndView getReSchedulePlannedDate(String jobId,
			SchedulerDTO schedulerDTO, BindingResult result, ModelMap model);

	/**
	 * Gets the onsite date.
	 * 
	 * @param jobId
	 *            the job id
	 * @param schedulerDTO
	 *            the scheduler dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the onsite date
	 */
	ModelAndView getOnsiteDate(String jobId, SchedulerDTO schedulerDTO,
			BindingResult result, ModelMap model);

	/**
	 * Gets the offsite date.
	 * 
	 * @param jobId
	 *            the job id
	 * @param schedulerDTO
	 *            the scheduler dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the offsite date
	 */
	ModelAndView getOffsiteDate(String jobId, SchedulerDTO schedulerDTO,
			BindingResult result, ModelMap model);

	/**
	 * Update job category priority value.
	 * 
	 * @param jobInformationDTO
	 *            the job information dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 */
	void updateJobCategoryPriority(JobInformationDTO jobInformationDTO,
			ModelMap model, HttpServletRequest request,
			HttpServletResponse response);

	/**
	 * Gets the job category priority.
	 * 
	 * @param jobId
	 *            the job id
	 * @param jobInformationDTO
	 *            the job information dto
	 * @param model
	 *            the model
	 * @return the job category priority
	 */
	ModelAndView getJobCategoryPriority(String jobId,
			JobInformationDTO jobInformationDTO, ModelMap model);

	/**
	 * Save re scheduler info.
	 * 
	 * @param schedulerDTO
	 *            the scheduler dto
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 */
	void saveReSchedulerInfo(SchedulerDTO schedulerDTO,
			HttpServletRequest request, HttpServletResponse response);

	/**
	 * Gets the task reminder Screen.
	 * 
	 * @param jobId
	 *            the job id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the task reminder
	 */
	ModelAndView getTaskReminder(String jobId, ModelMap model,
			HttpServletRequest request);

	/**
	 * Switch job owner.
	 * 
	 * @param jobId
	 *            the job id
	 * @param appendixId
	 *            the appendix id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView switchJobOwner(String jobId, String appendixId,
			ModelMap model, HttpServletRequest request);

	/**
	 * Update job owner.
	 * 
	 * @param jobInformationDTO
	 *            the job informationdto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 */
	void updateJobOwner(JobInformationDTO jobInformationDTO, ModelMap model,
			HttpServletRequest request, HttpServletResponse response);

	/**
	 * Enter customer field.
	 * 
	 * @param jobId
	 *            the job id
	 * @param appendixId
	 *            the appendix id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView enterCustomerField(String jobId, String appendixId,
			ModelMap model, HttpServletRequest request);

	/**
	 * Update customer field.
	 * 
	 * @param jobId
	 *            the job id
	 * @param appendixId
	 *            the appendix id
	 * @param customerValues
	 *            the customer values
	 * @param appendixCustomerIds
	 *            the appendix customer ids
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 */
	void updateCustomerField(String jobId, String appendixId,
			String customerValues, String appendixCustomerIds, ModelMap model,
			HttpServletRequest request, HttpServletResponse response);

	/**
	 * View installation notes.
	 * 
	 * @param jobId
	 *            the job id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView viewInstallationNotes(String jobId, ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets the partners list.
	 * 
	 * @param zipCode
	 *            the zip code
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the partners list
	 */
	ModelAndView getPartnersList(String zipCode, ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets the partner info.
	 * 
	 * @param partnerId
	 *            the partner id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the partner info
	 */
	ModelAndView getPartnerInfo(String partnerId, ModelMap model,
			HttpServletRequest request);
}
