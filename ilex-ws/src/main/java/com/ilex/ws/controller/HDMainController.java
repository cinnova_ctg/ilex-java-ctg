package com.ilex.ws.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.core.dto.HDTicketDTO;
import com.ilex.ws.core.dto.HdCancelAllDTO;
import com.ilex.ws.core.dto.SiteDetailDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface HDMainController.
 */
public interface HDMainController {

	/**
	 * Gets the hD table page.
	 * 
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the hD table page
	 */
	ModelAndView getHDTablePage(ModelMap model, HttpServletRequest request,
			HttpServletResponse response);

	/**
	 * Gets data for hD pending ack customer cube.
	 * 
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the hD pending ack table data
	 */
	String getHDPendingAckCustTableData(ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets data for HD pending ack security cube.
	 * 
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the hD pending ack table data
	 */
	String getHDPendingAckSecurityTableData(ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets the backup circuit status.
	 * 
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the backup circuit status
	 */
	String getBackupCircuitStatus(ModelMap model, HttpServletRequest request);

	/**
	 * Effort change manage.
	 * 
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the string
	 */
	String effortChangeManage(ModelMap model, HttpServletRequest request);

	/**
	 * Gets data for the hDWIP cube.
	 * 
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the hDWIP table data
	 */
	String getHDWIPTableData(ModelMap model, HttpServletRequest request);

	/**
	 * Check words.
	 * 
	 * @param text
	 *            string to check for words
	 * @return Array of the wrong spell words[index,length]
	 */
	String checkWords(String text);

	/**
	 * Suggest words.
	 * 
	 * @param suggest
	 *            wrong word
	 * @return the suggested words
	 */
	String suggestWords(String suggest);

	/**
	 * Generate excel view.
	 * 
	 * @param tableId
	 *            the table id
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the model and view
	 */
	ModelAndView generateExcelView(String tableId, HttpServletRequest request,
			HttpServletResponse response);

	/**
	 * Gets the enlarge cube view.
	 * 
	 * @param tableId
	 *            the table id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the enlarge cube view
	 */
	ModelAndView getEnlargeCubeView(String tableId, ModelMap model,
			HttpServletRequest request, HttpServletResponse response);

	/**
	 * To setup the Event Correlator
	 * 
	 * @param Type
	 *            customer or staff
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the enlarge cube view
	 */

	public ModelAndView setupEventCorrelator(String type, String customerInfo,
			String condition, String duration, String threshold, String color,
			String save, String id, ModelMap model, HttpServletRequest request,
			HttpServletResponse response);

	/**
	 * Gets data for the hDWIP overdue cube.
	 * 
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the hDWIP over due table data
	 */
	String getHDWIPOverDueTableData(ModelMap model, HttpServletRequest request);

	/**
	 * Gets data for the hDWIP nextAction cube.
	 * 
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the hDWIP next action table data
	 */
	String getHDWIPNextActionTableData(ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets data for the hD Resolved cube.
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	String getHDResolvedTableData(ModelMap model, HttpServletRequest request);

	/**
	 * Creates the ticket step1.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param appendixId
	 *            the appendix id
	 * @param impact
	 *            the impact
	 * @param urgency
	 *            the urgency
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView createTicketStep1(HDTicketDTO hdTicketDTO, Long appendixId,
			String impact, String urgency, ModelMap model);

	/**
	 * Gets the ticket summary.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param model
	 *            the model
	 * @return the ticket summary
	 */
	ModelAndView getTicketSummary(Long ticketId, ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets the ack ticket summary.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param model
	 *            the model
	 * @return the ack ticket summary
	 */
	ModelAndView getAckTicketSummary(Long ticketId, ModelMap model);

	/**
	 * Creates the ticket step2.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param hdCustomer
	 *            the hd customer
	 * @param isFromMain
	 *            the is from main
	 * @param hdTicketNumber
	 *            the hd ticket number
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView createTicketStep2(HDTicketDTO hdTicketDTO, String hdCustomer,
			String isFromMain, String hdTicketNumber, ModelMap model);

	/**
	 * Gets data for the hD pending ack alert cube.
	 * 
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the hD pending ack alert table data
	 */
	String getHDPendingAckAlertTableData(ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets data for the hD pending ack monitor cube.
	 * 
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the hD pending ack monitor table data
	 */
	String getHDPendingAckMonitorTableData(ModelMap model,
			HttpServletRequest request);

	String getHDPendingAckMonitorTier2TableData(ModelMap model,
			HttpServletRequest request);

	/**
	 * Creates the hd ticket.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the string
	 */
	String createHDTicket(HDTicketDTO hdTicketDTO, ModelMap model,
			HttpServletRequest request);

	/**
	 * Update hd ticket.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the string
	 */
	String updateHDTicket(HDTicketDTO hdTicketDTO, ModelMap model,
			HttpServletRequest request);

	/**
	 * Acknowledges a ticket.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param request
	 *            the request
	 * @return the string
	 */
	String ackTicket(HDTicketDTO hdTicketDTO, HttpServletRequest request);

	/**
	 * Gets the acknowledge ticket step 1 form.
	 * 
	 * @param ackCubeType
	 *            (customer, alert or monitor)
	 * @param ticketId
	 *            the ticket id for which we are retrieving the details.
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView ackTicketStep1(String ackCubeType, String ticketId,
			ModelMap model);

	/**
	 * Gets the cube status.
	 * 
	 * @return the cube status
	 */
	String getCubeStatus();

	/**
	 * Clear session.
	 * 
	 * @param request
	 *            the request
	 */
	void clearSession(HttpServletRequest request);

	/**
	 * Gets the alert state page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the alert state page
	 */
	ModelAndView getAlertStatePage(String ticketId);

	/**
	 * Gets the edit ticket details page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView editTicketDetailsPage(String ticketId, ModelMap model);

	/**
	 * Edits the ticket details.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @return the string
	 */
	String editTicketDetails(HDTicketDTO hdTicketDTO);

	/**
	 * Edits the priority sec page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView editPrioritySecPage(String ticketId, ModelMap model);

	/**
	 * Edits the backup page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView editBackupPage(String ticketId, ModelMap model);

	/**
	 * Edits the resolution sec page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView editResolutionSecPage(String ticketId, ModelMap model);

	/**
	 * Edits the priority section.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param request
	 *            the request
	 * @return the string
	 */
	String editPrioritySection(HDTicketDTO hdTicketDTO,
			HttpServletRequest request);

	/**
	 * Edits the resolution details.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param request
	 *            the request
	 * @return the string
	 */
	String editResolutionDetails(HDTicketDTO hdTicketDTO,
			HttpServletRequest request);

	/**
	 * State tail.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView stateTail(String ticketId, HttpServletRequest request);

	/**
	 * Update resolve ticket.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the string
	 */
	String updateResolveTicket(HDTicketDTO hdTicketDTO, ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets the resolve ticket.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the resolve ticket
	 */
	ModelAndView getResolveTicket(Long ticketId, ModelMap model,
			HttpServletRequest request);

	/**
	 * Enter new site.
	 * 
	 * @param msaId
	 *            the msa id
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView enterNewSite(String msaId, ModelMap model);

	/**
	 * Adds the new site.
	 * 
	 * @param siteDetailDTO
	 *            the site detail dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the string
	 */
	String addNewSite(SiteDetailDTO siteDetailDTO, ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets the sites list.
	 * 
	 * @param msaId
	 *            the msa id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the sites list
	 */
	String getSitesList(String msaId, ModelMap model, HttpServletRequest request);

	/**
	 * Gets the work notes page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param map
	 *            the map
	 * @return the work notes page
	 */
	ModelAndView getWorkNotesPage(String ticketId, ModelMap map);

	/**
	 * Gets the efforts save page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param map
	 *            the map
	 * @return the efforts save page
	 */
	ModelAndView getEffortsSavePage(String ticketId, ModelMap map);

	/**
	 * Gets the next action save page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param nextActionId
	 *            the next action id
	 * @param map
	 *            the map
	 * @return the next action save page
	 */
	ModelAndView getNextActionSavePage(String ticketId, String nextActionId,
			ModelMap map);

	/**
	 * Gets the cancel ticket page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param map
	 *            the map
	 * @return the cancel ticket page
	 */
	ModelAndView getCancelTicketPage(String ticketId, ModelMap map);

	/**
	 * Cancel ticket.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param workNotes
	 *            the work notes
	 * @param request
	 *            the request
	 * @return the string
	 */
	String cancelTicket(String ticketId, String workNotes,
			HttpServletRequest request);

	/**
	 * Gets the no action page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param map
	 *            the map
	 * @return the no action page
	 */
	ModelAndView getNoActionPage(String ticketId, ModelMap map);

	/**
	 * Save no action.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param request
	 *            the request
	 * @return the string
	 */
	String saveNoAction(String ticketId, HttpServletRequest request);

	/**
	 * Gets the no actions days page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param map
	 *            the map
	 * @return the no actions days page
	 */
	ModelAndView getNoActionsDaysPage(String ticketId, ModelMap map);

	/**
	 * Gets the edits the site page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param siteId
	 *            the site id
	 * @param model
	 *            the model
	 * @return the edits the site page
	 */
	ModelAndView getEditSitePage(String ticketId, String siteId, ModelMap model);

	/**
	 * Update site section.
	 * 
	 * @param siteDetailDTO
	 *            the site detail dto
	 * @param request
	 *            the request
	 * @return the string
	 */
	String updateSiteSection(SiteDetailDTO siteDetailDTO,
			HttpServletRequest request);

	/**
	 * Edits the backup details.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param request
	 *            the request
	 * @return the string
	 */
	String editBackupDetails(HDTicketDTO hdTicketDTO, HttpServletRequest request);

	/**
	 * Edits the ticket state page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param ticketStatus
	 *            the ticket status
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView editTicketStatePage(String ticketId, String ticketStatus,
			ModelMap model, HttpServletRequest request);

	/**
	 * Update ticket state.
	 * 
	 * @param hdTicketDTO
	 *            the hd ticket dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the string
	 */
	String updateTicketState(HDTicketDTO hdTicketDTO, ModelMap model,
			HttpServletRequest request);

	/**
	 * No action all.
	 * 
	 * @param request
	 *            the request
	 * @return the string
	 */
	String noActionAll(HttpServletRequest request);

	/**
	 * Gets the last update event detail.
	 * 
	 * @param request
	 *            the request
	 * @return the last update event detail
	 */
	String getLastUpdateEventDetail(HttpServletRequest request);

	/**
	 * Gets the work notes list.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the work notes list
	 */
	String getWorkNotesList(Long ticketId, ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets the work notes page.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the work notes page
	 */
	ModelAndView getWorkNotesPage(Long ticketId, ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets the cancel all form.
	 * 
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the cancel all form
	 */
	ModelAndView getCancelAllForm(ModelMap model, HttpServletRequest request);

	/**
	 * Display outage list.
	 * 
	 * @param hdCancelAllDTO
	 *            the hd cancel all dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the string
	 */
	String displayOutageList(HdCancelAllDTO hdCancelAllDTO, ModelMap model,
			HttpServletRequest request);

	/**
	 * Refresh outage list.
	 * 
	 * @param hdCancelAllDTO
	 *            the hd cancel all dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the string
	 */
	String refreshOutageList(HdCancelAllDTO hdCancelAllDTO, ModelMap model,
			HttpServletRequest request);

	/**
	 * Cancel all outages.
	 * 
	 * @param hdCancelAllDTO
	 *            the hd cancel all dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the string
	 */
	String cancelAllOutages(HdCancelAllDTO hdCancelAllDTO, ModelMap model,
			HttpServletRequest request);

	/**
	 * Save work notes.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param workNotes
	 *            the work notes
	 * @param request
	 *            the request
	 * @return the string
	 */
	String saveWorkNotes(String ticketId, String workNotes,
			HttpServletRequest request);

	/**
	 * Check status.
	 * 
	 * @param ackCubeType
	 *            the ack cube type
	 * @param ticketId
	 *            the ticket id
	 * @param request
	 *            the request
	 * @return the string
	 */
	String checkStatus(String ackCubeType, String ticketId,
			HttpServletRequest request);

	/**
	 * Clear review status.
	 * 
	 * @param ticketId
	 *            the ticket id
	 */
	void clearReviewStatus(String ticketId);

	/**
	 * Gets the effort details.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the effort details
	 */
	String getEffortDetails(Long ticketId);

	ModelAndView reOpenHdTicket(Long ticketId, ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets the ticket summary resolved.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param model
	 *            the model
	 * @return the ticket summary resolved
	 */
	ModelAndView getTicketSummaryResolved(Long ticketId, ModelMap model);

	/**
	 * Update hd ticket.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the string
	 */
	ModelAndView closeHDTicket(Long ticketId, ModelMap model,
			HttpServletRequest request);

	String checkLockStatus(String ticketId, HttpServletRequest request);

	String clearLockStatus(String ticketId, HttpServletRequest request);

	String getHDMonitoringDateTimeTableData(ModelMap model,
			HttpServletRequest request);

	String getDateTimeListForMonitoring(Long ticketId, ModelMap model,
			HttpServletRequest request);

	String effortTimerStatus(ModelMap model, HttpServletRequest request);

}
