package com.ilex.ws.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.core.dto.IncidentDTO;

/**
 * The Interface ServiceNowIntegrationController.
 */
public interface ServiceNowIntegrationController {

	/**
	 * Creates the proactive hd ticket.
	 * 
	 * @param proactiveIncidentDTO
	 *            the proactive incident dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the string
	 */
	String createHDTicket(IncidentDTO proactiveIncidentDTO, ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets the proactive incident creation page.
	 * 
	 * @param msaId
	 *            the msa id
	 * @param appendixId
	 *            the appendix id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the proactive incident creation page
	 */
	ModelAndView getProactiveIncidentCreationPage(String msaId,
			String appendixId, ModelMap model, HttpServletRequest request);

	/**
	 * Gets the recent incidents both reactive and proactive.
	 * 
	 * @param msaId
	 *            the msa id
	 * @param appendixId
	 *            the appendix id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the recent incidents
	 */
	ModelAndView getRecentIncidents(String msaId, String appendixId,
			ModelMap model, HttpServletRequest request);

	/**
	 * Update incident.
	 * 
	 * @param incidentDTO
	 *            the incident dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the string
	 */
	String updateIncident(IncidentDTO incidentDTO, ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets the error logs.
	 * 
	 * @param msaId
	 *            the msa id
	 * @param appendixId
	 *            the appendix id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the error logs
	 */
	ModelAndView getErrorLogs(String msaId, String appendixId, ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets the incident send update screen.
	 * 
	 * @param createIdentifier
	 *            the create identifier
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the incident send update screen
	 */
	ModelAndView getIncidentSendUpdateScreen(Long createIdentifier,
			ModelMap model, HttpServletRequest request);

	/**
	 * Gets the incident detail screen.
	 * 
	 * @param createIdentifier
	 *            the create identifier
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the incident detail screen
	 */
	ModelAndView getIncidentDetailScreen(Long createIdentifier, ModelMap model,
			HttpServletRequest request);

	/**
	 * Gets the sn message for pop up screen.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param mode
	 *            the mode
	 * @return the sn message
	 */

	String getSnMessage(Long ticketId);

	/**
	 * Clear integrated ticket status.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @return the string
	 */
	String clearIntegratedTicketStatus(Long ticketId);

}
