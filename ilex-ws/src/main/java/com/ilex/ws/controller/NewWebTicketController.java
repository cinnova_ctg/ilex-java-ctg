package com.ilex.ws.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.core.dto.NewDispatchDTO;

/**
 * The Interface NewWebTicketController.
 */
public interface NewWebTicketController {

	/**
	 * Gets the web ticket list.
	 * 
	 * @param model
	 *            the model
	 * @return the web ticket list
	 */
	ModelAndView getWebTicketList(ModelMap model);

	/**
	 * Gets the web ticket info.
	 * 
	 * @param newDispatchDTO
	 *            the new dispatch dto
	 * @param msaId
	 *            the msa id
	 * @param appendixId
	 *            the appendix id
	 * @param jobId
	 *            the job id
	 * @param ticketId
	 *            the ticket id
	 * @param model
	 *            the model
	 * @return the web ticket info
	 */
	ModelAndView getWebTicketInfo(NewDispatchDTO newDispatchDTO, Integer msaId,
			Integer appendixId, Integer jobId, Integer ticketId, ModelMap model);

	/**
	 * Save web ticket info.
	 * 
	 * @param newDispatchDTO
	 *            the new dispatch dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 */
	void saveWebTicketInfo(NewDispatchDTO newDispatchDTO, ModelMap model,
			HttpServletRequest request, HttpServletResponse response);

	/**
	 * Gets the resource list.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @param criticalityId
	 *            the criticality id
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the resource list
	 */
	String getResourceList(Long appendixId, String criticalityId,
			HttpServletRequest request, HttpServletResponse response);

}
