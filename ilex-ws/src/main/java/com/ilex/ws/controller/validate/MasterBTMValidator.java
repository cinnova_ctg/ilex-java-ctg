package com.ilex.ws.controller.validate;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindingResult;

import com.ilex.ws.core.dto.ClientListDTO;
import com.ilex.ws.core.dto.MasterBTMDTO;

public class MasterBTMValidator {

	public static void validateClient(MasterBTMDTO masterBTMDTO,
			BindingResult result) {
		if (masterBTMDTO.getClient() == null
				|| StringUtils.isEmpty(masterBTMDTO.getClient().getClientId())) {
			result.rejectValue("client.clientId", "master.btm.client.invalid");
		}

	}

	public static void validateDivision(MasterBTMDTO masterBTMDTO,
			BindingResult result) {
		if (masterBTMDTO.getDivision() == null
				|| StringUtils.isEmpty(masterBTMDTO.getDivision()
						.getDivisionId())) {
			result.rejectValue("division.divisionId",
					"master.btm.division.invalid");
		}

	}

	public static void validatePOC(MasterBTMDTO masterBTMDTO,
			BindingResult result) {
		if (masterBTMDTO.getPoc() == null
				|| StringUtils.isEmpty(masterBTMDTO.getPoc().getPocId())) {
			result.rejectValue("poc.pocId", "master.btm.poc.invalid");
		}

	}

	public static void validateDeliveryMethod(MasterBTMDTO masterBTMDTO,
			BindingResult result) {

	}

	public static void validateClientList(ClientListDTO clientListDTO,
			BindingResult result)

	{
		if (clientListDTO.getInvBTMClientDTOList() == null
				|| clientListDTO.getInvBTMClientDTOList().size() == 0) {
			result.rejectValue("clientListDTO.invBTMClientDTOList",
					"e.record.not.found");

		}
	}

}
