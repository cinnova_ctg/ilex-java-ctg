package com.ilex.ws.controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.core.dto.SiteSearchDTO;

/**
 * The Interface SiteSearchController.
 */
public interface SiteSearchController {

	/**
	 * Search site.
	 * 
	 * @param siteSearchDTO
	 *            the site search dto
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView searchSite(SiteSearchDTO siteSearchDTO, ModelMap model);

	/**
	 * Search result.
	 * 
	 * @param siteSearchDTO
	 *            the site search dto
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView searchResult(SiteSearchDTO siteSearchDTO, ModelMap model);

}
