package com.ilex.ws.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.core.dto.AppendixInvSetupDTO;
import com.ilex.ws.core.dto.InvBillToAddressDTO;
import com.ilex.ws.core.dto.InvSetupDTO;

public interface InvSearchController {
	/**
	 * Search client list get.
	 * 
	 * @param clientName
	 *            the client name
	 * @param pageIndex
	 *            the page index
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView searchClientListGet(String clientName, int pageIndex,
			ModelMap model, HttpServletRequest request);

	/**
	 * Search client list.
	 * 
	 * @param clientName
	 *            the client name
	 * @param pageIndex
	 *            the page index
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView searchClientList(String clientName, int pageIndex,
			ModelMap model, HttpServletRequest request);

	/**
	 * Edits the client profile.
	 * 
	 * @param clientId
	 *            the client id
	 * @param invBillToAddressDTO
	 *            the inv bill to address dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView editClientProfile(String clientId,
			InvBillToAddressDTO invBillToAddressDTO, BindingResult result,
			ModelMap model);

	/**
	 * Update client profile.
	 * 
	 * @param invBillToAddressDTO
	 *            the inv bill to address dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView updateClientProfile(InvBillToAddressDTO invBillToAddressDTO,
			BindingResult result, ModelMap model, HttpServletRequest request);

	/**
	 * Delete inv setup details.
	 * 
	 * @param invBillToAddressId
	 *            the inv bill to address id
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView deleteInvSetupDetails(String invBillToAddressId,
			ModelMap model, HttpServletRequest request);

	/**
	 * Edits the invoice profile.
	 * 
	 * @param invSetupId
	 *            the inv setup id
	 * @param invSetupDTO
	 *            the inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView editInvoiceProfile(String invSetupId, InvSetupDTO invSetupDTO,
			BindingResult result, ModelMap model);

	/**
	 * Edits the summary sheet.
	 * 
	 * @param invSetupId
	 *            the inv setup id
	 * @param invSetupDTO
	 *            the inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView editSummarySheet(String invSetupId, InvSetupDTO invSetupDTO,
			BindingResult result, ModelMap model);

	/**
	 * Edits the line item details.
	 * 
	 * @param invSetupId
	 *            the inv setup id
	 * @param invSetupDTO
	 *            the inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView editLineItemDetails(String invSetupId,
			InvSetupDTO invSetupDTO, BindingResult result, ModelMap model);

	/**
	 * Edits the client req inv info.
	 * 
	 * @param invSetupId
	 *            the inv setup id
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView editClientReqInvInfo(String invSetupId,
			AppendixInvSetupDTO appendixInvSetupDTO, BindingResult result,
			ModelMap model);

	/**
	 * Update invoice profile.
	 * 
	 * @param invSetupDTO
	 *            the inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView updateInvoiceProfile(InvSetupDTO invSetupDTO,
			BindingResult result, ModelMap model, HttpServletRequest request);

	/**
	 * Update summary sheet.
	 * 
	 * @param invSetupDTO
	 *            the inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView updateSummarySheet(InvSetupDTO invSetupDTO,
			BindingResult result, ModelMap model, HttpServletRequest request);

	/**
	 * Update line item details.
	 * 
	 * @param invSetupDTO
	 *            the inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView updateLineItemDetails(InvSetupDTO invSetupDTO,
			BindingResult result, ModelMap model, HttpServletRequest request);

	/**
	 * Update client req inv info.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView updateClientReqInvInfo(
			AppendixInvSetupDTO appendixInvSetupDTO, BindingResult result,
			ModelMap model, HttpServletRequest request);

}
