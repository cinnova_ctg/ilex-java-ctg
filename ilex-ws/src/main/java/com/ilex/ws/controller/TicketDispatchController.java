package com.ilex.ws.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.core.dto.TicketDispatchDTO;

/**
 * The Interface TicketDispatchController.
 */
public interface TicketDispatchController {

	/**
	 * Gets the ticket request page.
	 * 
	 * @param ticketDispatchDTO
	 *            the ticket dispatch dto
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the ticket request page
	 */
	ModelAndView getTicketRequestPage(TicketDispatchDTO ticketDispatchDTO,
			ModelMap model, HttpServletRequest request);

}
