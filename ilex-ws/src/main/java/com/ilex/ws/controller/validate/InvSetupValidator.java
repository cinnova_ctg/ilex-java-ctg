package com.ilex.ws.controller.validate;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindingResult;

import com.ilex.ws.core.dto.AppendixInvSetupDTO;

public class InvSetupValidator {

	public static void validateClient(AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result) {
		if (appendixInvSetupDTO.getClient() == null
				|| StringUtils.isEmpty(String.valueOf(appendixInvSetupDTO
						.getClient().getMbtClientId()))) {
			result.rejectValue("client.mbtClientId", "invoice.client.invalid");
		}

	}

	public static void validateAppendix(
			AppendixInvSetupDTO appendixInvSetupDTO, BindingResult result) {
		if (appendixInvSetupDTO.getAppendix() == null
				|| StringUtils.isEmpty(String.valueOf(appendixInvSetupDTO
						.getAppendix().getAppendixId()))) {
			result.rejectValue("client.mbtClientId", "invoice.client.invalid");
		}

	}

	public static void validateBillTOAddress(
			AppendixInvSetupDTO appendixInvSetupDTO, BindingResult result) {
		if (appendixInvSetupDTO.getBillToAddress() == null
				|| StringUtils.isEmpty(String.valueOf(appendixInvSetupDTO
						.getBillToAddress().getInvBillToAddressId()))) {
			result.rejectValue("client.mbtClientId", "invoice.client.invalid");
		}

	}
}
