package com.ilex.ws.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.core.dto.AppendixInvSetupDTO;

/**
 * The Interface InvSetupController.
 */
public interface InvSetupController {

	/**
	 * Gets the client list.
	 * 
	 * @param model
	 *            the model
	 * @return the client list
	 */
	ModelAndView getClientList(ModelMap model);

	/**
	 * Gets the client list.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the client list
	 */
	ModelAndView getClientList(AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model);

	/**
	 * Gets the appendix list.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the appendix list
	 */
	ModelAndView getAppendixList(AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model);

	/**
	 * Gets the client profile.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the client profile
	 */
	ModelAndView getClientProfile(AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model);

	/**
	 * Gets the invoice profile.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the invoice profile
	 */
	ModelAndView getinvoiceProfile(AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model);

	/**
	 * Gets the client req inv info.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the client req inv info
	 */
	ModelAndView getClientReqInvInfo(AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model);

	/**
	 * Gets the line item details.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the line item details
	 */
	ModelAndView getLineItemDetails(AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model);

	/**
	 * Gets the summary sheet.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the summary sheet
	 */
	ModelAndView getSummarySheet(AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model);

	/**
	 * Back to client req inv info.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView backToClientReqInvInfo(
			AppendixInvSetupDTO appendixInvSetupDTO, BindingResult result,
			ModelMap model);

	/**
	 * Back to line line item details.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView backToLineLineItemDetails(
			AppendixInvSetupDTO appendixInvSetupDTO, BindingResult result,
			ModelMap model);

	/**
	 * Save appendix inv detail.
	 * 
	 * @param appendixInvSetupDTO
	 *            the appendix inv setup dto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView saveAppendixInvDetail(AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model, HttpServletRequest request);

	/**
	 * Gets the cust info parameter.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @param model
	 *            the model
	 * @return the cust info parameter
	 */
	ModelAndView getCustInfoParameter(String appendixId, ModelMap model);

	ModelAndView backToClientProfile(AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model);

	ModelAndView backTOInvoiceProfile(AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model);

}
