package com.ilex.ws.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.core.dto.MasterBTMDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface MasterBTMController.
 */
public interface MasterBTMController {

	/**
	 * Gets the client list. For Get request
	 * 
	 * @param map
	 *            the map
	 * @return the client list
	 */
	ModelAndView getClientList(ModelMap map);

	/**
	 * Gets the client list. Using Post request from select division page
	 * 
	 * @param masterBTMDTO
	 *            the master btmdto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the client list
	 */
	ModelAndView getClientList(@ModelAttribute MasterBTMDTO masterBTMDTO,
			BindingResult result, ModelMap model);

	/**
	 * Gets the division list.
	 * 
	 * @param masterBTMDTO
	 *            the master btmdto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the division list
	 */
	ModelAndView getDivisionList(@ModelAttribute MasterBTMDTO masterBTMDTO,
			BindingResult result, ModelMap model);

	/**
	 * Gets the contact list.
	 * 
	 * @param masterBTMDTO
	 *            the master btmdto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the contact list
	 */
	ModelAndView getContactList(@ModelAttribute MasterBTMDTO masterBTMDTO,
			BindingResult result, ModelMap model);

	/**
	 * Gets the delivery type.
	 * 
	 * @param masterBTMDTO
	 *            the master btmdto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @return the delivery type
	 */
	ModelAndView getDeliveryType(@ModelAttribute MasterBTMDTO masterBTMDTO,
			BindingResult result, ModelMap model);

	/**
	 * Final billing info.
	 * 
	 * @param masterBTMDTO
	 *            the master btmdto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView finalBillingInfo(@ModelAttribute MasterBTMDTO masterBTMDTO,
			BindingResult result, ModelMap model, HttpServletRequest request);

	/**
	 * Search client list.
	 * 
	 * @param clientName
	 *            the client name
	 * @param pageIndex
	 *            the page index
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView searchClientList(String clientName, int pageIndex,
			ModelMap model, HttpServletRequest request);

	/**
	 * Update btm.
	 * 
	 * @param mbtClientId
	 *            the mbt client id
	 * @param model
	 *            the model
	 * @return the model and view
	 */
	ModelAndView updateBTM(String mbtClientId, ModelMap model);

	/**
	 * Update btm client details.
	 * 
	 * @param masterBTMDTO
	 *            the master btmdto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView updateBTMClientDetails(
			@ModelAttribute MasterBTMDTO masterBTMDTO, BindingResult result,
			ModelMap model, HttpServletRequest request);

	/**
	 * Update btm contact details.
	 * 
	 * @param masterBTMDTO
	 *            the master btmdto
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView updateBTMContactDetails(MasterBTMDTO masterBTMDTO,
			BindingResult result, ModelMap model, HttpServletRequest request);

	/**
	 * Search client list get.
	 * 
	 * @param clientName
	 *            the client name
	 * @param pageIndex
	 *            the page index
	 * @param model
	 *            the model
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	ModelAndView searchClientListGet(String clientName, int pageIndex,
			ModelMap model, HttpServletRequest request);

}
