package com.ilex.ws.controller;

import javax.servlet.http.HttpServletRequest;

public interface HDActiveTicketController {

	String isTicketAvailableForDrop(Long ticketId, HttpServletRequest request);

	String saveDroppedTicketState(Long ticketId, String divId,
			HttpServletRequest request);

	String saveActiveTicket(Long ticketId, HttpServletRequest request);

	String closeActiveTicket(Long ticketId, HttpServletRequest request);

	/**
	 * Gets the ticket details.
	 * 
	 * @param ilexNumber
	 *            the ilex number
	 * @return the ticket details
	 */
	String getTicketDetails(String ilexNumber);

	/**
	 * Update next action.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param nextActionTime
	 *            the next action time
	 * @param nextActionUnit
	 *            the next action unit
	 * @param nextAction
	 *            the next action
	 * @param request
	 *            the request
	 * @return the string
	 */
	String updateNextAction(String ticketId, String nextActionTime,
			String nextActionUnit, String nextAction, HttpServletRequest request);

	/**
	 * Record efforts.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param effortValue
	 *            the effort value
	 * @param request
	 *            the request
	 * @return the string
	 */
	String recordEfforts(String ticketId, String effortValue,
			HttpServletRequest request);

	/**
	 * Update state of active ticket.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param state
	 *            the state
	 * @param request
	 *            the request
	 * @return the string
	 */
	String updateState(String ticketId, String state, HttpServletRequest request);

	String updateBackupStatus(String ticketId, String cktStatus, String userId);

	String getTicketDetailForDashboard(String ticketId);
}
