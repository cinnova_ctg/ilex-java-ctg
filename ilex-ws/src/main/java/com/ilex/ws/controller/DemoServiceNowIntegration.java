package com.ilex.ws.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.core.dto.ServiceNowTicketDTO;

public interface DemoServiceNowIntegration {

	ModelAndView getIncidentCreatePage(ServiceNowTicketDTO serviceNowTicketDTO,
			ModelMap model, HttpServletRequest request);

	String createServiceNowIncident(ServiceNowTicketDTO serviceNowTicketDTO,
			ModelMap model, HttpServletRequest request);

	String getIncidentTicketDetails();

	ModelAndView getServiceTickets(ServiceNowTicketDTO serviceNowTicketDTO,
			ModelMap model, HttpServletRequest request);

	String getServiceNowTicketDetails();

}
