package com.ilex.ws.controllerImpl;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ilex.ws.controller.HDActiveTicketController;
import com.ilex.ws.core.dto.HDTicketDTO;
import com.ilex.ws.core.dto.HDWIPTableDTO;
import com.ilex.ws.core.logic.HDActiveTicketLogic;

/**
 * The Class HDActiveTicketControllerImpl.
 */
@Component
@RequestMapping("/hdActiveTicket")
public class HDActiveTicketControllerImpl implements HDActiveTicketController {

	/** The Constant logger. */
	private static final Logger logger = Logger
			.getLogger(HDActiveTicketControllerImpl.class);

	/** The hd active ticket logic. */
	@Resource
	HDActiveTicketLogic hdActiveTicketLogic;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDActiveTicketController#isTicketAvailableForDrop
	 * (java.lang.Long, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/isTicketAvailableForDrop.html", method = RequestMethod.POST)
	public @ResponseBody
	String isTicketAvailableForDrop(@RequestParam("ticketId") Long ticketId,
			HttpServletRequest request) {

		HttpSession session = request.getSession();

		String userId = (String) session.getAttribute("userid");
		String sessionId = session.getId();
		String domainName = "";
		try {
			domainName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			logger.error(e.getMessage(), e);
		}

		return hdActiveTicketLogic.isTicketAvailableForDrop(ticketId, userId,
				sessionId, domainName);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDActiveTicketController#saveDroppedTicketState
	 * (java.lang.Long, java.lang.String, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/saveDroppedTicketState.html", method = RequestMethod.POST)
	public @ResponseBody
	String saveDroppedTicketState(@RequestParam("ticketId") Long ticketId,
			@RequestParam("divId") String divId, HttpServletRequest request) {

		HttpSession session = request.getSession();
		String sessionId = session.getId();
		hdActiveTicketLogic.saveDroppedTicketState(ticketId, divId, sessionId);
		return "success";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDActiveTicketController#saveActiveTicket(java
	 * .lang.Long, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/saveActiveTicket.html", method = RequestMethod.POST)
	public @ResponseBody
	String saveActiveTicket(@RequestParam("ticketId") Long ticketId,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		String sessionId = session.getId();
		hdActiveTicketLogic.saveActiveTicket(ticketId, sessionId);
		return "success";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDActiveTicketController#closeActiveTicket(java
	 * .lang.Long, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/closeActiveTicket.html", method = RequestMethod.POST)
	public @ResponseBody
	String closeActiveTicket(@RequestParam("ticketId") Long ticketId,
			HttpServletRequest request) {

		HttpSession session = request.getSession();

		String userId = (String) session.getAttribute("userid");

		boolean status = hdActiveTicketLogic
				.closeActiveTicket(ticketId, userId);
		if (status) {
			return "success";
		} else {
			return "Not Able to Close The ticket";
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDActiveTicketController#getTicketDetails(java
	 * .lang.String)
	 */
	@Override
	@RequestMapping(value = "getTicketDetails.html", method = RequestMethod.POST)
	public @ResponseBody
	String getTicketDetails(String ticketId) {

		HDWIPTableDTO ticketDetails = hdActiveTicketLogic
				.getTicketDetails(ticketId);
		JSONObject jsonObject = JSONObject.fromObject(ticketDetails);
		return jsonObject.toString();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDActiveTicketController#updateNextAction(java
	 * .lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "updateNextAction.html", method = RequestMethod.POST)
	public @ResponseBody
	String updateNextAction(String ticketId, String nextActionTime,
			String nextActionUnit, String nextAction, HttpServletRequest request) {

		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");

		HDWIPTableDTO hdwipTableDTO = hdActiveTicketLogic.updateNextAction(
				ticketId, nextActionTime, nextActionUnit, nextAction, userId);

		JSONObject jsonObject = new JSONObject();
		jsonObject = JSONObject.fromObject(hdwipTableDTO);

		return jsonObject.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDActiveTicketController#recordEfforts(java.lang
	 * .String, java.lang.String, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "recordEfforts.html", method = RequestMethod.POST)
	public @ResponseBody
	String recordEfforts(String ticketId, String effortValue,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");

		hdActiveTicketLogic.saveEfforts(ticketId, effortValue, userId);
		return "success";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDActiveTicketController#updateState(java.lang
	 * .String, java.lang.String, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "updateState.html", method = RequestMethod.POST)
	public @ResponseBody
	String updateState(String ticketId, String state, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");

		hdActiveTicketLogic.updateState(ticketId, state, userId);
		return "success";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDActiveTicketController#updateCircuitStatus(java
	 * .lang.String, java.lang.String, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "updateCircuitStatus.html", method = RequestMethod.POST)
	public @ResponseBody
	String updateBackupStatus(String ticketId, String cktStatus, String userId) {

		hdActiveTicketLogic.updateBackupStatus(ticketId, cktStatus, userId);
		return "success";
	}

	@Override
	@RequestMapping(value = "ticketDetailForDashboard.html", method = RequestMethod.POST)
	public @ResponseBody
	String getTicketDetailForDashboard(String ticketId) {
		HDTicketDTO hdTicketDTO = hdActiveTicketLogic
				.getTicketDetailForDashboard(ticketId);
		JSONObject jsonObject = JSONObject.fromObject(hdTicketDTO);
		return jsonObject.toString();

	}
}
