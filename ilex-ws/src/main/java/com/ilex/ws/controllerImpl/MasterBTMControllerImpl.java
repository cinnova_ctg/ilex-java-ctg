package com.ilex.ws.controllerImpl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.controller.MasterBTMController;
import com.ilex.ws.controller.validate.MasterBTMValidator;
import com.ilex.ws.core.dto.ClientListDTO;
import com.ilex.ws.core.dto.InvBTMClientDTO;
import com.ilex.ws.core.dto.MasterBTMDTO;
import com.ilex.ws.core.dto.MsaClientDTO;
import com.ilex.ws.core.dto.OrgDivisionDTO;
import com.ilex.ws.core.dto.PointOfContactDTO;
import com.ilex.ws.core.logic.DeliveryMethodLogic;
import com.ilex.ws.core.logic.InvBTMClientLogic;
import com.ilex.ws.core.logic.InvBTMContactLogic;
import com.ilex.ws.core.logic.InvBTMLogic;
import com.ilex.ws.core.logic.InvMasterCountryLogic;
import com.ilex.ws.core.logic.InvMasterStateLogic;
import com.ilex.ws.core.logic.MsaClientLogic;
import com.ilex.ws.core.logic.OrgDivisionLogic;
import com.ilex.ws.core.logic.PointOfContactLogic;
import com.ilex.ws.core.util.MessageKeyConstant;
import com.ilex.ws.util.MessageUtils;

@Controller
@RequestMapping(value = "/masterbtm")
public class MasterBTMControllerImpl extends AbstractIlexBaseController
		implements MasterBTMController {

	/** The master btm logic. */

	@Resource
	MsaClientLogic msaClientLogic;
	@Resource
	OrgDivisionLogic orgDivisionLogic;
	@Resource
	PointOfContactLogic pointOfContactLogic;
	@Resource
	DeliveryMethodLogic deliveryMethodLogic;

	@Resource
	MessageUtils messageUtils;

	@Resource
	InvBTMLogic invBTMLogic;
	@Resource
	InvBTMContactLogic invBTMContactLogic;
	@Resource
	InvBTMClientLogic invBTMClientLogic;
	@Resource
	InvMasterStateLogic invMasterStateLogic;
	@Resource
	InvMasterCountryLogic invMasterCountryLogic;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.MasterBTMController#getClientList(org.springframework
	 * .ui.ModelMap)
	 */
	@RequestMapping(value = "/selectclient.html", method = RequestMethod.GET)
	@Override
	public ModelAndView getClientList(ModelMap model) {

		initExceptionHandler("error", "command", model);
		List<MsaClientDTO> clientList = msaClientLogic.getClientList();
		model.addAttribute("clientList", clientList);
		model.addAttribute("masterBTMDTO", new MasterBTMDTO());
		return new ModelAndView("IM/MBT/selectClient", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.MasterBTMController#getClientList(com.ilex.ws.
	 * core.dto.MasterBTMDTO, org.springframework.ui.ModelMap,
	 * org.springframework.validation.BindingResult)
	 */
	@Override
	@RequestMapping(value = "/selectclient.html", method = RequestMethod.POST)
	public ModelAndView getClientList(
			@ModelAttribute("masterBTMDTO") MasterBTMDTO masterBTMDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("IM/MBT/selectClient", "command", model);
		List<MsaClientDTO> clientList = msaClientLogic.getClientList();
		model.addAttribute("clientList", clientList);
		model.addAttribute("masterBTMDTO", masterBTMDTO);

		return new ModelAndView("IM/MBT/selectClient", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.MasterBTMController#getDivisionList(com.ilex.ws
	 * .core.dto.MasterBTMDTO, org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/division.html", method = RequestMethod.POST)
	@Override
	public ModelAndView getDivisionList(
			@ModelAttribute("masterBTMDTO") MasterBTMDTO masterBTMDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("IM/MBT/selectClient", "command", model);
		if (!result.hasErrors()) {
			MasterBTMValidator.validateClient(masterBTMDTO, result);
			if (result.hasErrors()) {
				return getClientList(masterBTMDTO, result, model);
			}
		}
		List<OrgDivisionDTO> divisionList = orgDivisionLogic
				.getDivisions(masterBTMDTO.getClient());
		masterBTMDTO.setClient(msaClientLogic.getClientDetail(masterBTMDTO
				.getClient()));
		model.addAttribute("divisionList", divisionList);
		model.addAttribute("masterBTMDTO", masterBTMDTO);
		return new ModelAndView("IM/MBT/selectDivision", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.MasterBTMController#getContactList(com.ilex.ws
	 * .core.dto.MasterBTMDTO, org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/selectContact.html", method = RequestMethod.POST)
	@Override
	public ModelAndView getContactList(
			@ModelAttribute("masterBTMDTO") MasterBTMDTO masterBTMDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("IM/MBT/selectDivision", "command", model);
		if (!result.hasErrors()) {
			MasterBTMValidator.validateClient(masterBTMDTO, result);
			MasterBTMValidator.validateDivision(masterBTMDTO, result);
			if (result.hasErrors()) {
				return getDivisionList(masterBTMDTO, result, model);
			}
		}
		List<PointOfContactDTO> pocList = pointOfContactLogic
				.getContactList(masterBTMDTO.getDivision());
		masterBTMDTO.setClient(msaClientLogic.getClientDetail(masterBTMDTO
				.getClient()));
		masterBTMDTO.setDivision(orgDivisionLogic
				.getDivisionDetail(masterBTMDTO.getDivision()));
		model.addAttribute("pocList", pocList);
		model.addAttribute("masterBTMDTO", masterBTMDTO);
		return new ModelAndView("IM/MBT/selectContact", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.MasterBTMController#getDeliveryType(com.ilex.ws
	 * .core.dto.MasterBTMDTO, org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/deliverymethod.html", method = RequestMethod.POST)
	@Override
	public ModelAndView getDeliveryType(
			@ModelAttribute("masterBTMDTO") MasterBTMDTO masterBTMDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("IM/MBT/selectContact", "command", model);
		if (!result.hasErrors()) {
			MasterBTMValidator.validateClient(masterBTMDTO, result);
			MasterBTMValidator.validateDivision(masterBTMDTO, result);
			MasterBTMValidator.validatePOC(masterBTMDTO, result);

			if (result.hasErrors()) {
				return getContactList(masterBTMDTO, result, model);
			}
		}
		masterBTMDTO.setClient(msaClientLogic.getClientDetail(masterBTMDTO
				.getClient()));
		masterBTMDTO.setDivision(orgDivisionLogic
				.getDivisionDetail(masterBTMDTO.getDivision()));
		if (!masterBTMDTO.getPoc().getPocId().equals("0")) {
			masterBTMDTO.setPoc(pointOfContactLogic
					.getContactDetail(masterBTMDTO.getPoc()));
		}
		model.addAttribute("masterBTMDTO", masterBTMDTO);
		return new ModelAndView("IM/MBT/selectDelivery", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.MasterBTMController#finalBillingInfo(com.ilex.
	 * ws.core.dto.MasterBTMDTO, org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap, javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "/finalbillinginfo.html", method = RequestMethod.POST)
	@Override
	public ModelAndView finalBillingInfo(MasterBTMDTO masterBTMDTO,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		initExceptionHandler("error", "command", model,
				MessageKeyConstant.MBT_RECORD_UNSUCCESS);
		if (!result.hasErrors()) {
			MasterBTMValidator.validateClient(masterBTMDTO, result);
			MasterBTMValidator.validateDivision(masterBTMDTO, result);
			MasterBTMValidator.validatePOC(masterBTMDTO, result);
			if (result.hasErrors()) {
				return getDeliveryType(masterBTMDTO, result, model);
			}
		}
		masterBTMDTO.setClient(msaClientLogic.getClientDetail(masterBTMDTO
				.getClient()));
		masterBTMDTO.setDivision(orgDivisionLogic
				.getDivisionDetail(masterBTMDTO.getDivision()));
		if (!masterBTMDTO.getPoc().getPocId().equals("0")) {
			masterBTMDTO.setPoc(pointOfContactLogic
					.getContactDetail(masterBTMDTO.getPoc()));
		}
		String deliveryMethodName = deliveryMethodLogic
				.getDeliveryMethod(masterBTMDTO.getDeliveryMethod());

		invBTMLogic.insertBTMRecord(masterBTMDTO);
		messageUtils.handleSuccess(request, model,
				MessageKeyConstant.MBT_RECORD_SUCCESS);
		model.addAttribute("deliveryMethodName", deliveryMethodName);
		model.addAttribute("masterBTMDTO", masterBTMDTO);
		return new ModelAndView("IM/MBT/finalBillInfo", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.MasterBTMController#searchClientList(java.lang
	 * .String, int, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "/searchclientlist.html", method = RequestMethod.POST)
	@Override
	public ModelAndView searchClientList(
			@RequestParam(value = "clientName") String clientName,
			@RequestParam(value = "pageIndex") int pageIndex, ModelMap model,
			HttpServletRequest request) {
		initExceptionHandler("error", "command", model,
				MessageKeyConstant.RECORD_NOT_FOUND);
		ClientListDTO clientListDTO = new ClientListDTO();
		clientListDTO = invBTMClientLogic.getClientListByName(
				clientName.trim(), pageIndex);

		clientListDTO.setClientSearchKeyword(clientName);
		model.addAttribute("clientListDTO", clientListDTO);
		return new ModelAndView("IM/MBT/searchResult", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.MasterBTMController#searchClientListGet(java.lang
	 * .String, int, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "/searchclientlist.html", method = RequestMethod.GET)
	@Override
	public ModelAndView searchClientListGet(
			@RequestParam(value = "clientName") String clientName,
			@RequestParam(value = "pageIndex") int pageIndex, ModelMap model,
			HttpServletRequest request) {
		initExceptionHandler("error", "command", model,
				MessageKeyConstant.RECORD_NOT_FOUND);
		ClientListDTO clientListDTO = new ClientListDTO();
		clientListDTO = invBTMClientLogic.getClientListByName(
				clientName.trim(), pageIndex);
		clientListDTO.setClientSearchKeyword(clientName);
		model.addAttribute("clientListDTO", clientListDTO);
		return new ModelAndView("IM/MBT/searchResultPage", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.MasterBTMController#updateBTM(java.lang.String,
	 * org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/updatebtmdetails.html", method = RequestMethod.GET)
	@Override
	public ModelAndView updateBTM(
			@RequestParam(value = "mbtClientId") String mbtClientId,
			ModelMap model) {
		initExceptionHandler("error", "command", model);
		MasterBTMDTO masterBTMDTO = new MasterBTMDTO();
		InvBTMClientDTO invBTMClientDTO = new InvBTMClientDTO();
		invBTMClientDTO.setMbtClientId(Integer.valueOf(mbtClientId));
		invBTMClientDTO = invBTMClientLogic.getClientDetails(String
				.valueOf(invBTMClientDTO.getMbtClientId()));
		masterBTMDTO.setBtmClientDTO(invBTMClientDTO);
		masterBTMDTO.setInvBTMContactList(invBTMContactLogic
				.getBTMContactDetails(invBTMClientDTO));
		model.addAttribute("stateList", invMasterStateLogic.getStateList());
		model.addAttribute("countryList",
				invMasterCountryLogic.getCountryList());
		model.addAttribute("masterBTMDTO", masterBTMDTO);
		return new ModelAndView("IM/MBT/updateBTM", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.MasterBTMController#updateBTMClientDetails(com
	 * .ilex.ws.core.dto.MasterBTMDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap, javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "/updatebtmclientdetails.html", method = RequestMethod.POST)
	@Override
	public ModelAndView updateBTMClientDetails(
			@ModelAttribute("masterBTMDTO") MasterBTMDTO masterBTMDTO,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		initExceptionHandler("error", "command", model,
				MessageKeyConstant.CLIENT_UPDATE_UNSUCCESS);
		invBTMClientLogic
				.updateBTMClientDetails(masterBTMDTO.getBtmClientDTO());
		masterBTMDTO.setBtmClientDTO(invBTMClientLogic.getClientDetails(String
				.valueOf(masterBTMDTO.getBtmClientDTO().getMbtClientId())));

		masterBTMDTO.setInvBTMContactList(invBTMContactLogic
				.getBTMContactDetails(masterBTMDTO.getBtmClientDTO()));

		model.addAttribute("stateList", invMasterStateLogic.getStateList());
		model.addAttribute("countryList",
				invMasterCountryLogic.getCountryList());
		model.addAttribute("masterBTMDTO", masterBTMDTO);
		messageUtils.handleSuccess(request, model,
				MessageKeyConstant.CLIENT_UPDATE_SUCCESS);
		return new ModelAndView("IM/MBT/updateBTM", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.MasterBTMController#updateBTMContactDetails(com
	 * .ilex.ws.core.dto.MasterBTMDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap, javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "/updatebtmcontactdetails.html", method = RequestMethod.POST)
	@Override
	public ModelAndView updateBTMContactDetails(
			@ModelAttribute("masterBTMDTO") MasterBTMDTO masterBTMDTO,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		initExceptionHandler("error", "command", model,
				MessageKeyConstant.CONTACT_UPDATE_UNSUCCESS);
		invBTMContactLogic.updateBTMContactDetails(masterBTMDTO);
		masterBTMDTO.setBtmClientDTO(invBTMClientLogic.getClientDetails(String
				.valueOf(masterBTMDTO.getBtmClientDTO().getMbtClientId())));

		masterBTMDTO.setInvBTMContactList(invBTMContactLogic
				.getBTMContactDetails(masterBTMDTO.getBtmClientDTO()));

		model.addAttribute("stateList", invMasterStateLogic.getStateList());
		model.addAttribute("countryList",
				invMasterCountryLogic.getCountryList());
		model.addAttribute("masterBTMDTO", masterBTMDTO);
		messageUtils.handleSuccess(request, model,
				MessageKeyConstant.CONTACT_UPDATE_SUCCESS);
		return new ModelAndView("IM/MBT/updateBTM", model);
	}

}
