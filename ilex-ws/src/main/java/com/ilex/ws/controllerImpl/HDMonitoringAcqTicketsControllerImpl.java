package com.ilex.ws.controllerImpl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.controller.HDMonitoringAcqTicketsController;
import com.ilex.ws.core.dto.HdAquiredTicketDTO;
import com.ilex.ws.core.logic.HDMonitoringAcqTicketsLogic;

@Controller
@RequestMapping("/hdTicketMonitoring")
public class HDMonitoringAcqTicketsControllerImpl implements
        HDMonitoringAcqTicketsController {

    @Resource
    HDMonitoringAcqTicketsLogic hdMonitoringAcqTicketsLogic;

    @RequestMapping(value = "getAcquiredTickets.html", method = RequestMethod.GET)
    public ModelAndView getHdAquiredTickets(ModelMap model) {
        String hostName = "";
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
        }
        List<HdAquiredTicketDTO> tickets = hdMonitoringAcqTicketsLogic
                .getAcquiredTickets(hostName);
        model.addAttribute("acquiredTickets", tickets);
        return new ModelAndView("/hdGroupManage/hdAdminMonitoring", model);

    }

    @RequestMapping(value = "releaseLock.html", method = RequestMethod.POST)
    public @ResponseBody
    String releaseLock(@RequestParam("ticketId") Long ticketId) {
        hdMonitoringAcqTicketsLogic.releaseLock(ticketId);
        return "success";

    }

}
