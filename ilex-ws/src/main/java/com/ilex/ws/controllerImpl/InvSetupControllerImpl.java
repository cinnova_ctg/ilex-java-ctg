package com.ilex.ws.controllerImpl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.controller.InvSetupController;
import com.ilex.ws.controller.validate.InvSetupValidator;
import com.ilex.ws.core.dto.AppendixDetailDTO;
import com.ilex.ws.core.dto.AppendixInvSetupDTO;
import com.ilex.ws.core.dto.InvAdditionalInfoDTO;
import com.ilex.ws.core.dto.InvBTMClientDTO;
import com.ilex.ws.core.dto.InvBTMContactDTO;
import com.ilex.ws.core.dto.InvBillToAddressDTO;
import com.ilex.ws.core.dto.InvMasterLineItemDetailsDTO;
import com.ilex.ws.core.dto.InvSetupDTO;
import com.ilex.ws.core.logic.AppendixLogic;
import com.ilex.ws.core.logic.InvBTMClientLogic;
import com.ilex.ws.core.logic.InvBTMContactLogic;
import com.ilex.ws.core.logic.InvMasterCountryLogic;
import com.ilex.ws.core.logic.InvMasterStateLogic;
import com.ilex.ws.core.logic.InvSetupLogic;

/**
 * The Class InvSetupControllerImpl.
 */
@Controller
@RequestMapping(value = "/invoiceSetup")
public class InvSetupControllerImpl extends AbstractIlexBaseController
		implements InvSetupController {

	/** The inv btm client logic. */
	@Resource
	InvBTMClientLogic invBTMClientLogic;

	/** The appendix logic. */
	@Resource
	AppendixLogic appendixLogic;

	/** The inv btm contact logic. */
	@Resource
	InvBTMContactLogic invBTMContactLogic;

	/** The inv setup logic. */
	@Resource
	InvSetupLogic invSetupLogic;

	/** The inv master state logic. */
	@Resource
	InvMasterStateLogic invMasterStateLogic;

	/** The inv master country logic. */
	@Resource
	InvMasterCountryLogic invMasterCountryLogic;
	public static final int INV_BILL_TO_ADDRESS_ID_PRESENT = 0;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#getClientList(org.springframework
	 * .ui.ModelMap)
	 */
	@RequestMapping(value = "/client.html", method = RequestMethod.GET)
	@Override
	public ModelAndView getClientList(ModelMap model) {
		initExceptionHandler("error", "command", model);
		List<InvBTMClientDTO> clientList = invBTMClientLogic.getClientList();
		model.addAttribute("clientList", clientList);
		model.addAttribute("appendixInvSetupDTO", new AppendixInvSetupDTO());
		return new ModelAndView("IM/invSetup/selectClient", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#getClientList(com.ilex.ws.core
	 * .dto.AppendixInvSetupDTO, org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/client.html", method = RequestMethod.POST)
	public ModelAndView getClientList(
			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("IM/invSetup/selectClient", "command", model);
		List<InvBTMClientDTO> clientList = invBTMClientLogic.getClientList();
		model.addAttribute("clientList", clientList);

		return new ModelAndView("IM/invSetup/selectClient", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#getAppendixList(com.ilex.ws
	 * .core.dto.AppendixInvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/appendixList.html", method = RequestMethod.POST)
	public ModelAndView getAppendixList(
			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model) {
		List<InvBTMClientDTO> clientList = invBTMClientLogic.getClientList();
		model.addAttribute("clientList", clientList);
		model.addAttribute("appendixInvSetupDTO", appendixInvSetupDTO);
		initExceptionHandler("IM/invSetup/selectClient", model);
		InvSetupValidator.validateClient(appendixInvSetupDTO, result);
		if (result.hasErrors()) {
			return getClientList(appendixInvSetupDTO, result, model);
		}
		appendixInvSetupDTO.setClient(invBTMClientLogic.getClientDetails(String
				.valueOf(appendixInvSetupDTO.getClient().getMbtClientId())));
		List<AppendixDetailDTO> appendixList = appendixLogic
				.getAppendixList(appendixInvSetupDTO.getClient());
		model.addAttribute("appendixList", appendixList);
		model.addAttribute("appendixInvSetupDTO", appendixInvSetupDTO);
		return new ModelAndView("IM/invSetup/selectAppendix", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#getClientProfile(com.ilex.ws
	 * .core.dto.AppendixInvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/clientProfile.html", method = RequestMethod.POST)
	public ModelAndView getClientProfile(

			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("error", "command", model);
		InvSetupValidator.validateClient(appendixInvSetupDTO, result);
		InvSetupValidator.validateAppendix(appendixInvSetupDTO, result);
		if (result.hasErrors()) {
			return getAppendixList(appendixInvSetupDTO, result, model);
		}

		appendixInvSetupDTO.setClient(invBTMClientLogic.getClientDetails(String
				.valueOf(appendixInvSetupDTO.getClient().getMbtClientId())));
		List<InvBTMContactDTO> invBTMContactList = invBTMContactLogic
				.getBTMContactDetails(appendixInvSetupDTO.getClient());

		AppendixDetailDTO appendixDetailDTO = appendixLogic
				.getAppendixDetail(appendixInvSetupDTO.getAppendix()
						.getAppendixId());
		appendixInvSetupDTO.setAppendix(appendixDetailDTO);
		appendixInvSetupDTO.setContactDetailsList(invBTMContactList);
		model.addAttribute("stateList", invMasterStateLogic.getStateList());
		model.addAttribute("countryList",
				invMasterCountryLogic.getCountryList());

		return new ModelAndView("IM/invSetup/clientProfile", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#getClientProfile(com.ilex.ws
	 * .core.dto.AppendixInvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/backToClientProfile.html", method = RequestMethod.POST)
	public ModelAndView backToClientProfile(
			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("error", "command", model);
		InvSetupValidator.validateClient(appendixInvSetupDTO, result);
		InvSetupValidator.validateAppendix(appendixInvSetupDTO, result);
		if (result.hasErrors()) {
			return getinvoiceProfile(appendixInvSetupDTO, result, model);
		}

		InvBillToAddressDTO invBillToAddress = invSetupLogic
				.getBillToAddressDetails(appendixInvSetupDTO.getBillToAddress()
						.getInvBillToAddressId());
		BeanUtils.copyProperties(invBillToAddress,
				appendixInvSetupDTO.getClient());

		List<InvBTMContactDTO> invBTMContactList = invBTMContactLogic
				.getBTMContactDetails(appendixInvSetupDTO.getClient());
		if (invBillToAddress.getInvContactId() != null) {
			for (int i = 0; i < invBTMContactList.size(); i++) {
				if (invBTMContactList.get(i).getInvContactId()
						.equals(invBillToAddress.getInvContactId())) {
					BeanUtils.copyProperties(invBillToAddress,
							invBTMContactList.get(i));
					appendixInvSetupDTO.setSelectedClient(i);
					break;
				}

			}

		}

		AppendixDetailDTO appendixDetailDTO = appendixLogic
				.getAppendixDetail(appendixInvSetupDTO.getAppendix()
						.getAppendixId());
		appendixInvSetupDTO.setAppendix(appendixDetailDTO);
		appendixInvSetupDTO.setContactDetailsList(invBTMContactList);
		model.addAttribute("stateList", invMasterStateLogic.getStateList());
		model.addAttribute("countryList",
				invMasterCountryLogic.getCountryList());

		return new ModelAndView("IM/invSetup/clientProfile", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#getinvoiceProfile(com.ilex.
	 * ws.core.dto.AppendixInvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/invoiceProfile.html", method = RequestMethod.POST)
	public ModelAndView getinvoiceProfile(
			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("error", "command", model);
		InvSetupValidator.validateClient(appendixInvSetupDTO, result);
		InvSetupValidator.validateAppendix(appendixInvSetupDTO, result);
		if (result.hasErrors()) {
			return getClientProfile(appendixInvSetupDTO, result, model);
		}
		InvBillToAddressDTO invBillToAddressDTO = new InvBillToAddressDTO();
		if (appendixInvSetupDTO.getBillToAddress().getInvBillToAddressId() == INV_BILL_TO_ADDRESS_ID_PRESENT) {
			appendixInvSetupDTO.setInvSetupDetails(invSetupLogic
					.getInitialDetails(appendixInvSetupDTO));
			int invBillToAddressId = invSetupLogic
					.saveAppendixInvDetail(appendixInvSetupDTO);

			invBillToAddressDTO.setInvBillToAddressId(invBillToAddressId);

		} else {
			BeanUtils.copyProperties(appendixInvSetupDTO.getClient(),
					invBillToAddressDTO);
			invBillToAddressDTO.setInvBillToAddressId(appendixInvSetupDTO
					.getBillToAddress().getInvBillToAddressId());
			if (appendixInvSetupDTO.getSelectedClient() == null) {
				invBillToAddressDTO.setInvContactId(null);
			}

			if (appendixInvSetupDTO.getClient().getDeliveryMethod() == 0) {
				invBillToAddressDTO.setDeliveryExist(true);
			} else {
				invBillToAddressDTO.setDeliveryExist(false);
			}
			invSetupLogic.updateClientProfile(invBillToAddressDTO);
		}
		invSetupLogic.getInvSetupDetails(invBillToAddressDTO);
		appendixInvSetupDTO.setInvSetupDetails(invSetupLogic
				.getInvSetupDetails(invBillToAddressDTO));
		appendixInvSetupDTO.setBillToAddress(invBillToAddressDTO);
		return new ModelAndView("IM/invSetup/invoiceProfile", model);
	}

	@Override
	@RequestMapping(value = "/backTOInvoiceProfile.html", method = RequestMethod.POST)
	public ModelAndView backTOInvoiceProfile(
			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("error", "command", model);
		InvSetupValidator.validateClient(appendixInvSetupDTO, result);
		InvSetupValidator.validateAppendix(appendixInvSetupDTO, result);
		InvSetupValidator.validateBillTOAddress(appendixInvSetupDTO, result);
		if (result.hasErrors()) {
			return getClientReqInvInfo(appendixInvSetupDTO, result, model);
		}
		InvBillToAddressDTO invBillToAddressDTO = new InvBillToAddressDTO();

		invBillToAddressDTO.setInvBillToAddressId(appendixInvSetupDTO
				.getBillToAddress().getInvBillToAddressId());

		invSetupLogic.getInvSetupDetails(invBillToAddressDTO);
		appendixInvSetupDTO.setInvSetupDetails(invSetupLogic
				.getInvSetupDetails(invBillToAddressDTO));
		appendixInvSetupDTO.setBillToAddress(invBillToAddressDTO);
		return new ModelAndView("IM/invSetup/invoiceProfile", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#getClientReqInvInfo(com.ilex
	 * .ws.core.dto.AppendixInvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/clientReqInvInfo.html", method = RequestMethod.POST)
	public ModelAndView getClientReqInvInfo(
			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("IM/invSetup/invoiceProfile", "command", model);
		InvSetupValidator.validateClient(appendixInvSetupDTO, result);
		InvSetupValidator.validateAppendix(appendixInvSetupDTO, result);
		InvSetupValidator.validateBillTOAddress(appendixInvSetupDTO, result);
		if (result.hasErrors()) {
			return getinvoiceProfile(appendixInvSetupDTO, result, model);
		}
		invSetupLogic.updateInvoiceProfile(appendixInvSetupDTO
				.getInvSetupDetails());
		InvSetupDTO invSetup = invSetupLogic
				.getInvSetupDetails(appendixInvSetupDTO.getInvSetupDetails()
						.getInvoiceSetupId());
		BeanUtils.copyProperties(invSetup,
				appendixInvSetupDTO.getInvSetupDetails());

		return new ModelAndView("IM/invSetup/clientReqInvInfo", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#backToClientReqInvInfo(com.
	 * ilex.ws.core.dto.AppendixInvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/backToClientReqInvInfo.html", method = RequestMethod.POST)
	public ModelAndView backToClientReqInvInfo(
			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("IM/invSetup/invoiceProfile", "command", model);
		InvSetupValidator.validateClient(appendixInvSetupDTO, result);
		InvSetupValidator.validateAppendix(appendixInvSetupDTO, result);
		InvSetupValidator.validateBillTOAddress(appendixInvSetupDTO, result);
		if (result.hasErrors()) {
			return getClientReqInvInfo(appendixInvSetupDTO, result, model);
		}

		InvBillToAddressDTO invBillToAddressDTO = new InvBillToAddressDTO();

		invBillToAddressDTO.setInvBillToAddressId(appendixInvSetupDTO
				.getBillToAddress().getInvBillToAddressId());

		InvSetupDTO invSetupDTO = invSetupLogic
				.getInvSetupDetails(invBillToAddressDTO);
		appendixInvSetupDTO.setInvSetupDetails(invSetupLogic
				.getInvSetupDetails(invBillToAddressDTO));
		appendixInvSetupDTO.setBillToAddress(invBillToAddressDTO);
		List<InvAdditionalInfoDTO> additionalInformationList = invSetupLogic
				.getAdditionalInfo(invSetupDTO);
		appendixInvSetupDTO
				.setAdditionalInformationList(additionalInformationList);

		return new ModelAndView("IM/invSetup/clientReqInvInfo", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#getLineItemDetails(com.ilex
	 * .ws.core.dto.AppendixInvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/lineItemDetails.html", method = RequestMethod.POST)
	public ModelAndView getLineItemDetails(
			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model) {
		InvSetupValidator.validateClient(appendixInvSetupDTO, result);
		InvSetupValidator.validateAppendix(appendixInvSetupDTO, result);
		InvSetupValidator.validateBillTOAddress(appendixInvSetupDTO, result);
		if (result.hasErrors()) {
			return getLineItemDetails(appendixInvSetupDTO, result, model);
		}

		invSetupLogic.updateClientReqInfo(appendixInvSetupDTO);
		InvSetupDTO invSetup = invSetupLogic.getInvSetupDetails(Integer
				.valueOf(appendixInvSetupDTO.getInvSetupDetails()
						.getInvoiceSetupId()));
		BeanUtils.copyProperties(invSetup,
				appendixInvSetupDTO.getInvSetupDetails());
		AppendixDetailDTO appendixDetailDTO = appendixLogic
				.getAppendixDetail(invSetup.getAppendixId());
		List<InvMasterLineItemDetailsDTO> invMasterLineItemDetailsDTOList = invSetupLogic
				.getLineItemDetails(appendixDetailDTO);
		model.addAttribute("invMasterLineItemDetailsDTOList",
				invMasterLineItemDetailsDTOList);

		return new ModelAndView("IM/invSetup/lineItemDetails", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#backToLineLineItemDetails(com
	 * .ilex.ws.core.dto.AppendixInvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/backToLineItemDetails.html", method = RequestMethod.POST)
	public ModelAndView backToLineLineItemDetails(
			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("IM/invSetup/invoiceProfile", "command", model);
		InvSetupValidator.validateClient(appendixInvSetupDTO, result);
		InvSetupValidator.validateAppendix(appendixInvSetupDTO, result);
		InvSetupValidator.validateBillTOAddress(appendixInvSetupDTO, result);
		if (result.hasErrors()) {
			return getSummarySheet(appendixInvSetupDTO, result, model);
		}
		AppendixDetailDTO appendixDetailDTO = appendixLogic
				.getAppendixDetail(appendixInvSetupDTO.getAppendix()
						.getAppendixId());
		InvBillToAddressDTO invBillToAddressDTO = new InvBillToAddressDTO();

		invBillToAddressDTO.setInvBillToAddressId(appendixInvSetupDTO
				.getBillToAddress().getInvBillToAddressId());

		invSetupLogic.getInvSetupDetails(invBillToAddressDTO);
		appendixInvSetupDTO.setInvSetupDetails(invSetupLogic
				.getInvSetupDetails(invBillToAddressDTO));
		appendixInvSetupDTO.setBillToAddress(invBillToAddressDTO);

		List<InvMasterLineItemDetailsDTO> invMasterLineItemDetailsDTOList = invSetupLogic
				.getLineItemDetails(appendixDetailDTO);
		model.addAttribute("invMasterLineItemDetailsDTOList",
				invMasterLineItemDetailsDTOList);
		return new ModelAndView("IM/invSetup/lineItemDetails", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#getSummarySheet(com.ilex.ws
	 * .core.dto.AppendixInvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/summarySheet.html", method = RequestMethod.POST)
	public ModelAndView getSummarySheet(
			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("error", "command", model);
		InvSetupValidator.validateClient(appendixInvSetupDTO, result);
		InvSetupValidator.validateAppendix(appendixInvSetupDTO, result);
		InvSetupValidator.validateBillTOAddress(appendixInvSetupDTO, result);
		if (result.hasErrors()) {
			return getLineItemDetails(appendixInvSetupDTO, result, model);
		}
		invSetupLogic.updateLineItemDetails(appendixInvSetupDTO
				.getInvSetupDetails());
		InvSetupDTO invSetup = invSetupLogic
				.getInvSetupDetails(appendixInvSetupDTO.getInvSetupDetails()
						.getInvoiceSetupId());
		BeanUtils.copyProperties(invSetup,
				appendixInvSetupDTO.getInvSetupDetails());

		if (appendixInvSetupDTO.getInvSetupDetails().getInvoiceSetupType()
				.equals("Individual")) {
			appendixInvSetupDTO.getInvSetupDetails().setSummarySheet(
					"Not Applicable");
		} else {
			appendixInvSetupDTO.getInvSetupDetails().setSummarySheet(
					"List of Sites");
		}
		return new ModelAndView("IM/invSetup/summarySheet", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#saveAppendixInvDetail(com.ilex
	 * .ws.core.dto.AppendixInvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/saveAppendixInvDetail.html", method = RequestMethod.POST)
	public ModelAndView saveAppendixInvDetail(
			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		invSetupLogic.updateSummarySheetDetails(appendixInvSetupDTO
				.getInvSetupDetails());

		return getClientList(appendixInvSetupDTO, result, model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#getCustInfoParameter(java.lang
	 * .String, org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/getCustInfoParameter.html", method = RequestMethod.GET)
	public ModelAndView getCustInfoParameter(
			@RequestParam(value = "appendixId") String appendixId,
			ModelMap model) {
		initExceptionHandler("error", "command", model);

		model.addAttribute("custInfoParameterList",
				invSetupLogic.getCustInfoParameter(Integer.valueOf(appendixId)));
		return new ModelAndView("IM/invSetup/getCustInfoParameter", model);
	}
}
