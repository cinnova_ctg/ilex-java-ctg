package com.ilex.ws.controllerImpl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.controller.DashportJobActionController;
import com.ilex.ws.core.dto.IlexDateComboDTO;
import com.ilex.ws.core.dto.InvoiceJobDetailDTO;
import com.ilex.ws.core.dto.JobInformationDTO;
import com.ilex.ws.core.dto.LabelValue;
import com.ilex.ws.core.dto.PartnerDetailDTO;
import com.ilex.ws.core.dto.SchedulerDTO;
import com.ilex.ws.core.logic.DashportJobActionLogic;
import com.ilex.ws.util.IlexDateUtils;
import com.mind.bean.prm.ViewInstallationNotesBean;
import com.mind.bean.pvs.Partner_SearchBean;
import com.mind.dao.PRM.CustomerInfoBean;

/**
 * The Class DashportJobActionControllerImpl.
 */
@Controller
@RequestMapping(value = "/dashport")
public class DashportJobActionControllerImpl extends AbstractIlexBaseController
		implements DashportJobActionController {

	/** The dashport job action logic. */
	@Resource
	DashportJobActionLogic dashportJobActionLogic;
	/** The Constant logger. */
	public static final Logger logger = Logger
			.getLogger(DashportJobActionControllerImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#getJobInfo(java.lang
	 * .String, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "/getJobInfo.html", method = RequestMethod.GET)
	@Override
	public ModelAndView getJobInfo(@RequestParam(value = "jobId") String jobId,
			ModelMap model, HttpServletRequest request) {

		InvoiceJobDetailDTO formdetailArg = new InvoiceJobDetailDTO();
		InvoiceJobDetailDTO invoiceJobDetailForm = dashportJobActionLogic
				.getJobInfo(formdetailArg, jobId);
		model.addAttribute("InvoiceJobDetailForm", invoiceJobDetailForm);
		List<InvoiceJobDetailDTO> resList = dashportJobActionLogic
				.getInvoiceActResourceList(invoiceJobDetailForm.getTypeJob(),
						String.valueOf(jobId));

		request.setAttribute("resList", resList);
		List<InvoiceJobDetailDTO> scheduleList = dashportJobActionLogic
				.getScheduledDetail(String.valueOf(jobId));
		request.setAttribute("scheduleList", scheduleList);
		return new ModelAndView("dashport/jobInfo", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#enterInstallNotes(
	 * java.lang.String, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "/enterInstallNotes.html", method = RequestMethod.GET)
	@Override
	public ModelAndView enterInstallNotes(
			@RequestParam(value = "jobId") String jobId, ModelMap model,
			HttpServletRequest request) {
		JobInformationDTO jobInformationDTO = new JobInformationDTO();
		jobInformationDTO.setJobId(jobId);
		model.addAttribute("jobInformationDTO", jobInformationDTO);
		return new ModelAndView("dashport/InstallationNotes", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#addInstallNotes(com
	 * .ilex.ws.core.dto.JobInformationDTO, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@RequestMapping(value = "/addInstallNotes.html", method = RequestMethod.GET)
	@Override
	public void addInstallNotes(
			@ModelAttribute("jobInformationDTO") JobInformationDTO jobInformationDTO,
			ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		String success = String.valueOf(dashportJobActionLogic
				.addInstalltionNotes(jobInformationDTO.getJobId(),
						jobInformationDTO.getInstallationNotes(), userId));
		try {
			response.getOutputStream().write(success.getBytes());
		} catch (IOException e) {
			logger.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#getTaskReminder(java
	 * .lang.String, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "getTaskReminder.html", method = RequestMethod.GET)
	@Override
	public ModelAndView getTaskReminder(
			@RequestParam(value = "jobId") String jobId, ModelMap model,
			HttpServletRequest request) {
		SchedulerDTO schedulerDTO = new SchedulerDTO();
		schedulerDTO.setJobId(jobId);
		IlexDateComboDTO ilexDateComboDTO = IlexDateUtils.fillIlexDateCombo();
		model.put("ilexDateComboDTO", ilexDateComboDTO);
		model.addAttribute("schedulerDTO", schedulerDTO);
		return new ModelAndView("dashport/taskReminder", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#getSchedulePlannedDate
	 * (java.lang.String, com.ilex.ws.core.dto.SchedulerDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/getSchedulePlannedDate.html", method = RequestMethod.GET)
	@Override
	public ModelAndView getSchedulePlannedDate(
			@RequestParam(value = "jobId") String jobId,
			@ModelAttribute("schedulerDTO") SchedulerDTO schedulerDTO,
			BindingResult result, ModelMap model) {

		SchedulerDTO schedulerInfo = dashportJobActionLogic
				.setScheduleDates(schedulerDTO.getJobId());
		BeanUtils.copyProperties(schedulerInfo, schedulerDTO);
		schedulerDTO.setJobId(jobId);
		IlexDateComboDTO ilexDateComboDTO = IlexDateUtils.fillIlexDateCombo();
		model.put("ilexDateComboDTO", ilexDateComboDTO);
		return new ModelAndView("/dashport/scheduledPlannedDate", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#saveSchedulerInfo(
	 * com.ilex.ws.core.dto.SchedulerDTO, javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@RequestMapping(value = "/saveSchedulerInfo.html", method = RequestMethod.GET)
	@Override
	public void saveSchedulerInfo(
			@ModelAttribute("schedulerDTO") SchedulerDTO schedulerDTO,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();

		String userId = (String) session.getAttribute("userid");
		// String cubeLastRefreshTime = (String) request
		// .getAttribute("cubeLastRefreshTime");
		// String isValid = dashportJobActionLogic.isJobValidForChange(
		// schedulerDTO.getJobId(), cubeLastRefreshTime);
		// System.out.println(isValid);

		String status = dashportJobActionLogic.saveSchedulerInfo(schedulerDTO,
				userId);

		try {
			response.getOutputStream().write(status.toString().getBytes());
		} catch (IOException e) {
			logger.error(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#saveReSchedulerInfo
	 * (com.ilex.ws.core.dto.SchedulerDTO,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@RequestMapping(value = "/saveReSchedulerInfo.html", method = RequestMethod.GET)
	@Override
	public void saveReSchedulerInfo(
			@ModelAttribute("schedulerDTO") SchedulerDTO schedulerDTO,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		String cubeLastRefreshTime = (String) request
				.getAttribute("cubeLastRefreshTime");
		/*
		 * String isValid = dashportJobActionLogic.isJobValidForChange(
		 * schedulerDTO.getJobId(), cubeLastRefreshTime);
		 * System.out.println(isValid);
		 */
		String status = dashportJobActionLogic.saveSchedulerInfo(schedulerDTO,
				userId);
		dashportJobActionLogic.saveReScheduledInfo(schedulerDTO, userId);

		try {
			response.getOutputStream().write(status.toString().getBytes());
		} catch (IOException e) {
			logger.error(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#getReSchedulePlannedDate
	 * (java.lang.String, com.ilex.ws.core.dto.SchedulerDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/getReSchedulePlannedDate.html", method = RequestMethod.GET)
	@Override
	public ModelAndView getReSchedulePlannedDate(String jobId,
			SchedulerDTO schedulerDTO, BindingResult result, ModelMap model) {
		SchedulerDTO schedulerInfo = dashportJobActionLogic
				.setScheduleDates(schedulerDTO.getJobId());
		BeanUtils.copyProperties(schedulerInfo, schedulerDTO);
		schedulerDTO.setJobId(jobId);
		IlexDateComboDTO ilexDateComboDTO = IlexDateUtils.fillIlexDateCombo();
		model.put("ilexDateComboDTO", ilexDateComboDTO);
		return new ModelAndView("/dashport/reScheduledPlannedDate", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#getOnsiteDate(java
	 * .lang.String, com.ilex.ws.core.dto.SchedulerDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/getOnsiteDate.html", method = RequestMethod.GET)
	@Override
	public ModelAndView getOnsiteDate(
			@RequestParam(value = "jobId") String jobId,
			SchedulerDTO schedulerDTO, BindingResult result, ModelMap model) {
		SchedulerDTO schedulerInfo = dashportJobActionLogic
				.setScheduleDates(schedulerDTO.getJobId());
		BeanUtils.copyProperties(schedulerInfo, schedulerDTO);
		schedulerDTO.setJobId(jobId);
		IlexDateComboDTO ilexDateComboDTO = IlexDateUtils.fillIlexDateCombo();
		model.put("ilexDateComboDTO", ilexDateComboDTO);
		return new ModelAndView("/dashport/scheduledOnsiteDate", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#getOffsiteDate(java
	 * .lang.String, com.ilex.ws.core.dto.SchedulerDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/getOffsiteDate.html", method = RequestMethod.GET)
	@Override
	public ModelAndView getOffsiteDate(
			@RequestParam(value = "jobId") String jobId,
			SchedulerDTO schedulerDTO, BindingResult result, ModelMap model) {
		SchedulerDTO schedulerInfo = dashportJobActionLogic
				.setScheduleDates(schedulerDTO.getJobId());
		BeanUtils.copyProperties(schedulerInfo, schedulerDTO);
		schedulerDTO.setJobId(jobId);
		IlexDateComboDTO ilexDateComboDTO = IlexDateUtils.fillIlexDateCombo();
		model.put("ilexDateComboDTO", ilexDateComboDTO);
		return new ModelAndView("/dashport/scheduledOffsiteDate", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#getJobCategoryPriority
	 * (java.lang.String, com.ilex.ws.core.dto.JobInformationDTO,
	 * org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/getJobCategoryPriority.html", method = RequestMethod.GET)
	@Override
	public ModelAndView getJobCategoryPriority(
			@RequestParam(value = "jobId") String jobId,
			@ModelAttribute("jobInformationDTO") JobInformationDTO jobInformationDTO,
			ModelMap model) {
		JobInformationDTO jobInfo = dashportJobActionLogic
				.getJobCategoryPriority(jobId);
		BeanUtils.copyProperties(jobInfo, jobInformationDTO);
		model.addAttribute("jobInfo", jobInfo);
		return new ModelAndView("/dashport/jobCategoryPriority", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#updateJobCategoryPriority
	 * (com.ilex.ws.core.dto.JobInformationDTO, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@RequestMapping(value = "/updateJobCategoryPriority.html", method = RequestMethod.GET)
	@Override
	public void updateJobCategoryPriority(JobInformationDTO jobInformationDTO,
			ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		String status = dashportJobActionLogic.updateJobCategoryPriority(
				jobInformationDTO, userId);

		try {
			response.getOutputStream().write(status.toString().getBytes());
		} catch (IOException e) {
			logger.error(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#switchJobOwner(java
	 * .lang.String, java.lang.String, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "/switchJO.html", method = RequestMethod.GET)
	@Override
	public ModelAndView switchJobOwner(
			@RequestParam(value = "jobId") String jobId,
			@RequestParam(value = "appendixId") String appendixId,
			ModelMap model, HttpServletRequest request) {
		JobInformationDTO jobInformationDTO = new JobInformationDTO();
		jobInformationDTO.setJobId(jobId);
		jobInformationDTO.setAppendixId(appendixId);
		List<LabelValue> jobOwnerList = dashportJobActionLogic
				.getJOList(appendixId);
		model.addAttribute("jobOwnerList", jobOwnerList);
		model.addAttribute("jobInformationDTO", jobInformationDTO);
		return new ModelAndView("dashport/switchJO", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#updateJobOwner(com
	 * .ilex.ws.core.dto.JobInformationDTO, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@RequestMapping(value = "/updateJobOwner.html", method = RequestMethod.GET)
	@Override
	public void updateJobOwner(JobInformationDTO jobInformationDTO,
			ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
		int jobId = Integer.valueOf(jobInformationDTO.getJobId());
		int newJobOwnerId = Integer.valueOf(jobInformationDTO
				.getNewJobOwnerId());
		String status = dashportJobActionLogic.updateJobOwner(jobId,
				newJobOwnerId);
		try {
			response.getOutputStream().write(status.toString().getBytes());
		} catch (IOException e) {
			logger.error(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#enterCustomerField
	 * (java.lang.String, java.lang.String, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "/enterCustomerField.html", method = RequestMethod.GET)
	@Override
	public ModelAndView enterCustomerField(
			@RequestParam(value = "jobId") String jobId,
			@RequestParam(value = "appendixId") String appendixId,
			ModelMap model, HttpServletRequest request) {
		JobInformationDTO jobInformationDTO = new JobInformationDTO();
		jobInformationDTO.setJobId(jobId);
		jobInformationDTO.setAppendixId(appendixId);
		List<CustomerInfoBean> requiredDataList = dashportJobActionLogic
				.gerCustomerFieldData(jobId, appendixId);
		/* Check for Snapon */
		boolean isSnapon = dashportJobActionLogic.checkForSnapon(appendixId);
		model.addAttribute("isSnapon", isSnapon);
		model.addAttribute("requiredDataList", requiredDataList);
		model.addAttribute("jobInformationDTO", jobInformationDTO);
		return new ModelAndView("dashport/customerFields", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#updateCustomerField
	 * (java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * org.springframework.ui.ModelMap, javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@RequestMapping(value = "/updateCustomerField.html", method = RequestMethod.GET)
	@Override
	public void updateCustomerField(
			@RequestParam(value = "jobId") String jobId,
			@RequestParam(value = "appendixId") String appendixId,
			@RequestParam(value = "customerValues") String customerValues,
			@RequestParam(value = "appendixCustomerIds") String appendixCustomerIds,
			ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		if (StringUtils.isNotBlank(customerValues)) {
			try {
				customerValues = URLDecoder.decode(customerValues, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.error(e);
			}
		}
		String status = dashportJobActionLogic.updateCustomerFiledInfo(jobId,
				appendixCustomerIds, customerValues, userId);
		try {
			response.getOutputStream().write(status.toString().getBytes());
		} catch (IOException e) {
			logger.error(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#viewInstallationNotes
	 * (java.lang.String, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "/viewInstallationNotes.html", method = RequestMethod.GET)
	@Override
	public ModelAndView viewInstallationNotes(
			@RequestParam(value = "jobId") String jobId, ModelMap model,
			HttpServletRequest request) {
		List<ViewInstallationNotesBean> installtionNotesList = dashportJobActionLogic
				.getInstallationNotesList(jobId);
		model.addAttribute("installtionNotesList", installtionNotesList);
		return new ModelAndView("dashport/viewInstallationNotes", model);
	}

	@RequestMapping(value = "/getPartnersList.html", method = RequestMethod.GET)
	@Override
	public ModelAndView getPartnersList(
			@RequestParam(value = "zipCode") String zipCode, ModelMap model,
			HttpServletRequest request) {
		List<Partner_SearchBean> partnerList = dashportJobActionLogic
				.getpartnersSearchList(zipCode);
		model.addAttribute("partnerList", partnerList);
		return new ModelAndView("dashport/quickPartnerSearch", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.DashportJobActionController#getPartnerInfo(java
	 * .lang.String, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "/getPartnerInfo.html", method = RequestMethod.GET)
	@Override
	public ModelAndView getPartnerInfo(
			@RequestParam(value = "partnerId") String partnerId,
			ModelMap model, HttpServletRequest request) {
		PartnerDetailDTO partnerDetailDTO = new PartnerDetailDTO();
		partnerDetailDTO.setPartnerId(partnerId);
		String checkPartner = dashportJobActionLogic.getPartnerDetails(
				partnerDetailDTO, request.getRequestURL().toString());
		if (partnerId == null && request.getParameter("poId") != null) {
			request.setAttribute("showPartnerType", "showPartnertype");
		}
		if (!checkPartner.equals("partnerFound")) {
			request.setAttribute("found", "failed");
		}
		model.addAttribute("partnerDetailDTO", partnerDetailDTO);
		return new ModelAndView("dashport/partnerInfo", model);
	}
}