package com.ilex.ws.controllerImpl;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.apache.log4j.Logger;
import org.languagetool.JLanguageTool;
import org.languagetool.language.AmericanEnglish;
import org.languagetool.rules.RuleMatch;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.controller.HDMainController;
import com.ilex.ws.core.dto.AjaxResponseDTO;
import com.ilex.ws.core.dto.CircuitStatusDTO;
import com.ilex.ws.core.dto.CountryDTO;
import com.ilex.ws.core.dto.EventCorrelatorDTO;
import com.ilex.ws.core.dto.HDAuditTrailDTO;
import com.ilex.ws.core.dto.HDCategoryDTO;
import com.ilex.ws.core.dto.HDConfigurationItemDTO;
import com.ilex.ws.core.dto.HDCubeStatusDTO;
import com.ilex.ws.core.dto.HDCustomerDetailDTO;
import com.ilex.ws.core.dto.HDEffortDTO;
import com.ilex.ws.core.dto.HDMonitoringTimeDTO;
import com.ilex.ws.core.dto.HDNextActionDTO;
import com.ilex.ws.core.dto.HDTableParentDTO;
import com.ilex.ws.core.dto.HDTicketDTO;
import com.ilex.ws.core.dto.HDWIPTableDTO;
import com.ilex.ws.core.dto.HDWorkNoteDTO;
import com.ilex.ws.core.dto.HdCancelAllDTO;
import com.ilex.ws.core.dto.IncidentStateDTO;
import com.ilex.ws.core.dto.OutageDetailDTO;
import com.ilex.ws.core.dto.ResolutionsDTO;
import com.ilex.ws.core.dto.SiteDetailDTO;
import com.ilex.ws.core.dto.StateDTO;
import com.ilex.ws.core.dto.StateTailDTO;
import com.ilex.ws.core.dto.TicketStateDTO;
import com.ilex.ws.core.logic.GeneralUtilsLogic;
import com.ilex.ws.core.logic.HDActiveTicketLogic;
import com.ilex.ws.core.logic.HDMainLogic;
import com.mind.bean.msp.RealTimeStateInfo;

/**
 * The Class HDMainControllerImpl.
 */
@Controller
@RequestMapping("/hdMainTable")
public class HDMainControllerImpl extends AbstractIlexBaseController implements
		HDMainController {

	public static final Logger logger = Logger
			.getLogger(HDMainControllerImpl.class);

	/**
	 * The hd main logic.
	 */
	@Resource
	HDMainLogic hdMainLogic;
	@Resource
	GeneralUtilsLogic generalUtilsLogic;
	@Resource
	HDActiveTicketLogic hdActiveTicketLogic;

	/**
	 * The proxy server url.
	 */
	@Value("#{ilexProperties['spellCheck.proxy.server.url']}")
	private String proxyServerUrl;

	/**
	 * The proxy server port.
	 */
	@Value("#{ilexProperties['spellCheck.proxy.server.port']}")
	private Integer proxyServerPort;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getHDTablePage(org.springframework
	 * .ui.ModelMap, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "tableMainIndex.html", method = RequestMethod.GET)
	public ModelAndView getHDTablePage(ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		// if (userId == null || "".equals(userId)) {
		// return new ModelAndView("redirect:" + request.getScheme() + "://"
		// + request.getServerName() + ":" + request.getServerPort()
		// + "/Ilex/index", model);
		// }

		String sessionId = session.getId();
		List<HDWIPTableDTO> hdwipTableDTOList = hdMainLogic.getActiveTickets(
				userId, sessionId);
		JSONArray jsonArray = JSONArray.fromObject(hdwipTableDTOList);
		model.put("activeTicketsDTO", jsonArray.toString());

		List<HDNextActionDTO> nextActions = hdMainLogic.getNextActions();
		model.put("nextActions", nextActions);

		List<TicketStateDTO> ticketStateDTOList = hdMainLogic.getTicketStates();
		model.put("ticketStates", ticketStateDTOList);

		List<CircuitStatusDTO> circuitStatusDTOList = hdMainLogic
				.getCircuitStatus();
		model.put("circuitStatus", circuitStatusDTOList);

		HDCubeStatusDTO hdCubeStatusDTO = new HDCubeStatusDTO();
		hdCubeStatusDTO = hdMainLogic.getCubeStatus();

		model.put("cubeStatus", hdCubeStatusDTO);

		model.addAttribute("tier2TicketCountsAttrib",
				hdMainLogic.getPendingAckTier2count(userId));

		HDTableParentDTO tableParentDTO = hdMainLogic.getTablesHeader();
		model.addAttribute("tableParentDTO", tableParentDTO);
		return new ModelAndView("hdGroupManage/hdTableIndex", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getHDWIPTableData(org.springframework
	 * .ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "hdWipTable.html", method = RequestMethod.GET)
	public @ResponseBody
	String getHDWIPTableData(ModelMap model, HttpServletRequest request) {
		JSONObject jsonObject = new JSONObject();
		String siteNumber = "";
		if (request.getParameter("searchVal") != null) {
			siteNumber = request.getParameter("searchVal").toString();
		}

		List<HDWIPTableDTO> jsonRowData = hdMainLogic
				.getHDWIPTableData(siteNumber);
		jsonObject.put("aaData", jsonRowData);
		String data = jsonObject.toString();
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.controller.HDMainController#getHDResolvedTableData(org.
	 * springframework .ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "hdResolvedTable.html", method = RequestMethod.GET)
	public @ResponseBody
	String getHDResolvedTableData(ModelMap model, HttpServletRequest request) {
		JSONObject jsonObject = new JSONObject();
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		String siteNumber = "";
		if (request.getParameter("searchVal") != null) {
			siteNumber = request.getParameter("searchVal").toString();
		}

		List<HDWIPTableDTO> jsonRowData = hdMainLogic.getHDResolvedTableData(
				siteNumber, userId);
		jsonObject.put("aaData", jsonRowData);
		String data = jsonObject.toString();
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getHDWIPTableData(org.springframework
	 * .ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "hdWipOverDueTable.html", method = RequestMethod.GET)
	public @ResponseBody
	String getHDWIPOverDueTableData(ModelMap model, HttpServletRequest request) {
		JSONObject jsonObject = new JSONObject();
		String siteNumber = "";
		if (request.getParameter("searchVal") != null) {
			siteNumber = request.getParameter("searchVal").toString();
		}

		List<HDWIPTableDTO> jsonRowData = hdMainLogic
				.getHDWIPOverDueTableData(siteNumber);
		jsonObject.put("aaData", jsonRowData);
		String data = jsonObject.toString();
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getHDWIPNextActionTableData(org
	 * .springframework .ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "hdWipNextActionTable.html", method = RequestMethod.GET)
	public @ResponseBody
	String getHDWIPNextActionTableData(ModelMap model,
			HttpServletRequest request) {
		JSONObject jsonObject = new JSONObject();
		String siteNumber = "";
		if (request.getParameter("searchVal") != null) {
			siteNumber = request.getParameter("searchVal").toString();
		}

		List<HDWIPTableDTO> jsonRowData = hdMainLogic
				.getHDWIPNextActionTableData(siteNumber);
		jsonObject.put("aaData", jsonRowData);
		String data = jsonObject.toString();
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getHDPendingAckTableData(org.
	 * springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "hdPendingAckCust.html", method = RequestMethod.GET)
	public @ResponseBody
	String getHDPendingAckCustTableData(ModelMap model,
			HttpServletRequest request) {
		String siteNumber = "";
		if (request.getParameter("searchVal") != null) {
			siteNumber = request.getParameter("searchVal").toString();
		}

		String data = hdMainLogic.getHDPendingAckTableData(siteNumber);
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getHDPendingAckSecurityTableData
	 * (org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "hdPendingAckSecurity.html", method = RequestMethod.GET)
	public @ResponseBody
	String getHDPendingAckSecurityTableData(ModelMap model,
			HttpServletRequest request) {
		String siteNumber = "";
		if (request.getParameter("searchVal") != null) {
			siteNumber = request.getParameter("searchVal").toString();
		}

		String data = hdMainLogic.getHDPendingAckSecurityTableData(siteNumber);
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.controller.HDMainController#checkWords(java.lang.String)
	 */
	@Override
	@RequestMapping(value = "checkWords.html", method = RequestMethod.POST)
	public @ResponseBody
	String checkWords(String text) {
		JSONArray jsonArray = new JSONArray();
		List<RuleMatch> matches = null;
		JLanguageTool langTool = null;
		try {
			langTool = new JLanguageTool(new AmericanEnglish());
			langTool.activateDefaultPatternRules();
			matches = langTool.check(text);
		} catch (IOException e1) {
			logger.error("Error initializing spell checker" + e1);
		}
		for (RuleMatch match : matches) {
			if ("Spelling mistake".equalsIgnoreCase(match.getShortMessage())) {
				JSONArray jsonObject = new JSONArray();
				jsonObject.add(match.getFromPos());
				jsonObject.add(match.getToPos() - match.getFromPos());
				jsonArray.add(jsonObject);
			}
		}
		return jsonArray.toString();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#suggestWords(java.lang.String)
	 */
	@Override
	@RequestMapping(value = "checkSuggests.html", method = RequestMethod.POST)
	public @ResponseBody
	String suggestWords(String suggest) {
		List<RuleMatch> matches = null;
		JLanguageTool langTool = null;
		JSONArray jsonArray = new JSONArray();
		try {
			langTool = new JLanguageTool(new AmericanEnglish());
			langTool.activateDefaultPatternRules();
			matches = langTool.check(suggest);
		} catch (IOException e1) {
			logger.error("Error initializing spell checker" + e1);
		}
		for (RuleMatch match : matches) {
			jsonArray.addAll(match.getSuggestedReplacements());
		}
		return jsonArray.toString();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getEnlargeCubeView(java.lang.
	 * String, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	@RequestMapping(value = "enlargeCubeView.html", method = RequestMethod.GET)
	public ModelAndView getEnlargeCubeView(
			@RequestParam(value = "tableId") String tableId, ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {
		HDTableParentDTO tableParentDTO = hdMainLogic
				.getIndividualTableHeader(tableId);
		model.addAttribute("tableParentDTO", tableParentDTO);
		model.addAttribute("tableId", tableId);
		return new ModelAndView("hdGroupManage/enlargeCubeView", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getEnlargeCubeView(java.lang.
	 * String, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */

	@Override
	@RequestMapping(value = "EventCorrelatorAction.html", method = RequestMethod.GET)
	public ModelAndView setupEventCorrelator(String type, String customerInfo,
			String condition, String duration, String threshold, String color,
			String save, String id, ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {

		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");

		if ("Save".equals(save)) {
			EventCorrelatorDTO dto = new EventCorrelatorDTO();
			dto.setType(type);
			dto.setAddedBy(userId);
			dto.setUpdatedBy(userId);
			dto.setColor(color);
			dto.setCondition(condition);
			dto.setCustomerInfo(customerInfo);
			dto.setDuration(duration);
			dto.setThreshold(threshold);

			hdMainLogic.saveEventCorrelator(dto);

		} else if ("Delete".equals(save)) {
			hdMainLogic.deleteEventCorrelator(id);
			type = "Customer";
		}

		if ("Staff".equals(type)) {

		} else {
			Map<String, List<HDCustomerDetailDTO>> customers = hdMainLogic
					.getHDCustomers();
			model.put("customers", customers);
		}

		Map<String, String> pointOfFailureList = hdMainLogic
				.getPointOfFailureList();
		List<EventCorrelatorDTO> lst = hdMainLogic.getEventCorrelator(type);
		model.put("pointOfFailureList", pointOfFailureList);
		model.put("pointOfFailureId", condition);
		model.put("colorTitle", color);
		model.put("type", type);
		model.put("EventsList", lst);

		// model.put("typeChkbox", "");

		return new ModelAndView("hdGroupManage/eventCorrelatorView", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#generateExcelView(java.lang.String
	 * , javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	@RequestMapping(value = "generateExcelView.html", method = RequestMethod.GET)
	public ModelAndView generateExcelView(
			@RequestParam(value = "tableId") String tableId,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		String tableTickets = hdMainLogic.getExcelData(tableId, userId);
		Map<String, Object> cubeInfo = new HashMap<String, Object>();
		HDTableParentDTO tableParentDTO = hdMainLogic
				.getIndividualTableHeader(tableId);
		cubeInfo.put("tableParentDTO", tableParentDTO);
		cubeInfo.put("tableId", tableId);
		cubeInfo.put("tickets", tableTickets);
		return new ModelAndView("ExportCubeToExcel", "cubeInfo", cubeInfo);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#createTicketStep1(org.springframework
	 * .ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "createTicketStep1.html", method = RequestMethod.POST)
	public ModelAndView createTicketStep1(
			@ModelAttribute("hdTicketDTO") HDTicketDTO hdTicketDTO,
			@RequestParam("appendixId") Long appendixId,
			@RequestParam("impact") String impact,
			@RequestParam("urgency") String urgency, ModelMap model) {

		int priority = hdMainLogic.createTicketPriority(appendixId,
				Integer.valueOf(impact), Integer.valueOf(urgency));

		hdTicketDTO.setPriority(String.valueOf(priority));
		model.put("priority", String.valueOf(priority));
		return new ModelAndView("hdGroupManage/hdTicketCreateStep2", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#createTicketStep2(com.ilex.ws
	 * .core.dto.HDTicketDTO, java.lang.String, org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "createTicketStep2.html", method = RequestMethod.POST)
	public ModelAndView createTicketStep2(
			@ModelAttribute("hdTicketDTO") HDTicketDTO hdTicketDTO,
			@RequestParam("hdCustomer") String hdCustomer,
			@RequestParam("isFromMain") String isFromMain,
			@RequestParam("hdTicketNumber") String hdTicketNumber,
			ModelMap model) {

		Map<String, List<HDCustomerDetailDTO>> customers = hdMainLogic
				.getHDCustomers();
		model.put("customers", customers);

		if (hdCustomer.equals("")) {
			if (hdTicketDTO.getCustomerName() != null) {
				hdCustomer = hdTicketDTO.getCustomerName();
			} else {
				hdTicketDTO.setIsFromMain(isFromMain);
				hdTicketDTO.setTicketNumber(hdMainLogic
						.avaliableIlexTicketNumber());

				if (hdTicketDTO.getBackUpCktStatus() == null) {
					hdTicketDTO.setBackUpCktStatus("Not Applicable");
				}
				if (hdTicketDTO.getCreatedOnDateString() == null) {
					hdTicketDTO.setCreatedOnDateString("");
				}

				model.put("hdTicket", hdTicketDTO);
				return new ModelAndView("hdGroupManage/hdTicketCreateStep2",
						model);
			}
		}

		HDTicketDTO hdTicket = hdMainLogic.getHDTicketDetail(hdCustomer);

		List<SiteDetailDTO> sites = hdMainLogic.getSites(hdTicket.getMsaId());

		// List<LabelValue> incidentStatesList = hdMainLogic.getIncidentState();
		List<HDConfigurationItemDTO> configurationItems = hdMainLogic
				.getConfigurationItems();

		hdTicket.setTicketNumber(hdTicketNumber);
		hdTicket.setIsFromMain("yes");
		hdTicket.setBackUpCktStatus("Not Applicable");

		// get Clients Email
		String clientEmail = hdMainLogic.getClientEmailAddress(hdTicket
				.getAppendixId());
		hdTicket.setClientEmailAddress(clientEmail);
		List<HDCategoryDTO> categories = hdMainLogic.getCategories();
		List<HDNextActionDTO> nextActions = hdMainLogic.getNextActions();

		model.put("nextActions", nextActions);
		model.put("categories", categories);
		model.put("configurationItems", configurationItems);
		model.put("sites", sites);
		model.put("clientEmail", clientEmail);
		// model.put("incidentStates", incidentStatesList);
		model.put("hdTicket", hdTicket);

		hdTicket.setTicketStatus("New");
		BeanUtils.copyProperties(hdTicket, hdTicketDTO);

		return new ModelAndView("hdGroupManage/hdTicketCreateStep2", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#createHDTicket(com.ilex.ws.core
	 * .dto.HDTicketDTO, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "createHDTicket.html", method = RequestMethod.POST)
	public @ResponseBody
	String createHDTicket(
			@ModelAttribute("hdTicketDTO") HDTicketDTO hdTicketDTO,
			ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		hdMainLogic.createTicket(hdTicketDTO, userId);
		return "success";

	}

	@Override
	@RequestMapping(value = "updateHDTicket.html", method = RequestMethod.POST)
	public @ResponseBody
	String updateHDTicket(
			@ModelAttribute("hdTicketDTO") HDTicketDTO hdTicketDTO,
			ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		hdTicketDTO.setCallerName(hdTicketDTO.getContactName());
		hdTicketDTO.setCallerEmail(hdTicketDTO.getEmailAddress());
		hdTicketDTO.setCallerPhone(hdTicketDTO.getPhoneNumber());

		hdMainLogic.updateTicket(hdTicketDTO, userId);
		return "success";

	}

	@Override
	@RequestMapping(value = "hdAckTicketSummary.html", method = RequestMethod.GET)
	public ModelAndView getAckTicketSummary(
			@RequestParam("ticketId") Long ticketId, ModelMap model) {
		HDTicketDTO hdTicketDTO = hdMainLogic.getTicketSummary(ticketId);

		model.put("hdTicketDTO", hdTicketDTO);
		return new ModelAndView("hdGroupManage/hdTicketSummary", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getTicketSummaryResolved(java
	 * .lang.Long, org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "hdTicketSummaryResolved.html", method = RequestMethod.GET)
	public ModelAndView getTicketSummaryResolved(
			@RequestParam("ticketId") Long ticketId, ModelMap model) {

		String effortFlag = "";
		HDTicketDTO hdTicketDTO = hdMainLogic.getTicketSummary(ticketId);

		hdTicketDTO.setContactName(hdTicketDTO.getCallerName());
		hdTicketDTO.setEmailAddress(hdTicketDTO.getCallerEmail());
		hdTicketDTO.setPhoneNumber(hdTicketDTO.getCallerPhone());

		List<HDAuditTrailDTO> hdAuditTrailList = hdMainLogic
				.getAuditTrail(hdTicketDTO.getTicketNumber());
		List<HDNextActionDTO> nextActions = hdMainLogic.getNextActions();
		List<HDCategoryDTO> categories = hdMainLogic.getCategories();
		List<HDConfigurationItemDTO> configurationItems = hdMainLogic
				.getConfigurationItems();

		Map<String, List<HDCustomerDetailDTO>> customers = hdMainLogic
				.getHDCustomers();

		hdTicketDTO.setHdAuditTrailList(hdAuditTrailList);
		effortFlag = hdMainLogic.getEffortStatus(String.valueOf(ticketId));

		StateTailDTO stateTailDTO = hdMainLogic.getStateTail(String
				.valueOf(ticketId));

		List<ResolutionsDTO> resolutionsList = hdMainLogic.getResolutions();

		// List<SiteDetailDTO> sites = hdMainLogic
		// .getSites(hdTicketDTO.getMsaId());
		model.put("nextActions", nextActions);
		model.put("categories", categories);
		model.put("configurationItems", configurationItems);
		// model.put("auditTrail", hdAuditTrailList);
		// model.put("sites", sites);
		model.put("hdTicketDTO", hdTicketDTO);
		model.put("resolutions", resolutionsList);
		model.put("effortFlag", effortFlag);
		model.put("stateTailDTO", stateTailDTO);
		/*
		 * List<HDWorkNoteDTO> hdWorkNoteDTOs = new ArrayList<>();
		 * hdWorkNoteDTOs = hdMainLogic.getWorkNotesList(ticketId); model.put()
		 */

		return new ModelAndView("hdGroupManage/hdTicketResolved", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getTicketSummary(org.springframework
	 * .ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "hdTicketSummary.html", method = RequestMethod.GET)
	public ModelAndView getTicketSummary(
			@RequestParam("ticketId") Long ticketId, ModelMap model,
			HttpServletRequest request) {

		HttpSession session = request.getSession();

		ticketSummary(ticketId, model, "update", session);

		return new ModelAndView("hdGroupManage/hdTicketUpdate", model);

	}

	@Override
	@RequestMapping(value = "reOpenHdTicket.html", method = RequestMethod.GET)
	public ModelAndView reOpenHdTicket(@RequestParam("ticketId") Long ticketId,
			ModelMap model, HttpServletRequest request) {

		// HttpSession session = request.getSession();
		// String userId = (String) session.getAttribute("userid");
		// hdActiveTicketLogic.updateState(String.valueOf(ticketId), "12",
		// userId); // 12
		// // is
		// // for
		// // reopened.
		HttpSession session = request.getSession();
		ticketSummary(ticketId, model, "reopen", session);

		return new ModelAndView("hdGroupManage/hdTicketReOpen", model);

	}

	@Override
	@RequestMapping(value = "closeHDTicket.html", method = RequestMethod.GET)
	public ModelAndView closeHDTicket(@RequestParam("ticketId") Long ticketId,
			ModelMap model, HttpServletRequest request) {
		// HttpSession session = request.getSession();
		// String userId = (String) session.getAttribute("userid");
		// hdActiveTicketLogic.updateState(String.valueOf(ticketId), "11",
		// userId); // 11
		// // is
		// for
		HttpSession session = request.getSession();
		ticketSummary(ticketId, model, "close", session); // closed
		// ticket
		return new ModelAndView("hdGroupManage/hdTicketClose", model);

	}

	/**
	 * Ticket summary.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param model
	 *            the model
	 */
	private void ticketSummary(Long ticketId, ModelMap model, String jspType,
			HttpSession session) {
		String effortFlag = "";
		HDTicketDTO hdTicketDTO = hdMainLogic.getTicketSummary(ticketId);
		// get clients Email
		String clientEmail = hdMainLogic.getClientEmailAddress(hdTicketDTO
				.getAppendixId());
		if (clientEmail == null)
			clientEmail = "";
		hdTicketDTO.setClientEmailAddress(clientEmail);
		List<IncidentStateDTO> EmailAddressList = new ArrayList<>();
		EmailAddressList = hdMainLogic.getEmailAddress(hdTicketDTO
				.getAppendixId());

		// convert list
		String array[] = new String[EmailAddressList.size()];
		for (int j = 0; j < EmailAddressList.size(); j++) {
			array[j] = EmailAddressList.get(j).getValue().toString();
		}
		String emailids = "";
		for (String k : array) {
			// System.out.println(k);
			emailids = emailids + "," + k + ",";
		}
		if (emailids.contains(",,")) {
			emailids = emailids.replace(",,", ",");
		}
		hdTicketDTO.setContactName(hdTicketDTO.getCallerName());
		hdTicketDTO.setEmailAddress(hdTicketDTO.getCallerEmail() + emailids);
		// hdTicketDTO.setEmailAddress(hdTicketDTO.getEmailAddress());
		// hdTicketDTO.setEmailAddressAdditional(hdTicketDTO.getCallerEmail());
		hdTicketDTO.setPhoneNumber(hdTicketDTO.getCallerPhone());

		List<HDAuditTrailDTO> hdAuditTrailList = hdMainLogic
				.getAuditTrail(hdTicketDTO.getTicketNumber());
		List<HDNextActionDTO> nextActions = hdMainLogic.getNextActions();
		List<HDCategoryDTO> categories = hdMainLogic.getCategories();
		List<HDConfigurationItemDTO> configurationItems = hdMainLogic
				.getConfigurationItems();

		Map<String, List<HDCustomerDetailDTO>> customers = hdMainLogic
				.getHDCustomers();

		List<IncidentStateDTO> incidentStatesList = new ArrayList<>();

		if (jspType.contains("update")) {
			// Get the list of incident stateat are both active AND set to show
			// on the list.
			incidentStatesList = hdMainLogic.getIncidentState(false,
					hdTicketDTO.getTicketStatus());
		} else if (jspType.contains("reopen")) {
			IncidentStateDTO newDTO = new IncidentStateDTO();
			newDTO.setLabel("Reopened");
			newDTO.setValue("Reopened");
			newDTO.setEnabled(1);
			incidentStatesList.add(newDTO);
		} else if (jspType.contains("close")) {
			IncidentStateDTO newDTO = new IncidentStateDTO();
			newDTO.setLabel("Closed");
			newDTO.setValue("Closed");
			newDTO.setEnabled(1);
			incidentStatesList.add(newDTO);
		}

		hdTicketDTO.setHdAuditTrailList(hdAuditTrailList);
		effortFlag = hdMainLogic.getEffortStatus(String.valueOf(ticketId));

		StateTailDTO stateTailDTO = hdMainLogic.getStateTail(String
				.valueOf(ticketId));

		List<ResolutionsDTO> resolutionsList = hdMainLogic.getResolutions();
		// List<SiteDetailDTO> sites = hdMainLogic
		// .getSites(hdTicketDTO.getMsaId());
		model.put("nextActions", nextActions);
		model.put("categories", categories);
		model.put("configurationItems", configurationItems);
		// model.put("auditTrail", hdAuditTrailList);
		// model.put("sites", sites);
		model.put("hdTicketDTO", hdTicketDTO);
		model.put("incidentStates", incidentStatesList);
		model.put("resolutions", resolutionsList);
		model.put("effortFlag", effortFlag);
		model.put("stateTailDTO", stateTailDTO);
		model.put("clientEmail", clientEmail);
		/*
		 * List<HDWorkNoteDTO> hdWorkNoteDTOs = new ArrayList<>();
		 * hdWorkNoteDTOs = hdMainLogic.getWorkNotesList(ticketId); model.put()
		 */
	}

	private String getTicketStatusAbbreviation(String ticketStatus) {
		String abr = ticketStatus;
		if (ticketStatus.contains("WIP - Tech on site")) {
			abr = "DSP-ON";
		} else if (ticketStatus.contains("WIP-Dispatch scheduled")) {
			abr = "DSP-S";
		}

		return abr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#enterNewSite(org.springframework
	 * .ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "enterNewSite.html", method = RequestMethod.GET)
	public ModelAndView enterNewSite(@RequestParam("msaId") String msaId,
			ModelMap model) {
		SiteDetailDTO siteDetailDTO = new SiteDetailDTO();
		siteDetailDTO.setMsaId(msaId);
		List<StateDTO> stateList = generalUtilsLogic.getStateList();
		List<CountryDTO> countryList = generalUtilsLogic.getCountrylist();
		model.put("siteDetailDTO", siteDetailDTO);
		model.put("stateList", stateList);
		model.put("countryList", countryList);
		return new ModelAndView("hdGroupManage/enterNewSite", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getHDPendingAckAlertTableData
	 * (org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "hdPendingAckAlert.html", method = RequestMethod.GET)
	public @ResponseBody
	String getHDPendingAckAlertTableData(ModelMap model,
			HttpServletRequest request) {

		String siteNumber = "";
		if (request.getParameter("searchVal") != null) {
			siteNumber = request.getParameter("searchVal").toString();
		}

		String data = hdMainLogic.getHDPendingAckAlertTableData(siteNumber);
		return data;
	}

	@Override
	@RequestMapping(value = "backupCircuitStatus.html", method = RequestMethod.GET)
	public @ResponseBody
	String getBackupCircuitStatus(ModelMap model, HttpServletRequest request) {

		String siteNumber = "";
		if (request.getParameter("searchVal") != null) {
			siteNumber = request.getParameter("searchVal").toString();
		}

		String data = hdMainLogic.getBackupCircuitStatus(siteNumber);
		return data;
	}

	@Override
	@RequestMapping(value = "effortTimerStatus.html", method = RequestMethod.GET)
	public @ResponseBody
	String effortTimerStatus(ModelMap model, HttpServletRequest request) {

		return hdMainLogic.getEffortStatus(request.getParameter("ticketId"));

	}

	@Override
	@RequestMapping(value = "effortChange.html", method = RequestMethod.GET)
	public @ResponseBody
	String effortChangeManage(ModelMap model, HttpServletRequest request) {

		if (request.getParameter("effortFlag").toString().contains("restart")) {

			hdActiveTicketLogic.saveEffortsForHD(request
					.getParameter("ticketId"), request
					.getParameter("effortDate"), (String) request.getSession()
					.getAttribute("userid"));

		} else if (request.getParameter("effortFlag").toString()
				.contains("stop")) {
			hdActiveTicketLogic.updateEfforts(request.getParameter("ticketId"),
					request.getParameter("effortDate"), (String) request
							.getSession().getAttribute("userid"));

		}
		return "success";

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getHDPendingAckMonitorTableData
	 * (org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "monitoringDateTimeDetailsTable.html", method = RequestMethod.GET)
	public @ResponseBody
	String getHDMonitoringDateTimeTableData(ModelMap model,
			HttpServletRequest request) {

		String siteNumber = "";
		if (request.getParameter("searchVal") != null) {
			siteNumber = request.getParameter("searchVal").toString();
		}
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		String data = hdMainLogic.getHDMonitoringDateTableData(siteNumber,
				userId);
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getHDPendingAckMonitorTableData
	 * (org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "hdPendingAckMonitorTier2.html", method = RequestMethod.GET)
	public @ResponseBody
	String getHDPendingAckMonitorTier2TableData(ModelMap model,
			HttpServletRequest request) {

		String siteNumber = "";
		if (request.getParameter("searchVal") != null) {
			siteNumber = request.getParameter("searchVal").toString();
		}

		String data = hdMainLogic.getHDPendingAckMonitorTableData(siteNumber,
				"in");
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getHDPendingAckMonitorTableData
	 * (org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "hdPendingAckMonitor.html", method = RequestMethod.GET)
	public @ResponseBody
	String getHDPendingAckMonitorTableData(ModelMap model,
			HttpServletRequest request) {

		String siteNumber = "";
		if (request.getParameter("searchVal") != null) {
			siteNumber = request.getParameter("searchVal").toString();
		}

		String data = hdMainLogic.getHDPendingAckMonitorTableData(siteNumber,
				"not in");
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#ackTicketStep1(java.lang.String,
	 * java.lang.String, org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "ackTicketStep1.html", method = RequestMethod.POST)
	public ModelAndView ackTicketStep1(
			@RequestParam("ackCubeType") String ackCubeType,
			@RequestParam("ticketId") String ticketId, ModelMap model) {
		HDTicketDTO hdTicketDTO = new HDTicketDTO();
		hdTicketDTO = hdMainLogic.getAckTicketDetails(ticketId);
		hdTicketDTO.setAckCubeType(ackCubeType);
		model.put("hdTicketDTO", hdTicketDTO);

		List<SiteDetailDTO> sites = hdMainLogic
				.getSites(hdTicketDTO.getMsaId());

		List<HDConfigurationItemDTO> configurationItems = hdMainLogic
				.getConfigurationItems();

		List<HDCategoryDTO> categories = hdMainLogic.getCategories();
		model.put("categories", categories);
		model.put("sites", sites);
		model.put("configurationItems", configurationItems);
		List<HDNextActionDTO> nextActions = hdMainLogic.getNextActions();

		model.put("nextActions", nextActions);

		return new ModelAndView("hdGroupManage/ackTicketStep1", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#ackTicket(com.ilex.ws.core.dto
	 * .HDTicketDTO)
	 */
	@Override
	@RequestMapping(value = "ackTicket.html", method = RequestMethod.POST)
	public @ResponseBody
	String ackTicket(@ModelAttribute("hdTicketDTO") HDTicketDTO hdTicketDTO,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		hdMainLogic.clearReviewStatus(hdTicketDTO.getTicketId());
		hdMainLogic.ackTicket(hdTicketDTO, userId);
		return "success";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.controller.HDMainController#getCubeStatus()
	 */
	@Override
	@RequestMapping(value = "getCubeStatus.html", method = RequestMethod.GET)
	public @ResponseBody
	String getCubeStatus() {
		HDCubeStatusDTO hdCubeStatusDTO = new HDCubeStatusDTO();

		hdCubeStatusDTO = hdMainLogic.getCubeStatus();
		// have to do some thing for next action due

		if (hdMainLogic.getNextActionDueStatus()) {
			hdCubeStatusDTO.setNextActiondueStatus("true");
		} else {
			hdCubeStatusDTO.setNextActiondueStatus("false");
		}

		JSONObject jsonObject = new JSONObject();
		jsonObject = JSONObject.fromObject(hdCubeStatusDTO);

		return jsonObject.toString();
	}

	@Override
	@RequestMapping(value = "getEffortDetails.html", method = RequestMethod.GET)
	public @ResponseBody
	String getEffortDetails(@RequestParam("ticketId") Long ticketId) {

		List<HDEffortDTO> hdEffortDTOList = hdMainLogic
				.getEffortsDetail(ticketId);
		JSONArray jsonObject = new JSONArray();
		jsonObject = JSONArray.fromObject(hdEffortDTOList);

		return jsonObject.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getResolveTicket(java.lang.Long,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "hdResolveTicket.html", method = RequestMethod.GET)
	public ModelAndView getResolveTicket(
			@RequestParam("ticketId") Long ticketId, ModelMap model,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		HDTicketDTO hdTicketDTO = new HDTicketDTO();
		hdTicketDTO = hdMainLogic.getResolveTicketSummary(ticketId, userId);
		Map<String, String> pointOfFailureList = hdMainLogic
				.getPointOfFailureList();
		Map<String, String> rootCauseList = hdMainLogic.getRootCauseList();
		Map<String, String> resolutionList = hdMainLogic.getResolutionList();
		model.put("hdTicketDTO", hdTicketDTO);
		model.put("pointOfFailureList", pointOfFailureList);
		model.put("rootCauseList", rootCauseList);
		model.put("resolutionList", resolutionList);
		return new ModelAndView("hdGroupManage/hdResolveTicket", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#clearSession(javax.servlet.http
	 * .HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "hdClearSession.html", method = RequestMethod.POST)
	public void clearSession(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.invalidate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#getAlertStatePage(java.lang.String
	 * )
	 */
	@Override
	@RequestMapping(value = "alertState.html", method = RequestMethod.GET)
	public ModelAndView getAlertStatePage(String ticketId) {
		return new ModelAndView("hdGroupManage/alertState");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#editTicketDetailsPage(java.lang
	 * .String)
	 */
	@Override
	@RequestMapping(value = "editTicketDetailsPage.html", method = RequestMethod.GET)
	public ModelAndView editTicketDetailsPage(String ticketId, ModelMap model) {
		HDTicketDTO hdTicketDTO = hdMainLogic.getTicketDetailsForEdit(ticketId);
		model.put("hdTicketDTO", hdTicketDTO);

		List<HDConfigurationItemDTO> configurationItems = hdMainLogic
				.getConfigurationItems();
		List<HDCategoryDTO> categories = hdMainLogic.getCategories();

		model.put("categories", categories);
		model.put("configurationItems", configurationItems);

		return new ModelAndView("hdGroupManage/hdEditTicket");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.HDMainController#editTicketDetails(com.ilex.ws
	 * .core.dto.HDTicketDTO)
	 */
	@Override
	@RequestMapping(value = "editTicketDetails.html", method = RequestMethod.POST)
	public @ResponseBody
	String editTicketDetails(HDTicketDTO hdTicketDTO) {
		hdMainLogic.editTicketDetails(hdTicketDTO);

		return "success";
	}

	@Override
	@RequestMapping(value = "editPrioritySecPage.html", method = RequestMethod.GET)
	public ModelAndView editPrioritySecPage(String ticketId, ModelMap model) {
		HDTicketDTO hdTicketDTO = hdMainLogic
				.getPriorityDetailsForEdit(ticketId);
		model.put("hdTicketDTO", hdTicketDTO);

		return new ModelAndView("hdGroupManage/hdEditPrioritySection");
	}

	@Override
	@RequestMapping(value = "editPrioritySection.html", method = RequestMethod.POST)
	public @ResponseBody
	String editPrioritySection(HDTicketDTO hdTicketDTO,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		hdMainLogic.editPrioritySection(hdTicketDTO, userId);

		return "success";
	}

	@Override
	@RequestMapping(value = "editBackupPage.html", method = RequestMethod.GET)
	public ModelAndView editBackupPage(String ticketId, ModelMap model) {
		HDTicketDTO hdTicketDTO = hdMainLogic.getBackupDetailsForEdit(ticketId);
		model.put("hdTicketDTO", hdTicketDTO);

		return new ModelAndView("hdGroupManage/hdEditBackupSection");
	}

	@Override
	@RequestMapping(value = "editBackupDetails.html", method = RequestMethod.POST)
	public @ResponseBody
	String editBackupDetails(HDTicketDTO hdTicketDTO, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		hdMainLogic.editBackupDetails(hdTicketDTO, userId);

		return "success";
	}

	@Override
	@RequestMapping(value = "editResolutionSecPage.html", method = RequestMethod.GET)
	public ModelAndView editResolutionSecPage(String ticketId, ModelMap model) {
		HDTicketDTO hdTicketDTO = hdMainLogic
				.getResolutionDetailsForEdit(ticketId);
		model.put("hdTicketDTO", hdTicketDTO);

		Map<String, String> pointOfFailureList = hdMainLogic
				.getPointOfFailureList();
		Map<String, String> rootCauseList = hdMainLogic.getRootCauseList();
		Map<String, String> resolutionList = hdMainLogic.getResolutionList();

		model.put("pointOfFailureList", pointOfFailureList);
		model.put("rootCauseList", rootCauseList);
		model.put("resolutionList", resolutionList);

		return new ModelAndView("hdGroupManage/hdEditResolutionSection");
	}

	@Override
	@RequestMapping(value = "editResolution.html", method = RequestMethod.POST)
	public @ResponseBody
	String editResolutionDetails(HDTicketDTO hdTicketDTO,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		hdMainLogic.editResolutionDetails(hdTicketDTO, userId);

		return "success";
	}

	@Override
	@RequestMapping(value = "stateTail.html", method = RequestMethod.GET)
	public ModelAndView stateTail(String ticketId, HttpServletRequest request) {
		StateTailDTO stateTailDTO = hdMainLogic.getStateTail(ticketId);
		request.setAttribute("stateTailDTO", stateTailDTO);
		return new ModelAndView("hdGroupManage/hdStateTail");
	}

	@Override
	@RequestMapping(value = "updateResolveTicket.html", method = RequestMethod.POST)
	public @ResponseBody
	String updateResolveTicket(
			@ModelAttribute("hdTicketDTO") HDTicketDTO hdTicketDTO,
			ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");

		hdMainLogic.updateTicketToResolved(hdTicketDTO, userId);

		if (hdTicketDTO.getTicketStatus().equals("Closed")) {

			List<HDWorkNoteDTO> workNotesList = hdMainLogic
					.getWorkNotesList(Long.parseLong(hdTicketDTO.getTicketId()));

			for (int i = 0; i < workNotesList.size(); i++) {
				HDWorkNoteDTO dto = workNotesList.get(i);

				if (dto.getWorkNote()
						.contains(
								"ServiceNow update failed.Contact customer by secondary method")) {
					return dto.getWorkNote();

				}
				if (i >= 2) {
					/**
					 * Expecting that this message could be at first 2 indexes
					 * so using break after 2nd iteration.
					 * 
					 */
					break;
				}
			}

		}

		return "success";

	}

	@Override
	@RequestMapping(value = "addNewSite.html", method = RequestMethod.POST)
	public @ResponseBody
	String addNewSite(
			@ModelAttribute("siteDetailDTO") SiteDetailDTO siteDetailDTO,
			ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		Long siteId = hdMainLogic.addNewSite(siteDetailDTO, userId);
		AjaxResponseDTO ajaxResponseDTO = new AjaxResponseDTO();
		ajaxResponseDTO.setSuccess("success");
		ajaxResponseDTO.setId(siteId);
		JsonConfig jsonConfig = new JsonConfig();
		JSONObject jsonObject = JSONObject.fromObject(ajaxResponseDTO,
				jsonConfig);
		return jsonObject.toString();
	}

	@Override
	@RequestMapping(value = "getSitesList.html", method = RequestMethod.POST)
	public @ResponseBody
	String getSitesList(@RequestParam(value = "msaId") String msaId,
			ModelMap model, HttpServletRequest request) {
		List<SiteDetailDTO> sites = new ArrayList<>();
		sites = hdMainLogic.getSites(Long.parseLong(msaId));
		JSONArray jsonArray = JSONArray.fromObject(sites);
		return jsonArray.toString();
	}

	@Override
	@RequestMapping(value = "workNotes.html", method = RequestMethod.GET)
	public ModelAndView getWorkNotesPage(String ticketId, ModelMap map) {
		map.put("ticketId", ticketId);
		return new ModelAndView("hdGroupManage/hdWorkNotes");
	}

	@Override
	@RequestMapping(value = "effortsSave.html", method = RequestMethod.GET)
	public ModelAndView getEffortsSavePage(String ticketId, ModelMap map) {
		map.put("ticketId", ticketId);
		return new ModelAndView("hdGroupManage/hdEfforts");
	}

	@Override
	@RequestMapping(value = "nextActionSave.html", method = RequestMethod.GET)
	public ModelAndView getNextActionSavePage(
			@RequestParam("ticketId") String ticketId,
			@RequestParam("nextActionId") String nextActionId, ModelMap map) {
		map.put("ticketId", ticketId);
		map.put("currentNextActionId", nextActionId);
		List<HDNextActionDTO> nextActions = hdMainLogic.getNextActions();
		map.put("nextActions", nextActions);
		return new ModelAndView("hdGroupManage/hdNextAction");
	}

	@Override
	@RequestMapping(value = "hdCancelTicket.html", method = RequestMethod.GET)
	public ModelAndView getCancelTicketPage(String ticketId, ModelMap map) {
		map.put("ticketId", ticketId);
		return new ModelAndView("hdGroupManage/hdCancelTicket", map);
	}

	@Override
	@RequestMapping(value = "cancelTicket.html", method = RequestMethod.POST)
	public @ResponseBody
	String cancelTicket(String ticketId, String workNotes,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		if (ticketId == null || ticketId.equals("")) {
			return "fail";
		}
		hdMainLogic.clearReviewStatus(ticketId);
		hdMainLogic.cancelTicket(ticketId, workNotes, userId);
		return "success";
	}

	@Override
	@RequestMapping(value = "hdNoActionPage.html", method = RequestMethod.GET)
	public ModelAndView getNoActionPage(String ticketId, ModelMap map) {
		map.put("ticketId", ticketId);
		return new ModelAndView("hdGroupManage/hdNoAction", map);
	}

	@Override
	@RequestMapping(value = "saveNoAction.html", method = RequestMethod.POST)
	public @ResponseBody
	String saveNoAction(String ticketId, HttpServletRequest request) {
		// HttpSession session = request.getSession();
		// String userId = (String) session.getAttribute("userid");
		// hdMainLogic.saveNoAction(ticketId, userId);
		return "fail";
	}

	@Override
	@RequestMapping(value = "openNoActionsDaysPage.html", method = RequestMethod.GET)
	public ModelAndView getNoActionsDaysPage(String ticketId, ModelMap map) {
		Map<String, String> noActionDataMap = hdMainLogic
				.getNoActionsDaysPage(ticketId);
		map.put("noActionDataMap", noActionDataMap);

		return new ModelAndView("hdGroupManage/hdNoActionDays", map);
	}

	@Override
	@RequestMapping(value = "editSiteSecPage.html", method = RequestMethod.GET)
	public ModelAndView getEditSitePage(
			@RequestParam("ticketId") String ticketId,
			@RequestParam("siteId") String siteId, ModelMap model) {
		SiteDetailDTO siteDetailDTO = hdMainLogic.getSiteDetails(siteId);
		siteDetailDTO.setTicketId(ticketId);
		siteDetailDTO.setTicketId(ticketId);
		model.put("siteDetailDTO", siteDetailDTO);
		return new ModelAndView("hdGroupManage/hdEditSiteSection");
	}

	@Override
	@RequestMapping(value = "updateSiteSection.html", method = RequestMethod.POST)
	public @ResponseBody
	String updateSiteSection(SiteDetailDTO siteDetailDTO,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		hdMainLogic.updateSite(siteDetailDTO, userId);
		return "success";
	}

	@Override
	@RequestMapping(value = "editTicketState.html", method = RequestMethod.GET)
	public ModelAndView editTicketStatePage(
			@RequestParam("ticketId") String ticketId,
			@RequestParam("ticketStatus") String ticketStatus, ModelMap model,
			HttpServletRequest request) {
		HDTicketDTO hdTicketDTO = new HDTicketDTO();
		hdTicketDTO.setTicketId(ticketId);
		hdTicketDTO.setTicketStatus(ticketStatus);
		model.put("hdTicketDTO", hdTicketDTO);
		return new ModelAndView("hdGroupManage/hdTicketState", model);

	}

	@Override
	@RequestMapping(value = "updateTicketTargetState.html", method = RequestMethod.POST)
	public @ResponseBody
	String updateTicketState(
			@ModelAttribute("hdTicketDTO") HDTicketDTO hdTicketDTO,
			ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		hdActiveTicketLogic.updateState(hdTicketDTO.getTicketId(),
				hdTicketDTO.getTicketStatus(), userId);
		hdMainLogic.saveWorkNotes(hdTicketDTO.getTicketId(),
				hdTicketDTO.getTicketStatus(), userId);
		return "success";

	}

	@Override
	@RequestMapping(value = "noActionAll.html", method = RequestMethod.POST)
	public @ResponseBody
	String noActionAll(HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		hdMainLogic.noActionAll(userId);
		return "success";
	}

	@Override
	@RequestMapping(value = "lastUpdateEventDetail.html", method = RequestMethod.POST)
	public @ResponseBody
	String getLastUpdateEventDetail(HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		RealTimeStateInfo realTimeStateInfo = hdMainLogic
				.getLastUpdateEventDetail();
		JsonConfig jsonConfig = new JsonConfig();
		JSONObject jsonObject = JSONObject.fromObject(realTimeStateInfo,
				jsonConfig);
		return jsonObject.toString();
	}

	@Override
	@RequestMapping(value = "getWorkNotesList.html", method = RequestMethod.POST)
	public @ResponseBody
	String getWorkNotesList(@RequestParam(value = "ticketId") Long ticketId,
			ModelMap model, HttpServletRequest request) {
		List<HDWorkNoteDTO> hdWorkNoteDTOs = new ArrayList<>();
		hdWorkNoteDTOs = hdMainLogic.getWorkNotesList(ticketId);
		JSONArray jsonArray = JSONArray.fromObject(hdWorkNoteDTOs);
		return jsonArray.toString();
	}

	// get Install Notes

	@Override
	@RequestMapping(value = "getDateTimeListForMonitoring.html", method = RequestMethod.POST)
	public @ResponseBody
	String getDateTimeListForMonitoring(
			@RequestParam(value = "ticketId") Long ticketId, ModelMap model,
			HttpServletRequest request) {
		List<HDMonitoringTimeDTO> hdMonitoringTimeDTOs = new ArrayList<>();
		hdMonitoringTimeDTOs = hdMainLogic
				.getDateTimeListForMonitoringLogic(ticketId);
		JSONArray jsonArray = JSONArray.fromObject(hdMonitoringTimeDTOs);
		return jsonArray.toString();
	}

	@Override
	@RequestMapping(value = "getWorkNotesPage.html", method = RequestMethod.GET)
	public ModelAndView getWorkNotesPage(
			@RequestParam("ticketId") Long ticketId, ModelMap model,
			HttpServletRequest request) {
		List<HDWorkNoteDTO> hdWorkNoteDTOs = new ArrayList<HDWorkNoteDTO>();
		hdWorkNoteDTOs = hdMainLogic.getWorkNotesList(ticketId);
		model.put("hdWorkNoteDTOs", hdWorkNoteDTOs);
		return new ModelAndView("hdGroupManage/latestWorkNotes", model);
	}

	@Override
	@RequestMapping(value = "getCancelAllForm.html", method = RequestMethod.GET)
	public ModelAndView getCancelAllForm(ModelMap model,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		HdCancelAllDTO hdCancelAllDTO = new HdCancelAllDTO();
		List<HDCustomerDetailDTO> customerList = hdMainLogic
				.getCustomerListForCancelAll();
		Map<String, String> pointOfFailureList = hdMainLogic
				.getPointOfFailureList();
		Map<String, String> rootCauseList = hdMainLogic.getRootCauseList();
		Map<String, String> resolutionList = hdMainLogic.getResolutionList();
		hdCancelAllDTO.setDuration(5);
		hdCancelAllDTO.setCustomerList(customerList);

		List<ResolutionsDTO> resolutionsList = hdMainLogic.getResolutions();
		model.put("resolutions", resolutionsList);

		model.put("hdCancelAllDTO", hdCancelAllDTO);
		model.put("pointOfFailureList", pointOfFailureList);
		model.put("rootCauseList", rootCauseList);
		model.put("resolutionList", resolutionList);
		return new ModelAndView("hdGroupManage/hdCancelAll", model);

	}

	@Override
	@RequestMapping(value = "displayOutageList.html", method = RequestMethod.POST)
	public @ResponseBody
	String displayOutageList(
			@ModelAttribute("hdCancelAllDTO") HdCancelAllDTO hdCancelAllDTO,
			ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		List<OutageDetailDTO> outageList = hdMainLogic
				.getAllOutageEvents(hdCancelAllDTO);
		JSONArray jsonArray = JSONArray.fromObject(outageList);
		return jsonArray.toString();

	}

	@Override
	@RequestMapping(value = "refreshOutageList.html", method = RequestMethod.POST)
	public @ResponseBody
	String refreshOutageList(
			@ModelAttribute("hdCancelAllDTO") HdCancelAllDTO hdCancelAllDTO,
			ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		List<OutageDetailDTO> outageList = hdMainLogic
				.getFalseOutageEvents(hdCancelAllDTO);
		JSONArray jsonArray = JSONArray.fromObject(outageList);
		return jsonArray.toString();

	}

	@Override
	@RequestMapping(value = "cancelAllOutages.html", method = RequestMethod.POST)
	public @ResponseBody
	String cancelAllOutages(
			@ModelAttribute("hdCancelAllDTO") HdCancelAllDTO hdCancelAllDTO,
			ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		hdMainLogic.cancelAll(userId, hdCancelAllDTO);
		return "success";

	}

	@Override
	@RequestMapping(value = "saveWorkNotes.html", method = RequestMethod.POST)
	public @ResponseBody
	String saveWorkNotes(String ticketId, String workNotes,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		hdMainLogic.saveWorkNotes(ticketId, workNotes, userId);
		return "success";
	}

	@Override
	@RequestMapping(value = "checkStatus.html", method = RequestMethod.POST)
	public @ResponseBody
	String checkStatus(@RequestParam("ackCubeType") String ackCubeType,
			@RequestParam("ticketId") String ticketId,
			HttpServletRequest request) {

		HttpSession session = request.getSession();

		String userId = (String) session.getAttribute("userid");
		String sessionId = session.getId();
		String domainName = "";
		try {
			domainName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			logger.error(e.getMessage(), e);
		}
		hdMainLogic.checkForStatus(ackCubeType, ticketId, userId, sessionId,
				domainName);
		return "success";
	}

	@Override
	@RequestMapping(value = "checkLockStatus.html", method = RequestMethod.POST)
	public @ResponseBody
	String checkLockStatus(@RequestParam("ticketId") String ticketId,
			HttpServletRequest request) {

		HttpSession session = request.getSession();

		String userId = (String) session.getAttribute("userid");
		String sessionId = session.getId();
		String domainName = "";
		try {
			domainName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			logger.error(e.getMessage(), e);
		}
		return hdMainLogic.checkForHDTicketLockStatus(ticketId, userId,
				sessionId, domainName);

	}

	@Override
	@RequestMapping(value = "clearLockStatus.html", method = RequestMethod.POST)
	public @ResponseBody
	String clearLockStatus(@RequestParam("ticketId") String ticketId,
			HttpServletRequest request) {

		if (ticketId == null || ticketId.equals("")) {
			return "failure";
		}

		hdMainLogic.clearReviewStatus(ticketId);
		return "success";

	}

	@Override
	@RequestMapping(value = "clearReviewStatus.html", method = RequestMethod.POST)
	public void clearReviewStatus(@RequestParam("ticketId") String ticketId) {
		if (ticketId == null || ticketId.equals("")) {
			return;
		}
		hdMainLogic.clearReviewStatus(ticketId);

	}
}
