package com.ilex.ws.controllerImpl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.controller.TicketDispatchController;
import com.ilex.ws.core.dto.LabelValueDTO;
import com.ilex.ws.core.dto.TicketDispatchDTO;
import com.ilex.ws.core.logic.TicketDispatchLogic;
import com.ilex.ws.core.util.IlexDateUtils;
import com.ilex.ws.core.util.NullAwareBeanUtilsBean;

/**
 * The Class TicketDispatchControllerImpl.
 */
@Controller
public class TicketDispatchControllerImpl implements TicketDispatchController {

	private static final Logger logger = Logger
			.getLogger(TicketDispatchControllerImpl.class);

	/** The ticket dispatch logic. */
	@Resource
	TicketDispatchLogic ticketDispatchLogic;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.TicketDispatchController#getTicketRequestPage(
	 * com.ilex.ws.core.dto.TicketDispatchDTO, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "getTicketRequestPage.html", method = RequestMethod.GET)
	public ModelAndView getTicketRequestPage(
			@ModelAttribute("ticketDispatchDTO") TicketDispatchDTO ticketDispatchDTO,
			ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");

		TicketDispatchDTO ticketDispatchInfo = ticketDispatchLogic
				.getTicketDispatchInfo(Long.valueOf(userId),
						ticketDispatchDTO.getAppendixId());

		NullAwareBeanUtilsBean awareBeanUtilsBean = new NullAwareBeanUtilsBean();
		List<LabelValueDTO> preferredHourList = awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getHours());
		List<LabelValueDTO> preferredMinuteList = awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getMinutes());
		List<LabelValueDTO> windowFromHourList = awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getHours());
		List<LabelValueDTO> windowFromMinuteList = awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getMinutes());
		List<LabelValueDTO> windowToHourList = awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getHours());
		List<LabelValueDTO> windowToMinuteList = awareBeanUtilsBean.copyList(
				LabelValueDTO.class, IlexDateUtils.getMinutes());
		model.addAttribute("preferredHourList", preferredHourList);
		model.addAttribute("preferredMinuteList", preferredMinuteList);
		model.addAttribute("windowFromHourList", windowFromHourList);
		model.addAttribute("windowFromMinuteList", windowFromMinuteList);
		model.addAttribute("windowToHourList", windowToHourList);
		model.addAttribute("windowToMinuteList", windowToMinuteList);
		BeanUtils.copyProperties(ticketDispatchInfo, ticketDispatchDTO);
		ticketDispatchDTO.setOnsiteFieldRequest(true);
		return new ModelAndView("requestDispatch/ticketRequest", model);
	}

}
