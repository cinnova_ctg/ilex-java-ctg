package com.ilex.ws.controllerImpl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.controller.ServiceNowIntegrationController;
import com.ilex.ws.core.dto.HDConfigurationItemDTO;
import com.ilex.ws.core.dto.HDTicketDTO;
import com.ilex.ws.core.dto.IncidentDTO;
import com.ilex.ws.core.dto.SiteDetailDTO;
import com.ilex.ws.core.dto.TicketStateDTO;
import com.ilex.ws.core.logic.HDMainLogic;
import com.ilex.ws.core.logic.ServiceNowLogic;

/**
 * The Class ServiceNowIntegrationControllerImpl.
 */
@Controller
@RequestMapping("/serviceNowIntegrate")
public class ServiceNowIntegrationControllerImpl extends
		AbstractIlexBaseController implements ServiceNowIntegrationController {

	/** The hd main logic. */
	@Resource
	HDMainLogic hdMainLogic;

	/** The service now logic. */
	@Resource
	ServiceNowLogic serviceNowLogic;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.ServiceNowIntegrationController#getRecentIncidents
	 * (java.lang.String, java.lang.String, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/recentIncidents.html", method = RequestMethod.GET)
	public ModelAndView getRecentIncidents(@RequestParam("msaId") String msaId,
			@RequestParam("appendixId") String appendixId, ModelMap model,
			HttpServletRequest request) {
		List<IncidentDTO> proactiveIncidentDTOs = serviceNowLogic
				.getIncidentsList("Proactive", msaId);
		List<IncidentDTO> reactiveIncidentDTOs = serviceNowLogic
				.getIncidentsList("Reactive", msaId);
		model.put("proactiveIncidentDTOs", proactiveIncidentDTOs);
		model.put("reactiveIncidentDTOs", reactiveIncidentDTOs);
		return new ModelAndView("/serviceNow/serviceNowIncidents", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.controller.ServiceNowIntegrationController#
	 * getProactiveIncidentCreationPage(java.lang.String, java.lang.String,
	 * org.springframework.ui.ModelMap, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/getProactiveIncidentCreatePage.html", method = RequestMethod.GET)
	public ModelAndView getProactiveIncidentCreationPage(
			@RequestParam("msaId") String msaId,
			@RequestParam("appendixId") String appendixId, ModelMap model,
			HttpServletRequest request) {
		IncidentDTO proactiveIncidentDTO = new IncidentDTO();
		proactiveIncidentDTO.setMsaId(msaId);
		proactiveIncidentDTO.setAppendixId(appendixId);
		// Set Default values
		proactiveIncidentDTO.setCategory("Restaurant/Hardware/Connectivity");
		proactiveIncidentDTO
				.setShortDescription("Network Monitoring [Site] is Down");
		proactiveIncidentDTO.setType("Connectivity");
		proactiveIncidentDTO.setVendorKey("ILEX");
		proactiveIncidentDTO.setWorkNotes("Beginning trouble shooting process");
		proactiveIncidentDTO.setState("New");
		List<HDConfigurationItemDTO> configurationItems = hdMainLogic
				.getConfigurationItems();
		List<SiteDetailDTO> sites = hdMainLogic.getSites(Long
				.parseLong((msaId)));
		List<TicketStateDTO> ticketStateDTOList = serviceNowLogic
				.getTicketStates();
		model.put("ticketStates", ticketStateDTOList);
		model.put("configurationItems", configurationItems);
		model.put("sites", sites);
		model.put("ticketStateDTOList", ticketStateDTOList);
		model.put("proactiveIncidentDTO", proactiveIncidentDTO);
		return new ModelAndView("/serviceNow/createProactiveIncident", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.ServiceNowIntegrationController#createHDTicket
	 * (com.ilex.ws.core.dto.IncidentDTO, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "addProactiveIncident.html", method = RequestMethod.POST)
	public @ResponseBody
	String createHDTicket(
			@ModelAttribute("proactiveIncidentDTO") IncidentDTO proactiveIncidentDTO,
			ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		String ilexTicketNumber = hdMainLogic.avaliableIlexTicketNumber();
		proactiveIncidentDTO.setTicketNumber(ilexTicketNumber);
		String siteNumber = proactiveIncidentDTO.getSiteNumber();
		if (proactiveIncidentDTO.getMsaId().equalsIgnoreCase("740")) {
			// For TXR
			if (StringUtils.isNotBlank(siteNumber)) {
				siteNumber = siteNumber.replaceAll("\\D+", "");
				siteNumber = siteNumber.replaceAll("^0*", "");
			}
		} else if (proactiveIncidentDTO.getMsaId().equalsIgnoreCase("462")) {
			// For CFA
			if (StringUtils.isNotBlank(siteNumber)) {
				siteNumber = siteNumber.replaceAll("\\D+", "");
				if (StringUtils.isNotBlank(siteNumber)) {
					int siteNum = Integer.parseInt(siteNumber);
					siteNumber = String.format("%05d", siteNum);
				}
			}
		}
		proactiveIncidentDTO.setSiteNumber(siteNumber);
		proactiveIncidentDTO.setIntegrationType("Proactive");
		proactiveIncidentDTO.setIntegrationStatus("Pending");

		Map<String, String> createResponse = serviceNowLogic
				.createProactiveIncident(proactiveIncidentDTO,
						Integer.parseInt(userId));
		String message = createResponse.get("message");
		return message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.ServiceNowIntegrationController#getErrorLogs(java
	 * .lang.String, java.lang.String, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/errorLogs.html", method = RequestMethod.GET)
	public ModelAndView getErrorLogs(@RequestParam("msaId") String msaId,
			@RequestParam("appendixId") String appendixId, ModelMap model,
			HttpServletRequest request) {
		List<IncidentDTO> createIncidentErrorDTOs = serviceNowLogic
				.getCreateIncidentErrors();
		List<IncidentDTO> updateIncidentErrorDTOs = serviceNowLogic
				.getUpdateIncidentErrors();
		model.put("createIncidentErrorDTOs", createIncidentErrorDTOs);
		model.put("updateIncidentErrorDTOs", updateIncidentErrorDTOs);
		return new ModelAndView("/serviceNow/serviceNowErrorLogs", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.controller.ServiceNowIntegrationController#
	 * getIncidentSendUpdateScreen(java.lang.Long,
	 * org.springframework.ui.ModelMap, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/getIncidentSendUpdateScreen.html", method = RequestMethod.GET)
	public ModelAndView getIncidentSendUpdateScreen(
			@RequestParam("createIdentifier") Long createIdentifier,
			ModelMap model, HttpServletRequest request) {
		IncidentDTO incidentDTO = serviceNowLogic
				.getIncidentDetail(createIdentifier);
		incidentDTO.setVendorKey("ILEX");
		incidentDTO.setState("");
		incidentDTO.setWorkNotes("");
		model.put("incidentDTO", incidentDTO);
		return new ModelAndView("/serviceNow/updateIncident", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.ServiceNowIntegrationController#updateIncident
	 * (com.ilex.ws.core.dto.IncidentDTO, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "updateIncident.html", method = RequestMethod.POST)
	public @ResponseBody
	String updateIncident(
			@ModelAttribute("incidentDTO") IncidentDTO incidentDTO,
			ModelMap model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		incidentDTO.setIntegrationStatus("Pending");
		incidentDTO.setTriggerSource("Ilex");
		Map<String, String> updateResponse = serviceNowLogic
				.updateIncidentFromIlextoSN(incidentDTO,
						Integer.parseInt(userId));
		String message = updateResponse.get("message");
		return message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.controller.ServiceNowIntegrationController#
	 * getIncidentDetailScreen(java.lang.Long, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/getIncidentDetailScreen.html", method = RequestMethod.GET)
	public ModelAndView getIncidentDetailScreen(
			@RequestParam("createIdentifier") Long createIdentifier,
			ModelMap model, HttpServletRequest request) {
		IncidentDTO incidentDTO = serviceNowLogic
				.getIncidentDetail(createIdentifier);
		List<IncidentDTO> incidentUpdatesDTO = serviceNowLogic
				.getIncidentUpdates(createIdentifier);
		model.put("incidentDTO", incidentDTO);
		model.put("incidentUpdatesDTO", incidentUpdatesDTO);
		return new ModelAndView("/serviceNow/incidentDetail", model);
	}

	@Override
	@RequestMapping(value = "/getSnMessage.html", method = RequestMethod.POST)
	public @ResponseBody
	String getSnMessage(@RequestParam("ticketId") Long ticketId) {
		HDTicketDTO hdTicketDTO = serviceNowLogic.getSnMessage(ticketId);
		JSONObject jsonObject = new JSONObject();
		jsonObject = JSONObject.fromObject(hdTicketDTO);
		return jsonObject.toString();

	}

	@Override
	@RequestMapping(value = "/clearIntegratedTicketStatus.html", method = RequestMethod.POST)
	public @ResponseBody
	String clearIntegratedTicketStatus(@RequestParam("ticketId") Long ticketId) {
		serviceNowLogic.clearIntegratedTicketStatus(ticketId);
		return "success";
	}
}
