package com.ilex.ws.controllerImpl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.controller.SiteSearchController;
import com.ilex.ws.core.dto.SiteSearchDTO;
import com.ilex.ws.core.dto.SiteSearchDetailDTO;
import com.ilex.ws.core.logic.SiteSearchLogic;

/**
 * The Class SiteSearchControllerImpl.
 */
@Controller
public class SiteSearchControllerImpl implements SiteSearchController {

	/** The site search logic. */
	@Resource
	SiteSearchLogic siteSearchLogic;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.SiteSearchController#searchSite(com.ilex.ws.core
	 * .dto.SiteSearchDTO, org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/searchSite.html", method = RequestMethod.GET)
	public ModelAndView searchSite(
			@ModelAttribute("siteSearchDTO") SiteSearchDTO siteSearchDTO,
			ModelMap model) {

		SiteSearchDTO siteInfo = siteSearchLogic.getSiteInfo(siteSearchDTO);
		BeanUtils.copyProperties(siteInfo, siteSearchDTO);
		String siteEndCustomer = siteSearchLogic.getEndCustomer(siteSearchDTO
				.getAppendixId());
		siteSearchDTO.setSiteEndCustomer(siteEndCustomer);
		model.addAttribute("siteSearchDTO", siteSearchDTO);

		return new ModelAndView("newDispatch/searchSite", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.SiteSearchController#searchResult(com.ilex.ws.
	 * core.dto.SiteSearchDTO, org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/searchResult.html", method = RequestMethod.GET)
	public ModelAndView searchResult(
			@ModelAttribute("siteSearchDTO") SiteSearchDTO siteSearchDTO,
			ModelMap model) {

		SiteSearchDTO siteInfo = siteSearchLogic.getSiteInfo(siteSearchDTO);
		List<SiteSearchDetailDTO> siteSearchDetails = siteSearchLogic
				.getSearchResult(siteInfo);

		BeanUtils.copyProperties(siteInfo, siteSearchDTO);
		siteSearchDTO.setSiteSearchDetails(siteSearchDetails);
		model.addAttribute("siteSearchDTO", siteSearchDTO);

		return new ModelAndView("newDispatch/searchSite", model);

	}

}
