package com.ilex.ws.controllerImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.invoice.reporting.dto.InvoiceDetailsDTO;
import com.ilex.report.pdf.helper.TemplateATestData;
import com.ilex.report.pdf.template.PDFTemplateA;
import com.ilex.ws.core.bean.UserDTO;
import com.ilex.ws.core.logic.DemoLogic;

@Controller
@SessionAttributes
public class DemoController extends AbstractIlexBaseController {

	@RequestMapping(value = "home.html")
	public ModelAndView homepage() {
		return new ModelAndView("showPdfReport");
	}

	@Resource
	DemoLogic demoLogic;

	@RequestMapping(value = "userDetails.html")
	public ModelAndView getUserDetails(@ModelAttribute("user") UserDTO user,
			BindingResult result) {
		initExceptionHandler("Home", "command", user);
		String userId = user.getUserId();
		UserDTO loginDTO = demoLogic.getUserInfo(userId);
		System.out.println("Hello World");
		return new ModelAndView("UserDetails", "command", loginDTO);
	}

	@RequestMapping(value = "singlePagePdfReport.html")
	public void singlePageReport(HttpServletRequest Request,
			HttpServletResponse response) throws IOException {

		TemplateATestData.populateBaseData();

		List<InvoiceDetailsDTO> invoiceDetList = new ArrayList<InvoiceDetailsDTO>();
		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);
		TemplateATestData.invoiceReportDTO
				.setInvoiceDetailsDTOs(invoiceDetList);

		PDFTemplateA pdfTemplateA = new PDFTemplateA();
		byte[] byteArray = pdfTemplateA
				.generatePDF(TemplateATestData.invoiceReportDTO);

		response.reset();
		response.setContentType("application/pdf");
		response.setContentLength(byteArray.length);
		response.setHeader("Content-Disposition",
				"attachment;filename=singlePagePdfReport.pdf");

		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.write(byteArray);
		outputStream.flush();
		outputStream.close();

	}

	@RequestMapping(value = "multiplePagesPdfReport.html")
	public void multiplePagesReport(HttpServletRequest Request,
			HttpServletResponse response) throws IOException {

		TemplateATestData.populateBaseData();

		List<InvoiceDetailsDTO> invoiceDetList = new ArrayList<InvoiceDetailsDTO>();

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.invoiceDetDto2);
		invoiceDetList.add(TemplateATestData.invoiceDetDto3);
		invoiceDetList.add(TemplateATestData.invoiceDetDto4);
		invoiceDetList.add(TemplateATestData.invoiceDetDto5);

		TemplateATestData.invoiceReportDTO
				.setInvoiceDetailsDTOs(invoiceDetList);

		PDFTemplateA pdfTemplateA = new PDFTemplateA();
		byte[] byteArray = pdfTemplateA
				.generatePDF(TemplateATestData.invoiceReportDTO);
		response.reset();
		response.setContentType("application/pdf");
		response.setContentLength(byteArray.length);
		response.setHeader("Content-Disposition",
				"attachment;filename=multiplePagesPdfReport.pdf");

		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.write(byteArray);
		outputStream.flush();
		outputStream.close();
	}

	@RequestMapping(value = "NetMedX_longPdfReport.html")
	public void NetMedX_longPdfReport(HttpServletRequest Request,
			HttpServletResponse response) throws IOException {

		TemplateATestData.populateBaseData();

		List<InvoiceDetailsDTO> invoiceDetList = new ArrayList<InvoiceDetailsDTO>();

		invoiceDetList.add(TemplateATestData.invoiceDetDto1);
		invoiceDetList.add(TemplateATestData.netMedXLongInvoiceDetDto);

		TemplateATestData.invoiceReportDTO
				.setInvoiceDetailsDTOs(invoiceDetList);

		PDFTemplateA pdfTemplateA = new PDFTemplateA();
		byte[] byteArray = pdfTemplateA
				.generatePDF(TemplateATestData.invoiceReportDTO);

		response.reset();
		response.setContentType("application/pdf");
		response.setContentLength(byteArray.length);
		response.setHeader("Content-Disposition",
				"attachment;filename=NetMedX_longPdfReport.pdf");

		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.write(byteArray);
		outputStream.flush();
		outputStream.close();

	}

	@RequestMapping(value = "invoiceTestDataSheet1PdfReport.html")
	public void invoiceTestDataSheet1PdfReport(HttpServletRequest Request,
			HttpServletResponse response) throws IOException {

		PDFTemplateA pdfTemplateA = new PDFTemplateA();
		byte[] byteArray = pdfTemplateA.generatePDF(TemplateATestData
				.getInvoiceTestDataSheet1());

		response.reset();
		response.setContentType("application/pdf");
		response.setContentLength(byteArray.length);
		response.setHeader("Content-Disposition",
				"attachment;filename=invoiceTestDataSheet1PdfReport.pdf");

		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.write(byteArray);
		outputStream.flush();
		outputStream.close();

	}

	@RequestMapping(value = "invoiceTestDataSheet2PdfReport.html")
	public void invoiceTestDataSheet2PdfReport(HttpServletRequest Request,
			HttpServletResponse response) throws IOException {

		PDFTemplateA pdfTemplateA = new PDFTemplateA();
		byte[] byteArray = pdfTemplateA.generatePDF(TemplateATestData
				.populateBaseTestDataSheet2());

		response.reset();
		response.setContentType("application/pdf");
		response.setContentLength(byteArray.length);
		response.setHeader("Content-Disposition",
				"attachment;filename=invoiceTestDataSheet2PdfReport.pdf");

		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.write(byteArray);
		outputStream.flush();
		outputStream.close();

	}

	@RequestMapping(value = "invoiceTestDataSheet4PdfReport.html")
	public void invoiceTestDataSheet4PdfReport(HttpServletRequest Request,
			HttpServletResponse response) throws IOException {

		TemplateATestData.populateBaseTestDataSheet4();
		List<InvoiceDetailsDTO> invoiceDetList = new ArrayList<InvoiceDetailsDTO>();

		invoiceDetList.add(TemplateATestData.invoiceDetDtoTest6);
		invoiceDetList.add(TemplateATestData.invoiceDetDtoTest7);

		TemplateATestData.invoiceReportDTO
				.setInvoiceDetailsDTOs(invoiceDetList);

		PDFTemplateA pdfTemplateA = new PDFTemplateA();

		byte[] byteArray = pdfTemplateA
				.generatePDF(TemplateATestData.invoiceReportDTO);

		response.reset();
		response.setContentType("application/pdf");
		response.setContentLength(byteArray.length);
		response.setHeader("Content-Disposition",
				"attachment;filename=invoiceTestDataSheet4PdfReport.pdf");

		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.write(byteArray);
		outputStream.flush();
		outputStream.close();

	}
}
