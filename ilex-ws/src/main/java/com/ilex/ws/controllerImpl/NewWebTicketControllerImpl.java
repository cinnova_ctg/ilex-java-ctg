package com.ilex.ws.controllerImpl;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.controller.NewWebTicketController;
import com.ilex.ws.core.dto.LabelValueDTO;
import com.ilex.ws.core.dto.NewDispatchDTO;
import com.ilex.ws.core.dto.NewWebTicketDTO;
import com.ilex.ws.core.dto.SiteDetailDTO;
import com.ilex.ws.core.logic.NewWebTicketLogic;
import com.ilex.ws.core.logic.SiteSearchLogic;
import com.mind.bean.newjobdb.ResourceLevel;

/**
 * The Class NewWebTicketControllerImpl.
 */
@Controller
@RequestMapping(value = "/newDispatch")
public class NewWebTicketControllerImpl implements NewWebTicketController {

	/** The logger. */
	private static Logger logger = Logger
			.getLogger(NewWebTicketControllerImpl.class);

	/** The new web ticket logic. */
	@Resource
	NewWebTicketLogic newWebTicketLogic;

	/** The site search logic. */
	@Resource
	SiteSearchLogic siteSearchLogic;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ilex.ws.controller.NewWebTicketController#getWebTicketList(org.
	 * springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/getWebTicketList.html", method = RequestMethod.GET)
	@Override
	public ModelAndView getWebTicketList(ModelMap model) {
		List<NewWebTicketDTO> webTickets = newWebTicketLogic.getWebTicketList();
		System.out.println(webTickets);
		return null;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.NewWebTicketController#getWebTicketInfo(com.ilex
	 * .ws.core.dto.NewDispatchDTO, java.lang.Integer, java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/getWebTicketInfo.html", method = RequestMethod.GET)
	@Override
	public ModelAndView getWebTicketInfo(
			@ModelAttribute("newDispatchDTO") NewDispatchDTO newDispatchDTO,
			@RequestParam(value = "msaId") Integer msaId,
			@RequestParam(value = "appendixId") Integer appendixId,
			@RequestParam(value = "jobId") Integer jobId,
			@RequestParam(value = "ticketId") Integer ticketId, ModelMap model) {

		NewDispatchDTO ticketInfo = newWebTicketLogic.getWebTicketInfo(msaId,
				appendixId, jobId, ticketId);
		ticketInfo.setJobId(jobId.longValue());
		ticketInfo.setAppendixId(appendixId.longValue());
		ticketInfo.setTicketId(ticketId.longValue());
		model.addAttribute("newDispatchDTO", ticketInfo);
		List<ResourceLevel> resourceList = newWebTicketLogic.getResourceList(
				Long.valueOf(appendixId), ticketInfo.getCriticality());
		BeanUtils.copyProperties(ticketInfo, newDispatchDTO);
		model.addAttribute("resourceList", resourceList);
		List<LabelValueDTO> states = newWebTicketLogic.getStates();
		model.addAttribute("states", states);
		return new ModelAndView("newDispatch/ticketInfo", model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.NewWebTicketController#getResourceList(java.lang
	 * .Long, java.lang.String, javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@RequestMapping(value = "/getResourceList.html", method = RequestMethod.GET)
	@Override
	public @ResponseBody
	String getResourceList(@RequestParam(value = "appendixId") Long appendixId,
			@RequestParam(value = "criticalityId") String criticalityId,
			HttpServletRequest request, HttpServletResponse response) {
		List<ResourceLevel> resourceList = newWebTicketLogic.getResourceList(
				appendixId, criticalityId);

		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonObject = JSONArray.fromObject(resourceList, jsonConfig);
		return jsonObject.toString();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.NewWebTicketController#saveWebTicketInfo(com.ilex
	 * .ws.core.dto.NewDispatchDTO, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@RequestMapping(value = "/saveWebTicketInfo.html", method = RequestMethod.POST)
	@Override
	public void saveWebTicketInfo(
			@ModelAttribute("newDispatchDTO") NewDispatchDTO newDispatchDTO,
			ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {

		HttpSession session = request.getSession();

		String userId = (String) session.getAttribute("userid");

		String status = newWebTicketLogic.saveWebTicketInfo(newDispatchDTO,
				Long.valueOf(userId));
		try {
			response.getOutputStream().write(status.toString().getBytes());
		} catch (IOException e) {
			logger.error(e);
		}
	}

	/**
	 * Assign site.
	 * 
	 * @param siteListId
	 *            the site list id
	 * @param siteId
	 *            the site id
	 * @param jobId
	 *            the job id
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 */
	@RequestMapping(value = "/assignJobSite.html", method = RequestMethod.GET)
	public void assignSite(
			@RequestParam(value = "siteListId") String siteListId,
			@RequestParam(value = "siteId") String siteId,
			@RequestParam(value = "jobId") String jobId,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();

		String userId = (String) session.getAttribute("userid");
		String status = "";
		try {
			siteSearchLogic.assignSite(siteListId, siteId, jobId, userId);
			status = "success";
		} catch (Exception ex) {
			status = "failed";
		}

		try {
			response.getOutputStream().write(status.toString().getBytes());
		} catch (IOException e) {
			logger.error(e);
		}

	}

	/**
	 * Gets the site details.
	 * 
	 * @param jobId
	 *            the job id
	 * @return the site details
	 */
	@RequestMapping(value = "/jobSiteDetails.html", method = RequestMethod.GET)
	public @ResponseBody
	String getSiteDetails(@RequestParam(value = "jobId") String jobId) {
		SiteDetailDTO siteDetails = siteSearchLogic.getSiteDetails(jobId);
		JsonConfig jsonConfig = new JsonConfig();
		JSONObject jsonObject = JSONObject.fromObject(siteDetails, jsonConfig);
		return jsonObject.toString();
	}

}
