package com.ilex.ws.controllerImpl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.controller.InvSearchController;
import com.ilex.ws.core.dto.AppendixDetailDTO;
import com.ilex.ws.core.dto.AppendixInvSetupDTO;
import com.ilex.ws.core.dto.InvBillToAddressDTO;
import com.ilex.ws.core.dto.InvMasterLineItemDetailsDTO;
import com.ilex.ws.core.dto.InvSearchListDTO;
import com.ilex.ws.core.dto.InvSetupDTO;
import com.ilex.ws.core.logic.AppendixLogic;
import com.ilex.ws.core.logic.InvMasterCountryLogic;
import com.ilex.ws.core.logic.InvMasterStateLogic;
import com.ilex.ws.core.logic.InvSetupLogic;
import com.ilex.ws.core.util.MessageKeyConstant;

@Controller
@RequestMapping(value = "/invoiceSetup")
public class InvSearchControllerImpl extends AbstractIlexBaseController
		implements InvSearchController {

	/** The appendix logic. */
	@Resource
	AppendixLogic appendixLogic;

	/** The inv setup logic. */
	@Resource
	InvSetupLogic invSetupLogic;

	/** The inv master state logic. */
	@Resource
	InvMasterStateLogic invMasterStateLogic;

	/** The inv master country logic. */
	@Resource
	InvMasterCountryLogic invMasterCountryLogic;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#searchClientList(java.lang.
	 * String, int, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "/searchclientlist.html", method = RequestMethod.POST)
	@Override
	public ModelAndView searchClientList(
			@RequestParam(value = "client") String clientName,
			@RequestParam(value = "pageIndex") int pageIndex, ModelMap model,
			HttpServletRequest request) {
		initExceptionHandler("error", "command", model,
				MessageKeyConstant.RECORD_NOT_FOUND);
		InvSearchListDTO invSearchListDTO = new InvSearchListDTO();
		invSearchListDTO = invSetupLogic.getClientListForAppendixInvSetup(
				clientName.trim(), pageIndex);

		invSearchListDTO.setClientSearchKeyword(clientName);
		model.addAttribute("invSearchListDTO", invSearchListDTO);
		return new ModelAndView("IM/invSetup/searchResult", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.MasterBTMController#searchClientListGet(java.lang
	 * .String, int, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@RequestMapping(value = "/searchclientlist.html", method = RequestMethod.GET)
	@Override
	public ModelAndView searchClientListGet(
			@RequestParam(value = "client") String clientName,
			@RequestParam(value = "pageIndex") int pageIndex, ModelMap model,
			HttpServletRequest request) {
		initExceptionHandler("error", "command", model,
				MessageKeyConstant.RECORD_NOT_FOUND);
		InvSearchListDTO invSearchListDTO = new InvSearchListDTO();
		invSearchListDTO = invSetupLogic.getClientListForAppendixInvSetup(
				clientName.trim(), pageIndex);

		invSearchListDTO.setClientSearchKeyword(clientName);
		model.addAttribute("invSearchListDTO", invSearchListDTO);
		return new ModelAndView("IM/invSetup/searchResultPage", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#editClientProfile(java.lang
	 * .String, com.ilex.ws.core.dto.InvBillToAddressDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/editClientProfile.html", method = RequestMethod.GET)
	public ModelAndView editClientProfile(
			@RequestParam(value = "id") String clientId,
			@ModelAttribute("invBillToAddressDTO") InvBillToAddressDTO invBillToAddressDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("error", "command", model);
		InvBillToAddressDTO invBillToAddress = invSetupLogic
				.getBillToAddressDetails(Integer.valueOf(clientId));
		BeanUtils.copyProperties(invBillToAddress, invBillToAddressDTO);
		model.addAttribute("stateList", invMasterStateLogic.getStateList());
		model.addAttribute("countryList",
				invMasterCountryLogic.getCountryList());

		return new ModelAndView("IM/invSetup/editClientProfile", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#updateClientProfile(com.ilex
	 * .ws.core.dto.InvBillToAddressDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/updateClientProfile.html", method = RequestMethod.POST)
	public ModelAndView updateClientProfile(
			@ModelAttribute("invBillToAddressDTO") InvBillToAddressDTO invBillToAddressDTO,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		initExceptionHandler("error", "command", model);
		invSetupLogic.updateClientProfile(invBillToAddressDTO);

		InvBillToAddressDTO invBillToAddress = invSetupLogic
				.getBillToAddressDetails(invBillToAddressDTO
						.getInvBillToAddressId());
		BeanUtils.copyProperties(invBillToAddress, invBillToAddressDTO);
		model.addAttribute("stateList", invMasterStateLogic.getStateList());
		model.addAttribute("countryList",
				invMasterCountryLogic.getCountryList());
		messageUtils.handleSuccess(request, model,
				MessageKeyConstant.CLIENT_UPDATE_SUCCESS);

		return new ModelAndView("IM/invSetup/editClientProfile", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#editInvoiceProfile(java.lang
	 * .String, com.ilex.ws.core.dto.InvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/editInvoiceProfile.html", method = RequestMethod.GET)
	public ModelAndView editInvoiceProfile(
			@RequestParam(value = "id") String invSetupId,
			@ModelAttribute("invSetupDTO") InvSetupDTO invSetupDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("error", "command", model);
		InvSetupDTO invSetup = invSetupLogic.getInvSetupDetails(Integer
				.valueOf(invSetupId));
		BeanUtils.copyProperties(invSetup, invSetupDTO);
		return new ModelAndView("IM/invSetup/editInvoiceProfile", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#updateInvoiceProfile(com.ilex
	 * .ws.core.dto.InvSetupDTO, org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/updateInvoiceProfile.html", method = RequestMethod.POST)
	public ModelAndView updateInvoiceProfile(
			@ModelAttribute("invSetupDTO") InvSetupDTO invSetupDTO,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		initExceptionHandler("error", "command", model);
		invSetupLogic.updateInvoiceProfile(invSetupDTO);
		InvSetupDTO invSetup = invSetupLogic.getInvSetupDetails(invSetupDTO
				.getInvoiceSetupId());
		BeanUtils.copyProperties(invSetup, invSetupDTO);
		messageUtils.handleSuccess(request, model,
				MessageKeyConstant.CLIENT_UPDATE_SUCCESS);
		return new ModelAndView("IM/invSetup/editInvoiceProfile", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#editClientReqInvInfo(java.lang
	 * .String, com.ilex.ws.core.dto.AppendixInvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/editClientReqInvInfo.html", method = RequestMethod.GET)
	public ModelAndView editClientReqInvInfo(
			@RequestParam(value = "id") String invSetupId,
			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model) {
		AppendixInvSetupDTO appendixInvSetup = new AppendixInvSetupDTO();
		appendixInvSetup.setInvSetupDetails(invSetupLogic
				.getInvSetupDetails(Integer.valueOf(invSetupId)));
		appendixInvSetup.setAdditionalInformationList(invSetupLogic
				.getAdditionalInfo(appendixInvSetup.getInvSetupDetails()));

		BeanUtils.copyProperties(appendixInvSetup, appendixInvSetupDTO);

		return new ModelAndView("IM/invSetup/editClientReqInvInfo", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#updateClientReqInvInfo(com.
	 * ilex.ws.core.dto.AppendixInvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/updateClientReqInvInfo.html", method = RequestMethod.POST)
	public ModelAndView updateClientReqInvInfo(
			@ModelAttribute("appendixInvSetupDTO") AppendixInvSetupDTO appendixInvSetupDTO,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		AppendixInvSetupDTO appendixInvSetup = new AppendixInvSetupDTO();
		invSetupLogic.updateClientReqInfo(appendixInvSetupDTO);
		appendixInvSetup.setInvSetupDetails(invSetupLogic
				.getInvSetupDetails(appendixInvSetupDTO.getInvSetupDetails()
						.getInvoiceSetupId()));
		appendixInvSetup.setAdditionalInformationList(invSetupLogic
				.getAdditionalInfo(appendixInvSetup.getInvSetupDetails()));

		BeanUtils.copyProperties(appendixInvSetup, appendixInvSetupDTO);
		messageUtils.handleSuccess(request, model,
				MessageKeyConstant.CLIENT_UPDATE_SUCCESS);

		return new ModelAndView("IM/invSetup/editClientReqInvInfo", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#editLineItemDetails(java.lang
	 * .String, com.ilex.ws.core.dto.InvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/editLineItemDetails.html", method = RequestMethod.GET)
	public ModelAndView editLineItemDetails(
			@RequestParam(value = "id") String invSetupId,
			@ModelAttribute("invSetupDTO") InvSetupDTO invSetupDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("error", "command", model);
		InvSetupDTO invSetup = invSetupLogic.getInvSetupDetails(Integer
				.valueOf(invSetupId));
		AppendixDetailDTO appendixDetailDTO = appendixLogic
				.getAppendixDetail(invSetup.getAppendixId());
		List<InvMasterLineItemDetailsDTO> invMasterLineItemDetailsDTOList = invSetupLogic
				.getLineItemDetails(appendixDetailDTO);
		model.addAttribute("invMasterLineItemDetailsDTOList",
				invMasterLineItemDetailsDTOList);

		BeanUtils.copyProperties(invSetup, invSetupDTO);
		return new ModelAndView("IM/invSetup/editLineItemDetails", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#updateLineItemDetails(com.ilex
	 * .ws.core.dto.InvSetupDTO, org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/updateLineItemDetails.html", method = RequestMethod.POST)
	public ModelAndView updateLineItemDetails(
			@ModelAttribute("invSetupDTO") InvSetupDTO invSetupDTO,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		initExceptionHandler("error", "command", model);
		invSetupLogic.updateLineItemDetails(invSetupDTO);
		InvSetupDTO invSetup = invSetupLogic.getInvSetupDetails(invSetupDTO
				.getInvoiceSetupId());
		BeanUtils.copyProperties(invSetup, invSetupDTO);
		AppendixDetailDTO appendixDetailDTO = appendixLogic
				.getAppendixDetail(invSetup.getAppendixId());
		List<InvMasterLineItemDetailsDTO> invMasterLineItemDetailsDTOList = invSetupLogic
				.getLineItemDetails(appendixDetailDTO);
		model.addAttribute("invMasterLineItemDetailsDTOList",
				invMasterLineItemDetailsDTOList);
		messageUtils.handleSuccess(request, model,
				MessageKeyConstant.CLIENT_UPDATE_SUCCESS);
		return new ModelAndView("IM/invSetup/editLineItemDetails", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#editSummarySheet(java.lang.
	 * String, com.ilex.ws.core.dto.InvSetupDTO,
	 * org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap)
	 */
	@Override
	@RequestMapping(value = "/editSummarySheet.html", method = RequestMethod.GET)
	public ModelAndView editSummarySheet(
			@RequestParam(value = "id") String invSetupId,
			@ModelAttribute("invSetupDTO") InvSetupDTO invSetupDTO,
			BindingResult result, ModelMap model) {
		initExceptionHandler("error", "command", model);
		InvSetupDTO invSetup = invSetupLogic.getInvSetupDetails(Integer
				.valueOf(invSetupId));
		BeanUtils.copyProperties(invSetup, invSetupDTO);
		return new ModelAndView("IM/invSetup/editSummarySheet", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#updateSummarySheet(com.ilex
	 * .ws.core.dto.InvSetupDTO, org.springframework.validation.BindingResult,
	 * org.springframework.ui.ModelMap, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/updateSummarySheet.html", method = RequestMethod.POST)
	public ModelAndView updateSummarySheet(
			@ModelAttribute("invSetupDTO") InvSetupDTO invSetupDTO,
			BindingResult result, ModelMap model, HttpServletRequest request) {
		initExceptionHandler("error", "command", model);
		invSetupLogic.updateSummarySheetDetails(invSetupDTO);
		InvSetupDTO invSetup = invSetupLogic.getInvSetupDetails(invSetupDTO
				.getInvoiceSetupId());
		BeanUtils.copyProperties(invSetup, invSetupDTO);
		messageUtils.handleSuccess(request, model,
				MessageKeyConstant.CLIENT_UPDATE_SUCCESS);
		return new ModelAndView("IM/invSetup/editSummarySheet", model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.controller.InvSetupController#deleteInvSetupDetails(java.
	 * lang.String, org.springframework.ui.ModelMap,
	 * javax.servlet.http.HttpServletRequest)
	 */
	@Override
	@RequestMapping(value = "/deleteClientProfile.html", method = RequestMethod.GET)
	public ModelAndView deleteInvSetupDetails(
			@RequestParam(value = "id") String invBillToAddressId,
			ModelMap model, HttpServletRequest request) {
		initExceptionHandler("error", "command", model,
				MessageKeyConstant.RECORD_NOT_FOUND);
		invSetupLogic
				.deleteInvSetupDetails(Integer.valueOf(invBillToAddressId));
		messageUtils.handleSuccess(request, model,
				MessageKeyConstant.CLIENT_DELETE_SUCCESS);
		return new ModelAndView("error");

	}
}
