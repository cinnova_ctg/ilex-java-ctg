package com.ilex.ws.controllerImpl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ilex.ws.controller.DemoServiceNowIntegration;
import com.ilex.ws.core.dto.IlexTicketDTO;
import com.ilex.ws.core.dto.ServiceNowTicketDTO;
import com.ilex.ws.core.logic.DemoServiceNowIntegrationLogic;

@Controller
@RequestMapping("/serviceNow")
public class DemoServiceNowIntegrationImpl implements DemoServiceNowIntegration {

	@Resource
	DemoServiceNowIntegrationLogic demoServiceNowIntegrationLogic;

	@Override
	@RequestMapping(value = "/incident.html", method = RequestMethod.GET)
	public ModelAndView getIncidentCreatePage(
			@ModelAttribute("serviceNowTicketDTO") ServiceNowTicketDTO serviceNowTicketDTO,
			ModelMap model, HttpServletRequest request) {

		List<IlexTicketDTO> ilexTickets = demoServiceNowIntegrationLogic
				.getIncidentTicketDetails();
		model.addAttribute("ilexTickets", ilexTickets);

		return new ModelAndView("/serviceNow/ticketCreate", model);

	}

	@Override
	@RequestMapping(value = "/createServiceNowIncident.html", method = RequestMethod.POST)
	public @ResponseBody
	String createServiceNowIncident(
			@ModelAttribute("serviceNowTicketDTO") ServiceNowTicketDTO serviceNowTicketDTO,
			ModelMap model, HttpServletRequest request) {
		serviceNowTicketDTO.setServiceNowTicket(false);
		String status = demoServiceNowIntegrationLogic
				.insertTicket(serviceNowTicketDTO);
		return status;

	}

	@Override
	@RequestMapping(value = "/getTicketDetails.html", method = RequestMethod.GET)
	public @ResponseBody
	String getIncidentTicketDetails() {
		List<IlexTicketDTO> ilexTickets = demoServiceNowIntegrationLogic
				.getIncidentTicketDetails();
		JSONArray array = JSONArray.fromObject(ilexTickets);

		return array.toString();

	}

	@Override
	@RequestMapping(value = "/serviceNowTickets.html", method = RequestMethod.GET)
	public ModelAndView getServiceTickets(
			@ModelAttribute("serviceNowTicketDTO") ServiceNowTicketDTO serviceNowTicketDTO,
			ModelMap model, HttpServletRequest request) {

		List<IlexTicketDTO> ilexTickets = demoServiceNowIntegrationLogic
				.getServiceNowTicketDetails();
		model.addAttribute("ilexTickets", ilexTickets);

		return new ModelAndView("/serviceNow/serviceNowTickets", model);

	}

	@Override
	@RequestMapping(value = "/getServiceNowTicketDetails.html", method = RequestMethod.GET)
	public @ResponseBody
	String getServiceNowTicketDetails() {
		List<IlexTicketDTO> ilexTickets = demoServiceNowIntegrationLogic
				.getServiceNowTicketDetails();
		JSONArray array = JSONArray.fromObject(ilexTickets);

		return array.toString();

	}

}
