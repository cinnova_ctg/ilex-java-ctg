package com.ilex.ws.util;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.springframework.web.servlet.view.document.AbstractJExcelView;

import com.ilex.ws.core.dto.HDPendingAckTableDTO;
import com.ilex.ws.core.dto.HDTableHeaderDTO;
import com.ilex.ws.core.dto.HDTableParentDTO;
import com.ilex.ws.core.util.HDTables;

/**
 * The Class ExportCubeToExcel.
 */
public class ExportCubeToExcel extends AbstractJExcelView {
	/** The Constant logger. */
	public static final Logger logger = Logger
			.getLogger(ExportCubeToExcel.class);
	private static final Object HDPendingAckTableDTO = null;

	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			WritableWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userid");
		Map<String, Object> cubeInfo = (Map<String, Object>) model
				.get("cubeInfo");
		String tableId = (String) cubeInfo.get("tableId");
		String tickets = (String) cubeInfo.get("tickets");
		HDTableParentDTO tableParentDTO = (HDTableParentDTO) cubeInfo
				.get("tableParentDTO");
		WritableSheet sheet = workbook.createSheet("sheet 1", 0);
		createCubeHeaderAndBody(sheet, tableId, tableParentDTO, tickets);
		createOutputFilename(tableId, userId, response);
	}

	/**
	 * Creates the cube header and body.
	 * 
	 * @param sheet
	 *            the sheet
	 * @param tableId
	 *            the cube id
	 * @param HDTableParentDTO
	 *            the table parent dto
	 * @param cubeJobsList
	 *            the cube jobs list
	 */
	public void createCubeHeaderAndBody(WritableSheet sheet, String tableId,
			HDTableParentDTO HDTableParentDTO, String cubeJobsList) {
		HDTables cubeType = HDTables.getTableFromTableId(tableId);
		switch (cubeType) {
		case HD_PENDING_ACNOWLEDGEMENT_CUST:
		case HD_PENDING_ACNOWLEDGEMENT_ALERT:
		case HD_PENDING_ACNOWLEDGEMENT_MONITOR:

			createPendingAcknowledgementCubeHeader(sheet,
					HDTableParentDTO.getHdPendingAckDTO());
			createPendingAcknowledgementCubeBody(sheet, cubeJobsList);
			break;

		case HD_WIP:
		case HD_WIP_OVERDUE:
			createWIPTableHeader(sheet, HDTableParentDTO.getHdWIPTableDTO());
			createWIPTableBody(sheet, cubeJobsList);
			break;

		case HD_MONITOR_DATE_TIME:
			createAddToMonitorCubeHeader(sheet,
					HDTableParentDTO.getHdWIPTableDTO());
			createAddtoMonitorBody(sheet, cubeJobsList);
			break;
		case HD_PENDING_ACNOWLEDGEMENT_MONITOR_TIER2:
			createDataTierCubeHeader(sheet,
					HDTableParentDTO.getHdPendingAckDTO());
			createTierToDataCubeBody(sheet, cubeJobsList);
			break;
		default:
			break;
		}

	}

	private void createPendingAcknowledgementCubeBody(WritableSheet sheet,
			String tickets) {

		JSONObject jsonObject = JSONObject.fromObject(tickets);
		DynaBean dynaBean = (DynaBean) JSONObject.toBean(jsonObject);
		List<DynaBean> ticketList = (List<DynaBean>) dynaBean.get("aaData");

		int rowNumber = 1;
		for (DynaBean ticket : ticketList) {
			List<String> rowValues = new ArrayList<>();
			HDPendingAckTableDTO hdPendingAckTableDTO = new HDPendingAckTableDTO();
			try {
				BeanUtils.copyProperties(hdPendingAckTableDTO, ticket);
			} catch (IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}
			rowValues.add(hdPendingAckTableDTO.getOpenedDate());
			rowValues.add(hdPendingAckTableDTO.getSiteNumber());
			rowValues.add(Jsoup.parse(hdPendingAckTableDTO.getState()).text());
			rowValues.add(hdPendingAckTableDTO.getSource());
			rowValues.add(hdPendingAckTableDTO.getCustomer());
			addRowsToSheet(sheet, rowNumber, rowValues);
			rowNumber++;
		}

	}

	// add tier 2 data header

	private void createDataTierCubeHeader(WritableSheet sheet,
			HDTableHeaderDTO hdPendingAckDTO) {
		int rowNumber = 0;
		List<String> rowValues = new ArrayList<>();
		rowValues.add("Customer");
		rowValues.add("Site#");
		rowValues.add("State");
		rowValues.add("Opened");

		addRowsToSheet(sheet, rowNumber, rowValues);

	}

	private void createTierToDataCubeBody(WritableSheet sheet, String tickets) {

		JSONObject jsonObject = JSONObject.fromObject(tickets);
		DynaBean dynaBean = (DynaBean) JSONObject.toBean(jsonObject);
		List<DynaBean> ticketList = (List<DynaBean>) dynaBean.get("aaData");

		int rowNumber = 1;
		for (DynaBean ticket : ticketList) {
			List<String> rowValues = new ArrayList<>();
			HDPendingAckTableDTO hdPendingAckTableDTO = new HDPendingAckTableDTO();
			try {
				BeanUtils.copyProperties(hdPendingAckTableDTO, ticket);
			} catch (IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}

			rowValues.add(hdPendingAckTableDTO.getCustomer());
			rowValues.add(hdPendingAckTableDTO.getSiteNumberDisplay());
			rowValues.add(Jsoup.parse(hdPendingAckTableDTO.getState()).text());
			rowValues.add(hdPendingAckTableDTO.getOpenedDate());

			addRowsToSheet(sheet, rowNumber, rowValues);
			rowNumber++;
		}

	}

	private void createPendingAcknowledgementCubeHeader(WritableSheet sheet,
			HDTableHeaderDTO hdPendingAckDTO) {
		int rowNumber = 0;
		List<String> rowValues = new ArrayList<>();
		rowValues.add("Opened");
		rowValues.add("Site#");
		rowValues.add("State");
		rowValues.add("Source");
		rowValues.add("Customer");
		addRowsToSheet(sheet, rowNumber, rowValues);

	}

	private void createWIPTableHeader(WritableSheet sheet,
			HDTableHeaderDTO hdPendingAckDTO) {
		int rowNumber = 0;
		List<String> rowValues = new ArrayList<>();
		rowValues.add("Priority");
		rowValues.add("Opened");
		rowValues.add("Site#");
		rowValues.add("Status");
		rowValues.add("Next Action");
		rowValues.add("Action Due");
		rowValues.add("Owner");
		rowValues.add("Short Description");
		rowValues.add("Customer");
		rowValues.add("Source");
		addRowsToSheet(sheet, rowNumber, rowValues);

	}

	private void createAddToMonitorCubeHeader(WritableSheet sheet,
			HDTableHeaderDTO hdPendingAckDTO) {
		int rowNumber = 0;
		List<String> rowValues = new ArrayList<>();
		rowValues.add("Ticket#");
		rowValues.add("Site#");
		rowValues.add("State");
		rowValues.add("Time Added");
		rowValues.add("Next Action");
		rowValues.add("Opened");

		addRowsToSheet(sheet, rowNumber, rowValues);

	}

	private void createWIPTableBody(WritableSheet sheet, String tickets) {

		JSONObject jsonObject = JSONObject.fromObject(tickets);
		DynaBean dynaBean = (DynaBean) JSONObject.toBean(jsonObject);
		List<DynaBean> ticketList = (List<DynaBean>) dynaBean.get("aaData");

		int rowNumber = 1;
		for (DynaBean ticket : ticketList) {
			List<String> rowValues = new ArrayList<>();

			rowValues.add((String) ticket.get("priority"));
			rowValues.add((String) ticket.get("openedDate"));
			rowValues.add((String) ticket.get("siteNumber"));
			rowValues.add((String) ticket.get("status"));
			rowValues.add((String) ticket.get("nextAction"));
			rowValues.add((String) ticket.get("nextActionDueDate"));
			rowValues.add((String) ticket.get("cca_tss"));
			rowValues.add((String) ticket.get("shortDescription"));
			rowValues.add((String) ticket.get("customer"));
			rowValues.add((String) ticket.get("source"));

			addRowsToSheet(sheet, rowNumber, rowValues);
			rowNumber++;
		}

	}

	// Add to Monitor table data
	private void createAddtoMonitorBody(WritableSheet sheet, String tickets) {

		JSONObject jsonObject = JSONObject.fromObject(tickets);
		DynaBean dynaBean = (DynaBean) JSONObject.toBean(jsonObject);
		List<DynaBean> ticketList = (List<DynaBean>) dynaBean.get("aaData");

		int rowNumber = 1;
		for (DynaBean ticket : ticketList) {
			List<String> rowValues = new ArrayList<>();

			rowValues.add((String) ticket.get("ilexNumber"));
			rowValues.add((String) ticket.get("siteNumber"));
			rowValues.add((String) ticket.get("state"));
			rowValues.add((String) ticket.get("dateDiff"));
			rowValues.add((String) ticket.get("nextActionLabel"));
			rowValues.add((String) ticket.get("openedDate"));
			//

			addRowsToSheet(sheet, rowNumber, rowValues);
			rowNumber++;
		}

	}

	/**
	 * Adds the rows to sheet.
	 * 
	 * @param sheet
	 *            the sheet
	 * @param rowNumber
	 *            the row number
	 * @param rowValues
	 *            the row values
	 */
	private void addRowsToSheet(WritableSheet sheet, int rowNumber,
			List<String> rowValues) {
		int cellNumber = 0;
		for (String rowValue : rowValues) {
			try {
				sheet.addCell(new Label(cellNumber++, rowNumber, rowValue));
			} catch (RowsExceededException e) {
				logger.error(e.getMessage());
			} catch (WriteException e) {
				logger.error(e.getMessage());
			}
		}

	}

	/**
	 * Creates the response export excel filename.
	 * 
	 * @param cubeId
	 *            the cube id
	 * @param userId
	 *            the user id
	 * @param response
	 *            the response
	 */
	public void createOutputFilename(String cubeId, String userId,
			HttpServletResponse response) {
		SimpleDateFormat simpledateFormat = new SimpleDateFormat(
				"yyyyMMdd_hhmm");
		Calendar calendar = Calendar.getInstance();
		String currentDate = simpledateFormat.format(calendar.getTime());
		String filename = cubeId + "_" + userId + "_" + currentDate;
		response.setHeader("Content-Disposition", "attachment; filename=\""
				+ filename + ".xls\"");
	}
}
