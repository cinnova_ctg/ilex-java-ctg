package com.ilex.ws.util;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;

public class WebServiceHitInterceptor implements MethodInterceptor {

	private static Logger logger = Logger
			.getLogger(WebServiceHitInterceptor.class);

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		return invocation.proceed();
	}
}
