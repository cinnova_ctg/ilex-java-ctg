package com.ilex.ws.util;

import java.io.Serializable;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.ilex.ws.core.dto.HDSessionDTO;
import com.ilex.ws.core.logic.HDActiveTicketLogic;
import com.ilex.ws.core.util.HDTicketPersistUtils;

@WebListener
public class HDSessionDestroyListener implements Serializable,
		HttpSessionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	HDActiveTicketLogic hdActiveTicketLogic;

	@Override
	public void sessionCreated(HttpSessionEvent sessionEvent) {
		WebApplicationContextUtils
				.getRequiredWebApplicationContext(
						sessionEvent.getSession().getServletContext())
				.getAutowireCapableBeanFactory().autowireBean(this);

	}

	@Override
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {

		String userId = (String) sessionEvent.getSession().getAttribute(
				"userid");
		String sessionId = sessionEvent.getSession().getId();
		HDTicketPersistUtils.removeTicket(sessionId);
		HDSessionDTO hdSessionDTO = new HDSessionDTO();
		hdSessionDTO.setUserId(userId);
		hdSessionDTO.setSessionId(sessionId);
		hdActiveTicketLogic.removeTicketLock(hdSessionDTO);

	}
}
