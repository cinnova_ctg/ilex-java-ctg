package com.ilex.ws.util;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.ilex.ws.core.dto.HDSessionDTO;
import com.ilex.ws.core.logic.HDActiveTicketLogic;

@WebListener
public class HdContextLoaderListener implements Serializable,
		ServletContextListener {

	private static final Logger logger = Logger
			.getLogger(HdContextLoaderListener.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	HDActiveTicketLogic hdActiveTicketLogic;

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {

	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		WebApplicationContextUtils
				.getRequiredWebApplicationContext(
						servletContextEvent.getServletContext())
				.getAutowireCapableBeanFactory().autowireBean(this);

		String domainName = "";
		try {
			domainName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			logger.error(e.getMessage(), e);
		}
		HDSessionDTO hdSessionDTO = new HDSessionDTO();
		hdSessionDTO.setDomainName(domainName);
		hdActiveTicketLogic.removeTicketLock(hdSessionDTO);

	}

}
