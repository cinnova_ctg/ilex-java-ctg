package com.ilex.ws.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.activation.DataSource;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;

import com.ilex.ws.core.exception.IlexSystemException;
import com.ilex.ws.core.util.MessageKeyConstant;

public class IlexFileUtils {

	private static final String ILEX_TEMP_PATH = System
			.getProperty("ilex.temp.path");

	private static final int SIZE_THRESHOLD = Integer.MAX_VALUE;

	public static FileItem getFileItem(Attachment attachment) {

		try {

			DataSource attachmentDS = attachment.getDataHandler()
					.getDataSource();

			InputStream fileIS = attachmentDS.getInputStream();

			String fileName = attachment.getContentDisposition().getParameter(
					"filename");

			if (StringUtils.isEmpty(fileName)) {
				return null;
			}
			fileName = new File(fileName).getName();
			fileName = fileName.replaceAll(".bin", ".jpg");
			String contentType = attachmentDS.getContentType();

			FileItem fileItem = new DiskFileItem(null, contentType, true,
					fileName, SIZE_THRESHOLD, new File(ILEX_TEMP_PATH));

			IOUtils.copy(fileIS, fileItem.getOutputStream());

			return fileItem;

		} catch (IOException e) {
			throw new IlexSystemException(
					MessageKeyConstant.ILEX_SYSTEM_EXCEPTION, e);
		}

	}

}
