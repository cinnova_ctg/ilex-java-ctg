package com.ilex.ws.util;

import java.util.concurrent.ConcurrentHashMap;

import com.ilex.ws.core.util.RandomNumberGenerator;

public class StagingUtil {

	private static ConcurrentHashMap<String, Object> persistMap = new ConcurrentHashMap<String, Object>();

	/**
	 * Persist objects in memory before final save (i.e. staging objects). This
	 * method should be used only when first call to persist is made. All
	 * further calls for same objects should use #persistObject(String name,
	 * Object obj, String uniqueId)
	 * 
	 * @param name
	 *            the name of the bean to persist
	 * @param obj
	 *            the object to persist
	 * @return the uniqueId for persisting object.
	 */
	public static String persist(String name, Object obj) {

		String uniqueId = RandomNumberGenerator.getNDigitRandomNumber(4);

		persist(name, obj, uniqueId);

		return uniqueId;
	}

	/**
	 * Persist objects in memory before final save.
	 * 
	 * @param name
	 *            the name
	 * @param obj
	 *            the obj
	 * @param uniqueId
	 *            the unique id
	 */
	public static void persist(String name, Object obj, String uniqueId) {

		String key = getKey(name, uniqueId);
		persistMap.put(key, obj);

	}

	/**
	 * Gets the object for staging memory.
	 * 
	 * @param name
	 *            the name
	 * @param uniqueId
	 *            the unique id
	 * @return the object
	 */
	public static Object get(String name, String uniqueId) {
		String key = getKey(name, uniqueId);
		return persistMap.get(key);
	}

	/**
	 * Removes the staging object. Call this method once staging object is no
	 * longer required.
	 * 
	 * @param name
	 *            the name
	 * @param uniqueId
	 *            the unique id
	 */
	public static void remove(String name, String uniqueId) {
		String key = getKey(name, uniqueId);
		persistMap.remove(key);
	}

	/**
	 * Gets key for using in persistent object
	 * 
	 * @param name
	 * @param uniqueId
	 * @return
	 */
	private static String getKey(String name, String uniqueId) {
		String key = name + "_" + uniqueId;
		return key;
	}

}
