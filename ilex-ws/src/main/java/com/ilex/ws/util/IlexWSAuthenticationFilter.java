package com.ilex.ws.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Component;

import com.ilex.ws.core.dao.LoginDAO;
import com.mind.masterSchedulerTask.MasterSchedulerTaskUtil;

@Component
public class IlexWSAuthenticationFilter implements Filter {

    @Resource
    LoginDAO loginDAO;
    private static final Logger logger = Logger
            .getLogger(IlexWSAuthenticationFilter.class);

    @Override
    public void destroy() {
        logger.debug(this.getClass().getName()
                + ": Start Authencation  filter.");

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        HttpSession session = httpRequest.getSession();
        if (session.getAttribute("userid") == null) {

            // String username = httpRequest.getRemoteUser();
            // LoginForm formdetail = loginDAO.getUserInfo(username);
            // String userId = formdetail.getLogin_user_id();
            String userId = httpRequest.getParameter("userid");
            if (userId == null || "".equals(userId)) {
                httpResponse.sendRedirect(httpRequest.getScheme() + "://"
                        + httpRequest.getServerName() + ":"
                        + httpRequest.getServerPort() + "/Ilex");
                return;
            }

            String tokenn = httpRequest.getParameter("token");

            String jsonString = MasterSchedulerTaskUtil.sendAPIRequestForLogin(userId, tokenn);

            if (jsonString == "") {
                httpResponse.sendRedirect(request.getScheme() + "://"
                        + request.getServerName() + ":"
                        + request.getServerPort() + "/Ilex");
            }

            session.setAttribute("userid", userId);

            session.setAttribute("PC", loginDAO.checkUserRoleByRole(userId, "PC"));

            session.setAttribute("PAM", loginDAO.checkUserRoleByRole(userId, "PAM"));
        }
        chain.doFilter(request, response);

    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        logger.debug(this.getClass().getName() + ": Stop Authencation  filter.");

    }

    public final static String CONTENT_TYPE_KEY = "Content-type";
    public final static String CONTENT_TYPE_VALUE = "application/json";
    public final static String POST_REQUEST = "POST";
    public final static String PROTOCOL = "TLSv1";

	// public final static String PATH =
    // "ms/project/template_task/auto_complete/";
    // static Logger logger = Logger.getLogger(MasterSchedulerTaskUtil.class);
}
