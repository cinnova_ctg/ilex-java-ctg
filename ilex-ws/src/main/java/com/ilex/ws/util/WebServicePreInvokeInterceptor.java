package com.ilex.ws.util;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

public class WebServicePreInvokeInterceptor extends
		AbstractPhaseInterceptor<Message> {

	public WebServicePreInvokeInterceptor() {
		super(Phase.PRE_INVOKE);
	}

	@Override
	public void handleMessage(Message message) {
		// HttpServletRequest request = (HttpServletRequest) message
		// .get(AbstractHTTPDestination.HTTP_REQUEST);
		// LoginInfo loginInfo = new LoginInfo();
		// loginInfo.setIpAddress(request.getRemoteAddr());
		//
		// WebApiHitThreadLocal.set(loginInfo);

	}

	public void handleFault(Message messageParam) {
	}

}
