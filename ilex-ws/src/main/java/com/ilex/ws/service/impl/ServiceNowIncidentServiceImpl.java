package com.ilex.ws.service.impl;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.springframework.stereotype.Service;

import com.ilex.ws.core.dto.IlexTicketCreateInputDTO;
import com.ilex.ws.core.dto.TicketCreateResponse;
import com.ilex.ws.core.logic.DemoServiceNowIntegrationLogic;
import com.ilex.ws.service.ServiceNowIncidentService;
import com.ilex.ws.util.MessageUtils;

@WebService
@Service
public class ServiceNowIncidentServiceImpl implements ServiceNowIncidentService {

	@Resource
	WebServiceContext jaxwsContext;

	@Resource
	DemoServiceNowIntegrationLogic demoServiceNowIntegrationLogic;

	@Resource
	MessageUtils messageUtils;

	@WebResult(name = "insertResponse")
	@WebMethod
	@Override
	public TicketCreateResponse createTicket(
			@WebParam(name = "category") String category,
			@WebParam(name = "incidentState") String incidentState,
			@WebParam(name = "causedBy") String causedBy,
			@WebParam(name = "shortDescription") String shortDescription,
			@WebParam(name = "active") String active,
			@WebParam(name = "ticketNumber") String ticketNumber,
			@WebParam(name = "sysId") String sysId) {
		try {
			IlexTicketCreateInputDTO ilexTicketCreateInputDTO = new IlexTicketCreateInputDTO();

			ilexTicketCreateInputDTO.setActive(active);
			ilexTicketCreateInputDTO.setCategory(category);
			ilexTicketCreateInputDTO.setIncidentState(incidentState);
			ilexTicketCreateInputDTO.setShortDescription(shortDescription);
			ilexTicketCreateInputDTO.setSysId(sysId);
			ilexTicketCreateInputDTO.setTicketNumber(ticketNumber);
			String ticketId = demoServiceNowIntegrationLogic
					.createServiceNowTicket(ilexTicketCreateInputDTO);
			TicketCreateResponse response = new TicketCreateResponse();
			response.setCorelationId(ticketId);
			response.setStatus("Ticket Created Successfully");
			return response;
		} catch (Exception ex) {
			HttpServletRequest request = null;
			String errMsg = "";
			if (jaxwsContext.getMessageContext() != null) {
				request = (HttpServletRequest) jaxwsContext.getMessageContext()
						.get(MessageContext.SERVLET_REQUEST);
				errMsg = messageUtils.handleException(ex, request);
			}
			throw new RuntimeException(errMsg);
		}
	}

	@WebResult(name = "insertResponse")
	@WebMethod
	@Override
	public TicketCreateResponse addComment(
			@WebParam(name = "comments") String comments,
			@WebParam(name = "workNotes") String workNotes,
			@WebParam(name = "ticketNumber") String ticketNumber) {

		demoServiceNowIntegrationLogic.addComment(comments, workNotes,
				ticketNumber);
		TicketCreateResponse response = new TicketCreateResponse();
		response.setStatus("Comment added Successfully");
		return response;

	}

}
