package com.ilex.ws.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import net.sf.json.JSONObject;

import org.apache.commons.fileupload.FileItem;
import org.apache.cxf.jaxrs.ext.MessageContext;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ilex.ws.core.dto.POImageUploadDTO;
import com.ilex.ws.core.dto.POImageUploadRequirementDTO;
import com.ilex.ws.core.exception.IlexBusinessException;
import com.ilex.ws.core.logic.POImageUploadLogic;
import com.ilex.ws.core.util.MessageKeyConstant;
import com.ilex.ws.service.POImageUploadService;
import com.ilex.ws.util.IlexFileUtils;
import com.ilex.ws.util.MessageUtils;

@Service
@Path("/ilexmobile")
public class POImageUploadServiceImpl implements POImageUploadService {
	private static final Logger logger = Logger
			.getLogger(POImageUploadServiceImpl.class);
	@Resource
	POImageUploadLogic poImageUploadLogic;
	@Resource
	MessageUtils messageUtils;

	@Override
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/getPOImageUploadRequirement")
	public String getPOImageUploadRequirement(
			@QueryParam("lm_po_id") String poId,
			@QueryParam("authentication") String auth,
			@Context MessageContext jaxrsContext) {

		try {
			POImageUploadRequirementDTO poImageUploadRequirementDTO = poImageUploadLogic
					.getPOImageUploadRequirement(poId, auth);
			JSONObject jsonObj = JSONObject
					.fromObject(poImageUploadRequirementDTO);
			return jsonObj.toString();
		} catch (Exception ex) {
			messageUtils.handleException(ex,
					jaxrsContext.getHttpServletRequest());
			Map<String, String> errorMap = new HashMap<String, String>();
			errorMap.put("status", MessageKeyConstant.STATUS_FAIL);
			return JSONObject.fromObject(errorMap).toString();
		}

	}

	@Value("#{ilexProperties['mobile.upload.image.max.size']}")
	private String imageMaxSize;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.service.POImageUploadService#poImageUpload(javax.servlet.
	 * http.HttpServletRequest, org.apache.cxf.jaxrs.ext.MessageContext,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * org.apache.cxf.jaxrs.ext.multipart.Attachment)
	 */
	@Override
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/poImageUpload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String poImageUpload(

	@Context HttpServletRequest request, @Context MessageContext jaxrsContext,
			@QueryParam("lm_po_id") String poId,
			@QueryParam("name") String imageName,
			@QueryParam("imagenumber") String imageNumber,
			@QueryParam("authentication") String authentication,
			@QueryParam("description") String description, Attachment attachment) {

		POImageUploadDTO poImageUploadDTO = new POImageUploadDTO();
		try {

			FileItem fileItem = IlexFileUtils.getFileItem(attachment);

			poImageUploadDTO.setStatus(MessageKeyConstant.STATUS_FAIL);
			poImageUploadDTO.setAuthentication(authentication);

			poImageUploadDTO.setImagenumber(imageNumber);
			poImageUploadDTO.setLm_po_id(poId);
			poImageUploadDTO.setDescription(description);

			if (fileItem != null && fileItem.getSize() > 0
					&& fileItem.getSize() <= Long.valueOf(imageMaxSize)) {

				poImageUploadDTO.setName(imageName);

				boolean isUploaded = poImageUploadLogic.poImageUpload(
						poImageUploadDTO, fileItem);
				if (isUploaded) {
					poImageUploadDTO.setStatus(MessageKeyConstant.STATUS_PASS);
				}
			} else {
				throw new IlexBusinessException(
						MessageKeyConstant.IMAGE_SIZE_NOT_VALID);
			}

		} catch (Exception ex) {
			messageUtils.handleException(ex,
					jaxrsContext.getHttpServletRequest());
			poImageUploadDTO.setStatus(MessageKeyConstant.STATUS_FAIL);
		}

		JSONObject jsonObj = JSONObject.fromObject(poImageUploadDTO);
		return jsonObj.toString();
	}
}