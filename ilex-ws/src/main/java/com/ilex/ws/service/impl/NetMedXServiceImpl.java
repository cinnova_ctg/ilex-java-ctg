package com.ilex.ws.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.fileupload.FileItem;
import org.apache.cxf.jaxrs.ext.MessageContext;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.springframework.stereotype.Service;

import com.ilex.ws.core.dto.NetMedXConsolidatorRequestDTO;
import com.ilex.ws.core.dto.NetMedXConsolidatorRespDTO;
import com.ilex.ws.core.dto.NetMedXIntegrationDTO;
import com.ilex.ws.core.dto.NetMedXIntegrationResponseDTO;
import com.ilex.ws.core.logic.NetMedXLogic;
import com.ilex.ws.service.NetMedXService;
import com.ilex.ws.util.IlexFileUtils;
import com.ilex.ws.util.MessageUtils;

@Service
@Path("/netMedX")
public class NetMedXServiceImpl implements NetMedXService {

	@Resource
	NetMedXLogic netMedXLogic;
	@Resource
	MessageUtils messageUtils;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.service.NetMedXService#netMedXIntergration(javax.servlet.
	 * http.HttpServletRequest, org.apache.cxf.jaxrs.ext.MessageContext, int,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String,
	 * org.apache.cxf.jaxrs.ext.multipart.Attachment, java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/netMedXIntergration")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String netMedXIntergration(
			@Context HttpServletRequest request,
			@Context MessageContext jaxrsContext,
			@QueryParam("appendix_id") int appendixId,
			@QueryParam("site_number") String siteNumber,
			@QueryParam("site_phone") String sitePhone,
			@QueryParam("site_worklocation") String siteWorkLocation,
			@QueryParam("po_number") String poNumber,
			@QueryParam("site_address") String siteAddress,
			@QueryParam("site_city") String siteCity,
			@QueryParam("site_state") String siteState,
			@QueryParam("site_zipcode") String siteZipCode,
			@QueryParam("site_name") String siteName,
			@QueryParam("email") String email,
			@QueryParam("contact_person") String contactPerson,
			@QueryParam("contact_phone") String contactPhone,
			@QueryParam("primary_Name") String primaryName,
			@QueryParam("pri_site_phone") String priSitePhone,
			@QueryParam("secondary_Name") String secondaryName,
			@QueryParam("secondary_phone") String secondaryPhone,
			@QueryParam("requested_date") String requestedDate,
			@QueryParam("criticality") String criticality,
			@QueryParam("arrivalwindowstartdate") String arrivalWindowStartDate,
			@QueryParam("arrivalwindowenddate") String arrivalWindowEndDate,
			@QueryParam("arrivaldate") String arrivalDate,
			@QueryParam("problem_description") String problemDescription,
			@QueryParam("special_instruction") String specialInstruction,
			@QueryParam("stand_by") String standBy,
			@QueryParam("other_email_info") String otherEmailInfo,
			@QueryParam("cust_specific_fields") String custSpecificFields,
			Attachment attachment,
			@QueryParam("silver_bucket") String silverBucket,
			@QueryParam("user_name") String userName,
			@QueryParam("user_pass") String userPass) {

		NetMedXIntegrationDTO netMedXIntegrationDTO = new NetMedXIntegrationDTO();
		NetMedXIntegrationResponseDTO netMedXIntegrationResponseDTO = new NetMedXIntegrationResponseDTO();
		netMedXIntegrationDTO.setAppendixid(appendixId);
		netMedXIntegrationDTO.setArrivaldate(arrivalDate);
		netMedXIntegrationDTO.setArrivalwindowenddate(arrivalWindowEndDate);
		netMedXIntegrationDTO.setArrivalwindowstartdate(arrivalWindowStartDate);
		netMedXIntegrationDTO.setContact_person(contactPerson);
		netMedXIntegrationDTO.setContact_phone(contactPhone);
		netMedXIntegrationDTO.setCriticality(criticality);
		netMedXIntegrationDTO.setCust_specific_fields(custSpecificFields);
		netMedXIntegrationDTO.setEmail(email);
		netMedXIntegrationDTO.setOther_email_info(otherEmailInfo);
		netMedXIntegrationDTO.setPo_number((poNumber.isEmpty()) ? "0"
				: poNumber);
		netMedXIntegrationDTO.setPri_site_phone(priSitePhone);
		netMedXIntegrationDTO.setPrimary_Name(primaryName);
		netMedXIntegrationDTO.setProblem_description(problemDescription);
		netMedXIntegrationDTO.setRequested_date(requestedDate);
		netMedXIntegrationDTO.setSecondary_Name(secondaryName);
		netMedXIntegrationDTO.setSecondary_phone(secondaryPhone);
		netMedXIntegrationDTO.setSilver_bucket(silverBucket);
		netMedXIntegrationDTO.setSite_address(siteAddress);
		netMedXIntegrationDTO.setSite_city(siteCity);
		netMedXIntegrationDTO.setSite_name(siteName);
		netMedXIntegrationDTO.setSite_number(siteNumber);
		netMedXIntegrationDTO.setSite_phone(sitePhone);
		netMedXIntegrationDTO.setSite_state(siteState);
		netMedXIntegrationDTO.setSite_worklocation(siteWorkLocation);
		netMedXIntegrationDTO.setSite_zipcode(siteZipCode);
		netMedXIntegrationDTO.setSpecial_instruction(specialInstruction);
		netMedXIntegrationDTO.setStand_by(standBy);
		netMedXIntegrationDTO.setUserName(userName);
		netMedXIntegrationDTO.setUserPass(userPass);
		FileItem fileItem = IlexFileUtils.getFileItem(attachment);

		netMedXIntegrationResponseDTO = netMedXLogic.netMedXIntergration(
				netMedXIntegrationDTO, fileItem);

		JSONObject json = JSONObject.fromObject(netMedXIntegrationResponseDTO);
		return json.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.service.NetMedXService#netMedXIntergration(javax.servlet.
	 * http.HttpServletRequest, org.apache.cxf.jaxrs.ext.MessageContext, int,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.util.Date, java.lang.String, java.util.Date, java.util.Date,
	 * java.util.Date, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/netMedXIntergration")
	public String netMedXIntergration(@Context HttpServletRequest request,
			@Context MessageContext jaxrsContext,
			@QueryParam("appendix_id") int appendixId,
			@QueryParam("site_number") String siteNumber,
			@QueryParam("site_phone") String sitePhone,
			@QueryParam("site_worklocation") String siteWorkLocation,
			@QueryParam("po_number") String poNumber,
			@QueryParam("site_address") String siteAddress,
			@QueryParam("site_city") String siteCity,
			@QueryParam("site_state") String siteState,
			@QueryParam("site_zipcode") String siteZipCode,
			@QueryParam("site_name") String siteName,
			@QueryParam("email") String email,
			@QueryParam("contact_person") String contactPerson,
			@QueryParam("contact_phone") String contactPhone,
			@QueryParam("primary_Name") String primaryName,
			@QueryParam("pri_site_phone") String priSitePhone,
			@QueryParam("secondary_Name") String secondaryName,
			@QueryParam("secondary_phone") String secondaryPhone,
			@QueryParam("requested_date") Date requestedDate,
			@QueryParam("criticality") String criticality,
			@QueryParam("arrivalwindowstartdate") Date arrivalWindowStartDate,
			@QueryParam("arrivalwindowenddate") Date arrivalWindowEndDate,
			@QueryParam("arrivaldate") Date arrivalDate,
			@QueryParam("problem_description") String problemDescription,
			@QueryParam("special_instruction") String specialInstruction,
			@QueryParam("stand_by") String standBy,
			@QueryParam("other_email_info") String otherEmailInfo,
			@QueryParam("cust_specific_fields") String custSpecificFields,
			@QueryParam("silver_bucket") String silverBucket,
			@QueryParam("user_name") String userName,
			@QueryParam("user_pass") String userPass) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.service.NetMedXService#netMedXIntergration(javax.servlet.
	 * http.HttpServletRequest, org.apache.cxf.jaxrs.ext.MessageContext)
	 */
	@Override
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/netMedXConsolidator")
	public String netMedXIntergration(@Context HttpServletRequest request,
			@Context MessageContext jaxrsContext) {

		int pageNumber = 1;
		int pageSize = 20;
		int pageStartLimit = 0;
		int pageEndLimit = 19;

		NetMedXConsolidatorRequestDTO consolidatorRequestDTO = new NetMedXConsolidatorRequestDTO();

		consolidatorRequestDTO.setSiteNumber(request
				.getParameter("il_ji_lm_si_number"));
		consolidatorRequestDTO.setArchivedPeriod(request
				.getParameter("archived_period"));
		consolidatorRequestDTO.setScheduledDate(request
				.getParameter("il_ji_date_scheduled"));
		consolidatorRequestDTO.setStatus(request.getParameter("il_ji_status"));
		consolidatorRequestDTO.setSortOrder(!(request.getParameter("ord")
				.trim().equals("")) ? request.getParameter("ord").trim()
				: "DESC");

		consolidatorRequestDTO.setSortBy(!(request.getParameter("ord").trim()
				.equals("")) ? request.getParameter("sortby").trim() : "sch");

		consolidatorRequestDTO.setProjectId(request.getParameter("pr_id"));
		consolidatorRequestDTO.setSiteNumber(request
				.getParameter("txt_site_number"));
		consolidatorRequestDTO.setUserName(request.getParameter("user_name"));
		consolidatorRequestDTO.setUserPass(request.getParameter("user_pass"));

		if (!consolidatorRequestDTO.getSiteNumber().equals("")
				&& consolidatorRequestDTO.getArchivedPeriod() != null) {
			consolidatorRequestDTO.setArchivedPeriod("");
		}

		if (!consolidatorRequestDTO.getScheduledDate().equals("")
				&& consolidatorRequestDTO.getArchivedPeriod() != null) {
			consolidatorRequestDTO.setArchivedPeriod("");
		}
		if (!consolidatorRequestDTO.getStatus().equals("")
				&& consolidatorRequestDTO.getArchivedPeriod() != null) {
			consolidatorRequestDTO.setArchivedPeriod("");
		}
		if (consolidatorRequestDTO.getArchivedPeriod() != null
				&& !(consolidatorRequestDTO.getArchivedPeriod().isEmpty())) {
			consolidatorRequestDTO.setArchivedPeriod("1");
		}
		if (!request.getParameter("paging_size").equals("")) {
			pageSize = Integer.parseInt(request.getParameter("paging_size"));
		}

		if (!request.getParameter("page_number").equals("")) {
			pageNumber = Integer.parseInt(request.getParameter("page_number"));
		}

		pageStartLimit = ((pageNumber - 1) * pageSize) - 1;
		if (pageStartLimit < 0) {
			pageStartLimit = 0;
		}
		pageEndLimit = pageSize * pageNumber;

		consolidatorRequestDTO.setStartLimit(pageStartLimit);
		consolidatorRequestDTO.setEndLimit(pageEndLimit);

		String userAuthentication = netMedXLogic.netmedxUserAuth(
				consolidatorRequestDTO.getUserName(),
				consolidatorRequestDTO.getUserPass(),
				Integer.parseInt(consolidatorRequestDTO.getProjectId()));
		if (userAuthentication.contains("GrantAccess")) {

			List<NetMedXConsolidatorRespDTO> consolidatorRespDTOList = netMedXLogic
					.getNetMedXJobIntergration(consolidatorRequestDTO);

			JSONArray jsarr = JSONArray.fromObject(consolidatorRespDTOList);
			String jobCount = "{ \"JobCount\" : \""
					+ netMedXLogic.getJobCount(consolidatorRequestDTO) + "\"}";

			jsarr.add(jobCount);
			return jsarr.toString();
		} else {
			return "{\"ApiAccess\" : \"Invalid Access\"}";

		}

	}
}