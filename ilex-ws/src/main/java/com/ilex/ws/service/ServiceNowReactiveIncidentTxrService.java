package com.ilex.ws.service;

import com.ilex.ws.core.dto.IncidentResponseDTO;

/**
 * The Interface ServiceNowReactiveIncidentTxrService.Web service Used to create
 * Reactive Incident i.e from Service now to Ilex for Texas Roadhouse (TXR).
 */
public interface ServiceNowReactiveIncidentTxrService {
	/**
	 * Insert.
	 * 
	 * @param uSiteNumber
	 *            the u site number
	 * @param uCategory
	 *            the u category
	 * @param uConfigurationItem
	 *            the u configuration item
	 * @param uShortDescriptinon
	 *            the u short descriptinon
	 * @param uDescription
	 *            the u description
	 * @param uState
	 *            the u state
	 * @param uImpact
	 *            the u impact
	 * @param uUrgency
	 *            the u urgency
	 * @param uSympton
	 *            the u sympton
	 * @param uType
	 *            the u type
	 * @param uVendorKey
	 *            the u vendor key
	 * @param uTicketNumber
	 *            the u ticket number
	 * @param uWorkNote
	 *            the u work note
	 * @return the incident response dto
	 */
	IncidentResponseDTO insert(String uSiteNumber, String uCategory,
			String uConfigurationItem, String uShortDescriptinon,
			String uDescription, String uState, String uImpact,
			String uUrgency, String uSympton, String uType, String uVendorKey,
			String uTicketNumber, String uWorkNote);

}
