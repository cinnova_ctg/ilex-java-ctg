package com.ilex.ws.service.impl;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.springframework.stereotype.Service;

import com.ilex.ws.core.dto.IncidentDTO;
import com.ilex.ws.core.dto.IncidentResponseDTO;
import com.ilex.ws.core.logic.ServiceNowLogic;
import com.ilex.ws.service.ServiceNowToIlexUpdateIncidentService;
import com.ilex.ws.util.MessageUtils;

/**
 * The Class ServiceNowToIlexUpdateIncidentServiceImpl.
 */
@WebService
@Service
public class ServiceNowToIlexUpdateIncidentServiceImpl implements
		ServiceNowToIlexUpdateIncidentService {

	/** The jaxws context. */
	@Resource
	WebServiceContext jaxwsContext;

	/** The service now logic. */
	@Resource
	ServiceNowLogic serviceNowLogic;

	/** The message utils. */
	@Resource
	MessageUtils messageUtils;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.service.ServiceNowToIlexUpdateIncidentService#insert(java
	 * .lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@WebResult(name = "insertResponse")
	@WebMethod
	@Override
	public IncidentResponseDTO insert(
			@WebParam(name = "sys_id") String sysId,
			@WebParam(name = "u_state") String uState,
			@WebParam(name = "u_ticket_number") String uTicketNumber,
			@WebParam(name = "u_vendor_ticket_number") String uVendorTicketNumber,
			@WebParam(name = "u_work_notes") String uWorkNotes) {
		try {
			IncidentDTO incidentDTO = new IncidentDTO();
			incidentDTO.setServiceNowSysId(sysId);
			incidentDTO.setState(uState);
			incidentDTO.setTicketNumber(uTicketNumber);
			incidentDTO.setvTaskNumber(uVendorTicketNumber);
			incidentDTO.setWorkNotes(uWorkNotes);
			IncidentResponseDTO incidentResponseDTO = new IncidentResponseDTO();
			incidentResponseDTO = serviceNowLogic
					.updateIncidentFromServicenowToIlex(incidentDTO);
			return incidentResponseDTO;
		} catch (Exception ex) {
			HttpServletRequest request = null;
			String errMsg = "";
			if (jaxwsContext.getMessageContext() != null) {
				request = (HttpServletRequest) jaxwsContext.getMessageContext()
						.get(MessageContext.SERVLET_REQUEST);
				errMsg = messageUtils.handleException(ex, request);
			}
			throw new RuntimeException(errMsg);
		}
	}
}
