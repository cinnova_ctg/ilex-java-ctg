package com.ilex.ws.service;

import javax.servlet.http.HttpServletRequest;

import org.apache.cxf.jaxrs.ext.MessageContext;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;

/**
 * The Interface MobileUploadService.
 */
public interface POImageUploadService {

	/**
	 * upload deliverable image for the PO.
	 * 
	 * @param request
	 *            the request fetch authenticationString ,imageNumber and
	 *            name(imageName) from the request
	 * @param jaxrsContext
	 *            the jaxrsContext
	 * @param poId
	 *            the Purchase Order Id
	 * @param imageName
	 *            the image name
	 * @param imageNumber
	 *            the image number
	 * @param authentication
	 *            the authentication
	 * @param attachment
	 *            the image attachment
	 * @return the string
	 */
	public String poImageUpload(final HttpServletRequest request,
			MessageContext jaxrsContext, String poId, String imageName,
			String imageNumber, String authentication, String description,
			Attachment attachment);

	/**
	 * Gets the pO image upload requirement. Fetch poId and authenticationString
	 * from the request
	 * 
	 * @param request
	 *            the request
	 * @param jaxrsContext
	 *            the jaxrsContext
	 * @return the pO image upload requirement
	 */
	String getPOImageUploadRequirement(String poId, String auth,
			MessageContext jaxrsContext);

}
