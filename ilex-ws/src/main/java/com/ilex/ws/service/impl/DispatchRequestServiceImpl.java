package com.ilex.ws.service.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import net.sf.json.JSONArray;

import org.apache.cxf.jaxrs.ext.MessageContext;
import org.springframework.stereotype.Service;

import com.ilex.ws.core.dto.LabelValueDTO;
import com.ilex.ws.core.logic.NewWebTicketLogic;
import com.ilex.ws.service.DispatchRequestService;

@Service
@Path("/dispatch")
public class DispatchRequestServiceImpl implements DispatchRequestService {

	@Resource
	NewWebTicketLogic newWebTicketLogic;

	@Override
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/getRequestors")
	public String getRequestors(@QueryParam("msaId") Long msaId,
			@Context MessageContext jaxrsContext) {

		List<LabelValueDTO> requestors = newWebTicketLogic.getRequestors(msaId);

		JSONArray json = JSONArray.fromObject(requestors);

		return json.toString();

	}

}
