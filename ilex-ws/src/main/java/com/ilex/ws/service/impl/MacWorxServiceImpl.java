package com.ilex.ws.service.impl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import net.sf.json.JSONObject;

import org.apache.commons.fileupload.FileItem;
import org.apache.cxf.jaxrs.ext.MessageContext;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.springframework.stereotype.Service;

import com.ilex.ws.core.dto.MacWorxIntergrationDTO;
import com.ilex.ws.core.dto.MacWorxIntergrationResponseDTO;
import com.ilex.ws.core.logic.MacWorxLogic;
import com.ilex.ws.service.MacWorxService;
import com.ilex.ws.util.IlexFileUtils;
import com.ilex.ws.util.MessageUtils;

@Service
@Path("/macWorx")
public class MacWorxServiceImpl implements MacWorxService {
	@Resource
	MacWorxLogic macWorxLogic;
	@Resource
	MessageUtils messageUtils;

	@Override
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/macWorxIntergration")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String macWorxIntergration(
			@Context HttpServletRequest request,
			@Context MessageContext jaxrsContext,
			@QueryParam("macx_pr_id") String macxProjectId,
			@QueryParam("macx_requestor") String macxRequestor,
			@QueryParam("macx_site_number") String macxSiteNumber,
			@QueryParam("macx_site_location") String macxSiteLocation,
			@QueryParam("macx_address1") String macxAddress1,
			@QueryParam("macx_address2") String macxAddress2,
			@QueryParam("macx_city") String macxCity,
			@QueryParam("macx_state") String macxState,
			@QueryParam("macx_zip_code") String macxZipCode,
			@QueryParam("macx_criticality") String macxCriticality,
			@QueryParam("macx_schedule_date") String macxScheduleDate,
			@QueryParam("macx_schedule_time") String macxScheduleTime,
			@QueryParam("mac_special_instructions") String macSpecialInstructions,
			@QueryParam("macx_poc_name1") String macxPocName1,
			@QueryParam("macx_poc_phone1") String macxPocPhone1,
			@QueryParam("macx_poc_email1") String macxPocEmail1,
			@QueryParam("macx_poc_name2") String macxPocName2,
			@QueryParam("macx_poc_phone2") String macxPocPhone2,
			@QueryParam("macx_poc_email2") String macxPocEmail2,
			@QueryParam("macx_activity_list") String macxActivityList,
			@QueryParam("macx_activity_qty") String macxActivityQty,
			@QueryParam("macx_install_notes") String macxInstallNotes,
			@QueryParam("macx_non_pps") String macxNonPps,
			Attachment attachment,
			@QueryParam("macx_po_number") String macxPoNumber) {

		MacWorxIntergrationDTO macWorxIntergrationDTO = new MacWorxIntergrationDTO();
		MacWorxIntergrationResponseDTO macWorxIntergrationResponseDTO = new MacWorxIntergrationResponseDTO();
		try {
			Integer projectIdentfier = null;

			projectIdentfier = Integer.valueOf(macxProjectId);

			macWorxIntergrationDTO.setMacxProjectId(projectIdentfier);
			macWorxIntergrationDTO.setMacxRequestor(macxRequestor);
			macWorxIntergrationDTO.setMacxSiteNumber(macxSiteNumber);
			macWorxIntergrationDTO.setMacxSiteLocation(macxSiteLocation);
			macWorxIntergrationDTO.setMacxAddress1(macxAddress1);
			macWorxIntergrationDTO.setMacxAddress2(macxAddress2);
			macWorxIntergrationDTO.setMacxCity(macxCity);
			macWorxIntergrationDTO.setMacxState(macxState);
			macWorxIntergrationDTO.setMacxZipCode(macxZipCode);
			macWorxIntergrationDTO.setMacxCriticality(macxCriticality);
			macWorxIntergrationDTO.setMacxScheduleDate(macxScheduleDate);
			macWorxIntergrationDTO.setMacxScheduleTime(macxScheduleTime);
			macWorxIntergrationDTO
					.setMacSpecialInstructions(macSpecialInstructions);
			macWorxIntergrationDTO.setMacxPocName1(macxPocName1);
			macWorxIntergrationDTO.setMacxPocPhone1(macxPocPhone1);
			macWorxIntergrationDTO.setMacxPocEmail1(macxPocEmail1);
			macWorxIntergrationDTO.setMacxPocName2(macxPocName2);
			macWorxIntergrationDTO.setMacxPocPhone2(macxPocPhone2);
			macWorxIntergrationDTO.setMacxPocEmail2(macxPocEmail2);
			macWorxIntergrationDTO.setMacxActivityList(macxActivityList);
			macWorxIntergrationDTO.setMacxActivityQty(macxActivityQty);
			macWorxIntergrationDTO.setMacxInstallNotes(macxInstallNotes);
			macWorxIntergrationDTO.setMacxNonPps(macxNonPps);
			macWorxIntergrationDTO.setMacxPoNumber(macxPoNumber);
			FileItem fileItem = IlexFileUtils.getFileItem(attachment);

			macWorxIntergrationResponseDTO = macWorxLogic.macWorxIntergration(
					macWorxIntergrationDTO, fileItem);

		} catch (Exception ex) {
			if (ex instanceof NumberFormatException) {

				macWorxIntergrationResponseDTO
						.setTicketNumber("Invalid Project Identifier");

			} else
				macWorxIntergrationResponseDTO.setTicketNumber(messageUtils
						.getMessage(ex, jaxrsContext.getHttpServletRequest()));
			messageUtils.handleException(ex,
					jaxrsContext.getHttpServletRequest());

			macWorxIntergrationResponseDTO.setStatus(3);

		}
		JSONObject json = JSONObject.fromObject(macWorxIntergrationResponseDTO);
		return json.toString();
	}

	@Override
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/macWorxAssociation")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String macWorxAssociation(
			@Context HttpServletRequest request,
			@Context MessageContext jaxrsContext,
			@QueryParam("macx_pr_id") String macxProjectId,
			@QueryParam("macx_requestor") String macxRequestor,
			@QueryParam("macx_site_number") String macxSiteNumber,
			@QueryParam("macx_site_location") String macxSiteLocation,
			@QueryParam("macx_address1") String macxAddress1,
			@QueryParam("macx_address2") String macxAddress2,
			@QueryParam("macx_city") String macxCity,
			@QueryParam("macx_state") String macxState,
			@QueryParam("macx_zip_code") String macxZipCode,
			@QueryParam("macx_criticality") String macxCriticality,
			@QueryParam("macx_schedule_date") String macxScheduleDate,
			@QueryParam("macx_schedule_time") String macxScheduleTime,
			@QueryParam("mac_special_instructions") String macSpecialInstructions,
			@QueryParam("macx_poc_name1") String macxPocName1,
			@QueryParam("macx_poc_phone1") String macxPocPhone1,
			@QueryParam("macx_poc_email1") String macxPocEmail1,
			@QueryParam("macx_poc_name2") String macxPocName2,
			@QueryParam("macx_poc_phone2") String macxPocPhone2,
			@QueryParam("macx_poc_email2") String macxPocEmail2,
			@QueryParam("macx_activity_list") String macxActivityList,
			@QueryParam("macx_activity_qty") String macxActivityQty,
			@QueryParam("macx_install_notes") String macxInstallNotes,
			@QueryParam("macx_non_pps") String macxNonPps,
			Attachment attachment,
			@QueryParam("macx_po_number") String macxPoNumber,
			@QueryParam("user_name") String userName,
			@QueryParam("user_pass") String pass) {

		MacWorxIntergrationDTO macWorxIntergrationDTO = new MacWorxIntergrationDTO();
		MacWorxIntergrationResponseDTO macWorxIntergrationResponseDTO = new MacWorxIntergrationResponseDTO();
		try {
			Integer projectIdentfier = null;

			projectIdentfier = Integer.valueOf(macxProjectId);

			if (macWorxLogic.macWorxUserAuth(userName, pass, projectIdentfier) == 1) {
				macWorxIntergrationDTO.setMacxProjectId(projectIdentfier);
				macWorxIntergrationDTO.setMacxRequestor(macxRequestor);
				macWorxIntergrationDTO.setMacxSiteNumber(macxSiteNumber);
				macWorxIntergrationDTO.setMacxSiteLocation(macxSiteLocation);
				macWorxIntergrationDTO.setMacxAddress1(macxAddress1);
				macWorxIntergrationDTO.setMacxAddress2(macxAddress2);
				macWorxIntergrationDTO.setMacxCity(macxCity);
				macWorxIntergrationDTO.setMacxState(macxState);
				macWorxIntergrationDTO.setMacxZipCode(macxZipCode);
				macWorxIntergrationDTO.setMacxCriticality(macxCriticality);
				macWorxIntergrationDTO.setMacxScheduleDate(macxScheduleDate);
				macWorxIntergrationDTO.setMacxScheduleTime(macxScheduleTime);
				macWorxIntergrationDTO
						.setMacSpecialInstructions(macSpecialInstructions);
				macWorxIntergrationDTO.setMacxPocName1(macxPocName1);
				macWorxIntergrationDTO.setMacxPocPhone1(macxPocPhone1);
				macWorxIntergrationDTO.setMacxPocEmail1(macxPocEmail1);
				macWorxIntergrationDTO.setMacxPocName2(macxPocName2);
				macWorxIntergrationDTO.setMacxPocPhone2(macxPocPhone2);
				macWorxIntergrationDTO.setMacxPocEmail2(macxPocEmail2);
				macWorxIntergrationDTO.setMacxActivityList(macxActivityList);
				macWorxIntergrationDTO.setMacxActivityQty(macxActivityQty);
				macWorxIntergrationDTO.setMacxInstallNotes(macxInstallNotes);
				macWorxIntergrationDTO.setMacxNonPps(macxNonPps);
				macWorxIntergrationDTO.setMacxPoNumber(macxPoNumber);
				FileItem fileItem = IlexFileUtils.getFileItem(attachment);

				macWorxIntergrationResponseDTO = macWorxLogic
						.macWorxIntergration(macWorxIntergrationDTO, fileItem);
			} else {
				macWorxIntergrationResponseDTO
						.setTicketNumber("Invalid API Access");
			}
		} catch (Exception ex) {
			if (ex instanceof NumberFormatException) {

				macWorxIntergrationResponseDTO
						.setTicketNumber("Invalid Project Identifier");

			} else
				macWorxIntergrationResponseDTO.setTicketNumber(messageUtils
						.getMessage(ex, jaxrsContext.getHttpServletRequest()));
			messageUtils.handleException(ex,
					jaxrsContext.getHttpServletRequest());

			macWorxIntergrationResponseDTO.setStatus(3);

		}
		JSONObject json = JSONObject.fromObject(macWorxIntergrationResponseDTO);
		return json.toString();
	}

	@Override
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/macWorxIntergration")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String macWorxIntergration(
			@Context HttpServletRequest request,
			@Context MessageContext jaxrsContext,
			@QueryParam("macx_pr_id") String macxProjectId,
			@QueryParam("macx_requestor") String macxRequestor,
			@QueryParam("macx_site_number") String macxSiteNumber,
			@QueryParam("macx_site_location") String macxSiteLocation,
			@QueryParam("macx_address1") String macxAddress1,
			@QueryParam("macx_address2") String macxAddress2,
			@QueryParam("macx_city") String macxCity,
			@QueryParam("macx_state") String macxState,
			@QueryParam("macx_zip_code") String macxZipCode,
			@QueryParam("macx_criticality") String macxCriticality,
			@QueryParam("macx_schedule_date") String macxScheduleDate,
			@QueryParam("macx_schedule_time") String macxScheduleTime,
			@QueryParam("mac_special_instructions") String macSpecialInstructions,
			@QueryParam("macx_poc_name1") String macxPocName1,
			@QueryParam("macx_poc_phone1") String macxPocPhone1,
			@QueryParam("macx_poc_email1") String macxPocEmail1,
			@QueryParam("macx_poc_name2") String macxPocName2,
			@QueryParam("macx_poc_phone2") String macxPocPhone2,
			@QueryParam("macx_poc_email2") String macxPocEmail2,
			@QueryParam("macx_activity_list") String macxActivityList,
			@QueryParam("macx_activity_qty") String macxActivityQty,
			@QueryParam("macx_install_notes") String macxInstallNotes,
			@QueryParam("macx_non_pps") String macxNonPps,
			@QueryParam("macx_po_number") String macxPoNumber) {
		MacWorxIntergrationDTO macWorxIntergrationDTO = new MacWorxIntergrationDTO();
		MacWorxIntergrationResponseDTO macWorxIntergrationResponseDTO = new MacWorxIntergrationResponseDTO();
		try {
			Integer projectIdentfier = null;

			projectIdentfier = Integer.valueOf(macxProjectId);
			macWorxIntergrationDTO.setMacxProjectId(projectIdentfier);
			macWorxIntergrationDTO.setMacxRequestor(macxRequestor);
			macWorxIntergrationDTO.setMacxSiteNumber(macxSiteNumber);
			macWorxIntergrationDTO.setMacxSiteLocation(macxSiteLocation);
			macWorxIntergrationDTO.setMacxAddress1(macxAddress1);
			macWorxIntergrationDTO.setMacxAddress2(macxAddress2);
			macWorxIntergrationDTO.setMacxCity(macxCity);
			macWorxIntergrationDTO.setMacxState(macxState);
			macWorxIntergrationDTO.setMacxZipCode(macxZipCode);
			macWorxIntergrationDTO.setMacxCriticality(macxCriticality);
			macWorxIntergrationDTO.setMacxScheduleDate(macxScheduleDate);
			macWorxIntergrationDTO.setMacxScheduleTime(macxScheduleTime);
			macWorxIntergrationDTO
					.setMacSpecialInstructions(macSpecialInstructions);
			macWorxIntergrationDTO.setMacxPocName1(macxPocName1);
			macWorxIntergrationDTO.setMacxPocPhone1(macxPocPhone1);
			macWorxIntergrationDTO.setMacxPocEmail1(macxPocEmail1);
			macWorxIntergrationDTO.setMacxPocName2(macxPocName2);
			macWorxIntergrationDTO.setMacxPocPhone2(macxPocPhone2);
			macWorxIntergrationDTO.setMacxPocEmail2(macxPocEmail2);
			macWorxIntergrationDTO.setMacxActivityList(macxActivityList);
			macWorxIntergrationDTO.setMacxActivityQty(macxActivityQty);
			macWorxIntergrationDTO.setMacxInstallNotes(macxInstallNotes);
			macWorxIntergrationDTO.setMacxNonPps(macxNonPps);
			macWorxIntergrationDTO.setMacxPoNumber(macxPoNumber);

			macWorxIntergrationResponseDTO = macWorxLogic.macWorxIntergration(
					macWorxIntergrationDTO, null);
		} catch (Exception ex) {
			if (ex instanceof NumberFormatException) {
				macWorxIntergrationResponseDTO
						.setTicketNumber("Invalid Project Identifier");
			} else
				macWorxIntergrationResponseDTO.setTicketNumber(messageUtils
						.getMessage(ex, jaxrsContext.getHttpServletRequest()));
			messageUtils.handleException(ex,
					jaxrsContext.getHttpServletRequest());

			macWorxIntergrationResponseDTO.setStatus(3);

		}
		JSONObject json = JSONObject.fromObject(macWorxIntergrationResponseDTO);

		return json.toString();
	}

}
