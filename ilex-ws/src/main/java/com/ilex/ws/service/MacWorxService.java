package com.ilex.ws.service;

import javax.servlet.http.HttpServletRequest;

import org.apache.cxf.jaxrs.ext.MessageContext;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;

// TODO: Auto-generated Javadoc
/**
 * The Interface MacWorxService.
 */
public interface MacWorxService {

	/**
	 * Mac worx intergration.
	 * 
	 * @param request
	 *            the request
	 * @param jaxrsContext
	 *            the jaxrs context
	 * @param macxProjectId
	 *            the macx project id
	 * @param macxRequestor
	 *            the macx requestor
	 * @param macxSiteNumber
	 *            the macx site number
	 * @param macxSiteLocation
	 *            the macx site location
	 * @param macxAddress1
	 *            the macx address1
	 * @param macxAddress2
	 *            the macx address2
	 * @param macxCity
	 *            the macx city
	 * @param macxState
	 *            the macx state
	 * @param macxZipCode
	 *            the macx zip code
	 * @param macxCriticality
	 *            the macx criticality
	 * @param macxScheduleDate
	 *            the macx schedule date
	 * @param macxScheduleTime
	 *            the macx schedule time
	 * @param macSpecialInstructions
	 *            the mac special instructions
	 * @param macxPocName1
	 *            the macx poc name1
	 * @param macxPocPhone1
	 *            the macx poc phone1
	 * @param macxPocEmail1
	 *            the macx poc email1
	 * @param macxPocName2
	 *            the macx poc name2
	 * @param macxPocPhone2
	 *            the macx poc phone2
	 * @param macxPocEmail2
	 *            the macx poc email2
	 * @param macxActivityList
	 *            the macx activity list
	 * @param macxActivityQty
	 *            the macx activity qty
	 * @param macxInstallNotes
	 *            the macx install notes
	 * @param macxNonPps
	 *            the macx non pps
	 * @param attachment
	 *            the attachment
	 * @param macxPoNumber
	 *            the macx po number
	 * @param userName
	 *            the user name
	 * @param pass
	 *            the pass
	 * @return the string
	 */
	String macWorxIntergration(HttpServletRequest request,
			MessageContext jaxrsContext, String macxProjectId,
			String macxRequestor, String macxSiteNumber,
			String macxSiteLocation, String macxAddress1, String macxAddress2,
			String macxCity, String macxState, String macxZipCode,
			String macxCriticality, String macxScheduleDate,
			String macxScheduleTime, String macSpecialInstructions,
			String macxPocName1, String macxPocPhone1, String macxPocEmail1,
			String macxPocName2, String macxPocPhone2, String macxPocEmail2,
			String macxActivityList, String macxActivityQty,
			String macxInstallNotes, String macxNonPps, Attachment attachment,
			String macxPoNumber);

	/**
	 * Mac worx intergration.
	 * 
	 * @param request
	 *            the request
	 * @param jaxrsContext
	 *            the jaxrs context
	 * @param macxProjectId
	 *            the macx project id
	 * @param macxRequestor
	 *            the macx requestor
	 * @param macxSiteNumber
	 *            the macx site number
	 * @param macxSiteLocation
	 *            the macx site location
	 * @param macxAddress1
	 *            the macx address1
	 * @param macxAddress2
	 *            the macx address2
	 * @param macxCity
	 *            the macx city
	 * @param macxState
	 *            the macx state
	 * @param macxZipCode
	 *            the macx zip code
	 * @param macxCriticality
	 *            the macx criticality
	 * @param macxScheduleDate
	 *            the macx schedule date
	 * @param macxScheduleTime
	 *            the macx schedule time
	 * @param macSpecialInstructions
	 *            the mac special instructions
	 * @param macxPocName1
	 *            the macx poc name1
	 * @param macxPocPhone1
	 *            the macx poc phone1
	 * @param macxPocEmail1
	 *            the macx poc email1
	 * @param macxPocName2
	 *            the macx poc name2
	 * @param macxPocPhone2
	 *            the macx poc phone2
	 * @param macxPocEmail2
	 *            the macx poc email2
	 * @param macxActivityList
	 *            the macx activity list
	 * @param macxActivityQty
	 *            the macx activity qty
	 * @param macxInstallNotes
	 *            the macx install notes
	 * @param macxNonPps
	 *            the macx non pps
	 * @param macxPoNumber
	 *            the macx po number
	 * @return the string
	 */
	String macWorxIntergration(HttpServletRequest request,
			MessageContext jaxrsContext, String macxProjectId,
			String macxRequestor, String macxSiteNumber,
			String macxSiteLocation, String macxAddress1, String macxAddress2,
			String macxCity, String macxState, String macxZipCode,
			String macxCriticality, String macxScheduleDate,
			String macxScheduleTime, String macSpecialInstructions,
			String macxPocName1, String macxPocPhone1, String macxPocEmail1,
			String macxPocName2, String macxPocPhone2, String macxPocEmail2,
			String macxActivityList, String macxActivityQty,
			String macxInstallNotes, String macxNonPps, String macxPoNumber);

	/**
	 * Mac worx association.
	 * 
	 * @param request
	 *            the request
	 * @param jaxrsContext
	 *            the jaxrs context
	 * @param macxProjectId
	 *            the macx project id
	 * @param macxRequestor
	 *            the macx requestor
	 * @param macxSiteNumber
	 *            the macx site number
	 * @param macxSiteLocation
	 *            the macx site location
	 * @param macxAddress1
	 *            the macx address1
	 * @param macxAddress2
	 *            the macx address2
	 * @param macxCity
	 *            the macx city
	 * @param macxState
	 *            the macx state
	 * @param macxZipCode
	 *            the macx zip code
	 * @param macxCriticality
	 *            the macx criticality
	 * @param macxScheduleDate
	 *            the macx schedule date
	 * @param macxScheduleTime
	 *            the macx schedule time
	 * @param macSpecialInstructions
	 *            the mac special instructions
	 * @param macxPocName1
	 *            the macx poc name1
	 * @param macxPocPhone1
	 *            the macx poc phone1
	 * @param macxPocEmail1
	 *            the macx poc email1
	 * @param macxPocName2
	 *            the macx poc name2
	 * @param macxPocPhone2
	 *            the macx poc phone2
	 * @param macxPocEmail2
	 *            the macx poc email2
	 * @param macxActivityList
	 *            the macx activity list
	 * @param macxActivityQty
	 *            the macx activity qty
	 * @param macxInstallNotes
	 *            the macx install notes
	 * @param macxNonPps
	 *            the macx non pps
	 * @param attachment
	 *            the attachment
	 * @param macxPoNumber
	 *            the macx po number
	 * @param userName
	 *            the user name
	 * @param pass
	 *            the pass
	 * @return the string
	 */
	String macWorxAssociation(HttpServletRequest request,
			MessageContext jaxrsContext, String macxProjectId,
			String macxRequestor, String macxSiteNumber,
			String macxSiteLocation, String macxAddress1, String macxAddress2,
			String macxCity, String macxState, String macxZipCode,
			String macxCriticality, String macxScheduleDate,
			String macxScheduleTime, String macSpecialInstructions,
			String macxPocName1, String macxPocPhone1, String macxPocEmail1,
			String macxPocName2, String macxPocPhone2, String macxPocEmail2,
			String macxActivityList, String macxActivityQty,
			String macxInstallNotes, String macxNonPps, Attachment attachment,
			String macxPoNumber, String userName, String pass);

}
