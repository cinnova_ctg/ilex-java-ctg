package com.ilex.ws.service.impl;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.springframework.stereotype.Service;

import com.ilex.ws.core.dto.IncidentDTO;
import com.ilex.ws.core.dto.IncidentResponseDTO;
import com.ilex.ws.core.logic.ServiceNowLogic;
import com.ilex.ws.service.ServiceNowReactiveIncidentService;
import com.ilex.ws.util.MessageUtils;

/**
 * The Class ServiceNowReactiveIncidentServiceImpl.
 */
@WebService
@Service
public class ServiceNowReactiveIncidentServiceImpl implements
		ServiceNowReactiveIncidentService {

	/** The jaxws context. */
	@Resource
	WebServiceContext jaxwsContext;

	/** The service now logic. */
	@Resource
	ServiceNowLogic serviceNowLogic;

	/** The message utils. */
	@Resource
	MessageUtils messageUtils;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ilex.ws.service.ServiceNowReactiveIncidentService#insert(java.lang
	 * .String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@WebResult(name = "insertResponse")
	@WebMethod
	@Override
	public IncidentResponseDTO insert(
			@WebParam(name = "u_site_number") String uSiteNumber,
			@WebParam(name = "u_category") String uCategory,
			@WebParam(name = "u_configuration_item") String uConfigurationItem,
			@WebParam(name = "u_short_description") String uShortDescription,
			@WebParam(name = "u_description") String uDescription,
			@WebParam(name = "u_state") String uState,
			@WebParam(name = "u_impact") String uImpact,
			@WebParam(name = "u_urgency") String uUrgency,
			@WebParam(name = "u_sympton") String uSympton,
			@WebParam(name = "u_type") String uType,
			@WebParam(name = "u_vendor_key") String uVendorKey,
			@WebParam(name = "u_ticket_number") String uTicketNumber,
			@WebParam(name = "u_work_note") String uWorkNote) {
		try {
			IncidentDTO reactiveIncidentDTO = new IncidentDTO();
			reactiveIncidentDTO.setSiteNumber(uSiteNumber);
			reactiveIncidentDTO.setCategory(uCategory);
			reactiveIncidentDTO.setConfigurationItem(uConfigurationItem);
			reactiveIncidentDTO.setShortDescription(uShortDescription);
			reactiveIncidentDTO.setDescription(uDescription);
			reactiveIncidentDTO.setState(uState);
			reactiveIncidentDTO.setImpact(uImpact);
			reactiveIncidentDTO.setUrgency(uUrgency);
			reactiveIncidentDTO.setSymptom(uSympton);
			reactiveIncidentDTO.setType(uType);
			reactiveIncidentDTO.setVendorKey(uVendorKey);
			reactiveIncidentDTO.setTicketNumber(uTicketNumber);
			reactiveIncidentDTO.setWorkNotes(uWorkNote);
			reactiveIncidentDTO.setMsaId("462");// The “cfa-sn” end point will
												// default the MSA and project
												// identifiers
			reactiveIncidentDTO.setAppendixId("2023");
			IncidentResponseDTO incidentResponseDTO = new IncidentResponseDTO();
			incidentResponseDTO = serviceNowLogic
					.createReactiveIncident(reactiveIncidentDTO);
			return incidentResponseDTO;
		} catch (Exception ex) {
			HttpServletRequest request = null;
			String errMsg = "";
			if (jaxwsContext.getMessageContext() != null) {
				request = (HttpServletRequest) jaxwsContext.getMessageContext()
						.get(MessageContext.SERVLET_REQUEST);
				errMsg = messageUtils.handleException(ex, request);
			}
			throw new RuntimeException(errMsg);
		}
	}
}
