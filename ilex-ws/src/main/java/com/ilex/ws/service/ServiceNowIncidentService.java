package com.ilex.ws.service;

import com.ilex.ws.core.dto.TicketCreateResponse;

public interface ServiceNowIncidentService {

	TicketCreateResponse createTicket(String category, String incidentState,
			String causedBy, String shortDescription, String active,
			String ticketNumber, String sys_id);

	TicketCreateResponse addComment(String comments, String workNotes,
			String ticketNumber);

}
