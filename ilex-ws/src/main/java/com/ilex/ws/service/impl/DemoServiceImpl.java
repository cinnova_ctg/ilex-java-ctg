package com.ilex.ws.service.impl;

import javax.annotation.Resource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import net.sf.json.JSONObject;

import org.apache.cxf.jaxrs.ext.MessageContext;
import org.springframework.stereotype.Service;

import com.ilex.ws.core.bean.UserDTO;
import com.ilex.ws.core.logic.DemoLogic;
import com.ilex.ws.service.DemoService;
import com.ilex.ws.util.MessageUtils;

@Service
@Path("/demo")
public class DemoServiceImpl implements DemoService {

	@Resource
	DemoLogic demoLogic;

	@Resource
	MessageUtils messageUtils;

	@Override
	@GET
	@Produces({ MediaType.TEXT_HTML })
	@Path("/getUserInfo/{id}")
	public String getUserInfo(@PathParam("id") String userId,
			@Context MessageContext jaxrsContext) {
		try {
			UserDTO loginDTO = demoLogic.getUserInfo(userId);

			JSONObject jsonObj = JSONObject.fromObject(loginDTO);
			return jsonObj.toString();
		} catch (Exception ex) {
			String errMsg = messageUtils.handleException(ex, jaxrsContext.getHttpServletRequest());

			return errMsg;
		}
	}

}
