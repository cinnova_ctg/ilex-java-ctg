package com.ilex.ws.service;

import com.ilex.ws.core.dto.IncidentResponseDTO;

/**
 * The Interface ServiceNowToIlexUpdateIncidentService.Web service used to
 * update incident from Servicenow to Ilex for Texas Roadhouse (TXR).
 */
public interface ServiceNowToIlexUpdateIncidentTxrService {
	IncidentResponseDTO insert(String sysId, String uState,
			String uTicketNumber, String uVendorTicketNumber, String uWorkNotes);

}
