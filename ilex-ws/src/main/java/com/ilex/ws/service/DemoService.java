package com.ilex.ws.service;

import org.apache.cxf.jaxrs.ext.MessageContext;

public interface DemoService {

	public String getUserInfo(String userId, MessageContext req);

}
