package com.ilex.ws.service;

import org.apache.cxf.jaxrs.ext.MessageContext;

public interface DispatchRequestService {

	String getRequestors(Long msaId, MessageContext jaxrsContext);

}
