package com.ilex.ws.service;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.cxf.jaxrs.ext.MessageContext;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;

public interface NetMedXService {

	String netMedXIntergration(HttpServletRequest request,
			MessageContext jaxrsContext, int appendixId, String siteNumber,
			String sitePhone, String siteWorkLocation, String poNumber,
			String siteAddress2, String siteCity, String siteState,
			String siteZipCode, String siteName, String email,
			String contactPerson, String contactPhone, String primaryName,
			String priSitePhone, String secondaryName, String secondaryPhone,
			String requestedDate, String criticality,
			String arrivalWindowStartDate, String arrivalwindowEndDate,
			String arrivalDate, String problemDescription,
			String specialInstruction, String standBy, String otherEmailInfo,
			String custSpecificFields, Attachment attachment,
			String silverBucket, String userName, String userPass);

	String netMedXIntergration(HttpServletRequest request,
			MessageContext jaxrsContext, int appendixid, String siteNumber,
			String sitePhone, String siteWorkLocation, String poNumber,
			String siteAddress2, String siteCity, String siteState,
			String siteZipcode, String siteName, String email,
			String contactPerson, String contactPhone, String primaryName,
			String pri_sitePhone, String secondaryName, String secondaryPhone,
			Date requestedDate, String criticality,
			Date arrivalWindowStartdate, Date arrivalWindowEndDate,
			Date arrivalDate, String problemDescription,
			String specialInstruction, String standBy, String otherEmailInfo,
			String custSpecificFields, String silverBucket, String userName,
			String userPass);

	String netMedXIntergration(HttpServletRequest request,
			MessageContext jaxrsContext);
}
