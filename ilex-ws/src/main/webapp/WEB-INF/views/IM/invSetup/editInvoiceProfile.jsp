
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
 <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jMenu.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <style type="text/css">
 .contactHeader{
 background: #CCC
 }
 #jQ-menu ul {
	list-style-type: none;
}

#jQ-menu li {
	text-decoration: none;
}
 </style>
 <script type="text/javascript">
 $(document).ready(function() {
	
		
		
	 $("input[name='invoiceSetupType']").change(function(){
		 if("Consolidated" ==$("input[name='invoiceSetupType']:checked").val()){
			 $("input[name='invoiceSetupGroupingType']").attr("checked", false);
			 $("input[name='invoiceSetupGroupingType']").attr("disabled", "disabled"); 
			 
			 
		 }
		 if("Individual" ==$("input[name='invoiceSetupType']:checked").val()){
			 $("input[name='invoiceSetupGroupingType']").removeAttr("disabled");
			 
			 
		 }});
	
	
	 $(".floatValue").val(parseFloat($(".floatValue").val()).toFixed(2));
	 
	 $(".floatValue").change(function() {
		if( isNaN($(".floatValue").val())){
			alert("Please Enter Correct Tax Value");
		}else{
			 $(".floatValue").val(parseFloat($(".floatValue").val()).toFixed(2));
		};
		  });
	
 });

function validate(){
	if("Individual" ==$("input[name='invoiceSetupType']:checked").val()){
		if($("input[name='invoiceSetupGroupingType']:checked").val()==undefined){
			alert("Select Invoice Type Seperate or Combine");
			return false;
		}
	};
	
	if( isNaN($(".floatValue").val())){
		alert("Please Enter Correct Tax Value");
		return false;
	}else{
		 $(".floatValue").val(parseFloat($(".floatValue").val()).toFixed(2));
	};
	
}
function editInvoiceProfile(){
	$("#btnEdit").attr("disabled", "disabled"); 
	$("input[name='pageLayout']").removeAttr("disabled"); 
	$("input[name='contingentAddress']").removeAttr("disabled"); 
	$("input[name='invoiceSetupType']").removeAttr("disabled"); 
	$("input[name='invoiceSetupGroupingType']").removeAttr("disabled"); 
	$("input[name='outstandingBalanceExist']").removeAttr("disabled"); 
	$("input[name='timeFrame']").removeAttr("disabled"); 
	$("#btnUpdate").removeAttr("disabled"); 
	$("#collectedTax").removeAttr("disabled"); 
}
function updateInvoiceProfile(){
	if("Individual" ==$("input[name='invoiceSetupType']:checked").val()){
		if($("input[name='invoiceSetupGroupingType']:checked").val()==undefined){
			alert("Select Invoice Type Seperate or Combine");
			return false;
		}
	};
	
	if( isNaN($(".floatValue").val())){
		alert("Please Enter Correct Tax Value");
		return false;
	}else{
		 $(".floatValue").val(parseFloat($(".floatValue").val()).toFixed(2));
	};
	$.post("${pageContext.request.contextPath}/invoiceSetup/updateInvoiceProfile.html",$("form").serialize(),
			   function(data) {
		$('#popupBody').html(data);
			   });	
	
}

</script>



	<div class="WASTD0001" style="text-align: left; margin-right: 20px;">
			<%@ include file="../../common/displayMessages.jsp" %>
			
			<form:form  action="${pageContext.request.contextPath}/invoiceSetup/editInvoiceProfile.html" commandName="invSetupDTO" method="POST" onsubmit="return validate();">
				<form:hidden path="invoiceSetupId"/>
						<div id="labelDiv" class="divFloatLeft PCSTD0001" style="width:110px;">
										<div  style="margin-bottom: 17px;" >Page Layout :</div>
										<div style="margin-bottom: 18px;">Address:</div>
										<div style="margin-bottom:50px;">Invoice Type:</div>
										<div>Time Frame:</div>
							</div>
							<div class="PCSTD0007">
										<div  style="margin-bottom: 11px;"><form:radiobutton path="pageLayout" value="Standard-1" disabled="true"/>Standard</div>
										<div class="divFloatLeft" style="width: 300px;" ><form:radiobutton path="contingentAddress" value="Contingent Network Services, LLC" disabled="true"/>Contingent Network Services, LLC</div>
										<div><form:radiobutton path="contingentAddress"  value="Contingent Network Services, International" disabled="true"/>Contingent Network Services, International</div><br/>
										<div><form:radiobutton path="invoiceSetupType"   value="Individual" disabled="true" />Individual &nbsp;&nbsp;&nbsp;(<form:radiobutton path="invoiceSetupGroupingType" value="Separate" disabled="true" />Separate  or <form:radiobutton path="invoiceSetupGroupingType" value="Combined" disabled="true" />Combined )</div>
										<div  style="margin-left: 145px;">or</div>
										<div style="margin-bottom: 11px;"><form:radiobutton path="invoiceSetupType"  value="Consolidated" disabled="true" />Consolidated</div>
										<div><form:radiobutton path="timeFrame" value="Offsite" disabled="true" />Offsite, &nbsp;&nbsp;&nbsp;<form:radiobutton path="timeFrame" value="Weekly" disabled="true" />Weekly, &nbsp;&nbsp;&nbsp;<form:radiobutton path="timeFrame" value="Semimonthly" disabled="true" />Bi-monthly(1<sup>st</sup> and 15<sup>th</sup>), &nbsp;&nbsp;&nbsp;<form:radiobutton path="timeFrame" value="Monthly" disabled="true" />Monthly &nbsp;&nbsp;&nbsp;</div>
							</div>
							<div style="height: 10px;">&nbsp;</div>
						     	<div id="jQ-menu" style="text-align: left;margin: 0px;padding-bottom: 10px;">
						     <ul  class="PCSTD0007" style="text-align: left;margin: 0px;padding: 0px;">
						     <li style="text-align: left;margin: 0px;padding: 0px;"><span class="toggle"><font>Notes:</font></span>
						     <ul>
						     <li>
						     <div class="PCSTD0007">
						     <pre style="font-family: inherit;">
The invoice type defines how a job is invoiced to a client. The two options available are an individual job or as a consolidation of
multiple jobs. An individual job invoice is simply invoiced on the basis of a job and it’s extended price. The consolidated invoices
sums one or more jobs into a single billable amount which becomes undividable meaning the client is expected to pay the total
amount. Individual jobs may still be grouped together for administrative convenience but represent individual job invoices . All 
invoices are assigned  an invoice date(described below) and a unique invoice number.

Individual invoice are eligible to be invoiced when the job is marked offsite and thus the invoice will use the offsite date(last 
offsite date). A consolidated invoice will use the latest offsite date from all grouped jobs.
 
</pre>
								
						     </div>
			</li>
						     </ul>
						     </li>
						   
							</ul>
								
							</div>
							
								<div  class="divFloatLeft PCSTD0001" style="width: 195px;">Include Outstanding Balance:</div>
								<div class="PCSTD0007"><form:radiobutton path="outstandingBalanceExist" value="true" disabled="true"/>Yes&nbsp;&nbsp;<form:radiobutton path="outstandingBalanceExist" value="false" disabled="true"/>No</div>
							<div style="height: 10px;">&nbsp;</div>
							<div style="width: 140px;"class="divFloatLeft PCSTD0001 " >Include Tax At Rate:</div>	<div class="PCSTD0007" ><form:input path="collectedTax"  cssClass="floatValue PCTXT0006" size="1"/></div>
							<div style="margin-left: 570px;">
								<input type="button" id="btnUpdate" class="FMBUT0001" value="Update" disabled="disabled" onclick="updateInvoiceProfile()"/>
								<input type="button" id="btnEdit" class="FMBUT0001" value="Edit" onclick="editInvoiceProfile();"/>
							</div>
				
				
			
			</form:form>
				</div>
