<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <style type="text/css">
 .contactHeader{
 background: #CCC
 }
 </style>
 <script type="text/javascript">
function updateSummarySheet(){
	$.post("${pageContext.request.contextPath}/invoiceSetup/updateSummarySheet.html",$("form").serialize(),
			   function(data) {
		$('#popupBody').html(data);
			   });	
	
}
function editSummarySheet(){
	$("#btnEdit").attr("disabled", "disabled"); 
	$("input[name='summarySheet']").removeAttr("disabled"); 
	$("#btnUpdate").removeAttr("disabled"); 
}
</script>



<div class="WASTD0001" style="text-align: left;margin-right: 20px;">
			<%@ include file="../../common/displayMessages.jsp" %>
			
			<form:form  action="${pageContext.request.contextPath}/invoiceSetup/saveAppendixInvDetail.html" commandName="invSetupDTO" method="POST">
			<form:hidden path="invoiceSetupId"/>
						<div class="PCSTD0007"><form:radiobutton path="summarySheet" value="List of Sites" disabled="true"/>List of Sites - site number and address information</div>
						<div class="PCSTD0007"><form:radiobutton path="summarySheet" value="List of All Activities" disabled="true"/>List of All Activities-sum of activities across all jobs</div>
						<div class="PCSTD0007"><form:radiobutton path="summarySheet" value="Not Applicable" disabled="true"/>Not Applicable</div>
							<div style="text-align: right;">
								<input type="button" id="btnUpdate" class="FMBUT0001" value="Update" disabled="disabled" onclick="updateSummarySheet()"/>
								<input type="button" id="btnEdit" class="FMBUT0001" value="Edit" onclick="editSummarySheet();"/>
							</div>
			</form:form>
</div>
