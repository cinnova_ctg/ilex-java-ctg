<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>

 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>

 <script>
 function set(){
//	window.returnValue=$("input[name='selectedcustomerInfoParameter']").val();
	//window.close() ;
 }
 </script>
<div style="margin: 20px;">
	<form>
		<table cellspacing="1" >
			<tr>
				<th  class="TBHED0002" style="padding: 4px;">Select</th>
				<th  class="TBHED0002">Value</th>
			</tr>
			<c:if test="${not empty custInfoParameterList}">
				<c:forEach var="custInfoParameter" items="${custInfoParameterList}" varStatus="loopStatus">
					<tr  class="${loopStatus.index % 2 == 0 ? 'odd' : 'even'}">
						<td><input type="radio" class="FMRAD0001"  width="10px;" name="selectedcustomerInfoParameter" value="${custInfoParameter}" onclick="setValue();"/></td>
						<td><label class="TBTXT0004" style="padding:4px; white-space: nowrap;">${custInfoParameter}</label></td>
					</tr>
				</c:forEach>
			</c:if>
		</table>
	</form>
</div>