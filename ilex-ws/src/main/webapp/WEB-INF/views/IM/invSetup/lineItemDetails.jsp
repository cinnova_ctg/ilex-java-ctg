<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
  <style type="text/css">
 .contactHeader{
 background: #CCC
 }
 </style>
 <script type="text/javascript">
 
function back(){
	var url="${pageContext.request.contextPath}/invoiceSetup/backToClientReqInvInfo.html";
	document.forms[0].action= url;
	
	  document.forms[0].submit();
}
$(document).ready(function() {
	var label="";
	if($("#appendix\\.appendixType").val()==3){
		label="NetMedX";
	}else{
		label="Projects";
	}
	 $("#txtLabel").html(label + "  Options:  ");	 
});
</script>
</head>
<body>

	<jsp:include page="appendixInvBreadcrum.jsp"/>

	<div class="WASTD0001">
			<%@ include file="../../common/displayMessages.jsp" %>
			
			<form:form  action="${pageContext.request.contextPath}/invoiceSetup/summarySheet.html" commandName="appendixInvSetupDTO" method="POST">
				<div class="PCSSH0001 PCSSH0004">Line Item  Details</div>
					<div><label id="txtLabel" class="PCSTD0001"></label></div>
								<div style="height: 5px;">&nbsp;</div>
								<form:hidden path="client.mbtClientId" />
									<form:hidden path="appendix.appendixId"/>
									<form:hidden path="invSetupDetails.invoiceSetupId"/>
										<form:hidden path="billToAddress.invBillToAddressId" />
					<form:hidden path="appendix.appendixType" />
					<div class="PCSTD0007">
					<c:if test="${not empty invMasterLineItemDetailsDTOList}">
							<c:forEach var="invMasterLineItemDetailsDTOList" items="${invMasterLineItemDetailsDTOList}">
								<form:radiobutton path="invSetupDetails.lineItemFormat" value="${invMasterLineItemDetailsDTOList.lineItemName}"/>${invMasterLineItemDetailsDTOList.lineItemName}
								<br/>
							</c:forEach>
						</c:if>
					</div>
						
							
							<div>
								<input type="button" id="btnBack" class="FMBUT0001" value="<fmt:message key="mbt.btn.back" ></fmt:message>" onclick="back();"/>
								<input type="submit" id="btnGo" class="FMBUT0001" value="<fmt:message key="mbt.btn.go" ></fmt:message>"/>
							</div>
							
				
				
			
			</form:form>
				</div>



</body>
</html>