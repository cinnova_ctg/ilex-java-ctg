<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<script type="text/javascript">
function editClientProfile(){
	$("#btnEdit").attr("disabled", "disabled"); 
	$("#clientName").removeAttr("disabled"); 
	$("#deliveryMethod").removeAttr("disabled"); 
	$("#address1").removeAttr("disabled"); 
	$("#address2").removeAttr("disabled"); 
	$("#city").removeAttr("disabled"); 
	$("#state").removeAttr("disabled"); 
	$("#zipCode").removeAttr("disabled"); 
	$("#country").removeAttr("disabled"); 
	$("#firstName").removeAttr("disabled"); 
	$("#lastName").removeAttr("disabled"); 
	$("#phone").removeAttr("disabled"); 
	$("#email").removeAttr("disabled"); 
	$("#cell").removeAttr("disabled"); 
	$("#fax").removeAttr("disabled"); 
	$("#btnUpdate").removeAttr("disabled"); 
}
function updateClient(){
	if($.trim($("#clientName").val())==""){
		alert("Please Enter Client Name");
		return false;
	}
	if($.trim($("#address1").val())==""){
		alert("Please Enter Address ");
		return false;
	}
	if($.trim($("#city").val())==""){
		alert("Please Enter City ");
		return false;
	}
	
	if($.trim($("#state").val())==""){
		alert("Please Select State ");
		return false;
	}
	if($.trim($("#country").val())==""){
		alert("Please Select Country ");
		return false;
	}
	if($.trim($("#firstName").val())==""){
		alert("Please Enter First Name ");
		return false;
	}
	if($.trim($("#lastName").val())==""){
		alert("Please Enter Last Name ");
		return false;
	}
	if($.trim($("#phone").val())==""){
		alert("Please Enter Phone");
		return false;
	}
	
	for(var j=0;j <=$('.billingContactTable tr').length-2;j++){
		if($.trim($("input[name='contactDetailsList["+j+"].firstName']").val())==""){
			alert("Please Enter First Name For All Contacts ");
			return false;
		} ;	
		if($.trim($("input[name='contactDetailsList["+j+"].lastName']").val())==""){
			alert("Please Enter Last Name For All Contacts ");
			return false;
		} ;
		if($.trim($("input[name='contactDetailsList["+j+"].phone']").val())==""){
			alert("Please Enter phone For All Contacts ");
			return false;
		} ;
	}
	$.post("${pageContext.request.contextPath}/invoiceSetup/updateClientProfile.html",$("form").serialize(),
			   function(data) {
		$('#popupBody').html(data);
			   });	
}
</script>

	<div class="WASTD0001" style="text-align: left;">
			<%@ include file="../../common/displayMessages.jsp" %>
			
			<form:form  action="${pageContext.request.contextPath}/invoiceSetup/updateClientProfile.html" commandName="invBillToAddressDTO" method="GET">
			<div style="margin-bottom: 20px;">
							<div id="labelDiv1" class="divFloatLeft PCSTD0001" style="width: 130px;">
								<div style="margin-bottom: 17px;"><fmt:message key="client.label"></fmt:message>:</div>
								<div><fmt:message key="deliveryMethod"></fmt:message>: </div>
							</div>
							 <div  class="PCSTD0002"id="rightClientDiv">
									<div style="margin-bottom: 10px;">
									<form:hidden path="invBillToAddressId" />
										<form:input path="clientName" cssClass="PCTXT0004" disabled="true"/><acronym title="This field is required"><font color="red">&nbsp;*</font></acronym>
									</div>
									<div >
										<form:select path="deliveryMethod" cssClass="PCCOMBO002" disabled="true">
										   		<form:option value="1" label="Email" />
										   		<form:option value="2" label="Fax" />
										   		<form:option value="3" label="Mail" />
										   		<form:option value="0" label="To Be Determined" />
										</form:select>
										<acronym title="This field is required"><font color="red">*</font></acronym>
									</div>
							</div>
					</div>
					<div class="PCSTD0001" style="margin-bottom: 5px;"><fmt:message key="billingAddress"/></div>
				<div style="">
						<div id="labelDiv2" class="divFloatLeft PCSTD0002" style="width: 80px;margin-left: 50px;">
									<div  style="margin-bottom: 30px;" ><fmt:message key="address.label"/>:</div>
									<div style="margin-bottom: 10px;"><fmt:message key="city.label"/>:</div>
									<div style="margin-bottom: 8px;"><fmt:message key="state.label"/>:</div>
									<div style="margin-bottom: 8px;"><fmt:message key="zipcode.label"/>:</div>
									<div><fmt:message key="country.label"/>:</div>
									
						</div>
					<div  class="PCSTD0002" id="rightContactDiv">
								<div>
									<div style="margin-bottom: 4px;"><form:input path="address1" cssClass="PCTXT0004" disabled="true" /><acronym title="This field is required"><font color="red">&nbsp;*</font></acronym></div>
									<div style="margin-bottom: 4px;"><form:input path="address2" cssClass="PCTXT0004" disabled="true" /></div>
								</div>
								<div style="margin-bottom: 4px;">
									<form:input path="city"  cssClass="PCTXT0004" disabled="true"/><acronym title="This field is required"><font color="red">&nbsp;*</font></acronym>
								</div>
								<div style="margin-bottom: 4px;">
									<form:select path="state" cssClass="PCCOMBO002" disabled="true" >
										<form:option value="" label="Select a State"/>
									   		<c:forEach var="state" items="${stateList}">
								   				<form:option value="${state.stateShortName}" label="${state.stateName}" />
								   			</c:forEach>
									</form:select><acronym title="This field is required"><font color="red">&nbsp;*</font></acronym>
								</div>
								<div style="margin-bottom: 4px;">
									<div><form:input path="zipCode" cssClass="PCTXT0005" disabled="true"/></div>
								</div>
								<div>
									<form:select path="country" cssClass="PCCOMBO002" disabled="true"  >
										<form:option value="" label="Select a Country"/>
									   		<c:forEach var="countryList" items="${countryList}">
								   				<form:option value="${countryList.countryShortName}" label="${countryList.countryName}" />
								   			</c:forEach>
									</form:select><acronym title="This field is required"><font color="red">&nbsp;*</font></acronym>
								</div>
						
								
				            
					</div>
				</div>
					<div style="height: 5px;">&nbsp;</div>
			<div>
				<div>
					<div  class="PCSTD0001" style="margin-bottom: 10px;">Billing Contact </div>
				</div>
				
				<div style="width: 92%">
					<div  >
						<table id="contactTable" style="table-layout: fixed" width="1000px" cellspacing="1px;">
							<tr >
								
								<th class="TBHED0002" ><fmt:message key="first.name" ></fmt:message> <acronym title="This field is required"><font color="red">*</font></acronym></th>
								<th class="TBHED0002"><fmt:message key="last.name" ></fmt:message> <acronym title="This field is required"><font color="red">*</font></acronym></th>
								<th class="TBHED0002"><fmt:message key="phone" ></fmt:message> <acronym title="This field is required"><font color="red">*</font></acronym></th>
								<th class="TBHED0002"><fmt:message key="email" ></fmt:message></th>
								<th class="TBHED0002"><fmt:message key="cell" ></fmt:message></th>
								<th class="TBHED0002"><fmt:message key="fax" ></fmt:message></th>
							</tr>
							<tr>
								<td>
								<form:input path="firstName"  cssClass="TBTXT0006" disabled="true" />
					            </td>
					            <td>
					            <form:input path="lastName"  cssClass="TBTXT0006" disabled="true" />
					            </td>
					            <td >
					            <form:input path="phone"  cssClass="TBTXT0006" disabled="true" />
					            </td>
					            <td>
					            <form:input path="email"  cssClass="TBTXT0006" disabled="true" />
					            </td>
					            <td>
					            <form:input path="cell"  cssClass="TBTXT0006" disabled="true" />
					            </td>
					            <td>
					            <form:input path="fax"  cssClass="TBTXT0006" disabled="true" />
					            </td>
					           
								        
						</tr>
						<tr style="text-align: right">
							<td colspan="6">
							<input type="button" id="btnUpdate" class="FMBUT0001" value="Update" disabled="disabled" onclick="updateClient()"/>
							<input type="button" id="btnEdit" class="FMBUT0001" value="Edit" onclick="editClientProfile();"/>
							</td>
						</tr>
									
						</table>
					</div>
					
				</div>
				</div>
				<div>&nbsp;</div>
			</form:form>
	</div>
