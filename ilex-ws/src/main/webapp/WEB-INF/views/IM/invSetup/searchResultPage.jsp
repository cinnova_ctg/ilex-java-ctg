<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
 <script type="text/javascript">
 var pageCount;
 var pageStart;
$(function() {
	 pageCount=${invSearchListDTO.pageCount};
	 pageStart=${invSearchListDTO.pageIndex};
 	var clientName ="${invSearchListDTO.clientSearchKeyword}";
 	if(pageCount==1){
 		$(".paginateBar").hide();
 		
 	}

	$("#paginateBar").paginate({
		count 		: pageCount,
		start 		: pageStart,
		display     : 1,
		border					: true,
		border_color			: '#9c9c9c',
		text_color  			: '#000000',
		background_color    	: '#c4c7c8',	
		text_hover_color  		: 'black',
		background_hover_color	: '#9cb2ff',
		images					: false,
		onChange     		: function(page){
		
		$("#clientList").load("./searchclientlist.html?client="+clientName+"&pageIndex=" +page);
		}
	});
});
function closeWindow2 () {
	$('#popUpScreen').unblock();
	$("#popupBody2").empty().html('<img src="${pageContext.request.contextPath}/resources/images/waiting_tree.gif" />');
	$("#pageTitle2").innerHTML = '';	
}
function closeWindow () {
    $.unblockUI(); 
	$("#popupBody").empty().html('<img src="${pageContext.request.contextPath}/resources/images/waiting_tree.gif" />');
	$("#pageTitle").innerHTML = '';	
}
function clientProfile(clientId) {
	document.getElementById("pageTitle").innerHTML ="Client Profile";
	$.blockUI(
			{ 
				message: $('#popUpScreen'),
				css: { 
					width: '1040px',
					height:'420px',
					position:'absolute',
					left:'20px',
					top:'180px',
					color:'black' 
					},
				overlayCSS:  { 
			        backgroundColor: '#f0f0f0', 
			        opacity:         0.3 
			    },
			    centerX: true,  
			    centerY: true,
			    fadeIn:  0,
			    fadeOut: 0
			}
			);
	$("#popupBody").load("${pageContext.request.contextPath}/invoiceSetup/editClientProfile.html?id="+clientId);
	
}
function invoiceProfile(invoiceSetupId) {
	document.getElementById("pageTitle").innerHTML ="Invoice Profile";
	$.blockUI(
			{ 
				message: $('#popUpScreen'),
				css: { 
					width: 'auto',
					
					height:'450px',
					position:'absolute',
					left:'12%',
					top:'180px',
					color:'black' 
					},
				overlayCSS:  { 
			        backgroundColor: '#f0f0f0', 
			        opacity:         0.3 
			    },
			    centerX: true,  
			    centerY: true,
			    fadeIn:  0,
			    fadeOut: 0
			}
			);
	$("#popupBody").load("${pageContext.request.contextPath}/invoiceSetup/editInvoiceProfile.html?id="+invoiceSetupId);
	
}
function reqInfo(invoiceSetupId) {
	document.getElementById("pageTitle").innerHTML ="Client Required Invoice Information";
	$.blockUI(
			{ 
				message: $('#popUpScreen'),
				css: { 
					width: '1000px',
					height:'auto',
					position:'absolute',
					left:'20px',
					top:'180px',
					color:'black' 
					},
				overlayCSS:  { 
			        backgroundColor: '#f0f0f0', 
			        opacity:         0.3 
			    },
			    centerX: true,  
			    centerY: true,
			    fadeIn:  0,
			    fadeOut: 0
			}
			);
	$("#popupBody").load("${pageContext.request.contextPath}/invoiceSetup/editClientReqInvInfo.html?id="+invoiceSetupId);
	
}
function lineItem(invoiceSetupId) {
	document.getElementById("pageTitle").innerHTML ="Line Item Details";
	$.blockUI(
			{ 
				message: $('#popUpScreen'),
				css: { 
					width: 'auto',
					height:'220px',
					position:'absolute',
					left:'20%',
					top:'180px',
					color:'black' 
					},
				overlayCSS:  { 
			        backgroundColor: '#f0f0f0', 
			        opacity:         0.3 
			    },
			    centerX: true,  
			    centerY: true,
			    fadeIn:  0,
			    fadeOut: 0
			}
			);
	$("#popupBody").load("${pageContext.request.contextPath}/invoiceSetup/editLineItemDetails.html?id="+invoiceSetupId);
	
}
function summarySheet(invoiceSetupId) {
	document.getElementById("pageTitle").innerHTML ="Summary Sheet";
	$.blockUI(
			{ 
				message: $('#popUpScreen'),
				css: { 
					width: 'auto',
					height:'170px',
					position:'absolute',
					left:'30%',
					top:'180px',
					color:'black' 
					},
				overlayCSS:  { 
			        backgroundColor: '#f0f0f0', 
			        opacity:         0.3 
			    },
			    centerX: true,  
			    centerY: true,
			    fadeIn:  0,
			    fadeOut: 0
			}
			);
	$("#popupBody").load("${pageContext.request.contextPath}/invoiceSetup/editSummarySheet.html?id="+invoiceSetupId);
	
}

</script>

	<div>
    		 <table class="PCSTD0002" style="table-layout:fixed;width: 1000px;" cellpadding="0px;" cellspacing="1px;">
					<tr  style="height: 20px;background-color: #c4c7c8;" > 
					<td  class="tableHeader" width="20%">Client Name</td>
					<td  class="tableHeader" width="22%">Appendix Name</td>
					<td  class="tableHeader" width="*">View</td>
					</tr>
				<c:forEach var="clientDetail" items="${invSearchListDTO.invSearchDTOList}" varStatus="loopStatus">
					<tr class="${loopStatus.index % 2 == 0 ? 'odd' : 'even'}" height="20px">
						<td width="30%" align="left" style="padding: 5px;white-space:normal;" ><c:out value="${clientDetail.clientName}"/>
						</td>
						<td width="15%""  align="left" style="padding: 5px;white-space:normal;" ><c:out value="${clientDetail.appendixName}"/>
						</td>
						<td width="*"   align="left" style="padding: 5px;" ><a href="#" onclick="clientProfile('<c:out value="${clientDetail.invBillToAddressId}"/>');">Client Profile </a>&nbsp;|&nbsp;<a href="#" onclick="invoiceProfile('<c:out value="${clientDetail.invSetupId}"/>');">Invoice Profile  </a>&nbsp;|&nbsp;<a href="#" onclick="reqInfo('<c:out value="${clientDetail.invSetupId}"/>');">Required Information </a>&nbsp;|&nbsp;<a href="#" onclick="lineItem('<c:out value="${clientDetail.invSetupId}"/>');">Line Items  </a>&nbsp;|&nbsp;<a href="#" onclick="summarySheet('<c:out value="${clientDetail.invSetupId}"/>');">Summary Sheet  </a>&nbsp;|&nbsp;<a href="${pageContext.request.contextPath}/invoiceSetup/deleteClientProfile.html?id=${clientDetail.invBillToAddressId}" onclick="return confirm('Do You Want To Delete Selected Client Invoice Details?');">Delete</a></td>
					</tr>
				</c:forEach>
				<tr  style="background-color: #e7e7e7; text-align: center;" class="paginateBar">
					<td colspan="3" style="padding-left: 73%;padding-top: 5px;" ><div id="paginateBar" ></div></td>
				</tr>
			</table>
		</div>
 <script type="text/javascript">
$(function() {
	if(pageStart==pageCount){
		$(".jPag-control-front").attr('disabled', 'disabled');
		}	
	if(pageStart==1){
		$(".jPag-control-back").attr('disabled', 'disabled');
		}	
	
	
});
</script>
	     
			
