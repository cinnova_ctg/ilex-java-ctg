<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <style type="text/css">
 .contactHeader{
 background: #CCC
 }
 </style>
 <script type="text/javascript">

 function updateLineItemDetails(){
	 $.post("${pageContext.request.contextPath}/invoiceSetup/updateLineItemDetails.html",$("form").serialize(),
			   function(data) {
		$('#popupBody').html(data);
			   });
	 
 }
 function editLineItemDetails(){
	 $("#btnEdit").attr("disabled", "disabled"); 
		$("input[name='lineItemFormat']").removeAttr("disabled"); 
		$("#btnUpdate").removeAttr("disabled"); 
 }
</script>
</head>
<body>


	<div class="WASTD0001" style="text-align: left;margin-right: 20px;">
			<%@ include file="../../common/displayMessages.jsp" %>
			
			<form:form  action="${pageContext.request.contextPath}/invoiceSetup/editLineItemDetails.html" commandName="invSetupDTO" method="POST">
			<form:hidden path="invoiceSetupId"/>
			<div class="PCSTD0007">
						<c:if test="${not empty invMasterLineItemDetailsDTOList}">
							<c:forEach var="invMasterLineItemDetailsDTOList" items="${invMasterLineItemDetailsDTOList}">
								<form:radiobutton path="lineItemFormat" disabled="true" value="${invMasterLineItemDetailsDTOList.lineItemName}"/>${invMasterLineItemDetailsDTOList.lineItemName}
								<br/>
							</c:forEach>
						</c:if>
						</div>
							
							<div style="text-align: right;">
									<input type="button" id="btnUpdate" class="FMBUT0001" value="Update" disabled="disabled" onclick="updateLineItemDetails()"/>
								<input type="button" id="btnEdit" class="FMBUT0001" value="Edit" onclick="editLineItemDetails();"/>
							</div>
							
				
				
			
			</form:form>
				</div>
