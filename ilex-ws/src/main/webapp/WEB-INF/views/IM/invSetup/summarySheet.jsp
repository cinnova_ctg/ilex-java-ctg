<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
  <style type="text/css">
 .contactHeader{
 background: #CCC
 }
 </style>
 <script type="text/javascript">
 $(document).ready(function() {
	 $("#txtLabel").html($("#invSetupDetails\\.invoiceSetupType").val() + "  Options:  ");	 
 });

 
function back(){
	var url="${pageContext.request.contextPath}/invoiceSetup/backToLineItemDetails.html";
	document.forms[0].action= url;
	
	  document.forms[0].submit();
}
</script>
</head>
<body>

	<jsp:include page="appendixInvBreadcrum.jsp"/>

	<div class="WASTD0001">
			<%@ include file="../../common/displayMessages.jsp" %>
			
			<form:form  action="${pageContext.request.contextPath}/invoiceSetup/saveAppendixInvDetail.html" commandName="appendixInvSetupDTO" method="POST">
				<div class="PCSSH0001 PCSSH0004">Summary Sheet</div>
			
				<div><label id="txtLabel" class="PCSTD0001"></label></div>
				<div style="height: 5px;">&nbsp;</div>
				<form:hidden path="client.mbtClientId" />
									<form:hidden path="appendix.appendixId"/>
						<form:hidden path="invSetupDetails.invoiceSetupType"/>
						<form:hidden path="invSetupDetails.invoiceSetupId"/>
							<form:hidden path="billToAddress.invBillToAddressId" />
					
						<div class="PCSTD0007"><form:radiobutton path="invSetupDetails.summarySheet" value="List of Sites"/>List of Sites - site number and address information</div>
						<div class="PCSTD0007"><form:radiobutton path="invSetupDetails.summarySheet" value="List of All Activities"/>List of All Activities-sum of activities across all jobs</div>
						<div class="PCSTD0007"><form:radiobutton path="invSetupDetails.summarySheet" value="Not Applicable"/>Not Applicable</div>
							
							<div>
								<input type="button" id="btnBack" class="FMBUT0001" value="<fmt:message key="mbt.btn.back" ></fmt:message>" onclick="back();"/>
								<input type="submit" id="btnGo" class="FMBUT0001" value="Save"/>
							</div>
							
		
			</form:form>
				</div>

</body>
</html>