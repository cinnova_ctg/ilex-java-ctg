<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
 <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jMenu.js"></script>

  <style type="text/css">
 .contactHeader{
 background: #CCC
 }
     
#jQ-menu ul {
	list-style-type: none;
}

#jQ-menu li {
	text-decoration: none;
}
 </style>
 <script type="text/javascript">
 $(document).ready(function() {
	 $("input[name='invSetupDetails.invoiceSetupType']").change(function(){
		 if("Consolidated" ==$("input[name='invSetupDetails.invoiceSetupType']:checked").val()){
			 $("input[name='invSetupDetails.invoiceSetupGroupingType']").attr("checked", false);
			 $("input[name='invSetupDetails.invoiceSetupGroupingType']").attr("disabled", "disabled"); 
			 
			 
		 }
		 if("Individual" ==$("input[name='invSetupDetails.invoiceSetupType']:checked").val()){
			 $("input[name='invSetupDetails.invoiceSetupGroupingType']").removeAttr("disabled");
			 
			 
		 }
		 
		 
	 });
	 
	
	 $(".floatValue").val(parseFloat($(".floatValue").val()).toFixed(4));
	 
	 $(".floatValue").change(function() {
		if( isNaN($(".floatValue").val())){
			alert("Please Enter Correct Tax Value");
		}else{
			 $(".floatValue").val(parseFloat($(".floatValue").val()).toFixed(4));
		};
		  });
	
 });
function back(){
	var url="${pageContext.request.contextPath}/invoiceSetup/backToClientProfile.html";
	document.forms[0].action= url;
	
	  document.forms[0].submit();
}
function validate(){
	if("Individual" ==$("input[name='invSetupDetails.invoiceSetupType']:checked").val()){
		if($("input[name='invSetupDetails.invoiceSetupGroupingType']:checked").val()==undefined){
			alert("Select Invoice Type Seperate or Combine");
			return false;
		}
	};
	
	if( isNaN($(".floatValue").val())){
		alert("Please Enter Correct Tax Value");
		return false;
	}else{
		 $(".floatValue").val(parseFloat($(".floatValue").val()).toFixed(4));
	};
	
}

</script>
</head>
<body>

	<jsp:include page="appendixInvBreadcrum.jsp"/>

	<div class="WASTD0001">
			<%@ include file="../../common/displayMessages.jsp" %>
			
			<form:form  action="${pageContext.request.contextPath}/invoiceSetup/clientReqInvInfo.html" commandName="appendixInvSetupDTO" method="POST" onsubmit="return validate()">
				<div class="PCSSH0001 PCSSH0004">Invoice Profile</div>
				
				<form:hidden path="client.mbtClientId" />
					<form:hidden path="appendix.appendixId"/>
					<form:hidden path="billToAddress.invBillToAddressId" />
					<form:hidden path="invSetupDetails.invoiceSetupId"/>
						<div id="labelDiv" class="divFloatLeft PCSTD0001" style="width: 110px;">
										<div  style="margin-bottom: 17px;" >Page Layout :</div>
										<div style="margin-bottom: 20px;">Address:</div>
										<div style="margin-bottom:48px;">Invoice Type:</div>
										<div>Time Frame:</div>
										
							</div>
							<div class="PCSTD0007">
										<div  style="margin-bottom: 11px;"><form:radiobutton path="invSetupDetails.pageLayout" value="Standard-1"/>Standard</div>
										<div class="divFloatLeft" style="width: 300px;" ><form:radiobutton path="invSetupDetails.contingentAddress" value="Contingent Network Services, LLC"/>Contingent Network Services, LLC</div>
										<div><form:radiobutton path="invSetupDetails.contingentAddress"  value="Contingent Network Services, International"/>Contingent Network Services, International</div><br/>
										<div><form:radiobutton path="invSetupDetails.invoiceSetupType"  value="Individual" />Individual &nbsp;&nbsp;&nbsp;(<form:radiobutton path="invSetupDetails.invoiceSetupGroupingType" value="Separate" />Separate  or <form:radiobutton path="invSetupDetails.invoiceSetupGroupingType" value="Combined"  />Combined )</div>
										<div  style="margin-left: 140px;">or</div>
										<div style="margin-bottom: 11px;"><form:radiobutton path="invSetupDetails.invoiceSetupType"  value="Consolidated"/>Consolidated</div>
										<div><form:radiobutton path="invSetupDetails.timeFrame" value="Offsite" />Offsite, &nbsp;&nbsp;&nbsp;<form:radiobutton path="invSetupDetails.timeFrame" value="Weekly" />Weekly, &nbsp;&nbsp;&nbsp;<form:radiobutton path="invSetupDetails.timeFrame" value="Semimonthly" />Bi-monthly(1<sup>st</sup> and 15<sup>th</sup>), &nbsp;&nbsp;&nbsp;or&nbsp;&nbsp;<form:radiobutton path="invSetupDetails.timeFrame" value="Monthly" />Monthly &nbsp;&nbsp;&nbsp;</div>
							</div>
						<div style="height: 10px;">&nbsp;</div>
						<div id="jQ-menu" style="text-align: left;margin: 0px;padding-bottom: 10px;">
						     <ul  class="PCSTD0007" style="text-align: left;margin: 0px;padding: 0px;">
						     <li style="text-align: left;margin: 0px;padding: 0px;"><span class="toggle"><font>Notes:</font></span>
						     <ul>
						     <li>
						     <div class="PCSTD0007">
						     <pre style="font-family: inherit;">
The invoice type defines how a job is invoiced to a client. The two options available are an individual job or as a consolidation of
multiple jobs. An individual job invoice is simply invoiced on the basis of a job and it’s extended price. The consolidated invoices
sums one or more jobs into a single billable amount which becomes undividable meaning the client is expected to pay the total
amount. Individual jobs may still be grouped together for administrative convenience but represent individual job invoices . All 
invoices are assigned  an invoice date(described below) and a unique invoice number.

Individual invoice are eligible to be invoiced when the job is marked offsite and thus the invoice will use the offsite date(last 
offsite date). A consolidated invoice will use the latest offsite date from all grouped jobs.
 
</pre>
								
						     </div>
			</li>
						     </ul>
						     </li>
						   
							</ul>
								
							</div>
								<div  class="divFloatLeft PCSTD0001" style="width: 195px;">Include Outstanding Balance:</div>
								<div class="PCSTD0007"><form:radiobutton path="invSetupDetails.outstandingBalanceExist" value="true"/>Yes &nbsp;&nbsp;<form:radiobutton path="invSetupDetails.outstandingBalanceExist" value="false"/>No</div>
							<div style="height: 10px;">&nbsp;</div>
							<div style="width: 140px;"class="divFloatLeft PCSTD0001 " >Include Tax At Rate:</div>	<div class="PCSTD0007" ><form:input path="invSetupDetails.collectedTax"  cssClass="floatValue PCTXT0006" size="1"/></div>
							<div>&nbsp;</div>
							<div style="width: 790px;;text-align: right;">
								<input type="button" id="btnBack" class="FMBUT0001" value="<fmt:message key="mbt.btn.back" ></fmt:message>" onclick="back();"/>
								<input type="submit" id="btnGo" class="FMBUT0001" value="<fmt:message key="mbt.btn.go" ></fmt:message>"/>
							</div>
				
				
			
			</form:form>
				</div>



</body>
</html>