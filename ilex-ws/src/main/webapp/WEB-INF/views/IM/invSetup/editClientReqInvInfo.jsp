
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jMenu.js"></script>

  <style type="text/css">
 .contactHeader{
 background: #CCC
 }
 .jQ-menu ul {
	list-style-type: none;
}

.jQ-menu li {
	text-decoration: none;
}
 </style>
 <script type="text/javascript">
 var selectedRow;
 $(document).ready(function() {
	
	 $(".delete").hide();
	 if($("input[name=invSetupDetails.customerReferenceSource]:checked").val()=='Job'){
		  $(".customerReferenceSource").attr('disabled', true);
	 }
	 var rowCount = $('.additionalInformationTable tr').length;
	 if(rowCount==1){
		 $('.additionalInformationTable tr').hide();
		 
	 }
	
	 $('input[name=invSetupDetails.customerReferenceSource]:radio').change(function(){
		    var val = $("input[name=invSetupDetails.customerReferenceSource]:checked").val();
		    if (val == 'Invoice') {
		    	 $(".customerReferenceSource").val("");
		    	  $(".customerReferenceSource").removeAttr('disabled');
		    } else if (val == 'Not Used'){	 
	    		$(".customerReferenceSource").removeAttr('disabled');
	    		$(".customerReferenceSource").val("Not required for this invoice");
		    	 }else{
		    		 $(".customerReferenceSource").val("");
		    	  $(".customerReferenceSource").attr('disabled', true);
		      
		    }
	});
 });
 
 function addAdditionalInformation(){
	 var rowCount = $('.additionalInformationTable tr').length - 1  ;
	 $('.additionalInformationTable tr').show();
	 var nameCell="<td><input type=\"text\" class=\"TBTXT0004\" name=\"additionalInformationList[" + rowCount + "].name\" id=\"additionalInformationList["+ rowCount + "].name\"/></td>";
	
	 var levelCell="<td  style=\"white-space: nowrap;\" class=\"PCSTD0007\"><input type=\"radio\"  name=\"additionalInformationList[" + rowCount + "].level\" id=\"additionalInformationList["+ rowCount + "].level\"  value=\"Invoice\" onclick=\"setReadAttr("+ rowCount + ",false);\" />Invoice&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"radio\" name=\"additionalInformationList[" + rowCount + "].level\" id=\"additionalInformationList["+ rowCount + "].level\" onclick=\"setReadAttr("+ rowCount + ",true);\"  value=\"Job\" />Job</td>";
	 var valueCell="<td><input type=\"text\" class=\"TBTXT0007\" name=\"additionalInformationList[" + rowCount + "].value\" id=\"additionalInformationList["+ rowCount + "].value\" onfocus=\"getValue("+ rowCount + ")\"  /></td>";
	 var descriptionCell="<td><input type=\"text\" class=\"TBTXT0007\" name=\"additionalInformationList[" + rowCount + "].description\" id=\"additionalInformationList["+ rowCount + "].description\" /></td>";
	 var deleteButtonCell="<td>&nbsp;&nbsp;<a class=\"delete\"><img src=\"${pageContext.request.contextPath}/resources/images/deleteBtn.jpg\"/></a></td>";
  var newAdditionalInfoRow = "<tr>" +nameCell +levelCell+valueCell+ descriptionCell+deleteButtonCell+"</tr>";
	  $('.additionalInformationTable')
	                    .append(newAdditionalInfoRow);	
	 }
 $(".delete").live('click', function(event) {
	  $(this).parent().parent().remove();
	 var rowCount = $('.additionalInformationTable tr').length;
	 if(rowCount==1){
		 $('.additionalInformationTable tr').hide();
   }
});

function getValue(rowNumber) {
	if($("input[name='additionalInformationList["+rowNumber+"].level']:checked").val()=='Invoice'){
		$("input[name='additionalInformationList["+rowNumber+"].value']").removeAttr('readonly');
		$("input[name='additionalInformationList["+rowNumber+"].value']").val("");
		
	}
	else if($("input[name='additionalInformationList["+rowNumber+"].level']:checked").val()=='Job'){
		$("input[name='additionalInformationList["+rowNumber+"].value']").attr('readonly', 'readonly');
		document.getElementById("pageTitle2").innerHTML ="Additional Client Required Information";
		$('#popUpScreen').block({ message: $('#popUpScreen2'),
			css: { 
				width: 'auto',
				height:'auto',
				position:'absolute',
				left:'30%',
				top:'180px',
				color:'black' 
				},
			overlayCSS:  { 
		        backgroundColor: '#f0f0f0', 
		        opacity:         0.3 
		    },
		    centerX: true,  
		    centerY: true,
		    fadeIn:  0,
		    fadeOut: 0
			});


		$("#popupBody2").load("${pageContext.request.contextPath}/invoiceSetup/getCustInfoParameter.html?appendixId=" + $('#invSetupDetails\\.appendixId').val());
	//	var retval=window.showModalDialog("${pageContext.request.contextPath}/invoiceSetup/getCustInfoParameter.html?appendixId=" + $('#invSetupDetails\\.appendixId').val());
		//		$("input[name='additionalInformationList["+rowNumber+"].value']").val(retval);
		selectedRow= rowNumber;
	}
	
	
	
}
function setValue(){
	$("input[name='additionalInformationList["+selectedRow+"].value']").val($("input[name='selectedcustomerInfoParameter']:checked").val());
	closeWindow2();
}

function editClientInfo(){
	$("#btnEdit").attr("disabled", "disabled"); 
	$("input[name='invSetupDetails.customerReferenceSource']").removeAttr("disabled"); 
	$("input[name='invSetupDetails.customerReferenceName']").removeAttr("disabled"); 
	$("input[name='invSetupDetails.customerReferenceValue']").removeAttr("disabled"); 
	$("input[name='invSetupDetails.customerReferencePoc']").removeAttr("disabled"); 
	$("#btnUpdate").removeAttr("disabled"); 
	$("#btnAdd").removeAttr("disabled"); 
	 $(".delete").show(); 
	var rowCount = $('.additionalInformationTable tr').length - 1  ;
	for(var i=0;i<rowCount;i++){
		$("input[name='additionalInformationList[" + i + "].name']").removeAttr("disabled"); 
		$("input[name='additionalInformationList[" + i + "].level']").removeAttr("disabled"); 
		$("input[name='additionalInformationList[" + i + "].value']").removeAttr("disabled"); 
		$("input[name='additionalInformationList[" + i + "].description']").removeAttr("disabled"); 
	}
	if($("input[name=invSetupDetails.customerReferenceSource]:checked").val()=='Job'){
		  $(".customerReferenceSource").attr('disabled', true);
	 }
	
}
function updateClientInfo(){
	if($("input[name='invSetupDetails.customerReferenceSource']:checked").val()=='Invoice'){
		if($("input[name='invSetupDetails.customerReferenceValue']").val()==''){
			alert("Enter Value For Invoice");
			return false;
		}
		
	}
	for(var i=0;i <=$('.additionalInformationTable tr').length-2;i++){
		if($("input[name='additionalInformationList["+i+"].name']").length && $("input[name='additionalInformationList["+i+"].name']").val()==""){
			alert("Enter Additional Information Name ");
			return false;
			
		}
		
		if($("input[name='additionalInformationList["+i+"].level']").length && $("input[name='additionalInformationList["+i+"].level']:checked").val()==null){
			alert("Select Additional Information Level");
			return false;
		} 
		if($("input[name='additionalInformationList["+i+"].value']").length && $("input[name='additionalInformationList["+i+"].value']").val()==""){
			alert("Enter Additional Information Value ");
			return false;
			
		}	
	}
	$.post("${pageContext.request.contextPath}/invoiceSetup/updateClientReqInvInfo.html",$("form").serialize(),
			   function(data) {
		$('#popupBody').html(data);
			   });	
	
}
function setReadAttr(rowNumber,flag){
	$("input[name='additionalInformationList["+rowNumber+"].value']").val("");
	if(flag){
		$("input[name='additionalInformationList["+rowNumber+"].value']").attr('readonly', 'readonly');
	}else{
		$("input[name='additionalInformationList["+rowNumber+"].value']").removeAttr('readonly');
	}
}

</script>

	<div class="WASTD0001" style="text-align: left;">
			<%@ include file="../../common/displayMessages.jsp" %>
			
			<form:form  action="${pageContext.request.contextPath}/invoiceSetup/lineItemDetails.html" commandName="appendixInvSetupDTO" method="POST">
						<div class="PCSTD0001" >Customer Reference/Purchase Order</div>
						<form:hidden path="invSetupDetails.appendixId"/>
						<form:hidden path="invSetupDetails.invoiceSetupId"/>
						<div id="labelDiv" class="divFloatLeft PCSTD0002" style="margin-left: 50px;width: 155px;">
										<div  style="margin-bottom: 10px;" >Source :</div>
										<div style="margin-bottom: 10px;">Show as:</div>
										<div style="margin-bottom:10px;">Value if Source =Invoice:</div>
										<div>Accounting Contact:</div>
						</div>
						<div>
										<div class="PCSTD0008"  style="margin-bottom: 5px;"><form:radiobutton disabled="true" path="invSetupDetails.customerReferenceSource" value="Job" />Job&nbsp;&nbsp;<form:radiobutton disabled="true" path="invSetupDetails.customerReferenceSource" value="Invoice"/>Invoice ,  or&nbsp;&nbsp;<form:radiobutton path="invSetupDetails.customerReferenceSource" disabled="true" value="Not Used"/> <font color="red">Not Used</font></div>
										<div style="margin-bottom: 5px;"><form:input disabled="true"  cssClass="PCTXT0004" path="invSetupDetails.customerReferenceName"/><font class="PCSTD0007" style="font-size: 9px;">&nbsp;&nbsp;&nbsp;The name displayed on the invoice instead of Customer Reference.</font></div>
										<div  style="margin-bottom: 5px;"><form:input disabled="true"  cssClass="PCTXT0004 customerReferenceSource" path="invSetupDetails.customerReferenceValue" /><font class="PCSTD0007" style="font-size: 9px;">&nbsp;&nbsp;&nbsp;Required for Source = Invoice otherwise blank.</font></div>
										<div><form:input disabled="true" cssClass="PCTXT0004" path="invSetupDetails.customerReferencePoc"/><font class="PCSTD0007" style="font-size: 9px;">&nbsp;&nbsp;&nbsp;Enter name and phone number, email address as applicable.</font></div>
						</div>
						<div>&nbsp;</div>
							<div class="jQ-menu" style="text-align: left;margin: 0px;padding-bottom: 10px;">
						     <ul  class="PCSTD0007" style="text-align: left;margin: 0px;padding: 0px;">
						     <li style="text-align: left;margin: 0px;padding: 0px;"><span class="toggle"><font>Notes:</font></span>
						     <ul>
						     <li>
						     <div class="PCSTD0007">
						     <pre  style="font-family: inherit;">
A customer reference refers to the clients’s explicit authorization to bill for work performed under an appendix.Clients may also refer
to it as a Purchase Order number. This is generally provided as a number or code.

The customer reference setup is required. Two pieces of information are required to setup the customer reference.

First , the source must be defined. There are currently three source for a customer reference : 1) Job, 2)Invoice , or 3)Not Used.

Second ,the customer’s naming convention is captured. This is what the client calls the authorization to bill. We refer to it as the 
"Customer Reference",but a customer may have a different name for it like purchase Order.

Based on the selection of source,the form will enforce additional data entry or set certain fields to default values. Follow the alerts 
provided.

An optional third piece of information is who to contact for the customer reference. Include name,phone ,etc.if the person is
 different than the client contact setup earlier.

</pre>
								
						     </div>
			</li>
						     </ul>
						     </li>
						   
							</ul>
							</div>
							<div class="PCSTD0001 divFloatLeft">Additional Client Required Information</div><div  style="width: 950px;text-align: right;"><input type="button" disabled="disabled" id="btnAdd" class="FMBUT0002" value="Add" onclick="addAdditionalInformation();"/></div>
							<div>&nbsp;</div>
							<div>
								<table class="additionalInformationTable">
									<tr>
										<th class="TBHED0002" >Name</th>
										<th class="TBHED0002">Level</th>
										<th class="TBHED0001">Value</th>
										<th class="TBHED0001">Description</th>
										<th>&nbsp;</th>
									</tr>
										<c:if test="${not empty appendixInvSetupDTO.additionalInformationList}">
											<c:forEach varStatus="additionalInformationList" items="${appendixInvSetupDTO.additionalInformationList}">
												<tr>
													<td>
														<spring:bind path="appendixInvSetupDTO.additionalInformationList[${additionalInformationList.index}].name">
															<input type="text" disabled="disabled" class="TBTXT0004"  name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
										            	</spring:bind>
										            </td>
													<td style="white-space: nowrap;" class="PCSTD0007">
													<spring:bind path="appendixInvSetupDTO.additionalInformationList[${additionalInformationList.index}].level">
															<input type="radio" disabled="disabled" name="<c:out value="${status.expression}"/>"   id="<c:out value="${status.expression}"/>" value="Invoice" <c:if test="${status.value == 'Invoice'}"> checked="checked"</c:if> onclick="setReadAttr(${additionalInformationList.index},false);" />Invoice &nbsp;&nbsp;
															<input type="radio" disabled="disabled" name="<c:out value="${status.expression}"/>"   id="<c:out value="${status.expression}"/>" value="Job" <c:if test="${status.value == 'Job'}"> checked="checked"</c:if> onclick="setReadAttr(${additionalInformationList.index},true);" />Job
										            	</spring:bind>
													</td>
													<td>
														<spring:bind path="appendixInvSetupDTO.additionalInformationList[${additionalInformationList.index}].value">
															<input type="text" disabled="disabled" class="TBTXT0007" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" onfocus="getValue(${additionalInformationList.index})"/>
										            	</spring:bind>
										            </td>
													<td>
														<spring:bind path="appendixInvSetupDTO.additionalInformationList[${additionalInformationList.index}].description">
															<input type="text" disabled="disabled" class="TBTXT0007"   name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
										            	</spring:bind>
										            </td>
													<td>&nbsp;
														<a class="delete"><img src="${pageContext.request.contextPath}/resources/images/deleteBtn.jpg"/></a>
													</td>
												</tr>
											</c:forEach>
									</c:if>
								</table>
							</div>
								<div class="jQ-menu" style="text-align: left;margin: 0px;">
						     <ul  class="PCSTD0007" style="text-align: left;margin: 0px;padding: 0px;">
						     <li style="text-align: left;margin: 0px;padding: 0px;"><span class="toggle"><font>Notes:</font></span>
						     <ul>
						     <li>
						     <div class="PCSTD0007">
						     <pre  style="font-family: inherit;">
The client may require specific information to accept a contingent invoice beyond the customer reference described above. If 
required,this information will be setup at the invoice level using the above fields.  
</pre>
								
						     </div>
			</li>
						     </ul>
						     </li>
						   
							</ul>
	</div>
							
							<div style="width: 950px;text-align: right;">
								<input type="button" id="btnUpdate" class="FMBUT0001" value="Update" disabled="disabled" onclick="updateClientInfo()"/>
								<input type="button" id="btnEdit" class="FMBUT0001" value="Edit" onclick="editClientInfo();"/>
							</div>
							<div>&nbsp;</div>
							
				
				
			
			</form:form>
				</div>
