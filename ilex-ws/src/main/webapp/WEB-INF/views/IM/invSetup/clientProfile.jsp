<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
  <style type="text/css">
 .contactHeader{
 background: #CCC
 }
 </style>
 <script type="text/javascript">
 $(document).ready(function() {
		 });

function back(){
	var url="${pageContext.request.contextPath}/invoiceSetup/appendixList.html";
	document.forms[0].action= url;
	
	  document.forms[0].submit();
}
function go(){
	
	if($.trim( $("#client\\.clientName").val())==""){
		alert("Please Enter Client Name");
		return false;
	}
	if($.trim( $("#client\\.address1").val())==""){
		alert("Please Enter Address ");
		return false;
	}
	if($.trim( $("#client\\.city").val())==""){
		alert("Please Enter City ");
		return false;
	}
	
	if($.trim( $("#client\\.state").val())==""){
		alert("Please Select State ");
		return false;
	}
	if($.trim( $("#client\\.country").val())==""){
		alert("Please Select Country ");
		return false;
	}
	if($("input[name='selectedClient']").length>0 ){
		if( $("input[name='selectedClient']:checked").val()==undefined){
			alert("Please Select one contact");
			return false;
		}
	}
	if( $("input[name='selectedClient']:checked").val()!=undefined){
		
		var j= $("input[name='selectedClient']:checked").val();
		if($("input[name='contactDetailsList["+j+"].firstName']").length && $.trim( $("input[name='contactDetailsList["+j+"].firstName']").val())==""){
			alert("Please Enter First Name For Selected Contact ");
			return false;
		} ;	
		if($("input[name='contactDetailsList["+j+"].lastName']").length && $.trim( $("input[name='contactDetailsList["+j+"].lastName']").val())==""){
			alert("Please Enter Last Name For Selected Contact ");
			return false;
		} ;
		if($("input[name='contactDetailsList["+j+"].phone']").length && $.trim( $("input[name='contactDetailsList["+j+"].phone']").val())==""){
			alert("Please Enter phone For Selected Contact ");
			return false;
		} ;
	}

	var url="${pageContext.request.contextPath}/invoiceSetup/invoiceProfile.html";
	$('#contactId').val($("input[@name='selectedContactId']:checked").val());
	document.forms[0].action= url;
	  document.forms[0].submit();
}

</script>
</head>
<body>

	<jsp:include page="appendixInvBreadcrum.jsp"/>
		

	<div class="WASTD0001">
			<%@ include file="../../common/displayMessages.jsp" %>
			
			<form:form  action="${pageContext.request.contextPath}/invoiceSetup/invoiceProfile.html" commandName="appendixInvSetupDTO" method="POST" >
			<div class="PCSSH0001 PCSSH0004">Client Profile</div>
					<form:hidden path="client.mbtClientId" />
					<form:hidden path="appendix.appendixId"/>
						<form:hidden path="billToAddress.invBillToAddressId" />
						<form:hidden path="client.contactExist"/>
					<div style="margin-bottom: 20px;">
						<div id="labelDiv" class="divFloatLeft PCSTD0001" style="width: 130px;">
							<div style="margin-bottom:18px;">Client Name:</div>
							<div>Delivery Method: </div>
						</div>
						<div  class="PCSTD0002"id="rightDiv">
							<div style="margin-bottom: 10px;">
								<form:input path="client.clientName"  cssClass="PCTXT0004"/>
								<acronym title="This field is required"><font color="red">*</font></acronym>
							</div>
							
							<div>
								<form:select path="client.deliveryMethod" cssClass="PCCOMBO002"  >
								   		<form:option value="1" label="Email" />
								   		<form:option value="2" label="Fax" />
								   		<form:option value="3" label="Mail" />
								   		<form:option value="0" label="To Be Determined" />
								</form:select>
								<acronym title="This field is required"><font color="red">*</font></acronym>
							</div>
						</div>
					</div>
				<div class="PCSTD0001" style="margin-bottom: 5px;">Bill To Address</div>
				
					<div id="labelDiv" class="divFloatLeft PCSTD0002" style="margin-left: 50px;width: 80px;;">
								<div  style="margin-bottom: 30px;" ><fmt:message key="address.label"/>:</div>
									<div style="margin-bottom: 10px;"><fmt:message key="city.label"/>:</div>
									<div style="margin-bottom: 8px;"><fmt:message key="state.label"/>:</div>
									<div style="margin-bottom: 8px;"><fmt:message key="zipcode.label"/>:</div>
									<div style="margin-bottom: 8px;"><fmt:message key="country.label"/>:</div>
								
					</div>
					<div  class="PCSTD0002"id="rightDiv" style="width: 85%;">
							<div>
								<div style="margin-bottom: 4px;"><form:input path="client.address1"  cssClass="PCTXT0004" />
								<acronym title="This field is required"><font color="red">*</font></acronym>
								</div>
								
								<div style="margin-bottom: 4px;"><form:input path="client.address2" cssClass="PCTXT0004" /></div>
							</div>
							<div style="margin-bottom: 4px;">
								<form:input path="client.city" cssClass="PCTXT0004" />
								<acronym title="This field is required"><font color="red">*</font></acronym>
							</div>
							<div style="margin-bottom: 4px;">
								<form:select path="client.state" cssClass="PCCOMBO002" >
									<form:option value="" label="Select a State"/>
									<c:forEach var="state" items="${stateList}">
								   				<form:option value="${state.stateShortName}" label="${state.stateName}" />
								   	</c:forEach>
								</form:select>
								  <acronym title="This field is required"><font color="red">*</font></acronym>
							</div>
							<div style="margin-bottom: 4px;">
								<div><form:input path="client.zipCode"  cssClass="PCTXT0005" />
							</div>
							<div style="margin-top: 4px;" >
								<form:select path="client.country" cssClass="PCCOMBO002" >
									<form:option value="" label="Select a Country"/>
									<c:forEach var="countryList" items="${countryList}">
								   				<form:option value="${countryList.countryShortName}" label="${countryList.countryName}" />
								   	</c:forEach>
								</form:select>
								  <acronym title="This field is required"><font color="red">*</font></acronym>
							</div>
					</div>
				</div>
					<div style="height:5px;">&nbsp;</div>
			<div>
				<c:if test="${not empty appendixInvSetupDTO.contactDetailsList}">
					<div>
						<div  class="PCSTD0001" style="margin-bottom: 10px;">Billing Contact<font style="font-weight: normal;" > (Select one contact.)</font> </div>
					</div>
				</c:if>
				
				<div style="width: 920px;">
					<div  >
					<c:if test="${not empty appendixInvSetupDTO.contactDetailsList}">
						<table class="billingContactTable" cellspacing="1px;">
							<tr >
								<th>&nbsp;</th>
								<th class="TBHED0001" >First Name  <acronym title="This field is required"><font color="red">*</font></acronym> </th>
								<th class="TBHED0001">Last Name  <acronym title="This field is required"><font color="red">*</font></acronym> </th>
								<th class="TBHED0002">Phone  <acronym title="This field is required"><font color="red">*</font></acronym></th>
								<th class="TBHED0001">Email </th>
								<th class="TBHED0002">Cell </th>
								<th class="TBHED0002">FAX </th>
							</tr>
							
								<c:forEach varStatus="contactList" items="${appendixInvSetupDTO.contactDetailsList}">
										<tr>
										 <td>
											<spring:bind path="appendixInvSetupDTO.contactDetailsList[${contactList.index}].invContactId">
													<input type="hidden" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
											</spring:bind>
											
											<form:radiobutton path="selectedClient" value="${contactList.index}"/>
								            </td>
								            <td>
												<spring:bind path="appendixInvSetupDTO.contactDetailsList[${contactList.index}].firstName">
												<input type="text" class="TBTXT0004" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
									            </spring:bind>
								            </td>
								              <td>
												<spring:bind path="appendixInvSetupDTO.contactDetailsList[${contactList.index}].lastName">
												<input type="text" class="TBTXT0004" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
									            </spring:bind>
								            </td>
								              <td>
												<spring:bind path="appendixInvSetupDTO.contactDetailsList[${contactList.index}].phone">
												<input type="text" class="TBTXT0005" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
									            </spring:bind>
								            </td>
								              <td>
												<spring:bind path="appendixInvSetupDTO.contactDetailsList[${contactList.index}].email">
												<input type="text" class="TBTXT0004" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
									            </spring:bind>
								            </td>
								              <td>
												<spring:bind path="appendixInvSetupDTO.contactDetailsList[${contactList.index}].cell">
												<input type="text" class="TBTXT0005" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
									            </spring:bind>
								            </td>
								              <td>
												<spring:bind path="appendixInvSetupDTO.contactDetailsList[${contactList.index}].fax">
												<input type="text" class="TBTXT0005" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
									            </spring:bind>
								            </td>
										</tr>
									</c:forEach>
							
						</table>
							</c:if>
					</div>
					<div style="float: right;">
						<input type="button" id="btnBack" class="FMBUT0001" value="<fmt:message key="mbt.btn.back" ></fmt:message>" onclick="back();"/>
						<input type="button" id="btnGo" class="FMBUT0001" value="<fmt:message key="mbt.btn.go" ></fmt:message>" onclick="return go();"/>
					</div>
				</div>
				
				
				</div>
			</form:form>
	</div>



</body>
</html>