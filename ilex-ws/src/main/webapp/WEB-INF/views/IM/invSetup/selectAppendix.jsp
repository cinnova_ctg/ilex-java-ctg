<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
 <style type="text/css">
 .appendixListHeader{
 background: #CCC
 }
 </style>
</head>
<script type="text/javascript">
function back(){
	var url="${pageContext.request.contextPath}/invoiceSetup/client.html";
	document.forms[0].action= url;
	
	  document.forms[0].submit();
}
function validate(){
	if($("input[name='appendix.appendixId']:checked").val()==undefined){
		alert("Select Appendix");
		return false;
	}
}

</script>
<body>

	<jsp:include page="appendixInvBreadcrum.jsp"/>
	

	<div class="WASTD0001">
			<%@ include file="../../common/displayMessages.jsp" %>
			<form:form  action="${pageContext.request.contextPath}/invoiceSetup/clientProfile.html" commandName="appendixInvSetupDTO" method="POST" onsubmit="return validate();">
			<div class="MSERR0001">
				<form:errors path="client.mbtClientId" cssClass="MSERR0001"/>
			</div>
				<div>
				<form:hidden path="client.mbtClientId"/>
					<div class="PCSSH0001 PCSSH0003"><c:out value="${appendixInvSetupDTO.client.clientName}"/></div>
					<div class="appendixList ">
						<table style="width: 435px;" CELLSPACING="0" cellpadding="0" > 
							<tr><td>&nbsp;</td>
							<td class="PCSTD0008"  style="text-align: center;padding:4px; background: #CCC"><fmt:message key="appendix" ></fmt:message></td>
						</tr>
							<c:forEach var="appendixs" items="${appendixList}" varStatus="loopStatus">
								<tr   class="${loopStatus.index % 2 == 0 ? 'odd' : 'even'}" >
									<td  class="FMRAD0001" style="background-color:white;" width="10px;" ><form:radiobutton path="appendix.appendixId" value="${appendixs.appendixId}" /></td>
									<td class="PCSTD0007" style="padding:4px;">${appendixs.appendixName}</td>
								</tr>
							</c:forEach>
							<tr>
							<td>&nbsp;</td>
							<td>
								<input type="button" id="btnBack" class="FMBUT0001" value="<fmt:message key="mbt.btn.back" ></fmt:message>" onclick="back();"/>
								<input type="submit" id="btnGo" class="FMBUT0001" value="<fmt:message key="mbt.btn.go" ></fmt:message>"/>
							</td>
							</tr>
							</table>
						
					</div>
				</div>
				<div>
					
			    </div>
				
				
			</form:form>
	</div>



</body>
</html>