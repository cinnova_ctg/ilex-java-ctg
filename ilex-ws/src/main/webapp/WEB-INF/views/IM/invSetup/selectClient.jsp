<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
</head>
<script>
function validate(){

	if($("#client\\.mbtClientId").val()==""){
		alert("Please select a Client");
		return false;
	}
	
	    
}
</script>
<body>

	<jsp:include page="appendixInvBreadcrum.jsp"/>


	<div class="WASTD0001">
		<%@ include file="../../common/displayMessages.jsp" %>
			
			<form:form  action="${pageContext.request.contextPath}/invoiceSetup/appendixList.html" commandName="appendixInvSetupDTO" method="POST"  onsubmit="return validate();">
			<div class="MSERR0001">
				<form:errors path="client.clientId" cssClass="MSERR0001"/>
			</div>
				<div class="PCSTD0001 divFloatLeft"  style="width: 100px;padding-bottom: 5px;padding-top: 5px;"><fmt:message key="selectClient" ></fmt:message></div>
				<div>
					<div class="divFloatLeft PCSTD0002">
						<form:select path="client.mbtClientId" cssClass="PCCOMBO001"  >
						 	<form:option value="" label="--- Select ---"/>
					   		<form:options items="${clientList}" itemLabel="clientName" itemValue="mbtClientId" />
						</form:select>
					</div>
							<div><input type="submit" id="btnGo" class="FMBUT0002" value="<fmt:message key="mbt.btn.go" ></fmt:message>"/></div>
				</div>
				
				
			</form:form>
	</div>



</body>
</html>