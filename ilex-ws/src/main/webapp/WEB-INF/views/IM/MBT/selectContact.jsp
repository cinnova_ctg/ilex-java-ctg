<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
<script>
function back(){
	var url="${pageContext.request.contextPath}/masterbtm/division.html";
	document.forms[0].action= url;
	  document.forms[0].submit();
}
function validate(){

	if (!$("input[@name='poc.pocId']:checked").val())
	     {
	       alert("Please select a Contact");
	       return false;
	       }
	      
}
</script>
</head>
<body>
    
	<jsp:include page="newBillToMasterBreadCrumb.jsp"/>
	<%@ include file="../../common/displayMessages.jsp" %>

<div class="WASTD0001">
	<form:form  action=" ${pageContext.request.contextPath}/masterbtm/deliverymethod.html" commandName="masterBTMDTO" method="POST" id ="selectContactForm" onsubmit="return validate();">
		<div class="MSERR0001"> 
				<form:errors path="client.clientId"></form:errors> 
				<form:errors path="division.divisionId"></form:errors> 
				<form:errors path="poc.pocId"></form:errors>
		</div>  
			<form:hidden path="client.clientId"/>
			<form:hidden path="division.divisionId"/>
		<div id="labelDiv" class="divFloatLeft PCSTD0001" style="width: 150px;height: 1000px ;">
				<div><fmt:message key="clientName" ></fmt:message>
				</div>
				<br/>
				<div><fmt:message key="division" ></fmt:message></div>
				<div style="height: 75px;"></div>
				<div><fmt:message key="selectContact" ></fmt:message></div>
				<br/>
			
		</div>
	     <div class="PCSTD0002"id="rightDiv" style="width: 850px;margin-left: 150px">
					<div>
						<c:out value="${masterBTMDTO.client.clientName}" />&nbsp;
					</div>
					 <br/>
			<div style=" padding: 2px 5px; width: 298px; border: 1px solid black;" >
						<div>${masterBTMDTO.division.divisionName}&nbsp;</div>
						<div>${masterBTMDTO.division.address1}&nbsp;</div>
						<div>${masterBTMDTO.division.address2}&nbsp;</div>
						<div>
							<div class="divFloatLeft">${masterBTMDTO.division.city},&nbsp;</div>
							<div align="center" style="padding-left: 40px; padding-right: 10px;"
								 class="divFloatLeft">${masterBTMDTO.division.state}&nbsp;</div>
							<div align="right" style="padding-left: 10px; padding-right: 30px;">${masterBTMDTO.division.zipCode}&nbsp;</div>
						</div>
						<div align="right"  style="padding-left: 10px; padding-right: 30px;">${masterBTMDTO.division.country}&nbsp;</div>
			</div><br/><br/>
		
	     <c:forEach var="pocItr" items="${pocList}">
				<div style="width: 800px;height: 90px;">
					<div style="float: left;height: 90px;">
					  <form:radiobutton path="poc.pocId" value="${pocItr.pocId}"/>
					</div>
			
					<div style=" padding: 2px 5px; width: 298px; border: 1px solid black;margin-left: 25px;">
					   
					   <div>
							<div class="divFloatLeft">${pocItr.firstName}&nbsp;</div>
							<div style="padding-left: 10px;padding-right: 10px;" class="divFloatLeft">${pocItr.lastName},&nbsp;</div>
						</div><br>
						<div><div class="divFloatLeft" style="width: 15%"><fmt:message key="poc.phone" ></fmt:message></div><div>${pocItr.phoneNo}&nbsp;</div></div>
						<div><div class="divFloatLeft" style="width: 15%"><fmt:message key="poc.cell" ></fmt:message></div><div>${pocItr.cellNo}&nbsp;</div></div>
						<div><div class="divFloatLeft"style="width: 15%"><fmt:message key="poc.email" ></fmt:message></div><div>${pocItr.emailID}&nbsp;</div></div>
						<div><div class="divFloatLeft"style="width: 15%"><fmt:message key="poc.fax" ></fmt:message></div><div>${pocItr.faxNo}&nbsp;</div></div>
						
					</div>
				</div>
		</c:forEach>
				<div class="PCSTD0008">
				 		<form:radiobutton path="poc.pocId" value="0"/>&nbsp;<fmt:message key="nobillcontact"/>
				</div>
				<div style="margin-left: 250px;">
					
					<input type="button" id="btnBack" class="FMBUT0001" value="<fmt:message key="mbt.btn.back" ></fmt:message>" onclick="back();"/>
					<input type="submit" id="btnGo" class="FMBUT0001" value="<fmt:message key="mbt.btn.go" ></fmt:message>"/>
			    </div>
		</div>
	</form:form>
</div>



</body>
</html>