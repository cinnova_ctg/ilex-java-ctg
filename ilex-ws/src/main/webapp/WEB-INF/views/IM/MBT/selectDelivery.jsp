<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
<script>
$(document).ready(function() {
	$('input[name=deliveryMethod]').attr('checked', false);
	 });
function validate(){

	if (!$("input[@name='deliveryMethod']:checked").val()) {
	       alert("Please select a Delivery Method");
	       return false;
	       }
	    }
function back(){
	var url="${pageContext.request.contextPath}/masterbtm/selectContact.html";
	document.forms[0].action= url;
	  document.forms[0].submit();
}
</script>
</head>
<body>
     
  <jsp:include page="newBillToMasterBreadCrumb.jsp"/>
  <%@ include file="../../common/displayMessages.jsp" %>
<div class="WASTD0001">
			<form:form  action=" ${pageContext.request.contextPath}/masterbtm/finalbillinginfo.html" 
			commandName="masterBTMDTO" method="POST" id ="selectDeliveryForm" onsubmit=" return validate();">
			<div class="MSERR0001"> 
					<form:errors path="client.clientId"></form:errors> 
					<form:errors path="division.divisionId"></form:errors> 
					<form:errors path="poc.pocId"></form:errors>
			</div>  
					 <form:hidden path="client.clientId"/>
					 <form:hidden path="division.divisionId"/>
					 <form:hidden path="poc.pocId"/>
	   <div id="labelDiv" class="divFloatLeft PCSTD0001" style="width: 150px;height: 1000px ;">	
		       <div><fmt:message key="clientName" ></fmt:message>
			   </div>
			   <br/>
			   <div><fmt:message key="deliveryMethod" ></fmt:message></div>
			   <div style="height: 85px;"></div>
			   <div><fmt:message key="division"></fmt:message></div>
			   <div style="height: 55px;"></div>
			   <c:if test="${masterBTMDTO.poc.pocId !=0}">
			   		<div><fmt:message key="contact" ></fmt:message></div>
			   </c:if>
		   
      </div>
	  <div class="PCSTD0002"id="rightDiv" style="width: 850px;margin-left: 150px">
				  <div>
						<c:out value="${masterBTMDTO.client.clientName}" />&nbsp;
				 </div>
				 
		   <div class ="RadioLabel"style="width: 450px;" >
	                    <div><form:radiobutton path="deliveryMethod" value="1"/>
	                    <fmt:message key="email" ></fmt:message></div>
	                    <div><form:radiobutton path="deliveryMethod" value="2" />
	                    <fmt:message key="fax" ></fmt:message></div>
	                    <div><form:radiobutton path="deliveryMethod" value="3"/>
	                    <fmt:message key="mail" ></fmt:message></div>
	                    <div ><form:radiobutton path="deliveryMethod" value="0"/>
	                    <fmt:message key="tobedetermined" ></fmt:message></div>
            </div>
                 <br/>
             <div  style=" padding: 2px 5px; width: 280px;" >
						<div>${masterBTMDTO.division.divisionName}&nbsp;</div>
						<div>${masterBTMDTO.division.address1}&nbsp;</div>
						<div>${masterBTMDTO.division.address2}&nbsp;</div>
						<div>
						<div class="divFloatLeft">${masterBTMDTO.division.city},&nbsp;</div>
						<div align="center" style="padding-left: 40px; padding-right: 10px;"
							 class="divFloatLeft">${masterBTMDTO.division.state}&nbsp;</div>
						<div align="right" style="padding-left: 10px; padding-right: 30px;">${masterBTMDTO.division.zipCode}&nbsp;</div>
						</div>
						<div align="right"  style="padding-left: 10px; padding-right: 30px;">${masterBTMDTO.division.country}&nbsp;</div>
		</div>
		      <br/>
		    <c:if test="${masterBTMDTO.poc.pocId !=0}">
			    <div  style=" padding: 2px 5px; width: 280px; ">
				        <div>
					    <div class="divFloatLeft">${masterBTMDTO.poc.firstName}&nbsp;</div>
					    <div style="padding-left: 10px;padding-right: 10px;" class="divFloatLeft">${masterBTMDTO.poc.lastName},&nbsp;</div>
					    </div><br/>
						<div> <div class="divFloatLeft" style="width: 15%"><fmt:message key="poc.phone" ></fmt:message></div><div>${masterBTMDTO.poc.phoneNo}&nbsp;</div></div>
						<div> <div class="divFloatLeft" style="width: 15%"><fmt:message key="poc.cell" ></fmt:message></div><div>${masterBTMDTO.poc.cellNo}&nbsp;</div></div>
						<div> <div class="divFloatLeft"style="width: 15%"><fmt:message key="poc.email" ></fmt:message></div><div>${masterBTMDTO.poc.emailID}&nbsp;</div></div>
						<div> <div class="divFloatLeft"style="width: 15%"><fmt:message key="poc.fax" ></fmt:message></div><div>${masterBTMDTO.poc.faxNo}&nbsp;</div></div>
			     </div>
			    
	         </c:if>
	           	<div style="margin-left: 142px;">
				    <input type="button" id="btnBack" class="FMBUT0001" value="<fmt:message key="mbt.btn.back" ></fmt:message>" onclick="back();"/>
				    <input type="submit" id="btnAccept" class="FMBUT0001" value="<fmt:message key="mbt.btn.accept" ></fmt:message>"/>
			   </div>
		</div>	   
		</form:form>
	</div>
</body>
</html>