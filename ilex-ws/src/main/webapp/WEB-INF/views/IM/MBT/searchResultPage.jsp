<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
 <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/plugins/jPaginate/css/style.css" media="screen"/>
<script src="${pageContext.request.contextPath}/resources/plugins/jPaginate/jquery.paginate.js" type="text/javascript"></script>
 <script type="text/javascript">
	var pageCount;
	var pageStart;
$(function() {
	pageCount=${clientListDTO.pageCount};
	pageStart=${clientListDTO.pageIndex};
 	var clientName ="${clientListDTO.clientSearchKeyword}";
 	if(pageCount==1){
 		$(".paginateBar").hide();
 		
 	}

	$("#paginateBar").paginate({
		count 		: pageCount,
		start 		: pageStart,
		display     : 1,
		border					: true,
		border_color			: '#9c9c9c',
		text_color  			: '#000000',
		background_color    	: '#c4c7c8',	
		text_hover_color  		: 'black',
		background_hover_color	: '#9cb2ff',
		images					: false,
		onChange     		: function(page){
		
		$("#clientList").load("./searchclientlist.html?clientName="+clientName+"&pageIndex=" +page);
		}
	});
});
</script>
</head>
     <table class="PCSTD0002" style="table-layout: fixed;width: 600px" cellpadding="0px;" cellspacing="1px;">
				<tr bgcolor="#c4c7c8" height="20px"> 
					<td  class="tableHeader"><fmt:message key="client.label"></fmt:message>
					</td>
					<td  class="tableHeader"><fmt:message key="address.label"></fmt:message>
					</td>
				</tr>
				<c:forEach var="clientDetail" items="${clientListDTO.invBTMClientDTOList}" varStatus="loopStatus">
				<tr class="${loopStatus.index % 2 == 0 ? 'odd' : 'even'}" height="20px">
					<td align="left" style="padding-left: 10px;width: 25px" ><a href="${pageContext.request.contextPath}/masterbtm/updatebtmdetails.html?mbtClientId=<c:out value="${clientDetail.mbtClientId}"/>"><c:out value="${clientDetail.clientName}"/></a>
					</td>
					<td  align="left" style="padding-left: 10px;width: 25px" ><c:out value="${clientDetail.city}"/>,
						<c:out value="${clientDetail.state}"/>,
						<c:out value="${clientDetail.country}"/></td>
				</tr>
				</c:forEach>
				<tr  style="background-color: #e7e7e7;" class="paginateBar">
					<td colspan="2" style="padding-left: 330px;padding-top: 5px;" ><div id="paginateBar" ></div></td>
				</tr>
				
				
			</table>
			<script type="text/javascript">
$(function() {
	if(pageStart==pageCount){
		//$(".jPag-control-front").attr('disabled', 'disabled');
		}	
	if(pageStart==1){
		//$(".jPag-control-back").attr('disabled', 'disabled');
		}	
	
	
});
</script>
			
