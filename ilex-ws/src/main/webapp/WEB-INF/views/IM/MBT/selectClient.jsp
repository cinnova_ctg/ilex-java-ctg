<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
<script>
function validate(){

	if($(".PCCOMBO001").val()==""){
		alert("Please select a Client");
		return false;
	}
	
	    
}
</script>
</head>
<body>
<jsp:include page="newBillToMasterBreadCrumb.jsp"/>
	<%@ include file="../../common/displayMessages.jsp" %>

	<div class="WASTD0001">
			
			<form:form  action="${pageContext.request.contextPath}/masterbtm/division.html" commandName="masterBTMDTO" method="POST" onsubmit="return validate();">
			<div class="MSERR0001">
				<form:errors path="client.clientId" cssClass="MSERR0001"/>
			</div>
				<div class="divFloatLeft PCSTD0001" style="width: 100px;padding-bottom: 5px;padding-top: 5px;"><fmt:message key="selectClient" ></fmt:message></div>
				<div>
					<div class="divFloatLeft PCSTD0002">
						<form:select path="client.clientId" cssClass="PCCOMBO001">
						 	<form:option value="" label="Select a Client"/>
					   		<form:options items="${clientList}" itemLabel="clientName" itemValue="clientId" />
						</form:select>
					</div>
							<div><input type="submit" id="btnGo" class="FMBUT0002" value="<fmt:message key="mbt.btn.go" ></fmt:message>"/></div>
				</div>
				
				
			</form:form>
	</div>



</body>
</html>