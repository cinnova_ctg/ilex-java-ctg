<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
<style type="text/css">
 </style>
 <script type="text/javascript">

function addContact(){ //function for dynamically add row in table for adding new contact
   var rowCount = $('#contactTable tr').length - 1  ;
   
   var invContactIdCell = "<td><input type=\"hidden\" name=\"invBTMContactList[" + rowCount + "].invContactId\" id=\"invBTMContactList["+ rowCount + "].invContactId\"/>";
   var firstNameCell = "<input type=\"text\" class=\"TBTXT0004\"name=\"invBTMContactList[" + rowCount + "].firstName\" id=\"invBTMContactList["+ rowCount + "].firstName\"/></td>";
   var lastNameCell = "<td><input type=\"text\" class=\"TBTXT0004\" name=\"invBTMContactList[" + rowCount + "].lastName\" id=\"invBTMContactList["+ rowCount + "].lastName\"/></td>";
   var phoneCell = "<td><input type=\"text\" class=\"TBTXT0005\" name=\"invBTMContactList[" + rowCount + "].phone\" id=\"invBTMContactList["+ rowCount + "].phone\"/></td>";
   var emailCell = "<td><input type=\"text\" class=\"TBTXT0004\" name=\"invBTMContactList[" + rowCount + "].email\" id=\"invBTMContactList["+ rowCount + "].email\"/></td>";
   var cellCell = "<td><input type=\"text\"  class=\"TBTXT0005\" name=\"invBTMContactList[" + rowCount + "].cell\" id=\"invBTMContactList["+ rowCount + "].cell\"/></td>";
   var faxCell = "<td><input type=\"text\" class=\"TBTXT0005\" name=\"invBTMContactList[" + rowCount + "].fax\" id=\"invBTMContactList["+ rowCount + "].fax\"/></td>";
   var statusCell = "<td align=\"center\" class=\"PCSTD0003\" style=\"padding-left: 30px\"><input type=\"checkbox\" name=\"invBTMContactList[" + rowCount + "].status\" id=\"invBTMContactList["+ rowCount + "].status\" checked=\"checked\" /><span class=\"PCSTD0002\"> Active</span></td>";
   var newContactRow = "<tr>" +invContactIdCell + firstNameCell +lastNameCell+ phoneCell+ emailCell +cellCell+ faxCell+statusCell +"</tr>";
	$('#contactTable')
                   .append(newContactRow);
}

function validate(){
	if( $('#contactTable tr').length==1){
		alert("No Contact Exist");
		return false;
	}
	
}



</script>
</head>
<body>
<jsp:include page="updateBTMBreadCrumb.jsp"/>
<%@ include file="../../common/displayMessages.jsp" %>
<div class="WASTD0001">
	
			<form:form  id="updateClientDetailsForm" action="${pageContext.request.contextPath}/masterbtm/updatebtmclientdetails.html" commandName="masterBTMDTO" method="POST">
                    <form:hidden path="btmClientDTO.mbtClientId"/>
                    <div style="margin-bottom: 20px;">
							<div id="labelDiv1" class="divFloatLeft PCSTD0001" style="width: 150px;">
								<div style="margin-bottom: 17px;"><fmt:message key="client.label"></fmt:message>:</div>
								<div><fmt:message key="client.status"></fmt:message>: </div><br/>
								<div><fmt:message key="deliveryMethod"></fmt:message>: </div>
							</div>
							 <div  class="PCSTD0002"id="rightClientDiv">
									<div style="margin-bottom: 10px;">
										<form:input path="btmClientDTO.clientName" cssClass="PCTXT0004"/>
									</div>
									<div class="divFloatLeft"><form:checkbox path="btmClientDTO.status"/></div>
									<div class="PCSTD0003"><fmt:message key="client.status.active"/></div><br/>
									<div>
										<form:select path="btmClientDTO.deliveryMethod" cssClass="PCCOMBO002" >
										   		<form:option value="1" label="Email" />
										   		<form:option value="2" label="Fax" />
										   		<form:option value="3" label="Mail" />
										   		<form:option value="0" label="To Be Determined" />
										</form:select>
									</div>
							</div>
					</div>
					<div class="PCSTD0001" style="margin-bottom: 5px;"><fmt:message key="billingAddress"/></div>
				<div>
						<div id="labelDiv2" class="divFloatLeft PCSTD0002" style="width: 100px;margin-left: 50px;">
									<div  style="margin-bottom: 30px;" ><fmt:message key="address.label"/>:</div>
									<div style="margin-bottom: 10px;"><fmt:message key="city.label"/>:</div>
									<div style="margin-bottom: 8px;"><fmt:message key="state.label"/>:</div>
									<div style="margin-bottom: 8px;"><fmt:message key="zipcode.label"/>:</div>
									<div style="margin-bottom: 8px;"><fmt:message key="country.label"/>:</div>
									
						</div>
					<div  class="PCSTD0002"id="rightContactDiv">
								<div>
									<div style="margin-bottom: 4px;"><form:input path="btmClientDTO.address1" cssClass="PCTXT0004" /></div>
									<div style="margin-bottom: 4px;"><form:input path="btmClientDTO.address2" cssClass="PCTXT0004" /></div>
								</div>
								<div style="margin-bottom: 4px;">
									<form:input path="btmClientDTO.city"  cssClass="PCTXT0004" />
								</div>
								<div style="margin-bottom: 4px;">
									<form:select path="btmClientDTO.state" cssClass="PCCOMBO002" >
										<form:option value="" label="Select a State"/>
									   		<c:forEach var="state" items="${stateList}">
								   				<form:option value="${state.stateShortName}" label="${state.stateName}" />
								   			</c:forEach>
									</form:select>
								</div>
								<div style="margin-bottom: 4px;">
									<div><form:input path="btmClientDTO.zipCode" cssClass="PCTXT0005"/></div>
								</div>
								<div>
									<form:select path="btmClientDTO.country" cssClass="PCCOMBO002"  >
										<form:option value="" label="Select a Country"/>
									   		<c:forEach var="countryList" items="${countryList}">
								   				<form:option value="${countryList.countryShortName}" label="${countryList.countryName}" />
								   			</c:forEach>
									</form:select>
								</div>
						
								<div style="margin-left: 200px;margin-bottom: 10px;margin-top:15px;">
						
									<input type="reset" id="btnCancel" class="FMBUT0002" value="<fmt:message key="mbt.btn.cancel" ></fmt:message>"/>
									<input type="submit" id="btnUpdate" class="FMBUT0002" value="<fmt:message key="mbt.btn.updateclient" ></fmt:message>"/>
				              </div>
				            
					</div>
				</div>
					
			
	</form:form>
		<form:form id="updateContactsDetailsForm" action="${pageContext.request.contextPath}/masterbtm/updatebtmcontactdetails.html" commandName="masterBTMDTO" method="POST" onsubmit="return validate();">
		 <form:hidden path="btmClientDTO.mbtClientId"/>
		<div>
				<div style="width: 1000px;margin-bottom:10px" >
					<div  class="PCSTD0001 divFloatLeft" style="vertical-align: bottom;"><fmt:message key="billingContact" ></fmt:message></div>
					<div style="margin-left:900px">
					<input type="button" id="btnAddContact" class="FMBUT0002" value="<fmt:message key="mbt.btn.add.contact" ></fmt:message>"/ onclick="addContact();">
					</div>
				</div>
				
			   <div>
			   
					<div>
						<table id="contactTable" style="table-layout: fixed" width="1000px" cellspacing="1px;">
							<tr >
								
								<th class="TBHED0001" ><fmt:message key="first.name" ></fmt:message></th>
								<th class="TBHED0001"><fmt:message key="last.name" ></fmt:message></th>
								<th class="TBHED0002"><fmt:message key="phone" ></fmt:message></th>
								<th class="TBHED0001"><fmt:message key="email" ></fmt:message></th>
								<th class="TBHED0002"><fmt:message key="cell" ></fmt:message></th>
								<th class="TBHED0002"><fmt:message key="fax" ></fmt:message></th>
								<th class="TBHED0002"><fmt:message key="status" ></fmt:message></th>
							</tr>
							<c:if test="${not empty masterBTMDTO.invBTMContactList}">
									<c:forEach varStatus="contact" items="${masterBTMDTO.invBTMContactList}">
										<tr>
											<td>
											<spring:bind path="masterBTMDTO.invBTMContactList[${contact.index}].invContactId">
											<input type="hidden" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
								            </spring:bind>
											<spring:bind path="masterBTMDTO.invBTMContactList[${contact.index}].firstName">
											<input  class="TBTXT0004" type="text" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
								            </spring:bind>
								            </td>
								            <td>
											<spring:bind path="masterBTMDTO.invBTMContactList[${contact.index}].lastName">
											<input type="text" class="TBTXT0004" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
								            </spring:bind>
								            </td>
								            <td >
											<spring:bind path="masterBTMDTO.invBTMContactList[${contact.index}].phone">
											<input type="text" class="TBTXT0005" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
								            </spring:bind>
								            </td>
								            <td>
											<spring:bind path="masterBTMDTO.invBTMContactList[${contact.index}].email">
											<input type="text"  class="TBTXT0004" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
								            </spring:bind>
								            </td>
								            <td>
											<spring:bind path="masterBTMDTO.invBTMContactList[${contact.index}].cell">
											<input type="text" class="TBTXT0005" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
								            </spring:bind>
								            </td>
								            <td>
											<spring:bind path="masterBTMDTO.invBTMContactList[${contact.index}].fax">
											<input type="text" class="TBTXT0005" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
								            </spring:bind>
								            </td>
								            <td align="center" class="PCSTD0003" style="padding-left: 30px" >
											<spring:bind path="masterBTMDTO.invBTMContactList[${contact.index}].status">
											<input type="checkbox" name="<c:out value="${status.expression}"/>" id="<c:out value="${status.expression}"/>"   ${status.value eq 'true' ? 'checked' : ''}/>
											<span class="PCSTD0002"><fmt:message key="client.status.active"/></span>
								            </spring:bind>
								            </td>
								        
										</tr>
									</c:forEach>
							</c:if>
						</table>
					</div>
					<div style="width: 1000px;margin-top:9px;">
				
					   <div style="margin-left:802px">
						<input type="reset" id="btnCancel" class="FMBUT0002" value="<fmt:message key="mbt.btn.cancel" ></fmt:message>"/>
						<input type="submit" id="btnUpdateContacts" class="FMBUT0002" value="<fmt:message key="mbt.btn.update.Contacts" ></fmt:message>"/>
					   </div>
					</div>
				</div>
		</div>
		
		</form:form>
	</div>
</body>
</html>