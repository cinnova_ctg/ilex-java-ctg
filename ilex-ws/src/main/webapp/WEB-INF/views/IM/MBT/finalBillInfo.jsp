<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
<script>
function ok(){
	var url="${pageContext.request.contextPath}/masterbtm/selectclient.html";
	document.forms[0].action= url;
	  document.forms[0].submit();
}
</script>
</head>
<body>
<jsp:include page="newBillToMasterBreadCrumb.jsp"/>

 <div class="WASTD0001">
  <%@ include file="../../common/displayMessages.jsp" %>
			<form:form commandName="masterBTMDTO"  id ="selectFinalInfoForm" method="GET">
			<div class="MSERR0001"> 
				<form:errors path="client.clientId"></form:errors> 
				<form:errors path="division.divisionId"></form:errors> 
				<form:errors path="poc.pocId"></form:errors>
			</div>  
        <div class="divFloatLeft PCSTD0001" style="width: 150px;"><fmt:message key="clientName" ></fmt:message></div>
	        	<div class="PCSTD0002"><c:out value="${masterBTMDTO.client.clientName}" />&nbsp;</div>
		   <div class="divFloatLeft PCSTD0001" style="width: 150px;"><fmt:message key="deliveryMethod" ></fmt:message></div>
		    <div class="PCSTD0002">
			   <c:out value="${deliveryMethodName}"/>&nbsp;
		   </div>
		   <div class="PCSTD0001"><fmt:message key="billingAddress"></fmt:message></div>
		    <div  style=" padding-left:20px; width:130px;" class="divFloatLeft PCSTD0002" >
		        <div><fmt:message key="address.label" />:</div>
		        <div>&nbsp;</div>
				<div><fmt:message key="city.label" />:</div>
				<div><fmt:message key="state.label" />:</div>
				<div><fmt:message key="zipcode.label" />:</div>
				<div><fmt:message key="country.label" />:</div>
		    </div>
		     <div  class="PCSTD0002">
		        <div>${masterBTMDTO.division.address1}&nbsp;</div>
				<div>${masterBTMDTO.division.address2}&nbsp;</div>
				<div>${masterBTMDTO.division.city}&nbsp;</div>
				<div>${masterBTMDTO.division.state}&nbsp;</div>
				<div>${masterBTMDTO.division.zipCode}&nbsp;</div>
				<div>${masterBTMDTO.division.country}&nbsp;</div>
		 	 </div>
		    <c:if test="${masterBTMDTO.poc.pocId !=0}">
		    <div class="PCSTD0001" style="width: 150px;"><fmt:message key="billingContact" ></fmt:message></div>
		    <div  style=" padding-left:20px; width: 130px;" class="divFloatLeft PCSTD0002">
		        <div><fmt:message key="first.name" />:</div>
			    <div><fmt:message key="last.name" />:</div>
				<div><fmt:message key="poc.phone" /></div>
				<div><fmt:message key="poc.cell" /></div>
				<div><fmt:message key="poc.email" /></div>
				<div><fmt:message key="poc.fax" /></div>
		    </div>
		    <div  class="PCSTD0002">
		        <div>${masterBTMDTO.poc.firstName}&nbsp;</div>
				<div>${masterBTMDTO.poc.lastName},&nbsp;</div>
				<div>${masterBTMDTO.poc.phoneNo}&nbsp;</div>
				<div>${masterBTMDTO.poc.cellNo}&nbsp;</div>
				<div>${masterBTMDTO.poc.emailID}&nbsp;</div>
				<div>${masterBTMDTO.poc.faxNo}&nbsp;</div>
			</div>
		    </c:if>
		     	<div style="margin-left:150px;">
				    <input type="button" id="btnOK" class="FMBUT0001" value="<fmt:message key="mbt.btn.ok" ></fmt:message>" onclick="ok();"/>
				    
			   </div>
	 </form:form>
</div>
</body>
</html>
		   
		   
		  
		   	
			

		
	    