
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style_common.css" />
<title>Insert title here</title>
<script>

function back(){
	var url="${pageContext.request.contextPath}/masterbtm/selectclient.html";
	document.forms[0].action= url;
	
	  document.forms[0].submit();
}
function validate(){

	if (!$("input[@name='division.divisionId']:checked").val()) {
	       alert("Please select a Division");
	       return false;
	       }
	    
}

</script>
</head>
<body>
<jsp:include page="newBillToMasterBreadCrumb.jsp" />
<%@ include file="../../common/displayMessages.jsp" %>
<div class="WASTD0001" >
		<form:form
			action=" ${pageContext.request.contextPath}/masterbtm/selectContact.html"
			commandName="masterBTMDTO" method="POST" id="selectDivisionForm"
			onsubmit="return validate();">
			<div class="MSERR0001">
					<form:errors path="client.clientId"></form:errors>
					<form:errors path="division.divisionId"></form:errors>
			</div>
			<form:hidden path="client.clientId" />
			<div id="labelDiv" class="divFloatLeft PCSTD0001" style="width: 150px;">
					<div>
						<fmt:message key="clientName"></fmt:message>
					</div>
				<br/>
					<div>
						<fmt:message key="selectDivision"></fmt:message>
					</div>
			</div>
			
			<div  class="PCSTD0002"id="rightDiv" style="margin-left: 150px">
					<div>
						<c:out value="${masterBTMDTO.client.clientName}" />&nbsp;
					</div>
				<br/>
				<c:forEach var="divisions" items="${divisionList}">
						<div style="width: 800px;">
						<div style="float: left;">
							<form:radiobutton path="division.divisionId"
								value="${divisions.divisionId}" />
						</div>
						<div style=" padding: 2px 5px; width: 298px; border: 1px solid black;margin-left:25px">
							<div>${divisions.divisionName}&nbsp;</div>
	
							<div>${divisions.address1}&nbsp;</div>
							<div>${divisions.address2}&nbsp;</div>
							<div>
									<div  class="divFloatLeft">${divisions.city},&nbsp;</div>
									<div align="center" style="padding-left: 40px; padding-right: 10px;"
										class="divFloatLeft">${divisions.state}&nbsp;</div>
									<div align="right" style="padding-left: 10px; padding-right: 30px;">${divisions.zipCode}&nbsp;</div>
							</div>
							<div align="right"  style="padding-left: 10px; padding-right: 30px;">${divisions.country}&nbsp;</div>
						</div>
						</div>

				</c:forEach>
				<div style="margin-left: 249px;">
				<input type="button" id="btnBack" class="FMBUT0001"
					value="<fmt:message key="mbt.btn.back" ></fmt:message>"
					onclick="back();" /> <input type="submit" id="btnGo"
					class="FMBUT0001"
					value="<fmt:message key="mbt.btn.go" ></fmt:message>" />
			</div>
			</div>
		</form:form>

	</div>

</body>
</html>