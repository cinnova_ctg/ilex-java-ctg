<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/plugins/jPaginate/css/style.css" media="screen"/>
<script src="${pageContext.request.contextPath}/resources/plugins/jPaginate/jquery.paginate.js" type="text/javascript"></script>

<body>
    <jsp:include page="btmSearchBreadCrumb.jsp"/>
    <%@ include file="../../common/displayMessages.jsp" %>
    
		<div id="clientList" class="WASTD0001" >
			<div style="height: 5px;">&nbsp;</div>
			 <jsp:include page="searchResultPage.jsp"/>  
		</div>
		 	 

</body>
</html>