<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<html>
	<head></head>
	<body>
	<%@ include file="common/displayMessages.jsp" %>

	<form:form id="form1" name="form1" action="userDetails.html">
		<div style="float: left;width: 10%;">
			<form:label path="userId"><fmt:message key="userId"/></form:label>
		</div>
		<div>
			<c:out value="${command.userId}" />
		</div>
		<div style="float: left;width: 10%;">
			<form:label path="userName"><fmt:message key="userName"/></form:label>
		</div>
		<div>
			<c:out value="${command.userName}" />
		</div>
		<div style="float: left;width: 10%;">
		<form:label path="userPassword"><fmt:message key="password"/></form:label>
		</div>
		<div>
			<c:out value="${command.userPassword}" />
		</div>
		<div></div>
	</form:form>
		
	</body>
</html>