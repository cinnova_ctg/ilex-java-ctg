<!DOCTYPE html>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style_common.css?v=<fmt:message key='hd.js.latest.version'/>" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/hd-style.css?v=<fmt:message key='hd.js.latest.version'/>" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/plugins/jquery-ui/base/ui.all.css" />
<style>
table.standardTable tr:not(.headerRow):nth-child(2n+2){
	background-color: #ffffff;
}
table.standardTable tr:not(.headerRow):nth-child(2n+1){
	background-color: #e8eef7;
}
.headerRow {
	background-color: #b5c8d9;
}
</style>
<script type="text/javascript">
	var pageContextPath = '${pageContext.request.contextPath}';
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery-ui-1.8.23.custom.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery.ui.core.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery.ui.widget.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery-ui-timepicker.js?v=<fmt:message key='hd.js.latest.version'/>"></script>
</head>
<body id="pbody">
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="servicenowBreadCrumbTable">
			<tr>
				<td class ="BCMEN0001">&nbsp;
				</td>
			</tr>
			<tr>
			<td background="${pageContext.request.contextPath}/resources/images/content_head_04.jpg" height="21">
			         
						<div id="BCNAV0001">
					</div>
						
			    </td>
			</tr>
</table>
	<div>
		<div class="PCSSH0001" style="margin: 20px;">Errors - Create
			Incident</div>
		<div style="margin: 20px;">
			<table style="table-layout: fixed; margin-top: 10px;" cellpadding="5"
				cellspacing="2" class="standardTable">
				<col width="120px;">
				<col width="120px;">
				<col width="90px;">
				<col width="90px;">
				<col width="150px;">
				<col width="150px;">
				<tr style="height: 20px;" class="headerRow PCSTD0011">
					<td style="text-align: center;">Ilex#</td>
					<td style="text-align: center;">ServiceNow #</td>
					<td style="text-align: center;">Date</td>
					<td style="text-align: center;">Type</td>
					<td style="text-align: center;">Integartion Status</td>
				</tr>
				<c:forEach items="${createIncidentErrorDTOs}"
					var="createIncidentErrorDTO">
					<tr class="PCSTD0010">
						<td>${createIncidentErrorDTO.ticketNumber}</td>
						<td>${createIncidentErrorDTO.vTaskNumber}</td>
						<td style="text-align: center;">${createIncidentErrorDTO.createdDateDisplay}</td>
						<td>${createIncidentErrorDTO.integrationType}</td>
						<td>${createIncidentErrorDTO.integrationStatusMsg}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	<div>&nbsp;</div>
	<div>
		<div class="PCSSH0001" style="margin: 20px;">Errors - Update
			Incident</div>
		<div style="margin: 20px;">
			<table style="table-layout: fixed; margin-top: 10px;" cellpadding="5"
				cellspacing="2" class="standardTable">
				<col width="120px;">
				<col width="120px;">
				<col width="90px;">
				<col width="90px;">
				<col width="150px;">
				<col width="150px;">
				<tr style="height: 20px;" class="headerRow PCSTD0011">
					<td style="text-align: center;">Ilex#</td>
					<td style="text-align: center;">ServiceNow #</td>
					<td style="text-align: center;">Date</td>
					<td style="text-align: center;">Source</td>
					<td style="text-align: center;">Integartion Status</td>
				</tr>
				<c:forEach items="${updateIncidentErrorDTOs}"
					var="updateIncidentErrorDTO">
					<tr class="PCSTD0010">
						<td>${updateIncidentErrorDTO.ticketNumber}</td>
						<td>${updateIncidentErrorDTO.vTaskNumber}</td>
						<td style="text-align: center;">${updateIncidentErrorDTO.createdDateDisplay}</td>
						<td>${updateIncidentErrorDTO.triggerSource}</td>
						<td>${updateIncidentErrorDTO.integrationStatusMsg}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>
