<!DOCTYPE html>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css"
              href="${pageContext.request.contextPath}/resources/css/style_common.css?v=<fmt:message key='hd.js.latest.version'/>" />
        <link rel="stylesheet" type="text/css"
              href="${pageContext.request.contextPath}/resources/css/hd-style.css?v=<fmt:message key='hd.js.latest.version'/>" />
        <link rel="stylesheet" type="text/css"
              href="${pageContext.request.contextPath}/resources/plugins/jquery-ui/base/ui.all.css" />
        <style>
            .disabledText{
                background-color: #efebde;
            }
            #progressScreen{
                background-color: #666666;
                height: 100%;
                left: 0;
                position: fixed;
                top: 0;
                width: 100%;
                z-index: 60001;
                opacity:0.9;
            }
        </style>
        <script type="text/javascript">
            var pageContextPath = '${pageContext.request.contextPath}';
        </script>
        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery/jquery-ui-1.8.23.custom.min.js"></script>
        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery/jquery.ui.core.min.js"></script>
        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery/jquery.ui.widget.min.js"></script>
        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery-ui-timepicker.js?v=<fmt:message key='hd.js.latest.version'/>"></script>
    </head>
    <script>
            $(document).ready(function() {
                $("#dialog").dialog({
                    autoOpen: false,
                    modal: true
                });
            });
            function createNewIncident() {
                var errorMessage = '<b>Please enter information for the following:</b> <br/><br/>';
                var errorOccured = false;
                if ($("#siteNumber").val() == "") {
                    errorMessage += "Site Number,<br/>";
                    errorOccured = true;
                }
                $("#siteDisplayNumber").val($("#siteNumber").val());

                if ($("#configurationItem").val() == "") {
                    errorMessage += "Configuration Item,<br/>";
                    errorOccured = true;
                }
                if ($("#description").val() == "") {
                    errorMessage += "Description,<br/>";
                    errorOccured = true;
                }
                if ($("#state").val() == "") {
                    errorMessage += "State,<br/>";
                    errorOccured = true;
                }

                if ($('input:radio[name="impact"]:checked').val() == undefined) {
                    errorMessage += "Impact,<br/>";
                    errorOccured = true;
                }
                if ($('input:radio[name="urgency"]:checked').val() == undefined) {
                    errorMessage += "Urgency";
                    errorOccured = true;
                }
                if (errorOccured) {
                    openErrorDialog("400", errorMessage);
                    return false;
                } else {
                    var msaId = $('#msaId').val();
                    var appendixId = $('#appendixId').val();
                    var form_fh = document.getElementById("proactiveIncidentDTO");
                    var data;
                    data = $(form_fh).serializeArray();
                    $('#progressScreen').show();
                    $.ajax({
                        type: 'post',
                        url: pageContextPath + '/serviceNowIntegrate/addProactiveIncident.html',
                        data: data,
                        success: function(data) {
                            if (data == "success") {
                                $('#progressScreen').hide();
                                var url = '/Ilex-WS/serviceNowIntegrate/recentIncidents.html?msaId=' + msaId + '&appendixId=' + appendixId;
                                location.href = url;
                            } else {
                                $('#progressScreen').hide();
                                openErrorDialog("500", data);
                            }
                        }
                    });
                }
            }
            function openErrorDialog(width, errorMessage)
            {
                $("#dialog").dialog("option", "title", "Error");
                $("#dialog").dialog("option", "width", width);
                $("#dialog").dialog("option", "zIndex", "50001");
                $("#dialog").dialog({position: {my: "center", at: "center", of: window}});
                $("#dialog p").html(errorMessage);
                $("#dialog").dialog("open");
            }
            function setShortDescrption(obj) {
                if ($("#siteNumber").val() == "") {
                    $("#shortDescription").val("Network Monitoring [Site] is Down");
                }
                else {
                    $("#shortDescription").val("Network Monitoring " + $("#siteNumber").val() + " is Down");
                }
            }
    </script>
    <body>
        <table border="0" cellspacing="0" cellpadding="0" width="100%" id="servicenowBreadCrumbTable">
            <tr>
                <td class ="BCMEN0001">&nbsp;
                </td>
            </tr>
            <tr>
                <td background="${pageContext.request.contextPath}/resources/images/content_head_04.jpg" height="21">

                    <div id="BCNAV0001">
                    </div>

                </td>
            </tr>
        </table>
        <div class="WASTD0003" style="text-align: left;">
            <div class="PCSSH0001" style="margin: 20px;">Ilex Proactive Ticket Creation</div>
            <div style="margin:20px;">
                <form:form commandName="proactiveIncidentDTO" id="proactiveIncidentDTO">
                    <form:hidden path="msaId"/>
                    <form:hidden path="appendixId"/>
                    <form:hidden path="workNotes"/>
                    <div class="PCSTD0011 divFloatLeft" style="width: 150px;">
                        Site Number:
                    </div>
                    <div style="margin-top: 10px;">
                        <form:select path="siteNumber" cssStyle="width:250px;" cssClass="PCSTD0010" onchange="setShortDescrption(this)">
                            <c:choose>
                                <c:when test ="${proactiveIncidentDTO.msaId eq 740}"><form:option value="" label="Select TXR Location" /></c:when>
                                <c:otherwise><form:option value="" label="Select CFA Location" /></c:otherwise>
                            </c:choose>
                            <c:forEach items="${sites}" var="sites">
                                <option value="${sites.siteNumber}">${sites.siteNumber}</option>
                            </c:forEach>
                        </form:select>
                        <input id="siteDisplayNumber" name="siteDisplayNumber" type="hidden" value="">
                    </div>
                    <div class="PCSTD0011 divFloatLeft" style="width: 150px; margin-top: 10px;">
                        Category:
                    </div>
                    <div style="margin-top: 10px;">
                        <form:input cssStyle="width: 250px;" path="category" maxlength="40"  cssClass="PCSTD0010 disabledText" readonly="true"></form:input>
                        </div>
                        <div class="PCSTD0011 divFloatLeft" style="width: 150px; margin-top: 10px;">
                            Configuration Item:
                        </div>
                        <div style="margin-top: 10px;">
                            <div style="margin-top: 10px;">
                            <form:select path="configurationItem" cssStyle="width:250px;" cssClass="PCSTD0010">
                                <form:option value="" label="Select Configuration Item" />
                                <c:choose>
                                    <c:when test ="${proactiveIncidentDTO.msaId eq 740}">
                                        <form:option value="Access Point" label="Access Point" />
                                        <form:option value="ATA" label="ATA" />
                                        <form:option value="Computer" label="Computer" />
                                        <form:option value="Other" label="Other" />
                                        <form:option value="Phone" label="Phone" />
                                        <form:option value="Power Outage" label="Power Outage" />
                                        <form:option value="Switch" label="Switch" />
                                        <form:option value="Wireless Modem" label="Wireless Modem" />
                                        <form:option value="To Be determined" label="To Be determined" />
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach items="${configurationItems}" var="configurationItems">
                                            <option value="${configurationItems.configurationItem}">${configurationItems.configurationItem}</option>
                                        </c:forEach>
                                    </c:otherwise>
                                </c:choose>
                            </form:select>
                        </div>
                    </div>
                    <div class="PCSTD0011 divFloatLeft" style="width: 150px; margin-top: 10px;">
                        Short Description:
                    </div>
                    <div style="margin-top: 10px;">
                        <form:input cssStyle="width: 250px;" path="shortDescription" maxlength="100"  disabled="disabled" cssClass="PCSTD0010 disabledText" readonly="true"></form:input>
                        </div>
                        <div class="PCSTD0011 divFloatLeft" style="width: 150px; margin-top: 10px;">
                            Description:
                        </div>
                        <div style="margin-top: 10px;">
                        <form:input cssStyle="width: 400px;" path="description" maxlength="1000" cssClass="PCSTD0010"></form:input>
                        </div>
                        <div class="PCSTD0011 divFloatLeft" style="margin-top: 10px; width: 150px;">
                            State:
                        </div>
                        <div style="margin-top: 10px;">
                        <form:select path="state" cssStyle="width:250px;" cssClass="PCSTD0010">
                            <form:option value="" label="Select a State" />
                            <form:option value="2" label="Work in Progress" />
                        </form:select>
                    </div>
                    <div class="PCSTD0011 divFloatLeft" style="width: 150px; margin-top: 10px;">
                        Impact:
                    </div>
                    <div style="margin-top: 10px;" cssClass="PCSTD0010">
                        <form:radiobutton path="impact" value="1"/>&nbsp;<span style="vertical-align: top;" class="PCSTD0010">1</span>&nbsp;
                        <form:radiobutton path="impact" value="2"/>&nbsp;<span style="vertical-align: top;" class="PCSTD0010">2</span>&nbsp;
                        <form:radiobutton path="impact" value="3"/>&nbsp;<span style="vertical-align: top;" class="PCSTD0010">3</span>&nbsp;
                    </div>
                    <div class="PCSTD0011 divFloatLeft" style="width: 150px; margin-top: 10px;">
                        Urgency:
                    </div>
                    <div style="margin-top: 10px;" cssClass="PCSTD0010">
                        <form:radiobutton path="urgency" value="1"/>&nbsp;<span style="vertical-align: top;" class="PCSTD0010">1</span>&nbsp;
                        <form:radiobutton path="urgency" value="2"/>&nbsp;<span style="vertical-align: top;" class="PCSTD0010">2</span>&nbsp;
                        <form:radiobutton path="urgency" value="3"/>&nbsp;<span style="vertical-align: top;" class="PCSTD0010">3</span>&nbsp;
                    </div>
                    <div class="PCSTD0011 divFloatLeft" style="margin-top: 10px; width: 150px;">Symptom:&nbsp;</div>
                    <div style="margin-top: 10px;">
                        <form:input type="text" cssStyle="width: 100px;" path="symptom" maxlength="40"  disabled="disabled" cssClass="PCSTD0010 disabledText" readonly="true"></form:input>
                        </div>
                        <div class="PCSTD0011 divFloatLeft" style="margin-top: 10px; width: 150px;">Type:&nbsp;</div>
                        <div style="margin-top: 10px;">
                        <form:input type="text" cssStyle="width: 100px;" path="type" maxlength="40"  disabled="disabled" cssClass="PCSTD0010 disabledText" readonly="true"></form:input>
                        </div>
                        <div class="PCSTD0011 divFloatLeft" style="margin-top: 10px; width: 150px;">Vendor Key:&nbsp;</div>
                        <div style="margin-top: 10px;">
                        <form:input type="text" cssStyle="width: 100px;" path="vendorKey" maxlength="40"  disabled="disabled" cssClass="PCSTD0010 disabledText" readonly="true"></form:input>
                        </div>
                        <div style="text-align: center; margin-top: 20px;">
                            <input type="button" value="Submit" onclick="createNewIncident();" class="FMBUT0006" />
                        </div>
                </form:form>
            </div>
        </div>
        <div id="dialog" title="" class="PCSTD0010">
            <p></p>
        </div>
        <div id="progressScreen" style="display: none; cursor: default; border: 1px solid #aaaaaa">
            <div style="padding:200px;">
                <div style="width: 350px;height: 100px;background:white;padding-top:50px;padding-left: 50px;border:2px solid #8ba6c2;border-radius: 5px;cursor: pointer;">
                    <img src="${pageContext.request.contextPath}/resources/images/waiting_tree.gif" style="width:30px;height:30px;padding-right:20px;" />
                    <span class="PCSTD0011">Please Wait.....</span>
                </div>
            </div>
        </div>
    </body>
</html>