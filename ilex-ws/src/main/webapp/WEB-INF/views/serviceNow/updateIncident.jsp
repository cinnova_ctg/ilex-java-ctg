<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div style="margin:20px;width:550px;">
    <form:form commandName="incidentDTO" id="incidentDTO">
        <form:hidden path="createIdentifier"/>
        <form:hidden path="vendorKey"/>
        <form:hidden path="msaId"/>
        <div class="PCSTD0011 divFloatLeft"
             style="width: 150px;text-align: left;">Ilex Ticket:&nbsp;</div>
        <div style="text-align: left;">
            <form:input type="text" cssStyle="width: 100px;" path="ticketNumber"
                        maxlength="40"  disabled="disabled" cssClass="PCSTD0010 disabledText" readonly="true"></form:input>
            </div>
            <div class="PCSTD0011 divFloatLeft"
                 style="margin-top: 10px; width: 150px;text-align: left;">vTask#:&nbsp;</div>
            <div style="margin-top: 10px;text-align: left;">
            <form:input type="text" cssStyle="width: 100px;" path="vTaskNumber"
                        maxlength="40"  disabled="disabled" cssClass="PCSTD0010 disabledText" readonly="true"></form:input>
            </div>
            <div class="PCSTD0011 divFloatLeft"
                 style="margin-top: 10px; width: 150px;text-align: left;">
                State:
            </div>
            <div style="margin-top: 10px;text-align: left;">
            <form:select path="state" cssStyle="width:250px;" cssClass="PCSTD0010">
                <form:option value="" label="Select a State" />
                <c:choose>
                    <c:when test ="${incidentDTO.msaId eq 740}">
                        <option value="Work in Progress">Work in Progress</option>
                        <option value="Dispatch Requested">Dispatch Requested</option>
                        <option value="Dispatch Scheduled">Dispatch Scheduled</option>
                        <option value="Dispatch Onsite">Dispatch Onsite</option>
                        <option value="Resolved">Resolved</option>
                        <option value="Cancelled">Cancelled</option>
                    </c:when>
                    <c:otherwise>
                        <option value="2">Work in Progress</option>
                        <option value="3">Requested Scheduled Dispatch</option>
                        <option value="4">WIP-Dispatch scheduled</option>
                        <option value="5">WIP - Tech on site</option>
                        <option value="6">Resolved</option>
                        <option value="7">Awaiting Response/Action</option>
                        <option value="8">Cancelled</option>
                        <option value="9">Assigned to Customer</option>
                        <option value="10">Complete</option>
                        <option value="11">Closed</option>
                        <option value="12">Reopened</option>
                    </c:otherwise>
                </c:choose>
            </form:select>
        </div>
        <div class="divFloatLeft PCSTD0011" style="margin-top: 10px; width: 150px;text-align: left;">
            Work Note:</div>
        <div style="margin-top: 10px;text-align: left;">
            <form:textarea path="workNotes" rows="5" cols="60"
                           cssStyle="width: 70%;overflow-y: scroll;resize:none;"/>
        </div>
        <div style="text-align: right;">
            <input type="button" value="Submit"
                   onclick="updateIncident();" class="FMBUT0006" />
        </div>
    </form:form>
</div>