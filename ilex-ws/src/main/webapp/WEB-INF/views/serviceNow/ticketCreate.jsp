<%@ page language="java" contentType="text/html;"%>
<!DOCTYPE html >
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
<link href="${pageContext.request.contextPath}/resources/css/style_common.css" rel="stylesheet" type="text/css"></link>
<script type="text/javascript">


function createIncident(){

	var form_fh = document.getElementById("createIncidentForm");
	var data;
	data = $(form_fh).serializeArray();
	$("#causedBy").val("");
	$("#shortDescription").val("");
	$.blockUI();
	$.ajax({
		type : 'POST',
		url : "${pageContext.request.contextPath}/serviceNow/createServiceNowIncident.html",
		data : data,
		success : function(data) {
			$.unblockUI();
			alert(data);	
		},
		error: function(){
			$.unblockUI();
		}
	});
	}
	

$(document).ready(function(){
	var interval = 1000 * 60;
	self.setInterval(function(){getTicketDetails()},interval);
});


function getTicketDetails(){
	
	$.ajax({
		type : 'GET',
		url : "${pageContext.request.contextPath}/serviceNow/getTicketDetails.html",
		success : function(data) {
			var rows;
			var obj=$.parseJSON(data);
			$.each(obj,function(index,conversion){
				var tr='<tr>';
				tr+='<td>'+conversion.serviceNowTicketId+'</td>';
				tr+='<td>'+conversion.comments+'</td>';
				tr+='<td>'+conversion.workNotes+'</td>';
				tr+='<td>'+conversion.category+'</td>';
				tr+='<td>'+conversion.shortDescription+'</td>';
				tr+='</tr>';
				
				rows+=tr;
			});
			
			$("#serviceNowTicketTable tbody").html(rows);
			
		}
	}).error(function(data) {
		alert("Exception Occured Please Contact the Administrator");
	});
	
}



</script>




</head>
<body class="PCSTD0007 WASTD0003">
<div style="margin-bottom: 10px;"><b>Incident</b></div> 
<form:form  commandName="serviceNowTicketDTO" id="createIncidentForm">
	<form:hidden path="active" value="true"/>
	<form:hidden path="ticketId" value="4" />
	<table>
		<tr>
			<td>
				Category:
			</td>
			<td>
				<form:select path="category">
					<form:option value="Inquiry/ Help">Inquiry/ Help</form:option>
					<form:option value="Hardware">Request</form:option>
					<form:option value="Hardware">Hardware</form:option>
					<form:option value="Software">Software</form:option>
					<form:option value="Database">Database</form:option>
					<form:option value="Network">Network</form:option>
				</form:select>
			</td>
		</tr>
		<tr>
			<td>
				Incident State:
			</td>
			<td>
				<form:select path="incidentState">
					<form:option value="New">New</form:option>
					<form:option value="Active">Active</form:option>
					<form:option value="Awaiting Problem">Awaiting Problem</form:option>
					<form:option value="Awaiting User Info">Awaiting User Info</form:option>
					<form:option value="Awaiting Evidence">Awaiting Evidence</form:option>
					<form:option value="Progress">Progress</form:option>
					<form:option value="Resolved">Resolved</form:option>
					<form:option value="Closed">Closed</form:option>
				
				</form:select>
			</td>
		</tr>
		<tr>
			<td>
				Caused By:
			</td>
			<td>
				<form:input path="causedBy"/>
			</td>
		</tr>
	</table>
<div style="margin-bottom: 10px;">Short Description:</div>
<div>
<form:textarea path="shortDescription" cssStyle="width: 400px; height: 100px"/>
</div>
<div style="margin-left: 355px;">
	<input type="button" class="FMBUT0001" value ="Create" onclick="createIncident();" >
</div>

<div>

<table id="serviceNowTicketTable" class="TBSTD0005">
	<thead>
		<tr>
			<th>Service Now Ticket Id</th>
			<th>Comments</th>
			<th>Work Notes</th>
			<th>Category</th>
			<th>Short Description</th>
		</tr>
	</thead>
	<tbody>
	<c:forEach var="ilexTickets" items="${ilexTickets}">
				<tr>
					<td>${ilexTickets.serviceNowTicketId}</td>
					<td>${ilexTickets.comments}</td>
					<td>${ilexTickets.workNotes}</td>
					<td>${ilexTickets.category}</td>
					<td>${ilexTickets.shortDescription}</td>
				</tr>
			</c:forEach>
		
	</tbody>

</table>
<div>
	<input type="button" class="FMBUT0001" value ="Refresh(Auto Refresh after 60 Seconds)" onclick="getTicketDetails();" >
</div>

</div>


</form:form>
</body>
</html>

 <div id="popupScreen" style="display: none; cursor: default; border: 1px solid #aaaaaa">
       <div style="border: 0px;">
                 <table cellspacing="0" cellpadding="0" class="headerpaleyellow"
                           width="100%" style="background-color: #faf8cc">
                           <tr>
                                    <td align="left" id="pageTitle" height="20px"
                                              style="padding-left: 5px;"></td>
                                    <td align="right" style="padding-right: 5px;"><img
                                              height="15" width="15" alt="Close"
                                              src="${pageContext.request.contextPath}/resources/images/delete.gif"
                                              id="close" onclick="closeWindow();"></td>
                           </tr>
                 </table>
       </div>
       <div id="popupBody"
                 style="padding-top: 4px; overflow: visible; overflow-x: auto; overflow-y: auto;">
                 <img
                           src="${pageContext.request.contextPath}/resources/images/waiting_tree.gif" />
       </div>
   </div>