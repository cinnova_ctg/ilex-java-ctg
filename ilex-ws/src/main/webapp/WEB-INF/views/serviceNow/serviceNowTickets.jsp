<%@ page language="java" contentType="text/html;"%>
<!DOCTYPE html >
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
<link href="${pageContext.request.contextPath}/resources/css/style_common.css" rel="stylesheet" type="text/css"></link>
<script type="text/javascript">




$(document).ready(function(){
	var interval = 1000 * 10;
	self.setInterval(function(){
		getServiceNowTicketDetails()
		
	},interval);
});


function getServiceNowTicketDetails(){
	
	$.ajax({
		type : 'GET',
		url : "${pageContext.request.contextPath}/serviceNow/getServiceNowTicketDetails.html",
		success : function(data) {
			var rows;
			var obj=$.parseJSON(data);
			$.each(obj,function(index,conversion){
				var tr='<tr>';
				tr+='<td>'+conversion.serviceNowTicketId+'</td>';
				tr+='<td>'+conversion.comments+'</td>';
				tr+='<td>'+conversion.workNotes+'</td>';
				tr+='<td>'+conversion.category+'</td>';
				tr+='<td>'+conversion.shortDescription+'</td>';
				tr+='</tr>';
				
				rows+=tr;
			});
			
			$("#serviceNowTicketTable tbody").html(rows);
			
		}
	}).error(function(data) {
		alert("Exception Occured Please Contact the Administrator");
	});
	
}



</script>




</head>
<body class="PCSTD0007 WASTD0003">
<div style="margin-bottom: 10px;"><b>Incident</b></div> 
<form:form  commandName="serviceNowTicketDTO" id="createIncidentForm">
	

<div>

<table id="serviceNowTicketTable" class="TBSTD0005">
	<thead>
		<tr>
			<th>Service Now Ticket Id</th>
			<th>Comments</th>
			<th>Work Notes</th>
			<th>Category</th>
			<th>Short Description</th>
		</tr>
	</thead>
	<tbody>
	<c:forEach var="ilexTickets" items="${ilexTickets}">
				<tr>
					<td>${ilexTickets.serviceNowTicketId}</td>
					<td>${ilexTickets.comments}</td>
					<td>${ilexTickets.workNotes}</td>
					<td>${ilexTickets.category}</td>
					<td>${ilexTickets.shortDescription}</td>
				</tr>
			</c:forEach>
		
	</tbody>

</table>
<div>
	<input type="button" class="FMBUT0001" value ="Refresh(Auto Refresh after 60 Seconds)" onclick="getServiceNowTicketDetails();" >
</div>

</div>


</form:form>
</body>
</html>

