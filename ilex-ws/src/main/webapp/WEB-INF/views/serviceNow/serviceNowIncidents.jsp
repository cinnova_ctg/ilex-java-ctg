<!DOCTYPE html>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style_common.css?v=<fmt:message key='hd.js.latest.version'/>" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/hd-style.css?v=<fmt:message key='hd.js.latest.version'/>" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/plugins/jquery-ui/base/ui.all.css" />
<style>
table.standardTable tr:not(.headerRow):nth-child(2n+2){
	background-color: #ffffff;
}
table.standardTable tr:not(.headerRow):nth-child(2n+1){
	background-color: #e8eef7;
}
.headerRow{
background-color: #b5c8d9;
}
.disabledText{
background-color: #efebde;
}
#progressScreen{
   background-color: #666666;
    height: 100%;
    left: 0;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 60001;
    opacity:0.9;
}
</style>
<script type="text/javascript">
	var pageContextPath = '${pageContext.request.contextPath}';
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery-ui-1.8.23.custom.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery.ui.core.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery/jquery.ui.widget.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery-ui-timepicker.js?v=<fmt:message key='hd.js.latest.version'/>"></script>
<script>
$(document).ready(function(){
	$("#dialog").dialog({
		autoOpen : false,
		modal : true
	});
});
function openSendUpdatePopUp(createIdentifier){
	$.blockUI({
		message : $('#popUpScreen'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	$("#pageTitle").text("Send Update");
	var $updateLink = $('#'+createIdentifier+'update');
	$('#popUpScreen').parent('.blockUI.blockMsg').offset({
		top : $updateLink.offset().top+75,
		left : $updateLink.offset().left-400
	});
	var url=pageContextPath+'/serviceNowIntegrate/getIncidentSendUpdateScreen.html?createIdentifier='+createIdentifier;
	$("#popupBody").load(url);
}
function openErrorDialog(width,errorMessage)
{		$("#dialog").dialog("option", "title", "Error");
		$("#dialog").dialog("option", "width", width);
		$("#dialog").dialog("option","zIndex","50001");		
		$("#dialog" ).dialog({ position: { my: "center", at: "center", of: window } });
		$("#dialog p").html(errorMessage);
		$("#dialog").dialog("open");
}
function closeWindow() {
	$.unblockUI();
	$("#pageTitle").innerHTML = '';
	$("#popupBody").empty().html(
			'<img src="' + pageContextPath
					+ '/resources/images/waiting_tree.gif" />');

}
function updateIncident(){
	var errorMessage = '<b>Please enter information for the following:</b> <br/><br/>';
	var errorOccured = false;
	 if ($("#state").val() == "") {
		 errorMessage += "State,<br/>";
		 errorOccured = true;
	 }
	 if ($("#workNotes").val() == "") {
		 errorMessage += "Work Notes";
		 errorOccured = true;
	 }
	 if (errorOccured) {
			 openErrorDialog("400",errorMessage);
			return false;
		}else{
				var form_fh = document.getElementById("incidentDTO");
				var data;
				data = $(form_fh).serializeArray();
				$('#progressScreen').show();
				$.ajax({
						type: 'post',
						url: pageContextPath + '/serviceNowIntegrate/updateIncident.html',
						data: data,
						success: function(data){
							if(data=="success"){
								$('#progressScreen').hide();
								closeWindow();
							}else {
								$('#progressScreen').hide();
								openErrorDialog("500",data);
							}
						}
				});
		}
}
function openIncidentDetailPopUp(createIdentifier){
	$.blockUI({
		message : $('#popUpScreen'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	$("#pageTitle").text("Details");
	var $detailLink = $('#'+createIdentifier+'detail');
	$('#popUpScreen').parent('.blockUI.blockMsg').offset({
		top : $detailLink.offset().top + 75,
		left : $detailLink.offset().left-400
	});
	var url=pageContextPath+'/serviceNowIntegrate/getIncidentDetailScreen.html?createIdentifier='+createIdentifier;
	$("#popupBody").load(url);
}
</script>
</head>
<body id="pbody">
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="servicenowBreadCrumbTable">
			<tr>
				<td class ="BCMEN0001">&nbsp;
				</td>
			</tr>
			<tr>
			<td background="${pageContext.request.contextPath}/resources/images/content_head_04.jpg" height="21">
			         
						<div id="BCNAV0001">
					</div>
						
			    </td>
			</tr>
</table>
	<div>
		<div class="PCSSH0001" style="margin: 20px;">Proactive Tickets</div>
		<div style="margin: 20px;">
			<table style="table-layout: fixed; margin-top: 10px;" cellpadding="5"
				cellspacing="2" class="standardTable">
				<col width="120px;">
				<col width="120px;">
				<col width="90px;">
				<col width="90px;">
				<col width="150px;">
				<col width="150px;">
				<tr style="height: 20px;" class="headerRow PCSTD0011">
					<td style="text-align: center;">Ilex#</td>
					<td style="text-align: center;">ServiceNow #</td>
					<td style="text-align: center;">Date</td>
					<td style="text-align: center;">Status</td>
					<td style="text-align: center;">Short Description</td>
					<td style="text-align: center;">Actions</td>
				</tr>
				<c:forEach items="${proactiveIncidentDTOs}"
					var="proactiveIncidentDTO">
					<tr class="PCSTD0010">
						<td>${proactiveIncidentDTO.ticketNumber}</td>
						<td>${proactiveIncidentDTO.vTaskNumber}</td>
						<td style="text-align: center;">${proactiveIncidentDTO.createdDateDisplay}</td>
						<td>${proactiveIncidentDTO.integrationStatus}</td>
						<td>${proactiveIncidentDTO.shortDescription}</td>
						<td style="text-align: center;">
							<c:if test="${proactiveIncidentDTO.integrationStatus eq 'Successful'}">
							<a style="cursor: pointer;" onclick="openIncidentDetailPopUp(${proactiveIncidentDTO.createIdentifier})" id="${proactiveIncidentDTO.createIdentifier}detail">Details</a> | <a style="cursor: pointer;" onclick="openSendUpdatePopUp(${proactiveIncidentDTO.createIdentifier})" id="${proactiveIncidentDTO.createIdentifier}update">Send Update</a>
							</c:if>&nbsp;
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		</div>
		<div>
		<div class="PCSSH0001" style="margin: 20px;">Reactive Tickets</div>
		<div style="margin: 20px;">
			<table style="table-layout: fixed; margin-top: 10px;" cellpadding="5"
				cellspacing="2" class="standardTable">
				<col width="120px;">
				<col width="120px;">
				<col width="90px;">
				<col width="90px;">
				<col width="150px;">
				<col width="150px;">
				<tr style="height: 20px;" class="headerRow PCSTD0011">
					<td style="text-align: center;">Ilex#</td>
					<td style="text-align: center;">ServiceNow #</td>
					<td style="text-align: center;">Date</td>
					<td style="text-align: center;">Status</td>
					<td style="text-align: center;">Short Description</td>
					<td style="text-align: center;">Actions</td>
				</tr>
				<c:forEach items="${reactiveIncidentDTOs}"
					var="reactiveIncidentDTO">
					<tr class="PCSTD0010">
						<td>${reactiveIncidentDTO.ticketNumber}</td>
						<td>${reactiveIncidentDTO.vTaskNumber}</td>
						<td style="text-align: center;">${reactiveIncidentDTO.createdDateDisplay}</td>
						<td>${reactiveIncidentDTO.integrationStatus}</td>
						<td>${reactiveIncidentDTO.shortDescription}</td>
						<td style="text-align: center;">
						<c:if test="${reactiveIncidentDTO.integrationStatus eq 'Successful'}">
							<a style="cursor: pointer;" onclick="openIncidentDetailPopUp(${reactiveIncidentDTO.createIdentifier})" id="${reactiveIncidentDTO.createIdentifier}detail">Details</a> | <a style="cursor: pointer;" onclick="openSendUpdatePopUp(${reactiveIncidentDTO.createIdentifier})" id="${reactiveIncidentDTO.createIdentifier}update">Send Update</a>
						</c:if>&nbsp;
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		</div>
		   <div id="popUpScreen" style="display: none; cursor: default;">
				<div style="border: 0px;">
					<table cellspacing="0" cellpadding="0" class="headerpaleyellow"
						width="100%">
						<tr>
							<td align="left" id="pageTitle" height="20px"
								style="padding-left: 5px;"></td>
							<td align="right" style="padding-right: 5px;"><img
								height="15" width="15" alt="Close" src="${pageContext.request.contextPath}/resources/images/delete.gif"
								id="close" onclick="closeWindow();"></td>
						</tr>
					</table>
				</div>
				<div id="popupBody"
					style="padding-top: 4px; overflow: visible; overflow-x: auto; overflow-y: auto;">
					<img src="${pageContext.request.contextPath}/resources/images/waiting_tree.gif" />
				</div>
			</div>
			<div id="dialog" title="" class="PCSTD0010">
  				<p></p>
			</div>
			    <div id="progressScreen" style="display: none; cursor: default; border: 1px solid #aaaaaa">
    			 <div style="padding:200px;"> 
					<div style="width: 350px;height: 100px;background:white;padding-top:50px;padding-left: 50px;border:2px solid #8ba6c2;border-radius: 5px;cursor: pointer;">
						<img src="${pageContext.request.contextPath}/resources/images/waiting_tree.gif" style="width:30px;height:30px;padding-right:20px;" />
						<span class="PCSTD0011">Please Wait.....</span>
					</div>
				</div>
   </div>
</body>
</html>
