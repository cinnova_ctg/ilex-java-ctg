<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div style="margin: 20px;width:700px;">
	<div>
			<div class="PCSTD0010 divFloatLeft"
			style="width: 250px; text-align: left;">Ilex Ticket:&nbsp;${incidentDTO.ticketNumber}</div>
			<div class="PCSTD0010 divFloatLeft" style="width: 300px; text-align: left;">vTask#:&nbsp;${incidentDTO.vTaskNumber}</div>
			<div class="PCSTD0010" style="text-align: left;">Type:&nbsp;${incidentDTO.integrationType}</div>
			
			<div class="PCSTD0010 divFloatLeft"
			style="width: 250px; text-align: left;margin-top:5px;">Site:&nbsp;${incidentDTO.siteNumber}</div>
			<div class="PCSTD0010 divFloatLeft" style="width: 300px; text-align: left;margin-top:5px;">Category:&nbsp;${incidentDTO.category}</div>
			<div class="PCSTD0010" style="text-align: left;margin-top:5px;">Created:&nbsp;${incidentDTO.createdDateDisplay}</div>
			
			<div class="PCSTD0010 divFloatLeft"
			style="width: 250px; text-align: left;margin-top:5px;">Impact:&nbsp;${incidentDTO.impact}</div>
			<div class="PCSTD0010 divFloatLeft" style="width: 300px; text-align: left;margin-top:5px;">Urgency:&nbsp;${incidentDTO.urgency}</div>
			<div class="PCSTD0010" style="text-align: left;margin-top:5px;">CI:&nbsp;${incidentDTO.configurationItem}</div>
			<div class="PCSTD0010" style="text-align: left;margin-top:5px;">Short Description:&nbsp;${incidentDTO.shortDescription}</div>
			<div class="PCSTD0010" style="text-align: left;margin-top:5px;">Description:&nbsp;${incidentDTO.description}</div>
			<div class="PCSTD0010" style="text-align: left;margin-top:5px;">Initial Work Note:&nbsp;${incidentDTO.workNotes}</div>
			
	</div>
	<div>
		<div class="PCSSH0001" style="margin-top:20px;">Updates</div>
		<div>
			<table style="table-layout: fixed; margin-top: 10px;" cellpadding="5"
				cellspacing="2" class="standardTable">
				<tr style="height: 20px;" class="headerRow PCSTD0011">
					<td style="text-align: center;">Date</td>
					<td style="text-align: center;">Trigger</td>
					<td style="text-align: center;">State</td>
					<td style="text-align: center;">Work Note</td>
				</tr>
				<c:forEach items="${incidentUpdatesDTO}"
					var="incidentUpdate">
					<tr class="PCSTD0010">
						<td style="text-align: center;">${incidentUpdate.createdDateDisplay}</td>
						<td style="text-align: left;">${incidentUpdate.triggerSource}</td>
						<td style="text-align: left;">${incidentUpdate.state}</td>
						<td style="text-align: left;">${incidentUpdate.workNotes}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</div>