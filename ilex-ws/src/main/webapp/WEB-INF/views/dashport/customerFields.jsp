<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script>
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "");
};

setCustomerData = function (){
	var custmereValues = '';
	var appendixCustomerIds = '';
 	var fieldcount=0;
	var notEmptyValueCount=0;
	<c:forEach items="${requiredDataList}" var="requiredDataList" varStatus="index">
	fieldcount=fieldcount+1;
	var custValue=$("#custValue"+<c:out value='${index.index}'/>).val();
	 if(custValue.trim()!='')
		{
		 notEmptyValueCount=notEmptyValueCount+1;
		} 
	</c:forEach>
	if(fieldcount>=2)
		{
		if(notEmptyValueCount<2){
			alert('Please Enter values in at least two customer fields');
			return false;
		}
		} 
	<c:forEach items="${requiredDataList}" var="requiredDataList" varStatus="index">
		<c:if test="${not index.last}">
			custmereValues = custmereValues + $("#custValue"+<c:out value='${index.index}'/>).val().trim() + '~'
			appendixCustomerIds = appendixCustomerIds + $("#appendixCustInfoId"+<c:out value='${index.index}'/>).val() + '~'
		</c:if>
		<c:if test="${index.last}">
			custmereValues = custmereValues +$("#custValue"+<c:out value='${index.index}'/>).val().trim()
			appendixCustomerIds = appendixCustomerIds +$("#appendixCustInfoId"+<c:out value='${index.index}'/>).val();
		</c:if>
	</c:forEach>
	$.ajax({
		type : 'GET',
		url : '/Ilex-WS/dashport/updateCustomerField.html?jobId=<c:out value='${jobInformationDTO.jobId}'/>&appendixId=<c:out value='${jobInformationDTO.appendixId}'/>&customerValues='+escape(encodeURI(custmereValues))+'&appendixCustomerIds='+appendixCustomerIds,
		success: function(data) {
			if(data=='success')
			{
			closeWindow2();
		}else{
			$("#errorDiv").html(data);
		}	
	}
});
}	
</script>
<div class="WASTD0002"
	style="padding: 20px; margin-left: 0px; padding-top: 0px;">
	<div id="errorDiv" class="MSERR0001"
		style="text-align: left; padding-bottom: 5px;"></div>
	<table>
		<tr>
			<td class="PCSTD0001" style="text-align: center;" ><b>Customer Field </td>
			<td class="PCSTD0001" style="text-align: center;"><b>Value</td>
		</tr>
		<c:if test="${empty requiredDataList}">
			<tr>
				<td class="PCSTD0001" style="padding-left: 10px;" colspan="2">
					No Parameters Defined.</td>
			</tr>
		</c:if>

		<c:if test="${(isSnapon ne true)}">
			<c:forEach items="${requiredDataList}" var="requiredDataList"
				varStatus="index">
				<tr>
					<td class="PCSTD0002">
						<c:out value='${requiredDataList.infoParameter}' />
					</td>
					<td>
						<input type="hidden"
						id="appendixCustInfoId<c:out value='${index.index}'/>"
						value="<c:out value='${requiredDataList.infoId}' />" /> <input
						type="text" class="text" size="30" style="margin-left:10px;"
						id="custValue<c:out value='${index.index}'/>"
						value="<c:out value='${requiredDataList.value}' />" />
					</td>
				</tr>
			</c:forEach>
		</c:if>
		<c:if test="${(isSnapon eq true)}">
			<c:forEach items="${requiredDataList}" var="requiredDataList"
				varStatus="index">
				<tr valign="top">
					<td valign="top" class="PCSTD0002">
						<c:out value='${requiredDataList.infoParameter}' />
					</td>
					<td valign="top" width="175"><input type="hidden"
						id="appendixCustInfoId<c:out value='${index.index}'/>"
						value="<c:out value='${requiredDataList.infoId}'/>" /> <input
						type="text" class="text" size="30" style="margin-left:10px;"
						id="custValue<c:out value='${index.index}'/>"
						value="<c:out value='${requiredDataList.value}'/>" /></br></td>
				</tr>
			</c:forEach>
		</c:if>
		<tr>
			<td align="right" colspan="2"><input type="button"
				value="Submit" class="FMBUT0001" onclick="setCustomerData();"
				<c:if test="${empty requiredDataList}">disabled="disabled"</c:if> /></td>
			</td>
	</table>
</div>