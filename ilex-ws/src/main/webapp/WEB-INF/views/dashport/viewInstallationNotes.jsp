<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
 <style>
 div.installNotesDiv table thead tr th{
 font-size: 11px;
 font-weight: bold;
 font-family:Verdana, Arial, Helvetica, sans-serif;
 vertical-align: top;
 padding: 5px;
 text-align: center;
 background-color: #d6d6d6}
div.installNotesDiv table tbody tr:nth-child(2n+1){
	text-align:left;
	background-color: #e8eef7;
	padding:4px;
}
div.installNotesDiv table tbody tr:nth-child(2n+2){
	text-align:left;
	background-color: #ffffff;
}
div.installNotesDiv table tr td{
	white-space: wrap;
	word-break:break-all;
	word-wrap: break-word;
}
 </style>
 <script type="text/javascript">
</script>
<div class="WASTD0002" style="padding:20px;margin-left:0px;">
<div class="installNotesDiv">
<table cellspacing="1" cellpadding="2" style="table-layout: fixed;">
	<col width="130px">
	<col width="250px">
	<col width="100px">
			 <thead>
			<tr>
				<th>Date</th>
				<th>Notes</th>
				<th>By</th>
			</tr>
			</thead>
			<tbody>
			<c:if test="${not empty installtionNotesList}">
				<c:forEach var="installtionNotesVar" items="${installtionNotesList}" varStatus="loopStatus">
					<tr  class="PCSTD0007" >
					<td style="font-weight:${installtionNotesVar.typeId ne 0 ? 'bold' : 'normal'}">
					${installtionNotesVar.createdatetime}
					</td>
					<td>${installtionNotesVar.installnotes}</td>
					<td>${installtionNotesVar.createdby}</td>
					</tr>
				</c:forEach>
			</c:if>
			</tbody>
		</table>
</div>

