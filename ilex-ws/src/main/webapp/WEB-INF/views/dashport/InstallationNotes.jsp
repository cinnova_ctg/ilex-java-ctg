<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
 <script type="text/javascript">

 $(document).ready(function(){
	 $('#installationNotes').attr('value','Enter Text');
 });
	function validate(){
		var installationNotes=$.trim($('#installationNotes').attr('value'));
		if(installationNotes=='' || installationNotes=='Enter Text')
		{
		alert('Please Enter Installation Notes');
		return false;
		}
		$.ajax({
			type : 'GET',
			url : '/Ilex-WS/dashport/addInstallNotes.html?'+$("form").serialize(),
			success: function(data) {
				if(data=='success')
				{
				closeWindow2();
			}else{
				$("#errorDiv").html(data);
			}
			
			
		}
	});
	}

</script>
<div class="WASTD0002" style="padding:20px;margin-left:0px;padding-top:0px;">
<div id="errorDiv" class="MSERR0001" style="text-align: left;padding-bottom: 5px;"></div>
<form:form commandName="jobInformationDTO">
<form:hidden path="jobId"/>
<div>
<form:textarea path="installationNotes" rows="10" cols="50" id="installationNotes" onblur="javascript: if(this.value=='') this.value='Enter Text';"  onfocus="javascript: if(this.value=='Enter Text') this.value='';"/>
</div>
<div style="text-align: right;">
<input type="button" value="Submit" class="FMBUT0001" onclick="return validate();">
</div>
</form:form>
</div>

