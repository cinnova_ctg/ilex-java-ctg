<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/plugins/jquery-ui/base/ui.all.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
 <script type="text/javascript">
 $(document).ready(function(){
		
		
		$(  "#actenddate").datepicker({
			buttonImage: '${pageContext.request.contextPath}/resources/images/calendar.gif',
				showOn : "both",
					yearRange: "-10:+5" ,
					changeMonth: true,
					changeYear: true
		}).next(".ui-datepicker-trigger").addClass("calImageStyle");

		
	});
</script>
<div class="WASTD0002" style="padding:20px;margin-left:0px;padding-top:0px;">
<%@ include file="../common/displayMessages.jsp" %>
<form:form commandName="schedulerDTO" id="updateTaskReminderForm">
		<table>
					<tr>
						<td class="PCSTD0010" colspan="2">&nbsp;</td>
						<td class="PCSTD0010" style="color: grey;">Hour</td>
						<td class="PCSTD0010" style="color: grey;">Minutes</td>
						<td class="PCSTD0010" style="color: grey;">AM/PM</td>
					</tr>
					<tr>
						<td class="PCSTD0011">Scheduled Date/Time:</td>
						<td class="PCSTD0010" style="white-space: nowrap;" >
								<form:input id="actenddate" cssClass="textbox" size="10"  path="actenddate" readonly="true" />
						</td>
						<td style="vertical-align:top;">
							<form:select path="actenddatehh" cssClass="comboo">
				            	<form:options items="${ilexDateComboDTO.hours}" itemValue="value" itemLabel="label"/>
							</form:select>
						</td>
						<td style="vertical-align:top;">
							<form:select path="actenddatemm" cssClass="comboo">
					            <form:options items="${ilexDateComboDTO.minutes}" itemValue="value" itemLabel="label"/>
							</form:select>
						</td>
						<td style="vertical-align:top;">
							<form:select path="actenddateop" cssClass="comboo">
						            <form:options items="${ilexDateComboDTO.options}" itemValue="value" itemLabel="label"/>
							</form:select>
						</td>
					</tr>					
				</table>
<div class="PCSTD0001">
Task Reminder:
</div>
<div>
<form:textarea path="rseReason" rows="5" cols="60" id="taskReminder"/>
</div>
<div style="text-align: right;">
<input type="button" value="Submit" class="FMBUT0001" onclick="">
</div>
</form:form>
</div>

