<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
 <script type="text/javascript">
 
 
 $("#jobPrioritySelect").change(function(){
	 if($("#jobPrioritySelect").val()!=""){
		 $("#jobPriority").attr("disabled","disabled");
	 }else{
		 $("#jobPriority").removeAttr("disabled");
		
	 }
	 $("#jobPriority").val($("#jobPrioritySelect").val());
	 
 });
 
 $("#jobCategorySelect").change(function(){
	 if($("#jobCategorySelect").val()!=""){
		 $("#jobCategory").attr("disabled","disabled");
	 }else{
		 $("#jobCategory").removeAttr("disabled");
		
	 }
	 $("#jobCategory").val($("#jobCategorySelect").val());
	 
 });

	function validate(){
		 $("#jobCategory").removeAttr("disabled");
		 $("#jobPriority").removeAttr("disabled");
		$.ajax({
			type : 'GET',
			url : '/Ilex-WS/dashport/updateJobCategoryPriority.html?'+$("form").serialize(),
			success: function(data) {
			if(data=='success')
				{
				$.ajax({
					type : 'GET',
					url : '/Ilex-Dashport/dashPort/insertChangedDashPortData.html'
					}).done(function() { 
						if ( $.browser.msie ){
							processAjaxPoll();
						}
					});
					closeWindow2();
			}else{
				$("#errorDiv").html(data);
			}
			
		}
	});
	}

</script>
<div 	style="padding:20px;margin-left:0px;padding-top: 0px;">
<div id="errorDiv" class="MSERR0001" style="text-align: left;padding-bottom: 5px;"></div>
<form:form commandName="jobInformationDTO"  >
	<form:hidden path="jobId"/>
		       <div class="PCSTD0011" style="text-align: left;">Category:</div>
		       <div class="PCSTD0011" style="margin-bottom: 10px;">
		       		<span style="width: 120px;display: inline-block;text-align: left;">
				       <form:select path="jobCategorySelect" cssClass="PCCOMBO001">
				       		<option value="">Select Category</option>
				       	<form:options items="${jobInfo.jobCategories}"/>
				       </form:select>
			       </span>
			       <form:input cssClass="PCTXT0005" path="jobCategory"/>
		       </div>
		        <div class="PCSTD0011" style="text-align: left;">Priority:</div>
		       <div class="PCSTD0011" style="margin-bottom: 10px;">
		       		<span style="width: 120px;display: inline-block;text-align: left;">
				       <form:select path="jobPrioritySelect" cssClass="PCCOMBO001">
				       		<option value="">Select Priority&nbsp;&nbsp;</option>
				       	<form:options items="${jobInfo.jobPriorities}"/>
				       </form:select>
			       </span>
			       <form:input cssClass="PCTXT0005" path="jobPriority"/>
		       </div>
			   
	
<div style="text-align: right;">
<input type="button" value="Submit" class="FMBUT0001" onclick="return validate();">
</div>
</form:form>
</div>