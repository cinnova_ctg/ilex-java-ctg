<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<style>
.PCCOMBO111{
margin-left:10px;
padding:0px;
}
</style>
 <script type="text/javascript">

	function validate(){
		var newJoId=$('.PCCOMBO001').val();
		if(newJoId=='' || newJoId=='0')
		{
		alert('Please Select Job Owner');
		return false;
		}
		$.ajax({
			type : 'GET',
			url : '/Ilex-WS/dashport/updateJobOwner.html?'+$("form").serialize(),
			success: function(data) {
				if(data=='success')
				{
					$.ajax({
						type : 'GET',
						url : '/Ilex-Dashport/dashPort/insertChangedDashPortData.html'
						}).done(function() { 
							if ( $.browser.msie ){
								processAjaxPoll();
							}
						});
					
				closeWindow2();
			}else{
				$("#errorDiv").html(data);
			}
			
			
		}
	});
	}

</script>
<div class="WASTD0002" style="padding:20px;margin-left:0px;padding-top:0px;">
<div id="errorDiv" class="MSERR0001" style="text-align: left;padding-bottom: 5px;"></div>
<form:form commandName="jobInformationDTO">
<form:hidden path="jobId"/>
<form:hidden path="appendixId"/>
<div><span class="PCSTD0001" style="vertical-align:bottom;">Select New Job Owner:<span>
			<form:select path="newJobOwnerId" cssClass="PCCOMBO001 PCCOMBO111">
					   		<form:options items="${jobOwnerList}" itemLabel="label" itemValue="value" />
			</form:select>
</div>
<div style="text-align: right;">
<input type="button" value="Submit" class="FMBUT0001" onclick="return validate();">
</div>
</form:form>
</div>

