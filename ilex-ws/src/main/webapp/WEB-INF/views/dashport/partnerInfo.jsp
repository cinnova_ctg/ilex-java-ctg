<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
	<style>
	.inside{
	text-align: left;
	}
	</style>
<TABLE style="width:171px;" BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD style="background-image: url('../resources/images/box_01.gif');width: 7px;height:15px;"></TD>
		<TD style="background-image: url('../resources/images/box_02.gif');width: 157px;height:15px;border-bottom:0px"></TD>
		<TD  id="PartnerPopUp" style="background-image: url('../resources/images/box_03.gif');width:7px;height:15px;border-right:1px solid black"></TD>
	</TR>
	<TR>
		<TD style="background-image: url('../resources/images/box_04.gif');width:7px;height:109px;"></TD>
		<TD valign="top" bgcolor="#E6E6E6">
		<table width="100%" border="0"
				cellspacing="0" cellpadding="1">
			<tr>
					<td class="inside" colspan="3">${partnerDetailDTO.partnerName}
					</td>
				</tr>
				<tr>
					<td class="inside">${partnerDetailDTO.partnerAddress1}
					<c:if test ="${partnerDetailDTO.partnerAddress2 ne ''}">,&nbsp;${partnerDetailDTO.partnerAddress2}
					</c:if>
					</td>				
					<td></td>
				</tr>
				<tr>
					<td class="inside" colspan="3">${partnerDetailDTO.partnerCity},&nbsp;${partnerDetailDTO.partnerState}&nbsp;${partnerDetailDTO.partnerZipCode}&nbsp;</td>
				</tr>
				<tr>
					<td class="inside" colspan="3">${partnerDetailDTO.partnerPhone}</td>
				</tr>
				<tr>
					<td class="inside" colspan="3" height="4"></td>
				</tr>
				<tr>
					<td class="inside" colspan="3">Primary Contact</td>
				</tr>
				<tr>
					<td style="padding-left: 12px">
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td class="inside" colspan="3">${partnerDetailDTO.partnerPriName}</td>
							</tr>
							<tr>
								<td class="inside" colspan="3">${partnerDetailDTO.partnerPriPhone}</td>
							</tr>
							<tr>
								<td class="inside" colspan="3">${partnerDetailDTO.partnerPriCellPhone}</td>
							</tr>
							<tr>
								<td class="inside" colspan="3">${partnerDetailDTO.partnerPriEmail}</td>
							</tr>
						</table>
					</td>
				</tr>
			</table></TD>
	<TD style="background-image: url('../resources/images/box_06.gif');width:16px;height:109px;border-right:1px solid black"></TD>
	</TR>
	<TR>
		<TD style="background-image: url('../resources/images/box_07.gif');width:7px;height:10px;"></TD>
		<TD style="background-image: url('../resources/images/box_08.gif');width:157px;height:10px;"></TD>
		<TD style="background-image: url('../resources/images/box_09.gif');width:7px;height:10px;"></TD>
	</TR>
</TABLE>
