<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/plugins/jquery-ui/base/ui.all.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>

<script>
$(document).ready(function(){
	
	
	$("#startdate").datepicker({
		buttonImage: '${pageContext.request.contextPath}/resources/images/calendar.gif',
			showOn : "both",
			yearRange: "-10:+5" ,
			changeMonth: false,
			changeYear: false
	}).next(".ui-datepicker-trigger").addClass("calImageStyle");
	
	$('input[name=rseType]').removeAttr("checked");
	
 	$(".clientReasons").hide();
 	$("input[name='rseType']").change(function(){
 		if ($("input[name='rseType']:checked").val()=="Client") {
 			$(".clientReasons").show();
 		}else{
 			$(".clientReasons").hide();
 		}
 	});
 		
});
function submitPlannedDate(){
	var startdate=$.trim($('#startdate').attr('value'));
	if(startdate=='')
	{
		alert("Please Select Start Date.");	
			return false;
	}
	if (!$("input[name='rseType']:checked").val()) {
	       alert("Please select a Reason For ReSchedule");
	       return false;
	       }
	
	if ($("input[name='rseType']:checked").val()=="Client") {
		if (!$("input[name='rseReason']:checked").val()) {
			alert("Please select a Reason");
		       return false;
		}
	}
	    
	
	$.ajax({
		type : 'GET',
		url : '/Ilex-WS/dashport/saveReSchedulerInfo.html?'+$("#reSchedulerPlannedDateForm").serialize(),
		success: function(data) {
			if(data=='success')
			{
				$.ajax({
					type : 'GET',
					url : '/Ilex-Dashport/dashPort/insertChangedDashPortData.html'
					}).done(function() { 
						if ( $.browser.msie ){
							processAjaxPoll();
						}
					});
				
			closeWindow2();
		}else{
			$("#errorDiv").html(data);
		}
		
	}
	});
}
</script>
	
<div style="padding:20px;margin-left:0px;padding-top: 0px;">
		<div id="errorDiv" class="MSERR0001" style="text-align: left;padding-bottom: 5px;"></div>
			
			<form:form commandName="schedulerDTO" id="reSchedulerPlannedDateForm">
							<form:hidden path="jobId"/>
							
							<form:hidden path="scheduleEnddate"/>
							<form:hidden path="scheduleEndhh"/>
							<form:hidden path="scheduleEndmm"/>
							<form:hidden path="scheduleEndop"/>	
							
							<form:hidden path="oldstartdate"/>
							<form:hidden path="oldstartdatehh"/>
							<form:hidden path="oldstartdatemm"/>
							<form:hidden path="oldstartdateop"/>
							
							<form:hidden path="actstartdate"/>
							<form:hidden path="actstartdatehh"/>
							<form:hidden path="actstartdatemm"/>
							<form:hidden path="actstartdateop"/>
							
							<form:hidden path="actenddate"/>
							<form:hidden path="actenddatehh"/>
							<form:hidden path="actenddatemm"/>
							<form:hidden path="actenddateop"/>
							
			
				<table>
					<tr>
						<td class="PCSTD0010" colspan="2">&nbsp;</td>
						<td class="PCSTD0010" style="color: grey;">Hour</td>
						<td class="PCSTD0010" style="color: grey;">Minutes</td>
						<td class="PCSTD0010" style="color: grey;">AM/PM</td>
					</tr>
					<tr>
						<td class="PCSTD0011" style="text-align: left;">Scheduled Onsite Date/Time:</td>
						<td class="PCSTD0010">
								<form:input id="startdate" cssClass="textbox" size="10"  path="startdate" readonly="true" />
						</td>
						<td>
							<form:select path="startdatehh" cssClass="comboo">
				            	<form:options items="${ilexDateComboDTO.hours}" itemValue="value" itemLabel="label"/>
							</form:select>
						</td>
						<td>
							<form:select path="startdatemm" cssClass="comboo">
					            <form:options items="${ilexDateComboDTO.minutes}" itemValue="value" itemLabel="label"/>
							</form:select>
						</td>
						<td><form:select path="startdateop" cssClass="comboo">
						            <form:options items="${ilexDateComboDTO.options}" itemValue="value" itemLabel="label"/>
							</form:select>
						</td>
					</tr>
					<tr>
						<td class="PCSTD0011" style="text-align: left;" >Enter Cause for Rescheduled:</td>
						<td colspan="4" style="text-align: left;">
								<div class="PCSTD0010"><form:radiobutton  path="rseType" label=" Client" value="Client"  /></div>
						</td>
					</tr>
					<tr>
						<td class="PCSTD0011" >&nbsp;</td>
						<td colspan="4" style="text-align: left;">
								<div class="PCSTD0010"><form:radiobutton  path="rseType" label=" Internal" value="Internal"  /></div>
						</td>
					</tr>
					<tr>
						<td class="PCSTD0011" >&nbsp;</td>
						<td colspan="4" style="text-align: left;">
								<div class="PCSTD0010"><form:radiobutton  path="rseType" label=" Administrative" value="Administrative"  /></div>
						</td>
					</tr>
					<tr class="clientReasons">
						<td class="PCSTD0011" style="text-align: left;" >Client Reason Code:   </td>
						<td colspan="4" style="text-align: left;">
								<div class="PCSTD0010"><form:radiobutton  path="rseReason" label=" No facility access" value="1"  /></div>
						</td>
					</tr>
					<tr class="clientReasons">
						<td class="PCSTD0011" >&nbsp;</td>
						<td colspan="4" style="text-align: left;">
								<div class="PCSTD0010"><form:radiobutton  path="rseReason" label=" Client supplied equipment not available" value="2"  /></div>
						</td>
					</tr>
					<tr class="clientReasons">
						<td class="PCSTD0011" >&nbsp;</td>
						<td colspan="4" style="text-align: left;">
								<div class="PCSTD0010"><form:radiobutton  path="rseReason" label=" Client not ready" value="3"  /></div>
						</td>
					</tr>
					<tr class="clientReasons">
						<td class="PCSTD0011" >&nbsp;</td>
						<td colspan="4" style="text-align: left;">
								<div class="PCSTD0010"><form:radiobutton  path="rseReason" label=" Other" value="4"  /></div>
						</td>
					</tr>
					
					<tr>
						<td colspan="5" style="text-align: right;"><input type="button" id="btnGo" onclick="submitPlannedDate();" class="FMBUT0002" value="Submit"/></td>
					</tr>
					
				</table>
							
				
			</form:form>
</div>