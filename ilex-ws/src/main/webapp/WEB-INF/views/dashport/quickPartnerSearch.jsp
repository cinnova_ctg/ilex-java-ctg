<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/summary.css" />

 
 <style>
 div table thead tr th{
 font-size: 10px;
 font-weight: bold;
 font-family:Verdana, Arial, Helvetica, sans-serif;
 vertical-middle: top;
 padding: 5px;
 text-align: center;
 background-color: #d6d6d6
 }
div.quickPartnerMMSearchDiv{
max-height:200px;
overflow: scroll;
}
div.quickPartnerSPSearchDiv{
max-height:200px;
overflow: scroll;
}
td.lastUsedDateHighLight{
font-weight: bold;
}
tr.leadMinuteManHighLight td{ font-family:Verdana, Arial, Helvetica, sans-serif;white-space:nowrap; font-size:10px;  padding: 2px 5px 2px 5px;color: #000000; background: #DEE3ED ;}
tr.redHighLight td{ font-family:Verdana, Arial, Helvetica, sans-serif;white-space:nowrap; font-size:10px; padding: 2px 5px 2px 5px;color: #000000; background: #FFEAEA ;}
tr.greenHighLight td{ font-family:Verdana, Arial, Helvetica, sans-serif; white-space:nowrap; font-size:10px;  padding: 2px 5px 2px 5px;color: #000000; background: #EDF8E7;}
tr.yellowHighLight td{ font-family:Verdana, Arial, Helvetica, sans-serif; white-space:nowrap; font-size:10px;  padding: 2px 5px 2px 5px;color: #000000; background: #FFFCDF;}
tr.orangeHighLight td{ font-family:Verdana, Arial, Helvetica, sans-serif; white-space:nowrap; font-size:10px; padding: 2px 5px 2px 5px;color: #000000; background: #FFE7D7;}


</style>

 <script type="text/javascript">
 function showPartnerDetail(pId) {
	 var $partnerDetailLink = $('#'+pId+'PartnerLink');
 	$('#partnerDetailPopUpDiv').offset({
 		top : $partnerDetailLink.offset().top + 15,
 		left : $partnerDetailLink.offset().left
 	});
		str = '/Ilex-WS/dashport/getPartnerInfo.html?partnerId='+pId;
		$('#partnerDetailPopUpDiv').load(str);
	}
 $('#PartnerPopUp').live('click',function(){
	 document.all.partnerDetailPopUpDiv.innerHTML='';	 
 });
</script>
<div class="WASTD0002" style="padding:20px;margin-left:0px;padding-top:0px;">
<div class="PCSTD0001">Minuteman</div>
<div class="quickPartnerMMSearchDiv">
			<table cellspacing="1" cellpadding="2" style="table-layout: fixed;">
				<col width="130px">
				<col width="250px">
				<col width="80px">
				<col width="80px">
				<col width="80px">
				<col width="130px">
				<col width="130px">
				<col width="130px">
						 <thead>
						<tr>
							<th>Name</th>
							<th>City</th>
							<th>State</th>
							<th>Zip</th>
							<th>Rating</th>
							<th>Completed</th>
							<th>Last Used<sup>1</sup></th>
							<th>Distance</th>
						</tr>
						</thead>
						<tbody>
						<c:if test="${not empty partnerList}">
							<c:forEach var="partnerListVar" items="${partnerList}" varStatus="loopStatus">
							<c:if test="${partnerListVar.partnerType eq 'Minuteman'}">
								<tr class="${partnerListVar.partnerHighLight}">
								<td class="TBTXT0001" id="${partnerListVar.pid}PartnerLink"><a href="javascript:void(0)" onClick="showPartnerDetail(${partnerListVar.pid});">${partnerListVar.partnername}</a></td>
								<td  class="TBTXT0001">${partnerListVar.city}</td>
								<td  class="TBTXT0001">${partnerListVar.state}</td>
								<td  class="TBMON0001">${partnerListVar.zipcode}</td>
								<td  class="TBMON0001">${partnerListVar.partnerAvgRating}</td>
								<td  class="TBMON0001">${partnerListVar.compeletedCount}</td>
								<td  class="TBDAT0001 ${partnerListVar.lastUsedDateHighLight}">${partnerListVar.lastUsedDate}</td>
								<td  class="TBMON0001">${partnerListVar.distance}</td>
								</tr>
								</c:if>
							</c:forEach>
						</c:if>
						</tbody>
					</table>
	</div>
	<div class="PCSTD0001">Speedpay</div>
	<div class="quickPartnerSPSearchDiv">
		<table cellspacing="1" cellpadding="2" style="table-layout: fixed;">
			<col width="130px">
			<col width="250px">
			<col width="80px">
			<col width="80px">
			<col width="80px">
			<col width="130px">
			<col width="130px">
			<col width="130px">
					 <thead>
					<tr>
						<th>Name</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Rating</th>
						<th>Completed</th>
						<th>Last Used<sup>1</sup></th>
						<th>Distance</th>
					</tr>
					</thead>
					<tbody>
					<c:if test="${not empty partnerList}">
						<c:forEach var="partnerListVar" items="${partnerList}" varStatus="loopStatus">
						<c:if test="${partnerListVar.partnerType eq 'Certified Partners - Speedpay Users'}">
							<tr class="${partnerListVar.partnerHighLight}">
								<td class="TBTXT0001"  id="${partnerListVar.pid}PartnerLink"><a href="javascript:void(0)" onClick="showPartnerDetail(${partnerListVar.pid});">${partnerListVar.partnername}</a></td>
								<td  class="TBTXT0001">${partnerListVar.city}</td>
								<td  class="TBTXT0001">${partnerListVar.state}</td>
								<td  class="TBMON0001">${partnerListVar.zipcode}</td>
								<td  class="TBMON0001">${partnerListVar.partnerAvgRating}</td>
								<td  class="TBMON0001">${partnerListVar.compeletedCount}</td>
								<td  class="TBDAT0001 ${partnerListVar.lastUsedDateHighLight}">${partnerListVar.lastUsedDate}</td>
								<td  class="TBMON0001">${partnerListVar.distance}</td>
							</tr>
							</c:if>
						</c:forEach>
					</c:if>
					</tbody>
				</table>
</div>
<div class="PCSTD0002">1 When the date in the "Last Used" column is displayed in bold text, this indicate the partner has not been used. The date displayed is when the partner has added to the PVS database.</div>
<div id="partnerDetailPopUpDiv" style="position: absolute;z-index:1002;display: inline-block;"></div>
</div>

