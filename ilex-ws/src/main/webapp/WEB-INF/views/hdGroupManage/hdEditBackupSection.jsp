<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript">

$(document).ready(function(){
	
	});
</script>

<form:form commandName="hdTicketDTO" id="hdTicketDTOEdit">
<form:hidden path="ticketId"/>
<div class="WASTD0003 PCSTD0010" style="text-align: left;">

	<div class="divFloatLeft" style="width: 70px;">
	Device:</div>
	<div style="margin-top: 10px;width: 100px;">${hdTicketDTO.device}&nbsp;</div>
	<div class="divFloatLeft" style="width: 70px;padding-top: 10px;">Status<font style="color: red;">*</font>:</div>
	<div class="divFloatLeft" style="margin-top: 10px; width: 60px;">
		 <form:radiobutton path="backUpCktStatus" value="0"/> <label style="vertical-align: top;">Down</label>
	</div>
	<div class="divFloatLeft" style="margin-top: 10px;width: 50px;">
		 <form:radiobutton path="backUpCktStatus" value="1" /> <label style="vertical-align: top;">Up</label>
	</div>
	<div style="margin-top: 10px;">
		 <form:radiobutton path="backUpCktStatus" value="99" /> <label style="vertical-align: top;">Not Applicable</label>
	</div>
	
	<div style="margin-top: 15px;text-align: right;margin-right: 20px;">
		<input type="button" value="Update" class="FMBUT0003" onclick="editBackupStatus();">
	</div>

</div>
</form:form>