<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="WASTD0003">
	<div class="PCSTD0011 divFloatLeft" style="padding-right: 20px;">
	Customer and Appendix:
	</div>
	<div style="text-align: left;">
		<select style="width: 300px;" id="hdCustomer">
			<c:forEach items="${customers}" var="customer">
				<optgroup label="${customer.key}">
					<c:forEach items="${customer.value}" var="customerList">
						<option value="${customerList.msaId}~${customerList.appendixId}~${customerList.customerName}~${customerList.appendixName}~${customerList.apppendicBreakOut}">${customerList.customerName}, ${customerList.appendixName} - ${customerList.apppendicBreakOut}</option>
					</c:forEach>
			</c:forEach>
		</select>
	</div>
	<div style="text-align: right;margin-right: 100px;margin-top: 20px;">
		<input type="button" value="Next" onclick="createTicketStep2();" class="FMBUT0006" style="width: 100px;"/>
	</div>



</div>