<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript">

$(document).ready(function(){
	
	});
</script>

<form:form commandName="hdTicketDTO" id="hdTicketDTOEdit">
<form:hidden path="ticketId"/>
<form:hidden path="appendixId"/>
<div class="WASTD0003 PCSTD0010" style="text-align: left;">

	<div class="divFloatLeft" style="width: 100px;">
	Priority:</div>
	<div style="margin-top: 10px;width: 100px;">${hdTicketDTO.priority }</div>
	<div class="divFloatLeft" style="margin-top: 10px;margin-bottom: 10px;width: 10%;height: 80px;">
		<div class="divFloatLeft" style="height: 70px;width: 100px;padding-top: 10px;">Impact<font style="color: red;">*</font>:</div>
		<div style="margin-top: 10px;">
			 <form:radiobutton path="impact" value="1"/>  &nbsp;<span class="impact">1-High</span>
		</div>
		<div style="margin-top: 10px;" >
			 <form:radiobutton path="impact" value="2" /> &nbsp;<span class="impact">2-Medium</span>
		</div>
		<div style="margin-top: 10px;">
			 <form:radiobutton path="impact" value="3" /> &nbsp;<span class="impact">3-Low</span>
		</div>
	</div>
	<div  style="margin-top: 10px;height: 80px;">
		<div style="height: 70px;width: 100px;float: left;margin-left: 200px;">Urgency<font style="color: red;">*</font>:</div>
		<div style="margin-top: 20px;">
			 <form:radiobutton path="urgency" value="1" /> &nbsp;<span class="urgency">1-High</span>
		</div>
		<div style="margin-top: 10px;">
			<form:radiobutton path="urgency" value="2" /> &nbsp;<span class="urgency">2-Medium</span>
		</div>
		<div style="margin-top: 10px;">
			<form:radiobutton path="urgency" value="3" /> &nbsp;<span class="urgency">3-Low</span>
		</div>
	</div>
	
	<div class="PCSTD0010 divFloatLeft" style="width: 100px;margin-top: 10px;">
		Work Notes<font style="color: red;">*</font>:
	</div>
	<div style="margin-top: 10px;">
		<form:textarea path="workNotes"  rows="5" cols="60" cssStyle="width: 70%;overflow-y: scroll;resize:none;"  spellcheck="true"/>
	</div>
	
	<div style="margin-top: 15px;text-align: right;margin-right: 40px;">
			<input type="button" value="Update" class="FMBUT0003" onclick="editPrioritySection();">
	</div>

</div>
</form:form>