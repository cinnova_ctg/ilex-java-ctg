<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/hdMainTable.css">

<div style="float:left; width:100%;">
    <input type="text" placeholder="Quick Search" style="float: left; padding: 2px; margin-top: 2px; border:solid 1px #aaa; font-size:12px;" id="siteSearchId">
    <div style="line-height: 15px; height: 25px; margin-right: 20px; margin-top: 0px;" class="divFloatLeft dataTables_paginate paging_full_numbers">
        <a onclick="createTicketStep2();" class="previous paginate_button" tabindex="0" style="padding-right: 4px; padding-left:4px; padding-top:3px; padding-bottom:2px;">Create Ticket</a>
    </div>
</div>
<div id="hdMainTableOuter" style="width: 100%; overflow: hidden;">
    <div id="leftDiv" style="width: 450px;">
        <!-- Customer Integration Pending Acknolwedgement -->
        <div id='hdPendingAckCustDiv' class="tableWrapper" style="width: 430px; float: left;">
            <div class="containerTitle" style="margin: 0px;">
                <div  class="divFloatLeft" style="padding: 4px; width: 250px; margin: 0px;">Pending Acknowledgement - Customers</div>
                <div style="text-align: right;">
                    <img class="enlargeView" src="${pageContext.request.contextPath}/resources/images/restore.png" />
                    <img id="minMax" src="${pageContext.request.contextPath}/resources/images/sm-scr.png" />
                </div>
            </div>
            <table class="container display" cellpadding="0" cellspacing="1" border="0" id="hdPendingAckCustTable" style="width: 100%;">
                <thead>
                    <tr>
                        <th width="25%">Opened</th>
                        <th width="30%">Site#</th>
                        <th width="20%">Source</th>
                        <th width="25%">Customer</th>
                        <th width="10px" style="padding:0px; margin:0px;"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <!-- Event Tracker Integration Pending Acknolwedgement -->
        <div id='hdPendingAckSecurityDiv' class="tableWrapper" style="width: 430px; float: left;">
            <div class="containerTitle" style="margin: 0px;">
                <div  class="divFloatLeft" style="padding: 4px; width: 250px; margin: 0px;">Pending Acknowledgement - Security</div>
                <div style="text-align: right;">
                    <img class="enlargeView" src="${pageContext.request.contextPath}/resources/images/restore.png" />
                    <img id="minMax" src="${pageContext.request.contextPath}/resources/images/sm-scr.png" />
                </div>
            </div>
            <table class="container display" cellpadding="0" cellspacing="1" border="0" id="hdPendingAckSecurityTable" style="width: 100%;">
                <thead>
                    <tr>
                        <th width="25%">Opened</th>
                        <th width="27.5%">Site#</th>
                        <th width="22.5%">Source</th>
                        <th width="25%">Customer</th>
                        <th width="10px" style="padding:0px;margin:0px;"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id='hdPendingAckAlertDiv' class="tableWrapper" style="width: 430px; float: left;">
            <div class="containerTitle" style="margin: 0px;">
                <div  class="divFloatLeft" style="padding: 4px; width: 250px; margin: 0px;">Network Change Request</div>
                <div style="text-align: right;">
                    <img class="enlargeView" src="${pageContext.request.contextPath}/resources/images/restore.png" />
                    <img id="minMax" src="${pageContext.request.contextPath}/resources/images/sm-scr.png" />
                </div>
            </div>
            <table class="container display" cellpadding="0" cellspacing="1" border="0" id="hdPendingAckAlertTable" style="width: 100%;">
                <thead>
                    <tr>
                        <th width="25%">Opened</th>
                        <th width="30%">Site#</th>
                        <th width="20%">Alert</th>
                        <th width="25%">Customer</th>
                        <th width="10px" style="padding: 0px; margin: 0px;"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id='hdPendingAckTier2Div' class="tableWrapper" style="width: 430px; float: left;">
            <div class="containerTitle" style="margin: 0px;">
                <div  class="divFloatLeft" style="padding: 4px; width: 250px; margin: 0px;">Pending Acknowledgement - 2nd Tier</div>
                <div style="text-align: right;">
                    <span style="padding-left: 30px; font-weight: normal; font-family: Verdana,Arial,Helvetica,sans-serif;">Tickets: <a href="#" style="text-decoration: underline;" class="enlargeView">${tier2TicketCountsAttrib}</a></span>
                    <img class="enlargeView" src="${pageContext.request.contextPath}/resources/images/restore.png" />
                    <img id="minMax" src="${pageContext.request.contextPath}/resources/images/sm-scr.png" />
                </div>
            </div>
            <table id="hdPendingAckMonitorTier2Table" class="container display" cellpadding="0" cellspacing="1" border="0" style="display:none; width: 100%;">
                <thead>
                    <tr>
                        <th width="25%">Opened</th>
                        <th width="30%">Site#</th>
                        <th width="20%">Alert</th>
                        <th width="25%">Customer</th>
                        <th width="10px" style="padding: 0px; margin: 0px;"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="hdPendingAckMonitorDiv" class="tableWrapper" style="width: 430px; float: left;">
            <div class="containerTitle" style="margin: 0px;">
                <div  class="divFloatLeft" style="padding: 4px; width: 250px; margin: 0px;">Pending Acknowledgement - Monitoring</div>
                <div style="text-align: right;">
                    <span style="display: inline-block; vertical-align: top;"><a style="text-decoration: underline; color: #1786d4; font-size: 11px;" href="#" onclick="openCancelAllMonitoringPopUp()">Cancel All</a></span>
                    <img class="enlargeView" src="${pageContext.request.contextPath}/resources/images/restore.png" />
                    <img id="minMax" src="${pageContext.request.contextPath}/resources/images/sm-scr.png" />
                </div>
            </div>
            <table id="hdPendingAckMonitorTable" class="container display" cellpadding="0" cellspacing="1" border="0" style="width: 100%;">
                <thead>
                    <tr>
                        <th width="25%">Opened</th>
                        <th width="30%">Site#</th>
                        <th width="20%">State</th>
                        <th width="25%">Customer</th>
                        <th width="10px" style="padding: 0px; margin: 0px;"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div id="middleDiv" style="width: 1180px; float:left;">
        <div id='hdWipDivOverDue' class="tableWrapper" align="left" style="width: 1180px;">
            <div class="containerTitle" ">
                <div class="divFloatLeft" style="padding: 4px;">Overdue</div>
                <div style="text-align: right;">
                    <img class="enlargeView" src="${pageContext.request.contextPath}/resources/images/restore.png" />
                    <img id="minMax" src="${pageContext.request.contextPath}/resources/images/sm-scr.png" />
                </div>
            </div>
            <table class="container display mainTable" cellpadding="0" cellspacing="1" border="0" id="hdWipOverDueTable" style="width: 100%;">
                <thead>
                    <tr>
                        <th width="3%" class="nowrap">Pr</th>
                        <th width="8%" class="nowrap" id="first">Opened</th>
                        <th width="8%" class="nowrap">Ticket#</th>
                        <th width="14%" class="nowrap" id="siteTh">Site#</th>
                        <th width="7%" class="nowrap">State</th>
                        <th width="10%" class="nowrap">Next Action</th>
                        <th width="8%" class="nowrap">Action Due</th>
                        <th width="7%" class="nowrap">Updated By</th>
                        <th class="nowrap">Short Description</th>
                        <th width="9%" class="nowrap">Customer</th>
                        <th width="8%" class="nowrap">Assignment Group</th>
                        <th width="5%" class="nowrap">Type</th>
                        <th width="3%" class="nowrap" style="padding:0px;margin:0px;"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id='hdWipDivOverDue' class="tableWrapper" align="left" style="width: 1180px;">
            <div class="containerTitle">
                <div class="divFloatLeft" style="padding: 4px;">Next Action Due</div>
                <div style="text-align: right;">
                    <img class="enlargeView" src="${pageContext.request.contextPath}/resources/images/restore.png" />
                    <img id="minMax" src="${pageContext.request.contextPath}/resources/images/sm-scr.png" />
                </div>
            </div>
            <table class="container display mainTable" cellpadding="0" cellspacing="1" border="0" id="hdWipNextActionTable" style="width: 100%;">
                <thead>
                    <tr>
                        <th width="3%">Pr</th>
                        <th width="8%">Opened</th>
                        <th width="8%">Ticket#</th>
                        <th width="14%" id="siteTh">Site#</th>
                        <th width="7%">State</th>
                        <th width="10%">Next Action</th>
                        <th width="8%">Action Due</th>
                        <th width="7%">Updated By</th>
                        <th>Short Description</th>
                        <th width="9%">Customer</th>
                        <th width="8%">Assignment Group</th>
                        <th width="5%" class="nowrap">Type</th>
                        <th width="3%" style="padding: 0px; margin: 0px;"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id='hdWipDiv' class="tableWrapper" align="left" style="width: 1180px;">
            <div class="containerTitle" >
                <div class="divFloatLeft" style="padding: 4px;">Work In Progress (WIP)</div>
                <div style="text-align: right;">
                    <img class="enlargeView" src="${pageContext.request.contextPath}/resources/images/restore.png" />
                    <img id="minMax" src="${pageContext.request.contextPath}/resources/images/sm-scr.png" />
                </div>
            </div>
            <table class="container display mainTable" cellpadding="0" cellspacing="1" border="0" id="hdWipTable" style="width: 100%;">
                <thead>
                    <tr>
                        <th width="3%">Pr</th>
                        <th width="8%">Opened</th>
                        <th width="8%">Ticket#</th>
                        <th width="14%" id="siteTh">Site#</th>
                        <th width="7%">State</th>
                        <th width="10%">Next Action</th>
                        <th width="8%">Action Due</th>
                        <th width="7%">Updated By</th>
                        <th>Short Description</th>
                        <th width="9%">Customer</th>
                        <th width="8%">Assignment Group</th>
                        <th width="5%" class="nowrap">Type</th>
                        <th width="3%" style="padding: 0px; margin: 0px;"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id='hdWipDiv' class="tableWrapper" align="left" style="width: 1180px;">
            <div class="containerTitle" >
                <div class="divFloatLeft" style="padding: 4px;">Resolved</div>
                <div style="text-align: right;">
                    <img class="enlargeView" src="${pageContext.request.contextPath}/resources/images/restore.png" />
                    <img id="minMax" src="${pageContext.request.contextPath}/resources/images/sm-scr.png" />
                </div>
            </div>
            <table class="container display nonEditableTable" cellpadding="0" cellspacing="1" border="0" id="hdResolvedTable" style="width: 100%;">
                <thead>
                    <tr>
                        <th width="3%">Pr</th>
                        <th width="8%">Resolved</th>
                        <th width="8%">Ticket#</th>
                        <th width="14%" id="siteTh">Site#</th>
                        <th width="7%">State</th>
                        <th width="10%">Next Action</th>
                        <th width="8%">Action Due</th>
                        <th width="7%">Updated By</th>
                        <th>Short Description</th>
                        <th width="9%">Customer</th>
                        <th width="8%">Assignment Group</th>
                        <th width="5%" class="nowrap">Type</th>
                        <th width="3%" style="padding:0px;margin:0px;"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<input type="hidden" id="currentTicketNextActionId" />
<input type="hidden" id="sumOfEfforts"  />
<input type="hidden" id="activeTicketsDTO" value="${activeTicketsDTO }"/>

<div id="dialog"  title="" class="PCSTD0010"><p></p></div>
<div id="toolTip"></div>
<div id="redrawDiv"></div>
<div id="hiddenDiv" style="display:none;"></div>