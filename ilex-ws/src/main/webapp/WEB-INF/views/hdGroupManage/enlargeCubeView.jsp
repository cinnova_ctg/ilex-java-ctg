<!DOCTYPE html>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/style_common.css?v=<fmt:message key='hd.js.latest.version'/>" />
<link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/hd-style.css?v=<fmt:message key='hd.js.latest.version'/>" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_hd.css" />
<link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/plugins/dataTable/css/table_paginate.css?v=<fmt:message key='hd.js.latest.version'/>"
          title="currentStyle" />
            <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/plugins/jquery-ui/base/ui.all.css"/>
               <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/plugins/spellChecker/css/screen.css"/>
               <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/plugins/spellChecker/css/spellchecker.css"/>
          <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/dropdownStyle.css?v=<fmt:message key='hd.js.latest.version'/>" />
          
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style_common.css?v=<fmt:message key='hd.js.latest.version'/>" />
	<link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/hd-enlargeCube-style.css?v=<fmt:message key='hd.js.latest.version'/>" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/plugins/dataTable/css/table_paginate.css?v=<fmt:message key='hd.js.latest.version'/>"
	title="currentStyle" />
	     <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/plugins/tableTool/css/TableTools.css?v=<fmt:message key='hd.js.latest.version'/>"
          />
          
<script type="text/javascript">
	var pageContextPath = '${pageContext.request.contextPath}';
	var userId = '<%= (String) session.getAttribute("userid")%>';
</script>
 
        <script src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.selectboxes.min.js" language="JavaScript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery/jquery.ui.core.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery/jquery.ui.widget.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js" type="text/javascript"></script>
        <script type="text/javascript"
                 src="${pageContext.request.contextPath}/resources/plugins/dataTable/js/dataTables-1.9.3.js?v=<fmt:message key='hd.js.latest.version'/>"></script>
<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/exportCubeToExcel.js?v=<fmt:message key='hd.js.latest.version'/>"></script>
		     <script type="text/javascript"
                   src="${pageContext.request.contextPath}/resources/plugins/tableTool/js/tableTool.js?v=<fmt:message key='hd.js.latest.version'/>"></script>
                   
                <script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/hdMainTable.js?v=<fmt:message key='hd.js.latest.version'/>"></script>   
                   
<style type="text/css">
div.tableWrapper {
	margin:20px;
	background: #E0E0DC;
	display: inline-block;
}

div.tableWrapper table {
	table-layout: fixed;
}

div.tableWrapper table tr td {
	white-space: wrap;
	word-break: break-all;
	word-wrap: break-word;
}

div.tableWrapper table tr:nth-child(2n+1) {
	background-color: #ffffff;
}

div.tableWrapper table tr:nth-child(2n+2) {
	background-color: #e8eef7;
}

div.tableWrapper table tr:nth-child(n+1) td {
	border-bottom: 1px solid transparent;
	font-size: 11px;
	font-weight: normal;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	vertical-align: top;
	text-align: left;
	padding-left: 4px;
	padding-top: 2px;
	padding-right: 4px;
	padding-bottom: 2px;
}

.containerTitle {
	background: #E0E0DC;
	color: #000;
	font-family: Tahoma, Geneva, sans-serif;
	font-weight: bold;
	font-size: 12px;
}

.container {
	background: white;
}
.printCube{
padding:2px;
}
.wantToHideThis{
	background: none;
	display: none;
}
</style>
</head>
<body>
	<c:choose>
		<c:when test="${tableId eq	'hdPendingAckCustTable'}">
			<div id='hdPendingAckCustDiv' class="tableWrapper" align="left">
				<div class="containerTitle"
					style="width:${tableParentDTO.hdPendingAckDTO.tableWidth+20}px;">
					<div  class="divFloatLeft" style="padding: 4px;width:250px;margin:0px;">
						Pending Acknowledgement-Customers
					</div>
					<div style="text-align: right;">
						<abbr title="Open or download Excel"><img class="generateExcel" 
							src="${pageContext.request.contextPath}/resources/images/page_excel.png" /></abbr>
						<abbr title="print"><img class="printCube" 
							src="${pageContext.request.contextPath}/resources/images/printer.png" /></abbr>
					</div>
				</div>
						<table class="container display" cellpadding="0" cellspacing="1"
								border="0" id="hdPendingAckCustTable" >
								<col width="${tableParentDTO.hdPendingAckDTO.openedDateWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.siteNumberWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.sourceWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.customerWidth+20}px">
								<col width="10px">
								<thead>
								<tr>
										<th>Opened</th>
										<th>Site#</th>
										<th>Source</th>
										<th>Customer</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
				</table>
			</div>
		</c:when>
		
		<c:when test="${tableId eq	'hdPendingAckAlertTable'}">
			<div id='hdPendingAckAlertDiv' class="tableWrapper" align="left">
				<div class="containerTitle"
					style="width:${tableParentDTO.hdPendingAckDTO.tableWidth+20}px;">
					<div  class="divFloatLeft" style="padding: 4px;width:250px;margin:0px;">
						Network Change Request
					</div>
					<div style="text-align: right;">
						<abbr title="Open or download Excel"><img class="generateExcel" 
							src="${pageContext.request.contextPath}/resources/images/page_excel.png" /></abbr>
						<abbr title="print"><img class="printCube" 
							src="${pageContext.request.contextPath}/resources/images/printer.png" /></abbr>
					</div>
				</div>
						<table class="container display" cellpadding="0" cellspacing="1"
								border="0" id="hdPendingAckAlertTable" >
								<col width="${tableParentDTO.hdPendingAckDTO.openedDateWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.siteNumberWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.stateWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.customerWidth+20}px">
								<col width="10px">
								<thead>
								<tr>
										<th>Opened</th>
										<th>Site#</th>
										<th>Alert</th>
										<th>Customer</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
				</table>
			</div>
		</c:when>
		
		<c:when test="${tableId eq	'hdPendingAckMonitorTable'}">
			<div id='hdPendingAckMonitorDiv' class="tableWrapper" align="left">
				<div class="containerTitle"
					style="width:${tableParentDTO.hdPendingAckDTO.tableWidth+20}px;">
					<div  class="divFloatLeft" style="padding: 4px;width:250px;margin:0px;">
						Pending Acknowledgement-Monitoring
					</div>
					<div style="text-align: right;">
						<abbr title="Open or download Excel"><img class="generateExcel" 
							src="${pageContext.request.contextPath}/resources/images/page_excel.png" /></abbr>
						<abbr title="print"><img class="printCube" 
							src="${pageContext.request.contextPath}/resources/images/printer.png" /></abbr>
					</div>
				</div>
						<table class="container display" cellpadding="0" cellspacing="1"
								border="0" id="hdPendingAckMonitorTable" >
								<col width="${tableParentDTO.hdPendingAckDTO.openedDateWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.siteNumberWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.stateWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.customerWidth+20}px">
								<col width="10px">
								<thead>
								<tr>
										<th>Opened</th>
										<th>Site#</th>
										<th>State</th>
										<th>Customer</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
				</table>
			</div>
		</c:when>
		
		<c:when test="${tableId eq	'hdPendingAckMonitorTier2Table'}">
			<div id='hdPendingAckMonitorTier2Div' class="tableWrapper" align="left">
				<div class="containerTitle"
					style="width:${tableParentDTO.hdPendingAckDTO.tableWidth+30}px;">
					<div  class="divFloatLeft" style="padding: 4px;width:250px;margin:0px;">
						2nd Tier Pending Acknowledgement
					</div>
					<div style="text-align: right;">
						<abbr title="Open or download Excel"><img class="generateExcel" 
							src="${pageContext.request.contextPath}/resources/images/page_excel.png" /></abbr>
						<abbr title="print"><img class="printCube" 
							src="${pageContext.request.contextPath}/resources/images/printer.png" /></abbr>
					</div>
				</div>
						<table class="container display" cellpadding="0" cellspacing="1"
								border="0" id="hdPendingAckMonitorTier2Table" >
								<col width="${tableParentDTO.hdPendingAckDTO.customerWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.siteNumberWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.stateWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.openedDateWidth+20}px">
								<col width="10px">
								<thead>
								<tr>
										<th>Customer</th>
										<th>Site#</th>
										<th>State</th>
										<th>Opened</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
				</table>
			</div>
		</c:when>
		
		
		
		
		
		<c:when test="${tableId eq	'monitoringDateTimeDetailsTable'}">
			<div id='hdMonitoringDateTimeDiv' class="tableWrapper" align="left">
				<div class="containerTitle"
<%-- 					style="width:${tableParentDTO.hdPendingAckDTO.tableWidth+30}px;" --%>
					>
					<div  class="divFloatLeft" style="padding: 4px;width:250px;margin:0px;">
						Added to Monitoring
					</div>
					<div style="text-align: right;">
						<abbr title="Open or download Excel"><img class="generateExcel" 
							src="${pageContext.request.contextPath}/resources/images/page_excel.png" /></abbr>
						<abbr title="print"><img class="printCube" 
							src="${pageContext.request.contextPath}/resources/images/printer.png" /></abbr>
					</div>
				</div>
						<table class="container display" cellpadding="0" cellspacing="1"
								border="0" id="monitoringDateTimeDetailsTable" >
								<col width="${tableParentDTO.hdPendingAckDTO.customerWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.siteNumberWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.stateWidth+20}px">
								<col width="${tableParentDTO.hdPendingAckDTO.openedDateWidth+40}px">
								<col width="${tableParentDTO.hdPendingAckDTO.openedDateWidth+45}px">
								<col width="${tableParentDTO.hdPendingAckDTO.openedDateWidth+20}px">
								<thead>
								<tr>
										<th>Ticket#</th>
										<th>Site#</th>
										<th>State</th>
										<th>Time Added</th>
										<th>Next Action</th>
										<th>Opened</th>
										<th style="display:none;">&nbsp;</th>
								</tr>
								</thead>
								<tbody>
								</tbody>
				</table>
			</div>
		</c:when>
		
		
		
		
		
		<c:when test="${tableId eq	'hdWipOverDueTable'}">
			<div id='hdWipDivOverDue' class="tableWrapper" align="left">
				<div class="containerTitle"
					style="width:100%"
					initialWidth="${tableParentDTO.hdWIPTableDTO.tableWidth+20}px">
					<div class="divFloatLeft" style="padding: 4px; width: 100px;">
						Overdue
					</div>
					<div style="text-align: right;">
					<abbr title="Open or download Excel"><img class="generateExcel" 
							src="${pageContext.request.contextPath}/resources/images/page_excel.png" /></abbr>
						<abbr title="print"><img class="printCube" 
							src="${pageContext.request.contextPath}/resources/images/printer.png" /></abbr>
					</div>
				</div>
				<table class="container display" cellpadding="0" cellspacing="1"
						border="0" id="hdWipOverDueTable" >
						<col width="${tableParentDTO.hdWIPTableDTO.priorityWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.openedDateWidth+20}px">
						
						<col width="50px">
						
						<col width="${tableParentDTO.hdWIPTableDTO.siteNumberWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.stateWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.nextActionWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.nextActionDueDateWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.ccaWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.shortDescWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.customerWidth+20}px">
						
						<col width="150px">
						<col width="70px">
						<thead>
						<tr>
								<th>Priority</th>
								<th>Opened</th>
								<th>Ticket#</th>
								<th>Site#</th>
								<th>State</th>
								<th>Next Action</th>
								<th>Action Due</th>
								<th>Owner</th>
								<th>Short Description</th>
								<th>Customer</th>
								<th>Assignment Group</th>
								<th>Type</th>
								
							</tr>
						</thead>
						<tbody>
						</tbody>
				</table>
			</div>
		</c:when>
		<c:when test="${tableId eq 'hdWipTable'}">
			<div id='hdWipDiv' class="tableWrapper">
				<div class="containerTitle"
					style="width:${tableParentDTO.hdWIPTableDTO.tableWidth+20}px;"
					initialWidth="${tableParentDTO.hdWIPTableDTO.tableWidth+20}px">
					<div class="divFloatLeft" style="padding: 4px; width: 100px;">
						WIP
					</div>
					<div style="text-align: right;">
					<abbr title="Open or download Excel"><img class="generateExcel" 
							src="${pageContext.request.contextPath}/resources/images/page_excel.png" /></abbr>
						<abbr title="print"><img class="printCube" 
							src="${pageContext.request.contextPath}/resources/images/printer.png" /></abbr>
					</div>
				</div>
				<table class="container display" cellpadding="0" cellspacing="1"
						border="0" id="hdWipTable" >
						<col width="${tableParentDTO.hdWIPTableDTO.priorityWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.openedDateWidth+20}px">
						<col width="50px">
						<col width="${tableParentDTO.hdWIPTableDTO.siteNumberWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.stateWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.nextActionWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.nextActionDueDateWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.ccaWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.shortDescWidth+20}px">
						<col width="${tableParentDTO.hdWIPTableDTO.customerWidth+20}px">
						<col width="150px">
						<col width="70px">
						<thead>
						<tr>
								<th>Priority</th>
								<th>Opened</th>
								<th>Ticket#</th>
								<th>Site#</th>
								<th>State</th>
								<th>Next Action</th>
								<th>Action Due</th>
								<th>Owner</th>
								<th>Short Description</th>
								<th>Customer</th>
								<th>Assignment Group</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
			</div>
		</c:when>
		
		
		
		<c:when test="${tableId eq 'hdWipNextActionTable'}">
			<div id='hdWipNextActionDiv' class="tableWrapper">
				<div class="containerTitle"
					style="width:100%;"
					initialWidth="${tableParentDTO.hdWIPTableDTO.tableWidth+20}px">
					<div class="divFloatLeft" style="padding: 4px; width: 100px;">
						Next Action Due
					</div>
					<div style="text-align: right;">
					<abbr title="Open or download Excel"><img class="generateExcel" 
							src="${pageContext.request.contextPath}/resources/images/page_excel.png" /></abbr>
						<abbr title="print"><img class="printCube" 
							src="${pageContext.request.contextPath}/resources/images/printer.png" /></abbr>
					</div>
				</div>
				<table class="container display mainTable" cellpadding="0" cellspacing="1"
					border="0" id="hdWipNextActionTable" style="width: 1040px;">
					<col width=3%>
					<col width=8%>
					<col width=8%>
					<col width=15% >
					<col width=7%>
					<col width=11%>
					<col width=8%>
					<col width=7%>
					<col >
					<col width=10%>
					<col width=9%>
					<col width="5%">
					
					<thead>
					<tr>
							<th>Pr</th>
							<th>Opened</th>
							<th>Ticket#</th>
							<th>Site#</th>
							<th>State</th>
							<th>Next Action</th>
							<th>Action Due</th>
							<th>Owner</th>
							<th>Short Description</th>
							<th>Customer</th>
							<th>Assignment Group</th>
							<th>Type</th>
							
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</c:when>
		
		<c:when test="${tableId eq 'hdResolvedTable'}">
			<div id='hdWipResolvedDiv' class="tableWrapper">
				<div class="containerTitle"
					style="width:100%;"
					initialWidth="${tableParentDTO.hdWIPTableDTO.tableWidth+20}px">
					<div class="divFloatLeft" style="padding: 4px; width: 100px;">
						Resolved
					</div>
					<div style="text-align: right;">
					<abbr title="Open or download Excel"><img class="generateExcel" 
							src="${pageContext.request.contextPath}/resources/images/page_excel.png" /></abbr>
						<abbr title="print"><img class="printCube" 
							src="${pageContext.request.contextPath}/resources/images/printer.png" /></abbr>
					</div>
				</div>
				<table class="container display mainTable" cellpadding="0" cellspacing="1"
					border="0" id="hdResolvedTable" style="width: 1040px;">
					<col width=3%>
					<col width=8%>
					<col width=8%>
					<col width=15% >
					<col width=7%>
					<col width=11%>
					<col width=8%>
					<col width=7%>
					<col >
					<col width=10%>
					<col width=9%>
					<col width=5%>
					
					
					<thead>
						<tr>
							<th>Pr</th>
							<th>Resolved</th>
							<th>Ticket#</th>
							<th>Site#</th>
							<th>State</th>
							<th>Next Action</th>
							<th>Action Due</th>
							<th>Owner</th>
							<th>Short Description</th>
							<th>Customer</th>
							<th>Assignment Group</th>
							<th>Type</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</c:when>
		
	</c:choose>
	<div id="popUpScreen2" style="display: none; cursor: default;">
		<div style="border: 0px;">
			<table cellspacing="0" cellpadding="0" class="headerpaleyellow"
				width="100%">
				<tr>
					<td align="left" id="pageTitle2" height="20px"
						style="padding-left: 5px;"></td>
					<td align="right" style="padding-right: 5px;"><img height="15"
						width="15" alt="Close"
						src="${pageContext.request.contextPath}/resources/images/delete.gif"
						id="close" onclick="closeWindow2();"></td>
				</tr>
			</table>
		</div>
		<div id="popupBody2"
			style="padding-top: 4px; overflow: visible; overflow-x: auto; overflow-y: auto;">
			<img
				src="${pageContext.request.contextPath}/resources/images/waiting_tree.gif" />
		</div>
	</div>
	<div id="dialog" title="" class="PCSTD0010">
		<p></p>
	</div>
	<div id="toolTip"></div>
	<div id="redrawDiv"></div>
	<div id="hiddenDiv" style="display: none;"></div>
	
	 <div id="ticketPopUpScreen" style="display: none; cursor: default; outline: 2px solid #d7c960; border: 2px solid #e0e1e3">
               <div style="border: 0px;">
                         <table cellspacing="0" cellpadding="0" class="headerpaleyellow"
                                   width="100%" style="background-color: #d7c960">
                                   <tr>
                                            <td align="left" id="ticketPageTitle" height="20px"
                                                      style="padding-left: 5px;"></td>
                                            <td align="right" style="padding-right: 5px;"><img
                                                      height="15" width="15" alt="Close"
                                                      src="${pageContext.request.contextPath}/resources/images/delete.gif"
                                                      id="close" onclick="closeWindow();"></td>
                                   </tr>
                         </table>
               </div>
               <div id="ticketPopupBody"
                         style="padding-top: 4px; overflow: visible; overflow-x: auto; overflow-y: auto;">
                         <img
                                   src="${pageContext.request.contextPath}/resources/images/waiting_tree.gif" />
              </div>  
          </div> 
          
          
	<iframe id="secretIFrame" src="" style="display:none; visibility:hidden;"></iframe>

<!-- 	  <script type="text/javascript" -->
<%--                     src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script> --%>
<!-- 	<script type="text/javascript" -->
<%--                    src="${pageContext.request.contextPath}/resources/js/jquery/jquery-ui-1.8.23.custom.min.js"></script> --%>
<%--                     <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.ui.core.min.js"></script> --%>
<%-- 			<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.ui.widget.min.js"></script> --%>
	  
               
                  
</body>
</html>
