<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="WASTD0003 PCSTD0010" style="text-align: left;">
	<div class="divFloatLeft" style="width: 70px;">
	Down:</div>
	<div style="margin-top: 10px;">${stateTailDTO.downMinutes}&nbsp;(Minutes)</div>
	<table style="table-layout: fixed;margin-top: 10px;" cellpadding="0" cellspacing="1" id="stateTailTable">
	<col width="150px;">
	<col width="150px;">
	<col width="150px;">
	<tr style="height:20px;" class="headerRow">
		<td style="text-align: center;">Date/Time</td>
		<td style="text-align: center;">State</td>
		<td style="text-align: center;">Backup Status</td>
	</tr>
	<c:forEach items="${stateTailDTO.wugDetailsDTOList}" var="wugDetails">
		<tr>
		<td style="text-align: center;">${wugDetails.wugDate}</td>
		<td style="text-align: center;">${wugDetails.wugState}</td>
		<td style="text-align: center;">${wugDetails.backUpStatus}</td>
		</tr>
	</c:forEach>
	</table>
	<div style="margin-bottom:10px;">&nbsp;</div>
</div>
