<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="WASTD0003 PCSTD0010" style="text-align: left;">
	<form:form commandName="hdTicketDTO">
			<form:hidden path="ticketId"/>
			<form:hidden path="ticketStatus"/> 
			<div  style="float: left;width: 90px;">State<font style="color: red;">*</font>:</div>
	       	<div>
	       		<c:choose>
		       		<c:when test="${hdTicketDTO.ticketStatus eq '3'}">Dispatch Requested</c:when>
		       		<c:when test="${hdTicketDTO.ticketStatus eq '7'}">Awaiting Response/Action</c:when>
		       		<c:when test="${hdTicketDTO.ticketStatus eq '9'}">Assigned to Customer</c:when>
	       		</c:choose>
	       		&nbsp;
	       	</div>
		    <div  style="float: left;width: 90px;margin-top: 10px;margin-bottom: 10px;">Work Notes<font style="color: red;">*</font>:</div>
		    
	       	<div style="margin-top: 10px;margin-bottom: 10px;">
	       		<form:textarea rows="5" cols="30" style="overflow-y: scroll;resize:none;" path="workNotes"></form:textarea>
	       	</div>
	       	 		       	
       		<div style="text-align: right;">
       		 	<input type="button" value="Submit" onclick="updateTicketTargetState();"  class="FMBUT0003" style="margin-right:80px;"/>
       		</div>
       	</form:form>
       		
</div>
