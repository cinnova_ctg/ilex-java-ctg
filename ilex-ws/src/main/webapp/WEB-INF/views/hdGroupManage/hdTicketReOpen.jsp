<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" >
        <c:if test="${hdTicketDTO.customerName eq null}">
            <title>Create Ticket - Enter Ticket Details</title>
        </c:if>

        <c:if test="${hdTicketDTO.customerName != ''}">
            <title>${hdTicketDTO.ticketType} for ${hdTicketDTO.customerName}, ${hdTicketDTO.appendixName}, ${hdTicketDTO.siteDetailDTO.siteNumber}, ${hdTicketDTO.ticketNumber}</title>
        </c:if>




        <script type="text/javascript">


            var pageContextPath = '/Ilex-WS';
        </script>



        <link href="/Ilex-WS/resources/css/style_common.css?v=66" type="text/css" rel="stylesheet">

        <link href="/Ilex-WS/resources/css/hd-style.css?v=66" type="text/css" rel="stylesheet">
        <link title="currentStyle" href="/Ilex-WS/resources/plugins/dataTable/css/table_paginate.css?v=66" type="text/css" rel="stylesheet">
        <link href="/Ilex-WS/resources/plugins/jquery-ui/base/ui.all.css" type="text/css" rel="stylesheet">
        <link href="/Ilex-WS/resources/plugins/spellChecker/css/screen.css" type="text/css" rel="stylesheet">
        <link href="/Ilex-WS/resources/plugins/spellChecker/css/spellchecker.css" type="text/css" rel="stylesheet">
        <link href="/Ilex-WS/resources/css/dropdownStyle.css?v=66" type="text/css" rel="stylesheet">

        <link href="/Ilex-WS/resources/css/style_hd.css" rel="stylesheet" type="text/css" media="all" />

        <link href="/Ilex-WS/resources/css/messi.min.css?v=45" type="text/css" rel="stylesheet">

        <script src="/Ilex-WS/resources/js/jquery.selectboxes.min.js" language="JavaScript"></script>
        <script src="/Ilex-WS/resources/js/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="/Ilex-WS/resources/js/jquery/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
        <script src="/Ilex-WS/resources/js/jquery/jquery.ui.core.min.js" type="text/javascript"></script>
        <script src="/Ilex-WS/resources/js/jquery/jquery.ui.widget.min.js" type="text/javascript"></script>
        <script src="/Ilex-WS/resources/js/jquery.blockUI.js" type="text/javascript"></script>
        <script src="/Ilex-WS/resources/plugins/dataTable/js/dataTables-1.9.3.js?v=66" type="text/javascript"></script>
        <script src="/Ilex-WS/resources/js/jquery-ui-timepicker.js?v=66" type="text/javascript"></script>
        <script src="/Ilex-WS/resources/plugins/spellChecker/js/jquery-impromptu.js " type="text/javascript"></script>
        <script src="/Ilex-WS/resources/plugins/spellChecker/js/jquery.spellchecker.js " type="text/javascript"></script>
        <script src="/Ilex-WS/resources/plugins/spellChecker/js/jquery-impspeller.js " type="text/javascript"></script>
        <script src="/Ilex-WS/resources/js/messi.min.js?v=66" type="text/javascript"></script>



        <script src="/Ilex-WS/resources/js/jquery.easytabs.js" type="text/javascript"></script>

        <!--     <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" /> -->



        <!-- 	  <script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>	 -->

        <!-- 	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>	 -->


        <!--  easy tabs style -->

        <style>
            .etabs { margin: 0; padding: 0; }
            .tab { display: inline-block; zoom:1; *display:inline; background: #eee; border: solid 1px #999; border-bottom: none; -moz-border-radius: 4px 4px 0 0; -webkit-border-radius: 4px 4px 0 0; }
            .tab a { font-size: 14px; line-height: 2em; display: block; padding: 0 10px; outline: none; }
            .tab a:hover { text-decoration: underline; }
            .tab.active { background: #fff; padding-top: 6px; position: relative; top: 1px; border-color: #666; }
            .tab a.active { font-weight: bold; }
            .tab-container .panel-container { background: #fff; border: solid #666 1px; padding: 10px; -moz-border-radius: 0 4px 4px 4px; -webkit-border-radius: 0 4px 4px 4px; }


            table.display thead th {
                cursor: default;
            }
            div.tableWrapper table tr:nth-child(n+1) td:last-child:hover {
                background-image: none;
            }

            .calImageStyle{margin-top:-3px;}

            /* 	.header { color: red;  */

            /* 		cursor:pointer;} */

            /* 	.headerHistory */
            /*   { color: red;  */

            /* 		cursor:pointer;} */


            .TimerStartBtn{
                background-image: url('/Ilex-WS/resources/images/btn_start_timer.png');
            }
            .TimerStopBtn{
                background-image: url('/Ilex-WS/resources/images/btn_stop_timer.png');
            }
        </style>








        <script src="/Ilex-WS/resources/js/hdMainTable.js?1" type="text/javascript"></script>
        <script src="/Ilex-WS/resources/js/expandJquery.js" type="text/javascript"></script>

        <script type="text/javascript">

            function getParameterByName(name) {
                name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }



            var isOpera = !!(window.opera && window.opera.version);  // Opera 8.0+
            var isFirefox = testCSS('MozBoxSizing');                 // FF 0.8+
            var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
            // At least Safari 3+: "[object HTMLElementConstructor]"
            var isChrome = !isSafari && testCSS('WebkitTransform');  // Chrome 1+
            var isIE = /*@cc_on!@*/false || testCSS('msTransform');  // At least IE6

            function testCSS(prop) {
                return prop in document.documentElement.style;
            }




            //  window.onresize = function()
            //  {
            //      window.resizeTo(1150,820);
            //  }
            //  window.onclick = function()
            //  {
            //      window.resizeTo(1150,820);
            //  }

            document.onreadystatechange = function() {

                var state = document.readyState;
                // 	  if (state == 'interactive') {

                // 	  } else

                //alert(document.getElementById("ticketIdFake").value);

                $.ajax({
                    type: 'POST',
                    data: {"ticketId": document.getElementById("ticketIdFake").value},
                    url: pageContextPath + '/hdMainTable/checkLockStatus.html',
                    async: false,
                    success: function(data) {

                        if (data == "success") {

                            //$("div.tableWrapper table tr[RowID='" + ticketId + "']").find('span').removeClass('greenIcon');
                            //$("div.tableWrapper table tr[RowID='" + ticketId + "']").find('span').addClass('redIcon');

                        }

                    }
                });

                $(window).bind("beforeunload", function() {

                    //alert("start     1");
                    clearLockStatus(document.getElementById("ticketIdFake").value);
                    //alert("start    2");
                    if (document.getElementById('stopTimerButton').style.display == 'block')
                    {
                        $("#stopTimerButton").click();
                    }
                    //alert("start    3");
                });


                if (state == 'complete') {


                    if ($("#backupValue").html() == "Not Applicable") {
                        $("#backupValue").empty();
                        $("#backupValue").html("N/A");

                    }


                    getTimer();
                    trimEffort();

                    ////////////////////////////////////////////////////////////////////////////////////////
                    // workNotes table //

                    var arr = [], s = [];
                    title = '';

                    /* $('#workNotesTable tbody tr').sort(function(a, b) {
                        return $(a).text() < $(b).text();
                    }).each(function(i, v) {



                        var t = $('td:first', v).text();
                        var str = t;
                        t = str.substr(0, 5);
                        title = t;
                        if (arr.indexOf(t) === -1) {
                            arr.push(t);
                            s.push(v);
                        }

                    }).appendTo('#workNotesTable tbody').filter(s);

                    for (i in s) {

                        $(s[i]).attr("class", "header");
                    } */



                    /*
                     $(".header").each(function (i,v){


                     if ($(this).next().attr("class") == "odd"){

                     $(this).find('td:first-child').prepend('+ ');

                     }else {
                     $(this).find('td:first-child').prepend('<font color="#A9A9A9">+</font> ');

                     }

                     });






                     $('#workNotesTable tbody .header').nextUntil('.header').css('display','none');



                     $('.header').click(function(){

                     if ($(this).next().css('display') == 'none'){
                     var text = $(this).find('td:first-child').html();					// minus
                     var new_text = text.replace('+', '-');
                     $(this).find('td:first-child').html(new_text);
                     }
                     if ($(this).next().css('display') == 'table-row'){



                     var text = $(this).find('td:first-child').html();					// plus
                     var new_text = text.replace('-', '+');
                     $(this).find('td:first-child').html(new_text);
                     }

                     $(this).nextUntil('tr.header').slideToggle(100);


                     });
                     */

                    ////////////////////////////////////////////////////////////////////////////////////

                    ////////////////////////////////////////////////////////////////////////////////////////
                    // History table //

                    var arr = [], s = [];
                    title = '';

                   /*  $('#workNotesTable1 tbody tr').sort(function(a, b) {
                        return $(a).text() < $(b).text();
                    }).each(function(i, v) {



                        var t = $('td:nth-child(2)', v).text();
                        var str = t;
                        t = str.substr(0, 5);
                        title = t;
                        if (arr.indexOf(t) === -1) {
                            arr.push(t);
                            s.push(v);
                        }

                    }).appendTo('#workNotesTable1 tbody').filter(s);

                    for (i in s) {

                        $(s[i]).attr("class", "headerHistory");
                    } */




                    $(".headerHistory").each(function(i, v) {


                        if ($(this).next().attr("class") == "odd") {

                            //$(this).find('td:first-child').prepend('+ ');

                        } else {
                            //$(this).find('td:first-child').prepend('<font color="#A9A9A9">+</font> ');

                        }

                    });






                    // $('#workNotesTable1 tbody .headerHistory').nextUntil('.headerHistory').css('display','none');



                    // $('.headerHistory').click(function(){

                    // 	if ($(this).next().css('display') == 'none'){
                    // 		var text = $(this).find('td:first-child').html();					// minus
                    // 		var new_text = text.replace('+', '-');
                    // 		$(this).find('td:first-child').html(new_text);
                    // 	}
                    // 	if ($(this).next().css('display') == 'table-row'){



                    // 		var text = $(this).find('td:first-child').html();					// plus
                    // 		var new_text = text.replace('-', '+');
                    // 		$(this).find('td:first-child').html(new_text);
                    // 	}

                    // 	$(this).nextUntil('tr.headerHistory').slideToggle(100);


                    // 	});


                    ////////////////////////////////////////////////////////////////////////////////////


                    // 	////////////////////////////////////////////////////////////////////////////////////////
                    // 		// efforts table //

                    // 		var arr = [], s = []; title = '';

                    // 		$('#effortsTable tbody tr').sort(function(a, b){
                    // 		   return $(a).text() > $(b).text();
                    // 		}).each(function(i, v){
                    // 		    var t = $('td:nth-child(2)', v).text();
                    // 		    var str = t;
                    // 		     t = str.substr(0,5);
                    // 		     title = t;
                    // 		    if ( arr.indexOf(t) === -1) {
                    // 		       arr.push(t);
                    // 		       s.push(v);
                    // 		    }
                    // 		}).appendTo('#effortsTable tbody').filter(s).before(function(){
                    // 		    return '<tr class="headerEffort"><td><img src="/Ilex-WS/resources/images/expand.png" id="expand" align="absmiddle" height="10px" width="10px" /><img src="/Ilex-WS/resources/images/collapse.png" id="collapse" align="absmiddle" height="10px" width="10px" style="display:none;" />' +  $('td:nth-child(2)', this).text().substr(0,5) +  '</td></tr>';
                    // 		});

                    // 	$('.headerEffort').css('background-color','white');


                    // 		$('#effortsTable tbody .headerEffort').nextUntil('.headerEffort').css('display','none');

                    // 		$('.headerEffort').click(function(){

                    // 			if ($(this).next().css('display') == 'none'){
                    // 				$(this).find('#expand').css('display','none');
                    // 				$(this).find('#collapse').css('display','inline');
                    // 			}
                    // 			if ($(this).next().css('display') == 'table-row'){

                    // 				$(this).find('#expand').css('display','inline');
                    // 				$(this).find('#collapse').css('display','none');
                    // 			}

                    // 			$(this).nextUntil('tr.headerEffort').slideToggle(100);


                    // 			});


                    // 	////////////////////////////////////////////////////////////////////////////////////



                    // check if the provider. ref # and cust. ref # are there, if yes then make them non-editable.

                    if ($("#customerTicketReference").val() != "") {
                        $("#customerTicketReference").attr("readonly", "readonly");
                    }

                    if ($("#providerRefNo").val() != "") {
                        $("#providerRefNo").attr("readonly", "readonly");
                    }

                    /* if ($("#flagVal").html() == "stop"){
                     $("#stopTimerButton").css("display","block");
                     $("#restartTimerButton").css("display","none");
                     }

                     else if ($("#flagVal").html() == "restart"){

                     $("#restartTimerButton").css("display","block");
                     $("#stopTimerButton").css("display","none");

                     } */

                    document.getElementById("stopTimerButton").style.display = "none";
                    document.getElementById("restartTimerButton").style.display = "none";

                    // alert("1");
                    if ($("#flagVal").html() == "stop")
                    {
                        // not work for IE 9
                        //$("#stopTimerButton").css("display","block");
                        //$("#restartTimerButton").css("display","none");

                        // alert("2");

                        document.getElementById("stopTimerButton").style.display = "block";
                        document.getElementById("restartTimerButton").style.display = "none";


                        $("#stopTimerButton").click();
                        $("#restartTimerButton").click();


                    }

                    else if ($("#flagVal").html() == "restart")
                    {

                        // not working with IE 9
                        //$("#restartTimerButton").css("display","block");
                        //$("#stopTimerButton").css("display","none");

                        document.getElementById("restartTimerButton").style.display = "block";
                        document.getElementById("stopTimerButton").style.display = "none";


                        $("#restartTimerButton").click();
                    }


                    if (isIE) {

                        $("#effortBy").css("margin-left", "317px");
                        $("#effortBy").css("width", "100px");
                        $(".effortBy2").css("margin-left", "317px");

                        $(".effortEffort").css("padding-left", "165px");
                        $(".effortEffort").css("margin-right", "90px");
                        $("#worknoteDate").css("margin-right", "112px");
                        $("#worknoteAuthor").css("margin-right", "20px");
                        $("#worknoteNotes").css("margin-left", "50px");
                        $("#worknoteNotes").css("width", "100px");

                    }




                    // 		 window.onresize = function()
                    // 		 {
                    // 		     window.resizeTo(1150,820);
                    // 		 }
                    // 		 window.onclick = function()
                    // 		 {
                    // 		     window.resizeTo(1150,820);
                    // 		 }
                    // $("#ui-datepicker-div").css("display","none");


                    $('#phoneNumber').keypress(function(key) {
                        if (key.charCode < 48 || key.charCode > 57) {
                            return false;
                        }
                    });



                    if ($("#isFromMain").val() == 'yes')
                    {




                        checkResolvedState();
                        // $(".ui-dialog").css("height", "280px");
                        // $(".ui-dialog-titlebar").css("height", "20px");


                        $('#Screen2').find('input[type=button]').css("background-color", "#D3D3D3");
                        $('#Screen2').find('*').attr('disabled', true);
                        $(".siteStaticDetail").css("display", "none");


                        $("#ticketNumber").attr('disabled', false);
                        $(".nextActionDate").css("display", "none");

                        $('#Screen2').find('*').attr('disabled', true);

                        $("#ticketNumber").attr("readonly", "readonly");
                        $("#priority").attr("readonly", "readonly");

                    }
                    else {


                        $("#customerNameID").html($("#customerName").val() + " and " + $("#appendixName").val());
                        $("#ticket_type").css("display", "block");
                        $("#ticketType1").html("MSP Help Desk");
                        $("#ticket_type").css("margin-top", "10px");
                        $(".customerName").css("display", "block");

                        //  	$("#wugAttributesEdit").css("display","block");
                        $("#Screen1").css("display", "none");
                        $(".siteDetail").css("display", "none");

                        $("#ticketNumber").attr("readonly", "readonly");
                        $("#priority").attr("readonly", "readonly");
                        $("#workNotesInsert").css("display", "block");
                        $("#workNotesEdit").css("display", "block");
                        $("#effortEdit").css("display", "block");
                        $("#priority_label").html($("#priority").val());
                        $("#enterNewSite").css("display", "none");

                        /*
                         Change by cinnova start
                         */
                        $("#nextActionTimeRadio").removeAttr("checked");

                        $("#nextActionDateRadio").attr("checked", "checked");
                        checknextActionRadio($("#nextActionDateRadio"));

                        /*
                         Change by cinnova end
                         */

                        $("#createTicketButton").css("display", "none");

                        $("#updateTicketButton1").css("display", "inline-block");


                        //$("#ticketId").attr("value",currentTicketId);
                        $("#pageTitle2").text(
                                "${hdTicketDTO.ticketType} for ${hdTicketDTO.customerName}, ${hdTicketDTO.appendixName}, ${hdTicketDTO.siteDetailDTO.siteNumber}, ${hdTicketDTO.ticketNumber}");

                                                checkResolvedState();

                                                $("#tab-container").tabs();

                                                $("#workNotesTable").find('td').css('background', 'none');

                                                $('.etabs').removeClass('ui-widget-header');

                                            }



                                            //#D7E1EB
                                            //$("#Screen2:not(:last-child) :input").attr("disabled", true);
                                            //$(".divclass:not(:last-child) :input").attr("disabled", true);

                                            //  $('#preferredArrivalWindowTo').datetimepicker({
                                            //  dateFormat :"mm-dd-yy",
                                            //  showOn : "both",
                                            //  buttonImage : pageContextPath
                                            //  	+ '/resources/images/calendar.gif'

                                            //  }).next(".ui-datepicker-trigger").addClass(
                                            //  	"calImageStyle");

                                            //  $('#preferredArrivalWindowFrom').datetimepicker({
                                            //  dateFormat :"mm-dd-yy",
                                            //  showOn : "both",
                                            //  buttonImage : pageContextPath
                                            //  	+ '/resources/images/calendar.gif'

                                            //  }).next(".ui-datepicker-trigger").addClass(
                                            //  	"calImageStyle");

                                            //  $('#preferredArrivalTime').datetimepicker({
                                            //  dateFormat :"mm-dd-yy",
                                            //  showOn : "both",
                                            //  buttonImage : pageContextPath
                                            //  	+ '/resources/images/calendar.gif'

                                            //  }).next(".ui-datepicker-trigger").addClass(
                                            //  	"calImageStyle");


                                            $('#createTicketNextActionDate').datetimepicker({
                                                dateFormat: "mm/dd/yy",
                                                showOn: "both",
                                                buttonImage: pageContextPath
                                                        + '/resources/images/calendar.gif'
                                                        //  	,
                                                        //  	 beforeShow: function(input, instance) {
                                                        //  	    	if($('#createTicketNextActionDate').val()=="")
                                                        //  	    	{
                                                        //  	    	var currentDate=getCurrentDate();
                                                        //  	        $('#createTicketNextActionDate').datetimepicker('setDate', currentDate);
                                                        //  	    	}
                                                        //  	    }
                                            }).next(".ui-datepicker-trigger").addClass(
                                                    "calImageStyle");

                                            $("#ui-datepicker-div").css("display", "none");


                                            $('#createTicketEffortStart').datetimepicker({
                                                dateFormat: "mm/dd/yy",
                                                showOn: "both",
                                                buttonImage: pageContextPath
                                                        + '/resources/images/calendar.gif'

                                            }).next(".ui-datepicker-trigger").addClass(
                                                    "calImageStyle");
                                            $('#createTicketEffortStop').datetimepicker({
                                                dateFormat: "mm/dd/yy",
                                                showOn: "both",
                                                buttonImage: pageContextPath
                                                        + '/resources/images/calendar.gif'

                                            }).next(".ui-datepicker-trigger").addClass(
                                                    "calImageStyle");



                                        }


                                    }



                                    /*

                                     function enableScreen2(){



                                     if ($("#isFromMain").val() == 'yes')
                                     {


                                     $('#Screen2').find('*').attr('disabled', false);


                                     $('#Screen2').find('input[type=button]').css("background-color","#D7E1EB");
                                     //$(".customerName").css("display","block");
                                     //	$("#ticket_type").css("margin-top","10px");
                                     $("#ticket_type").css("display","block");
                                     $("#billingStatus").attr("value","select_bill");

                                     $("#enterNewSite").attr("onclick","createTicketSteph1("+$('#msaId').val()+")");
                                     //$('#enterNewSiteImg').attr('onclick','enterNewSite(${hdTicket.msaId});');
                                     //$(".customerName").css("display","block");
                                     $("#ticket_type").css("margin-top","7px");
                                     $("#ticketType1").css("margin-top","7px");
                                     $("#ticket_type").css("display","block");
                                     $("#ticketType1").css("display","inline-block");


                                     }
                                     else {
                                     $("#wugAttributesEdit").css("display","block");
                                     $("#Screen1").css("display","none");
                                     $(".siteDetail").css("display","none");
                                     $("#ticketNumber").attr("readonly","readonly");
                                     $("#priority").attr("readonly","readonly");
                                     $("#workNotesInsert").css("display","block");
                                     $("#workNotesEdit").css("display","block");

                                     $("#updateTicketButton1").css("display","block");
                                     $("#createTicketButton").css("display","none");
                                     $("#ticketId").attr("value",currentTicketId);


                                     }

                                     }
                                     */

                                    //  function checkEffortRadio(ele)
                                    //  {

                                    // 	var effortRadioVal = $(ele).val();



                                    // 	if (effortRadioVal == 'date')
                                    // 		{


                                    // 		$(".effortTime").css("display","none");
                                    // 		$(".spacingDiv").css("display","none");
                                    // 		$(".EffortDate").css("display","block");

                                    // 		}

                                    // 	else {

                                    // 		$(".effortTime").css("display","inline-table");
                                    // 		$(".spacingDiv").css("display","block");
                                    // 		$(".EffortDate").css("display","none");

                                    // 	}
                                    //  }




                                    //  function checknextActionRadio(ele){

                                    // 	 var nextActionRadioVal = $(ele).val();
                                    // 	 if (nextActionRadioVal == 'date')
                                    // 		{


                                    // 		$(".nextActionTime").css("display","none");
                                    // 		$(".nextActionDate").css("display","block");

                                    // 		}

                                    // 	else {

                                    // 		$(".nextActionTime").css("display","inline-table");
                                    // 		$(".nextActionDate").css("display","none");

                                    // 	}


                                    //  }
                                    /*

                                     function appendData(){


                                     var hdCustomerValue = "";


                                     if($("#hdCustomer").val() ){
                                     hdCustomerValue = $("#hdCustomer").val();
                                     isFromMain= "yes";
                                     }

                                     //  var hdCustomerValue = "658~2712~Admiral Petroleum Company~Appendix 1 - NetMedX~NetMedX,MSP Help Desk";
                                     var hdTicketNumber = '';
                                     if($("#ticketNumber").val()){
                                     hdTicketNumber = $("#ticketNumber").val();

                                     }

                                     var url = pageContextPath + '/hdMainTable/createTicketStep2.html';



                                     $.ajax({
                                     type : 'POST',
                                     data : {
                                     hdCustomer : hdCustomerValue,
                                     isFromMain: isFromMain,
                                     hdTicketNumber: hdTicketNumber


                                     },
                                     url : url,
                                     async : false,
                                     success : function(data) {


                                     //$("#mainDiv").empty();
                                     var $result = $(data);

                                     $("#site_dd").empty();
                                     $("#customerNameID").empty();
                                     $("#categoryDiv").empty();
                                     $("#configDiv").empty();
                                     $("#nextActionDiv").empty();
                                     $("#ticketType1").empty();
                                     $("#testdiv").empty();
                                     $("#enterNewSite").empty();




                                     $("#site_dd").html($result.find('#site_dd').html());
                                     $("#customerNameID").html($result.find('#customerNameID').html());
                                     $("#categoryDiv").html($result.find('#categoryDiv').html());
                                     $("#configDiv").html($result.find('#configDiv').html());
                                     $("#nextActionDiv").html($result.find('#nextActionDiv').html());
                                     $("#ticketType1").html($result.find('#ticketType1').html());
                                     $("#testdiv").html($result.find('#testdiv').html());
                                     $("#enterNewSite").html($result.find('#enterNewSite').html());





                                     enableScreen2();



                                     }

                                     });

                                     }
                                     */
                                    function createTicketSteph1(msaId) {



                                        $.blockUI({
                                            message: $('#test'),
                                            css: {
                                                width: '500px',
                                                height: 'auto',
                                                position: 'absolute',
                                                color: 'black'
                                            },
                                            overlayCSS: {
                                                backgroundColor: '#f0f0f0',
                                                opacity: 0.3
                                            },
                                            centerX: true,
                                            centerY: true,
                                            fadeIn: 0,
                                            fadeOut: 0
                                        });
                                        //ticketPopUp=true;


                                        //$('#test').parent('.blockUI.blockMsg')
                                        //	.css('left', $($tr).offset().left);
                                        //$('#test').parent('.blockUI.blockMsg')
                                        //		.css('top', $($tr).offset().top + 20);



                                        $.ajax({
                                            type: 'GET',
                                            url: pageContextPath + '/hdMainTable/enterNewSite.html?msaId=' + msaId,
                                            success: function(obj) {

                                                //alert(obj);
                                                $("#testBody").empty();

                                                $("#testBody").html(obj);

                                                //var $result = $(obj);

                                                //$("#testBody").html($result.find('.WASTD0003').html());

                                            }
                                        });

                                    }


                                    ///////////////////////////////////////////////////////////////////////////////////////

                                    // timer
                                    function trimEffort() {

                                        $(".effortMins").each(function(e, v) {

                                            var s = $(this).html();

                                            s = s.substring(0, s.indexOf('.'));

                                            $(this).html(s);

                                        });

                                    }

                                    var calcDiff = null;

                                    function getTimer() {

                                        if ($(".effortEndDate:last").html() == 'n/a' && ($(".effortMins:last").html() == '0' || $(".effortMins:last").html() == '0.00')) // if the last most end date is not empty. means last most effort is still open.
                                        {



                                            calcDiff = setInterval(function() {
                                                var date_start_val = $(".effortStartDate:last").html();
                                                var date_start = new Date(date_start_val);
                                                var date_now = new Date();



                                                seconds = Math.floor((date_now - (date_start)) / 1000);
                                                minutes = Math.floor(seconds / 60);
                                                hours = Math.floor(minutes / 60);
                                                days = Math.floor(hours / 24);

                                                hours = hours - (days * 24);
                                                minutes = minutes - (days * 24 * 60) - (hours * 60);
                                                seconds = seconds - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);

                                                $("#time").html(("0" + hours).slice(-2) + ":" + ("0" + minutes).slice(-2) + ":" + ("0" + seconds).slice(-2));
                                            }, 1000);


                                        }
                                    }

                                    ///////////////////////////////////////////////////////////////////////////////////////





                                    function sendTimerStopDate() {
                                        var tid = $("#ticketId").val();
                                        var dateTime = getDateTime();

                                        $.ajax({
                                            type: 'GET',
                                            data: {
                                                effortFlag: "stop",
                                                ticketId: tid,
                                                effortDate: dateTime

                                            },
                                            url: pageContextPath + '/hdMainTable/effortChange.html',
                                            success: function(obj) {
                                                //if (obj == "success"){

                                                $("#stopTimerButton").css("display", "none");
                                                $("#restartTimerButton").css("display", "block");
                                                getEfforts();

                                                if (calcDiff != null)
                                                {
                                                    clearInterval(calcDiff);
                                                }

                                                $("#time").html('');
                                                trimEffort();
                                                //}
                                            }

                                        });

                                    }

                                    function sendTimerRestartDate() {

                                        var tid = $("#ticketId").val();
                                        var dateTime = getDateTime();

                                        $.ajax({
                                            type: 'GET',
                                            data: {
                                                effortFlag: "restart",
                                                ticketId: tid,
                                                effortDate: dateTime

                                            },
                                            url: pageContextPath + '/hdMainTable/effortChange.html',
                                            success: function(obj) {
                                                //if (obj == "success"){

                                                $("#restartTimerButton").css("display", "none");
                                                $("#stopTimerButton").css("display", "block");
                                                getEfforts();
                                                trimEffort();
                                                getTimer();

                                                //	}



                                            }
                                        });

                                    }


                                    function getEfforts() {


                                        var tid = $("#ticketId").val();

                                        $.ajax({
                                            type: 'GET',
                                            data: {ticketId: tid},
                                            url: pageContextPath + '/hdMainTable/getEffortDetails.html',
                                            success: function(json_data) {

                                                $("#effortsTable").find("tbody").remove();
                                                var json_obj = JSON.parse(json_data);
                                                var table_obj = $('<tbody>');
                                                $('#effortsTable').append(table_obj);

                                                if (json_obj.length > 0) {

                                                    // $("#effortsTable").find("tbody").remove();

                                                    for (var i = 0; i < json_obj.length; i++)
                                                    {


                                                        var table_row = $('<tr>');
                                                        var effort_cell = $('<td>', {class: 'effortMins', html: json_obj[i].effort});
                                                        var effortStartDate_cell = $('<td>', {class: 'effortStartDate', html: json_obj[i].effortDate});
                                                        var effortEndDate_cell = $('<td>', {class: 'effortEndDate', html: json_obj[i].effortEndDate});
                                                        var workedBy_cell = $('<td>', {html: json_obj[i].workedBy});



                                                        table_row.append(effort_cell);
                                                        table_row.append(effortStartDate_cell);
                                                        table_row.append(effortEndDate_cell);
                                                        table_row.append(workedBy_cell);

                                                        $(table_obj).append($(table_row));
                                                    }

                                                    $("#effortsTable").append(table_obj);

                                                }
                                            }
                                        });


                                    }



                                    function checkCancelledAndResolved(ele) {


                                        if ($(ele).val() == "Cancelled") {
                                            // disable all fields.

                                            $('#Screen2 :input').attr('disabled', true);
                                            $(".enabled").attr('disabled', false);
                                            if ($("#stopTimerButton").css("display") == "block") {

                                                $("#stopTimerButton").removeAttr("disabled");
                                            }

                                        } else if ($(ele).val() == "RES") {

                                            $('#Screen2').find('*').attr('disabled', false);
                                            $(".disabled").attr('disabled', true);
                                            $(".disabled :input").attr('disabled', true);
                                        } else {
                                            $('#Screen2').find('*').attr('disabled', false);
                                        }


                                    }



        </script>

    </head>
    <body>
        <form:form commandName="hdTicketDTO" id="hdTicketCreateForm2">
            <div id="popup_progress">
                <div id="light" class="white_content">
                    <p id="ticketProcessStatus">Reopening Ticket...</p>
                    <div class="meter">
                        <span style="width: 5%"></span>
                    </div>
                    <input id="confirmButton" type="button" value="Close" class="button">
                </div>
                <div id="fade" class="black_overlay"></div>
            </div>
            <div id="testdiv">
                <form:hidden path="customerName"/>
                <form:hidden path="appendixName"/>
                <input id="userId" name="userId" type="hidden" value="<%= (String) session.getAttribute("userid")%>">
                <form:hidden path="msaId"/>
                <form:hidden path="appendixId"/>
                <form:hidden path="ticketType"/>
                <form:hidden path="ticketSource"/>
                <form:hidden path="isFromMain" />
                <form:hidden path="ticketId" />
                <form:hidden path="siteId"/>
                <form:hidden path="backUpCktStatus" />
                <!-- To save the last values to generate the email body -->
                <form:hidden path="lastCreateTicketNextActionDate" />
                <form:hidden path="lastTicketStatus" />
                <form:hidden path="lastNextAction"/>
                <form:hidden path="lastNextActionDueDate"/>
                <form:hidden path="primaryState"/>
                <!-- To save the last values to generate the email body -->
            </div>
            <div id="mainDiv" style="background-color: menu;">
                <div class="WASTD0003" style="text-align:left;margin-top: 0px" id="Screen1">

                    <!-- 	<div class="PCSTD0011 divFloatLeft" style="text-align:left;margin-right:5px;margin-top: 10px"> -->
                    <!-- 	Customer and Appendix: -->
                    <!-- 	</div> -->
                    <!-- 	<div style="display:inline; width:460px;margin-top: 10px"> -->
                    <!-- 		<select style="width: 700px;margin-top: 10px" id="hdCustomer"  onchange=" appendData();"> -->
                    <!-- 			<option value="">Select Customer and Appendix</option> -->
                    <%-- 			<c:forEach items="${customers}" var="customer"> --%>
                    <%-- 				<optgroup label="${customer.key}" > --%>
                    <%-- 					<c:forEach items="${customer.value}" var="customerList"> --%>
                    <%-- 						<option  value="${customerList.msaId}~${customerList.appendixId}~${customerList.customerName}~${customerList.appendixName}~${customerList.apppendicBreakOut}">${customerList.customerName}, ${customerList.appendixName} - ${customerList.apppendicBreakOut}</option> --%>
                    <%-- 					</c:forEach> --%>
                    <%-- 			</c:forEach> --%>
                    <!-- 		</select> -->
                    <!-- 	</div> -->
                    <!-- <div style="display:inline;">
                            <input type="button" value="Next" onclick="createTicketStep2(); enableScreen2(); " class="FMBUT0006" style="width: 100px;"/>
                    </div> -->

                </div>

                <div class="WASTD0003 PCSTD0010" id="Screen2" style="text-align: left;margin-top: 0px; padding-top: 10px;width: 100%" >

                    <div class="PCSTD0010 divFloatLeft customerName" style="width: 160px;display:none;" >

                        <b>Customer and Appendix:</b>
                    </div>
                    <div style="display:none;" class="customerName" id="customerNameID">
                        ${hdTicket.customerName} and  ${hdTicket.appendixName}

                    </div>
                    <div
                        style="width: 25%; margin-left: -9px; background-color: menu; float: left; padding-left: 10px;">
                        <div
                            style="display: none; width: 150px; margin-top: 10px; margin-right: 10px;"
                            class="PCSTD0010 divFloatLeft" id="ticket_type">
                            <b>Ticket Type:</b>
                        </div>
                        <div
                            style="margin-top: 10px; background-color: menu; margin-bottom: 0px; padding-bottom: 10px;"
                            id="ticketType1">${hdTicket.ticketType}</div>
                        <c:if test="${hdTicketDTO.createdOnDateString ne ''}">
                        </div>
                        <div
                            style="width: 75%; margin-left: -9px; background-color: menu; float: left; padding-top: 10px; padding-bottom: 2px;">

                            <div
                                style="float: left; display: block; float: left; margin-left: 9px; margin-top: 0px; width: 90px;"
                                class="PCSTD0010 divFloatLeft" id="createdOnLabelId">
                                <font color="#A9A9A9">Created On:</font>
                            </div>
                            <div
                                style="float: left; margin-top: 0px; height: 20px; margin-left: 35px;"
                                id="createdOnDateId">${hdTicketDTO.createdOnDateString}</div>
                        </div>
                    </c:if>
                    <!-- 	<hr color="gray" size="2" style="margin-top: 15px;height:20px;"> -->
                    <div style="width: 100%;margin-left: -9px;background-color: #FAF8CC;border-top: solid 2px gray;float: left;margin-top:0px;margin-bottom: 2px;" >
                        <div style="width: 150px;margin-top: 8px;float: left;margin-left: 10px;margin-bottom: 5px;">
                            Ilex Ticket#:

                        </div>
                        <div style="margin-top: 5px;float: left;width:190px;margin-bottom: 5px;">
                            <form:input path="ticketNumber"
                                        placeholder="Enter Ticket Number" cssStyle="width: 178px;" />
                            <%-- <form:input path="preferredArrivalTime" cssStyle="width:178px;"/> --%>
                        </div>
                        <div style="width: 115px;margin-top: 8px;float: left;margin-left: 68px;">
                            Cust. Ref#:
                        </div>
                        <div style="margin-top: 05px;float: left;width:190px;">

                            <form:input maxlength="15" path="customerTicketReference" cssStyle="width:178px;"/>

                            <%-- <form:select path="siteDetailDTO.siteId" cssStyle="width: 180px;" onchange="fillPOCDetails(this);">
                                            <option value="">Select</option>
                                            <c:forEach items="${sites}" var="site">
                                                            <option contactName="${site.primaryName}" phoneNumber="${site.sitePhone}" emailAddress="${site.primaryEmail}" siteNumber="${site.siteNumber}"
                                                                            siteAddress="${site.siteAddress}" siteCity="${site.siteCity}" siteState="${site.state}"
                                                                            siteCountry="${site.country}" siteZipCode="${site.siteZipcode}"  value="${site.siteId}">
                                                                                    ${site.siteNumber} - ${site.siteAddress}, ${site.siteCity},${site.state}</option>
                                            </c:forEach>
                                    </form:select> --%>
                        </div>
                        <div style="width: 137px;margin-top: 8px;float: left;margin-left: 6px;">
                            Provider Ref#:
                        </div>
                        <div style="margin-top: 05px;float: left;width:190px;">
                            <form:input maxlength="15" path="providerRefNo" cssStyle="width:178px;"/>
                        </div>
                    </div>
                    <div class="divFloatLeft" style="width: 33%;margin-bottom: 20px;">



<!-- 					<div class="PCSTD0010 divFloatLeft" -->
<!-- 						style="width: 150px; margin-top: 10px; height: 20px;"> -->
<!-- 						Site Client Hd is first <br /> escalation contact? -->
<!-- 					</div> -->

<!-- 					<div style="margin-top: 10px; height: 20px;"> -->
<%-- 						<form:select class="sitedto" path="siteDetailDTO.client1stContact" --%>
<%-- 							cssStyle="width: 180px;"> --%>
<%-- 							<option <c:if test="${hdTicketDTO.siteDetailDTO.client1stContact eq 'no'}"> selected="selected" </c:if> value="no">No</option> --%>
<%-- 							<option <c:if test="${hdTicketDTO.siteDetailDTO.client1stContact eq 'yes'}"> selected="selected" </c:if> value="yes">Yes</option> --%>

<%-- 						</form:select> --%>
<!-- 					</div> -->




					<div class="PCSTD0010 divFloatLeft siteDetail" style="width: 150px;margin-top: 10px;height:20px;" >
                            Site Number:<font style="color: red;">*</font>
                        </div>

                        <div id="site_dd" style="float:left;">
                            <div class="PCSTD0010 siteDetail" style="margin-top: 10px;height:20px;margin-right: 10px;">
                                <form:select class="sitedto" path="siteDetailDTO.siteId"  cssStyle="width: 180px;" onchange="fillPOCDetails(this);">
                                    <option value="">Select Site Number</option>
                                    <c:forEach items="${sites}" var="site">
                                        <option contactName="${site.primaryName}" phoneNumber="${site.sitePhone}" emailAddress="${site.primaryEmail}" siteNumber="${site.siteNumber}"
                                                siteAddress="${site.siteAddress}" siteCity="${site.siteCity}" siteState="${site.state}"
                                                siteCountry="${site.country}" siteZipCode="${site.siteZipcode}"  value="${site.siteId}">
                                            ${site.siteNumber} - ${site.siteAddress}, ${site.siteCity},${site.state}</option>
                                        </c:forEach>
                                    </form:select>
                            </div>
                        </div>

                        <div style="margin-top: 0px;height:0px;" >
                            <!-- 		<a href="javascript:void(0);" id="enterNewSite" >[Add Site]</a> -->
                            <%-- 			<img src="/Ilex-WS/resources/images/AddSite.png" id="enterNewSiteImg" onclick="enterNewSite(${hdTicket.msaId});"/> --%>
                            <%-- 			<input type="button" value="Enter New Site" onclick="enterNewSite(${hdTicket.msaId});" class="FMBUT0006" style="width: 120px;background-image:/Ilex-WS/resources/images/AddSite.png"/> --%>
                        </div>




                        <div  class="PCSTD0010 divFloatLeft siteDetail" style="width: 150px;margin-top: 10px;height:20px;">
                            Site Address:
                        </div>
                        <div id="siteNumber" style="margin-top: 10px;height:20px;" class="siteDetail">
                            <!-- 				<a href="javascript:void(0);" onclick="window.open('masterSiteAction.do?siteName=');">[SiteNumber]</a>  -->
                        </div>
                        <div id="siteAddress" style="margin-top: 0px;margin-left: 150px;white-space: normal;" class="siteDetail">
                            [Address]
                        </div>
                        <div id="siteAddress1" style="margin-top: 5px;margin-left: 150px;" class="siteDetail">
                            [City,ST,[Country for Non-US]<br />Zip code]
                        </div>



                        <div   class="PCSTD0010 divFloatLeft siteStaticDetail" style="width: 150px;margin-top: 10px;height:20px;">
                            Site Address:
                        </div>
                        <div  id="siteNumber" style="margin-top: 10px;height:20px;" class="siteStaticDetail">
                            <a href="javascript:void(0);" onclick="openSiteDetailPopup('${hdTicketDTO.siteDetailDTO.siteNumber}', '${hdTicketDTO.msaId}');">${hdTicketDTO.siteDetailDTO.siteNumber}</a>
                        </div>
                        <div  id="siteAddress" style="margin-top: 5px;margin-left: 150px;white-space: normal;" class="siteStaticDetail">
                            ${hdTicketDTO.siteDetailDTO.siteAddress}
                        </div>
                        <div  id="siteAddress1" style="margin-top: 5px;margin-left: 150px;" class="siteStaticDetail">
                            ${hdTicketDTO.siteDetailDTO.siteCity},${hdTicketDTO.siteDetailDTO.state},${hdTicketDTO.siteDetailDTO.siteZipcode}
                        </div>


                        <div style="height: 0px;">&nbsp;</div>

                        <div  class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;">
                            Caller:
                        </div>
                        <div style="margin-top: 10px;height:20px;">
                            <form:input   path="contactName"  placeholder="Enter Name" cssStyle="width: 178px;"/>
                        </div>
                        <div  class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;">
                            Caller Email:
                        </div>
                        <div style="margin-top: 10px;height:20px;">
                            <form:input  path="emailAddress"  placeholder="Enter Email" cssStyle="width: 178px;"/>
                        </div>
<!--                         <div  class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;"> -->
<!--                             Additional Recipients: -->
<!--                         </div> -->
<!--                         <div style="margin-top: 10px;height:20px;"> -->
<%--                             <form:input  path="emailAddressAdditional"  placeholder="Enter Additional Recipients" cssStyle="width: 178px;"/> --%>
<!--                         </div><div  class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;"> -->
<!--                             Client Alert Email: -->
<!--                         </div> -->
<!--                         <div style="margin-top: 10px;height:20px;"> -->
<%--                             <form:input   path="emailAddress"  placeholder="Enter Email" cssStyle="width: 178px;"/> --%>
<!--                         </div> -->
<!--                         <div  class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;"> -->
<!--                             Additional Recipients: -->
<!--                         </div> -->
<!--                         <div style="margin-top: 10px;height:20px;"> -->
<%--                             <form:input  path="emailAddressAdditional"  placeholder="Enter Additional Recipients" cssStyle="width: 178px;"/> --%>
<!--                         </div> -->
                        <div  class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;">
                            Caller Phone:
                        </div>
                        <div style="margin-top: 10px;height:20px;">
                            <form:input path="phoneNumber" onkeypress="validateDigits()"  placeholder="Enter Phone" cssStyle="width: 178px;"/>

                        </div>

                        <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;">
                            Contact Type:<font style="color: red;">*</font>
                        </div>

                        <div style="margin-top: 10px;height:20px;">
                            <form:select path="contactTypeId" id="contactTypeId"
                                         cssStyle="width: 180px;">
                                <option <c:if test="${hdTicketDTO.contactTypeId eq '3'}"> selected="selected" </c:if> value="3">Email</option>
                                <option <c:if test="${hdTicketDTO.contactTypeId eq '1'}"> selected="selected" </c:if> value="1">NMS</option>
                                <option <c:if test="${hdTicketDTO.contactTypeId eq '2'}"> selected="selected" </c:if> value="2">Walk-Up</option>

                                    <option <c:if test="${hdTicketDTO.contactTypeId eq '4'}"> selected="selected" </c:if> value="4">CCC</option>
                                <option <c:if test="${hdTicketDTO.contactTypeId eq '5'}"> selected="selected" </c:if> value="5">Phone</option>
                                <option <c:if test="${hdTicketDTO.contactTypeId eq '6'}"> selected="selected" </c:if> value="6">Chat</option>
                                <option <c:if test="${hdTicketDTO.contactTypeId eq '7'}"> selected="selected" </c:if> value="7">Web Services</option>

                            </form:select>
                        </div>

                        <!-- 		<div style="margin-top: 15px;height:20px;"> -->
                        <%-- 			<input type="button" value="Enter New Site" onclick="enterNewSite(${hdTicket.msaId});" class="FMBUT0006" style="width: 120px;"/> --%>
                        <!-- 		</div> -->


                        <div  class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;">
                            Billable:<font style="color: red;">*</font>
                        </div>
                        <div style="margin-top: 10px;height:20px;">
                            <form:select path="billingStatus" cssStyle="width: 180px;" >
                                <option <c:if test="${hdTicketDTO.billingStatus eq false}"> selected="selected" </c:if> value="0">No</option>
                                <option <c:if test="${hdTicketDTO.billingStatus eq true}"> selected="selected" </c:if> value="1">Yes</option>

                            </form:select>

                        </div>
                        <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;">
                            Back up Circuit Status:
                        </div>

                        <div style="margin-top: 10px;height:20px;" id="backupValue" >

                            <c:choose>
                                <c:when test="${hdTicketDTO.backUpCktStatus eq 'Not Applicable' }">
                                    N/A
                                </c:when>

                                <c:otherwise>
                                    ${hdTicketDTO.backUpCktStatus}
                                </c:otherwise>
                            </c:choose>

                        </div>

                        <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;">
                            Urgency:<font style="color: red;">*</font>
                        </div>
                        <div style="margin-top: 10px;height:20px;">
                            <form:select path="urgency" cssStyle="width: 180px;" onchange="checkSecondList()">
                                <option value="0">Select Urgency</option>
                                <option <c:if test="${hdTicketDTO.urgency eq '1-High'}"> selected="selected" </c:if> value="1">1-High</option>
                                <option <c:if test="${hdTicketDTO.urgency eq '2-Medium'}"> selected="selected" </c:if> value="2">2-Medium</option>
                                <option <c:if test="${hdTicketDTO.urgency eq '3-Low'}"> selected="selected" </c:if> value="3">3-Low</option>
                            </form:select>

                        </div>

                        <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;">
                            Impact:<font style="color: red;">*</font>
                        </div>
                        <div style="margin-top: 10px;height:20px;">
                            <form:select path="impact"
                                         cssStyle="width: 180px;" onchange="checkSecondList()" >
                                <option value="0">Select Impact</option>
                                <option <c:if test="${hdTicketDTO.impact eq '1-High'}"> selected="selected" </c:if> value="1">1-High</option>
                                <option <c:if test="${hdTicketDTO.impact eq '2-Medium'}"> selected="selected" </c:if> value="2">2-Medium</option>
                                <option <c:if test="${hdTicketDTO.impact eq '3-Low'}"> selected="selected" </c:if> value="3">3-Low</option>
                            </form:select>
                        </div>

                        <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;">
                            Priority:
                        </div>
                        <div style="margin-top: 10px;height:20px;">
                            <form:hidden path="priority"
                                         cssStyle="width: 178px;" style="display:none;" />
                            <!-- 					 placeholder="Enter Priority" -->
                            <span id="priority_label">Select Urgency/Impact</span>
                        </div>
                        <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 5px;height:20px;">
                            Category:<font style="color: red;">*</font>
                        </div>
                        <div id="categoryDiv" style="margin-top: 5px;height:20px;width: 180px;margin-left:0px;" class="divFloatLeft" >
                            <form:select path="category"
                                         cssStyle="width: 180px;" onchange="setVal()">
                                <option value="">Select Category</option>
                                <c:forEach items="${categories}" var="category">
                                    <option <c:if test="${hdTicketDTO.category eq category.category}"> selected="selected" </c:if> value="${category.categoryId}">${category.category}</option>
                                </c:forEach>
                            </form:select>
                        </div>

                        <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;">
                            Configuration Item:<font style="color: red;">*</font>
                        </div>
                        <div id="configDiv" style="margin-top: 10px;height:20px;" class="divFloatLeft">
                            <form:select path="configurationItem" cssStyle="width: 180px;">
                                <option value="">Select Configuration Item</option>
                                <c:forEach items="${configurationItems}" var="configurationItem">
                                    <option <c:if test="${hdTicketDTO.configurationItem eq configurationItem.configurationItem}"> selected="selected" </c:if> value="${configurationItem.configurationItemId}">${configurationItem.configurationItem}</option>
                                </c:forEach>
                            </form:select>
                        </div>
                        
                           
                        










<!-- 					<div class="PCSTD0010 divFloatLeft" -->
<!-- 						style="width: 150px; margin-top: 10px; height: 20px;"> -->
<!-- 						Client HD Contact Name:</div> -->
<!-- 					<div style="margin-top: 10px; height: 20px;" class="divFloatLeft"> -->
<%-- 						<form:input path="clientHDContactName" placeholder="Enter Name" --%>
<%-- 							cssStyle="width: 178px;" /> --%>
<!-- 					</div> -->
<!-- 					<div class="PCSTD0010 divFloatLeft" -->
<!-- 						style="width: 150px; margin-top: 10px; height: 20px;"> -->
<!-- 						Client HD Email:</font> -->
<!-- 					</div> -->
<!-- 					<div style="margin-top: 10px; height: 20px;" class="divFloatLeft"> -->
<%-- 						<form:input path="clientHDEmailAddress" placeholder="Enter Email" --%>
<%-- 							cssStyle="width: 178px;" /> --%>
<!-- 					</div> -->
<!-- 					<div class="PCSTD0010 divFloatLeft" -->
<!-- 						style="width: 150px; margin-top: 10px; height: 20px;"> -->
<!-- 						Client HD Phone:</div> -->
<!-- 					<div style="margin-top: 10px; height: 20px;" class="divFloatLeft"> -->
<%-- 						<form:input path="clientHDPhoneNumber" onkeypress="validateDigits()" --%>
<%-- 							placeholder="Enter Phone" cssStyle="width: 178px;" /> --%>

<!-- 					</div> -->










                        

                        <div class="PCSTD0010 resolutionDiv" style="width: 350px;margin-top: 60px;margin-left: 0px;display:block;">
                            <b>Resolution</b>
                        </div>

                        <div class="PCSTD0010 divFloatLeft resolutionDiv" style="width: 150px;margin-top: 10px;height:20px;">
                            Resolution:<font style="color: red;">*</font>
                        </div>
                        <div style="margin-top: 10px;height:20px;" class="resolutionDiv">
                            <form:select path="resolutionId"
                                         cssStyle="width: 180px;" >
                                <option value="0">Select Resolution</option>
                                <option <c:if test="${hdTicketDTO.resolutionId eq '3'}"> selected="selected" </c:if> value="3" >Power cycle modem & router</option>

                                    <option <c:if test="${hdTicketDTO.resolutionId eq '2'}"> selected="selected" </c:if> value="2">Power cycle router</option>

                                    <option <c:if test="${hdTicketDTO.resolutionId eq '1'}"> selected="selected" </c:if> value="1">Power cycle modem</option>

                                    <option <c:if test="${hdTicketDTO.resolutionId eq '7'}"> selected="selected" </c:if> value="7">No troubleshooting performed</option>

                                    <option <c:if test="${hdTicketDTO.resolutionId eq '6'}"> selected="selected" </c:if> value="6">Replace defective equipment</option>

                                    <option <c:if test="${hdTicketDTO.resolutionId eq '5'}"> selected="selected" </c:if> value="5">Power returned</option>

                                    <option <c:if test="${hdTicketDTO.resolutionId eq '4'}"> selected="selected" </c:if> value="4">Provider repair</option>

                                    <option <c:if test="${hdTicketDTO.resolutionId eq '8'}"> selected="selected" </c:if> value="8">Reconfigured device</option>
                            </form:select>
                        </div>

                        <div class="PCSTD0010 divFloatLeft resolutionDiv" style="width: 150px;margin-top: 10px;height:20px;">
                            Point of Failure:<font style="color: red;">*</font>
                        </div>
                        <div style="margin-top: 10px;height:20px;" class="resolutionDiv">
                            <form:select path="pointOfFailureId"
                                         cssStyle="width: 180px;" >
                                <option value="0">Select Point of Failure</option>
                                <option <c:if test="${hdTicketDTO.pointOfFailureId eq '3'}"> selected="selected" </c:if> value="3">ISP</option>

                                    <option <c:if test="${hdTicketDTO.pointOfFailureId eq '2'}"> selected="selected" </c:if> value="2">Contingent</option>

                                    <option <c:if test="${hdTicketDTO.pointOfFailureId eq '1'}"> selected="selected" </c:if> value="1">Customer</option>
                            		<option <c:if test="${hdTicketDTO.pointOfFailureId eq '4'}"> selected="selected" </c:if> value="4">Other</option>	
                            </form:select>
                        </div>

                        <div class="PCSTD0010 divFloatLeft resolutionDiv" style="width: 150px;margin-top: 10px;height:20px;">
                            Root Cause:<font style="color: red;">*</font>
                        </div>
                        <div style="margin-top: 10px;height:20px;" class="resolutionDiv">
                            <form:select path="rootCauseId"
                                         cssStyle="width: 180px;" >
                                <option value="0">Select Root Cause</option>

                                <option <c:if test="${hdTicketDTO.pointOfFailureId eq '3'}"> selected="selected" </c:if> value="3">Customer caused outage</option>

                                    <option <c:if test="${hdTicketDTO.pointOfFailureId eq '2'}"> selected="selected" </c:if> value="2">Configuration error</option>

                                    <option <c:if test="${hdTicketDTO.pointOfFailureId eq '1'}"> selected="selected" </c:if> value="1">General connection error</option>

                                    <option <c:if test="${hdTicketDTO.pointOfFailureId eq '7'}"> selected="selected" </c:if> value="7">DynDNS</option>

                                    <option <c:if test="${hdTicketDTO.pointOfFailureId eq '6'}"> selected="selected" </c:if> value="6">Provider Outage</option>

                                    <option <c:if test="${hdTicketDTO.pointOfFailureId eq '5'}"> selected="selected" </c:if> value="5">Hardware failure</option>

                                    <option <c:if test="${hdTicketDTO.pointOfFailureId eq '4'}"> selected="selected" </c:if> value="4">Power outage</option>
                            </form:select>
                        </div>

                        <!-- 			<div id="wugAttributesEdit" style="display:none;"> -->
                        <!-- 				<div style="margin-left: 0px;margin-bottom: 10px;" class="PCSSH0001">WUG Attributes</div> -->
                        <!-- 				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">Help Desk #:</div> -->
                        <%-- 				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.wugDetailsDTO.wugHelpDesk}&nbsp;</div> --%>
                        <!-- 				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">APC Address:</div> -->
                        <%-- 				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.wugDetailsDTO.wugApcAddress}&nbsp;</div> --%>
                        <!-- 				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">Modem Type:</div> -->
                        <%-- 				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.wugDetailsDTO.wugModemType}&nbsp;</div> --%>
                        <!-- 				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">Circuit IP:</div> -->
                        <%-- 				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.wugDetailsDTO.wugCircuitIp}&nbsp;</div> --%>

                        <!-- 			</div> -->

                        <div style="float: left;width: 410px;margin-top: 15px;">
                            <div style="float: left;width: 200px;margin-left: 5px;">

                                <div style="margin-left: 0px; margin-bottom: 10px;"
                                     class="PCSSH0001">WUG Attributes</div>
                                <div style="margin-bottom: 5px;width: 95px;"
                                     class="divFloatLeft">Help Desk #:</div>
                                <div
                                    style="margin-bottom: 5px; white-space: normal; word-wrap: break-word; overflow: hidden; ">${hdTicketDTO.wugDetailsDTO.wugHelpDesk}&nbsp;</div>
                                <div style="margin-bottom: 5px;width: 95px;"
                                     class="divFloatLeft">APC Address:</div>
                                <div
                                    style="margin-bottom: 5px; white-space: normal; word-wrap: break-word; overflow: hidden;">${hdTicketDTO.wugDetailsDTO.wugApcAddress}&nbsp;</div>
                                <div style="margin-bottom: 5px;width: 95px;"
                                     class="divFloatLeft">Modem Type:</div>
                                <div
                                    style="margin-bottom: 5px; white-space: normal; word-wrap: break-word; overflow: hidden;">${hdTicketDTO.wugDetailsDTO.wugModemType}&nbsp;</div>
                                <div style="margin-bottom: 5px;width: 95px;"
                                     class="divFloatLeft">Circuit IP:</div>
                                <div
                                    style="margin-bottom: 5px; white-space: normal; word-wrap: break-word; overflow: hidden;">${hdTicketDTO.wugDetailsDTO.wugCircuitIp}&nbsp;</div>
                            </div>
                            <div style="float: left;width: 200px;margin-left: 5px;">

                                <!--                                 <div class="divFloatLeft" style="width: 50%;"> -->
                                <div style="margin-left: 0px; margin-bottom: 10px;"
                                     class="PCSSH0001">Provisioning Data</div>
                                <div style="margin-bottom: 5px;width: 95px;"
                                     class="divFloatLeft">IP Type:</div>
                                <div
                                    style="margin-bottom: 5px; white-space: normal; word-wrap: break-word; overflow: hidden; ">${hdTicketDTO.provisioningDetailDTO.ipType}&nbsp;</div>
                                <div style="margin-bottom: 5px;width: 95px;"
                                     class="divFloatLeft">IP Username:</div>
                                <div
                                    style="margin-bottom: 5px; white-space: normal; word-wrap: break-word; overflow: hidden;">${hdTicketDTO.provisioningDetailDTO.ipUsername}&nbsp;</div>
                                <div style="margin-bottom: 5px;width: 95px;"
                                     class="divFloatLeft">IP Password:</div>
                                <div
                                    style="margin-bottom: 5px; white-space: normal; word-wrap: break-word; overflow: hidden;">${hdTicketDTO.provisioningDetailDTO.ipPassword}&nbsp;</div>
                                <div style="margin-bottom: 5px;width: 95px;"
                                     class="divFloatLeft">Usable IP:</div>
                                <div
                                    style="margin-bottom: 5px; white-space: normal; word-wrap: break-word; overflow: hidden; ">${hdTicketDTO.provisioningDetailDTO.useableIp}&nbsp;</div>
                                <div style="margin-bottom: 5px;width: 95px;"
                                     class="divFloatLeft">Gateway:</div>
                                <div
                                    style="margin-bottom: 5px; white-space: normal; word-wrap: break-word; overflow: hidden;">${hdTicketDTO.provisioningDetailDTO.gateway}&nbsp;</div>
                                <div style="margin-bottom: 5px;width: 95px;"
                                     class="divFloatLeft">Subnet Mask:</div>
                                <div
                                    style="margin-bottom: 5px; white-space: normal; word-wrap: break-word; overflow: hidden;">${hdTicketDTO.provisioningDetailDTO.subnetMask}&nbsp;</div>
                                <div style="margin-bottom: 5px;width: 95px;"
                                     class="divFloatLeft">Primary DNS:</div>
                                <div
                                    style="margin-bottom: 5px; white-space: normal; word-wrap: break-word; overflow: hidden;">${hdTicketDTO.provisioningDetailDTO.primaryDNS}&nbsp;</div>
                                <div style="margin-bottom: 5px;width: 95px;"
                                     class="divFloatLeft">Seconday DNS:</div>
                                <div
                                    style="margin-bottom: 5px; white-space: normal; word-wrap: break-word; overflow: hidden;">${hdTicketDTO.provisioningDetailDTO.secondayDNS}&nbsp;</div>
                                <div style="margin-bottom: 5px;width: 95px;"
                                     class="divFloatLeft">Circuit#:</div>
                                <div
                                    style="margin-bottom: 5px; white-space: normal; word-wrap: break-word; overflow: hidden;">${hdTicketDTO.provisioningDetailDTO.circuit}&nbsp;</div>
                            </div>
                        </div>

                        <!-- 		<div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 15px;height:20px;"> -->
                        <!-- 		<b>WUG Configuration</b> -->
                        <!-- 		</div> -->
                        <!-- 		<div style="margin-top: 35px;"> -->


                        <!-- 			<textarea cols="44" rows="5"></textarea> -->
                        <!-- 		</div> -->


                        <!-- 		<div class="PCCOMMENTS divFloatLeft" style="margin-bottom: 20px;margin-top: 20px;">Note 1: A Phone number or email address is required. -->
                        <!-- 		</div> -->
                        <!-- 		<div style="text-align: right;margin-right: 20px;margin-top: 20px;"> -->
                        <!-- 			<input type="button" value="Next" onclick="createTicketStep3();" class="FMBUT0006" style="width: 100px;"/> -->
                        <!-- 		</div> -->
                    </div>

                    <!-- 	Vertical line -->
                    <div class="divFloatLeft" style="height: 350px;width: 2px; background-color: menu;margin: 10px 25px;">&nbsp;
                    </div>

                    <div class="divFloatLeft" style="width: 61%;margin-bottom: 20px;">
                        <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 12px;height:20px;">
                            Short Description:<font style="color: red;">*</font>
                        </div>
                        <div style="margin-top: 10px;height:60px;">
                            <form:textarea
                                path="shortDescription" rows="3" cols="60"
                                cssStyle="overflow-y: scroll;resize:none;" spellcheck="true"
                                placeholder="Enter Short Description" />
                        </div>
                        <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: -5px;height:20px;">
                            Problem Description:<font style="color: red;">*</font>
                        </div>
                        <div style="margin-top: -5px;height:60px;">
                            <form:textarea
                                path="problemDescription" rows="3" cols="60"
                                cssStyle="overflow-y: scroll;resize:none;" spellcheck="true"
                                placeholder="Enter Problem Description" />
                        </div>

                        <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: -5px;height:20px;">
                            Incident State:<font style="color: red;">*</font>
                        </div>

                        <div style="margin-top: -5px;height:20px;">
                            <form:select path="ticketStatus" class="enabled" id="ticketStatusId"
                                         cssStyle="width: 180px;"  onchange="checkResolvedState(); checkCancelledAndResolved(this);">
                                <option class="enabled" value="select">Select Incident State</option>
                                <c:forEach items="${incidentStates}" var="incidentStates">

                                    <c:if test="${incidentStates.enabled eq 1}">
                                        <option class="enabled"  selected="selected"  value="${incidentStates.value}">${incidentStates.label}</option>
                                    </c:if>

                                </c:forEach>

                                <%-- 					<option selected="selected" <c:if test="${hdTicketDTO.ticketStatus eq 'NEW'}"> selected="selected" </c:if> value="WIP">New</option> --%>
                                <%-- 					<option <c:if test="${hdTicketDTO.ticketStatus eq 'WIP'}"> selected="selected" </c:if> value="WIP">WIP</option> --%>
                                <%-- 					<option <c:if test="${hdTicketDTO.ticketStatus eq 'Dispatch Requested'}"> selected="selected" </c:if> value="DSP-R">Dispatch Requested</option> --%>
                                <%-- 					<option <c:if test="${hdTicketDTO.ticketStatus eq 'Dispatch Scheduled'}"> selected="selected" </c:if> value="DSP-S">Dispatch Scheduled</option> --%>
                                <%-- 					<option <c:if test="${hdTicketDTO.ticketStatus eq 'Dispatch Onsite'}"> selected="selected" </c:if> value="DSP-ON">Dispatch Onsite</option> --%>
                                <%-- 					<option <c:if test="${hdTicketDTO.ticketStatus eq 'Resolved'}"> selected="selected" </c:if> value="RES">Resolved</option> --%>
                                <%-- 					<option <c:if test="${hdTicketDTO.ticketStatus eq 'Awaiting Response/Action'}"> selected="selected" </c:if> value="AR/A">Awaiting Response/Action</option> --%>
                                <%-- 					<option <c:if test="${hdTicketDTO.ticketStatus eq 'Cancelled'}"> selected="selected" </c:if> value="Cancelled">Cancelled</option> --%>
                                <%-- 					<option <c:if test="${hdTicketDTO.ticketStatus eq 'Assigned to Customer'}"> selected="selected" </c:if> value="Customer">Assigned to Customer</option> --%>

                            </form:select>
                        </div>

                        <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;">
                            Assignment Group:<font style="color: red;">*</font>
                        </div>
                        <div style="margin-top: 10px;height:20px;">
                            <form:select path="assignmentGroupId"
                                         cssStyle="width: 180px;" >

                                <option <c:if test="${hdTicketDTO.assignmentGroupId eq '1'}" >selected="selected"</c:if> value="1">Level 1</option>
                                <option <c:if test="${hdTicketDTO.assignmentGroupId eq '2'}" >selected="selected"</c:if> value="2">Level 2</option>
                                <option <c:if test="${hdTicketDTO.assignmentGroupId eq '3'}" >selected="selected"</c:if> value="3">NOC Engineer</option>
                                <option <c:if test="${hdTicketDTO.assignmentGroupId eq '4'}" >selected="selected"</c:if> value="4">MSP Dispatch</option>
                                <option <c:if test="${hdTicketDTO.assignmentGroupId eq '5'}" >selected="selected"</c:if> value="5">PMO</option>
                            </form:select>
                        </div>

                        <!-- 		<div class="divFloatLeft" style="padding-left: 300px;margin-top: 15px;height:20px;"><b>Start</b></div> -->
                        <!-- 		<div  style="padding-left: 455px;margin-top: 15px;height:20px;"><b>Stop</b></div> -->

                        <div class="divFloatLeft" style="width: 150px;margin-top:10px;">Effort:<font style="color: red;">*</font></div>

                        <div class="divFloatLeft" style="margin-right: 5px;margin-top:8px;height: 20px;">

                            <input type="button"  onclick="sendTimerStopDate();" class="TimerStopBtn" id="stopTimerButton" style="width:90px;height:22px;border:none;"/>

                            <input type="button"  onclick="sendTimerRestartDate();" class="TimerStartBtn" id="restartTimerButton" style="width:91px;height:22px;border:none;"/>

                        </div>
                        <div class="PCSTD0010" id="time" style="margin-top:10px;"></div>
                        <div style="display:none;" id="flagVal"><c:out value="${effortFlag}" /></div>

                        <!-- 		<div class="divFloatLeft" style="margin-right: 10px;margin-top:8px;height: 20px;"> -->
                        <!-- 		<input type="radio" onClick="checkEffortRadio(this)" checked="checked" name="effortRadio" value="mins" style="float:left;margin-top: 3px;"/> -->
                        <!-- 					<label for="mins" style="float:left;margin-top: 3px;font-size: 11px;">&nbsp;Time </label> -->

                        <!-- 		<div class="effortTime" style="float:left;margin-left: 20px;margin-top: 1px;" > -->
                        <!-- 		<label style="font-size: 11px;">Mins</label>&nbsp; -->
                        <%-- 			<form:input path="efforts" cssStyle="width:30px;"/> --%>
                        <!-- 		</div> -->

                        <!-- 		</div> -->

                        <!-- 		<div style="clear:both;"></div> -->


                        <!-- 		<div style="margin-top:8px;margin-left:150px;float: left;height: 20px;" > -->
                        <!-- 		<input type="radio" onClick="checkEffortRadio(this)" name="effortRadio" value="date" style="float: left;"/> -->
                        <!-- 					<label for="date" style="vertical-align: top;float: left;">&nbsp;Date </label> -->
                        <!-- 		</div> -->


                        <!-- 		<div class="divFloatLeft EffortDate" style="width: 150px;margin-top:6px;margin-right:30px;margin-left:20px; display:none;height: 20px;" > -->
                        <!-- 		<label>Start</label> -->
                        <!-- 			<input type="text" id="createTicketEffortStart" onchange="calculateCreateTicketEfforts();" style="width: 120px;"> -->
                        <!-- 		</div> -->



                        <!-- 		<div style="margin-top:5px;display:none;height: 20px;" class="EffortDate"> -->
                        <!-- 		<label>Stop</label> -->
                        <!-- 			<input type="text" id="createTicketEffortStop" onchange="calculateCreateTicketEfforts();" style="width: 120px;"> -->
                        <!-- 		</div> -->


                        <!-- 		<div id="effortEdit" style="display:none;"> -->
                        <!-- 		<div class="PCSSH0001" style="margin-top: 20px;">Efforts:</div> -->



                        <!-- 		<div class="divFloatLeft" style="width: 100px;margin-top: 15px;height:20px;text-align: center;margin-right:60px;padding-left:140;"><b>Effort(Mins)</b></div> -->
                        <!-- 		<div class="divFloatLeft" style="width: 100px;margin-top: 15px;height:20px;text-align: center;"><b>Date</b></div> -->
                        <!-- 		<div id="effortBy" style="width: 350px;margin-top: 15px;height:20px;text-align: center;margin-left: 200px;"><b>By</b></div> -->

                        <%-- 		<c:forEach items="${hdTicketDTO.hdEffortDTOList }" var="effort"> --%>
                        <%-- 			<div class="effortEffort divFloatLeft"  style="width: 100px;margin-top: 15px;height:20px;text-align: center;margin-right:60px;padding-left:140;">${effort.effort }</div> --%>
                        <%-- 			<div class="divFloatLeft" style="width: 100px;margin-top: 15px;height:20px;text-align: center;">${effort.effortDate }</div> --%>
                        <!-- 			<div style="margin-top: 15px;height:20px;padding-left:97px;"> -->

                        <%-- 					<div class="effortBy2" style="width: 100px;margin-left: 360px;white-space: normal;">${effort.workedBy }</div> --%>


                        <!-- 			</div>  -->
                        <%-- 		</c:forEach> --%>



                        <!-- 		</div> -->

                        <div style="clear:both;"></div>

                        <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 5px;height:20px;">
                            Next Action:<font style="color: red;">*</font>
                        </div>
                        <div id="nextActionDiv" style="margin-top: 5px;height:20px;">
                            <form:select class="disabled" path="nextAction"
                                         cssStyle="width: 180px;">
                                <option value="">Select Next Action</option>
                                <c:forEach items="${nextActions}" var="nextAction">
                                    <option <c:if test="${hdTicketDTO.nextAction eq nextAction.nextActionId}"> selected="selected" </c:if> value="${nextAction.nextActionId}">${nextAction.nextAction}</option>
                                </c:forEach>

                            </form:select>
                        </div>

                        <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 15px;height:20px;">
                            Next Action Due:<font style="color: red;">*</font>
                        </div>

                        <div class="disabled divFloatLeft" style="margin-right: 10px;margin-top:10px;">
                            <input type="radio" class="checkManual" onClick="checknextActionRadio(this)" checked="checked" name="nextActionRadio" value="mins" style="float: left;height: 20px;" id="nextActionTimeRadio" />
                            <label for="mins" style="vertical-align: top;float: left;margin-top: 3px;height: 20px;">&nbsp;Time </label>

                            <div class="divFloatLeft nextActionTime" style="margin-right: 10px;margin-left:20px;float: left;">
                                <form:input path="createTicketNextActionTime" cssStyle="width:30px;margin-top: 3px;"/>
                            </div>
                            <div class="divFloatLeft nextActionTime" style="width: 100px;margin-top: 3px;float: left;">
                                <c:choose>
                                    <c:when test="${hdTicketDTO.createTicketNextActionUnit eq 'Hrs'}">
                                        <form:radiobutton  checked="checked" path="createTicketNextActionUnit" value="Hrs"/>
                                        <label for="hrs" style="vertical-align: top;">Hrs </label>
                                    </c:when>

                                    <c:otherwise>
                                        <form:radiobutton path="createTicketNextActionUnit" value="Hrs"/>
                                        <label for="hrs" style="vertical-align: top;">Hrs </label>
                                    </c:otherwise>
                                </c:choose>

                                <c:choose>
                                    <c:when test="${hdTicketDTO.createTicketNextActionUnit eq 'Mins'}">
                                        <form:radiobutton  checked="checked" path="createTicketNextActionUnit" value="Mins"/>
                                        <label for="Mins" style="vertical-align: top;">Mins </label>
                                    </c:when>

                                    <c:otherwise>
                                        <form:radiobutton class="checkManual" path="createTicketNextActionUnit" value="Mins"/>
                                        <label for="Mins" style="vertical-align: top;">Mins </label>
                                    </c:otherwise>
                                </c:choose>

                            </div>

                        </div>

                        <div style="clear:both;"></div>

                        <input class="disabled" type="radio" onClick="checknextActionRadio(this)" name="nextActionRadio" value="date"
                               style="float: left;margin-left: 150px;margin-top: 3px;" id="nextActionDateRadio"/>
                        <label for="date" style="vertical-align: top;float: left;height: 20px;margin-top: 3px;">&nbsp;Date </label>

                        <!-- 		<div class="divFloatLeft" style="margin-right: 30px;margin-top: 5px;"> -->
                        <!-- 			<b>or</b> -->
                        <!-- 		</div> -->

                        <div style="margin-left:20px;float: left;" class="nextActionDate"><form:input class="disabled" path="createTicketNextActionDate" cssStyle="width: 120px;"/></div>
                        <div>&nbsp;</div>


                        <%-- <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 15px;height:20px;">
                        Next Action Due:
                        </div>
                        <div style="margin-top: 15px;height:20px;">
                                <form:input path="createTicketNextActionTime"
                                                cssStyle="width:178px;margin-top: 5px;" />
                        </div> --%>

                        <div id="workNotesInsert" style="margin-top:15px;">
                            <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;height:20px;">
                                Work Notes:<font style="color: red;">*</font>
                            </div>
                            <div style="margin-top: 15px;height:90px;">
                                <form:textarea path="workNotes" rows="4" class="enabled" cols="60" cssStyle="width: %;overflow-y: scroll;resize:none;"  spellcheck="true"/>
                            </div>

                        </div>

                        <div id="workNotesEdit" style="display:none;">

                            <div id="tab-container" class="tab-container" style="width:670px;height:200px;">
                                <ul class="etabs" style="height: 30px;">
                                    <li class="tab"><a href="#tabs-1">Work Notes</a></li>
                                    <li class="tab"><a href="#tabs-2">History</a></li>
                                    <li class="tab"><a href="#tabs-3" >Efforts</a></li>
                                    <li class="tab"><a href="#tabs-4" >State Trail</a></li>
                                    <!--  Add Install Notes in help desk Module -->
									<li class="tab"><a href="#tabs-5">Install Notes</a></li>



                                </ul>
                                <div id="tabs-1" style="height: 147px;overflow-x:auto;border: 2px solid lemonchiffon;">

                                    <div id="hdPendingAckAlertDiv_Tab1" class="tableWrapper" align="left" style="display:block;width:638px; margin-top:6px; margin-left:4px;outline: none;">





                                        <div id="hdPendingAckAlertTable_wrapper" class="dataTables_wrapper">

                                            <table style="width:638px;" class="container display dataTable nonEditableTable" cellpadding="0" cellspacing="1" border="0" id="workNotesTable">
                                                <colgroup>
                                                    <col width="90px">
                                                    <col width="130px">
                                                    <col width="205px">

                                                </colgroup>
                                                <thead>
                                                    <tr role="row">
                                                        <th class="textCenter">
                                                            Date
                                                        </th>
                                                        <th class="siteNumberTextPendingCubes">
                                                            Author
                                                        </th>
                                                        <th  role="columnheader">
                                                            Note
                                                        </th>
                                                </thead>


                                                <tbody >
                                                    <c:forEach items="${hdTicketDTO.hdWorkNoteDTOList}" var="workNotes">

                                                        <tr class="odd">
                                                            <td>${workNotes.workNoteDateStr}</td>
                                                            <td>${workNotes.userLastName}</td>
                                                            <td style="word-break: keep-all;">${workNotes.workNote}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>


                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <div id="tabs-2" style="height: 147px; overflow-x:auto;overflow-y:auto;border: 2px solid lemonchiffon;">

                                    <div id="hdPendingAckAlertDiv_Tab2" class="tableWrapper" align="left" style="display:block;width:1058px; margin-top:6px; margin-left:4px;outline: none;">

                                        <div id="hdPendingAckAlertTable_wrapper" class="dataTables_wrapper">

                                            <table style="width:635px;" class="container display dataTable nonEditableTable" cellpadding="0" cellspacing="1" border="0" id="workNotesTable1">
                                                <colgroup>
                                                    <col width="80px">
                                                    <col width="145px">
                                                    <col width="135px">
                                                    <col width="135px">
                                                    <col width="110px">
                                                    <col width="135px">
                                                    <col width="140px">
                                                    <col width="120px">
                                                    <col width="140px">

                                                </colgroup>
                                                <thead>
                                                    <tr role="row">
                                                        <th class="textCenter">
                                                            Ticket#
                                                        </th>
                                                        <th class="siteNumberTextPendingCubes">
                                                            Date
                                                        </th>
                                                        <th>
                                                            Incident State
                                                        </th>
                                                        <th class="textCenter">
                                                            Short Description
                                                        </th>
                                                        <th class="siteNumberTextPendingCubes">
                                                            Next Action
                                                        </th>
                                                        <th>
                                                            Back up Cct State
                                                        </th>
                                                        <th class="textCenter">
                                                            Point of Failure
                                                        </th>
                                                        <th class="siteNumberTextPendingCubes">
                                                            Resolution
                                                        </th>
                                                        <th>
                                                            Root Cause
                                                        </th>
                                                    </tr>
                                                </thead>


                                                <tbody >
                                                    <c:forEach items="${hdTicketDTO.hdAuditTrailList}" var="auditTrail">
                                                        <tr class="odd">
                                                            <td nowrap>${auditTrail.ticketId}</td>
                                                            <td>${auditTrail.changeDateString}</td>
                                                            <td>${auditTrail.incidentStateDesc}</td>
                                                            <td style="word-break: keep-all;">${auditTrail.shortDesc}</td>
                                                            <td>${auditTrail.nextActionDesc}</td>
                                                            <td>${auditTrail.backUpCctStateDesc}</td>
                                                            <td>${auditTrail.pointOfFailureDesc}</td>
                                                            <td style="word-break: keep-all;">${auditTrail.resolutionDesc}</td>
                                                            <td style="word-break: keep-all;">${auditTrail.rootCauseDesc}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div id="tabs-3" style="height: 147px;overflow-x:auto;border: 2px solid lemonchiffon;">

                                    <div id="hdPendingAckAlertDiv_Tab1" class="tableWrapper" align="left" style="display:block;width:638px; margin-top:6px; margin-left:4px;outline: none;">




                                        <div id="hdPendingAckAlertTable_wrapper" class="dataTables_wrapper">

                                            <table style="width:638px;" class="container display dataTable nonEditableTable" cellpadding="0" cellspacing="1" border="0" id="effortsTable">
                                                <colgroup>
                                                    <col width="90px">
                                                    <col width="140px">
                                                    <col width="140px">
                                                    <col width="205px">

                                                </colgroup>
                                                <thead>
                                                    <tr role="row">
                                                        <th class="textCenter">
                                                            Effort (Min)
                                                        </th>
                                                        <th class="siteNumberTextPendingCubes">
                                                            Start Date
                                                        </th>
                                                        <th class="siteNumberTextPendingCubes">
                                                            End Date
                                                        </th>
                                                        <th  role="columnheader">
                                                            by
                                                        </th>
                                                </thead>


                                                <tbody>
                                                    <c:forEach items="${hdTicketDTO.hdEffortDTOList}" var="effort">

                                                        <tr class="odd">
                                                            <td class="effortMins">${effort.effort}</td>
                                                            <td class="effortStartDate">${effort.effortDate}</td>
                                                            <td class="effortEndDate">${effort.effortEndDate}</td>
                                                            <td style="word-break: keep-all;">${effort.workedBy }</td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>




                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div id="tabs-4" style="height: 147px;overflow-x:auto;border: 2px solid lemonchiffon;">

                                    <div id="hdPendingAckAlertDiv_Tab1" class="tableWrapper" align="left" style="display:block;width:638px; margin-top:6px; margin-left:4px;outline: none;">





                                        <div id="hdPendingAckAlertTable_wrapper" class="dataTables_wrapper">

                                            <table style="width:638px;" class="container display dataTable nonEditableTable" cellpadding="0" cellspacing="1" border="0" id="stateTailTable">
                                                <colgroup>
                                                    <col width="90px">
                                                    <col width="130px">
                                                    <col width="205px">

                                                </colgroup>
                                                <thead>
                                                    <tr role="row">
                                                        <th class="textCenter">
                                                            Date
                                                        </th>
                                                        <th class="siteNumberTextPendingCubes">
                                                            Primary
                                                        </th>
                                                        <th class="siteNumberTextPendingCubes">
                                                            Secondary
                                                        </th>

                                                </thead>


                                                <tbody>
                                                    <c:forEach items="${stateTailDTO.wugDetailsDTOList}" var="stateTail">

                                                        <tr class="odd">

                                                            <td>${stateTail.wugDate}</td>
                                                            <td>${stateTail.wugState}</td>
                                                            <td style="word-break: keep-all;">${stateTail.backUpStatus}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>




                                            </table>
                                        </div>
                                    </div>

                                </div>
                                
                         	<!--  install notes div  -->
						<div id="tabs-5" style="height: 147px;overflow-x:auto;border: 2px solid lemonchiffon;">

                                    <div id="hdPendingAckAlertDiv_Tab5" class="tableWrapper" align="left" style="display:block;width:638px; margin-top:6px; margin-left:4px;outline: none;">





                                        <div id="hdPendingAckAlertTable_wrapper" class="dataTables_wrapper">

                                            <table style="width:638px;" class="container display dataTable nonEditableTable" cellpadding="0" cellspacing="1" border="0" id="NotesTable">
                                                <colgroup>
                                                    <col width="30px">
                                                    <col width="130px">
                                                   

                                                </colgroup>
                                                <thead>
                                                    <tr role="row">
                                                        <th class="textCenter">
                                                            Date
                                                        </th>
                                                       <th  role="columnheader">
                                                            Note
                                                       </th>
                                                </thead>


                                                <tbody >
                                                    <c:forEach items="${hdTicketDTO.hdInstallNoteDTOList}" var="InstallNotes">

                                                        <tr class="odd">
                                                            <td>${InstallNotes.installNoteDateStr}</td>
                                                            <td style="word-break: keep-all;">${InstallNotes.installNote}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>


                                            </table>
                                        </div>
                                    </div>

                                </div>
						
						
						<!--  End of  Div -->
	       
                                
                                


                            </div>

                        </div>


                        <%--
                        <div class="PCSTD0010 divFloatLeft" style="width: 110px;margin-top: 15px;height:20px;">
                        Timer
                        </div>
                        <div style="margin-top: 15px;height:20px;">
                                <form:select path="siteDetailDTO.siteId"
                                                cssStyle="width: 180px;" onchange="fillPOCDetails(this);">
                                                <option value="">Ticket Timer Function</option>
                                        </form:select>

                                        <input type="button" value="Stop Timer"
                                        class="FMBUT0006" style="width: 120px;" />
                        </div>

                        <div class="PCSTD0010 divFloatLeft" style="width: 110px;margin-top: 15px;height:20px;">
                        History
                        </div>
                        <div style="margin-top: 15px;height:20px;">
                                <table style="width: 77%;">
                                                <tr style="background-color: gainsboro;">
                                                        <td>Action</td>
                                                        <td>Date</td>
                                                        <td>By</td>
                                                </tr>
                                                <tr>
                                                        <td>Action1</td>
                                                        <td>20-Aug-2013</td>
                                                        <td>Craig</td>
                                                </tr>
                                                <tr>
                                                        <td>Action2</td>
                                                        <td>20-Aug-2013</td>
                                                        <td>Craig</td>
                                                </tr>
                                                <tr>
                                                        <td>Action3</td>
                                                        <td>20-Aug-2013</td>
                                                        <td>Craig</td>
                                                </tr>
                                        </table>
                        </div> --%>

                        <div style="text-align: right;margin-right: 70px;margin-top: 0px;">
                            <input type="button" value="Create Ticket" onclick="createHDTicket();" class="enabled FMBUT0006" id="createTicketButton" style="width: 120px;"/>
                            <input  type="button" value="ReOpen Ticket" onclick="updateHDTicketonReOpen();" class="enabled FMBUT0006" id="updateTicketButton1"  style="display:none;width: 120px;"/>
                            <input type="button" value="Cancel" class="enabled FMBUT0006" style="width: 120px;" onclick="window.close();
                                    " />

                        </div>

                    </div>


                </div>
            </div>





            <div id="dialog" title="" class="PCSTD0010" style="display:none;">
                <p></p>
            </div>
            <div id="test" style="display: none; cursor: default; outline: 2px solid #d7c960; border: 2px solid #e0e1e3">

                <div style="border: 2px;">
                    <table cellspacing="0" cellpadding="0" class="headerpaleyellow"
                           width="100%" style="background-color: #d7c960">
                        <tr>
                            <td align="left" id="ticketPageTitle" height="20px"
                                style="padding-left: 5px;">Add Site</td>
                            <td align="right" style="padding-right: 5px;">
                                <img
                                    height="15" width="15" alt="Close"
                                    src="${pageContext.request.contextPath}/resources/images/delete.gif"
                                    id="close" onclick="closeWindow();"></td>
                        </tr>
                    </table>
                </div>
                <div id="testBody" style="padding-top: 4px; overflow: visible; overflow-x: auto; overflow-y: auto;">
                    <img
                        src="${pageContext.request.contextPath}/resources/images/waiting_tree.gif" />

                </div>
            </div>
            <input type="hidden" id="ticketIdFake" value="${hdTicketDTO.ticketId}" />

        </form:form>

    </body>
</html>