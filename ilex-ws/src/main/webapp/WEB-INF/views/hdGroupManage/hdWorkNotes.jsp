<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<div class="WASTD0003 PCSTD0010" style="text-align: left;">
			<form id="workNotesSave">
			<input type="hidden" id="ticketId" name="ticketId" value="${ticketId}">
			    <div  style="float: left;width: 90px;">Work Notes<font style="color: red;">*</font>:</div>
			    
		       	<div>
		       		<textarea rows="5" cols="60" style="width: 300px;overflow-y: scroll;resize:none;" id="workNotesTicketSummary" spellcheck="true" ></textarea>
		       	</div>
		       	 
		       	<div style="margin-bottom: 5px;margin-top: 5px;">
		       	 	<div  style="float: left;width: 90px;">&nbsp;</div>
		       		<div style="float: left;"><input type="button" value="Spell Check" onclick="javascript:runImpspellerTextarea('#workNotesTicketSummary');" class="FMBUT0003"/></div>
		       		<div style="margin-left: 20px;">&nbsp;&nbsp;<input type="checkbox" id="overrideSpellCheckTicketSummary"/>&nbsp;Override Spell Check</div>
	       		</div>
	       		<div style="text-align: right;">
	       		 	<input type="button" value="Submit" onclick="validateTicketSummaryWorkNotes();"  class="FMBUT0003" style="margin-right:40px;"/>
	       		</div>
       		</form>
       		
</div>
