<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<script>

$(document).ready(function(){
	
	
});
</script>

<div class="WASTD0003 PCSTD0010" style="text-align: left;">
<form:form modelAttribute="hdTicketDTO" id="hdTicketDTOEdit" method="post">
<form:hidden path="ticketId"/>
	<div class="divFloatLeft" style="width: 48%;margin-bottom: 20px;">
		<div class="PCSSH0001 divFloatLeft">Ticket Details</div>
		<div  style="margin-top: 15px;">Ticket Contact</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 15px;">Name:</div>
		<div style="margin-top: 15px;"><form:input path="callerName" /></div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 15px;">Phone Number:</div>
		<div style="margin-top: 15px;"><form:input path="callerPhone" /></div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 15px;">Email Address:</div>
		<div style="margin-top: 15px;"><form:input path="callerEmail" /></div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 15px;">Customer Ticket #:</div>
		<div style="margin-top: 15px;"><form:input path="customerTicketReference" /></div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 15px;">Billable:</div>
		<div style="margin-top: 15px;">
			<form:radiobutton path="billingStatus" value="true"/>&nbsp;Yes&nbsp;
			<form:radiobutton path="billingStatus" value="false"/>&nbsp;No&nbsp;
		</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 15px;">Category<font style="color: red;">*</font>:</div>
		<div style="margin-top: 15px;">
			<form:select path="category" cssStyle="width: 250px;">
				<option value="">Select</option>
					<c:forEach items="${categories}" var="category">
						<option value="${category.categoryId}" <c:if test="${category.categoryId==hdTicketDTO.category}">selected="selected"</c:if>>${category.category}</option>
					</c:forEach>
			</form:select>
		</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 15px;">Configuration Item<font style="color: red;">*</font>:</div>
		<div style="margin-top: 15px;">
			<form:select path="configurationItem" cssStyle="width: 250px;">
			<option value="">Select</option>
				<c:forEach items="${configurationItems}" var="configurationItem">
						<option value="${configurationItem.configurationItemId}" <c:if test="${configurationItem.configurationItemId==hdTicketDTO.configurationItem}">selected="selected"</c:if>>${configurationItem.configurationItem}</option>
				</c:forEach>
			</form:select>
		</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 15px;">Short Description<font style="color: red;">*</font>:</div>
		<div style="margin-top: 15px;"><form:textarea path="shortDescription" cols="30" rows="4"></form:textarea></div>
	
		<div class="divFloatLeft" style="width: 150px;margin-top: 15px;">Problem Description<font style="color: red;">*</font>:</div>
		<div style="margin-top: 15px;"><form:textarea path="problemDescription" cols="30" rows="6"></form:textarea></div>
		
		<div style="margin-top: 15px;text-align: right;margin-right: 10px;">
			<input type="button" value="Update" class="FMBUT0003" onclick="editTicketDetails();">
		</div>
	</div>
</form:form>
</div>