<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript">



	
</script>

<div class="WASTD0003 PCSTD0010" style="text-align: left;">
<div style="display: none;">
<input type="hidden" id="ticketId" name="ticketId" value="${ticketId}">
</div>
<div class="PCSTD0010 divFloatLeft" style="width: 100px;margin-top: 10px;">
	Work Note:<font color="red">*</font>
</div>
<div>
	<textarea rows="3" cols="50" style="overflow-y: scroll;resize:none;margin-top: 10px;" id="cancelWorkNotes" spellcheck="true" ></textarea>
</div>
<div style="margin-bottom: 5px;margin-top: 20px;margin-left: 100px;">
	     <div style="float: left;"><input type="button" value="Spell Check" onclick="javascript:runImpspellerTextarea('#cancelWorkNotes');" class="FMBUT0003"/></div>
	     <div style="float: left;margin-left: 20px;">&nbsp;&nbsp;<input type="checkbox" style="margin-top: 5px;vertical-align: bottom;" id="overrideSpellCheck"/>&nbsp;Override Spell Check</div>
	     <div style="text-align: right;margin-right: 60px;"><input type="button" value="Update" onclick="cancelTicket(${ticketId});" class="FMBUT0003" /></div>
 </div>
 <div style="padding-bottom: 12px;margin-top: 25px;">
	     <div class="PCCOMMENTS" style="float: left;">&nbsp;&nbsp;(Client visible notes. A work note is required to close this ticket.)</div>
</div>


</div>
