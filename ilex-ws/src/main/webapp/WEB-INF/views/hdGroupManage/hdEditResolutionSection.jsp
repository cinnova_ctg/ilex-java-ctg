<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript">

$(document).ready(function(){
	
	});
</script>

<form:form commandName="hdTicketDTO" id="hdTicketDTOEdit">
<form:hidden path="ticketId"/>
<div class="WASTD0003 PCSTD0010" style="text-align: left;">

	<div class="divFloatLeft" style="width: 120px;">
	Point Of Failure<font style="color: red;">*</font>:</div>
	<div style="margin-top: 10px;width: 100px;">
		<form:select path="pointOfFailure">
			<option value="">Select</option>
			<c:forEach items="${pointOfFailureList}" var="failureList">
				<option value="${failureList.key }" <c:if test="${failureList.key==hdTicketDTO.pointOfFailure}">selected="selected"</c:if>>${failureList.value }</option>
			</c:forEach>
		</form:select>
	</div>
	<div class="divFloatLeft" style="width: 120px;margin-top: 10px;">
	Resolution<font style="color: red;">*</font>:</div>
	<div style="margin-top: 10px;width: 100px;">
		<form:select path="resolution">
			<option value="">Select</option>
			<c:forEach items="${resolutionList}" var="resolutionList">
				<option value="${resolutionList.key }" <c:if test="${resolutionList.key == hdTicketDTO.resolution}">selected="selected"</c:if>>${resolutionList.value }</option>
			</c:forEach>
		</form:select>
	</div>
	<div class="divFloatLeft" style="width: 120px;margin-top: 10px;">
	Root Cause<font style="color: red;">*</font>:</div>
	<div style="margin-top: 10px;width: 100px;">
		<form:select path="rootCause">
			<option value="">Select</option>
			<c:forEach items="${rootCauseList }" var="rootCauseList">
				<option value="${rootCauseList.key }" <c:if test="${rootCauseList.key ==hdTicketDTO.rootCause}">selected="selected"</c:if>>${rootCauseList.value }</option>
			</c:forEach>
		</form:select>
	</div>
	<div class="PCSTD0010 divFloatLeft" style="width: 120px;margin-top: 10px;">
		Work Notes<font style="color: red;">*</font>:
	</div>
	<div style="margin-top: 10px;">
		<form:textarea path="workNotes"  rows="5" cols="60" cssStyle="width: 70%;overflow-y: scroll;resize:none;"  spellcheck="true"/>
	</div>
	<div style="margin-top: 15px;text-align: right;margin-right: 10px;">
			<input type="button" value="Update" class="FMBUT0003" onclick="editResolution();">
		</div>

</div>
</form:form>