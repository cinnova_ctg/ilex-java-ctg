<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script>

$(document).ready(function(){
	
	$('#wipNextActionDueDateTicketSummary').datetimepicker({
		dateFormat :"mm-dd-yy",
		showOn : "both",
		buttonImage : pageContextPath
			+ '/resources/images/calendar.gif',
			    beforeShow: function(input, instance) {
			    	if($('#wipNextActionDueDateTicketSummary').val()=="")
			    	{
			    	var currentDate=getCurrentDate();
			        $('#wipNextActionDueDateTicketSummary').datetimepicker('setDate', currentDate);
			    	}
			    }
		
	}).next(".ui-datepicker-trigger").addClass(
			"calImageStyle");
});
</script>



<div class="WASTD0003 PCSTD0010" style="text-align: left;">
		<form id="ticketNextActionSaveForm">
			<input type="hidden" id="ticketId" name="ticketId" value="${ticketId}">
			<input type="hidden" id="currentNextActionTicketSummary" name="currentNextActionTicketSummary" value="${currentNextActionId}">
	       			<div class="divFloatLeft" style="width: 90px;margin-bottom: 10px;">Update:</div>
	       			<div  style="margin-bottom: 10px;">
	       				<select id="ticketNextActionTicketSummary" style="width: 300px;">
	       					<option value="0">Extend Current Next Action</option>
		       				<c:forEach items="${nextActions}" var="nextAction">
								<option 
								<c:if test="${nextAction.nextActionId==currentNextActionId}">selected="selected"</c:if>
								value="${nextAction.nextActionId}" >
								${nextAction.nextAction}</option>
							</c:forEach>
	       				</select>
	       			</div>
	       			<div style="margin-bottom: 10px;">
		       			<div class="divFloatLeft" style="width: 90px;margin-bottom: 10px;">Due:</div>
		       			<div style="float: left;"><input type="text" id="nextActionTimeTicketSummary" style="width: 30px;"/></div>
		       			<div style="float: left;padding-left: 10px;margin-top: 4px;">
		       				<input type="radio" name="nextActionTimeTicketSummary" value="Hrs" ><label for="hrs" style="vertical-align: top;">Hrs</label>&nbsp;
		       				<input type="radio" name="nextActionTimeTicketSummary" value="Mins" ><label for="hrs" style="vertical-align: top;">Mins</label>&nbsp;&nbsp;
		       			</div>
		       			<div class="divFloatLeft" style="margin-top: 3px;">
		       				<label for="or" ><b>or</b></label>&nbsp;&nbsp;
		       			</div>
		       			<div>
		       				<input id="wipNextActionDueDateTicketSummary" type="text" style="width: 120px;"/>
		       			</div>
	       			</div>
	       			
	       			<div  style="float: left;width: 90px;margin-top: 10px;margin-bottom: 10px;">Work Notes<font style="color: red;">*</font>:</div>
		    
		       	<div style="margin-top: 10px;margin-bottom: 10px;">
		       		<textarea rows="5" cols="60" style="width: 300px;overflow-y: scroll;resize:none;" id="workNotesTicketSummary" spellcheck="true" ></textarea>
		       	</div>
	       	 		       	
       		<div style="text-align: right;">
       		 	<input type="button" value="Submit" onclick="updateNextActionFromTicketSummary();"  class="FMBUT0003" style="margin-right:40px;"/>
       		</div>
       	</form>
       		
</div>
