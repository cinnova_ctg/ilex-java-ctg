<%@ page language="java" contentType="text/html;"%>
<!DOCTYPE html >
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>

        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>

        <script>

            function releaseLock(ticketId) {


                $.ajax({
                    type: 'POST',
                    url: "${pageContext.request.contextPath}/hdTicketMonitoring/releaseLock.html",
                    data: {"ticketId": ticketId},
                    success: function(data) {
                        if ("success" == data) {
                            var url = '${pageContext.request.contextPath}/hdTicketMonitoring/getAcquiredTickets.html';
                            location.href = url;
                        }
                    }
                });


            }
        </script>
        <script>

            function unlockManual(ticketId) {


                $.ajax({
                    type: 'POST',
                    url: "${pageContext.request.contextPath}/hdTicketMonitoring/unlockManual.html",
                    data: {"ticketId": ticketId},
                    success: function(data) {
                        if ("success" == data) {
                            var url = '${pageContext.request.contextPath}/hdTicketMonitoring/getAcquiredTickets.html';
                            location.href = url;
                        }
                    }
                });


            }
        </script>
        <link href="${pageContext.request.contextPath}/resources/css/style_common.css" rel="stylesheet" type="text/css"></link>

    </head>
    <body class="PCSTD0007 WASTD0003">
        <div style="margin-bottom: 10px;"><b>Help -Desk Acquired Tickets</b></div>

        <table id="autoEmailConversations" class="TBSTD0005">
            <thead>
                <tr>
                    <th>Ticket Number</th>
                    <th>Session Id</th>
                    <th>User ID</th>
                    <th>User Name</th>
                    <th>Host Name</th>
                    <th>Active/<br/>
                        InActive</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="tickets" items="${acquiredTickets}">
                <c:if test="${tickets.ticketStatus != 1}">
                    <tr>
                        <td>${tickets.ticketNumber}</td>
                        <td>${tickets.sessionId}</td>
                        <td>${tickets.userId}</td>
                        <td>${tickets.userName}</td>
                        <td>${tickets.hostName}</td>
                        <td style="text-align: center;">${tickets.status}</td>
                        <td><a href="#" onclick="releaseLock('${tickets.ticketId}')">Release Lock</a></td>
                    </tr>
                </c:if>
            </c:forEach>
        </tbody>

    </table>



    <div style="margin-bottom: 10px;"><b>Help - Desk Pending Acknowledgment Acquired Tickets</b></div>
    <table  class="TBSTD0005">
        <thead>
            <tr>
                <th>Ticket Number</th>
                <th>Session Id</th>
                <th>User ID</th>
                <th>User Name</th>
                <th>Host Name</th>
                <th>Active/<br/>
                    InActive</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        <c:forEach var="tickets" items="${acquiredTickets}">
            <c:if test="${tickets.ticketStatus == 1}">
                <tr>
                    <td>${tickets.ticketNumber}</td>
                    <td>${tickets.sessionId}</td>
                    <td>${tickets.userId}</td>
                    <td>${tickets.userName}</td>
                    <td>${tickets.hostName}</td>
                    <td style="text-align: center;">${tickets.status}</td>
                    <td><a href="#" onclick="releaseLock('${tickets.ticketId}')">Release Lock</a></td>
                </tr>
            </c:if>
        </c:forEach>
    </tbody>
</table>

<div><b>Input ticket IDs to unlock. Separate with commas.</b></div>
<input type="text" name="IdList" id = "IdList">
<div><b>Unlock Tickets</b></div>
<input type="button" value="Unlock Tickets" onclick="unlockManual(document.getElementById('IdList').value);">

</body>

</html>