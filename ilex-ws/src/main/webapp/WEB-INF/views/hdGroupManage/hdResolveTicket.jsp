<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<script>

$(document).ready(function(){
	
	$('#createTicketEffortStart').datetimepicker({
		dateFormat :"mm-dd-yy",
		showOn : "both",
		buttonImage : pageContextPath
			+ '/resources/images/calendar.gif'
		
	}).next(".ui-datepicker-trigger").addClass(
			"calImageStyle");
	
	$('#createTicketEffortStop').datetimepicker({
		dateFormat :"mm-dd-yy",
		showOn : "both",
		buttonImage : pageContextPath
			+ '/resources/images/calendar.gif'
		
	}).next(".ui-datepicker-trigger").addClass(
			"calImageStyle");
});
</script>

<div class="WASTD0003 PCSTD0010" style="text-align: left;">
	<form:form commandName="hdTicketDTO">
	<form:hidden path="ticketId"/>
	<form:hidden path="ticketSource"/>
			<div class="PCSTD0010 divFloatLeft" style="width: 150px;">
			<fmt:message key="label.customerappendix"/>:
			</div>
			<div>
				${hdTicketDTO.customerName}&nbsp;&nbsp;${hdTicketDTO.appendixName}
			</div>
			<div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 5px;">
			<fmt:message key="label.ticket.type"/>:
			</div>
			<div style="margin-top: 5px;">
				${hdTicketDTO.ticketType}
			</div>
			<c:choose>
			<c:when test="${hdTicketDTO.error eq true}">
			<br>
			<b>${hdTicketDTO.errorMessage}</b>
			</c:when>
			<c:otherwise>
						<div class="PCSSH0001 " style="margin-top: 20px;"><fmt:message key="label.resolution"/>:</div>
			<div style="width: 150px;margin-top: 10px;"><fmt:message key="label.firstcall"/></div>
			<div class="divFloatLeft" style="width: 150px;"><fmt:message key="label.resolution"/><font style="color: red;">*</font>:</div>
			<div >
				<form:radiobutton path="lmCompleteFirstCall" value="true"/>&nbsp;<span style="vertical-align: top;">Yes</span>&nbsp;
				<form:radiobutton path="lmCompleteFirstCall" value="false"/>&nbsp;<span style="vertical-align: top;">No</span>&nbsp;
			</div>
			<div class="divFloatLeft" style="width: 150px;margin-top: 10px;"><fmt:message key="label.pof"/><font style="color: red;">*</font>:</div>
			<div style="width: 150px;margin-top: 10px;">
				<form:select path="pointOfFailureId">
					<form:option value="" label="Select"/>
					<c:forEach items="${pointOfFailureList}" var="failureList">
						<form:option value="${failureList.key }" label="${failureList.value }"/>
					</c:forEach>
				</form:select>
			</div>
			<div class="divFloatLeft" style="width: 150px;margin-top: 10px;"><fmt:message key="label.resolution"/><font style="color: red;">*</font>:</div>
			<div style="width: 150px;margin-top: 10px;">
				<form:select path="resolutionId">
					<form:option value="" label="Select"/>
					<c:forEach items="${resolutionList}" var="resolutionList">
						<form:option value="${resolutionList.key }" label="${resolutionList.value }"/>
					</c:forEach>
				</form:select>
			</div>
			<div class="divFloatLeft" style="width: 150px;margin-top: 10px;"><fmt:message key="label.rc"/><font style="color: red;">*</font>:</div>
			<div style="width: 150px;margin-top: 10px;">
				<form:select path="rootCauseId">
					<form:option value="" label="Select"/>
					<c:forEach items="${rootCauseList }" var="rootCauseList">
						<form:option value="${rootCauseList.key }" label="${rootCauseList.value }"/>
					</c:forEach>
				</form:select>
			</div>
			
			<div class="PCSSH0001 divFloatLeft" style="margin-top: 20px;"><fmt:message key="label.effort"/>:</div>
			<!-- <div class="divFloatLeft" style="margin-top: 20px; margin-left: 250px;"><b>Start</b></div>
			<div style="margin-top: 20px;margin-left: 440px;"><b>Stop</b></div>
			<div class="divFloatLeft" style="margin-right: 10px;margin-left:100px; margin-top: 5px; vertical-align: top;">
				<input type="text" style="width:30px;"/> Mins
			</div>
			<div class="divFloatLeft" style="margin-right: 20px;margin-top: 5px;">
				<b>or</b>
			</div>
			<div class="divFloatLeft" style="width: 150px;margin-top: 5px;" >
				<input type="text" id="createTicketEffortStart" style="width: 120px;">
			</div>
			<div style="width: 150px;margin-top: 5px;">
				<input type="text" id="createTicketEffortStop" style="width: 120px;">
			</div> -->
			
			<div  style="margin-top: 20px;margin-left: 130px;">
				<div class="divFloatLeft" style="text-align: center;width: 100px;"><b><fmt:message key="label.date"/></b></div>
				<div class="divFloatLeft" style="text-align: center;width: 100px;"><b><fmt:message key="label.workedby"/></b></div>
				<div class="divFloatLeft" style="text-align: center;width: 150px;"><b><fmt:message key="label.effort"/>(Mins)</b></div>
				<div style="text-align: center;width: 100px;"><b>Billable</b></div>
			</div>
			<c:forEach items="${hdTicketDTO.hdEffortDTOList}" var="hdTicketDTO">
				<div  style="margin-top: 10px;margin-left: 130px;">
					<div class="divFloatLeft" style="text-align: center;width: 100px;">${hdTicketDTO.effortDate}</div>
					<div class="divFloatLeft" style="text-align: center;width: 100px;">${hdTicketDTO.workedBy}</div>
					<div class="divFloatLeft" style="text-align: center;width: 150px;">${hdTicketDTO.effort}</div>
					<div  style="text-align: center;width: 100px;">
					<c:choose>
					<c:when test="${hdTicketDTO.billable eq true}">Yes</c:when>
					<c:when test="${hdTicketDTO.billable eq false}">No</c:when>
					</c:choose>
					&nbsp;
					</div>
				</div>
			</c:forEach>
			<div  style="margin-top: 20px;margin-left: 140px;">
				<div class="divFloatLeft" style="width: 50px;"><fmt:message key="label.billable"/>:</div>
				<div class="divFloatLeft" style="width: 100px;">${hdTicketDTO.hdBillableDTO.billable}(Mins)</div>
				<div class="divFloatLeft" style="width: 80px;"><fmt:message key="label.non.billable"/>:</div>
				<div style="width: 100px;">${hdTicketDTO.hdBillableDTO.nonBillable}(Mins)</div>
			</div>
			
			<div class="divFloatLeft" style="width: 140px;margin-top: 20px;"> Closing Work<br/>Note: </div>
			<div style="margin-top: 20px;">
				<form:textarea rows="5" cols="50" style="overflow-y: scroll;resize:none;" path="closingWorkNote"></form:textarea>
			</div>
			
			<div style="text-align: right;margin-right: 20px;margin-top: 20px;">
				<input type="button" value="Resolve Ticket" class="FMBUT0003" onclick="updateResolveTicket();"/>
			</div>
			</c:otherwise>
			</c:choose>
	</form:form>
</div>