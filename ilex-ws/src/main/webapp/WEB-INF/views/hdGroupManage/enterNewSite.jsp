<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<script type="text/javascript">


          var pageContextPath = '/Ilex-WS';
</script>
  <link href="/Ilex-WS/resources/css/style_common.css?v=66" type="text/css" rel="stylesheet">
  <link href="/Ilex-WS/resources/css/messi.min.css?v=45" type="text/css" rel="stylesheet">
  <script src="/Ilex-WS/resources/js/jquery/jquery.min.js" type="text/javascript"></script>
	<script src="/Ilex-WS/resources/js/jquery/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
	<script src="/Ilex-WS/resources/js/messi.min.js?v=66" type="text/javascript"></script>	
	<script src="/Ilex-WS/resources/js/hdMainTable.js" type="text/javascript"></script>
  

<div class="WASTD0003" style="text-align: left;">
<form:form commandName="siteDetailDTO" id="siteDetailDTONew">
	<form:hidden path="msaId"/>
		<div class="PCSTD0010 divFloatLeft" style="width: 178px;">
			Site Number:<font style="color: red;">*</font>
		</div>
		<div style="margin-top: 10px;">
			<form:input path="siteNumber" placeholder="Enter Site Number" cssStyle="width: 178px;" maxlength="50"></form:input>
		</div>
		<div class="PCSTD0010 divFloatLeft" style="width: 178px;margin-top: 10px;">
			Address:<font style="color: red;">*</font>
		</div>
		<div style="margin-top: 10px;">
			<form:input  placeholder="Enter Address" cssStyle="width: 178px;" path="siteAddress" maxlength="128"></form:input>
		</div>
		<div class="PCSTD0010 divFloatLeft" style="width: 178px;margin-top: 10px;">
			City:<font style="color: red;">*</font>
		</div>
		<div  style="margin-top: 10px;">
			<form:input placeholder="Enter City" cssStyle="width: 178px;" path="siteCity" maxlength="50"></form:input>
		</div>
		<div class="PCSTD0010 divFloatLeft" style="margin-top: 10px;width: 178px;">
			State:<font style="color: red;">*</font>&nbsp;
		</div>
		<div  style="margin-top: 10px;">
				<form:select path="state" style="width:178px">
					<form:option value="" label="Select State"/>
					<c:forEach items="${stateList}" var="state">
						<form:option value="${state.stateId}" label="${state.stateId}"/>
					</c:forEach>
				</form:select>
		</div>
		<div class="PCSTD0010 divFloatLeft" style="margin-top: 10px;width: 178px;">
			Country:<font style="color: red;">*</font>&nbsp;
		</div>
		<div  style="margin-top: 10px;">
			<form:select path="country" style="width:178px" >
					<form:option value="" label="Select Country"/>
					<c:forEach items="${countryList}" var="country">
						<form:option value="${country.countryId}" label="${country.countryId}"/>
					</c:forEach>
				</form:select>
		</div>
		<div class="PCSTD0010 divFloatLeft" style="margin-top: 10px;width: 178px;">
			Zip Code:&nbsp;
		</div>
		<div style="margin-top: 10px;">
			<form:input type="text" placeholder="Enter Zip Code" cssStyle="width: 178px;" path="siteZipcode" maxlength="50"></form:input>
		</div>
		
		<div class="PCSTD0010 divFloatLeft" style="width: 178px;margin-top: 10px;">
			Site Contact:
		</div>
		<div style="margin-top: 10px;">
			<form:input placeholder="Enter Site Contact" cssStyle="width: 178px;" path="primaryName" maxlength="50"></form:input>
		</div>
		
		<div class="PCSTD0010 divFloatLeft" style="width: 178px;margin-top: 10px;">
			Phone Number:
		</div>
		<div style="margin-top: 10px;">
			<form:input  placeholder="Enter Phone Number" cssStyle="width: 178px;" path="sitePhone" maxlength="50"></form:input>
		</div>
		
		<div class="PCSTD0010 divFloatLeft" style="width: 178px;margin-top: 10px;">
			Email Address:
		</div>
		<div style="margin-top: 10px;">
			<form:input placeholder="Enter Email Address" cssStyle="width:178px;" path="primaryEmail" maxlength="50"></form:input>
		</div>
		
	<div style="margin-left:0207px;margin-top: 20px;">
		<input type="button" value="Save and Update Form" onclick="addNewSite();" class="FMBUT0006" style="width: 150px;"/>
	</div>
</form:form>
</div>
<div id="dialog" title="" class="PCSTD0010" style="display:none;">
  <p></p>
</div>
