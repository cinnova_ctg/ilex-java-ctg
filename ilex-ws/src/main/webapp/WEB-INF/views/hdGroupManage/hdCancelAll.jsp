<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script>
	$(document).ready(function() {	
		$('#timeFrameStart').datetimepicker({
			dateFormat :"mm-dd-yy",
			showOn : "both",
			buttonImage : pageContextPath
				+ '/resources/images/calendar.gif'
			
		}).next(".ui-datepicker-trigger").addClass(
				"calImageStyle");
		$('#timeFrameEnd').datetimepicker({
			dateFormat :"mm-dd-yy",
			showOn : "both",
			buttonImage : pageContextPath
				+ '/resources/images/calendar.gif'
			
		}).next(".ui-datepicker-trigger").addClass(
				"calImageStyle");
		$('#displayOutageListBtn').css('margin-left',$('#customerTable').width()-150 +"px");
		$('#refreshOutageListBtn').css('margin-left',$('#customerTable').width()-150 +"px");
		
	});
</script>

<div class="WASTD0003 PCSTD0010"
	style="text-align: left;padding-left: 20px; padding-top: 20px;padding-right:20px;">
	<form:form commandName="hdCancelAllDTO">
		<div class="divFloatLeft" style="width: 150px;">
			<fmt:message key="label.pof" /><font style="color: red;">*</font>:
		</div>
		<div style="width: 150px;">
			<form:select path="pointOfFailureId">
				<form:option value="" label="Select Point of Failure" />
				<c:forEach items="${pointOfFailureList}" var="failureList">
					<form:option value="${failureList.key }"
						label="${failureList.value }" />
				</c:forEach>
			</form:select>
		</div>
		<div class="divFloatLeft" style="width: 150px; margin-top: 10px;">
			<fmt:message key="label.resolution" /><font style="color: red;">*</font>:
		</div>
		<div style="width: 150px; margin-top: 10px;">
                    <form:select path="resolutionId" cssStyle="width: 180px;" >
                        <option value="0">Select Resolution</option>
                        <c:forEach items="${resolutions}" var="resolutions">
                            <c:if test='${resolutions.status eq "A"}'>
                                <option class="enabled" value="${resolutions.value}">${resolutions.label}</option>
                            </c:if>
                        </c:forEach>
                    </form:select>
		</div>
		<div class="divFloatLeft" style="width: 150px; margin-top: 10px;">
			<fmt:message key="label.rc" /><font style="color: red;">*</font>:
		</div>
		<div style="width: 150px; margin-top: 10px;">
			<form:select path="rootCauseId">
				<form:option value="" label="Select Root Cause" />
				<c:forEach items="${rootCauseList }" var="rootCauseList">
					<form:option value="${rootCauseList.key }"
						label="${rootCauseList.value }" />
				</c:forEach>
			</form:select>
		</div>
		<div class="divFloatLeft" style="width: 150px; margin-top: 10px;">
			<fmt:message key="label.duration" /><font style="color: red;">*</font>:
		</div>
		<div style="width: 150px; margin-top: 10px;">
			<form:input path="duration" cssStyle="width: 30px;text-align:right;"></form:input>&nbsp;<font style="color: gray;font-size: 11px;font-weight: normal;font-family: Verdana, Arial, Helvetica, sans-serif;vertical-align:bottom;">(Minutes)</font>
		</div>
		<div class="divFloatLeft" style="width: 150px; margin-top: 18px;">
			<fmt:message key="label.timeframe" />:
		</div>
		<div class="divFloatLeft" style="margin-top: 18px;">From:&nbsp;&nbsp;</div>
		<div class="divFloatLeft" style="margin-top: 10px;">
			<form:input path="timeFrameStart" cssStyle="width: 120px;" />
		</div>
		<div class="divFloatLeft" style="margin-top: 18px;margin-left:20px;">To:&nbsp;&nbsp;</div>
		<div style="margin-top: 10px; padding-left: 20px;">
			<form:input path="timeFrameEnd" cssStyle="width: 120px;" />
		</div>
		<div class="divFloatLeft" style="width: 150px; margin-top: 10px;">
			<a
				style="text-decoration: underline; color: #1786d4; font-size: 11px;"
				href="javascript:void(0);" onclick="selectDeselectCustomers()"><fmt:message
					key="label.customers" /></a><font style="color: red;">*</font>:
		</div>
		<div style="margin-top: 10px;margin-left:150px;">
			<c:set var="counter" value="1"></c:set>
			<table id="customerTable">
			<c:forEach items="${hdCancelAllDTO.customerList}"
				var="customerDetail" varStatus="contact">
				<c:if test="${counter%3 eq 1}">
					<tr class=" PCSTD0007">
				</c:if>
				<td><span style="width: 150px; display: inline-block;" class="spanCheck">
					<form:checkbox path="customerList[${contact.index}].checked"
						cssClass="cancelCustomers" />&nbsp;${customerDetail.customerName}
					<form:hidden path="customerList[${contact.index}].msaId" />
				</span></td>
				<c:if test="${(counter%3 eq 0)or contact.last}">
				</tr>
				</c:if>
		<c:set var="counter" value="${counter + 1}" />
		</c:forEach>
		</table>
	<div style="margin-top: 10px;">
	<input id="displayOutageListBtn" type="button" value="Display Outages"
		class="FMBUT0003" onclick="getOutagesList();" /> <input
		id="refreshOutageListBtn" type="button" value="Refresh Outages List"
		class="FMBUT0003" onclick="refreshOutagesList();" style="display: none;" />
	</div>
</div>

<div id="outagesDiv" style="display:none;">
	<div class="PCSSH0001 divFloatLeft" style="width: 150px;">Outages</div>
	<div style="margin-top: 10px;">	<a
				style="text-decoration: underline; color: #1786d4; font-size: 11px;"
				href="javascript:void(0);" onclick="selectAllOutages()">Select All</a>&nbsp;&nbsp;	<a
				style="text-decoration: underline; color: #1786d4; font-size: 11px;"
				href="javascript:void(0);" onclick="deselectAllOutages()">Deselect All</a></div>
	<div>&nbsp;</div>
	<div id="outagesListDiv"></div>
</div>
<div class="PCSTD0011" style="display:none;margin-top:20px;" id="messageDiv"></div>
</div>
</form:form>
</div>