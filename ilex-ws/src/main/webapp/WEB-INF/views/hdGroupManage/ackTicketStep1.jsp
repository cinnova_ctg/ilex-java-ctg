<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import = "com.mind.common.EnvironmentSelector" %>
<%
 String apiUrl = EnvironmentSelector.getBundleString("api.url");
%>

<!--<script src="/Ilex-WS/resources/js/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>-->

<script src="${pageContext.request.contextPath}/resources/js/cookie_management.js"></script>

<script type="text/javascript">

    $(document).ready(function() {        
                
        var api_url = '<%= apiUrl %>';        
                
        var user_data = getCookie('cns-user_data');
        var token = getCookie('cns-token');
        
        var parsedUserData = JSON.parse(user_data);

        //Get the customer ID and ticket number from the java object.
        var mmId = ${hdTicketDTO.msaId};     
        var ticketNumber = "${hdTicketDTO.ticketNumber}";
          
        var responseDataSuffix = 0;
        // Get the data about a current site.
        $.ajax({
            method: "GET",
            url: api_url + "wug/customer_device/device_data/" + mmId,
            headers: {"x-access-token": token, "x-access-user-id": parsedUserData.id},
            cache: true,
            dataType: "json"         
        }).done(function (res) {
            // Did the API call end successfully?
            if (res.status === 'success') {
                responseDataSuffix = res;

                    // Figure out which devices are considered backup.
                    var backupSuffixArray = [];
                    for(var i = 0; i < responseDataSuffix.data.length; i++){

                        if(responseDataSuffix.data[i].name === "Backup"){
                            backupSuffixArray.push(responseDataSuffix.data[i].suffix);
                        }
                    }

                    // Figure out which devices are considered primary.
                    var primarySuffixArray = [];
                    for(var i = 0; i < responseDataSuffix.data.length; i++){

                        if(responseDataSuffix.data[i].name === "Primary"){
                            primarySuffixArray.push(responseDataSuffix.data[i].suffix);
                        }
                    }

                    var responseDataDevice = 0;
                    //Feed the ticket number to get the device list, and their statuses. 
                    $.ajax({
                        method: "GET",
                        url: api_url + "ilex/wugattributes/" + ticketNumber,
                        headers: {"x-access-token": token, "x-access-user-id": parsedUserData.id},
                        cache: true,
                        dataType: "json"
                    }).done(function (res) {
                        if (res.status === 'success') {
                            responseDataDevice = res;

                                    //Check to see what type of device this ticket is for.
                                    var ticketDevice = responseDataDevice.data[0].wug_ip_name;
                                    var resDeviceSuffix = "";

                                    //Cut off the suffix to compare to known suffixes for this customer.
                                            var the_dash_location = ticketDevice.lastIndexOf("-");

                                            //Ensure that there actually IS a dash, which will be something other than -1.
                                            if (the_dash_location !== -1) {
                                                var resDeviceSuffix = ticketDevice.substring(the_dash_location + 1, ticketDevice.length);
                                                //Make it look pretty, regardless of how the person typed it in.
                                                if (resDeviceSuffix.length === 1) {
                                                    resDeviceSuffix = resDeviceSuffix.toUpperCase();
                                                }
                                                resDeviceSuffix = resDeviceSuffix.trim();
                                            }
                                            else {
                                                //No suffix
                                                resDeviceSuffix = 'No Suffix';
                                            }
                                    
                                    var backupNames = [];
                                    
                                    for(var l = 0; l < backupSuffixArray.length; l++){
                                        //Construct the backup's name.
                                        var backupName = "";
                                        var the_dash_location = ticketDevice.lastIndexOf("-");

                                        if (the_dash_location !== -1) {
                                            backupName = ticketDevice.substring(0, the_dash_location);
                                        } else {
                                            backupName = ticketDevice;
                                        }

                                        //Adjust if the backup happens to be devices without suffixes.
                                        if(backupSuffixArray[l] === 'No Suffix'){
                                        //Do nothing.
                                        } else {                                      
                                        backupName = backupName + "-" + backupSuffixArray[l];
                                        }
                                        
                                        backupNames.push(backupName);
                                        
                                    }
                                    
                                    var thePrimaryDevice = false;
                                    //Compare this suffix to the known primary device suffixes for this customer.
                                    for(var p = 0; p < primarySuffixArray.length; p++){
                                        if(resDeviceSuffix === primarySuffixArray[p]){
                                            thePrimaryDevice = true;
                                        }
                                    }

                                    //If this is the primary device, check for the existance and status of a backup.
                                    if(thePrimaryDevice === true){
                                        var deviceStatusRes = "";
                                           $.ajax({
                                            type: "POST",
                                            url: api_url + "wug/customer_device/checkBackup/",
                                            headers: {"x-access-token": token, "x-access-user-id": parsedUserData.id},
                                            cache: true,
                                            dataType: "json",
                                            data: {'device_names': backupNames}
                                        }).done(function (res) {
                                            if (res.status === 'success') {
                                            deviceStatusRes = res;                                                

                                            var backupStatus = deviceStatusRes.data;
                                            
                                            if(backupStatus === "Up") {
                                            document.getElementById('backupUp').style.display = "inline";
                                            } 
                                            if(backupStatus === "Down") {
                                            document.getElementById('backupDown').style.display = "inline"; 
                                            }
                                            if(backupStatus === "No Backup") {
                                            document.getElementById('noBackup').style.display = "inline"; 
                                            }

                                            } else {
                                                // If error, show unknown status.
                                                //alert(JSON.stringify(res));
                                            }
                                        }).fail(function () {
                                            // If error, show unknown status.
                                            //alert('Unknown error.');
                                        }); 
                                    } else{
                                        //This isn't a primary device.
                                            document.getElementById('noPrimary').style.display = "inline"; 
                                    }
                           } else {
                                document.getElementById('noEntry').style.display = "inline";
                                document.getElementById('backupUp').style.display = "none";
                                document.getElementById('backupDown').style.display = "none";
                                document.getElementById('noBackup').style.display = "none";
                                document.getElementById('noPrimary').style.display = "none";
                            }
                        }).fail(function () {
                            // If error, show unknown status.
                            //alert('Unknown error.');
                        });
                            
                    } else {
                        // If error, show unknown status.
                        //alert(JSON.stringify(res));
                    }
                }).fail(function () {
                    // If error, show unknown status.
                    //alert('Unknown error.');
                });
        

        $('#preferredArrivalWindowTo').datetimepicker({
            dateFormat: "mm-dd-yy",
            showOn: "both",
            buttonImage: pageContextPath
                    + '/resources/images/calendar.gif'

        }).next(".ui-datepicker-trigger").addClass(
                "calImageStyle");

        $('#preferredArrivalWindowFrom').datetimepicker({
            dateFormat: "mm-dd-yy",
            showOn: "both",
            buttonImage: pageContextPath
                    + '/resources/images/calendar.gif'

        }).next(".ui-datepicker-trigger").addClass(
                "calImageStyle");

        $('#preferredArrivalTime').datetimepicker({
            dateFormat: "mm-dd-yy",
            showOn: "both",
            buttonImage: pageContextPath
                    + '/resources/images/calendar.gif'

        }).next(".ui-datepicker-trigger").addClass(
                "calImageStyle");

        $('#createTicketNextActionDate').datetimepicker({
            dateFormat: "mm-dd-yy",
            showOn: "both",
            buttonImage: pageContextPath
                    + '/resources/images/calendar.gif',
            beforeShow: function(input, instance) {
                if ($('#createTicketNextActionDate').val() == "")
                {
                    var currentDate = getCurrentDate();
                    $('#createTicketNextActionDate').datetimepicker('setDate', currentDate);
                }
            }


        }).next(".ui-datepicker-trigger").addClass(
                "calImageStyle");

        $('#createTicketEffortStart').datetimepicker({
            dateFormat: "mm-dd-yy",
            showOn: "both",
            buttonImage: pageContextPath
                    + '/resources/images/calendar.gif'

        }).next(".ui-datepicker-trigger").addClass(
                "calImageStyle");
        $('#createTicketEffortStop').datetimepicker({
            dateFormat: "mm-dd-yy",
            showOn: "both",
            buttonImage: pageContextPath
                    + '/resources/images/calendar.gif'

        }).next(".ui-datepicker-trigger").addClass(
                "calImageStyle");


    });
</script>

<form:form commandName="hdTicketDTO" id="ackTicketForm1">
    <div id="popup_progress" style="position: absolute; top: 0px;">
        <div id="light" class="white_content">
            <p id="ticketProcessStatus">Acknowledging Ticket...</p>
            <div class="meter">
                <span style="width: 5%"></span>
            </div>
            <input id="confirmButtonAck" type="button" value="Close" class="button">
        </div>
        <div id="fade" class="black_overlay"></div>
    </div>
    <form:hidden path="customerName"/>
    <form:hidden path="appendixName"/>
    <input id="userId" name="userId" type="hidden" value="<%= (String) session.getAttribute("userid")%>">
    <form:hidden path="msaId"/>
    <form:hidden path="appendixId"/>
    <form:hidden path="ticketType"/>
    <form:hidden path="ticketSource"/>
    <form:hidden path="ackCubeType"/>
    <form:hidden path="ticketId"/>
    <form:hidden path="ticketNumber"/>
    <form:hidden path="siteId"/>
    <div class="WASTD0003 PCSTD0010" style="text-align: left;">

        <div style="border-bottom: 1px solid black;padding-bottom: 15px;">
            <div class="PCSTD0010 divFloatLeft" style="width: 150px;">
                Customer and Appendix:
            </div>
            <div>
                ${hdTicketDTO.customerName} and  ${hdTicketDTO.appendixName}
            </div>

            <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 5px;">
                Ticket Type:
            </div>
            <div style="margin-top: 5px;">
                ${hdTicketDTO.ticketType}
            </div>
        </div>
        <div class="divFloatLeft" id="LeftDiv" style="margin-bottom: 20px; width:450px;">
            <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;">
                Billable:
            </div>
            <div style="margin-top: 10px;" >
                <form:radiobutton path="billingStatus" value="true" />&nbsp;Yes&nbsp;
                <form:radiobutton path="billingStatus" value="false"/>&nbsp;No&nbsp;
            </div>

            <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;">
                Site:
            </div>

            <c:if test="${hdTicketDTO.ackCubeType == 'hdPendingAckCust' }">
                <c:if test="${empty hdTicketDTO.siteId }">
                    <div class="divFloatLeft" style="margin-top: 10px;margin-right: 10px;">
                        <select  style="width: 180px;">
                            <option value="">Select</option>
                            <c:forEach items="${sites}" var="site">
                                <option value="${site.siteId}~${site.siteNumber}">${site.siteNumber},${site.siteAddress},${site.siteCity},${site.state}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div style="margin-top: 10px;">
                        <input type="button" value="Enter New Site" onclick="enterNewSite();" class="FMBUT0006" style="width: 120px;"/>
                    </div>
                    <div style="margin-top: 10px;margin-left: 150px;">
                        ${hdTicketDTO.siteDetailDTO.siteNumber}
                    </div>
                </c:if>
                <c:if test="${not empty hdTicketDTO.siteId }">
                    <div style="margin-top: 10px;">
                        ${hdTicketDTO.siteDetailDTO.siteNumber}
                    </div>
                </c:if>
            </c:if>
            <c:if test="${hdTicketDTO.ackCubeType != 'hdPendingAckCust' }">
                <div style="margin-top: 10px;">
                    ${hdTicketDTO.siteDetailDTO.siteNumber}
                </div>
            </c:if>
            <div style="margin-top: 5px;margin-left: 150px;">
                ${hdTicketDTO.siteDetailDTO.siteAddress}
            </div>
            <div style="margin-top: 5px;margin-left: 150px;">
                ${hdTicketDTO.siteDetailDTO.siteCity},${hdTicketDTO.siteDetailDTO.state},${hdTicketDTO.siteDetailDTO.country} ${hdTicketDTO.siteDetailDTO.siteZipcode}
            </div>
            <c:if test="${hdTicketDTO.ackCubeType == 'hdPendingAckCust' }">
                <c:if test="${empty hdTicketDTO.siteId }">
                    <div class="PCCOMMENTS " style="margin-top: 5px;margin-left: 150px;">
                        The above site information was provided with this ticket but could not <br/> be located within Ilex. Click Enter New Site to add.
                    </div>
                </c:if>
            </c:if>
            <div style="height: 5px;">&nbsp;</div>


            <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;">
                Name:
            </div>
            <div style="margin-top: 10px;">
                <form:input path="callerName" placeholder="Enter Name" value="${callerName}" cssStyle="width: 250px;"/>
            </div>
            <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;">
                Phone Number:
            </div>
            <div style="margin-top: 10px;">
                <form:input path="callerPhone" placeholder="Enter Phone Number" value="${callerPhone}" cssStyle="width: 180px;"/>
            </div>
            <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;">
                Email Address:
            </div>
            <div style="margin-top: 10px;">
                <form:input path="callerEmail" placeholder="Enter Email Address" value="${callerEmail}" cssStyle="width: 250px;"/>
            </div>
            <c:if test="${hdTicketDTO.ackCubeType == 'hdPendingAckCust' }">
                <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;">
                    Customer Ticket #:
                </div>
                <div style="margin-top: 10px;">
                    ${hdTicketDTO.customerTicketReference }
                </div>
            </c:if>
            <div>&nbsp;</div>

            <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;">
                Category<font style="color: red;">*</font>:
            </div>
            <div style="margin-top: 10px;">
                <form:select path="category" cssStyle="width: 250px;" >
                    <option value="">Select</option>
                    <c:forEach items="${categories}" var="category">
                        <option value="${category.categoryId}" <c:if test="${category.categoryId==hdTicketDTO.category}">selected="selected"</c:if>>${category.category}</option>
                    </c:forEach>
                </form:select>
            </div>

            <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;">
                Configuration Item<font style="color: red;">*</font>:
            </div>
            <div style="margin-top: 10px;">
                <form:select path="configurationItem" cssStyle="width: 250px;" >
                    <option value="">Select</option>
                    <c:forEach items="${configurationItems}" var="configurationItem">
                        <option value="${configurationItem.configurationItemId}"  <c:if test="${configurationItem.configurationItemId==hdTicketDTO.configurationItem}">selected="selected"</c:if>>${configurationItem.configurationItem}</option>
                    </c:forEach>
                </form:select>
            </div>

            <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;">
                Short Description<font style="color: red;">*</font>:
            </div>
            <div style="margin-top: 10px;">
                <form:input path="shortDescription" placeholder="Enter Short Description" cssStyle="width: 250px;" value="${hdTicketDTO.shortDescription }"/>
            </div>
            
            <div class="PCSTD0010 divFloatLeft" style="width: 150px;margin-top: 10px;">
                Problem Description<font style="color: red;">*</font>:
            </div>
            <div style="margin-top: 10px;">
                <form:textarea path="problemDescription" rows="8" cols="60" cssStyle="width: 50%;overflow-y: scroll;resize:none;font-size: 11px;font-family: Verdana, Arial, Helvetica, sans-serif;font-weight:normal"  spellcheck="true" placeholder="Enter Problem Description" value="${hdTicketDTO.problemDescription }"/>
            </div>
        </div>
        <div id="rightDiv" style="border-left: 1px solid black;margin-top: 10px;margin-bottom: 15px;padding-left: 20px;">

            <c:if test="${hdTicketDTO.ackCubeType == 'hdPendingAckAlert'}">
                <div class="divFloatLeft" style="width: 100px;margin-top: 10px;">
                    Alert Condition:
                </div>
                <div style="margin-top: 10px;">
                    [Alert] &nbsp;&nbsp;(<a href="#" onclick="openAlertState();" style="text-decoration: underline;">Alert Details</a>)
                </div>

                <div class="divFloatLeft" style="width: 100px;margin-top: 10px;">
                    Priority:
                </div>
                <div style="margin-top: 10px;">
                    ${hdTicketDTO.priority}
                </div>

            </c:if>
            <c:if test="${hdTicketDTO.ackCubeType == 'hdPendingAckMonitor'}">
                <div class="divFloatLeft" style="width: 100px;margin-top: 10px;">
                    Primary Circuit:
                </div>
                <div style="margin-top: 10px;">
                    <c:if test="${hdTicketDTO.primaryStatus=='down'}">
                        Down&nbsp;<img src="${pageContext.request.contextPath}/resources/images/redactive.jpg"/>
                    </c:if>
                    <c:if test="${hdTicketDTO.primaryStatus=='up'}">
                        Up&nbsp;<img src="${pageContext.request.contextPath}/resources/images/greenactive.jpg"/>
                    </c:if>

                    (<a href="#" style="text-decoration: underline;" onclick="openStateTail();">State Tail</a>)
                </div>

                <div class="divFloatLeft" style="width: 100px;margin-top: 10px;">
                    Backup Circuit <br/> Status<font style="color: red;">*</font>:
                </div>
                <div class="divFloatLeft" style="margin-top: 15px;">
                    <div id="backupDown" style="display: none">
                        Down&nbsp;<img src="${pageContext.request.contextPath}/resources/images/redactive.jpg"/>
                    </div>
                    <div id="backupUp" style="display: none">
                        Up&nbsp;<img src="${pageContext.request.contextPath}/resources/images/greenactive.jpg"/>
                    </div>
                    <div id="noBackup" style="display: none">
                        No Registered Backup
                    </div>
                    <div id="noPrimary" style="display: none">
                        Not a Registered Primary Device
                    </div>
                    <div id="noEntry" style="display: none">
                        N/A
                    </div>
                </div>
                <c:choose>
                    <c:when test="${hdTicketDTO.backUpCktStatus=='1' or hdTicketDTO.backUpCktStatus=='0'}">
                        <div style="float: left;margin-left: 10px;margin-top: 15px;">Device:</div>
                        <c:if test="${not empty hdTicketDTO.device}">
                            <div style="margin-top: 15px;" id="deviceDiv">${hdTicketDTO.device}&nbsp;</div>
                        </c:if>
                        <c:if test="${empty hdTicketDTO.device}">
                            <div style="margin-top: 15px;" id="deviceDiv">Unknown&nbsp;</div>
                        </c:if>
                    </c:when>
                    <c:otherwise >
                        <div style="margin-top: 15px;">&nbsp;</div>
                    </c:otherwise>
                </c:choose>
                <div>&nbsp;</div>
                <div class="divFloatLeft" style="width: 100px;margin-top: 10px;">
                    Priority:
                </div>
                <div style="margin-top: 10px;">
                    ${hdTicketDTO.priority}
                </div>
            </c:if>
            <div class="divFloatLeft" style=";margin-bottom: 10px;width: 22%;height: 80px;">
                <div class="divFloatLeft" style="height: 70px;width: 100px;padding-top: 10px;">Impact<font style="color: red;">*</font>:</div>
                <div style="margin-top: 10px;">
                    <form:radiobutton path="impact" value="1"/>  &nbsp;<span class="impact">1-High</span>
                </div>
                <div style="margin-top: 10px;">
                    <form:radiobutton path="impact" value="2" /> &nbsp;<span class="impact">2-Medium</span>
                </div>
                <div style="margin-top: 10px;">
                    <form:radiobutton path="impact" value="3" /> &nbsp;<span class="impact">3-Low</span>
                </div>
            </div>

            <div  style="margin-top: 0px;margin-bottom: 10px;height: 80px;">
                <div style="height: 70px;width: 100px;float: left;margin-left: 200px;">Urgency<font style="color: red;">*</font>:</div>
                <div style="margin-top:10px;">
                    <form:radiobutton path="urgency" value="1" /> &nbsp;<span class="urgency">1-High</span>
                </div>
                <div style="margin-top: 10px;">
                    <form:radiobutton path="urgency" value="2" /> &nbsp;<span  class="urgency">2-Medium</span>
                </div>
                <div style="margin-top: 10px;">
                    <form:radiobutton path="urgency" value="3" /> &nbsp;<span  class="urgency">3-Low</span>
                </div>
            </div>

            <div class="divFloatLeft" style="padding-left: 230px;"><b>Start</b></div>
            <div  style="padding-left: 395px;"><b>Stop</b></div>
            <div>&nbsp;</div>

            <div style="margin-bottom: 20px;">
                <div class="divFloatLeft" style="width: 100px;">Effort<font style="color: red;">*</font>:</div>
                <div class="divFloatLeft" style="margin-right: 10px;vertical-align: top;">
                    <form:input path="efforts" cssStyle="width:30px;"/> <font style="padding-top:2px;">Mins</font>
                </div>
                <div class="divFloatLeft" style="margin-right: 30px;padding-top:3px;">
                    <b>or</b>
                </div>
                <div class="divFloatLeft" style="width: 150px;" >
                    <input type="text" id="createTicketEffortStart" onchange="calculateCreateTicketEfforts();" style="width: 120px;">
                </div>
                <div>
                    <input type="text" id="createTicketEffortStop" onchange="calculateCreateTicketEfforts();" style="width: 120px;">
                </div>
            </div>
            <div  class="divFloatLeft" style="width: 100px;">Next Action<font style="color: red;">*</font>:</div>
            <div>
                <form:select path="nextAction" cssStyle="width: 250px;">
                    <option value="">Select</option>
                    <c:forEach items="${nextActions}" var="nextAction">
                        <option value="${nextAction.nextActionId}" <c:if test="${nextAction.nextActionId == hdTicketDTO.nextAction }">selected="selected"</c:if>>${nextAction.nextAction}</option>
                    </c:forEach>

                </form:select>
            </div>

            <div style="margin-bottom: 20px;">
                <div class="divFloatLeft" style="width: 100px;margin-top: 3px;">Due<font style="color: red;">*</font>:</div>
                <div class="divFloatLeft" style="margin-right: 10px; margin-top: 3px;">
                    <form:input path="createTicketNextActionTime" cssStyle="width:30px;"/>
                </div>
                <div class="divFloatLeft" style="width: 100px;margin-top: 5px;">
                    <form:radiobutton path="createTicketNextActionUnit" value="Hrs"/><label for="hrs" style="vertical-align: top;">Hrs</label>&nbsp;
                    <form:radiobutton path="createTicketNextActionUnit" value="Mins"/><label for="hrs" style="vertical-align: top;">Mins </label>
                </div>
                <div class="divFloatLeft" style="margin-right: 30px;margin-top: 5px;">
                    <b>or</b>
                </div>

                <div ><form:input path="createTicketNextActionDate" cssStyle="width: 120px; margin-top: 3px;" value="${hdTicketDTO.createTicketNextActionDate}"/></div>
                <div>&nbsp;</div>
            </div>


            <div class="PCSTD0010 divFloatLeft" style="width: 100px;margin-top: 10px;">
                Work Notes<font style="color: red;">*</font>:
            </div>
            <div style="margin-top: 10px;">
                <form:textarea path="workNotes"  rows="8" cols="60" cssStyle="width: 80%;overflow-y: scroll;resize:none;font-size: 11px;font-family: Verdana, Arial, Helvetica, sans-serif;font-weight:normal"  spellcheck="true"/>
            </div>
            
            <c:if test="${hdTicketDTO.ackCubeType == 'hdPendingAckCust'}">
                <div class="PCSTD0010 divFloatLeft" style="width: 100px;margin-top: 10px;">Customer<br/>
                    Work Notes:
                </div>
                <div style="margin-top: 10px;">
                    <form:textarea path="customerWorkNotes" disabled="true"  rows="8" cols="60" cssStyle="width: 80%;overflow-y: scroll;resize:none;font-size: 11px;font-family: Verdana, Arial, Helvetica, sans-serif;font-weight:normal"  spellcheck="true"/>
                </div>
            </c:if>
            
            <div style="text-align: right;margin-top: 20px;">
                <input type="button" value="Acknowledge Ticket" onclick="ackTicket();" class="FMBUT0006" style="width: 120px;"/>
            </div>
            
            
        </div>


    </div>
</form:form>