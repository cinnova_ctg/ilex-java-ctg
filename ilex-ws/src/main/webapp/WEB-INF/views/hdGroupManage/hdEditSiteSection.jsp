<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript">
	$(document).ready(function() {

	});
</script>

<form:form commandName="siteDetailDTO" id="siteDetailDTOEdit">
	<form:hidden path="ticketId" />
	<form:hidden path="siteId" />
	<div class="WASTD0003 PCSTD0010" style="text-align: left;">

		<c:if test="${not empty siteDetailDTO.siteNumber}">
			<div style="margin-top: 5px;">
				${siteDetailDTO.siteNumber}&nbsp;</div>
		</c:if>
		<c:if test="${not empty siteDetailDTO.siteAddress}">
			<div style="margin-top: 5px;">
				${siteDetailDTO.siteAddress}&nbsp;</div>
		</c:if>
		<div style="margin-top: 5px;">
			<c:if test="${not empty siteDetailDTO.siteCity }">${siteDetailDTO.siteCity}</c:if>
			<c:if test="${not empty siteDetailDTO.state }">,&nbsp;${siteDetailDTO.state}</c:if>
			<c:if test="${not empty siteDetailDTO.country and siteDetailDTO.country ne 'US'}">,&nbsp;${siteDetailDTO.country}</c:if>
			<c:if test="${not empty siteDetailDTO.siteZipcode }">,&nbsp;${siteDetailDTO.siteZipcode}</c:if>
		</div>
		<div class="divFloatLeft" style="margin-top: 5px; width: 50px;">
			Notes:</div>
		<div style="margin-top: 5px;">
			<form:textarea path="workNotes" rows="5" cols="60"
				cssStyle="width: 70%;overflow-y: scroll;resize:none;"
				spellcheck="true" />
		</div>
		<div style="margin-top: 15px; text-align: right; margin-right: 90px;">
			<input type="button" value="Submit" class="FMBUT0003"
				onclick="updateSiteSection();">
		</div>
		<div
			style="margin-bottom: 5px; padding-bottom: 20px; margin-top: 10px;">
			<div class="PCCOMMENTS">Note: Edit other site information from
				the Ilex site master page.</div>
		</div>

	</div>
</form:form>