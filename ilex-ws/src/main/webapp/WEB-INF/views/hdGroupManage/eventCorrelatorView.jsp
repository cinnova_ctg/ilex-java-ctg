<!DOCTYPE html>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
	var pageContextPath = '/Ilex-WS';
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/hdMainTable.js?v=<fmt:message key='hd.js.latest.version'/>"></script>

<script type="text/javascript">
	function updateDuration(duration) {
		if (document.getElementById("duration") != null) {
			document.getElementById("duration").value = duration;
		}
	}

	function validate() {
		var type = document.getElementById("type").value;
		if (type == 'Customer') {
// 			if (document.getElementById("customerInfo").value == null
// 					|| document.getElementById("customerInfo").value == '0') {
// 				alert("Please select customer");
// 				document.getElementById("customerInfo").focus();
// 				return false;
// 			} else 
			if (document.getElementById("pointOfFailureId").value == null
				|| document.getElementById("pointOfFailureId").value == '0') {
				alert("Please select Point of Failure.");
				document.getElementById("pointOfFailureId").focus();
				return false;
			}
		}
		if (document.getElementById("threshold").value == null
				|| document.getElementById("threshold").value == '') {
			alert("Threshold value should not be empty.");
			document.getElementById("threshold").focus();
			return false;
		}

		return true;
	}
	
	function isNumberKey(evt){
	    var charCode = (evt.which) ? evt.which : event.keyCode
	    if (charCode > 31 && (charCode < 48 || charCode > 57))
	        return false;
	    return true;
	}
</script>

<style>
body {
	font: 11px Verdana, Arial, Helvetica, sans-serif;
}

table.display thead th {
	font-size: 11px;
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	cursor: pointer;
	cursor: hand;
	text-align: center;
	padding-top: 5px;
	padding-left: 5px;
	padding-bottom: 5px;
}

table.tableWrapper  tr:nth-child(2n+2) {
	background-color: #e8eef7;
}
</style>
</head>
<body>

	<form action="/Ilex-WS/hdMainTable/EventCorrelatorAction.html"
		method="get">
		<input type="hidden" name="type" id="type" value="${type}" /> <input
			type="hidden" name="duration" id="duration" value="week" />

		<table>
			<tr>
				<td colspan="2"
					style="font-weight: bold; font-size: 12px; color: #000; font-family: Tahoma, Geneva, sans-serif; padding: 4px;">Event
					Correlator</td>
			</tr>

			<tr>
				<td>&nbsp;</td>
				<td>Customer:<input type="radio" name="typeChkbox"
					id="typeChkbox" onclick="openEventCorrelatorPopup('Customer');"
					title="Customer"
					<c:if test="${type eq 'Customer'}"> checked="checked"</c:if> />
					&nbsp;&nbsp;&nbsp;Staff:<input
					onclick="openEventCorrelatorPopup('Staff');" type="radio"
					name="typeChkbox" id="typeChkbox" title="Staff"
					<c:if test="${type eq 'Staff'}"> checked="checked"</c:if> /></td>
			</tr>

			<tr>
				<td>${type}:</td>
				<td><c:if test="${type eq 'Customer'}">
						<select style="width: 330px;" id="customerInfo"
							name="customerInfo">
							<option value="0">All Customers</option>
							<c:forEach items="${customers}" var="customer">
								<optgroup label="${customer.key}">
									<c:forEach items="${customer.value}" var="customerList">
										<option
											value="${customerList.msaId}~${customerList.appendixId}~${customerList.customerName}~${customerList.appendixName}~${customerList.apppendicBreakOut}">${customerList.customerName},
											${customerList.appendixName} -
											${customerList.apppendicBreakOut}</option>
									</c:forEach>
							</c:forEach>
						</select>
					</c:if> <c:if test="${type eq 'Staff'}">
				NOC Staff Member
			</c:if></td>
			</tr>

			<tr>
				<td>Condition:</td>
				<td><c:if test="${type eq 'Customer'}">
						<select style="width: 160px;" name="condition"
							id="pointOfFailureId">
							<option value="0">Select Point of Failure</option>
							<c:forEach items="${pointOfFailureList}" var="POF">
								<option value="${POF.key}">${POF.value}</option>
							</c:forEach>
						</select>
					</c:if> <c:if test="${type eq 'Staff'}">
				Updated By
			</c:if></td>
			</tr>

			<tr>
				<td>Duration:</td>
				<td>Today:<input type="radio" name="durationRadio"
					onclick="updateDuration('day');" id="durationRadio" title="Day" />&nbsp;&nbsp;&nbsp;Current
					Week:<input checked="checked" onclick="updateDuration('week');"
					type="radio" name="durationRadio" id="durationRadio" title="Week" />&nbsp;&nbsp;&nbsp;Current
					Month:<input type="radio" name="durationRadio" id="durationRadio"
					onclick="updateDuration('month');" title="Month" />
				</td>
			</tr>

			<tr>
				<td>Threshold %:</td>
				<td><input type="text" name="threshold" id="threshold" value="" maxlength="2" onkeypress="return isNumberKey(event)" 
					style="width: 156px;" /></td>
			</tr>

			<tr>
				<td>Color:</td>
				<td><input type="color" name="color" id="color"
					style="width: 154px;"></td>
			</tr>

			<tr>
				<td colspan="2"><input type="submit" name="save" id="save"
					onclick="return validate();" value="Save" /></td>
			</tr>

			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>

		</table>
	</form>

	<table class="tableWrapper">
		<tr style="background: #b5c8d9;">
			<th>Customer/Staff</th>
			<th>Appendix</th>
			<th>Condition</th>
			<th>Duration</th>
			<th>Threshold(%)</th>
			<th>Added By</th>
			<th>Date</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${EventsList}" var="eventLst">
			<tr style="background:${eventLst.color};color:white; ">
				<td>${eventLst.customerTitle}</td>
				<td>${eventLst.projectTitle}</td>
				<td>${eventLst.pofTitle}</td>
				<td>${eventLst.duration}</td>
				<td>${eventLst.threshold}</td>
				<td>${eventLst.pocName}</td>
				<td>${eventLst.updateDate}</td>
				<td><a
					href="/Ilex-WS/hdMainTable/EventCorrelatorAction.html?save=Delete&id=${eventLst.id}">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>
