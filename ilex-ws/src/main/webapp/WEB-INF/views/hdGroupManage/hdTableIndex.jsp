<!DOCTYPE html>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css"
              href="${pageContext.request.contextPath}/resources/css/style_common.css?v=<fmt:message key='hd.js.latest.version'/>" />
        <link rel="stylesheet" type="text/css"
              href="${pageContext.request.contextPath}/resources/css/hd-style.css?v=<fmt:message key='hd.js.latest.version'/>" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_hd.css" />
        <link rel="stylesheet" type="text/css"
              href="${pageContext.request.contextPath}/resources/plugins/dataTable/css/table_paginate.css?v=<fmt:message key='hd.js.latest.version'/>"
              title="currentStyle" />
        <link rel="stylesheet" type="text/css"
              href="${pageContext.request.contextPath}/resources/plugins/jquery-ui/base/ui.all.css"/>
        <link rel="stylesheet" type="text/css"
              href="${pageContext.request.contextPath}/resources/plugins/spellChecker/css/screen.css"/>
        <link rel="stylesheet" type="text/css"
              href="${pageContext.request.contextPath}/resources/plugins/spellChecker/css/spellchecker.css"/>
        <link rel="stylesheet" type="text/css"
              href="${pageContext.request.contextPath}/resources/css/dropdownStyle.css?v=<fmt:message key='hd.js.latest.version'/>" />



        <script type="text/javascript">
            var pageContextPath = '${pageContext.request.contextPath}';
            var userId = '<%= (String) session.getAttribute("userid")%>';
        </script>
        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>




        <link href="/Ilex-WS/resources/css/messi.min.css?v=45" type="text/css" rel="stylesheet">

        <script src="/Ilex-WS/resources/js/messi.min.js?v=66" type="text/javascript"></script>
    </head>
    <body id = "pbody">
        <div>
            <jsp:include page="hdBreadCrumb.jsp" />
            <div class="contentWrapper">
                <%--                      		<jsp:include page="summaryAlertBar.jsp" /> --%>
                <div>&nbsp;</div>
                <jsp:include page="hdMainTable.jsp" />
            </div>

        </div>
        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery/jquery-ui-1.8.23.custom.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.ui.core.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.ui.widget.min.js"></script>

        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/plugins/dataTable/js/dataTables-1.9.3.js?v=<fmt:message key='hd.js.latest.version'/>"></script>
        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery-ui-timepicker.js?v=<fmt:message key='hd.js.latest.version'/>"></script>

        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/plugins/spellChecker/js/jquery-impromptu.js "></script>
        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/plugins/spellChecker/js/jquery.spellchecker.js "></script>
        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/plugins/spellChecker/js/jquery-impspeller.js "></script>

        <script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/hdMainTable.js?1"></script>






        <script type="text/javascript">
            $(document).ready(function() {
                $(".contentWrapper").width($("#leftDiv").width() + $("#middleDiv").width() + $("#rightDiv").width() + 40);
                $('#pbody').css('width', '100%');
                if ($(window).width() > $(".contentWrapper").width())
                {
                    $("#hdBreadCrumbTable").width($(window).width());
                }
                else {
                    $("#hdBreadCrumbTable").width($(".contentWrapper").width() + 20);
                }
            });

        </script>

        <div id="popUpScreen" style="display: none; cursor: default;">
            <div style="border: 0px;">
                <table cellspacing="0" cellpadding="0" class="headerpaleyellow"
                       width="100%">
                    <tr>
                        <td align="left" id="pageTitle" height="20px"
                            style="padding-left: 5px;"></td>
                        <td align="right" style="padding-right: 5px;"><img
                                height="15" width="15" alt="Close" src="${pageContext.request.contextPath}/resources/images/delete.gif"
                                id="close" onclick="closeWindow();"></td>
                    </tr>
                </table>
            </div>
            <div id="popupBody"
                 style="padding-top: 4px; overflow: visible; overflow-x: auto; overflow-y: auto;">
                <img src="${pageContext.request.contextPath}/resources/images/waiting_tree.gif" />
            </div>
        </div>

        <div id="popUpScreen2" style="display: none; cursor: default;">
            <div style="border: 0px;">
                <table cellspacing="0" cellpadding="0" class="headerpaleyellow"
                       width="100%">
                    <tr>
                        <td align="left" id="pageTitle2" height="20px"
                            style="padding-left: 5px;"></td>
                        <td align="right" style="padding-right: 5px;"><img
                                height="15" width="15" alt="Close" src="${pageContext.request.contextPath}/resources/images/delete.gif"
                                id="close" onclick="closeWindow2();"></td>
                    </tr>
                </table>
            </div>
            <div id="popupBody2"
                 style="padding-top: 4px; overflow: visible; overflow-x: auto; overflow-y: auto;">
                <img src="${pageContext.request.contextPath}/resources/images/waiting_tree.gif" />
            </div>
        </div>
        <div id="popUpScreen3" style="display: none; cursor: default;">
            <div style="border: 0px;">
                <table cellspacing="0" cellpadding="0" class="headerpaleyellow"
                       width="100%">
                    <tr>
                        <td align="left" id="pageTitle3" height="20px"
                            style="padding-left: 5px;"></td>
                        <td align="right" style="padding-right: 5px;"><img
                                height="15" width="15" alt="Close" src="${pageContext.request.contextPath}/resources/images/delete.gif"
                                id="close" onclick="closeWindow3();"></td>
                    </tr>
                </table>
            </div>
            <div id="popupBody3"
                 style="padding-top: 4px; overflow: visible; overflow-x: auto; overflow-y: auto;">
                <img src="${pageContext.request.contextPath}/resources/images/waiting_tree.gif" />
            </div>
        </div>
        <div id="ticketPopUpScreen" style="display: none; cursor: default; outline: 2px solid #d7c960; border: 2px solid #e0e1e3">
            <div style="border: 0px;">
                <table cellspacing="0" cellpadding="0" class="headerpaleyellow"
                       width="100%" style="background-color: #d7c960">
                    <tr>
                        <td align="left" id="ticketPageTitle" height="20px"
                            style="padding-left: 5px;"></td>
                        <td align="right" style="padding-right: 5px;"><img
                                height="15" width="15" alt="Close"
                                src="${pageContext.request.contextPath}/resources/images/delete.gif"
                                id="close" onclick="closeWindow();"></td>
                    </tr>
                </table>
            </div>
            <div id="ticketPopupBody"
                 style="padding-top: 4px; overflow: visible; overflow-x: auto; overflow-y: auto;">
                <img
                    src="${pageContext.request.contextPath}/resources/images/waiting_tree.gif" />
            </div>
        </div>

    </body>
</html>
