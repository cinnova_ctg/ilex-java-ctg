<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script>
$(document).ready(function(){
});
</script>
<div class="WASTD0003 PCSTD0010" style="text-align: left;">
		<div>&nbsp;</div>
		<div class="divFloatLeft" style="width: 100px;text-align: center;"><b>Date</b></div>
		<div class="divFloatLeft" style="width: 100px;text-align: center;"><b>Author</b></div>
		<div style="width: 350px;text-align: center;margin-left: 200px;"><b>Note</b></div>
		<c:forEach items="${hdWorkNoteDTOs}" var="workNotes">
			<div class="divFloatLeft"  style="width: 100px;margin-top: 10px;text-align: center;">${workNotes.workNoteDateStr }</div>
			<div class="divFloatLeft" style="width: 100px;margin-top: 10px;text-align: center;">${workNotes.userLastName }</div>
			<div style="margin-top: 10px;">
				<c:if test="${fn:length(workNotes.workNote) <= 240 }">
					<div style="width: 350px;margin-left: 200px;white-space: normal;">${workNotes.workNote }</div>
				</c:if>
				<c:if test="${fn:length(workNotes.workNote) > 240 }">
					<textarea class="workNotesText" rows="5" cols="100" style="width: 350px;font-size: 11px;font-family: Verdana, Arial, Helvetica, sans-serif;font-weight:normal;border: 1px solid #c9c2c1;border-top: 2px solid #999999;overflow-y: scroll;resize:none;" readonly="readonly">${workNotes.workNote }</textarea>
				</c:if>
			</div>
		</c:forEach>
		<div>&nbsp;</div>
</div>