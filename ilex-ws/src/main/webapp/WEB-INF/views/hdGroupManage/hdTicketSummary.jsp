<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script>

$(document).ready(function(){
	
	$('#ticketSummaryStartDate').datetimepicker({
		dateFormat :"mm-dd-yy",
		showOn : "both",
		buttonImage : pageContextPath
			+ '/resources/images/calendar.gif'
		
	}).next(".ui-datepicker-trigger").addClass(
			"calImageStyle");
	
	$('#ticketSummaryStopDate').datetimepicker({
		dateFormat :"mm-dd-yy",
		showOn : "both",
		buttonImage : pageContextPath
			+ '/resources/images/calendar.gif'
		
	}).next(".ui-datepicker-trigger").addClass(
			"calImageStyle");
	
	
	$("#pageTitle2").text(
	"${hdTicketDTO.ticketType} for ${hdTicketDTO.customerName}, ${hdTicketDTO.appendixName}, ${hdTicketDTO.siteDetailDTO.siteNumber}, ${hdTicketDTO.ticketNumber}");
	$("#pageTitle").text(
	"${hdTicketDTO.customerName}, ${hdTicketDTO.appendixName}, ${hdTicketDTO.siteDetailDTO.siteNumber}, ${hdTicketDTO.ticketType}, ${hdTicketDTO.ticketNumber}");
	var ticketStatus="${hdTicketDTO.ticketStatus}";
	if(ticketStatus=='New')
		{
		$('.disableEdit').each(function(){
			$(this).attr('disabled',true);
			$(this).addClass('disableText');
		});
		}
	
});
</script>
<div class="WASTD0003 PCSTD0010" style="text-align: left;">
<form:form modelAttribute="hdTicketDTO" id="hdTicketDTO">
<form:hidden path="ticketId"/>
<form:hidden path="siteId"/>
	<div class="divFloatLeft" style="width: 48%;margin-bottom: 20px;">
		<div class="PCSSH0001 divFloatLeft">Ticket Details</div>
		<div style="text-align: right;margin-right: 20px;"><input type="button" value="Edit" class="FMBUT0003 disableEdit" onclick="openTicketDetails();"/></div>
		<div  style="margin-top: 10px;">Ticket Contact</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 10px;">Name:</div>
		<div style="margin-top: 10px; " id="callerName">${hdTicketDTO.callerName}&nbsp;</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Phone Number:</div>
		<div style="margin-top: 5px;" id="callerPhone">${hdTicketDTO.callerPhone}&nbsp;</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Email Address:</div>
		<div style="margin-top: 5px;" id="callerEmail">${hdTicketDTO.callerEmail}&nbsp;</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Customer Ticket #:</div>
		<div style="margin-top: 5px;" id="customerTicketReference">${hdTicketDTO.customerTicketReference}&nbsp;</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Billable:</div>
		<div style="margin-top: 5px;" id="billingStatus">
			<c:if test="${empty hdTicketDTO.billingStatus}">
			&nbsp;
			</c:if>
			<c:if test="${hdTicketDTO.billingStatus eq true}">
			Yes	
			</c:if>
			<c:if test="${hdTicketDTO.billingStatus ne true}">
			No	
			</c:if>
		</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Category:</div>
		<div style="margin-top: 5px;" id="category">${hdTicketDTO.category}&nbsp;</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Configuration Item:</div>
		<div style="margin-top: 5px;" id="configurationItem">${hdTicketDTO.configurationItem}&nbsp;</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Short Description:</div>
		<div style="margin-top: 5px;"><textarea cols="40" rows="4" id="shortDescription" style="font-size: 11px;font-family: Verdana, Arial, Helvetica, sans-serif;font-weight:normal">${hdTicketDTO.shortDescription}</textarea></div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Problem Description:</div>
		<div style="margin-top: 5px;"><textarea cols="40" rows="6" id="problemDescription" style="font-size: 11px;font-family: Verdana, Arial, Helvetica, sans-serif;font-weight:normal">${hdTicketDTO.problemDescription}</textarea></div>
		
		<div class="PCSSH0001 divFloatLeft" style="margin-top: 30px;">Site</div>
		<div style="text-align: right;margin-right: 20px;margin-top: 30px;"><input type="button" value="Edit" class="FMBUT0003 disableEdit" onclick="openEditSiteSection();"/></div>
		<c:if test="${not empty hdTicketDTO.siteDetailDTO.siteNumber}">
			<div style="margin-top: 5px;">
				${hdTicketDTO.siteDetailDTO.siteNumber}&nbsp;
			</div>
		</c:if>
		<c:if test="${not empty hdTicketDTO.siteDetailDTO.siteAddress}">
			<div style="margin-top: 5px;">
				${hdTicketDTO.siteDetailDTO.siteAddress}&nbsp;
			</div>
		</c:if>
		<div style="margin-top: 5px;">
			<c:if test="${not empty hdTicketDTO.siteDetailDTO.siteCity }">${hdTicketDTO.siteDetailDTO.siteCity}</c:if><c:if test="${not empty hdTicketDTO.siteDetailDTO.state }">,&nbsp;${hdTicketDTO.siteDetailDTO.state}</c:if><c:if test="${not empty hdTicketDTO.siteDetailDTO.country }">,&nbsp;${hdTicketDTO.siteDetailDTO.country}</c:if><c:if test="${not empty hdTicketDTO.siteDetailDTO.siteZipcode }">,&nbsp;${hdTicketDTO.siteDetailDTO.siteZipcode}</c:if>
		</div>
		<div class="divFloatLeft" style="margin-top: 5px;width: 50px;">
			Notes:
		</div>
		<div style="margin-top: 5px;"><textarea cols="40" rows="4" id="siteWorkNotes" style="font-size: 11px;font-family: Verdana, Arial, Helvetica, sans-serif;font-weight:normal">${hdTicketDTO.siteDetailDTO.workNotes}</textarea></div>
		
		<div style="margin-top: 15px;">
			<div class="divFloatLeft" style="width: 50%;">
				<div style="margin-left: 40px;margin-bottom: 10px;" class="PCSSH0001">Provisioning Data</div>
				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">IP Type:</div>
				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.provisioningDetailDTO.ipType}&nbsp;</div>
				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">IP Username:</div>
				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.provisioningDetailDTO.ipUsername}&nbsp;</div>
				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">IP Password:</div>
				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.provisioningDetailDTO.ipPassword}&nbsp;</div>
				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">Usable IP:</div>
				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.provisioningDetailDTO.useableIp}&nbsp;</div>
				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">Gateway:</div>
				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.provisioningDetailDTO.gateway}&nbsp;</div>
				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">Subnet Mask:</div>
				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.provisioningDetailDTO.subnetMask}&nbsp;</div>
				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">Primary DNS:</div>
				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.provisioningDetailDTO.primaryDNS}&nbsp;</div>
				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">Seconday DNS:</div>
				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.provisioningDetailDTO.secondayDNS}&nbsp;</div>
				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">Circuit#:</div>
				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.provisioningDetailDTO.circuit}&nbsp;</div>
			</div>
			<div>
				<div style="margin-left: 270px;margin-bottom: 10px;" class="PCSSH0001">WUG Attributes</div>
				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">Help Desk #:</div>
				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.wugDetailsDTO.wugHelpDesk}&nbsp;</div>
				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">APC Address:</div>
				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.wugDetailsDTO.wugApcAddress}&nbsp;</div>
				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">Modem Type:</div>
				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.wugDetailsDTO.wugModemType}&nbsp;</div>
				<div style="margin-bottom: 5px;width: 100px;" class="divFloatLeft">Circuit IP:</div>
				<div style="margin-bottom: 5px;white-space: normal;word-wrap: break-word;overflow:hidden;">${hdTicketDTO.wugDetailsDTO.wugCircuitIp}&nbsp;</div>
			
			</div>
		</div>
		
		
	</div>
	<div style="margin-left: 49%;">
		<div class="PCSSH0001 divFloatLeft">Priority</div>
		<div style="text-align: right;margin-right: 20px;"><input type="button" value="Edit" class="FMBUT0003 disableEdit" onclick="openEditPrioritySection();"/></div>
		<div class="divFloatLeft" style="width: 50px;margin-top: 10px;">Priority:</div>
		<div class="divFloatLeft" style="width: 100px;margin-top: 10px;">${hdTicketDTO.priority}&nbsp;</div>
		<div class="divFloatLeft" style="width: 50px;margin-top: 10px;">Impact:</div>
		<div class="divFloatLeft" style="width: 100px;margin-top: 10px;">${hdTicketDTO.impact}&nbsp;</div>
		<div class="divFloatLeft" style="width: 55px;margin-top: 10px;">Urgency:</div>
		<div  style="width: 100px;margin-top: 10px;">${hdTicketDTO.urgency}&nbsp;</div>
		
		<div class="PCSSH0001 divFloatLeft" style="margin-top: 20px;">Backup Circuit Status</div>
		<div style="text-align: right;margin-right: 20px;margin-top: 20px;">
		&nbsp;
		<c:if test="${hdTicketDTO.ticketSource == '2' }">
			<input type="button" value="Edit" class="FMBUT0003" onclick="openEditBackUp();"/>
		</c:if>
		</div>
		<div>&nbsp;</div>
		<div class="divFloatLeft" style="width: 150px;">Device:</div>
		<div  style="width: 100px;">${hdTicketDTO.device}&nbsp;</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Status:</div>
		<div  style="width: 100px;margin-top: 5px;">${hdTicketDTO.backUpCktStatus}&nbsp;</div>
		
		<div class="PCSSH0001 divFloatLeft" style="margin-top: 20px;">Resolution</div>
		<div style="text-align: right;margin-right: 20px;margin-top: 20px;"><input type="button" value="Edit" class="FMBUT0003 disableEdit" onclick="openResolutionSection();"/></div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Point of Failure:</div>
		<div style="width: 100px;margin-top: 5px;">${hdTicketDTO.pointOfFailure}&nbsp;</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Resolution:</div>
		<div style="width: 100px;margin-top: 5px;">${hdTicketDTO.resolution}&nbsp;</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Root Cause:</div>
		<div style="width: 100px;margin-top: 5px;">${hdTicketDTO.rootCause}&nbsp;</div>
		
		<div class="PCSSH0001" style="margin-top: 20px;">ISP Dispatch</div>
		<div style=";margin-top: 5px;"><b>Check work notes for ISP dispatch requests and status if applicable</b></div>
		
		<div class="PCSSH0001" style="margin-top: 20px;">Contingent Dispatch</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Scheduled:</div>
		<div style="width: 100px;margin-top: 5px;">[Scheduled]</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Ops Contact:</div>
		<div style="width: 100px;margin-top: 5px;">[Ops Contact]</div>
		<div class="divFloatLeft" style="width: 150px;margin-top: 5px;">Partner:</div>
		<div style="width: 100px;margin-top: 5px;">[Partner Name]</div>
		
		<div class="PCSSH0001" style="margin-top: 20px;">Work Notes:</div>
		<div class="divFloatLeft" style="width: 100px;margin-top: 10px;text-align: center;"><b>Date</b></div>
		<div class="divFloatLeft" style="width: 100px;margin-top: 10px;text-align: center;"><b>Author</b></div>
		<div style="width: 350px;margin-top: 10px;text-align: center;margin-left: 200px;"><b>Note</b></div>
		
		<c:forEach items="${hdTicketDTO.hdWorkNoteDTOList }" var="workNotes">
			<div class="divFloatLeft"  style="width: 100px;margin-top: 10px;text-align: center;">${workNotes.workNoteDateStr }</div>
			<div class="divFloatLeft" style="width: 100px;margin-top: 10px;text-align: center;">${workNotes.userLastName }</div>
			<div style="margin-top: 10px;">
				<c:if test="${fn:length(workNotes.workNote) <= 240 }">
					<div style="width: 350px;margin-left: 200px;white-space: normal;">${workNotes.workNote }</div>
				</c:if>
				<c:if test="${fn:length(workNotes.workNote) > 240 }">
					<textarea rows="5" cols="100" style="width: 350px;font-size: 11px;font-family: Verdana, Arial, Helvetica, sans-serif;font-weight:normal;border: 1px solid #c9c2c1;border-top: 2px solid #999999;">${workNotes.workNote }</textarea>
				</c:if>
			</div>
		</c:forEach>
		<div>&nbsp;</div>
	</div>
</form:form>
</div>