<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<div class="WASTD0003 PCSTD0010" style="text-align: left;">
		<form id="ticketEffortsSaveForm">
			<input type="hidden" id="ticketId" name="ticketId" value="${ticketId}">
			 
			 <div  style="float: left;width: 90px;">Efforts<font style="color: red;">*</font>:</div>
	       	<div>
	       		<input type="text" style="width: 30px;" id="ticketSummaryEfforts"/>&nbsp;&nbsp;Minutes
	       	</div>
		    <div  style="float: left;width: 90px;margin-top: 10px;margin-bottom: 10px;">Work Notes<font style="color: red;">*</font>:</div>
		    
	       	<div style="margin-top: 10px;margin-bottom: 10px;">
	       		<textarea rows="5" cols="60" style="width: 300px;overflow-y: scroll;resize:none;" id="workNotesTicketSummary" spellcheck="true" ></textarea>
	       	</div>
	       	 		       	
       		<div style="text-align: right;">
       		 	<input type="button" value="Submit" onclick="recordTicketSummaryEfforts();"  class="FMBUT0003" style="margin-right:40px;"/>
       		</div>
       	</form>
       		
</div>
