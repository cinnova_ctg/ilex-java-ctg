<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/plugins/jquery-ui/base/ui.all.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>
<script type="text/javascript">

$(document).ready(function(){
	$("#requestedDate").datepicker({
		buttonImage: '${pageContext.request.contextPath}/resources/images/calendar.gif',
			showOn : "both",
			yearRange: "-10:+5" ,
			changeMonth: false,
			changeYear: false
	}).next(".ui-datepicker-trigger").addClass("calImageStyle");
	
	$("#preferredDate").datepicker({
		buttonImage: '${pageContext.request.contextPath}/resources/images/calendar.gif',
			showOn : "both",
			yearRange: "-10:+5" ,
			changeMonth: false,
			changeYear: false
	}).next(".ui-datepicker-trigger").addClass("calImageStyle");
	$("#windowFromDate").datepicker({
		buttonImage: '${pageContext.request.contextPath}/resources/images/calendar.gif',
			showOn : "both",
			yearRange: "-10:+5" ,
			changeMonth: false,
			changeYear: false
	}).next(".ui-datepicker-trigger").addClass("calImageStyle");
	$("#windowToDate").datepicker({
		buttonImage: '${pageContext.request.contextPath}/resources/images/calendar.gif',
			showOn : "both",
			yearRange: "-10:+5" ,
			changeMonth: false,
			changeYear: false
	}).next(".ui-datepicker-trigger").addClass("calImageStyle");
});



function setRequestorEmail()
	{
	var name = document.forms[0].requestor.value;
		if(name!=0){
			document.forms[0].requestorEmail.value = name.substring(name.indexOf("~", name)+1, name.length+1);
		}else if(name==0){
			document.forms[0].requestorEmail.value ="";
		}
	}
	
	
function validate(){
	if( document.forms[0].requestor.value == "0" ){
 		alert("Please Enter Requestor Name");
 		document.forms[0].requestor.focus();
		return false;
 	}
 	if( document.forms[0].requestorEmail.value == '' ){
		alert("Email should be in aa@bb.cc format");		
		document.forms[0].requestorEmail.focus();
		return false;
	}
	if( document.forms[0].requestType.value == '0' ){
			alert("Please select request type.");
			document.forms[0].requestType.focus();
		return false;
		}
		if( document.forms[0].criticality.value == '0' ){
			alert("Please Select Criticality");
			document.forms[0].criticality.focus();
		return false;
		}
 	if( document.forms[0].resource.value == '0' ){
 			alert("Please Select Resource Level");
 			document.forms[0].resource.focus();
 					return false;
		}
		if (document.forms[0].customerReference.value == '' ){
			alert("Please Enter Customer Reference");
			document.forms[0].customerReference.focus();
		return false;
		}
		
		if ($("#siteDetails\\.siteNumber").val() == '' ){
			alert("Please Enter Site Number");
			$("#siteDetails\\.siteNumber").focus();
		return false;
		}
		if ($("#siteDetails\\.siteAddress").val() == '' ){
			alert("Please Enter Site Address");
			$("#siteDetails\\.siteAddress").focus();
		return false;
		}
		if ($("#siteDetails\\.siteCity").val() == '' ){
			alert("Please Enter Site City");
			$("#siteDetails\\.siteCity").focus();
		return false;
		}
		
		if ($("#siteDetails\\.state").val() == '' ){
			alert("Please Enter Site State");
			$("#siteDetails\\.state").focus();
		return false;
		}
		if ($("#siteDetails\\.siteZipcode").val() == '' ){
			alert("Please Enter Site Zip code");
			$("#siteDetails\\.siteZipcode").focus();
		return false;
		}
		if ($("#siteDetails\\.primaryName").val() == '' ){
			alert("Please Enter Primary Contact Name");
			$("#siteDetails\\.primaryName").focus();
		return false;
		}
		if ($("#siteDetails\\.primaryEmail").val() == '' ){
			alert("Please Enter Primary Contact Email");
			$("#siteDetails\\.primaryEmail").focus();
		return false;
		}
		if ($("#siteDetails\\.sitePhone").val() == '' ){
			alert("Please Enter Primary Contact Phone");
			$("#siteDetails\\.sitePhone").focus();
		return false;
		}
		
	if( document.forms[0].requestedDate.value == ''){
			alert("Please Select Requested Date");
			document.forms[0].requestedDate.focus();
			return false;
		}
		if( document.forms[0].preferredDate.value == '' && (document.forms[0].windowFromDate.value == '' || document.forms[0].windowToDate.value == '')){
			if(document.forms[0].preferredDate.value == '' && document.forms[0].windowFromDate.value=='' && document.forms[0].windowToDate.value == ''){
			alert("Please Select Preferred Arrival Date");
			document.forms[0].preferredDate.focus();
		}else if(document.forms[0].windowFromDate.value == ''){
			alert("Please Select Arrival Window Start Date");
			document.forms[0].windowFromDate.focus();
		}else if(document.forms[0].windowToDate.value == ''){
			alert("Please Select Arrival Window End Date");
			document.forms[0].windowToDate.focus();
		}
		return false;
	}
			
		if( document.forms[0].problemCategory.value == '0' ){
			alert("Please Select Category");
			document.forms[0].problemCategory.focus();
		return false;
		}	
		
		if( document.forms[0].problemDescription.value == '' ){
			alert("Please Enter Problem Description");
			document.forms[0].problemDescription.focus();
		return false;
		}
 	return true;
}

function saveTicketInfo(){
	
	
	
	if(validate()){
		$("#siteDetails\\.state").removeAttr("disabled");
		$
		.ajax({
			type : 'POST',
			url : '${pageContext.request.contextPath}/newDispatch/saveWebTicketInfo.html?'+$("form").serialize(),
			success : function(data) {
				
				if(data=='success')
				{
					removeSavedTicket('${newDispatchDTO.jobId}');
					closeWindow3();
					
					
				}else{
					$("#errorDiv").html(data);
				}
			}
			});
	}
	
	
	
}

function getReourcesCombo(criticalityObj,appendixId){
		$
		.ajax({
			type : 'GET',
			url : '${pageContext.request.contextPath}/newDispatch/getResourceList.html?appendixId='+appendixId+'&criticalityId='+criticalityObj.value,
			success : function(data) {
				var resourceCombo='';
				var obj = $.parseJSON(data);
				$
				.each(
						obj,
						function(index, resource) {
							if(resource.resName!=' '){
								resourceCombo+=' <optgroup label="'+resource.resName+'">';
							}
							
							$.each(resource.resourceList,function(index,options){
								resourceCombo+='<option value="'+options.value +'" >'+options.label +'</option>';
							})
							if(resource.resName!=' '){
								resourceCombo+='</optgroup>';
							}
							
							
							
						});
				
				
				$("#resource").html(resourceCombo);
				$("#resource").addClass("comboo");
				
				
			}
			});
	
}

function setSiteDetails(siteDetails){
	
	$("#siteDetails\\.siteNumber").val(siteDetails.siteNumber);
	$("#siteDetails\\.siteAddress").val(siteDetails.siteAddress);
	$("#siteDetails\\.siteCity").val(siteDetails.siteCity);
	$("#siteDetails\\.state").val(siteDetails.state);
	$("#siteDetails\\.siteZipcode").val(siteDetails.siteZipcode);
	$("#siteDetails\\.primaryName").val(siteDetails.primaryName);
	$("#siteDetails\\.primaryEmail").val(siteDetails.primaryEmail);
	$("#siteDetails\\.sitePhone").val(siteDetails.sitePhone);
	$("#siteDetails\\.secondaryName").val(siteDetails.secondaryName);
	$("#siteDetails\\.secondaryEmail").val(siteDetails.secondaryEmail);
	$("#siteDetails\\.secondaryPhone").val(siteDetails.secondaryPhone);
	
	$("#siteDetails\\.siteId").val(siteDetails.siteId);
	$("#siteDetails\\.siteListId").val(siteDetails.siteListId);
	$("#siteDetails\\.jobId").val(siteDetails.jobId);
	$("#siteDetails\\.siteName").val(siteDetails.siteName);
	$("#siteDetails\\.siteDesignator").val(siteDetails.siteDesignator);
	$("#siteDetails\\.siteWorkLocation").val(siteDetails.siteWorkLocation);
	$("#siteDetails\\.country").val(siteDetails.country);
	$("#siteDetails\\.siteNotes").val(siteDetails.siteNotes);
	$("#siteDetails\\.siteDirection").val(siteDetails.siteDirection);
	$("#siteDetails\\.localityUplift").val(siteDetails.localityUplift);
	$("#siteDetails\\.unionSite").val(siteDetails.unionSite);
	$("#siteDetails\\.appendixName").val(siteDetails.appendixName);
	$("#siteDetails\\.jobName").val(siteDetails.jobName);
	$("#siteDetails\\.nettype").val(siteDetails.nettype);
	$("#siteDetails\\.viewjobtype").val(siteDetails.viewjobtype);
	$("#siteDetails\\.ownerId").val(siteDetails.ownerId);
	$("#siteDetails\\.addendumId").val(siteDetails.addendumId);
	$("#siteDetails\\.siteLatDeg").val(siteDetails.siteLatDeg);
	$("#siteDetails\\.siteLatMin").val(siteDetails.siteLatMin);
	$("#siteDetails\\.siteLatDirection").val(siteDetails.siteLatDirection);
	$("#siteDetails\\.siteLonDeg").val(siteDetails.siteLonDeg);
	$("#siteDetails\\.siteLonMin").val(siteDetails.siteLonMin);
	$("#siteDetails\\.siteLonDirection").val(siteDetails.siteLonDirection);
	$("#siteDetails\\.siteStatus").val(siteDetails.siteStatus);
	$("#siteDetails\\.siteCategory").val(siteDetails.siteCategory);
	$("#siteDetails\\.siteRegion").val(siteDetails.siteRegion);
	$("#siteDetails\\.siteGroup").val(siteDetails.siteGroup);
	$("#siteDetails\\.siteEndCustomer").val(siteDetails.siteEndCustomer);
	$("#siteDetails\\.siteTimeZone").val(siteDetails.siteTimeZone);
	$("#siteDetails\\.siteCountryId").val(siteDetails.siteCountryId);
	$("#siteDetails\\.siteCountryIdName").val(siteDetails.siteCountryIdName);
	$("#siteDetails\\.siteLatLon").val(siteDetails.siteLatLon);
	
}
	



</script>
 
<div 	style="padding:20px;margin-left:0px;padding-top: 0px;">
<form:form commandName="newDispatchDTO">


<form:hidden path="jobId"/>
<form:hidden path="appendixId"/>
<form:hidden path="ticketId"/>
<form:hidden path="ticketNumber"/>
<form:hidden path="msp"/>
<form:hidden path="helpDesk"/>


<div id="errorDiv" class="MSERR0001" style="text-align: left;padding-bottom: 5px;"></div>
<div class="PCSTD0011" style="text-align: left;margin-bottom: 20px;">Contact and Request Type</div>

<div style="float: left;">
		<table style="text-align: left; border-right: 1px solid grey;padding-right: 30px;" >
				<tr>
					<td class="PCSTD0010">Requestor <font color="red">*</font>:</td>
					<td>
						<form:select path="requestor" onchange="setRequestorEmail();" cssClass="comboo">
							<c:forEach items="${newDispatchDTO.requestors}" var="requestor">
								<option value="${requestor.value}" <c:if test="${newDispatchDTO.requestor eq requestor.value}">selected="selected"</c:if>>${requestor.label}</option>
							</c:forEach>
						</form:select>
					</td>
				</tr>
				<tr>
					<td class="PCSTD0010" >Email:</td>
					<td>
					<form:input  path="requestorEmail"  cssClass="PCTXT0004"/>
					</td>
				</tr>
				<tr>
					<td class="PCSTD0010" >Company:</td>
					<td>
					<input type="text" disabled="disabled" class="PCTXT0004"/>
					</td>
				</tr>
				<tr>
					<td class="PCSTD0010"  colspan="2">Customer Reference / PO #<font color="red">*</font></td>
					
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><form:input path="customerReference" class="PCTXT0004"/></td>
					
				</tr>
		</table>
</div>

<div >
<table style="text-align: left; padding-left: 30px;" >
				<tr>
					<td class="PCSTD0010">Contact:</td>
					<td>
					<input type="text" disabled="disabled" class="PCTXT0004"/></td>
				</tr>
				<tr>
					<td class="PCSTD0010" >Phone:</td>
					<td>
					<input type="text" disabled="disabled" class="PCTXT0004"/>
					</td>
				</tr>
				<tr>
					<td class="PCSTD0010"  colspan="2">Type Of Request<font color="red">*</font>:</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><select name="requestType" class="comboo">
						<c:forEach items="${newDispatchDTO.requestTypes}" var="requestType">
							<option value="${requestType.value}" 
							<c:if test="${newDispatchDTO.requestType ==requestType.value}">selected="selected"</c:if>
							>${requestType.label}</option>
						</c:forEach>
					 <select>
					</td>
				</tr>
				<tr>
					<td class="PCSTD0010">Stand By:</td>
					<td>
					<input type="checkbox" name="standBy" <c:if test="${newDispatchDTO.standBy ==true}">checked="checked"</c:if>  />
					</td>
				</tr>
		</table>

</div>
<div style="border-bottom: 1px solid grey;">&nbsp;</div>
<div style="float: left;">

<table style="text-align: left;padding-right: 30px;margin-top: 10px;border-right: 1px solid grey;" >
				<tr>
					<td colspan="2" class="PCSTD0011">Schedule And Resources</td>
				</tr>
				<tr>
					<td class="PCSTD0010">Received :</td>
					<td class="PCSTD0010" colspan="3">
					<c:if test='${not empty newDispatchDTO.receivedDate}'><c:out value='${newDispatchDTO.receivedDate}'/> 
												<c:if test='${not empty newDispatchDTO.receivedHour}'><c:out value='${newDispatchDTO.receivedHour}'/>:<c:out value='${newDispatchDTO.receivedMinute}'/> <c:out value='${newDispatchDTO.receivedIsAm}'/></c:if></c:if>
												<c:if test='${empty newDispatchDTO.receivedDate}'>No value</c:if>
					</td>
				</tr>
				
				<tr>
					<td class="PCSTD0010">Submitted<font color="red">*</font> :</td>
					
					<td class="PCSTD0010"><form:input path="requestedDate" cssClass="textbox" size="10"  value="${newDispatchDTO.requestedDate}"/></td>
   					<td class="PCSTD0010">
   						<form:select path="requestedHour" cssClass="comboo">
		   						<c:forEach items="${newDispatchDTO.requestedHourList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${newDispatchDTO.requestedHour eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select>
   						<form:select path="requestedMinute" cssClass="comboo">
		   						<c:forEach items="${newDispatchDTO.requestedMinuteList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${newDispatchDTO.requestedMinute eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select>
   					</td>
   					<td class="PCSTD0010">
   						<input type="radio" name="requestedIsAm" value="AM" <c:if test="${newDispatchDTO.requestedIsAm eq 'AM'}">checked="checked"</c:if> >AM
   						<input type="radio" name="requestedIsAm" value="PM" <c:if test="${newDispatchDTO.requestedIsAm eq 'PM'}">checked="checked"</c:if>>PM
   					</td>
				   					
				  
				</tr>
				<tr>
					<td class="PCSTD0010">Prefered<font color="red">*</font> :</td>
   					<td class="PCSTD0010"><form:input path="preferredDate" cssClass="textbox" size="10"   value="${newDispatchDTO.preferredDate}"/></td>
   					<td class="PCSTD0010">
   						<form:select path="preferredHour" cssClass="comboo">
		   						<c:forEach items="${newDispatchDTO.preferredHourList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${newDispatchDTO.preferredHour eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select>
   						<form:select path="preferredMinute" cssClass="comboo">
		   						<c:forEach items="${newDispatchDTO.preferredMinuteList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${newDispatchDTO.preferredMinute eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select>
   					</td>
   					<td class="PCSTD0010">
   						<input type="radio" name="preferredIsAm" value="AM" <c:if test="${newDispatchDTO.preferredIsAm eq 'AM'}">checked="checked"</c:if> >AM
   						<input type="radio" name="preferredIsAm" value="PM" <c:if test="${newDispatchDTO.preferredIsAm eq 'PM'}">checked="checked"</c:if>>PM
   					</td>
				</tr>
				<tr>
					<td>or</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="PCSTD0010">Window :</td>
   					<td class="PCSTD0010"><form:input path="windowFromDate" cssClass="textbox" size="10"   value="${newDispatchDTO.windowFromDate}"/></td>
   					<td class="PCSTD0010">
   						<form:select path="windowFromHour" cssClass="comboo">
		   						<c:forEach items="${newDispatchDTO.windowFromHourList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${newDispatchDTO.windowFromHour eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select>
   						<form:select path="windowFromMinute" cssClass="comboo" >
		   						<c:forEach items="${newDispatchDTO.windowFromMinuteList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${newDispatchDTO.windowFromMinute eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select>
   					</td>
   					<td class="PCSTD0010">
   						<input type="radio" name="windowFromIsAm" value="AM" <c:if test="${newDispatchDTO.windowFromIsAm eq 'AM'}">checked="checked"</c:if> >AM
   						<input type="radio" name="windowFromIsAm" value="PM" <c:if test="${newDispatchDTO.windowFromIsAm eq 'PM'}">checked="checked"</c:if>>PM
   					</td>
				</tr>
				<tr>
					<td class="PCSTD0010">&nbsp;</td>
					<td class="PCSTD0010"><form:input path="windowToDate"  value="${newDispatchDTO.windowToDate}" cssClass="textbox" size="10" /></td>
   					<td class="PCSTD0010">
   						<form:select path="windowToHour" cssClass="comboo">
		   						<c:forEach items="${newDispatchDTO.windowToHourList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${newDispatchDTO.windowToHour eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select>
   						<form:select path="windowToMinute" cssClass="comboo">
		   						<c:forEach items="${newDispatchDTO.windowToMinuteList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${newDispatchDTO.windowToMinute eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select>
   					</td>
   					<td class="PCSTD0010">
   						<input type="radio" name="windowToIsAm" value="AM" <c:if test="${newDispatchDTO.windowToIsAm eq 'AM'}">checked="checked"</c:if> >AM
   						<input type="radio" name="windowToIsAm" value="PM" <c:if test="${newDispatchDTO.windowToIsAm eq 'PM'}">checked="checked"</c:if>>PM
   					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="PCSTD0010" colspan="3">(All times local site times)</td>
				</tr>
				
				
		</table>

</div>
<div>
	<table style="text-align: left;padding-left: 30px;" >
				<tr>
					<td class="PCSTD0010"  colspan="2">Criticality<font color="red">*</font> :</td>
				</tr>
				<tr>
					<td class="PCSTD0010"  colspan="2">
						<form:select path="criticality" cssClass="comboo"  cssStyle="min-width:150px;" onchange="getReourcesCombo(this,'${newDispatchDTO.appendixId}');">
						<c:forEach items="${newDispatchDTO.criticalities}" var="criticality">
								<option value="${criticality.value}" <c:if test="${newDispatchDTO.criticality eq criticality.value}">selected="selected"</c:if>>${criticality.label}</option>
							</c:forEach>
						</form:select>
					</td>
				</tr>
				<tr>
					<td class="PCSTD0010">Elapsed Time :</td>
					<td class="PCSTD0010">&nbsp;</td>
				</tr>
				<tr>
					<td class="PCSTD0010">Time of Arrival Onsite :</td>
					<td class="PCSTD0010">${newDispatchDTO.arrivalDate}</td>
				</tr>
				<tr>
					<td class="PCSTD0010"  colspan="2">Period of Performance<font color="red">*</font> :</td>
				</tr>
				<tr>
					<td class="PCSTD0010"><input type="radio" name="pps" <c:if test="${newDispatchDTO.pps==true}">checked="checked"</c:if>>PPS</td>
					<td class="PCSTD0010"><input type="radio" name="pps" <c:if test="${newDispatchDTO.pps==false}">checked="checked"</c:if>>Non-PPS</td>
				</tr>
				<tr>
					<td class="PCSTD0010"  colspan="2">Resources<font color="red">*</font> :</td>
				</tr>
				<tr>
					<td class="PCSTD0011"  colspan="2">
					<form:select path="resource" cssClass="comboo" cssStyle="min-width:150px;">
						<c:forEach items="${resourceList}" var="optionGroup">
						<c:if test="${optionGroup.resName!=' '}">
							<optgroup label="${optionGroup.resName}">
								<c:forEach items="${optionGroup.resourceList}" var="options">
									<option value="${options.value}" <c:if test="${newDispatchDTO.resource eq options.value}">selected="selected"</c:if> >${options.label}</option>
								</c:forEach>
							</optgroup>
						</c:if>
						<c:if test="${optionGroup.resName==' '}">
								<c:forEach items="${optionGroup.resourceList}" var="options">
									<option value="${options.value}" <c:if test="${newDispatchDTO.resource eq options.value}">selected="selected"</c:if> >${options.label}</option>
								</c:forEach>
						</c:if>
						</c:forEach>
						
					</form:select>
					</td>
				</tr>
	</table>
</div>


<div style="border-bottom: 1px solid grey;margin-top: 40px;">&nbsp;</div>
<div class="PCSTD0011" style="text-align: left;margin-bottom: 10px;margin-top: 10px;">Nature Of Request</div>
<div style="float: left;">

<table style="text-align: left;padding-right: 30px;" >
				<tr>
					<td class="PCSTD0010">Category<font color="red">*</font> :</td>
					<td>
					<form:select path="problemCategory" cssClass="comboo">
						<c:forEach items="${newDispatchDTO.problemCategories}" var="problemCategory">
								<option value="${problemCategory.value}" <c:if test="${newDispatchDTO.problemCategory eq problemCategory.value}">selected="selected"</c:if> >${problemCategory.label}</option>
							</c:forEach>
						</form:select>
					</td>
				</tr>
				
				<tr>
					<td class="PCSTD0010"  colspan="2">Description<font color="red">*</font> :</td>
				</tr>
				<tr>
					<td class="PCSTD0010"  colspan="2">
					<form:textarea path="problemDescription"  rows="15" cols="45" style="resize: none;height: 100px;overflow-y:scroll; " />
					  </td>
				</tr>
				
		</table>

</div>
<div>
	<table style="text-align: left;padding-left: 30px;" >
				<tr>
					<td class="PCSTD0010"  colspan="2">Ticket Rules :</td>
				</tr>
				<tr>
					<td class="PCSTD0010"  colspan="2"><form:textarea  path="ticketRules" rows="15" cols="30" style="resize: none;height: 50px;overflow-y:scroll; "></form:textarea></td>
				</tr>
				<tr>
					<td class="PCSTD0010"  colspan="2">Special Instructions :</td>
				</tr>
				<tr>
					<td class="PCSTD0010"  colspan="2"><form:textarea  path="specialInstructions" rows="15" cols="30" style="resize: none;height: 50px;overflow-y:scroll; "></form:textarea></textarea>  </td>
				</tr>
	</table>
</div>
<div style="border-bottom: 1px solid grey;margin-top: 20px;">&nbsp;</div>
<div class="PCSTD0011" style="text-align: left;margin-bottom: 10px;margin-top: 10px;">Site</div>
	<div>
		<table style="text-align: left;" id="siteDetailTable">
				<tr>
					<td class="PCSTD0010" >Site Number<font color="red">*</font> :</td>
					<td colspan="4">
					<form:hidden path="siteDetails.siteId"/>
					<form:hidden path="siteDetails.siteListId"/>
					<form:hidden path="siteDetails.jobId"/>
					<form:hidden path="siteDetails.siteName"/>
					<form:hidden path="siteDetails.siteDesignator"/>
					<form:hidden path="siteDetails.siteWorkLocation"/>
					<form:hidden path="siteDetails.country"/>
					<form:hidden path="siteDetails.siteNotes"/>
					<form:hidden path="siteDetails.siteDirection"/>
					<form:hidden path="siteDetails.localityUplift"/>
					<form:hidden path="siteDetails.unionSite"/>
					<form:hidden path="siteDetails.appendixName"/>
					<form:hidden path="siteDetails.jobName"/>
					<form:hidden path="siteDetails.nettype"/>
					<form:hidden path="siteDetails.viewjobtype"/>
					<form:hidden path="siteDetails.ownerId"/>
					<form:hidden path="siteDetails.addendumId"/>
					<form:hidden path="siteDetails.siteLatDeg"/>
					<form:hidden path="siteDetails.siteLatMin"/>
					<form:hidden path="siteDetails.siteLatDirection"/>
					<form:hidden path="siteDetails.siteLonDeg"/>
					<form:hidden path="siteDetails.siteLonMin"/>
					<form:hidden path="siteDetails.siteLonDirection"/>
					<form:hidden path="siteDetails.siteStatus"/>
					<form:hidden path="siteDetails.siteCategory"/>
					<form:hidden path="siteDetails.siteRegion"/>
					<form:hidden path="siteDetails.siteGroup"/>
					<form:hidden path="siteDetails.siteEndCustomer"/>
					<form:hidden path="siteDetails.siteTimeZone"/>
					<form:hidden path="siteDetails.siteCountryId"/>
					<form:hidden path="siteDetails.siteCountryIdName"/>
					<form:hidden path="siteDetails.siteLatLon"/>
					
					<form:input path="siteDetails.siteNumber" cssClass="PCTXT0004" readonly="true"/></td>
					<td colspan="4" style="text-align: right;"><input type="button" id="siteSearchLink" class="FMBUT0003" onclick="newDispatchSearchSite('${newDispatchDTO.jobId}','${newDispatchDTO.appendixId}','${newDispatchDTO.siteDetails.siteId}')" value="Find Site"/>  </td>
				</tr>
				<tr>
					<td class="PCSTD0010" >Address<font color="red">*</font> :</td>
					<td colspan="5"><form:input path="siteDetails.siteAddress" cssClass="PCTXT0007" readonly="true"/>  </td>
				</tr>
				<tr>
					<td class="PCSTD0010" >City<font color="red">*</font> :</td>
					<td><form:input path="siteDetails.siteCity" cssClass="PCTXT0004" readonly="true" />  </td>
					<td class="PCSTD0010" style="width: 40px;" >State<font color="red">*</font> :</td>
					<td>
						<form:select path="siteDetails.state" cssClass="comboo" cssStyle="text-align: left;" disabled="true">
							<c:forEach items="${states}" var="states" varStatus="index" >
		   							<option value="<c:out value='${states.value}'/>" <c:if test="${newDispatchDTO.siteDetails.state eq states.value}">selected="selected"</c:if>><c:out value='${states.label}'/></option>
		   						</c:forEach>
						</form:select>
					</td>
					<td class="PCSTD0010" >ZipCode<font color="red">*</font> :</td>
					<td><form:input path="siteDetails.siteZipcode"  cssClass="PCTXT0008" readonly="true" />  </td>
				</tr>
				<tr>
					<td class="PCSTD0010" >Contact</td>
					<td class="PCSTD0010" >Name </td>
					<td class="PCSTD0010" colspan="2" >Email</td>
					<td class="PCSTD0010" colspan="2" >Phone Number</td>
				</tr>
				<tr>
				<td class="PCSTD0010" >Primary<font color="red">*</font> :</td>
					<td class="PCSTD0010" ><form:input path="siteDetails.primaryName" cssClass="PCTXT0004"/>  </td>
					<td class="PCSTD0010" colspan="2" ><form:input path="siteDetails.primaryEmail" cssClass="PCTXT0004"/> </td>
					<td class="PCSTD0010" colspan="2" ><form:input path="siteDetails.sitePhone" cssClass="PCTXT0004"/> </td>
				</tr>
				<tr>
					<td class="PCSTD0010" >Secondary :</td>
					<td class="PCSTD0010" ><form:input path="siteDetails.secondaryName" cssClass="PCTXT0004"/> </td>
					<td class="PCSTD0010" colspan="2" ><form:input path="siteDetails.secondaryEmail" cssClass="PCTXT0004"/></td>
					<td class="PCSTD0010" colspan="2" ><form:input path="siteDetails.secondaryPhone" cssClass="PCTXT0004"/></td>
				</tr>
				
		</table>
	</div>
	
	<div style="border-bottom: 1px solid grey;margin-top: 20px;">&nbsp;</div>
	<div class="PCSTD0011" style="text-align: left;margin-bottom: 10px;margin-top: 10px;">Other Request Comments</div>
	<div>
		<table style="text-align: left;" >
					<tr>
						<td class="PCSTD0010">Customer Specific Details :</td>
					</tr>
					<tr>
						<td class="PCSTD0010">
						<div style="height: 100px;width: 760px;overflow-y: auto;border: 1px solid black;background-color: #ebebe4;padding: 2px;">${newDispatchDTO.customerSpecificDetails}</div>
					</tr>
					<tr>
						<td class="PCSTD0010">Request Summmary :</td>
					</tr>
					<tr>
						<td class="PCSTD0010">
						<div style="height: 100px;width:760px;overflow-y: auto;border: 1px solid black;background-color: #ebebe4;padding: 2px;">${newDispatchDTO.requestSummary}</div>
						</td>
					</tr>
					<tr>
						<td class="PCSTD0010" style="text-align: right;padding-top: 10px;"><input type="button" class="FMBUT0003" value="Save/Receive" onclick="saveTicketInfo();"/> 
						</td>
					</tr>
		</table>
	</div>

</form:form>
</div>