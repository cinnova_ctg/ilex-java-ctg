<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/content.css"/>

<script>
function searchSite(){
	$("#siteSearchPopUpBody").load(
			'/Ilex-WS/searchResult.html?'+$("form").serialize()+"&siteSearch=search");
	
}

function assignSite(siteListId){
	$
	.ajax({
		type : 'GET',
		url : '/Ilex-WS/newDispatch/assignJobSite.html?siteListId='+siteListId+'&siteId=${siteSearchDTO.siteId}&jobId=${siteSearchDTO.jobId}',
		success : function(data) {
			if(data=='success')
			{
				$
				.ajax({
					type : 'GET',
					url : '/Ilex-WS/newDispatch/jobSiteDetails.html?jobId=${siteSearchDTO.jobId}',
					success : function(data) {
						
						
						var obj = $.parseJSON(data);
						setSiteDetails(obj);
						closeWindow4();
					}
					
				});
				
				
				
				
			}
		}
	});
//closeWindow4();
}
</script>

<style>
table#siteSearchTable  tbody tr:nth-child(2n+1){
font-family:Verdana, Arial, Helvetica, sans-serif; white-space:nowrap;  font-size: 11px;padding: 2px 5px 2px 5px;color: #000000;background: #f9f9f9;

}
table#siteSearchTable  tbody tr:nth-child(2n+2){
	 font-family:Verdana, Arial, Helvetica, sans-serif;  white-space:nowrap; font-size: 11px; padding: 2px 5px 2px 5px;color: #000000; background: #ececec;
}

</style>

<form:form commandName="siteSearchDTO">
<div style="padding:20px;margin-left:0px;">
 
<table border="0" cellspacing="1" cellpadding="1"  style="text-align: left;"> 
  
  <form:hidden path="siteId"/>
  <tr> 
    <td  class = "colDark"  >Site Name</td>
	<td  class = "colLight" ><form:input path="siteName" cssClass="text" size="20"  maxlength="50"/></td>
	<td  class = "colDark"  >Site Number</td>
	<td  class = "colLight" ><form:input path="siteNumber" cssClass="text" size="20"  maxlength="50"/></td>
    
    <td class="colDark"  >Site City</td>
	<td class="colLight" ><form:input path="siteCity" cssClass="text" size="20"  maxlength="50"/></td>
	

	<td  class="colDark" >Site Brand</td>
	<td  class="colLight">
		<form:select path="siteBrand"  cssClass="select">
			<c:forEach items="${siteSearchDTO.siteBrands}" var="brand">
				<option value="${brand.value}" <c:if test="${siteSearchDTO.siteBrand eq brand.value}">selected="selected"</c:if>>${brand.label}</option>
			</c:forEach>
		</form:select>
		
	</td>
	</tr>
	<tr>
	<td  class="colDark"  >End Customer</td>
	<td  class="colLight">
			<form:select path="siteEndCustomer"  cssClass="select">
			<c:forEach items="${siteSearchDTO.siteEndCustomers}" var="customer">
				<option value="${customer.value}" <c:if test="${siteSearchDTO.siteEndCustomer eq customer.value}">selected="selected"</c:if>>${customer.label}</option>
			</c:forEach>
		</form:select>
	</td>
    <td  class="colDark" >Site State</td>
	<td  class="colLight" colspan="5">
			<form:select path="siteState"  cssClass="select">
		   			<option value=" ">--Select--</option>
		   			<c:forEach items="${siteSearchDTO.statesMap}" var="entry">
		   				<optgroup label="${entry.key}">
		   				<c:forEach items="${entry.value}" var="options">
									<option value="${options.value}" <c:if test="${siteSearchDTO.siteState eq options.value}">selected="selected"</c:if> >${options.label}</option>
						</c:forEach>
		   				</optgroup>
					</c:forEach>
			</form:select>
	
			
	</td>
 </tr>
  
  
  <tr> 
	<td  class="colLight" colspan="8" align="center">
			<input type="button" class="button_c" onclick="return searchSite();" value="Search" ></input>
			<input type="reset"  class="button_c" value="Reset"/>
	</td>
  </tr>
  
  </table>
  <c:if test="${(not empty siteSearchDTO.siteSearchDetails) or (param.siteSearch=='search')}">
  
  <table  border="0" cellspacing="1" cellpadding="1" id="siteSearchTable" style="text-align: left;">
  <thead>
  	<tr>
    	<td  width="100%" colspan="7"><h1>Search Results</h1></td>
  	</tr>
  	<tr  height="20"> 
    <td  class="Ntryb"></td>
    <td class="Ntryb">Site Name</td>
    <td class="Ntryb"  >Site Number</td>
    <td class="Ntryb" >Site Address</td>
	<td class="Ntryb" >Site City</td>
    <td class="Ntryb" >Site State</td>
	<td class="Ntryb" >ZIP Code</td>
    <td class="Ntryb" >Brand</td>
    <td class="Ntryb" >End Customer</td>
  </tr>
  </thead>
  <tbody>
  <c:forEach items="${siteSearchDTO.siteSearchDetails}" var="siteSearchDetails">
   <tr>
    	<td    height="20"><input type="radio" name="selectedSiteListId" value="${siteSearchDetails.siteListId}" onclick="assignSite('${siteSearchDetails.siteListId}');">  </td>  
	 	<td   height="20">${siteSearchDetails.siteName}</td>
	 	<td   height="20">${siteSearchDetails.siteNumber}</td>
	 	<td   height="20">${siteSearchDetails.siteAddress}</td>
	 	<td   height="20">${siteSearchDetails.siteCity}</td>
		<td   height="20">${siteSearchDetails.siteState}</td>
		<td   height="20">${siteSearchDetails.siteZipCode}</td>
	  	<td   height="20">${siteSearchDetails.siteBrand }</td>
	  	<td   height="20">${siteSearchDetails.siteEndCustomer}</td>
	  </tr> 
	 </c:forEach> 
	 <c:if test="${empty siteSearchDTO.siteSearchDetails}">
	 <tr height="20"><td colspan="8">No Site Found</td></tr>
	 </c:if>
  </tbody>
  </table>
  </c:if>
  
  </div>
</form:form>  