<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/plugins/jquery-ui/base/ui.all.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style_common.css"/>

<script>
$(function(){
	
	$.getJSON("/Ilex-WS/service/dispatch/getRequestors?msaId=239", function(data) {
		var requestorCombo='';
		$.each(data,
				function(index, requestor) {
					requestorCombo+='<option value="'+requestor.value +'" >'+requestor.label +'</option>';
				});
		$("#requestorName").html(requestorCombo);
	});
	
	
});
$("#preferredDate").datepicker({
	buttonImage: '${pageContext.request.contextPath}/resources/images/calendar.gif',
		showOn : "both",
		yearRange: "-10:+5" ,
		changeMonth: false,
		changeYear: false
}).next(".ui-datepicker-trigger").addClass("calImageStyle");

</script>
<style>
table
{
    border-collapse: collapse; /* 'cellspacing' equivalent */
}
table td, table th
{
    padding: 5px; /* 'cellpadding' equivalent */
}


</style>

<form:form commandName="ticketDispatchDTO">
<div class="WASTD0001">
<table style="text-align: left;" class="PCSTD0010">
	<thead >
	<tr><th colspan="2" class="PCSTD0011" style="text-align: left;background-color: #d7e1eb"> NetMedX Dispatch Form</th></tr>
	</thead>

	<tr>
		<td class="PCSTD0010 TBSTD0004">Name:</td>
		<td><form:input path="userName"  cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Company Name:</td>
		<td><form:input path="companyName" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Email:</td>
		<td><form:input path="email" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Send a copy to above: </td>
		<td><form:checkbox path="sendEmail"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Contact Person:</td>
		<td><form:select path="requestorName" cssClass="comboo2">
		</form:select></td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Contact Person Phone:</td>
		<td><form:input path="requestorPhone" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Site Number:</td>
		<td><form:input path="siteNumber" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Site Phone:</td>
		<td><form:input path="sitePhone" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Site Location:</td>
		<td><form:input path="siteLocation" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Site Address:</td>
		<td><form:input path="siteAddress" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Site City:</td>
		<td><form:input path="siteCity" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Country:</td>
		<td><form:input path="siteCountry" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Site State:</td>
		<td><form:input path="siteState" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Site ZIP Code:</td>
		<td><form:input path="siteZipCode" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">PO Number:</td>
		<td><form:input path="poNumber" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">On-Site Field Request:</td>
		<td><form:checkbox path="onsiteFieldRequest" />   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Remote Help Desk Request:</td>
		<td><form:checkbox path="remoteHelpDeskRequest" />   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Preferred Arrival Date:</td>
		<td><form:input path="preferredDate" cssClass="textbox" size="10"/></td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Preferred Arrival Time:</td>
		<td>
						<form:select path="preferredHour" cssClass="comboo1">
		   						<c:forEach items="${preferredHourList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${ticketDispatchDTO.preferredHour eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select>
   						<form:select path="preferredMinute" cssClass="comboo1">
		   						<c:forEach items="${preferredMinuteList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${ticketDispatchDTO.preferredMinute eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select> 
   						<form:select path="preferredIsAm" cssClass="comboo1">
		   					<option>AM</option>
		   					<option>PM</option>
   						</form:select> 
   		</td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Call Criticality:</td>
		<td><form:select path="callCriticality" cssClass="comboo2">
		</form:select>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Preferred Arrival Window:</td>
		<td>Between</td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">&nbsp;</td>
		<td><form:select path="windowFromHour" cssClass="comboo1">
		   						<c:forEach items="${windowFromHourList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${ticketDispatchDTO.preferredHour eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select>
   						<form:select path="windowFromMinute" cssClass="comboo1">
		   						<c:forEach items="${windowFromMinuteList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${ticketDispatchDTO.preferredMinute eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select> 
   						<form:select path="windowFromIsAm" cssClass="comboo1">
		   					<option>AM</option>
		   					<option>PM</option>
   						</form:select> </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">&nbsp;</td>
		<td>And</td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">&nbsp;</td>
		<td><form:select path="windowToHour" cssClass="comboo1">
		   						<c:forEach items="${windowToHourList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${ticketDispatchDTO.preferredHour eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select>
   						<form:select path="windowToMinute" cssClass="comboo1">
		   						<c:forEach items="${windowToMinuteList}" var="requestor" varStatus="index" >
		   							<option value="<c:out value='${requestor.value}'/>" <c:if test="${ticketDispatchDTO.preferredMinute eq requestor.value}">selected="selected"</c:if>><c:out value='${requestor.label}'/></option>
		   						</c:forEach>
   						</form:select> 
   						<form:select path="windowToIsAm" cssClass="comboo1">
		   					<option>AM</option>
		   					<option>PM</option>
   						</form:select> </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004" style="vertical-align: top;">Summary of Problem:</td>
		<td><form:textarea path="problemSummary" cols="25" rows="5" /></td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004" style="vertical-align: top;">Special Instructions:</td>
		<td><form:textarea path="specialInstructions" cols="25" rows="5" /></td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Primary Point of Contact on Site:</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Name:</td>
		<td><form:input path="pocName" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Phone:</td>
		<td><form:input path="pocPhone" cssClass="PCTXT0007"/>   </td>
	</tr>
		<tr>
		<td class="PCSTD0010 TBSTD0004">Secondary Point of Contact on Site:</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Name:</td>
		<td><form:input path="pocName2" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Phone:</td>
		<td><form:input path="pocPhone2" cssClass="PCTXT0007"/>   </td>
	</tr>
	<tr>
		<td class="PCSTD0010 TBSTD0004">Stand By: </td>
		<td><form:checkbox path="standBy"/>   </td>
	</tr>
		<tr>
		<td class="PCSTD0010 TBSTD0004">File Attachment 1: </td>
		<td><form:input path="fileAttachment1" type="file"/> </td>
	</tr>
		<tr>
		<td class="PCSTD0010 TBSTD0004">File Attachment 2: </td>
		<td><form:input path="fileAttachment2" type="file"/>   </td>
	</tr>
		<tr>
		<td class="PCSTD0010 TBSTD0004">File Attachment 3: </td>
		<td><form:input path="fileAttachment3" type="file"/>   </td>
	</tr>
	
</table>
</div>
</form:form>