/*
 * jquery-impspeller.js - use impromptu to create a spell checker
 * By: Trent Richardson [http://trentrichardson.com]
 * 
 * Copyright 2010 Trent Richardson
 * Dual licensed under the MIT and GPL licenses.
 * http://trentrichardson.com/Impromptu/GPL-LICENSE.txt
 * http://trentrichardson.com/Impromptu/MIT-LICENSE.txt
 * 
 */
(function($) {
	$.impspeller = function(text, options){
		options = $.extend({},$.impspeller.defaults,options);

		var wrapper = '<div class="imp-spellcheck"><div id="misspelledcontent" class="misspelledcontent">'+ text +'</div></div>';
		
		var impoptions = {
			buttons: { Save: true, Cancel: false },
			submit: function(v,m,f){
				options.doneCheckSpelling('misspelledcontent'); // clean up whatever mess the spell checker left..
				var misspelledcontent = m.find('#misspelledcontent');
				
				if(v){ //save..
					
					options.success(misspelledcontent.text(), misspelledcontent.html(),misspelledcontent);
				}
				else{ //cancel..
					options.cancel(text);
				}
				
				return true;
			}
		};
		impoptions = $.extend({}, impoptions, options.impromptuOptions);
		
		$.prompt(wrapper,impoptions);
		
		options.checkSpelling('misspelledcontent');
	};
	
	$.impspeller.defaults = {
		checkSpelling: function(containerId){
			//call the spell check plugin here...
			Spelling.init(containerId);
			Spelling.check(function(){});
		
		},
		doneCheckSpelling: function(containerId){
			Spelling.remove();
		},
		impromptuOptions: {
		
		},
		success: function(correctedtext, correctedhtml, misspelledcontentElement){
		
		},
		cancel: function(originaltext){
		
		}
	};
	
	$.impspeller.setDefaults = function(options) {
		$.impspeller.defaults = $.extend({}, $.impspeller.defaults, options);
	};

})(jQuery);
