$(document)
		.ready(
				function() {

					$('#hdWipTable')
							.dataTable(
									{
										"bPaginate" : false,
										"bLengthChange" : false,
										"bFilter" : false,
										"bSort" : false,
										"bInfo" : false,
										"bAutoWidth" : false,
										"sAjaxSource" : 'hdWipTable.html',
										"aoColumns" : [ {
											"mDataProp" : "priority",
											"sClass" : "textCenter"
										}, 
										{
											"mDataProp" : "openedDate",
											"sClass" : "textCenter"
										},
										
										{
											"mDataProp" : "ticketNumber",
											"sClass" : "ticketNumber"
										}, 
										{
											"mDataProp" : "siteNumberDisplay",
											"sClass" : "siteNumberText"
										}, 
										{
											"mDataProp" : "status",
										},
										{
											"mDataProp" : "nextActionForDisplay"
										}, 
										{
											"mDataProp" : "nextActionDueDate",
											"sClass" : "textCenter"
										}, 
										{
											"mDataProp" : "cca_tssForDisplay"
										}, 
										{
											"mDataProp" : "shortDescriptionDisplay",
											"sClass" : "shortDescriptionText"
										}, 
										{
											"mDataProp" : "customerForDisplay"
										}
										, 
										{
											"mDataProp" : "assignmentGroupName",
											"sClass" : "shortDescriptionText"
										},
										{
											"mDataProp" : "clientType",
											"sClass" : "shortDescriptionText"
										}
										],
										"sDom" : 'T<"clear">lfrtip',
										"oTableTools" : {
											"sSwfPath" : pageContextPath
													+ "/resources/plugins/tableTool/swf/copy_csv_xls.swf",
											"aButtons" : [ {
												"sExtends" : "copy",
												"sToolTip" : "Copy To Clipboard",
												"sButtonText" : "",
											} ]
										}
									});
					$('#hdWipOverDueTable')
							.dataTable(
									{
										"bPaginate" : false,
										"bLengthChange" : false,
										"bFilter" : false,
										"bSort" : false,
										"bInfo" : false,
										"bAutoWidth" : false,
										"sAjaxSource" : "hdWipOverDueTable.html",
										"aoColumns" : [ {
											"mDataProp" : "priority",
											"sClass" : "textCenter"
										}, 
										{
											"mDataProp" : "openedDate",
											"sClass" : "textCenter"
										}, 
										
										
										{
											"mDataProp" : "ticketNumber",
											"sClass" : "ticketNumber"
										}, 
										
										
										
										{
											"mDataProp" : "siteNumberDisplay",
											"sClass" : "siteNumberText"
										}, 
										{
											"mDataProp" : "status",
										},
										{
											"mDataProp" : "nextActionForDisplay"
										},
										{
											"mDataProp" : "nextActionDueDate",
											"sClass" : "textCenter"
										}, 
										{
											"mDataProp" : "cca_tssForDisplay"
										}, 
										{
											"mDataProp" : "shortDescriptionDisplay",
											"sClass" : "shortDescriptionText"
										}, 
										{
											"mDataProp" : "customerForDisplay"
										}
										, 
										{
											"mDataProp" : "assignmentGroupName",
											"sClass" : "shortDescriptionText"
										},
										{
											"mDataProp" : "clientType",
											"sClass" : "shortDescriptionText"
										}
										
										],
										"sDom" : 'T<"clear">lfrtip',
										"oTableTools" : {
											"sSwfPath" : pageContextPath
													+ "/resources/plugins/tableTool/swf/copy_csv_xls.swf",
											"aButtons" : [ {
												"sExtends" : "copy",
												"sToolTip" : "Copy To Clipboard",
												"sButtonText" : "",
											} ]
										}
									});
					
					// for next action over due
					
					$('#hdWipNextActionTable')
					.dataTable(
							{
								"bPaginate" : false,
								"bLengthChange" : false,
								"bFilter" : false,
								"bSort" : false,
								"bInfo" : false,
								"bAutoWidth" : false,
								"sAjaxSource" : "hdWipNextActionTable.html",
								"aoColumns" : [ {
									"mDataProp" : "priority",
									"sClass" : "textCenter"
								}, 
								{
									"mDataProp" : "openedDate",
									"sClass" : "textCenter"
								}, 
								
								
								{
									"mDataProp" : "ticketNumber",
									"sClass" : "ticketNumber"
								}, 
								
								
								
								{
									"mDataProp" : "siteNumberDisplay",
									"sClass" : "siteNumberText"
								}, 
								{
									"mDataProp" : "status",
								},
								{
									"mDataProp" : "nextActionForDisplay"
								},
								{
									"mDataProp" : "nextActionDueDate",
									"sClass" : "textCenter"
								}, 
								{
									"mDataProp" : "cca_tssForDisplay"
								}, 
								{
									"mDataProp" : "shortDescriptionDisplay",
									"sClass" : "shortDescriptionText"
								}, 
								{
									"mDataProp" : "customerForDisplay"
								}
								, 
								{
									"mDataProp" : "assignmentGroupName",
									"sClass" : "shortDescriptionText"
								},
								{
									"mDataProp" : "clientType",
									"sClass" : "shortDescriptionText"
								}
								
								],
								"sDom" : 'T<"clear">lfrtip',
								"oTableTools" : {
									"sSwfPath" : pageContextPath
											+ "/resources/plugins/tableTool/swf/copy_csv_xls.swf",
									"aButtons" : [ {
										"sExtends" : "copy",
										"sToolTip" : "Copy To Clipboard",
										"sButtonText" : "",
									} ]
								}
							});
					
// for resolved over due
					
					$('#hdResolvedTable')
					.dataTable(
							{
								"bPaginate" : false,
								"bLengthChange" : false,
								"bFilter" : false,
								"bSort" : false,
								"bInfo" : false,
								"bAutoWidth" : false,
								"sAjaxSource" : "hdResolvedTable.html",
								"aoColumns" : [ {
									"mDataProp" : "priority",
									"sClass" : "textCenter"
								}, 
								{
									"mDataProp" : "updatedDate",
									"sClass" : "textCenter"
								}, 
								
								
								{
									"mDataProp" : "ticketNumber",
									"sClass" : "ticketNumber"
								}, 
								
								
								
								{
									"mDataProp" : "siteNumberDisplay",
									"sClass" : "siteNumberText"
								}, 
								{
									"mDataProp" : "status",
								},
								{
									"mDataProp" : "nextActionForDisplay"
								},
								{
									"mDataProp" : "nextActionDueDate",
									"sClass" : "textCenter"
								}, 
								{
									"mDataProp" : "cca_tssForDisplay"
								}, 
								{
									"mDataProp" : "shortDescriptionDisplay",
									"sClass" : "shortDescriptionText"
								}, 
								{
									"mDataProp" : "customerForDisplay"
								}
								, 
								{
									"mDataProp" : "assignmentGroupName",
									"sClass" : "shortDescriptionText"
								},
								{
									"mDataProp" : "clientType",
									"sClass" : "shortDescriptionText"
								}
								
								],
								"sDom" : 'T<"clear">lfrtip',
								"oTableTools" : {
									"sSwfPath" : pageContextPath
											+ "/resources/plugins/tableTool/swf/copy_csv_xls.swf",
									"aButtons" : [ {
										"sExtends" : "copy",
										"sToolTip" : "Copy To Clipboard",
										"sButtonText" : "",
									} ]
								}
							});
					$('#hdPendingAckCustTable')
							.dataTable(
									{
										"bPaginate" : false,
										"bLengthChange" : false,
										"bFilter" : false,
										"bSort" : false,
										"bInfo" : false,
										"bAutoWidth" : false,
										"sAjaxSource" : "hdPendingAckCust.html",
										"aoColumns" : [ {
											"mDataProp" : "openedDate",
											"sClass" : "textCenter"
										}, {
											"mDataProp" : "siteNumberDisplay",
											"sClass" : "siteNumberTextPendingCubes"
										}, {
											"mDataProp" : "source"
										}, {
											"mDataProp" : "customer"
										} ],
										"sDom" : 'T<"clear">lfrtip',
										"oTableTools" : {
											"sSwfPath" : pageContextPath
													+ "/resources/plugins/tableTool/swf/copy_csv_xls.swf",
											"aButtons" : [ {
												"sExtends" : "copy",
												"sToolTip" : "Copy To Clipboard",
												"sButtonText" : "",
											} ]
										}
									});
					$('#hdPendingAckAlertTable')
					.dataTable(
							{
								"bPaginate" : false,
								"bLengthChange" : false,
								"bFilter" : false,
								"bSort" : false,
								"bInfo" : false,
								"bAutoWidth" : false,
								"sAjaxSource" : "hdPendingAckAlert.html",
								"aoColumns" : [ {
									"mDataProp" : "openedDate",
									"sClass" : "textCenter"
								}, {
									"mDataProp" : "siteNumberDisplay",
									"sClass" : "siteNumberTextPendingCubes"
								}, {
									"mDataProp" : "state",
									"sClass" : "textCenter"
								}, {
									"mDataProp" : "customer"
								}],
								"sDom" : 'T<"clear">lfrtip',
								"oTableTools" : {
									"sSwfPath" : pageContextPath
											+ "/resources/plugins/tableTool/swf/copy_csv_xls.swf",
									"aButtons" : [ {
										"sExtends" : "copy",
										"sToolTip" : "Copy To Clipboard",
										"sButtonText" : "",
									} ]
								}
							});
					$('#hdPendingAckMonitorTable')
					.dataTable(
							{
								"bPaginate" : false,
								"bLengthChange" : false,
								"bFilter" : false,
								"bSort" : false,
								"bInfo" : false,
								"bAutoWidth" : false,
								"sAjaxSource" : "hdPendingAckMonitor.html",
								"aoColumns" : [
												{
													"mDataProp" : "openedDate",
													"sClass" : "textCenter"
												},
												{
													"mDataProp" : "siteNumberDisplay",
													"sClass" : "siteNumberTextPendingCubes"
												},
												{
													"mDataProp" : "state",
													"sClass" : "textCenter"
												},
												{
													"mDataProp" : "customer"
												} ],
								"sDom" : 'T<"clear">lfrtip',
								"oTableTools" : {
									"sSwfPath" : pageContextPath
											+ "/resources/plugins/tableTool/swf/copy_csv_xls.swf",
									"aButtons" : [ {
										"sExtends" : "copy",
										"sToolTip" : "Copy To Clipboard",
										"sButtonText" : "",
									} ]
								}
							});
					$('#hdPendingAckMonitorTier2Table')
					.dataTable(
							{
								"bPaginate" : false,
								"bLengthChange" : false,
								"bFilter" : false,
								"bSort" : false,
								"bInfo" : false,
								"bAutoWidth" : false,
								"sAjaxSource" : "hdPendingAckMonitorTier2.html",
								"aoColumns" : [
												{
													"mDataProp" : "customer"
												},
												{
													"mDataProp" : "siteNumberDisplay",
													"sClass" : "siteNumberTextPendingCubes"
												},
												{
													"mDataProp" : "state",
													"sClass" : "textCenter"
												},
												{
													"mDataProp" : "openedDate",
													"sClass" : "textCenter"
												},
					                            {
					                                "mDataProp": "lastTd"
					                            } ],
								"sDom" : 'T<"clear">lfrtip',
								"oTableTools" : {
									"sSwfPath" : pageContextPath
											+ "/resources/plugins/tableTool/swf/copy_csv_xls.swf",
									"aButtons" : [ {
										"sExtends" : "copy",
										"sToolTip" : "Copy To Clipboard",
										"sButtonText" : "",
									} ]
								}
							});
					
					
					
					$('#monitoringDateTimeDetailsTable')
					.dataTable(
							{
								"bPaginate" : false,
								"bLengthChange" : false,
								"bFilter" : false,
								"bSort" : false,
								"bInfo" : false,
								"bAutoWidth" : false,
								"sAjaxSource" : "monitoringDateTimeDetailsTable.html",
								"aoColumns" : [
												{
													"mDataProp" : "customer"
												},
												{
													"mDataProp" : "siteNumberDisplay",
													"sClass" : "siteNumberTextPendingCubes"
												},
												{
													"mDataProp" : "state",
													"sClass" : "textCenter"
												},
												{
													"mDataProp" : "dateDiff",
													"sClass" : "DateDiffMonitoringTimeCube"
												},
												{
													"mDataProp" : "nextActionLabel"													
												},
												{
													"mDataProp" : "openedDate",
													"sClass" : "textCenter"
												},
					                            {
					                                "mDataProp": "lastTd",
					                                	"sClass" : "wantToHideThis"
					                            } ],
								"sDom" : 'T<"clear">lfrtip',
								"oTableTools" : {
									"sSwfPath" : pageContextPath
											+ "/resources/plugins/tableTool/swf/copy_csv_xls.swf",
									"aButtons" : [ {
										"sExtends" : "copy",
										"sToolTip" : "Copy To Clipboard",
										"sButtonText" : "",
									} ]
								}
							});
					
					
					

				});
/* function :To generate Excel of cube -start */
$(".generateExcel").live(
		'click',
		function() {
			$generateExcellImg = $(this);
			var tableId = $generateExcellImg.parents('div').find('table').attr(
					'id');
			//alert(tableId);
			$("#secretIFrame").attr(
					"src",
					pageContextPath
							+ '/hdMainTable/generateExcelView.html?tableId='
							+ tableId);
		});
/* function :To generate Excel of cube -end */

/* function :To print cube -start */
$(".printCube").live('click', function() {
	window.print();
});
/* function :To print cube -end */