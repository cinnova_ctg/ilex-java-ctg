function closeWindow() {
	$.unblockUI();
	$("#popupBody").empty().html(
			'<img src="' + pageContextPath
					+ '/resources/images/waiting_tree.gif" />');
}

function closeWindow2() {
	$('#popupScreen').unblock();
	$("#popupBody2").empty().html(
			'<img src="' + pageContextPath
					+ '/resources/images/waiting_tree.gif" />');
	$("#pageTitle2").innerHTML = '';
}

function setupNewCustomerStep1() {
	$.blockUI({
		message : $('#popupScreen'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	$('#popupScreen').parent('.blockUI.blockMsg').css('left',100);
	$('#popupScreen').parent('.blockUI.blockMsg').css('top',100);
	$("#pageTitle").html(
			"Setup New Customer- Select Customer/Appendix (Step 1/3)");
	$("#popupBody").load(
			pageContextPath + "/hdGroupMgmt/setupNewCustomerStep1.html");
}

function findCustomer() {

	$('#popupScreen').block({
		message : $('#popupScreen2'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			left : '30%',
			top : '180px',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	$('#popupScreen2').parent('.blockUI.blockMsg').css('left',70);
	$('#popupScreen2').parent('.blockUI.blockMsg').css('top',70);

	$("#pageTitle2").html("Select Customer and NetMedX Appendix");
	$("#popupBody2").load(
			pageContextPath + "/hdGroupMgmt/selectCustomerAppendix.html");

}

function setupNewCustomerStep2() {
	if ($("#customerName").val() == "") {
		alert("Please Select Customer");
		return false;
	}
	if ($("#appendixTitle").val() == "") {
		alert("Please Select Project");
		return false;
	}
	var activeMonitorApproach = $(
			'input:radio[name=activeMonitorApproach]:checked').val();
	var customerType = $('input:radio[name=customerType]:checked').val();
	if (activeMonitorApproach == "" || activeMonitorApproach == undefined) {
		alert("Please select Monitoring Approach");
		return false;
	}
	if (customerType == "" || customerType == undefined) {
		alert("Please select Customer Type ");
		return false;
	}

	$.blockUI({
		message : $('#popupScreen'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	$('#popupScreen').parent('.blockUI.blockMsg').css('left',100);
	$('#popupScreen').parent('.blockUI.blockMsg').css('top',100);
	$("#pageTitle").html(
			"Setup New Customer- Assign Group To Project (Step 2/3)");
	var form_fh = $("#newCustomerSetupStep1");
	var data = $(form_fh).serializeArray();

	$.ajax({
		type : 'POST',
		data : data,
		url : pageContextPath + '/hdGroupMgmt/setupNewCustomerStep2.html',
		success : function(data) {
			$("#popupBody").html(data);

		}
	});

}

function setupNewCustomerStep3() {
	$.blockUI({
		message : $('#popupScreen'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	$('#popupScreen').parent('.blockUI.blockMsg').css('left',100);
	$('#popupScreen').parent('.blockUI.blockMsg').css('top',100);
	
	$("#pageTitle").html("Setup New Customer- Set Options (Step 3/3)");
	var form_fh = $("#newCustomerSetupStep2");
	var data = $(form_fh).serializeArray();

	$.ajax({
		type : 'POST',
		data : data,
		url : pageContextPath + '/hdGroupMgmt/setupNewCustomerStep3.html',
		success : function(data) {
			$("#popupBody").html(data);

		}
	});

}

function setCustomerAppedix() {

	if ($("#hdCustomerAppendixSelect").val() == null) {
		alert("Please select Customer- Apppendix");
		return false;
	} else {
		var customerAppendixids = $("#hdCustomerAppendixSelect").val();
		var customerId = customerAppendixids.substring(0, customerAppendixids
				.indexOf("~"));
		var appendixId = customerAppendixids.substring(customerAppendixids
				.indexOf("~") + 1);

		$.ajax({
			type : 'POST',
			data : {
				"customerId" : customerId,
				"appendixId" : appendixId
			},
			url : pageContextPath
					+ '/hdGroupMgmt/getCustomerAppendixDetails.html',
			success : function(data) {
				var obj = $.parseJSON(data);
				$("#customerId").val(obj.customerId);
				$("#appendixId").val(obj.appendixId);
				$("#customerName").val(obj.customerName);
				$("#appendixTitle").val(obj.appendixTitle);
				$("#msaId").val(obj.msaId);
				closeWindow2();

			}
		});

	}

}

function saveCustomerSetup() {
	var alertMessage = false;

	$("#monitoringKeyTable input[type=text]").each(function() {
		if ($(this).parent().attr("mandatoryAttr") == "true") {
			if ($.trim($(this).val()) == "") {
				alertMessage = true;
				return;
			}
		}

	});
	if (alertMessage == true) {
		alert("Please Enter The Mandetory Fields");
		return false;
	}

	var form_fh = $("#newCustomerSetupStep3");
	var data = $(form_fh).serializeArray();

	$.ajax({
		type : 'POST',
		data : data,
		url : pageContextPath + '/hdGroupMgmt/saveNewCustomer.html',
		success : function(data) {
			if (data == "success") {
				closeWindow();
			} else {
				$("#errorDiv").html(data);
			}
		}

	}).error(function() {
		$("#errorDiv").html(data);
	});

}

function editCustomerDetails(customerId, appendixId) {
	$.blockUI({
		message : $('#popupScreen3'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	// $('#popupScreen').parent('.blockUI.blockMsg').offset({
	// top : 100,
	// left : 100
	// });
	$("#pageTitle3").html("Edit Customer Setup");

	$.ajax({
		type : 'POST',
		data : {
			"customerId" : customerId,
			"appendixId" : appendixId
		},
		url : pageContextPath + '/hdGroupMgmt/editCustomerDetails.html',
		success : function(data) {
			$("#popupBody3").html(data);
			$("#popupBody3Table").css("width", $("#popupBody3").width() + 10);
		}
	});
}

function editCustmerSetup() {

	var form_fh = $("#editCustmerSetup");
	var data = $(form_fh).serializeArray();

	$
			.ajax(
					{
						type : 'POST',
						data : data,
						url : pageContextPath
								+ '/hdGroupMgmt/editCustmerSetup.html',
						success : function(data) {
							if (data == "success") {

								var url = pageContextPath
										+ '/hdGroupMgmt/viewCustomers.html?customerType='
										+ customerTypeParam + '&pageIndex='
										+ pageStart;
								location.href = url;
								closeWindow();

							} else {
								$("#errorDiv").html(data);
							}
						}

					}).error(function() {
				$("#errorDiv").html(data);
			});
}

function editGroupIntegrationDetails(customerId, appendixId) {

	$.blockUI({
		message : $('#groupIntegrationPopupScreen'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});

	$("#groupIntegrationPageTitle").html("Assign Groups to Project");
	
	$('#groupIntegrationPopupScreen').parent('.blockUI.blockMsg').css('left',100);
	$('#groupIntegrationPopupScreen').parent('.blockUI.blockMsg').css('top',100);

	$.ajax({
		type : 'POST',
		data : {
			"customerId" : customerId,
			"appendixId" : appendixId
		},
		url : pageContextPath
				+ '/hdGroupMgmt/getCustomerGroupIntegrations.html',
		success : function(data) {
			$("#groupIntegrationPopupBody").html(data);
			$("#groupIntegrationPopupBody").css("height", "450px");
			$("#groupIntegrationPopupScreenTable").css("width",
					$("#groupIntegrationPopupBody").width() + 20);

		}
	});

}

function editCustomerGroupIntegrations() {

	var form_fh = $("#editCustomerGroupIntegrationsForm");
	var data = $(form_fh).serializeArray();

	$.ajax(
			{
				type : 'POST',
				data : data,
				url : pageContextPath
						+ '/hdGroupMgmt/editCustomerGroupIntegrations.html',
				success : function(data) {
					if (data == "success") {
						closeWindow();
					} else {
						$("#errorDiv").html(data);
					}
				}

			}).error(function() {
		$("#errorDiv").html(data);
	});

}

function getCustomerOptions(customerId, appendixId) {

	$.blockUI({
		message : $('#popupScreen3'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	$('#popupScreen3').parent('.blockUI.blockMsg').css('left',100);
	$('#popupScreen3').parent('.blockUI.blockMsg').css('top',100);
	

	$("#pageTitle3").html("Edit Options (Key/Value Pairs)");

	$.ajax({
		type : 'POST',
		data : {
			"customerId" : customerId,
			"appendixId" : appendixId
		},
		url : pageContextPath + '/hdGroupMgmt/getCustomerOptions.html',
		success : function(data) {
			$("#popupBody3").html(data);
			$("#popupBody3Table").css("width", $("#popupBody3").width());

		}
	});

}

function editCustomerOptions() {

	var alertMessage = false;

	$("#monitoringKeyTable input[type=text]").each(function() {
		if ($(this).parent().attr("mandatoryAttr") == "true") {
			if ($.trim($(this).val()) == "") {
				alertMessage = true;
				return;
			}
		}

	});
	if (alertMessage == true) {
		alert("Please Enter The Mandetory Fields");
		return false;
	}

	var form_fh = $("#editCustomerOptionsForm");
	var data = $(form_fh).serializeArray();

	$.ajax({
		type : 'POST',
		data : data,
		url : pageContextPath + '/hdGroupMgmt/editCustomerOptions.html',
		success : function(data) {
			if (data == "success") {
				closeWindow();
			} else {
				$("#errorDiv").html(data);
			}
		}

	}).error(function() {
		$("#errorDiv").html(data);
	});

}

function getHDCustSummaryDetails(customerId, appendixId) {

	$.blockUI({
		message : $('#popupScreen3'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	
	$('#popupScreen').parent('.blockUI.blockMsg').css('left',100);
	$('#popupScreen').parent('.blockUI.blockMsg').css('top',100);

	$("#pageTitle3").html("Customer/Appendix Setup Summary");

	$.ajax({
		type : 'POST',
		data : {
			"customerId" : customerId,
			"appendixId" : appendixId
		},
		url : pageContextPath + '/hdGroupMgmt/getHDCustSummaryDetails.html',
		success : function(data) {
			$("#popupBody3").html(data);
			$("#popupBody3Table").css("width", $("#popupBody3").width());

		}
	});

}
