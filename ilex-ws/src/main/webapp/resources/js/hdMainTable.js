$.ajaxSetup({
    cache: false,
    async: true
});

$.support.cors = true;

var hdTables = new Object();
var ticketPopUp = false;

var ilex_url = document.URL;

var isFromMain = "yes";
var currentTicketId = '';
var ilexTicketNumber = '';
var siteNumber = '';
var headerText = '';
var customer = '';
var appendixName = '';
var ticketId = '';
var nextActionId = '';
var status = '';
var ticketSourceno = '';
var state = '';
var isStateContainsDown = '';

var ticketchanged = {
    recordEffortChanged: false,
    nextActionChanged: false,
    workNotesChanged: false,
    ticketStateChanged: false,
    circuitStatusChanged: false
};
var currentTableInUse = '';
var redrawQueueProcessing = false;

var cubestatus = {
    pendingAckCustomer: 0,
    pendingAckAlert: 0,
    pendingAckMonitor: 0,
    pendingAckSecurity: 0,
    wip: 0,
    wipOD: 0
};

$redrawQueue = $('#redrawDiv');

function processRedrawQueue() {
    if (!redrawQueueProcessing) {
        redrawQueueProcessing = true;
    } else {
        return true;
    }

    if (currentTableInUse == '') {
        $redrawQueue.dequeue();
    }

    redrawQueueProcessing = false;
    return true;
}

var intervalID = this.setInterval(processRedrawQueue, 125);

/* function :To reload ajaxsource of datatable -Start */

$.fn.dataTableExt.oApi.fnReloadAjax = function (oSettings, sNewSource,
        fnCallback, bStandingRedraw) {
    bStandingRedraw = true;
    if (typeof sNewSource != 'undefined' && sNewSource != null) {
        oSettings.sAjaxSource = sNewSource;
    }
    this.oApi._fnProcessingDisplay(oSettings, true);
    var that = this;
    var iStart = oSettings._iDisplayStart;
    var aData = [];

    this.oApi._fnServerParams(oSettings, aData);

    oSettings.fnServerData(oSettings.sAjaxSource, aData, function (json) {
        /*
         * Clear the old information from the table
         */
        that.oApi._fnClearTable(oSettings);

        /*
         * Got the data - add it to the table
         */
        var aData = (oSettings.sAjaxDataProp !== "") ? that.oApi
                ._fnGetObjectDataFn(oSettings.sAjaxDataProp)(json) : json;

        for (var i = 0; i < aData.length; i++) {
            that.oApi._fnAddData(oSettings, aData[i]);
        }

        oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
        that.fnDraw();

        if (typeof bStandingRedraw != 'undefined' && bStandingRedraw == true) {
            oSettings._iDisplayStart = iStart;
            that.fnDraw(false);
        }
        that.oApi._fnProcessingDisplay(oSettings, false);

        /*
         * Callback user function - for event handlers etc
         */
        if (typeof fnCallback == 'function' && fnCallback != null) {
            fnCallback(oSettings);
        }
    }, oSettings);

};

/* function :To reload ajaxsource of datatable -End */

/* function :To call AjaxPoll maethod after every 1 minute -Start */
function processUsingAjaxPoll() {
    var ajax_call = function () {
        processAjaxPoll();
    };



    var interval = 1000 * 60;

    setInterval(ajax_call, interval);

}
/* function :To call AjaxPoll maethod after every 1 minute -End */

/* function :To get updated table list using AjaxPoll -Start */
function processAjaxPoll() {
    $.ajax({
        type: 'GET',
        url: pageContextPath + '/hdMainTable/getCubeStatus.html',
        success: function (obj) {
            var data = $.parseJSON(obj);

            if (cubestatus.pendingAckCustomer != data.pendingAckCustCubeStatus) {
                hdTables.hdpendingackcustTable.fnReloadAjax();
                hdTables.hdpendingacksecurityTable.fnReloadAjax();
                cubestatus.pendingAckCustomer = data.pendingAckCustCubeStatus;
                disableResolvedDraggable();
            } else
            if (cubestatus.pendingAckAlert != data.pendingAckAlertCubeStatus) {
                hdTables.hdpendingackalertTable.fnReloadAjax();
                cubestatus.pendingAckAlert = data.pendingAckAlertCubeStatus;
                disableResolvedDraggable();
                flag = false;
            } else
            if (cubestatus.pendingAckMonitor != data.pendingAckMonitorCubeStatus) {
                hdTables.hdpendingackmonitorTable.fnReloadAjax();
                cubestatus.pendingAckMonitor = data.pendingAckMonitorCubeStatus;
                setPendingAckMonitoringFooterDetail();
                disableResolvedDraggable();
            } else
            if (cubestatus.wip != data.wipCubeStatus) {
                hdTables.hdWipTable.fnClearTable();
                hdTables.hdWipTable.fnReloadAjax();
                hdTables.hdWipNextActionTable.fnClearTable();
                hdTables.hdWipNextActionTable.fnReloadAjax();
                hdTables.hdResolvedTable.fnClearTable();
                hdTables.hdResolvedTable.fnReloadAjax();
                hdTables.hdWipoverdueTable.fnClearTable();
                hdTables.hdWipoverdueTable.fnReloadAjax();
                cubestatus.wip = data.wipCubeStatus;
                appendSiteDetail();
                disableResolvedDraggable();
            } else
            if (cubestatus.wipOD != data.wipODCubeStatus) {
                hdTables.hdWipTable.fnClearTable();
                hdTables.hdWipTable.fnReloadAjax();
                hdTables.hdWipNextActionTable.fnClearTable();
                hdTables.hdWipNextActionTable.fnReloadAjax();
                hdTables.hdResolvedTable.fnClearTable();
                hdTables.hdResolvedTable.fnReloadAjax();
                hdTables.hdWipoverdueTable.fnClearTable();
                hdTables.hdWipoverdueTable.fnReloadAjax();
                cubestatus.wipOD = data.wipODCubeStatus;
                appendSiteDetail();
                disableResolvedDraggable();
            } else if (data.nextActiondueStatus != 'false') {
                hdTables.hdWipTable.fnClearTable();
                hdTables.hdWipTable.fnReloadAjax();
                hdTables.hdWipNextActionTable.fnClearTable();
                hdTables.hdWipNextActionTable.fnReloadAjax();
                hdTables.hdResolvedTable.fnClearTable();
                hdTables.hdResolvedTable.fnReloadAjax();
                hdTables.hdWipoverdueTable.fnClearTable();
                hdTables.hdWipoverdueTable.fnReloadAjax();
                appendSiteDetail();
                disableResolvedDraggable();
            }
        }
    });
}
/* function :To get updated table list using AjaxPoll -End */

function preInitializeCubeVariables() {
}

function postRedrawCubeVariables() {

}

function processCube(cubeObject) {
    // currentTableInUse = cubeObject.attr("id");
    cubeObject.fnReloadAjax();

    appendSiteDetail();
    disableResolvedDraggable();
}

var activeTickets = $("#activeTicketsDTO").val();

$(document).ready(hdOnLoad1);


function hdOnLoad1() {
    $("#confirmButton").click(function (e) {
        e.preventDefault();
        $('#popup_progress').hide();
        clearLockStatus($("#ticketId").val());
        window.close();
    });

    $('#siteSearchId').keyup(function (e) {
        for (var key in hdTables) {
            var hdTable = hdTables[key];
            console.log(hdTable);

            hdTable.fnFilter($('#siteSearchId').val());
        }
    });

    disableResolvedDraggable();

    $("#ui-datepicker-div").css("display", "none");

    hdTables.hdWipTable = $('#hdWipTable')
            .dataTable(
                    {
                        "bDestroy": true,
                        "iDisplayLength": -1,
//						"aoColumnDefs" : [ {
//							"bSortable" : true,
//							"aTargets" : [ 10 ]
//						} ],

                        "aoColumnDefs": [{
                                "bSortable": true,
                                "aTargets": [0]
                            }, {
                                "bSortable": true,
                                "aTargets": [1]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [2]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [3]
                            }, {
                                "bSortable": true,
                                "aTargets": [4]
                            }, {
                                "bSortable": true,
                                "aTargets": [5]
                            }, {
                                "bSortable": true,
                                "aTargets": [6]
                            }, {
                                "bSortable": true,
                                "aTargets": [7]
                            }, {
                                "bSortable": true,
                                "aTargets": [8]
                            }, {
                                "bSortable": true,
                                "aTargets": [9]
                            }, {
                                "bSortable": true,
                                "aTargets": [10]
                            }, {
                                "bSortable": true,
                                "aTargets": [11]
                            }, {
                                "bSortable": false,
                                "aTargets": [12]
                            }],
                        /////////////////
                        "sAjaxSource": 'hdWipTable.html',
                        "aoColumns": [
                            {
                                "mDataProp": "priority",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "openedDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "ticketNumber",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "siteNumberDisplay",
                                "sClass": "siteNumberText"
                            },
                            {
                                "mDataProp": "status"
                            },
                            {
                                "mDataProp": "nextActionForDisplay"
                            },
                            {
                                "mDataProp": "nextActionDueDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "cca_tssForDisplay"
                            },
                            {
                                "mDataProp": "shortDescriptionDisplay",
                                "sClass": "shortDescriptionText"
                            },
                            {
                                "mDataProp": "customerForDisplay"
                            },
                            {
                                "mDataProp": "assignmentGroupName"
                            },
                            {
                                "mDataProp": "clientType"
                            },
                            {
                                "mDataProp": "lastTd"
                            }],
                        "fnInitComplete": function (oSettings,
                                json) {

                            // setActiveTickets();
                        },
                        "fnCreatedRow": function (nRow, aData, iDataIndex) {
                            $(nRow).css("background-color", aData.eventColor);
                        },
                        "fnDrawCallback": function () {
                            $("span.greenIcon,span.redIcon")
                                    .filter(':visible')
                                    .draggable(
                                            {
                                                revert: 'invalid',
                                                helper: function (
                                                        event) {

                                                    var $ret = $('<div/>');
                                                    $ret
                                                            .addClass('helperDiv');
                                                    var ilexNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'ilexnumber');
                                                    $($ret)
                                                            .text(
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "ilexNumber",
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "priority",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'priority'));
                                                    $($ret)
                                                            .attr(
                                                                    "ticketId",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'ticketId'));

                                                    var siteNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'siteNumber');


                                                    $($ret)
                                                            .attr(
                                                                    "siteNumber",
                                                                    siteNumber);

                                                    $($ret)
                                                            .attr(
                                                                    "customer",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customer'));
                                                    $($ret)
                                                            .attr(
                                                                    "customerNumber",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customerNumber'));
                                                    $($ret)
                                                            .attr(
                                                                    "circuitStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'circuitStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "overdueStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'overdueStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "siteStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'siteStatus'));
                                                    return $ret;
                                                },
                                                distance: 0,
                                                start: function () {
                                                    $(this)
                                                            .data(
                                                                    "origPosition",
                                                                    $(
                                                                            this)
                                                                    .position());
                                                }
                                            });

                            $("span.redIcon").draggable(
                                    "disable", 1);

                            $(
                                    "#leftWIPTickets div,#rightWIPTickets div")
                                    .droppable(
                                            {
                                                accept: function (
                                                        d) {

                                                    // checkIsTicketAvaiableForDrop(d,this);
                                                    if ($(this)
                                                            .text()
                                                            .indexOf(
                                                                    "Drop") == "0") {
                                                        return true;
                                                    } else {
                                                        return false;
                                                    }

                                                },
                                                drop: function (
                                                        event,
                                                        ui) {
                                                    var ticketId = ui.helper
                                                            .attr("ticketId");
                                                    var isSuccess = true;
                                                    $
                                                            .ajax({
                                                                type: 'POST',
                                                                data: {
                                                                    'ticketId': ticketId
                                                                },
                                                                url: pageContextPath
                                                                        + '/hdActiveTicket/isTicketAvailableForDrop.html',
                                                                async: false,
                                                                success: function (
                                                                        data) {
                                                                    if (data == "success") {
                                                                        isSuccess = true;
                                                                    } else {
                                                                        alert(data);
                                                                        isSuccess = false;
                                                                    }
                                                                }
                                                            });

                                                    if (!isSuccess) { // failed
                                                        ui.draggable
                                                                .animate(
                                                                        ui.draggable
                                                                        .data().origPosition,
                                                                        "slow");
                                                        return;
                                                    }

                                                    $(this)
                                                            .ticketDrop(
                                                                    event,
                                                                    ui);
                                                }

                                            });

                        },
                        "sDom": 'tir'

                    });

    hdTables.hdWipoverdueTable = $('#hdWipOverDueTable')
            .dataTable(
                    {
                        "bDestroy": true,
                        "aaSorting": [],
                        "iDisplayLength": -1,
                        "aoColumnDefs": [{
                                "bSortable": true,
                                "aTargets": [0]
                            }, {
                                "bSortable": true,
                                "aTargets": [1]
                            }, {
                                "bSortable": true,
                                "aTargets": [2]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [3]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [4]
                            }, {
                                "bSortable": true,
                                "aTargets": [5]
                            }, {
                                "bSortable": true,
                                "aTargets": [6]
                            }, {
                                "bSortable": true,
                                "aTargets": [7]
                            }, {
                                "bSortable": true,
                                "aTargets": [8]
                            }, {
                                "bSortable": true,
                                "aTargets": [9]
                            }, {
                                "bSortable": true,
                                "aTargets": [10]
                            }, {
                                "bSortable": true,
                                "aTargets": [11]
                            }, {
                                "bSortable": false,
                                "aTargets": [12]
                            }],
                        "sAjaxSource": 'hdWipOverDueTable.html',
                        "aoColumns": [
                            {
                                "mDataProp": "priority",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "openedDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "ticketNumber",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "siteNumberDisplay",
                                "sClass": "siteNumberText"
                            },
                            {
                                "mDataProp": "status",
                            },
                            {
                                "mDataProp": "nextActionForDisplay"
                            },
                            {
                                "mDataProp": "nextActionDueDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "cca_tssForDisplay"
                            },
                            {
                                "mDataProp": "shortDescriptionDisplay",
                                "sClass": "shortDescriptionText"
                            },
                            {
                                "mDataProp": "customerForDisplay"
                            },
                            {
                                "mDataProp": "assignmentGroupName"
                            },
                            {
                                "mDataProp": "clientType"
                            },
                            {
                                "mDataProp": "lastTd"
                            }],
                        "fnInitComplete": function (oSettings,
                                json) {

                            //setActiveTickets();
                        },
                        "fnCreatedRow": function (nRow, aData, iDataIndex) {
                            $(nRow).css("background-color", aData.eventColor);
                        },
                        "fnDrawCallback": function () {
                            $("span.greenIcon,span.redIcon")
                                    .filter(':visible')
                                    .draggable(
                                            {
                                                revert: 'invalid',
                                                helper: function (
                                                        event) {

                                                    var $ret = $('<div/>');
                                                    $ret
                                                            .addClass('helperDiv');
                                                    var ilexNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'ilexnumber');
                                                    $($ret)
                                                            .text(
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "ilexNumber",
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "priority",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'priority'));
                                                    $($ret)
                                                            .attr(
                                                                    "ticketId",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'ticketId'));
                                                    var siteNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'siteNumber');


                                                    $($ret)
                                                            .attr(
                                                                    "siteNumber",
                                                                    siteNumber);

                                                    $($ret)
                                                            .attr(
                                                                    "customer",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customer'));
                                                    $($ret)
                                                            .attr(
                                                                    "customerNumber",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customerNumber'));
                                                    $($ret)
                                                            .attr(
                                                                    "circuitStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'circuitStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "overdueStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'overdueStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "siteStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'siteStatus'));
                                                    return $ret;
                                                },
                                                distance: 0,
                                                start: function () {
                                                    $(this)
                                                            .data(
                                                                    "origPosition",
                                                                    $(
                                                                            this)
                                                                    .position());
                                                }
                                            });
                            $("span.redIcon").draggable(
                                    "disable", 1);

                            $(
                                    "#leftWIPTickets div,#rightWIPTickets div")
                                    .droppable(
                                            {
                                                accept: function (
                                                        d) {

                                                    if ($(this)
                                                            .text()
                                                            .indexOf(
                                                                    "Drop") == "0") {
                                                        return true;
                                                    } else {
                                                        return false;
                                                    }

                                                },
                                                drop: function (
                                                        event,
                                                        ui) {
                                                    var ticketId = ui.helper
                                                            .attr("ticketId");
                                                    var isSuccess = true;
                                                    $
                                                            .ajax({
                                                                type: 'POST',
                                                                data: {
                                                                    'ticketId': ticketId
                                                                },
                                                                url: pageContextPath
                                                                        + '/hdActiveTicket/isTicketAvailableForDrop.html',
                                                                async: false,
                                                                success: function (
                                                                        data) {
                                                                    if (data == "success") {
                                                                        isSuccess = true;
                                                                    } else {
                                                                        alert(data);
                                                                        isSuccess = false;
                                                                    }
                                                                }
                                                            });

                                                    if (!isSuccess) { // failed
                                                        ui.draggable
                                                                .animate(
                                                                        ui.draggable
                                                                        .data().origPosition,
                                                                        "slow");
                                                        return;
                                                    }

                                                    $(this)
                                                            .ticketDrop(
                                                                    event,
                                                                    ui);
                                                }
                                            });


                        }
                        ,
                        "sDom": 'tir'
                    });



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    hdTables.hdResolvedTable = $('#hdResolvedTable')
            .dataTable(
                    {
                        "bDestroy": true,
                        "aaSorting": [],
                        "iDisplayLength": -1,
                        "aoColumnDefs": [{
                                "bSortable": true,
                                "aTargets": [0]
                            }, {
                                "bSortable": true,
                                "aTargets": [1]
                            }, {
                                "bSortable": true,
                                "aTargets": [2]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [3]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [4]
                            }, {
                                "bSortable": true,
                                "aTargets": [5]
                            }, {
                                "bSortable": true,
                                "aTargets": [6]
                            }, {
                                "bSortable": true,
                                "aTargets": [7]
                            }, {
                                "bSortable": true,
                                "aTargets": [8]
                            }, {
                                "bSortable": true,
                                "aTargets": [9]
                            }, {
                                "bSortable": true,
                                "aTargets": [10]
                            }, {
                                "bSortable": true,
                                "aTargets": [11]
                            }, {
                                "bSortable": false,
                                "aTargets": [12]
                            }],
                        "sAjaxSource": 'hdResolvedTable.html',
                        "aoColumns": [
                            {
                                "mDataProp": "priority",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "updatedDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "ticketNumber",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "siteNumberDisplay",
                                "sClass": "siteNumberText"
                            },
                            {
                                "mDataProp": "status",
                            },
                            {
                                "mDataProp": "nextActionForDisplay"
                            },
                            {
                                "mDataProp": "nextActionDueDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "cca_tssForDisplay"
                            },
                            {
                                "mDataProp": "shortDescriptionDisplay",
                                "sClass": "shortDescriptionText"
                            },
                            {
                                "mDataProp": "customerForDisplay"
                            },
                            {
                                "mDataProp": "assignmentGroupName"
                            },
                            {
                                "mDataProp": "clientType"
                            },
                            {
                                "mDataProp": "lastTd"
                            }],
                        "fnInitComplete": function (oSettings,
                                json) {

                            //setActiveTickets();
                        },
                        "fnCreatedRow": function (nRow, aData, iDataIndex) {
                            $(nRow).css("background-color", aData.eventColor);
                        },
                        "fnDrawCallback": function () {
                            $("span.greenIcon,span.redIcon")
                                    .filter(':visible')
                                    .draggable(
                                            {
                                                revert: 'invalid',
                                                helper: function (
                                                        event) {

                                                    var $ret = $('<div/>');
                                                    $ret
                                                            .addClass('helperDiv');
                                                    var ilexNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'ilexnumber');
                                                    $($ret)
                                                            .text(
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "ilexNumber",
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "priority",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'priority'));
                                                    $($ret)
                                                            .attr(
                                                                    "ticketId",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'ticketId'));
                                                    var siteNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'siteNumber');


                                                    $($ret)
                                                            .attr(
                                                                    "siteNumber",
                                                                    siteNumber);

                                                    $($ret)
                                                            .attr(
                                                                    "customer",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customer'));
                                                    $($ret)
                                                            .attr(
                                                                    "customerNumber",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customerNumber'));
                                                    $($ret)
                                                            .attr(
                                                                    "circuitStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'circuitStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "overdueStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'overdueStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "siteStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'siteStatus'));
                                                    return $ret;
                                                },
                                                distance: 0,
                                                start: function () {
                                                    $(this)
                                                            .data(
                                                                    "origPosition",
                                                                    $(
                                                                            this)
                                                                    .position());
                                                }
                                            });
                            $("span.redIcon").draggable(
                                    "disable", 1);


                            $(
                                    "#leftWIPTickets div,#rightWIPTickets div")
                                    .droppable(
                                            {
                                                accept: function (
                                                        d) {

                                                    if ($(this)
                                                            .text()
                                                            .indexOf(
                                                                    "Drop") == "0") {
                                                        return true;
                                                    } else {
                                                        return false;
                                                    }

                                                },
                                                drop: function (
                                                        event,
                                                        ui) {
                                                    var ticketId = ui.helper
                                                            .attr("ticketId");
                                                    var isSuccess = true;
                                                    $
                                                            .ajax({
                                                                type: 'POST',
                                                                data: {
                                                                    'ticketId': ticketId
                                                                },
                                                                url: pageContextPath
                                                                        + '/hdActiveTicket/isTicketAvailableForDrop.html',
                                                                async: false,
                                                                success: function (
                                                                        data) {
                                                                    if (data == "success") {
                                                                        isSuccess = true;
                                                                    } else {
                                                                        alert(data);
                                                                        isSuccess = false;
                                                                    }
                                                                }
                                                            });

                                                    if (!isSuccess) { // failed
                                                        ui.draggable
                                                                .animate(
                                                                        ui.draggable
                                                                        .data().origPosition,
                                                                        "slow");
                                                        return;
                                                    }

                                                    $(this)
                                                            .ticketDrop(
                                                                    event,
                                                                    ui);
                                                }
                                            });


                        }
                        ,
                        "sDom": 'tir'
                    });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////













    hdTables.hdWipNextActionTable = $('#hdWipNextActionTable')
            .dataTable(
                    {
                        "bDestroy": true,
                        "aaSorting": [],
                        "iDisplayLength": -1,
                        "aoColumnDefs": [{
                                "bSortable": true,
                                "aTargets": [0]
                            }, {
                                "bSortable": true,
                                "aTargets": [1]
                            }, {
                                "bSortable": true,
                                "aTargets": [2]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [3]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [4]
                            }, {
                                "bSortable": true,
                                "aTargets": [5]
                            }, {
                                "bSortable": true,
                                "aTargets": [6]
                            }, {
                                "bSortable": true,
                                "aTargets": [7]
                            }, {
                                "bSortable": true,
                                "aTargets": [8]
                            }, {
                                "bSortable": true,
                                "aTargets": [9]
                            }, {
                                "bSortable": true,
                                "aTargets": [10]
                            }, {
                                "bSortable": true,
                                "aTargets": [11]
                            }, {
                                "bSortable": false,
                                "aTargets": [12]
                            }],
                        "sAjaxSource": 'hdWipNextActionTable.html',
                        "aoColumns": [
                            {
                                "mDataProp": "priority",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "openedDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "ticketNumber",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "siteNumberDisplay",
                                "sClass": "siteNumberText"
                            },
                            {
                                "mDataProp": "status",
                            },
                            {
                                "mDataProp": "nextActionForDisplay"
                            },
                            {
                                "mDataProp": "nextActionDueDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "cca_tssForDisplay"
                            },
                            {
                                "mDataProp": "shortDescriptionDisplay",
                                "sClass": "shortDescriptionText"
                            },
                            {
                                "mDataProp": "customerForDisplay"
                            },
                            {
                                "mDataProp": "assignmentGroupName"
                            },
                            {
                                "mDataProp": "clientType"
                            },
                            {
                                "mDataProp": "lastTd"
                            }],
                        "fnInitComplete": function (oSettings,
                                json) {

                            //setActiveTickets();
                        },
                        "fnCreatedRow": function (nRow, aData, iDataIndex) {
                            $(nRow).css("background-color", aData.eventColor);
                        },
                        "fnDrawCallback": function () {
                            $("span.greenIcon,span.redIcon")
                                    .filter(':visible')
                                    .draggable(
                                            {
                                                revert: 'invalid',
                                                helper: function (
                                                        event) {

                                                    var $ret = $('<div/>');
                                                    $ret
                                                            .addClass('helperDiv');
                                                    var ilexNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'ilexnumber');
                                                    $($ret)
                                                            .text(
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "ilexNumber",
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "priority",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'priority'));
                                                    $($ret)
                                                            .attr(
                                                                    "ticketId",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'ticketId'));
                                                    var siteNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'siteNumber');


                                                    $($ret)
                                                            .attr(
                                                                    "siteNumber",
                                                                    siteNumber);

                                                    $($ret)
                                                            .attr(
                                                                    "customer",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customer'));
                                                    $($ret)
                                                            .attr(
                                                                    "customerNumber",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customerNumber'));
                                                    $($ret)
                                                            .attr(
                                                                    "circuitStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'circuitStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "overdueStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'overdueStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "siteStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'siteStatus'));

                                                    return $ret;
                                                },
                                                distance: 0,
                                                start: function () {
                                                    $(this)
                                                            .data(
                                                                    "origPosition",
                                                                    $(
                                                                            this)
                                                                    .position());
                                                }
                                            });
                            $("span.redIcon").draggable(
                                    "disable", 1);

                            $(
                                    "#leftWIPTickets div,#rightWIPTickets div")
                                    .droppable(
                                            {
                                                accept: function (
                                                        d) {

                                                    if ($(this)
                                                            .text()
                                                            .indexOf(
                                                                    "Drop") == "0") {
                                                        return true;
                                                    } else {
                                                        return false;
                                                    }

                                                },
                                                drop: function (
                                                        event,
                                                        ui) {
                                                    var ticketId = ui.helper
                                                            .attr("ticketId");
                                                    var isSuccess = true;
                                                    $
                                                            .ajax({
                                                                type: 'POST',
                                                                data: {
                                                                    'ticketId': ticketId
                                                                },
                                                                url: pageContextPath
                                                                        + '/hdActiveTicket/isTicketAvailableForDrop.html',
                                                                async: false,
                                                                success: function (
                                                                        data) {
                                                                    if (data == "success") {
                                                                        isSuccess = true;
                                                                    } else {
                                                                        alert(data);
                                                                        isSuccess = false;
                                                                    }
                                                                }
                                                            });

                                                    if (!isSuccess) { // failed
                                                        ui.draggable
                                                                .animate(
                                                                        ui.draggable
                                                                        .data().origPosition,
                                                                        "slow");
                                                        return;
                                                    }

                                                    $(this)
                                                            .ticketDrop(
                                                                    event,
                                                                    ui);
                                                }
                                            });


                        }
                        ,
                        "sDom": 'tir'
                    });



    hdTables.hdpendingackcustTable = $('#hdPendingAckCustTable')
            .dataTable({
                "bDestroy": true,
                "iDisplayLength": -1,
                "aoColumnDefs": [{
                        "bSortable": false,
                        "aTargets": [4]
                    }],
                "sAjaxSource": 'hdPendingAckCust.html',
                "aoColumns": [{
                        "mDataProp": "openedDate",
                        "sClass": "textCenter"
                    }, {
                        "mDataProp": "siteNumberDisplay",
                        "sClass": "siteNumberTextPendingCubes"
                    }, {
                        "mDataProp": "source"
                    }, {
                        "mDataProp": "customer"
                    }, {
                        "mDataProp": "lastTd"
                    }],
                "fnInitComplete": function (oSettings, json) {

                },
                "sDom": 'tir'

            });

    hdTables.hdpendingacksecurityTable = $('#hdPendingAckSecurityTable').dataTable({
        "bDestroy": true,
        "iDisplayLength": -1,
        "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": [4]
            }],
        "sAjaxSource": 'hdPendingAckSecurity.html',
        "aoColumns": [{
                "mDataProp": "openedDate",
                "sClass": "textCenter"
            }, {
                "mDataProp": "siteNumberDisplay",
                "sClass": "siteNumberTextPendingCubes"
            }, {
                "mDataProp": "source"
            }, {
                "mDataProp": "customer"
            }, {
                "mDataProp": "lastTd"
            }],
        "fnInitComplete": function (oSettings, json) {

        },
        "sDom": 'tir'
    });

    hdTables.hdpendingackalertTable = $(
            '#hdPendingAckAlertTable').dataTable({
        "bDestroy": true,
        "iDisplayLength": -1,
        "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": [4]
            }],
        "sAjaxSource": 'hdPendingAckAlert.html',
        "aoColumns": [{
                "mDataProp": "openedDate",
                "sClass": "textCenter"
            }, {
                "mDataProp": "siteNumberDisplay",
                "sClass": "siteNumberTextPendingCubes"
            }, {
                "mDataProp": "state"
            }, {
                "mDataProp": "customer"
            }, {
                "mDataProp": "lastTd"
            }],
        "fnInitComplete": function (oSettings, json) {

        },
        "sDom": 'tir'

    });

    hdTables.hdpendingackmonitorTable = $(
            '#hdPendingAckMonitorTable')
            .dataTable(
                    {
                        "bDestroy": true,
                        "iDisplayLength": -1,
                        "aoColumnDefs": [{
                                "bSortable": false,
                                "aTargets": [4]
                            }],
                        "sAjaxSource": 'hdPendingAckMonitor.html',
                        "aoColumns": [
                            {
                                "mDataProp": "openedDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "siteNumberDisplay",
                                "sClass": "siteNumberTextPendingCubes"
                            },
                            {
                                "mDataProp": "state"
                            },
                            {
                                "mDataProp": "customer"
                            }, {
                                "mDataProp": "lastTd"
                            }],
                        "fnInitComplete": function (oSettings,
                                json) {

                            setPendingAckMonitoringFooterDetail();

                        },
                        "sDom": 'tir'

                    });

    $(".wipTicketsInActive")
            .live(
                    'click',
                    function () {

                        if ($(this).attr("ticketId") == undefined) {
                            alert("Please Drag a Ticket From Help Desk WIP ");
                            return false;
                        }

                        if (!setPreviousTicket()) {
                            return false;
                        }
                        resetActiveForm();

                        if ($(this).hasClass(
                                'wipTicketsActiveChanged')) {
                            $("#updateTicketButton")
                                    .removeClass('FMBUT0003')
                                    .addClass('FMBUT0005');
                            var efforts = $(this).attr(
                                    "recordedEfforts");
                            var nextActionUnit = $(this).attr(
                                    "nextActionUnit");
                            var workNotes = $(this).attr(
                                    "workNotes");

                            $("#ticketWorkNotes")
                                    .val(workNotes);

                            if (efforts != "undefined"
                                    || efforts != undefined) {
                                $("#effortMinutes")
                                        .val(efforts);

                            }
                            if (nextActionUnit == "date") {
                                $("#wipNextActionDueDate")
                                        .val(
                                                $(this)
                                                .attr(
                                                        "nextActionTime"));

                            } else if (nextActionUnit == "Hrs") {
                                $(
                                        "input[type='radio'][name='nextActionTime']")
                                        .filter('[value=Hrs]')
                                        .attr('checked', true);
                                $("#nextActionTime")
                                        .val(
                                                $(this)
                                                .attr(
                                                        "nextActionTime"));

                            } else {
                                $("#nextActionTime")
                                        .val(
                                                $(this)
                                                .attr(
                                                        "nextActionTime"));
                                $(
                                        "input[type='radio'][name='nextActionTime']")
                                        .filter('[value=Mins]')
                                        .attr('checked', true);

                            }

                        }

                        $(".wipTicketsActive")
                                .each(
                                        function () {
                                            $(this)
                                                    .toggleClass(
                                                            'wipTicketsActive');
                                            $(this)
                                                    .toggleClass(
                                                            'wipTicketsInActive');
                                        });
                        $(this).toggleClass(
                                'wipTicketsInActive');
                        $(this).toggleClass('wipTicketsActive');
                        $("#activeTicketHeader").removeClass(
                                "headerPriority1");
                        $("#activeTicketHeader").removeClass(
                                "headerPriority3");
                        if (($(this).attr("siteStatus") == 'down') || ($(this).attr("circuitStatus") == '0') || ($(this).attr("overdueStatus") == 'overdue')) {
                            $("#activeTicketHeader").addClass(
                                    "headerPriority1");
                        }
                        else {
                            $("#activeTicketHeader").addClass(
                                    "headerPriority3");
                        }

                        var ticketId = $(this).attr("ticketId");

                        $(
                                '#hdWipTable tbody tr, #hdWipOverDueTable tbody tr, #hdWipNextActionTable tbody tr')
                                .each(
                                        function () {
                                            if ($(this).attr(
                                                    "ticketid") == ticketId) {

                                                var shortDescription = $(
                                                        this)
                                                        .attr(
                                                                'shortdescription');
                                                var nextAction = $(
                                                        this)
                                                        .attr(
                                                                'nextaction');
                                                var nextActionDueDate = $(
                                                        this)
                                                        .attr(
                                                                'nextactionduedate');
                                                var siteContact = $(
                                                        this)
                                                        .attr(
                                                                'contactname');
                                                var contactNumber = $(
                                                        this)
                                                        .attr(
                                                                'contactphonenumber');
                                                var currentTicketNextActionId = $(
                                                        this)
                                                        .attr(
                                                                'nextActionId');
                                                var customer = $(
                                                        this)
                                                        .attr(
                                                                'customer');
                                                var siteNumber = $(
                                                        this)
                                                        .attr(
                                                                'sitenumber');


                                                var ticketState = $(
                                                        this)
                                                        .attr(
                                                                'ticketState');
                                                var backUpStatus = $(
                                                        this)
                                                        .attr(
                                                                'circuitStatus');
                                                var priority = $(
                                                        this)
                                                        .attr(
                                                                'priority');
                                                var headerText = priority
                                                        + " - "
                                                        + siteNumber
                                                        + " - "
                                                        + customer
                                                        + '<span style="display:none;" id="headerIlexTicketId">'
                                                        + ticketId
                                                        + '</span>';

                                                $(
                                                        "#ticketDescription")
                                                        .text(
                                                                shortDescription);
                                                $(
                                                        "#siteContact")
                                                        .html(
                                                                "&nbsp;");
                                                $(
                                                        "#contactNumber")
                                                        .html(
                                                                "&nbsp;");
                                                $(
                                                        "#siteContact")
                                                        .text(
                                                                siteContact);
                                                $(
                                                        "#contactNumber")
                                                        .text(
                                                                contactNumber);
                                                $(
                                                        "#ticketNextAction")
                                                        .text(
                                                                nextAction);
                                                $("#ticketNextActionWIP").val(currentTicketNextActionId);
                                                $(
                                                        "#currentTicketNextActionId")
                                                        .val(
                                                                currentTicketNextActionId);
                                                $(
                                                        "#ticketNextActionDueDate")
                                                        .text(
                                                                nextActionDueDate);
                                                $(
                                                        "#ticketState")
                                                        .val(
                                                                ticketState);

                                                if ($(this).attr("ticketSource") == '2') {

                                                    $("#backUpStatusDiv").show();

                                                    if (backUpStatus == "1") {
                                                        $("input[name=backUpStatus]")[0].checked = true;
                                                    } else if (backUpStatus == "0") {
                                                        $("input[name=backUpStatus]")[1].checked = true;
                                                    } else if (backUpStatus == "99") {
                                                        $("input[name=backUpStatus]")[2].checked = true;
                                                    }

                                                    if ($(this).attr("device")) {
                                                        $("#deviceDiv").text($(this).attr("device"));
                                                    } else {
                                                        $("#deviceDiv").text("Unknown");
                                                    }

                                                } else {
                                                    $("#backUpStatusDiv").hide();
                                                }

                                                $(
                                                        "#activeTicketTitle")
                                                        .html(
                                                                headerText);
                                                $(
                                                        "#activeTicketDiv")
                                                        .show();

                                                $
                                                        .ajax({
                                                            type: 'POST',
                                                            data: {
                                                                "ticketId": ticketId
                                                            },
                                                            url: pageContextPath
                                                                    + '/hdActiveTicket/getTicketDetails.html',
                                                            success: function (
                                                                    obj) {
                                                                var data = $
                                                                        .parseJSON(obj);

                                                                $(
                                                                        "#ticketStatus")
                                                                        .html(
                                                                                data.ticketStatus);
                                                                $(
                                                                        "#sumOfEfforts")
                                                                        .val(
                                                                                data.sumOfEfforts);
                                                            }
                                                        });
                                                saveActiveTicket(ticketId);
                                                //show latest work notes
                                                showLatestWorkNotesForActiveTicket(ticketId);
                                            }
                                        });
                    });

    $("#dialog").dialog({
        autoOpen: false,
        modal: true
    });

    $("#nextActionTime")
            .change(
                    function () {
                        if ($(
                                "input[type='radio'][name='nextActionTime']")
                                .is(':checked')) {

                        } else {
                            $(
                                    "input[type='radio'][name='nextActionTime']")
                                    .prop("checked", true);
                        }
                        ticketchanged.nextActionChanged = true;
                        $(".wipTicketsActive").addClass(
                                "wipTicketsActiveChanged");

                    });

    $("#createTicketNextActionTime")
            .live('change',
                    function () {
                        if ($(
                                "input[type='radio'][name='createTicketNextActionUnit']")
                                .is(':checked')) {

                        } else {
                            $(
                                    "input[type='radio'][name='createTicketNextActionUnit']")[1].checked = true;
                        }

                    });

    $("#nextActionTimeTicketSummary")
            .live('change',
                    function () {
                        if ($(
                                "input[type='radio'][name='nextActionTimeTicketSummary']")
                                .is(':checked')) {

                        } else {
                            $(
                                    "input[type='radio'][name='nextActionTimeTicketSummary']")[1].checked = true;
                        }

                    });

    $("#effortMinutes").keypress(
            function () {
                $("#updateTicketButton").removeClass(
                        'FMBUT0003').addClass('FMBUT0005');
                ticketchanged.recordEffortChanged = true;
                $(".wipTicketsActive").addClass(
                        "wipTicketsActiveChanged");

            });
    $("#effortMinutes").live('paste', function () {
        $("#updateTicketButton").removeClass(
                'FMBUT0003').addClass('FMBUT0005');
        ticketchanged.recordEffortChanged = true;
        $(".wipTicketsActive").addClass(
                "wipTicketsActiveChanged");

    });

    $("#wipNextActionDueDate").change(
            function () {
                ticketchanged.nextActionChanged = true;
                $(".wipTicketsActive").addClass(
                        "wipTicketsActiveChanged");
                $("#updateTicketButton").removeClass(
                        'FMBUT0003').addClass('FMBUT0005');

            });

    $("#ticketWorkNotes").keypress(
            function () {
                ticketchanged.workNotesChanged = true;
                $(".wipTicketsActive").addClass(
                        "wipTicketsActiveChanged");
                $("#updateTicketButton").removeClass(
                        'FMBUT0003').addClass('FMBUT0005');

            });
    $("#ticketWorkNotes").live('paste', function () {
        ticketchanged.workNotesChanged = true;
        $(".wipTicketsActive").addClass(
                "wipTicketsActiveChanged");
        $("#updateTicketButton").removeClass(
                'FMBUT0003').addClass('FMBUT0005');

    });

    /* function :To Minimize or Maximize cube -start */
    $("#minMax")
            .live(
                    'click',
                    function () {
                        //alert("min max 1 clicked");
                        $minMaxImage = $(this);
                        var tableId = $minMaxImage.parents(
                                'div').find('table').attr('id');
                        var cubeId = tableId + "_wrapper";
                        var containerTitleWidth = $minMaxImage
                                .parents('div.containerTitle')
                                .width();
                        $('#' + cubeId).toggle();
                        if ($minMaxImage.attr("src") == pageContextPath
                                + '/resources/images/sm-scr.png') {
                            /*$minMaxImage
                             .parents(
                             'div.containerTitle')
                             .width(
                             containerTitleWidth / 2);

                             */

                            $minMaxImage
                                    .parents(
                                            'div.containerTitle')
                                    .width(
                                            "100%");

                            $minMaxImage.parents(
                                    'div.containerTitle').find(
                                    "div.dataTables_paginate")
                                    .hide();

                            $minMaxImage
                                    .attr(
                                            "src",
                                            pageContextPath
                                            + '/resources/images/full-scr.png');
                            if (tableId == 'hdPendingAckCustTable' || tableId == 'hdPendingAckSecurityTable') {
//								$minMaxImage
//										.parents(
//												'div.containerTitle')
//										.width(
//												containerTitleWidth / 2 + 120);
                                $minMaxImage
                                        .parents(
                                                'div.containerTitle')
                                        .width(
                                                "100%");
                                if ($('#hdPendingAckAlertDiv')
                                        .find(
                                                'div.containerTitle')
                                        .width() > $(
                                        '#hdPendingAckCustDiv')
                                        .find(
                                                'div.containerTitle')
                                        .width()) {
                                    $("#leftDiv")
                                            .width(
                                                    $(
                                                            "#hdPendingAckAlertDiv")
                                                    .width() + 15);
                                } else {
                                    if ($(
                                            '#hdPendingAckMonitorDiv')
                                            .find(
                                                    'div.containerTitle')
                                            .width() > $(
                                            '#hdPendingAckCustDiv')
                                            .find(
                                                    'div.containerTitle')
                                            .width()) {
                                        $("#leftDiv")
                                                .width(
                                                        $(
                                                                "#hdPendingAckMonitorDiv")
                                                        .width() + 15);
                                    } else {
                                        $("#leftDiv")
                                                .width(
                                                        $(
                                                                "#hdPendingAckCustDiv")
                                                        .width() + 15);
                                    }
                                }
                            } else {
                                if (tableId == 'hdPendingAckAlertTable') {
//									$minMaxImage
//											.parents(
//													'div.containerTitle')
//											.width(
//													containerTitleWidth / 2 + 120);
                                    $minMaxImage
                                            .parents(
                                                    'div.containerTitle')
                                            .width(
                                                    "100%");
                                    if ($(
                                            '#hdPendingAckCustDiv')
                                            .find(
                                                    'div.containerTitle')
                                            .width() > $(
                                            '#hdPendingAckAlertDiv')
                                            .find(
                                                    'div.containerTitle')
                                            .width()) {
                                        $("#leftDiv")
                                                .width(
                                                        $(
                                                                "#hdPendingAckCustDiv")
                                                        .width() + 15);
                                    } else {
                                        if ($(
                                                '#hdPendingAckMonitorDiv')
                                                .find(
                                                        'div.containerTitle')
                                                .width() > $(
                                                '#hdPendingAckAlertDiv')
                                                .find(
                                                        'div.containerTitle')
                                                .width()) {
                                            $("#leftDiv")
                                                    .width(
                                                            $(
                                                                    "#hdPendingAckMonitorDiv")
                                                            .width() + 15);
                                        } else {
                                            $("#leftDiv")
                                                    .width(
                                                            $(
                                                                    "#hdPendingAckAlertDiv")
                                                            .width() + 15);
                                        }
                                    }
                                } else {
                                    if (tableId == 'hdPendingAckMonitorTable') {
//										$minMaxImage
//												.parents(
//														'div.containerTitle')
//												.width(
//														containerTitleWidth / 2 + 120);
                                        $minMaxImage
                                                .parents(
                                                        'div.containerTitle')
                                                .width(
                                                        "100%");
                                        if ($(
                                                '#hdPendingAckCustDiv')
                                                .find(
                                                        'div.containerTitle')
                                                .width() > $(
                                                '#hdPendingAckMonitorDiv')
                                                .find(
                                                        'div.containerTitle')
                                                .width()) {
                                            $("#leftDiv")
                                                    .width(
                                                            $(
                                                                    "#hdPendingAckCustDiv")
                                                            .width() + 15);
                                        } else {
                                            if ($(
                                                    '#hdPendingAckAlertDiv')
                                                    .find(
                                                            'div.containerTitle')
                                                    .width() > $(
                                                    '#hdPendingAckMonitorDiv')
                                                    .find(
                                                            'div.containerTitle')
                                                    .width()) {
                                                $("#leftDiv")
                                                        .width(
                                                                $(
                                                                        "#hdPendingAckAlertDiv")
                                                                .width() + 15);
                                            } else {
                                                $("#leftDiv")
                                                        .width(
                                                                $(
                                                                        "#hdPendingAckMonitorDiv")
                                                                .width() + 15);
                                            }
                                        }
                                    } else {
                                        if ($(
                                                '#hdWipDivOverDue')
                                                .find(
                                                        'div.containerTitle')
                                                .width() > $(
                                                '#hdWipDiv')
                                                .find(
                                                        'div.containerTitle')
                                                .width()) {
                                            $('#middleDiv')
                                                    .width(
                                                            (($(
                                                                    '#hdWipDivOverDue')
                                                                    .find(
                                                                            'div.containerTitle')
                                                                    .width()) + 15)
                                                            + "px");
                                            ;
                                        } else {
                                            $('#middleDiv')
                                                    .width(
                                                            (($(
                                                                    '#hdWipDiv')
                                                                    .find(
                                                                            'div.containerTitle')
                                                                    .width()) + 15)
                                                            + "px");

                                        }
                                    }
                                }
                            }
                        } else {
                            $minMaxImage
                                    .parents(
                                            'div.containerTitle')
                                    .width(
                                            $minMaxImage
                                            .parents(
                                                    'div.containerTitle')
                                            .attr(
                                                    'initialWidth'));

                            $minMaxImage.parents(
                                    'div.containerTitle').find(
                                    "div.dataTables_paginate")
                                    .show();

                            $minMaxImage
                                    .attr(
                                            "src",
                                            pageContextPath
                                            + '/resources/images/sm-scr.png');
                            if (tableId == 'hdPendingAckCustTable' || tableId == 'hdPendingAckSecurityTable') {
                                $("#leftDiv")
                                        .width(
                                                (($(
                                                        "#hdPendingAckCustDiv")
                                                        .find(
                                                                'div.containerTitle')
                                                        .width()) + 15)
                                                + "px");
                                ;

                            } else {
                                if (tableId == 'hdPendingAckAlertTable') {
                                    $("#leftDiv")
                                            .width(
                                                    (($(
                                                            "#hdPendingAckAlertDiv")
                                                            .find(
                                                                    'div.containerTitle')
                                                            .width()) + 15)
                                                    + "px");
                                    ;
                                } else {
                                    if (tableId == 'hdPendingAckMonitorTable') {
                                        $("#leftDiv")
                                                .width(
                                                        (($(
                                                                "#hdPendingAckMonitorDiv")
                                                                .find(
                                                                        'div.containerTitle')
                                                                .width()) + 15)
                                                        + "px");
                                        ;
                                    } else {
                                        if ($(
                                                '#hdWipDivOverDue')
                                                .find(
                                                        'div.containerTitle')
                                                .width() > $(
                                                '#hdWipDiv')
                                                .find(
                                                        'div.containerTitle')
                                                .width()) {

                                            $('#middleDiv')
                                                    .width(
                                                            (($(
                                                                    '#hdWipDivOverDue')
                                                                    .find(
                                                                            'div.containerTitle')
                                                                    .width()) + 15)
                                                            + "px");
                                            ;
                                        } else {
                                            $('#middleDiv')
                                                    .width(
                                                            (($(
                                                                    '#hdWipDiv')
                                                                    .find(
                                                                            'div.containerTitle')
                                                                    .width()) + 15)
                                                            + "px");

                                        }
                                    }
                                }
                            }
                        }

                        $(".contentWrapper").width(
                                $("#leftDiv").width()
                                + $("#middleDiv")
                                .width()
                                + $("#rightDiv")
                                .width() + 40);
                        if ($(window).width() > $(".contentWrapper").width())
                        {
                            $("#hdBreadCrumbTable").width($(window).width());
                        }
                        else {
                            $("#hdBreadCrumbTable").width($(".contentWrapper").width() + 20);
                        }

                    }


            );
    /* function :To Minimize or Maximize cube -end */

//alert("ajax poll on hdload1");
    processUsingAjaxPoll();

    $("#ticketState").change(function () {
        ticketchanged.ticketStateChanged = true;
    });

    $("input[name=backUpStatus]").change(function () {
        ticketchanged.circuitStatusChanged = true;
    });







    appendSiteDetail();
    disableResolvedDraggable();


}


function openSiteDetailPopup(siteNum, mmId) {
    siteNum = siteNum.replace("#", "-,-,-");
    window.open('/Ilex/masterSiteAction.do?siteName=' + siteNum + '&mmId=' + mmId, '_blank', 'width=1100,height=700,scrollbars=yes');
}


function appendSiteDetail() {

//	$('.siteNumberText:not(#siteTh)').each(function(i, v) {
//	    var text = $(v).text();
//		var textInner = $(v).text();
//
//		$(v)
//		  .contents()
//		  .filter(function() {
//		    return this.nodeType == 3; // if the node type is text.
//		  }).remove();
//
//		textInner = textInner.replace("#","-,-,-");
//
////		var str = "'/Ilex/masterSiteAction.do?siteName="+$.trim(textInner)+"&mmId=0', '_blank', 'width=1100,height=700,scrollbars=yes'";
////		var strSite =  '<a style="float:left; width:97px;" href="javascript:void(0);" onclick="window.open('+str+');" >'+text+'</a>';
//
////		$(this).prepend(strSite);
//		$(this).prepend('<img style="float:left; margin-right:3px;" src="/Ilex-WS/resources/images/details.png" class="siteDetails"/> ');
//
//
//
//});

}


function disableResolvedDraggable() {

    $("div.tableWrapper table span").each(function (i, v) {

        $(this).draggable('disable');

    });


}





function siteSearchByNo() {

//	if(document.getElementById("siteSearchId").value == '')
//	{
//		alert("Please Enter Site Number.");
//		document.getElementById("siteSearchId").focus();
//		return false;
//	}

    $("#ui-datepicker-div").css("display", "none");
    hdTables.hdWipTable.fnDestroy();
    hdTables.hdWipoverdueTable.fnDestroy();
    hdTables.hdWipNextActionTable.fnDestroy();
    hdTables.hdResolvedTable.fnDestroy();
    hdTables.hdpendingackcustTable.fnDestroy();
    hdTables.hdpendingacksecurityTable.fnDestroy();
    hdTables.hdpendingackalertTable.fnDestroy();
    hdTables.hdpendingackmonitorTable.fnDestroy();



    hdTables.hdWipTable = $('#hdWipTable')
            .dataTable(
                    {
                        "bDestroy": true,
                        "iDisplayLength": -1,
//						"aoColumnDefs" : [ {
//							"bSortable" : true,
//							"aTargets" : [ 10 ]
//						} ],

                        "aoColumnDefs": [{
                                "bSortable": true,
                                "aTargets": [0]
                            }, {
                                "bSortable": true,
                                "aTargets": [1]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [2]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [3]
                            }, {
                                "bSortable": true,
                                "aTargets": [4]
                            }, {
                                "bSortable": true,
                                "aTargets": [5]
                            }, {
                                "bSortable": true,
                                "aTargets": [6]
                            }, {
                                "bSortable": true,
                                "aTargets": [7]
                            }, {
                                "bSortable": true,
                                "aTargets": [8]
                            }, {
                                "bSortable": true,
                                "aTargets": [9]
                            }, {
                                "bSortable": true,
                                "aTargets": [10]
                            }, {
                                "bSortable": true,
                                "aTargets": [11]
                            }, {
                                "bSortable": false,
                                "aTargets": [12]
                            }],
                        "sAjaxSource": 'hdWipTable.html?searchVal=' + document.getElementById("siteSearchId").value,
                        "aoColumns": [
                            {
                                "mDataProp": "priority",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "openedDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "ticketNumber",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "siteNumberDisplay",
                                "sClass": "siteNumberText"
                            },
                            {
                                "mDataProp": "status"
                            },
                            {
                                "mDataProp": "nextActionForDisplay"
                            },
                            {
                                "mDataProp": "nextActionDueDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "cca_tssForDisplay"
                            },
                            {
                                "mDataProp": "shortDescriptionDisplay",
                                "sClass": "shortDescriptionText"
                            },
                            {
                                "mDataProp": "customerForDisplay"
                            },
                            {
                                "mDataProp": "assignmentGroupName"
                            },
                            {
                                "mDataProp": "clientType"
                            },
                            {
                                "mDataProp": "lastTd"
                            }],
                        "fnInitComplete": function (oSettings,
                                json) {

                            // setActiveTickets();
                        },
                        "fnDrawCallback": function () {
                            $("span.greenIcon,span.redIcon")
                                    .filter(':visible')
                                    .draggable(
                                            {
                                                revert: 'invalid',
                                                helper: function (
                                                        event) {

                                                    var $ret = $('<div/>');
                                                    $ret
                                                            .addClass('helperDiv');
                                                    var ilexNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'ilexnumber');
                                                    $($ret)
                                                            .text(
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "ilexNumber",
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "priority",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'priority'));
                                                    $($ret)
                                                            .attr(
                                                                    "ticketId",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'ticketId'));

                                                    var siteNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'siteNumber');


                                                    $($ret)
                                                            .attr(
                                                                    "siteNumber",
                                                                    siteNumber);

                                                    $($ret)
                                                            .attr(
                                                                    "customer",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customer'));
                                                    $($ret)
                                                            .attr(
                                                                    "customerNumber",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customerNumber'));
                                                    $($ret)
                                                            .attr(
                                                                    "circuitStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'circuitStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "overdueStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'overdueStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "siteStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'siteStatus'));

                                                    return $ret;
                                                },
                                                distance: 0,
                                                start: function () {
                                                    $(this)
                                                            .data(
                                                                    "origPosition",
                                                                    $(
                                                                            this)
                                                                    .position());
                                                }
                                            });

                            $("span.redIcon").draggable(
                                    "disable", 1);

                            $(
                                    "#leftWIPTickets div,#rightWIPTickets div")
                                    .droppable(
                                            {
                                                accept: function (
                                                        d) {

                                                    // checkIsTicketAvaiableForDrop(d,this);
                                                    if ($(this)
                                                            .text()
                                                            .indexOf(
                                                                    "Drop") == "0") {
                                                        return true;
                                                    } else {
                                                        return false;
                                                    }

                                                },
                                                drop: function (
                                                        event,
                                                        ui) {
                                                    var ticketId = ui.helper
                                                            .attr("ticketId");
                                                    var isSuccess = true;
                                                    $
                                                            .ajax({
                                                                type: 'POST',
                                                                data: {
                                                                    'ticketId': ticketId
                                                                },
                                                                url: pageContextPath
                                                                        + '/hdActiveTicket/isTicketAvailableForDrop.html',
                                                                async: false,
                                                                success: function (
                                                                        data) {
                                                                    if (data == "success") {
                                                                        isSuccess = true;
                                                                    } else {
                                                                        alert(data);
                                                                        isSuccess = false;
                                                                    }
                                                                }
                                                            });

                                                    if (!isSuccess) { // failed
                                                        ui.draggable
                                                                .animate(
                                                                        ui.draggable
                                                                        .data().origPosition,
                                                                        "slow");
                                                        return;
                                                    }

                                                    $(this)
                                                            .ticketDrop(
                                                                    event,
                                                                    ui);
                                                }

                                            });

                        },
                        "sDom": 'tir'

                    });


    hdTables.hdWipNextActionTable = $('#hdWipNextActionTable')
            .dataTable(
                    {
                        "bDestroy": true,
                        "aaSorting": [],
                        "iDisplayLength": -1,
                        "aoColumnDefs": [{
                                "bSortable": true,
                                "aTargets": [0]
                            }, {
                                "bSortable": true,
                                "aTargets": [1]
                            }, {
                                "bSortable": true,
                                "aTargets": [2]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [3]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [4]
                            }, {
                                "bSortable": true,
                                "aTargets": [5]
                            }, {
                                "bSortable": true,
                                "aTargets": [6]
                            }, {
                                "bSortable": true,
                                "aTargets": [7]
                            }, {
                                "bSortable": true,
                                "aTargets": [8]
                            }, {
                                "bSortable": true,
                                "aTargets": [9]
                            }, {
                                "bSortable": true,
                                "aTargets": [10]
                            }, {
                                "bSortable": true,
                                "aTargets": [11]
                            }, {
                                "bSortable": false,
                                "aTargets": [12]
                            }],
                        "sAjaxSource": 'hdWipNextActionTable.html?searchVal=' + document.getElementById("siteSearchId").value,
                        "aoColumns": [
                            {
                                "mDataProp": "priority",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "openedDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "ticketNumber",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "siteNumberDisplay",
                                "sClass": "siteNumberText"
                            },
                            {
                                "mDataProp": "status",
                            },
                            {
                                "mDataProp": "nextActionForDisplay"
                            },
                            {
                                "mDataProp": "nextActionDueDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "cca_tssForDisplay"
                            },
                            {
                                "mDataProp": "shortDescriptionDisplay",
                                "sClass": "shortDescriptionText"
                            },
                            {
                                "mDataProp": "customerForDisplay"
                            },
                            {
                                "mDataProp": "assignmentGroupName"
                            },
                            {
                                "mDataProp": "clientType"
                            },
                            {
                                "mDataProp": "lastTd"
                            }],
                        "fnInitComplete": function (oSettings,
                                json) {

                            //setActiveTickets();
                        },
                        "fnDrawCallback": function () {
                            $("span.greenIcon,span.redIcon")
                                    .filter(':visible')
                                    .draggable(
                                            {
                                                revert: 'invalid',
                                                helper: function (
                                                        event) {

                                                    var $ret = $('<div/>');
                                                    $ret
                                                            .addClass('helperDiv');
                                                    var ilexNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'ilexnumber');
                                                    $($ret)
                                                            .text(
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "ilexNumber",
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "priority",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'priority'));
                                                    $($ret)
                                                            .attr(
                                                                    "ticketId",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'ticketId'));
                                                    var siteNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'siteNumber');


                                                    $($ret)
                                                            .attr(
                                                                    "siteNumber",
                                                                    siteNumber);

                                                    $($ret)
                                                            .attr(
                                                                    "customer",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customer'));
                                                    $($ret)
                                                            .attr(
                                                                    "customerNumber",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customerNumber'));
                                                    $($ret)
                                                            .attr(
                                                                    "circuitStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'circuitStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "overdueStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'overdueStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "siteStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'siteStatus'));

                                                    return $ret;
                                                },
                                                distance: 0,
                                                start: function () {
                                                    $(this)
                                                            .data(
                                                                    "origPosition",
                                                                    $(
                                                                            this)
                                                                    .position());
                                                }
                                            });
                            $("span.redIcon").draggable(
                                    "disable", 1);

                            $(
                                    "#leftWIPTickets div,#rightWIPTickets div")
                                    .droppable(
                                            {
                                                accept: function (
                                                        d) {

                                                    if ($(this)
                                                            .text()
                                                            .indexOf(
                                                                    "Drop") == "0") {
                                                        return true;
                                                    } else {
                                                        return false;
                                                    }

                                                },
                                                drop: function (
                                                        event,
                                                        ui) {
                                                    var ticketId = ui.helper
                                                            .attr("ticketId");
                                                    var isSuccess = true;
                                                    $
                                                            .ajax({
                                                                type: 'POST',
                                                                data: {
                                                                    'ticketId': ticketId
                                                                },
                                                                url: pageContextPath
                                                                        + '/hdActiveTicket/isTicketAvailableForDrop.html',
                                                                async: false,
                                                                success: function (
                                                                        data) {
                                                                    if (data == "success") {
                                                                        isSuccess = true;
                                                                    } else {
                                                                        alert(data);
                                                                        isSuccess = false;
                                                                    }
                                                                }
                                                            });

                                                    if (!isSuccess) { // failed
                                                        ui.draggable
                                                                .animate(
                                                                        ui.draggable
                                                                        .data().origPosition,
                                                                        "slow");
                                                        return;
                                                    }

                                                    $(this)
                                                            .ticketDrop(
                                                                    event,
                                                                    ui);
                                                }
                                            });


                        }
                        ,
                        "sDom": 'tir'
                    });





////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    hdTables.hdResolvedTable = $('#hdResolvedTable')
            .dataTable(
                    {
                        "bDestroy": true,
                        "aaSorting": [],
                        "iDisplayLength": -1,
                        "aoColumnDefs": [{
                                "bSortable": true,
                                "aTargets": [0]
                            }, {
                                "bSortable": true,
                                "aTargets": [1]
                            }, {
                                "bSortable": true,
                                "aTargets": [2]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [3]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [4]
                            }, {
                                "bSortable": true,
                                "aTargets": [5]
                            }, {
                                "bSortable": true,
                                "aTargets": [6]
                            }, {
                                "bSortable": true,
                                "aTargets": [7]
                            }, {
                                "bSortable": true,
                                "aTargets": [8]
                            }, {
                                "bSortable": true,
                                "aTargets": [9]
                            }, {
                                "bSortable": true,
                                "aTargets": [10]
                            }, {
                                "bSortable": true,
                                "aTargets": [11]
                            }, {
                                "bSortable": false,
                                "aTargets": [12]
                            }],
                        "sAjaxSource": 'hdResolvedTable.html?searchVal=' + document.getElementById("siteSearchId").value,
                        "aoColumns": [
                            {
                                "mDataProp": "priority",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "updatedDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "ticketNumber",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "siteNumberDisplay",
                                "sClass": "siteNumberText"
                            },
                            {
                                "mDataProp": "status",
                            },
                            {
                                "mDataProp": "nextActionForDisplay"
                            },
                            {
                                "mDataProp": "nextActionDueDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "cca_tssForDisplay"
                            },
                            {
                                "mDataProp": "shortDescriptionDisplay",
                                "sClass": "shortDescriptionText"
                            },
                            {
                                "mDataProp": "customerForDisplay"
                            },
                            {
                                "mDataProp": "assignmentGroupName"
                            },
                            {
                                "mDataProp": "clientType"
                            },
                            {
                                "mDataProp": "lastTd"
                            }],
                        "fnInitComplete": function (oSettings,
                                json) {

                            //setActiveTickets();
                        },
                        "fnDrawCallback": function () {
                            $("span.greenIcon,span.redIcon")
                                    .filter(':visible')
                                    .draggable(
                                            {
                                                revert: 'invalid',
                                                helper: function (
                                                        event) {

                                                    var $ret = $('<div/>');
                                                    $ret
                                                            .addClass('helperDiv');
                                                    var ilexNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'ilexnumber');
                                                    $($ret)
                                                            .text(
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "ilexNumber",
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "priority",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'priority'));
                                                    $($ret)
                                                            .attr(
                                                                    "ticketId",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'ticketId'));
                                                    var siteNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'siteNumber');


                                                    $($ret)
                                                            .attr(
                                                                    "siteNumber",
                                                                    siteNumber);

                                                    $($ret)
                                                            .attr(
                                                                    "customer",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customer'));
                                                    $($ret)
                                                            .attr(
                                                                    "customerNumber",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customerNumber'));
                                                    $($ret)
                                                            .attr(
                                                                    "circuitStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'circuitStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "overdueStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'overdueStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "siteStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'siteStatus'));

                                                    return $ret;
                                                },
                                                distance: 0,
                                                start: function () {
                                                    $(this)
                                                            .data(
                                                                    "origPosition",
                                                                    $(
                                                                            this)
                                                                    .position());
                                                }
                                            });
                            $("span.redIcon").draggable(
                                    "disable", 1);

                            $(
                                    "#leftWIPTickets div,#rightWIPTickets div")
                                    .droppable(
                                            {
                                                accept: function (
                                                        d) {

                                                    if ($(this)
                                                            .text()
                                                            .indexOf(
                                                                    "Drop") == "0") {
                                                        return true;
                                                    } else {
                                                        return false;
                                                    }

                                                },
                                                drop: function (
                                                        event,
                                                        ui) {
                                                    var ticketId = ui.helper
                                                            .attr("ticketId");
                                                    var isSuccess = true;
                                                    $
                                                            .ajax({
                                                                type: 'POST',
                                                                data: {
                                                                    'ticketId': ticketId
                                                                },
                                                                url: pageContextPath
                                                                        + '/hdActiveTicket/isTicketAvailableForDrop.html',
                                                                async: false,
                                                                success: function (
                                                                        data) {
                                                                    if (data == "success") {
                                                                        isSuccess = true;
                                                                    } else {
                                                                        alert(data);
                                                                        isSuccess = false;
                                                                    }
                                                                }
                                                            });

                                                    if (!isSuccess) { // failed
                                                        ui.draggable
                                                                .animate(
                                                                        ui.draggable
                                                                        .data().origPosition,
                                                                        "slow");
                                                        return;
                                                    }

                                                    $(this)
                                                            .ticketDrop(
                                                                    event,
                                                                    ui);
                                                }
                                            });


                        }
                        ,
                        "sDom": 'tir'
                    });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////










    hdTables.hdWipoverdueTable = $('#hdWipOverDueTable')
            .dataTable(
                    {
                        "bDestroy": true,
                        "aaSorting": [],
                        "iDisplayLength": -1,
                        "aoColumnDefs": [{
                                "bSortable": true,
                                "aTargets": [0]
                            }, {
                                "bSortable": true,
                                "aTargets": [1]
                            }, {
                                "bSortable": true,
                                "aTargets": [2]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [3]
                            },
                            {
                                "bSortable": true,
                                "aTargets": [4]
                            }, {
                                "bSortable": true,
                                "aTargets": [5]
                            }, {
                                "bSortable": true,
                                "aTargets": [6]
                            }, {
                                "bSortable": true,
                                "aTargets": [7]
                            }, {
                                "bSortable": true,
                                "aTargets": [8]
                            }, {
                                "bSortable": true,
                                "aTargets": [9]
                            }, {
                                "bSortable": true,
                                "aTargets": [10]
                            }, {
                                "bSortable": true,
                                "aTargets": [11]
                            }
                            , {
                                "bSortable": false,
                                "aTargets": [12]
                            }],
                        "sAjaxSource": 'hdWipOverDueTable.html?searchVal=' + document.getElementById("siteSearchId").value,
                        "aoColumns": [
                            {
                                "mDataProp": "priority",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "openedDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "ticketNumber",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "siteNumberDisplay",
                                "sClass": "siteNumberText"
                            },
                            {
                                "mDataProp": "status",
                            },
                            {
                                "mDataProp": "nextActionForDisplay"
                            },
                            {
                                "mDataProp": "nextActionDueDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "cca_tssForDisplay"
                            },
                            {
                                "mDataProp": "shortDescriptionDisplay",
                                "sClass": "shortDescriptionText"
                            },
                            {
                                "mDataProp": "customerForDisplay"
                            },
                            {
                                "mDataProp": "assignmentGroupName"
                            }, {
                                "mDataProp": "clientType"
                            },
                            {
                                "mDataProp": "lastTd"
                            }],
                        "fnInitComplete": function (oSettings,
                                json) {

                            //setActiveTickets();
                        },
                        "fnDrawCallback": function () {
                            $("span.greenIcon,span.redIcon")
                                    .filter(':visible')
                                    .draggable(
                                            {
                                                revert: 'invalid',
                                                helper: function (
                                                        event) {

                                                    var $ret = $('<div/>');
                                                    $ret
                                                            .addClass('helperDiv');
                                                    var ilexNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'ilexnumber');
                                                    $($ret)
                                                            .text(
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "ilexNumber",
                                                                    ilexNumber);
                                                    $($ret)
                                                            .attr(
                                                                    "priority",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'priority'));
                                                    $($ret)
                                                            .attr(
                                                                    "ticketId",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'ticketId'));
                                                    var siteNumber = $(
                                                            this)
                                                            .parent()
                                                            .parent()
                                                            .attr(
                                                                    'siteNumber');


                                                    $($ret)
                                                            .attr(
                                                                    "siteNumber",
                                                                    siteNumber);

                                                    $($ret)
                                                            .attr(
                                                                    "customer",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customer'));
                                                    $($ret)
                                                            .attr(
                                                                    "customerNumber",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'customerNumber'));
                                                    $($ret)
                                                            .attr(
                                                                    "circuitStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'circuitStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "overdueStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'overdueStatus'));
                                                    $($ret)
                                                            .attr(
                                                                    "siteStatus",
                                                                    $(
                                                                            this)
                                                                    .parent()
                                                                    .parent()
                                                                    .attr(
                                                                            'siteStatus'));

                                                    return $ret;
                                                },
                                                distance: 0,
                                                start: function () {
                                                    $(this)
                                                            .data(
                                                                    "origPosition",
                                                                    $(
                                                                            this)
                                                                    .position());
                                                }
                                            });
                            $("span.redIcon").draggable(
                                    "disable", 1);

                            $(
                                    "#leftWIPTickets div,#rightWIPTickets div")
                                    .droppable(
                                            {
                                                accept: function (
                                                        d) {

                                                    if ($(this)
                                                            .text()
                                                            .indexOf(
                                                                    "Drop") == "0") {
                                                        return true;
                                                    } else {
                                                        return false;
                                                    }

                                                },
                                                drop: function (
                                                        event,
                                                        ui) {
                                                    var ticketId = ui.helper
                                                            .attr("ticketId");
                                                    var isSuccess = true;
                                                    $
                                                            .ajax({
                                                                type: 'POST',
                                                                data: {
                                                                    'ticketId': ticketId
                                                                },
                                                                url: pageContextPath
                                                                        + '/hdActiveTicket/isTicketAvailableForDrop.html',
                                                                async: false,
                                                                success: function (
                                                                        data) {
                                                                    if (data == "success") {
                                                                        isSuccess = true;
                                                                    } else {
                                                                        alert(data);
                                                                        isSuccess = false;
                                                                    }
                                                                }
                                                            });

                                                    if (!isSuccess) { // failed
                                                        ui.draggable
                                                                .animate(
                                                                        ui.draggable
                                                                        .data().origPosition,
                                                                        "slow");
                                                        return;
                                                    }

                                                    $(this)
                                                            .ticketDrop(
                                                                    event,
                                                                    ui);
                                                }
                                            });


                        }
                        ,
                        "sDom": 'tir'
                    });

    hdTables.hdpendingackcustTable = $('#hdPendingAckCustTable')
            .dataTable({
                "iDisplayLength": -1,
                "aoColumnDefs": [{
                        "bSortable": false,
                        "aTargets": [4]
                    }],
                "sAjaxSource": 'hdPendingAckCust.html?searchVal=' + document.getElementById("siteSearchId").value,
                "aoColumns": [{
                        "mDataProp": "openedDate",
                        "sClass": "textCenter"
                    }, {
                        "mDataProp": "siteNumberDisplay",
                        "sClass": "siteNumberTextPendingCubes"
                    }, {
                        "mDataProp": "source"
                    }, {
                        "mDataProp": "customer"
                    }, {
                        "mDataProp": "lastTd"
                    }],
                "fnInitComplete": function (oSettings, json) {

                },
                "sDom": 'tir'

            });

    hdTables.hdpendingacksecurityTable = $('#hdPendingAckSecurityTable').dataTable({
        "iDisplayLength": -1,
        "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": [4]
            }],
        "sAjaxSource": 'hdPendingAckSecurity.html?searchVal=' + document.getElementById("siteSearchId").value,
        "aoColumns": [{
                "mDataProp": "openedDate",
                "sClass": "textCenter"
            }, {
                "mDataProp": "siteNumberDisplay",
                "sClass": "siteNumberTextPendingCubes"
            }, {
                "mDataProp": "source"
            }, {
                "mDataProp": "customer"
            }, {
                "mDataProp": "lastTd"
            }],
        "fnInitComplete": function (oSettings, json) {
        },
        "sDom": 'tir'
    });

    hdTables.hdpendingackalertTable = $(
            '#hdPendingAckAlertTable').dataTable({
        "iDisplayLength": -1,
        "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": [4]
            }],
        "sAjaxSource": 'hdPendingAckAlert.html?searchVal=' + document.getElementById("siteSearchId").value,
        "aoColumns": [{
                "mDataProp": "openedDate",
                "sClass": "textCenter"
            }, {
                "mDataProp": "siteNumberDisplay",
                "sClass": "siteNumberTextPendingCubes"
            }, {
                "mDataProp": "state"
            }, {
                "mDataProp": "customer"
            }, {
                "mDataProp": "lastTd"
            }],
        "fnInitComplete": function (oSettings, json) {

        },
        "sDom": 'tir'

    });

    hdTables.hdpendingackmonitorTable = $(
            '#hdPendingAckMonitorTable')
            .dataTable(
                    {
                        "iDisplayLength": -1,
                        "aoColumnDefs": [{
                                "bSortable": false,
                                "aTargets": [4]
                            }],
                        "sAjaxSource": 'hdPendingAckMonitor.html?searchVal=' + document.getElementById("siteSearchId").value,
                        "aoColumns": [
                            {
                                "mDataProp": "openedDate",
                                "sClass": "textCenter"
                            },
                            {
                                "mDataProp": "siteNumberDisplay",
                                "sClass": "siteNumberTextPendingCubes"
                            },
                            {
                                "mDataProp": "state"
                            },
                            {
                                "mDataProp": "customer"
                            }, {
                                "mDataProp": "lastTd"
                            }],
                        "fnInitComplete": function (oSettings,
                                json) {

                            setPendingAckMonitoringFooterDetail();

                        },
                        "sDom": 'tir'

                    });

    $(".wipTicketsInActive")
            .live(
                    'click',
                    function () {

                        if ($(this).attr("ticketId") == undefined) {
                            alert("Please Drag a Ticket From Help Desk WIP ");
                            return false;
                        }

                        if (!setPreviousTicket()) {
                            return false;
                        }
                        resetActiveForm();

                        if ($(this).hasClass(
                                'wipTicketsActiveChanged')) {
                            $("#updateTicketButton")
                                    .removeClass('FMBUT0003')
                                    .addClass('FMBUT0005');
                            var efforts = $(this).attr(
                                    "recordedEfforts");
                            var nextActionUnit = $(this).attr(
                                    "nextActionUnit");
                            var workNotes = $(this).attr(
                                    "workNotes");

                            $("#ticketWorkNotes")
                                    .val(workNotes);

                            if (efforts != "undefined"
                                    || efforts != undefined) {
                                $("#effortMinutes")
                                        .val(efforts);

                            }
                            if (nextActionUnit == "date") {
                                $("#wipNextActionDueDate")
                                        .val(
                                                $(this)
                                                .attr(
                                                        "nextActionTime"));

                            } else if (nextActionUnit == "Hrs") {
                                $(
                                        "input[type='radio'][name='nextActionTime']")
                                        .filter('[value=Hrs]')
                                        .attr('checked', true);
                                $("#nextActionTime")
                                        .val(
                                                $(this)
                                                .attr(
                                                        "nextActionTime"));

                            } else {
                                $("#nextActionTime")
                                        .val(
                                                $(this)
                                                .attr(
                                                        "nextActionTime"));
                                $(
                                        "input[type='radio'][name='nextActionTime']")
                                        .filter('[value=Mins]')
                                        .attr('checked', true);

                            }

                        }

                        $(".wipTicketsActive")
                                .each(
                                        function () {
                                            $(this)
                                                    .toggleClass(
                                                            'wipTicketsActive');
                                            $(this)
                                                    .toggleClass(
                                                            'wipTicketsInActive');
                                        });
                        $(this).toggleClass(
                                'wipTicketsInActive');
                        $(this).toggleClass('wipTicketsActive');
                        $("#activeTicketHeader").removeClass(
                                "headerPriority1");
                        $("#activeTicketHeader").removeClass(
                                "headerPriority3");
                        if (($(this).attr("siteStatus") == 'down') || ($(this).attr("circuitStatus") == '0') || ($(this).attr("overdueStatus") == 'overdue')) {
                            $("#activeTicketHeader").addClass(
                                    "headerPriority1");
                        }
                        else {
                            $("#activeTicketHeader").addClass(
                                    "headerPriority3");
                        }

                        var ticketId = $(this).attr("ticketId");

                        $(
                                '#hdWipTable tbody tr, #hdWipOverDueTable tbody tr, #hdWipNextActionTable tbody tr')
                                .each(
                                        function () {
                                            if ($(this).attr(
                                                    "ticketid") == ticketId) {

                                                var shortDescription = $(
                                                        this)
                                                        .attr(
                                                                'shortdescription');
                                                var nextAction = $(
                                                        this)
                                                        .attr(
                                                                'nextaction');
                                                var nextActionDueDate = $(
                                                        this)
                                                        .attr(
                                                                'nextactionduedate');
                                                var siteContact = $(
                                                        this)
                                                        .attr(
                                                                'contactname');
                                                var contactNumber = $(
                                                        this)
                                                        .attr(
                                                                'contactphonenumber');
                                                var currentTicketNextActionId = $(
                                                        this)
                                                        .attr(
                                                                'nextActionId');
                                                var customer = $(
                                                        this)
                                                        .attr(
                                                                'customer');
                                                var siteNumber = $(
                                                        this)
                                                        .attr(
                                                                'sitenumber');


                                                var ticketState = $(
                                                        this)
                                                        .attr(
                                                                'ticketState');
                                                var backUpStatus = $(
                                                        this)
                                                        .attr(
                                                                'circuitStatus');
                                                var priority = $(
                                                        this)
                                                        .attr(
                                                                'priority');
                                                var headerText = priority
                                                        + " - "
                                                        + siteNumber
                                                        + " - "
                                                        + customer
                                                        + '<span style="display:none;" id="headerIlexTicketId">'
                                                        + ticketId
                                                        + '</span>';

                                                $(
                                                        "#ticketDescription")
                                                        .text(
                                                                shortDescription);
                                                $(
                                                        "#siteContact")
                                                        .html(
                                                                "&nbsp;");
                                                $(
                                                        "#contactNumber")
                                                        .html(
                                                                "&nbsp;");
                                                $(
                                                        "#siteContact")
                                                        .text(
                                                                siteContact);
                                                $(
                                                        "#contactNumber")
                                                        .text(
                                                                contactNumber);
                                                $(
                                                        "#ticketNextAction")
                                                        .text(
                                                                nextAction);
                                                $("#ticketNextActionWIP").val(currentTicketNextActionId);
                                                $(
                                                        "#currentTicketNextActionId")
                                                        .val(
                                                                currentTicketNextActionId);
                                                $(
                                                        "#ticketNextActionDueDate")
                                                        .text(
                                                                nextActionDueDate);
                                                $(
                                                        "#ticketState")
                                                        .val(
                                                                ticketState);

                                                if ($(this).attr("ticketSource") == '2') {

                                                    $("#backUpStatusDiv").show();

                                                    if (backUpStatus == "1") {
                                                        $("input[name=backUpStatus]")[0].checked = true;
                                                    } else if (backUpStatus == "0") {
                                                        $("input[name=backUpStatus]")[1].checked = true;
                                                    } else if (backUpStatus == "99") {
                                                        $("input[name=backUpStatus]")[2].checked = true;
                                                    }

                                                    if ($(this).attr("device")) {
                                                        $("#deviceDiv").text($(this).attr("device"));
                                                    } else {
                                                        $("#deviceDiv").text("Unknown");
                                                    }

                                                } else {
                                                    $("#backUpStatusDiv").hide();
                                                }

                                                $(
                                                        "#activeTicketTitle")
                                                        .html(
                                                                headerText);
                                                $(
                                                        "#activeTicketDiv")
                                                        .show();

                                                $
                                                        .ajax({
                                                            type: 'POST',
                                                            data: {
                                                                "ticketId": ticketId
                                                            },
                                                            url: pageContextPath
                                                                    + '/hdActiveTicket/getTicketDetails.html',
                                                            success: function (
                                                                    obj) {
                                                                var data = $
                                                                        .parseJSON(obj);

                                                                $(
                                                                        "#ticketStatus")
                                                                        .html(
                                                                                data.ticketStatus);
                                                                $(
                                                                        "#sumOfEfforts")
                                                                        .val(
                                                                                data.sumOfEfforts);
                                                            }
                                                        });
                                                saveActiveTicket(ticketId);
                                                //show latest work notes
                                                showLatestWorkNotesForActiveTicket(ticketId);
                                            }
                                        });
                    });

    $("#dialog").dialog({
        autoOpen: false,
        modal: true
    });

    $("#nextActionTime")
            .change(
                    function () {
                        if ($(
                                "input[type='radio'][name='nextActionTime']")
                                .is(':checked')) {

                        } else {
                            $(
                                    "input[type='radio'][name='nextActionTime']")
                                    .prop("checked", true);
                        }
                        ticketchanged.nextActionChanged = true;
                        $(".wipTicketsActive").addClass(
                                "wipTicketsActiveChanged");

                    });

    $("#createTicketNextActionTime")
            .live('change',
                    function () {
                        if ($(
                                "input[type='radio'][name='createTicketNextActionUnit']")
                                .is(':checked')) {

                        } else {
                            $(
                                    "input[type='radio'][name='createTicketNextActionUnit']")[1].checked = true;
                        }

                    });

    $("#nextActionTimeTicketSummary")
            .live('change',
                    function () {
                        if ($(
                                "input[type='radio'][name='nextActionTimeTicketSummary']")
                                .is(':checked')) {

                        } else {
                            $(
                                    "input[type='radio'][name='nextActionTimeTicketSummary']")[1].checked = true;
                        }

                    });

    $("#effortMinutes").keypress(
            function () {
                $("#updateTicketButton").removeClass(
                        'FMBUT0003').addClass('FMBUT0005');
                ticketchanged.recordEffortChanged = true;
                $(".wipTicketsActive").addClass(
                        "wipTicketsActiveChanged");

            });
    $("#effortMinutes").live('paste', function () {
        $("#updateTicketButton").removeClass(
                'FMBUT0003').addClass('FMBUT0005');
        ticketchanged.recordEffortChanged = true;
        $(".wipTicketsActive").addClass(
                "wipTicketsActiveChanged");

    });

    $("#wipNextActionDueDate").change(
            function () {
                ticketchanged.nextActionChanged = true;
                $(".wipTicketsActive").addClass(
                        "wipTicketsActiveChanged");
                $("#updateTicketButton").removeClass(
                        'FMBUT0003').addClass('FMBUT0005');

            });

    $("#ticketWorkNotes").keypress(
            function () {
                ticketchanged.workNotesChanged = true;
                $(".wipTicketsActive").addClass(
                        "wipTicketsActiveChanged");
                $("#updateTicketButton").removeClass(
                        'FMBUT0003').addClass('FMBUT0005');

            });
    $("#ticketWorkNotes").live('paste', function () {
        ticketchanged.workNotesChanged = true;
        $(".wipTicketsActive").addClass(
                "wipTicketsActiveChanged");
        $("#updateTicketButton").removeClass(
                'FMBUT0003').addClass('FMBUT0005');

    });

    /* function :To Minimize or Maximize cube -start */
//	$("#minMax")
//			.live(
//					'click',
//					function() {
//						alert("minmax2 clicked");
//						$minMaxImage = $(this);
//						var tableId = $minMaxImage.parents(
//								'div').find('table').attr('id');
//						var cubeId = tableId + "_wrapper";
//						var containerTitleWidth = $minMaxImage
//								.parents('div.containerTitle')
//								.width();
//						$('#' + cubeId).toggle();
//						if ($minMaxImage.attr("src") == pageContextPath
//								+ '/resources/images/sm-scr.png') {
//							$minMaxImage
//									.parents(
//											'div.containerTitle')
//									.width(
//											containerTitleWidth / 2);
//
//							$minMaxImage.parents(
//									'div.containerTitle').find(
//									"div.dataTables_paginate")
//									.hide();
//
//							$minMaxImage
//									.attr(
//											"src",
//											pageContextPath
//													+ '/resources/images/full-scr.png');
//							if (tableId == 'hdPendingAckCustTable') {
//								$minMaxImage
//										.parents(
//												'div.containerTitle')
//										.width(
//												containerTitleWidth / 2 + 120);
//								if ($('#hdPendingAckAlertDiv')
//										.find(
//												'div.containerTitle')
//										.width() > $(
//										'#hdPendingAckCustDiv')
//										.find(
//												'div.containerTitle')
//										.width()) {
//									$("#leftDiv")
//											.width(
//													$(
//															"#hdPendingAckAlertDiv")
//															.width() + 15);
//								} else {
//									if ($(
//											'#hdPendingAckMonitorDiv')
//											.find(
//													'div.containerTitle')
//											.width() > $(
//											'#hdPendingAckCustDiv')
//											.find(
//													'div.containerTitle')
//											.width()) {
//										$("#leftDiv")
//												.width(
//														$(
//																"#hdPendingAckMonitorDiv")
//																.width() + 15);
//									} else {
//										$("#leftDiv")
//												.width(
//														$(
//																"#hdPendingAckCustDiv")
//																.width() + 15);
//									}
//								}
//							} else {
//								if (tableId == 'hdPendingAckAlertTable') {
//									$minMaxImage
//											.parents(
//													'div.containerTitle')
//											.width(
//													containerTitleWidth / 2 + 120);
//									if ($(
//											'#hdPendingAckCustDiv')
//											.find(
//													'div.containerTitle')
//											.width() > $(
//											'#hdPendingAckAlertDiv')
//											.find(
//													'div.containerTitle')
//											.width()) {
//										$("#leftDiv")
//												.width(
//														$(
//																"#hdPendingAckCustDiv")
//																.width() + 15);
//									} else {
//										if ($(
//												'#hdPendingAckMonitorDiv')
//												.find(
//														'div.containerTitle')
//												.width() > $(
//												'#hdPendingAckAlertDiv')
//												.find(
//														'div.containerTitle')
//												.width()) {
//											$("#leftDiv")
//													.width(
//															$(
//																	"#hdPendingAckMonitorDiv")
//																	.width() + 15);
//										} else {
//											$("#leftDiv")
//													.width(
//															$(
//																	"#hdPendingAckAlertDiv")
//																	.width() + 15);
//										}
//									}
//								} else {
//									if (tableId == 'hdPendingAckMonitorTable') {
//										$minMaxImage
//												.parents(
//														'div.containerTitle')
//												.width(
//														containerTitleWidth / 2 + 120);
//										if ($(
//												'#hdPendingAckCustDiv')
//												.find(
//														'div.containerTitle')
//												.width() > $(
//												'#hdPendingAckMonitorDiv')
//												.find(
//														'div.containerTitle')
//												.width()) {
//											$("#leftDiv")
//													.width(
//															$(
//																	"#hdPendingAckCustDiv")
//																	.width() + 15);
//										} else {
//											if ($(
//													'#hdPendingAckAlertDiv')
//													.find(
//															'div.containerTitle')
//													.width() > $(
//													'#hdPendingAckMonitorDiv')
//													.find(
//															'div.containerTitle')
//													.width()) {
//												$("#leftDiv")
//														.width(
//																$(
//																		"#hdPendingAckAlertDiv")
//																		.width() + 15);
//											} else {
//												$("#leftDiv")
//														.width(
//																$(
//																		"#hdPendingAckMonitorDiv")
//																		.width() + 15);
//											}
//										}
//									} else {
//										if ($(
//												'#hdWipDivOverDue')
//												.find(
//														'div.containerTitle')
//												.width() > $(
//												'#hdWipDiv')
//												.find(
//														'div.containerTitle')
//												.width()) {
//											$('#middleDiv')
//													.width(
//															(($(
//																	'#hdWipDivOverDue')
//																	.find(
//																			'div.containerTitle')
//																	.width()) + 15)
//																	+ "px");
//											;
//										} else {
//											$('#middleDiv')
//													.width(
//															(($(
//																	'#hdWipDiv')
//																	.find(
//																			'div.containerTitle')
//																	.width()) + 15)
//																	+ "px");
//
//										}
//									}
//								}
//							}
//						} else {
//							$minMaxImage
//									.parents(
//											'div.containerTitle')
//									.width(
//											$minMaxImage
//													.parents(
//															'div.containerTitle')
//													.attr(
//															'initialWidth'));
//
//							$minMaxImage.parents(
//									'div.containerTitle').find(
//									"div.dataTables_paginate")
//									.show();
//
//							$minMaxImage
//									.attr(
//											"src",
//											pageContextPath
//													+ '/resources/images/sm-scr.png');
//							if (tableId == 'hdPendingAckCustTable') {
//								$("#leftDiv")
//										.width(
//												(($(
//														"#hdPendingAckCustDiv")
//														.find(
//																'div.containerTitle')
//														.width()) + 15)
//														+ "px");
//								;
//
//							} else {
//								if (tableId == 'hdPendingAckAlertTable') {
//									$("#leftDiv")
//											.width(
//													(($(
//															"#hdPendingAckAlertDiv")
//															.find(
//																	'div.containerTitle')
//															.width()) + 15)
//															+ "px");
//									;
//								} else {
//									if (tableId == 'hdPendingAckMonitorTable') {
//										$("#leftDiv")
//												.width(
//														(($(
//																"#hdPendingAckMonitorDiv")
//																.find(
//																		'div.containerTitle')
//																.width()) + 15)
//																+ "px");
//										;
//									} else {
//										if ($(
//												'#hdWipDivOverDue')
//												.find(
//														'div.containerTitle')
//												.width() > $(
//												'#hdWipDiv')
//												.find(
//														'div.containerTitle')
//												.width()) {
//
//											$('#middleDiv')
//													.width(
//															(($(
//																	'#hdWipDivOverDue')
//																	.find(
//																			'div.containerTitle')
//																	.width()) + 15)
//																	+ "px");
//											;
//										} else {
//											$('#middleDiv')
//													.width(
//															(($(
//																	'#hdWipDiv')
//																	.find(
//																			'div.containerTitle')
//																	.width()) + 15)
//																	+ "px");
//
//										}
//									}
//								}
//							}
//						}
//
//						$(".contentWrapper").width(
//								$("#leftDiv").width()
//										+ $("#middleDiv")
//												.width()
//										+ $("#rightDiv")
//												.width() + 40);
//						   if($(window).width()>$(".contentWrapper").width())
//						   {
//						   $("#hdBreadCrumbTable").width($(window).width());
//						   }
//					   else{
//					   		$("#hdBreadCrumbTable").width($(".contentWrapper").width()+20);
//					   		}
//
//					});
    /* function :To Minimize or Maximize cube -end */
//alert("ajaxpoll on search site by no");
//	processUsingAjaxPoll();

    $("#ticketState").change(function () {
        ticketchanged.ticketStateChanged = true;
    });

    $("input[name=backUpStatus]").change(function () {
        ticketchanged.circuitStatusChanged = true;
    });




    appendSiteDetail();
    disableResolvedDraggable();

}


/* function :To get current date in MM/dd/YYYY format-start */
function getCurrentdate() {
    var fullDate = new Date();
    var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate
            .getMonth() + 1) : '0' + (fullDate.getMonth() + 1);

    var currentDate = twoDigitMonth + "/" + fullDate.getDate() + "/"
            + fullDate.getFullYear();
    return currentDate;
}
/* function :To get current date in MM/dd/YYYY format-end */

/* function :when ticket Drop to WIP AREA -starts */
$.fn.ticketDrop = function (event, ui) {

    var priority = ui.helper.attr("priority");
    var ilexNumber = ui.helper.attr("ilexNumber");
    var siteNumber = ui.helper.attr("siteNumber");
    var customer = ui.helper.attr("customer");
    var customerNumber = ui.helper.attr("customerNumber");
    var ticketId = ui.helper.attr("ticketId");
    var siteStatus = ui.helper.attr("siteStatus");
    var circuitStatus = ui.helper.attr("circuitStatus");
    var overdueStatus = ui.helper.attr("overdueStatus");

    $(this).attr("priority", priority);
    $(this).attr("ilexNumber", ilexNumber);
    $(this).attr("siteNumber", siteNumber);
    $(this).attr("customer", customer);
    $(this).attr("customerNumber", customerNumber);
    $(this).attr("ticketId", ticketId);
    $(this).attr("siteStatus", siteStatus);
    $(this).attr("circuitStatus", circuitStatus);
    $(this).attr("overdueStatus", overdueStatus);

    if ((ui.helper.attr("siteStatus") == 'down') || (ui.helper.attr("circuitStatus") == '0') || (ui.helper.attr("overdueStatus") == 'overdue')) {
        $(this).addClass("wipTicketsActivePriority1");
    }
    else {
        $(this).addClass("wipTicketsActivePriority3");
    }

    $(this).text(priority + " - " + siteNumber + " - " + customer);
    if ($("#activeTicketDiv").is(':hidden')) {
        $(this).click();
    }
    $(ui.draggable).parent().parent().children().find("span.greenIcon")
            .addClass('redIcon').removeClass('greenIcon');
    ui.draggable.draggable("disable", 1);
    $(this).removeClass("emptySlot");

    saveDroppedTicketState(ticketId, $(this).attr('id'));

};
/* function :when ticket Drop to WIP AREA -Ends */
$.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", ((($(window).height() - this.height()) / 2 + $(window)
            .scrollTop()) - 30)
            + "px");
    this.css("left", ($(window).width() - this.width()) / 2
            + $(window).scrollLeft() + "px");
    return this;
};

function closeWindow() {
    //alert("closing window");
    $.unblockUI();


    $("#pageTitle").innerHTML = '';
    $("#popupBody").empty().html(
            '<img src="' + pageContextPath
            + '/resources/images/waiting_tree.gif" />');

}

//function closeWindow4() {
//$( "#test" ).popup( "close" );

//$.unblockUI();
//$("#testBody").innerHTML = '';
//	$("#testBody").empty().html(
//			'<img src="' + pageContextPath
//					+ '/resources/images/waiting_tree.gif" />');

//}

function closeWindow2() {

    var ticketId = $("#ticketId").val();
    if (ticketId != undefined) {
        $.ajax({
            type: "post",
            data: {
                "ticketId": ticketId
            },
            url: pageContextPath + '/hdMainTable/clearReviewStatus.html',
            success: function (data) {

            }
        });
    }


    if (ticketPopUp)
    {
        $('#ticketPopUpScreen').unblock();
        $('#popUpScreen').unblock();
        $("#popupBody2").empty().html(
                '<img src=""' + pageContextPath
                + '/resources/images/waiting_tree.gif" />');
        $("#pageTitle2").innerHTML = '';
    }
    else {
        $.unblockUI();
        $("#popupBody2").empty().html(
                '<img src=""' + pageContextPath
                + '/resources/images/waiting_tree.gif" />');
        $("#pageTitle2").innerHTML = '';
    }


}



function closeWindow3() {
    $('#popUpScreen2').unblock();
    $("#popupBody3").empty().html(
            '<img src=""' + pageContextPath
            + '/resources/images/waiting_tree.gif" />');
    $("#pageTitle3").innerHTML = '';

}

/* function :calculate Efforts on change of start and stop time starts */
function calculateRecordEffort() {
    var startDate = new Date($("#wipStartDate").val());
    var stopDate = new Date($("#wipStopDate").val());
    var dif = stopDate.getTime() - startDate.getTime();
    var differenceInMinutes = dif / (1000 * 60);
    if (!isNaN(differenceInMinutes)) {
        if (parseFloat(differenceInMinutes) >= 0) {
            $("#effortMinutes").val(differenceInMinutes);
            $("#updateTicketButton").removeClass('FMBUT0003').addClass(
                    'FMBUT0005');
            ticketchanged.recordEffortChanged = true;
        }
    }

}
/* function : calculate Efforts on change of start and stop time Ends */

/*
 * function : validate work Notes 1. check for the length of the notes should be
 * greater than 0 and less than 2000 characters. 2. Check for spells in the
 * notes using google API. and Save Work Notes
 *
 *
 *
 *
 */

function validateWorkNotes() {
    var ajaxReturn = true;
    var ticketId = $("#headerIlexTicketId").text();
    var workNotes = $.trim($("#ticketWorkNotes").val());
    var workNotesSpellMessage = 'Your note contains one or more misspelled words.<br/> Since this note will be sent to the client,<br/> it is important to keep this communication as professional as possible.<br/> Please correct the spelling.';
    var correctionLink = '<br/><a style="text-decoration: underline;color: #0000ff; " href="javascript:runImpspellerTextarea(\'#ticketWorkNotes\');" id="impcheck" >Click here</a> ';
    workNotesSpellMessage += correctionLink;
    workNotesSpellMessage += ' for assistance correcting misspelled word(s). Ensure all abbreviations are in upper case.';
    if (workNotes.length == 0) {
        openErrorDialog("200", "Please Enter Work Notes");
        return false;
    }

    if (workNotes.length > 2000) {
        openErrorDialog("500", "You have entered a note that is too long.<br/>  Notes should be limited to less than 2000 characters. <br/> Please correct your input and consult your supervisor on the proper use of this item.");
        return false;
    }
    if (!$("#overrideSpellCheck").is(":checked")) {
        $.ajax({
            type: "POST",
            url: pageContextPath + '/hdMainTable/checkWords.html',
            async: false,
            data: 'text=' + workNotes,
            dataType: "json",
            error: function (XHR, status, error) {
                $("#updateMessage").html("Update Failed");
                $("#updateMessage").addClass("MSERR0002").removeClass(
                        "MSSUC0002");
                $("#updateMessage").show();
                ajaxReturn = false;
            },
            success: function (json) {
                if (!json.length) {
                    ajaxReturn = saveWorkNotes(ticketId, workNotes);

                } else {
                    openErrorDialog("600", workNotesSpellMessage);
                    ajaxReturn = false;
                }
            }
        });
    } else {
        ajaxReturn = saveWorkNotes(ticketId, workNotes);
    }

    return ajaxReturn;
}

function saveWorkNotes(ticketId, workNotes) {
    var ajaxReturn = true;
    $.ajax({
        type: 'POST',
        data: {
            "ticketId": ticketId,
            "workNotes": workNotes
        },
        async: false,
        url: pageContextPath + '/hdMainTable/saveWorkNotes.html',
        success: function (obj) {
            if (obj == "success") {
                $("#ticketWorkNotes").val("");
                ajaxReturn = true;
            } else {
                $("#updateMessage").html("Update Failed");
                $("#updateMessage").addClass("MSERR0002").removeClass(
                        "MSSUC0002");
                $("#updateMessage").show();
                ajaxReturn = false;
            }
            ticketchanged.workNotesChanged = false;

        }
    });
    return ajaxReturn;
}

/*
 * function : Save work Notes Ends
 */

/*
 * function : update Next Action Starts
 *
 * 1. determines which method of entry to use. 2. If the user is entering a
 * discrete minute value, the user enters the effort in the text field.it should
 * be greater than 0 and less than 120. 3. If the user is entering a discrete
 * hours , the user enters the effort in the text field.it should be greater
 * than 0 and less than 24. 4.If date, then is limited to 7 days in the future
 *
 * 5. Work Notes should be enter first.
 *
 *
 */
function updateNextAction() {
    var ajaxReturn = true;
    var nextActionUnit;
    var nextActionTimes;
    var ticketId = $("#headerIlexTicketId").text();
    var nextAction = $("#ticketNextActionWIP").val();
    if ($("#ticketNextActionWIP").val() == "0") {
        nextAction = $("#currentTicketNextActionId").val();
    }

    var invalidTimeFrameMessage = 'An invalid time frame or date was entered. <br/>Time frame in hours should less than 24.<br/> Time frame in minutes should be less 60.<br/>  The date is limited to 30 days in the future. <br/> Please correct your input.';
    if ($("input[type='radio'][name='nextActionTime']").is(':checked')) {
        var nextActionTimeRadio = $(
                "input[type='radio'][name='nextActionTime']:checked").val();

        var nextActionTime = $("#nextActionTime").val();

        if (nextActionTimeRadio == "Hrs") {
            if (parseFloat(nextActionTime) < 0
                    || parseFloat(nextActionTime) > 24) {
                openErrorDialog("500", invalidTimeFrameMessage);
                return false;
            }
            nextActionUnit = "Hrs";

        } else {
            if (parseFloat(nextActionTime) < 0
                    || parseFloat(nextActionTime) > 60) {
                openErrorDialog("500", invalidTimeFrameMessage);
                return false;
            }
            nextActionUnit = "Mins";

        }

        if (nextActionTime == "") {
            openErrorDialog("500", invalidTimeFrameMessage);
            return false;
        }
        nextActionTimes = nextActionTime;

    } else {
        var wipNextActionDueDate = $("#wipNextActionDueDate").val();
        if (wipNextActionDueDate == "") {
            openErrorDialog("500", invalidTimeFrameMessage);
            return false;

        } else {
            var dueDate = new Date(wipNextActionDueDate);
            var currentDate = new Date();
            var dif = dueDate.getTime() - currentDate.getTime() + 80000;

            if (parseFloat(dif) < 0) {
                openErrorDialog("500", invalidTimeFrameMessage);
                return false;
            }
            var differenceInDays = parseInt((dueDate - currentDate)
                    / (1000 * 60 * 60 * 24));
            if (!isNaN(differenceInDays)) {
                if (parseFloat(differenceInDays) >= 30) {
                    openErrorDialog("500", invalidTimeFrameMessage);
                    return false;
                }
            }
        }

        nextActionUnit = "Date";
        nextActionTimes = wipNextActionDueDate;

    }

    if ($("#ticketWorkNotes").val() != "") {
        if (!validateWorkNotesForSpell()) {
            return false;
        }

        $.ajax({
            type: 'POST',
            data: {
                "ticketId": ticketId,
                "nextActionUnit": nextActionUnit,
                "nextActionTime": nextActionTimes,
                "nextAction": nextAction
            },
            async: false,
            url: pageContextPath + '/hdActiveTicket/updateNextAction.html',
            success: function (obj) {
                $("input[type='radio'][name='nextActionTime']:checked").each(
                        function () {
                            $(this).prop('checked', false);
                        });
                $("#nextActionTime").val("");
                $("#wipNextActionDueDate").val("");
                try {
                    var data = $.parseJSON(obj);
                    $("#ticketNextActionDueDate").text(data.nextActionDueDate);
                    $("#ticketStatus").html(data.ticketStatus);
                    ticketchanged.nextActionChanged = false;
                    ajaxReturn = true;
                } catch (e) {
                    $("#updateMessage").html("&nbsp;Update Failed");
                    $("#updateMessage").addClass("MSERR0002").removeClass(
                            "MSSUC0002");
                    ajaxReturn = false;
                }
            }
        });

    } else {
        var invalidWorkNotes = 'You are updating the Next Action field.<br/>   You must explain why this action is necessary with a note.<br/>   Please enter a Work note and click Update Next Action.';
        openErrorDialog("500", invalidWorkNotes);
        return false;
    }

    return ajaxReturn;

}

/*
 * function : update Next Action Ends
 */

function recordEfforts() {
    var ajaxReturn = true;
    var invalidEffortMessage = 'An invalid effort value was entered.<br/> The effort value should be less than 1440 minutes.<br/> In general, you should be entering these values incrementally to avoid this error.<br/>  Please correct your input or consult your supervisor on how to resolve.';
    var effortValue = $("#effortMinutes").val();
    if (effortValue == "") {
        openErrorDialog("500", invalidEffortMessage);
        return false;
    }
    if (isNaN(effortValue)) {
        openErrorDialog("500", invalidEffortMessage);
        return false;

    }
    if ((parseInt(effortValue) <= 0) || (parseInt(effortValue) > 1440)) {
        openErrorDialog("500", invalidEffortMessage);
        return false;

    }

    if ($("#ticketWorkNotes").val() == "") {
        var invalidWorkNotes = 'You are updating the effort field.<br/>  You must explain why this action is necessary with a note.<br/>  Please enter a Work note and click Record Effort.';
        openErrorDialog("500", invalidWorkNotes);
        return false;

    } else {
        if (!validateWorkNotesForSpell()) {
            return false;
        }
        var ticketId = $("#headerIlexTicketId").text();
        $.ajax({
            type: "POST",
            url: pageContextPath + '/hdActiveTicket/recordEfforts.html',
            async: false,
            data: {
                "ticketId": ticketId,
                "effortValue": effortValue
            },
            success: function (data) {
                if (data == "success") {
                    $("#effortMinutes").val("");
                    $("#wipStartDate").val("");
                    $("#wipStopDate").val("");
                    ticketchanged.recordEffortChanged = false;
                    ajaxReturn = true;
                }
            }

        });
    }
    return ajaxReturn;
}

function checkNextActionStatus() {
    if ($("#ticketStatus").text() == "Overdue") {
        openErrorDialog("300", "Please update the Next Action");
        return false;
    }
    return true;

}
function checkChangedForm() {

    if (ticketchanged.ticketStateChanged) {
        if (!updateTicketState()) {
            return false;
        }
    }
    if (ticketchanged.circuitStatusChanged) {
        if (!updateCircuitStatus()) {
            return false;
        }
    }

    if (ticketchanged.nextActionChanged) {
        if (!updateNextAction()) {
            return false;
        }
    }
    if (ticketchanged.recordEffortChanged && $("#effortMinutes").val() != '') {
        if (!recordEfforts()) {
            return false;
        }
    }
    if (ticketchanged.workNotesChanged) {
        if (!validateWorkNotes()) {
            return false;
        }
    }

    return true;
}

function updateTicket() {
    var updateSuccess = false;
    var ticketId = $("#headerIlexTicketId").text();
    if (ticketchanged.ticketStateChanged) {
        if (!updateTicketState()) {
            return false;
        }
        else {
            updateSuccess = true;
        }
    }
    if (ticketchanged.circuitStatusChanged) {
        if (!updateCircuitStatus()) {
            return false;
        }
        else {
            updateSuccess = true;
        }
    }
    if (ticketchanged.nextActionChanged) {
        if (!updateNextAction()) {
            return false;
        }
        else {
            updateSuccess = true;
        }
    }
    if (ticketchanged.recordEffortChanged && $("#effortMinutes").val() != '') {
        if (!recordEfforts()) {
            return false;
        }
        else {
            updateSuccess = true;
        }
    }
    if (ticketchanged.workNotesChanged) {
        if (!validateWorkNotes()) {
            return false;
        }
        else {
            updateSuccess = true;
        }
    }
    if (updateSuccess)
    {
        $("#updateMessage").html("&nbsp;Update Completed");
        $("#updateMessage").show();
        $("#updateMessage").addClass("MSSUC0002").removeClass("MSERR0002");
        $(".wipTicketsActive").removeClass('wipTicketsActiveChanged');
        $("#updateTicketButton").removeClass('FMBUT0005').addClass('FMBUT0003');
        setInterval(function () {
            $("#updateMessage").html("&nbsp;");
            $("#updateMessage").hide();
        }, 10000);
        //update work notes for active ticket
        showLatestWorkNotesForActiveTicket(ticketId);
        //Refresh WIP and WIP-Overdue Cubes
        hdTables.hdWipTable.fnReloadAjax();
        hdTables.hdWipoverdueTable.fnReloadAjax();
        hdTables.hdResolvedTable.fnReloadAjax();
        hdTables.hdWipNextActionTable.fnReloadAjax();
    }

}

// Method to update ticket state (work notes mandatory)
function updateTicketState() {
    var ajaxReturn = true;
    if ($("#ticketWorkNotes").val() != "") {
        if (!validateWorkNotesForSpell()) {
            return false;
        }
        var state = $("#ticketState").val();
        var ticketId = $("#headerIlexTicketId").text();
        $.ajax({
            type: "POST",
            url: pageContextPath + '/hdActiveTicket/updateState.html',
            async: false,
            data: {
                "ticketId": ticketId,
                "state": state
            },
            success: function (data) {
                if (data == "success") {
                    ticketchanged.ticketStateChanged = false;
                    ajaxReturn = true;
                }
            }

        });
    } else {
        var invalidWorkNotes = 'You are updating the Ticket state field.<br/>   You must explain why this action is necessary with a note.<br/>   Please enter a Work note and click Update Ticket.';
        openErrorDialog("500", invalidWorkNotes);
        return false;
    }
    return ajaxReturn;
}

function updateCircuitStatus() {
    var ajaxReturn = true;
    var cktStatus = $("input[name=backUpStatus]:checked").val();
    var ticketId = $("#headerIlexTicketId").text();
    $.ajax({
        type: "POST",
        url: pageContextPath + '/hdActiveTicket/updateCircuitStatus.html',
        async: false,
        data: {
            "ticketId": ticketId,
            "cktStatus": cktStatus
        },
        success: function (data) {
            if (data == "success") {
                ticketchanged.circuitStatusChanged = false;
                ajaxReturn = true;
            }
        }

    });
    return ajaxReturn;
}

function closeActiveTicket() {
    var ticketId = $("#headerIlexTicketId").text();

    if (!$("#overrideNextAction").is(":checked") && !checkNextActionStatus()) {
        return false;
    }

    if (!checkChangedForm()) {
        return false;
    }

    $.ajax({
        type: 'POST',
        data: {
            "ticketId": ticketId
        },
        async: false,
        url: pageContextPath + '/hdActiveTicket/closeActiveTicket.html',
        success: function (obj) {
            if (obj == "success") {

            } else {
                alert(obj);
                return false;
            }
        }
    });

    $("#updateTicketButton").removeClass('FMBUT0005').addClass('FMBUT0003');

    var ticketId = $(".wipTicketsActive").attr("ticketId");

    $("div.tableWrapper table tr:nth-child(n+1)").filter(function () {
        return $(this).attr("ticketId") == ticketId;
    }).each(
            function () {
                // $(this).find("span.redIcon").draggable("enable", 1);
                $(this).find("span.redIcon").addClass('greenIcon').removeClass(
                        'redIcon');
                $(this).find("span.greenIcon").draggable("enable", 1);
            });

    $("#activeTicketDiv").hide();
    $(".wipTicketsActive").text("Drop Ticket Here");
    $(".wipTicketsActive").addClass("emptySlot");
    $(".wipTicketsActive").removeAttr(
            "ilexNumber ticketId  customer customerNumber sitenumber");

    $(".wipTicketsActive")
            .addClass("wipTicketsInActive")
            .removeClass(
                    "wipTicketsActivePriority1  wipTicketsActivePriority3 wipTicketsActive  wipTicketsActivePriority1 wipTicketsActive wipTicketsActiveChanged");
}

function runImpspellerTextarea(id) {
    var mp = $(id).val();
    $("#dialog").dialog("close");
    $.impspeller(mp, {
        success: function (text, html, el) {
            $(id).val(text);
        }
    });
}

function resolveTicket() {
    if (!$("#overrideNextAction").is(":checked") && !checkNextActionStatus()) {
        return false;
    }
    if (!checkChangedForm()) {
        return false;
    }

    var ilexNumber = $(".wipTicketsActive").attr("ilexNumber");

    $("div.tableWrapper table tr:nth-child(n+1)").filter(function () {
        return $(this).attr("ilexNumber") == ilexNumber;

    }).each(
            function () {
                $(this).find("span.redIcon").draggable("enable", 1);
                $(this).find("span.redIcon").addClass('greenIcon').removeClass(
                        'redIcon');

            });

    $("#activeTicketDiv").hide();
    $(".wipTicketsActive").text($(".wipTicketsActive").attr("id"));
    $(".wipTicketsActive").removeAttr(
            "ilexNumber  customer customerNumber sitenumber");
    $(".wipTicketsActive")
            .addClass("wipTicketsInActive")
            .removeClass(
                    "wipTicketsActivePriority1  wipTicketsActivePriority3 wipTicketsActive");
    $.blockUI({
        message: $('#popUpScreen'),
        css: {
            width: '580px',
            height: 'auto',
            position: 'absolute',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    $("#pageTitle").text("Resolve Ticket");
    $('#popUpScreen').parent('.blockUI.blockMsg').css('left', 1400);
    $('#popUpScreen').parent('.blockUI.blockMsg').css('top', 200);

}

function dispatchTicket() {
    if (!$("#overrideNextAction").is(":checked") && !checkNextActionStatus()) {
        return false;
    }
    if (!checkChangedForm()) {
        return false;
    }

    var ilexNumber = $(".wipTicketsActive").attr("ilexNumber");

    $("div.tableWrapper table tr:nth-child(n+1)").filter(function () {
        return $(this).attr("ilexNumber") == ilexNumber;

    }).each(
            function () {
                $(this).find("span.redIcon").draggable("enable", 1);
                $(this).find("span.redIcon").addClass('greenIcon').removeClass(
                        'redIcon');

            });

    $("#activeTicketDiv").hide();
    $(".wipTicketsActive").text($(".wipTicketsActive").attr("id"));
    $(".wipTicketsActive").removeAttr(
            "ilexNumber  customer customerNumber sitenumber");
    $(".wipTicketsActive")
            .addClass("wipTicketsInActive")
            .removeClass(
                    "wipTicketsActivePriority1  wipTicketsActivePriority3 wipTicketsActive");
    $.blockUI({
        message: $('#popUpScreen'),
        css: {
            width: '580px',
            height: 'auto',
            position: 'absolute',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    $("#pageTitle").text("Dispatch Ticket");
    $('#popUpScreen').parent('.blockUI.blockMsg').css('left', 1400);
    $('#popUpScreen').parent('.blockUI.blockMsg').css('top', 200);

}
function setPreviousTicket() {

    if ($("#activeTicketDiv").is(':hidden')) {
        return true;
    }

    $(".wipTicketsActive").attr("recordedEfforts", $("#effortMinutes").val());

    if ($("input[type='radio'][name='nextActionTime']").is(':checked')) {
        var nextActionTimeRadio = $(
                "input[type='radio'][name='nextActionTime']:checked").val();

        var nextActionTime = $("#nextActionTime").val();
        $(".wipTicketsActive").attr("nextActionUnit", nextActionTimeRadio);
        $(".wipTicketsActive").attr("nextActionTime", nextActionTime);
    } else {
        $(".wipTicketsActive").attr("nextActionUnit", "date");
        $(".wipTicketsActive").attr("nextActionTime",
                $("#wipNextActionDueDate").val());

    }
    $(".wipTicketsActive").attr("workNotes", $("#ticketWorkNotes").val());

    return true;
}
function resetActiveForm() {
    $("#effortMinutes").val("");
    $("#nextActionTime").val("");
    $("#wipStartDate").val("");
    $("#wipStopDate").val("");
    $("#recordEffortButton").removeClass('FMBUT0005').addClass('FMBUT0003');
    $('#ticketWorkNotes').val("");
    $("input[type='radio'][name='nextActionTime']:checked").each(function () {
        $(this).prop('checked', false);
    });
    $("#wipNextActionDueDate").val("");
    $("#overrideNextAction").prop("checked", false);
    $("#overrideSpellCheck").prop("checked", false);
    ticketchanged.recordEffortChanged = false;
    ticketchanged.nextActionChanged = false;
    ticketchanged.workNotesChanged = false;
    ticketchanged.ticketStateChanged = false;
    ticketchanged.circuitStatusChanged = false;
    $("#updateMessage").html("&nbsp;");
    $("#updateMessage").hide();
}

/* function :To open Enalrge view of cube -start */
$(".enlargeView")
        .live(
                'click',
                function () {
                    $enlargeViewlImg = $(this);
                    var tableId = $enlargeViewlImg.parents('div').find('table')
                            .attr('id');
                    var url = pageContextPath
                            + '/hdMainTable/enlargeCubeView.html?tableId='
                            + tableId;
                    window
                            .open(
                                    url,
                                    'popupwindow',
                                    'width=900,height=500,top=100,left=150,scrollbars=yes,status=yes,resizable=yes,location=1,toolbar=yes,');

                }
        );
/* function :To open Enlarge view of cube -end */

// ticket edit popup

function openEnlargePopupForTier2() {

    var url = pageContextPath
            + '/hdMainTable/enlargeCubeView.html?tableId=monitoringDateTimeDetailsTable';
    window
            .open(
                    url,
                    'popupwindow',
                    'width=900,height=500,top=100,left=150,scrollbars=yes,status=yes,resizable=yes,location=1,toolbar=yes,');

}


function openEventCorrelatorPopup(type) {
    var url = pageContextPath
            + '/hdMainTable/EventCorrelatorAction.html?type=' + type;
    window
            .open(
                    url,
                    'popupwindow',
                    'width=900,height=500,top=100,left=150,scrollbars=yes,status=yes,resizable=yes,location=1,toolbar=yes,');

}



$('div.tableWrapper .mainTable tr:nth-child(n+1) td:nth-child(3) ')
        .live(
                'click', function openDirectRowUpdate() {

                    $tr = this.parentNode;
                    var ticketId = $($tr).attr("ticketId");

                    openTicketSummary(ticketId);

                });



$('div.tableWrapper #hdResolvedTable tr:nth-child(n+1) td:nth-child(3) ')
        .live(
                'click', function openDirectRowUpdate() {

                    $tr = this.parentNode;
                    var ticketId = $($tr).attr("ticketId");

                    openTicketSummaryResolved(ticketId);

                });


$('div.tableWrapper #hdResolvedTable tr:nth-child(n+1) td:last-child ')
        .live(
                'click', function openDirectRowUpdate() {

                    $tr = this.parentNode;
                    var ticketId = $($tr).attr("ticketId");

                    openTicketSummaryResolved(ticketId);

                });





$('div.tableWrapper table:not(.nonEditableTable) tr:nth-child(n+1) td:last-child ')
        .live(
                'click',
//				function openTicketSummary(ticketId) {
//
//					currentTicketId = ticketId;
//					$('#ticketPopUpScreen').block({
//						message : $('#popUpScreen2'),
//						css : {
//							width : '1150',
//							height : 'auto',
//							position : 'absolute',
//							color : 'black'
//
//						},
//						overlayCSS : {
//							backgroundColor : '#f0f0f0',
//							opacity : 0.3
//						},
//						centerX : true,
//						centerY : true,
//						fadeIn : 0,
//						fadeOut : 0
//					});
//
//					$("#pageTitle2").text(
//							"[Customer Name, Appendix, Site Number, Ticket Type, Ilex #]");
//					$("#popupBody2").load(
//							pageContextPath + "/hdMainTable/hdTicketSummary.html?ticketId="
//									+ ticketId);
//
//					var ODTop=$("#hdWipDivOverDue").offset().top;
//					var block1Top=$("#ticketPopUpScreen").offset().top;
//					var ODLeft=$("#hdWipDivOverDue").offset().left - 110;
//					var block1Left=$("#ticketPopUpScreen").offset().left;
//					var currentTop=(block1Top-ODTop)+20;
//					var currentLeft=(ODLeft-block1Left);
//
//					$('#popUpScreen2').parent('.blockUI.blockMsg').css('left', currentLeft);
//					$('#popUpScreen2').parent('.blockUI.blockMsg').css('top', -currentTop);
//
//				});

                function openRowUpdateDialog() {
                    $td = this;
                    var tableEmpty = $(this).hasClass('dataTables_empty');
                    if (tableEmpty == false) {
                        $tr = this.parentNode;

                        ticketPopUp = true;


                        //$('#ticketPopUpScreen').parent('.blockUI.blockMsg')
                        //		.css('left', $($tr).offset().left);
                        //$('#ticketPopUpScreen').parent('.blockUI.blockMsg')
                        //	.css('top', $($tr).offset().top + 20);
                        ilexTicketNumber = $($tr).attr("ilexNumber");
                        siteNumber = $($tr).attr("siteNumber");
                        $div = $("<div/>");
                        $div.append(siteNumber);
                        siteNumber = $($div).text();
                        headerText = " Ticket Actions For "
                                + ilexTicketNumber + ", Site: "
                                + siteNumber;
                        $('#ticketPageTitle').text(headerText);

                        customer = $($tr).attr("customer");
                        appendixName = $($tr).attr("appendixName");

                        ticketId = $($tr).attr("ticketId");
                        nextActionId = $($tr).attr("nextActionId");

                        //The two variables added for WIP & WIP Overdue changes
                        status = $($tr).attr("status");
                        ticketSourceno = $($tr).attr("ticketsource");

                        state = $($tr).attr("state");
                        isStateContainsDown = $(state).text().search("Down");   //0 if found, -1 if not

                        var bodyContents = '<div style="margin-bottom:10px;">'
                                + customer + ', Appendix: ' + appendixName + '</div>';

                        var table = $($tr).parent().parent().attr("id");
                        var actionsDiv = "";
                        var detailsDiv = "";
                        var historyDiv = "";

                        if (table == "hdPendingAckCustTable" || table == "hdPendingAckSecurityTable") {

                            $.blockUI({
                                message: $('#ticketPopUpScreen'),
                                css: {
                                    width: '700px',
                                    height: 'auto',
                                    position: 'absolute',
                                    color: 'black'

                                },
                                overlayCSS: {
                                    backgroundColor: '#f0f0f0',
                                    opacity: 0.3
                                },
                                centerX: true,
                                centerY: true,
                                fadeIn: 0,
                                fadeOut: 0
                            });

                            $('#ticketPopUpScreen').parent('.blockUI.blockMsg').css("width", "500px");
                            actionsDiv += '<div style="height:180px;width:120px;float:left;margin-left:30px;">';
                            actionsDiv += '<div class="PCSTD0011" style="padding-left:10px;margin-bottom:10px;" >Actions</div>';
                            actionsDiv += '<div style="width:100px;margin-right:50px;margin-bottom:10px;">';
                            actionsDiv += '<div class="aUnderline"><a href="#" onclick="ackTicketStepFromTicketActions(\'hdPendingAckCust\', ' +
                                    ticketId + ')">Acknowledge Ticket</a></div>';
                            actionsDiv += '<div class="aUnderline"><a href="#" onclick="openCancelPage(' + ticketId + ')" >Cancel</a></div>';
                            actionsDiv += '</div>';
                            actionsDiv += '<div>&nbsp;</div>';


                            actionsDiv += '</div>';

                            bodyContents += actionsDiv;

                            detailsDiv += '<div style="height:180px;width:120px;float:left;">';
                            detailsDiv += '<div class="PCSTD0011" style="padding-left:30px;margin-bottom:10px;" >Status</div>';
                            detailsDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" onclick="openAckTicketSummary('
                                    + ticketId + ')"  >Ticket Details</a></div>';
                            detailsDiv += '</div>';

                            bodyContents += detailsDiv;

                            historyDiv += '<div style="height:180px;width:120px;float:left;">';
                            historyDiv += '<div class="PCSTD0011" style="padding-left:30px;margin-bottom:10px;" >History</div>';
                            historyDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" >Tickets</a></div>';
                            historyDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" >Performance (30 days)</a></div>';

                            historyDiv += '</div>';

                            bodyContents += historyDiv;

                            $("#ticketPopupBody").html(
                                    "<div style='text-align:left;' class='WASTD0003 PCSTD0010'>"
                                    + bodyContents + "</div>");

                        } else if (table == "hdPendingAckAlertTable") {

                            $.blockUI({
                                message: $('#ticketPopUpScreen'),
                                css: {
                                    width: '700px',
                                    height: 'auto',
                                    position: 'absolute',
                                    color: 'black'

                                },
                                overlayCSS: {
                                    backgroundColor: '#f0f0f0',
                                    opacity: 0.3
                                },
                                centerX: true,
                                centerY: true,
                                fadeIn: 0,
                                fadeOut: 0
                            });

                            $('#ticketPopUpScreen').parent('.blockUI.blockMsg').css("width", "500px");
                            actionsDiv += '<div style="height:180px;width:120px;float:left;margin-left:30px;">';
                            actionsDiv += '<div class="PCSTD0011" style="padding-left:10px;margin-bottom:10px;" >Actions</div>';
                            actionsDiv += '<div style="width:100px;margin-right:50px;margin-bottom:10px;">';
                            actionsDiv += '<div class="aUnderline"><a href="#" onclick="ackTicketStepFromTicketActions(\'hdPendingAckAlert\', ' +
                                    ticketId + ')">Acknowledge Ticket</a></div>';
                            actionsDiv += '<div class="aUnderline"><a href="#" onclick="openNoActionPage(' + ticketId + ')" >Cancel</a></div>';
                            actionsDiv += '</div>';
                            actionsDiv += '<div>&nbsp;</div>';


                            actionsDiv += '</div>';

                            bodyContents += actionsDiv;

                            detailsDiv += '<div style="height:180px;width:150px;float:left;">';
                            detailsDiv += '<div class="PCSTD0011" style="padding-left:30px;margin-bottom:10px;" >Status</div>';
                            detailsDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" onclick="openAckTicketSummary('
                                    + ticketId + ')"  >Ticket Details</a></div>';
                            detailsDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" >Alert Condition Details</a></div>';
                            detailsDiv += '</div>';

                            bodyContents += detailsDiv;

                            historyDiv += '<div style="height:180px;width:120px;float:left;">';
                            historyDiv += '<div class="PCSTD0011" style="padding-left:30px;margin-bottom:10px;" >History</div>';
                            historyDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" >Tickets</a></div>';
                            historyDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" >Cancelled (14 days)</a></div>';
                            historyDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" >Performance (30 days)</a></div>';

                            historyDiv += '</div>';

                            bodyContents += historyDiv;

                            $("#ticketPopupBody").html(
                                    "<div style='text-align:left;' class='WASTD0003 PCSTD0010'>"
                                    + bodyContents + "</div>");

                        } else if (table == "hdPendingAckMonitorTable" || table == "hdPendingAckMonitorTier2Table") {

                            $.blockUI({
                                message: $('#ticketPopUpScreen'),
                                css: {
                                    width: '700px',
                                    height: 'auto',
                                    position: 'absolute',
                                    color: 'black'

                                },
                                overlayCSS: {
                                    backgroundColor: '#f0f0f0',
                                    opacity: 0.3
                                },
                                centerX: true,
                                centerY: true,
                                fadeIn: 0,
                                fadeOut: 0
                            });


                            $('#ticketPopUpScreen').parent('.blockUI.blockMsg').css("width", "500px");
                            actionsDiv += '<div style="height:180px;width:120px;float:left;margin-left:30px;">';
                            actionsDiv += '<div class="PCSTD0011" style="padding-left:10px;margin-bottom:10px;" >Actions</div>';
                            actionsDiv += '<div style="width:100px;margin-right:50px;margin-bottom:10px;">';
                            actionsDiv += '<div class="aUnderline"><a href="#" onclick="ackTicketStepFromTicketActions(\'hdPendingAckMonitor\', ' +
                                    ticketId + ')">Acknowledge Ticket</a></div>';

                            customerName = $($tr).attr("Customer");

                            if (isStateContainsDown == 0) {
                                actionsDiv += '<div class="aUnderline">Cancel</div>';
                            } else {
                                actionsDiv += '<div class="aUnderline"><a href="#" onclick="openNoActionPage(' + ticketId + ')" >Cancel</a></div>';
                            }
                            actionsDiv += '</div>';
                            actionsDiv += '<div>&nbsp;</div>';


                            actionsDiv += '</div>';

                            bodyContents += actionsDiv;

                            detailsDiv += '<div style="height:180px;width:120px;float:left;">';
                            detailsDiv += '<div class="PCSTD0011" style="padding-left:30px;margin-bottom:10px;" >Status</div>';
                            detailsDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" onclick="openAckTicketSummary('
                                    + ticketId + ')"  >Ticket Details</a></div>';
                            detailsDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" onclick="openStateTailFromAction('
                                    + ticketId + ')"  >State Tail</a></div>';
                            detailsDiv += '</div>';

                            bodyContents += detailsDiv;

                            historyDiv += '<div style="height:180px;width:120px;float:left;">';
                            historyDiv += '<div class="PCSTD0011" style="padding-left:30px;margin-bottom:10px;" >History</div>';
                            historyDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" >Tickets</a></div>';
                            historyDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" onclick="openNoActionsDaysPage('
                                    + ticketId + ')">Cancelled (14 days)</a></div>';
                            historyDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" >Performance (30 days)</a></div>';

                            historyDiv += '</div>';

                            bodyContents += historyDiv;

                            $("#ticketPopupBody").html(
                                    "<div style='text-align:left;' class='WASTD0003 PCSTD0010'>"
                                    + bodyContents + "</div>");
                        } else {


                            openTicketSummary(ticketId);


//							var ticketLocked=false;
//							if($(this).find("span").hasClass("redIcon"))
//								{
//								ticketLocked=true;
//								}
//							if(ticketLocked)
//								{
//								actionsDiv+= '<div style="height:280px;width:240px;float:left;margin-left:30px;">';
//								actionsDiv += '<div class="PCSTD0011" style="padding-left:50px;margin-bottom:10px;" >Actions</div>';
//								actionsDiv += '<div style="width:100px;float:left;margin-right:50px;margin-bottom:10px;">';
//								actionsDiv += '<div><a class="aUnderlinehide" href="javascript:void(0);">Enter Work Notes</a></div>';
//								actionsDiv += '<div><a class="aUnderlinehide" href="javascript:void(0);">Record Effort</a></div>';
//								actionsDiv += '<div><a class="aUnderlinehide" href="javascript:void(0);">Update Next Action</a></div>';
//								actionsDiv += '<div class="aUnderline" style="margin-bottom: 0px!important;"><a href="#" onclick="openTicketSummary('
//									+ ticketId + ')" >Edit Ticket Details</a></div>';
//								actionsDiv += '<div><a class="aUnderlinehide" href="javascript:void(0);" >Resolve Ticket</a></div>';
//								actionsDiv += '<div><a class="aUnderlinehide" href="javascript:void(0);" >Request ISP Dispatch</a></div>';
//								actionsDiv += '<div><a class="aUnderlinehide" href="javascript:void(0);" >Request CNS Dispatch</a></div>';
//								actionsDiv += '<div><a class="aUnderlinehide" href="javascript:void(0);" >Assigned to Customer</a></div>';
//								actionsDiv += '</div>';
//								}
//							else{
//								actionsDiv+= '<div style="height:280px;width:240px;float:left;margin-left:30px;">';
//								actionsDiv += '<div class="PCSTD0011" style="padding-left:50px;margin-bottom:10px;" >Actions</div>';
//								actionsDiv += '<div style="width:100px;float:left;margin-right:50px;margin-bottom:10px;">';
//								actionsDiv += '<div class="aUnderline"><a href="#" onclick="openWorkNotesScreen('
//									+ ticketId + ')">Enter Work Notes</a></div>';
//								actionsDiv += '<div class="aUnderline"><a href="#" onclick="openEffortScreen('
//									+ ticketId + ')">Record Effort</a></div>';
//								actionsDiv += '<div class="aUnderline"><a href="#" onclick="openNextActionScreen('
//									+ ticketId +','+nextActionId+ ')">Update Next Action</a></div>';
//								actionsDiv += '<div class="aUnderline"><a href="#" onclick="openTicketSummary('
//										+ ticketId + ')" >Edit Ticket Details</a></div>';
//								if(status=='WIP'){
//									actionsDiv += '<div class="aUnderline"><a href="#" onclick="openResolveTicket('
//										+ ticketId + ')"   >Resolve Ticket</a></div>';
//									actionsDiv += '<div class="aUnderline" ><a href="#"  onclick="openEditTicketState('
//										+ ticketId +',3)">Request ISP Dispatch</a></div>';
//									actionsDiv += '<div class="aUnderline"><a href="#"  onclick="openEditTicketState('
//										+ ticketId +',7)">Request CNS Dispatch</a></div>';
//									actionsDiv += '<div class="aUnderline"><a href="#"  onclick="openEditTicketState('
//										+ ticketId +',9)">Assigned to Customer</a></div>';
//									actionsDiv += '</div>';
//									}
//									else
//									{
//										actionsDiv += '<div><a class="aUnderlinehide" href="javascript:void(0);" >Resolve Ticket</a></div>';
//										actionsDiv += '<div><a class="aUnderlinehide" href="javascript:void(0);" >Request ISP Dispatch</a></div>';
//										actionsDiv += '<div><a class="aUnderlinehide" href="javascript:void(0);" >Request CNS Dispatch</a></div>';
//										actionsDiv += '<div><a class="aUnderlinehide" href="javascript:void(0);" >Assigned to Customer</a></div>';
//										actionsDiv += '</div>';
//
//									}
//								}
//
//						/*actionsDiv += '<div style="width:100px;margin-bottom:10px;">';
//
//						actionsDiv += '<div class="aUnderline"><a href="#" >Backup Status</a></div>';
//						actionsDiv += '<div class="aUnderline"><a href="#" >Priority</a></div>';
//						actionsDiv += '<div class="aUnderline"><a href="#" >View NextMedX Tab</a></div>';
//						actionsDiv += '</div>';*/
//						actionsDiv += '<div>&nbsp;</div>';
//						actionsDiv += '<div style="margin-bottom:10px;margin-left:0px;"></div>';
//						actionsDiv += '</div>';
//
//						bodyContents += actionsDiv;
//
//						detailsDiv+= '<div style="height:280px;width:240px;float:left;">';
//						detailsDiv += '<div class="PCSTD0011" style="padding-left:25px;margin-bottom:10px;" >Details</div>';
//						detailsDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" onclick="openTicketSummary('
//								+ ticketId + ')"  >Ticket Details</a></div>';
//						detailsDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" onclick="openLatestWorkNotes('
//							+ ticketId + ')"  >Work Notes</a></div>';
//						if(ticketSourceno=='2')
//							{
//							detailsDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" onclick="openStateTailFromAction('
//								+ ticketId + ')"  >State Tail</a></div>';
//							}
//						if(ticketSourceno=='5')
//							{
//							detailsDiv += '<div class="aUnderline" style="margin-left:20px;"><a class="aUnderlinehide" href="javascript:void(0);">Alert Condition</a></div>';
//							}
//						/*detailsDiv += '<div class="aUnderline" style="margin-left:20px;"><a href="#" >[Ticket Type Dependent]</a></div>';						*/
//						detailsDiv += '</div>';
//
//						bodyContents += detailsDiv;
//
//						historyDiv += '<div style="height:280px;width:150px;float:left;">';
//						historyDiv += '<div class="PCSTD0011" style="padding-left:30px;margin-bottom:10px;" >History</div>';
//										historyDiv += '<div  style="margin-left:20px;"><a class="aUnderlinehide" href="javascript:void(0);" href="#" >Tickets</a></div>';
//						historyDiv += '<div style="margin-left:20px;"><a class="aUnderlinehide" href="javascript:void(0);" >Performance (30 Days)</a></div>';
//
//						historyDiv += '</div>';
//
//						bodyContents += historyDiv;
//
                        }


                    }
                });

// code added for tooltip -start
var cX = 0;
var cY = 0;
var rX = 0;
var rY = 0;
function UpdateCursorPosition(e) {
    cX = e.pageX;
    cY = e.pageY;
}
function UpdateCursorPositionDocAll(e) {
    cX = e.clientX;
    cY = e.clientY;
}
if (document.all) {
    document.onmousemove = UpdateCursorPositionDocAll;
} else {
    document.onmousemove = UpdateCursorPosition;
}
function AssignPosition(d, divId) {
    d.style.left = $(divId).offset().left + "px";
    d.style.top = $(divId).offset().top + $(divId).height() + 7 + "px";
}
function HideContent(d) {
    if (d.length < 1) {
        return;
    }
    document.getElementById(d).style.display = "none";
}
function ShowContent(d, divId) {
    if (d.length < 1) {
        return;
    }
    var dd = document.getElementById(d);
    AssignPosition(dd, divId);
    dd.style.display = "block";
    dd.style.background = "#EFEFEF";
}
function ReverseContentDisplay(d) {
    if (d.length < 1) {
        return;
    }
    var dd = document.getElementById(d);
    AssignPosition(dd);
    if (dd.style.display == "none") {
        dd.style.display = "block";
    } else {
        dd.style.display = "none";
    }
}
// code added for tooltip -end

$("td.shortDescriptionText").live("click", function (e) {
    $('#toolTip').html($(this).parent().attr('shortDescription'));
    ShowContent('toolTip', this);
});
$("td.shortDescriptionText").live("mouseout", function () {
    HideContent('toolTip');
});


$("td.siteNumberText").live("click", function (e) {


    //alert("yes");
});


$(".siteDetails").live(
        "click",
        function (e) {

            var siteNumber = $(this).parent().parent().attr("siteNumber");
            var siteAddress = $(this).parent().parent().attr("siteaddress");
            var siteCity = $(this).parent().parent().attr("sitecity");
            var siteState = $(this).parent().parent().attr("sitestate");
            var siteCountry = $(this).parent().parent().attr("sitecountry");
            if (siteCountry == "US") {
                siteCountry = "";
            }
            var siteZipcode = $(this).parent().parent().attr("sitezipcode");
            var contactName = $(this).parent().parent().attr("contactname");
            if (contactName) {

            } else {
                contactName = "";
            }
            var contactPhoneNo = $(this).parent().parent().attr("contactphonenumber");
            if (contactPhoneNo) {

            } else {
                contactPhoneNo = "";
            }
            var contactEmail = $(this).parent().parent().attr("contactemail");
            if (contactEmail) {

            } else {
                contactEmail = "";
            }
            var content = '<div><u>Site</u>: ';
            content += '<br/>' + siteNumber;
            content += '<br/>' + siteAddress;
            content += '<br/>' + siteCity + ',&nbsp;' + siteState + siteCountry + '&nbsp;'
                    + siteZipcode + '<br/>';
            content += '<br/><u>Contact</u>:';
            if (contactName != "")
                content += '<br/>' + contactName + '';
            if (contactPhoneNo != "")
                content += '<br/>' + contactPhoneNo + '';
            if (contactEmail != "")
                content += '<br/>' + contactEmail + '';
            content += '</div>';
            //code for work notes
            var worNotesTitle = '<br/><div><u>Recent Work Notes (Latest 2)</u></div>';
            content += worNotesTitle;
            var ticketId = $(this).parent().parent().attr("ticketId");
            $.ajax({
                type: 'post',
                async: false,
                url: pageContextPath + '/hdMainTable/getWorkNotesList.html',
                data: {'ticketId': ticketId},
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    var count = 0;
                    var worNotesContent = '<div>';
                    $.each(obj, function (idx, workNoteDTO) {
                        if (count >= 2)
                            return false;
                        worNotesContent += '<div class="divFloatLeft"  style="width: 100px;text-align: left;">' + workNoteDTO.workNoteDateStr + '</div>';
                        worNotesContent += '<div style="margin-left:100px;width: 300px;word-break: keep-all;word-wrap: break-word;"><div style="word-break: keep-all;">' + workNoteDTO.workNote + '</div><div style="color: #1786d4;">[' + workNoteDTO.userLastName + ']</div></div>';
                        count++;

                    });
                    worNotesContent += '</div>';
                    if (count == 0)
                    {
                        worNotesContent = '<div>There are no work notes.</div>';
                    }
                    content += worNotesContent;
                }
            });
            $('#toolTip').html(content);
            ShowContent('toolTip', $(this).parent());

        });

















$("td.DateDiffMonitoringTimeCube").live("mouseout", function () {
    HideContent('toolTip');
});

$("td.DateDiffMonitoringTimeCube").live("mouseover", function () {
    $(this).attr("title", "Show Details");
});

$("td.DateDiffMonitoringTimeCube").live(
        "click",
        function (e) {
            //  var siteNumber = $(this).parent().attr("siteNumber");


            var content = '<table style="width: 400px;"> <tr><td><u>Time Details</u>:</td></tr> ';
            content += '<tr> ';
            content += '<td style="width: 100px;text-align: left;">Start Time</td>';
            content += '<td style="width: 100px;text-align: left;">End Time</td>';
            content += '<td style="width: 50px;text-align: left;">Difference</td></tr>';
            var ticketId = $(this).parent().attr("ticketId");
            $.ajax({
                type: 'post',
                async: false,
                url: pageContextPath + '/hdMainTable/getDateTimeListForMonitoring.html',
                data: {'ticketId': ticketId},
                success: function (data) {
                    var obj = jQuery.parseJSON(unescape(decodeURI(data).replace(/\+/g, " ")));
                    var count = 0;
                    var worNotesContent = '';
                    $.each(obj, function (idx, workNoteDTO) {
                        worNotesContent += '<tr>';
//                        if (count >= 2)
//                            return false;
                        worNotesContent += '<td style="width: 100px;text-align: left;">' + workNoteDTO.startDate + '</td>';
                        worNotesContent += '<td style="width: 100px;text-align: left;">' + workNoteDTO.endDate + '</td>';
                        worNotesContent += '<td style="width: 50px;text-align: left;">' + workNoteDTO.dateDiff + '</td>';
                        // worNotesContent += '<div class="divFloatLeft"  style="width: 100px;text-align: left;">' + workNoteDTO.workNoteDateStr + '</div>';
                        // worNotesContent += '<div style="margin-left:100px;width: 200px;word-break: break-all;word-wrap: break-word;"><div>' + workNoteDTO.workNote + '</div><div style="color: #1786d4;">[' + workNoteDTO.userLastName + ']</div></div>';
                        worNotesContent += '</tr>';
                        count++;

                    });
                    //worNotesContent += '</div>';
//                    if (count == 0)
//                    {
//                        worNotesContent = '<div>There are no work notes.</div>';
//                    }
                    content += worNotesContent;
                }
            });
            $('#toolTip').html(content + '</table>');
            ShowContent('toolTip', this);
        });
















$("td.siteNumberTextPendingCubes").live("mouseout", function () {
    HideContent('toolTip');
});

$("td.siteNumberTextPendingCubes").live(
        "click",
        function (e) {
            var siteNumber = $(this).parent().attr("siteNumber");
            var siteAddress = $(this).parent().attr("siteaddress");
            var siteCity = $(this).parent().attr("sitecity");
            var siteState = $(this).parent().attr("sitestate");
            var siteCountry = $(this).parent().attr("sitecountry");
            if (siteCountry == "US") {
                siteCountry = "";
            }
            var siteZipcode = $(this).parent().attr("sitezipcode");
            var contactName = $(this).parent().attr("contactname");
            if (contactName) {

            } else {
                contactName = "";
            }
            var contactPhoneNo = $(this).parent().attr("contactphonenumber");
            if (contactPhoneNo) {

            } else {
                contactPhoneNo = "";
            }
            var contactEmail = $(this).parent().attr("contactemail");
            if (contactEmail) {

            } else {
                contactEmail = "";
            }
            var content = '<div><u>Site</u>: ';
            content += '<br/>' + siteNumber;
            content += '<br/>' + siteAddress;
            content += '<br/>' + siteCity + ',&nbsp;' + siteState + siteCountry + '&nbsp;'
                    + siteZipcode + '<br/>';
            content += '<br/><u>Contact</u>:';
            if (contactName != "")
                content += '<br/>' + contactName + '';
            if (contactPhoneNo != "")
                content += '<br/>' + contactPhoneNo + '';
            if (contactEmail != "")
                content += '<br/>' + contactEmail + '';
            content += '</div>';
            //code for work notes
            var worNotesTitle = '<br/><div><u>Recent Work Notes (Limited to 2)</u></div>';
            content += worNotesTitle;
            var ticketId = $(this).parent().attr("ticketId");
            $.ajax({
                type: 'post',
                async: false,
                url: pageContextPath + '/hdMainTable/getWorkNotesList.html',
                data: {'ticketId': ticketId},
                success: function (data) {
                    var obj = jQuery.parseJSON(unescape(decodeURI(data).replace(/\+/g, " ")));
                    var count = 0;
                    var worNotesContent = '<div>';
                    $.each(obj, function (idx, workNoteDTO) {
                        if (count >= 2)
                            return false;
                        worNotesContent += '<div class="divFloatLeft"  style="width: 100px;text-align: left;">' + workNoteDTO.workNoteDateStr + '</div>';
                        worNotesContent += '<div style="margin-left:100px;width: 200px;word-break: break-all;word-wrap: break-word;"><div>' + workNoteDTO.workNote + '</div><div style="color: #1786d4;">[' + workNoteDTO.userLastName + ']</div></div>';
                        count++;

                    });
                    worNotesContent += '</div>';
                    if (count == 0)
                    {
                        worNotesContent = '<div>There are no work notes.</div>';
                    }
                    content += worNotesContent;
                }
            });
            $('#toolTip').html(content);
            ShowContent('toolTip', this);
        });

$("td.siteNumberText").live("mouseout", function () {
    HideContent('toolTip');
});

$(".impact")
        .live(
                "mouseover",
                function (e) {
                    var content = '';
                    content += '<b>Impact</b><br/>' + $.trim($(this).text())
                            + '<br/>';
                    if ($.trim($(this).text()) == "1-High") {
                        content += '<ul class="niceList"><li>Degradation of service to a point in which the customer is unable to perform business operation</li>';
                        content += '<li>Incident has caused a work stoppage or potential to cause a work stoppage of a vital<br/> business function or service</li>';
                        content += '<li>Financial impact is or has the potential to be high</li>';
                        content += '<li>Impact to business reputation is or has the potential to be high</li></ul>';

                    }
                    if ($.trim($(this).text()) == "2-Medium") {
                        content += '<ul class="niceList"><li>The incident has not resulted in the stoppage of business operations, but it has significantly<br/> impaired normal business operations and a workaround is not available</li>';
                        content += '<li>Financial impact could be moderate</li>';
                        content += '<li>Impact to business reputation could be moderate</li>';

                    }
                    if ($.trim($(this).text()) == "3-Low") {
                        content += '<ul class="niceList"><li>The incident has not resulted in the stoppage of business operations, but it has impaired normal<br/>business operations. A workaround is available. </li>';
                        content += '<li>Financial impact is low</li>';
                        content += '<li>Impact to business reputation is low</li>';

                    }

                    $('#toolTip').html(content);
                    ShowContent('toolTip', this);
                });

$(".impact").live("mouseout", function () {
    HideContent('toolTip');
});

$("span.primaryStateIcon").live(
        "click",
        function (e) {
            var ticketId = $(this).parent().parent().attr("ticketId");
            var stateTrailContentTitle = "<div style='padding-left:5px;'><u>WUG State Trail</u></div>";
            $('#toolTip').html(stateTrailContentTitle);
            $.ajax({
                type: 'GET',
                url: pageContextPath + '/hdMainTable/stateTail.html?ticketId=' + ticketId,
                success: function (obj) {
                    $('#toolTip').append(obj);
                }
            });
            ShowContent('toolTip', $(this).parent());
            return false;

        });
$("span.primaryStateIcon").live("mouseout", function () {
    HideContent('toolTip');
});

$(".urgency")
        .live(
                "mouseover",
                function (e) {
                    var content = '';
                    content += '<b>Urgency</b><br/>' + $.trim($(this).text())
                            + '<br/>';
                    if ($.trim($(this).text()) == "1-High") {
                        content += '<ul class="niceList"><li>Issue affects entire customer network i.e. VPN concentrator is down</li>';
                        content += '<li>Issue affects whole organization i.e. VPN to corporate is down</li>';

                    }
                    if ($.trim($(this).text()) == "2-Medium") {
                        content += '<ul class="niceList"><li>Issue affects a number of client locations i.e. Area ISP outage</li>';
                        content += '<li>Issue affects entire individual service i.e. Stores cannot reach food ordering website</li>';

                    }
                    if ($.trim($(this).text()) == "3-Low") {
                        content += '<ul class="niceList"><li>Issue affects individual location i.e. DSL service is down</li>';
                        content += '<li>Issue affects few number of users i.e. 1-5 users</li>';
                    }

                    $('#toolTip').html(content);
                    ShowContent('toolTip', this);
                });

$(".urgency").live("mouseout", function () {
    HideContent('toolTip');
});

function createTicketStep1() {
    $.blockUI({
        message: $('#popUpScreen'),
        css: {
            width: '600px',
            height: 'auto',
            position: 'absolute',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    $("#pageTitle").text("Create Ticket - Select Customer and Appendix");
    $('#popUpScreen').parent('.blockUI.blockMsg').css('left', 200);
    $('#popUpScreen').parent('.blockUI.blockMsg').css('top', 120);

    $.ajax({
        type: 'POST',
        url: pageContextPath + '/hdMainTable/createTicketStep1.html',
        async: false,
        success: function (data) {
            $("#popupBody").html(data);
        }

    });

}


function createTicketStep2() {
//
//	$.blockUI({
//		message : $('#popUpScreen'),
//		css : {
//			width : '1100',
//			height : 'auto',
//			position : 'absolute',
//			color : 'black'
//		},
//		overlayCSS : {
//			backgroundColor : '#f0f0f0',
//			opacity : 0.3
//		},
//		centerX : true,
//		centerY : true,
//		fadeIn : 0,
//		fadeOut : 0
//	});
//
//
//	$("#pageTitle").text("Create Ticket - Enter Ticket Details");
//	$('#popUpScreen').parent('.blockUI.blockMsg').css('left', 200);
//	$('#popUpScreen').parent('.blockUI.blockMsg').css('top', 120);
//

    //alert($("input[name='isFromMain']").val());



    if ($("#isFromMain").val() != "yes") {
        var newWindow = null;
        newWindow = window.open('', '', 'width=1210,height=800,scrollbars=yes');
    }

    var hdCustomerValue = "";//$("#hdCustomer").val();\


    if ($("#hdCustomer").val()) {
        hdCustomerValue = $("#hdCustomer").val();
        isFromMain = "yes";
    }

    //  var hdCustomerValue = "658~2712~Admiral Petroleum Company~Appendix 1 - NetMedX~NetMedX,MSP Help Desk";
    var hdTicketNumber = '';
    if ($("#ticketNumber").val()) {
        hdTicketNumber = $("#ticketNumber").val();

    }

    //alert(hdTicketNumber +" --- "+ hdCustomerValue);
    var url = pageContextPath + '/hdMainTable/createTicketStep2.html';



    $.ajax({
        type: 'POST',
        data: {
            hdCustomer: hdCustomerValue,
            isFromMain: isFromMain,
            hdTicketNumber: hdTicketNumber


        },
        url: url,
        async: false,
        success: function (data) {
            //$("#popupBody").html(data);
            //temp();


            newWindow.document.write(data);
            newWindow.document.close();



        }

    });

}









function checkSecondList() {

    var priorityVal = getPriority();
    $("#priority").attr("value", priorityVal);
    if (priorityVal == "Planned") {
        $("#priority_label").html("Low");
    } else {
        $("#priority_label").html($("#priority").val());
    }



//	/*if ($("#impact").val() == '0'){
//		//alert("Please select Impact");
//		$("#impact").focus();
//		return false;
//	}
//	else */
//	if ($("#urgency").val() == '0'){
//		//alert("Please select Urgency");
//		$("#urgency").focus();
//		return false;
//	}
//	else {
//		//$("#priority").attr("value"," ");
//		$("#priority").attr("value",getPriority());
//		return true;
//
//
//		}

}






function getPriority() {

    var appendixId = '';
    var impact = '';
    var urgency = '';


    if ($("#appendixId").val()) {
        appendixId = $("#appendixId").val();
    }
    if ($("#impact").val()) {
        impact = $("#impact").val();
    }
    if ($("#urgency").val()) {
        urgency = $("#urgency").val();
    }


    if (impact == '1' && urgency == '1') {
        return 'Critical';
    }
    else if (impact == '2' && urgency == '1') {
        return 'High';
    }
    else if (impact == '3' && urgency == '1') {
        return 'Medium';
    }
    else if (impact == '1' && urgency == '2') {
        return 'High';
    }
    else if (impact == '2' && urgency == '2') {
        return 'Medium';
    }
    else if (impact == '3' && urgency == '2') {
        return 'Low';
    }
    else if (impact == '1' && urgency == '3') {
        return 'Medium';
    }
    else if (impact == '2' && urgency == '3') {
        return 'Low';
    }
    else if (impact == '3' && urgency == '3') {
        return 'Planned';
    }
    else if (impact == '0' && urgency == '0') {
        return "Select Urgency/Impact";
    }
    else if (impact != '0' && urgency == '0') {
        return "Select Urgency";
    }
    else if (impact == '0' && urgency != '0') {
        return "Select Impact";
    }
    else {
        return "Select Urgency/Impact";
    }

}



function checkResolvedState()
{







    if ($("#ticketStatusId").val() == "RES")
    {
        //$("#wugAttributesEdit").css("margin-top","0px");
        $(".resolutionDiv").show();
    }
    else {
        //$("#wugAttributesEdit").css("margin-top","70px");
        $(".resolutionDiv").hide();
    }

}


function validateEmail(emailText) {

    var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-zA-Z0-9]+(\-[a-zA-Z0-9]+)*(\.[a-zA-Z0-9]+(\-[a-zA-Z0-9]+)*)*\.[a-zA-Z]{2,4}$/;
    if (pattern.test(emailText)) {
        return true;
    } else {

        return false;
    }
}


function emailvalidation(emailvalue, emailinputfield)
{
    if (emailvalue == '') {
        return false;
    } else
    {
        var emailFilter2 = /^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:[,;][A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4})*$/i;
        var finalemails = "";
        var emailValue2 = emailvalue.trim();
        var emailadressess = new Array();
        if (emailValue2.indexOf(",") > -1)
        {
            emailadressess = emailValue2.split(",");
            for (var i = 0; i < emailadressess.length; i++)
            {
                if (emailadressess[i].trim() != "")
                    if (!(emailFilter2.test(emailadressess[i].trim()))) {

                        return false;
                    } else
                    {
                        if (i == emailadressess.length - 1)
                            finalemails += emailadressess[i].trim();
                        else
                            finalemails += emailadressess[i].trim() + ",";
                    }

            }
        } else if (emailValue2.indexOf(";") > -1)
        {
            emailadressess = emailValue2.split(";");
            for (var i = 0; i < emailadressess.length; i++)
            {
                if (emailadressess[i].trim() != "")
                    if (!(emailFilter2.test(emailadressess[i].trim()))) {

                        return false;
                    } else
                    {
                        if (i == emailadressess.length - 1)
                            finalemails += emailadressess[i].trim();
                        else
                            finalemails += emailadressess[i].trim() + ",";
                    }

            }

        } else if (emailValue2.indexOf(" ") > -1)
        {
            emailadressess = emailValue2.split(" ");
            for (var i = 0; i < emailadressess.length; i++)
            {

                if (emailadressess[i] != "")
                    if (!(emailFilter2.test(emailadressess[i].trim()))) {

                        return false;
                    } else
                    {
                        if (i == emailadressess.length - 1)
                            finalemails += emailadressess[i].trim();
                        else
                            finalemails += emailadressess[i].trim() + ",";
                    }

            }

        } else
        {
            if (!(emailFilter2.test(emailValue2))) {

                return false;
            } else
            {
                finalemails += emailValue2.trim();


            }

        }

        emailinputfield.value = finalemails;
    }

    return true;

}


function validateCreateTicket3() {

//	if ($("#stopTimerButton").css("display") == "block"){
//	if ($("#ticketStatusId").val() == "RES" || $("#ticketStatusId").val() == "Cancelled" || $("#ticketStatusId").val() == "Closed"){
//		$("#stopTimerButton").removeAttr("disabled");
//		errorMessage = "Stop timer<br/>";
//		errorOccured = true;
//
//	}

//	}

//	if (errorOccured) {
//
//
//		openErrorDialog("400",errorMessage);
//		return false;
//	}

    var errorMessage = '<b>Please enter information for the following:</b> <br/><br/>';
    var errorOccured = false;




    if ($(".siteDetail").css("display") == "block")
    {

        if ($("#siteDetailDTO\\.siteId").val() == "") {
            errorMessage += "Site,<br/>";
            errorOccured = true;
        }

    }



    // email validation.
    /*  if ($("#ticketStatusId").val() != "RES"
     && $("#ticketStatusId").val() != "Cancelled"
     && $("#ticketStatusId").val() != "Closed") {
     if ($("#emailAddress").val() == "") {
     errorMessage += " Caller Email,<br/>";
     errorOccured = true;
     } else {
     */
    if ($("#emailAddress").val().trim() != "" && !emailvalidation(document.getElementById("emailAddress").value,
            document.getElementById("emailAddress"))) {
        errorMessage += " Email should be in aa@bb.cc,xx@yy.zz format .<br/>";
        errorOccured = true;

    }

//            if (/\s/.test($("#emailAddress").val())) {
//
//             errorMessage += " Caller Email Contains White Spaces,<br/>";
//             errorOccured = true;
//
//             } else {
//
//             if (!validateEmail($("#emailAddress").val())) {
//             errorMessage += " Email should be in aa@bb.cc,xx@yy.zz format .<br/>";
//             errorOccured = true;
//             }
//             }
    /*}

     }*/

    // contact type validation.

    if ($("#ticketStatusId").val() != "RES" && $("#ticketStatusId").val() != "Cancelled" && $("#ticketStatusId").val() != "Closed") {
        if ($("#contactTypeId").val() == "0") {
            errorMessage += "Contact Type,<br/>";
            errorOccured = true;
        }
    }

    if ($("#billingStatus").val() == "select_bill") {
        errorMessage += "Billable,<br/>";
        errorOccured = true;
    }


    if ($("#urgency").val() == "0") {
        errorMessage += "Urgency,<br/>";

        errorOccured = true;
    }


    if ($("#impact").val() == "0") {
        errorMessage += "Impact,<br/>";

        errorOccured = true;
    }



    // if ($("#preferredArrivalTime").val() == "") {
    // if ($("#preferredArrivalWindowFrom").val() == ""
    // || $("#preferredArrivalWindowTo").val() == "") {
    // errorMessage += "Prefered Arrival Date Or Preferred window Start & End
    // Dates,<br/>";
    // errorOccured = true;
    // }
    // }

    if ($("#category").val() == "") {
        errorMessage += "Category,<br/>";
        errorOccured = true;

    }


    if ($("#configurationItem").val() == "") {
        errorMessage += "Configuration Item,<br/>";
        errorOccured = true;

    }

    if ($(".resolutionDiv").css('display') == 'block') {

        if ($("#resolutionId").val() == "0") {
            errorMessage += "Resolution,<br/>";
            errorOccured = true;

        }
        if ($("#pointOfFailureId").val() == "0") {
            errorMessage += "Point Of Failure,<br/>";
            errorOccured = true;

        }

        if ($("#rootCauseId").val() == "0") {
            errorMessage += "Root Cause,<br/>";
            errorOccured = true;


        }

    }



    if ($("#shortDescription").val() == "") {
        errorMessage += "Short Description,<br/>";
        errorOccured = true;

    }
    if ($("#problemDescription").val() == "") {
        errorMessage += "Problem Description,<br/>";
        errorOccured = true;

    }


    if ($("#ticketStatusId").val() == "select") {
        errorMessage += "Incident State,<br/>";
        errorOccured = true;

    }





//	if ($("#ticketStatusId").val() == "RES" && $('input[name="backUpCktStatus"]:checked').val() != "Up") {
//		errorMessage += "The ticket cannot be marked Resolved beacause the associated device is currentlyÂ tracked as down.<br/>";
//		errorOccured = true;
//
//	}


    // assignment group validation.

    if ($("#ticketStatusId").val() != "RES" && $("#ticketStatusId").val() != "Cancelled" && $("#ticketStatusId").val() != "Closed") {
        if ($("#assignmentGroupId").val() == "0") {
            errorMessage += "Assignment Group,<br/>";
            errorOccured = true;
        }
    }


    /*if ($.trim($("#manuallyCreationReason").val()) == "") {
     errorMessage += "Why is this ticket being created manually,<br/>";
     errorOccured = true;
     }*/




    /*if ($.trim($("#efforts").val()) == "") {
     errorMessage += "Efforts,<br/>";
     errorOccured = true;
     }*/

//	 var x = $("#emailAddress").val();
//		var atpos = x.indexOf("@");
//		var dotpos = x.lastIndexOf(".");
//		if (atpos<1 || dotpos<atpos+2 || dotpos+2 >= x.length)
//		  {
//			errorMessage +="Please Enter Valid Email ilke xxx@xxx.com,<br/>";
//		  return false;
//		  }





//	var effortValue = $.trim($("#efforts").val());
//
//	if (effortValue == '')
//		{
//		errorMessage += "Effort,<br/>";
//		errorOccured = true;
//
//		}


    // if (isNaN(effortValue)) {
    // openErrorDialog("300","Please Enter Valid Efforts");
    // return false;

    // }
    // if ((parseInt(effortValue) <= 0) || (parseInt(effortValue) > 1440)) {
    // openErrorDialog("300","The effort value should be less than 1440 minutes.");
    // return false;

    // }


    if ($("#ticketStatusId").val() != "RES") {
        if ($.trim($("#nextAction").val()) == "") {
            errorMessage += "Next Action,<br/>";
            errorOccured = true;
        }
    }

    if ($("#ticketStatusId").val() != "RES") {
        if (!$("input[name='createTicketNextActionUnit']").is(":checked")) {
            if ($("#createTicketNextActionDate").val() == "") {
                errorMessage += "Due Time,<br/>";
                errorOccured = true;
            }
        } else {
            if ($.trim($("#createTicketNextActionTime").val()) == "") {
                errorMessage += "Due Time,<br/>";
                errorOccured = true;
            }
        }
    }

    if ($.trim($("#workNotes").val()) == "") {
        errorMessage += "Work Notes,<br/>";
        errorOccured = true;
    }



    if (errorOccured) {


        openErrorDialog("400", errorMessage.substring(0, errorMessage.length - 6));
        return false;
    }

//	var effortValue = $.trim($("#efforts").val());
//
//	if (isNaN(effortValue)) {
//		openErrorDialog("300","Please Enter Valid Efforts");
//		return false;
//
//	}
//	if ((parseInt(effortValue) <= 0) || (parseInt(effortValue) > 1440)) {
//		openErrorDialog("300","The effort value should be less than 1440 minutes.");
//		return false;
//
//	}




    var invalidTimeFrameMessage = 'An invalid time frame or date was entered. <br/>Time frame in hours should less than 24.<br/> Time frame in minutes should be less 60.<br/>  The date is limited to 30 days in the future. <br/> Please correct your input.';

    if ($("#ticketStatusId").val() != "RES" && $("#ticketStatusId").val() != "Cancelled" && $("#ticketStatusId").val() != "Closed")

    {
        if ($("input[type='radio'][name='createTicketNextActionUnit']").is(
                ':checked')) {
            var nextActionTimeRadio = $(
                    "input[type='radio'][name='createTicketNextActionUnit']:checked")
                    .val();

            var nextActionTime = $("#createTicketNextActionTime").val();

            if (nextActionTimeRadio == "Hrs") {
                if (parseFloat(nextActionTime) < 0
                        || parseFloat(nextActionTime) > 24) {

                    openErrorDialog("500", invalidTimeFrameMessage);
                    return false;
                }

            } else {
                if (parseFloat(nextActionTime) < 0
                        || parseFloat(nextActionTime) > 60) {

                    openErrorDialog("500", invalidTimeFrameMessage);
                    return false;
                }
            }

            if (nextActionTime == "") {

                openErrorDialog("500", invalidTimeFrameMessage);
                return false;
            }

        } else {
            var createTicketNextActionDate = $("#createTicketNextActionDate").val();
            if (createTicketNextActionDate == "") {

                openErrorDialog("500", invalidTimeFrameMessage);
                return false;

            } else {
                var dueDate = new Date(createTicketNextActionDate);
                var currentDate = new Date();
                var dif = dueDate.getTime() - currentDate.getTime() + 120000;

                if (parseFloat(dif) < 0) {

                    openErrorDialog("500", invalidTimeFrameMessage);
                    return false;
                }
                var differenceInDays = parseInt((dueDate - currentDate)
                        / (1000 * 60 * 60 * 24));
                if (!isNaN(differenceInDays)) {
                    if (parseFloat(differenceInDays) >= 30) {

                        openErrorDialog("500", invalidTimeFrameMessage);
                        return false;
                    }
                }
            }

        }

    }




    return true;


}

function calculateCreateTicketEfforts() {

    var startDate = new Date($("#createTicketEffortStart").val());
    var stopDate = new Date($("#createTicketEffortStop").val());
    var dif = stopDate.getTime() - startDate.getTime();
    var differenceInMinutes = dif / (1000 * 60);
    if (!isNaN(differenceInMinutes)) {
        if (parseFloat(differenceInMinutes) >= 0) {
            $("#efforts").val(differenceInMinutes);
        }
    }

}


function getDateTime() {
    var now = new Date();
    var time = formatAMPM(now);
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    if (month.toString().length == 1) {
        var month = '0' + month;
    }
    if (day.toString().length == 1) {
        var day = '0' + day;
    }
    if (hour.toString().length == 1) {
        var hour = '0' + hour;
    }
    if (minute.toString().length == 1) {
        var minute = '0' + minute;
    }
    if (second.toString().length == 1) {
        var second = '0' + second;
    }

    var dateTime = month + '/' + day + '/' + year + ' ' + time;
    return dateTime;
}


function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ':' + seconds + ' ' + ampm;
    return strTime;
}


function closeHDTicket() {


    var ticketId = $("#ticketId").val(); // get the current ticket id first.
    //window.opener.location.href = window.opener.location.href; // refresh parent window before closing.
//	window.close();// close current window.

    var newWindow1 = window.open('', '', 'width=1210,height=800,scrollbars=yes');
    $.ajax({
        type: 'GET',
        data: {"ticketId": ticketId},
        url: pageContextPath + '/hdMainTable/closeHDTicket.html',
        async: false,
        success: function (data) {

            newWindow1.document.write(data);
            newWindow1.document.close();

            window.close();	// close previous window.



        }
    });

}

function reopenHDTicket() {


    var ticketId = $("#ticketId").val(); // get the current ticket id first.
    //window.opener.location.href = window.opener.location.href; // refresh parent window before closing.

    window.close(); // close current window.

    var newWindow2 = window.open('', '', 'width=1210,height=800,scrollbars=yes');


    $.ajax({
        type: 'GET',
        data: {"ticketId": ticketId},
        url: pageContextPath + '/hdMainTable/reOpenHdTicket.html',
        async: false,
        success: function (data) {


            newWindow2.document.write(data);
            newWindow2.document.close();



        }
    });



}


var maxCntCommon = 60; // ~15 Seconds
var maxAttempts = 3;
var animationSpeed = 'fast';
var lastMsg = "";
var statusError = false;
var unknownInternalError = 'An unknown internal server error occured. Please submit an MIS ticket with details of what you are currently doing.';

function createID() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 42; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

function getStatus(session_id, cnt, errorFlag, waitingForStatus, processFunc) {
    if (maxCntCommon <= cnt) {
        processFunc(session_id, 'error', 'Request timed out.');
        return;
    }

    $.ajax({
        url: "/common/ajax/status.php?session_id=" + session_id,
        type: "GET",
        dataType: 'json',
        timeout: 750,
        cache: false,
        async: true,
        success: function (data) {
            if (statusError === true) {
                return;
            }

            if (data.status == "0") {
                var msg = data.message.split("||");
                $('#ticketProcessStatus').html(msg[0]);
                $('.meter > span').animate({width: msg[1]}, animationSpeed);
                if (msg[1] != lastMsg) {
                    lastMsg = msg[1];
                    cnt = 0;
                }
                setTimeout(function () {
                    getStatus(session_id, (cnt + 1), errorFlag, waitingForStatus, processFunc);
                }, 250);
            } else if (data.status != "-1" || !waitingForStatus) {
                waitingForStatus = false;
                if (data.status == "3") {
                    var error_msg = data.message;
                    if (error_msg.length > 200) {
                        error_msg = error_msg.substring(0, 197);
                        error_msg += "...";
                    }

                    complete(session_id, 'error', error_msg);
                } else if (data.status == "1" || data.status == "2") {
                    if (data.message == 'Failed.') {
                        $('#' + session_id + ' .status').html("Failure!");
                    } else {
                        processFunc(session_id, 'success', data.message);
                    }
                }
            } else {
                setTimeout(function () {
                    getStatus(session_id, (cnt + 1), errorFlag, waitingForStatus, processFunc);
                }, 250);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            setTimeout(function () {
                getStatus(session_id, (cnt + 6), errorFlag, waitingForStatus, processFunc);
            }, 1500);
        }
    });
}

function complete(session_id, msgType, msg) {
    var msg = msg.split("||");

    if (msgType == "success") {
        $('#ticketProcessStatus').html(msg[0]);
        clearLockStatus($("#ticketId").val());
        setTimeout(function () {
            window.close();
        }, 1000);
    } else {
        $('#ticketProcessStatus').html(msg[0]);
        $('.meter').addClass('red');
        $("#confirmButton").show();
    }

    $('.meter > span').animate({width: msg[1]}, animationSpeed);
}

function handleSuccessfullAJAX(data) {
    if (data.status != "error" || statusError === true) {
        return;
    }
    $('#ticketProcessStatus').html(data.msg);
    $('.meter').addClass('red');
    $("#confirmButton").show();
    statusError = true;
}

function handleFailedAJAX(xhr, status, error, session_id) {
    if (error == 'parsererror') {
        error = unknownInternalError;
    } else {
        error = "System error occurred. Three attempts where made to complete the process. The following message was returned: " + xhr.responseText;
    }
    $('#ticketProcessStatus').html(error);
    $('.meter').addClass('red');
    $("#confirmButton").show();
    statusError = true;
}

function createHDTicket() {
    $("#efforts").val(getDateTime());
    if (!validateCreateTicket3()) {
        return false;
    } else {
        //var sideID = $("#siteDetailDTO\\.siteId").val()
        //form_fh += "&siteDetailDTO.siteId="+sideID;
        //var data = $(form_fh).serializeArray();

        var session_id = createID();

        $(".meter > span").each(function () {
            $(this).data("origWidth", $(this).width()).width(0).animate({width: $(this).data("origWidth")}, animationSpeed);
        });

        $('#popup_progress').show();

        setTimeout(function () {
            getStatus(session_id, 0, false, true, complete);
        }, 500);

        var form_fh = $("#hdTicketCreateForm2").serialize();
        form_fh += '&method=createHDTicket&session_id=' + session_id;

        $.ajax({
            type: 'GET',
            data: form_fh,
            url: '/helpdesk/index.php',
            async: true,
            dataType: "json",
            success: function (data) {
                handleSuccessfullAJAX(data);
            }, error: function (xhr, status, error) {
                handleFailedAJAX(xhr, status, error, session_id);
            }
        });
    }
}

function updateHDTicket() {
    if (!validateCreateTicket3()) {
        return false;
    } else {
        var method = 'updateHDTicket';

        if (document.getElementById("ticketStatusId").value == "Closed" || document.getElementById("ticketStatusId").value == "RES") {
            method = 'updateResolveTicket';
        }

        var session_id = createID();
        $(".meter > span").each(function () {
            $(this).data("origWidth", $(this).width()).width(0).animate({width: $(this).data("origWidth")}, animationSpeed);
        });

        $('#popup_progress').show();
        setTimeout(function () {
            getStatus(session_id, 0, false, true, complete);
        }, 500);

        $('#Screen2 :input').attr('disabled', false);
        var form_fh = $("#hdTicketCreateForm2").serialize();
        $('#Screen2 :input').attr('disabled', true);

        form_fh += '&method=' + method + '&session_id=' + session_id;

        $.ajax({
            type: 'GET',
            data: form_fh,
            url: '/helpdesk/index.php',
            async: true,
            dataType: "json",
            success: function (data) {
                handleSuccessfullAJAX(data);
            }, error: function (xhr, status, error) {
                handleFailedAJAX(xhr, status, error, session_id);
            }
        });
    }
}

function updateHDTicketOnClose() {
    if (!validateCreateTicket3()) {
        return false;
    } else {
        var session_id = createID();
        $(".meter > span").each(function () {
            $(this).data("origWidth", $(this).width()).width(0).animate({width: $(this).data("origWidth")}, animationSpeed);
        });

        $('#popup_progress').show();
        setTimeout(function () {
            getStatus(session_id, 0, false, true, complete);
        }, 500);

        $('#Screen2 :input').attr('disabled', false);
        var form_fh = $("#hdTicketCreateForm2").serialize();
        $('#Screen2 :input').attr('disabled', true);

        form_fh += '&method=updateResolveTicket&session_id=' + session_id;

        $.ajax({
            type: 'GET',
            data: form_fh,
            url: '/helpdesk/index.php',
            async: true,
            dataType: "json",
            success: function (data) {
                handleSuccessfullAJAX(data);
            }, error: function (xhr, status, error) {
                handleFailedAJAX(xhr, status, error, session_id);
            }
        });
    }
}

function updateHDTicketonReOpen() {
    if (!validateCreateTicket3()) {
        return false;
    } else {
        var session_id = createID();
        $(".meter > span").each(function () {
            $(this).data("origWidth", $(this).width()).width(0).animate({width: $(this).data("origWidth")}, animationSpeed);
        });

        $('#popup_progress').show();
        setTimeout(function () {
            getStatus(session_id, 0, false, true, complete);
        }, 500);

        $('#Screen2 :input').attr('disabled', false);
        var form_fh = $("#hdTicketCreateForm2").serialize();
        $('#Screen2 :input').attr('disabled', true);

        form_fh += '&method=updateHDTicket&session_id=' + session_id;

        $.ajax({
            type: 'GET',
            data: form_fh,
            url: '/helpdesk/index.php',
            async: true,
            dataType: "json",
            success: function (data) {
                handleSuccessfullAJAX(data);
            }, error: function (xhr, status, error) {
                handleFailedAJAX(xhr, status, error, session_id);
            }
        });
    }
}


function enterNewSite(msaId) {



    ticketPopUp = true;

//	$('#popUpScreen').block({
//		message : $('#popUpScreen2'),
//		css : {
//			width : '580px',
//			height : 'auto',
//			position : 'absolute',
//			color : 'black'
//		},
//		overlayCSS : {
//			backgroundColor : '#f0f0f0',
//			opacity : 0.3
//		},
//		centerX : true,
//		centerY : true,
//		fadeIn : 0,
//		fadeOut : 0
//	});
//	$('#popUpScreen2').parent('.blockUI.blockMsg').offset({
//		top : 150,
//		left : 210
//	});
//	$("#pageTitle2").html("Enter New Site ");

    var url = pageContextPath + '/hdMainTable/enterNewSite.html?msaId=' + msaId;

    $.ajax({
        type: 'GET',
        url: pageContextPath + '/hdMainTable/enterNewSite.html?msaId=' + msaId,
        success: function (obj) {
            //$("#popupBody2").html(obj);

            var enterNewSiteWindow = window.open('', '', 'width=550,height=500,scrollbars=yes');
            enterNewSiteWindow.document.write(obj);
            enterNewSiteWindow.document.close();

        }
    });

}



function openTicketSummaryResolved(ticketId) {

    currentTicketId = ticketId;
//	$('#ticketPopUpScreen').block({
//		message : $('#popUpScreen2'),
//		css : {
//			width : '1150',
//			height : 'auto',
//			position : 'absolute',
//			color : 'black'
//
//		},
//		overlayCSS : {
//			backgroundColor : '#f0f0f0',
//			opacity : 0.3
//		},
//		centerX : true,
//		centerY : true,
//		fadeIn : 0,
//		fadeOut : 0
//	});

    //$("#pageTitle2").text(
    //	"[Customer Name, Appendix, Site Number, Ticket Type, Ilex #]");
    $.ajax({
        type: 'POST',
        data: {"ticketId": ticketId},
        url: pageContextPath + '/hdMainTable/checkLockStatus.html',
        async: false,
        success: function (data) {

            if (data == "success") {

                //$("div.tableWrapper table tr[RowID='" + ticketId + "']").find('span').removeClass('greenIcon');
                //$("div.tableWrapper table tr[RowID='" + ticketId + "']").find('span').addClass('redIcon');

                var newWindow1 = window.open('', '', 'width=1210,height=800,scrollbars=yes');


                $.ajax({
                    type: 'GET',
                    data: {"ticketId": ticketId},
                    url: pageContextPath + '/hdMainTable/hdTicketSummaryResolved.html',
                    async: false,
                    success: function (data) {


                        newWindow1.document.write(data);
                        newWindow1.document.close();



                    }
                });


            } else {
                alert(data);

            }
        }
    });





//	$("#popupBody2").load(
//			pageContextPath + "/hdMainTable/hdTicketSummary.html?ticketId="
//			+ ticketId);
//
//	var ODTop=$("#hdWipDivOverDue").offset().top;
//	var block1Top=$("#ticketPopUpScreen").offset().top;
//	var ODLeft=$("#hdWipDivOverDue").offset().left - 110;
//	var block1Left=$("#ticketPopUpScreen").offset().left;
//	var currentTop=(block1Top-ODTop)+20;
//	var currentLeft=(ODLeft-block1Left);
//
//	$('#popUpScreen2').parent('.blockUI.blockMsg').css('left', currentLeft);
//	$('#popUpScreen2').parent('.blockUI.blockMsg').css('top', -currentTop);

}

function clearLockStatus(ticketId) {

    $.ajax({
        type: 'POST',
        data: {"ticketId": ticketId},
        url: pageContextPath + '/hdMainTable/clearLockStatus.html',
        async: false,
        success: function (data) {
            if (data = "success") {

                //$("div.tableWrapper table tr[RowID='" + ticketId + "']").find('span').removeClass('redIcon');
                //$("div.tableWrapper table tr[RowID='" + ticketId + "']").find('span').addClass('redIcon');

            }
        }

    });

}



function openTicketSummaryWithUser(ticketId, userId) {
    currentTicketId = ticketId;
    //alert(ticketId+" ----"+userId);
    var session_id = createID();

    $.ajax({
        type: 'GET',
        data: {'method': 'manageTicketLock', 'userId': userId, 'session_id': session_id, 'ticketId': ticketId, 'lockStatus': 1},
        url: '/helpdesk/index.php',
        async: false,
        dataType: "json",
        success: function (data) {
            if (data.status == "success") {
                var newWindow1 = window.open('', '', 'width=1210,height=800,scrollbars=yes');
                $.ajax({
                    type: 'GET',
                    data: {"ticketId": ticketId},
                    url: pageContextPath + '/hdMainTable/hdTicketSummary.html',
                    async: false,
                    success: function (data) {
                        newWindow1.document.write(data);
                        newWindow1.document.close();
                    }
                });
            } else {
                alert(data.msg);
            }
        }
    });
}



function openTicketSummary(ticketId) {

    currentTicketId = ticketId;
//	$('#ticketPopUpScreen').block({
//		message : $('#popUpScreen2'),
//		css : {
//			width : '1150',
//			height : 'auto',
//			position : 'absolute',
//			color : 'black'
//
//		},
//		overlayCSS : {
//			backgroundColor : '#f0f0f0',
//			opacity : 0.3
//		},
//		centerX : true,
//		centerY : true,
//		fadeIn : 0,
//		fadeOut : 0
//	});


    // checkLockStatus.html
    //$("#pageTitle2").text(
    //	"[Customer Name, Appendix, Site Number, Ticket Type, Ilex #]");



    $.ajax({
        type: 'POST',
        data: {"ticketId": ticketId},
        url: pageContextPath + '/hdMainTable/checkLockStatus.html',
        async: false,
        success: function (data) {

            if (data == "success") {

                //$("div.tableWrapper table tr[RowID='" + ticketId + "']").find('span').removeClass('greenIcon');
                //$("div.tableWrapper table tr[RowID='" + ticketId + "']").find('span').addClass('redIcon');
                var newWindow1 = window.open('', '', 'width=1210,height=800,scrollbars=yes');
                $.ajax({
                    type: 'GET',
                    data: {"ticketId": ticketId},
                    url: pageContextPath + '/hdMainTable/hdTicketSummary.html',
                    async: false,
                    success: function (data) {


                        newWindow1.document.write(data);
                        newWindow1.document.close();



                    }
                });
            } else {
                alert(data);

            }
        }
    });








//	$("#popupBody2").load(
//			pageContextPath + "/hdMainTable/hdTicketSummary.html?ticketId="
//			+ ticketId);
//
//	var ODTop=$("#hdWipDivOverDue").offset().top;
//	var block1Top=$("#ticketPopUpScreen").offset().top;
//	var ODLeft=$("#hdWipDivOverDue").offset().left - 110;
//	var block1Left=$("#ticketPopUpScreen").offset().left;
//	var currentTop=(block1Top-ODTop)+20;
//	var currentLeft=(ODLeft-block1Left);
//
//	$('#popUpScreen2').parent('.blockUI.blockMsg').css('left', currentLeft);
//	$('#popUpScreen2').parent('.blockUI.blockMsg').css('top', -currentTop);

}

function openAckTicketSummary(ticketId) {

    currentTicketId = ticketId;
    $('#ticketPopUpScreen').block({
        message: $('#popUpScreen2'),
        css: {
            width: '1150',
            height: 'auto',
            position: 'absolute',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });

    $("#pageTitle2").text(
            "[Customer Name, Appendix, Site Number, Ticket Type, Ilex #]");






    $("#popupBody2").load(
            pageContextPath + "/hdMainTable/hdAckTicketSummary.html?ticketId="
            + ticketId);

    var ODTop = $("#hdWipDivOverDue").offset().top;
    var block1Top = $("#ticketPopUpScreen").offset().top;
    var ODLeft = $("#hdWipDivOverDue").offset().left - 110;
    var block1Left = $("#ticketPopUpScreen").offset().left;
    var currentTop = (block1Top - ODTop) + 20;
    var currentLeft = (ODLeft - block1Left);

    $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', currentLeft);
    $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', -currentTop);

}

function openWorkNotesScreen(ticketId) {

    $('#ticketPopUpScreen').block({
        message: $('#popUpScreen2'),
        css: {
            width: '450',
            height: 'auto',
            position: 'absolute',
            color: 'black'

        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });

    $("#pageTitle2").text(
            "Enter Work Notes");
    $("#popupBody2").load(
            pageContextPath + "/hdMainTable/workNotes.html?ticketId="
            + ticketId);

    $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 100);
    $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 100);

}


function openEffortScreen(ticketId) {

    $('#ticketPopUpScreen').block({
        message: $('#popUpScreen2'),
        css: {
            width: '450',
            height: 'auto',
            position: 'absolute',
            color: 'black'

        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });

    $("#pageTitle2").text(
            "Record Effort");
    $("#popupBody2").load(
            pageContextPath + "/hdMainTable/effortsSave.html?ticketId="
            + ticketId);

    $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 100);
    $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 100);

}

function openNextActionScreen(ticketId, nextActionId) {

    $('#ticketPopUpScreen').block({
        message: $('#popUpScreen2'),
        css: {
            width: '450',
            height: 'auto',
            position: 'absolute',
            color: 'black'

        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });

    $("#pageTitle2").text(
            "Update Next Action");

    $.ajax({
        type: 'GET',
        data: {
            "ticketId": ticketId,
            "nextActionId": nextActionId
        },
        url: pageContextPath + '/hdMainTable/nextActionSave.html',
        async: false,
        success: function (data) {
            $("#popupBody2").html(data);
        }

    });

    $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 100);
    $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 100);

}








function openTicketSummary1(ticketId) {
    //	ticketPopUp=false;
    //	$.blockUI({
    //		message : $('#popUpScreen2'),
    //		css : {
    //			width : '1150',
    //			height : 'auto',
    //			position : 'absolute',
//			color : 'black'
// 	},
    //		overlayCSS : {
    //			backgroundColor : '#f0f0f0',
//			opacity : 0.3
//		},
    //		centerX : true,
    // 	centerY : true,
//		fadeIn : 0,
    //		fadeOut : 0
//	});
//	$("#pageTitle2").text(
    //			"[Customer Name, Appendix, Site Number, Ticket Type, Ilex #]");
    //	$("#popupBody2").load(
//			pageContextPath + "/hdMainTable/hdTicketSummary.html?ticketId="
//					+ ticketId);
//
//	$('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 300);
//	$('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 100);

    var newWindow1 = window.open('', '', 'width=1210,height=800,scrollbars=yes');


    $.ajax({
        type: 'GET',
        data: {"ticketId": ticketId},
        url: pageContextPath + '/hdMainTable/hdTicketSummary.html',
        async: false,
        success: function (data) {


            newWindow1.document.write(data);
            newWindow1.document.close();



        }
    });
}

function openResolveTicket(ticketId) {
    $('#ticketPopUpScreen').block({
        message: $('#popUpScreen2'),
        css: {
            width: '600',
            height: 'auto',
            position: 'absolute',
            left: '30%',
            top: '180px',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });

    $("#pageTitle2").text("Resolve Ticket");
    $("#popupBody2").load(
            pageContextPath + "/hdMainTable/hdResolveTicket.html?ticketId="
            + ticketId);

    $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 20);
    $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 20);

}

function openResolveTicket1(ticketId) {
    ticketPopUp = false;
    $.blockUI({
        message: $('#popUpScreen2'),
        css: {
            width: '600',
            height: 'auto',
            position: 'absolute',
            left: '30%',
            top: '180px',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    $("#pageTitle2").text("Resolve Ticket");
    $("#popupBody2").load(
            pageContextPath + "/hdMainTable/hdResolveTicket.html?ticketId="
            + ticketId);

    $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 500);
    $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 100);
}

function ackTicketStep1(ackCubeType, obj) {
    var ticketId = $(obj).parent().parent().attr("ticketid");

    $.ajax({
        type: 'POST',
        data: {
            "ticketId": ticketId,
            "ackCubeType": ackCubeType
        },
        url: pageContextPath + '/hdMainTable/checkStatus.html',
        success: function (data) {
            if (data == "success") {
                ticketPopUp = false;
                $.blockUI({
                    message: $('#popUpScreen2'),
                    css: {
                        width: '1100px',
                        /*height: '620',*/
                        position: 'absolute',
                        color: 'black'
                    },
                    overlayCSS: {
                        backgroundColor: '#f0f0f0',
                        opacity: 0.3
                    },
                    centerX: true,
                    centerY: true,
                    fadeIn: 0,
                    fadeOut: 0
                });

                if ("hdPendingAckCust" == ackCubeType) {
                    $("#pageTitle2").text("Acknowledge Customer Ticket");
                }
                else if ("hdPendingAckAlert" == ackCubeType) {
                    $("#pageTitle2").text("Acknowledge Alert Ticket");
                }

                else if ("hdPendingAckMonitor" == ackCubeType) {
                    $("#pageTitle2").text("Acknowledge Monitoring Ticket");
                }
                else if ("hdPendingAckMonitorTier2" == ackCubeType) {
                    $("#pageTitle2").text("Acknowledge 2nd Tier Monitoring Ticket");
                } else if ("hdAddedToMonitoring" == ackCubeType) {
                    $("#pageTitle2").text("Added to Monitoring Ticket");
                }

                $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 200);
                $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 120);

                var ticketId = $(obj).parent().parent().attr("ticketid");
                $.ajax({
                    type: 'POST',
                    url: pageContextPath + '/hdMainTable/ackTicketStep1.html',
                    data: {
                        "ackCubeType": ackCubeType,
                        "ticketId": ticketId,
                    },
                    async: false,
                    success: function (data) {
                        $("#popupBody2").html(data);
                    }

                });
            }
            else {
                alert(data);
            }
        }
    });


}

function ackTicketStepFromTicketActions(ackCubeType, ticketId) {
    $.ajax({
        type: 'POST',
        data: {
            "ticketId": ticketId,
            "ackCubeType": ackCubeType
        },
        url: pageContextPath + '/hdMainTable/checkStatus.html',
        success: function (data) {
            if (data == "success") {
                $('#ticketPopUpScreen').block({
                    message: $('#popUpScreen2'),
                    css: {
                        width: '1100',
                        /* height: '620',*/
                        position: 'absolute',
                        color: 'black'

                    },
                    overlayCSS: {
                        backgroundColor: '#f0f0f0',
                        opacity: 0.3
                    },
                    centerX: true,
                    centerY: true,
                    fadeIn: 0,
                    fadeOut: 0
                });
                if ("hdPendingAckCust" == ackCubeType) {
                    $("#pageTitle2").text("Acknowledge Customer Ticket");
                }
                if ("hdPendingAckAlert" == ackCubeType) {
                    $("#pageTitle2").text("Acknowledge Alert Ticket");
                }

                if ("hdPendingAckMonitor" == ackCubeType) {
                    $("#pageTitle2").text("Acknowledge Monitoring Ticket");
                }
                else if ("hdPendingAckMonitorTier2" == ackCubeType) {
                    $("#pageTitle2").text("Acknowledge 2nd Tier Monitoring Ticket");
                } else if ("hdAddedToMonitoring" == ackCubeType) {
                    $("#pageTitle2").text("Added to Monitoring Ticket");
                }

//                var ODTop = $("#hdWipDivOverDue").offset().top;
//                var block1Top = $("#ticketPopUpScreen").offset().top;
//                var ODLeft = $("#hdWipDivOverDue").offset().left - 110;
//                var block1Left = $("#ticketPopUpScreen").offset().left;
//                var currentTop = (block1Top - ODTop) + 20;
//                var currentLeft = (ODLeft - block1Left);

                $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 10);
                $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', -10);

                $.ajax({
                    type: 'POST',
                    url: pageContextPath + '/hdMainTable/ackTicketStep1.html',
                    data: {
                        "ackCubeType": ackCubeType,
                        "ticketId": ticketId,
                    },
                    async: false,
                    success: function (data) {
                        $("#popupBody2").html(data);
                    }

                });
            }
            else {
                alert(data);
            }
        }
    });


}

function validateAckTicketStep1() {
    var errorMessage = '<b>Please enter information for the following: </b><br/><br/>';
    var errorOccured = false;

    if ($("#category").val() == "") {
        errorMessage += "Category,<br/>";
        errorOccured = true;
    }
    if ($("#configurationItem").val() == "") {
        errorMessage += "Configuration Item,<br/>";
        errorOccured = true;

    }
    if ($("#shortDescription").val() == "") {
        errorMessage += "Short Description,<br/>";
        errorOccured = true;

    }
    if ($("#problemDescription").val() == "") {
        errorMessage += "Problem Description,<br/>";
        errorOccured = true;

    }
    if ($("input[name='backUpCktStatus']").is(":visible")) {
        if (!$("input[name='backUpCktStatus']").is(":checked")) {
            errorMessage += "Backup Circuit Status,<br/>";
            errorOccured = true;

        }
    }
    if (!$("input[name='impact']").is(":checked")) {
        errorMessage += "Impact,<br/>";
        errorOccured = true;
    }
    if (!$("input[name='urgency']").is(":checked")) {
        errorMessage += "Urgency,<br/>";
        errorOccured = true;
    }

//	validating efforts
    var invalidEffortMessage = 'An invalid effort value was entered.<br/> The effort value should be less than 1440 minutes.<br/> In general, you should be entering these values incrementally to avoid this error.<br/>  Please correct your input or consult your supervisor on how to resolve.';
    var effortValue = $("#efforts").val();
    if (effortValue == "") {
        openErrorDialog_old("500", invalidEffortMessage);
        return false;
    }
    if (isNaN(effortValue)) {
        openErrorDialog_old("500", invalidEffortMessage);
        return false;

    }
    if ((parseInt(effortValue) <= 0) || (parseInt(effortValue) > 1440)) {
        openErrorDialog_old("500", invalidEffortMessage);
        return false;

    }



//	validating next action
    var invalidTimeFrameMessage = 'An invalid time frame or date was entered. <br/>Time frame in hours should less than 24.<br/> Time frame in minutes should be less 60.<br/>  The date is limited to 30 days in the future. <br/> Please correct your input.';
    if ($("input[type='radio'][name='createTicketNextActionUnit']").is(':checked')) {
        var nextActionTimeRadio = $(
                "input[type='radio'][name='createTicketNextActionUnit']:checked").val();

        var nextActionTime = $("#createTicketNextActionTime").val();

//			alert(nextActionTime);
        if (nextActionTimeRadio == "Hrs") {
            if (parseFloat(nextActionTime) < 0
                    || parseFloat(nextActionTime) > 24) {
                openErrorDialog_old("500", invalidTimeFrameMessage);
                return false;
            }

        } else {
            if (parseFloat(nextActionTime) < 0
                    || parseFloat(nextActionTime) > 60) {

                openErrorDialog_old("500", invalidTimeFrameMessage);
                return false;
            }

        }

        if (nextActionTime == "") {
            openErrorDialog_old("500", invalidTimeFrameMessage);
            ;
            return false;
        }
    } else {
        var wipNextActionDueDate = $("#createTicketNextActionDate").val();
        if (wipNextActionDueDate == "") {
            openErrorDialog_old("500", invalidTimeFrameMessage);
            return false;

        } else {
            var dueDate = new Date(wipNextActionDueDate);
            var currentDate = new Date();
            var dif = dueDate.getTime() - currentDate.getTime() + 80000;
            if (parseFloat(dif) < 0) {
                openErrorDialog_old("500", invalidTimeFrameMessage);
                return false;
            }
            var differenceInDays = parseInt((dueDate - currentDate)
                    / (1000 * 60 * 60 * 24));
            if (!isNaN(differenceInDays)) {
                if (parseFloat(differenceInDays) >= 30) {
                    openErrorDialog_old("500", invalidTimeFrameMessage);
                    return false;
                }
            }
        }

    }



    if ($.trim($("#nextAction").val()) == "") {
        errorMessage += "Next Action,<br/>";
        errorOccured = true;
    }

    if ($.trim($("#workNotes").val()) == "") {
        errorMessage += "Work Notes,<br/>";
        errorOccured = true;
    }
    if (errorOccured) {
        openErrorDialog_old("400", errorMessage.substring(0, errorMessage.length - 6));
        return false;
    }
    return true;
}

function completeAck(session_id, msgType, msg) {
    var msg = msg.split("||");

    if (msgType == "success") {
        setTimeout(function () {
            var ackCubeType = $("#ackTicketForm1").find('#ackCubeType').val();
            closeWindow2();
            if (ticketPopUp) {
                $.unblockUI();
                $("#ticketPopUpBody").empty().html(
                        '<img src=""' + pageContextPath
                        + '/resources/images/waiting_tree.gif" />');
                $("#ticketPageTitle").innerHTML = '';
            }
//Refresh Pending cubes and WIP cube
            if ("hdPendingAckCust" == ackCubeType) {
                hdTables.hdpendingackcustTable.fnReloadAjax();
                hdTables.hdpendingacksecurityTable.fnReloadAjax();
            } else if ("hdPendingAckAlert" == ackCubeType) {
                hdTables.hdpendingackalertTable.fnReloadAjax();
            } else if ("hdPendingAckMonitor" == ackCubeType) {
                hdTables.hdpendingackmonitorTable.fnReloadAjax();
                setPendingAckMonitoringFooterDetail();
            }
            hdTables.hdWipTable.fnReloadAjax();
        }, 1000);
    } else {
        $('#ticketProcessStatus').html(msg[0]);
        $('.meter').addClass('red');
        $("#confirmButton").show();
    }

    $('.meter > span').animate({width: msg[1]}, animationSpeed);
}

function ackTicket() {
    if (!validateAckTicketStep1()) {
        return false;
    } else {

        var session_id = createID();
        $(".meter > span").each(function () {
            $(this).data("origWidth", $(this).width()).width(0).animate({width: $(this).data("origWidth")}, animationSpeed);
        });

        $('#popup_progress').show();
        setTimeout(function () {
            getStatus(session_id, 0, false, true, completeAck);
        }, 500);

        $("#confirmButtonAck").click(function (e) {
            e.preventDefault();
            closeWindow2();
            if (ticketPopUp) {
                $.unblockUI();
                $("#ticketPopUpBody").empty().html(
                        '<img src=""' + pageContextPath
                        + '/resources/images/waiting_tree.gif" />');
                $("#ticketPageTitle").innerHTML = '';
            }
        });

        var form_fh = $("#ackTicketForm1").serialize();
        form_fh += '&method=ackTicket&session_id=' + session_id;

        $.ajax({
            type: 'GET',
            data: form_fh,
            url: '/helpdesk/index.php',
            async: true,
            dataType: "json",
            success: function (data) {
                handleSuccessfullAJAX(data);
            }, error: function (xhr, status, error) {
                handleFailedAJAX(xhr, status, error, session_id);
            }
        });
    }
}

function checkIsTicketAvaiableForDrop(ticket, obj) {
    var ticketId = $(ticket).parent().find("div.helperDiv").attr("ticketId");
    var isSuccess = true;
    $
            .ajax({
                type: 'POST',
                data: {
                    'ticketId': ticketId
                },
                url: pageContextPath
                        + '/hdActiveTicket/isTicketAvailableForDrop.html',
                async: false,
                success: function (data) {
                    if (data == "success") {
                        isSuccess = true;
                    } else {
                        isSuccess = false;
                    }
                }
            });
    if (isSuccess) {
        return true;
    } else {
        return false;
    }
}

function saveDroppedTicketState(ticketId, divId) {
    $.ajax({
        type: 'POST',
        data: {
            'ticketId': ticketId,
            'divId': divId
        },
        url: pageContextPath + '/hdActiveTicket/saveDroppedTicketState.html',
        async: false,
        success: function (data) {

        }
    });
}

function saveActiveTicket(ticketId) {
    $.ajax({
        type: 'POST',
        data: {
            'ticketId': ticketId
        },
        url: pageContextPath + '/hdActiveTicket/saveActiveTicket.html',
        async: false,
        success: function (data) {

        }
    });
}

function openSelectedActionForm(obj) {
    var ticketId = $("#headerIlexTicketId").text();
    var selectedAction = $("#additionalActions").val();
    if (selectedAction == "") {
        return false;
    }
    switch (selectedAction) {
        case "View Ticket":
            openTicketSummary1(ticketId);
            break;
        case "Resolve Ticket":
            openResolveTicket1(ticketId);
            break;
        case "View NetMedX Tab":
            redirectToNetMedxBoard(ticketId);
            break;
        default:
            break;
    }
    $("#additionalActions").val("");
}

function fillPOCDetails(obj, mmId) {
    var selectedOption = $(obj).val();
    $(obj)
            .find("option")
            .each(
                    function () {
                        if (selectedOption == $(this).val()) {

                            $("#contactName").val($(this).attr('contactName'));
                            $("#phoneNumber").val($(this).attr('phoneNumber'));
                            //$("#emailAddress").val($(this).attr('emailAddress'));

                            var textInner = $(this).attr('siteNumber');
                            textInner = textInner.replace("#", "-,-,-");
                            var str = "'/Ilex/masterSiteAction.do?siteName=" + textInner + "&mmId=" + mmId + "', '_blank', 'width=1100,height=700,scrollbars=yes'";
                            $("#siteNumber").html('<a href="javascript:void(0);" onclick="window.open(' + str + ');" >' + $(this).attr('siteNumber') + '</a>');
                            $("#siteAddress").text($(this).attr('siteAddress'));
                            var address1 = $(this).attr('siteCity') + ','
                                    + $(this).attr('siteState') + ',';
                            if ($(this).attr('siteCountry') != 'US') {
                                address1 = address1
                                        + $(this).attr('siteCountry') + ',';
                            }
                            address1 = address1 + $(this).attr('siteZipCode');
                            $("#siteAddress1").text(address1);
                        }
                        if (selectedOption == "") {
                            $("#siteNumber").text("[SiteNumber]");
                            $("#siteAddress").text("[Address]");
                            $("#siteAddress1").text(
                                    "[City,ST,[Country for Non-US]Zip code]");
                        }
                    });
}

function fillPOCDetailsOfParent(obj) {
    var selectedOption = $(obj).val();
    $(obj)
            .find("option")
            .each(
                    function () {
                        if (selectedOption == $(this).val()) {
                            $("#contactName", window.opener.document).val($(this).attr('contactName'));
                            $("#phoneNumber", window.opener.document).val($(this).attr('phoneNumber'));
                            $("#emailAddress", window.opener.document)
                                    .val($(this).attr('emailAddress'));
                            $("#siteNumber", window.opener.document).text($(this).attr('siteNumber'));
                            $("#siteAddress", window.opener.document).text($(this).attr('siteAddress'));
                            var address1 = $(this).attr('siteCity') + ','
                                    + $(this).attr('siteState') + ',';
                            if ($(this).attr('siteCountry') != 'US') {
                                address1 = address1
                                        + $(this).attr('siteCountry') + ',';
                            }
                            address1 = address1 + $(this).attr('siteZipCode');
                            $("#siteAddress1", window.opener.document).text(address1);
                        }
                        if (selectedOption == "") {
                            $("#siteNumber", window.opener.document).text("[SiteNumber]");
                            $("#siteAddress", window.opener.document).text("[Address]");
                            $("#siteAddress1", window.opener.document).text(
                                    "[City,ST,[Country for Non-US]Zip code]");
                        }
                    });
}

function openAlertState() {
    $('#popUpScreen').block({
        message: $('#popUpScreen2'),
        css: {
            width: '400px',
            height: 'auto',
            position: 'absolute',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    $('#popUpScreen2').parent('.blockUI.blockMsg').offset({
        top: 150,
        left: 210
    });
    $("#pageTitle2").html("Alert State");
    $("#popupBody2").load(
            pageContextPath + "/hdMainTable/alertState.html?ticketId=" + 21);

}
function openTicketDetails() {
    var ticketId = $("#hdTicketDTO").find("#ticketId").val();
    $('#popUpScreen2').block({
        message: $('#popUpScreen3'),
        css: {
            width: '580px',
            height: 'auto',
            position: 'absolute',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    var blockTop = $('#popUpScreen2').height() / 2 - 200;
    var blockLeft = $('#popUpScreen2').width() / 2 - 50;
    $('#popUpScreen3').parent('.blockUI.blockMsg').offset({
        top: blockTop,
        left: blockLeft
    });
    $("#pageTitle3").html("Enter Ticket Details ");

    $.ajax({
        type: 'GET',
        url: pageContextPath + '/hdMainTable/editTicketDetailsPage.html?ticketId=' + ticketId,
        success: function (obj) {
            $("#popupBody3").html(obj);
        }
    });
}

function editTicketDetails() {
    if (!validateTicketDetails()) {
        return false;
    } else {
        var form_fh = $("#hdTicketDTOEdit");
        var data = $(form_fh).serializeArray();
        var ticketId = $("#hdTicketDTOEdit").find("#ticketId").val();

        $.ajax({
            type: 'post',
            url: pageContextPath + '/hdMainTable/editTicketDetails.html',
            data: data,
            success: function (data) {
                if (data == "success") {
                    closeWindow3();
                    $("#popupBody2").load(
                            pageContextPath + "/hdMainTable/hdTicketSummary.html?ticketId="
                            + ticketId);
                } else {
                    $("#popupBody3").html(
                            '<div class="MSERR0001 WASTD0003">' + data
                            + '</div>');
                    $("#pageTitle3").text("Error");
                }

            }
        });
    }
}

function validateTicketDetails() {
    var errorMessage = '<b>Please enter information for the following:</b> <br/><br/>';
    var errorOccured = false;

    if ($("#hdTicketDTOEdit").find("#category").val() == "") {
        errorMessage += "Category,<br/>";
        errorOccured = true;
    }

    if ($("#hdTicketDTOEdit").find("#configurationItem").val() == "") {
        errorMessage += "Configuration Item,<br/>";
        errorOccured = true;
    }

    if ($("#hdTicketDTOEdit").find("#shortDescription").val() == "") {
        errorMessage += "Short Description,<br/>";
        errorOccured = true;
    }

    if ($("#hdTicketDTOEdit").find("#problemDescription").val() == "") {
        errorMessage += "Problem Description,<br/>";
        errorOccured = true;
    }
    if (errorOccured) {
        openErrorDialog("400", errorMessage.substring(0, errorMessage.length - 6));
        return false;
    } else {
        return true;
    }
}

function openEditPrioritySection() {
    var ticketId = $("#hdTicketDTO").find("#ticketId").val();
    $('#popUpScreen2').block({
        message: $('#popUpScreen3'),
        css: {
            width: '500px',
            height: 'auto',
            position: 'absolute',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    var blockTop = $('#popUpScreen2').height() / 2 - 100;
    var blockLeft = $('#popUpScreen2').width() / 2;
    $('#popUpScreen3').parent('.blockUI.blockMsg').offset({
        top: blockTop,
        left: blockLeft
    });
    $("#pageTitle3").html("Edit Impact/Urgency");
    $.ajax({
        type: 'GET',
        url: pageContextPath + '/hdMainTable/editPrioritySecPage.html?ticketId=' + ticketId,
        success: function (obj) {
            $("#popupBody3").html(obj);
        }
    });
}

function editPrioritySection() {
    var errorMessage = '<b>Please enter information for the following:</b> <br/><br/>';
    var errorOccured = false;

    if (!$("#hdTicketDTOEdit").find("input[type='radio'][name='impact']").is(':checked')) {
        errorMessage += "Impact,<br/>";
        errorOccured = true;
    }
    if (!$("#hdTicketDTOEdit").find("input[type='radio'][name='urgency']").is(':checked')) {
        errorMessage += "Urgency,<br/>";
        errorOccured = true;
    }
    var workNotes = $.trim($("#workNotes").val());
    if (workNotes.length == 0) {
        errorMessage += "Work Notes,<br/>";
        errorOccured = true;
    }

    if (errorOccured) {
        openErrorDialog("400", errorMessage.substring(0, errorMessage.length - 6));
        return false;
    } else {
        var form_fh = $("#hdTicketDTOEdit");
        var data = $(form_fh).serializeArray();
        var ticketId = $("#hdTicketDTOEdit").find("#ticketId").val();

        $.ajax({
            type: 'post',
            url: pageContextPath + '/hdMainTable/editPrioritySection.html',
            data: data,
            success: function (data) {
                if (data == "success") {
                    closeWindow3();
                    $("#popupBody2").load(
                            pageContextPath + "/hdMainTable/hdTicketSummary.html?ticketId="
                            + ticketId);
                } else {
                    $("#popupBody3").html(
                            '<div class="MSERR0001 WASTD0003">' + data
                            + '</div>');
                    $("#pageTitle3").text("Error");
                }
            }
        });
    }
}

function openEditBackUp() {
    var ticketId = $("#hdTicketDTO").find("#ticketId").val();
    $('#popUpScreen2').block({
        message: $('#popUpScreen3'),
        css: {
            width: '500px',
            height: 'auto',
            position: 'absolute',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    var blockTop = $('#popUpScreen2').height() / 2 - 100;
    var blockLeft = $('#popUpScreen2').width() / 2;
    $('#popUpScreen3').parent('.blockUI.blockMsg').offset({
        top: blockTop,
        left: blockLeft
    });
    $("#pageTitle3").html("Edit Backup Status");
    $.ajax({
        type: 'GET',
        url: pageContextPath + '/hdMainTable/editBackupPage.html?ticketId=' + ticketId,
        success: function (obj) {
            $("#popupBody3").html(obj);
        }
    });
}

function editBackupStatus() {
    var errorMessage = '<b>Please enter information for the following:</b> <br/><br/>';
    var errorOccured = false;

    if (!$("#hdTicketDTOEdit").find("input[type='radio'][name='backUpCktStatus']").is(':checked')) {
        errorMessage += "Status,<br/>";
        errorOccured = true;
    }
    if (errorOccured) {
        openErrorDialog("400", errorMessage.substring(0, errorMessage.length - 6));
        return false;
    } else {
        var form_fh = $("#hdTicketDTOEdit");
        var data = $(form_fh).serializeArray();
        var ticketId = $("#hdTicketDTOEdit").find("#ticketId").val();

        $.ajax({
            type: 'post',
            url: pageContextPath + '/hdMainTable/editBackupDetails.html',
            data: data,
            success: function (data) {
                if (data == "success") {
                    closeWindow3();
                    $("#popupBody2").load(
                            pageContextPath + "/hdMainTable/hdAckTicketSummary.html?ticketId="
                            + ticketId);
                } else {
                    $("#popupBody3").html(
                            '<div class="MSERR0001 WASTD0003">' + data
                            + '</div>');
                    $("#pageTitle3").text("Error");
                }
            }
        });
    }
}

function addNewSite() {
    var errorMessage = '<b>Please enter information for the following:</b> <br/><br/>';
    var errorOccured = false;

    if ($("#siteDetailDTONew").find("#siteNumber").val() == "") {
        errorMessage += "Site Number,<br/>";
        errorOccured = true;
    }
    if ($("#siteDetailDTONew").find("#siteAddress").val() == "") {
        errorMessage += "Site Address,<br/>";
        errorOccured = true;
    }
    if ($("#siteDetailDTONew").find("#siteCity").val() == "") {
        errorMessage += "City,<br/>";
        errorOccured = true;
    }
    if ($("#siteDetailDTONew").find("#state").val() == "") {
        errorMessage += "State,<br/>";
        errorOccured = true;
    }
    if ($("#siteDetailDTONew").find("#country").val() == "") {
        errorMessage += "Country,<br/>";
        errorOccured = true;
    }
    if (errorOccured) {
        openErrorDialog("400", errorMessage.substring(0, errorMessage.length - 6));
        return false;
    } else {
        var msaId = $("#siteDetailDTONew").find("#msaId").val();
        var form_fh = $("#siteDetailDTONew");
        var data = $(form_fh).serializeArray();



        var newSiteId = '';
        var siteAddStatus = false;
        $.ajax({
            type: 'post',
            url: pageContextPath + '/hdMainTable/addNewSite.html',
            data: data,
            success: function (data) {

                try {
                    var obj = jQuery.parseJSON(unescape(decodeURI(data).replace(/\+/g, " ")));
                    if (obj.success = 'success' && obj.id != '') {
                        newSiteId = obj.id;
                        siteAddStatus = true;
                        closeWindow2();
                    } else {
                        $("#popupBody2").html(
                                '<div class="MSERR0001 WASTD0003">' + data
                                + '</div>');
                        $("#pageTitle2").text("Error");
                    }
                } catch (e)
                {
                    //openErrorDialog("400",data);
                    return false;
                }
            }
        });
        if (siteAddStatus)
        {

            $.ajax({
                type: 'post',
                async: false,
                url: pageContextPath + '/hdMainTable/getSitesList.html',
                data: {'msaId': msaId},
                success: function (data) {
                    var obj = jQuery.parseJSON(unescape(decodeURI(data).replace(/\+/g, " ")));
                    var options = '';
                    options = '<option value="">Select</option>';
                    $.each(obj, function (idx, site) {
                        options += '<option contactName="' + site.primaryName + '" phoneNumber="' + site.sitePhone + '" emailAddress="' + site.primaryEmail + '"';
                        options += 'siteNumber="' + site.siteNumber + '" siteAddress="' + site.siteAddress + '" siteCity="' + site.siteCity + '" siteState="' + site.state + '"';
                        options += 'siteCountry="' + site.country + '" siteZipCode="' + site.siteZipcode + '"  value="' + site.siteId + '"';
                        if (site.siteId == newSiteId)
                        {
                            options += ' selected="selected"';
                        }
                        options += '>' + site.siteNumber + ' - ' + site.siteAddress + ', ' + site.siteCity + ',' + site.state;
                        options += '</option>';
                    });

                    //var siteDropdown = getElementById('siteDetailDTO\\.siteId');

                    var siteDropdown = $("#siteDetailDTO\\.siteId");



                    siteDropdown.html(options);



                    //$('#siteDetailDTO\\.siteId').html(options);
                    //var siteSelect=$('#siteDetailDTO\\.siteId');


                    fillPOCDetails(siteDropdown);
                    //window.close();
                    $("#toPopup").css("display", "none");
                    $("#backgroundPopup").css("display", "none");
                }
            });
        }
    }
}

function openResolutionSection() {
    var ticketId = $("#hdTicketDTO").find("#ticketId").val();
    $('#popUpScreen2').block({
        message: $('#popUpScreen3'),
        css: {
            width: '500px',
            height: 'auto',
            position: 'absolute',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    var blockTop = $('#popUpScreen2').height() / 2 - 100;
    var blockLeft = $('#popUpScreen2').width() / 2;
    $('#popUpScreen3').parent('.blockUI.blockMsg').offset({
        top: blockTop,
        left: blockLeft
    });
    $("#pageTitle3").html("Edit Resolution Status");

    $.ajax({
        type: 'GET',
        url: pageContextPath + '/hdMainTable/editResolutionSecPage.html?ticketId=' + ticketId,
        success: function (obj) {
            $("#popupBody3").html(obj);
        }
    });
}

function editResolution() {
    var errorMessage = '<b>Please enter information for the following:</b> <br/><br/>';
    var errorOccured = false;

    if ($("#pointOfFailure").val() == "") {
        errorMessage += "Point of Failure,<br/>";
        errorOccured = true;
    }
    if ($("#resolution").val() == "") {
        errorMessage += "Resolution,<br/>";
        errorOccured = true;
    }
    if ($("#rootCause").val() == "") {
        errorMessage += "Root Cause,<br/>";
        errorOccured = true;
    }
    var workNotes = $.trim($("#workNotes").val());
    if (workNotes.length == 0) {
        errorMessage += "Work Notes,<br/>";
        errorOccured = true;
    }
    if (errorOccured) {
        openErrorDialog("400", errorMessage.substring(0, errorMessage.length - 6));
        return false;
    } else {
        var form_fh = $("#hdTicketDTOEdit");
        var data = $(form_fh).serializeArray();
        var ticketId = $("#hdTicketDTOEdit").find("#ticketId").val();

        $.ajax({
            type: 'post',
            url: pageContextPath + '/hdMainTable/editResolution.html',
            data: data,
            success: function (data) {
                if (data == "success") {
                    closeWindow3();
                    $("#popupBody2").load(
                            pageContextPath + "/hdMainTable/hdTicketSummary.html?ticketId="
                            + ticketId);
                } else {
                    $("#popupBody3").html(
                            '<div class="MSERR0001 WASTD0003">' + data
                            + '</div>');
                    $("#pageTitle3").text("Error");
                }
            }
        });
    }
}


function openStateTail() {
    var ticketId = $("#ackTicketForm1").find("#ticketId").val();
    $('#popUpScreen2').block({
        message: $('#popUpScreen3'),
        css: {
            width: '500px',
            height: 'auto',
            position: 'absolute',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    var blockTop = $('#popUpScreen2').height() / 2 - 100;
    var blockLeft = $('#popUpScreen2').width() / 2;
    $('#popUpScreen3').parent('.blockUI.blockMsg').offset({
        top: blockTop,
        left: blockLeft
    });
    $("#pageTitle3").html("WUG State Tail");

    $.ajax({
        type: 'GET',
        url: pageContextPath + '/hdMainTable/stateTail.html?ticketId=' + ticketId,
        success: function (obj) {
            $("#popupBody3").html(obj);
        }
    });
}

function openStateTailFromAction(ticketId) {
    $('#ticketPopUpScreen').block({
        message: $('#popUpScreen2'),
        css: {
            width: '500px',
            height: 'auto',
            position: 'absolute',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 100);
    $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 100);
    $("#pageTitle2").html("WUG State Tail");

    $.ajax({
        type: 'GET',
        url: pageContextPath + '/hdMainTable/stateTail.html?ticketId=' + ticketId,
        success: function (obj) {
            $("#popupBody2").html(obj);
        }
    });
}
function updateResolveTicket() {
    var errorMessage = '<b>Please enter information for the following:</b> <br/><br/>';
    var errorOccured = false;

    if ($('input:radio[name="lmCompleteFirstCall"]:checked').val() == undefined) {
        errorMessage += "First Call Resolution,<br/>";
        errorOccured = true;
    }
    if ($("#pointOfFailureId").val() == "") {
        errorMessage += "Point Of Failure,<br/>";
        errorOccured = true;
    }
    if ($("#resolutionId").val() == "") {
        errorMessage += "Resolution,<br/>";
        errorOccured = true;
    }
    if ($("#rootCauseId").val() == "") {
        errorMessage += "Root Cause,<br/>";
        errorOccured = true;
    }
    if (errorOccured) {
        openErrorDialog("400", errorMessage.substring(0, errorMessage.length - 6));
        return false;
    } else {
        var ticketId = $("#ticketId").val();
        $.ajax({
            type: 'post',
            url: pageContextPath + '/hdMainTable/updateResolveTicket.html',
            data: $("form").serialize(),
            success: function (data) {
                if (data == "success") {
                    closeWindow2();
                    if (ticketPopUp)
                    {
                        $.unblockUI();
                        $("#ticketPopUpBody").empty().html(
                                '<img src=""' + pageContextPath
                                + '/resources/images/waiting_tree.gif" />');
                        $("#ticketPageTitle").innerHTML = '';
                    }
                    else {
                        closeActiveResolvedTicket(ticketId);
                    }
                    //refresh WIP, WIP over due and WIP next action cubes
                    hdTables.hdWipTable.fnReloadAjax();
                    hdTables.hdWipoverdueTable.fnReloadAjax();
                    hdTables.hdResolvedTable.fnReloadAjax();
                    hdTables.hdWipNextActionTable.fnReloadAjax();
                } else {
                    $("#popupBody2").html(
                            '<div class="MSERR0001 WASTD0003">' + data
                            + '</div>');
                    $("#pageTitle2").text("Error");
                }
            }
        });
    }
}

function validateTicketSummaryWorkNotes() {
    var ajaxReturn = true;
    var ticketId = $("#workNotesSave").find("#ticketId").val();
    var workNotes = $.trim($("#workNotesTicketSummary").val());
    var workNotesSpellMessage = 'Your note contains one or more misspelled words.<br/> Since this note will be sent to the client,<br/> it is important to keep this communication as professional as possible.<br/> Please correct the spelling.';
    var correctionLink = '<br/><a style="text-decoration: underline;color: #0000ff; " href="javascript:runImpspellerTextarea(\'#workNotesTicketSummary\');" id="impcheck" >Click here</a> ';
    workNotesSpellMessage += correctionLink;
    workNotesSpellMessage += ' for assistance correcting misspelled word(s). Ensure all abbreviations are in upper case.';
    if (workNotes.length == 0) {
        openErrorDialog("200", "Please Enter Work Notes");
        return false;
    }

    if (workNotes.length > 2000) {
        openErrorDialog("500", "You have entered a note that is too long.<br/>  Notes should be limited to less than 2000 characters. <br/> Please correct your input and consult your supervisor on the proper use of this item.");
        return false;
    }
    if (!$("#overrideSpellCheckTicketSummary").is(":checked")) {
        $.ajax({
            type: "POST",
            url: pageContextPath + '/hdMainTable/checkWords.html',
            async: false,
            data: 'text=' + workNotes,
            dataType: "json",
            error: function (XHR, status, error) {
                $("#updateMessage").html("Update Failed");
                $("#updateMessage").addClass("MSERR0002").removeClass(
                        "MSSUC0002");
                $("#updateMessage").show();
                ajaxReturn = false;
            },
            success: function (json) {
                if (!json.length) {
                    ajaxReturn = saveTicketSummaryWorkNotes(ticketId, workNotes);

                } else {
                    openErrorDialog("600", workNotesSpellMessage);
                    ajaxReturn = false;
                }
            }
        });
    } else {
        ajaxReturn = saveTicketSummaryWorkNotes(ticketId, workNotes);
    }

    return ajaxReturn;
}

function saveTicketSummaryWorkNotes(ticketId, workNotes) {
    var ajaxReturn = true;
    $.ajax({
        type: 'POST',
        data: {
            "ticketId": ticketId,
            "workNotes": workNotes
        },
        async: false,
        url: pageContextPath + '/hdMainTable/saveWorkNotes.html',
        success: function (obj) {
            if (obj == "success") {
                closeWindow2();
            } else {
                ajaxReturn = false;
            }


        }
    });
    return 	ajaxReturn;
}


function recordTicketSummaryEfforts() {

    var ticketId = $("#ticketEffortsSaveForm").find("#ticketId").val();
    var workNotes = $.trim($("#workNotesTicketSummary").val());
    var ajaxReturn = true;
    var invalidEffortMessage = 'An invalid effort value was entered.<br/> The effort value should be less than 1440 minutes.<br/> In general, you should be entering these values incrementally to avoid this error.<br/>  Please correct your input or consult your supervisor on how to resolve.';
    var effortValue = $("#ticketSummaryEfforts").val();
    if (effortValue == "") {
        openErrorDialog("500", invalidEffortMessage);
        return false;
    }
    if (isNaN(effortValue)) {
        openErrorDialog("500", invalidEffortMessage);
        return false;

    }
    if ((parseInt(effortValue) <= 0) || (parseInt(effortValue) > 1440)) {
        openErrorDialog("500", invalidEffortMessage);
        return false;

    }

    if (workNotes == "") {
        var invalidWorkNotes = 'You are updating the effort field.<br/>  You must explain why this action is necessary with a note.<br/>  Please enter a Work note and click Record Effort.';
        openErrorDialog("500", invalidWorkNotes);
        return false;

    } else {
        $.ajax({
            type: 'POST',
            data: {
                "ticketId": ticketId,
                "workNotes": workNotes
            },
            async: false,
            url: pageContextPath + '/hdMainTable/saveWorkNotes.html',
            success: function (obj) {
                if (obj == "success") {
                    $.ajax({
                        type: "POST",
                        url: pageContextPath + '/hdActiveTicket/recordEfforts.html',
                        async: false,
                        data: {
                            "ticketId": ticketId,
                            "effortValue": effortValue
                        },
                        success: function (data) {
                            if (data == "success") {
                                closeWindow2();
                            }
                        }

                    });

                } else {
                    ajaxReturn = false;
                }


            }
        });


    }
    return ajaxReturn;
}


function updateNextActionFromTicketSummary() {
    var ajaxReturn = true;
    var nextActionUnit;
    var nextActionTimes;
    var ticketId = $("#ticketNextActionSaveForm").find("#ticketId").val();
    var nextAction = $("#ticketNextActionTicketSummary").val();
    if (nextAction == "0") {
        nextAction = $("#currentNextActionTicketSummary").val();
    }

    var invalidTimeFrameMessage = 'An invalid time frame or date was entered. <br/>Time frame in hours should less than 24.<br/> Time frame in minutes should be less 60.<br/>  The date is limited to 30 days in the future. <br/> Please correct your input.';
    if ($("input[type='radio'][name='nextActionTimeTicketSummary']").is(':checked')) {
        var nextActionTimeRadio = $(
                "input[type='radio'][name='nextActionTimeTicketSummary']:checked").val();

        var nextActionTime = $("#nextActionTimeTicketSummary").val();

        if (nextActionTimeRadio == "Hrs") {
            if (parseFloat(nextActionTime) < 0
                    || parseFloat(nextActionTime) > 24) {
                openErrorDialog("500", invalidTimeFrameMessage);
                return false;
            }
            nextActionUnit = "Hrs";

        } else {
            if (parseFloat(nextActionTime) < 0
                    || parseFloat(nextActionTime) > 60) {
                openErrorDialog("500", invalidTimeFrameMessage);
                return false;
            }
            nextActionUnit = "Mins";

        }

        if (nextActionTime == "") {
            openErrorDialog("500", invalidTimeFrameMessage);
            return false;
        }
        nextActionTimes = nextActionTime;

    } else {
        var wipNextActionDueDate = $("#wipNextActionDueDateTicketSummary").val();
        if (wipNextActionDueDate == "") {
            openErrorDialog("500", invalidTimeFrameMessage);
            return false;

        } else {
            var dueDate = new Date(wipNextActionDueDate);
            var currentDate = new Date();
            var dif = dueDate.getTime() - currentDate.getTime() + 80000;

            if (parseFloat(dif) < 0) {
                openErrorDialog("500", invalidTimeFrameMessage);
                return false;
            }
            var differenceInDays = parseInt((dueDate - currentDate)
                    / (1000 * 60 * 60 * 24));
            if (!isNaN(differenceInDays)) {
                if (parseFloat(differenceInDays) >= 30) {
                    openErrorDialog("500", invalidTimeFrameMessage);
                    return false;
                }
            }
        }

        nextActionUnit = "Date";
        nextActionTimes = wipNextActionDueDate;

    }
    var workNotes = $.trim($("#workNotesTicketSummary").val());

    if (workNotes != "") {


        $.ajax({
            type: 'POST',
            data: {
                "ticketId": ticketId,
                "workNotes": workNotes
            },
            async: false,
            url: pageContextPath + '/hdMainTable/saveWorkNotes.html',
            success: function (obj) {
                if (obj == "success") {
                    $.ajax({
                        type: 'POST',
                        data: {
                            "ticketId": ticketId,
                            "nextActionUnit": nextActionUnit,
                            "nextActionTime": nextActionTimes,
                            "nextAction": nextAction
                        },
                        async: false,
                        url: pageContextPath + '/hdActiveTicket/updateNextAction.html',
                        success: function (obj) {
                            closeWindow2();
                        }
                    });

                } else {
                    ajaxReturn = false;
                }


            }
        });




    } else {
        var invalidWorkNotes = 'You are updating the Next Action field.<br/>   You must explain why this action is necessary with a note.<br/>   Please enter a Work note and click Update Next Action.';
        openErrorDialog("500", invalidWorkNotes);
        return false;
    }

    return ajaxReturn;

}

function openCancelPage(ticketId) {
    $.ajax({
        type: 'POST',
        data: {
            "ticketId": ticketId,
            "ackCubeType": "hdPendingAckCust"
        },
        url: pageContextPath + '/hdMainTable/checkStatus.html',
        success: function (data) {
            if (data == "success") {
                $('#ticketPopUpScreen').block({
                    message: $('#popUpScreen2'),
                    css: {
                        width: '600',
                        height: 'auto',
                        position: 'absolute',
                        color: 'black'

                    },
                    overlayCSS: {
                        backgroundColor: '#f0f0f0',
                        opacity: 0.3
                    },
                    centerX: true,
                    centerY: true,
                    fadeIn: 0,
                    fadeOut: 0
                });

                $("#pageTitle2").text(
                        "Cancel Ticket");
                $("#popupBody2").load(
                        pageContextPath + "/hdMainTable/hdCancelTicket.html?ticketId="
                        + ticketId);

                $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 20);
                $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 20);
            }
            else {
                alert(data);
            }
        }
    });

}

function cancelTicket(ticketId) {
    var errorMessage = '<b>Please enter information for the following:</b> <br/><br/>';
    var errorOccured = false;

    if ($("#cancelWorkNotes").val() == "") {
        errorMessage += "Work Notes,<br/>";
        errorOccured = true;
    }

    if (errorOccured) {
        openErrorDialog("400", errorMessage.substring(0, errorMessage.length - 6));
        return false;
    } else {
        var workNotes = $("#cancelWorkNotes").val();

        $("#popupBody2").html('<div class="WASTD0003">Cancelling...</div>');

        var session_id = createID();

        cancelTicketAJAX(ticketId, workNotes, session_id, 1);
    }
}

function cancelTicketAJAX(ticketId, workNotes, session_id, attempt) {
    $.ajax({
        type: 'GET',
        data: {
            "ticketId": ticketId,
            "workNotes": workNotes,
            "userId": userId,
            "session_id": session_id,
            "method": "cancelHDTicket"
        },
        url: '/helpdesk/index.php',
        async: true,
        dataType: "json",
        success: function (data) {
            if (data.status == 'success') {
                $("#popupBody2").html('<div class="WASTD0003">' + data.msg + '</div>');
                setTimeout(function () {
                    closeWindow2();
                    hdTables.hdpendingackcustTable.fnReloadAjax();
                    hdTables.hdpendingacksecurityTable.fnReloadAjax();
                    $.unblockUI();
                }, 1000);
            } else {
                $("#popupBody2").html('<div class="WASTD0003">Ticket cancellation failed. Error: ' + data.msg + '</div>');
            }
            clearLockStatus(ticketId);
        }, error: function (xhr, error) {
            if (attempt == maxAttempts) {
                $("#popupBody2").html('<div class="MSERR0001 WASTD0003">' + error + '</div>');
                $("#pageTitle2").text("Error");
                clearLockStatus(ticketId);
            } else {
                setTimeout(function () {
                    cancelTicketAJAX(ticketId, workNotes, session_id, attempt + 1);
                }, 1000);
            }

        }
    });
}

function openNoActionPage(ticketId) {
    $.ajax({
        type: 'POST',
        data: {
            "ticketId": ticketId,
            "ackCubeType": "hdPendingAckAlert"
        },
        url: pageContextPath + '/hdMainTable/checkStatus.html',
        success: function (data) {
            if (data == "success") {
                $('#ticketPopUpScreen').block({
                    message: $('#popUpScreen2'),
                    css: {
                        width: '400',
                        height: 'auto',
                        position: 'absolute',
                        color: 'black'

                    },
                    overlayCSS: {
                        backgroundColor: '#f0f0f0',
                        opacity: 0.3
                    },
                    centerX: true,
                    centerY: true,
                    fadeIn: 0,
                    fadeOut: 0
                });

                $("#pageTitle2").text(
                        "Cancel Ticket");
                $("#popupBody2").load(
                        pageContextPath + "/hdMainTable/hdNoActionPage.html?ticketId="
                        + ticketId);

                $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 20);
                $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 20);
            }
            else {
                alert(data);
            }
        }
    });


}

function saveNoAction(ticketId) {

    var session_id = createID();
    cancelTicketAJAX(ticketId, 'Canceling ticket ' + ticketId, session_id, 1);


//    $.ajax({
//        type: 'post',
//        url: pageContextPath + '/hdMainTable/saveNoAction.html',
//        data: {
//            "ticketId": ticketId
//        },
//        success: function(data) {
//            if (data == "success") {
//                closeWindow2();
//                hdTables.hdpendingackmonitorTable.fnReloadAjax();
//                setPendingAckMonitoringFooterDetail();
//                hdTables.hdpendingackalertTable.fnReloadAjax();
//                $.unblockUI();
//            } else {
//                $("#popupBody2").html(
//                        '<div class="MSERR0001 WASTD0003">' + data
//                        + '</div>');
//                $("#pageTitle2").text("Error");
//            }
//        }
//    });
}

function openNoActionsDaysPage(ticketId) {
    $('#ticketPopUpScreen').block({
        message: $('#popUpScreen2'),
        css: {
            width: '400',
            height: 'auto',
            position: 'absolute',
            color: 'black'

        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });

    $("#pageTitle2").text(
            "No Action (14 days)");
    $("#popupBody2").load(
            pageContextPath + "/hdMainTable/openNoActionsDaysPage.html?ticketId="
            + ticketId);

    $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 20);
    $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 20);
}
$(window).scroll(function () {
    var windowScrollTop = $(window).scrollTop();
    var windowHeight = $(window).height();
    var overflowHeight = ($('#middleDiv').height() + 100) - windowHeight;
    //if(overflowHeight>windowScrollTop)
    //$('#popUpScreen2').parent().css('margin-top', windowScrollTop );
});
function openEditSiteSection() {
    var ticketId = $("#hdTicketDTO").find("#ticketId").val();
    var siteId = $("#hdTicketDTO").find("#siteId").val();
    $('#popUpScreen2').block({
        message: $('#popUpScreen3'),
        css: {
            width: '500px',
            height: 'auto',
            position: 'absolute',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    var blockTop = $('#popUpScreen2').height() / 2 - 100;
    var blockLeft = $('#popUpScreen2').width() / 2;
    $('#popUpScreen3').parent('.blockUI.blockMsg').offset({
        top: blockTop,
        left: blockLeft
    });
    $("#pageTitle3").html("Site");

    $.ajax({
        type: 'GET',
        url: pageContextPath + '/hdMainTable/editSiteSecPage.html?siteId=' + siteId + '&ticketId=' + ticketId,
        success: function (obj) {
            $("#popupBody3").html(obj);
        }
    });
}
function updateSiteSection() {
    var errorMessage = '<b>Please enter information for the following:</b> <br/><br/>';
    var errorOccured = false;
    if (errorOccured) {
        openErrorDialog("400", errorMessage.substring(0, errorMessage.length - 6));
        return false;
    } else {
        var form_fh = $("#siteDetailDTOEdit");
        var data = $(form_fh).serializeArray();
        var ticketId = $("#siteDetailDTOEdit").find("#ticketId").val();

        $.ajax({
            type: 'post',
            url: pageContextPath + '/hdMainTable/updateSiteSection.html',
            data: data,
            success: function (data) {
                if (data == "success") {
                    closeWindow3();
                    $("#popupBody2").load(
                            pageContextPath + "/hdMainTable/hdTicketSummary.html?ticketId="
                            + ticketId);
                } else {
                    $("#popupBody3").html(
                            '<div class="MSERR0001 WASTD0003">' + data
                            + '</div>');
                    $("#pageTitle3").text("Error");
                }
            }
        });
    }
}

function onErrorBoxHide()
{
    $('#Screen2').find('input[type=button]').css("background-color", "#D7E1EB");
    $('#Screen2').find('*').attr('disabled', false);


}
// var hd_test="closed";

function openErrorDialog(width, errorMessage)

{
    //if(hd_test=="closed"){

    $("#dialog").html(errorMessage);

    new Messi($("#dialog").html());
    hd_test = "opened";

    $.blockUI({
        css: {'background': 'none'},
        message: ''
    });

    $(".blockOverlay").css("background-color", "#F0F0F0");


//    $('#Screen2').find('input[type=button]').css("background-color","#D3D3D3");
//    $('#Screen2').find('*').attr('disabled', true);
//    $('#Screen1').find('*').attr('disabled', true);
//


}
//}


function openErrorDialog_old(width, errorMessage)
{
    $("#dialog").dialog("option", "title", "Error");
    $("#dialog").dialog("option", "width", width);
    $("#dialog").dialog("option", "zIndex", "50001");
    $("#dialog").dialog({position: {my: "center", at: "center", of: window}});
    $("#dialog p").html(errorMessage);
    $("#dialog").dialog("open");


}


function redirectToNetMedxBoard(ticketId) {
    $.ajax({
        type: 'post',
        async: false,
        url: pageContextPath + '/hdActiveTicket/ticketDetailForDashboard.html',
        data: {'ticketId': ticketId},
        success: function (data) {
            var obj = jQuery.parseJSON(unescape(decodeURI(data).replace(/\+/g, " ")));
            var jobId = obj.jobId;
            var appendixId = obj.appendixId;
            var url = '/Ilex/JobDashboardAction.do?ticketType=existingTicket&ticket_id=' + ticketId + '&Job_Id=' + jobId + '&appendixid=' + appendixId + '&tabId=4&isClicked=1';
            window
                    .open(
                            url,
                            'popupwindow',
                            'width=1100,height=900,top=100,left=150,scrollbars=yes,status=yes,resizable=yes,location=1');
        }
    });


}

function validateWorkNotesForSpell() {
    var ajaxReturn = true;
    var ticketId = $("#headerIlexTicketId").text();
    var workNotes = $.trim($("#ticketWorkNotes").val());
    var workNotesSpellMessage = 'Your note contains one or more misspelled words.<br/> Since this note will be sent to the client,<br/> it is important to keep this communication as professional as possible.<br/> Please correct the spelling.';
    var correctionLink = '<br/><a style="text-decoration: underline;color: #0000ff; " href="javascript:runImpspellerTextarea(\'#ticketWorkNotes\');" id="impcheck" >Click here</a> ';
    workNotesSpellMessage += correctionLink;
    workNotesSpellMessage += ' for assistance correcting misspelled word(s). Ensure all abbreviations are in upper case.';

    if (workNotes.length > 2000) {
        openErrorDialog("500", "You have entered a note that is too long.<br/>  Notes should be limited to less than 2000 characters. <br/> Please correct your input and consult your supervisor on the proper use of this item.");
        return false;
    }
    if (!$("#overrideSpellCheck").is(":checked")) {
        $.ajax({
            type: "POST",
            url: pageContextPath + '/hdMainTable/checkWords.html',
            async: false,
            data: 'text=' + workNotes,
            dataType: "json",
            error: function (XHR, status, error) {
                $("#updateMessage").html("Update Failed");
                $("#updateMessage").addClass("MSERR0002").removeClass(
                        "MSSUC0002");
                $("#updateMessage").show();
                ajaxReturn = false;
            },
            success: function (json) {
                if (!json.length) {
                    //ajaxReturn = saveWorkNotes(ticketId, workNotes);

                } else {
                    openErrorDialog("600", workNotesSpellMessage);
                    ajaxReturn = false;
                }
            }
        });
    } else {
        //ajaxReturn = saveWorkNotes(ticketId, workNotes);
    }

    return ajaxReturn;
}
function openEditTicketState(ticketId, ticketStatus) {
    $('#ticketPopUpScreen').block({
        message: $('#popUpScreen2'),
        css: {
            width: '450',
            height: 'auto',
            position: 'absolute',
            left: '30%',
            top: '180px',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    var ticketStatusName = "";
    if (ticketStatus == "3")
        ticketStatusName = "Dispatch Requested";
    else if (ticketStatus == "7")
        ticketStatusName = "Awaiting Response/Action";
    else if (ticketStatus == "9")
        ticketStatusName = "Assigned to Customer";
    var pageTitle = "Set State " + ticketStatusName;
    $("#pageTitle2").text(pageTitle);
    $("#popupBody2").load(
            pageContextPath + "/hdMainTable/editTicketState.html?ticketId=" + ticketId + "&ticketStatus=" + ticketStatus);

    $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 20);
    $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 20);

}
function updateTicketTargetState() {
    var errorMessage = '<b>Please enter information for the following:</b> <br/><br/>';
    var errorOccured = false;
    if ($("#workNotes").val() == "") {
        errorMessage += "Work Notes,<br/>";
        errorOccured = true;
    }
    if (errorOccured) {
        openErrorDialog("400", errorMessage.substring(0, errorMessage.length - 6));
        return false;
    } else {
        $.ajax({
            type: 'post',
            url: pageContextPath + '/hdMainTable/updateTicketTargetState.html',
            data: $("form").serialize(),
            success: function (data) {
                if (data == "success") {
                    closeWindow2();
                    //refresh WIP-overdue cube and WIP next action cube
                    hdTables.hdWipoverdueTable.fnReloadAjax();
                    hdTables.hdResolvedTable.fnReloadAjax()
                    hdTables.hdWipNextActionTable.fnReloadAjax();
                } else {
                    $("#popupBody2").html(
                            '<div class="MSERR0001 WASTD0003">' + data
                            + '</div>');
                    $("#pageTitle2").text("Error");
                }
            }
        });
    }
}
function noActionAllMonitoringTickets()
{
    $.ajax({
        type: 'post',
        url: pageContextPath + '/hdMainTable/noActionAll.html',
        success: function (data) {
            if (data == "success") {
                //refresh pending acknowledgement Monitoring cube
                hdTables.hdpendingackmonitorTable.fnReloadAjax();
                setPendingAckMonitoringFooterDetail();
            } else {
                openErrorDialog("400", data);
                return false;
            }
        }
    });
}
function setPendingAckMonitoringFooterDetail()
{
    $.ajax({
        type: 'post',
        async: false,
        url: pageContextPath + '/hdMainTable/lastUpdateEventDetail.html',
        success: function (data) {
            var obj = jQuery.parseJSON(unescape(decodeURI(data).replace(/\+/g, " ")));
            var infoText = $("#hdPendingAckMonitorTable_info").text();
            infoText += "<span style='padding-left:20px;' >Last Update: " + obj.runTimeDisplay + "<span>";
            infoText += "<span style='padding-left:20px;' >Events: " + obj.eventCount + "<span>";
            $("#hdPendingAckMonitorTable_info")
                    .html(infoText);

        }
    });
}
function getCurrentDate() {
    var d = new Date();
    var hours = d.getHours();
    var minutes = d.getMinutes();

    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (hours < 10) {
        hours = "0" + hours;
    }
    var currentDate = $.datepicker.formatDate("mm-dd-yy", new Date());
    var fullCurrentDate = currentDate + " " + hours + ":" + minutes;  //Date format : YYYY-MM-DD HH:mm
    return fullCurrentDate;
}
function closeActiveResolvedTicket(ticketId) {

    $("#updateTicketButton").removeClass('FMBUT0005').addClass('FMBUT0003');
    $("#activeTicketDiv").hide();
    $(".wipTicketsActive").text("Drop Ticket Here");
    $(".wipTicketsActive").addClass("emptySlot");
    $(".wipTicketsActive").removeAttr(
            "ilexNumber ticketId  customer customerNumber sitenumber");
    $(".wipTicketsActive")
            .addClass("wipTicketsInActive")
            .removeClass(
                    "wipTicketsActivePriority1  wipTicketsActivePriority3 wipTicketsActive  wipTicketsActivePriority1 wipTicketsActive wipTicketsActiveChanged");
}
function openLatestWorkNotes(ticketId) {

    $('#ticketPopUpScreen').block({
        message: $('#popUpScreen2'),
        css: {
            width: 'auto',
            height: 'auto',
            position: 'absolute',
            color: 'black'

        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });

    $("#pageTitle2").text(
            "Work Notes");
    $("#popupBody2").load(
            pageContextPath + "/hdMainTable/getWorkNotesPage.html?ticketId="
            + ticketId);

    $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 100);
    $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 100);

}
function showLatestWorkNotesForActiveTicket(ticketId)
{
    var content = '';
    var worNotesTitle = '<div><b>Recent Work Notes (Latest 2)</b></div><br/>';
    content += worNotesTitle;
    $.ajax({
        type: 'post',
        async: false,
        url: pageContextPath + '/hdMainTable/getWorkNotesList.html',
        data: {'ticketId': ticketId},
        success: function (data) {
            var obj = jQuery.parseJSON(data);
            var count = 0;
            var worNotesContent = '<div>';
            $.each(obj, function (idx, workNoteDTO) {
                if (count >= 2)
                    return false;
                worNotesContent += '<div class="divFloatLeft"  style="width: 100px;text-align: left;">' + workNoteDTO.workNoteDateStr + '</div>';
                worNotesContent += '<div style="margin-left:100px;width: 330px;word-break: break-all;word-wrap: break-word;"><div>' + workNoteDTO.workNote + '</div><div style="color: grey;">[' + workNoteDTO.userLastName + ']</div></div><br/>';
                count++;

            });
            worNotesContent += '</div>';
            if (count == 0)
            {
                worNotesContent = '<div>There are no work notes.</div>';
            }
            content += worNotesContent;
        }
    });
    $('#activeTicketWorkNotes').html(content);
}
function openCancelAllMonitoringPopUp() {
    ticketPopUp = false;
    $.blockUI({
        message: $('#popUpScreen2'),
        css: {
            width: 'auto',
            height: 'auto',
            position: 'absolute',
            color: 'black'
        },
        overlayCSS: {
            backgroundColor: '#f0f0f0',
            opacity: 0.3
        },
        centerX: true,
        centerY: true,
        fadeIn: 0,
        fadeOut: 0
    });
    $("#pageTitle2").text(
            "Cancel All");
    $("#popupBody2").load(
            pageContextPath + "/hdMainTable/getCancelAllForm.html?");

    $('#popUpScreen2').parent('.blockUI.blockMsg').css('left', 300);
    $('#popUpScreen2').parent('.blockUI.blockMsg').css('top', 100);
}
function getOutagesList() {
    var errorMessage = '<b>Please enter information for the following:</b> <br/><br/>';
    var errorOccured = false;
    if ($("#pointOfFailureId").val() == "") {
        errorMessage += "Point of Failure,<br/>";
        errorOccured = true;
    }
    if ($("#resolutionId").val() == "") {
        errorMessage += "Resolution,<br/>";
        errorOccured = true;
    }
    if ($("#rootCauseId").val() == "") {
        errorMessage += "Root Cause,<br/>";
        errorOccured = true;
    }
    if ($("#duration").val() == "") {
        errorMessage += "Duration,<br/>";
        errorOccured = true;
    }
    else {
        var intRegex = /^\d+$/;
        if (isNaN($("#duration").val()) || (!intRegex.test($("#duration").val()))) {
            errorMessage += "Please enter valid value for Duration,<br/>";
            errorOccured = true;

        }
    }
    if (!$(".cancelCustomers").is(':checked')) {
        errorMessage += "Customers,<br/>";
        errorOccured = true;
    }
    if ($("#timeFrameStart").val() != "" || $("#timeFrameEnd").val() != "") {
        var invalidTimeFrameMessage = "An invalid time frame or date was entered. <br/>Start time must be less than current date/time and end date/time <br/>must be greater than start.Please correct your input.";
        var invalidDate = false;
        if (($("#timeFrameStart").val() == "") || ($("#timeFrameEnd").val() == ""))
        {
            invalidDate = true;
        }
        else {
            var startDate = getDateFromString($("#timeFrameStart").val());
            var endDate = getDateFromString($("#timeFrameEnd").val());
            var currentDate = new Date();
            if ((startDate > currentDate) || (endDate < startDate))
            {
                invalidDate = true;
            }
        }
        if (invalidDate)
        {
            errorMessage += invalidTimeFrameMessage;
            errorOccured = true;
        }

    }
    if (errorOccured) {
        openErrorDialog("500", errorMessage);
        return false;
    } else {
        var form_fh = $("#hdCancelAllDTO");
        //var data = $(form_fh).serializeArray();

        var customers = '';
        $('.cancelCustomers:checked').each(function () {
            var tName = $(this).attr('id');
            var parts = tName.split(".");

            if (customers != '') {
                customers += ',';
            }
            customers += $('#' + parts[0] + '\\.msaId').val();
        });

        data = {};
        data.timeFrameStart = $('#timeFrameStart').val();
        data.timeFrameEnd = $('#timeFrameEnd').val();
        data.duration = $('#duration').val();
        data.method = "getOutagesList";
        data.session_id = createID();
        data.userId = userId;
        data.customers = customers;

        $.ajax({
            type: 'POST',
            data: data,
            url: '/helpdesk/index.php',
            async: true,
            dataType: "json",
            success: function (data) {
                try {
                    if (typeof data.status !== 'undefined' && data.status == 'error') {
                        openErrorDialog("500", data.msg);
                    } else {
                        //$('#displayOutageListBtn').hide();
                        //$('#refreshOutageListBtn').show();
                        $('#outagesDiv').show();
                        var outageList = '';
                        var listSize = data.length;
                        if (listSize == 0) {
                            var message = '<div class="PCSTD0010">No matching site outages found.</div>';
                            $('#outagesListDiv').html(message);
                        } else {
                            var counter = 1;
                            var rowCount = 0;
                            outageList += '<table id=\"outagesTable\">';
                            $.each(data, function (idx, outageDetailDTO) {
                                if (counter % 5 == 1) {
                                    outageList += '<tr class=\" PCSTD0007\">';
                                }
                                outageList += '<td><span style=\"width: 150px; display: inline-block;\" class=\"spanCheck\">';
                                outageList += '<input style="position: relative; top: 3px;" name=\"outageList[' + rowCount + '].checked\" id=\"outageList[' + rowCount + '].checked\" type=\"checkbox\" data-ticket-id="' + outageDetailDTO.lm_hd_tc_id + '" checked=\"checked\" class=\"siteOutage\"/>&nbsp;' + outageDetailDTO.wug_ip_name;
                                outageList += '</span></td>';
                                if (counter % 5 == 0 || counter == listSize) {
                                    outageList += '</tr>';
                                }
                                counter++;
                                rowCount++;
                            });
                            outageList += '</table>';
                            outageList += '<div style=\"margin-top: 10px;\">';
                            outageList += '<input id=\"cancelAllBtn\" type=\"button\" value="Submit" class=\"FMBUT0003\" onclick=\"cancelOutages();\"/>';
                            $('#outagesListDiv').html(outageList);
                            //$('#refreshOutageListBtn').css('margin-left', $('#customerTable').width() - 150 + "px");
                            $('#cancelAllBtn').css('margin-left', $('#outagesTable').width() - 150 + "px");
                        }
                    }
                } catch (err) {
                    openErrorDialog("500", err.message);
                }
            }, error: function (xhr, error) {
                openErrorDialog("500", error);
            }
        });
    }
}
var customerSelectOffIndicator = false;
function selectDeselectCustomers() {
    $('.cancelCustomers').each(function () {
        if (customerSelectOffIndicator)
        {
            $(this).prop('checked', true);
        }
        else
        {
            $(this).prop('checked', false);
        }
    });
    if (customerSelectOffIndicator)
    {
        customerSelectOffIndicator = false;
    }
    else
    {
        customerSelectOffIndicator = true;
    }
}
function selectAllOutages() {
    $('.siteOutage').each(function () {
        $(this).prop('checked', true);
    });
}
function deselectAllOutages() {
    $('.siteOutage').each(function () {
        $(this).prop('checked', false);
    });
}
function cancelOutages() {
    var errorMessage = '';
    var errorOccured = false;
    if (!$(".siteOutage").is(':checked')) {
        errorMessage += "Please Select Outages";
        errorOccured = true;
    }
    if (errorOccured) {
        openErrorDialog("500", errorMessage);
        return false;
    } else {
        var form_fh = $("#hdCancelAllDTO");

        var tickets = '';
        $('.siteOutage:checked').each(function () {
            if (tickets != '') {
                tickets += ',';
            }

            tickets += $(this).data('ticket-id');
        });

        data = {};
        data.rootCauseId = $('#rootCauseId').val();
        data.resolutionId = $('#resolutionId').val();
        data.pointOfFailureId = $('#pointOfFailureId').val();
        data.method = "cancelOutages";
        data.session_id = createID();
        data.userId = userId;
        data.tickets = tickets;

        $.ajax({
            type: 'POST',
            data: data,
            url: '/helpdesk/index.php',
            async: true,
            dataType: "json",
            success: function (data) {
                try {
                    if (typeof data.status !== 'undefined' && data.status == 'error') {
                        openErrorDialog("500", data.msg);
                    } else {
                        $.unblockUI();
                        $("#popupBody2").empty().html('<img src=""' + pageContextPath + '/resources/images/waiting_tree.gif" />');
                        $("#pageTitle2").innerHTML = '';
                        hdTables.hdpendingackmonitorTable.fnReloadAjax();
                    }
                } catch (err) {
                    openErrorDialog("500", err.message);
                }
            }, error: function (xhr, error) {
                openErrorDialog("500", error);
            }
        });
    }
}
function getDateFromString(dateArg) {
    var month = parseInt(dateArg.substring(0, 2) - 1);
    var day = parseInt(dateArg.substring(3, 5));
    var year = parseInt(dateArg.substring(6, 10));
    var hour = parseInt(dateArg.substring(11, 13));
    var seconds = parseInt(dateArg.substring(14, 16));
    return new Date(year, month, day, hour, seconds);
}
$("td span.snSymbol").live(
        "click",
        function (e) {
            var content = "<div>";
            var ticketId = $(this).parent().parent().attr("ticketId");

            $.ajax({
                type: 'post',
                async: false,
                url: pageContextPath + '/serviceNowIntegrate/getSnMessage.html',
                data: {'ticketId': ticketId},
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    content = content + "MessageSource:  " + obj.ticketSource;
                    content = content + "<br/>";
                    content = content + "Message: " + obj.extSystemUpdate + " " + obj.workNotes;
                },
                error: function (data) {
                    content = data;
                }});
            content = content + "</div>";
            $('#toolTip').html(content);
            ShowContent('toolTip', this);
            //			$.ajax({
//
//				type : 'post',
            //				async:false,
            //				url : pageContextPath +'/serviceNowIntegrate/clearIntegratedTicketStatus.html',
//	 		data : {'ticketId':ticketId},
            //				success : function(data) {
//
//					}
//				});
            setTimeout(function () {
                HideContent('toolTip');
                // 20000 for 20 seconds
            }, 20000);
        });

$("td span.snSymbol").live("mouseout", function () {
    HideContent('toolTip');
});

function checkEffortRadio(ele)
{

    var effortRadioVal = $(ele).val();



    if (effortRadioVal == 'date')
    {


        $(".effortTime").css("display", "none");
        $(".spacingDiv").css("display", "none");
        $(".EffortDate").css("display", "block");
        document.getElementById("efforts").value = "";

    }

    else {

        $(".effortTime").css("display", "inline-table");
        $(".spacingDiv").css("display", "block");
        $(".EffortDate").css("display", "none");
        if (document.getElementById("createTicketEffortStart").value != '' && document.getElementById("createTicketEffortStop").value != '') {
            document.getElementById("efforts").value = "";
        }
        document.getElementById("createTicketEffortStart").value = "";
        document.getElementById("createTicketEffortStop").value = "";
    }
}

function checknextActionRadio(ele) {

    var nextActionRadioVal = $(ele).val();
    if (nextActionRadioVal == 'date')
    {


        $(".nextActionTime").css("display", "none");
        $(".nextActionDate").css("display", "block");
        document.getElementById("createTicketNextActionTime").value = "";
        $("input[name='createTicketNextActionUnit']").removeAttr("checked");
    }

    else {

        $(".nextActionTime").css("display", "inline-table");
        $(".nextActionDate").css("display", "none");
        document.getElementById("createTicketNextActionDate").value = "";

    }


}


/*
 * Code to fix $.ajax calls for IE9 when contacting 32.103
 */
// add ajax transport method for cross domain requests when using IE9
if ('XDomainRequest' in window && window.XDomainRequest !== null) {
    $.ajaxTransport("+*", function (options, originalOptions, jqXHR) {
        // verify if we need to do a cross domain request
        // if not return so we don't break same domain requests
        if (typeof options.crossDomain === 'undefined' || !options.crossDomain) {
            return;
        }

        var xdr;

        return {
            send: function (headers, completeCallback) {
                // Use Microsoft XDR
                xdr = new XDomainRequest();
                xdr.open("post", options.url); // NOTE: make sure protocols are the same otherwise this will fail silently
                xdr.onload = function () {
                    if (this.contentType.match(/\/xml/)) {
                        var dom = new ActiveXObject("Microsoft.XMLDOM");
                        dom.async = false;
                        dom.loadXML(this.responseText);
                        completeCallback(200, "success", [dom]);
                    } else {
                        completeCallback(200, "success", [this.responseText]);
                    }
                };

                xdr.onprogress = function () {
                };

                xdr.ontimeout = function () {
                    completeCallback(408, "error", ["The request timed out."]);
                };

                xdr.onerror = function () {
                    completeCallback(404, "error", ["The requested resource could not be found."]);
                };

                xdr.send();
            },
            abort: function () {
                if (xdr)
                    xdr.abort();
            }
        };
    });
}