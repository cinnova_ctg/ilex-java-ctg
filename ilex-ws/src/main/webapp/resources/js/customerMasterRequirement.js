function closeWindow() {
	$.unblockUI();
	$("#popupBody").empty().html(
			'<img src="' + pageContextPath
					+ '/resources/images/waiting_tree.gif" />');
}

function defineNewRequirement() {

	$.blockUI({
		message : $('#popupScreen'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	$('#popupScreen').parent('.blockUI.blockMsg').css('left',100);
	$('#popupScreen').parent('.blockUI.blockMsg').css('top',100);
	
	$("#pageTitle").html("Define Master Monitoring Requirements");
	$("#popupBody").load(
			pageContextPath
					+ "/masterRequirement/defineMasterRequirements.html");

}

function getRequirementsDescription(requirementId) {
	var data = {
		"requirementId" : requirementId
	};
	$.blockUI({
		message : $('#popupScreen'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	$('#popupScreen').parent('.blockUI.blockMsg').css('left',100);
	$('#popupScreen').parent('.blockUI.blockMsg').css('top',100);
	
	$("#pageTitle").html("Monitoring Requirement");

	$.ajax({
		type : 'POST',
		data : data,
		url : pageContextPath
				+ '/masterRequirement/getRequirementDescription.html',
		success : function(data) {
			$("#popupBody").html(data);
		}
	});
}

function addMasterRequirement() {

	var validate = validateRequirement();
	if (!validate) {
		return false;
	}

	var data = $("#hdMasterRequirementForm").serializeArray();

	$
			.ajax({
				type : 'POST',
				data : data,
				url : pageContextPath
						+ '/masterRequirement/addMasterRequirements.html',
				success : function(data) {
					if (data == "success") {
						var url = pageContextPath
								+ '/masterRequirement/viewMasterRequirement.html?pageIndex='
								+ pageStart;
						location.href = url;
						closeWindow();
					} else {
						$("#errorDiv").html(data);
					}
				}
			});

}
function editMasterRequirement(requirementId) {

	var data = {
		"requirementId" : requirementId
	};
	$.blockUI({
		message : $('#popupScreen'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	$('#popupScreen').parent('.blockUI.blockMsg').css('left',100);
	$('#popupScreen').parent('.blockUI.blockMsg').css('top',100);
	
	$("#pageTitle").html("Edit Master Monitoring Requirement");

	$.ajax({
		type : 'POST',
		data : data,
		url : pageContextPath + '/masterRequirement/getMasterRequirement.html',
		success : function(data) {
			$("#popupBody").html(data);
		}
	});

}
function validateRequirement() {
	if ($.trim($("#requirement").val()) == "") {
		alert("Please Enter The Requirement Title");
		$("#requirement").focus();
		return false;
	}

	var cnt = $("input[name='deviceType']:checked").length;

	if (cnt <= 0) {
		alert("Please Select Device Type");
		$("input[name='deviceType']").focus();
		return false;
	}
	if ($("#requirement").val().length < 8) {
		alert("Minimun 8 Characters reqiured in  Title");
		$("#requirement").focus();
		return false;
	}

	if ($("#requirement").val().length > 64) {
		alert("Maximum 64 Characters allowed in Title");
		$("#requirement").focus();
		return false;
	}
	if ($("#description").val().length > 1000) {
		alert("Maximum 1000 Characters allowed in Description");
		$("#description").focus();
		return false;
	}
	return true;
}
function editRequirement() {

	var validate = validateRequirement();
	if (!validate) {
		return false;
	}

	var data = $("#editHDMasterRequirementForm").serializeArray();

	$
			.ajax({
				type : 'POST',
				data : data,
				url : pageContextPath
						+ '/masterRequirement/editMasterRequirement.html',
				success : function(data) {
					if (data == "success") {
						var url = pageContextPath
								+ '/masterRequirement/viewMasterRequirement.html?pageIndex='
								+ pageStart;
						location.href = url;
						closeWindow();
					} else {
						$("#errorDiv").html(data);
					}
				}
			});

}
function getMasterrequirementReport(requirementId) {

	var data = {
		"requirementId" : requirementId
	};
	$.blockUI({
		message : $('#popupScreen'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	$('#popupScreen').parent('.blockUI.blockMsg').css('left',100);
	$('#popupScreen').parent('.blockUI.blockMsg').css('top',100);
	
	$("#pageTitle").html("Monitoring Requirement Report");

	$.ajax({
		type : 'POST',
		data : data,
		url : pageContextPath
				+ '/masterRequirement/reportMasterRequirement.html',
		success : function(data) {
			$("#popupBody").html(data);
		}
	});
}

function getProjectRequirement(projectId, projectName, customerName) {

	var data = {
		"projectId" : projectId,
		"customerName" : customerName,
		"projectName" : projectName
	};

	$.blockUI({
		message : $('#popupScreen'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	$('#popupScreen').parent('.blockUI.blockMsg').css('left',100);
	$('#popupScreen').parent('.blockUI.blockMsg').css('top',100);
	
	$("#pageTitle").html("Edit Project Monitoring Requirements");

	$.ajax({
		type : 'POST',
		data : data,
		url : pageContextPath
				+ '/customerRequirement/getProjectRequirement.html',
		success : function(data) {
			$("#popupBody").html(data);
		}
	});

}

function editProjectRequirements() {

	var cnt = $("#editProjectRequirementForm input[type='checkbox']:checked")
			.filter(':enabled').length;
	if (cnt <= 0) {
		alert("Please Select one of the requirement");
		return false;
	}

	if ($("#editProjectRequirementForm input[type='checkbox']:checked").filter(
			':enabled').parent().next().find('input[type=\'text\']').val().length > 512) {
		alert("Selected Requirements's Notes Length should be less than 512 characters");
		return false;
	}

	var data = $("#editProjectRequirementForm").serializeArray();
	$.ajax({
		type : 'POST',
		data : data,
		url : pageContextPath
				+ '/customerRequirement/editProjectRequirements.html',
		success : function(data) {
			if (data == "success") {
				var url = pageContextPath
						+ '/customerRequirement/viewCustomers.html?pageIndex='
						+ pageStart;
				location.href = url;
				closeWindow();
			} else {
				$("#errorDiv").html(data);
			}
		}
	});

}

function getProjectRequirementReport(projectId, projectName, customerName) {

	var data = {
		"projectId" : projectId,
		"customerName" : customerName,
		"projectName" : projectName
	};

	$.blockUI({
		message : $('#popupScreen'),
		css : {
			width : 'auto',
			height : 'auto',
			position : 'absolute',
			color : 'black'
		},
		overlayCSS : {
			backgroundColor : '#f0f0f0',
			opacity : 0.3
		},
		centerX : true,
		centerY : true,
		fadeIn : 0,
		fadeOut : 0
	});
	$('#popupScreen').parent('.blockUI.blockMsg').css('left',100);
	$('#popupScreen').parent('.blockUI.blockMsg').css('top',100);
	
	$("#pageTitle").html("Project Monitoring Requirement Report");

	$.ajax({
		type : 'POST',
		data : data,
		url : pageContextPath
				+ '/customerRequirement/getProjectRequirementReport.html',
		success : function(data) {
			$("#popupBody").html(data);
		}
	});

}