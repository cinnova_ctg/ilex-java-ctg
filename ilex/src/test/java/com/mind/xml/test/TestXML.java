package com.mind.xml.test;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.mind.ilex.xml.doc.Document;

public class TestXML {

	public static void main(String[] args) {
		try {
			testUnmarshall();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void testMarshal() {

	}

	private static void testUnmarshall() throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Document.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		unmarshaller.setSchema(unmarshaller.getSchema());

		Document doc = (Document) unmarshaller.unmarshal(new File(
				"Y:/common/rohitjain/xml/AppendixFinal.xml"));
		System.out.println(doc.getDoctype());

	}

}
