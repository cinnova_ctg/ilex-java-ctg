package com.mind.PO.actions;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.PO.dao.PODao;
import com.mind.PO.formbean.POBean;
import com.mind.bean.DynamicComboCNSWEB;
import com.mind.bean.mpo.PODTO;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.newjobdb.dao.PoWoInfo;
import com.mind.newjobdb.dao.PurchaseOrderDAO;

public class PurchaseorderAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(PurchaseorderAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		POBean bean = (POBean) form;
		PODTO dto = new PODTO();
		PODao poDao = new PODao();
		DynamicComboCNSWEB dynamiccomboCNSWEB = new DynamicComboCNSWEB();
		bean.setJobId(((request.getParameter("jobId") == null) ? bean
				.getJobId() : request.getParameter("jobId")));
		bean.setPoId(((request.getParameter("poId") == null) ? bean.getPoId()
				: request.getParameter("poId")));
		String tabId = (request.getParameter("type") == null ? "1" : request
				.getParameter("type"));
		request.setAttribute(
				"tabId",
				(request.getParameter("type") == null ? "1" : request
						.getParameter("type")));
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		poDao.getDataForWorkOrder(dto, tabId);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}

		logger.trace("End :(Class : com.mind.docm.actions.ViewManupulationAction | Method : addView)");
		return (mapping.findForward("success"));
	}

	public void setEntityNames(String appId, HttpServletRequest request) {
		String msaName = "MSA";
		String appendixName = "Appendix";
		try {
			msaName = Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					IMdao.getMSAId(appId, getDataSource(request, "ilexnewDB")));
			appendixName = Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"), appId);
		} catch (Exception e) {
			logger.error("setEntityNames(String, HttpServletRequest)", e);

			logger.error("setEntityNames(String, HttpServletRequest)", e);
		}
		if (msaName == null || msaName.equals(""))
			msaName = "MSA";
		if (appendixName == null || appendixName.equals(""))
			appendixName = "Appendix";
		request.setAttribute("msaName", msaName);
		request.setAttribute("appendixName", appendixName);

	}

	public void setPurchaseOrderInfo(PODTO bean, HttpServletRequest request) {

		PoWoInfo poWoInfo = new PoWoInfo();
		poWoInfo.setPoSpecialConditions(PurchaseOrderDAO
				.getPoSpecialConditions(bean));
		poWoInfo.setWoSpecialInstructions(PurchaseOrderDAO
				.getWoSpecialInstructions(bean));
	}
}
