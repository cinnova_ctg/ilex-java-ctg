package com.mind.PO.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.PO.dao.PODao;
import com.mind.PO.formbean.POBean;
import com.mind.bean.docm.DocumentMasterDTO;
import com.mind.bean.mpo.PODTO;
import com.mind.bean.msp.SiteInfo;
import com.mind.bean.newjobdb.EInvoice;
import com.mind.bean.newjobdb.POActivityManager;
import com.mind.bean.newjobdb.PODeliverable;
import com.mind.bean.newjobdb.POFooterInfo;
import com.mind.bean.newjobdb.POResource;
import com.mind.bean.po.FreqUsedDeliverableDTO;
import com.mind.common.EnvironmentSelector;
import com.mind.common.LabelValue;
import com.mind.common.PODeliverableStatusTypes;
import com.mind.common.Util;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.PartnerSOWAssumptiondao;
import com.mind.common.dao.ViewList;
import com.mind.dao.PRM.POWODetaildao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.docm.client.ClientOperator;
import com.mind.docm.dao.DocumentMasterDao;
import com.mind.docm.dao.EntityManager;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.CouldNotMarkDeleteException;
import com.mind.docm.exceptions.CouldNotPhysicallyDeleteException;
import com.mind.docm.exceptions.DocumentIdNotFoundException;
import com.mind.docm.exceptions.DocumentNotExistsException;
import com.mind.docm.utils.DocumentUtility;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.AuthorizedTabOrganizer;
import com.mind.newjobdb.business.ConvertorForPO;
import com.mind.newjobdb.business.POWOTabOrganizer;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.dao.POAuthorizedCostInfo;
import com.mind.newjobdb.dao.POGeneralInfo;
import com.mind.newjobdb.dao.PoWoInfo;
import com.mind.newjobdb.dao.PurchaseOrder;
import com.mind.newjobdb.dao.PurchaseOrderDAO;
import com.mind.newjobdb.dao.PurchaseOrderList;
import com.mind.newjobdb.dao.TicketDAO;
import com.mind.util.HttpClientUtils;
import com.mind.util.WebUtil;

public class POAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(POAction.class);

	public static int saved = 1;

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
			// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Partner",
				"Manage PO/WO", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
			// Security.
		}
		/* Page Security=: End */

		POBean poBean = (POBean) form;
		PODao dao = new PODao();

		String poId = "";
		if (request.getParameter("poValue") != null
				&& !request.getParameter("poValue").equals("")) {
			poId = request.getParameter("poValue");
		} else {
			poId = poBean.getPoId();
		}
		// String refValue = "";

		if (request.getParameter("refValue") == null
				|| request.getParameter("refValue").equals("")) {
			request.setAttribute(
					"refValue",
					dao.isCalendarValueExist(poId,
							getDataSource(request, "ilexnewDB")));
		} else {
			request.setAttribute("refValue", request.getParameter("refValue"));
		}

		if (request.getParameter("poValue") != null
				&& !request.getParameter("poValue").equals("")) {

			try {

				if (request.getParameter("refValue").equals("Delete")) {
					dao.deleteNocCalendarRef(poId,
							getDataSource(request, "ilexnewDB"));
				} else {

					dao.insertNocCalendarRef(poId,
							getDataSource(request, "ilexnewDB"));
				}
				response.setContentType("application/xml; charset=UTF-8");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("success");
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			return null;
		}

		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - In unspecified");
		}

		/*
		 * The following block of code is use to set the latitude and longitude
		 * of partner in request to calculate the distance and duration between
		 * Partner and Site at the time of assignment of a partner to a site.
		 */
		request.setAttribute("partnerLatitude", (request
				.getAttribute("partnerLatitude") == null || ("").equals(request
				.getAttribute("partnerLatitude"))) ? "0.0" : request
				.getAttribute("partnerLatitude").toString());
		request.setAttribute("partnerLongitude", (request
				.getAttribute("partnerLongitude") == null || ("")
				.equals(request.getAttribute("partnerLongitude"))) ? "0.0"
				: request.getAttribute("partnerLongitude").toString());
		request.setAttribute("siteLatitude",
				(request.getAttribute("siteLatitude") == null || ("")
						.equals(request.getAttribute("siteLatitude"))) ? "0.0"
						: request.getAttribute("siteLatitude").toString());
		request.setAttribute("siteLongitude", (request
				.getAttribute("siteLongitude") == null || ("").equals(request
				.getAttribute("siteLongitude"))) ? "0.0" : request
				.getAttribute("siteLongitude").toString());
		request.setAttribute("purchaseOrderId",
				request.getAttribute("purchaseOrderId"));

		poBean.setJobId(((request.getParameter("jobId") == null) ? poBean
				.getJobId() : request.getParameter("jobId")));
		poBean.setAppendixId((request.getParameter("appendix_Id") == null) ? poBean
				.getAppendixId() : request.getParameter("appendix_Id"));
		poBean.setUpdateQuantity(request.getParameter("updateQuantity") != null ? request
				.getParameter("updateQuantity") : poBean.getUpdateQuantity());

		poBean.setPoId(((request.getParameter("poId") == null) ? poBean
				.getPoId() : request.getParameter("poId")));

		poBean.setJobId(((poBean.getJobId() != null) ? poBean.getJobId()
				: (String) request.getAttribute("jobId")));
		poBean.setAppendixId(((poBean.getAppendixId() != null) ? poBean
				.getAppendixId() : (String) request.getAttribute("appendixId")));

		if (poBean.getJobId() != null && !poBean.getJobId().equals("")) {
			if (TicketDAO.isOnSite(Integer.parseInt(poBean.getJobId()),
					getDataSource(request, "ilexnewDB"))) {
				request.setAttribute("onsite", "onsite");
			}
		}
		if (PurchaseOrder.isCopiedFromMPO(poBean.getPoId(),
				getDataSource(request, "ilexnewDB"))) {
			request.setAttribute("copiedFromMpo", "y");
		}
		poBean.setPoId(((poBean.getPoId() != null) ? poBean.getPoId()
				: (String) request.getAttribute("poId")));
		poBean.setOverrideMinumtemanCostChecked(PurchaseOrderList
				.buildNewPOMinutemanOverrideCostValue(poBean.getPoId(),
						getDataSource(request, "ilexnewDB")));
		request.setAttribute(
				"tabId",
				(request.getParameter("tabId") == null ? "0" : request
						.getParameter("tabId")));

		String tabId = request.getParameter("tabId");
		poBean.setContactPoc(POWODetaildao.getPartnerAssignedPOC(
				PODao.getPartnerId(poBean.getPoId(),
						getDataSource(request, "ilexnewDB")), poBean.getPoId(),
				getDataSource(request, "ilexnewDB")));
		setFormFromObject(request, poBean, tabId);

		if (tabId == null) {
			tabId = "0";
		}

		String mode = request.getParameter("mode");

		if (mode != null) {
			request.setAttribute("mode", "view");
		}
		if (tabId != null && tabId.trim().equalsIgnoreCase("2")) {
			String url = "/DocumentManagementAction.do?type=" + tabId
					+ "&poId=" + poBean.getPoId() + "&appendix_Id="
					+ poBean.getAppendixId() + "&jobId=" + poBean.getJobId();
			if (logger.isDebugEnabled()) {
				logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - url"
						+ url);
			}
			ActionForward fwd = new ActionForward();
			fwd.setPath(url);
			return fwd;
		}

		poBean.setIsNewPO("");
		return (mapping.findForward("success"));
	}

	public void setFormFromObject(HttpServletRequest request, POBean poBean,
			String tabId) {
		PODao dao = new PODao();
		PODTO dto = new PODTO();
		String appendixType = ViewList.getAppendixtypedesc(
				poBean.getAppendixId(), getDataSource(request, "ilexnewDB"));
		poBean.setAppendixType(appendixType);
		setFooterInfo(poBean, request);
		populateCombo(poBean.getPoId(), getDataSource(request, "ilexnewDB"),
				poBean);
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");

		PurchaseOrder purchaseOrder = PODao.getPurchaseOrder(poBean.getPoId(),
				getDataSource(request, "ilexnewDB"), appendixType);
		request.setAttribute("poStatus", purchaseOrder.getPowoInfo()
				.getPoGeneralInfo().getPoStatusName());
		request.setAttribute("poType", purchaseOrder.getPowoInfo()
				.getPoGeneralInfo().getPoType());

		POGeneralInfo poGeneralInfo = purchaseOrder.getPowoInfo()
				.getPoGeneralInfo();
		try {
			BeanUtils.copyProperties(dto, poBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		if (tabId != null && tabId.trim().equalsIgnoreCase("0")) {
			ArrayList<POResource> listOfPOResource = PODao.getActivityList(dto,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("actvityList", listOfPOResource);
			request.setAttribute("listSize", listOfPOResource.size());
			if (logger.isDebugEnabled()) {
				logger.debug("setFormFromObject(HttpServletRequest, POBean, String) - listOfPOResource"
						+ listOfPOResource);
			}
			request.setAttribute("tabId", tabId);

			POAuthorizedCostInfo poAuthorizedCostInfo = new POAuthorizedCostInfo();
			POActivityManager poActivityManager = PurchaseOrderDAO
					.getPOResources(poBean.getPoId(),
							getDataSource(request, "ilexnewDB"));
			poAuthorizedCostInfo.setPoActivityManager(poActivityManager);
			poAuthorizedCostInfo.setPoGeneralInfo(poGeneralInfo);
			purchaseOrder.setPoAuthorizedCostInfo(poAuthorizedCostInfo);

			purchaseOrder
					.getPoAuthorizedCostInfo()
					.getPoActivityManager()
					.setMpoTotalCost(
							PODao.getMPOTotalCost(poBean.getPoId(),
									getDataSource(request, "ilexnewDB")));

			Float[] poAuthorizedTotalCost = PODao.getPOAuthorizedTotalCost(
					poBean.getPoId(), getDataSource(request, "ilexnewDB"));
			purchaseOrder.getPoAuthorizedCostInfo().getPoActivityManager()
					.setAuthorizedCostInput(poAuthorizedTotalCost[0]);
			request.setAttribute("poAuthorizedTotalCost",
					poAuthorizedTotalCost[0] + "");
			request.setAttribute("poAuthorizedTotalCostDiff",
					poAuthorizedTotalCost[1] + "");
		}
		try {
			BeanUtils.copyProperties(dto, poBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		if (tabId != null && tabId.trim().equalsIgnoreCase("1")) {
			request.setAttribute("tabId", tabId);
			dao.getDataForWorkOrder(dto, tabId);
			request.setAttribute("mode", "view");
		}
		try {
			BeanUtils.copyProperties(poBean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		String clicked = request.getParameter("clicked");

		if (tabId != null && tabId.trim().equalsIgnoreCase("3")) {
			if (clicked != null && (!clicked.equals("addFreqDel"))) {
				request.setAttribute("clicked", clicked);
				String devId = "0";
				if (request.getParameter("devId") != null) {
					devId = request.getParameter("devId");
				}
				dao.generalfetchData(dto, tabId, devId);
				try {
					BeanUtils.copyProperties(poBean, dto);
				} catch (IllegalAccessException e) {
					logger.error(e);
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					logger.error(e);
					e.printStackTrace();
				}
			} else {
				Map<String, Object> map = new HashMap<String, Object>();
				dao.getGeneralPODetailListForDelivrables(map, dto, tabId);
				WebUtil.copyMapToRequest(request, map);
				ArrayList<ArrayList<String>> deliverables = (ArrayList<ArrayList<String>>) map
						.get("devList");

				List<FreqUsedDeliverableDTO> freqUsedDeliverables = PODao
						.getFreqUsedDeliverable(getDataSource(request,
								"ilexnewDB"));

				List<String> selectedFreqUsedDeliverables = new ArrayList<String>();

				for (ArrayList<String> deliverable : deliverables) {
					for (FreqUsedDeliverableDTO freqUsedDeliverableDTO : freqUsedDeliverables) {

						if ((freqUsedDeliverableDTO.getDeliverableTitle()
								.equals(deliverable.get(2)))
								&& (freqUsedDeliverableDTO
										.getDeliverableDescription()
										.equals(deliverable.get(5)))) {
							String selectedFreqUsedDeliverable = deliverable
									.get(2);

							selectedFreqUsedDeliverables
									.add(selectedFreqUsedDeliverable);
							break;
						}

					}

				}

				poBean.setSelectedFreqUsedDeliverables(selectedFreqUsedDeliverables
						.toArray(new String[] {}));
				request.setAttribute("selectedFreqUsedDeliverables",
						selectedFreqUsedDeliverables.toArray(new String[] {}));
				request.setAttribute("freqUsedDeliverables",
						freqUsedDeliverables);

			}

			request.setAttribute("mode", "view");
		}
		if (tabId != null && tabId.trim().equalsIgnoreCase("5")) {
			request.setAttribute("tabId", tabId);
			EInvoice eInvoice = setEInvoiceInfo(poBean.getPoId(),
					getDataSource(request, "ilexnewDB"));
			purchaseOrder.setEInvoice(eInvoice);
			request.setAttribute("mode", "view");
		}
		if (poBean.getJobId() != null) {
			SiteInfo siteInfo = JobDAO.getSiteInfo(poBean.getJobId(),
					getDataSource(request, "ilexnewDB"));
			purchaseOrder.setSiteInfo(siteInfo);
		}
		try {
			BeanUtils.copyProperties(dto, poBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		ConvertorForPO.setFormFromPurchaseOrder(purchaseOrder, dto);
		try {
			BeanUtils.copyProperties(poBean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		setEditableValue(poBean, getDataSource(request, "ilexnewDB"));
		if (poBean.getJobId() != null
				&& !poBean.getJobId().trim().equalsIgnoreCase("")) {
			Map<String, Object> map = DatabaseUtilityDao.setMenu(
					poBean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		if (request.getParameter("isBuild") == null && saved == 1) {
			request.setAttribute("save", "save");
		}
	}

	/**
	 * Fill eInvoce info.
	 */
	private EInvoice setEInvoiceInfo(String poId, DataSource ds) {
		EInvoice eInvoice = new EInvoice();
		eInvoice.setEInvoiceSummary(PODao.getEInvoiceSummary(poId, ds));
		eInvoice.setEInvoiceItems(PODao.getEInvoiceItems(poId, ds));
		return eInvoice;
	}

	public void setFooterInfo(POBean poBean, HttpServletRequest request) {
		POFooterInfo poFooterInfo = PurchaseOrderDAO.getPoFooterInfo(
				poBean.getPoId(), getDataSource(request, "ilexnewDB"));
		poBean.setPoFooterCreateDate(poFooterInfo.getPoFooterCreateDate());
		poBean.setPoFooterChangeDate(poFooterInfo.getPoFooterChangeDate());
		poBean.setPoFooterSource(poFooterInfo.getPoFooterSource());
		poBean.setPoFooterCreateBy(poFooterInfo.getPoFooterCreateBy());
		poBean.setPoFooterChangeBy(poFooterInfo.getPoFooterChangeBy());
	}

	public ActionForward buildPO(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		POBean poBean = (POBean) form;
		logger.trace("Start :(Class : com.mind.docm.actions.ViewManupulationAction | Method : addView)");
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		request.setAttribute("tabId", request.getParameter("tabId"));

		String poId = PurchaseOrderList.buildNewPO(poBean.getJobId(),
				loginuserid, "Add", "", "", "", "",
				getDataSource(request, "ilexnewDB"));

		// Added 3 default deliverables when new PO Created starts
		addDefaultDeliverables(loginuserid, poId,
				getDataSource(request, "ilexnewDB"));
		// Added 3 default deliverables when new PO Created end

		String minutemanOverrideCost = PurchaseOrderList
				.buildNewPOMinutemanOverrideCostValue(poId,
						getDataSource(request, "ilexnewDB"));

		poBean.setOverrideMinumtemanCostChecked(minutemanOverrideCost);

		if (poId != null && !poId.equals("") && !poId.equals("0")) {
			PurchaseOrderDAO.updatePurchaseOrder(poId,
					getDataSource(request, "ilexnewDB"));
		}
		if (logger.isDebugEnabled()) {
			logger.debug("buildPO(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - poUId-------"
					+ poId);
		}

		if (request.getParameter("isNewPO") != null
				&& request.getParameter("isNewPO").equals("Y")) {
			poBean.setIsNewPO("Y");
		}
		poBean.setPoId(String.valueOf(poId));
		populateCombo(poBean.getPoId(), getDataSource(request, "ilexnewDB"),
				poBean);

		String appendixType = ViewList.getAppendixtypedesc(
				poBean.getAppendixId(), getDataSource(request, "ilexnewDB"));
		PurchaseOrder purchaseOrder = PODao.getPurchaseOrder(poBean.getPoId(),
				getDataSource(request, "ilexnewDB"), appendixType);
		PODTO dto = new PODTO();
		try {
			BeanUtils.copyProperties(dto, poBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		ConvertorForPO.setFormFromPurchaseOrder(purchaseOrder, dto);
		try {
			BeanUtils.copyProperties(poBean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}

		if (poBean.getPoId() != null
				&& !poBean.getPoId().trim().equalsIgnoreCase("")
				&& !poBean.getPoId().trim().equalsIgnoreCase("-1")) {
			ArrayList<POResource> listOfPOResource = PODao.getActivityList(dto,
					getDataSource(request, "ilexnewDB"));
			try {
				BeanUtils.copyProperties(poBean, dto);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			request.setAttribute("actvityList", listOfPOResource);
			request.setAttribute("listSize", listOfPOResource.size());
		}
		if (poBean.getJobId() != null
				&& !poBean.getJobId().trim().equalsIgnoreCase("")) {
			Map<String, Object> map = DatabaseUtilityDao.setMenu(
					poBean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		if (request.getParameter("isBuild") != null) {
			saved = 0;
		}
		String mode = request.getParameter("mode");
		if (mode != null) {
			request.setAttribute("mode", "view");
		}
		setEditableValue(poBean, getDataSource(request, "ilexnewDB"));
		setFooterInfo(poBean, request);

		poBean.setAppendixType(ViewList.getAppendixtypedesc(
				poBean.getAppendixId(), getDataSource(request, "ilexnewDB")));
		// added default values for PO Type and Terms
		if (StringUtils.isEmpty(poBean.getPoType())) {
			poBean.setPoType("Minuteman");
		}
		if (StringUtils.isEmpty(poBean.getTerms())) {
			poBean.setTerms("8");
		}
		return (mapping.findForward("success"));
	}

	public void addDefaultDeliverables(String loginuserid, String poId,
			DataSource ds) {
		String[] defaultDeliverables = { "Before Work", "After Work",
				"Work Order", "Equipment Cabinet/Rack" };

		String[] defaultDeliverablesDescription = {
				"Picture of work area before work has begun.",
				"Picture of work area after work has been completed.",
				"Picture of work order, signed by the manager on duty.",
				"This photo should be close enough to read the brand name on the hardware but wide enough to capture the entire cabinet or concentration of hardware (even if it is in a pile or a nest)" };
		int deliverableCount = 0;
		for (String deliverable : defaultDeliverables) {
			PurchaseOrderDAO.insertDefaultPODeliverable(poId, deliverable,
					defaultDeliverablesDescription[deliverableCount],
					loginuserid, ds);
			deliverableCount++;
		}
	}

	public ActionForward Save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);
		POBean poBean = (POBean) form;
		// In the case of Senior Project Manager
		if ((Boolean) session.getAttribute("isUserRoleSeniorPM")) {
			if (null == poBean.getOverrideMinumtemanCostChecked()) {
				poBean.setOverrideMinumtemanCostChecked("0");
			}
		} else {
			// In the case of Non Senior Project Manager
			poBean.setOverrideMinumtemanCostChecked(poBean
					.getOverrideMinumtemanCostCheckedHidden());
		}

		logger.trace("Start :(Class : com.mind.docm.actions.ViewManupulationAction | Method : addView)");
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		PODTO dto = new PODTO();
		poBean.setAppendixType(ViewList.getAppendixtypedesc(
				poBean.getAppendixId(), getDataSource(request, "ilexnewDB")));
		if (poBean.getPoId() != null
				&& !poBean.getPoId().trim().equalsIgnoreCase("")
				&& !poBean.getPoId().trim().equalsIgnoreCase("-1")) {
			try {
				BeanUtils.copyProperties(dto, poBean);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}

			POAuthorizedCostInfo poAuthorizedCostInfo = AuthorizedTabOrganizer
					.setAuthorizedTabFromForm(dto);
			try {
				BeanUtils.copyProperties(poBean, dto);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			PurchaseOrder purchaseOrder = new PurchaseOrder();
			purchaseOrder.setPoAuthorizedCostInfo(poAuthorizedCostInfo);
			purchaseOrder.setPoId(poBean.getPoId());
			purchaseOrder.setOverrideMinutemanCostChecked(poBean
					.getOverrideMinumtemanCostChecked());

			String changedPOResources = PurchaseOrderDAO
					.getChangedPOResourceIds(purchaseOrder, loginuserid,
							getDataSource(request, "ilexnewDB"));
			PurchaseOrder.savePOAuthorizedCostInfo(purchaseOrder, loginuserid,
					getDataSource(request, "ilexnewDB"), changedPOResources,
					poBean.getAppendixType());
			poBean.setSave("save");

			if (poBean.getIsNewPO() != null && poBean.getIsNewPO().equals("Y")) {

				String sowAssumption = getDefaultSOWAssumption(
						poBean.getPoId(), getDataSource(request, "ilexnewDB"));
				poBean.setWoNOA(sowAssumption);
				String splInst = getSplInst(poBean.getPoId(), loginuserid,
						poBean.getJobId(), poBean.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
				poBean.setWoSI(splInst);
				String condition = getSplCondition(poBean.getAppendixId(),
						getDataSource(request, "ilexnewDB"));
				poBean.setSpecial_condition(condition);
				try {
					BeanUtils.copyProperties(dto, poBean);
				} catch (IllegalAccessException e) {
					logger.error(e);
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					logger.error(e);
					e.printStackTrace();
				}
				PoWoInfo poWoInfo = POWOTabOrganizer.setPOWOTabFromForm(dto);
				try {
					BeanUtils.copyProperties(poBean, dto);
				} catch (IllegalAccessException e) {
					logger.error(e);
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					logger.error(e);
					e.printStackTrace();
				}
				purchaseOrder.setPowoInfo(poWoInfo);

				PurchaseOrder.savePurchaseWorkOrder(purchaseOrder, loginuserid,
						getDataSource(request, "ilexnewDB"));
				poBean.setIsNewPO("");

			}
			saved = 1;
			if (saved == 1) {
				request.setAttribute("save", "save");
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Save(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - in save tabId= "
					+ request.getParameter("tabId"));
		}
		request.setAttribute("tabId", request.getParameter("tabId"));
		populateCombo(poBean.getPoId(), getDataSource(request, "ilexnewDB"),
				poBean);
		if (poBean.getJobId() != null
				&& !poBean.getJobId().trim().equalsIgnoreCase("")) {
			Map<String, Object> map = DatabaseUtilityDao.setMenu(
					poBean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		String mode = request.getParameter("mode");
		request.setAttribute("POStatus", "Draft");
		if (mode != null) {
			request.setAttribute("mode", "view");
		}

		return (mapping.findForward("savesuccess"));
	}

	public void populateCombo(String poId, DataSource ds, POBean poBean) {
		ArrayList terms = POWODetaildao.getPWOdetailterms(ds);
		ArrayList potype = POWODetaildao.getPWOdetailtype(ds);
		ArrayList masterpotype = POWODetaildao.getPWOdetailmasterpotype(ds);
		ArrayList pVSJustificationList = POWODetaildao
				.getPVSJustificationList(ds);
		ArrayList geographicConstraintList = POWODetaildao
				.getGeographicConstraintList(ds);
		poBean.setPartnerPOId(PODao.getPartnerId(poId, ds));

		if (logger.isDebugEnabled()) {
			logger.debug("populateCombo(String, DataSource, POBean) - ppoid"
					+ poBean.getPartnerPOId());
		}

		ArrayList partnerPoc = POWODetaildao.getPartnerPOC(
				PODao.getPartnerId(poId, ds), ds);

		if (poBean.getPartnerPOId() != null
				&& !poBean.getPartnerPOId().equals("")
				&& !poBean.getPartnerPOId().equals("0")) {
			poBean.setPartnerType(POWODetaildao.getPartnerType(
					poBean.getPartnerPOId(), ds));
			if (!poBean.getPartnerType().equals("")
					&& !poBean.getPartnerType().equals("M")) {
			}
			// Add condition to remove minute from list when potype "S"
			if (poBean.getPartnerType().equals("S")) {
				potype.remove(1);
			}

		}

		ArrayList<LabelValue> hourList = Util.getHours();
		ArrayList<LabelValue> minuteList = Util.getMinutes();
		poBean.setPoTerms(terms);
		poBean.setPo(potype);
		poBean.setMasterpOItem(masterpotype);
		poBean.setPartnerPoc(partnerPoc);
		poBean.setHourList(hourList);
		poBean.setMinuteList(minuteList);
		poBean.setpVSJustificationList(pVSJustificationList);
		poBean.setGeographicConstraintList(geographicConstraintList);
		List<LabelValue> dcCoreCompetancy = new ArrayList<>();
		dcCoreCompetancy = Pvsdao.getCoreCompetancy(ds);
		poBean.setDcCoreCompetancy(dcCoreCompetancy);

	}

	public ActionForward savePO(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		POBean poBean = (POBean) form;
		PODao poDao = new PODao();
		PODTO dto = new PODTO();
		poBean.setJobId(((request.getParameter("jobId") == null) ? poBean
				.getJobId() : request.getParameter("jobId")));
		poBean.setAppendixId((request.getParameter("appendix_Id") == null) ? poBean
				.getAppendixId() : request.getParameter("appendix_Id"));

		poBean.setPoId(((request.getParameter("poId") == null) ? poBean
				.getPoId() : request.getParameter("poId")));
		poBean.setPoId(((poBean.getPoId() != null) ? poBean.getPoId()
				: (String) request.getAttribute("poId")));
		request.setAttribute("tabId", (request.getParameter("tabId")));
		String tabId = request.getParameter("tabId");
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");

		if (tabId != null && tabId.trim().equals("1")) {
			if (logger.isDebugEnabled()) {
				logger.debug("savePO(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - inside savePO");
			}
			try {
				BeanUtils.copyProperties(dto, poBean);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			PoWoInfo poWoInfo = POWOTabOrganizer.setPOWOTabFromForm(dto);
			try {
				BeanUtils.copyProperties(poBean, dto);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			if (logger.isDebugEnabled()) {
				logger.debug("savePO(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - poWoInfo.getRequiredEquipments()::: "
						+ poWoInfo.getRequiredEquipments());
			}
			PurchaseOrder purchaseOrder = new PurchaseOrder();
			purchaseOrder.setPowoInfo(poWoInfo);
			purchaseOrder.setPoId(poBean.getPoId());
			PurchaseOrder.savePurchaseWorkOrder(purchaseOrder, loginuserid,
					getDataSource(request, "ilexnewDB"));

		}
		if (PurchaseOrder.isCopiedFromMPO(poBean.getPoId(),
				getDataSource(request, "ilexnewDB"))) {
			request.setAttribute("copiedFromMpo", "y");
		}
		String clicked = "";
		if (request.getParameter("clicked") != null) {
			clicked = request.getParameter("clicked");
		}
		try {
			BeanUtils.copyProperties(dto, poBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		if (tabId != null && tabId.trim().equals("3")) {
			if (clicked.equals("")) {
				PurchaseOrderDAO.savePODeliverables(dto, loginuserid,
						getDataSource(request, "ilexnewDB"));
			}
			if (clicked.equals("addFreqDel")) {

				addFreqUsedDeliverable(request, poBean, poDao, "3", loginuserid);

			}

			try {
				BeanUtils.copyProperties(poBean, dto);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
		}
		setFormFromObject(request, poBean, tabId);

		return (mapping.findForward("success"));

	}

	private void addFreqUsedDeliverable(HttpServletRequest request,
			POBean poBean, PODao dao, String tabId, String loginuserid) {
		List<FreqUsedDeliverableDTO> freqUsedDeliverables = PODao
				.getFreqUsedDeliverable(getDataSource(request, "ilexnewDB"));

		List<FreqUsedDeliverableDTO> checkedFreqUsedDeliverables = new ArrayList<FreqUsedDeliverableDTO>();
		String[] checkedDeliverables = poBean.getSelectedFreqUsedDeliverables();
		if (checkedDeliverables != null) {
			for (String checkedDeliverable : checkedDeliverables) {

				for (FreqUsedDeliverableDTO freqUsedDeliverableDTO : freqUsedDeliverables) {

					if (freqUsedDeliverableDTO.getDeliverableTitle().equals(
							checkedDeliverable)) {
						checkedFreqUsedDeliverables.add(freqUsedDeliverableDTO);
						break;
					}

				}

			}
		}

		List<FreqUsedDeliverableDTO> unCheckedFredUsedDeliverables = getUnCheckedFredUsedDeliverables(
				checkedFreqUsedDeliverables, freqUsedDeliverables);

		List<FreqUsedDeliverableDTO> deliverablesList = getDeliverableList(
				poBean, dao, tabId);

		for (FreqUsedDeliverableDTO deliverable : deliverablesList) {
			if (unCheckedFredUsedDeliverables.contains(deliverable)) {
				PurchaseOrderDAO.deletePODeliverable(
						deliverable.getDeliverableId(),
						getDataSource(request, "ilexnewDB"));

			}
		}

		for (FreqUsedDeliverableDTO checkedDeliverable : checkedFreqUsedDeliverables) {
			if (!deliverablesList.contains(checkedDeliverable)) {
				PODTO dto = new PODTO();
				dto.setPoId(poBean.getPoId());
				dto.setDevDescription(checkedDeliverable
						.getDeliverableDescription());
				dto.setDevTitle(checkedDeliverable.getDeliverableTitle());
				dto.setDevType("Customer");
				dto.setDevFormat("Image");
				dto.setMobileUploadAllowed('y');
				PurchaseOrderDAO.savePODeliverables(dto, loginuserid,
						getDataSource(request, "ilexnewDB"));

			}

		}

	}

	private List<FreqUsedDeliverableDTO> getDeliverableList(POBean poBean,
			PODao dao, String tabId) {
		Map<String, Object> map = new HashMap<String, Object>();
		PODTO dto = new PODTO();
		try {
			BeanUtils.copyProperties(dto, poBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}

		dao.getGeneralPODetailListForDelivrables(map, dto, tabId);

		ArrayList<ArrayList<String>> deliverables = (ArrayList<ArrayList<String>>) map
				.get("devList");
		List<FreqUsedDeliverableDTO> deliverablesList = new ArrayList<FreqUsedDeliverableDTO>();

		for (ArrayList<String> deliverable : deliverables) {
			FreqUsedDeliverableDTO freqUsedDeliverableDTO = new FreqUsedDeliverableDTO();
			freqUsedDeliverableDTO
					.setDeliverableDescription(deliverable.get(5));
			freqUsedDeliverableDTO.setDeliverableTitle(deliverable.get(2));
			freqUsedDeliverableDTO.setDeliverableId(deliverable.get(1));
			deliverablesList.add(freqUsedDeliverableDTO);
		}

		return deliverablesList;
	}

	public List<FreqUsedDeliverableDTO> getUnCheckedFredUsedDeliverables(
			List<FreqUsedDeliverableDTO> checkedfreqUsedDeliverables,
			List<FreqUsedDeliverableDTO> freqUsedDeliverables) {

		List<FreqUsedDeliverableDTO> unCheckedFredUsedDeliverables = new ArrayList<FreqUsedDeliverableDTO>(
				freqUsedDeliverables);

		unCheckedFredUsedDeliverables.removeAll(checkedfreqUsedDeliverables);
		return unCheckedFredUsedDeliverables;
	}

	/**
	 * This method is used to saved Quantity for Fixed Price PO.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward SaveFixedPriceQuantity(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		POBean poBean = (POBean) form;
		PODTO dto = new PODTO();

		request.setAttribute("tabId", (request.getParameter("tabId")));
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		poBean.setAppendixType(ViewList.getAppendixtypedesc(
				poBean.getAppendixId(), getDataSource(request, "ilexnewDB")));
		try {
			BeanUtils.copyProperties(dto, poBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}

		POAuthorizedCostInfo poAuthorizedCostInfo = AuthorizedTabOrganizer
				.setAuthorizedTabFromForm(dto);
		try {
			BeanUtils.copyProperties(poBean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		purchaseOrder.setPoAuthorizedCostInfo(poAuthorizedCostInfo);
		purchaseOrder.setPoId(poBean.getPoId());
		PurchaseOrder.savePurchaseWorkOrderFP(purchaseOrder, loginuserid,
				getDataSource(request, "ilexnewDB"));

		populateCombo(poBean.getPoId(), getDataSource(request, "ilexnewDB"),
				poBean);
		if (poBean.getJobId() != null
				&& !poBean.getJobId().trim().equalsIgnoreCase("")) {
			Map<String, Object> map = DatabaseUtilityDao.setMenu(
					poBean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}

		if (poBean.getIsNewPO() != null && poBean.getIsNewPO().equals("Y")) {

			String sowAssumption = getDefaultSOWAssumption(poBean.getPoId(),
					getDataSource(request, "ilexnewDB"));
			poBean.setWoNOA(sowAssumption);
			String splInst = getSplInst(poBean.getPoId(), loginuserid,
					poBean.getJobId(), poBean.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			poBean.setWoSI(splInst);
			String condition = getSplCondition(poBean.getAppendixId(),
					getDataSource(request, "ilexnewDB"));
			poBean.setSpecial_condition(condition);
			try {
				BeanUtils.copyProperties(dto, poBean);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}

			PoWoInfo poWoInfo = POWOTabOrganizer.setPOWOTabFromForm(dto);
			try {
				BeanUtils.copyProperties(poBean, dto);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			purchaseOrder.setPowoInfo(poWoInfo);

			PurchaseOrder.savePurchaseWorkOrder(purchaseOrder, loginuserid,
					getDataSource(request, "ilexnewDB"));
			poBean.setIsNewPO("");

		}
		String mode = request.getParameter("mode");
		request.setAttribute("POStatus", "Draft");
		if (mode != null) {
			request.setAttribute("mode", "view");
		}

		return (mapping.findForward("savesuccess"));

	}

	public ActionForward savePODoc(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		POBean poBean = (POBean) form;
		PODTO dto = new PODTO();
		poBean.setJobId(((request.getParameter("jobId") == null) ? poBean
				.getJobId() : request.getParameter("jobId")));
		poBean.setAppendixId((request.getParameter("appendix_Id") == null) ? poBean
				.getAppendixId() : request.getParameter("appendix_Id"));

		poBean.setPoId(((request.getParameter("poId") == null) ? poBean
				.getPoId() : request.getParameter("poId")));
		poBean.setPoId(((poBean.getPoId() != null) ? poBean.getPoId()
				: (String) request.getAttribute("poId")));
		request.setAttribute("tabId", (request.getParameter("tabId")));
		String tabId = request.getParameter("tabId");
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		String clicked = "";
		if (request.getParameter("clicked") != null) {
			clicked = request.getParameter("clicked");
		}
		if (tabId != null && tabId.trim().equals("3")) {
			if (clicked.equals("")) {
				try {
					BeanUtils.copyProperties(dto, poBean);
				} catch (IllegalAccessException e) {
					logger.error(e);
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					logger.error(e);
					e.printStackTrace();
				}
			}
			PurchaseOrderDAO.savePODeliverablesDocs(dto, loginuserid,
					getDataSource(request, "ilexnewDB"));
			try {
				BeanUtils.copyProperties(poBean, dto);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
		}
		setFormFromObject(request, poBean, tabId);

		return (mapping.findForward("success"));

	}

	public ActionForward sowAssumption(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String poId = request.getParameter("poId");
		String check = request.getParameter("check");
		String jobId = request.getParameter("jobId");
		String appendixId = request.getParameter("appendixId");
		String result = "";

		/* Get default sp SOW/Assumption */
		if (check != null && check.equals("sowAssumption")) {
			StringBuffer sow = new StringBuffer();
			StringBuffer assumption = new StringBuffer();
			String[] resourceIds = PODao.getResouceIdByPO(poId,
					getDataSource(request, "ilexnewDB"));
			String tempSowAssumption = "";

			for (int i = 0; i < resourceIds.length; i++) {
				tempSowAssumption = POWODetaildao.getSowAssumption(
						resourceIds[i], "sow",
						getDataSource(request, "ilexnewDB"));
				if (!tempSowAssumption.equals("")) {
					sow.append(tempSowAssumption + "\n");
				}
			}

			for (int i = 0; i < resourceIds.length; i++) {
				tempSowAssumption = POWODetaildao
						.getSowAssumption(resourceIds[i], "",
								getDataSource(request, "ilexnewDB"));
				if (!tempSowAssumption.equals("")) {
					assumption.append(tempSowAssumption + "\n");
				}
			}

			String sowLast = "";
			String assumpLast = "";
			if (!sow.toString().trim().equals("")) {
				sowLast = "Statement of Work:\n\n" + sow.toString().trim();
			}
			if (!assumption.toString().trim().equals("")) {
				assumpLast = "\nAssumptions:\n\n"
						+ assumption.toString().trim();
			}
			result = (sowLast + "\n" + assumpLast).trim();
		} /* Get default sp instruction */
		else if (check != null && check.equals("specialIns")) {
			String specialinstruction = PartnerSOWAssumptiondao
					.getDefaultPartnerSOWAssumption(appendixId, "prm_app",
							"si", "partner",
							getDataSource(request, "ilexnewDB"));
			result = specialinstruction.trim();
		} else if (check != null && check.equals("spclCond")) {
			result = getSplCondition(appendixId,
					getDataSource(request, "ilexnewDB"));
		}
		try {
			response.setContentType("text/html");
			response.getWriter().write(result);
			return null;
		} catch (Exception e) {
			logger.error(
					"sowAssumption(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"sowAssumption(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}
		return null;
	}

	public ActionForward DeleteDev(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		POBean poBean = (POBean) form;
		String documentId = null;
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		String tabId = request.getParameter("tabId");
		request.setAttribute("tabId", tabId);

		try {
			documentId = EntityManager.getDocumentId(poBean.getDevId(),
					"Deliverables");
		} catch (DocumentIdNotFoundException e1) {
			e1.printStackTrace();
		}

		if (!documentId.equals("-1")) {
			try {
				ClientOperator.deleteDocument(documentId, false, loginuserid);
			} catch (DocumentNotExistsException e) {
				e.printStackTrace();
			} catch (CouldNotPhysicallyDeleteException e) {
				e.printStackTrace();
			} catch (CouldNotCheckDocumentException e) {
				e.printStackTrace();
			} catch (CouldNotMarkDeleteException e) {
				e.printStackTrace();
			}
		}
		PODTO dto = new PODTO();
		try {
			BeanUtils.copyProperties(dto, poBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		PurchaseOrderDAO.deletePODeliverable(dto, loginuserid,
				getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(dto, poBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		setFormFromObject(request, poBean, tabId);
		request.setAttribute("mode", "view");
		return (mapping.findForward("success"));
	}

	public ActionForward poActionAjax(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("Pragma", "No-Cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", 0);
		if (logger.isDebugEnabled()) {
			logger.debug("poActionAjax(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - actionType:  "
					+ request.getParameter("actionType"));
		}

		if (logger.isDebugEnabled()) {
			logger.debug("poActionAjax(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - devId:  "
					+ request.getParameter("devId"));
		}

		String actionType = request.getParameter("actionType");
		String devId = request.getParameter("devId");
		String poId = request.getParameter("poId");
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		if (actionType != null && actionType.equals("Accept")) {
			PODao.changeDeliverableStatus(devId,
					PODeliverableStatusTypes.STATUS_ACCEPTED + "", poId,
					loginuserid, getDataSource(request, "ilexnewDB"));
			uploadDeliverableToCustomerCare(devId, poId);
		}
		if (actionType != null && actionType.equals("Reject")) {
			PODao.changeDeliverableStatus(devId,
					PODeliverableStatusTypes.STATUS_REJECTED + "", poId,
					loginuserid, getDataSource(request, "ilexnewDB"));
		}

		PODeliverable poDeliverable = new PODeliverable();
		poDeliverable = PODao.getPODeliverable(poId, devId,
				getDataSource(request, "ilexnewDB"));
		StringBuffer sb = new StringBuffer();

		try {
			sb.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
			sb.append("<devInfo generated=\"ulq\">\n");

			JobDAO.writeNode("devTitle", Util.filter(poDeliverable.getTitle()),
					sb);
			JobDAO.writeNode("devType", poDeliverable.getType(), sb);
			JobDAO.writeNode("devFormat", poDeliverable.getFormat(), sb);
			JobDAO.writeNode("devDesc", poDeliverable.getDescription(), sb);
			JobDAO.writeNode("devStatus", poDeliverable.getStatus(), sb);
			JobDAO.writeNode("devDate", poDeliverable.getDate(), sb);
			JobDAO.writeNode("docId", poDeliverable.getDocId(), sb);

			JobDAO.writeNode("poId", poId + "", sb);

			sb.append("</devInfo>");
			if (logger.isDebugEnabled()) {
				logger.debug("poActionAjax(ActionMapping, ActionForm, HttpServletRquest, HttpServletResponse) - generated xml: "
						+ sb.toString());
			}
			response.setContentType("application/xml");
			response.getWriter().write(sb.toString());
		} catch (Exception e) {
			logger.error(
					"poActionAjax(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}

		return null;
	}

	public static void uploadDeliverableToCustomerCare(String devId, String poId) {
		PODTO poDTO = PODao.getDocumentDetails(poId, devId);
		String fileId = DocumentUtility.getfileIdFromDoc(poDTO.getDoc_id());

		String jobId = poDTO.getJobId();
		String documentTitle = poDTO.getDevTitle();
		byte[] deliverableFile = DocumentMasterDao.openFile(fileId);
		DocumentMasterDTO dto = new DocumentMasterDTO();
		DocumentMasterDao docMasterDao = new DocumentMasterDao();

		docMasterDao.getFileData(fileId, dto);

		String filename = dto.getFilename();

		if (filename.contains(".")) {
			filename = filename.substring(0, filename.indexOf("."));
		}

		filename = filename + "." + dto.getMIME();

		String baseURL = EnvironmentSelector
				.getBundleString("po.deliverable.upload.url");
		List<SimpleEntry<String, String>> requestParams = new ArrayList<SimpleEntry<String, String>>();
		SimpleEntry<String, String> jobIdParam = new SimpleEntry<String, String>(
				"il_ji_lm_js_id", jobId);
		SimpleEntry<String, String> docNameParam = new SimpleEntry<String, String>(
				"doc_name", documentTitle);
		requestParams.add(jobIdParam);
		requestParams.add(docNameParam);

		try {
			HttpClientUtils.uploadFile(baseURL, requestParams, deliverableFile,
					"doc", filename);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public ActionForward getStandarContactLine(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		response.setHeader("Pragma", "No-Cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", 0);
		if (logger.isDebugEnabled()) {
			logger.debug("getStandarContactLine(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - actionType:  "
					+ request.getParameter("actionType"));
		}

		String loginUserId = (String) request.getSession(true).getAttribute(
				"userid");
		String standardContactLine = PODao.getStandardContactLine(loginUserId,
				getDataSource(request, "ilexnewDB"));
		if (logger.isDebugEnabled()) {
			logger.debug("getStandarContactLine(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - standardContactLine: "
					+ standardContactLine);
		}
		StringBuffer sb = new StringBuffer();
		try {
			sb.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
			sb.append("<standardConLine generated=\"ulq\">\n");
			JobDAO.writeNode("standardContactLine", "\n\n"
					+ standardContactLine, sb);
			sb.append("</standardConLine>");
			if (logger.isDebugEnabled()) {
				logger.debug("getStandarContactLine(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - generated xml: "
						+ sb.toString());
			}
			response.setContentType("application/xml");
			response.getWriter().write(sb.toString());
		} catch (Exception e) {
			logger.error(
					"getStandarContactLine(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"getStandarContactLine(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}
		return null;
	}

	public static void setEditableValue(POBean poBean, DataSource ds) {
		PODao dao = new PODao();
		boolean isEditable = dao.isEditablePO(poBean.getPoId(), ds);
		if (isEditable) {
			poBean.setIsEditablePO("Y");
		}
	}

	/**
	 * Set appproved total.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward ApprovedEInvoice(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		POBean poBean = (POBean) form;
		poBean.setJobId(((request.getParameter("jobId") == null) ? poBean
				.getJobId() : request.getParameter("jobId")));
		poBean.setAppendixId((request.getParameter("appendix_Id") == null) ? poBean
				.getAppendixId() : request.getParameter("appendix_Id"));

		poBean.setPoId(((request.getParameter("poId") == null) ? poBean
				.getPoId() : request.getParameter("poId")));
		poBean.setPoId(((poBean.getPoId() != null) ? poBean.getPoId()
				: (String) request.getAttribute("poId")));
		request.setAttribute("tabId", (request.getParameter("tabId")));
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");

		if (logger.isDebugEnabled()) {
			logger.debug("ApprovedEInvoice(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - piId ="
					+ poBean.getInvoiceId());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("ApprovedEInvoice(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - comments ="
					+ poBean.getExplanationToPartner());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("ApprovedEInvoice(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - approvedTotal ="
					+ poBean.getInvoiceApprovedTotal());
		}

		PODao.setApprovedTotal(poBean.getInvoiceId(),
				poBean.getExplanationToPartner(),
				poBean.getInvoiceApprovedTotal(), loginuserid,
				getDataSource(request, "ilexnewDB"));

		return (mapping.findForward("eInvoice"));
	}

	/**
	 * Gets the default Sow Assumption.
	 * 
	 * @param poId
	 *            the po id
	 * @param ds
	 *            the ds
	 * 
	 * @return the default sow assumption
	 */
	public static String getDefaultSOWAssumption(String poId, DataSource ds) {
		StringBuffer sow = new StringBuffer();
		StringBuffer assumption = new StringBuffer();

		String[] resourceIds = PODao.getResouceIdByPO(poId, ds);
		String tempSowAssumption = "";
		String defaultSOWAssumption = "";

		for (int i = 0; i < resourceIds.length; i++) {
			// Get the SOW for Activity.
			tempSowAssumption = POWODetaildao.getSowAssumption(resourceIds[i],
					"sow", ds);
			if (!tempSowAssumption.equals("")) {
				sow.append(tempSowAssumption + "\n");
			}

			// Get the Assumption for Activity.
			tempSowAssumption = POWODetaildao.getSowAssumption(resourceIds[i],
					"", ds);
			if (!tempSowAssumption.equals("")) {
				assumption.append(tempSowAssumption + "\n");
			}
		}

		String sowLast = "";
		String assumpLast = "";
		if (!sow.toString().trim().equals("")) {
			sowLast = "Statement of Work:\n\n" + sow.toString().trim();
		}
		if (!assumption.toString().trim().equals("")) {
			assumpLast = "\nAssumptions:\n\n" + assumption.toString().trim();
		}

		defaultSOWAssumption = (sowLast + "\n\n" + assumpLast);
		return defaultSOWAssumption;
	}

	/**
	 * Gets the Special Instruction.
	 * 
	 * @param poId
	 *            the po id
	 * @param loginuserid
	 *            the Login User Id
	 * @param jobId
	 *            the job id
	 * @param appendixId
	 *            the appendix id
	 * @param ds
	 *            the Data Source
	 * 
	 * @return the Special Instruction
	 */
	public static String getSplInst(String poId, String loginuserid,
			String jobId, String appendixId, DataSource ds) {
		String specialInstruction = "";

		String specialinstruction = PartnerSOWAssumptiondao
				.getDefaultPartnerSOWAssumption(appendixId, "prm_app", "si",
						"partner", ds);

		specialInstruction = specialinstruction.trim();

		return specialInstruction;
	}

	/**
	 * Gets the Special Condition.
	 * 
	 * @param appendixId
	 *            the appendix id
	 * @param ds
	 *            the DataSource
	 * 
	 * @return the Special Condition
	 */
	public static String getSplCondition(String appendixId, DataSource ds) {
		String specialCondition = PartnerSOWAssumptiondao
				.getDefaultPartnerSOWAssumption(appendixId, "prm_app", "sc",
						"partner", ds);

		specialCondition = specialCondition.trim();

		return specialCondition;
	}
}
