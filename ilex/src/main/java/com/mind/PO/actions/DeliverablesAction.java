package com.mind.PO.actions;

import static com.mind.PO.actions.POAction.uploadDeliverableToCustomerCare;
import com.mind.PO.dao.PODao;
import com.mind.PO.formbean.POBean;
import com.mind.bean.mpo.PODTO;
import com.mind.common.ChangeDateFormat;
import com.mind.common.PODeliverableStatusTypes;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Activitydao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.POWODetaildao;
import com.mind.docm.dao.DocumentAddDao;
import com.mind.docm.dao.DocumentMasterDao;
import com.mind.docm.dao.EntityManager;
import com.mind.docm.exceptions.CouldNotAddToRepositoryException;
import com.mind.docm.exceptions.CouldNotCheckDocumentException;
import com.mind.docm.exceptions.DocMFormatException;
import com.mind.docm.exceptions.EntityDetailsNotFoundException;
import com.mind.docm.utils.DocumentUtility;
import com.mind.ilex.docmanager.dao.EntityDescriptionDao;
import com.mind.util.WebUtil;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

public class DeliverablesAction extends com.mind.common.IlexDispatchAction {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger
            .getLogger(DeliverablesAction.class);

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        POBean bean = (POBean) form;
        PODTO dto = new PODTO();
        PODao dao = new PODao();
        bean.setJobId(((request.getParameter("jobId") == null) ? bean
                .getJobId() : request.getParameter("jobId")));
        bean.setPoId(((request.getParameter("poId") == null) ? bean.getPoId()
                : request.getParameter("poId")));
        String tabId = (request.getParameter("type") == null ? "3" : request
                .getParameter("type"));
        request.setAttribute("tabId", tabId);
        String loginuserid = (String) request.getSession(true).getAttribute(
                "userid");
        String clicked = "";
        if (request.getParameter("clicked") != null) {
            request.setAttribute("clicked", request.getParameter("clicked"));
            clicked = request.getParameter("clicked");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - clicked:: "
                    + clicked.toString());
        }

        if (clicked != null && !clicked.equals("")) {
            request.setAttribute("clicked", clicked);
            String devId = "0";
            if (request.getParameter("devId") != null) {
                devId = request.getParameter("devId");
            }
            try {
                BeanUtils.copyProperties(dto, bean);
            } catch (IllegalAccessException e) {
                // logger.error(e);
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                // logger.error(e);
                e.printStackTrace();
            }
            dao.generalfetchData(dto, tabId, devId);
            try {
                BeanUtils.copyProperties(bean, dto);
            } catch (IllegalAccessException e) {
                // logger.error(e);
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                // logger.error(e);
                e.printStackTrace();
            }
        } else {

            Map<String, Object> map = new HashMap<String, Object>();
            try {
                BeanUtils.copyProperties(dto, bean);
            } catch (IllegalAccessException e) {
                // logger.error(e);
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                // logger.error(e);
                e.printStackTrace();
            }
            dao.getGeneralPODetailListForDelivrables(map, dto, tabId);
            try {
                BeanUtils.copyProperties(bean, dto);
            } catch (IllegalAccessException e) {
                // logger.error(e);
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                // logger.error(e);
                e.printStackTrace();
            }
            WebUtil.copyMapToRequest(request, map);
        }

        request.setAttribute("mode", "view");
        return (mapping.findForward("success"));
    }

    public ActionForward saveDocument(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        POBean bean = (POBean) form;
        PODTO dto = new PODTO();
        PODao dao = new PODao();
        bean.setJobId(((request.getParameter("jobId") == null) ? bean
                .getJobId() : request.getParameter("jobId")));
        bean.setPoId(((request.getParameter("poId") == null) ? bean.getPoId()
                : request.getParameter("poId")));
        request.setAttribute("mode", "view");
        String loginuserid = (String) request.getSession(true).getAttribute(
                "userid");
        String tabId = (request.getParameter("type") == null ? "3" : request
                .getParameter("type"));

        Map<String, Object> map = new HashMap<String, Object>();

        try {
            BeanUtils.copyProperties(dto, bean);
        } catch (IllegalAccessException e) {
            // logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // logger.error(e);
            e.printStackTrace();
        }
        dao.saveGeneralMPODetailListForDelivrables(map, dto, tabId);
        try {
            BeanUtils.copyProperties(bean, dto);
        } catch (IllegalAccessException e) {
            // logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // logger.error(e);
            e.printStackTrace();
        }
        WebUtil.copyMapToRequest(request, map);
        request.setAttribute("mode", "view");
        bean.setDevMode("");
        request.setAttribute("ActivityCreated", "Deliverable Saved.");
        dao.generalfetchData(dto, tabId, null);
        try {
            BeanUtils.copyProperties(bean, dto);
        } catch (IllegalAccessException e) {
            // logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // logger.error(e);
            e.printStackTrace();
        }
        return (mapping.findForward("success"));
    }

    public ActionForward uploadPODeliverableData(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) {
        POBean poBean = (POBean) form;
        PODTO dto = new PODTO();
        PODao poDao = new PODao();
        ArrayList deliverableType = null;
        ArrayList deliverableStatus = null;

        String devId = "0";
        if (request.getParameter("devId") != null) {
            devId = request.getParameter("devId");
        }
        if (request.getParameter("poId") != null) {
            poBean.setPoId(request.getParameter("poId"));
        }
        if (request.getParameter("jobId") != null) {
            poBean.setJobId(request.getParameter("jobId"));
        }
        if (request.getParameter("appendixId") != null) {
            poBean.setAppendixId(request.getParameter("appendixId"));
        }

        if (request.getParameter("supportDeliverable") != null) {
            request.setAttribute("supportDeliverable",
                    request.getParameter("supportDeliverable"));
        }

        if (poBean.getDevStatus().equals("1")) {
            poBean.setDevStatusName("Uploaded");
        } else if (poBean.getDevStatus().equals("2")) {
            poBean.setDevStatusName("Accepted");
        } else if (poBean.getDevStatus().equals("3")) {
            poBean.setDevStatusName("Rejected");
        } else {
            poBean.setDevStatusName("Pending");
        }

        deliverableType = poDao.getPoDeliverableType();
        request.setAttribute("deliverableType", deliverableType);
        deliverableStatus = poDao.getPoDeliverableStatusList();
        request.setAttribute("deliverableStatus", deliverableStatus);

        if (request.getParameter("supportDeliverable") != null
                && request.getParameter("supportDeliverable").equals(
                        "uploadSupportDeliverable")) {

            poBean.setDevType("Supporting");
            poBean.setDevStatus("2");

            // System.out.println("EntityDescriptionDao.getDbCurrentDate()---"+EntityDescriptionDao.getDbCurrentDate());
            poBean.setDevDate(ChangeDateFormat.getDateFormat(
                    EntityDescriptionDao.getDbCurrentDate(), "MM/dd/yyyy",
                    "yyyy-MM-dd HH:mm:ss.ms"));

        } else if (request.getParameter("supportDeliverable") != null
                && request.getParameter("supportDeliverable").equals(
                        "editDeliverable")) {

            try {
                BeanUtils.copyProperties(dto, poBean);
            } catch (IllegalAccessException e) {
                // logger.error(e);
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                // logger.error(e);
                e.printStackTrace();
            }
            poDao.generalfetchData(dto, "3", devId);
            try {
                BeanUtils.copyProperties(poBean, dto);
            } catch (IllegalAccessException e) {
                // logger.error(e);
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                // logger.error(e);
                e.printStackTrace();
            }

        } else {
            try {
                BeanUtils.copyProperties(dto, poBean);
            } catch (IllegalAccessException e) {
                // logger.error(e);
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                // logger.error(e);
                e.printStackTrace();
            }
            poDao.generalfetchData(dto, "3", devId);
            try {
                BeanUtils.copyProperties(poBean, dto);
            } catch (IllegalAccessException e) {
                // logger.error(e);
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                // logger.error(e);
                e.printStackTrace();
            }

        }

        if (("Supporting").equals(poBean.getDevType())) {
            poBean.setDevStatus("2");
        }
        return (mapping.findForward("uploadDeliverable"));
    }

    public ActionForward saveDeliverableDocument(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) {
        POBean bean = (POBean) form;
        // System.out.println("save delivarable details");
        PODao dao = new PODao();
        String docId = "";
        byte[] bytes = null;
        String loginUserName = (String) request.getSession().getAttribute(
                "username");

        bean.setJobId(((request.getParameter("jobId") == null) ? bean
                .getJobId() : request.getParameter("jobId")));
        bean.setPoId(((request.getParameter("poId") == null) ? bean.getPoId()
                : request.getParameter("poId")));
        request.setAttribute("mode", "view");
        String loginuserid = (String) request.getSession(true).getAttribute(
                "userid");

        String tabId = (request.getParameter("type") == null ? "3" : request
                .getParameter("type"));
        request.setAttribute("mode", "view");
        bean.setDevMode("");
        request.setAttribute("ActivityCreated", "Deliverable Saved.");

        if (bean.getMyfile() != null && bean.getMyfile().getFileSize() > 0) {
            FormFile file = bean.getMyfile();
            if (file.getFileSize() > 0) {
                try {
                    bytes = file.getFileData();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (bean.getPoDocId() != null && !bean.getPoDocId().equals("")
                && !bean.getPoDocId().equals("-1")) {
            docId = bean.getPoDocId();
            bean.setDoc_id(docId);
            request.setAttribute("docId", docId);
        }

        bean.setJobId((POWODetaildao.getJobIdFromPoId(
                getDataSource(request, "ilexnewDB"), bean.getPoId())));
        bean.setJob_Name(Activitydao.getJobname(
                getDataSource(request, "ilexnewDB"), bean.getJobId()));
        bean.setPoId(bean.getPoId());
        bean.setWO_ID(bean.getPoId());
        bean.setPartner_Name(DocumentAddDao.getPartnerName(
                getDataSource(request, "ilexnewDB"), bean.getPoId()));
        bean.setAppendixId(IMdao.getAppendixId(bean.getJobId(),
                getDataSource(request, "ilexnewDB")));
        bean.setMsaId(IMdao.getMSAId(bean.getAppendixId(),
                getDataSource(request, "ilexnewDB")));
        bean.setAppendix_Name(Jobdao.getAppendixname(
                getDataSource(request, "ilexnewDB"), bean.getAppendixId()));
        bean.setMSA_Name(com.mind.dao.PM.Appendixdao.getMsaname(
                getDataSource(request, "ilexnewDB"), bean.getMsaId()));
        bean.setUserName(loginUserName);
        PODTO dto = new PODTO();

        // set source of uploading deliverable to Internal(using Ilex
        // Application)
        dto.setUploadSource("Internal");
        try {
            BeanUtils.copyProperties(dto, bean);
        } catch (IllegalAccessException e1) {
            logger.error(e1);
            e1.printStackTrace();
        } catch (InvocationTargetException e1) {
            logger.error(e1);
            e1.printStackTrace();
        }
        if (request.getParameter("supportDeliverable") != null) {
            String[] deliverableData = dao.createPOSupportingDeliverableUpload(
                    dto, loginuserid);
            bean.setDevId(deliverableData[0]);
            dto.setDevId(deliverableData[0]);
        }

        try {
            /**
             * This was requested that on upload deliverable screen deliverable
             * text field that uploads file should not be required that is way
             * applied this condition to avoid exception.
             *
             */
            if (!bean.getMyfile().getFileName().equals("")) {
                docId = EntityManager.approveEntity(dto, "Deliverables", bytes,
                        bean.getMyfile().getFileName());
                bean.setDoc_id(docId);
                request.setAttribute("docId", docId);
            }
        } catch (DocMFormatException e) {
            e.printStackTrace();
        } catch (CouldNotAddToRepositoryException e) {
            e.printStackTrace();
        } catch (CouldNotCheckDocumentException e) {
            e.printStackTrace();
        } catch (EntityDetailsNotFoundException e) {
            e.printStackTrace();
        }

        /*
         * In order to use this for both add and edit case.
         */
        dao.savePODeliverablesUpload(dto, loginuserid, docId);

        if (request.getParameter("supportDeliverable") != null) {
            // PODao.updateDeliverableDocId(bean.getDevId(), bean.getPoId(),
            // docId, getDataSource(request, "ilexnewDB"));
        } else {
			// dao.savePODeliverablesUpload(dto, loginuserid, docId);

            // Deliverable is uploaded as accepted Status
            if (dto.getDevStatus() != null
                    && Integer.valueOf(dto.getDevStatus()) == PODeliverableStatusTypes.STATUS_ACCEPTED) {
                /**
                 * Commented as per client(Bob Schwieterman) request ref: When
                 * manually uploading the final deliverable, the PO status now
                 * changes to complete. It should stay in whatever status it was
                 * in when uploading the deliverable.
                 *
                 */
				// boolean poMarkedComplete =
                // PODao.markPOComplete(dto.getPoId(),
                // loginuserid, getDataSource(request, "ilexnewDB"));

                // commented for not to show checkList on PO Completion
                // if (poMarkedComplete) {
                // request.setAttribute("job_id", dto.getJobId());
                // request.setAttribute("poId", dto.getPoId());
                // request.setAttribute("ownerId", loginuserid);
                // return (mapping.findForward("qcchecklist"));
                // }
            }
        }

        if (bean.getDevStatus() != null && bean.getDevStatus().equals("2")) {
            /* if status is Accept i-e 2 */

            /*
             * if (bean.getDevFormat().equals("PDF") &&
             * bean.getDevFormat().equals("Image")) {
             */
            viewDeliverableBeforeAccept(mapping, form, request, response);
            /*
             * } else {
             *
             * viewDeliverableBeforeAcceptExceptCase(mapping, form, request,
             * response); }
             */

            if (bean.getDevType() != null
                    && bean.getDevType().equals("Customer")) {
                request.setAttribute("actionType", "Accept");
                acceptDeliverable(mapping, form, request, response);
            }
        }

        return (mapping.findForward("PODeliverableTab"));
    }

    public ActionForward viewDeliverableBeforeAccept(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) {
        POBean poBean = (POBean) form;

        String docId = request.getParameter("docId");
        if (docId == null && request.getAttribute("docId") != null) {
            docId = request.getAttribute("docId").toString();
        }
        String poId = request.getParameter("poId");
        String jobId = request.getParameter("jobId");
        String devId = request.getParameter("devId");
        String type = request.getParameter("type");
        poBean.setPoDocId(docId);
        poBean.setDevId(devId);
        poBean.setPoId(poId);
        poBean.setJobId(jobId);
        poBean.setDevType(type);

        String fileId = DocumentUtility.getfileIdFromDoc(docId);

        String filename = DocumentMasterDao.getFileDataForAcceptCase(fileId);

        if (filename.equals("jpeg") || filename.equals("png")
                || filename.equals("x-png") || filename.equals("bmp")
                || filename.equals("gif") || filename.equals("jpg")
                || filename.equals("psd") || filename.equals("pspimage")
                || filename.equals("thm") || filename.equals("tif")
                || filename.equals("yuv") || filename.equals("pdf")) {

            return (mapping.findForward("viewDeliverable"));

        } else {

            return (mapping.findForward("viewDeliverableExcept"));

        }

    }

    public ActionForward viewDeliverableBeforeAcceptExceptCase(
            ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) {
        POBean poBean = (POBean) form;

        String docId = request.getParameter("docId");
        if (docId == null && request.getAttribute("docId") != null) {
            docId = request.getAttribute("docId").toString();
        }
        String poId = request.getParameter("poId");
        String jobId = request.getParameter("jobId");
        String devId = request.getParameter("devId");

        poBean.setPoDocId(docId);
        poBean.setDevId(devId);
        poBean.setPoId(poId);
        poBean.setJobId(jobId);

        return (mapping.findForward("viewDeliverableExcept"));
    }

    public ActionForward viewDeliverableBeforeAcceptExceptCaseView(
            ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) {
        POBean poBean = (POBean) form;

        String docId = request.getParameter("docId");
        if (docId == null && request.getAttribute("docId") != null) {
            docId = request.getAttribute("docId").toString();
        }
        /*
         * String poId = request.getParameter("poId"); String jobId =
         * request.getParameter("jobId"); String devId =
         * request.getParameter("devId");
         */
        poBean.setPoDocId(docId);
        /*
         * poBean.setDevId(devId); poBean.setPoId(poId); poBean.setJobId(jobId);
         */

        return (mapping.findForward("viewDeliverableExceptView"));
    }

    public ActionForward acceptDeliverable(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String actionType = request.getParameter("actionType") == null ? request.getAttribute("actionType").toString() : request.getParameter("actionType");
        String devId = request.getParameter("devId");
        String poId = request.getParameter("poId");
        String type = request.getParameter("type");
        String loginuserid = (String) request.getSession(true).getAttribute("userid");

        if (actionType != null && actionType.equals("Accept")) {
            PODao.changeDeliverableStatus(devId, PODeliverableStatusTypes.STATUS_ACCEPTED + "", poId, loginuserid, getDataSource(request, "ilexnewDB"));

            if ("Customer".equals(type)) {
                uploadDeliverableToCustomerCare(devId, poId);
            }
        }

        return (mapping.findForward("PODeliverableTab"));
    }
}
