package com.mind.PO.actions;

public class DeliverableUploadRunnable implements Runnable {

	private String devId;
	private String poId;

	public DeliverableUploadRunnable(String devId, String poId) {
		this.devId = devId;
		this.poId = poId;
	}

	@Override
	public void run() {
		POAction.uploadDeliverableToCustomerCare(devId, poId);

	}

}
