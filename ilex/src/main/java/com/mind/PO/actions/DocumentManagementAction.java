package com.mind.PO.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.PO.dao.PODao;
import com.mind.PO.formbean.POBean;
import com.mind.bean.mpo.PODTO;
import com.mind.bean.newjobdb.POFooterInfo;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.docm.client.ClientOperator;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.dao.PurchaseOrderDAO;
import com.mind.util.WebUtil;

public class DocumentManagementAction extends
		com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DocumentManagementAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		POBean bean = (POBean) form;
		PODao dao = new PODao();
		bean.setJobId(((request.getParameter("jobId") == null) ? bean
				.getJobId() : request.getParameter("jobId")));
		bean.setPoId(((request.getParameter("poId") == null) ? bean.getPoId()
				: request.getParameter("poId")));

		List<List<String>> al = dao.featchDocListForMPO(new Integer(bean
				.getPoId()).intValue());
		request.setAttribute(
				"tabId",
				(request.getParameter("type") == null ? "2" : request
						.getParameter("type")));
		request.setAttribute("docDetailPageList", al);
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - al"
					+ al);
		}
		return (mapping.findForward("success"));
	}

	public ActionForward poSearchResult(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		POBean bean = (POBean) form;
		PODao dao = new PODao();
		// bean.setAppendixId(((request.getParameter("appendix_Id") == null) ?
		// bean.getAppendixId(): request.getParameter("appendix_Id")));

		String type = request.getParameter("type");
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		request.setAttribute("tabId", type);
		ArrayList al = null;
		if (logger.isDebugEnabled()) {
			logger.debug("poSearchResult(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - docIdList :: "
					+ request.getParameter("docIdList"));
		}
		bean.setJobId(((request.getParameter("jobId") == null) ? bean
				.getJobId() : request.getParameter("jobId")));
		bean.setPoId(((request.getParameter("poId") == null) ? bean.getPoId()
				: request.getParameter("poId")));

		// System.out.println("mpoId"+new Integer(bean.getMpoId()).intValue());

		List<List<String>> detailList = PODao.featchDocListForMPO(Integer
				.parseInt(bean.getPoId()));
		if (request.getParameter("through") != null) {
			bean.setDocIdList(request.getParameter("docIdList"));
			al = ClientOperator.getApprovedDocForMPO(bean.getDocIdList());
		} else {
			String docIdList = bean.getDocIdList();
			if (logger.isDebugEnabled()) {
				logger.debug("poSearchResult(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - docIdList :: "
						+ docIdList);
			}
			al = ClientOperator.getApprovedDocForMPO(docIdList.substring(1,
					docIdList.length()));
		}
		Map<String, Object> map;

		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		POFooterInfo poFooterInfo = PurchaseOrderDAO.getPoFooterInfo(
				bean.getPoId(), getDataSource(request, "ilexnewDB"));
		bean.setPoFooterCreateDate(poFooterInfo.getPoFooterCreateDate());
		bean.setPoFooterChangeDate(poFooterInfo.getPoFooterChangeDate());
		bean.setPoFooterSource(poFooterInfo.getPoFooterSource());
		request.setAttribute("mode", "view");
		if (logger.isDebugEnabled()) {
			logger.debug("poSearchResult(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - al :: "
					+ al);
		}
		request.setAttribute("docMList", al);
		request.setAttribute("docDetailPageList", detailList);
		// setEntityNames(bean.getAppendixId(),request);

		POAction.setEditableValue(bean, getDataSource(request, "ilexnewDB"));

		return (mapping.findForward("success"));
	}

	public void setEntityNames(String appId, HttpServletRequest request) {
		String msaName = "MSA";
		String appendixName = "Appendix";
		try {
			msaName = Appendixdao.getMsaname(
					getDataSource(request, "ilexnewDB"),
					IMdao.getMSAId(appId, getDataSource(request, "ilexnewDB")));
			appendixName = Jobdao.getAppendixname(
					getDataSource(request, "ilexnewDB"), appId);
		} catch (Exception e) {
			logger.error("setEntityNames(String, HttpServletRequest)", e);

			logger.error("setEntityNames(String, HttpServletRequest)", e);
		}
		if (msaName == null || msaName.equals(""))
			msaName = "MSA";
		if (appendixName == null || appendixName.equals(""))
			appendixName = "Appendix";
		request.setAttribute("msaName", msaName);
		request.setAttribute("appendixName", appendixName);

	}

	public ActionForward saveDocument(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		POBean bean = (POBean) form;
		PODao dao = new PODao();
		bean.setJobId(((request.getParameter("jobId") == null) ? bean
				.getJobId() : request.getParameter("jobId")));
		bean.setPoId(((request.getParameter("poId") == null) ? bean.getPoId()
				: request.getParameter("poId")));
		request.setAttribute("mode", "view");
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		String tabId = request.getParameter("type");
		PODTO dto = new PODTO();
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		Map<String, Object> sessionMap;
		sessionMap = WebUtil
				.copySessionToAttributeMap(request.getSession(true));
		Map<String, Object> map = new HashMap<String, Object>();
		if (tabId.equalsIgnoreCase("2")) {
			request.setAttribute("tabId", tabId);
			dao.pODetailSave(sessionMap, map, dto, "dbo.mp_document", tabId);
			WebUtil.copyMapToRequest(request, map);
			try {
				BeanUtils.copyProperties(bean, dto);
			} catch (IllegalAccessException e) {
				logger.error(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				logger.error(e);
				e.printStackTrace();
			}
			request.setAttribute("mode", "view");
			List<List<String>> al = dao.featchDocListForMPO(Integer
					.parseInt(bean.getPoId()));
			request.setAttribute("docDetailPageList", al);
			request.setAttribute("Document", "Document List saved.");
		}

		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		POFooterInfo poFooterInfo = PurchaseOrderDAO.getPoFooterInfo(
				bean.getPoId(), getDataSource(request, "ilexnewDB"));
		bean.setPoFooterCreateDate(poFooterInfo.getPoFooterCreateDate());
		bean.setPoFooterChangeDate(poFooterInfo.getPoFooterChangeDate());
		bean.setPoFooterSource(poFooterInfo.getPoFooterSource());

		POAction.setEditableValue(bean, getDataSource(request, "ilexnewDB"));

		request.setAttribute("save", "save");
		return (mapping.findForward("success"));
	}

	public ActionForward updateDocument(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		POBean bean = (POBean) form;
		PODTO dto = new PODTO();
		PODao dao = new PODao();
		bean.setJobId(((request.getParameter("jobId") == null) ? bean
				.getJobId() : request.getParameter("jobId")));
		bean.setPoId(((request.getParameter("poId") == null) ? bean.getPoId()
				: request.getParameter("poId")));
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		bean.setDoc_id(request.getParameter("docId"));
		if (logger.isDebugEnabled()) {
			logger.debug("updateDocument(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - +++ bean.getMpoId()).intValue() ++ "
					+ bean.getMpoId());
		}
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> sessionMap;
		sessionMap = WebUtil
				.copySessionToAttributeMap(request.getSession(true));

		dao.updateDocFormMPO(dto, sessionMap);
		dao.fetchDataForMPO(map, dto, request.getParameter("type"));
		WebUtil.copyMapToRequest(request, map);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		List<List<String>> al = dao.featchDocListForMPO(new Integer(bean
				.getPoId()).intValue());
		request.setAttribute("docDetailPageList", al);
		request.setAttribute("tabId", request.getParameter("type"));
		request.setAttribute("mode", "view");

		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		POFooterInfo poFooterInfo = PurchaseOrderDAO.getPoFooterInfo(
				bean.getPoId(), getDataSource(request, "ilexnewDB"));
		bean.setPoFooterCreateDate(poFooterInfo.getPoFooterCreateDate());
		bean.setPoFooterChangeDate(poFooterInfo.getPoFooterChangeDate());
		bean.setPoFooterSource(poFooterInfo.getPoFooterSource());
		request.setAttribute("save", "save");

		// setEntityNames(request.getParameter("appendixid"),request);
		POAction.setEditableValue(bean, getDataSource(request, "ilexnewDB"));

		return (mapping.findForward("success"));
	}

	public ActionForward deleteDocument(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		POBean bean = (POBean) form;
		PODTO dto = new PODTO();
		PODao dao = new PODao();
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		bean.setDoc_id(request.getParameter("docId"));
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}

		if (!dto.getDoc_id().isEmpty()) {
			dao.deleteDocFormMPO(dto);
		}
		Map<String, Object> map = new HashMap<String, Object>();

		dao.fetchDataForMPO(map, dto, request.getParameter("type"));
		WebUtil.copyMapToRequest(request, map);
		List<List<String>> al = dao.featchDocListForMPO(new Integer(bean
				.getPoId()).intValue());
		request.setAttribute("docDetailPageList", al);
		request.setAttribute("tabId", request.getParameter("type"));
		request.setAttribute("mode", "view");
		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		POFooterInfo poFooterInfo = PurchaseOrderDAO.getPoFooterInfo(
				bean.getPoId(), getDataSource(request, "ilexnewDB"));
		bean.setPoFooterCreateDate(poFooterInfo.getPoFooterCreateDate());
		bean.setPoFooterChangeDate(poFooterInfo.getPoFooterChangeDate());
		bean.setPoFooterSource(poFooterInfo.getPoFooterSource());
		request.setAttribute("save", "save");
		POAction.setEditableValue(bean, getDataSource(request, "ilexnewDB"));

		return (mapping.findForward("success"));
	}

}
