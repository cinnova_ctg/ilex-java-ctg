package com.mind.PO.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.PO.dao.PODao;
import com.mind.PO.formbean.CopyMpoToPoBean;
import com.mind.bean.newjobdb.GeneralPOSetup;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.MPOList;
import com.mind.bean.po.CopyMpoToPoDTO;
import com.mind.common.Util;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.ConvertorForCopyMPO;
import com.mind.newjobdb.dao.MPOCopier;
import com.mind.newjobdb.dao.MPODAO;
import com.mind.newjobdb.dao.PurchaseOrder;
import com.mind.newjobdb.dao.PurchaseOrderList;
import com.mind.newjobdb.dao.TicketDAO;
import com.mind.util.WebUtil;

public class CopyMpoToPoAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(CopyMpoToPoAction.class);

	public ActionForward openCopyMPO(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		CopyMpoToPoBean copyMpoToPoBean = (CopyMpoToPoBean) form;
		CopyMpoToPoDTO copyMpoToPoDTO = new CopyMpoToPoDTO();
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		Map<String, Object> map;

		if (copyMpoToPoBean.getJobId() != null
				&& !copyMpoToPoBean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(copyMpoToPoBean.getJobId(),
					loginuserid, getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		try {
			BeanUtils.copyProperties(copyMpoToPoDTO, copyMpoToPoBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		setFormFromMPOToBeCopiedObjects(copyMpoToPoDTO,
				getDataSource(request, "ilexnewDB"), request);
		/*
		 * if( poBean.getMpoId() != null && !poBean.getMpoId().equals("")){
		 * PurchaseOrderList.copyNewPOFromMPO(poBean.getJobId(), loginuserid,
		 * "Add", poBean.getMpoId(), "", "", getDataSource( request ,
		 * "ilexnewDB" )); PODao poDao = new PODao(); String mode =
		 * request.getParameter("mode"); request.setAttribute("POStatus",
		 * "Draft"); request.setAttribute("tabId",
		 * request.getParameter("type")); if(mode!=null)
		 * request.setAttribute("mode","view");
		 * 
		 * System.out.println("tabid= "+request.getAttribute("tabId")); return
		 * (mapping.findForward("success")); }
		 */

		try {
			BeanUtils.copyProperties(copyMpoToPoBean, copyMpoToPoDTO);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		return (mapping.findForward("copyMPO"));
	}

	private static void setFormFromMPOToBeCopiedObjects(
			CopyMpoToPoDTO copyMpoToPoBean, DataSource ds,
			HttpServletRequest request) {
		MPOList mpoList = MPODAO.getListOfMPO(copyMpoToPoBean.getAppendixId(),
				ds);
		request.setAttribute("listSize", mpoList.getMasterPurchaseOrders()
				.size());
		MPOCopier mpoCopier = new MPOCopier();
		mpoCopier.setMpoList(mpoList);
		GeneralPOSetup generalPOSetup = MPODAO.getCopyToBeMPODates(
				copyMpoToPoBean.getJobId(), ds);
		mpoCopier.setGeneralPOSetup(generalPOSetup);
		ConvertorForCopyMPO.setFormFromMpoCopier(mpoCopier, copyMpoToPoBean);
		copyMpoToPoBean.setHourList(Util.getHours());
		copyMpoToPoBean.setMinuteList(Util.getMinutes());
	}

	public ActionForward createPO(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		CopyMpoToPoBean copyMpoToPoBean = (CopyMpoToPoBean) form;
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		String mpoId = copyMpoToPoBean.getMpoId()[0];
		if (logger.isDebugEnabled()) {
			logger.debug("createPO(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - mpoId: "
					+ mpoId);
		}
		if (mpoId != null && !mpoId.equals("")) {
			boolean isValid = false;
			String flag = MPOCopier.validateCopyToPO(mpoId,
					copyMpoToPoBean.getJobId(),
					getDataSource(request, "ilexnewDB"));
			if (flag.equalsIgnoreCase("True"))
				isValid = true;

			if (isValid) {
				String newPoWoId = PurchaseOrderList.copyNewPOFromMPO(
						copyMpoToPoBean.getJobId(), loginuserid, "Add", mpoId,
						"", "", "", "", getDataSource(request, "ilexnewDB"));
				PODao poDao = new PODao();
				String mode = request.getParameter("mode");
				request.setAttribute("POStatus", "Draft");
				request.setAttribute("tabId", request.getParameter("tabId"));
				if (mode != null)
					request.setAttribute("mode", "view");
				if (logger.isDebugEnabled()) {
					logger.debug("createPO(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - tabid= "
							+ request.getAttribute("tabId"));
				}
				IlexTimestamp deliveryTimeStamp = new IlexTimestamp(
						copyMpoToPoBean.getDeliveryDate(),
						copyMpoToPoBean.getDeliveryHour(),
						copyMpoToPoBean.getDeliveryMinute(), copyMpoToPoBean
								.getDeliveryIsAm().equals("AM") ? true : false);
				IlexTimestamp issueTimeStamp = new IlexTimestamp(
						copyMpoToPoBean.getIssueDate(),
						copyMpoToPoBean.getIssueHour(),
						copyMpoToPoBean.getIssueMinute(), copyMpoToPoBean
								.getIssueIsAm().equals("AM") ? true : false);

				// Copying PO info from mpo: start
				PurchaseOrder purchaseOrder = MPOCopier
						.getPurchaseOrderFromMPO(newPoWoId, mpoId,
								copyMpoToPoBean.getJobId(), deliveryTimeStamp,
								issueTimeStamp,
								getDataSource(request, "ilexnewDB"));
				PurchaseOrder.savePOAuthorizedCostInfo(purchaseOrder,
						loginuserid, getDataSource(request, "ilexnewDB"));
				PurchaseOrder.savePurchaseWorkOrder(purchaseOrder, loginuserid,
						getDataSource(request, "ilexnewDB"));
				PurchaseOrder.savePODocuments(purchaseOrder, loginuserid,
						getDataSource(request, "ilexnewDB"));
				PurchaseOrder.savePODeliverables(purchaseOrder, loginuserid,
						getDataSource(request, "ilexnewDB"));

				// Copying PO info from mpo: end

				request.setAttribute("poId", newPoWoId);
				return (mapping.findForward("success"));
			} else {
				Map<String, Object> map;

				if (copyMpoToPoBean.getJobId() != null
						&& !copyMpoToPoBean.getJobId().trim()
								.equalsIgnoreCase("")) {
					map = DatabaseUtilityDao.setMenu(
							copyMpoToPoBean.getJobId(), loginuserid,
							getDataSource(request, "ilexnewDB"));
					WebUtil.copyMapToRequest(request, map);
				}
				copyMpoToPoBean.setIsValidToCopy(flag);
				CopyMpoToPoDTO copyMpoToPoDTO = new CopyMpoToPoDTO();
				try {
					BeanUtils.copyProperties(copyMpoToPoDTO, copyMpoToPoBean);
				} catch (IllegalAccessException e) {

					logger.error(e);
					e.printStackTrace();
				} catch (InvocationTargetException e) {

					logger.error(e);
					e.printStackTrace();
				}
				setFormFromMPOToBeCopiedObjects(copyMpoToPoDTO,
						getDataSource(request, "ilexnewDB"), request);

				try {
					BeanUtils.copyProperties(copyMpoToPoBean, copyMpoToPoDTO);
				} catch (IllegalAccessException e) {

					logger.error(e);
					e.printStackTrace();
				} catch (InvocationTargetException e) {

					logger.error(e);
					e.printStackTrace();
				}
				return (mapping.findForward("copyMPO"));
			}
		}
		return null;
	}

	/**
	 * Copy PO/Wo, PODocuments and PODeliverables into Default PO of Ticket.
	 * 
	 * @param request
	 * @param jobId
	 * @param ds
	 */
	public static void copyToDefaultTicketPO(HttpServletRequest request,
			String jobId, DataSource ds) {
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		String[] mopPoId = TicketDAO.getMpoPOId(jobId, ds);
		String mpoId = mopPoId[0];
		String poWoId = mopPoId[1];
		String appendixId = mopPoId[2];

		if (poWoId != null && mpoId != null) {
			PurchaseOrder purchaseOrder = MPOCopier.getPurchaseOrderFromMPO(
					poWoId, mpoId, jobId, null, null, ds);
			TicketDAO.concateTicketSpecInstructionSOW(purchaseOrder, jobId, ds);
			PurchaseOrder.savePurchaseWorkOrder(purchaseOrder, loginuserid, ds);
			PurchaseOrder.savePODocuments(purchaseOrder, loginuserid, ds);
			PurchaseOrder.savePODeliverables(purchaseOrder, loginuserid, ds);
		}
	}
}
