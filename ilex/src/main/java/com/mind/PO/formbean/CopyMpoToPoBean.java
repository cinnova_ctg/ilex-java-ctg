package com.mind.PO.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.mind.common.LabelValue;

public class CopyMpoToPoBean extends ActionForm{
	private String appendixId ;
	private String jobId ;
	private String deliveryDate ;
	private String deliveryHour;
	private String deliveryMinute;
	private String deliveryIsAm;
	private String issueDate ;
	private String issueHour;
	private String issueMinute;
	private String issueIsAm;
	private String mpoId[] = null;
	private String mpoName[] = null;
	private String masterPOItem[] = null;;
	private String estimatedTotalCost[] = null;;
	private String status[] = null;
	private String isSelectable[] = null;
	private ArrayList<LabelValue>  hourList= null;
	private ArrayList<LabelValue>  minuteList= null;
	private String isValidToCopy = null;
	
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getDeliveryHour() {
		return deliveryHour;
	}
	public void setDeliveryHour(String deliveryHour) {
		this.deliveryHour = deliveryHour;
	}
	public String getDeliveryIsAm() {
		return deliveryIsAm;
	}
	public void setDeliveryIsAm(String deliveryIsAm) {
		this.deliveryIsAm = deliveryIsAm;
	}
	public String getDeliveryMinute() {
		return deliveryMinute;
	}
	public void setDeliveryMinute(String deliveryMinute) {
		this.deliveryMinute = deliveryMinute;
	}
	
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getIssueHour() {
		return issueHour;
	}
	public void setIssueHour(String issueHour) {
		this.issueHour = issueHour;
	}
	public String getIssueIsAm() {
		return issueIsAm;
	}
	public void setIssueIsAm(String issueIsAm) {
		this.issueIsAm = issueIsAm;
	}
	public String getIssueMinute() {
		return issueMinute;
	}
	public void setIssueMinute(String issueMinute) {
		this.issueMinute = issueMinute;
	}
	public String[] getMasterPOItem() {
		return masterPOItem;
	}
	public void setMasterPOItem(String[] masterPOItem) {
		this.masterPOItem = masterPOItem;
	}
	public String[] getMpoId() {
		return mpoId;
	}
	public void setMpoId(String[] mpoId) {
		this.mpoId = mpoId;
	}
	public String[] getMpoName() {
		return mpoName;
	}
	public void setMpoName(String[] mpoName) {
		this.mpoName = mpoName;
	}
	public String[] getStatus() {
		return status;
	}
	public void setStatus(String[] status) {
		this.status = status;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String[] getEstimatedTotalCost() {
		return estimatedTotalCost;
	}
	public void setEstimatedTotalCost(String[] estimatedTotalCost) {
		this.estimatedTotalCost = estimatedTotalCost;
	}
	public ArrayList<LabelValue> getHourList() {
		return hourList;
	}
	public void setHourList(ArrayList<LabelValue> hourList) {
		this.hourList = hourList;
	}
	public ArrayList<LabelValue> getMinuteList() {
		return minuteList;
	}
	public void setMinuteList(ArrayList<LabelValue> minuteList) {
		this.minuteList = minuteList;
	}
	public String[] getIsSelectable() {
		return isSelectable;
	}
	public void setIsSelectable(String[] isSelectable) {
		this.isSelectable = isSelectable;
	}
	public String getIsValidToCopy() {
		return isValidToCopy;
	}
	public void setIsValidToCopy(String isValidToCopy) {
		this.isValidToCopy = isValidToCopy;
	}
}
