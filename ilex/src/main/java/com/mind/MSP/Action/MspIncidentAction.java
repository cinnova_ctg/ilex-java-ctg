package com.mind.MSP.Action;

import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.IncidentBean;
import com.mind.MSP.business.MspOrganizer;
import com.mind.MSP.common.MspUtility;
import com.mind.MSP.formbean.MspIncidentForm;
import com.mind.dao.IM.IMdao;
import com.mind.msp.common.EscalationLevel;

/**
 * The Class MspIncidentAction.
 */
public class MspIncidentAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IMdao.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */

	/**
	 * Incident. Called on the first time on page load.
	 * 
	 * @param ActionMapping
	 *            the mapping
	 * @param ActionForm
	 *            the form
	 * @param HttpServletRequest
	 *            the request
	 * @param HttpServletResponse
	 *            the response
	 * 
	 * @return the action forward
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MspIncidentForm mspIncidentForm = (MspIncidentForm) form;
		IncidentBean incidentBean = new IncidentBean();
		MspOrganizer.setIncident(incidentBean, getDataSource(request,
				"ilexnewDB"));
		MspOrganizer.setFormFromIncidentBean(mspIncidentForm, incidentBean);
		return mapping.findForward("success"); // - /MSP/MspIncident.jsp
	}

	/**
	 * Incident. Called through ajax, on the basis of interval.
	 * 
	 * @param ActionMapping
	 *            the mapping
	 * @param ActionForm
	 *            the form
	 * @param HttpServletRequest
	 *            the request
	 * @param HttpServletResponse
	 *            the response
	 * 
	 * @return 1 or 0
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward incident(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		int pageFlag = MspOrganizer.getEscalationLevel(
				EscalationLevel.HELP_DESK, getDataSource(request, "ilexnewDB"));
		if (logger.isDebugEnabled()) {
			logger
					.debug("incident(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Monitoring Summary:: Flag=" + pageFlag + "::::: " + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		} //$NON-NLS-1$
		PrintWriter out = response.getWriter();
		MspUtility.ajaxReturn(pageFlag, out);
		return null;
	}

}
