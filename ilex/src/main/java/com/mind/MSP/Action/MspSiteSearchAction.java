package com.mind.MSP.Action;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.MspSiteSearchOrganizer;
import com.mind.MSP.formbean.MspSiteSearchForm;
import com.mind.bean.msp.MspSiteSearchBean;
import com.mind.common.dao.Authenticationdao;

// TODO: Auto-generated Javadoc
/**
 * The Class MspSearchHistoryAction.
 */
public class MspSiteSearchAction extends com.mind.common.IlexDispatchAction {

	/** Logger for this class. */
	private static final Logger logger = Logger
			.getLogger(MspSiteSearchAction.class);

	/*
	 * this method is called initially when the search site page is clicked
	 * 
	 * @see
	 * org.apache.struts.actions.DispatchAction#unspecified(org.apache.struts
	 * .action.ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid,
				"Monitoring Services", "Monitoring Services Page",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
															// Security.
		}
		/* Page Security=: End */

		session.setAttribute("flag", "0");
		MspSiteSearchForm siteSearchForm = (MspSiteSearchForm) form;
		MspSiteSearchBean siteSearchBean = new MspSiteSearchBean();
		MspSiteSearchOrganizer.setCustomerList(siteSearchBean,
				getDataSource(request, "ilexnewDB"));
		try {
			MspSiteSearchOrganizer.setFormFromSiteSearchBean(siteSearchForm,
					siteSearchBean);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			logger.warn(
					"Could not map form elements to bean elements for MSP Site Search :",
					e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			logger.warn(
					"Could not map form elements to bean elements for MSP Site Search :",
					e);
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (StringUtils.isNotBlank(request.getParameter("source"))
				&& request.getParameter("source").equalsIgnoreCase("HD")) {
			request.setAttribute("fromHD", true);
		}

		return mapping.findForward("success");
	}

	/**
	 * This method is called when search button is clicked and used to retreive
	 * the list of rows that satisfy the choosen criteria .
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the action forward
	 */
	public ActionForward search(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String flagChange = (String) session.getAttribute("flag");
		flagChange = "1";
		session.setAttribute("flag", flagChange);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid,
				"Monitoring Services", "Monitoring Services Page",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
															// Security.
		}
		/* Page Security=: End */
		MspSiteSearchForm siteSearchForm = (MspSiteSearchForm) form;
		MspSiteSearchBean siteSearchBean = new MspSiteSearchBean();
		boolean fromHd = siteSearchForm.isFromHD();

		try {
			MspSiteSearchOrganizer.setSiteSearchBeanFromForm(siteSearchForm,
					siteSearchBean);
			MspSiteSearchOrganizer.setSiteSearch(siteSearchBean,
					getDataSource(request, "ilexnewDB"));
			MspSiteSearchOrganizer.setFormFromSiteSearchBean(siteSearchForm,
					siteSearchBean);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			logger.warn(
					"Could not map form elements to bean elements for MSP Site Search :",
					e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			logger.warn(
					"Could not map form elements to bean elements for MSP Site Search :",
					e);
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (fromHd) {
			request.setAttribute("fromHD", true);
		}
		return mapping.findForward("success");
	}

}
