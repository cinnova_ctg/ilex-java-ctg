package com.mind.MSP.Action;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.MSPCCReportCheckListBean;
import com.mind.MSP.business.MSPCCReportCheckListConverter;
import com.mind.MSP.business.MSPCCReportCheckListOrganizer;
import com.mind.MSP.formbean.ManageCheckListDataForm;
import com.mind.common.IlexDispatchAction;
import com.mind.fw.lang.ExceptionHelper;

public class ManageCheckListDataAction extends IlexDispatchAction{

	/*
	 * 
	 */
	private static final Logger logger = Logger.getLogger(ManageCheckListDataAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse responce) {
		
		ManageCheckListDataForm manageCheckListDataForm = (ManageCheckListDataForm) form;
		MSPCCReportCheckListBean mspccReportCheckListBean = new MSPCCReportCheckListBean();
		
		if(request.getParameter("appendixId")!= null) {
			manageCheckListDataForm.setAppendixId(request.getParameter("appendixId"));
			mspccReportCheckListBean.setAppendixId(manageCheckListDataForm.getAppendixId());
		}
		if(request.getParameter("jobId")!= null) {
			manageCheckListDataForm.setJobId(request.getParameter("jobId"));
			mspccReportCheckListBean.setJobId(manageCheckListDataForm.getJobId());
		}
		if(request.getParameter("div")!= null) {
			request.setAttribute("div", request.getParameter("div"));	
		}
		if(request.getParameter("rowNumber")!= null) {
			request.setAttribute("rowNumber", request.getParameter("rowNumber"));	
		}
		
		MSPCCReportCheckListOrganizer.setCheckListData(mspccReportCheckListBean, getDataSource(request,"ilexnewDB"));
		try {
			MSPCCReportCheckListConverter.setFormFromMSPCCReportCheckListBean(mspccReportCheckListBean, manageCheckListDataForm);
		} catch (IllegalAccessException e) {
			logger.error(ExceptionHelper.getStackTrace(e));
		} catch (InvocationTargetException e) {
			logger.error(ExceptionHelper.getStackTrace(e));
		}
		
		return mapping.findForward("manageCheckListData");
	}
	
	public ActionForward chengeStatus (ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse responce) {
		return mapping.findForward("manageCheckListData");
		
	}
	
}
