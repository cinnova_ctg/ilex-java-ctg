package com.mind.MSP.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.ChronicSiteDelegate;
import com.mind.MSP.formbean.ChronicSitesForm;
import com.mind.bean.msp.ChronicSitesBean;
import com.mind.common.IlexDispatchAction;

public class ChronicSitesAction extends IlexDispatchAction {
	
	private static final Logger logger = Logger.getLogger(ChronicSitesAction.class);
	
	public ActionForward unspecified(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession( true );
		String loginuserid = (String)session.getAttribute( "userid" );
		if( loginuserid == null ) {
			return( mapping.findForward( "SessionExpire" ) ); //- Check for session expired.
		}
				
		ChronicSitesForm chronicSiteForm = (ChronicSitesForm)form;
		ChronicSitesBean chronicSiteBean = new ChronicSitesBean();
		
		ChronicSiteDelegate.populateCustomerList(chronicSiteBean, getDataSource(request, "ilexnewDB"));
		ChronicSiteDelegate.setCustomerListToForm(chronicSiteForm, chronicSiteBean);
		
		return mapping.findForward("success");
	}
	
	
	public ActionForward view(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession( true );
		String loginuserid = (String)session.getAttribute( "userid" );
		
		if( loginuserid == null ) {
			return( mapping.findForward( "SessionExpire" ) ); //- Check for session expired.
		}
		
		ChronicSitesForm chronicSiteForm = (ChronicSitesForm)form;
		ChronicSitesBean chronicSiteBean = new ChronicSitesBean();
		
		ChronicSiteDelegate.convertFormToBean(chronicSiteForm, chronicSiteBean);
		ChronicSiteDelegate.viewChronicSites(chronicSiteBean, getDataSource(request, "ilexnewDB"));
		ChronicSiteDelegate.convertBeanToForm(chronicSiteForm, chronicSiteBean);
		
		request.setAttribute("viewResult", "view");
		return mapping.findForward("success");
	}
}
