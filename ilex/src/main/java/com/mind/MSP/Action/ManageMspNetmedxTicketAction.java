package com.mind.MSP.Action;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.ManageMspNetmedxTicketBean;
import com.mind.MSP.business.ManageMspNetmedxTicketOrganizer;
import com.mind.MSP.common.MspUtility;
import com.mind.MSP.dao.ManageOutageDao;
import com.mind.MSP.formbean.ManageMspNetmedxTicketForm;
import com.mind.bean.msp.TicketInformation;
import com.mind.common.dao.Authenticationdao;
import com.mind.msp.common.MspNetMedxTicketType;

/**
 * The Class ManageMspNetmedxTicketAction.
 * 
 * @author vijay kumar singh
 */
public class ManageMspNetmedxTicketAction extends
		com.mind.common.IlexDispatchAction {

	private static final Logger logger = Logger
			.getLogger(MspNetmedxTicketAction.class);

	/**
	 * Purpose: Initial load.
	 * 
	 * @throws Exception
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified");
		}
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		/* Check for session expired. */
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire"));
		}
		/* - Check for Pager Security. */
		if (!Authenticationdao.getPageSecurity(loginuserid,
				"Monitoring Services", "Monitoring Services Page",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		ManageMspNetmedxTicketForm mspNetmedxTicketForm = (ManageMspNetmedxTicketForm) form;
		DataSource ds = getDataSource(request, "ilexnewDB");
		ManageMspNetmedxTicketBean mspNetmedxTicketBean = new ManageMspNetmedxTicketBean();
		ManageMspNetmedxTicketOrganizer.setDefaultValue(request
				.getParameter("ticketId"), request.getParameter("jobId"),
				mspNetmedxTicketBean, ds);
		ManageMspNetmedxTicketOrganizer.copyFormFromBean(mspNetmedxTicketForm,
				mspNetmedxTicketBean);
		return mapping.findForward("manageMspNetmedxTicket");
	}

	/**
	 * Purpose: Set ticket to close.
	 * 
	 * @throws Exception
	 */
	public ActionForward closeTicket(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("closeTicket");
		}
		HttpSession session = request.getSession(true);
		String loginUserId = (String) session.getAttribute("userid");
		ManageMspNetmedxTicketForm mspNetmedxTicketForm = (ManageMspNetmedxTicketForm) form;
		DataSource ds = getDataSource(request, "ilexnewDB");
		ManageMspNetmedxTicketBean mspNetmedxTicketBean = new ManageMspNetmedxTicketBean();
		TicketInformation ticketInformation = new TicketInformation();
		mspNetmedxTicketBean.setTicketInformation(ticketInformation);
		ManageMspNetmedxTicketOrganizer.copyBeanFromForm(mspNetmedxTicketBean,
				mspNetmedxTicketForm);
		/* set ticket to close. */
		int createTicketFlag = ManageOutageDao.setTicketToClosed(loginUserId,
				mspNetmedxTicketBean.getTicketInformation(),
				MspNetMedxTicketType.INVOICE_NUMBER.getCode(), ds);
		request.setAttribute("createTicketFlag", createTicketFlag);
		return mapping.findForward("mspMonitoring");
	}

	/**
	 * Purpose: Update estimated effort. This method is called through Ajax.
	 * 
	 * @throws Exception
	 */
	public void updateEffort(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/* Get request parameters */
		String jobId = request.getParameter("jobId");
		String effort = request.getParameter("estimatedEffort");
		String ticketId = request.getParameter("ticketId");
		if (logger.isDebugEnabled()) {
			logger.debug("jobId=" + jobId + ",  effort=" + effort
					+ ", ticketId=" + ticketId);
		}
		HttpSession session = request.getSession(true);
		String loginUserId = (String) session.getAttribute("userid");
		DataSource ds = getDataSource(request, "ilexnewDB");
		/* Update estimated effort */
		int updatedFlag = ManageMspNetmedxTicketOrganizer.setEstimatedEffort(
				jobId, effort, loginUserId, ds);
		TreeMap<String, String> data = new TreeMap<String, String>();
		data.put("updateflag", String.valueOf(updatedFlag));
		TicketInformation ticketInfo = ManageMspNetmedxTicketOrganizer
				.getTicketInfo(ticketId, ds);
		data.put("estimatedEffort", ticketInfo.getEstimatedEffort());
		MspUtility mspUtility = new MspUtility();
		List<TreeMap<String, String>> dataList = new ArrayList<TreeMap<String, String>>();
		dataList.add(data);
		String returnData = mspUtility.getJSONString(dataList);
		if (logger.isDebugEnabled())
			logger.debug(returnData);
		response.getWriter().print(returnData);
	}

}
