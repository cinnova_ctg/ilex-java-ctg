package com.mind.MSP.Action;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.ManageOutageBean;
import com.mind.MSP.business.ManageOutageOrganizer;
import com.mind.MSP.common.MspUtility;
import com.mind.MSP.formbean.ManageOutageForm;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.IM.IMdao;
import com.mind.newjobdb.dao.JobDAO;

public class ManageOutageAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IMdao.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.struts.actions.DispatchAction#unspecified(org.apache.struts
	 * .action.ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
			// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid,
				"Monitoring Services", "Monitoring Services Page",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
			// Security.
		}
		/* Page Security=: End */
		ManageOutageForm manageOutageForm = (ManageOutageForm) form;
		setRequestParameter(request, manageOutageForm);

		ManageOutageBean manageOutageBean = new ManageOutageBean();
		ManageOutageOrganizer.setManageOutage(manageOutageBean, Integer
				.parseInt(manageOutageForm.getEscalationLevel()), Integer
				.parseInt(manageOutageForm.getNActiveMonitorStateChangeID()),
				Integer.parseInt(manageOutageForm.getWugInstance()),
				getDataSource(request, "ilexnewDB"));

		/* Convert simple bean to fromBean */
		ManageOutageOrganizer.setFormFromManageOutage(manageOutageForm,
				manageOutageBean);
		manageOutageForm.setNextAction(JobDAO.getNextAction(
				manageOutageForm.getNActiveMonitorStateChangeID(),
				manageOutageForm.getWugInstance(),
				getDataSource(request, "ilexnewDB")));
		return mapping.findForward("success"); // -
		// /MSP/MspManageOutageContainer.jsp
	}

	/**
	 * Resolved by help desk.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward resolvedByHelpDesk(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		HttpSession session = request.getSession(true);
		String loginUser = (String) session.getAttribute("userid");
		ManageOutageForm manageOutageForm = (ManageOutageForm) form;
		setRequestParameter(request, manageOutageForm);
		ManageOutageBean manageOutageBean = new ManageOutageBean();
		ManageOutageOrganizer.setManageOutageBeanFromForm(manageOutageForm,
				manageOutageBean);
		int resolvedHelpDeskFlag = ManageOutageOrganizer.setResolvedHelpDesk(
				manageOutageForm.getNActiveMonitorStateChangeID(),
				manageOutageForm.getWugInstance(),
				manageOutageBean.getTicketInformation(), loginUser,
				getDataSource(request, "ilexnewDB"));

		request.setAttribute("resolvedHelpDeskFlag", resolvedHelpDeskFlag);
		return mapping.findForward("resolvedByHelpDesk"); // -
		// /MSP/MspManageOutageContainer.jsp
	}

	/**
	 * Sets the request parameter into form bean.
	 * 
	 * @param request
	 *            the request
	 * @param form
	 *            the form
	 */
	private void setRequestParameter(HttpServletRequest request,
			ManageOutageForm form) {
		form.setEscalationLevel(request.getParameter("escalationLevel"));
		form.setNActiveMonitorStateChangeID(request
				.getParameter("NActiveMonitorStateChangeID"));
		form.setWugInstance(request.getParameter("wugInstance"));
		if (form.getEscalationLevel() != null
				&& form.getEscalationLevel().equals("2")) {
			form.setDisableButton(true);
		}
		if (request.getParameter("fromPage") != null
				&& request.getParameter("fromPage").equalsIgnoreCase(
						"searchPage")) {
			form.setDisableButton(true);
		}
	}

	public ActionForward escalateToMSPDispatch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession(true);
		String loginUser = (String) session.getAttribute("userid");
		ManageOutageForm manageOutageForm = (ManageOutageForm) form;
		setRequestParameter(request, manageOutageForm);
		ManageOutageBean manageOutageBean = new ManageOutageBean();
		// ManageOutageOrganizer.getEstimatedEfforts(manageOutageForm.getNActiveMonitorStateChangeID(),
		// getDataSource(request, "ilexnewDB"));
		ManageOutageOrganizer.setManageOutageBeanFromForm(manageOutageForm,
				manageOutageBean);
		int escalateToMSP = ManageOutageOrganizer.setEscalateMSPDispatch(
				manageOutageForm.getNActiveMonitorStateChangeID(),
				manageOutageBean.getTicketInformation(),
				manageOutageForm.getWugInstance(), loginUser,
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("escalateToMSPDispatch", escalateToMSP);
		return mapping.findForward("escalateToMSP");
	}

	public void addInstallationNotesAndUpdateBackupCircuitStatus(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession(true);
		String loginUser = (String) session.getAttribute("userid");
		ManageOutageForm manageOutageForm = (ManageOutageForm) form;
		ManageOutageBean manageOutageBean = new ManageOutageBean();
		ManageOutageOrganizer.setManageOutageBeanFromForm(manageOutageForm,
				manageOutageBean);

		ManageOutageOrganizer.addInstallationNotesAndUpdateBackupCircuitStatus(
				manageOutageForm.getJobId(),
				manageOutageForm.getInstallationNotes(), loginUser,
				manageOutageBean.getTicketInformation(),
				manageOutageForm.getNActiveMonitorStateChangeID(),
				manageOutageForm.getWugInstance(),
				getDataSource(request, "ilexnewDB"));

		String xml = JobDAO.setLatestInstallNotes(manageOutageForm.getJobId(),
				getDataSource(request, "ilexnewDB"));
		response.setContentType("application/xml; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(xml);
		} catch (Exception e) {
			logger.error(
					"addInstallationNotesAndUpdateBackupCircuitStatus(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"addInstallationNotesAndUpdateBackupCircuitStatus(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}
	}

	public ActionForward updateResolutionAndActions(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession(true);
		String loginUser = (String) session.getAttribute("userid");
		String emailForm = (String) session.getAttribute("useremail");
		String emailFromName = (String) session.getAttribute("username");
		ManageOutageForm manageOutageForm = (ManageOutageForm) form;
		String currentStatus = manageOutageForm.getCurrentStatus();
		String backUpCircuitStatus = manageOutageForm.getBackupCircuitStatus();
		ManageOutageBean manageOutageBean = new ManageOutageBean();
		ManageOutageOrganizer.setManageOutageBeanFromForm(manageOutageForm,
				manageOutageBean);
		if ((currentStatus.equalsIgnoreCase("DOWN"))
				&& backUpCircuitStatus.equalsIgnoreCase("0")) {
			ManageOutageOrganizer.sendEmailForDownStatus(emailForm,
					emailFromName, manageOutageForm.getEscalationLevel(),
					manageOutageForm.getNActiveMonitorStateChangeID(),
					manageOutageForm.getWugInstance(),
					getDataSource(request, "ilexnewDB"));
		}

		int updateResActions = ManageOutageOrganizer.setUpdateInfo(
				manageOutageForm.getNActiveMonitorStateChangeID(),
				manageOutageBean.getTicketInformation(),
				manageOutageForm.getWugInstance(),
				getDataSource(request, "ilexnewDB"));
		int effortInsertStatus = 0;
		if (updateResActions != 0) {
			effortInsertStatus = ManageOutageOrganizer.saveEfforts(
					manageOutageBean.getTicketInformation(), loginUser,
					getDataSource(request, "ilexnewDB"));
		}
		if (effortInsertStatus != 0) {
			effortInsertStatus = 1;

		}
		request.setAttribute("updateResActionsFlag", effortInsertStatus);
		return mapping.findForward("sendUpdateResActions");
	}

	public ActionForward updateNextActionIdActions(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession(true);
		ManageOutageForm manageOutageForm = (ManageOutageForm) form;

		String nextAction = manageOutageForm.getNextAction();
		String nActiveMonitorStateChangeID = manageOutageForm
				.getNActiveMonitorStateChangeID();
		String wugInstance = manageOutageForm.getWugInstance();

		ManageOutageBean manageOutageBean = new ManageOutageBean();
		ManageOutageOrganizer.setManageOutageBeanFromForm(manageOutageForm,
				manageOutageBean);

		int count = ManageOutageOrganizer.updateNextAction(nextAction,
				nActiveMonitorStateChangeID, wugInstance,
				getDataSource(request, "ilexnewDB"));

		manageOutageForm.setNextActionMsg("Could not updated");
		if (count > 0) {
			manageOutageForm.setNextActionMsg("Updated");
		}
		return mapping.findForward("sendUpdateResActions");
	}

	public ActionForward countDispatches(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int count = 0;

		if (request.getParameter("jobId") != null) {
			String jobId = request.getParameter("jobId");

			if (jobId == null || jobId.equals("")) {
				jobId = "0";
			}

			count = ManageOutageOrganizer.dispatchCount(
					Integer.parseInt(jobId),
					getDataSource(request, "ilexnewDB"));
		}
		PrintWriter out = response.getWriter();
		MspUtility.ajaxReturn(count, out);
		return null;
	}

}
