package com.mind.MSP.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.BouncesBean;
import com.mind.MSP.business.MspOrganizer;
import com.mind.MSP.formbean.MspBouncesForm;
import com.mind.dao.IM.IMdao;

/**
 * The Class MspBouncesAction.
 */
public class MspBouncesAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IMdao.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MspBouncesForm mspBouncesForm = (MspBouncesForm) form;
		BouncesBean bouncesBean = new BouncesBean();
		// Changes Done On 20 December 2010 By Yogendra Pratap Singh
		// Code Commented For Removing Bounces Section From the Screen For Now
		// bouncesBean.setBouncesList(MspOrganizer.getBouncesList(getDataSource(
		// request, "ilexnewDB")));
		// Changes Ends
		MspOrganizer.setFormFromBouncesBean(mspBouncesForm, bouncesBean);
		return mapping.findForward("success"); // - /MSP/MspMonitor.jsp
	}

}
