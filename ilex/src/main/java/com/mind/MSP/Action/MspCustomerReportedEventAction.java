package com.mind.MSP.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.MspCustomerReportedEventBean;
import com.mind.MSP.business.MspOrganizer;
import com.mind.MSP.formbean.MspCustomerReportedEventForm;

/**
 * The Class MspCustomerReportedEventAction.
 * 
 * @author Vijay Kumar Singh
 */
public class MspCustomerReportedEventAction extends
		com.mind.common.IlexDispatchAction {

	private static final Logger logger = Logger
			.getLogger(MspNetmedxTicketAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified");
		}
		HttpSession session = request.getSession(true);
		MspCustomerReportedEventForm customerReportedEventForm = (MspCustomerReportedEventForm) form;
		DataSource ds = getDataSource(request, "ilexnewDB");
		MspCustomerReportedEventBean customerReportedEventBean = new MspCustomerReportedEventBean();
		customerReportedEventBean.setMspNetmedxTicketList(MspOrganizer
				.setMspNetmedixTicket(customerReportedEventBean, ds));
		MspOrganizer.copyFormFromMspCustomerReportedEventBean(
				customerReportedEventForm, customerReportedEventBean);
		/* Forward to-> /MSP/ MspCustomerReportedEvent.jsp */
		return mapping.findForward("mspNetMedixTicket");
	}
}
