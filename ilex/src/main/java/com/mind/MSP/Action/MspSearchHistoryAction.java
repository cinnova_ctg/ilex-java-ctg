package com.mind.MSP.Action;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.SearchHistoryOrganizer;
import com.mind.MSP.formbean.MspSearchHistoryForm;
import com.mind.bean.msp.SearchHistoryBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.IM.IMdao;

// TODO: Auto-generated Javadoc
/**
 * The Class MspSearchHistoryAction.
 */
public class MspSearchHistoryAction extends com.mind.common.IlexDispatchAction {

	/** Logger for this class. */
	private static final Logger logger = Logger.getLogger(IMdao.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.struts.actions.DispatchAction#unspecified(org.apache.struts
	 * .action.ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid,
				"Monitoring Services", "Monitoring Services Page",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
															// Security.
		}
		/* Page Security=: End */
		MspSearchHistoryForm historyForm = (MspSearchHistoryForm) form;
		SearchHistoryBean historyBean = new SearchHistoryBean();
		SearchHistoryOrganizer.setSearchHistory(historyBean,
				getDataSource(request, "ilexnewDB"));
		SearchHistoryOrganizer.setFormFromSearchHistoryBean(historyForm,
				historyBean);
		if (StringUtils.isNotBlank(request.getParameter("source"))
				&& request.getParameter("source").equalsIgnoreCase("HD")) {
			request.setAttribute("fromHD", true);
		}
		return mapping.findForward("success");
	}

	/**
	 * Search ticket history.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the action forward
	 */
	public ActionForward search(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid,
				"Monitoring Services", "Monitoring Services Page",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
															// Security.
		}
		/* Page Security=: End */
		MspSearchHistoryForm historyForm = (MspSearchHistoryForm) form;
		boolean fromHd = historyForm.isFromHD();
		SearchHistoryBean historyBean = new SearchHistoryBean();
		SearchHistoryOrganizer.setSearchHistoryBeanFromForm(historyForm,
				historyBean);
		SearchHistoryOrganizer.setSearchHistory(historyBean,
				getDataSource(request, "ilexnewDB"));
		SearchHistoryOrganizer.setFormFromSearchHistoryBean(historyForm,
				historyBean);
		if (fromHd) {
			request.setAttribute("fromHD", true);
		}
		return mapping.findForward("success");
	}

	/**
	 * this method searches the closed tickets on the basis of site number and
	 * customer msa id.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward searchSiteTicket(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
			// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid,
				"Monitoring Services", "Monitoring Services Page",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
			// Security.
		}
		MspSearchHistoryForm historyForm = (MspSearchHistoryForm) form;
		SearchHistoryBean historyBean = new SearchHistoryBean();
		try {
			historyForm.setCustomer(URLDecoder.decode(request
					.getParameter("customerId") == null ? "" : request
					.getParameter("customerId").toString(), "UTF8"));
			historyForm.setSite(URLDecoder.decode(request
					.getParameter("siteNumber") == null ? "" : request
					.getParameter("siteNumber").toString(), "UTF8"));

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		historyForm.setFrom("");
		historyForm.setTo("");
		SearchHistoryOrganizer.setSearchHistoryBeanFromForm(historyForm,
				historyBean);
		SearchHistoryOrganizer.setSearchHistory(historyBean,
				getDataSource(request, "ilexnewDB"));
		SearchHistoryOrganizer.setFormFromSearchHistoryBean(historyForm,
				historyBean);

		return mapping.findForward("successful");
	}
}
