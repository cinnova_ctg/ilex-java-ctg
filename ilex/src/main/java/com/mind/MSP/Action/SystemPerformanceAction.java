package com.mind.MSP.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.SystemPerformanceOrganizer;
import com.mind.MSP.formbean.SystemPerformanceFrom;
import com.mind.bean.msp.SystemPerformanceBean;
import com.mind.common.dao.Authenticationdao;

/**
 * This Class SystemPerformanceAction is used to get the data related Site
 * Count, Site Bounces and System Performance.
 */
public class SystemPerformanceAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(SystemPerformanceAction.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.struts.actions.DispatchAction#unspecified(org.apache.struts
	 * .action.ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid,
				"Monitoring Services", "Monitoring Services Page",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
															// Security.
		}
		/* Page Security=: End */
		SystemPerformanceFrom systemPerformanceFrom = (SystemPerformanceFrom) form;
		SystemPerformanceBean systemPerformanceBean = new SystemPerformanceBean();

		// Assign the form value into bean.
		setFormValueTOBean(systemPerformanceFrom, systemPerformanceBean);
		SystemPerformanceOrganizer.setSystemPerformance(systemPerformanceBean,
				getDataSource(request, "ilexnewDB"));

		SystemPerformanceOrganizer.setFormFromSystemPerformanceBean(
				systemPerformanceFrom, systemPerformanceBean);

		request
				.setAttribute("SLAGraph", systemPerformanceFrom
						.getSlaGraphXML());
		request.setAttribute("BouncesGraph", systemPerformanceFrom
				.getBouncesGraphXML());
		request.setAttribute("CountsGraph", systemPerformanceFrom
				.getCountsGraphXML());
		request.setAttribute("SLATable", systemPerformanceFrom.getSlaTable());
		request.setAttribute("CountTable", systemPerformanceFrom
				.getCountTable());
		request.setAttribute("CostTable", systemPerformanceFrom.getCostTable());

		return mapping.findForward("success");
	}

	private void setFormValueTOBean(
			SystemPerformanceFrom systemPerformanceFrom,
			SystemPerformanceBean systemPerformanceBean) {
		systemPerformanceBean
				.setCustomerId(systemPerformanceFrom.getCustomer() == null ? "0"
						: systemPerformanceFrom.getCustomer());
	}
}
