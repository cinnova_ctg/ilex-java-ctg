package com.mind.MSP.Action;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.MspSiteSearchOrganizer;
import com.mind.MSP.formbean.SiteDetailForm;
import com.mind.bean.msp.SiteDetailBean;
import com.mind.common.dao.Authenticationdao;

public class SiteDetailAction extends com.mind.common.IlexDispatchAction {

	private static final Logger logger = Logger
			.getLogger(SiteDetailAction.class);

	/**
	 * Sitedetail method is called to retrieve value corresponding to the
	 * selected Site on the basis of the Site Id .
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the action forward
	 */
	public ActionForward sitedetail(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid,
				"Monitoring Services", "Monitoring Services Page",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
															// Security.
		}
		SiteDetailForm sitedetailform = (SiteDetailForm) form;
		SiteDetailBean sitedetailbean = new SiteDetailBean();
		String siteId = "";
		String siteNumber = "";
		try {
			siteId = URLDecoder.decode(request.getParameter("siteId")
					.toString(), "UTF8");
			siteNumber = URLDecoder.decode(request.getParameter("siteNumber")
					.toString(), "UTF8");
		} catch (UnsupportedEncodingException e) {
			logger.warn(
					"Could not be able to do the decoding of the given element properly :",
					e);
		}

		sitedetailbean.setSiteId(siteId);
		sitedetailbean.setSiteNumber(siteNumber);
		MspSiteSearchOrganizer.setSiteDetail(sitedetailbean,
				getDataSource(request, "ilexnewDB"));

		MspSiteSearchOrganizer.setFormFromSiteDetailBean(sitedetailform,
				sitedetailbean);

		return mapping.findForward("success");

	}
}
