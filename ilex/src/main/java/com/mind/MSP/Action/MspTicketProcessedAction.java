package com.mind.MSP.Action;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.MspReportDelegate;
import com.mind.MSP.formbean.MspReportForm;
import com.mind.bean.msp.MspReportBean;
import com.mind.common.IlexDispatchAction;

public class MspTicketProcessedAction extends IlexDispatchAction {
private static final Logger logger = Logger.getLogger(MspTicketProcessedAction.class);
	
	public ActionForward unspecified(ActionMapping mapping, ActionForm form, 
				HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession( true );
		String loginuserid = (String)session.getAttribute( "userid" );
		if( loginuserid == null ) {
			return( mapping.findForward( "SessionExpire" ) ); //- Check for session expired.
		}
		
		return mapping.findForward("success");
	}
	
	/**
	 * Method name		: search.
	 * Description		: This is called when a search is performed to list day wise count of processed tickets in MSP. 
	 * @param mapping 	: reference of ActionMapping.
	 * @param form 		: reference of ActionForm.
	 * @param request 	: reference of HttpServletRequest.
	 * @param response 	: reference of HttpServletResponse.
	 * 
	 * @return the action forward
	 */
	public ActionForward search(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession( true );
		String loginuserid = (String)session.getAttribute( "userid" );
		if( loginuserid == null ) {
			return( mapping.findForward( "SessionExpire" ) ); //- Check for session expired.
		}
		
		MspReportForm ticketProcessedForm = (MspReportForm)form;
		MspReportBean ticketProcessedBean = new MspReportBean();
		
		try {
			MspReportDelegate.ticketProcessedFormToBean(ticketProcessedForm, ticketProcessedBean);
		} catch (IllegalAccessException e) {
			logger.warn("Could not map form elements to bean elements for MSP processed tickets:", e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.warn("Could not map form elements to bean elements for MSP processed tickets:", e);
			e.printStackTrace();
		}
		
		MspReportDelegate.searchDayWiseProcessedTickets(ticketProcessedBean, getDataSource(request, "ilexnewDB"));
		
		try {
			MspReportDelegate.ticketProcessedBeanToForm(ticketProcessedForm, ticketProcessedBean);
		} catch (IllegalAccessException e) {
			logger.warn("Could not map bean elements to form elements for MSP processed tickets:", e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.warn("Could not map bean elements to form elements for MSP processed tickets:", e);
			e.printStackTrace();
		}
		
		request.setAttribute("searchFlag", "search");
		return mapping.findForward("success");
	}

}
