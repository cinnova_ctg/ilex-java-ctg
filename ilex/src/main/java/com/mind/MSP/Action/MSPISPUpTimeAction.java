package com.mind.MSP.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.MSPISPUpTimeBean;
import com.mind.MSP.business.MSPISPUpTimeOrganizer;
import com.mind.MSP.formbean.MSPISPUpTimeForm;
import com.mind.msp.common.EscalationLevel;

public class MSPISPUpTimeAction extends com.mind.common.IlexDispatchAction{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MSPISPUpTimeAction.class);
	
	public ActionForward actionCircuitType( ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		/*Page Security: Start*/
		HttpSession session = request.getSession( true );
		String loginuserid = (String)session.getAttribute( "userid" );
		if( loginuserid == null ) {
			return( mapping.findForward( "SessionExpire" ) ); //- Check for session expired.
		}
		/*if( !Authenticationdao.getPageSecurity( loginuserid , "Monitoring Services" , "Monitoring Services Page" , getDataSource( request , "ilexnewDB" ) ) ) {
  			return( mapping.findForward( "UnAuthenticate" ) ); // - Check for Pager Security.
  		}*/
		/*Page Security=: End*/
		
		MSPISPUpTimeForm mspispUpTimeForm = (MSPISPUpTimeForm)form;
		MSPISPUpTimeBean mspispUpTimeBean = new MSPISPUpTimeBean();
		MSPISPUpTimeOrganizer.setMSPISPBean(mspispUpTimeBean, EscalationLevel.ILEX_MSP_ISP_CIRCUIT_TYPE, getDataSource(request, "ilexnewDB"));
		MSPISPUpTimeOrganizer.setFormFromMSPISPUpTime(mspispUpTimeForm, mspispUpTimeBean);
		request.setAttribute("tableData", mspispUpTimeForm.getTableData());
		request.setAttribute("pageTitle","ISP vs Circuit Type");
		
		return mapping.findForward("success"); // - /MSP/MspMonitor.jsp
	}
	
	public ActionForward actionCircuitIP(ActionMapping mapping , ActionForm form , HttpServletRequest request , HttpServletResponse response )throws Exception 
	{
		/*Page Security: Start*/
		HttpSession session = request.getSession( true );
		String loginuserid = (String)session.getAttribute( "userid" );
		if( loginuserid == null ) {
			return( mapping.findForward( "SessionExpire" ) ); //- Check for session expired.
		}
		/*if( !Authenticationdao.getPageSecurity( loginuserid , "Monitoring Services" , "Monitoring Services Page" , getDataSource( request , "ilexnewDB" ) ) ) {
  			return( mapping.findForward( "UnAuthenticate" ) ); // - Check for Pager Security.
  		}*/
		/*Page Security=: End*/
		
		MSPISPUpTimeForm mspispUpTimeForm = (MSPISPUpTimeForm)form;
		MSPISPUpTimeBean mspispUpTimeBean = new MSPISPUpTimeBean();
		MSPISPUpTimeOrganizer.setMSPISPBean(mspispUpTimeBean, EscalationLevel.ILEX_MSP_ISP_CIRCUIT_IP, getDataSource(request, "ilexnewDB"));
		MSPISPUpTimeOrganizer.setFormFromMSPISPUpTime(mspispUpTimeForm, mspispUpTimeBean);
		request.setAttribute("tableData", mspispUpTimeForm.getTableData());
		request.setAttribute("pageTitle","ISP vs Circuit IP");
		return mapping.findForward("success"); // - /MSP/MspMonitor.jsp
	}
}
