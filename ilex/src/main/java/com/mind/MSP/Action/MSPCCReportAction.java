package com.mind.MSP.Action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.MSPCCReportOrganizer;
import com.mind.MSP.formbean.MSPCCReportForm;
import com.mind.bean.msp.MSPCCReportBean;
import com.mind.bean.newjobdb.JobWorkflowItem;
import com.mind.common.Util;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.MSAdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.AssignTeamdao;
import com.mind.newjobdb.dao.JobLevelWorkflowDAO;
import com.mind.util.WebUtil;

public class MSPCCReportAction extends com.mind.common.IlexDispatchAction {

	private static final Logger logger = Logger
			.getLogger(MSPCCReportAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse responce)
			throws Exception {

		MSPCCReportForm mspCCReportForm = (MSPCCReportForm) form;
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		/* Variable for Appendix Dashboard View Selector: Start */
		ArrayList jobOwnerList = new ArrayList(); // - for list of Job Owner
		ArrayList jobStatusList = new ArrayList(); // - list of job status
		String jobSelectedStatus = ""; // - selected status
		String jobSelectedOwner = ""; // - selected owner
		String jobTempStatus = "";
		String jobTempOwner = "";
		int jobOwnerListSize = 0;
		/* Variable for Appendix Dashboard View Selector: End */

		if (request.getParameter("appendixid") != null) {
			mspCCReportForm.setAppendixid((String) request
					.getParameter("appendixid"));
			// mspCCReportForm.setAppendixid("1170");
			mspCCReportForm.setAppendixName(Appendixdao.getAppendixname(
					mspCCReportForm.getAppendixid(),
					getDataSource(request, "ilexnewDB")));
			mspCCReportForm.setMsaId(IMdao.getMSAId(
					mspCCReportForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"))); // -
			// for
			// menu
		}
		String msaName = com.mind.dao.PM.Appendixdao
				.getMsaname(getDataSource(request, "ilexnewDB"),
						mspCCReportForm.getMsaId());

		mspCCReportForm.setMsaName(msaName);

		if (request.getParameter("ownerId") != null) // - for menu
		{
			mspCCReportForm.setOwnerId(request.getParameter("ownerId"));
		}
		if (request.getParameter("viewjobtype") != null) // - for menu
		{
			mspCCReportForm.setViewjobtype(request.getParameter("viewjobtype"));
		}
		if (request.getParameter("fromPage") != null) {
			mspCCReportForm.setFromPage(request.getParameter("fromPage"));
		}

		mspCCReportForm.setPocList(AssignTeamdao.getPocList(getDataSource(
				request, "ilexnewDB")));
		mspCCReportForm.setRoleList(AssignTeamdao.getRoleList(getDataSource(
				request, "ilexnewDB")));
		mspCCReportForm.setAssignTeamList(AssignTeamdao.getAssignTeamList(
				mspCCReportForm.getAppendixid(),
				getDataSource(request, "ilexnewDB")));

		/* For Menu & View Selector: Start */

		/* Start : Appendix Dashboard View Selector Code */

		if (mspCCReportForm.getJobOwnerOtherCheck() != null
				&& !mspCCReportForm.getJobOwnerOtherCheck().equals("")) {
			session.setAttribute("jobOwnerOtherCheck",
					mspCCReportForm.getJobOwnerOtherCheck());
		}
		if (request.getParameter("opendiv") != null) {
			request.setAttribute("opendiv", request.getParameter("opendiv"));
		}
		if (request.getParameter("resetList") != null
				&& request.getParameter("resetList").equals("something")) {
			WebUtil.setDefaultAppendixDashboardAttribute(session);
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", mspCCReportForm.getAppendixid());
			request.setAttribute("msaId", mspCCReportForm.getMsaId());
			return mapping.findForward("temppage");
		}
		if (mspCCReportForm.getJobSelectedStatus() != null) {
			for (int i = 0; i < mspCCReportForm.getJobSelectedStatus().length; i++) {
				jobSelectedStatus = jobSelectedStatus + "," + "'"
						+ mspCCReportForm.getJobSelectedStatus(i) + "'";
				jobTempStatus = jobTempStatus + ","
						+ mspCCReportForm.getJobSelectedStatus(i);
			}

			jobSelectedStatus = jobSelectedStatus.substring(1,
					jobSelectedStatus.length());
			jobSelectedStatus = "(" + jobSelectedStatus + ")";
		}
		if (mspCCReportForm.getJobSelectedOwners() != null) {
			for (int j = 0; j < mspCCReportForm.getJobSelectedOwners().length; j++) {
				jobSelectedOwner = jobSelectedOwner + ","
						+ mspCCReportForm.getJobSelectedOwners(j);
				jobTempOwner = jobTempOwner + ","
						+ mspCCReportForm.getJobSelectedOwners(j);
			}
			jobSelectedOwner = jobSelectedOwner.substring(1,
					jobSelectedOwner.length());
			if (jobSelectedOwner.equals("0")) {
				jobSelectedOwner = "%";
			}
			jobSelectedOwner = "(" + jobSelectedOwner + ")";
		}
		if (request.getParameter("showList") != null
				&& request.getParameter("showList").equals("something")) {

			session.setAttribute("jobSelectedOwners", jobSelectedOwner);
			session.setAttribute("jobTempOwner", jobTempOwner);
			session.setAttribute("jobSelectedStatus", jobSelectedStatus);
			session.setAttribute("jobTempStatus", jobTempStatus);
			session.setAttribute("timeFrame",
					mspCCReportForm.getJobMonthWeekCheck());
			session.setAttribute("jobOwnerOtherCheck",
					mspCCReportForm.getJobOwnerOtherCheck());
			request.setAttribute("type", "AppendixViewSelector");
			request.setAttribute("appendixId", mspCCReportForm.getAppendixid());
			request.setAttribute("msaId", mspCCReportForm.getMsaId());

			return mapping.findForward("temppage"); // - /common/Tempupload.jsp
		} else {
			if (session.getAttribute("jobSelectedOwners") != null) {
				jobSelectedOwner = session.getAttribute("jobSelectedOwners")
						.toString();
				mspCCReportForm.setJobSelectedOwners(session
						.getAttribute("jobTempOwner").toString().split(","));
			}
			if (session.getAttribute("jobSelectedStatus") != null) {
				jobSelectedStatus = session.getAttribute("jobSelectedStatus")
						.toString();
				mspCCReportForm.setJobSelectedStatus(session
						.getAttribute("jobTempStatus").toString().split(","));
			}
			if (session.getAttribute("jobOwnerOtherCheck") != null) {
				mspCCReportForm.setJobOwnerOtherCheck(session.getAttribute(
						"jobOwnerOtherCheck").toString());
			}
			if (session.getAttribute("timeFrame") != null) {
				mspCCReportForm.setJobMonthWeekCheck(session.getAttribute(
						"timeFrame").toString());
			}
		}
		jobOwnerList = MSAdao.getOwnerList(loginuserid,
				mspCCReportForm.getJobOwnerOtherCheck(), "prj_job_new",
				mspCCReportForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		jobStatusList = MSAdao.getStatusList("prj_job_new",
				getDataSource(request, "ilexnewDB"));
		if (jobOwnerList.size() > 0)
			jobOwnerListSize = jobOwnerList.size();

		request.setAttribute("jobOwnerListSize", jobOwnerListSize + "");
		request.setAttribute("jobStatusList", jobStatusList);
		request.setAttribute("jobOwnerList", jobOwnerList);
		/* End : Appendix Dashboard View Selector Code */

		String contractDocoumentList = com.mind.dao.PM.Appendixdao
				.getcontractDocoumentMenu(mspCCReportForm.getAppendixid(),
						getDataSource(request, "ilexnewDB")); // - for addendum
		// section of
		// menu
		String appendixcurrentstatus = Appendixdao.getCurrentstatus(
				mspCCReportForm.getAppendixid(),
				getDataSource(request, "ilexnewDB")); // - Appendix
		// current
		// status

		/* Set Parameter for menu and view selector */
		request.setAttribute("appendixcurrentstatus", appendixcurrentstatus);
		request.setAttribute("contractDocMenu", contractDocoumentList);
		request.setAttribute("msaId", mspCCReportForm.getMsaId());
		request.setAttribute("viewjobtype", mspCCReportForm.getViewjobtype());
		request.setAttribute("ownerId", mspCCReportForm.getOwnerId());
		request.setAttribute("appendixid", mspCCReportForm.getAppendixid());
		request.setAttribute("appendixName", mspCCReportForm.getAppendixName());
		request.setAttribute("msaName", msaName);
		request.setAttribute("fromPage", request.getParameter("fromPage"));

		/* For Menu & View Selector: End */

		// Get the data from database ...
		if (MSPCCReportOrganizer.isConfigured(mspCCReportForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"))) {
			request.setAttribute("isConfigured", "isConfigured");
		} else {
			request.setAttribute("isConfigured", "");
		}

		MSPCCReportBean mspccReportBean = new MSPCCReportBean();

		/* Find out special msa and appendix name form properties file */
		ResourceBundle bundle = ResourceBundle
				.getBundle("com.mind.properties.ApplicationResourcesPRM");
		if (logger.isDebugEnabled()) {
			logger.debug("prm.mspccreport.split.str-> "
					+ bundle.getString("prm.mspccreport.split.str"));
			logger.debug("prm.mspccreport.msa.name-> "
					+ bundle.getString("prm.mspccreport.msa.name"));
			logger.debug("prm.mspccreport.appendix.name-> "
					+ bundle.getString("prm.mspccreport.appendix.name"));
		}
		String splitStr = bundle.getString("prm.mspccreport.split.str").trim();
		String[] msaNames = Util.getSplitArray(
				bundle.getString("prm.mspccreport.msa.name"), splitStr);
		String[] appendixNames = Util.getSplitArray(
				bundle.getString("prm.mspccreport.appendix.name"), splitStr);
		ArrayList<String> enabledMsaList = new ArrayList<String>(
				Arrays.asList(msaNames));
		ArrayList<String> enabledAppendixList = new ArrayList<String>(
				Arrays.asList(appendixNames));

		if (enabledMsaList.contains(msaName)
				&& enabledAppendixList.contains(mspCCReportForm
						.getAppendixName())) {
			MSPCCReportOrganizer.setMSPCCChickFilReport(mspccReportBean,
					mspCCReportForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
		} else {
			MSPCCReportOrganizer.setMSPCCReport(mspccReportBean,
					mspCCReportForm.getAppendixid(),
					getDataSource(request, "ilexnewDB"));
		}

		MSPCCReportOrganizer.setFormFromMSPCCReport(mspCCReportForm,
				mspccReportBean);
		request.setAttribute("mspReportEnabledMSAs", enabledMsaList);
		request.setAttribute("mspReportEnabledAppendixes", enabledAppendixList);

		return mapping.findForward("success");
	}

	public void getSectonBInfo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String appendixId = request.getParameter("appendixId");
		String appendixName = Appendixdao.getAppendixname(appendixId,
				getDataSource(request, "ilexnewDB"));
		String msaId = IMdao.getMSAId(appendixId,
				getDataSource(request, "ilexnewDB")); // -

		String msaName = com.mind.dao.PM.Appendixdao.getMsaname(
				getDataSource(request, "ilexnewDB"), msaId);

		String categoryName = request.getParameter("name");

		String jSonString = "";
		ResourceBundle bundle = ResourceBundle
				.getBundle("com.mind.properties.ApplicationResourcesPRM");
		String splitStr = bundle.getString("prm.mspccreport.split.str").trim();
		String[] msaNames = Util.getSplitArray(
				bundle.getString("prm.mspccreport.msa.name"), splitStr);
		String[] appendixNames = Util.getSplitArray(
				bundle.getString("prm.mspccreport.appendix.name"), splitStr);
		ArrayList<String> enabledMsaList = new ArrayList<String>(
				Arrays.asList(msaNames));
		ArrayList<String> enabledAppendixList = new ArrayList<String>(
				Arrays.asList(appendixNames));
		if (enabledMsaList.contains(msaName)
				&& enabledAppendixList.contains(appendixName)) {
			jSonString = getJSONString(MSPCCReportOrganizer
					.getChickFilSectionBInfo(
							request.getParameter("appendixId"),
							request.getParameter("titleId"), categoryName,
							getDataSource(request, "ilexnewDB")));
		} else {
			jSonString = getJSONString(MSPCCReportOrganizer.getSectionBInfo(
					request.getParameter("appendixId"),
					request.getParameter("titleId"),
					getDataSource(request, "ilexnewDB")));
		}

		response.getWriter().print(jSonString);
	}

	private String getJSONString(List<TreeMap<String, String>> markerList) {
		JSONArray jArray = new JSONArray();
		for (TreeMap<String, String> treeMap : markerList) {
			JSONObject jObj = new JSONObject();
			jObj.putAll(treeMap);
			jArray.add(jObj);
		}
		if (logger.isDebugEnabled()) {
			logger.debug(jArray.toString());

		}
		return jArray.toString();
	}

	public void setCheckListWorkFlow(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse responce)
			throws IOException {
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		JobWorkflowItem jobWorkflowItem = new JobWorkflowItem();
		if (request.getParameter("itemId") != null) {
			jobWorkflowItem.setItemId(Long.parseLong(request
					.getParameter("itemId")));
		}
		JobLevelWorkflowDAO.completeItemStatus(jobWorkflowItem,
				getDataSource(request, "ilexnewDB"), loginuserid);
		responce.getWriter().print("");
	}

	public ActionForward taskCompleted(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse responce)
			throws Exception {

		MSPCCReportForm mspCCReportForm = (MSPCCReportForm) form;

		if (request.getParameter("appendixid") != null) {
			mspCCReportForm.setAppendixid((String) request
					.getParameter("appendixid"));
			// mspCCReportForm.setAppendixid("1170");
		}

		MSPCCReportBean mspccReportBean = new MSPCCReportBean();
		MSPCCReportOrganizer.setMSPCCReportCompletedTaskList(mspccReportBean,
				mspCCReportForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		MSPCCReportOrganizer.setFormFromMSPCCReport(mspCCReportForm,
				mspccReportBean);
		return mapping.findForward("taskCompleted");
	}

	public ActionForward pendingAndCompleteTask(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) throws Exception {

		MSPCCReportForm mspCCReportForm = (MSPCCReportForm) form;

		if (request.getParameter("appendixid") != null) {
			mspCCReportForm.setAppendixid((String) request
					.getParameter("appendixid"));
			// mspCCReportForm.setAppendixid("1170");
		}

		MSPCCReportBean mspccReportBean = new MSPCCReportBean();
		MSPCCReportOrganizer.setMSPCCReportPendingAndCompleteList(
				mspccReportBean, mspCCReportForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		MSPCCReportOrganizer.setFormFromMSPCCReport(mspCCReportForm,
				mspccReportBean);
		return mapping.findForward("pendingAndCompleted");
	}

	public ActionForward activityTrends(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse responce)
			throws Exception {
		MSPCCReportForm mspCCReportForm = (MSPCCReportForm) form;

		if (request.getParameter("appendixid") != null) {
			mspCCReportForm.setAppendixid((String) request
					.getParameter("appendixid"));
			// mspCCReportForm.setAppendixid("1170");
		}

		MSPCCReportBean mspccReportBean = new MSPCCReportBean();
		MSPCCReportOrganizer.setMSPCCReportActivityTrendList(mspccReportBean,
				mspCCReportForm.getAppendixid(),
				getDataSource(request, "ilexnewDB"));
		MSPCCReportOrganizer.setFormFromMSPCCReport(mspCCReportForm,
				mspccReportBean);
		return mapping.findForward("activityTrends");
	}

}