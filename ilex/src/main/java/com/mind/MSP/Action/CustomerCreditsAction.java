package com.mind.MSP.Action;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.CustomerCreditDelegate;
import com.mind.MSP.formbean.MspReportForm;
import com.mind.bean.msp.MspReportBean;
import com.mind.common.IlexDispatchAction;

public class CustomerCreditsAction extends IlexDispatchAction {
	
	private static final Logger logger = Logger.getLogger(CustomerCreditsAction.class);
	
	public ActionForward unspecified(ActionMapping mapping, ActionForm form, 
				HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession( true );
		String loginuserid = (String)session.getAttribute( "userid" );
		if( loginuserid == null ) {
			return( mapping.findForward( "SessionExpire" ) ); //- Check for session expired.
		}
		
		/* if( !Authenticationdao.getPageSecurity( loginuserid , "Monitoring Services" , "Customer Credits" , getDataSource( request , "ilexnewDB" ) ) ) {
  			return( mapping.findForward( "UnAuthenticate" ) ); // - Check for Pager Security.
  		} */
		
		MspReportForm custCreditsForm = (MspReportForm)form;
		MspReportBean custCreditsBean = new MspReportBean();
		
		handleRequestParams(custCreditsForm, request);
		
		CustomerCreditDelegate.fillCustomerListforBean(custCreditsBean, getDataSource(request, "ilexnewDB"));
		CustomerCreditDelegate.setCustomerListToForm(custCreditsForm, custCreditsBean);
		
		return mapping.findForward("success");
	}

	public static void handleRequestParams(MspReportForm form, HttpServletRequest request) {
		if(request.getParameter("reportType") != null) {
			form.setReportType(request.getParameter("reportType"));
		}
	}
	
	/**
	 * Method name	: search.
	 * Description	: This is called when a search is performed on Customer Credits page. 
	 * @param mapping 	:	reference of ActionMapping.
	 * @param form 		:	reference of ActionForm.
	 * @param request 	:	reference of HttpServletRequest.
	 * @param response 	:	reference of HttpServletResponse.
	 * 
	 * @return the action forward
	 */
	public ActionForward search(ActionMapping mapping , ActionForm form , 
				HttpServletRequest request , HttpServletResponse response ){
		
		HttpSession session = request.getSession( true );
		String loginuserid = (String)session.getAttribute( "userid" );
		if( loginuserid == null ) {
			return( mapping.findForward( "SessionExpire" ) ); //- Check for session expired.
		}
		
		/* if( !Authenticationdao.getPageSecurity( loginuserid , "Monitoring Services" , "Customer Credits" , getDataSource( request , "ilexnewDB" ) ) ) {
  			return( mapping.findForward( "UnAuthenticate" ) ); // - Check for Pager Security.
  		} */
		
		MspReportForm custCreditsForm = (MspReportForm)form;
		MspReportBean custCreditsBean = new MspReportBean();
		
		try {
			CustomerCreditDelegate.convertFormToBean(custCreditsForm, custCreditsBean);
		} catch (IllegalAccessException e) {
			logger.warn("Could not map form elements to bean elements for MSP Customer Credits Report:", e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.warn("Could not map form elements to bean elements for MSP Customer Credits Report:", e);
			e.printStackTrace();
		}
		
		CustomerCreditDelegate.getCreditsSearch(custCreditsBean, getDataSource(request, "ilexnewDB"));
		
		try {
			CustomerCreditDelegate.convertBeanToForm(custCreditsForm, custCreditsBean);
		} catch (IllegalAccessException e) {
			logger.warn("Could not map bean elements to form elements for MSP Customer Credits Report:", e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.warn("Could not map bean elements to form elements for MSP Customer Credits Report:", e);
			e.printStackTrace();
		}
		
		request.setAttribute("searchFlag", "search");
		
		return mapping.findForward("success");
	}
}
