package com.mind.MSP.Action;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.ShippingInstruction;
import com.mind.MSP.business.ShippingInstructionBean;
import com.mind.MSP.business.ShippingInstructionConverter;
import com.mind.MSP.formbean.ManageShippingInstructionForm;
import com.mind.common.IlexDispatchAction;
import com.mind.fw.lang.ExceptionHelper;

public class ManageShippingInstructionAction extends IlexDispatchAction {

	/*
	 * 
	 */
	private static final Logger logger =
			Logger.getLogger(ManageShippingInstructionAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse responce) {

		ManageShippingInstructionForm manageShippingInstructionForm =
				(ManageShippingInstructionForm) form;
		ShippingInstructionBean shippingInstructionBean =
				new ShippingInstructionBean();

		if (request.getParameter("appendixId") != null) {
			manageShippingInstructionForm.setAppendixId(request
					.getParameter("appendixId"));
			shippingInstructionBean.setAppendixId(manageShippingInstructionForm
					.getAppendixId());
		}
		if (request.getParameter("jobId") != null) {
			manageShippingInstructionForm.setJobId(request
					.getParameter("jobId"));
			shippingInstructionBean.setJobId(manageShippingInstructionForm
					.getJobId());
		}
		if (request.getParameter("div") != null) {
			request.setAttribute("div", request.getParameter("div"));
		}
		if (request.getParameter("rowNumber") != null) {
			request
					.setAttribute("rowNumber", request
							.getParameter("rowNumber"));
		}

		shippingInstructionBean =
				ShippingInstruction.setShippingInstructionData(
						shippingInstructionBean, getDataSource(request,
								"ilexnewDB"));
		try {
			ShippingInstructionConverter.setFormFromShippingInstructionBean(
					shippingInstructionBean, manageShippingInstructionForm);
		} catch (IllegalAccessException e) {
			logger.error(ExceptionHelper.getStackTrace(e));
		} catch (InvocationTargetException e) {
			logger.error(ExceptionHelper.getStackTrace(e));
		}

		request.setAttribute("resourceSize", manageShippingInstructionForm
				.getResourceArr().size());
		return mapping.findForward("manageShippingInstruction");
	}
}
