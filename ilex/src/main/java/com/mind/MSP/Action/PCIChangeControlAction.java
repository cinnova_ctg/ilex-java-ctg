package com.mind.MSP.Action;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.PCIChangeControlOrganiser;
import com.mind.MSP.dao.PCIChangeControlDAO;
import com.mind.MSP.formbean.PCIChangeControlForm;
import com.mind.bean.msp.ChangeRequestActionDetails;
import com.mind.bean.msp.PCIChangeControlBean;
import com.mind.common.IlexDispatchAction;
import com.mind.common.LabelValue;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.ExceptionHelper;
import com.mind.fw.lang.SysException;

public class PCIChangeControlAction extends IlexDispatchAction {

	private static final Logger logger = Logger
			.getLogger(PCIChangeControlAction.class);

	/*
	 * @author LALIT GOYAL
	 * 
	 * 
	 * Default action called First time Action is called
	 * 
	 * @see org.apache.struts.actions.DispatchAction
	 * #unspecified(org.apache.struts.action.ActionMapping,
	 * org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		DataSource ds = getDataSource(request, "ilexnewDB");
		PCIChangeControlForm pciChangeControlForm = (PCIChangeControlForm) form;
		PCIChangeControlBean pciControlBean = new PCIChangeControlBean();
		try {
			PCIChangeControlOrganiser.setChangeRequestData(pciControlBean, ds,
					request);
		} catch (SysException e) {
			// TODO Auto-generated catch block
			logger.error("unspecified method SysException error occure."
					+ ExceptionHelper.getStackTrace(e));
		}
		try {
			PCIChangeControlOrganiser.setFormFromBean(pciControlBean,
					pciChangeControlForm);
		} catch (SysException e) {
			// TODO Auto-generated catch block
			logger.error("unspecified method SysException error occure."
					+ ExceptionHelper.getStackTrace(e));
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			logger.error("unspecified method SysException error occure."
					+ ExceptionHelper.getStackTrace(e));
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			logger.error("unspecified method SysException error occure."
					+ ExceptionHelper.getStackTrace(e));
		}
		pciChangeControlForm.setSelectedChangeRequestStatus("CR_O");
		pciChangeControlForm.setOrgTopId("0");
		request.setAttribute("mode", "create");
		pciChangeControlForm.setFinalDispositionStatusSelected("FD_DE");
		return mapping.findForward("success");
	}

	/**
	 * Populate site identifier based on the Customer Selected From the Drop
	 * Down List.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward populateSiteIdentifier(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		/* Page Security: Start */
		DataSource ds = getDataSource(request, "ilexnewDB");
		List<LabelValue> siteIdentifierList = PCIChangeControlOrganiser
				.siteIdentifierList(request.getParameter("customer"), ds);
		String jsonResult = new flexjson.JSONSerializer()
				.serialize(siteIdentifierList);
		response.setContentType("text/javascript");
		response.getWriter().write(jsonResult);
		return null;

	}

	/**
	 * Submit update change request.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward submitUpdateChangeRequest(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		/* Page Security: Start */
		DataSource ds = getDataSource(request, "ilexnewDB");
		PCIChangeControlForm pciChangeControlForm = (PCIChangeControlForm) form;
		PCIChangeControlBean pciControlBean = new PCIChangeControlBean();
		PCIChangeControlOrganiser.copyBeanFromForm(pciControlBean,
				pciChangeControlForm);
		Integer result = PCIChangeControlOrganiser.saveChangeRequestData(
				pciControlBean, ds);
		ArrayList<String> al = new ArrayList<String>();
		al.add(pciControlBean.getChangeRequestID());
		al.add(pciControlBean.getSelectedSiteIdentifierName());
		String jsonResult = new flexjson.JSONSerializer().serialize(al);
		response.setContentType("text/javascript");
		response.getWriter().write(jsonResult);
		return null;

	}

	/**
	 * Pci view change request.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward pciViewChangeRequest(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		/* Page Security: Start */
		DataSource ds = getDataSource(request, "ilexnewDB");
		PCIChangeControlForm pciChangeControlForm = (PCIChangeControlForm) form;

		PCIChangeControlBean pciControlBean = new PCIChangeControlBean();
		try {
			pciControlBean = PCIChangeControlOrganiser.getChangeRequestData(
					request.getParameter("changeRequestID"), ds,
					pciControlBean, request);
		} catch (SysException e) {
			// TODO Auto-generated catch block
			logger.error("unspecified method SysException error occure."
					+ ExceptionHelper.getStackTrace(e));
		}
		try {
			PCIChangeControlOrganiser.setFormFromBean(pciControlBean,
					pciChangeControlForm);
		} catch (SysException e) {
			// TODO Auto-generated catch block
			logger.error("unspecified method SysException error occure."
					+ ExceptionHelper.getStackTrace(e));
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			logger.error("unspecified method SysException error occure."
					+ ExceptionHelper.getStackTrace(e));
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			logger.error("unspecified method SysException error occure."
					+ ExceptionHelper.getStackTrace(e));
		}
		pciChangeControlForm.setChangeRequestID(request
				.getParameter("changeRequestID"));
		request.setAttribute("mode", "view");
		return mapping.findForward("success");
	}

	// TO Save The Actions
	public ActionForward saveUpdateAction(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		/* Page Security: Start */

		PCIChangeControlForm pciChangeControlForm = (PCIChangeControlForm) form;
		PCIChangeControlBean pciControlBean = new PCIChangeControlBean();
		PCIChangeControlOrganiser.setFormFromBean(pciControlBean,
				pciChangeControlForm);
		ChangeRequestActionDetails actiondetail = new ChangeRequestActionDetails();
		actiondetail.setActionID(request.getParameter("actionID"));
		actiondetail.setActionName(request.getParameter("actionName"));
		actiondetail.setActualDate(request.getParameter("actualDate"));
		actiondetail.setPlannedDate(request.getParameter("plannedDate"));
		actiondetail.setStatus(request.getParameter("status"));

		actiondetail
				.setChangeRequestId(request.getParameter("changeRequestID"));
		actiondetail.setNotes(request.getParameter("notes"));

		DataSource ds = getDataSource(request, "ilexnewDB");

		if (request.getParameter("actionID").equalsIgnoreCase("notSaved")
				|| request.getParameter("actionID").equalsIgnoreCase("")) {
			// Saved the Action.
			actiondetail.setActionMode("A");
			PCIChangeControlOrganiser.saveActionData(ds, actiondetail);
		} else {
			// Update the Action
			actiondetail.setActionMode("U");
			PCIChangeControlOrganiser.updateActionData(ds, actiondetail);

		}

		pciChangeControlForm.setChangeRequestActionsList(PCIChangeControlDAO
				.getChangeRequestActionData(actiondetail.getChangeRequestId(),
						ds));

		String jsonResult = new flexjson.JSONSerializer()
				.serialize(pciChangeControlForm.getChangeRequestActionsList());
		response.setContentType("text/javascript");
		response.getWriter().write(jsonResult);

		return null;
	}

	/**
	 * Delete action.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward deleteAction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/* Page Security: Start */

		PCIChangeControlForm pciChangeControlForm = (PCIChangeControlForm) form;
		PCIChangeControlBean pciControlBean = new PCIChangeControlBean();
		PCIChangeControlOrganiser.setFormFromBean(pciControlBean,
				pciChangeControlForm);
		ChangeRequestActionDetails actiondetail = new ChangeRequestActionDetails();
		actiondetail.setActionID(request.getParameter("actionID"));
		actiondetail.setActionName(request.getParameter("actionName"));
		actiondetail.setActualDate(request.getParameter("actualDate"));
		actiondetail.setPlannedDate(request.getParameter("plannedDate"));
		actiondetail.setStatus(request.getParameter("status"));

		actiondetail
				.setChangeRequestId(request.getParameter("changeRequestID"));
		actiondetail.setNotes(request.getParameter("notes"));
		actiondetail.setActionMode("D");
		DataSource ds = getDataSource(request, "ilexnewDB");

		// Update the Action
		Integer errorCode = PCIChangeControlOrganiser.deleteActionData(ds,
				actiondetail);
		if (errorCode == -9903) {
			response.sendError(errorCode, "errorCode");
			throw new AppException(Integer.toString(errorCode));

		}

		String jsonResult = new flexjson.JSONSerializer().serialize(errorCode);
		response.setContentType("text/javascript");
		response.getWriter().write(jsonResult);

		return null;
	}

	/**
	 * Submit final disposition.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward submitFinalDisposition(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		/* Page Security: Start */

		PCIChangeControlForm pciChangeControlForm = (PCIChangeControlForm) form;
		PCIChangeControlBean pciControlBean = new PCIChangeControlBean();
		try {
			PCIChangeControlOrganiser.copyBeanFromForm(pciControlBean,
					pciChangeControlForm);
		} catch (IllegalAccessException e) {
			logger.error("submitFinalDisposition method IllegalAccessException Exception occure."
					+ ExceptionHelper.getStackTrace(e));
		} catch (InvocationTargetException e) {
			logger.error("submitFinalDisposition method InvocationTargetException Exception occure."
					+ ExceptionHelper.getStackTrace(e));
		}
		DataSource ds = getDataSource(request, "ilexnewDB");

		// Update the Action
		Integer result = 0;
		try {
			result = PCIChangeControlOrganiser.saveFinalDisposition(
					pciControlBean, ds, request);
		} catch (SysException e) {
			logger.error("unspecified method SysException error occure."
					+ ExceptionHelper.getStackTrace(e));
		}
		try {
			pciControlBean = PCIChangeControlOrganiser.getChangeRequestData(
					pciControlBean.getChangeRequestID(), ds, pciControlBean,
					request);
		} catch (SysException e) {
			logger.error("submitFinalDisposition method SysException error occure."
					+ ExceptionHelper.getStackTrace(e));
		}
		if (result != 0) {
			request.setAttribute("errorMessage", "9002");

			PCIChangeControlOrganiser.setFormDataFromBean(pciControlBean,
					pciChangeControlForm);

		} else {
			request.setAttribute("errorMessage", "0");
			try {
				PCIChangeControlOrganiser.setFormFromBean(pciControlBean,
						pciChangeControlForm);
			} catch (SysException e) {
				logger.error("unspecified method SysException error occure."
						+ ExceptionHelper.getStackTrace(e));
			} catch (IllegalAccessException e) {
				logger.error("unspecified method SysException error occure."
						+ ExceptionHelper.getStackTrace(e));
			} catch (InvocationTargetException e) {
				logger.error("unspecified method SysException error occure."
						+ ExceptionHelper.getStackTrace(e));
			}
		}
		request.setAttribute("mode", "Update");
		return mapping.findForward("success");
	}

}
