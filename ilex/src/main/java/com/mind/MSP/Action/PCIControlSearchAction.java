/**
 * 
 */
package com.mind.MSP.Action;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.PCIChangeControlOrganiser;
import com.mind.MSP.business.PCIControlSearchOrganizer;
import com.mind.MSP.formbean.PCIControlSearchForm;
import com.mind.bean.msp.PCIControlSearchBean;
import com.mind.common.IlexDispatchAction;
import com.mind.common.LabelValue;
import com.mind.common.dao.Authenticationdao;
import com.mind.fw.lang.ExceptionHelper;
import com.mind.fw.lang.SysException;
import com.mind.util.WebUtil;

/**
 * @author yogendrasingh
 * 
 */
public class PCIControlSearchAction extends IlexDispatchAction {

	/**
	 * 
	 */
	private static final Logger logger = Logger
			.getLogger(PCIControlSearchAction.class);

	/*
	 * Method called First time the Action Called
	 * 
	 * @see
	 * org.apache.struts.actions.DispatchAction#unspecified(org.apache.struts
	 * .action.ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		if (logger.isDebugEnabled()) {
			logger.debug("Method unspecified of PCIControlSearchAction starts.");
		}
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
			// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid,
				"Monitoring Services", "Monitoring Services Page",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		PCIControlSearchForm pciControlSearchForm = (PCIControlSearchForm) form;
		DataSource ds = getDataSource(request, "ilexnewDB");
		PCIControlSearchBean pciControlBean = new PCIControlSearchBean();
		try {
			PCIControlSearchOrganizer.setChangeRequestData(pciControlBean, ds);
		} catch (SysException e) {
			logger.error("unspecified method SysException error occure."
					+ ExceptionHelper.getStackTrace(e));
		}
		try {
			PCIControlSearchOrganizer.setFormFromBean(pciControlSearchForm,
					pciControlBean);
		} catch (IllegalAccessException e) {
			logger.error("unspecified method IllegalAccessException error occure"
					+ ExceptionHelper.getStackTrace(e));
		} catch (InvocationTargetException e) {
			logger.error("unspecified method InvocationTargetException error occure"
					+ ExceptionHelper.getStackTrace(e));
		}

		return mapping.findForward("success");
	}

	/**
	 * Populate site identifier for search based on Customer Selected.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward populateSiteIdentifierForSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		/* Page Security: Start */
		DataSource ds = getDataSource(request, "ilexnewDB");
		List<LabelValue> siteIdentifierList = PCIChangeControlOrganiser
				.siteIdentifierList(request.getParameter("customer"), ds);
		String jsonResult = new flexjson.JSONSerializer()
				.serialize(siteIdentifierList);
		response.setContentType("text/javascript");
		response.getWriter().write(jsonResult);
		return null;

	}

	/**
	 * Pci control search result .
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward pciControlSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
			// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid,
				"Monitoring Services", "Monitoring Services Page",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		PCIControlSearchForm pciControlSearchForm = (PCIControlSearchForm) form;
		Map<String, String> paramMap = null;
		paramMap = WebUtil.copyRequestToParamMap(request);
		pciControlSearchForm = PCIControlSearchOrganizer
				.setPCIControlSearchFormValues(paramMap, pciControlSearchForm);

		PCIControlSearchBean pciControlSearchBean = new PCIControlSearchBean();
		PCIControlSearchOrganizer.setBeanFromPCIControlSearchForm(
				pciControlSearchForm, pciControlSearchBean);

		PCIControlSearchOrganizer.setPCISearchControlDataList(
				pciControlSearchBean, getDataSource(request, "ilexnewDB"));

		PCIControlSearchOrganizer.setFormFromBean(pciControlSearchForm,
				pciControlSearchBean);

		String jsonResult = new flexjson.JSONSerializer()
				.serialize(pciControlSearchForm.getPciChangeControlFormList());
		response.setContentType("text/javascript");
		response.getWriter().write(jsonResult);

		return null;
	}
}
