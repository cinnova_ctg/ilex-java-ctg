package com.mind.MSP.Action;

import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.MonitoringSummaryBean;
import com.mind.MSP.business.MspOrganizer;
import com.mind.MSP.common.MspUtility;
import com.mind.MSP.formbean.MspMonitoringSummaryForm;
import com.mind.dao.IM.IMdao;

/**
 * The Class MspMonitoringSummaryAction.
 */
public class MspMonitoringSummaryAction extends
		com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IMdao.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */

	/**
	 * Monitoring Summary. Called on the first time on page load.
	 * 
	 * @param ActionMapping
	 *            the mapping
	 * @param ActionForm
	 *            the form
	 * @param HttpServletRequest
	 *            the request
	 * @param HttpServletResponse
	 *            the response
	 * 
	 * @return the action forward
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession(true);
		session.removeAttribute("mspMessage");
		MspMonitoringSummaryForm monitoringSummaryForm = (MspMonitoringSummaryForm) form;
		MonitoringSummaryBean monitoringSummaryBean = new MonitoringSummaryBean();
		monitoringSummaryBean.setMonitoringSummaryList(MspOrganizer
				.getMonitoringSummary(request.getParameter("fromPage"),
						getDataSource(request, "ilexnewDB")));
		MspOrganizer.setFormFromMonitoringSummaryBean(monitoringSummaryForm,
				monitoringSummaryBean);
		return mapping.findForward("success"); // - /MSP/MspMonitorSummary.jsp
	}

	/**
	 * Monitoring summary. Called through ajax, on the basis of interval.
	 * 
	 * @param ActionMapping
	 *            the mapping
	 * @param ActionForm
	 *            the form
	 * @param HttpServletRequest
	 *            the request
	 * @param HttpServletResponse
	 *            the response
	 * 
	 * @return 1 or 2 through Ajax
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward monitoringSummary(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (logger.isDebugEnabled()) {
			logger
					.debug("monitoringSummary(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - Monitoring Summary::" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		PrintWriter out = response.getWriter();
		MspUtility.ajaxReturn(1, out);
		return null;
	}
}
