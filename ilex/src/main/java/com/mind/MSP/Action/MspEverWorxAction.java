package com.mind.MSP.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.MspEverWorxOrganizer;
import com.mind.MSP.common.MspUtility;
import com.mind.dao.IM.IMdao;

public class MspEverWorxAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IMdao.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.struts.actions.DispatchAction#unspecified(org.apache.struts
	 * .action.ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		return mapping.findForward("success");
	}

	public void getMarkers(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MspUtility mspUtility = new MspUtility();
		String markers = mspUtility.getJSONString(MspEverWorxOrganizer
				.getMarkers(request.getParameter("status"), getDataSource(
						request, "ilexnewDB")));
		if (logger.isDebugEnabled())
			logger.debug(markers);

		response.getWriter().print(markers);

	}
}
