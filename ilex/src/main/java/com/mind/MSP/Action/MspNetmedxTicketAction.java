package com.mind.MSP.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.MspNetmedxTicketBean;
import com.mind.MSP.business.MspNetmedxTicketOrganizer;
import com.mind.MSP.business.MspOrganizer;
import com.mind.MSP.formbean.MspNetmedxTicketForm;
import com.mind.common.Util;
import com.mind.common.dao.Authenticationdao;

/**
 * The Class MspNetmedxTicketAction.
 * 
 * @author Vijay Kumar Singh
 */
public class MspNetmedxTicketAction extends com.mind.common.IlexDispatchAction {

	private static final Logger logger = Logger
			.getLogger(MspNetmedxTicketAction.class);

	/**
	 * Purpose: Initial load.
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified");
		}
		HttpSession session = request.getSession(true);
		String loginuserId = (String) session.getAttribute("userid");
		/* Check for session expired. */
		if (loginuserId == null) {
			return (mapping.findForward("SessionExpire"));
		}
		/* - Check for Pager Security. */
		if (!Authenticationdao.getPageSecurity(loginuserId,
				"Monitoring Services", "Monitoring Services Page",
				getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate"));
		}
		MspNetmedxTicketForm ticketForm = (MspNetmedxTicketForm) form;
		DataSource ds = getDataSource(request, "ilexnewDB");
		MspNetmedxTicketBean ticketBean = new MspNetmedxTicketBean();
		MspNetmedxTicketOrganizer.setDefaultValue(ticketBean, ds);
		MspNetmedxTicketOrganizer.copyFormFromBean(ticketForm, ticketBean);

		/* Forward to-> /MSP/MspNetmedxTicket.jsp */
		return mapping.findForward("searchForSite");
	}

	/**
	 * Purpose: Site search.
	 * 
	 * @throws Exception
	 */
	public ActionForward siteSearch(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("siteSearch");
		}
		MspNetmedxTicketForm ticketForm = (MspNetmedxTicketForm) form;
		DataSource ds = getDataSource(request, "ilexnewDB");
		MspNetmedxTicketBean ticketBean = new MspNetmedxTicketBean();
		MspNetmedxTicketOrganizer.copyBeanFromForm(ticketBean, ticketForm);
		ticketBean.setSiteList(MspNetmedxTicketOrganizer.setSiteList(ds,
				ticketBean));
		MspNetmedxTicketOrganizer.setDefaultValue(ticketBean, ds);
		MspNetmedxTicketOrganizer.copyFormFromBean(ticketForm, ticketBean);
		request.setAttribute("search", "true");
		/* Forward to-> /MSP/MspNetmedxTicket.jsp */
		return mapping.findForward("searchForSite");
	}

	/**
	 * Purpose: Create ticket.
	 * 
	 * @throws Exception
	 */
	public ActionForward createTicket(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("createTicket");
		}
		HttpSession session = request.getSession(true);
		String loginUserId = (String) session.getAttribute("userid");
		MspNetmedxTicketForm ticketForm = (MspNetmedxTicketForm) form;
		DataSource ds = getDataSource(request, "ilexnewDB");
		MspNetmedxTicketBean ticketBean = new MspNetmedxTicketBean();
		MspNetmedxTicketOrganizer.copyBeanFromForm(ticketBean, ticketForm);
		String[] createdIds = MspNetmedxTicketOrganizer.createTicket(
				loginUserId, ticketBean, ds);
		if (!Util.isNullOrBlank(createdIds[1])) {
			/* Check ticket has created or not */
			String ticketNumber = MspOrganizer.getTicketNumber(createdIds[1],
					getDataSource(request, "ilexnewDB"));
			setMessage(session, " - Ticket# " + ticketNumber + " created.");
		} else {
			setMessage(session, " - Ticket Creation Error.");
		}
		return mapping.findForward("mspMonitoring");
	}

	/**
	 * Purpose: To set message into request.
	 */
	private void setMessage(HttpSession session, String message) {
		session.setAttribute("ticketCreateMessage", message);
	}
}
