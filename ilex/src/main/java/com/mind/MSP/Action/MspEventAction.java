package com.mind.MSP.Action;

import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.MspEventBean;
import com.mind.MSP.business.MspOrganizer;
import com.mind.MSP.common.MspUtility;
import com.mind.MSP.formbean.MspEventForm;
import com.mind.dao.IM.IMdao;
import com.mind.msp.common.EscalationLevel;

/**
 * The Class MspEventAction.
 */
public class MspEventAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IMdao.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */

	/**
	 * Msp Event. Called on the first time on page load.
	 * 
	 * @param ActionMapping
	 *            the mapping
	 * @param ActionForm
	 *            the form
	 * @param HttpServletRequest
	 *            the request
	 * @param HttpServletResponse
	 *            the response
	 * 
	 * @return the action forward
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MspEventForm mspEventForm = (MspEventForm) form;
		MspEventBean mspEventBean = new MspEventBean();
		MspOrganizer.setMspDispatch(mspEventBean, getDataSource(request,
				"ilexnewDB"));
		MspOrganizer.setFormFromMspDispatchBean(mspEventForm, mspEventBean);
		return mapping.findForward("success"); // - /MSP/MspEven.jsp
	}

	/**
	 * Event. Called through ajax, on the basis of interval.
	 * 
	 * @param ActionMapping
	 *            the mapping
	 * @param ActionForm
	 *            the form
	 * @param HttpServletRequest
	 *            the request
	 * @param HttpServletResponse
	 *            the response
	 * 
	 * @return null, ajax return.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward event(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		int pageFlag = MspOrganizer.getEscalationLevel(
				EscalationLevel.MSP_DISPATCH, getDataSource(request,
						"ilexnewDB"));
		if (logger.isDebugEnabled()) {
			logger
					.debug("event(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - - MSP Dispatch:: Flag="
							+ pageFlag
							+ "::::: "
							+ new Date().getHours()
							+ ":"
							+ new Date().getMinutes()
							+ ":"
							+ new Date().getSeconds());
		}
		PrintWriter out = response.getWriter();
		MspUtility.ajaxReturn(pageFlag, out);
		return null;
	}
}
