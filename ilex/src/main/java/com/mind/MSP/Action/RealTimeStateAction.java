package com.mind.MSP.Action;

import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.MSP.business.MspOrganizer;
import com.mind.MSP.business.RealTimeStateBean;
import com.mind.MSP.common.MspUtility;
import com.mind.MSP.formbean.RealTimeStateForm;
import com.mind.bean.msp.RealTimeState;
import com.mind.bean.msp.RealTimeStateInfo;
import com.mind.common.Util;
import com.mind.dao.IM.IMdao;

/**
 * The Class RealTimeStateAction.
 */
public class RealTimeStateAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IMdao.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */

	/**
	 * Real Time. Called on the first time on page load.
	 * 
	 * @param ActionMapping
	 *            the mapping
	 * @param ActionForm
	 *            the form
	 * @param HttpServletRequest
	 *            the request
	 * @param HttpServletResponse
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - RealTimeStateAction	" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		RealTimeStateForm realTimeStateForm = (RealTimeStateForm) form;

		/* Call Business Logic */
		RealTimeStateBean RealTimeState = new RealTimeStateBean();
		MspOrganizer.getRealTimeStatic(RealTimeState,
				getDataSource(request, "ilexnewDB"));
		MspOrganizer.setFormFromRealTimeStateBean(realTimeStateForm,
				RealTimeState);
		return mapping.findForward("success"); // - /MSP/MspMonitor.jsp
	}

	// Changes Done By Yogendra
	/**
	 * unManaged Initial Assessment. Called on the first time on page load.
	 * 
	 * @param ActionMapping
	 *            the mapping
	 * @param ActionForm
	 *            the form
	 * @param HttpServletRequest
	 *            the request
	 * @param HttpServletResponse
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward unManagedInitialAssessment(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("unManagedInitialAssessment(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - RealTimeStateAction	" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		RealTimeStateForm realTimeStateForm = (RealTimeStateForm) form;

		/* Call Business Logic */
		RealTimeStateBean RealTimeState = new RealTimeStateBean();
		MspOrganizer.unManagedInitialAssessment(RealTimeState,
				getDataSource(request, "ilexnewDB"));
		MspOrganizer.setFormFromUnManagedInitAssessment(realTimeStateForm,
				RealTimeState);
		return mapping.findForward("unmanaged"); // -MSP/MspUnmgdInitialAssessment.jsp
	}

	public ActionForward noActionAllForManaged(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (logger.isDebugEnabled()) {
			logger.debug("noActionAllForManaged(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - RealTimeStateAction	" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		/* Call Business Logic */

		int noActionFlag = -1;

		List<RealTimeState> realTimeStateListUpStatus = MspOrganizer
				.managedInitialAssessmentOnlyUpStatus(getDataSource(request,
						"ilexnewDB"));
		for (RealTimeState realTimeState : realTimeStateListUpStatus) {
			noActionFlag = MspOrganizer.setNoAction(
					realTimeState.getNActiveMonitorStateChangeID(),
					realTimeState.getWugInstance(),
					getDataSource(request, "ilexnewDB"), loginuserid);
		}

		PrintWriter out = response.getWriter();
		MspUtility.ajaxReturn(noActionFlag, out);
		return null;
	}

	public ActionForward noActionAllForUnManaged(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (logger.isDebugEnabled()) {
			logger.debug("noActionAllForUnManaged(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - RealTimeStateAction	" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		/* Call Business Logic */

		int noActionFlag = -1;

		List<RealTimeState> realTimeStateListUpStatus = MspOrganizer
				.unManagedInitialAssessmentOnlyUpStatus(getDataSource(request,
						"ilexnewDB"));
		for (RealTimeState realTimeState : realTimeStateListUpStatus) {
			noActionFlag = MspOrganizer.setNoAction(
					realTimeState.getNActiveMonitorStateChangeID(),
					realTimeState.getWugInstance(),
					getDataSource(request, "ilexnewDB"), loginuserid);
		}

		PrintWriter out = response.getWriter();
		MspUtility.ajaxReturn(noActionFlag, out);
		return null;
	}

	/**
	 * Forwards when Initial Assessment -Unmanaged Site link is clicked
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward unManagedSitesLink(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("unManagedSitesLink(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - RealTimeStateAction	" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		return mapping.findForward("unmanagedlink");// MSP/MSPUnmanagedSites.jsp
	}

	/**
	 * unManaged Escalation Level . Called through ajax, on the basis of
	 * interval.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward unManagedEscalationLevel(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession(true);
		removeMessage(request, session, "escalationLevel");
		RealTimeStateInfo realTimeStateInfo = MspOrganizer
				.getRealTimeStateInfo(getDataSource(request, "ilexnewDB"));
		if (logger.isDebugEnabled()) {
			logger.debug("unManagedEscalationLevel(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - In escalationLevel:: Flag=" + realTimeStateInfo.getPageFlag() + ":::::::::" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()); //$NON-NLS-1$
		}

		// PrintWriter out = response.getWriter();
		int flag = 0;
		String prePageFlag = request.getParameter("pageFlag");
		if (!Util.isNullOrBlank(prePageFlag)
				&& Integer.parseInt(prePageFlag) != realTimeStateInfo
						.getPageFlag()) {
			flag = 1;
		}
		// MspUtility.ajaxReturn(flag, out);

		return mapping.findForward("unmanaged");
	}

	// /End Changes

	/**
	 * Escalation level. Called through ajax, on the basis of interval.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward escalationLevel(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession(true);
		removeMessage(request, session, "escalationLevel");
		RealTimeStateInfo realTimeStateInfo = MspOrganizer
				.getRealTimeStateInfo(getDataSource(request, "ilexnewDB"));
		if (logger.isDebugEnabled()) {
			logger.debug("escalationLevel(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - In escalationLevel:: Flag=" + realTimeStateInfo.getPageFlag() + ":::::::::" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()); //$NON-NLS-1$
		}

		PrintWriter out = response.getWriter();
		int flag = 0;
		String prePageFlag = request.getParameter("pageFlag");
		if (!Util.isNullOrBlank(prePageFlag)
				&& Integer.parseInt(prePageFlag) != realTimeStateInfo
						.getPageFlag()) {
			flag = 1;
		}
		MspUtility.ajaxReturn(flag, out);
		return null;
	}

	/**
	 * Real time footer. Update Last Update, Event Count and Site Errors of
	 * Real-Time State Changes - Initial Assessment. Called through ajax, on the
	 * basis of interval.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward realTimeFooter(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("realTimeFooter(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - In footer:::::"
					+ new Date().getHours()
					+ ":"
					+ new Date().getMinutes()
					+ ":" + new Date().getSeconds());
		}
		request.setAttribute("realTimeStateInfo", MspOrganizer
				.getRealTimeStateInfo(getDataSource(request, "ilexnewDB")));
		return mapping.findForward("footer"); // - /MSP/MspMonitor.jsp
	}

	/**
	 * To remove message from session.
	 * 
	 * @param request
	 * @param session
	 */
	private void removeMessage(HttpServletRequest request, HttpSession session,
			String removeFrom) {
		if (request.getParameter("hmode") != null
				&& request.getParameter("hmode").equals(removeFrom)) {
			session.removeAttribute("mspMessage");
		}
	}

	/**
	 * No action. To remove inprocess from main and history table. Called
	 * through ajax, on no action.
	 * 
	 * @param ActionMapping
	 *            the mapping
	 * @param ActionForm
	 *            the form
	 * @param HttpServletRequest
	 *            the request
	 * @param HttpServletResponse
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward noAction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		logger.error("Added for debuging purpose*****Login Id:" + loginuserid
				+ " ******** nActiveMonitorStateChangeID:"
				+ request.getParameter("nActiveMonitorStateChangeID"));
		if (logger.isDebugEnabled()) {
			logger.debug("noAction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - nActiveMonitorStateChangeID=" + request.getParameter("nActiveMonitorStateChangeID")); //$NON-NLS-1$ //$NON-NLS-2$
		}
		int noActionFlag = -1;
		if (!Util.isNullOrBlank(request
				.getParameter("nActiveMonitorStateChangeID"))) {
			noActionFlag = MspOrganizer.setNoAction(
					request.getParameter("nActiveMonitorStateChangeID"),
					request.getParameter("wugInstance"),
					getDataSource(request, "ilexnewDB"), loginuserid);
		}
		PrintWriter out = response.getWriter();
		MspUtility.ajaxReturn(noActionFlag, out);
		return null;
	}

	/**
	 * unmanaged No Action. To remove inprocess from main and history table.
	 * Called through ajax, on no action.
	 * 
	 * @param ActionMapping
	 *            the mapping
	 * @param ActionForm
	 *            the form
	 * @param HttpServletRequest
	 *            the request
	 * @param HttpServletResponse
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	// public ActionForward unmanagedNoAction(ActionMapping mapping,
	// ActionForm form, HttpServletRequest request,
	// HttpServletResponse response) throws Exception {
	// HttpSession session = request.getSession(true);
	// String loginuserid = (String) session.getAttribute("userid");
	// logger.error("Added for debuging purpose*****Login Id:" + loginuserid
	// + " ******** nActiveMonitorStateChangeID:"
	// + request.getParameter("nActiveMonitorStateChangeID"));
	// if (logger.isDebugEnabled()) {
	//			logger.debug("unmanagedNoAction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - nActiveMonitorStateChangeID=" + request.getParameter("nActiveMonitorStateChangeID")); //$NON-NLS-1$ //$NON-NLS-2$
	// }
	// int noActionFlag = -1;
	// if (!Util.isNullOrBlank(request
	// .getParameter("nActiveMonitorStateChangeID"))) {
	// noActionFlag = MspOrganizer.setNoAction(
	// request.getParameter("nActiveMonitorStateChangeID"),
	// request.getParameter("wugInstance"),
	// getDataSource(request, "ilexnewDB"));
	// }
	// PrintWriter out = response.getWriter();
	// MspUtility.ajaxReturn(noActionFlag, out);
	// return null;
	// }

	/**
	 * Escalate. To create a ticket.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws Exception
	 *             the exception
	 */
	public ActionForward escalate(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession(true);
		String loginUser = (String) session.getAttribute("userid");
		int createFlag = -1;
		String[] createdIds = new String[3];
		String ticketNumber = "";
		String nActiveMonitorStateChangeID = request
				.getParameter("nActiveMonitorStateChangeID");
		String wugInstance = request.getParameter("wugInstance");
		if (!Util.isNullOrBlank(nActiveMonitorStateChangeID)) {
			createdIds = MspOrganizer.createTicket(loginUser,
					nActiveMonitorStateChangeID, wugInstance,
					getDataSource(request, "ilexnewDB"));
		}
		if (!Util.isNullOrBlank(createdIds[1])) {
			/* Check ticket has created or not */
			createFlag = 1;
			ticketNumber = MspOrganizer.getTicketNumber(createdIds[1],
					getDataSource(request, "ilexnewDB"));
			MspOrganizer.setToHelpDesk(nActiveMonitorStateChangeID,
					wugInstance, ticketNumber,
					getDataSource(request, "ilexnewDB"));
			setMessage(session, createFlag, " - Ticket# " + ticketNumber
					+ " created.");
		} else {
			setMessage(session, 1, " - Ticket Creation Error.");
		}
		PrintWriter out = response.getWriter();
		MspUtility.ajaxReturn(createFlag, out);
		return null;
	}

	public ActionForward unmanagedEscalate(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession(true);
		String loginUser = (String) session.getAttribute("userid");
		int createFlag = -1;
		String[] createdIds = new String[3];
		String ticketNumber = "";
		String nActiveMonitorStateChangeID = request
				.getParameter("nActiveMonitorStateChangeID");
		String wugInstance = request.getParameter("wugInstance");
		if (!Util.isNullOrBlank(nActiveMonitorStateChangeID)) {
			createdIds = MspOrganizer.createTicket(loginUser,
					nActiveMonitorStateChangeID, wugInstance,
					getDataSource(request, "ilexnewDB"));
		}
		if (!Util.isNullOrBlank(createdIds[1])) {
			/* Check ticket has created or not */
			createFlag = 1;
			ticketNumber = MspOrganizer.getTicketNumber(createdIds[1],
					getDataSource(request, "ilexnewDB"));
			MspOrganizer.setToHelpDesk(nActiveMonitorStateChangeID,
					wugInstance, ticketNumber,
					getDataSource(request, "ilexnewDB"));
			setMessage(session, createFlag, " - Ticket# " + ticketNumber
					+ " created.");
		} else {
			setMessage(session, 1, " - Ticket Creation Error.");
		}
		PrintWriter out = response.getWriter();
		MspUtility.ajaxReturn(createFlag, out);
		return mapping.findForward("unmanaged");
	}

	/**
	 * To set message into session.
	 * 
	 * @param request
	 * @param createFlag
	 */
	private void setMessage(HttpSession session, int createFlag, String message) {
		if (createFlag == 1) {
			session.setAttribute("mspMessage", message);
		} else {
			session.setAttribute("mspMessage", "");
		}
	}
}
