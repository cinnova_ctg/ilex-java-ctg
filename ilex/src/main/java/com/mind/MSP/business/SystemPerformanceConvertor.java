package com.mind.MSP.business;

import com.mind.MSP.formbean.SystemPerformanceFrom;
import com.mind.bean.msp.SystemPerformanceBean;

/**
 * The Class SytstemPerformanceConvertor.
 */
public class SystemPerformanceConvertor {

	/**
	 * Sets the form from search history bean.
	 * 
	 * @param MspSearchHistoryForm
	 *            the form
	 * @param SearchHistoryBean
	 *            the bean
	 */
	public static void setFormFromSystemPerformanceBean(
			SystemPerformanceFrom form, SystemPerformanceBean bean) {
		form.setCustomerList(bean.getCustomerList());
		form.setSlaGraphXML(bean.getSlaGraphXML());
		form.setBouncesGraphXML(bean.getBouncesGraphXML());
		form.setCountsGraphXML(bean.getCountsGraphXML());
		form.setSlaTable(bean.getSlaTable());
		form.setCountTable(bean.getCountTable());
		form.setCostTable(bean.getCostTable());
	}

	/**
	 * Sets the search history bean from form.
	 * 
	 * @param MspSearchHistoryForm
	 *            the form
	 * @param SearchHistoryBean
	 *            the bean
	 */
	public static void setSystemPerformanceBeanFromForm(
			SystemPerformanceFrom form, SystemPerformanceBean bean) {
		bean.setCustomer(form.getCustomer());
	}
}
