package com.mind.MSP.business;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;

import com.mind.MSP.dao.PCIChangeControlDAO;
import com.mind.MSP.formbean.PCIChangeControlForm;
import com.mind.bean.msp.ChangeRequestActionDetails;
import com.mind.bean.msp.PCIChangeControlBean;
import com.mind.common.LabelValue;
import com.mind.fw.lang.SysException;

/**
 * The Class PCIChangeControlOrganiser.
 * 
 * @author LALIT GOYAL
 */
public class PCIChangeControlOrganiser {

	/**
	 * Sets the change request data.
	 * 
	 * @param bean
	 *            the bean
	 * @param ds
	 *            the ds
	 * @param request
	 *            the request
	 * @throws SysException
	 *             the sys exception
	 */
	public static void setChangeRequestData(PCIChangeControlBean bean,
			DataSource ds, HttpServletRequest request) throws SysException {
		HttpSession session = request.getSession(false);
		bean.setCustomerList(PCIChangeControlDAO.getCustomerList(ds));
		bean.setSelectedChangeRequestStatusList(PCIChangeControlDAO
				.getChangeRequestStatusList(ds));
		bean.setFinalDispositionStatusList(PCIChangeControlDAO
				.getFinalDispositionStatusList(ds));
		if (null == (bean.getChangeRequestActionsList())
				|| bean.getChangeRequestActionsList().size() == 0) {
			ChangeRequestActionDetails actionDetail = new ChangeRequestActionDetails();
			actionDetail.setActionID("notSaved");
			List<ChangeRequestActionDetails> actionDetailList = new ArrayList<ChangeRequestActionDetails>();
			actionDetailList.add(actionDetail);
			bean.setChangeRequestActionsList(actionDetailList);
		}
		bean.setChangeRequestActionStatusList(PCIChangeControlDAO
				.getChangeReqActionsStatusList(ds));

		bean.setRequestorName((String) session.getAttribute("username"));
	}

	/**
	 * Sets the form from bean.
	 * 
	 * @param bean
	 *            the bean
	 * @param form
	 *            the form
	 * @throws SysException
	 *             the sys exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 */
	public static void setFormFromBean(PCIChangeControlBean bean,
			PCIChangeControlForm form) throws SysException,
			IllegalAccessException, InvocationTargetException {
		BeanUtils.copyProperties(form, bean);
	}

	/**
	 * Sets the form data from bean.
	 * 
	 * @param pciControlBean
	 *            the pci control bean
	 * @param pciChangeControlForm
	 *            the pci change control form
	 */
	public static void setFormDataFromBean(PCIChangeControlBean pciControlBean,
			PCIChangeControlForm pciChangeControlForm) {
		pciChangeControlForm.setChangeRequestActionStatusList(pciControlBean
				.getChangeRequestActionStatusList());
		pciChangeControlForm.setCustomerList(pciControlBean.getCustomerList());
		pciChangeControlForm.setSiteIdentifierList(pciControlBean
				.getSiteIdentifierList());
		pciChangeControlForm.setSelectedChangeRequestStatusList(pciControlBean
				.getSelectedChangeRequestStatusList());
		pciChangeControlForm.setFinalDispositionStatusList(pciControlBean
				.getFinalDispositionStatusList());
		pciChangeControlForm.setChangeRequestActionsList(pciControlBean
				.getChangeRequestActionsList());
	}

	/**
	 * Sets the change request site identifier.
	 * 
	 * @param bean
	 *            the bean
	 * @param custID
	 *            the cust id
	 * @param ds
	 *            the ds
	 * @throws SysException
	 *             the sys exception
	 */
	public static void setChangeRequestSiteIdentifier(
			PCIChangeControlBean bean, String custID, DataSource ds)
			throws SysException {
		bean.setSiteIdentifierList(siteIdentifierList(custID, ds));

	}

	/**
	 * Save change request data.
	 * 
	 * @param bean
	 *            the bean
	 * @param ds
	 *            the ds
	 * @return the integer
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws SysException
	 *             the sys exception
	 */
	public static Integer saveChangeRequestData(PCIChangeControlBean bean,
			DataSource ds) throws NumberFormatException, SysException {

		Integer result = PCIChangeControlDAO.saveChangeRequestData(ds, bean);

		return result;
	}

	/**
	 * Copy bean from form.
	 * 
	 * @param bean
	 *            the bean
	 * @param form
	 *            the form
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 */
	public static void copyBeanFromForm(PCIChangeControlBean bean,
			PCIChangeControlForm form) throws IllegalAccessException,
			InvocationTargetException {
		BeanUtils.copyProperties(bean, form);
	}

	/**
	 * Gets the change request data.
	 * 
	 * @param changeRequestID
	 *            the change request id
	 * @param ds
	 *            the ds
	 * @param bean
	 *            the bean
	 * @param request
	 *            the request
	 * @return the change request data
	 * @throws SysException
	 *             the sys exception
	 */
	public static PCIChangeControlBean getChangeRequestData(
			String changeRequestID, DataSource ds, PCIChangeControlBean bean,
			HttpServletRequest request) throws SysException {
		bean = PCIChangeControlDAO.getChangeRequestData(changeRequestID, ds);
		bean.setChangeRequestActionsList(PCIChangeControlDAO
				.getChangeRequestActionData(changeRequestID, ds));
		bean.setFinalDispositionStatusList(PCIChangeControlDAO
				.getFinalDispositionStatusList(ds));
		bean.setChangeRequestID(changeRequestID);
		bean.setSiteIdentifierList(siteIdentifierList(bean.getOrgTopId(), ds));
		setChangeRequestData(bean, ds, request);
		return bean;
	}

	/**
	 * Save action data.
	 * 
	 * @param ds
	 *            the ds
	 * @param actiondetail
	 *            the actiondetail
	 * @throws SysException
	 *             the sys exception
	 */
	public static void saveActionData(DataSource ds,
			ChangeRequestActionDetails actiondetail) throws SysException {

		PCIChangeControlDAO.saveUpdateChangeRequestActionData(ds, actiondetail);

	}

	/**
	 * Update action data.
	 * 
	 * @param ds
	 *            the ds
	 * @param actiondetail
	 *            the actiondetail
	 * @throws SysException
	 *             the sys exception
	 */
	public static void updateActionData(DataSource ds,
			ChangeRequestActionDetails actiondetail) throws SysException {

		PCIChangeControlDAO.saveUpdateChangeRequestActionData(ds, actiondetail);

	}

	/**
	 * Delete action data.
	 * 
	 * @param ds
	 *            the ds
	 * @param actiondetail
	 *            the actiondetail
	 * @return the integer
	 * @throws SysException
	 *             the sys exception
	 */
	public static Integer deleteActionData(DataSource ds,
			ChangeRequestActionDetails actiondetail) throws SysException {

		return PCIChangeControlDAO.saveUpdateChangeRequestActionData(ds,
				actiondetail);

	}

	/**
	 * Save final disposition.
	 * 
	 * @param bean
	 *            the bean
	 * @param ds
	 *            the ds
	 * @param request
	 *            the request
	 * @return the integer
	 * @throws SysException
	 *             the sys exception
	 */
	public static Integer saveFinalDisposition(PCIChangeControlBean bean,
			DataSource ds, HttpServletRequest request) throws SysException {

		Integer result = PCIChangeControlDAO.saveFinalDisposition(ds, bean);
		// getChangeRequestData()
		return result;

	}

	/**
	 * Getting Site identifier list.
	 * 
	 * @param customerID
	 *            the customer id
	 * @param ds
	 *            the ds
	 * @return the list
	 * @throws SysException
	 *             the sys exception
	 */
	public static List<LabelValue> siteIdentifierList(String customerID,
			DataSource ds) throws SysException {
		List<LabelValue> siteIdentifierList = new ArrayList<LabelValue>();
		siteIdentifierList = PCIChangeControlDAO
				.getStaticSiteIdentifierList(ds);
		siteIdentifierList = PCIChangeControlDAO.getSiteIdentifierList(
				customerID, ds, siteIdentifierList);

		return siteIdentifierList;

	}
}
