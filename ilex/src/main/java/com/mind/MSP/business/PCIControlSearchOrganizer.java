package com.mind.MSP.business;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;

import com.mind.MSP.dao.PCIChangeControlDAO;
import com.mind.MSP.dao.PCIControlSearchDao;
import com.mind.MSP.formbean.PCIControlSearchForm;
import com.mind.bean.msp.PCIControlSearchBean;
import com.mind.fw.lang.SysException;

/**
 * The Class PCIControlSearchOrganizer.
 * 
 * @return PCIControlSearchForm
 * 
 *         This method is used for setting the all request parameter to the
 *         PCIControlSearchForm
 * @author yogendrasingh
 */
public class PCIControlSearchOrganizer {

	/**
	 * Sets the change request data.
	 * 
	 * @param bean
	 *            the bean
	 * @param ds
	 *            the ds
	 * @param request
	 *            the request
	 * @throws SysException
	 *             the sys exception
	 */
	public static void setChangeRequestData(PCIControlSearchBean bean,
			DataSource ds) throws SysException {
		bean.setCustomerList(PCIChangeControlDAO.getCustomerList(ds));
		bean.setSelectedChangeRequestStatusList(PCIChangeControlDAO
				.getChangeRequestStatusList(ds));
	}

	/**
	 * Sets the pci control search form values.
	 * 
	 * @param request
	 *            the request
	 * @param pciControlSearchForm
	 *            the pci control search form
	 * @return the pCI control search form
	 */
	public static PCIControlSearchForm setPCIControlSearchFormValues(
			Map<String, String> paramMap,
			PCIControlSearchForm pciControlSearchForm) {
		pciControlSearchForm.setOrgTopId(paramMap.get("customer").toString());
		pciControlSearchForm.setSelectedChangeRequestStatus(paramMap.get(
				"selectedChangeRequestStatus").toString());
		pciControlSearchForm.setSelectedSiteID(paramMap.get("selectedSiteID")
				.toString());
		pciControlSearchForm.setFromDate(paramMap.get("fromDate").toString());
		pciControlSearchForm.setToDate(paramMap.get("toDate").toString());
		return pciControlSearchForm;
	}

	/**
	 * Sets the form from site search bean.
	 * 
	 * @param form
	 *            the form
	 * @param bean
	 *            the bean
	 * 
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 */
	public static void setBeanFromPCIControlSearchForm(
			PCIControlSearchForm form, PCIControlSearchBean bean)
			throws IllegalAccessException, InvocationTargetException {
		BeanUtils.copyProperties(bean, form);

	}

	/**
	 * Sets the form from bean.
	 * 
	 * @param form
	 *            the form
	 * @param bean
	 *            the bean
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 */
	public static void setFormFromBean(PCIControlSearchForm form,
			PCIControlSearchBean bean) throws IllegalAccessException,
			InvocationTargetException {
		BeanUtils.copyProperties(form, bean);

	}

	/**
	 * Sets the pci search control data list.
	 * 
	 * @param bean
	 *            the bean
	 * @param dataSource
	 *            the data source
	 * @throws SysException
	 *             the sys exception
	 */
	public static void setPCISearchControlDataList(PCIControlSearchBean bean,
			DataSource dataSource) throws SysException {

		bean.setPciChangeControlFormList(PCIControlSearchDao
				.getPCISearchControlDataList(bean, dataSource));
	}
}
