package com.mind.MSP.business;

import com.mind.MSP.formbean.ChronicSitesForm;
import com.mind.bean.msp.ChronicSitesBean;

public class ChronicSiteConverter {
	
	/**
	 * Method name	: mapCustomerListToForm.
	 * Description	: set customer list from bean to form. This is called only when page is initial 
	 * 				  loaded without search. 
	 * @param form	: reference to ChronicSitesForm.	
	 * @param bean 	: reference of ChronicSitesBean.
	 */
	public static void mapCustomerListToForm(ChronicSitesForm form, ChronicSitesBean bean) {
		form.setCustomerList(bean.getCustomerList());
	}

	/**
	 * Method name	: mapFormToBean.
	 * Description	: map form elements to bean elements.
	 * @param form	: reference to ChronicSitesForm.
	 * @param bean 	: reference of ChronicSitesBean.
	 */
	public static void mapFormToBean(ChronicSitesForm form, ChronicSitesBean bean) {
		bean.setCustomerName(form.getCustomerName());
	}
	
	/**
	 * Method name	: mapBeanToForm.
	 * Description	: map bean elements to form elements.
	 * @param form	: reference to ChronicSitesForm.
	 * @param bean 	: reference of ChronicSitesBean.
	 */
	public static void mapBeanToForm(ChronicSitesForm form, ChronicSitesBean bean) {
		form.setCustomerList(bean.getCustomerList());
		form.setChronicSiteList(bean.getChronicSiteList());
	}
}
