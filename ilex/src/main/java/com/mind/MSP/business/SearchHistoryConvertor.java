package com.mind.MSP.business;

import com.mind.MSP.formbean.MspSearchHistoryForm;
import com.mind.bean.msp.SearchHistoryBean;

/**
 * The Class SearchHistoryConvertor.
 */
public class SearchHistoryConvertor {

	/**
	 * Sets the form from search history bean.
	 * 
	 * @param MspSearchHistoryForm
	 *            the form
	 * @param SearchHistoryBean
	 *            the bean
	 */
	public static void setFormFromSearchHistoryBean(MspSearchHistoryForm form,
			SearchHistoryBean bean) {
		form.setCustomerList(bean.getCustomerList());
		form.setSearchList(bean.getSearchList());
	}

	/**
	 * Sets the search history bean from form.
	 * 
	 * @param MspSearchHistoryForm
	 *            the form
	 * @param SearchHistoryBean
	 *            the bean
	 */
	public static void setSearchHistoryBeanFromForm(MspSearchHistoryForm form,
			SearchHistoryBean bean) {
		bean.setCustomer(form.getCustomer());
		bean.setFrom(form.getFrom());
		bean.setTo(form.getTo());
		bean.setSite(form.getSite());
	}
}
