package com.mind.MSP.business;

import java.lang.reflect.InvocationTargetException;

import javax.sql.DataSource;

import com.mind.MSP.dao.CustomerCreditDAO;
import com.mind.MSP.dao.MspUtilityDao;
import com.mind.MSP.formbean.MspReportForm;
import com.mind.bean.msp.MspReportBean;
import com.mind.msp.common.EscalationLevel;

public class CustomerCreditDelegate {
	/**
	 * Method name : fillCustomerListforBean. Description : sets customer ids
	 * and names as LabelValue pairs in bean object.
	 * 
	 * @param bean
	 *            : reference of MspReportBean.
	 * @param ds
	 *            : reference of DataSource.
	 */
	public static void fillCustomerListforBean(MspReportBean bean, DataSource ds) {
		bean.setCustomerList(MspUtilityDao.getOptionList(
				EscalationLevel.ILEX_CUSTOMER_NAME_FOR_CREDITS_RPT, ds));
	}

	/**
	 * Method name : getCreditsSearch. Description : populates bean object with
	 * customer list and search result list.
	 * 
	 * @param bean
	 *            : reference of MspReportBean.
	 * @param ds
	 *            : reference of DataSource.
	 */
	public static void getCreditsSearch(MspReportBean bean, DataSource ds) {
		bean.setMspSearch(CustomerCreditDAO.getCustomerCreditSearch(bean, ds));
		fillCustomerListforBean(bean, ds);
	}

	/**
	 * Method name : convertFormToBean. Description : calls a method in
	 * converter class to map form elements to bean elements.
	 * 
	 * @param form
	 *            : reference to MspReportForm
	 * @param bean
	 *            : reference of MspReportBean.
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public static void convertFormToBean(MspReportForm form, MspReportBean bean)
			throws IllegalAccessException, InvocationTargetException {
		CustomerCreditConvertor.mapFormToBean(form, bean);
	}

	/**
	 * Method name : convertBeanToForm. Description : calls a method in
	 * converter class to map bean elements to form elements.
	 * 
	 * @param form
	 *            : reference to MspReportForm
	 * @param bean
	 *            : reference of MspReportBean.
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public static void convertBeanToForm(MspReportForm form, MspReportBean bean)
			throws IllegalAccessException, InvocationTargetException {
		CustomerCreditConvertor.mapBeanToForm(form, bean);
	}

	/**
	 * Method name : setCustomerListToForm. Description : calls a method in
	 * converter class to set customer list from bean to form. This is called
	 * only when page is initial loaded without search.
	 * 
	 * @param form
	 *            : reference to MspReportForm
	 * @param bean
	 *            : reference of MspReportBean.
	 */
	public static void setCustomerListToForm(MspReportForm form,
			MspReportBean bean) {
		CustomerCreditConvertor.mapCustomerListToForm(form, bean);
	}

}
