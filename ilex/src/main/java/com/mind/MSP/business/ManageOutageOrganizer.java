package com.mind.MSP.business;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;

import com.mind.MSP.dao.ManageOutageDao;
import com.mind.MSP.dao.MspUtilityDao;
import com.mind.MSP.formbean.ManageOutageForm;
import com.mind.bean.msp.DeviceInfoBean;
import com.mind.bean.msp.SiteInfo;
import com.mind.bean.msp.TicketInformation;
import com.mind.common.EnvironmentSelector;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Email;
import com.mind.msp.common.EscalationLevel;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.dao.TicketNatureDAO;
import com.mind.util.VelocityUtils;

/**
 * The Class ManageOutageOrganizer.
 */
public class ManageOutageOrganizer {

    static ResourceBundle rb = ResourceBundle
            .getBundle("com.mind.properties.ApplicationResources");
    static String username = rb.getString("common.Email.username");
    static String userPassword = rb.getString("common.Email.userpassword");

    static String smtpServerName = EnvironmentSelector
            .getBundleString("common.Email.smtpservername");

    static String smtpServerPort = rb.getString("common.Email.smtpserverport");

    static String subject = rb.getString("email.ticket.subject");

    static String emailTo = EnvironmentSelector
            .getBundleString("email.ticket.emergency.to");

    /**
     * Sets the manage outage bean.
     *
     * @param ManageOutageBean the manage outage bean
     * @param int the escalation level
     * @param int the n active monitor state change id
     * @param DataSource the ds
     */
    public static void setManageOutage(ManageOutageBean manageOutageBean,
            int escalationLevel, int nActiveMonitorStateChangeID,
            int wugInstance, DataSource ds) {
        manageOutageBean.setDeviceInfoBean(ManageOutageDao
                .getDeviceInformation(nActiveMonitorStateChangeID,
                        escalationLevel, wugInstance, ds));
        manageOutageBean.setSiteInfo(JobDAO.getSiteInfo(manageOutageBean
                .getDeviceInfoBean().getJobId(), ds));
        manageOutageBean.setTicketInformation(ManageOutageDao
                .getTicketInformation(nActiveMonitorStateChangeID, wugInstance,
                        ds));
        manageOutageBean.setProblemCategoryList(TicketNatureDAO
                .getProblemCategories(ds));
        manageOutageBean.setIssueOwnerList(MspUtilityDao.getOptionList(
                EscalationLevel.ISSUE_LIST_QUERY, ds));
        manageOutageBean.setRootCauseList(MspUtilityDao.getOptionList(
                EscalationLevel.ROOT_CAUSE_QUERY, ds));
        manageOutageBean.setResolutionList(MspUtilityDao.getOptionList(
                EscalationLevel.RESOLUTION_QUERY, ds));
        manageOutageBean.setNextActionList(TicketNatureDAO
                .getNextActionList(ds));
    }

    /**
     * Sets the form from manage outage.
     *
     * @param ManageOutageForm the form
     * @param ManageOutageBean the bean
     */
    public static void setFormFromManageOutage(ManageOutageForm form,
            ManageOutageBean bean) {
        ManageOutageConverter.setFormFromManageOutageBean(form, bean);
        form.setEscalationLabel(getLevelLabel(Integer.parseInt(form
                .getEscalationLevel())));
    }

    /**
     * Sets the manage outage bean from ActionForm.
     *
     * @param ManageOutageForm the form
     * @param ManageOutageBean the bean
     */
    public static void setManageOutageBeanFromForm(ManageOutageForm form,
            ManageOutageBean bean) {
        ManageOutageConverter.setManageOutageBeanFromForm(form, bean);
    }

    /**
     * Sets the resolved help desk. First set Resolved By Help Desk. On success
     * of this set Ticket to closed.
     *
     * @param String the n active monitor state change id
     * @param TicketInformation the ticket information
     * @param DataSource the ds
     * @return int the returnFlag
     */
    public static int setResolvedHelpDesk(String nActiveMonitorStateChangeID,
            String wugInstance, TicketInformation ticketInformation,
            String loginUserId, DataSource ds) {
        int returnFlag = -1;
        returnFlag = ManageOutageDao
                .setResolvedByHelpDesk(nActiveMonitorStateChangeID,
                        ticketInformation, wugInstance, ds);
        if (returnFlag > 0) {
            ManageOutageDao.setTicketToClosed(loginUserId, ticketInformation,
                    EscalationLevel.INVOICE_NUMBER, ds);
        }
        return returnFlag;
    }

    public static int setEscalateMSPDispatch(
            String nActiveMonitorStateChangeID,
            TicketInformation ticketInformation, String wugInstance,
            String loginUserId, DataSource ds) {
        int returnFlag = -1;
        int esclateLevelFlag = -1;
        int finalSubmitFlag = -1;
        esclateLevelFlag = ManageOutageDao.setHelpDeskEscalate(
                nActiveMonitorStateChangeID, wugInstance,
                ticketInformation.getEstimatedEffort(), ds);

        if (esclateLevelFlag > 0) {
            returnFlag = setUpdateInfo(nActiveMonitorStateChangeID,
                    ticketInformation, wugInstance, ds);
        }

        if (returnFlag > 0) {
            finalSubmitFlag = ManageOutageDao.setEscalateToMSPDispatch(
                    loginUserId, ticketInformation, ds);
        }
        return finalSubmitFlag;
    }

    public static int setUpdateInfo(String nActiveMonitorStateChangeID,
            TicketInformation ticketInformation, String wugInstance,
            DataSource ds) {
        int returnFlag = -1;
        returnFlag = ManageOutageDao
                .setResolutionActionDetails(nActiveMonitorStateChangeID,
                        ticketInformation, wugInstance, ds);
        return returnFlag;
    }

    public static int saveEfforts(TicketInformation ticketInformation,
            String loginUser, DataSource dataSource) {
        Double efforts = null;
		// If No Efforts are Enter return 1 else save the efforts and returns
        // effort Id
        try {
            efforts = Double.valueOf(ticketInformation.getEstimatedEffort());
        } catch (NumberFormatException ex) {
            return 1;
        }
        String ticketId = ticketInformation.getTicketId();
        return ManageOutageDao.saveEfforts(efforts, ticketId, loginUser,
                dataSource);
    }

    /**
     * Gets the label on the basis of its level. 1 - Help Desk 2 - Msp Dispatch
     *
     * @param int the level
     * @return String label
     */
    public static String getLevelLabel(int level) {
        String label = "";
        if (level == 1) {
            label = EscalationLevel.LABEL_1;
        } else if (level == 2) {
            label = EscalationLevel.LABEL_2;
        }
        return label;
    }

    /**
     * Gets the escalated ticket outage id(i.e., nActiveMonitorStateChangeID)..
     *
     * @param jobId - the job id.
     * @param ds - the ds.
     * @return the escalated ticket outage id(i.e.,
     * nActiveMonitorStateChangeID).
     */
    public static int[] getEscalatedTicketOutageID(String jobId, DataSource ds) {
        int outageIds[] = ManageOutageDao.getEscalatedOutageTicketMainID(jobId,
                ds);
        return outageIds;
    }

    /**
     * Sets the status for escalated ticket from ticket dashboard.
     *
     * @param jobId - the job id.
     * @param nActiveMonitorStateChangeID - the n active monitor state change
     * id.
     * @param ds - the ds.
     * @return the int - the success/error code.
     */
    public static int setStatusForDashBoardEscalatedTicket(String jobId,
            int nActiveMonitorStateChangeID, int wugInstance,
            String loginuserid, DataSource ds) {
        int returnFlag = shiftEscalatedInProcessToHistory(
                nActiveMonitorStateChangeID, wugInstance, ds);

        if (returnFlag > 0) {
            returnFlag = ManageOutageDao
                    .checkAndSetEscalatedTicketStatusToClose(jobId,
                            loginuserid, ds);
        }
        return returnFlag;
    }

    /**
     * Shift escalated ticket's in process table records to history table.
     *
     * @param nActiveMonitorStateChangeID - the n active monitor state change
     * id.
     * @param ds - the ds.
     * @return the int - the success/error code.
     */
    public static int shiftEscalatedInProcessToHistory(
            int nActiveMonitorStateChangeID, int wugInstance, DataSource ds) {
        int returnFlag = -1;
        TicketInformation ticketInformation = ManageOutageDao
                .getTicketInformation(nActiveMonitorStateChangeID, wugInstance,
                        ds);
        returnFlag = ManageOutageDao.setResolvedByHelpDesk(
                nActiveMonitorStateChangeID + "", ticketInformation,
                wugInstance + "", ds);
        return returnFlag;
    }

    public static int dispatchCount(int jobId, DataSource ds) {
        int dispatches = ManageOutageDao.getTotalDispatchCount(jobId, ds);
        return dispatches;
    }

    public static int updateNextAction(String nextAction, String chngAction,
            String wugInstance, DataSource ds) {
        return ManageOutageDao.updateNextAction(nextAction, chngAction,
                wugInstance, ds);
    }

    public static int addInstallationNotesAndUpdateBackupCircuitStatus(
            String jobId, String installationNotes, String loginUser,
            TicketInformation ticketInformation,
            String nActiveMonitorStateChangeID, String wugInstance,
            DataSource dataSource) {
        // TODO Auto-generated method stub
        return ManageOutageDao.addinstallNotesAndUpdateBackupstatus(jobId,
                installationNotes, loginUser, ticketInformation,
                nActiveMonitorStateChangeID, wugInstance, dataSource);
    }

    /**
     * Send email when backUpcircuit and current status of ticket is down-down.
     *
     * @param emailForm the email form
     * @param emailFromName the email from name
     */
    public static void sendEmailForDownStatus(String emailForm,
            String emailFromName, String escalationLevel,
            String nActiveMonitorStateChangeID, String wugInstance,
            DataSource ds) {

        ManageOutageBean manageOutageBean = new ManageOutageBean();
        manageOutageBean.setDeviceInfoBean(ManageOutageDao
                .getDeviceInformation(
                        Integer.valueOf(nActiveMonitorStateChangeID),
                        Integer.valueOf(escalationLevel),
                        Integer.valueOf(wugInstance), ds));
        manageOutageBean.setSiteInfo(JobDAO.getSiteInfo(manageOutageBean
                .getDeviceInfoBean().getJobId(), ds));

        DeviceInfoBean deviceInfoBean = manageOutageBean.getDeviceInfoBean();

        SiteInfo siteInfo = manageOutageBean.getSiteInfo();

        Map<String, String> model = new HashMap<String, String>();
        if (StringUtils.isNotBlank(siteInfo.getSite_number())) {
            model.put("siteNumber", siteInfo.getSite_number());
        } else {
            model.put("siteNumber", " ");
        }
        model.put("escalationLevel",
                getLevelLabel(Integer.valueOf(escalationLevel)));

        if (StringUtils.isNotBlank(emailFromName)) {
            model.put("updatedUser", emailFromName);
        } else {
            model.put("updatedUser", " ");
        }

        if (StringUtils.isNotBlank(deviceInfoBean.getCircuitType())) {
            model.put("circuitType", deviceInfoBean.getCircuitType());
        } else {
            model.put("circuitType", " ");
        }

        if (StringUtils.isNotBlank(deviceInfoBean.getModemType())) {
            model.put("modemType", deviceInfoBean.getModemType());
        } else {
            model.put("modemType", "");
        }

        if (StringUtils.isNotBlank(deviceInfoBean.getInternetServiceLineNo())) {
            model.put("internetServiceLine",
                    deviceInfoBean.getInternetServiceLineNo());
        } else {
            model.put("internetServiceLine", "");
        }

        if (StringUtils.isNotBlank(deviceInfoBean.getIsp())) {
            model.put("ISP", deviceInfoBean.getIsp());
        } else {
            model.put("ISP", " ");
        }

        if (StringUtils.isNotBlank(deviceInfoBean.getIspSupportNo())) {
            model.put("ISPSupportNo", deviceInfoBean.getIspSupportNo());
        } else {
            model.put("ISPSupportNo", "");
        }

        if (StringUtils.isNotBlank(deviceInfoBean.getCircuitIP())) {
            model.put("circuitIp", deviceInfoBean.getCircuitIP());
        } else {
            model.put("circuitIp", deviceInfoBean.getCircuitIP());
        }

        if (StringUtils.isNotBlank(deviceInfoBean.getSiteContactNo())) {
            model.put("siteContactNo", deviceInfoBean.getSiteContactNo());
        } else {
            model.put("siteContactNo", " ");
        }

        if (StringUtils.isNotBlank(deviceInfoBean.getLocation())) {
            model.put("location", deviceInfoBean.getLocation());
        } else {
            model.put("location", " ");
        }

        if (StringUtils.isNotBlank(deviceInfoBean.getVOIP())) {
            model.put("VOIP", deviceInfoBean.getVOIP());
        } else {
            model.put("VOIP", " ");
        }
        if (StringUtils.isNotBlank(deviceInfoBean.getAccountNew())) {
            model.put("account", deviceInfoBean.getAccountNew());
        } else {
            model.put("account", " ");
        }
        if (StringUtils.isNotBlank(deviceInfoBean.getAccessCode())) {
            model.put("accessCode", deviceInfoBean.getAccessCode());
        } else {
            model.put("accessCode", " ");
        }
        if (StringUtils.isNotBlank(deviceInfoBean.getHelpDeskNew())) {
            model.put("helpDesk", deviceInfoBean.getHelpDeskNew());
        } else {
            model.put("helpDesk", "");
        }
        if (StringUtils.isNotBlank(deviceInfoBean.getFax())) {
            model.put("fax", deviceInfoBean.getFax());
        } else {
            model.put("fax", " ");
        }

        String emailContent = VelocityUtils.getMailContent(
                "emailTemplate/siteDownEmail.vm", model);

        EmailBean emailBean = new EmailBean();
        emailBean.setUsername(username);
        emailBean.setUserpassword(userPassword);
        emailBean.setSmtpservername(smtpServerName);
        emailBean.setSmtpserverport(smtpServerPort);
        emailBean.setTo(emailTo);
        emailBean.setSubject(subject);
        emailBean.setFrom(emailForm);
        emailBean.setFromName(emailFromName);
        emailBean.setContent(emailContent);
        emailBean.setType(" ");
        emailBean.setChangeStatus("");
        Email.Send(emailBean, "text/html", ds);

    }

}
