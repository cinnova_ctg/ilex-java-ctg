package com.mind.MSP.business;

import java.util.ArrayList;

import javax.sql.DataSource;

import com.mind.MSP.dao.MspUtilityDao;
import com.mind.MSP.dao.SystemPerformanceDAO;
import com.mind.MSP.formbean.SystemPerformanceFrom;
import com.mind.bean.msp.SystemPerformanceBean;
import com.mind.common.LabelValue;
import com.mind.msp.common.EscalationLevel;

/**
 * The Class SystemPerformanceOrganizer.
 */
public class SystemPerformanceOrganizer {

	private static StringBuffer baseGraphbeforeY = new StringBuffer(
			"<graph caption='' subcaption='' ");
	private static final String yCordinateSLA = "'System Performance'";
	private static final String yCordinateCOUNTS = "'Sites'";
	private static final String yCordinateBOUNCES = "'Bounces'";

	private static StringBuffer baseGraphAfterY = new StringBuffer(
			" showNames='1' showValues='0'  rotateNames='1' showColumnShadow='1' animation='1'"
					+ " showAlternateHGridColor='1' AlternateHGridColor='000000'  chartTopMargin='25' chartRightMargin= '30' divLineColor='000000' divLineAlpha='20'"
					+ " VDivlinecolor = '000000' VDivLineThickness='1' "
					+ " numdivlines='5' showLimits='1' showLegend= '1' legendPosition ='RIGHT'"
					+ " alternateHGridAlpha='5' canvasBorderColor='666666' baseFontColor='666666'>");

	// yAxisMinValue='98.8'

	/**
	 * Method name : setSystemPerformance. Description : This method is used to
	 * set the initial values for a form and all three graph's XML into
	 * SystemPerformanceBean.
	 * 
	 * @param systemPerformanceBean
	 *            the reference of SystemPerformanceBean
	 * @param ds
	 *            the reference of DataSource
	 */
	public static void setSystemPerformance(
			SystemPerformanceBean systemPerformanceBean, DataSource ds) {
		ArrayList<LabelValue> temp = new ArrayList<LabelValue>();
		temp = MspUtilityDao.getCustomerList(
				EscalationLevel.ILEX_CUSTOMER_NAME_BY_DAY, ds);
		LabelValue label = new LabelValue();
		label.setLabel("All Customers");
		label.setValue("0");
		if (temp.contains(label)) {
			temp.remove(label);
			temp.add(0, label);
		}

		systemPerformanceBean.setCustomerList(temp);
		ArrayList<SystemPerformanceBean> systemPerformanceBeans = SystemPerformanceDAO
				.getSearchList(systemPerformanceBean, ds);
		String goalData = SystemPerformanceDAO.getGoalLineData(
				systemPerformanceBean, ds);
		float[] maxCount = SystemPerformanceDAO.getMaxCount(
				systemPerformanceBean, ds);
		systemPerformanceBean.setSlaGraphXML(getGraphXML(
				systemPerformanceBeans, "SLA", goalData, maxCount));
		systemPerformanceBean.setBouncesGraphXML(getGraphXML(
				systemPerformanceBeans, "BOUNCES", goalData, maxCount));
		systemPerformanceBean.setCountsGraphXML(getGraphXML(
				systemPerformanceBeans, "COUNTS", goalData, maxCount));

		systemPerformanceBean.setSlaTable(SystemPerformanceDAO.getTableDetail(
				systemPerformanceBean, "SLA", ds));
		systemPerformanceBean.setCountTable(SystemPerformanceDAO
				.getTableDetail(systemPerformanceBean, "UCount", ds));
		systemPerformanceBean.setCostTable(SystemPerformanceDAO.getTableDetail(
				systemPerformanceBean, "Cost", ds));
	}

	/**
	 * Sets the form from search history bean.
	 * 
	 * @param MspSearchHistoryForm
	 *            the form
	 * @param SearchHistoryBean
	 *            the bean
	 */
	public static void setFormFromSystemPerformanceBean(
			SystemPerformanceFrom systemPerformanceFrom,
			SystemPerformanceBean systemPerformanceBean) {
		SystemPerformanceConvertor.setFormFromSystemPerformanceBean(
				systemPerformanceFrom, systemPerformanceBean);
	}

	/**
	 * Sets the search history bean from form.
	 * 
	 * @param MspSearchHistoryForm
	 *            the form
	 * @param SearchHistoryBean
	 *            the bean
	 */
	public static void setSystemPerformanceBeanFromForm(
			SystemPerformanceFrom systemPerformanceFrom,
			SystemPerformanceBean systemPerformanceBean) {
		SystemPerformanceConvertor.setSystemPerformanceBeanFromForm(
				systemPerformanceFrom, systemPerformanceBean);
	}

	/**
	 * Gets the graph xml.
	 * 
	 * @param systemPerformanceBeans
	 *            the system performance beans
	 * @param graphType
	 *            the graph type
	 * @param goalLineData
	 *            the goal line data
	 * @param maxCount
	 *            the max count
	 * 
	 * @return the graph xml
	 */
	private static String getGraphXML(
			ArrayList<SystemPerformanceBean> systemPerformanceBeans,
			String graphType, String goalLineData, float[] maxCount) {
		int length = 0;
		if (systemPerformanceBeans != null) {
			length = systemPerformanceBeans.size() - 2;
		}
		StringBuilder sb = new StringBuilder();
		sb.append(baseGraphbeforeY);
		sb.append(" xAxisName='Trailing 30 days'");
		sb.append(" yAxisName=");
		String legend = "";
		if (graphType.equals("SLA")) {
			sb.append(yCordinateSLA);
			sb.append(" yAxisMinValue='" + maxCount[2] + "'");
			sb.append(" yAxisMaxValue='100.0'");
			sb.append(" numberSuffix='%'");
			legend = "System Performance";
		} else if (graphType.equals("BOUNCES")) {
			sb.append(yCordinateBOUNCES);
			sb.append(" yAxisMaxValue='" + maxCount[0] + "'");
			sb.append(" decimalPrecision='0'");
			sb.append(" formatNumberScale='0'");
			legend = "Daily Bounces";
		} else if (graphType.equals("COUNTS")) {
			sb.append(yCordinateCOUNTS);
			sb.append(" yAxisMaxValue='" + maxCount[1] + "'");
			sb.append(" decimalPrecision='0'");
			sb.append(" formatNumberScale='0'");
			legend = "Daily Total";
		}
		sb.append(" numVDivLines = '" + length + "'");
		sb.append(baseGraphAfterY);

		// sb.append("<categories>");
		StringBuffer tempCategory = new StringBuffer("<categories>");
		StringBuffer tempDataSet1 = new StringBuffer(
				"<dataset seriesname='"
						+ legend
						+ "'  color='0099FF' anchorRadius='3' anchorSides='8' anchorBorderColor='1D8BD1' anchorBgColor='1D8BD1'>");
		StringBuffer tempDataSet2 = new StringBuffer(
				"<dataset seriesname='Goal'  color='#2dff2d' anchorRadius='0'>");

		int i = 0;
		for (SystemPerformanceBean systemPerformanceBean : systemPerformanceBeans) {
			tempCategory.append("<category name='");
			tempCategory.append(systemPerformanceBean.getxCordinateDate());
			tempCategory.append("'/>");

			tempDataSet1.append("<set value='");
			if (graphType.equals("SLA")) {
				tempDataSet1.append(systemPerformanceBean.getyCordinateSLA());
			} else if (graphType.equals("BOUNCES")) {
				tempDataSet1.append(systemPerformanceBean
						.getyCordinateBounces());
			} else if (graphType.equals("COUNTS")) {
				tempDataSet1.append(systemPerformanceBean.getyCordinateCount());
			}
			tempDataSet1.append("'/>");
			tempDataSet2.append("<set value='");
			tempDataSet2.append(goalLineData);

			if (i == 1) {
				tempDataSet2.append("' showValue = '" + i + "'/>");
			} else {
				tempDataSet2.append("' toolText = '99.98' />");
			}
			i++;
		}
		tempCategory.append("</categories>");
		tempDataSet1.append("</dataset>");
		tempDataSet2.append("</dataset>");

		if (graphType.equals("SLA")) {
			sb.append(tempCategory).append(tempDataSet1).append(tempDataSet2)
					.append("</graph>");
		} else {
			sb.append(tempCategory).append(tempDataSet1).append("</graph>");
		}
		return sb.toString();
	}

}
