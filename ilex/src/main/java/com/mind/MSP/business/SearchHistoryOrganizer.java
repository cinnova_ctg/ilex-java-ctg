package com.mind.MSP.business;

import javax.sql.DataSource;

import com.mind.MSP.dao.MspUtilityDao;
import com.mind.MSP.dao.MspSearchHistoryDao;
import com.mind.MSP.formbean.MspSearchHistoryForm;
import com.mind.bean.msp.SearchHistoryBean;
import com.mind.msp.common.EscalationLevel;

/**
 * The Class SearchHistoryOrganizer.
 */
public class SearchHistoryOrganizer {

	/**
	 * Sets the search history.
	 * 
	 * @param SearchHistoryBean
	 *            the history bean
	 * @param DataSource
	 *            the ds
	 */
	public static void setSearchHistory(SearchHistoryBean historyBean,
			DataSource ds) {
		historyBean.setCustomerList(MspUtilityDao.getOptionList(
				EscalationLevel.ILEX_CUSTOMER_NAME_FOR_CREDITS_RPT, ds));
		historyBean.setSearchList(MspSearchHistoryDao.getSearchList(
				historyBean, ds));
	}

	/**
	 * Sets the form from search history bean.
	 * 
	 * @param MspSearchHistoryForm
	 *            the form
	 * @param SearchHistoryBean
	 *            the bean
	 */
	public static void setFormFromSearchHistoryBean(MspSearchHistoryForm form,
			SearchHistoryBean bean) {
		SearchHistoryConvertor.setFormFromSearchHistoryBean(form, bean);
	}

	/**
	 * Sets the search history bean from form.
	 * 
	 * @param MspSearchHistoryForm
	 *            the form
	 * @param SearchHistoryBean
	 *            the bean
	 */
	public static void setSearchHistoryBeanFromForm(MspSearchHistoryForm form,
			SearchHistoryBean bean) {
		SearchHistoryConvertor.setSearchHistoryBeanFromForm(form, bean);
	}
}
