package com.mind.MSP.business;

import com.mind.MSP.formbean.ManageOutageForm;
import com.mind.bean.msp.DeviceInfoBean;
import com.mind.bean.msp.SiteInfo;
import com.mind.bean.msp.TicketInformation;

/**
 * The Class ManageOutageConverter.
 */
public class ManageOutageConverter {

	/**
	 * Sets the formbean from manage outage bean. Convert all manage outage bean
	 * data into ManageOutage Form
	 * 
	 * @param ManageOutageForm
	 *            the form
	 * @param ManageOutageBean
	 *            the bean
	 */
	public static void setFormFromManageOutageBean(ManageOutageForm form,
			ManageOutageBean bean) {
		setDeviceInformation(form, bean.getDeviceInfoBean());
		setSiteInfo(form, bean.getSiteInfo());
		setTicketInfo(form, bean.getTicketInformation());
		form.setProblemCategoryList(bean.getProblemCategoryList());
		form.setIssueOwnerList(bean.getIssueOwnerList());
		form.setResolutionList(bean.getResolutionList());
		form.setRootCauseList(bean.getRootCauseList());
		form.setNextActionList(bean.getNextActionList());
	}

	/**
	 * Sets the device information into formbean.
	 * 
	 * @param form
	 *            the form
	 * @param deviceInfo
	 *            the device info
	 */
	private static void setDeviceInformation(ManageOutageForm form,
			DeviceInfoBean deviceInfo) {
		form.setCircuitIP(deviceInfo.getCircuitIP());
		form.setCircuitType(deviceInfo.getCircuitType());
		form.setInternetServiceLineNo(deviceInfo.getInternetServiceLineNo());
		form.setIsp(deviceInfo.getIsp());
		form.setIspSupportNo(deviceInfo.getIspSupportNo());
		form.setLocation(deviceInfo.getLocation());
		form.setModemType(deviceInfo.getModemType());
		form.setSiteContactNo(deviceInfo.getSiteContactNo());
		form.setVOIP(deviceInfo.getVOIP());
		form.setAccountNew(deviceInfo.getAccountNew());
		form.setAccessCode(deviceInfo.getAccessCode());
		form.setHelpDeskNew(deviceInfo.getHelpDeskNew());
		form.setFax(deviceInfo.getFax());
	}

	/**
	 * Sets the site info into formbean.
	 * 
	 * @param form
	 *            the form
	 * @param siteInfo
	 *            the site info
	 */
	private static void setSiteInfo(ManageOutageForm form, SiteInfo siteInfo) {
		form.setSiteAddress(siteInfo.getSite_address());
		form.setSiteCity(siteInfo.getSite_city());
		form.setSiteNumber(siteInfo.getSite_number());
		form.setSiteState(siteInfo.getSite_state());
		form.setSiteZip(siteInfo.getSite_zip());
		form.setSiteTimeZone(siteInfo.getSite_time_zone());

	}

	/**
	 * Sets the ticket info into formbean.
	 * 
	 * @param form
	 *            the form
	 * @param ticketInformation
	 *            the ticket information
	 */
	private static void setTicketInfo(ManageOutageForm form,
			TicketInformation ticketInformation) {
		form.setProblemCategory(ticketInformation.getProblemCategory());
		form.setProbleDescription(ticketInformation.getProbleDescription());
		form.setAppendixId(ticketInformation.getAppendixId());
		form.setJobId(ticketInformation.getJobId());
		form.setTicketNumber(ticketInformation.getTicketNumber());
		form.setTicketId(ticketInformation.getTicketId());
		form.setInstallNotesPresent(ticketInformation.isInstallNotesPresent());
		form.setLatestInstallNotesDate(ticketInformation
				.getLatestInstallNotesDate());
		form.setLatestInstallNotes(ticketInformation.getLatestInstallNotes());
		form.setSecondLatestInstallNotesDate(ticketInformation
				.getSecondLatestInstallNotesDate());
		form.setSecondLatestInstallNotes(ticketInformation
				.getSecondLatestInstallNotes());
		// Reset Estimated Effort To Blank on initial and redisplay as per
		// effort Widget Changes
		// requirement
		form.setEstimatedEffort("");
		form.setIssueOwner(ticketInformation.getIssueOwner());
		form.setRootCause(ticketInformation.getRootCause());
		form.setResolution(ticketInformation.getCorrectiveAction());
		form.setCurrentStatus(ticketInformation.getCurrentStatus());
		form.setLastAction(ticketInformation.getLastAction());
		form.setBackupCircuitStatus(ticketInformation.getBackupCircuitStatus());
		form.setInstallNotesCount(ticketInformation.getInstallNotesCount());
	}

	public static void setManageOutageBeanFromForm(ManageOutageForm form,
			ManageOutageBean bean) {
		setTicketInfoFromForm(form, bean.getTicketInformation());
	}

	private static void setTicketInfoFromForm(ManageOutageForm form,
			TicketInformation ticketInformation) {
		ticketInformation.setProblemCategory(form.getProblemCategory());
		ticketInformation.setProbleDescription(form.getProbleDescription());
		ticketInformation.setAppendixId(form.getAppendixId());
		ticketInformation.setJobId(form.getJobId());
		ticketInformation.setTicketNumber(form.getTicketNumber());
		ticketInformation.setTicketId(form.getTicketId());
		ticketInformation.setSecondLatestInstallNotes(form
				.getSecondLatestInstallNotes());
		ticketInformation.setEstimatedEffort(form.getEstimatedEffort());
		ticketInformation.setIssueOwner(form.getIssueOwner());
		ticketInformation.setRootCause(form.getRootCause());
		ticketInformation.setCorrectiveAction(form.getResolution());
		ticketInformation.setBackupCircuitStatus(form.getBackupCircuitStatus());
	}

}
