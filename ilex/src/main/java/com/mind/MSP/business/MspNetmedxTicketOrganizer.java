package com.mind.MSP.business;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.mind.MSP.dao.MspNetmedxTicketDAO;
import com.mind.MSP.formbean.MspNetmedxTicketForm;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.TicketTab;
import com.mind.common.LabelValue;
import com.mind.common.Util;
import com.mind.dao.PM.Jobdao;
import com.mind.msp.common.MspNetMedxTicketType;
import com.mind.newjobdb.dao.TicketDAO;
import com.mind.newjobdb.dao.TicketNatureDAO;
import com.mind.newjobdb.dao.TicketRequestInfoDAO;

/**
 * The Class MspNetmedxTicketOrganizer.
 *
 * @author Vijay Kumar Singh
 */
public class MspNetmedxTicketOrganizer {

    private static final Logger logger = Logger
            .getLogger(MspNetmedxTicketOrganizer.class);

    /**
     * Purpose: Sets the default value for ticket creation page like fill
     * Combo-box value and its default selected values.
     *
     * @param MspNetmedxTicketBean
     * @param DataSource
     */
    public static void setDefaultValue(MspNetmedxTicketBean bean, DataSource ds) {
        bean.setProblemCategoryList(TicketNatureDAO.getProblemCategories(ds));
        bean.setCustomerList(MspNetmedxTicketDAO.getCustomerList(ds));
        bean.setProblemDescription(MspNetMedxTicketType.PROBLEM_DESCRIPTION
                .getCode());
        bean.setProblemCategory(Util.getListValue(
                bean.getProblemCategoryList(),
                MspNetMedxTicketType.PROBLEM_CATEGORY.getCode(), "label"));
        bean.setSiteName(null);
    }

    /**
     * Purpose: Find and set site list.
     *
     * @param DataSource
     * @param String - search String
     *
     * @return ArrayList<LabelValue>
     */
    public static ArrayList<LabelValue> setSiteList(DataSource ds,
            MspNetmedxTicketBean bean) {
        String[] msaAppendixId = Util.getSplitArray(bean.getCustomer(),
                MspNetMedxTicketType.SEPARATOR1.getCode());
        return MspNetmedxTicketDAO.findSiteList(ds, bean.getSiteSearch(),
                msaAppendixId[1]);
    }

    /**
     * Purpose: Create the ticket.
     *
     * @param String the login user id
     * @param MspNetmedxTicketBean
     * @param DataSource
     *
     * @return String[] 0-jobId, 1-ticketId, 2-Received date
     */
    public static String[] createTicket(String loginUserId,
            MspNetmedxTicketBean bean, DataSource ds) {
        if (logger.isDebugEnabled()) {
            logger.debug("createTicket");
        }
        String[] msaAppendixIds = Util.getSplitArray(bean.getCustomer(),
                MspNetMedxTicketType.SEPARATOR1.getCode());

        /* Fill New Ticket required data */
        ArrayList<LabelValue> resourceList = TicketRequestInfoDAO
                .getResourceList(1, Long.valueOf(msaAppendixIds[1]),
                        MspNetMedxTicketType.POP_VALUE.getCode(),
                        MspNetMedxTicketType.RESOURCE_CLASS_NAME.getCode(), ds);
        ArrayList<LabelValue> requestTypesList = TicketRequestInfoDAO
                .getRequestTypes(ds);
        ArrayList<LabelValue> criticalityList = TicketRequestInfoDAO
                .getCriticalities(Long.valueOf(msaAppendixIds[0]), ds);
        TicketTab ticketTab = TicketDAO.createNewTicket(Long
                .valueOf(msaAppendixIds[1]), ds);
        ticketTab.getRequestInfo().setRequestor(
                MspNetMedxTicketType.REQUESTOR.getCode());
        ticketTab.getRequestInfo().setRequestorEmail(
                MspNetMedxTicketType.REQUESTOR_EMAIL.getCode());
        ticketTab.getRequestNature().setSpecialInstructions(
                MspNetMedxTicketType.SPECIAL_INSTRUCTIONS.getCode());
        ticketTab.getRequestInfo()
                .setPPS(
                        getBoolean(MspNetMedxTicketType.PERIOD_OF_PERFORMANCE
                                .getCode()));
        String[] siteDivId = Util.getSplitArray(bean.getSiteName(),
                MspNetMedxTicketType.SEPARATOR1.getCode());
        ticketTab.setDeviceId(Long.valueOf(siteDivId[0]));
        ticketTab.getRequestInfo().setSite(siteDivId[1]);

        ticketTab.getScheduleInfo().setRequestedTimestamp(
                IlexTimestamp.getSystemDateTime());
        ticketTab.getRequestInfo().setStandBy(
                getBoolean(MspNetMedxTicketType.STANDBY.getCode()));
        ticketTab.getRequestInfo().setMSP(
                getBoolean(MspNetMedxTicketType.MSP.getCode()));
        ticketTab.getRequestInfo().setHelpDesk(
                getBoolean(MspNetMedxTicketType.HELPDESK.getCode()));
        ticketTab.getRequestNature().setProblemCategory(
                bean.getProblemCategory());
        ticketTab.getRequestInfo().setCustomerReference(
                MspNetMedxTicketType.MSP_AGREEMENT.getCode());
        ticketTab.getRequestNature().setProblemDescription(
                bean.getProblemDescription());
        ticketTab.getRequestInfo().setCriticality(
                Util.getListValue(criticalityList,
                        MspNetMedxTicketType.CRITICALITY.getCode(), "label"));
        ticketTab.getRequestInfo()
                .setPPS(
                        getBoolean(MspNetMedxTicketType.PERIOD_OF_PERFORMANCE
                                .getCode()));
        ticketTab.getRequestInfo().setResource(
                Util.getListValue(resourceList, MspNetMedxTicketType.RESOURCE
                        .getCode(), "label"));
        ticketTab.getScheduleInfo().setPreferredTimestamp(
                IlexTimestamp.getSystemDateTime());
        ticketTab.getScheduleInfo().setWindowFromTimestamp(
                IlexTimestamp.converToTimestamp(null));
        ticketTab.getScheduleInfo().setWindowToTimestamp(
                IlexTimestamp.converToTimestamp(null));
        ticketTab.setAppendixId(Long.valueOf(msaAppendixIds[1]));
        ticketTab
                .setMonitoringSourceCd(MspNetMedxTicketType.MSP_NETMEDIX_MONITORING_SOURCE
                        .getInt());
        ticketTab.getRequestInfo().setRequestType(
                Util.getListValue(requestTypesList,
                        MspNetMedxTicketType.REQUEST_TYPE.getCode(), "label"));
        /* Call database to create a ticket. */
        Jobdao.addLogEntry(ds, "MSP NetMedX Ticket Organizer!", "");
        String[] createdIds = TicketDAO.updateTicket(ticketTab, Long
                .valueOf(loginUserId), ds);
        return createdIds;
    }

    /**
     * Purpose: convert string ture/false value to boolean true/false
     *
     * @param String - true/false
     * @return boolean
     */
    private static boolean getBoolean(String code) {
        return code == "true" ? true : false;
    }

    /**
     * Purpose: Copy form bean data from logic bean using BeanUtils.
     *
     * @param MspNetmedxTicketForm
     * @param MspNetmedxTicketBean
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public static void copyFormFromBean(MspNetmedxTicketForm dest,
            MspNetmedxTicketBean orig) throws IllegalAccessException,
            InvocationTargetException {
        BeanUtils.copyProperties(dest, orig);
    }

    /**
     * Purpose: copy logic bean data to form bean.
     *
     * @param MspNetmedxTicketBean
     * @param MspNetmedxTicketForm
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public static void copyBeanFromForm(MspNetmedxTicketBean dest,
            MspNetmedxTicketForm orig) throws IllegalAccessException,
            InvocationTargetException {
        BeanUtils.copyProperties(dest, orig);
    }

}
