package com.mind.MSP.business;

import javax.sql.DataSource;

import com.mind.MSP.dao.ChronicSiteDAO;
import com.mind.MSP.dao.MspUtilityDao;
import com.mind.MSP.formbean.ChronicSitesForm;
import com.mind.bean.msp.ChronicSitesBean;
import com.mind.msp.common.EscalationLevel;

public class ChronicSiteDelegate {
	/**
	 * Method name : populateCustomerList. Description : sets customer ids and
	 * names as LabelValue pairs in bean object.
	 * 
	 * @param bean
	 *            : reference of ChronicSitesBean.
	 * @param ds
	 *            : reference of DataSource.
	 */
	public static void populateCustomerList(ChronicSitesBean bean, DataSource ds) {
		bean.setCustomerList(MspUtilityDao.getOptionList(
				EscalationLevel.ILEX_CUSTOMER_NAME_FOR_CHONIC_SITES, ds));
	}

	/**
	 * Method name : setCustomerListToForm. Description : calls a method in
	 * converter class to set customer list from bean to form. This is called
	 * only when page is initial loaded without search.
	 * 
	 * @param form
	 *            : reference of ChronicSitesForm.
	 * @param bean
	 *            : reference of ChronicSitesBean.
	 */
	public static void setCustomerListToForm(ChronicSitesForm form,
			ChronicSitesBean bean) {
		ChronicSiteConverter.mapCustomerListToForm(form, bean);
	}

	/**
	 * Method name : convertFormToBean. Description : call method in converter
	 * class to copy form attribute values to corresponding bean attribute
	 * values.
	 * 
	 * @param form
	 *            : reference of ChronicSitesForm.
	 * @param bean
	 *            : reference of ChronicSitesBean.
	 */
	public static void convertFormToBean(ChronicSitesForm form,
			ChronicSitesBean bean) {
		ChronicSiteConverter.mapFormToBean(form, bean);
	}

	/**
	 * Method name : viewChronicSites. Description : search problem sites to be
	 * displayed when user clicks view button on page.
	 * 
	 * @param bean
	 *            : reference of ChronicSitesBean.
	 * @param ds
	 *            : reference of DataSource.
	 */
	public static void viewChronicSites(ChronicSitesBean bean, DataSource ds) {
		bean.setChronicSiteList(ChronicSiteDAO.getChronicSitesData(bean, ds));
		populateCustomerList(bean, ds);
	}

	/**
	 * Method name : convertBeanToForm. Description : call method in converter
	 * class to copy bean attribute values to corresponding form attribute
	 * values.
	 * 
	 * @param form
	 *            : reference of ChronicSitesForm.
	 * @param bean
	 *            : reference of ChronicSitesBean.
	 */
	public static void convertBeanToForm(ChronicSitesForm form,
			ChronicSitesBean bean) {
		ChronicSiteConverter.mapBeanToForm(form, bean);
	}
}
