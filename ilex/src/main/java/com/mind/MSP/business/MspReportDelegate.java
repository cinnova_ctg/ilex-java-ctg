package com.mind.MSP.business;

import java.lang.reflect.InvocationTargetException;

import javax.sql.DataSource;

import com.mind.MSP.dao.MSpReportDAO;
import com.mind.MSP.formbean.MspReportForm;
import com.mind.bean.msp.MspReportBean;

public class MspReportDelegate {
	
	/**
	 * Method name	: ticketProcessedFormToBean.
	 * Description	: calls a method in CustomerCreditDelegate class to map form elements to bean elements.
	 * @param form	: reference to MspReportForm.
	 * @param bean 	: reference of MspReportBean.
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public static void ticketProcessedFormToBean(MspReportForm form, MspReportBean bean) throws IllegalAccessException, InvocationTargetException {
		CustomerCreditDelegate.convertFormToBean(form, bean);
	}

	/**
	 * Method name	: ticketProcessedBeanToForm.
	 * Description	: calls a method in CustomerCreditDelegate class to map bean elements to form elements.
	 * @param form	: reference to MspReportForm.	
	 * @param bean 	: reference of MspReportBean.
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public static void ticketProcessedBeanToForm(MspReportForm form, MspReportBean bean) throws IllegalAccessException, InvocationTargetException {
		CustomerCreditDelegate.convertBeanToForm(form, bean);
	}
	
	/**
	 * Method name	: searchDayWiseProcessedTickets.
	 * Description	: This method sets list of processed tickets in bean object.
	 * @param bean 	: reference of MspReportBean.
	 * @param ds 	: reference of DataSource.
	 */
	public static void searchDayWiseProcessedTickets(MspReportBean bean, DataSource ds) {
		bean.setMspSearch(MSpReportDAO.getListofProcessedTickets(bean, ds));
	}
}
