package com.mind.MSP.business;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

import com.mind.MSP.formbean.MspReportForm;
import com.mind.bean.msp.MspReportBean;

public class CustomerCreditConvertor {
	/**
	 * Method name	: convertFormToBean.
	 * Description	: map form elements to bean elements.
	 * @param form	: reference to MspReportForm	
	 * @param bean 	: reference of MspReportBean.
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public static void mapFormToBean(MspReportForm form, MspReportBean bean) throws IllegalAccessException, InvocationTargetException {
		BeanUtils.copyProperties(bean, form);
	}
	
	/**
	 * Method name	: convertBeanToForm.
	 * Description	: map bean elements to form elements.
	 * @param form	: reference to MspReportForm	
	 * @param bean 	: reference of MspReportBean.
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public static void mapBeanToForm(MspReportForm form, MspReportBean bean) throws IllegalAccessException, InvocationTargetException {
		BeanUtils.copyProperties(form, bean);
		form.setSearchList(bean.getMspSearch().getSearchList());
		form.setTotal(bean.getMspSearch().getTotal());
	}
	
	/**
	 * Method name	: setCustomerListToForm.
	 * Description	: set customer list from bean to form. This is called only when page is initial 
	 * 				  loaded without search. 
	 * @param form	: reference to MspReportForm	
	 * @param bean 	: reference of MspReportBean.
	 */
	public static void mapCustomerListToForm(MspReportForm form, MspReportBean bean) {
		form.setCustomerList(bean.getCustomerList());
	}
}
