package com.mind.MSP.business;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

import javax.sql.DataSource;

import com.mind.MSP.dao.MSPCCReportDao;
import com.mind.MSP.formbean.MSPCCReportForm;
import com.mind.bean.msp.MSPCCReportBean;
import com.mind.msp.common.EscalationLevel;
import com.mind.type.MSPTaskCategory;

public class MSPCCReportOrganizer {

	/**
	 * Sets the MSPCCReportBean.
	 * 
	 * @param MSPCCReportBean
	 *            the mspccReportBean
	 * @param String
	 *            appendixId
	 * @param DataSource
	 *            the ds
	 */
	public static void setMSPCCReport(MSPCCReportBean mspccReportBean,
			String appendixId, DataSource ds) {

		mspccReportBean.setSectionBHeaderList(MSPCCReportDao.getHeaderList(
				appendixId, ds));
	}

	public static void setMSPCCChickFilReport(MSPCCReportBean mspccReportBean,
			String appendixId, DataSource ds) {

		int isExist = 0;
		String categoryName = "";
		String[] categoryColumns = null;

		ArrayList<MSPCCReportBean> dbSectionHeaderList =
				MSPCCReportDao.getHeaderList(appendixId, ds);

		ArrayList<MSPCCReportBean> categoryHeaderList =
				new ArrayList<MSPCCReportBean>();

		MSPTaskCategory[] arrMSPTaskCategory =
				MSPTaskCategory.displayAllCategoryNames();

		Iterator iterator = null;

		for (MSPTaskCategory mtc : arrMSPTaskCategory) {
			isExist = 0;
			iterator = dbSectionHeaderList.iterator();
			while (iterator.hasNext()) {
				MSPCCReportBean mspCCReport = (MSPCCReportBean) iterator.next();

				categoryColumns =
						MSPTaskCategory.showColumnNamebyCategory(mtc
								.getCategoryName());

				if (mspCCReport.getName().equals(mtc.getCategoryName())) {
					mspCCReport.setCategoryColumns(categoryColumns);

					categoryHeaderList.add(mspCCReport);
					isExist = 1;
					break;
				}
			}

			if (isExist == 0) {
				MSPCCReportBean mspCCReport = new MSPCCReportBean();
				mspCCReport.setName(mtc.getCategoryName());
				mspCCReport.setValue("0");
				mspCCReport.setCount("0");
				mspCCReport.setCategoryColumns(categoryColumns);
				categoryHeaderList.add(mspCCReport);
			}
		}

		mspccReportBean.setSectionBHeaderList(categoryHeaderList);

	}

	/**
	 * Sets the mspcc report CompletedTask list.
	 * 
	 * @param mspccReportBean
	 *            the mspcc report bean
	 * @param appendixId
	 *            the appendix id
	 * @param ds
	 *            the ds
	 */
	public static void setMSPCCReportCompletedTaskList(
			MSPCCReportBean mspccReportBean, String appendixId, DataSource ds) {
		String query = EscalationLevel.BASE_QUERY_FORMSPCCREPORT;
		String orderBy = EscalationLevel.BASE_QUERY_ORDERBYCLAUSE;
		mspccReportBean.setSectionAList(MSPCCReportDao.getList(query
				+ " where lr_ri_rt_pr_id = " + appendixId
				+ " and lr_ri_group ='A-1' " + orderBy, ds));

		String[] labelValue =
				MSPCCReportDao.getConstantValues(query
						+ " where lr_ri_rt_pr_id = " + appendixId
						+ " and lr_ri_group ='A-2' and lr_ri_sequence =1", ds);
		mspccReportBean.setStrtDateName(labelValue[0]);
		mspccReportBean.setStrtDateValue(labelValue[1]);

		labelValue =
				MSPCCReportDao.getConstantValues(query
						+ " where lr_ri_rt_pr_id = " + appendixId
						+ " and lr_ri_group ='A-2' and lr_ri_sequence =2", ds);
		mspccReportBean.setPlannedDateName(labelValue[0]);
		mspccReportBean.setPlannedDateValue(labelValue[1]);

		labelValue =
				MSPCCReportDao.getConstantValues(query
						+ " where lr_ri_rt_pr_id = " + appendixId
						+ " and lr_ri_group ='A-2' and lr_ri_sequence =3", ds);
		mspccReportBean.setSiteCountName(labelValue[0]);
		mspccReportBean.setSiteCountValue(labelValue[1]);

		labelValue =
				MSPCCReportDao.getConstantValues(query
						+ " where lr_ri_rt_pr_id = " + appendixId
						+ " and lr_ri_group ='A-2' and lr_ri_sequence =4", ds);
		mspccReportBean.setLastUpdateDateName(labelValue[0]);
		mspccReportBean.setLastUpdateDateValue(labelValue[1]);

		MSPCCReportDao.getTotalVlaue(mspccReportBean, appendixId, ds);
	}

	/**
	 * Sets the mspcc report activity trend list.
	 * 
	 * @param mspccReportBean
	 *            the mspcc report bean
	 * @param appendixId
	 *            the appendix id
	 * @param ds
	 *            the ds
	 */
	public static void setMSPCCReportActivityTrendList(
			MSPCCReportBean mspccReportBean, String appendixId, DataSource ds) {
		String query = EscalationLevel.BASE_QUERY_FORMSPCCREPORT;
		String orderBy = EscalationLevel.BASE_QUERY_ORDERBYCLAUSE;

		mspccReportBean.setSectionCList(MSPCCReportDao.getList(query
				+ " where lr_ri_rt_pr_id = " + appendixId
				+ " and lr_ri_group ='C-1' " + orderBy, ds));
		mspccReportBean.setSectionDList(MSPCCReportDao.getList(query
				+ " where lr_ri_rt_pr_id = " + appendixId
				+ " and lr_ri_group ='D-1' " + orderBy, ds));
		mspccReportBean.setSectionEList(MSPCCReportDao.getList(query
				+ " where lr_ri_rt_pr_id = " + appendixId
				+ " and lr_ri_group ='E-1' " + orderBy, ds));
	}

	/**
	 * Sets the mspcc report pending and complete list.
	 * 
	 * @param mspccReportBean
	 *            the mspcc report bean
	 * @param appendixId
	 *            the appendix id
	 * @param ds
	 *            the ds
	 */
	public static void setMSPCCReportPendingAndCompleteList(
			MSPCCReportBean mspccReportBean, String appendixId, DataSource ds) {
		String query = EscalationLevel.BASE_QUERY_FORMSPCCREPORT;
		String orderBy = EscalationLevel.BASE_QUERY_ORDERBYCLAUSE;

		mspccReportBean.setSectionFList(MSPCCReportDao.getList(query
				+ " where lr_ri_rt_pr_id = " + appendixId
				+ " and lr_ri_group ='F-1' " + orderBy, ds));
	}

	/**
	 * Sets the form from manage outage.
	 * 
	 * @param ManageOutageForm
	 *            the form
	 * @param ManageOutageBean
	 *            the bean
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public static void setFormFromMSPCCReport(MSPCCReportForm form,
			MSPCCReportBean mspccReportBean) throws IllegalAccessException,
			InvocationTargetException {
		MSPCCReportConvertor.setFormFromMSPCCReportBean(form, mspccReportBean);
	}

	public static ArrayList<TreeMap<String, String>> getSectionBInfo(
			String appendixId, String titleId, DataSource ds) {
		return MSPCCReportDao.getSectionBInfo(
				EscalationLevel.BASE_QUERY_SECTIONB
						+ " where lr_ci_rt_pr_id = " + appendixId
						+ " and lr_ci_pc_prj_item_id = " + titleId, ds);
	}

	public static ArrayList<TreeMap<String, String>> getChickFilSectionBInfo(
			String appendixId, String titleId, String categoryName,
			DataSource ds) {

		String sql = "";
		String[] categoryColumns =
				MSPTaskCategory.showColumnNamebyCategory(categoryName);
		sql =
				EscalationLevel.BASE_QUERY_CHICKFIL_SECTIONB
						+ " where lr_ci_rt_pr_id = " + appendixId
						+ " and lr_ci_pc_prj_item_id = " + titleId;

		sql = sql + " order by lr_ci_js_id, lr_jc_name";
		// System.out.println("Organizer=== " + sql);

		return MSPCCReportDao.getCheckFilSectionBInfo(sql, categoryColumns, ds);
	}

	public static boolean isConfigured(String appendixId, DataSource ds) {
		String query =
				EscalationLevel.BASE_CHECK_QUERY + " where lr_rt_pr_id = "
						+ appendixId + " and lr_rt_status = '1'";
		return MSPCCReportDao.isConfigured(query, ds);
	}

}
