package com.mind.MSP.business;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import com.mind.MSP.formbean.ManageShippingInstructionForm;

public class ShippingInstructionConverter {
	public static void setFormFromShippingInstructionBean(
			ShippingInstructionBean bean, ManageShippingInstructionForm form)
			throws IllegalAccessException, InvocationTargetException {

		String defaultValue = "Not Available";
		ArrayList<String> resources = new ArrayList<String>();

		form.setJobId(bean.getJobId());
		form.setAppendixId(form.getAppendixId());
		form.setSiteNumber(bean.getSiteInfo().getSite_number());
		form.setSiteAddress(bean.getSiteInfo().getSite_address());
		form.setSiteCity(bean.getSiteInfo().getSite_city());
		form.setSiteState(bean.getSiteInfo().getSite_state());
		form.setSiteZip(bean.getSiteInfo().getSite_zip());
		form.setSitePrimaryPoc(bean.getSiteInfo().getSite_primary_poc());
		form.setMsaName(bean.getSiteInfo().getMsaName());
		
		if (bean.getCustomFieldUsedMap().containsKey("Required Delivery Date")) {
			form.setReqDeliveryDate(bean.getCustomFieldUsedMap().get(
					"Required Delivery Date"));
		} else {
			form.setReqDeliveryDate(defaultValue);
		}

		if (bean.getCustomFieldUsedMap().containsKey("Circuit Type")) {
			form.setCircuitType(bean.getCustomFieldUsedMap()
					.get("Circuit Type"));
		} else {
			form.setCircuitType(defaultValue);
		}

		if (bean.getCustomFieldUsedMap().containsKey("POTS Type")) {
			form.setPotsType(bean.getCustomFieldUsedMap().get("POTS Type"));
		} else {
			form.setPotsType(defaultValue);
		}
		
		if (bean.getCustomFieldUsedMap().containsKey("Modem Location")) {
			form.setModemLocation(bean.getCustomFieldUsedMap().get("Modem Location"));
		} else {
			form.setModemLocation("ModemLocationNotSetted");
		}
		
		if(form.getCircuitType() != null && !form.getCircuitType().equals(defaultValue)) {
			resources.add("One (1) White Backboard with CNS Label");
			resources.add("One (1) CBA 250 Cradlepoint");
			resources.add("One (1) USB EVDO card");
			resources.add("One (1) CPS APC");
			resources.add("Two (2) 7 Foot, White Cat5e Patch Cables");
			
			resources.add("");
			resources.add("");
			resources.add("");
			resources.add("");
			resources.add("");
			resources.add("");
		}

		if (form.getCircuitType().equals("Cable")) {
			resources.add("One (1) 7 Foot, Pink Cat5e Patch Cable");
			resources.add("Two (2) Modem Labels");
		} else if (form.getCircuitType().equals("ADSL")) {
			resources.add("One (1) 7 Foot, Pink Cat5e Patch Cable");
			resources.add("Two (2) Modem Labels");
			
			if (form.getPotsType().toUpperCase().startsWith("LINE")
					|| form.getPotsType().toUpperCase().startsWith("MAIN")
					|| form.getPotsType().toUpperCase().contains("FAX")) {
				resources.add("");
				resources.add("");
				resources.add("");
				resources.add("");
				resources.add("");
				resources.add("");
				resources.add("Suttle POTS NID Splitter");
				resources.add("CNS Return Ship Label");
			}
			if(form.getModemLocation()!= null && form.getModemLocation().equals("CNS")) {
				resources.add("");
				resources.add("");
				resources.add("");
				resources.add("");
				resources.add("");
				resources.add("");
				resources.add("Modem");
			} else if (form.getModemLocation()== null || form.getModemLocation().equals("") || form.getModemLocation().equals(defaultValue)){
				resources.add("");
				resources.add("");
				resources.add("");
				resources.add("");
				resources.add("");
				resources.add("");
				resources.add("Verify modem shipping requirement with Operations.");
			}
		} else if (form.getCircuitType().equals("T1")) {
			resources.add("One (1) 7 Foot, Pink  Cat5e Crossover Cable");
			resources.add("Two (2) Modem Labels");
		} else if(form.getCircuitType().equals("FIOS")) {
			resources.add("One (1) 7 Foot, Pink Cat5e Patch Cable");
		}
		form.setResourceArr(resources);
	}
}
