package com.mind.MSP.business;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

import com.mind.MSP.formbean.MSPCCReportForm;
import com.mind.bean.msp.MSPCCReportBean;

public class MSPCCReportConvertor {
	
	public static void setFormFromMSPCCReportBean(MSPCCReportForm form, MSPCCReportBean mspccReportBean) throws IllegalAccessException, InvocationTargetException {
		BeanUtils.copyProperties(form, mspccReportBean);
	}
}
