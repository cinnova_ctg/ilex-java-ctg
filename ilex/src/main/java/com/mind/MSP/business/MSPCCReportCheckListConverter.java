package com.mind.MSP.business;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.beanutils.BeanUtils;

import com.mind.MSP.formbean.ManageCheckListDataForm;
import com.mind.bean.newjobdb.ChecklistStatusType;
import com.mind.bean.newjobdb.JobWorkflowItem;
import com.mind.newjobdb.dao.JobWorkflowChecklist;

public class MSPCCReportCheckListConverter {
	
		public static void setFormFromMSPCCReportCheckListBean(MSPCCReportCheckListBean bean, ManageCheckListDataForm form) throws IllegalAccessException, InvocationTargetException {
			BeanUtils.copyProperties(form, bean);
			setFormFromJobWorkflowChecklist(form.getChecklist(), form);
		}
		
		public static void setFormFromJobWorkflowChecklist(JobWorkflowChecklist jobWorkflowChecklist,ManageCheckListDataForm form) {
			
			ArrayList<JobWorkflowItem> jobWorkflowItems = (jobWorkflowChecklist.getTopPendingItems(2)).getJobWorkflowItems();
			Iterator iter = jobWorkflowItems.iterator();		
			String[] jobWorkflowItemDescription = new String[jobWorkflowItems.size()];
			long[] jobWorkflowItemId = new long[jobWorkflowItems.size()];
			String[] jobWorkflowItemStatus = new String[jobWorkflowItems.size()];		
			String[] jobWorkflowItemSequence = new String[jobWorkflowItems.size()];
			String[] jobWorkflowItemReferenceDate = new String[jobWorkflowItems.size()];
			String[] jobWorkflowItemUser = new String[jobWorkflowItems.size()];
			
			int i = 0;
			while (iter.hasNext()){			
				JobWorkflowItem jobWorkflowItem = (JobWorkflowItem)iter.next();
				jobWorkflowItemDescription[i] = jobWorkflowItem.getDescription();
				jobWorkflowItemId[i] = jobWorkflowItem.getItemId();
				if(jobWorkflowItem.getItemId()>0) {
					jobWorkflowItemStatus[i] = ChecklistStatusType.getStatusName(jobWorkflowItem.getItemStatusCode());
				} else {
					jobWorkflowItemStatus[i]="";
				}
				jobWorkflowItemSequence[i] = jobWorkflowItem.getSequence();
				jobWorkflowItemReferenceDate[i] = jobWorkflowItem.getReferenceDate();
				jobWorkflowItemUser[i] = jobWorkflowItem.getUser();
				i++;	
			}
			form.setJobWorkflowItemDescription(jobWorkflowItemDescription);
			form.setJobWorkflowItemId(jobWorkflowItemId);
			form.setJobWorkflowItemStatus(jobWorkflowItemStatus);
			form.setJobWorkflowItemSequence(jobWorkflowItemSequence);
			form.setJobWorkflowItemReferenceDate(jobWorkflowItemReferenceDate);
			form.setJobWorkflowItemUser(jobWorkflowItemUser);
			form.setAllItemStatus(jobWorkflowChecklist.getAllItemStatus());
			form.setItemListSize(jobWorkflowChecklist.getItemListSize());
		}
}
