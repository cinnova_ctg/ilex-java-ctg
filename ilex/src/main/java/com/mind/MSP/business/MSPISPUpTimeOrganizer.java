package com.mind.MSP.business;

import javax.sql.DataSource;

import com.mind.MSP.dao.MSPISPUpTimeDao;
import com.mind.MSP.formbean.MSPISPUpTimeForm;

public class MSPISPUpTimeOrganizer {
	
	/**
	 * Sets the mspisp bean.
	 * 
	 * @param mspispUpTimeBean the mspisp up time bean
	 * @param query the query
	 * @param ds the ds
	 */
	public static void setMSPISPBean(MSPISPUpTimeBean mspispUpTimeBean, String query,  DataSource ds) {
		mspispUpTimeBean.setTableData(MSPISPUpTimeDao.getTableDetail(query, ds));
	}
	
	/**
	 * Sets the form from mspisp up time.
	 * 
	 * @param mspispUpTimeForm the mspisp up time form
	 * @param mspispUpTimeBean the mspisp up time bean
	 */
	public static void setFormFromMSPISPUpTime(MSPISPUpTimeForm mspispUpTimeForm, MSPISPUpTimeBean mspispUpTimeBean) {
		MSPISPUpTimeConvertor.setFormFromMSPISPUpTime(mspispUpTimeForm, mspispUpTimeBean);
	}
	

}
