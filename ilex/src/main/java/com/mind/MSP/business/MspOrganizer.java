package com.mind.MSP.business;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.mind.MSP.dao.Mspdao;
import com.mind.MSP.formbean.MspBouncesForm;
import com.mind.MSP.formbean.MspCustomerReportedEventForm;
import com.mind.MSP.formbean.MspEventForm;
import com.mind.MSP.formbean.MspIncidentForm;
import com.mind.MSP.formbean.MspMonitoringSummaryForm;
import com.mind.MSP.formbean.RealTimeStateForm;
import com.mind.bean.msp.Bounces;
import com.mind.bean.msp.MonitoringSummary;
import com.mind.bean.msp.MspNetmedxTicket;
import com.mind.bean.msp.RealTimeState;
import com.mind.bean.msp.RealTimeStateInfo;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.TicketTab;
import com.mind.common.LabelValue;
import com.mind.common.Util;
import com.mind.dao.PM.Jobdao;
import com.mind.msp.common.EscalationLevel;
import com.mind.msp.common.MspNetMedxTicketType;
import com.mind.newjobdb.dao.TicketDAO;
import com.mind.newjobdb.dao.TicketNatureDAO;
import com.mind.newjobdb.dao.TicketRequestInfoDAO;

/**
 * The Class MSPOrganizer. Discription: Delegate layer for MSP(Monitoring
 * Services Page).
 */
public class MspOrganizer {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(MspOrganizer.class);

    /**
     * Gets the real time state info form database.
     *
     * @param DataSource the ds
     * @return RealTimeStateBean the real time state
     */
    public static void getRealTimeStatic(RealTimeStateBean RealTimeState,
            DataSource ds) {
        RealTimeState.setRealTimeState(Mspdao.managedInitialAssessment(
                EscalationLevel.INITIAL_ASSESSMENT, ds));
        RealTimeState.setRealTimeStateInfo(getRealTimeStateInfo(ds));
    }

    public static List<RealTimeState> managedInitialAssessmentOnlyUpStatus(
            DataSource ds) {
        List<RealTimeState> realTimeStateList = Mspdao
                .managedInitialAssessment(EscalationLevel.INITIAL_ASSESSMENT,
                        ds);
        List<RealTimeState> realTimeStateListUpStatus = new ArrayList<RealTimeState>();
        for (RealTimeState realTimeState : realTimeStateList) {
            if (realTimeState.getCurrentStatus().equalsIgnoreCase("up")) {
                realTimeStateListUpStatus.add(realTimeState);
            }
        }

        return realTimeStateListUpStatus;

    }

    public static List<RealTimeState> unManagedInitialAssessmentOnlyUpStatus(
            DataSource ds) {
        List<RealTimeState> realTimeStateList = Mspdao
                .unManagedInitialAssessment(EscalationLevel.INITIAL_ASSESSMENT,
                        ds);
        List<RealTimeState> realTimeStateListUpStatus = new ArrayList<RealTimeState>();
        for (RealTimeState realTimeState : realTimeStateList) {
            if (realTimeState.getCurrentStatus().equalsIgnoreCase("up")) {
                realTimeStateListUpStatus.add(realTimeState);
            }
        }

        return realTimeStateListUpStatus;

    }

    /**
     * Gets the real time state info form database.
     *
     * @param DataSource the ds
     * @return RealTimeStateBean the real time state
     */
    public static void unManagedInitialAssessment(
            RealTimeStateBean RealTimeState, DataSource ds) {

        RealTimeState.setUnManagedIniAssessment(Mspdao
                .unManagedInitialAssessment(EscalationLevel.INITIAL_ASSESSMENT,
                        ds));
        RealTimeState.setUnManagedRealTimeStateInfo(getRealTimeStateInfo(ds));

    }

    /**
     * Call convertor to fill RealTimeStateForm.
     *
     * @param ActionForm the form
     * @param RealTimeStateBean the bean
     */
    public static void setFormFromRealTimeStateBean(RealTimeStateForm form,
            RealTimeStateBean bean) {
        MspConvertor.setFormFromRealTimeStateBean(form, bean);
    }

    /**
     * Call convertor to fill UnManaged Initial Assessment.
     *
     * @param ActionForm the form
     * @param RealTimeStateBean the bean
     */
    public static void setFormFromUnManagedInitAssessment(
            RealTimeStateForm form, RealTimeStateBean bean) {
        MspConvertor.setFormFromUnManagedInitAssessment(form, bean);
    }

    /**
     * Gets the real time state info from database.
     *
     * @param DataSource the ds
     * @return the real time state info
     */
    public static RealTimeStateInfo getRealTimeStateInfo(DataSource ds) {
        return Mspdao.getRealTimeStateInfo(ds);
    }

    /**
     * To set the No Action. Call database.
     *
     * @param nActiveMonitorStateChangeID the Active Monitor State Change ID
     * @param DataSource the ds
     */
    public static int setNoAction(String nActiveMonitorStateChangeID,
            String wugInstance, DataSource ds, String userId) {
        int noActionFlag = Mspdao.setNoAction(nActiveMonitorStateChangeID,
                wugInstance, ds, userId);
        if (noActionFlag > -1) {
            noActionFlag = 1;
        }
        return noActionFlag;
    }

    /**
     * Gets the monitoring summary from database.
     *
     * @param DataSource the ds
     * @return MonitoringSummaryBean the monitoring summary
     */
    public static ArrayList<MonitoringSummary> getMonitoringSummary(
            String fromPage, DataSource ds) {
        return Mspdao.getMonitoringSummary(fromPage, ds);
    }

    /**
     * Call convertor to fill MspMonitoringSummaryForm.
     *
     * @param MspMonitoringSummaryForm the form
     * @param MonitoringSummaryBean the bean
     */
    public static void setFormFromMonitoringSummaryBean(
            MspMonitoringSummaryForm form, MonitoringSummaryBean bean) {
        MspConvertor.setFormFromMonitoringSummaryBean(form, bean);
    }

    /**
     * Fill all default value for create a default ticket. Create a ticket.
     *
     * @param nActiveMonitorStateChangeID the n active monitor state change id
     * @param ds the ds
     */
    public static String[] createTicket(String loginUserId,
            String nActiveMonitorStateChangeID, String wugInstance,
            DataSource ds) {

        /*
         * Get Site, Msa, Appendix Id String[0] the Site Id String[1] the Msa Id
         * String[2] the Appendix Id
         */
        String[] siteData = Mspdao.getSiteData(nActiveMonitorStateChangeID,
                wugInstance, ds);
        String[] createdIds = new String[3];
        if (logger.isDebugEnabled()) {
            logger.debug("createTicket(String, String, DataSource) "
                    + "\n - nActiveMonitorStateChangeID="
                    + nActiveMonitorStateChangeID
                    + "\n - SiteId=" + siteData[0] + //$NON-NLS-1$
                    "\n - MsaId=" + siteData[1] + //$NON-NLS-1$
                    "\n - AppendixId=" + siteData[2]); //$NON-NLS-1$
        }
        /* When site id or appendix id is not present */
        if (Util.isNullOrBlank(siteData[0]) || Util.isNullOrBlank(siteData[2])) {
            if (logger.isDebugEnabled()) {
                logger.debug("createTicket(String, String, DataSource) - Site or Appendix Id is not present."); //$NON-NLS-1$
            }
            return createdIds;
        }

        /* Fill New Ticket required data */
        TicketTab ticketTab;
        ArrayList<LabelValue> resourceList = TicketRequestInfoDAO
                .getResourceList(1, new Long(siteData[2]), "1", "Other", ds);
        ArrayList<LabelValue> problemCategoriesList = TicketNatureDAO
                .getProblemCategories(ds);
        ArrayList<LabelValue> requestTypesList = TicketRequestInfoDAO
                .getRequestTypes(ds);

        ticketTab = TicketDAO.createNewTicket(
                new Long(siteData[2]).longValue(), ds);
        ticketTab.getRequestInfo().setRequestor(EscalationLevel.REQUESTOR);
        ticketTab.getRequestInfo().setRequestorEmail(
                EscalationLevel.REQUESTOR_EMAIL);
        ticketTab.getRequestInfo().setSite(siteData[0]);
        ticketTab.getScheduleInfo().setRequestedTimestamp(
                IlexTimestamp.getSystemDateTime());
        ticketTab.getRequestInfo().setStandBy(EscalationLevel.STANDBY);
        ticketTab.getRequestInfo().setMSP(EscalationLevel.MSP);
        ticketTab.getRequestInfo().setHelpDesk(EscalationLevel.HELPDESK);
        ticketTab.getRequestNature().setProblemCategory(
                Util.getListValue(problemCategoriesList,
                        EscalationLevel.PROBLEM_CATEGORY, "label"));
        ticketTab.getRequestInfo().setCustomerReference(
                EscalationLevel.MSP_AGREEMENT);
        ticketTab.getRequestNature().setProblemDescription(
                EscalationLevel.PROBLEM_DESCRIPTION);
        ticketTab.getRequestNature().setSpecialInstructions(
                EscalationLevel.SPECIAL_INSTRUCTIONS);
        ticketTab.getRequestInfo().setCriticality(EscalationLevel.CRITICALITY);
        ticketTab.getRequestInfo().setResource(
                Util.getListValue(resourceList, EscalationLevel.RESOURCE,
                        "label"));
        ticketTab.getScheduleInfo().setPreferredTimestamp(
                IlexTimestamp.getSystemDateTime());
        ticketTab.getScheduleInfo().setWindowFromTimestamp(
                IlexTimestamp.converToTimestamp(null));
        ticketTab.getScheduleInfo().setWindowToTimestamp(
                IlexTimestamp.converToTimestamp(null));
        ticketTab.setAppendixId(new Long(siteData[2]));
        ticketTab.getRequestInfo().setRequestType(
                Util.getListValue(requestTypesList,
                        EscalationLevel.REQUEST_TYPE, "label"));
        ticketTab
                .setMonitoringSourceCd(MspNetMedxTicketType.ESCALATE_MONITORING_SOURCE
                        .getInt());
        /* Call database to create a ticket. */
        Jobdao.addLogEntry(ds, "MSP Organizer!", "");
        createdIds = TicketDAO.updateTicket(ticketTab,
                new Long(loginUserId).longValue(), ds);
        return createdIds;
    }

    /**
     * Sets the to help desk. When ticket is created then the record identified
     * by wug_ip_nActiveMonitor StateChangeID is switch from �Initial
     * Assessment� state to �Help Desk� by updating the wug_ip_escalation_level
     * to one(1). The ticket number is also added this table in the
     * wug_ip_ticket_reference field.
     *
     * @param String the n active monitor state change id
     * @param ticketNumber the ticket number
     * @param DataSource the ds
     */
    public static void setToHelpDesk(String nActiveMonitorStateChangeID,
            String wugInstance, String ticketNumber, DataSource ds) {
        Mspdao.setToHelpDesk(nActiveMonitorStateChangeID, wugInstance,
                ticketNumber, EscalationLevel.HELP_DESK, ds);
    }

    /**
     * Gets created ticket number.
     *
     * @param String the ticket id
     * @param DataSource the ds
     * @return the ticket number
     */
    public static String getTicketNumber(String ticketId, DataSource ds) {
        return Mspdao.getTicketNumber(ticketId, ds);
    }

    /**
     * Sets the incident.
     *
     * @param IncidentBean the incident bean
     * @param DataSource the ds
     */
    public static void setIncident(IncidentBean incidentBean, DataSource ds) {
        incidentBean.setIncidentList(Mspdao.getRealTimeState(
                EscalationLevel.HELP_DESK, ds));
    }

    /**
     * Call convertor to fill MspIncidentForm.
     *
     * @param mspIncidentForm the msp incident form
     * @param incidentBean the incident bean
     */
    public static void setFormFromIncidentBean(MspIncidentForm mspIncidentForm,
            IncidentBean incidentBean) {
        MspConvertor.setFormFromIncidentBean(mspIncidentForm, incidentBean);
    }

    /**
     * Sets the msp dispatch.
     *
     * @param mspEventBean the event bean
     * @param ds the ds
     */
    public static void setMspDispatch(MspEventBean mspEventBean, DataSource ds) {
        mspEventBean.setIncidentList(Mspdao.getRealTimeState(
                EscalationLevel.MSP_DISPATCH, ds));
    }

    /**
     * Sets the MspEventForm from mspDispatchBean.
     *
     * @param mspEventForm the msp event form
     * @param mspDispatchBean the msp dispatch bean
     */
    public static void setFormFromMspDispatchBean(MspEventForm mspEventForm,
            MspEventBean mspDispatchBean) {
        MspConvertor.setFormFromMspEventBean(mspEventForm, mspDispatchBean);
    }

    /**
     * Gets the escalation level. To check page section should be reload or not.
     * For Incident section.
     *
     * @param DataSource the ds
     * @param int the Escalation Level
     * @return int the escalation level
     */
    public static int getEscalationLevel(int escalationLevel, DataSource ds) {
        return Mspdao.getEscalationLevel(escalationLevel, ds);
    }

    /**
     * Gets the bounces List.
     *
     * @param DataSource the ds
     */
    public static ArrayList<Bounces> getBouncesList(DataSource ds) {
        return Mspdao.getBouncesList(ds, EscalationLevel.Site_NAME_LENGTH);
    }

    /**
     * Call convertor to fill MspBouncesForm..
     *
     * @param MspBouncesForm the form
     * @param BouncesBean the bean
     */
    public static void setFormFromBouncesBean(MspBouncesForm form,
            BouncesBean bean) {
        MspConvertor.setFormFromBouncesBean(form, bean);
    }

    /**
     * Purpose: Sets the MSP-NetMediX ticket table data.
     *
     * @param MspCustomerReportedEventBean
     * @param DataSource
     */
    public static ArrayList<MspNetmedxTicket> setMspNetmedixTicket(
            MspCustomerReportedEventBean bean, DataSource ds) {
        return Mspdao.findCustomerReportedEventList(
                MspNetMedxTicketType.MSP_NETMEDIX_MONITORING_SOURCE.getInt(),
                ds);
    }

    /**
     * Purpose: Copy form bean from msp customer reported event bean.
     *
     * @param MspCustomerReportedEventForm
     * @param MspCustomerReportedEventBean
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public static void copyFormFromMspCustomerReportedEventBean(
            MspCustomerReportedEventForm dest, MspCustomerReportedEventBean orig)
            throws IllegalAccessException, InvocationTargetException {
        BeanUtils.copyProperties(dest, orig);
    }
}
