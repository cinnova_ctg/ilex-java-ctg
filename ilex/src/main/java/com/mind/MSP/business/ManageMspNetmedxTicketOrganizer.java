package com.mind.MSP.business;

import java.lang.reflect.InvocationTargetException;

import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.mind.MSP.dao.MspNetmedxTicketDAO;
import com.mind.MSP.formbean.ManageMspNetmedxTicketForm;
import com.mind.bean.msp.TicketInformation;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.dao.TicketNatureDAO;

/**
 * The Class ManageMspNetmedxTicketOrganizer.
 * 
 * @author vijay kumar singh
 */
public class ManageMspNetmedxTicketOrganizer {

	private static final Logger logger = Logger
			.getLogger(ManageMspNetmedxTicketOrganizer.class);

	/**
	 * Purpose: Sets the default value of MSA-NetMedX Ticket.
	 * 
	 * @param ManageMspNetmedxTicketBean
	 * @param DataSource
	 */
	public static void setDefaultValue(String ticketId, String jobId,
			ManageMspNetmedxTicketBean bean, DataSource ds) {
		if (logger.isDebugEnabled()) {
			logger.debug("ticketId->" + ticketId + ", jobId->" + jobId);
		}
		bean.setDeviceInfo(MspNetmedxTicketDAO.getDeviceInformation(Integer
				.valueOf(ticketId), ds));
		bean.setSiteInfo(JobDAO.getSiteInfo(jobId, ds));
		bean.setProblemCategoryList(TicketNatureDAO.getProblemCategories(ds));
		bean.setTicketInformation(MspNetmedxTicketDAO.getTicketInformation(
				Integer.valueOf(ticketId), ds));
	}

	/**
	 * Purpose: Gets the ticket info.
	 * 
	 * @param ticketId
	 *            the ticket id
	 * @param ds
	 *            the ds
	 * 
	 * @return the ticket info
	 */
	public static TicketInformation getTicketInfo(String ticketId, DataSource ds) {
		return MspNetmedxTicketDAO.getTicketInformation(Integer
				.valueOf(ticketId), ds);
	}

	/**
	 * Purpose: update the estimated effort.
	 * 
	 * @param ManageMspNetmedxTicketBean
	 *            the bean
	 * @param String
	 *            the user id
	 * @param DataSource
	 *            the ds
	 * 
	 * @return int - updated flag
	 */
	public static int setEstimatedEffort(String jobId, String effort,
			String userId, DataSource ds) {
		int updatedFlag = MspNetmedxTicketDAO.setEstimatedEffort(Integer
				.valueOf(jobId), Float.valueOf(effort),
				Integer.valueOf(userId), ds);
		return updatedFlag;
	}

	/**
	 * Purpose: Copy form from bean logic bean.
	 * 
	 * @param ManageMspNetmedxTicketForm
	 *            - Form bean
	 * @param ManageMspNetmedxTicketBean
	 *            - Logic bean
	 * 
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 */
	public static void copyFormFromBean(ManageMspNetmedxTicketForm dest,
			ManageMspNetmedxTicketBean orig) throws IllegalAccessException,
			InvocationTargetException {
		BeanUtils.copyProperties(dest, orig);
		if (orig.getDeviceInfo() != null)
			BeanUtils.copyProperties(dest, orig.getDeviceInfo());
		if (orig.getSiteInfo() != null)
			BeanUtils.copyProperties(dest, orig.getSiteInfo());
		if (orig.getTicketInformation() != null)
			BeanUtils.copyProperties(dest, orig.getTicketInformation());
	}

	/**
	 * Purpose: copy logic bean from form bean data.
	 * 
	 * @param ManageMspNetmedxTicketBean
	 *            - Logic bean
	 * @param ManageMspNetmedxTicketForm
	 *            - Form bean
	 * 
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 */
	public static void copyBeanFromForm(ManageMspNetmedxTicketBean dest,
			ManageMspNetmedxTicketForm orig) throws IllegalAccessException,
			InvocationTargetException {
		BeanUtils.copyProperties(dest, orig);
		if (dest.getTicketInformation() != null)
			BeanUtils.copyProperties(dest.getTicketInformation(), orig);
	}

}
