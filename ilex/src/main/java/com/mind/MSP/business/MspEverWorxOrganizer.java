package com.mind.MSP.business;

import java.util.ArrayList;
import java.util.TreeMap;

import javax.sql.DataSource;

import com.mind.MSP.dao.MspEverWorxDao;
import com.mind.MSP.formbean.MspEverWorxForm;

public class MspEverWorxOrganizer {
	
	public static ArrayList<TreeMap<String, String>> getMarkers(String status, DataSource ds){
		return MspEverWorxDao.getEverWorxList(status, ds);
	}
	
	public static void setFormFromEverWorxBean(MspEverWorxForm form, MspEverWorxBean bean){
		MspEverWorxConverter.setFormFromEverWorxBean(form, bean);
	}
}
