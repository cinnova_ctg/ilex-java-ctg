package com.mind.MSP.business;

import com.mind.MSP.formbean.MSPISPUpTimeForm;

/**
 * The Class MSPISPUpTimeConvertor.
 */
public class MSPISPUpTimeConvertor {
	
	/**
	 * Sets the form from mspisp up time.
	 * 
	 * @param mspispUpTimeForm the mspisp up time form
	 * @param mspispUpTimeBean the mspisp up time bean
	 */
	public static void setFormFromMSPISPUpTime(MSPISPUpTimeForm mspispUpTimeForm, MSPISPUpTimeBean mspispUpTimeBean) {
		mspispUpTimeForm.setTableData(mspispUpTimeBean.getTableData());
	}

}
