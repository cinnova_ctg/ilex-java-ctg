package com.mind.MSP.business;

import com.mind.MSP.formbean.MspBouncesForm;
import com.mind.MSP.formbean.MspEventForm;
import com.mind.MSP.formbean.MspIncidentForm;
import com.mind.MSP.formbean.MspMonitoringSummaryForm;
import com.mind.MSP.formbean.RealTimeStateForm;
import com.mind.msp.common.EscalationLevel;

/**
 * The Class MspConvertor.
 */
public class MspConvertor {

	/**
	 * Convert the RealTimeStateForm from RealTimeStateBean.
	 * 
	 * @param RealTimeStateForm
	 *            the form
	 * @param RealTimeStateBean
	 *            the bean
	 */

	public static void setFormFromRealTimeStateBean(RealTimeStateForm form,
			RealTimeStateBean bean) {
		form.setRealTimeState(bean.getRealTimeState());
		form.setEventCount(String.valueOf(bean.getRealTimeStateInfo()
				.getEventCount()));
		form.setPageFlag(bean.getRealTimeStateInfo().getPageFlag());
		form.setRunTimeAlert(String.valueOf(bean.getRealTimeStateInfo()
				.getRunTimeAlert()));
		form.setRunTimeDisplay(bean.getRealTimeStateInfo().getRunTimeDisplay());
	}

	/**
	 * Convert the RealTimeStateForm from RealTimeStateBean for Unmanaged
	 * Content
	 * 
	 * @param RealTimeStateForm
	 *            the form
	 * @param RealTimeStateBean
	 *            the bean
	 */
	public static void setFormFromUnManagedInitAssessment(
			RealTimeStateForm form, RealTimeStateBean bean) {
		form.setUnManagedIniAssessment(bean.getUnManagedIniAssessment());
		form.setEventCount(String.valueOf(bean.getRealTimeStateInfo()
				.getEventCount()));
		form.setPageFlag(bean.getRealTimeStateInfo().getPageFlag());
		form.setRunTimeAlert(String.valueOf(bean.getRealTimeStateInfo()
				.getRunTimeAlert()));
		form.setRunTimeDisplay(bean.getRealTimeStateInfo().getRunTimeDisplay());

	}

	/**
	 * Convert the MspMonitoringSummaryForm from MonitoringSummaryBean.
	 * 
	 * @param MspMonitoringSummaryForm
	 *            the form
	 * @param MonitoringSummaryBean
	 *            the bean
	 */
	public static void setFormFromMonitoringSummaryBean(
			MspMonitoringSummaryForm form, MonitoringSummaryBean bean) {
		form.setMonitoringSummaryList(bean.getMonitoringSummaryList());
	}

	/**
	 * Convert MspIncidentForm from IncidentBean. Sets the form from real time
	 * state bean.
	 * 
	 * @param form
	 *            the form
	 * @param bean
	 *            the bean
	 */
	public static void setFormFromIncidentBean(MspIncidentForm form,
			IncidentBean bean) {
		form.setIncidentList(bean.getIncidentList());
		form.setEscalationLevel(String.valueOf(EscalationLevel.HELP_DESK));
	}

	/**
	 * Convert MspEventForm from MspEventBean.
	 * 
	 * @param MspEventForm
	 *            the form
	 * @param MspEventBean
	 *            the bean
	 */
	public static void setFormFromMspEventBean(MspEventForm form,
			MspEventBean bean) {
		form.setIncidentList(bean.getIncidentList());
		form.setEscalationLevel(String.valueOf(EscalationLevel.MSP_DISPATCH));
	}

	/**
	 * Sets the MspBouncesForm from BouncesBean.
	 * 
	 * @param MspBouncesForm
	 *            the form
	 * @param BouncesBean
	 *            the bean
	 */
	public static void setFormFromBouncesBean(MspBouncesForm form,
			BouncesBean bean) {
		form.setBouncesList(bean.getBouncesList());
		form.setListSize(bean.getBouncesList().size());
	}
}
