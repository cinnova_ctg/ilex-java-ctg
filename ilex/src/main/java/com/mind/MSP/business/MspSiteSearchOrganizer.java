package com.mind.MSP.business;

import java.lang.reflect.InvocationTargetException;

import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;

import com.mind.MSP.dao.MspUtilityDao;
import com.mind.MSP.dao.MspSiteSearchDao;
import com.mind.MSP.formbean.MspSiteSearchForm;
import com.mind.MSP.formbean.SiteDetailForm;
import com.mind.bean.msp.DeviceInfoBean;
import com.mind.bean.msp.MspSiteSearchBean;
import com.mind.bean.msp.SiteDetailBean;
import com.mind.bean.msp.SiteInfo;
import com.mind.msp.common.EscalationLevel;

// TODO: Auto-generated Javadoc
/**
 * The Class MspSiteSearchOrganizer.
 */
public class MspSiteSearchOrganizer {


	/**
	 * This method gets the customer list and Site Search list.
	 * 
	 * @param bean the bean
	 * @param ds the ds
	 */
	public static void setSiteSearch(MspSiteSearchBean bean,
			DataSource ds) {
		bean.setCustomerList(MspUtilityDao.getOptionList(
				EscalationLevel.ILEX_CUSTOMER_NAME_FOR_CREDITS_RPT, ds));
		bean.setSearchList(MspSiteSearchDao.getSiteSearchList(
				bean, ds));
	}

	/**
	 * This method gets customer list.
	 * 
	 * @param bean the bean
	 * @param ds the ds
	 */
	public static void setCustomerList(MspSiteSearchBean bean,
			DataSource ds)
	{
		bean.setCustomerList(MspUtilityDao.getOptionList(
				EscalationLevel.ILEX_CUSTOMER_NAME_FOR_CREDITS_RPT, ds));
	}
	
	
	/**
	 * this method sets device information and site information
	 * into the SiteDetailBean.
	 * 
	 * @param bean the bean
	 * @param ds the ds
	 */
	public static void setSiteDetail(SiteDetailBean bean,DataSource ds)		
	{
		bean.setDeviceInfoBean(MspSiteSearchDao.getDeviceInfo(
				bean, ds));
		bean.setSiteInfo(MspSiteSearchDao.getSiteInfo(
				bean, ds));
	}
	
	
	/**
	 * Sets the form from site search bean.
	 * 
	 * @param form the form
	 * @param bean the bean
	 * 
	 * @throws IllegalAccessException the illegal access exception
	 * @throws InvocationTargetException the invocation target exception
	 */
	public static void setFormFromSiteSearchBean(MspSiteSearchForm form,
			MspSiteSearchBean bean) throws IllegalAccessException, InvocationTargetException {
		BeanUtils.copyProperties(form, bean);
	}


	/**
	 * Sets the site search bean from form.
	 * 
	 * @param form the form
	 * @param bean the bean
	 * 
	 * @throws IllegalAccessException the illegal access exception
	 * @throws InvocationTargetException the invocation target exception
	 */
	public static void setSiteSearchBeanFromForm(MspSiteSearchForm form,
			MspSiteSearchBean bean) throws IllegalAccessException, InvocationTargetException {
		BeanUtils.copyProperties(bean, form);
		
	}
	

	/**
	 * this method sets Device Information from SiteDetailBean to SiteDetailForm
	 * Sets the device information.
	 * 
	 * @param form the form
	 * @param deviceInfo the device info
	 */
	private static void setDeviceInformation(SiteDetailForm form,
			DeviceInfoBean deviceInfo) {
		form.setCircuitIP(deviceInfo.getCircuitIP());
		form.setCircuitType(deviceInfo.getCircuitType());
		form.setInternetServiceLineNo(deviceInfo.getInternetServiceLineNo());
		form.setIsp(deviceInfo.getIsp());
		form.setIspSupportNo(deviceInfo.getIspSupportNo());
		form.setLocation(deviceInfo.getLocation());
		form.setModemType(deviceInfo.getModemType());
		form.setSiteContactNo(deviceInfo.getSiteContactNo());
		form.setAccountNumber(deviceInfo.getAccount());
	}

	
	/**
	 * This method sets Site Information from bean to form 
	 * 
	 * @param form the form
	 * @param siteInfo the site info
	 */
	private static void setSiteInfo(SiteDetailForm form, SiteInfo siteInfo) {
		form.setSiteAddress(siteInfo.getSite_address());
		form.setSiteCity(siteInfo.getSite_city());
		form.setSiteNumber(siteInfo.getSite_number());
		form.setSiteState(siteInfo.getSite_state());
		form.setSiteZip(siteInfo.getSite_zip());
		form.setSiteTimeZone(siteInfo.getSite_time_zone());

	}
	
	/**
	 * this method calls other methods for setting the bean value to form 
	 * 
	 * @param form the form
	 * @param bean the bean
	 */
	public static void setFormFromSiteDetailBean(SiteDetailForm form,
			SiteDetailBean bean) {
		setDeviceInformation(form, bean.getDeviceInfoBean());
		setSiteInfo(form, bean.getSiteInfo());
	}
	
	
	
}
