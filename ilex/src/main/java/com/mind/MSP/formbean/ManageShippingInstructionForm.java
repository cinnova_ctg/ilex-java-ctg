package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class ManageShippingInstructionForm extends ActionForm {
	//
	private static final long serialVersionUID = 1L;
	private String appendixId = null;
	private String jobId = null;
	private String reqDeliveryDate = null;
	private String circuitType = null;
	private String potsType = null;
	private String siteNumber = "";
	private String siteCity = "";
	private String siteState = "";
	private String siteZip = "";
	private String siteAddress = "";
	private String sitePrimaryPoc = "";
	private ArrayList<String> resourceArr;
	private String msaName = "";
	private String modemLocation = null;

	public String getModemLocation() {
		return modemLocation;
	}

	public void setModemLocation(String modemLocation) {
		this.modemLocation = modemLocation;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getReqDeliveryDate() {
		return reqDeliveryDate;
	}

	public void setReqDeliveryDate(String reqDeliveryDate) {
		this.reqDeliveryDate = reqDeliveryDate;
	}

	public String getCircuitType() {
		return circuitType;
	}

	public void setCircuitType(String circuitType) {
		this.circuitType = circuitType;
	}

	public String getPotsType() {
		return potsType;
	}

	public void setPotsType(String potsType) {
		this.potsType = potsType;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getSiteZip() {
		return siteZip;
	}

	public void setSiteZip(String siteZip) {
		this.siteZip = siteZip;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSitePrimaryPoc() {
		return sitePrimaryPoc;
	}

	public void setSitePrimaryPoc(String sitePrimaryPoc) {
		this.sitePrimaryPoc = sitePrimaryPoc;
	}

	public ArrayList<String> getResourceArr() {
		return resourceArr;
	}

	public void setResourceArr(ArrayList<String> resourceArr) {
		this.resourceArr = resourceArr;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	public String getMsaName() {
		return msaName;
	}

}
