package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;

import com.mind.common.LabelValue;

public class ManageOutageForm extends ActionForm {

	//
	private static final long serialVersionUID = 1L;

	private String nActiveMonitorStateChangeID = null;

	/* Site/Device Information */
	private String site = null;
	private String escalationLevel = null;

	/** The escalation lebel. */
	private String escalationLabel = null;
	private String circuitType = null;
	private String modemType = null;
	private String internetServiceLineNo = null;
	private String isp = null;
	private String ispSupportNo = null;
	private String circuitIP = null;
	private String siteContactNo = null;
	private String location = null;
	private String siteNumber = null;
	private String siteAddress = null;
	private String siteCity = null;
	private String siteState = null;
	private String siteZip = null;
	private String siteTimeZone = null;
	private String wugInstance = null;
	private Integer installNotesCount;
	private String nextActionMsg = null;

	public Integer getInstallNotesCount() {
		return installNotesCount;
	}

	public void setInstallNotesCount(Integer installNotesCount) {
		this.installNotesCount = installNotesCount;
	}

	public String getWugInstance() {
		return wugInstance;
	}

	public void setWugInstance(String wugInstance) {
		this.wugInstance = wugInstance;
	}

	private String appendixId = null;
	private String jobId = null;
	private String ticketId = null;
	private String ticketNumber = null;
	private String problemCategory = null;
	private String nextAction = null;
	private int nextActionId = 0;
	private String probleDescription = null;
	private String installationNotes = null;
	private boolean isInstallNotesPresent = false;
	private String latestInstallNotesDate = null;
	private String latestInstallNotes = null;
	private String secondLatestInstallNotesDate = null;
	private String secondLatestInstallNotes = null;
	private String addNotes = null;
	private String estimatedEffort = null;
	private String helpDesk = null;
	private String mspDispatch = null;
	private String updateTicket = null;

	private String issueOwner = null;
	private String resolution = null;
	private String rootCause = null;

	private String updateResolutionSection = null;
	private String currentStatus = null;
	private String lastAction = null;

	// Added For SPR #114 Monitoring Section Changes
	private String backupCircuitStatus = null;
	private String VOIP = null;
	private String accountNew = null;
	private String accessCode = null;
	private String helpDeskNew = null;
	private String fax = null;

	public String getHelpDeskNew() {
		return helpDeskNew;
	}

	public void setHelpDeskNew(String helpDeskNew) {
		this.helpDeskNew = helpDeskNew;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	private ArrayList<LabelValue> problemCategoryList = new ArrayList<LabelValue>();
	private ArrayList<LabelValue> nextActionList = new ArrayList<LabelValue>();
	private ArrayList<LabelValue> issueOwnerList = new ArrayList<LabelValue>();
	private ArrayList<LabelValue> resolutionList = new ArrayList<LabelValue>();
	private ArrayList<LabelValue> rootCauseList = new ArrayList<LabelValue>();

	private boolean disableButton = false;

	public boolean isInstallNotesPresent() {
		return isInstallNotesPresent;
	}

	public void setInstallNotesPresent(boolean isInstallNotesPresent) {
		this.isInstallNotesPresent = isInstallNotesPresent;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	public String getAddNotes() {
		return addNotes;
	}

	public void setAddNotes(String addNotes) {
		this.addNotes = addNotes;
	}

	public String getCircuitIP() {
		return circuitIP;
	}

	public void setCircuitIP(String circuitIP) {
		this.circuitIP = circuitIP;
	}

	public String getCircuitType() {
		return circuitType;
	}

	public void setCircuitType(String circuitType) {
		this.circuitType = circuitType;
	}

	public String getEscalationLevel() {
		return escalationLevel;
	}

	public void setEscalationLevel(String escalationLevel) {
		this.escalationLevel = escalationLevel;
	}

	public String getEstimatedEffort() {
		return estimatedEffort;
	}

	public void setEstimatedEffort(String estimatedEffort) {
		this.estimatedEffort = estimatedEffort;
	}

	public String getHelpDesk() {
		return helpDesk;
	}

	public void setHelpDesk(String helpDesk) {
		this.helpDesk = helpDesk;
	}

	public String getInternetServiceLineNo() {
		return internetServiceLineNo;
	}

	public void setInternetServiceLineNo(String internetServiceLineNo) {
		this.internetServiceLineNo = internetServiceLineNo;
	}

	public String getIsp() {
		return isp;
	}

	public void setIsp(String isp) {
		this.isp = isp;
	}

	public String getIspSupportNo() {
		return ispSupportNo;
	}

	public void setIspSupportNo(String ispSupportNo) {
		this.ispSupportNo = ispSupportNo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getModemType() {
		return modemType;
	}

	public void setModemType(String modemType) {
		this.modemType = modemType;
	}

	public String getMspDispatch() {
		return mspDispatch;
	}

	public void setMspDispatch(String mspDispatch) {
		this.mspDispatch = mspDispatch;
	}

	public String getNActiveMonitorStateChangeID() {
		return nActiveMonitorStateChangeID;
	}

	public void setNActiveMonitorStateChangeID(String activeMonitorStateChangeID) {
		nActiveMonitorStateChangeID = activeMonitorStateChangeID;
	}

	public String getProbleDescription() {
		return probleDescription;
	}

	public void setProbleDescription(String probleDescription) {
		this.probleDescription = probleDescription;
	}

	public String getProblemCategory() {
		return problemCategory;
	}

	public void setProblemCategory(String problemCategory) {
		this.problemCategory = problemCategory;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteContactNo() {
		return siteContactNo;
	}

	public void setSiteContactNo(String siteContactNo) {
		this.siteContactNo = siteContactNo;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getSiteTimeZone() {
		return siteTimeZone;
	}

	public void setSiteTimeZone(String siteTimeZone) {
		this.siteTimeZone = siteTimeZone;
	}

	public String getSiteZip() {
		return siteZip;
	}

	public void setSiteZip(String siteZip) {
		this.siteZip = siteZip;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getUpdateTicket() {
		return updateTicket;
	}

	public void setUpdateTicket(String updateTicket) {
		this.updateTicket = updateTicket;
	}

	public ArrayList<LabelValue> getProblemCategoryList() {
		return problemCategoryList;
	}

	public void setProblemCategoryList(ArrayList<LabelValue> problemCategoryList) {
		this.problemCategoryList = problemCategoryList;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getIssueOwner() {
		return issueOwner;
	}

	public void setIssueOwner(String issueOwner) {
		this.issueOwner = issueOwner;
	}

	public ArrayList<LabelValue> getIssueOwnerList() {
		return issueOwnerList;
	}

	public void setIssueOwnerList(ArrayList<LabelValue> issueOwnerList) {
		this.issueOwnerList = issueOwnerList;
	}

	public String getRootCause() {
		return rootCause;
	}

	public void setRootCause(String rootCause) {
		this.rootCause = rootCause;
	}

	public ArrayList<LabelValue> getRootCauseList() {
		return rootCauseList;
	}

	public void setRootCauseList(ArrayList<LabelValue> rootCauseList) {
		this.rootCauseList = rootCauseList;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public ArrayList<LabelValue> getResolutionList() {
		return resolutionList;
	}

	public void setResolutionList(ArrayList<LabelValue> resolutionList) {
		this.resolutionList = resolutionList;
	}

	public String getLatestInstallNotes() {
		return latestInstallNotes;
	}

	public void setLatestInstallNotes(String latestInstallNotes) {
		this.latestInstallNotes = latestInstallNotes;
	}

	public String getLatestInstallNotesDate() {
		return latestInstallNotesDate;
	}

	public void setLatestInstallNotesDate(String latestInstallNotesDate) {
		this.latestInstallNotesDate = latestInstallNotesDate;
	}

	public String getSecondLatestInstallNotes() {
		return secondLatestInstallNotes;
	}

	public void setSecondLatestInstallNotes(String secondLatestInstallNotes) {
		this.secondLatestInstallNotes = secondLatestInstallNotes;
	}

	public String getSecondLatestInstallNotesDate() {
		return secondLatestInstallNotesDate;
	}

	public void setSecondLatestInstallNotesDate(
			String secondLatestInstallNotesDate) {
		this.secondLatestInstallNotesDate = secondLatestInstallNotesDate;
	}

	public String getInstallationNotes() {
		return installationNotes;
	}

	public void setInstallationNotes(String installationNotes) {
		this.installationNotes = installationNotes;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getEscalationLabel() {
		return escalationLabel;
	}

	public void setEscalationLabel(String escalationLabel) {
		this.escalationLabel = escalationLabel;
	}

	public String getUpdateResolutionSection() {
		return updateResolutionSection;
	}

	public void setUpdateResolutionSection(String updateResolutionSection) {
		this.updateResolutionSection = updateResolutionSection;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public boolean isDisableButton() {
		return disableButton;
	}

	public void setDisableButton(boolean disableButton) {
		this.disableButton = disableButton;
	}

	public String getLastAction() {
		return lastAction;
	}

	public void setLastAction(String lastAction) {
		this.lastAction = lastAction;
	}

	public String getBackupCircuitStatus() {
		return backupCircuitStatus;
	}

	// Added For SPR #114 Monitoring Section Changes
	public void setBackupCircuitStatus(String backupCircuitStatus) {
		this.backupCircuitStatus = backupCircuitStatus;
	}

	public String getVOIP() {
		return VOIP;
	}

	public void setVOIP(String vOIP) {
		VOIP = vOIP;
	}

	public String getAccountNew() {
		return accountNew;
	}

	public void setAccountNew(String accountNew) {
		this.accountNew = accountNew;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public String getNextAction() {
		return nextAction;
	}

	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}

	public ArrayList<LabelValue> getNextActionList() {
		return nextActionList;
	}

	public void setNextActionList(ArrayList<LabelValue> nextActionList) {
		this.nextActionList = nextActionList;
	}

	public int getNextActionId() {
		return nextActionId;
	}

	public void setNextActionId(int nextActionId) {
		this.nextActionId = nextActionId;
	}

	public String getNextActionMsg() {
		return nextActionMsg;
	}

	public void setNextActionMsg(String nextActionMsg) {
		this.nextActionMsg = nextActionMsg;
	}

}
