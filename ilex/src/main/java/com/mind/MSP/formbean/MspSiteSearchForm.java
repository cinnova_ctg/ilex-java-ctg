package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;

import com.mind.bean.msp.SiteInfo;
import com.mind.common.LabelValue;

public class MspSiteSearchForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String customer = null;
	private ArrayList<LabelValue> customerList = new ArrayList<LabelValue>();
	private String siteNumber = null;
	private String address = null;
	private String city = null;
	private String zipcode = null;
	private String accountNumber = null;
	private String sitePhoneNumber = null;
	private String search = null;
	private String cancel = null;
	private String state = null;
	private ArrayList<SiteInfo> searchList = new ArrayList<SiteInfo>();
	public String siteInternetLineNumber = null;
	private boolean fromHD;

	public String getSiteInternetLineNumber() {
		return siteInternetLineNumber;
	}

	public void setSiteInternetLineNumber(String siteInternetLineNumber) {
		this.siteInternetLineNumber = siteInternetLineNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	public String getCancel() {
		return cancel;
	}

	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public ArrayList<LabelValue> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(ArrayList<LabelValue> customerList) {
		this.customerList = customerList;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getSitePhoneNumber() {
		return sitePhoneNumber;
	}

	public void setSitePhoneNumber(String sitePhoneNumber) {
		this.sitePhoneNumber = sitePhoneNumber;
	}

	public ArrayList<SiteInfo> getSearchList() {
		return searchList;
	}

	public void setSearchList(ArrayList<SiteInfo> searchList) {
		this.searchList = searchList;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public boolean isFromHD() {
		return fromHD;
	}

	public void setFromHD(boolean fromHD) {
		this.fromHD = fromHD;
	}

}
