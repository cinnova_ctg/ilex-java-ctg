package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.mind.dao.PRM.CustomerInfoBean;
import com.mind.newjobdb.dao.JobWorkflowChecklist;

public class ManageCheckListDataForm extends ActionForm {
	
	private String appendixId = null;
	private String jobId = null;
	private ArrayList<CustomerInfoBean> customerInfoList = null;
	private JobWorkflowChecklist checklist = null; 
	
	private long[] jobWorkflowItemId;
	private String[] jobWorkflowItemStatus;
	private String[] jobWorkflowItemDescription;
	private String[] jobWorkflowItemSequence;
	private String[] jobWorkflowItemReferenceDate;
	private String[] jobWorkflowItemUser;
	public long[] getJobWorkflowItemId() {
		return jobWorkflowItemId;
	}
	public void setJobWorkflowItemId(long[] jobWorkflowItemId) {
		this.jobWorkflowItemId = jobWorkflowItemId;
	}
	public String[] getJobWorkflowItemStatus() {
		return jobWorkflowItemStatus;
	}
	public void setJobWorkflowItemStatus(String[] jobWorkflowItemStatus) {
		this.jobWorkflowItemStatus = jobWorkflowItemStatus;
	}
	public String[] getJobWorkflowItemDescription() {
		return jobWorkflowItemDescription;
	}
	public void setJobWorkflowItemDescription(String[] jobWorkflowItemDescription) {
		this.jobWorkflowItemDescription = jobWorkflowItemDescription;
	}
	public String[] getJobWorkflowItemSequence() {
		return jobWorkflowItemSequence;
	}
	public void setJobWorkflowItemSequence(String[] jobWorkflowItemSequence) {
		this.jobWorkflowItemSequence = jobWorkflowItemSequence;
	}
	public String[] getJobWorkflowItemReferenceDate() {
		return jobWorkflowItemReferenceDate;
	}
	public void setJobWorkflowItemReferenceDate(
			String[] jobWorkflowItemReferenceDate) {
		this.jobWorkflowItemReferenceDate = jobWorkflowItemReferenceDate;
	}
	public String[] getJobWorkflowItemUser() {
		return jobWorkflowItemUser;
	}
	public void setJobWorkflowItemUser(String[] jobWorkflowItemUser) {
		this.jobWorkflowItemUser = jobWorkflowItemUser;
	}
	public String getAllItemStatus() {
		return allItemStatus;
	}
	public void setAllItemStatus(String allItemStatus) {
		this.allItemStatus = allItemStatus;
	}
	public String getItemListSize() {
		return itemListSize;
	}
	public void setItemListSize(String itemListSize) {
		this.itemListSize = itemListSize;
	}
	private String allItemStatus;
	private String itemListSize;
	
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public ArrayList<CustomerInfoBean> getCustomerInfoList() {
		return customerInfoList;
	}
	public void setCustomerInfoList(ArrayList<CustomerInfoBean> customerInfoList) {
		this.customerInfoList = customerInfoList;
	}
	public JobWorkflowChecklist getChecklist() {
		return checklist;
	}
	public void setChecklist(JobWorkflowChecklist checklist) {
		this.checklist = checklist;
	}

}
