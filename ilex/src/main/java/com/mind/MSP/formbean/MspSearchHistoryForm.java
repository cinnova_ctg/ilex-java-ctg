package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;

import com.mind.bean.msp.TicketSearchBean;
import com.mind.common.LabelValue;

public class MspSearchHistoryForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String customer = null;
	private ArrayList<LabelValue> customerList = new ArrayList<LabelValue>();
	private ArrayList<TicketSearchBean> searchList = new ArrayList<TicketSearchBean>();
	private String site = null;
	private String from = null;
	private String to = null;
	private String search = null;
	private String cancel = null;
	private boolean fromHD;

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	public String getCancel() {
		return cancel;
	}

	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public ArrayList<LabelValue> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(ArrayList<LabelValue> customerList) {
		this.customerList = customerList;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public ArrayList<TicketSearchBean> getSearchList() {
		return searchList;
	}

	public void setSearchList(ArrayList<TicketSearchBean> searchList) {
		this.searchList = searchList;
	}

	public boolean isFromHD() {
		return fromHD;
	}

	public void setFromHD(boolean fromHD) {
		this.fromHD = fromHD;
	}

}
