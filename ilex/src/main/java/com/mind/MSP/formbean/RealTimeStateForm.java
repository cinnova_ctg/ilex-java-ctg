package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.mind.bean.msp.RealTimeState;

/**
 * The Class RealTimeStateForm.
 */
public class RealTimeStateForm extends ActionForm {

	/** The real time state. */
	private ArrayList<RealTimeState> realTimeState = new ArrayList<RealTimeState>();

	/** The unManaged Initial Assessment. */
	private ArrayList<RealTimeState> unManagedIniAssessment = new ArrayList<RealTimeState>();


	/** The event count. */
	private String eventCount = null;

	/** The run time alert. */
	private String runTimeAlert = null;

	/** The site errors. */
	private String siteErrors = null;
	private int pageFlag = 0;
	private String runTimeDisplay = null;

	public int getPageFlag() {
		return pageFlag;
	}

	public void setPageFlag(int pageFlag) {
		this.pageFlag = pageFlag;
	}

	public String getRunTimeDisplay() {
		return runTimeDisplay;
	}

	public void setRunTimeDisplay(String timeDisplay) {
		this.runTimeDisplay = timeDisplay;
	}

	/**
	 * Gets the event count.
	 * 
	 * @return the event count
	 */
	public String getEventCount() {
		return eventCount;
	}

	/**
	 * Sets the event count.
	 * 
	 * @param eventCount
	 *            the new event count
	 */
	public void setEventCount(String eventCount) {
		this.eventCount = eventCount;
	}

	/**
	 * Gets the run time alert.
	 * 
	 * @return the run time alert
	 */
	public String getRunTimeAlert() {
		return runTimeAlert;
	}

	/**
	 * Sets the run time alert.
	 * 
	 * @param runTimeAlert
	 *            the new run time alert
	 */
	public void setRunTimeAlert(String runTimeAlert) {
		this.runTimeAlert = runTimeAlert;
	}

	/**
	 * Gets the site errors.
	 * 
	 * @return the site errors
	 */
	public String getSiteErrors() {
		return siteErrors;
	}

	/**
	 * Sets the site errors.
	 * 
	 * @param siteErrors
	 *            the new site errors
	 */
	public void setSiteErrors(String siteErrors) {
		this.siteErrors = siteErrors;
	}

	/**
	 * Gets the real time state.
	 * 
	 * @return the real time state
	 */
	public ArrayList<RealTimeState> getRealTimeState() {
		return realTimeState;
	}

	/**
	 * Sets the real time state.
	 * 
	 * @param realTimeState
	 *            the new real time state
	 */
	public void setRealTimeState(ArrayList<RealTimeState> realTimeState) {
		this.realTimeState = realTimeState;
	}

	public ArrayList<RealTimeState> getUnManagedIniAssessment() {
		return unManagedIniAssessment;
	}

	public void setUnManagedIniAssessment(
			ArrayList<RealTimeState> unManagedIniAssessment) {
		this.unManagedIniAssessment = unManagedIniAssessment;
	}

}
