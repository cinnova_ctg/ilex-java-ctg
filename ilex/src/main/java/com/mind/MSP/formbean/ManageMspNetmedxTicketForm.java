package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.mind.common.LabelValue;

/**
 * The Class ManageMspNetmedxTicketForm.
 * 
 * @author vijay kumar singh
 */
public class ManageMspNetmedxTicketForm extends ActionForm {

	private String customerName;
	private String circuitType;
	private String modemType;
	private String internetServiceLineNo;
	private String isp;
	private String ispSupportNo;
	private String circuitIP;
	private String siteContactNo;
	private String location;

	private String site_city;
	private String site_state;
	private String site_zip;
	private String site_number;
	private String site_name;
	private String site_address;
	private String site_time_zone;

	private String appendixId;
	private String jobId;
	private String ticketId;
	private String ticketNumber;
	private String problemCategory;
	private String probleDescription;
	private String installationNotes;
	private boolean isInstallNotesPresent;
	private String latestInstallNotesDate;
	private String latestInstallNotes;
	private String secondLatestInstallNotesDate;
	private String secondLatestInstallNotes;
	private String addNotes;
	private String estimatedEffort;
	private ArrayList<LabelValue> problemCategoryList;
	private String closeTicket;
	private String updateEffort;
	private Integer installNotesCount;

	public Integer getInstallNotesCount() {
		return installNotesCount;
	}

	public void setInstallNotesCount(Integer installNotesCount) {
		this.installNotesCount = installNotesCount;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCircuitType() {
		return circuitType;
	}

	public void setCircuitType(String circuitType) {
		this.circuitType = circuitType;
	}

	public String getModemType() {
		return modemType;
	}

	public void setModemType(String modemType) {
		this.modemType = modemType;
	}

	public String getInternetServiceLineNo() {
		return internetServiceLineNo;
	}

	public void setInternetServiceLineNo(String internetServiceLineNo) {
		this.internetServiceLineNo = internetServiceLineNo;
	}

	public String getIsp() {
		return isp;
	}

	public void setIsp(String isp) {
		this.isp = isp;
	}

	public String getIspSupportNo() {
		return ispSupportNo;
	}

	public void setIspSupportNo(String ispSupportNo) {
		this.ispSupportNo = ispSupportNo;
	}

	public String getCircuitIP() {
		return circuitIP;
	}

	public void setCircuitIP(String circuitIP) {
		this.circuitIP = circuitIP;
	}

	public String getSiteContactNo() {
		return siteContactNo;
	}

	public void setSiteContactNo(String siteContactNo) {
		this.siteContactNo = siteContactNo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSite_city() {
		return site_city;
	}

	public void setSite_city(String siteCity) {
		site_city = siteCity;
	}

	public String getSite_state() {
		return site_state;
	}

	public void setSite_state(String siteState) {
		site_state = siteState;
	}

	public String getSite_zip() {
		return site_zip;
	}

	public void setSite_zip(String siteZip) {
		site_zip = siteZip;
	}

	public String getSite_number() {
		return site_number;
	}

	public void setSite_number(String siteNumber) {
		site_number = siteNumber;
	}

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String siteName) {
		site_name = siteName;
	}

	public String getSite_address() {
		return site_address;
	}

	public void setSite_address(String siteAddress) {
		site_address = siteAddress;
	}

	public String getSite_time_zone() {
		return site_time_zone;
	}

	public void setSite_time_zone(String siteTimeZone) {
		site_time_zone = siteTimeZone;
	}

	public String getAppendixId() {
		return appendixId;
	}

	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getProblemCategory() {
		return problemCategory;
	}

	public void setProblemCategory(String problemCategory) {
		this.problemCategory = problemCategory;
	}

	public String getProbleDescription() {
		return probleDescription;
	}

	public void setProbleDescription(String probleDescription) {
		this.probleDescription = probleDescription;
	}

	public String getInstallationNotes() {
		return installationNotes;
	}

	public void setInstallationNotes(String installationNotes) {
		this.installationNotes = installationNotes;
	}

	public boolean isInstallNotesPresent() {
		return isInstallNotesPresent;
	}

	public void setInstallNotesPresent(boolean isInstallNotesPresent) {
		this.isInstallNotesPresent = isInstallNotesPresent;
	}

	public String getLatestInstallNotesDate() {
		return latestInstallNotesDate;
	}

	public void setLatestInstallNotesDate(String latestInstallNotesDate) {
		this.latestInstallNotesDate = latestInstallNotesDate;
	}

	public String getLatestInstallNotes() {
		return latestInstallNotes;
	}

	public void setLatestInstallNotes(String latestInstallNotes) {
		this.latestInstallNotes = latestInstallNotes;
	}

	public String getSecondLatestInstallNotesDate() {
		return secondLatestInstallNotesDate;
	}

	public void setSecondLatestInstallNotesDate(
			String secondLatestInstallNotesDate) {
		this.secondLatestInstallNotesDate = secondLatestInstallNotesDate;
	}

	public String getSecondLatestInstallNotes() {
		return secondLatestInstallNotes;
	}

	public void setSecondLatestInstallNotes(String secondLatestInstallNotes) {
		this.secondLatestInstallNotes = secondLatestInstallNotes;
	}

	public String getAddNotes() {
		return addNotes;
	}

	public void setAddNotes(String addNotes) {
		this.addNotes = addNotes;
	}

	public String getEstimatedEffort() {
		return estimatedEffort;
	}

	public void setEstimatedEffort(String estimatedEffort) {
		this.estimatedEffort = estimatedEffort;
	}

	public ArrayList<LabelValue> getProblemCategoryList() {
		return problemCategoryList;
	}

	public void setProblemCategoryList(ArrayList<LabelValue> problemCategoryList) {
		this.problemCategoryList = problemCategoryList;
	}

	public String getCloseTicket() {
		return closeTicket;
	}

	public void setCloseTicket(String closeTicket) {
		this.closeTicket = closeTicket;
	}

	public String getUpdateEffort() {
		return updateEffort;
	}

	public void setUpdateEffort(String updateEffort) {
		this.updateEffort = updateEffort;
	}

}
