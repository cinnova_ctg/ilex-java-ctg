package com.mind.MSP.formbean;

import org.apache.struts.action.ActionForm;

/**
 * The Class MspMonitorForm.
 */
public class MspMonitorForm extends ActionForm {

	/** The escalation level. */
	private int escalationLevel = 0;

	/**
	 * Gets the escalation level.
	 * 
	 * @return the escalation level
	 */
	public int getEscalationLevel() {
		return escalationLevel;
	}

	/**
	 * Sets the escalation level.
	 * 
	 * @param escalationLevel
	 *            the new escalation level
	 */
	public void setEscalationLevel(int escalationLevel) {
		this.escalationLevel = escalationLevel;
	}
}
