package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.mind.bean.msp.RealTimeState;

/**
 * The Class MspIncidentForm.
 */
public class MspIncidentForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The real time state. */
	private ArrayList<RealTimeState> incidentList = new ArrayList<RealTimeState>();

	/** The escalation level. */
	private String escalationLevel = null;

	/**
	 * Gets the real time state.
	 * 
	 * @return the real time state
	 */
	public ArrayList<RealTimeState> getIncidentList() {
		return incidentList;
	}

	/**
	 * Sets the real time state.
	 * 
	 * @param realTimeState
	 *            the new real time state
	 */
	public void setIncidentList(ArrayList<RealTimeState> realTimeState) {
		this.incidentList = realTimeState;
	}

	/**
	 * Gets the escalation level.
	 * 
	 * @return the escalation level
	 */
	public String getEscalationLevel() {
		return escalationLevel;
	}

	/**
	 * Sets the escalation level.
	 * 
	 * @param escalationLevel
	 *            the new escalation level
	 */
	public void setEscalationLevel(String escalationLevel) {
		this.escalationLevel = escalationLevel;
	}
}
