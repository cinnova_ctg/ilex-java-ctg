package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.mind.bean.msp.ChronicSite;
import com.mind.common.LabelValue;

public class ChronicSitesForm extends ActionForm {

	private static final long serialVersionUID = 1L;
	private String customerName = null;
	private ArrayList<ChronicSite> chronicSiteList = null;
	private ArrayList<LabelValue> customerList =null;
	private String view = null;
	private String cancel = null;
	
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public ArrayList<ChronicSite> getChronicSiteList() {
		return chronicSiteList;
	}
	public void setChronicSiteList(ArrayList<ChronicSite> chronicSiteList) {
		this.chronicSiteList = chronicSiteList;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getView() {
		return view;
	}
	public void setView(String view) {
		this.view = view;
	}
	public ArrayList<LabelValue> getCustomerList() {
		return customerList;
	}
	public void setCustomerList(ArrayList<LabelValue> customerList) {
		this.customerList = customerList;
	}
}
