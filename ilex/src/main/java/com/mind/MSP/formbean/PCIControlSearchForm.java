/**
 * 
 */
package com.mind.MSP.formbean;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

import com.mind.bean.msp.PCISearchInfo;
import com.mind.common.LabelValue;

/**
 * @author yogendrasingh
 * 
 *         Used for Searching the appropriate PCI Change Control.
 */
public class PCIControlSearchForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String orgTopId = "";

	private String selectedSiteID = "";
	private List<LabelValue> customerList;
	private String selectedChangeRequestStatus = "";
	private List<LabelValue> selectedChangeRequestStatusList = new ArrayList<LabelValue>();
	private List<LabelValue> siteIdentifierList = new ArrayList<LabelValue>();

	private List<String> pciControlSearchList;

	private List<PCISearchInfo> pciChangeControlFormList;

	private String fromDate;
	private String toDate;

	/**
	 * @return the orgTopId
	 */
	public String getOrgTopId() {
		return orgTopId;
	}

	/**
	 * @param orgTopId
	 *            the orgTopId to set
	 */
	public void setOrgTopId(String orgTopId) {
		this.orgTopId = orgTopId;
	}

	/**
	 * @return the selectedSiteID
	 */
	public String getSelectedSiteID() {
		return selectedSiteID;
	}

	/**
	 * @param selectedSiteID
	 *            the selectedSiteID to set
	 */
	public void setSelectedSiteID(String selectedSiteID) {
		this.selectedSiteID = selectedSiteID;
	}

	/**
	 * @return the customerList
	 */
	public List<LabelValue> getCustomerList() {
		return customerList;
	}

	/**
	 * @param customerList
	 *            the customerList to set
	 */
	public void setCustomerList(List<LabelValue> customerList) {
		this.customerList = customerList;
	}

	/**
	 * @return the selectedChangeRequestStatus
	 */
	public String getSelectedChangeRequestStatus() {
		return selectedChangeRequestStatus;
	}

	/**
	 * @param selectedChangeRequestStatus
	 *            the selectedChangeRequestStatus to set
	 */
	public void setSelectedChangeRequestStatus(
			String selectedChangeRequestStatus) {
		this.selectedChangeRequestStatus = selectedChangeRequestStatus;
	}

	/**
	 * @return the selectedChangeRequestStatusList
	 */
	public List<LabelValue> getSelectedChangeRequestStatusList() {
		return selectedChangeRequestStatusList;
	}

	/**
	 * @param selectedChangeRequestStatusList
	 *            the selectedChangeRequestStatusList to set
	 */
	public void setSelectedChangeRequestStatusList(
			List<LabelValue> selectedChangeRequestStatusList) {
		this.selectedChangeRequestStatusList = selectedChangeRequestStatusList;
	}

	/**
	 * @return the siteIdentifierList
	 */
	public List<LabelValue> getSiteIdentifierList() {
		return siteIdentifierList;
	}

	/**
	 * @param siteIdentifierList
	 *            the siteIdentifierList to set
	 */
	public void setSiteIdentifierList(List<LabelValue> siteIdentifierList) {
		this.siteIdentifierList = siteIdentifierList;
	}

	/**
	 * @return the pciControlSearchList
	 */
	public List<String> getPciControlSearchList() {
		return pciControlSearchList;
	}

	/**
	 * @param pciControlSearchList
	 *            the pciControlSearchList to set
	 */
	public void setPciControlSearchList(List<String> pciControlSearchList) {
		this.pciControlSearchList = pciControlSearchList;
	}

	/**
	 * @return the pciChangeControlFormList
	 */
	public List<PCISearchInfo> getPciChangeControlFormList() {
		return pciChangeControlFormList;
	}

	/**
	 * @param pciChangeControlFormList
	 *            the pciChangeControlFormList to set
	 */
	public void setPciChangeControlFormList(
			List<PCISearchInfo> pciChangeControlFormList) {
		this.pciChangeControlFormList = pciChangeControlFormList;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate
	 *            the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate
	 *            the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

}
