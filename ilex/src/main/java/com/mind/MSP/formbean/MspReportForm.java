package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.mind.bean.msp.RealTimeState;
import com.mind.common.LabelValue;

public class MspReportForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<LabelValue> customerList = new ArrayList<LabelValue>();
	private String customerId = null;
	private String fromDate = null;
	private String toDate = null;
	private String total = null;
	private ArrayList<RealTimeState> searchList = new ArrayList<RealTimeState>();
	private String search = null;
	private String cancel = null;
	private String reportType = null; 
	
	public String getReportType() {
		return reportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public ArrayList<LabelValue> getCustomerList() {
		return customerList;
	}
	public void setCustomerList(ArrayList<LabelValue> customerList) {
		this.customerList = customerList;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public ArrayList<RealTimeState> getSearchList() {
		return searchList;
	}
	public void setSearchList(ArrayList<RealTimeState> searchList) {
		this.searchList = searchList;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}

}
