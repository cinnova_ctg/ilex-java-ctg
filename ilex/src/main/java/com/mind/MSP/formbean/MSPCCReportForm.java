package com.mind.MSP.formbean;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.struts.action.ActionForm;

import com.mind.bean.msp.MSPCCReportBean;

public class MSPCCReportForm extends ActionForm {

	private String siteCount = null;

	// For View Selector: Start
	private String jobOwnerOtherCheck = null;
	private String[] jobSelectedStatus = null;
	private String[] jobSelectedOwners = null;
	private String jobMonthWeekCheck = null;
	// For View Selector: End

	// For Menu: Start
	private String ownerId = null;
	private String viewjobtype = null;
	private String msaId = null;
	private String fromPage = null;
	// For Menu: End

	private String appendixid = null;
	private String appendixName = null;
	private String pocId = null;
	private Collection assignTeamList = null;
	private String roleId = null;
	private Collection roleList = null;
	private Collection pocList = null;
	private String save = null;
	private String msaName = null;

	private String strtDateName = null;
	private String strtDateValue = null;
	private String plannedDateName = null;
	private String plannedDateValue = null;
	private String siteCountName = null;
	private String siteCountValue = null;
	private String lastUpdateDateName = null;
	private String lastUpdateDateValue = null;
	private String totalCompleted = null;
	private String totalErrors = null;
	private String[] categoryColumns = null;

	public String[] getCategoryColumns() {
		return categoryColumns;
	}

	public void setCategoryColumns(String[] categoryColumns) {
		this.categoryColumns = categoryColumns;
	}

	public String getTotalCompleted() {
		return totalCompleted;
	}

	public void setTotalCompleted(String totalCompleted) {
		this.totalCompleted = totalCompleted;
	}

	public String getTotalErrors() {
		return totalErrors;
	}

	public void setTotalErrors(String totalErrors) {
		this.totalErrors = totalErrors;
	}

	private ArrayList<MSPCCReportBean> sectionAList = null;
	private ArrayList<MSPCCReportBean> sectionCList = null;
	private ArrayList<MSPCCReportBean> sectionDList = null;
	private ArrayList<MSPCCReportBean> sectionEList = null;
	private ArrayList<MSPCCReportBean> sectionFList = null;
	private ArrayList<MSPCCReportBean> sectionBHeaderList = null;

	public String getStrtDateName() {
		return strtDateName;
	}

	public void setStrtDateName(String strtDateName) {
		this.strtDateName = strtDateName;
	}

	public ArrayList<MSPCCReportBean> getSectionBHeaderList() {
		return sectionBHeaderList;
	}

	public void setSectionBHeaderList(
			ArrayList<MSPCCReportBean> sectionBHeaderList) {
		this.sectionBHeaderList = sectionBHeaderList;
	}

	public String getStrtDateValue() {
		return strtDateValue;
	}

	public void setStrtDateValue(String strtDateValue) {
		this.strtDateValue = strtDateValue;
	}

	public String getPlannedDateName() {
		return plannedDateName;
	}

	public void setPlannedDateName(String plannedDateName) {
		this.plannedDateName = plannedDateName;
	}

	public String getPlannedDateValue() {
		return plannedDateValue;
	}

	public void setPlannedDateValue(String plannedDateValue) {
		this.plannedDateValue = plannedDateValue;
	}

	public String getSiteCountName() {
		return siteCountName;
	}

	public void setSiteCountName(String siteCountName) {
		this.siteCountName = siteCountName;
	}

	public String getSiteCountValue() {
		return siteCountValue;
	}

	public void setSiteCountValue(String siteCountValue) {
		this.siteCountValue = siteCountValue;
	}

	public String getLastUpdateDateName() {
		return lastUpdateDateName;
	}

	public void setLastUpdateDateName(String lastUpdateDateName) {
		this.lastUpdateDateName = lastUpdateDateName;
	}

	public String getLastUpdateDateValue() {
		return lastUpdateDateValue;
	}

	public void setLastUpdateDateValue(String lastUpdateDateValue) {
		this.lastUpdateDateValue = lastUpdateDateValue;
	}

	public ArrayList<MSPCCReportBean> getSectionAList() {
		return sectionAList;
	}

	public void setSectionAList(ArrayList<MSPCCReportBean> sectionAList) {
		this.sectionAList = sectionAList;
	}

	public ArrayList<MSPCCReportBean> getSectionCList() {
		return sectionCList;
	}

	public void setSectionCList(ArrayList<MSPCCReportBean> sectionCList) {
		this.sectionCList = sectionCList;
	}

	public ArrayList<MSPCCReportBean> getSectionDList() {
		return sectionDList;
	}

	public void setSectionDList(ArrayList<MSPCCReportBean> sectionDList) {
		this.sectionDList = sectionDList;
	}

	public ArrayList<MSPCCReportBean> getSectionEList() {
		return sectionEList;
	}

	public void setSectionEList(ArrayList<MSPCCReportBean> sectionEList) {
		this.sectionEList = sectionEList;
	}

	public ArrayList<MSPCCReportBean> getSectionFList() {
		return sectionFList;
	}

	public void setSectionFList(ArrayList<MSPCCReportBean> sectionFList) {
		this.sectionFList = sectionFList;
	}

	public String getAppendixid() {
		return appendixid;
	}

	public void setAppendixid(String appendixid) {
		this.appendixid = appendixid;
	}

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getPocId() {
		return pocId;
	}

	public void setPocId(String pocId) {
		this.pocId = pocId;
	}

	public Collection getAssignTeamList() {
		return assignTeamList;
	}

	public void setAssignTeamList(Collection assignTeamList) {
		this.assignTeamList = assignTeamList;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public Collection getRoleList() {
		return roleList;
	}

	public void setRoleList(Collection roleList) {
		this.roleList = roleList;
	}

	public Collection getPocList() {
		return pocList;
	}

	public void setPocList(Collection pocList) {
		this.pocList = pocList;
	}

	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public String getSiteCount() {
		return siteCount;
	}

	public String getJobOwnerOtherCheck() {
		return jobOwnerOtherCheck;
	}

	public void setJobOwnerOtherCheck(String jobOwnerOtherCheck) {
		this.jobOwnerOtherCheck = jobOwnerOtherCheck;
	}

	public String[] getJobSelectedStatus() {
		return jobSelectedStatus;
	}

	public String getJobSelectedStatus(int i) {
		return jobSelectedStatus[i];
	}

	public void setJobSelectedStatus(String[] jobSelectedStatus) {
		this.jobSelectedStatus = jobSelectedStatus;
	}

	public String[] getJobSelectedOwners() {
		return jobSelectedOwners;
	}

	public String getJobSelectedOwners(int i) {
		return jobSelectedOwners[i];
	}

	public void setJobSelectedOwners(String[] jobSelectedOwners) {
		this.jobSelectedOwners = jobSelectedOwners;
	}

	public String getJobMonthWeekCheck() {
		return jobMonthWeekCheck;
	}

	public void setJobMonthWeekCheck(String jobMonthWeekCheck) {
		this.jobMonthWeekCheck = jobMonthWeekCheck;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getViewjobtype() {
		return viewjobtype;
	}

	public void setViewjobtype(String viewjobtype) {
		this.viewjobtype = viewjobtype;
	}

	public String getMsaId() {
		return msaId;
	}

	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public void setSiteCount(String siteCount) {
		this.siteCount = siteCount;
	}

	public String getMsaName() {
		return msaName;
	}

	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

}
