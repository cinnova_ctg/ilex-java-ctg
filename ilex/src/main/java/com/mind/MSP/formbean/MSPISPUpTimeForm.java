package com.mind.MSP.formbean;

import org.apache.struts.action.ActionForm;

public class MSPISPUpTimeForm extends ActionForm{
	private String tableData = null;

	public String getTableData() {
		return tableData;
	}

	public void setTableData(String tableData) {
		this.tableData = tableData;
	}

}
