package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.mind.bean.msp.MspNetmedxTicket;

/**
 * The Class MspCustomerReportedEventForm.
 * 
 * @author vijay kumar singh
 */
public class MspCustomerReportedEventForm extends ActionForm {

	private ArrayList<MspNetmedxTicket> mspNetmedxTicketList;

	public ArrayList<MspNetmedxTicket> getMspNetmedxTicketList() {
		return mspNetmedxTicketList;
	}

	public void setMspNetmedxTicketList(
			ArrayList<MspNetmedxTicket> mspNetmedxTicketList) {
		this.mspNetmedxTicketList = mspNetmedxTicketList;
	}

}
