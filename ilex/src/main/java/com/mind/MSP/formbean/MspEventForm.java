package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.mind.bean.msp.RealTimeState;

/**
 * The Class MspEventForm.
 */
public class MspEventForm extends ActionForm {

	/** The incident list. */
	private ArrayList<RealTimeState> incidentList = new ArrayList<RealTimeState>();

	/** The escalation level. */
	private String escalationLevel = null;

	/**
	 * Gets the escalation level.
	 * 
	 * @return the escalation level
	 */
	public String getEscalationLevel() {
		return escalationLevel;
	}

	/**
	 * Sets the escalation level.
	 * 
	 * @param escalationLevel
	 *            the new escalation level
	 */
	public void setEscalationLevel(String escalationLevel) {
		this.escalationLevel = escalationLevel;
	}

	/**
	 * Gets the incident list.
	 * 
	 * @return the incident list
	 */
	public ArrayList<RealTimeState> getIncidentList() {
		return incidentList;
	}

	/**
	 * Sets the incident list.
	 * 
	 * @param incidentList
	 *            the new incident list
	 */
	public void setIncidentList(ArrayList<RealTimeState> incidentList) {
		this.incidentList = incidentList;
	}

}
