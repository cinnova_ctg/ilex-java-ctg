package com.mind.MSP.formbean;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;

public class SiteDetailForm extends ActionForm {
	
	
	/**
	 * Site Detail information 
	 */
	private static final long serialVersionUID = 1L;
	private String circuitType = null;
	private String modemType = null;
	private String internetServiceLineNo = null;
	private String isp = null;
	private String ispSupportNo = null;
	private String circuitIP = null;
	private String siteContactNo = null;
	private String location = null;
	private String siteNumber = null;
	private String siteAddress = null;
	private String siteCity = null;
	private String siteState = null;
	private String siteZip = null;
	private String siteTimeZone = null;
	private String accountNumber = null;
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getCircuitType() {
		return circuitType;
	}
	public void setCircuitType(String circuitType) {
		this.circuitType = circuitType;
	}
	public String getModemType() {
		return modemType;
	}
	public void setModemType(String modemType) {
		this.modemType = modemType;
	}
	public String getInternetServiceLineNo() {
		return internetServiceLineNo;
	}
	public void setInternetServiceLineNo(String internetServiceLineNo) {
		this.internetServiceLineNo = internetServiceLineNo;
	}
	public String getIsp() {
		return isp;
	}
	public void setIsp(String isp) {
		this.isp = isp;
	}
	public String getIspSupportNo() {
		return ispSupportNo;
	}
	public void setIspSupportNo(String ispSupportNo) {
		this.ispSupportNo = ispSupportNo;
	}
	public String getCircuitIP() {
		return circuitIP;
	}
	public void setCircuitIP(String circuitIP) {
		this.circuitIP = circuitIP;
	}
	public String getSiteContactNo() {
		return siteContactNo;
	}
	public void setSiteContactNo(String siteContactNo) {
		this.siteContactNo = siteContactNo;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getSiteNumber() {
		return siteNumber;
	}
	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}
	public String getSiteAddress() {
		return siteAddress;
	}
	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}
	public String getSiteCity() {
		return siteCity;
	}
	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}
	public String getSiteState() {
		return siteState;
	}
	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}
	public String getSiteZip() {
		return siteZip;
	}
	public void setSiteZip(String siteZip) {
		this.siteZip = siteZip;
	}
	public String getSiteTimeZone() {
		return siteTimeZone;
	}
	public void setSiteTimeZone(String siteTimeZone) {
		this.siteTimeZone = siteTimeZone;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String toString(){
	      return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	    }
}
