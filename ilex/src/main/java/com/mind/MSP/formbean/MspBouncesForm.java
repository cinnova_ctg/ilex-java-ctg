package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.mind.bean.msp.Bounces;

public class MspBouncesForm extends ActionForm{
	
	private ArrayList<Bounces> bouncesList = new ArrayList<Bounces>();
	private int listSize = 0;
	
	public ArrayList<Bounces> getBouncesList() {
		return bouncesList;
	}
	public void setBouncesList(ArrayList<Bounces> bouncesList) {
		this.bouncesList = bouncesList;
	}
	public int getListSize() {
		return listSize;
	}
	public void setListSize(int listSize) {
		this.listSize = listSize;
	}
	
	
		

}
