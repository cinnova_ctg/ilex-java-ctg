package com.mind.MSP.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.mind.bean.msp.EverWorx;

public class MspEverWorxForm extends ActionForm{

	private ArrayList<EverWorx> everWorxList= new ArrayList<EverWorx>();

	public ArrayList<EverWorx> getEverWorxList() {
		return everWorxList;
	}

	public void setEverWorxList(ArrayList<EverWorx> everWorxList) {
		this.everWorxList = everWorxList;
	}
	
	
}
