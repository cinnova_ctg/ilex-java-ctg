package com.mind.MSP.formbean;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

import com.mind.bean.msp.ChangeRequestActionDetails;
import com.mind.common.LabelValue;

public class PCIChangeControlForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String orgTopId = "";
	private String requestorName = "";
	private String selectedSiteID = "";
	private List<LabelValue> customerList;
	private String selectedChangeRequestStatus = "CR_O";
	private List<LabelValue> selectedChangeRequestStatusList = new ArrayList<LabelValue>();
	private String descriptionOfChange = "";
	private String reasonForChanges = "";
	private String resourceImpact = "";
	private String finalDispositionName = "";
	private String finalDispositionStatusSelected = "";
	private String changeRequestID = "";
	private List<ChangeRequestActionDetails> changeRequestActionsList;
	private List<LabelValue> changeRequestActionStatusList;
	private String currentMode = "";

	public String getCurrentMode() {
		return currentMode;
	}

	public void setCurrentMode(String currentMode) {
		this.currentMode = currentMode;
	}

	public List<LabelValue> getChangeRequestActionStatusList() {
		return changeRequestActionStatusList;
	}

	public void setChangeRequestActionStatusList(
			List<LabelValue> changeRequestActionStatusList) {
		this.changeRequestActionStatusList = changeRequestActionStatusList;
	}

	public List<ChangeRequestActionDetails> getChangeRequestActionsList() {
		return changeRequestActionsList;
	}

	public void setChangeRequestActionsList(
			List<ChangeRequestActionDetails> changeRequestActionsList) {
		this.changeRequestActionsList = changeRequestActionsList;
	}

	public String getChangeRequestID() {
		return changeRequestID;
	}

	public void setChangeRequestID(String changeRequestID) {
		this.changeRequestID = changeRequestID;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	private List<LabelValue> finalDispositionStatusList = new ArrayList<LabelValue>();
	private String finalDispositionDate = "";
	private String finalDispositionComments = "";
	private String selectedSiteIdentifierName = "";

	public String getSelectedSiteIdentifierName() {
		return selectedSiteIdentifierName;
	}

	public void setSelectedSiteIdentifierName(String selectedSiteIdentifierName) {
		this.selectedSiteIdentifierName = selectedSiteIdentifierName;
	}

	public String getFinalDispositionName() {
		return finalDispositionName;
	}

	public void setFinalDispositionName(String finalDispositionName) {
		this.finalDispositionName = finalDispositionName;
	}

	public String getFinalDispositionStatusSelected() {
		return finalDispositionStatusSelected;
	}

	public void setFinalDispositionStatusSelected(
			String finalDispositionStatusSelected) {
		this.finalDispositionStatusSelected = finalDispositionStatusSelected;
	}

	public List<LabelValue> getFinalDispositionStatusList() {
		return finalDispositionStatusList;
	}

	public void setFinalDispositionStatusList(
			List<LabelValue> finalDispositionStatusList) {
		this.finalDispositionStatusList = finalDispositionStatusList;
	}

	public String getFinalDispositionDate() {
		return finalDispositionDate;
	}

	public void setFinalDispositionDate(String finalDispositionDate) {
		this.finalDispositionDate = finalDispositionDate;
	}

	public String getFinalDispositionComments() {
		return finalDispositionComments;
	}

	public void setFinalDispositionComments(String finalDispositionComments) {
		this.finalDispositionComments = finalDispositionComments;
	}

	public String getResourceImpact() {
		return resourceImpact;
	}

	public void setResourceImpact(String resourceImpact) {
		this.resourceImpact = resourceImpact;
	}

	public String getReasonForChanges() {
		return reasonForChanges;
	}

	public void setReasonForChanges(String reasonForChanges) {
		this.reasonForChanges = reasonForChanges;
	}

	public String getSelectedChangeRequestStatus() {
		return selectedChangeRequestStatus;
	}

	public String getDescriptionOfChange() {
		return descriptionOfChange;
	}

	public void setDescriptionOfChange(String descriptionOfChange) {
		this.descriptionOfChange = descriptionOfChange;
	}

	public List<LabelValue> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<LabelValue> customerList) {
		this.customerList = customerList;
	}

	public void setSelectedChangeRequestStatus(
			String selectedChangeRequestStatus) {
		this.selectedChangeRequestStatus = selectedChangeRequestStatus;
	}

	public List<LabelValue> getSelectedChangeRequestStatusList() {
		return selectedChangeRequestStatusList;
	}

	public void setSelectedChangeRequestStatusList(
			List<LabelValue> selectedChangeRequestStatusList) {
		this.selectedChangeRequestStatusList = selectedChangeRequestStatusList;
	}

	public String getSelectedSiteID() {
		return selectedSiteID;
	}

	public void setSelectedSiteID(String selectedSiteID) {
		this.selectedSiteID = selectedSiteID;
	}

	public List<LabelValue> getSiteIdentifierList() {
		return siteIdentifierList;
	}

	public void setSiteIdentifierList(List<LabelValue> siteIdentifierList) {
		this.siteIdentifierList = siteIdentifierList;
	}

	private List<LabelValue> siteIdentifierList = new ArrayList<LabelValue>();

	public String getOrgTopId() {
		return orgTopId;
	}

	public void setOrgTopId(String orgTopId) {
		this.orgTopId = orgTopId;
	}

}
