package com.mind.MSP.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Class Discription: This Servlet class is used for Ilex Authentication.
 * Methods : doGet
 * @author		ILEX
 */
public class MspLogin extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		response.sendRedirect("./main.jsp?msp=msp");
	}

}



