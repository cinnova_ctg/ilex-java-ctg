package com.mind.MSP.common;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TreeMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class MspUtility {

	/**
	 * @param noActionFlag
	 * @return
	 */
	public static int returnInt(int noActionFlag) {
		if (noActionFlag > -1) {
			noActionFlag = 1;
		}
		return noActionFlag;
	}

	/**
	 * To set ajax return value.
	 * 
	 * @param createFlag
	 * @param out
	 */
	public static void ajaxReturn(int createFlag, PrintWriter out) {
		out.println(createFlag);
		out.flush();
		out.close();
	}

	/**
	 * System date.
	 * 
	 * @return the string
	 */
	public static String getSystemDate() {
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date date = new java.util.Date();
		return sf.format(date);
	}

	public String getJSONString(List<TreeMap<String, String>> markerList) {
		JSONArray jArray = new JSONArray();
		for (TreeMap<String, String> treeMap : markerList) {
			JSONObject jObj = new JSONObject();
			jObj.putAll(treeMap);
			jArray.add(jObj);
		}
		return jArray.toString();
	}

}
