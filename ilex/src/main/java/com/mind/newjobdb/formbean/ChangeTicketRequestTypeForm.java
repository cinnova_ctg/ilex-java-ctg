package com.mind.newjobdb.formbean;

import java.util.Collection;

import org.apache.struts.action.ActionForm;

public class ChangeTicketRequestTypeForm  extends ActionForm{
	private String requestType = null;
	private Collection requestTypeCombo = null;
	private String saveRequestType = null;
	private String previousRequsetType = null;
	private String msaId = null;
	private String jobId = null;
	
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public String getPreviousRequsetType() {
		return previousRequsetType;
	}
	public void setPreviousRequsetType(String previousRequsetType) {
		this.previousRequsetType = previousRequsetType;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getSaveRequestType() {
		return saveRequestType;
	}
	public void setSaveRequestType(String saveRequestType) {
		this.saveRequestType = saveRequestType;
	}
	public Collection getRequestTypeCombo() {
		return requestTypeCombo;
	}
	public void setRequestTypeCombo(Collection requestTypeCombo) {
		this.requestTypeCombo = requestTypeCombo;
	}

	
}
