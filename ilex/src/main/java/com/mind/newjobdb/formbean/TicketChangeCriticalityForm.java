package com.mind.newjobdb.formbean;

import java.util.Collection;

import org.apache.struts.action.ActionForm;

public class TicketChangeCriticalityForm extends ActionForm{
	
	private String msaId = null;
	private String jobId = null;
	private String criticality = null;
	private String prveCriticality = null;
	private String resource = null;
	private Collection criticalityCombo = null;
	private Collection resourceCombo = null;
	private String update = null;
	private String cancel = null;
	private String helpDesk = null;
	private String appendixId = null;
	
	
	public String getAppendixId() {
		return appendixId;
	}
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getMsaId() {
		return msaId;
	}
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}
	public Collection getCriticalityCombo() {
		return criticalityCombo;
	}
	public void setCriticalityCombo(Collection criticalityCombo) {
		this.criticalityCombo = criticalityCombo;
	}
	public Collection getResourceCombo() {
		return resourceCombo;
	}
	public void setResourceCombo(Collection resourceCombo) {
		this.resourceCombo = resourceCombo;
	}
	public String getCriticality() {
		return criticality;
	}
	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}
	public String getPrveCriticality() {
		return prveCriticality;
	}
	public void setPrveCriticality(String prveCriticality) {
		this.prveCriticality = prveCriticality;
	}
	public String getHelpDesk() {
		return helpDesk;
	}
	public void setHelpDesk(String helpDesk) {
		this.helpDesk = helpDesk;
	}
	
}
