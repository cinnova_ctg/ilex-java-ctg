package com.mind.newjobdb.actions;

import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.newjobdb.CostVariations;
import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobDashboard;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.JobPlanningNote;
import com.mind.bean.newjobdb.TeamInfo;
import com.mind.common.EnvironmentSelector;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Email;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.CustomerInfoBean;
import com.mind.dao.PRM.CustomerInformationdao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.BuyTab;
import com.mind.newjobdb.business.Convertor;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.dao.JobLevelWorkflowDAO;
import com.mind.newjobdb.dao.JobWorkflowChecklist;
import com.mind.newjobdb.dao.POResourceDAO;
import com.mind.newjobdb.dao.PurchaseOrder;
import com.mind.newjobdb.dao.PurchaseOrderDAO;
import com.mind.newjobdb.dao.PurchaseOrderList;
import com.mind.newjobdb.formbean.JobDashboardForm;
import com.mind.util.WebUtil;

public class JobBuyAction extends com.mind.common.IlexDispatchAction {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JobBuyAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
			// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
				"Job Dashboard - Buy Tab", getDataSource(request, "ilexnewDB"))) {
			request.setAttribute("HidePage", "HidePage");
			// return( mapping.findForward( "UnAuthenticate" ) ); // - Check for
			// Pager Security.
		}
		/* Page Security=: End */

		request.setAttribute(
				"tabId",
				(request.getParameter("tabId") == null ? ""
						+ JobDashboardTabType.BUY_TAB : request
						.getParameter("tabId")));
		JobDashboardForm bean = (JobDashboardForm) form;
		JobDashboardBean dto = new JobDashboardBean();
		bean.setTabId("" + JobDashboardTabType.BUY_TAB);
		setGeneralFormInfo(bean, request);
		boolean isResource = POResourceDAO.isResourceExist(bean.getJobId(),
				getDataSource(request, "ilexnewDB"));
		if (isResource) {
			request.setAttribute("isResource", "isResource");
		}
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		JobDAO.setLatestComments(map, dto, getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		setBuyTabInfoInForm(dto, getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		String jobType = JobSetUpDao.getjobType(bean.getJobId(),
				getDataSource(request, "ilexnewDB"));

		if (jobType != null && jobType.trim().equalsIgnoreCase("Default")) {
			jobType = "Default";
		} else if (jobType != null
				&& jobType.trim().equalsIgnoreCase("Addendum")) {
			jobType = "Addendum";
		} else {
			jobType = "";
		}

		setStatusOfTabs(jobType, request, bean);
		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		ArrayList<CustomerInfoBean> requiredDataList = CustomerInformationdao
				.getCustomerRequiredData(bean.getAppendixId(), bean.getJobId(),
						getDataSource(request, "ilexnewDB"));
		boolean isSnapon = Appendixdao.checkForSnapon(bean.getAppendixId(),
				getDataSource(request, "ilexnewDB"));

		request.setAttribute("requiredDataList", requiredDataList);
		request.setAttribute("isSnapon", isSnapon);

		request.setAttribute("buyTab", "buyTab");

		return mapping.findForward("success");
	}

	private void writeNode(String name, String value, StringBuffer sb) {
		if (value == null || value.trim().equals("")) {
			value = "-";
		}
		sb.append("<" + name + ">" + value + "</" + name + ">\n");
	}

	public void setStatusOfTabs(String jobType, HttpServletRequest request,
			JobDashboardForm bean) {
		String ticketType = null;
		ticketType = bean.getTicketType();

		if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
			request.setAttribute("fromStatus", "fromStatus");
		}
		if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
			request.setAttribute("isTicket", "Ticket");
			request.setAttribute("tabStatus", "job");
		} else {
			if (jobType != null && !jobType.trim().equalsIgnoreCase("")
					&& jobType.trim().equalsIgnoreCase("Default")) {
				request.setAttribute("tabStatus", "Default");
			}
			if (jobType != null && !jobType.trim().equalsIgnoreCase("")
					&& jobType.trim().equalsIgnoreCase("Addendum")) {
				request.setAttribute("tabStatus", "Addendum");
			} else if (jobType != null && jobType.trim().equalsIgnoreCase("")) {
				request.setAttribute("tabStatus", "job");
			}
			request.setAttribute("isTicket", "job");
		}
		// request.setAttribute("tabStatus",jobType);
	}

	public void setBuyTabInfoInForm(JobDashboardBean jobDashboardForm,
			DataSource ds) {
		BuyTab buyTab = new BuyTab();
		JobDashboard jobDashboard = new JobDashboard();
		JobDAO jobDao = new JobDAO();

		buyTab.setAppendixId(jobDashboardForm.getAppendixId());
		buyTab.setJobId(jobDashboardForm.getJobId());
		GeneralJobInfo generalJobInfo = jobDao.getGeneralJobInfo(
				Long.parseLong(jobDashboardForm.getJobId()), ds);
		buyTab.setGeneralJobInfo(generalJobInfo);

		JobPlanningNote jobPlanningNote = (JobPlanningNote) jobDao
				.getJobPlanningNotes(
						Long.parseLong(jobDashboardForm.getJobId()), ds);
		buyTab.setJobPlanningNote(jobPlanningNote);

		boolean showEvenIfNoPending = true;
		JobWorkflowChecklist topPendingCheckListItems = ((JobWorkflowChecklist) JobLevelWorkflowDAO
				.getJobWorkflowChecklist(
						Long.parseLong(jobDashboardForm.getJobId()), ds))
				.getTopPendingItems(2, showEvenIfNoPending);

		buyTab.setTopPendingCheckListItems(topPendingCheckListItems);

		ArrayList<PurchaseOrder> listOfPurchaseOrders = PurchaseOrderDAO
				.getPurchaseOrderList(jobDashboardForm.getJobId(), ds);
		PurchaseOrderList purchaseOrderList = new PurchaseOrderList();
		purchaseOrderList.setPurchaseOrders(listOfPurchaseOrders);
		buyTab.setPurchaseOrderList(purchaseOrderList);
		CostVariations totalCostVariations = PurchaseOrderList
				.getTotalCostVariations(purchaseOrderList);
		buyTab.setCostVariations(totalCostVariations);
		jobDashboard.setJobDashboardTab(buyTab);
		Convertor.convertToJobDashboardForm(jobDashboard, jobDashboardForm);
	}

	public ActionForward addComments(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("Pragma", "No-Cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", 0);

		String jobId = request.getParameter("jobId");
		String comments = request.getParameter("comments");
		// comments = URLDecoder.decode(comments);
		try {
			comments = URLDecoder.decode(comments, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		String reqEmail = request.getParameter("requestorEmail");
		String ticket_id = request.getParameter("ticketId");
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		String userName = (String) session.getAttribute("username");

		// boolean isSendEmail = Boolean.valueOf(request
		// .getParameter("isSendEmail"));
		// boolean isSendEmail = true;
		Map<String, Object> map = new HashMap<String, Object>();

		try {

			JobDAO.addJobComments(comments, jobId, loginuserid,
					getDataSource(request, "ilexnewDB"));
			String xml = JobDAO.setLatestComments(jobId, map,
					getDataSource(request, "ilexnewDB"), null);
			WebUtil.copyMapToRequest(request, map);

			// response.setContentType("application/xml");
			response.setContentType("application/xml; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(xml);
			// if (isSendEmail) {
			// sendEmail(reqEmail, jobId, ticket_id, comments, loginuserid,
			// userName, request);
			// }

		} catch (Exception e) {
			logger.error(
					"addComments(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"addComments(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}
		return null;
	}

	private void sendEmail(String reqEmail, String jobId, String ticket_id,
			String note, String userId, String userName,
			HttpServletRequest request, DataSource ds) {

		EmailBean emailform = new EmailBean();

		emailform.setUsername(this.getResources(request).getMessage(
				"common.Email.username"));
		emailform.setUserpassword(this.getResources(request).getMessage(
				"common.Email.userpassword"));

		emailform.setSmtpservername(EnvironmentSelector
				.getBundleString("common.Email.smtpservername"));
		emailform.setSmtpserverport(this.getResources(request).getMessage(
				"common.Email.smtpserverport"));

		emailform.setFrom("cma@contingent.com");
		emailform.setTo(reqEmail);
		emailform.setSubject("(" + jobId + "-" + userId + ") - " + ticket_id
				+ " - Internal Comments");

		StringBuffer mailBody = new StringBuffer();

		mailBody.append("<b>Internal Comment Added:</b>  " + note + "<br />");

		mailBody.append("<br /><br />Sincerely, <br />");
		mailBody.append("Contingent Network Services<br />4400 Port Union Road<br />West Chester, "
				+ "OH 45011<br />(800) 506-9609<br />(513)860-2105(facsimile)<br />medius.contingent.com"
				+ "<br /><br />How are we doing?Please let us know! medius.contingent.com/satisfaction");

		emailform.setContent(mailBody.toString());

		Email.Send(emailform, "text/html", ds);

	}

	public void setGeneralFormInfo(JobDashboardForm bean,
			HttpServletRequest request) {
		bean.setTabId((request.getParameter("tabId") == null ? ""
				+ JobDashboardTabType.BUY_TAB : request.getParameter("tabId")));
		String loginUserName = (String) request.getSession().getAttribute(
				"username");
		bean.setCreatedBy(loginUserName);
		bean.setJobId((String) (request.getParameter("Job_Id") == null ? request
				.getAttribute("jobid") : request.getParameter("Job_Id")));
		// To open general job Dashboard.
		if (request.getParameter("jobid") != null) {
			bean.setJobId(request.getParameter("jobid"));
		}
		if (request.getAttribute("jobid") != null) {
			bean.setJobId((String) request.getAttribute("jobid"));
		}
		JobDAO jobDAO = new JobDAO();
		TeamInfo teamInfo = jobDAO.getTeamInfo(new Long(bean.getJobId()),
				getDataSource(request, "ilexnewDB"));

		bean.setOwner(teamInfo.getOwner());

		bean.setAppendixId((String) (request.getParameter("appendix_Id") == null ? bean
				.getAppendixId() : request.getParameter("appendix_Id")));
		bean.setTicket_id(request.getParameter("ticket_id") == null ? bean
				.getTicket_id() : request.getParameter("ticket_id"));
		if (request.getParameter("appendix_Id") == null
				&& request.getParameter("appendixid") != null) {
			bean.setAppendixId(request.getParameter("appendixid"));
		}

		if (request.getAttribute("tabId") != null) {
			bean.setTabId((String) request.getAttribute("tabId"));
		}
	}

	public ActionForward POaction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		return mapping.findForward("success");
	}
}
