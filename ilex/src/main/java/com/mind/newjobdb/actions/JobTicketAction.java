package com.mind.newjobdb.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.PO.actions.CopyMpoToPoAction;
import com.mind.actions.PM.JobEditAction;
import com.mind.bean.newjobdb.JobDashboard;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.TicketTab;
import com.mind.bean.pm.SiteBean;
import com.mind.common.EnvironmentSelector;
import com.mind.common.LabelValue;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Email;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Appendixdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.AddContactdao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.Convertor;
import com.mind.newjobdb.dao.PurchaseOrderDAO;
import com.mind.newjobdb.dao.TicketDAO;
import com.mind.newjobdb.dao.TicketNatureDAO;
import com.mind.newjobdb.dao.TicketRequestInfoDAO;
import com.mind.newjobdb.dao.TicketScheduleDAO;
import com.mind.newjobdb.formbean.JobDashboardForm;
import com.mind.util.WebUtil;

public class JobTicketAction extends com.mind.common.IlexDispatchAction {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger
            .getLogger(JobTicketAction.class);

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {

        /* Page Security: Start */
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        if (loginuserid == null) {
            return (mapping.findForward("SessionExpire")); // - Check for
            // session expired.
        }
        if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
                "Job Dashboard - Ticket Tab",
                getDataSource(request, "ilexnewDB"))) {
            request.setAttribute("HidePage", "HidePage");
            // return( mapping.findForward( "UnAuthenticate" ) ); // - Check for
            // Pager Security.
        }
        /* Page Security=: End */

        JobDashboard jobDashboard = new JobDashboard();
        JobDashboardForm bean = (JobDashboardForm) form;
        TicketTab ticketTab = null;
        setJobSetUpBean(bean, request);
        if (bean.getAppendixId() == null || bean.getAppendixId().equals("0")) {
            String appendixId = IMdao.getAppendixId(bean.getJobId(),
                    getDataSource(request, "ilexnewDB"));
            bean.setAppendixId(appendixId);
        }

        String msaId = IMdao.getMSAId(bean.getAppendixId(),
                getDataSource(request, "ilexnewDB"));
        bean.setMsaId(msaId);

        String ticketType = bean.getTicketType();

        if (ticketType == null || ticketType.equals("")) {
            ticketType = request.getParameter("ticket_type");
        }

        bean.setTicketType(ticketType);
        if (bean.getTicketType() != null
                && bean.getTicketType().trim().equalsIgnoreCase("newTicket")) {
            setStatusOfTabs("newTicket", request);
            ticketTab = TicketDAO.createNewTicket(
                    new Long(bean.getAppendixId()).longValue(),
                    getDataSource(request, "ilexnewDB"));
        }

        if (bean.getTicketType() != null
                && bean.getTicketType().trim()
                .equalsIgnoreCase("existingTicket")) {
            setStatusOfTabs("existingTicket", request);
            String ticketId = request.getParameter("ticket_id");

            if (ticketId == null) {
                ticketId = (String) request.getAttribute("ticket_id");
            }

            bean.setTicket_id(ticketId);
            ticketTab = TicketDAO.getTicket(
                    new Long(bean.getAppendixId()).longValue(),
                    new Long(bean.getTicket_id()).longValue(),
                    getDataSource(request, "ilexnewDB"));
        }

        // ticketTab.getRequestNature().setTicketRules(TicketDAO.getRules(bean.getMsaId(),
        // getDataSource( request , "ilexnewDB" )));
        ticketTab.getRequestNature().setTicketRules(
                TicketDAO.getRules(bean.getAppendixId(),
                        getDataSource(request, "ilexnewDB")));
        fillControls(ticketTab, bean, request);
        jobDashboard.setJobDashboardTab(ticketTab);
        setMenu(bean, request, loginuserid);

        ticketTab.setWebTicket(bean.getWebTicket());
        JobDashboardBean dto = new JobDashboardBean();
        try {
            BeanUtils.copyProperties(dto, bean);
        } catch (IllegalAccessException e) {

            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {

            logger.error(e);
            e.printStackTrace();
        }
        Convertor.convertToJobDashboardForm(jobDashboard, dto);
        try {
            BeanUtils.copyProperties(bean, dto);
        } catch (IllegalAccessException e) {

            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {

            logger.error(e);
            e.printStackTrace();
        }
        try {
            BeanUtils.copyProperties(dto, bean);
        } catch (IllegalAccessException e) {

            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {

            logger.error(e);
            e.printStackTrace();
        }

        setSiteInfo(bean, getDataSource(request, "ilexnewDB"));
        request.setAttribute("tabId", request.getParameter("type"));

        request.setAttribute("api_url", EnvironmentSelector.getBundleString("api.url"));
        request.setAttribute("api_user", EnvironmentSelector.getBundleString("api.username"));
        request.setAttribute("api_pass", EnvironmentSelector.getBundleString("api.password"));

        setEditableTicket(bean, getDataSource(request, "ilexnewDB"));

        return mapping.findForward("success");
    }

    public ActionForward ticketSave(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {

        String[] pocdetails = new String[16];
        // String[] cnsPrjManangerDetail = new String[5];
        // String[] teamLeadDetail = new String[5];
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        String loginusername = (String) session.getAttribute("username");
        String loginUserEmail = (String) request.getSession(false).getAttribute("useremail");
        if (loginuserid == null) {
            return (mapping.findForward("SessionExpire")); // - Check for session expired.
        }
        JobDashboardForm bean = (JobDashboardForm) form;
        JobDashboardBean dto = new JobDashboardBean();
        ActionForward forward = new ActionForward();// return value
        forward = mapping.findForward("success");

        bean.setTabId(request.getParameter("type"));

        if (request.getParameter("webTicketType") != null) {
            bean.setWebTicket(request.getParameter("webTicketType"));
        }

        TicketTab ticketTab = null;
        try {
            BeanUtils.copyProperties(dto, bean);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }
        JobDashboard jobDashboard = Convertor.convertToJobDashboard(dto);
        try {
            BeanUtils.copyProperties(bean, dto);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }
        ticketTab = (TicketTab) jobDashboard.getJobDashboardTab();

        boolean newJob = false;
        // if(bean.getJobId() != null && bean.getJobId().trim().equals("0")){
        if ((bean.getJobId() != null && bean.getJobId().trim().equals("0"))
                || (bean.getWebTicket() != null && bean.getWebTicket().equals(
                        "webTicket"))) {
            newJob = true;
        }

        Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Ticket Action!", bean.getRequestorIds() + " || " + bean.getAdditionalRecipientIds());

        if (ticketTab.getRequestInfo().getRequestor().indexOf("~") > 0) {
            String[] parts = ticketTab.getRequestInfo().getRequestor().split("~");
            if (parts.length == 3) {
                ticketTab.getRequestInfo().setRequestor(parts[2]);
                ticketTab.getRequestInfo().setRequestorEmail(parts[1]);
                bean.setRequestor(parts[2]);
                bean.setRequestorEmail(parts[1]);
            }
        }

        String[] returnArray = TicketDAO.updateTicket(ticketTab, new Long(
                loginuserid).longValue(), getDataSource(request, "ilexnewDB"));
        bean.setJobId(returnArray[0]);
        bean.setTicket_id(returnArray[1]);
        bean.setReceivedDate(returnArray[2]);

        Jobdao.addUpdateRequestorEmail(Long.parseLong(bean.getJobId()), bean.getRequestorIds(), bean.getAdditionalRecipientIds(), getDataSource(request, "ilexnewDB"));

        if (bean.getJobId() != null && !bean.getJobId().equals("")) {
            bean.setTabId((JobDashboardTabType.SETUP_TAB + "").trim());
            forward = mapping.findForward("setuptab");
        } else {
            forward = mapping.findForward("success");
        }

        /* To copy Document and Deliverables into default PO from default MPO */
        if (bean.getJobId() != null && !bean.getJobId().equals("") && newJob) {
            // Add Default Deliverables to Default Purchase order
            String poId = PurchaseOrderDAO.getPoId(bean.getJobId(),
                    getDataSource(request, "ilexnewDB"));
            addDefaultDeliverables(loginuserid, poId,
                    getDataSource(request, "ilexnewDB"));

            JobEditAction.setWorkflowCheckListPlanningNote(
                    getDataSource(request, "ilexnewDB"), bean.getAppendixId(),
                    loginuserid, Long.valueOf(bean.getJobId()));
            CopyMpoToPoAction.copyToDefaultTicketPO(request, bean.getJobId(),
                    getDataSource(request, "ilexnewDB"));
            // Add function that getting conact information

            pocdetails = com.mind.dao.PRM.Appendixdao.AppendixpocdetailsInfo(
                    bean.getAppendixId(), getDataSource(request, "ilexnewDB"));
            bean.setPrimary_Name(pocdetails[0]);
            bean.setPocTitle(pocdetails[14]);
            bean.setPocAddress(pocdetails[13]);
            bean.setPocPhone(pocdetails[1]);
            bean.setPocEmail(pocdetails[12]);
            // DEFINE

            bean.setMsaId(IMdao.getMSAId(bean.getAppendixId(),
                    getDataSource(request, "ilexnewDB")));
            bean.setMsaName(Appendixdao.getMsaname(
                    getDataSource(request, "ilexnewDB"), bean.getMsaId()));

            // ticketTab.getRequestInfo().getRequestType();
            String companyname = bean.getMsaName();
            // if (LeadName != null) {
            try {
                String emailRecipients = Jobdao.getEmailRecipientsByJob(bean.getJobId(), getDataSource(request, "ilexnewDB"));

                sendEmail(
                        bean,
                        loginuserid,
                        loginusername,
                        request,
                        AddContactdao.getEscalationContacts(
                                bean.getAppendixId(),
                                getDataSource(request, "ilexnewDB")),
                        companyname, loginUserEmail, emailRecipients, getDataSource(request, "ilexnewDB"));
            } catch (Exception e) {
                Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Ticket Creation. An exception occurred", e.getMessage());
                logger.debug("sendEmail - Exception", e);
            }

        }

        fillControls(ticketTab, bean, request);
        setStatusOfTabs("existingTicket", request);
        bean.setTicketType("existingTicket");
        bean.setUpdateCompleteTicket(ticketTab.getUpdateCompleteTicket());
        bean.setWebTicket(ticketTab.getWebTicket());

        setMenu(bean, request, loginuserid);
        request.setAttribute("tabId", bean.getTabId());
        request.setAttribute("jobid", bean.getJobId());
        request.setAttribute("ticket_id", bean.getTicket_id());
        request.setAttribute("refershtree", "refershtree");
        return (forward);
    }

    private void sendEmail(JobDashboardForm bean, String userId, String userName, HttpServletRequest request,
            List<String[]> contacts, String companyname, String loginUserEmail, String emailRecipients, DataSource ds) {

        EmailBean emailform = new EmailBean();

        emailform.setUsername(this.getResources(request).getMessage(
                "common.Email.username"));
        emailform.setUserpassword(this.getResources(request).getMessage(
                "common.Email.userpassword"));

        emailform.setSmtpservername(EnvironmentSelector
                .getBundleString("common.Email.smtpservername"));
        emailform.setSmtpserverport(this.getResources(request).getMessage(
                "common.Email.smtpserverport"));

        emailform.setFrom(Emailtemplate.SENDER_EMAIL);
        emailform.setSubject("(" + bean.getJobId() + "-" + userId + ") - " + bean.getTicketNumber() + " - Ticket Creation");

        if (emailRecipients.equals("")) {
            emailRecipients = loginUserEmail;
        } else if (!emailRecipients.contains(loginUserEmail)) {
            emailRecipients = loginUserEmail + "," + emailRecipients;
        }
        emailform.setTo(emailRecipients);

        String mailBody = null;
        // Email field
        String requestor = bean.getRequestor();
        // String usernameticket;
        String delimiter;
        delimiter = "~";

        // String Note = null;

        /*
         * String OnSiteDate = null; String OffSiteDate = null; int z = 0;
         * String startdate = null; String actstart = null; String actend =
         * null;
         */
        String JobName = Jobdao.getJobname(bean.getJobId(), getDataSource(request, "ilexnewDB"));

        mailBody = Emailtemplate.buildJobEmailBody(getDataSource(request, "ilexnewDB"), userId, userName, bean.getMsaName(), JobName, bean.getJobId(), Emailtemplate.JobType.NETMEDX, Emailtemplate.EmailType.CREATION, contacts, bean);

        // //////////////////////Email Template code ////////////////
        // ///////////////////////////////////////////////////////////
        emailform.setContent(mailBody.toString());

        Map<String, String> inlineImages = new HashMap<String, String>();
        String filePath = System.getProperty("catalina.base");
        String filepath = filePath
                + "//webapps//Ilex//images//ampm_header_v2.png";
        // System.out.println("--File location is " + filePath);
        inlineImages.put("image1", filepath);

        Email.send(emailform, "text/html", mailBody.toString(), inlineImages, ds);
    }

    public void setStatusOfTabs(String tabStatus, HttpServletRequest request) {
        request.setAttribute("tabStatus", tabStatus);
        request.setAttribute("isTicket", "Ticket");
    }

    public void addDefaultDeliverables(String loginuserid, String poId,
            DataSource ds) {
        String[] defaultDeliverables = {"Before Work", "After Work",
            "Work Order", "Equipment Cabinet/Rack"};

        String[] defaultDeliverablesDescription = {
            "Picture of work area before work has begun.",
            "Picture of work area after work has been completed.",
            "Picture of work order, signed by the manager on duty.",
            "This photo should be close enough to read the brand name on the hardware but wide enough to capture the entire cabinet or concentration of hardware (even if it is in a pile or a nest)"};
        int deliverableCount = 0;
        for (String deliverable : defaultDeliverables) {
            PurchaseOrderDAO.insertDefaultPODeliverable(poId, deliverable,
                    defaultDeliverablesDescription[deliverableCount],
                    loginuserid, ds);
            deliverableCount++;
        }
    }

    public void setJobSetUpBean(JobDashboardForm bean,
            HttpServletRequest request) {
        // bean.setJobId((String)(request.getParameter("Job_Id")==null?request.getAttribute("jobid"):request.getParameter("Job_Id")));

        if (request.getParameter("jobid") != null) {
            bean.setJobId(request.getParameter("jobid"));
        }

        if (request.getAttribute("jobid") != null) {
            bean.setJobId((String) request.getAttribute("jobid"));
        }
        if (request.getParameter("Job_Id") != null) {
            bean.setJobId((String) request.getParameter("Job_Id"));
        }

        if (request.getParameter("webTicketType") != null
                && request.getParameter("webTicketType").equals("webTicket")) {
            bean.setWebTicket(request.getParameter("webTicketType"));
        }

        bean.setAppendixId((String) (request.getParameter("appendix_Id") == null ? bean
                .getAppendixId() : request.getParameter("appendix_Id")));
        bean.setTicket_id(request.getParameter("ticket_id") == null ? bean
                .getTicket_id() : request.getParameter("ticket_id"));

        if (request.getParameter("appendix_Id") == null
                && request.getParameter("appendixid") != null) {
            bean.setAppendixId(request.getParameter("appendixid"));
        }
    }

    private void fillControls(TicketTab ticketTab, JobDashboardForm bean,
            HttpServletRequest request) {

        Map<String, Object> map = new HashMap<String, Object>();

        bean.setRequestorList(TicketRequestInfoDAO.getRequestors(
                new Long(bean.getMsaId()).longValue(),
                getDataSource(request, "ilexnewDB")));
        bean.setRequestTypeList((TicketRequestInfoDAO
                .getRequestTypes(getDataSource(request, "ilexnewDB"))));
        bean.setAddRecipitant(TicketRequestInfoDAO.getClientEmailByType(
                bean.getAppendixId(), "NetMedX",
                getDataSource(request, "ilexnewDB")));

        bean.setCriticalityList(TicketRequestInfoDAO.getCriticalities(new Long(
                bean.getMsaId()).longValue(),
                getDataSource(request, "ilexnewDB")));
        if (!bean.getTicketType().trim().equalsIgnoreCase("newTicket")) {
            // bean.setResourceList(TicketRequestInfoDAO.getResources1(new
            // Long(bean.getAppendixId()).longValue(),
            // ticketTab.getRequestInfo().getCriticality(), true ,getDataSource(
            // request , "ilexnewDB" )));
            bean.setResourceList(TicketRequestInfoDAO.getResources(map,
                    new Long(bean.getAppendixId()).longValue(), ticketTab
                    .getRequestInfo().getCriticality(), true,
                    getDataSource(request, "ilexnewDB")));
            WebUtil.copyMapToRequest(request, map);
        }
        bean.setProblemCategoriesList(TicketNatureDAO
                .getProblemCategories(getDataSource(request, "ilexnewDB")));
        bean.setRequestedHourList(TicketScheduleDAO.getHours());
        bean.setRequestedMinuteList(TicketScheduleDAO.getMinutes());
        bean.setPreferredHourList(TicketScheduleDAO.getHours());
        bean.setPreferredMinuteList(TicketScheduleDAO.getMinutes());
        bean.setWindowFromHourList(TicketScheduleDAO.getHours());
        bean.setWindowFromMinuteList(TicketScheduleDAO.getMinutes());
        bean.setWindowToHourList(TicketScheduleDAO.getHours());
        bean.setWindowToMinuteList(TicketScheduleDAO.getMinutes());
    }

    public String fillCombo(long appendixId, String criticalityId, String pps,
            HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();

        StringBuffer buffer = new StringBuffer();
        ArrayList resourceList = TicketRequestInfoDAO.getResources(map,
                new Long(appendixId).longValue(), criticalityId, true, null);
        WebUtil.copyMapToRequest(request, map);
        // TicketRequestInfoDAO.getResources(new Long(appendixId).longValue(),
        // criticalityId, true, null);
        buffer.append(formatArrayList(resourceList, false, request));
        return buffer.toString();
    }

    private String formatArrayList(ArrayList list, boolean All_header,
            HttpServletRequest request) {
        StringBuffer response = new StringBuffer("");
        for (int i = 0; i < list.size(); i++) {
            LabelValue labelValue = (LabelValue) list.get(i);
            String id = labelValue.getValue();
            String name = labelValue.getLabel();
            if (i == 0 && All_header) {
                response.append("select");
            }
            response.append((i == 0 ? "" : ",") + "{ name:'" + escapeHTML(name)
                    + "', id:'" + id + "' }");

        }
        return response.toString();
    }

    public static final String escapeHTML(String s) {
        StringBuffer sb = new StringBuffer();
        int n = s.length();
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            switch (c) {
                case '<':
                    sb.append("'+_lt+'");
                    break;
                case '>':
                    sb.append("'+_gt+'");
                    break;
                case '&':
                    sb.append("'+_amp+'");
                    break;
                case '"':
                    sb.append("'+_quot+'");
                    break;
                case '\'':
                    sb.append("'+_apo+'");
                    break;
                case '\n':
                    sb.append("'+_newLine+'");
                    break;

                default:
                    sb.append(c);
                    break;
            }
        }
        return sb.toString();
    }

    private void setMenu(JobDashboardForm bean, HttpServletRequest request,
            String loginuserid) {
        Map<String, Object> map;
        if (bean.getTicketType() != null
                && bean.getTicketType().equalsIgnoreCase("newTicket")) {
            map = DatabaseUtilityDao.setNetMedXMenu(bean.getAppendixId(),
                    getDataSource(request, "ilexnewDB"));
            WebUtil.copyMapToRequest(request, map);
        } else {
            if (bean.getJobId() != null
                    && !bean.getJobId().trim().equalsIgnoreCase("")) {
                map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
                        getDataSource(request, "ilexnewDB"));
                WebUtil.copyMapToRequest(request, map);
            }
        }

    }

    private void setSiteInfo(JobDashboardForm bean, DataSource ds) {
        SiteBean siteForm = new SiteBean();
        siteForm.setId2(bean.getJobId());
        try {
            siteForm = Jobdao.getSite(siteForm, ds);
            bean.setSite(siteForm.getSite_number());
            bean.setSite_id(siteForm.getSite_Id());
            bean.setSitelistid(siteForm.getSitelistid());
        } catch (Exception e) {
            logger.error("setSiteInfo(JobDashboardForm, DataSource)", e);

            logger.error("setSiteInfo(JobDashboardForm, DataSource)", e);
        }
    }

    /**
     * Method used to check whether there exists one PO which is in not in
     * Draft/Cancelled state.
     *
     * @param bean
     * @param ds
     */
    public static void setEditableTicket(JobDashboardForm bean, DataSource ds) {
        boolean isEditable = TicketDAO.isPOAssignedTOJob(bean.getJobId(), ds);
        if (isEditable) {
            bean.setIsPOAssigned("Y");
        }
    }

    public ActionForward ResourceComboList(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        response.setHeader("pragma", "no-cache");// HTTP 1.1
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.addDateHeader("Expires", -1);
        response.setDateHeader("max-age", 0);
        response.setIntHeader("Expires", -1); // - prevents caching at the proxy
        // server
        response.addHeader("cache-Control", "private");
        String appendixId = "";
        String criticalityId = "";

        if (request.getParameter("appendixId") != null) {
            appendixId = request.getParameter("appendixId");
        }
        if (request.getParameter("criticalitiId") != null) {
            criticalityId = request.getParameter("criticalitiId");
        }
        Map<String, Object> map = new HashMap<String, Object>();

        TicketRequestInfoDAO.getResources(map,
                new Long(appendixId).longValue(), criticalityId, true,
                getDataSource(request, "ilexnewDB"));
        WebUtil.copyMapToRequest(request, map);
        request.setAttribute("fromJsp", "Ticket.jsp");
        response.setContentType("text/html");
        RequestDispatcher rd = request
                .getRequestDispatcher("/PRM/Job/DropDown.jsp"); // - inclued
        // JobReschedule.jsp
        rd.include(request, response);
        return null;
    }

}
