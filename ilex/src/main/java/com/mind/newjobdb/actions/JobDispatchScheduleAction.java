package com.mind.newjobdb.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.DynamicComboCNSWEB;
import com.mind.bean.newjobdb.DispatchScheduleTab;
import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobDashboard;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.TeamInfo;
import com.mind.common.EnvironmentSelector;
import com.mind.common.Util;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.Email;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.AddContactdao;
import com.mind.dao.PRM.AddInstallationNotesdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.DispatchScheduledao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.dao.PRM.Scheduledao;
import com.mind.dao.PRM.objects.ScheduleElement;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.Convertor;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.formbean.JobDashboardForm;
import com.mind.util.WebUtil;

public class JobDispatchScheduleAction extends com.mind.common.IlexDispatchAction {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(JobDispatchScheduleAction.class);

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        /* Page Security: Start */
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        if (loginuserid == null) {
            return (mapping.findForward("SessionExpire")); // - Check for session expired.
        }

        if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job", "Job Dashboard - Dispatch Schedule Tab", getDataSource(request, "ilexnewDB"))) {
            request.setAttribute("HidePage", "HidePage"); // - Check for Page Security.
        }
        /* Page Security=: End */

        String ticketId = JobDAO.getTicketId(request.getParameter("jobid"), getDataSource(request, "ilexnewDB"));
        if (ticketId != null && !ticketId.equals("")) {
            request.setAttribute("ticket_type", "existingTicket");
            request.setAttribute("ticket_id", ticketId);
            return (mapping.findForward("TicketDashboard"));
        }

        request.setAttribute("tabId", (request.getParameter("tabId") == null ? "" + JobDashboardTabType.DISPATCH_SCHEDULE_TAB : request.getParameter("tabId")));
        if (logger.isDebugEnabled()) {
            logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - param tab id" + request.getParameter("tabId"));
        }

        JobDashboardForm bean = (JobDashboardForm) form;
        bean.setTabId("" + JobDashboardTabType.DISPATCH_SCHEDULE_TAB);
        setGeneralFormInfo(bean, request);
        JobDashboardBean dto = new JobDashboardBean();
        fillCombo(bean, request);
        try {
            BeanUtils.copyProperties(dto, bean);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }

        setDispatchScheduleTabInfoInForm(dto, getDataSource(request, "ilexnewDB"));
        try {
            BeanUtils.copyProperties(bean, dto);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }
         //setJobDispatchScheduleInfo(bean, request, getDataSource(request, "ilexnewDB"));

        String jobType = JobSetUpDao.getjobType(bean.getJobId(), getDataSource(request, "ilexnewDB"));
        if (logger.isDebugEnabled()) {
            logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - job type---- " + jobType);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - JOB TYPE" + jobType + " job id " + bean.getJobId());
        }

        if (jobType != null && jobType.trim().equalsIgnoreCase("Default")) {
            jobType = "Default";
        } else if (jobType != null && jobType.trim().equalsIgnoreCase("Addendum")) {
            jobType = "Addendum";
        } else {
            jobType = "";
        }

        if (logger.isDebugEnabled()) {
            logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - bean.getOwner()" + bean.getOwner());
        }

        setStatusOfTabs(jobType, request, bean);
        Map<String, Object> map;

        // request.setAttribute("SAVE",(request.getParameter("save")==null?"":"Activity added successfully."));
        if (bean.getJobId() != null && !bean.getJobId().trim().equalsIgnoreCase("")) {
            map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid, getDataSource(request, "ilexnewDB"));
            WebUtil.copyMapToRequest(request, map);
        }
        return mapping.findForward("success");
    }

    public void setJobDispatchScheduleInfo(JobDashboardForm bean, HttpServletRequest request, DataSource ds) {
        /*ArrayList dispatchScheduleList = DispatchScheduledao.getDispatchScheduleList(bean.getJobId(), "J", getDataSource(request, "ilexnewDB"));
         request.setAttribute("dispatchScheduleList", dispatchScheduleList);
         request.setAttribute("listSize", dispatchScheduleList.size() + "");
         bean.setRownum(dispatchScheduleList.size() + "");
         bean.setDeletenum(dispatchScheduleList.size() + "");
         bean.setPage("");
         bean.setPageid(bean.getJobId());*/
    }

    public void setStatusOfTabs(String jobType, HttpServletRequest request, JobDashboardForm bean) {
        String ticketType = null;
        if (logger.isDebugEnabled()) {
            logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobggggggggggggggggg------- " + jobType);
        }
        // ticketType = request.getParameter("ticket_type");
        ticketType = bean.getTicketType();
        if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
            request.setAttribute("fromStatus", "fromStatus");
        }

        if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
            request.setAttribute("isTicket", "Ticket");
            request.setAttribute("tabStatus", "job");
        } else {
            if (jobType != null && !jobType.trim().equalsIgnoreCase("") && jobType.trim().equalsIgnoreCase("Default")) {
                request.setAttribute("tabStatus", "Default");
            }
            if (jobType != null && !jobType.trim().equalsIgnoreCase("") && jobType.trim().equalsIgnoreCase("Addendum")) {
                request.setAttribute("tabStatus", "Addendum");
            } else if (jobType != null && jobType.trim().equalsIgnoreCase("")) {
                if (logger.isDebugEnabled()) {
                    logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobggggggggggggggggg");
                }
                request.setAttribute("tabStatus", "job");
            }
            request.setAttribute("isTicket", "job");
        }
    }

    public void setDispatchScheduleTabInfoInForm(JobDashboardBean jobDashboardForm, DataSource ds) {
        DispatchScheduleTab DispatchScheduleTab = new DispatchScheduleTab();
        JobDashboard jobDashboard = new JobDashboard();
        JobDAO jobDao = new JobDAO();
        if (logger.isDebugEnabled()) {
            logger.debug("setDispatchScheduleTabInfoInForm(JobDashboardForm, DataSource) - appendix+++++++++" + jobDashboardForm.getAppendixId());
        }
        DispatchScheduleTab.setAppendixId(jobDashboardForm.getAppendixId());
        DispatchScheduleTab.setJobId(jobDashboardForm.getJobId());
        GeneralJobInfo generalJobInfo = jobDao.getGeneralJobInfo(Long.parseLong(jobDashboardForm.getJobId()), ds);
        DispatchScheduleTab.setGeneralJobInfo(generalJobInfo);

        jobDashboard.setJobDashboardTab(DispatchScheduleTab);
        Convertor.convertToJobDashboardForm(jobDashboard, jobDashboardForm);
        if (logger.isDebugEnabled()) {
            logger.debug("setDispatchScheduleTabInfoInForm(JobDashboardForm, DataSource) - COUNTRY " + jobDashboardForm.getSite_country() + " ," + jobDashboardForm.isUs());
        }
    }

    public void setGeneralFormInfo(JobDashboardForm bean, HttpServletRequest request) {
        bean.setTabId((request.getParameter("tabId") == null ? "" + JobDashboardTabType.DISPATCH_SCHEDULE_TAB : request.getParameter("tabId")));
        String loginUserName = (String) request.getSession().getAttribute("username");
        bean.setCreatedBy(loginUserName);
        bean.setJobId((String) (request.getParameter("Job_Id") == null ? request.getAttribute("jobid") : request.getParameter("Job_Id")));
        // To open general job Dashboard.
        if (request.getParameter("jobid") != null) {
            bean.setJobId(request.getParameter("jobid"));
        }
        if (request.getAttribute("jobid") != null) {
            bean.setJobId((String) request.getAttribute("jobid"));
        }
        JobDAO jobDAO = new JobDAO();
        TeamInfo teamInfo = jobDAO.getTeamInfo(new Long(bean.getJobId()), getDataSource(request, "ilexnewDB"));

        bean.setOwner(teamInfo.getOwner());

        bean.setAppendixId((String) (request.getParameter("appendix_Id") == null ? bean.getAppendixId() : request.getParameter("appendix_Id")));
        bean.setTicket_id(request.getParameter("ticket_id") == null ? bean.getTicket_id() : request.getParameter("ticket_id"));
        if (request.getParameter("appendix_Id") == null && request.getParameter("appendixid") != null) {
            bean.setAppendixId(request.getParameter("appendixid"));
        }

        if (request.getAttribute("tabId") != null) {
            bean.setTabId((String) request.getAttribute("tabId"));
        }
    }

    public void fillCombo(JobDashboardForm bean, HttpServletRequest request) {
        DynamicComboCNSWEB dynamiccomboCNSWEB = new DynamicComboCNSWEB();
        dynamiccomboCNSWEB.setHourname(Util.getHours());
        dynamiccomboCNSWEB.setMinutename(Util.getMinutes());
        dynamiccomboCNSWEB.setOptionname(DynamicComboDao.getOptionName());
        request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);
        request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);
    }

    public ActionForward POaction(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        return mapping.findForward("success");
    }

    public ActionForward Save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        JobDashboardForm bean = (JobDashboardForm) form;
        // JobDashboardBean dto = new JobDashboardBean();
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        String loginusername = (String) session.getAttribute("username");
        String loginUserEmail = (String) session.getAttribute("useremail");

        String[] requestorDetails = JobSetUpDao.getRequestorDetailsByTicketId(bean.getJobId(), getDataSource(request, "ilexnewDB"));

        if (requestorDetails != null) {
            bean.setTicket_id(requestorDetails[0]);
            bean.setTicketNumber(requestorDetails[1]);
            bean.setRequestor(requestorDetails[2]);
            bean.setRequestorEmail(requestorDetails[3]);
        }

        boolean isMarkOnsite = Scheduledao.isAccepted();
        if (logger.isDebugEnabled()) {
            logger.debug("Save(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - inMarkOnsite= "
                    + isMarkOnsite);
        }
        if (isMarkOnsite) {
            int i = 0;
            int status = 0;
            String mailBody = new String();
            ScheduleElement[] prevs = new ScheduleElement[bean.getStartdate().length];
            for (i = 0; i < bean.getStartdate().length; i++) {
                int z = i + 1;
                prevs[i] = Scheduledao.getScheduleByDispatchId(bean.getJobId(), z, getDataSource(request, "ilexnewDB"));
            }

            for (i = 0; i < bean.getStartdate().length; i++) {
                int z = i + 1;

                if (bean.getScheduleNumber(z).equals("")) {
                    bean.setScheduleNumber("0", z);
                }

                if (!bean.getActstartdatehh().equals("  ")
                        && bean.getActstartdatemm(i).equals("  ")) {
                    bean.setActstartdatemm("00", i);
                }

                if (!bean.getActenddatehh().equals("  ")
                        && bean.getActenddatemm(i).equals("  ")) {
                    bean.setActenddatemm("00", i);
                }

                if (!bean.getStartdatehh(i).equals("  ")
                        && bean.getStartdatemm(i).equals("  ")) {
                    bean.setStartdatemm("00", i);
                }

                String actStart = bean.getActstartdate(i);
                if (!bean.getActstartdatehh(i).equals("  ")) {
                    actStart += " " + bean.getActstartdatehh(i) + ":" + bean.getActstartdatemm(i) + " " + bean.getActstartdateop(i);
                }
                bean.setActstartdate(actStart, i);

                String actEnd = bean.getActenddate(i);
                if (!bean.getActenddatehh(i).equals("  ")) {
                    actEnd += " " + bean.getActenddatehh(i) + ":" + bean.getActenddatemm(i) + " " + bean.getActenddateop(i);
                }
                bean.setActenddate(actEnd, i);

                String startDate = bean.getStartdate(i);
                if (!bean.getStartdatehh(i).equals("  ")) {
                    startDate += " " + bean.getStartdatehh(i) + ":" + bean.getStartdatemm(i) + " " + bean.getStartdateop(i);
                }

                if (actStart.equals(" : ")) {
                    actStart = "";
                }

                if (actEnd.equals(" : ")) {
                    actEnd = "";
                }

                if (startDate.equals(" : ")) {
                    startDate = "";
                }

                if (bean.getActstartdate(i) == null || bean.getActstartdate(i).equals("")) {
                    actStart = "";
                }
                if (bean.getActenddate(i) == null || bean.getActenddate(i).equals("")) {
                    actEnd = "";
                }
                if (bean.getStartdate(i) == null || bean.getStartdate(i).equals("")) {
                    startDate = "";
                }

                if (bean.getScheduleid() == null) {
                    bean.setScheduleid("0");
                }

                int retValArray[] = new int[2];
                retValArray = DispatchScheduledao.addScheduleForPRM(bean.getScheduleNumber(z), bean.getJobId(), actStart, actEnd, "J", loginuserid, startDate, i, getDataSource(request, "ilexnewDB"));
                status = retValArray[0];
                String newScheduleId = String.valueOf(retValArray[1]);

                /* Add Install Notes: Start */
                if (bean.getActstartdate(i) != null && bean.getActstartdate(i).length() > 9) {
                    String defaultset = "Tech onsite for dispatch " + (z);
                    AddInstallationNotesdao.addinstallnotes(bean.getJobId(), defaultset, loginuserid, getDataSource(request, "ilexnewDB"), bean.getActstartdate(i), bean.getScheduleNumber(z), newScheduleId, "S");
                }
                if (bean.getActenddate(i) != null && bean.getActenddate(i).length() > 9) {
                    String defaultset = "Tech offsite for dispatch " + (z);
                    AddInstallationNotesdao.addinstallnotes(bean.getJobId(), defaultset, loginuserid, getDataSource(request, "ilexnewDB"), bean.getActenddate(i), bean.getScheduleNumber(z), newScheduleId, "E");
                }
                /* Add Install Notes: Start */
                bean.setStartDate(startDate);

                // ////////////////////////
                // getting deatail info
                String[] pocdetails = new String[16];
                // String[] cnsPrjManangerDetail = new String[5];
                // String[] teamLeadDetail = new String[5];
                pocdetails = com.mind.dao.PRM.Appendixdao.AppendixpocdetailsInfo(bean.getAppendixId(), getDataSource(request, "ilexnewDB"));
                bean.setPrimary_Name(pocdetails[0]);
                bean.setPocTitle(pocdetails[14]);
                bean.setPocAddress(pocdetails[13]);
                bean.setPocPhone(pocdetails[1]);
                bean.setPocEmail(pocdetails[12]);

                // getting company name
                bean.setMsaId(IMdao.getMSAId(bean.getAppendixId(), getDataSource(request, "ilexnewDB")));
                bean.setMsaName(Appendixdao.getMsaname(bean.getAppendixId(), getDataSource(request, "ilexnewDB")));

                // getMsaname(
                // getDataSource(request, "ilexnewDB"), bean.getMsaId()));
                String companyName = bean.getMsaName();
                String jobName = Jobdao.getJobname(bean.getJobId(), getDataSource(request, "ilexnewDB"));

                // ////////////////////////////////
                try {
                    ScheduleElement se = Scheduledao.getScheduleByDispatchId(bean.getJobId(), z, getDataSource(request, "ilexnewDB"));

                    Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Handling change to dispatch " + z, "");
                    Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Dispatch Changes - Prev", "" + (prevs[i] != null));
                    if (prevs[i] != null) {
                        Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Dispatch Changes - " + prevs[i].getActualStart() + " != " + se.getActualStart(), "" + (!se.getActualStart().equals(prevs[i].getActualStart())));
                        Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Dispatch Changes - " + prevs[i].getActualEnd() + " != " + se.getActualEnd(), "" + (!se.getActualEnd().equals(prevs[i].getActualEnd())));
                        Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Dispatch Changes - " + prevs[i].getPlannedStart() + " != " + se.getPlannedStart(), "" + (!se.getPlannedStart().equals(prevs[i].getPlannedStart())));
                        Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Dispatch Changes - " + prevs[i].getPlannedEnd() + " != " + se.getPlannedEnd(), "" + (!se.getPlannedEnd().equals(prevs[i].getPlannedEnd())));
                    }

                    if (prevs[i] == null || !se.getActualStart().equals(prevs[i].getActualStart()) || !se.getActualEnd().equals(prevs[i].getActualEnd())
                            || !se.getPlannedStart().equals(prevs[i].getPlannedStart()) || !se.getPlannedEnd().equals(prevs[i].getPlannedEnd())) {
                        mailBody = Emailtemplate.buildJobEmailBody_DispatchScheduled(getDataSource(request, "ilexnewDB"), loginuserid, loginusername, companyName, jobName, bean.getJobId(), Emailtemplate.JobType.NETMEDX, AddContactdao.getEscalationContacts(bean.getAppendixId(), getDataSource(request, "ilexnewDB")), se);

                        String mailSubject = "(" + bean.getJobId() + "-" + loginuserid + ") - " + jobName + " - Scheduling Update - Dispatch " + z;

                        if (se != null) {
                            bean.setIsSendEmail("true");
                            if (Boolean.valueOf(bean.getIsSendEmail())) {
                                sendEmail(bean, mailSubject, loginuserid, mailBody.toString(), request, loginUserEmail, getDataSource(request, "ilexnewDB"));
                            }
                        }
                    }
                } catch (Exception e) {
                    Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "JobDispatchScheduleAction.java. An exception occurred", e.getMessage());
                    logger.debug("buildJobEmailBody_DispatchScheduled - Exception", e);
                }

            }
        }// - end of if(inMarkOnsite)

        return mapping.findForward("save");
    }

    private void sendEmail(JobDashboardForm bean, String mailSubject, String userId, String mailBody, HttpServletRequest request, String loginUserEmail, DataSource ds) {
        /*
         EmailBean emailform = new EmailBean();

         emailform.setUsername(this.getResources(request).getMessage(
         "common.Email.username"));
         emailform.setUserpassword(this.getResources(request).getMessage(
         "common.Email.userpassword"));

         emailform.setSmtpservername(EnvironmentSelector
         .getBundleString("common.Email.smtpservername"));
         emailform.setSmtpserverport(this.getResources(request).getMessage(
         "common.Email.smtpserverport"));

         emailform.setFrom(Emailtemplate.SENDER_EMAIL);

         String emailRecipients = Jobdao.getEmailRecipientsByJob(bean.getJobId(), getDataSource(request, "ilexnewDB"));
         if (emailRecipients.equals("")) {
         emailRecipients = loginUserEmail;
         } else if (!emailRecipients.contains(loginUserEmail)) {
         emailRecipients = loginUserEmail + "," + emailRecipients;
         }
         emailform.setTo(emailRecipients);

         emailform.setSubject(mailSubject);

         emailform.setContent(mailBody);

         Map<String, String> inlineImages = new HashMap<String, String>();
         String filePath = System.getProperty("catalina.base");
         String filepath = filePath
         + "//webapps//Ilex//images//ampm_header_v2.png";
         // System.out.println("--File location is " + filePath);
         inlineImages.put("image1", filepath);

         Email.send(emailform, "text/html", mailBody.toString(), inlineImages, ds);*/
    }

    public ActionForward Delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        /*JobDashboardForm bean = (JobDashboardForm) form;
         if (request.getParameter("scheduleId") != null && !request.getParameter("scheduleId").equals("")) {
         DispatchScheduledao.deleteDispatchSchedule(bean.getJobId(), "J", request.getParameter("scheduleId"), getDataSource(request, "ilexnewDB"));
         }*/
        return mapping.findForward("delete");
    }
}
