package com.mind.newjobdb.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.newjobdb.ChangeTicketRequestTypeBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.dao.TicketRequestInfoDAO;
import com.mind.newjobdb.formbean.ChangeTicketRequestTypeForm;
import com.mind.util.WebUtil;

public class ChangeTicketRequestTypeAction extends com.mind.common.IlexAction {
	private static final Logger logger = Logger
			.getLogger(ChangeTicketRequestTypeAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse responce) {

		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
				"Change Request Type", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
															// Security.
		}
		/* Page Security=: End */

		ChangeTicketRequestTypeForm changeTicketResource = (ChangeTicketRequestTypeForm) form;

		ChangeTicketRequestTypeBean changeTicketRequestTypeBean = new ChangeTicketRequestTypeBean();

		if (changeTicketResource.getSaveRequestType() != null) {
			int result = TicketRequestInfoDAO.updateRequestType(
					changeTicketResource.getRequestType(),
					changeTicketResource.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("result", result);
		}
		Map<String, Object> map;

		ChangeTicketRequestTypeAction.setGeneralFormInfo(changeTicketResource,
				request);
		changeTicketResource.setRequestTypeCombo(TicketRequestInfoDAO
				.getRequestTypes(getDataSource(request, "ilexnewDB")));
		map = DatabaseUtilityDao.setMenu(changeTicketResource.getJobId(),
				loginuserid, getDataSource(request, "ilexnewDB"));
		WebUtil.copyMapToRequest(request, map);
		try {
			BeanUtils.copyProperties(changeTicketRequestTypeBean,
					changeTicketResource);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		TicketRequestInfoDAO.getRequestType(changeTicketResource.getJobId(),
				changeTicketRequestTypeBean,
				getDataSource(request, "ilexnewDB"));

		try {
			BeanUtils.copyProperties(changeTicketResource,
					changeTicketRequestTypeBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}

		return mapping.findForward("success");

	}

	private static void setGeneralFormInfo(
			ChangeTicketRequestTypeForm changeTicketResource,
			HttpServletRequest request) {

		if (request.getParameter("msaId") != null)
			changeTicketResource.setMsaId(request.getParameter("msaId"));

		if (request.getParameter("Job_Id") != null)
			changeTicketResource.setJobId(request.getParameter("Job_Id"));
	}
}
