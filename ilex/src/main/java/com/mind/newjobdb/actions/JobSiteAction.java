package com.mind.newjobdb.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobDashboard;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.TeamInfo;
import com.mind.bean.pm.SiteBean;
import com.mind.common.LabelValue;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.Convertor;
import com.mind.newjobdb.business.SiteTab;
import com.mind.newjobdb.business.SiteTabOrganizer;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.formbean.JobDashboardForm;
import com.mind.util.WebUtil;

public class JobSiteAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JobSiteAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao
				.getPageSecurity(loginuserid, "Manage Job",
						"Job Dashboard - Site Tab",
						getDataSource(request, "ilexnewDB"))) {
			request.setAttribute("HidePage", "HidePage");
			// return( mapping.findForward( "UnAuthenticate" ) ); // - Check for
			// Pager Security.
		}
		/* Page Security=: End */

		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - in JobSiteAction");
		}
		request.setAttribute(
				"tabId",
				(request.getParameter("tabId") == null ? ""
						+ JobDashboardTabType.SITE_TAB : request
						.getParameter("tabId")));
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - param tab id"
					+ request.getParameter("tabId"));
		}
		JobDashboardForm bean = (JobDashboardForm) form;
		JobDashboardBean dto = new JobDashboardBean();

		SiteTab siteTab = new SiteTab();
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		SiteTabOrganizer.setSiteTabFromForm(siteTab, dto);
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		SiteBean siteForm = siteTab.getSiteForm();
		if (request.getParameter("update") != null
				&& request.getParameter("update").equalsIgnoreCase("update")) {
			try {
				if (logger.isDebugEnabled()) {
					logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - inside update");
				}
				// String LatLong[] =
				// RestClient.getLatitudeLongitude(siteForm.getSite_address(),siteForm.getSite_city(),siteForm.getState(),siteForm.getCountry(),siteForm.getSite_zipcode(),getDataSource(request,
				// "ilexnewDB"));
				Jobdao.updateSite(siteForm, loginuserid,
						getDataSource(request, "ilexnewDB"));
			} catch (Exception e) {
				logger.error(
						"unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);

				logger.error(
						"unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
						e);
			}

		}
		int siteId = 0;
		int jobSiteId = 0;
		if (request.getParameter("add") != null
				&& request.getParameter("add").equalsIgnoreCase("add")) {
			if (request.getParameter("sitelistid") != null
					&& !request.getParameter("sitelistid").equals(""))
				siteId = Integer.parseInt(request.getParameter("sitelistid"));

			if (request.getParameter("siteId") != null
					&& !request.getParameter("siteId").equals("")) {
				jobSiteId = Integer.parseInt(request.getParameter("siteId"));
			}
			if (logger.isDebugEnabled()) {
				logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - siteId112233:: "
						+ siteId + "::::jobSiteId: " + jobSiteId);
			}
			JobDashboarddao.addNewSite(siteId,
					Integer.parseInt(bean.getJobId()), loginuserid,
					getDataSource(request, "ilexnewDB"), jobSiteId);
		}

		bean.setSite_id(JobDAO.getSiteId(bean.getJobId(),
				getDataSource(request, "ilexnewDB")));
		bean.setMsaId(IMdao.getMSAId(bean.getAppendixId(),
				getDataSource(request, "ilexnewDB")));
		bean.setMsaName(com.mind.dao.PM.Appendixdao.getMsaname(
				getDataSource(request, "ilexnewDB"),
				IMdao.getMSAId(bean.getAppendixId(),
						getDataSource(request, "ilexnewDB"))));

		populateCombo(request, bean.getMsaId(), bean.getMsaName(),
				getDataSource(request, "ilexnewDB"));

		bean.setTabId("" + JobDashboardTabType.SITE_TAB);

		setGeneralFormInfo(bean, request);
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		setSiteTabInfoInForm(dto, request, getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		String jobType = JobSetUpDao.getjobType(bean.getJobId(),
				getDataSource(request, "ilexnewDB"));
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - job type---- "
					+ jobType);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - JOB TYPE"
					+ jobType + " job id " + bean.getJobId());
		}
		if (jobType != null
				&& (jobType.equalsIgnoreCase("Default") || jobType
						.equalsIgnoreCase("Addendum")))
			jobType = "Default";
		else
			jobType = "";
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - bean.getOwner()"
					+ bean.getOwner());
		}

		setStatusOfTabs(jobType, request, bean);
		Map<String, Object> map;

		// request.setAttribute("SAVE",(request.getParameter("save")==null?"":"Activity added successfully."));
		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		return mapping.findForward("success");
	}

	public void setStatusOfTabs(String jobType, HttpServletRequest request,
			JobDashboardForm bean) {
		String ticketType = null;
		if (logger.isDebugEnabled()) {
			logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobggggggggggggggggg------- "
					+ jobType);
		}
		// ticketType = request.getParameter("ticket_type");
		ticketType = bean.getTicketType();
		if (ticketType != null && !ticketType.trim().equalsIgnoreCase(""))
			request.setAttribute("fromStatus", "fromStatus");
		if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
			request.setAttribute("isTicket", "Ticket");
			request.setAttribute("tabStatus", "job");
		} else {
			if (jobType != null && !jobType.trim().equalsIgnoreCase("")
					&& jobType.trim().equalsIgnoreCase("Default"))
				request.setAttribute("tabStatus", "Default");
			else if (jobType != null && jobType.trim().equalsIgnoreCase("")) {
				if (logger.isDebugEnabled()) {
					logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobggggggggggggggggg");
				}
				request.setAttribute("tabStatus", "job");
			}
			request.setAttribute("isTicket", "job");
		}
		// request.setAttribute("tabStatus",jobType);
	}

	public void setSiteTabInfoInForm(JobDashboardBean jobDashboardForm,
			HttpServletRequest request, DataSource ds) {
		SiteTab SiteTab = new SiteTab();
		JobDashboard jobDashboard = new JobDashboard();

		JobDAO jobDao = new JobDAO();
		if (logger.isDebugEnabled()) {
			logger.debug("setSiteTabInfoInForm(JobDashboardForm, HttpServletRequest, DataSource) - appendix+++++++++"
					+ jobDashboardForm.getAppendixId());
		}
		SiteTab.setAppendixId(jobDashboardForm.getAppendixId());
		SiteTab.setJobId(jobDashboardForm.getJobId());
		GeneralJobInfo generalJobInfo = jobDao.getGeneralJobInfo(
				Long.parseLong(jobDashboardForm.getJobId()), ds);
		SiteTab.setGeneralJobInfo(generalJobInfo);
		SiteBean siteForm = new SiteBean();
		siteForm.setId2(jobDashboardForm.getJobId());
		try {
			siteForm = Jobdao.getSite(siteForm, ds);
		} catch (Exception e) {
			logger.error(
					"setSiteTabInfoInForm(JobDashboardForm, HttpServletRequest, DataSource)",
					e);

			logger.error(
					"setSiteTabInfoInForm(JobDashboardForm, HttpServletRequest, DataSource)",
					e);
		}

		SiteTab.setSiteForm(siteForm);
		// JobPlanningNote jobPlanningNote =
		// (JobPlanningNote)jobDao.getJobPlanningNotes(Long.parseLong(jobDashboardForm.getJobId()),ds);
		// SiteTab.setJobPlanningNote(jobPlanningNote);

		// boolean showEvenIfNoPending=true;
		// JobWorkflowChecklist topPendingCheckListItems =
		// ((JobWorkflowChecklist) JobLevelWorkflowDAO.getJobWorkflowChecklist
		// (Long.parseLong(jobDashboardForm.getJobId()),ds)).getTopPendingItems(2,
		// showEvenIfNoPending);

		// SiteTab.setTopPendingCheckListItems(topPendingCheckListItems);

		// ArrayList<PurchaseOrder> listOfPurchaseOrders=
		// PurchaseOrderDAO.getPurchaseOrderList(jobDashboardForm.getJobId(),ds);
		// PurchaseOrderList purchaseOrderList = new PurchaseOrderList();
		// purchaseOrderList.setPurchaseOrders(listOfPurchaseOrders);
		// SiteTab.setPurchaseOrderList(purchaseOrderList);
		// CostVariations totalCostVariations =
		// PurchaseOrderList.getTotalCostVariations(purchaseOrderList);
		// SiteTab.setCostVariations(totalCostVariations);
		jobDashboard.setJobDashboardTab(SiteTab);
		Convertor.convertToJobDashboardForm(jobDashboard, jobDashboardForm);
		if (logger.isDebugEnabled()) {
			logger.debug("setSiteTabInfoInForm(JobDashboardForm, HttpServletRequest, DataSource) - COUNTRY "
					+ jobDashboardForm.getSite_country()
					+ " ,"
					+ jobDashboardForm.isUs());
		}
	}

	// public ActionForward addComments (ActionMapping mapping , ActionForm form
	// , HttpServletRequest request , HttpServletResponse response)
	// {
	// response.setHeader("Pragma","No-Cache");
	// response.setHeader("Cache-Control","no-cache");
	// response.setHeader("Cache-Control","no-store");
	// response.setDateHeader("Expires",0);
	// System.out.println(request.getParameter("jobId"));
	// String jobId = request.getParameter("jobId");
	// String comments = request.getParameter("comments");
	// HttpSession session = request.getSession( true );
	// String loginuserid = ( String ) session.getAttribute( "userid" );
	// try{
	//
	// JobDAO.addJobComments(comments, jobId, loginuserid, getDataSource(
	// request , "ilexnewDB" ));
	//
	// }catch(Exception e){
	// e.printStackTrace();
	// }
	// return null;
	// }

	public void setGeneralFormInfo(JobDashboardForm bean,
			HttpServletRequest request) {
		bean.setTabId((request.getParameter("tabId") == null ? ""
				+ JobDashboardTabType.SITE_TAB : request.getParameter("tabId")));
		String loginUserName = (String) request.getSession().getAttribute(
				"username");
		bean.setCreatedBy(loginUserName);
		bean.setJobId((String) (request.getParameter("Job_Id") == null ? request
				.getAttribute("jobid") : request.getParameter("Job_Id")));
		// To open general job Dashboard.
		if (request.getParameter("jobid") != null) {
			bean.setJobId(request.getParameter("jobid"));
		}
		if (request.getAttribute("jobid") != null) {
			bean.setJobId((String) request.getAttribute("jobid"));
		}
		JobDAO jobDAO = new JobDAO();
		TeamInfo teamInfo = jobDAO.getTeamInfo(new Long(bean.getJobId()),
				getDataSource(request, "ilexnewDB"));

		bean.setOwner(teamInfo.getOwner());

		bean.setAppendixId((String) (request.getParameter("appendix_Id") == null ? bean
				.getAppendixId() : request.getParameter("appendix_Id")));
		bean.setTicket_id(request.getParameter("ticket_id") == null ? bean
				.getTicket_id() : request.getParameter("ticket_id"));
		if (request.getParameter("appendix_Id") == null
				&& request.getParameter("appendixid") != null)
			bean.setAppendixId(request.getParameter("appendixid"));

		if (request.getAttribute("tabId") != null) {
			bean.setTabId((String) request.getAttribute("tabId"));
		}
	}

	public void populateCombo(HttpServletRequest request, String msaId,
			String msaName, DataSource ds) {
		ArrayList<LabelValue> stateList = new ArrayList<LabelValue>();
		stateList = Pvsdao.getStateList(ds);

		ArrayList<LabelValue> countryList = new ArrayList<LabelValue>();
		countryList = (ArrayList<LabelValue>) DynamicComboDao
				.getCountryList(ds);

		ArrayList<LabelValue> endCustomersList = new ArrayList<LabelValue>();
		if (logger.isDebugEnabled()) {
			logger.debug("populateCombo(HttpServletRequest, String, String, DataSource) - msaId,msaName--"
					+ msaId + "---" + msaName);
		}
		endCustomersList = (ArrayList<LabelValue>) DynamicComboDao
				.getEndCustomerList(ds, msaId, msaName);

		ArrayList<LabelValue> timezoneList = new ArrayList<LabelValue>();
		timezoneList = (ArrayList<LabelValue>) DynamicComboDao.gettimezone(ds);

		ArrayList<LabelValue> brandList = new ArrayList<LabelValue>();
		brandList = (ArrayList<LabelValue>) DynamicComboDao
				.getGroupNameListSiteSearch(msaId, ds);

		request.setAttribute("stateList", stateList);
		request.setAttribute("countryList", countryList);

		request.setAttribute("endCustomersList", endCustomersList);
		request.setAttribute("timezoneList", timezoneList);

		request.setAttribute("brandList", brandList);

	}

	public ActionForward POaction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		return mapping.findForward("success");
	}
}
