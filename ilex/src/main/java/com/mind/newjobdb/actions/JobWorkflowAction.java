package com.mind.newjobdb.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.newjobdb.ChecklistStatusType;
import com.mind.bean.newjobdb.JobWorkflowBean;
import com.mind.bean.newjobdb.JobWorkflowItem;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.Convertor;
import com.mind.newjobdb.dao.JobLevelWorkflowDAO;
import com.mind.newjobdb.dao.JobWorkflowChecklist;
import com.mind.newjobdb.formbean.JobWorkflowForm;
import com.mind.util.WebUtil;

public class JobWorkflowAction extends com.mind.common.IlexDispatchAction {
	private static final Logger logger = Logger
			.getLogger(JobWorkflowAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		JobWorkflowForm JobWorkflowForm = (JobWorkflowForm) form;
		JobWorkflowBean jobWorkflowBean = new JobWorkflowBean();
		DataSource ds = getDataSource(request, "ilexnewDB");
		String loginuserid = (String) request.getSession(false).getAttribute(
				"userid");
		JobWorkflowChecklist jobWorkflowChecklist = (JobWorkflowChecklist) JobLevelWorkflowDAO
				.getJobWorkflowChecklist(
						Long.parseLong(JobWorkflowForm.getJobId()), ds);
		try {
			BeanUtils.copyProperties(jobWorkflowBean, JobWorkflowForm);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		Convertor.convertJobWorkflowChecklistToJobWorkFlowForm(
				jobWorkflowChecklist, jobWorkflowBean);
		try {
			BeanUtils.copyProperties(JobWorkflowForm, jobWorkflowBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		Map<String, Object> map;

		if (JobWorkflowForm.getJobId() != null
				&& !JobWorkflowForm.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(JobWorkflowForm.getJobId(),
					loginuserid, getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		return mapping.findForward("Workflow");
	}

	public ActionForward manipulateCheckList(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String mode = request.getParameter("mode");
		DataSource ds = getDataSource(request, "ilexnewDB");
		JobWorkflowForm jobWorkflowBean = (JobWorkflowForm) form;
		JobWorkflowBean jobWorkflowDTO = new JobWorkflowBean();
		String loginuserid = (String) request.getSession(false).getAttribute(
				"userid");
		try {
			BeanUtils.copyProperties(jobWorkflowDTO, jobWorkflowBean);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		JobWorkflowChecklist jobWorkflowChecklist = Convertor
				.convertToJobWorkflowChecklist(jobWorkflowDTO);
		try {
			BeanUtils.copyProperties(jobWorkflowBean, jobWorkflowDTO);
		} catch (IllegalAccessException e1) {
			logger.error(e1);
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			logger.error(e1);
			e1.printStackTrace();
		}
		JobWorkflowItem lastCompleteItem = findLastCompleteItem(jobWorkflowChecklist);
		for (int i = 0; i < jobWorkflowChecklist.getJobWorkflowItems().size(); i++) {
			JobWorkflowItem jobWorkflowItem = jobWorkflowChecklist
					.getJobWorkflowItems().get(i);
			if (mode != null && mode.trim().equalsIgnoreCase("skipped")) {
				if (jobWorkflowItem.getItemStatusCode() == ChecklistStatusType.PENDING_STATUS) {
					JobWorkflowChecklist.setNewlySkippedItem(jobWorkflowItem,
							ds, loginuserid);
					break;
				}
			} else if (mode != null && mode.trim().equalsIgnoreCase("undo")) {
				JobWorkflowChecklist.undoLastAction(lastCompleteItem, ds,
						loginuserid);
				break;
			} else if (mode != null
					&& mode.trim().equalsIgnoreCase("notApplicable")) {
				if (jobWorkflowItem.getItemStatusCode() == ChecklistStatusType.PENDING_STATUS) {
					JobWorkflowChecklist.setNewlyNotApplicableItem(
							jobWorkflowItem, ds, loginuserid);
					break;
				}
			} else if (mode != null
					&& mode.trim().equalsIgnoreCase("onComplete")) {
				if (jobWorkflowItem.getItemStatusCode() == ChecklistStatusType.PENDING_STATUS) {
					JobWorkflowChecklist.setNewlyCompletedItem(jobWorkflowItem,
							ds, loginuserid);
					break;
				}
			}

		}
		Map<String, Object> map;

		JobWorkflowChecklist jobWorkflowChecklist1 = (JobWorkflowChecklist) JobLevelWorkflowDAO
				.getJobWorkflowChecklist(
						Long.parseLong(jobWorkflowBean.getJobId()), ds);
		try {
			BeanUtils.copyProperties(jobWorkflowDTO, jobWorkflowBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		Convertor.convertJobWorkflowChecklistToJobWorkFlowForm(
				jobWorkflowChecklist1, jobWorkflowDTO);
		try {
			BeanUtils.copyProperties(jobWorkflowBean, jobWorkflowDTO);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		map = DatabaseUtilityDao.setMenu(jobWorkflowBean.getJobId(),
				loginuserid, ds);
		WebUtil.copyMapToRequest(request, map);
		return mapping.findForward("Workflow");
	}

	private JobWorkflowItem findLastCompleteItem(
			JobWorkflowChecklist jobWorkflowChecklist) {
		ArrayList<JobWorkflowItem> completeList = new ArrayList<JobWorkflowItem>();
		for (int i = 0; i < jobWorkflowChecklist.getJobWorkflowItems().size(); i++) {
			JobWorkflowItem jobWorkflowItem = jobWorkflowChecklist
					.getJobWorkflowItems().get(i);
			if (jobWorkflowItem.isCompletedType()) {
				completeList.add(jobWorkflowItem);
			} else
				continue;
		}
		return completeList.size() > 0 ? completeList
				.get(completeList.size() - 1) : null;
	}
}
