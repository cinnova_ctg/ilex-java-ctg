package com.mind.newjobdb.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobDashboard;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.TeamInfo;
import com.mind.bean.newjobdb.VendexTab;
import com.mind.bean.pvs.Partner_SearchBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.dao.PVS.Pvsdao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.Convertor;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.dao.VendexDAO;
import com.mind.newjobdb.formbean.JobDashboardForm;
import com.mind.util.WebUtil;

public class JobVendexAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(JobVendexAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
				"Job Dashboard - Vendex View Tab",
				getDataSource(request, "ilexnewDB"))) {
			request.setAttribute("HidePage", "HidePage");
			// return( mapping.findForward( "UnAuthenticate" ) ); // - Check for
			// Pager Security.
		}
		/* Page Security=: End */

		request.setAttribute(
				"tabId",
				(request.getParameter("tabId") == null ? ""
						+ JobDashboardTabType.VENDEX_TAB : request
						.getParameter("tabId")));

		JobDashboardForm bean = (JobDashboardForm) form;
		bean.setTabId("" + JobDashboardTabType.VENDEX_TAB);

		setGeneralFormInfo(bean, request);

		String jobType = JobSetUpDao.getjobType(bean.getJobId(),
				getDataSource(request, "ilexnewDB"));

		if (jobType != null && jobType.trim().equalsIgnoreCase("Default"))
			jobType = "Default";
		else if (jobType != null && jobType.trim().equalsIgnoreCase("Addendum"))
			jobType = "Addendum";
		else
			jobType = "";

		setStatusOfTabs(jobType, request, bean);
		setXmlForGmap(request, bean);
		JobDashboardBean dto = new JobDashboardBean();
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		setVendexTabInfoInForm(dto, getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		Map<String, Object> map;

		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}

		return mapping.findForward("success");
	}

	public void setXmlForGmap(HttpServletRequest request, JobDashboardForm bean) {
		int size = 0;
		ArrayList siteList = new ArrayList();
		ArrayList partnerList = new ArrayList();
		ArrayList currentJobSite = new ArrayList();
		// bean.setMapView("allPartners");
		partnerList = VendexDAO.getpartnerList(bean.getJobId(), "50",
				getDataSource(request, "ilexnewDB"));
		String partnerXml = Pvsdao.generateXML(partnerList);
		String poXml = VendexDAO.getJobPOnotAssignedtoPartner(bean.getJobId(),
				getDataSource(request, "ilexnewDB"));
		if (logger.isDebugEnabled()) {
			logger.debug("setXmlForGmap(HttpServletRequest, JobDashboardForm) - partnerXml = "
					+ partnerXml);
		}
		request.setAttribute("partnerXML", partnerXml);
		if (size == 0)
			size = partnerList.size();

		currentJobSite = VendexDAO.getCurrentSite(bean.getJobId(),
				getDataSource(request, "ilexnewDB"));

		if (bean.getMapView() != null
				&& bean.getMapView().equalsIgnoreCase("allJobsAllPartners")) {
			// bean.setMapView("allJobsAllPartners");
			if (bean.getSelection() != null
					&& bean.getSelection().equals("all")) {
				siteList = Pvsdao.getSiteDeatils(bean.getJobId(), "0",
						"multiplejobtojob", "50",
						getDataSource(request, "ilexnewDB"));
			} else {
				bean.setSelection("notAll");
				siteList = Pvsdao.getSiteDeatils(bean.getJobId(), "0",
						"appendixtojob", "50",
						getDataSource(request, "ilexnewDB"));
			}
		}

		Iterator iterator = currentJobSite.iterator();
		while (iterator.hasNext()) {
			Partner_SearchBean partnerSearchForm = (Partner_SearchBean) iterator
					.next();
			siteList.add(partnerSearchForm);
		}
		if (size == 0)
			size = siteList.size();
		String siteXml = Pvsdao.convertSiteRatingToXML("JOB", "", "", siteList);
		// siteXml=Pvsdao.convertSiteRatingToXML("JOB","","",siteList);
		request.setAttribute("siteXML", siteXml);
		request.setAttribute("size", size);
		request.setAttribute("poXml", poXml);

	}

	public void setStatusOfTabs(String jobType, HttpServletRequest request,
			JobDashboardForm bean) {
		String ticketType = null;
		ticketType = bean.getTicketType();

		if (ticketType != null && !ticketType.trim().equalsIgnoreCase(""))
			request.setAttribute("fromStatus", "fromStatus");
		if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
			request.setAttribute("isTicket", "Ticket");
			request.setAttribute("tabStatus", "job");
		} else {
			if (jobType != null && !jobType.trim().equalsIgnoreCase("")
					&& jobType.trim().equalsIgnoreCase("Default"))
				request.setAttribute("tabStatus", "Default");
			if (jobType != null && !jobType.trim().equalsIgnoreCase("")
					&& jobType.trim().equalsIgnoreCase("Addendum"))
				request.setAttribute("tabStatus", "Addendum");
			else if (jobType != null && jobType.trim().equalsIgnoreCase("")) {
				request.setAttribute("tabStatus", "job");
			}
			request.setAttribute("isTicket", "job");
		}
	}

	public void setVendexTabInfoInForm(JobDashboardBean jobDashboardForm,
			DataSource ds) {
		VendexTab VendexTab = new VendexTab();
		JobDashboard jobDashboard = new JobDashboard();
		JobDAO jobDao = new JobDAO();
		if (logger.isDebugEnabled()) {
			logger.debug("setVendexTabInfoInForm(JobDashboardForm, DataSource) - appendix+++++++++"
					+ jobDashboardForm.getAppendixId());
		}
		VendexTab.setAppendixId(jobDashboardForm.getAppendixId());
		VendexTab.setJobId(jobDashboardForm.getJobId());
		GeneralJobInfo generalJobInfo = jobDao.getGeneralJobInfo(
				Long.parseLong(jobDashboardForm.getJobId()), ds);
		VendexTab.setGeneralJobInfo(generalJobInfo);

		// JobPlanningNote jobPlanningNote =
		// (JobPlanningNote)jobDao.getJobPlanningNotes(Long.parseLong(jobDashboardForm.getJobId()),ds);
		// VendexTab.setJobPlanningNote(jobPlanningNote);

		// boolean showEvenIfNoPending=true;
		// JobWorkflowChecklist topPendingCheckListItems =
		// ((JobWorkflowChecklist) JobLevelWorkflowDAO.getJobWorkflowChecklist
		// (Long.parseLong(jobDashboardForm.getJobId()),ds)).getTopPendingItems(2,
		// showEvenIfNoPending);

		// VendexTab.setTopPendingCheckListItems(topPendingCheckListItems);

		// ArrayList<PurchaseOrder> listOfPurchaseOrders=
		// PurchaseOrderDAO.getPurchaseOrderList(jobDashboardForm.getJobId(),ds);
		// PurchaseOrderList purchaseOrderList = new PurchaseOrderList();
		// purchaseOrderList.setPurchaseOrders(listOfPurchaseOrders);
		// VendexTab.setPurchaseOrderList(purchaseOrderList);
		// CostVariations totalCostVariations =
		// PurchaseOrderList.getTotalCostVariations(purchaseOrderList);
		// VendexTab.setCostVariations(totalCostVariations);
		jobDashboard.setJobDashboardTab(VendexTab);
		Convertor.convertToJobDashboardForm(jobDashboard, jobDashboardForm);
		if (logger.isDebugEnabled()) {
			logger.debug("setVendexTabInfoInForm(JobDashboardForm, DataSource) - COUNTRY "
					+ jobDashboardForm.getSite_country()
					+ " ,"
					+ jobDashboardForm.isUs());
		}
	}

	// public ActionForward addComments (ActionMapping mapping , ActionForm form
	// , HttpServletRequest request , HttpServletResponse response)
	// {
	// response.setHeader("Pragma","No-Cache");
	// response.setHeader("Cache-Control","no-cache");
	// response.setHeader("Cache-Control","no-store");
	// response.setDateHeader("Expires",0);
	// System.out.println(request.getParameter("jobId"));
	// String jobId = request.getParameter("jobId");
	// String comments = request.getParameter("comments");
	// HttpSession session = request.getSession( true );
	// String loginuserid = ( String ) session.getAttribute( "userid" );
	// try{
	//
	// JobDAO.addJobComments(comments, jobId, loginuserid, getDataSource(
	// request , "ilexnewDB" ));
	//
	// }catch(Exception e){
	// e.printStackTrace();
	// }
	// return null;
	// }

	public void setGeneralFormInfo(JobDashboardForm bean,
			HttpServletRequest request) {
		bean.setTabId((request.getParameter("tabId") == null ? ""
				+ JobDashboardTabType.VENDEX_TAB : request
				.getParameter("tabId")));
		String loginUserName = (String) request.getSession().getAttribute(
				"username");
		bean.setCreatedBy(loginUserName);
		bean.setJobId((String) (request.getParameter("Job_Id") == null ? request
				.getAttribute("jobid") : request.getParameter("Job_Id")));
		// To open general job Dashboard.
		if (request.getParameter("jobid") != null) {
			bean.setJobId(request.getParameter("jobid"));
		}
		if (request.getParameter("mapView") != null) {
			bean.setMapView(request.getParameter("mapView"));
		}
		if (request.getAttribute("jobid") != null) {
			bean.setJobId((String) request.getAttribute("jobid"));
		}
		JobDAO jobDAO = new JobDAO();
		TeamInfo teamInfo = jobDAO.getTeamInfo(new Long(bean.getJobId()),
				getDataSource(request, "ilexnewDB"));

		bean.setOwner(teamInfo.getOwner());

		bean.setAppendixId((String) (request.getParameter("appendix_Id") == null ? bean
				.getAppendixId() : request.getParameter("appendix_Id")));
		bean.setTicket_id(request.getParameter("ticket_id") == null ? bean
				.getTicket_id() : request.getParameter("ticket_id"));
		if (request.getParameter("appendix_Id") == null
				&& request.getParameter("appendixid") != null)
			bean.setAppendixId(request.getParameter("appendixid"));

		if (request.getAttribute("tabId") != null) {
			bean.setTabId((String) request.getAttribute("tabId"));
		}
	}

	public ActionForward POaction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		return mapping.findForward("success");
	}
}
