package com.mind.newjobdb.actions;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.common.EnvironmentSelector;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PRM.AppendixCustomerInformationdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.dao.PRM.Scheduledao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.dao.JobContainerDao;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.dao.PurchaseOrder;
import com.mind.newjobdb.dao.TicketDAO;
import com.mind.newjobdb.formbean.JobDashboardForm;
import com.mind.newjobdb.util.InvalidPOActionException;
import com.mind.util.WebUtil;

public class JobContainerAction extends com.mind.common.IlexDispatchAction {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger
            .getLogger(JobContainerAction.class);

    /*
     * isClicked = null || Blank - when responce come on the basis of status.
     * isClicked != null && not Blank - when responce come on paricular tab from
     * where action was requested.
     */
    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        /* Page Security: Start */
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        if (loginuserid == null) {
            return (mapping.findForward("SessionExpire")); // - Check for
            // session expired.
        }
        if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Project",
                "View Job Dashboard", getDataSource(request, "ilexnewDB"))) {
            return (mapping.findForward("UnAuthenticate")); // - Check for Pager
            // Security.
        }
        /* Page Security=: End */

        JobDashboardForm jobDashboardForm = (JobDashboardForm) form;

		// request.setAttribute("JobDashboardTabType", new
        // JobDashboardTabType());

        /*
         * The following block of code is use to set the latitude and longitude
         * of partner in request to calculate the distance and duration between
         * Partner and Site at the time of assignment of a partner to a site.
         */
        request.setAttribute("partnerLatitude", (request
                .getAttribute("partnerLatitude") == null || ("").equals(request
                        .getAttribute("partnerLatitude"))) ? "0.0" : request
                        .getAttribute("partnerLatitude").toString());
        request.setAttribute("partnerLongitude", (request
                .getAttribute("partnerLongitude") == null || ("")
                .equals(request.getAttribute("partnerLongitude"))) ? "0.0"
                        : request.getAttribute("partnerLongitude").toString());
        request.setAttribute("siteLatitude",
                (request.getAttribute("siteLatitude") == null || ("")
                .equals(request.getAttribute("siteLatitude"))) ? "0.0"
                        : request.getAttribute("siteLatitude").toString());
        request.setAttribute("siteLongitude", (request
                .getAttribute("siteLongitude") == null || ("").equals(request
                        .getAttribute("siteLongitude"))) ? "0.0" : request
                        .getAttribute("siteLongitude").toString());
        request.setAttribute("purchaseOrderId",
                request.getAttribute("purchaseOrderId"));

        String url = "";
        String jobType = "";
        boolean poAccepted = false;
        String loginUserName = (String) request.getSession().getAttribute(
                "userid");

        // jobDashboardForm.setTicketType(request.getParameter("ticket_type"));
        String type = request.getParameter("tabId") != null ? request
                .getParameter("tabId") : request.getParameter("type");

        if (jobDashboardForm.getJobId() == null
                || jobDashboardForm.getJobId().trim().equalsIgnoreCase("")) {
            setGeneralFormInfo(jobDashboardForm, request);
        }
        String jobId = jobDashboardForm.getJobId();
        if (jobDashboardForm.getJobId() != null
                && !jobDashboardForm.getJobId().equals("")) {
            jobType = JobSetUpDao.getjobType(jobDashboardForm.getJobId(),
                    getDataSource(request, "ilexnewDB"));
        }
        if (request.getParameter("isClicked") == null) {
            if (jobType != null
                    && (jobType.trim().equalsIgnoreCase("Default") || jobType
                    .trim().equalsIgnoreCase("Addendum"))) {
                jobDashboardForm.setIsClicked("1");
                type = new Integer(JobDashboardTabType.SETUP_TAB).toString();
                if (jobType.trim().equalsIgnoreCase("Addendum")
                        && request.getAttribute("changestatusflag") != null) {
                    request.setAttribute("addendumChangeStatus", request
                            .getAttribute("changestatusflag").toString());
                }
            }
        }
        String appendixId = jobDashboardForm.getAppendixId();
        if (appendixId == null || appendixId.trim().equalsIgnoreCase("")) {
            appendixId = IMdao.getAppendixId(jobId,
                    getDataSource(request, "ilexnewDB"));
            jobDashboardForm.setAppendixId(appendixId);
        }
        jobDashboardForm
                .setIsClicked(request.getParameter("isClicked") != null ? request
                                .getParameter("isClicked") : jobDashboardForm
                                .getIsClicked());
        String jobStatus = "";
        if (jobId != null && !jobId.trim().equalsIgnoreCase("")) {
            jobStatus = JobContainerDao.getJobStatus(jobId,
                    getDataSource(request, "ilexnewDB"));
            jobDashboardForm.setJobStatus(jobStatus);

            // Added for appendix type check for headers
            if (jobId.equals("0")) {
                if (request.getAttribute("jobid") != null) {
                    String appendixType = JobDAO.getAppendixType(
                            (String) request.getAttribute("jobid"),
                            getDataSource(request, "ilexnewDB"));
                    if (!appendixType.equals("3")) {
                        jobDashboardForm.setTicket_id("");
                        jobDashboardForm.setTicketType("");
                    }
                }
            } else {
                String ticketId = JobDAO.getTicketId(jobId,
                        getDataSource(request, "ilexnewDB"));

                // Logger for finding error
                if (logger.isDebugEnabled()) {
                    logger.debug("Ticket id in JobContainerAction: for job id: "
                            + jobId + " is " + ticketId);
                }

                if (ticketId != null && !ticketId.equals("")) {
                    jobDashboardForm.setTicket_id(ticketId);
                    jobDashboardForm.setTicketType("existingTicket");
                } else if (jobDashboardForm.getTicketType() == null
                        || jobDashboardForm.getTicketType().trim()
                        .equalsIgnoreCase("")) {
                    jobDashboardForm.setTicket_id("");
                    jobDashboardForm.setTicketType("");
                }
            }

            jobDashboardForm.setJobIndicator(JobDAO.getJobTestIndicator(jobId,
                    getDataSource(request, "ilexnewDB")));
            poAccepted = Scheduledao.checkPOStatus(jobId,
                    getDataSource(request, "ilexnewDB"));
            // do PO actions
        }

        if (request.getParameter("CancelTicket") != null
                && request.getParameter("CancelTicket").equals("CancelTicket")) {
            TicketDAO.cancelTicket(
                    Integer.parseInt(jobDashboardForm.getJobId()),
                    Integer.parseInt(loginUserName),
                    getDataSource(request, "ilexnewDB"));
        }

        // API Settings
        request.setAttribute("api_url", EnvironmentSelector.getBundleString("api.url"));
        request.setAttribute("api_user", EnvironmentSelector.getBundleString("api.username"));
        request.setAttribute("api_pass", EnvironmentSelector.getBundleString("api.password"));

        try {
            if (request.getParameter("isClicked") != null
                    && !request.getParameter("isClicked").trim()
                    .equalsIgnoreCase("")) {
                if (jobDashboardForm.getTicketType() != null) {
                    if (type != null
                            && type.trim().equalsIgnoreCase(
                                    JobDashboardTabType.SETUP_TAB + "")) {
                        url = "/JobSetUpAction.do?tabId=" + type
                                + "&appendix_Id=" + appendixId + "&Job_Id="
                                + jobId + "&ticketType="
                                + jobDashboardForm.getTicketType();
                        ActionForward fwd = new ActionForward();
                        fwd.setPath(url);
                        return fwd;
                    }
                    if (type != null
                            && type.trim().equalsIgnoreCase(
                                    JobDashboardTabType.TICKET_TAB + "")) {
                        url = "/JobTicketAction.do?tabId=" + type
                                + "&appendix_Id=" + appendixId + "&Job_Id="
                                + jobId + "&ticketType="
                                + jobDashboardForm.getTicketType()
                                + "&webTicketType=";
                        ActionForward fwd = new ActionForward();
                        fwd.setPath(url);
                        return fwd;
                    }
                    if (type != null
                            && type.trim().equalsIgnoreCase(
                                    JobDashboardTabType.BUY_TAB + "")) {
                        insertCustomFields(appendixId + "", loginUserName,
                                request);
                        url = "/JobBuyAction.do?tabId=" + type
                                + "&appendix_Id=" + appendixId + "&Job_Id="
                                + jobId + "&ticketType="
                                + jobDashboardForm.getTicketType();
                        ActionForward fwd = new ActionForward();
                        fwd.setPath(url);
                        return fwd;
                    }
                    if (type != null
                            && type.trim().equalsIgnoreCase(
                                    JobDashboardTabType.COMPLETE_TAB + "")) {
                        url = "/JobDBCompleteAction.do?tabId=" + type
                                + "&appendixId=" + appendixId + "&jobId="
                                + jobId + "&ticketType="
                                + jobDashboardForm.getTicketType();
                        ActionForward fwd = new ActionForward();
                        fwd.setPath(url);
                        return fwd;
                    }
                    if (type != null
                            && type.trim().equalsIgnoreCase(
                                    JobDashboardTabType.VENDEX_TAB + "")) {
                        url = "/JobVendexAction.do?tabId=" + type
                                + "&appendix_Id=" + appendixId + "&Job_Id="
                                + jobId + "&ticketType="
                                + jobDashboardForm.getTicketType();
                        ActionForward fwd = new ActionForward();
                        fwd.setPath(url);
                        return fwd;
                    }
                    if (type != null
                            && type.trim().equalsIgnoreCase(
                                    JobDashboardTabType.EXECUTE_TAB + "")) {
                        logger.debug("Check for Integer.ParseInt(appendixId) Job JobContainerAction.unspecified(String) 11111 =>"
                                + appendixId);
                        insertCustomFields(appendixId + "", loginUserName,
                                request);
                        url = "/JobExecuteAction.do?tabId=" + type
                                + "&appendix_Id=" + appendixId + "&Job_Id="
                                + jobId + "&ticketType="
                                + jobDashboardForm.getTicketType();
                        ActionForward fwd = new ActionForward();
                        fwd.setPath(url);
                        return fwd;
                    }
                    if (type != null
                            && type.trim().equalsIgnoreCase(
                                    JobDashboardTabType.CLOSE_TAB + "")) {
                        url = "/JobCloseAction.do?tabId=" + type
                                + "&appendix_Id=" + appendixId + "&Job_Id="
                                + jobId + "&ticketType="
                                + jobDashboardForm.getTicketType();
                        ActionForward fwd = new ActionForward();
                        fwd.setPath(url);
                        return fwd;
                    }
                    if (type != null
                            && type.trim().equalsIgnoreCase(
                                    JobDashboardTabType.SCHEDULE_TAB + "")) {
                        url = "/JobScheduleAction.do?tabId=" + type
                                + "&appendix_Id=" + appendixId + "&Job_Id="
                                + jobId + "&ticketType="
                                + jobDashboardForm.getTicketType();
                        ActionForward fwd = new ActionForward();
                        fwd.setPath(url);
                        return fwd;
                    }
                    if (type != null
                            && type.trim().equalsIgnoreCase(
                                    JobDashboardTabType.DISPATCH_SCHEDULE_TAB
                                    + "")) {
                        url = "/JobDispatchScheduleAction.do?tabId=" + type
                                + "&appendix_Id=" + appendixId + "&Job_Id="
                                + jobId + "&ticketType="
                                + jobDashboardForm.getTicketType();
                        ActionForward fwd = new ActionForward();
                        fwd.setPath(url);
                        return fwd;
                    }
                    if (type != null
                            && type.trim().equalsIgnoreCase(
                                    JobDashboardTabType.SITE_TAB + "")) {
                        String site_list_id = request
                                .getParameter("site_list_id") != null ? request
                                        .getParameter("site_list_id") : "";
                        String siteId = request.getParameter("siteId") != null ? request
                                .getParameter("siteId") : "";
                        String update = request.getParameter("update") != null ? request
                                .getParameter("update") : "";
                        String add = request.getParameter("add") != null ? request
                                .getParameter("add") : "";
                        url = "/JobSiteAction.do?tabId=" + type
                                + "&appendix_Id=" + appendixId + "&Job_Id="
                                + jobId + "&ticketType="
                                + jobDashboardForm.getTicketType() + "&update="
                                + update + "&site_list_id=" + site_list_id
                                + "&siteId=" + siteId + "&add=" + add;
                        ActionForward fwd = new ActionForward();
                        fwd.setPath(url);
                        return fwd;
                    }
                    if (type != null
                            && type.trim().equalsIgnoreCase(
                                    JobDashboardTabType.FINANCIAL_TAB + "")) {
                        url = "/JobFinancialAction.do?tabId=" + type
                                + "&appendix_Id=" + appendixId + "&Job_Id="
                                + jobId + "&ticketType="
                                + jobDashboardForm.getTicketType();
                        ActionForward fwd = new ActionForward();
                        fwd.setPath(url);
                        return fwd;
                    } else {
                        url = "/JobSetUpAction.do?tabId=" + type
                                + "&appendix_Id=" + appendixId + "&Job_Id="
                                + jobId + "&ticketType="
                                + jobDashboardForm.getTicketType();
                        ActionForward fwd = new ActionForward();
                        fwd.setPath(url);
                        return fwd;
                    }
                }
            }
            if (request.getParameter("isClicked") == null
                    || request.getParameter("isClicked").trim()
                    .equalsIgnoreCase("")) {
                if (jobDashboardForm.getTicketType() != null
                        && !jobDashboardForm.getTicketType().trim()
                        .equalsIgnoreCase("")
                        && jobDashboardForm.getTicketType().trim()
                        .equalsIgnoreCase("existingTicket")
                        && jobDashboardForm.getWebTicket() != null
                        && jobDashboardForm.getWebTicket().equals("webTicket")) {
                    url = "/JobTicketAction.do?tabId="
                            + JobDashboardTabType.TICKET_TAB + "&appendix_Id="
                            + appendixId + "&Job_Id=" + jobId + "&ticketType="
                            + jobDashboardForm.getTicketType();
                    ActionForward fwd = new ActionForward();
                    fwd.setPath(url);
                    return fwd;
                }

                if (jobDashboardForm.getTicketType() != null
                        && !jobDashboardForm.getTicketType().trim()
                        .equalsIgnoreCase("")
                        && jobDashboardForm.getTicketType().trim()
                        .equalsIgnoreCase("newTicket")) {
                    url = "/JobTicketAction.do?tabId="
                            + JobDashboardTabType.TICKET_TAB + "&appendix_Id="
                            + appendixId + "&Job_Id=" + jobId + "&ticketType="
                            + jobDashboardForm.getTicketType();
                    ActionForward fwd = new ActionForward();
                    fwd.setPath(url);
                    return fwd;
                }
                if (jobStatus != null && !jobStatus.trim().equalsIgnoreCase("")
                        && jobStatus.trim().equalsIgnoreCase("Not Scheduled")) {
                    url = "/JobSetUpAction.do?tabId="
                            + JobDashboardTabType.SETUP_TAB + "&appendix_Id="
                            + appendixId + "&Job_Id=" + jobId + "&ticketType="
                            + jobDashboardForm.getTicketType();
                    ActionForward fwd = new ActionForward();
                    fwd.setPath(url);
                    return fwd;
                }
                if (jobStatus != null && !jobStatus.trim().equalsIgnoreCase("")
                        && jobStatus.trim().equalsIgnoreCase("Scheduled")) {
                    insertCustomFields(appendixId + "", loginUserName, request);
                    if (jobDashboardForm.getTicketType() != null
                            && jobDashboardForm.getTicketType().trim()
                            .equalsIgnoreCase("")) {
                        url = "/JobBuyAction.do?tabId="
                                + JobDashboardTabType.BUY_TAB + "&appendix_Id="
                                + appendixId + "&Job_Id=" + jobId
                                + "&ticketType="
                                + jobDashboardForm.getTicketType();
                    } else {
                        url = "/JobBuyAction.do?tabId="
                                + JobDashboardTabType.BUY_TAB + "&appendix_Id="
                                + appendixId + "&Job_Id=" + jobId
                                + "&ticketType="
                                + jobDashboardForm.getTicketType();
                    }
                    ActionForward fwd = new ActionForward();
                    fwd.setPath(url);
                    return fwd;
                }
                if (jobStatus != null
                        && !jobStatus.trim().equalsIgnoreCase("")
                        && (jobStatus.trim().equalsIgnoreCase("Complete") || (jobStatus
                        .trim().equalsIgnoreCase("Offsite") && !poAccepted))) {
                    url = "/JobDBCompleteAction.do?tabId="
                            + JobDashboardTabType.COMPLETE_TAB + "&appendixId="
                            + appendixId + "&jobId=" + jobId + "&ticketType="
                            + jobDashboardForm.getTicketType();
                    ActionForward fwd = new ActionForward();
                    fwd.setPath(url);
                    return fwd;
                }
                if (jobStatus != null
                        && !jobStatus.trim().equalsIgnoreCase("")
                        && (jobStatus.trim().equalsIgnoreCase("Confirmed")
                        || jobStatus.trim().equalsIgnoreCase("Onsite") || (jobStatus
                        .trim().equalsIgnoreCase("Offsite") && poAccepted))) {
                    logger.debug("Check for Integer.ParseInt(appendixId) Job JobContainerAction.unspecified(String) 2222 =>"
                            + appendixId);
                    insertCustomFields(appendixId + "", loginUserName, request);
                    url = "/JobExecuteAction.do?tabId="
                            + JobDashboardTabType.EXECUTE_TAB + "&appendix_Id="
                            + appendixId + "&Job_Id=" + jobId + "&ticketType="
                            + jobDashboardForm.getTicketType();
                    ActionForward fwd = new ActionForward();
                    fwd.setPath(url);
                    return fwd;
                }
                if (jobStatus != null && !jobStatus.trim().equalsIgnoreCase("")
                        && jobStatus.trim().equalsIgnoreCase("Closed")) {

                    url = "/JobCloseAction.do?tabId="
                            + JobDashboardTabType.CLOSE_TAB + "&appendix_Id="
                            + appendixId + "&Job_Id=" + jobId + "&ticketType="
                            + jobDashboardForm.getTicketType();
                    ActionForward fwd = new ActionForward();
                    fwd.setPath(url);
                    return fwd;
                } else if (jobStatus != null
                        && !jobStatus.trim().equalsIgnoreCase("")
                        && !jobStatus.trim().equalsIgnoreCase("Scheduled")
                        && !jobStatus.trim().equalsIgnoreCase("Not Scheduled")) {
                    url = "/JobSetUpAction.do?tabId="
                            + JobDashboardTabType.SETUP_TAB + "&appendix_Id="
                            + appendixId + "&Job_Id=" + jobId + "&ticketType="
                            + jobDashboardForm.getTicketType();
                    ActionForward fwd = new ActionForward();
                    fwd.setPath(url);
                    return fwd;
                } else {
                    url = "/JobSetUpAction.do?tabId="
                            + JobDashboardTabType.SETUP_TAB + "&appendix_Id="
                            + appendixId + "&Job_Id=" + jobId + "&ticketType="
                            + jobDashboardForm.getTicketType();
                    ActionForward fwd = new ActionForward();
                    fwd.setPath(url);
                    return fwd;
                }
            }

        } catch (Exception e) {
            logger.error(
                    "unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                    e);

            logger.error(
                    "unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                    e);
        }
        return mapping.findForward("success");
    }

    public void setGeneralFormInfo(JobDashboardForm bean,
            HttpServletRequest request) {
        String loginUserName = (String) request.getSession().getAttribute(
                "username");
        bean.setCreatedBy(loginUserName);
        bean.setJobId((String) (request.getParameter("Job_Id") == null ? request
                .getAttribute("jobid") : request.getParameter("Job_Id")));
        // To open general job Dashboard.
        if (request.getParameter("jobid") != null) {
            bean.setJobId(request.getParameter("jobid"));
        }
        if (request.getAttribute("jobid") != null) {
            bean.setJobId((String) request.getAttribute("jobid"));
        }

        if (request.getParameter("webTicketType") != null) {
            bean.setWebTicket(request.getParameter("webTicketType"));
        }

        bean.setAppendixId((String) (request.getParameter("appendix_Id") == null ? bean
                .getAppendixId() : request.getParameter("appendix_Id")));
        bean.setTicket_id(request.getParameter("ticket_id") == null ? bean
                .getTicket_id() : request.getParameter("ticket_id"));
        if (request.getParameter("appendix_Id") == null
                && request.getParameter("appendixid") != null) {
            bean.setAppendixId(request.getParameter("appendixid"));
        }
    }

    public ActionForward POaction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        /* Page Security: Start */
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        if (loginuserid == null) {
            return (mapping.findForward("SessionExpire")); // - Check for
            // session expired.
        }
        if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Partner",
                "Manage PO/WO", getDataSource(request, "ilexnewDB"))) {
            return (mapping.findForward("UnAuthenticate")); // - Check for Pager
            // Security.
        }
        /* Page Security=: End */

        String url = "";
        JobDashboardForm jobDashboardForm = (JobDashboardForm) form;
        String poActionName = request.getParameter("poActionName");
        String type = request.getParameter("type");
        String poId = request.getParameter("poId");
        String jobId = jobDashboardForm.getJobId();
        String appendixId = jobDashboardForm.getAppendixId();
        Map<String, Object> map;

        if (jobDashboardForm.getJobId() != null
                && !jobDashboardForm.getJobId().equals("")) {
            if (TicketDAO.isOnSite(
                    Integer.parseInt(jobDashboardForm.getJobId()),
                    getDataSource(request, "ilexnewDB"))) {
                request.setAttribute("onsite", "onsite");
            }
        }

        if (poActionName != null && poId != null) {
            if (poActionName.equalsIgnoreCase("Assign")) {
                url = "/Partner_Search.do?assign=true&powoid=" + poId
                        + "&tabId=" + type
                        + "&formtype=powo&ref=search&typeid=" + jobId
                        + "&type=Update&viewjobtype=A";
                ActionForward fwd = new ActionForward();
                logger.debug(url);
                fwd.setPath(url);
                return fwd;
            } else if (poActionName.equalsIgnoreCase("Reassign")) {
                url = "/Partner_Search.do?assign=true&powoid="
                        + poId
                        + "&tabId="
                        + type
                        + "&appendix_Id="
                        + appendixId
                        + "&formtype=powo&ref=search&isReassign=reassign&typeid="
                        + jobId + "&type=Update&viewjobtype=A";
                ActionForward fwd = new ActionForward();
                logger.debug(url);
                fwd.setPath(url);
                return fwd;
            } else if (poActionName.equalsIgnoreCase("Delete")) {
                try {
                    PurchaseOrder.delete(poId,
                            getDataSource(request, "ilexnewDB"));
                    jobDashboardForm.setIsClicked("1");
                } catch (InvalidPOActionException e) {
                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);

                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);
                }
            } else if (poActionName.equalsIgnoreCase("Cancel")) {
                try {
                    PurchaseOrder.cancel(poId, loginuserid,
                            getDataSource(request, "ilexnewDB"));
                    jobDashboardForm.setIsClicked("1");
                } catch (InvalidPOActionException e) {
                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);

                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);
                }
            } else if (poActionName.equalsIgnoreCase("Accept")) {
                try {
					// String poMasterType =
                    // request.getParameter("poMasterType");
                    //
                    // String deliverableAcceptCount = "";
                    // deliverableAcceptCount = PODao.getDeliverableAcceptCount(
                    // poId, getDataSource(request, "ilexnewDB"));
                    // if (poMasterType.contains("Material")
                    // && deliverableAcceptCount.equals("0")) {
                    // PurchaseOrder.complete(poId, loginuserid,
                    // getDataSource(request, "ilexnewDB"));
                    // } else {
                    PurchaseOrder.accept(poId, loginuserid,
                            getDataSource(request, "ilexnewDB"));
                    // }
                    jobDashboardForm.setIsClicked("1");
                } catch (InvalidPOActionException e) {
                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);

                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);
                }
            } else if (poActionName.equalsIgnoreCase("Complete")) {
                try {
                    PurchaseOrder.complete(poId, loginuserid,
                            getDataSource(request, "ilexnewDB"));
                    jobDashboardForm.setIsClicked("1");
                } catch (InvalidPOActionException e) {
                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);

                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);
                }
            } else if (poActionName.equalsIgnoreCase("Reissue")) {
                try {
                    PurchaseOrder.reIssue(poId, loginuserid,
                            getDataSource(request, "ilexnewDB"));
                    jobDashboardForm.setIsClicked("1");
                } catch (InvalidPOActionException e) {
                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);

                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);
                } catch (Exception e) {
                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);

                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);
                }
            } else if (poActionName.equalsIgnoreCase("Undo")) {
                try {
                    PurchaseOrder.undo(poId, loginuserid,
                            getDataSource(request, "ilexnewDB"));
                    jobDashboardForm.setIsClicked("1");
                } catch (InvalidPOActionException e) {
                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);

                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);
                } catch (Exception e) {
                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);

                    logger.error(
                            "POaction(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                            e);
                }
            } else if (poActionName.equalsIgnoreCase("Send")) {
                url = "/POWOAction.do?jobid=" + jobId
                        + "&function_type=Send POWO&order_type=powo&powoid="
                        + poId + "&tabId=" + type + "&onlyEmail=0";

                if (jobId != null && !jobId.trim().equalsIgnoreCase("")) {
                    map = DatabaseUtilityDao.setMenu(jobId, loginuserid,
                            getDataSource(request, "ilexnewDB"));
                    WebUtil.copyMapToRequest(request, map);
                }
                ActionForward fwd = new ActionForward();
                fwd.setPath(url);
                return fwd;
            } else if (poActionName.equalsIgnoreCase("Email")) {
                if (jobId != null && !jobId.trim().equalsIgnoreCase("")) {
                    map = DatabaseUtilityDao.setMenu(jobId, loginuserid,
                            getDataSource(request, "ilexnewDB"));
                    WebUtil.copyMapToRequest(request, map);
                }
                url = "/POWOAction.do?jobid=" + jobId
                        + "&function_type=Send POWO&order_type=powo&powoid="
                        + poId + "&tabId=" + type + "&onlyEmail=1";
                ActionForward fwd = new ActionForward();
                fwd.setPath(url);
                return fwd;
            } else if (poActionName.equalsIgnoreCase("Edit")) {
                url = "/POAction.do?hmode=unspecified&appendixType=&tabId=0&appendix_Id="
                        + appendixId + "&jobId=" + jobId + "&poId=" + poId;
                ActionForward fwd = new ActionForward();
                fwd.setPath(url);
                return fwd;
            } else if (poActionName.equalsIgnoreCase("Update Quantity")) {
                url = "/POAction.do?hmode=unspecified&appendixType=&tabId=0&appendix_Id="
                        + appendixId
                        + "&jobId="
                        + jobId
                        + "&poId="
                        + poId
                        + "&updateQuantity=" + poActionName;
                ActionForward fwd = new ActionForward();
                fwd.setPath(url);
                return fwd;
            } else if (poActionName.equalsIgnoreCase("View PO History")) {
                url = "/EntityHistory.do?entityId=" + poId
                        + "&entityType=PO&function=viewHistory";
                ActionForward fwd = new ActionForward();
                fwd.setPath(url);
                return fwd;
            } else if (poActionName.equalsIgnoreCase("View WO History")) {
                url = "/EntityHistory.do?entityId=" + poId
                        + "&entityType=WO&function=viewHistory";
                ActionForward fwd = new ActionForward();
                fwd.setPath(url);
                return fwd;
            }
        }
        return mapping.findForward("success");
    }

    private void insertCustomFields(String appandixId, String userId,
            HttpServletRequest request) {
        String msaName = Appendixdao.getMsaname(appandixId,
                getDataSource(request, "ilexnewDB"));
        // only add default custom fields for XO Communications
        if (msaName.equalsIgnoreCase("XO Communications")) {
            String[][] custinfolist = new String[10][2];

            Boolean[] addField = {true, true, true, true};

            String[] field = {
                "*First Visit/ Problem Solved?:First Visit Resolution",
                "*Revisit Required?:Revisit", "Total # of Site Visits:",
                "Multiple Visits/POF Code:"};

            String parameterstring = "";
            String parameterstringId = "";

            custinfolist = AppendixCustomerInformationdao.getAppendixcustinfo(
                    appandixId, getDataSource(request, "ilexnewDB"));

            for (int i = 0; i < custinfolist.length; i++) {
                String[] arr = custinfolist[i];
                if (addField[0] && arr[1].equals(field[0])) {
                    addField[0] = false;
                } else if (addField[1] && arr[1].equals(field[1])) {
                    addField[1] = false;
                } else if (addField[2] && arr[1].equals(field[2])) {
                    addField[2] = false;
                } else if (addField[3] && arr[1].equals(field[3])) {
                    addField[3] = false;
                }
            }

            if (!addField[0] && !addField[1] && !addField[2] && !addField[3]) {
                logger.info("Fields already exist...");
                return;
            }

            String sep = "";

            for (int i = 0; i < field.length; i++) {
                if (addField[i]) {
                    Integer index = getFieldIdIndex(custinfolist);
                    if (index != null) {
                        parameterstring = parameterstring + sep + field[i];
                        parameterstringId = parameterstringId + sep
                                + custinfolist[index][0];
                        custinfolist[index][1] = field[i];
                    }
                    if (!parameterstring.equals("")) {
                        sep = "~";
                    }
                }
            }

            int statusvalue = AppendixCustomerInformationdao
                    .addAppendixCustInfo(appandixId, parameterstringId,
                            parameterstring, userId,
                            getDataSource(request, "ilexnewDB"));

            if (statusvalue == 0) {
                logger.info(" Fields populated successfully............");
            }
        }
    }

    private Integer getFieldIdIndex(String[][] custinfolist) {

        for (int i = 0; i < custinfolist.length; i++) {
            String[] arr = custinfolist[i];
            if (arr[1].equals("")) {
                return i;
            }
        }
        return null;
    }

}
