package com.mind.newjobdb.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.common.dao.Authenticationdao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.dao.TicketRequestInfoDAO;
import com.mind.newjobdb.formbean.TicketChangeCriticalityForm;
import com.mind.util.WebUtil;

public class TicketChangeCriticalityAction extends
		com.mind.common.IlexDispatchAction {

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
				"Change Criticality", getDataSource(request, "ilexnewDB"))) {
			return (mapping.findForward("UnAuthenticate")); // - Check for Pager
															// Security.
		}
		/* Page Security=: End */

		TicketChangeCriticalityForm ticketChangeCriticality = (TicketChangeCriticalityForm) form;
		setGeneralFormInfo(ticketChangeCriticality, request);
		setPrveCriticality(ticketChangeCriticality,
				getDataSource(request, "ilexnewDB"));
		fillCombo(ticketChangeCriticality, request,
				getDataSource(request, "ilexnewDB"));
		Map<String, Object> map;

		/* Set attribute for menu and breadCream */
		if (ticketChangeCriticality.getJobId() != null
				&& !ticketChangeCriticality.getJobId().trim()
						.equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(
					ticketChangeCriticality.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		TicketRequestInfoDAO.setDataSource(getDataSource(request, "ilexnewDB"));
		return mapping.findForward("success");
	}

	/**
	 * Set request parameter
	 * 
	 * @param ticketChangeCriticality
	 * @param request
	 */
	private void setGeneralFormInfo(
			TicketChangeCriticalityForm ticketChangeCriticality,
			HttpServletRequest request) {

		if (request.getParameter("msaId") != null)
			ticketChangeCriticality.setMsaId(request.getParameter("msaId"));

		if (request.getParameter("Job_Id") != null)
			ticketChangeCriticality.setJobId(request.getParameter("Job_Id"));

		if (request.getParameter("appendixId") != null) {
			ticketChangeCriticality.setAppendixId(request
					.getParameter("appendixId"));
		}
	}

	/**
	 * Set Current Criticality.
	 * 
	 * @param ticketChangeCriticality
	 * @param ds
	 */
	private void setPrveCriticality(
			TicketChangeCriticalityForm ticketChangeCriticality, DataSource ds) {
		String[] criticalityResource = TicketRequestInfoDAO.getTicketData(
				ticketChangeCriticality.getJobId(), ds);
		ticketChangeCriticality.setPrveCriticality(criticalityResource[0]);
		ticketChangeCriticality.setHelpDesk(criticalityResource[2]);
		if (ticketChangeCriticality.getCriticality() == null
				|| ticketChangeCriticality.getCancel() != null) {
			ticketChangeCriticality.setCriticality(criticalityResource[0]);
			ticketChangeCriticality.setResource(criticalityResource[1]);
		}
	}

	/**
	 * Fill Combobox
	 * 
	 * @param ticketChangeCriticality
	 * @param ds
	 */
	private void fillCombo(TicketChangeCriticalityForm ticketChangeCriticality,
			HttpServletRequest request, DataSource ds) {

		ticketChangeCriticality.setCriticalityCombo(TicketRequestInfoDAO
				.getCriticalities(new Long(ticketChangeCriticality.getMsaId())
						.longValue(), ds));
		Map<String, Object> map = new HashMap<String, Object>();

		if (ticketChangeCriticality.getCriticality() != null) {
			ticketChangeCriticality.setResourceCombo(TicketRequestInfoDAO
					.getResources(map,
							new Long(ticketChangeCriticality.getAppendixId())
									.longValue(), ticketChangeCriticality
									.getCriticality(), true, ds));
			WebUtil.copyMapToRequest(request, map);
		} else {
			ArrayList resourcelevel = new java.util.ArrayList();
			resourcelevel.add(new com.mind.common.LabelValue("Select", "0"));
			ticketChangeCriticality.setResourceCombo(resourcelevel);
		}
		// request.setAttribute("", "")
	}

	/**
	 * Save Criticality and resource level.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward Save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(true);
		if (session.getAttribute("userid") == null)
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		String loginuserid = (String) session.getAttribute("userid");

		TicketChangeCriticalityForm ticketChangeCriticality = (TicketChangeCriticalityForm) form;
		setGeneralFormInfo(ticketChangeCriticality, request);
		if (ticketChangeCriticality.getUpdate() != null) {
			int updateFlag = TicketRequestInfoDAO.updateCricalityResource(
					ticketChangeCriticality.getJobId(),
					ticketChangeCriticality.getCriticality(),
					ticketChangeCriticality.getResource(), loginuserid,
					ticketChangeCriticality.getHelpDesk(),
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("updateFlag", updateFlag);
		}
		return mapping.findForward("save");
	}

}
