package com.mind.newjobdb.actions;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.newjobdb.ChecklistStatusType;
import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobActivities;
import com.mind.bean.newjobdb.JobChecklistParameters;
import com.mind.bean.newjobdb.JobDashboard;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.JobPlanningNote;
import com.mind.bean.newjobdb.JobWorkflowItem;
import com.mind.bean.newjobdb.TeamInfo;
import com.mind.common.EnvironmentSelector;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.util.MySqlConnection;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PMT.DeleteJobdao;
import com.mind.dao.PRM.JobDashboarddao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.Convertor;
import com.mind.newjobdb.business.SetupTab;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.dao.JobLevelWorkflowDAO;
import com.mind.newjobdb.dao.JobWorkflowChecklist;
import com.mind.newjobdb.formbean.JobDashboardForm;
import com.mind.util.WebUtil;

public class JobSetupAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JobSetupAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
				"Job Dashboard - Setup Tab",
				getDataSource(request, "ilexnewDB"))) {
			request.setAttribute("HidePage", "HidePage");
			// return( mapping.findForward( "UnAuthenticate" ) ); // - Check for
			// Pager Security.
		}
		/* Page Security=: End */

		request.setAttribute(
				"tabId",
				(request.getParameter("tabId") == null ? ""
						+ JobDashboardTabType.SETUP_TAB : request
						.getParameter("tabId")));
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - param tab id"
					+ request.getParameter("tabId"));
		}
		JobDashboardForm bean = (JobDashboardForm) form;
		JobDashboardBean dto = new JobDashboardBean();
		setGeneralFormInfo(bean, request);
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		setSetupTabInfoInForm(dto, getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		String jobType = JobSetUpDao.getjobType(bean.getJobId(),
				getDataSource(request, "ilexnewDB"));
		if (jobType != null && jobType.equalsIgnoreCase("Default")
				|| jobType.equalsIgnoreCase("Addendum")) {
			request.setAttribute("Default", "Default");
		}
		if (jobType != null && jobType.trim().equalsIgnoreCase("Default"))
			jobType = "Default";
		else if (jobType != null && jobType.trim().equalsIgnoreCase("Addendum"))
			jobType = "Addendum";
		else
			jobType = "";
		setStatusOfTabs(jobType, request, bean);
		Map<String, Object> map;

		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		return mapping.findForward("success");
	}

	public ActionForward refreshJobWorkFlow(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		response.setHeader("Pragma", "No-Cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", 0);
		if (logger.isDebugEnabled()) {
			logger.debug("refreshJobWorkFlow(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - "
					+ request.getParameter("jobId"));
		}
		long jobId = Long.parseLong(request.getParameter("jobId"));
		if (logger.isDebugEnabled()) {
			logger.debug("refreshJobWorkFlow(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - "
					+ request.getParameter("item_id"));
		}
		long itemId = Long.parseLong(request.getParameter("item_id"));
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		JobWorkflowItem jobWorkflowItem = JobLevelWorkflowDAO
				.getJobWorkflowItemObj(jobId, itemId,
						getDataSource(request, "ilexnewDB"));
		JobWorkflowChecklist.setNewlyCompletedItem(jobWorkflowItem,
				getDataSource(request, "ilexnewDB"), loginuserid);
		int noOfTopPending = 2;

		StringBuffer sb = new StringBuffer();
		int noOfTop = 2;
		JobWorkflowChecklist topPendingCheckListItems = ((JobWorkflowChecklist) JobLevelWorkflowDAO
				.getJobWorkflowChecklist(jobId,
						getDataSource(request, "ilexnewDB")))
				.getTopPendingItems(noOfTop);
		ArrayList<JobWorkflowItem> jobWorkflowItemsAL = topPendingCheckListItems
				.getJobWorkflowItems();
		int noOfItems = jobWorkflowItemsAL.size();
		String itemDescription1 = "-";
		String itemId1 = "-";
		String itemStatus1 = "-";
		String itemSequence1 = "-";
		String itemReferenceDate1 = "-";
		String itemUser1 = "-";

		String itemDescription2 = "-";
		String itemId2 = "-";
		String itemStatus2 = "-";
		String itemSequence2 = "-";
		String itemReferenceDate2 = "-";
		String itemUser2 = "-";
		try {
			sb.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
			sb.append("<pendingItems generated=\"ulq\">\n");
			if (noOfItems > 0) {
				itemDescription1 = jobWorkflowItemsAL.get(0).getDescription();
				itemId1 = jobWorkflowItemsAL.get(0).getItemId() + "";
				itemStatus1 = ChecklistStatusType
						.getStatusName(jobWorkflowItemsAL.get(0)
								.getItemStatusCode());
				itemSequence1 = jobWorkflowItemsAL.get(0).getSequence();
				itemReferenceDate1 = jobWorkflowItemsAL.get(0)
						.getReferenceDate();
				itemUser1 = jobWorkflowItemsAL.get(0).getUser();
			}
			if (noOfItems > 1) {
				itemDescription2 = jobWorkflowItemsAL.get(1).getDescription();
				itemId2 = jobWorkflowItemsAL.get(1).getItemId() + "";
				itemStatus2 = ChecklistStatusType
						.getStatusName(jobWorkflowItemsAL.get(1)
								.getItemStatusCode());
				itemSequence2 = jobWorkflowItemsAL.get(1).getSequence();
				itemReferenceDate2 = jobWorkflowItemsAL.get(1)
						.getReferenceDate();
				itemUser2 = jobWorkflowItemsAL.get(1).getUser();
			}

			JobDAO.writeNode(JobChecklistParameters.ITEM_DESCRIPTION_1,
					itemDescription1, sb);
			JobDAO.writeNode(JobChecklistParameters.ITEM_ID1, itemId1, sb);
			JobDAO.writeNode(JobChecklistParameters.ITEM_STATUS1, itemStatus1,
					sb);
			JobDAO.writeNode(JobChecklistParameters.ITEM_REFERENCE_DATE_1,
					itemReferenceDate1, sb);
			JobDAO.writeNode(JobChecklistParameters.ITEM_USER_1, itemUser1, sb);

			JobDAO.writeNode(JobChecklistParameters.ITEM_DESCRIPTION_2,
					itemDescription2, sb);
			JobDAO.writeNode(JobChecklistParameters.ITEM_ID2, itemId2, sb);
			JobDAO.writeNode(JobChecklistParameters.ITEM_STATUS2, itemStatus2,
					sb);
			JobDAO.writeNode(JobChecklistParameters.ITEM_REFERENCE_DATE_2,
					itemReferenceDate2, sb);
			JobDAO.writeNode(JobChecklistParameters.ITEM_USER_2, itemUser2, sb);

			sb.append("</pendingItems>");
			if (logger.isDebugEnabled()) {
				logger.debug("refreshJobWorkFlow(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - generated xml: "
						+ sb.toString());
			}
			response.setContentType("application/xml");

			if (logger.isDebugEnabled()) {
				logger.debug("refreshJobWorkFlow(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - xml movie name: "
						+ sb.toString());
			}
			response.getWriter().write(sb.toString());
		} catch (Exception e) {
			logger.error(
					"refreshJobWorkFlow(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);

			logger.error(
					"refreshJobWorkFlow(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
					e);
		}
		return null;
	}

	public void setStatusOfTabs(String jobType, HttpServletRequest request,
			JobDashboardForm bean) {
		String ticketType = null;
		if (logger.isDebugEnabled()) {
			logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobggggggggggggggggg------- "
					+ jobType);
		}
		// ticketType = request.getParameter("ticketType");
		ticketType = bean.getTicketType();

		// Logger for finding error
		if (logger.isDebugEnabled()) {
			logger.debug("Ticket type in setStatusOfTabs : for job id : "
					+ bean.getJobId() + " is: " + ticketType);
		}

		if (ticketType != null && !ticketType.trim().equalsIgnoreCase(""))
			request.setAttribute("fromStatus", "fromStatus");
		if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
			request.setAttribute("isTicket", "Ticket");
			request.setAttribute("tabStatus", "job");
		} else {
			if (jobType != null && !jobType.trim().equalsIgnoreCase("")
					&& jobType.trim().equalsIgnoreCase("Default"))
				request.setAttribute("tabStatus", "Default");
			if (jobType != null && !jobType.trim().equalsIgnoreCase("")
					&& jobType.trim().equalsIgnoreCase("Addendum"))
				request.setAttribute("tabStatus", "Addendum");
			else if (jobType != null && jobType.trim().equalsIgnoreCase("")) {
				if (logger.isDebugEnabled()) {
					logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobggggggggggggggggg");
				}
				request.setAttribute("tabStatus", "job");
			}
			request.setAttribute("isTicket", "job");
		}
		// request.setAttribute("tabStatus",jobType);
	}

	public void setSetupTabInfoInForm(JobDashboardBean jobDashboardForm,
			DataSource ds) {
		SetupTab setupTab = new SetupTab();
		JobDashboard jobDashboard = new JobDashboard();
		JobDAO jobDao = new JobDAO();

		GeneralJobInfo generalJobInfo = jobDao.getGeneralJobInfo(
				Long.parseLong(jobDashboardForm.getJobId()), ds);
		setupTab.setGeneralJobInfo(generalJobInfo);

		JobPlanningNote jobPlanningNote = (JobPlanningNote) jobDao
				.getJobPlanningNotes(
						Long.parseLong(jobDashboardForm.getJobId()), ds);
		setupTab.setJobPlanningNote(jobPlanningNote);

		boolean showEvenIfNoPending = true;
		JobWorkflowChecklist topPendingCheckListItems = ((JobWorkflowChecklist) JobLevelWorkflowDAO
				.getJobWorkflowChecklist(
						Long.parseLong(jobDashboardForm.getJobId()), ds))
				.getTopPendingItems(2, showEvenIfNoPending);

		setupTab.setTopPendingCheckListItems(topPendingCheckListItems);

		JobActivities jobActivities = (JobActivities) jobDao.getActivityList(
				jobDashboardForm.getJobId(), ds);
		setupTab.setJobActivities(jobActivities);

		jobDashboard.setJobDashboardTab(setupTab);
		Convertor.convertToJobDashboardForm(jobDashboard, jobDashboardForm);
		if (logger.isDebugEnabled()) {
			logger.debug("setSetupTabInfoInForm(JobDashboardForm, DataSource) - COUNTRY "
					+ jobDashboardForm.getSite_country()
					+ " ,"
					+ jobDashboardForm.isUs());
		}
	}

	public void setGeneralFormInfo(JobDashboardForm bean,
			HttpServletRequest request) {
		bean.setTabId((request.getParameter("tabId") == null ? ""
				+ JobDashboardTabType.SETUP_TAB : request.getParameter("tabId")));
		String loginUserName = (String) request.getSession().getAttribute(
				"username");
		bean.setCreatedBy(loginUserName);
		bean.setJobId((String) (request.getParameter("Job_Id") == null ? request
				.getAttribute("jobid") : request.getParameter("Job_Id")));
		// To open general job Dashboard.
		if (request.getParameter("jobid") != null) {
			bean.setJobId(request.getParameter("jobid"));
		}
		if (request.getAttribute("jobid") != null) {
			bean.setJobId((String) request.getAttribute("jobid"));
		}
		JobDAO jobDAO = new JobDAO();
		TeamInfo teamInfo = jobDAO.getTeamInfo(new Long(bean.getJobId()),
				getDataSource(request, "ilexnewDB"));

		bean.setOwner(teamInfo.getOwner());

		bean.setAppendixId((String) (request.getParameter("appendix_Id") == null ? bean
				.getAppendixId() : request.getParameter("appendix_Id")));

		if (request.getParameter("ticket_id") != null
				&& !request.getParameter("ticket_id").equals("")
				&& !request.getParameter("ticket_id").equals("0")) {
			bean.setTicket_id(request.getParameter("ticket_id"));
		} else if (request.getAttribute("ticket_id") != null) {
			bean.setTicket_id((String) request.getAttribute("ticket_id"));
		} else {
			bean.setTicket_id(bean.getTicket_id());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("setGeneralFormInfo(JobDashboardForm, HttpServletRequest) - appendix_id---"
					+ request.getParameter("appendix_Id"));
		}
		if (logger.isDebugEnabled()) {
			logger.debug("setGeneralFormInfo(JobDashboardForm, HttpServletRequest) - appendixid---"
					+ request.getParameter("appendixid"));
		}
		if (request.getParameter("appendix_Id") == null
				&& request.getParameter("appendixid") != null)
			bean.setAppendixId(request.getParameter("appendixid"));

		int countESA = Jobdao.getAppendixESACount(bean.getAppendixId(),
				getDataSource(request, "ilexnewDB"));
		request.setAttribute("countESA", countESA + "");
	}

	public ActionForward addOutOfScope(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute(
				"tabId",
				(request.getParameter("tabId") == null ? ""
						+ JobDashboardTabType.SETUP_TAB : request
						.getParameter("tabId")));
		if (logger.isDebugEnabled()) {
			logger.debug("addOutOfScope(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - param tab id"
					+ request.getParameter("tabId"));
		}
		JobDashboardForm bean = (JobDashboardForm) form;
		setGeneralFormInfo(bean, request);
		String url = "/EditOOSActivityAction.do?Job_Id=" + bean.getJobId()
				+ "&Appendix_Id=" + bean.getAppendixId() + "&addOOA=addOOA";
		ActionForward fwd = new ActionForward();
		fwd.setPath(url);
		return fwd;
	}

	/**
	 * Job Indicator.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward jobIndicator(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute(
				"tabId",
				(request.getParameter("tabId") == null ? ""
						+ JobDashboardTabType.SETUP_TAB : request
						.getParameter("tabId")));
		if (logger.isDebugEnabled()) {
			logger.debug("jobIndicator(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - param tab id"
					+ request.getParameter("tabId"));
		}
		JobDashboardForm bean = (JobDashboardForm) form;
		JobDashboardBean dto = new JobDashboardBean();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		setGeneralFormInfo(bean, request);

		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {

			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {

			logger.error(e);
			e.printStackTrace();
		}
		setSetupTabInfoInForm(dto, getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {

			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {

			logger.error(e);
			e.printStackTrace();
		}
		String jobType = JobSetUpDao.getjobType(bean.getJobId(),
				getDataSource(request, "ilexnewDB"));
		if (jobType != null && jobType.equalsIgnoreCase("Default")
				|| jobType.equalsIgnoreCase("Addendum")) {
			request.setAttribute("Default", "Default");
		}
		if (jobType != null && jobType.trim().equalsIgnoreCase("Default"))
			jobType = "Default";
		else if (jobType != null && jobType.trim().equalsIgnoreCase("Addendum"))
			jobType = "Addendum";
		else
			jobType = "";
		setStatusOfTabs(jobType, request, bean);
		Map<String, Object> map;

		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		if (request.getParameter("changeTestIndicator") != null) {
			setJobIndicator(bean.getJobId(), loginuserid, request,
					getDataSource(request, "ilexnewDB"));
		}
		return mapping.findForward("JobDashboard");
	}

	/**
	 * Set JobIndicator.
	 * 
	 * @param jobId
	 * @param testIndicator
	 * @param loginUserId
	 * @param request
	 * @param ds
	 */
	private void setJobIndicator(String jobId, String loginUserId,
			HttpServletRequest request, DataSource ds) {
		int jobIndicatorFlag = -1;
		if (request.getParameter("jobIndicator") != null
				&& !request.getParameter("jobIndicator").equals("")) {
			jobIndicatorFlag = JobDashboarddao.changeJobTestIndicator(jobId,
					request.getParameter("jobIndicator"), loginUserId,
					getDataSource(request, "ilexnewDB"));
			request.setAttribute("jobIndicatorFlag", jobIndicatorFlag + "");

			if (request.getParameter("jobIndicator").equals("Internal Test")) {
				Connection mySqlConn = null;
				try {
					String url = EnvironmentSelector
							.getBundleString("appendix.detail.changestatus.mysql.url");
					mySqlConn = MySqlConnection.getMySqlConnection(url);
					DeleteJobdao.mySqlDeleteJob(mySqlConn, jobId);
				} catch (Exception e) {
					logger.error(
							"setJobIndicator(String, String, HttpServletRequest, DataSource)",
							e);

					if (logger.isDebugEnabled()) {
						logger.debug("setJobIndicator(String, String, HttpServletRequest, DataSource) - Exception caught in creating MySql Connection: "
								+ e);
					}
					logger.error(
							"setJobIndicator(String, String, HttpServletRequest, DataSource)",
							e);
				} finally {
					if (mySqlConn != null) {
						try {
							mySqlConn.close();
						} catch (Exception e) {
							logger.error(
									"setJobIndicator(String, String, HttpServletRequest, DataSource)",
									e);

							if (logger.isDebugEnabled()) {
								logger.debug("setJobIndicator(String, String, HttpServletRequest, DataSource) - Exception caught on closed MySql Connection: "
										+ e);
							}
							logger.error(
									"setJobIndicator(String, String, HttpServletRequest, DataSource)",
									e);
						}
					}
				}
			}
		}
	} // - end of setJobIndicator()

}
