package com.mind.newjobdb.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.common.CMPocSearchList;
import com.mind.common.CMSearchList;
import com.mind.common.LabelValue;
import com.mind.common.Util;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.CM.CustomerManagerDao;
import com.mind.dao.PM.Appendixdao;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.formbean.JobDashboardForm;

public class ChangeOrderAction extends com.mind.common.IlexDispatchAction {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ChangeOrderAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
			// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
				"Job Dashboard - Execute Tab",
				getDataSource(request, "ilexnewDB"))) {
			request.setAttribute("HidePage", "HidePage");
			// return( mapping.findForward( "UnAuthenticate" ) ); // - Check for
			// Pager Security.
		}

		JobDashboardForm bean = (JobDashboardForm) form;

		JobDAO jobDao = new JobDAO();
		GeneralJobInfo generalJobInfo = jobDao.getGeneralJobInfo(
				Long.parseLong(bean.getJobId()),
				getDataSource(request, "ilexnewDB"));

		String mailId = JobDAO.getEmailId(loginuserid,
				getDataSource(request, "ilexnewDB"));
		String senderSign = "Sincerely,\n"
				+ (String) session.getAttribute("username") + "\n"
				+ "Contingent Network Services, LLC\n"
				+ "4400 Port Union Rd.\n" + "West Chester, Ohio 45011\n"
				+ "desk (800) 506-9609 x\n" + "fax  (888) 556-0932\n"
				+ "medius.contingent.com";

		bean.setSite_number(generalJobInfo.getSiteInfo().getSite_number());
		bean.setCustomerName(generalJobInfo.getSiteInfo().getSite_name().trim());
		// bean.setProjectNameCombo(generalJobInfo.getSiteInfo().getSite_name());
		bean.setSite_address(generalJobInfo.getSiteInfo().getSite_address());
		bean.setSite_city(generalJobInfo.getSiteInfo().getSite_city());
		bean.setSite_state(generalJobInfo.getSiteInfo().getSite_state());
		bean.setSite_zipcode(generalJobInfo.getSiteInfo().getSite_zip());
		bean.setProjectName(Appendixdao.getAppendixname(
				getDataSource(request, "ilexnewDB"), bean.getAppendixId()));
		bean.setAppendixId(bean.getAppendixId());
		bean.setJobId(bean.getJobId());
		bean.setTabId(request.getParameter("type"));
		bean.setType(bean.getType());
		bean.setTicketType(request.getParameter("ticketType"));
		bean.setTicket_id(request.getParameter("ticket_id"));
		bean.setSenderEmailId(mailId);
		bean.setProposedChange((request.getParameter("installNote") == null ? ""
				: request.getParameter("installNote")));
		bean.setSenderSignatures(senderSign);
		// ////////////////////////////////////////////

		String customerName = CustomerManagerDao.getEndCustomerNameByMSAId(
				(String) session.getAttribute("msaId"),
				getDataSource(request, "ilexnewDB"));
		if (customerName.equals("")) {
			customerName = bean.getCustomerName();
		}

		ArrayList searchList = CustomerManagerDao.getSearchResult("C",
				customerName.replaceAll("[^a-zA-Z0-9 ]", "_").trim() + "%",
				"%", "0", getDataSource(request, "ilexnewDB"));
		// replacing all special chars with _ i-e wildcard entry for DB in
		// Customer Name
		String divisionId = "";
		ArrayList pL = null;
		if (searchList != null) {
			for (int i = 0; i < searchList.size(); i++) {
				CMSearchList cml = (CMSearchList) searchList.get(i);

				pL = CustomerManagerDao.getPOCSearchResult(cml.getLo_om_id(),
						"%%", getDataSource(request, "ilexnewDB"));
				if (pL != null)
					break;
			}
		}

		ArrayList<LabelValue> pocList = new ArrayList<LabelValue>();
		if (pL != null) {
			for (int i = 0; i < pL.size(); i++) {
				CMPocSearchList poc = (CMPocSearchList) pL.get(i);
				pocList.add(new LabelValue(poc.getContact(), poc
						.getLo_pc_email1()));
			}
		}

		// ///////////////////////////////////////////

		ArrayList<LabelValue> hourList = Util.getHours();
		ArrayList<LabelValue> minuteList = Util.getMinutes();
		request.setAttribute("pocList", pocList);
		request.setAttribute("hourList", hourList);
		request.setAttribute("minuteList", minuteList);
		request.setAttribute("executeTab", "executeTab");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");

		return (mapping.findForward("success"));
	}
}
