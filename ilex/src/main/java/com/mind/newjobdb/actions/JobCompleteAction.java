package com.mind.newjobdb.actions;

import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobDashboard;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.TeamInfo;
import com.mind.common.EnvironmentSelector;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Email;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.AddContactdao;
import com.mind.dao.PRM.AddInstallationNotesdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.CompleteTab;
import com.mind.newjobdb.business.Convertor;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.dao.JobLevelWorkflowDAO;
import com.mind.newjobdb.dao.JobWorkflowChecklist;
import com.mind.newjobdb.dao.PurchaseOrder;
import com.mind.newjobdb.dao.PurchaseOrderDAO;
import com.mind.newjobdb.dao.PurchaseOrderList;
import com.mind.newjobdb.formbean.JobDashboardForm;
import com.mind.util.WebUtil;

public class JobCompleteAction extends com.mind.common.IlexDispatchAction {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger
            .getLogger(JobCompleteAction.class);

    private static final String P = "Project";
    private static final String N = "NetMedX";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        /* Page Security: Start */
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        if (loginuserid == null) {
            return (mapping.findForward("SessionExpire")); // - Check for
            // session expired.
        }
        if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
                "Job Dashboard - Complete Tab",
                getDataSource(request, "ilexnewDB"))) {
            request.setAttribute("HidePage", "HidePage");
            // return( mapping.findForward( "UnAuthenticate" ) ); // - Check for
            // Pager Security.
        }
        /* Page Security=: End */

        request.setAttribute(
                "tabId",
                (request.getParameter("tabId") == null ? ""
                + JobDashboardTabType.COMPLETE_TAB : request
                .getParameter("tabId")));
        JobDashboardForm bean = (JobDashboardForm) form;
        JobDashboardBean dto = new JobDashboardBean();

        String[] requestorDetails = JobSetUpDao.getRequestorDetailsByTicketId(
                bean.getJobId(), getDataSource(request, "ilexnewDB"));

        if (requestorDetails != null) {
            bean.setTicket_id(requestorDetails[0]);
            bean.setTicketNumber(requestorDetails[1]);
            bean.setRequestor(requestorDetails[2]);
            bean.setRequestorEmail(requestorDetails[3]);
        }

        bean.setTabId("" + JobDashboardTabType.COMPLETE_TAB);

        if (bean.getJobId() == null || bean.getJobId().equalsIgnoreCase("")) {
            setGeneralFormInfo(bean, request);
        }
        String appendixId = IMdao.getAppendixId(bean.getJobId(),
                getDataSource(request, "ilexnewDB"));
        Map<String, Object> map = new HashMap<String, Object>();

        bean.setAppendixId(appendixId);
        try {
            BeanUtils.copyProperties(dto, bean);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }
        JobDAO.setLatestInstallNotes(dto, getDataSource(request, "ilexnewDB"));

        JobDAO.setLatestComments(map, dto, getDataSource(request, "ilexnewDB"));
        WebUtil.copyMapToRequest(request, map);
        setCompleteTabInfoInForm(dto, getDataSource(request, "ilexnewDB"));
        try {
            BeanUtils.copyProperties(bean, dto);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }
        String jobType = JobSetUpDao.getjobType(bean.getJobId(),
                getDataSource(request, "ilexnewDB"));
        if (jobType != null && jobType.trim().equalsIgnoreCase("Default")) {
            jobType = "Default";
        } else if (jobType != null && jobType.trim().equalsIgnoreCase("Addendum")) {
            jobType = "Addendum";
        } else {
            jobType = "";
        }

        setStatusOfTabs(jobType, request, bean);

        // request.setAttribute("SAVE",(request.getParameter("save")==null?"":"Activity added successfully."));
        if (bean.getJobId() != null
                && !bean.getJobId().trim().equalsIgnoreCase("")) {
            map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
                    getDataSource(request, "ilexnewDB"));
            WebUtil.copyMapToRequest(request, map);
        }
        String msaName = Appendixdao.getMsaname(bean.getAppendixId(),
                getDataSource(request, "ilexnewDB"));
        bean.setMsaName(msaName);
        return mapping.findForward("success");
    }

    public void setStatusOfTabs(String jobType, HttpServletRequest request,
            JobDashboardForm bean) {
        String ticketType = null;
        // ticketType = request.getParameter("ticket_type");
        ticketType = bean.getTicketType();
        if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
            request.setAttribute("fromStatus", "fromStatus");
        }
        if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
            request.setAttribute("isTicket", "Ticket");
            request.setAttribute("tabStatus", "job");
        } else {
            if (jobType != null && !jobType.trim().equalsIgnoreCase("")
                    && jobType.trim().equalsIgnoreCase("Default")) {
                request.setAttribute("tabStatus", "Default");
            }
            if (jobType != null && !jobType.trim().equalsIgnoreCase("")
                    && jobType.trim().equalsIgnoreCase("Addendum")) {
                request.setAttribute("tabStatus", "Addendum");
            } else if (jobType != null && jobType.trim().equalsIgnoreCase("")) {
                request.setAttribute("tabStatus", "job");
            }
            request.setAttribute("isTicket", "job");
        }
        // request.setAttribute("tabStatus",jobType);
    }

    public void setCompleteTabInfoInForm(JobDashboardBean jobDashboardForm,
            DataSource ds) {
        CompleteTab CompleteTab = new CompleteTab();
        JobDashboard jobDashboard = new JobDashboard();
        JobDAO jobDao = new JobDAO();
        CompleteTab.setAppendixId(jobDashboardForm.getAppendixId());
        CompleteTab.setJobId(jobDashboardForm.getJobId());
        GeneralJobInfo generalJobInfo = jobDao.getGeneralJobInfo(
                Long.parseLong(jobDashboardForm.getJobId()), ds);
        CompleteTab.setGeneralJobInfo(generalJobInfo);

        // JobPlanningNote jobPlanningNote =
        // (JobPlanningNote)jobDao.getJobPlanningNotes(Long.parseLong(jobDashboardForm.getJobId()),ds);
        // CompleteTab.setJobPlanningNote(jobPlanningNote);
        boolean showEvenIfNoPending = true;
        JobWorkflowChecklist topPendingCheckListItems = ((JobWorkflowChecklist) JobLevelWorkflowDAO
                .getJobWorkflowChecklist(
                        Long.parseLong(jobDashboardForm.getJobId()), ds))
                .getTopPendingItems(2, showEvenIfNoPending);

        CompleteTab.setTopPendingCheckListItems(topPendingCheckListItems);

        ArrayList<PurchaseOrder> listOfPurchaseOrders = PurchaseOrderDAO
                .getPurchaseOrderList(jobDashboardForm.getJobId(), ds);
        PurchaseOrderList purchaseOrderList = new PurchaseOrderList();
        purchaseOrderList.setPurchaseOrders(listOfPurchaseOrders);
        CompleteTab.setPurchaseOrderList(purchaseOrderList);
        // CostVariations totalCostVariations =
        // PurchaseOrderList.getTotalCostVariations(purchaseOrderList);
        // CompleteTab.setCostVariations(totalCostVariations);
        jobDashboard.setJobDashboardTab(CompleteTab);
        Convertor.convertToJobDashboardForm(jobDashboard, jobDashboardForm);
    }

    public void setGeneralFormInfo(JobDashboardForm bean,
            HttpServletRequest request) {
        bean.setTabId((request.getParameter("tabId") == null ? ""
                + JobDashboardTabType.COMPLETE_TAB : request
                .getParameter("tabId")));
        String loginUserName = (String) request.getSession().getAttribute(
                "username");
        bean.setCreatedBy(loginUserName);
        bean.setJobId((String) (request.getParameter("Job_Id") == null ? request
                .getAttribute("jobid") : request.getParameter("Job_Id")));
        // To open general job Dashboard.
        if (request.getParameter("jobid") != null) {
            bean.setJobId(request.getParameter("jobid"));
        }
        if (request.getAttribute("jobid") != null) {
            bean.setJobId((String) request.getAttribute("jobid"));
        }
        JobDAO jobDAO = new JobDAO();
        TeamInfo teamInfo = jobDAO.getTeamInfo(new Long(bean.getJobId()),
                getDataSource(request, "ilexnewDB"));

        bean.setOwner(teamInfo.getOwner());

        bean.setAppendixId((String) (request.getParameter("appendix_Id") == null ? bean
                .getAppendixId() : request.getParameter("appendix_Id")));
        bean.setTicket_id(request.getParameter("ticket_id") == null ? bean
                .getTicket_id() : request.getParameter("ticket_id"));
        if (request.getParameter("appendix_Id") == null
                && request.getParameter("appendixid") != null) {
            bean.setAppendixId(request.getParameter("appendixid"));
        }

        if (request.getAttribute("tabId") != null) {
            bean.setTabId((String) request.getAttribute("tabId"));
        }
    }

    public ActionForward POaction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        return mapping.findForward("success");
    }

    public void addComments(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        if (logger.isDebugEnabled()) {
            logger.debug("addComments(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - in Comments");
        }
        String loginuserid = (String) request.getSession(false).getAttribute(
                "userid");
        String loginUserName = (String) request.getSession(false).getAttribute(
                "username");

        String loginUserEmail = (String) request.getSession(false)
                .getAttribute("useremail");

        JobDashboardForm bean = (JobDashboardForm) form;
        // JobDashboardBean dto = new JobDashboardBean();

        String jobId = request.getParameter("jobId");
        String ticket_id = request.getParameter("ticketId");
        String installationNotes = request.getParameter("installationNotes");
        String instalationTypeId = request.getParameter("instalationTypeId");
        String appendixId = IMdao.getAppendixId(jobId,
                getDataSource(request, "ilexnewDB"));

        String reqEmail = Jobdao.getEmailRecipientsByJob(jobId, getDataSource(request, "ilexnewDB"));

        // boolean isSendEmail = Boolean.valueOf(request
        // .getParameter("isSendEmail"));
        try {
            installationNotes = URLDecoder.decode(installationNotes, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        AddInstallationNotesdao.addinstallnotes(jobId, installationNotes,
                loginuserid, getDataSource(request, "ilexnewDB"), "", "0", "0",
                "", instalationTypeId);

        // getting info
        String[] pocdetails = new String[16];
        // String[] cnsPrjManangerDetail = new String[5];
        // String[] teamLeadDetail = new String[5];
        pocdetails = com.mind.dao.PRM.Appendixdao.AppendixpocdetailsInfo(
                appendixId, getDataSource(request, "ilexnewDB"));
        bean.setPrimary_Name(pocdetails[0]);
        bean.setPocTitle(pocdetails[14]);
        bean.setPocAddress(pocdetails[13]);
        bean.setPocPhone(pocdetails[1]);
        bean.setPocEmail(pocdetails[12]);
		// DEFINE

        // getting company name
        bean.setMsaId(IMdao.getMSAId(appendixId,
                getDataSource(request, "ilexnewDB")));
        bean.setMsaName(Appendixdao.getMsaname(appendixId,
                getDataSource(request, "ilexnewDB")));

        // getMsaname(
        // getDataSource(request, "ilexnewDB"), bean.getMsaId()));
        String companyName = bean.getMsaName();

        String xml = JobDAO.setLatestInstallNotes(jobId,
                getDataSource(request, "ilexnewDB"));
        // response.setContentType("application/xml");
        response.setContentType("application/xml; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        // boolean isSendEmail = true;
        try {
            response.getWriter().write(xml);

            String pOrN = N;
            if (ticket_id == null || ticket_id.equals("")) {
                pOrN = P;
            }

            sendEmailNotes(reqEmail, jobId, ticket_id, installationNotes,
                    loginuserid, loginUserName, loginUserEmail, request,
                    AddContactdao.getEscalationContacts(appendixId,
                            getDataSource(request, "ilexnewDB")), companyName,
                    pOrN, loginUserEmail, getDataSource(request, "ilexnewDB"), bean);

        } catch (Exception e) {
            logger.error(
                    "addComments(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                    e);

            logger.error(
                    "addComments(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)",
                    e);
        }
    }

    private void sendEmailNotes(String emailRecipients, String jobId,
            String ticket_id, String note, String userId, String userName, String loginUserEmail,
            HttpServletRequest request, List<String[]> contacts,
            String companyName, String pOrN, String loginuseremail, DataSource ds, JobDashboardForm bean) {

        EmailBean emailform = new EmailBean();

        emailform.setUsername(this.getResources(request).getMessage(
                "common.Email.username"));
        emailform.setUserpassword(this.getResources(request).getMessage(
                "common.Email.userpassword"));

        emailform.setSmtpservername(EnvironmentSelector
                .getBundleString("common.Email.smtpservername"));
        emailform.setSmtpserverport(this.getResources(request).getMessage(
                "common.Email.smtpserverport"));

        emailform.setFrom(Emailtemplate.SENDER_EMAIL);

        if (emailRecipients.equals("")) {
            emailRecipients = loginUserEmail;
        } else if (!emailRecipients.contains(loginUserEmail)) {
            emailRecipients = loginUserEmail + "," + emailRecipients;
        }
        emailform.setTo(emailRecipients);

        String mailBody = "";

        String JobName = Jobdao.getJobname(jobId, getDataSource(request, "ilexnewDB"));
        String[] notesParam = new String[2];
        notesParam[0] = "";
        notesParam[1] = note;
        if (pOrN.equals(P)) {
            emailform.setSubject("(" + jobId + "-" + userId + ") - Work Notes");
            mailBody = Emailtemplate.buildJobEmailBody(getDataSource(request, "ilexnewDB"), userId, userName, bean.getMsaName(), JobName, jobId, Emailtemplate.JobType.PROJECTS, Emailtemplate.EmailType.WORK_NOTE, AddContactdao.getEscalationContacts(IMdao.getAppendixId(jobId, getDataSource(request, "ilexnewDB")), getDataSource(request, "ilexnewDB")), bean);
        } else {
            emailform.setSubject("(" + jobId + "-" + userId + ") - " + JobName + " - Work Notes");
            mailBody = Emailtemplate.buildJobEmailBody(getDataSource(request, "ilexnewDB"), userId, userName, bean.getMsaName(), JobName, jobId, Emailtemplate.JobType.NETMEDX, Emailtemplate.EmailType.WORK_NOTE, AddContactdao.getEscalationContacts(IMdao.getAppendixId(jobId, getDataSource(request, "ilexnewDB")), getDataSource(request, "ilexnewDB")), bean);
        }

        emailform.setContent(mailBody);

        Map<String, String> inlineImages = new HashMap<String, String>();
        String filePath = System.getProperty("catalina.base");
        String filepath = filePath
                + "//webapps//Ilex//images//ampm_header_v2.png";
        // System.out.println("--File location is " + filePath);
        inlineImages.put("image1", filepath);

        Email.send(emailform, "text/html", mailBody.toString(), inlineImages, ds);
    }

}
