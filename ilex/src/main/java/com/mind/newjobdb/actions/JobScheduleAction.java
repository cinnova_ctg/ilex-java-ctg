package com.mind.newjobdb.actions;

import com.mind.bean.DynamicComboCNSWEB;
import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.JobDashboard;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.ScheduleTab;
import com.mind.bean.newjobdb.TeamInfo;
import com.mind.common.ChangeDateFormat;
import com.mind.common.EnvironmentSelector;
import com.mind.common.Util;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.DynamicComboDao;
import com.mind.common.dao.Email;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.AddContactdao;
import com.mind.dao.PRM.AddInstallationNotesdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.DispatchScheduledao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.dao.PRM.Scheduledao;
import com.mind.dao.PRM.objects.ScheduleElement;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.Convertor;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.dao.JobScheduleDAO;
import com.mind.newjobdb.formbean.JobDashboardForm;
import com.mind.util.WebUtil;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class JobScheduleAction extends com.mind.common.IlexDispatchAction {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(JobScheduleAction.class);

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        /* Page Security: Start */
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        if (loginuserid == null) {
            return (mapping.findForward("SessionExpire")); // - Check for session expired.
        }
        if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job", "Job Dashboard - Schedule Tab", getDataSource(request, "ilexnewDB"))) {
            request.setAttribute("HidePage", "HidePage"); // - Check for Page Security.
        }
        /* Page Security=: End */

        request.setAttribute("tabId", (request.getParameter("tabId") == null ? "" + JobDashboardTabType.SCHEDULE_TAB : request.getParameter("tabId")));
        if (logger.isDebugEnabled()) {
            logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - param tab id" + request.getParameter("tabId"));
        }
        JobDashboardForm bean = (JobDashboardForm) form;
        JobDashboardBean dto = new JobDashboardBean();
        bean.setTabId("" + JobDashboardTabType.SCHEDULE_TAB);

        setGeneralFormInfo(bean, request);
        try {
            BeanUtils.copyProperties(dto, bean);
        } catch (IllegalAccessException e1) {
            logger.error(e1);
            e1.printStackTrace();
        } catch (InvocationTargetException e1) {
            logger.error(e1);
            e1.printStackTrace();
        }

        setScheduleTabInfoInForm(dto, getDataSource(request, "ilexnewDB"));
        try {
            BeanUtils.copyProperties(bean, dto);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }

        /*JobScheduleDAO.setScheduleDates(dto, getDataSource(request, "ilexnewDB"));
         try {
         BeanUtils.copyProperties(bean, dto);
         } catch (IllegalAccessException e) {
         logger.error(e);
         e.printStackTrace();
         } catch (InvocationTargetException e) {
         logger.error(e);
         e.printStackTrace();
         }
         setJobDispatchScheduleInfo(bean, request);*/
        String jobType = JobSetUpDao.getjobType(bean.getJobId(), getDataSource(request, "ilexnewDB"));
        if (logger.isDebugEnabled()) {
            logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - job type---- " + jobType);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - JOB TYPE" + jobType + " job id " + bean.getJobId());
        }

        if (jobType != null && jobType.trim().equalsIgnoreCase("Default")) {
            jobType = "Default";
        } else if (jobType != null && jobType.trim().equalsIgnoreCase("Addendum")) {
            jobType = "Addendum";
        } else {
            jobType = "";
        }

        if (logger.isDebugEnabled()) {
            logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - bean.getOwner()" + bean.getOwner());
        }

        setStatusOfTabs(jobType, request, bean);
        Map<String, Object> map;

        if (bean.getJobId() != null && !bean.getJobId().trim().equalsIgnoreCase("")) {
            map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid, getDataSource(request, "ilexnewDB"));
            WebUtil.copyMapToRequest(request, map);
        }

        fillCombo(bean, request);
        return mapping.findForward("success");
    }

    public void setJobDispatchScheduleInfo(JobDashboardForm bean, HttpServletRequest request) {
        ArrayList dispatchScheduleList = JobScheduleDAO.getJobDispatchScheduleList(bean.getJobId(), "J", getDataSource(request, "ilexnewDB"));
        request.setAttribute("dispatchScheduleList", dispatchScheduleList);
        request.setAttribute("listSize", dispatchScheduleList.size());
        bean.setRownum(dispatchScheduleList.size() + "");
        bean.setDeletenum(dispatchScheduleList.size() + "");
        bean.setPage("");
        bean.setPageid(bean.getJobId());
    }

    public void setStatusOfTabs(String jobType, HttpServletRequest request, JobDashboardForm bean) {
        String ticketType = null;
        if (logger.isDebugEnabled()) {
            logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobs " + jobType);
        }

        // ticketType = request.getParameter("ticket_type");
        ticketType = bean.getTicketType();
        if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
            request.setAttribute("fromStatus", "fromStatus");
        }

        if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
            request.setAttribute("isTicket", "Ticket");
            request.setAttribute("tabStatus", "job");
        } else {
            if (jobType != null && !jobType.trim().equalsIgnoreCase("") && jobType.trim().equalsIgnoreCase("Default")) {
                request.setAttribute("tabStatus", "Default");
            }
            if (jobType != null && !jobType.trim().equalsIgnoreCase("") && jobType.trim().equalsIgnoreCase("Addendum")) {
                request.setAttribute("tabStatus", "Addendum");
            } else if (jobType != null && jobType.trim().equalsIgnoreCase("")) {
                if (logger.isDebugEnabled()) {
                    logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobs");
                }
                request.setAttribute("tabStatus", "job");
            }
            request.setAttribute("isTicket", "job");
        }
        // request.setAttribute("tabStatus",jobType);
    }

    public void setScheduleTabInfoInForm(JobDashboardBean jobDashboardForm, DataSource ds) {
        ScheduleTab ScheduleTab = new ScheduleTab();
        JobDashboard jobDashboard = new JobDashboard();
        JobDAO jobDao = new JobDAO();
        if (logger.isDebugEnabled()) {
            logger.debug("setScheduleTabInfoInForm(JobDashboardForm, DataSource) - appendix+++++++++" + jobDashboardForm.getAppendixId());
        }
        ScheduleTab.setAppendixId(jobDashboardForm.getAppendixId());
        ScheduleTab.setJobId(jobDashboardForm.getJobId());
        GeneralJobInfo generalJobInfo = jobDao.getGeneralJobInfo(Long.parseLong(jobDashboardForm.getJobId()), ds);
        ScheduleTab.setGeneralJobInfo(generalJobInfo);

        jobDashboard.setJobDashboardTab(ScheduleTab);
        Convertor.convertToJobDashboardForm(jobDashboard, jobDashboardForm);
        if (logger.isDebugEnabled()) {
            logger.debug("setScheduleTabInfoInForm(JobDashboardForm, DataSource) - COUNTRY " + jobDashboardForm.getSite_country() + " ," + jobDashboardForm.isUs());
        }
    }

    public void setGeneralFormInfo(JobDashboardForm bean, HttpServletRequest request) {
        bean.setTabId((request.getParameter("tabId") == null ? "" + JobDashboardTabType.SCHEDULE_TAB : request.getParameter("tabId")));
        String loginUserName = (String) request.getSession().getAttribute("username");
        bean.setCreatedBy(loginUserName);
        bean.setJobId((String) (request.getParameter("Job_Id") == null ? request.getAttribute("jobid") : request.getParameter("Job_Id")));
        // To open general job Dashboard.
        // if (request.getParameter("jobid") != null) {
        // bean.setJobId(request.getParameter("jobid"));
        // } else if (request.getParameter("jobid") == null) {
        // bean.setJobId(request.getParameter("jobId"));
        // }
        // if (request.getAttribute("jobid") != null) {
        // bean.setJobId((String) request.getAttribute("jobid"));
        // }
        JobDAO jobDAO = new JobDAO();
        TeamInfo teamInfo = jobDAO.getTeamInfo(new Long(bean.getJobId()), getDataSource(request, "ilexnewDB"));

        bean.setOwner(teamInfo.getOwner());

        bean.setAppendixId((String) (request.getParameter("appendix_Id") == null ? bean.getAppendixId() : request.getParameter("appendix_Id")));
        bean.setTicket_id(request.getParameter("ticket_id") == null ? bean.getTicket_id() : request.getParameter("ticket_id"));
        if (request.getParameter("appendix_Id") == null && request.getParameter("appendixid") != null) {
            bean.setAppendixId(request.getParameter("appendixid"));
        }

        if (request.getAttribute("tabId") != null) {
            bean.setTabId((String) request.getAttribute("tabId"));
        }
    }

    public void fillCombo(JobDashboardForm bean, HttpServletRequest request) {
        DynamicComboCNSWEB dynamiccomboCNSWEB = new DynamicComboCNSWEB();
        dynamiccomboCNSWEB.setHourname(Util.getHours());
        dynamiccomboCNSWEB.setMinutename(Util.getMinutes());
        dynamiccomboCNSWEB.setOptionname(DynamicComboDao.getOptionName());
        request.setAttribute("dynamiccomboCNSWEB", dynamiccomboCNSWEB);
    }

    public ActionForward POaction(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        return mapping.findForward("success");
    }

    /**
     * Save All job's dispatches and its default install notes.
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
    public ActionForward Save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        /*JobDashboardForm bean = (JobDashboardForm) form;
         HttpSession session = request.getSession(true);
         String loginuserid = (String) session.getAttribute("userid");
         String loginusername = (String) session.getAttribute("username");
         String loginUserEmail = (String) request.getSession(false).getAttribute("useremail");
         boolean isMarkOnsite = Scheduledao.isAccepted();
         String newScheduleStartDate = "";

         if (isMarkOnsite) {
         ScheduleElement[] prevs = new ScheduleElement[bean.getStartdate().length];
         for (int i = 0; i < bean.getStartdate().length; i++) {
         int z = i + 1;
         prevs[i] = Scheduledao.getScheduleByDispatchId(bean.getJobId(), z, getDataSource(request, "ilexnewDB"));
         }

         for (int i = 0; i < bean.getStartdate().length; i++) {
         IlexTimestamp scheduleStartTimestamp = new IlexTimestamp(bean.getStartdate(i), bean.getStartdatehh(i), bean.getStartdatemm(i), IlexTimestamp.getBooleanAMPM(bean.getStartdateop(i)));
         IlexTimestamp scheduleEndTimestamp = new IlexTimestamp(bean.getScheduleEnddate(i), bean.getScheduleEndhh(i), bean.getScheduleEndmm(i), IlexTimestamp.getBooleanAMPM(bean.getScheduleEndop(i)));
         IlexTimestamp actualStartTimestamp = new IlexTimestamp(bean.getActstartdate(i), bean.getActstartdatehh(i), bean.getActstartdatemm(i), IlexTimestamp.getBooleanAMPM(bean.getActstartdateop(i)));
         IlexTimestamp actualEndTimestamp = new IlexTimestamp(bean.getActenddate(i), bean.getActenddatehh(i), bean.getActenddatemm(i), IlexTimestamp.getBooleanAMPM(bean.getActenddateop(i)));

         String scheduleStartDate = IlexTimestamp.getDateTimeType1(scheduleStartTimestamp);
         String scheduleEndDate = IlexTimestamp.getDateTimeType1(scheduleEndTimestamp);
         String actualStartDate = IlexTimestamp.getDateTimeType1(actualStartTimestamp);
         String actualEndDate = IlexTimestamp.getDateTimeType1(actualEndTimestamp);

         // Adding contact Information
         String appendixId = IMdao.getAppendixId(bean.getJobId(), getDataSource(request, "ilexnewDB"));

         // String AppendixName= Jobdao.getAppendixname(
         // getDataSource(request, "ilexnewDB"),
         // appendixId);
         String[] pocdetails = new String[16];
         // String[] cnsPrjManangerDetail = new String[5];
         // String[] teamLeadDetail = new String[5];
         pocdetails = com.mind.dao.PRM.Appendixdao.AppendixpocdetailsInfo(appendixId, getDataSource(request, "ilexnewDB"));
         bean.setPrimary_Name(pocdetails[0]);
         bean.setPocTitle(pocdetails[14]);
         bean.setPocAddress(pocdetails[13]);
         bean.setPocPhone(pocdetails[1]);
         bean.setPocEmail(pocdetails[12]);
         // ///////////////////////////////////////
         // String LeadName = null;
         // String LeadAddress = null;
         // String LeadEmail = null;

         String reqEmail = Jobdao.getEmailRecipientsByJob(bean.getJobId(), getDataSource(request, "ilexnewDB"));

         // getting company name
         bean.setMsaId(IMdao.getMSAId(appendixId, getDataSource(request, "ilexnewDB")));
         bean.setMsaName(Appendixdao.getMsaname(appendixId, getDataSource(request, "ilexnewDB")));

         // getMsaname(
         // getDataSource(request, "ilexnewDB"), bean.getMsaId()));
         String companyname = bean.getMsaName();

         // ////////////////
         if (i == 0) {
         newScheduleStartDate = scheduleStartDate;
         }

         int z = i + 1;

         int retValArray[] = new int[2];
         retValArray = Scheduledao.addScheduleForPRM(bean.getJobId(), actualStartDate, actualEndDate, "J", loginuserid, scheduleStartDate, scheduleEndDate, i, getDataSource(request, "ilexnewDB"));
         String newScheduleId = String.valueOf(retValArray[1]);

         ScheduleElement se = Scheduledao.getScheduleByDispatchId(bean.getJobId(), z, getDataSource(request, "ilexnewDB"));

         Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Handling change to dispatch " + i, "");
         Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Dispatch Changes - Prev", "" + (prevs[i] != null));
         if (prevs[i] != null) {
         Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Dispatch Changes - " + prevs[i].getActualStart() + " != " + se.getActualStart(), "" + (!se.getActualStart().equals(prevs[i].getActualStart())));
         Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Dispatch Changes - " + prevs[i].getActualEnd() + " != " + se.getActualEnd(), "" + (!se.getActualEnd().equals(prevs[i].getActualEnd())));
         Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Dispatch Changes - " + prevs[i].getPlannedStart() + " != " + se.getPlannedStart(), "" + (!se.getPlannedStart().equals(prevs[i].getPlannedStart())));
         Jobdao.addLogEntry(getDataSource(request, "ilexnewDB"), "Dispatch Changes - " + prevs[i].getPlannedEnd() + " != " + se.getPlannedEnd(), "" + (!se.getPlannedEnd().equals(prevs[i].getPlannedEnd())));
         }

         if (prevs[i] == null || !se.getActualStart().equals(prevs[i].getActualStart()) || !se.getActualEnd().equals(prevs[i].getActualEnd())
         || !se.getPlannedStart().equals(prevs[i].getPlannedStart()) || !se.getPlannedEnd().equals(prevs[i].getPlannedEnd())) {
         if (se != null) {
         sendEmailDispatch(bean, se, loginuserid, loginusername, request, companyname, scheduleStartDate, scheduleEndDate, actualStartDate, actualEndDate, reqEmail, i + 1, loginUserEmail, getDataSource(request, "ilexnewDB"));
         }
         }

         // Add Install notes: Start
         if (!actualStartDate.equals("")
         && !actualStartDate.equals("01/01/1900")) {
         String defaultset = "Tech onsite for Site Visit " + (i + 1);
         AddInstallationNotesdao.addinstallnotes(bean.getJobId(),
         defaultset, loginuserid,
         getDataSource(request, "ilexnewDB"),
         actualStartDate, bean.getScheduleid(),
         newScheduleId, "S");
         }
         if (!actualEndDate.equals("")
         && !actualEndDate.equals("01/01/1900")) {
         String defaultset = "Tech offsite for Site Visit "
         + (i + 1);
         AddInstallationNotesdao.addinstallnotes(bean.getJobId(),
         defaultset, loginuserid,
         getDataSource(request, "ilexnewDB"), actualEndDate,
         bean.getScheduleid(), newScheduleId, "E");
         }
         // Add Install notes: End

         } // - End for loop

         // For Job Reschedule: Start
         if (bean.getRseType() != null
         && !bean.getRseType().equals("Administrative")) {
         Scheduledao.setReschedule(bean.getJobId(), bean.getRseType(),
         bean.getOldSchStart(), newScheduleStartDate,
         bean.getRseInternalJustification(), loginuserid,
         getDataSource(request, "ilexnewDB"));
         }
         // For Job Reschedule: End

         } // - end of if(inMarkOnsite)*/

        return mapping.findForward("save");
    }

    /**
     * To delete job's Schedules.
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
    public ActionForward Delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        /*JobDashboardForm bean = (JobDashboardForm) form;
         if (request.getParameter("scheduleId") != null
         && !request.getParameter("scheduleId").equals("")) {
         DispatchScheduledao.deleteDispatchSchedule(bean.getJobId(), "J",
         request.getParameter("scheduleId"),
         getDataSource(request, "ilexnewDB"));
         }*/
        return mapping.findForward("delete");
    }

    /**
     * Discription: This method is used when any job is rescheduled. Its provied
     * a user interface to lock reschedule action.
     */
    public ActionForward reschedule(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        /*response.setHeader("pragma", "no-cache");// HTTP 1.1
         response.setHeader("Cache-Control", "no-cache");
         response.setHeader("Cache-Control", "no-store");
         response.addDateHeader("Expires", -1);
         response.setDateHeader("max-age", 0);
         response.setIntHeader("Expires", -1); // - prevents caching at the proxy
         // server
         response.addHeader("cache-Control", "private");

         if (!request.getParameter("hide").equals("hide")) {

         JobDashboardForm bean = (JobDashboardForm) form;
         String date = "";
         String hour = "";
         String min = "";
         String aaa = "";
         // String startDate = "";
         String jobId = "";
         String currStartDateTime = "";
         boolean check = true;

         if (request.getParameter("date") != null) {
         date = request.getParameter("date");
         }
         if (request.getParameter("hh") != null) {
         hour = request.getParameter("hh");
         }
         if (request.getParameter("mm") != null) {
         min = request.getParameter("mm");
         }
         if (request.getParameter("aa") != null) {
         aaa = request.getParameter("aa");
         }
         if (request.getParameter("jobId") != null) {
         jobId = request.getParameter("jobId");
         }

         // startDate =date+" "+hour+":"+min+" "+aaa;
         IlexTimestamp newStartDateTimestamp = new IlexTimestamp(date, hour,
         min, IlexTimestamp.getBooleanAMPM(aaa));
         String newStartDateTime = IlexTimestamp
         .getDateTimeType1(newStartDateTimestamp);

         // Get and Set current schedule date
         currStartDateTime = Scheduledao.checkStartDate(jobId,
         getDataSource(request, "ilexnewDB"));
         // Conver date from MM/dd/yyyy hh:mm aaa to yyyy-MM-dd HH:mm
         if (!currStartDateTime.equalsIgnoreCase("NoRes")) {
         currStartDateTime = ChangeDateFormat.getDateFormat(
         currStartDateTime, "MM/dd/yyyy hh:mm aaa",
         "yyyy-MM-dd HH:mm:ss");
         }

         bean.setOldSchStart(currStartDateTime);

         if (newStartDateTime.equals(currStartDateTime)) {
         check = false;
         } // - when scheduled start date not changed.

         // When actual start date exists or job is in Not scheduled state.
         // else if (currStartDateTime.equals("NoRes")) {
         // check = false;
         // }
         response.setContentType("application/xml");
         if (check) {
         RequestDispatcher rd = request
         .getRequestDispatcher("/PRM/JobReschedule.jsp"); // -
         // inclued
         // JobReschedule.jsp
         rd.include(request, response);
         }

         }*/
        return null;
    }

    //
    // add New function
    private void sendEmailDispatch(JobDashboardForm bean, ScheduleElement se, String userId, String userName, HttpServletRequest request, String companyname,
            String scheduleStartDate, String scheduleEndDate, String actualStartDate, String actualEndDate, String emailRecipients,
            int i, String loginUserEmail, DataSource ds) {
        /*EmailBean emailform = new EmailBean();

         emailform.setUsername(this.getResources(request).getMessage("common.Email.username"));
         emailform.setUserpassword(this.getResources(request).getMessage("common.Email.userpassword"));

         emailform.setSmtpservername(EnvironmentSelector.getBundleString("common.Email.smtpservername"));
         emailform.setSmtpserverport(this.getResources(request).getMessage("common.Email.smtpserverport"));

         emailform.setFrom(Emailtemplate.SENDER_EMAIL);
         if (emailRecipients.equals("")) {
         emailRecipients = loginUserEmail;
         } else if (!emailRecipients.contains(loginUserEmail)) {
         emailRecipients = loginUserEmail + "," + emailRecipients;
         }
         emailform.setTo(emailRecipients);

         emailform.setSubject("(" + bean.getJobId() + "-" + userId + ") - Scheduling Update - Dispatch " + (i));

         String mailBody;

         String appendixId = IMdao.getAppendixId(bean.getJobId(),
         getDataSource(request, "ilexnewDB"));
         //
         // String AppendixName = Jobdao.getAppendixname(
         // getDataSource(request, "ilexnewDB"), appendixId);

         String JobName = Jobdao.getJobname(bean.getJobId(),
         getDataSource(request, "ilexnewDB"));

         mailBody = Emailtemplate.buildJobEmailBody_DispatchScheduled(getDataSource(request, "ilexnewDB"), userId, userName, bean.getMsaName(), JobName, bean.getJobId(), Emailtemplate.JobType.PROJECTS, AddContactdao.getEscalationContacts(bean.getAppendixId(), getDataSource(request, "ilexnewDB")), se);

         emailform.setContent(mailBody);

         Map<String, String> inlineImages = new HashMap<String, String>();
         String filePath = System.getProperty("catalina.base");
         String filepath = filePath
         + "//webapps//Ilex//images//ampm_header_v2.png";
         // System.out.println("--File location is " + filePath);
         inlineImages.put("image1", filepath);

         Email.send(emailform, "text/html", mailBody.toString(), inlineImages, ds);*/
    }
}
