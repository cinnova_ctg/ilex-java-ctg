package com.mind.newjobdb.actions;

import java.util.Date;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.common.EnvironmentSelector;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Email;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.newjobdb.formbean.JobDashboardForm;

public class SendNotificationEmail extends com.mind.common.IlexDispatchAction {

    private static final Logger logger = Logger
            .getLogger(SendNotificationEmail.class);

    static ResourceBundle rb = ResourceBundle
            .getBundle("com.mind.properties.ApplicationResources");

    static String Username = rb.getString("common.Email.username"); // - smtp
    // user name
    static String Userpassword = rb.getString("common.Email.userpassword"); // -
    // smtp
    // password
    static String Smtpservername = EnvironmentSelector
            .getBundleString("common.Email.smtpservername"); // - smtp sever
    // name
    static String Smtpserverport = rb.getString("common.Email.smtpserverport"); // -
    // smtp
    // port
    // no.

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        if (loginuserid == null) {
            return (mapping.findForward("SessionExpire")); // - Check for
            // session expired.
        }
        if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
                "Job Dashboard - Execute Tab",
                getDataSource(request, "ilexnewDB"))) {
            request.setAttribute("HidePage", "HidePage");
			// return( mapping.findForward( "UnAuthenticate" ) ); // - Check for
            // Pager Security.
        }

        JobDashboardForm bean = (JobDashboardForm) form;

        String type = request.getParameter("tabId") != null ? request
                .getParameter("tabId") : request.getParameter("type");
        String url = "";
        String jobType = "";

        String appendixId = bean.getAppendixId();

        String jobId = bean.getJobId();
        if (bean.getJobId() != null && !bean.getJobId().equals("")) {
            jobType = JobSetUpDao.getjobType(bean.getJobId(),
                    getDataSource(request, "ilexnewDB"));
        }

        if (appendixId == null || appendixId.trim().equalsIgnoreCase("")) {
            appendixId = IMdao.getAppendixId(jobId,
                    getDataSource(request, "ilexnewDB"));
            bean.setAppendixId(appendixId);
        }

        EmailBean emailForm = new EmailBean();
        String contentType = "text/html";
        String message = "Contingent Change Order Notification <br /><br />";
        message += "Type: " + bean.getType() + " <br />";
        message += "Project Name:  " + bean.getProjectName() + " <br />";
        message += "Company Name: " + bean.getCustomerName() + " <br />";
        message += "Site Number: " + bean.getSite_number() + " <br />";
        message += "Site Address: " + bean.getSite_address() + " <br />";
        message += "Site City: " + bean.getSite_city() + " <br />";
        message += "Site State: " + bean.getSite_state() + " <br />";
        message += "State ZipCode " + bean.getSite_zipcode() + " <br />";
        message += "Customer POC: "
                + (bean.getCustomerPOC() == null ? bean
                .getAlternateCustomerPOC() : bean.getCustomerPOC())
                + " <br />";
        message += "Client Email: " + bean.getClientEmailId() + " <br />";
        message += "Sender Email: " + bean.getSenderEmailId() + " <br />";
        message += "CC Email: " + bean.getCcEmail() + " <br />";
        message += "Date to Initiate: " + bean.getIntiateDate() + " <br />";
        message += "Time to Initiate: " + bean.getIntiateHour() + ":"
                + bean.getIntiateMinute() + " " + bean.getIntiateIsAm()
                + " <br />";
        message += "Proposed Change: " + bean.getProposedChange() + " <br />";
        message += "Estimated Cost Impact: " + bean.getEstimatedCost()
                + " <br />";
        message += "Estimated Schedule Impact: "
                + bean.getEstimatedScheduleImpact() + " <br /><br /><br />";
        message += " " + bean.getStandardNotification() + " <br /><br />";
        message += " " + bean.getSenderSignatures() + " <br />";
		// ////////////////////////////
        // ////////////////////////////

        String to = bean.getSenderEmailId() + "," + bean.getClientEmailId();
        String subject = "Change Order"
                + ((bean.getTypeOption() == "Explicit Authorization Required") ? " (Explicit Authorization Required)"
                : " ") + ": " + bean.getSite_number() + ", "
                + bean.getSite_city() + ", " + bean.getSite_state()
                + bean.getSite_zipcode() + " on " + new Date() + "";
		// /////////////////////////////////////////
        // ////////////////////////////////////////

        emailForm.setTo(to);
        emailForm.setFrom(bean.getSenderEmailId());// Mail From
        emailForm.setSubject(subject);
        emailForm.setContent(message);
        emailForm.setCc((bean.getCcEmail().equals(null) ? "" : bean
                .getCcEmail()));
        emailForm.setBcc("");
        emailForm.setUsername(SendNotificationEmail.Username);
        emailForm.setUserpassword(SendNotificationEmail.Userpassword);
        emailForm.setSmtpservername(SendNotificationEmail.Smtpservername);
        emailForm.setSmtpserverport(SendNotificationEmail.Smtpserverport);
        emailForm.setContentType(contentType);

        Email.Send(emailForm, contentType, getDataSource(request, "ilexnewDB"));

        if (type != null
                && type.trim().equalsIgnoreCase(
                        JobDashboardTabType.EXECUTE_TAB + "")) {
            logger.debug("Check for Integer.ParseInt(appendixId) Job JobContainerAction.unspecified(String) 11111 =>"
                    + appendixId);
            url = "/JobExecuteAction.do?tabId=" + type + "&appendix_Id="
                    + appendixId + "&Job_Id=" + jobId + "&ticketType="
                    + bean.getTicketType();
            ActionForward fwd = new ActionForward();
            fwd.setPath(url);
            logger.info(" -------------------------------Email sent Execute Tab----------------------------------------");
            return fwd;
        }
        if (type != null
                && type.trim().equalsIgnoreCase(
                        JobDashboardTabType.COMPLETE_TAB + "")) {
            url = "/JobDBCompleteAction.do?tabId=" + type + "&appendixId="
                    + appendixId + "&jobId=" + jobId + "&ticketType="
                    + bean.getTicketType();
            ActionForward fwd = new ActionForward();
            fwd.setPath(url);
            logger.info(" -------------------------------Email sent Complete Tab----------------------------------------");
            return fwd;
        }

        logger.info(" -------------------------------Email Not sent----------------------------------------");
        return mapping.findForward("failure");
    }
}
