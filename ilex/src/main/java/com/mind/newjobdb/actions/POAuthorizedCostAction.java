package com.mind.newjobdb.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.PO.dao.PODao;
import com.mind.PO.formbean.POBean;
import com.mind.bean.mpo.PODTO;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.util.WebUtil;

public class POAuthorizedCostAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(POAuthorizedCostAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		POBean poBean = (POBean) form;
		PODTO podto = new PODTO();
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - mpoId "
					+ poBean.getMpoId());
		}
		logger.trace("Start :(Class : com.mind.docm.actions.ViewManupulationAction | Method : addView)");
		PODao poDao = new PODao();
		try {
			BeanUtils.copyProperties(podto, poBean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		poDao.getActivityList(podto, getDataSource(request, "ilexnewDB"));
		String loginuserid = (String) request.getSession(true).getAttribute(
				"userid");
		request.setAttribute("tabId", request.getParameter("tabId"));
		Map<String, Object> map;

		if (poBean.getJobId() != null
				&& !poBean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(poBean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		String mode = request.getParameter("mode");
		request.setAttribute("POStatus", "Draft");
		if (mode != null)
			request.setAttribute("mode", "view");
		return (mapping.findForward("success"));
	}
}
