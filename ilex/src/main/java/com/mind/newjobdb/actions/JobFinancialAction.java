package com.mind.newjobdb.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.newjobdb.FinancialAnalysis;
import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobDashboard;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.TeamInfo;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.Convertor;
import com.mind.newjobdb.business.FinancialTab;
import com.mind.newjobdb.dao.FinancialAnalysisDAO;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.formbean.JobDashboardForm;
import com.mind.util.WebUtil;

public class JobFinancialAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(JobFinancialAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
				"Job Dashboard - Financial Tab",
				getDataSource(request, "ilexnewDB"))) {
			request.setAttribute("HidePage", "HidePage");
			// return( mapping.findForward( "UnAuthenticate" ) ); // - Check for
			// Pager Security.
		}
		/* Page Security=: End */

		String ticketId = JobDAO.getTicketId(request.getParameter("jobid"),
				getDataSource(request, "ilexnewDB"));
		if (ticketId != null && !ticketId.equals("")) {
			request.setAttribute("ticket_type", "existingTicket");
			request.setAttribute("ticket_id", ticketId);
			return (mapping.findForward("TicketDashboard"));
		}
		request.setAttribute(
				"tabId",
				(request.getParameter("tabId") == null ? ""
						+ JobDashboardTabType.FINANCIAL_TAB : request
						.getParameter("tabId")));
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - param tab id"
					+ request.getParameter("tabId"));
		}
		JobDashboardForm bean = (JobDashboardForm) form;
		JobDashboardBean dto = new JobDashboardBean();
		bean.setTabId("" + JobDashboardTabType.FINANCIAL_TAB);
		setGeneralFormInfo(bean, request);
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		setFinancialTabInfoInForm(dto, getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		String jobType = JobSetUpDao.getjobType(bean.getJobId(),
				getDataSource(request, "ilexnewDB"));
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - job type---- "
					+ jobType);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - JOB TYPE"
					+ jobType + " job id " + bean.getJobId());
		}
		if (jobType != null && jobType.equalsIgnoreCase("Default"))
			jobType = "Default";
		else if (jobType != null && jobType.equalsIgnoreCase("Addendum"))
			jobType = "Addendum";
		else
			jobType = "";
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - bean.getOwner()"
					+ bean.getOwner());
		}

		setStatusOfTabs(jobType, request, bean);
		Map<String, Object> map;

		// request.setAttribute("SAVE",(request.getParameter("save")==null?"":"Activity added successfully."));
		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		return mapping.findForward("success");
	}

	public void setStatusOfTabs(String jobType, HttpServletRequest request,
			JobDashboardForm bean) {
		String ticketType = null;
		if (logger.isDebugEnabled()) {
			logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobggggggggggggggggg------- "
					+ jobType);
		}
		// ticketType = request.getParameter("ticket_type");
		ticketType = bean.getTicketType();
		if (ticketType != null && !ticketType.trim().equalsIgnoreCase(""))
			request.setAttribute("fromStatus", "fromStatus");
		if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
			request.setAttribute("isTicket", "Ticket");
			request.setAttribute("tabStatus", "job");
		} else {
			if (jobType != null && !jobType.trim().equalsIgnoreCase("")
					&& jobType.trim().equalsIgnoreCase("Default"))
				request.setAttribute("tabStatus", "Default");
			if (jobType != null && !jobType.trim().equalsIgnoreCase("")
					&& jobType.trim().equalsIgnoreCase("Addendum"))
				request.setAttribute("tabStatus", "Addendum");
			else if (jobType != null && jobType.trim().equalsIgnoreCase("")) {
				if (logger.isDebugEnabled()) {
					logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobggggggggggggggggg");
				}
				request.setAttribute("tabStatus", "job");
			}
			request.setAttribute("isTicket", "job");
		}
		// request.setAttribute("tabStatus",jobType);
	}

	public void setFinancialTabInfoInForm(JobDashboardBean jobDashboardForm,
			DataSource ds) {
		FinancialTab financialTab = new FinancialTab();
		JobDashboard jobDashboard = new JobDashboard();
		JobDAO jobDao = new JobDAO();
		if (logger.isDebugEnabled()) {
			logger.debug("setFinancialTabInfoInForm(JobDashboardForm, DataSource) - appendix+++++++++"
					+ jobDashboardForm.getAppendixId());
		}
		financialTab.setAppendixId(jobDashboardForm.getAppendixId());
		financialTab.setJobId(jobDashboardForm.getJobId());

		GeneralJobInfo generalJobInfo = jobDao.getGeneralJobInfo(
				Long.parseLong(jobDashboardForm.getJobId()), ds);
		financialTab.setGeneralJobInfo(generalJobInfo);

		financialTab.setFinancialAnalysis(getFinancialAnalysis(
				jobDashboardForm.getJobId(), ds)); // - Set Financial Analysis
		/*
		 * ArrayList<PurchaseOrder> listOfPurchaseOrders=
		 * PurchaseOrderDAO.getPurchaseOrderList
		 * (jobDashboardForm.getJobId(),ds); PurchaseOrderList purchaseOrderList
		 * = new PurchaseOrderList();
		 * purchaseOrderList.setPurchaseOrders(listOfPurchaseOrders);
		 * financialTab.setPurchaseOrderList(purchaseOrderList);
		 */
		jobDashboard.setJobDashboardTab(financialTab);

		Convertor.convertToJobDashboardForm(jobDashboard, jobDashboardForm);
		if (logger.isDebugEnabled()) {
			logger.debug("setFinancialTabInfoInForm(JobDashboardForm, DataSource) - COUNTRY "
					+ jobDashboardForm.getSite_country()
					+ " ,"
					+ jobDashboardForm.isUs());
		}

	}

	/**
	 * Discription: Get Fanancila Tab Data.
	 * 
	 * @param jobId
	 * @param ds
	 * @return
	 */
	private FinancialAnalysis getFinancialAnalysis(String jobId, DataSource ds) {
		FinancialAnalysis financialAnalysis = new FinancialAnalysis();
		FinancialAnalysisDAO.setCost(financialAnalysis, jobId, ds);
		FinancialAnalysisDAO.setLaborAnalysis(financialAnalysis, jobId, ds);
		FinancialAnalysisDAO.setRevenueVGPM(financialAnalysis, jobId, ds);
		FinancialAnalysisDAO.setUsagesAnalysis(financialAnalysis, jobId, ds);
		financialAnalysis.setContractCost(FinancialAnalysisDAO
				.getContractCost(financialAnalysis));
		financialAnalysis.setCorporateCost(FinancialAnalysisDAO
				.getCorporateCost(financialAnalysis));
		financialAnalysis
				.setVgp(FinancialAnalysisDAO.getVGP(financialAnalysis));
		financialAnalysis.setDiscountCost(FinancialAnalysisDAO
				.getDiscount(financialAnalysis));
		return financialAnalysis;
	}

	public void setGeneralFormInfo(JobDashboardForm bean,
			HttpServletRequest request) {
		bean.setTabId((request.getParameter("tabId") == null ? ""
				+ JobDashboardTabType.FINANCIAL_TAB : request
				.getParameter("tabId")));
		String loginUserName = (String) request.getSession().getAttribute(
				"username");
		bean.setCreatedBy(loginUserName);
		bean.setJobId((String) (request.getParameter("Job_Id") == null ? request
				.getAttribute("jobid") : request.getParameter("Job_Id")));
		// To open general job Dashboard.
		if (request.getParameter("jobid") != null) {
			bean.setJobId(request.getParameter("jobid"));
		}
		if (request.getAttribute("jobid") != null) {
			bean.setJobId((String) request.getAttribute("jobid"));
		}
		JobDAO jobDAO = new JobDAO();
		TeamInfo teamInfo = jobDAO.getTeamInfo(new Long(bean.getJobId()),
				getDataSource(request, "ilexnewDB"));

		bean.setOwner(teamInfo.getOwner());

		bean.setAppendixId((String) (request.getParameter("appendix_Id") == null ? bean
				.getAppendixId() : request.getParameter("appendix_Id")));
		bean.setTicket_id(request.getParameter("ticket_id") == null ? bean
				.getTicket_id() : request.getParameter("ticket_id"));
		if (request.getParameter("appendix_Id") == null
				&& request.getParameter("appendixid") != null)
			bean.setAppendixId(request.getParameter("appendixid"));

		if (request.getAttribute("tabId") != null) {
			bean.setTabId((String) request.getAttribute("tabId"));
		}
	}

	public ActionForward POaction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		return mapping.findForward("success");
	}
}
