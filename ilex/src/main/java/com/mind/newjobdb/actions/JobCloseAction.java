package com.mind.newjobdb.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.JobDashboard;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.TeamInfo;
import com.mind.common.dao.Authenticationdao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.CloseTab;
import com.mind.newjobdb.business.Convertor;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.dao.JobLevelWorkflowDAO;
import com.mind.newjobdb.dao.JobWorkflowChecklist;
import com.mind.newjobdb.dao.PurchaseOrder;
import com.mind.newjobdb.dao.PurchaseOrderDAO;
import com.mind.newjobdb.dao.PurchaseOrderList;
import com.mind.newjobdb.formbean.JobDashboardForm;
import com.mind.util.WebUtil;

public class JobCloseAction extends com.mind.common.IlexDispatchAction {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JobCloseAction.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		/* Page Security: Start */
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // - Check for
															// session expired.
		}
		if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
				"Job Dashboard - Close Tab",
				getDataSource(request, "ilexnewDB"))) {
			request.setAttribute("HidePage", "HidePage");
			// return( mapping.findForward( "UnAuthenticate" ) ); // - Check for
			// Pager Security.
		}
		/* Page Security=: End */

		request.setAttribute(
				"tabId",
				(request.getParameter("tabId") == null ? ""
						+ JobDashboardTabType.CLOSE_TAB : request
						.getParameter("tabId")));
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - param tab id"
					+ request.getParameter("tabId"));
		}
		JobDashboardForm bean = (JobDashboardForm) form;
		JobDashboardBean dto = new JobDashboardBean();
		Map<String, Object> map = new HashMap<String, Object>();

		bean.setTabId("" + JobDashboardTabType.CLOSE_TAB);
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		JobDAO.setLatestInstallNotes(dto, getDataSource(request, "ilexnewDB"));

		JobDAO.setLatestComments(map, dto, getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		WebUtil.copyMapToRequest(request, map);
		setGeneralFormInfo(bean, request);
		setCloseTabInfoInForm(dto, getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		String jobType = JobSetUpDao.getjobType(bean.getJobId(),
				getDataSource(request, "ilexnewDB"));
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - job type---- "
					+ jobType);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - JOB TYPE"
					+ jobType + " job id " + bean.getJobId());
		}
		request.setAttribute(
				"installNotesList",
				JobDAO.getInstallNotes(bean.getJobId(),
						getDataSource(request, "ilexnewDB")));
		if (jobType != null && jobType.trim().equalsIgnoreCase("Default"))
			jobType = "Default";
		else if (jobType != null && jobType.trim().equalsIgnoreCase("Addendum"))
			jobType = "Addendum";
		else
			jobType = "";
		if (logger.isDebugEnabled()) {
			logger.debug("unspecified(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - bean.getOwner()"
					+ bean.getOwner());
		}

		setStatusOfTabs(jobType, request, bean);
		// request.setAttribute("SAVE",(request.getParameter("save")==null?"":"Activity added successfully."));

		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		return mapping.findForward("success");
	}

	public void setStatusOfTabs(String jobType, HttpServletRequest request,
			JobDashboardForm bean) {
		String ticketType = null;
		if (logger.isDebugEnabled()) {
			logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobggggggggggggggggg------- "
					+ jobType);
		}
		// ticketType = request.getParameter("ticket_type");
		ticketType = bean.getTicketType();
		if (ticketType != null && !ticketType.trim().equalsIgnoreCase(""))
			request.setAttribute("fromStatus", "fromStatus");
		if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
			request.setAttribute("isTicket", "Ticket");
			request.setAttribute("tabStatus", "job");
		} else {
			if (jobType != null && !jobType.trim().equalsIgnoreCase("")
					&& jobType.trim().equalsIgnoreCase("Default"))
				request.setAttribute("tabStatus", "Default");
			if (jobType != null && !jobType.trim().equalsIgnoreCase("")
					&& jobType.trim().equalsIgnoreCase("Addendum"))
				request.setAttribute("tabStatus", "Addendum");
			else if (jobType != null && jobType.trim().equalsIgnoreCase("")) {
				if (logger.isDebugEnabled()) {
					logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobggggggggggggggggg");
				}
				request.setAttribute("tabStatus", "job");
			}
			request.setAttribute("isTicket", "job");
		}
		// request.setAttribute("tabStatus",jobType);
	}

	public void setCloseTabInfoInForm(JobDashboardBean jobDashboardForm,
			DataSource ds) {
		CloseTab closeTab = new CloseTab();
		JobDashboard jobDashboard = new JobDashboard();
		JobDAO jobDao = new JobDAO();
		if (logger.isDebugEnabled()) {
			logger.debug("setCloseTabInfoInForm(JobDashboardForm, DataSource) - appendix+++++++++"
					+ jobDashboardForm.getAppendixId());
		}
		closeTab.setAppendixId(jobDashboardForm.getAppendixId());
		closeTab.setJobId(jobDashboardForm.getJobId());
		GeneralJobInfo generalJobInfo = jobDao.getGeneralJobInfo(
				Long.parseLong(jobDashboardForm.getJobId()), ds);
		closeTab.setGeneralJobInfo(generalJobInfo);

		// JobPlanningNote jobPlanningNote =
		// (JobPlanningNote)jobDao.getJobPlanningNotes(Long.parseLong(jobDashboardForm.getJobId()),ds);
		// CloseTab.setJobPlanningNote(jobPlanningNote);

		// boolean showEvenIfNoPending=true;
		// JobWorkflowChecklist topPendingCheckListItems =
		// ((JobWorkflowChecklist) JobLevelWorkflowDAO.getJobWorkflowChecklist
		// (Long.parseLong(jobDashboardForm.getJobId()),ds)).getTopPendingItems(2,
		// showEvenIfNoPending);

		// CloseTab.setTopPendingCheckListItems(topPendingCheckListItems);

		// ArrayList<PurchaseOrder> listOfPurchaseOrders=
		// PurchaseOrderDAO.getPurchaseOrderList(jobDashboardForm.getJobId(),ds);
		// PurchaseOrderList purchaseOrderList = new PurchaseOrderList();
		// purchaseOrderList.setPurchaseOrders(listOfPurchaseOrders);
		// CloseTab.setPurchaseOrderList(purchaseOrderList);
		// CostVariations totalCostVariations =
		// PurchaseOrderList.getTotalCostVariations(purchaseOrderList);
		// CloseTab.setCostVariations(totalCostVariations);
		boolean showEvenIfNoPending = true;
		JobWorkflowChecklist topPendingCheckListItems = ((JobWorkflowChecklist) JobLevelWorkflowDAO
				.getJobWorkflowChecklist(
						Long.parseLong(jobDashboardForm.getJobId()), ds))
				.getTopPendingItems(2, showEvenIfNoPending);

		closeTab.setTopPendingCheckListItems(topPendingCheckListItems);

		ArrayList<PurchaseOrder> listOfPurchaseOrders = PurchaseOrderDAO
				.getPurchaseOrderList(jobDashboardForm.getJobId(), ds);
		PurchaseOrderList purchaseOrderList = new PurchaseOrderList();
		purchaseOrderList.setPurchaseOrders(listOfPurchaseOrders);
		closeTab.setPurchaseOrderList(purchaseOrderList);
		closeTab.setJobInvoiceSummary(jobDao.getInvoiceSummary(
				jobDashboardForm.getJobId(), ds));

		jobDashboard.setJobDashboardTab(closeTab);
		Convertor.convertToJobDashboardForm(jobDashboard, jobDashboardForm);
		if (logger.isDebugEnabled()) {
			logger.debug("setCloseTabInfoInForm(JobDashboardForm, DataSource) - COUNTRY "
					+ jobDashboardForm.getSite_country()
					+ " ,"
					+ jobDashboardForm.isUs());
		}
	}

	// public ActionForward addComments (ActionMapping mapping , ActionForm form
	// , HttpServletRequest request , HttpServletResponse response)
	// {
	// response.setHeader("Pragma","No-Cache");
	// response.setHeader("Cache-Control","no-cache");
	// response.setHeader("Cache-Control","no-store");
	// response.setDateHeader("Expires",0);
	// System.out.println(request.getParameter("jobId"));
	// String jobId = request.getParameter("jobId");
	// String comments = request.getParameter("comments");
	// HttpSession session = request.getSession( true );
	// String loginuserid = ( String ) session.getAttribute( "userid" );
	// try{
	//
	// JobDAO.addJobComments(comments, jobId, loginuserid, getDataSource(
	// request , "ilexnewDB" ));
	//
	// }catch(Exception e){
	// e.printStackTrace();
	// }
	// return null;
	// }

	public void setGeneralFormInfo(JobDashboardForm bean,
			HttpServletRequest request) {
		bean.setTabId((request.getParameter("tabId") == null ? ""
				+ JobDashboardTabType.CLOSE_TAB : request.getParameter("tabId")));
		String loginUserName = (String) request.getSession().getAttribute(
				"username");
		bean.setCreatedBy(loginUserName);
		bean.setJobId((String) (request.getParameter("Job_Id") == null ? request
				.getAttribute("jobid") : request.getParameter("Job_Id")));
		// To open general job Dashboard.
		if (request.getParameter("jobid") != null) {
			bean.setJobId(request.getParameter("jobid"));
		}
		if (request.getAttribute("jobid") != null) {
			bean.setJobId((String) request.getAttribute("jobid"));
		}
		JobDAO jobDAO = new JobDAO();
		TeamInfo teamInfo = jobDAO.getTeamInfo(new Long(bean.getJobId()),
				getDataSource(request, "ilexnewDB"));

		bean.setOwner(teamInfo.getOwner());

		bean.setAppendixId((String) (request.getParameter("appendix_Id") == null ? bean
				.getAppendixId() : request.getParameter("appendix_Id")));
		bean.setTicket_id(request.getParameter("ticket_id") == null ? bean
				.getTicket_id() : request.getParameter("ticket_id"));
		if (request.getParameter("appendix_Id") == null
				&& request.getParameter("appendixid") != null)
			bean.setAppendixId(request.getParameter("appendixid"));

		if (request.getAttribute("tabId") != null) {
			bean.setTabId((String) request.getAttribute("tabId"));
		}
	}

	public ActionForward viewInstallNotes(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		request.setAttribute(
				"tabId",
				(request.getParameter("tabId") == null ? ""
						+ JobDashboardTabType.CLOSE_TAB : request
						.getParameter("tabId")));
		if (logger.isDebugEnabled()) {
			logger.debug("viewInstallNotes(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - param tab id"
					+ request.getParameter("tabId"));
		}
		JobDashboardForm bean = (JobDashboardForm) form;
		bean.setTabId("" + JobDashboardTabType.CLOSE_TAB);
		JobDashboardBean dto = new JobDashboardBean();
		try {
			BeanUtils.copyProperties(dto, bean);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		JobDAO.setLatestInstallNotes(dto, getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");
		setGeneralFormInfo(bean, request);
		setCloseTabInfoInForm(dto, getDataSource(request, "ilexnewDB"));
		try {
			BeanUtils.copyProperties(bean, dto);
		} catch (IllegalAccessException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e);
			e.printStackTrace();
		}
		String jobType = JobSetUpDao.getjobType(bean.getJobId(),
				getDataSource(request, "ilexnewDB"));
		if (logger.isDebugEnabled()) {
			logger.debug("viewInstallNotes(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - job type---- "
					+ jobType);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("viewInstallNotes(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - JOB TYPE"
					+ jobType + " job id " + bean.getJobId());
		}
		if (jobType.equalsIgnoreCase("Default")
				|| jobType.equalsIgnoreCase("Addendum"))
			jobType = "Default";
		else
			jobType = "";
		if (logger.isDebugEnabled()) {
			logger.debug("viewInstallNotes(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse) - bean.getOwner()"
					+ bean.getOwner());
		}

		setStatusOfTabs(jobType, request, bean);
		// request.setAttribute("SAVE",(request.getParameter("save")==null?"":"Activity added successfully."));
		Map<String, Object> map;

		if (bean.getJobId() != null
				&& !bean.getJobId().trim().equalsIgnoreCase("")) {
			map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
					getDataSource(request, "ilexnewDB"));
			WebUtil.copyMapToRequest(request, map);
		}
		request.setAttribute(
				"installNotesList",
				JobDAO.getInstallNotes(bean.getJobId(),
						getDataSource(request, "ilexnewDB")));
		return mapping.findForward("viewinstallnotes");
	}

	public ActionForward POaction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		return mapping.findForward("success");
	}
}
