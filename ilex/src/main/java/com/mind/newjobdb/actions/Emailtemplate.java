package com.mind.newjobdb.actions;

import com.mind.bean.msp.SiteInfo;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.objects.ScheduleElement;
import com.mind.formbean.PM.JobEditForm;
import com.mind.newjobdb.actions.Emailtemplate.EmailType;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.dao.TicketRequestInfoDAO;
import com.mind.newjobdb.formbean.JobDashboardForm;
import java.text.CharacterIterator;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.Calendar;
import java.util.List;
import javax.sql.DataSource;
import org.apache.struts.action.ActionForm;

public class Emailtemplate {

    public enum JobType {

        NETMEDX("NetMedX"),
        PROJECTS("Projects");

        private final String name;
        private Class<? extends ActionForm> formClass;

        private JobType(final String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public enum EmailType {

        DISPATCH("Ticket Dispatch"),
        DISPATCH_SCHEDULED("Dispatch OnSite / OffSite"),
        CREATION("Job Creation"),
        WORK_NOTE("Work Note");

        private final String name;

        private EmailType(final String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static final String P = "Project";
    public static final String N = "NetMedX";
    public static final String SENDER_EMAIL = "cma@contingent.com";

    /*
     * Description: Builds the email body content for a job email based on the parameters given
     *
     * Version History:
     * 01/15/15 - Initial version
     *
     * Parameters:
     * @param (DataSource) ds - The database data source object.
     * @param (String) userId - The current user's user Id.
     * @param (String) userName - The current user's name.
     * @param (String) companyName - The name of the company the email is for. This will be displayed at the top of the email.
     * @param (String) jobName - The name of the job.
     * @param (String) jobId - The ID of the job.
     * @param (JobType) jobType - The type of job the email is to be generated for.
     * @param (EmailType) emailType - The type of email to be generated.
     * @param (List<String[]>) escalationContacts - Escalation contacts for the appendix of the job.
     * @param (ActionForm) formData - The job data for generating the email.
     *
     * Returns:
     * @return (String) The html email body content.
     */
    public static String buildJobEmailBody(DataSource ds, String userId, String userName, String companyName, String jobName, String jobId, JobType jobType, EmailType emailType, List<String[]> escalationContacts, ActionForm formData) {
        String emailBody = "";

        switch (emailType) {
            case CREATION:
                emailBody = buildJobEmailBody_Creation(ds, userId, userName, jobName, jobType, formData);
                break;
            case DISPATCH:
                emailBody = buildJobEmailBody_Dispatch(ds, userId, userName, jobName, jobType, formData);
                break;
            // Call directly due to use of new ScheduleElement
            /*case DISPATCH_SCHEDULED:
             emailBody = buildJobEmailBody_DispatchScheduled(ds, userId, userName, jobName, jobType, formData);
             break;*/
            case WORK_NOTE:
                emailBody = buildJobEmailBody_WorkNote(ds, userId, userName, jobName, jobType, formData);
                break;
        }

        return getGeneralHeader(companyName, jobType) + emailBody + buildSiteDetailsSection(jobId, ds, emailType) + getGeneralFooter(escalationContacts);
    }

    /*
     * Description: Builds the email body content for a job created email based on the parameters given
     *
     * Version History:
     * 01/15/15 - Initial version
     *
     * Parameters:
     * @param (DataSource) ds - The database data source object.
     * @param (String) userId - The current user's user Id.
     * @param (String) userName - The current user's name.
     * @param (String) jobName - The name of the job.
     * @param (JobType) jobType - The type of job the email is to be generated for.
     * @param (EmailType) emailType - The type of email to be generated.
     * @param (ActionForm) formData - The job data for generating the email.
     *
     * Returns:
     * @return (String) The html email body content.
     */
    public static String buildJobEmailBody_Creation(DataSource ds, String userId, String userName, String jobName, JobType jobType, ActionForm formData) {
        String emailBody = "";

        if (jobType == JobType.NETMEDX) {
            JobDashboardForm jobData = (JobDashboardForm) formData;

            String requestor = jobData.getRequestor();
            requestor = requestor.split(",")[0];

            String requestorEmail = jobData.getRequestorEmail();

            String requestType = TicketRequestInfoDAO.getRequestTypesWithName(ds, jobData.getRequestType());
            String criticality = TicketRequestInfoDAO.getCriticalitiesname(jobData.getCriticality(), ds);
            String problemCategory = TicketRequestInfoDAO.getProblemCategoriesname(ds, jobData.getProblemCategory());

            String to = "";
            String emaillist[] = null;// Mail id you want to send;
            if (requestorEmail.contains(",")) {
                emaillist = requestorEmail.split(",");
                for (int i = 0; i < emaillist.length; i++) {
                    to = to + "<a href=\"mailto:" + emaillist[i] + "\">" + emaillist[i] + "</a> ";
                }
            } else {
                to = to + "<a href=\"mailto:" + requestorEmail + "\">" + requestorEmail + "</a> ";
            }

            String problemDescription = forHTML(jobData.getProblemDescription());
            String specialInstructions = forHTML(jobData.getSpecialInstructions());
            String ticketRules = forHTML(jobData.getTicketRules());

            String preferredTime = jobData.getPreferredDate() + " " + jobData.getPreferredHour() + ":" + jobData.getPreferredMinute() + " " + jobData.getPreferredIsAm();

            emailBody = "<table width=990 cellspacing=0 cellpadding=0 border=0>"
                    + "<tbody>"
                    + "<tr><td style=\"font-size:11pt; font-family: Calibri, \'Arial Narrow\';word-break: keep-all; \">"
                    + "<span><b>Requestor: " + "</b>" + requestor + " " + to + "</span><br>"
                    + "<span><b>Ticket #: </b>" + jobName + "</span><br>"
                    + "<span><b>Request Type: </b>" + requestType + "</span><br>"
                    + "<span><b>Criticality: </b>" + criticality + "</span><br>"
                    + "<span><b>Customer Reference: </b>" + jobData.getCustomerReference() + "</span><br>"
                    + "<span><b>Site Number: </b>" + jobData.getSite() + "</span><br>"
                    + "<span><b>Problem Category: </b>" + problemCategory + "</span><br>"
                    + "<span><b>Submitted By: </b>" + userName + "</span><br>"
                    + "<span><b>Preferred Time: </b>" + preferredTime + "</span><br>"
                    + "</td></tr>"
                    + "<tr>&nbsp;</tr>"
                    + "<tr valign=top><td bgcolor=#f6f6f6 style=\"font-size:11pt; font-family: Calibri, \'Arial Narrow\'; padding: 3px;font-weight: bold;width:990px;\" >"
                    + "Problem Description"
                    + "</td></tr>"
                    + "<tr><td style=\"font-size:11pt;font-family: Calibri, \'Arial Narrow\';padding: 3px 5px 10px;word-break: keep-all;width:990px;word-break: break-word;\">"
                    + problemDescription.replace("\n", "<br />")
                    + "</td></tr>"
                    + "<tr valign=top><td bgcolor=#f6f6f6 style=\"font-size:11pt; font-family: Calibri, \'Arial Narrow\'; 'Arial Narrow'; padding: 3px;width:990px;font-weight: bold;\">"
                    + "<b>Special Instructions</b>"
                    + "</td></tr>"
                    + "<tr><td style=\"font-size:11pt;font-family: Calibri, \'Arial Narrow\'; padding: 3px 5px 10px;word-break: keep-all;width:990px;word-break: break-word;\" >"
                    + specialInstructions.replace("\n", "<br />")
                    + "</td></tr>"
                    + "<tr valign=top><td bgcolor=#f6f6f6 style=\"font-size:11pt; font-family:Calibri, \'Arial Narrow\'; padding: 3px;font-weight: bold;\">"
                    + "Ticket Rules"
                    + "</td></tr>"
                    + "<tr><td style=\"font-size:11pt;font-family:Calibri, \'Arial Narrow\'; padding: 3px 5px 10px;word-break: keep-all;width:990px;word-break: break-word;\">"
                    + ticketRules.replace("\n", "<br />")
                    + "</td></tr>"
                    + "</tbody>"
                    + "</table><br><br>";
        } else if (jobType == JobType.PROJECTS) {
            JobEditForm jobData = (JobEditForm) formData;
            String estimatedStartSchedule = validateDate(jobData.getEstimatedStartSchedule());
            String estimatedCompleteSchedule = validateDate(jobData.getEstimatedCompleteSchedule());

            emailBody = "<table width=990 cellspacing=0 cellpadding=0 border=0>"
                    + "<tbody>"
                    + "<tr>"
                    + "<td style=\"font-size:11pt; font-family: Calibri, \'Arial Narrow\';word-break: keep-all;width:990px;word-break: break-word; \">"
                    + "<span><b>Your job request has been submitted with the following details:</b></span><br>"
                    + "<br>"
                    + "<span><b>Job Name: </b>" + jobName + "</span><br>"
                    + "<span><b>Estimated Start Date: </b>" + estimatedStartSchedule + "</span><br>"
                    + "<span><b>Expected Complete Date: </b>" + estimatedCompleteSchedule + "</span><br>"
                    + "</tr>"
                    + "</td>"
                    + "<tr>"
                    + "&nbsp;</tr>"
                    + "</tbody></table>";
        }

        return emailBody;
    }

    /*
     * Description: Builds the email body content for a job dispatch email based on the parameters given
     *
     * Version History:
     * 01/15/15 - Initial version
     *
     * Parameters:
     * @param (DataSource) ds - The database data source object.
     * @param (String) userId - The current user's user Id.
     * @param (String) userName - The current user's name.
     * @param (String) jobName - The name of the job.
     * @param (JobType) jobType - The type of job the email is to be generated for.
     * @param (EmailType) emailType - The type of email to be generated.
     * @param (ActionForm) formData - The job data for generating the email.
     *
     * Returns:
     * @return (String) The html email body content.
     */
    public static String buildJobEmailBody_Dispatch(DataSource ds, String userId, String userName, String jobName, JobType jobType, ActionForm formData) {
        String emailBody = "";

        JobDashboardForm jobData = (JobDashboardForm) formData;
        String onSiteDate = validateDate(jobData.getOnsiteDate() + " " + jobData.getOnsiteHour() + ":" + jobData.getOnsiteMinute() + " " + jobData.getOnsiteIsAm());
        String offSiteDate = validateDate(jobData.getOffsiteDate() + " " + jobData.getOffsiteHour() + ":" + jobData.getOffsiteMinute() + " " + jobData.getOffsiteIsAm());

        String type = "Job";
        String labelName = "Job Name: ";
        if (jobType == JobType.NETMEDX) {
            type = "NetMedX Ticket";
            labelName = "Ticket #: ";
        } else if (jobType == JobType.PROJECTS) {
        }

        emailBody = "<table width=990 cellspacing=0 cellpadding=0 border=0>"
                + "<tbody>"
                + "<tr>"
                + "<td style=\"font-size:11pt; font-family: Calibri, \'Arial Narrow\'; \" nowrap>"
                + "<span><b>Your " + type + " has been updated with the following details:"
                + "</b></span><br>"
                + "<br>"
                + "<span><b>" + labelName + "</b>" + jobName + "</span>"
                + "<span><br><b>OnSiteDate: </b>" + onSiteDate + "</span><br>"
                + "<span><b>Off Site Date: </b>" + offSiteDate + "</span><br>"
                + "</tr>"
                + "<tr>"
                + "&nbsp;</tr>"
                + "</tbody></table>";

        return emailBody;
    }

    /*
     * Description: Builds the email body content for a job dispatch scheduled email based on the parameters given
     *
     * Version History:
     * 01/15/15 - Initial version
     * 01/16/15 - Updated to be directly callable with new Schedule Element object.
     *
     * Parameters:
     * @param (DataSource) ds - The database data source object.
     * @param (String) userId - The current user's user Id.
     * @param (String) userName - The current user's name.
     * @param (String) companyName - The name of the company the email is for. This will be displayed at the top of the email.
     * @param (String) jobName - The name of the job.
     * @param (String) jobId - The ID of the job.
     * @param (JobType) jobType - The type of job the email is to be generated for.
     * @param (EmailType) emailType - The type of email to be generated.
     * @param (List<String[]>) escalationContacts - Escalation contacts for the appendix of the job.
     * @param (ScheduleElement) se - A schedule element object holding the needed data.
     *
     * Returns:
     * @return (String) The html email body content.
     */
    public static String buildJobEmailBody_DispatchScheduled(DataSource ds, String userId, String userName, String companyName, String jobName, String jobId, JobType jobType, List<String[]> escalationContacts, ScheduleElement se) {
        String emailBody = "";

        if (jobType == JobType.NETMEDX) {
            int dispatchNumber = se.getDispatchId();
            String actualStart = validateDate(se.getActualStart());
            String actualEnd = validateDate(se.getActualEnd());
            String plannedStart = validateDate(se.getPlannedStart());

            emailBody = "<table width=990 cellspacing=0 cellpadding=0 border=0>"
                    + "<tbody>"
                    + "<tr>"
                    + "<td style=\"font-size:11pt; font-family: Calibri, \'Arial Narrow\'; \" nowrap>"
                    + "<span><b>Your ticket has been updated with the following details:</b></span><br>"
                    + "<span><b>Dispatch: </b>" + dispatchNumber + "</span><br>"
                    + "<br>"
                    + "<span><b>Ticket #: </b>" + jobName + "</span><br>"
                    + "<span><b>Scheduled Arrival: </b>" + plannedStart + "</span><br>"
                    + "<span><b>Actual Arrival: </b>" + actualStart + "</span><br>"
                    + "<span><b>Departure: </b>" + actualEnd + "</span><br>"
                    + "</tr>"
                    + "<tr>&nbsp;</tr>"
                    + "</tbody>"
                    + "</table>";
        } else if (jobType == JobType.PROJECTS) {
            int dispatchNumber = se.getDispatchId();
            String actualStart = validateDate(se.getActualStart());
            String actualEnd = validateDate(se.getActualEnd());
            String plannedStart = validateDate(se.getPlannedStart());
            String plannedEnd = validateDate(se.getPlannedEnd());

            emailBody = "<table width=990 cellspacing=0 cellpadding=0 border=0>"
                    + "<tbody>"
                    + "<tr>"
                    + "<td style=\"font-size:11pt; font-family: Calibri, \'Arial Narrow\'; \" nowrap>"
                    + "<span><b>Your job has been updated with the following details:"
                    + "</b></span><br>"
                    + "<span><b>Dispatch: </b>" + dispatchNumber + "</span><br>"
                    + "<br>"
                    + "<span><b>Job Name: </b>" + jobName + "</span><br>"
                    + "<span><b>Schedule Start: </b>" + plannedStart + "</span><br>"
                    + "<span><b>Schedule End: </b>" + plannedEnd + "</span><br>"
                    + "<span><b>Actual Start: </b>" + actualStart + "</span><br>"
                    + "<span><b>Actual End: </b>" + actualEnd + "</span>"
                    + "</tr>"
                    + "<tr>&nbsp;</tr>"
                    + "</tbody>"
                    + "</table>";
        }

        return getGeneralHeader(companyName, jobType) + emailBody + buildSiteDetailsSection(jobId, ds, EmailType.DISPATCH_SCHEDULED) + getGeneralFooter(escalationContacts);
    }

    /*
     * Description: Builds the email body content for a job work notes email based on the parameters given
     *
     * Version History:
     * 01/15/15 - Initial version
     *
     * Parameters:
     * @param (DataSource) ds - The database data source object.
     * @param (String) userId - The current user's user Id.
     * @param (String) userName - The current user's name.
     * @param (String) jobName - The name of the job.
     * @param (JobType) jobType - The type of job the email is to be generated for.
     * @param (ActionForm) formData - The job data for generating the email.
     *
     * Returns:
     * @return (String) The html email body content.
     */
    public static String buildJobEmailBody_WorkNote(DataSource ds, String userId, String userName, String jobName, JobType jobType, ActionForm formData) {
        String emailBody = "";

        String type = "Job";
        String labelName = "Job Name: ";

        if (jobType == JobType.NETMEDX) {
            type = "ticket";
            labelName = "Ticket #: ";
        }

        JobDashboardForm jobData = (JobDashboardForm) formData;
        String workNote = Jobdao.getLatestWorkNote(jobData.getJobId(), ds);

        emailBody = "<table width=990 cellspacing=0 cellpadding=0 border=0>"
                + "<tbody>"
                + "<tr>"
                + "<td style=\"font-size:11pt; font-family: Calibri, \'Arial Narrow\';word-break: keep-all;width:990px;word-break: break-word; \">"
                + "<span><b>Your " + type + " has been updated with the following details:</b>"
                + "</span><br>"
                + "<br>"
                + "<span><b>" + labelName + "</b>" + jobName + "</span><br>"
                + "<span><b>Work Note: </b>" + workNote.replace("\n", "<br>") + "</span><br>"
                + "</td>"
                + "</tr>"
                + "<tr>&nbsp;</tr>"
                + "</tbody>"
                + "</table>";

        return emailBody;
    }

    /*
     * Description: Builds the header for an email.
     *
     * Version History:0
     * 01/15/15 - Initial documentation
     *
     * Parameters:
     * @param (String) companyName - The name of the company the email is for. This will be displayed at the top of the email.
     * @param (JobType) jobType - The type of job the email is to be generated for.
     *
     * Returns:
     * @return (String) The html email header content.
     */
    private static String getGeneralHeader(String companyName, JobType jobType) {
        DateFormat df = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
        Calendar calobj = Calendar.getInstance();

        String header = "<span style=\"display:none; max-height: 0px; font-size: 0px; overflow: hidden; mso-hide: all\">Email12345</span><div style=\"background-color:#ffffff; font-size:11pt; font-family:Calibri, \'Arial Narrow\',sans-serif; color: #000000; word-break: break-all; word-wrap: break-word;width:990px;\">"
                + "<table width=990 cellspacing=0 cellpadding=0 border=0>"
                + "<tbody>"
                + "<tr valign=\"top\" style=\"font-size:11pt; font-family: Calibri, \'Arial Narrow\', Arial; height: 21px; padding-left: 10px; width:990px;background-color: #222e78; color: #f6f6f6; vertical-align:middle; \">"
                + "<td width=50% style=\"padding-left: 10px;\">";

        if (jobType == JobType.NETMEDX) {
            header += "NetMedX Ticket Update";
        } else {
            header += "Job Update";
        }

        header += "</td>"
                + "<td align=right style=\"padding-right: 10px;font-size:11pt; font-family: Calibri, \'Arial Narrow\'; \">"
                + df.format(calobj.getTime())
                + "</td>"
                + "</tr>"
                // Start of New Header

                + "<tr width=990>"
                // comment here
                + "<!--[if gte mso 9]>"
                + "<v:image xmlns:v=\"urn:schemas-microsoft-com:vml\" id=\"theImage\" "
                + "style=\"behavior: url(#default#VML); display:inline-block; position:absolute;background-color: #CCCCFF; height:45px; width:990px; top:0; left:0; border:0; z-index:1;\" "
                + "src=\"cid:image1\">"
                + "<p  visible style=\"font-size: 18pt; font-family:Tahoma, Geneva, sans-serif; position:fixed; padding-left: 10px;top:0; left:0; border:0; vertical-align:middle; width:900px;\">"
                + companyName
                + "</p>"
                + " <![endif]--> "
                + "<!--[if gte mso 9]>"
                + "</v:image>"
                + "<![endif]-->"
                + "</tr>"
                + "</tbody>"
                + "</table>"
                + "<!--[if !mso]>"
                + "<!-->"
                + "<div style=\"background-image: url(http://contingent.com/images/ilex_ntmdx_header_v2.png); height:40px;background-repeat: no-repeat;font-size: 18pt; font-family:Tahoma, Geneva, sans-serif;padding-left:10px;padding-top:5px;background-color:#CCCCFF; \">"
                + companyName
                + "</div>"
                + "<!--<![endif]-->"
                + "<br/><!--[if gte mso 9]><br/>"
                + "<br/>"
                + "<br/><![endif]-->";

        return header;

    }

    /*
     * Description: Builds the header for an email.
     *
     * Version History:
     * 01/15/15 - Initial documentation
     *
     * Parameters:
     * @param (List<String[]>) escalationContacts - Escalation contacts for the appendix of the job.
     *
     * Returns:
     * @return (String) The html email footer content.
     */
    private static String getGeneralFooter(List<String[]> escalationContacts) {
        String footer = "";
        String email = "";
        if (escalationContacts.size() > 0) {
            footer += "<table width=990 cellspacing=0 cellpadding=5 bordercolor=#CCCCCC border=1 style=\"border-collapse:collapse;\">"
                    + "<tbody>"
                    + "<tr>"
                    + "<td width=990  bgcolor=#000000 style=\"color: #FFFFFF; font-size:11pt; font-family:Calibri, \'Arial Narrow\', Arial; padding: 3px;\">"
                    + "Escalation Contacts" + "</td>" + "</tr>";

            for (int i = 0; i < escalationContacts.size(); i++) {
                String[] contacts = escalationContacts.get(i);
                footer += "<tr >"
                        + "<td style=\"font-size:11pt; font-family:Calibri, \'Arial Narrow\'; padding-left: 3px;padding-right: 3px;width:990; \"nowrap>"
                        + contacts[0] + contacts[3] + contacts[1]
                        + "<a href=mailto:" + contacts[2] + "\">"
                        + contacts[2] + "</a>" + "</td>" + "</tr>";

                email += contacts[2] + ", ";
            }
            footer += "</tbody>" + "</table>";
        }

        footer += "<table width=990   height=35  cellspacing=\"0\" cellpadding=0 border=0>"
                + "<tbody>"
                + "<tr>"
                + "<table width=990 cellspacing=0 cellpadding=0 border=0>"
                + "<tbody>"
                + "<tr valign=top style=\"font-size:11pt; font-family: Calibri, \'Arial Narrow\', Arial; height: 21px; padding-left: width:990px;  vertical-align:middle; \">"
                + "<td align=right>"
                + "<span  style=\"margin-right:5px;\">"
                + "<a href=\"mailto:mstuhlreyer@contingent.com;jstine@contingent.com;bsupinger@contingent.com?subject=Not Satisfied? Please Tell Us Why!\">"
                + "Not Satisfied"
                + "</a>"
                + "|"
                + "</span>"
                + "<span  style=\"margin-right:5px;\">"
                + "<a href=\"http://contingent.com/index.php?cmp=have_us_call_you\">"
                + "Have Sales Call Me"
                + "</a>"
                + "|"
                + "</span>"
                + "<span  style=\"margin-right:5px;\">"
                + "<a href=\"http://contingent.com//dispatch\">"
                + "Request a Dispatch"
                + "</a>"
                + "|"
                + "</span>"
                + "<span  style=\"margin-right:5px;\">"
                + "<a href=\"mailto:helpdesk@contingent.com?subject=EMERGENCY ESCALATION&amp;Body=Note: This issue will be escalated to the helpdesk and all associated Operations "
                + "team members associated with your account. You will receive a response from someone on the Operations team within 30 minutes.";

        if (!email.equals("")) {
            footer = footer + "&amp;cc=" + email;
        }

        footer = footer + "\">" + "Escalate" + "</span>"
                + "</td>"
                + "</tr>"
                + "</tr>" + "</tbody>" + "</table>" + "</div>";

        return footer;
    }

    /*
     * Description: Changes special characters in a string to their HTML equivalent.
     *
     * Version History:
     * 01/15/15 - Initial documentation
     *
     * Parameters:
     * @param (String) aText - The text to escape.
     *
     * Returns:
     * @return (String) The escaped string.
     */
    public static String forHTML(String aText) {
        final StringBuilder result = new StringBuilder();
        final StringCharacterIterator iterator = new StringCharacterIterator(aText);
        char character = iterator.current();
        while (character != CharacterIterator.DONE) {
            if (character == '<') {
                result.append("&lt;");
            } else if (character == '>') {
                result.append("&gt;");
            } else if (character == '&') {
                result.append("&amp;");
            } else if (character == '\"') {
                result.append("&quot;");
            } else if (character == '\t') {
                addCharEntity(9, result);
            } else if (character == '!') {
                addCharEntity(33, result);
            } else if (character == '#') {
                addCharEntity(35, result);
            } else if (character == '$') {
                addCharEntity(36, result);
            } else if (character == '%') {
                addCharEntity(37, result);
            } else if (character == '\'') {
                addCharEntity(39, result);
            } else if (character == '(') {
                addCharEntity(40, result);
            } else if (character == ')') {
                addCharEntity(41, result);
            } else if (character == '*') {
                addCharEntity(42, result);
            } else if (character == '+') {
                addCharEntity(43, result);
            } else if (character == ',') {
                addCharEntity(44, result);
            } else if (character == '-') {
                addCharEntity(45, result);
            } else if (character == '.') {
                addCharEntity(46, result);
            } else if (character == '/') {
                addCharEntity(47, result);
            } else if (character == ':') {
                addCharEntity(58, result);
            } else if (character == ';') {
                addCharEntity(59, result);
            } else if (character == '=') {
                addCharEntity(61, result);
            } else if (character == '?') {
                addCharEntity(63, result);
            } else if (character == '@') {
                addCharEntity(64, result);
            } else if (character == '[') {
                addCharEntity(91, result);
            } else if (character == '\\') {
                addCharEntity(92, result);
            } else if (character == ']') {
                addCharEntity(93, result);
            } else if (character == '^') {
                addCharEntity(94, result);
            } else if (character == '_') {
                addCharEntity(95, result);
            } else if (character == '`') {
                addCharEntity(96, result);
            } else if (character == '{') {
                addCharEntity(123, result);
            } else if (character == '|') {
                addCharEntity(124, result);
            } else if (character == '}') {
                addCharEntity(125, result);
            } else if (character == '~') {
                addCharEntity(126, result);
            } else {
                // the char is not a special one
                // add it to the result as is
                result.append(character);
            }
            character = iterator.next();
        }
        return result.toString();
    }

    // handling charater
    private static void addCharEntity(Integer aIdx, StringBuilder aBuilder) {
        String padding = "";
        if (aIdx <= 9) {
            padding = "00";
        } else if (aIdx <= 99) {
            padding = "0";
        } else {
            // no prefix
        }
        String number = padding + aIdx.toString();
        aBuilder.append("&#" + number + ";");
    }

    /*
     * Description: Formats a date, ensuring no extra white space, and no epoch dates.
     *
     * Version History:
     * 01/15/15 - Initial documentation
     *
     * Parameters:
     * @param (String) date - The date to be formatted.
     *
     * Returns:
     * @return (String) The formatted date.
     */
    private static String validateDate(String date) {
        if (date == null || date.trim().equals("") || date.trim().equals("00:00 AM") || date.trim().equals("00:00 PM")
                || date.trim().equals("01/01/1900") || date.trim().contains("1900")) {
            return "N/A";
        }

        return date.trim();
    }

    /*
     * Description: Formats a date, ensuring no extra white space, and no epoch dates.
     *
     * Version History:
     * 01/15/15 - Initial documentation
     *
     * Parameters:
     * @param (String) jobId - The ID of the job.
     * @param (DataSource) ds - The database data source object.
     * @param (EmailType) emailType - The type of email to be generated.
     *
     * Returns:
     * @return (String) The formatted date.
     */
    private static String buildSiteDetailsSection(String jobId, DataSource ds, EmailType emailType) {
        SiteInfo si = JobDAO.getSiteInfo(jobId, ds);

        String emailBody = "";

        if (si.getSite_number() != null) {
            emailBody = "<table width=990 cellspacing=0 cellpadding=0 border=0>"
                    + "<tbody>"
                    + "<tr>"
                    + "<td style=\"font-size:11pt; font-family: Calibri, \'Arial Narrow\'; \" nowrap>"
                    + "<span>";

            // Work notes email does not have any dates in it.
            if (emailType != EmailType.WORK_NOTE) {
                emailBody += "All times above are in the time zone of the site (" + si.getSite_time_zone() + "). ";
            }

            emailBody += "The site details are below.</span><br>"
                    + "<br>"
                    + "<span><b>Site Number: </b>" + si.getSite_number() + "</span><br>"
                    + "<span><b>Site Name: </b>" + si.getSite_name() + "</span><br>"
                    + "<span><b>Address: </b>" + si.getSite_address() + ", " + si.getSite_city() + ", " + si.getSite_state() + " " + si.getSite_zip() + "</span><br>"
                    + "</td>"
                    + "</tr>"
                    + "<tr>&nbsp;</tr>"
                    + "</tbody></table>";
        }

        return emailBody;
    }
}
