package com.mind.newjobdb.actions;

import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mind.PO.dao.PODao;
import com.mind.bean.newjobdb.GeneralJobInfo;
import com.mind.bean.newjobdb.IlexTimestamp;
import com.mind.bean.newjobdb.JobDashboard;
import com.mind.bean.newjobdb.JobDashboardBean;
import com.mind.bean.newjobdb.JobDashboardTabType;
import com.mind.bean.newjobdb.TeamInfo;
import com.mind.common.EnvironmentSelector;
import com.mind.common.LabelValue;
import com.mind.common.Util;
import com.mind.common.bean.EmailBean;
import com.mind.common.dao.Authenticationdao;
import com.mind.common.dao.Email;
import com.mind.dao.IM.IMdao;
import com.mind.dao.PM.Jobdao;
import com.mind.dao.PRM.AddContactdao;
import com.mind.dao.PRM.AddInstallationNotesdao;
import com.mind.dao.PRM.Appendixdao;
import com.mind.dao.PRM.CustomerInfoBean;
import com.mind.dao.PRM.CustomerInformationdao;
import com.mind.dao.PRM.JobSetUpDao;
import com.mind.dao.PRM.Scheduledao;
import com.mind.dao.PRM.objects.ScheduleElement;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;
import com.mind.newjobdb.business.Convertor;
import com.mind.newjobdb.dao.ExecuteTab;
import com.mind.newjobdb.dao.JobDAO;
import com.mind.newjobdb.dao.JobLevelWorkflowDAO;
import com.mind.newjobdb.dao.JobWorkflowChecklist;
import com.mind.newjobdb.dao.PurchaseOrder;
import com.mind.newjobdb.dao.PurchaseOrderDAO;
import com.mind.newjobdb.dao.PurchaseOrderList;
import com.mind.newjobdb.dao.TicketDAO;
import com.mind.newjobdb.formbean.JobDashboardForm;
import com.mind.util.WebUtil;

public class JobExecuteAction extends com.mind.common.IlexDispatchAction {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger
            .getLogger(JobExecuteAction.class);

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        /* Page Security: Start */
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        String loginusername = (String) session.getAttribute("username");
        String loginUserEmail = (String) request.getSession(false)
                .getAttribute("useremail");

        if (loginuserid == null) {
            return (mapping.findForward("SessionExpire")); // - Check for
            // session expired.
        }
        if (!Authenticationdao.getPageSecurity(loginuserid, "Manage Job",
                "Job Dashboard - Execute Tab",
                getDataSource(request, "ilexnewDB"))) {
            request.setAttribute("HidePage", "HidePage");
            // return( mapping.findForward( "UnAuthenticate" ) ); // - Check for
            // Pager Security.
        }
        /* Page Security=: End */

        request.setAttribute(
                "tabId",
                (request.getParameter("tabId") == null ? ""
                + JobDashboardTabType.EXECUTE_TAB : request
                .getParameter("tabId")));

        JobDashboardForm bean = (JobDashboardForm) form;
        JobDashboardBean dto = new JobDashboardBean();
        Map<String, Object> map = new HashMap<String, Object>();

        String[] requestorDetails = JobSetUpDao.getRequestorDetailsByTicketId(
                bean.getJobId(), getDataSource(request, "ilexnewDB"));

        if (requestorDetails != null) {
            bean.setTicket_id(requestorDetails[0]);
            bean.setTicketNumber(requestorDetails[1]);
            bean.setRequestor(requestorDetails[2]);
            bean.setRequestorEmail(requestorDetails[3]);
        }
        // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        String appendixId = IMdao.getAppendixId(bean.getJobId(),
                getDataSource(request, "ilexnewDB"));
        String[] pocdetails = new String[16];
        // String[] cnsPrjManangerDetail = new String[5];
        // String[] teamLeadDetail = new String[5];
        pocdetails = com.mind.dao.PRM.Appendixdao.AppendixpocdetailsInfo(
                appendixId, getDataSource(request, "ilexnewDB"));
        bean.setPrimary_Name(pocdetails[0]);
        bean.setPocTitle(pocdetails[14]);
        bean.setPocAddress(pocdetails[13]);
        bean.setPocPhone(pocdetails[1]);
        bean.setPocEmail(pocdetails[12]);
		// DEFINE

        // getting company name
        bean.setMsaId(IMdao.getMSAId(appendixId,
                getDataSource(request, "ilexnewDB")));
        bean.setMsaName(Appendixdao.getMsaname(appendixId,
                getDataSource(request, "ilexnewDB")));

        // getMsaname(
        // getDataSource(request, "ilexnewDB"), bean.getMsaId()));
        String companyname = bean.getMsaName();
        String reqEmail = Jobdao.getEmailRecipientsByJob(bean.getJobId(), getDataSource(request, "ilexnewDB"));

        /* When change Onsite and Offsite data. */
        if (request.getParameter("saveJobActions") != null
                && request.getParameter("saveJobActions").trim().equals("1")) {
            // JobScheduleAction jobScheduleAction = new JobScheduleAction();
            boolean inMarkOnsite = Scheduledao.isAccepted();
            if (inMarkOnsite) {
                addSchedule(request, bean, loginuserid);

                bean.setIsSendEmail("true");
                try {
                    // send email
                    if (Boolean.valueOf(bean.getIsSendEmail())) {

                        // if (LeadName != null) {
                        sendEmailDispatch(bean, loginuserid, loginusername, request,
                                AddContactdao.getEscalationContacts(appendixId,
                                        getDataSource(request, "ilexnewDB")),
                                companyname, loginUserEmail, reqEmail, getDataSource(request, "ilexnewDB"));

                        // }
                        //
                        // else {
                        //
                        // sendEmailDispatch(bean, loginuserid, request,
                        // pocName, pocphone, pocEmail, pocTitle,
                        // companyname, loginUserEmail);
                        //
                        // }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        try {
            BeanUtils.copyProperties(dto, bean);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }
        JobDAO.setOniseOffsite(dto, getDataSource(request, "ilexnewDB"));
        try {
            BeanUtils.copyProperties(bean, dto);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }
        bean.setTabId("" + JobDashboardTabType.EXECUTE_TAB);
        dto.setTabId("" + JobDashboardTabType.EXECUTE_TAB);
        JobDAO.setLatestInstallNotes(dto, getDataSource(request, "ilexnewDB"));
        try {
            BeanUtils.copyProperties(bean, dto);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }
        JobDAO.setLatestComments(map, dto, getDataSource(request, "ilexnewDB"));
        try {
            BeanUtils.copyProperties(bean, dto);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }
        WebUtil.copyMapToRequest(request, map);
        try {
            BeanUtils.copyProperties(dto, bean);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }
        setGeneralFormInfo(bean, request);
        try {
            BeanUtils.copyProperties(bean, dto);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }
        setExecuteTabInfoInForm(dto, getDataSource(request, "ilexnewDB"));
        try {
            BeanUtils.copyProperties(bean, dto);
        } catch (IllegalAccessException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.error(e);
            e.printStackTrace();
        }
        // System.out.println("actstart= "+
        // bean.getOnsiteDate()+" "+bean.getOnsiteHour()+":"+bean.getOnsiteMinute()+" "+bean.getOnsiteIsAm());
        // System.out.println("actend= "+
        // bean.getOffsiteDate()+" "+bean.getOffsiteHour()+":"+bean.getOffsiteMinute()+" "+bean.getOffsiteIsAm());

        String jobType = JobSetUpDao.getjobType(bean.getJobId(),
                getDataSource(request, "ilexnewDB"));
        if (jobType != null && jobType.trim().equalsIgnoreCase("Default")) {
            jobType = "Default";
        } else if (jobType != null && jobType.trim().equalsIgnoreCase("Addendum")) {
            jobType = "Addendum";
        } else {
            jobType = "";
        }
        // System.out.println("bean.getOwner()"+bean.getOwner());

        setStatusOfTabs(jobType, request, bean);

        WebUtil.copyMapToRequest(request, map);
        if (bean.getJobId() != null
                && !bean.getJobId().trim().equalsIgnoreCase("")) {
            map = DatabaseUtilityDao.setMenu(bean.getJobId(), loginuserid,
                    getDataSource(request, "ilexnewDB"));
            WebUtil.copyMapToRequest(request, map);
        }
        ArrayList<CustomerInfoBean> requiredDataList = CustomerInformationdao
                .getCustomerRequiredData(bean.getAppendixId(), bean.getJobId(),
                        getDataSource(request, "ilexnewDB"));
        boolean isSnapon = Appendixdao.checkForSnapon(bean.getAppendixId(),
                getDataSource(request, "ilexnewDB"));

        ArrayList<LabelValue> hourList = Util.getHours();
        ArrayList<LabelValue> minuteList = Util.getMinutes();
        request.setAttribute("hourList", hourList);
        request.setAttribute("minuteList", minuteList);
        request.setAttribute("executeTab", "executeTab");
        request.setAttribute("requiredDataList", requiredDataList);
        request.setAttribute("isSnapon", isSnapon);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        request.setAttribute("notesDummy", bean.getLatestInstallNotes());

        return mapping.findForward("success");
    }

    /**
     * This method is used to set onsite and offsite date for a job.
     *
     * @param request
     * @param bean
     * @param loginuserid
     */
    private void addSchedule(HttpServletRequest request, JobDashboardForm bean,
            String loginuserid) {
        int retValArray[] = new int[2];
        /*
         * String[] list = new String[4]; list = Scheduledao.getSchedule(
         * bean.getJobId(), "J" ,getDataSource(request,"ilexnewDB")); String
         * scheduleId = list[0]!=null?list[0]:"0"; String sestart =
         * list[3]!=null?list[3]:""; String seend = list[4]!=null?list[4]:"";
         */
        IlexTimestamp onsiteTimestamp = new IlexTimestamp(bean.getOnsiteDate(),
                bean.getOnsiteHour(), bean.getOnsiteMinute(),
                IlexTimestamp.getBooleanAMPM(bean.getOnsiteIsAm()));

        IlexTimestamp offsiteTimestamp = new IlexTimestamp(
                bean.getOffsiteDate(), bean.getOffsiteHour(),
                bean.getOffsiteMinute(), IlexTimestamp.getBooleanAMPM(bean
                        .getOffsiteIsAm()));

        String actualStartDate = IlexTimestamp
                .getDateTimeType1(onsiteTimestamp);
        String actualEndDate = IlexTimestamp.getDateTimeType1(offsiteTimestamp);

        // retValArray = Scheduledao.addScheduleForPRM(scheduleId,
        // bean.getJobId(), actstart, actend, "0", "J", loginuserid, sestart,
        // seend, getDataSource(request,"ilexnewDB"));
        retValArray = Scheduledao.setActualEndDate(bean.getJobId(),
                actualStartDate, actualEndDate, loginuserid,
                getDataSource(request, "ilexnewDB"));
        String scheduleId = String.valueOf(retValArray[1]);
        int count = Scheduledao.getDispatchCount(bean.getJobId(),
                getDataSource(request, "ilexnewDB"));

        /* Update Installation Notes */
        if (retValArray[0] > -1) {
            StringBuffer notePart = new StringBuffer();
            notePart.append("Site Visit ");
            if (!Util.isNullOrBlank(bean.getTicketType())) { // true for ticket
                notePart.setLength(0);
                notePart.append("dispatch ");
            }
            notePart.append(count);
            if (!actualStartDate.equals("")
                    && !actualStartDate.equals("01/01/1900")) {
                String defaultset = "Tech onsite for " + notePart.toString();
                AddInstallationNotesdao.addinstallnotes(bean.getJobId(),
                        defaultset, loginuserid,
                        getDataSource(request, "ilexnewDB"), actualStartDate,
                        scheduleId, scheduleId, "S");
            }
            if (!actualEndDate.equals("")
                    && !actualEndDate.equals("01/01/1900")) {
                String defaultset = "Tech offsite for " + notePart.toString();
                AddInstallationNotesdao.addinstallnotes(bean.getJobId(),
                        defaultset, loginuserid,
                        getDataSource(request, "ilexnewDB"), actualEndDate,
                        scheduleId, scheduleId, "E");
            }
        }
    }

    public void setStatusOfTabs(String jobType, HttpServletRequest request,
            JobDashboardForm bean) {
        String ticketType = null;
        if (logger.isDebugEnabled()) {
            logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobggggggggggggggggg------- "
                    + jobType);
        }
        // ticketType = request.getParameter("ticket_type");
        ticketType = bean.getTicketType();
        if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
            request.setAttribute("fromStatus", "fromStatus");
        }
        if (ticketType != null && !ticketType.trim().equalsIgnoreCase("")) {
            request.setAttribute("isTicket", "Ticket");
            request.setAttribute("tabStatus", "job");
        } else {
            if (jobType != null && !jobType.trim().equalsIgnoreCase("")
                    && jobType.trim().equalsIgnoreCase("Default")) {
                request.setAttribute("tabStatus", "Default");
            }
            if (jobType != null && !jobType.trim().equalsIgnoreCase("")
                    && jobType.trim().equalsIgnoreCase("Addendum")) {
                request.setAttribute("tabStatus", "Addendum");
            } else if (jobType != null && jobType.trim().equalsIgnoreCase("")) {
                if (logger.isDebugEnabled()) {
                    logger.debug("setStatusOfTabs(String, HttpServletRequest, JobDashboardForm) - in jobggggggggggggggggg");
                }
                request.setAttribute("tabStatus", "job");
            }
            request.setAttribute("isTicket", "job");
        }
        // request.setAttribute("tabStatus",jobType);
    }

    public void setExecuteTabInfoInForm(JobDashboardBean jobDashboardForm,
            DataSource ds) {
        ExecuteTab executeTab = new ExecuteTab();
        JobDashboard jobDashboard = new JobDashboard();
        JobDAO jobDao = new JobDAO();

        executeTab.setAppendixId(jobDashboardForm.getAppendixId());
        executeTab.setJobId(jobDashboardForm.getJobId());
        GeneralJobInfo generalJobInfo = jobDao.getGeneralJobInfo(
                Long.parseLong(jobDashboardForm.getJobId()), ds);
        executeTab.setGeneralJobInfo(generalJobInfo);

        // JobPlanningNote jobPlanningNote =
        // (JobPlanningNote)jobDao.getJobPlanningNotes(Long.parseLong(jobDashboardForm.getJobId()),ds);
        // CompleteTab.setJobPlanningNote(jobPlanningNote);
        boolean showEvenIfNoPending = true;
        JobWorkflowChecklist topPendingCheckListItems = ((JobWorkflowChecklist) JobLevelWorkflowDAO
                .getJobWorkflowChecklist(
                        Long.parseLong(jobDashboardForm.getJobId()), ds))
                .getTopPendingItems(2, showEvenIfNoPending);

        executeTab.setTopPendingCheckListItems(topPendingCheckListItems);

        ArrayList<PurchaseOrder> listOfPurchaseOrders = PurchaseOrderDAO
                .getPurchaseOrderList(jobDashboardForm.getJobId(), ds);
        PurchaseOrderList purchaseOrderList = new PurchaseOrderList();
        purchaseOrderList.setPurchaseOrders(listOfPurchaseOrders);
        executeTab.setPurchaseOrderList(purchaseOrderList);
        // CostVariations totalCostVariations =
        // PurchaseOrderList.getTotalCostVariations(purchaseOrderList);
        // CompleteTab.setCostVariations(totalCostVariations);
        logger.debug("Check for Integer.ParseInt(appendixId) Job ExecuteAction.setExecuteTabInfoInForm() 5555 =>"
                + jobDashboardForm.getAppendixId());
        if (DatabaseUtilityDao.getProjectType(jobDashboardForm.getAppendixId(),
                ds).equals("NetMedX")) {
            executeTab.setTicketActivitySummary(TicketDAO
                    .getTicketActivitySummary(jobDashboardForm.getJobId(), ds));
            TicketDAO.getScheduleDetail(jobDashboardForm.getJobId(),
                    executeTab, ds);
            executeTab.getGeneralJobInfo().setRequestType(
                    TicketDAO.getTicketRequestType(jobDashboardForm.getJobId(),
                            ds));
        }

        jobDashboard.setJobDashboardTab(executeTab);
        Convertor.convertToJobDashboardForm(jobDashboard, jobDashboardForm);
        if (logger.isDebugEnabled()) {
            logger.debug("setExecuteTabInfoInForm(JobDashboardForm, DataSource) - COUNTRY "
                    + jobDashboardForm.getSite_country()
                    + " ,"
                    + jobDashboardForm.isUs());
        }
    }

    // public ActionForward addComments (ActionMapping mapping , ActionForm form
    // , HttpServletRequest request , HttpServletResponse response)
    // {
    // response.setHeader("Pragma","No-Cache");
    // response.setHeader("Cache-Control","no-cache");
    // response.setHeader("Cache-Control","no-store");
    // response.setDateHeader("Expires",0);
    // System.out.println(request.getParameter("jobId"));
    // String jobId = request.getParameter("jobId");
    // String comments = request.getParameter("comments");
    // HttpSession session = request.getSession( true );
    // String loginuserid = ( String ) session.getAttribute( "userid" );
    // try{
    //
    // JobDAO.addJobComments(comments, jobId, loginuserid, getDataSource(
    // request , "ilexnewDB" ));
    //
    // }catch(Exception e){
    // e.printStackTrace();
    // }
    // return null;
    // }
    public void setGeneralFormInfo(JobDashboardForm bean,
            HttpServletRequest request) {
        bean.setTabId((request.getParameter("tabId") == null ? ""
                + JobDashboardTabType.EXECUTE_TAB : request
                .getParameter("tabId")));
        String loginUserName = (String) request.getSession().getAttribute(
                "username");
        bean.setCreatedBy(loginUserName);
        bean.setJobId((String) (request.getParameter("Job_Id") == null ? request
                .getAttribute("jobid") : request.getParameter("Job_Id")));

        // To open general job Dashboard.
        if (request.getParameter("jobid") != null) {
            bean.setJobId(request.getParameter("jobid"));
        }
        if (request.getAttribute("jobid") != null) {
            bean.setJobId((String) request.getAttribute("jobid"));
        }

        JobDAO jobDAO = new JobDAO();
        TeamInfo teamInfo = jobDAO.getTeamInfo(new Long(bean.getJobId()),
                getDataSource(request, "ilexnewDB"));

        bean.setOwner(teamInfo.getOwner());

        logger.debug("Check for Integer.ParseInt(appendixId) Job ExecuteAction.setGeneralFormInfo() 3333300000 =>"
                + request.getParameter("appendix_Id"));
        bean.setAppendixId((String) (request.getParameter("appendix_Id") == null ? bean
                .getAppendixId() : request.getParameter("appendix_Id")));
        bean.setTicket_id(request.getParameter("ticket_id") == null ? bean
                .getTicket_id() : request.getParameter("ticket_id"));

        logger.debug("Check for Integer.ParseInt(appendixId) Job ExecuteAction.setGeneralFormInfo() 33333 =>"
                + request.getParameter("appendixid"));
        if (request.getParameter("appendix_Id") == null
                && request.getParameter("appendixid") != null) {
            int appendixIdIntVal = Util.fixNumeric(request
                    .getParameter("appendixid"));
            bean.setAppendixId(String.valueOf(appendixIdIntVal));
        }

        logger.debug("Check for Integer.ParseInt(appendixId) Job ExecuteAction.setGeneralFormInfo() 4444 =>"
                + bean.getAppendixId());
        if (request.getAttribute("tabId") != null) {
            bean.setTabId((String) request.getAttribute("tabId"));
        }
    }

    public ActionForward POaction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        return mapping.findForward("success");
    }

    public ActionForward viewInstallNotes(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) {
        JobDashboardForm bean = (JobDashboardForm) form;
        IlexTimestamp scheduleStartTimestamp = new IlexTimestamp(
                bean.getScheduleStartDate(), bean.getScheduleStartHour(),
                bean.getScheduleStartMinute(),
                IlexTimestamp.getBooleanAMPM(bean.getScheduleStartIsAm()));

        IlexTimestamp scheduleEndTimestamp = new IlexTimestamp(
                bean.getScheduleEndDate(), bean.getScheduleEndHour(),
                bean.getScheduleEndMinute(), IlexTimestamp.getBooleanAMPM(bean
                        .getScheduleEndIsAm()));

        IlexTimestamp onsiteTimestamp = new IlexTimestamp(bean.getOnsiteDate(),
                bean.getOnsiteHour(), bean.getOnsiteMinute(),
                IlexTimestamp.getBooleanAMPM(bean.getOnsiteIsAm()));

        IlexTimestamp offsiteTimestamp = new IlexTimestamp(
                bean.getOffsiteDate(), bean.getOffsiteHour(),
                bean.getOffsiteMinute(), IlexTimestamp.getBooleanAMPM(bean
                        .getOffsiteIsAm()));

        // String scheduleStartDate =
        IlexTimestamp.getDateTimeType1(scheduleStartTimestamp);
        // String scheduleEndDate =
        IlexTimestamp.getDateTimeType1(scheduleEndTimestamp);
        // String onsiteDate =
        IlexTimestamp.getDateTimeType1(onsiteTimestamp);
        // String offsiteDate =
        IlexTimestamp.getDateTimeType1(offsiteTimestamp);

        return mapping.findForward("success");
    }

    /**
     * Used to get/set Customer Required Data/Reporting in database.
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    public ActionForward setCustomerDate(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        response.setHeader("pragma", "no-cache");// HTTP 1.1
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.addDateHeader("Expires", -1);
        response.setDateHeader("max-age", 0);
        response.setIntHeader("Expires", -1); // - prevents caching at the proxy
        // server
        response.addHeader("cache-Control", "private");

        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");
        String appendixId = "";
        String jobId = "";
        String custmereValues = "";
        String appendixCustomerIds = "";
        if (request.getParameter("appendixId") != null) {
            appendixId = request.getParameter("appendixId");
        }
        if (request.getParameter("jobId") != null) {
            jobId = request.getParameter("jobId");
        }
        if (request.getParameter("custmereValues") != null) {
            custmereValues = request.getParameter("custmereValues");
            custmereValues = URLDecoder.decode(custmereValues, "UTF-8");
        }
        if (request.getParameter("appendixCustomerIds") != null) {
            appendixCustomerIds = request.getParameter("appendixCustomerIds");
        }

        /* Add Customer information */
        int statusvalue = -1;
        statusvalue = CustomerInformationdao.addCustInfo(jobId,
                appendixCustomerIds.trim(), custmereValues.trim(), loginuserid,
                getDataSource(request, "ilexnewDB"));

        ArrayList<CustomerInfoBean> requiredDataList = CustomerInformationdao
                .getCustomerRequiredData(appendixId, jobId,
                        getDataSource(request, "ilexnewDB"));

        /* Check for Snapon */
        boolean isSnapon = Appendixdao.checkForSnapon(appendixId,
                getDataSource(request, "ilexnewDB"));
        request.setAttribute("isSnapon", isSnapon);
        request.setAttribute("requiredDataList", requiredDataList);
        request.setAttribute("statusvalue", statusvalue);

        response.setContentType("application/xml");
        RequestDispatcher rd = request
                .getRequestDispatcher("/PRM/Job/CustomerInfoFields.jsp"); // -
        // inclued
        // JobReschedule.jsp
        rd.include(request, response);
        return null;
    }

    public void isPOCompletionAllowed(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String poId = "0";
        String poMasterItem = "";
        String completeCheckFlag = "-1";
        if (request.getParameter("poId") != null) {
            poId = request.getParameter("poId");
        }
        if (request.getParameter("poMasterItem") != null) {
            poMasterItem = request.getParameter("poMasterItem");
        }

        if (poMasterItem.equals("Material")) {
            completeCheckFlag = PODao.getDeliverableAcceptCount(poId,
                    getDataSource(request, "ilexnewDB"));
        } else {
            completeCheckFlag = JobDAO.getPOCompletionCheck(poId,
                    getDataSource(request, "ilexnewDB"));
        }

        response.setContentType("text/html");
        response.getWriter().write(completeCheckFlag + "");
    }

    // New Function that call for Email
    private void sendEmailDispatch(JobDashboardForm bean, String userId, String userName,
            HttpServletRequest request, List<String[]> contacts,
            String companyname, String loginUserEmail, String emailRecipients, DataSource ds) {

        EmailBean emailform = new EmailBean();

        emailform.setUsername(this.getResources(request).getMessage(
                "common.Email.username"));
        emailform.setUserpassword(this.getResources(request).getMessage(
                "common.Email.userpassword"));

        emailform.setSmtpservername(EnvironmentSelector
                .getBundleString("common.Email.smtpservername"));
        emailform.setSmtpserverport(this.getResources(request).getMessage(
                "common.Email.smtpserverport"));

        emailform.setFrom(Emailtemplate.SENDER_EMAIL);

        if (emailRecipients.equals("")) {
            emailRecipients = loginUserEmail;
        } else if (!emailRecipients.contains(loginUserEmail)) {
            emailRecipients = loginUserEmail + "," + emailRecipients;
        }
        emailform.setTo(emailRecipients);

        String mailBody;
        String OnSiteDate;
        String OffSiteDate;
        OnSiteDate = bean.getOnsiteDate() + " " + bean.getOnsiteHour() + ":"
                + bean.getOnsiteMinute() + " " + bean.getOnsiteIsAm();
        OffSiteDate = bean.getOffsiteDate() + " " + bean.getOffsiteHour() + ":"
                + bean.getOffsiteMinute() + " " + bean.getOffsiteIsAm();

        String JobName = Jobdao.getJobname(bean.getJobId(),
                getDataSource(request, "ilexnewDB"));

        String[] dispatchParam = new String[5];
        dispatchParam[0] = "";
        dispatchParam[1] = "";
        dispatchParam[2] = OnSiteDate;
        dispatchParam[3] = OffSiteDate;

        int dispatchCount = Integer.parseInt(bean.getCountDispatch());
        ScheduleElement se = Scheduledao.getScheduleByDispatchId(bean.getJobId(), dispatchCount, getDataSource(request, "ilexnewDB"));

        if (bean.getTicket_id() == null || bean.getTicket_id().equals("")) {
            emailform.setSubject("(" + bean.getJobId() + "-" + userId + ") - Marked On/Off Site - Dispatch " + bean.getCountDispatch());

            mailBody = Emailtemplate.buildJobEmailBody_DispatchScheduled(getDataSource(request, "ilexnewDB"), userId, userName, bean.getMsaName(), JobName, bean.getJobId(), Emailtemplate.JobType.PROJECTS, AddContactdao.getEscalationContacts(bean.getAppendixId(), getDataSource(request, "ilexnewDB")), se);
        } else {
            emailform.setSubject("(" + bean.getJobId() + "-" + userId + ") - " + JobName + " - Marked On/Off Site - Dispatch " + bean.getCountDispatch());

            mailBody = Emailtemplate.buildJobEmailBody_DispatchScheduled(getDataSource(request, "ilexnewDB"), userId, userName, bean.getMsaName(), JobName, bean.getJobId(), Emailtemplate.JobType.NETMEDX, AddContactdao.getEscalationContacts(bean.getAppendixId(), getDataSource(request, "ilexnewDB")), se);
        }

        emailform.setContent(mailBody);

        Map<String, String> inlineImages = new HashMap<String, String>();
        String filePath = System.getProperty("catalina.base");
        String filepath = filePath
                + "//webapps//Ilex//images//ampm_header_v2.png";
        // System.out.println("--File location is " + filePath);
        inlineImages.put("image1", filepath);

        Email.send(emailform, "text/html", mailBody.toString(), inlineImages, ds);
    }
}
