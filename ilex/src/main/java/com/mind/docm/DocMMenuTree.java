package com.mind.docm;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

/**
 * The Class DocMMenuTree.
 */
public class DocMMenuTree {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DocMMenuTree.class);

	/** The strarr_level. */
	private int strarr_level[] = null;

	/** The strarr_prevlevel. */
	private String strarr_prevlevel[] = { "", "", "", "" };

	/** The strarr_img. */
	private String strarr_img[] = null;

	/** The strarr_diff img. */
	private String strarr_diffImg[][] = null;

	/** The strarr_diff imgfrom db. */
	private int strarr_diffImgfromDb[][] = null;

	/** The strarr_diff urlfrom db. */
	private int strarr_diffUrlfromDb[][] = null;

	/** The strarr_url. */
	private String strarr_url[] = null;

	/** The strarr_label. */
	private int strarr_label[] = null;

	/** The image_width. */
	private int image_width = 0;

	/** The image_height. */
	private int image_height = 0;

	/** The image_border. */
	private int image_border = 0;

	/** The strarr_urlparam. */
	private String strarr_urlparam[][] = null;

	/** The target. */
	private String target = "";

	/** The str_query. */
	private String str_query = null;

	/** The str_header label. */
	private String str_headerLabel = "";

	/** The str_headerurl. */
	private String str_headerurl = null;

	/** The str_header img. */
	private String str_headerImg = null;

	/** The rs. */
	private ResultSet rs = null;

	/** The conn. */
	private Connection conn = null;

	/** The stmt. */
	private Statement stmt = null;

	/** The menu element. */
	private DocMMenuElement menuElement = null;

	/** The root element. */
	private DocMMenuElement rootElement = null;

	/** The mc. */
	private DocMMenuController mc = null;

	/** The pw. */
	private PrintWriter pw = null;

	/**
	 * Gets the root element.
	 * 
	 * @return the root element
	 */
	public DocMMenuElement getRootElement() {
		return rootElement;
	}

	/**
	 * Gets the doc m menu controller.
	 * 
	 * @return the doc m menu controller
	 */
	public DocMMenuController getDocMMenuController() {
		return mc;
	}

	/**
	 * Gets the pw.
	 * 
	 * @return the pw
	 */
	public PrintWriter getPw() {
		return pw;
	}

	/**
	 * Sets the pw.
	 * 
	 * @param pw
	 *            the new pw
	 */
	public void setPw(PrintWriter pw) {
		this.pw = pw;
	}

	/**
	 * Instantiates a new doc m menu tree.
	 */
	public DocMMenuTree() {
		strarr_diffImg = null;
	}

	/**
	 * Gets the target.
	 * 
	 * @return the target
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * Sets the target.
	 * 
	 * @param t
	 *            the new target
	 */
	public void setTarget(String t) {
		target = t;
	}

	/**
	 * Gets the strarr_level.
	 * 
	 * @return the strarr_level
	 */
	public int[] getStrarr_level() {
		return strarr_level;
	}

	/**
	 * Sets the strarr_level.
	 * 
	 * @param a
	 *            the new strarr_level
	 */
	public void setStrarr_level(int[] a) {
		strarr_level = a;
	}

	/**
	 * Gets the strarr_label.
	 * 
	 * @return the strarr_label
	 */
	public int[] getStrarr_label() {
		return strarr_label;
	}

	/**
	 * Sets the strarr_label.
	 * 
	 * @param a
	 *            the new strarr_label
	 */
	public void setStrarr_label(int[] a) {
		// strarr_label=new String[a.length];
		strarr_label = a;
	}

	/**
	 * Gets the strarr_urlparam.
	 * 
	 * @return the strarr_urlparam
	 */
	public String[][] getStrarr_urlparam() {
		return strarr_urlparam;
	}

	/**
	 * Sets the strarr_urlparam.
	 * 
	 * @param a
	 *            the new strarr_urlparam
	 */
	public void setStrarr_urlparam(String[][] a) {
		strarr_urlparam = a;
	}

	/**
	 * Gets the strarr_img.
	 * 
	 * @return the strarr_img
	 */
	public String[] getStrarr_img() {
		return strarr_img;
	}

	/**
	 * Sets the strarr_img.
	 * 
	 * @param a
	 *            the new strarr_img
	 */
	public void setStrarr_img(String[] a) {
		strarr_img = a;
	}

	/**
	 * Gets the strarr_diff url same level db.
	 * 
	 * @return the strarr_diff url same level db
	 */
	public int[][] getStrarr_diffUrlSameLevelDb() {
		return strarr_diffUrlfromDb;
	}

	/**
	 * Sets the strarr_diff url same level db.
	 * 
	 * @param a
	 *            the new strarr_diff url same level db
	 */
	public void setStrarr_diffUrlSameLevelDb(int[][] a) {
		strarr_diffUrlfromDb = a;
	}

	/**
	 * Gets the strarr_diff img same level db.
	 * 
	 * @return the strarr_diff img same level db
	 */
	public int[][] getStrarr_diffImgSameLevelDb() {
		return strarr_diffImgfromDb;
	}

	/**
	 * Sets the strarr_diff img same level db.
	 * 
	 * @param a
	 *            the new strarr_diff img same level db
	 */
	public void setStrarr_diffImgSameLevelDb(int[][] a) {
		strarr_diffImgfromDb = a;
	}

	/**
	 * Gets the strarr_diff img same level.
	 * 
	 * @return the strarr_diff img same level
	 */
	public String[][] getStrarr_diffImgSameLevel() {
		return strarr_diffImg;
	}

	/**
	 * Sets the strarr_diff img same level.
	 * 
	 * @param a
	 *            the new strarr_diff img same level
	 */
	public void setStrarr_diffImgSameLevel(String[][] a) {
		strarr_diffImg = a;
	}

	/**
	 * Gets the strarr_url.
	 * 
	 * @return the strarr_url
	 */
	public String[] getStrarr_url() {
		return strarr_url;
	}

	/**
	 * Sets the strarr_url.
	 * 
	 * @param a
	 *            the new strarr_url
	 */
	public void setStrarr_url(String[] a) {
		// strarr_url = new String[a.length];
		strarr_url = a;
	}

	/**
	 * Gets the str_query.
	 * 
	 * @return the str_query
	 */
	public String getStr_query() {
		return str_query;
	}

	/**
	 * Sets the str_query.
	 * 
	 * @param a
	 *            the new str_query
	 */
	public void setStr_query(String a) {
		str_query = a;
	}

	/**
	 * Gets the str_header label.
	 * 
	 * @return the str_header label
	 */
	public String getStr_headerLabel() {
		return str_headerLabel;
	}

	/**
	 * Sets the str_header label.
	 * 
	 * @param a
	 *            the new str_header label
	 */
	public void setStr_headerLabel(String a) {
		str_headerLabel = a;
	}

	/**
	 * Gets the str_headerurl.
	 * 
	 * @return the str_headerurl
	 */
	public String getStr_headerurl() {
		return str_headerurl;
	}

	/**
	 * Sets the str_header img.
	 * 
	 * @param a
	 *            the new str_header img
	 */
	public void setStr_headerImg(String a) {
		str_headerImg = a;
	}

	/**
	 * Gets the str_header img.
	 * 
	 * @return the str_header img
	 */
	public String getStr_headerImg() {
		return str_headerImg;
	}

	/**
	 * Sets the str_headerurl.
	 * 
	 * @param a
	 *            the new str_headerurl
	 */
	public void setStr_headerurl(String a) {
		str_headerurl = a;
	}

	/**
	 * Sets the rs.
	 * 
	 * @param s
	 *            the new rs
	 */
	public void setRs(ResultSet s) {
		rs = s;
	}

	/**
	 * Gets the rs.
	 * 
	 * @param conn
	 *            the conn
	 * 
	 * @return the rs
	 */
	public ResultSet getRs(Connection conn) {
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(getStr_query());
		} catch (SQLException e) {
			logger.error("getRs(Connection)", e);

			logger.trace("Exception caught" + e);
		}
		return rs;
	}

	/**
	 * Sets the image_width.
	 * 
	 * @param s
	 *            the new image_width
	 */
	public void setImage_width(int s) {
		image_width = s;
	}

	/**
	 * Gets the image_width.
	 * 
	 * @return the image_width
	 */
	public int getImage_width() {
		return image_width;
	}

	/**
	 * Sets the image_height.
	 * 
	 * @param s
	 *            the new image_height
	 */
	public void setImage_height(int s) {
		image_height = s;
	}

	/**
	 * Gets the image_height.
	 * 
	 * @return the image_height
	 */
	public int getImage_height() {
		return image_height;
	}

	/**
	 * Sets the image_border.
	 * 
	 * @param s
	 *            the new image_border
	 */
	public void setImage_border(int s) {
		image_border = s;
	}

	/**
	 * Gets the image_border.
	 * 
	 * @return the image_border
	 */
	public int getImage_border() {
		return image_border;
	}

	/**
	 * Sets the header.
	 */
	public void setHeader() {

		if ((str_headerurl == null) || (str_headerurl.equals(""))) {
			menuElement = new DocMMenuElement("<b>" + getStr_headerLabel()
					+ "</b>", "", "<img src='" + str_headerImg + "' width="
					+ getImage_width() + " height=" + getImage_height()
					+ " border=" + getImage_border() + ">", "#", null);

		} else {
			menuElement = new DocMMenuElement("<a href=" + str_headerurl
					+ " target=" + target + "><b>" + getStr_headerLabel()
					+ "</b></a>", "", "<img src='" + str_headerImg + "' width="
					+ getImage_width() + " height=" + getImage_height()
					+ " border=" + getImage_border() + ">", "#", null);

		}
		rootElement = menuElement;
	}

	/**
	 * Sets the element.
	 * 
	 * @param label
	 *            the label
	 * @param url
	 *            the url
	 * @param img
	 *            the img
	 * @param target
	 *            the target
	 */
	public void setElement(String label, String url, String img, String target) {

		if (url.equals("")) {
			menuElement = menuElement.add(new DocMMenuElement(label, "",
					"<img src='" + img + "' width=" + getImage_width()
							+ " height=" + getImage_height() + " border="
							+ getImage_border() + ">", "#", menuElement));
		} else {
			menuElement = menuElement.add(new DocMMenuElement("<a href=" + url
					+ " target=" + target + ">" + label + "</a>", "",
					"<img src='" + img + "' width=" + getImage_width()
							+ " height=" + getImage_height() + " border="
							+ getImage_border() + ">", "#", menuElement));
		}
	}

	// start

	/**
	 * Sets the element pv.
	 * 
	 * @param label
	 *            the label
	 * @param url
	 *            the url
	 * @param img
	 *            the img
	 * @param target
	 *            the target
	 * @param moduleName
	 *            the module name
	 * @param incident
	 *            the incident
	 * @param status
	 *            the status
	 */
	public void setElementPV(String label, String url, String img,
			String target, String moduleName, String incident, String status) {
		String image1 = "";
		String image2 = "";
		String title = "";
		String addImageStr = "";

		if (incident.equals("Y"))
			image1 = "docm/images/2.gif";

		if (status.equals("R"))
			image2 = "docm/images/1.gif";

		if (url.equals("")) {
			title = label;
			if (!image1.equals(""))
				addImageStr = addImageStr + " " + "<img src='" + image1
						+ "' border=" + 0 + " >";
			if (!image2.equals(""))
				addImageStr = addImageStr + " " + "<img src='" + image2
						+ "' border=" + 0 + " >";
		} else {
			title = "<a href=" + url + " target=" + target + ">" + label
					+ "</a>";
			if (!image1.equals(""))
				addImageStr = addImageStr + " " + "<img src='" + image1
						+ "' border=" + 0 + " >";
			if (!image2.equals(""))
				addImageStr = addImageStr + " " + "<img src='" + image2
						+ "' border=" + 0 + " >";
		}

		menuElement = menuElement.add(new DocMMenuElement(title, "",
				"<img src='" + img + "' width=" + getImage_width() + " height="
						+ getImage_height() + " border=" + getImage_border()
						+ ">", "#", menuElement, addImageStr));
	}

	// stop

	/**
	 * Generate menu.
	 * 
	 * @param conn
	 *            the conn
	 * @param moduleName
	 *            the module name
	 * 
	 * @return the string
	 */
	public String generateMenu(Connection conn, String moduleName) {

		String url = "";
		boolean tochangelevelflag = true;
		int prevprinted = 0;
		int i = 0;
		boolean haveresultset = false;
		boolean isnullset = false;
		int imageCounter = -1;
		String img = "";
		String str = "";
		StringTokenizer st = null;
		boolean resultsetvalue = false;

		setHeader();
		try {
			rs = getRs(conn);
			if (rs != null) {
				haveresultset = true;

				while (rs.next()) {

					resultsetvalue = true;
					// Change Level
					int levelDiff = 0;
					int count, levelLast = 0;

					if (strarr_prevlevel != null) {
						for (count = 0; count < strarr_level.length; count++) {
							String currstr = rs.getString(strarr_level[count]) == null ? ""
									: rs.getString(strarr_level[count]);
							String prevstr = strarr_prevlevel[count] == null ? ""
									: strarr_prevlevel[count];

							if (!prevstr.equals(currstr) && !prevstr.equals(""))
								levelDiff = levelDiff > strarr_level.length
										- count ? levelDiff
										: strarr_level.length - count;
							if (!prevstr.equals(""))
								levelLast = count + 1;
						}
					}
					levelDiff = levelLast + levelDiff - strarr_level.length >= 0 ? levelLast
							+ levelDiff - strarr_level.length
							: 0;

					for (count = 0; count < levelDiff; count++) {
						if (menuElement.parent != null)
							menuElement = menuElement.parent;
					}
					// End Change Level

					// forsinglerow:
					for (i = 0; i < strarr_level.length; i++) {

						if (strarr_prevlevel[i].equals(rs
								.getString(strarr_level[i]))) { // url
							tochangelevelflag = false; // same value so same
														// level
							isnullset = false; // value is not null

						} else {

							// for changing the image as per the constatnt level
							// data
							if (strarr_diffImg != null) {
								for (int l = 0; l < strarr_diffImg[0].length; l++) {
									if (i == (int) Integer
											.parseInt(strarr_diffImg[0][l])) {
										imageCounter++;
										img = strarr_diffImg[1][imageCounter];
										break;
									} else {

										img = strarr_img[i];
									}
								}

							} else {
								if (strarr_diffImgfromDb != null) {
									for (int k = 0; k < strarr_diffImgfromDb[0].length; k++) {

										if (i == (strarr_diffImgfromDb[0][k])) {
											img = rs.getString(strarr_diffImgfromDb[1][k]);

											break;
										} else {

											img = strarr_img[i];
										}
									}
								} else
									img = strarr_img[i];
							}

							if (rs.getString(strarr_level[i]) != null) // if
																		// current
																		// value
																		// is
																		// not
																		// null
																		// then
																		// it is
																		// to be
																		// printed
							{
								// read the data and send for the total url
								// string to be attached.

								if (strarr_urlparam != null) {
									st = new StringTokenizer(
											strarr_urlparam[1][i], "~");
									url = "";
									String namevalue = "";
									do {
										namevalue = st.nextToken();
										url = url
												+ namevalue.substring(0,
														namevalue.indexOf('|'));
										url = url
												+ "="
												+ rs.getString((int) Integer.parseInt(namevalue.substring(
														namevalue.indexOf('|') + 1,
														namevalue.length())))
												+ "&";

									} while (st.hasMoreTokens());
									url = url.substring(0, url.length() - 1);
									/* Start: Added By Atul 08/01/2007 */
									if (strarr_url[i].equals("")) {
										url = "";
										if (strarr_diffUrlfromDb != null) {
											for (int k = 0; k < strarr_diffUrlfromDb[0].length; k++) {

												if (i == (strarr_diffUrlfromDb[0][k])) {
													url = rs.getString(strarr_diffUrlfromDb[1][k]);
													break;
												} else {
													url = strarr_url[i] + "?"
															+ url;
												}
											}
										}
									} else {
										url = strarr_url[i] + "?" + url;
									}
									/* End: Added By Atul 16/01/2007 */
								}

								// end of url
								if (moduleName.equals("PV")
										|| (i == 2 && moduleName.equals("CM"))) {
									String incident = "";
									String status = "";
									if (rs.getString("cp_pd_incident_type") != null)
										incident = rs
												.getString("cp_pd_incident_type");
									if (rs.getString("cp_partner_status") != null)
										status = rs
												.getString("cp_partner_status");

									setElementPV(rs.getString(strarr_label[i]),
											url, img, getTarget(), moduleName,
											incident, status);
								} else {
									setElement(rs.getString(strarr_label[i]),
											url, img, getTarget());

								}
								strarr_prevlevel[i] = rs
										.getString(strarr_level[i]);
								for (int j = i + 1; j < strarr_level.length; j++) {
									strarr_prevlevel[j] = "";
								}
								prevprinted = i + 1;
								tochangelevelflag = true;
								isnullset = false;
							} // end of isnull if
							else {

								isnullset = true;
								i = 0;
								break;// forsinglerow;

							} // end of isnull else

						} // end of matching else
					} // end of for

					i = 0;
					tochangelevelflag = true;
				}// end of while

			} // end of if

		}

		catch (Exception e) {
			logger.error("generateMenu(Connection, String)", e);

			logger.trace("Exception caught" + e);
		}

		finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.warn(
						"generateMenu(Connection, String) - exception ignored",
						sql);

			}

			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.warn(
						"generateMenu(Connection, String) - exception ignored",
						sql);

			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException sql) {
				logger.warn(
						"generateMenu(Connection, String) - exception ignored",
						sql);

			}
		}

		mc = new DocMMenuController();
		mc.moduleName = moduleName;

		return "";
	}

	/**
	 * Gets the curr state.
	 * 
	 * @return the curr state
	 */
	public String getCurrState() {
		if (mc != null)
			return (mc.currState);
		return ("");
	}

	/**
	 * Gets the expansion state.
	 * 
	 * @return the expansion state
	 */
	public String getExpansionState() {
		if (mc != null)
			return (mc.expansionState);
		return ("");
	}

}
