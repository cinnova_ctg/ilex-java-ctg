package com.mind.docm;

import java.util.Vector;

/**
 * The Class DocMMenuElement.
 */
public class DocMMenuElement {

	/** The out. */
	private StringBuffer out = null;

	// images
	/** The collapsed widget. */
	private String collapsedWidget = "docm/images/ce_menu/oplus.gif";

	/** The collapsed widget start. */
	private String collapsedWidgetStart = "docm/images/ce_menu/oplusStart.gif";

	/** The collapsed widget start top. */
	private String collapsedWidgetStartTop = "docm/images/ce_menu/oplusStartTop.gif";

	/** The collapsed widget end. */
	private String collapsedWidgetEnd = "docm/images/ce_menu/oplusEnd.gif";

	/** The expanded widget. */
	private String expandedWidget = "docm/images/ce_menu/ominus.gif";

	/** The expanded widget start. */
	private String expandedWidgetStart = "docm/images/ce_menu/ominusStart.gif";

	/** The expanded widget start top. */
	private String expandedWidgetStartTop = "docm/images/ce_menu/ominusStartTop.gif";

	/** The expanded widget end. */
	private String expandedWidgetEnd = "docm/images/ce_menu/ominusEnd.gif";

	/** The node widget. */
	private String nodeWidget = "docm/images/ce_menu/onode.gif";

	/** The node widget end. */
	private String nodeWidgetEnd = "docm/images/ce_menu/onodeEnd.gif";

	/** The empty space. */
	private String emptySpace = "docm/images/ce_menu/oempty.gif";

	/** The chain space. */
	private String chainSpace = "docm/images/ce_menu/ochain.gif";

	/** The title. */
	private String title = "";

	/** The add image str. */
	private String addImageStr = "";

	/** The supplemental. */
	private String supplemental = "";

	/** The idents. */
	private String idents = "";

	/** The links. */
	private String links = "";

	/** The elements. */
	private Vector elements = new Vector();

	/** The parent. */
	public DocMMenuElement parent = null;

	/**
	 * Instantiates a new doc m menu element.
	 * 
	 * @param title
	 *            the title
	 * @param supplemental
	 *            the supplemental
	 * @param idents
	 *            the idents
	 * @param links
	 *            the links
	 * @param parent
	 *            the parent
	 */
	DocMMenuElement(String title, String supplemental, String idents,
			String links, DocMMenuElement parent) {
		this.title = title;
		this.supplemental = supplemental;
		this.idents = idents;
		this.links = links;
		this.parent = parent;
		out = new StringBuffer("");
	}

	/**
	 * Instantiates a new doc m menu element.
	 * 
	 * @param title
	 *            the title
	 * @param supplemental
	 *            the supplemental
	 * @param idents
	 *            the idents
	 * @param links
	 *            the links
	 * @param parent
	 *            the parent
	 * @param addImageStr
	 *            the add image str
	 */
	DocMMenuElement(String title, String supplemental, String idents,
			String links, DocMMenuElement parent, String addImageStr) {
		this.title = title;
		this.supplemental = supplemental;
		this.idents = idents;
		this.links = links;
		this.parent = parent;
		this.addImageStr = addImageStr;
		out = new StringBuffer("");
	}

	/**
	 * Adds the.
	 * 
	 * @param m
	 *            the m
	 * 
	 * @return the doc m menu element
	 */
	public DocMMenuElement add(DocMMenuElement m) {
		if (m != null)
			elements.add(m);
		return m;
	}

	/**
	 * Gets the level.
	 * 
	 * @return the level
	 */
	public int getLevel() {
		DocMMenuElement t = this;
		int count = 0;
		while (t.parent != null) {
			t = t.parent;
			count++;
		}
		return count;
	}

	/**
	 * Gets the ancestor.
	 * 
	 * @param level
	 *            the level
	 * 
	 * @return the ancestor
	 */
	public DocMMenuElement getAncestor(int level) {
		DocMMenuElement t = this;

		while (t.parent != null) {
			t = t.parent;
			if (t.getLevel() == level)
				break;
		}
		return t;
	}

	/**
	 * Checks if is last element.
	 * 
	 * @param m
	 *            the m
	 * 
	 * @return true, if is last element
	 */
	public boolean isLastElement(DocMMenuElement m) {
		if (m.parent == null)
			return true;
		if (((DocMMenuElement) (m.parent.elements.elementAt(m.parent.elements
				.size() - 1))).hashCode() == m.hashCode())
			return true;
		return false;
	}

	/**
	 * Draw.
	 * 
	 * @param level
	 *            the level
	 * @param mc
	 *            the mc
	 * 
	 * @return the string
	 */
	public String draw(int level, DocMMenuController mc) {

		if (mc.currID == 0) {
			out.append("<div style='visibility:hidden;'  id='line"
					+ (mc.currID++) + "'>");
		} else {
			out.append("<div class='m2' id='line" + (mc.currID++) + "'>");
		}

		for (int lcount = level; lcount >= 1; lcount--) {
			if (isLastElement(getAncestor(level - lcount)))
				out.append("<img src='" + emptySpace + "'>");
			else
				out.append("<img src='" + chainSpace + "'>");
		}
		if (elements != null && elements.size() > 0) {
			out.append("<img id='widget" + (mc.currID - 1) + "' src='");

			if (parent != null
					&& parent.elements != null
					&& ((DocMMenuElement) (parent.elements
							.elementAt(parent.elements.size() - 1))).hashCode() == this
							.hashCode() && mc.blockID != 0) {
				out.append(collapsedWidgetEnd);
			} else {
				if (mc.blockID == 0) {
					out.append(collapsedWidgetStartTop);
				} else {
					out.append(collapsedWidget);
				}
			}

			out
					.append("' width='15' height='15' border='0' onClick='toggle(this, "
							+ mc.blockID + ",\"" + mc.moduleName + "\")'>");
			// Output display information as needed
			String style = "";
			if (mc.currID == mc.hLine) {
				style = "'a.color : white; background-color:#B7B3AA; '";
			}
			out
					.append(this.supplemental
							+ this.idents
							+ "<span style='"
							+ style
							+ "position:relative; top:-3px; height:11px;' onclick='sethighlight("
							+ mc.currID + ")'>" + title + "</span>");

			mc.currState += (this.getLevel() <= 0) ? "1" : "0";

			out.append("<span class='' blocknum='" + mc.blockID + "' id='block"
					+ (mc.blockID++) + "' style='display:block;' >");

			for (int i = 0; i < elements.size(); i++) {
				out.append((((DocMMenuElement) (elements.get(i))).draw(
						level + 1, mc)));
			}
			out.append("</span></div>");
		} else {
			out.append("<img id='widget" + (mc.currID - 1) + "' src='");
			if (parent != null && parent.elements != null) {
				out
						.append((((DocMMenuElement) (parent.elements
								.elementAt(parent.elements.size() - 1)))
								.hashCode() == this.hashCode() ? nodeWidgetEnd
								: nodeWidget));
			} else
				out.append(nodeWidgetEnd);
			out.append("' alt='' border='0'>");
			String style = "";
			if (mc.currID == mc.hLine) {
				style = "a.color : white; background-color:#B7B3AA; ";
			}
			out.append(this.supplemental + this.idents + "<span style='"
					+ style + "position:relative; top: -2px; height: 10px'>"
					+ this.title + "</span>");
			out.append("</div>");
		}

		return out.toString();
	}

	/**
	 * Draw direct.
	 * 
	 * @param level
	 *            the level
	 * @param mc
	 *            the mc
	 * @param pw
	 *            the pw
	 * 
	 * @return the string
	 */
	public String drawDirect(int level, DocMMenuController mc, StringBuffer pw) {

		if (mc.currID == 0) {
			pw.append("<div style='visibility:hidden;'  id='line"
					+ (mc.currID++) + "'>");
		} else {
			pw.append("<div class='m2' id='line" + (mc.currID++) + "'>");
		}

		for (int lcount = level; lcount >= 1; lcount--) {
			if (isLastElement(getAncestor(level - lcount)))
				pw.append("<img src='" + emptySpace + "'>");
			else
				pw.append("<img src='" + chainSpace + "'>");
		}
		if (elements != null && elements.size() > 0) {
			pw.append("<img id='widget" + (mc.currID - 1) + "' src='");

			if (parent != null
					&& parent.elements != null
					&& ((DocMMenuElement) (parent.elements
							.elementAt(parent.elements.size() - 1))).hashCode() == this
							.hashCode() && mc.blockID != 0) {
				pw.append(collapsedWidgetEnd);
			} else {
				if (mc.blockID == 0) {
					pw.append(collapsedWidgetStartTop);
				} else {
					pw.append(collapsedWidget);
				}
			}

			pw
					.append("' width='15' height='15' border='0' onClick='toggle(this, "
							+ mc.blockID + ",\"" + mc.moduleName + "\")'>");
			// Output display information as needed
			String style = "";
			if (mc.currID == mc.hLine) {
				style = "'a.color : white; background-color:#B7B3AA; '";
			}

			if (!this.addImageStr.equals(""))
				pw
						.append(this.supplemental
								+ this.idents
								+ "<span style='"
								+ style
								+ "position:relative; top:-3px; height:11px;' onclick='sethighlight("
								+ mc.currID + ")'>" + title + "</span> "
								+ addImageStr);
			else
				pw
						.append(this.supplemental
								+ this.idents
								+ "<span style='"
								+ style
								+ "position:relative; top:-3px; height:11px;' onclick='sethighlight("
								+ mc.currID + ")'>" + title + "</span>");

			mc.currState += (this.getLevel() <= 0) ? "1" : "0";

			pw.append("<span class='' blocknum='" + mc.blockID + "' id='block"
					+ (mc.blockID++) + "' style='display:none;'>");

			for (int i = 0; i < elements.size(); i++) {
				((DocMMenuElement) (elements.get(i))).drawDirect(level + 1, mc,
						pw);
			}

			pw.append("</span></div>");
		} else {
			pw.append("<img id='widget" + (mc.currID - 1) + "' src='");
			if (parent != null && parent.elements != null) {
				pw
						.append((((DocMMenuElement) (parent.elements
								.elementAt(parent.elements.size() - 1)))
								.hashCode() == this.hashCode() ? nodeWidgetEnd
								: nodeWidget));
			} else
				pw.append(nodeWidgetEnd);
			pw.append("' alt='' border='0'>");
			String style = "";
			if (mc.currID == mc.hLine) {
				style = "a.color : white; background-color:#B7B3AA; ";
			}

			pw.append(this.supplemental + this.idents + "<span style='" + style
					+ "position:relative; top: -2px; height: 10px'>"
					+ this.title + "</span>");

			if (!this.addImageStr.equals(""))
				pw.append(this.addImageStr + "</div>");
			else
				pw.append("</div>");
		}

		return pw.toString();
	}

	/**
	 * Draw check.
	 * 
	 * @param level
	 *            the level
	 * @param mc
	 *            the mc
	 * 
	 * @return the string
	 */
	public String drawCheck(int level, DocMMenuController mc) {

		out.append("<BR>" + this.title + "---" + level);
		if (elements != null) {

			for (int i = 0; i < elements.size(); i++) {
				out.append((((DocMMenuElement) (elements.get(i))).drawCheck(
						level + 1, mc)));
			}
		}
		return out.toString();
	}

}
