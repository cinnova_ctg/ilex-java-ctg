package com.mind.docm;

import java.util.ArrayList;
import java.util.Iterator;

import com.mind.docm.dao.DocumentMasterDao;

/**
 * The Class DocumentGroupNode.
 */
public class DocumentGroupNode {

	/** The field name. */
	String fieldName = null;

	/** The field value. */
	String fieldValue = null;

	/** The field description. */
	String fieldDescription = null;

	/** The document list. */
	ArrayList documentList = null;

	/** The parent document group node. */
	DocumentGroupNode parentDocumentGroupNode = null;

	/** The child document group nodes. */
	DocumentGroupNode[] childDocumentGroupNodes = null;

	/**
	 * @name getChildren
	 * @purpose This method returns the children nodes corresponding to the
	 *          group field selected and other filter criteria
	 * @param parentsFieldNameValueMap
	 * @param libId
	 * @param viewId
	 * @param findStatus
	 * @return DocumentGroupNode[] i.e. array of DocumentGroupNode objects The
	 *         may either contain the list of group field values or, one of them
	 *         (first an only node) may contain the document list itself.
	 * @throws Exception
	 */
	public static DocumentGroupNode[] getChildren(
			ArrayList parentsFieldNameValueMap, String libId, String viewId,
			String findStatus) throws Exception {
		ArrayList documentGroupNodes = new ArrayList();
		;

		// if parentsFieldNameValueMap is null then
		if (parentsFieldNameValueMap == null
				|| parentsFieldNameValueMap.size() == 0) {
			DocumentGroupNode rootNode = new DocumentGroupNode();
			// return only a single node with value="Documents" and
			// fieldName=null
			rootNode.setFieldName("Documents");
			rootNode.setFieldValue("Documents");
			rootNode.setFieldDescription("Documents");
			// show "Documents" as the tree's root node
			documentGroupNodes.add(rootNode);
		} else {// if parentsFieldNameValueMap is not null, then
			// get the list of group fields to group with
			ArrayList groupOrder = DocumentMasterDao.getGroupOrdering(libId,
					viewId);

			// get the group field name next to parentDocumentGroupNode's field
			// name
			Iterator keyIter = parentsFieldNameValueMap.iterator();
			String currentFieldName = null;
			while (keyIter.hasNext()) {
				ArrayList innerList = (ArrayList) keyIter.next();
				currentFieldName = (String) innerList.get(0);
			}
			if (currentFieldName == null)
				throw new Exception(
						"The current selection must have a field name assigned!");

			// if the next group field name IS not found then
			if (groupOrder.size() == 0
					|| groupOrder.size() <= groupOrder
							.indexOf(currentFieldName) + 1) {
				// fill the documentList with filter criteria having
				// ...the non-null field name/value pair of all parentNodes of
				// this node
				ArrayList documentList = DocumentMasterDao.getDocumentList(
						parentsFieldNameValueMap, libId, viewId, findStatus);

				// return a node with null field name and value and set the
				// documentList
				DocumentGroupNode childNode = new DocumentGroupNode();
				childNode.setFieldName(null);
				childNode.setFieldValue(null);
				childNode.setFieldDescription(null);
				childNode.setDocumentList(documentList);
				documentGroupNodes.add(childNode);
			} else {// if the next group field name IS found then
				String nextFieldName = null;
				if (currentFieldName != null
						&& currentFieldName.equals("Documents")) {
					nextFieldName = (String) groupOrder.get(0);
					parentsFieldNameValueMap = null;
				} else {
					nextFieldName = (String) groupOrder.get(groupOrder
							.indexOf(currentFieldName) + 1);
				}
				// find the distinct values available for
				// ...this next field name from records with fiter criteria
				// having
				// ...the non-null field name/value pair of all parentNodes of
				// this node
				ArrayList groupValueList = DocumentMasterDao.getGroupValueList(
						nextFieldName, parentsFieldNameValueMap, libId, viewId,
						findStatus);

				Iterator groupValueListIter = groupValueList.iterator();
				if (groupValueListIter.hasNext()) {
					groupValueListIter.next();
				}

				while (groupValueListIter.hasNext()) {
					// add values to the list of child nodes
					DocumentGroupNode childNode = new DocumentGroupNode();
					childNode.setFieldName(nextFieldName);
					ArrayList field = (ArrayList) groupValueListIter.next();
					childNode.setFieldValue((String) (field).get(0));
					childNode.setFieldDescription((String) (field).get(1));
					documentGroupNodes.add(childNode);
				}
				// return the array of these nodes
			}
		}
		DocumentGroupNode[] dt = new DocumentGroupNode[documentGroupNodes
				.size()];
		for (int i = 0; i < documentGroupNodes.size(); i++) {
			dt[i] = (DocumentGroupNode) documentGroupNodes.get(i);
		}
		// return (DocumentGroupNode[]) documentGroupNodes.toArray();
		return dt;
	}

	/**
	 * Gets the child document group nodes.
	 * 
	 * @return the child document group nodes
	 */
	public DocumentGroupNode[] getChildDocumentGroupNodes() {
		return childDocumentGroupNodes;
	}

	/**
	 * Adds the child document group node.
	 * 
	 * @param childDocumentGroupNode
	 *            the child document group node
	 */
	public void addChildDocumentGroupNode(
			DocumentGroupNode childDocumentGroupNode) {
		if (childDocumentGroupNode == null)
			return;

		ArrayList groups = new ArrayList();
		if (childDocumentGroupNodes != null) {
			for (int i = 0; i < childDocumentGroupNodes.length; i++) {
				groups.add(childDocumentGroupNodes[i]);
			}
		}
		groups.add(childDocumentGroupNode);

		this.childDocumentGroupNodes = (DocumentGroupNode[]) groups.toArray();
		childDocumentGroupNode.setParentDocumentGroupNode(this);
	}

	/**
	 * Gets the document list.
	 * 
	 * @return the document list
	 */
	public ArrayList getDocumentList() {
		return documentList;
	}

	/**
	 * Sets the document list.
	 * 
	 * @param documentList
	 *            the new document list
	 */
	public void setDocumentList(ArrayList documentList) {
		this.documentList = documentList;
	}

	/**
	 * Gets the parent document group node.
	 * 
	 * @return the parent document group node
	 */
	public DocumentGroupNode getParentDocumentGroupNode() {
		return parentDocumentGroupNode;
	}

	/**
	 * Sets the parent document group node.
	 * 
	 * @param parentDocumentGroupNode
	 *            the new parent document group node
	 */
	private void setParentDocumentGroupNode(
			DocumentGroupNode parentDocumentGroupNode) {
		this.parentDocumentGroupNode = parentDocumentGroupNode;
	}

	/**
	 * Gets the field name.
	 * 
	 * @return the field name
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * Sets the field name.
	 * 
	 * @param fieldName
	 *            the new field name
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * Gets the field value.
	 * 
	 * @return the field value
	 */
	public String getFieldValue() {
		return fieldValue;
	}

	/**
	 * Sets the field value.
	 * 
	 * @param fieldValue
	 *            the new field value
	 */
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	/**
	 * Gets the field description.
	 * 
	 * @return the field description
	 */
	public String getFieldDescription() {
		return fieldDescription;
	}

	/**
	 * Sets the field description.
	 * 
	 * @param fieldDescription
	 *            the new field description
	 */
	public void setFieldDescription(String fieldDescription) {
		this.fieldDescription = fieldDescription;
	}

}
