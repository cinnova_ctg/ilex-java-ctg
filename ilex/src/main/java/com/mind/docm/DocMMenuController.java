package com.mind.docm;

/**
 * The Class DocMMenuController.
 */
public class DocMMenuController {

	/** The curr state. */
	public String currState = "";

	/** The h line. */
	public int hLine = 0;

	/** The expansion state. */
	public String expansionState = "";

	/** The curr id. */
	public int currID = 0;

	/** The block id. */
	public int blockID = 0;

	/** The module name. */
	public String moduleName = "@@";

	/**
	 * Instantiates a new doc m menu controller.
	 */
	DocMMenuController() {

	}
}
