package com.mind.docm;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.mind.docm.dao.TransferSupplements;
import com.mind.xml.DocUtil;

/**
 * The Class TransferAllDoc.
 */
public class TransferAllDoc implements Runnable {

	/** The transfer doc dao msa. */
	DocUtil transferDocDaoMSA = null;

	/** The transfer doc dao appendix. */
	DocUtil transferDocDaoAppendix = null;

	/** The transfer doc dao addendum. */
	DocUtil transferDocDaoAddendum = null;

	/** The transfer doc dao po. */
	DocUtil transferDocDaoPO = null;

	/** The transfer doc dao wo. */
	DocUtil transferDocDaoWO = null;

	/** The transfer doc dao support. */
	DocUtil transferDocDaoSupport = null;

	/** The transfer supplemnts. */
	TransferSupplements transferSupplemnts = null;

	/**
	 * Instantiates a new transfer all doc.
	 * 
	 * @param type
	 *            the type
	 * @param ds
	 *            the ds
	 * @param request
	 *            the request
	 */
	public TransferAllDoc(String type, DataSource ds, HttpServletRequest request) {
		transferDocDaoMSA = new DocUtil("MSA", ds);
		DocUtil.setVariables("MSA", transferDocDaoMSA);
		transferDocDaoAppendix = new DocUtil("Appendix", ds);
		DocUtil.setVariables("Appendix", transferDocDaoAppendix);
		transferDocDaoAddendum = new DocUtil("Addendum", ds);
		DocUtil.setVariables("Addendum", transferDocDaoAddendum);
		transferDocDaoPO = new DocUtil("PO", ds);
		DocUtil.setVariables("PO", transferDocDaoPO);
		transferDocDaoWO = new DocUtil("WO", ds);
		DocUtil.setVariables("WO", transferDocDaoWO);
		transferSupplemnts = new TransferSupplements();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {

		transferDocDaoMSA.run();
		transferDocDaoAppendix.run();
		transferDocDaoAddendum.run();
		transferDocDaoPO.run();
		transferDocDaoWO.run();
		transferSupplemnts.run();
	}
}
