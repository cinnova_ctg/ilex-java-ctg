package com.mind.docm.security;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mind.common.IlexConstants;
import com.mind.docm.connection.DatabaseUtils;
import com.mind.docm.constants.DocumentConstants;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.ilex.docmanager.dao.DatabaseUtilityDao;

/**
 * The Class Authentication.
 */
public class Authentication {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Authentication.class);

	/**
	 * Gets the page security.
	 * 
	 * @param user_id
	 *            the user_id
	 * @param cc_fu_name
	 *            the cc_fu_name
	 * @param cc_fu_type
	 *            the cc_fu_type
	 * 
	 * @return the page security
	 */
	public static boolean getPageSecurity(String user_id, String cc_fu_name,
			String cc_fu_type) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean pagesecurity = false;
		String dbName = DatabaseUtilityDao.getKeyValue("DbName");
		logger.trace("dbName is ::: " + dbName);
		logger.trace("cc_fu_name is ::: " + cc_fu_name);
		logger.trace("cc_fu_type is ::: " + cc_fu_type);

		if (logger.isDebugEnabled()) {
			logger.debug("getPageSecurity(String, String, String) - dbName is ::: "
					+ dbName);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getPageSecurity(String, String, String) - cc_fu_name is ::: "
					+ cc_fu_name);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getPageSecurity(String, String, String) - cc_fu_type is ::: "
					+ cc_fu_type);
		}

		try {
			DataSource ds = (dbName.trim().equals("ilex") ? DBUtil
					.getDataSource(IlexConstants.ILEX_DS_NAME) : DatabaseUtils
					.getDataSource(DocumentConstants.DOCM_SYSDB));

			conn = ds.getConnection();

			if (dbName.trim().equalsIgnoreCase("ilex"))
				pstmt = conn
						.prepareStatement("select 1 from lo_role_privilege a, lo_poc_rol b, cc_function c where b.lo_pr_pc_id = ? and a.lo_rp_ro_id = b.lo_pr_ro_id and a.lo_rp_fu_id = c.cc_fu_id and c.cc_fu_name = ? and c.cc_fu_type = ?");
			else
				pstmt = conn
						.prepareStatement("select 1 from dm_lo_role_privilege a, dm_lo_poc_rol b, dm_cc_function c where b.dm_lo_pr_pc_id = ? and a.dm_lo_rp_ro_id = b.dm_lo_pr_ro_id and a.dm_lo_rp_fu_id = c.dm_cc_fu_id and c.dm_cc_fu_name = ? and c.dm_cc_fu_type = ?");

			pstmt.setString(1, user_id);
			pstmt.setString(2, cc_fu_name);
			pstmt.setString(3, cc_fu_type);
			rs = pstmt.executeQuery();
			if (rs.next())
				pagesecurity = true;
		}

		catch (Exception e) {
			logger.error("getPageSecurity(String, String, String)", e);
			if (logger.isDebugEnabled()) {
				logger.debug("getPageSecurity(String, String, String) - Error occured during Page Security");
			}
		}

		finally {
			DBUtil.close(rs, pstmt);
			DBUtil.close(conn);
		}

		return pagesecurity;
	}

}
