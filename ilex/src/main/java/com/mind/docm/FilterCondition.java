package com.mind.docm;

import java.io.Serializable;

/**
 * The Class FilterCondition.
 */
public class FilterCondition implements Serializable {

	/** The field name. */
	private String fieldName = null;

	/** The field operator. */
	private String fieldOperator = null;

	/** The field text. */
	private String fieldText = null;

	/** The logic operator. */
	private String logicOperator = null;

	/**
	 * Instantiates a new filter condition.
	 * 
	 * @param fieldName
	 *            the field name
	 * @param fieldOperator
	 *            the field operator
	 * @param fieldText
	 *            the field text
	 * @param logicOperator
	 *            the logic operator
	 */
	public FilterCondition(String fieldName, String fieldOperator,
			String fieldText, String logicOperator) {
		this.fieldName = fieldName;
		this.fieldOperator = fieldOperator;
		this.fieldText = fieldText;
		this.logicOperator = logicOperator;
	}

	/**
	 * Gets the field name.
	 * 
	 * @return the field name
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * Sets the field name.
	 * 
	 * @param fieldName
	 *            the new field name
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * Gets the field operator.
	 * 
	 * @return the field operator
	 */
	public String getFieldOperator() {
		return fieldOperator;
	}

	/**
	 * Sets the field operator.
	 * 
	 * @param fieldOperator
	 *            the new field operator
	 */
	public void setFieldOperator(String fieldOperator) {
		this.fieldOperator = fieldOperator;
	}

	/**
	 * Gets the field text.
	 * 
	 * @return the field text
	 */
	public String getFieldText() {
		return fieldText;
	}

	/**
	 * Sets the field text.
	 * 
	 * @param fieldText
	 *            the new field text
	 */
	public void setFieldText(String fieldText) {
		this.fieldText = fieldText;
	}

	/**
	 * Gets the logic operator.
	 * 
	 * @return the logic operator
	 */
	public String getLogicOperator() {
		return logicOperator;
	}

	/**
	 * Sets the logic operator.
	 * 
	 * @param logicOperator
	 *            the new logic operator
	 */
	public void setLogicOperator(String logicOperator) {
		this.logicOperator = logicOperator;
	}

}
