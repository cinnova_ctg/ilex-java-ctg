package com.mind.docm.formbean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

/**
 * The Class LibraryListBean.
 */
public class LibraryListBean extends ActionForm {

	/** The Library list. */
	ArrayList LibraryList = null;

	/** The hmode. */
	String hmode = "";

	/**
	 * Gets the library list.
	 * 
	 * @return the library list
	 */
	public ArrayList getLibraryList() {
		return LibraryList;
	}

	/**
	 * Sets the library list.
	 * 
	 * @param libraryList
	 *            the new library list
	 */
	public void setLibraryList(ArrayList libraryList) {
		LibraryList = libraryList;
	}

	/**
	 * Gets the hmode.
	 * 
	 * @return the hmode
	 */
	public String getHmode() {
		return hmode;
	}

	/**
	 * Sets the hmode.
	 * 
	 * @param hmode
	 *            the new hmode
	 */
	public void setHmode(String hmode) {
		this.hmode = hmode;
	}

}
