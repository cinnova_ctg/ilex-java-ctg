package com.mind.docm.formbean;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

/**
 * The Class DocumentAddBean.
 */
public class DocumentAddBean extends ActionForm {

	/** The hmode. */
	private String hmode = "";

	/** The lib id. */
	private int libId;

	/** The msa id. */
	private String msaId = "";

	/** The msa name. */
	private String msaName = "";

	/** The lib name. */
	private String libName = "";

	/** The myfile. */
	private FormFile myfile;

	/** The myfilename. */
	private String myfilename;

	/** The linked document. */
	private ArrayList linkedDocument = null;

	/** The appendix id. */
	private String appendixId = "";

	/** The entity type. */
	private String entityType = "";

	/** The entity id. */
	private String entityId = "";

	/** The dm_doc_id. */
	private String dm_doc_id = null;

	/** The D ocument version. */
	private String DOcumentVersion = null;

	/** The dm_doc_file_id. */
	private String dm_doc_file_id = null;

	/** The title. */
	private String title = null;

	/** The document date. */
	private String documentDate = null;

	/** The type. */
	private String type = null;

	/** The status. */
	private String status = null;

	/** The author. */
	private String author = null;

	/** The category. */
	private String category = null;

	/** The remarks. */
	private String remarks = null;

	/** The source. */
	private String source = null;

	/** The add date. */
	private String addDate = null;

	/** The add user. */
	private String addUser = null;

	/** The add method. */
	private String addMethod = null;

	/** The change control type. */
	private String changeControlType = null;

	/** The update document date. */
	private String updateDocumentDate = null;

	/** The update document user. */
	private String updateDocumentUser = null;

	/** The update method. */
	private String updateMethod = null;

	/** The previous document reference. */
	private String previousDocumentReference = null;

	/** The delete date. */
	private String deleteDate = null;

	/** The delete user. */
	private String deleteUser = null;

	/** The viewable on web. */
	private String viewableOnWeb = null;

	/** The archive date. */
	private String archiveDate = null;

	/** The archive file name. */
	private String archiveFileName = null;

	/** The base document. */
	private String baseDocument = null;

	/** The application. */
	private String application = null;

	/** The size. */
	private String size = null;

	/** The MIME. */
	private String MIME = null;

	/** The msa_ id. */
	private String msa_ID = null;

	/** The appendix_ id. */
	private String appendix_ID = null;

	/** The job_ id. */
	private String job_ID = null;

	/** The activity id. */
	private String activityId = null;

	/** The po_ id. */
	private String po_Id = null;

	/** The invoice id. */
	private String invoiceId = null;

	/** The change control id. */
	private String changeControlId = null;

	/** The W o_ id. */
	private String WO_ID = null;

	/** The dtl_ smry_ n mx_ id. */
	private String dtl_Smry_NMx_Id = null;

	/** The dtl_ smry_ n mx_ flt_ start date. */
	private String dtl_Smry_NMx_Flt_StartDate = null;

	/** The dtl_ smry_ n mx_ flt_ end date. */
	private String dtl_Smry_NMx_Flt_EndDate = null;

	/** The dtl_ smry_ n mx_ flt_ criticality. */
	private String dtl_Smry_NMx_Flt_Criticality = null;

	/** The dtl_ smry_ n mx_ flt_ status. */
	private String dtl_Smry_NMx_Flt_Status = null;

	/** The dtl_ smry_ n mx_ flt_ requestor. */
	private String dtl_Smry_NMx_Flt_Requestor = null;

	/** The dtl_ smry_ n mx_ flt_ arr_ options. */
	private String dtl_Smry_NMx_Flt_Arr_Options = null;

	/** The dtl_ smry_ n mx_ flt_ msp. */
	private String dtl_Smry_NMx_Flt_Msp = null;

	/** The dtl_ smry_ n mx_ flt_ help desk. */
	private String dtl_Smry_NMx_Flt_HelpDesk = null;

	/** The file name. */
	private String fileName = null;

	/** The link to other doc id. */
	private String linkToOtherDocId = null;

	/** The auto archive. */
	private String autoArchive = null;

	/** The archive time frame. */
	private String archiveTimeFrame = null;

	/** The archival action. */
	private String archivalAction = null;

	/** The archive file path. */
	private String archiveFilePath = null;

	/** The MS a_ name. */
	private String MSA_Name = null;

	/** The appendix_ name. */
	private String appendix_Name = null;

	/** The job_ name. */
	private String job_Name = null;

	/** The partner_ name. */
	private String partner_Name = null;

	/** The W o_ partner_ id. */
	private String WO_Partner_ID = null;

	/** The dtl_ nedx_ i d_ name. */
	private String dtl_Nedx_ID_Name = null;

	/** The type1. */
	private String type1 = null;

	/** The job id. */
	private String jobId = null;

	/** The link lib name from ilex. */
	private String linkLibNameFromIlex = "";

	/** The appendix type. */
	private String appendixType = null;

	/** The appendix status checking. */
	private String appendixStatusChecking = null;

	/** The job name. */
	private String jobName = null;

	/** The appendix name. */
	private String appendixName = null;

	/** The po id. */
	private String poId = null;

	/** The wo id. */
	private String woId = null;

	/** The link lib name. */
	private String linkLibName = null;

	/**
	 * Gets the link lib name.
	 * 
	 * @return the link lib name
	 */
	public String getLinkLibName() {
		return linkLibName;
	}

	/**
	 * Sets the link lib name.
	 * 
	 * @param linkLibName
	 *            the new link lib name
	 */
	public void setLinkLibName(String linkLibName) {
		this.linkLibName = linkLibName;
	}

	/**
	 * Gets the po id.
	 * 
	 * @return the po id
	 */
	public String getPoId() {
		return poId;
	}

	/**
	 * Sets the po id.
	 * 
	 * @param poId
	 *            the new po id
	 */
	public void setPoId(String poId) {
		this.poId = poId;
	}

	/**
	 * Gets the wo id.
	 * 
	 * @return the wo id
	 */
	public String getWoId() {
		return woId;
	}

	/**
	 * Sets the wo id.
	 * 
	 * @param woId
	 *            the new wo id
	 */
	public void setWoId(String woId) {
		this.woId = woId;
	}

	/**
	 * Gets the appendix name.
	 * 
	 * @return the appendix name
	 */
	public String getAppendixName() {
		return appendixName;
	}

	/**
	 * Sets the appendix name.
	 * 
	 * @param appendixName
	 *            the new appendix name
	 */
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	/**
	 * Gets the job name.
	 * 
	 * @return the job name
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * Sets the job name.
	 * 
	 * @param jobName
	 *            the new job name
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * Gets the activity id.
	 * 
	 * @return the activity id
	 */
	public String getActivityId() {
		return this.activityId;
	}

	/**
	 * Sets the activity id.
	 * 
	 * @param activity_ID
	 *            the new activity id
	 */
	public void setActivityId(String activity_ID) {
		this.activityId = activity_ID;
	}

	/**
	 * Gets the adds the date.
	 * 
	 * @return the adds the date
	 */
	public String getAddDate() {
		return this.addDate;
	}

	/**
	 * Sets the adds the date.
	 * 
	 * @param addDate
	 *            the new adds the date
	 */
	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}

	/**
	 * Gets the adds the method.
	 * 
	 * @return the adds the method
	 */
	public String getAddMethod() {
		return this.addMethod;
	}

	/**
	 * Sets the adds the method.
	 * 
	 * @param addMethod
	 *            the new adds the method
	 */
	public void setAddMethod(String addMethod) {
		this.addMethod = addMethod;
	}

	/**
	 * Gets the adds the user.
	 * 
	 * @return the adds the user
	 */
	public String getAddUser() {
		return this.addUser;
	}

	/**
	 * Sets the adds the user.
	 * 
	 * @param addUser
	 *            the new adds the user
	 */
	public void setAddUser(String addUser) {
		this.addUser = addUser;
	}

	/**
	 * Gets the appendix_ id.
	 * 
	 * @return the appendix_ id
	 */
	public String getAppendix_ID() {
		return this.appendix_ID;
	}

	/**
	 * Sets the appendix_ id.
	 * 
	 * @param appendix_ID
	 *            the new appendix_ id
	 */
	public void setAppendix_ID(String appendix_ID) {
		this.appendix_ID = appendix_ID;
	}

	/**
	 * Gets the appendix_ name.
	 * 
	 * @return the appendix_ name
	 */
	public String getAppendix_Name() {
		return appendix_Name;
	}

	/**
	 * Sets the appendix_ name.
	 * 
	 * @param appendix_Name
	 *            the new appendix_ name
	 */
	public void setAppendix_Name(String appendix_Name) {
		this.appendix_Name = appendix_Name;
	}

	/**
	 * Gets the application.
	 * 
	 * @return the application
	 */
	public String getApplication() {
		return this.application;
	}

	/**
	 * Sets the application.
	 * 
	 * @param application
	 *            the new application
	 */
	public void setApplication(String application) {
		this.application = application;
	}

	/**
	 * Gets the archival action.
	 * 
	 * @return the archival action
	 */
	public String getArchivalAction() {
		return archivalAction;
	}

	/**
	 * Sets the archival action.
	 * 
	 * @param archivalAction
	 *            the new archival action
	 */
	public void setArchivalAction(String archivalAction) {
		this.archivalAction = archivalAction;
	}

	/**
	 * Gets the archive date.
	 * 
	 * @return the archive date
	 */
	public String getArchiveDate() {
		return this.archiveDate;
	}

	/**
	 * Sets the archive date.
	 * 
	 * @param archiveDate
	 *            the new archive date
	 */
	public void setArchiveDate(String archiveDate) {
		this.archiveDate = archiveDate;
	}

	/**
	 * Gets the archive file name.
	 * 
	 * @return the archive file name
	 */
	public String getArchiveFileName() {
		return this.archiveFileName;
	}

	/**
	 * Sets the archive file name.
	 * 
	 * @param archiveFileName
	 *            the new archive file name
	 */
	public void setArchiveFileName(String archiveFileName) {
		this.archiveFileName = archiveFileName;
	}

	/**
	 * Gets the archive file path.
	 * 
	 * @return the archive file path
	 */
	public String getArchiveFilePath() {
		return archiveFilePath;
	}

	/**
	 * Sets the archive file path.
	 * 
	 * @param archiveFilePath
	 *            the new archive file path
	 */
	public void setArchiveFilePath(String archiveFilePath) {
		this.archiveFilePath = archiveFilePath;
	}

	/**
	 * Gets the archive time frame.
	 * 
	 * @return the archive time frame
	 */
	public String getArchiveTimeFrame() {
		return archiveTimeFrame;
	}

	/**
	 * Sets the archive time frame.
	 * 
	 * @param archiveTimeFrame
	 *            the new archive time frame
	 */
	public void setArchiveTimeFrame(String archiveTimeFrame) {
		this.archiveTimeFrame = archiveTimeFrame;
	}

	/**
	 * Gets the author.
	 * 
	 * @return the author
	 */
	public String getAuthor() {
		return this.author;
	}

	/**
	 * Sets the author.
	 * 
	 * @param author
	 *            the new author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * Gets the auto archive.
	 * 
	 * @return the auto archive
	 */
	public String getAutoArchive() {
		return autoArchive;
	}

	/**
	 * Sets the auto archive.
	 * 
	 * @param autoArchive
	 *            the new auto archive
	 */
	public void setAutoArchive(String autoArchive) {
		this.autoArchive = autoArchive;
	}

	/**
	 * Gets the base document.
	 * 
	 * @return the base document
	 */
	public String getBaseDocument() {
		return this.baseDocument;
	}

	/**
	 * Sets the base document.
	 * 
	 * @param baseDocument
	 *            the new base document
	 */
	public void setBaseDocument(String baseDocument) {
		this.baseDocument = baseDocument;
	}

	/**
	 * Gets the category.
	 * 
	 * @return the category
	 */
	public String getCategory() {
		return this.category;
	}

	/**
	 * Sets the category.
	 * 
	 * @param category
	 *            the new category
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * Gets the change control id.
	 * 
	 * @return the change control id
	 */
	public String getChangeControlId() {
		return this.changeControlId;
	}

	/**
	 * Sets the change control id.
	 * 
	 * @param changeControl_ID
	 *            the new change control id
	 */
	public void setChangeControlId(String changeControl_ID) {
		this.changeControlId = changeControl_ID;
	}

	/**
	 * Gets the change control type.
	 * 
	 * @return the change control type
	 */
	public String getChangeControlType() {
		return this.changeControlType;
	}

	/**
	 * Sets the change control type.
	 * 
	 * @param changeControlType
	 *            the new change control type
	 */
	public void setChangeControlType(String changeControlType) {
		this.changeControlType = changeControlType;
	}

	/**
	 * Gets the delete date.
	 * 
	 * @return the delete date
	 */
	public String getDeleteDate() {
		return this.deleteDate;
	}

	/**
	 * Sets the delete date.
	 * 
	 * @param deleteDate
	 *            the new delete date
	 */
	public void setDeleteDate(String deleteDate) {
		this.deleteDate = deleteDate;
	}

	/**
	 * Gets the delete user.
	 * 
	 * @return the delete user
	 */
	public String getDeleteUser() {
		return this.deleteUser;
	}

	/**
	 * Sets the delete user.
	 * 
	 * @param deleteUser
	 *            the new delete user
	 */
	public void setDeleteUser(String deleteUser) {
		this.deleteUser = deleteUser;
	}

	/**
	 * Gets the dm_doc_file_id.
	 * 
	 * @return the dm_doc_file_id
	 */
	public String getDm_doc_file_id() {
		return dm_doc_file_id;
	}

	/**
	 * Sets the dm_doc_file_id.
	 * 
	 * @param dm_doc_file_id
	 *            the new dm_doc_file_id
	 */
	public void setDm_doc_file_id(String dm_doc_file_id) {
		this.dm_doc_file_id = dm_doc_file_id;
	}

	/**
	 * Gets the dm_doc_id.
	 * 
	 * @return the dm_doc_id
	 */
	public String getDm_doc_id() {
		return dm_doc_id;
	}

	/**
	 * Sets the dm_doc_id.
	 * 
	 * @param dm_doc_id
	 *            the new dm_doc_id
	 */
	public void setDm_doc_id(String dm_doc_id) {
		this.dm_doc_id = dm_doc_id;
	}

	/**
	 * Gets the document date.
	 * 
	 * @return the document date
	 */
	public String getDocumentDate() {
		return this.documentDate;
	}

	/**
	 * Sets the document date.
	 * 
	 * @param documentDate
	 *            the new document date
	 */
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}

	/**
	 * Gets the d ocument version.
	 * 
	 * @return the d ocument version
	 */
	public String getDOcumentVersion() {
		return this.DOcumentVersion;
	}

	/**
	 * Sets the d ocument version.
	 * 
	 * @param documentVersion
	 *            the new d ocument version
	 */
	public void setDOcumentVersion(String documentVersion) {
		this.DOcumentVersion = documentVersion;
	}

	/**
	 * Gets the dtl_ nedx_ i d_ name.
	 * 
	 * @return the dtl_ nedx_ i d_ name
	 */
	public String getDtl_Nedx_ID_Name() {
		return dtl_Nedx_ID_Name;
	}

	/**
	 * Sets the dtl_ nedx_ i d_ name.
	 * 
	 * @param dtl_Nedx_ID_Name1
	 *            the new dtl_ nedx_ i d_ name
	 */
	public void setDtl_Nedx_ID_Name(String dtl_Nedx_ID_Name1) {
		dtl_Nedx_ID_Name = dtl_Nedx_ID_Name1;
	}

	/**
	 * Gets the dtl_ smry_ n mx_ flt_ arr_ options.
	 * 
	 * @return the dtl_ smry_ n mx_ flt_ arr_ options
	 */
	public String getDtl_Smry_NMx_Flt_Arr_Options() {
		return this.dtl_Smry_NMx_Flt_Arr_Options;
	}

	/**
	 * Sets the dtl_ smry_ n mx_ flt_ arr_ options.
	 * 
	 * @param dtl_Smry_NMx_Flt_Arr_Options
	 *            the new dtl_ smry_ n mx_ flt_ arr_ options
	 */
	public void setDtl_Smry_NMx_Flt_Arr_Options(
			String dtl_Smry_NMx_Flt_Arr_Options) {
		this.dtl_Smry_NMx_Flt_Arr_Options = dtl_Smry_NMx_Flt_Arr_Options;
	}

	/**
	 * Gets the dtl_ smry_ n mx_ flt_ criticality.
	 * 
	 * @return the dtl_ smry_ n mx_ flt_ criticality
	 */
	public String getDtl_Smry_NMx_Flt_Criticality() {
		return this.dtl_Smry_NMx_Flt_Criticality;
	}

	/**
	 * Sets the dtl_ smry_ n mx_ flt_ criticality.
	 * 
	 * @param dtl_Smry_NMx_Flt_Criticality
	 *            the new dtl_ smry_ n mx_ flt_ criticality
	 */
	public void setDtl_Smry_NMx_Flt_Criticality(
			String dtl_Smry_NMx_Flt_Criticality) {
		this.dtl_Smry_NMx_Flt_Criticality = dtl_Smry_NMx_Flt_Criticality;
	}

	/**
	 * Gets the dtl_ smry_ n mx_ flt_ end date.
	 * 
	 * @return the dtl_ smry_ n mx_ flt_ end date
	 */
	public String getDtl_Smry_NMx_Flt_EndDate() {
		return this.dtl_Smry_NMx_Flt_EndDate;
	}

	/**
	 * Sets the dtl_ smry_ n mx_ flt_ end date.
	 * 
	 * @param dtl_Smry_NMx_Flt_EndDate
	 *            the new dtl_ smry_ n mx_ flt_ end date
	 */
	public void setDtl_Smry_NMx_Flt_EndDate(String dtl_Smry_NMx_Flt_EndDate) {
		this.dtl_Smry_NMx_Flt_EndDate = dtl_Smry_NMx_Flt_EndDate;
	}

	/**
	 * Gets the dtl_ smry_ n mx_ flt_ help desk.
	 * 
	 * @return the dtl_ smry_ n mx_ flt_ help desk
	 */
	public String getDtl_Smry_NMx_Flt_HelpDesk() {
		return this.dtl_Smry_NMx_Flt_HelpDesk;
	}

	/**
	 * Sets the dtl_ smry_ n mx_ flt_ help desk.
	 * 
	 * @param dtl_Smry_NMx_Flt_HelpDesk
	 *            the new dtl_ smry_ n mx_ flt_ help desk
	 */
	public void setDtl_Smry_NMx_Flt_HelpDesk(String dtl_Smry_NMx_Flt_HelpDesk) {
		this.dtl_Smry_NMx_Flt_HelpDesk = dtl_Smry_NMx_Flt_HelpDesk;
	}

	/**
	 * Gets the dtl_ smry_ n mx_ flt_ msp.
	 * 
	 * @return the dtl_ smry_ n mx_ flt_ msp
	 */
	public String getDtl_Smry_NMx_Flt_Msp() {
		return this.dtl_Smry_NMx_Flt_Msp;
	}

	/**
	 * Sets the dtl_ smry_ n mx_ flt_ msp.
	 * 
	 * @param dtl_Smry_NMx_Flt_Msp
	 *            the new dtl_ smry_ n mx_ flt_ msp
	 */
	public void setDtl_Smry_NMx_Flt_Msp(String dtl_Smry_NMx_Flt_Msp) {
		this.dtl_Smry_NMx_Flt_Msp = dtl_Smry_NMx_Flt_Msp;
	}

	/**
	 * Gets the dtl_ smry_ n mx_ flt_ requestor.
	 * 
	 * @return the dtl_ smry_ n mx_ flt_ requestor
	 */
	public String getDtl_Smry_NMx_Flt_Requestor() {
		return this.dtl_Smry_NMx_Flt_Requestor;
	}

	/**
	 * Sets the dtl_ smry_ n mx_ flt_ requestor.
	 * 
	 * @param dtl_Smry_NMx_Flt_Requestor
	 *            the new dtl_ smry_ n mx_ flt_ requestor
	 */
	public void setDtl_Smry_NMx_Flt_Requestor(String dtl_Smry_NMx_Flt_Requestor) {
		this.dtl_Smry_NMx_Flt_Requestor = dtl_Smry_NMx_Flt_Requestor;
	}

	/**
	 * Gets the dtl_ smry_ n mx_ flt_ start date.
	 * 
	 * @return the dtl_ smry_ n mx_ flt_ start date
	 */
	public String getDtl_Smry_NMx_Flt_StartDate() {
		return this.dtl_Smry_NMx_Flt_StartDate;
	}

	/**
	 * Sets the dtl_ smry_ n mx_ flt_ start date.
	 * 
	 * @param dtl_Smry_NMx_Flt_StartDate
	 *            the new dtl_ smry_ n mx_ flt_ start date
	 */
	public void setDtl_Smry_NMx_Flt_StartDate(String dtl_Smry_NMx_Flt_StartDate) {
		this.dtl_Smry_NMx_Flt_StartDate = dtl_Smry_NMx_Flt_StartDate;
	}

	/**
	 * Gets the dtl_ smry_ n mx_ flt_ status.
	 * 
	 * @return the dtl_ smry_ n mx_ flt_ status
	 */
	public String getDtl_Smry_NMx_Flt_Status() {
		return this.dtl_Smry_NMx_Flt_Status;
	}

	/**
	 * Sets the dtl_ smry_ n mx_ flt_ status.
	 * 
	 * @param dtl_Smry_NMx_Flt_Status
	 *            the new dtl_ smry_ n mx_ flt_ status
	 */
	public void setDtl_Smry_NMx_Flt_Status(String dtl_Smry_NMx_Flt_Status) {
		this.dtl_Smry_NMx_Flt_Status = dtl_Smry_NMx_Flt_Status;
	}

	/**
	 * Gets the dtl_ smry_ n mx_ id.
	 * 
	 * @return the dtl_ smry_ n mx_ id
	 */
	public String getDtl_Smry_NMx_Id() {
		return this.dtl_Smry_NMx_Id;
	}

	/**
	 * Sets the dtl_ smry_ n mx_ id.
	 * 
	 * @param dtl_Smry_NMx_Id
	 *            the new dtl_ smry_ n mx_ id
	 */
	public void setDtl_Smry_NMx_Id(String dtl_Smry_NMx_Id) {
		this.dtl_Smry_NMx_Id = dtl_Smry_NMx_Id;
	}

	/**
	 * Gets the file name.
	 * 
	 * @return the file name
	 */
	public String getFileName() {
		return this.fileName;
	}

	/**
	 * Sets the file name.
	 * 
	 * @param fileName
	 *            the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the invoice id.
	 * 
	 * @return the invoice id
	 */
	public String getInvoiceId() {
		return this.invoiceId;
	}

	/**
	 * Sets the invoice id.
	 * 
	 * @param invoice_ID
	 *            the new invoice id
	 */
	public void setInvoiceId(String invoice_ID) {
		this.invoiceId = invoice_ID;
	}

	/**
	 * Gets the job_ id.
	 * 
	 * @return the job_ id
	 */
	public String getJob_ID() {
		return this.job_ID;
	}

	/**
	 * Sets the job_ id.
	 * 
	 * @param job_ID
	 *            the new job_ id
	 */
	public void setJob_ID(String job_ID) {
		this.job_ID = job_ID;
	}

	/**
	 * Gets the job_ name.
	 * 
	 * @return the job_ name
	 */
	public String getJob_Name() {
		return job_Name;
	}

	/**
	 * Sets the job_ name.
	 * 
	 * @param jobName1
	 *            the new job_ name
	 */
	public void setJob_Name(String jobName1) {
		this.job_Name = jobName1;
	}

	/**
	 * Gets the mIME.
	 * 
	 * @return the mIME
	 */
	public String getMIME() {
		return this.MIME;
	}

	/**
	 * Sets the mIME.
	 * 
	 * @param mime
	 *            the new mIME
	 */
	public void setMIME(String mime) {
		this.MIME = mime;
	}

	/**
	 * Gets the msa_ id.
	 * 
	 * @return the msa_ id
	 */
	public String getMsa_ID() {
		return this.msa_ID;
	}

	/**
	 * Sets the msa_ id.
	 * 
	 * @param msa_id
	 *            the new msa_ id
	 */
	public void setMsa_ID(String msa_id) {
		this.msa_ID = msa_id;
	}

	/**
	 * Gets the partner_ name.
	 * 
	 * @return the partner_ name
	 */
	public String getPartner_Name() {
		return partner_Name;
	}

	/**
	 * Sets the partner_ name.
	 * 
	 * @param partnerName
	 *            the new partner_ name
	 */
	public void setPartner_Name(String partnerName) {
		partner_Name = partnerName;
	}

	/**
	 * Gets the po_ id.
	 * 
	 * @return the po_ id
	 */
	public String getPo_Id() {
		return this.po_Id;
	}

	/**
	 * Sets the po_ id.
	 * 
	 * @param po_id
	 *            the new po_ id
	 */
	public void setPo_Id(String po_id) {
		this.po_Id = po_id;
	}

	/**
	 * Gets the previous document reference.
	 * 
	 * @return the previous document reference
	 */
	public String getPreviousDocumentReference() {
		return this.previousDocumentReference;
	}

	/**
	 * Sets the previous document reference.
	 * 
	 * @param previousDocumentReference
	 *            the new previous document reference
	 */
	public void setPreviousDocumentReference(String previousDocumentReference) {
		this.previousDocumentReference = previousDocumentReference;
	}

	/**
	 * Gets the remarks.
	 * 
	 * @return the remarks
	 */
	public String getRemarks() {
		return this.remarks;
	}

	/**
	 * Sets the remarks.
	 * 
	 * @param remarks
	 *            the new remarks
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * Gets the size.
	 * 
	 * @return the size
	 */
	public String getSize() {
		return this.size;
	}

	/**
	 * Sets the size.
	 * 
	 * @param size
	 *            the new size
	 */
	public void setSize(String size) {
		this.size = size;
	}

	/**
	 * Gets the source.
	 * 
	 * @return the source
	 */
	public String getSource() {
		return this.source;
	}

	/**
	 * Sets the source.
	 * 
	 * @param source
	 *            the new source
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the update document date.
	 * 
	 * @return the update document date
	 */
	public String getUpdateDocumentDate() {
		return this.updateDocumentDate;
	}

	/**
	 * Sets the update document date.
	 * 
	 * @param updateDocumentDate
	 *            the new update document date
	 */
	public void setUpdateDocumentDate(String updateDocumentDate) {
		this.updateDocumentDate = updateDocumentDate;
	}

	/**
	 * Gets the update document user.
	 * 
	 * @return the update document user
	 */
	public String getUpdateDocumentUser() {
		return this.updateDocumentUser;
	}

	/**
	 * Sets the update document user.
	 * 
	 * @param updateDocumentUser
	 *            the new update document user
	 */
	public void setUpdateDocumentUser(String updateDocumentUser) {
		this.updateDocumentUser = updateDocumentUser;
	}

	/**
	 * Gets the update method.
	 * 
	 * @return the update method
	 */
	public String getUpdateMethod() {
		return this.updateMethod;
	}

	/**
	 * Sets the update method.
	 * 
	 * @param updateMethod
	 *            the new update method
	 */
	public void setUpdateMethod(String updateMethod) {
		this.updateMethod = updateMethod;
	}

	/**
	 * Gets the viewable on web.
	 * 
	 * @return the viewable on web
	 */
	public String getViewableOnWeb() {
		return this.viewableOnWeb;
	}

	/**
	 * Sets the viewable on web.
	 * 
	 * @param viewableOnWeb
	 *            the new viewable on web
	 */
	public void setViewableOnWeb(String viewableOnWeb) {
		this.viewableOnWeb = viewableOnWeb;
	}

	/**
	 * Gets the w o_ id.
	 * 
	 * @return the w o_ id
	 */
	public String getWO_ID() {
		return WO_ID;
	}

	/**
	 * Sets the w o_ id.
	 * 
	 * @param wo_id
	 *            the new w o_ id
	 */
	public void setWO_ID(String wo_id) {
		WO_ID = wo_id;
	}

	/**
	 * Gets the w o_ partner_ id.
	 * 
	 * @return the w o_ partner_ id
	 */
	public String getWO_Partner_ID() {
		return WO_Partner_ID;
	}

	/**
	 * Sets the w o_ partner_ id.
	 * 
	 * @param partner_ID
	 *            the new w o_ partner_ id
	 */
	public void setWO_Partner_ID(String partner_ID) {
		WO_Partner_ID = partner_ID;
	}

	/**
	 * Sets the link to other doc id.
	 * 
	 * @param linkToOtherDocId
	 *            the new link to other doc id
	 */
	public void setLinkToOtherDocId(String linkToOtherDocId) {
		this.linkToOtherDocId = linkToOtherDocId;
	}

	/**
	 * Gets the myfile.
	 * 
	 * @return the myfile
	 */
	public FormFile getMyfile() {
		return myfile;
	}

	/**
	 * Sets the myfile.
	 * 
	 * @param myfile
	 *            the new myfile
	 */
	public void setMyfile(FormFile myfile) {
		this.myfile = myfile;
	}

	/**
	 * Sets the hmode.
	 * 
	 * @param hmode
	 *            the new hmode
	 */
	public void setHmode(String hmode) {
		this.hmode = hmode;
	}

	/**
	 * Gets the lib id.
	 * 
	 * @return the lib id
	 */
	public int getLibId() {
		return libId;
	}

	/**
	 * Sets the lib id.
	 * 
	 * @param libId
	 *            the new lib id
	 */
	public void setLibId(int libId) {
		this.libId = libId;
	}

	/**
	 * Gets the myfilename.
	 * 
	 * @return the myfilename
	 */
	public String getMyfilename() {
		return myfilename;
	}

	/**
	 * Sets the myfilename.
	 * 
	 * @param myfilename
	 *            the new myfilename
	 */
	public void setMyfilename(String myfilename) {
		this.myfilename = myfilename;
	}

	/**
	 * Gets the lib name.
	 * 
	 * @return the lib name
	 */
	public String getLibName() {
		return libName;
	}

	/**
	 * Sets the lib name.
	 * 
	 * @param libName
	 *            the new lib name
	 */
	public void setLibName(String libName) {
		this.libName = libName;
	}

	/**
	 * Gets the hmode.
	 * 
	 * @return the hmode
	 */
	public String getHmode() {
		return hmode;
	}

	/**
	 * Gets the linked document.
	 * 
	 * @return the linked document
	 */
	public ArrayList getLinkedDocument() {
		return linkedDocument;
	}

	/**
	 * Sets the linked document.
	 * 
	 * @param linkedDocument
	 *            the new linked document
	 */
	public void setLinkedDocument(ArrayList linkedDocument) {
		this.linkedDocument = linkedDocument;
	}

	/**
	 * Gets the link to other doc id.
	 * 
	 * @return the link to other doc id
	 */
	public String getLinkToOtherDocId() {
		return linkToOtherDocId;
	}

	/**
	 * Gets the msa id.
	 * 
	 * @return the msa id
	 */
	public String getMsaId() {
		return msaId;
	}

	/**
	 * Sets the msa id.
	 * 
	 * @param msaId
	 *            the new msa id
	 */
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	/**
	 * Gets the msa name.
	 * 
	 * @return the msa name
	 */
	public String getMsaName() {
		return msaName;
	}

	/**
	 * Sets the msa name.
	 * 
	 * @param msaName
	 *            the new msa name
	 */
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	/**
	 * Gets the entity type.
	 * 
	 * @return the entity type
	 */
	public String getEntityType() {
		return entityType;
	}

	/**
	 * Sets the entity type.
	 * 
	 * @param entityType
	 *            the new entity type
	 */
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	/**
	 * Gets the entity id.
	 * 
	 * @return the entity id
	 */
	public String getEntityId() {
		return entityId;
	}

	/**
	 * Sets the entity id.
	 * 
	 * @param entityId
	 *            the new entity id
	 */
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	/**
	 * Gets the appendix id.
	 * 
	 * @return the appendix id
	 */
	public String getAppendixId() {
		return appendixId;
	}

	/**
	 * Sets the appendix id.
	 * 
	 * @param appendixId
	 *            the new appendix id
	 */
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	/**
	 * Gets the job id.
	 * 
	 * @return the job id
	 */
	public String getJobId() {
		return jobId;
	}

	/**
	 * Sets the job id.
	 * 
	 * @param jobId
	 *            the new job id
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	/**
	 * Gets the mS a_ name.
	 * 
	 * @return the mS a_ name
	 */
	public String getMSA_Name() {
		return MSA_Name;
	}

	/**
	 * Sets the mS a_ name.
	 * 
	 * @param name
	 *            the new mS a_ name
	 */
	public void setMSA_Name(String name) {
		MSA_Name = name;
	}

	/**
	 * Gets the appendix status checking.
	 * 
	 * @return the appendix status checking
	 */
	public String getAppendixStatusChecking() {
		return appendixStatusChecking;
	}

	/**
	 * Sets the appendix status checking.
	 * 
	 * @param appendixStatusChecking
	 *            the new appendix status checking
	 */
	public void setAppendixStatusChecking(String appendixStatusChecking) {
		this.appendixStatusChecking = appendixStatusChecking;
	}

	/**
	 * Gets the appendix type.
	 * 
	 * @return the appendix type
	 */
	public String getAppendixType() {
		return appendixType;
	}

	/**
	 * Sets the appendix type.
	 * 
	 * @param appendixType
	 *            the new appendix type
	 */
	public void setAppendixType(String appendixType) {
		this.appendixType = appendixType;
	}

	/**
	 * Gets the link lib name from ilex.
	 * 
	 * @return the link lib name from ilex
	 */
	public String getLinkLibNameFromIlex() {
		return linkLibNameFromIlex;
	}

	/**
	 * Sets the link lib name from ilex.
	 * 
	 * @param linkLibNameFromIlex
	 *            the new link lib name from ilex
	 */
	public void setLinkLibNameFromIlex(String linkLibNameFromIlex) {
		this.linkLibNameFromIlex = linkLibNameFromIlex;
	}

	/**
	 * Gets the type1.
	 * 
	 * @return the type1
	 */
	public String getType1() {
		return type1;
	}

	/**
	 * Sets the type1.
	 * 
	 * @param type1
	 *            the new type1
	 */
	public void setType1(String type1) {
		this.type1 = type1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
