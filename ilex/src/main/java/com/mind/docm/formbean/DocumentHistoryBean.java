package com.mind.docm.formbean;

import org.apache.struts.action.ActionForm;

/**
 * The Class DocumentHistoryBean.
 */
public class DocumentHistoryBean extends ActionForm {
	// For the div which should appear in Every Form.
	/** The msa id. */
	private String msaId = null;

	/** The msa name. */
	private String msaName = null;

	/** The type. */
	private String type = null;

	/** The appendix id. */
	private String appendixId = null;

	/** The job id. */
	private String jobId = null;

	/** The job name. */
	private String jobName = null;

	/** The appendix name. */
	private String appendixName = null;

	/** The appendix status checking. */
	private String appendixStatusChecking = null;

	/** The link lib name from ilex. */
	private String linkLibNameFromIlex = "";

	/** The appendix type. */
	private String appendixType = null;

	/** The appendix type id. */
	private String appendixTypeId = null;

	/**
	 * Gets the appendix type id.
	 * 
	 * @return the appendix type id
	 */
	public String getAppendixTypeId() {
		return appendixTypeId;
	}

	/**
	 * Sets the appendix type id.
	 * 
	 * @param appendixTypeId
	 *            the new appendix type id
	 */
	public void setAppendixTypeId(String appendixTypeId) {
		this.appendixTypeId = appendixTypeId;
	}

	/**
	 * Gets the appendix type.
	 * 
	 * @return the appendix type
	 */
	public String getAppendixType() {
		return appendixType;
	}

	/**
	 * Sets the appendix type.
	 * 
	 * @param appendixType
	 *            the new appendix type
	 */
	public void setAppendixType(String appendixType) {
		this.appendixType = appendixType;
	}

	/**
	 * Gets the appendix status checking.
	 * 
	 * @return the appendix status checking
	 */
	public String getAppendixStatusChecking() {
		return appendixStatusChecking;
	}

	/**
	 * Sets the appendix status checking.
	 * 
	 * @param appendixStatusChecking
	 *            the new appendix status checking
	 */
	public void setAppendixStatusChecking(String appendixStatusChecking) {
		this.appendixStatusChecking = appendixStatusChecking;
	}

	/**
	 * Gets the appendix name.
	 * 
	 * @return the appendix name
	 */
	public String getAppendixName() {
		return appendixName;
	}

	/**
	 * Sets the appendix name.
	 * 
	 * @param appendixName
	 *            the new appendix name
	 */
	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	/**
	 * Gets the job name.
	 * 
	 * @return the job name
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * Sets the job name.
	 * 
	 * @param jobName
	 *            the new job name
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * Gets the msa id.
	 * 
	 * @return the msa id
	 */
	public String getMsaId() {
		return msaId;
	}

	/**
	 * Sets the msa id.
	 * 
	 * @param msaId
	 *            the new msa id
	 */
	public void setMsaId(String msaId) {
		this.msaId = msaId;
	}

	/**
	 * Gets the msa name.
	 * 
	 * @return the msa name
	 */
	public String getMsaName() {
		return msaName;
	}

	/**
	 * Sets the msa name.
	 * 
	 * @param msaName
	 *            the new msa name
	 */
	public void setMsaName(String msaName) {
		this.msaName = msaName;
	}

	/**
	 * Gets the appendix id.
	 * 
	 * @return the appendix id
	 */
	public String getAppendixId() {
		return appendixId;
	}

	/**
	 * Sets the appendix id.
	 * 
	 * @param appendixId
	 *            the new appendix id
	 */
	public void setAppendixId(String appendixId) {
		this.appendixId = appendixId;
	}

	/**
	 * Gets the job id.
	 * 
	 * @return the job id
	 */
	public String getJobId() {
		return jobId;
	}

	/**
	 * Sets the job id.
	 * 
	 * @param jobId
	 *            the new job id
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the link lib name from ilex.
	 * 
	 * @return the link lib name from ilex
	 */
	public String getLinkLibNameFromIlex() {
		return linkLibNameFromIlex;
	}

	/**
	 * Sets the link lib name from ilex.
	 * 
	 * @param linkLibNameFromIlex
	 *            the new link lib name from ilex
	 */
	public void setLinkLibNameFromIlex(String linkLibNameFromIlex) {
		this.linkLibNameFromIlex = linkLibNameFromIlex;
	}
}
